<?php
#  Editing by
/*
 *  Date : 2019-07-29 (Bill)
 *          modified Get_Student_Marksheet_Import_Export_Column_Title_Arr()
 *  Date : 2019-03-21 (Bill)    [2018-1002-1146-08277]
 *          added Get_Student_Marksheet_Import_Export_Column_Title_Arr(), Delete_Import_Temp_Marksheet()
 *	Date : 2018-03-29 (Bill)    [2017-1102-1135-27054]
 *          modified Get_Subject_Info(), to fix PHP 5.4 problem of trim()
 */

# In case of not setting $ReportCardCustomSchoolName in includes/settings.php, set it to "general" by default
if (!isset($ReportCard_Rubrics_CustomSchoolName)) $ReportCard_Rubrics_CustomSchoolName = "general";

### Include Settings File
include_once($intranet_root."/includes/erc_rubrics_config.php");

if (!$NoLangWordings)
{
//	include_once($intranet_root."/lang/bak/lang.marcus.$intranet_session_language.php");
	include_once($intranet_root."/lang/reportcard_rubrics_lang.$intranet_session_language.php");
	
	if (file_exists($intranet_root."/lang/reportcard_rubrics_custom/$ReportCardCustomSchoolName.$intranet_session_language.php")) {
		include_once($intranet_root."/lang/reportcard_rubrics_custom/$ReportCardCustomSchoolName.$intranet_session_language.php");
	}
}

class libreportcardrubrics extends libdb {
	
	/**
	 * Constructor
	 */
	function libreportcardrubrics($AcademicYearID='')
	{
		global $intranet_root, $ReportCard_Rubrics_CustomSchoolName, $PATH_WRT_ROOT, $eRC_Rubrics_ConfigArr;
		
		$this->libdb();
		$this->ModuleName = 'eReportCard_Rubrics';
		$this->ModuleDocumentRoot = $PATH_WRT_ROOT.'home/eAdmin/StudentMgmt/reportcard_rubrics/';
		$this->MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		$this->AcademicYearID = ($AcademicYearID == '')? $this->Get_Active_AcademicYearID() : $AcademicYearID;
		$this->DBName = $this->Get_Database_Name($this->AcademicYearID);
        
		//$this->OtherInfoTypeArr = array("summary","award","merit","eca","remark","interschool","schoolservice","attendance");
		$this->OtherInfoTypeArr = array("attendance","award");
		$this->OtherInfoItemArr['attendance'] = array("Total Number of School Days", "Days Absent", "Time Late");
		$this->OtherInfoItemArr['award'] = array("Award");
	}
	
	function Get_Left_Menu_Structure_Array($CheckAccessRight=1)
	{
		global $Lang, $PATH_WRT_ROOT;
		
		$MenuArr = array();
		
		$MenuArr['Management']['Schedule']['Title'] = $Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MenuTitle'];
		$MenuArr['Management']['Schedule']['URL'] = $this->ModuleDocumentRoot.'management/schedule/schedule.php';
		$MenuArr['Management']['MarksheetRevision']['Title'] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'];
		$MenuArr['Management']['MarksheetRevision']['URL'] = $this->ModuleDocumentRoot.'management/marksheet_revision/marksheet_revision.php?clearCoo=1';
		$MenuArr['Management']['ClassTeacherComment']['Title'] = $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'];
		$MenuArr['Management']['ClassTeacherComment']['URL'] = $this->ModuleDocumentRoot.'management/class_teacher_comment/index.php?clearCoo=1';
		$MenuArr['Management']['ECAService']['Title'] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'];
		$MenuArr['Management']['ECAService']['URL'] = $this->ModuleDocumentRoot.'management/eca/eca.php';
		$MenuArr['Management']['OtherInfo']['Title'] = $Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['MenuTitle'];
		$MenuArr['Management']['OtherInfo']['URL'] = $this->ModuleDocumentRoot.'management/other_info/index.php?clearCoo=1';
		$MenuArr['Management']['DataHandling']['Title'] = $Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['MenuTitle'];
		$MenuArr['Management']['DataHandling']['URL'] = $this->ModuleDocumentRoot.'management/data_handling/transition.php';
		
		$MenuArr['Reports']['ReportCardGeneration']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MenuTitle'];
		$MenuArr['Reports']['ReportCardGeneration']['URL'] = $this->ModuleDocumentRoot.'reports/reportcard_generation/index.php';
		$MenuArr['Reports']['GrandMarksheet']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MenuTitle'];
		$MenuArr['Reports']['GrandMarksheet']['URL'] = $this->ModuleDocumentRoot.'reports/grand_marksheet/index.php';
		$MenuArr['Reports']['SubjectTaughtReport']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['MenuTitle'];
		$MenuArr['Reports']['SubjectTaughtReport']['URL'] = $this->ModuleDocumentRoot.'reports/subject_taught/index.php';
		$MenuArr['Reports']['TopicTaughtReport']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['TopicTaughtReportArr']['MenuTitle'];
		$MenuArr['Reports']['TopicTaughtReport']['URL'] = $this->ModuleDocumentRoot.'reports/topic_taught/index.php';
		$MenuArr['Reports']['ClassAverageRubricsCount']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['MenuTitle'];
		$MenuArr['Reports']['ClassAverageRubricsCount']['URL'] = $this->ModuleDocumentRoot.'reports/class_rubrics_count/index.php';
		$MenuArr['Reports']['FormAverageRubricsCount']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['FormAverageRubricsCountArr']['MenuTitle'];
		$MenuArr['Reports']['FormAverageRubricsCount']['URL'] = $this->ModuleDocumentRoot.'reports/form_rubrics_count/index.php';
		$MenuArr['Reports']['TopicCompareReport']['Title'] = $Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['MenuTitle'];
		$MenuArr['Reports']['TopicCompareReport']['URL'] = $this->ModuleDocumentRoot.'reports/topic_compare_report/index.php';
		
		$MenuArr['Statistics']['RubricsReport']['Title'] = $Lang['eRC_Rubrics']['StatisticsArr']['RubricsReportArr']['MenuTitle'];
		$MenuArr['Statistics']['RubricsReport']['URL'] = $this->ModuleDocumentRoot.'statistics/rubrics_report/index.php';		
		$MenuArr['Statistics']['RadarDiagram']['Title'] = $Lang['eRC_Rubrics']['StatisticsArr']['RadarDiagramArr']['MenuTitle'];
		$MenuArr['Statistics']['RadarDiagram']['URL'] = $this->ModuleDocumentRoot.'statistics/radar_diagram/index.php';
		$MenuArr['Statistics']['ClassSubjectPerformance']['Title'] = $Lang['eRC_Rubrics']['StatisticsArr']['ClassSubjectPerformanceArr']['MenuTitle'];
		$MenuArr['Statistics']['ClassSubjectPerformance']['URL'] = $this->ModuleDocumentRoot.'statistics/class_performance/index.php';
		$MenuArr['Statistics']['SubjectClassPerformance']['Title'] = $Lang['eRC_Rubrics']['StatisticsArr']['SubjectClassPerformanceArr']['MenuTitle'];
		$MenuArr['Statistics']['SubjectClassPerformance']['URL'] = $this->ModuleDocumentRoot.'statistics/subject_performance/index.php';
		$MenuArr['Statistics']['ClassTopicPerformance']['Title'] = $Lang['eRC_Rubrics']['StatisticsArr']['ClassTopicPerformanceArr']['MenuTitle'];
		$MenuArr['Statistics']['ClassTopicPerformance']['URL'] = $this->ModuleDocumentRoot.'statistics/class_topic_performance/index.php';
		$MenuArr['Statistics']['TopicClassPerformance']['Title'] = $Lang['eRC_Rubrics']['StatisticsArr']['TopicClassPerformanceArr']['MenuTitle'];
		$MenuArr['Statistics']['TopicClassPerformance']['URL'] = $this->ModuleDocumentRoot.'statistics/topic_class_performance/index.php';
				
		$MenuArr['Settings']['Module']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'];
		$MenuArr['Settings']['Module']['URL'] = $this->ModuleDocumentRoot.'settings/module/module.php';
		$MenuArr['Settings']['ModuleSubject']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['MenuTitle'];
		$MenuArr['Settings']['ModuleSubject']['URL'] = $this->ModuleDocumentRoot.'settings/module_subject/module_subject.php';
		$MenuArr['Settings']['SubjectRubrics']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['SubjectRubricsArr']['MenuTitle'];
		$MenuArr['Settings']['SubjectRubrics']['URL'] = $this->ModuleDocumentRoot.'settings/subject_rubrics/subject_rubrics.php';
		$MenuArr['Settings']['CurriculumPool']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'];
		$MenuArr['Settings']['CurriculumPool']['URL'] = $this->ModuleDocumentRoot.'settings/curriculum_pool/curriculum_pool.php?clearCoo=1';
		$MenuArr['Settings']['StudentLearningDetails']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['MenuTitle'];
		$MenuArr['Settings']['StudentLearningDetails']['URL'] = $this->ModuleDocumentRoot.'settings/student_learning_details/list.php';
		$MenuArr['Settings']['ReportCardTemplate']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['ReportCardTemplateArr']['MenuTitle'];
		$MenuArr['Settings']['ReportCardTemplate']['URL'] = $this->ModuleDocumentRoot.'settings/reportcard_template/reportcard_template.php?clearCoo=1';
		$MenuArr['Settings']['ECA']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle'];
		$MenuArr['Settings']['ECA']['URL'] = $this->ModuleDocumentRoot.'settings/eca/eca.php';
		$MenuArr['Settings']['CommentBank']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['MenuTitle'];
		$MenuArr['Settings']['CommentBank']['URL'] = $this->ModuleDocumentRoot.'settings/comment_bank/index.php?clearCoo=1';
		$MenuArr['Settings']['AdminGroup']['Title'] = $Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle'];
		$MenuArr['Settings']['AdminGroup']['URL'] = $this->ModuleDocumentRoot.'settings/admin_group/admin_group.php?clearCoo=1';
		
		$IsAdmin = $this->Is_Admin_User($_SESSION['UserID']);
		if ($CheckAccessRight == 1 && $IsAdmin == false)
		{
			$IsClassTeacher = $this->Is_Class_Teacher();
			$IsSubjectTeacher = $this->Is_Subject_Teacher();
			$PageAccessRightArr = $this->Get_User_Page_Access_Right($_SESSION['UserID'], $FromSession=1);
			
			$AccessibleMenuArr = array();
			foreach((array)$MenuArr as $thisMenu => $thisSubMenuArr)
			{
				foreach((array)$thisSubMenuArr as $thisSubMenu => $thisSubMenuInfoArr)
				{
					$thisPageCode = $thisMenu.'_'.$thisSubMenu;
					
					if ($IsClassTeacher)
					{
						/*
						 * Class Teacher MUST have menu:
						 * 	- Management > ClassTeacherComment
						 * 	- All Reports
						 * 	- All Statistics
						 */
						 if ($thisMenu == 'Reports' || $thisMenu == 'Statistics' || $thisPageCode == 'Management_ClassTeacherComment')
						 {
						 	$AccessibleMenuArr[$thisMenu][$thisSubMenu] = $MenuArr[$thisMenu][$thisSubMenu];
						 	continue;
						 }
					}
					
					if ($IsSubjectTeacher)
					{
						/*
						 * Class Teacher MUST have menu:
						 * 	- Management > Marksheet
						 */
						if ($thisPageCode == 'Management_MarksheetRevision')
						{
							$AccessibleMenuArr[$thisMenu][$thisSubMenu] = $MenuArr[$thisMenu][$thisSubMenu];
							continue;
						}
					}
					
					if (in_array($thisPageCode, (array)$PageAccessRightArr))
						$AccessibleMenuArr[$thisMenu][$thisSubMenu] = $MenuArr[$thisMenu][$thisSubMenu];
				}
			}
			
			return $AccessibleMenuArr;
		}
		else
		{
			return $MenuArr;
		}
	}
	
	/**
	 * Get MODULE_OBJ array for generation of left menu
	 *
	 * @return array Array contains the required info for generating left menu
	 */
	function GET_MODULE_OBJ_ARR(){

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPage, $CurrentPageArr, $ck_ReportCard_Rubrics_UserType, $ck_ReportCard_Rubrics_From_eAdmin;
		global $plugin, $UserID, $ReportCard_Rubrics_CustomSchoolName, $Lang;
		
		if(isset($ck_ReportCard_Rubrics_From_eAdmin) && $ck_ReportCard_Rubrics_From_eAdmin != "") {
			if($ck_ReportCard_Rubrics_From_eAdmin==0) {
				$ck_ReportCard_Rubrics_UserType = $ck_ReportCard_Rubrics_UserType=="ADMIN"?"TEACHER":$ck_ReportCard_Rubrics_UserType;
				$CurrentPageArr['eReportCard_Rubrics'] = 0;
				$CurrentPageArr['eServiceeReportCard_Rubrics'] = 1;
			} else {
				$CurrentPageArr['eReportCard_Rubrics'] = 1;
				$CurrentPageArr['eServiceeReportCard_Rubrics'] = 0;
			}
		} else {
			if($ck_ReportCard_Rubrics_UserType=="TEACHER" || $ck_ReportCard_Rubrics_UserType=="PARENT" || $ck_ReportCard_Rubrics_UserType=="STUDENT") {
				$CurrentPageArr['eReportCard_Rubrics'] = 0;
				$CurrentPageArr['eServiceeReportCard_Rubrics'] = 1;
			} else if($ck_ReportCard_Rubrics_UserType=="ADMIN") {
				$CurrentPageArr['eReportCard_Rubrics'] = 1;
				$CurrentPageArr['eServiceeReportCard_Rubrics'] = 0;
			}
		}

//		if($ck_ReportCard_Rubrics_UserType=="ADMIN" || $ck_ReportCard_Rubrics_UserType=="TEACHER")
//		{
//			// Logo link
//			$MODULE_OBJ['root_path'] = "/home/eAdmin/StudentMgmt/reportcard_rubrics/module/module.php";
//			
//			// Current Page Information init
//			$PageManagement = 0;
//			$PageManagement_Schedule = 0;
//			$PageManagement_MarksheetRevision = 0;
//			$PageManagement_ClassTeacherComment = 0;
//			
//			$PageReports = 0;
//			$PageReports_ReportCardGeneration = 0;
//			
//			$PageSettings = 0;
//			$PageSettings_Module = 0;
//			$PageSettings_ModuleSubject = 0;
//			$PageSettings_Rubrics = 0;
//			$PageSettings_CurriculumInfoPool = 0;
//			$PageSettings_StudentLearningDetails = 0;
//			$PageSettings_ReportCardTemplate = 0;
//			
//			switch ($CurrentPage) {
//				case "Management_Schedule":
//					$PageManagement = 1;
//					$PageManagement_Schedule = 1;
//					break;
//				case "Management_MarksheetRevision":
//					$PageManagement = 1;
//					$PageManagement_MarksheetRevision = 1;
//					break;
//				case "Management_ClassTeacherComment":
//					$PageManagement = 1;
//					$PageManagement_ClassTeacherComment = 1;
//					break;
//				case "Management_ECA":
//					$PageManagement = 1;
//					$PageManagement_ECAService = 1;
//					break;
//				case "Management_OtherInfo":
//					$PageManagement = 1;
//					$PageManagement_OtherInfo = 1;
//					break;
//				case "Management_DataHandling":
//					$PageManagement = 1;
//					$PageManagement_DataHandling = 1;
//					break;
//				
//				# Report
//				case "Reports_ReportCardGeneration":
//					$PageReports = 1;
//					$PageReports_ReportCardGeneration = 1;
//					break;
//				case "Reports_SubjectTaughtReport":
//					$PageReports = 1;
//					$PageReports_SubjectTaughtReport = 1;
//					break;
//				case "Reports_TopicTaughtReport":
//					$PageReports = 1;
//					$PageReports_TopicTaughtReport = 1;
//					break;
//				case "Reports_ClassAverageRubricsCountReport":
//					$PageReports = 1;
//					$PageReports_ClassAverageRubricsCountReport = 1;
//					break;
//				case "Reports_FormAverageRubricsCountReport":
//					$PageReports = 1;
//					$PageReports_FormAverageRubricsCountReport = 1;
//					break;
//				case "Reports_GrandMarksheet":
//					$PageReports = 1;
//					$PageReports_GrandMarksheet = 1;
//					break;
//					
//				# statistic	
//				case "Statistics_RubricsReport":
//					$PageStatistics = 1;
//					$PageStatistics_RubricsReport = 1;
//					break;
//				case "Statistics_RadarDiagram":
//					$PageStatistics = 1;
//					$PageStatistics_RadarDiagram = 1;
//					break;
//				case "Statistics_ClassSubjectPerformance":
//					$PageStatistics = 1;
//					$PageStatistics_ClassSubjectPerformance = 1;
//					break;
//				case "Statistics_SubjectClassPerformance":
//					$PageStatistics = 1;
//					$PageStatistics_SubjectClassPerformance = 1;
//					break;
//				case "Statistics_ClassTopicPerformance":
//					$PageStatistics = 1;
//					$PageStatistics_ClassTopicPerformance = 1;
//					break;
//				case "Statistics_TopicClassPerformance":
//					$PageStatistics = 1;
//					$PageStatistics_TopicClassPerformance = 1;
//					break;
//					
//				# setting
//				case "Settings_Module":
//					$PageSettings = 1;
//					$PageSettings_Module = 1;
//					break;
//				case "Settings_ModuleSubject":
//					$PageSettings = 1;
//					$PageSettings_ModuleSubject = 1;
//					break;
//				case "Settings_Rubrics":
//					$PageSettings = 1;
//					$PageSettings_Rubrics = 1;
//					break;
//				case "Settings_CurriculumPool":
//					$PageSettings = 1;
//					$PageSettings_CurriculumPool = 1;
//					break;
//				case "Settings_StudentLearningDetails":
//					$PageSettings = 1;
//					$PageSettings_StudentLearningDetails = 1;
//					break;
//				case "Settings_ReportCardTemplate":
//					$PageSettings = 1;
//					$PageSettings_ReportCardTemplate = 1;
//					break;
//				case "Settings_ECA":
//					$PageSettings = 1;
//					$PageSettings_ECA = 1;
//					break;	
//				case "Settings_CommentBank":
//					$PageSettings = 1;
//					$PageSettings_CommentBank = 1;
//					break;
//				case "Settings_AdminGroup":
//					$PageSettings = 1;
//					$PageSettings_AdminGroup = 1;
//					break;
//			}
//			
//			// Menu information
//			$MenuArr["Management"] = array($Lang['eRC_Rubrics']['ManagementArr']['MenuTitle'], "", $PageManagement);
//			
//			# Mgmt > Schedule
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN")
//				$MenuArr["Management"]["Child"]["Schedule"] = array($Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/schedule/schedule.php", $PageManagement_Schedule);
//			
//			# Mgmt > Marksheet
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN" || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"])
//				$MenuArr["Management"]["Child"]["MarksheetRevision"] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/marksheet_revision/marksheet_revision.php", $PageManagement_MarksheetRevision);
//			
//			# Mgmt > Class Teacher Comment
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN" || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"])
//				$MenuArr["Management"]["Child"]["ClassTeacherComment"] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/class_teacher_comment/index.php?clearCoo=1", $PageManagement_ClassTeacherComment);
//			
//			# Mgmt > ECA
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN")
//				$MenuArr["Management"]["Child"]["ECAService"] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/eca/eca.php", $PageManagement_ECAService);
//			
//			# Mgmt > Other Info
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN")
//				$MenuArr["Management"]["Child"]["OtherInfo"] = array($Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/other_info/index.php?clearCoo=1", $PageManagement_OtherInfo);
//			
//			//$MenuArr["Management"]["Child"]["BehaviorAndAttitude"] = array($Lang['eRC_Rubrics']['ManagementArr']['BehaviorAndAttitudeArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/behavior_attitude/behavior_attitude.php", $PageManagement_BehaviorAndAttitude);
//			
//			# Mgmt > Data Handling
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN")
//				$MenuArr["Management"]["Child"]["DataHandling"] = array($Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/management/data_handling/transition.php", $PageManagement_DataHandling);
//			
//			
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN" || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"])
//			{
//				$MenuArr["Reports"] = array($Lang['eRC_Rubrics']['ReportsArr']['MenuTitle'], "", $PageReports);
//				$MenuArr["Reports"]["Child"]["ReportCardGeneration"] = array($Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/reports/reportcard_generation/index.php", $PageReports_ReportCardGeneration);
//				$MenuArr["Reports"]["Child"]["GrandMarksheet"] = array($Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/reports/grand_marksheet/index.php", $PageReports_GrandMarksheet);
//				$MenuArr["Reports"]["Child"]["SubjectTaughtReport"] = array($Lang['eRC_Rubrics']['ReportsArr']['SubjectTaughtReportArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/reports/subject_taught/index.php", $PageReports_SubjectTaughtReport);
//				$MenuArr["Reports"]["Child"]["TopicTaughtReport"] = array($Lang['eRC_Rubrics']['ReportsArr']['TopicTaughtReportArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/reports/topic_taught/index.php", $PageReports_TopicTaughtReport);
//				$MenuArr["Reports"]["Child"]["ClassAverageRubricsCountReport"] = array($Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/reports/class_rubrics_count/index.php", $PageReports_ClassAverageRubricsCountReport);
//				$MenuArr["Reports"]["Child"]["FormAverageRubricsCountReport"] = array($Lang['eRC_Rubrics']['ReportsArr']['FormAverageRubricsCountArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/reports/form_rubrics_count/index.php", $PageReports_FormAverageRubricsCountReport);
//				
//	
//				$MenuArr["Statistics"] = array($Lang['eRC_Rubrics']['StatisticsArr']['MenuTitle'], "", $PageStatistics);
//				$MenuArr["Statistics"]["Child"]["RubricsReport"] = array($Lang['eRC_Rubrics']['StatisticsArr']['RubricsReportArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/statistics/rubrics_report/index.php", $PageStatistics_RubricsReport);
//				$MenuArr["Statistics"]["Child"]["RadarDiagram"] = array($Lang['eRC_Rubrics']['StatisticsArr']['RadarDiagramArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/statistics/radar_diagram/index.php", $PageStatistics_RadarDiagram);
//				$MenuArr["Statistics"]["Child"]["ClassSubjectPerformance"] = array($Lang['eRC_Rubrics']['StatisticsArr']['ClassSubjectPerformanceArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/statistics/class_performance/index.php", $PageStatistics_ClassSubjectPerformance);
//				$MenuArr["Statistics"]["Child"]["SubjectClassPerformance"] = array($Lang['eRC_Rubrics']['StatisticsArr']['SubjectClassPerformanceArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/statistics/subject_performance/index.php", $PageStatistics_SubjectClassPerformance);
//				$MenuArr["Statistics"]["Child"]["ClassTopicPerformance"] = array($Lang['eRC_Rubrics']['StatisticsArr']['ClassTopicPerformanceArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/statistics/class_topic_performance/index.php", $PageStatistics_ClassTopicPerformance);
//				$MenuArr["Statistics"]["Child"]["TopicClassPerformance"] = array($Lang['eRC_Rubrics']['StatisticsArr']['TopicClassPerformanceArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/statistics/topic_class_performance/index.php", $PageStatistics_TopicClassPerformance);
//			}
//			
//			
//			if ($ck_ReportCard_Rubrics_UserType=="ADMIN")
//			{
//				$MenuArr["Settings"] = array($Lang['eRC_Rubrics']['SettingsArr']['MenuTitle'], "", $PageSettings);
//				$MenuArr["Settings"]["Child"]["Module"] = array($Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/module/module.php", $PageSettings_Module);
//				$MenuArr["Settings"]["Child"]["ModuleSubject"] = array($Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/module_subject/module_subject.php", $PageSettings_ModuleSubject);
//				$MenuArr["Settings"]["Child"]["SubjectRubrics"] = array($Lang['eRC_Rubrics']['SettingsArr']['SubjectRubricsArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/subject_rubrics/subject_rubrics.php", $PageSettings_Rubrics);
//				$MenuArr["Settings"]["Child"]["CurriculumPool"] = array($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/curriculum_pool/curriculum_pool.php?clearCoo=1", $PageSettings_CurriculumPool);
//				$MenuArr["Settings"]["Child"]["StudentLearningDetails"] = array($Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/student_learning_details/list.php", $PageSettings_StudentLearningDetails);
//				$MenuArr["Settings"]["Child"]["ReportCardTemplate"] = array($Lang['eRC_Rubrics']['SettingsArr']['ReportCardTemplateArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/reportcard_template/reportcard_template.php?clearCoo=1", $PageSettings_ReportCardTemplate);
//				$MenuArr["Settings"]["Child"]["ECA"] = array($Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/eca/eca.php", $PageSettings_ECA);
//				$MenuArr["Settings"]["Child"]["CommentBank"] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/comment_bank/index.php?clearCoo=1", $PageSettings_CommentBank);
//				$MenuArr["Settings"]["Child"]["AdminGroup"] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard_rubrics/settings/admin_group/admin_group.php?clearCoo=1", $PageSettings_AdminGroup);
//			}
//		}
		
		
		$CurrentPageArr = explode('_', $CurrentPage);
		$CurrentMenu = $CurrentPageArr[0];
		$MenuInfoArr = $this->Get_Left_Menu_Structure_Array($CheckAccessRight=1);
		$MenuArr = array();
		$IsFirstAccessiableMenu = true;
		
		foreach ((array)$MenuInfoArr as $thisMenu => $thisSubMenuArr)
		{
			$thisIsCurrentMenu = ($CurrentMenu == $thisMenu)? true : false;
			$MenuArr[$thisMenu] = array($Lang['eRC_Rubrics'][$thisMenu.'Arr']['MenuTitle'], "", $thisIsCurrentMenu);
			
			foreach ((array)$thisSubMenuArr as $thisSubMenu => $thisSubMenuInfoArr)
			{
				$thisPageCode = $thisMenu.'_'.$thisSubMenu;
				$thisIsCurrentSubMenu = ($CurrentPage == $thisPageCode)? true : false;
				$MenuArr[$thisMenu]["Child"][$thisSubMenu] = array($thisSubMenuInfoArr['Title'], $thisSubMenuInfoArr['URL'], $thisIsCurrentSubMenu);
				
				if ($IsFirstAccessiableMenu == true)
				{
					$MODULE_OBJ['root_path'] = $thisSubMenuInfoArr['URL'];
					$IsFirstAccessiableMenu = false;
				}
			}
		}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eReportCard";'."\n";
        $js.= '</script>'."\n";
		
		// module information
		$MODULE_OBJ['title'] = $Lang['Header']['Menu']['eReportCard_Rubrics'].$js;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reportcardsystem.gif";
		$MODULE_OBJ['menu'] = $MenuArr;
		
		return $MODULE_OBJ;
	}
	
	function Get_Active_AcademicYearID()
	{
		include_once("libgeneralsettings.php");
		$lgs = new libgeneralsettings();
		$SettingsArr = $lgs->Get_General_Setting($this->ModuleName, array("'ActiveAcademicYearID'"));
		$activeYearID = $SettingsArr['ActiveAcademicYearID'];
		
		return $activeYearID;
	}
	
	function Get_Active_AcademicYearName()
	{
		include_once('form_class_manage.php');
		$ObjAcademicYear = new academic_year($this->AcademicYearID);
		
		return $ObjAcademicYear->Get_Academic_Year_Name();
	}
	
	function Update_Active_AcademicYearID($AcademicYearID)
	{
		include_once("libgeneralsettings.php");
		$lgs = new libgeneralsettings();
		
		$SettingsArr['ActiveAcademicYearID'] = $AcademicYearID;
		$success = $lgs->Save_General_Setting($this->ModuleName, $SettingsArr);
		
		return $success;
	}
	
	function Get_Database_Name($year) {
		global $intranet_db;
		$prefix = $intranet_db."_DB_REPORT_CARD_RUBRICS_";
		return $prefix.$year;
	}
	
	function Get_Table_Name($Table)
	{
		return $this->DBName.'.'.$Table;
	}
	
	function Is_Admin_User() {
		return $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard_Rubrics"];
	}
	
	function Is_Class_Teacher() {
		return $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"];
	}
	
	function Is_Subject_Teacher() {
		return $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"];
	}
	
	/**
	 * Check various conditions to see if user have access right to a page
	 *
	 * @use $PageRight Defined in the browsing page, specifying who can view it
	 * @use $intranet_reportcard_admin Whether user is an eReportCard admin
	 * @use $intranet_reportcard_usertype Type of user
	 * @return boolean
	 */
	function Has_Access_Right($ParPageRight='', $ParCurrentPage='') {
		global $PageRight, $CurrentPage, $ck_ReportCard_Rubrics_UserType, $PATH_WRT_ROOT, $plugin;
		
		$CurrentPage = ($CurrentPage != '')? $ParCurrentPage : $CurrentPage;
		$PageRight = ($ParPageRight != '')? $ParPageRight : $PageRight;
		$PageRightArr = (!is_array($PageRight))? array($PageRight) : $PageRight;
		
		$IsAdmin = $this->Is_Admin_User($_SESSION['UserID']);
		$HasPageAccessRightFromAdminGroup = in_array($CurrentPage, $this->Get_User_Page_Access_Right($_SESSION['UserID'], $FromSession=1));
		$HasPageAccessRightFromUserType = in_array($ck_ReportCard_Rubrics_UserType, $PageRightArr);
		
		$HaveRight = false;
		if ($IsAdmin || $HasPageAccessRightFromAdminGroup || $HasPageAccessRightFromUserType)
			$HaveRight = true;
		
		
//		if (!is_array($PageRight))
//			$PageRightArr = array($PageRight);
//		else
//			$PageRightArr = $PageRight;
//			
//		if(in_array("ADMIN", $PageRightArr) && count($PageRightArr)==1) {
//			# Admin Only
//			$HaveRight = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard_Rubrics"]==1) ? true : false;
//		} else {
//			# User Type which in the PageRightArr or Admin can access the page
//			$HaveRight = (in_array($ck_ReportCard_Rubrics_UserType, $PageRightArr) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard_Rubrics"]==1) ? true : false;
//		}
		
		if (!$plugin['ReportCard_Rubrics'] || $HaveRight == false)
		{
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			intranet_closedb();
			exit;
		}
		
		if ($HaveRight)
			$this->Update_Current_Page_Session($CurrentPage);
		
		return $HaveRight;
	}
	
	function Has_Page_Access_Right($ParCurrentPage)
	{
		// Ignore the Person Type in this checking
		// e.g. Report Generation - Class Teacher has Access Right to the page but only can view the Teaching Class Report
		
		$IsAdmin = $this->Is_Admin_User($_SESSION['UserID']);
		$HasPageAccessRightFromAdminGroup = in_array($ParCurrentPage, $this->Get_User_Page_Access_Right($_SESSION['UserID'], $FromSession=1));
		
		return ($IsAdmin || $HasPageAccessRightFromAdminGroup);
	}
	
	/*
	 * If $SubjectGroupID != '', return $ReturnArr[$SubjectGroupID][$ModuleID][$SubjectID][$TopicID] = 1
	 * If $StudentID != '', return $ReturnArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = 1
	 */
	function Get_Student_Study_Topic_Info($ModuleIDArr='', $SubjectIDArr='', $TopicIDArr='', $StudentIDArr='', $SubjectGroupID='', $ReturnAsso=1)
	{
		$conds_ModuleID = '';
		if ($ModuleIDArr != '')
		{
			if (!is_array($ModuleIDArr))
				$ModuleIDArr = array($ModuleIDArr);
			$conds_ModuleID = " And ModuleID In (".implode(',', $ModuleIDArr).") ";
		}
			
		$conds_SubjectID = '';
		if ($SubjectIDArr != '')
		{
			if (!is_array($SubjectIDArr))
				$SubjectIDArr = array($SubjectIDArr);
			$conds_SubjectID = " And SubjectID In (".implode(',', $SubjectIDArr).") ";
		}
			
		$conds_TopicID = '';
		if ($TopicIDArr != '')
		{
			if (!is_array($TopicIDArr))
				$TopicIDArr = array($TopicIDArr);
			$conds_TopicID = " And TopicID In (".implode(',', $TopicIDArr).") ";
		}
			
		$conds_StudentID = '';
		if ($StudentIDArr != '')
		{
			if (!is_array($StudentIDArr))
				$StudentIDArr = array($StudentIDArr);
			$conds_StudentID = " And StudentID In (".implode(',', $StudentIDArr).") ";
		}
		else
			$conds_StudentID = " And (StudentID Is Null Or StudentID = 0) ";
			
		$conds_SubjectGroupID = '';
		if ($SubjectGroupID != '')
			$conds_SubjectGroupID = " And SubjectGroupID = '$SubjectGroupID' ";
		
		$RC_STUDENT_STUDY_TOPIC = $this->Get_Table_Name('RC_STUDENT_STUDY_TOPIC');
		$sql = "Select
						StudentID,
						ModuleID,
						SubjectID,
						TopicID,
						SubjectGroupID,
						DateModified,
						LastModifiedBy
				From
						$RC_STUDENT_STUDY_TOPIC
				Where
						1
						$conds_ModuleID
						$conds_SubjectID
						$conds_TopicID
						$conds_StudentID
						$conds_SubjectGroupID
				";
				
		$StudentLearningDetailsArr = $this->returnArray($sql);
		$numOfRecord = count($StudentLearningDetailsArr);
		
		$ReturnArr = array();
		
		if ($ReturnAsso == 1)
		{
			for ($i=0; $i<$numOfRecord; $i++)
			{
				$thisStudentID = $StudentLearningDetailsArr[$i]['StudentID'];
				$thisSubjectGroupID = $StudentLearningDetailsArr[$i]['SubjectGroupID'];
				$thisModuleID = $StudentLearningDetailsArr[$i]['ModuleID'];
				$thisTopicID = $StudentLearningDetailsArr[$i]['TopicID'];
				$thisSubjectID = $StudentLearningDetailsArr[$i]['SubjectID'];
				
				if ($StudentIDArr != '')
					$ReturnArr[$thisStudentID][$thisModuleID][$thisSubjectID][$thisTopicID] = $StudentLearningDetailsArr[$i];
				else if ($SubjectGroupID != '')
					$ReturnArr[$thisSubjectGroupID][$thisModuleID][$thisSubjectID][$thisTopicID] = $StudentLearningDetailsArr[$i];
			}
		}
		else
		{
			$ReturnArr = $StudentLearningDetailsArr;
		}
		
		return $ReturnArr;
	}
	
	function Get_Student_Study_Topic_Tree($StudentID, $SubjectID, $ModuleIDArr)
	{
		global $PATH_WRT_ROOT, $lreportcard;
		
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_topic.php');
		$lreportcard_topic = new libreportcardrubrics_topic();
		
		### Get all subject topics even if the student has not studied
		$SubjectTopicMappingArr = $lreportcard_topic->Get_Topic_Tree($SubjectID);
		
		### Get Student Study Topics
		$StudentStudyTopicArr = $this->Get_Student_Study_Topic_Info($ModuleIDArr, $SubjectID, $TopicID='', $StudentID, $SubjectGroupID='', $ReturnAsso=0);
		$StudentStudyTopicIDArr = array_unique(Get_Array_By_Key($StudentStudyTopicArr, 'TopicID'));
		
		### Count the number of Topics studied by the Student
		$StudentStudyTopicCountArr = $this->Count_Student_Study_Topic_Recursion($SubjectTopicMappingArr, $StudentStudyTopicIDArr);
		
		### Re-build the Topic Tree
		$StudentStudyTopicMappingArr = $this->Get_Student_Study_Topic_Tree_Recursion($SubjectTopicMappingArr, $StudentStudyTopicCountArr);
		
		return $StudentStudyTopicMappingArr;
	}
	
	function Count_Student_Study_Topic_Recursion($SubjectTopicMappingArr, $StudentStudyTopicIDArr, $StudentStudyTopicCountArr=array())
	{
		foreach ($SubjectTopicMappingArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$numOfLinkedTopic = count($thisLinkedTopicIDArr);
			
			if ($numOfLinkedTopic > 0)
			{
				### Loop through all lower-topic Level to get the number of studied Topic within this Topic
				$StudentStudyTopicCountArr = $this->Count_Student_Study_Topic_Recursion($thisLinkedTopicIDArr, $StudentStudyTopicIDArr, $StudentStudyTopicCountArr);
				
				### Sum up the number of studied Topic
				foreach ((array)$thisLinkedTopicIDArr as $thisNextLevelTopicID => $thisInfoArr)
					$StudentStudyTopicCountArr[$thisTopicID] += $StudentStudyTopicCountArr[$thisNextLevelTopicID];
			}
			else
			{
				### Last Level => Check if student need to study this Topic
				if (in_array($thisTopicID, $StudentStudyTopicIDArr))
					$NumOfStudyTopic = 1;
				else
					$NumOfStudyTopic = 0;
					
				$StudentStudyTopicCountArr[$thisTopicID] = $NumOfStudyTopic;
			} 
		}
		
		return $StudentStudyTopicCountArr;
	}
	
	function Get_Student_Study_Topic_Tree_Recursion($SubjectTopicMappingArr, $StudentStudyTopicCountArr)
	{
		foreach ($SubjectTopicMappingArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$numOfLinkedTopic = count($thisLinkedTopicIDArr);
			
			if ($numOfLinkedTopic > 0)
			{
				$TempArr = $this->Get_Student_Study_Topic_Tree_Recursion($thisLinkedTopicIDArr, $StudentStudyTopicCountArr);
				
				if ($StudentStudyTopicCountArr[$thisTopicID] > 0)
					$StudentStudyTopicMappingArr[$thisTopicID] = $TempArr;
			}
			else
			{
				if ($StudentStudyTopicCountArr[$thisTopicID] > 0)
					$StudentStudyTopicMappingArr[$thisTopicID] = array();
			}
		}
		
		return $StudentStudyTopicMappingArr;
	}
	
	function Check_Is_All_Student_Study_Topic($SubjectID, $SubjectGroupID)
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		
		$ObjSubjectGroup = new subject_term_class($SubjectGroupID, $GetTeacherList=false, $GetStudentList=true);
		$StudentIDArr = Get_Array_By_Key($ObjSubjectGroup->ClassStudentList, 'UserID');
		$numOfSubjectGroupStudent = count($StudentIDArr);
		
		// $StudentNumberArr[$ModuleID][$TopicID]['NumOfStudent'] = Number Of Student Study that Topic in that Module
		$StudentNumberArr = $this->Get_Number_Of_Student_Study_Topic_Info($SubjectID, $StudentIDArr);
		
		$AllStudentStudyArr = array();
		foreach ($StudentNumberArr as $thisModuleID => $thisModuleStudentNumberArr)
		{
			foreach ($thisModuleStudentNumberArr as $thisTopicID => $thisTopicModuleStudentNumberArr)
			{
				$thisStudentNumber = $thisTopicModuleStudentNumberArr['NumOfStudent'];
				
				if ($thisStudentNumber == 0)
					$AllStudentStudyArr[$thisModuleID][$thisTopicID] = -1;
				else
					$AllStudentStudyArr[$thisModuleID][$thisTopicID] = ($thisStudentNumber == $numOfSubjectGroupStudent)? 1 : 0;
			}
		}
		
		return $AllStudentStudyArr;
	}
	
	function Get_Number_Of_Student_Study_Topic_Info($SubjectID='', $StudentIDArr='')
	{
		if ($SubjectID != '')
			$cond_SubjectID = " And SubjectID = '$SubjectID' ";
			
		if ($StudentIDArr != '')
		{
			if (!is_array($StudentIDArr))
				$StudentIDArr = array($StudentIDArr);
			$cond_StudentID = " And StudentID In (".implode(',', $StudentIDArr).") ";
		}
			
		$RC_STUDENT_STUDY_TOPIC = $this->Get_Table_Name('RC_STUDENT_STUDY_TOPIC');
		$sql = "Select 
						Count(Distinct(StudentID)) as NumOfStudent,
						ModuleID,
						TopicID 
				From 
						$RC_STUDENT_STUDY_TOPIC
				Where
						StudentID != 0 
						$cond_SubjectID
						$cond_StudentID
				Group By 
						ModuleID, TopicID
				";
		$ResultArr = $this->returnArray($sql);		
		return BuildMultiKeyAssoc($ResultArr, array('ModuleID', 'TopicID'), array('NumOfStudent')); 
	}
	
	// $StudyArr[$ModuleID][$TopicID] = 1
	function Save_Student_Study_Topic($SubjectID, $StudyArr, $SubjectGroupID, $StudentID='', $ModuleIDArr='',$TopicIDArr='')
	{
		global $PATH_WRT_ROOT;
		
		$SuccessArr = array();
		$this->Start_Trans();
		
		if ($StudentID != '')
		{
			// From Student View
			$StudentIDArr = array($StudentID);
			$TargetSubjectGroupID = '';
		}
		else
		{
			// From Subject Group View
			### Get Student From Subject Group
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$ObjSG = new subject_term_class($SubjectGroupID, $GetTeacherList=false, $GetStudentList=true);
			$StudentIDArr = Get_Array_By_Key($ObjSG->ClassStudentList, 'UserID');
			
			$TargetSubjectGroupID = $SubjectGroupID;
		}
		$numOfStudent = count($StudentIDArr);
		
		
		### Delete Old Records
		$SuccessArr['Delete_Old_Student_Study_Topic'] = $this->Delete_Student_Study_Topic($SubjectID, $StudentIDArr, $ModuleIDArr, $TargetSubjectGroupID,$TopicIDArr);
		
		### Insert New Records

		$InsertValueArr = array();
		foreach ((array)$StudyArr as $thisModuleID => $thisModuleStudyTopicArr) {
			foreach ((array)$thisModuleStudyTopicArr as $thisTopicID => $thisStudy) {
				# Insert Student Record
				for ($i=0; $i<$numOfStudent; $i++) {
					$thisStudentID = $StudentIDArr[$i];
					
					if($StudyArr[$thisModuleID][$thisTopicID]==1) {
						$InsertValueArr[] = " ('".$thisStudentID."', '$thisModuleID', '$SubjectID', '0', '$thisTopicID', now(), '".$_SESSION['UserID']."') ";
					}					
				}					
				# Insert Subject Group Record
				if ($TargetSubjectGroupID != '') {
					if($StudyArr[$thisModuleID][$thisTopicID]==1) {
						$InsertValueArr[] = " ('0', '$thisModuleID', '$SubjectID', '$SubjectGroupID', '$thisTopicID', now(), '".$_SESSION['UserID']."') ";
					}
				}						
			}
		}			
		
		if (count($InsertValueArr) > 0) {
			$InsertValueList = implode(',', $InsertValueArr);
			$RC_STUDENT_STUDY_TOPIC = $this->Get_Table_Name('RC_STUDENT_STUDY_TOPIC');
			$sql = "Insert Into $RC_STUDENT_STUDY_TOPIC
						(StudentID, ModuleID, SubjectID, SubjectGroupID, TopicID, DateModified, LastModifiedBy)
					Values
						$InsertValueList
					";
			$SuccessArr['Insert_New_Student_Study_Topic'] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	} 
	
	function Delete_Student_Study_Topic($SubjectID, $StudentIDArr, $ModuleIDArr='', $SubjectGroupID='',$TopicIDArr='')
	{		
		$SuccessArr = array();
		$RC_STUDENT_STUDY_TOPIC = $this->Get_Table_Name('RC_STUDENT_STUDY_TOPIC');
		$RC_STUDENT_STUDY_TOPIC_BACKUP = $this->Get_Table_Name('RC_STUDENT_STUDY_TOPIC_BACKUP');
		
		### Copy to backup table
		$conds_StudentID = '';
		if ($StudentIDArr != '')
		{
			if (!is_array($StudentIDArr))
				$StudentIDArr = array($StudentIDArr);
			$conds_StudentID = " And StudentID In (".implode(',', $StudentIDArr).") ";
		}
			
		$conds_ModuleID = '';
		if ($ModuleIDArr != '')
		{
			if (!is_array($ModuleIDArr))
				$ModuleIDArr = array($ModuleIDArr);
			$conds_ModuleID = " And ModuleID In (".implode(',', $ModuleIDArr).") ";
		}
		
		$conds_TopicID = '';
		if ($TopicIDArr != '')
		{
			if (!is_array($TopicIDArr))
				$TopicIDArr = array($TopicIDArr);
			$conds_TopicID = " And TopicID In (".implode(',', $TopicIDArr).") ";
		}
		
		

		if ($SubjectGroupID != '')
		{
			$sql = "Insert Into $RC_STUDENT_STUDY_TOPIC_BACKUP Select * From $RC_STUDENT_STUDY_TOPIC Where SubjectID = '$SubjectID' And SubjectGroupID = '$SubjectGroupID' $conds_ModuleID $conds_TopicID";
			$SuccessArr['Backup_SubjectGroup_Study_Topic'] = $this->db_db_query($sql);
			
			$sql = "Delete From $RC_STUDENT_STUDY_TOPIC Where SubjectID = '$SubjectID' And SubjectGroupID = '$SubjectGroupID' $conds_ModuleID $conds_TopicID";
			$SuccessArr['Delete_SubjectGroup_Study_Topic'] = $this->db_db_query($sql);
		}
	
		### Move to Backup Table
		$sql = "Insert Into $RC_STUDENT_STUDY_TOPIC_BACKUP Select * From $RC_STUDENT_STUDY_TOPIC Where SubjectID = '$SubjectID' $conds_ModuleID $conds_StudentID $conds_TopicID";
		$SuccessArr['Backup_Student_Study_Topic'] = $this->db_db_query($sql);
	
		### Delete the records in the real in-use table
		$sql = "Delete From $RC_STUDENT_STUDY_TOPIC Where SubjectID = '$SubjectID' $conds_ModuleID $conds_StudentID $conds_TopicID";
		$SuccessArr['Delete_Student_Study_Topic'] = $this->db_db_query($sql);
	
		return !in_array(false, $SuccessArr);
	}
	
	function Get_Subject_Rubrics_Mapping($SubjectIDArr, $YearID)
	{
		if (!is_array($SubjectIDArr))
			$SubjectIDArr = array($SubjectIDArr);
		$SubjectIDList = implode(',', $SubjectIDArr);
		
		$RC_RUBRCIS = $this->Get_Table_Name('RC_RUBRICS');
		$RC_RUBRCIS_ITEM = $this->Get_Table_Name('RC_RUBRICS_ITEM');
		$RC_RUBRICS_SUBJECT_MAPPING = $this->Get_Table_Name('RC_RUBRICS_SUBJECT_MAPPING');
		$sql = "Select 
						rsm.SubjectID,
						rsm.RubricsID,
						rsm.YearID,
						ri.RubricsItemID,
						ri.Level,
						ri.LevelNameEn,
						ri.LevelNameCh
				From
						$RC_RUBRICS_SUBJECT_MAPPING as rsm
						Inner Join
						$RC_RUBRCIS as r
						On (rsm.RubricsID = r.RubricsID)
						Inner Join
						$RC_RUBRCIS_ITEM as ri
						On (r.RubricsID = ri.RubricsID)
				Where
						ri.RecordStatus = 1
						And
						rsm.SubjectID In ($SubjectIDList)
						And
						(rsm.YearID = '$YearID' || rsm.YearID = 0)
				Order By
						ri.Level Desc
				";
		$RubricsInfoArr = $this->returnArray($sql);
		$numOfRubricsInfo = count($RubricsInfoArr);
		
		$ReturnArr = array();
		for ($i=0; $i<$numOfRubricsInfo; $i++)
		{
			$thisSubjectID = $RubricsInfoArr[$i]['SubjectID'];
			$thisRubricsID = $RubricsInfoArr[$i]['RubricsID'];
			$thisYearID = $RubricsInfoArr[$i]['YearID'];
			$thisRubricsItemID = $RubricsInfoArr[$i]['RubricsItemID'];
			
			$ReturnArr[$YearID][$thisSubjectID][$thisRubricsID][$thisRubricsItemID] = $RubricsInfoArr[$i];
		}
		return $ReturnArr;
	}
	
	function Get_Student_Class_Info($StudentID)
	{
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		$cond_StudentID = " ycu.UserID IN (".implode(",", (array)$StudentID).") "; 
		
		$sql = "Select
						y.YearID,
						y.YearName,
						yc.YearClassID,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						ycu.ClassNumber,
						ycu.UserID,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('<font style=\"color:red;\">*</font>', ".$ArchiveNameField.") 
							ELSE ".$NameField." 
						END as StudentName 
				From
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join
						YEAR as y On (yc.YearID = y.YearID)
						LEFT JOIN 
						INTRANET_USER as u on (ycu.UserID = u.UserID)
						LEFT JOIN 
						INTRANET_ARCHIVE_USER as au on (ycu.UserID = au.UserID)
				Where
						$cond_StudentID
						And
						yc.AcademicYearID = '".$this->AcademicYearID."'
				Order By
						y.Sequence, yc.Sequence, ycu.ClassNumber
				";
		return $this->returnArray($sql);
	}
	
	function Get_Student_Marksheet_Score($SubjectIDArr='', $StudentIDArr='', $ModuleIDArr='')
	{
		$conds_SubjectID = '';
		if ($SubjectIDArr != '')
		{
			if (!is_array($SubjectIDArr))
				$SubjectIDArr = array($SubjectIDArr);
			$conds_SubjectID = " And SubjectID In (".implode(',', $SubjectIDArr).") ";
		}
		
		$conds_StudentID = '';
		if ($StudentIDArr != '')
		{
			if (!is_array($StudentIDArr))
				$StudentIDArr = array($StudentIDArr);
			$conds_StudentID = " And StudentID In (".implode(',', $StudentIDArr).") ";
		}
		
		$conds_ModuleID = '';
		if ($ModuleIDArr != '')
		{
			if (!is_array($ModuleIDArr))
				$ModuleIDArr = array($ModuleIDArr);
			$conds_ModuleID = " And ModuleID In (".implode(',', $ModuleIDArr).") ";
		}
		
		$RC_MARKSHEET_SCORE = $this->Get_Table_Name('RC_MARKSHEET_SCORE');
		
		$sql = "Select
						*
				From
						$RC_MARKSHEET_SCORE
				Where
						1
						$conds_SubjectID
						$conds_StudentID
						$conds_ModuleID
				";
		$MarksheetScoreArr = $this->returnArray($sql);
		$numOfMarksheet = count($MarksheetScoreArr);
		
		$ReturnArr = array();
		for ($i=0; $i<$numOfMarksheet; $i++)
		{
			$thisStudentID = $MarksheetScoreArr[$i]['StudentID'];
			$thisSubjectID = $MarksheetScoreArr[$i]['SubjectID'];
			$thisTopicID = $MarksheetScoreArr[$i]['TopicID'];
			$thisModuleID = $MarksheetScoreArr[$i]['ModuleID'];
			
			$ReturnArr[$thisStudentID][$thisSubjectID][$thisTopicID][$thisModuleID] = $MarksheetScoreArr[$i];
		}
		return $ReturnArr;
	}
	
	// $SubjectLevelArr[$ModuleID][$TopicID] = $Level
	function Save_Student_Marksheet_Score($SubjectID, $StudentID, $RubricsID, $SubjectLevelArr, $SubjectLevelRemarkArr)
	{
		global $PATH_WRT_ROOT;
		$SuccessArr = array();
		
		### For Logging
		// $FromMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = InfoArr;
		$FromMarksheetScoreArr = $this->Get_Student_Marksheet_Score($SubjectID, $StudentID);
		
		### Get Rubrics Item Info
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_rubrics.php');
		$librubrics = new libreportcardrubrics_rubrics();
		$RubricsItemInfoArr = $librubrics->Get_Rubics_Item_Info($RubricsID);
		$numOfRubricsItem = count($RubricsItemInfoArr);
		
		$RubricsItemAssoArr = array();
		for ($i=0; $i<$numOfRubricsItem; $i++)
		{
			$thisRubricsItemID = $RubricsItemInfoArr[$i]['RubricsItemID'];
			$RubricsItemAssoArr[$thisRubricsItemID] = $RubricsItemInfoArr[$i];
		}
		
		### Change Score
		$this->Start_Trans();
		
		$RC_MARKSHEET_SCORE = $this->Get_Table_Name('RC_MARKSHEET_SCORE');
		foreach ((array)$SubjectLevelArr as $thisModuleID => $thisModuleSubjectLevelArr)
		{
			foreach ((array)$thisModuleSubjectLevelArr as $thisTopicID => $thisRubricsItemID)
			{
				$thisLevel = $RubricsItemAssoArr[$thisRubricsItemID]['Level'];
				$thisRemark = $this->Get_Safe_Sql_Query(stripslashes($SubjectLevelRemarkArr[$thisModuleID][$thisTopicID]));
				
				$sql = "Insert Into $RC_MARKSHEET_SCORE 
							(StudentID, ModuleID, SubjectID, TopicID, RubricsID, RubricsItemID, Level, Remarks, DateInput, InputBy, DateModified, LastModifiedBy) 
						Values 
							('$StudentID', '$thisModuleID', '$SubjectID', '$thisTopicID', '$RubricsID', '$thisRubricsItemID', '$thisLevel', '$thisRemark', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') 
						On Duplicate Key Update 
							RubricsID = '$RubricsID', RubricsItemID = '$thisRubricsItemID', Level = '$thisLevel', Remarks = '$thisRemark', DateModified = NOW(), LastModifiedBy = '".$_SESSION['UserID']."'
						";
				
				$SuccessArr['Save_MarksheetScore'][$thisTopicID][$thisModuleID] = $this->db_db_query($sql);
				
			}
		}
		
		
		### Add Log
		if (!in_array(false, $SuccessArr))
		{
			$InsertLogArr = array();
			foreach ((array)$SubjectLevelArr as $thisModuleID => $thisModuleSubjectLevelArr)
			{
				foreach ((array)$thisModuleSubjectLevelArr as $thisTopicID => $thisRubricsItemID)
				{
					$thisFromRubricsItemID = $FromMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
					$thisFromRubricsID = $FromMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsID'];
					
					$thisFromLevel = $RubricsItemAssoArr[$thisFromRubricsItemID]['Level'];
					$thisToLevel = $RubricsItemAssoArr[$thisRubricsItemID]['Level'];

					$thisFromRemarks =  $this->Get_Safe_Sql_Query($FromMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['Remarks']);
					$thisToRemarks = $this->Get_Safe_Sql_Query(stripslashes($SubjectLevelRemarkArr[$thisModuleID][$thisTopicID]));
					
					$InsertLogArr[] = " (
											'$StudentID', '$thisModuleID', '$SubjectID', '$thisTopicID', '$thisFromRubricsID', '$RubricsID', 
											'$thisFromRubricsItemID', '$thisRubricsItemID', '$thisFromLevel', '$thisToLevel', '$thisFromRemarks', '$thisToRemarks', now(), '".$_SESSION['UserID']."'
										) ";
				}
			} 
			
			if ($InsertLogArr > 0)
			{
				$InsertLogList = implode(',', $InsertLogArr);
				$RC_MARKSHEET_SCORE_LOG = $this->Get_Table_Name('RC_MARKSHEET_SCORE_LOG');
				$sql = "Insert Into $RC_MARKSHEET_SCORE_LOG
							(	
								StudentID, ModuleID, SubjectID, TopicID, FromRubricsID, ToRubricsID, 
								FromRubricsItemID, ToRubricsItemID,	FromLevel, ToLevel, FromRemarks, ToRemarks, DateModified, LastModifiedBy
							)
						Values
							$InsertLogList
						";
				$SuccessArr['Insert_Log'] = $this->db_db_query($sql);
			}
		}

		if (in_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Get_Student_Marksheet_Import_Export_Column_Title_Arr()
	{
	    global $Lang;
	    
	    $titleArr = array();
	    foreach($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader'] as $thisHeaderName) {
            $titleArr[] = $thisHeaderName;
        }
	    
	    //$titleRowArr = array();
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['TopicCode'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['TopicNameEn'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['ObjectiveCode'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['ObjectiveNameEn'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['DetailsCode'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['DetailsNameEn'];
	    //$titleArr[] = $titleRowArr;
	    //
	    //$titleRowArr = array();
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleArr[] = $titleRowArr;
	    //$titleArr[] = $titleRowArr;
	    
	    return $titleArr;
	}
	
	function Delete_Import_Temp_Marksheet()
	{
	    $TEMP_IMPORT_TOPIC = $this->Get_Table_Name('TEMP_IMPORT_MARKSHEET_SCORE');
	    $sql = "Delete From $TEMP_IMPORT_TOPIC Where ImportUserID = '".$_SESSION["UserID"]."'";
	    $Success = $this->db_db_query($sql);
	    
	    return $Success;
	}
	
	function InsertReportTemplate($ReportTitleEn, $ReportTitleCh, $ReportType)
	{
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		$sql = "
			INSERT INTO 
				$RC_REPORT_TEMPLATE
				(
					ReportTitleEn,
					ReportTitleCh,
					ReportType,
					RecordStatus,
					DateInput,
					InputBy,
					DateModified,
					LastModifiedBy
				)
			VALUES
				(
					'".$this->Get_Safe_Sql_Query($ReportTitleEn)."',
					'".$this->Get_Safe_Sql_Query($ReportTitleCh)."',
					'$ReportType',
					1,
					NOW(),
					'".$_SESSION['UserID']."',
					NOW(),
					'".$_SESSION['UserID']."'
				)				
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?mysql_insert_id():false;
		
	}
	
	function UpdateReportTemplate($ReportID, $ReportTitleEn, $ReportTitleCh)
	{
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		$sql = "
			UPDATE
				$RC_REPORT_TEMPLATE
			SET
				ReportTitleEn = '".$this->Get_Safe_Sql_Query($ReportTitleEn)."',
				ReportTitleCh = '".$this->Get_Safe_Sql_Query($ReportTitleCh)."',
				DateModified = NOW(),
				LastModifiedBy = '".$_SESSION['UserID']."'
			WHERE
				ReportID = '$ReportID'	
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
	}
	
	function DeleteReportTemplate($ReportID)
	{
		
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		foreach((array)$ReportID as $thisReportID)
		{
			$sql = "
				UPDATE 
					$RC_REPORT_TEMPLATE
				SET 
					RecordStatus = 0
				WHERE
					ReportID = '$thisReportID'; 
			";
			
			$result[] = $this->db_db_query($sql);
		}
		
		return in_array(false,$result)?false:true;
	}
	
	function InsertReportTemplateModuleMapping($ReportID,$ModuleIDArr)
	{
		foreach((array)$ModuleIDArr as $thisModuleID)
			$thisMappingSql[] = "('$ReportID','$thisModuleID',NOW(),'".$_SESSION['UserID']."')";

		$AllMappingSql = implode(",",$thisMappingSql);
		
		$RC_REPORT_TEMPLATE_MODULE_MAPPING = $this->Get_Table_Name('RC_REPORT_TEMPLATE_MODULE_MAPPING');
		$sql = "
			INSERT INTO 
				$RC_REPORT_TEMPLATE_MODULE_MAPPING 
					(
						ReportID,
						ModuleID,
						DateModified,
						LastModifiedBy
					)
				VALUES
					$AllMappingSql
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
		
	}
	
	function GetReportTemplateDBTableSql()
	{
		global $Lang;
		
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		$RC_REPORT_TEMPLATE_MODULE_MAPPING = $this->Get_Table_Name('RC_REPORT_TEMPLATE_MODULE_MAPPING');
		$RC_MODULE = $this->Get_Table_Name('RC_MODULE');
		
		$ChEn = Get_Lang_Selection("Ch","En");
		
		$sql = "
			SELECT 
				CONCAT('<a href=\"edit.php?ReportID=',RT.ReportID,'\">',RT.ReportTitle$ChEn,'</a>') AS ReportEdit,
				CASE RT.ReportType
					WHEN 'W' THEN '".$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ConsolidatedReport']."'
					WHEN 'T' THEN '".$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ModuleReport']."'
				END As ReportType,
				GROUP_CONCAT(
					RM.ModuleName$ChEn
					ORDER BY RT.ReportID, RM.StartDate
					SEPARATOR '<br />'
				) As Module,
				RT.DateModified,
				CONCAT('<input type=\"checkbox\" name=\"ReportID[]\" value=\"',RT.ReportID,'\">') as CheckBox,
				RT.ReportTitle$ChEn AS ReportTitle,
				CASE RT.ReportType
					WHEN 'W' THEN 2
					WHEN 'T' THEN 1
				END As ReportTypeDisplayOrder
			FROM 
				$RC_REPORT_TEMPLATE RT 
				INNER JOIN $RC_REPORT_TEMPLATE_MODULE_MAPPING RTM ON RT.ReportID = RTM.ReportID
				INNER JOIN $RC_MODULE RM ON RTM.ModuleID = RM.ModuleID
			WHERE
				RT.RecordStatus = 1
			GROUP BY  
				RTM.ReportID				
		";				
		return $sql;
	}
	
	function GetReportTemplateInfo($ReportID='')
	{
		if($ReportID!='')
			$cond_ReportID = " AND RT.ReportID = '$ReportID' ";
		
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		$RC_REPORT_TEMPLATE_MODULE_MAPPING = $this->Get_Table_Name('RC_REPORT_TEMPLATE_MODULE_MAPPING');
		$RC_MODULE = $this->Get_Table_Name('RC_MODULE');
		$sql = "
			SELECT 
				RT.ReportTitleEn, 
				RT.ReportTitleCh, 
				RT.ReportType,
				RT.ReportID,
				CASE RT.ReportType
					WHEN 'W' THEN 2
					WHEN 'T' THEN 1
				END As ReportTypeDisplayOrder,
				Min(RM.StartDate) as ModuleFirstStartDate
			FROM 
				$RC_REPORT_TEMPLATE RT 
				Inner Join
				$RC_REPORT_TEMPLATE_MODULE_MAPPING RTMM
				On (RT.ReportID = RTMM.ReportID)
				Inner Join
				$RC_MODULE RM
				On (RTMM.ModuleID = RM.ModuleID)
			WHERE 
				RT.RecordStatus = 1
				$cond_ReportID
			Group By
				RT.ReportID
			Order By
				ReportTypeDisplayOrder, ModuleFirstStartDate
		";
		
		$result = $this->returnArray($sql);
		
		return $result;
	}
	
	function GetReportTemplateModuleInfo($ReportID)
	{
		if($ReportID!='')
			$cond_ReportID = " AND RT.ReportID = '$ReportID' ";

		$RC_REPORT_TEMPLATE_MODULE_MAPPING = $this->Get_Table_Name('RC_REPORT_TEMPLATE_MODULE_MAPPING');
		$RC_MODULE = $this->Get_Table_Name('RC_MODULE');
		$sql = "
			SELECT
				RM.ModuleID, 
				RM.ModuleCode,
				RM.ModuleNameEn, 
				RM.ModuleNameCh,
				RM.StartDate,
				RM.EndDate,
				RM.YearTermID
			FROM 
				$RC_REPORT_TEMPLATE_MODULE_MAPPING RTM 	
				INNER JOIN $RC_MODULE RM ON RTM.ModuleID = RM.ModuleID
			WHERE 
				RTM.ReportID = '$ReportID'	
		";
		
		$result = $this->returnArray($sql);
		
		return $result;
	}
	
	function Get_Report_Template_Settings($SettingsID='')
	{
		$conds_SettingsID = '';
		if ($SettingsID != '')
			$conds_SettingsID = " And SettingsID = '$SettingsID' ";
		
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "Select
						SettingsID,
						SettingsCode,
						SettingsNameEn,
						SettingsNameCh,
						DisplayOrder,
						SettingsValue
				From
						$RC_REPORT_TEMPLATE_SETTINGS
				Where
						RecordStatus = 1
						$conds_SettingsID
				Order By
						DisplayOrder
				";
		return $this->returnArray($sql);
	}
	
	function Get_Report_Template_Settings_Max_Display_Order()
	{
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "SELECT Max(DisplayOrder) FROM $RC_REPORT_TEMPLATE_SETTINGS Where RecordStatus = 1";
		$MaxDisplayOrder = $this->returnVector($sql);
		
		return $MaxDisplayOrder[0];
	}
	
	function Get_Report_Template_Settings_Last_Modified_Info()
	{
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "Select 
						DateModified, LastModifiedBy
				From
						$RC_REPORT_TEMPLATE_SETTINGS
				Where
						DateModified IN 
								(SELECT MAX(DateModified) FROM $RC_REPORT_TEMPLATE_SETTINGS Where RecordStatus = 1)
				";
		return $this->returnArray($sql);
	}
	
	function Is_Report_Template_Settings_Code_Valid($Value, $ExcludeSettingsID='')
	{
		$Value = $this->Get_Safe_Sql_Query(trim($Value));
		
		$conds_ExcludeSettingsID = '';
		if ($ExcludeSettingsID != '')
			$conds_ExcludeSettingsID = " And SettingsID != '$ExcludeSettingsID' ";
		
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "Select
						SettingsID
				From
						$RC_REPORT_TEMPLATE_SETTINGS
				Where
						SettingsCode = '$Value'
						And
						RecordStatus = 1
						$conds_ExcludeSettingsID
				";
		$ResultArr = $this->returnArray($sql);
		return (count($ResultArr) > 0)? false : true;
	}
	
	function Insert_Report_Template_Settings($DataArr)
	{
		if (!is_array($DataArr) || count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# RecordStatus
		$fieldArr[] = 'RecordStatus';
		$valueArr[] = '1';
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# LastModifiedBy
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "Insert Into $RC_REPORT_TEMPLATE_SETTINGS ($fieldText) Values ($valueText)";
		$success = $this->db_db_query($sql);
		
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			$this->Commit_Trans();
			
			return $insertedID;
		}
	}
	
	function Update_Report_Template_Settings($SettingsID, $DataArr, $UpdateLastModified=1)
	{
		if (!is_array($DataArr) || count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $field => $value)
		{
			$valueFieldArr[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
		}
		
		if ($UpdateLastModified==1)
		{
			$valueFieldArr[] = " DateModified = now() ";
			$valueFieldArr[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
		}
		
		$valueFieldText .= implode(',', $valueFieldArr);
		
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "Update $RC_REPORT_TEMPLATE_SETTINGS Set $valueFieldText Where SettingsID = '$SettingsID'";
		
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function Delete_Report_Template_Settings($SettingsID)
	{
		$RC_REPORT_TEMPLATE_SETTINGS = $this->Get_Table_Name('RC_REPORT_TEMPLATE_SETTINGS');
		$sql = "Delete From $RC_REPORT_TEMPLATE_SETTINGS Where SettingsID = '$SettingsID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function Check_Within_DateRange($StartDate, $EndDate)
	{
		if ($StartDate=='' || $EndDate=='')
			return false;
		
		$CurrentDate = date('Y-m-d');
		if ($StartDate <= $CurrentDate && $CurrentDate <= $EndDate)
			return true;
		else
			return false;
	}
	
	# Other Information
	function Get_Other_Info_Category($CategoryID='')
	{
		global $ReportCard_Rubrics_CustomSchoolName;
		
		if(trim($CategoryID)!='')
			$cond_CategoryID = " AND CategoryID = '$CategoryID' "; 
		
		$CategoryCodeArr = "'".implode("','",(array)$this->OtherInfoTypeArr)."'";
		
		$RC_OTHER_INFO_CATEGORY = $this->Get_Table_Name('RC_OTHER_INFO_CATEGORY');
		$sql = "
			SELECT
				CategoryID,
				CategoryCode,
				CategoryNameEn,
				CategoryNameCh
			FROM
				$RC_OTHER_INFO_CATEGORY
			WHERE
				SchoolName = '$ReportCard_Rubrics_CustomSchoolName'
				AND CategoryCode IN ($CategoryCodeArr)
				$cond_CategoryID
		";
		
		$result = $this->returnArray($sql);

		# sort by OtherInfoTypeArr
		$result = BuildMultiKeyAssoc($result, "CategoryCode");
		foreach($this->OtherInfoTypeArr as $type)
			$ReturnArr[] = $result[$type];

		return $ReturnArr;
	}

	# Class Teacher Comment (Copy from libreportcard)
	function returnClassTeacherForm($ParUserID)
	{
		$sql = "SELECT
						DISTINCT(y.YearID) as ClassLevelID,
						y.YearName as LevelName,
						y.FormStageID
				FROM
						YEAR_CLASS_TEACHER as yct
						INNER JOIN
						YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."')
						INNER JOIN
						YEAR as y ON (yc.YearID = y.YearID)
				Where
						yct.UserID = '".$ParUserID."'
				Order By
						y.Sequence
				";
		return $this->returnArray($sql);
	}
	
	function returnClassTeacherClass($UserID, $ClassLevelID='', $ClassName='', $YearClassID='')
	{
		$con = $ClassLevelID != "" ? " and y.YearID IN ('".implode("','",$ClassLevelID)."') " : "";
		$con .= $ClassName != "" ? " and yc.ClassTitleEn = '$ClassName' " : "";
		$con .= $YearClassID != "" ? " and yc.YearClassID = '$YearClassID' " : "";
		
	 	$sql = "SELECT
	 					yc.YearClassID as ClassID,
	 					yc.ClassTitleEn as ClassName,
	 					y.YearID as ClassLevelID,
						y.YearName
	 			FROM
	 					YEAR_CLASS_TEACHER as yct
	 					INNER JOIN
	 					YEAR_CLASS as yc
	 					ON (yct.YearClassID = yc.YearClassID AND yct.UserID = '$UserID' AND yc.AcademicYearID = '".$this->AcademicYearID."')
	 					INNER JOIN
	 					YEAR as y
	 					ON (yc.YearID = y.YearID)
	 			WHERE
	 					1
	 					$con
				Order By
						y.Sequence, yc.Sequence
	 			";
		return $this->returnArray($sql);
	}
	
	function GET_CLASSES_BY_FORM($ParClassLevelID="", $ClassID="", $returnAssoName=0, $returnAssoInfo=0)
	{
		if ($_SESSION['intranet_session_language'] == 'en')
			$titleField = 'yc.ClassTitleEN';
		else
			$titleField = 'yc.ClassTitleB5';
			
		$formCond = '';
		if ($ParClassLevelID != '')
			$formCond = " AND yc.YearID IN ('".implode("','",(array)$ParClassLevelID)."') ";
			
		$classCond = '';
		if ($ClassID != '')
			$classCond = " AND yc.YearClassID = '$ClassID' ";
		
		//add ClassGroupID by Marcus 20091127 (for Stream ordering)
		$sql= "SELECT 
						yc.YearClassID as ClassID, 
						$titleField as ClassName,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.ClassGroupID,
						yc.YearID as ClassLevelID,
						y.YearName
				From 
						YEAR_CLASS yc
						LEFT JOIN YEAR y ON y.YearID = yc.YearID 
				WHERE 
						yc.AcademicYearID = '".$this->AcademicYearID."'
						$formCond
						$classCond
				ORDER BY
						yc.YearID, yc.Sequence
				";
		$return = $this->returnArray($sql, 2);
		
		$returnArr = array();
		if ($returnAssoName == 1)
			$returnArr = build_assoc_array($return, "---");
		else if ($returnAssoInfo == 1)
		{
			foreach ((array)$return as $key => $ValueArr)
			{
				$thisClassID = $ValueArr['ClassID'];
				$returnArr[$thisClassID] = $ValueArr;
			}
		}
		else
			$returnArr = $return;

		return $returnArr;
	}
	
	function GET_STUDENT_BY_CLASS($ParClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=0)
	{
		$cond = ($ParStudentIDList!="") ? " AND u.UserID IN ($ParStudentIDList)" : "";
		
		if ($withClassNumber == 1)
		{
			$NameField = getNameFieldWithClassNumberByLang('u.');
			$ArchiveNameField = getNameFieldWithClassNumberByLang('au.');
		}
		else
		{
			if ($isShowBothLangs)
			{
				$NameField = "	CONCAT(	TRIM(u.EnglishName), 
										If (
											u.ChineseName Is Not Null Or u.ChineseName != '',
											Concat(' (', TRIM(u.ChineseName), ')'),
											''
										)
								)
							";
				$ArchiveNameField = "	CONCAT(	TRIM(au.EnglishName), 
												If (
													au.ChineseName Is Not Null Or au.ChineseName != '',
													Concat(' (', TRIM(au.ChineseName), ')'),
													''
												)
										)
									";
			}
			else
			{
				$NameField = getNameFieldByLang2("u.");
				$ArchiveNameField = getNameFieldByLang2("au.");
			}
			//$NameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("u.") : "CONCAT(TRIM(u.EnglishName), ' (', TRIM(u.ChineseName), ')')";
			//$ArchiveNameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("au.") : "CONCAT(TRIM(au.EnglishName), ' (', TRIM(au.ChineseName), ')')";
		}
		
		
		
		if ($isShowStyle==0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
			
		$sql = "SELECT 
						DISTINCT(ycu.UserID), 
						u.WebSAMSRegNo, 
						ycu.ClassNumber as ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
							ELSE ".$NameField." 
						END as StudentName,
						yc.ClassTitleEN as ClassName,
						u.EnglishName as StudentNameEn,
						u.ChineseName as StudentNameCh,
						yc.ClassTitleEN as ClassTitleEn,
						yc.ClassTitleB5 as ClassTitleCh,
						u.WebSAMSRegNo,
						u.UserLogin,
						u.Gender
				FROM 
						YEAR_CLASS_USER as ycu
						INNER JOIN 
						YEAR_CLASS as yc
						ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."')
						INNER JOIN
						INTRANET_USER as u 
						ON (ycu.UserID = u.UserID)
						Left Join
						INTRANET_ARCHIVE_USER as au 
						ON (u.UserID = au.UserID) 
				WHERE 
						yc.YearClassID = '$ParClassID'
						$cond
				ORDER BY 
						yc.Sequence,
						ycu.ClassNumber 
				";
		$row_student = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1)
		{
			foreach ($row_student as $key => $thisStudentInfoArr)
			{
				$thisStudentID = $thisStudentInfoArr['UserID'];
				$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
			}
		}
		else
		{
			$ReturnArr = $row_student;
		}
		
		return $ReturnArr;
	}
	
	# Class Teacher Comment (New)
	function GET_ALL_FORMS() {

			$sql = "SELECT 
						YearID as ClassLevelID, 
						YearName as LevelName,
						FormStageID
					FROM 
						YEAR 
					ORDER BY 
						Sequence
					";
		
		$row = $this->returnArray($sql, 2);
		
		return $row;
	}	
	
	function Get_All_Form_Stage()
	{
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libformstage.php');
		$libformstage = new Form_Stage();
		
		return $libformstage->Get_All_Form_Stage($Active=1);
	}
	
	function Get_ClassLevel_By_ClassID($ParClassID)
	{
		include_once("form_class_manage.php");
		$objYearClass = new year_class($ParClassID, true);
		
		return $objYearClass->YearID;
	}
	
	function returnClassLevel($ClassLevelID)
	{
		include_once("form_class_manage.php");
		$objYear = new year($ClassLevelID);
		
		return $objYear->Get_Year_Name();
		/*
		$sql = "SELECT 
						YearName as LevelName
				FROM 
						YEAR 
				WHERE
						YearID = '$ClassLevelID'
				ORDER BY 
						Sequence
				";
		$result = $this->returnVector($sql);
		
		return $result[0];
		*/
	}
	
	function Get_User_Accessible_Class($ClassLevelID='')
	{
		global $ck_ReportCard_Rubrics_UserType;
		
		$CurrentPage = $this->Get_Current_Page_Session();
		$IsAdmin = $this->Is_Admin_User();
		$CanAccessPage = $this->Has_Page_Access_Right($CurrentPage);
		if ($IsAdmin || $CanAccessPage)
		{
			$ClassArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		}
		else if($ck_ReportCard_Rubrics_UserType=="TEACHER")
		{
			$ClassArr = $this->returnClassTeacherClass($_SESSION['UserID'],$ClassLevelID);
		}
		
		return $ClassArr;
		
	}
	
	function Get_User_Accessible_Form_Stage()
	{
		global $ck_ReportCard_Rubrics_UserType, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libformstage.php');
		$libformstage = new Form_Stage();
		
		$CurrentPage = $this->Get_Current_Page_Session();
		$IsAdmin = $this->Is_Admin_User();
		$CanAccessPage = $this->Has_Page_Access_Right($CurrentPage);
		
		if ($IsAdmin || $CanAccessPage)
		{
			$FormArr = $this->GET_ALL_FORMS();
		}
		else if($ck_ReportCard_Rubrics_UserType=="TEACHER")
		{
			$FormArr = $this->returnClassTeacherForm($_SESSION['UserID']);
		}
		
		$FormStageIDArr = array_values(array_remove_empty(array_unique(Get_Array_By_Key($FormArr, 'FormStageID'))));
		$FormStageInfoArr = $libformstage->Get_All_Form_Stage($Active=1, $FormStageIDArr);
		
		return $FormStageInfoArr;
	}
	
	function Get_User_Accessible_Form()
	{
		global $ck_ReportCard_Rubrics_UserType;
		
		$CurrentPage = $this->Get_Current_Page_Session();
		$IsAdmin = $this->Is_Admin_User();
		$CanAccessPage = $this->Has_Page_Access_Right($CurrentPage);
		
		if ($IsAdmin || $CanAccessPage)
		{
			$FormArr = $this->GET_ALL_FORMS();
		}
		else if($ck_ReportCard_Rubrics_UserType=="TEACHER")
		{
			$FormArr = $this->returnClassTeacherForm($_SESSION['UserID']);
		}
		
		return $FormArr;
	}
	
	function Get_User_Accessible_Subject($YearTermIDArr='')
	{
		global $PATH_WRT_ROOT;
		
		$CurrentPage = $this->Get_Current_Page_Session();
		$IsAdmin = $this->Is_Admin_User();
		$CanAccessPage = $this->Has_Page_Access_Right($CurrentPage);
		if ($IsAdmin || $CanAccessPage)
		{
			// Get all Subjects for Admin
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$libSubject = new subject();
			$SubjectArr = $libSubject->Get_Subject_List();
		}
		else
		{
			// Get Teaching Subject only for Normal Teacher
			$SubjectArr = $this->Get_User_Teaching_Subject($YearTermIDArr);
		}
		
		return $SubjectArr;
	}
	
	function Get_User_Teaching_Subject($YearTermIDArr='')
	{
		if ($YearTermIDArr != '')
			$conds_YearTermID = " And ayt.YearTermID In (".implode(',', (array)$YearTermIDArr).") ";
			
		$sql = "Select
						Distinct(st.SubjectID)
				From
						SUBJECT_TERM_CLASS_TEACHER as stct
						Inner Join
						SUBJECT_TERM as st On (stct.SubjectGroupID = st.SubjectGroupID)
						Inner Join
						ACADEMIC_YEAR_TERM as ayt On (st.YearTermID = ayt.YearTermID)
				Where
						stct.UserID = '".$_SESSION['UserID']."'
						And
						ayt.AcademicYearID = '".$this->AcademicYearID."'
						$conds_YearTermID
				";
		return $this->returnArray($sql);
	}
	
	function Get_User_Page_Access_Right($ParUserID, $FromSession=1)
	{
		if ($FromSession)
		{
			return $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["PageAccessRightArr"];
		}
		else
		{
			$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP');
			$RC_ADMIN_GROUP_USER = $this->Get_Table_Name('RC_ADMIN_GROUP_USER');
			$RC_ADMIN_GROUP_RIGHT = $this->Get_Table_Name('RC_ADMIN_GROUP_RIGHT');
			
			$sql = "Select
							agr.AdminGroupRightName
					From
							$RC_ADMIN_GROUP_USER as agu
							Inner Join
							$RC_ADMIN_GROUP as ag On (ag.AdminGroupID = agu.AdminGroupID)
							Inner Join
							$RC_ADMIN_GROUP_RIGHT as agr On (ag.AdminGroupID = agr.AdminGroupID)
					Where
							agu.UserID = '".$ParUserID."'
							And ag.RecordStatus = 1
					";
			return $this->returnArray($sql);
		}
	}
	
	function GET_STUDENT_BY_CLASSLEVEL($ParClassLevelIDArr, $ParClassID="", $ParStudentIDArr="", $ReturnAsso=0, $isShowStyle=0, $CheckActiveStudent=1)
	{
		
		if($ParStudentIDArr!="") {
			if (!is_array($ParStudentIDArr))
				$ParStudentIDArr = array($ParStudentIDArr);
			$cond = " AND ycu.UserID IN (".implode(',', $ParStudentIDArr).")";
		} else if($ParClassID!="") {
			if(is_array($ParClassID))
			{
				$ClassIDStr = implode(",",$ParClassID);
				$cond = " AND yc.YearClassID IN ($ClassIDStr)";
			}
			else
				$cond = " AND yc.YearClassID = '$ParClassID'";
		}
		
		//$ClassNumField = getClassNumberField("ycu.");
		$NameField = getNameFieldByLang2('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		if ($CheckActiveStudent == 1)
		{
			if ($isShowStyle==0)
				$starHTML = '*';
			else
				$starHTML = '<font style="color:red;">*</font>';
		}
					
		$sql = "SELECT 
						DISTINCT(ycu.UserID), 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
							ELSE ".$NameField." 
						END as StudentName,
						yc.ClassTitleEN as ClassTitleEn,
						yc.ClassTitleB5 as ClassTitleCh,
						yc.YearClassID,
						u.WebSAMSRegNo,
						u.UserLogin,
						y.YearName,
						u.EnglishName as StudentNameEn,
						u.ChineseName as StudentNameCh,
						u.Gender
				FROM 
						YEAR as y
						Inner Join
						YEAR_CLASS as yc
						On (y.YearID = yc.YearID)
						INNER JOIN
						YEAR_CLASS_USER as ycu 
						ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."')
						LEFT JOIN 
						INTRANET_USER as u 
						ON (ycu.UserID = u.UserID)
						LEFT Join
						INTRANET_ARCHIVE_USER as au 
						ON (ycu.UserID = au.UserID) 
				WHERE 
						yc.YearID IN (".implode(",", (array)$ParClassLevelIDArr).")
						$cond
				ORDER BY
						yc.Sequence, 
						ycu.ClassNumber
				";
		$row_student = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1)
		{
			foreach ($row_student as $key => $thisStudentInfoArr)
			{
				$thisStudentID = $thisStudentInfoArr['UserID'];
				$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
			}
		}
		else
		{
			$ReturnArr = $row_student;
		}
		
		return $ReturnArr;
	}
	
	function Get_User_Array_From_Selection($SelectionArr, $YearID)
    {
    	global $PATH_WRT_ROOT, $ck_ReportCard_Rubrics_UserType;
    	include_once($PATH_WRT_ROOT.'includes/libgrouping.php');
    	
    	$UserIDArr = array();
    	$libgrouping = new libgrouping();
    	$OwnClassStudentOnly = ($ck_ReportCard_Rubrics_UserType=="ADMIN")? 0 : 1;
    	
    	foreach((array)$SelectionArr as $key => $value)
		{
			$type = substr($value, 0, 1);	// U: User, G: Group, C: Class 
			$type_id = substr($value, 1);
			switch($type)
			{
				case "U":
					$UserIDArr[] = $type_id;
					break;
				case "G":
				case "C":
					//$objGroup = new libgroup($type_id);
					//$GroupMemberID = $objGroup->Get_Group_User($UserType=2);
					//$temp = $libgrouping->returnGroupUsersInIdentity($groupIDAry, $PermittedUserTypeArr);
					
					$GroupMemberInfoArr = $libgrouping->returnGroupUsersInIdentity(array($type_id), array($PermittedUserType=2), $YearID, $OwnClassStudentOnly);
					foreach($GroupMemberInfoArr as $k1 => $d1)
						$UserIDArr[] = $d1['UserID'];
					
					break;
				default:
					$UserIDArr[] = $value;
					break;
			}
		}	
		
		$UserIDArr = array_unique($UserIDArr);
		return $UserIDArr;
    }
    
    function Get_Available_User_To_Add_Comment($YearID='', $ExcludeUserIDArr='', $UserLogin='', $ClassName='', $ClassNumber='', $StudentName='', $TeachingClassOnly=0)
    {
    	# Exclude Student condition
    	$ExcludeUserIDArr = array_remove_empty($ExcludeUserIDArr);
		if (is_array($ExcludeUserIDArr) && count($ExcludeUserIDArr) > 0)
		{
			$excludeUserList = implode(',', $ExcludeUserIDArr);
			$conds_excludeUserID = " And u.UserID Not In ($excludeUserList) ";
		}
		
		# Student form condition
		$conds_YearID = '';
		if ($YearID != '')
			$conds_YearID = " And yc.YearID = '".$YearID."' ";
			
		# Student Login condition
		$conds_UserLogin = '';
		if ($UserLogin != '')
			$conds_UserLogin = " And u.UserLogin Like '%".$this->Get_Safe_Sql_Like_Query($UserLogin)."%' ";
			
		# Student class condition
		$conds_ClassName = '';
		if ($ClassName != '')
			$conds_ClassName = " And	(
											yc.ClassTitleEN Like '%".$this->Get_Safe_Sql_Like_Query($ClassName)."%'
											Or
											yc.ClassTitleB5 Like '%".$this->Get_Safe_Sql_Like_Query($ClassName)."%' 
										)
								";
			
		# Student class number condition
		$conds_ClassNumber = '';
		if ($ClassNumber != '')
			$conds_ClassNumber = " And ycu.ClassNumber = '".$this->Get_Safe_Sql_Query($ClassNumber)."' ";
			
		# Student name condition
		$conds_StudentName = '';
		if ($StudentName != '')
			$conds_StudentName = " And	(
											u.EnglishName Like '%".$this->Get_Safe_Sql_Like_Query($StudentName)."%'
											Or
											u.ChineseName Like '%".$this->Get_Safe_Sql_Like_Query($StudentName)."%' 
										)
								";
			
		$conds_ClassTeacher = '';
		if ($TeachingClassOnly == 1)
		{
			$TeachingClassInfoArr = $this->Get_User_Accessible_Class($YearID);
			$TeachingYearClassIDArr = Get_Array_By_Key($TeachingClassInfoArr, 'ClassID');
			
			if (count($TeachingYearClassIDArr) > 0)
				$conds_ClassTeacher = " And yc.YearClassID In (".implode(',', $TeachingYearClassIDArr).") ";
			else
				$conds_ClassTeacher = " And 0 ";
		}
		
		$name_field = getNameFieldWithClassNumberByLang('u.');
    	$sql = "Select
						u.UserID,
						$name_field as UserName,
						u.UserLogin
				From
						INTRANET_USER as u
						Inner Join
						YEAR_CLASS_USER as ycu
						On (u.UserID = ycu.UserID)
						Inner Join
						YEAR_CLASS as yc
						On (ycu.YearClassID = yc.YearClassID)
				Where
						yc.AcademicYearID = '".$this->AcademicYearID."'
						And
						u.RecordType = '2'
						$conds_YearID
						$conds_excludeUserID
						$conds_UserLogin
						$conds_ClassName
						$conds_ClassNumber
						$conds_StudentName
						$conds_ClassTeacher
				Order By
						yc.Sequence, ycu.ClassNumber
				";
		return $this->returnArray($sql);
	}
	
	function Get_Module_Subject($ModuleIDArr='', $ReturnAsso=1)
	{
		if ($ModuleIDArr != '')
		{
			if (!is_array($ModuleIDArr))
				$ModuleIDArr = array($ModuleIDArr);
			$conds_ModuleID = " And ModuleID In (".implode(',', $ModuleIDArr).") ";
		}
		
		$RC_MODULE_SUBJECT = $this->Get_Table_Name('RC_MODULE_SUBJECT');
		$sql = "Select 
						* 
				From 
						$RC_MODULE_SUBJECT
				Where
						1
						$conds_ModuleID
				";
		$ModuleSubjectArr = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso == 1)
			$ReturnArr = BuildMultiKeyAssoc($ModuleSubjectArr, array('ModuleID', 'SubjectID'));
		else
			$ReturnArr = $ModuleSubjectArr;
		
		return $ReturnArr;
	}
	
	function Get_Module_Subject_Last_Modified_Info()
	{
		$RC_MODULE_SUBJECT = $this->Get_Table_Name('RC_MODULE_SUBJECT');
		$sql = "Select 
						DateModified, LastModifiedBy
				From
						$RC_MODULE_SUBJECT
				Where
						DateModified IN 
								(SELECT MAX(DateModified) FROM $RC_MODULE_SUBJECT)
				";
		return $this->returnArray($sql);
	}
	
	function Delete_Module_Subject()
	{
		$RC_MODULE_SUBJECT = $this->Get_Table_Name('RC_MODULE_SUBJECT');
		$sql = "Delete From $RC_MODULE_SUBJECT";
		
		return $this->db_db_query($sql);
	}
	
	function Save_Module_Subject($ModuleSubjectArr)
	{
		$ValueArr = array();
		foreach ((array)$ModuleSubjectArr as $thisModuleID => $thisModuleInfoArr)
		{
			foreach((array)$thisModuleInfoArr as $thisSubjectID => $thisActive)
			{
				if ($thisActive == 1)
				{
					$ValueArr[] = " ('$thisModuleID', '$thisSubjectID', now(), '".$_SESSION['UserID']."') ";
				}
			}
		}
		
		if (count($ValueArr) > 0)
		{
			$ValueList = implode(',', $ValueArr);
			
			$RC_MODULE_SUBJECT = $this->Get_Table_Name('RC_MODULE_SUBJECT');
			$sql = "Insert Into $RC_MODULE_SUBJECT
						(ModuleID, SubjectID, DateModified, LastModifiedBy)
					Values
						$ValueList
					";
			$success = $this->db_db_query($sql);
		}
		else
		{
			$success = true;
		}		
		return $success;
	}
	
	function GetOtherInfoDBTableSql($CategoryID,$ReportID='',$ClassLevelID='', $ClassID='')
	{
		global $Lang;
		
		if(trim($ReportID)!='')
			$cond_ReportID = " AND rrt.ReportID = '$ReportID' ";
		if(trim($ClassLevelID)!='')
			$cond_ClassLevelID = " AND yc.YearID = '$ClassLevelID' ";
		if(trim($ClassID)!='')
			$cond_ClassID = " AND yc.YearClassID = '$ClassID' ";	
		
		# if no CatgoryID passed
		if(trim($CategoryID)=='')
		{
			$CategoryArr = $this->Get_Other_Info_Category();
			$CategoryID = 	$CategoryArr[0]['CategoryID'];
		}

		$ReportTitle = Get_Lang_Selection("ReportTitleCh","ReportTitleEn");
		$ClassTitle = Get_Lang_Selection("ClassTitleB5","ClassTitleEn");
		
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		$RC_OTHER_INFO_STUDENT_RECORD = $this->Get_Table_Name('RC_OTHER_INFO_STUDENT_RECORD');	
		$sql = "
			/* Display Class if any student of the class exist in RC_OTHER_INFO_STUDENT_RECORD*/
			SELECT 
				rrt.$ReportTitle AS ReportTitle,
				y.YearName AS YearName,
				yc.$ClassTitle  AS ClassName,
				CONCAT('<a href=\"export.php?ClassID=',yc.YearClassID,'&ClassLevelID=',y.YearID,'&CategoryID=',rosr.CategoryID,'&ReportID=',rrt.ReportID,'\">','Download CSV','</a>'),
				MAX(rosr.DateInput) AS DateInput,
				CONCAT('<input type=\"checkbox\" name=\"CSVFile[]\" value=\"',rrt.ReportID,'_',y.YearID,'_',yc.YearClassID,'\">'),
				1 AS DisplayOrder
			FROM 
				$RC_OTHER_INFO_STUDENT_RECORD rosr
				INNER JOIN $RC_REPORT_TEMPLATE rrt ON rosr.ReportID = rrt.ReportID
				INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID  = rosr.StudentID
				INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."'
				LEFT JOIN YEAR y ON y.YearID = yc.YearID
			WHERE
				CategoryID = '$CategoryID'
				$cond_ReportID
				$cond_ClassLevelID
				$cond_ClassID
			GROUP BY rrt.ReportID, yc.YearClassID
			UNION
			/* Display Whole Form if any student of the form exist in RC_OTHER_INFO_STUDENT_RECORD*/
			SELECT 
				rrt.$ReportTitle AS ReportTitle,
				y.YearName AS YearName,
				'".$Lang['eRC_Rubrics']['GeneralArr']['WholeForm']."' AS ClassName,
				CONCAT('<a href=\"export.php?ClassID=0&ClassLevelID=',y.YearID,'&CategoryID=',rosr.CategoryID,'&ReportID=',rrt.ReportID,'\">','Download CSV','</a>'),
				MAX(rosr.DateInput) AS DateInput,
				CONCAT('<input type=\"checkbox\" name=\"CSVFile[]\" value=\"',rrt.ReportID,'_',y.YearID,'_0\">'),
				0 AS DisplayOrder
			FROM 
				$RC_OTHER_INFO_STUDENT_RECORD rosr
				INNER JOIN $RC_REPORT_TEMPLATE rrt ON rosr.ReportID = rrt.ReportID
				INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID  = rosr.StudentID
				INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."'
				LEFT JOIN YEAR y ON y.YearID = yc.YearID
			WHERE
				CategoryID = '$CategoryID'
				$cond_ReportID
				$cond_ClassID
				$cond_ClassLevelID
			GROUP BY rrt.ReportID, DisplayOrder, y.YearID
		";
		
		return $sql;
	}
	
	function Get_Subject_Info($SubjectID='')
	{
	    //if(trim($SubjectID) != '')
	    if(!is_array($SubjectID) && trim($SubjectID) != '' || is_array($SubjectID)) {
			$cond_SubjectID = " AND subject.RecordID IN (".implode(',', (array)$SubjectID).") ";
        }
		
		$sql = "SELECT
						subject.RecordID as SubjectID,
						subject.CODEID,
						subject.EN_DES,
						subject.CH_DES,
						subject.EN_ABBR,
						subject.CH_ABBR,
						subject.EN_SNAME,
						subject.CH_SNAME,
						subject.CMP_CODEID,
						parent.RecordID as ParentSubjectID,
						IF(subject.CMP_CODEID IS NULL OR subject.CMP_CODEID = '', 1, 2) as SubjectType
				FROM
		  				ASSESSMENT_SUBJECT as subject
						INNER JOIN LEARNING_CATEGORY as lc ON (subject.LearningCategoryID = lc.LearningCategoryID)
						INNER JOIN ASSESSMENT_SUBJECT as parent ON (subject.CODEID = parent.CODEID)
				WHERE
						1
						$cond_SubjectID
						AND
						subject.RecordStatus = '1'
						AND
						parent.RecordStatus = '1'
						AND
						(parent.CMP_CODEID IS NULL OR parent.CMP_CODEID = '')
				ORDER BY
						SubjectType, lc.DisplayOrder, subject.DisplayOrder ";
		$AllSubjectInfoArr = $this->returnArray($sql);
		$numOfSubject = count($AllSubjectInfoArr);
		
		$SubjectInfoArr = array();
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $AllSubjectInfoArr[$i]['SubjectID'];
			$thisParentSubjectID = $AllSubjectInfoArr[$i]['ParentSubjectID'];
			
			if ($thisSubjectID == $thisParentSubjectID)		// Main Subject
				$SubjectInfoArr[$thisSubjectID] = $AllSubjectInfoArr[$i];
			else											// Cmp Subject
				$SubjectInfoArr[$thisParentSubjectID]['CmpSubjectInfoArr'][$thisSubjectID] = $AllSubjectInfoArr[$i];
		}
		
		return $SubjectInfoArr;
	}
	
	function Get_Student_Studying_Subject_Group($YearTermID, $StudentID, $SubjectID)
	{
		$sql = "Select
						stcu.SubjectGroupID
				From
						SUBJECT_TERM as st
						Inner Join
						SUBJECT_TERM_CLASS_USER as stcu
						On (stcu.SubjectGroupID = st.SubjectGroupID)
				Where
						stcu.UserID = '".$StudentID."'
						And
						st.SubjectID = '".$SubjectID."'
						And
						st.YearTermID = '".$YearTermID."'
				";
		$resultSet = $this->returnVector($sql);
		$SubjectGroupID = $resultSet[0];
		
		return $SubjectGroupID;
	}
	
	function Get_Student_Class_Teacher_Comment($ReportID='', $StudentIDArr='', $SubjectID='')
	{
		if(trim($ReportID)!='')
			$cond_ReportID = " AND ReportID = '$ReportID' ";
		if(!empty($StudentIDArr))
		{
			$StudentIDSql = implode(",",(array)$StudentIDArr);
			$cond_StudentID = " AND StudentID IN ($StudentIDSql) ";
		}
		if(trim($SubjectID)!='')
			$cond_SubjectID = " AND TopicID = '$SubjectID' ";
		
		$RC_TEACHER_COMMENT =  $this->Get_Table_Name("RC_TEACHER_COMMENT");
		
		$sql = "
			SELECT 
				TeacherCommentID,
				StudentID,
				ReportID,
				TopicID,
				Comment
			FROM 
				$RC_TEACHER_COMMENT
			WHERE
				1
				$cond_ReportID
				$cond_StudentID
				$cond_SubjectID
		";
		$Result = $this->returnArray($sql);
		return $Result;
	}
	
	// 20100831 Marcus
	function Get_OtherInfo_Item_Info($CategoryID='')
	{
		global $ReportCard_Rubrics_CustomSchoolName;
		
		if(trim($CategoryID)!='')
			$cond_CategoryID = " AND roc.CategoryID = '$CategoryID' ";
			
		$CategoryCodeArr = "'".implode("','",(array)$this->OtherInfoTypeArr)."'";
		
		$RC_OTHER_INFO_CATEGORY = $this->Get_Table_Name('RC_OTHER_INFO_CATEGORY');
		$RC_OTHER_INFO_ITEM = $this->Get_Table_Name('RC_OTHER_INFO_ITEM');
		$sql = "
			SELECT
				roc.CategoryID, 
				roc.CategoryCode,
				roi.*
			FROM 
				$RC_OTHER_INFO_CATEGORY roc
				INNER JOIN $RC_OTHER_INFO_ITEM roi ON roc.CategoryID = roi.CategoryID
			WHERE
				roc.SchoolName = '$ReportCard_Rubrics_CustomSchoolName'
				AND roc.CategoryCode IN ($CategoryCodeArr)
				$cond_CategoryID
		";
		
		$result = $this->returnArray($sql);

		return $result;
		
	}
	
	function Get_OtherInfo_Sample_Student_Info()
	{
		global $Lang;
		
		$fieldName = array();
		$sampleData = array();
		
		$StudentFieldArr = $Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportHeader'];
		$StudentSampleArr = $Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportSample'];
	
		foreach((array)$StudentFieldArr as $Field => $ItemName)
		{
			$fieldName[] = $ItemName['En'];
			$sampleData[0][] = $ItemName['Ch'];
			for($i=0; $i<3; $i++)
			{
				$sampleData[$i+1][] = $StudentSampleArr[$Field][$i];
			}
		}
		
		return array($fieldName,$sampleData);
				
	}
	
	function Get_OtherInfo_CSV_Header($CategoryID, $IncludeChineseRow=0)
	{
		global $Lang;
		
		$OtherInfoItemInfo = $this->Get_OtherInfo_Item_Info($CategoryID);
		$SampleStudentInfo = $this->Get_OtherInfo_Sample_Student_Info();
		
		$fieldName = array();
		$chineseRow = array();
		list($fieldName,$chineseRow) = $SampleStudentInfo;
		
		for($i=0; $i<sizeof($OtherInfoItemInfo); $i++) {
			
			$fieldName[] = $OtherInfoItemInfo[$i]["ItemNameEn"];
			$chineseRow[0][] =  "(".$OtherInfoItemInfo[$i]["ItemNameCh"].")";
		}
		$chineseRow[0][] = $Lang['General']['ImportArr']['SecondRowWarn'];// append (do not remove this row)
		
		if($IncludeChineseRow==1)
			return array($fieldName,$chineseRow[0]);
		else
			return $fieldName;
	}
	
	function Replace_Student_OtherInfo_Record($ReportID,$CategoryID,$ClassLevelID,$ClassID,$ImportData)
	{
		$this->Start_Trans();
		
		$Success["Delete"] = $this->Delete_Student_Existing_OtherInfo($ReportID,$CategoryID,$ClassLevelID,$ClassID);
		
		if($Success["Delete"])
			$Success["Import"] = $this->Import_Student_OtherInfo($ReportID,$ImportData);	
		
		if(in_array(false,$Success))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
			
	}
	
	function Delete_Student_Existing_OtherInfo($ReportID, $CategoryID, $ClassLevelID='', $ClassID='')
	{
		$StudentIDArr = $this->Get_StudentID_List($ClassLevelID,$ClassID);
		if(empty($StudentIDArr))
			return false;
				
		$StudentListSql = implode(",",$StudentIDArr);
		
		$RC_OTHER_INFO_STUDENT_RECORD = $this->Get_Table_Name('RC_OTHER_INFO_STUDENT_RECORD');
		$sql = "
			DELETE FROM 
				$RC_OTHER_INFO_STUDENT_RECORD
			WHERE
				ReportID = '$ReportID'
				AND CategoryID = '$CategoryID'
				AND StudentID In ($StudentListSql)
		";

		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Import_Student_OtherInfo($ReportID,$ImportData)
	{

		$OtherInfoItemInfo = $this->Get_OtherInfo_Item_Info();
		$ItemCategoryIDMap = BuildMultiKeyAssoc($OtherInfoItemInfo,"ItemID","CategoryID",1);
		
		foreach((array)$ImportData as $StudentID => $StudentOtherInfo)
		{
			foreach((array)$StudentOtherInfo as $ItemID => $OtherInfoValArr)
			{
				foreach((array)$OtherInfoValArr as $OtherInfoVal)
				{
					$CategoryID = $ItemCategoryIDMap[$ItemID];
					$RecordSqlArr[] = "('$ReportID','$ItemID','$StudentID','".$this->Get_Safe_Sql_Query($OtherInfoVal)."','$CategoryID', NOW(),'".$_SESSION['UserID']."')";
				} 
			}
		}
		
		$RecordSql = implode(",\n",$RecordSqlArr);
		
		$RC_OTHER_INFO_STUDENT_RECORD = $this->Get_Table_Name('RC_OTHER_INFO_STUDENT_RECORD');
		$sql = "
			INSERT INTO
				$RC_OTHER_INFO_STUDENT_RECORD
				(ReportID,ItemID,StudentID,Information,CategoryID,DateInput,InputBy)
			VALUES
				$RecordSql		
			"; 
		
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Get_Student_OtherInfo_Export_Data($ReportID,$CategoryID,$ClassLevelID,$ClassID='')
	{
		global $ReportCard_Rubrics_CustomSchoolName;
		
		list($ExportHeader,$ExportHeaderCh) = $this->Get_OtherInfo_CSV_Header($CategoryID,1);
		
		$OtherInfoItemInfo = $this->Get_OtherInfo_Item_Info($CategoryID);
		
		$StudentOtherInfo = $this->Get_Student_OtherInfo_Record($ReportID,$CategoryID,$ClassLevelID,$ClassID);
		
		# build student info assoc 
		$StudentInfoArr = BuildMultiKeyAssoc($StudentOtherInfo, array("StudentID"),array("ClassTitleEn","ClassNumber","UserLogin","WebSAMSRegNo","EnglishName"));
		
		# build student other info assoc 
		$StudentOtherInfoAssocArr = BuildMultiKeyAssoc($StudentOtherInfo, array("StudentID","ItemID","RecordID")); 
		
		$ExportData = array();
		$ExportData[] = $ExportHeaderCh;
		
		foreach((array)$StudentOtherInfoAssocArr as $thisStudentID => $thisOtherInfoArr)
		{
			// find out how many rows are needed to show duplicated records
			$maxRow = 0;
			foreach((array)$OtherInfoItemInfo as $ItemInfo)
			{
				$thisItemID = $ItemInfo["ItemID"];
				$maxRow = max($maxRow,count($thisOtherInfoArr[$thisItemID]));
				$thisOtherInfoArr[$thisItemID] = array_values((array)$thisOtherInfoArr[$thisItemID]);
			}
			
			// get Student Info () 
			$thisStudentInfo = $StudentInfoArr[$thisStudentID];
			
			// loop rows per student
			for($j=0;$j<$maxRow; $j++)
			{
				$ExportRow = array();
				
				$ExportRow[] = $thisStudentInfo['ClassTitleEn'];
				$ExportRow[] = $thisStudentInfo['ClassNumber'];
				$ExportRow[] = $thisStudentInfo['WebSAMSRegNo'];
				$ExportRow[] = $thisStudentInfo['UserLogin'];
				$ExportRow[] = $thisStudentInfo['EnglishName'];
				
				// loop otherinfo per rows
				foreach((array)$OtherInfoItemInfo as $ItemInfo)
				{
					$thisItemID = $ItemInfo["ItemID"];
					$ExportRow[] = $thisOtherInfoArr[$thisItemID][$j]['Information'];
				}
				
				$ExportData[] = $ExportRow;
			}
		}
		
		return array($ExportHeader,$ExportData);
	}
	
	function Get_Student_OtherInfo_Record($ReportID, $CategoryID='', $ClassLevelID='', $ClassID='', $StudentIDArr='')
	{
		if ($StudentIDArr == '')
			$StudentIDArr = $this->Get_StudentID_List($ClassLevelID, $ClassID);	
		$StudentListSql = implode(",", $StudentIDArr);
		
		if ($CategoryID != '')
			$cond_CategoryID = " AND rosr.CategoryID = '$CategoryID' ";
		
		$RC_OTHER_INFO_STUDENT_RECORD = $this->Get_Table_Name('RC_OTHER_INFO_STUDENT_RECORD');
		$sql ="
			SELECT
				yc.ClassTitleEn,
				ycu.ClassNumber,
				iu.UserLogin,
				iu.WebSAMSRegNo,
				iu.EnglishName,
				rosr.RecordID,
				rosr.StudentID,
				rosr.ItemID,
				rosr.Information,
				rosr.CategoryID
			FROM 
				$RC_OTHER_INFO_STUDENT_RECORD rosr
				INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID = rosr.StudentID
				INNER JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."' 
				INNER JOIN INTRANET_USER iu ON iu.UserID = ycu.UserID
			WHERE
				rosr.ReportID = '$ReportID'
				AND rosr.StudentID IN ($StudentListSql)
				$cond_CategoryID
			ORDER BY
				yc.YearClassID, iu.ClassNumber, rosr.StudentID, rosr.ItemID, rosr.RecordID
		";
		
		$result = $this->returnArray($sql);

		return $result;
	}
	
	function Get_OtherInfo_Export_Filename($ReportID,$CategoryID,$ClassLevelID,$ClassID)
	{
		$RC_REPORT_TEMPLATE = $this->Get_Table_Name('RC_REPORT_TEMPLATE');
		$sql = "SELECT ReportTitleEn FROM $RC_REPORT_TEMPLATE WHERE ReportID = '$ReportID'";
		$tmp = $this->returnVector($sql);
		$ReportTitle = $tmp[0];
		
		$RC_OTHER_INFO_CATEGORY = $this->Get_Table_Name('RC_OTHER_INFO_CATEGORY');
		$sql = "SELECT CategoryCode FROM $RC_OTHER_INFO_CATEGORY WHERE CategoryID = '$CategoryID'";
		$tmp = $this->returnVector($sql);
		$CatTitle = $tmp[0];
		
		if(empty($ClassID))
			$sql = "SELECT YearName FROM YEAR WHERE YearID='$ClassLevelID' ";
		else
			$sql = "SELECT ClassTitleEn FROM YEAR_CLASS WHERE YearClassID='$ClassID' ";
		$tmp = $this->returnVector($sql);
		$ClassTitle = $tmp[0];
	
		return str_replace(" ","_",$ReportTitle."_".$ClassTitle."_".$CatTitle).".csv";	
	}
	
	function Remove_Student_OtherInfo_Data($ReportID, $CategoryID, $ClassLevelID, $ClassID)
	{
		$StudentIDArr = $this->Get_StudentID_List($ClassLevelID,$ClassID);
		if(empty($StudentIDArr))
			return false;
			
		$StudentIDSql = implode(",",(array)$StudentIDArr);
				
		$RC_OTHER_INFO_STUDENT_RECORD = $this->Get_Table_Name('RC_OTHER_INFO_STUDENT_RECORD');
		$sql = "
			DELETE FROM
				$RC_OTHER_INFO_STUDENT_RECORD
			WHERE
				ReportID = '$ReportID'
				AND CategoryID = '$CategoryID'
				AND StudentID IN ($StudentIDSql) 
		";
		
		$Success = $this->db_db_query($sql); 
		
		return $Success;
	}
	
	function Get_StudentID_List($ClassLevelID, $ClassID='')
	{
		if (empty($ClassID))
			$StudentList = $this->GET_STUDENT_BY_CLASSLEVEL($ClassLevelID); 
		else
			$StudentList = $this->GET_STUDENT_BY_CLASS($ClassID);
			
		if(empty($StudentList))
			return array();
			
		$StudentIDArr = Get_Array_By_Key($StudentList,"UserID");	
		
		return $StudentIDArr;
	}
	
	function Get_Number_Of_Last_Level_Topic($SubjectTopicTreeArr, $LastTopicLevel, $TopicInfoArr, $IncludeEmptyNonLastLevel=0, $TopicLevelArr='', $Counter=0)
	{
		foreach ($SubjectTopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel = $TopicInfoArr[$thisTopicID]['Level'];
			$thisNumOfNextLevelTopic = count($thisLinkedTopicIDArr);
			
			### Increase Counter if start counting and it is the last level
			if ($thisLevel <= $LastTopicLevel)
			{
				if ($thisLevel == $LastTopicLevel)
					$Counter++;
				else if ($IncludeEmptyNonLastLevel && $thisNumOfNextLevelTopic==0 && ($TopicLevelArr=='' || in_array($thisLevel, (array)$TopicLevelArr)))
					$Counter++;
				
				if ($thisNumOfNextLevelTopic > 0)
					$Counter = $this->Get_Number_Of_Last_Level_Topic($thisLinkedTopicIDArr, $LastTopicLevel, $TopicInfoArr, $IncludeEmptyNonLastLevel, $TopicLevelArr, $Counter);
			}	
		}
		
		return $Counter;
	}
	
	function Get_Topic_All_Last_Level_Topic($TargetTopicID, $SubjectTopicTreeArr, $TopicInfoArr, $LastLevelTopicArr=array(), $Valid=0)
	{
		global $eRC_Rubrics_ConfigArr;
		
		foreach ((array)$SubjectTopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel = $TopicInfoArr[$thisTopicID]['Level'];
			$thisNumOfNextLevelTopic = count($thisLinkedTopicIDArr);
			
			if ($thisTopicID == $TargetTopicID) 
				$Valid = true;
			
			### Increase Counter if start counting and it is the last level
			if ($Valid && $thisLevel == $eRC_Rubrics_ConfigArr['MaxLevel'])
				$LastLevelTopicArr[] = $thisTopicID;
			
			if ($thisNumOfNextLevelTopic > 0)
				$LastLevelTopicArr = $this->Get_Topic_All_Last_Level_Topic($TargetTopicID, $thisLinkedTopicIDArr, $TopicInfoArr, $LastLevelTopicArr, $Valid);
				
			if ($thisTopicID == $TargetTopicID)
				break;
		}
		
		return $LastLevelTopicArr;
	}
	
	
	function Get_Topic_Mark_Statistics($StudentIDArr, $ModuleIDArr, $SubjectIDArr, $TargetLevel='')
	{
		global $Global_StudentStudyTopicInfoArr, $Global_StudentMarksheetScoreArr, $Global_SubjectTopicTreeArr, $Global_TopicInfoArr;
		global $lreportcard_topic;
		
		if (!isset($lreportcard_topic))
		{
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_topic.php');
			$lreportcard_topic = new libreportcardrubrics_topic();
		}
		
		$StudentIDArr = (!is_array($StudentIDArr))? array($StudentIDArr) : $StudentIDArr;
		$numOfStudent = count($StudentIDArr);
		$ModuleIDArr = (!is_array($ModuleIDArr))? array($ModuleIDArr) : $ModuleIDArr;
		$numOfModule = count($ModuleIDArr);
		$SubjectIDArr = (!is_array($SubjectIDArr))? array($SubjectIDArr) : $SubjectIDArr;
		$numOfSubject = count($SubjectIDArr);
		
		### Get Student Studied Topic
		// $StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = InfoArr
		if (isset($Global_StudentStudyTopicInfoArr))
			$StudentStudyTopicInfoArr = $Global_StudentStudyTopicInfoArr;
		else
			$StudentStudyTopicInfoArr = $this->Get_Student_Study_Topic_Info($ModuleIDArr, $SubjectIDArr, $TopicIDArr, $StudentIDArr, $SubjectGroupID='', $ReturnAsso=1);
		
		### Get Student Score
		// $StudentMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = InfoArr
		if (isset($Global_StudentMarksheetScoreArr))
			$StudentMarksheetScoreArr = $Global_StudentMarksheetScoreArr;
		else
			$StudentMarksheetScoreArr = $this->Get_Student_Marksheet_Score($SubjectIDArr, $StudentIDArr, $ModuleIDArr);
			
					
		### Get Subject Tree
		//$SubjectTopicTreeArr[$SubjectID] = InfoArr	
		if (isset($Global_SubjectTopicTreeArr))
			$SubjectTopicTreeArr = $Global_SubjectTopicTreeArr;
		else
			$SubjectTopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectIDArr);
			
		### Get All Topic Info
		// $TopicInfoArr[$TopicID] = InfoArr
		if (isset($Global_TopicInfoArr))
			$TopicInfoArr = $Global_TopicInfoArr;
		else
			$TopicInfoArr = $lreportcard_topic->Get_Topic_Info();
		
		$SubjectTopicInfoAssoArr = array();
		foreach ((array)$TopicInfoArr as $thisTopicID => $thisTopicInfoArr)
		{
			$thisSubjectID = $thisTopicInfoArr['SubjectID'];
			(array)$SubjectTopicInfoAssoArr[$thisSubjectID][] = $thisTopicInfoArr;
		}
		
		//debug_pr($Global_TopicInfoArr);

		$TopicMarkStatisticsArr = array();
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			
			for ($j=0; $j<$numOfModule; $j++)
			{
				$thisModuleID = $ModuleIDArr[$j];
				
				for ($k=0; $k<$numOfSubject; $k++)
				{
					$thisSubjectID = $SubjectIDArr[$k];
					$thisSubjectTree = $SubjectTopicTreeArr[$thisSubjectID];
					$thisSubjectTopicInfoArr = $SubjectTopicInfoAssoArr[$thisSubjectID];
					$numOfTopic = count($thisSubjectTopicInfoArr);
					
					for ($l=0; $l<$numOfTopic; $l++)
					{
						$thisTopicID = $thisSubjectTopicInfoArr[$l]['TopicID'];
						
						if ($TargetLevel != '' && $thisSubjectTopicInfoArr[$l]['Level'] != $TargetLevel)
							continue;
						
						### Get the Last Level Topics belongs to this Topic
						$thisLastLevelTopicIDArr = $this->Get_Topic_All_Last_Level_Topic($thisTopicID, $thisSubjectTree, $TopicInfoArr);
						$numOfLastLevelTopic = count($thisLastLevelTopicIDArr);
						
						### Summarize the marks for all related Last Level Topic
						for ($m=0; $m<$numOfLastLevelTopic; $m++)
						{
							$thisLastLevelTopicID = $thisLastLevelTopicIDArr[$m];
							
							# Count the Student Studied Topic only
							if (!isset($StudentStudyTopicInfoArr[$thisStudentID][$thisModuleID][$thisSubjectID][$thisLastLevelTopicID]))
								continue;
								
							# Record the Mark Count of each Rubrics Items
							$thisRubricsItemID = $StudentMarksheetScoreArr[$thisStudentID][$thisSubjectID][$thisLastLevelTopicID][$thisModuleID]['RubricsItemID'];
							
							// Skip the un-input marks
							if ($thisRubricsItemID != 0)
							{
								# Record Total Number of Studied Modules of the Topic
								$TopicMarkStatisticsArr[$thisStudentID][$thisSubjectID][$thisTopicID]['NumOfStudiedModule']++;
								
								# Record the Occurrence of the RubricsItem in this Topic
								$TopicMarkStatisticsArr[$thisStudentID][$thisSubjectID][$thisTopicID]['MarkInfoArr'][$thisRubricsItemID]['Count']++;
							}
							
						}
					}	// End loop Topic
				}	// End loop Subject
			}	// End loop Module
		}	// End loop Student
		
		return $TopicMarkStatisticsArr;
	}
	
	function Get_User_Extra_Info_Item($ItemID='', $CategoryID='')
	{
		if(trim($ItemID!=''))
			$cond_ItemID = " AND eii.ItemID = '$ItemID' ";
		if(trim($CategoryID!=''))
			$cond_CategoryID = " AND eic.CategoryID = '$CategoryID' ";
			
		$sql = "
			SELECT 
				*
			FROM 
				INTRANET_USER_EXTRA_INFO_ITEM eii
				INNER JOIN INTRANET_USER_EXTRA_INFO_CATEGORY eic ON eii.CategoryID = eic.CategoryID
			WHERE
				1
				AND eii.RecordStatus = 1
				AND eic.RecordStatus = 1
				$cond_CategoryID
				$cond_ItemID
		";
		
		$result = $this->returnArray($sql);
		
		return $result;
	}
	
	function Get_User_Extra_Info_Mapping($ParUserIDArr, $ParExtraInfoItemArr='')
	{
		if(empty($ParUserIDArr))
			return false;
		
		$cond_UserID = " AND iu.UserID IN (".implode(",",(array)$ParUserIDArr).") ";
		
//		if(!empty($ParExtraInfoItemArr))
//			$cond_ItemID = " AND eim.ItemID IN (".implode(",",(array)$ParExtraInfoItemArr).") ";

		$sql = "
			SELECT 
				iu.UserID,
				IF(eim.ItemID IS NULL,0,eim.ItemID) ItemID,
				eii.ItemNameEn,
				eii.ItemNameCh,
				eic.CategoryID,
				eic.CategoryNameEn,
				eic.CategoryNameCh
			FROM
				INTRANET_USER iu
				LEFT JOIN INTRANET_USER_EXTRA_INFO_MAPPING eim ON iu.UserID = eim.UserID  
				LEFT JOIN INTRANET_USER_EXTRA_INFO_ITEM eii ON eim.ItemID = eii.ItemID AND eii.RecordStatus = 1 
				LEFT JOIN INTRANET_USER_EXTRA_INFO_CATEGORY eic ON eii.CategoryID = eic.CategoryID AND eic.RecordStatus = 1
			WHERE
				1
				$cond_UserID
		";
		
		$result = $this->returnArray($sql);
		if(!empty($ParExtraInfoItemArr))
		{
			foreach($result as $rec)
			{
				if(in_array($rec['ItemID'],$ParExtraInfoItemArr))
					$ReturnArr[] = $rec;
			}
		}
		
		return $ReturnArr;
	}
	
	function Get_Admin_Group_DBTable_Sql($Keyword='')
	{
		global $Lang;
		
		$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP');
		$RC_ADMIN_GROUP_USER = $this->Get_Table_Name('RC_ADMIN_GROUP_USER');
		$GroupNameField = Get_Lang_Selection('ag.AdminGroupNameCh', 'ag.AdminGroupNameEn');
		
		$conds_Keyword = '';
		if ($Keyword != '')
			$conds_Keyword = " And (	
										ag.AdminGroupCode Like '%$Keyword%'
										Or $GroupNameField Like '%$Keyword%'
									)
							 ";
		
		$sql = "Select UserID From INTRANET_USER Where RecordType = 1 And RecordStatus = 1";
		$existingUserIdAry = $this->returnVector($sql);
		
		$sql = "
			SELECT 
				ag.AdminGroupCode,
				CONCAT('<a class=\"tablelink\" href=\"new_step1.php?AdminGroupIDArr=', ag.AdminGroupID, '\">', $GroupNameField, '</a>') as GroupInfoLink,
				CONCAT('<a class=\"tablelink\" href=\"member_list.php?clearCoo=1&AdminGroupID=', ag.AdminGroupID, '\">', Count(agu.UserID), '</a>') as MemberListLink,
				ag.DateModified,
				CONCAT('<input type=\"checkbox\" name=\"AdminGroupIDArr[]\" value=\"', ag.AdminGroupID, '\">') as CheckBox
			From
				$RC_ADMIN_GROUP as ag
				Left Outer Join $RC_ADMIN_GROUP_USER as agu On (ag.AdminGroupID = agu.AdminGroupID AND agu.UserID IN ('".implode("', '", (array)$existingUserIdAry)."'))
			Where
				ag.RecordStatus = 1
				$conds_Keyword
			Group By
				ag.AdminGroupID		
		";				
		
		return $sql;
	}
	
	function Get_Admin_Group_Member_List_DBTable_Sql($AdminGroupID, $Keyword='')
	{
		global $Lang;
		
		$RC_ADMIN_GROUP_USER = $this->Get_Table_Name('RC_ADMIN_GROUP_USER');
		
		$conds_Keyword = '';
		if ($Keyword != '')
			$conds_Keyword = " And ( iu.EnglishName Like '%$Keyword%' Or iu.ChineseName Like '%$Keyword%' ) ";
		
		$sql = "
			SELECT 
				iu.EnglishName,
				iu.ChineseName,
				CONCAT('<input type=\"checkbox\" name=\"UserIDArr[]\" value=\"', iu.UserID, '\">') as CheckBox
			From
				$RC_ADMIN_GROUP_USER as agu
				Inner Join
				INTRANET_USER as iu On (agu.UserID = iu.UserID)
			Where
				agu.AdminGroupID = '".$AdminGroupID."'
				And iu.RecordType = 1
				And iu.RecordStatus = 1
				$conds_Keyword		
		";
		return $sql;
	}
	
	function Get_Admin_Group_Info($AdminGroupID='')
	{
		if ($AdminGroupID != '')
			$conds_AdminGroupID = " And AdminGroupID = '".$AdminGroupID."' ";
			
		$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP');
		$sql = "Select
						AdminGroupID,
						AdminGroupCode,
						AdminGroupNameEn,
						AdminGroupNameCh
				From
						$RC_ADMIN_GROUP
				Where 
						RecordStatus = 1
						$conds_AdminGroupID
				";
		return $this->returnArray($sql);
	}
	
	function Get_Admin_Group_Member($AdminGroupID)
	{
		$RC_ADMIN_GROUP_USER = $this->Get_Table_Name('RC_ADMIN_GROUP_USER');
		
		$sql = "Select
						UserID
				From
						$RC_ADMIN_GROUP_USER
				Where
						AdminGroupID = '".$AdminGroupID."'
				";
		return $this->returnArray($sql);
	}
	
	function Get_Admin_Group_Access_Right($AdminGroupIDArr)
	{
		$RC_ADMIN_GROUP_RIGHT = $this->Get_Table_Name('RC_ADMIN_GROUP_RIGHT');
		
		$sql = "Select
						AdminGroupRightName
				From
						$RC_ADMIN_GROUP_RIGHT
				Where
						AdminGroupID In (".implode(',', (array)$AdminGroupIDArr).")
				";
		return $this->returnArray($sql);
	}
	
	function Get_Admin_Group_Available_User($AdminGroupID, $SearchUserLogin='', $ExcludeUserIDArr='', $TargetUserType='')
	{
		if (!is_array($ExcludeUserIDArr))
			$ExcludeUserIDArr = array();
			
		### Exclude Current Member
		$MemberInfoArr = $this->Get_Admin_Group_Member($AdminGroupID);
		$MemberUserIDArr = Get_Array_By_Key($MemberInfoArr, 'UserID');
		if (count($MemberUserIDArr) > 0)
		{
			$MemberList = implode(',', $MemberUserIDArr);
			$conds_excludeMember = " And UserID Not In ($MemberList) ";
		}
		
		### Exclude Other Users
		$ExcludeUserIDArr = array_remove_empty($ExcludeUserIDArr);
		if (is_array($ExcludeUserIDArr) && count($ExcludeUserIDArr) > 0)
		{
			$excludeUserList = implode(',', $ExcludeUserIDArr);
			$conds_excludeUserID = " And UserID Not In ($excludeUserList) ";
		}
		
		if ($SearchUserLogin != '')
			$conds_userlogin = " And UserLogin Like '%$SearchUserLogin%' ";

		if ($TargetUserType != '')
			$conds_UserType = " And RecordType In (".implode(',', (array)$TargetUserType).") ";

		$name_field = getNameFieldByLang();
		$sql = "Select
						UserID,
						$name_field as UserName,
						UserLogin
				From
						INTRANET_USER
				Where
						RecordStatus = 1
						$conds_excludeUserID
						$conds_excludeMember
						$conds_userlogin
						$conds_UserType
				Order By
						EnglishName
				";
		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}
	
	function Is_Admin_Group_Code_Valid($Value, $ExcludeID='')
	{
		$Value = $this->Get_Safe_Sql_Query(trim($Value));
		
		$conds_ExcludeID = '';
		if ($ExcludeID != '')
			$conds_ExcludeID = " And AdminGroupID != '$ExcludeID' ";
		
		$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP');
		$sql = "Select
						AdminGroupID
				From
						$RC_ADMIN_GROUP
				Where
						AdminGroupCode = '$Value'
						And
						RecordStatus = 1
						$conds_ExcludeID
				";
		$ResultArr = $this->returnArray($sql);
		return (count($ResultArr) > 0)? false : true;
	}
	
	function Add_Admin_Group($DataArr)
	{
		if (!is_array($DataArr) || count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# LastModifiedBy
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP');
		$sql = "Insert Into $RC_ADMIN_GROUP ($fieldText) Values ($valueText)";
		$success = $this->db_db_query($sql);
		
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			$this->Commit_Trans();
			return $insertedID;
		}
	}
	
	function Update_Admin_Group($AdminGroupID, $DataArr, $UpdateLastModified=1)
	{
		if (!is_array($DataArr) || count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $field => $value)
		{
			$valueFieldArr[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
		}
		
		if ($UpdateLastModified==1)
		{
			$valueFieldArr[] = " DateModified = now() ";
			$valueFieldArr[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
		}
		
		$valueFieldText .= implode(',', $valueFieldArr);
		
		$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP');
		$sql = "Update $RC_ADMIN_GROUP Set $valueFieldText Where AdminGroupID = '$AdminGroupID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function Delete_Admin_Group($AdminGroupIDArr)
	{
		$AdminGroupIDArr = (array)$AdminGroupIDArr;
		$numOfAdminGroup = count($AdminGroupIDArr);
		
		$DataArr = array();
		$DataArr['RecordStatus'] = 0;
		for ($i=0; $i<$numOfAdminGroup; $i++)
		{
			$thisAdminGroupID = $AdminGroupIDArr[$i];
			$SuccessArr['Delete_Topic'][$thisAdminGroupID] = $this->Update_Admin_Group($thisAdminGroupID, $DataArr);
		}
		
		return !in_array(false, $SuccessArr);
	}
	
	function Add_Admin_Group_Member($AdminGroupID, $TargetMemberArr)
	{
		$TargetMemberArr = array_remove_empty($TargetMemberArr);
		$UserIDArr = Get_User_Array_From_Common_Choose($TargetMemberArr, $UserTypeArr=array(1));
		$numOfUser = count($UserIDArr);
		
		if ($numOfUser > 0)
		{
			$InsertValueArr = array();
			for ($i=0; $i<$numOfUser; $i++)
			{
				$thisUserID = $UserIDArr[$i];
				$InsertValueArr[] = "('".$AdminGroupID."', '".$thisUserID."', '".$_SESSION['UserID']."', '".$_SESSION['UserID']."', now(), now())";
			}
			
			$insertValueList = implode(', ', $InsertValueArr);
			$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP_USER');
			
			$sql = "Insert Into $RC_ADMIN_GROUP
						(AdminGroupID, UserID, InputBy, LastModifiedBy, DateInput, DateModified)
					Values
						$insertValueList
					";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	function Delete_Admin_Group_Member($AdminGroupID, $UserIDArr) {
		if (count($UserIDArr) == 0)
			return false;
			
		$UserIDList = implode(', ', $UserIDArr);
		$RC_ADMIN_GROUP = $this->Get_Table_Name('RC_ADMIN_GROUP_USER');
		
		$sql = "Delete From 
					$RC_ADMIN_GROUP
				Where
					AdminGroupID = '".$AdminGroupID."'
					And
					UserID In ($UserIDList)
				";
		return $this->db_db_query($sql);
	}
	
	function Update_Admin_Group_Access_Right($AdminGroupID, $AccessRightArr)
	{
		$SuccessArr = array();
		$this->Start_Trans();
		
		$RC_ADMIN_GROUP_RIGHT = $this->Get_Table_Name('RC_ADMIN_GROUP_RIGHT');
		$sql = "Delete From $RC_ADMIN_GROUP_RIGHT Where AdminGroupID = '".$AdminGroupID."' ";
		$SuccessArr['DeleteOldAccessRight'] = $this->db_db_query($sql);
		
		$numOfAccessRight = count((array)$AccessRightArr);
		if (is_array($AccessRightArr) && $numOfAccessRight > 0) {
			$InsertArr = array();
			for ($i=0; $i<$numOfAccessRight; $i++) {
				$thisAccessRight = $AccessRightArr[$i];
				$InsertArr[] = " ('".$AdminGroupID."', '".$this->Get_Safe_Sql_Query($thisAccessRight)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
			}
			
			$sql = "Insert Into	$RC_ADMIN_GROUP_RIGHT
						(AdminGroupID, AdminGroupRightName, DateInput, InputBy, DateModified, LastModifiedBy)
					Values
						".implode(',', $InsertArr);
			$SuccessArr['InsertNewAccessRight'] = $this->db_db_query($sql);
		}
		
		if (!in_array(false, $SuccessArr)) {
			$this->Commit_Trans();
			return true;
		}
		else {
			$this->RollBack_Trans();
			return false;
		}
	}
	
	function Get_Invalid_Member_Selection($AdminGroupID, $SelectionList) {
		$InvalidSelectionArr = array();
		
		$SelectionArr = explode(',', $SelectionList);
		$SelectionArr = array_remove_empty($SelectionArr);
		$numOfSelection = count($SelectionArr);
		
		if ($numOfSelection > 0)
		{
			$GroupMemberInfoArr = $this->Get_Admin_Group_Member($AdminGroupID);
			$GroupMemberUserIDArr = Get_Array_By_Key($GroupMemberInfoArr, 'UserID');
			
			for ($i=0; $i<$numOfSelection; $i++)
			{
				$thisSelection = $SelectionArr[$i];
				$thisUserIDArr = Get_User_Array_From_Common_Choose(array($thisSelection), $UserTypeArr=array(1));
				
				# $thisIsMemberUserIDArr contains the UserID which is member and is in the user selection => which is an invalid selection
				$thisIsMemberUserIDArr = array_intersect($GroupMemberUserIDArr, $thisUserIDArr);
				
				if (count($thisIsMemberUserIDArr))
					$InvalidSelectionArr[] = $thisSelection;
			}
		}
		
		$InvalidSelectionList = implode(',', $InvalidSelectionArr);
		return $InvalidSelectionList;
	}
	
	function Update_Current_Page_Session($ParCurrentPage) {
		$_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["CurrentPage"] = $ParCurrentPage;
		return true;
	}
	
	function Get_Current_Page_Session() {
		return $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["CurrentPage"];
	}
	
	function Insert_General_Log($Functionality, $Content) {
		$sql = "Insert Into RC_RUBRICS_LOG (Functionality, UserID, LogContent, InputDate) Values ('".$Functionality."', '".$_SESSION['UserID']."', '".$Content."', now())";
		$this->db_db_query($sql);
	}
}

?>