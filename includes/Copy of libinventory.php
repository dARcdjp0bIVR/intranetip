<?php
// using by:    

#################################################
#
#   Date:   2020-03-09 Cameron
#           Following functions are initially created for eClass Teacher App
#           - getBulkItemCeilingPriceCondition(), getStocktakeStatus(), getBulkItemFundingAndQty(),
#             getSingleItemGroupInCharge(), isItemInLocation(), isSurplusRecordExist(), isMissingRecordExist(),
#             getSurplusRecordID(), getMissingRecordID(), doStocktake(), addSingleItemSurplus(),
#             updateSingleItemMissing(), updateSingleItemRemark(), updateBulkItemStocktake(), getSingleItemDetail(),
#             getBulkItemDetail(), getItemStatus(), getSubCategoryInUse(), isStocktakeDoneForSingleItem(),
#             isStocktakeDoneForBulkItem(), getValidTag()
#
#   Date:   2020-03-06 Cameron
#           - add function getSingleItemMoreDetail(), getBulkItemMoreDetail()
#
#   Date:   2020-03-05 Cameron
#           - add function getValidTag(), getItemIDByTag()
#
#   Date:   2020-02-20 Cameron
#           - add function checkItemAdminGroupAndLocationByBarcode()
#
#   Date:   2020-02-18 Cameron
#           - add function getSingleItemLatestStocktakeRemark()
#           - modify getSingleItemDetail() to retrieve StocktakeRemark
#           - fix last update person in getStocktakeProgressByAdminGroup() and getStocktakeProgressByLocation()
#
#   Date:   2020-02-12 Tommy
#           - add permission checking for $sys_custom['eInventory']['plkchc_report']
#
#   Date:   2020-02-12 Cameron
#           - add function getItemList()
#
#   Date:   2020-02-10 Cameron
#           - add function getFundingSoureInUse()
#
#   Date:   2020-02-07 Tommy
#           - add Write-off Approval_PLKCHC, Fixed Assets Register_PLKCHC, Fixed Assets Stocktake List_PLKCHC #A170032 
#
#   Date:   2020-01-24 Cameron
#           - add function getStocktakeNotDoneByLocation()
#
#   Date:   2020-01-23 Cameron
#           - add function getStocktakeProgressByLocation()
#
#   Date:   2020-01-20 Cameron
#           - add alias of FundingSource in returnFundingSource()
#
#   Date:   2020-01-14 Cameron
#           - add function getStocktakeDoneByLocation()
#
#   Date:   2020-01-09 Cameron
#           - add function getItemByBarcode()
#
#   Date:   2020-01-09 Tommy
#           - add function getNumOfBulkItemInvoice()
#
#   Date:   2019-12-18 Cameron
#           - add function getStocktakeProgressByAdminGroup()
#
#   Date:   2019-12-17 Cameron
#           - add function getAdminGroupIDAry(), getGroupInChargeLocation()
#           - add parameter $adminGroupIDFilter to returnAdminGroup()
#
#   Date:   2019-12-16 Cameron
#           add function checkAppAccessRight()
#
#   Date:   2019-07-25 Tommy
#           add getItemCodeDigitLength() - get item code digit length from database for item code setting
#           add getItemCodeSeparator() - get item code separator from database for item code setting
#
#   Date:   2019-06-28 Tommy
#           fix: allow write-off item to import in function checkImportStocktakeCode()
#
#   Date:   2019-05-13 Henry
#           Security fix: SQL without quote in function FilterForStockTakeBulk()
#
#   Date:   2019-02-19 Isaac
#           migrated the warrantyExpiryWarningPopup from /home/index.php to create functions getWarrantyExpiry() and warrantyExpiryWarningPopup()
#
#   Date:   2018-05-18 Cameron
#           add function getNumberOfOrderByFieldInLabel() [case #P138005]
#
#	Date:	2017-11-28 Cameron
#			- modify GET_MODULE_OBJ_ARR() to set Menu Items for PowerClass
#
#	Date:	2016-11-02 Henry [case#Z107197]
#			- allow view this writeoff approval page when group leader not allow writeoff item
#
#	Date: 	2016-09-09 Villa
#			Add functon returnFundingSourceWithFundingType - get funding id, funding name and funding type
#
#	Date:	2016-02-01 
#			PHP 5.4 fix: change split to explode
#
#	Date:	2015-05-12 Henry
#			general deployment for Stocktake List Report
#
#	Date:	2015-05-05 Cameron
#			add flag to clear cookie for Resource Management Group setting in GET_MODULE_OBJ_ARR() 
#
#	Date:	2014-11-26	Henry
#			display both Variance Handling Notice and socktake in management menu
#
#	Date:	2014-04-09	YatWoon
#			add returnAdminGroupSelection()
#
#	Date:	2013-07-25	YatWoon
#			add new action constant ITEM_ACTION_WRITEOFF_RESTORE
#
#	Date:	2013-06-24	YatWoon
#			updated GET_MODULE_OBJ_ARR(), add "Stocktake List report" for CatholicEducation_eInventory [Case#2013-0218-1231-03054]
#
#	Date:	2013-05-27	YatWoon
#			added TrimWriteOffTag(), avoid display the tag with write-off items only
#
#	Date: 	2013-03-13 	Carlos
#			modified checkImportStocktakeCode() add parameter itemType 
#
#	Date:	2013-03-12	YatWoon
#			add insertBulkInvoiceHistoryLog()
#
#	Date:	2013-02-06	Carlos
#			added getUniqueBarcode(), checkImportStocktakeAdminGroup(), checkImportStocktakeFundingSource(), trimBarcodeStars()
#			checkImportStocktakeItemOwnedByAdminGroup(), getImportStocktakeItemAdminGroup(), checkImportStocktakeItemFundingSource(), 
#			getImportStocktakeItemFundingSource()
#
#	Date:	2012-12-06	Yuen
#			Enhanced: support tags 
#
#	Date:	2012-12-06	YatWoon
#			update sendEmailNotification_eBooking(), updated retrieve eBooking admin instead of eDiscipline admin
#
#	Date:	2012-11-09	YatWoon
#			added function parseStockTakeOption() for import function
#
#	Date:	2012-10-29	YatWoon
#			Improved: update random_barcode(), not allow "0" as the 1st char [Case#2012-1025-1014-06072]
#
#	Date: 	2012-10-29 Rita
#			add enablePhotoDisplayInBarcodeRight() for customization in barcode display
#
#	Date:	2012-10-25	YatWoon
#			add sendEmailNotification_eBooking()
#			update returnItemInfo(), add $bi_lang parameter
#
#	Date:	2012-10-22	YatWoon
#			add returnBulkItemNormalQty(), returnSingleItemStatus() for eBooking intergation
#
#	Date: 	2012-10-10 	Rita
#			add constant GROUP_HELPER, IS_GROUP_HELPER(), GET_ITEM_GROUP_IN_CHARGE;
#
#	Date:	2012-09-25	YatWoon
#			update returnItemInfo(), display multi funding source
#
#	Date: 	2012-09-14	Rita
#			add enableMemberUpdateLocationRight() for customization flag
#
#	Date:	2012-07-03	YatWoon
#			update getCategoryName(), getCategoryLevel2Name(), returnFundingSource() add RecordStatus checking
#
#	Date:	2012-06-06	YAtWoon
#			add returnAdminGroupCode(), returnCategoryCode(), returnSubCategoryCode()
#
#	Date:	2012-05-25	YatWoon
#			update returnItemInfo(), add GroupID and FundingID
#			update DeleteLog(), add GroupID and FundingID
#
#	Date:	2012-05-23	YatWoon
#			add returnFundingSourceByFundingID(), returnGroupNameByGroupID()
#
#	Date:	2012-03-27	YatWoon
#			add returnOwnGroup()
#
#	Date:	2012-01-05	YatWoon
#			update retrieveWriteOffApprovalNumber(), if user is admin, should retrieve group id with INVENTORY_ADMIN_GROUP table and not with _MEMBER table
#			due to there may be no member in the group [Case#2012-0103-1353-22132]
#
#	Date:	2011-07-21	YatWoon
#			add ITEM_SPONSORING_BODY_FUNDING
#
#	Date:	2011-06-08	YatWoon
#			update returnItemInfo(), add category and subcategory checking
#
#	Date:	2011-06-04	YatWoon
#			add DeleteLog() [case#2011-0124-1006-09067]
#			update returnItemInfo(), return location, category, resource mgmt group
#			update GET_MODULE_OBJ_ARR(), add "Deletion Log" menu
#
#	Date:	2011-06-03	YatWoon
#			update menu checking, remove checking for variance group leader, replace of resoruce mgmt group leader
#
#	Date:	2011-05-31	YatWoon
#			add returnLocationStr()
#
#	Date:	2011-05-26	YatWoon
#			update returnExistingItems(), add Group Leader checking
#			update IS_ADMIN_USER(), add RecordType parameter
#
#	Date:	2011-05-25	YatWoon
#			update retriveGroupLeaderAddItemRight(), getStocktakePeriodEnd(), getStocktakePeriodEnd(), getWarrantyExpiryWarningDayPeriod(),
#			getBarcodeMaxLength(), getBarcodeFormat(), checkItemCodeFormatSetting(), retriveItemCodeFormat(), return data from session (not read file setting)
#			add retriveGroupLeaderNotAllowWriteOffItem()
#
#	DAte:	2011-05-19	YAtWoon
#			add returnCategoryName(), returnSubCategoryName(), returnItemNameByItemCode()
#			add new action constant ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING
#			update returnExistingItems(), add order by NameEng
#
#	Date:	2011-05-18	YatWoon
#			add returnItemType(), returnBulkItemLocationQty(),
#			add new action constant ITEM_ACTION_REMOVE_LOCATION_ITEM
#
#	Date:	2011-04-26	Yuen
#			improved checkImportStocktakeCode() for checking existence of bulk item using item code
#
#	Date:	2011-04-18	YatWoon
#			re-order menu display "stocktake variance handling" and "variance handling notice"
#
#	Date:	2011-04-07	YatWoon
#			- add returnFundingSource()
#
#	Date:	2011-03-15	YatWoon
#			- update extends libaccessright 
#			- update IS_GROUP_ADMIN(), add parameter for checking specific group
#
#################################################

if (!defined("LIBINVENTORY_DEFINED"))                  // Preprocessor directives
{
	define("LIBINVENTORY_DEFINED",true);
	
	# Constant definition
	define("DEFAULT_ITEM_CODE_NUM_LENGTH",				5);
	define("ITEM_TYPE_SINGLE",							1);
	define("ITEM_TYPE_BULK",							2);
    define("MANAGEMENT_GROUP_ADMIN",					1);
    define("MANAGEMENT_GROUP_MEMBER",					2);
    define("MANAGEMENT_GROUP_HELPER",					3);   
    define("ITEM_OWN_BY_SCHOOL",						1);
    define("ITEM_OWN_BY_GOVERNMENT",					2);
    define("ITEM_OWN_BY_DONOR",							3);
    define("ITEM_SCHOOL_FUNDING",						1);
    define("ITEM_GOVERNMENT_FUNDING",					2);
    define("ITEM_SPONSORING_BODY_FUNDING",				3);
    define("ITEM_STATUS_NORMAL",						1);
    define("ITEM_STATUS_DAMAGED",						2);
    define("ITEM_STATUS_REPAIR",						3);
    
    define("ITEM_ACTION_PURCHASE",						1);
    define("ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION",	2);
    define("ITEM_ACTION_STOCKTAKE_OTHER_LOCATION",		3);
    define("ITEM_ACTION_STOCKTAKE_NOT_FOUND",			4);
    define("ITEM_ACTION_WRITEOFF",						5);
    define("ITEM_ACTION_UPDATESTATUS",					6);
    define("ITEM_ACTION_CHANGELOCATION",				7);
	define("ITEM_ACTION_CHANGELOCATIONQTY",				8);
	define("ITEM_ACTION_MOVEBACKTOORGINIAL",			9);
	define("ITEM_ACTION_IGNORE_SURPLUS",				10);
	define("ITEM_ACTION_EDIT_ITEM",						11);
	define("ITEM_ACTION_REMOVE_LOCATION_ITEM",			12);		# Remove location item
	define("ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING",	13);	
	define("ITEM_ACTION_UPDATE_LOCATION_ITEM_AMOUNT",	14);
	define("ITEM_ACTION_WRITEOFF_RESTORE", 				15);		# restore approved/rejected write-off request
		
	define("DEFAULT_MAXIMUN_ITEM_INSERT_QTY",			30);
	define("WRITE_OFF_REASON_LOST",$i_InventorySystem_WriteOff_Reason_LostItem);
	define("ADMIN_GROUP_GENERAL",						1);
	define("ADMIN_GROUP_BULK",							2);
	define("DEFAULT_BARCODE_WIDTH",						160);
	define("DEFAULT_MINIMUM_BARCODE_WIDTH",				200);
	define("DEFAULT_MAXIMUN_BARCODE_WIDTH",				700);
	define("DEFAULT_BARCODE_WIDTH_INCREMENT",			50);

	define("ITEM_STOCKTAKE_STATUS_DONE",				1);
	define("ITEM_STOCKTAKE_STATUS_NOTDONE",				0);
	
	include_once("libaccessright.php");      
	class libinventory extends libaccessright {
	
	        function libinventory()
	        {
		        $this->ModuleName = "eInventory";
	                $this->libdb();
	                $this->uid = $_SESSION['UserID'];
	                
	                
			         if (!isset($_SESSION["SSV_PRIVILEGE"]["eInventory"]))
		         	{
						include_once("libgeneralsettings.php");
						$lgeneralsettings = new libgeneralsettings();
						
						$settings_ary = $lgeneralsettings->Get_General_Setting($this->ModuleName);
						
						if(!empty($settings_ary))
						{
							foreach($settings_ary as $key=>$data)
							{
								$_SESSION["SSV_PRIVILEGE"]["eInventory"][$key] = $data;
								$this->$key = $data; 
							}
						}
						else
						{
							$this->Leader_AddItemRight = false;
		                 	$this->stocktake_period_start = date('Y-m-d');
		                 	$this->stocktake_period_end = date('Y-m-d');
		                 	$this->warranty_expiry_warning = 5;
		                 	$this->barcode_max_length = 10;
		                 	$this->barcode_format = 1;
		                 	$this->itemcode_setting = "";
		                 	$this->itemCodeNumberOfDigit = 5;
		                 	$this->groupLeaderNowAllowWriteOffItem = 0;
		                 	$this->StockTakePriceSingle = 0;
		                 	$this->StockTakePriceBulk = 0;
						}
					}
					else
					{
						$this->Leader_AddItemRight = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["Leader_AddItemRight"];
						$this->stocktake_period_start = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["stocktake_period_start"];
						$this->stocktake_period_end = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["stocktake_period_end"];
		             	$this->StockTakePriceSingle = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["StockTakePriceSingle"];
		             	if ($this->StockTakePriceSingle=="")
		             	{
		             		$this->StockTakePriceSingle = 0;
		             	}
		             	$this->StockTakePriceBulk = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["StockTakePriceBulk"];
		             	if ($this->StockTakePriceBulk=="")
		             	{
		             		$this->StockTakePriceBulk = 0;
		             	}
						$this->warranty_expiry_warning = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["warranty_expiry_warning"];
						$this->barcode_max_length = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["barcode_max_length"];
						$this->barcode_format = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["barcode_format"];
						$this->itemcode_setting = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["itemcode_setting"];
						$this->itemCodeNumberOfDigit = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["itemCodeNumberOfDigit"];
						$this->ItemCodeSeparator = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["ItemCodeSeparator"];
						$this->groupLeaderNowAllowWriteOffItem = $_SESSION["SSV_PRIVILEGE"]["eInventory"]["groupLeaderNowAllowWriteOffItem"];
		             }
	        }
	
	        function GET_ADMIN_USER()
	        {
	                global $intranet_root;
	
	                $lf = new libfilesystem();
	                $AdminUser = trim($lf->file_read($intranet_root."/file/inventory/admin_user.txt"));
	
	                return $AdminUser;
	        }
	
	        function IS_ADMIN_USER($ParUserID='')
	        {
	                global $intranet_root;
	
	                if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"]) {
	                	return 1;
	                } else {
	                	return 0;
	                }
	        }
	        
	        function hasAccessRight($level=0)
	        {
	                global $intranet_inventory_admin, $intranet_inventory_usertype;
	                
	                if ($_SESSION["inventory"]["role"] != "")
	                {
	                    return true;
	                }
	                return false;
	        }   
	        
	        function getAccessLevel()
	        {
		        global $intranet_inventory_admin, $intranet_inventory_usertype;

		        if($_SESSION["inventory"]["role"] == "ADMIN")
		        {
			        return 1;
		        }
		        else if($_SESSION["inventory"]["role"] == "LEADER")
		        {
			        return 2;
		        }
		        else if($_SESSION["inventory"]["role"] == "MEMBER")
		        {
			        return 3;
		        }
		        else
		       	{
			       	return 0;
		       	}
	        }
	           
	        function IS_GROUP_ADMIN($AdminGroupID='')
	        {
		        $user_id = $this->uid;
		        $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '$user_id' AND RecordType = 1";
		        if(!empty($AdminGroupID))
		        	$sql .= " and AdminGroupID='$AdminGroupID' ";
		        $inventory_admin_group = $this->returnArray($sql,1);
		        
		        if(sizeof($inventory_admin_group)>0)
		        {
			        return true;
		        }
		        else
		        {
			        return false;
		        }
	        }
	        
	        function getInventoryAdminGroup($record_type='')
	        {
		        $cond = $record_type ? " and RecordType='$record_type'" : "";
		        $user_id = $this->uid;
		        $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '$user_id' $cond";
		        $inventory_admin_group = $this->returnVector($sql);
		        $inventory_admin_group_list = implode(",",$inventory_admin_group);
		        return $inventory_admin_group_list;
	        }
	        
	        function getGroupUserType($GroupID)
	        {
		        $user_id = $this->uid;
		        $sql = "SELECT AdminGroupID, RecordType FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE AdminGroupID IN ($GroupID) AND UserID = '$user_id'";
		        $temp = $this->returnArray($sql,2);
                return $temp;
            }
            
            function retrieveWriteOffApprovalNumber()
            {
	            $user_id = $this->uid;
	            if($this->IS_ADMIN_USER($user_id))
	            {
		            //$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER";
		            $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
		            $arr_group = $this->returnVector($sql);
		            if(sizeof($arr_group)>0)
		            {
			            $group_id_list = implode(",",$arr_group);
		            }
	            }
	            else
	            {
	            	$group_id_list = $this->getInventoryAdminGroup();
            	}
	            
	            $sql = "SELECT 
	            				COUNT(a.RecordID) 
	            		FROM 
	            				INVENTORY_ITEM_WRITE_OFF_RECORD AS a 
	            				INNER JOIN INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) 
	            				INNER JOIN INVENTORY_ADMIN_GROUP AS c ON (a.AdminGroupID = c.AdminGroupID)
	            		WHERE 
	            				a.RecordStatus = 0 AND
	            				c.AdminGroupID IN ($group_id_list)
	            				";
	            $temp = $this->returnVector($sql);
	            return $temp[0];
            }
            
            function retriveVarianceHandlingNotice()
            {
	            $user_id = $this->uid;
            	$sql = "SELECT 
            					COUNT(ReminderID) 
            			FROM 
            					INVENTORY_VARIANCE_HANDLING_REMINDER 
            			WHERE 
            					HandlerID = '$user_id' AND RecordStatus = 0
            			";
            					
            	$notice = $this->returnVector($sql);
            	return $notice[0];
        	}
	        	                
	        # added by ronald on 2007-10-05 #
	        function getInventoryNameByLang($prefix ="")
	        {
				global $intranet_session_language;
				$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($chi)
				{
					 $firstChoice = "NameChi";
					 $altChoice = "NameEng";
				}
				else
				{
					 $firstChoice = "NameEng";
					 $altChoice = "NameChi";
				}
		        $inventory_item_name_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
				
				return $inventory_item_name_field;
			}
			
	        function getInventoryItemNameByLang($prefix ="")
	        {
				global $intranet_session_language;
				$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($chi)
				{
					 $firstChoice = "NameChi";
					 $altChoice = "NameEng";
				}
				else
				{
					 $firstChoice = "NameEng";
					 $altChoice = "NameChi";
				}
		        $inventory_item_name_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
				
				return $inventory_item_name_field;
			}
			
			function getInventoryItemWriteOffReason($prefix ="")
	        {
				global $intranet_session_language;
				$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($chi)
				{
					 $firstChoice = "ReasonTypeNameChi";
					 $altChoice = "ReasonTypeNameEng";
				}
				else
				{
					 $firstChoice = "ReasonTypeNameEng";
					 $altChoice = "ReasonTypeNameChi";
				}
		        $inventory_item_write_off_reason = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
				
				return $inventory_item_write_off_reason;
			}
			
			# added by ronald on 2007-10-08 #
			function getInventoryDescriptionNameByLang($prefix ="")
	        {
				global $intranet_session_language;
				$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($chi)
				{
					 $firstChoice = "DescriptionChi";
					 $altChoice = "DescriptionEng";
				}
				else
				{
					 $firstChoice = "DescriptionEng";
					 $altChoice = "DescriptionChi";
				}
		        $inventory_description_name_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
				
				return $inventory_description_name_field;
			}
			# added by Kelvin on 2008-02-06 #
			function getInventoryItemLocationByLang($prefix ="")
	        {
				global $intranet_session_language;
				$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($chi)
				{
					 $firstChoice = "NameChi";
					 $altChoice = "NameEng";
				}
				else
				{
					 $firstChoice = "NameEng";
					 $altChoice = "NameChi";
				}
		        $inventory_item_name_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
				
				return $inventory_item_name_field;
			}
			# added by Kelvin on 2008-02-06 #
			function getInventoryItemRemarkByLang($prefix ="")
	        {
				global $intranet_session_language;
				$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($chi)
				{
					 $firstChoice = "NameChi";
					 $altChoice = "NameEng";
				}
				else
				{
					 $firstChoice = "NameEng";
					 $altChoice = "NameChi";
				}
		        $inventory_item_name_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
				
				return $inventory_item_name_field;
			}
			
			function getCategoryName($item_type = "1,2", $withItem=1, $RecordStatus="1")
			{
				if($withItem)
				{
					$sql = "SELECT 
									DISTINCT b.CategoryID,
									".$this->getInventoryNameByLang("b.")."
							FROM
									INVENTORY_ITEM AS a 
									INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID)
							WHERE
									a.ItemType IN ($item_type)
									and b.RecordStatus in ($RecordStatus)
							ORDER BY
									b.DisplayOrder
									";
				}
				else
				{
					$sql = "SELECT 
									DISTINCT CategoryID,
									".$this->getInventoryNameByLang("")."
							FROM
									INVENTORY_CATEGORY
							where
									RecordStatus in ($RecordStatus)
							ORDER BY
									DisplayOrder";
				}
				
				return $this->returnArray($sql,2);
			}
			
			function getCategoryLevel2Name($cat_id, $RecordStatus="1")
			{
				$sql = "SELECT 
								Category2ID,
								".$this->getInventoryNameByLang()."
						FROM
								INVENTORY_CATEGORY_LEVEL2
						WHERE
								CategoryID = '$cat_id'
								and RecordStatus in ($RecordStatus)
						ORDER BY
								DisplayOrder";
				
				return $this->returnArray($sql,2);
			}
			
			
			
			/*
			function getResponseMsg($msg) 
			{
				global $i_con_msg_add, $i_con_msg_update, $i_con_msg_delete, $i_con_msg_email;
	            global $i_con_msg_password1, $i_con_msg_password2, $i_con_msg_email1, $i_con_msg_email2;
	            global $i_con_msg_suspend, $i_con_msg_add_form_failed, $i_con_msg_delete_part, $i_con_msg_add_failed;
	            global $i_con_msg_update_failed, $i_con_msg_delete_failed;
	
	            $xmsg = "";
	            switch($msg){
	                        case 1: $xmsg = $i_con_msg_add; break;
	                        case 2: $xmsg = $i_con_msg_update; break;
	                        case 3: $xmsg = $i_con_msg_delete; break;
	                        case 4: $xmsg = $i_con_msg_email; break;
	                        case 5: $xmsg = $i_con_msg_password1; break;
	                M   M   case 6: $xmsg = $i_con_msg_password2; break;
	                        case 7: $xmsg = $i_con_msg_email1; break;
	                        case 8: $xmsg = $i_con_msg_email2; break;
	                        case 9: $xmsg = $i_con_msg_suspend; break;
	                        case 10: $xmsg = $i_con_msg_add_form_failed; break;
	                        case 11: $xmsg = $i_con_msg_delete_part; break;
	                        case 12: $xmsg = $i_con_msg_add_failed; break;
	                        case 13: $xmsg = $i_con_msg_delete_failed; break;
	                        case 14: $xmsg = $i_con_msg_update_failed; break;
				}
	            return $xmsg;
	        }
	        */
	        function getWarrantyExpiryWarningDayPeriod()
	        {
		        /*
		        global $intranet_root;
	
	            $lf = new libfilesystem();
	            
	            $file_content = trim($lf->file_read($intranet_root."/file/inventory/others_setting.txt"));
	                            
	            if(!empty($file_content))
	            {
					$Arr_OthersInfo = explode(",", $file_content);	
	                //$WarningDayPeriod = $Arr_OthersInfo[2];
	                $WarningDayPeriod = $Arr_OthersInfo[0];
	            }
	            else
	            {
		            $WarningDayPeriod = 5;
	            }
	
	            return $WarningDayPeriod;
	            */
	            return $this->warranty_expiry_warning;
	        }
	        
	        function getBarcodeMaxLength()
	        {
		        /*
		        global $intranet_root;
	
	            $lf = new libfilesystem();
	            
	            $file_content = trim($lf->file_read($intranet_root."/file/inventory/others_setting.txt"));
	                            
	            if(!empty($file_content))
	            {
					$Arr_OthersInfo = explode(",", $file_content);	
					$BarcodeMaxLength = $Arr_OthersInfo[1];
	            }
	            else
	            {
		            $BarcodeMaxLength = 10;
	            }
	
	            return $BarcodeMaxLength;
	            */
	            
	            return $this->barcode_max_length;
	        }
	        
	        function getBarcodeFormat()
	        {
		        /*
		        global $intranet_root;
	
	            $lf = new libfilesystem();
	            
	            $file_content = trim($lf->file_read($intranet_root."/file/inventory/others_setting.txt"));
	                            
	            if(!empty($file_content))
	            {
					$Arr_OthersInfo = explode(",", $file_content);	
					$BarcodeFormat = $Arr_OthersInfo[2];
	            }
	            else
	            {
		            $BarcodeFormat = 1;
	            }
	            
	            return $BarcodeFormat;
	            */
	            
	            return $this->barcode_format;
	        }
	        
	        function random_barcode()
	        {
		        # $possible = "0123456789";
		        # $possible = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
		        $barcode_format = $this->getBarcodeFormat();
		        if($barcode_format == 1)
		        {
			        $possible = "0123456789";
		        }
		        if($barcode_format == 2)
		        {
			        $possible = "0123456789ABCDEFGHIJKMNPQRSTUVWXYZ";
		        }
		        $str = "";
		        $length = $this->getBarcodeMaxLength();
		        while(strlen($str)<$length){
		                mt_srand((double)microtime()*1000000);
		                
		                do
		                {
		                	$new_char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
	                	}while($new_char=="0" && strlen($str)==0);

		                $str .= $new_char;
		                
		        }
		        return $str;
			}
			
			function getStocktakePeriodStart()
			{
				/*
				global $intranet_root;
	
				include_once($intranet_root."/includes/libfilesystem.php");
				
	            $lf = new libfilesystem();
	            
	            $file_content = trim($lf->file_read($intranet_root."/file/inventory/stocktake_setting.txt"));
	                            
	            if(!empty($file_content))
	            {
					$Arr_Stocktake_Period = explode(",", $file_content);	
					$StocktakePeriodStart = $Arr_Stocktake_Period[0];
	            }
	            else
	            {
		            $StocktakePeriodStart = "";
	            }
	            
	            return $StocktakePeriodStart;
	            */
	            
	            return $this->stocktake_period_start;
            }
            
            function getStocktakePeriodEnd()
			{
				/*
				global $intranet_root;
	
	            $lf = new libfilesystem();
	            
	            $file_content = trim($lf->file_read($intranet_root."/file/inventory/stocktake_setting.txt"));
	                            
	            if(!empty($file_content))
	            {
					$Arr_Stocktake_Period = explode(",", $file_content);	
					$StocktakePeriodEnd = $Arr_Stocktake_Period[1];
	            }
	            else
	            {
		            $StocktakePeriodEnd = "";
	            }
	            
	            return $StocktakePeriodEnd;
	            */
	            
	            return $this->stocktake_period_end;
            }
            
            function getStockTakePriceSingle()
			{
				
	            
	            return $this->StockTakePriceSingle;
            }
            
            function getStockTakePriceBulk()
			{
				
	            return $this->StockTakePriceBulk;
            }
            
            function retrieveVarianceManagerGroup()
            {
	            $user_id = $this->uid;
	            
		        $sql = "SELECT 
		        				distinct(a.AdminGroupID)
		        		FROM 
		        				INVENTORY_ADMIN_GROUP_MEMBER AS a INNER JOIN 
		        				INVENTORY_ITEM_BULK_EXT AS b ON (a.AdminGroupID = b.BulkItemAdmin) 
		        		WHERE a.UserID = '$user_id' AND a.RecordType = 1";
		        		
		        $arr_temp = $this->returnVector($sql,1);
		        if(sizeof($arr_temp)>0)
		        	$variance_manager_group_list = implode(",",$arr_temp);
                return $variance_manager_group_list;
            }
            
            function checkItemCodeFormatSetting()
            {
	            /*
	            global $intranet_root;
	
				include_once($intranet_root."/includes/libfilesystem.php");
				
				$lf = new libfilesystem();
				
				$content = trim($lf->file_read($intranet_root."/file/inventory/itemcode_format_settings.txt"));
	                            
	            if(!empty($content))
	            {
					return true;
	            }
	            else
	            {
		            return false;
	            }
	            
	            return false;
	            */
	            
	            return $this->itemcode_setting ? true : false;
            }
            
            function retriveItemCodeFormat()
            {
	            /*
	            global $intranet_root;
	
				include_once($intranet_root."/includes/libfilesystem.php");
				
				$lf = new libfilesystem();
				
				$content = trim($lf->file_read($intranet_root."/file/inventory/itemcode_format_settings.txt"));
	                            
	            if(!empty($content))
	            {
					$arr_ItemCodeFormat = explode(",", $content);
	            }
	            else
	            {
		            $arr_ItemCodeFormat = "";
	            }
	            return $arr_ItemCodeFormat;
	            */
	            
	            if(!empty($this->itemcode_setting))
	            	$arr_ItemCodeFormat = explode(",", $this->itemcode_setting);
	            return $arr_ItemCodeFormat;
            }
            
            function retriveGroupLeaderAddItemRight()
            {
				/*
	            global $intranet_root;
				
				include_once($intranet_root."/includes/libfilesystem.php");

				$lf = new libfilesystem();
				
				$content = trim($lf->file_read($intranet_root."/file/inventory/group_leader_right.txt"));
	                            
	            if(!empty($content))
	            {
					$addItemRight = $content;
	            }
	            else
	            {
		            $addItemRight = "0";
	            }
	            return $addItemRight;
	            */
	            
	           return $this->Leader_AddItemRight;
            }
            
            function getItemCodeDigitLength()
            {
                // get item code number length
                return $this->itemCodeNumberOfDigit;
            }
            
            function getItemCodeSeparator(){
                return $this->ItemCodeSeparator;
            }
            
            function checkBarcodeSettingChanged()
            {
	            $sql = "SELECT LogID, MaxLengthChanged, FormatChanged FROM INVENTORY_BARCODE_SETTING_LOG WHERE RecordStatus = 0 ORDER BY DateInput DESC LIMIT 0,1";
	            $arr_result = $this->returnArray($sql,3);
	            
	            if(sizeof($arr_result) == 1){
		            list($logID, $maxLengthChanged, $formatChanged) = $arr_result[0];
		            if(($maxLengthChanged == 1) && ($formatChanged == 1)){
			            return 3;
		            }else if($maxLengthChanged == 1){
			            return 2;
		            }else if($formatChanged == 1){
			            return 1;
		            }else{
			            return 0;
		            }
	            }else{
		            return 0;
	            }
	            return 0;
            }
            
            function checkImportStocktakeLocation($FullLocationCode){
            	if(strpos($FullLocationCode,"+")>0)
				{
					# if '+' detected, check location code is valid
					$LocationArray = explode("+",$FullLocationCode);
									
					if(sizeof($LocationArray)>0)
					{
						$BuildingCode = $LocationArray[0];
						$LocationCode = $LocationArray[1];
						$SubLocationCode = $LocationArray[2];
						
						$sql = "SELECT 
									COUNT(*) 
								FROM 
									INVENTORY_LOCATION AS c INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS b ON (c.LocationLevelID = b.LocationLevelID) INNER JOIN 
									INVENTORY_LOCATION_BUILDING AS a ON (b.BuildingID = a.BuildingID) 
								WHERE 
									a.Barcode='$BuildingCode' AND b.Barcode='$LocationCode' AND c.Barcode='$SubLocationCode' AND a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
						$LocationExist = $this->returnVector($sql);
						if($LocationExist[0] == 1)
							return true;
						else
							return false;
					}
				}else{
					return false;
				}
            }
            
            function checkImportStocktakeCode($Code, $itemType=1){
            	
            	# (1) check existence in single item first
            	## Barcode detected - Single item
				## Check ItemID
				$Code = trim($Code);
				$check = "";
				
				// check if the barcode is start & end w/'*', if yes, then remove the '*'
				if((strpos($Code,"*") == 0) && (strrpos($Code,"*") == strlen($Code)-1))
				{
					$Code = substr($Code,1,strlen($Code)-2);
				}
				
				if($itemType == ITEM_TYPE_SINGLE) {
				    $sql = "SELECT item.RecordStatus
                           FROM INVENTORY_ITEM_SINGLE_EXT AS a
                           LEFT JOIN INVENTORY_ITEM As item ON (item.ItemID = a.ItemID)
                           WHERE a.TagCode = '". $Code ."'";
				    $arrItem = $this -> returnVector($sql);
				    list($status) = $arrItem;
				    if(sizeof($arrItem) > 0){
				        if($status == 0){
				            // record status in inventory item is 0 = single ext status is 0 (write off)
				            $check = 'writeOff';
				        }else{
				            $sql2 ="SELECT a.ItemID
    				                FROM INVENTORY_ITEM_SINGLE_EXT AS a
    				         	    LEFT JOIN INVENTORY_ITEM_WRITE_OFF_RECORD AS wor ON (wor.ItemID = a.ItemID)
    				                WHERE a.TagCode = '". $Code. "' AND wor.RecordStatus = 1 ";
				            $arrItemID2 = $this->returnVector($sql2);
				            if(sizeof($arrItemID2) > 0){
				                $check = 'writeOff';
				            }else{
				                $check = 'success';
				            }
				        }
				    }else{
				        // record does not exist
				        $check = 'fail';
				    }
				}
				
				/*
            	# (2) check existence in bulk item first
				## Manually input - Bulk item 
				## Check ItemID
				$Code = str_replace("+","_",trim($Code));
				
				$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$Code'";
				$arrItemID = $this->returnVector($sql);
				if(sizeof($arrItemID)>0)
					return true;
				else
					return false;  
				*/
				# (2) check existence in bulk item
				if($itemType == ITEM_TYPE_BULK) {
				    $sql = "SELECT item.RecordStatus
                               FROM INVENTORY_ITEM_BULK_EXT AS a
                               LEFT JOIN INVENTORY_ITEM As item ON (item.ItemID = a.ItemID)
                               WHERE a.BarCode = '". $Code ."'";
				    $arrItem = $this -> returnVector($sql);
				    list($status) = $arrItem;
				    if(sizeof($arrItem) > 0){
				        if($status == 0){
				            $check = 'writeOff';
				        }else{
				            $sql2 ="SELECT a.ItemID
        				                FROM INVENTORY_ITEM_BULK_EXT AS a
        				         	    LEFT JOIN INVENTORY_ITEM_WRITE_OFF_RECORD AS wor ON (wor.ItemID = a.ItemID)
        				                WHERE a.BarCode = '". $Code. "' AND wor.RecordStatus = 1 ";
				            $arrItemID2 = $this->returnVector($sql2);
				            if(sizeof($arrItemID2) > 0){
				                $check = 'writeOff';
				            }else{
				                $check = 'success';
				            }
				        }
				    }else{
				        $check = 'fail';
				    }
				}
            	/*
            	if(strpos($Code,"+") > 0)
				{
					## Manually input - Bulk item 
					## Check ItemID
					$Code = str_replace("+","_",trim($Code));
					
					$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$Code'";
					$arrItemID = $this->returnVector($sql);
					if(sizeof($arrItemID)>0)
						return true;
					else
						return false;
				}
				else
				{
					## Barcode detected - Single item
					## Check ItemID
					$Code = trim($Code);
					
					// check if the barcode is start & end w/'*', if yes, then remove the '*'
					if((strpos($Code,"*") == 0) && (strrpos($Code,"*") == strlen($Code)-1))
					{
						$Code = substr($Code,1,strlen($Code)-2);
					}
					
					$sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '$Code'";
					$arrItemID = $this->returnVector($sql);
					if(sizeof($arrItemID)>0)
						return true;
					else
						return false;
				}
				*/
				return $check;
            }
            
            function checkImportStocktakeAdminGroup($Barcode) {
            	$Barcode = $this->trimBarcodeStars($Barcode);
            	
            	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Barcode='".$Barcode."'";
            	$groupIdArr = $this->returnVector($sql);
            	if(sizeof($groupIdArr)>0) {
            		return $groupIdArr[0];
            	}
            	return false;
            }
            
            // for bulk item
            function checkImportStocktakeItemOwnedByAdminGroup($Barcode,$GroupCode) {
            	$Barcode = $this->trimBarcodeStars($Barcode);
            	$GroupCode = $this->trimBarcodeStars($GroupCode);
            	
            	$sql = "SELECT
							COUNT(c.AdminGroupID)  
						FROM INVENTORY_ITEM_BULK_EXT as a 
						INNER JOIN INVENTORY_ITEM_BULK_LOCATION as b ON a.ItemID=b.ItemID 
						INNER JOIN INVENTORY_ADMIN_GROUP as c ON c.AdminGroupID=b.GroupInCharge 
						WHERE a.Barcode='$Barcode' AND c.Barcode='$GroupCode'";
				$result = $this->returnVector($sql);
				
				return $result[0]>0;
            }
            
            // for bulk item
            function getImportStocktakeItemAdminGroup($Barcode) {
            	global $intranet_session_language;
            	
            	$Barcode = $this->trimBarcodeStars($Barcode);
            	
            	$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($lang)
				{
					$firstChoice = "c.NameChi";
					$altChoice = "c.NameEng";
				}
				else
				{
					$firstChoice = "c.NameEng";
					$altChoice = "c.NameChi";
				}
				
				$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";
            	
            	$sql = "SELECT DISTINCT 
							c.AdminGroupID,
							$namefield as AdminGroupName,
							c.Barcode 
						FROM INVENTORY_ITEM_BULK_EXT as a 
						INNER JOIN INVENTORY_ITEM_BULK_LOCATION as b ON a.ItemID=b.ItemID 
						INNER JOIN INVENTORY_ADMIN_GROUP as c ON c.AdminGroupID=b.GroupInCharge 
						WHERE a.Barcode = '$Barcode'";
						
				$result = $this->returnArray($sql);
				return $result;
            }
            
            function getAdminGroupByBarcode($Barcode)
            {
            	global $intranet_session_language;
            	
            	$Barcode = $this->trimBarcodeStars($Barcode);
            	
            	$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($lang)
				{
					$firstChoice = "c.NameChi";
					$altChoice = "c.NameEng";
				}
				else
				{
					$firstChoice = "c.NameEng";
					$altChoice = "c.NameChi";
				}
				
				$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";
            	
            	$sql = "SELECT
							c.AdminGroupID,
							$namefield as AdminGroupName,
							c.Barcode 
						FROM INVENTORY_ADMIN_GROUP as c
						WHERE c.Barcode = '$Barcode'";
						
				$result = $this->returnArray($sql);
				return $result;
            }
            
            function checkImportStocktakeFundingSource($Barcode) {
            	$Barcode = $this->trimBarcodeStars($Barcode);
				
				$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE Barcode='".$Barcode."'";
            	$fundingSourceIdArr = $this->returnVector($sql);
            	if(sizeof($fundingSourceIdArr)>0) {
            		return $fundingSourceIdArr[0];
            	}
            	return false;
            }
            
            // for bulk item
            function checkImportStocktakeItemFundingSource($Barcode,$FundingSourceCode) {
            	$ItemCode = $this->trimBarcodeStars($ItemCode);
            	$FundingSourceCode = $this->trimBarcodeStars($FundingSourceCode);
            	
            	$sql = "SELECT 
							COUNT(c.FundingSourceID)  
						FROM INVENTORY_ITEM_BULK_EXT as a 
						INNER JOIN INVENTORY_ITEM_BULK_LOCATION as b ON a.ItemID=b.ItemID 
						INNER JOIN INVENTORY_FUNDING_SOURCE as c ON c.FundingSourceID=b.FundingSourceID 
						WHERE a.Barcode = '$Barcode' AND c.Barcode='$FundingSourceCode'";
				$result = $this->returnVector($sql);
				return $result[0]>0;
            }
            
            // for bulk item
            function getImportStocktakeItemFundingSource($Barcode) {
            	global $intranet_session_language;
            	
            	$Barcode = $this->trimBarcodeStars($Barcode);
            	
            	$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($lang)
				{
					$firstChoice = "c.NameChi";
					$altChoice = "c.NameEng";
				}
				else
				{
					$firstChoice = "c.NameEng";
					$altChoice = "c.NameChi";
				}
				
				$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";
            	
            	$sql = "SELECT DISTINCT 
							c.FundingSourceID,
							$namefield as FundingSourceName,
							c.Barcode 
						FROM INVENTORY_ITEM_BULK_EXT as a 
						INNER JOIN INVENTORY_ITEM_BULK_LOCATION as b ON a.ItemID=b.ItemID 
						INNER JOIN INVENTORY_FUNDING_SOURCE as c ON c.FundingSourceID=b.FundingSourceID 
						WHERE a.Barcode = '$Barcode'";
				
				$result = $this->returnArray($sql);
				return $result;
            }
            
            function getFundingSourceByBarcode($Barcode)
            {
            	global $intranet_session_language;
            	
            	$Barcode = $this->trimBarcodeStars($Barcode);
            	
            	$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
				if ($lang)
				{
					$firstChoice = "c.NameChi";
					$altChoice = "c.NameEng";
				}
				else
				{
					$firstChoice = "c.NameEng";
					$altChoice = "c.NameChi";
				}
				
				$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";
            	
            	$sql = "SELECT
							c.FundingSourceID,
							$namefield as FundingSourceName,
							c.Barcode 
						FROM INVENTORY_FUNDING_SOURCE as c 
						WHERE c.Barcode = '$Barcode'";
				
				$result = $this->returnArray($sql);
				return $result;
            }
            
            function checkImportStocktakeDate($StocktakeDate){
            	if(checkDateIsValid($StocktakeDate) == true)
				{
		            if(($this->getStocktakePeriodStart() <= $StocktakeDate) && ($StocktakeDate <= $this->getStocktakePeriodEnd()))
		            {
		            	return 1;
		            }
		            else
		            {
		            	//$error_msg[] = "Stocktake date is not valid";
		            	return -1;
		            }
				}
				else
				{
					//$error_msg[] = "Stocktake date format is not valid";
					return -2;
				}
            }
            
            function checkImportStocktakeTime($StocktakeTime){
            	if(checkTimeIsValid($StocktakeTime) == true)
					return true;
				else
					return false;
            }
            
            function getImportStocktakeItem($Code){
				
				# (1) check existence in single item first
					### Single Item (Barcode)
					if((strpos($Code,"*") == 0) && (strrpos($Code,"*") == strlen($Code)-1))
					{
						$Code = substr($Code,1,strlen($Code)-2);
					}
					
					## Barcode detected - Single item
					## Check ItemID
					$Code = trim($Code);
					
					$sql = "SELECT a.ItemID, ".$this->getInventoryNameByLang("a.")."  FROM INVENTORY_ITEM AS a INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) WHERE b.TagCode = '$Code'";
					$arrItemInfo = $this->returnArray($sql,2);
					
					if (sizeof($arrItemInfo)>0)					
						return $arrItemInfo;
				/*	
				# (2) check existence in bulk item first
					## Manually input - Bulk item (Item Code) 
					## Check ItemID
					$Code = str_replace("+","_",trim($Code));
					
					$sql = "SELECT ItemID, ".$this->getInventoryNameByLang()." FROM INVENTORY_ITEM WHERE ItemCode = '$Code'";
					$arrItemInfo = $this->returnArray($sql,2);
				*/
				# (2) check existence in bulk item
					$sql = "SELECT a.ItemID, ".$this->getInventoryNameByLang("a.")." FROM INVENTORY_ITEM AS a INNER JOIN INVENTORY_ITEM_BULK_EXT AS b ON b.ItemID=a.ItemID WHERE b.Barcode = '$Code'";
					$arrItemInfo = $this->returnArray($sql,2);
					
					return $arrItemInfo;
					
					
					/*
            	if(strpos($Code,"+") > 0)
				{
					## Manually input - Bulk item (Item Code) 
					## Check ItemID
					$Code = str_replace("+","_",trim($Code));
					
					$sql = "SELECT ItemID, ".$this->getInventoryNameByLang()." FROM INVENTORY_ITEM WHERE ItemCode = '$Code'";
					$arrItemInfo = $this->returnArray($sql,2);

					return $arrItemInfo;
				}
				else
				{
					### Single Item (Barcode)
					if((strpos($Code,"*") == 0) && (strrpos($Code,"*") == strlen($Code)-1))
					{
						$Code = substr($Code,1,strlen($Code)-2);
					}
					
					## Barcode detected - Single item
					## Check ItemID
					$Code = trim($Code);
					
					$sql = "SELECT a.ItemID, ".$this->getInventoryNameByLang("a.")."  FROM INVENTORY_ITEM AS a INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) WHERE b.TagCode = '$Code'";
					$arrItemInfo = $this->returnArray($sql,2);
					
					return $arrItemInfo;
				}
				*/
            }
            
            function getImportStocktakeLocation($FullLocationCode){
            	if(strpos($FullLocationCode,"+")>0)
				{
					# if '+' detected, check location code is valid
					$LocationArray = explode("+",$FullLocationCode);
									
					if(sizeof($LocationArray)>0)
					{
						$BuildingCode = $LocationArray[0];
						$LocationCode = $LocationArray[1];
						$SubLocationCode = $LocationArray[2];
						
						$sql = "SELECT 
									CONCAT(a.Code,'>',b.Code,'>',c.Code)
								FROM 
									INVENTORY_LOCATION AS c INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS b ON (c.LocationLevelID = b.LocationLevelID) INNER JOIN 
									INVENTORY_LOCATION_BUILDING AS a ON (b.BuildingID = a.BuildingID) 
								WHERE 
									a.Barcode='$BuildingCode' AND b.Barcode='$LocationCode' AND c.Barcode='$SubLocationCode' AND a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
						
						$arrLocationInfo = $this->returnArray($sql);
						return $arrLocationInfo;
					}
				}
            }
            
            function getImportStocktakeLocationID($FullLocationCode){
            	
            	if(strpos($FullLocationCode,"+")>0)
				{
					# if '+' detected, check location code is valid
					$LocationArray = explode("+",$FullLocationCode);
					
					if(sizeof($LocationArray)>0)
					{
						$BuildingCode = $LocationArray[0];
						$LocationCode = $LocationArray[1];
						$SubLocationCode = $LocationArray[2];
						
						$sql = "SELECT 
									a.BuildingID,
									b.LocationLevelID,
									c.LocationID
								FROM 
									INVENTORY_LOCATION AS c INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS b ON (c.LocationLevelID = b.LocationLevelID) INNER JOIN 
									INVENTORY_LOCATION_BUILDING AS a ON (b.BuildingID = a.BuildingID) 
								WHERE 
									a.Barcode='$BuildingCode' AND b.Barcode='$LocationCode' AND c.Barcode='$SubLocationCode' AND a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
						$arrLocationID = $this->returnArray($sql);
						return $arrLocationID;
					}
				}
            }

            function checkItemInCorrectLocation($ItemCode,$FullLocationCode)
            {
            	if(strpos($ItemCode,"+") > 0)
				{
					# Bulk Item
					$arrLocationID = $this->getImportStocktakeLocationID($FullLocationCode);
					
					$ItemCode = str_replace("+","_",trim($ItemCode));
					$sql = "SELECT 
								a.ItemID 
							FROM 
								INVENTORY_ITEM AS a INNER JOIN 
								INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID)
							WHERE 
								a.ItemCode = '$ItemCode' AND b.LocationID = ".$arrLocationID[0][2]
							;
					$result = $this->returnArray($sql,1);
					
					if(sizeof($result)>0){
						return true;
					}else{
						return false;
					}
				}else{
					## Single item, pass this checking
					return true;
				}
            }
            
            function Check_Can_Delete_Category($CategoryID='', $Category2ID='')
	        {
	        	$cond_CategoryID = '';
	        	if ($CategoryID != '')
	        		$cond_CategoryID = " And CategoryID = '$CategoryID' ";
	        		
	        	$cond_Category2ID = '';
	        	if ($Category2ID != '')
	        		$cond_Category2ID = " And Category2ID = '$Category2ID' ";
	        	
	        	$sql = "(	
							Select 
									ItemID
							From
									INTRANET_EBOOKING_ITEM
							Where
									RecordStatus = 1
									$cond_CategoryID
									$cond_Category2ID
						)
						Union
						(
							Select 
									ItemID
							From
									INVENTORY_ITEM
							Where
									RecordStatus = 1
									$cond_CategoryID
									$cond_Category2ID
						)";
				$ReturnArr = $this->returnArray($sql);
				
				if (count($ReturnArr) > 0)
					$canDelete = false;
				else
					$canDelete = true;
					
				return $canDelete;
	        }
            
	
	        /*
	        * Get MODULE_OBJ array
	        */
	        function GET_MODULE_OBJ_ARR()
	        {
					global $intranet_inventory_admin, $intranet_inventory_usertype, $Lang;
	                global $PATH_WRT_ROOT, $i_InventorySystem, $CurrentPage, $LAYOUT_SKIN, $ip20TopMenu;
	                global $sys_custom, $button_search, $CurrentPageArr, $button_new, $button_import;
	                global $i_InventorySystem;
	                global $i_InventorySystem_Report_ItemDetails, $i_InventorySystem_Report_WarrantyExpiry,$i_InventorySystem_Report_ItemNotStockCheck;
	                global $i_InventorySystem_Item_Registration, $i_InventorySystem_Report_ItemFinishStockCheck;
	                global $i_InventorySystem_WriteOffItemApproval, $i_InventorySystem_Report_ItemWrittenOff, $i_InventorySystem_StocktakeVarianceHandling;
	                global $i_InventorySystem_Report_FixedAssetsRegister, $i_InventorySystem_Report_ItemStatus;
	                global $i_InventorySystem_StockTakeResult, $i_InventorySystem_BarcodeLabels, $i_InventorySystem_Report_Stocktake, $i_InventorySystem_Report_StocktakeProgress;
	
	                # Current Page Information init
	                $PageManagement_InventoryList = 0;
	                $PageManagement_InventoryList_View = 0;
	                $PageManagement_InventoryList_Category = 0;
	                $PageManagement_InventoryList_Location = 0;
	                $PageManagement_InventoryList_Group = 0;
	                $PageManagement_InventoryList_FundingSource = 0;
	                $PageManagement_InventoryList_Search = 0;
	                $PageManagement_InventoryList_New = 0;
	                $PageManagement_InventoryList_Import = 0;
	
	                $PageManagement_Stocktake = 0;
	
	                $PageReport = 0;
	                $PageReport_StockTake = 0;
	                $PageReport_StocktakeProgress = 0;
	                $PageManagement_BarcodeLabels = 0;
	                $PageReport_WarrantyExpiry = 0;
	                $PageReport_ItemFinishStockCheck = 0;
	                $PageReport_ItemNotStockCheck = 0;
	                $PageReport_WrittenOffItems = 0;
	                $PageReport_InventoryStatus = 0;
	                $PageReport_FixedAssetsRegister = 0;
	                $PageReport_FixedAssetsRegister_PLKCHC = 0;
	                
	                $PageManagement =0;
	                $PageManagement_StockTakeResult = 0;
	                $PageManagement_WriteOffApproval = 0;
	                $PageManagement_WriteOffApproval_PLKCHC = 0;
	                $PageManagement_StocktakeVarianceHandling = 0;
	                $PageManagement_VarianceHandlingNotice = 0;
	
	                $PageSettings = 0;
	                $PageSettings_Others = 0;
	                $PageSettings_Category = 0;
	                $PageSettings_Location = 0;
	                $PageSettings_Group = 0;
	                $PageSettings_FundingSource = 0;
	                $PageSettings_WriteOffReason = 0;
	                $PageReports_DeleteLog = 0;
	                $PageReports_StocktakeListReport = 0;
	                $PageReports_StocktakeListReport_PLKCHC = 0;
	                
	                
	                if(!$this->hasAccessRight())  
	                {
	                	$CurrentPageArr['eInventory'] = 0;
						//$CurrentPageArr['eServiceeSports'] = 1;
					}
					else
					{
						$CurrentPageArr['eInventory'] = 1;
					}
					
	                switch ($CurrentPage) {
		                	case "Management_InventoryList":
		                			$PageManagement = 1;
		                			$PageManagement_InventoryList = 1;
		                			break;
	                        case "Management_BarcodeLabels":
	                                $PageManagement = 1;
	                                $PageManagement_BarcodeLabels = 1;
	                                break;
	                        case "Management_Stocktake":
	                                $PageManagement = 1;
	                                $PageManagement_Stocktake = 1;
	                                break;
	                        case "Management_VarianceHandlingNotice":
	                        		$PageManagement = 1;
	                        		$PageManagement_VarianceHandlingNotice = 1;
	                        		break;
	                        case "Management_StocktakeVarianceHandling":
	                                $PageManagement = 1;
	                                $PageManagement_StocktakeVarianceHandling = 1;
	                                break;
	                        case "Management_WriteOffApproval":
	                                $PageManagement = 1;
	                                $PageManagement_WriteOffApproval = 1;
	                                break;
	                                
	                        case "Report":
	                                $PageReport = 1;
	                                break;
	                       	case "Report_StockTake":
	                                $PageReport = 1;
	                                $PageReport_StockTake = 1;
	                                break;                          
	                        case "Report_StockTakeProgress":
	                                $PageReport = 1;
	                                $PageReport_StockTakeProgress = 1;
	                                break;                               
	                        case "Report_InventoryStatus":
	                        		$PageReport = 1;
	                        		$PageReport_InventoryStatus = 1;
	                        		break;
	                        case "Report_WrittenOffItems":
	                        		$PageReport = 1;
	                        		$PageReport_WrittenOffItems = 1;
	                        		break;
	                        case "Report_WarrantyExpiry":
	                        		$PageReport = 1;
	                        		$PageReport_WarrantyExpiry = 1;
	                        		break;
	                        case "Report_FixedAssetsRegister":
	                        		$PageReport = 1;
	                        		$PageReport_FixedAssetsRegister = 1;
	                        		break;
	                        case "Reports_DeleteLog":
									$PageReports = 1;
									$PageReports_DeleteLog = 1;
									break;
					 		case "Reports_StocktakeListReport":
									$PageReports = 1;
									$PageReports_StocktakeListReport = 1;
									break;

									
	                        case "Settings_BasicSettings":
	                                $PageSettings = 1;
	                                $PageSettings_BasicSettings = 1;
	                                break;		
	                        case "Settings_Category":
	                                $PageSettings = 1;
	                                $PageSettings_Category = 1;
	                                break;
	                        case "Settings_Location":
	                                $PageSettings = 1;
	                                $PageSettings_Location = 1;
	                                break;
	                        case "Settings_Group":
	                                $PageSettings = 1;
	                                $PageSettings_Group = 1;
	                                break;
	                        case "Settings_FundingSource":
	                                $PageSettings = 1;
	                                $PageSettings_FundingSource = 1;
	                                break;
	                        case "Settings_WriteOffReason":
	                                $PageSettings = 1;
	                                $PageSettings_WriteOffReason = 1;
	                                break;
	                        case "Settings_Others":
	                                $PageSettings = 1;
	                                $PageSettings_Others = 1;
	                                break;
	                                
	                        case "Management_WriteOffApproval_PLKCHC":
	                            $PageCustomizedReports = 1;
	                            $PageManagement_WriteOffApproval_PLKCHC = 1;
	                            break;
	                        case "Report_FixedAssetsRegister_PLKCHC":
	                            $PageCustomizedReports = 1;
	                            $PageReport_FixedAssetsRegister_PLKCHC = 1;
	                            break;
	                        case "Reports_StocktakeListReport_PLKCHC":
	                            $PageCustomizedReports = 1;
	                            $PageReports_StocktakeListReport_PLKCHC = 1;
	                            break;
	                }
	
	                /*
	                structure:
	                $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
	                $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
	                */
	
	                # Menu information
	                $MenuArr["Management"] = array($i_InventorySystem['PageManagement'], $PATH_WRT_ROOT."", $PageManagement);
	                $MenuArr["Management"]["Child"]["View"] = array($i_InventorySystem['Inventory'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1", $PageManagement_InventoryList);
	                $MenuArr["Management"]["Child"]["BarcodeLabels"] = array($i_InventorySystem_BarcodeLabels, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/barcode_labels/item_details.php", $PageManagement_BarcodeLabels);
	                
//	                $curr_date = date("Y-m-d");
//	                if(($this->getStocktakePeriodStart() <= $curr_date) && ($curr_date <= $this->getStocktakePeriodEnd()))
//	                {
	                	$MenuArr["Management"]["Child"]["StockTake"] = array($i_InventorySystem['StockTake'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/", $PageManagement_Stocktake);
//                	}
	                
//	                if($curr_date > $this->getStocktakePeriodEnd())
//	                {
		                //if(($this->getAccessLevel() == 1) || ($this->retrieveVarianceManagerGroup() != "") )
		                if(($this->getAccessLevel() == 1) || ($this->IS_GROUP_ADMIN()) )
		                {
		                	$MenuArr["Management"]["Child"]["StockTakeVarianceHandling"] = array($i_InventorySystem_StocktakeVarianceHandling, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake_variance_handling/outstanding_items.php", $PageManagement_StocktakeVarianceHandling);
	                	}
//	                }
	                
	                $MenuArr["Management"]["Child"]["VarianceHandlingNotice"] = array($i_InventorySystem['VarianceHandlingNotice'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/variance_handling_notice/",$PageManagement_VarianceHandlingNotice);
                	
	                //$groupLeaderNowAllowWriteOffItem = $this->retriveGroupLeaderNotAllowWriteOffItem();
	                if(($this->getAccessLevel() == 1) || ($this->getAccessLevel() == 2/* && !$groupLeaderNowAllowWriteOffItem*/))
	                {	                	
	                	$MenuArr["Management"]["Child"]["PageManagement_WriteOffApproval"] = array($i_InventorySystem_WriteOffItemApproval, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/write_off_approval/write_off_item.php", $PageManagement_WriteOffApproval);
                	}
	                
	                $MenuArr["Report"] = array($i_InventorySystem['Report'], $PATH_WRT_ROOT."", $PageReport);
	                $MenuArr["Report"]["Child"]["PageReport_StockTake"] = array($i_InventorySystem_Report_Stocktake, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/stocktake_result.php", $PageReport_StockTake);
	                $MenuArr["Report"]["Child"]["PageReport_StockTakeProgress"] = array($i_InventorySystem_Report_StocktakeProgress, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/stocktake_progress.php", $PageReport_StockTakeProgress);
	                $MenuArr["Report"]["Child"]["PageReport_InventoryStatus"] = array($i_InventorySystem_Report_ItemStatus, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/item_status.php", $PageReport_InventoryStatus);
	                $MenuArr["Report"]["Child"]["PageReport_WrittenOffItems"] = array($i_InventorySystem_Report_ItemWrittenOff, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/item_written_off.php", $PageReport_WrittenOffItems);
                	$MenuArr["Report"]["Child"]["PageReport_WarrantyExpiry"] = array($i_InventorySystem_Report_WarrantyExpiry, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/item_warranty_expiry.php", $PageReport_WarrantyExpiry);
	                if($this->getAccessLevel() == 1)
	                {
	                	$MenuArr["Report"]["Child"]["PageReport_FixedAssetsRegister"] = array($i_InventorySystem_Report_FixedAssetsRegister, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/fixed_assets_register.php", $PageReport_FixedAssetsRegister);
                	}
                	if($this->getAccessLevel() == 1)
					{
						$MenuArr["Report"]["Child"]["DeleteLog"] = array($Lang['eInventory']['DeleteLog'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/del_log/", $PageReports_DeleteLog);
					}
                	if($this->getAccessLevel() == 1/* && ($sys_custom['CatholicEducation_eInventory']  || $sys_custom['StocktakeListReport'])*/)
                	{
	                	$MenuArr["Report"]["Child"]["PageReport_StocktakeList"] = array($Lang['eInventory']['Report']['StocktakeList'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/cust/stocktake_list/", $PageReports_StocktakeListReport);
                	}
                	if($sys_custom['eInventory']['plkchc_report'] && $this->getAccessLevel() == 1){
                	    $MenuArr["Report_Cust"] = array($Lang['eDiscipline']['CustomizedReports'], "", $PageCustomizedReports);
                	    $MenuArr["Report_Cust"]["Child"]["PageManagement_WriteOffApproval_PLKCHC"] = array($i_InventorySystem_WriteOffItemApproval, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/write_off_approval/write_off_item_cust.php", $PageManagement_WriteOffApproval_PLKCHC);
                	    $MenuArr["Report_Cust"]["Child"]["PageReport_FixedAssetsRegister_PLKCHC"] = array($i_InventorySystem_Report_FixedAssetsRegister, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/fixed_assets_register_cust.php", $PageReport_FixedAssetsRegister_PLKCHC);
                	    $MenuArr["Report_Cust"]["Child"]["PageReport_StocktakeList_PLKCHC"] = array($Lang['eInventory']['Report']['StocktakeList'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/report/cust/stocktake_list/index_cust.php", $PageReports_StocktakeListReport_PLKCHC);
                	}
					
	                if($this->getAccessLevel() == 1)
	                {
	                	$MenuArr["Settings"] = array($i_InventorySystem['Settings'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/category/category_setting.php", $PageSettings);
	                	$MenuArr["Settings"]["Child"]["PageSettings_Category"] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/category/category_setting.php", $PageSettings_Category);
	                	//$MenuArr["Settings"]["Child"]["PageSettings_Location"] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/location/location_level_setting.php", $PageSettings_Location);
	                	$MenuArr["Settings"]["Child"]["PageSettings_Group"] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/group/group_setting.php?clearCoo=1", $PageSettings_Group);
	                	$MenuArr["Settings"]["Child"]["PageSettings_FundingSource"] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/funding/fundingsource_setting.php", $PageSettings_FundingSource);
	                	$MenuArr["Settings"]["Child"]["PageSettings_WriteOffReason"] = array($i_InventorySystem['WriteOffReason'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/write_off_reason/write_off_reason_setting.php", $PageSettings_WriteOffReason);
	                	$MenuArr["Settings"]["Child"]["PageSettings_Others"] = array($Lang['eInventory']['SystemProperties'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/settings/others/others_setting.php", $PageSettings_Others);
	            	}

					if($sys_custom['PowerClass']){
						if(isset($_SESSION['PowerClass_PAGE'])){
							if($_SESSION['PowerClass_PAGE']=="reports"){
						  		$MenuArr = array("Report"=>$MenuArr["Report"]);
							}
							elseif($_SESSION['PowerClass_PAGE']=="app"){
								$MenuArr = array("Management"=>$MenuArr["Management"]);
							}
						}
					}
		  			                
	                # module information
	                $MODULE_OBJ['title'] = $i_InventorySystem['eInventory'];
	                $MODULE_OBJ['title_css'] = "menu_opened";
	                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_einventory.gif";
	                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/index.php";
	                $MODULE_OBJ['menu'] = $MenuArr;
	                $MODULE_OBJ['TitleBar_Style'] = 2;
	
	                return $MODULE_OBJ;
	        }
	        function returnFundingSourceWithFundingType($RecordStatus="1")
	        {
	        	$namefield = $this->getInventoryItemNameByLang();
	        	$sql = "SELECT FundingSourceID, $namefield, FundingType  FROM INVENTORY_FUNDING_SOURCE
	        	where RecordStatus in ($RecordStatus)
	        	order by FundingType";
	        	return $this->returnArray($sql);
	        }
	        
	        function returnFundingSource($RecordStatus="1")
	        {
				$namefield = $this->getInventoryItemNameByLang();
				$sql = "SELECT FundingSourceID, $namefield AS FundingSourceName FROM INVENTORY_FUNDING_SOURCE 
						where RecordStatus in ($RecordStatus) 
						order by DisplayOrder";
				return $this->returnArray($sql);   
	        }
	        
	        function returnAdminGroup($adminGroupIDFilter=array())
	        {
	            $condition = "";
	            if (count($adminGroupIDFilter)) {
	                $condition .= "WHERE AdminGroupID IN ('". implode("','",(array)$adminGroupIDFilter)."') ";
                }
		     	$sql = "SELECT AdminGroupID, ".$this->getInventoryNameByLang()." as AdminGroupName FROM INVENTORY_ADMIN_GROUP ".$condition."ORDER BY DisplayOrder";
				return $this->returnArray($sql);   
	        }
	        
	        function returnExistingItems($targetItemType, $targetCategory, $targetCategory2)
	        {
				$user_id = $this->uid;
	            if($this->IS_ADMIN_USER($user_id))
	            {
		        	$sql = "SELECT ItemID, ".$this->getInventoryItemNameByLang()." as item_name FROM INVENTORY_ITEM WHERE ItemType = '$targetItemType' AND CategoryID = '$targetCategory' AND Category2ID = '$targetCategory2' order by item_name";
	        	}
	        	else
	        	{
		        	$admin_group = $this->getInventoryAdminGroup(1);
		        	
		        	$sql = "SELECT 
				        		distinct(a.ItemID), ".$this->getInventoryItemNameByLang("a.")." as item_name 
				        	FROM 
				        		INVENTORY_ITEM as a
				        		left join INVENTORY_ITEM_SINGLE_EXT as b on b.ItemID=a.ItemID
				        		left join INVENTORY_ITEM_BULK_LOCATION as c on c.ItemID=a.ItemID
				        	WHERE 
					        	a.ItemType = '$targetItemType' AND 
					        	a.CategoryID = '$targetCategory' AND 
					        	a.Category2ID = '$targetCategory2' AND
					        	(b.GroupInCharge in ($admin_group) or c.GroupInCharge in ($admin_group))
				        	order by 
				        		item_name
				        	";
	        	}
				return $this->returnArray($sql);   
	        }
	        
	        function returnItemInfo($ItemID, $LocationID='',$CategoryID='', $Category2ID='',$GroupID='',$FundingID='', $bi_lang=0)
	        {
		        if($LocationID != "")
				{
					$location_condition1 = " AND e.LocationID IN ($LocationID) ";
					$location_condition2 = " AND g.LocationID IN ($LocationID) ";
					
					$location_con = " and (e.LocationID IN ($LocationID) or g.LocationID IN ($LocationID)) "; 
				}
				
				if($CategoryID != "")
				{
					$category_con = " and a.CategoryID='$CategoryID' ";
				}
				
				if($Category2ID != "")
				{
					$category2_con = " and a.Category2ID='$Category2ID' ";
				}
				
				if($GroupID != "")
				{
					$group_con = " and i.AdminGroupID='$GroupID' ";
				}

				if($FundingID != "")
				{
					$funding_con = " and funding.FundingSourceID='$FundingID' ";
				}
				//			".$this->getInventoryNameByLang("funding.")." as FundingSource
				
				if($bi_lang)
				{
					$bi_fields = ",CONCAT(b.NameEng,' > ',c.NameEng) as full_category_name_EN,
									CONCAT(b.NameChi,' > ',c.NameChi) as full_category_name_B5,
									CONCAT(LocBuilding.NameEng, ' > ' , j.NameEng,' > ',h.NameEng) as full_location_EN,
									CONCAT(LocBuilding.NameChi, ' > ' , j.NameChi,' > ',h.NameChi) as full_location_B5,
									i.NameEng as Resource_Group_EN,
									i.NameChi as Resource_Group_B5,
									if(k2.NameEng>'', concat(funding.NameEng,', ',k2.NameEng), funding.NameEng) as FundingSource_EN,
									if(k2.NameChi>'', concat(funding.NameChi,', ',k2.NameChi), funding.NameChi) as FundingSource_B5
					";
				}
		        $sql = "SELECT 
								a.ItemID,
								a.ItemType,
								a.CategoryID,
								".$this->getInventoryNameByLang("b.")." as category_name,
								a.Category2ID,
								a.NameChi,
								a.NameEng,
								a.DescriptionChi,
								a.DescriptionEng,
								a.ItemCode,
								a.Ownership,
								a.PhotoLink,
								CONCAT(".$this->getInventoryNameByLang("b.").",' > ',".$this->getInventoryNameByLang("c.").") as full_category_name,
								CONCAT(".$this->getInventoryNameByLang("LocBuilding.").", ' > ' , ".$this->getInventoryNameByLang("j.").",' > ',".$this->getInventoryNameByLang("h.").") as full_location,
								".$this->getInventoryNameByLang("i.")." as Resource_Group,
								if(".$this->getInventoryNameByLang("k2.").">'', concat(".$this->getInventoryNameByLang("funding.").",', ',".$this->getInventoryNameByLang("k2.")."), ".$this->getInventoryNameByLang("funding.").") as FundingSource
								$bi_fields
						FROM
								INVENTORY_ITEM AS a 
								INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID)
								INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID)
								
								LEFT OUTER JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID $location_condition1)  
								LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOCATION AS g ON (a.ItemID = g.ItemID $location_condition2) 
								
								LEFT OUTER JOIN INVENTORY_LOCATION AS h ON (e.LocationID = h.LocationID OR g.LocationID = h.LocationID) 
								LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS j ON (h.LocationLevelID = j.LocationLevelID) 
								LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS LocBuilding ON (j.BuildingID = LocBuilding.BuildingID)
								
								LEFT JOIN INVENTORY_ADMIN_GROUP AS i ON (e.GroupInCharge = i.AdminGroupID OR g.GroupInCharge = i.AdminGroupID) 
								LEFT JOIN INVENTORY_FUNDING_SOURCE AS funding ON (e.FundingSource = funding.FundingSourceID OR g.FundingSourceID = funding.FundingSourceID)
								LEFT JOIN INVENTORY_FUNDING_SOURCE AS k2 ON (e.FundingSource2 = k2.FundingSourceID)
						WHERE
								a.ItemID = '$ItemID'
								$location_con 
								$category_con
								$category2_con
								$group_con
								$funding_con
								";
				return $this->returnArray($sql);   
	        }
	        
	        function returnItemType($ItemID)
	        {
		        $sql = "select ItemType from INVENTORY_ITEM where ItemID = '$ItemID'";
		        $result = $this->returnVector($sql);
		        return $result[0];
	        }
	        
	        function returnBulkItemLocationQty($ItemID, $LocationID, $GroupID, $FundingID)
	        {
		        $sql = "select Quantity from INVENTORY_ITEM_BULK_LOCATION where ItemID = '$ItemID' and LocationID='$LocationID' and GroupInCharge='$GroupID' and FundingSourceID='$FundingID'";
		        $result = $this->returnVector($sql);
		        return $result[0];
	        }
	        
	        function returnCategoryName($CategoryID)
	        {
		        $cate_name = $this->getInventoryNameByLang("");
		        $sql = "select $cate_name from INVENTORY_CATEGORY where CategoryID = '$CategoryID'";
		        $result = $this->returnVector($sql);
		        return $result[0];
	        }
	        
	        function returnSubCategoryName($CategoryID2)
	        {
		        $cate_name = $this->getInventoryNameByLang("");
		        $sql = "select $cate_name from INVENTORY_CATEGORY_LEVEL2 where Category2ID = '$CategoryID2'";
		        $result = $this->returnVector($sql);
		        return $result[0];
	        }
	        
	        function returnItemNameByItemCode($ItemCode)
	        {
		        $sql = "select NameChi, NameEng from INVENTORY_ITEM where ItemCode='$ItemCode'";
		        return $this->returnArray($sql);
	        }
	        
	        function retriveGroupLeaderNotAllowWriteOffItem()
	        {
		        return $this->groupLeaderNowAllowWriteOffItem;
	        }
	        
	        function returnLocationStr($locationID)
	        {
		        $sql = "SELECT 
						CONCAT(".$this->getInventoryNameByLang("c.").",' > ',".$this->getInventoryNameByLang("b.").",' > ',".$this->getInventoryNameByLang("a.").")
					FROM
							INVENTORY_LOCATION AS a 
							INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING AS c ON (b.BuildingID = c.BuildingID) 
					WHERE 
							a.LocationID = '$locationID'";
				$result = $this->returnVector($sql);
				return $result[0];
				
	        }
	        
	        function DeleteLog($ItemID='', $LocationID='', $GroupID='', $FundingID='')
	        {
		        # Add deletion log
				include_once("liblog.php");
				$lg = new liblog();
				$RecordDetailAry = array();
				
				$infoAry = $this->returnItemInfo($ItemID, $LocationID, '','',$GroupID, $FundingID);
				$info = $infoAry[0];
							
				$RecordDetailAry['Item_Code'] = $info['ItemCode'];
				
				if($info['ItemType'] == 1)	# single
				{
					$sql = "select TagCode from INVENTORY_ITEM_SINGLE_EXT where ItemID='$ItemID'";
					$result = $this->returnVector($sql);
					$RecordDetailAry['Barcode'] = $result[0];
				}
				
				$RecordDetailAry['Item_Name'] = $info['NameEng'] . "(" . $info['NameEng'] . ")";
				$RecordDetailAry['Category'] = $info['full_category_name'];
				$RecordDetailAry['Location'] = $info['full_location'];
				$RecordDetailAry['Resource_Management_Group'] = $info['Resource_Group'];
				$RecordDetailAry['Funding'] = $info['FundingSource'];
			
				$this_type = $info['ItemType']==1 ? ITEM_TYPE_SINGLE : ITEM_TYPE_BULK;
				$related_table = "INVENTORY_ITEM";
				$lg->INSERT_LOG($this->ModuleName, $this_type, $RecordDetailAry, $related_table, $ItemID);
	        }
	        
	        function returnOwnGroup()
	        {
		        global $UserID;
		        
		        $group_namefield = $this->getInventoryNameByLang();
				if($this->IS_ADMIN_USER($UserID))
				{
					$sql = "SELECT AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
				}
				else
				{
					$sql = "
						SELECT 
							a.AdminGroupID, 
							$group_namefield as GroupName
						FROM 
							INVENTORY_ADMIN_GROUP as a 
							inner join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
						WHERE 
							b.UserID =  '$UserID'
						 ORDER BY 
						 	a.DisplayOrder
						";
				} 
				return $this->returnArray($sql,2);
	        }
	        
	         function returnFundingSourceByFundingID($funding_id)
	        {
		        $namefield = $this->getInventoryItemNameByLang();
				
		        if(strpos($FundingID,":")>0)
		        {
			     	   
		        }
// 		        if(strpos($FundingID,":")>0)
// 				{
// 					//if(".$linventory->getInventoryNameByLang("k2.").">'', concat(".$linventory->getInventoryNameByLang("k.").",', ',".$linventory->getInventoryNameByLang("k2.")."), ".$linventory->getInventoryNameByLang("k.")."),	
// 					$sql = "select 
// 								$namefield
// 							from 
// 								INVENTORY_FUNDING_SOURCE 
// 				}
// 				else
// 				{	
// 					
					$sql = "SELECT $namefield FROM INVENTORY_FUNDING_SOURCE where FundingSourceID='$funding_id'";
					$result = $this->returnVector($sql);   
// 				}
				return $result[0];
	        }
	        
	        function returnGroupNameByGroupID($group_id)
	        {
				$namefield = $this->getInventoryItemNameByLang();
				$sql = "SELECT $namefield FROM INVENTORY_ADMIN_GROUP where AdminGroupID='$group_id'";
				$result = $this->returnVector($sql); 
				return $result[0];
	        }
	        
	        
	        # PurchasedPrice, UnitPrice, StockTakeOption
	        function FilterForStockTakeSingle($arr_result)
	        {
	        	$StockTakePriceSingle = $this->getStockTakePriceSingle();
	        	$arr_sort = array();
				for ($i=0; $i<sizeof($arr_result); $i++)
				{
					#debug($arr_result[$i]["item_name"],$arr_result[$i]["ItemCode"], $arr_result[$i]["PurchasedPrice"], $arr_result[$i]["UnitPrice"], $arr_result[$i]["StockTakeOption"]);
					$StockTakeOption = strtoupper($arr_result[$i]["StockTakeOption"]);
					
					if ($StockTakeOption=="YES")
					{
						$arr_sort[] = $arr_result[$i];
					} elseif ($StockTakeOption=="NO")
					{
						// skip
					} elseif ( $StockTakePriceSingle=="" || $StockTakePriceSingle==0)
					{
						$arr_sort[] = $arr_result[$i];
					} else
					{
						# check setting
						if ($arr_result[$i]["UnitPrice"]!="" && $arr_result[$i]["UnitPrice"]>0)
						{
							$item_price = $arr_result[$i]["UnitPrice"];
						} else
						{
							$item_price = $arr_result[$i]["PurchasedPrice"];
						}
						
						if ($item_price>=$StockTakePriceSingle)
						{
							$arr_sort[] = $arr_result[$i];
						}
					}
				}
	        	
	        	return $arr_sort;
	        }
	        
	        function FilterForStockTakeBulk($arr_result)
	        {
	        	$StockTakePriceBulk = $this->getStockTakePriceBulk();
	        	
	        	
	        	$arr_sort = array();
				for ($i=0; $i<sizeof($arr_result); $i++)
				{
					//debug($arr_result[$i]["item_name"], $arr_result[$i]["ItemCode"], $arr_result[$i]["PurchasedPrice"], $arr_result[$i]["UnitPrice"], $arr_result[$i]["StockTakeOption"]);
					$StockTakeOption = strtoupper($arr_result[$i]["StockTakeOption"]);
//debug_pr("StockTakeOption=$StockTakeOption");					
					if ($StockTakeOption=="YES")
					{
						$arr_sort[] = $arr_result[$i];
					} elseif ($StockTakeOption=="NO")
					{
						// skip
					} else
					{
						# check setting
						$written_off_qty = 0;
						$total_purchase_price = 0;
						$totalQty = 0;
						$final_unit_price = 0;
						$item_price = 0;
						
						$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $arr_result[$i]["ItemID"]."'";
						$tmp_result = $this->returnVector($sql);
						$written_off_qty += $tmp_result[0];
// debug_pr($sql);
// debug_pr($written_off_qty);
						
						$sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $arr_result[$i]["ItemID"]."' AND Action = 1";
						$tmp_result = $this->returnVector($sql);
						$total_purchase_price += $tmp_result[0];
// debug_pr($sql);
// debug_pr("totalpurchase price=".$total_purchase_price);
						$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_EXT WHERE ItemID = '" . $arr_result[$i]["ItemID"]."'";
						$tmp_result = $this->returnVector($sql);
						$totalQty += $tmp_result[0];
// debug_pr($sql);					
// debug_pr("totalQty=".$totalQty);
					
						$ini_total_qty = $totalQty + $written_off_qty;
// debug_pr("ini_total_qty=$ini_total_qty");
						if ($ini_total_qty>0)
							$item_price = round($totalQty * ($total_purchase_price/$ini_total_qty),2);
						
// if ($arr_result[$i]["ItemID"] == 1368) {
//     debug_pr("item_price=$item_price");
//     debug_pr("StockTakePriceBulk=$StockTakePriceBulk");
// }
						#debug($arr_result[$i]["item_name"], $arr_result[$i]["ItemCode"], $StockTakePriceBulk, $item_price, $totalQty);
						if ($item_price>=$StockTakePriceBulk)
						{
							$arr_sort[] = $arr_result[$i];
						}
					}
				}
	        	
	        	return $arr_sort;
	        }
	        
	        function IsItemForStockTake($location_id, $admin_list)
	        {
	        	
	        	# check if there is any items for stock-take
				$sql = "SELECT 
							a.PurchasedPrice, a.UnitPrice,
							c.StockTakeOption
					FROM 
							INVENTORY_ITEM_SINGLE_EXT AS a LEFT JOIN 
							INVENTORY_ITEM AS c ON (c.ItemID = a.ItemID) 
					WHERE 
							a.LocationID = '$location_id' AND 
							a.GroupInCharge IN ($admin_list)";
				$arr_single_result = $this->returnArray($sql);
				$arr_single_result = $this->FilterForStockTakeSingle($arr_single_result);
				if (sizeof($arr_single_result)<=0)
				{
					$sql = "SELECT
									a.ItemID,
									a.StockTakeOption
							FROM
									INVENTORY_ITEM AS a 
									INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0)
							WHERE
									a.ItemType = 2 AND
									a.RecordStatus = 1 AND
									b.LocationID = '$location_id' AND
									b.GroupInCharge IN ($admin_list)";
					$arr_bulk_result = $this->returnArray($sql);
					$arr_bulk_result = $this->FilterForStockTakeBulk($arr_bulk_result);
					if (sizeof($arr_bulk_result)<=0)
					{
						return false;
					}
				}
				
				return true;
	        	
	        }
	        
	         function returnAdminGroupCode($GroupID)
	        {
		     	$sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP where AdminGroupID='$GroupID'";
				$result = $this->returnVector($sql);   
				return $result[0];
	        }
	        
	        function returnCategoryCode($CategoryID)
	        {
		        $sql = "select Code from INVENTORY_CATEGORY where CategoryID = '$CategoryID'";
		        $result = $this->returnVector($sql);
		        return $result[0];
	        }
	        
	        function returnSubCategoryCode($CategoryID2)
	        {
		        $sql = "select Code from INVENTORY_CATEGORY_LEVEL2 where Category2ID = '$CategoryID2'";
		        $result = $this->returnVector($sql);
		        return $result[0];
	        }
	        
	        function enableMemberUpdateLocationRight() {
	        	global $sys_custom;
	        	return $sys_custom['eInventory']['MemberUpdateLocationRight'];
	        }
	                
	        function IS_GROUP_HELPER($AdminGroupID='')
	        {
		        $user_id = $this->uid;
		        $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '$user_id' AND RecordType = 3";
		        if(!empty($AdminGroupID))
		        	$sql .= " and AdminGroupID='$AdminGroupID' ";
		        $inventory_admin_group = $this->returnArray($sql,1);
		        
		        if(sizeof($inventory_admin_group)>0)
		        {
			        return true;
		        }
		        else
		        {
			        return false;
		        }
	        }
	      
	      function GET_ITEM_GROUP_IN_CHARGE($itemID, $itemLocationId){
	      	   	
		      $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_SINGLE_EXT 
						WHERE ItemID = '$itemID' AND LocationID = '$itemLocationId';
					";
	      	  
	      	  $ResultArr = $this->returnVector($sql);
	      	  $result = $ResultArr[0];
	      	  
	      	return $result;
	      }
	      

		function returnBulkItemNormalQty($this_item_id, $this_location_id)
		{
			$TotalNormalQty = 0;
			
			# loop INVENTORY_ITEM_BULK_LOCATION to find out all the same location's bulk item (different GroupInCharge and FundingSourceID)
			$sql = "select GroupInCharge, FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where ItemID='$this_item_id' and LocationID='$this_location_id'";
			$result = $this->returnArray($sql);
			
			if(!empty($result))
			{
				foreach($result as $k=>$d)
				{
					list($bulk_group_id, $bulk_funding_id) = $d;
					$sql = "SELECT 
								BL.QtyNormal
							FROM
									INVENTORY_ITEM_BULK_LOG AS BL 
							WHERE
									BL.ItemID = '$this_item_id' AND
									BL.LocationID = '$this_location_id' and 
									BL.GroupInCharge='$bulk_group_id' and BL.FundingSource='$bulk_funding_id' and
									BL.ACTION IN (".ITEM_ACTION_PURCHASE.",".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING.")
							ORDER BY
										DateModified DESC, RecordID DESC
							LIMIT 0,1			
							";
					$result2 = $this->returnVector($sql);
					$TotalNormalQty += $result2[0];
				}	
			}
			return $TotalNormalQty;
		}
		
		# 1-Normal, 2-Damaged, 3-Repairing, 4-Deleted, 5-Write-off
		function returnSingleItemStatus($this_item_id_array = array())
		{
			$data = array();
			if(is_array($this_item_id_array) && !empty($this_item_id_array))
			{
				foreach($this_item_id_array as $k=>$this_item_id)
				{
					$sql = "select RecordStatus from INVENTORY_ITEM where ItemID='$this_item_id'";
					$result = $this->returnVector($sql);
					
					# deleted?
					if(empty($result))
					{
						$data[$this_item_id] = 4;
					}
					else
					{
						# write-off?
						if(!$result[0])
						{
							$data[$this_item_id] = 5;
						}
						else
						{
							# normat / damaged / repairing
							$sql = "SELECT 
										NewStatus 
								FROM 
										INVENTORY_ITEM_SINGLE_STATUS_LOG
								WHERE
										ItemID = '$this_item_id' AND
										ACTION IN (".ITEM_ACTION_PURCHASE.",".ITEM_ACTION_UPDATESTATUS.")
								ORDER BY
										DateModified DESC 
								LIMIT 0,1
							";
							$ResultArr = $this->returnVector($sql);		# 1-Normal, 2-Damaged, 3-Repairing
							$data[$this_item_id] = $ResultArr[0];	
						}
					}
				}	
			}
	      	return $data;		
		}
		
		function sendEmailNotification_eBooking($item_type,$item_id, $status_remark, $item_status,$item_location_id='', $item_group_id='', $item_funding_id='', $bulk_location_qty=0)
		{
			global $plugin, $Lang, $intranet_session_language, $intranet_root;
			
			# retrieve item data
			$data_ary = $this->returnItemInfo($item_id, $item_location_id,'','',$item_group_id,$item_funding_id,1);
			$data = $data_ary[0];
					
			# eBooking integration - send email notification
			if($plugin['eBooking'])
			{
				include_once("libebooking.php");
				$lebooking = new libebooking();
				if($lebooking->returnIsItemAllowBooking($item_type, $item_id, $item_location_id))
				{
					$lebooking->Load_General_Setting();
					
					if($lebooking->SettingArr['ReceiveEmailNotificationFromeInventory'])
					{
						# retrieve eBooking Admin
						include_once("librole.php");
						$lrole = new librole();
						$receiver = $lrole->returnRoleAdminUserID("eBooking");

						$subject = $Lang['eInventory']['UpdateItemStatusEmailToeBookingAdmin']['Subject'];
						$subject = str_replace("[ITEM_NAME_EN]", $data['NameEng'], $subject);
						$subject = str_replace("[ITEM_NAME_B5]", $data['NameChi'], $subject);
						
						$status_remark_inEmail = $status_remark ? "\n".$status_remark : "--";
						
						$email_content = "";
						for($e=1;$e<=2;$e++)
						{
							$l = $e==1? "EN":"B5";
							$content = $Lang['eInventory']['UpdateItemStatusEmailToeBookingAdmin']['Content'][$l];
							$content = str_replace("[ITEM_NAME_EN]", $data['NameEng'], $content);
							$content = str_replace("[ITEM_NAME_B5]", $data['NameChi'], $content);
							
							$content = str_replace("[ITEM_LOCATION_". $l."]", $data['full_location_'.$l], $content);
							$content = str_replace("[ITEM_GROUP_". $l."]", $data['Resource_Group_'.$l], $content);
							$content = str_replace("[ITEM_FUNDING_". $l."]", $data['FundingSource_'.$l], $content);
							
							# single 
							if($item_type == 1)
							{
								# 1-Normal, 2-Damaged, 3-Repairing, 4-Deleted, 5-Write-off
								switch($item_status)
								{
									case "1":	$this_status = $Lang['eInventory']['ItemStatus']['Normal'][$l];	break;
									case "2":	$this_status = $Lang['eInventory']['ItemStatus']['Damaged'][$l];	break;
									case "3":	$this_status = $Lang['eInventory']['ItemStatus']['Repairing'][$l];	break;
									case "4":	$this_status = $Lang['eInventory']['ItemStatus']['Deleted'][$l];	break;
									case "5":	$this_status = $Lang['eInventory']['ItemStatus']['WriteOff'][$l];	break;
								}
								$content = str_replace("[ITEM_STATUS_". $l ."]", $this_status, $content);
							}
							else
							{
								# $item_status = normal-damaged-repairing
								if($item_status<=3)	
								{
									list($this_normal, $this_demaged, $this_repairing) = explode("-",$item_status);
									$this_status = $Lang['eInventory']['ItemStatus']['Normal'][$l] . "-".$this_normal .", ";
									$this_status .= $Lang['eInventory']['ItemStatus']['Damaged'][$l] . "-".$this_demaged .", ";
									$this_status .= $Lang['eInventory']['ItemStatus']['Repairing'][$l] . "-".$this_repairing;
									$content = str_replace("[ITEM_STATUS_". $l ."]", $this_status, $content);
								}
								else
								{
									if($item_status==4)
										$this_status = $Lang['eInventory']['ItemStatus']['Deleted'][$l] . "-".$bulk_location_qty;
									elseif($item_status==5)
										$this_status = $Lang['eInventory']['ItemStatus']['WriteOff'][$l] . "-".$bulk_location_qty;
									$content = str_replace("[ITEM_STATUS_". $l ."]", $this_status, $content);
								}
							}
							
							$content = str_replace("[ITEM_REMARK]", $status_remark_inEmail, $content);
							$email_content .= $content."\n\n\n\n";
						}
						
					    include_once("libwebmail.php");
						$lwebmail = new libwebmail();
						$lwebmail->sendModuleMail($receiver, $subject, $email_content,0);
					}
				}
			}
			
		}
		
		
		function enablePhotoDisplayInBarcodeRight() {
	       global $sys_custom;
	       return $sys_custom['CatholicEducation_eInventory'];
	    }
	    
	    function parseStockTakeOption($StockTakeOption)
		{
			$StockTakeOption = strtoupper($StockTakeOption);
			if (strtoupper($StockTakeOption)=="Y" || strtoupper($StockTakeOption)=="YES")
			{
				return "YES";
			} elseif (strtoupper($StockTakeOption)=="N" || strtoupper($StockTakeOption)=="NO")
			{
				return "No";
			} else
			{
				return "FOLLOW_SETTING";
			}
		}
		
		
		
		function saveEntryTag($RecordID, $tagNameText) {
			$successAry = array();
			$tagNameText = trim($tagNameText);
			
			// will auto create tag records if the tag does not exist
			$targetTagIdAry = returnModuleTagIDByTagName($tagNameText, $this->ModuleName);
			$numOfTargetTag = count($targetTagIdAry);
			
			### Delete records of removed tags
			$originalTagIdAry = Get_Array_By_Key($this->getEntryTag($RecordID), 'TagID');
			$removedTagIdAry = array_values(array_diff((array)$originalTagIdAry, (array)$targetTagIdAry));
			if (count($removedTagIdAry) > 0) {
				$successAry['deleteRecordTag'] = $this->deleteEntryTagDbRecord($RecordID, $removedTagIdAry);
			}
			
			### Insert new tags
			$newTagIdAry = array();
			for ($i=0; $i<$numOfTargetTag; $i++) {
				$_tagId = $targetTagIdAry[$i];
				
				$_tagDataAry = $this->getEntryTag($RecordID, $_tagId);
				if (count($_tagDataAry) == 0) {
					$newTagIdAry[] = $_tagId;
				}
			}
			if (count($newTagIdAry) > 0) {
				$successAry['insertRecordTag'] = $this->insertEntryTagDbRecord($RecordID, $newTagIdAry);
			}
			
			return !in_multi_array(false, $successAry);
		}
		
		function getEntryTag($RecordIDAry='', $tagIdAry='') {
			if ($RecordIDAry !== '') {
				$condsRecordId = " And RecordID In ('".implode("','", (array)$RecordIDAry)."') ";
			}
			if ($tagIdAry !== '') {
				$condsTagId = " And TagID In ('".implode("','", (array)$tagIdAry)."') ";
			}
			
			$sql = "Select RecordID, TagID from INVENTORY_TAG Where 1 $condsRecordId $condsTagId";
			return $this->returnResultSet($sql); 
		}
		
		function getEntryTagName($RecordID)
		{
			$sql = "SELECT TagID FROM INVENTORY_TAG WHERE RecordID='$RecordID' ";
			$tagIDs =  $this->returnVector($sql);
			
			$tagNameAry = array();
			if(count($tagIDs)>0){
				$tagNameAry = returnModuleTagNameByTagID(implode(",",$tagIDs), $this->ModuleName);
			}
			return $tagNameAry;
		}
		
		
		
		function displayTags($RecordID, $InLink=false)
		{
			$tagNameAry = $this->getEntryTagName($RecordID);
			for ($i=0; $i<sizeof($tagNameAry); $i++)
			{
				$tagNow = ($InLink) ? "<a href=\"items_full_list.php?Tag=".intranet_htmlspecialchars($tagNameAry[$i])."\">".$tagNameAry[$i]."</a>" : $tagNameAry[$i];
				$ReturnText .= (($ReturnText!="") ? ", " : "" ).$tagNow;
			}
			return $ReturnText;
		}
		
		function deleteEntryTagDbRecord($RecordID, $tagIdAry) {
			
			if (count((array)$tagIdAry) == 0) {
				return false;
			}
			else {
				$successAry = array();
				$sql = "Delete From INVENTORY_TAG Where RecordID = '".$RecordID."' And TagID In ('".implode("','", (array)$tagIdAry)."')";
				$successAry['deleteRecordTag'] = $this->db_db_query($sql);
				
				$sql = "Select TagID, Count(*) as NumOfRecord From INVENTORY_TAG Where TagID In ('".implode("','", (array)$tagIdAry)."') Group By TagID";
				$tagCountAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'TagID', array('NumOfRecord'), $SingleValue=1);
				
				$numOfTag = count((array)$tagIdAry);
				for ($i=0; $i<$numOfTag; $i++) {
					$_tagId = $tagIdAry[$i];
					$_numOfRecord = $tagCountAssoAry[$_tagId];
					
					if ($_numOfRecord == 0 || $_numOfRecord == '') {
						removeTagModuleRelation($_tagId, $this->ModuleName);
					}
				}
				
				return !in_array(false, $successAry);
			}
		}
		
		function insertEntryTagDbRecord($RecordID, $tagIdAry) {
			global $indexVar;
			
			$numOfTag = count((array)$tagIdAry);
			for ($i=0; $i<$numOfTag; $i++) {
				$_tagId = $tagIdAry[$i];
				
				$insertValueAry[] = " ('".$RecordID."', '".$_tagId."', now(), '".$indexVar['drUserId']."') ";
			}
			
			$success = false;
			if (count($insertValueAry) > 0) {
				$sql = "Insert Into INVENTORY_TAG (RecordID, TagID, DateInput, InputBy)
							Values ".implode(',', (array)$insertValueAry);
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function getRecordIDsFromTagName($Tag)
		{
			$tagID_arr = returnModuleTagIDByTagName($Tag, $this->ModuleName);
			if (is_array($tagID_arr) && sizeof($tagID_arr)>0)
			{
				$tagIDs = trim(implode(",", $tagID_arr));
				if ($tagIDs!="")
				{
					$sql = "SELECT RecordID FROM INVENTORY_TAG WHERE TagID IN ($tagIDs)";
					$rows = $this->returnVector($sql);
					return $rows;
				}
			}
			
			return;
		}
		
		function returnGroupNameByCode($group_code)
        {
			$namefield = $this->getInventoryItemNameByLang();
			$sql = "SELECT $namefield FROM INVENTORY_ADMIN_GROUP where Code='$group_code'";
			$result = $this->returnVector($sql); 
			return $result[0];
        }
        
        function returnCategoryNameByCode($category_code)
        {
			$namefield = $this->getInventoryItemNameByLang();
			$sql = "SELECT $namefield FROM INVENTORY_CATEGORY where Code='$category_code'";
			$result = $this->returnVector($sql); 
			return $result[0];
        }
        
        function returnSubCategoryNameByCode($subcategory_code)
        {
			$namefield = $this->getInventoryItemNameByLang();
			$sql = "SELECT $namefield FROM INVENTORY_CATEGORY_LEVEL2 where Code='$subcategory_code'";
			$result = $this->returnVector($sql); 
			return $result[0];
        }
		
		/*
		 * @param positive int $request_number: number of barcodes required
		 * @param string $for_what: "item" => "single or bulk item", "group" => "resource mgmt group", "funding" => "funding source"
		 * @return array of barcode string
		 */
		function getUniqueBarcode($request_number, $for_what="item")
		{
			$return_barcode_ary = array();
			$for_what = strtolower($for_what);
			while(sizeof($return_barcode_ary)!=$request_number)
			{
				$tmp_barcode = $this->random_barcode();
				if($for_what == "group") {
					$sql = "SELECT Barcode FROM INVENTORY_ADMIN_GROUP WHERE Barcode = '$tmp_barcode'";
				}else if($for_what == "funding") {
					$sql = "SELECT Barcode FROM INVENTORY_FUNDING_SOURCE WHERE Barcode = '$tmp_barcode'";
				}else {
					$sql = "SELECT TagCode as Barcode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '$tmp_barcode'";
					$sql.= "UNION (SELECT Barcode FROM INVENTORY_ITEM_BULK_EXT WHERE Barcode='$tmp_barcode')";
				}
				$arr_tmp_barcode = $this->returnVector($sql);
				if((sizeof($arr_tmp_barcode)==0) && (!array_search($tmp_barcode,$return_barcode_ary))) {
					array_push($return_barcode_ary,$tmp_barcode);
				}
			}
			
			return $return_barcode_ary;
		}
		
		// Remove start and end * from barcode
		function trimBarcodeStars($Barcode)
		{
			$Barcode = trim($Barcode);
        	// check if the barcode is start & end w/'*', if yes, then remove the '*'
			if((strpos($Barcode,"*") == 0) && (strrpos($Barcode,"*") == strlen($Barcode)-1))
			{
				$Barcode = substr($Barcode,1,strlen($Barcode)-2);
			}
			return $Barcode;
		}
		
		function insertBulkInvoiceHistoryLog($log)
		{
			global $UserID;
			
			$fields = array();
			foreach($log as $k=>$d)
			{
				$fields[] = $k;
				$data[] = "'". $this->Get_Safe_Sql_Query($d) ."'";
			}
			
			$sql = "insert into INVENTORY_ITEM_BULK_INV_UPDATE_LOG ";
			$sql .= "(". implode(",",$fields) .", DateInput, InputBy)";
			$sql .=" values ";
			$sql .= "(". implode(",",$data) .", now(), $UserID)";
			$this->db_db_query($sql);
		}
		
		function TrimWriteOffTag($tagAry=array())
		{
			$tagAry2 = array();
			if(!empty($tagAry))
			{
				foreach($tagAry as $tag_id=>$tag_name)
				{
					$sql = "select 
								count(*) 
							from 
								INVENTORY_TAG as t
								left join INVENTORY_ITEM as i on i.ItemID=t.RecordID
							where
								t.TagID = '$tag_id'
								and i.RecordStatus = 1
							";
					$result = $this->returnVector($sql);
					if($result[0]>0)
					{
						$tagAry2[$tag_id] = $tag_name;
					}	
				}
			}
			
			return $tagAry2;
			
		}
		
		function returnAdminGroupSelection($targetGroup=array())
		{
			$group_ary = $this->returnAdminGroup();
			
			$group_selection = "<select id=\"targetGroup[]\" name=\"targetGroup[]\" multiple size=\"10\">";
			
			if(sizeof($group_ary)>0) {
				for($i=0; $i<sizeof($group_ary); $i++) {
					list($group_id, $group_name) = $group_ary[$i];
					
					$selected = "";
					if(is_array($targetGroup)){
						if(in_array($group_id,$targetGroup)){
							$selected = " SELECTED ";
						}
					}
					$group_selection .= "<option value=\"$group_id\" $selected>$group_name</option>";
				}
			}
			$group_selection .= "</select>";
			return $group_selection;
		}
		
		function SELECTFROMTABLE($table,$fields='*',$condition=null, $limits=null, $mode = 0,$sortBy=null,  $sortOrder ='ASC')
     	{
	    	global $UserID;
		 
	    	$sql = "SELECT ";
		    if (is_array($fields))
			    $sql .= " `" . implode("`,`",$fields). "` ";
		    else
			    if (empty($fields) || $fields == '*' )
				    $sql .= " * ";
			    else
				    $sql .=" `{$fields}` ";
			
		    $sql .= " FROM `{$table}` "; 
		    if(!empty($condition)){
			    foreach($condition as $field=>$value){
				    if (is_int($field)){
					    $tmp_cond[] =$value;
				    }else{
					    $tmp_cond[] = "`{$field}` = {$value}";
			    	}
	    		}
				
		    	$sql .= " WHERE ". implode(' AND ',$tmp_cond);
		    }
		
	    	if (!empty($sortBy)){
		    	if (!is_array($sortBy)) {
			    	$sortBy = array($sortBy);
	     		}
		    	$sql .= "ORDER BY `" . implode(',', $sortBy) . "` "  . $sortOrder;
	    	}
		
	    	if(!empty($limits)){
	    		if (is_array($limits)) 
		    		$sql .= " LIMIT {$limits[0]},{$limits[1]}";
		    	else
		    		$sql .= " LIMIT {$limits}";
		    }
	
	    	$result = $this->returnArray($sql,null,$mode);
            return $result;
	    }
	    
	    function GET_LABEL_FORMAT($Code)
    	{
	        $sql = "SELECT * FROM INVENTORY_LABEL_FORMAT WHERE id=\"$Code\"";	
	        return $this->returnArray($sql);
	    }
		
		function UPDATE_LABEL_FORMAT($Code, $dataAry=array())
	    {
	        global $UserID;
	    
	        foreach($dataAry as $fields[]=>$values[]);
	    	$sql = "UPDATE `INVENTORY_LABEL_FORMAT` SET ";
	        foreach($dataAry as $field=>$value)
     		if ($value=="NULL")
	    		$sql .= "`".$field ."`". "=". $value.", ";
	    	else
	    		$sql .= "`".$field ."`". "=\"". $value."\", ";
	        $sql .= "DateModified=now(), LastModifiedBy=$UserID";
	        $sql .= " WHERE `id`=\"$Code\"";
	        $this->db_db_query($sql);
	        //dump($sql);
	        //dex(mysql_error());
    	}
		
		function ADD_LABEL_FORMAT($dataAry=array())
	    {
	        global $UserID;
	    
	        foreach($dataAry as $fields[]=>$values[]);
     		$sql = "INSERT IGNORE INTO `INVENTORY_LABEL_FORMAT` (";
	        foreach($fields as $field)	$sql .= "`".$field ."`, ";
	    	$sql .= "DateModified, LastModifiedBy) values (";
	        foreach($values as $value)	
	    	if ($value=="NULL")
	     		$sql .= $value.", ";
	    	else
	    		$sql .= "'". $value ."', ";
	      	$sql .= "now(), $UserID)";
	        $this->db_db_query($sql);
    	}
    	
    	function REMOVE_LABEL_FORMAT($Codes=array())
	    {
	    	//global $UserID;
		 
	    	if(is_array($Codes) && count($Codes)>0 ) {
	    		$CodesList = '"'.implode('","',$Codes).'"';
	
	    		$sql = "DELETE FROM `INVENTORY_LABEL_FORMAT` WHERE `id` IN ($CodesList) ";
	    		$this->db_db_query($sql);
	    		//dump($sql);
	    		//dex(mysql_error());
	    	}
    	}
    	
    	function getNumberOfOrderByFieldInLabel()
    	{
    	    $sql = "DESC INVENTORY_LABEL_FORMAT";
    	    $rs = $this->returnResultSet($sql);
    	    $nrOrderByField = 0;
    	    if (count($rs)) {
    	        foreach($rs as $_r) {
    	            if (!empty($_r['Field'])&& (strpos($_r['Field'],'info_order_') !== false)) {
    	                $nrOrderByField++;
    	            }
    	        }
    	    }
    	    return $nrOrderByField;
    	}
    	
        function getWarrantyExpiry()
        {
            $UserID = $_SESSION['UserID'];
            $warning_day_period = $this->getWarrantyExpiryWarningDayPeriod();
            
            if($warning_day_period > 0){
               
                if($this->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '$UserID'";
                        
                        $tmp_arr_group = $this->returnVector($sql); 
                        if(sizeof($tmp_arr_group)>0)
                        {
                            $target_admin_group = implode(",",$tmp_arr_group);
                        }

                        if($target_admin_group != "")
                        {
                            $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                        }
                        
                        $curr_date = date("Y-m-d");
                        $date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $warning_day_period, date("Y")));
                        
                        $sql = "SELECT
                        count(a.ItemID)
                        FROM
                        INVENTORY_ITEM AS a INNER JOIN
                        INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '') INNER JOIN
                        INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
                        INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
                        INVENTORY_LOCATION_LEVEL AS floor ON (d.LocationLevelID = floor.LocationLevelID) INNER JOIN
                        INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
                        WHERE
                        b.WarrantyExpiryDate BETWEEN '$curr_date' AND '$date_range'
                        $cond
                        ORDER BY
                        b.WarrantyExpiryDate";
                        $arr_expiry = $this->returnVector($sql);
            }
            return $arr_expiry;
        }
        
        function warrantyExpiryWarningPopup(){
           return ' <script language="javascript">
                    //newWindow("/home/eAdmin/ResourcesMgmt/eInventory/report/item_warranty_expiry_warning.php",4);
                    var eInventory_url = "/home/eAdmin/ResourcesMgmt/eInventory/report/item_warranty_expiry_warning.php";
                    var eInventory_win_name = "intranet_popup4_eInventory";
                    var eInventory_win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
                    var eInventory_newWin = window.open (eInventory_url, eInventory_win_name, eInventory_win_size);
                    if ((navigator.appName=="Netscape") || (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE")>1))
                        eInventory_newWin.focus();
                    </script>';
        }
        
        function getNumOfBulkItemInvoice($itemID){
            $item = IntegerSafe($itemID);
            
            $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
            return $this->returnVector($sql);
        }

        ###############################################################################################
        ## function start for eclass App
        function checkAppAccessRight()
        {
            global $Lang, $plugin, $indexVar;

            $leClassApp = $indexVar['leClassApp'];
            $ret = true;

            if ($_SESSION["SSV_PRIVILEGE"]["eInventory"]) {
                if ($this->IS_ADMIN_USER()) {
                    // admin by pass the checking
                }
                elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
                    if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T') && $this->hasAccessRight()) {
                        // pass
                    } else {
                        $ret = false;
                    }
                }
            }

            if (!$ret) {
                $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"] = false;
                No_Access_Right_Pop_Up();
            }
            else {
                $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"] = true;
            }
            return $ret;
        }

        function getAdminGroupIDAry()
        {
            if($this->IS_ADMIN_USER($this->uid)) {
                $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            }
            else {
                $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '" . $this->uid . "'";
            }
            $returnAry = $this->returnVector($sql);
            return $returnAry;
        }

        function getGroupInChargeLocation()
        {
            $adminGroupIDAry = $this->getAdminGroupIDAry();
            $sql = "(SELECT DISTINCT LocationID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge IN ('".implode("','",$adminGroupIDAry)."')) 
              UNION (SELECT DISTINCT LocationID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge IN ('".implode("','",$adminGroupIDAry)."'))";
            $tempLocationID = $this->returnVector($sql);

            if(sizeof($tempLocationID)>0)
            {
                $targetLocationID = implode("','",$tempLocationID);
            }
            $sql = "SELECT 
                        DISTINCT a.BuildingID 
                    FROM 
                        INVENTORY_LOCATION_BUILDING AS a 
                        INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) 
                        INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) 
                    WHERE 
                        a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1 AND c.LocationID IN ('".$targetLocationID."')";
            $arrBuildingOneD = $this->returnVector($sql);

            $sql = "SELECT 
                        DISTINCT a.LocationLevelID 
                    FROM 
                        INVENTORY_LOCATION_LEVEL AS a 
                        INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) 
                    WHERE 
                        a.RecordStatus = 1 AND b.RecordStatus = 1 AND b.LocationID IN ('".$targetLocationID."') ";
            $arrLevelOneD = $this->returnVector($sql);
            $ret = array();
            $ret['BuildingID'] = $arrBuildingOneD;
            $ret['LevelID'] = $arrLevelOneD;
            $ret['LocationID'] = $tempLocationID;
            return $ret;
        }


        function getBulkItemCeilingPriceCondition($condition, $adminGroupIDStr)
        {
            $arr_targetSuccessBulkItem = array();
            $arr_targetFailBulkItem = array();
            
            $sql = "SELECT
    						a.ItemID, a.CategoryID,
    						" . $this->getInventoryItemNameByLang("a.") . " as item_name
						FROM
    						INVENTORY_ITEM AS a INNER JOIN
    						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
    						INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
						WHERE
    						a.ItemType = 2 AND
    						a.RecordStatus = 1 AND
    						b.GroupInCharge IN ('".$adminGroupIDStr."')
                            ".$condition."
						ORDER BY
    						a.ItemCode";
            $arr_tmp_result = $this->returnArray($sql);
// debug_pr($sql);
// debug_pr($arr_tmp_result);
            if (sizeof($arr_tmp_result) > 0) {
                for ($i=0, $iMax=count($arr_tmp_result); $i < $iMax; $i++) {
                    list ($tmp_item_id, $tmp_cat_id) = $arr_tmp_result[$i];
                    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$tmp_item_id."' AND Action = " . ITEM_ACTION_PURCHASE . "";
                    $total_cost = $this->returnVector($sql);
// debug_pr($sql);
// debug_pr($total_cost);
                    
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$tmp_item_id."'";
                    $total_qty = $this->returnVector($sql);
// debug_pr($sql);
// debug_pr($total_qty);
                    
                    $unit_price = $total_cost[0] / $total_qty[0];
//debug_pr("unit_price=$unit_price");
                    $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = '".$tmp_cat_id."'";
                    $tmp_result2 = $this->returnArray($sql);
// debug_pr($sql);
// debug_pr($tmp_result2);
                    
                    $jMax = count($tmp_result2);
                    if ($jMax) {
                        for ($j = 0; $j < $jMax; $j++) {
                            list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                            if ($apply_to_bulk == 1) {
                                if ($unit_price >= $price_ceiling) {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                } else {
                                    $arr_targetFailBulkItem[] = $tmp_item_id;
                                }
                            } else {
                                $arr_targetSuccessBulkItem[] = $tmp_item_id;
                            }
                        }
                    }
                }
            }
// echo "success<br>";            
// debug_pr($arr_targetSuccessBulkItem);
// echo "fail<br>";
// debug_pr($arr_targetFailBulkItem);
            $result = array();
            if (sizeof($arr_targetSuccessBulkItem) > 0) {
                $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                $success_bulk_item_cond = " AND b.ItemID IN ($targetSuccessList) ";
                $result['needStocktake'] = true;
            }
            if (sizeof($arr_targetFailBulkItem) > 0) {
                $targetFailList = implode(",", $arr_targetFailBulkItem);
                $fail_bulk_item_cond = " AND b.ItemID NOT IN ($targetFailList) ";
                $result['needStocktake'] = false;
            }
            
            $result['success'] = $success_bulk_item_cond;
            $result['fail'] = $fail_bulk_item_cond;
            
//debug_pr($result);            
            return $result;
        }
        
        ## return stock take status by location for sepcific admin group
        function getStocktakeProgressByAdminGroup($adminGroupID='',$locationID='')
        {
            global $PATH_WRT_ROOT, $Lang, $sys_custom;
            include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
            $location = new liblocation();

            if ($adminGroupID == '') {
                $adminGroupIDAry = $this->getAdminGroupIDAry();
            }
            else {
                $adminGroupIDAry = is_array($adminGroupID) ? $adminGroupID : array($adminGroupID);
            }
            $adminGroupIDStr = implode("','",$adminGroupIDAry);

            $priceCondition = "";
            if ($sys_custom['eInventory_PriceCeiling']) {
                $priceCondition = " INNER JOIN INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID AND (b.UnitPrice>=d.PriceCeiling)) ";
            }
            
            $condition = $locationID ? " AND b.LocationID='".$locationID."' " : "";
            $stocktakePriceSingle = $this->StockTakePriceSingle > 0 ? $this->StockTakePriceSingle : 0;
            
            ## total items by Location for single item            
            $sql = "SELECT 
                            b.LocationID,
                            COUNT(*) AS NrItem 
                    FROM 
                            INVENTORY_ITEM a 
                            INNER JOIN INVENTORY_ITEM_SINGLE_EXT b ON b.ItemID=a.ItemID
                            ".$priceCondition."
                    WHERE
                            a.RecordStatus = 1
                            AND (a.StockTakeOption = 'YES' 
                                    OR (a.StockTakeOption = 'FOLLOW_SETTING' 
                                        AND IF(b.UnitPrice>0,b.UnitPrice,IFNULL(b.PurchasedPrice,0))>=".$stocktakePriceSingle."))   
                            AND b.GroupInCharge IN ('".$adminGroupIDStr."')

                            ".$condition."
                            GROUP BY b.LocationID";
            $totalSingleItemAry = $this->returnResultSet($sql);
            $totalSingleItemAssoc = BuildMultiKeyAssoc($totalSingleItemAry,'LocationID','NrItem',1);
// debug_pr($sql);
// print "totalSingleItemAssoc\n";
// debug_pr($totalSingleItemAssoc);

            ## for bulk items
            $success_bulk_item_cond = "";
            $fail_bulk_item_cond = "";
            
            if ($sys_custom['eInventory_PriceCeiling']) {
                $bulkItemCeilingPriceCondition = $this->getBulkItemCeilingPriceCondition($condition, $adminGroupIDStr);
                $success_bulk_item_cond = $bulkItemCeilingPriceCondition['success'];
                $fail_bulk_item_cond = $bulkItemCeilingPriceCondition['fail'];
            }
//debug_pr($fail_bulk_item_cond);            
            $stocktakePriceBulk = $this->StockTakePriceBulk > 0 ? $this->StockTakePriceBulk : 0;
            
            ## total items by Location for bulk items
            $sql = "SELECT 
                            b.LocationID,
                            COUNT(DISTINCT CONCAT(a.ItemID,'-',b.FundingSourceID)) AS NrItem 
                    FROM 
                            INVENTORY_ITEM a 
                            INNER JOIN INVENTORY_ITEM_BULK_LOCATION b ON b.ItemID=a.ItemID
                            LEFT JOIN (
                                SELECT 
                                    ItemID, SUM(WriteOffQty) AS WriteOffQty 
                                FROM 
                                    INVENTORY_ITEM_WRITE_OFF_RECORD
                                WHERE 
                                    RecordStatus=1
                                GROUP BY ItemID 
                            ) AS w ON w.ItemID=a.ItemID
                            LEFT JOIN (
                                SELECT 
                                    ItemID, SUM(UnitPrice*QtyChange) AS PurchasePrice 
                                FROM 
                                    INVENTORY_ITEM_BULK_LOG
                                WHERE 
                                    Action=1
                                GROUP BY ItemID 
                            ) AS p ON p.ItemID=a.ItemID
                            LEFT JOIN 
                                INVENTORY_ITEM_BULK_EXT t ON t.ItemID=a.ItemID
                    WHERE
                            a.RecordStatus = 1
                            AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                            ".$condition
                            .$success_bulk_item_cond
                            .$fail_bulk_item_cond."
                            AND (a.StockTakeOption = 'YES' 
                                    OR (a.StockTakeOption = 'FOLLOW_SETTING' 
                                        AND
                                            IF((IFNULL(t.Quantity,0) + w.WriteOffQty)>0, 
                                            	p.PurchasePrice/(IFNULL(t.Quantity,0) + w.WriteOffQty),0) * t.Quantity >=".$stocktakePriceBulk."
                                        )
                                )   
                            GROUP BY b.LocationID";
            $totalBulkItemAry = $this->returnResultSet($sql);
            $totalBulkItemAssoc = BuildMultiKeyAssoc($totalBulkItemAry,'LocationID','NrItem',1);
            $locationIDAry = array_unique(array_merge(array_keys($totalSingleItemAssoc), array_keys($totalBulkItemAssoc)));
// debug_pr($sql);
// print "totalBulkItemAssoc\n";
// debug_pr($totalBulkItemAssoc);
// print "locationIDAry\n";
// debug_pr($locationIDAry);

            $start_date = $this->stocktake_period_start;
            $end_date = $this->stocktake_period_end;

            ## stocktake done for single item, should include original location and missing
            $sql = "SELECT 
                        a.LocationID, a.NrItem, a.MaxDateInput, b.PersonInCharge
                    FROM ( 
                            SELECT 
                                    b.LocationID, count(DISTINCT b.ItemID) as NrItem, MAX(c.DateInput) as MaxDateInput
                            FROM 
                                    INVENTORY_ITEM AS a
                                    INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID 
                                    INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS c ON (c.ItemID = b.ItemID)
                                    ".$priceCondition."
                            WHERE 
                                    a.RecordStatus = 1
                                    AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition." 
                                    AND c.Action IN (2,4) 
                                    AND (c.RecordDate BETWEEN '$start_date' AND '$end_date') 
                            GROUP BY 
                                    b.LocationID
                        ) as a
                    INNER JOIN 
                         (
                            SELECT 
                                    b.LocationID, c.DateInput, IFNULL(" . getNameFieldByLang2("u.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "') as PersonInCharge
                            FROM
                                    INVENTORY_ITEM AS a
                                    INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID
                                    INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS c ON (c.ItemID = b.ItemID)
                                    ".$priceCondition."
                                    LEFT JOIN INTRANET_USER u ON u.UserID=c.PersonInCharge
                            WHERE 
                                    a.RecordStatus = 1
                                    AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition." 
                                    AND c.Action IN (2,4) 
                                    AND (c.RecordDate BETWEEN '$start_date' AND '$end_date') 
                            ) AS b ON (b.LocationID=a.LocationID AND b.DateInput=a.MaxDateInput) 
                     ORDER BY a.LocationID";
            $completeSingleItemAry = $this->returnResultSet($sql);
            $completeSingleItemAssoc = BuildMultiKeyAssoc($completeSingleItemAry,'LocationID');
// debug_pr($sql);
// print "completeSingleItemAssoc\n";
// debug_pr($completeSingleItemAssoc);

            ## stocktake done for bulk items
            $sql = "SELECT 
                        a.LocationID, a.NrItem, a.MaxDateInput, b.PersonInCharge
                    FROM ( 
                            SELECT 
                                    b.LocationID, count(DISTINCT CONCAT(b.ItemID,'-',b.FundingSource)) as NrItem, MAX(b.DateInput) as MaxDateInput
                            FROM 
                                    INVENTORY_ITEM_BULK_LOG AS b 
                            WHERE 
                                    b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition 
                                    .$success_bulk_item_cond
                                    .$fail_bulk_item_cond."
                                    AND b.Action IN (2) 
                                    AND (b.RecordDate BETWEEN '$start_date' AND '$end_date') 
                            GROUP BY 
                                    b.LocationID
                         ) as a
                    INNER JOIN
                         (
                            SELECT 
                                    b.LocationID, b.DateInput, IFNULL(" . getNameFieldByLang2("u.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "') as PersonInCharge
                            FROM
                                    INVENTORY_ITEM_BULK_LOG AS b
                                    LEFT JOIN INTRANET_USER u ON u.UserID=b.PersonInCharge
                            WHERE 
                                    b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition 
                                    .$success_bulk_item_cond
                                    .$fail_bulk_item_cond."
                                    AND b.Action IN (2) 
                                    AND (b.RecordDate BETWEEN '$start_date' AND '$end_date') 
                            ) AS b ON (b.LocationID=a.LocationID AND b.DateInput=a.MaxDateInput) 
                     ORDER BY a.LocationID";

            $completeBulkItemAry = $this->returnResultSet($sql);
            $completeBulkItemAssoc = BuildMultiKeyAssoc($completeBulkItemAry,'LocationID');
// debug_pr($sql);
// print "completeBulkItemAssoc\n";
// debug_pr($completeBulkItemAssoc);
// print "before locationAry \n";
            $locationAry = $location->getBuildingFloorRoomNameAry($locationIDAry);
// print "after locationAry \n";
// debug_pr($locationAry);            
            $buildingAssoc = BuildMultiKeyAssoc($locationAry,array('BuildingID'),array('BuildingName'),1);
            $floorAssoc = BuildMultiKeyAssoc($locationAry,array('BuildingID','LocationLevelID'),array('FloorName'));
            $roomAssoc = BuildMultiKeyAssoc($locationAry,array('BuildingID','LocationLevelID','LocationID'), array('LocationID','LocationName'));
// print "roomAssoc \n";
// debug_pr($roomAssoc);
            $newRoomAssoc = array();
            foreach((array)$roomAssoc as $_buildingID=>$_buildingAssoc) {
                foreach((array)$_buildingAssoc as $_floorID=>$_floorAssoc) {
                    $nrIncompleteByFloor = 0;
                    foreach((array)$_floorAssoc as $_locationID=>$_roomAssoc) {
                        $_singleComplete = $completeSingleItemAssoc[$_locationID]['NrItem'] ? $completeSingleItemAssoc[$_locationID]['NrItem'] : 0;
                        $_bulkComplete = $completeBulkItemAssoc[$_locationID]['NrItem'] ? $completeBulkItemAssoc[$_locationID]['NrItem'] : 0;
                        $_numberComplete = $_singleComplete + $_bulkComplete;
                        $_totalSingle = $totalSingleItemAssoc[$_locationID] ? $totalSingleItemAssoc[$_locationID] : 0;
                        $_totalBulk = $totalBulkItemAssoc[$_locationID] ? $totalBulkItemAssoc[$_locationID] : 0;
                        $_total = $_totalSingle + $_totalBulk;
                        $_numberIncomplete = $_total - $_numberComplete;

                        if ($completeSingleItemAssoc[$_locationID]['MaxDateInput'] && $completeBulkItemAssoc[$_locationID]['MaxDateInput']) {
                            if ($completeSingleItemAssoc[$_locationID]['MaxDateInput'] > $completeBulkItemAssoc[$_locationID]['MaxDateInput']) {
                                $_maxDateInput = $completeSingleItemAssoc[$_locationID]['MaxDateInput'];
                                $_personInCharge = $completeSingleItemAssoc[$_locationID]['PersonInCharge'];
                            }
                            else {
                                $_maxDateInput = $completeBulkItemAssoc[$_locationID]['MaxDateInput'];
                                $_personInCharge = $completeBulkItemAssoc[$_locationID]['PersonInCharge'];
                            }
                        }
                        else if ($completeSingleItemAssoc[$_locationID]['MaxDateInput'] && !$completeBulkItemAssoc[$_locationID]['MaxDateInput']) {
                            $_maxDateInput = $completeSingleItemAssoc[$_locationID]['MaxDateInput'];
                            $_personInCharge = $completeSingleItemAssoc[$_locationID]['PersonInCharge'];
                        }
                        else if (!$completeSingleItemAssoc[$_locationID]['MaxDateInput'] && $completeBulkItemAssoc[$_locationID]['MaxDateInput']) {
                            $_maxDateInput = $completeBulkItemAssoc[$_locationID]['MaxDateInput'];
                            $_personInCharge = $completeBulkItemAssoc[$_locationID]['PersonInCharge'];
                            
                        }
                        else {
                            $_maxDateInput = '';
                            $_personInCharge =  '';
                        }

                        $_roomAssoc['Total'] = $_total;
                        $_roomAssoc['NumberComplete'] = $_numberComplete;
                        $_roomAssoc['NumberIncomplete'] = $_numberIncomplete;
                        $_roomAssoc['MaxDateInput'] = $_maxDateInput;
                        $_roomAssoc['PersonInCharge'] = $_personInCharge;
                        $newRoomAssoc[$_buildingID][$_floorID][$_locationID] = $_roomAssoc;
                        $nrIncompleteByFloor += $_numberIncomplete;
                    }
                    $floorAssoc[$_buildingID][$_floorID]['NumberIncomplete'] = $nrIncompleteByFloor;
                }
            }

            $ret = array();
            $ret['Building'] = $buildingAssoc;
            $ret['Floor'] = $floorAssoc;
            $ret['Room'] = $newRoomAssoc;
// debug_pr($ret);            
            return $ret;
        }

        
        ## return stock take status by admin group for specific location 
        function getStocktakeProgressByLocation($locationID)
        {
            global $PATH_WRT_ROOT, $Lang, $sys_custom;
            include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
            $location = new liblocation();

            $adminGroupIDAry = $this->getAdminGroupIDAry();
            $adminGroupAry = $this->returnAdminGroup($adminGroupIDAry);
            $adminGroupAssoc = BuildMultiKeyAssoc($adminGroupAry, 'AdminGroupID','AdminGroupName',1);
            $adminGroupIDStr = implode("','",$adminGroupIDAry);
            
            $priceCondition = "";
            if ($sys_custom['eInventory_PriceCeiling']) {
                $priceCondition = " INNER JOIN INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID AND (b.UnitPrice>=d.PriceCeiling)) ";
            }
            
            $condition = $locationID ? " AND b.LocationID='".$locationID."' " : "";
            $stocktakePriceSingle = $this->StockTakePriceSingle > 0 ? $this->StockTakePriceSingle : 0;
            
            ## total items by AdminGroup for single item
            $sql = "SELECT
                            b.GroupInCharge as AdminGroupID,
                            COUNT(*) AS NrItem
                    FROM
                            INVENTORY_ITEM a
                            INNER JOIN INVENTORY_ITEM_SINGLE_EXT b ON b.ItemID=a.ItemID
                            ".$priceCondition."
                    WHERE
                            a.RecordStatus = 1
                            AND (a.StockTakeOption = 'YES'
                                    OR (a.StockTakeOption = 'FOLLOW_SETTING'
                                        AND IF(b.UnitPrice>0,b.UnitPrice,IFNULL(b.PurchasedPrice,0))>=".$stocktakePriceSingle."))
                            AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                                
                            ".$condition."
                            GROUP BY b.GroupInCharge";
            $totalSingleItemAry = $this->returnResultSet($sql);
            $totalSingleItemAssoc = BuildMultiKeyAssoc($totalSingleItemAry,'AdminGroupID','NrItem',1);
            // debug_pr($sql);
            // print "totalSingleItemAssoc\n";
            // debug_pr($totalSingleItemAssoc);
            
            ## for bulk items
            $success_bulk_item_cond = "";
            $fail_bulk_item_cond = "";
            
            if ($sys_custom['eInventory_PriceCeiling']) {
                $bulkItemCeilingPriceCondition = $this->getBulkItemCeilingPriceCondition($condition, $adminGroupIDStr);
                $success_bulk_item_cond = $bulkItemCeilingPriceCondition['success'];
                $fail_bulk_item_cond = $bulkItemCeilingPriceCondition['fail'];
            }
            //debug_pr($fail_bulk_item_cond);
            $stocktakePriceBulk = $this->StockTakePriceBulk > 0 ? $this->StockTakePriceBulk : 0;
            
            ## total items by AdminGroup for bulk items
            $sql = "SELECT
                            b.GroupInCharge as AdminGroupID,
                            COUNT(DISTINCT CONCAT(a.ItemID,'-',b.FundingSourceID)) AS NrItem
                    FROM
                            INVENTORY_ITEM a
                            INNER JOIN INVENTORY_ITEM_BULK_LOCATION b ON b.ItemID=a.ItemID
                            LEFT JOIN (
                                SELECT
                                    ItemID, SUM(WriteOffQty) AS WriteOffQty
                                FROM
                                    INVENTORY_ITEM_WRITE_OFF_RECORD
                                WHERE
                                    RecordStatus=1
                                GROUP BY ItemID
                            ) AS w ON w.ItemID=a.ItemID
                            LEFT JOIN (
                                SELECT
                                    ItemID, SUM(UnitPrice*QtyChange) AS PurchasePrice
                                FROM
                                    INVENTORY_ITEM_BULK_LOG
                                WHERE
                                    Action=1
                                GROUP BY ItemID
                            ) AS p ON p.ItemID=a.ItemID
                            LEFT JOIN
                                INVENTORY_ITEM_BULK_EXT t ON t.ItemID=a.ItemID
                    WHERE
                            a.RecordStatus = 1
                            AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                            ".$condition
                            .$success_bulk_item_cond
                            .$fail_bulk_item_cond."
                            AND (a.StockTakeOption = 'YES'
                                    OR (a.StockTakeOption = 'FOLLOW_SETTING'
                                        AND
                                            IF((IFNULL(t.Quantity,0) + w.WriteOffQty)>0,
                                            	p.PurchasePrice/(IFNULL(t.Quantity,0) + w.WriteOffQty),0) * t.Quantity >=".$stocktakePriceBulk."
                                        )
                                )
                            GROUP BY b.GroupInCharge";
            $totalBulkItemAry = $this->returnResultSet($sql);
            $totalBulkItemAssoc = BuildMultiKeyAssoc($totalBulkItemAry,'AdminGroupID','NrItem',1);
            $qtyByAdminGroupIDAry = array_unique(array_merge(array_keys($totalSingleItemAssoc), array_keys($totalBulkItemAssoc)));
            // debug_pr($sql);
            // print "totalBulkItemAssoc\n";
            // debug_pr($totalBulkItemAssoc);
            // print "qtyByAdminGroupIDAry\n";
            // debug_pr($qtyByAdminGroupIDAry);
            
            $start_date = $this->stocktake_period_start;
            $end_date = $this->stocktake_period_end;
            
            ## stocktake done for single item
            $sql = "SELECT
                        a.AdminGroupID, a.NrItem, a.MaxDateInput, b.PersonInCharge
                    FROM (
                            SELECT
                                    b.GroupInCharge as AdminGroupID, count(DISTINCT b.ItemID) as NrItem, MAX(c.DateInput) as MaxDateInput
                            FROM
                                    INVENTORY_ITEM AS a
                                    INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID
                                    INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS c ON (c.ItemID = b.ItemID)
                                    ".$priceCondition."
                            WHERE
                                    a.RecordStatus = 1
                                    AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition."
                                    AND c.Action IN (2,4)
                                    AND (c.RecordDate BETWEEN '$start_date' AND '$end_date')
                                    GROUP BY b.GroupInCharge
                          ) as a
                    INNER JOIN
                          (
                            SELECT
                                    b.GroupInCharge as AdminGroupID, c.DateInput, IFNULL(" . getNameFieldByLang2("u.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "') as PersonInCharge
                            FROM
                                    INVENTORY_ITEM AS a
                                    INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID
                                    INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS c ON (c.ItemID = b.ItemID)
                                    ".$priceCondition."
                                    LEFT JOIN INTRANET_USER u ON u.UserID=c.PersonInCharge
                            WHERE
                                    a.RecordStatus = 1
                                    AND b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition."
                                    AND c.Action IN (2,4)
                                    AND (c.RecordDate BETWEEN '$start_date' AND '$end_date')
                          ) AS b ON (b.AdminGroupID=a.AdminGroupID AND b.DateInput=a.MaxDateInput)
                                    ORDER BY a.AdminGroupID";
            $completeSingleItemAry = $this->returnResultSet($sql);
            $completeSingleItemAssoc = BuildMultiKeyAssoc($completeSingleItemAry,'AdminGroupID');
            // debug_pr($sql);
            // print "completeSingleItemAssoc\n";
            // debug_pr($completeSingleItemAssoc);
                            
            ## stocktake done for bulk items
            $sql = "SELECT
                        a.AdminGroupID, a.NrItem, a.MaxDateInput, b.PersonInCharge
                    FROM (
                            SELECT
                                    b.GroupInCharge as AdminGroupID, count(DISTINCT b.ItemID) as NrItem, MAX(b.DateInput) as MaxDateInput
                            FROM
                                    INVENTORY_ITEM_BULK_LOG AS b
                            WHERE
                                    b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition
                                    .$success_bulk_item_cond
                                    .$fail_bulk_item_cond."
                                    AND b.Action IN (2)
                                    AND (b.RecordDate BETWEEN '$start_date' AND '$end_date')
                                    GROUP BY b.GroupInCharge
                         ) as a
                    INNER JOIN
                         (
                            SELECT
                                    b.GroupInCharge as AdminGroupID, b.DateInput, IFNULL(" . getNameFieldByLang2("u.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "') as PersonInCharge
                            FROM
                                    INVENTORY_ITEM_BULK_LOG AS b
                                    LEFT JOIN INTRANET_USER u ON u.UserID=b.PersonInCharge
                            WHERE
                                    b.GroupInCharge IN ('".$adminGroupIDStr."')
                                    ".$condition
                                    .$success_bulk_item_cond
                                    .$fail_bulk_item_cond."
                                    AND b.Action IN (2)
                                    AND (b.RecordDate BETWEEN '$start_date' AND '$end_date')
                         ) AS b ON (b.AdminGroupID=a.AdminGroupID AND b.DateInput=a.MaxDateInput)
                                    ORDER BY a.AdminGroupID";
                                    
            $completeBulkItemAry = $this->returnResultSet($sql);
            $completeBulkItemAssoc = BuildMultiKeyAssoc($completeBulkItemAry,'AdminGroupID');
            // debug_pr($sql);
            // print "completeBulkItemAssoc\n";
            // debug_pr($completeBulkItemAssoc);
                                    
            $newAdminGroupAssoc = array();
            foreach((array)$adminGroupAssoc as $_adminGroupID=>$_adminGroupName) {
                
                $_singleComplete = $completeSingleItemAssoc[$_adminGroupID]['NrItem'] ? $completeSingleItemAssoc[$_adminGroupID]['NrItem'] : 0;
                $_bulkComplete = $completeBulkItemAssoc[$_adminGroupID]['NrItem'] ? $completeBulkItemAssoc[$_adminGroupID]['NrItem'] : 0;
                $_numberComplete = $_singleComplete + $_bulkComplete;
                $_totalSingle = $totalSingleItemAssoc[$_adminGroupID] ? $totalSingleItemAssoc[$_adminGroupID] : 0;
                $_totalBulk = $totalBulkItemAssoc[$_adminGroupID] ? $totalBulkItemAssoc[$_adminGroupID] : 0;
                $_total = $_totalSingle + $_totalBulk;
                $_numberIncomplete = $_total - $_numberComplete;
                
                if ($completeSingleItemAssoc[$_adminGroupID]['MaxDateInput'] && $completeBulkItemAssoc[$_adminGroupID]['MaxDateInput']) {
                    if ($completeSingleItemAssoc[$_adminGroupID]['MaxDateInput'] > $completeBulkItemAssoc[$_adminGroupID]['MaxDateInput']) {
                        $_maxDateInput = $completeSingleItemAssoc[$_adminGroupID]['MaxDateInput'];
                        $_personInCharge = $completeSingleItemAssoc[$_adminGroupID]['PersonInCharge'];
                    }
                    else {
                        $_maxDateInput = $completeBulkItemAssoc[$_adminGroupID]['MaxDateInput'];
                        $_personInCharge = $completeBulkItemAssoc[$_adminGroupID]['PersonInCharge'];
                    }
                }
                else if ($completeSingleItemAssoc[$_adminGroupID]['MaxDateInput'] && !$completeBulkItemAssoc[$_adminGroupID]['MaxDateInput']) {
                    $_maxDateInput = $completeSingleItemAssoc[$_adminGroupID]['MaxDateInput'];
                    $_personInCharge = $completeSingleItemAssoc[$_adminGroupID]['PersonInCharge'];
                }
                else if (!$completeSingleItemAssoc[$_adminGroupID]['MaxDateInput'] && $completeBulkItemAssoc[$_adminGroupID]['MaxDateInput']) {
                    $_maxDateInput = $completeBulkItemAssoc[$_adminGroupID]['MaxDateInput'];
                    $_personInCharge = $completeBulkItemAssoc[$_adminGroupID]['PersonInCharge'];
                }
                else {
                    $_maxDateInput = '';
                    $_personInCharge =  '';
                }
                
                $_adminGroupAssoc = array();
                $_adminGroupAssoc['AdminGroupName'] = $_adminGroupName;
                $_adminGroupAssoc['Total'] = $_total;
                $_adminGroupAssoc['NumberComplete'] = $_numberComplete;
                $_adminGroupAssoc['NumberIncomplete'] = $_numberIncomplete;
                $_adminGroupAssoc['MaxDateInput'] = $_maxDateInput;
                $_adminGroupAssoc['PersonInCharge'] = $_personInCharge;
                if ($_total > 0) {      // don't show if total is zero
                    $newAdminGroupAssoc[$_adminGroupID] = $_adminGroupAssoc;
                }
            }                        
            return $newAdminGroupAssoc;
        }
        
        /*
         *  check if item admin group and location match the pass in values
         */
        function checkItemAdminGroupAndLocationByBarcode($barcode, $adminGroupID, $locationID)
        {
            $returnAry = array();
            $isAdminGroupFound = false;
            $isLocationFound = false;
            $matchBoth = false;
            $itemType = '';
            $locationIDAry = array();
            $adminGroupLocationIDAry = array();
            $adminGroupIDAry = array();
            $locationAdminGroupIDAry = array();
            
            $sql = "SELECT
                            i.ItemID,
                            i.ItemType,
                            e.GroupInCharge as AdminGroupID,
                            e.LocationID
                    FROM
                            INVENTORY_ITEM i
                            INNER JOIN INVENTORY_ITEM_SINGLE_EXT e ON e.ItemID=i.ItemID
                    WHERE
                            e.TagCode='".$barcode."'
                            ORDER BY ItemID";
            $itemAry = $this->returnResultSet($sql);
            if (count($itemAry)>0) {
                foreach($itemAry as $_itemAry) {
                    $_adminGroupID = $_itemAry['AdminGroupID'];
                    $_locationID = $_itemAry['LocationID'];
                    if (($adminGroupID != '') && ($_adminGroupID == $adminGroupID) && ($locationID != '') && ($_locationID == $locationID)) {
                        $matchBoth = true;
                    }
                    if (($adminGroupID != '') && ($_adminGroupID == $adminGroupID)) {
                        $isAdminGroupFound = true;
                        $adminGroupLocationIDAry[] = $_locationID;
                    }
                    if (($locationID != '') && ($_locationID == $locationID)) {
                        $isLocationFound = true;
                        $locationAdminGroupIDAry[] = $_adminGroupID;
                    }
                    $locationIDAry[] = $_locationID;
                    $adminGroupIDAry[] = $_adminGroupID;
                }
                $itemType = ITEM_TYPE_SINGLE;
                if (count($adminGroupLocationIDAry)) {
                    $locationIDAry = $adminGroupLocationIDAry;      // with admin group restriction
                }
                if (count($locationAdminGroupIDAry)) {
                    $adminGroupIDAry = $locationAdminGroupIDAry;    // with location restriction
                }
            }
            else {
                $sql = "SELECT
                                i.ItemID,
                                i.ItemType,
                                b.GroupInCharge as AdminGroupID,
                                b.LocationID,
                                b.FundingSourceID
                        FROM
                                INVENTORY_ITEM i
                                INNER JOIN INVENTORY_ITEM_BULK_EXT e ON e.ItemID=i.ItemID
                                INNER JOIN INVENTORY_ITEM_BULK_LOCATION b ON (b.ItemID=i.ItemID AND b.Quantity!=0)
                        WHERE
                                e.Barcode='".$barcode."'
                                ORDER BY ItemID";
                $itemAry = $this->returnResultSet($sql);
                            
                if (count($itemAry)>0) {
                    foreach($itemAry as $_itemAry) {
                        $_adminGroupID = $_itemAry['AdminGroupID'];
                        $_locationID = $_itemAry['LocationID'];
                        if (($adminGroupID != '') && ($_adminGroupID == $adminGroupID) && ($locationID != '') && ($_locationID == $locationID)) {
                            $matchBoth = true;
                        }
                        if (($adminGroupID != '') && ($_adminGroupID == $adminGroupID)) {
                            $isAdminGroupFound = true;
                            $adminGroupLocationIDAry[] = $_locationID;
                        }
                        if (($locationID != '') && ($_locationID == $locationID)) {
                            $isLocationFound = true;
                            $locationAdminGroupIDAry[] = $_adminGroupID;
                        }
                        $locationIDAry[] = $_locationID;
                        $adminGroupIDAry[] = $_adminGroupID;
                    }
                    $itemType = ITEM_TYPE_BULK;
                    if (count($adminGroupLocationIDAry)) {
                        $locationIDAry = $adminGroupLocationIDAry;      // with admin group restriction
                    }
                    if (count($locationAdminGroupIDAry)) {
                        $adminGroupIDAry = $locationAdminGroupIDAry;    // with location restriction
                    }                    
                }
            }
            $locationIDAry = array_unique($locationIDAry);
            $adminGroupIDAry = array_unique($adminGroupIDAry);
            $returnAry['IsAdminGroupFound'] = $isAdminGroupFound;
            $returnAry['IsLocationFound'] = $isLocationFound;
            $returnAry['MatchBoth'] = $matchBoth;
            $returnAry['ItemType'] = $itemType;
            $returnAry['LocationID'] = implode(',',$locationIDAry);
            $returnAry['AdminGroupID'] = implode(',',$adminGroupIDAry);

            return $returnAry;
        }
        
        /*
         *  Note: Barcode should be unique in single and bulk items, either found in single or bulk item but not both.
         *  also check for stocktake
         */
        function getItemByBarcode($barcode, $adminGroupID='', $locationID='')
        {
//            CONCAT('<a href=\"','?task=teacherApp.', IF(i.ItemType=1,'view_single_item','view_bulk_item'), '&ItemID=',i.ItemID,'&ItemType=',i.ItemType,'\">',".$this->getInventoryNameByLang('i.').",'</a>') as ItemName,
            $singleAdminGroupCond = '';
            $bulkAdminGroupCond = '';
            $bulkLocationCond = '';
            if (!is_array($adminGroupID)) {
                $adminGroupIDAry = array($adminGroupID);
            }
            else {
                $adminGroupIDAry = $adminGroupID;
            }
            
            if (count($adminGroupIDAry)) {
                $singleAdminGroupCond = " AND e.GroupInCharge IN ('".implode("','",$adminGroupIDAry)."') ";
                $bulkAdminGroupCond = " AND b.GroupInCharge IN ('".implode("','",$adminGroupIDAry)."') ";
            }
            if ($locationID) {
                $bulkLocationCond= " AND b.LocationID='".$locationID."' ";  // don't set location condition for single item because it's allowed to do stock take from other locations
            }
            
            $returnAry = array();
            $sql = "SELECT 
                            i.ItemID, 
                            i.ItemType, 
                            ".$this->getInventoryNameByLang('i.')." as ItemName,
                            i.StockTakeOption, 
                            e.TagCode as Barcode,
                            e.GroupInCharge as AdminGroupID,
                            e.LocationID,
                            e.PurchasedPrice,
                            e.UnitPrice
                    FROM 
                            INVENTORY_ITEM i
                            INNER JOIN INVENTORY_ITEM_SINGLE_EXT e ON e.ItemID=i.ItemID
                    WHERE 
                            e.TagCode='".$barcode."'"
                            . $singleAdminGroupCond
                            . " ORDER BY ItemID";
            $itemAry = $this->returnResultSet($sql);
//             debug_pr($sql);
//             debug_pr($itemAry);
            if (count($itemAry)>0) {
                $returnAry = $this->FilterForStockTakeSingle($itemAry);
                $nrItem = count($returnAry);
                if ($nrItem > 0) {
                    for ($i=0; $i<$nrItem; $i++) {
                        $returnAry[$i]['IsDoneStocktake'] = $this->isStocktakeDoneForSingleItem($returnAry[$i]['ItemID']);
                    }
                }
            }
            else {
                $sql = "SELECT
                                i.ItemID,
                                i.ItemType, ".
                                $this->getInventoryNameByLang('i.')." as ItemName,
                                i.StockTakeOption,
                                e.Barcode,
                                b.GroupInCharge as AdminGroupID,
                                b.LocationID,
                                b.FundingSourceID,
                                ".$this->getInventoryItemNameByLang('c.')." as FundingSourceName
                        FROM
                                INVENTORY_ITEM i
                                INNER JOIN INVENTORY_ITEM_BULK_EXT e ON e.ItemID=i.ItemID
                                INNER JOIN INVENTORY_ITEM_BULK_LOCATION b ON (b.ItemID=i.ItemID AND b.Quantity!=0)
                                LEFT JOIN INVENTORY_FUNDING_SOURCE c ON c.FundingSourceID=b.FundingSourceID
                        WHERE
                                e.Barcode='".$barcode."'"
                                . $bulkAdminGroupCond. $bulkLocationCond . "
                                ORDER BY ItemID";
                $itemAry = $this->returnResultSet($sql);
                
                if (count($itemAry)>0) {
                    $returnAry = $this->FilterForStockTakeBulk($itemAry);
                    $nrItem = count($returnAry);
                    if ($nrItem > 0) {
                        for ($i=0; $i<$nrItem; $i++) {
                            $thisItem = $returnAry[$i];
                            $_itemID = $thisItem['ItemID'];
                            $_locationID = $thisItem['LocationID'];
                            $_adminGroupID = $thisItem['AdminGroupID'];
                            $_fundtionSourceID = $thisItem['FundingSourceID'];
                            $returnAry[$i]['IsDoneStocktake'] = $this->isStocktakeDoneForBulkItem($_itemID, $_locationID, $_adminGroupID, $_fundingSourceID);
                        }
                    }
                }
            }
            //$itemAssoc = BuildMultiKeyAssoc($itemAry, array('ItemID'));
//debug_pr($returnAry);            
            return $returnAry;
        }
        
        // use to check if the item has been done stocktake or not
        function getStocktakeStatus($itemID, $itemType, $locationID='', $groupInCharge='', $fundingSource='')
        {
            global $sys_custom;
            
            $start_date = $this->stocktake_period_start;
            $end_date = $this->stocktake_period_end;
            
            switch ($itemType)
            {
                case ITEM_TYPE_SINGLE:
                    $sql = "SELECT 
                                    b.LocationID
                            FROM 
                                    INVENTORY_ITEM AS a
                                    INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID 
                                    INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS c ON (c.ItemID = b.ItemID)
                            WHERE 
                                    a.ItemID='".$itemID."'
                                    AND c.Action IN (2) 
                                    AND (c.RecordDate BETWEEN '$start_date' AND '$end_date')";
                    $locationAry = $this->returnResultSet($sql);
                    
                    $status = count($locationAry) ? ITEM_STOCKTAKE_STATUS_DONE : ITEM_STOCKTAKE_STATUS_NOTDONE;
//debug_pr($sql);
// echo "status<br>";
// debug_pr($status);                    
                    break;
                    
                case ITEM_TYPE_BULK:
                    $condition = $locationID ? " AND b.LocationID='".$locationID."' " : "";
                    $condition .= $groupInCharge ? " AND b.GroupInCharge='".$groupInCharge."' " : "";
                    $condition .= $fundingSource ? " AND b.FundingSource='".$fundingSource."' " : "";
                    $sql = "SELECT 
                                    b.LocationID
                            FROM 
                                    INVENTORY_ITEM_BULK_LOG AS b 
                            WHERE 
                                    b.ItemID='".$itemID."'"
                                    .$condition."
                                    AND b.Action IN (2) 
                                    AND (b.RecordDate BETWEEN '$start_date' AND '$end_date') 
                            ORDER BY 
                                    b.LocationID";
                    $locationAry = $this->returnResultSet($sql);

                    $status = count($locationAry) ? ITEM_STOCKTAKE_STATUS_DONE : ITEM_STOCKTAKE_STATUS_NOTDONE;
// debug_pr($sql);
// echo "status<br>";
// debug_pr($status);
                    
                    break;
            }
            return $status;
        }
        
        function getBulkItemFundingAndQty($itemID, $locationID, $groupInCharge, $fundingSource='')
        {
            $sql = "SELECT 
                            FundingSourceID, Quantity 
                    FROM 
                            INVENTORY_ITEM_BULK_LOCATION 
                    WHERE
                            ItemID='".$itemID."'
                            AND LocationID='".$locationID."'
                            AND GroupInCharge='".$groupInCharge."'";
            if ($fundingSource) {
                $sql .= " AND FundingSourceID='".$fundingSource."'";
            }
            $resultAry = $this->returnResultSet($sql);
            return $resultAry;
        }

        function getSingleItemGroupInCharge($itemID)
        {
            $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID='".$itemID."'";
            $groupInChargeAry = $this->returnResultSet($sql);
            return count($groupInChargeAry) ? $groupInChargeAry[0]['GroupInCharge'] : ''; 
        }
        
        function isItemInLocation($itemID, $itemType, $locationID)
        {
            switch ($itemType)
            {
                case ITEM_TYPE_SINGLE:
                    $sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID='".$itemID."' AND LocationID='".$locationID."'";
                    $result = $this->returnResultSet($sql);
                    break;
                    
                case ITEM_TYPE_BULK:
                    $sql = "SELECT ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID='".$itemID."' AND LocationID='".$locationID."' AND Quantity>0";
                    $result = $this->returnResultSet($sql);
                    break;
            }
            return count($result) ? true : false;
        }
        
        function isSurplusRecordExist($itemID)
        {
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$itemID."'";
            $result = $this->returnResultSet($sql);
            return count($result) ? true : false;
        }

        function isMissingRecordExist($itemID)
        {
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$itemID."' AND RecordStatus = 0";
            $result = $this->returnResultSet($sql);
            return count($result) ? true : false;
        }
        
        function getSurplusRecordID($itemID, $locationID, $adminGroupID, $fundingSourceID, $recordStatus=0)
        {
            $sql = "SELECT 
                            RecordID 
                    FROM 
                            INVENTORY_ITEM_SURPLUS_RECORD 
                    WHERE 
                            ItemID = '".$itemID."'
                            AND LocationID = '". $locationID."'
                            AND AdminGroupID='".$adminGroupID."'
                            AND FundingSourceID='".$fundingSourceID."'
                            AND RecordStatus='".$recordStatus."'";
            $result = $this->returnResultSet($sql);
// debug_pr($sql);
// print "getSurplusRecodID\n";
// debug_pr($result);
            return count($result) ? Get_Array_By_Key($result, 'RecordID') : null;
        }

        function getMissingRecordID($itemID, $locationID, $adminGroupID, $fundingSourceID, $recordStatus=0)
        {
            $sql = "SELECT
                            RecordID
                    FROM
                            INVENTORY_ITEM_MISSING_RECORD
                    WHERE
                            ItemID = '".$itemID."'
                            AND LocationID = '". $locationID."'
                            AND AdminGroupID='".$adminGroupID."'
                            AND FundingSourceID='".$fundingSourceID."'
                            AND RecordStatus='".$recordStatus."'";
            $result = $this->returnResultSet($sql);
// debug_pr($sql);
// print "getMissingRecodID\n";
// debug_pr($result);
            
            return count($result) ? Get_Array_By_Key($result, 'RecordID') : null;
        }
        
        function doStocktake($itemID, $itemType, $locationID='', $groupInCharge='', $fundingSource='')
        {
            $today = date('Y-m-d');

            $result = array();
            switch ($itemType)
            {
                case ITEM_TYPE_SINGLE:
                    $sql = "INSERT INTO 
                                INVENTORY_ITEM_SINGLE_STATUS_LOG
                                (ItemID, RecordDate, Action, PersonInCharge, DateInput, DateModified)
                            VALUES
                            ('$itemID', '$today', ". ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION .", '".$_SESSION['UserID']."',  NOW(), NOW())";
                    $result[] = $this->db_db_query($sql);
                    
                    break;
                    
                case ITEM_TYPE_BULK:
                    $bulkItemFundingAndQtyAry = $this->getBulkItemFundingAndQty($itemID, $locationID, $groupInCharge, $fundingSource);
                    for($i=0, $iMax=count($bulkItemFundingAndQtyAry); $i<$iMax; $i++) {
                        $fundingSourceID = $bulkItemFundingAndQtyAry[$i]['FundingSourceID'];
                        $stockCheckQty = $bulkItemFundingAndQtyAry[$i]['Quantity'];
                        $sql = "INSERT INTO INVENTORY_ITEM_BULK_LOG
                                    (ItemID, RecordDate, Action, QtyChange, FundingSource, PersonInCharge, 
                                    DateInput, DateModified, LocationID, GroupInCharge, StockCheckQty)
                                VALUES
                                    ('$itemID', '$today', ". ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION .", 0, '$fundingSourceID', '".$_SESSION['UserID']."',
                                    NOW(), NOW(), '$locationID', '$groupInCharge', '$stockCheckQty')";
                        $result[] = $this->db_db_query($sql);
                    }                    
                    break;
            }
            return !in_array(false,$result) ? true : false;
        }
        
        
        function addSingleItemSurplus($itemID, $locationID='')
        {
            $result = array();
            $today = date('Y-m-d');
            $this->Start_Trans();
            
            if ($this->isSurplusRecordExist($itemID)) {
                $sql = "UPDATE
                            INVENTORY_ITEM_SURPLUS_RECORD
                        SET
                            LocationID = '$locationID',
                            RecordDate = '$today',
                            PersonInCharge = '".$_SESSION['UserID']."',
                            DateModified = NOW()
                        WHERE
                            ItemID = '".$itemID."'";
                $result['UpdateSurplus'] = $this->db_db_query($sql);
            }
            else {
                $adminGroupID = $this->getSingleItemGroupInCharge($itemID);
                
                $sql = "INSERT INTO
                            INVENTORY_ITEM_SURPLUS_RECORD
                            (ItemID, LocationID, AdminGroupID, RecordDate, SurplusQty, PersonInCharge,
                            RecordType, RecordStatus, DateInput, DateModified)
                        VALUES
                            ('$itemID', '$locationID', '$adminGroupID', '$today', 1, '".$_SESSION['UserID']."',
                            0, 0, NOW(), NOW())";
                $result['AddSurplus'] = $this->db_db_query($sql);
            }
            
            $sql = "INSERT INTO
                        INVENTORY_ITEM_SINGLE_STATUS_LOG
                        (ItemID, RecordDate, Action, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified)
                    VALUES
                        ('$itemID', '$today', '". ITEM_ACTION_STOCKTAKE_OTHER_LOCATION."', '".$_SESSION['UserID']."', 0, 0, NOW(), NOW())";
            $result['AddStatusLog'] = $this->db_db_query($sql);
                    
            if (!in_array(false,$result)) {
                $this->Commit_Trans();
                $return = true;
            }
            else {
                $this->RollBack_Trans();
                $return = false;
            }
            return $return;
            
        }
        
        function updateSingleItemMissing($itemID, $locationID, $adminGroupID, $remark)
        {
            $result = array();
            $today = date('Y-m-d');
            $this->Start_Trans();
            
            if ($this->isMissingRecordExist($itemID)) {
                
                $sql = "UPDATE 
                            INVENTORY_ITEM_MISSING_RECORD
                        SET
							LocationID = '$locationID', 
							AdminGroupID = '$adminGroupID',
							RecordDate = '$today',
							MissingQty = 1,
							PersonInCharge = '".$_SESSION['UserID']."',
							RecordType = 0,
							RecordStatus = 0,
							DateModified = NOW()
						WHERE
                            ItemID = '".$itemID."'";
                $result['UpdateMissing'] = $this->db_db_query($sql);
            }
            else {
                $sql = "INSERT INTO
                            INVENTORY_ITEM_MISSING_RECORD
                            (ItemID, LocationID, AdminGroupID, RecordDate, MissingQty, PersonInCharge,
                            RecordType, RecordStatus, DateInput, DateModified)
                        VALUES
                            ('$itemID', '$locationID', '$adminGroupID', '$today', 1, '".$_SESSION['UserID']."',
                            0, 0 , NOW(), NOW())";
                $result['AddMissing'] = $this->db_db_query($sql);
            }
            
            $sql = "INSERT INTO
                        INVENTORY_ITEM_SINGLE_STATUS_LOG
                        (ItemID, RecordDate, Action, PersonInCharge, Remark, DateInput, DateModified)
                    VALUES
                    ('$itemID', '$today', " . ITEM_ACTION_STOCKTAKE_NOT_FOUND . ", '".$_SESSION['UserID']."', '".$this->Get_Safe_Sql_Query($remark)."', NOW(), NOW())";
            $result['AddStatusLog'] = $this->db_db_query($sql);

            if (!in_array(false,$result)) {
                $this->Commit_Trans();
                $return = true;
            }
            else {
                $this->RollBack_Trans();
                $return = false;
            }
            return $return;
        }

        function updateSingleItemRemark($itemID, $remark)
        {
            $result = array();
            $today = date('Y-m-d');
            $this->Start_Trans();
            
            if ($this->isMissingRecordExist($itemID)) {
                $sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID='".$itemID."'";
                $result['DeleteMissing'] = $this->db_db_query($sql);
            }
            
            $sql = "INSERT INTO
                        INVENTORY_ITEM_SINGLE_STATUS_LOG
                        (ItemID, RecordDate, Action, PersonInCharge, Remark, DateInput, DateModified)
                    VALUES
                    ('$itemID', '$today', " . ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION. ", '".$_SESSION['UserID']."', '".$this->Get_Safe_Sql_Query($remark)."', NOW(), NOW())";
            $result['AddStatusLog'] = $this->db_db_query($sql);
            
            if (!in_array(false,$result)) {
                $this->Commit_Trans();
                $return = true;
            }
            else {
                $this->RollBack_Trans();
                $return = false;
            }
            return $return;
        }
        
        /*
         *  $qtyChange = $stockCheckQty - expectedQty
         *  $stockCheckQty <- number of items found in the location
         */
        function updateBulkItemStocktake($itemID, $locationID, $adminGroupID, $fundingSourceID, $recordStatus=0, $qtyChange=0, $stockCheckQty=0, $remark='')
        {
            $result = array();
            $today = date('Y-m-d');
            
            $surplusRecordIDAry = $this->getSurplusRecordID($itemID, $locationID, $adminGroupID, $fundingSourceID, $recordStatus);
            $nrSurplusRecordID = count($surplusRecordIDAry);
// print "surplus\n";
// print "nrSurplus=".$nrSurplusRecordID."\n";
// debug_pr($surplusRecordIDAry);            

            $missingRecordIDAry = $this->getMissingRecordID($itemID, $locationID, $adminGroupID, $fundingSourceID, $recordStatus);
            $nrMissingRecordID = count($missingRecordIDAry);
// print "missing\n";
// print "nrMissing=".$nrMissingRecordID."\n";
// debug_pr($missingRecordIDAry);

            $this->Start_Trans();
            
            if ($qtyChange > 0) {       // Item Surplus
                if ($nrSurplusRecordID) {
                    $sql = "UPDATE
                                INVENTORY_ITEM_SURPLUS_RECORD
                            SET
                                RecordDate = '$today',
                                SurplusQty='".abs($qtyChange)."',
                                PersonInCharge = '".$_SESSION['UserID']."',
    							RecordType = 0,
    							RecordStatus = 0,
                                DateModified = NOW()
                            WHERE
                                RecordID IN ('".implode("','",$surplusRecordIDAry)."')";
                    $result['UpdateSurplus'] = $this->db_db_query($sql);
// debug_pr($sql);                    
                }
                else {
                    $sql = "INSERT INTO
                                INVENTORY_ITEM_SURPLUS_RECORD
                                (ItemID, LocationID, AdminGroupID, FundingSourceID, RecordDate, SurplusQty, PersonInCharge,
                                RecordType, RecordStatus, DateInput, DateModified)
                            VALUES
                                ('$itemID', '$locationID', '$adminGroupID', '$fundingSourceID', '$today', '".abs($qtyChange)."', '".$_SESSION['UserID']."',
                                0, 0, NOW(), NOW())";
                    $result['AddSurplus'] = $this->db_db_query($sql);
// debug_pr($sql);
                }
            }
            else {
                if ($nrSurplusRecordID) {
                    $sql = "DELETE FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE RecordID IN ('".implode("','",(array)$surplusRecordIDAry)."')";
                    $result['DeleteSurplus'] = $this->db_db_query($sql);
// debug_pr($sql);
                }
            }
            
            if ($qtyChange < 0) {       // Item Missing
                if ($nrMissingRecordID) {
                    $sql = "UPDATE
                                INVENTORY_ITEM_MISSING_RECORD
                            SET
                                RecordDate = '$today',
                                MissingQty = '".abs($qtyChange)."',
                                PersonInCharge = '".$_SESSION['UserID']."',
    							RecordType = 0,
    							RecordStatus = 0,
                                DateModified = NOW()
                            WHERE
                                RecordID IN ('".implode("','",(array)$missingRecordIDAry)."')";
                    $result['UpdateMissing'] = $this->db_db_query($sql);
// debug_pr($sql);
                }
                else {
                    $sql = "INSERT INTO
                                INVENTORY_ITEM_MISSING_RECORD
                                (ItemID, LocationID, AdminGroupID, FundingSourceID, RecordDate, MissingQty, PersonInCharge,
                                RecordType, RecordStatus, DateInput, DateModified)
                            VALUES
                                ('$itemID', '$locationID', '$adminGroupID', '$fundingSourceID', '$today', '".abs($qtyChange)."', '".$_SESSION['UserID']."',
                                0, 0, NOW(), NOW())";
                    $result['AddMissing'] = $this->db_db_query($sql);
// debug_pr($sql);
                }
            }
            else {
                if ($nrMissingRecordID) {
                    $sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE RecordID IN ('".implode("','",$missingRecordIDAry)."')";
                    $result['DeleteMissing'] = $this->db_db_query($sql);
// debug_pr($sql);
                }
            }
            
            $sql = "INSERT INTO
                            INVENTORY_ITEM_BULK_LOG
                            (ItemID, RecordDate, Action, QtyChange, FundingSource, PersonInCharge, Remark, LocationID, GroupInCharge, StockCheckQty, 
                             RecordType, RecordStatus, DateInput, DateModified)
                    VALUES
                            ('$itemID', '$today', '". ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION."', '".$qtyChange."', '".$fundingSourceID.
                            "', '".$_SESSION['UserID']."', '".$this->Get_Safe_Sql_Query($remark)."','".$locationID."','".$adminGroupID."','".$stockCheckQty."', 0, 0, NOW(), NOW())";
            $result['AddStatusLog'] = $this->db_db_query($sql);
// debug_pr($sql);
// debug_pr($result);
            if (!in_array(false,$result)) {
                $this->Commit_Trans();
                $return = true;
            }
            else {
                $this->RollBack_Trans();
                $return = false;
            }
            return $return;
            
        }
        
        function getStocktakeDoneByLocation($locationID, $groupInCharge='', $fundingSourceID='', $itemID='', $itemType='')
        {
            global $sys_custom, $intranet_root, $plugin, $Lang, $intranet_session_language;
            
            if ($groupInCharge) {
                $singleItemCondition = " AND b.GroupInCharge='".$groupInCharge."' ";
                $bulkItemCondition = " AND b.GroupInCharge='".$groupInCharge."' ";
                
                $adminGroupIDAry = is_array($groupInCharge) ? $groupInCharge: array($groupInCharge);
            }
            else {
                $singleItemCondition = "";
                $bulkItemCondition = "";
                
                $adminGroupIDAry = $this->getAdminGroupIDAry();
            }
            
            $itemIDCondition = $itemID ? " AND a.ItemID='".$itemID."' " : "";
            $fundingSourceCondition = $fundingSourceID ? " AND b.FundingSourceID='".$fundingSourceID."' " : "";
            
            $priceCondition = "";
            if ($sys_custom['eInventory_PriceCeiling']) {
                $priceCondition = " INNER JOIN INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID AND (b.UnitPrice>=d.PriceCeiling)) ";

                $condition = $locationID ? " AND b.LocationID='".$locationID."' " : "";
                $adminGroupIDStr = implode("','",$adminGroupIDAry);
                $bulkItemCeilingPriceCondition = $this->getBulkItemCeilingPriceCondition($condition, $adminGroupIDStr);

                $success_bulk_item_cond = $bulkItemCeilingPriceCondition['success'];
                $fail_bulk_item_cond = $bulkItemCeilingPriceCondition['fail'];

            }
            else {
                $success_bulk_item_cond = "";
                $fail_bulk_item_cond = "";
            }
            $stocktakePriceSingle = $this->StockTakePriceSingle > 0 ? $this->StockTakePriceSingle : 0;

            $start_date = $this->stocktake_period_start;
            $end_date = $this->stocktake_period_end;
            
            // should include original location and missing for single item
            $singleItemSql = "SELECT
                                        a.ItemID,
                                        a.ItemType,"
                                        .$this->getInventoryNameByLang('a.')." as ItemName,
                                        a.StockTakeOption,
                                        b.UnitPrice,
                                        b.PurchasedPrice,
                                        '' as ExpectedQty,
                                        '' as StocktakeQty,
                                        '' as Variance,
                                        c.DateModified,
                                        e.Action,
                                        b.GroupInCharge as GroupInCharge,
                                        '' as FundingSource
                                FROM
                                        INVENTORY_ITEM a
                                        INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID
                                        INNER JOIN (
                                            SELECT 
                                                ItemID, MAX(DateModified) AS DateModified
                                            FROM 
                                                INVENTORY_ITEM_SINGLE_STATUS_LOG
                                            WHERE 
                                                Action IN (2,4)
                                                AND RecordDate BETWEEN '$start_date' AND '$end_date'
                                                GROUP BY ItemID
                                            ) AS c ON c.ItemID=b.ItemID
                                        INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG e 
                                            ON (e.ItemID=c.ItemID AND e.DateModified=c.DateModified AND e.Action IN (2,4) 
                                                AND e.RecordDate BETWEEN '$start_date' AND '$end_date') 
                                        ".$priceCondition."
                                WHERE
                                        a.ItemType = '".ITEM_TYPE_SINGLE."'
                    					AND a.RecordStatus = 1 
                                        AND (a.StockTakeOption = 'YES' 
                                                OR (a.StockTakeOption = 'FOLLOW_SETTING' 
                                                    AND IF(b.UnitPrice>0,b.UnitPrice,IFNULL(b.PurchasedPrice,0))>=".$stocktakePriceSingle."))   
                                        AND b.LocationID='".$locationID."'".
                                        $singleItemCondition.
                                        $itemIDCondition;

            $bulkItemSql = "SELECT
                                    a.ItemID,
                                    a.ItemType,"
                                    .$this->getInventoryNameByLang('a.')." as ItemName,
                                    a.StockTakeOption,
                                    '' as UnitPrice,
                                    '' as PurchasedPrice,
                                    b.Quantity as ExpectedQty,
                                    e.StockCheckQty as StocktakeQty,
                                    e.StockCheckQty - b.Quantity as Variance,
                                    d.DateModified,
                                    '' as Action,
                                    b.GroupInCharge,
                                    d.FundingSource
                            FROM
                                    INVENTORY_ITEM a
                					INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) 
                					INNER JOIN INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                                    INNER JOIN (
                                        SELECT 
                                            ItemID, 
                                            LocationID, 
                                            GroupInCharge, 
                                            FundingSource, 
                                            MAX(DateModified) AS DateModified
                                        FROM
                                            INVENTORY_ITEM_BULK_LOG
                                        WHERE
                                            Action IN (2)
                                            AND RecordDate BETWEEN '$start_date' AND '$end_date'
                                            GROUP BY ItemID, LocationID, GroupInCharge, FundingSource
                                        ) AS d ON (d.ItemID=b.ItemID 
                                            AND d.LocationID=b.LocationID 
                                            AND d.GroupInCharge=b.GroupInCharge 
                                            AND d.FundingSource=b.FundingSourceID)
                                    INNER JOIN INVENTORY_ITEM_BULK_LOG e ON (
                                                e.ItemID=d.ItemID 
                                            AND e.LocationID=d.LocationID 
                                            AND e.GroupInCharge=d.GroupInCharge 
                                            AND e.FundingSource=d.FundingSource
                                            AND e.DateModified=d.DateModified)
                            WHERE
                                    a.ItemType = '".ITEM_TYPE_BULK."'
                					AND a.RecordStatus = 1 
                                    AND b.LocationID='".$locationID."'".
                                    $bulkItemCondition
                                    .$itemIDCondition
                                    .$fundingSourceCondition
                                    .$success_bulk_item_cond
                                    .$fail_bulk_item_cond;
            
            switch ($itemType) {
                case ITEM_TYPE_SINGLE:
                    $sql = $singleItemSql;
                break;
                
                case ITEM_TYPE_BULK:
                    $sql = $bulkItemSql;
                break;
                
                default:
                    $sql = $singleItemSql .' UNION ' . $bulkItemSql;
                    break;
            }
            
            $sql .= " ORDER BY DateModified DESC";
            
//debug_pr($sql);                            
            $resultAry = $this->returnResultSet($sql);
// debug_pr($success_bulk_item_cond);            
// debug_pr($fail_bulk_item_cond);            
// debug_pr($resultAry);            
            $nrRecord = count($resultAry);
//debug_pr("nrRec=$nrRecord");

            $adminGroupAry = $this->returnAdminGroup();
            $adminGroupAssoc = BuildMultiKeyAssoc($adminGroupAry, 'AdminGroupID','AdminGroupName',1);
            
            $fundingSourceAry = $this->returnFundingSource();
            $fundingSourceAssoc = BuildMultiKeyAssoc($fundingSourceAry, 'FundingSourceID','FundingSourceName',1);
            
            
            $returnAry = array();
            if ($nrRecord) {
                if ($plugin['eBooking']) {
                    include_once ($intranet_root . "/includes/libebooking.php");
                    $itemIDAry = array_unique(Get_Array_By_Key($resultAry, 'ItemID'));
                    $lebooking = new libebooking();
                    $checkInStatusAry = $lebooking->returnIsItemCheckedIn($itemIDAry);
                }
                
                for($i=0;$i<$nrRecord;$i++) {
                    $_itemID = $resultAry[$i]['ItemID'];
                    $resultAry[$i]['RecordStatus'] = $this->getItemStatus($_itemID);
                    if ($plugin['eBooking'] && $checkInStatusAry[$_itemID]) {
                        $resultAry[$i]['RecordStatus'] = $Lang['eInventory']['ItemStatus']['Borrowed'];
                        $resultAry[$i]['ShowRecordStatus'] = true;
                    }
                    else {
                        $resultAry[$i]['ShowRecordStatus'] = false;
                    }
                    
                    if ($resultAry[$i]['ItemType'] == ITEM_TYPE_BULK) {
                        $bulkItemAry = array($resultAry[$i]);
//debug_pr("bulk items");                        
                        $bulkStocktakeAry = $this->FilterForStockTakeBulk($bulkItemAry);
                        
                        if (count($bulkStocktakeAry) > 0) {
                            $groupInChargeID = $resultAry[$i]['GroupInCharge'];
                            $resultAry[$i]['GroupInChargeName'] = $groupInChargeAssoc[$groupInChargeID];    // set GroupInCharge name
                            $fundingSourceID = $resultAry[$i]['FundingSource'];
                            $resultAry[$i]['FundingSourceName'] = $fundingSourceAssoc[$fundingSourceID];    // set FundingSource name
                            $returnAry[] = $resultAry[$i];
                        }
                    }
                    else {
                        $returnAry[] = $resultAry[$i];
                    }
                }
            }
//debug_pr($returnAry);
            return $returnAry;                            
        }
  
/*
        // this function is not used anymore
        function getSingleItemLatestStatus($itemID)
        {
            global $i_InventorySystem_ItemStatus_Normal, $i_InventorySystem_ItemStatus_Damaged, $i_InventorySystem_ItemStatus_Repair;
            $sql = "SELECT 
                        CASE NewStatus
                            WHEN 1 THEN '".$i_InventorySystem_ItemStatus_Normal."'
                            WHEN 2 THEN '".$i_InventorySystem_ItemStatus_Damaged."'
                            WHEN 3 THEN '".$i_InventorySystem_ItemStatus_Repair."'
                            ELSE '".$Lang['eInventory']['eClassApp']['EmptySymbol']."'
                        END AS Status
                    FROM
                        INVENTORY_ITEM_SINGLE_STATUS_LOG 
                    WHERE
                        ItemID='".$itemID."'
                        AND Action IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
                        ORDER BY RecordDate DESC, DateInput DESC LIMIT 0,1";
            $resultAry = $this->returnResultSet($sql);
            return count($resultAry) ? $resultAry[0]['Status'] : '';
        }
*/        
        function getSingleItemLatestStocktakeRemark($itemID)
        {
            $sql = "SELECT 
                        Remark 
                    FROM 
                        INVENTORY_ITEM_SINGLE_STATUS_LOG 
                    WHERE 
                        ItemID='".$itemID."' 
                        AND Action IN (".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION.",".ITEM_ACTION_STOCKTAKE_NOT_FOUND.")
                        ORDER BY DateModified DESC LIMIT 1";
            $resultAry = $this->returnResultSet($sql);
            return count($resultAry) ? $resultAry[0]['Remark'] : '';
        }
        
        // used in stocktake
        function getSingleItemDetail($itemID)
        {
            
            
            $sql = "SELECT 
                            a.ItemID,
                            a.NameChi,
                            a.NameEng,
                            a.DescriptionChi,
                            a.DescriptionEng,
                            a.ItemCode,
                            e.TagCode as Barcode,
                            e.ItemRemark,
                            CONCAT(".$this->getInventoryNameByLang("bd.").", ' > ' , ".$this->getInventoryNameByLang("j.").",' > ',".$this->getInventoryNameByLang("h.").") as FullLocation,
                            p.PhotoPath, 
                            p.PhotoName
                    FROM
                            INVENTORY_ITEM AS a
                            INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON e.ItemID = a.ItemID
                            LEFT OUTER JOIN INVENTORY_LOCATION AS h ON h.LocationID = e.LocationID
                            LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS j ON j.LocationLevelID = h.LocationLevelID 
                            LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS bd ON bd.BuildingID = j.BuildingID
                            LEFT OUTER JOIN INVENTORY_PHOTO_PART AS p ON p.PartID=a.PhotoLink
                    WHERE
                            a.ItemID='".$itemID."'";
            $resultAry = $this->returnResultSet($sql);
            if (count($resultAry)) {
                $resultAry = $resultAry[0];
                $resultAry['Status'] = $this->getItemStatus($itemID);
                $resultAry['StocktakeRemark'] = $this->getSingleItemLatestStocktakeRemark($itemID);
            }
            else {
                $resultAry['Status'] = '';
                $resultAry['StocktakeRemark'] = '';
            }
            return $resultAry;
        }

        // used in stocktake
        function getBulkItemDetail($itemID, $locationID='', $groupInCharge='', $fundingSourceID='')
        {
            $start_date = $this->stocktake_period_start;
            $end_date = $this->stocktake_period_end;
            
            $groupBy = " GROUP BY b.ItemID, b.LocationID";
            
            $locationCondition = $locationID ? " AND b.LocationID='".$locationID."' " : "";
            $groupInChargeCondition = $groupInCharge ? " AND b.GroupInCharge='".$groupInCharge."' " : "";
            $fundingSourceCondition = $fundingSourceID ? " AND b.FundingSourceID='".$fundingSourceID."' " : ""; 
            $sql = "SELECT
                            a.ItemID,
                            a.NameChi,
                            a.NameEng,
                            a.DescriptionChi,
                            a.DescriptionEng,
                            a.ItemCode,
                            e.Barcode,
                            b.Quantity as ExpectedQty,
                            b.LocationID,
                            b.GroupInCharge,
                            b.FundingSourceID,
                            CONCAT(".$this->getInventoryNameByLang("bd.").", ' > ' , ".$this->getInventoryNameByLang("j.").",' > ',".$this->getInventoryNameByLang("h.").") as FullLocation,
                            p.PhotoPath,
                            p.PhotoName
                    FROM
                            INVENTORY_ITEM AS a
                            INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (b.ItemID=a.ItemID)
                            INNER JOIN INVENTORY_ITEM_BULK_EXT AS e ON (e.ItemID = b.ItemID)
                            LEFT OUTER JOIN INVENTORY_LOCATION AS h ON h.LocationID = b.LocationID
                            LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS j ON j.LocationLevelID = h.LocationLevelID
                            LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS bd ON bd.BuildingID = j.BuildingID
                            LEFT OUTER JOIN INVENTORY_PHOTO_PART AS p ON p.PartID=a.PhotoLink
                    WHERE
                            a.ItemID='".$itemID."'" . 
                            $locationCondition.
                            $groupInChargeCondition .
                            $fundingSourceCondition .
                            " ORDER BY b.GroupInCharge, b.FundingSourceID";
            $resultAry = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($resultAry);
            for($i=0,$iMax=count($resultAry);$i<$iMax;$i++) {
                $_resultAry = $resultAry[$i];
                $_locationID = $_resultAry['LocationID'];
                $_groupInCharge = $_resultAry['GroupInCharge'];
                $_fundingSourceID = $_resultAry['FundingSourceID'];
                $_expectedQty = $_resultAry['ExpectedQty'];
                
                $sql = "SELECT
                                StockCheckQty,
                                Remark
                        FROM                            
                                INVENTORY_ITEM_BULK_LOG
                        WHERE 
                                ItemID='".$itemID."'
                                AND LocationID='".$_locationID."'
                                AND GroupInCharge='".$_groupInCharge."'
                                AND FundingSource='".$_fundingSourceID."'
                                AND Action IN (2)
                                AND RecordDate BETWEEN '$start_date' AND '$end_date'
                                ORDER BY DateModified DESC LIMIT 1";
                $stockCheckAry = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($stockCheckAry);
                
                if (count($stockCheckAry)) {
                    $stockCheckQty = $stockCheckAry[0]['StockCheckQty'];
                    $variance = intval($stockCheckQty) - intval($_expectedQty);
                    $resultAry[$i]['StockCheckQty'] = $stockCheckQty;
                    $resultAry[$i]['Variance'] = $variance;
                    $resultAry[$i]['Remark'] = $stockCheckAry[0]['Remark'];
                }                                    
            }
// debug_pr($resultAry);
            return $resultAry;
        }
        
        function getStocktakeNotDoneByLocation($locationID, $groupInCharge='', $fundingSourceID='', $itemID='', $itemType='')
        {
            global $sys_custom, $intranet_root, $Lang, $plugin, $intranet_session_language;
            
            if ($groupInCharge) {
                $singleItemCondition = " AND b.GroupInCharge='".$groupInCharge."' ";
                $bulkItemCondition = " AND b.GroupInCharge='".$groupInCharge."' ";
                
                $adminGroupIDAry = is_array($groupInCharge) ? $groupInCharge: array($groupInCharge);
            }
            else {
                $singleItemCondition = "";
                $bulkItemCondition = "";
                
                $adminGroupIDAry = $this->getAdminGroupIDAry();
            }
            
            $itemIDCondition = $itemID ? " AND a.ItemID='".$itemID."' " : "";
            $fundingSourceCondition = $fundingSourceID ? " AND b.FundingSourceID='".$fundingSourceID."' " : "";
            
            $priceCondition = "";
            if ($sys_custom['eInventory_PriceCeiling']) {
                $priceCondition = " INNER JOIN INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID AND (b.UnitPrice>=d.PriceCeiling)) ";
                
                $condition = $locationID ? " AND b.LocationID='".$locationID."' " : "";
                $adminGroupIDStr = implode("','",$adminGroupIDAry);
                $bulkItemCeilingPriceCondition = $this->getBulkItemCeilingPriceCondition($condition, $adminGroupIDStr);
                
                $success_bulk_item_cond = $bulkItemCeilingPriceCondition['success'];
                $fail_bulk_item_cond = $bulkItemCeilingPriceCondition['fail'];
            }
            else {
                $success_bulk_item_cond = "";
                $fail_bulk_item_cond = "";
            }
            $stocktakePriceSingle = $this->StockTakePriceSingle > 0 ? $this->StockTakePriceSingle : 0;
            
            $start_date = $this->stocktake_period_start;
            $end_date = $this->stocktake_period_end;
            
            // should include original location and missing
            $singleItemSql = "SELECT
                                        a.ItemID,
                                        a.ItemType,"
                                        .$this->getInventoryNameByLang('a.')." as ItemName,
                                        a.NameEng,
                                        a.StockTakeOption,
                                        b.TagCode as Barcode,
                                        b.UnitPrice,
                                        b.PurchasedPrice,
                                        '' as ExpectedQty,
                                        '' as StocktakeQty,
                                        b.GroupInCharge as GroupInCharge,
                                        '' as FundingSourceID
                                FROM
                                        INVENTORY_ITEM a
                                        INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON b.ItemID=a.ItemID
                                        LEFT JOIN (
                                            SELECT
                                                ItemID, MAX(DateModified) AS DateModified
                                            FROM
                                                INVENTORY_ITEM_SINGLE_STATUS_LOG
                                            WHERE
                                                Action IN (2,4)
                                                AND RecordDate BETWEEN '$start_date' AND '$end_date'
                                                GROUP BY ItemID
                                            ) AS c ON c.ItemID=b.ItemID
                                        ".$priceCondition."
                                WHERE
                                        a.ItemType = '".ITEM_TYPE_SINGLE."'
                    					AND a.RecordStatus = 1
                                        AND (a.StockTakeOption = 'YES'
                                                OR (a.StockTakeOption = 'FOLLOW_SETTING'
                                                    AND IF(b.UnitPrice>0,b.UnitPrice,IFNULL(b.PurchasedPrice,0))>=".$stocktakePriceSingle."))
                                        AND c.ItemID IS NULL
                                        AND b.LocationID='".$locationID."'".
                                        $singleItemCondition.
                                        $itemIDCondition;
                                        
            $bulkItemSql = "SELECT
                                    a.ItemID,
                                    a.ItemType,"
                                    .$this->getInventoryNameByLang('a.')." as ItemName,
                                    a.NameEng,
                                    a.StockTakeOption,
                                    c.Barcode,
                                    '' as UnitPrice,
                                    '' as PurchasedPrice,
                                    b.Quantity as ExpectedQty,
                                    IFNULL(d.StockCheckQty,0) as StocktakeQty,
                                    b.GroupInCharge,
                                    b.FundingSourceID
                            FROM
                                    INVENTORY_ITEM a
                                    INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0)
                                    INNER JOIN INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                                    LEFT JOIN INVENTORY_ITEM_BULK_LOG d ON (
                                        d.ItemID=b.ItemID
                                        AND d.LocationID=b.LocationID
                                        AND d.GroupInCharge=b.GroupInCharge
                                        AND d.FundingSource=b.FundingSourceID
                                        AND d.Action=2
                                        AND RecordDate BETWEEN '$start_date' AND '$end_date')
                            WHERE
                                    a.ItemType = '".ITEM_TYPE_BULK."'
                					AND a.RecordStatus = 1
                                    AND d.ItemID IS NULL
                                    AND b.LocationID='".$locationID."'".
                                    $bulkItemCondition
                                    .$itemIDCondition
                                    .$fundingSourceCondition
                                    .$success_bulk_item_cond
                                    .$fail_bulk_item_cond;
                                    
            switch ($itemType) {
                case ITEM_TYPE_SINGLE:
                    $sql = $singleItemSql;
                    break;
                    
                case ITEM_TYPE_BULK:
                    $sql = $bulkItemSql;
                    break;
                    
                default:
                    $sql = $singleItemSql .' UNION ' . $bulkItemSql;
                    break;
            }
                                    
            $sql .= " ORDER BY ItemType, NameEng, Barcode";
                                    
            //debug_pr($sql);exit;
            $resultAry = $this->returnResultSet($sql);
            // debug_pr($success_bulk_item_cond);
            // debug_pr($fail_bulk_item_cond);
            // debug_pr($resultAry);
            $nrRecord = count($resultAry);
            //debug_pr("nrRec=$nrRecord");
            
            $adminGroupAry = $this->returnAdminGroup();
            $adminGroupAssoc = BuildMultiKeyAssoc($adminGroupAry, 'AdminGroupID','AdminGroupName',1);
            
            $fundingSourceAry = $this->returnFundingSource();
            $fundingSourceAssoc = BuildMultiKeyAssoc($fundingSourceAry, 'FundingSourceID','FundingSourceName',1);
            
            $returnAry = array();
            if ($nrRecord) {
                if ($plugin['eBooking']) {
                    include_once ($intranet_root . "/includes/libebooking.php");
                    $itemIDAry = array_unique(Get_Array_By_Key($resultAry, 'ItemID'));
                    $lebooking = new libebooking();
                    $checkInStatusAry = $lebooking->returnIsItemCheckedIn($itemIDAry);
                }
                
                for($i=0;$i<$nrRecord;$i++) {
                    $_itemID = $resultAry[$i]['ItemID'];
                    $resultAry[$i]['RecordStatus'] = $this->getItemStatus($_itemID);
                    if ($plugin['eBooking'] && $checkInStatusAry[$_itemID]) {
                        $resultAry[$i]['RecordStatus'] = $Lang['eInventory']['ItemStatus']['Borrowed'];
                        $resultAry[$i]['ShowRecordStatus'] = true;
                    }
                    else {
                        $resultAry[$i]['ShowRecordStatus'] = false;
                    }
                    
                    if ($resultAry[$i]['ItemType'] == ITEM_TYPE_BULK) {
                        $bulkItemAry = array($resultAry[$i]);
                        //debug_pr("bulk items");
                        $bulkStocktakeAry = $this->FilterForStockTakeBulk($bulkItemAry);
                        
                        if (count($bulkStocktakeAry) > 0) {
                            $groupInChargeID = $resultAry[$i]['GroupInCharge'];
                            $resultAry[$i]['GroupInChargeName'] = $groupInChargeAssoc[$groupInChargeID];    // set GroupInCharge name
                            $fundingSourceID = $resultAry[$i]['FundingSourceID'];
                            $resultAry[$i]['FundingSourceName'] = $fundingSourceAssoc[$fundingSourceID];    // set FundingSource name
                            $returnAry[] = $resultAry[$i];
                        }
                    }
                    else {
                        $returnAry[] = $resultAry[$i];
                    }
                }
            }
            //debug_pr($returnAry);exit;
            return $returnAry;
        }
        
        function getItemStatus($itemID)
        {
            global $i_InventorySystem_ItemStatus_Normal, $i_InventorySystem_ItemStatus_Damaged, $i_InventorySystem_ItemStatus_Repair;
            
            $displayStatus = '';
            $sql = "SELECT 
                        NewStatus 
                    FROM 
                        INVENTORY_ITEM_SINGLE_STATUS_LOG 
                    WHERE 
                        ItemID = '".$itemID."'
                        AND (NewStatus IS NOT NULL AND NewStatus != '' AND NewStatus != '0') 
                        ORDER BY DateInput DESC LIMIT 0,1";
            $statusAry = $this->returnResultSet($sql);
            if (count($statusAry))
            {
                $status = $statusAry[0]['NewStatus'];
                switch($status) {
                    case ITEM_STATUS_NORMAL:
                        $displayStatus = $i_InventorySystem_ItemStatus_Normal;
                        break;
                    case ITEM_STATUS_DAMAGED:
                        $displayStatus = $i_InventorySystem_ItemStatus_Damaged;
                        break;
                    case ITEM_STATUS_REPAIR:
                        $displayStatus = $i_InventorySystem_ItemStatus_Repair;
                        break;
                }
            }
            
            return $displayStatus;
        }
        
        function getFundingSoureInUse($showZero=false)
        {
            $sql = "SELECT
                        DISTINCT (a.FundingSource) AS FundingSourceID
                    FROM
                        INVENTORY_ITEM_SINGLE_EXT a
                        INNER JOIN INVENTORY_ITEM b ON b.ItemID=a.ItemID
                    WHERE 
                        b.RecordStatus=1 
                        UNION
                    SELECT
                        DISTINCT (a.FundingSource2) AS FundingSourceID
                    FROM
                        INVENTORY_ITEM_SINGLE_EXT a
                        INNER JOIN INVENTORY_ITEM b ON b.ItemID=a.ItemID
                            AND (a.FundingSource2 IS NOT NULL AND a.FundingSource2!=0)
                    WHERE 
                        b.RecordStatus=1
                        UNION
                    SELECT 
                        DISTINCT(FundingSourceID) AS FundingSourceID
                    FROM 
                        INVENTORY_ITEM_BULK_LOCATION
                    ";
            if (!$showZero) {
                $sql .= " WHERE Quantity>0";
            }
            $fundingSourceIDAry = $this->returnResultSet($sql);
            $fundingSourceIDAry = Get_Array_By_Key($fundingSourceIDAry,'FundingSourceID');
            
            if (! empty($fundingSourceIDAry)) {
                $fundingSourceIDStr = implode(",", $fundingSourceIDAry);
            } else {
                $fundingSourceIDStr = "-999";
            }
            $sql = "SELECT
				        FundingSourceID,
				    " . $this->getInventoryNameByLang("") . "
		            FROM
				        INVENTORY_FUNDING_SOURCE
            		WHERE
            			FundingSourceID IN (" . $fundingSourceIDStr . ")
            		ORDER BY
            				DisplayOrder";
            $resultAry = $this->returnArray($sql);
            return $resultAry;
        }
        
        function getSubCategoryInUse($categoryID)
        {
            $sql = "SELECT
                        DISTINCT (a.Category2ID) AS SubCategoryID,
					   ".$this->getInventoryNameByLang('b.')." AS SubCategoryName
					FROM
                        INVENTORY_ITEM a
					    INNER JOIN INVENTORY_CATEGORY_LEVEL2 b ON b.Category2ID=a.Category2ID
					WHERE
					    b.CategoryID = '$categoryID'
						ORDER BY b.DisplayOrder";
            $resultAry = $this->returnArray($sql);
            return $resultAry;
        }
        
        function getItemList($offset=0, $numberOfRecord=20, $filterAry=array())
        {
            $cond = '';
            $item_type_condition = '';
            $location_condition1 = '';
            $location_condition2 = '';
            $admin_condition1 = '';
            $admin_condition2 = '';
            
            if ($filterAry['ItemType']) {
                $item_type_condition = " AND a.ItemType =  '".$filterAry['ItemType']."' ";
                $cond .= " AND a.ItemType =  '".$filterAry['ItemType']."'";
            }
            
            if ($filterAry['Category']) {
                $cond .= " AND a.CategoryID =  '".$filterAry['Category']."' ";
            }
            
            if ($filterAry['SubCategory']) {
                $cond .= " AND a.Category2ID =  '".$filterAry['SubCategory']."' ";
            }
            
            $getLocationFilter = false;
            if ($filterAry['BuildingID'] || $filterAry['FloorID'] || $filterAry['RoomID']) {
                $sql = "SELECT
                            c.LocationID
                        FROM
                            INVENTORY_LOCATION_BUILDING AS a INNER JOIN
                            INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
                            INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
                        WHERE 
                            a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                if ($filterAry['BuildingID']) {
                    $sql .= " AND a.BuildingID='".$filterAry['BuildingID']."'";
                }
                if ($filterAry['FloorID']) {
                    $sql .= " AND b.LocationLevelID='".$filterAry['FloorID']."'";
                }
                if ($filterAry['RoomID']) {
                    $sql .= " AND c.LocationID='".$filterAry['RoomID']."'";
                }
                $getLocationFilter = true;
            }
            
            // to consistent with web, check if the item is assigned to a location that doesn't exist if no location related filter is selected
            else if ($filterAry['BuildingID'] == '' && $filterAry['FloorID'] == '' && $filterAry['RoomID'] == '') {
                $sql = "SELECT LocationID FROM INVENTORY_LOCATION WHERE RecordStatus=1 ORDER BY LocationID";
                $getLocationFilter = true;
            }
            
            if ($getLocationFilter) {
                $locationIDAry = $this->returnResultSet($sql);
                $locationIDAry = Get_Array_By_Key($locationIDAry,'LocationID');
                $locationIDStr = implode(",", $locationIDAry);
                if ($locationIDStr != '') {
                    $cond .= " AND (e.LocationID IN ($locationIDStr) OR g.LocationID IN ($locationIDStr)) ";
                    $location_condition1 = " AND e.LocationID IN ($locationIDStr) ";
                    $location_condition2 = " AND g.LocationID IN ($locationIDStr) ";
                }
            }
            
            
            $adminGroupIDAry = $this->getAdminGroupIDAry();
            if (count($adminGroupIDAry)){
                $adminGroupIDStr = implode(',',$adminGroupIDAry);
                $admin_condition1 = " AND e.GroupInCharge IN (" . $adminGroupIDStr. ") ";
                $admin_condition2 = " AND g.GroupInCharge IN (" . $adminGroupIDStr. ") ";
            }
            
            if ($filterAry['GroupID']) {
                $cond .= " AND (e.GroupInCharge IN ('".$filterAry['GroupID']."') OR g.GroupInCharge IN ('".$filterAry['GroupID']."')) ";
            }
            $zero_cond = $filterAry['DisplayZeroQty'] ? "" : "AND g.Quantity>0 ";
            
            if ($filterAry['Tag']) {
                $itemIDAry = $this->getItemIDByTag($filterAry['Tag']);
                if (count($itemIDAry)) {
                    $cond .= " AND a.ItemID IN (".implode(",",(array)$itemIDAry).") ";
                }
            }

            if ($filterAry['FundingSourceID']) {
                $cond .= " AND (e.FundingSource IN (".$filterAry['FundingSourceID'].") OR e.FundingSource2 IN (".$filterAry['FundingSourceID'].") OR g.FundingSourceID IN (".$filterAry['FundingSourceID'].")) ";
            }
            
            $sql = "SELECT
                            a.ItemID,
					        a.ItemCode,
						    ".$this->getInventoryItemNameByLang("a.")." AS ItemName,
					        CONCAT(" . $this->getInventoryNameByLang("b.") . ",' > '," . $this->getInventoryNameByLang("c.") . ") AS Category,
					        CONCAT(" . $this->getInventoryNameByLang("LocBuilding.") . ", ' > ' , " . $this->getInventoryNameByLang("j.") . ",' > '," . $this->getInventoryNameByLang("h.") . ") AS Location,".
					        $this->getInventoryNameByLang("i.") . " AS AdminGroup,
					        IF(" . $this->getInventoryNameByLang("k2.") . ">'', CONCAT(" . $this->getInventoryNameByLang("k.") . ",', '," . $this->getInventoryNameByLang("k2.") . "), " . $this->getInventoryNameByLang("k.") . ") AS FundingSource,
					        IF(ItemType = 2, g.Quantity, ' 1 ') AS Qty,
                            h.LocationID,
                            i.AdminGroupID,
                            k.FundingSourceID,
        					a.ItemType, a.DateInput
					FROM
        					INVENTORY_ITEM AS a INNER JOIN
        					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN
        					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = 1 $item_type_condition) LEFT OUTER JOIN
        					INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID $location_condition1 $admin_condition1) LEFT OUTER JOIN
        					INVENTORY_ITEM_BULK_LOCATION AS g ON (a.ItemID = g.ItemID $location_condition2 $admin_condition2 $zero_cond) LEFT OUTER JOIN
        					INVENTORY_LOCATION AS h ON (e.LocationID = h.LocationID OR g.LocationID = h.LocationID) LEFT OUTER JOIN
        					INVENTORY_LOCATION_LEVEL AS j ON (h.LocationLevelID = j.LocationLevelID) LEFT OUTER JOIN
        					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (j.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
        					INVENTORY_ADMIN_GROUP AS i ON (e.GroupInCharge = i.AdminGroupID OR g.GroupInCharge = i.AdminGroupID) LEFT OUTER JOIN
        					INVENTORY_FUNDING_SOURCE AS k ON (e.FundingSource = k.FundingSourceID OR g.FundingSourceID = k.FundingSourceID)
        					LEFT JOIN INVENTORY_FUNDING_SOURCE AS k2 ON (e.FundingSource2 = k2.FundingSourceID)
        					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
                    WHERE   1
        					$cond
        					ORDER BY a.NameEng";
            $sql .= " LIMIT ".$offset.", ".$numberOfRecord;        // temp restriction
        	$itemListAry = $this->returnResultSet($sql);
        	//debug_pr($sql);
			return $itemListAry; 		
        }
        
        // check if a single item has been stocktake or not
        function isStocktakeDoneForSingleItem($itemID)
        {
            $sql = "SELECT
                        ItemID
                    FROM
                        INVENTORY_ITEM_SINGLE_STATUS_LOG
                    WHERE
                        ItemID='".$itemID."'
                        AND Action IN (2,4)
                        AND RecordDate BETWEEN '".$this->stocktake_period_start."' AND '".$this->stocktake_period_end."'";
            $checkAry = $this->returnResultSet($sql);
            return count($checkAry) ? '1' : '0';
        }
        
        // check if a bulk item has been stocktake or not
        function isStocktakeDoneForBulkItem($itemID, $locationID, $adminGroupID, $fundingSourceID)
        {
            $sql = "SELECT
                        ItemID
                    FROM
                        INVENTORY_ITEM_BULK_LOG
                    WHERE
                        ItemID='".$itemID."'
                        AND Action=2
                        AND LocationID='".$locationID."'
                        AND GroupInCharge='".$adminGroupID."'
                        AND FundingSource='".$fundingSourceID."'
                        AND RecordDate BETWEEN '".$this->stocktake_period_start."' AND '".$this->stocktake_period_end."'";
            $checkAry = $this->returnResultSet($sql);
            return count($checkAry) ? '1' : '0';
        }
        
        function getValidTag()
        {
            $sql = "SELECT 
                    	DISTINCT t.TagID, 
                    	t.TagName 
                    FROM 
                    	TAG t 
                    	INNER JOIN INVENTORY_TAG it ON it.TagID=t.TagID
                    	INNER JOIN INVENTORY_ITEM i ON i.ItemID=it.RecordID
                    WHERE
                    	i.RecordStatus=1
                    	ORDER BY t.TagName";
            $tagAry = $this->returnArray($sql);
            return $tagAry;
        }
        
        function getItemIDByTag($tagID)
        {
            $sql = "SELECT DISTINCT RecordID FROM INVENTORY_TAG WHERE TagID='".$tagID."' ORDER BY RecordID";
            $tagAry = $this->returnResultSet($sql);
            $returnAry = Get_Array_By_Key($tagAry,'RecordID');
            return $returnAry;
        }
        
        function getSingleItemMoreDetail($itemID)
        {
            global $i_InventorySystem_Ownership_School, $i_InventorySystem_Ownership_Government, $i_InventorySystem_Ownership_Donor;
            
            $sql = "SELECT
                            a.ItemID,
                            ".$this->getInventoryNameByLang("a.")." AS ItemName,
                            a.ItemCode,
                            ".$this->getInventoryDescriptionNameByLang("a.")." AS DescriptionName,
                            e.TagCode as Barcode,
                            ".$this->getInventoryNameByLang("b.")." AS CategoryName,
                            ".$this->getInventoryNameByLang("c.")." AS SubCategoryName,
                            CONCAT(".$this->getInventoryNameByLang("bd.").", ' > ' , ".$this->getInventoryNameByLang("j.").",' > ',".$this->getInventoryNameByLang("h.").") as FullLocation,
                            ".$this->getInventoryNameByLang("i.")." AS AdminGroupName,
                            CASE a.OwnerShip
                                WHEN ". ITEM_OWN_BY_SCHOOL . " THEN '$i_InventorySystem_Ownership_School'
                                WHEN " . ITEM_OWN_BY_GOVERNMENT . " THEN '$i_InventorySystem_Ownership_Government' 
                                WHEN " . ITEM_OWN_BY_DONOR . " THEN '$i_InventorySystem_Ownership_Donor'
                                ELSE ' - '
                            END AS OwnerShip,
                            e.Brand,
                            IF(e.WarrantyExpiryDate='0000-00-00', '', e.WarrantyExpiryDate) AS PurchaseDate,
                            p.PhotoPath,
                            p.PhotoName
                    FROM
                            INVENTORY_ITEM AS a
                            LEFT OUTER JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON e.ItemID = a.ItemID
                            LEFT OUTER JOIN INVENTORY_CATEGORY AS b ON b.CategoryID = a.CategoryID
                            LEFT OUTER JOIN INVENTORY_CATEGORY_LEVEL2 AS c ON c.Category2ID = a.Category2ID
                            LEFT OUTER JOIN INVENTORY_LOCATION AS h ON h.LocationID = e.LocationID
                            LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS j ON j.LocationLevelID = h.LocationLevelID
                            LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS bd ON bd.BuildingID = j.BuildingID
                            LEFT OUTER JOIN INVENTORY_ADMIN_GROUP AS i ON i.AdminGroupID = e.GroupInCharge
                            LEFT OUTER JOIN INVENTORY_PHOTO_PART AS p ON p.PartID=a.PhotoLink
                    WHERE
                            a.ItemID='".$itemID."'";
            $resultAry = $this->returnResultSet($sql);
            return count($resultAry) ? $resultAry[0] : '';
        }
        
        function getBulkItemMoreDetail($itemID)
        {
            $sql = "SELECT
                            a.ItemID,
                            ".$this->getInventoryNameByLang("a.")." AS ItemName,
                            a.ItemCode,
                            ".$this->getInventoryDescriptionNameByLang("a.")." AS DescriptionName,
                            e.Barcode,
                            ".$this->getInventoryNameByLang("b.")." AS CategoryName,
                            ".$this->getInventoryNameByLang("c.")." AS SubCategoryName,
                            CASE a.OwnerShip
                                WHEN ". ITEM_OWN_BY_SCHOOL . " THEN '$i_InventorySystem_Ownership_School'
                                WHEN " . ITEM_OWN_BY_GOVERNMENT . " THEN '$i_InventorySystem_Ownership_Government'
                                WHEN " . ITEM_OWN_BY_DONOR . " THEN '$i_InventorySystem_Ownership_Donor'
                                ELSE ' - '
                            END AS OwnerShip,
                            e.Quantity,
                            p.PhotoPath,
                            p.PhotoName
                    FROM
                            INVENTORY_ITEM AS a
                            LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT AS e ON (e.ItemID = a.ItemID)
                            LEFT OUTER JOIN INVENTORY_CATEGORY AS b ON b.CategoryID = a.CategoryID
                            LEFT OUTER JOIN INVENTORY_CATEGORY_LEVEL2 AS c ON c.Category2ID = a.Category2ID
                            LEFT OUTER JOIN INVENTORY_PHOTO_PART AS p ON p.PartID=a.PhotoLink
                    WHERE
                            a.ItemID='".$itemID."'";
            $resultAry = $this->returnResultSet($sql);
            return count($resultAry) ? $resultAry[0] : '';
        }
        
        ## function end for eclass App
        ###############################################################################################

    }       // end class
}       // end !defined
?>