<?php
// Modifying by : 

##### Change Log [Start] #######
#
#   Date    :   2020-09-10  Bill    [2020-0909-0909-16235]
#               modified restructureData(), to support auto fill-in images stored in 2009a folder
#
#	Date	:	2019-12-03	(Philips) [2019-1126-1210-58206]
#				First time defaultDisNumDays as same to defaultNumDays
#
#	Date	:	2019-12-02	Philips [2019-1126-1210-58206]
#				Modified restructureData(), added checking of Module before using defaultDisNumDays
#
#	Date	:	2019-11-29	Philips [2019-1126-1210-58206]
#				Modified restructureData(), replace defaultNumDays by defaultDisNumDays
#
#	Date	:	2017-11-28	Bill	[2017-1123-1427-08236]
#				modified restructureData(), fix: all styles in images removed
#				added removeGIFStyleCallback(), to perform image type checking before removing styles
#
#	Date	:	2017-10-06	Bill	[2017-0929-1716-17235]		[removed]
#				modified restructureData(), prevent auto fill-in not work due to inline style
#
#	Date	:	2017-09-29 Anna
#				modified restructureData() - check $tempArr to restructure recipientID
#				modified sendNotice(), added targetType
#
#	Date	:	2017-08-31 Anna
#				modified restructureData(), check if $tempArr is empty
#
#	Date	:	2017-07-13	Bill	[2017-0627-1533-38235]
#				modified restructureData(), to fixed auto fill-in not work due to extra spaces in image tag
#
#	Date	:	2016-06-06	Kenneth
#				modified restructureData(), set eNotice StartDate and EndDate as Datetime instead of Date only
#
#	Date	:	2015-12-17	Omas
#				Replace session_register() by session_register_intranet() - for php5.4
#
#	Date	:	2015-10-05	Bill	[2015-0925-1135-39207]
#				modified restructureData(), prevent auto fill-in not work due to inline style
#
#	Date	:	2011-09-21 Henry Chow
#				update sendNotice(), restructureData(), getDescriptionFromDB(), getNoticeDetails()
#				add variable "SendReplySlip"
#
#	Date	:	2011-03-28	YatWoon
#				update sendNotifyEmail(), change notification email subject & content
#
#	Date	:	2011-03-24 (Henry Chow)
#				modified getDescriptionFromDB(), return NULL if $this->TemplateID is null
#
#	Date	:	2010-09-24 Henry Chow
#				modified sendNotice(), add parameter $emailNotify to control send email to parent or not
#
###### Change Log [End] ########

/*******************************************************
*
*	Date	:	2010-05-17 (Henry Chow)
*	Detail	:	modified sendNotifyEmail(), send to iMail rather than external email
*
*******************************************************/
include_once("libnotice.php");
class libucc extends libnotice {

       # Notice Record
        var $NoticeID;
        var $NoticeNumber;
        var $TemplateID;
        var $Module;
        var $Subject;
        var $Question;
        var $ReplySlipContent;	# 20090611
        var $SendReplySlip;
        var $DateStart;
        var $DateEnd;
        var $Description;
        var $IssueUserID;
        var $IssueUserName;
        var $RecipientID;
        var $Attachment;
        var $TargetRecipientType;
        var $RecordType; 
        var $CategoryID;
        ####Record Types####
        #1 To Whole School
	 	#2 To Some Levels
	 	#3 To Some Classes
	 	#4 To Some Students
        ####################
	 	var $RecordStatus; 
	 	var $IsModule;
	 	####RecordStatus####
	 	#1. Notice Record (Suspended) 
	 	#2. Notice Record (Distributed) 
	 	#3. Template Record 
	 	####################
	 	var $Variable;
	 	var $IsDateRestructured=0;
                
        # Reply record
        var $answer;
        var $studentID;
        var $signerID;
        var $replyType;
        var $replyStatus;
        var $signedTime;
        var $signerName;
       
		
        function libucc($ID="")
        {
	        $this->libnotice($ID);
	    }
		function setModule($Module)
        {
	        $this->Module = strtoupper($Module);
        }
        function setNoticeNumber($NoticeNumber)
        {
	        $this->NoticeNumber = $NoticeNumber;
        }
        function setTemplateID($TemplateID)
        {
	     	$this->TemplateID = $TemplateID;
        }
        function setSubject($Subject)
        {
	        $this->Subject = $Subject;
        }
        function setDescription($Description)
        {
	        $this->Description = $Description;
        }
        function setVariable($Variable)
        {
	        $this->Variable = $Variable;
        }
        function setDateStart($DateStart)
        {
	        $this->DateStart = $DateStart;
        }
        function setDateEnd($DateEnd)
        {
	        $this->DateEnd = $DateEnd;
        }
        function setIssueUserID($IssueUserID)
        {
	        $this->IssueUserID = $IssueUserID;
        }
        function setIssueUserName($IssueUserName)
        {
	        $this->IssueUserName = $IssueUserName;
        }
        function setTargetRecipientType($TargetRecipientType)
        {
	        $this->TargetRecipientType = $TargetRecipientType;
        }
        function setRecipientID($RecipientID)
        {
	        $this->RecipientID = $RecipientID;
        }
        function setAttachment($Attachment) 
        {
	        $this->Attachment = $Attachment;
        }
        function setRecordType($RecordType)
        {
	        $this->RecordType = $RecordType;
        }
        function setRecordStatus()
        {
	        $this->RecordStatus = DISTRIBUTED_NOTICE_RECORD;
        }
        function setIsModule()
        {
	        $this->IsModule = 1;
        }
       
        function sendNotice($emailNotify=1,$TargetType='')
        {
	        global $file_path,$PATH_WRT_ROOT,$i_Notice_ModuleID;
	     
	        
	        //if(!$this->IsDateRestructured)
	        //{
	        	$this->restructureData();
        	//}
	        include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	        $li = new libfilesystem();
	        
	        # Attachment Folder
			# create a new folder to avoid more than 1 notice using the same folder
//	        session_register("uccAttFolder");
	        $uccAttFolder = session_id().".".time();
			session_register_intranet("uccAttFolder",$uccAttFolder);
	        
			$moduleFolder = $this->Module;
			
			$path = "$file_path/file/notice/$moduleFolder";
			$path2 = "$file_path/file/notice/$moduleFolder/$uccAttFolder";
			
	        for($a=0;$a<sizeof($this->Attachment);$a++)
	        {
		        if($this->Attachment[$a]['error']==0)
		        {
			        $tmppath = $this->Attachment[$a]['tmp_name'];
					$filename = $this->Attachment[$a]['name'];
					if (!is_dir($path))
					{
						$li->folder_new($path);
					}
					if (!is_dir($path2))
					{
						$li->folder_new($path2);
					}
					if (is_dir($path2))
					{
						$li->lfs_copy($tmppath, $path2."/".$filename);
					}
					$AttachmentPath = $uccAttFolder;
				}
			}
			
			if(is_array($this->RecipientID))
			{
				$list = implode(",",$this->RecipientID);
			}
			if($TargetType != ''){
				$TargetTypeField = ",TargetType";
				$TargetTypeValue = ", '$TargetType'";
			}

			$ReplySlipContent = ($this->SendReplySlip) ? $this->ReplySlipContent : "";
			$ReplySlipQuestion = ($this->SendReplySlip) ? $this->Question : "";

			$fieldname = "NoticeNumber ,Title, Module, Description, DateStart, DateEnd, IssueUserID, IssueUserName, RecipientID, Question, ReplySlipContent, Attachment, RecordType, RecordStatus, IsModule, DateInput, DateModified".$TargetTypeField;
			$values = "'".$this->NoticeNumber."',
					  '".$this->Subject."',
					  '".$this->Module."',
					  '".addslashes($this->Description)."',
					  '".$this->DateStart."',
					  '".$this->DateEnd."',
					  '".$this->IssueUserID."',
					  '".$this->IssueUserName."',
					  '".$list."',
					  '".$ReplySlipQuestion."',
					  '".$ReplySlipContent."',
					  '".$AttachmentPath."',
					  '".$this->RecordType."',
					  '".$this->RecordStatus."',
					  '".$this->IsModule."',now(),now()"
					  		.$TargetTypeValue;
					  
			$result = false;				  
			$sql = "INSERT INTO INTRANET_NOTICE ($fieldname) VALUES ($values)";

			$result = $this->db_db_query($sql);
			$this->NoticeID = $this->db_insert_id();
			if($this->NoticeID!='')
			{
				$this->insertNoticeReply();
				if($emailNotify)		
					$this->sendNotifyEmail();
			}
			if($result)
			{
				return $this->NoticeID;
			}
			else
			{
				return -1;	
			}
        }
        
        function restructureData()
        {
	     	global $PATH_WRT_ROOT, $image_path, $intranet_session_language, $sys_custom;
	     	global $LAYOUT_SKIN;
	     	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	     	
	     	# get template from DB by $this->TemplateID
	     	$this->setRecordStatus();
	     	$this->setIsModule();
// 	     	debug_pr($this->Module); // DISCIPLINE
// 	     	die();
	     	
	     	$tempArr = $this->getDescriptionFromDB();
	     	if(!empty($tempArr)){
	     		$this->Description = addslashes($tempArr[0]['Content']);
	     		$this->CategoryID = addslashes($tempArr[0]['CategoryID']);
	     		$this->Subject = addslashes($tempArr[0]['Subject']);
	     		$this->Question = $tempArr[0]['ReplySlip'];
	     		$this->SendReplySlip = $tempArr[0]['SendReplySlip'];
	     	}
	     	
	     	$this->Question = addslashes($this->Question);
	     	$this->ReplySlipContent = addslashes($tempArr[0]['ReplySlipContent']);
	     	$this->ReplySlipContent = addslashes($this->ReplySlipContent);
	     	if($this->Question=="" && $this->ReplySlipContent=="") $this->SendReplySlip = 0;
	     	
	     	$now = time();
		    if($this->DateStart=='')
	     	{
		    	$this->DateStart = date('Y-m-d H:i:s',$now);
	     	}
	     	if(trim($this->DateEnd)=='')
	     	{
	     		if($this->Module == 'DISCIPLINE' && $this->defaultDisNumDays){
     				$defaultEnd = $now + ($this->defaultDisNumDays*24*3600); // replace defaultNumDays by defaultDisNumDays
	     		} else {
	     			$defaultEnd = $now + ($this->defaultNumDays*24*3600); // Other Modules
	     		}
    			$this->DateEnd = date('Y-m-d',$defaultEnd).' 23:59:59';
	     	}
	        
	     	$target = $this->TargetRecipientType;
	     	/*
	     	# error: cannot add $target to every Recipient in the array (which is in string format)
	     	for($a=0;$a<sizeof($this->RecipientID);$a++)
	     	{
		     	$this->RecipientID[$a] = $target.$this->RecipientID[$a];
	     	}
	     	*/
	     	$RecipientID_temp = explode(",",$this->RecipientID[0]);
	     	if(empty($tempArr)){
	     		if(is_array($this->RecipientID)){
	     			$RecipientID_temp = $this->RecipientID;
	     		}else{
	     			$RecipientID_temp = explode(",",strval($this->RecipientID));
	     		}
	     	}
	   		
	     	$RecipientID_temp_ary = array();
	     	for($a=0;$a<sizeof($RecipientID_temp);$a++)
	     	{
		     	$RecipientID_temp_ary[] = $target.$RecipientID_temp[$a];
	     	}
	     	$this->RecipientID = $RecipientID_temp_ary;
// 	     	debug_pr($this->RecipientID );
// 	     	$ldiscipline = new libdisciplinev12();
//	     	$TempVar = $ldiscipline->TemplateVariable($this->CategoryID);
// 	     	$TempVar = $ldiscipline->TemplateVariable();
			
	     	$this->Description = stripslashes(undo_htmlspecialchars($this->Description));
	     	if(is_array($this->Variable))
	     	{
		     	foreach($this->Variable as $Key=>$Value)
				{
					/* 
					 * [2015-0925-1135-39207] Remove unwanted style in auto fill-in images (after ".gif")
					 * 1. Find: .gif{...} style="{...}" >
					 * 2. Return: .gif{...} >
					 */
					$this->Description = preg_replace("/(.gif[^>]+) style=\".*?\"/", "$1", $this->Description);
					/* 
					 * [2017-1123-1427-08236] Apply preg_replace_callback() to perform ".gif" checking before removing any style in images
					 * [2017-0929-1716-17235] Remove unwanted style in auto fill-in images (before ".gif") [Commented - All styles in images (e.g. School Logo) would be removed after preg_replace()]
					 * 1. Find: style="{...}" .gif{...} >
					 * 2. Return: .gif{...} >
					 */
//					$this->Description = preg_replace("/(\<img[^>]+)(style\=\"[^\"]+\" )([^>]+)(>)/", "$1$3$4", $this->Description);
//					$this->Description = preg_replace("/(\<img[^>]+)(style\=\"[^\"]+\")([^>]+)(>)/", "$1$3$4", $this->Description);
					if($sys_custom['eDiscipline']['AutoFillIn_REGX_Handling']) {
						$this->Description = preg_replace_callback("/(\<img[^>]+)(style\=\"[^\"]+\" )([^>]+)(>)/", array($this, 'removeGIFStyleCallback'), $this->Description);
						$this->Description = preg_replace_callback("/(\<img[^>]+)(style\=\"[^\"]+\")([^>]+)(>)/", array($this, 'removeGIFStyleCallback'), $this->Description);
					}
					/* 
					 * [2017-0627-1533-38235] Remove extra spaces in auto fill-in images
					 * 1. Find: .gif{...}  >
					 * 2. Return: .gif{...} >
					 */
					$this->Description = preg_replace("/(.gif[^>]+)(?<=[^\s])([\s]{2,})/", "$1 ", $this->Description);
					
//					$this->Description = str_replace("&lt;IMG src=&quot;".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif&quot;&gt;",$Value,$this->Description)
					$this->Description = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" />",$Value,$this->Description);	# original
					$this->Description = str_ireplace("<IMG src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" alt=\"\" />",$Value,$this->Description);	# new add
					$this->Description = str_ireplace("<IMG src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\">",$Value,$this->Description);				# new add
					$this->Description = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/2007a/discipline/".$Key.".gif\" />",$Value,$this->Description);				# new add
					$this->Description = str_ireplace("<IMG src=\"".$image_path."/2007a/discipline/".$Key.".gif\" alt=\"\" />",$Value,$this->Description);				# new add
					$this->Description = str_ireplace("<IMG src=\"".$image_path."/2007a/discipline/".$Key.".gif\">",$Value,$this->Description);							# new add

                    // [2020-0909-0909-16235]
                    // IP30 : $LAYOUT_SKIN = '2020a'
                    // need to specify auto fill-in images stored in old layout folder '2009a'
                    $this->Description = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/2009a/discipline/".$Key.".gif\" />",$Value,$this->Description);
                    $this->Description = str_ireplace("<IMG src=\"".$image_path."/2009a/discipline/".$Key.".gif\" alt=\"\" />",$Value,$this->Description);
                    $this->Description = str_ireplace("<IMG src=\"".$image_path."/2009a/discipline/".$Key.".gif\">",$Value,$this->Description);
				}
			}
//			$this->Description = undo_htmlspecialchars($this->Description);
			
			$this->ReplySlipContent = stripslashes(undo_htmlspecialchars($this->ReplySlipContent));
	     	if(is_array($this->Variable))
	     	{
		     	foreach($this->Variable as $Key=>$Value)
				{
					// [2015-0925-1135-39207] Remove unwanted Style Tag in Auto Fill-in Image
					//$this->ReplySlipContent = preg_replace("/gif\"[^>]*>/", "gif\">", $this->ReplySlipContent);
					$this->ReplySlipContent = preg_replace("/(.gif[^>]+) style=\".*?\"/", "$1", $this->ReplySlipContent);
					// [2017-0627-1533-38235] Remove extra spaces in Auto Fill-in Image
					$this->ReplySlipContent = preg_replace("/(.gif[^>]+)(?<=[^\s])([\s]{2,})/", "$1 ", $this->ReplySlipContent);
					
					$this->ReplySlipContent = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" />",$Value,$this->ReplySlipContent);	# original
					$this->ReplySlipContent = str_ireplace("<IMG src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" alt=\"\" />",$Value,$this->ReplySlipContent);	# new add
					$this->ReplySlipContent = str_ireplace("<IMG src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\">",$Value,$this->ReplySlipContent);			# new add
					$this->ReplySlipContent = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/2007a/discipline/".$Key.".gif\" />",$Value,$this->ReplySlipContent);			# new add
					$this->ReplySlipContent = str_ireplace("<IMG src=\"".$image_path."/2007a/discipline/".$Key.".gif\" alt=\"\" />",$Value,$this->ReplySlipContent);			# new add
					$this->ReplySlipContent = str_ireplace("<IMG src=\"".$image_path."/2007a/discipline/".$Key.".gif\">",$Value,$this->ReplySlipContent);						# new add

                    // [2020-0909-0909-16235]
                    // IP30 : $LAYOUT_SKIN = '2020a'
                    // need to specify auto fill-in images stored in old layout folder '2009a'
                    $this->ReplySlipContent = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/2009a/discipline/".$Key.".gif\" />",$Value,$this->ReplySlipContent);
                    $this->ReplySlipContent = str_ireplace("<IMG src=\"".$image_path."/2009a/discipline/".$Key.".gif\" alt=\"\" />",$Value,$this->ReplySlipContent);
                    $this->ReplySlipContent = str_ireplace("<IMG src=\"".$image_path."/2009a/discipline/".$Key.".gif\">",$Value,$this->ReplySlipContent);
				}
			}

			$this->IsDateRestructured = 1;
        }
        
        function removeGIFStyleCallback($match) {
        	// [2017-1123-1427-08236] Only remove style for ".gif" images
			if(strpos($match[1], ".gif")!==false || strpos($match[3], ".gif")!==false) {
				return $match[1].$match[3].$match[4];
			}
			else {
				return $match[1].$match[2].$match[3].$match[4];
			}
		}
		
        function getDescriptionFromDB()
        {
        	if($this->TemplateID!="") {
		     	$sql = "select CategoryID, Content, Subject, ReplySlip, ReplySlipContent, SendReplySlip from INTRANET_NOTICE_MODULE_TEMPLATE where TemplateID = ".$this->TemplateID;
		     	$result = $this->returnArray($sql);
        	}
	     	
	     	return $result;
	     	
        }
        
        function insertNoticeReply()
        { 
	        $username_field = getNameFieldWithClassNumberEng("");
	        switch ($this->RecordType)
	        {
		        case 1: # Whole School
				
				    $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
				            SELECT NoticeID,UserID, $username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1";
				            
				    $this->db_db_query($sql);
				
				break;
				case 2: # Some levels only
				
				     # Grab the class name for the levels
				     $list = implode(",",$this->RecipientID);
				     $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassLevelID IN ($list)";
				     $classes = $this->returnVector($sql);
				     $classList = "'".implode("','",$classes)."'";
				     $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
				             SELECT $this->NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName IN ($classList)";
				     
				     $this->db_db_query($sql);
				
				break;
				case 3:    # Some classes only
				
				     # Grab the class name for the classes
				     $list = implode(",",$this->RecipientID);
				     $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN ($list)";
				     $classes = $this->returnVector($sql);
				     $classList = "'".implode("','",$classes)."'";
				     $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
				             SELECT $this->NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName IN ($classList)";
				             
				     $this->db_db_query($sql);
				
				break;
				case 4:  # Some only
				
				     $actual_target_users = implode(",",$this->RecipientID);

				     $actual_target_users = $this->returnTargetUserIDArray($actual_target_users);

				     if (sizeof($actual_target_users)!=0)
				     {
				         $delimiter = "";
				         $values = "";
				         
				         for ($i=0; $i<sizeof($actual_target_users); $i++)
				         {
				              list($uid,$name,$usertype) = $actual_target_users[$i];
				              
				              $values .= "$delimiter ($this->NoticeID,$uid,'$name',0,now(),now())";
				              $delimiter = ",";
				              
				         }
				         $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
				                 VALUES $values";
				     
				         $this->db_db_query($sql);
				         
				     }
				
				break;
			}
	        
        }
        
        function getNoticeDetails()
        {
	        global $intranet_httppath;
	    
	        $this->restructureData();
	    	
	        $returnArr['Subject'] = $this->Subject;
	        $returnArr['Description'] = $this->Description;
	        //$returnArr['url'] = $intranet_httppath."/home/notice/sign.php?NoticeID=".$this->NoticeID;
	        $returnArr['url'] = $intranet_httppath."/home/eService/notice/sign.php?NoticeID=".$this->NoticeID;
	        $returnArr['Question'] = $this->Question;
	        $returnArr['ReplySlipContent'] = $this->ReplySlipContent;
	        $returnArr['SendReplySlip'] = $this->SendReplySlip;
	        
        	return $returnArr;
	    }
	    
	    function sendNotifyEmail()
	    {
		    global $PATH_WRT_ROOT,$intranet_root, $website;
		    
		    include_once($PATH_WRT_ROOT."includes/libwebmail.php");
			include_once($PATH_WRT_ROOT."lang/email.php");
			
			$lwebmail = new libwebmail();
			
		    $sql = "SELECT DISTINCT StudentID FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '".$this->NoticeID."'";
		    $students = $this->returnVector($sql);
		    $student_list = implode(",",$students);
		    $sql = "SELECT DISTINCT b.UserEmail, a.ParentID
		                  FROM INTRANET_PARENTRELATION as a
		                       LEFT OUTER JOIN INTRANET_USER as b ON a.ParentID = b.UserID
		                  WHERE a.StudentID IN ($student_list)";
		    $parent_data = $this->returnArray($sql,2);
		    
		    $parent_emails = array();
		    $parentIDs = array();
		    for($i=0; $i<sizeof($parent_data); $i++) {
		    	$parent_emails[] = $parent_data[$i][0];
		    	$parentIDs[] = $parent_data[$i][1];
		    }
		    //debug_pr($parentIDs);
// 		    $email_subject = enotice_notify_mail_subject($this->Subject);
// 		    $email_subject = stripslashes(intranet_undo_htmlspecialchars($email_subject));
// 		    		    
//     		$email_body = enotice_notify_mail_body($this->DateStart,$this->DateEnd,$this->Subject);
//     		$email_body = stripslashes(intranet_undo_htmlspecialchars($email_body));
		    
    		list($email_subject, $email_body) = $this->returnEmailNotificationData($this->DateStart,$this->DateEnd,$this->Subject);
    		
    		$email_list = $parent_emails;
		    
    		# send to iMail rather than external email
		    //$exmail_success = $lwebmail->SendMail($email_subject,$email_body,$webmaster,$email_list,$cc_address,$bcc_address,$attachment_actual_path,$IsImportant,$email_return_path,$p_reply_email);
		    $exmail_success = $lwebmail->sendModuleMail($parentIDs, $email_subject, $email_body, 1);
		}
		
		function setNoticeParameter($Module="", $NoticeNumber="", $TemplateID="", $Variable="", $IssueUserID="",$IssueUserName="", $TargetRecipientType="",$RecipientID="", $RecordType="")
		{
			$this->setModule($Module);
			$this->setNoticeNumber($NoticeNumber);
			$this->setTemplateID($TemplateID);
			$this->setVariable($Variable);
			$this->setIssueUserID($IssueUserID);
			$this->setIssueUserName($IssueUserName);

			$this->setTargetRecipientType($TargetRecipientType);
			$this->setRecipientID($RecipientID);
			$this->setRecordType($RecordType);
		
			$ReturnArr = array("Module"=>$Module,"NoticeNumber"=>$NoticeNumber, "TemplateID"=>$TemplateID, "Variable"=>$Variable,"IssueUserID"=>$IssueUserID,"IssueUserName"=>$IssueUserName,"TargetRecipientType"=>$TargetRecipientType,"RecipientID"=>$RecipientID,"RecordType"=>$RecordType);
			return $ReturnArr;
		}
}
?>