<?php
// Modifying by : 

### Change Log [Start] ###
/*
 *  Date:   2019-08-01  (Bill)  [2019-0723-1334-33206]
 *          modified saveOfficialPhoto(), useOfficialPhotoAsPersonalPhoto(), to handle official photo & personal photo linkage when user update official photo
 */
### Change Log [End] ###

class libuserphoto extends libdb 
{
    function libuserphoto() {
		$this->libdb();
	}
	
	function savePersonalPhoto($parUserId, $parImageBinary) {
	    global $PATH_WRT_ROOT, $intranet_root;
	    
	    include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
	    include_once($PATH_WRT_ROOT.'includes/libuser.php');
	    $lf = new libfilesystem();
	    $luser = new libuser();
	    
	    $successAry = array();
	    
	    $re_path = "/file/photo/personal";
	    $personalPhotoPath = $intranet_root.$re_path;
	    
	    // prevent cannot update official photo url in db
	    if (!is_dir($personalPhotoPath)) {
	        $lf->folder_new($personalPhotoPath);
	    }
	    
	    $ext = '.jpg';
	    $targetPersonalPhotoPath = $personalPhotoPath ."/p". $parUserId. $ext;
	    $targetPersonalPhotoRelPath = $re_path."/p". $parUserId. $ext;
	    $successAry['savePersonal'] = $lf->file_write($parImageBinary, $targetPersonalPhotoPath);
	    
	    if ($successAry['savePersonal']) {
	        $sql = "UPDATE INTRANET_USER SET PersonalPhotoLink = '".$targetPersonalPhotoRelPath."' WHERE UserID = '".$parUserId."'";
	        $successAry['savePersonalPathInDb'] = $this->db_db_query($sql);
	        
	        # check need to resize or not
	        include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
	        $im = new SimpleImage();
	        if($im->GDLib_ava)
	        {
	            $im->load($targetPersonalPhotoPath);
	            $w = $im->getWidth();
	            $h = $im->getHeight();
	            $default_photo_width = $default_photo_width ? $default_photo_width : 100;
	            $default_photo_height = $default_photo_height ? $default_photo_height : 130;
	            if($w > $default_photo_width || $h > $default_photo_height)
	            {
	                $im->resize($default_photo_width,$default_photo_height);
	                $im->save($targetPersonalPhotoPath);
	            }
	        }
	        
	        $luser->synUserDataToModules($parUserId);
	    }
	    
	    return !in_array(false, $successAry)? true : false;
	}
	
	function saveOfficialPhoto($parUserId, $parImageBinary) {
	    global $PATH_WRT_ROOT, $intranet_root;
	    
	    include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
	    include_once($PATH_WRT_ROOT.'includes/libuser.php');
	    $lf = new libfilesystem();
	    $luser = new libuser();
	    
	    $successAry = array();
	    
	    $re_path = "/file/user_photo";
	    $officialPhotoPath = $intranet_root.$re_path;
	    
	    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '".$parUserId."'";
	    $userAry = $this->returnResultSet($sql);
	    $targetUserLogin = $userAry[0]['UserLogin'];
	    
	    if (!is_dir($officialPhotoPath)) {
	        $lf->folder_new($officialPhotoPath);
	    }
	    
	    $ext = '.jpg';
	    $targetOfficialPhotoPath = $officialPhotoPath ."/". $targetUserLogin. $ext;
	    $targetOfficialPhotoRelPath = $re_path ."/". $targetUserLogin. $ext;
	    $successAry['saveOfficial'] = $lf->file_write($parImageBinary, $targetOfficialPhotoPath);
	    
	    if ($successAry['saveOfficial']) {
	        $sql = "UPDATE INTRANET_USER SET PhotoLink = '".$targetOfficialPhotoRelPath."' WHERE UserID = '".$parUserId."'";
	        $successAry['savePersonalPathInDb'] = $this->db_db_query($sql);
	        
	        $luser->synUserDataToModules($parUserId);
	        
	        # check need to resize or not
	        include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
	        $im = new SimpleImage();
	        if($im->GDLib_ava)
	        {
	            $im->load($targetOfficialPhotoPath);
	            $w = $im->getWidth();
	            $h = $im->getHeight();
	            $default_photo_width = $default_photo_width ? $default_photo_width : 100;
	            $default_photo_height = $default_photo_height ? $default_photo_height : 130;
	            if($w > $default_photo_width || $h > $default_photo_height)
	            {
	                $im->resize($default_photo_width,$default_photo_height);
	                $im->save($targetOfficialPhotoPath);
	            }
	        }

	        // [2019-0723-1334-33206] update personal photo linkage
	        if($this->isUserPersonalPhotoLinkedToOfficialPhoto($parUserId)) {
	             $this->useOfficialPhotoAsPersonalPhoto($parUserId, true);
            }
	    }
	    
	    return !in_array(false, $successAry)? true : false;
	}
	
	function useOfficialPhotoAsPersonalPhoto($parUserId, $skipPersonalPhotoBackup=false) {
	    global $PATH_WRT_ROOT, $intranet_root;
	    
	    include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
	    include_once($PATH_WRT_ROOT.'includes/libuser.php');
	    $lf = new libfilesystem();
	    $luser = new libuser();
	    
	    $successAry = array();
	    
	    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '".$parUserId."'";
	    $userAry = $this->returnResultSet($sql);
	    $targetUserLogin = $userAry[0]['UserLogin'];
	    
	    $personalPhotoRelPath = '/file/photo/personal/p'. $parUserId. '.jpg';
	    $personalPhotoPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg';
	    $personalPhotoBakPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg.appBak';
	    $officialPhotoPath = $intranet_root.'/file/user_photo/'. $targetUserLogin. '.jpg';

        // [2019-0723-1334-33206] skip create backup for personal photo
	    if($skipPersonalPhotoBackup) {
	        // do nothing
        }
	    else if (file_exists($personalPhotoPath)) {
	        $successAry['copyPersonalPhotoForBak'] = $lf->lfs_copy($personalPhotoPath, $personalPhotoBakPath);
	    }
	    else {
	        $successAry['createTmpBakFile'] = $lf->file_write('TMP', $personalPhotoBakPath);
	    }
	    $successAry['copyPersonalPhotoAsOfficialPhoto'] = $lf->lfs_copy($officialPhotoPath, $personalPhotoPath);
	    
	    if ($successAry['copyPersonalPhotoAsOfficialPhoto']) {
	        $sql = "UPDATE INTRANET_USER SET PersonalPhotoLink = '".$personalPhotoRelPath."' WHERE UserID = '".$parUserId."'";
	        $successAry['savePersonalPathInDb'] = $this->db_db_query($sql);
	        
	        $luser->synUserDataToModules($parUserId);
	    }
	    
	    return !in_array(false, $successAry)? true : false;
	}
	
	function cancelOfficialPhotoAsPersonalPhoto($parUserId) {
	    global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;
	    
	    include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
	    include_once($PATH_WRT_ROOT.'includes/libuser.php');
	    $lf = new libfilesystem();
	    $luser = new libuser();
	    
	    $successAry = array();
	    
	    $personalPhotoBakPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg.appBak';
	    $personalPhotoPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg';
	    
	    if ($lf->file_read($personalPhotoBakPath) == 'TMP') {
	        // originally has no Personal photo
	        $successAry['removePersonalPhoto'] = $lf->file_remove($personalPhotoPath);
	        
	        $sql = "UPDATE INTRANET_USER SET PersonalPhotoLink = '' WHERE UserID = '".$parUserId."'";
	        $successAry['savePersonalPathInDb'] = $this->db_db_query($sql);
	        
	        $luser->synUserDataToModules($parUserId);
	    }
	    else {
	        // origianlly has a Personal photo
	        $successAry['copyBakPhotoToPersonalPhoto'] = $lf->lfs_copy($personalPhotoBakPath, $personalPhotoPath);
	    }
	    
	    if ($successAry['copyBakPhotoToPersonalPhoto'] || $successAry['removePersonalPhoto']) {
	        $successAry['removePersonalBakPhoto'] = $this->deletePersonalPhotoTempFile($parUserId);
	    }
	    
	    return !in_array(false, $successAry)? true : false;
	}
	
	function deletePersonalPhotoTempFile($parUserId) {
	    global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;
	    
	    include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
	    $lf = new libfilesystem();
	    
	    $personalPhotoBakPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg.appBak';
	    
	    return $lf->file_remove($personalPhotoBakPath);
	}
	
	function isUserPersonalPhotoLinkedToOfficialPhoto($parUserId) {
	    global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;
	    
	    $personalPhotoBakPath = $intranet_root.'/file/photo/personal/p'. $parUserId. '.jpg.appBak';
	    
	    return (file_exists($personalPhotoBakPath))? true : false;
	}
}
?>