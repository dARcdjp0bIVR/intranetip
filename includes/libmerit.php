<?php
# using: 

##### Change Log [Start] #####
#
#   Date    :   2020-04-22 Tommy
#               modified displayStudentRecordByYear(), show student in class year only
#
#	Date	:	2013-10-23 Ivan [2013-1009-1833-14073]
#				modified returnDetailedRecordByYear(), returnStudentRecordByYear() to solve the TermID problem
#
#	Date	:	2010-10-06 Henry chow
#				modified returnStudentRecordByYear() and add retrieveStudentMeritRecordByDate()
#				if client site does not have "eDiscipline" module, then iAccount retrieves merit/demerit records from "Student Profile" directly (without any conversion)
#
#	Date	:	2010-10-06 Henry chow
#				modified displayStudentRecordByYear(), display "0" rather than "There is no record at this moment." if no AP record in selected academic year
#
###### Change Log [End] ######

### Note: If you update this file (libmerit.php) to JUNIOR site, please make sure you have already copied the function displayStudentRecordByYear_jr() from the JUNIOR site first. ###

if (!defined("LIBMERIT_DEFINED"))         // Preprocessor directives
{

 define("LIBMERIT_DEFINED",true);

 class libmerit extends libclass{
       var $StudentMeritID;
       var $UserID;
       var $Year;
       var $Semester;
       var $MeritDate;
       var $RecordType;
       var $Number;
       var $Reason;
       var $Remark;
       var $PersonInCharge;
       var $type_array;
       var $type_archive_array;
       var $YearDescending = true;   # Eric Yip : Boolean controling sorting order of year

       function libmerit($StudentMeritID="")
       {
                $this->libclass();
                if ($StudentMeritID != "")
                {
                    $this->StudentMeritID = $StudentMeritID;
                    $this->retrieveRecord($this->StudentMeritID);
                }
                $this->type_array = array(1,2,3,4,5,-1,-2,-3,-4,-5);
                $this->type_archive_array = array ("Merit", "Minor Credit", "Major Credit", "Super Credit", "Ultra Credit", "Black Mark", "Minor Demerit", "Major Demerit", "Super Demerit", "Ultra Demerit");

       }
       function retrieveRecord($id)
       {
                $fields = "UserID, Year, DATE_FORMAT(MeritDate,'%Y-%m-%d'), RecordType, NumberOfUnit, Reason, Remark,Semester,PersonInCharge";
                $conds = "StudentMeritID = $id";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_MERIT WHERE $conds";
                $result = $this->returnArray($sql,9);
                list(
                $this->UserID, $this->Year,  $this->MeritDate,$this->RecordType,
                $this->Number, $this->Reason, $this->Remark,$this->Semester,
                $this->PersonInCharge) = $result[0];
                return $result;
       }
       function returnDateConditions($start, $end)
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
	                $start .= " 00:00:00";
	                $end .= " 23:59:59";
                    $date_conds = "AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                return $date_conds;
       }
       function returnReasonConditions($reason,$prefix="")
       {
                return ($reason==""? "" : " AND ".$prefix."Reason = '$reason'");
       }
       function returnStudentRecord($uid, $start, $end)
       {
                $fields = "DATE_FORMAT(MeritDate,'%Y-%m-%d'), RecordType,NumberOfUnit, Reason";
                $conds = "UserID = $uid AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                $order = "MeritDate DESC";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_MERIT WHERE $conds ORDER BY $order";
                return $this->returnArray($sql,4);
       }
       function getMeritCountByClassName ($class, $start, $end)
       {
                $students = $this->getClassStudentList($class);
                if (sizeof($students)!=0)
                {
                    $student_list = implode(",",$students);
                    $date_conds = " AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";

                    $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                            WHERE UserID IN ($student_list) $date_conds AND RecordType > 0";
                    $merit = $this->returnVector($sql);
                    $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                            WHERE UserID IN ($student_list) $date_conds AND RecordType < 0";
                    $demerit = $this->returnVector($sql);
                    $entry = array($merit[0]+0,$demerit[0]+0);
                    return $entry;
                }
                else
                {
                    $entry = array(0,0);
                    return $entry;
                }
       }
       function getClassMeritList ($start, $end, $reason="")
       {
                $dateConds = $this->returnDateConditions($start, $end);
                $reasonConds = $this->returnReasonConditions($reason); 
                
                $counts = array();
                $result = array();

                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $type = $this->type_array[$i];
                     
                     $sql = "SELECT c.YearClassID, b.ClassName, SUM(a.NumberOfUnit), c.YearID
                             FROM 
                             PROFILE_STUDENT_MERIT as a 
                             LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN
                             WHERE b.RecordStatus IN (0,1,2) AND a.RecordType = $type $dateConds $reasonConds
                             GROUP BY c.YearClassID
                             ";
                             
                     $result = $this->returnArray($sql,4);
                     for ($j=0; $j<sizeof($result); $j++)
                     {
                          list ($ClassID,$ClassName,$MeritCount,$LvlID) = $result[$j];
                          $counts[$i][$ClassID] = array($ClassName,$MeritCount,$LvlID);
                     }
                }

                $classList = $this->getClassList();
                //$returnResult = array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $returnResult[$i][0] = $ClassID;
                     $returnResult[$i][1] = $ClassName;
                     $returnResult[$i][2] = $LvlID;
                     for ($j=0; $j<sizeof($this->type_array); $j++)
                     {
                          $MeritCount = $counts[$j][$ClassID][1]+0;
                          $returnResult[$i][3+$j] = $MeritCount;
                     }
                }
                return $returnResult;
       }
       function getDetailedMeritListByClass($classname, $start="", $end="",$reason="")
       {
                $dateConds = $this->returnDateConditions($start, $end);
                $reasonConds = $this->returnReasonConditions($reason);
                $name_field = getNameFieldByLang("b.");
                $counts = array();
                $result = array();
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $type = $this->type_array[$i];
                     $sql = "SELECT a.UserID, $name_field, b.ClassNumber, SUM(a.NumberOfUnit)
                             FROM PROFILE_STUDENT_MERIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             WHERE b.RecordStatus IN (0,1,2) AND b.ClassName = '$classname' AND a.RecordType = $type $dateConds $reasonConds
                             GROUP BY a.UserID";
                     $result = $this->returnArray($sql,4);
                     for ($j=0; $j<sizeof($result); $j++)
                     {
                          list ($StudentID,$StudentName,$classNumber,$MeritCount) = $result[$j];
                          $counts[$i][$StudentID] = array($StudentName,$classNumber,$MeritCount);
                     }
                }
                $studentList = $this->getStudentNameListByClassName($classname, "1");
                //$returnResult = array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $classNumber) = $studentList[$i];
                     $returnResult[$i][0] = $StudentID;
                     $returnResult[$i][1] = $StudentName;
                     $returnResult[$i][2] = $classNumber;
                     for ($j=0; $j<sizeof($this->type_array); $j++)
                     {
                          $MeritCount = $counts[$j][$StudentID][2]+0;
                          $returnResult[$i][3+$j] = $MeritCount;
                     }
                }
                return $returnResult;
       }
       function getMeritCountByStudent($studentid,$start="",$end="")
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
                    $date_conds = "AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType > 0";
                $merit = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType < 0";
                $demerit = $this->returnVector($sql);
                $entry = array($merit[0]+0,$demerit[0]+0);
                return $entry;
       }
       function getDetailedMeritCountByStudent($studentid,$start="",$end="")
       {
                if ($start == "" || $end == "")
                {
                    $date_conds = "";
                }
                else
                {
                    $date_conds = "AND UNIX_TIMESTAMP(MeritDate) BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')";
                }
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 1";
                $merit = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 2";
                $minorC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 3";
                $majorC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 4";
                $superC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = 5";
                $ultraC = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -1";
                $black = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -2";
                $minorD = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -3";
                $majorD = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -4";
                $superD = $this->returnVector($sql);
                $sql = "SELECT SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                        WHERE UserID=$studentid $date_conds AND RecordType = -5";
                $ultraD = $this->returnVector($sql);
                $entry = array(
                $merit[0]+0,
                $minorC[0]+0,
                $majorC[0]+0,
                $superC[0]+0,
                $ultraC[0]+0,
                $black[0]+0,
                $minorD[0]+0,
                $majorD[0]+0,
                $superD[0]+0,
                $ultraD[0]+0
                );
                return $entry;
       }
       function getMeritListByClass($classid,$start,$end)
       {
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $temp = $this->getMeritCountByStudent($id,$start,$end);
                     $result[] = array($id,$name,$classnumber,$temp[0],$temp[1]);
                }
                return $result;
       }
       # Use the method in libstudentprofile.php
       function getSelectMeritType ($tags,$selected="")
       {
                global $i_Merit_TypeArray;
                $type = array(1,2,3,4,5,-1,-2,-3,-4,-5);
                $x = "<SELECT $tags>\n";
                for ($i=0; $i<sizeof($type); $i++)
                {
                     $value = $type[$i];
                     $name = $i_Merit_TypeArray[$i];
                     $selected_str = ($value==$selected? "SELECTED":"");
                     $x .= "<OPTION value=$value $selected_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
#                $fields = "MeritDate, RecordType,NumberOfUnit, Reason";
       function displayStudentRecord($studentid,$start,$end)
       {
                global $i_Merit_Date,$i_Merit_Type,$i_Merit_Qty,$i_Merit_Reason;
                global $i_no_record_exists_msg;
                global $i_Merit_Merit,$i_Merit_MinorCredit,$i_Merit_MajorCredit,
                       $i_Merit_SuperCredit,$i_Merit_UltraCredit,$i_Merit_BlackMark,
                       $i_Merit_MinorDemerit,$i_Merit_MajorDemerit,$i_Merit_SuperDemerit,
                       $i_Merit_UltraDemerit, $i_general_record_date;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnStudentRecord($studentid,$start,$end);
              $x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>
        <tr>
          <td align='left' class='tablebluetop tabletopnolink'>$i_general_record_date</td>
          <td align='left' class='tablebluetop tabletopnolink'>$i_Merit_Type</td>
          <td align='left' class='tablebluetop tabletopnolink'>$i_Merit_Qty</td>
          <td align='left' class='tablebluetop tabletopnolink'>$i_Merit_Reason</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($meritDate,$type,$qty,$reason) = $result[$i];
                   if (is_null($qty) || $qty == 0)
                   {
                           $qty = "--";
                   }
                   switch ($type)
                   {
                           case 1: $typeStr = $i_Merit_Merit; break;
                           case 2: $typeStr = $i_Merit_MinorCredit; break;
                           case 3: $typeStr = $i_Merit_MajorCredit; break;
                           case 4: $typeStr = $i_Merit_SuperCredit; break;
                           case 5: $typeStr = $i_Merit_UltraCredit; break;
                           case -1: $typeStr = $i_Merit_BlackMark; break;
                           case -2: $typeStr = $i_Merit_MinorDemerit; break;
                           case -3: $typeStr = $i_Merit_MajorDemerit; break;
                           case -4: $typeStr = $i_Merit_SuperDemerit; break;
                           case -5: $typeStr = $i_Merit_UltraDemerit; break;
                           default: $typeStr = "Error"; break;
                   }
                   $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
          <td align='left' class='tabletext'>$meritDate</td>
          <td align='left' class='tabletext'>$typeStr</td>
          <td align='left' class='tabletext'>$qty</td>
          <td align='left' class='tabletext'>&nbsp;$reason</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr class='tablebluerow2'><td colspan='4' class='tabletext'>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;

       }

       function returnYears($studentid="")
       {
                if ($studentid != "")
                {
                    $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_MERIT WHERE UserID = '$studentid' ORDER BY Year DESC";
                }
                else
                {
                    $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_MERIT ORDER BY Year DESC";
                }
                return $this->returnVector($sql);
       }
       function returnPICs()
       {
                $namefield = getNameFieldWithLoginByLang("b.");
                $sql = "SELECT DISTINCT b.UserID, $namefield FROM PROFILE_STUDENT_MERIT as a
                               LEFT OUTER JOIN INTRANET_USER as b ON a.PersonInCharge = b.UserID
                        WHERE b.UserID IS NOT NULL
                               ORDER BY b.EnglishName, b.UserLogin";
                return $this->returnArray($sql,2);
       }

       function returnStudentRecordByYear($studentid,$year,$sem="")
       {
	       global $PATH_WRT_ROOT, $plugin;
	       
                # Grab information in DB
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     /*$sql = "SELECT Year, SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT
                             WHERE UserID = $studentid AND RecordType = ".$this->type_array[$i]." $conds
                             GROUP BY Year";*/
                     $sql = "SELECT AY.AcademicYearID as Year, SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT PSM
                     			LEFT OUTER JOIN ACADEMIC_YEAR AY ON (AY.YearNameEN=PSM.Year OR AY.YearNameB5=PSM.Year)
                             WHERE UserID = $studentid AND RecordType = ".$this->type_array[$i]." $conds
                             GROUP BY Year";
                     $data[] = $this->returnArray($sql,2);
                }
		
                # Build the whole array
				if($year==""){
					## $all_disintct_years, refer to /admin/academic/studentview.php
  	                global $all_distinct_years;
	                if(isset($all_distinct_years) && sizeof($all_distinct_years)>0)
	                	$years = $all_distinct_years;
	                else 
	                    $years = $this->returnYears($studentid);
				}
				else $years = array($year);

		        if(is_array($years) && $this->YearDescending)
					rsort($years);    # Eric Yip : sort years

				//include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
				include_once("libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
	
				$tempYear = array();
				foreach($years as $val) {
					$yearID = $ldiscipline->getAcademicYearIDByYearName($val);
					if(!in_array($yearID, $tempYear) && $yearID != '') $tempYear[] = $yearID;
				}
				$years = $tempYear;
               	//$years = ($year==""? $this->returnYears($studentid):array($year));
				// $years[0] = 3;
                $result = array();
                for ($i=0; $i<sizeof($years); $i++)
                {
                     $result[$i][0] = $ldiscipline->getAcademicYearNameByYearID($years[$i]);
                     //$result[$i][0] = getAcademicYearByAcademicYearID($years[$i], "en");
                    
                    // 2013-1009-1833-14073
                    if ($sem == '') {
	                	$_semId = '';
	                }
	                else {
	                	include_once('form_class_manage.php');
	                	$libTerm = new academic_year_term();
	                	$_semId = $libTerm->Get_YearTermID_By_Name($years[$i], $sem);
	                }
                     
					# merit / demerit after conversion #                          
//					$yearStartDate = getStartDateOfAcademicYear($years[$i], $sem);
//					$yearEndDate = getEndDateOfAcademicYear($years[$i], $sem);
					$yearStartDate = getStartDateOfAcademicYear($years[$i], $_semId);
					$yearEndDate = getEndDateOfAcademicYear($years[$i], $_semId);
					
					//$plugin['Disciplinev12'] = false;
					if($plugin['Disciplinev12']) {
						$temp[$years[$i]] = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($studentid, $yearStartDate, $yearEndDate);
					} else {
						$temp[$years[$i]] = $this->retrieveStudentMeritRecordByDate($studentid, $yearStartDate, $yearEndDate);
	                }
                }
                /*
                debug_pr($years);
                debug_pr($temp);
                debug_pr($this->type_array);
                debug_pr($result);
                */
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     //$result[$i] = array();
                     for ($j=0; $j<sizeof($years); $j++)
                     {
	                     /************** merit / demerit before conversion *
                          $pos = -1;
                          for ($k=0; $k<sizeof($data[$i]); $k++)
                          {
                               if ($data[$i][$k][0] == $years[$j])
                               {
                                   $pos = $k;
                                   break;
                               }
                          }
                          if ($pos == -1)
                          {
                              $count = 0;
                          }
                          else
                          {
                              $count = $data[$i][$k][1];
                          }
                          $result[$j][$i+1] = $count;
                          #$result[$i+1][$j] = $count;
                          ***************************/
                          
						# merit / demerit after conversion # 
						$result[$j][$i+1] = ($temp[$years[$j]][$this->type_array[$i]] != '') ? $temp[$years[$j]][$this->type_array[$i]] : 0;
						
                     }
                }

                #########################################################
                # Discipline System upgrade rule
                global $plugin;
                global $lstudentprofile;
                if ($plugin['Discipline'])
                {
                    # Grab upgrade rule
                    $sql = "SELECT UpgradeFromType, UpgradeNum, MeritType
                                   FROM DISCIPLINE_MERIT_TYPE_SETTING
                                   WHERE MeritType IS NOT NULL AND MeritType != 0
                                         AND UpgradeFromType > 0
                                         AND UpgradeNum > 0
                                   ORDER BY UpgradeFromType ASC";
                    $temp = $this->returnArray($sql,3);
                    for ($i=0; $i<sizeof($temp); $i++)
                    {
                         list($t_fromtype, $t_num, $t_type) = $temp[$i];
                         $merit_array[$t_fromtype] = array($t_num, $t_type);
                    }
                    $sql = "SELECT UpgradeFromType, UpgradeNum, MeritType
                                   FROM DISCIPLINE_MERIT_TYPE_SETTING
                                   WHERE MeritType IS NOT NULL AND MeritType != 0
                                         AND UpgradeFromType < 0
                                         AND UpgradeNum > 0
                                   ORDER BY UpgradeFromType DESC";
                    $temp = $this->returnArray($sql,3);
                    for ($i=0; $i<sizeof($temp); $i++)
                    {
                         list($t_fromtype, $t_num, $t_type) = $temp[$i];
                         $demerit_array[$t_fromtype] = array($t_num, $t_type);
                    }

                    # Handle data
                    
                    for ($i=0; $i<sizeof($result); $i++)
                    {
                         list($t_year, $t_m_pt, $t_m_min, $t_m_maj, $t_m_sup, $t_m_ult,
                              $t_d_pt, $t_d_min, $t_d_maj, $t_d_sup, $t_d_ult) = $result[$i];
                         ##################################
                         # Merit part
                         if ($t_m_pt != 0)
                         {
                             $temp = $merit_array[1];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_m_pt >= $r_num)
                                 {
                                        $t_m_pt -= $r_num;
                                        if ($r_type==2 && !$lstudentprofile->is_min_merit_disabled)
                                        {
                                            $t_m_min++;
                                        }
                                        else if ($r_type==3 && !$lstudentprofile->is_maj_merit_disabled)
                                        {
                                             $t_m_maj++;
                                        }
                                        else if ($r_type==4 && !$lstudentprofile->is_sup_merit_disabled)
                                        {
                                             $t_m_sup++;
                                        }
                                        else if ($r_type==5 && !$lstudentprofile->is_ult_merit_disabled)
                                        {
                                             $t_m_ult++;
                                        }
                                        else # Add back if no upgrade
                                        {
                                            $t_m_pt += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         if ($t_m_min != 0)
                         {
                             $temp = $merit_array[2];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_m_min >= $r_num)
                                 {
                                        $t_m_min -= $r_num;
                                        if ($r_type==3 && !$lstudentprofile->is_maj_merit_disabled)
                                        {
                                             $t_m_maj++;
                                        }
                                        else if ($r_type==4 && !$lstudentprofile->is_sup_merit_disabled)
                                        {
                                             $t_m_sup++;
                                        }
                                        else if ($r_type==5 && !$lstudentprofile->is_ult_merit_disabled)
                                        {
                                             $t_m_ult++;
                                        }
                                        else
                                        {
                                            $t_m_min += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         if ($t_m_maj != 0)
                         {
                             $temp = $merit_array[3];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_m_maj >= $r_num)
                                 {
                                        $t_m_maj -= $r_num;
                                        if ($r_type==4 && !$lstudentprofile->is_sup_merit_disabled)
                                        {
                                             $t_m_sup++;
                                        }
                                        else if ($r_type==5 && !$lstudentprofile->is_ult_merit_disabled)
                                        {
                                             $t_m_ult++;
                                        }
                                        else
                                        {
                                            $t_m_maj += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         if ($t_m_sup != 0)
                         {
                             $temp = $merit_array[4];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_m_sup >= $r_num)
                                 {
                                        $t_m_sup -= $r_num;
                                        if ($r_type==5 && !$lstudentprofile->is_ult_merit_disabled)
                                        {
                                             $t_m_ult++;
                                        }
                                        else
                                        {
                                            $t_m_sup += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         ##################################

                         ##################################
                         # Demerit part
                         if ($t_d_pt != 0)
                         {
                             $temp = $demerit_array["-1"];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_d_pt >= $r_num)
                                 {
                                        $t_d_pt -= $r_num;
                                        if ($r_type==-2 && !$lstudentprofile->is_min_demer_disabled)
                                        {
                                            $t_d_min++;
                                        }
                                        else if ($r_type==-3 && !$lstudentprofile->is_maj_demer_disabled)
                                        {
                                             $t_d_maj++;
                                        }
                                        else if ($r_type==-4 && !$lstudentprofile->is_sup_demer_disabled)
                                        {
                                             $t_d_sup++;
                                        }
                                        else if ($r_type==-5 && !$lstudentprofile->is_ult_demer_disabled)
                                        {
                                             $t_d_ult++;
                                        }
                                        else
                                        {
                                            $t_d_pt += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         if ($t_d_min != 0)
                         {
                             $temp = $demerit_array["-2"];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_d_min >= $r_num)
                                 {
                                        $t_d_min -= $r_num;
                                        if ($r_type==-3 && !$lstudentprofile->is_maj_demer_disabled)
                                        {
                                             $t_d_maj++;
                                        }
                                        else if ($r_type==-4 && !$lstudentprofile->is_sup_demer_disabled)
                                        {
                                             $t_d_sup++;
                                        }
                                        else if ($r_type==-5 && !$lstudentprofile->is_ult_demer_disabled)
                                        {
                                             $t_d_ult++;
                                        }
                                        else
                                        {
                                            $t_d_min += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         if ($t_d_maj != 0)
                         {
                             $temp = $demerit_array["-3"];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_d_maj >= $r_num)
                                 {
                                        $t_d_maj -= $r_num;
                                        if ($r_type==-4 && !$lstudentprofile->is_sup_demer_disabled)
                                        {
                                             $t_d_sup++;
                                        }
                                        else if ($r_type==-5 && !$lstudentprofile->is_ult_demer_disabled)
                                        {
                                             $t_d_ult++;
                                        }
                                        else
                                        {
                                            $t_d_maj += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         if ($t_d_sup != 0)
                         {
                             $temp = $demerit_array["-4"];
                             if (is_array($temp) && sizeof($temp)==2)
                             {
                                 list($r_num, $r_type) = $temp;
                                 while ($r_num > 0 && $t_m_sup >= $r_num)
                                 {
                                        $t_d_sup -= $r_num;
                                        if ($r_type==-5 && !$lstudentprofile->is_ult_demer_disabled)
                                        {
                                             $t_d_ult++;
                                        }
                                        else
                                        {
                                            $t_d_sup += $r_num;
                                            break;
                                        }
                                 }
                             }
                         }
                         ##################################

                         $result[$i] = array($t_year, $t_m_pt, $t_m_min, $t_m_maj, $t_m_sup, $t_m_ult,
                              $t_d_pt, $t_d_min, $t_d_maj, $t_d_sup, $t_d_ult);
                    }

                }
                #########################################################

                return $result;

       }

       function displayStudentRecordByYearAdmin($studentid,$year="",$sem="",$previewMode=false)
       {
                global $i_no_record_exists_msg;
                global $lstudentprofile;
                global $sys_custom;
                $result = $this->returnStudentRecordByYear($studentid,$year,$sem);
                
                $x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
                if (sizeof($result)==0)
                {
                    return "$x<tr><td>$i_no_record_exists_msg</td></tr></table>\n";
                }
                global $i_Profile_Year,$i_Merit_TypeArray;
                $title_array = array($i_Profile_Year);
                $title_array = array_merge($title_array,$i_Merit_TypeArray);
                $field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
                                              $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
                                              $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
                                              $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
                                              $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);
								# Customarization for UCCKE to hide "homework" and "bad name" in print preview
								# 6: homework column
								# 7: badname column
								$field_disabled_array[6] = ($previewMode && $sys_custom['student_profile_report_print_no_homework'])?true:$field_disabled_array[6];
								$field_disabled_array[7] = ($previewMode && $sys_custom['student_profile_report_print_no_badname'])?true:$field_disabled_array[7];
                $size = sizeof($result[0]) + 1;
                # $width = 100/$size;

                # ================== Changes from here
                global $merit_col_not_display;
                $x .= "<tr>\n";
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     if ($merit_col_not_display[$i]==1) continue;
                     if (!$field_disabled_array[$i])
                     {
                          $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
                     }
                }
                $x .= "</tr>\n";
                $col = sizeof($result[0]);
                $row = sizeof($result);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     $x .= "<tr>\n";
                     for ($j=0; $j<sizeof($result[$i]); $j++)
                     {
                          if ($merit_col_not_display[$j]==1) continue;
                          if (!$field_disabled_array[$j])
                          {
                          $data = $result[$i][$j];
													# Eric Yip : hide javascript link for print preview
                          if ($j == 0 && !$previewMode)
                          {
                              $data = "<a href=javascript:viewMeritByYear($studentid,'$data')>$data</a>";
                          }
                          $x .= "<td>$data</td>";
                          }
                     }
                     $x .= "</tr>\n";
                }
                $x .= "</table>\n";

                return $x;
       }
       
       function displayStudentRecordByYear($studentid,$year="",$student_class=array())
       {
                global $i_no_record_exists_msg,$image_path,$intranet_session_language, $PATH_WRT_ROOT;
                global $lstudentprofile;       # Got from original page to check fields to be used
                global $sys_custom;
                
				include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
						
                $result = $this->returnStudentRecordByYear($studentid,$year);
                $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";

                global $i_Profile_Year,$i_Merit_TypeArray, $i_Merit_TypeArrayWithWarning;
                
                $title_array = array($i_Profile_Year);
                $title_array = array_merge($title_array,$i_Merit_TypeArray);

                $field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
                                              $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
                                              $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
                                              $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
                                              $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);
                $size = sizeof($result[0]) + 1;
                
                # 20081015 - case [CRM Ref No.: 2008-0904-1514] add warning data in iAccount
                if($sys_custom['display_warning_iaccount'])
                {
	                # re-declare $field_disabled_array (add warning disabled=0)
	                $field_disabled_array_temp = array();
	                # merit part
	                for($i=0;$i<=5;$i++)
	                	$field_disabled_array_temp[$i] = $field_disabled_array[$i];
	                
	                # warning part
	                $field_disabled_array_temp[6] = 0; 
	                
	                # demerit part
	                for($i=6;$i<=10;$i++)
	                	$field_disabled_array_temp[$i+1] = $field_disabled_array[$i];
	                
	                # re-declare $title_array (add warning title)
					$title_array = array($i_Profile_Year);
					$title_array = array_merge($title_array,$i_Merit_TypeArrayWithWarning);
					//debug_r($title_array);
					# re-declare $result (add warning data)
					$new_result = array();
					foreach($result as $key=>$dataAry)
					{
						$new_dataAry = array();
						$this_year = $dataAry[0];
						
						# retireve warning# 
						$tempMeritRecod = $ldiscipline->retrieveStudentMeritTypeByYearSemester($studentid, $this_year);
						$warning_number = $tempMeritRecod[0] + 0;
						
						# year & merit part
	                	for($i=0;$i<=5;$i++)
	                		$new_dataAry[$i] = $dataAry[$i];
						# warning part
							$new_dataAry[6] = $warning_number;
						# demerit part
		                for($i=6;$i<=10;$i++)
		                	$new_dataAry[$i+1] = $dataAry[$i];	
		                	
		                $new_result[] = $new_dataAry;	
					}	
					$result = $new_result;
            	}
            	
                # $width = 100/$size;

                # ================== Changes from here
                $x .= "<tr>\n";
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     if (!$field_disabled_array[$i])
                     {
                          $x .= "<td class='tablebluetop tabletopnolink' align='left'>".$title_array[$i]."</td>";
                     }
                }
                $x .= "</tr>\n";
                $col = sizeof($result[0]);
                //$row = sizeof($result);
                # check includes record or not
                $data_no = 0;
                if(sizeof($result)==1)
                {
	                for ($i=1; $i<$col; $i++)
	                {
		             	$data_no += $result[0][$i];
	                }
            	}
 
                if($year!="") {
                	$sql = "SELECT AcademicYearID, YearNameEN, YearNameB5 FROM ACADEMIC_YEAR where YearNameEN='$year' OR YearNameB5='$year'";
                	$yearInfo = $ldiscipline->returnArray($sql,3);	
                } 
                else 
                    $yearInfo = GetAllAcademicYearInfo();
            
            //debug_pr($field_disabled_array);
            
                $class_year = array();
                for($i = 0; $i < sizeof($student_class); $i++){
                    for($k = 0; $k < sizeof($yearInfo); $k++){
                        if(in_array($student_class[$i][0], $yearInfo[$k])){
                                $class_year[] = $yearInfo[$k];
                        }
                    }
                }
                    
            	if($data_no || sizeof($result)>1)
            	{
            		/*
	                for ($i=0; $i<sizeof($result); $i++)
	                {
	                     $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>";
	                     for ($j=0; $j<sizeof($result[0]); $j++)
	                     {
	                          if (!$field_disabled_array[$j])
	                          {
	
	                          $data = $result[$i][$j];
	                          $academicYearID = $ldiscipline->getAcademicYearIDByYearName($data);
	                          
	                          if ($j == 0)
	                          {
	                              $data = "<a class=\"tablebluelink\" href=\"javascript:viewMeritByYear($studentid,'$data', '$academicYearID')\">$data</a>";
	                          }
	                          $x .= "<td class='tabletext'>$data</td>";
	                          }
	                     }
	                     $x .= "</tr>\n";
                	}
                	*/
                	
            	    for($a=0; $a<sizeof($class_year); $a++) {
            	        list($yearid, $yearEN, $yearB5) = $class_year[$a];
                		
                		$x .= "<tr class='tablebluerow". ( $a%2 + 1 )."'>";
                		
                		$tempAry = array();
                		
                		for($k=0; $k<sizeof($result); $k++) {
                			if($result[$k][0]==$yearEN || $result[$k][0]==$yearB5) {
                				$tempAry = $result[$k];
                				break;	
                			}
                		}
                		//debug_pr($tempAry);
                		if(sizeof($tempAry)==0) {	# no record in this year
                		
                			for($k=0; $k<sizeof($title_array); $k++) {
	                    		if($k==0)
	                    			$x .= "<td class='tabletext'><a class=\"tablebluelink\" href=\"javascript:viewMeritByYear($studentid,'".Get_Lang_Selection($yearB5, $yearEN)."', '$yearid')\">".Get_Lang_Selection($yearB5, $yearEN)."</a></td>";
	                    		else 
	                    			if(!$field_disabled_array[$k])
	 	                   				$x .= "<td class='tabletext'>0</td>";
	                    	}
                		} else {			               	
	                    	for($k=0; $k<sizeof($tempAry); $k++) {
	                    		if($k==0)
	                    			$x .= "<td class='tabletext'><a class=\"tablebluelink\" href=\"javascript:viewMeritByYear($studentid,'".Get_Lang_Selection($yearB5, $yearEN)."', '$yearid')\">".Get_Lang_Selection($yearB5, $yearEN)."</a></td>";
	                    		else 
	                    			if(!$field_disabled_array[$k])
		                    			$x .= "<td class='tabletext'>".$tempAry[$k]."</td>";
	                    	}
                		}
                		$x .= "</tr>\n";
                	}
            	}
                else
                //if (sizeof($result)==0)
                {
                	# commented by Henry Chow on 2010-10-06 (display "0" in school year rather than display "There is no record at this moment.") [CRM Ref No.: 2010-0929-0908]
                    //$x .= "<tr class='tablebluerow2'><td class='tablebluerow2 tabletext' colspan='".sizeof($title_array)."' align='center'>$i_no_record_exists_msg</td></tr>\n";
	                    
                    for($a=0; $a<sizeof($class_year); $a++) {
                        list($yearid, $yearEN, $yearB5) = $class_year[$a];
                    	
                    	//$startDate = getStartDateOfAcademicYear($yearid);
                    	//if($startDate<date("Y-m-d")) {
	                    	$x .= "<tr class='tablebluerow". ( $a%2 + 1 )."'>";
	                    	for($k=0; $k<sizeof($title_array); $k++) {
	                    		if($k==0)
	                    			$x .= "<td class='tabletext'><a class=\"tablebluelink\" href=\"javascript:viewMeritByYear($studentid,'".Get_Lang_Selection($yearB5, $yearEN)."', '$yearid')\">".Get_Lang_Selection($yearB5, $yearEN)."</a></td>";
	                    		else 
	                    			if(!$field_disabled_array[$k])
		 	                   			$x .= "<td class='tabletext'>0</td>";
	                    	}
	                    	$x .= "<tr>";
                    	//}
                    }
                }
                $x .= "</table>\n";
                return $x;
       }
       
       function returnDetailedRecordByYear($StudentID, $year, $displayWarning=0, $yearid="")
       {
       			global $sys_custom;
       			
       			// 2013-1009-1833-14073
       			if ($yearid == '') {
       				$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE YearNameEN='$year' OR YearNameB5='$year'";
       				$ary = $this->returnArray($sql);
       				$yearid = $ary[0]['AcademicYearID'];
       			}
       			
       			//$sql = "SELECT YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE YearNameEN='$year' OR YearNameB5='$year'";
       			$sql = "SELECT YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID='$yearid'";
       			$yearInfo = $this->returnArray($sql, 2);
       			$yearNameB5 = $yearInfo[0][0];
       			$yearNameEN = $yearInfo[0][1];
       			
       			if ($sys_custom['skhtsk'])
       			{
       				$sql = "SELECT
                                DATE_FORMAT(a.MeritDate,'%Y-%m-%d'), a.RecordType, a.NumberOfUnit, IFNULL(a.Reason, '--'), IFNULL(b.Subject, '--')
                        FROM
                                PROFILE_STUDENT_MERIT AS a
                                LEFT OUTER JOIN DISCIPLINE_MERIT_RECORD AS b ON a.StudentMeritID = b.ProfileMeritID
                        WHERE
                                a.UserID = '$StudentID' AND (a.AcademicYearID='$yearid' OR Year='".$this->Get_Safe_Sql_Query($yearNameEN)."' OR Year='".$this->Get_Safe_Sql_Query($yearNameB5)."')
                        ORDER BY
                                a.MeritDate DESC
                        ";
                } else
                {
	                $fields = "DATE_FORMAT(MeritDate,'%Y-%m-%d') as MeritDate, RecordType,NumberOfUnit, IFNULL(Reason, '--') as Reason,IFNULL(Remark, '--') as Remark";
	                $conds = "UserID = $StudentID AND (AcademicYearID='$yearid' OR Year='".$this->Get_Safe_Sql_Query($yearNameEN)."' OR Year='".$this->Get_Safe_Sql_Query($yearNameB5)."')";
	                $order = "MeritDate DESC";
	                $sql = "SELECT $fields FROM PROFILE_STUDENT_MERIT WHERE $conds ORDER BY $order";
	            }
	            $result = $this->returnArray($sql,5);
	            
	            # 20081016 (yatwoon) - display warning data if requested [customization]
	            if($displayWarning)
	            {
		            # retrieve warning records from DISCIPLINE_MERIT_RECORD
		            $sql = "SELECT
                                RecordDate as MeritDate, ProfileMeritType, ProfileMeritCount, ItemText as Reason, IFNULL(Remark, '--') as Remark
                        FROM
                                DISCIPLINE_MERIT_RECORD 
                        WHERE
                                StudentID = '$StudentID' AND (AcademicYearID='$yearid' OR Year='".$this->Get_Safe_Sql_Query($yearNameEN)."' OR Year='".$this->Get_Safe_Sql_Query($yearNameB5)."') and ProfileMeritType=0 and RecordStatus=1 AND ReleaseStatus=1
                        ORDER BY
                                MeritDate DESC, RecordDate DESC
                        ";
		            $result2 = $this->returnArray($sql);
		            
		            # merage and sort $result
		            $result = array_merge($result, $result2);
					$result = sortByColumn($result,'MeritDate',0);
	            }
	            
                return $result;
       }
       
       function displayDetailedRecordByYear($StudentID, $year, $displayWarning=0, $yearid="")
       {
                global $i_Merit_Date,$i_Merit_Type,$i_Merit_Qty,$i_Merit_Reason,$i_Merit_Remark;
                global $i_no_record_exists_msg;
                global $i_Merit_Merit,$i_Merit_MinorCredit,$i_Merit_MajorCredit,
                       $i_Merit_SuperCredit,$i_Merit_UltraCredit,$i_Merit_BlackMark,
                       $i_Merit_MinorDemerit,$i_Merit_MajorDemerit,$i_Merit_SuperDemerit, $i_Merit_Warning, 
                       $i_Merit_UltraDemerit, $i_general_record_date, $i_Teaching_Subject, $i_general_na;
                global $sys_custom;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnDetailedRecordByYear($StudentID,$year, $displayWarning, $yearid);
               
                $header_remark = ($sys_custom['skhtsk']) ? $i_Teaching_Subject : $i_Merit_Remark;
              $x .= "<table width=100% border=0 cellpadding=4 cellspacing=0>
        <tr>
          <td class='tablebluetop tabletopnolink' >$i_general_record_date</td>
          <td class='tablebluetop tabletopnolink' >$i_Merit_Type</td>
          <td class='tablebluetop tabletopnolink' >$i_Merit_Qty</td>
          <td class='tablebluetop tabletopnolink' >$i_Merit_Reason</td>
          <td class='tablebluetop tabletopnolink' >$header_remark</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($meritDate,$type,$qty,$reason,$remark) = $result[$i];
                   if (is_null($qty) || $qty == 0)
                   {
                           $qty = "--";
                   }
                   switch ($type)
                   {
                           case 1: $typeStr = $i_Merit_Merit; break;
                           case 2: $typeStr = $i_Merit_MinorCredit; break;
                           case 3: $typeStr = $i_Merit_MajorCredit; break;
                           case 4: $typeStr = $i_Merit_SuperCredit; break;
                           case 5: $typeStr = $i_Merit_UltraCredit; break;
                           case -1: $typeStr = $i_Merit_BlackMark; break;
                           case -2: $typeStr = $i_Merit_MinorDemerit; break;
                           case -3: $typeStr = $i_Merit_MajorDemerit; break;
                           case -4: $typeStr = $i_Merit_SuperDemerit; break;
                           case -5: $typeStr = $i_Merit_UltraDemerit; break;
                           case 0: $typeStr = $i_Merit_Warning; break;		# 20081016
                           default: $typeStr = "(".$i_general_na.")"; break;
                   }
                   $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
          <td class='tabletext' nowrap>$meritDate</td>
          <td class='tabletext'>$typeStr</td>
          <td class='tabletext'>$qty</td>
          <td class='tabletext'>&nbsp;$reason</td>
          <td class='tabletext'>&nbsp;$remark</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr class='tablebluerow2'><td colspan='5' class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;
       }

       function getClassMeritListByReason($start,$end,$reason)
       {
                $date_conds = $this->returnDateConditions($start,$end);
                $sql = "SELECT c.YearClassID, b.ClassName, SUM(a.NumberOfUnit), c.YearID
                        FROM PROFILE_STUDENT_MERIT as a 
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN
                        WHERE Reason = '$reason' $date_conds
                        GROUP BY b.ClassName";
                $result = $this->returnArray($sql,4);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($ClassID,$ClassName,$MeritCount,$LvlID) = $result[$i];
                     $counts[$ClassID] = array($ClassName,$MeritCount,$LvlID);
                }

                $classList = $this->getClassList();
                $returnResult = array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $MeritCount = $counts[$ClassID][1]+0;
                     $returnResult[] = array($ClassID, $ClassName, $MeritCount, $LvlID);
                }
                return $returnResult;
       }
       function getMeritListByClassReason($classid, $reason, $start, $end)
       {
                $date_conds = $this->returnDateConditions($start,$end);
                $class_name = $this->getClassName($classid);
                $name_field = getNameFieldByLang("b.");
                $sql = "SELECT a.UserID, $name_field, b.ClassNumber, SUM(a.NumberOfUnit)
                        FROM PROFILE_STUDENT_MERIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.ClassName = '$class_name' AND a.Reason = '$reason' $date_conds
                        GROUP BY a.UserID";
                $result = $this->returnArray($sql,4);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$StudentName,$ClassNumber,$count) = $result[$i];
                     $counts[$StudentID] = array($StudentName,$ClassNumber,$count);
                }

                $studentList = $this->getClassStudentNameList($classid);
                $returnResult = array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $ClassNumber) = $studentList[$i];
                     $MeritCount = $counts[$StudentID][2]+0;
                     $returnResult[] = array($StudentID, $StudentName,$ClassNumber, $MeritCount);
                }
                return $returnResult;
       }

       // ++++++ for archive student ++++++ \\
       function displayArchiveStudentRecordByYearAdmin($studentid,$year="",$sem="")
       {
                global $i_no_record_exists_msg;
                global $lstudentprofile;
                $result = $this->returnArchiveStudentRecordByYear($studentid,$year,$sem);
                $x = "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
                if (sizeof($result)==0)
                {
/*
                      global $i_Merit_Date,$i_Merit_Type,$i_Merit_Qty,$i_Merit_Reason,$i_Merit_Remark;

                              $x .= "
                                                                        <tr>
                                                                          <td class=tableTitle_new>$i_Merit_Date</td>
                                                                          <td class=tableTitle_new>$i_Merit_Type</td>
                                                                          <td class=tableTitle_new>$i_Merit_Qty</td>
                                                                          <td class=tableTitle_new>$i_Merit_Reason</td>
                                                                          <td class=tableTitle_new>$i_Merit_Remark</td>
                                                                        </tr>\n";
                                       $x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr></table>\n";

                                       return $x;
*/
                    return "$x<tr><td>$i_no_record_exists_msg</td></tr></table>\n";
                }
                global $i_Profile_Year,$i_Merit_TypeArray;
                $title_array = array($i_Profile_Year);
                $title_array = array_merge($title_array,$i_Merit_TypeArray);
                $field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
                                        $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
                                        $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
                                        $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
                                        $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);


                $size = sizeof($result[0]) + 1;
                # $width = 100/$size;

                # ================== Changes from here
                global $merit_col_not_display;
                $x .= "<tr>\n";
                for ($i=0; $i<sizeof($title_array); $i++)
                {
                     if ($merit_col_not_display[$i]==1) continue;
                     if (!$field_disabled_array[$i])
                     {
                          $x .= "<td class=tableTitle_new>".$title_array[$i]."</td>";
                     }
                }
                $x .= "</tr>\n";



                $col = sizeof($result[0]);
                $row = sizeof($result);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     $x .= "<tr>\n";
                     for ($j=0; $j<sizeof($result[$i]); $j++)
                     {
                          if ($merit_col_not_display[$j]==1) continue;
                          if (!$field_disabled_array[$j])
                          {
                               $data = $result[$i][$j];
                               if ($j == 0)
                               {
                                   $data = "<a href=\"javascript:viewMeritByYear($studentid,'$data')\">$data</a>";
                               }
                               $x .= "<td>$data</td>";
                          }
                     }
                     $x .= "</tr>\n";
                }
                $x .= "</table>\n";

                return $x;
       }

       function returnArchiveStudentRecordByYear($studentid,$year,$sem="")
       {
                # Grab information in DB
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");

                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     $sql = "SELECT Year, SUM(NumberOfUnit) FROM PROFILE_ARCHIVE_MERIT
                             WHERE UserID = $studentid AND MeritType<>\"\" AND MeritType = '".$this->type_archive_array[$i]."' $conds
                             GROUP BY Year";
                     $data[] = $this->returnArray($sql,2);
                }

                # Build the whole array

               // $years = ($year==""? $this->returnArchiveYears($studentid):array($year));
                if ($year == "")
                {
	                global $all_distinct_years;
	                if($all_distinct_years==""){
                    	$years = $this->returnArchiveYears($studentid);
                    }else{
                    	$years = $all_distinct_years;
                    }
                }
                else
                {
                    $years = array($year);
                }
                $result = array();
                for ($i=0; $i<sizeof($years); $i++)
                {
                     $result[$i][0] = $years[$i];
                }
                for ($i=0; $i<sizeof($this->type_array); $i++)
                {
                     //$result[$i] = array();
                     for ($j=0; $j<sizeof($years); $j++)
                     {
                          $pos = -1;
                          for ($k=0; $k<sizeof($data[$i]); $k++)
                          {
                               if ($data[$i][$k][0] == $years[$j])
                               {
                                   $pos = $k;
                                   break;
                               }
                          }
                          if ($pos == -1)
                          {
                              $count = 0;
                          }
                          else
                          {
                              $count = $data[$i][$k][1];
                          }
                          $result[$j][$i+1] = $count;
                          #$result[$i+1][$j] = $count;
                     }
                }
                return $result;

       }

       function returnArchiveYears($studentid)
       {
                $sql = "SELECT DISTINCT Year FROM PROFILE_ARCHIVE_MERIT WHERE UserID = '$studentid' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }

       function displayArchiveDetailedRecordByYear($StudentID, $year)
       {
                global $i_Merit_Date,$i_Merit_Type,$i_Merit_Qty,$i_Merit_Reason,$i_Merit_Remark;
                global $i_no_record_exists_msg;
                global $i_Merit_Merit,$i_Merit_MinorCredit,$i_Merit_MajorCredit,
                       $i_Merit_SuperCredit,$i_Merit_UltraCredit,$i_Merit_BlackMark,
                       $i_Merit_MinorDemerit,$i_Merit_MajorDemerit,$i_Merit_SuperDemerit,
                       $i_Merit_UltraDemerit, $i_general_na;

                $no_msg = $i_no_record_exists_msg;

                $result = $this->returnArchiveDetailedRecordByYear($StudentID,$year);
              $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Date</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Type</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Qty</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Reason</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Merit_Remark</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($meritDate,$type,$qty,$reason,$remark) = $result[$i];
                   switch ($type)
                   {
                           case "Merit": $typeStr = $i_Merit_Merit; break;
                           case "Minor Credit": $typeStr = $i_Merit_MinorCredit; break;
                           case "Major Credit": $typeStr = $i_Merit_MajorCredit; break;
                           case "Super Credit": $typeStr = $i_Merit_SuperCredit; break;
                           case "Ultra Credit": $typeStr = $i_Merit_UltraCredit; break;
                           case "Black Mark": $typeStr = $i_Merit_BlackMark; break;
                           case "Minor Demerit": $typeStr = $i_Merit_MinorDemerit; break;
                           case "Major Demerit": $typeStr = $i_Merit_MajorDemerit; break;
                           case "Super Demerit": $typeStr = $i_Merit_SuperDemerit; break;
                           case "Ultra Demerit": $typeStr = $i_Merit_UltraDemerit; break;
                           default: $typeStr = "(".$i_general_na.")"; break;
                   }
                   $x .= "<tr>
          <td class=td_center_middle bgcolor=#E8FBFC>$meritDate</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$typeStr</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$qty</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$reason</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$remark</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=5 class=td_center_middle bgcolor=#E8FBFC>".$no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;
       }

       function returnArchiveDetailedRecordByYear($StudentID, $year)
       {
                $fields = "DATE_FORMAT(RecordDate,'%Y-%m-%d'), MeritType,NumberOfUnit, Reason,Remark";
                $conds = "UserID = $StudentID AND Year = '$year'";
                $order = "RecordDate DESC";
                $sql = "SELECT $fields FROM PROFILE_ARCHIVE_MERIT WHERE MeritType<>\"NULL\" AND $conds ORDER BY $order";
                return $this->returnArray($sql,5);
       }

       // +++++ end of archive student +++++ \\

		function retrieveStudentMeritRecordByDate($studentID, $start, $end)
		{
			include_once("libdisciplinev12.php");
			$ldiscipline = new libdisciplinev12();
			
			$sql = "SELECT RecordType, SUM(NumberOfUnit) FROM PROFILE_STUDENT_MERIT WHERE UserID='$studentID' AND MeritDate BETWEEN '$start' AND '$end' GROUP BY RecordType";
			$tempResult = $this->returnArray($sql,2);
		
			$returnAry = array();
		
			for($i=0; $i<sizeof($tempResult); $i++) {
				list($type, $count) = $tempResult[$i];
				$returnAry[$type] = $ldiscipline->TrimDeMeritNumber_00($count);
			}
			
			return $returnAry;
		}

 }


} // End of directives
?>
