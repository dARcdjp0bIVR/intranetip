<?php
# using: 

####### Change log [Start] #######
#
#   Date:   2019-09-10 (Bill)   [2019-0905-1214-23207]
#           modified return_FormContent(), to update sync button lang
#
#   Date:   2019-08-12 (Bill)   [2019-0812-1014-25206]
#           modified displayAttachment(), fixed cannot download attachment in edge
#
#   Date:   2019-06-25 (Tommy)
#           add kis output in GET_MODULE_OBJ_ARR()
#
#   Date:   2019-05-14 (Bill)
#           modified countTotalNumber(), countSignedNumber(), getStar(), returnIssuerName(), synchronizeSignedCount(), getReplyAnswer(), isFullAdmin(), to prevent SQL Injection
#
#   Date:   2019-03-06 (Bill)
#           modified displayAttachment_showImage(), to support token parm in attachment url
#
#	Date:	2017-07-04	(Bill)
#			- modified libcircular(), to fix PHP error when eCircular without any module settings
#
#	Date:	2016-11-25	(Villa)
#			-modified  returnEmailNotificationData() add space after the url
#
#	Date:	2016-11-18	(Tiffany) 
#			-modified displayAttachment_showImage() let the attachment link encrypt.
#
#	Date:	2016-11-10	(Villa) K108335
#			-modified return_FormContent() add NotifyEmailRemark msg
#
# 	Date:	2016-09-23	(Villa)
#			-modified return_FormContent()	libcircular()
#			add to module setting - "default using eclass App to notify"
#			
#	
#	Date:	2016-09-23	(Villa)
#			= modified return_FormContent()
#			- add block to push msg setting
#
#	Date:	2016-08-23 (Omas) ip.2.5.6.10.1
#			added returnIndividualTypeNames2()
#
#	Date:	2015-10-16 (Roy)
#			version: ip.2.5.6.12.1
#			modified return_FormContent(), add radio button and time picker for scheduled push message
#
#	Date:	2014-10-16 (Roy)
#			- add returnPushMessageNotificationData()
#			- update return_FormContent(), add send push message option
#
#	Date:	2014-09-23 (Ivan)
#			modified displayAttachment() to add target="_blank" for attachment download link for app
#			
#	Date:	2014-02-06 (YatWoon)
#			modified displayAttachment(), add checking, return empty if no attachment folder data [Case#J58238 ]
#
#	Date:	2013-09-17 (Siuwan)
#			modified return_FormContent() - set Fckeditor Toolbarset - Basic2_withInsertImageFlash
#
#	Date:	2013-09-06 (YatWoon)
#			add displayAttachmentEdit() - copied from school news
#			add genAttachmentFolderName()
#
#	Date:	2013-08-27 (Henry)
#			added the new page button "PageCircular_UnsignedCircularList"
#			
#	Date:	2013-03-11 (YatWoon)
#			update returnEmailNotificationData(), add $senderEmail parameter for display sender [Case#2013-0304-1518-38093]
#
#	Date:	2012-07-06 (YatWoon)
#			update displayAttachment(), download_attachment.php with encrypt logic
#
#	Date:	2011-12-02 (YatWoon)
#			modified returnTargetUserIDArray(), fixed: failed to retrieve result if the target is mixed with U & G
#
#	Date:	2011-10-14 (Henry Chow)
#			modified returnTargetUserIDArray(), revise slow query [CRM : 2011-1012-0928-23066] 
#			
#	Date:	2011-08-02	YatWoon
#			add return_FormContent(), support 1 page new/edit
#
#	Date:	2011-04-14	YatWoon
#			#2011-0413-1111-06067, client request don't display user login
#			replace all getNameFieldWithLoginByLang to getNameFieldByLang
#
#	Date:	2011-03-25	YatWoon
#			add function returnEmailNotificationData(), return notification email subject & content
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2011-02-23	YatWoon
#			Add option "Display question number"
#
#	Date:	2010-10-29	YatWoon
#			udpate returnTableViewAll(), returnAnswerString(), returnAllResult()
#			retrieve teaching staff / non-teaching staff data
#
#	Date: 2010-09-13 YatWoon
#			update parseAnswerStr(), add ";" to separate the answers
#
#	 Date:	2010-03-24	YatWoon
#			update returnRecord()
#			Add field "AllFieldsReq" for option  "All questions are required to be answered"
#
#	Date:	2010-02-03	YatWoon
#			add retrieveCircularYears()
#			retrieve the year which includes any circular.
#
#	Date:	2010-02-01 [YatWoon]
#			update buildReplySlipTemplates()
#			replace "line break" with "space"
#
#	Date:	2010-01-14 [YatWoon]
#			add buildReplySlipTemplates()
#			build the reply slip tempates array
#
####### Change log [End] #######

#### New wordings #####
# include_once($intranet_root."/lang/circular_lang.$intranet_session_language.php");

if (!defined("LIBCIRCULAR_DEFINED"))                     // Preprocessor directive
{
  define("LIBCIRCULAR_DEFINED", true);

  $circular_conf_file = "$intranet_root/plugins/circular_conf.php";
  if (is_file($circular_conf_file))
  {
      include_once($circular_conf_file);
  }

  class libcircular extends libdb
  {

        # Param
        var $admin_id_type;
        var $recipient_id_type;


        # Settings
        var $disabled;
        var $isHelpSignAllow;
        var $isLateSignAllow;
        var $isResignAllow;
        var $defaultNumDays;
        var $setting_file;
        var $showAllEnabled;
        var $MaxReplySlipOption;
        var $sendPushMsg;

        # Record
        var $CircularID;
        var $CircularNumber;
        var $Title;
        var $DateStart;
        var $DateEnd;
        var $Description;
        var $IssueUserID;
        var $RecipientID;
        var $Question;
        var $Attachment;
        var $RecordType;
        var $RecordStatus;
        var $IssueUserName;
        var $signedCount;
        var $totalCount;
        var $DateInput;
        var $DateModified;
        var $AllFieldsReq;
		var $DisplayQuestionNumber;
		
        # Reply record
        var $replyID;
        var $answer;
        var $targetUserID;
        var $targetUserName;
        var $signerID;
        var $signerName;
        var $replyType;
        var $replyStatus;
        var $signedTime;

        var $question_array;

        function libcircular($ID="")
        {
				global $intranet_root;
                 
                $this->libdb();
                $this->Module = "CIRCULAR";
                 
				if (!isset($_SESSION["SSV_PRIVILEGE"]["circular"]))
             	{
					include_once("libgeneralsettings.php");
					$lgeneralsettings = new libgeneralsettings();
					
					$settings_ary = $lgeneralsettings->Get_General_Setting($this->Module);
					foreach((array)$settings_ary as $key => $data)
					{
						$_SESSION["SSV_PRIVILEGE"]["circular"][$key] = $data;
						$this->$key = $data; 
					}
				}
				else
				{
					$this->disabled = $_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"];
					$this->isHelpSignAllow = $_SESSION["SSV_PRIVILEGE"]["circular"]["isHelpSignAllow"];
					$this->isLateSignAllow = $_SESSION["SSV_PRIVILEGE"]["circular"]["isLateSignAllow"];			
					$this->isResignAllow = $_SESSION["SSV_PRIVILEGE"]["circular"]["isResignAllow"];
					$this->defaultNumDays = $_SESSION["SSV_PRIVILEGE"]["circular"]["defaultNumDays"];
					$this->showAllEnabled = $_SESSION["SSV_PRIVILEGE"]["circular"]["showAllEnabled"];
					$this->MaxReplySlipOption = $_SESSION["SSV_PRIVILEGE"]["circular"]["MaxReplySlipOption"];
					$this->sendPushMsg =  $_SESSION["SSV_PRIVILEGE"]["circular"]['sendPushMsg'];
                 }
                 
                 if ($ID!="")
                 {
                     $this->returnRecord($ID);
                 }
                 
                 global $circular_admin_id_type,$circular_recipient_id_type;
                 $this->admin_id_type = ($circular_admin_id_type!=""? $circular_admin_id_type : "1");
                 $this->recipient_id_type = ($circular_recipient_id_type!=""? $circular_recipient_id_type : "1");
        }
        
        function returnNonAdminUser()
        {
                 $name_field = getNameFieldByLang("a.");
                 $sql = "SELECT a.UserID, $name_field FROM
                                INTRANET_USER as a LEFT OUTER JOIN INTRANET_ADMIN_USER as b ON a.UserID = b.UserID AND b.RecordType = 1
                         WHERE b.UserID IS NULL AND a.RecordStatus = 1 AND a.RecordType IN (".$this->admin_id_type.")";
                 return $this->returnArray($sql,2);
        }
        function displayNonAdminUserInput()
        {
                 $users = $this->returnNonAdminUser();
                 $x = "<table width=95% border=0 cellpadding=2 cellspacing=2>";
                 for ($i=0; $i<sizeof($users); $i++)
                 {
                      list ($id, $name) = $users[$i];
                      if ($i%2==0)       # first col
                      {
                          $x .= "<tr>";
                      }
                      $x .= "<td><input type=checkbox name=targetUserID[] value=$id> $name</td>";
                      if ($i%2==1)
                      {
                          $x .= "</tr>\n";
                      }
                 }
                 $x .= "</table>";
                 return $x;
        }

        # Return Circular list in admin view
        # Param:
        # status - 1: published, 2: suspended, 3: template only
        # past - 1: Past circular, otherwise current
        # my - 1: Issue by this user, otherwise all
        # year, month - for filtering

        function returnCircularListAdminView($status,$past, $my,$allsign, $year,$month)
        {
                 $conds = "";
                 if ($year != "")
                 {
                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                 }
                 if ($month != "")
                 {
                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                 }
                 if ($my == 1)
                 {
                     global $UserID;
                     $conds .= " AND a.IssueUserID = $UserID";
                 }
                 if ($past == 1)
                 {
                     $conds .= " AND a.DateEnd < CURDATE()";
                 }
                 else if ($past == -1)
                 {
                     $conds .= " AND a.DateEnd >= CURDATE()";
                 }

                 if ($allsign==1)
                 {
                     $conds .= " AND a.SignedCount = a.TotalCount";
                 }
                 else if ($allsign == -1)
                 {
                      $conds .= " AND a.SignedCount < a.TotalCount";
                 }

                 $name_field = getNameFieldByLang("b.");
                 $sql = "SELECT a.CircularID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
                                DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),
                                a.CircularNumber,a.Title,
                         IF (b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),
                         a.RecordType, a.SignedCount, a.TotalCount
                         FROM INTRANET_CIRCULAR as a LEFT OUTER JOIN INTRANET_USER as b ON a.IssueUserID = b.UserID
                         WHERE a.RecordStatus = $status $conds
                         ORDER BY a.DateStart DESC";
                 return $this->returnArray($sql,9);
        }


        # Display admin table listing
        # Param:
        # status - 1: published, 2: suspended, 3: template only
        # past - 1: Past circular, otherwise current
        # my - 1: Issue by this user, otherwise all
        # year, month - for filtering
        function displayAdminView($status,$past, $my, $allsign, $year="",$month="")
        {
                 global $i_Circular_DateStart,$i_Circular_DateEnd,$i_Circular_CircularNumber,
                        $i_Circular_Title,$i_Circular_Issuer,$i_Circular_RecipientType,
                        $i_Circular_RecipientTypeAllStaff,$i_Circular_RecipientTypeAllTeaching,
                        $i_Circular_RecipientTypeAllNonTeaching,$i_Circular_RecipientTypeIndividual,
                        $i_Circular_Signed,$i_Circular_Total,$i_Circular_ViewResult,$i_Circular_NoRecord;
                 global $UserID;
                 global $image_path;


                 $circulars = $this->returnCircularListAdminView($status,$past,$my,$allsign,$year, $month);
                 $targetType = array("",$i_Circular_RecipientTypeAllStaff,
                                        $i_Circular_RecipientTypeAllTeaching,
                                        $i_Circular_RecipientTypeAllNonTeaching,
                                        $i_Circular_RecipientTypeIndividual);

                 $cols = 8;

                 $cell_bg = array("#FFFBB7","#EFE9C6");
                 $x = "<table width=760 border=1 cellpadding=5 cellspacing=0 bordercolorlight=#F4D3AF bordercolordark=#E19794 class=body>\n";
                 $x .= "<tr bgcolor=#C9DA7F><td><B>$i_Circular_DateStart</B></td><td><B>$i_Circular_DateEnd</B></td><td><B>$i_Circular_CircularNumber</B></td>
                         <td><B>$i_Circular_Title</B></td><td><B>$i_Circular_Issuer</B></td>";
                 if ($status==1)
                 {
                     $x .= "<td><B>$i_Circular_Signed/$i_Circular_Total</B></td><td><B>$i_Circular_ViewResult</B></td>";
                 }
                 $x .= "<td><B>$i_Circular_RecipientType</B></td></tr>\n";
                 $today = mktime(0,0,0,date('m'),date('d'),date('Y'));
                 for ($i=0; $i<sizeof($circulars); $i++)
                 {
                      list ($circularID,$issueDate,$endDate, $circularNumber,$title,$issuer,$type, $numSigned, $numTotal) = $circulars[$i];

                      $control = "";
                      $start = strtotime($issueDate);
                      if (!($status == 1 && compareDate($start,$today) <= 0))
                      {
                            $control = $this->returnEditLink($circularID);
                      }
                      $control .= " ".$this->returnDeleteLink($circularID);

                      $title_link = "<a href=javascript:viewCircular($circularID)>$title</a> $control";
                      $bgcolor = $cell_bg[$i%2];
                      $x .= "<tr bgcolor=$bgcolor><td>$issueDate</td><td>$endDate</td><td>$circularNumber</td><td>$title_link</td><td>$issuer</td>";
                      if ($status == 1)
                      {
                          $result_link = "<a href=javascript:viewResult($circularID)><img src=\"$image_path/enotice/icon_resultview.gif\" border=0 width=37 height=22 align=absmiddle></a>";
                          $x .= "<td>$numSigned/$numTotal</td><td>$result_link</td>";
                      }
                      $x .= "<td>".$targetType[$type]."</td></tr>\n";
                 }
                 if (sizeof($circulars)==0)
                 {
                     $x .= "<tr><td colspan=$cols align=center>$i_Circular_NoRecord</td></tr>\n";
                 }
                 $x .= "</table>\n";
                 return $x;
        }
        function returnEditLink ($id)
        {
                 global $image_path, $button_edit;
                 return "<A HREF=/home/circular/edit.php?CircularID=$id><img src=\"$image_path/edit_icon.gif\" alt=\"$button_edit\" border=0></A>";
        }
        function returnDeleteLink ($id)
        {
                 global $image_path, $button_remove;
                 return "<A HREF=/home/circular/remove.php?CircularID=$id><img src=\"$image_path/eraser_icon.gif\" alt=\"$button_remove\" border=0></A>";
        }
        function returnTemplates()
        {
                 $sql = "SELECT CircularID, CONCAT(CircularNumber,' - ',Title) FROM INTRANET_CIRCULAR WHERE RecordStatus = 3";
                 return $this->returnArray($sql,2);
        }


        function returnRecord($ID)
        {
                 $sql = "SELECT CircularNumber, Title,DATE_FORMAT(DateStart,'%Y-%m-%d'),DATE_FORMAT(DateEnd,'%Y-%m-%d'),Description,IssueUserID,
                         RecipientID,Question,Attachment,RecordType,RecordStatus,IssueUserName, SignedCount, TotalCount, DateInput, DateModified, AllFieldsReq, DisplayQuestionNumber
                         FROM INTRANET_CIRCULAR WHERE CircularID = $ID";
                 $result = $this->returnArray($sql);
                 $this->CircularID = $ID;
                 list($this->CircularNumber,$this->Title,$this->DateStart,$this->DateEnd,
                      $this->Description,$this->IssueUserID,$this->RecipientID,
                      $this->Question, $this->Attachment,$this->RecordType,
                      $this->RecordStatus, $this->IssueUserName, $this->signedCount,
                      $this->totalCount, $this->DateInput, $this->DateModified,
					  $this->AllFieldsReq, $this->DisplayQuestionNumber
                      ) = $result[0];
			
                 # 20081002 yat woon
                 $Questiontemp = $this->Question;
                 $Questiontemp = str_replace("\r","",$Questiontemp);
                 $Questiontemp = str_replace("\n","<br>",$Questiontemp);
                 $this->Question = $Questiontemp;
                 
                 return $result[0];
        }
        # Split group and user target
        function splitTargetGroupUserID($target)
        {
                 $GroupIDList = "";
                 $UserIDsList = "";
                 $group_delimiter = "";
                 $user_delimiter = "";
                 $row = explode(",",$target);
                 
                 for($i=0; $i<sizeof($row); $i++)
                 {
                     $targetType = substr($row[$i],0,1);
                     $targetID = substr($row[$i],1);
                     if ($targetType=="G")
                     {
                         $GroupIDList .= $group_delimiter.$targetID;
                         $group_delimiter = ",";
                     }
                     else
                     {
                         $UserIDsList .= $user_delimiter.$targetID;
                         $user_delimiter = ",";
                     }
                 }
                 $x[0] = ($GroupIDList == ""? 0:$GroupIDList);
                 $x[1] = ($UserIDsList == ""? 0:$UserIDsList);
                 return $x;
        }


        # Find the UserID of target for type 4
        # return UserID, name , user type
        function returnTargetUserIDArray($target)
        {
                 $row = $this->splitTargetGroupUserID($target);
                 $GroupIDList = $row[0];
                 $UserIDsList = $row[1];

                 $username_field = getNameFieldWithClassNumberForRecord("a.");
                 # Group

                 if ($GroupIDList!="")
                 {
                 	// revised SQL due to slow query [CRM : 2011-1012-0928-23066], by Henry Chow @ 20111014
                 	//$conds = "b.GroupID IN ($GroupIDList) and a.UserID = b.UserID";
//                  	$joinTableCond = " a.UserID = b.UserID and b.GroupID IN ($GroupIDList)";
					$joinTableCond = " a.UserID = b.UserID ";
					$conds = "b.GroupID IN ($GroupIDList)";
                    
                 }
                 else
                 {
                     $conds = "";
                 }

                 if ($UserIDsList!="")
                 {
                     if ($conds == "")
                         $conds = "AND a.UserID IN ($UserIDsList)";
                     else 
                     	$conds = "AND ($conds OR a.UserID IN ($UserIDsList))";
                 }
                 else
                 {
                 	if($conds!="")
	                    $conds = "AND $conds";
                 }

                 if ($UserIDsList=="" && $GroupIDList=="") return array();

                 $sql = "SELECT DISTINCT a.UserID,$username_field,a.RecordType
                         FROM INTRANET_USER as a";
                         
                 if($GroupIDList) {
                 		// revised SQL due to slow query [CRM : 2011-1012-0928-23066]
                 		// $sql .=", INTRANET_USERGROUP as b";
//                  		$sql .=" INNER JOIN INTRANET_USERGROUP as b ON ($joinTableCond)";
						$sql .=" left JOIN INTRANET_USERGROUP as b ON ($joinTableCond)";
                 }
                 $sql .="
                         WHERE 
                         a.RecordType IN (".$this->recipient_id_type.")
                         AND a.RecordStatus = 1
                         $conds";
                 $result = $this->returnArray($sql,3);

                 return $result;
        }

        function countTotalNumber($CircularID)
        {
                 $CircularID = IntegerSafe($CircularID);
                 $sql = "SELECT COUNT(*) FROM INTRANET_CIRCULAR_REPLY WHERE CircularID = '$CircularID'";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }
        
        function countSignedNumber ($CircularID)
        {
                 $CircularID = IntegerSafe($CircularID);
                 $sql = "SELECT COUNT(*) FROM INTRANET_CIRCULAR_REPLY WHERE CircularID = '$CircularID' AND RecordStatus = 2";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }

		function getStar ($CircularID)
        {
	             global $UserID;
	             
	             $CircularID = IntegerSafe($CircularID);
                 //$sql = "SELECT HasStar FROM INTRANET_CIRCULAR_REPLY WHERE CircularID = '$CircularID' AND RecordStatus = 2";
                 $sql = "SELECT HasStar FROM INTRANET_CIRCULAR_REPLY WHERE CircularID = '$CircularID' AND UserID = '$UserID'";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }
		
        function returnAllCircularWithStatus()
        {
                 $sql = "SELECT a.CircularID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),
                         a.CircularNumber,a.Title,
                         a.RecordType, a.RecordStatus
                         FROM INTRANET_CIRCULAR as a
                         WHERE a.RecordStatus = 1 OR a.RecordStatus = 2
                         ORDER BY a.DateStart DESC, a.CircularNumber DESC, a.CircularID";
                 return $this->returnArray($sql,7);
        }
        function displayNormalView($status, $past, $year="",$month="")
        {
                 global $i_Circular_DateStart,$i_Circular_DateEnd,$i_Circular_CircularNumber,
                        $i_Circular_Title,$i_Circular_Issuer,$i_Circular_RecipientType,
                        $i_Circular_RecipientTypeAllStaff,$i_Circular_RecipientTypeAllTeaching,
                        $i_Circular_RecipientTypeAllNonTeaching,$i_Circular_RecipientTypeIndividual,
                        $i_Circular_Signed,$i_Circular_Total,$i_Circular_ViewResult,$i_Circular_NoRecord,
                        $i_Circular_Issuer,$i_Circular_SignStatus,$i_Circular_Unsigned,$i_Circular_Signed;
                 global $UserID;

                 if ($year != "")
                 {
                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                 }
                 if ($month != "")
                 {
                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                 }
                 if ($past == 1)
                 {
                     $conds .= " AND a.DateEnd < CURDATE()";
                     $order_str = " a.DateStart DESC";
                 }
                 else
                 {
                     $conds .= " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
                     $order_str = " a.DateEnd ASC";
                 }
                 if ($status == "0" || $status == 2)
                 {
                     $conds .= " AND c.RecordStatus = $status";
                 }
//                  $name_field = getNameFieldWithLoginByLang("b.");
                 $name_field = getNameFieldByLang("b.");
                 $sql = "SELECT a.CircularID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
                                DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),a.CircularNumber,a.Title,
                                IF(b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),
                                a.RecordType, c.RecordType, c.RecordStatus
                         FROM INTRANET_CIRCULAR_REPLY as c
                              LEFT OUTER JOIN INTRANET_CIRCULAR as a ON a.CircularID = c.CircularID
                              LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = a.IssueUserID
                         WHERE c.UserID = $UserID AND a.RecordStatus = 1
                               $conds
                         ORDER BY $order_str";
                 $replies = $this->returnArray($sql,9);
                 $targetType = array("",$i_Circular_RecipientTypeAllStaff,
                                        $i_Circular_RecipientTypeAllTeaching,
                                        $i_Circular_RecipientTypeAllNonTeaching,
                                        $i_Circular_RecipientTypeIndividual);


                 $x = "<table width=760 border=1 cellpadding=5 cellspacing=0 bordercolorlight=#F4D3AF bordercolordark=#E19794 class=body>\n";
                 $x .= "<tr bgcolor=#C9DA7F><td><B>$i_Circular_DateStart</B></td><td><B>$i_Circular_DateEnd</B></td><td><B>$i_Circular_CircularNumber</B></td>";
                 $x .= "<td><B>$i_Circular_Title</B></td><td><B>$i_Circular_Issuer</B></td>";
                 $x .= "<td><B>$i_Circular_SignStatus</B></td><td><B>$i_Circular_RecipientType</B></td></tr>\n";
                 for ($i=0; $i<sizeof($replies); $i++)
                 {
                      list($id,$start,$end,$number,$title,$name,$recipientType,$replyType,$reply_status) = $replies[$i];

                      //$css = (($reply_status == 0 || $reply_status == "")? "unsigned":"signed");
                      //if ($replyType==2) $css = "signedbyother";

                      $title_link = "<a href=javascript:sign($id)>$title</a>";
                      if ($reply_status == 0 || $reply_status == "")
                      {
                          $css = "unsigned";
                          $status_string = "$i_Circular_Unsigned";
                      }
                      else if ($replyType == 2)
                      {
                           $css = "signedbyother";
                           $status_string = "$i_Circular_Signed";
                      }
                      else
                      {
                          $css = "signed";
                          $status_string = "$i_Circular_Signed";
                      }
                      #$sign_link = "<a href=javascript:sign($id,$studentID)>$i_Notice_OpenSign</a>";
                      $x .= "<tr class=$css><td>$start</td><td>$end</td><td>$number</td>";
                      $x .= "<td>$title_link</td><td>$name</td>";
                      $x .= "<td>$status_string</td>";
                      $x .= "<td>".$targetType[$recipientType]."</td></tr>\n";
                 }
                 if (sizeof($replies)==0)
                 {
                     $x .= "<tr><td align=center colspan=7>$i_Circular_NoRecord</td></tr>";
                 }
                 $x .= "</table>\n";

                 return $x;
        }


        function retrieveReply($targetUserID)
        {
                 if ($this->CircularID == "") return false;
                 $sql = "SELECT CircularReplyID, Answer,SignerID,RecordType,RecordStatus,DateModified,SignerName,UserName
                         FROM INTRANET_CIRCULAR_REPLY WHERE UserID = $targetUserID AND CircularID = ".$this->CircularID;
                 $result = $this->returnArray($sql,8);
                 list ($this->replyID, $this->answer,$this->signerID,$this->replyType,
                       $this->replyStatus,$this->signedTime,$this->signerName, $this->targetUserName)
                       = $result[0];
                 $this->targetUserID = $targetUserID;
                 if ($this->replyID == "") return false;
                 else return true;
        }

        function isReplySigned()
        {
                 if ($this->replyID == "") return false;
                 if ($this->replyStatus == 2) return true;
                 else return false;
        }

        function displayAttachment()
        {
		         if(trim($this->Attachment) == "")	return "";

                 global $file_path, $image_path, $intranet_httppath, $intranet_root;
                 $path = "$file_path/file/circular/".$this->Attachment;


                 $a = new libfiletable("", $path, 0, 0, "");
                 $files = $a->files;
                 while (list($key, $value) = each($files))
                 {
                        //$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
                        $url = str_replace($file_path, "", $path)."/".$files[$key][0];
//                         $url = rawurlencode($file_path.$url);
                        $x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        #$x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        //$encoded_url = "../../download_attachment.php?targetID=".$this->CircularID."&target=".urlencode($files[$key][0]);
                        $encoded_url = "/home/download_attachment.php?target_e=".getEncryptedText($intranet_root.$url);

                        // [2019-0812-1014-25206] prevent cannot download attachment in edge
                        //$x .= "<a href=\"$encoded_url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        //$x .= "<a href=\"$encoded_url\" target=\"_blank\">".$files[$key][0]."</a>";
                        $x .= "<a class=\"tablelink\" href=\"$encoded_url\" >".$files[$key][0]."</a>";

                        $x .= " (".ceil($files[$key][1]/1000)."Kb)";
                        $x .= "<br>\n";
                 }
                 return $x;
        }
 
        function displayAttachment_showImage($token='')
        {
	        if(trim($this->Attachment) == "")	return "";
    	         
    	         global $file_path, $image_path, $intranet_httppath,$intranet_root;
    	         
    	         $token_parms = '';
    	         if($token != '') {
    	             $token_parms = "&token=".$token;
    	         }
	             
                 $path = "$file_path/file/circular/".$this->Attachment;
                 
                 $a = new libfiletable("", $path, 0, 0, "");
                 $files = $a->files;
                 while (list($key, $value) = each($files))
                 {
                        //$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
                        $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                        //$url = rawurlencode($file_path.$url);
                        $target_filepath = $path."/".$files[$key][0];
                        if (isImage($target_filepath))
		                {
		                    	$size = GetImageSize($target_filepath);
		                        list($width, $height, $type, $attr) = $size;
		                        if ($width > 412 || $height > 550) {
			                        if($height>$width) {
		                        		$image_html_tag = " height=550";
			                        } else if($height<$width) {
		                        		$image_html_tag = "width=412";
			                        }
		                        }
		                        else
		                        {
		                            $image_html_tag = "";
		                        }
		                }
                        $x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        #$x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        //$encoded_url = "../../download_attachment.php?targetID=".$this->CircularID."&target=".urlencode($files[$key][0]);
                        // $encoded_url = "/home/download_attachment.php?target=".$url;
                        $encoded_url = "/home/download_attachment.php?target_e=".getEncryptedText($intranet_root.$url).$token_parms;
                        //$x .= "<a href=\"$encoded_url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $x .= "<a href=\"$encoded_url\" target=\"_blank\">".$files[$key][0]."</a>";
                        $x .= " (".ceil($files[$key][1]/1000)."Kb)";
                        
                        if (isImage($target_filepath))
		                {  
		                	$x .= "</br><img border=0 src=\"$encoded_url\" $image_html_tag>";
		                }
                        $x .= "<br>\n";
                 }
                 return $x;
        }
        
        function returnIssuerName()
        {
//               $name_field = getNameFieldWithLoginByLang("");
				 $name_field = getNameFieldByLang("");
                 $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '".$this->IssueUserID."'";
                 $result = $this->returnVector($sql);
                 return ($result[0]==""? $this->IssueUserName: $result[0]);
        }

        # Update SignedCount in Circular table
        function synchronizeSignedCount()
        {
                 if ($this->CircularID == "") return;
                 $sql = "LOCK TABLES INTRANET_CIRCULAR WRITE, INTRANET_CIRCULAR_REPLY READ";
                 $this->db_db_query($sql);

                 $sql = "SELECT COUNT(*) FROM INTRANET_CIRCULAR_REPLY WHERE RecordStatus = 2 AND CircularID = '".$this->CircularID."'";
                 $temp = $this->returnVector($sql);
                 $count = $temp[0]+0;
                 $sql = "UPDATE INTRANET_CIRCULAR SET SignedCount = '$count' WHERE CircularID = '".$this->CircularID."'";
                 $this->db_db_query($sql);

                 $sql = "UNLOCK TABLES";
                 $this->db_db_query($sql);
        }

        function getReplyAnswer($targetID)
        {
                 $targetID = IntegerSafe($targetID);
                 $sql = "SELECT Answer FROM INTRANET_CIRCULAR_REPLY WHERE UserID = '$targetID' AND CircularID = '".$this->CircularID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        
        function returnIndividualTypeNames($target)
        {
                 $IDs = $this->splitTargetGroupUserID($target);
                 $groups = $IDs[0];
                 $users = $IDs[1];

                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID IN ($groups) ORDER BY Title";
                 $result1 = $this->returnVector($sql);

//                  $namefield = getNameFieldWithLoginByLang();
                 $namefield = getNameFieldByLang();
                 $sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID IN ($users) ORDER BY ClassName, ClassNumber+0, ClassNumber";
                 $result2 = $this->returnVector($sql);

                 return array_merge($result1,$result2);
        }
        
        function returnIndividualTypeNames2($target)
        {
        	$IDs = $this->splitTargetGroupUserID($target);
        	$groups = $IDs[0];
        	$users = $IDs[1];
        	
        	$groupNameField = Get_Lang_Selection('TitleChinese','Title');
        	$sql = "SELECT concat('G',GroupID) as tempID, $groupNameField as tempName FROM INTRANET_GROUP WHERE GroupID IN ($groups) ORDER BY Title";
        	$result1 = $this->returnArray($sql);
        
        	$namefield = getNameFieldWithClassNumberByLang();
        	$sql = "SELECT concat('U',UserID) as tempID, $namefield as tempName FROM INTRANET_USER WHERE UserID IN ($users) ORDER BY tempName";
        	$result2 = $this->returnArray($sql);
        
        	return array_merge($result1,$result2);
        }

        function splitQuestion($qStr)
        {
                 $qSeparator = "#QUE#";
                 $pSeparator = "||";
                 $oSeparator = "#OPT#";
                 $questions = explode($qSeparator,$qStr);
                 for ($i=1; $i<sizeof($questions); $i++)
                 {
                      $que = $questions[$i];
                      $parts = explode($pSeparator,$que);
                      $temp = explode(",",$parts[0]);
                      $type = $temp[0];
                      $numOp = 0;
                      $options = "";
                      switch ($type)
                      {
                              case 1:
                                   $numOp = 2;
                                   $opStr = $parts[2];
                                   $options = explode($oSeparator,$opStr);
                                   array_shift($options);
                                   break;
                              case 2:
                              case 3:
                                   $numOp = $temp[1];
                                   $opStr = $parts[2];
                                   $options = explode($oSeparator,$opStr);
                                   array_shift($options);
                                   break;
                              case 4:
                              case 5:
                              case 6:
                                   $options = array();
                      }
                      $mainQ = $parts[1];
                      $result[] = array($type, $mainQ, $options);
                 }
                 $this->question_array = $result;
                 return $result;
        }
        function parseAnswerStr ($aStr)
        {
                 if ($aStr == "") return array();
                 $aSeparator = "#ANS#";
                 $answers = explode($aSeparator,$aStr);
                 for ($i=1; $i<sizeof($answers); $i++)
                 {
                      $qType = $this->question_array[$i-1][0];
                      if ($qType == 2 || $qType == 1)
                      {
                          $options = $this->question_array[$i-1][2];
                          $ansPart = $options[$answers[$i]];
                      }
                      else if ($qType == 3)
                      {
                          $options = $this->question_array[$i-1][2];
                          $selected = explode(",",$answers[$i]);
                          $ansPart = "";
                          $sep = "";
                          for ($j=0; $j<sizeof($selected); $j++)
                          {
                               $ansPart .= $sep . $options[$selected[$j]];
                               $sep = "; ";
                          }
                      }
                      else if ($qType == 6)
                      {
                           $ansPart = "";
                      }
                      else
                      {
                          $ansPart = $answers[$i];
                      }
                      $result[] = $ansPart;
                 }
                 return $result;
        }
        function returnTableViewAll($TeachingType="")
        {
                 $answers = $this->returnAnswerString($TeachingType);

                 # Parse answers
                 for ($i=0; $i<sizeof($answers); $i++)
                 {
                      list ($sid,$name,$type,$status,$aStr,$signer,$last, $user_identity) = $answers[$i];
                      $parsed = $this->parseAnswerStr($aStr);
                      $result[] = array($sid,$name,$type,$status,$parsed,$signer,$last, $user_identity);
                 }
                 return $result;
        }
        function returnAnswerString($TeachingType="")
        {
//                  $namefield = getNameFieldWithLoginByLang("b.");
				$namefield = getNameFieldByLang("b.");
//                  $namefield2 = getNameFieldWithLoginByLang("c.");
                 $namefield2 = getNameFieldByLang("c.");
                 global $Lang;
                 
/*                 
                 $sql = "SELECT a.UserID,
                                IF(b.UserID IS NULL,CONCAT('<I>',a.UserName,'</I>'),$namefield),
                                a.RecordType,a.RecordStatus,a.Answer,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.SignerName,'</I>'),$namefield2),
                                a.DateModified
                         FROM INTRANET_CIRCULAR_REPLY as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.SignerID = c.UserID
                         WHERE CircularID = ".$this->CircularID ."
                         ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
*/                         

				$conds = "";
				if($TeachingType!="")
				{
					if($TeachingType==1)
						$conds = " and b.Teaching = 1 ";
					else
						$conds = " and (b.Teaching = 0 or b.Teaching is null) ";
				}
                 $sql = "SELECT a.UserID,
                                IF(b.UserID IS NULL,CONCAT('<I>',a.UserName,'</I>'),$namefield),
                                a.RecordType,a.RecordStatus,a.Answer,
                                IF(c.UserID IS NULL,CONCAT('<I>',a.SignerName,'</I>'),$namefield2),
                                a.DateModified, if(b.Teaching=1, '". $Lang['Identity']['TeachingStaff'] ."', '". $Lang['Identity']['NonTeachingStaff'] ."')
                         FROM INTRANET_CIRCULAR_REPLY as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                              LEFT OUTER JOIN INTRANET_USER as c ON a.SignerID = c.UserID
                         WHERE CircularID = ".$this->CircularID ." $conds
                         ORDER BY b.EnglishName";
                 return $this->returnArray($sql,7);
        }
        function returnAllResult($TeachingType="")
        {
	        if($TeachingType=="")	 # All Staff
	        {
                 $sql = "SELECT a.Answer FROM INTRANET_CIRCULAR_REPLY as a
                         WHERE a.RecordStatus = 2 AND a.Answer != '' AND
                         a.CircularID = ".$this->CircularID."";
        	}
        	else
        	{
	        	if($TeachingType!="")
				{
					if($TeachingType==1)
						$conds = " and b.Teaching = 1 ";
					else
						$conds = " and (b.Teaching = 0 or b.Teaching is null) ";
				}
				
	        	$sql = "SELECT a.Answer FROM INTRANET_CIRCULAR_REPLY as a
	        	 		LEFT JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
                         WHERE a.RecordStatus = 2 AND a.Answer != '' AND
                         a.CircularID = ".$this->CircularID . $conds;
        	}
                 return $this->returnVector($sql);
        }


        function isEditable($circularID="") 
        {
                 global $UserID;
                 if ($circularID == "")
                 {
                     $circularID = $this->CircularID;
                 }
                 if ($circularID == "")
                 {
                     return false;
                 }
                 $this->returnRecord($circularID);

                 /*
                 $now = time();
                 $start = strtotime($this->DateStart);
                 $status = $this->RecordStatus;
                 if ($start > $now || $status == 2 || $status == 3)
                 {
                     return true;
                 }
                 return false;
                 */
                 
                 return ($this->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]==1));
                 
        }
        function isFullAdmin()
        {
                 global $UserID;
                 $sql = "SELECT AdminLevel FROM INTRANET_ADMIN_USER WHERE RecordType = 1 AND UserID = '$UserID'";
                 $temp = $this->returnVector($sql);
                 return ($temp[0]==1);
        }
        function returnRecipientNames()
        {
                 global $i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual;
                 if ($this->CircularID == "") return array("","");
                 if ($this->RecordType == 1) return array($i_Circular_RecipientTypeAllStaff,"");
                 $recipient = $this->RecipientID;
                 if ($this->RecordType == 2)     # Teaching
                 {
                     return array($i_Circular_RecipientTypeAllTeaching,"");
                 }
                 else if ($this->RecordType == 3)              # Non-teaching
                 {
                     return array($i_Circular_RecipientTypeAllNonTeaching,"");
                 }
                 else if ($this->RecordType == 4)              # Individual
                 {
                      $result = $this->returnIndividualTypeNames($recipient);
                      return array($i_Circular_RecipientTypeIndividual,implode(", ",$result));
                 }
                 return array("","");
        }

		/*
		# moved to libportal.php
        function getUnsignedCircularCount($targetUserID="", $Ended = 0)
        {
                 if ($targetUserID == "")
                 {
                     global $UserID;
                     $targetUserID = $UserID;
                 }
                 if ($targetUserID == "")
                 {
                     return 0;
                 }
                 if ($this->isLateSignAllow)
                 {
                 }
                 else
                 {
                     $conds = " AND b.DateEnd >= CURDATE()";
                 }

                 if($Ended)
                 {
                         $conds = " AND b.DateEnd >= CURDATE()";
                 }

                 $sql = "SELECT COUNT(a.CircularReplyID)
                                FROM INTRANET_CIRCULAR_REPLY as a
                                     LEFT OUTER JOIN INTRANET_CIRCULAR as b ON a.CircularID = b.CircularID
                                     WHERE a.UserID = $targetUserID AND a.RecordStatus != 2 AND
                                             b.DateStart <= CURDATE() $conds";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }
        */

        
        #########################################################################
        # Start in IP25
        #########################################################################
        /*
        * Get MODULE_OBJ array
        */
        function GET_MODULE_OBJ_ARR($From_eService = 0)
        {
                global $UserID, $plugin, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root, $top_menu_mode, $special_feature;
                
                ### wordings 
                global  $Lang;
                
                # Current Page Information init
                $PageCircular 							= 0;
                $PageCircularSettings 					= 0;
                $PageCircularSettings_BasicSettings 	= 0;
                
                switch ($CurrentPage) {
                    case "PageCircular_Index":
                    	$PageCircular = 1;
                    	$PageCircular_Index = 1;
                    	break;
                    case "PageCircular_UnsignedCircularList":
                    	$PageCircular = 1;
                    	$PageCircular_UnsignedCircularList = 1;
                    	break;
                    case "PageCircularSettings_BasicSettings":
                    	$PageCircularSettings = 1;
                    	$PageCircularSettings_BasicSettings = 1;
                    	break;
                    case "PageCircularSettings_AdminSettings":
                    	$PageCircularSettings = 1;
                    	$PageCircularSettings_AdminSettings = 1;
                    	break;
                }
                
                
                if($CurrentPageArr['eAdminCircular'])
                {
	                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/circular/";
	                
	                if(!$this->disabled && ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-eCircular'])){
						$MenuArr["Circular"] 	= array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageCircular);
						$MenuArr["Circular"]["Child"]["Index"] 	= array($Lang['Header']['Menu']['eCircular'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/circular/", $PageCircular_Index);
						
	                }
	                if($_SESSION["platform"] == "KIS" && ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-eCircular'])){
	                    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/circular/";
	                }
					if($_SESSION['SSV_USER_ACCESS']['eAdmin-eCircular'])
					{    
						$MenuArr["Circular"]["Child"]["UnsignedCircularList"] 	= array($Lang['Circular']['UnsignedCircularList'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/circular/unsignedCircularList.php", $PageCircular_UnsignedCircularList);
						$MenuArr["Settings"] 	= array($Lang['Circular']['Settings'], "#", $PageCircularSettings);
						$MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($Lang['Circular']['BasicSettings'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/circular/settings/basic_settings/", $PageCircularSettings_BasicSettings);
						$MenuArr["Settings"]["Child"]["AdminSettings"] 	= array($Lang['Circular']['AdminSettings'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/circular/settings/admin_settings/", $PageCircularSettings_AdminSettings);
					}
					
				}
				else
				{
				    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/circular/";
				    
				    $MenuArr["Circular"] 	= array($Lang['Header']['Menu']['eCircular'], $PATH_WRT_ROOT."home/eService/circular/", $PageCircular);
				    
				    if ($_SESSION["platform"] == "KIS" &&($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-eCircular'])) {
				        $MenuArr["Settings"] = array(
				            $Lang['Circular']['Settings'],
				            $PATH_WRT_ROOT . "",
				            false
				        );
				        $MenuArr["Settings"]["Child"]["eCircularAdmin"] = array(
				            $Lang['Header']['Menu']['eCircular'] . " (" . $Lang['General']['Admin'] . ")",
				            $PATH_WRT_ROOT . "home/eAdmin/StaffMgmt/circular/",
				            false
				        );
				    }
				}


                # change page web title
                $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
                $js.= 'document.title="eClass eCircular";'."\n";
                $js.= '</script>'."\n";
                
                ### module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['eCircular'].$js;
                // for kis client go to eCircular page
                if($From_eService != 1 && $_SESSION["platform"] == "KIS" && ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION['SSV_USER_ACCESS']['eAdmin-eCircular'])){
                    $MODULE_OBJ['title'] .= " (" . $Lang['General']['Admin'] . ")";
                }
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eoffice.gif";
                
                $MODULE_OBJ['menu'] = $MenuArr;
				
                return $MODULE_OBJ;
        }
        
        # 2010-01-14 [YatWoon]
        function buildReplySlipTemplates()
        {
	        $sql = "select Title, Question from INTRANET_CIRCULAR where RecordStatus=3";
			$result = $this->returnArray($sql);
			
			for($i=0;$i<sizeof($result);$i++)
			{
				$this_data = str_replace("\r\n"," ",$result[$i]['Question']);
				$this_data = str_replace("\"","\\\"",$this_data);
				//$x .= "form_templates[". $i ."] = new Array(\"". $result[$i]['Title'] ."\", \"". str_replace("\r\n"," ",$result[$i]['Question']) ."\");\n";
				$x .= "form_templates[". $i ."] = new Array(\"". $result[$i]['Title'] ."\", \"". $this_data ."\");\n";
			}
			
			return $x;
        }
        
        # 2010-02-03 YatWoon
        function retrieveCircularYears()
        {
	        $sql = "select distinct(DATE_FORMAT(DateStart,'%Y')) as StartYear from INTRANET_CIRCULAR order by StartYear";
	        $result = $this->returnVector($sql);
	        return $result;
        }
        
                
        function returnPushMessageNotificationData($startdate, $enddate, $title, $circularNumber)
        {
	        global $Lang;
	        
			$website = get_website();

			$Today = date("Y-m-d");
			if ($startdate>$Today)
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term1'];
			}
			else
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term2'];
			}
	        
	        $email_subject = $Lang['AppNotifyMessage']['eCircular']['Subject'];
	        $email_subject = str_replace("__TITLE__", $title, $email_subject);
	        $email_subject = str_replace("__NOTICENUMBER__", $circularNumber, $email_subject);
	        
	        $email_content = $Lang['AppNotifyMessage']['eCircular']['Content'];
	        $email_content = str_replace("__TITLE__", $title, $email_content);
	        $email_content = str_replace("__TERM__", $Term, $email_content);
	        $email_content = str_replace("__STARTDATE__", $startdate, $email_content);
	        $email_content = str_replace("__ENDDATE__", $enddate, $email_content);
	        $email_content = str_replace("__WEBSITE__", $website, $email_content);
	        $email_content = str_replace("__NOTICENUMBER__", $circularNumber, $email_content);
	        			
	        return array($email_subject, $email_content);
        }
  
        
        function returnEmailNotificationData($startdate, $enddate, $title, $senderEmail='')
        {
	        global $Lang;
	        
			$website = get_website();
	        $webmaster = $senderEmail ? $senderEmail : get_webmaster();

			$Today = date("Y-m-d");
			if ($startdate>$Today)
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term1'];
			}
			else
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term2'];
			}
	        
	        $email_subject = $Lang['EmailNotification']['eCircular']['Subject'];
	        $email_subject = str_replace("__TITLE__", $title, $email_subject);
	        
	        $email_content = $Lang['EmailNotification']['eCircular']['Content'];
	        $email_content = str_replace("__TITLE__", $title, $email_content);
	        $email_content = str_replace("__TERM__", $Term, $email_content);
	        $email_content = str_replace("__STARTDATE__", $startdate, $email_content);
	        $email_content = str_replace("__ENDDATE__", $enddate, $email_content);
	        $email_content = str_replace("__WEBSITE__", $website."  ", $email_content);
	        
	        $email_footer = $Lang['EmailNotification']['Footer'];
			$email_footer = str_replace("__WEBSITE__", $website."  ", $email_footer);
			$email_footer = str_replace("__WEBMASTER__", $webmaster."  ", $email_footer);
			$email_content .= $email_footer;
			
	        return array($email_subject, $email_content);
        }
        
        #########################################
        ### need update to IP25 later
        #########################################
        /*
		function isCircularAdmin($uid="")
		{
			if ($uid=="") $uid = $_SESSION['UserID'];
			
			$type = 1;		# for Circular type
			
			$sql = "SELECT RecordType FROM INTRANET_ADMIN_USER WHERE UserID = '$uid' ORDER BY RecordType";
            $jobs = $this->returnVector($sql);
            
			if (sizeof($jobs)!=0)
			{
			    return (in_array($type,$jobs));
			}
			else 
				return false;
		}
		*/
        #########################################################################

        function return_FormContent($start='', $end='', $isIssuedCircular=0, $Title='', $Description='', $Question='', $CircularNumber='', $AllFieldsReq=0, $RecordStatus=1, $circularAttFolder='', $DisplayQuestionNumber=0, $eClassTeacherApp=0, $sendTimeMode='', $sendTimeString='')
        {
        	global $linterface, $PATH_WRT_ROOT, $Lang, $cfg, $sys_custom;
// 	        global $i_Notice_NoticeNumber, $i_Notice_CurrentList, $i_Notice_Title, $i_Notice_DateStart, $i_Notice_DateEnd, $i_Notice_Description, $i_Notice_Type;
// 	        global $i_Notice_Attachment, $i_frontpage_campusmail_attach, $i_frontpage_campusmail_remove, $button_edit, $i_Survey_AllRequire2Fill;
// 	        global $i_Notice_StatusPublished, $i_Notice_StatusSuspended, $i_Notice_StatusTemplate, $i_Notice_TemplateNotes, $i_Notice_SendEmailToParent;

			global $i_Circular_CircularNumber, $i_Circular_Title, $i_Circular_CurrentList, $i_Circular_DateStart, $i_Circular_DateEnd;
			global $i_Circular_Description, $i_Circular_Attachment, $i_frontpage_campusmail_attach, $i_frontpage_campusmail_remove;
			global $i_Survey_AllRequire2Fill, $button_edit, $i_Circular_Type, $i_Circular_TemplateNotes, $i_Notice_StatusPublished;
			global $i_Notice_StatusSuspended,$i_Notice_StatusTemplate;
			global $eclassAppConfig;
			
	        $RecordStatus = $RecordStatus ? $RecordStatus : 1;
			include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$li = new libfilesystem();
			$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description));
			$objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['eCircular'], $id);
	        
			
			
	        if ($eClassTeacherApp) {
				include_once($PATH_WRT_ROOT.'includes/eClassApp/eClassAppConfig.inc.php');
				
				$nowChecked = false;
				$scheduledChecked = true;
				if($Title==''){
					
					$scheduledMessageChecked = $this->sendPushMsg? "checked":"";
				}
				$nowTime = strtotime("now");
				if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
					$sendTime = strtotime($sendTimeString);
					if ($sendTime > $nowTime) {
						$scheduledMessageChecked = "checked";
						$nowChecked = false;
						$scheduledChecked = true;
					} else {
						$scheduledMessageChecked = "";
						$sendTimeString = "";
					}
				} else {
					$sendTimeString = "";
				}

				$htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $nowChecked, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
				$htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");
				
				
				// reference: http://stackoverflow.com/questions/2480637/round-minute-down-to-nearest-quarter-hour
				$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
				$nextTimeslotHour = date('H', $rounded_seconds);
				$nextTimeslotMinute = date('i', $rounded_seconds);
				
				if ($sendTimeString == "") {
					$selectionBoxDate = date('Y-m-d', $nowTime);
					$selectionBoxHour = $nextTimeslotHour;
					$selectionBoxMinute = $nextTimeslotMinute;
				} else {
					$selectionBoxDate = date('Y-m-d', $sendTime);
					$selectionBoxHour = date('H', $sendTime);
					$selectionBoxMinute = date('i', $sendTime);
				}

				$x = '';
				$x .= "&nbsp;&nbsp;";
				$x .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
				$x .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
				$x .= " : ";
				$x .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15);
				$x .= "&nbsp;&nbsp;";

				// [2019-0905-1214-23207]
				// $x .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
                $x .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDateOnly'],'button','javascript:syncIssueTimeToPushMsg()');
				$x .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='div_push_message_date_err_msg'></span>";
				$x .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
				$htmlAry['sendTimeDateTimeDisplay'] = $x;
			}
	        
	        $x = "
				<table class=\"form_table_v30\">
					<tr>
						<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Circular_CircularNumber."</td>
						<td class='field_content_short'><input type='text' name='CircularNumber' value='". $CircularNumber ."' maxlength='50' class='textboxnum'> &nbsp;<a href=javascript:newWindow('currentlist.php',1) class='tablelink'>[". $i_Circular_CurrentList."]</a>
						<br><span id='div_CircularNumber_err_msg'></span>
						</td>
					</tr>
				
					<tr> 
						<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Circular_Title."</td>
						<td class='field_content_short'><input type='text' name='Title' maxlength='255' value='". $Title."' class='textboxtext'>
						<span id='div_Title_err_msg'></span>
						</td>
					</tr>";
			
			if($isIssuedCircular)
			{
				$x .= "	<tr valign='top'>
							<td class='field_title'>". $i_Circular_DateStart."</span></td>
							<td>$start</td>
						</tr>    
						";
			} else {
				$x .= "	<tr valign='top'>
							<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Circular_DateStart."</span></td>
							<td>". $linterface->GET_DATE_PICKER("DateStart", $start) ."
							<br><span id='div_DateStart_err_msg'></span>
							</td>
						</tr>    
						";
			}	
			
			$x .= " <tr valign='top'> 
						<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Circular_DateEnd."</span></td>
						<td>". $linterface->GET_DATE_PICKER("DateEnd", $end)."
						<br><span id='div_DateEnd_err_msg'></span>
						</td>
					</tr>   
					
					<tr valign='top'>
						<td class='field_title'>". $i_Circular_Description."</td>
						<td>". ($isIssuedCircular ? htmlspecialchars_decode($Description)."&nbsp;" :  $objHtmlEditor->CreateHtml()) ."</td>
					</tr> 
					
					<tr valign='top'>
						<td class='field_title'>". $i_Circular_Attachment."</td>
						<td>";
			$displayAttachment = $this->displayAttachmentEdit("file2delete[]", $circularAttFolder);
			$x .= "<table>". $displayAttachment ." </table>";
			$x .= "<table id='upload_file_list' border='0' cellpadding='0' cellpadding='0'>
					<tr><td><input class='file' type='file' name='filea0' size='40'>
					<input type='hidden' name='hidden_userfile_name0'></td></tr>
			
				</table>
				<input type=button value=' + ' onClick='add_field()'>";
			/*
							<table border='0' cellspacing='1' cellpadding='1' class='inside_form_table'>
							<tr>
							<td>
							<select name='Attachment[]' size='4' multiple> 
								<option>";
								for($i = 0; $i < 40; $i++) { $x .="&nbsp;"; } 
							$x .= "</option>
							</select>
							</td>
							<td>
							". $linterface->GET_BTN($i_frontpage_campusmail_attach, "button","newWindow('attach.php?folder=$circularAttFolder',2)") ."<br>
							". $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['Attachment[]'])") ."<br>
							</td>
							</tr>
							</table>
							*/
			//23-9-2016 [villa] check -> if the sendPushMsg is checked

			
			$x .="</td>
					</tr>  
					
					<tr valign='top'>
						<td class='field_title'>". $Lang['eCircular']['ReplySlipForm']."</td>
						<td>
						". ($isIssuedCircular ? "" : $linterface->GET_BTN($button_edit, "button","newWindow('editform.php',1)"))." 
						". $linterface->GET_BTN($Lang['eCircular']['Preview'], "button","newWindow('preview.php',10)")."
						<br>
						<input name='AllFieldsReq' type='checkbox' value='1' id='AllFieldsReq' ". ($isIssuedCircular ? "disabled":"") ." " . ($AllFieldsReq ? "checked":"") . "> <label for='AllFieldsReq'>". $i_Survey_AllRequire2Fill."</label> <br>
						<input name='DisplayQuestionNumber' type='checkbox' value='1' id='DisplayQuestionNumber' " . ($DisplayQuestionNumber ? "checked":"") . "> <label for='DisplayQuestionNumber'>". $Lang['eCircular']['DisplayQuestionNumber'] ."</label>
						</td>
					</tr>
					
					<tr valign='top'>
						<td class='field_title'>".$i_Circular_Type."</td>
						<td>
							<table border='0' cellpadding='0' cellspacing='0' width='100%' class='inside_form_table'>
								<tr>
									<td> 
										<input type='radio' name='status' value='1' id='status1' ". ($RecordStatus==1 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status1'>". $i_Notice_StatusPublished."</label><br />
										";
			if($eClassTeacherApp){
									$x.=	"<span style='margin-left:20px; ".($eClassTeacherApp ? "" : "display: none;")."'>
											<input type='checkbox' name='pushmessagenotify' value='1' id='pushmessagenotify'  onclick='checkedSendPushMessage(this);' $scheduledMessageChecked ".($RecordStatus==1 ? "":"disabled")."> <label for='pushmessagenotify'>". $Lang['eCircular']['NotifyStaffByPushMessage']."</label><br/>
											</span>
											<div id = 'PushMessageSetting'>".$linterface->Get_Warning_Message_Box($Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting'],
												"<div id='sendTimeSettingDiv'>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeNowRadio']."
													<br />
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeScheduledRadio']."
													<br />
													<div id='specificSendTimeDiv' style='".($scheduledChecked ? "" : "display: none;")."'>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeDateTimeDisplay']."
													</div>
												</div></div>")."";
			}
			$allowEmailNotify = true;
			if ($sys_custom['project']['Amway'] || $sys_custom['DHL']) {
				$allowEmailNotify = false;
			}
			if ($allowEmailNotify) {
										$x .="<span style='margin-left:20px;'>
											<input type='checkbox' name='emailnotify' value='1' id='emailnotify' ".($RecordStatus==1 ? "":"disabled")."> <label for='emailnotify'>". $Lang['eCircular']['NotifyStaffByEmail']."</label><br>
										</span>
										<span style='margin-left:20px;'>(
										".$Lang['eCircular']['NotifyEmailRemark']."
										)</span>";
			}
									$x .= "</td>
								</tr>
								<tr>
									<td>
										<input type='radio' name='status' value='2' id='status2' ". ($RecordStatus==2 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status2'>". $i_Notice_StatusSuspended."</label><br />
									</td>
								</tr>
								<tr>
									<td>
										<input type='radio' name='status' value='3' id='status3' ". ($RecordStatus==3 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status3'>". $i_Notice_StatusTemplate."</label><br />". $i_Circular_TemplateNotes."<br />
									</td>
								</tr>
							</table>
						</td>
					</tr>	
					
				</table>
				
				<input type='hidden' name='qStr' value='". $Question. "'>
				<input type='hidden' name='aStr' value=''>
			";
			//2016-09-27	Villa add the clickon function of smallBtn
			$x .= "<script>
						function syncIssueTimeToPushMsg(){
							var issueDate = $('#DateStart').val();						
							$('#sendTime_date').val(issueDate);;
						}
					</script>";
			
			return $x;
        }
        
        
        function displayAttachmentEdit($name, $attachment_folder)	
        {
			global $file_path, $image_path;
			
			$path = "$file_path/file/circular/$attachment_folder";

			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			
			$x1 = "";
			
			while (list($key, $value) = each($files)) 
			{	                 
				$url = str_replace($file_path, "", $path)."/".$files[$key][0];
                $x1 .= "<tr><td width=\"1\"><input type=\"checkbox\" name=\"$name\" value=\"".urlencode($files[$key][0])."\"  onClick=\"setRemoveFile($key,this.checked)\"></td>";
                $x1 .="<td id=\"a_".$key."\" class=\"tabletext\"><img src=\"$image_path/file.gif\" hspace=\"2\" vspace=\"2\" border=\"0\" align=\"absmiddle\">";
                
                $dl_url = str_replace($file_path, "", $path)."/".$files[$key][0];
	        	$dl_url = $file_path.$url;
	        	
				$x1 .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target_e=".getEncryptedText($dl_url)."\" >".$files[$key][0]."</a>";
                
                $x1 .= " (".ceil($files[$key][1]/1000)."Kb)";
			}
			$x1 .= "</tr>\n";
             return $x1;
        
		}
		
		function genAttachmentFolderName()
		{
			//$t = time();
			$t = date("YmdHis");
			do{
				$fn = session_id().".". $t;
				# check the foler name is exists or not
				$sql = "select count(*) from INTRANET_CIRCULAR where Attachment='". $fn ."'";
				$result = $this->returnVector($sql);
				$t++;
			}while ($result[0] > 0);
			
			return $fn;			
		}
		
        
        }        // End of class definition

}        // End of directive
?>