<?php
// page modifing by :  

############# Change Log [Start] ################
#
#   Date    :   2020-09-18 [Bill]
#               Modified user_right_target(), add Power Portfolio
#
#   Date    :   2020-06-16 [Cameron]
#               Modified user_right_target(), show OrganizationInfo if $sys_custom['SchoolSettings']['OrganizationInfo'] is true or $intranet_version > 2.5
#               Reorder eGuidance position according to English Name of the module
#
#   Date    :   2020-03-31 [Cameron]
#               Modified user_right_target(), show OrganizationInfo if $sys_custom['project']['CourseTraining']['IsEnable'] is true
#
#   Date    :   2020-03-12 [Henry]
#               Modified user_right_target(), hide photo ablum if $sys_custom['KIS_HidePhotoAlbum'] is true
#
#   Date    :   2019-08-12 [Henry]
#               Modified user_right_target(), hide photo ablum if digital channels is installed
#
#   Date    :   2019-05-13 [Anna]
#               Added Get_Safe_Sql_Query to avoid sql injection
#
#	Date	:	2017-10-17 [Siuwan]
#				Modified user_right_target(), added PowerLesson2 in eLearning
#
#	Date	:	2017-08-17 [Carlos]
#				Modified user_right_target(), define $hide_modules array to hide certain modules.
#
#	Date	:	2017-05-31 [Frankie]
#				Added eForm admin settings in eAdmin
#
#	Date	:	2017-04-11 [Carlos]
#				Added back plugin flag checking for eClassXXXApp in user_right_target().
#
#	Date	:	2017-03-17 [Cameron]
#				Modified user_right_target(), add eGuidance
#
#	Date	:	2016-12-19 [Frankie] 
#				Modified user_right_target(), add TeacherPortfolio
#
#	Date	:	2016-10-12 [Ivan] 
#				Modified user_right_target(), add eAppraisal
#
#	Date	:	2016-05-16 [Ivan] 
#				Modified user_right_target(), add SLRS
#
#	Date	:	2016-04-18 [Omas] 
#				Modified user_right_target(), add ePCM
#
#	Date	:	2016-02-17 [Pun] [ip.2.5.7.3.1] [90826]
#				Modified user_right_target(), changed Student Analysis Data System access right 
#
#	Date	:	2015-06-10 (Siuwan) [ip.2.5.6.7.1]
#				add FlippedChannels admin settings in eAdmin
#
#	Date	:	2015-05-26 (Carlos)
#				added Get_All_Mail_Target_Users() to retrieve all available users restricted with its mail targets settings.
#
#	Date	:	2014-11-11 (Pun)
#				add cust printing module for mssch admin settings in eAdmin
#
#	Date	:	2014-10-09 (Henry)
#				add DigitalChannels admin settings in eAdmin
#				deploy: IPv10.1
#
#	Date	:	2014-09-03 (YatWoon)
#				hide "iCalendar" role admin settings [Case#Y50926]
#				deploy: IPv10.1
#
#	Date	:	2014-07-08 (Ivan)
#				add eClass App (Teacher) admin settings in eAdmin
#
#	Date	:	2014-04-24 (Ivan)
#				add eClass App admin settings in eAdmin
#
#	Date	:	2014-02-14 (Henry)
#				add PhotoAlbum admin module for KIS
#
#	Date	:	2013-12-31 (Ivan)
#				added $plugin['eClassApp'] for push message center and set different name display for KIS
#
#	Date	:	2013-12-15 (fai)
#				add Medical module eAdmin > Student Management > Medical Caring System
#
#	Date	:	2013-04-11 (Siuwan)
#				add ePost admin module eLearning > ePost 
#
#	Date	:	2013-04-11 (Rita)
#				add Subject eResources admin module
#
#	Date	:	2013-01-04 (Yuen)
#				disable ePayment if special setting $sys_custom['payment_disabled_in_role'] is on.
#
#	Date	:	2012-10-08 (Rita)
#				added Doc routing admin module
#
#	Date	:	2012-08-28 (YatWoon)
#				added Others > Update student password
#
#	Date	:	2012-08-14 (Carlos)
#				added eAdmin > Student Mgmt > Class Diary
#
#	Date	:	2012-06-26 (Ivan)
#				added eAdmin > Resource Mgmt > Library Management System
#
#	Date	:	2012-06-19 (Henry Chow)
#				added eLearning > SBA
#
#	Date	:	2012-06-13 (Ivan) [2012-0611-1522-01071]
#				modified to show one "eReportCard" row only if the client has enabled eRC1.0 only
#
#	Date	:	2011-12-05 (Ivan)
#				added eLearning > Writing 2.0
#
#	Date	:	2011-10-20 (Carlos)
#				added Alumni mail target
#
#	Date	:	2011-08-30 (Henry Chow)
#				comment the choice of "Invoice Mgmt System Admin"
#
#	Date	:	2011-08-18 (Marcus)
#				Added eAdmin > General Mgmt > Message Center (EmailMerge and SMS)
#
#	Date	:	2011-08-15 (Henry Chow)
#				Added eAdmin > Resources Mgmt > Invoice Mgmt System (cust)
#
#	Date	:	2011-06-02 (Henry Chow)
#				modified Load_User_Right(), revise the SQL
#
#	Date	:	2011-05-31 (Thomas)
#				added eLearning > iTextbook
#
#	Date	:	2011-05-27 (Henry Chow)
#				added eAdmin > Digital Archive
#
#	Date	:	2011-02-16 (Henry Chow)
#				combine the "Account Mgmt" into 1 again
#
#	Date	:	2011-02-11 (Henry Chow)
#				Split "Account Mgmt" into 3 differen rights 
#
#	Date	:	2010-12-30 (Marcus)
#				Added eLearning > Reading Scheme 
#
#	Date	:	2010-12-08 (Carlos)
#				Added eAdmin > Account Mgmt (Shared MailBox)
#
#	Date	:	2010-10-08 (Kenneth Chung)
#				Optimized Function Load_User_Right/Load_User_Target to reduce sql loading
#
#	Date	:	2010-09-21 (Ivan)
#				Added eAdmin > eReportCard 1.0 Admin
#
#	Date	:	2010-07-13 (Henry Chow)
#				Added eAdmin > Account Mgmt (Student Registry)
#
#	Date	:	2010-06-25 (Henry Chow)
#				Added eLearning > IES Admin
#
#	Date	:	2010-06-21 (Ivan)
#				Added eAdmin > eReportCard (Rubrics) Admin
#
#	Date	:	2010-05-20 (Henry Chow)
#				Add School Settings option in Role > Module Administration > Account Management
#
#	Date	:	2010-04-14 (Henry)
#				Add School Settings option in Role > Module Administration > Repair System
#
#	Date	:	2009-12-15 YatWoon
#				Add School Settings option in Role > Module Administration
############# Change Log [End] ################

include_once("libdb.php");
include_once("role_manage.php");

class user_right_target extends libdb{
	var $SysFunctionList;
	var $Target;

  function user_right_target(){
  	global $Lang, $plugin, $special_feature, $module_version, $sys_custom, $intranet_version;
  	
  	//debug_pr($special_feature);
  	$isKIS = $_SESSION["platform"]=="KIS";
  	$hide_modules = array();
  	if($sys_custom['LivingHomeopathy']){
  		$hide_modules = array('Campus','Timetable','AccountMgmt_StudentRegistry','SharedMailBox','DigitalArchive','DocRouting','DigitalChannels','eBooking',
								'eCircular','eClassApp','eClassStudentApp','eClassTeacherApp','eDiscipline','eInventory','LibraryMgmtSystem','EmailMerge','ePayment',
								'ePOS','ePolling','ePCM','eReportCard1.0','eReportCard','eReportCard_Rubrics','eReportCardKindergarten','eSchoolBus','eSportsAdmin',
								'eSurvey','LessonAttendance','PushMessageCenter','RepairSystem','SLRS','eForm','sms','StaffAttendance','StudentAttendance',
								'StudentAppNotify','TeacherAppNotify','eAppraisal','eClass','IES','ReadingScheme','SBA','W2','SubjecteResources','iTextbook','ePost',
								'FlippedChannels','eAdmission','PhotoAlbum','eSchoolPad_MDM','medical','msschPrinting','eGuidance','campusLink','StudentDataAnalysisSystem',
								'UpdateStudentPwdPopUp','TeacherPortfolio', 'PowerLesson2', 'PowerPortfolio');
  	}
  	else if (isset($sys_custom['HideModuleAry']) && is_array($sys_custom['HideModuleAry'])) {
  	    $hide_modules = $sys_custom['HideModuleAry'];
  	}
  	
  	if (!($sys_custom['project']['CourseTraining']['IsEnable'] || $sys_custom['SchoolSettings']['OrganizationInfo'] || (float)$intranet_version > 2.5)) {
  	    $hide_modules[] = 'OrganizationInfo';        // show this for customized portal (CourseTraining)
  	}
  	
  	$this->libdb();
    //debug_r($_SESSION);
    
    //$this->SysFunctionList[Section][Module] = array($ModuleName, $DefaultEnabledFlag, $ModulePurchasedFlag);
    
    ## School Settings
    if(!in_array('Campus',$hide_modules)) $this->SysFunctionList['SchoolSettings']['Campus'] = array($Lang['Header']['Menu']['Site'], false, true);
    if(!in_array('Class',$hide_modules)) $this->SysFunctionList['SchoolSettings']['Class'] = array($Lang['Header']['Menu']['Class'], false, true);
    $groupLang = ($sys_custom['DHL'])? $Lang['SysMgr']['RoleManagement']['OrganizationAdmin'] : $Lang['Header']['Menu']['Group'];
    if(!in_array('Group',$hide_modules)) $this->SysFunctionList['SchoolSettings']['Group'] = array($groupLang, false, true);
    if(!in_array('OrganizationInfo',$hide_modules)) $this->SysFunctionList['SchoolSettings']['OrganizationInfo'] = array($Lang['Header']['Menu']['OrganizationInfo'], false, true);
    if(!in_array('SchoolCalendar',$hide_modules)) $this->SysFunctionList['SchoolSettings']['SchoolCalendar'] = array($Lang['Header']['Menu']['SchoolCalendar'], false, true);
    if(!in_array('Subject',$hide_modules)) $this->SysFunctionList['SchoolSettings']['Subject'] = array($Lang['Header']['Menu']['Subject'], false, true);
    if(!in_array('Timetable',$hide_modules)) $this->SysFunctionList['SchoolSettings']['Timetable'] = array($Lang['Header']['Menu']['Timetable'], false, true);

    
    /* ------ Access Right ------*/
    if(!in_array('AccountMgmt',$hide_modules)) $this->SysFunctionList['eAdmin']['AccountMgmt'] = array($Lang['SysMgr']['RoleManagement']['AccountMgmt'].$Lang['AccountMgmt']['Menu_ThreeAccMgmt'], false, true);
    /*
    $this->SysFunctionList['eAdmin']['AccountMgmt_Parent'] = array($Lang['SysMgr']['RoleManagement']['AccountMgmt'].$Lang['AccountMgmt']['Menu_ParentMgmt'], false, true);
    $this->SysFunctionList['eAdmin']['AccountMgmt_Staff'] = array($Lang['SysMgr']['RoleManagement']['AccountMgmt'].$Lang['AccountMgmt']['Menu_StaffMgmt'], false, true);
    $this->SysFunctionList['eAdmin']['AccountMgmt_Student'] = array($Lang['SysMgr']['RoleManagement']['AccountMgmt'].$Lang['AccountMgmt']['Menu_StudentMgmt'], false, true);
    */
    if($plugin['AccountMgmt_StudentRegistry'] && !in_array('AccountMgmt_StudentRegistry',$hide_modules))
    	$this->SysFunctionList['eAdmin']['AccountMgmt_StudentRegistry'] = array($Lang['SysMgr']['RoleManagement']['AccountMgmt'].$Lang['AccountMgmt']['Menu_StudentRegistry'], false, true);
    if($plugin['imail_gamma']==true && !in_array('SharedMailBox',$hide_modules))
    	$this->SysFunctionList['eAdmin']['SharedMailBox'] = array($Lang['SysMgr']['RoleManagement']['AccountMgmt'].$Lang['AccountMgmt']['Menu_SharedMailBox'], false, $plugin['imail_gamma']);
    if($plugin['digital_archive'] && !in_array('DigitalArchive',$hide_modules)){
    	$this->SysFunctionList['eAdmin']['DigitalArchive'] = array($Lang['SysMgr']['RoleManagement']['DigitalArchive'], false, $plugin['digital_archive']);
    }
  	
  	# Document routing
	if($plugin['DocRouting'] && !in_array('DocRouting',$hide_modules)){
		$this->SysFunctionList['eAdmin']['DocRouting'] = array($Lang['SysMgr']['RoleManagement']['DocRouting'], false,  $plugin['DocRouting']);
	}
	# DigitalChannels
  	if($plugin['DigitalChannels'] && !in_array('DigitalChannels',$hide_modules)){
		$this->SysFunctionList['eAdmin']['DigitalChannels'] = array($Lang['SysMgr']['RoleManagement']['DigitalChannels'], false,  $plugin['DigitalChannels']);
	}
	if(!in_array('eBooking',$hide_modules)) $this->SysFunctionList['eAdmin']['eBooking'] = array($Lang['SysMgr']['RoleManagement']['eBookingAdmin'], false, $plugin['eBooking']);
    if(!in_array('eCircular',$hide_modules)) $this->SysFunctionList['eAdmin']['eCircular'] = array($Lang['SysMgr']['RoleManagement']['eCircularAdmin'], false, $special_feature['circular']);
    if($plugin['eClassApp'] && !in_array('eClassApp',$hide_modules)){
    	$this->SysFunctionList['eAdmin']['eClassApp'] = array($Lang['SysMgr']['RoleManagement']['eClassAppAdmin'], false, $plugin['eClassApp']);
    }
    if($plugin['eClassStudentApp'] && !in_array('eClassStudentApp',$hide_modules)){
    	$this->SysFunctionList['eAdmin']['eClassStudentApp'] = array($Lang['SysMgr']['RoleManagement']['eClassStudentAppAdmin'], false, $plugin['eClassStudentApp']);
    }
    if($plugin['eClassTeacherApp'] && !in_array('eClassTeacherApp',$hide_modules)){
    	$this->SysFunctionList['eAdmin']['eClassTeacherApp'] = array($Lang['SysMgr']['RoleManagement']['eClassTeacherAppAdmin'], false, $plugin['eClassTeacherApp']);
    }
	if(!in_array('eDiscipline',$hide_modules)) $this->SysFunctionList['eAdmin']['eDiscipline'] = array($Lang['SysMgr']['RoleManagement']['eDisciplineAdmin'], false, $plugin['Disciplinev12']); // eDis Admin
	if(!in_array('eEnrolment',$hide_modules)) $this->SysFunctionList['eAdmin']['eEnrolment'] = array($Lang['SysMgr']['RoleManagement']['eEnrolmentAdmin'], false, $plugin['eEnrollment']);

    # eGuidance
    if ($plugin['eGuidance'] && !in_array('eGuidance',$hide_modules)) {
        $this->SysFunctionList['eAdmin']['eGuidance'] = array($Lang['SysMgr']['RoleManagement']['eGuidanceAdmin'], false, $plugin['eGuidance']);
    }

	if(!in_array('eHomework',$hide_modules)) $this->SysFunctionList['eAdmin']['eHomework'] = array($Lang['SysMgr']['RoleManagement']['eHomework'], false, true);
	if(!in_array('eInventory',$hide_modules)) $this->SysFunctionList['eAdmin']['eInventory'] = array($Lang['SysMgr']['RoleManagement']['eInventoryAdmin'], false, $plugin['Inventory']);
	if(!in_array('LibraryMgmtSystem',$hide_modules)) $this->SysFunctionList['eAdmin']['LibraryMgmtSystem'] = array($Lang['Header']['Menu']['LibraryMgmtSystem'], false, $plugin['library_management_system']);
		
		if($plugin['EmailMerge'] && !in_array('EmailMerge',$hide_modules))
			$this->SysFunctionList['eAdmin']['EmailMerge'] = array($Lang['SysMgr']['RoleManagement']['EmailMerge'], false, $plugin['EmailMerge']);
		
	if(!in_array('eNotice',$hide_modules)) $this->SysFunctionList['eAdmin']['eNotice'] = array($Lang['SysMgr']['RoleManagement']['eNoticeAdmin'], false, $plugin['notice']);
	if(!in_array('ePayment',$hide_modules)) $this->SysFunctionList['eAdmin']['ePayment'] = array($Lang['SysMgr']['RoleManagement']['ePayment'], false, (!$sys_custom['payment_disabled_in_role'] && $plugin['payment']));
	if(!in_array('ePOS',$hide_modules)) $this->SysFunctionList['eAdmin']['ePOS'] = array($Lang['SysMgr']['RoleManagement']['ePOS'], false, ($plugin['payment'] && $plugin['ePOS']));
	if(!in_array('ePolling',$hide_modules)) $this->SysFunctionList['eAdmin']['ePolling'] = array($Lang['SysMgr']['RoleManagement']['ePolling'], false, true);
	if(!in_array('ePCM',$hide_modules)) $this->SysFunctionList['eAdmin']['ePCM'] = array($Lang['SysMgr']['RoleManagement']['ePCM'], false, $plugin['ePCM']);
		
		# eRC 1.0 (for one client only)
		if ($plugin['ReportCard']==true && !in_array('eReportCard1.0',$hide_modules))
			$this->SysFunctionList['eAdmin']['eReportCard1.0'] = array($Lang['SysMgr']['RoleManagement']['eReportCardAdmin'], false, $plugin['ReportCard']);
			
		# eRC
		// show only if eRC1.0 is not enabled
		if ($plugin['ReportCard']==false && !in_array('eReportCard',$hide_modules)) {
			$this->SysFunctionList['eAdmin']['eReportCard'] = array($Lang['SysMgr']['RoleManagement']['eReportCardAdmin'], false, $plugin['ReportCard2008']);
		}
		
		# eRC Rubrics (for one client only)
		if ($plugin['ReportCard_Rubrics']==true && !in_array('eReportCard_Rubrics',$hide_modules))
			$this->SysFunctionList['eAdmin']['eReportCard_Rubrics'] = array($Lang['SysMgr']['RoleManagement']['eReportCard_Rubrics_Admin'], false, $plugin['ReportCard_Rubrics']);
			
		if ($plugin['ReportCardKindergarten'] && !in_array('eReportCardKindergarten',$hide_modules)) {
			$this->SysFunctionList['eAdmin']['eReportCardKindergarten'] = array($Lang['SysMgr']['RoleManagement']['eReportCardKindergarten_Admin'], false, $plugin['ReportCardKindergarten']);
		}
		
		if ($plugin['eSchoolBus'] && !in_array('eSchoolBus',$hide_modules)) {
			$this->SysFunctionList['eAdmin']['eSchoolBus'] = array($Lang['Header']['Menu']['eSchoolBus'], false, $plugin['eSchoolBus']);
		}
		
		if(!in_array('eSportsAdmin',$hide_modules)) $this->SysFunctionList['eAdmin']['eSportsAdmin'] = array($Lang['SysMgr']['RoleManagement']['eSportsAdmin'], false, ($plugin['Sports'] || $plugin['swimming_gala']));
		//if($_SESSION['SSV_PRIVILEGE']['plugin']['ResourcesBooking'])
		//{
		//	$this->SysFunctionList['eAdmin']['ResourcesBooking'] = array($Lang['SysMgr']['RoleManagement']['ResourcesBooking'],false);
		//}
		//$this->SysFunctionList['eAdmin']['eAttendance'] = array($Lang['SysMgr']['RoleManagement']['eAttendanceAdmin'], false, $plugin['attendancestaff']);
		if(!in_array('eSurvey',$hide_modules)) $this->SysFunctionList['eAdmin']['eSurvey'] = array($Lang['SysMgr']['RoleManagement']['eSurveyAdmin'], false, true);
		/* commented by henry chow on 20110830 (set "Inventory Admin" = "Invoice Mgmt System Admin" in login page at current stage) 
		if($sys_custom['Invoice2Inventory'] && $plugin['Inventory']) {
			$this->SysFunctionList['eAdmin']['InvoiceMgmtSystem'] = array($Lang['SysMgr']['RoleManagement']['InvoiceMgmtSystem'], false, ($sys_custom['Invoice2Inventory'] && $plugin['Inventory']));
		}
		*/
		if(!in_array('LessonAttendance',$hide_modules)) $this->SysFunctionList['eAdmin']['LessonAttendance'] = array($Lang['SysMgr']['RoleManagement']['LessonAttendance'], false, $plugin['attendancelesson']);
		
		if(($plugin['ASLParentApp'] || $plugin['eClassApp']) && !in_array('PushMessageCenter',$hide_modules)) {
			$moduleTitle = ($isKIS)? $Lang['SysMgr']['RoleManagement']['PushMessageCenter'] : $Lang['SysMgr']['RoleManagement']['ParentAppNotify'];
			$this->SysFunctionList['eAdmin']['ParentAppNotify'] = array($moduleTitle, false, ($plugin['ASLParentApp'] || $plugin['eClassApp']));
		}
			
		if(isset($plugin['RepairSystem']) && $plugin['RepairSystem'] && !in_array('RepairSystem',$hide_modules))
			$this->SysFunctionList['eAdmin']['RepairSystem'] = array($Lang['SysMgr']['RoleManagement']['RepairSystem'], false, true);
			
		if(!in_array('SLRS',$hide_modules)) $this->SysFunctionList['eAdmin']['SLRS'] = array($Lang['SysMgr']['RoleManagement']['SLRSAdmin'], false, $plugin['SLRS']);
		
		if(isset($plugin['eForm']) && $plugin['eForm'] && !in_array('eForm',$hide_modules)) {
			$this->SysFunctionList['eAdmin']['eForm'] = array($Lang['SysMgr']['RoleManagement']['eForm'], false, $plugin['eForm']);
		}
		
		if($plugin['sms'] && !in_array('sms',$hide_modules))
			$this->SysFunctionList['eAdmin']['SMS'] = array($Lang['SysMgr']['RoleManagement']['SMS'], false, $plugin['sms']);
		if(!in_array('StaffAttendance',$hide_modules)) $this->SysFunctionList['eAdmin']['StaffAttendance'] = array($Lang['SysMgr']['RoleManagement']['StaffAttendance'], false, ($plugin['attendancestaff'] && $module_version['StaffAttendance'] == 3.0));
		if(!in_array('StudentAttendance',$hide_modules)) $this->SysFunctionList['eAdmin']['StudentAttendance'] = array($Lang['SysMgr']['RoleManagement']['StudentAttendance'], false, $plugin['attendancestudent']);
		//if($plugin['ClassDiary']){
		//	$this->SysFunctionList['eAdmin']['ClassDiary'] = array($Lang['SysMgr']['RoleManagement']['ClassDiary'], false, $plugin['ClassDiary']);
		//}
		if($plugin['eClassStudentApp'] && !in_array('StudentAppNotify',$hide_modules)){
			$this->SysFunctionList['eAdmin']['StudentAppNotify'] = array($Lang['SysMgr']['RoleManagement']['StudentAppNotify'], false, $plugin['eClassStudentApp']);
		}
		if(!in_array('eAppraisal',$hide_modules)) $this->SysFunctionList['eAdmin']['eAppraisal'] = array($Lang['SysMgr']['RoleManagement']['eAppraisalAdmin'], false, $plugin['eAppraisal']);
		if($plugin['eClassTeacherApp'] && !in_array('TeacherAppNotify',$hide_modules)){
			$this->SysFunctionList['eAdmin']['TeacherAppNotify'] = array($Lang['SysMgr']['RoleManagement']['TeacherAppNotify'], false, $plugin['eClassTeacherApp']);
		}
		
		if(!in_array('eClass',$hide_modules)) $this->SysFunctionList['eLearning']['eClass'] = array($Lang['SysMgr']['RoleManagement']['eClassAdmin'], false, true);
		
		if($plugin['IES'] && !in_array('IES',$hide_modules))
			$this->SysFunctionList['eLearning']['IES'] = array($Lang['SysMgr']['RoleManagement']['IES_Admin'], false, true);
		if($plugin['ReadingScheme'] && !in_array('ReadingScheme',$hide_modules))
			$this->SysFunctionList['eLearning']['ReadingScheme'] = array($Lang['SysMgr']['RoleManagement']['ReadingScheme'], false, true);
		if(count($plugin['SBA'])>0 && !in_array('SBA',$hide_modules))
			$this->SysFunctionList['eLearning']['SBA'] = array($Lang['SysMgr']['RoleManagement']['SBA_Admin'], false, true);
		
		# Subject eResources
	  	if($sys_custom['subject_resources'] && !in_array('SubjecteResources',$hide_modules)){
	  		$this->SysFunctionList['eLearning']['SubjecteResources'] = array($Lang['SysMgr']['RoleManagement']['SubjecteResources'], false, $sys_custom['subject_resources']); 		
	  	}
		
		if($plugin['iTextbook'] && !in_array('iTextbook',$hide_modules))
			$this->SysFunctionList['eLearning']['iTextbook'] = array($Lang['SysMgr']['RoleManagement']['iTextbook'], false, true);
		
		if(!in_array('W2',$hide_modules)) $this->SysFunctionList['eLearning']['W2'] = array($Lang['Header']['Menu']['Writing'], false, $plugin['W2']);
  	
		# ePost
	  	if(isset($plugin['ePost']) && $plugin['ePost'] && !in_array('ePost',$hide_modules)){
	  		$this->SysFunctionList['eLearning']['ePost'] = array($Lang['SysMgr']['RoleManagement']['ePostAdmin'], false, true); 		
	  	}	
		# FlippedChannels
	  	if($plugin['FlippedChannels'] && !in_array('FlippedChannels',$hide_modules)){
			$this->SysFunctionList['eLearning']['FlippedChannels'] = array($Lang['SysMgr']['RoleManagement']['FlippedChannels'], false,  $plugin['FlippedChannels']);
		}

        # PowerLesson2
        if($plugin['power_lesson_2'] && !in_array('PowerLesson2',$hide_modules)){
            $this->SysFunctionList['eLearning']['PowerLesson2'] = array($Lang['SysMgr']['RoleManagement']['PowerLesson2'], false,  $plugin['power_lesson_2']);
        }
        
		# Admission
	  	if(isset($plugin['eAdmission']) && $plugin['eAdmission'] && !in_array('eAdmission',$hide_modules)){
	  		$this->SysFunctionList['eAdmin']['eAdmission'] = array($Lang['SysMgr']['RoleManagement']['eAdmission'], false, true); 		
	  	}
	  	# Photo Album		
		if($isKIS && !in_array('PhotoAlbum',$hide_modules) && !$sys_custom['KIS_HidePhotoAlbum'] && !($plugin['DigitalChannels'] && !in_array('DigitalChannels',$hide_modules))){
			$this->SysFunctionList['eAdmin']['PhotoAlbum'] = array($Lang['SysMgr']['RoleManagement']['PhotoAlbum'], false, true);
		}
		
		# MDM
		if(isset($plugin['eSchoolPad_MDM']) && $plugin['eSchoolPad_MDM'] && !in_array('eSchoolPad_MDM',$hide_modules)){
			$this->SysFunctionList['eAdmin']['eSchoolPad_MDM'] = array($Lang['Header']['Menu']['MDM'], false, $plugin['eSchoolPad_MDM']);
		}
		
		# Medical
	  	if(isset($plugin['medical']) && $plugin['medical'] && !in_array('medical',$hide_modules)){
	  		$this->SysFunctionList['eAdmin']['medical'] = array($Lang['Header']['Menu']['Medical'], false, true); 		
	  	}		
		
		# Mssch Printing
	  	if(isset($plugin['mssch_printing_module']) && $plugin['mssch_printing_module'] && !in_array('msschPrinting',$hide_modules)){
	  		$this->SysFunctionList['eAdmin']['msschPrinting'] = array($Lang['Header']['Menu']['msschPrinting'], false, true); 		
	  	}

	  	# Power Portfolio (for KIS)
        if ($isKIS && isset($plugin['PowerPortfolio']) && $plugin['PowerPortfolio'] && !in_array('PowerPortfolio',$hide_modules)) {
            $this->SysFunctionList['eAdmin']['PowerPortfolio'] = array($Lang['SysMgr']['RoleManagement']['PowerPortfolio'], false, $plugin['PowerPortfolio']);
        }

		if(!in_array('campusLink',$hide_modules)) $this->SysFunctionList['other']['campusLink'] = array($Lang['SysMgr']['RoleManagement']['campusLink'], false, true);
		//$this->SysFunctionList['other']['iCalendar'] = array($Lang['SysMgr']['RoleManagement']['iCalendar'], false, true);

		if (isset($plugin['iPortfolio']) && $plugin['iPortfolio'] && !in_array('iPortfolio',$hide_modules)) {  // comment out on 20120802
			$this->SysFunctionList['other']['iPortfolio'] = array($Lang['SysMgr']['RoleManagement']['iPortfolioAdmin'], false, true);
		}

		if (isset($plugin['StudentDataAnalysisSystem']) && $plugin['StudentDataAnalysisSystem'] && !in_array('StudentDataAnalysisSystem',$hide_modules)) {
			$sdasSkin = ($plugin['StudentDataAnalysisSystem_Style'])?$plugin['StudentDataAnalysisSystem_Style']:'catholic';
			$this->SysFunctionList['other']['SDAS'] = array($Lang['SysMgr']['RoleManagement']['sdasAdmin'][$sdasSkin], false, true);
		}

		if(!in_array('schoolNews',$hide_modules)) $this->SysFunctionList['other']['schoolNews'] = array($Lang['SysMgr']['RoleManagement']['schoolNews'], false, true);	
		
		# Update student password function (pop-up in portal)
		if($special_feature['UpdateStudentPwdPopUp'] && !in_array('UpdateStudentPwdPopUp',$hide_modules))
		{
			$this->SysFunctionList['other']['UpdateStudentPwdPopUp'] = array($Lang['SysMgr']['RoleManagement']['UpdateStudentPwdPopUp'], false, true);	
		}
		
		if (isset($plugin['TeacherPortfolio']) && $plugin['TeacherPortfolio'] && !in_array('TeacherPortfolio',$hide_modules)) {
			$this->SysFunctionList['other']['TeacherPortfolio'] = array($Lang['SysMgr']['RoleManagement']['TeacherPortfolio'], false, $plugin['TeacherPortfolio']);
		}
		
		/*
		$this->SysFunctionList['eLearning'] = array("ARR",false);
		$this->SysFunctionList['eService'] = array("ARR",false);
		$this->SysFunctionList['SchoolSettings'] = array("ARR",false);
		*/
		
		/* ------ End Access Right ------ */
		
		/* ------ Email/ Active Event Target ------ */
		// All
		$this->Target['All']['Yes'] = array('',false); 
		$this->Target['All']['No'] = array('',false); 
		
		// staff
		$this->Target['Staff']['AllTeaching'] = array($Lang['SysMgr']['RoleManagement']['AllTeachingStaff'],false); 
		$this->Target['Staff']['AllNonTeaching'] = array($Lang['SysMgr']['RoleManagement']['AllSupportStaff'],false); 
		$this->Target['Staff']['MyForm'] = array($Lang['SysMgr']['RoleManagement']['MyFormParent'],false); 
		$this->Target['Staff']['MyClass'] = array($Lang['SysMgr']['RoleManagement']['MyClassStaff'],false); 
		$this->Target['Staff']['MySubject'] = array($Lang['SysMgr']['RoleManagement']['MySubject'],false); 
		$this->Target['Staff']['MySubjectGroup'] = array($Lang['SysMgr']['RoleManagement']['MySubjectGroupStaff'],false); 
		$this->Target['Staff']['MyGroup'] = array($Lang['SysMgr']['RoleManagement']['MyGroupStaff'],false); 
		$this->Target['NonTeaching']['MyGroup'] = array($Lang['SysMgr']['RoleManagement']['MyGroupNonTeaching'],false); 
		
		// student 
		$this->Target['Student']['All'] = array($Lang['SysMgr']['RoleManagement']['AllStudent'],false); 
		$this->Target['Student']['MyForm'] = array($Lang['SysMgr']['RoleManagement']['MyFormStudent'],false); 
		$this->Target['Student']['MyClass'] = array($Lang['SysMgr']['RoleManagement']['MyClassStudent'],false); 
		$this->Target['Student']['MySubject'] = array($Lang['SysMgr']['RoleManagement']['MySubject'],false); 
		$this->Target['Student']['MySubjectGroup'] = array($Lang['SysMgr']['RoleManagement']['MySubjectGroupStudent'],false); 
		$this->Target['Student']['MyGroup'] = array($Lang['SysMgr']['RoleManagement']['MyGroupStudent'],false); 
		//$this->Target['Student']['MyChildren'] = array($Lang['SysMgr']['RoleManagement']['MyChildren'],false); // My children for parent
		
		// parent
		$this->Target['Parent']['All'] = array($Lang['SysMgr']['RoleManagement']['AllParent'],false); 
		$this->Target['Parent']['MyForm'] = array($Lang['SysMgr']['RoleManagement']['MyFormParent'],false); 
		$this->Target['Parent']['MyClass'] = array($Lang['SysMgr']['RoleManagement']['MyClassParent'],false); 
		$this->Target['Parent']['MySubject'] = array($Lang['SysMgr']['RoleManagement']['MySubject'],false); 
		$this->Target['Parent']['MySubjectGroup'] = array($Lang['SysMgr']['RoleManagement']['MySubjectGroupParent'],false); 
		$this->Target['Parent']['MyGroup'] = array($Lang['SysMgr']['RoleManagement']['MyGroupParent'],false); 
		//$this->Target['Parent']['MyParent'] = array($Lang['SysMgr']['RoleManagement']['MyParent'],false); 
		
		// alumni
		if($special_feature['alumni'])
		{
			$this->Target['Alumni']['All'] = array($Lang['SysMgr']['RoleManagement']['Alumni'],false); 
			$this->Target['Alumni']['MyGroup'] = array($Lang['SysMgr']['RoleManagement']['MyGroupAlumni'],false); 
		}
		/*
		// Foundation
		$this->Target["Foundation"] = array("Foundation", false);
		$this->Target["Foundation"]["AllStaff"] = array("All Staff", false);
		$this->Target["Foundation"]["NonExeStaff"] = array("Non-executive Staff", false);
		
		// All Schools
		$this->Target["AllSchool"] = array("All School", false);
		$this->Target["AllSchool"]["SchoolCouncil"] = array("School Council", false);
		$this->Target["AllSchool"]["NonSeniorLeaderStaff"] = array("Non-senior Leader Staff", false);		
		$this->Target["AllSchool"]["TeachingStaff"] = array("Teaching Staff", false);
		$this->Target["AllSchool"]["NonTeachingStaff"] = array("Non-teaching Staff", false);
		$this->Target["AllSchool"]["Student"] = array("Student", false);
		$this->Target["AllSchool"]["Parent"] = array("Parent", false);
		
		// Local School Target
		$this->Target["MySchool"] = array("My School", false);
		$this->Target["MySchool"]["SchoolCouncil"] = array("School Council", false);
		$this->Target["MySchool"]["TeachingStaff"] = array("Teaching Staff", false);
		$this->Target["MySchool"]["NonTeachingStaff"] = array("Non-teaching Staff", false);
		$this->Target["MySchool"]["Student"] = array("Students", false);
		$this->Target["MySchool"]["SchoolBasedGroup"] = array("School Based Groups", false);
		$this->Target["MySchool"]["Parent"] = array("Parents by Rollgroup/Year Group (Bcc)", false);
		*/
		/* ------ End Target ------ */
		
		/*echo '<pre>';
		var_dump($this->SysFunctionList);
		echo '</pre>';
		die;*/
  }
  
  function Get_Right_Display() {
  	
  	$RightList = $this->Loop_Right($this->SysFunctionList);
  	/*echo '<pre>';
  	var_dump($RightList);
  	echo '</pre>';*/
  	
  	return $RightList;
  }
  
  function Loop_Right($CurElement,$RightPath="",$Level=0) {
	
	$sizeOfCyrElement = count($CurElement);  
  	if (is_bool($CurElement[1])) {
  		$ReturnVal[] = array(substr($RightPath,1, strlen($RightPath)), $CurElement[0], $CurElement[1], $Level, $CurElement[2]);
  		for ($i=0;$i<$sizeOfCyrElement;$i++) 
  			next($CurElement);
  	}
  	
  	$i=0;
		while ($Value = current($CurElement)) {
			if (!$i==0) 
				$RightPath = substr($RightPath,0,strripos($RightPath,"-"));
			
  		$RightPath .= "-".Key($CurElement);
  		$TempVal = $this->Loop_Right($Value,$RightPath,($Level+1));
  		
  		for($j=0;$j< sizeof($TempVal); $j++) 
  			$ReturnVal[] = $TempVal[$j];
  		
  		$i++;
  		next($CurElement);
		}
  	
  	return $ReturnVal;	
  }
  
  
  // function outcome
  // [0]: Function name appears in session and in db
  // [1]: The item Display Name
  // [2]: The right flag
  // [3]: The function level (1 or 2)
	function Combine_Right_Permission($RoleRightTargetList=array(),$OutFormat="Display",$TargetOrRight="Right",$IsLogin=false) {
		
		if ($TargetOrRight == "Right")
			$ReplaceList = $this->Get_Right_Display();
		else 
			$ReplaceList = $this->Get_Target_Display();
		
		/*echo '<pre>';
		var_dump($ReplaceList);
		echo '</pre>';
		echo '<pre>';
		var_dump($RoleRightTargetList);
		echo '</pre>';
		die;*/
		if ($IsLogin) {	
			for ($i=0; $i< sizeof($RoleRightTargetList); $i++) {
				for ($j=0; $j< sizeof($ReplaceList); $j++) {
					for($k=0; $k< sizeof($RoleRightTargetList[$i]); $k++) {
						if ($ReplaceList[$j][0] == $RoleRightTargetList[$i][$k][0]) {
							if ($RoleRightTargetList[$i][$k][1] == 1)
								$ReplaceList[$j][2] = $RoleRightTargetList[$i][$k][1];
							break;
						}
					}
				}
			}
		}
		else {
			for ($i=0; $i< sizeof($ReplaceList); $i++) {
				for($j=0; $j< sizeof($RoleRightTargetList); $j++) {
					if ($ReplaceList[$i][0] == $RoleRightTargetList[$j][0]) {
						if ($RoleRightTargetList[$j][1] == 1)
							$ReplaceList[$i][2] = $RoleRightTargetList[$j][1];
						break;
					}
				}
			}
		}
		
		// output to session
		if ($OutFormat != "Display") {
			for ($i=0; $i< sizeof($ReplaceList); $i++) {
				$OutputList[$ReplaceList[$i][0]] = $ReplaceList[$i][2];
			}
			return $OutputList;
		}
		// output to display and db function
		else {
			return $ReplaceList;
		}
	}
	
	function Combine_Target_Permission_With_Identity($TargetList=array(),$OutFormat="Display") {
		$ReplaceList = $this->Get_Target_Display();
		
		for ($i=0; $i< sizeof($ReplaceList); $i++) {
			$Result[$i]['TargetString'] = $ReplaceList[$i][0];
			
			$Result[$i]['TargetName'] = $ReplaceList[$i][1];
			
			$Target = $TargetList['Student'];
			$FinalTargetRight = ($Target[$ReplaceList[$i][0]][1])? $Target[$ReplaceList[$i][0]][1]:$ReplaceList[$i][2];
			$Result[$i]['Student'] = $FinalTargetRight;
			
			$Target = $TargetList['Parent'];
			$FinalTargetRight = ($Target[$ReplaceList[$i][0]][1])? $Target[$ReplaceList[$i][0]][1]:$ReplaceList[$i][2];
			$Result[$i]['Parent'] = $FinalTargetRight;
			
			$Target = $TargetList['Teaching'];
			$FinalTargetRight = ($Target[$ReplaceList[$i][0]][1])? $Target[$ReplaceList[$i][0]][1]:$ReplaceList[$i][2];
			$Result[$i]['Teaching'] = $FinalTargetRight;
			
			$Target = $TargetList['NonTeaching'];
			$FinalTargetRight = ($Target[$ReplaceList[$i][0]][1])? $Target[$ReplaceList[$i][0]][1]:$ReplaceList[$i][2];
			$Result[$i]['NonTeaching'] = $FinalTargetRight;
		}
		
		return $Result;
	}
  
  function Load_User_Right($TargetUserID) {
		global $SYS_CONFIG, $plugin;
		
		$sql = "select 
						rr.FunctionName,
						MAX(rr.RightFlag) as UserRight 
					from 
						ROLE_MEMBER rm 
					INNER JOIN 
						ROLE_RIGHT rr 
						on  
							(rm.RoleID = rr.RoleID 
							and 
							rm.UserID = '".$TargetUserID."') 
					Group By 
						rr.FunctionName";
		//debug_r($sql);
		$RightList[] = $this->returnArray($sql,2);
		$FinalRightList = $this->Combine_Right_Permission($RightList,"Session","Right",true);
  	
  	/*
  	#select the role id based on the school type, ie. Primary or Secondary, as in the batch sync both primary and secondary other support role will be assigned to staff
  	$sql = 'Select 
  						RoleID 
  					From 
  						ROLE_MEMBER 
  					WHERE 
  						UserID = \''.$TargetUserID.'\' 
  				 '; 
  	$RoleList = $this->returnVector($sql,1);
  	
  	for ($i=0; $i< sizeof($RoleList); $i++) {
  		$LocalRole = new role($RoleList[$i],true,false,false);
  		$RoleRight[] = $LocalRole->RightList;
  	}
  	$FinalRightList = $this->Combine_Right_Permission($RoleRight,"Session","Right",true);
  	*/
  	//debug_r($FinalRightList);
  	
  	return $FinalRightList;
  }
  
  function Load_User_Target($TargetUserID) {
  	/*$RoleManage = new role_manage();
  	
  	$IdentityType = $RoleManage->Get_Identity_Type($UserID);*/
  	/*
  	$sql = 'Select 
  				RoleID 
  			From 
  				ROLE_MEMBER 
  			WHERE 
  				UserID = \''.$UserID.'\'
  			';
  	$RoleList = $this->returnVector($sql,1);
  	
  	for ($i=0; $i< sizeof($RoleList); $i++) {
  		$LocalRole = new role($RoleList[$i],false,true,false);
  		
  		//$TargetList = $LocalRole->TargetList[$IdentityType];
  		$TargetList = $LocalRole->TargetList;
  		$TempTarget = array();
  		if (sizeof($TargetList) == 0) 
  			continue;
  		else {
	  		foreach($TargetList as $TargetName => $Targets) {
	  			$TempTarget[] = $Targets;
	  		}
	  		$RoleTarget[] = $TempTarget;
  		}
  	}
  	
  	$FinalTargetList = $this->Combine_Right_Permission($RoleTarget,"Session","Target",true);
  	*/
  	
  	$sql = "select 
							rt.TargetName,
							MAX(rt.RightFlag) as UserRight 
						from 
							ROLE_MEMBER rm 
							INNER JOIN 
							ROLE_TARGET rt 
							on 
								rm.UserID = '".$TargetUserID."' 
								and 
								rm.RoleID = rt.RoleID 
						Group By 
							rt.TargetName";
		//debug_r($sql);
		$RoleTarget[] = $this->returnArray($sql,2);
		$FinalTargetList = $this->Combine_Right_Permission($RoleTarget,"Session","Target",true);
		
  	//debug_r($FinalTargetList);
  	
  	return $FinalTargetList;
  }
  
  function Load_Target($UserID) {
  }	
  
  function Loop_Target($CurElement,$TargetPath="",$Level=0) {
  	if (is_bool($CurElement[1])) {
  		$ReturnVal[] = array(substr($TargetPath,1,strlen($TargetPath)),$CurElement[0],$CurElement[1],$Level);
  		for ($i=0;$i<2;$i++) 
  			next($CurElement);
  	}
  	
  	$i=0;
		while ($Value = current($CurElement)) {
			if (!$i==0) 
				$TargetPath = substr($TargetPath,0,strripos($TargetPath,"-"));
			
  		$TargetPath .= "-".Key($CurElement);
  		$TempVal = $this->Loop_Target($Value,$TargetPath,($Level+1));
  		
  		for($j=0;$j< sizeof($TempVal); $j++) 
  			$ReturnVal[] = $TempVal[$j];
  		
  		$i++;
  		next($CurElement);
		}

  	return $ReturnVal;	
  }
  
 	function Get_Target_Display() {
  		$TargetList = $this->Loop_Target($this->Target);
  		return $TargetList;
  }
  
  function Get_All_Mail_Target_Users($TargetUserId)
  {
  		global $intranet_root, $special_feature;
  		//include_once($intranet_root."/includes/libuser.php");
    	include_once($intranet_root."/includes/role_manage.php");
    	include_once($intranet_root."/includes/form_class_manage.php");
    	
    	//$luser = new libuser($TargetUserId);
    	$lrole = new role_manage();
    	$fcm = new form_class_manage();
    	//$academic_year_id = $_SESSION['CurrentSchoolYearID'];
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		
		$UserTarget = $_SESSION['UserID'] == $TargetUserId ? $_SESSION['SSV_USER_TARGET'] : $this->Load_User_Target($TargetUserId);
		//$user_type = $luser->RecordType;
		//debug_r($UserTarget);
		// $user_type: selected user type - 1=Teaching staff, 2=Student, 3=Non teaching staff, 4=Parent
    	$where_cond = "";
    	/*
    	if($user_type == 1) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_STAFF."' AND u.Teaching='1' ";
    	}else if($user_type == 3) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_STAFF."' AND (u.Teaching='0' OR u.Teaching IS NULL) ";
    	}else if($user_type == 2) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_STUDENT."' ";
    	}else if($user_type == 4) {
    		$where_cond .= " AND u.RecordType='".USERTYPE_PARENT."' ";
    	}
		*/
		/* Reference Mail-Target: 
    	 * $_SESSION['SSV_USER_TARGET']['All-Yes']
    	 * 	$_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']
    	 * 		$_SESSION['SSV_USER_TARGET']['Staff-MyForm']
    	 * 		$_SESSION['SSV_USER_TARGET']['Staff-MyClass']
    	 * 		$_SESSION['SSV_USER_TARGET']['Staff-MySubject']
    	 * 		$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']
    	 * 		$_SESSION['SSV_USER_TARGET']['Staff-MyGroup']
    	 * $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']
    	 * 		$_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']
    	 * $_SESSION['SSV_USER_TARGET']['Student-All']
    	 * 		$_SESSION['SSV_USER_TARGET']['Student-MyForm']
    	 * 		$_SESSION['SSV_USER_TARGET']['Student-MyClass']
    	 * 		$_SESSION['SSV_USER_TARGET']['Student-MySubject']
    	 * 		$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']
    	 * 		$_SESSION['SSV_USER_TARGET']['Student-MyGroup']
    	 * $_SESSION['SSV_USER_TARGET']['Parent-All']
    	 * 		$_SESSION['SSV_USER_TARGET']['Parent-MyForm']
    	 * 		$_SESSION['SSV_USER_TARGET']['Parent-MyClass']
    	 * 		$_SESSION['SSV_USER_TARGET']['Parent-MySubject']
    	 * 		$_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']
    	 * 		$_SESSION['SSV_USER_TARGET']['Parent-MyGroup']
    	 * $_SESSION['SSV_USER_TARGET']['Alumni-All']
    	 * 		$_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']
    	 */
    	
    	if(!$UserTarget['All-Yes']) 
    	{
    		$filterUserIdAry = array();
    		$identity = $lrole->Get_Identity_Type($TargetUserId); // 'Student' | 'Parent' | 'Teaching' | 'NonTeaching' | 'Alumni'
    		
    		// Give default mail target for each identity
    		
    		if($identity == "Parent"){
    			$UserTarget['Parent-MyClass'] = true;
    			$UserTarget['Parent-MySubjectGroup'] = true;
    		}
    		else if($identity == "Student"){
    			$UserTarget['Student-MyClass'] = true;	
				$UserTarget['Student-MySubjectGroup'] = true;
    		}
    		
    		if($identity == 'Teaching' || $identity == 'NonTeaching' || $identity == 'Student' || $identity == 'Parent') {
    			if($identity == 'Teaching' || $identity == 'NonTeaching') {
	    			// My class by teacher type
    			    $sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID WHERE a.UserID = '".$this->Get_Safe_Sql_Query($TargetUserId)."' AND b.AcademicYearID='$CurrentAcademicYearID'";
					$arrTargetYearClassID = $this->returnVector($sql);
					$targetYearClassID = count($arrTargetYearClassID)>0? implode(",",$arrTargetYearClassID):"-1";
	    			
	    			// My subject group by teacher type
					$sql = "SELECT DISTINCT a.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON a.SubjectGroupID = b.SubjectGroupID WHERE a.UserID='".$this->Get_Safe_Sql_Query($TargetUserId)."' AND b.YearTermID='$CurrentTermID'";
					$arrTargetSubjectGroupID = $this->returnVector($sql);
					$targetSubjectGroupID = count($arrTargetSubjectGroupID)>0? implode(",",$arrTargetSubjectGroupID):"-1";
    			}else if($identity == 'Student'){
    				// My class by student type
    			    $sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID WHERE a.UserID = '".$this->Get_Safe_Sql_Query($TargetUserId)."' AND b.AcademicYearID='$CurrentAcademicYearID'";
					$arrTargetYearClassID = $this->returnVector($sql);
					$targetYearClassID = count($arrTargetYearClassID)>0? implode(",",$arrTargetYearClassID):"-1";
	    			
	    			// My subject group by student type
					$sql = "SELECT DISTINCT a.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON a.SubjectGroupID = b.SubjectGroupID WHERE a.UserID='".$this->Get_Safe_Sql_Query($TargetUserId)."' AND b.YearTermID='$CurrentTermID'";
					$arrTargetSubjectGroupID = $this->returnVector($sql);
					$targetSubjectGroupID = count($arrTargetSubjectGroupID)>0? implode(",",$arrTargetSubjectGroupID):"-1";
    			}else if($identity == 'Parent') {
    				// My class by parent type, get my children's class
    			    $sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON a.YearClassID = b.YearClassID INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.UserID WHERE r.ParentID = '".$this->Get_Safe_Sql_Query($TargetUserId)."' AND b.AcademicYearID='$CurrentAcademicYearID'";
					$arrTargetYearClassID = $this->returnVector($sql);
					$targetYearClassID = count($arrTargetYearClassID)>0? implode(",",$arrTargetYearClassID):"-1";
	    			
	    			// My subject group by parent type, get my children's class
					$sql = "SELECT DISTINCT a.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON a.SubjectGroupID = b.SubjectGroupID INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.UserID WHERE r.ParentID='".$this->Get_Safe_Sql_Query($TargetUserId)."' AND b.YearTermID='$CurrentTermID'";
					$arrTargetSubjectGroupID = $this->returnVector($sql);
					$targetSubjectGroupID = count($arrTargetSubjectGroupID)>0? implode(",",$arrTargetSubjectGroupID):"-1";
    			}
    			
    			// Same form teachers or same class teachers
    			if(!$UserTarget['Staff-AllTeaching'] && ($UserTarget['Staff-MyForm'] || $UserTarget['Staff-MyClass'])) {
					$sql = "SELECT DISTINCT UserID FROM YEAR_CLASS_TEACHER WHERE YearClassID IN ($targetYearClassID)";
    				$teacherUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$teacherUserIdAry);
    			}
    			// Same subject group teachers
    			if(!$UserTarget['Staff-AllTeaching'] && ($UserTarget['Staff-MySubject'] || $UserTarget['Staff-MySubjectGroup'])) {
					$sql = "SELECT DISTINCT UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE SubjectGroupID IN ($targetSubjectGroupID)";
					$teacherUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$teacherUserIdAry);
    			}
    			
    			// Same form students or same class students
    			if(!$UserTarget['Student-All'] && ($UserTarget['Student-MyForm'] || $UserTarget['Student-MyClass'])){
    				$sql = "SELECT DISTINCT UserID FROM YEAR_CLASS_USER WHERE YearClassID IN ($targetYearClassID)";
    				$classStudentUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$classStudentUserIdAry);
    			}
    			// Same subject group students
    			if(!$UserTarget['Student-All'] && ($UserTarget['Student-MySubject'] || $UserTarget['Student-MySubjectGroup'])){
    				$sql = "SELECT DISTINCT UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID IN ($targetSubjectGroupID)";
					$subjectStudentUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$subjectStudentUserIdAry);
    			}
    			// Same form student's parents or same class student's parents
    			if(!$UserTarget['Parent-All'] && ($UserTarget['Parent-MyForm'] || $UserTarget['Parent-MyClass'])){
    				$sql = "SELECT DISTINCT r.ParentID FROM YEAR_CLASS_USER as ycu INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=ycu.UserID WHERE ycu.YearClassID IN ($targetYearClassID)";
    				$classParentUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$classParentUserIdAry);
    			}
    			// Same subject group student's parents
    			if(!$UserTarget['Parent-All'] && ($UserTarget['Parent-MySubject'] || $UserTarget['Parent-MySubjectGroup'])){
    				$sql = "SELECT DISTINCT r.ParentID FROM SUBJECT_TERM_CLASS_USER as a INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.UserID WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
					$subjectParentUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$subjectParentUserIdAry);
    			}
    			
    			if($UserTarget['Staff-AllTeaching']){
    				$sql = "SELECT DISTINCT u.UserID FROM INTRANET_USER as u WHERE u.RecordStatus='1' AND u.RecordType='".USERTYPE_STAFF."' AND u.Teaching='1'";
    				$teacherUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$teacherUserIdAry);
    			}
    			if($UserTarget['Staff-AllNonTeaching']){
    				$sql = "SELECT DISTINCT UserID FROM INTRANET_USER WHERE RecordStatus='1' AND RecordType='".USERTYPE_STAFF."' AND (Teaching='0' OR Teaching IS NULL)";
    				$teacherUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$teacherUserIdAry);
    			}
    			if($UserTarget['Student-All']){
    				$sql = "SELECT DISTINCT UserID FROM INTRANET_USER WHERE RecordStatus='1' AND RecordType='".USERTYPE_STUDENT."'";
    				$studentUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$studentUserIdAry);
    			}
    			if($UserTarget['Parent-All']){
    				$sql = "SELECT DISTINCT UserID FROM INTRANET_USER WHERE RecordStatus='1' AND RecordType='".USERTYPE_PARENT."'";
    				$parentUserIdAry = $this->returnVector($sql);
    				$filterUserIdAry = array_merge($filterUserIdAry,$parentUserIdAry);
    			}
    		}
			
			if($special_feature['alumni'] && $UserTarget['Alumni-All']){
				$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType='".USERTYPE_ALUMNI."'";
				$alumniUserIdAry = $this->returnVector($sql);
				$filterUserIdAry = array_merge($filterUserIdAry, $alumniUserIdAry);
			}
			
			// Same group teaching-teachers, or non-teaching teachers, or students, or parents
			if((!$UserTarget['Staff-AllTeaching'] && $UserTarget['Staff-MyGroup'])
				 || (!$UserTarget['Staff-AllNonTeaching'] && $UserTarget['NonTeaching-MyGroup'])
				 || (!$UserTarget['Student-All'] && $UserTarget['Student-MyGroup'])
				 || (!$UserTarget['Parent-All'] && $UserTarget['Parent-MyGroup'])
				 || ($special_feature['alumni'] && !$UserTarget['Alumni-All'] && $UserTarget['Alumni-MyGroup'])) {
				
				$allow_user_types = array(0);
				if($UserTarget['Staff-AllTeaching'] || $UserTarget['Staff-AllNonTeaching']) {
					$allow_user_types[] = USERTYPE_STAFF;
				}
				if($UserTarget['Student-All']){
					$allow_user_types[] = USERTYPE_STUDENT;
				}
				if($UserTarget['Parent-All']){
					$allow_user_types[] = USERTYPE_PARENT;
				}
				if($special_feature['alumni'] && $UserTarget['Alumni-All']){
					$allow_user_types[] = USERTYPE_ALUMNI;
				}
				
				// My Groups
				$sql = "SELECT DISTINCT b.GroupID FROM INTRANET_USERGROUP as a INNER JOIN INTRANET_GROUP as b ON b.GroupID=a.GroupID WHERE a.UserID='".$TargetUserId."' AND b.AcademicYearID='$CurrentAcademicYearID'";
				$arrTargetGroupID = $this->returnVector($sql);
				$targetGroupID = count($arrTargetGroupID)>0? implode(",",$arrTargetGroupID):"-1";
				
				$sql = "SELECT DISTINCT a.UserID FROM INTRANET_USER as a INNER JOIN INTRANET_USERGROUP as b ON a.UserID=b.UserID WHERE (b.GroupID IN ($targetGroupID) AND a.RecordType IN (".implode(",",$allow_user_types).")) ";
				if($UserTarget['Staff-MyGroup']) {
					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_STAFF."' AND a.Teaching='1') ";
				}
				if($UserTarget['NonTeaching-MyGroup']) {
					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_STAFF."' AND (a.Teaching!='1' OR a.Teaching IS NULL)) ";
				}
				if($UserTarget['Student-MyGroup']) {
					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_STUDENT."') ";
				}
				if($UserTarget['Parent-MyGroup']) {
					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_PARENT."') ";
				}
				if($UserTarget['Alumni-MyGroup']){
					$sql .= " OR (b.GroupID IN ($targetGroupID) AND a.RecordType='".USERTYPE_ALUMNI."') ";
				}
				
				$groupUserIdAry = $this->returnVector($sql);
				$filterUserIdAry = array_merge($filterUserIdAry,$groupUserIdAry);
			}
    		
    		if(!$UserTarget['Staff-AllTeaching'] && !$UserTarget['Staff-AllNonTeaching']
    			&& !$UserTarget['Student-All'] && !$UserTarget['Parent-All']
    			 && count($filterUserIdAry)==0) {
    			$filterUserIdAry[] = -1;
    		}
    		$where_cond .= " AND u.UserID IN (".implode(",",$filterUserIdAry).") ";
    	} // End of filtering mail target
    	
    	$name_field = getNameFieldByLang("u.");
    	$sql = "SELECT DISTINCT u.UserID,
				    ".$name_field." as UserName,
				    u.ClassName,
					u.ClassNumber,
				    u.RecordType 
				FROM INTRANET_USER as u 
				WHERE u.RecordStatus='1' $where_cond 
				ORDER BY u.EnglishName";
    	//debug_r($sql); 
    	return $this->returnResultSet($sql);
  }
  
}
?>