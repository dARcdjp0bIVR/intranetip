<?php

## Using : ronald

if (!defined("LIBPERIOD_DEFINED"))         // Preprocessor directives
{
	define("LIBPERIOD_DEFINED",true);

	class libperiods extends libdb{
		
		var $PeriodID;
		var $PeriodStart;
		var $PeriodEnd;
		var $PeriodType;
		var $CycleType;
		var $PeriodDays;
		var $FirstDay;
		var $SaturdayCounted;
		
		var $AcademicYearID;
		
		function libperiods($SchoolYearID='')
		{
			include_once("form_class_manage.php");
			$lfcm = new form_class_manage();
			
			$this->libdb();
			if($SchoolYearID!='')
				$this->AcademicYearID = $SchoolYearID;
			else
				$this->AcademicYearID = $lfcm->getCurrentAcademicaYearID();
				
		}
		
		function retriveAllPeriodsInfo()
		{
			$sql = "SELECT 
						a.PeriodID, a.AcademicYearID, a.PeriodName, a.RecordStatus, c.StartDate, c.EndDate, e.TimeSessionID, e.TimeSessionName, e.StartTime, e.EndTime
					FROM 
						INTRANET_PERIOD AS a INNER JOIN 
						INTRANET_PERIOD_DATERANGE_RELATION AS b ON (a.PeriodID = b.PeriodID) INNER JOIN
						INTRANET_PERIOD_DATERANGE AS c ON (b.DateRangeID = c.DateRangeID) INNER JOIN 
						INTRANET_PERIOD_TIMESESSION_RELATION AS d ON (a.PeriodID = d.PeriodID) INNER JOIN
						INTRANET_PERIOD_TIMESESSION AS e ON (d.TimeSessionID = e.TimeSessionID)
					ORDER BY b.PeriodID, b.DateRangeID, d.TimeSessionID";
						
			$result = $this->returnArray($sql,9);

			return $result;
		}
		
		
		function retrivePeriodsDateRange($SchoolYearID)
		{
			if($SchoolYearID == "")
				$SchoolYearID = $this->AcademicYearID;
				
			$sql = "SELECT 
						a.PeriodID, a.AcademicYearID, a.PeriodName, a.RecordStatus, c.StartDate, c.EndDate
					FROM 
						INTRANET_PERIOD AS a INNER JOIN 
						INTRANET_PERIOD_DATERANGE_RELATION AS b ON (a.PeriodID = b.PeriodID) INNER JOIN
						INTRANET_PERIOD_DATERANGE AS c ON (b.DateRangeID = c.DateRangeID)
					WHERE 
						a.AcademicYearID = $SchoolYearID
					ORDER BY 
						b.PeriodID, b.DateRangeID";
			$result = $this->returnArray($sql,6);
			
			return $result;	
		}
		
		function checkPeriodName($SchoolYearID,$PeriodName)
		{
			if($PeriodName == "")
				return 0;
			
			$sql = "SELECT 
						count(1) 
					FROM 
						INTRANET_PERIOD
					WHERE 
						PeriodName = '".$this->Get_Safe_Sql_Query($PeriodName)."' AND
						AcademicYearID = $SchoolYearID";
						
			$result = $this->returnVector($sql);
			return ($result[0] == 0);
		}
	}
}

?>