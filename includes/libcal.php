<?php
// using : 

class libcal extends libdb { 

	var $image_path;
	var $view;
	var $ts;
	var $d;
	var $m;
	var $y;
	var $today;

	function libcal($ts="", $v=""){
		global $image_path;
		$this->libdb();
		$this->image_path = $image_path;
		$ts = (!$ts) ? mktime(0,0,0,date("m"),date("d"),date("Y")) : $ts;
		$ts = max($ts,mktime(0,0,0,1,1,1971));
		$ts = min($ts,mktime(0,0,0,1,1,2030));
		$this->ts = $ts;
	    $this->d = date("d",$ts);
	    $this->m = date("m",$ts);
	    $this->y = date("Y",$ts);
	    $this->today = mktime(0,0,0,date("m"),date("d"),date("Y"));
		switch($v){
			case 1: $this->view = $v; break; # day
			case 2: $this->view = $v; break; # week
			case 3: $this->view = $v; break; # month
			case 4: $this->view = $v; break; # year
			default: $this->view = 3; break; # month
		}
    }

	###############################################################################

	function totalDaysInMonth($month, $year){
		$x = 0;
		while(checkdate($month,$x+1,$year)) $x++;
		return $x;
	}

	function returnCalendarArray($month, $year){
		$j = 0;
		$TotalDays = $this->totalDaysInMonth($month, $year);
		$DaysOffSet = date("w",mktime(0,0,0,$month,1,$year));
		for($i=1; $i<=$DaysOffSet; $i++)
		$x[$j++] = "";
#		$x[$j++] = mktime(0,0,0,$month,($i-$DaysOffSet),$year);
		for($i=1; $i<=$TotalDays; $i++)
		$x[$j++] = mktime(0,0,0,$month,$i,$year);
		$DaysOffSet = 6 - date("w",mktime(0,0,0,$month,$TotalDays,$year));
		for($i=1; $i<=$DaysOffSet; $i++)
		$x[$j++] = "";
#		$x[$j++] = mktime(0,0,0,$month,($TotalDays+$i),$year);
		return $x;
	}

	function go($x, $type){
		switch($type){
			case "d" :
				$ts = mktime(0,0,0,$this->m, ($this->d+$x), $this->y); 
				break;
			case "w" :
				$ts = mktime(0,0,0,$this->m, ($this->d+(7*$x)), $this->y);
				break;
			case "m" :
				$a = $this->m+$x;
				$b = $this->d;
				$c = $this->y;
				if(($a==4 || $a==6 || $a==9 || $a==11) && ($b==31)) $b = 30;
				if(($a==2) && ($b==30 || $b==31)) $b = $this->totalDaysInMonth($a,$c);
				$ts = mktime(0,0,0,$a, $b, $c);
				break;
			case "y" :
				$a = $this->m;
				$b = $this->d;
				$c = $this->y+$x;
				if(($a==2) && ($b==29)) $b = $this->totalDaysInMonth($a,$c);
				$ts = mktime(0,0,0,$a,$b,$c);
				break;
		}
		return $ts;
	}

	###############################################################################

	function displayPanelToggle(){
		$x .= ($this->view==1) ? "DAY\n" : "<a href=?v=1&ts=".$this->ts.">DAY</a>\n";
		$x .= " - ";
		$x .= ($this->view==2) ? "WEEK\n" : "<a href=?v=2&ts=".$this->ts.">WEEK</a>\n";
		$x .= " - ";
		$x .= ($this->view==3) ? "MONTH\n" : "<a href=?v=3&ts=".$this->ts.">MONTH</a>\n";
		$x .= " - ";
		$x .= ($this->view==4) ? "YEAR\n" : "<a href=?v=4&ts=".$this->ts.">YEAR</a>\n";
		return $x;
	}

	function displayToday(){
		$x = "Today is <a href=?ts=".$this->today.">".date("Y-m-d", $this->today)."</a>\n";
		return $x;
	} 

	###############################################################################

	function displayCalendarArray($month, $year){
		$row = $this->returnCalendarArray($month, $year);
		$x .= "<table width=180 border=0 cellpadding=1 cellspacing=1>\n";
		$x .= "<tr><td colspan=7 align=center>\n";
		$x .= "<a href=?v=".$this->view."&ts=".$this->go(-1,"m")."><img hspace=5 vspace=0 src=".$this->image_path."/left4.gif border=0></a>\n";
		$x .= date("M, Y", mktime(0,0,0,$month,1,$year));
		$x .= "<a href=?v=".$this->view."&ts=".$this->go(+1,"m")."><img hspace=5 vspace=0 src=".$this->image_path."/right4.gif border=0></a>\n";
		$x .= "</td></tr>\n";
		$x .= "<tr>\n";
		$x .= "<td class=small width=14% align=center>S</td>\n";
		$x .= "<td class=small width=14% align=center>M</td>\n";
		$x .= "<td class=small width=14% align=center>T</td>\n";
		$x .= "<td class=small width=14% align=center>W</td>\n";
		$x .= "<td class=small width=14% align=center>T</td>\n";
		$x .= "<td class=small width=14% align=center>F</td>\n";
		$x .= "<td class=small width=14% align=center>S</td>\n";
		$x .= "</tr>\n";
		for($i=0; $i<sizeof($row); $i++){
			$x .= ($i%7==0) ? "</tr>\n<tr>\n" : "";
			$x .= $this->displayCalendarEvent($row[$i]);
		}
		$x .= "</tr>\n";
		$x .= "</table>\n";
		return $x;
	}

	function displayCalendarEvent($ts){
		$x .= "<td class=small align=center>";
		if($ts==""){
			$x .= "<br>";
		}else{
			if(date("w", $ts)==0){
				$x .= ($this->ts==$ts) ? "" : "<font color=red>";
				$x .= date("j",$ts);
				$x .= ($this->ts==$ts) ? "" : "</font>";
			}else{
				$x .= date("j",$ts);
			}
		}
		$x .= "</td>\n";
		return $x;
	}

	###############################################################################

	function displayDayPanel(){
		$x .= $this->displayCalendarArray($this->m, $this->y);
		$x .= $this->displayCalendarArray(date("m",mktime(0,0,0,$this->m+1,1,$this->y)), date("Y",mktime(0,0,0,$this->m+1,1,$this->y)));
		return $x;
	}

	function displayMonthPanel(){
		$x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr><td class=tableTitle colspan=3 align=center>\n";
		$x .= "<a href=?v=".$this->view."&ts=".$this->go(-1,"y")."><img hspace=5 vspace=0 src=".$this->image_path."/left4.gif border=0></a>\n";
		$x .= date("Y", $this->ts);
		$x .= "<a href=?v=".$this->view."&ts=".$this->go(+1,"y")."><img hspace=5 vspace=0 src=".$this->image_path."/right4.gif border=0></a>\n";
		$x .= "</td></tr>\n";
		$x .= "<tr>\n";
		for($i=1;$i<=12;$i++){
			$mon = date("M", mktime(0,0,0,$i,1,date("Y")));
			$x .= ($this->m==$i) ? "<td align=center>$mon</td>\n" : "<td align=center><a href=?v=".$this->view."&ts=".$this->go($i-$this->m,"m").">$mon</a></td>\n";
			$x .= ($i%3==0) ? "</tr>\n<tr>\n" : "";
		}
		$x .= "</tr>\n";
		$x .= "</table>\n";
		return $x;
	}

	function displayYearPanel(){
		$x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr><td class=tableTitle colspan=3 align=center>\n";
		$x .= "<a href=?v=".$this->view."&ts=".$this->go(-1,"y")."><img hspace=5 vspace=0 src=".$this->image_path."/left4.gif border=0></a>\n";
		$x .= date("Y", $this->ts);
		$x .= "<a href=?v=".$this->view."&ts=".$this->go(+1,"y")."><img hspace=5 vspace=0 src=".$this->image_path."/right4.gif border=0></a>\n";
		$x .= "</td></tr>\n";
		$x .= "<tr>\n";
		$x .= "<td align=center>".$this->y."</td>\n";
		$x .= "<td align=center><a href=?v=".$this->view."&ts=".$this->go(1,"y").">".(($this->y)+1)."</a></td>\n";
		$x .= "<td align=center><a href=?v=".$this->view."&ts=".$this->go(2,"y").">".(($this->y)+2)."</a></td>\n";
		$x .= "</tr>\n";
		$x .= "</table>\n";
		return $x;
	}

	function displayDay(){
		$x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr><td class=head>".date("l F d, Y", $this->ts)."</td></tr>\n";
		$x .= "</table><br>\n";
		$x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
		for($i=0;$i<24;$i++){
			$x .= "<tr>\n";
			$x .= "<td class=tableTitle width=10% align=center>$i:00</td>\n";
			$x .= "<td class=tableContent><br></td>\n";
			$x .= "</tr>\n";
		}
		$x .= "</table>\n";
		return $x;
	}

	function displayWeek(){
		$x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr><td class=head>".date("F d, Y", $this->ts)." - ".date("F d, Y", $this->ts+604800)."</td></tr>\n";
		$x .= "</table><br>\n";
		$x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
		for($i=0; $i<7; $i++){
			$ts = mktime(0,0,0,$this->m,($this->d+$i),$this->y);
			$x .= "<tr><td class=tableTitle><a class=tableTitle href=?v=1&ts=$ts>".date("l, M d",$ts)."</a></td></tr>\n";
			$x .= "<tr><td height=40 class=tableContent><br></td></tr>\n";
			$x .= "<tr><td><br></td></tr>\n";
		}
		$x .= "</table>\n";

		return $x;
	}

	function displayMonth(){
		$row = $this->returnCalendarArray($this->m, $this->y);
		$x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr><td class=head>".date("F, Y", mktime(0,0,0,$this->m, 1, $this->y))."</td></tr>\n";
		$x .= "</table><br>\n";
		$x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr>\n";
		$x .= "<td width=9%><br></td>\n";
		$x .= "<td class=tableTitle width=13%>S</td>\n";
		$x .= "<td class=tableTitle width=13%>M</td>\n";
		$x .= "<td class=tableTitle width=13%>T</td>\n";
		$x .= "<td class=tableTitle width=13%>W</td>\n";
		$x .= "<td class=tableTitle width=13%>T</td>\n";
		$x .= "<td class=tableTitle width=13%>F</td>\n";
		$x .= "<td class=tableTitle width=13%>S</td>\n";
		$x .= "</tr>\n";
		for($i=0; $i<sizeof($row); $i++){
			$ts = $row[$i];
			$x .= ($i%7==0) ? "</tr>\n<tr>\n" : "";
			$x .= ($i%7==0) ? "<td height=60><a href=?v=2&ts=".$this->go(date("j",$ts)-$this->d,"d").">Week View</a></td>\n" : "";
			$x .= "<td class=tableContent>";
			$x .= ($this->ts==$ts) ? date("j",$ts) : "<a href=?v=1&ts=$ts>".date("j",$ts)."</a>";
			$x .= "</td>\n";
		}
		$x .= "</tr>\n";
		$x .= "</table>\n";
		return $x;
	}

	function displayYear(){
		$x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr><td class=head>".date("Y", $this->ts)."</td></tr>\n";
		$x .= "</table><br>\n";
		$x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr>\n";
		$x .= "<td>".$this->displayCalendarArray(1, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(2, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(3, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(4, $this->y)."</td>\n";
		$x .= "</tr>\n";
		$x .= "<tr>\n";
		$x .= "<td>".$this->displayCalendarArray(5, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(6, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(7, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(8, $this->y)."</td>\n";
		$x .= "</tr>\n";
		$x .= "<tr>\n";
		$x .= "<td>".$this->displayCalendarArray(9, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(10, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(11, $this->y)."</td>\n";
		$x .= "<td>".$this->displayCalendarArray(12, $this->y)."</td>\n";
		$x .= "</tr>\n";
		$x .= "</table>\n";
		return $x;
	}

	###############################################################################

	function display(){
		$x  = "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
		$x .= "<tr>\n";
		$x .= "<td width=75% align=right>\n";
		switch($this->view){
			case 1: $x .= $this->displayDay(); break;
			case 2: $x .= $this->displayWeek(); break;
			case 3: $x .= $this->displayMonth(); break;
			case 4: $x .= $this->displayYear(); break;
		}
		$x .= "</td>\n";
		$x .= "<td width=25% align=center>\n";
		$x .= "<p>".$this->displayPanelToggle()."\n";
		$x .= "<p>\n";
		switch($this->view){
			case 1: $x .= $this->displayDayPanel(); break;
			case 2: $x .= $this->displayDayPanel(); break;
			case 3: $x .= $this->displayMonthPanel(); break;
			case 4: $x .= $this->displayYearPanel(); break;
		}
		$x .= "<p>".$this->displayToday()."\n";
		$x .= "</td>\n";
		$x .= "</tr>\n";
		$x .= "</table>\n";
		return $x;
	}

}
?>