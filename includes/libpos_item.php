<?php
// Using 
/*
 * 2020-02-06 (Cameron)
 *      - modify Delete_Photo(), also delete original photo
 *      - modify Upload_Photo() to store original image (in original path) and resized image (180x120 pixel)
 * 2020-02-05 (Cameron): disable uploaded photo dimension checking in Upload_Photo()
 * 2019-10-28 (Henry); add parameter CostPrice to Update_Inventory()
 * 
 * 2019-10-15 (Cameron): add parameter $inventoryOnly to Get_All_Items()
 *
 * 2019-07-18 (Cameron): add parameter $isAssoc to Get_All_Items()
 *
 * 2019-07-16 (Cameron): add field "Description" for showing in eClassApp
 *
 * 2014-02-11 (Carlos): Added Permanent_Delete_Item() - permanently delete the item if no transaction performed
 * 						Modified Delete_Photo() - check valid photo path
 */
//include_once('libpos.php');
include_once('libpos_category.php');

class POS_Item extends POS_Category {
	var $TableName;
	var $ID_FieldName;
	var $ObjectID;
	var $PhotoWidthMax;
	var $PhotoHeightMax;
	
	var $ItemID;
	var $CategoryID;
	var $Barcode;
	var $ItemName;
	var $UnitPrice;
	var $Description;
	var $PhotoExt;
	var $ItemCount;
	var $ItemSalesTotal;
	var $Sequence;
	var $RecordStatus;
	var $DateInput;
	var $InputBy;
	var $DateModify;
	var $ModifyBy;
	
	var $PhotoPath;
	var $OriginalPhotoPath;
// 	var $OriginalPhotoWidthMax;
// 	var $OriginalPhotoHeightMax;
	
	function POS_Item($ObjectID="",$GetCategoryInfo=false){
		parent:: POS_Category();
		
		$this->TableName = 'POS_ITEM';
		$this->ID_FieldName = 'ItemID';
		$this->PhotoWidthMax = 180;
		$this->PhotoHeightMax = 120;
// 		$this->OriginalPhotoWidthMax = 1000;
// 		$this->OriginalPhotoHeightMax = 750;
		
		if ($ObjectID!="")
		{
			$this->ObjectID = $ObjectID;
			$this->Get_Self_Info();
			
			// Get PhotoPath
			if ($this->PhotoExt != '') {
				$this->PhotoPath = "/file/pos/photo/item/".$this->ObjectID.$this->PhotoExt;
				$this->OriginalPhotoPath = "/file/pos/photo/item/original/".$this->ObjectID.$this->PhotoExt;
			}
			
			if ($GetCategoryInfo) {
				parent::POS_Category($this->CategoryID);
			}
		}
	}
	
	function Get_Self_Info()
	{
		$sql = "";
		$sql .= " SELECT * FROM ".$this->TableName;
		$sql .= " WHERE ".$this->ID_FieldName." = '".$this->ObjectID."'";
		
		$resultSet = $this->returnArray($sql);
		$infoArr = $resultSet[0];
		
		foreach ($infoArr as $key => $value)
			$this->{$key} = $value;
	}
	
	function Insert_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# ItemSalesTotal
		$fieldArr[] = 'RecordStatus';
		$valueArr[] = 1;
		# ItemCount
		$fieldArr[] = 'ItemCount';
		$valueArr[] = 0;
		# ItemSalesTotal
		$fieldArr[] = 'ItemSalesTotal';
		$valueArr[] = 0;
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = $_SESSION['UserID'];
		# DateModify
		$fieldArr[] = 'DateModify';
		$valueArr[] = 'now()';
		# ModifyBy
		$fieldArr[] = 'ModifyBy';
		$valueArr[] = $_SESSION['UserID'];
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$sql = '';
		$sql .= ' INSERT INTO '.$this->TableName;
		$sql .= ' ( '.$fieldText.' ) ';
		$sql .= ' VALUES ';
		$sql .= ' ( '.$valueText.' ) ';
		$success = $this->db_db_query($sql);
		
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			$this->Commit_Trans();
			
			return $insertedID;
		}
	}
	
	function Update_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModify = now(), ModifyBy = \''.$_SESSION['UserID'].'\' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName;
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Delete_Record()
	{
		$success = $this->Inactivate_Record();
		return $success;
	}
	
	function Inactivate_Record()
	{
		$DataArr['RecordStatus'] = "0";
		$success = $this->Update_Record($DataArr);
		
		return $success;
	}
	
	function Activate_Record()
	{
		$DataArr['RecordStatus'] = 1;
		$success = $this->Update_Record($DataArr);
		
		return $success;
	}
	
	function Is_Barcode_Valid($Barcode, $ExcludeObjectID='')
	{
		# Barcode can be empty
		if ($Barcode == '')
			return true;
			
		$cond_excludeObjectID = '';
		if ($ExcludeObjectID != '')
			$cond_excludeObjectID = " And ".$this->ID_FieldName." != '".$ExcludeObjectID."' ";
			
		$sql = "Select
						".$this->ID_FieldName."
				From
						".$this->TableName."
				Where
						Barcode = '".$this->Get_Safe_Sql_Query($Barcode)."'
						$cond_excludeObjectID
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return 1;
		else
			return 0;
	}
	
	function Is_Name_Valid($Name, $ExcludeObjectID='', $CategoryID='')
	{
		$cond_excludeObjectID = '';
		$cond_categoryID = '';
		if ($ExcludeObjectID != '')
			$cond_excludeObjectID = " And ".$this->ID_FieldName." != '".$ExcludeObjectID."' ";
		if ($CategoryID != '' && $CategoryID > 0){
			$cond_categoryID = " AND CategoryID = '$CategoryID' ";
		}
			
		$sql = "Select
						".$this->ID_FieldName."
				From
						".$this->TableName."
				Where
						ItemName = '".$this->Get_Safe_Sql_Query($Name)."'
						$cond_excludeObjectID $cond_categoryID
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return 1;
		else
			return 0;
	}
	
	function Upload_Photo($FileLocation, $FileName)
	{
		global $intranet_root;
		
// 		$isValidDimension = $this->Check_Valid_Photo_Dimension($FileLocation);
// 		if ($isValidDimension == false)
// 			return false;
		
		### Delete the old photo first
		$SuccessArr['DeleteOldPhoto'] = $this->Delete_Photo();
		
		$fs = new libfilesystem();
		
		# Create folder if not exist
		$folder_prefix = $intranet_root."/file/pos";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/photo";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$folder_prefix .= "/item";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
		$original_folder = $folder_prefix."/original";
		if (!file_exists($original_folder)) {
		    $fs->folder_new($original_folder);
		}
			
		# Rename photo file
		$extention = $fs->file_ext($FileName);
		$filename = $this->ObjectID.$extention;
		
		### Store File Extension
		$DataArr = array();
		$DataArr['PhotoExt'] = $extention;
		$SuccessArr['StoreFileExtension'] = $this->Update_Object($DataArr);
		
		### Upload File
		$SuccessArr = array();
		$uploadResult = array();
		list($width, $height) = getimagesize($FileLocation);
// 		if ($width > $this->OriginalPhotoWidthMax || $height > $this->OriginalPhotoHeightMax) {
// 		    if ($width > $this->OriginalPhotoWidthMax){
// 		        $toWidth = $this->OriginalPhotoWidthMax;
// 		    }
// 		    else {
// 		        if ($height>0) {
// 		            $toWidth = floor (($this->OriginalPhotoHeightMax ) * ($width/$height));
// 		        }
// 		        else {
// 		            $toWidth = $width;
// 		        }
// 		    }
// 		    $largeImage = $this->resizeImage($FileLocation, $toWidth);
// 		    $toFile = $original_folder."/".$filename;
// 		    switch($extention){
// 		        case '.jpg':
// 		        case '.jpeg':
// 		            $uploadResult[] = imagejpeg($largeImage, $toFile, 100);
// 		            break;
// 		        case '.gif':
// 		            $uploadResult[] = imagegif($largeImage, $toFile);
// 		            break;
// 		        case '.png':
// 		            $uploadResult[] = imagepng($largeImage, $toFile, 0);
// 		            break;
// 		    }
// 		}
// 		else {
		    $uploadResult[] = $fs->file_copy($FileLocation, $original_folder."/".$filename);
//		}
		
		if ($width > $this->PhotoWidthMax || $height > $this->PhotoHeightMax) {
		    if ($width > $this->PhotoWidthMax){
		        $toWidth = $this->PhotoWidthMax;
		    }
		    else {
		        if ($height>0) {
		            $toWidth = floor (($this->PhotoHeightMax) * ($width/$height));
		        }
		        else {
		            $toWidth = $width;
		        }
		    }
		    $smallImage = $this->resizeImage($FileLocation, $toWidth);
		    $toFile = $folder_prefix."/".$filename;
		    switch($extention){
		        case '.jpg':
		        case '.jpeg':
		            $uploadResult[] = imagejpeg($smallImage, $toFile, 100);
		            break;
		        case '.gif':
		            $uploadResult[] = imagegif($smallImage, $toFile);
		            break;
		        case '.png':
		            $uploadResult[] = imagepng($smallImage, $toFile, 0);
		            break;
		    }
		}
		else {
		    $uploadResult[] = $fs->file_copy($FileLocation, $folder_prefix."/".$filename);
		}
		$SuccessArr['UploadPhoto'] = in_array(false,$uploadResult) ? false : true;
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Check_Valid_Photo_Dimension($FileLocation)
	{
		list($PhotoWidth, $PhotoHeight) = getimagesize($FileLocation);
		
		if ($PhotoWidth > $this->PhotoWidthMax || $PhotoHeight > $this->PhotoHeightMax)
			return 0;
		else
			return 1;
	}
	
	function Delete_Photo()
	{
		global $intranet_root;
		$fs = new libfilesystem();
		
		$SuccessArr = array();
		$fileRemoveArr = array();
		$PhotoPath = $intranet_root.$this->PhotoPath;
		if($this->PhotoPath != '' && file_exists($PhotoPath)) {
		    $fileRemoveArr[] = $fs->file_remove($PhotoPath);
		}
		$OriginalPhotoPath = $intranet_root.$this->OriginalPhotoPath;
		if($this->OriginalPhotoPath != '' && file_exists($OriginalPhotoPath)) {
		    $fileRemoveArr[] = $fs->file_remove($OriginalPhotoPath);
		}
		$SuccessArr['FileRemove'] = in_array(false,$fileRemoveArr) ? false : true;
		
		if ($SuccessArr['FileRemove'] == true)
		{
			$DataArr = array();
			$DataArr['PhotoExt'] = '';
			$SuccessArr['StoreFileExtension'] = $this->Update_Object($DataArr);
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Get_Max_Display_Order()
	{
		$sql = '';
		$sql .= 'SELECT max(Sequence) FROM '.$this->TableName;
		$MaxDisplayOrder = $this->returnVector($sql);
		
		return $MaxDisplayOrder[0];
	}
	
	function Get_Update_Display_Order_Arr($OrderText, $Separator=",")
	{
		if ($OrderText == "")
			return false;
			
		$orderArr = explode($Separator, $OrderText);
		$orderArr = array_remove_empty($orderArr);
		$orderArr = Array_Trim($orderArr);
		
		$numOfOrder = count($orderArr);
		# display order starts from 1
		$counter = 1;
		$newOrderArr = array();
		for ($i=0; $i<$numOfOrder; $i++)
		{
			$thisID = $orderArr[$i];
			if (is_numeric($thisID))
			{
				$newOrderArr[$counter] = $thisID;
				$counter++;
			}
		}
		
		return $newOrderArr;
	}
	
	function Update_DisplayOrder($DisplayOrderArr=array())
	{
		if (count($DisplayOrderArr) == 0)
			return false;
			
		$this->Start_Trans();
		for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
			$thisObjectID = $DisplayOrderArr[$i];
			
			$sql = 'UPDATE '.$this->TableName.' SET 
								Sequence = \''.$i.'\' 
							WHERE 
								'.$this->ID_FieldName.' = \''.$thisObjectID.'\'';
			$Result['ReorderResult'.$i] = $this->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) 
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	/*
	 *	return: "-1"	not enough inventory for decrease
	 *			"0"		update failed
	 			"1"		update success
	 */
	function Update_Inventory($AdjustedByValue, $ForIncrease, $Remarks, $CostPrice)
	{
		if ($ForIncrease == false)
		{
			// Update the object item count and check if there are enough inventory
			$this->Get_Self_Info();
			
			if ($this->ItemCount - $AdjustedByValue < 0)
				return "-1";
		}
		
		$this->Start_Trans_For_Lock_Table_Procedure();
		# Lock tables
		$sql = "LOCK TABLES 
						POS_ITEM WRITE,
						POS_ITEM_LOG WRITE
						";
		$this->db_db_query($sql);
		
		# calculate result value
		if ($ForIncrease)
		{
			$resultValue = $this->ItemCount + $AdjustedByValue;
			$logType = 3;
		}
		else
		{
			$resultValue = $this->ItemCount - $AdjustedByValue;
			$logType = 4;
		}
		
		### Update Item Count of the Item
		$sql = "Update
						POS_ITEM
				Set
						ItemCount = '".$resultValue."',
						InventoryDateModify = now(),
						InventoryModifyBy = '".$_SESSION['UserID']."'
				Where
						ItemID = '".$this->ItemID."'
				";
		$SuccessArr['POS_ITEM'] = $this->db_db_query($sql);
		
		if ($SuccessArr['POS_ITEM'] == true)
		{
			### Insert Log Record
			$Remarks = $this->Get_Safe_Sql_Query($Remarks);
						
			$sql = "Insert Into POS_ITEM_LOG
						(ItemID, LogType, Amount, CountAfter, Remark, DateInput, InputBy, DateModify, ModifyBy, CostPrice)
					Values
						(
							'".$this->ItemID."', '".$logType."', '".$AdjustedByValue."', '".$resultValue."', '".$Remarks."', 
							now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."', '".$CostPrice."'
						)
					";
			$SuccessArr['POS_ITEM_LOG'] = $this->db_db_query($sql);
		}
		
		if (!in_array(false, $SuccessArr)) 
		{
			$this->Commit_Trans();
			$returnValue = '1';
		}
		else {
			$this->RollBack_Trans();
			$returnValue = '0';
		}
		
		$sql = "UNLOCK TABLES";
		$this->db_db_query($sql);
		
		return $returnValue;
	}
	
	function Get_All_Items($Keyword='', $isAssoCategory=0, $isAssoc=false, $inventoryOnly=false)
	{
		$cond_keyword = '';
		if ($Keyword != '')
			$cond_keyword = " And (ItemName Like '%".$Keyword."%' Or Barcode Like '%".$Keyword."%') ";

        $cond_inventory = '';
		if ($inventoryOnly) {
		    $cond_inventory = " and pi.ItemCount>0 ";
        }

		$sql = "Select
						pi.ItemID,
						pi.CategoryID,
						pi.Barcode,
						pi.ItemName,
						pi.UnitPrice,
						pi.PhotoExt,
						pi.ItemCount,
						pi.ItemSalesTotal,
						pi.Sequence,
						pi.RecordStatus,
						pi.DateInput,
						pi.InputBy,
						pi.DateModify,
						pi.ModifyBy,
						pi.Description
				From
						POS_ITEM as pi
						Inner Join
						POS_ITEM_CATEGORY as pic
						On (pi.CategoryID = pic.CategoryID)
				Where
						1
						$cond_keyword
						$cond_inventory
				Order By
						pic.Sequence, pi.Sequence
				";
		if ($isAssoc) {
            $ItemArr = $this->returnResultSet($sql);
        }
        else {
            $ItemArr = $this->returnArray($sql);
        }
		
		$ReturnArr = array();
		if ($isAssoCategory == 1)
		{
			$numOfItem = count($ItemArr);
			for ($i=0; $i<$numOfItem; $i++)
			{
				$thisCategoryID = $ItemArr[$i]['CategoryID'];
				$thisItemID = $ItemArr[$i]['ItemID'];
				
				foreach ($ItemArr[$i] as $key => $value)
				{
					$ReturnArr[$thisCategoryID][$thisItemID][$key] = $value;
				}
			}
		}
		else
			$ReturnArr = $ItemArr;
		
		return $ReturnArr;
	}
	
	function Get_Item_Log_Info($StartDate='', $EndDate='')
	{
		$cond_startdate = '';
		if ($StartDate != '')
			$cond_startdate = " And DateInput >= '".$StartDate."' ";
		
		$cond_enddate = '';
		if ($EndDate != '')
		{
			$EndDate = $EndDate.' 23:59:59';
			$cond_enddate = " And DateInput <= '".$EndDate."' ";
		}
		
		$sql = "Select
						ItemLogID,
						ItemID,
						LogType,
						RefLogID,
						Amount,
						CountAfter,
						Remark,
						DateInput,
						InputBy,
						DateModify,
						ModifyBy
				From
						POS_ITEM_LOG
				Where
						ItemID = '".$this->ItemID."'
						$cond_startdate
						$cond_enddate
				";
		$LogArr = $this->returnArray($sql);
		
		return $LogArr;
	}
	
	function Permanent_Delete_Item()
	{
		global $intranet_root;
		$sql = "SELECT COUNT(*) FROM POS_ITEM_LOG WHERE ItemID='".$this->ItemID."' AND LogType IN (1,2)";
		$record = $this->returnVector($sql);
		
		if($record[0] > 0){
			return false;
		}
		
		$result = array();
		$result['DeletePhoto'] = $this->Delete_Photo();
		
		$sql = "DELETE FROM POS_ITEM_HEALTH_INGREDIENT WHERE ItemID='".$this->ItemID."'";
		$result['DeleteItemIngredients'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM POS_ITEM WHERE ItemID='".$this->ItemID."'";
		$result['DeleteItem'] = $this->db_db_query($sql);
		
		return !in_array(false,$result);
	}
}

?>