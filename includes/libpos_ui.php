<?php
// Editing by   
/*
Change Log
2020-08-20 (Cameron): add MerchantSetting to GET_MODULE_OBJ_ARR()
2020-06-08 (Cameron): use $LAYOUT_SKIN to replace hardcode "2009a" for image path in Get_Manage_Pickup_Detail() and Get_Report_Pickup_Detail()
2020-06-05 (Cameron): modify GET_MODULE_OBJ_ARR(), show Management tab for current page is ManagePurchase or ManagePickup only
2020-05-14 (Ray): modify Get_POS_Transaction_Report_PrintTable, Get_POS_Transaction_Report_Export_Content hide keyword when empty
2020-05-07 (Ray): modify Get_POS_Sales_Report_PrintTable, Get_POS_Sales_Report_Export_Content, add YearName
2020-04-22 (Cameron): pass default RecordStatus=NC to pickup in GET_MODULE_OBJ_ARR() 
2020-04-16 (Cameron): modify Get_Manage_Pickup_Detail(), Get_POS_Transaction_Log_Detail() and Get_Report_Pickup_Detail() to handle case of all items are void 
2020-04-14 (Cameron): add hidden field Keyword_Index in Get_Management_Inventory_Table() for passing to log_view of an item
                    - add hidKeyword in Get_Settings_Category_UI() to remember the Keyword when return from item
                    - add parameter $hidKeyword to Get_Settings_Item_UI() and process it as hidden field to return to category
2020-04-07 (Cameron): add arguement ActionFrom (NotTake/Take) to checkItem in Get_Manage_Pickup_Detail()
                    - add parameter ActionFrom and method="post' to thickboxForm in Get_Writeoff_Thickbox_Content()
2020-04-04 (Cameron): add parameter $LogID to Get_Item_Selection_By_Price() and Get_Category_Selection_By_Price() to bypass Item that have been replaced before (not allow to change item back)
                    - pass argument $LogID to Get_Change_Item_Table() and Get_Category_Selection_By_Price()
2020-04-02 (Cameron): modify Get_Manage_Pickup_Detail_Table() to set defaut sorting order to TransactionTime for Void and Complete list 
2020-04-01 (Cameron): Fix: should use lang file instead of hardcode for column title in Get_Manage_Pickup_Detail_Table()
                    - add TransactionTime column in Get_Manage_Pickup_Detail_Table() for Complete and Void transaction
2020-03-17 (Tommy): - Modified Get_POS_Student_Report_Layout(), Get_POS_Student_Report_PrintTable(), Get_POS_Student_Report_Export_Content(),  add two extra column "Transaction Times" and "Last Transaction Date" Case (#P180125)
2020-03-05 (Ray):	Modified Get_Management_Inventory_UI add import
2020-03-03 (Tommy): modified Get_POS_Sales_Report_PrintLayout and Get_POS_Sales_Report_PrintTable for print page, fixed left and right side spaces , put class name and class number to next line
2020-02-05 (Cameron): disable image size restriction remark in Get_Category_Add_Edit_Layer() and Get_Item_Add_Edit_Layer()
2020-01-13 (Henry): modified Get_Change_Item_Table(), Get_Pickup_Item_Table() and Get_Writeoff_Item_Table() for bug fixing 
2020-01-10 (Cameron): pass argument $hasAvailableItem to Get_POS_Transaction_Purchase_Detail() in Get_Void_POS_Transaction_Form()
2019-10-29 (Henry): modified Get_Inventory_Edit_Layer() and Get_Item_Inventory_Change_Log_Printing_UI() to support cost price
2019-10-29 (Henry): add column CostPrice in Get_Item_Inventory_Change_Log_Table()
2019-10-16 (Henry): add flag $sys_custom['ePOS']['enablePurchaseAndPickup'] to Get_POS_Transaction_Report_PrintTable(), Get_POS_Transaction_Report_Export_Content() and Get_POS_Transaction_Report_Table()
2019-10-16 (Henry): add flag $sys_custom['ePOS']['enablePurchaseAndPickup'] to control display PurchaseOnBehalfOfStudent and Pickup or not in GET_MODULE_OBJ_ARR()
2019-10-15 (Cameron): pass argument $inventoryOnly=true to Get_All_Category() and Get_All_Items() in Get_Category_Selection_By_Price()
                      pass argument $inventoryOnly=true to Get_All_Items() in Get_Item_Selection_By_Price()
2019-10-10 (Cameron): add flag $sys_custom['ePOS']['exclude_webview'] to control display PurchaseOnBehalfOfStudent or not in GET_MODULE_OBJ_ARR()
2019-10-04 (Henry): Modified Get_Manage_Pickup_Detail()
2019-09-26 (Ray): add Menu ePOS print Receipt
2019-08-20 (Cameron): add linking PurchaseOnBehalfOfStudent
2019-07-16 (Cameron): add Description field in Get_Item_Add_Edit_Layer()
2019-01-03 (Henry): Added Get_Manage_Pickup_Index(), Get_Export_Form()
2018-07-20 (Anna): Modified Get_POS_Transaction_Report_Table(), added void for cancelled transaction report 
2018-02-06 (Carlos): Modified Get_Teminal_Setting_Form(), added [Need to confirm before paying?] option for Payment Method [Student Selection].
2017-08-18 (Carlos): Modified Get_Teminal_Manage_Table(), added enable/disable terminal status button.
2017-06-06 (Icarus): Modified Get_Teminal_Setting_Form(), change the CSS of the form to reach the UI consistency to the sample page.
2017-03-17 (Carlos): Modified the following functions below to fix sorting number type data, print and export versions to follow UI sorting field and order.
						Get_POS_Item_Report_Layout(), Get_POS_Item_Report_PrintLayout(), Get_POS_Item_Report_PrintTable(), Get_POS_Item_Report_Export_Content(), 
						Get_POS_Transaction_Report_Table(), Get_POS_Transaction_Report_PrintLayout(), Get_POS_Transaction_Report_PrintTable(), Get_POS_Transaction_Report_Export_Content()
						Get_POS_Student_Report_Layout(), Get_POS_Student_Report_PrintLayout(), Get_POS_Student_Report_PrintTable(), Get_POS_Student_Report_Export_Content()
2017-02-10 (Carlos): Modified Get_POS_Sales_Report_PrintTable(), Get_POS_Sales_Report_Export_Content(), added student name and item total amount. 
2016-03-24 (Carlos): Fixed Get_Inventory_Edit_Layer() form onsubmit return false to prevent Enter key submit the form.
2016-02-16 (Carlos): Modified Get_Management_Inventory_UI(), added [Export Change Log] button.
2015-10-22 (Carlos): Modified Set_Report_Date_Range_Cookies(), set ePayment cookies altogether.
2015-08-06 (Carlos): Modified Get_POS_Transaction_Report_Table(), Get_POS_Transaction_Report_PrintTable(), Get_POS_Transaction_Report_Export_Content(), Get_POS_Item_Report_Layout(),
					 Get_POS_Item_Report_PrintTable(), Get_POS_Item_Report_Export_Content(), added display of unit price info.
2015-08-05 (Carlos): - Modified report pages Get_POS_Sales_Report_PrintLayout(), Get_POS_Sales_Report_PrintTable(), Get_POS_Transaction_Report_PrintLayout(), 
						Get_POS_Transaction_Report_PrintTable(), Get_POS_Item_Report_PrintLayout(), Get_POS_Item_Report_PrintTable(), Get_POS_Item_Report_Detail(), 
						Get_POS_Student_Report_PrintLayout(), Get_POS_Student_Report_PrintTable(), Get_POS_Student_Report_Detail(), Get_Health_Class_Report(), 
						Get_Health_Personal_Report(), Get_POS_Transaction_Log_Detail(), display table header in every print page. 
					 - Added Set_Report_Date_Range_Cookies(), Get_Report_Date_Range_Cookies(), modified Get_POS_Transaction_Log_Detail(), GET_MODULE_OBJ_ARR(), 
					 	Get_POS_Transaction_Report_Table(), Get_POS_Item_Report_Layout(), Get_POS_Student_Report_Layout(), Get_POS_Sales_Report_Layout(), 
					 	Get_Health_Personal_Report_Layout(), Get_Health_Class_Report_Layout(), Get_Manage_Transaction_Index(), save report date range values to cookies for reuse when navigating between reports. 
2015-06-30 (Carlos): Modified Get_Settings_Item_UI() and Get_Settings_Item_Table(), added filter RecordStatus. 
					 In order to keep sorting order for drag and drop sorting, items are hidden only, not filter in query.
2015-03-02 (Carlos): Modified Get_Management_Inventory_UI() added print and export buttons
2015-02-26 (Carlos): $sys_custom['ePOS']['ReportWithSchoolName'] - Modified Get_POS_Sales_Report_PrintLayout() display school name.
2014-12-04 (Carlos): Added global $page_size to Get_Manage_Transaction_Index()
2014-10-17 (Carlos): [ip2.5.5.10.1] Modified Get_POS_Item_Report_Detail(), added print button and export csv button
2014-02-11 (Carlos): Modified Get_Settings_Item_Table(), Get_Settings_Category_Table() - added delete link for no transaction log items
					 Modified Get_POS_Transaction_Log_Detail(), Get_POS_Item_Report_Layout(), Get_POS_Item_Report_Detail(), 
					 	Get_POS_Item_Report_PrintTable(), Get_POS_Item_Report_Export_Content(), Get_POS_Student_Report_Detail() - add Category display field
2014-01-23 (Carlos): Modified Get_Teminal_Setting_Form() - added setting [PaymentMethod]
Date: 2010-09-28 [Kenneth Chung]
Detail: modify Get_Inventory_Edit_Layer, hide iframe and current inventory hidden value,
				discard iframe real time inventory check
*/
include_once('libinterface.php');

class libpos_ui extends interface_html {
	var $CurrencySign;
	function libpos_ui() {
		parent::interface_html();
		
		$this->thickBoxWidth = 750;
		$this->thickBoxHeight = 450;
		$this->toolCellWidth = "100px";
		
		$this->CurrencySign = '$';
	}
		
	function GET_MODULE_OBJ_ARR(){
		global $PATH_WRT_ROOT, $ip20TopMenu, $CurrentPage, $LAYOUT_SKIN, $Lang, $sys_custom;
		global $i_Payment_Menu_PrintPage, $i_Payment_Menu_PrintPage_Receipt;

		if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])){
			$this->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
		}else if(isset($_REQUEST['StartDate']) && isset($_REQUEST['EndDate'])){
			$this->Set_Report_Date_Range_Cookies($_REQUEST['StartDate'], $_REQUEST['EndDate']);
		}
		
		# Menu information
		$POSPath = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/pos/";
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
		    $MenuArr["Management"] = array($Lang['StaffAttendance']['Management'], "", ($CurrentPage['ManageTransaction'] || $CurrentPage['ManageInventory'] || $CurrentPage['ManagePurchase'] || $CurrentPage['ManagePickup']));
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) {
		    if (!$sys_custom['ePOS']['exclude_webview'] && $sys_custom['ePOS']['enablePurchaseAndPickup']) {
                $MenuArr["Management"]["Child"]["Purchase"] = array($Lang['ePOS']['PurchaseOnBehalfOfStudent'], "javascript: newWindow('/home/eClassApp/common/ePOS/index.php?FromAdmin=1',8)", $CurrentPage['ManagePurchase']);
            }
            $MenuArr["Management"]["Child"]["Attendance"] = array($Lang['ePOS']['Transaction'], $POSPath . "management/transaction/", $CurrentPage['ManageTransaction']);
        }
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Management"]["Child"]["OTRecords"] = array($Lang['ePOS']['Inventory'],$POSPath."management/inventory/inventory.php", $CurrentPage['ManageInventory']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $sys_custom['ePOS']['enablePurchaseAndPickup']) 
			$MenuArr["Management"]["Child"]["Pickup"] = array($Lang['ePOS']['PickupManagement'], $POSPath."management/pickup/?RecordStatus=NC", $CurrentPage['ManagePickup']);

		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) {
			if($sys_custom['ttmss_payment_receipt']) {
				$MenuArr["PrintPage"] = array($i_Payment_Menu_PrintPage, "", $CurrentPage['PrintPage_Receipt']);
				$MenuArr["PrintPage"]["Child"]["Receipt"] = array($i_Payment_Menu_PrintPage_Receipt, $POSPath."printpages/receipt/paymentlist.php", $CurrentPage['PrintPage_Receipt']);

			}
		}

		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"])
			$MenuArr["Report"] = array($Lang['StaffAttendance']['Report'], "", ($CurrentPage['ReportSales'] || $CurrentPage['ReportStudent'] || $CurrentPage['ReportItem'] || $CurrentPage['ReportTransaction'] || $CurrentPage['ReportHealthPersonal'] || $CurrentPage['ReportHealthClass']));
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Report"]["Child"]["Transaction"] = array($Lang['ePOS']['POSTransactionReport'], $POSPath."report/pos_transaction_report/", $CurrentPage['ReportTransaction']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Report"]["Child"]["Item"] = array($Lang['ePOS']['POSItemReport'], $POSPath."report/pos_item_report/", $CurrentPage['ReportItem']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Report"]["Child"]["Student"] = array($Lang['ePOS']['POSStudentReport'], $POSPath."report/pos_student_report/", $CurrentPage['ReportStudent']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Report"]["Child"]["Sales"] = array($Lang['ePOS']['POSSalesReport'], $POSPath."report/pos_sales_report/", $CurrentPage['ReportSales']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Report"]["Child"]["HealthPersonalReport"] = array($Lang['ePOS']['HealthPersonalReport'], $POSPath."report/health_personal/", $CurrentPage['ReportHealthPersonal']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Report"]["Child"]["HealthClassReport"] = array($Lang['ePOS']['HealthClassReport'], $POSPath."report/health_class/", $CurrentPage['ReportHealthClass']);
		
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Settings"] = array($Lang['StaffAttendance']['Settings'], "", ($CurrentPage['Terminal'] || $CurrentPage['Category&ItemSetting'] || $CurrentPage['HealthIngredientSetting']));
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Settings"]["Child"]["Category&Item"] = array($Lang['ePOS']['Category&ItemSetting'], $POSPath."setting/category_and_item/category.php", $CurrentPage['Category&ItemSetting']);
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Settings"]["Child"]["HealthIngredient"] = array($Lang['ePOS']['HealthIngredientSetting'], $POSPath."setting/health_ingredient/", $CurrentPage['HealthIngredientSetting']);
		/*
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Settings"]["Child"]["FunctionAccess"] = array($Lang['StaffAttendance']['FunctionAccess'], $POSPath."setting/access/", $CurrentPage['FunctionAccess']);
		*/
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) 
			$MenuArr["Settings"]["Child"]["Terminal"] = array($Lang['ePOS']['TerminalSetting'], $POSPath."setting/terminal/terminal.php", $CurrentPage['Terminal']);
        if ($sys_custom['ePayment']['MultiPaymentGateway'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]))
            $MenuArr["Settings"]["Child"]["Merchant"] = array($Lang['ePOS']['MerchantSetting'], $POSPath."setting/merchant/", $CurrentPage['MerchantSetting']);


        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass ePOS";'."\n";
        $js.= '</script>'."\n";

		# module information
		$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ePOS'].$js;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_POS.png";
		$MODULE_OBJ['root_path'] = $POSPath;
		$MODULE_OBJ['menu'] = $MenuArr;
		
		return $MODULE_OBJ;
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		include_once($PATH_WRT_ROOT.'home/eAdmin/GeneralMgmt/pos/common_js_lang.php');
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/2009a/js/pos.js"></script>
			
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
			';
			
		return $x;
	}
	
	// Remeber current using report date range values for navigating through different reports
	function Set_Report_Date_Range_Cookies($StartDate, $EndDate, $Expire=0)
	{
		$result_startdate = setcookie("ck_epos_report_start_date", $StartDate, $Expire, '/home/eAdmin/GeneralMgmt/pos', '', 0);
		$result_enddate = setcookie("ck_epos_report_end_date", $EndDate, $Expire, '/home/eAdmin/GeneralMgmt/pos', '', 0);
		
		setcookie("ck_epayment_report_start_date", $StartDate, $Expire, '/home/eAdmin/GeneralMgmt/payment', '', 0);
		setcookie("ck_epayment_report_end_date", $EndDate, $Expire, '/home/eAdmin/GeneralMgmt/payment', '', 0);
		
		return $result_startdate && $result_enddate;
	}
	
	function Get_Report_Date_Range_Cookies()
	{
		$start_date = $_COOKIE['ck_epos_report_start_date'];
		$end_date = $_COOKIE['ck_epos_report_end_date'];
		
		return array('StartDate'=>$start_date, 'EndDate'=>$end_date);
	}
	
	function Get_Settings_Category_UI($Keyword='', $IncludeSearchItem='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		
		# Add Icon
		$thisHref = '#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer';
		$thisOnclick = "js_Show_Category_Add_Edit_Layer(); return false;";
		$thisTag = 'title="'.$Lang['ePOS']['Add']['Category'].'"';
		$thisClass = 'thickbox';
		$AddIcon .= $this->GET_LNK_ADD($thisHref, '', $thisOnclick, $thisTag, $thisClass, 0);
		
		# SearchBox
		$IncludeItemChkBox = $this->Get_Checkbox('IncludeSearchItem', 'IncludeSearchItem', 1, $IncludeSearchItem, '', $Lang['ePOS']['IncludeItem']);
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		# Import Icon
		//$ImportIcon = $this->GET_LNK_IMPORT('#', "", "", "", "", $useThickBox=0);
		
		# Export Icon
		//$ExportIcon = $this->GET_LNK_EXPORT('#', "", "", "", "", $useThickBox=0);
		
		# Client Program Connection Shortcut
		$libpos = new libpos();
		$SettingsArr = $libpos->Get_POS_Settings();
		$AllowClientProgramConnect = $SettingsArr['AllowClientProgramConnect'];
		$thisActiveIconTitle 	= ($AllowClientProgramConnect==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
		$thisID = 'AllowClientProgramConnectIcon';
		$thisJS = 'javascript:js_Change_Client_Connection()';
		
		$ClientConnectShortCut = '';
		$ClientConnectShortCut .= '<div id="AllowClientProgramConnectDiv" style="float:right">';
			$ClientConnectShortCut .= '<span style="float:left;">';
				$ClientConnectShortCut .= $Lang['ePOS']['AllowClientProgramConnection'];
				$ClientConnectShortCut .= ' : ';
			$ClientConnectShortCut .= '</span>';
			$ClientConnectShortCut .= '<span class="table_row_tool" style="vertical-align:top;">';
				$ClientConnectShortCut .= $this->GET_LNK_ACTIVE($thisJS, $AllowClientProgramConnect, $thisActiveIconTitle, 0, $thisID);
			$ClientConnectShortCut .= '</span>';
		$ClientConnectShortCut .= '</div>';
		
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<form id="CategoryMainForm" name="CategoryMainForm" method="post" onsubmit="return false;">'."\n";
			$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$AddIcon.'</td>'."\n";
					$x .= '<td align="right">'.$IncludeItemChkBox.'&nbsp;&nbsp;'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $ImportIcon."\n";
						$x .= toolBarSpacer()."\n";
						$x .= $ExportIcon."\n";
					$x .= '</td>'."\n";
					$x .= '<td align="right">'.$ClientConnectShortCut.'</td>'."\n";
					
				$x .= '</tr>'."\n";
				
				### Category List Table
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= '<div id="CategoryDiv">'."\n";
							$x .= $this->Get_Settings_Category_Table($Keyword, $IncludeSearchItem);
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" id="CategoryID" name="CategoryID" value="" />'."\n";
			$x .= '<input type="hidden" id="hidKeyword" name="hidKeyword" value="" />'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Category_Table($Keyword='', $IncludeSearchItem=false, $TemplateID="")
	{
		global $Lang,$PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");

		### Get Category Data
		$ItemKeyword = ($IncludeSearchItem==false)? '' : $Keyword;
		$libCategory = new POS_Category();
		$CategoryArr = $libCategory->Get_All_Category($Keyword, 0, '', $ItemKeyword, $TemplateID);
		$numOfCategory = count($CategoryArr);
		
		$CategoryStatusArr = $libCategory->Get_RecordStatus_Count($TemplateID);
		$EnabledCount = $CategoryStatusArr['Enabled'];
		$DisabledCount = $CategoryStatusArr['Disabled'];
		
		$CategoryItemLogCount = $libCategory->Get_Category_Item_Log_Counting(array(), true);

		### Get Item Data
		$libItem = new POS_Item();
		$ItemArr = $libItem->Get_All_Items($ItemKeyword, $isAssoCategory=1);

		// Get Setting to see if the independent setting is on
		$libpos = new libpos();
		$SettingsArr = $libpos->Get_POS_Settings();
		$TerminalIndependentCat = $SettingsArr['TerminalIndependentCat'];
		
		$allowDragAndDrop = ($Keyword=='')? true : false;
		
		$x = '';
		$x .= '<div class="tabletext">'."\n";
			$x .= $Lang['General']['Enabled'].": \n";
			$x .= '<span id="enabledCountSpan">'.$EnabledCount.'</span>';
		$x .= '</div>'."\n";
		$x .= '<div class="tabletext">'."\n";
			$x .= $Lang['General']['Disabled'].": \n";
			$x .= '<span id="disabledCountSpan">'.$DisabledCount.'</span>';
		$x .= '</div>'."\n";
		$x .= '<table class="common_table_list" id="CategoryContentTable">'."\n";
			## Header
			$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th style="width:35%">'.$Lang['ePOS']['Name'].'</th>'."\n";
				$x .= '<th style="width:25%">'.$Lang['ePOS']['Code'].'</th>'."\n";
				$x .= '<th style="width:15%">'.$Lang['ePOS']['NoOfItems'].'</th>'."\n";
				$x .= '<th style="width:10%;">'.$Lang['ePOS']['Photo'].'</th>'."\n";
				$x .= '<th style="width:'.$this->toolCellWidth.'">&nbsp;</th>'."\n";
		if ($TemplateID == "")
				$x .= '<th style="width:1;">&nbsp;</th>'."\n";
			$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			if ($numOfCategory == 0)
			{
				$x .= '<tr><td colspan="4" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			else
			{
				$x .= '<tbody>'."\n";
				
					for ($i=0; $i<$numOfCategory; $i++)
					{
						$thisCategoryID 		= $CategoryArr[$i]['CategoryID'];
						$thisCategoryName 		= $CategoryArr[$i]['CategoryName'];
						$thisCategoryCode 		= $CategoryArr[$i]['CategoryCode'];
						$thisPhotoExt 			= $CategoryArr[$i]['PhotoExt'];
						$thisIsActive 			= $CategoryArr[$i]['RecordStatus'];
						$thisActiveIconTitle 	= ($thisIsActive==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
						
						$thisItemArr = $ItemArr[$thisCategoryID];
						$thisNumOfItem = count($thisItemArr);
						
						### Photo Icon
						if ($thisPhotoExt != '')
						{
							$thisPhotoIcon = '';
							$thisPhotoIcon .= '<span style="float:left">'."\n";
								$thisPhotoIcon .= $this->Get_Photo_Icon('#', "javascript:newWindow('".$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/pos/setting/category_and_item/view_photo.php?CategoryID=".$thisCategoryID."', 10)");
							$thisPhotoIcon .= '</span>'."\n";
							if ($TemplateID == "") 
								$thisPhotoIcon .= $this->GET_LNK_DELETE('#', $Lang['Btn']['RemovePhoto'], "js_Update_Category_Info('Delete_Photo', '$thisCategoryID');", '', 1);
						}
						else
							$thisPhotoIcon = '&nbsp;';
							
						### Category Name display
						if ($TemplateID == "") 
							$thisItemNumDisplay = '<a href="javascript:js_Go_Item_Page(\''.$thisCategoryID.'\');" title="'.$Lang['ePOS']['ItemSetting'].'" class="tablelink">'.$thisNumOfItem.'</a>';
						else 
							$thisItemNumDisplay = $thisNumOfItem;
						
						$x .= '<tr id="CategoryTr_'.$thisCategoryID.'">'."\n";
							$x .= '<td>'.$thisCategoryName.'</td>'."\n";
							$x .= '<td>'.$thisCategoryCode.'</td>'."\n";
							$x .= '<td>'.$thisItemNumDisplay.'</td>'."\n";
							$x .= '<td>'.$thisPhotoIcon.'</td>'."\n";
							$x .= '<td align="left">'."\n";
								$x .= '<div class="table_row_tool">'."\n";
									# Enable / Disable Icon
									$thisID = 'ActiveStatus_'.$thisCategoryID;
									if ($TemplateID == "") 
										$thisJS = 'javascript:js_Update_Category_Info(\'Change_Active_Status\', '.$thisCategoryID.')';
									else 
										$thisJS = 'javascript:Toggle_Category_Linkage('.$thisCategoryID.')';
									
									if ($TemplateID != "" || ($TemplateID == "" && !$TerminalIndependentCat))
										$x .= $this->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);
						if ($TemplateID == "") {
							if (!$TerminalIndependentCat)
									$x .= '<span> | </span>'."\n";
									# Edit Category
									$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
												$Lang['ePOS']['Edit']['Category'], "js_Show_Category_Add_Edit_Layer('$thisCategoryID');");
									
									# Delete Category Link
									if($CategoryItemLogCount[$thisCategoryID] == 0 || $CategoryItemLogCount[$thisCategoryID] == ''){
										$x .= $this->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "js_Delete_Category($thisCategoryID);", "", 0);
									}
									
						}
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						if ($TemplateID == "") {
							$x .= '<td class="Dragable">'."\n";
								if ($allowDragAndDrop == true)
								{
									$x .= '<div class="table_row_tool" style="float:right">'."\n";
										# Move
										$x .= $this->GET_LNK_MOVE("#", $Lang['ePOS']['Move']['Category']);
									$x .= '</div>'."\n";
								}
							$x .= '</td>'."\n";
						}
						$x .= '</tr>'."\n";
					}
					
				$x .= '</tbody>'."\n";
			}
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Category_Add_Edit_Layer($CategoryID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		$libCategory = new POS_Category();
		
		# Add / Edit
		$isEdit = ($CategoryID=='')? false : true;
		
		# Title
		$thisTitle = ($isEdit)? $Lang['ePOS']['Edit']['Category'] : $Lang['ePOS']['Add']['Category'];
		
		# Button Submit
		$btn_Submit = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_Update_Category_Info('Insert_Edit_Category', '".$CategoryID."');", $id="Btn_Submit");
		
		# Cancel
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
		
		# Show Category info if it is edit mode
		if ($isEdit == true)
		{
			$objCategory = new POS_Category($CategoryID);
			$CategoryName = intranet_htmlspecialchars($objCategory->CategoryName);
			$CategoryCode = intranet_htmlspecialchars($objCategory->CategoryCode);
			$LastModifiedDate = $objCategory->DateModify;
			$LastModifiedBy = $objCategory->ModifyBy;
		}
		
		$x = '';
		$x .= '<div id="debugArea"></div>'."\n";
		$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div class="edit_pop_board_write">'."\n";
				$x .= '<form id="CategoryForm" name="CategoryForm">'."\n";
					$x .= '<table class="form_table">'."\n";
						$x .= '<col class="field_title" />'."\n";
						$x .= '<col class="field_c" />'."\n";
						
						# Title input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Name'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="CategoryName" name="CategoryName" type="text" class="textbox" maxlength="'.$libCategory->MaxLength['CategoryName'].'" value="'.$CategoryName.'" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("NameWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Code input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Code'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="CategoryCode" name="CategoryCode" type="text" class="textbox" maxlength="'.$libCategory->MaxLength['CategoryCode'].'" value="'.$CategoryCode.'" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Photo Upload
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Photo'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="CategoryPhoto" name="CategoryPhoto" type="file" class="textboxtext" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("FileUploadWarningDiv");
								//$x .= '<span class="tabletextremark">('.$Lang['ePOS']['CategoryPhotoDimensionRemarks'].')</span>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					
					### iFrame for photo dimension checking
					$x .= '<iframe id="CheckDimensionFileUploadIframe" name="CheckDimensionFileUploadIframe" style="display:none" />'."\n";
					$x .= '<input type="hidden" id="IsPhotoDimensionValid" name="IsPhotoDimensionValid" value="0" />'."\n";
				$x .= '</form>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				if ($isEdit == true)
				{
					$LastModifiedDisplay = Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>'."\n";
				}
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $btn_Submit."\n";
				$x .= $btn_Cancel."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Item_UI($CategoryID, $Keyword='', $IncludeSearchItem=false, $hidKeyword='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		
		# Page Navigation
		$objCategory = new POS_Category($CategoryID);
		$NavigationArr = array();
		$NavigationArr[] = array($Lang['ePOS']['CategorySetting'], "javascript:js_Go_Category_Page();");
		$NavigationArr[] = array($objCategory->CategoryName, "");
		$NavigationHTML = $this->GET_NAVIGATION($NavigationArr);
		
		# Add Icon
		# 20100211 Ivan - Allow add item even if the category is disabled
		$CanAddItem = true;
		//$CanAddItem = ($objCategory->RecordStatus == 1)? true : false;
		if ($CanAddItem == true)
		{
			$thisHref = '#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer';
			$thisOnclick = "js_Show_Item_Add_Edit_Layer(''); return false;";
			$thisTag = 'title="'.$Lang['ePOS']['Add']['Item'].'"';
			$thisClass = 'thickbox';
			$AddIcon = $this->GET_LNK_ADD($thisHref, '', $thisOnclick, $thisTag, $thisClass, 0);
		}
		else
		{
			$AddIcon = '&nbsp;';
		}
		
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		# Import Icon
		//$ImportIcon = $this->GET_LNK_IMPORT('#', "", "", "", "", $useThickBox=0);
		
		# Export Icon
		//$ExportIcon = $this->GET_LNK_EXPORT('#', "", "", "", "", $useThickBox=0);
		
		# Client Program Connection Shortcut
		$libpos = new libpos();
		$SettingsArr = $libpos->Get_POS_Settings();
		$AllowClientProgramConnect = $SettingsArr['AllowClientProgramConnect'];
		$thisActiveIconTitle 	= ($AllowClientProgramConnect==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
		$thisID = 'AllowClientProgramConnectIcon';
		$thisJS = 'javascript:js_Change_Client_Connection()';
		
		$ClientConnectShortCut = '';
		$ClientConnectShortCut .= '<div id="AllowClientProgramConnectDiv" style="float:right">';
			$ClientConnectShortCut .= '<span style="float:left;">';
				$ClientConnectShortCut .= $Lang['ePOS']['AllowClientProgramConnection'];
				$ClientConnectShortCut .= ' : ';
			$ClientConnectShortCut .= '</span>';
			$ClientConnectShortCut .= '<span class="table_row_tool" style="vertical-align:top;">';
				$ClientConnectShortCut .= $this->GET_LNK_ACTIVE($thisJS, $AllowClientProgramConnect, $thisActiveIconTitle, 0, $thisID);
			$ClientConnectShortCut .= '</span>';
		$ClientConnectShortCut .= '</div>';
		
		# Category Filtering
		$CategoryFilter = $this->Get_Category_Selection('CategoryIDFilter', $CategoryID, $OnChange='js_Reload_Item_Table();');
		
		# Item RecordStatus Filter
		$RecordStatus = '';
		$recordStatusAry = array(
								array('1',$Lang['General']['Enabled']),
								array('0',$Lang['General']['Disabled'])
							);
		$RecordStatusFilter = getSelectByArray($recordStatusAry, ' id="RecordStatus" name="RecordStatus" onchange="js_Reload_Item_Table();" ', $RecordStatus, 1, 0, '', 1);
		
		# Back to Category Settings
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['ePOS']['Btn']['BackToCategorySettings'], "button", $onclick="js_Go_Category_Page();", $id="Btn_Back");
		
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<div>'."\n";
			$x .= '<form id="ItemMainForm" name="ItemMainForm" method="post" onsubmit="return false;">'."\n";
				$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$NavigationHTML.'</td>'."\n";
						$x .= '<td align="right">&nbsp;</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$AddIcon.'</td>'."\n";
						$x .= '<td align="right">&nbsp;</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$CategoryFilter.'&nbsp;'.$RecordStatusFilter.'</td>'."\n";
						$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= $ImportIcon."\n";
							$x .= toolBarSpacer()."\n";
							$x .= $ExportIcon."\n";
						$x .= '</td>'."\n";
						$x .= '<td align="right">'.$ClientConnectShortCut.'</td>'."\n";
					$x .= '</tr>'."\n";
					
					### Item List Table
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2">'."\n";
							$x .= '<div id="ItemDiv">'."\n";
								$x .= $this->Get_Settings_Item_Table($Keyword, $CategoryID, $RecordStatus);
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					/*
					### Remarks
					$x .= '<tr><td colspan="2" class="tabletext">'."\n";
						$x .= '<font color="red">* </font>'.$Lang['ePOS']['RepresentDisabledCategory']."\n";
					$x .= '</td></tr>'."\n";
					*/
				$x .= '</table>'."\n";
				$x .= '<input type="hidden" id="CategoryID" name="CategoryID" value="'.$CategoryID.'" />'."\n";
				$x .= '<input type="hidden" id="IncludeSearchItem" name="IncludeSearchItem" value="'.$IncludeSearchItem.'" />'."\n";
				$x .= '<input type="hidden" id="hidKeyword" name="hidKeyword" value="'.intranet_htmlspecialchars($hidKeyword).'" />'."\n";
			$x .= '</form>';
		$x .= '</div>';
		$x .= '<br style="clear:both" />'."\n";
		$x .= '<div class="edit_bottom" style="width:95%">'."\n";
			$x .= $btn_Cancel."\n";
			$x .= '<p class="spacer"></p>'."\n";
		$x .= '</div>'."\n";
			
		
		return $x;
	}
	
	### This function is no use now as the UI is changed (2010-01-28)
	function Get_Settings_Item_Table_Multiply_Category_View($Keyword='', $CategoryID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		### Get Category Data
		$libCategory = new POS_Category();
		$CategoryArr = $libCategory->Get_All_Category('', $ActiveOnly=0, $CategoryID);
		$numOfCategory = count($CategoryArr);
		
		### Get Item Data
		$libItem = new POS_Item();
		$ItemArr = $libItem->Get_All_Items($Keyword, $isAssoCategory=1);
		
		$allowDragAndDrop = ($Keyword=='')? true : false;
		
		$x = '';
		$x .= '<div class="tabletext">'."\n";
			$x .= $Lang['General']['Enabled'].": \n";
			$x .= '<span id="enabledCountSpan">'.$EnabledCount.'</span>';
		$x .= '</div>'."\n";
		$x .= '<div class="tabletext">'."\n";
			$x .= $Lang['General']['Disabled'].": \n";
			$x .= '<span id="disabledCountSpan">'.$DisabledCount.'</span>';
		$x .= '</div>'."\n";
		$x .= '<table class="common_table_list" id="ItemContentTable">'."\n";
			## Header
			$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th style="width:20%">'.$Lang['ePOS']['Category'].'</th>'."\n";
				$x .= '<th style="width:25%">'.$Lang['ePOS']['Name'].'</th>'."\n";
				$x .= '<th style="width:20%">'.$Lang['ePOS']['Barcode'].'</th>'."\n";
				$x .= '<th style="width:10%;">'.$Lang['ePOS']['UnitPrice'].'</th>'."\n";
				$x .= '<th style="width:10%;">'.$Lang['ePOS']['Photo'].'</th>'."\n";
				$x .= '<th>'."\n";
					/*
					# Add Item Button
					$x .= '<div class="table_row_tool row_content_tool">'."\n";
						$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add", 
								$Lang['ePOS']['Add']['Item'], "js_Show_Item_Add_Edit_Layer('', ''); return false;");
					$x .= '</div>'."\n";
					*/
					$x .= '&nbsp;'."\n";
				$x .= '</th>'."\n";
			$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			if ($numOfCategory == 0)
			{
				$x .= '<tr><td colspan="6" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			else
			{
				for ($i=0; $i<$numOfCategory; $i++)
				{
					$thisCategoryID = $CategoryArr[$i]['CategoryID'];
					$thisCategoryName = $CategoryArr[$i]['CategoryName'];
					$thisRecordStatus = $CategoryArr[$i]['RecordStatus'];
					$thisCanAddItem = ($thisRecordStatus==1)? true : false;
					
					/*
					if ($thisCanAddItem == false)
						$thisCategoryName = '<font color="red">*</font>'.$thisCategoryName;
					*/
					
					$thisItemArr = $ItemArr[$thisCategoryID];
					$numOfItemInCategory = count($thisItemArr);
					
					$firstAddRowDisplayStyle = '';
					if ($numOfItemInCategory > 0)
					{
						$firstAddRowDisplayStyle = 'style="display:none"';
						$rowspan = $numOfItemInCategory + 2;
					}
					else
					{
						$rowspan = 1;
					}
					
					$x .= '<tbody>'."\n";
						$x .= '<tr class="nodrop nodrag">'."\n";
							$x .= '<td rowspan="'.$rowspan.'">'.$thisCategoryName.'</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							# Add Item Button 
							$x .= '<td '.$firstAddRowDisplayStyle.'>'."\n";
								if ($thisCanAddItem == true)
								{
									$x .= '<div class="table_row_tool row_content_tool">'."\n";
										$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
												$Lang['ePOS']['Add']['Item'], "js_Show_Item_Add_Edit_Layer('', '$thisCategoryID'); return false;");
									$x .= '</div>'."\n";
								}
								else
								{
									$x .= '&nbsp;'."\n";
								}
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					
						# Display each Item in this Category
						foreach((array)$thisItemArr as $thisItemID => $thisItemInfoArr)
						{
							$thisItemName 			= $thisItemInfoArr['ItemName'];
							$thisBarcode 			= $thisItemInfoArr['Barcode'];
							$thisUnitPrice 			= $thisItemInfoArr['UnitPrice'];
							$thisPhotoExt 			= $thisItemInfoArr['PhotoExt'];
							$thisIsActive 			= $thisItemInfoArr['RecordStatus'];
							$thisItemName 			= $thisItemInfoArr['ItemName'];
							$thisActiveIconTitle 	= ($thisIsActive==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
							
							$thisBarcode = ($thisBarcode == '')? '&nbsp;' : $thisBarcode;
							
							### Photo Icon
							if ($thisPhotoExt != '')
							{
								$thisPhotoIcon = '';
								$thisPhotoIcon .= '<span style="float:left">'."\n";
									$thisPhotoIcon .= $this->Get_Photo_Icon('#', "javascript:newWindow('view_photo.php?ItemID=".$thisItemID."', 10)");
								$thisPhotoIcon .= '</span>'."\n";
								$thisPhotoIcon .= $this->GET_LNK_DELETE('#', $Lang['Btn']['RemovePhoto'], "js_Update_Item_Info('Delete_Photo', '$thisItemID');", '', 1);
							}
							else
								$thisPhotoIcon = '&nbsp;';
							
							$x .= '<tr id="ItemTr_'.$thisItemID.'" class="sub_row">'."\n";
								$x .= '<td>'.$thisItemName.'</td>'."\n";
								$x .= '<td>'.$thisBarcode.'</td>'."\n";
								$x .= '<td>'.$thisUnitPrice.'</td>'."\n";
								$x .= '<td>'.$thisPhotoIcon.'</td>'."\n";
								
								$x .= '<td class="Dragable" align="left">'."\n";
									$x .= '<div class="table_row_tool">'."\n";
										# Enable / Disable Icon
										$thisID = 'ActiveStatus_'.$thisItemID;
										$thisJS = 'javascript:js_Update_Item_Info(\'Change_Active_Status\', '.$thisItemID.')';
										$x .= $this->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);
										
										$x .= '<span> | </span>'."\n";
										
										# Edit Item
										$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
													$Lang['ePOS']['Edit']['Item'], "js_Show_Item_Add_Edit_Layer('$thisItemID');");
									$x .= '</div>';
									
									$x .= '<div style="float:left">';
										# Edit Item Health Ingredient
										$thisHref = 'item_health_ingredient.php?ItemID='.$thisItemID;
										$x .= $this->GET_ACTION_LNK($thisHref, $Lang['ePOS']['Manage']['HealthIngredient'], $ParOnClick="", $ParClass="copy_dim");
									$x .= '</div>';
									
									if ($allowDragAndDrop == true)
									{
										$x .= '<div class="table_row_tool" style="float:right">'."\n";
											# Move
											$x .= $this->GET_LNK_MOVE("#", $Lang['ePOS']['Move']['Item']);
										$x .= '</div>'."\n";
									}
								$x .= '</td>'."\n";
							
							$x .= '</tr>'."\n";
						}
						
						# Add Item Row 
						$lastAddRowDisplayStyle = '';
						if ($numOfItemInCategory == 0)
							$lastAddRowDisplayStyle = 'style="display:none"';
						
						$x .= '<tr class="nodrop nodrag" '.$lastAddRowDisplayStyle.'>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							# Add Item Button 
							$x .= '<td>'."\n";
								if ($thisCanAddItem == true)
								{
									$x .= '<div class="table_row_tool row_content_tool">'."\n";
										$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
												$Lang['ePOS']['Add']['Item'], "js_Show_Item_Add_Edit_Layer('', '$thisCategoryID'); return false;");
									$x .= '</div>'."\n";
								}
								else
								{
									$x .= '&nbsp;'."\n";
								}
								
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</tbody>'."\n";
				}	// for ($i=0; $i<$numOfCategory; $i++)
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Item_Table($Keyword='', $CategoryID='', $RecordStatus='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		### Get Category Data
		$objCategory = new POS_Category($CategoryID);
		$ItemArr = $objCategory->Get_All_Items($ActiveOnly=0, $Keyword);
		$numOfItem = count($ItemArr);
		$categoryItemLogCount = $objCategory->Get_Category_Item_Log_Counting(array($CategoryID));
		
		$thisRecordStatus = $objCategory->RecordStatus;
		$thisCanAddItem = ($thisRecordStatus==1)? true : false;
		
		$allowDragAndDrop = ($Keyword=='')? true : false;
		
		$x = '';
		$x .= '<table class="common_table_list" id="ItemContentTable">'."\n";
			## Header
			$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th style="width:35%">'.$Lang['ePOS']['Name'].'</th>'."\n";
				$x .= '<th style="width:30%">'.$Lang['ePOS']['Barcode'].'</th>'."\n";
				$x .= '<th style="width:10%;">'.$Lang['ePOS']['UnitPrice'].'</th>'."\n";
				$x .= '<th style="width:10%;">'.$Lang['ePOS']['Photo'].'</th>'."\n";
				$x .= '<th style="width:'.$this->toolCellWidth.'">&nbsp;</th>'."\n";
				$x .= '<th style="width:1;">&nbsp;</th>'."\n";
			$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				for ($i=0; $i<$numOfItem; $i++)
				{
					$thisItemID 			= $ItemArr[$i]['ItemID'];
					$thisItemName 			= $ItemArr[$i]['ItemName'];
					$thisBarcode 			= $ItemArr[$i]['Barcode'];
					$thisUnitPrice 			= $ItemArr[$i]['UnitPrice'];
					$thisPhotoExt 			= $ItemArr[$i]['PhotoExt'];
					$thisIsActive 			= $ItemArr[$i]['RecordStatus'];
					
					$thisActiveIconTitle 	= ($thisIsActive==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
					$thisBarcode = ($thisBarcode == '')? '&nbsp;' : $thisBarcode;
							
					### Photo Icon
					if ($thisPhotoExt != '')
					{
						$thisPhotoIcon = '';
						$thisPhotoIcon .= '<span style="float:left">'."\n";
							$thisPhotoIcon .= $this->Get_Photo_Icon('#', "javascript:newWindow('view_photo.php?ItemID=".$thisItemID."', 10)");
						$thisPhotoIcon .= '</span>'."\n";
						$thisPhotoIcon .= $this->GET_LNK_DELETE('#', $Lang['Btn']['RemovePhoto'], "js_Update_Item_Info('Delete_Photo', '$thisItemID');", '', 1);
					}
					else
						$thisPhotoIcon = '&nbsp;';
					
					if($thisIsActive == 1)
					{
						$display_css = $RecordStatus == '0' ? ' style="display:none" ' : '';
					}else{
						$display_css = $RecordStatus == '1' ? ' style="display:none" ' : '';
					}
					
					$x .= '<tr id="ItemTr_'.$thisItemID.'" '.$display_css.'>'."\n";
						$x .= '<td>'.$thisItemName.'</td>'."\n";
						$x .= '<td>'.$thisBarcode.'</td>'."\n";
						$x .= '<td>$'.$thisUnitPrice.'</td>'."\n";
						$x .= '<td>'.$thisPhotoIcon.'</td>'."\n";
						
						$x .= '<td align="left">'."\n";
							$x .= '<div class="table_row_tool">'."\n";
								# Enable / Disable Icon
								$thisID = 'ActiveStatus_'.$thisItemID;
								$thisJS = 'javascript:js_Update_Item_Info(\'Change_Active_Status\', '.$thisItemID.')';
								$x .= $this->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID)."\n";
								
								$x .= '<span> | </span>'."\n";
								
								# Edit Item
								$x .= $this->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
											$Lang['ePOS']['Edit']['Item'], "js_Show_Item_Add_Edit_Layer('$thisItemID');");
								
								# Delete Item 
								if($categoryItemLogCount[$CategoryID][$thisItemID] == 0 || $categoryItemLogCount[$CategoryID][$thisItemID] == ''){
									$x .= $this->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "js_Delete_Item($thisItemID);", "", 0);
								}
											
								# Edit Item Health Ingredient
								$thisHref = 'item_health_ingredient.php?ItemID='.$thisItemID;
								$thisTitle = $Lang['ePOS']['Manage']['HealthIngredient'];
								$x .= '<a href="'.$thisHref.'" class="health_ingredient_dim" title="'.$thisTitle.'"></a>'."\n";
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td class="Dragable">'."\n";
							if ($allowDragAndDrop == true)
							{
								$x .= '<div class="table_row_tool" style="float:right">'."\n";
									# Move
									$x .= $this->GET_LNK_MOVE("#", $Lang['ePOS']['Move']['Item'])."\n";
								$x .= '</div>'."\n";
							}
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Item_Add_Edit_Layer($ItemID='', $CategoryID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		$libItem = new POS_Item();
		
		# Add / Edit
		$isEdit = ($ItemID=='')? false : true;
		
		# Title
		$thisTitle = ($isEdit)? $Lang['ePOS']['Edit']['Item'] : $Lang['ePOS']['Add']['Item'];
		
		# Button Submit
		$btn_Submit = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_Update_Item_Info('Insert_Edit_Item', '".$ItemID."');", $id="Btn_Submit");
		
		# Cancel
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
							
		# Show Category info if it is edit mode
		if ($isEdit == true)
		{
			$objItem = new POS_Item($ItemID);
			$CategoryID = $objItem->CategoryID;
			$ItemName = intranet_htmlspecialchars($objItem->ItemName);
			$Barcode = intranet_htmlspecialchars($objItem->Barcode);
			$UnitPrice = intranet_htmlspecialchars($objItem->UnitPrice);
            $Description = intranet_htmlspecialchars($objItem->Description);
			$LastModifiedDate = $objItem->DateModify;
			$LastModifiedBy = $objItem->ModifyBy;
		}
		
		$x = '';
		$x .= '<div id="debugArea"></div>'."\n";
		$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div class="edit_pop_board_write">'."\n";
				$x .= '<form id="ItemForm" name="ItemForm">'."\n";
					$x .= '<table class="form_table">'."\n";
						$x .= '<col class="field_title" />'."\n";
						$x .= '<col class="field_c" />'."\n";
						
						# Category Selection
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Category'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->Get_Category_Selection('ItemCategoryID', $CategoryID);
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Name input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Name'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="ItemName" name="ItemName" type="text" class="textbox" maxlength="'.$libItem->MaxLength['ItemName'].'" value="'.$ItemName.'" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("NameWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Barcode input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Barcode'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Barcode" name="Barcode" type="text" class="textbox" maxlength="'.$libItem->MaxLength['ItemBarcode'].'" value="'.$Barcode.'" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("BarcodeWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Unit Price input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['UnitPrice'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="UnitPrice" name="UnitPrice" type="text" class="textbox" value="'.$UnitPrice.'" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("UnitPriceWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";

                        # Description
                        $x .= '<tr>'."\n";
                            $x .= '<td>'.$Lang['ePOS']['Description'].'</td>'."\n";
                            $x .= '<td>:</td>'."\n";
                            $x .= '<td>'."\n";
                                $x .= '<textarea id="Description" name="Description" cols="75" rows="4" wrap="virtual" value="'.$Description.'">'.$Description.'</textarea>'."\n";
                            $x .= '</td>'."\n";
                        $x .= '</tr>'."\n";

						# Photo Upload
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Photo'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="ItemPhoto" name="ItemPhoto" type="file" class="textboxtext" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("FileUploadWarningDiv");
								//$x .= '<span class="tabletextremark">('.$Lang['ePOS']['ItemPhotoDimensionRemarks'].')</span>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					
					### iFrame for photo dimension checking
					$x .= '<iframe id="CheckDimensionFileUploadIframe" name="CheckDimensionFileUploadIframe" style="display:none" />'."\n";
					$x .= '<input type="hidden" id="IsPhotoDimensionValid" name="IsPhotoDimensionValid" value="0" />'."\n";
				$x .= '</form>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				if ($isEdit == true)
				{
					$LastModifiedDisplay = Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>'."\n";
				}
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $btn_Submit."\n";
				$x .= $btn_Cancel."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Item_Health_Ingredient_UI($ItemID)
	{
		global $Lang;
		
		# Add Icon
		$thisHref = '#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer';
		$thisOnclick = "js_Show_Category_Add_Edit_Layer(); return false;";
		$thisTag = 'title="'.$Lang['ePOS']['Add']['Category'].'"';
		$thisClass = 'thickbox';
		$AddIcon .= $this->GET_LNK_ADD($thisHref, '', $thisOnclick, $thisTag, $thisClass, 0);
		
		# SearchBox
		//$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		# Import Icon
		//$ImportIcon = $this->GET_LNK_IMPORT('#', "", "", "", "", $useThickBox=0);
		
		# Export Icon
		//$ExportIcon = $this->GET_LNK_EXPORT('#', "", "", "", "", $useThickBox=0);
		
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<form id="ItemHealthIngredientForm" name="ItemHealthIngredientForm" method="post" onsubmit="return false;">'."\n";
			$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$AddIcon.'</td>'."\n";
					$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= $ImportIcon."\n";
						$x .= toolBarSpacer()."\n";
						$x .= $ExportIcon."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				### Item Health Ingredient List Table
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= '<div id="CategoryDiv">'."\n";
							$x .= $this->Get_Settings_Item_Health_Ingredient_Table($ItemID);
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" id="ItemID" name="ItemID" value="'.$ItemID.'" />'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Management_Inventory_UI($CategoryID='', $Keyword='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $Lang, $PATH_WRT_ROOT;
		
		$today_ts = time();
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		$StartDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		$EndDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		
		# Category Filtering
		$CategoryFilter = $this->Get_Category_Selection('CategoryIDFilter', $CategoryID, $OnChange='js_Reload_Inventory_Table();', 0, 1, 1, 0);
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		$PrintIcon = $this->GET_LNK_PRINT("javascript:openPrintPage();","","","","",0);
		
		# Import Icon
		$ImportIcon = $this->GET_LNK_IMPORT("javascript:checkNew('import.php')", "", "", "", "", $useThickBox=0);
		
		# Export Icon
		$ExportIcon = $this->GET_LNK_EXPORT('javascript:exportPage();', "", "", "", "", 0);
		
		# Export Change Log Icon
		$ExportChangeLogButton = $this->GET_LNK_EXPORT('javascript:void(0);', $Lang['ePOS']['ExportChangeLog'], "$('#exportChangeLogLayer').show();", "", "", 0);
		$ExportChangeLogLayer = '<div class="Conntent_tool">
									<div class="print_option_layer" id="exportChangeLogLayer" style="width:208px;left:300px;visibility:visible;display:none;">
				                 	<em> - '.$Lang['General']['ExportOptions'].' - </em>
				                    	<table class="form_table">
				                            <colgroup>
												<col class="field_title">
				                            	<col class="field_c">
				                            </colgroup>
											<tbody>
												<tr>
					                              	<td width="15%">'.$Lang['General']['From'].'</td>
					                              	<td>'.$this->GET_DATE_PICKER("StartDate",$StartDate).'</td>
				                            	</tr>
				                            	<tr>
				                              		<td width="15%">'.$Lang['General']['To'].'</td>
				                             		<td>'.$this->GET_DATE_PICKER("EndDate",$EndDate).'</td>
				                            	</tr>
				                          	</tbody>
										</table>
				                        <div class="edit_bottom">
				                            <p class="spacer"></p>
				                            '.$this->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "exportChangeLog();", "submitChangeLogBtn").'
											'.$this->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", '$(\'#exportChangeLogLayer\').hide();', "cancelChangeLogBtn").'
				                            <p class="spacer"></p>
				                        </div>
				                  </div>
								</div>';
		
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<div>'."\n";
			$x .= '<form id="form1" name="form1" method="post" action="inventory.php" onsubmit="return false;">'."\n";
				$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$CategoryFilter.'</td>'."\n";
						$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2">'."\n";
							$x .= $PrintIcon."\n";
							$x .= toolBarSpacer()."\n";
							$x .= $ExportIcon."\n";
							$x .= toolBarSpacer()."\n";
							$x .= $ExportChangeLogButton;
							$x .= $ExportChangeLogLayer;
							$x .= toolBarSpacer()."\n";
							$x .= $ImportIcon."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					### Item List Table
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" align="center">'."\n";
							$x .= '<div>'."\n";
								$x .= $this->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['ePOS']['InventoryNotRealTimeRemarks']);
							$x .= '</div>'."\n";
							$x .= '<div id="InventoryTableDiv" style="text-align:left;">'."\n";
								$x .= $this->Get_Management_Inventory_Table($CategoryID, $Keyword, $PageNumber, $PageSize, $Order, $SortField)."\n";
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</form>'."\n";
		$x .= '</div>';
		$x .= '<br style="clear:both" />'."\n";
		
		return $x;
	}
	
	function Get_Management_Inventory_Table($CategoryID='', $Keyword='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		
		$libPOS = new libpos();
		
		# Default Table Settings
		$PageNumber = ($PageNumber == '')? 1 : $PageNumber;
		$PageSize = ($PageSize == '')? 20 : $PageSize;
		$Order = ($Order == '')? 1 : $Order;
		$SortField = ($SortField == '')? 0 : $SortField;
		
		# TABLE INFO
		$li = new libdbtable2007($SortField, $Order, $PageNumber);
		$li->field_array = array("item.ItemName",
								 "cat.CategoryName",
								 "item.Barcode",
								 "item.ItemCount",
								 "item.UnitPrice",
								 "item.InventoryDateModify",
								 "item.InventoryModifyBy"
								 );
		$li->sql = $libPOS->Get_Management_Inventory_Index_Sql($CategoryID, $Keyword);
		$li->no_col = sizeof($li->field_array) + 3;		// #, Photo, Functional Icons
		$li->title = "";
		$li->column_array = array(0,0,0,0);
		$li->wrap_array = array(0,0,0,0);
		$li->IsColOff = 2;
		$li->page_size = $PageSize;
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='1' class='tableTitle'>#</td>\n";
		$li->column_list .= "<td width='20%' class='tableTitle'>".$li->column($pos++, $Lang['ePOS']['Item'])."</td>\n";
		$li->column_list .= "<td width='15%' class='tableTitle'>".$li->column($pos++, $Lang['ePOS']['Category'])."</td>\n";
		$li->column_list .= "<td width='10%' class='tableTitle'>".$li->column($pos++, $Lang['ePOS']['Barcode'])."</td>\n";
		$li->column_list .= "<td width='8%' class='tableTitle'>".$li->column($pos++, $Lang['ePOS']['Inventory'])."</td>\n";
		$li->column_list .= "<td width='8%' class='tableTitle'>".$li->column($pos++, $Lang['ePOS']['UnitPrice'])."</td>\n";
		$li->column_list .= "<td width='5%' class='tableTitle'>".$li->column($pos++, $Lang['ePOS']['Photo'])."</td>\n";
		$li->column_list .= "<td width='14%' class='tableTitle'>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tableTitle'>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</td>\n";
		$li->column_list .= "<td width='13%' class='tableTitle'>&nbsp;</td>\n";
		
		$x = '';
		$x .= '<div class="tabletextremark">'.$Lang['ePOS']['PageLoadingTime'].': <span id="PageLoadingTimeSpan">'.date("Y-m-d h:i:s").'</div>'."\n";
		$x .= $li->display()."\n";
		$x .= '	<input type="hidden" id="pageNo" name="pageNo" value="'.$li->pageNo.'">
				<input type="hidden" id="order" name="order" value="'.$li->order.'">
				<input type="hidden" id="field" name="field" value="'.$li->field.'">
				<input type="hidden" id="page_size_change" name="page_size_change" value="">
				<input type="hidden" id="numPerPage" name="numPerPage" value="'.$li->page_size.'">
                <input type="hidden" id="Keyword_Index" name="Keyword_Index" value="">
				'."\n";
		
		return $x;
	}
	
	function Get_Inventory_Edit_Layer($ItemID, $ForIncrease=1)
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		$libItem = new POS_Item();
		
		# Title
		if ($ForIncrease == 1)
		{
			$thisTitle = $Lang['ePOS']['IncreaseInventory'];
			$ActionTitle = $Lang['ePOS']['IncreaseBy'];
			$jsSubmitOnclick = "js_Update_Inventory('".$ItemID."', 1);";
		}
		else
		{
			$thisTitle = $Lang['ePOS']['DecreaseInventory'];
			$ActionTitle = $Lang['ePOS']['DecreaseBy'];
			$jsSubmitOnclick = "js_Update_Inventory('".$ItemID."', 0);";
		}
		
		# Button Submit
		$btn_Submit = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $jsSubmitOnclick, $id="Btn_Submit");
		
		# Cancel
		$btn_Cancel = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Close_Edit_Layer()", $id="Btn_Cancel");
							
		# Show Category info if it is edit mode
		$objItem = new POS_Item($ItemID);
		$ItemName = $objItem->ItemName;
		$Barcode = $objItem->Barcode;
		//$Inventory = $objItem->ItemCount;
		$UnitPrice = $objItem->UnitPrice;
		$LastModifiedDate = $objItem->InventoryDateModify;
		$LastModifiedBy = $objItem->InventoryModifyBy;
		
		$CategoryID = $objItem->CategoryID;
		$objCategory = new POS_Category($CategoryID);
		$CategoryName = $objCategory->CategoryName;
		
		$x = '';
		$x .= '<div id="debugArea"></div>'."\n";
		$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div class="edit_pop_board_write">'."\n";
				$x .= '<form id="InventoryForm" name="InventoryForm" onsubmit="return false;">'."\n";
					$x .= '<table class="form_table">'."\n";
						$x .= '<col class="field_title" />'."\n";
						$x .= '<col class="field_c" />'."\n";
						
						# Item Name
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Item'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $ItemName."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Category Name
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Category'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $CategoryName."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Item Barcode
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Barcode'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $Barcode."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						/*
						# Item Current Inventory
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['CurrentInventory'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<span id="ItemInventorySpan">'.$Inventory.'</span>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						*/
						
						# Item Unit Price
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['UnitPrice'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $UnitPrice."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						if ($ForIncrease == 1)
						{
						# Cost Price
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['CostPrice'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="CostPrice" name="CostPrice" type="text" class="textboxnum" value="" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("CostPriceWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						}
						
						# Increase / Decrease By
						$x .= '<tr>'."\n";
							$x .= '<td>'.$ActionTitle.'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="AdjustedBy" name="AdjustedBy" type="text" class="textboxnum" value="" />'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("AdjustedByWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Remarks
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['General']['Remark'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->GET_TEXTAREA('Remarks', '');
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					
					/*
					### iFrame for real-time update inventory count
					$thisSrc = "ajax_reload.php?Action=Item_Inventory_Count&ItemID=".$ItemID;
					$x .= '<iframe id="GetLiveInventoryIframe" name="GetLiveInventoryIframe" src="'.$thisSrc.'" style="display:none"/>'."\n";
					*/
					
					// updated inventory will store in this hidden field for js inventory checking
					//$x .= '<input type="hidden" id="CurrentInventory" name="CurrentInventory" value="'.$Inventory.'" />'."\n";
				$x .= '</form>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				if ($LastModifiedDate != '' && $LastModifiedBy != '')
				{
					$LastModifiedDisplay = Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedBy);
					$x .= '<span>'.$LastModifiedDisplay.'</span>'."\n";
				}
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $btn_Submit."\n";
				$x .= $btn_Cancel."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Item_Inventory_Change_Log_UI($ItemID, $StartDate='', $EndDate='', $IndexInfoArr=array(), $Keyword='', $PageNumber='', $PageSize='', $Order='', $SortField='', $LogType='')
	{
		global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		
		### Get Item Info
		$objItem = new POS_Item($ItemID);
		$ItemName = $objItem->ItemName;
		$Barcode = $objItem->Barcode;
		
		$objCategory = new POS_Category($objItem->CategoryID);
		$CategoryName = $objCategory->CategoryName;
		
		### Page Navigation
		$NavigationArr = array();
		$NavigationArr[] = array($Lang['ePOS']['Inventory'], "javascript:js_Back_To_Inventory_Index();");
		$NavigationArr[] = array($ItemName, '');
		$NavigationArr[] = array($Lang['ePOS']['InventoryUpdateLog'], '');
		$NavigationHTML = $this->GET_NAVIGATION($NavigationArr);
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($StartDate=="")
		  $StartDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($EndDate=="")
		  $EndDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
			
		### Print Button
		$PrintIcon = $this->GET_LNK_PRINT('javascript:js_Go_Print('.$ItemID.')', '', '', '', '', 0);
		
		# SearchBox
		$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		# Export Icon
		//$ExportIcon = $this->GET_LNK_EXPORT('#', "", "", "", "", $useThickBox=0);
		
		### Back Button
		$btn_Back = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Back_To_Inventory_Index()", $id="Btn_Back");
		
		### Start Date Picker
		$StartDatePicker = $this->GET_DATE_PICKER('StartDate', $StartDate);
		
		### End Date Picker
		$EndDatePicker = $this->GET_DATE_PICKER('EndDate', $EndDate);
		
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<div>'."\n";
			$x .= '<form id="form1" name="form1" method="post" onsubmit="return false;">'."\n";
				$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$NavigationHTML.'</td>'."\n";
						$x .= '<td align="right">&nbsp;</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>';
				
				$x .= '<br style="clear:both">';
					
				$x .= '<table width="95%" border="0" cellpadding="2" cellspacing="0">'."\n";
					# Category Name
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['Category'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$CategoryName.'</td>'."\n";
					$x .= '</tr>'."\n";
					# Item Name
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['Item'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$ItemName.'</td>'."\n";
					$x .= '</tr>'."\n";
					# Barcode
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['Barcode'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Barcode.'</td>'."\n";
					$x .= '</tr>'."\n";
					# Start Date
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['From'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$StartDatePicker.'<span class="tabletextremark">(yyyy-mm-dd)</span></td>'."\n";
					$x .= '</tr>'."\n";
					# End Date
					$x .= '<tr>'."\n";
						$x .= '<td class="formfieldtitle tabletext">'.$Lang['ePOS']['To'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$EndDatePicker.'<span class="tabletextremark">(yyyy-mm-dd)</span></td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td class="formfieldtitle tabletext">'.$Lang['ePOS']['IncludeTransactionDetail'].'</td>'."\n";
						$x .= '<td class="tabletext"><input type="checkbox" id="LogType" value="1" name="LogType" '.(($LogType == "1")? "CHECKED":"").'></td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" class="dotline"><img src="{$image_path}/{$LAYOUT_SKIN}/10x10.gif" width="10" height="1" /></td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" align="center" class="tabletext">'."\n";
							$x .= $this->GET_ACTION_BTN($Lang['Btn']['View'], "button", "js_Reload_Report_Table()")."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				
				$x .= '<br style="clear:both">';
					
				$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$PrintIcon.'</td>'."\n";
						$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
					$x .= '</tr>'."\n";
					
					### Item List Table
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" align="center">'."\n";
							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Item_Inventory_Change_Log_Table($ItemID, $StartDate, $EndDate, $Keyword, $PageNumber, $PageSize, $Order, $SortField, $LogType)."\n";
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				
				$x .= '<input type="hidden" id="ItemID" name="ItemID" value="'.$ItemID.'" />'."\n";
				
				### Hidden value to preserve index page table view
				$x .= '<input type="hidden" id="Keyword_Index" name="Keyword_Index" value="'.intranet_htmlspecialchars($IndexInfoArr['Keyword']).'" />'."\n";
				$x .= '<input type="hidden" id="CategoryIDFilter_Index" name="CategoryIDFilter_Index" value="'.$IndexInfoArr['CategoryIDFilter'].'" />'."\n";
				$x .= '<input type="hidden" id="pageNo_Index" name="pageNo_Index" value="'.$IndexInfoArr['pageNo'].'" />'."\n";
				$x .= '<input type="hidden" id="order_Index" name="order_Index" value="'.$IndexInfoArr['order'].'" />'."\n";
				$x .= '<input type="hidden" id="field_Index" name="field_Index" value="'.$IndexInfoArr['field'].'" />'."\n";
				$x .= '<input type="hidden" id="num_per_page_Index" name="num_per_page_Index" value="'.$IndexInfoArr['num_per_page'].'" />'."\n";
			$x .= '</form>'."\n";
		$x .= '</div>';
		$x .= '<br style="clear:both" />'."\n";
		$x .= '<div class="edit_bottom" style="width:95%">'."\n";
			$x .= $btn_Back."\n";
			$x .= '<p class="spacer"></p>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Item_Inventory_Change_Log_Table($ItemID, $StartDate, $EndDate, $Keyword, $PageNumber='', $PageSize='', $Order='', $SortField='', $LogType='')
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		### Get Log Info
		$objItem = new POS_Item($ItemID);
		$LogArr = $objItem->Get_Item_Log_Info($StartDate, $EndDate);
		
		$libPOS = new libpos();
		
		# Default Table Settings
		$PageNumber = ($PageNumber == '')? 1 : $PageNumber;
		$PageSize = ($PageSize == '')? 20 : $PageSize;
		$Order = ($Order == '')? 0 : $Order;
		$SortField = ($SortField == '')? 0 : $SortField;
		
		# TABLE INFO
		$namefield = getNameFieldByLang("iu.");
		$li = new libdbtable2007($SortField, $Order, $PageNumber);
		$li->field_array = array("log.DateInput");
		$li->sql = $libPOS->Get_Management_Inventory_Log_Sql($ItemID, $StartDate, $EndDate, $Keyword, '',$LogType);
		$li->no_col = sizeof($li->field_array) + 7;
		$li->title = "";
		$li->column_array = array(0,0,0,0);
		$li->wrap_array = array(0,0,0,0);
		$li->IsColOff = 2;
		$li->page_size = $PageSize;
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='1' class='tablebluetop tableTitle'>#</td>\n";
		$li->column_list .= "<td width='20%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePOS']['UpdateTime'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tablebluetop tableTitle'>".$Lang['ePOS']['UpdatedBy']."</td>\n";
		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$Lang['ePOS']['IncreaseBy']."</td>\n";
		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$Lang['ePOS']['DecreaseBy']."</td>\n";
		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$Lang['ePOS']['InventoryAfter']."</td>\n";
		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$Lang['ePOS']['CostPrice']."</td>\n";
		$li->column_list .= "<td width='20%' class='tablebluetop tableTitle'>".$Lang['General']['Remark']."</td>\n";
		
		
		$x = '';
		$x .= $li->displayFormat2('100%', "blue")."\n";
		$x .= '	<input type="hidden" id="pageNo" name="pageNo" value="'.$li->pageNo.'">
				<input type="hidden" id="order" name="order" value="'.$li->order.'">
				<input type="hidden" id="field" name="field" value="'.$li->field.'">
				<input type="hidden" id="page_size_change" name="page_size_change" value="">
				<input type="hidden" id="numPerPage" name="numPerPage" value="'.$li->page_size.'">
				'."\n";
		
		return $x;
	}
	
	function Get_Item_Inventory_Change_Log_Printing_UI($ItemID, $StartDate, $EndDate, $Keyword, $Order, $LogType='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		
		### Get Log Info
		$objItem = new POS_Item($ItemID);
		$objCategory = new POS_Category($objItem->CategoryID);
		
		$libPOS = new libpos();
		$sql = $libPOS->Get_Management_Inventory_Log_Sql($ItemID, $StartDate, $EndDate, $Keyword, $Order, $LogType);
		$LogArr = $libPOS->returnArray($sql);
		$numOfLog = count($LogArr);
		
		$x = '';
		
		# Hide Print Button when printing
		$x .= '<style type="text/css" media="print">'."\n";
			$x .= '.print_hide {display:none;}'."\n";
		$x .= '</style>'."\n";
		
		### Print Button
		$x .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td align="right">'."\n";
					$x .= $this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "btn_print")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$x .= '<table width="90%" align="center" border="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td colspan="3" align="center" class="eSportprinttitle">'."\n";
					$x .= '<strong>'.$Lang['ePOS']['InventoryUpdateLog'].'</strong>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td width="5%" nowrap>'.$Lang['ePOS']['Category'].'</td>'."\n";
				$x .= '<td width="1">:</td>'."\n";
				$x .= '<td>'.$objCategory->CategoryName.'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td nowrap>'.$Lang['ePOS']['Item'].'</td>'."\n";
				$x .= '<td width="1">:</td>'."\n";
				$x .= '<td>'.$objItem->ItemName.'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td nowrap>'.$Lang['ePOS']['Barcode'].'</td>'."\n";
				$x .= '<td>:</td>'."\n";
				$x .= '<td>'.$objItem->Barcode.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
		$x .= "	<tr>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['UpdateTime']."</td>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['UpdatedBy']."</td>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['IncreaseBy']."</td>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['DecreaseBy']."</td>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['InventoryAfter']."</td>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['CostPrice']."</td>
					<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['Remark']."</td>
				</tr>\n";

			for ($i=0; $i<$numOfLog; $i++)
			{
				$thisUpdateTime			= $LogArr[$i]['DateInput'];
				$thisUpdatedUserName	= $LogArr[$i]['UpdatedUserName'];
				$thisIncreasedBy 		= $LogArr[$i]['IncreaseCount'];
				$thisDecreasedBy 		= $LogArr[$i]['DecreaseCount'];
				$thisInventoryAfter 	= $LogArr[$i]['CountAfter'];
				$thisCostPrice 			= $LogArr[$i]['CostPrice'];
				$thisRemarks 			= $LogArr[$i]['Remark'];
				$thisRemarks = ($thisRemarks=='')? '&nbsp;' : $thisRemarks;
				
				$x .= "<tr>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisUpdateTime</td>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisUpdatedUserName</td>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisIncreasedBy</td>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisDecreasedBy</td>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisInventoryAfter</td>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisCostPrice</td>\n";
					$x .= "<td class=\"eSporttdborder eSportprinttext\">$thisRemarks</td>\n";
				$x .= "</tr>\n";
			}
		
		$x .= "</table>\n";
		
		
		### Print Button
		$x .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">';
			$x .= '<tr>';
				$x .= '<td align="right">';
					$x .= $this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "btn_print");
				$x .= '</td>';
			$x .= '</tr>';
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Category_Selection($ID_Name, $SelectedCategoryID='', $OnChange='', $DisplayStatus=1, $ActiveOnly=0, $ShowOptionAll=0, $noFirst=1)
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		
		$libCategory = new POS_Category();
		$CategoryArr = $libCategory->Get_All_Category('', $ActiveOnly);
		$numOfCategory = count($CategoryArr);
		
		$selectArr = array();
		if ($numOfCategory > 0)
		{
			for ($i=0; $i<$numOfCategory; $i++)
			{
				$thisCategoryID = $CategoryArr[$i]['CategoryID'];
				$thisCategoryName = $CategoryArr[$i]['CategoryName'];
				
				if ($DisplayStatus == 1)
				{
					$thisRecordStatus = $CategoryArr[$i]['RecordStatus'];
					if ($thisRecordStatus == 0)
						$thisCategoryName .= ' ('.$Lang['General']['Disabled'].')';
				}
				
				$selectArr[$thisCategoryID] = $thisCategoryName;
			}
		}
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
			
		$firstTitle = '';
		if ($ShowOptionAll == 1)
			$firstTitle = Get_Selection_First_Title($Lang['ePOS']['AllCategories']);
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
		$CategorySelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedCategoryID, $ShowOptionAll, $noFirst, $firstTitle);
		
		return $CategorySelection;
	}
	
	function Get_Category_Selection_By_Price($ItemID, $SelectedCategoryID='', $OnChange='', $Price='', $DisplayStatus=1, $ActiveOnly=0, $ShowOptionAll=0, $noFirst=1, $LogID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_category.php");
		
		$libCategory = new POS_Category();
		$CategoryArr = $libCategory->Get_All_Category('', $ActiveOnly, $CategoryID='', $ItemKeyword='', $TemplateID="", $inventoryOnly=true);
		
		### Get ItemID that have been replaced before
		$replacedItemIDAry = array();
		if ($LogID != '') {
		    $libPOS = new libpos();
		    $replacedItemAry = $libPOS->getReplaceItemsByTransaction($LogID);
		    if (count($replacedItemAry)) {
		        $replacedItemIDAry = Get_Array_By_Key($replacedItemAry,'ItemID');
		    }
		}
		$nrReplacedItem = count($replacedItemIDAry);
		
		if($Price){
			include_once($PATH_WRT_ROOT."includes/libpos_item.php");
			$libItem = new POS_Item();
			$ItemArr = $libItem->Get_All_Items('', $ActiveOnly, $isAssoc=false, $inventoryOnly=true);
			
			$numOfCategory = count($CategoryArr);
			$tempCategoryArr = array();
			for($i=0; $i<$numOfCategory; $i++){
				$Items = $ItemArr[$CategoryArr[$i]['CategoryID']];
				if($Items){
					foreach($Items as $Item){
						if($Item['UnitPrice'] == $Price && $Item['ItemID'] != $ItemID){
						    if ($nrReplacedItem) {
						        if (!in_array($Item['ItemID'], $replacedItemIDAry)) {
						            $tempCategoryArr[] = $CategoryArr[$i];
						        }
						    }
						    else {
							    $tempCategoryArr[] = $CategoryArr[$i];
						    }
							break;
						}
					}
				}
			}
			$CategoryArr = $tempCategoryArr;
		}
		$numOfCategory = count($CategoryArr);
		$selectArr = array();
		if ($numOfCategory > 0)
		{
			for ($i=0; $i<$numOfCategory; $i++)
			{
				$thisCategoryID = $CategoryArr[$i]['CategoryID'];
				$thisCategoryName = $CategoryArr[$i]['CategoryName'];
				
				if ($DisplayStatus == 1)
				{
					$thisRecordStatus = $CategoryArr[$i]['RecordStatus'];
					if ($thisRecordStatus == 0)
						$thisCategoryName .= ' ('.$Lang['General']['Disabled'].')';
				}
				
				$selectArr[$thisCategoryID] = $thisCategoryName;
			}
		}
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
			
		$firstTitle = '';
		if ($ShowOptionAll == 1)
			$firstTitle = Get_Selection_First_Title($Lang['ePOS']['AllCategories']);
			
		$selectionTags = ' id="CategoryIDFilter['.$ItemID.']" name="CategoryIDFilter['.$ItemID.']" '.$onchange;
		$CategorySelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedCategoryID, $ShowOptionAll, $noFirst, $firstTitle);
		
		return $CategorySelection;
	}
	
	# HealthIngredient
	function Get_Settings_HealthIngredient_UI($Keyword)
	{
		$x = '';
		$x .= $this->Include_JS_CSS();
		$x .= '<form name="form1" action="index.php" method="post">'."\n";
			$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$thisHref = '#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer';
						$thisOnclick = "js_Show_HealthIngredient_Add_Edit_Layer(''); return false;";
						$thisTag = 'title="Add Health Ingredient"';
						$thisClass = 'thickbox';
						$x .= $this->GET_LNK_ADD($thisHref, '', $thisOnclick, $thisTag, $thisClass, 0);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<br style="clear:both" />';
						$x .= '<div class="table_board" id="MainTableDiv">';
							$x .= $this->Get_Ingredient_List_Table();
						$x .= '</div">';
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>';
		
		
		//$x .= '<div id="FakeLayer"></div>';
		
		return $x;
	}
	
	function Get_HealthIngredient_Add_Edit_Layer($HealthIngredientID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		
		$libinterface = new interface_html();
		
		# Add / Edit
		$isEdit = ($HealthIngredientID=='')? false : true;
		
		# Title
		$thisTitle = ($isEdit)? $Lang['ePOS']['Edit']['HealthIngredient'] : $Lang['ePOS']['Add']['HealthIngredient'];
		
		# Button Submit
		$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
							$onclick="Update_HealthIngredient('".$HealthIngredientID."')", $id="Btn_Edit");
							
		# Cancel
		$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
							$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
							
		# Show Category info if it is edit mode
		if ($isEdit)
		{
			$libpos = new libpos();
			$thisHealthIngredientList = $libpos->Get_HealthIngredient_List($HealthIngredientID);
			$thisCode = intranet_htmlspecialchars($thisHealthIngredientList[0]['HealthIngredientCode']);
			$thisName = intranet_htmlspecialchars($thisHealthIngredientList[0]['HealthIngredientName']);
			$thisUnitName = intranet_htmlspecialchars($thisHealthIngredientList[0]['UnitName']);
			$thisStandardIntake = $thisHealthIngredientList[0]['StandardInTakePerDay'];
			$thisStatus = $thisHealthIngredientList[0]['RecordStatus'];
			
		}
		
		$x = '';
		$x .= '<div id="debugArea"></div>';
		$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
			$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
			$x .= '<div class="edit_pop_board_write">'."\n";
				$x .= '<form id="LocationForm" name="LocationForm">'."\n";
					$x .= '<table class="form_table">'."\n";
						$x .= '<col class="field_title" />'."\n";
						$x .= '<col class="field_c" />'."\n";
						
						# Title input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Name'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="IngredientName" name="IngredientName" type="text" class="textbox" value="'.$thisName.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("NameWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Code input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Code'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="IngredientCode" name="IngredientCode" type="text" class="textbox" maxlength="'.$this->Code_Maxlength.'" value="'.$thisCode.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Unit Name input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['UnitName'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="UnitName" name="UnitName" type="text" class="textbox" maxlength="'.$this->Code_Maxlength.'" value="'.$thisUnitName.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("UnitNameWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# StandardInTakePerDay input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['StandardIntakePerDay'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="StandardIntakePerDay" name="StandardIntakePerDay" type="text" class="textbox" maxlength="8" value="'.$thisStandardIntake.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("StandardIntakeWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					$x .= '<input id="HealthIngredientID" name="HealthIngredientID" type="hidden" value="'.$HealthIngredientID.'" />'."\n";
				$x .= '</form>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				$x .= '<span></span>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $btn_Edit."\n";
//				$x .= $btn_Reset."\n";
				$x .= $btn_Cancel."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>';
		$x .= '</div>';
		
		
		return $x;
	}
	
	function Get_Ingredient_List_Table() {
		global $Lang;

		$libPOS = new libpos();

		$IngredientList = $libPOS->Get_HealthIngredient_List();
		
		$x = '<table class="common_table_list DragAndDrop" id="IngredientListTable" >
						<thead>
							<tr>
								<th>'.$Lang['ePOS']['Name'].'</th>
							  	<th>'.$Lang['ePOS']['Code'].'</th>
								<th>'.$Lang['ePOS']['UnitName'].'</th>
								<th>'.$Lang['ePOS']['StandardIntakePerDay'].'</th>
								<th>&nbsp;</th>
							 </tr>
						</thead>
						<tbody>';
		for ($i=0; $i< sizeof($IngredientList); $i++) 
		{
			$HealthIngredientID = $IngredientList[$i]['HealthIngredientID'];
			
			# Disable Btn
			$thisIsActive = $IngredientList[$i]['RecordStatus'];
			$thisActiveIconTitle = ($thisIsActive==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
			$thisID = 'ActiveStatus_'.$HealthIngredientID;
			$thisJS = 'javascript:js_Disable_Ingredient('.$HealthIngredientID.')';
			$thisIsDeletable = $libPOS->IsHealthIngredientDeletable($HealthIngredientID);
			$DisableBtn = $this->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);

			# Edit Link
			$EditLink = '<a href="#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer" class="edit_dim thickbox" title="'.$Lang['Btn']['Edit'].'" onclick="js_Show_HealthIngredient_Add_Edit_Layer(\''.$IngredientList[$i]['HealthIngredientID'].'\'); return false;"></a>';
			
			# Move Link
			$MoveLink = $this->GET_LNK_MOVE("#", $Lang['ePOS']['Move']['HealthIngredient']);
			
			# Delete Link
			if($thisIsDeletable==1)
				$DeleteLink = $this->GET_LNK_DELETE('#', $Lang['Btn']['Delete'], "js_Delete_HealthIngredient('$HealthIngredientID');", '', 1);
			else
				$DeleteLink = "";
				
			$x .= '<tr hid="'.$HealthIngredientID.'">
						<td>
							'.intranet_htmlspecialchars($IngredientList[$i]['HealthIngredientName']).'
						</td>
						<td>
							'.intranet_htmlspecialchars($IngredientList[$i]['HealthIngredientCode']).'
						</td>
						<td>
							'.intranet_htmlspecialchars($IngredientList[$i]['UnitName']).'
						</td>
						<td>
							'.$IngredientList[$i]['StandardInTakePerDay'].'
						</td>
						<td class="Dragable">
							<span class="table_row_tool row_content_tool">
								'.$DisableBtn.'
								'.$EditLink.'
								'.$DeleteLink.'
								'.$MoveLink.'
							</span>
						</td>
					</tr>';
		}

		$x .= '</tbody>
			</table>';


		return $x;
	}
		
	# Item HealthIngredient Mapping
	function Get_Settings_Item_Health_Ingredient_Mapping_UI($ItemID)
	{
		global $Lang;
		
		# Add Icon
		$thisHref = '#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer';
		$thisOnclick = "js_Show_Health_Ingredient_Add_Edit_Layer('$ItemID',''); return false;";
		$thisTag = 'title="'.$Lang['ePOS']['Add']['HealthIngredient'].'"';
		$thisClass = 'thickbox';
		$AddIcon .= $this->GET_LNK_ADD($thisHref, '', $thisOnclick, $thisTag, $thisClass, 0);
		
		# SearchBox
		//$SearchBox = $this->Get_Search_Box_Div('Keyword', $Keyword);
		
		# Import Icon
		//$ImportIcon = $this->GET_LNK_IMPORT('#', "", "", "", "", $useThickBox=0);
		
		# Export Icon
		//$ExportIcon = $this->GET_LNK_EXPORT('#', "", "", "", "", $useThickBox=0);
		
		# Page Navigation
		$Item = new POS_Item($ItemID,true);
		$NavigationArr = array();
		$NavigationArr[] = array($Item->CategoryName, "category.php");
		$NavigationArr[] = array($Item->ItemName, "item.php?CategoryID=".$Item->CategoryID);
		$NavigationArr[] = array($Lang['ePOS']['Manage']['HealthIngredient'], "");
		$NavigationHTML = $this->GET_NAVIGATION($NavigationArr);
		
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<form id="ItemHealthIngredientForm" name="ItemHealthIngredientForm" method="post" onsubmit="return false;">'."\n";
			$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$NavigationHTML.'</td>'."\n";
					$x .= '<td align="right">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$AddIcon.'</td>'."\n";
					$x .= '<td align="right">'.$SearchBox.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= $ImportIcon."\n";
						$x .= toolBarSpacer()."\n";
						$x .= $ExportIcon."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				### Item Health Ingredient List Table
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'."\n";
						$x .= '<div id="MainTableDiv">'."\n";
							$x .= $this->Get_Settings_Item_Health_Ingredient_Table($ItemID);
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table><br>'."\n";
			$x .= '<input type="hidden" id="ItemID" name="ItemID" value="'.$ItemID.'" />'."\n";
			$x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button","window.location = 'item.php?CategoryID=".$Item->CategoryID."';","Btn_Back");
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Item_Health_Ingredient_Table($ItemID)
	{
		global $Lang;

		$libPOS = new libpos();

		$IngredientList = $libPOS->Get_Item_Health_Ingredient_List($ItemID);
		
		$x = '<table class="common_table_list" id="IngredientListTable" >
						<thead>
							<tr>
								<th>'.$Lang['ePOS']['Name'].'</th>
							  	<th>'.$Lang['ePOS']['Unit'].'</th>
								<th>'.$Lang['ePOS']['StandardIntakePerDay'].'</th>
								<th>'.$Lang['ePOS']['ItemIntake'].'</th>
								<th>&nbsp;</th>
							 </tr>
						</thead>
						<tbody>';
		for ($i=0; $i< sizeof($IngredientList); $i++) 
		{
			$HealthIngredientID = $IngredientList[$i]['HealthIngredientID'];
			
			# Edit Link
			$EditLink = '<a href="#TB_inline?height='.$this->thickBoxHeight.'&width='.$this->thickBoxWidth.'&inlineId=FakeLayer" class="edit_dim thickbox" title="'.$Lang['Btn']['Edit'].'" onclick="js_Show_Health_Ingredient_Add_Edit_Layer(\''.$ItemID.'\',\''.$IngredientList[$i]['HealthIngredientID'].'\'); return false;"></a>';
			
			# Delete Link
			$DeleteLink = $this->GET_LNK_DELETE('#', $Lang['Btn']['Delete'], "Update_HealthIngredient_List('Delete', '$ItemID' ,'$HealthIngredientID');", '', 1);
			
			$x .= '<tr hid="'.$HealthIngredientID.'">
						<td>
							'.intranet_htmlspecialchars($IngredientList[$i]['HealthIngredientName']).'
						</td>
						<td>
							'.intranet_htmlspecialchars($IngredientList[$i]['UnitName']).'
						</td>
						<td>
							'.$IngredientList[$i]['StandardIntakePerDay'].'
						</td>
						<td>
							'.$IngredientList[$i]['IngredientIntake'].'
						</td>
						<td>
							<span class="table_row_tool row_content_tool">
								'.$EditLink.'
								'.$DeleteLink.'
							</span>
						</td>
					</tr>';
		}

		$x .= '</tbody>
			</table>';


		return $x;
	}
	
	function Get_Item_Health_Ingredient_Add_Edit_Layer($ItemID='', $HealthIngredientID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		
		$libinterface = new interface_html();
		
		# Add / Edit
		$isEdit = ($HealthIngredientID=='')? false : true;
		
		# Action 
 		$Action = $isEdit?"Edit":"Add";
		
		# Title
		$thisTitle = ($isEdit)? $Lang['ePOS']['Edit']['HealthIngredient'] : $Lang['ePOS']['Add']['HealthIngredient'];
		
		# Button Submit
		$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
							$onclick="Update_HealthIngredient_List('$Action','$ItemID','".$HealthIngredientID."')", $id="Btn_Edit");
							
		# Cancel
		$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
							$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
							
		# Show Category info if it is edit mode
		if ($isEdit)
		{
			$libpos = new libpos();
			$thisHealthIngredientList = $libpos->Get_Item_Health_Ingredient_List($ItemID,$HealthIngredientID);
			$thisName = intranet_htmlspecialchars($thisHealthIngredientList[0]['HealthIngredientName']);
			$thisItemIntake = $thisHealthIngredientList[0]['IngredientInTake'];
			
			$IngredientInfo = $libpos->Get_Item_Health_Ingredient_List($ItemID,$HealthIngredientID);
			$IngredientIntake = $IngredientInfo[0]['IngredientIntake'];
			$IngredientName = $IngredientInfo[0]['HealthIngredientName'];
		}
		else
		{
			$IngredientName = $this->Get_Health_Ingredient_Select_Box("HealthIngredientID",$ItemID);	
		}
		
		$x = '';
		$x .= '<div id="debugArea"></div>';
		$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
			$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
			$x .= '<div class="edit_pop_board_write">'."\n";
				$x .= '<form id="LocationForm" name="LocationForm">'."\n";
					$x .= '<table class="form_table">'."\n";
						$x .= '<col class="field_title" />'."\n";
						$x .= '<col class="field_c" />'."\n";
						
						# Title input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['Name'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $IngredientName."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Item input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ePOS']['ItemIntake'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="ItemIntake" name="ItemIntake" type="text" class="textbox" maxlength="8" value="'.$IngredientIntake.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("ItemIntakeWarningDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					$x .= '<input id="ItemID" name="ItemID" type="hidden" value="'.$ItemID.'" />'."\n";
				$x .= '</form>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				$x .= '<span></span>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $btn_Edit."\n";
//				$x .= $btn_Reset."\n";
				$x .= $btn_Cancel."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>';
		$x .= '</div>';
		
		
		return $x;
	}
	
	function Get_Health_Ingredient_Select_Box($TagName,$ExceptItemID='',$otherTagInfo='')
	{
		$libPOS = new libpos();
		
		//get ingredient list of item
		$ItemIngredientList = $libPOS->Get_Item_Health_Ingredient_List($ExceptItemID);
		
		//get all ingredient except existing ingredient
		$IngredientList = $libPOS->Get_HealthIngredient_List();
		$ItemIngredientIDAry = Get_Array_By_Key($ItemIngredientList,"HealthIngredientID");

		foreach((array)$IngredientList as $Ingredient)
		{
			if(in_array($Ingredient["HealthIngredientID"],$ItemIngredientIDAry)) 
				continue;
			
			$tmpAry[] = array($Ingredient["HealthIngredientID"],$Ingredient["HealthIngredientName"]);
		}		
		 
		$TagInfo .= " name='$TagName' id='$TagName' "; 
		$TagInfo .= $otherTagInfo;
		 
		$SelectBox = getSelectByArray($tmpAry, $TagInfo, $selected="", $all=0, $noFirst=1);
		return $SelectBox;
		
	}
	
		// pos report
	function Get_POS_Transaction_Report_Table($field=8, $order=0, $pageNo=1, $page_size=20,$FromDate='',$ToDate='',$ClassName='',$RecordStatus='',$keyword='',$void='')
	{
		global $PATH_WRT_ROOT,$intranet_session_language, $Lang, $sys_custom;
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		if($FromDate==""){
			$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		}
		if($ToDate==""){
			$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		}
		//$this->Set_Report_Date_Range_Cookies($FromDate, $ToDate);
		
		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("InvoiceNumber","ClassName","ClassNumber","Name","Detail","GrandTotalForSort","REFCode","DateInput");
		if($sys_custom['ePOS']['enablePurchaseAndPickup']){
			$li->field_array = array("InvoiceNumber","ClassName","ClassNumber","Name","Detail","GrandTotalForSort","REFCode","DateInput","Status");
		}
		$li->sql = $POS->Get_POS_Transaction_Report_Table_Sql($FromDate,$ToDate,$ClassName,$RecordStatus,$keyword,true,'','',$void);
		$li->no_col = sizeof($li->field_array)+($sys_custom['ePOS']['enablePurchaseAndPickup']?2:1);
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0,0);
		$li->wrap_array = array(0,0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['InvoiceNumber'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassName'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassNumber'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['UserName'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['General']['Details'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['RefCode'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['TranscationTime'])."</td>\n";
		if($sys_custom['ePOS']['enablePurchaseAndPickup']){
			$li->column_list .= "<td width=8% class=tableTitle>".$li->column($pos++, $Lang['General']['Status'])."</td>\n";
			$li->column_list .= "<td width=2% class=tableTitle></td>\n";
		}
		
		$lclass = new libclass();
		$select_class = $lclass->getSelectClass("name=ClassName onChange=\"document.form1.submit();\"",$_REQUEST['ClassName'],"",'-- '.$Lang['General']['All'].' --');
		
		# Item RecordStatus Filter
		$recordStatusAry = array(
								array('C',$Lang['ePOS']['Completed']),
								array('NC',$Lang['ePOS']['NotCompleted'])
							);
		$RecordStatusFilter = getSelectByArray($recordStatusAry, ' id="RecordStatus" name="RecordStatus" onchange="document.form1.submit();" ', $RecordStatus, 1, 0, '', 1);
		
		
		$toolbar = $this->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
		$toolbar2 = $this->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate&field=$field&order=$order&void=$void')","","","","",0);
		$toolbar3 = $this->GET_LNK_PRINT("detail.php?FromDate=".$FromDate."&ToDate=".$ToDate."&void=".$void,$Lang["ePOS"]["PrintTransactionDetail"],""," target='_blank' ","",0);

		$searchbar .= $this->Get_Search_Box_Div('keyword', $keyword);
		$actionPage = $void=='1'?"voidindex.php":"index.php";
		
		$x ='
			<br>
			<form name="form1" id="form1" method="get" action="'.$actionPage.'">
				<input type="hidden" name="ItemName" id="ItemName" value="">
				<!-- date range -->
				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['ePOS']['SelectDateRange'] .'</td>
						<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
							'.$this->GET_DATE_PICKER("FromDate",$FromDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
							'.$Lang['ePOS']['To'].' 
							'.$this->GET_DATE_PICKER("ToDate",$ToDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dotline"><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td colspan="2" class="tabletext" align="center">
							'. $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2") .'
						</td>
					</tr>
				</table>

				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td align="left">'.$toolbar.' '.$toolbar2.' '.$toolbar3.'</td>
						<td align="right">'.$searchbar .'</td>
					</tr>
					<tr>
						<td align="left" colspan="2"><span id="ClassLayer" '.$ClassDisplay.'>'.$select_class.'&nbsp;'.$RecordStatusFilter.'</span></td>
					</tr>
					<tr>
						<td colspan="2">'. $li->display() .'</td>
					</tr>
				</table>
				
				
				<input type=hidden name=pageNo value="'.$li->pageNo .'">
				<input type=hidden name=order value="'.$li->order .'">
				<input type=hidden name=field value="'.$li->field .'">
				<input type=hidden name=page_size_change value="">
				<input type=hidden name=numPerPage value="'.$li->page_size.'">
                <input type=hidden name=void value="'.$void.'">
			</form><br><br>';

		return $x;
	}
	
	function Get_POS_Transaction_Report_PrintLayout($FromDate='',$ToDate='',$ClassName='',$RecordStatus='',$keyword='',$field='',$order='',$void='')
	{
		global $PATH_WRT_ROOT,$intranet_session_language, $Lang;

		$table = $this->Get_POS_Transaction_Report_PrintTable($FromDate,$ToDate,$ClassName,$RecordStatus,$keyword,$field,$order,$void);
		
		$x .= '<style type="text/css" media="print">'."\n";
			$x .= 'thead {display: table-header-group;}'."\n";
		$x .= '</style>'."\n";
		
		$x .= "
			<table border=0 width=95% cellpadding=2 align=center>
				<tr>
					<td class='".$css_title."'><b>".$Lang['Payment']['POSTransactionReport']." ("."$FromDate ".$Lang['ePOS']['To']." $ToDate".")</b></td>
				</tr>
			</table>
			".$table."
			<br>
			<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
				<tr>
					<td align=right class='".$css_text."'>
						".$Lang['ePOS']['ReportGenerationTime']." : ".date('Y-m-d H:i:s')."
					</td>
				</tr>
			</table>
			<BR><BR>";
			
			return $x;
		
	}
	
	function Get_POS_Transaction_Report_PrintTable($FromDate='',$ToDate='',$ClassName='',$RecordStatus='',$keyword='',$field='',$order='',$void='')
	{
		global $PATH_WRT_ROOT,$intranet_session_language, $Lang, $sys_custom;
		
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
			$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
			$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
		
			$Result = $POS->Get_POS_Transaction_Report_Data($FromDate,$ToDate,$ClassName,$RecordStatus,$keyword,$field,$order,$void);
		
		$x .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
			$x .= '<tr>';
				$x .= '<td width="10%"><b>'.$Lang['ePOS']['ClassName'].'</b></td>';
				$x .= '<td><b>:'.(($ClassName != "")? $ClassName:$Lang['General']['All']).'</b></td>';
			$x .= '</tr>';
			if($keyword != "") {
				$x .= '<tr>';
				$x .= '<td><b>' . $Lang['Payment']['Keyword'] . '</b></td>';
				$x .= '<td><b>:' . $keyword . '</b></td>';
				$x .= '</tr>';
			}
		$x .= '</table>';
		$x .= '<hr>';
		$x .= "<table width=95% border=0  cellpadding=1 cellspacing=0 align='center'>";
			$x .= "<thead>";
			$x .= "<tr>";
				$x .= "<td width=10%><b>".$Lang['ePayment']['InvoiceNumber']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['ClassName']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['ClassNumber']."</b></td>\n";
				$x .= "<td width=15%><b>".$Lang['ePOS']['UserName']."</b></td>\n";
				$x .= "<td width=15%><b>".$Lang['General']['Details']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePayment']['GrandTotal']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['RefCode']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['TransactionTime']."</b></td>\n";
				if($sys_custom['ePOS']['enablePurchaseAndPickup']){
					$x .= "<td width=10%><b>".$Lang['General']['Status']."</b></td>\n";
				}
			$x .= "</tr>";
		$x .= "</thead>";
		
		$x .= "<tbody>";
		for ($i=0; $i< sizeof($Result); $i++) {
			$x .= '<tr>';
				$x .= '<td>'.$Result[$i]['InvoiceNumber'].'</td>';
				$x .= '<td>'.$Result[$i]['ClassName'].'</td>';
				$x .= '<td>'.$Result[$i]['ClassNumber'].'</td>';
				$x .= '<td>'.$Result[$i]['Name'].'</td>';
				$x .= '<td>'.$Result[$i]['Detail'].'</td>';
				$x .= '<td>'.$this->CurrencySign.$Result[$i]['GrandTotal'].'</td>';
				$x .= '<td>'.$Result[$i]['REFCode'].'</td>';
				$x .= '<td>'.$Result[$i]['DateInput'].'</td>';
				if($sys_custom['ePOS']['enablePurchaseAndPickup']){
					$x .= '<td>'.$Result[$i]['Status'].'</td>';
				}
			$x .= '</tr>';
            $x .= '<tr height=15px></tr>';
		}
		$x .= "</tbody>";
		$x.="</table>";
		
		return $x;
	}
	
	function Get_POS_Transaction_Log_Detail($LogID='',$FromDate='', $ToDate='',$void='')
	{
		global $intranet_session_language,$Lang;
		
		$POS = new libpos();
	
		if(!empty($LogID))
			$LogIDAry = array($LogID);
		else
		    $LogIDAry = $POS->Get_POS_Transaction_LogID_By_Date($FromDate,$ToDate,$void);
		
		$table_content .= '<style type="text/css" media="print">'."\n";
			$table_content .= 'thead {display: table-header-group;}'."\n";
		$table_content .= '</style>'."\n";
		
		for($j=0; $j<sizeof($LogIDAry);$j++)
		{
			$Total = 0;
			$thisLogID = $LogIDAry[$j];
			$Result = $POS->Get_POS_Transaction_Log_Data($thisLogID,$void);
			if (count($Result) == 0) {
			    $allVoid = true;
			    $Result = $POS->Get_POS_Transaction_Log_Data($thisLogID,$void,$allVoid);       // get void data
			}
			else {
			    $allVoid = false;
			}
			
			$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
				$table_content .= "<tr>";
					$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['UserName']." : </b>".$Result[0]['Name']." (".$Result[0]['ClassName'].$Result[0]['ClassNumber'].")</td>";
				$table_content .= "</tr>";
				$table_content .= "<tr>";
					$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['TransactionTime']." : </b>".$Result[0]['DateInput']."</td>";
				$table_content .= "</tr>";
				$table_content .= "<tr>";
					$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['RefCode']." : </b>".$Result[0]['RefCode']."</td>";
				$table_content .= "</tr>";
				$table_content .= "<tr>";
					$table_content .= "<td class='$css_text'><B>".$Lang['ePayment']['InvoiceNumber']." : </b>".$Result[0]['InvoiceNumber']."</td>";
				$table_content .= "</tr>";
			$table_content .="</table><br>";
			$table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
				$table_content .= "<thead>";
				$table_content .= "<tr>";
					$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['Category']."</b></td>";
					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['ItemName']."</b></td>";
					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['UnitPrice']."</b></td>";
					$table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td>";
				$table_content .= "</tr>";
				$table_content .= "</thead>";
				$table_content .= "<tbody>";
				
			if (!$allVoid) {	
    			for ($i=0; $i< sizeof($Result);$i++) {
    				$css =$i%2==0?$css_table_content:$css_table_content."2";
    				
    				$table_content .= "<tr>";
    					$table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
    					$table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
    					$table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
    					$table_content .= "<td class='$css'>".$this->CurrencySign.$Result[$i]['ItemSubTotal']."</td>";
    					$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
    				$table_content .= "</tr>";
    				$Total += $Result[$i]['GrandTotal'];
    			}
			}
			$table_content .= "<tr><td colspan='5' align=right>".$this->CurrencySign.$Total."</td></tr>";
			$table_content .= "</tbody></table><br><br><br>";
			
		}
		
		return $table_content;
	}
	
	function Get_POS_Transaction_Report_Export_Content($FromDate='', $ToDate='',$ClassName='', $RecordStatus='', $Keyword='', $field='', $order='',$void='')
	{
		global $intranet_session_language,$Lang,$PATH_WRT_ROOT, $sys_custom;
		
		$POS = new libpos();
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		
		$Result = $POS->Get_POS_Transaction_Report_Data($FromDate, $ToDate,$ClassName, $RecordStatus, $Keyword, $field, $order,$void);

		if($void == '1') {
			$UtfContent = $Lang['ePOS']['POSCancelTransactionReport'];
		} else {
			$UtfContent = $Lang['ePayment']['POSTransactionReport'];
		}
		$UtfContent .= " (".$FromDate." ".$Lang["ePOS"]["To"]." ".$ToDate.")\r\n\r\n";
		$UtfContent .= $Lang['ePOS']['ClassName']."\t".(($ClassName != "")? $ClassName:$Lang['General']['All'])."\r\n";
		if($Keyword != "") {
			$UtfContent .= $Lang['Payment']['Keyword'] . "\t" . $Keyword . "\r\n";
		}
		$UtfContent .= "\r\n";
		
		$ExportColumn = array($Lang['ePayment']['InvoiceNumber'],$Lang['ePOS']['ClassName'],$Lang['ePOS']['ClassNumber'],$Lang['ePOS']['UserName'],$Lang['General']['Details'],$Lang['ePayment']['GrandTotal'],$Lang['ePOS']['RefCode'],$Lang['ePOS']['TransactionTime']);
		if($sys_custom['ePOS']['enablePurchaseAndPickup']){
			$ExportColumn = array($Lang['ePayment']['InvoiceNumber'],$Lang['ePOS']['ClassName'],$Lang['ePOS']['ClassNumber'],$Lang['ePOS']['UserName'],$Lang['General']['Details'],$Lang['ePayment']['GrandTotal'],$Lang['ePOS']['RefCode'],$Lang['ePOS']['TransactionTime'],$Lang['General']['Status']);
		}
		
		for ($i=0; $i< sizeof($Result); $i++) {
			unset($Detail);
			$Detail[] = $Result[$i]['InvoiceNumber'];
			$Detail[] = $Result[$i]['ClassName'];
			$Detail[] = $Result[$i]['ClassNumber'];
			$Detail[] = $Result[$i]['Name'];
			$Detail[] = str_replace(array('<br>','<br />'),', ',$Result[$i]['Detail']);
			$Detail[] = $POS->getExportAmountFormat($Result[$i]['GrandTotal']);
			$Detail[] = $Result[$i]['REFCode'];
			$Detail[] = $Result[$i]['DateInput'];
			if($sys_custom['ePOS']['enablePurchaseAndPickup']){
				$Detail[] = $Result[$i]['Status'];
			}
			
			$Rows[] = $Detail;
		}
		
		$ExportContent = $UtfContent.$lexport->GET_EXPORT_TXT($Rows,$ExportColumn);
		
		return $ExportContent;
	}
	
	function Get_POS_Item_Report_Layout($field=1, $order=0, $pageNo=1, $page_size=20,$FromDate='',$ToDate='',$keyword='')
	{
		global $PATH_WRT_ROOT,$intranet_session_language, $Lang;
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		
		$POS = new libpos();

		# date range
		$today_ts = strtotime(date('Y-m-d'));
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		if($FromDate==""){
		  $FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		}
		if($ToDate==""){
		  $ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		}
		//$this->Set_Report_Date_Range_Cookies($FromDate, $ToDate);
		
		$sql = $POS->Get_POS_Item_Report_Table_Sql($FromDate, $ToDate, $keyword, true);
		
		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("CategoryName","ItemName","b.UnitPrice","TotalSold","TotalPriceForSort");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0);
		$li->wrap_array = array(0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
		$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['Category'])."</td>\n";
		$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['ItemName'])."</td>\n";
		$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['UnitPrice'])."</td>\n";
		$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['Quantity'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";
		
		/*$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
		$toolbar2 = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";
		//$toolbar = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";
		//$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'pps_index.php')>".exportIcon()."$button_export_pps</a>";
		#$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
		#$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
		
		$searchbar .= $select_user;
		$searchbar .= '<span id="ClassLayer" '.$ClassDisplay.'>'.$select_class.'</span>';
		$searchbar .= '<span id="StudentLayer" '.$StudentDisplay.'>'.$select_student.'</span>';
		$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
		$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";*/
		
		$toolbar = $this->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
		$toolbar2 = $this->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')","","","","",0);
		
		$searchbar .= $this->Get_Search_Box_Div('keyword', $keyword);
		
		$x .='
			<br>
			<form name="form1" id="form1" method="get" action="index.php">
				<input type="hidden" name="ItemName" id="ItemName" value="">
				<input type="hidden" name="ItemID" id="ItemID" value="">
				<!-- date range -->
				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['ePOS']['SelectDateRange'] .'</td>
						<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
							'.$this->GET_DATE_PICKER("FromDate",$FromDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
							'.$Lang['ePOS']['To'].'  
							'.$this->GET_DATE_PICKER("ToDate",$ToDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dotline"><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td colspan="2" class="tabletext" align="center">
							'. $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2") .'
						</td>
					</tr>
				</table>
				
				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td align="left">'.$toolbar.' '.$toolbar2.'</td>
						<td align="right">'.$searchbar .'</td>
					</tr>
					<tr>
						<td colspan="2">'. $li->display() .'</td>
					</tr>
				</table>
				
				
				<input type=hidden name=pageNo value="'. $li->pageNo .'">
				<input type=hidden name=order value="'. $li->order .'">
				<input type=hidden name=field value="'. $li->field .'">
				<input type=hidden name=page_size_change value="">
				<input type=hidden name=numPerPage value="'.$li->page_size.'">
			</form>';
			
			return $x;
	}
	
	function Get_POS_Item_Report_PrintLayout($FromDate='',$ToDate='',$keyword='',$field='', $order='')
	{
		global $Lang;
		
		$table = $this->Get_POS_Item_Report_PrintTable($FromDate,$ToDate,$keyword,$field,$order);
		
		$x .= '<style type="text/css" media="print">'."\n";
			$x .= 'thead {display: table-header-group;}'."\n";
		$x .= '</style>'."\n";
		$x.="<table border=0 width=95% cellpadding=2 align=center>
				<tr><td class='".$css_title."'><b>".$Lang['ePayment']['POSItemReport']." ($FromDate ".$Lang["ePOS"]["To"]." $ToDate) </b></td></tr>
			</table>
				".$table."
				<br>
			<table width=96% border=0 cellpadding=0 cellspacing=0 align=center>
				<tr>
					<td align=right class='".$css_text."'>
						".$Lang['ePOS']['ReportGenerationTime']." : ".date('Y-m-d H:i:s')."
					</td>
				</tr>
			</table>
			<BR><BR>";
			
			return $x;
		
	}
	
	function Get_POS_Item_Report_PrintTable($FromDate='',$ToDate='',$keyword='',$field='', $order='')
	{
		global $Lang;
		
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
	        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
	        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
	
		$Result = $POS->Get_POS_Item_Report_Data($FromDate,$ToDate,$keyword,$field,$order);
		
		$x = "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
			$x .= '<tr>';
				$x .= '<td><b>'.$Lang['Payment']['Keyword'].':</b></td>';
				$x .= '<td><b>'.$keyword.'</b></td>';
			$x .= '</tr>';
		$x .= '</table>';
		$x .= '<hr>';
		$x .= "<table width=95% border=0  cellpadding=1 cellspacing=0 align='center'>";
			$x .= "<thead>";
			$x .= "<tr>";
				$x .= "<td width=25%><b>".$Lang['ePOS']['Category']."</b></td>\n";
				$x .= "<td width=25%><b>".$Lang['ePayment']['ItemName']."</b></td>\n";
				$x .= "<td width=20%><b>".$Lang['ePOS']['UnitPrice']."</b></td>\n";
				$x .= "<td width=20%><b>".$Lang['ePayment']['Quantity']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePayment']['GrandTotal']."</b></td>\n";
			$x .= "</tr>";
			$x .= "</thead>";
			$x .= "<tbody>";
		for ($i=0; $i< sizeof($Result); $i++) {
			$x .= '<tr>';
				$x .= '<td>'.$Result[$i]['CategoryName'].'</td>';
				$x .= '<td>'.$Result[$i]['ItemName'].'</td>';
				$x .= '<td>'.$Result[$i]['UnitPrice'].'</td>';
				$x .= '<td>'.$Result[$i]['TotalSold'].'</td>';
				$x .= '<td>'.$this->CurrencySign.$Result[$i]['TotalPrice'].'</td>';
			$x .= '</tr>';
            $x .= '<tr height=15px></tr>';
		}
			$x .= "</tbody>";
		$x.="</table>";
		
		return $x;
	}
	
	function Get_POS_Item_Report_Export_Content($FromDate='', $ToDate='', $Keyword='', $field='', $order='')
	{
		global $Lang,$PATH_WRT_ROOT;
		
		$POS = new libpos();
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();

		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
	        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
	        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

		$Result = $POS->Get_POS_Item_Report_Data($FromDate, $ToDate, $Keyword, $field, $order);		
		
		$UtfContent = $Lang['ePOS']['POSItemReport']." (".$FromDate." ".$Lang['ePOS']['To']." ".$ToDate.")\r\n\r\n";
		$UtfContent .= $Lang['Payment']['Keyword']."\t".$Keyword."\r\n";
		$UtfContent .= "\r\n";
		
		$ExportColumn = array($Lang['ePOS']['Category'],$Lang['ePayment']['ItemName'],$Lang['ePOS']['UnitPrice'],$Lang['ePayment']['Quantity'],$Lang['ePayment']['GrandTotal']);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			unset($Detail);
			$Detail[] = $Result[$i]['CategoryName'];
			$Detail[] = $Result[$i]['ItemName'];
			$Detail[] = str_replace($POS->CurrencySign,'',$Result[$i]['UnitPrice']);
			$Detail[] = $Result[$i]['TotalSold'];
			$Detail[] = $POS->getExportAmountFormat($Result[$i]['TotalPrice']);
			
			$Rows[] = $Detail;
		}
		
		$ExportContent = $UtfContent.$lexport->GET_EXPORT_TXT($Rows,$ExportColumn);
		
		return $ExportContent;
	}
	
	function Get_POS_Item_Report_Detail($FromDate='', $ToDate='', $ItemName='', $ItemID='')
	{
		global $intranet_session_language, $Lang;
		
		$POS = new libpos();
		
		$Result = $POS->Get_POS_Item_Report_Detail_Data($FromDate, $ToDate, $ItemName, $ItemID);
		
		$table_content .= '<style type="text/css" media="print">'."\n";
			$table_content .= 'thead {display: table-header-group;}'."\n";
		$table_content .= '</style>'."\n";
		$table_content .= '<form name="form1" id="form1" method="post" action="detail_export.php" target="_self">'."\n";
		$table_content .= '<input type="hidden" name="FromDate" value="'.$FromDate.'" />'."\n";
		$table_content .= '<input type="hidden" name="ToDate" value="'.$ToDate.'" />'."\n";
		$table_content .= '<input type="hidden" name="ItemID" value="'.$ItemID.'" />'."\n";
		$table_content .= '</form>'."\n";
		$table_content .= '<table width="95%" align="center" class="print_hide" border="0">
								<tr>
									<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "window.print();","print_button").'&nbsp;'.$this->GET_BTN($Lang['Btn']['Export'], "button", "document.getElementById('form1').submit();","export_button").'</td>
								</tr>
							</table>'."\n";
		
		$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
		if($ItemID != '') {
			$itemObj = new POS_Item($ItemID, true);
			$table_content .= "<tr>";
				$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['Category']." : </b>".$itemObj->CategoryName."</td>";
			$table_content .= "</tr>";
		}
			$table_content .= "<tr>";
				$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['ItemName']." : </b>".$ItemName."</td>";
			$table_content .= "</tr>";
			$table_content .= "<tr>";
				$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['TransactionTime']." : </b>".$FromDate." ".$Lang['ePOS']['To']." ".$ToDate."</td>";
			$table_content .= "</tr>";
		$table_content .="</table><br>";
		$table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
		$table_content .= "<thead>";
		$table_content .= "<tr>";
			$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['TransactionTime']."</b></td>";
			$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['InvoiceNumber']."</b></td>";
			$table_content .= "<td class='$css_text'><b>".$Lang['General']['Class']."</b></td>";
			$table_content .= "<td class='$css_text'><b>".$Lang['General']['ClassNumber']."</b></td>";
			$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['UserName']."</b></td>";
			$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
			$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['UnitPrice']."</b></td>";
			$table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td></tr>";
		$table_content .= "</thead>";
		
		$table_content .= "<tbody>";
		for ($i=0; $i< sizeof($Result);$i++) {
			$css =$i%2==0?$css_table_content:$css_table_content."2";
			
			$table_content .= "<tr>";
				$table_content .= "<td class='$css'>".$Result[$i]['ProcessDate']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['InvoiceNumber']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['ClassName']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['ClassNumber']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['UserName']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
				$table_content .= "<td class='$css'>".$this->CurrencySign.$Result[$i]['ItemSubTotal']."</td>";
				$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
			$table_content .= "</tr>";
			$Total += $Result[$i]['GrandTotal'];
		}
		$table_content .= "<tr><td colspan='8' align=right>".$this->CurrencySign.$Total."</td></tr>";
		$table_content .= "</tbody></table>";
		
		$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=\"center\">
								<tr><td>".$Lang['ePOS']['StudentRemoved']."</td></tr>
							</table>";
				
		return $table_content;
	}

	function Get_POS_Student_Report_Layout($field=8, $order=0, $pageNo=1, $page_size=20,$FromDate='',$ToDate='',$keyword='',$ClassName="")
	{
		global $Lang,$PATH_WRT_ROOT;
		
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		if($FromDate==""){
		  $FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		}
		if($ToDate==""){
		  $ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		}
		
		$sql = $POS->Get_POS_Student_Report_Sql($FromDate,$ToDate,$keyword,true,$ClassName);

		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("StudentName","ClassName","ClassNumber","Times","LastTransactionTime","GrandTotalForSort");
// 		$li->field_array = array("StudentName","ClassName","ClassNumber","GrandTotalForSort");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0,0,0);
		$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
		$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['UserName'])."</td>\n";
// 		$li->column_list .= "<td width=55% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['UserName'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassName'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassNumber'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['TransactionTimes'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['LastTransactionDate'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";
		
		$toolbar = $this->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
		$toolbar2 = $this->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate&field=$field&order=$order')","","","","",0);
		
		$searchbar = $this->Get_Search_Box_Div('keyword', $keyword);
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$ClassSelection = $lclass->getSelectClass('name="ClassName" id="ClassName" onchange="this.form.pageNo.value=1; this.form.submit();"',$ClassName,0,'-- '.$Lang['Btn']['All'].' --');
		
		$x .= '
			<br>
			<form name="form1" id="form1" method="get" action="index.php">
			<!-- date range -->
			<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['ePOS']['SelectDateRange'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
						'.$this->GET_DATE_PICKER("FromDate",$FromDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
						'.$Lang['ePOS']['To'].'  
						'.$this->GET_DATE_PICKER("ToDate",$ToDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext" align="center">
						'. $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2") .'
					</td>
				</tr>
			</table>
			
			<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td align="left">'.$toolbar.' '.$toolbar2.'</td>
					<td align="right">'.$searchbar .'</td>
				</tr>
				<tr>
					<td align="left">'.$ClassSelection.'</td>
				</tr>
				<tr>
					<td colspan="2">'.$li->display() .'</td>
				</tr>
			</table>
			
			<input type=hidden name=pageNo value="'.$li->pageNo.'">
			<input type=hidden name=order value="'.$li->order .'">
			<input type=hidden name=field value="'.$li->field .'">
			<input type=hidden name=page_size_change value="">
			<input type=hidden name=numPerPage value="'.$li->page_size.'">
			</form>';
			
			return $x;
	}	
	
	function Get_POS_Student_Report_PrintLayout($FromDate='',$ToDate='',$keyword='',$ClassName="",$field="",$order="")
	{
		global $Lang;
		
		$table = $this->Get_POS_Student_Report_PrintTable($FromDate,$ToDate,$keyword,$ClassName,$field,$order);
		
		$x .= '<style type="text/css" media="print">'."\n";
			$x .= 'thead {display: table-header-group;}'."\n";
		$x .= '</style>'."\n";
		$x.= "
			<table border=0 width=95% cellpadding=2 align=center>
				<tr>
					<td class='".$css_title."'><b>".$Lang['ePOS']['POSStudentReport']." ("."$FromDate ".$Lang["ePOS"]["To"]." $ToDate".")</b></td>
				</tr>
			</table>
			".$table."
			<br>
			<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
				<tr>
					<td align=right class='".$css_text."'>".$Lang['ePOS']['ReportGenerationTime']." : ".date('Y-m-d H:i:s')."</td>
				</tr>
			</table>
			<BR><BR>";
			
			return $x;
	}
	
	function Get_POS_Student_Report_PrintTable($FromDate='',$ToDate='',$keyword='',$ClassName="",$field="",$order="")
	{
		global $intranet_session_language,$Lang ;
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
	        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
	        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
		
		$Result = $POS->Get_POS_Student_Report_Data($FromDate,$ToDate,$keyword,$ClassName,$field,$order);
		
		$x = "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
			$x .= "<tr>";
				$x .= '<td><b>'.$Lang['Payment']['Keyword'].':</b> '.$keyword.'</td>';
			$x .= '</tr>';
		$x .= "<tr>";
				$x .= '<td><b>'.$Lang['ePOS']['ClassName'].':</b> '.$ClassName.'</td>';
			$x .= '</tr>';
		$x .= '</table>';
			$x .= '<hr>';
		$x .= "<table width=95% border=0  cellpadding=1 cellspacing=0 align='center'>";
			$x .= "<thead>";
			$x .= "<tr>";
				$x .= "<td width=30%><b>".$Lang['ePOS']['UserName']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['ClassName']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['ClassNumber']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['TransactionTimes']."</b></td>\n";
				$x .= "<td width=10%><b>".$Lang['ePOS']['LastTransactionDate']."</b></td>\n";				
				$x .= "<td width=30%><b>".$Lang['ePayment']['GrandTotal']."</b></td>\n";
			$x .= "</tr>";
			$x .= "</thead>";

		$x .= "<tbody>";
		for ($i=0; $i< sizeof($Result); $i++) {
			$x .= '<tr>';
				$x .= '<td>'.$Result[$i]['StudentName'].'</td>';
				$x .= '<td>'.$Result[$i]['ClassName'].'</td>';
				$x .= '<td>'.$Result[$i]['ClassNumber'].'</td>';
				$x .= '<td>'.$Result[$i]['Times'].'</td>';
				$x .= '<td>'.$Result[$i]['LastTransactionTime'].'</td>';
				$x .= '<td>'.$this->CurrencySign.$Result[$i]['GrandTotal'].'</td>';
			$x .= '</tr>';
		}
		$x .= "</tbody>";
		$x.="</table>";
		
		return $x;
	}
	
	function Get_POS_Student_Report_Export_Content($FromDate='',$ToDate='',$keyword='',$ClassName="",$field="",$order="")
	{
		global $Lang,$intranet_session_language,$PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
	        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
	        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
		
		$Result = $POS->Get_POS_Student_Report_Data($FromDate,$ToDate,$keyword,$ClassName,$field,$order);
		
		$UtfContent = $Lang['ePOS']['POSStudentReport']." (".$FromDate." ".$Lang['ePOS']['To']." ".$ToDate.")\r\n\r\n";
		$UtfContent .= $Lang['Payment']['Keyword']."\t".$keyword."\r\n";
		$UtfContent .= $Lang['ePOS']['ClassName']."\t".$ClassName."\r\n";
		$UtfContent .= "\r\n";
		
		$ExportColumn = array($Lang['ePOS']['UserName'],$Lang['ePOS']['ClassName'],$Lang['ePOS']['ClassNumber'],$Lang['ePOS']['TransactionTimes'],$Lang['ePOS']['LastTransactionDate'],$Lang['ePayment']['GrandTotal']);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			unset($Detail);
			$Detail[] = $Result[$i]['StudentName'];
			$Detail[] = $Result[$i]['ClassName'];
			$Detail[] = $Result[$i]['ClassNumber'];
			$Detail[] = $Result[$i]['Times'];
			$Detail[] = $Result[$i]['LastTransactionTime'];
			$Detail[] = $POS->getExportAmountFormat($Result[$i]['GrandTotal']);
			
			$Rows[] = $Detail;
		}	
		$ExportContent = $UtfContent.$lexport->GET_EXPORT_TXT($Rows,$ExportColumn);
		
		return $ExportContent;
	}
	
	function Get_POS_Student_Report_Detail($FromDate='', $ToDate='', $StudentID='')
	{
		global $intranet_session_language,$Lang;
		$POS = new libpos();
		
		$Result = $POS->Get_POS_Student_Report_Detail_Data($FromDate, $ToDate, $StudentID);
		
		$table_content .= '<style type="text/css" media="print">'."\n";
			$table_content .= 'thead {display: table-header-group;}'."\n";
		$table_content .= '</style>'."\n";
		$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
		$table_content .= "<tr>";
			$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['UserName']." : </b>".$Result[0]['Name']." (".$Result[0]['ClassName'].$Result[0]['ClassNumber'].")</td>";
		$table_content .= "</tr>";
		$table_content .= "<tr>";
			$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['TransactionTime']." : </b>".$FromDate." ".$Lang['ePOS']['To']." ".$ToDate."</td>";
		$table_content .= "</tr>";
		$table_content .="</table><br><hr/>";
		$table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
		$table_content .= "<thead>";
		$table_content .= "<tr><td class='$css_text'><b>".$Lang['ePOS']['Category']."</b></td><td class='$css_text'><b>".$Lang['ePayment']['ItemName']."</b></td>";
		$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
		$table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td></tr>";
		$table_content .= "</thead>";
		
		$table_content .= "<tbody>";
		for ($i=0; $i< sizeof($Result);$i++) {
			$css =$i%2==0?$css_table_content:$css_table_content."2";
			
			$table_content .= "<tr>";
				$table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
				$table_content .= "<td class='$css'>".$Result[$i]['Quantity']."</td>";
				$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
			$table_content .= "</tr>";
			$Total += $Result[$i]['GrandTotal'];
		}
		$table_content .= "<tr><td colspan='5' align=right>".$this->CurrencySign.$Total."</td></tr>";
		$table_content .= "</tbody></table>";
		
		return $table_content;
				
	}
	
	function Get_POS_Sales_Report_Layout()
	{
		global $Lang;
		
		//$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		$ToDate = $date_range_cookies['EndDate']!='' ? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		
		$x .= '
			<br>
			<form name="form1" id="form1" method="get" action="index.php" target="_blank" onsubmit="CheckDisplayFormat(this); return false;">
			<!-- date range -->
			<br>
			<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['ePOS']['SelectDateRange'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
						'.$this->GET_DATE_PICKER("FromDate",$FromDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
						'.$Lang['ePOS']['To'].'  
						'.$this->GET_DATE_PICKER("ToDate",$ToDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['ePOS']['DisplayFormat'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
						<input type="radio" name="display" value="html" id="html" checked><label for="html">'.$Lang["ePOS"]["PrinterFriendlyPage"].'</label>
						<input type="radio" name="display" value="csv" id="csv"><label for="csv">'.$Lang["ePOS"]["CSV"].'</label>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><br><br><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext" align="center">
						'. $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2") .'
					</td>
				</tr>
			</table>
			
			</form>';
			
			return $x;
	}
	
	function Get_POS_Sales_Report_PrintLayout($FromDate='',$ToDate='')
	{
		global $Lang, $sys_custom;
		
		$table = $this->Get_POS_Sales_Report_PrintTable($FromDate,$ToDate);
		
		# Hide Print Button when printing
		$x .= '<style type="text/css" media="print">'."\n";
			$x .= '.print_hide {display:none;}'."\n";
			$x .= 'thead {display: table-header-group;}'."\n";
			$x .= '@page{margin-left: 5mm;margin-right: 5mm;}'."\n";
		$x .= '</style>'."\n";
		
		### Print Button
		$x .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td align="right">'."\n";
					$x .= $this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "btn_print")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		if($sys_custom['ePOS']['ReportWithSchoolName']){
			$school_name = GET_SCHOOL_NAME();
			$x .= '<table width="90%" align="center" border="0">
					<tr>
						<td align="center"><h2><b>'.$school_name.'</b></h2></td>
					</tr>
				</table>';
		}
		
		$x.= "
			<br>
			<table border=\"0\" width=\"90%\" cellpadding=\"2\" align=\"center\">
				<tr>
					<td class='eSportprinttitle' colspan='3' align='center'><b>".$Lang['ePOS']['POSSalesReport']."</b></td>
				</tr>
				<tr>
					<td width='20%'>".$Lang['ePOS']['From']."</td>
					<td width='1'>:</td>
					<td>".$FromDate ."</td>
				</tr>
				<tr>
					<td>".$Lang['ePOS']['To']."</td>
					<td width='1'>:</td>
					<td>".$ToDate ."</td>
				</tr>
				<tr>
					<td>".$Lang['ePOS']['ReportGenerationTime']."</td>
					<td width='1'>:</td>
					<td>".date('Y-m-d H:i:s')."</td>
				</tr>
			</table>
			
			".$table."
			<BR><BR>";
			
			return $x;
	}
	
	function Get_POS_Sales_Report_PrintTable($FromDate='',$ToDate='')
	{
		global $intranet_session_language,$Lang, $i_ClassLevel;
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
	        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
	        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
		
		$Result = $POS->Get_POS_Sales_Report_Data($FromDate,$ToDate);
		
		# table start
		$x .= "<table align='center' class='calendar_board' style='width:90%; border: 1px solid #333333;'>";
		# table header
		// Category Name Row
		$x .= '<thead>';
		$x .= '<tr class="week_title">
						<td colspan="4" style="border:1px solid #333333;">
							'.$Lang['ePOS']['Category'].'
						</td>';
		foreach ((array)$Result['Category'] as $CategoryID => $CategoryName) {
			$x .= '<td colspan="'.sizeof($Result['Item'][$CategoryID]).'" align="center" style="border:1px solid #333333;">
							'.$CategoryName.'
						</td>';
			foreach ((array)$Result['Item'][$CategoryID] as $ItemID => $ItemName) {
				$ItemNameCol .= "<td align='center' class='eSporttdborder eSportprinttabletitle'>
													".$ItemName."
												</td>\n";
			}
		}

		$x .= '	<td style="border:1px solid #333333;">&nbsp;</td>
					</tr>';
			
		# Item name row 
		$x .= "<tr>";
			$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['RefCode']."</td>\n";
			$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePOS']['TransactionTime']."</td>\n";
			$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_ClassLevel."</td>\n";
			$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['Identity']['Student']."</td>\n";
			$x .= $ItemNameCol;
			$x .= "<td align='center' class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['GrandTotal']."</td>\n";
		$x .= "</tr>";
		
		$x .= '</thead>';
		
		$ItemTotalArr = array();
		$ItemTotalAmountArr = array();
		$GrandTotal = 0;
		$x .= '<tbody>';
		foreach ((array)$Result['ProcessDate'] as $LogID => $TimeStamp) {
			$TransactionTotal = 0;
			$student = str_replace('(', "\n(", $Result['TransactionDetail'][$LogID]['TransactionDetail']['StudentName']);
			$x .= '<tr>
							<td class="eSporttdborder eSportprinttext">'.$Result['TransactionDetail'][$LogID]['TransactionDetail']['InvoiceNumber'].'</td>
							<td class="eSporttdborder eSportprinttext">'.$Result['TransactionDetail'][$LogID]['TransactionDetail']['TransactionTime'].'</td>
							<td class="eSporttdborder eSportprinttext">'.$Result['TransactionDetail'][$LogID]['TransactionDetail']['YearName'].'</td>
							<td class="eSporttdborder eSportprinttext"><span style="white-space: pre;">'.$student.'</span></td>';
							
			foreach ((array)$Result['Category'] as $CategoryID => $CategoryName) {
				foreach ((array)$Result['Item'][$CategoryID] as $ItemID => $ItemName) {
					$x .= '<td align="center" class="eSporttdborder eSportprinttext">&nbsp;'.$Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemQty'].'</td>';
					$ItemTotalArr[$ItemID] += $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemQty'];
					$ItemTotalAmountArr[$ItemID] += $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemSubTotal'];
					$TransactionTotal += $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemSubTotal'];
				}
			}
			$x .= '<td align="center" class="eSporttdborder eSportprinttext">'.$this->CurrencySign.$TransactionTotal.'</td>';
			$x .= '</tr>';
			$GrandTotal += $TransactionTotal;
		}
		$x .= '<tr>
						<td colspan="4" class="eSporttdborder eSportprinttext">'.$Lang['ePayment']['GrandTotal'].'</td>';
						
		foreach ($ItemTotalArr as $ItemID => $ItemTotal) {
			$x .= '<td align="center" class="eSporttdborder eSportprinttext">&nbsp;'.$ItemTotal.'</td>';
		}
		$x .= '<td align="center" class="eSporttdborder eSportprinttext">'.$this->CurrencySign.$GrandTotal.'</td>';
		$x .= '</tr>';
		$x .= '<tr>
					<td colspan="4" class="eSporttdborder eSportprinttext">'.$Lang['ePOS']['ItemTotal'].'</td>';
						
		foreach ($ItemTotalAmountArr as $ItemID => $ItemTotal) {
			$x .= '<td align="center" class="eSporttdborder eSportprinttext">'.$this->CurrencySign.$ItemTotal.'</td>';
		}
		$x .= '<td align="center" class="eSporttdborder eSportprinttext">&nbsp;</td>';
		$x .= '</tr>';
		$x .= '</tbody>
			</table>';
		/*	# unit price
//			$x .= "<tr>";
//				$x .= "<td class='eSporttdborder eSportprinttabletitle'>&nbsp;</td>\n";
//				$x .= "<td class='eSporttdborder eSportprinttabletitle'>&nbsp;</td>\n";
//				for($i=0; $i<sizeof($ItemsAry);$i++)
//					$x .= "<td align='center' class='eSporttdborder eSportprinttabletitle'>$".$ItemsAry[$i]["UnitPrice"]."</td>\n";	
//				$x .= "<td class='eSporttdborder eSportprinttabletitle'>&nbsp;</td>\n";
//			$x .= "</tr>";
			
			# table content	
			# Qty Rows - loop each invoice	
			$TotalQty = array();	
			$GrandTotal=0;
			foreach((array)$Result as $InvoiceNumber => $InvoiceDetail) 
			{
				// Category Filter
				if(!empty($CategoryID))
				
				$TotalAmount = 0;
				
				$x .= '<tr>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.$InvoiceNumber.'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.$InvoiceDetail['DateInput'].'</td>';
					#Item Column - loop each item of invoice
					for($i=0; $i<sizeof($ItemsAry);$i++)
					{
						$thisItemID = $ItemsAry[$i]["ItemID"];
						$thisItemDetail = $InvoiceDetail["Items"][$thisItemID];
						
						if(!empty($thisItemDetail))
						{
							$x .= '<td align="center" class="eSporttdborder eSportprinttext">'.$thisItemDetail["ItemQty"].'</td>';
							$TotalAmount += $thisItemDetail["ItemSubTotal"];
							$TotalQty[$thisItemID] += $thisItemDetail["ItemQty"];
						}
						else
							$x .= '<td align="center" class="eSporttdborder eSportprinttext">0</td>';
					}
					
					$x .= '<td align="center" class="eSporttdborder eSportprinttext">$'.$TotalAmount.'</td>';
				$x .= '</tr>';
				
				$GrandTotal +=$TotalAmount;
			}
			
			# table footer
			# Total Qty row
			$x .= '<tr>';
				$x .= '<td colspan=2 class="eSporttdborder eSportprinttext">'.$Lang['ePayment']['GrandTotal'].'</td>';
				for($i=0; $i<sizeof($ItemsAry);$i++)
				{
					$thisItemID = $ItemsAry[$i]["ItemID"];
					if(!empty($TotalQty[$thisItemID]))
						$x .= '<td align="center" class="eSporttdborder eSportprinttext">'.$TotalQty[$thisItemID].'</td>';
					else
						$x .= '<td align="center" class="eSporttdborder eSportprinttext">0</td>';
						
				}
				$x .= '<td align="center" class="eSporttdborder eSportprinttext">$'.$GrandTotal.'</td>';
			$x .= '</tr>';
			
		$x.="</table>";*/
		
		return $x;
	}
	
	function Get_POS_Sales_Report_Export_Content($FromDate='',$ToDate='')
	{
		global $intranet_session_language,$Lang,$PATH_WRT_ROOT,$i_ClassLevel;
		
		$POS = new libpos();
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		$POS_item = new POS_item();
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		if($FromDate=="")
	        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
	        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
	        
	  $Result = $POS->Get_POS_Sales_Report_Data($FromDate,$ToDate); 
	       
		$ExportRowAry = array();
		# Category name row
		$ExportColumnAry = array();
		$ExportColumnAry[] = $Lang['ePOS']['Category'];
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = '';
		foreach ($Result['Category'] as $CategoryID => $CategoryName) {
			$ExportColumnAry[] = $CategoryName;
			for ($i=0; $i< (sizeof($Result['Item'][$CategoryID])-1); $i++) {
				$ExportColumnAry[] = '';
			}
		}
		$ExportRowAry[] = $ExportColumnAry;
		
		# Item name row 
		$ExportColumnAry = array();
		$ExportColumnAry[] = $Lang['ePOS']['RefCode'];
		$ExportColumnAry[] = $Lang['ePOS']['TransactionTime'];
		$ExportColumnAry[] = $i_ClassLevel;
		$ExportColumnAry[] = $Lang['Identity']['Student'];
		foreach ($Result['Category'] as $CategoryID => $CategoryName) {
			foreach ($Result['Item'][$CategoryID] as $ItemID => $ItemName) {			
				$ExportColumnAry[] = $ItemName;
			}
		}
		$ExportColumnAry[] = $Lang['ePayment']['GrandTotal'];
		$ExportRowAry[] = $ExportColumnAry;
		
		$ItemTotalArr = array();
		$ItemTotalAmountArr = array();
		$GrandTotal = 0;
		foreach ($Result['ProcessDate'] as $LogID => $TimeStamp) {
			$TransactionTotal = 0;
			
			$ExportColumnAry = array();
			$ExportColumnAry[] = $Result['TransactionDetail'][$LogID]['TransactionDetail']['InvoiceNumber'];
			$ExportColumnAry[] = $Result['TransactionDetail'][$LogID]['TransactionDetail']['TransactionTime'];
			$ExportColumnAry[] = $Result['TransactionDetail'][$LogID]['TransactionDetail']['YearName'];
			$ExportColumnAry[] = $Result['TransactionDetail'][$LogID]['TransactionDetail']['StudentName'];
			foreach ($Result['Category'] as $CategoryID => $CategoryName) {
				foreach ($Result['Item'][$CategoryID] as $ItemID => $ItemName) {			
					$ExportColumnAry[] =  $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemQty'];
					$ItemTotalArr[$ItemID] += $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemQty'];
					$ItemTotalAmountArr[$ItemID] += $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemSubTotal'];
					$TransactionTotal += $Result['TransactionDetail'][$LogID]['ItemDetail'][$ItemID]['ItemSubTotal'];
				}
			}
			$ExportColumnAry[] = $TransactionTotal;
			$ExportRowAry[] = $ExportColumnAry;
			$GrandTotal += $TransactionTotal;
		}
		
		$ExportColumnAry = array();
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = $Lang['ePayment']['GrandTotal'];						
		foreach ($ItemTotalArr as $ItemID => $ItemTotal) {
			$ExportColumnAry[] = $ItemTotal;
		}
		$ExportColumnAry[] = $GrandTotal;
		$ExportRowAry[] = $ExportColumnAry;
		$ExportColumnAry = array();
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = $Lang['ePOS']['ItemTotal'];
		foreach ($ItemTotalAmountArr as $ItemID => $ItemTotal) {
			$ExportColumnAry[] = $ItemTotal;
		}
		$ExportColumnAry[] = '';
		$ExportRowAry[] = $ExportColumnAry;
		/*
		# Qty Rows - loop each invoice	
		$TotalQty = array();	
		$GrandTotal=0;
		foreach((array)$Result as $InvoiceNumber => $InvoiceDetail) 
		{
			$TotalAmount = 0;
			
			$ExportColumnAry = array();
			$ExportColumnAry[] = $InvoiceNumber;
			$ExportColumnAry[] = $InvoiceDetail['DateInput'];
		
			#Item Column - loop each item of invoice
			for($i=0; $i<sizeof($ItemsAry);$i++)
			{
				$thisItemID = $ItemsAry[$i]["ItemID"];
				$thisItemDetail = $InvoiceDetail["Items"][$thisItemID];
				
				if(!empty($thisItemDetail))
				{
					$ExportColumnAry[] = $thisItemDetail["ItemQty"];
					$TotalAmount += $thisItemDetail["ItemSubTotal"];
					$TotalQty[$thisItemID] += $thisItemDetail["ItemQty"];
				}
				else
					$ExportColumnAry[] = 0;
					
			}
			$ExportColumnAry[] = '$'.$TotalAmount;	
			$GrandTotal += $TotalAmount;
			
			$ExportRowAry[] = $ExportColumnAry;
		}
		
		# Total Qty row
		$ExportColumnAry = array();
		$ExportColumnAry[] = '';
		$ExportColumnAry[] = $Lang['ePayment']['GrandTotal'];
	
		for($i=0; $i<sizeof($ItemsAry);$i++)
		{
			$thisItemID = $ItemsAry[$i]["ItemID"];
			if(!empty($TotalQty[$thisItemID]))
				$ExportColumnAry[] = $TotalQty[$thisItemID];
			else
				$ExportColumnAry[] = 0;
		}
		$ExportColumnAry[] = '$'.$GrandTotal;
		$ExportRowAry[] = $ExportColumnAry;
		*/
		$ExportContent = $lexport->GET_EXPORT_TXT($ExportRowAry,'');
		//debug_r($ExportContent);
		return $ExportContent;
	}
	
	function Get_Teminal_Setting_Form() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$Setting = $POS->Get_POS_Settings();
		
		$paymentMethodArr = array();
		$paymentMethodArr[] = array(1,$Lang['ePOS']['TapCard']);
		$paymentMethodArr[] = array(2,$Lang['ePOS']['StudentSelection']);
		$paymentMethodSelection = $this->GET_SELECTION_BOX($paymentMethodArr, ' name="PaymentMethod" onchange="$(this).val()==2?$(\'#ConfirmBeforePayOption\').show():$(\'#ConfirmBeforePayOption\').hide();" ', '', $Setting['PaymentMethod']);
		$confirmBeforeSubmitCheckbox = '<div id="ConfirmBeforePayOption"'.($Setting['PaymentMethod']==2?'':' style="display:none"').'>'.$Lang['ePOS']['ConfirmBeforePaying'].'&nbsp;'.$this->Get_Radio_Button('ConfirmBeforePayYes', 'ConfirmBeforePay', 1, $Setting['ConfirmBeforePay']==1,'',$Lang['General']['Yes']).'&nbsp;'.$this->Get_Radio_Button('ConfirmBeforePayNo', 'ConfirmBeforePay', 0, $Setting['ConfirmBeforePay']!=1,'',$Lang['General']['No']).'</div>';
		
		$x .= '<form name="form1" action="terminal_update.php" method="post">
						<table class="form_table_v30">
						<br />
							<tr>
								<td class="field_title">
									'.$Lang['ePOS']['AllowClientProgramConnection'].'
								</td>
								<td>
									'.$this->Get_Checkbox('AllowClientProgramConnect', 'AllowClientProgramConnect', 1, $Setting['AllowClientProgramConnect']).'
								</td>
							</tr>
							<tr>
								<td class="field_title">
									'.$Lang['ePOS']['TerminalIndependentCategorySetting'].'
								</td>
								<td class="tabletext" width="70%">
									'.$this->Get_Checkbox('TerminalIndependentCat', 'TerminalIndependentCat', 1, $Setting['TerminalIndependentCat']).'
								</td>
							</tr>
							<tr>
								<td class="field_title">'.$Lang['ePOS']['PaymentMethod'].'</td>
								<td>'.$paymentMethodSelection.$confirmBeforeSubmitCheckbox.'</td>
							</tr>
							<tr>
								<td class="field_title">
									'.$Lang['StaffAttendance']['TerminalIPList'].'
								</td>
								<td class="tabletext" width="70%">
									<span class="tabletextremark">'.$Lang['StaffAttendance']['TerminalIPInput'].'<br />
									'.$Lang['StaffAttendance']['TerminalYourAddress'].':'.getRemoteIpAddress().'</span><br />'
									.$this->GET_TEXTAREA("IPAllow", $Setting['IPAllow']).
								'</td>
							</tr>
							</table>
						<br />
						'.$Lang['StaffAttendance']['DescriptionTerminalIPSettings'].'
						<br /><br />
						<div class="edit_bottom_v30">
							<p class="spacer"></p>
							'.$this->GET_ACTION_BTN($Lang['Btn']['Save'], "submit").'
							'.$this->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset").'
							<p class="spacer"></p>
						</div>
					</form>
					<br>';

		return $x;
	}
	
	function Get_Manage_Transaction_Index($FromDate="",$ToDate="",$Keyword="",$PageNumber="1",$PageSize="20",$Order="1",$SortField="1") {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $page_size;
		
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		if($FromDate=="")
		  $FromDate = $date_range_cookies['StartDate']!='' ? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		if($ToDate=="")
		  $ToDate = $date_range_cookies['EndDate']!='' ? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		
		if (isset($PageSize) && $PageSize != "") $page_size = $PageSize;
		
		$Order = ($Order == 1) ? 1 : 0;
		if ($SortField == ""){
			 $SortField = 7;
		}
		
		# TABLE INFO
		$li = new libdbtable2007($SortField, $Order, $PageNumber);
		$li->field_array = array("InvoiceNumber",
														 "ClassName",
														 "ClassNumber",
														 "Name",
														 "SortGrandTotal",
														 "RefCode",
														 "ProcessName",
														 "ProcessDate",
														 "VoidName",
														 "VoidDate");
		$li->sql = $POS->Get_Manage_Transaction_Index_Sql($Keyword,$FromDate,$ToDate);
		$li->no_col = sizeof($li->field_array)+2;
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0,0,0,0);
		$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['InvoiceNumber'])."</td>\n";
		$li->column_list .= "<td width=5% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassName'])."</td>\n";
		$li->column_list .= "<td width=3% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassNumber'])."</td>\n";
		$li->column_list .= "<td width=13% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['UserName'])."</td>\n";
		$li->column_list .= "<td width=5% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['RefCode'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ProcessBy'])."</td>\n";
		$li->column_list .= "<td width=14% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ProcessDate'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['VoidBy'])."</td>\n";
		$li->column_list .= "<td width=9% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['VoidDate'])."</td>\n";
		$li->column_list .= "<td width=1 class=tableTitle>&nbsp;</td>\n";
		
		//$searchbar .= "<input class=text type=text name=Keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
		//$searchbar .= $this->GET_SMALL_BTN($button_search, "button", "document.form1.submit();","submit2");
		$x .= $this->Include_JS_CSS();
		$x .= '<br/>
					 <form name="form1" id="form1" method="get" action="index.php">';
		$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						'.$Lang['ePOS']['SelectDateRange'].'</td>
						<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
							'.$this->GET_DATE_PICKER("FromDate",$FromDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
							'.$Lang['ePOS']['To'].'
							'.$this->GET_DATE_PICKER("ToDate",$ToDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dotline"><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td colspan="2" class="tabletext" align="center">
							'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "Submit", "","submit2").'
						</td>
					</tr>
				</table>';
		$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
						<!--<tr>
							<td align="left">'.$toolbar.' '.$toolbar2.'</td>
						</tr>-->
						<tr>
							<td align="left">'.$searchbar.'</td>
						</tr>
						<tr>
							<td align="center">'.$li->display().'</td>
						</tr>
				</table>';
				
		$x .= '<input type=hidden name=pageNo value="'.$li->pageNo.'">
					<input type=hidden name=order value="'.$li->order.'">
					<input type=hidden name=field value="'.$li->field.'">
					<input type=hidden name=page_size_change value="">
					<input type=hidden name=numPerPage value="'.$li->page_size.'">
					<input type=hidden id="ReturnMessage" name="ReturnMessage" value="">
					</form>
					<br/>';
		
		return $x;
	}
	
	function Get_Manage_Pickup_Index($field=8, $order=0, $pageNo=1, $page_size=20,$FromDate='',$ToDate='',$ClassName='',$RecordStatus='',$keyword='',$void='')
	{
		global $PATH_WRT_ROOT,$intranet_session_language, $Lang, $image_path, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		
		$POS = new libpos();
		
		# date range
		$today_ts = strtotime(date('Y-m-d'));
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		if($FromDate==""){
			$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
		}
		if($ToDate==""){
			$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
		}
		//$this->Set_Report_Date_Range_Cookies($FromDate, $ToDate);
		
		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("InvoiceNumber","ClassName","ClassNumber","Name","Detail","GrandTotalForSort","REFCode","DateInput","Status");
		$li->sql = $POS->Get_Manage_Pickup_Index_Sql($FromDate,$ToDate,$ClassName,$RecordStatus,$keyword,true,'','',$void);
		$li->no_col = sizeof($li->field_array)+2;
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0,0);
		$li->wrap_array = array(0,0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['InvoiceNumber'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassName'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['ClassNumber'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['UserName'])."</td>\n";
		$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['General']['Details'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['RefCode'])."</td>\n";
		$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePOS']['TranscationTime'])."</td>\n";
		$li->column_list .= "<td width=8% class=tableTitle>".$li->column($pos++, $Lang['General']['Status'])."</td>\n";
		$li->column_list .= "<td width=2% class=tableTitle></td>\n";
		
		
		$lclass = new libclass();
		$select_class = $lclass->getSelectClass("name=ClassName onChange=\"document.form1.submit();\"",$_REQUEST['ClassName'],"",'-- '.$Lang['General']['All'].' --');
		
		# Item RecordStatus Filter
		$recordStatusAry = array(
								array('C',$Lang['ePOS']['Completed']),
								array('NC',$Lang['ePOS']['NotCompleted'])
							);
		$RecordStatusFilter = getSelectByArray($recordStatusAry, ' id="RecordStatus" name="RecordStatus" onchange="document.form1.submit();" ', $RecordStatus, 1, 0, '', 1);
		
		$toolbar = $this->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
		$toolbar2 = $this->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate&field=$field&order=$order&void=$void')","","","","",0);
		$toolbar3 = '<div class="Conntent_tool"><a href="#TB_inline?height=450&amp;width=750&amp;inlineId=FakeLayer" class="thickbox export" title="'.$Lang['ePOS']['ExportOrderList'].'" onclick="Get_Export_Form(\''.$FromDate.'\', \''.$ToDate.'\'); return false"> '.$Lang['ePOS']['ExportOrderList'].'</a></div>';

		$searchbar .= $this->Get_Search_Box_Div('keyword', $keyword);
		$actionPage = $void=='1'?"voidindex.php":"index.php";
		
		$x = $this->Include_JS_CSS()."\n";
		$x .='
			<br>
			<form name="form1" id="form1" method="get" action="'.$actionPage.'">
				<input type="hidden" name="ItemName" id="ItemName" value="">
				<!-- date range -->
				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['ePOS']['SelectDateRange'] .'</td>
						<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
							'.$this->GET_DATE_PICKER("FromDate",$FromDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
							'.$Lang['ePOS']['To'].' 
							'.$this->GET_DATE_PICKER("ToDate",$ToDate).'
							<span class="tabletextremark">(yyyy-mm-dd)</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dotline"><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td colspan="2" class="tabletext" align="center">
							'. $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2") .'
						</td>
					</tr>
				</table>

				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
					<tr>
						<td align="left">'.$toolbar.' '.$toolbar2.' '.$toolbar3.'</td>
						<td align="right">'.$searchbar .'</td>
					</tr>
					<tr>
						<td align="left" colspan="2"><span id="ClassLayer" '.$ClassDisplay.'>'.$select_class.'&nbsp;'.$RecordStatusFilter.'</span></td>
					</tr>
					<tr>
						<td colspan="2">'. $li->display() .'</td>
					</tr>
				</table>
				
				
				<input type=hidden name=pageNo value="'.$li->pageNo .'">
				<input type=hidden name=order value="'.$li->order .'">
				<input type=hidden name=field value="'.$li->field .'">
				<input type=hidden name=page_size_change value="">
				<input type=hidden name=numPerPage value="'.$li->page_size.'">
                <input type=hidden name=void value="'.$void.'">
			</form><br><br>';

		return $x;
	}
	
	function Get_Manage_Pickup_Detail($LogID='',$FromDate='', $ToDate='',$void='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $intranet_session_language,$Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$POS = new libpos();
	
		### Page Navigation
		$NavigationArr = array();
		$NavigationArr[] = array($Lang['ePOS']['PickupManagement'], "javascript:js_Back_To_Pickup_Index();");
		$NavigationArr[] = array($Lang['ePOS']['SingleOrderDetails'], '');
		$NavigationHTML = $this->GET_NAVIGATION($NavigationArr);
		
		if(!empty($LogID))
			$LogIDAry = array($LogID);
		else
		    $LogIDAry = $POS->Get_POS_Transaction_LogID_By_Date($FromDate,$ToDate,$void);
		
		// for the details
		$Result = $POS->Get_POS_Transaction_Log_Data($LogID,$void);
		if (count($Result) == 0) {
		    $Result = $POS->Get_POS_Transaction_Log_Data($LogID,$void,$allVoid=true);       // get void data
		}
//debug_pr($Result);

//		$css_table_content = 'tablebluerow';
		
        $totalCategory = 0;
		for ($i=0; $i< sizeof($Result);$i++) {
//			$css =$i%2==0?$css_table_content:$css_table_content."2";
//			
//			$table_content .= "<tr>";
//				$table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
//				$table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
//				$table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
//				$table_content .= "<td class='$css'>".$this->CurrencySign.$Result[$i]['ItemSubTotal']."</td>";
//				$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
//			$table_content .= "</tr>";
			$Total += $Result[$i]['GrandTotal'];
			$ReceiveAmounts += $Result[$i]['ReceiveAmount'];
			$ItemQtys += $Result[$i]['ItemQty'];
			if ($Result[$i]['ItemQty']) {
			    $totalCategory++;
			}
		}
			
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<div>'."\n";
				$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$NavigationHTML.'</td>'."\n";
						$x .= '<td align="right">&nbsp;</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>';
				
				$x .= '<br style="clear:both">';
				$x .= '<form id="form1" name="form1" method="post" onsubmit="return false;">'."\n";	
				
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">'."\n";
                    $x .= '<tbody>'."\n";
                   		$x .= '<tr>'."\n";
                   			$x .= '<td>'."\n";
				$x .= '<table width="95%" border="0" cellpadding="2" cellspacing="0">'."\n";
					# Invoice Number
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePayment']['InvoiceNumber'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['InvoiceNumber'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Class Name
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['ClassName'] .'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['ClassName'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Class Number
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['ClassNumber'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['ClassNumber'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# User Name
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['UserName'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['Name'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Detail
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['General']['Details'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$totalCategory.' '.$Lang['ePOS']['NumberOfItems'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Total
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['General']['Total'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$this->CurrencySign.number_format((float)$Total, 2, '.', '').'</td>'."\n";
					$x .= '</tr>'."\n";
					# Ref Code
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['RefCode'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['RefCode'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Transaction Time
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['TransactionTime'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['DateInput'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Ref Code
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['General']['Status'].'</td>'."\n";
						$x .= '<td class="tabletext">'.( $ReceiveAmounts >= $ItemQtys ? $Lang['ePOS']['Completed'] : $Lang['ePOS']['NotCompleted'] ) .'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td width="300">'."\n";
                    $x .= '<div class="orderDetailArea">'."\n";
                      $x .= '<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox delete" title="'.$Lang['ePOS']['Void'].'" onclick="Get_Void_Transaction_Form(\''.$Result[0]['LogID'].'\'); return false">'."\n";
                        $x .= '<img src="'.$PATH_WRT_ROOT.'/images/2009a/ePOS/icon_remove.png" alt="" style="width:20px;vertical-align:middle"> '.$Lang['ePOS']['VoidThisOrder']."\n";
                      $x .= '</a>'."\n";
                      $x .= '<div class="orderQRArea" style="visibility: hidden">'."\n";
                        $x .= '<img src="'.$PATH_WRT_ROOT.'/home/eAdmin/GeneralMgmt/pos/qrcode.php?encode='.$Result[0]['RefCode'].'" alt="">'."\n";
                      $x .= '</div>'."\n";
                    $x .= '</div>'."\n";
                $x .= '</td>'."\n";
                $x .= '</tr>'."\n";
              	$x .= '</tbody>'."\n";
            	$x .= '</table>'."\n";
            	$x .= '<br>'."\n";
				
				// Item not yet pickup
				$sql = $POS->Get_Manage_Pickup_Detail_Sql($LogID, 'NotComplete');
				$NotCompleteLogArr = $POS->returnArray($sql);
				$x .= '<div>';
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>
                          <tr>
                            <td valign="top">
                             	 '.$Lang['ePOS']['ItemDoNotTake'].'
                            </td>';
                            if($NotCompleteLogArr){
                            $x .= '<td valign="bottom">
                              <div class="common_table_tool">
                                <a href="javascript:checkItem(document.form1,\'NotCompleteItemID[]\',\'change_item_thickbox_content.php\',\'Change\',\'NotTake\')" class="tool_edit">'.$Lang['ePOS']['ChangeItem'].'</a>
                                <a href="javascript:checkItem(document.form1,\'NotCompleteItemID[]\',\'writeoff_item_thickbox_content.php\',\'Void\',\'NotTake\')" class="tool_delete">'.$Lang['ePOS']['WriteoffItem'].'</a>
                                <a href="javascript:checkItem(document.form1,\'NotCompleteItemID[]\',\'pickup_item_thickbox_content.php\',\'Pickup\',\'NotTake\')" class="tool_approve">'.$Lang['ePOS']['Take'].'</a>
                              </div>
                            </td>';
                            }
                          $x .= '</tr>
                        </tbody>
                      </table>';
//				$x .= '<div class="common_table_tool">
//<a class="tool_edit" href="javascript: void(0);">編輯</a>
//<a class="tool_edit" href="javascript: void(0);">Cust Lang</a>
//<a class="tool_delete" href="javascript: void(0);">刪除</a>
//<a class="tool_approve" href="javascript: void(0);">批核</a>
//<a class="tool_reject" href="javascript: void(0);">拒絕</a>
//<a class="tool_assign_to_student" href="javascript: void(0);">指派到學生</a>
//<a class="tool_unassign_to_student" href="javascript: void(0);">指派到學生</a>
//<a class="tool_totop" href="javascript: void(0);">置頂</a>
//<a class="tool_set" href="javascript: void(0);">指定</a>
//<a class="tool_other" href="javascript: void(0);">Other Action</a>
//</div>';
//				$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
//					### Item List Table
//					$x .= '<tr>'."\n";
//						$x .= '<td colspan="2" align="center">'."\n";
//							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Manage_Pickup_Detail_Table($LogID, 'NotComplete', $PageNumber, $PageSize, $Order, $SortField)."\n";
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
				$x .= '</div><br/><br/>';
				
				// Item already pickup
				$sql = $POS->Get_Manage_Pickup_Detail_Sql($LogID, 'Complete');
				$CompleteLogArr = $POS->returnArray($sql);
				if($CompleteLogArr){
				$x .= '<div>';
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>
                          <tr>
                            <td valign="top">
                              	'.$Lang['ePOS']['ItemTaken'].'
                            </td>';
                            
                            $x .= '<td valign="bottom">
                              <div class="common_table_tool">
                                <a href="javascript:checkItem(document.form1,\'CompleteItemID[]\',\'change_item_thickbox_content.php\',\'Change\',\'Take\')" class="tool_edit">'.$Lang['ePOS']['ChangeItem'].'</a>
                                <a href="javascript:checkItem(document.form1,\'CompleteItemID[]\',\'writeoff_item_thickbox_content.php\',\'Void\',\'Take\')" class="tool_delete">'.$Lang['ePOS']['WriteoffItem'].'</a>
                              </div>
                            </td>';
                            
                          $x .= '</tr>
                        </tbody>
                      </table>';
//				$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
//					### Item List Table
//					$x .= '<tr>'."\n";
//						$x .= '<td colspan="2" align="center">'."\n";
//							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Manage_Pickup_Detail_Table($LogID, 'Complete', $PageNumber, $PageSize, $Order, $SortField)."\n";
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
				$x .= '</div><br/><br/>'."\n";
				}
				// Item already void
				$sql = $POS->Get_Manage_Pickup_Detail_Sql($LogID, 'Void');
//debug_pr($sql);				
				$VoidLogArr = $POS->returnArray($sql);
				if($VoidLogArr){
				$x .= '<div>';
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tbody>
                          <tr>
                            <td valign="top">
                              	'.$Lang['ePOS']['ChangeItemAndVoidRecord'].'
                            </td>
                            <td valign="bottom">

                            </td>
                          </tr>
                        </tbody>
                      </table>';
				
//				$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
//					### Item List Table
//					$x .= '<tr>'."\n";
//						$x .= '<td colspan="2" align="center">'."\n";
//							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Manage_Pickup_Detail_Table($LogID, 'Void', $PageNumber, $PageSize, $Order, $SortField)."\n";
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
				$x .= '</div><br/>'."\n";
				}
				$x .= '	<input type="hidden" id="pageNo" name="pageNo" value="'.$PageNumber.'">
				<input type="hidden" id="order" name="order" value="'.$Order.'">
				<input type="hidden" id="field" name="field" value="'.$SortField.'">
				<input type="hidden" id="page_size_change" name="page_size_change" value="">
				<input type="hidden" id="numPerPage" name="numPerPage" value="'.$PageSize.'">
				'."\n";
				$x .= '<input type="hidden" id="LogID" name="LogID" value="'.$LogID.'" />'."\n";
				$x .= '</form>'."\n";
				
				### Back Button
				$btn_Print = $this->GET_ACTION_BTN($Lang['ePOS']['PrintReceipt'], "button", $onclick="js_Go_Print()", $id="Btn_Print");
				if($NotCompleteLogArr){
					$btn_Pickup = $this->GET_ACTION_BTN($Lang['ePOS']['PickupAll'], "button", $onclick="js_Pickup_All()", $id="Btn_Pickup");
				}
				
				$x .= '<br style="clear:both" />'."\n";
				$x .= '<div class="edit_bottom">'."\n";
					$x .= $btn_Print."\n";
					$x .= $btn_Pickup."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>'."\n";
				
//		for($j=0; $j<sizeof($LogIDAry);$j++)
//		{
//			$Total = 0;
//			$thisLogID = $LogIDAry[$j];
//			$Result = $POS->Get_POS_Transaction_Log_Data($thisLogID,$void);
//		
//			$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['UserName']." : </b>".$Result[0]['Name']." (".$Result[0]['ClassName'].$Result[0]['ClassNumber'].")</td>";
//				$table_content .= "</tr>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['TransactionTime']." : </b>".$Result[0]['DateInput']."</td>";
//				$table_content .= "</tr>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['RefCode']." : </b>".$Result[0]['RefCode']."</td>";
//				$table_content .= "</tr>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><B>".$Lang['ePayment']['InvoiceNumber']." : </b>".$Result[0]['InvoiceNumber']."</td>";
//				$table_content .= "</tr>";
//			$table_content .="</table><br>";
//			$table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
//				$table_content .= "<thead>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['Category']."</b></td>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['ItemName']."</b></td>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['UnitPrice']."</b></td>";
//					$table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td>";
//				$table_content .= "</tr>";
//				$table_content .= "</thead>";
//				$table_content .= "<tbody>";
//				
//			for ($i=0; $i< sizeof($Result);$i++) {
//				$css =$i%2==0?$css_table_content:$css_table_content."2";
//				
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
//					$table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
//					$table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
//					$table_content .= "<td class='$css'>".$this->CurrencySign.$Result[$i]['ItemSubTotal']."</td>";
//					$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
//				$table_content .= "</tr>";
//				$Total += $Result[$i]['GrandTotal'];
//			}
//			$table_content .= "<tr><td colspan='5' align=right>".$this->CurrencySign.$Total."</td></tr>";
//			$table_content .= "</tbody></table><br><br><br>";
//			
//		}
		
		return $x;
	}
	
	function Get_Report_Pickup_Detail($LogID='',$FromDate='', $ToDate='',$void='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $intranet_session_language,$Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$POS = new libpos();
	
		### Page Navigation
		$NavigationArr = array();
		$NavigationArr[] = array($Lang['ePOS']['POSTransactionReport'], "javascript:js_Back_To_Report_Index();");
		$NavigationArr[] = array($Lang['ePOS']['SingleOrderDetails'], '');
		$NavigationHTML = $this->GET_NAVIGATION($NavigationArr);
		
		if(!empty($LogID))
			$LogIDAry = array($LogID);
		else
		    $LogIDAry = $POS->Get_POS_Transaction_LogID_By_Date($FromDate,$ToDate,$void);
		
		// for the details
		$Result = $POS->Get_POS_Transaction_Log_Data($LogID,$void);
		if (count($Result) == 0) {
		    $Result = $POS->Get_POS_Transaction_Log_Data($LogID,$void,$allVoid=true);       // get void data
		}
		
//		$css_table_content = 'tablebluerow';
		
		$totalCategory = 0;
		for ($i=0; $i< sizeof($Result);$i++) {
//			$css =$i%2==0?$css_table_content:$css_table_content."2";
//			
//			$table_content .= "<tr>";
//				$table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
//				$table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
//				$table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
//				$table_content .= "<td class='$css'>".$this->CurrencySign.$Result[$i]['ItemSubTotal']."</td>";
//				$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
//			$table_content .= "</tr>";
			$Total += $Result[$i]['GrandTotal'];
			$ReceiveAmounts += $Result[$i]['ReceiveAmount'];
			$ItemQtys += $Result[$i]['ItemQty'];
			if ($Result[$i]['ItemQty']) {
			    $totalCategory++;
			}
		}
			
		$x = '';
		$x .= $this->Include_JS_CSS()."\n";
		$x .= '<br />'."\n";
		$x .= '<div>'."\n";
				$x .= '<table width="95%" border="0" cellpadding"0" cellspacing="0">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'.$NavigationHTML.'</td>'."\n";
						$x .= '<td align="right">&nbsp;</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>';
				
				$x .= '<br style="clear:both">';
				$x .= '<form id="form1" name="form1" method="post" onsubmit="return false;">'."\n";	
				
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">'."\n";
                    $x .= '<tbody>'."\n";
                   		$x .= '<tr>'."\n";
                   			$x .= '<td>'."\n";
				$x .= '<table width="95%" border="0" cellpadding="2" cellspacing="0">'."\n";
					# Invoice Number
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePayment']['InvoiceNumber'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['InvoiceNumber'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Class Name
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['ClassName'] .'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['ClassName'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Class Number
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['ClassNumber'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['ClassNumber'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# User Name
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['UserName'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['Name'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Detail
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['General']['Details'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$totalCategory.' '.$Lang['ePOS']['NumberOfItems'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Total
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['General']['Total'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$this->CurrencySign.number_format((float)$Total, 2, '.', '').'</td>'."\n";
					$x .= '</tr>'."\n";
					# Ref Code
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['RefCode'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['RefCode'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Transaction Time
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['ePOS']['TransactionTime'].'</td>'."\n";
						$x .= '<td class="tabletext">'.$Result[0]['DateInput'].'</td>'."\n";
					$x .= '</tr>'."\n";
					# Ref Code
					$x .= '<tr>'."\n";
						$x .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['General']['Status'].'</td>'."\n";
						$x .= '<td class="tabletext">'.( $ReceiveAmounts >= $ItemQtys ? $Lang['ePOS']['Completed'] : $Lang['ePOS']['NotCompleted'] ) .'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td width="300">'."\n";
                    $x .= '<div class="orderDetailArea">'."\n";
                      $x .= '<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox delete" title="'.$Lang['ePOS']['Void'].'" onclick="Get_Void_Transaction_Form(\''.$Result[0]['LogID'].'\'); return false">'."\n";
                        $x .= '<img src="'.$PATH_WRT_ROOT.'/images/2009a/ePOS/icon_remove.png" alt="" style="width:20px;vertical-align:middle"> '.$Lang['ePOS']['VoidThisOrder']."\n";
                      $x .= '</a>'."\n";
                      $x .= '<div class="orderQRArea">'."\n";
                        $x .= '<img src="'.$PATH_WRT_ROOT.'/home/eAdmin/GeneralMgmt/pos/qrcode.php?encode='.$Result[0]['InvoiceNumber'].'" alt="">'."\n";
                      $x .= '</div>'."\n";
                    $x .= '</div>'."\n";
                $x .= '</td>'."\n";
                $x .= '</tr>'."\n";
              	$x .= '</tbody>'."\n";
            	$x .= '</table>'."\n";
            	$x .= '<br>'."\n";
				
				// Item not yet pickup
				$x .= '<div>';
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
						<tbody>
                          <tr>
                            <td valign="top">
                             	 '.$Lang['ePOS']['ItemDoNotTake'].'
                            </td>';
                          $x .= '</tr>
                        </tbody>
                      </table>';
//				$x .= '<div class="common_table_tool">
//<a class="tool_edit" href="javascript: void(0);">編輯</a>
//<a class="tool_edit" href="javascript: void(0);">Cust Lang</a>
//<a class="tool_delete" href="javascript: void(0);">刪除</a>
//<a class="tool_approve" href="javascript: void(0);">批核</a>
//<a class="tool_reject" href="javascript: void(0);">拒絕</a>
//<a class="tool_assign_to_student" href="javascript: void(0);">指派到學生</a>
//<a class="tool_unassign_to_student" href="javascript: void(0);">指派到學生</a>
//<a class="tool_totop" href="javascript: void(0);">置頂</a>
//<a class="tool_set" href="javascript: void(0);">指定</a>
//<a class="tool_other" href="javascript: void(0);">Other Action</a>
//</div>';
//				$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
//					### Item List Table
//					$x .= '<tr>'."\n";
//						$x .= '<td colspan="2" align="center">'."\n";
//							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Manage_Pickup_Detail_Table($LogID, 'NotComplete', $PageNumber, $PageSize, $Order, $SortField)."\n";
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
				$x .= '</div><br/><br/>';
				
				// Item already pickup
				$sql = $POS->Get_Manage_Pickup_Detail_Sql($LogID, 'Complete');
				$CompleteLogArr = $POS->returnArray($sql);
				if($CompleteLogArr){
				$x .= '<div>';
//				$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
//					### Item List Table
//					$x .= '<tr>'."\n";
//						$x .= '<td colspan="2" align="center">'."\n";
//							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Manage_Pickup_Detail_Table($LogID, 'Complete', $PageNumber, $PageSize, $Order, $SortField)."\n";
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
				$x .= '</div><br/><br/>'."\n";
				}
				// Item already void
				$sql = $POS->Get_Manage_Pickup_Detail_Sql($LogID, 'Void');
				$VoidLogArr = $POS->returnArray($sql);
				if($VoidLogArr){
				$x .= '<div>';
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tbody>
                          <tr>
                            <td valign="top">
                              	'.$Lang['ePOS']['ChangeItemAndVoidRecord'].'
                            </td>
                            <td valign="bottom">

                            </td>
                          </tr>
                        </tbody>
                      </table>';
				
//				$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
//					### Item List Table
//					$x .= '<tr>'."\n";
//						$x .= '<td colspan="2" align="center">'."\n";
//							$x .= '<div id="LogTableDiv">'."\n";
								$x .= $this->Get_Manage_Pickup_Detail_Table($LogID, 'Void', $PageNumber, $PageSize, $Order, $SortField)."\n";
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
				$x .= '</div><br/>'."\n";
				$x .= '	<input type="hidden" id="pageNo" name="pageNo" value="'.$PageNumber.'">
				<input type="hidden" id="order" name="order" value="'.$Order.'">
				<input type="hidden" id="field" name="field" value="'.$SortField.'">
				<input type="hidden" id="page_size_change" name="page_size_change" value="">
				<input type="hidden" id="numPerPage" name="numPerPage" value="'.$PageSize.'">
				'."\n";
				$x .= '<input type="hidden" id="LogID" name="LogID" value="'.$LogID.'" />'."\n";
				$x .= '</form>'."\n";
				}
				### Back Button
				$btn_Print = $this->GET_ACTION_BTN($Lang['ePOS']['PrintReceipt'], "button", $onclick="js_Go_Print()", $id="Btn_Print");
				if($NotCompleteLogArr){
					$btn_Pickup = $this->GET_ACTION_BTN($Lang['ePOS']['PickupAll'], "button", $onclick="js_Pickup_All()", $id="Btn_Pickup");
				}
				
				$x .= '<br style="clear:both" />'."\n";
				$x .= '<div class="edit_bottom">'."\n";
					$x .= $btn_Print."\n";
					$x .= $btn_Pickup."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>'."\n";
				
//		for($j=0; $j<sizeof($LogIDAry);$j++)
//		{
//			$Total = 0;
//			$thisLogID = $LogIDAry[$j];
//			$Result = $POS->Get_POS_Transaction_Log_Data($thisLogID,$void);
//		
//			$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['UserName']." : </b>".$Result[0]['Name']." (".$Result[0]['ClassName'].$Result[0]['ClassNumber'].")</td>";
//				$table_content .= "</tr>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['TransactionTime']." : </b>".$Result[0]['DateInput']."</td>";
//				$table_content .= "</tr>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['RefCode']." : </b>".$Result[0]['RefCode']."</td>";
//				$table_content .= "</tr>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><B>".$Lang['ePayment']['InvoiceNumber']." : </b>".$Result[0]['InvoiceNumber']."</td>";
//				$table_content .= "</tr>";
//			$table_content .="</table><br>";
//			$table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
//				$table_content .= "<thead>";
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['Category']."</b></td>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['ItemName']."</b></td>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
//					$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['UnitPrice']."</b></td>";
//					$table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td>";
//				$table_content .= "</tr>";
//				$table_content .= "</thead>";
//				$table_content .= "<tbody>";
//				
//			for ($i=0; $i< sizeof($Result);$i++) {
//				$css =$i%2==0?$css_table_content:$css_table_content."2";
//				
//				$table_content .= "<tr>";
//					$table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
//					$table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
//					$table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
//					$table_content .= "<td class='$css'>".$this->CurrencySign.$Result[$i]['ItemSubTotal']."</td>";
//					$table_content .= "<td class='$css' align=right>".$this->CurrencySign.$Result[$i]['GrandTotal']."</td>";
//				$table_content .= "</tr>";
//				$Total += $Result[$i]['GrandTotal'];
//			}
//			$table_content .= "<tr><td colspan='5' align=right>".$this->CurrencySign.$Total."</td></tr>";
//			$table_content .= "</tbody></table><br><br><br>";
//			
//		}
		
		return $x;
	}
	
	function Get_Manage_Pickup_Detail_Table($LogID, $LogType='', $PageNumber='', $PageSize='', $Order='', $SortField='', $ShowCheckbox=true)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		$libPOS = new libpos();
		
		# Default Table Settings
		$PageNumber = ($PageNumber == '')? 1 : $PageNumber;
		$PageSize = ($PageSize == '')? 10000 : $PageSize;
		if ((($LogType=='Void') || ($LogType=='Complete')) && ($SortField == '')) {
		    $SortField = 6;   // order by TransactionTime 
		    $Order = 1;
		}
		$Order = ($Order == '')? 0 : $Order;
		$SortField = ($SortField == '')? 0 : $SortField;
		
		# TABLE INFO
		$namefield = getNameFieldByLang("iu.");
		$li = new libdbtable2007($SortField, $Order, $PageNumber);
		if($LogType=='Void'){
			$li->field_array = array("CategoryName", "ItemName", "ItemQty", "ItemSubTotal", "GrandTotal", "Remarks", "TransactionTime");
		}
		else if($LogType=='Complete'){
		    $li->field_array = array("CategoryName", "ItemName", "ItemQty", "ItemSubTotal", "GrandTotal", "Complete", "TransactionTime");
		}
		else {
			$li->field_array = array("CategoryName", "ItemName", "ItemQty", "ItemSubTotal", "GrandTotal");
		}
		$li->sql = $libPOS->Get_Manage_Pickup_Detail_Sql($LogID, $LogType, $ShowCheckbox);
		$li->no_col = sizeof($li->field_array) + ($ShowCheckbox?($LogType=='Void'?1:2):0);
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0);
		$li->wrap_array = array(0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		$li->page_size = $PageSize;
		$li->with_navigation = false;
		
		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='1' class='tablebluetop tableTitle'>#</td>\n";
		$li->column_list .= "<td width='20%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePOS']['Category'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['ItemName'])."</td>\n";
		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['Quantity'])."</td>\n";
		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['UnitPrice'])."</td>\n";
		$li->column_list .= "<td width='15%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";
		if($LogType=='Void'){
			$li->column_list .= '<td width="15%" class="tablebluetop tableTitle">'.$li->column($pos++, $Lang['ePOS']['Remark']).'</td>'."\n";
			$li->column_list .= '<td width="10%" class="tablebluetop tableTitle">'.$li->column($pos++, $Lang['ePOS']['TransactionTime']).'</td>'."\n";
		}
		if($LogType=='Complete'){
		    $li->column_list .= '<td width="7%" class="tablebluetop tableTitle">'.$li->column($pos++, $Lang['ePOS']['Taken']).'</td>'."\n";
		    $li->column_list .= '<td width="10%" class="tablebluetop tableTitle">'.$li->column($pos++, $Lang['ePOS']['TransactionTime']).'</td>'."\n";
		}
		if($LogType!='Void'){
			$li->column_list .= '<td width="8%" class="tablebluetop tableTitle"><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\''.$LogType.'ItemID[]\'):setChecked(0,this.form,\''.$LogType.'ItemID[]\')"></td>'."\n";
		}
		
		$x = '';
		$x .= @$li->displayFormat2('100%', "blue")."\n";
		
		return $x;
	}
	
	function Get_Void_POS_Transaction_Form($TransactionLogID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$TransactionDetail = $POS->Get_POS_Transaction_Detail($TransactionLogID);

		if ($TransactionDetail[0]['IsTransactionVoided'] == '1') {
			$VoidDetail = $POS->Get_POS_Transaction_Void_Detail($TransactionDetail[0]['VoidLogID']);
			$hasAvailableItem = false;
            $remark = $Lang['ePOS']['InventoryReturnedRemark'];
		}
		else {
            $hasAvailableItem = true;
            $remark = $Lang['ePOS']['InventoryReturnRemark'];
        }
        $PurchaseDetail = $POS->Get_POS_Transaction_Purchase_Detail($TransactionLogID, $hasAvailableItem);

		//$Lang['ePOS']['VoidBy']
		//$Lang['ePOS']['VoidDate']
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="VoidTransactionForm" id="VoidTransactionForm" onsubmit="return false;">
					<input type="hidden" name="TransactionLogID" id="TransactionLogID" value="'.$TransactionLogID.'">
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
							<table class="form_table">
								<tr>
									<td>'.$Lang['ePayment']['InvoiceNumber'].'</td>
									<td>:</td>
									<td>'.$TransactionDetail[0]['InvoiceNumber'].'</td> 
								</tr>
								<tr>
									<td>'.$Lang['ePOS']['UserName'].'</td>
									<td>:</td>
									<td>'.$TransactionDetail[0]['Name'].'</td> 
								</tr>
								<tr>
									<td>'.$Lang['ePayment']['GrandTotal'].'</td>
									<td>:</td>
									<td>'.$this->CurrencySign.$TransactionDetail[0]['GrandTotal'].'</td> 
								</tr>
								<tr>
									<td>'.$Lang['ePOS']['ProcessBy'].'</td>
									<td>:</td>
									<td>'.$TransactionDetail[0]['ProcessName'].'</td> 
								</tr>
								<tr>
									<td>'.$Lang['ePOS']['ProcessDate'].'</td>
									<td>:</td>
									<td>'.$TransactionDetail[0]['ProcessDate'].'</td> 
								</tr>';
		if ($TransactionDetail[0]['IsTransactionVoided'] == '1') {
			$x .= '		<tr>
									<td>'.$Lang['ePOS']['VoidBy'].'</td>
									<td>:</td>
									<td>'.$TransactionDetail[0]['VoidName'].'</td> 
								</tr>
								<tr>
									<td>'.$Lang['ePOS']['VoidDate'].'</td>
									<td>:</td>
									<td>'.$TransactionDetail[0]['VoidDate'].'</td> 
								</tr>';
		}
		$x .= '			<tr>
									<td>'.$Lang['ePOS']['Remark'].'</td>
									<td>:</td>
									<td>'.$this->GET_TEXTAREA('Remark',$TransactionDetail[0]['Remark']).'</td> 
								</tr>
								<tr>
									<td>'.$Lang['ePOS']['InventoryReturnSetting'].'</td>
									<td>:</td>
									<td>
										<table class="common_table_list">
											<tr>
												<th>
													'.$Lang['ePOS']['Category'].'
												</th>
												<th>
													'.$Lang['ePayment']['ItemName'].'
												</th>
												<th nowrap>
													'.$Lang['ePayment']['Quantity'].'
												</th>
												<th style="width: 120px;">
													*'.$Lang['ePOS']['InventoryReturn'].'
												</th>
											</tr>';
		for ($i=0; $i< sizeof($PurchaseDetail); $i++) {
			if ($TransactionDetail[0]['IsTransactionVoided'] == '1') {
				$_CSS = '';
				$Content = ($VoidDetail[$PurchaseDetail[$i]['ItemID']]['Amount'] != "")? $VoidDetail[$PurchaseDetail[$i]['ItemID']]['Amount']:"0";
			}
			else {
                $_CSS = 'style="background-color:#EFFDDB;"';
				$Content = '<select id="'.$PurchaseDetail[$i]['ItemID'].'" name="InventoryReturn[]" onchange="Change_Item_Return_Background(this);">';
				for ($j=0; $j<= $PurchaseDetail[$i]['ItemQty']; $j++) {
					$Content .= '<option value="'.$j.'" '.(($j == $PurchaseDetail[$i]['ItemQty'])? 'selected':'').'>'.$j.'</option>';
				}
				$Content .= '</select>';
			}
			$CSS = strlen($_CSS) > 0 ? 'style="' . $_CSS . '"' : '';
			$x .= '<tr>';
			$x .= '<td style="max-width: 120px;'.$_CSS.'">'.$PurchaseDetail[$i]['CategoryName'].'</td>';
			$x .= '<td style="max-width: 200px;'.$_CSS.'">'.$PurchaseDetail[$i]['ItemName'].'</td>';
			$x .= '<td '.$CSS.'>'.$PurchaseDetail[$i]['ItemQty'].'</td>';
			$x .= '<td '.$CSS.'>'.$Content.'</td>';
			$x .= '</tr>';
		}
		$x .= '					</table>
										<span class="tabletextremark">*'.$remark.'</span>
									</td> 
								</tr>
								<col class="field_title" />
								<col class="field_c" />
							</table>
						</div>
					</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>';
		if ($TransactionDetail[0]['IsTransactionVoided'] == '1') {
			$x .= '<input name="submit2" type="button" class="formbutton" onclick="Void_Transaction();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].' '.$Lang['ePOS']['Remark'].'" />';
		}
		else {
			$x .= '<input name="submit2" type="button" class="formbutton" onclick="Void_Transaction();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['ePOS']['Void'].'" />';
		}
		$x .= '	<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
					</div>
				</div>';
		return $x;
	}
	
	function Get_Teminal_Manage_Index() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$Settings = $POS->Get_POS_Settings();
		$AllowClientProgramConnect = $Settings['AllowClientProgramConnect'];
		$thisActiveIconTitle 	= ($AllowClientProgramConnect==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
		$thisID = 'AllowClientProgramConnectIcon';
		$thisJS = 'javascript:js_Change_Client_Connection()';
		
		$ClientConnectShortCut = '<div id="AllowClientProgramConnectDiv" style="float:right">
																<span style="float:left;">
																	'.$Lang['ePOS']['AllowClientProgramConnection'].': 
																</span>
																<span class="table_row_tool" style="vertical-align:top;">
																	'.$this->GET_LNK_ACTIVE($thisJS, $AllowClientProgramConnect, $thisActiveIconTitle, 0, $thisID).'
																</span>
															</div>';
		$x .= $this->Include_JS_CSS();
		$x .= '
					<br/>
					<div class="content_top_tool" id="SearchInputLayer" style="width:95%">
	          <div class="Conntent_search">
	          	<input name="Keyword" id="Keyword" type="text" onkeyup="Check_Go_Search(event);"/>
	          </div>
          	<br style="clear:both" />
          	<div style="float:left;">
          		<a href="#" class="tablelink" onclick="Process_Sync_Photo_Request(\'\',\'YES\'); return false;">'.$Lang['ePOS']['SyncPhotoForAllTerminal'].'</a>
          	</div>
          	'.$ClientConnectShortCut.'
         	</div>
					
          <div class="table_board" id="ManageTerminalListLayer" style="width:95%">
          	'.$this->Get_Teminal_Manage_Table().'
        	</div>
        	<br/>';
        	
		return $x;
	}
	
	function Get_Teminal_Manage_Table($Keyword="") {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$Setting = $POS->Get_POS_Settings();
		$TemplateList = $POS->Get_Terminal_Template_List($Keyword);
		$x = '<table class="common_table_list">
              <tr>
                <th width="20%"> 
                	<span style="float:left;">'.$Lang['ePOS']['SiteName'].'</span> ';
    $x .= '    		<span class="table_row_tool row_content_tool" style="float:right">
                		<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox add" title="'.$Lang['ePOS']['AddSite'].'" onclick="Get_Template_Form(); return false;"></a>
                	</span>';
    $x .= '     </th>';
    if ($Setting['TerminalIndependentCat'] == "1") {
    	$x .= '		<th width="5%"> '.$Lang['ePOS']['CategoryAssociated'].'</th>';
    }
    $x .= '			<th width="10%"> '.$Lang['ePOS']['StationName'].'</th>
    						<th width="10%"> '.$Lang['ePOS']['MacAddress'].'</th>
                <th width="13%"> '.$Lang['ePOS']['IP'].'</th>
                <th width="15%"> '.$Lang['ePOS']['LastConnectTime'].'</th>
                <th width="16%"> '.$Lang['ePOS']['LastPhotoSyncTime'].'</th>
                <th width="13%">
                	&nbsp;
                </th>
              </tr>
              <col />
              <col />
              <col />
              <col />
              <tbody>';
		for ($i=0; $i< sizeof($TemplateList); $i++) {
			$TerminalList = $POS->Get_Terminal_List($Keyword,$TemplateList[$i]['TemplateID']);
			$x .= '<tr>
	            <td rowspan="'.(sizeof($TerminalList)+2).'">'.$TemplateList[$i]['TemplateName'].'</td>';
	   	if ($Setting['TerminalIndependentCat'] == "1") {
	      $x .= '	<td rowspan="'.(sizeof($TerminalList)+2).'">
	    						<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox" title="'.$Lang['ePOS']['SetTerminalCategorySetting'].'" onclick="Get_Category_Setting_Form(\''.$TemplateList[$i]['TemplateID'].'\'); return false;">'.$TemplateList[$i]['CategoryAssociated'].'</a>
								</td>';
	    }
	    $x .= '<td colspan="6" style="display:none;">&nbsp;</td>';
	    $x .= '	</tr>';
			for ($j=0; $j < sizeof($TerminalList); $j++) {
				
				$thisIsActive = $TerminalList[$j]['TerminalStatus'];
				$thisActiveIconTitle = ($thisIsActive==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
				$thisID = 'TerminalStatus_'.$TerminalList[$j]['TerminalID'];
				$thisJS = 'javascript:js_Disable_Terminal('.$TerminalList[$j]['TerminalID'].')';
				$DisableBtn = $this->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);
				
				$x .= '<tr class="sub_row">
                  <td>'.$TerminalList[$j]['SiteName'].'</td>
                  <td>'.$TerminalList[$j]['MacAddress'].'</td>
                  <td>'.$TerminalList[$j]['IPAddress'].'</td>
                  <td>'.$TerminalList[$j]['LastConnected'].'</td>
                  <td>'.(($TerminalList[$j]['LastPhotoSync'] == "")? '--':$TerminalList[$j]['LastPhotoSync']).'</td>
                  <td>
                  	<span class="table_row_tool row_content_tool" style="float:right;">
						'.$DisableBtn.'	
                  		<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="zing_photo_dim thickbox" onclick="Get_Sync_Photo_Form(\''.$TerminalList[$j]['TerminalID'].'\'); return false;" title="'.$Lang['ePOS']['SyncPhotoForThisTerminal'].'"></a>
		   				<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="setting_row thickbox" title="'.$Lang['ePOS']['AssignToAnotherSite'].'" onclick="Get_Site_Selection_Form(\''.$TerminalList[$j]['TerminalID'].'\'); return false;"></a>
		    			<a href="#" class="delete_dim" title="'.$Lang['ePOS']['DeleteTerminal'].'" onclick="Remove_Terminal(\''.$TerminalList[$j]['TerminalID'].'\'); return false;"></a>
					</span>
        		</td>
                </tr>';
			}
			$x .= '<tr>
							<td colspan="6">
							<span class="table_row_tool row_content_tool" style="float:right;">
								<a href="#" class="delete_dim" title="'.$Lang['ePOS']['RemoveSite'].'" onclick="Remove_Template(\''.$TemplateList[$i]['TemplateID'].'\'); return false;"></a>
							</span>
							</td>
						</tr>';
		}
		$TerminalList = $POS->Get_Terminal_List($Keyword);
		if (sizeof($TerminalList) > 0) {
			$x .= '<tr>
							<td rowspan="'.(sizeof($TerminalList)+1).'">'.$Lang['ePOS']['TerminalUnassigned'].'</td>';
			if ($Setting['TerminalIndependentCat'] == "1") 
				$x .= '<td rowspan="'.(sizeof($TerminalList)+1).'">NA</td>';
			$x .= '<td colspan="6" style="display:none;">&nbsp;</td>';
			$x .= '</tr>';
			for ($j=0; $j < sizeof($TerminalList); $j++) {
				
				$thisIsActive = $TerminalList[$j]['TerminalStatus'];
				$thisActiveIconTitle = ($thisIsActive==1)? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
				$thisID = 'TerminalStatus_'.$TerminalList[$j]['TerminalID'];
				$thisJS = 'javascript:js_Disable_Terminal('.$TerminalList[$j]['TerminalID'].')';
				$DisableBtn = $this->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);
				
				$x .= '<tr class="sub_row">
	                <td>'.$TerminalList[$j]['SiteName'].'</td>
	                <td>'.$TerminalList[$j]['MacAddress'].'</td>
	                <td>'.$TerminalList[$j]['IPAddress'].'</td>
	                <td>'.$TerminalList[$j]['LastConnected'].'</td>
	                <td>'.$TerminalList[$j]['LastPhotoSync'].'</td>
	                <td>
	                	<span class="table_row_tool row_content_tool" style="float:right;">
							'.$DisableBtn.'	
	                		<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="zing_photo_dim thickbox" onclick="Get_Sync_Photo_Form(\''.$TerminalList[$j]['TerminalID'].'\'); return false;" title="'.$Lang['ePOS']['SyncPhotoForThisTerminal'].'"></a>
							<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="setting_row thickbox" title="'.$Lang['ePOS']['AssignToAnotherSite'].'" onclick="Get_Site_Selection_Form(\''.$TerminalList[$j]['TerminalID'].'\'); return false;"></a>
		    			  	<a href="#" class="delete_dim" title="'.$Lang['ePOS']['DeleteTerminal'].'" onclick="Remove_Terminal(\''.$TerminalList[$j]['TerminalID'].'\'); return false;"></a>
						</span>
	      			</td>
	              </tr>';
			}
		}
		$x .= '		</tbody>
        		</table> ';
		
		return $x;
	}
	
	function Get_Terminal_Template_Form($TemplateID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		if ($TemplateID != "") {
			$POS = new libpos();
			$TemplateDetail = $POS->Get_Terminal_Template_Detail($TemplateID);
		}
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="TemplateForm" id="TemplateForm" onsubmit="return false;">
					<input type="hidden" name="TemplateID" id="TemplateID" value="'.$TemplateID.'">
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
							<table class="form_table">
								<tr>
									<td>'.$Lang['ePOS']['SiteName'].'</td>
									<td>:</td>
									<td><input id="TemplateName" name="TemplateName" maxlength="100" class="textbox" type="text" value="'.$TemplateDetail[0]['TemplateID'].'" onkeyup="Check_Template_Name(this.value);"></td> 
								</tr>
								<tr id="TemplateNameWarningRow" name="TemplateNameWarningRow" style="display:none;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><div id="TemplateNameWarningLayer" style="color:red;"></div></td> 
								</tr>
								<col class="field_title" />
								<col class="field_c" />
							</table>
						</div>
					</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Save_Template();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" />
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
					</div>
				</div>';
				
		return $x;		
	}
	
	function Get_Terminal_Form($TerminalID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		if ($TerminalID != "") {
			$POS = new libpos();
			$TerminalDetail = $POS->Get_Terminal_Detail($TerminalID);
		}
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="TerminalForm" id="TerminalForm" onsubmit="return false;">
					<input type="hidden" name="TerminalID" id="TerminalID" value="'.$TerminalID.'">
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
							<table class="form_table">
								<tr>
									<td>'.$Lang['ePOS']['SiteName'].'</td>
									<td>:</td>
									<td><input id="SiteName" name="SiteName" maxlength="100" class="textbox" type="text" value="'.$TerminalDetail[0]['SiteName'].'" onkeyup="Check_Site_Name(this.value);"></td> 
								</tr>
								<tr id="SiteNameWarningRow" name="SiteNameWarningRow" style="display:none;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><div id="SiteNameWarningLayer" style="color:red;"></div></td> 
								</tr>
								<col class="field_title" />
								<col class="field_c" />
							</table>
						</div>
					</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Save_Terminal();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" />
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
					</div>
				</div>';
		
		return $x;
	}
	
	function Get_Terminal_Category_Linkage_Form($TemplateID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$TemplateDetail = $POS->Get_Terminal_Template_Detail($TemplateID);
		
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="TerminalCategoryForm" id="TerminalCategoryForm" onsubmit="return false;">
					<input type="hidden" name="TemplateID" id="TemplateID" value="'.$TemplateID.'">
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
						<table class="form_table">
							<tr>
								<td>'.$Lang['ePOS']['SiteName'].'</td>
								<td>:</td>
								<td>'.$TemplateDetail[0]['TemplateName'].'</td> 
							</tr>
							<col class="field_title" />
							<col class="field_c" />
							<tr>
								<td>'.$Lang['ePOS']['SetTerminalCategorySetting'].'</td>
								<td>:</td>
								<td><div id="CategoryListLayer">'.$this->Get_Settings_Category_Table("",false,$TemplateID).'</div></td> 
							</tr>
						</table>
					</div>
					</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Get_Terminal_List(); window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Close'].'" />
					</div>
				</div>';
				
		return $x;
	}
	
	function Get_Terminal_Template_Selection_Form($TerminalID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$TerminalDetail = $POS->Get_Terminal_Detail($TerminalID);
		$TemplateList = $POS->Get_Terminal_Template_List();
		
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="TerminalCategoryForm" id="TerminalCategoryForm" onsubmit="return false;">
					<input type="hidden" name="TerminalID" id="TerminalID" value="'.$TerminalID.'">
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
						<table class="form_table">
							<tr>
								<td>'.$Lang['ePOS']['StationName'].'</td>
								<td>:</td>
								<td>'.$TerminalDetail[0]['SiteName'].'</td> 
							</tr>
							<col class="field_title" />
							<col class="field_c" />
							<tr>
								<td>'.$Lang['ePOS']['SetTerminalCategorySetting'].'</td>
								<td>:</td>
								<td>
									<select id="TemplateID" name="TemplateID">
										<option value="">'.$Lang['ePOS']['TerminalUnassigned'].'</option>';
	for ($i=0; $i< sizeof($TemplateList); $i++) {
		if ($TemplateList[$i]['TemplateID'] == $TerminalDetail[0]['TemplateID']) 
			$Selected = "selected";
		else 
			$Selected = "";
		$x .= '					<option value="'.$TemplateList[$i]['TemplateID'].'" '.$Selected.'>'.$TemplateList[$i]['TemplateName'].'</option>';
	}
	$x .= '					</select>
								</td> 
							</tr>
						</table>
					</div>
					</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Save_Terminal_Template_Selection();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" />
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Close'].'" />
					</div>
				</div>';
				
		return $x;
	}
	
	function Get_Terminal_Sync_Photo_Form($TerminalID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$POS = new libpos();
		$TerminalDetail = $POS->Get_Terminal_Detail($TerminalID);
		$TemplateList = $POS->Get_Terminal_Template_List();
		
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="TerminalCategoryForm" id="TerminalCategoryForm" onsubmit="return false;">
					<input type="hidden" name="TerminalID" id="TerminalID" value="'.$TerminalID.'">
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
						<table class="form_table">
							<tr>
								<td>'.$Lang['ePOS']['StationName'].'</td>
								<td>:</td>
								<td>'.$TerminalDetail[0]['SiteName'].'</td> 
							</tr>
							<tr>
								<td>'.$Lang['ePOS']['MacAddress'].'</td>
								<td>:</td>
								<td>'.$TerminalDetail[0]['MacAddress'].'</td> 
							</tr>
							<tr>
								<td>'.$Lang['ePOS']['IP'].'</td>
								<td>:</td>
								<td>'.$TerminalDetail[0]['IPAddress'].'</td> 
							</tr>
							<tr>
								<td>'.$Lang['ePOS']['LastConnectTime'].'</td>
								<td>:</td>
								<td>'.$TerminalDetail[0]['LastConnected'].'</td> 
							</tr>
							<tr>
								<td>'.$Lang['ePOS']['LastPhotoSyncTime'].'</td>
								<td>:</td>
								<td>'.(($TerminalDetail[0]['LastPhotoSync'] == "")? '--':$TerminalDetail[0]['LastPhotoSync']).'</td> 
							</tr>
							<tr>
								<td>'.$Lang['ePOS']['LastPhotoRequestTime'].'</td>
								<td>:</td>
								<td>'.(($TerminalDetail[0]['LastPhotoRequest'] == "")? '--':$TerminalDetail[0]['LastPhotoRequest']).'</td> 
							</tr>
							<tr>
								<td>'.$Lang['ePOS']['LastPhotoRequestBy'].'</td>
								<td>:</td>
								<td>'.(($TerminalDetail[0]['LastRequestBy'] == "")? '--':$TerminalDetail[0]['LastRequestBy']).'</td> 
							</tr>
							<col class="field_title" />
							<col class="field_c" />
						</table>
					</div>
					</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Process_Sync_Photo_Request(\''.$TerminalID.'\');" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['ePOS']['SyncPhotoForThisTerminal'].'" />
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Close'].'" />
					</div>
				</div>';
				
		return $x;
	}
	
	function Get_Health_Personal_Report_Layout($IsISmartCard=false) {
		global $Lang, $PATH_WRT_ROOT, $image_path;
		
		//$POS = new libpos();
		
		# date range
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d');
		$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d');
		
		include_once($PATH_WRT_ROOT.'includes/libclass.php');
		$lc = new libclass();
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$lu = new libuser($_SESSION['UserID']);
		
		$x .= '
			<br>
			<form name="form1" id="form1" method="post">
			<!-- date range -->
			<br>
			<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['ePOS']['SelectDateRange'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
						'.$this->GET_DATE_PICKER("StartDate",$FromDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
						'.$Lang['ePOS']['To'].'  
						'.$this->GET_DATE_PICKER("EndDate",$ToDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
					</td>
				</tr>';
		if (!($lu->isStudent() || $lu->isParent())) {
			$x .= '		
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['SysMgr']['FormClassMapping']['Class'] .'</td>
						<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
							'.$lc->getSelectClassID('id="ClassID" name="ClassID" onchange="Get_Student_List(this.value);"',"",1).'
						</td>
					</tr>';
		}
		$x .= '
				<tr id="StudentRow" style="display:none;">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">';
		if ($IsISmartCard) {
			if ($lu->isStudent()) { // student
				$x .= '<input type="hidden" name="StudentID[]" id="StudentID[]" value="'.$_SESSION["UserID"].'">';
			}
			else { // parent
				include_once($PATH_WRT_ROOT.'includes/libfamily.php');
				$lfamily = new libfamily();
				$students = $lfamily->returnChildrens($_SESSION['UserID']);
				
				for ($i=0; $i< sizeof($students); $i++) {
					$x .= '<input type="hidden" name="StudentID[]" id="StudentID[]" value="'.$students[$i].'">';
				}
			}
		}
		else {
			$x .= '
						<div name="StudentLayer" id="StudentLayer">
						</div>';
		}
		$x .= '
						<div name="StudentWarningLayer" id="StudentWarningLayer" style="color:red;">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><br><br><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext" align="center" '.((!$IsISmartCard)?'style="display:none;"':'').' id="SubmitCell">
						'. $this->GET_ACTION_BTN($Lang['Btn']['View'], "button","Get_Health_Personal_Report();","submit2") .'
					</td>
				</tr>
			</table>
			
			</form>
			
			<div id="ReportLayer" name="ReportLayer">
			</div>';
			
			return $x;
	}	
	
	function Get_Health_Personal_Report($StartDate,$EndDate,$StudentList,$Format="WEB") {
		global $Lang, $PATH_WRT_ROOT;
		
		include_once('libpos.php');
		$pos = new libpos();
		
		list($StudentList,$StudentIntakeInfo) = $pos->Get_Health_Personal_Data($StartDate,$EndDate,$StudentList);
		$IngredientList = $pos->Get_HealthIngredient_List();
		
		if($Format=="PRINT") { // Print
			$x .= '<style type="text/css" media="print">'."\n";
				$x .= 'thead {display: table-header-group;}'."\n";
			$x .= '</style>'."\n";
			$x .= '<table width="96%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>
						</tr>
					</table>
				   <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
					 <tr>
						<td align="left"><b>'.$Lang['ePOS']['HealthPersonalReport'].'</b></td>
					 </tr>
					 <tr>
						<td align="left"><b>'.$Lang['ePOS']['From'].' '.$StartDate.' '.$Lang['ePOS']['To'].' '.$EndDate.'</b></td>
					 </tr>
				   </table>
				   <p>&nbsp;</p>';
		}
		else if($Format=="EXPORT"){ // Export
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				
			$Export = new libexporttext();
			
			$ExportColumn = array($Lang['ePOS']['HealthPersonalReport']);
			
			$Detail[] = $Lang['ePOS']['TargetDate'];
			$Detail[] = $Lang['ePOS']['From'].' '.$StartDate.' '.$Lang['ePOS']['To'].' '.$EndDate;
			
			$Rows[] = $Detail;
			$Rows[] = array(' ');
		}
		else { // WEB
			$x .= '<form id="form2" name="form2" method="POST" action="">';
			$x .=	'<input type="hidden" name="StartDate" id="StartDate" value="'.$StartDate.'">';
			$x .=	'<input type="hidden" name="EndDate" id="EndDate" value="'.$EndDate.'">';
			for ($i=0; $i< sizeof($StudentList); $i++) {
				$x .= '<input type="hidden" name="StudentID[]" value="'.$StudentList[$i]['UserID'].'">';
			}
			$x .= '<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td align="left">';
			$x .= '     '.$this->GET_LNK_PRINT("#","","PrintPage(document.form2); return false;","","",0).'&nbsp;';
			$x .= '		'.$this->GET_LNK_EXPORT("#","","ExportPage(document.form2); return false;","","",0).'
								</td>
							</tr>
						</table>
						</form>';
		}
		
		for ($i=0; $i< sizeof($StudentList); $i++) {
			list($StudentID,$StudentName,$ClassName,$ClassNumber) = $StudentList[$i];
			if ($Format != "EXPORT") {
				if ($Format == "WEB") {
					$TableStyle = 'class="common_table_list rights_table_list"';
					$TableRowTopStyle = 'class="tabletop"';
					$TableCellTopStyle = '';
					$TableCellStyle = '';
				}
				else {
					$TableStyle = 'align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="96%"';
					$TableRowTopStyle = 'class="tabletop"';
					$TableCellTopStyle = 'class="eSporttdborder eSportprinttext"';
					$TableCellStyle = 'class="eSporttdborder eSportprinttabletitle"';
				}
					
				$x .= '<table '.$TableStyle.'>
						  	<thead>
						  	<tr>
						  		<td '.$TableCellTopStyle.' colspan="4">'.$Lang['ePOS']['UserName'].': '.$StudentName.' '.(($ClassName != "")? '('.$ClassName.'-'.$ClassNumber.')':"").'</td>
						  	</tr>
						  	<tr '.$TableRowTopStyle.'>
						  		<th '.$TableCellStyle.'>'.$Lang['ePOS']['Name'].'</th>
									<th '.$TableCellStyle.'>'.$Lang['ePOS']['StandardIntakePerDay'].'('.$Lang['ePOS']['Unit'].')</th>
									<th '.$TableCellStyle.'>'.$Lang['ePOS']['AvgIntake'].'</th>
									<th '.$TableCellStyle.'>'.$Lang['ePOS']['Various'].'</th>
						    </tr>
							</thead>
							<tbody>';
			}
			else { // export
				unset($Detail);
				$Detail[] = $Lang['ePOS']['UserName'];
				$Detail[] = $StudentName.' '.(($ClassName != "")? '('.$ClassName.'-'.$ClassNumber.')':"");
				$Rows[] = $Detail;
				
				unset($Detail);
				$Detail[] = $Lang['ePOS']['Name'];
				$Detail[] = $Lang['ePOS']['StandardIntakePerDay'].'('.$Lang['ePOS']['Unit'].')';
				$Detail[] = $Lang['ePOS']['AvgIntake'];
				$Detail[] = $Lang['ePOS']['Various'];
				$Rows[] = $Detail;
			}
			
			for ($j=0; $j< sizeof($IngredientList); $j++) {
				$Various = $StudentIntakeInfo[$StudentID][$IngredientList[$j]['HealthIngredientID']] - $IngredientList[$j]['StandardInTakePerDay'];
				if ($Format != "EXPORT") {
					if ($Format == "PRINT") {
						$ClassName = ' class="eSporttdborder eSportprinttext"';
					}
					
					$x .= '<tr>
									<td '.$ClassName.'>'.$IngredientList[$j]['HealthIngredientName'].'</td>
									<td '.$ClassName.'>'.$IngredientList[$j]['StandardInTakePerDay'].'('.$IngredientList[$j]['UnitName'].')</td>
									<td '.$ClassName.'>'.($StudentIntakeInfo[$StudentID][$IngredientList[$j]['HealthIngredientID']]+0).'</td>
									<td '.$ClassName.'>'.(($Various > 0)? $Various:"&nbsp;").'</td>
								</tr>';
				}
				else { // export
					unset($Detail);
					$Detail[] = $IngredientList[$j]['HealthIngredientName'];
					$Detail[] = $IngredientList[$j]['StandardInTakePerDay'].'('.$IngredientList[$j]['UnitName'].')';
					$Detail[] = ($StudentIntakeInfo[$StudentID][$IngredientList[$j]['HealthIngredientID']]+0);
					$Detail[] = ($Various > 0)? $Various:"";	
					$Rows[] = $Detail;					
				}
			}
			
			if($Format != "EXPORT")
			{
				$x .= '</tbody>';
				$x .= '</table>
					   <p>&nbsp;</p>';
			} 
			else { //  export
				$Rows[] = array(' ');
			}
		}
		
		if ($Format != "EXPORT") {
			return $x;
		}
		else {
			$filename = "personal_health.csv";
			$export_content = $Export->GET_EXPORT_TXT($Rows, $ExportColumn);
			$Export->EXPORT_FILE($filename, $export_content);
		}
	}
	
	function Get_Health_Class_Report_Layout() {
		global $Lang, $PATH_WRT_ROOT;
		
		//$POS = new libpos();
		
		# date range
		$date_range_cookies = $this->Get_Report_Date_Range_Cookies();
		$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d');
		$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d');
		
		include_once($PATH_WRT_ROOT.'includes/libclass.php');
		$lc = new libclass();
		
		$x .= '
			<br>
			<form name="form1" id="form1" method="post">
			<!-- date range -->
			<br>
			<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['ePOS']['SelectDateRange'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
						'.$this->GET_DATE_PICKER("StartDate",$FromDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
						'.$Lang['ePOS']['To'].'  
						'.$this->GET_DATE_PICKER("EndDate",$ToDate).'
						<span class="tabletextremark">(yyyy-mm-dd)</span>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'. $Lang['SysMgr']['FormClassMapping']['Class'] .'</td>
					<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
						'.$lc->getSelectClassID('id="ClassID" name="ClassID" multiple style="min-width:200px; width:200px;"',-1,0).'
						<div name="ClassWarningLayer" id="ClassWarningLayer" style="color:red;">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><br><br><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext" align="center">
						'. $this->GET_ACTION_BTN($Lang['Btn']['View'], "button","Get_Health_Class_Report();","submit2") .'
					</td>
				</tr>
			</table>
			
			</form>
			
			<div id="ReportLayer" name="ReportLayer">
			</div>';
			
			return $x;
	}
	
	function Get_Health_Class_Report($StartDate,$EndDate,$ClassList,$Format="WEB") {
		global $Lang, $PATH_WRT_ROOT;
		
		include_once('libpos.php');
		$pos = new libpos();
		
		include_once('form_class_manage.php');
		for ($i=0; $i< sizeof($ClassList); $i++) {
			$ClassObj = new year_class($ClassList[$i],false,false,true);
			
			$ClassObjList[] = $ClassObj;
			
			for ($j=0; $j< sizeof($ClassObj->ClassStudentList); $j++) {
				$StudentList[] = $ClassObj->ClassStudentList[$j]['UserID'];
			}
		}
		unset($ClassObj);
		
		list($StudentList,$StudentIntakeInfo) = $pos->Get_Health_Personal_Data($StartDate,$EndDate,$StudentList);
		$IngredientList = $pos->Get_HealthIngredient_List();
		
		if($Format=="PRINT") { // Print
			$x .= '<style type="text/css" media="print">'."\n";
				$x .= 'thead {display: table-header-group;}'."\n";
			$x .= '</style>'."\n";
			$x .= '<table width="96%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>
						</tr>
					</table>
				   <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
					 <tr>
						<td align="left"><b>'.$Lang['ePOS']['HealthClassReport'].'</b></td>
					 </tr>
					 <tr>
						<td align="left"><b>'.$Lang['ePOS']['From'].' '.$StartDate.' '.$Lang['ePOS']['To'].' '.$EndDate.'</b></td>
					 </tr>
				   </table>
				   <p>&nbsp;</p>';
		}
		else if($Format=="EXPORT"){ // Export
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				
			$Export = new libexporttext();
			
			$ExportColumn = array($Lang['ePOS']['HealthClassReport']);
			
			$Detail[] = $Lang['ePOS']['TargetDate'];
			$Detail[] = $Lang['ePOS']['From'].' '.$StartDate.' '.$Lang['ePOS']['To'].' '.$EndDate;
			
			$Rows[] = $Detail;
			$Rows[] = array(' ');
		}
		else { // WEB
			$x .= '<form id="form2" name="form2" method="POST" action="">';
			$x .=	'<input type="hidden" name="StartDate" id="StartDate" value="'.$StartDate.'">';
			$x .=	'<input type="hidden" name="EndDate" id="EndDate" value="'.$EndDate.'">';
			for ($i=0; $i< sizeof($ClassList); $i++) {
				$x .= '<input type="hidden" name="ClassID[]" value="'.$ClassList[$i].'">';
			}
			$x .= '</form>';
			$x .= '<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td align="left">';
			$x .= '     '.$this->GET_LNK_PRINT("#","","PrintPage(document.form2); return false;","","",0).'&nbsp;';
			$x .= '		'.$this->GET_LNK_EXPORT("#","","ExportPage(document.form2); return false;","","",0).'
								</td>
							</tr>
						</table>
						</form>';
		}
		
		for ($i=0; $i< sizeof($ClassObjList); $i++) {
			if ($Format != "EXPORT") {
				if ($Format == "WEB") {
					$TableStyle = 'class="common_table_list rights_table_list"';
					$TableRowTopStyle = 'class="tabletop"';
					$TableCellTopStyle = '';
					$TableCellStyle = '';
				}
				else {
					$TableStyle = 'align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="96%"';
					$TableRowTopStyle = 'class="tabletop"';
					$TableCellTopStyle = 'class="eSporttdborder eSportprinttext"';
					$TableCellStyle = 'class="eSporttdborder eSportprinttabletitle"';
				}
					
				$x .= '<table '.$TableStyle.'>
						  	<thead>
						  	<tr>
						  		<td '.$TableCellTopStyle.' colspan="3">'.$Lang['ePOS']['ClassName'].': '.$ClassObjList[$i]->Get_Class_Name().'</td>
						  	</tr>
						  	<tr '.$TableRowTopStyle.'>
						  		<th '.$TableCellStyle.'>'.$Lang['ePOS']['Name'].'</th>
									<th '.$TableCellStyle.'>'.$Lang['ePOS']['StandardIntakePerDay'].'('.$Lang['ePOS']['Unit'].')</th>
									<th '.$TableCellStyle.'>'.$Lang['ePOS']['ExceedNumberOfPeople'].'</th>
						    </tr>
							</thead>
							<tbody>';
			}
			else { // export
				unset($Detail);
				$Detail[] = $Lang['ePOS']['ClassName'];
				$Detail[] = $ClassObjList[$i]->Get_Class_Name();
				$Rows[] = $Detail;
				
				unset($Detail);
				$Detail[] = $Lang['ePOS']['Name'];
				$Detail[] = $Lang['ePOS']['StandardIntakePerDay'].'('.$Lang['ePOS']['Unit'].')';
				$Detail[] = $Lang['ePOS']['ExceedNumberOfPeople'];
				$Rows[] = $Detail;
			}
			
			for ($j=0; $j< sizeof($IngredientList); $j++) {
				$ExceedList = "";
				$TotalExceed = 0;
				$ExceedLayerID = 'ExceedLimitStudentListLayer-'.$IngredientList[$j]['HealthIngredientID'].'-'.$ClassObjList[$i]->YearClassID;
				for ($k=0; $k< sizeof($ClassObjList[$i]->ClassStudentList); $k++) {
					$Various = $StudentIntakeInfo[$ClassObjList[$i]->ClassStudentList[$k]['UserID']][$IngredientList[$j]['HealthIngredientID']] - $IngredientList[$j]['StandardInTakePerDay'];
					//debug_r($ClassObjList[$i]->ClassStudentList[$k]['StudentName'].'-'.$IngredientList[$j]['HealthIngredientName'].'-'.$IngredientList[$j]['StandardInTakePerDay'].'-'.$StudentIntakeInfo[$ClassObjList[$i]->ClassStudentList[$k]['UserID']][$IngredientList[$j]['HealthIngredientID']]);
					if ($Various > 0) {
						if ($ExceedList == "") {
							$ExceedList .= '<span style="visibility: hidden; width: 280px; left: 895px; display: block;" id="'.$ExceedLayerID.'" class="member_list_group_layer">
													<h1><span style="float: left;">';
						}
						
						$ExceedList .= $ClassObjList[$i]->ClassStudentList[$k]['StudentName'].'<br>';
						$TotalExceed++;
					}
				}
				if ($ExceedList != "") {
					$ExceedList .= '</span>
													  <span style="float: right;" class="close_group_list">
														<a onclick="$(\'#'.$ExceedLayerID.'\').slideUp();return false;" title="'.$Lang['Btn']['Close'].'" href="javascript:void(0);">['.$Lang['Btn']['Close'].']</a>
													  </span>
													  </h1>
													  <p class="spacer"/>
													</span>';
				}
				
				if ($Format != "EXPORT") {
					if ($Format == "PRINT") {
						$ClassName = ' class="eSporttdborder eSportprinttext"';
					}
					
					$x .= '<tr>
									<td '.$ClassName.'>'.$IngredientList[$j]['HealthIngredientName'].'</td>
									<td '.$ClassName.'>'.$IngredientList[$j]['StandardInTakePerDay'].'('.$IngredientList[$j]['UnitName'].')</td>
									<td '.$ClassName.'>';
					if ($TotalExceed > 0 && $Format == "WEB") {
						$x .= '<a href="#" onclick="ShowPanel(this,\''.$ExceedLayerID.'\'); return false;">'.$TotalExceed.'</a>';
						$x .= $ExceedList;
					}
					else {
						$x .= $TotalExceed;
					}
					$x .= '
									</td>
								</tr>';
				}
				else { // export
					unset($Detail);
					$Detail[] = $IngredientList[$j]['HealthIngredientName'];
					$Detail[] = $IngredientList[$j]['StandardInTakePerDay'].'('.$IngredientList[$j]['UnitName'].')';
					$Detail[] = $TotalExceed;	
					$Rows[] = $Detail;					
				}
			}
			
			if($Format != "EXPORT")
			{
				$x .= '</tbody>';
				$x .= '</table>
					   <p>&nbsp;</p>';
			} 
			else { //  export
				$Rows[] = array(' ');
			}
		}
		
		if ($Format != "EXPORT") {
			return $x;
		}
		else {
			$filename = "personal_health.csv";
			$export_content = $Export->GET_EXPORT_TXT($Rows, $ExportColumn);
			$Export->EXPORT_FILE($filename, $export_content);
		}
	}
	
	function Get_Pickup_Thickbox_Content($LogID='', $ItemIDs='', $pageNo='', $PageSize='', $order='', $field=''){
		global $Lang, $PATH_WRT_ROOT;
		
		### action buttons
		$htmlAry['submitBtn'] = $this->Get_Action_Btn($Lang['Btn']['Submit'], "button", "js_Pickup();", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		$htmlAry['cancelBtn'] = $this->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		
		$html['contentTbl'] = $this->Get_Pickup_Item_Table($LogID, $ItemIDs, $pageNo, $PageSize, $order, $field);
		
		$x .='<div id="thickboxContainerDiv" class="edit_pop_board">
			<form id="thickboxForm" name="thickboxForm">
				<div id="thickboxContentDiv" class="edit_pop_board_write">
					'.$html['contentTbl'].'
				</div>
				<div id="editBottomDiv" class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$htmlAry['submitBtn'].'
					'.$htmlAry['cancelBtn'].'
					<p class="spacer"></p>
				</div>
				<input type="hidden" name="LogID" value="'.$LogID.'"/>
			</form>
		</div>';
		
		return $x;
	}
	
	function Get_Pickup_Item_Table($LogID, $ItemIDs='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		$libPOS = new libpos();
		
//		# Default Table Settings
//		$PageNumber = ($PageNumber == '')? 1 : $PageNumber;
//		$PageSize = ($PageSize == '')? 10000 : $PageSize;
//		$Order = ($Order == '')? 0 : $Order;
//		$SortField = ($SortField == '')? 0 : $SortField;
//		
//		# TABLE INFO
//		$namefield = getNameFieldByLang("iu.");
//		$li = new libdbtable2007($SortField, $Order, $PageNumber);
//		$li->field_array = array("ItemName", "ItemSubTotal", "ItemQty");
//		$li->sql = $libPOS->Get_Pickup_Item_Sql($LogID, $ItemIDs);
//		$li->no_col = sizeof($li->field_array)+1;
//		$li->title = "";
//		$li->column_array = array(0,0,0,0,0,0);
//		$li->wrap_array = array(0,0,0,0,0,0);
//		$li->IsColOff = 2;
//		$li->page_size = $PageSize;
//		$li->with_navigation = false;
//		
//		// TABLE COLUMN
//		$pos = 0;
//		$li->column_list .= "<td width='1' class='tablebluetop tableTitle'>#</td>\n";
//		$li->column_list .= "<td width='20%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['ItemName'])."</td>\n";
//		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['UnitPrice'])."</td>\n";
//		$li->column_list .= "<td width='10%' class='tablebluetop tableTitle'>".$li->column($pos++, $Lang['ePayment']['Quantity'])."</td>\n";
//		
//		$x = '';
//		$x .= $li->displayFormat2('100%', "blue")."\n";
		
		$Result = $libPOS->Get_Log_Item($LogID, $ItemIDs);
		$x .= '<table width="100%" cellspacing="0" cellpadding="4" border="0" align="center">
				<tbody><tr class="tabletop">
				<td class="tablebluetop tableTitle" width="1">#</td>
				<td class="tablebluetop tableTitle" width="40%">'.$Lang['ePayment']['ItemName'].'</td>
				<td class="tablebluetop tableTitle" width="20%">'.$Lang['ePayment']['UnitPrice'].'</td>
				<td class="tablebluetop tableTitle" width="40%">'.$Lang['ePayment']['Quantity'].'</td>
				</tr>';
				
				for ($i=0; $i< sizeof($Result);$i++) {
					$ItemQty = $Result[$i]['ItemQty'] - $Result[$i]['ReceivedItemQty'];
					$css =$i%2==0?"tablebluerow":"tablebluerow2";
					
					$numberSelection = '<select name="ItemQty['.$Result[$i]['ItemID'].']">';
					
				    for ($j = 1;$j <= $ItemQty;$j++)
				    {
				    	$selected = '';
				    	if($j == $ItemQty){
				    		$selected = 'selected';
				    	}
				        $numberSelection .=  "<option $selected>$j</option>";
				    }
				    
					$numberSelection .= '</select>';
					
					$x .= "<tr class='".$css."'>";
						$x .= "<td class='tabletext tablerow'>".($i+1)."</td>";
						$x .= "<td class='tabletext tablerow'>".$Result[$i]['ItemName']."</td>";
						$x .= "<td class='tabletext tablerow'>".$Result[$i]['ItemSubTotal']."</td>";
						$x .= "<td class='tabletext tablerow'>".$numberSelection." / ".$ItemQty." ".$Lang['ePOS']['Item']."</td>";
					$x .= "</tr>";
				}
				
				$x .= '</tbody></table>';
		
		return $x;
	}
	
	function Get_Writeoff_Thickbox_Content($LogID='', $ItemIDs='', $LogType='', $pageNo='', $PageSize='', $order='', $field='', $ActionFrom='NotTake'){
		global $Lang, $PATH_WRT_ROOT;
		
		### action buttons
		$htmlAry['submitBtn'] = $this->Get_Action_Btn($Lang['Btn']['Submit'], "button", "js_Writeoff();", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		$htmlAry['cancelBtn'] = $this->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		
		$html['contentTbl'] = $this->Get_Writeoff_Item_Table($LogID, $ItemIDs, $LogType, $pageNo, $PageSize, $order, $field);
		
		$x .='<div id="thickboxContainerDiv" class="edit_pop_board">
			<form id="thickboxForm" name="thickboxForm" method="post">
				<div id="thickboxContentDiv" class="edit_pop_board_write">
					'.$html['contentTbl'].'
				</div>
				<div id="editBottomDiv" class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$htmlAry['submitBtn'].'
					'.$htmlAry['cancelBtn'].'
					<p class="spacer"></p>
				</div>
				<input type="hidden" name="LogID" value="'.$LogID.'"/>
                <input type="hidden" name="ActionFrom" value="'.$ActionFrom.'"/>
			</form>
		</div>';
		
		return $x;
	}
	
	function Get_Writeoff_Item_Table($LogID, $ItemIDs='', $LogType='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		$libPOS = new libpos();
		
		$Result = $libPOS->Get_Log_Item($LogID, $ItemIDs);
		$x .= '<table width="100%" cellspacing="0" cellpadding="4" border="0" align="center">
				<tbody><tr class="tabletop">
				<td class="tablebluetop tableTitle" width="1">#</td>
				<td class="tablebluetop tableTitle" width="30%">'.$Lang['ePayment']['ItemName'].'</td>
				<td class="tablebluetop tableTitle" width="10%">'.$Lang['ePayment']['UnitPrice'].'</td>
				<td class="tablebluetop tableTitle" width="30%">'.$Lang['ePayment']['Quantity'].'</td>
				<td class="tablebluetop tableTitle" width="30%">'.$Lang['ePOS']['Remark'].'</td>
				</tr>';
				
				for ($i=0; $i< sizeof($Result);$i++) {
					if($LogType == 'Complete'){
						$ItemQty = $Result[$i]['ReceivedItemQty'];
					}
					else {
						$ItemQty = $Result[$i]['ItemQty'] - $Result[$i]['ReceivedItemQty'];
					}
					$css =$i%2==0?"tablebluerow":"tablebluerow2";
					
					$numberSelection = '<select name="ItemQty['.$Result[$i]['ItemID'].']">';
					
				    for ($j = 1;$j <= $ItemQty;$j++)
				    {
				    	$selected = '';
				    	if($j == $ItemQty){
				    		$selected = 'selected';
				    	}
				        $numberSelection .=  "<option $selected>$j</option>";
				    }
				    
					$numberSelection .= '</select>';
					
					$x .= "<tr class='".$css."'>";
						$x .= "<td class='tabletext tablerow'>".($i+1)."</td>";
						$x .= "<td class='tabletext tablerow'>".$Result[$i]['ItemName']."</td>";
						$x .= "<td class='tabletext tablerow'>".$Result[$i]['ItemSubTotal']."</td>";
						$x .= "<td class='tabletext tablerow'>".$numberSelection." / ".$ItemQty." ".$Lang['ePOS']['Item']."</td>";
						$x .= "<td class='tabletext tablerow'><input type='text' name='ItemRemark[".$Result[$i]['ItemID']."]'></input></td>";
					$x .= "</tr>";
				}
				
				$x .= '</tbody></table>';
		
		return $x;
	}
	
	function Get_Change_Thickbox_Content($LogID='', $ItemIDs='', $LogType='', $pageNo='', $PageSize='', $order='', $field=''){
		global $Lang, $PATH_WRT_ROOT;
		
		### action buttons
		$htmlAry['submitBtn'] = $this->Get_Action_Btn($Lang['Btn']['Submit'], "button", "js_Change();", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		$htmlAry['cancelBtn'] = $this->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		
		$html['contentTbl'] = $this->Get_Change_Item_Table($LogID, $ItemIDs, $LogType, $pageNo, $PageSize, $order, $field);
		
		$x .='<div id="thickboxContainerDiv" class="edit_pop_board">
			<form id="thickboxForm" name="thickboxForm">
				<div id="thickboxContentDiv" class="edit_pop_board_write">
					'.$html['contentTbl'].'
				</div>
				<div id="editBottomDiv" class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$htmlAry['submitBtn'].'
					'.$htmlAry['cancelBtn'].'
					<p class="spacer"></p>
				</div>
				<input type="hidden" name="LogID" value="'.$LogID.'"/>
			</form>
		</div>';
		
		return $x;
	}
	
	function Get_Change_Item_Table($LogID, $ItemIDs='', $LogType='', $PageNumber='', $PageSize='', $Order='', $SortField='')
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		$libPOS = new libpos();
		
		$Result = $libPOS->Get_Log_Item($LogID, $ItemIDs);
		$x .= '<table width="100%" cellspacing="0" cellpadding="4" border="0" align="center">
				<tbody><tr class="tabletop">
				<td class="tablebluetop tableTitle" width="1">#</td>
				<td class="tablebluetop tableTitle" width="20%">'.$Lang['ePayment']['ItemName'].'</td>
				<td class="tablebluetop tableTitle" width="10%">'.$Lang['ePayment']['UnitPrice'].'</td>
				<td class="tablebluetop tableTitle" width="50%">'.$Lang['ePOS']['SamePriceItemThatAllowToChange'].'</td>
				<td class="tablebluetop tableTitle" width="20%">'.$Lang['ePayment']['Quantity'].'</td>
				</tr>';
				
				for ($i=0; $i< sizeof($Result);$i++) {
					
					# Category Filtering
					$CategoryFilter = $this->Get_Category_Selection_By_Price($Result[$i]['ItemID'], '', $OnChange='js_Reload_Item_Selection('.$Result[$i]['ItemID'].','.$Result[$i]['ItemSubTotal'].');', $Result[$i]['ItemSubTotal'], 0, 1, 1, 0, $LogID);
										
					# Item Filtering
					$ItemFilter = $this->Get_Item_Selection_By_Price($Result[$i]['ItemID'], '', '', $Result[$i]['ItemSubTotal'], '', 0, 1, 0, 1, $LogID);
				
				
					if($LogType == 'Complete'){
						$ItemQty = $Result[$i]['ReceivedItemQty'];
					}
					else {
						$ItemQty = $Result[$i]['ItemQty'] - $Result[$i]['ReceivedItemQty'];
					}
					$css =$i%2==0?"tablebluerow":"tablebluerow2";
					
					$numberSelection = '<select name="ItemQty['.$Result[$i]['ItemID'].']">';
					
				    for ($j = 1;$j <= $ItemQty;$j++)
				    {
				    	$selected = '';
				    	if($j == $ItemQty){
				    		$selected = 'selected';
				    	}
				        $numberSelection .=  "<option $selected>$j</option>";
				    }
				    
					$numberSelection .= '</select>';
					
					$x .= "<tr class='".$css."'>";
						$x .= "<td class='tabletext tablerow'>".($i+1)."</td>";
						$x .= "<td class='tabletext tablerow'>".$Result[$i]['ItemName']."</td>";
						$x .= "<td class='tabletext tablerow'>".$Result[$i]['ItemSubTotal']."</td>";
						$x .= "<td class='tabletext tablerow'>".$CategoryFilter."<div style='display:inline' id='divItemSelection_".$Result[$i]['ItemID']."'>".$ItemFilter."</div></td>";
						$x .= "<td class='tabletext tablerow'>".$numberSelection." / ".$ItemQty." ".$Lang['ePOS']['Item']."</td>";
					$x .= "</tr>";
				}
				
				$x .= '</tbody></table>';
		
		return $x;
	}
	
	function Get_Item_Selection_By_Price($ItemID, $SelectedItemID='', $CategoryID='', $Price='', $OnChange='', $DisplayStatus=1, $ActiveOnly=0, $ShowOptionAll=0, $noFirst=1, $LogID='')
	{
		global $Lang, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		include_once($PATH_WRT_ROOT."includes/libpos_item.php");
		
		### Get ItemID that have been replaced before 
		$replacedItemIDAry = array();
		if ($LogID != '') {
		    $libPOS = new libpos();
		    $replacedItemAry = $libPOS->getReplaceItemsByTransaction($LogID);
		    if (count($replacedItemAry)) {
		        $replacedItemIDAry = Get_Array_By_Key($replacedItemAry,'ItemID');
		    }
		}
		$nrReplacedItem = count($replacedItemIDAry);
		
		### Get Item Data
		$libItem = new POS_Item();
		
		if($CategoryID > 0){
			$ItemArr = $libItem->Get_All_Items('', 1, $isAssoc=false, $inventoryOnly=true);
			$ItemArr = $ItemArr[$CategoryID];
		}
		else{
			$ItemArr = $libItem->Get_All_Items($Keyword='', $isAssoCategory=0, $isAssoc=false, $inventoryOnly=true);
		}
		
		$numOfItem = count($ItemArr);
		
		$selectArr = array();
		if ($numOfItem > 0)
		{
			foreach ($ItemArr as $Item)
			{ 
				if($Price && $Price != $Item['UnitPrice'] || $Item['ItemID'] == $ItemID){
					continue;
				}
				if ($nrReplacedItem && in_array($Item['ItemID'],$replacedItemIDAry)) {
				    continue;   // by pass item that have been replaced before
				}
				$thisItemID = $Item['ItemID'];
				$thisItemName = $Item['ItemName'];
				
				if ($DisplayStatus == 1)
				{
					$thisRecordStatus = $Item['RecordStatus'];
					if ($thisRecordStatus == 0)
						$thisItemName .= ' ('.$Lang['General']['Disabled'].')';
				}
				
				$selectArr[$thisItemID] = $thisItemName;
			}
		}
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
			
		$firstTitle = '';
		if ($ShowOptionAll == 1)
			$firstTitle = Get_Selection_First_Title($Lang['ePOS']['AllItems']);
			
		$selectionTags = ' id="ItemIDFilter['.$ItemID.']" name="ItemIDFilter['.$ItemID.']" '.$onchange;
		$ItemSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedItemID, $ShowOptionAll, $noFirst, $firstTitle);
		
		return $ItemSelection;
	}
	
	function Get_Pickup_Export_Form($FromDate, $ToDate, $field, $order, $ClassName, $keyword) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$x .= '<div class="edit_pop_board_write" style="height:374px;">
				  <form name="PickupExportForm" id="PickupExportForm" onsubmit="return false;">
			      <input type="hidden" name="FromDate" value="'.$FromDate.'" />
				  <input type="hidden" name="ToDate" value="'.$ToDate.'" />
				  <input type="hidden" name="field" value="'.$field.'" />
				  <input type="hidden" name="order" value="'.$order.'" />
				  <input type="hidden" name="ClassName" value="'.$ClassName.'" />
				  <input type="hidden" name="keyword" value="'.$keyword.'" />
				  <br><br>
			      <div>'.$Lang['ePOS']['ChooseTheOrderingToExportOrderList'].'</div>
			      <br><br>
			      <div><input type="radio" name="ExportOrderListOrder" id="ExportOrderListOrder1" value="1" checked><label for="ExportOrderListOrder1"> '.$Lang['ePOS']['OrderByInvoiceNumber'].'</label></div>
			      <br>
			      <div><input type="radio" name="ExportOrderListOrder" id="ExportOrderListOrder2" value="2"><label for="ExportOrderListOrder2"> '.$Lang['ePOS']['OrderByClassAndClassNumber'].'</label></div>
				  </from>
				</div>
				<div class="edit_bottom" style="height:10px;">
					<p class="spacer"></p><input name="submit2" type="button" class="formbutton" onclick="exportPage(document.PickupExportForm,\'export_order_list.php?FromDate='.$FromDate.'&ToDate='.$ToDate.'&field='.$field.'&order='.$order.'\')" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />
					<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
				</div>';

		return $x;
	}
	
	function Get_Pickup_Export_Content($FromDate='', $ToDate='',$ClassName='', $Keyword='', $ExportOrderListOrder='')
	{
		global $intranet_session_language,$Lang,$PATH_WRT_ROOT;
		
		$POS = new libpos();
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		
		$Result = $POS->Get_Pickup_Export_Data($FromDate, $ToDate,$ClassName, $Keyword, $ExportOrderListOrder);
		
		$UtfContent = $Lang['ePayment']['POSTransactionReport']." (".$FromDate." ".$Lang["ePOS"]["To"]." ".$ToDate.")\r\n\r\n";
		$UtfContent .= $Lang['ePOS']['ClassName']."\t".(($ClassName != "")? $ClassName:$Lang['General']['All'])."\r\n";
		$UtfContent .= $Lang['Payment']['Keyword']."\t".$Keyword."\r\n";
		$UtfContent .= "\r\n";
		
		$ExportColumn = array($Lang['ePayment']['InvoiceNumber'],
							$Lang['ePOS']['ClassName'],
							$Lang['ePOS']['ClassNumber'],
							$Lang['ePOS']['UserName'],
							$Lang['ePOS']['Category'],
							$Lang['General']['Details'],
							$Lang['ePOS']['NotTaken'],
							$Lang['ePayment']['Quantity'],
							$Lang['ePOS']['UnitPrice'],
							$Lang['ePayment']['GrandTotal'],
							$Lang['ePOS']['RefCode'],
							$Lang['ePOS']['TransactionTime']);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			unset($Detail);
			$Detail[] = $Result[$i]['InvoiceNumber'];
			$Detail[] = $Result[$i]['ClassName'];
			$Detail[] = $Result[$i]['ClassNumber'];
			$Detail[] = $Result[$i]['Name'];
			$Detail[] = $Result[$i]['CategoryName'];
			$Detail[] = str_replace(array('<br>','<br />'),', ',$Result[$i]['ItemName']);
			$Detail[] = $Result[$i]['NotTakenAmount'];
			$Detail[] = $Result[$i]['ItemQty'];
			$Detail[] = $POS->getExportAmountFormat($Result[$i]['ItemSubTotal']);
			$Detail[] = $POS->getExportAmountFormat($Result[$i]['GrandTotal']);
			$Detail[] = $Result[$i]['RefCode'];
			$Detail[] = $Result[$i]['DateInput'];
			
			$Rows[] = $Detail;
		}
		
		$ExportContent = $UtfContent.$lexport->GET_EXPORT_TXT($Rows,$ExportColumn);
		
		return $ExportContent;
	}

}
?>