<?php
// modifying :          

/*
## Change Log [Start] ###
Date: 2020-05-06 (Henry)
- added function isAlbumsReadable(), isAlbumsEditable() and isCategoryPicByAlbumIDs()
Date: 2020-05-05 (Henry)
- added function AppIsAlbumsReadable() and AppIsAlbumsEditable()
Date: 2020-04-20 (Henry)
- modified SQL in Get_Lastest_Album_Array()
Date: 2019-06-12 (Henry)
- add flag $sys_custom['DigitalChannels']['PhotoMaxSize'], $sys_custom['DigitalChannels']['ThumbnailMaxSize']
Date: 2019-04-30 (Henry)
- command injection handling
Date: 2019-04-01 (Cameron)
- add function AppUserCanUploadPhotos()
Date: 2019-03-14 (Cameron)
- retrieve ChiTitle and ChiDescription in Get_Album_Search()
Date: 2019-03-11 (Cameron)
- check if file exist before delete it in removeAlbumPhotos()
- modify removeAlbumPhoto(), check all sql result for return value
Date: 2019-03-08 (Cameron)
- add function getTargetUserName() for showing target user name in target group
- should also retrieve ChiTitle and ChiDescription in getAlbum()
- retrieve ChiDescription in getAlbumPhoto()
Date:2019-02-11 (Pun)
- modified getAlbum(), added SFOC cust
Date:2019-02-04 (Cameron)
- add function isDCAdmin(), AppIsAlbumEditable(), AppIsCategoryEditable()
Date: 2019-01-24 (Cameron)
- retrieve ChiDescription in AppGetAlbum()
Date: 2019-01-23 (Cameron)
- add parameter $currentUserID to Add_Album(), Update_Album(), updateAlbumPhoto(), updateAlbumPICs(), resetAlbumUsersGroups()
Date: 2019-01-22 (Cameron)
- add function addAlbumPhotoByApp()
Date: 2019-01-17 (Cameron)
- add parameter $fromApp to Add_Album() & Update_Album()
Date: 2019-01-14 (Tiffany)
- add parameter ParLang in AppGetAlbum(), AppGetPhoto(),AppGetRecPhoto()
Date: 2019-01-09 (Cameron)
- retrieve ChiDescription in Get_Recommend_Album_Photo(), Get_Album_Photo_Favorites(), Get_Recommend_Photo_Array_New(), Get_Most_Hit_Array(), 
    Get_Lastest_Photo_Array(), Get_Album_Photo_Search(), Get_Recommend_Photo_Array()
- Search Album Title, Album Description, Photo Description, Photo Event Title are searched based on $intranet_session_language:
    Photo Get_Album_Search(), Get_Album_Photo_Search()
Date: 2019-01-08 (Cameron)
- retrieve ChiEventTitle, ChiDescription in Get_Album_Photo()
- retrieve ChiTitle, ChiDescription, AlbumCode in Get_Album()
- add fields: ChiTitle, ChiDescription, AlbumCode in Add_Album(), Update_Album()
- add chiEventTitle in updateAlbumEvent()
- add ChiDescription, ChiEventTitle to updateAlbumPhoto()
Date: 2018-08-08 (Henry)
- fixed sql injection in getAlbumPhoto()
Date: 2018-05-30 (Tiffany)
- Modified AppGetPhoto() and AppGetRecPhoto(), add EventTitle and EventDate column.
Date: 2018-03-16 (Cameron)
- add function removeAlbumPhotos()
- add parameter $albumID to reorderAlbumPhotos()
Date: 2018-03-15 (Cameron)
- add EventTitle and EventDate range filter in Get_Album_Photo_Search() and Get_Album_Search()
Date: 2018-03-14 (Cameron)
- add function updateAlbumEvent()
Date: 2018-03-12 (Cameron)
- set EventDate to current date when Add_Album_Photo
Date: 2018-03-09 (Cameron)
- modify updateAlbumPhoto() to support update EventTitle and EventDate
- retrieve EventTitle and EventDate in Get_Album_Photo()
Date: 2017-07-11 (Henry)
- modified isAlbumReadable() to allow parent read their children album
Date: 2017-04-07 (Henry)
- modified Get_Album_Search(), Get_Album(), Get_Recommend_Album() and Get_Album_Photo_Search()
to support all academic year filtering [Case#E114934]
Date: 2017-04-05 (Pun):
- modified Get_Album_Photo(), Get_Recommend_Album_Photo(), added return Sequence field
Date: 2017-01-04 (Henry):
- support category pic
Date: 2016-12-29 (Henry)
- modified Get_Album_Search(), Get_Album(), Get_Recommend_Album(), Get_Album_Photo_Search() to filter Academic Year by DateTaken
Date: 2016-10-19 (Paul)
- modified Add_Album_Photo(), to correctly escape the special characters in file name before inserting to DB
Date: 2015-12-08 (Henry)
- modified AppGetCategory() to return Category only if contain album
Date: 2015-11-26 (Pun)
- Added getAdminUser(), isAdminUser()
Date: 2015-11-25 (Pun)
- Fixed cannot show thumbnail after uploading the wrong file type of file, then upload correct file.
Date: 2015-06-22 (Henry)
- fix the ordering of highlight photo
Date: 2015-05-06 (Henry)
- support MOV, RM, FLV video format
Date: 2015-03-17 (Henry)
- Add function AppGetCategory()
- Add function AppGetAlbum()
- Add function AppGetPhoto()
- Add function AppGetCategory()
- Add function AppGetPhotoType()
- Add function AppGetAlbumPhotoComment()
- Add function AppGetAlbumPhotoFavoritesUser()

Date: 2015-03-04 (Henry)
- Add function Remove_Recommend()

Date: 2015-02-26 (Henry)
- Add function Get_Recommend_Album()
- Add function Get_Recommend_Album_Photo()
- Add function Add_Recommend_Album()
- Add function Update_Recommend_Album()
- Add function Remove_Recommend_Album()
- Add function Add_Recommend_Album_Photo()

Date: 2015-01-27 (Henry)
- Modified Get_Album_Photo_Search() to add the original path to array and add parameter $album_id
- Modified Get_Album_Photo_Favorites() to join album table
- Modified Get_Album_Search() to allow order para
- Modified Get_Album_Photo_Favorites() to allow order/keyword para

Date: 2014-11-10 (Henry)
- Add function Get_Albums_By_Pic()

Date: 2014-10-09 (Henry)
- File Created

### Change Log [End] ####
 */

include_once ($intranet_root . "/includes/libaccessright.php");

class libdigitalchannels extends libaccessright
{

    public static $photo_urls = array(
        'photo' => "/file/album/photos/",
        'thumbnail' => "/file/album/thumbnails/",
        'original' => "/file/album/originals/"
    );

    public static $file_url = '/file/digital_channels/';

    public static $photo_max_size = "1000";

    public static $thumbnail_max_size = "184";

    public static $encrypt_key = "7bGa69zcn3S";
 // must not be changed after client using Digital channels
    public static $videoFormat = array(
        '3GP',
        'WMV',
        'MP4',
        'MOV',
        'RM',
        'FLV',
        'AVI'
    );

    public static $max_upload_size = 500;

    function libdigitalchannels($user_id = 0)
    {
        global $sys_custom;
        parent::libaccessright();
        if ($sys_custom['DigitalChannels']['NoLimitUploadSize']) {
            self::$max_upload_size = 0;
        }
        if ($sys_custom['DigitalChannels']['PhotoMaxSize']) {
            self::$photo_max_size = $sys_custom['DigitalChannels']['PhotoMaxSize'];
        }
        if ($sys_custom['DigitalChannels']['ThumbnailMaxSize']) {
            self::$thumbnail_max_size = $sys_custom['DigitalChannels']['ThumbnailMaxSize'];
        }
    }

    function getAdminUser()
    {
        global $intranet_root, $junior_mck;
        
        if ($junior_mck > 0) { // EJ
            include_once ($intranet_root . '/includes/libfilesystem.php');
            $lf = new libfilesystem();
            $filePath = $intranet_root . "/file/digital_channels/admin_user.txt";
            $AdminUser = trim($lf->file_read($filePath));
            return $AdminUser;
        }
        
        // TODO: IP version, IP does not need this function now
        return array();
    }

    function isAdminUser()
    {
        global $PATH_WRT_ROOT, $ck_super_user, $ck_super_admin_user, $junior_mck;
        global $UserID;
        
        if ($junior_mck > 0) { // EJ
            if (! isset($_SESSION['SSV_USER_ACCESS']["eAdmin-DigitalChannels"])) {
                $AdminUser = $this->getAdminUser();
                $IsAdmin = 0;
                if (! empty($AdminUser)) {
                    $AdminArray = explode(",", $AdminUser);
                    $IsAdmin = (in_array($UserID, $AdminArray)) ? 1 : 0;
                }
                $_SESSION['SSV_USER_ACCESS']["eAdmin-DigitalChannels"] = $IsAdmin || $ck_super_user || $ck_super_admin_user;
            }
        }
        
        return $_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels'];
    }

    function isCategoryPic($categoryCode = '')
    {
        global $UserID;
        if ($categoryCode) {
            $cond = " AND CategoryCode = '" . $categoryCode . "' ";
        }
        $sql = "SELECT COUNT(*)
		    FROM INTRANET_DC_CATEGORY_PIC
		    WHERE PicID = '" . $UserID . "' " . $cond;
        if (current($this->returnVector($sql))) {
            return true;
        }
        
        return false;
    }

    function isCategoryPicByAlbumID($album_id)
    {
        global $UserID;
        $sql = "SELECT COUNT(*)
		    FROM INTRANET_DC_CATEGORY_PIC p
			JOIN INTRANET_DC_ALBUM a
			ON a.CategoryCode = p.CategoryCode
		    WHERE p.PicID = '" . $UserID . "' AND a.AlbumID = '" . $album_id . "'";
        if (current($this->returnVector($sql))) {
            return true;
        }
        
        return false;
    }
    
    function isCategoryPicByAlbumIDs($albumIdArr)
    {
        global $UserID;
        
        $albumIDs = implode("','",$albumIdArr);
        $sql = "SELECT a.AlbumID as AlbumID, COUNT(*) as count
		    FROM INTRANET_DC_CATEGORY_PIC p
			JOIN INTRANET_DC_ALBUM a
			ON a.CategoryCode = p.CategoryCode
		    WHERE p.PicID = '" . $UserID . "' AND a.AlbumID IN ('" . $albumIDs . "') 
			GROUP BY a.AlbumID";
		
		$result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "AlbumID", array("AlbumID","count"), 1);
        
        $albumsReadable = array();
        foreach($albumIdArr as $albumId){
        	$albumsReadable[$albumId] = false;
            if(isset($result[$albumId]) && $result[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false){
                $albumsReadable[$albumId] = true;
            }
        }	

        return $albumsReadable;
    }

    // admin or mangement group member
    function isAlbumNewable($categoryCode = '')
    {
        if ($this->isAdminUser() || $this->isCategoryPic($categoryCode)) {
            return true;
        }
        return false;
    }

    // admin, creator and PICs
    public function isAlbumEditable($album_id = '')
    {
        global $UserID;
        if ($this->isAdminUser() || $this->isCategoryPicByAlbumID($album_id)) {
            return true;
        }
        
        $cond = '';
        if ($album_id)
            $cond = " AND AlbumID = '" . $album_id . "'";
        
        $sql = "SELECT COUNT(*)
		    FROM INTRANET_DC_ALBUM
		    WHERE InputBy = '" . $UserID . "' " . $cond;
        if (current($this->returnVector($sql))) {
            return true;
        }
        
        $sql = "SELECT COUNT(*)
		    FROM INTRANET_DC_ALBUM_PIC
		    WHERE PicID = '" . $UserID . "' " . $cond;
        if (current($this->returnVector($sql))) {
            return true;
        }
        
        return false;
    }
	
	public function isAlbumsEditable($albumIdArr)
    {
        global $UserID;
        
        $albumsReadable = array();
        
        if ($this->isAdminUser()) {
        	foreach($albumIdArr as $albumId){
	        	$albumsReadable[$albumId] = true;
	        }
            return $albumsReadable;
        }
        
        $albumsReadable = $this->isCategoryPicByAlbumIDs($albumIdArr);
        
        $albumIDs = implode("','",$albumIdArr);
        
        $cond = '';
        if ($albumIDs)
            $cond = " AND AlbumID IN ('" . $albumIDs . "')";
            
        ## digital channels input user
        $sql = "SELECT AlbumID, COUNT(*) as count
		        FROM INTRANET_DC_ALBUM
		        WHERE InputBy = '" . $UserID . "' " . $cond . " GROUP BY AlbumID";

        $result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result[$albumId]) && $result[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false){
                $albumsReadable[$albumId] = true;
            }
        }
        
        ## digital channels album pic
        $sql = "SELECT AlbumID, COUNT(*) as count
		        FROM INTRANET_DC_ALBUM_PIC
		        WHERE PicID = '" . $UserID . "' " . $cond . " GROUP BY AlbumID";

        $result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result[$albumId]) && $result[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false){
                $albumsReadable[$albumId] = true;
            }
        }
        
        return $albumsReadable;
    }

    public function isAlbumReadable($album_id)
    {
        global $UserID, $intranet_root;
        
        if ($this->isAlbumNewable($album_id) || $this->isAlbumEditable($album_id)) {
            return true;
        }
        
        $sql = "SELECT InputBy,
		IF (SharedSince <= CURDATE() OR SharedSince IS NULL OR SharedSince = 0, 1, 0),
		IF (SharedUntil >= CURDATE() OR SharedUntil IS NULL OR SharedUntil = 0, 1, 0) 
		FROM INTRANET_DC_ALBUM
		WHERE AlbumID = $album_id";
        
        $settings = current($this->returnArray($sql));
        echo mysql_error();
        
        if ($settings[0] == $this->user_id) {
            return true;
        }
        
        if (! $settings[1] || ! $settings[2]) {
            return false;
        }
        
        $sql1 = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID = $album_id AND a.RecordType = 'all'";
        
        include_once ($intranet_root . "/includes/libuser.php");
        $lu = new libuser($UserID);
        $childrenArray = $lu->getChildren();
        if (count($childrenArray) <= 0) {
            $childrenArray = array(
                0
            );
        }
        
        $sql2 = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER a
			INNER JOIN INTRANET_USERGROUP g ON a.RecordID = g.GroupID AND a.RecordType = 'group'
			WHERE a.AlbumID = $album_id AND (g.UserID = " . $UserID . " OR g.UserID IN (" . implode(',', $childrenArray) . "))";
        
        $sql3 = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID = $album_id AND (a.RecordID = " . $UserID . " OR a.RecordID IN (" . implode(',', $childrenArray) . ")) AND a.RecordType = 'user'";
        
        return current($this->returnVector($sql1)) || current($this->returnVector($sql2)) || current($this->returnVector($sql3));
    }

	public function isAlbumsReadable($albumIdArr)
    {
        global $UserID, $intranet_root;

        $albumsReadable = $this->isAlbumsEditable($albumIdArr);
        
        $albumIDs = implode("','",$albumIdArr);
        
        $sql = "SELECT AlbumID, InputBy,
		IF (SharedSince <= CURDATE() OR SharedSince IS NULL OR SharedSince = 0, 1, 0) as SharedSince,
		IF (SharedUntil >= CURDATE() OR SharedUntil IS NULL OR SharedUntil = 0, 1, 0) as SharedUntil
		FROM INTRANET_DC_ALBUM
		WHERE AlbumID IN ('".$albumIDs."') ORDER BY DateTaken desc, Title asc";
        
        $settings = $this->returnArray($sql);
        $settings = BuildMultiKeyAssoc($settings, "AlbumID", array("AlbumID","InputBy","SharedSince","SharedUntil"), 1);
        echo mysql_error();
        
        $withinSharePeriod = array();
        
        foreach($albumIdArr as $albumId){
        	$withinSharePeriod[$albumId] = true;
        	if($albumsReadable[$albumId] == false){
	            if ($settings[$albumId]['InputBy'] == $this->user_id) {
	                $albumsReadable[$albumId] = true;
	            }
		        if (! $settings[$albumId]['SharedSince'] || ! $settings[$albumId]['SharedUntil']) {
		           $albumsReadable[$albumId] = false;
		           $withinSharePeriod[$albumId] = false;
		        }
        	}
        }

		$sql1 = "SELECT  a.AlbumID as AlbumID, COUNT(*) as count
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID IN ('".$albumIDs."') AND a.RecordType = 'all'
			GROUP BY a.AlbumID";

        $result1 = $this->returnArray($sql1);
        $result1 = BuildMultiKeyAssoc($result1, "AlbumID", array("AlbumID", "count"), 1);

        foreach($albumIdArr as $albumId){
            if(isset($result1[$albumId]) && $result1[$albumId]['count'] > 0 && $albumsReadable[$albumId] == false && $withinSharePeriod[$albumId]){
                $albumsReadable[$albumId] = true;
            }
        }
        
        include_once ($intranet_root . "/includes/libuser.php");
        $lu = new libuser($UserID);
        $childrenArray = $lu->getChildren();
        if (count($childrenArray) <= 0) {
            $childrenArray = array(
                0
            );
        }
        
        $sql2 = "SELECT a.AlbumID as AlbumID, COUNT(*) as count
			FROM INTRANET_DC_ALBUM_USER a
			INNER JOIN INTRANET_USERGROUP g ON a.RecordID = g.GroupID AND a.RecordType = 'group'
			WHERE a.AlbumID IN ('".$albumIDs."') AND (g.UserID = " . $UserID . " OR g.UserID IN (" . implode(',', $childrenArray) . "))
            GROUP BY a.AlbumID";

        $result2 = $this->returnArray($sql2);
        $result2 = BuildMultiKeyAssoc($result2, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result2[$albumId]) && $result2[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false && $withinSharePeriod[$albumId]){
                $albumsReadable[$albumId] = true;
            }
        }
        
        $sql3 = "SELECT a.AlbumID as AlbumID, COUNT(*) as count
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID IN ('".$albumIDs."') AND (a.RecordID = " . $UserID . " OR a.RecordID IN (" . implode(',', $childrenArray) . ")) AND a.RecordType = 'user'
            GROUP BY a.AlbumID";
        $result3 = $this->returnArray($sql3);
        $result3 = BuildMultiKeyAssoc($result3, "AlbumID", array("AlbumID", "count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result3[$albumId]) && $result3[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false && $withinSharePeriod[$albumId]){
                $albumsReadable[$albumId] = true;
            }
        }
        
        return $albumsReadable;
    }

    function Get_Albums_By_Pic($PicID)
    {
        $sql = "SELECT 
					AlbumID  
				FROM 
					INTRANET_DC_ALBUM_PIC
				WHERE 
					PicID = '" . $PicID . "'";
        return $this->returnArray($sql);
    }

    function Get_Category($Code = '')
    {
        if ($Code != '') {
            $cond = ' AND CategoryCode="' . $Code . '"';
        }
        $sql = 'SELECT 
					*, ' . Get_Lang_Selection("DescriptionChi", "DescriptionEn") . '  as Description  
				FROM 
					INTRANET_DC_CATEGORY 
				WHERE 
					1 ' . $cond . '
				Order By
					DisplayOrder';
        
        return $this->returnArray($sql);
    }

    // DataAry: CategoryCode, DescriptionEn, DescriptionChi, DisplayOrder
    function Add_Category($dataAry = array())
    {
        global $UserID;
        
        // Get Max DisplayOrder's value and increment it by 1
        $sql = "SELECT MAX(DisplayOrder) FROM INTRANET_DC_CATEGORY";
        $ReturnMaxOrderVal = $this->returnVector($sql);
        $DisplayOrder = $ReturnMaxOrderVal[0] + 1;
        
        foreach ($dataAry as $fields[] => $values[]);
        $sql = "INSERT IGNORE INTO INTRANET_DC_CATEGORY (";
        foreach ($fields as $field)
            $sql .= $field . ", ";
        $sql .= "DisplayOrder, DateModified, LastModifiedBy) values (";
        foreach ($values as $value)
            $sql .= "'" . $value . "', ";
        $sql .= "$DisplayOrder, now(), $UserID)";
        return $this->db_db_query($sql);
    }

    // DataAry: CategoryCode, DescriptionEn, DescriptionChi, DisplayOrder
    function Update_Category($Code = '', $dataAry = array())
    {
        global $UserID;
        
        foreach ($dataAry as $fields[] => $values[]);
        
        $sql = "UPDATE INTRANET_DC_CATEGORY SET ";
        foreach ($dataAry as $field => $value) {
            $sql .= $field . "=\"" . $value . "\", ";
            
            // update the ablum cateoryCode here...
            if ($field == "CategoryCode" && $Code != $value) {
                $sql2 = "UPDATE INTRANET_DC_ALBUM SET
						CategoryCode = '" . $value . "' 
						WHERE CategoryCode = '" . $Code . "'";
                $this->db_db_query($sql2);
            }
        }
        $sql .= "DateModified=now(), LastModifiedBy=$UserID";
        $sql .= " WHERE `CategoryCode`=\"$Code\"";
        return $this->db_db_query($sql);
        // dump($sql);
        // dex(mysql_error());
    }

    function Delete_Category($Code)
    {
        $sql = "	DELETE FROM
								INTRANET_DC_CATEGORY
					WHERE
								CategoryCode = '" . $Code . "'
				";
        return $this->db_db_query($sql);
    }

    public function Update_Category_Order()
    {
        $sql = "select CategoryCode from INTRANET_DC_CATEGORY order by DisplayOrder";
        $infoAry = $this->returnVector($sql);
        $numOfInfo = count((array) $infoAry);
        
        // if ($numOfInfo > 0) {
        $success = true;
        // }
        // else {
        // $success = $this->Reorder_Display_Order('INTRANET_DC_CATEGORY', 'CategoryCode', 'DisplayOrder', $infoAry, $Starting=1);
        // }
        return $success;
    }

    function Get_Update_Display_Order_Arr($OrderText, $Separator = ",")
    {
        if ($OrderText == "")
            return false;
        $orderArr = explode($Separator, $OrderText);
        $orderArr = array_remove_empty($orderArr);
        $orderArr = Array_Trim($orderArr);
        $numOfOrder = count($orderArr);
        // display order starts from 1
        $counter = 1;
        $newOrderArr = array();
        for ($i = 0; $i < $numOfOrder; $i ++) {
            $thisID = $orderArr[$i];
            if ($thisID != '') {
                $newOrderArr[$counter] = $thisID;
                $counter ++;
            }
        }
        return $newOrderArr;
    }

    public function Update_Category_Record_DisplayOrder($DisplayOrderArr = array())
    {
        if (count($DisplayOrderArr) == 0)
            return false;
        
        $this->Start_Trans();
        for ($i = 1; $i <= sizeof($DisplayOrderArr); $i ++) {
            $thisObjectID = $DisplayOrderArr[$i];
            
            $sql = 'UPDATE INTRANET_DC_CATEGORY SET 
						DisplayOrder = \'' . $i . '\' 
					WHERE 
						CategoryCode = \'' . $thisObjectID . '\'';
            
            $Result['ReorderResult' . $i] = $this->db_db_query($sql);
        }
        
        if (in_array(false, $Result)) {
            $this->RollBack_Trans();
            return false;
        } else {
            $this->Commit_Trans();
            return true;
        }
    }

    function Is_Category_Linked_Data($CategoryIDArr)
    {
        $sql = "SELECT CategoryCode FROM INTRANET_DC_ALBUM";
        $CategoryCodeArr = $this->returnVector($sql);
        
        $numOfData = count($CategoryCodeArr);
        $returnVal = false;
        for ($i = 0; $i < $numOfData; $i ++) {
            $thisCharID = $CategoryCodeArr[$i];
            if (in_array($thisCharID, (array) $CategoryIDArr, false)) {
                $returnVal = true;
                break;
            }
        }
        
        return $returnVal;
    }

    /**
     * SELECT FROM table
     *
     * @param unknown_type $table
     *            = 'table_name';
     * @param unknown_type $fields
     *            = array( 'field1', 'field2' ); || $fields = 'Feildname'
     * @param unknown_type $condition
     *            = array( 'field1' => 'data1', 'field2' => 'data2');
     * @param unknown_type $limits
     *            array (['offset' , ] count );
     */
    function SELECTFROMTABLE($table, $fields = '*', $condition = null, $limits = null, $mode = 0, $sortBy = null, $sortOrder = 'ASC')
    {
        global $UserID;
        
        $sql = "SELECT ";
        if (is_array($fields))
            $sql .= " `" . implode("`,`", $fields) . "` ";
        else if (empty($fields) || $fields == '*')
            $sql .= " * ";
        else
            $sql .= " `{$fields}` ";
        
        $sql .= " FROM `{$table}` ";
        if (! empty($condition)) {
            foreach ($condition as $field => $value) {
                if (is_int($field)) {
                    $tmp_cond[] = $value;
                } else {
                    $tmp_cond[] = "`{$field}` = {$value}";
                }
            }
            
            $sql .= " WHERE " . implode(' AND ', $tmp_cond);
        }
        
        if (! empty($sortBy)) {
            if (! is_array($sortBy)) {
                $sortBy = array(
                    $sortBy
                );
            }
            $sql .= "ORDER BY `" . implode(',', $sortBy) . "` " . $sortOrder;
        }
        
        if (! empty($limits)) {
            if (is_array($limits))
                $sql .= " LIMIT {$limits[0]},{$limits[1]}";
            else
                $sql .= " LIMIT {$limits}";
        }
        
        $result = $this->returnArray($sql, null, $mode);
        // dump($sql);
        return $result;
    }

    function Get_Album_Search($AlbumID = '', $params = '', $SchoolYear = 0)
    {
        global $PATH_WRT_ROOT, $intranet_session_language;
        if ($params)
            extract($params);
        
        $cond = '';
        if ($AlbumID != '') {
            $cond .= ' AND  a.AlbumID ="' . $AlbumID . '"';
        }
        
        if ($SchoolYear > 0) {
            include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
            $fcm = new form_class_manage();
            $SchoolYearList = $fcm->Get_Academic_Year_List($SchoolYear);
            
            $cond .= ' AND (a.DateTaken = 0 AND a.DateInput >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND a.DateInput <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '"';
            $cond .= ' OR a.DateTaken >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND a.DateTaken <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '")';
        }
        
        if ($advance) {
            if (trim($album_title)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND a.Title like '%" . special_sql_str(trim($album_title)) . "%' ";  // because this field use htmlspecialchars when saved
                }
                else {
                    $cond .= " AND a.ChiTitle like '%" . $this->Get_Safe_Sql_Like_Query(trim($album_title)) . "%' ";
                }
            }
            
            if (trim($album_description)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND a.Description like '%" . special_sql_str(trim($album_description)) . "%' ";  // because this field use htmlspecialchars when saved
                }
                else {
                    $cond .= " AND a.ChiDescription like '%" . $this->Get_Safe_Sql_Like_Query(trim($album_description)) . "%' ";
                }
            }
            
            if (trim($photo_description)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND (p.Title like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%' OR p.Description like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%') ";
                }
                else {
                    $cond .= " AND (p.Title like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%' OR p.ChiDescription like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%') ";
                }
            }
            
            if (trim($photo_event_title)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND p.EventTitle LIKE '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_event_title)) . "%' ";
                }
                else {
                    $cond .= " AND p.ChiEventTitle LIKE '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_event_title)) . "%' ";
                }
            }
            
            if (trim($photo_event_date_from)) {
                $cond .= " AND p.EventDate >='" . trim($photo_event_date_from.' 00:00:00') . "' ";
            }
            
            if (trim($photo_event_date_to)) {
                $cond .= " AND p.EventDate <='" . trim($photo_event_date_to.' 23:59:59') . "' ";
            }

            if (trim($category_id)) {
                $cond .= " AND a.CategoryCode = '" . trim($category_id) . "' ";
            }
            
            if (trim($file_type) == 'video') {
                $cond .= " AND ((UPPER(RIGHT(p.Title,2))='RM') OR (UPPER(RIGHT(p.Title,3)) IN ('".implode("','",self::$videoFormat)."'))) ";
            }
            
        } else {
            if (trim($keyword)) {
                $cond .= " AND (";
                if ($intranet_session_language == 'en') {
                    $cond .= "a.Title like '%" . special_sql_str(trim($keyword)) . "%' ";
                    $cond .= " OR a.Description like '%" . special_sql_str(trim($keyword)) . "%' ";
                }
                else {
                    $cond .= "a.ChiTitle like '%" . $this->Get_Safe_Sql_Like_Query(trim($keyword)) . "%' ";
                    $cond .= " OR a.ChiDescription like '%" . $this->Get_Safe_Sql_Like_Query(trim($keyword)) . "%' ";
                }
                $cond .= " OR a.CategoryCode = '" . $this->Get_Safe_Sql_Query(trim($keyword)) . "') ";
            }
        }
        
        if ($orderby) {
            $order = 'a.'.$orderby;
        } else {
            $order = 'a.DateTaken desc';
        }
        
        $sql = 'SELECT 
					a.AlbumID,
					a.CategoryCode, 
					a.Title, 
					a.Description, 
					a.CoverPhotoID, 
					a.PlaceTaken,
					DATE(a.DateTaken) as DateTaken,
					DATE(a.SharedSince) as SharedSince,
					DATE(a.SharedUntil) as SharedUntil,
					a.DateInput,
					a.DateModified,
					a.InputBy,
					a.ModifyBy,
                    a.ChiTitle,
                    a.ChiDescription
				FROM 
					INTRANET_DC_ALBUM a
                INNER JOIN 
                    INTRANET_DC_ALBUM_PHOTO p ON p.AlbumID=a.AlbumID
				WHERE 
					1 ' . $cond . '
                GROUP BY a.AlbumID 
				ORDER BY ' . $order;
        
        return $this->returnArray($sql);
    }

    function Get_Album($AlbumID = '', $CategoryCode = '', $SchoolYear = 0)
    {
        global $PATH_WRT_ROOT;
        
        if ($AlbumID != '') {
            $cond .= ' AND  AlbumID ="' . $AlbumID . '"';
        }
        if ($CategoryCode != '') {
            $cond .= ' AND  CategoryCode = "' . $CategoryCode . '"';
        }
        if ($SchoolYear > 0) {
            include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
            $fcm = new form_class_manage();
            $SchoolYearList = $fcm->Get_Academic_Year_List($SchoolYear);
            
            $cond .= ' AND (DateTaken = 0 AND DateInput >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND DateInput <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '"';
            $cond .= ' OR DateTaken >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND DateTaken <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '")';
        }
        
        $sql = 'SELECT 
					AlbumID,
					CategoryCode, 
					Title, 
					Description, 
					CoverPhotoID, 
					PlaceTaken,
					DATE(DateTaken) as DateTaken,
					DATE(SharedSince) as SharedSince,
					DATE(SharedUntil) as SharedUntil,
					DateInput,
					DateModified,
					InputBy,
					ModifyBy,
                    ChiTitle,
                    ChiDescription,
                    AlbumCode 
				FROM 
					INTRANET_DC_ALBUM
				WHERE 
					1 ' . $cond . ' 
				ORDER BY DateTaken desc';
        
        return $this->returnArray($sql);
    }

    function Get_Recommend_Album($RecommendID = '', $SchoolYear = 0)
    {
        global $PATH_WRT_ROOT;
        
        if ($RecommendID != '') {
            $cond .= ' AND  RecommendID ="' . $RecommendID . '"';
        }
        if ($SchoolYear > 0) {
            include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
            $fcm = new form_class_manage();
            $SchoolYearList = $fcm->Get_Academic_Year_List($SchoolYear);
            
            $cond .= ' AND DateInput >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND DateInput <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '"';
        }
        
        $sql = 'SELECT 
					RecommendID,
					Title, 
					Description, 
					CoverPhotoID, 
					DATE(SharedSince) as SharedSince,
					DATE(SharedUntil) as SharedUntil,
					DateInput,
					DateModified,
					InputBy,
					ModifyBy 
				FROM 
					INTRANET_DC_RECOMMEND
				WHERE 
					1 ' . $cond . ' 
				ORDER BY SharedSince, SharedUntil';
        
        return $this->returnArray($sql);
    }

    function Get_Album_Photo($AlbumPhotoID = '', $AlbumID = '')
    {
        if ($AlbumID != '') {
            $cond .= ' AND  p.AlbumID ="' . $AlbumID . '"';
        }
        if ($AlbumPhotoID != '') {
            if (is_array($AlbumPhotoID)) {
                $cond .= ' AND  p.AlbumPhotoID IN ("' . implode('","', $AlbumPhotoID) . '")';
            } else {
                $cond .= ' AND  p.AlbumPhotoID = "' . $AlbumPhotoID . '"';
            }
        }
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by,
	        p.Sequence sequence,
            DATE_FORMAT(p.EventDate,'%Y-%m-%d') as event_date,
            p.EventTitle as event_title,
            p.ChiEventTitle as chi_event_title,
            p.ChiDescription as chi_description
		FROM INTRANET_DC_ALBUM_PHOTO p
		WHERE 
					1 " . $cond . " 
		ORDER BY $sort IF(Sequence IS NULL, -1, Sequence), DateInput";
        return $this->returnArray($sql);
    }

    function Get_Recommend_Album_Photo($RecommendID = '', $AlbumID = '')
    {
        if ($AlbumID != '') {
            $cond .= ' AND  p.AlbumID ="' . $AlbumID . '"';
        }
        if ($RecommendID != '') {
            $cond .= ' AND  rp.RecommendID = "' . $RecommendID . '"';
        }
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by,
	        rp.Sequence sequence,
            p.ChiDescription as chi_description
		FROM INTRANET_DC_RECOMMEND_PHOTO rp
		JOIN INTRANET_DC_ALBUM_PHOTO p
		ON rp.AlbumPhotoID = p.AlbumPhotoID
		WHERE 
					1 " . $cond . " 
		ORDER BY $sort IF(rp.Sequence IS NULL, -1, rp.Sequence), rp.DateInput";
        return $this->returnArray($sql);
    }

    function Get_Album_Photo_Favorites_Total($PhotoID)
    {
        $sql = "SELECT
				    COUNT(*) as total
				FROM 
					INTRANET_DC_ALBUM_PHOTO_VIEW v 
				WHERE
					v.PhotoID = '$PhotoID' AND v.IsFavorite = 1";
        
        $total = current($this->returnArray($sql));
        
        return $total['total'];
    }

    function Get_Album_Photo_Favorites($PhotoID = '', $params = '')
    {
        global $UserID;
        if ($params)
            extract($params);
        
        if ($PhotoID) {
            $cond = " AND v.PhotoID = '" . $PhotoID . "'";
        }
        
        if (trim($keyword)) {
            $cond .= " AND (p.Title like '%" . trim($keyword) . "%' OR p.Description like '%" . trim($keyword) . "%' ";
            $cond .= " OR a.CategoryCode = '" . trim($keyword) . "') ";
        }
        
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
			UNIX_TIMESTAMP(p.DateModified) modified_date,
		    p.InputBy input_by,
			UNIX_TIMESTAMP(a.DateInput) a_input_date,
			a.InputBy a_input_by,
            p.ChiDescription as chi_description
		FROM 
			INTRANET_DC_ALBUM_PHOTO_VIEW v 
		JOIN 
			INTRANET_DC_ALBUM_PHOTO p
		ON
			v.PhotoID = p.AlbumPhotoID AND v.IsFavorite = 1
		JOIN
			INTRANET_DC_ALBUM a
		ON
			a.AlbumID = p.AlbumID
		WHERE 
			v.UserID = '" . $UserID . "' $cond
		ORDER BY 
			v.DateModified desc";
        return $this->returnArray($sql);
    }

    function Get_Album_Photo_Search($params, $SchoolYear)
    {
        global $UserID, $PATH_WRT_ROOT, $intranet_session_language;
        extract($params);
        
        $cond = '';
        if ($SchoolYear > 0) {
            include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
            $fcm = new form_class_manage();
            $SchoolYearList = $fcm->Get_Academic_Year_List($SchoolYear);
            
            $cond .= ' AND (DateTaken = 0 AND DateInput >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND DateInput <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '"';
            $cond .= ' OR DateTaken >= "' . $SchoolYearList[0]['AcademicYearStart'] . '" AND DateTaken <= "' . $SchoolYearList[0]['AcademicYearEnd'] . '")';
        }
        
        if ($advance) {
            if (trim($album_title)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND a.Title like '%" . special_sql_str(trim($album_title)) . "%' ";  // because this field use htmlspecialchars when saved
                }
                else {
                    $cond .= " AND a.ChiTitle like '%" . $this->Get_Safe_Sql_Like_Query(trim($album_title)) . "%' ";
                }
            }
            
            if (trim($album_description)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND a.Description like '%" . special_sql_str(trim($album_description)) . "%' ";   // because this field use htmlspecialchars when saved
                }
                else {
                    $cond .= " AND a.ChiDescription like '%" . $this->Get_Safe_Sql_Like_Query(trim($album_description)) . "%' ";
                }
            }
            
            if (trim($photo_description)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND (p.Title like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%' OR p.Description like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%') ";
                }
                else {
                    $cond .= " AND (p.Title like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%' OR p.ChiDescription like '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_description)) . "%') ";
                }
            }

            if (trim($photo_event_title)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND p.EventTitle LIKE '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_event_title)) . "%' ";
                }
                else {
                    $cond .= " AND p.ChiEventTitle LIKE '%" . $this->Get_Safe_Sql_Like_Query(trim($photo_event_title)) . "%' ";
                }
            }
            
            if (trim($photo_event_date_from)) {
                $cond .= " AND p.EventDate >='" . trim($photo_event_date_from.' 00:00:00') . "' ";
            }
            
            if (trim($photo_event_date_to)) {
                $cond .= " AND p.EventDate <='" . trim($photo_event_date_to.' 23:59:59') . "' ";
            }
            
            if (trim($category_id)) {
                $cond .= " AND a.CategoryCode = '" . trim($category_id) . "' ";
            }
            
        } else {
            if (trim($keyword)) {
                if ($intranet_session_language == 'en') {
                    $cond .= " AND (p.Title like '%" . $this->Get_Safe_Sql_Like_Query(trim($keyword)) . "%' OR p.Description like '%" . $this->Get_Safe_Sql_Like_Query(trim($keyword)) . "%' ";
                }
                else {
                    $cond .= " AND (p.Title like '%" . $this->Get_Safe_Sql_Like_Query(trim($keyword)) . "%' OR p.ChiDescription like '%" . $this->Get_Safe_Sql_Like_Query(trim($keyword)) . "%' ";
                }
                $cond .= " OR a.CategoryCode = '" . $this->Get_Safe_Sql_Query(trim($keyword)) . "') ";
            }
        }
        
        if (trim($album_id)) {
            $cond .= " AND p.AlbumID = '" . trim($album_id) . "' ";
        }
        
        if ($photo_id != '') {
            if (is_array($photo_id)) {
                $cond .= ' AND  p.AlbumPhotoID IN ("' . implode('","', $photo_id) . '") ';
            } else {
                $cond .= ' AND  p.AlbumPhotoID = "' . $photo_id . '" ';
            }
        }
        
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
			UNIX_TIMESTAMP(p.DateModified) modified_date,
		    p.InputBy input_by,
			UNIX_TIMESTAMP(a.DateInput) a_input_date,
			a.InputBy a_input_by,
            p.ChiDescription as chi_description
		FROM 
			INTRANET_DC_ALBUM_PHOTO p
		JOIN
			INTRANET_DC_ALBUM a
		ON
			a.AlbumID = p.AlbumID
		WHERE 
			1 $cond
		ORDER BY 
			$sort IF(Sequence IS NULL, -1, Sequence), p.DateInput";
        $allPhoto = $this->returnArray($sql);

        $tempArr = array();
        foreach ($allPhoto as $aMostHit) {
            
            $photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['album_id'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            $filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
            $originalFilePath = self::getPhotoFileName('photo', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            $aMostHit['OriginalFilePath'] = $originalFilePath;
            $aMostHit['type'] = ($isVideo ? 'Video' : 'Photo');
            
            if ($file_type == 'video' && $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($file_type == 'photo' && ! $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if (! $file_type && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            }
        }
        
        return $tempArr;
    }

    function Get_Album_Photo_Favorites_User($PhotoID = '', $user_id = '')
    {
        global $UserID, $junior_mck;
        
        if ($PhotoID) {
            $cond = " AND v.PhotoID = '" . $PhotoID . "'";
        }
        
        if ($user_id) {
            $cond .= " AND v.UserID = '" . $user_id . "'";
        }
        
        $sql = "SELECT
		    v.PhotoID id,
		    " . getNameFieldWithClassNumberByLang('u.') . " as UserName
		FROM 
			INTRANET_DC_ALBUM_PHOTO_VIEW v 
		JOIN 
			INTRANET_USER u
		ON		
			v.UserID = u.UserID AND v.IsFavorite = 1
		WHERE 
			1 $cond
		ORDER BY 
			v.DateModified desc";
        $rs = $this->returnArray($sql);
        
        if ($junior_mck > 0) { // EJ
            foreach ($rs as $i => $r) {
                $rs[$i]['UserName'] = convert2unicode($r['UserName']);
            }
        }
        
        return $rs;
    }

    function Update_Album_Photo_Favorites($PhotoID)
    {
        global $UserID;
        
        $this->Add_Album_Photo_View($PhotoID);
        
        $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO_VIEW SET IsFavorite = IF(IsFavorite,'0','1'), DateModified = NOW() WHERE PhotoID = '$PhotoID' AND UserID = '$UserID'";
        return $this->db_db_query($sql);
    }

    function Get_Album_Photo_View_Total($PhotoID)
    {
        $sql = "SELECT
				    COUNT(*) as total
				FROM 
					INTRANET_DC_ALBUM_PHOTO_VIEW v 
				WHERE
					v.PhotoID = '$PhotoID'";
        
        $total = current($this->returnArray($sql));
        
        return $total['total'];
    }

    function Get_Album_Photo_View($PhotoID = '')
    {
        global $UserID;
        
        if ($PhotoID != '') {
            $cond = " AND v.PhotoID = '" . $PhotoID . "' ";
        }
        
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by
		FROM 
			INTRANET_DC_ALBUM_PHOTO_VIEW v 
		JOIN 
			INTRANET_DC_ALBUM_PHOTO p
		ON
			v.PhotoID = p.AlbumPhotoID
		WHERE 
			v.UserID = '" . $UserID . "' " . $cond . "
		ORDER BY 
			$sort IF(Sequence IS NULL, -1, Sequence), DateInput";
        return $this->returnArray($sql);
    }

    function Add_Album_Photo_View($PhotoID)
    {
        global $UserID;
        if (count($this->Get_Album_Photo_View($PhotoID)) > 0) {
            return true;
        }
        $sql = "INSERT INTO INTRANET_DC_ALBUM_PHOTO_VIEW (UserID, PhotoID, DateModified)
			VALUES ('$UserID','$PhotoID',NOW())";
        
        return $this->db_db_query($sql);
    }

    function Get_Most_Hit_Array($Type = '')
    {
        global $UserID;
        
        $sql = "SELECT 
					p.AlbumID as album_id,
				    p.AlbumPhotoID id,
				    p.Title title,
					p.Description description,
				    p.Size as size,
				    UNIX_TIMESTAMP(p.DateTaken) date_taken,
				    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
				    UNIX_TIMESTAMP(p.DateInput) input_date,
				    p.InputBy input_by,
					UNIX_TIMESTAMP(a.DateInput) a_input_date,
				    a.InputBy a_input_by,
					COUNT(v.PhotoID) as TotalHit,
                    p.ChiDescription as chi_description 
				FROM 
					INTRANET_DC_ALBUM_PHOTO p
				LEFT JOIN 
					INTRANET_DC_ALBUM_PHOTO_VIEW v 
				ON
					v.PhotoID = p.AlbumPhotoID 
				JOIN
					INTRANET_DC_ALBUM a
				ON
					a.AlbumID = p.AlbumID
				GROUP BY 
					p.AlbumPhotoID
				ORDER BY 
					TotalHit desc, 
					IF(p.Sequence IS NULL, -1, p.Sequence)";
        
        $allMostHit = $this->returnArray($sql);
        
        $tempArr = array();
        foreach ($allMostHit as $aMostHit) {
            
            if (count($tempArr) >= 100) {
                break;
            }
            
            $photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['album_id'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            $filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            
            if ($Type == 'Video' && $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($Type == 'Photo' && ! $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($Type == '' && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            }
        }
        
        return $tempArr;
    }

    function Get_Lastest_Photo_Array($Type = '')
    {
        global $UserID;
        $sql = "SELECT 
					p.AlbumID as album_id,
				    p.AlbumPhotoID id,
				    p.Title title,
				    p.Description description,
				    p.Size as size,
				    UNIX_TIMESTAMP(p.DateTaken) date_taken,
				    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
				    UNIX_TIMESTAMP(p.DateInput) input_date,
				    p.InputBy input_by,
					UNIX_TIMESTAMP(a.DateInput) a_input_date,
				    a.InputBy a_input_by,
                    p.ChiDescription as chi_description
				FROM 
					INTRANET_DC_ALBUM_PHOTO p
				JOIN
					INTRANET_DC_ALBUM a
				ON
					a.AlbumID = p.AlbumID
				ORDER BY 
					p.DateInput desc, 
					IF(p.Sequence IS NULL, -1, p.Sequence) LIMIT 100";
        
        $allLastestPhoto = $this->returnArray($sql);
        
        $tempArr = array();
        foreach ($allLastestPhoto as $aMostHit) {
            
            $photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['album_id'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            $filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            $aMostHit['type'] = ($isVideo ? 'Video' : 'Photo');
            
            if ($Type == 'Video' && $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($Type == 'Photo' && ! $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($Type == '' && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            }
        }
        
        return $tempArr;
    }

    function Get_Recommend_Photo_Array($Type = '')
    {
        global $UserID;
        
        $sql = "SELECT 
					p.AlbumID as album_id,
				    p.AlbumPhotoID id,
				    p.Title title,
					p.Description description,
				    p.Size as size,
				    UNIX_TIMESTAMP(p.DateTaken) date_taken,
				    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
				    UNIX_TIMESTAMP(p.DateInput) input_date,
				    p.InputBy input_by,
					UNIX_TIMESTAMP(a.DateInput) a_input_date,
				    a.InputBy a_input_by,
					COUNT(v.PhotoID) as TotalHit,
					IF(SUM(v.IsFavorite) IS NULL, 0, SUM(v.IsFavorite)) as TotalFav,
                    p.ChiDescription as chi_description
				FROM 
					INTRANET_DC_ALBUM_PHOTO p
				LEFT JOIN 
					INTRANET_DC_ALBUM_PHOTO_VIEW v 
				ON
					v.PhotoID = p.AlbumPhotoID
				JOIN
					INTRANET_DC_ALBUM a
				ON
					a.AlbumID = p.AlbumID
				GROUP BY 
					p.AlbumPhotoID
				ORDER BY 
					TotalFav desc, 
					IF(p.Sequence IS NULL, -1, p.Sequence) LIMIT 100";
        
        $allMostHit = $this->returnArray($sql);
        
        $tempArr = array();
        foreach ($allMostHit as $aMostHit) {
            
            $photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['album_id'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            $filePath = self::getPhotoFileName('photo', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            $aMostHit['type'] = ($isVideo ? 'Video' : 'Photo');
            
            if ($Type == 'Video' && $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($Type == 'Photo' && ! $isVideo && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            } else if ($Type == '' && $this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            }
        }
        
        return $tempArr;
    }

    function Get_Recommend_Photo_Array_New($Type = '')
    {
        global $UserID;
        
        $sql = "SELECT 
					AlbumPhotoID RecPhotoID
				FROM 
					INTRANET_DC_RECOMMEND_PHOTO p 
				JOIN 
					INTRANET_DC_RECOMMEND r 
				ON 
					p.RecommendID = r.RecommendID 
				WHERE 
					SharedSince <= CURDATE() AND SharedUntil >= CURDATE()";
        
        $recommendPhotoID = $this->returnVector($sql);
        
        $sql = "SELECT 
					p.AlbumID as album_id,
				    p.AlbumPhotoID id,
				    p.Title title,
					p.Description description,
				    p.Size as size,
				    UNIX_TIMESTAMP(p.DateTaken) date_taken,
				    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
				    UNIX_TIMESTAMP(p.DateInput) input_date,
				    p.InputBy input_by,
					UNIX_TIMESTAMP(a.DateInput) a_input_date,
				    a.InputBy a_input_by,
					COUNT(v.PhotoID) as TotalHit,
					IF(SUM(v.IsFavorite) IS NULL, 0, SUM(v.IsFavorite)) as TotalFav,
                    p.ChiDescription as chi_description
				FROM 
					INTRANET_DC_ALBUM_PHOTO p
				LEFT JOIN 
					INTRANET_DC_ALBUM_PHOTO_VIEW v 
				ON
					v.PhotoID = p.AlbumPhotoID
				JOIN
					INTRANET_DC_ALBUM a
				ON
					a.AlbumID = p.AlbumID
				LEFT JOIN
					INTRANET_DC_RECOMMEND_PHOTO r
				ON r.AlbumPhotoID = p.AlbumPhotoID
				WHERE
					 p.AlbumPhotoID IN ('" . implode("','", $recommendPhotoID) . "')
				GROUP BY 
					p.AlbumPhotoID
				ORDER BY  
					IF(r.Sequence IS NULL, -1, r.Sequence)";
        
        $allMostHit = $this->returnArray($sql);
        
        $tempArr = array();
        foreach ($allMostHit as $aMostHit) {
            
            $photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['album_id'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            $filePath = self::getPhotoFileName('photo', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            $aMostHit['type'] = ($isVideo ? 'Video' : 'Photo');
            
            if ($Type == 'Video' && $isVideo/* && $this->isAlbumReadable($aMostHit['album_id'])*/){
                $tempArr[] = $aMostHit;
            } else if ($Type == 'Photo' && ! $isVideo/* && $this->isAlbumReadable($aMostHit['album_id'])*/){
                $tempArr[] = $aMostHit;
            } else if ($Type == ''/* && $this->isAlbumReadable($aMostHit['album_id'])*/){
                $tempArr[] = $aMostHit;
            }
        }
        
        return $tempArr;
    }

    function Get_Recommend_Id_By_Photo_Id($PhotoID)
    {
        $sql = "SELECT 
					RecommendID
				FROM 
					INTRANET_DC_RECOMMEND_PHOTO 
				WHERE 
					AlbumPhotoID = '" . $PhotoID . "'";
        
        return current($this->returnVector($sql));
    }

    function Get_Lastest_Album_Array()
    {
        global $UserID;
        $sql = "SELECT 
					p.AlbumID as album_id,
				    p.AlbumPhotoID id,
				    a.Title title,
					a.ChiTitle ChiTitle,
				    p.Description description,
				    p.Size as size,
					DATE(a.DateTaken) as DateTaken,
				    UNIX_TIMESTAMP(a.DateTaken) date_taken,
				    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
				    UNIX_TIMESTAMP(p.DateInput) input_date,
				    p.InputBy input_by,
					UNIX_TIMESTAMP(a.DateInput) a_input_date,
				    a.InputBy a_input_by,
					a.CoverPhotoID
				FROM 
					INTRANET_DC_ALBUM_PHOTO p
				JOIN
					INTRANET_DC_ALBUM a
				ON
					a.AlbumID = p.AlbumID
				WHERE
					a.CoverPhotoID IS NULL AND p.Sequence = 0 OR p.AlbumPhotoID = a.CoverPhotoID
				GROUP BY 
					a.AlbumID
				ORDER BY 
					date_taken desc, a.DateInput desc, 
					IF(p.Sequence IS NULL, -1, p.Sequence) LIMIT 100";
        
        $allLastestPhoto = $this->returnArray($sql);
        
        $tempArr = array();
        foreach ($allLastestPhoto as $aMostHit) {
            
            $photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['album_id'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            $filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            
            if ($this->isAlbumReadable($aMostHit['album_id'])) {
                $tempArr[] = $aMostHit;
            }
        }
        
        return $tempArr;
        // global $UserID;
        // $sql = "SELECT
        // AlbumID,
        // CategoryCode,
        // Title,
        // Description,
        // CoverPhotoID,
        // PlaceTaken,
        // DATE(DateTaken) as DateTaken,
        // DATE(SharedSince) as SharedSince,
        // DATE(SharedUntil) as SharedUntil,
        // DateInput,
        // DateModified,
        // InputBy,
        // ModifyBy
        // FROM
        // INTRANET_DC_ALBUM
        // ORDER BY
        // DateTaken desc";
        //
        // $allLastestAlbum = $this->returnArray($sql);
        //
        // $tempArr = array();
        // foreach($allLastestAlbum as $aMostHit){
        // if($this->isAlbumReadable($aMostHit['AlbumID'])){
        // $tempArr[] = $aMostHit;
        // }
        // }
        //
        // return $tempArr;
    }
    
    function Add_Album($params, $fromApp=false, $currentUserID='')
    {
        global $UserID, $sys_custom;
        extract($params);
        $inputBy = empty($currentUserID) ? $UserID : $currentUserID;
        
        if ((($sys_custom['DigitalChannels']['showEventTitle']) || ($sys_custom['DigitalChannels']['showEventDate'])) && ($fromApp == false)){
            $extraFields = ", AlbumCode";
            $extraValues = ",'".$this->Get_Safe_Sql_Query($albumCode)."'";
        }
        else {
            $extraFields = "";
            $extraValues = "";
        }
        $sql = "INSERT INTO INTRANET_DC_ALBUM (CategoryCode, Title, Description, PlaceTaken, DateTaken, SharedSince, SharedUntil, InputBy, DateInput, ChiTitle, ChiDescription".$extraFields.")
        VALUES ('$CategoryCode','$title','$description','$place','$date','$since','$until','" . $inputBy. "',now(),'".$this->Get_Safe_Sql_Query($chiTitle).
                "','".$this->Get_Safe_Sql_Query($chiDescription)."'".$extraValues.")";
        
        $this->db_db_query($sql);
        $this->album_id = $this->db_insert_id();
        
        return $this->album_id;
    }

    function Update_Album($params, $fromApp=false, $currentUserID='')
    {
        global $UserID, $sys_custom;
        extract($params);
        $modifiedBy = empty($currentUserID) ? $UserID : $currentUserID;
        
        if ((($sys_custom['DigitalChannels']['showEventTitle']) || ($sys_custom['DigitalChannels']['showEventDate'])) && ($fromApp == false)){
            $extraFields = ", AlbumCode = '".$this->Get_Safe_Sql_Query($albumCode)."'";
        }
        else {
            $extraFields = "";
        }
        
        $sql = "UPDATE INTRANET_DC_ALBUM SET
				CategoryCode = '$CategoryCode',
			    Title = '$title',
			    Description = '$description',
			    PlaceTaken = '$place',
			    DateTaken = '$date',
			    SharedSince = '$since',
			    SharedUntil = '$until',
			    ModifyBy = '" . $modifiedBy. "',
				DateModified = now(),
                ChiTitle = '".$this->Get_Safe_Sql_Query($chiTitle)."',
                ChiDescription = '".$this->Get_Safe_Sql_Query($chiDescription)."'".
                $extraFields."
			WHERE AlbumID = '" . $album_id . "'";
        
        return $this->db_db_query($sql);
    }

    function Remove_Album($AlbumID)
    {
        global $file_path, $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
        $lfs = new libfilesystem();
        
        $album = $this->getAlbum($AlbumID);
        $photos = $this->getAlbumPhotos($AlbumID);
        foreach ($photos as $photo) {
            // unlink($file_path.self::getPhotoFileName('photo', $album, $photo));
            // unlink($file_path.self::getPhotoFileName('thumbnail', $album, $photo));
            // unlink($file_path.self::getPhotoFileName('original', $album, $photo));
        }
        $lfs->folder_remove_recursive($file_path . self::$file_url . date('Y', $album['input_date']) . '/' . $album['id']);
        // $lfs -> folder_remove_recursive($file_path.self::$file_url.date('Y',$album['input_date']).'/'.$album['id']);
        // $lfs -> folder_remove_recursive($file_path.self::$file_url.date('Y',$album['input_date']).'/'.$album['id']);
        // rmdir($file_path.self::getAlbumFileName('photo', $album));
        // rmdir($file_path.self::getAlbumFileName('thumbnail', $album));
        // rmdir($file_path.self::getAlbumFileName('original', $album));
        
        $sql = "DELETE a,p,u
			FROM INTRANET_DC_ALBUM a
			LEFT JOIN INTRANET_DC_ALBUM_PHOTO p ON a.AlbumID = p.AlbumID
			LEFT JOIN INTRANET_DC_ALBUM_USER u ON a.AlbumID = u.AlbumID
			WHERE a.AlbumID = '" . $AlbumID . "'";
        
        return $this->db_db_query($sql);
    }

    function Remove_Recommend($RecommendID)
    {
        $sql = "DELETE a,p 
			FROM INTRANET_DC_RECOMMEND a
			LEFT JOIN INTRANET_DC_RECOMMEND_PHOTO p ON a.RecommendID = p.RecommendID 
			WHERE a.RecommendID = '" . $RecommendID . "'";
        
        return $this->db_db_query($sql);
    }

    public function Add_Album_Photo($params = array())
    {
        global $UserID;
        extract($params);
        $sql = "SELECT MAX(Sequence)+1 FROM INTRANET_DC_ALBUM_PHOTO WHERE AlbumID='" . $album_id . "' GROUP BY AlbumID";
        $sequence = (int) current($this->returnVector($sql));
        
        $title = addslashes(stripslashes($title));
        
        $sql = "INSERT INTO INTRANET_DC_ALBUM_PHOTO (AlbumID, Title, Size, Sequence, DateTaken, InputBy, DateInput, EventDate)
		VALUES ('$album_id','$title', '$size', '$sequence', FROM_UNIXTIME('$date')," . $UserID . ",now(), CURDATE())";
        
        $this->db_db_query($sql);
        
        return $this->db_insert_id();
    }

    function Add_Recommend_Album($params)
    {
        global $UserID;
        extract($params);
        $sql = "INSERT INTO INTRANET_DC_RECOMMEND (Title, Description, SharedSince, SharedUntil, InputBy, DateInput)
			VALUES ('$title','$description','$since','$until'," . $UserID . ",now())";
        $this->db_db_query($sql);
        $this->album_id = $this->db_insert_id();
        
        if ($photo_ids) {
            $recordID = mysql_insert_id();
            for ($i = 0; $i < count($photo_ids); $i ++) {
                if ($photo_ids[$i] > 0) {
                    $sql2 = "INSERT INTO INTRANET_DC_RECOMMEND_PHOTO (RecommendID, AlbumPhotoID, Sequence, InputBy, DateInput)
					VALUES ('" . $recordID . "','" . $photo_ids[$i] . "','" . ($i + 1) . "'," . $UserID . ",now())";
                    $this->db_db_query($sql2);
                }
            }
        }
        
        return $this->album_id;
    }

    function Update_Recommend_Album($params)
    {
        global $UserID;
        extract($params);
        
        $sql = "UPDATE INTRANET_DC_RECOMMEND SET
			    Title = '$title',
			    Description = '$description',
			    SharedSince = '$since',
			    SharedUntil = '$until',
			    ModifyBy = '" . $UserID . "',
				DateModified = now()
			WHERE RecommendID = '" . $recommend_id . "'";
        
        if ($NeedUpdatePhoto) {
            $sql2 = "DELETE FROM INTRANET_DC_RECOMMEND_PHOTO WHERE RecommendID = '" . $recommend_id . "'";
            $this->db_db_query($sql2);
            if ($photo_ids) {
                for ($i = 0; $i < count($photo_ids); $i ++) {
                    if ($photo_ids[$i] > 0) {
                        $sql2 = "INSERT INTO INTRANET_DC_RECOMMEND_PHOTO (RecommendID, AlbumPhotoID, Sequence, InputBy, DateInput)
						VALUES ('" . $recommend_id . "','" . $photo_ids[$i] . "','" . ($i + 1) . "'," . $UserID . ",now())";
                        $this->db_db_query($sql2);
                    }
                }
            }
        }
        return $this->db_db_query($sql);
    }

    function Remove_Recommend_Album($AlbumID)
    {
        global $file_path, $PATH_WRT_ROOT;
        
        $sql = "DELETE a,p
			FROM INTRANET_DC_RECOMMEND a
			LEFT JOIN INTRANET_DC_RECOMMEND_PHOTO p ON a.RecommendID = p.RecommendID
			WHERE a.AlbumID = '" . $AlbumID . "'";
        
        return $this->db_db_query($sql);
    }

    public function Add_Recommend_Album_Photo($params = array())
    {
        global $UserID;
        extract($params);
        $sql = "SELECT MAX(Sequence)+1 FROM INTRANET_DC_RECOMMEND_PHOTO WHERE RecommendID='" . $recommend_id . "' GROUP BY RecommendID";
        $sequence = (int) current($this->returnVector($sql));
        
        $sql = "INSERT INTO INTRANET_DC_RECOMMEND_PHOTO (RecommendID, AlbumPhotoID, Sequence, InputBy, DateInput)
		VALUES ('$recommend_id','$album_photo_id', '$sequence'," . $UserID . ",now())";
        
        $this->db_db_query($sql);
        
        return $this->db_insert_id();
    }

    function Get_Album_Photo_Comment_Total($PhotoID)
    {
        $sql = "SELECT
				    COUNT(*) as total
				FROM 
					INTRANET_DC_ALBUM_PHOTO_COMMENT
				WHERE
					PhotoID = '$PhotoID'";
        
        $total = current($this->returnArray($sql));
        
        return $total['total'];
    }

    public function Get_Album_Photo_Comment($PhotoID)
    {
        global $junior_mck;
        if ($junior_mck > 0) { // EJ
            $photoSQL = 'u.PhotoLink as PersonalPhotoLink';
        } else {
            $photoSQL = 'u.PersonalPhotoLink';
        }
        $sql = "SELECT 
					c.RecordID,
					c.PhotoID, 
					c.UserID, 
					c.Content, 
					c.InputBy, 
					c.DateInput,
					c.ModifyBy,
					c.DateModified,
					{$photoSQL},
					" . getNameFieldWithClassNumberByLang('u.') . " as UserName
				FROM 
					INTRANET_DC_ALBUM_PHOTO_COMMENT c
				JOIN
					INTRANET_USER u
				ON
					c.UserID = u.UserID
				WHERE
					PhotoID = '" . $PhotoID . "' 
				ORDER BY 
					DateInput";
        $rs = $this->returnArray($sql);
        
        if ($junior_mck > 0) { // EJ
            foreach ($rs as $i => $r) {
                $rs[$i]['UserName'] = convert2unicode($r['UserName']);
            }
        }
        return $rs;
    }

    public function Add_Album_Photo_Comment($PhotoID, $CommentText)
    {
        global $UserID;
        
        $sql = "INSERT INTO INTRANET_DC_ALBUM_PHOTO_COMMENT (PhotoID, UserID, Content, InputBy, DateInput)
				VALUES ('$PhotoID', '$UserID', '$CommentText', '$UserID', now())";
        
        return $this->db_db_query($sql);
    }

    public function Update_Album_Photo_Comment($RecordID, $CommentText)
    {
        global $UserID;
        
        $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO_COMMENT SET
				Content = '$CommentText',
			    ModifyBy = '$UserID',
			    DateModified = now()
			WHERE RecordID = '" . $RecordID . "'";
        
        return $this->db_db_query($sql);
    }

    public function Delete_Album_Photo_Comment($RecordID)
    {
        global $UserID;
        
        $sql = "DELETE FROM INTRANET_DC_ALBUM_PHOTO_COMMENT WHERE RecordID = '" . $RecordID . "'";
        return $this->db_db_query($sql);
    }

    public static function getPhotoFileName($type, $album, $photo, $file_ext = '')
    {
        global $file_path;
        
        // Photo & Tumbnail
        if ($file_ext == '') {
            $file_ext = 'jpg';
        }
        
        // Video & Tumbnail
        // if the file is video get the original path for checking
        $o_ext = pathinfo($photo['title'], PATHINFO_EXTENSION);
        if (in_array(strtoupper($o_ext), self::$videoFormat)) {
            $file_ext = $o_ext;
            // Video
            if ($type == 'photo') {
                $file_ext = 'mp4';
            }
            // Thumbnail
            if ($type == 'thumbnail') {
                // Check thumbnail exist or not
                $targetThumbnail = self::getAlbumFileName($type, $album) . '/' . base64_encode($photo['id'] . '_' . $photo['input_date'] . '_' . $photo['input_by']) . '.jpg';
                if (! file_exists($file_path . $targetThumbnail)) {
                    // Return default thumbnail
                    return '/images/2009a/icon_files/avi_l.png';
                }
                $file_ext = 'jpg';
            }
        }
        
        // Original
        if ($type == 'original') {
            $file_ext = pathinfo($photo['title'], PATHINFO_EXTENSION);
        }
        
        return self::getAlbumFileName($type, $album) . '/' . base64_encode($photo['id'] . '_' . $photo['input_date'] . '_' . $photo['input_by']) . '.' . $file_ext;
    }

    public static function getAlbumFileName($type, $album)
    {
//        debug_pr(self::$file_url . date('Y', $album['input_date']) . '/' . $album['id'] . '/' . $type . '/' . base64_encode($album['id'] . '_' . $album['input_date'] . '_' . $album['input_by']));
        // return self::$photo_urls[$type].base64_encode($album['id'].'_'.$album['input_date'].'_'.$album['input_by']);
        return self::$file_url . date('Y', $album['input_date']) . '/' . $album['id'] . '/' . $type . '/' . base64_encode($album['id'] . '_' . $album['input_date'] . '_' . $album['input_by']);
    }

    public static function getResizedImage($image, $sw, $sh, $t)
    {
        $sr = $sw / $sh;
        
        // if ($sw <= $t && $sh <= $t) {
        // $rw = $sw;
        // $rh = $sh;
        // } else if ($sr < 1) {
        // $rw = floor($t * $sr);
        // $rh = $t;
        // } else {
        // $rw = $t;
        // $rh = floor($t / $sr);
        // }
        
        if ($sw <= $t && $sh <= $t) {
            $rw = $sw;
            $rh = $sh;
        } else if ($sr < 1) {
            $rw = $t;
            $rh = floor($t / $sr);
        } else {
            $rw = floor($t * $sr);
            $rh = $t;
        }
        
        $rimage = imagecreatetruecolor($rw, $rh);
        
        imagecopyresampled($rimage, $image, 0, 0, 0, 0, $rw, $rh, $sw, $sh);
        return $rimage;
    }

    public static function getVideoDetails($video_url)
    {
        $cmd_details = OsCommandSafe("/usr/local/bin/ffmpeg -i '" . $video_url . "' -vstats 2>&1");
        $details = shell_exec($cmd_details);
        
        $video_size = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
        if (preg_match($video_size, $details, $regs)) {
            $codec = $regs[1] ? $regs[1] : null;
            $width = $regs[3] ? $regs[3] : null;
            $height = $regs[4] ? $regs[4] : null;
            // $ratio = $width / $height;
        }
        
        $video_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
        if (preg_match($video_duration, $details, $regs)) {
            $hours = $regs[1] ? $regs[1] : 0;
            $mins = $regs[2] ? $regs[2] : 0;
            $secs = $regs[3] ? $regs[3] : 0;
            // $ms = $regs [4] ? $regs [4] : null;
            $total_length = ($hours * 3600) + ($mins * 60) + $secs;
        }
        
        return array(
            'Codec' => $codec,
            'Width' => $width,
            'Height' => $height,
            'Hours' => $hours,
            'Mins' => $mins,
            'Secs' => $secs,
            'Total_s' => $total_length
        );
    }

    public function Get_Category_Pic($CategoryCode)
    {
        global $Lang, $junior_mck;
        
        $NameField = Get_Lang_Selection("ChineseName", "EnglishName");
        $sql = 'Select
				u.UserID,						
				u.' . $NameField . ' as PicName
				
			From
				INTRANET_DC_CATEGORY_PIC gu
			INNER JOIN
			    INTRANET_USER u
			ON
			    u.UserID = gu.PicID
			WHERE 
				CategoryCode = \'' . $CategoryCode . '\'
			AND
				u.RecordStatus = 1	
			ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
			';
        // debug_r($sql);
        $rs = $this->returnArray($sql);
        
        if ($junior_mck > 0) { // EJ
            foreach ($rs as $i => $r) {
                $rs[$i]['PicName'] = convert2unicode($r['PicName']);
            }
        }
        
        return $rs;
    }

    function Get_Avaliable_User_List($AddUserID, $IdentityType, $YearClassSelect, $ParentStudentID)
    {
        global $Lang, $junior_mck;
        if (! empty($YearClassSelect))
            $YearClassSelect_cond = " and u.`ClassName` = '{$YearClassSelect}'";
        
        switch ($IdentityType) {
            case "Teaching":
                $Condition = ' u.RecordStatus=\'1\' and u.RecordType = \'' . USERTYPE_STAFF . '\' and u.Teaching = 1 ';
                break;
            case "NonTeaching":
                $Condition = ' u.RecordStatus=\'1\' and u.RecordType = \'' . USERTYPE_STAFF . '\' and u.Teaching <> 1 ';
                break;
            default:
                break;
        }
        $NameField = 'u.' . Get_Lang_Selection("ChineseName", "EnglishName");
        if (sizeof($AddUserID) > 0) {
            // var_dump($AddUserID); die;
            $UserQuery = 'and u.UserID not in (' . implode(',', $AddUserID) . ') ';
        }
        $sql = 'select
			  u.`UserID`,
			  ' . $NameField . ' as Name
			From
			    `INTRANET_USER` as u
			WHERE
			    ' . $Condition . '
			    ' . $UserQuery . '
			    AND u.`RecordStatus` = \'1\'
			    ' . $YearClassSelect_cond . '
			ORDER BY u.`ClassLevel`, u.`ClassName`, u.`ClassNumber`, u.`EnglishName` ';
        
        $rs = $this->returnArray($sql);
        
        if ($junior_mck > 0) { // EJ
            foreach ($rs as $i => $r) {
                $rs[$i]['Name'] = convert2unicode($r['Name']);
            }
        }
        
        return $rs;
    }

    function Search_User($SearchValue, $AddUserID)
    {
        global $junior_mck;
        
        $CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
        $CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
        $NameField = GET_LANG_SELECTION('u.`ChineseName`', 'u.`EnglishName`');
        $UserCondition = (trim($AddUserID) == "") ? "" : ' and u.UserID not in (' . $AddUserID . ') ';
        if ($junior_mck > 0) {
            $SearchValue = convert2Big5($SearchValue);
        }
        $SearchValue = $this->Get_Safe_Sql_Query($SearchValue);
        
        $this->With_Nolock_Trans();
        $sql = 'select
              u.UserID,
              ' . $NameField . ' as Name,
							CASE
								WHEN u.RecordType = \'' . USERTYPE_STAFF . '\' and u.Teaching = 1  then \'Teaching\'
								WHEN u.RecordType = \'' . USERTYPE_STAFF . '\' and u.Teaching <> 1 then \'Non Teaching\'
							END as UserType
					  from
					  	`INTRANET_USER` u
					  where
					  	u.RecordStatus = \'1\' and u.RecordType = \'' . USERTYPE_STAFF . '\'
              ' . $UserCondition . '
					  	and (
					  		u.UserLogin like \'%' . $SearchValue . '%\'
					  		OR
					  		u.EnglishName like \'%' . $SearchValue . '%\'
					  		OR
					  		u.UserEmail like \'%' . $SearchValue . '%\'
					  		OR
					  		u.ChineseName like \'%' . $SearchValue . '%\'
					  	)
					  	ORDER BY u.ClassLevel, u.ClassName, u.ClassNumber
					  	';
        // dump($sql);
        // dump(mysql_error());
        $rs = $this->returnArray($sql);
        
        if ($junior_mck > 0) { // EJ
            foreach ($rs as $i => $r) {
                $rs[$i]['Name'] = convert2unicode($r['Name']);
            }
        }
        
        return $rs;
    }

    function Add_Category_Pic($CategoryCode, $TargetUserIDs)
    {
        $sql = 'DELETE FROM `INTRANET_DC_CATEGORY_PIC`
				WHERE
				 CategoryCode = "' . $CategoryCode . '"';
        $this->db_db_query($sql);
        $Result = array();
        if (sizeof($TargetUserIDs) > 0) {
            $TargetUserIDs = array_values(array_unique($TargetUserIDs));
            for ($i = 0; $i < sizeof($TargetUserIDs); $i ++) {
                if ($TargetUserIDs != "") {
                    $sql = 'INSERT INTO `INTRANET_DC_CATEGORY_PIC` (
										CategoryCode,
										PicID,
										DateModified,
										LastModifiedBy
									)
									value (
										\'' . $CategoryCode . '\',
										\'' . $TargetUserIDs[$i] . '\',
										Now(),
										\'' . $_SESSION['UserID'] . '\'
									)';
                    // debug_r($sql);
                    $Result['InsertGroupMember:' . $TargetUserIDs[$i]] = $this->db_db_query($sql);
                }
            }
        }
        
        // debug_r($Result);
        return (! in_array(false, $Result));
    }

    function getTargetUserName($userIDAry)
    {
        global $Lang, $sys_custom, $junior_mck;
        
        $retAry = array();
        $userIDAry = IntegerSafe($userIDAry);
        if (count($userIDAry)) {
            $studentNameField = getNameFieldWithClassNumberByLang();
            if ($junior_mck) {
                $teacherNameField = getNameFieldByLang("",$sys_custom['hideTeacherTitle']);
            }
            else {
                $teacherNameField = getNameFieldWithClassNumberByLang("", $sys_custom['hideTeacherTitle'], $sys_custom['eEnrolment']['ShowStudentNickName']);
            }
            
            $sql = "SELECT  UserID,
                            RecordType,
                            $studentNameField AS StudentName,
                            $teacherNameField AS TeacherName
                    FROM
                            INTRANET_USER
                    WHERE
                            UserID IN (".implode(",",$userIDAry).")
                    ORDER BY
                            UserID";
            $userIDWithTypeAry = $this->returnResultSet($sql);
            if (count($userIDWithTypeAry)) {
                foreach((array)$userIDWithTypeAry as $_userIDWithTypeAry) {
                    $_userID = $_userIDWithTypeAry['UserID'];
                    $_recordType = $_userIDWithTypeAry['RecordType'];
                    switch ($_recordType) {
                        case USERTYPE_STUDENT:
                            $_studentName = $_userIDWithTypeAry['StudentName'];
                            $retAry[] = array($_userID, $_studentName);
                            break;
                            
                        case USERTYPE_STAFF:
                            $_teacherName = $_userIDWithTypeAry['TeacherName'];
                            $retAry[] = array($_userID, $_teacherName);
                            break;
                            
                        case USERTYPE_PARENT:
                            $name_field = getNameFieldWithClassNumberByLang("b.");
                            $name_field2 = getNameFieldByLang("c.");
                            
                            $sql = "SELECT  DISTINCT
                            $name_field AS ParentName,
                            $name_field2 AS StudentName,
                            c.ClassName,
                            c.ClassNumber
                            FROM
                            INTRANET_PARENTRELATION AS a
                            INNER JOIN INTRANET_USER AS b ON (b.UserID=a.ParentID)
                            INNER JOIN INTRANET_USER AS c ON (c.UserID=a.StudentID)
                            WHERE
                            a.ParentID='".$_userID."'
                                            AND c.RecordStatus=1
                                    ORDER BY
                                            StudentName, ParentName";
                            $_parentInfoAry = $this->returnResultSet($sql);
                            if (count($_parentInfoAry)) {
                                $_parentInfoAry = current($_parentInfoAry);
                                $_parentName = $_parentInfoAry['ParentName'];
                                $_studentName = $_parentInfoAry['StudentName'];
                                $_className= $_parentInfoAry['ClassName'];
                                $_classNumber= $_parentInfoAry['ClassNumber'];
                                
                                $tmp = $_className.($_classNumber!=""?"-".$_classNumber:"");
                                $_displayName = ($tmp=="") ? "" : "($tmp) ";
                                $_displayName .= $_studentName." ".$Lang['General']['s']."(".$_parentName.")</option>\n";
                                $retAry[] = array($_userID, $_displayName);
                            }
                            break;
                    }
                }
            }
        }
        
        if ($junior_mck) {
            $juniorRetAry = array();
            foreach((array)$retAry as $_index=>$_retAry) {
                $juniorRetAry[] = array($_retAry[0], convert2unicode($_retAry[1]));
            }
            return $juniorRetAry;
        }
        
        return $retAry;
    }
    
    // ///////////////-----------------function copy from /includes/kis/apps/libkis_admission.php [start]--------------------//////////////
    // public function getAlbums($params=array()){
    //
    // extract($params);
    // $group = $group_id? "INNER JOIN INTRANET_ALBUM_USER u ON a.AlbumID = u.AlbumID AND u.RecordID = $group_id AND u.RecordType = 'group'": "";
    // $mine = $my_album_only? "a.InputBy = ".$this->user_id." AND":'';
    //
    // $sql = "SELECT
    // a.AlbumID id,
    // a.Title title,
    // a.Description description,
    // a.PlaceTaken place,
    // UNIX_TIMESTAMP(a.DateTaken) date,
    // a.InputBy input_by,
    // a.CoverPhotoID cover_photo_id,
    // p.AlbumPhotoID first_photo_id,
    // UNIX_TIMESTAMP(a.DateInput) input_date,
    // a.InputBy input_by,
    // COUNT(p.AlbumPhotoID) as photo_count
    // FROM INTRANET_ALBUM a
    // LEFT JOIN INTRANET_ALBUM_PHOTO p ON a.AlbumID = p.AlbumID
    // $group
    // WHERE $mine (a.Title LIKE '%$keyword%')
    // GROUP BY a.AlbumID";
    //
    // return $this->returnArray($sql);
    //
    // }
    public function getAlbum($AlbumID)
    {
//         global $sys_custom;
// 		if($sys_custom['eClassApp']['SFOC']){
		    $extraField = ', a.ChiTitle AS titleZh, a.ChiDescription AS descriptionZh';	    
//		}
        $sql = "SELECT
		    a.AlbumID id,
		    a.Title title,
		    a.Description description,
		    a.CoverPhotoID cover_photo_id,
		    a.PlaceTaken place,
		    UNIX_TIMESTAMP(a.SharedSince) since,
		    UNIX_TIMESTAMP(a.SharedUntil) until,
		    UNIX_TIMESTAMP(a.DateTaken) date,
		    UNIX_TIMESTAMP(a.DateInput) input_date,
		    a.InputBy input_by, 
			a.CategoryCode
            {$extraField}
		FROM INTRANET_DC_ALBUM a
		WHERE a.AlbumID = '" . $AlbumID . "'";
        
        return current($this->returnArray($sql));
    }

    public function getAlbumUsers()
    {
        $sql = "SELECT
		    u.UserID user_id.
		    u.EnglishName user_name_en.
		    u.ChineseName user_name_b5
		FROM INTRANET_DC_ALBUM_USER a
		INNER JOIN INTRANET_DC_USER u ON a.RecordType = 'user' AND a.RecordID = u.UserID";
        
        return $this->returnArray($sql);
    }

    public function getAlbumGroups()
    {
        $sql = "SELECT
		    g.GroupID group_id,
		    g.Title group_name_en,
		    g.TitleChinese group_name_b5
		FROM INTRANET_DC_ALBUM_USER a
		INNER JOIN INTRANET_GROUP g ON a.RecordType = 'group' AND a.RecordID = g.GroupID";
        
        return $this->returnArray($sql);
    }

    public function getAlbumPhotos($AlbumID)
    {
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by,
			p.Sequence Sequence
		FROM INTRANET_DC_ALBUM_PHOTO p
		WHERE p.AlbumID = '" . $AlbumID . "'
		ORDER BY $sort IF(Sequence IS NULL, -1, Sequence), DateInput";
        
        return $this->returnArray($sql);
    }

    public function getAlbumPhoto($photo_id)
    {
        $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by, 
			p.Sequence Sequence,
            p.ChiDescription
		FROM INTRANET_DC_ALBUM_PHOTO p
		WHERE AlbumPhotoID = '".$photo_id."'";
        
        return current($this->returnArray($sql));
    }

    public function getAlbumPICs($AlbumID)
    {
        global $junior_mck;
        $namefield = GET_LANG_SELECTION("iu.ChineseName", "iu.EnglishName");
        $otherNameField = GET_LANG_SELECTION("iu.EnglishName", "iu.ChineseName");
        
        $sql = "SELECT
					iu.UserID,
					IF(" . $namefield . " IS NULL OR TRIM(" . $namefield . ") = '', " . $otherNameField . ", " . $namefield . ") AS NAME,
				    p.AlbumPicID
				FROM INTRANET_DC_ALBUM_PIC p
					INNER JOIN INTRANET_USER iu
					ON p.PicID = iu.UserID
				WHERE p.AlbumID = '" . $AlbumID . "'";
        $rs = $this->returnArray($sql);
        
        if ($junior_mck > 0) { // EJ
            foreach ($rs as $i => $r) {
                $rs[$i][1] = convert2unicode($r[1]);
                $rs[$i]['NAME'] = convert2unicode($r['NAME']);
            }
        }
        return $rs;
    }

    public function updateAlbumPICs($studentID, $AlbumID, $currentUserID='')
    {
        
        // Remove all existing PICs
        $sql = "DELETE FROM INTRANET_DC_ALBUM_PIC WHERE AlbumID = '" . $AlbumID . "'";
        $this->db_db_query($sql);
        
        $inputBy = empty($currentUserID) ? $_SESSION['UserID'] : $currentUserID;
        
        // Update current PICs
        $delim = "";
        $sql = "INSERT INTO INTRANET_DC_ALBUM_PIC (AlbumID, PicID, DateInput, InputBy) VALUES";
        if (count($studentID) > 0) {
            foreach ($studentID as $PICsID) {
                $PICsID = is_numeric($PICsID) ? $PICsID : substr($PICsID, 1);
                if (trim($PICsID) != "") {
                    $sql .= $delim . "('" . $AlbumID . "', '" . $PICsID . "', now(), '" . $inputBy. "')";
                    $delim = " , ";
                }
            }
            // debug_pr($sql);
            return $this->db_db_query($sql);
        }
    }

    // public function createAlbum($params){
    //
    // extract($params);
    //
    // $sql = "INSERT INTO INTRANET_ALBUM (Title, Description, PlaceTaken, DateTaken, SharedSince, SharedUntil, InputBy, DateInput)
    // VALUES ('$title','$description','$place','$date','$since','$until',".$this->user_id.",now())";
    //
    // $this->db_db_query($sql);
    // $this->album_id = $this->db_insert_id();
    //
    // return $this->album_id;
    //
    // }
    
    // public function updateAlbum($params){
    //
    // extract($params);
    //
    // $sql = "UPDATE INTRANET_ALBUM SET
    // Title = '$title',
    // Description = '$description',
    // PlaceTaken = '$place',
    // DateTaken = '$date',
    // SharedSince = '$since',
    // SharedUntil = '$until',
    // ModifyBy = '".$this->user_id."'
    // WHERE AlbumID = ".$this->album_id;
    //
    // $this->db_db_query($sql);
    //
    // }
    
    // public function removeAlbum(){
    //
    // $sql = "DELETE a,p,u
    // FROM INTRANET_ALBUM a
    // LEFT JOIN INTRANET_ALBUM_PHOTO p ON a.AlbumID = p.AlbumID
    // LEFT JOIN INTRANET_ALBUM_USER u ON a.AlbumID = u.AlbumID
    // WHERE a.AlbumID = ".$this->album_id;
    //
    // $this->db_db_query($sql);
    // }
    public function updateAlbumCoverPhoto($photo_id, $album_id)
    {
        global $UserID;
        
        $sql = "UPDATE INTRANET_DC_ALBUM SET
		    CoverPhotoID = '$photo_id',
		    ModifyBy = '" . $UserID . "'
		WHERE AlbumID = '" . $album_id . "'";
        
        return $this->db_db_query($sql);
    }

    // public function createAlbumPhoto($params=array()){
    //
    // $sql = "SELECT MAX(Sequence)+1 FROM INTRANET_ALBUM_PHOTO WHERE AlbumID=".$this->album_id." GROUP BY AlbumID";
    // $sequence = (int)current($this->returnVector($sql));
    //
    // extract($params);
    //
    // $sql = "INSERT INTO INTRANET_ALBUM_PHOTO (AlbumID, Title, Size, Sequence, DateTaken, InputBy, DateInput)
    // VALUES ('$album_id','$title', '$size', '$sequence', FROM_UNIXTIME('$date'),".$this->user_id.",now())";
    //
    // $this->db_db_query($sql);
    //
    // return $this->db_insert_id();
    //
    // }
    public function updateAlbumPhoto($photo_id, $params, $currentUserID='')
    {
        global $UserID;
        
        extract($params);
        
        $updateFields = array();
        if (isset($description)) {
            $updateFields[] = "Description = '".$this->Get_Safe_Sql_Query($description)."',";
        }
        if (isset($chiDescription)) {
            $updateFields[] = "ChiDescription = '".$this->Get_Safe_Sql_Query($chiDescription)."',";
        }        
        if (isset($eventTitle)) {
            $updateFields[] = "EventTitle = '".$this->Get_Safe_Sql_Query($eventTitle)."',";
        }
        if (isset($chiEventTitle)) {
            $updateFields[] = "ChiEventTitle = '".$this->Get_Safe_Sql_Query($chiEventTitle)."',";
        }        
        if (isset($EventDate)) {
            $updateFields[] = "EventDate = '".$EventDate."',";
        }
        if (count($updateFields) > 0 && $photo_id) {
            $updatedBy = empty($currentUserID) ? $UserID : $currentUserID; 
            $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO SET " . implode('', $updateFields) . "ModifyBy = '" . $updatedBy. "'
    			WHERE AlbumPhotoID = '$photo_id'";
            $this->db_db_query($sql);
        }
    }

    public function updateAlbumEvent($photoIDAry, $eventTitle = '', $eventDate = '', $chiEventTitle = '')
    {
        if (count($photoIDAry)) {
            if ($eventDate == '') {
                $eventDate = date('Y-m-d');
            }
            $photoIDAry = array_filter($photoIDAry);        // remove empty elements
            
            $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO SET 
                        EventTitle='".$this->Get_Safe_Sql_Query($eventTitle)."',
                        ChiEventTitle='".$this->Get_Safe_Sql_Query($chiEventTitle)."',
                        EventDate = '".$eventDate."',
                        ModifyBy = '" . $_SESSION['UserID'] . "'
                    WHERE AlbumPhotoID IN ('" . implode("','", $photoIDAry) . "')";
            $this->db_db_query($sql);
        }
    }

    public function reorderAlbumPhotos($photo_ids, $albumID='')
    {
        global $UserID;
        
        if ($albumID && count($photo_ids)) {
            $sql = "SELECT MAX(Sequence)+1 AS maxSeq FROM INTRANET_DC_ALBUM_PHOTO WHERE AlbumID='" . $albumID. "' AND AlbumPhotoID NOT IN ('".implode("','",$photo_ids)."')";
            $count = (int) current($this->returnVector($sql));
        }
        else {
            $count = 0;
        }
        
        foreach ($photo_ids as $photo_id) {
            if (! $photo_id) {
                continue;
            }
            $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO SET
		    Sequence = '$count',
		    ModifyBy = '" . $UserID . "'
		WHERE AlbumPhotoID = $photo_id";
            
            $this->db_db_query($sql);
            
            $count ++;
        }
    }

    public function removeAlbumPhoto($photo_id)
    {
        $result = array();
        
        $sql = "UPDATE INTRANET_DC_ALBUM SET CoverPhotoID = NULL WHERE CoverPhotoID = $photo_id";
        $result[] = $this->db_db_query($sql);
        
        $sql = "DELETE FROM INTRANET_DC_ALBUM_PHOTO WHERE AlbumPhotoID = $photo_id";
        $result[] = $this->db_db_query($sql);;

        return in_array(false,$result) ? false : true;
    }

    public function removeAlbumPhotos($albumID, $photoIDAry)
    {
        global $file_path;
        
        $album = $this->getAlbum($albumID);
        $result = array();
        
        foreach((array)$photoIDAry as $_photoID) {
            if ($_photoID) {        // skip the last emtpy one
                $photo = $this->getAlbumPhoto($_photoID);
                
                if ($this->removeAlbumPhoto($_photoID)){
                    
                    # Remove photo / video
                    $file = $file_path.self::getPhotoFileName('photo', $album, $photo);
                    if (is_file($file)) {
                        $result[] = unlink($file);
                    }

                    # Remove Thumbnail if not default photo
                    $thumbnail_url = trim(self::getPhotoFileName('thumbnail', $album, $photo));
                    if ($thumbnail_url != '/images/2009a/icon_files/avi_l.png'){
                        $file = $file_path.$thumbnail_url;
                        if (is_file($file)) {
                            $result[] = unlink($file);
                        }
                    }
                    
                    # Remove original photo / video
                    $file = $file_path.self::getPhotoFileName('original', $album, $photo);
                    if (is_file($file)) {
                        $result[] = unlink($file);
                    }
                }
                else {
                    $result[] = false;
                }
            }
        }
        return (in_array(false,$result) ? false : true);
    }
    
    public function getAlbumUsersGroups($album_id = 0)
    {
        // $album_id = $album_id? $album_id: $this->album_id;
        $sql = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER
			WHERE RecordType = 'all' AND
			    AlbumID = " . $album_id;
        
        if (current($this->returnVector($sql))) {
            
            return array(
                1,
                array(),
                array()
            );
        } else {
            
            $sql = "SELECT RecordID
			    FROM INTRANET_DC_ALBUM_USER
			    WHERE RecordType = 'group' AND
				AlbumID = " . $album_id;
            
            $groups = $this->returnVector($sql);
            
            $sql = "SELECT RecordID
			    FROM INTRANET_DC_ALBUM_USER
			    WHERE RecordType = 'user' AND
				AlbumID = " . $album_id;
            
            $users = $this->returnVector($sql);
            
            return array(
                0,
                $groups,
                $users
            );
        }
    }

    public function resetAlbumUsersGroups($all_users, $group_ids = array(), $user_ids = array(), $album_id = 0, $currentUserID='')
    {
        global $UserID;
        
        $sql = "DELETE FROM INTRANET_DC_ALBUM_USER WHERE AlbumID = '" . $album_id . "'";
        
        $this->db_db_query($sql);
        
        $inputBy = empty($currentUserID) ? $UserID : $currentUserID;
        
        if ($all_users) {
            
            $sql = "INSERT INTO INTRANET_DC_ALBUM_USER (AlbumID, RecordType, DateInput, InputBy)
		    VALUES ('" . $album_id . "', 'all', now(), '" . $inputBy. "')";
            
            $this->db_db_query($sql);
        } else {
            
            foreach ((array) $group_ids as $group_id) {
                
                $sql = "INSERT INTO INTRANET_DC_ALBUM_USER (AlbumID, RecordID, RecordType, DateInput, InputBy)
		    VALUES ('" . $album_id . "', $group_id, 'group', now(), '" . $inputBy. "')";
                
                $this->db_db_query($sql);
            }
            
            foreach ((array) $user_ids as $user_id) {
                
                $sql = "INSERT INTO INTRANET_DC_ALBUM_USER (AlbumID, RecordID, RecordType, DateInput, InputBy)
		    VALUES ('" . $album_id . "', $user_id, 'user',  now(), '" . $inputBy. "')";
                
                $this->db_db_query($sql);
            }
        }
        
        return $this;
    }

    // public static function getAcademicYearGroups($params=array()){
    //
    // extract($params);
    // $libdb = self::getLibdb();
    //
    // $cond .= isset($group_ids)? "g.GroupID IN (".implode(',',$group_ids).") AND ": "";
    // $cond .= isset($excludes)? "g.GroupID NOT IN (".implode(',',$excludes).") AND ": "";
    // $tables .= isset($user_id)? "INNER JOIN INTRANET_USERGROUP ug ON ug.GroupID = g.GroupID AND ug.UserID = $user_id": "";
    //
    // $academic_year_id = isset($academic_year_id)? $academic_year_id: $_SESSION['CurrentSchoolYearID'];
    // $hide_basic_groups = isset($hide_basic_groups)? '':'OR g.AcademicYearID IS NULL';
    //
    // $sql = "SELECT
    // g.GroupID group_id,
    // g.Title group_name_en,
    // g.TitleChinese group_name_b5,
    // g.RecordType group_type,
    // g.GroupLogoLink group_photo,
    // if (g.AcademicYearID IS NULL, '' ,g.Title) sort_order
    // FROM INTRANET_GROUP g
    // $tables
    // WHERE $cond (g.AcademicYearID = $academic_year_id $hide_basic_groups) AND (
    // g.Title LIKE '%$keyword%' OR
    // g.TitleChinese LIKE '%$keyword%'
    // )
    // ORDER BY sort_order asc, GroupID asc";
    //
    // return $libdb->returnArray($sql);
    //
    // }
    // ///////////////-----------------function copy from /includes/kis/apps/libkis_admission.php [end]--------------------//////////////
    
    // ///////////////-----------------function for elcass app [start]--------------------//////////////
    public function AppIsAlbumReadable($album_id, $parUserID)
    {
        $sql = "Select UserID From ROLE_RIGHT r JOIN ROLE_MEMBER m ON r.RoleID = m.RoleID where r.FunctionName = 'eAdmin-DigitalChannels'";
        $result = $this->returnVector($sql);
        
        if (! is_array($parUserID)) {
            $parUserID = array(
                $parUserID
            );
        }
        
        foreach ($parUserID as $aParUserID) {
            if (in_array($aParUserID, $result))
                return true;
        }
        
        $sql = "SELECT InputBy,
		IF (SharedSince <= CURDATE() OR SharedSince IS NULL OR SharedSince = 0, 1, 0),
		IF (SharedUntil >= CURDATE() OR SharedUntil IS NULL OR SharedUntil = 0, 1, 0) 
		FROM INTRANET_DC_ALBUM
		WHERE AlbumID = $album_id";
        
        $settings = current($this->returnArray($sql));
        echo mysql_error();
        
        foreach ($parUserID as $aParUserID) {
            if ($settings[0] == $aParUserID) {
                return true;
            }
        }
        
        if (! $settings[1] || ! $settings[2]) {
            return false;
        }
        
        $sql1 = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID = $album_id AND a.RecordType = 'all'";
        
        $sql2 = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER a
			INNER JOIN INTRANET_USERGROUP g ON a.RecordID = g.GroupID AND a.RecordType = 'group'
			WHERE a.AlbumID = $album_id AND (g.UserID IN ('" . implode("','", $parUserID) . "'))";
        
        $sql3 = "SELECT COUNT(*)
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID = $album_id AND a.RecordID IN ('" . implode("','", $parUserID) . "') AND a.RecordType = 'user'";
        
        return current($this->returnVector($sql1)) > 0 || current($this->returnVector($sql2)) > 0 || current($this->returnVector($sql3)) > 0;
    }

	public function AppIsAlbumsReadable($albumIdArr, $parUserID)
    {
        $sql = "Select UserID From ROLE_RIGHT r JOIN ROLE_MEMBER m ON r.RoleID = m.RoleID where r.FunctionName = 'eAdmin-DigitalChannels'";
        $result = $this->returnVector($sql);
        
        if (! is_array($parUserID)) {
            $parUserID = array(
                $parUserID
            );
        }
        
        foreach ($parUserID as $aParUserID) {
            if (in_array($aParUserID, $result)){
            	$albumsReadable = array();
		        foreach($albumIdArr as $albumId){
		        	$albumsReadable[$albumId] = true;
		        }	
		        return $albumsReadable;
            }
        }
        
        $albumIDs = implode("','",$albumIdArr);
        
        $sql = "SELECT AlbumID, InputBy,
		IF (SharedSince <= CURDATE() OR SharedSince IS NULL OR SharedSince = 0, 1, 0) as SharedSince,
		IF (SharedUntil >= CURDATE() OR SharedUntil IS NULL OR SharedUntil = 0, 1, 0) as SharedUntil
		FROM INTRANET_DC_ALBUM
		WHERE AlbumID IN ('".$albumIDs."') ORDER BY DateTaken desc, Title asc";
        
        $settings = $this->returnArray($sql);
        $settings = BuildMultiKeyAssoc($settings, "AlbumID", array("AlbumID","InputBy","SharedSince","SharedUntil"), 1);
        
        echo mysql_error();
        
        $albumsReadable = array();
        $withinSharePeriod = array();
        
        foreach($albumIdArr as $albumId){
        	$albumsReadable[$albumId] = false;
        	$withinSharePeriod[$albumId] = true;
	        foreach ($parUserID as $aParUserID) {
	            if ($settings[$albumId]['InputBy'] == $aParUserID) {
	                $albumsReadable[$albumId] = true;
	            }
	        }
	        if (! $settings[$albumId]['SharedSince'] || ! $settings[$albumId]['SharedUntil']) {
	           $albumsReadable[$albumId] = false;
	           $withinSharePeriod[$albumId] = false;
	        }
        }
        
        // please edit below...
        $sql1 = "SELECT  a.AlbumID as AlbumID, COUNT(*) as count
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID IN ('".$albumIDs."') AND a.RecordType = 'all'
			GROUP BY a.AlbumID";

        $result1 = $this->returnArray($sql1);
        $result1 = BuildMultiKeyAssoc($result1, "AlbumID", array("AlbumID", "count"), 1);

        foreach($albumIdArr as $albumId){
            if(isset($result1[$albumId]) && $result1[$albumId]['count'] > 0 && $albumsReadable[$albumId] == false && $withinSharePeriod[$albumId]){
                $albumsReadable[$albumId] = true;
            }
        }

        $sql2 = "SELECT a.AlbumID as AlbumID, COUNT(*) as count
			FROM INTRANET_DC_ALBUM_USER a
			INNER JOIN INTRANET_USERGROUP g ON a.RecordID = g.GroupID AND a.RecordType = 'group'
			WHERE a.AlbumID IN ('".$albumIDs."') AND (g.UserID IN ('" . implode("','", $parUserID) . "'))
            GROUP BY a.AlbumID";

        $result2 = $this->returnArray($sql2);
        $result2 = BuildMultiKeyAssoc($result2, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result2[$albumId]) && $result2[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false && $withinSharePeriod[$albumId]){
                $albumsReadable[$albumId] = true;
            }
        }
        
        $sql3 = "SELECT a.AlbumID as AlbumID, COUNT(*) as count
			FROM INTRANET_DC_ALBUM_USER a
			WHERE a.AlbumID IN ('".$albumIDs."') AND a.RecordID IN ('" . implode("','", $parUserID) . "') AND a.RecordType = 'user'
            GROUP BY a.AlbumID";
        $result3 = $this->returnArray($sql3);
        $result3 = BuildMultiKeyAssoc($result3, "AlbumID", array("AlbumID", "count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result3[$albumId]) && $result3[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false && $withinSharePeriod[$albumId]){
                $albumsReadable[$albumId] = true;
            }
        }

        return $albumsReadable;
    }

    function AppGetCategory($parUserID, $CategoryCode = '')
    { // return (CategoryID, DescriptionEn, DescriptionChi, NumOfAlbum)
        if ($CategoryCode != '') {
            $cond .= ' AND  CategoryCode = "' . $CategoryCode . '"';
        }
        $sql = 'SELECT 
					CategoryCode CategoryID,
					DescriptionEn,
					DescriptionChi
				FROM 
					INTRANET_DC_CATEGORY 
				WHERE 1 
				' . $cond . ' 
				Order By
					DisplayOrder';
        
        $categoryArr = $this->returnArray($sql);
        
        // get num of album in category
        $numOfCategory = count($categoryArr);
        for ($i = 0; $i < $numOfCategory; $i ++) {
            $countAlbums = 0;
            $catAlbums = $this->AppGetAlbum($parUserID, $categoryArr[$i]['CategoryID']);
            // debug_pr($catAlbums);
            
            $albumIDs = array();
			foreach ($catAlbums as $currentAlbum) {
				$albumIDs[] = $currentAlbum['AlbumID'];
			}
			$isAlbumsReadable = $this->AppIsAlbumsReadable($albumIDs, $parUserID);
			
            foreach ($catAlbums as $currentAlbum) {
                if ($isAlbumsReadable[$currentAlbum['AlbumID']]) {
                    $countAlbums ++;
                }
            }
            $categoryArr[$i]['NumOfAlbum'] = $countAlbums;
            $categoryArr[$i]['DescriptionEn'] = htmlspecialchars_decode($categoryArr[$i]['DescriptionEn']);
            $categoryArr[$i]['DescriptionChi'] = htmlspecialchars_decode($categoryArr[$i]['DescriptionChi']);
            if ($countAlbums <= 0) {
                unset($categoryArr[$i]);
            }
        }
        
        return array_values($categoryArr);
    }

    function AppGetAlbum($parUserID, $CategoryCode = '',$ParLang='')
    { // return (AlbumID, CategoryID, Title, Description, DateTaken, NumOfPhoto, CoverPhotoPath)
        if ($CategoryCode != '') {
            $cond .= ' AND  CategoryCode = "' . $CategoryCode . '"';
        }
        $sql = 'SELECT 
					AlbumID,
					CategoryCode CategoryID, 
					Title, 
					Description, 
					DATE(DateTaken) as DateTaken,
					/* DateInput, */
                    DateTaken as DateInput,
					InputBy,
					CoverPhotoID,
                    DateInput as DateInputOriginal,
                    ChiTitle,
                    ChiDescription
				FROM 
					INTRANET_DC_ALBUM
				WHERE 1
				' . $cond . ' ORDER BY DateTaken desc, Title asc';
        
        $albumInfoAry = $this->returnResultSet($sql);
        
        $albumIDs = array();
		foreach ($albumInfoAry as $aAlbumInfo) {
			$albumIDs[] = $aAlbumInfo['AlbumID'];
		}
		$isAlbumsReadable = $this->AppIsAlbumsReadable($albumIDs, $parUserID);
			
        $tempAlbumInfoAry = array();
        foreach ($albumInfoAry as $aAlbumInfo) {
            if (! $isAlbumsReadable[$aAlbumInfo['AlbumID']]) {
                continue;
            }
            
            $ablumPhotoInfoAry = $this->Get_Album_Photo('', $aAlbumInfo['AlbumID']);
            
            $album = array(
                "id" => $aAlbumInfo['AlbumID'],
                "input_date" => strtotime($aAlbumInfo['DateInputOriginal']),
                "input_by" => $aAlbumInfo['InputBy']
            );
            
            // get cover photo info
            $coverPhotoInfo = current($ablumPhotoInfoAry);
            
            $numOfPhoto = 0;
            foreach ($ablumPhotoInfoAry as $aPhoto) {
                $o_ext = pathinfo($aPhoto['title'], PATHINFO_EXTENSION);
                // if(in_array(strtoupper($o_ext),libdigitalchannels::$videoFormat)){
                $numOfPhoto ++;
                // }
                if ($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']) {
                    $coverPhotoInfo = $aPhoto;
                }
            }
            
            $coverPhotoInfo = ($coverPhotoInfo ? $coverPhotoInfo : ($ablumPhotoInfoAry ? $ablumPhotoInfoAry[0] : ''));
            
            $aAlbumInfo['NumOfPhoto'] = $numOfPhoto;
            $aAlbumInfo['CoverPhotoPath'] = $coverPhotoInfo ? libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo) : '';
            if($ParLang=="zh"){
                $aAlbumInfo['Title'] = htmlspecialchars_decode($aAlbumInfo['ChiTitle']);
                $aAlbumInfo['Description'] = htmlspecialchars_decode($aAlbumInfo['ChiDescription']);     // not store htmlspecialchars in db
            }else{
                $aAlbumInfo['Title'] = htmlspecialchars_decode($aAlbumInfo['Title']);
                $aAlbumInfo['Description'] = htmlspecialchars_decode($aAlbumInfo['Description']);
            }
            // if($numOfPhoto > 0)
            $tempAlbumInfoAry[] = $aAlbumInfo;
        }
        $albumInfoAry = $tempAlbumInfoAry;
        //debug_pr($albumInfoAry);

        return $albumInfoAry;
    }

    function AppGetPhoto($parUserID, $AlbumID = '', $PhotoID = '',$ParLang='')
    { // return (PhotoID, AlbumID, Title, Description, Size, DateInput, FilePath, OriginalFilePath, Type, ViewTotal, FavoriteTotal, CommentTotal)
        global $dcvod;

        if ($AlbumID) {
            $cond .= " AND p.AlbumID = '" . $AlbumID . "'";
        }
        
        if ($PhotoID) {
            $cond .= " AND p.AlbumPhotoID = '" . $PhotoID . "'";
        }
        
        $sql = "SELECT
		    p.AlbumPhotoID PhotoID,
			p.AlbumID as AlbumID,
		    p.Title Title,
		    p.Description Description,
		    p.Size as Size,
		    UNIX_TIMESTAMP(p.DateInput) DateInput,
			UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by,
			p.AlbumPhotoID id,
			p.Title title,
			UNIX_TIMESTAMP(a.DateInput) a_input_date,
			a.InputBy a_input_by,
			a.DateInput DateInput_DateFormat,
			p.EventTitle EventTitle,
		    p.EventDate EventDate,
		    p.ChiEventTitle ChiEventTitle,
		    p.ChiDescription ChiDescription,
		    p.Vod
	FROM 
			INTRANET_DC_ALBUM_PHOTO p
		JOIN
			INTRANET_DC_ALBUM a
		ON
			a.AlbumID = p.AlbumID
		WHERE 
			1 $cond
		ORDER BY 
			$sort IF(Sequence IS NULL, -1, Sequence), p.DateInput";
        $allPhoto = $this->returnResultSet($sql);
        
        $albumIDs = array();
		foreach ($allPhoto as $aMostHit) {
			$albumIDs[] = $aMostHit['AlbumID'];
		}
		$isAlbumsReadable = $this->AppIsAlbumsReadable($albumIDs, $parUserID);
        
        $tempArr = array();
        $unreadableAlbumIdArr = array();
        foreach ($allPhoto as $aMostHit) {
            if (in_array($aMostHit['AlbumID'], $unreadableAlbumIdArr)) {
                continue;
            } else if (! $isAlbumsReadable[$aMostHit['AlbumID']]) {
                $unreadableAlbumIdArr[] = $aMostHit['AlbumID'];
                continue;
            }
            $photo_ext = pathinfo($aMostHit['Title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['AlbumID'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            if ($isVideo && $dcvod && $aMostHit['Vod']){
                $originalFilePath = $this->getVodLink($aMostHit['Vod']);
            } else {
                $originalFilePath = self::getPhotoFileName('photo', $album, $aMostHit);
            }
            $filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            $aMostHit['OriginalFilePath'] = $originalFilePath;
            $aMostHit['Type'] = ($isVideo ? 'Video' : 'Photo');
            $aMostHit['ViewTotal'] = $this->Get_Album_Photo_View_Total($aMostHit['PhotoID']);
            $aMostHit['FavoriteTotal'] = $this->Get_Album_Photo_Favorites_Total($aMostHit['PhotoID']);
            $aMostHit['CommentTotal'] = $this->Get_Album_Photo_Comment_Total($aMostHit['PhotoID']);
            if($ParLang=="zh"){
                $aMostHit['Description'] = htmlspecialchars_decode($aMostHit['ChiDescription']);
                $aMostHit['Title'] = $aMostHit['ChiDescription'] ? $aMostHit['ChiDescription'] : $aMostHit['Title'];
                $aMostHit['EventTitle'] = htmlspecialchars_decode($aMostHit['ChiEventTitle']);
            }else{
                $aMostHit['Description'] = htmlspecialchars_decode($aMostHit['Description']);
                $aMostHit['Title'] = $aMostHit['Description'] ? $aMostHit['Description'] : $aMostHit['Title'];
                $aMostHit['EventTitle'] = htmlspecialchars_decode($aMostHit['EventTitle']);
            }

            $aMostHit['EventDate'] = htmlspecialchars_decode($aMostHit['EventDate']);            
            // unset($aMostHit['6']);
            // unset($aMostHit['7']);
            // unset($aMostHit['8']);
            // unset($aMostHit['9']);
            // unset($aMostHit['10']);
            // unset($aMostHit['11']);
            unset($aMostHit['input_date']);
            unset($aMostHit['input_by']);
            unset($aMostHit['id']);
            unset($aMostHit['title']);
            unset($aMostHit['a_input_date']);
            unset($aMostHit['a_input_by']);
            // if($this->AppIsAlbumReadable($aMostHit['AlbumID'],$parUserID)){
            $tempArr[] = $aMostHit;
            // }
        }
        
        return $tempArr;
    }

    function AppGetRecPhoto($parUserID, $PhotoIDArr = '', $ParLang = '')
    { // return (PhotoID, AlbumID, Title, Description, Size, DateInput, FilePath, OriginalFilePath, Type, ViewTotal, FavoriteTotal, CommentTotal)
        global $dcvod;

        if ($PhotoIDArr) {
            if (! is_array($PhotoIDArr)) {
                $PhotoIDArr = array(
                    $PhotoIDArr
                );
            }
            $cond .= " AND p.AlbumPhotoID IN ('" . implode("','", $PhotoIDArr) . "') ";
        }
        
        $sql = "SELECT
		    p.AlbumPhotoID PhotoID,
			p.AlbumID as AlbumID,
		    p.Title Title,
		    p.Description Description,
		    p.Size as Size,
		    UNIX_TIMESTAMP(p.DateInput) DateInput,
			UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by,
			p.AlbumPhotoID id,
			p.Title title,
			UNIX_TIMESTAMP(a.DateInput) a_input_date,
			a.InputBy a_input_by,
			a.DateInput DateInput_DateFormat,
			p.EventTitle EventTitle,
		    p.EventDate EventDate,
		    p.ChiEventTitle ChiEventTitle,
		    p.ChiDescription ChiDescription,
		    p.Vod
		FROM 
			INTRANET_DC_ALBUM_PHOTO p
		JOIN
			INTRANET_DC_ALBUM a
		ON
			a.AlbumID = p.AlbumID
		LEFT JOIN
			INTRANET_DC_RECOMMEND_PHOTO r
		ON r.AlbumPhotoID = p.AlbumPhotoID
		WHERE 
			1 $cond
		ORDER BY 
			$sort IF(r.Sequence IS NULL, -1, r.Sequence), r.DateInput";
        $allPhoto = $this->returnResultSet($sql);
        
        $tempArr = array();
        foreach ($allPhoto as $aMostHit) {
            
            $photo_ext = pathinfo($aMostHit['Title'], PATHINFO_EXTENSION);
            $isVideo = (in_array(strtoupper($photo_ext), self::$videoFormat)) ? true : false;
            
            // add path to the photo array
            $album = array(
                "id" => $aMostHit['AlbumID'],
                "input_date" => $aMostHit['a_input_date'],
                "input_by" => $aMostHit['a_input_by']
            );
            
            // get the file path
            if ($isVideo && $dcvod && $aMostHit['Vod']){
                $originalFilePath = $this->getVodLink($aMostHit['Vod']);
            } else {
                $originalFilePath = self::getPhotoFileName('photo', $album, $aMostHit);
            }
            $filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
            $aMostHit['FilePath'] = $filePath;
            $aMostHit['OriginalFilePath'] = $originalFilePath;
            $aMostHit['Type'] = ($isVideo ? 'Video' : 'Photo');
            $aMostHit['ViewTotal'] = $this->Get_Album_Photo_View_Total($aMostHit['PhotoID']);
            $aMostHit['FavoriteTotal'] = $this->Get_Album_Photo_Favorites_Total($aMostHit['PhotoID']);
            $aMostHit['CommentTotal'] = $this->Get_Album_Photo_Comment_Total($aMostHit['PhotoID']);
            if($ParLang=="zh") {
                $aMostHit['EventTitle'] = htmlspecialchars_decode($aMostHit['ChiEventTitle']);
            }else{
                $aMostHit['EventTitle'] = htmlspecialchars_decode($aMostHit['EventTitle']);
            }
            $aMostHit['EventDate'] = htmlspecialchars_decode($aMostHit['EventDate']);
            // unset($aMostHit['6']);
            // unset($aMostHit['7']);
            // unset($aMostHit['8']);
            // unset($aMostHit['9']);
            // unset($aMostHit['10']);
            // unset($aMostHit['11']);
            unset($aMostHit['input_date']);
            unset($aMostHit['input_by']);
            unset($aMostHit['id']);
            unset($aMostHit['title']);
            unset($aMostHit['a_input_date']);
            unset($aMostHit['a_input_by']);
            // if(!$this->AppIsAlbumReadable($aMostHit['AlbumID'],$parUserID)){
            $tempArr[] = $aMostHit;
            // }
        }
        
        return $tempArr;
    }

    function AppGetPhotoType($parUserID)
    { // return (FavPhotoID, RecPhotoID)
        if (! is_array($parUserID)) {
            $parUserID = array(
                $parUserID
            );
        }
        
        $sql = "SELECT
				    PhotoID FavPhotoID
				FROM 
					INTRANET_DC_ALBUM_PHOTO_VIEW 
				WHERE
					UserID IN ('" . implode("','", $parUserID) . "') AND IsFavorite = 1
				ORDER BY 
					DateModified desc";
        $favPhotoID = $this->returnVector($sql);
        
        $sql = "SELECT 
					AlbumPhotoID RecPhotoID
				FROM 
					INTRANET_DC_RECOMMEND_PHOTO p 
				JOIN 
					INTRANET_DC_RECOMMEND r 
				ON 
					p.RecommendID = r.RecommendID 
				WHERE 
					SharedSince <= CURDATE() AND SharedUntil >= CURDATE()";
        
        $recommendPhotoID = $this->returnVector($sql);
        
        $tempArr = array();
        $tempArr['FavPhotoID'] = $favPhotoID;
        $tempArr['RecPhotoID'] = array_values(array_unique($recommendPhotoID));
        
        return $tempArr;
    }

    function AppGetAlbumPhotoComment($PhotoID)
    { // return (RecordID, PhotoID, UserID, Content, InputBy, DateInput, ModifyBy, DateModified)
        $sql = "SELECT 
					c.RecordID,
					c.PhotoID, 
					c.UserID, 
					c.Content, 
					c.InputBy, 
					c.DateInput,
					c.ModifyBy,
					c.DateModified
				FROM 
					INTRANET_DC_ALBUM_PHOTO_COMMENT c
				JOIN
					INTRANET_USER u
				ON
					c.UserID = u.UserID
				WHERE
					PhotoID = '" . $PhotoID . "' 
				ORDER BY 
					DateInput";
        
        return $this->returnResultSet($sql);
    }

    function AppGetAlbumPhotoFavoritesUser($PhotoID)
    { // return (PhotoID, UserID)
        if ($PhotoID) {
            if (! is_array($PhotoID)) {
                $PhotoID = array(
                    $PhotoID
                );
            }
            $cond = " AND PhotoID IN ('" . implode("','", $PhotoID) . "')";
        }
        $sql = "SELECT
		    PhotoID,
		    UserID
		FROM 
			INTRANET_DC_ALBUM_PHOTO_VIEW
 
		WHERE IsFavorite = 1   
			$cond
		ORDER BY 
			DateModified desc";
        
        return $this->returnResultSet($sql);
    }

    function AppUpdateAlbumPhotoFavorites($parUserID, $PhotoID)
    {
        $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO_VIEW SET IsFavorite = IF(IsFavorite,'0','1'), DateModified = NOW() WHERE PhotoID = '$PhotoID' AND UserID = '$parUserID'";
        return $this->db_db_query($sql);
    }

    function AppIsPhotoReadable($parUserID, $PhotoID)
    {
        // if($PhotoID){
        $cond = " AND PhotoID = '" . $PhotoID . "'";
        // }
        $sql = "SELECT
		    AlbumID
		FROM 
			INTRANET_DC_ALBUM_PHOTO 
		WHERE 
			1 $cond";
        $AlbunID = current($this->returnVector($sql));
        
        if ($this->AppIsAlbumReadable($AlbunID, $parUserID)) {
            return true;
        } else {
            return false;
        }
    }
    
    function addAlbumPhotoByApp($album_id, $fileInfo)
    {
        global $intranet_root, $Lang, $file_path;
        
        include_once ($intranet_root . '/includes/libfilesystem.php');
        include_once ($intranet_root. "/includes/libgeneralsettings.php");
        $lgs = new libgeneralsettings();
        $settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'"));
        $lf = new libfilesystem();
        
        $img_path = $fileInfo['fileVar']['path'];
        $size = $fileInfo['fileVar']['file_size'];
        $fileName = $fileInfo['fileName'];
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        
        $videoFormat = libdigitalchannels::$videoFormat;
        
        list($width, $height, $img_type) = getimagesize($img_path);
        
        $imgFormatNotMatch = false;
        switch ($img_type) {
            case 1:     // IMAGETYPE_GIF
                $image = imagecreatefromgif($img_path);
                break;
            case 2:     // IMAGETYPE_JPEG
                $image = imagecreatefromjpeg($img_path);
                break;
            case 3:     // IMAGETYPE_PNG
                $image = imagecreatefrompng($img_path);
                break;
            default:
                $imgFormatNotMatch = true;
        }
        
        if ($image){
            
            $exif = @exif_read_data($img_path);
            
            //orientation handling [Start]
            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3:
                        $image = imagerotate($image, 180, 0);
                        break;
                        
                    case 6:
                        $image = imagerotate($image, -90, 0);
                        $temp_width = $width;
                        $width = $height;
                        $height = $temp_width;
                        break;
                        
                    case 8:
                        $image = imagerotate($image, 90, 0);
                        $temp_width = $width;
                        $width = $height;
                        $height = $temp_width;
                        break;
                }
            }
            //orientation handling [End]
            
            $kis_data['title'] = $fileName;
            $kis_data['date_taken'] = (int)strtotime($exif['DateTime']);
            $kis_data['date_uploaded'] = time();
            
            $photo = array(
                'title'=>htmlspecialchars($kis_data['title']),
                'date'=>$kis_data['date_taken'],
                'size'=>$size,
                'album_id'=>$album_id
            );
            
            $photo_id = $this->Add_Album_Photo($photo);
            
            $album = $this->getAlbum($album_id);
            $photo = $this->getAlbumPhoto($photo_id);
            
            $photo_url 		= libdigitalchannels::getPhotoFileName('photo', $album, $photo);
            $thumbnail_url 	= libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo);
            
            $ext = pathinfo($kis_data['title'], PATHINFO_EXTENSION);
            $original_url 	= libdigitalchannels::getPhotoFileName('original', $album, $photo, $ext);
            
            $resized = libdigitalchannels::getResizedImage($image, $width, $height, libdigitalchannels::$photo_max_size);
            imagejpeg($resized, $file_path.$photo_url, 100);
            imagedestroy($resized);
            
            $thumbnail = libdigitalchannels::getResizedImage($image, $width, $height, libdigitalchannels::$thumbnail_max_size);
            imagejpeg($thumbnail, $file_path.$thumbnail_url, 100);
            imagedestroy($thumbnail);
            
            if($settings['KeepOriginalPhoto']=='1'){
                $lf->lfs_move($img_path, $file_path.$original_url);
            }
            else {
                unlink($img_path);
            }
            
            imagedestroy($image);
            
            $kis_data['thumbnail'] = $thumbnail_url;
            $kis_data['photo_id']  = $photo_id;
            
        }
        # handle video file
        else if(in_array(strtoupper($ext),$videoFormat)){
            $current_ext = '.'.trim($ext);
            
            # Import records
            $kis_data['title'] = $fileName;
            $kis_data['date_uploaded'] = time();
            $photo = array(
                'title'=>htmlspecialchars($kis_data['title']),
                'date'=>$kis_data['date_uploaded'],
                'size'=>$size,
                'album_id'=>$album_id
            );
            
            # Add record to DB
            $photo_id = $this->Add_Album_Photo($photo);
            
            # Get required data
            $album = $this->getAlbum($album_id);
            $photo = $this->getAlbumPhoto($photo_id);
            
            # Get details of video
            $video_details = libdigitalchannels::getVideoDetails($img_path);
            
            # Get target path name of mp4 video
            $photo_url 	= libdigitalchannels::getPhotoFileName('photo', $album, $photo, 'mp4');
            
            # Keep original video
            if($settings['KeepOriginalPhoto']=='1'){
                # Get target path name of original video
                $original_url 	= libdigitalchannels::getPhotoFileName('original', $album, $photo, $ext);
                
                # Store original video
                $original_url = $file_path.$original_url;
                $lf->lfs_move($img_path, $original_url);
                
                # Check if original file exist
                if(!file_exists($original_url)){
                    $kis_data['error'] = $Lang['DigitalChannels']['Remarks']['FailToUpload'].'!';
                    $this->removeAlbumPhoto($photo_id);
                    break;
                }
            } else {
                $original_url = $img_path;
            }
            
            # check the orientation of the video [start]
            $cmd = OsCommandSafe("mediainfo '$original_url' | grep Rotation");
            $ret = @exec($cmd);
            
            $rotate_cmd = '';
            if($ret){
                if(!(strpos($ret, ': 90')===false)){
                    $rotate_cmd = " -vf transpose=1 ";
                }
                else if(!(strpos($ret, ': 180')===false)){
                    $rotate_cmd = " -vf vflip,hflip ";
                }
                else if(!(strpos($ret, ': 270')===false)){
                    $rotate_cmd = " -vf transpose=2 ";
                }
            }
            
            # check the orientation of the video [end]
            
            # Convert to mp4 - H.264/MPEG-4 & FAAC
            if(strpos(trim($video_details['Codec']), 'h264') === false || $rotate_cmd || isset($sys_custom['DigitalChannels']['custBitRate'])){
                $cmd = "/usr/local/bin/ffmpeg -version";
                $ffmpegInfo = @exec($cmd, $version);
                if($version[0] == "ffmpeg 0.7.1"){
                    $acodec = "-acodec libfaac";
                }else{
                    $acodec = "-acodec aac";
                }
                
                if ($sys_custom['DigitalChannels']['custBitRate']) {
                    $bitrate_cmd = "-maxrate ".$sys_custom['DigitalChannels']['custBitRate']." -bufsize ".$sys_custom['DigitalChannels']['custBitRate'];
                }
                
                $cmd_mp4 = OsCommandSafe("/usr/local/bin/ffmpeg -y -i '".$original_url."' $acodec -ar 22050 -f mp4 -vcodec libx264 -preset ultrafast -crf 25 ".$bitrate_cmd." ".$rotate_cmd." '".$file_path.$photo_url."' 2>&1");
                $tmp = exec($cmd_mp4, $encodeContent);
            }
            else {
                copy($original_url, $file_path.$photo_url);
            }
            
            # Check if converted mp4 file exist
            if(!file_exists($file_path.$photo_url)){
                $kis_data['error'] = $Lang['DigitalChannels']['Remarks']['FailToUpload'].'!';
                $this->removeAlbumPhoto($photo_id);
                break;
            }
            
            # Generate thumbnail
            $thumbnail_url = str_replace(".mp4", ".jpg", $photo_url);
            $thumbnail_url = str_replace("/photo/", "/thumbnail/", $thumbnail_url);
            $thumbnail_time = round($video_details['Total_s'] / 2) + 5;
            $cmd_thumbnail = OsCommandSafe("/usr/local/bin/ffmpeg -i '".$original_url."' -vframes 1 -an -ss ".$thumbnail_time." ".$rotate_cmd." '".$file_path.$thumbnail_url."'");
            exec($cmd_thumbnail);
            
            # Set Thumbnail Size
            if(file_exists($file_path.$thumbnail_url)){
                list($width, $height, $img_type) = getimagesize($file_path.$thumbnail_url);
                $image = imagecreatefromjpeg($file_path.$thumbnail_url);
                $thumbnail = libdigitalchannels::getResizedImage($image, $width, $height, libdigitalchannels::$thumbnail_max_size);
                imagejpeg($thumbnail, $file_path.$thumbnail_url, 100);
                imagedestroy($thumbnail);
                imagedestroy($image);
            }
            
            # Remove video in uploaded folder if not keep original photo
            if($settings['KeepOriginalPhoto'] !== '1'){
                unlink($original_url);
            }
            
            $kis_data['thumbnail'] = libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo);
            $kis_data['photo_id']  = $photo_id;
        }
        else{
            if($img_path && $imgFormatNotMatch){
                //display the file not match function
                $kis_data['error'] = $Lang['DigitalChannels']['Organize']['UnsupportedFormat'].'!';
            }
            else{
                //display the file too large function
                $kis_data['error'] = $Lang['DigitalChannels']['Organize']['FileSizeTooLarge'].'!';
            }
        }
        
        return $kis_data;
        
    }   // end addAlbumPhotoByApp

    public function isDCAdmin($parUserID)
    {
        $sql = "SELECT  
                        COUNT(*)
		        FROM 
                        ROLE_RIGHT r
                INNER JOIN 
                        ROLE_MEMBER m 
                        ON r.RoleID = m.RoleID
                WHERE 
                        r.FunctionName = 'eAdmin-DigitalChannels'
                        AND m.UserID IN ('" . implode("','", (array)$parUserID) . "')";
        
        if (current($this->returnVector($sql))) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function AppIsAlbumEditable($album_id, $parUserID)
    {
        ## 1. digital channels admin
        if ($this->isDCAdmin($parUserID)) {
            return true;
        }
        
        ## 2. digitial channels category pic
        $sql = "SELECT  
                        COUNT(*)
		        FROM 
                        INTRANET_DC_CATEGORY_PIC p
			    INNER JOIN 
                        INTRANET_DC_ALBUM a ON a.CategoryCode = p.CategoryCode
		        WHERE   p.PicID IN ('" . implode("','", (array)$parUserID) . "')
                        AND a.AlbumID = '" . $album_id . "'";

        if (current($this->returnVector($sql))) {
            return true;
        }
        
        $cond = '';
        if ($album_id)
            $cond = " AND AlbumID = '" . $album_id . "'";
            
        ## 3. digital channels input user
        $sql = "SELECT COUNT(*)
		        FROM INTRANET_DC_ALBUM
		        WHERE InputBy IN ('" . implode("','", (array)$parUserID). "') " . $cond;

        if (current($this->returnVector($sql))) {
            return true;
        }
        
        ## 4. digital channels album pic
        $sql = "SELECT COUNT(*)
		        FROM INTRANET_DC_ALBUM_PIC
		        WHERE PicID IN ('" . implode("','", (array)$parUserID). "') " . $cond;

        if (current($this->returnVector($sql))) {
            return true;
        }
        
        return false;
    }
    
    public function AppIsAlbumsEditable($albumIdArr, $parUserID)
    {
        ## 1. digital channels admin
        if ($this->isDCAdmin($parUserID)) {
        	$albumsReadable = array();
	        foreach($albumIdArr as $albumId){
	        	$albumsReadable[$albumId] = true;
	        }	
	        return $albumsReadable;
        }
        
        $albumsReadable = array();
        foreach($albumIdArr as $albumId){
        	$albumsReadable[$albumId] = false;
        }
        $albumIDs = implode("','",$albumIdArr);
        
        ## 2. digitial channels category pic
        $sql = "SELECT  
                        a.AlbumID as AlbumID, COUNT(*) as count
		        FROM 
                        INTRANET_DC_CATEGORY_PIC p
			    INNER JOIN 
                        INTRANET_DC_ALBUM a ON a.CategoryCode = p.CategoryCode
		        WHERE   p.PicID IN ('" . implode("','", (array)$parUserID) . "')
                        AND a.AlbumID IN ('" . $albumIDs . "')
				GROUP BY a.AlbumID";

		$result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result[$albumId]) && $result[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false){
                $albumsReadable[$albumId] = true;
            }
        }
        
        $cond = '';
        if ($albumIDs)
            $cond = " AND AlbumID IN ('" . $albumIDs . "')";
            
        ## 3. digital channels input user
        $sql = "SELECT AlbumID, COUNT(*) as count
		        FROM INTRANET_DC_ALBUM
		        WHERE InputBy IN ('" . implode("','", (array)$parUserID). "') " . $cond . " GROUP BY AlbumID";

        $result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result[$albumId]) && $result[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false){
                $albumsReadable[$albumId] = true;
            }
        }
        
        ## 4. digital channels album pic
        $sql = "SELECT AlbumID, COUNT(*) as count
		        FROM INTRANET_DC_ALBUM_PIC
		        WHERE PicID IN ('" . implode("','", (array)$parUserID). "') " . $cond . " GROUP BY AlbumID";

        $result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "AlbumID", array("AlbumID","count"), 1);
        
        foreach($albumIdArr as $albumId){
            if(isset($result[$albumId]) && $result[$albumId]["count"] > 0 && $albumsReadable[$albumId] == false){
                $albumsReadable[$albumId] = true;
            }
        }
        
        return $albumsReadable;
    }
    
    public function AppIsCategoryEditable($categoryCode, $parUserID)
    {
        ## 1. digital channels admin
        if ($this->isDCAdmin($parUserID)) {
            return true;
        }
        
        ## 2. digital channels category pic
        if ($categoryCode) {
            $cond = " AND CategoryCode = '" . $categoryCode . "' ";
        }
        $sql = "SELECT COUNT(*)
		        FROM INTRANET_DC_CATEGORY_PIC
                WHERE PicID IN ('" . implode("','", (array)$parUserID). "') " . $cond;
		    
        if (current($this->returnVector($sql))) {
            return true;
        }

        return false;
    }
    
    public function AppIsCategoriesEditable($categoryCodeArr, $parUserID)
    {
    	$categoriesEditable = array();
        
        ## 1. digital channels admin
        if ($this->isDCAdmin($parUserID)) {
            foreach($categoryCodeArr as $categoryCode){
	        	$categoriesEditable[$categoryCode] = true;
	        }
            return $categoriesEditable;
        }
        
         $categoriesCode = implode("','",$categoryCodeArr);
        
        ## 2. digital channels category pic
        if ($categoriesCode) {
            $cond = " AND CategoryCode IN ('" . $categoriesCode . "') ";
        }
        $sql = "SELECT CategoryCode, COUNT(*) as count
		        FROM INTRANET_DC_CATEGORY_PIC
                WHERE PicID IN ('" . implode("','", (array)$parUserID). "') " . $cond . " GROUP BY CategoryCode";
		    
        $result = $this->returnArray($sql);
        $result = BuildMultiKeyAssoc($result, "CategoryCode", array("CategoryCode","count"), 1);
        
        foreach($categoryCodeArr as $categoryCode){
        	$categoriesEditable[$categoryCode] = false;
            if(isset($result[$categoryCode]) && $result[$categoryCode]["count"] > 0){
                $categoriesEditable[$categoryCode] = true;
            }
        }	

        return $categoriesEditable;
    }
    
    public function AppUserCanUploadPhotos($parUserID='')
    {
        if ($parUserID == '') {
            return '0';
        }
        
        ## 1. digital channels admin
        if ($this->isDCAdmin($parUserID)) {
            return '1';
        }
        
        ## 2. digitial channels category pic
        $sql = "SELECT
                        COUNT(*)
		        FROM
                        INTRANET_DC_CATEGORY_PIC p
			    INNER JOIN
                        INTRANET_DC_ALBUM a ON a.CategoryCode = p.CategoryCode
		        WHERE   p.PicID IN ('" . implode("','", (array)$parUserID) . "')";
        
        if (current($this->returnVector($sql))) {
            return '1';
        }
        
        ## 3. digital channels album pic
        $sql = "SELECT COUNT(*)
	        FROM INTRANET_DC_ALBUM_PIC
	        WHERE PicID IN ('" . implode("','", (array)$parUserID). "') ";
        
        if (current($this->returnVector($sql))) {
            return '1';
        }
        
        return '0';
    }
    
    // ///////////////-----------------function for elcass app [end]--------------------//////////////

    public function updateVodSrc($vod, $photo_id)
    {
        global $UserID;

        $updatedBy = $UserID;
        $sql = "UPDATE INTRANET_DC_ALBUM_PHOTO SET Vod = " . $this->pack_value($vod) . ", ModifyBy = '" . $updatedBy. "'
    			WHERE AlbumPhotoID = '$photo_id'";
        $this->db_db_query($sql);
    }

    public function hasVod($photo_id)
    {
        global $dcvod;
        if (! $dcvod) { return false; }

        $sql = "SELECT Vod FROM INTRANET_DC_ALBUM_PHOTO WHERE AlbumPhotoID = '{$photo_id}'";
        $rs = $this->returnVector($sql);

        return $rs[0];
    }

    public function getVodLink($vod)
    {
        global $intranet_root;
        include_once($intranet_root.'/includes/libvod.php');
        $libvod = new libvod;
        $vod = $libvod->videoInfo($vod);

        return $libvod->getVideoLink($vod[1]);
    }

    public function getVodEmbedded($vod)
    {
        global $intranet_root;
        include_once($intranet_root.'/includes/libvod.php');
        $libvod = new libvod;
        $vod = $libvod->videoInfo($vod);

        return $libvod->getEmbedVimeo($vod[1]);
    }

    public function fromCN()
    {
        global $api_key, $api_url;
        // set IP address and API access key
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //$ip = '1.0.3.255'; //sample cn ip
        if(function_exists('filter_var')){
            if ( ! filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) )
            {
                return false;
            }
        } else {
            if (! validateIP($ip,  FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE )){
                return false;
            }
        }

        $ch = curl_init($api_url.'/api/ip/'.$ip.'/location');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$api_key,));
        $json = curl_exec($ch);
        $result = curl_getinfo($ch);
        curl_close($ch);

        // Decode JSON response:
        $api_result = json_decode($json, true);

        return $api_result['country_code'] == 'CN' || $result['http_code'] == 500 ;
    }
}

if (!defined('FILTER_FLAG_NO_PRIV_RANGE')) {
    define('FILTER_FLAG_NO_PRIV_RANGE', 0x800000);
}
if (!defined('FILTER_FLAG_NO_RES_RANGE')) {
    define('FILTER_FLAG_NO_RES_RANGE', 0x400000);
}
function validateIPv4($str, &$ip, $filters = 0) {
    $n = 0;
    for ($i = 0, $iMax = strlen($str); $i < $iMax; $i++) {
        $c = ord($str{$i});
        if ($c < 48 || $c > 57) {
            return false;
        }
        $leading_zero = $c == 48;
        $m = 1;
        $num = $c - 48; $i++;
        while (1) {
            if ($i >= $iMax) {
                break;
            }
            $c = ord($str{$i});
            if ($c >= 48 && $c <= 57) {
                $num = $num * 10 + $c - 48; $i++;
                if ($num > 255 || (++$m) > 3) {
                    return false;
                }
            } else {
                break;
            }
        }
        if ($leading_zero && ($num != 0 || $m > 1)) {
            return false;
        }
        $ip[$n++] = $num;
        if ($n == 4) {
            if ($i != $iMax) {
                return false;
            } else {
                if ($filters & FILTER_FLAG_NO_PRIV_RANGE) {
                    if (($ip[0] == 10) ||
                        ($ip[0] == 172 && ($ip[1] >= 16 && $ip[1] <= 31)) ||
                        ($ip[0] == 192 && $ip[1] == 168)) {
                        return false;
                    }
                }
                if ($filters & FILTER_FLAG_NO_RES_RANGE) {
                    if (($ip[0] == 0) ||
                        ($ip[0] == 128 && $ip[1] == 0) ||
                        ($ip[0] == 191 && $ip[1] == 255) ||
                        ($ip[0] == 169 && $ip[1] == 254) ||
                        ($ip[0] == 192 && $ip[1] == 0 && $ip[2] == 2) ||
                        ($ip[0] == 127 && $ip[1] == 0 && $ip[2] == 0 && $ip[3] == 1) ||
                        ($ip[0] >= 224 && $ip[0] <= 255)) {
                        return false;
                    }
                }
                return true;
            }
        } else if ($i >= $iMax) {
            return false;
        } if (($c = ord($str{$i})) != 46) {
            return false;
        }
    }
    return false;
}
function validateIPv6($str, $filters = 0) {
    if (!_validateIPv6($str)) {
        return false;
    }
    $len = strlen($str);
    if ($filters & FILTER_FLAG_NO_PRIV_RANGE) {
        if ($len >= 2 && (!strncasecmp("FC", $str, 2) || !strncasecmp("FD", $str, 2))) {
            return false;
        }
    }
    if ($filters & FILTER_FLAG_NO_RES_RANGE) {
        switch ($len) {
            case 0:
            case 1:
                break;
            case 2:
                if (!strcmp("::", $str)) {
                    return false;
                }
                break;
            case 3:
                if (!strcmp("::1", $str) || !strcmp("5f:", $str)) {
                    return false;
                }
                break;
            default:
                if ($len >= 5) {
                    if (!strncasecmp("fe8", $str, 3) ||
                        !strncasecmp("fe9", $str, 3) ||
                        !strncasecmp("fea", $str, 3) ||
                        !strncasecmp("feb", $str, 3)) {
                        return false;
                    }
                }
                if (($len >= 9 &&  !strncasecmp("2001:0db8", $str, 9)) ||
                    ($len >= 2 &&  !strncasecmp("5f", $str, 2)) ||
                    ($len >= 4 &&  !strncasecmp("3ff3", $str, 4)) ||
                    ($len >= 8 &&  !strncasecmp("2001:001", $str, 8))) {
                    return false;
                }
        }
    }
    return true;
}
function _validateIPv6($str) {
    $strlen = strlen($str);
    $compressed = 0;
    $blocks = 0;
    $ip4elm = array();
    if (false === strpos($str, ':')) {
        return false;
    }
    $ipv4 = strpos($str, '.');
    if (false !== $ipv4) {
        while ($ipv4 > 0 && ord($str{$ipv4 - 1}) != 58) {
            $ipv4--;
        }
        if (!validateIPv4(substr($str, $ipv4), $ip4elm)) {
            return false;
        }
        $strlen = $ipv4;
        if ($strlen < 2) {
            return false;
        }
        if (ord($str{$ipv4 - 2}) != 58) {
            $strlen--;
        }
        $blocks = 2;
    }
    $end = $strlen;
    $i = 0;
    while ($i < $end) {
        if (ord($str{$i}) == 58) {
            if (++$i >= $end) {
                return false;
            }
            if (ord($str{$i}) == 58) {
                if ($compressed) {
                    return false;
                }
                $blocks++;
                $compressed = 1;
                if (++$i == $end) {
                    return ($blocks <= 8);
                }
            } else if ($i == 1) {
                return false;
            }
        }
        $n = 0;
        while (($i < $end) &&
            ((($c = ord($str{$i})) >= 48 && $c <= 57) ||
                ($c >= 97 && $c <= 102) ||
                ($c >= 65 && $c <= 70))) {
            $n++;
            $i++;
        }
        if ($n < 1 || $n > 4) {
            return false;
        }
        if (++$blocks > 8) {
            return false;
        }
    }
    return (($compressed && $blocks <= 8) || $blocks == 8);
}
function validateIP($addr, $filters = 0) {
    if (false !== strpos($addr, ":")) {
        if (!validateIPv6($addr, $filters)) {
            return false;
        }
        return $addr;
    } else if (false !== strpos($addr, ".")) {
        $ip = array();
        if (!validateIPv4($addr, $ip, $filters)) {
            return false;
        }
        return $addr;
    } else {
        return false;
    }
}

include_once($intranet_root.'/includes/json.php');


if (!function_exists('json_encode')) {
    function json_encode($data) {
        $json = new JSON_obj();

        return $json->encode($data);
    }

}


if (!function_exists('json_decode')) {
    function json_decode($data) {
        $json = new JSON_obj();

        return $json->decode($data);
    }
}
?>