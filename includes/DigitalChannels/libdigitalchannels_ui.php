<?php
# modifying : 

##### Change Log [Start] #####
#
#   Date:   2020-05-15 (Henry)
#           - modified Include_JS_CSS() to disallow rigth click photo/video if the setting is not allow to download
#
#   Date:   2020-05-12 (Tommy)
#           - modified Include_JS_CSS(), do not use content_25.css when it is customization
#
#   Date:   2020-05-06 (Henry)
#           - change isAlbumReadable() to isAlbumsReadable(), isAlbumEditable() to isAlbumsEditable() 
#
#   Date:   2020-03-09 (Henry) [Case#Y179425]
#           - modified Display_Category_Album_Photo_View(), Display_Category_Album_Photo_View() to hide download button when disallow download video in video player
#
#   Date:   2019-10-03 (Bill)   [2019-1003-1204-18226]
#           - modified Display_Category_Album_New(), Get_Target_Group_Select(), DHL admin can select all Companies > Departments
#
#   Date:   2019-06-14 (Henry)
#           - modified Display_Category_Album_Photo_View() and Display_Recommend_Album_Photo_View() to fix the extension problem when download photo 
#
#   Date:   2019-03-14 (Cameron)
#           - show album title by Lang in Display_Category_Album_Search()
#           - add both Chi and Eng Title and Description in Display_Category_Album_New()
#
#   Date:   2019-03-08 (Cameron)
#           - add specific user(s) in target group(s) option list in Display_Category_Album_New() [case #Y154291]
#           - show album title by Lang in Display_Category_Album_Photo()
#           - show description by Lang in Display_Category_Album_Photo_View(), Display_Recommend_Album_Photo_View()
#
#   Date:   2019-02-23 (Cameron)
#           - fix: should show Chinese album title if Lang is Chinese in Display_Category_Album(), Display_Category_Album_Photo_View(), Display_Recommend_Album_Photo_View()
#   
#   Date:   2019-01-18 (Cameron)
#           - add parameter Disable_AddGroup_Button=1 to PIC 'Select' button in Display_Category_Album_New()
#
#   Date:   2019-01-09 (Cameron)
#           - show image popup description according to session language in Display_Category_Album_Photo(), Display_Recommend_Album_Photo(),
#               Display_Category_Album_Photo_Favorites(), Display_Category_Album_Photo_More(), Display_Category_Album_Photo_Search(), Display_Index_Page()
#
#   Date:   2019-01-08 (Cameron)
#           - add bilingual support for Title, Description, EventTitle and EventDescription in Display_Category_Album_New()
#           - add AlbumCode for SFOC in Display_Category_Album_New()
#
#   Date:   2018-07-30 (Henry)
#           - modified Display_Category_Album_Photo_View() to fix the sequence number problem when delete a photo
#
#   Date:   2018-06-04 (Cameron)
#           - don't show quick add button in Display_Category_Album_Photo() if $sys_custom['DigitalChannels']['showEventTitle'] and
#           $sys_custom['DigitalChannels']['showEventDate'] are not set
#
#   Date:   2018-06-01 (Cameron)
#           - add ImagePath and apply it to Display_Category_Album_Photo_View() and Display_Recommend_Album_Photo_View()
#           - add overloaded function GET_DATE_PICKER(), use jQuery with local variable ($) because following file 
#               home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album_photo_view.php
#               uses $j = $.noConflict(); to take over jQuery 
#
#   Date:   2018-03-15 (Cameron)
#           - add two search fields (EventTitle and EventDate) in advance search pannel Display_Title_Menu()
#   Date:   2018-03-12 (Cameron): 
#           - add quick add button in Display_Category_Album_Photo()
#           - add parameter isQuickAdd to Display_Category_Album_New()
#   Date:   2018-03-09 (Cameron): add eventDate and eventTitle in Display_Category_Album_New()
#	Date:	2017-11-28 (Henry): some wordings update [Case#Z131872]
#	Date:	2017-08-03 (Carlos): $sys_custom['DHL'] modified Get_Target_Group_Select($shared_groups) to use popup window common choose.
#	Date:	2017-08-03 (Ivan): modified Get_Add_Category_Pic_Form() to hide the teaching / non-teaching staff drop down list
#	Date:	2017-06-30 (Carlos) $sys_custom['DHL'] modified Get_Target_Group_Select($shared_groups) to use DHL group selections format.
# 	Date:	20170407 (Henry)
#			- modified Display_Title_Menu() to support all academic year filtering [Case#E114934] 
#	Date:	2017-01-04 (Henry):
#			- support category pic
#	Date:	2016-02-25 (Pun)
#			- Modified Display_Category_Album_Photo_View(), Display_Recommend_Album_Photo_View(), 
#				fixed view photo description cannot view new line
#	Date:	2016-02-25 (Henry)
#			- fixed using real file name when download the file
#	Date:	2016-02-11 (Henry)
#			- modified Display_Category_Album_Photo_View() to fix the problem of sliding big photo
#	Date:	2015-11-26 (Pun)
#			- modified GET_MODULE_OBJ_ARR(), Display_Title_Menu(), Display_Category_Thumbnail(), Display_Category_List() change access right checking from $_SESSION to $ldc->isAdminUser()
#	Date:	2015-11-25 (Pun)
#			- modified Display_Category_Album_Photo_View(), Display_Title_Menu() added support for EJ
#	Date:	2015-07-13 (Henry)
#			- modified Display_Category_Album_Photo_View() to fix the number of comment if not allow user add comment
#	Date:	2015-07-10 (Henry)
#			- modified Display_Index_Page()
#	Date:	2015-05-06 (Henry)
#			- modified Display_Category_List(), Display_Category_Thumbnail() to not to show the empty category for pic
#			- modified Display_Title_Menu() to not to show the empty category
#	Date:	2015-05-06 (Henry)
#			- support MOV, RM, FLV video format
#	Date:	2015-03-03	(Henry)
#			added Display_Recommend_Album_Photo()
#
#	Date:	2015-02-26	(Henry)
#			added Display_Recommend_Album()
#			added Display_Recommend_Album_New()
#			added Recommendation management button at menu bar
#
#	Date:	2015-02-23	(Henry)
#			modified Display_Category_Album_Photo_More to fix the bug when click the latest album
#			added Display_Category_Album_Photo_View_Control()
#
#	Date:	2015-02-18	(Henry)
#			Fixed the height of the image at image view page
#
#	Date:	2015-01-26	(Henry)
#			modified Display_Category_Thumbnail(), Display_Category_Album(), Display_Category_Album_Search(), Display_Category_Album_Photo_More(), Display_Category_Album_Photo_Search()
#
#	Date:	2014-12-18	(Bill)
#			Show download button only if 'AllowDownloadOriginalPhoto' = true
#
#	Date:	2014-12-10	(Bill)
#			Add max file size remarks
#
#	Date:	2014-10-09 	(Henry)
#				- file created
#
###### Change Log [End] ######

//include_once("libdigitalchannels.php");
include_once ($intranet_root . "/includes/DigitalChannels/libdigitalchannels.php");

define("FUNCTION_NEWEST", "ResourceNewest");
define("FUNCTION_FAVOURITE", "ResourceFavourite");
define("FUNCTION_MY_FAVOURITE", "ResourceMyFavourite");
define("FUNCTION_MY_SUBJECT", "ResourceMySubject");
define("FUNCTION_ADVANCE_SEARCH", "AdvanceSearch");
define("FUNCTION_TAG", "Tag");
define("FUNCTION_MORE", "More");

class libdigitalchannels_ui extends interface_html {

	var $Module;
//	var $File_text;
//	var $File_doc;
//	var $File_xls;
//	var $File_ppt;
//	var $File_pdf;
//	var $File_vdo;
//	var $File_image;
//	var $File_sound;
//	var $File_zip;
//	var $HighlightLimit;
//	var $DiaplayDescriptionLengthLimit;
//	var $right_element;
//	var $isFromeAdmin;
	var $ImagePath;
	
	function libdigitalchannels_ui() {
	    global $image_path, $LAYOUT_SKIN;
	    
		$this->Module = "DigitalChannels";
		$this->ImagePath = $image_path."/".$LAYOUT_SKIN;
		
//		$this->DiaplayDescriptionLengthLimit = 100;
//
//		$this->HighlightLimit = 10;
//
//		# for file extension checking
//		$this->FileExtension = array (
//			"txt" => "text",
//			"doc" => "doc",
//			"docx" => "doc",
//			"csv" => "xls",
//			"xls" => "xls",
//			"xlsx" => "xls",
//			"ppt" => "ppt",
//			"pptx" => "ppt",
//			"pdf" => "pdf",
//			"rmvb" => "vdo",
//			"wmv" => "vdo",
//			"avi" => "vdo",
//			"mov" => "vdo",
//			"mp4" => "vdo",
//			"flv" => "vdo",
//			"gif" => "image",
//			"jpg" => "image",
//			"png" => "image",
//			"wav" => "sound",
//			"wma" => "sound",
//			"mp3" => "sound",
//			"rar" => "zip",
//			"xml" => "text",
//			"zip" => "zip"
//		);
//		$this->isFromeAdmin = strpos($_SERVER["SCRIPT_NAME"], "/admin_doc/");
	}

	function GET_MODULE_OBJ_ARR() {
		global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $PrinterIPLincense, $sys_custom, $CurrentPageArr;
		global $CurrentPage;
		parent :: interface_html();
		
		$ldc = new libdigitalchannels();

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass Digital Channels";'."\n";
        $js.= '</script>'."\n";

//		if ($CurrentPageArr['DigitalArchive_SubjectReference']) { # Subject Reference
//			$MODULE_OBJ['title'] = $Lang["DigitalArchive"]["SubjectReference"] . " ";
//		} else { # Admin Document
			$MODULE_OBJ['title'] = "<a href='".$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalChannels/?clearCoo=1'>".$Lang['DigitalChannels']['DigitalChannelsName']."</a>".$js;
//
//			# check whether the scanner to DA service is established
//			$scannerReady = "";
//			if (file_exists($PATH_WRT_ROOT . "rifolder/global.php")) {
//				include_once ($PATH_WRT_ROOT . "rifolder/global.php");
//				if (sizeof($PrinterIPLincense) > 0) {
//					$scannerReady = 1;
//				}
//			}
//			if ($scannerReady) {
//				if ($sys_custom['digital_archive_general_scan_icon']) {
//					$MODULE_OBJ['title'] .= " <img src=\"{$image_path}/{$LAYOUT_SKIN}/digital_archive/scan_ready.png\" width=\"105\" height=\"32\" align=\"absmiddle\">";
//				} else {
//					$MODULE_OBJ['title'] .= " <img src=\"{$image_path}/{$LAYOUT_SKIN}/digital_archive/Ricoh_ready.png\" width=\"105\" height=\"32\" align=\"absmiddle\">";
//				}
//			}
//		}
//
//		//$MODULE_OBJ['title_css'] = "menu_opened";
//		//$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";
//
		# Check if not Admin Page, show logo only
		if ($CurrentPageArr['DigitalChannelsAdminMenu'] == true) { # Show admin menu
			$MODULE_OBJ['title_css'] = "menu_opened";
			//			if($CurrentPageArr['DigitalArchive_SubjectReference']) {
			//				$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_subject_resources.png";
			//				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/DigitalArchive/";
			//			}else{
			$MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_eLibrary.gif";
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalChannels/";
			//			}

			##### Menu #####
			# Current Page Information init		
			//$PageReports_DeleteLog = 0;
			//$PageSettings_AccessRight = 0;
			//$PageReports = 0;
			$PageSettings = 0;
			$PageSettings_BasicSetting = 0;
			$PageSettings_Category = 0;
			//Henry added
			//$MaxFileSize = 0;
			//$FileFormatSettings = 0;
			//$UploadPolicy = 0;
			//End Henry added

			# Current Page Information init
			switch ($CurrentPage) {
//				#-------------- Overview
//				case "Overview" :
//					$PageOverview = 1;
//					break;
//
//					#-------------- Reports
//				case "Reports_UsageSummary" :
//					$PageReports = 1;
//					$PageReports_Usage = 1;
//					break;
//					#-------------- Reports
//				case "Reports_SystemReport" :
//					$PageReports = 1;
//					$PageReports_DeletionLog = 1;
//					break;
//
//					#-------------- group and access right
//				case "Settings_AccessRight" :
//					$PageSettings = 1;
//					$PageSettings_AccessRight = 1;
//					break;
//
//					#-------------- group category
//				case "Settings_GroupCategory" :
//					$PageSettings = 1;
//					$PageSettings_GroupCategory = 1;
//					break;
//
//					//Henry added
//					#-------------- maximum file size
//					/*case "Settings_MaxFileSize":
//						$PageSettings = 1;
//						$MaxFileSize = 1;
//						break;*/
//
//					#-------------- file format settings
//					/*case "Settings_FileFormatSettings":
//						$PageSettings = 1;
//						$FileFormatSettings = 1;
//						break;*/
//
//				case "Settings_UploadPolicy":
//					$PageSettings = 1;
//					$UploadPolicy = 1;
//					break;
//					//Henry end added
					
				case "Settings_BasicSetting":
					$PageSettings = 1;
					$PageSettings_BasicSetting = 1;
					break;
					
				case "Settings_Category":
					$PageSettings = 1;
					$PageSettings_Category = 1;
					break;
			}

			# Menu information
			# Management
			//$MenuArr["Reports"]["Child"]["AdministrativeDocument"] = array($Lang['Menu']['DigitalArchive']['Settings']['AdministrativeDocument'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/reports/del_log/", $PageReports_DeleteLog);

			# Reports
//			$MenuArr["Reports"] = array (
//				$Lang['Menu']['DigitalArchive']['Reports']['Title'],
//				$PATH_WRT_ROOT . "",
//				$PageReports
//			);
//			if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
//				$MenuArr["Reports"]["Child"]["UsageReport"] = array (
//					$Lang['DigitalArchive']['ReportUsage'],
//					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/reports/system_report/usage_summary.php",
//					$PageReports_Usage
//				);
//				$MenuArr["Reports"]["Child"]["DeletionLog"] = array (
//					$Lang['Menu']['DigitalArchive']['Reports']['DeletionLog'],
//					$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/reports/system_report/del_log/index.php?clearCoo=1",
//					$PageReports_DeletionLog
//				);
//			}

			# Settings
			$MenuArr["Settings"] = array (
				$Lang['General']['Settings'],
				"",
				$PageSettings
			);

			if ($ldc->isAdminUser()) {
				
				$MenuArr["Settings"]["Child"]["BasicSetting"] = array(
					$Lang['DigitalChannels']['Settings']['BasicSettings'],
					$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/admin_settings/basic_settings.php',
					$PageSettings_BasicSetting
				);
				
				$MenuArr["Settings"]["Child"]["Category"] = array(
					$Lang['DigitalChannels']['Settings']['Category'],
					$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/admin_settings/category.php',
					$PageSettings_Category
				);
			}

			$MODULE_OBJ['menu'] = $MenuArr;

			# Check if Subject eResources Admin Page
//		}
//		elseif ($CurrentPageArr['DigitalArchiveSubjecteResourcesAdminMenu'] == true) {
//			$MODULE_OBJ['title_css'] = "menu_opened";
//			$MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_digital_archive.png";
//			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/";
//
//			##### Menu Start #####
//			$PageSettings = 0;
//
//			switch ($CurrentPage) {
//
//				#-------------- group and access right
//				case "Settings_Group" :
//					$PageSettings = 1;
//					$PageSettings_Group = 1;
//					break;
//
//			}
//
//			# Settings
//			$MenuArr["Settings"] = array (
//				$Lang['General']['Settings'],
//				"",
//				$PageSettings
//			);
//			//if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive_SubjecteResources']){
//			$MenuArr["Settings"]["Child"]["Group"] = array (
//				$Lang['Menu']['DigitalArchive']['Settings']['AccessRight'],
//				$PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/DigitalArchive/subject_eResources_settings/group/index.php?clearCoo=1",
//				$PageSettings_Group
//			);
//
//			//}
//
//			$MODULE_OBJ['menu'] = $MenuArr;
//			#####  Menu End  #####

		} else { # Show logo only		
			parent :: interface_html("digital_channels_default.html");
//			$MODULE_OBJ['title_css'] = "menu_small";
//			$MODULE_OBJ['CustomLogo'] = $this->printLeftMenu();
			$MODULE_OBJ = array();
		}

		return $MODULE_OBJ;
	}

	function Include_JS_CSS() {
	    global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPageArr, $junior_mck, $sys_custom;

		$x = '		
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.core.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.draggable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.droppable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.selectable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.pack.js"></script>		
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/script.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
		    		<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" media="screen" />
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css"  type="text/css" media="screen">
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
					';
		$msgTop = ($junior_mck > 0)? '35px' : '110px';
		$x .= '<style>
			#system_message_box{ top: '.$msgTop.'; }
		</style>';
		if (!$CurrentPageArr['DigitalChannelsAdminMenu']){
			include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
			$lgs = new libgeneralsettings();
			$settings = $lgs->Get_General_Setting("DigitalChannels", array("'AllowDownloadOriginalPhoto'"));
			//$x .= '<script type="text/javascript" src="/templates/jquery/plupload/plupload.full.js"></script>';
		    if($sys_custom['Project_Custom'] && $sys_custom['KiangWuNursing_Macau']){
		    }else{
			     $x .= '<link href="' . $PATH_WRT_ROOT . 'templates/'.$LAYOUT_SKIN.'/css/content_25.css" rel="stylesheet" type="text/css">';
		    }
			$x .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/'.$LAYOUT_SKIN.'/css/digital_channels.css" type="text/css" />';
			$x .= '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'home/eAdmin/ResourcesMgmt/DigitalChannels/script/photo_thumb.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'home/eAdmin/ResourcesMgmt/DigitalChannels/script/photo.js"></script>
					<script type="text/javascript">  
						$(document).ready(function() {
						    $(\'body\').addClass(\'elib_bg bookshelf_type_01  bookshelf_type_01_sky theme_rainbow\');
						    document.title = \'eClass Digital Channels\';
						
						});
						jQuery(function($){
							$(\'.photo_hover\').mosaic({
								animation	:	\'slide\'
							});
					    });
					    ';
			if(!$settings['AllowDownloadOriginalPhoto']){
						$x .= '$(\'.photo_gallery, .photo_container, .photo_thumb_images, .photo_pic\').live(\'contextmenu\', function(e) {
						    return false;
						});
					    
					    //disable drag and drop of the image
					    $(\'.photo_gallery, .photo_container, .photo_thumb_images, .photo_pic\').live(\'mousedown\', function (e) {
					           return false;
					    });
					';
			}
					$x .= '</script>';
					}
		return $x;
	}
	
	function Display_Title_Menu($params = array(), $enableYearSelection = false){
	    global $Lang, $PATH_WRT_ROOT, $CurrentPage, $UserID, $SchoolYear, $junior_mck, $sys_custom;
		extract($params);
		
		$ldc = new libdigitalchannels();
		$categoryInfoAry = $ldc->Get_Category();
		$numOfCategory = count($categoryInfoAry);
		
		$categoryList = "";
		$categoryByID = array();
		
		if($enableYearSelection){
			$SchoolYear = $SchoolYear!=0?$SchoolYear:Get_Current_Academic_Year_ID();
		}
		
		for($i = 0; $i < $numOfCategory; $i++){
			$categoryByID[$categoryInfoAry[$i]['CategoryCode']] = $categoryInfoAry[$i]['Description'];
			
			$catPhotos = 0;
			$countAlbums = 0;
			
			$catAlbums = $ldc->Get_Album("", $categoryInfoAry[$i]['CategoryCode'], $SchoolYear);
			
			$catAlbumIDs = array();
			foreach($catAlbums as $currentAlbum){
				$catAlbumIDs[] = $currentAlbum['AlbumID'];
			}
			$isAlbumsReadable = $ldc->isAlbumsReadable($catAlbumIDs);
			
			foreach($catAlbums as $currentAlbum){
				if($isAlbumsReadable[$currentAlbum['AlbumID']]){
					$catPhotos += count($ldc->getAlbumPhotos($currentAlbum['AlbumID']));
					$countAlbums++;
				}
			}
			if($countAlbums > 0)
				$categoryList .= '<li><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/categories/category_album.php?CategoryCode='.$categoryInfoAry[$i]['CategoryCode'].'">'.$categoryInfoAry[$i]['Description'].'('.$countAlbums.')</a></li>';
		}
		
 		$x .='<div class="seasonal_deco" id="seasonal_deco_00a"></div>
         <div class="seasonal_deco" id="seasonal_deco_00b"></div>
	 	<div class="module_title module_en">
	           <div class="module_title_icon"><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/?clearCoo=1" title="Digital Channels">&nbsp;</a></div>
	    	  	<div class="module_filter">
	    	
			<form class="search" method="post" action="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/search.php">
			<a class="show_advancesearch" href="#">'.$Lang['DigitalChannels']['General']['AdvanceSearch'] .'</a>
		
			    <input type="input" name="keyword" value="'.intranet_htmlspecialchars($keyword).'" placeholder="'.$Lang['DigitalChannels']['General']['Search'].'"/>
			    <input type="hidden" name="page" value="search" />
	
		    </form>';

 		if (($sys_custom['DigitalChannels']['showEventTitle']) && ($sys_custom['DigitalChannels']['showEventDate'])){
 		    $layerHeight = '260px';
 		}
 		elseif ($sys_custom['DigitalChannels']['showEventTitle']) {
 		    $layerHeight = '220px';
 		}
 		elseif ($sys_custom['DigitalChannels']['showEventDate']) {
 		    $layerHeight = '240px';
 		}
 		else {
 		    $layerHeight = '250px';
 		}
 		
		    $x .= <<<EOF
			<script type="text/javascript">
			$(function(){
                $('.advancesearch').css('height','{$layerHeight}');

			    $('.show_advancesearch').click(function(){
					jQuery('.advancesearch').fadeToggle();
			    });
			    
			    $(".dropdown dt a").click(function() {
			           $(".dropdown dd ul").not($(this).closest(".dropdown").find("dd ul").slideToggle('fast')).hide();
			           return false;
			    });
			    
			    $(document).click(function(e) {
			           var clicked = $(e.target);
				  
			           if (! clicked.parents().hasClass("dropdown"))
			               $(".dropdown dd ul").slideUp('fast');
			    });
			    
			    //$('.advancesearch').draggable({cancel:'dl, input', cursor: "move"});
			    $('.advancesearch .dropdown li a').click(function(){
				
				$(this).parent().addClass('selected').siblings('li').removeClass('selected').parent().slideUp().siblings('input').val($(this).find('.id').html()).closest('.dropdown').find('dt span').html($(this).html());
				
				return false;
			    });
			    $('.advancesearch .formsubbutton').click(function(){
				$('.advancesearch').fadeOut();
			    });
			    
			    $('.advancesearch .dropdown li.selected:first a').click();
			    
			    //for academic year filter
			    $('.AcademicYearID').click(function(){
			    	var academic_year_id = $(this).attr('id').replace("AcademicYearID_", ""); 
			    	$('#SchoolYear').val(academic_year_id);
			    	$('#AcademicYearForm').submit();
			        
			        return false;
			    });
			});
			</script>
EOF;
			$x.='<form class="advancesearch" '.($advance?'':'style="display:none"').' method="post" action="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/search.php?advance=1">
			
    <div class="fields">'.$Lang['DigitalChannels']['General']['AlbumTitle'].':</div>
    <input type="text" name="album_title" class="input" value="'.intranet_htmlspecialchars($album_title).'">
    
    <p class="spacer"></p>
    
    <div class="fields">'.$Lang['DigitalChannels']['General']['AlbumDescription'].':</div>
    <input type="text" name="album_description" class="input" value="'.intranet_htmlspecialchars($album_description).'">
    
    <p class="spacer"></p>
	
	<div class="fields">'.$Lang['DigitalChannels']['General']['PhotoDescription'].':</div>
    <input type="text" name="photo_description" class="input" value="'.intranet_htmlspecialchars($photo_description).'">';
    
    if ($sys_custom['DigitalChannels']['showEventTitle']) {
        $x .= '<p class="spacer"></p>';
        $x .= '<div class="fields">'.$Lang['DigitalChannels']['Organize']['EventTitle'].':</div>
                <input type="text" name="photo_event_title" class="input" value="'.intranet_htmlspecialchars($photo_event_title).'">';
    }

    if ($sys_custom['DigitalChannels']['showEventDate']) {
        $x .= '<p class="spacer"></p>';
        $x .= '<div class="fields">'.$Lang['DigitalChannels']['Organize']['EventDate'].':</div>';
        
        $AcademicYearID = Get_Current_Academic_Year_ID();
        $photo_event_date_from =  $photo_event_date_from ? $photo_event_date_from: substr(getStartDateOfAcademicYear($AcademicYearID), 0, 10);
        $photo_event_date_to =  $photo_event_date_to ? $photo_event_date_to: date('Y-m-d');
        
        $x .= '<label for="photo_event_date_from">'.$Lang['General']['From'].'</label> '.$this->GET_DATE_PICKER("photo_event_date_from",$photo_event_date_from);
        $x .= '<br><label for="photo_event_date_to">'.$Lang['General']['To'].'</label> '.$this->GET_DATE_PICKER("photo_event_date_to",$photo_event_date_to).'</label>';
    }
    
	$x .= '<p class="spacer"></p>    

    <div class="fields">'.$Lang['DigitalChannels']['General']['Type'].':</div>
    <dl class="dropdown">
	<dt><a href="#"><span>
	    '.($type == 'album' || !$type?$Lang['DigitalChannels']['General']['Album2']:$Lang['DigitalChannels']['General']['PhotoVideo']).'
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
	     ';

	     $x.='<li '.($type=='album' || !$type?'class="selected"':'').'><a href="#">'.$Lang['DigitalChannels']['General']['Album2'].'<span class="id" style="display:none;">album</span></a></li>';
			$x.='<li '.($type=='photo'?'class="selected"':'').'><a href="#">'.$Lang['DigitalChannels']['General']['PhotoVideo'].'<span class="id" style="display:none;">video</photo></a></li>';
		
	    $x.='</ul>
	    <input type="hidden" value="'.$type.'" name="type"/>
	</dd>
    </dl>	

    <p class="spacer"></p>    

    <div class="fields">'.$Lang['DigitalChannels']['Settings']['Category'].':</div>
    <dl class="dropdown">
	<dt><a href="#"><span>
	    '.($category_id?$categoryByID[$category_id]:$Lang['DigitalChannels']['General']['All']).'
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
	     <li '.(!$category_id?'class="selected"':'').'><a href="#">'.$Lang['DigitalChannels']['General']['All'].'<span class="id" style="display:none;"></span></a></li>';
	foreach ($categoryInfoAry as $aCategory) :
	       $x.='<li '.($category_id==$aCategory['CategoryCode']?'class="selected"':'').'><a href="#">'.$aCategory['Description'].'<span class="id" style="display:none;">'.$aCategory['CategoryCode'].'</span></a></li>';
	endforeach;
	    $x.='</ul>
	    <input type="hidden" value="'.$category_id.'" name="category_id"/>
	</dd>
    </dl>

	<p class="spacer"></p>    

    <div class="fields">'.$Lang['DigitalChannels']['General']['FileType'].':</div>
    <dl class="dropdown">
	<dt><a href="#"><span>
	    '.($file_type == 'video'?$Lang['DigitalChannels']['General']['Video']:($file_type == 'photo'?$Lang['DigitalChannels']['General']['Photo']:$Lang['DigitalChannels']['General']['All'])).'
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
	     <li '.(!$file_type?'class="selected"':'').'><a href="#">'.$Lang['DigitalChannels']['General']['All'].'<span class="id" style="display:none;"></span></a></li>';

	     $x.='<li '.($file_type=='photo'?'class="selected"':'').'><a href="#">'.$Lang['DigitalChannels']['General']['Photo'].'<span class="id" style="display:none;">photo</span></a></li>';
			$x.='<li '.($file_type=='video'?'class="selected"':'').'><a href="#">'.$Lang['DigitalChannels']['General']['Video'].'<span class="id" style="display:none;">video</span></a></li>';
		
	    $x.='</ul>
	    <input type="hidden" value="'.$file_type.'" name="file_type"/>
	</dd>
    </dl>
    
    <div class="edit_bottom">
	<input type="submit" class="formbutton" value="'.$Lang['DigitalChannels']['General']['Search'].'">
	<input type="button" value="'.$Lang['DigitalChannels']['General']['Cancel'].'" class="formbutton">
    </div>    
</form>';

	if($enableYearSelection){
		
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$fcm = new form_class_manage();
		$SchoolYearList = $fcm->Get_Academic_Year_List();
		
		if($junior_mck > 0){ // For EJ
			foreach($SchoolYearList as $i => $r){
				$SchoolYearList[$i]['YearNameB5'] = convert2unicode($r['YearNameB5']);
			}
		}
		
		$AllSchoolYear['AcademicYearID'] = -1;
		array_unshift($SchoolYearList, $AllSchoolYear);
		
		$SchoolYear = $SchoolYear!=0?$SchoolYear:Get_Current_Academic_Year_ID();
	
		$x.='<div class="select_book_type">
			    <dl class="dropdown" id="sample">
				<dt><a href="#"><span>';
					for ($i=0; $i< sizeof($SchoolYearList); $i++) {
						if ($SchoolYear==$SchoolYearList[$i]['AcademicYearID']){
							if($SchoolYearList[$i]['AcademicYearID'] == -1){
								$x.=$Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'];
							}
							else{
								$x.=Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
							}
							break;
						}
					}
				$x.='</span></a></dt>
				<dd>
				    <ul style="display: none;">';
				    
				    for ($i=0; $i< sizeof($SchoolYearList); $i++) {
				    	$x.='<li '.(($SchoolYear==$SchoolYearList[$i]['AcademicYearID']) ? 'class="selected"' : "").'>
						    <a href="#" class="AcademicYearID" id="AcademicYearID_'.$SchoolYearList[$i]['AcademicYearID'].'">'.($SchoolYearList[$i]['AcademicYearID'] == -1?$Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear']:Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN'])).'</a>
							</li>';
				    }
				    $x.='</ul>
				</dd>
			  </dl>
			</div><form id="AcademicYearForm"  method="post" action=""><input type="hidden" id="SchoolYear" name="SchoolYear" value="'.$SchoolYear.'" /></form>';
	}
	        $x.='<p class="spacer"></p>
	        </div>
	    	  <div class="digital_channels_menu">
	            <ul>
	              <li '.($CurrentPage=='Categories'?'class="selected"':'').' onmouseover="MM_showHideLayers(\'cat_list\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'cat_list\',\'\',\'hide\')"><div><a href="#" class="menu_icon_cat">'.$Lang['DigitalChannels']['Category']['Title'].'</a>
	              </div>
	               </li>
	               
	               <li '.($CurrentPage=='Favorites'?'class="selected"':'').'><div><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/favorites/category_album_photo_favorites.php" class="menu_icon_favorites">'.$Lang['DigitalChannels']['Favorites']['Title'].'</a></div></li>';
	      if ($ldc->isCategoryPic() || count($ldc->Get_Albums_By_Pic($UserID)) > 0 || $ldc->isAdminUser()) {   
	          $x .='<li '.($CurrentPage=='Organize'?'class="selected"':'').'><div><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_thumbnail.php" class="menu_icon_organize">'.$Lang['DigitalChannels']['Organize']['Title'].'</a></div></li>'; 
		  }
	      if ($ldc->isAdminUser()) {
	      	  $x .='<li '.($CurrentPage=='Recommend'?'class="selected"':'').'><div><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_recommend_album.php" class="menu_icon_organize">'.$Lang['DigitalChannels']['Recommend']['Title'].'</a></div></li>'; 
		  
		      $x .='<li>
		              <div><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/admin_settings/basic_settings.php" class="menu_icon_admin_setting">'.$Lang['DigitalChannels']['Settings']['Title'].'</a></div>
		            </li>';
	      }
	      $x .='</ul>    
	    	    <p class="spacer"></p>
	  	    </div>
	    	</div>
		  <div class="book_cat_filter_board" id="cat_list" onmouseover="MM_showHideLayers(\'cat_list\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'cat_list\',\'\',\'hide\')" style="overflow-y: auto; min-height: 0px;">
		                       
		                       
		                	
		                        <div class="cat_list">
		                        <div class="cat_item_list" >
		                        	
		                            <ul>
		                                '.$categoryList.'
		                                 <p class="spacer"></p>   
		                            </ul>
		                        </div>
		                
		                   		
		                        <p class="spacer"></p>
		                        </div>
		                    </div>';
		return $x;	                     
	}
	
	function Display_Category_Thumbnail($SchoolYear = 0){
		global $Lang, $PATH_WRT_ROOT;
		
		$ldc = new libdigitalchannels();
		
		$categoryInfoAry = $ldc->Get_Category();
		//debug_pr($categoryInfoAry);
		$numOfCategory = 0;
		
  		foreach($categoryInfoAry as $aCategoryInfo){
  			$albumInfoAry = $ldc->Get_Album('',$aCategoryInfo['CategoryCode'],$SchoolYear);
  			
  			//remove the album that the user cannot editable
  			
  			$albumIDs = array();
			foreach($albumInfoAry as $aAlbumInfo){
				$albumIDs[] = $aAlbumInfo['AlbumID'];
			}
			$isAlbumsEditable = $ldc->isAlbumsEditable($albumIDs);
  			
			$tempAlbumInfoAry = array();
			foreach($albumInfoAry as $aAlbumInfo){
				if(!$isAlbumsEditable[$aAlbumInfo['AlbumID']]){
	        		continue;
	        	}
	        	$tempAlbumInfoAry[] = $aAlbumInfo;
			}
			$albumInfoAry = $tempAlbumInfoAry;
			
			//for pic, do not show the empty category
			if(!$ldc->isCategoryPic($aCategoryInfo['CategoryCode']) && !$ldc->isAdminUser() && count($albumInfoAry) <= 0){
				continue;
			}
			
			$numOfCategory++;
			
  			$x .= '<li>
                      <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album.php?CategoryCode='.$aCategoryInfo['CategoryCode'].'">
                      <span class="add_ablum_block">';
            
            $count = 0;
            
            //get the four cover photo
            foreach($albumInfoAry as $aAlbumInfo){ 
            	if($count < 4){    
		            $ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$aAlbumInfo['AlbumID']);
		            
		            //$album = $ldc->getAlbum($aAlbumInfo['AlbumID']);
		            
		            # add path to the photo array
					$album = array(
					    "id" => $aAlbumInfo['AlbumID'],
					    "input_date" => strtotime($aAlbumInfo['DateInput']),
					    "input_by" => $aAlbumInfo['InputBy']
					);
		        
					$coverPhotoInfo = current($ablumPhotoInfoAry);
					foreach($ablumPhotoInfoAry as $aPhoto){
						if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
							$coverPhotoInfo = $aPhoto;
						}
					}
					if($coverPhotoInfo){		          
	                	$x .='<span class="add_ablum"><img src="'.libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo).'"/ class="add_ablum_images"></span>';
	            		$count++;
	            	}
            	}
            }
//            $date1 = $aCategoryInfo['DateModified'];
//			$ts1 = strtotime($date1);
//			$ts2 = strtotime(date('Y-m-d'));
//			$seconds_diff = $ts2 - $ts1;
//			$interval = floor($seconds_diff/3600/24) + 1;
			
            $x .='</span>
                  <div>'.$aCategoryInfo['Description'].'<span class="photo_number">('.count($albumInfoAry).')</span>
                  <!--<span class="photo_date">'.$interval.' '.$Lang['DigitalChannels']['General']['DaysAgo'].'</span>-->                      
				  </div>
                 <!--<em>New</em>-->
				</a></li>';
  		}
  		
	     $x.='</ul><p class="spacer"></p>';
         
         $x = '<div class="album_content">
              	<div class="Content_tool">'.sprintf($Lang['DigitalChannels']['General']['CategoriesInTotal'], $numOfCategory).' </div>  
              	<div class="toggle_tool">'.$Lang['DigitalChannels']['General']['ViewBy'].'
                   <ul>
                   <li ><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_list.php" class="view_list"></a></li>
                   <li class="selected"> <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_thumbnail.php" class="view_thumb_mail"></a></li>
                   </ul>    
                </div>
				<p class="spacer"></p>
				<ul class="album_list">'.$x;
                   	  
         return $x;
	}
	
	function Display_Category_List($order='', $sortBy='', $SchoolYear=0){
		global $Lang, $PATH_WRT_ROOT;
		
		$ldc = new libdigitalchannels();
		
		$categoryInfoAry = $ldc->Get_Category();
		$numOfCategory = count($categoryInfoAry);

  		$x = '<div class="table_board">	   
	    			<table class="common_table_list">
					  <col nowrap="nowrap" style="width:5%"/>
				      <col nowrap="nowrap" style="width:15%"/>
				      <col nowrap="nowrap" style="width:10%"/>
				      <!--<col nowrap="nowrap" style="width:15%"/>-->
					  	<thead>
					    <tr>
					  		<th class="num_check" style="width:5%">#</th>
							<th style="width:20%"><a href=\'#\'';
	//							if ($sortBy=='category'){
//							  $x .= "href='#' onclick=\"sortBookList('category', '".(($order=='asc')? 'desc': 'asc')."'); return false;\" class=\"sort_".$order."\"";
//							} else {
//							   $x .= "href='#' onclick=\"sortBookList('category', 'asc'); return false;\"";
//							}
				  
						$x .= '>'.$Lang['DigitalChannels']['Organize']['Category'].'</a></th>
					  		<th style="width:10%"><a ';
//				if ($sortBy=='number'){
//				  $x .= "href='#' onclick=\"sortBookList('number', '".(($order=='asc')? 'desc': 'asc')."'); return false;\" class=\"sort_".$order."\"";
//				} else {
//				   $x .= "href='#' onclick=\"sortBookList('number', 'asc'); return false;\"";
//				}
						$x .= '>'.$Lang['DigitalChannels']['Organize']['AlbumNum'].'</a></th>
					  		<!--<th style="width:20%"><a ';
//				if ($sortBy=='datecreate'){
//				  $x .= "href='#' onclick=\"sortBookList('datecreate', '".(($order=='asc')? 'desc': 'asc')."'); return false;\" class=\"sort_".$order."\"";
//				} else {
//				   $x .= "href='#' onclick=\"sortBookList('datecreate', 'asc'); return false;\"";
//				}
				    $x .= '>'.$Lang['DigitalChannels']['Organize']['CreateDate'].'</a></th>--></tr>';
				  $x .= '	</thead>
	  					<tbody>';

				if($numOfCategory > 0){
					$count = 1;
				
				    foreach($categoryInfoAry as $aCategoryInfo){
	  					$albumInfoAry = $ldc->Get_Album('',$aCategoryInfo['CategoryCode'],$SchoolYear);
	  					
	  					//remove the album that the user cannot editable
	  					$albumIDs = array();
						foreach($albumInfoAry as $aAlbumInfo){
							$albumIDs[] = $aAlbumInfo['AlbumID'];
						}
						$isAlbumsEditable = $ldc->isAlbumsEditable($albumIDs);
			
	  					$tempAlbumInfoAry = array();
						foreach($albumInfoAry as $aAlbumInfo){
							if(!$isAlbumsEditable[$aAlbumInfo['AlbumID']]){
				        		continue;
				        	}
				        	$tempAlbumInfoAry[] = $aAlbumInfo;
						}
						$albumInfoAry = $tempAlbumInfoAry;
				    	
				    	//for pic, do not show the empty category
						if(!$ldc->isCategoryPic($aCategoryInfo['CategoryCode']) && !$ldc->isAdminUser() && count($albumInfoAry) <= 0){
							continue;
						}
				    	
//				    	$date1 = $aCategoryInfo['DateModified'];
//						$ts1 = strtotime($date1);
//						$ts2 = strtotime(date('Y-m-d'));
//						$seconds_diff = $ts2 - $ts1;
//						$interval = floor($seconds_diff/3600/24) + 1;
				    	
					    $x .= '
					    	<tr>
						    	<td>'.$count++.'</td>
						    	<td>
									<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album.php?CategoryCode='.$aCategoryInfo['CategoryCode'].'" >'.$aCategoryInfo['Description'].'</a>
								</td>
								<td>'.count($albumInfoAry).'</td>
								<!--<td>'.$interval.' '.$Lang['DigitalChannels']['General']['DaysAgo'].'</td>-->
					      	</tr>';
				    } 
				}
				else 
				{
				   	$x .= '	<tr>
				   				<td colspan="4" style="text-align: center;">'.$Lang['DigitalChannels']['General']['NoRecord'].'</td>
				   			</tr>';
				}
	  		$x .= '		<tbody>			
     		 		</table>
  				</div>		     

				<p class="spacer"></p>';
  		
//  			$albumInfoAry = $ldc->Get_Album('',$aCategoryInfo['CategoryCode']);
//  			$x .= '<li>
//                      <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album.php?CategoryCode='.$aCategoryInfo['CategoryCode'].'">
//                      <span class="add_ablum_block">';
//            
//            $count = 0;
//            
//            //get the four cover photo
//            foreach($albumInfoAry as $aAlbumInfo){ 
//            	if($count < 4){    
//		            $ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$aAlbumInfo['AlbumID']);
//		            
//		            $album = $ldc->getAlbum($aAlbumInfo['AlbumID']);
//		        
//					$coverPhotoInfo = current($ablumPhotoInfoAry);
//					foreach($ablumPhotoInfoAry as $aPhoto){
//						if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
//							$coverPhotoInfo = $aPhoto;
//						}
//					}
//					if($coverPhotoInfo){		          
//	                	$x .='<span class="add_ablum"><img src="'.libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo).'"/></span>';
//	            		$count++;
//	            	}
//            	}
//            }
//            
//            $x .='</span>
//                  <div>'.$aCategoryInfo['Description'].'<span class="photo_number">('.count($albumInfoAry).')</span>
//                  <span class="photo_date">2 days ago</span>                      
//				  </div>
//                 <!--<em>New</em>-->
//				</a></li>';

				

//  		}
  		
	     $x.='<p class="spacer"></p>';
         
         		
		$x = '<div class="album_content">
              	<div class="Content_tool">'.sprintf($Lang['DigitalChannels']['General']['CategoriesInTotal'], ($count-1)).' </div>  
              	<div class="toggle_tool">'.$Lang['DigitalChannels']['General']['ViewBy'].'
	                <ul>
	                   <li class="selected"><a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_list.php" class="view_list"></a></li>
	                   <li> <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_thumbnail.php" class="view_thumb_mail"></a></li>
	                </ul>    
                </div>
				
				<p class="spacer"></p>'.$x;
                   	  
         return $x;
	}
	
	
	function Display_Category_Album($CategoryCode = '', $ViewMode = false, $SchoolYear = 0){
		global $PATH_WRT_ROOT, $Lang;
		
		$ldc = new libdigitalchannels();
		
		$categoryInfo = current($ldc->Get_Category($CategoryCode));
		
		$albumInfoAry = $ldc->Get_Album('',$CategoryCode, $SchoolYear);
		
		if($ViewMode){
			
			$albumIDs = array();
			foreach($albumInfoAry as $aAlbumInfo){
				$albumIDs[] = $aAlbumInfo['AlbumID'];
			}
			$isAlbumsReadable = $ldc->isAlbumsReadable($albumIDs);
			
			$tempAlbumInfoAry = array();
			foreach($albumInfoAry as $aAlbumInfo){
				if($ViewMode && !$isAlbumsReadable[$aAlbumInfo['AlbumID']]){
	        		continue;
	        	}
	        	$tempAlbumInfoAry[] = $aAlbumInfo;
			}
			$albumInfoAry = $tempAlbumInfoAry;
		}
		if(!$ViewMode){
			$albumIDs = array();
			foreach($albumInfoAry as $aAlbumInfo){
				$albumIDs[] = $aAlbumInfo['AlbumID'];
			}
			$isAlbumsEditable = $ldc->isAlbumsEditable($albumIDs);
			
			$tempAlbumInfoAry = array();
			foreach($albumInfoAry as $aAlbumInfo){
				if(!$ViewMode && !$isAlbumsEditable[$aAlbumInfo['AlbumID']]){
	        		continue;
	        	}
	        	$tempAlbumInfoAry[] = $aAlbumInfo;
			}
			$albumInfoAry = $tempAlbumInfoAry;
		}
		
		//debug_pr($categoryInfoAry);
		$numOfAlbum = count($albumInfoAry);
		
		$x .='<div class="album_content">
                
                    <div class="navigation_bar"><span>'.$categoryInfo['Description'].'<em>'.sprintf($Lang['DigitalChannels']['General']['AlbumsInTotal'], $numOfAlbum).'</em></span></div>
                      <p class="spacer"></p>


                        
                             <ul class="album_list">';
		if(!$ViewMode && $ldc->isAlbumNewable($aAlbumInfo['CategoryCode'])){	
			$x .='<li> 
                 <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album_new.php?CategoryCode='.$CategoryCode.'&isNew=1" class="new_categories"><span>'.$Lang['DigitalChannels']['Organize']['NewAlbum'].' </span>
                 <span class="add"></span> 
                  <div></div>
                  </a> </li>';
		}
		
        foreach($albumInfoAry as $aAlbumInfo){
//        	if($ViewMode && !$ldc->isAlbumReadable($aAlbumInfo['AlbumID'])){
//        		continue;
//        	}
        	$ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$aAlbumInfo['AlbumID']);
        	$numOfPhoto = count($ablumPhotoInfoAry);
			//$album = $ldc->getAlbum($aAlbumInfo['AlbumID']);
			
			# add path to the photo array
			$album = array(
			    "id" => $aAlbumInfo['AlbumID'],
			    "input_date" => strtotime($aAlbumInfo['DateInput']),
			    "input_by" => $aAlbumInfo['InputBy']
			);
			
			//get cover photo info
			$coverPhotoInfo = current($ablumPhotoInfoAry);
			foreach($ablumPhotoInfoAry as $aPhoto){
				if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
					$coverPhotoInfo = $aPhoto;
				}
			}
			
			$date1 = $aAlbumInfo['DateTaken'];
			$ts1 = strtotime($date1);
			$ts2 = strtotime(date('Y-m-d'));
			$seconds_diff = $ts2 - $ts1;
			$interval = floor($seconds_diff/3600/24);
			$intervalMsg = $interval.' '.$Lang['DigitalChannels']['General']['DaysAgo'];
			if($date1 == '0000-00-00' || !$date1){
					$intervalMsg = '';
				}
//			if($interval < 0){
//				$intervalMsg = date('Y-m-d', $date1);
//			}
			
			$albumTitle = Get_Lang_Selection($aAlbumInfo['ChiTitle'], $aAlbumInfo['Title']);
				
        	$x .='<li>';
        			if(!$ViewMode){
                    	$x .='<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album_photo.php?AlbumID='.$aAlbumInfo['AlbumID'].'">';
        			}
        			else{
        				$x .='<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/categories/category_album_photo.php?AlbumID='.$aAlbumInfo['AlbumID'].'">';
        			}
                     if($numOfPhoto > 0){
                     $x .='<span class="photo_pic"> <img src="'.libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo).'"/></span>';
                     }
                     $x .='<div>'.($albumTitle?$albumTitle:'('.$Lang['DigitalChannels']['Organize']['UntitledAlbum'].')').'<span class="photo_number">('.count($ablumPhotoInfoAry).')</span>
                      <span class="photo_date">'.$intervalMsg.'</span>                      </div>
                     <!--<em>New</em>-->                      </a>                      </li>';

        }
                             
                      $x.='</ul>
                   	  <p class="spacer"></p>

		</div>';
		return $x;
	}
	
	function Display_Recommend_Album($SchoolYear = 0){
		global $PATH_WRT_ROOT, $Lang;
		
		$ldc = new libdigitalchannels();
		
		$albumInfoAry = $ldc->Get_Recommend_Album('', $SchoolYear);
		
		$numOfAlbum = count($albumInfoAry);
		
		$x .='<div class="album_content">            
                    <div class="navigation_bar"><span><em>'.sprintf($Lang['DigitalChannels']['Recommend']['ReommendationInTotal'], $numOfAlbum).'</em></span></div>
                      <p class="spacer"></p>
                         <ul class="album_list">';
		if($ldc->isAlbumNewable()){	
			$x .='<li> 
                 <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_recommend_album_new.php?isNew=1" class="new_categories"><span>'.$Lang['DigitalChannels']['Recommend']['NewReommendation'].' </span>
                 <span class="add"></span> 
                  <div></div>
                  </a> </li>';
		}
		
        foreach($albumInfoAry as $aAlbumInfo){
//        	if($ViewMode && !$ldc->isAlbumReadable($aAlbumInfo['AlbumID'])){
//        		continue;
//        	}
        	$ablumPhotoInfoAry = $ldc->Get_Recommend_Album_Photo($aAlbumInfo['RecommendID']);
        	
        	$numOfPhoto = count($ablumPhotoInfoAry);
        	$currentPhotoInfo = current($ablumPhotoInfoAry);
			$album = $ldc->getAlbum($currentPhotoInfo['album_id']);
			
			# add path to the photo array
//			$album = array(
//			    "id" => $aAlbumInfo['RecommendID'],
//			    "input_date" => strtotime($aAlbumInfo['DateInput']),
//			    "input_by" => $aAlbumInfo['InputBy']
//			);
			
			//get cover photo info
			$coverPhotoInfo = current($ablumPhotoInfoAry);
			foreach($ablumPhotoInfoAry as $aPhoto){
				if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
					$coverPhotoInfo = $aPhoto;
				}
			}
			
			$date1 = $aAlbumInfo['DateInput'];
			$ts1 = strtotime($date1);
			$ts2 = strtotime(date('Y-m-d'));
			$seconds_diff = $ts2 - $ts1;
			$interval = floor($seconds_diff/3600/24)+1;
			$intervalMsg = $interval.' '.$Lang['DigitalChannels']['General']['DaysAgo'];
			if($date1 == '0000-00-00' || !$date1){
					$intervalMsg = '';
				}
//			if($interval < 0){
//				$intervalMsg = date('Y-m-d', $date1);
//			}
			
        	$x .='<li>';

                    $x .='<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_recommend_album_photo.php?RecommendID='.$aAlbumInfo['RecommendID'].'">';

                     if($numOfPhoto > 0){
                     $x .='<span class="photo_pic"> <img src="'.libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo).'"/></span>';
                     }
                      $x .='<div>'.($aAlbumInfo['Title']?$aAlbumInfo['Title']:'('.$Lang['DigitalChannels']['Organize']['UntitledAlbum'].')').'<span class="photo_number">('.count($ablumPhotoInfoAry).')</span>
                      <span class="photo_date">'.$intervalMsg.'</span>                      </div>
                     <!--<em>New</em>-->                      </a>                      </li>';

        }
                             
                      $x.='</ul>
                   	  <p class="spacer"></p>

		</div>';
		return $x;
	}
	
	function Get_Category_Selection($CategoryCode){
		$ldc = new libdigitalchannels();
		$categoryInfoAry = $ldc->Get_Category();
		$disabled = ($ldc->isAdminUser()?"":"disabled");
		$CategorySelection = "<select name='CategoryCode' id='CategoryCode' >\n";
		//$CategorySelection .= "<option value=\"\">--- " . $Lang['DigitalArchive']['AllCategories'] . " ---</option>\n";
		for ($i = 0; $i < sizeof($categoryInfoAry); $i++) {
			$ID = $categoryInfoAry[$i]['CategoryCode'];
			$Name = $categoryInfoAry[$i]['Description'];
			$SelectedStr = ($CategoryCode == $ID) ? "SELECTED" : "";
			$CategorySelection .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
		}
		$CategorySelection .= "</select>\n";
		
		if(!$ldc->isAdminUser()){
			$CategorySelection .= "<input type='hidden' name='CategoryCode' value='".$CategoryCode."' />";
		}
		
		return $CategorySelection;
	}
	
	function Display_Category_Album_New($AlbumID, $CategoryCode, $isQuickAdd=false) {
	    global $PATH_WRT_ROOT, $Lang, $sys_custom, $intranet_session_language;
		global $linterface;
		
		$ldc = new libdigitalchannels();
		if($AlbumID > 0)
			$albumInfo = current($ldc->Get_Album($AlbumID,''));
		else{
			$params = array(
				'CategoryCode'=>$CategoryCode,
				'date'=>date('Y-m-d')
			);
			$AlbumID = $ldc->Add_Album($params);
			$albumInfo = current($ldc->Get_Album($AlbumID,''));
		}
			
		$categoryInfoAry = $ldc->Get_Category();
		
		# Get PICS of this album
		$array_PICs = $ldc->getAlbumPICs($AlbumID);
		
		// [2019-1003-1204-18226]
		if($sys_custom['DHL'])
		{
		    # DHL Digital Channels - admin can view all Companies > Departments
            $urlParms = $ldc->isAdminUser()? '&isAdmin=1' : '';
		}
		
		if ($intranet_session_language == 'en') {
		    $albumTitleCss = 'album_form_title_eng';
		}
		else {
		    $albumTitleCss = 'album_form_title';
		}
		
		$x .= '<form id="form1" name="form1" class="newalbum" method="post" onsubmit="return checkForm();" action="admin_category_album_update.php">';
			$x .= '<div class="album_content">
                    	<div class="album_form">
                            <div class="'.$albumTitleCss.'" >'.$Lang['DigitalChannels']['Organize']['Category'].' :</div>';
					        $x .= $this->Get_Category_Selection($albumInfo['CategoryCode']);
//                     <dl  class="dropdown" style="float:left; margin-bottom:15px;">
//              <dt><a href="#">Music</a></dt>
//              <dd>
//                <ul>
//                  <li class="selected"><a href="#">Sports</a></li>
//                  <li><a href="#">Science</a></li>
//                  <li><a href="#">History</a></li>
//                  <li><a href="#">Cookery</a></li>
//                   <li><a href="#">Computer</a></li>
//                </ul>
//              </dd>
//            </dl>

		if (($sys_custom['DigitalChannels']['showEventTitle']) || ($sys_custom['DigitalChannels']['showEventDate'])){
		    $x .= '<p class="spacer"></p>
               		<div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['AlbumCode'].' :</div>
                        <input type="text" name="albumCode" size="30" placeholder="'.$Lang['DigitalChannels']['Organize']['AlbumCode'].'..." class="" value="'.intranet_htmlspecialchars($albumInfo['AlbumCode']).'" />';
		}
                     $x .= '<p class="spacer"></p>
                       		<div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['AlbumTitleEng'].' :</div> 
                                <input type="text" name="title" size="30"  placeholder="'.$Lang['DigitalChannels']['Organize']['AlbumEngTitle'].'..." class="album_form_bilingual_input" value="'.$albumInfo['Title'].'" />
                           	    <div class="album_form_date_access">
                             	  <span class="album_form_textarea_calendar">'.$Lang['DigitalChannels']['Organize']['Date'].':'.$this->GET_DATE_PICKER("AccessDate",$albumInfo['DateTaken'] > 0?$albumInfo['DateTaken']:date('Y-m-d'),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'</span>
                           		</div>
	                        <p class="spacer"></p>
                       		<div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['AlbumTitleChi'].' :</div> 
                                <input type="text" name="chiTitle" size="30"  placeholder="'.$Lang['DigitalChannels']['Organize']['AlbumChiTitle'].'..." class="album_form_bilingual_input" value="'.intranet_htmlspecialchars($albumInfo['ChiTitle']).'" />
	                        <p class="spacer"></p>
                        	<div class="'.$albumTitleCss.'" > '.$Lang['DigitalChannels']['Organize']['DescriptionEng'].' :</div> 
                           	    <input type="text" name="description" size="50"  placeholder="'.$Lang['DigitalChannels']['Organize']['EngDescription'].'..." class="album_form_bilingual_input" value="'.$albumInfo['Description'].'" /> 
                            <p class="spacer"></p>
                       		<div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['DescriptionChi'].' :</div> 
                                <input type="text" name="chiDescription" size="50"  placeholder="'.$Lang['DigitalChannels']['Organize']['ChiDescription'].'..." class="album_form_bilingual_input" value="'.intranet_htmlspecialchars($albumInfo['ChiDescription']).'" />
	                        <p class="spacer"></p>
                            <div class="'.$albumTitleCss.'" >'.$Lang['DigitalChannels']['Organize']['PIC'].' :</div> 
                                <span class="album_form_textarea">
                                     '.$linterface->GET_SELECTION_BOX($array_PICs, "name='PIC[]' id='PIC[]' class='album_form_textarea_select' size='3' multiple='multiple'", "").'
                                      </select> 
                                </span>
                                <span class="album_form_textarea"> 
                                    <input type="button" class="formsmallbutton" value="'.$Lang['DigitalChannels']['General']['Select'].'" onclick="javascript:newWindow(\'/home/common_choose/index.php?fieldname=PIC[]&page_title=SelectMembers&permitted_type=1&excluded_type=4&DisplayGroupCategory=1&Disable_AddGroup_Button=1'.$urlParms.'\', 9);" />
                                    <p class="spacer_margin"></p>
                                    <input type="button" class="formsmallbutton" value="'.$Lang['DigitalChannels']['General']['Remove'].'" onclick="javascript:checkOptionRemove(document.getElementById(\'PIC[]\'));"/>
                                </span>
                                <!--<div class="Content_tool"><a href="#" class="new"> Add photos</a></div>-->
                            <p class="spacer"></p>
                            <div class="'.$albumTitleCss.'">&nbsp;</div>
						        <span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['PicSelection'].'</span>';                	
                        

                        if ($isQuickAdd) {
                            if (($sys_custom['DigitalChannels']['showEventTitle']) && ($sys_custom['DigitalChannels']['showEventDate'])){
                                $x .= '<p class="spacer"></p><br>
                                       <div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['EventTitleEng'].' :</div> 
                                     	  <input type="text" name="eventTitle" size="30" placeholder="'.$Lang['DigitalChannels']['Organize']['EngEventTitleHere'].'..." class="album_form_bilingual_input" value="" />
                                       <div class="album_form_date_access">
                                 	      <span class="album_form_textarea_calendar">'.$Lang['DigitalChannels']['Organize']['EventDate'].':'.$this->GET_DATE_PICKER("eventDate",date('Y-m-d'),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'</span>
                               		   </div>
                                       <p class="spacer"></p>
                                       <div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['EventTitleChi'].' :</div> 
                                     	 <input type="text" name="chiEventTitle" size="30" placeholder="'.$Lang['DigitalChannels']['Organize']['ChiEventTitleHere'].'..." class="album_form_bilingual_input" value="" />';
                            }
                            elseif ($sys_custom['DigitalChannels']['showEventTitle']){
                                $x .= '<p class="spacer"></p><br>
                                       <div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['EventTitleEng'].' :</div>
                                        <input type="text" name="eventTitle" size="30" placeholder="'.$Lang['DigitalChannels']['Organize']['EngEventTitleHere'].'..." class="album_form_bilingual_input" value="" />
                                        <p class="spacer"></p>
                                        <div class="'.$albumTitleCss.'"> '.$Lang['DigitalChannels']['Organize']['EventTitleChi'].' :</div>
                                        <input type="text" name="chiEventTitle" size="30" placeholder="'.$Lang['DigitalChannels']['Organize']['ChiEventTitleHere'].'..." class="album_form_bilingual_input" value="" />';
                            }
                            elseif ($sys_custom['DigitalChannels']['showEventDate']) {
                               	$x .= '<p class="spacer"></p><br>
                                       <div class="'.$albumTitleCss.'">'.$Lang['DigitalChannels']['Organize']['EventDate2'].' :</div>
                                 	      <span class="album_form_textarea_calendar">'.$this->GET_DATE_PICKER("eventDate",date('Y-m-d'),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'</span>
                               		   ';
                            }
                        }
            
                        $x .= '</div>';
                        
                        $x .= '<span class="drag_photos_top"></span>';
                      
                      $photoInfoAry = $ldc->Get_Album_Photo('',$AlbumID);
                      
                      $album = $ldc->getAlbum($AlbumID);
                      $x .='<div class="edit_photo_list" id="attach_file_area">
							<ul>';
                      $photoCounter = 0;    // initialized
					  foreach ((array)$photoInfoAry as $photo):
    					  if ($sys_custom['DigitalChannels']['showEventDate']) {
    					      $photoCounter++;
    					      $eventDateName = 'eventDate'.$photoCounter;
    					      $eventDate = $this->GET_DATE_PICKER($eventDateName,$photo['event_date'] > 0?$photo['event_date']:date('Y-m-d'),$OtherMember='placeholder="'.$Lang['DigitalChannels']['Organize']['EventDate'].'..."',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="updateEventDate(this)",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
    					      $spanEventDate = '<p class="spacer"></p><span class="eventDate">'.$eventDate.'</span>';
    					  }
    					  else {
    					      $spanEventDate = '';
    					  }
    					  
    					  if ($sys_custom['DigitalChannels']['showEventTitle']) {
    					      $spanEventTitle = '<p class="spacer"></p><textarea maxlength="100" placeholder="'.$Lang['DigitalChannels']['Organize']['EngEventTitleHere'].'..." name="eventTitle[]" class="input_event_title" wrap="virtual" rows="2">'.intranet_htmlspecialchars($photo['event_title']).'</textarea>';
    					      $spanChiEventTitle = '<p class="spacer"></p><textarea maxlength="100" placeholder="'.$Lang['DigitalChannels']['Organize']['ChiEventTitleHere'].'..." name="chiEventTitle[]" class="input_chi_event_title" wrap="virtual" rows="2">'.intranet_htmlspecialchars($photo['chi_event_title']).'</textarea>';
    					  }
    					  else {
    					      $spanEventTitle = '';
    					      $spanChiEventTitle = '';
    					  }

    					  if (!$isQuickAdd) {
						      $x .='<li class="uploaded">';
						      $x .= '<span title="'.$photo['title'].'" style="background-image:url('.libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo).')"/></span>';
						      $x .= $spanEventDate.$spanEventTitle.$spanChiEventTitle;
						      $x .= '<p class="spacer"></p>
								<textarea maxlength="255" placeholder="'.$Lang['DigitalChannels']['Organize']['EngDescriptionHere'].'..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2">'.intranet_htmlspecialchars($photo['description']).'</textarea>';
						      $x .= '<p class="spacer"></p>
								<textarea maxlength="255" placeholder="'.$Lang['DigitalChannels']['Organize']['ChiDescriptionHere'].'..." name="chiDescriptions[]" class="input_chi_desc" wrap="virtual" rows="2">'.intranet_htmlspecialchars($photo['chi_description']).'</textarea>';
    						  $x .= '<input type="hidden" name="photo_ids[]" class="photo_ids" value="'.$photo['id'].'"/>
    								<input type="hidden" class="date_taken" value="'.$photo['date_taken'].'"/>
    								<input type="hidden" class="date_uploaded" value="'.$photo['date_uploaded'].'"/>
    								<input type="hidden" class="title" value="'.$photo['title'].'"/>
    								<p class="spacer"></p>
    								<div class="table_row_tool">
    						
    								    <a href="#" class="copy_dim" title="'.$Lang['DigitalChannels']['Organize']['SetAsCoverPhoto'].'"></a> 
    								    <a href="#" class="delete_dim" title="'.$Lang['DigitalChannels']['Organize']['RemoveFile'].'"></a>
    								</div>
    							    </li>';
    					  }
						  
					  endforeach;
//							   
							$x .='</ul>';
							   
                      list($shared_all, $shared_groups, $user_list) = $ldc->getAlbumUsersGroups($AlbumID);
                      $userListAry = $ldc->getTargetUserName($user_list);
                      
                      $x.='<div class="album_add_photo_area">
							<p class="spacer"></p>
	                   	    <span style="font-size:2em; color: #999999">'.$Lang['DigitalChannels']['Organize']['DragPhoto'].'</span> 
	                        <p class="spacer"></p>
	                           <span style="color:#ccc"> '.$Lang['DigitalChannels']['General']['Or'].' </span>
	                            <p class="spacer"></p>
	                            <a class="new" id="add_photos" href="#">'.$Lang['DigitalChannels']['Organize']['SelectPhotoFrom'].'</a>
							</div></div>';
						$x .= '<span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['SupportUploadFormat'].'</span>';
						if(libdigitalchannels::$max_upload_size > 0){
							$x .= '<br>
							   <span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['MaxFileSize'].libdigitalchannels::$max_upload_size.'MB</span>';
						}
						$x.='<div class="album_content">
                    	<div class="album_form">
                            <div class="album_form_title" >'.$Lang['DigitalChannels']['Organize']['TargetGroup'].' :</div>
							<input '.((!$shared_all && !$shared_groups && !$user_list)?'checked':'').' type="radio" data-form="uniform" name="share_to" id="share_to_myself" value="" class="album_form_radio" /><label for="share_to_myself">'.$Lang['DigitalChannels']['Organize']['Private'].'</label>&nbsp;
							<input '.($shared_all?'checked':'').' type="radio" data-form="uniform" name="share_to" id="share_to_all" value="all" class="album_form_radio" /><label for="share_to_all">'.$Lang['DigitalChannels']['Organize']['AllUsers'].'</label>&nbsp;
							<input '.($shared_groups?'checked':'').' type="radio" data-form="uniform" name="share_to" id="share_to_groups" value="groups" class="album_form_radio" /><label for="share_to_groups">'.$Lang['DigitalChannels']['Organize']['Groups'].'</label>&nbsp;
                            <input '.($user_list?'checked':'').' type="radio" data-form="uniform" name="share_to" id="share_to_users" value="users" class="album_form_radio" /><label for="share_to_users">'.$Lang['DigitalChannels']['Organize']['Users'].'</label>&nbsp;
 							<br/>'.$this->Get_Target_Group_Select($shared_groups).'<br/>
                            
                            <div id="select_specific_users"'.($user_list?"":" style='display:none'").'>
                                <span class="album_form_textarea">
                                     '.$linterface->GET_SELECTION_BOX($userListAry, "name='select_users[]' id='select_users' class='select_users album_form_textarea_select' size='5' multiple='multiple' style='min-width:200px;'", "").'
                                      </select> 
                                </span>
                                <span class="album_form_textarea"> 
                                    <input type="button" class="formsmallbutton" value="'.$Lang['DigitalChannels']['General']['Select'].'" onclick="javascript:newWindow(\'/home/common_choose/index.php?fieldname=select_users[]&page_title=SelectAudience&permitted_type=1,2,3&excluded_type=4&DisplayGroupCategory=0&Disable_AddGroup_Button=1'.$urlParms.'\', 12);" />
                                    <p class="spacer_margin"></p>
                                    <input type="button" class="formsmallbutton" value="'.$Lang['DigitalChannels']['General']['Remove'].'" onclick="javascript:checkOptionRemove(document.getElementById(\'select_users\'));"/>
                                </span>
                                <table><tr><td>&nbsp;</td></tr></table>
                            </div>
                            

                           <!--<input type="radio" data-form="uniform" name="target_group" id="" value="private" class="album_form_radio" checked="checked"/>    
                           '.$Lang['DigitalChannels']['Organize']['Private'].'
                            <input type="radio" data-form="uniform" name="target_group" id="" value="all" class="album_form_radio" />    
                            '.$Lang['DigitalChannels']['Organize']['AllUsers'].'   
                            <input type="radio" data-form="uniform" name="target_group" id="" value="group" class="album_form_radio" />    
                           '.$Lang['DigitalChannels']['Organize']['Groups'].'-->  
                            <p class="spacer"></p>
							<div class="album_form_title"> '.$Lang['DigitalChannels']['Organize']['Period'].' :</div> 
							<span class="album_form_textarea_calendar">'.$this->GET_DATE_PICKER("StartDate",$albumInfo['SharedSince']> 0?$albumInfo['SharedSince']:'',$OtherMember='placeholder="'.$Lang['DigitalChannels']['Organize']['StartDate'].'..."',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'<!--<input type="text" value="" placeholder="start date ..."  size="12" maxlength="10" class="album_form_date_access_input" /></span>
                              <span class="table_row_tool"><a href="#" class="select_date" title="Select"></a>--> &nbsp;  &nbsp;'.$Lang['DigitalChannels']['General']['To'].'&nbsp; &nbsp; </span>
							 
							  <span class="album_form_textarea_calendar">'.$this->GET_DATE_PICKER("EndDate",$albumInfo['SharedUntil']> 0?$albumInfo['SharedUntil']:'',$OtherMember='placeholder="'.$Lang['DigitalChannels']['Organize']['EndDate'].'..."',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'<!--<input type="text" value="" placeholder="end date ..."  size="12" maxlength="10" class="album_form_date_access_input" /></span>
                              <span class="table_row_tool"><a href="#" class="select_date" title="Select"></a>--></span>
	                        <p class="spacer"></p>
                            
      					<div class="edit_bottom">
                        	<input type="submit" class="formbutton" value="'.$Lang['DigitalChannels']['General']['Submit'].'" />
      					    <input type="button" class="formsubbutton" value="'.$Lang['DigitalChannels']['General']['Cancel'].'" onclick="cancelCheck(\''.$albumInfo['CategoryCode'].'\')" />';
						
						if (!$isQuickAdd) {
							$x .= '<a style="padding-left: 20px; position: absolute; left: 0;" class="album_form_remove" href="#">'.$Lang['DigitalChannels']['Organize']['RemoveAlbum'].'</a>';
						}
						else {
						    $x .= '<input type="hidden" name="isQuickAdd" value="1">';
						}
						
      					$x .='</div>
                     <!---->
		</div></div>';
		if($AlbumID){
			$x .='<input type="hidden" name="AlbumID" value="'.$AlbumID.'" />';
		}
		$x .='<input type="hidden" name="photoCounter" id="photoCounter" value="'.$photoCounter.'" />';
		$x .= '</form>';
		
		if ($sys_custom['DigitalChannels']['showEventDate']) {
		    // !!! note: do not call $this->GET_DATE_PICKER(...) here, just add the input date field
 		    $eventDate = '<input type=text name="eventDate" id="eventDate" value="'.date('Y-m-d').'" size=10 maxlength=10 class="textboxnum" placeholder="'.$Lang['DigitalChannels']['Organize']['DateTaken'].'..." onkeyup="Date_Picker_Check_Date_Format_CanEmpty(this,\'DPWL-eventDate\',\'\');">';
 		    $eventDate .= '<span style="color:red;" id="DPWL-eventDate"></span>';
		    $spanEventDate = '<p class="spacer"></p><span class="eventDate">'.$eventDate.'</span>';
		}
		else {
		    $spanEventDate = '';
		}

		if ($sys_custom['DigitalChannels']['showEventTitle']) {
		    $spanEventTitle = '<p class="spacer"></p><textarea disabled maxlength="100" placeholder="'.$Lang['DigitalChannels']['Organize']['EngEventTitleHere'].'..." name="eventTitle[]" class="input_event_title" wrap="virtual" rows="2"></textarea>';
		    $spanChiEventTitle = '<p class="spacer"></p><textarea disabled maxlength="100" placeholder="'.$Lang['DigitalChannels']['Organize']['ChiEventTitleHere'].'..." name="chiEventTitle[]" class="input_chi_event_title" wrap="virtual" rows="2"></textarea>';
		}
		else {
		    $spanEventTitle = '';
		    $spanChiEventTitle = '';
		}
		
		
		$x .='<ul id="edit_photo_list_template" style="display:none">
		    <li style="display:none">
			<span></span>';
		if (!$isQuickAdd) {
		    $x .= $spanEventDate.$spanEventTitle.$spanChiEventTitle;
            $x .= '<p class="spacer"></p>
			<textarea disabled maxlength="255" placeholder="'.$Lang['DigitalChannels']['Organize']['EngDescriptionHere'].'..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2"></textarea>
            <p class="spacer"></p>
			<textarea disabled maxlength="255" placeholder="'.$Lang['DigitalChannels']['Organize']['ChiDescriptionHere'].'..." name="chiDescriptions[]" class="input_chi_desc" wrap="virtual" rows="2"></textarea>';
		}          
		$x .= '<input type="hidden" name="photo_ids[]" class="photo_ids" value=""/>
			<input type="hidden" class="date_taken" value=""/>
			<input type="hidden" class="date_uploaded" value=""/>
			<input type="hidden" class="title" value=""/>
			<p class="spacer"></p>
			<div class="table_row_tool" style="display:none">
			    <a href="#" class="copy_dim" title="'.$Lang['DigitalChannels']['Organize']['SetAsCoverPhoto'].'"></a><a href="#" class="delete_dim" title="'.$Lang['DigitalChannels']['Organize']['RemoveFile'].'"></a>
			</div>
		    </li>
		</ul>';
		
		return $x;
	}
	
	function Display_Recommend_Album_New($RecommendID) {
		global $PATH_WRT_ROOT, $Lang;
		global $linterface;
		
		$ldc = new libdigitalchannels();
		if($RecommendID > 0){
			$albumInfo = current($ldc->Get_Recommend_Album($RecommendID,''));
			$photoInfoAry = $ldc->Get_Recommend_Album_Photo($RecommendID,'');
//			$currentPhotoInfo = current($photoInfoAry);
//			$album = $ldc->getAlbum($currentPhotoInfo['album_id']);
		}
		$x .= '<form id="form1" name="form1" class="newalbum" method="post" onsubmit="return checkForm();" action="admin_recommend_album_update.php">';
			$x .= '<div class="album_content">
                    	<div class="album_form">
                       ';
                     $x .= '<p class="spacer"></p>
                       			<div class="album_form_title"> '.$Lang['DigitalChannels']['Recommend']['RecommendTitle'].' :</div> 
                             	<input type="text" name="title" size="30"  placeholder="'.$Lang['DigitalChannels']['Recommend']['RecommendTitle'].'..." class="album_form_input" value="'.$albumInfo['Title'].'" />
                           		
	                        	<p class="spacer"></p>
                        		<div class="album_form_title" >'.$Lang['DigitalChannels']['Recommend']['RecommendDescription'].' :</div> 
                           		<input type="text" name="description" size="50"  placeholder="'.$Lang['DigitalChannels']['Recommend']['RecommendDescription'].'..." class="album_form_input" value="'.$albumInfo['Description'].'" /> 
                            	<p class="spacer"></p>
                          
                         </div> 
                         <span class="drag_photos_top"></span>';
                      
//                      $photoInfoAry = $ldc->Get_Album_Photo('',$AlbumID);
//                      $album = $ldc->getAlbum($AlbumID);
                      $x .='<input type="button" class="formsubbutton" id="fixedSizeThickboxLink" value="'.$Lang['DigitalChannels']['General']['Add'].'" />
							<div class="edit_photo_list" id="attach_file_area">
							<ul id="selected_photo_list">';
					  foreach ((array)$photoInfoAry as $photo):
								$album = $ldc->getAlbum($photo['album_id']);
							    $x .='<li class="uploaded">
								<span title="'.$photo['title'].'" style="background-image:url('.libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo).')"/></span>
								<p class="spacer"></p>
							    
								<textarea maxlength="255" placeholder="'.$Lang['DigitalChannels']['Organize']['DescriptionHere'].'..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2" readonly>'.($photo['description']?$photo['description']:$photo['title']).'</textarea>
								<input type="hidden" name="photo_ids[]" class="photo_ids" value="'.$photo['id'].'"/>
								<input type="hidden" class="date_taken" value="'.$photo['date_taken'].'"/>
								<input type="hidden" class="date_uploaded" value="'.$photo['date_uploaded'].'"/>
								<input type="hidden" class="title" value="'.$photo['title'].'"/>
								<p class="spacer"></p>
								<div class="table_row_tool">
								    <a href="#" class="delete_dim" title="'.$Lang['DigitalChannels']['Organize']['RemoveFile'].'"></a>
								</div>
							    </li>';
							    endforeach;
							   
							$x .='</ul>';
							   
                    
                      $x.='<div class="album_add_photo_area">
							<!--<p class="spacer"></p>
	                   	    <span style="font-size:2em; color: #999999">'.$Lang['DigitalChannels']['Organize']['DragPhoto'].'</span> 
	                        <p class="spacer"></p>
	                           <span style="color:#ccc"> '.$Lang['DigitalChannels']['General']['Or'].' </span>
	                            <p class="spacer"></p>
	                            <a class="new" id="add_photos" href="#">'.$Lang['DigitalChannels']['Organize']['SelectPhotoFrom'].'</a>-->
							</div></div>';
//						$x .= '<span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['SupportUploadFormat'].'</span>
//							   <br>
//							   <span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['MaxFileSize'].'</span>';
                      	
						$x.='<div class="album_content">
                    	<div class="album_form">
							<div class="album_form_title"> '.$Lang['DigitalChannels']['Organize']['Period'].' :</div> 
							<span class="album_form_textarea_calendar">'.$this->GET_DATE_PICKER("StartDate",$albumInfo['SharedSince']> 0?$albumInfo['SharedSince']:'',$OtherMember='placeholder="'.$Lang['DigitalChannels']['Organize']['StartDate'].'..."',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'<!--<input type="text" value="" placeholder="start date ..."  size="12" maxlength="10" class="album_form_date_access_input" /></span>
                              <span class="table_row_tool"><a href="#" class="select_date" title="Select"></a>--> &nbsp;  &nbsp;'.$Lang['DigitalChannels']['General']['To'].'&nbsp; &nbsp; </span>
							 
							  <span class="album_form_textarea_calendar">'.$this->GET_DATE_PICKER("EndDate",$albumInfo['SharedUntil']> 0?$albumInfo['SharedUntil']:'',$OtherMember='placeholder="'.$Lang['DigitalChannels']['Organize']['EndDate'].'..."',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum album_form_date_access_input").'<!--<input type="text" value="" placeholder="end date ..."  size="12" maxlength="10" class="album_form_date_access_input" /></span>
                              <span class="table_row_tool"><a href="#" class="select_date" title="Select"></a>--></span>
	                        <p class="spacer"></p>
                            <span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['RecAlbumDateRange'].'</span>
      					<div class="edit_bottom">
                        	<input type="submit" class="formbutton" value="'.$Lang['DigitalChannels']['General']['Submit'].'" />
      					    <input type="button" class="formsubbutton" value="'.$Lang['DigitalChannels']['General']['Cancel'].'" onclick="cancelCheck('.$RecommendID.')" />
							<a style="padding-left: 20px; position: absolute; left: 0;" class="album_form_remove" href="#">'.$Lang['DigitalChannels']['Recommend']['RemoveRecommendation'].'</a>
      					</div>
                     <!---->
		</div></div>';
		if($RecommendID){
			$x .='<input type="hidden" name="RecommendID" value="'.$RecommendID.'" />';
		}
		$x .='<input type="hidden" id="NeedUpdatePhoto" name="NeedUpdatePhoto" value="0" />';
		$x .= '</form>';
		
		$x .='<ul id="edit_photo_list_template" style="display:none">
		    <li style="display:none">
			
			<span></span>
			<p class="spacer"></p>
			<textarea disabled maxlength="255" placeholder="'.$Lang['DigitalChannels']['Organize']['DescriptionHere'].'..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2"></textarea>
			<input type="hidden" name="photo_ids[]" class="photo_ids" value=""/>
			<input type="hidden" class="date_taken" value=""/>
			<input type="hidden" class="date_uploaded" value=""/>
			<input type="hidden" class="title" value=""/>
			<p class="spacer"></p>
			<div class="table_row_tool" style="display:none">
			    <a href="#" class="copy_dim" title="'.$Lang['DigitalChannels']['Organize']['SetAsCoverPhoto'].'"></a><a href="#" class="delete_dim" title="'.$Lang['DigitalChannels']['Organize']['RemoveFile'].'"></a>
			</div>
		    </li>
		</ul>';
		
		return $x;
	}
	
	function Display_Category_Album_Photo($AlbumID, $ViewMode = false) {
	    global $PATH_WRT_ROOT, $Lang, $sys_custom;
		
//		$videoFormats = array('3GP','WMV','MP4');
		
		$ldc = new libdigitalchannels();
		$albumInfo = current($ldc->Get_Album($AlbumID,''));
		$categoryInfo = current($ldc->Get_Category($albumInfo['CategoryCode']));
		$ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$AlbumID);
		$numOfPhoto = count($ablumPhotoInfoAry);
	
		$albumTitle = Get_Lang_Selection($albumInfo['ChiTitle'],$albumInfo['Title']);
		
        $album = $ldc->getAlbum($AlbumID);
        $description = Get_Lang_Selection($album['descriptionZh'], $album['description']);

		$x = '<div class="album_content">
                    <div class="navigation_bar">';
        if(!$ViewMode){
              $x .= '<a href="admin_category_album.php?CategoryCode='.$albumInfo['CategoryCode'].'">'.$categoryInfo['Description'].'</a>';
        }
        else{
        	$x .= '<a href="category_album.php?CategoryCode='.$albumInfo['CategoryCode'].'">'.$categoryInfo['Description'].'</a>';
        }
        $x .= '<span>'.($albumTitle?$albumTitle:'('.$Lang['DigitalChannels']['Organize']['UntitledAlbum'].')').' <em>'.sprintf($Lang['DigitalChannels']['General']['PhotosInTotal'], $numOfPhoto).'</em></span></div>
                    <p class="spacer"></p>';	
			
            
			if(!$ViewMode){	
				$x .= '<div class="content_tool" style="float:right"><a href="admin_category_album_new.php?AlbumID='.$AlbumID.'" class="edit_content">'.$Lang['DigitalChannels']['General']['Edit'].'</a>';
				
				if (($sys_custom['DigitalChannels']['showEventTitle']) || ($sys_custom['DigitalChannels']['showEventDate'])){
				    $x .= '&nbsp;&nbsp;<a href="admin_category_album_quick_add.php?AlbumID='.$AlbumID.'" class="quick_add_content">'.$Lang['DigitalChannels']['General']['QuickAdd'].'</a>';
				}
				$x .= '</div>
			    <p class="spacer"></p>';
			}
			
			  $x .= $description; 
			  $x .='<div class="photo_thumb_list">
			    <ul>';

		if($numOfPhoto > 0){
			//debug_pr($ablumPhotoInfoAry);
			foreach($ablumPhotoInfoAry as $aAblumPhoto){
			    $description = Get_Lang_Selection($aAblumPhoto['chi_description'], $aAblumPhoto['description']);
			    $description = $description ? $description : $aAblumPhoto['title'];
			    
    		    $x .= '<li> 
    		    <div class="photo_thumb_block photo_thumb_effect">
    		               <div class="photo_thumb_details"> 
    		               <div class="photo_thumb_overlay">
    		                 <a href="#"><span class="title">'.$description.'</span></a>
    		                   </div></div>
    		                    
    		     <div class="photo_thumb_images" style="display:block">';
    		    if(!$ViewMode){	
    		    	$x .= '<a href="admin_category_album_photo_view.php?AlbumID='.$AlbumID.'&AlbumPhotoID='.$aAblumPhoto['id'].'">';
    		    }
    		    else{
    		    	$x .= '<a href="category_album_photo_view.php?AlbumID='.$AlbumID.'&AlbumPhotoID='.$aAblumPhoto['id'].'">';	
    		    }
    			if(in_array(strtoupper(pathinfo($aAblumPhoto['title'], PATHINFO_EXTENSION)),libdigitalchannels::$videoFormat)){
    				$x .= '<span class="play_button"></span>';
    			}
    			$x .='<img src="'.$PATH_WRT_ROOT.libdigitalchannels::getPhotoFileName('thumbnail', $album, $aAblumPhoto).'" title="'.$aAblumPhoto['title'].'"/></a></div>
    		             </div>
    		            <!--<em>New</em>-->
    		             <div class="clearfix"></div>  
    			</li>';
			}
		}
		else{
			$x .= $Lang['DigitalChannels']['General']['NoRecord'];
		}
		$x .= '</ul><p class="spacer"></p></div>';
		return $x;
	}
	
	function Display_Recommend_Album_Photo($RecommendID, $ViewMode = false) {
		global $PATH_WRT_ROOT, $Lang;
		
		$ldc = new libdigitalchannels();
		$albumInfo = current($ldc->Get_Recommend_Album($RecommendID,''));
		$ablumPhotoInfoAry = $ldc->Get_Recommend_Album_Photo($RecommendID,'');
		$numOfPhoto = count($ablumPhotoInfoAry);
	
        //$album = $ldc->getAlbum($ablumPhotoInfoAry[0]['album_id']);
        
		$x = '<div class="album_content">
                    <div class="navigation_bar">';
        
        $x .= '<span>'.($albumInfo['Title']?$albumInfo['Title']:'('.$Lang['DigitalChannels']['Recommend']['UntitledRecAlbum'].')').' <em>'.sprintf($Lang['DigitalChannels']['General']['PhotosInTotal'], $numOfPhoto).($albumInfo['SharedSince'] != 0 &&$albumInfo['SharedUntil']!=0?' ('.$albumInfo['SharedSince'].' ~ '.$albumInfo['SharedUntil'].')':'').'</em></span></div>
                    <p class="spacer"></p>';	
			
			if(!$ViewMode){	
				$x .= '<div class="content_tool" style="float:right"><a href="admin_recommend_album_new.php?RecommendID='.$RecommendID.'" class="edit_content">'.$Lang['DigitalChannels']['General']['Edit'].'</a></div>
			    <p class="spacer"></p>';
			}
			
			  $x .=$albumInfo['Description'];
			  $x .='<div class="photo_thumb_list">
			    <ul>';

		if($numOfPhoto > 0){
			//debug_pr($ablumPhotoInfoAry);
			foreach($ablumPhotoInfoAry as $aAblumPhoto){
			$album = $ldc->getAlbum($aAblumPhoto['album_id']);
			$description = Get_Lang_Selection($aAblumPhoto['chi_description'], $aAblumPhoto['description']);
			$description = $description ? $description : $aAblumPhoto['title'];
			
		    $x .= '<li> 
		    <div class="photo_thumb_block photo_thumb_effect">
		               <div class="photo_thumb_details"> 
		               <div class="photo_thumb_overlay">
		                 <a href="#"><span class="title">'.$description.'</span></a>
		                   </div></div>
		                    
		     <div class="photo_thumb_images" style="display:block">';
		    if(!$ViewMode){	
		    	//$x .= '<a href="admin_category_album_photo_view.php?AlbumID='.$album['id'].'&AlbumPhotoID='.$aAblumPhoto['id'].'">';
		    	$x .= '<a href="admin_recommend_album_photo_view.php?AlbumID='.$album['id'].'&RecommendID='.$RecommendID.'&RecommendPhotoID='.$aAblumPhoto['id'].'">';
		    }
		    else{
		    	//$x .= '<a href="category_album_photo_view.php?AlbumID='.$album['id'].'&AlbumPhotoID='.$aAblumPhoto['id'].'">';
		    	$x .= '<a href="recommend_album_photo_view.php?AlbumID='.$album['id'].'&RecommendID='.$RecommendID.'&RecommendPhotoID='.$aAblumPhoto['id'].'">';	
		    }
			if(in_array(strtoupper(pathinfo($aAblumPhoto['title'], PATHINFO_EXTENSION)),libdigitalchannels::$videoFormat)){
				$x .= '<span class="play_button"></span>';
			}
			$x .='<img src="'.$PATH_WRT_ROOT.libdigitalchannels::getPhotoFileName('thumbnail', $album, $aAblumPhoto).'" title="'.$aAblumPhoto['title'].'"/></a></div>
		             </div>
		            <!--<em>New</em>-->
		             <div class="clearfix"></div>  
			</li>';
			}
		}
		else{
			$x .= $Lang['DigitalChannels']['General']['NoRecord'];
		}
		$x .= '</ul><p class="spacer"></p></div>';
		return $x;
	}
	
	function Display_Category_Album_Photo_Favorites() {
		global $PATH_WRT_ROOT, $Lang;
		
//		$videoFormats = array('3GP','WMV','MP4');
		
		$ldc = new libdigitalchannels();
//		$albumInfo = current($ldc->Get_Album($AlbumID,''));
//		$categoryInfo = current($ldc->Get_Category($albumInfo['CategoryCode']));
		$favPhotoInfoAry = $ldc->Get_Album_Photo_Favorites();
		$numOfPhoto = count($favPhotoInfoAry);
        
		$x = '<div class="album_content">
                    <div class="navigation_bar">';
              $x .= '<span><em>'.sprintf($Lang['DigitalChannels']['General']['PhotosInTotal'], $numOfPhoto).'</em></span></div>
                    <p class="spacer"></p>';	
			  $x .='<div class="photo_thumb_list">
			    <ul>';

		if($numOfPhoto > 0){
			//debug_pr($ablumPhotoInfoAry);
			foreach($favPhotoInfoAry as $aAblumPhoto){
			$album = $ldc->getAlbum($aAblumPhoto['album_id']);
			$description = Get_Lang_Selection($aAblumPhoto['chi_description'], $aAblumPhoto['description']);
			$description = $description ? $description : $aAblumPhoto['title'];
			
		    $x .= '<li> 
		    <div class="photo_thumb_block photo_thumb_effect">
		               <div class="photo_thumb_details"> 
		               <div class="photo_thumb_overlay">
		                 <a href="#"><span class="title">'.$description.'</span></a>
		                   </div></div>
		                    
		     <div class="photo_thumb_images" style="display:block">';

		    $x .= '<a href="../categories/category_album_photo_view.php?AlbumID='.$aAblumPhoto['album_id'].'&AlbumPhotoID='.$aAblumPhoto['id'].'">';	

			if(in_array(strtoupper(pathinfo($aAblumPhoto['title'], PATHINFO_EXTENSION)),libdigitalchannels::$videoFormat)){
				$x .= '<span class="play_button"></span>';
			}
			$x .='<img src="'.$PATH_WRT_ROOT.libdigitalchannels::getPhotoFileName('thumbnail', $album, $aAblumPhoto).'" title="'.$aAblumPhoto['title'].'"/></a></div>
		             </div>
		            <!--<em>New</em>-->
		             <div class="clearfix"></div>  
			</li>';
			}
		}
		else{
			$x .= $Lang['DigitalChannels']['General']['NoRecord'];
		}
		$x .= '</ul><p class="spacer"></p></div>';
		return $x;
	}
	
	//developing by Henry...
	function Display_Category_Album_Search($params = array(), $SchoolYear= 0){
		global $PATH_WRT_ROOT, $Lang;
		
		$ldc = new libdigitalchannels();
		
		$albumInfoAry = $ldc->Get_Album_Search('',$params, $SchoolYear);
		$ViewMode = true;
		if($ViewMode){
			
			$albumIDs = array();
			foreach($albumInfoAry as $aAlbumInfo){
				$albumIDs[] = $aAlbumInfo['AlbumID'];
			}
			$isAlbumsReadable = $ldc->isAlbumsReadable($albumIDs);
			
			$tempAlbumInfoAry = array();
			foreach($albumInfoAry as $aAlbumInfo){
				if($ViewMode && !$isAlbumsReadable[$aAlbumInfo['AlbumID']]){
	        		continue;
	        	}
	        	$tempAlbumInfoAry[] = $aAlbumInfo;
			}
			$albumInfoAry = $tempAlbumInfoAry;
		}
		if(!$ViewMode){
			$albumIDs = array();
			foreach($albumInfoAry as $aAlbumInfo){
				$albumIDs[] = $aAlbumInfo['AlbumID'];
			}
			$isAlbumsEditable = $ldc->isAlbumsEditable($albumIDs);
			
			$tempAlbumInfoAry = array();
			foreach($albumInfoAry as $aAlbumInfo){
				if(!$ViewMode && !$isAlbumsEditable[$aAlbumInfo['AlbumID']]){
	        		continue;
	        	}
	        	$tempAlbumInfoAry[] = $aAlbumInfo;
			}
			$albumInfoAry = $tempAlbumInfoAry;
		}
		
		//debug_pr($categoryInfoAry);
		$numOfAlbum = count($albumInfoAry);
		
		$x .='<div class="album_content">
                
                    <div class="navigation_bar"><span>('.$Lang['DigitalChannels']['General']['SearchResult'].')<em>'.sprintf($Lang['DigitalChannels']['General']['AlbumsInTotal'], $numOfAlbum).'</em></span></div>
                      <p class="spacer"></p>


                        
                             <ul class="album_list">';
		if(!$ViewMode && $ldc->isAlbumNewable($aAlbumInfo['CategoryCode'])){	
			$x .='<li> 
                 <a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album_new.php?CategoryCode='.$CategoryCode.'&isNew=1" class="new_categories"><span>'.$Lang['DigitalChannels']['Organize']['NewAlbum'].' </span>
                 <span class="add"></span> 
                  <div></div>
                  </a> </li>';
		}
		
        foreach($albumInfoAry as $aAlbumInfo){
//        	if($ViewMode && !$ldc->isAlbumReadable($aAlbumInfo['AlbumID'])){
//        		continue;
//        	}
        	$ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$aAlbumInfo['AlbumID']);
        	$numOfPhoto = count($ablumPhotoInfoAry);
			//$album = $ldc->getAlbum($aAlbumInfo['AlbumID']);
			
			# add path to the photo array
			$album = array(
			    "id" => $aAlbumInfo['AlbumID'],
			    "input_date" => strtotime($aAlbumInfo['DateInput']),
			    "input_by" => $aAlbumInfo['InputBy']
			);
			
			//get cover photo info
			$coverPhotoInfo = current($ablumPhotoInfoAry);
			foreach($ablumPhotoInfoAry as $aPhoto){
				if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
					$coverPhotoInfo = $aPhoto;
				}
			}
			
			$date1 = $aAlbumInfo['DateTaken'];
			$ts1 = strtotime($date1);
			$ts2 = strtotime(date('Y-m-d'));
			$seconds_diff = $ts2 - $ts1;
			$interval = floor($seconds_diff/3600/24);
			$intervalMsg = $interval.' '.$Lang['DigitalChannels']['General']['DaysAgo'];
			if($date1 == '0000-00-00' || !$date1){
					$intervalMsg = '';
				}
//			if($interval < 0){
//				$intervalMsg = date('Y-m-d', $date1);
//			}

			$albumTitle = Get_Lang_Selection($aAlbumInfo['ChiTitle'],$aAlbumInfo['Title']);
				
        	$x .='<li>';
        			if(!$ViewMode){
                    	$x .='<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_category_album_photo.php?AlbumID='.$aAlbumInfo['AlbumID'].'">';
        			}
        			else{
        				$x .='<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/categories/category_album_photo.php?AlbumID='.$aAlbumInfo['AlbumID'].'">';
        			}
                     if($numOfPhoto > 0){
                     $x .='<span class="photo_pic"> <img src="'.libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo).'"/></span>';
                     }
                     $x .='<div>'.($albumTitle?$albumTitle:'('.$Lang['DigitalChannels']['Organize']['UntitledAlbum'].')').'<span class="photo_number">('.count($ablumPhotoInfoAry).')</span>
                      <span class="photo_date">'.$intervalMsg.'</span>                      </div>
                     <!--<em>New</em>-->                      </a>                      </li>';

        }
                             
                      $x.='</ul>
                   	  <p class="spacer"></p>

		</div>';
		return $x;
	}
	
	function Display_Category_Album_Photo_More($type) {
		global $PATH_WRT_ROOT, $Lang;
		
//		$videoFormats = array('3GP','WMV','MP4');
		
		$ldc = new libdigitalchannels();
//		$albumInfo = current($ldc->Get_Album($AlbumID,''));
//		$categoryInfo = current($ldc->Get_Category($albumInfo['CategoryCode']));

		$IsRecommendPhoto = false;
		if($type == 'Recommend'){
			$favPhotoInfoAry = $ldc->Get_Recommend_Photo_Array_New();
			$displatTitle = $Lang['DigitalChannels']['Recommend']['Reommendation'];
			$IsRecommendPhoto = true;
			if(count($favPhotoInfoAry) <= 0){
				$favPhotoInfoAry = $ldc->Get_Recommend_Photo_Array();
				$displatTitle = $Lang['DigitalChannels']['General']['MostPopular'];
				$IsRecommendPhoto = false;
			}
		}else if($type == 'MostHitPhoto'){
			$favPhotoInfoAry = $ldc->Get_Most_Hit_Array('Photo');
			$displatTitle = $Lang['DigitalChannels']['General']['MostHitPhoto'];
		}else if($type == 'MostHitVideo'){
			$favPhotoInfoAry = $ldc->Get_Most_Hit_Array('Video');
			$displatTitle = $Lang['DigitalChannels']['General']['MostHitVideo'];
		}else if($type == 'LastestPhoto'){
			$favPhotoInfoAry = $ldc->Get_Lastest_Photo_Array();
			$displatTitle = $Lang['DigitalChannels']['General']['LastestPhoto'];
		}else if($type == 'LastestAlbum'){
			$albumInfoAry = $ldc->Get_Lastest_Album_Array();
			$displatTitle = $Lang['DigitalChannels']['General']['LastestAlbum'];
			
			$albumIDs = array();
			foreach($albumInfoAry as $aAlbumInfo){
				$albumIDs[] = $aAlbumInfo['album_id'];
			}
			$isAlbumsReadable = $ldc->isAlbumsReadable($albumIDs);
			
			$tempAlbumInfoAry = array();
			foreach($albumInfoAry as $aAlbumInfo){
				if(!$isAlbumsReadable[$aAlbumInfo['album_id']]){
	        		continue;
	        	}
	        	$tempAlbumInfoAry[] = $aAlbumInfo;
			}
			$albumInfoAry = $tempAlbumInfoAry;
		
		
			//debug_pr($categoryInfoAry);
			$numOfAlbum = count($albumInfoAry);
			
			$x .='<div class="album_content">
	                    <div class="navigation_bar"><span>('.$displatTitle.')<em>'.sprintf($Lang['DigitalChannels']['General']['AlbumsInTotal'], $numOfAlbum).'</em></span></div>
	                      <p class="spacer"></p>
					<ul class="album_list">';
			
	        foreach($albumInfoAry as $aAlbumInfo){
	//        	if($ViewMode && !$ldc->isAlbumReadable($aAlbumInfo['AlbumID'])){
	//        		continue;
	//        	}
	        	$ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$aAlbumInfo['album_id']);
	        	$numOfPhoto = count($ablumPhotoInfoAry);
				//$album = $ldc->getAlbum($aAlbumInfo['AlbumID']);
				# add path to the photo array
				$album = array(
				    "id" => $aAlbumInfo['album_id'],
				    "input_date" => strtotime($aAlbumInfo['a_input_date']),
				    "input_by" => $aAlbumInfo['a_input_by']
				);
				//get cover photo info
				$coverPhotoInfo = current($ablumPhotoInfoAry);
				foreach($ablumPhotoInfoAry as $aPhoto){
					if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
						$coverPhotoInfo = $aPhoto;
					}
				}
				
				$date1 = $aAlbumInfo['DateTaken'];
				$ts1 = strtotime($date1);
				$ts2 = strtotime(date('Y-m-d'));
				$seconds_diff = $ts2 - $ts1;
				$interval = floor($seconds_diff/3600/24);
				$intervalMsg = $interval.' '.$Lang['DigitalChannels']['General']['DaysAgo'];
				if($date1 == '0000-00-00' || !$date1){
					$intervalMsg = '';
				}
	//			if($interval < 0){
	//				$intervalMsg = date('Y-m-d', $date1);
	//			}
				
	        	$x .='<li>';
	        			
    			$x .='<a href="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/DigitalChannels/categories/category_album_photo.php?AlbumID='.$aAlbumInfo['album_id'].'">';
    			
                 if($numOfPhoto > 0){
                 $x .='<span class="photo_pic"> <img src="'.$PATH_WRT_ROOT.$aAlbumInfo['FilePath'].'"/></span>';
                 }
                 $aAlbumInfo['title'] = Get_Lang_Selection($aAlbumInfo['ChiTitle'],$aAlbumInfo['Title']);
                 $x .='<div>'.($aAlbumInfo['title']?$aAlbumInfo['title']:'('.$Lang['DigitalChannels']['Organize']['UntitledAlbum'].')').'<span class="photo_number">('.count($ablumPhotoInfoAry).')</span>
                  <span class="photo_date">'.$intervalMsg.'</span>                      </div>
                 <!--<em>New</em>-->                      </a>                      </li>';
	
	        }	                             
          	$x.='</ul><p class="spacer"></p></div>';
			
			return $x;
		}
		
		$numOfPhoto = count($favPhotoInfoAry);
        
		$x = '<div class="album_content">
                    <div class="navigation_bar">';
              $x .= '<span>('.$displatTitle.')<em>'.sprintf($Lang['DigitalChannels']['General']['PhotosInTotal'], $numOfPhoto).'</em></span></div>
                    <p class="spacer"></p>';	
			  $x .='<div class="photo_thumb_list">
			    <ul>';

		if($numOfPhoto > 0){
			//debug_pr($ablumPhotoInfoAry);
			foreach($favPhotoInfoAry as $aAblumPhoto){
			//$album = $ldc->getAlbum($aAblumPhoto['album_id']);
			# add path to the photo array
			$album = array(
			    "id" => $aAblumPhoto['album_id'],
			    "input_date" => $aAblumPhoto['a_input_date'],
			    "input_by" => $aAblumPhoto['a_input_by']
			);
			$description = Get_Lang_Selection($aAblumPhoto['chi_description'], $aAblumPhoto['description']);
			$description = $description ? $description : $aAblumPhoto['title'];
			
		    $x .= '<li> 
		    <div class="photo_thumb_block photo_thumb_effect">
		               <div class="photo_thumb_details"> 
		               <div class="photo_thumb_overlay">
		                 <a href="#"><span class="title">'.$description.'</span></a>
		                   </div></div>
		                    
		     <div class="photo_thumb_images" style="display:block">';
			
			if($IsRecommendPhoto){
				$x.='<a href="categories/recommend_album_photo_view.php?AlbumID='.$aAblumPhoto['album_id'].'&RecommendPhotoID='.$aAblumPhoto['id'].'">';
			}
			else{
		    	$x .= '<a href="categories/category_album_photo_view.php?AlbumID='.$aAblumPhoto['album_id'].'&AlbumPhotoID='.$aAblumPhoto['id'].'">';	
			}
			if(in_array(strtoupper(pathinfo($aAblumPhoto['title'], PATHINFO_EXTENSION)),libdigitalchannels::$videoFormat)){
				$x .= '<span class="play_button"></span>';
			}
			$x .='<img src="'.$PATH_WRT_ROOT.libdigitalchannels::getPhotoFileName('thumbnail', $album, $aAblumPhoto).'" title="'.$aAblumPhoto['title'].'"/></a></div>
		             </div>
		            <!--<em>New</em>-->
		             <div class="clearfix"></div>  
			</li>';
			}
		}
		else{
			$x .= $Lang['DigitalChannels']['General']['NoRecord'];
		}
		$x .= '</ul><p class="spacer"></p></div>';
		return $x;
	}
	
	function Display_Category_Album_Photo_Search($params = array(), $SchoolYear = 0) {
		global $PATH_WRT_ROOT, $Lang;
		
//		$videoFormats = array('3GP','WMV','MP4');
		
		$ldc = new libdigitalchannels();
//		$albumInfo = current($ldc->Get_Album($AlbumID,''));
//		$categoryInfo = current($ldc->Get_Category($albumInfo['CategoryCode']));
		$favPhotoInfoAry = $ldc->Get_Album_Photo_Search($params, $SchoolYear);
		$numOfPhoto = count($favPhotoInfoAry);
        
		$x = '<div class="album_content">
                    <div class="navigation_bar">';
              $x .= '<span>('.$Lang['DigitalChannels']['General']['SearchResult'].')<em>'.sprintf($Lang['DigitalChannels']['General']['PhotosInTotal'], $numOfPhoto).'</em></span></div>
                    <p class="spacer"></p>';	
			  $x .='<div class="photo_thumb_list">
			    <ul>';

		if($numOfPhoto > 0){
			//debug_pr($ablumPhotoInfoAry);
			foreach($favPhotoInfoAry as $aAblumPhoto){
			//$album = $ldc->getAlbum($aAblumPhoto['album_id']);
			# add path to the photo array
			$album = array(
			    "id" => $aAblumPhoto['album_id'],
			    "input_date" => $aAblumPhoto['a_input_date'],
			    "input_by" => $aAblumPhoto['a_input_by']
			);
			$description = Get_Lang_Selection($aAblumPhoto['chi_description'], $aAblumPhoto['description']);
			$description = $description ? $description : $aAblumPhoto['title'];
			
		    $x .= '<li> 
		    <div class="photo_thumb_block photo_thumb_effect">
		               <div class="photo_thumb_details"> 
		               <div class="photo_thumb_overlay">
		                 <a href="#"><span class="title">'.$description.'</span></a>
		                   </div></div>
		                    
		     <div class="photo_thumb_images" style="display:block">';

		    $x .= '<a href="categories/category_album_photo_view.php?AlbumID='.$aAblumPhoto['album_id'].'&AlbumPhotoID='.$aAblumPhoto['id'].'">';	

			if(in_array(strtoupper(pathinfo($aAblumPhoto['title'], PATHINFO_EXTENSION)),libdigitalchannels::$videoFormat)){
				$x .= '<span class="play_button"></span>';
			}
			$x .='<img src="'.$PATH_WRT_ROOT.libdigitalchannels::getPhotoFileName('thumbnail', $album, $aAblumPhoto).'" title="'.$aAblumPhoto['title'].'"/></a></div>
		             </div>
		            <!--<em>New</em>-->
		             <div class="clearfix"></div>  
			</li>';
			}
		}
		else{
			$x .= $Lang['DigitalChannels']['General']['NoRecord'];
		}
		$x .= '</ul><p class="spacer"></p></div>';
		return $x;
	}
	
	function Display_Category_Album_Photo_View($AlbumID, $AlbumPhotoID, $ViewMode = false){
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $UserID, $userBrowser, $junior_mck, $file_path, $sys_custom;
		$videoFormat = libdigitalchannels::$videoFormat;
		
		include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
		$lgs = new libgeneralsettings();
		$settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'", "'DisallowUserToComment'"));
		
		$photoViewPage = ($ViewMode?'category_album_photo_view.php':'admin_category_album_photo_view.php');
		
		$ldc = new libdigitalchannels();
		
		# Get album info
		$album 			= $ldc->getAlbum($AlbumID);
		$album_Arr		= $ldc->getAlbumPhotos($AlbumID);
		$album_size 	= count($album_Arr);
		
		$albumTitle = Get_Lang_Selection($album['titleZh'], $album['title']);
		
		# Get current photo url
		$photo	 		= $ldc->getAlbumPhoto($AlbumPhotoID);
		$photo_url 		= libdigitalchannels::getPhotoFileName('photo', $album, $photo);
	    
	    # Get current photo / video info
		for($i = 0; $i < $album_size; $i++){
			if($album_Arr[$i]['id'] == $photo['id']){
				$currentSeq = $i;
				break;
			}
		}
		$photo_ext 		= pathinfo($photo['title'], PATHINFO_EXTENSION);
		$isVideo		= (in_array(strtoupper($photo_ext),$videoFormat))? true : false;
		if($isVideo)
			$videoDetails		 	= libdigitalchannels::getVideoDetails($file_path.$photo_url);
		else 
			list($width, $height) 	= getimagesize($file_path.$photo_url);
		
		# Get orignal photo path
		$orignal_url = libdigitalchannels::getPhotoFileName('original', $album, $photo);
		if(!file_exists($file_path.$orignal_url))
			$orignal_url = libdigitalchannels::getPhotoFileName('photo', $album, $photo);
		
		# Get album photo slider
		$slider_array = array();
		$slider_array_full = array();
		$slider_size  = ($album_size >= 7)? 7 : $album_size;
		$slider_start = $currentSeq - floor(($slider_size - 1) / 2);
		
		# loop each thumbnail
		for($i = 0; $i < $album_size; $i++, $slider_start++){
			# Get current thumbnail
			$sequence 	= ($slider_start < 0)? $album_size + $slider_start : $slider_start;
			$sequence 	= ($sequence >= $album_size)? $slider_start - $album_size : $sequence;
			$thumbnail 	= $album_Arr[$sequence];
			
			# Details of Thumbnail
			$thumbnail_url	= libdigitalchannels::getPhotoFileName('thumbnail', $album, $thumbnail);
			$thumbnail_ext 	= pathinfo($thumbnail['title'], PATHINFO_EXTENSION);
			$t_isVideo		= (in_array(strtoupper($thumbnail_ext),$videoFormat))? true : false;
			
			# get photo url
			if($t_isVideo)
			$original_url	= '/home/eAdmin/ResourcesMgmt/DigitalChannels/fancy_embed.php?id='.getEncryptedText($thumbnail['id'].'_'.$thumbnail['input_date'].'_'.$thumbnail['input_by'], libdigitalchannels::$encrypt_key);
			else{
//				$original_url = libdigitalchannels::getPhotoFileName('original', $album, $thumbnail);
//				if(!file_exists($PATH_WRT_ROOT.$original_url))
					$original_url = libdigitalchannels::getPhotoFileName('photo', $album, $thumbnail);
			}
			# Resize Thumbnail
			list($t_width, $t_height) 	= getimagesize($file_path.$thumbnail_url);
//			if($t_height > $t_width)
//				$thumbnail_style = "style='max-height:42px;";
//			else
//				$thumbnail_style = "style='max-width:42px;";
//			$thumbnail_style .= " left: auto; right: auto;'";
//			$thumbnail_style .= $t_isVideo? " margin-top: -40px;'" : "'";
			if($i < $slider_size){
				$slider_array[] = array($thumbnail_url, $album_Arr[$sequence]['id'], $t_isVideo, $thumbnail_style, $original_url);
			}
			$slider_array_full[] = array($t_isVideo, $original_url);
			
			if((int)$sequence === (int)$currentSeq){		
				$currentSeqi = $i;
			}
		}
	   
	    list($prev_photo_url, $prev_photo_id) = ((int)$currentSeqi !== 0)? $slider_array[$currentSeqi - 1] : end($slider_array);
	    list($next_photo_url, $next_photo_id) = ((int)$currentSeqi !== ((int)$slider_size - 1))? $slider_array[$currentSeqi + 1] : $slider_array[0];
		
		# Get category
		$currentCat = $ldc->Get_Category($album['CategoryCode']);
		
		# Define photo or video
		$ext = pathinfo($photo['title'], PATHINFO_EXTENSION);
		if($isVideo){
			if($videoDetails['Height'] > $videoDetails['Width'])
				$photo_style = "style='height:100%; max-height:455px;max-width:880px;'";
			else
				$photo_style = "style='width:100%; max-height:455px;max-width:880px;'";

			$vod = $ldc->hasVod($AlbumPhotoID);
            if( ! $vod || $ldc->fromCN()){
			    $photoDisplay = '<a href="javascript: void(0);">
								<video '.$photo_style.' controls '.($settings['AllowDownloadOriginalPhoto']?'':'controlsList="nodownload"').'>
								    <source type="video/mp4" src="'.$photo_url.'"></source>
								    <!--<source type="video/ogg" src="mov_bbb.ogg"></source>-->
								    Your browser does not support HTML5 video.
								</video>
							</a>';
            } else {
                $photoDisplay = $ldc->getVodEmbedded($vod);
            }
		} else {
			if($height > $width)
				$photo_style = "style='max-height:480px;max-width:880px'";
			else
				$photo_style = "style='max-height:480px;max-width:880px'";
			$photoDisplay = '<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'"><img src="'.$photo_url.'"  alt="" data-description="" '.$photo_style.'/></a>';
		}
		
		# Get user personal photo
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$li = new libuser($UserID);
		$photoLink = $li->PersonalPhotoLink;
		if($junior_mck > 0){
			$photoLink = $li->PhotoLink;
		}
		$personal_photo_link = '';
		if (is_file($intranet_root.$photoLink))
	    {
	        $personal_photo_link = $photoLink;
	        
	        if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")) 
	          $personal_photo_delete = "<a href=\"javascript:deletePersonalPhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeletePersonalPhoto'] ." </a>";
	    }
		
		# Get general settings
//		include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
//		$lgs = new libgeneralsettings();
//		$settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'", "'DisallowUserToComment'"));
		
		$photoCommentArray = $ldc->Get_Album_Photo_Comment($AlbumPhotoID);
		
		#check user enjoy or not
		$enjoyed = count($ldc->Get_Album_Photo_Favorites_User($AlbumPhotoID, $UserID)) > 0;
		
		$x.='	
			<div class="view_images_wrapper">
	  			
				<div class="view_images_nav">
					<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$prev_photo_id.'" class="view_images_nav_prev">Previous Image</a>
					<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'" class="view_images_nav_next">Next Image</a>
				</div>
				<div class="view_images" style="border-bottom:10px;position: relative;">'.$photoDisplay.'</div>	 	

				<br>
				<br>

				<div class="thumb_small_slider_wapper">
		 			<div class="thumb_small_slider"><div>
						<ul >
						<!--
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/01.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/02.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/03.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/04.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/05.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/06.jpg"  alt="" data-description="" /></a></li>
						-->
					';
				foreach($slider_array as $slider_photo){
//					$play_button = $slider_photo[2]? '<span class="play_button_small" style="background-size: 40px; width:40px; height:40px; max-width:40px; max-height:40px;"></span>' : '';
					$play_button = $slider_photo[2]? '<span class="play_button_mini"></span>' : '';
					$x .='	<li><div class="thumb_gallery">
								<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$slider_photo[1].'">
									'.$play_button.'
									<img src="'.$slider_photo[0].'" '.$slider_photo[3].' alt="" data-description=""/>
								</a>
							</div></li>';
				}
				$x .='	</ul>
			  		</div></div>
				</div>
				<div class="view_images_loading"></div>
			';
//		if(!$isVideo){
//	  	//$x.='	<div class="view_images_full_screen"><a onclick="window.open(\''.$photo_url.'\', \'\', \'fullscreen=yes\', \'status=yes\', \'scrollbars=yes\');""></a></div>';
//		$x.='	<div class="view_images_full_screen"><a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);"></a></div>';
//		}
		
		
		# loop each big photo
		$x.='<div class="view_images_full_screen">';
		for($i=0; $i<count($slider_array_full); $i++){
			if($currentSeqi != $i){
				$x.='<div style="display:none">';
			}
			if($slider_array_full[$i][0]){
				$x.='<a class="various3" title="" href="'.$slider_array_full[$i][1].'" rel="example_group"></a>';
				if($currentSeqi != $i){
					$x.='</div>';
				}
			}
			else{
				$x.='<a href="'.$slider_array_full[$i][1].'" rel="example_group"></a>';
				if($currentSeqi != $i){
					$x.='</div>';
				}
			}
			
		}
		$x.='</div>';
		
	    $x.='	<div class="view_images_back">';
	    		if(!$ViewMode){
					$x.='<a title="Back" href="admin_category_album_photo.php?AlbumID='.$AlbumID.'" >'.$Lang['DigitalChannels']['General']['Back'].'</a>';
	    		}
	    		else{
	    			$x.='<a title="Back" href="category_album_photo.php?AlbumID='.$AlbumID.'" >'.$Lang['DigitalChannels']['General']['Back'].'</a>';
	    		}
				$x.='</div>
				<div class="view_images_icon"> 
					<a title="'.($enjoyed?$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto']:$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto']).'" href="javascript:void(0);" onclick="Update_Photo_Favorites('.$AlbumPhotoID.');" class="icon_enjoy '.($enjoyed?'active':'').'"></a>
					<!--<a title="Share this photo" href="#" class="icon_export"></a>-->';
					// show download icon only if 'AllowDownloadOriginalPhoto'
//					if($settings['AllowDownloadOriginalPhoto'] || $ldc->isAlbumEditable($AlbumID))
					if($settings['AllowDownloadOriginalPhoto']){
						$org_ext = pathinfo($orignal_url, PATHINFO_EXTENSION);
						$x.='<a title="'.$Lang['DigitalChannels']['Organize']['DownloadThisPhoto'].'" href="'.$orignal_url.'" class="icon_download" download="'.rtrim($photo['title'], '.'.$ext).'.'.$org_ext.'" ></a>';
					}
				$x.='</div>     
			</div>
	
			<div id="rg-gallery" class="rg-gallery">
					<div class="es-carousel-wrapper" style="display:none;">
						<div class="es-nav">
							<span class="es-nav-prev">Previous</span>
							<span class="es-nav-next">Next</span>
							<div class="es-carousel">
		                    	<ul>
		  	                    	<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/01.jpg"  alt="" data-description="" /></a></li>
		    	                    <li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/02.jpg"  alt="" data-description="" /></a></li>
		                        </ul>
							</div>
						</div>
					</div>
				</div>
	
				<div class="comment">
					<div class="comment_left">
						<div class="comment_content comment_content_bold">
							'.nl2br(trim( htmlspecialchars( Get_Lang_Selection($photo['ChiDescription'], $photo['description']), ENT_QUOTES, 'UTF-8' ))).'
						</div>
						<div class="comment_content">
							'.$Lang['DigitalChannels']['Organize']['Album'].': 
							<a href="'.(!$ViewMode?"admin_":"").'category_album_photo.php?AlbumID='.$AlbumID.'" >'.($albumTitle?$albumTitle : $Lang['DigitalChannels']['Organize']['UntitledAlbum']).'</a> | 
							'.$Lang['DigitalChannels']['Organize']['Category'].': 
							<a href="'.(!$ViewMode?"admin_":"").'category_album.php?CategoryCode='.$currentCat[0]['CategoryCode'].'" >'.$currentCat[0]['Description'].'</a>
						</div>';
						if($ldc->isAlbumEditable($AlbumID) && $isVideo){
						    if(isset($sys_custom['HideDigitalChannelEmbedded']) && $sys_custom['HideDigitalChannelEmbedded']){
						        // Hide Embed code
                            } else {
                                if(! $vod || $ldc->fromCN()){
                                    $x.='<div class="comment_content">'.$Lang['DigitalChannels']['General']['Embed'].':<br/><textarea id="txtEmbed" readonly rows="5" cols="60" style="resize: none;">'.htmlspecialchars('<iframe width="420" height="315" src="http://'.$_SERVER['HTTP_HOST'].'/home/eAdmin/ResourcesMgmt/DigitalChannels/embed.php?id='.getEncryptedText($photo['id'].'_'.$photo['input_date'].'_'.$photo['input_by'], libdigitalchannels::$encrypt_key).'" frameborder="0" allowfullscreen></iframe>').'</textarea></div>';
                                }  else {
                                    $x.= '<div class="comment_content">'.$Lang['DigitalChannels']['General']['Embed'].':<br/><textarea id="txtEmbed" readonly rows="5" cols="60" style="resize: none;">'.htmlspecialchars($photoDisplay).'</textarea></div>';
                                }
                            }
                        }
					$x.='</div>
					<div class="comment_right">
						<div class="comment_content">
							<span class="icon_views"><span id="view_count">'.$ldc->Get_Album_Photo_View_Total($AlbumPhotoID).'</span><span class="no_effect">'.$Lang['DigitalChannels']['Organize']['View'].'</span></span>
							<span class="icon_small_enjoy"><span id="enjoy_count">'.$ldc->Get_Album_Photo_Favorites_Total($AlbumPhotoID).'</span><span class="no_effect">'.$Lang['DigitalChannels']['Organize']['Enjoy'].'</span></span>
							<span class="icon_comment"><span id="comment_count">'.$ldc->Get_Album_Photo_Comment_Total($AlbumPhotoID).'</span><span class="no_effect">'.$Lang['DigitalChannels']['Organize']['Comment'].'</span></span>
						</div>
				<!--
						<div class="comment_content">
							<a href="#" >Cyrus Fung</a>, <a href="#" >Justin Chan</a>, <a href="#">John Ma</a>, enjoy this.
						</div>

						<div class="previous_content">
							<a href="#" >View 64 previous comments</a>
						</div>
				-->
						<div id="enjoy_user_list">
						</div>
						<div id="comment_content">';
						
//						foreach($photoCommentArray as $aComment){
//							$x.='<li onmouseover="MM_showHideLayers(\'comment_edit'.$aComment['RecordID'].'\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit'.$aComment['RecordID'].'\',\'\',\'hide\')"> 
//								<div class="comment_user"><img style="width:32px" src="'.$aComment['PersonalPhotoLink'].'" alt="" /></div>
//								<div class="comment_info">
//									<div class="comment_name"><a href="#">'.$aComment['UserName'].'</a><span class="comment_time" title="'.$aComment['DateInput'].'">'.$this->time_passed(strtotime($aComment['DateInput'])).'</span></div>
//									<div class="comment_meta" id="comment_edit'.$aComment['RecordID'].'" style="visibility:hidden"> '.($aComment['UserID'] == $UserID?'<a href="#" class="icon_edit"></a>':'').' '.($ldc->isAdminUser()?'<a href="#" class="icon_delete"></a>':'').'</div>
//									<div class="comment_text">'.$aComment['Content'].'</div>
//								</div>
//							</li>';
//						}
						$x.='</div><ul class="comment_list">';
						$x.='<!--
							<li onmouseover="MM_showHideLayers(\'comment_edit\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit\',\'\',\'hide\')"> 
								<div class="comment_user"><img  src="" alt="" /></div>
								<div class="comment_info">
									<div class="comment_name"><a href="#">Katie Wong</a><span class="comment_time">11h</span></div>
									<div class="comment_meta" id="comment_edit" style="visibility:hidden"> <a href="#" class="icon_edit"></a> <a href="#" class="icon_delete"></a></div>
									<div class="comment_text">
										Very nice!!!! I like it!
									</div>
								</div>
							</li>
							<li onmouseover="MM_showHideLayers(\'comment_edit02\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit02\',\'\',\'hide\')"> 
								<div class="comment_user"><img  src="" alt="" /></div>
								<div class="comment_info">
									<div class="comment_name"><a href="#">Katie Wong Katie Wong Katie Wong Katie Wong Katie Wong Katie Wong </a><span class="comment_time">11h</span></div>
									<div class="comment_meta" id="comment_edit02" style="visibility:hidden"> <a href="#" class="icon_edit"></a> <a href="#" class="icon_delete"></a></div>
									<div class="comment_text">
										Beautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and light
									</div>
								</div>
							</li>
						-->';
			
						 $x.='<!-- Case 1: By default -->
						 <!--	
							<li>
								<div class="comment_user"><img  src="" alt="" /></div>
								<div class="comment_info">
									<textarea id="" name="" placeholder="Add a comment" class="comment_text_input" rows="3"></textarea>
								</div>
							 </li> 
						-->';
						if($settings['DisallowUserToComment']!='1'){
							$x.='<li>
						<!-- Case 2:When Comment textarea is clicked , comment_btn will be display --> 
								<div class="comment_user"><img style="width:32px" src="'.$personal_photo_link.'" alt="" /></div>
								<div class="comment_info">
								<form id="form1" name="form1" class="" method="post" onsubmit="return checkForm();" action="../organize/admin_category_album_photo_view_update.php">
									<textarea id="txtComment" name="txtComment" onfocus="document.getElementById(\'btnAddComment\').style.display = \'\';"  placeholder="'.$Lang['DigitalChannels']['Organize']['AddComment'].'" class="comment_text_input" rows="3" maxlength="255"></textarea>
									<input type="hidden" id="AlbumID" name="AlbumID" value="'.$AlbumID.'" />
									<input type="hidden" id="hidPhotoID" name="hidPhotoID" value="'.$AlbumPhotoID.'" />
									<input type="hidden" id="ViewMode" name="ViewMode" value="'.$ViewMode.'" />
									<input type="submit" id="btnAddComment" name="btnAddComment" class="comment_btn"  value="'.$Lang['DigitalChannels']['Organize']['Comment'].'" style="display:none" />
								</form>
								</div>
							</li>';
						}
						else{
							$x.='<form id="form1" name="form1">
									<input type="hidden" id="AlbumID" name="AlbumID" value="'.$AlbumID.'" />
									<input type="hidden" id="hidPhotoID" name="hidPhotoID" value="'.$AlbumPhotoID.'" />
									<input type="hidden" id="ViewMode" name="ViewMode" value="'.$ViewMode.'" />
								</form>';
						}
						$x.='</ul> <!-- Commentlist End -->
					</div>
					                     <!---->
					     
				</div>
		</div>
		<p class=" spacer"></p>';
 
	return $x;
	
	}
	
	function Display_Recommend_Album_Photo_View($RecommendID, $RecommendPhotoID, $ViewMode = false){
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $UserID, $userBrowser, $file_path, $sys_custom;
		$videoFormat = libdigitalchannels::$videoFormat;
		
		$photoViewPage = ($ViewMode?'recommend_album_photo_view.php':'admin_recommend_album_photo_view.php');
		
		include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
		$lgs = new libgeneralsettings();
		$settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'", "'DisallowUserToComment'"));
		
		$ldc = new libdigitalchannels();
		
		//$albumInfo = current($ldc->Get_Recommend_Album($RecommendID,''));
		
		if($ViewMode){
			$ablumPhotoInfoAry = $ldc->Get_Recommend_Photo_Array_New();
			$RecommendID = $ldc->Get_Recommend_Id_By_Photo_Id($RecommendPhotoID);
		}
		else{
			$ablumPhotoInfoAry = $ldc->Get_Recommend_Album_Photo($RecommendID,'');
		}
		
		foreach($ablumPhotoInfoAry as $aAblumPhoto){
			if($aAblumPhoto['id'] == $RecommendPhotoID){
				$AlbumID = $aAblumPhoto['album_id'];
			}
		}
		$numOfPhoto = count($ablumPhotoInfoAry);
		
		# Get album info
		$album 			= $ldc->getAlbum($AlbumID);
		
		$album_Arr = array();
		for($i=0; $i<$numOfPhoto; $i++){
			$album_Arr[] = $ldc->getAlbumPhoto($ablumPhotoInfoAry[$i]['id']);
			if($ablumPhotoInfoAry[$i]['id'] == $RecommendPhotoID){
				$currentSeq = $i;
			}
		}
		//$album_Arr		= BuildMultiKeyAssoc($ldc->getAlbumPhotos($AlbumID), 'Sequence');
		$album_size 	= count($album_Arr);
		//debug_pr($album_Arr);
		# Get current photo url
		$photo	 		= $ldc->getAlbumPhoto($RecommendPhotoID);
		$photo_url 		= libdigitalchannels::getPhotoFileName('photo', $album, $photo);
	    
	    # Get current photo / video info
		//$currentSeq 	= $photo['Sequence'];
		$photo_ext 		= pathinfo($photo['title'], PATHINFO_EXTENSION);
		$isVideo		= (in_array(strtoupper($photo_ext),$videoFormat))? true : false;
		if($isVideo)
			$videoDetails		 	= libdigitalchannels::getVideoDetails($file_path.$photo_url);
		else 
			list($width, $height) 	= getimagesize($file_path.$photo_url);
		
		# Get orignal photo path
		$orignal_url = libdigitalchannels::getPhotoFileName('original', $album, $photo);
		if(!file_exists($PATH_WRT_ROOT.$orignal_url))
			$orignal_url = libdigitalchannels::getPhotoFileName('photo', $album, $photo);
		
		# Get album photo slider
		$slider_array = array();
		$slider_size  = ($album_size >= 7)? 7 : $album_size;
		$slider_start = $currentSeq - floor(($slider_size - 1) / 2);
		
		# loop each thumbnail
		for($i = 0; $i < $slider_size; $i++, $slider_start++){

			# Get current thumbnail
			$sequence 	= ($slider_start < 0)? $album_size + $slider_start : $slider_start;
			$sequence 	= ($sequence >= $album_size)? $slider_start - $album_size : $sequence;
			$thumbnail 	= $album_Arr[$sequence];
			
			# Details of Thumbnail
			$album1 			= $ldc->getAlbum($album_Arr[$sequence]['album_id']);
			$thumbnail_url	= libdigitalchannels::getPhotoFileName('thumbnail', $album1, $thumbnail);
			$thumbnail_ext 	= pathinfo($thumbnail['title'], PATHINFO_EXTENSION);
			$t_isVideo		= (in_array(strtoupper($thumbnail_ext),$videoFormat))? true : false;
			
			# get photo url
			if($t_isVideo)
			$original_url	= '/home/eAdmin/ResourcesMgmt/DigitalChannels/fancy_embed.php?id='.getEncryptedText($thumbnail['id'].'_'.$thumbnail['input_date'].'_'.$thumbnail['input_by'], libdigitalchannels::$encrypt_key);
			else{
//				$original_url = libdigitalchannels::getPhotoFileName('original', $album1, $thumbnail);
//				if(!file_exists($PATH_WRT_ROOT.$original_url))
					$original_url = libdigitalchannels::getPhotoFileName('photo', $album1, $thumbnail);
			}
			
			# Resize Thumbnail
			list($t_width, $t_height) 	= getimagesize($file_path.$thumbnail_url);
//			if($t_height > $t_width)
//				$thumbnail_style = "style='max-height:42px;";
//			else
//				$thumbnail_style = "style='max-width:42px;";
//			$thumbnail_style .= " left: auto; right: auto;'";
//			$thumbnail_style .= $t_isVideo? " margin-top: -40px;'" : "'";
			
			$slider_array[] = array($thumbnail_url, $album_Arr[$sequence]['id'], $t_isVideo, $thumbnail_style, $original_url);
			if((int)$sequence === (int)$currentSeq){		
				$currentSeqi = $i;
			}
		}
	   
	    list($prev_photo_url, $prev_photo_id) = ((int)$currentSeqi !== 0)? $slider_array[$currentSeqi - 1] : end($slider_array);
	    list($next_photo_url, $next_photo_id) = ((int)$currentSeqi !== ((int)$slider_size - 1))? $slider_array[$currentSeqi + 1] : $slider_array[0];
		
		# Get category
		$currentCat = $ldc->Get_Category($album['CategoryCode']);
		
		# Define photo or video
		$ext = pathinfo($photo['title'], PATHINFO_EXTENSION);
		if($isVideo){
			if($videoDetails['Height'] > $videoDetails['Width'])
				$photo_style = "style='height:100%; max-height:455px;max-width:880px;'";
			else
				$photo_style = "style='width:100%; max-height:455px;max-width:880px;'";
            $vod = $ldc->hasVod($RecommendPhotoID);
            if( ! $vod || $ldc->fromCN()){
                $photoDisplay = '<a href="javascript: void(0);">
								<video '.$photo_style.' controls '.($settings['AllowDownloadOriginalPhoto']?'':'controlsList="nodownload"').'>
								    <source type="video/mp4" src="'.$photo_url.'"></source>
								    <!--<source type="video/ogg" src="mov_bbb.ogg"></source>-->
								    Your browser does not support HTML5 video.
								</video>
							</a>';
            } else {
                $photoDisplay = $ldc->getVodEmbedded($vod);
            }
		} else {
			if($height > $width)
				$photo_style = "style='max-height:480px;max-width:880px'";
			else
				$photo_style = "style='max-height:480px;max-width:880px'";
			$photoDisplay = '<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&RecommendID='.$RecommendID.'&RecommendPhotoID='.$next_photo_id.'"><img src="'.$photo_url.'"  alt="" data-description="" '.$photo_style.'/></a>';
		}
		
		# Get user personal photo
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$li = new libuser($UserID);
		$personal_photo_link = '';
		if (is_file($intranet_root.$li->PersonalPhotoLink))
	    {
	        $personal_photo_link = $li->PersonalPhotoLink;
	        
	        if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")) 
	          $personal_photo_delete = "<a href=\"javascript:deletePersonalPhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeletePersonalPhoto'] ." </a>";
	    }
		
		# Get general settings
//		include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
//		$lgs = new libgeneralsettings();
//		$settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'", "'DisallowUserToComment'"));
		
		$photoCommentArray = $ldc->Get_Album_Photo_Comment($RecommendPhotoID);
		
		#check user enjoy or not
		$enjoyed = count($ldc->Get_Album_Photo_Favorites_User($RecommendPhotoID, $UserID)) > 0;
		
		$x.='	
			<div class="view_images_wrapper">
	  			
				<div class="view_images_nav">
					<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&RecommendID='.$RecommendID.'&RecommendPhotoID='.$prev_photo_id.'" class="view_images_nav_prev">Previous Image</a>
					<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&RecommendID='.$RecommendID.'&RecommendPhotoID='.$next_photo_id.'" class="view_images_nav_next">Next Image</a>
				</div>
				<div class="view_images" style="border-bottom:10px;position: relative;">'.$photoDisplay.'</div>	 	

				<br>
				<br>

				<div class="thumb_small_slider_wapper">
		 			<div class="thumb_small_slider"><div>
						<ul >
						<!--
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/01.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/02.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/03.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/04.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/05.jpg"  alt="" data-description="" /></a></li>
							<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/06.jpg"  alt="" data-description="" /></a></li>
						-->
					';
				foreach($slider_array as $slider_photo){
//					$play_button = $slider_photo[2]? '<span class="play_button_small" style="background-size: 40px; width:40px; height:40px; max-width:40px; max-height:40px;"></span>' : '';
					$play_button = $slider_photo[2]? '<span class="play_button_mini"></span>' : '';
					$x .='	<li><div class="thumb_gallery">
								<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&RecommendID='.$RecommendID.'&RecommendPhotoID='.$slider_photo[1].'">
									'.$play_button.'
									<img src="'.$slider_photo[0].'" '.$slider_photo[3].' alt="" data-description=""/>
								</a>
							</div></li>';
				}
				$x .='	</ul>
			  		</div></div>
				</div>
				<div class="view_images_loading"></div>
			';
//		if(!$isVideo){
//	  	//$x.='	<div class="view_images_full_screen"><a onclick="window.open(\''.$photo_url.'\', \'\', \'fullscreen=yes\', \'status=yes\', \'scrollbars=yes\');""></a></div>';
//		$x.='	<div class="view_images_full_screen"><a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);"></a></div>';
//		}
		
		$x.='<div class="view_images_full_screen">';
		for($i=0; $i<count($slider_array); $i++){
			if($currentSeqi != $i){
				$x.='<div style="display:none">';
			}
			if($slider_array[$i][2]){
				$x.='<a class="various3" title="" href="'.$slider_array[$i][4].'" rel="example_group"></a>';
				if($currentSeqi != $i){
					$x.='</div>';
				}
			}
			else{
				$x.='<a href="'.$slider_array[$i][4].'" rel="example_group"></a>';
				if($currentSeqi != $i){
					$x.='</div>';
				}
			}
			
		}
		$x.='</div>';
		
	    $x.='	<div class="view_images_back">';
	    		if(!$ViewMode){
					$x.='<a title="Back" href="admin_recommend_album_photo.php?RecommendID='.$RecommendID.'" >'.$Lang['DigitalChannels']['General']['Back'].'</a>';
	    		}
	    		else{
	    			$x.='<a title="Back" href="../index.php" >'.$Lang['DigitalChannels']['General']['Back'].'</a>';
	    		}
				$x.='</div>
				<div class="view_images_icon">'; 
					if($ldc->isAlbumReadable($AlbumID)){
					$x.='<a title="'.($enjoyed?$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto']:$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto']).'" href="javascript:void(0);" onclick="Update_Photo_Favorites('.$RecommendPhotoID.');" class="icon_enjoy '.($enjoyed?'active':'').'"></a>
					<!--<a title="Share this photo" href="#" class="icon_export"></a>-->';
					}
					// show download icon only if 'AllowDownloadOriginalPhoto'
//					if($settings['AllowDownloadOriginalPhoto'] || $ldc->isAlbumEditable($AlbumID))
					if($settings['AllowDownloadOriginalPhoto']){
						$org_ext = pathinfo($orignal_url, PATHINFO_EXTENSION);
						$x.='<a title="'.$Lang['DigitalChannels']['Organize']['DownloadThisPhoto'].'" href="'.$orignal_url.'" class="icon_download" download="'.rtrim($photo['title'], '.'.$ext).'.'.$org_ext.'" ></a>';
					}
				$x.='</div>     
			</div>
	
			<div id="rg-gallery" class="rg-gallery">
					<div class="es-carousel-wrapper" style="display:none;">
						<div class="es-nav">
							<span class="es-nav-prev">Previous</span>
							<span class="es-nav-next">Next</span>
							<div class="es-carousel">
		                    	<ul>
		  	                    	<li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/01.jpg"  alt="" data-description="" /></a></li>
		    	                    <li><a href="#"><img src="'.$this->ImagePath.'/digital_channels/view/thumbs/02.jpg"  alt="" data-description="" /></a></li>
		                        </ul>
							</div>
						</div>
					</div>
				</div>
	
				<div class="comment">
					<div class="comment_left">
						<div class="comment_content comment_content_bold">
							'.nl2br(trim( htmlspecialchars( Get_Lang_Selection($photo['ChiDescription'], $photo['description']), ENT_QUOTES, 'UTF-8' ))).'
						</div>
						<div class="comment_content">
							'.$Lang['DigitalChannels']['Organize']['Album'].': ';
				
				            $albumTitle = Get_Lang_Selection($album['titleZh'], $album['title']);
				            
							if($ViewMode && !$ldc->isAlbumReadable($AlbumID)){
							    $x.=($albumTitle ? $albumTitle : $Lang['DigitalChannels']['Organize']['UntitledAlbum']);
							}
							else{
							    $x.='<a href="'.(!$ViewMode?"admin_":"").'category_album_photo.php?AlbumID='.$AlbumID.'" >'.($albumTitle ? $albumTitle : $Lang['DigitalChannels']['Organize']['UntitledAlbum']).'</a>';
							}
							
							$x.=' | '.$Lang['DigitalChannels']['Organize']['Category'].': ';
							
							if($ViewMode && !$ldc->isAlbumReadable($AlbumID)){
								$x.=$currentCat[0]['Description'];
							}
							else{
								$x.='<a href="'.(!$ViewMode?"admin_":"").'category_album.php?CategoryCode='.$currentCat[0]['CategoryCode'].'" >'.$currentCat[0]['Description'].'</a>';
							}
						$x.='</div>';
						if($ldc->isAlbumEditable($AlbumID) && $isVideo){
                            if(isset($sys_custom['HideDigitalChannelEmbedded']) && $sys_custom['HideDigitalChannelEmbedded']){
                                // Hide Embed code
                            } else {
                                if(! $vod || $ldc->fromCN()){
                                    $x.='<div class="comment_content">'.$Lang['DigitalChannels']['General']['Embed'].':<br/><textarea id="txtEmbed" readonly rows="5" cols="60" style="resize: none;">'.htmlspecialchars('<iframe width="420" height="315" src="http://'.$_SERVER['HTTP_HOST'].'/home/eAdmin/ResourcesMgmt/DigitalChannels/embed.php?id='.getEncryptedText($photo['id'].'_'.$photo['input_date'].'_'.$photo['input_by'], libdigitalchannels::$encrypt_key).'" frameborder="0" allowfullscreen></iframe>').'</textarea></div>';
                                }  else {
                                    $x.= '<div class="comment_content">'.$Lang['DigitalChannels']['General']['Embed'].':<br/><textarea id="txtEmbed" readonly rows="5" cols="60" style="resize: none;">'.htmlspecialchars($photoDisplay).'</textarea></div>';
                                }
                            }
                        }
					$x.='</div>
					<div class="comment_right">
						<div class="comment_content">
							<span class="icon_views"><span id="view_count">'.$ldc->Get_Album_Photo_View_Total($RecommendPhotoID).'</span><span class="no_effect">'.$Lang['DigitalChannels']['Organize']['View'].'</span></span>
							<span class="icon_small_enjoy"><span id="enjoy_count">'.$ldc->Get_Album_Photo_Favorites_Total($RecommendPhotoID).'</span><span class="no_effect">'.$Lang['DigitalChannels']['Organize']['Enjoy'].'</span></span>
							<span class="icon_comment"><span id="comment_count">'.$ldc->Get_Album_Photo_Comment_Total($RecommendPhotoID).'</span><span class="no_effect">'.$Lang['DigitalChannels']['Organize']['Comment'].'</span></span>
						</div>
				<!--
						<div class="comment_content">
							<a href="#" >Cyrus Fung</a>, <a href="#" >Justin Chan</a>, <a href="#">John Ma</a>, enjoy this.
						</div>

						<div class="previous_content">
							<a href="#" >View 64 previous comments</a>
						</div>
				-->
						<div id="enjoy_user_list">
						</div>
						<div id="comment_content">';
						
//						foreach($photoCommentArray as $aComment){
//							$x.='<li onmouseover="MM_showHideLayers(\'comment_edit'.$aComment['RecordID'].'\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit'.$aComment['RecordID'].'\',\'\',\'hide\')"> 
//								<div class="comment_user"><img style="width:32px" src="'.$aComment['PersonalPhotoLink'].'" alt="" /></div>
//								<div class="comment_info">
//									<div class="comment_name"><a href="#">'.$aComment['UserName'].'</a><span class="comment_time" title="'.$aComment['DateInput'].'">'.$this->time_passed(strtotime($aComment['DateInput'])).'</span></div>
//									<div class="comment_meta" id="comment_edit'.$aComment['RecordID'].'" style="visibility:hidden"> '.($aComment['UserID'] == $UserID?'<a href="#" class="icon_edit"></a>':'').' '.($ldc->isAdminUser()?'<a href="#" class="icon_delete"></a>':'').'</div>
//									<div class="comment_text">'.$aComment['Content'].'</div>
//								</div>
//							</li>';
//						}
						$x.='</div><ul class="comment_list">';
						$x.='<!--
							<li onmouseover="MM_showHideLayers(\'comment_edit\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit\',\'\',\'hide\')"> 
								<div class="comment_user"><img  src="" alt="" /></div>
								<div class="comment_info">
									<div class="comment_name"><a href="#">Katie Wong</a><span class="comment_time">11h</span></div>
									<div class="comment_meta" id="comment_edit" style="visibility:hidden"> <a href="#" class="icon_edit"></a> <a href="#" class="icon_delete"></a></div>
									<div class="comment_text">
										Very nice!!!! I like it!
									</div>
								</div>
							</li>
							<li onmouseover="MM_showHideLayers(\'comment_edit02\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit02\',\'\',\'hide\')"> 
								<div class="comment_user"><img  src="" alt="" /></div>
								<div class="comment_info">
									<div class="comment_name"><a href="#">Katie Wong Katie Wong Katie Wong Katie Wong Katie Wong Katie Wong </a><span class="comment_time">11h</span></div>
									<div class="comment_meta" id="comment_edit02" style="visibility:hidden"> <a href="#" class="icon_edit"></a> <a href="#" class="icon_delete"></a></div>
									<div class="comment_text">
										Beautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and lightBeautiful capture, crisp and light
									</div>
								</div>
							</li>
						-->';
			
						 $x.='<!-- Case 1: By default -->
						 <!--	
							<li>
								<div class="comment_user"><img  src="" alt="" /></div>
								<div class="comment_info">
									<textarea id="" name="" placeholder="Add a comment" class="comment_text_input" rows="3"></textarea>
								</div>
							 </li> 
						-->';
						if($settings['DisallowUserToComment']!='1' && $ldc->isAlbumReadable($AlbumID)){
							$x.='<li>
						<!-- Case 2:When Comment textarea is clicked , comment_btn will be display --> 
								<div class="comment_user"><img style="width:32px" src="'.$personal_photo_link.'" alt="" /></div>
								<div class="comment_info">
								<form id="form1" name="form1" class="" method="post" onsubmit="return checkForm();" action="../organize/admin_category_album_photo_view_update.php">
									<textarea id="txtComment" name="txtComment" onfocus="document.getElementById(\'btnAddComment\').style.display = \'\';"  placeholder="'.$Lang['DigitalChannels']['Organize']['AddComment'].'" class="comment_text_input" rows="3" maxlength="255"></textarea>
									<input type="hidden" id="AlbumID" name="AlbumID" value="'.$AlbumID.'" />
									<input type="hidden" id="hidPhotoID" name="hidPhotoID" value="'.$RecommendPhotoID.'" />
									<input type="hidden" id="ViewMode" name="ViewMode" value="'.$ViewMode.'" />
									<input type="submit" id="btnAddComment" name="btnAddComment" class="comment_btn"  value="'.$Lang['DigitalChannels']['Organize']['Comment'].'" style="display:none" />
								</form>
								</div>
							</li>';
						}
						$x.='</ul> <!-- Commentlist End -->
					</div>
					                     <!---->
					     
				</div>
		</div>
		<p class=" spacer"></p>';
 
	return $x;
	
	}
	
	function Display_Category_Album_Photo_View_Control($AlbumID, $AlbumPhotoID, $ViewMode = false, $RecommendID){
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $UserID, $file_path;
		$videoFormat = libdigitalchannels::$videoFormat;
		
		$photoViewPage = '/home/eAdmin/ResourcesMgmt/DigitalChannels/organize/view_original_control.php';
		
		$ldc = new libdigitalchannels();
		
		if($RecommendID){
			$albumInfo = current($ldc->Get_Recommend_Album($RecommendID,''));
			$ablumPhotoInfoAry = $ldc->Get_Recommend_Album_Photo($RecommendID,'');
			foreach($ablumPhotoInfoAry as $aAblumPhoto){
				if($aAblumPhoto['id'] == $AlbumPhotoID){
					$AlbumID = $aAblumPhoto['album_id'];
				}
			}
		}
		
		# Get album info
		$album 			= $ldc->getAlbum($AlbumID);
		$album_Arr		= BuildMultiKeyAssoc($ldc->getAlbumPhotos($AlbumID), 'Sequence');
		$album_size 	= count($album_Arr);
		
		if($RecommendID){
			$album_Arr = array();
			for($i=0; $i<count($ablumPhotoInfoAry); $i++){
				$album_Arr[] = $ldc->getAlbumPhoto($ablumPhotoInfoAry[$i]['id']);
				if($ablumPhotoInfoAry[$i]['id'] == $AlbumPhotoID){
					$currentSeq = $i;
				}
			}
			$album_size 	= count($album_Arr);
		}
		
		# Get current photo url
		$photo	 		= $ldc->getAlbumPhoto($AlbumPhotoID);
		$photo_url 		= libdigitalchannels::getPhotoFileName('photo', $album, $photo);
	    
	    # Get current photo / video info
		if(!$RecommendID)
			$currentSeq 	= $photo['Sequence'];
		$photo_ext 		= pathinfo($photo['title'], PATHINFO_EXTENSION);
		$isVideo		= (in_array(strtoupper($photo_ext),$videoFormat))? true : false;
//		if($isVideo)
//			$videoDetails		 	= libdigitalchannels::getVideoDetails($PATH_WRT_ROOT.$photo_url);
//		else 
//			list($width, $height) 	= getimagesize($PATH_WRT_ROOT.$photo_url);
		
		# Get orignal photo path
		$orignal_url = libdigitalchannels::getPhotoFileName('original', $album, $photo);
		if(!file_exists($file_path.$orignal_url))
			$orignal_url = libdigitalchannels::getPhotoFileName('photo', $album, $photo);
		
		# Get album photo slider
		$slider_array = array();
		$slider_size  = ($album_size >= 7)? 7 : $album_size;
		$slider_start = $currentSeq - floor(($slider_size - 1) / 2);
		
		# loop each thumbnail
		for($i = 0; $i < $slider_size; $i++, $slider_start++){
			# Get current thumbnail
			$sequence 	= ($slider_start < 0)? $album_size + $slider_start : $slider_start;
			$sequence 	= ($sequence >= $album_size)? $slider_start - $album_size : $sequence;
			$thumbnail 	= $album_Arr[$sequence];
			
			# Details of Thumbnail
			if($RecommendID)
				$album 		= $ldc->getAlbum($album_Arr[$sequence]['album_id']);
			$thumbnail_url	= libdigitalchannels::getPhotoFileName('thumbnail', $album, $thumbnail);
			$thumbnail_ext 	= pathinfo($thumbnail['title'], PATHINFO_EXTENSION);
			$t_isVideo		= (in_array(strtoupper($thumbnail_ext),$videoFormat))? true : false;
			
			# Resize Thumbnail
			list($t_width, $t_height) 	= getimagesize($file_path.$thumbnail_url);
//			if($t_height > $t_width)
//				$thumbnail_style = "style='max-height:42px;";
//			else
//				$thumbnail_style = "style='max-width:42px;";
//			$thumbnail_style .= " left: auto; right: auto;'";
//			$thumbnail_style .= $t_isVideo? " margin-top: -40px;'" : "'";
			
			$slider_array[] = array($thumbnail_url, $album_Arr[$sequence]['id'], $t_isVideo, $thumbnail_style);
			if((int)$sequence === (int)$currentSeq){		
				$currentSeqi = $i;
			}
		}
	   
	    list($prev_photo_url, $prev_photo_id) = ((int)$currentSeqi !== 0)? $slider_array[$currentSeqi - 1] : end($slider_array);
	    list($next_photo_url, $next_photo_id) = ((int)$currentSeqi !== ((int)$slider_size - 1))? $slider_array[$currentSeqi + 1] : $slider_array[0];
				
		$x.='<script>
				load_photo();
				function load_photo(){
					parent.document.getElementById("photo_content").src = "/home/eAdmin/ResourcesMgmt/DigitalChannels/organize/view_original_photo.php?isvideo='.$isVideo.'&url='.$photo_url.'";
				}
			</script>
			<!--<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$prev_photo_id.'" class="view_images_nav_prev">Previous Image</a>
			<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'" class="view_images_nav_next">Next Image</a>-->
			<div onmouseover="document.getElementById(\'view_images_nav_prev\').style.opacity=1;" onmouseout="document.getElementById(\'view_images_nav_prev\').style.opacity=\'\';" onclick="window.location=\''.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$prev_photo_id.'\'" style="position: relative;top: 0;left: 0;float:left; background-color: grey; height: 70px; width: 23px;">
			<a id="view_images_nav_prev" href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&RecommendID='.$RecommendID.'&AlbumPhotoID='.$prev_photo_id.'" class="view_images_nav_prev">Previous Image</a>
			<!--<input type="button" onclick="window.location.href=\''.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$prev_photo_id.'\'" value="<"></input>-->
			</div><div onmouseover="document.getElementById(\'view_images_nav_next\').style.opacity=1;" onmouseout="document.getElementById(\'view_images_nav_next\').style.opacity=\'\';" onclick="window.location=\''.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'\'" style="position: relative;top: 0;right: 0;float:right; background-color: grey; height: 70px; width: 23px;">
			<a id="view_images_nav_next" href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&RecommendID='.$RecommendID.'&AlbumPhotoID='.$next_photo_id.'" class="view_images_nav_next">Previous Image</a>
			<!--<input type="button" onclick="window.location.href=\''.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'\'" value=">"></input>-->
			</div>';
 
	return $x;
	
	}
	
	function Display_Index_Page(){
		global $PATH_WRT_ROOT, $Lang, $UserID, $userBrowser, $file_path, $LAYOUT_SKIN;
		
		$ldc = new libdigitalchannels();
		
		
		$recommendPhotoArray = $ldc->Get_Recommend_Photo_Array_New();
		$recommendPhotoCount = count($recommendPhotoArray);
		
		$IsRecommendPhoto = true;
		if(!$recommendPhotoArray){
			$recommendPhotoArray = $ldc->Get_Recommend_Photo_Array();
			$recommendPhotoCount = 10;
			$IsRecommendPhoto = false;
		}
		$photoMostHitArray = $ldc->Get_Most_Hit_Array('Photo');
		$videoMostHitArray = $ldc->Get_Most_Hit_Array('Video');
		$lastestPhotoArray = $ldc->Get_Lastest_Photo_Array();
		$lastestAlbumArray = $ldc->Get_Lastest_Album_Array();
		
		//debug_pr($lastestAlbumArray);
		
		$x ='<div class="album_content_portal">        
		        <div style="margin:0 auto 0 auto; width:1100px">
		        	<div class="col_left"> 
		         		<div class="photo_container">';
		         		for($i=0; $i<$recommendPhotoCount; $i++){
		         			if(!$recommendPhotoArray[$i]){
		         				break;
		         			}
		         			
		         			#check user enjoy or not
							$enjoyed = count($ldc->Get_Album_Photo_Favorites_User($recommendPhotoArray[$i]['id'], $UserID)) > 0;

//		         			if($recommendPhotoArray[$i]['TotalFav'] <= 0){
//		         				break;
//		         			}
		         			if($recommendPhotoArray[$i]['type'] == 'Video' && false)
								$videoDetails		 	= libdigitalchannels::getVideoDetails($file_path.$recommendPhotoArray[$i]['FilePath']);
							else 
								list($width, $height) 	= getimagesize($file_path.$recommendPhotoArray[$i]['FilePath']);
		         			
		         			# Define photo or video
							if($recommendPhotoArray[$i]['type'] == 'Video'){
								if($height > $width)
									$photo_style = "style='max-height:450px; height:100%'";
								else
									$photo_style = "style='max-width:678px; width:100%'";
								$photoDisplay = '<a href="categories/recommend_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&RecommendPhotoID='.$recommendPhotoArray[$i]['id'].'">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.($recommendPhotoArray[$i]['type'] == 'Video'?str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])):$recommendPhotoArray[$i]['FilePath']).'" alt="" data-description="" '.$photo_style.' /></a>';
								if(!$IsRecommendPhoto)
									$photoDisplay = '<a href="categories/category_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$recommendPhotoArray[$i]['id'].'">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.($recommendPhotoArray[$i]['type'] == 'Video'?str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])):$recommendPhotoArray[$i]['FilePath']).'" alt="" data-description="" '.$photo_style.' /></a>';
							
//								if($videoDetails['Height'] > $videoDetails['Width'])
//									$photo_style = "style='height:100%; max-height:450px;max-width:678px;'";
//								else
//									$photo_style = "style='width:100%; max-width:678px;max-height:450px;'";
//								$photoDisplay = '<a href="javascript: void(0);">
//													<video '.$photo_style.' controls>
//													    <source type="video/mp4" src="'.$recommendPhotoArray[$i]['FilePath'].'"></source>
//													    <!--<source type="video/ogg" src="mov_bbb.ogg"></source>-->
//													    Your browser does not support HTML5 video.
//													</video>
//												</a>';
							} else {
								if($height > $width)
									$photo_style = "style='max-height:450px;max-width:678px'";
								else
									$photo_style = "style='max-width:678px;max-height:450px'";
								//$photoDisplay = '<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'"><img src="'.$photo_url.'"  alt="" data-description="" '.$photo_style.'/></a>';
								$photoDisplay = '<div style="position: relative;'.($userBrowser->browsertype == "Safari"?'top: 50%;-webkit-transform: translateY(-50%);':'top: 50%;transform: translateY(-50%);').'">';
								if(!$IsRecommendPhoto)
									$photoDisplay .= '<a href="categories/category_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$recommendPhotoArray[$i]['id'].'">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.($recommendPhotoArray[$i]['type'] == 'Video'?str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])):$recommendPhotoArray[$i]['FilePath']).'" alt="" data-description="" '.$photo_style.' /></a>';
								else
									$photoDisplay .= '<a href="categories/recommend_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&RecommendPhotoID='.$recommendPhotoArray[$i]['id'].'">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.($recommendPhotoArray[$i]['type'] == 'Video'?str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])):$recommendPhotoArray[$i]['FilePath']).'" alt="" data-description="" '.$photo_style.' /></a>';
								$photoDisplay .= '</div>';
							}
		         			
							$description = Get_Lang_Selection($recommendPhotoArray[$i]['chi_description'], $recommendPhotoArray[$i]['description']);
							$description = $description ? $description : $recommendPhotoArray[$i]['title'];
							
		             		$x.='<div class="photo_block photo_hover" '.($i>0?'style="display:none"':'').'>
		               			<div class="photo_details"> 
		               				<div class="photo_overlay">
		              					<a href="categories/category_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$recommendPhotoArray[$i]['id'].'"><span class="title_portal">'.$description.'</span></a>
		               					<span class="icon_photo">
		                					<a title="'.($enjoyed?$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto']:$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto']).'" href="javascript:void(0);" onclick="'.($ldc->isAlbumReadable($recommendPhotoArray[$i]['album_id'])?'Update_Photo_Favorites('.$recommendPhotoArray[$i]['id'].');':'').'" id="icon_star_'.$recommendPhotoArray[$i]['id'].'" class="icon_star '.($enjoyed?'active':'').'"><span id="enjoy_count_'.$recommendPhotoArray[$i]['id'].'">'.$recommendPhotoArray[$i]['TotalFav'].'</span></a>
		                 					<a title="Comment this photo" href="categories/category_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$recommendPhotoArray[$i]['id'].'" class="icon_comment_small">'.$ldc->Get_Album_Photo_Comment_Total($recommendPhotoArray[$i]['id']).'</a>
		                  					<!--<a title="Share this photo" href="#" class="icon_share"></a>-->
		                 				</span>
		                   			</div>
								</div>                   
		             			<div class="photo_images_list">
		                        	<ul>
		                            	<li><span>'.$photoDisplay.'</span></li>
		                                <li><span><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/685x455/12.jpg" alt=""/></a></span>                                        </li>
		                            </ul>
		               			</div>
		            		</div>';
		         		}
		             		$x.='<div class="clearfix"></div>
		            		<p class="spacer"></p>
		             		<div class="photo_gallery_title_recommend">'.($IsRecommendPhoto?$Lang['DigitalChannels']['Recommend']['Reommendation']:$Lang['DigitalChannels']['General']['MostPopular']).'
		            			<span> <input type="button" class="btn_arrow_right" onclick="NextPhoto()" value="" /></span>
		             			<span><input type="button" class="btn_arrow_left" onclick="PrevPhoto()" value="" /></span>
		             			<span><a href="more.php?type=Recommend">'.$Lang['DigitalChannels']['General']['more'].'..</a></span></div>
		         			</div>		
		<div class="lastest_album">
		<ul>';
		for($i=0; $i<8; $i++){
			if(!$lastestAlbumArray[$i]){
 				break;
 			}
		  $x.='<li><div class="photo_gallery"> <a href="categories/category_album_photo.php?AlbumID='.$lastestAlbumArray[$i]['album_id'].'"><img src="'.$PATH_WRT_ROOT.$lastestAlbumArray[$i]['FilePath'].'" alt=""/></a></div>
		  </li>';
		}
		    $x.='<!--<li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/06.jpg" alt=""/></a></div>
		    </li>
		      <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/07.jpg" alt=""/></a></div>
		      </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/09.jpg" alt=""/></a></div>
		        </li>
		          <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/02.jpg" alt=""/></a></div>
		          </li>
		            <li><div class="photo_gallery"><a href="#"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/05.jpg" alt=""/></a></a></div>
		            </li>
		      <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/04.jpg" alt=""/></a></div>
		      </li>
		       <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/03.jpg" alt=""/></a></div>
		       </li>-->
		      
		  </ul>
		            <p class="spacer"></p>	
		             <div class="photo_gallery_title_latest">'.$Lang['DigitalChannels']['General']['LastestAlbum'].'<span><a href="more.php?type=LastestAlbum">'.$Lang['DigitalChannels']['General']['more'].'..</a></span></div></div>
		  </div>
		  
		  <div class="col_right">       
		            		
		 <div class="album_container">
		  <div class="photo_list">
		  <ul>';
		  for($i=0; $i<6; $i++){
		  	if(!$photoMostHitArray[$i]){
 				break;
 			}
			  $x .='<li><div class="photo_gallery"><a href="categories/category_album_photo_view.php?AlbumID='.$photoMostHitArray[$i]['album_id'].'&AlbumPhotoID='.$photoMostHitArray[$i]['id'].'"><img src="'.$PATH_WRT_ROOT.$photoMostHitArray[$i]['FilePath'].'" alt=""/></a></div>
			  </li>';
		  }
		    $x .='<!--<li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/02.jpg" alt=""/></a></div>
		    </li>
		      <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/06.jpg" alt=""/></a></div>
		      </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/04.jpg" alt=""/></a></div>
		        </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/07.jpg" alt=""/></a></div>
		        </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/08.jpg" alt=""/></a></div>
		        </li>-->
		  </ul>
		 <p class="spacer"></p>
		 <div class="photo_gallery_title_most_hit">'.$Lang['DigitalChannels']['General']['MostHitPhoto'].'<span><a href="more.php?type=MostHitPhoto">'.$Lang['DigitalChannels']['General']['more'].'..</a></span></div>
		 </div></div>
		
		  <div class="album_container">
		  <div class="photo_list">
		  <ul>';
		  for($i=0; $i<6; $i++){
		  	if(!$videoMostHitArray[$i]){
 				break;
 			}
			  $x.='<li><div class="photo_gallery"><a href="categories/category_album_photo_view.php?AlbumID='.$videoMostHitArray[$i]['album_id'].'&AlbumPhotoID='.$videoMostHitArray[$i]['id'].'"><span class="play_button_small"></span><img src="'.$PATH_WRT_ROOT.$videoMostHitArray[$i]['FilePath'].'" alt=""/></a></div>
			  </li>';
		  }
		    $x.='<!--<li><div class="photo_gallery"><a href="#"><span class="play_button_small"></span><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/01.jpg" alt=""/></a></div>
		    </li>
		      <li><div class="photo_gallery"><a href="#"><span class="play_button_small"></span><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/03.jpg" alt=""/></a></div>
		      </li>
		        <li><div class="photo_gallery"><a href="#"><span class="play_button_small"></span><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/05.jpg" alt=""/></a></div>
		        </li>
		        <li><div class="photo_gallery"><a href="#"><span class="play_button_small"></span><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/04.jpg" alt=""/></a></div>
		        </li>
		        <li><div class="photo_gallery"><a href="#"><span class="play_button_small"></span><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/09.jpg" alt=""/></a></div>
		        </li>-->
		  </ul>
		 <p class="spacer"></p>
		 <div class="photo_gallery_title_most_hit">'.$Lang['DigitalChannels']['General']['MostHitVideo'].'<span><a href="more.php?type=MostHitVideo">'.$Lang['DigitalChannels']['General']['more'].'..</a></span></div>
		 </div></div>
		 
		 
		   <div class="album_container">
		  <div class="photo_list">
		  <ul>';
		  for($i=0; $i<6; $i++){
		  	if(!$lastestPhotoArray[$i]){
 				break;
 			}
			  $x .='<li><div class="photo_gallery"><a href="categories/category_album_photo_view.php?AlbumID='.$lastestPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$lastestPhotoArray[$i]['id'].'">'.($lastestPhotoArray[$i]['type'] == 'Video'?'<span class="play_button_small"></span>':'').'<img src="'.$PATH_WRT_ROOT.$lastestPhotoArray[$i]['FilePath'].'" alt=""/></a></div>
			  </li>';
		  }
		    $x.='<!--<li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/04.jpg" alt=""/></a></div>
		    </li>
		      <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/03.jpg" alt=""/></a></div>
		      </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/09.jpg" alt=""/></a></div>
		        </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/07.jpg" alt=""/></a></div>
		        </li>
		        <li><div class="photo_gallery"><a href="#"><img src="../../../../images/'.$LAYOUT_SKIN.'/digital_channels/album/student/08.jpg" alt=""/></a></div>
		        </li>-->
		  </ul>
		 <p class="spacer"></p>
		 <div class="photo_gallery_title_latest">'.$Lang['DigitalChannels']['General']['LastestPhoto'].'<span><a href="more.php?type=LastestPhoto">'.$Lang['DigitalChannels']['General']['more'].'..</a></span></div>
		 </div></div>
           </div>';
           return $x;
	}
	
	function Get_Category_Table() {
		
	}
	
	# Select Target Group of Album
	function Get_Target_Group_Select($shared_groups){
		global $PATH_WRT_ROOT, $intranet_session_language, $Lang, $intranet_root, $sys_custom;
		global $linterface, $junior_mck;
	
        include_once($PATH_WRT_ROOT."includes/libgroup.php");
        include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
        
        if($sys_custom['DHL'])
        {
        	include_once($intranet_root.'/includes/DHL/libdhl.php');
        	$libdhl = new libdhl();
        	
        	// [2019-1003-1204-18226]
        	# DHL Digital Channels - admin can view all Companies > Departments
    	    global $ldc;
    	    $urlParms = $ldc->isAdminUser() ? '&isAdmin=1' : '';
        }
        
        $x = "";
        $group_ary = array();
        
        $lg = new libgroup();
        $lgc = new libgroupcategory();
        
        if($junior_mck > 0){
        	$title_field = 'Title';
        }else{
        	$title_field = $intranet_session_language =="en" ? "Title":"TitleChinese";      
        }
        $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
        
		$group_category_ary = $lgc->returnCats();
	
		if($junior_mck > 0){ // EJ
			foreach($group_category_ary as $i=> $r){
				$group_category_ary[$i][1] = convert2unicode($r[1]);
				$group_category_ary[$i]['CategoryName'] = convert2unicode($r['CategoryName']);
			}
		}
		
		# Get details of selected groups
		$groupList = (count($shared_groups) > 0)? $lg->returnGroup("", implode(",", $shared_groups)) : array();
		$selectedList = BuildMultiKeyAssoc($groupList, "GroupID");
        
        # List of selected Target Group
        if(count($group_category_ary) > 0){
			foreach($group_category_ary as $k=>$d)
			{
				$pass_CurrentAcademicYearID = $d['GroupCategoryID']==0 ? "" : $CurrentAcademicYearID;
				$group_data_ary = $lg->returnGroupsByCategory($d['GroupCategoryID'], $pass_CurrentAcademicYearID);
				
				if($junior_mck > 0){ // EJ
					foreach($group_data_ary as $i=> $r){
						$group_data_ary[$i][1] = convert2unicode($r[1]);
						$group_data_ary[$i][$title_field] = convert2unicode($r[$title_field]);
					}
				}
				
				if(count($group_data_ary) > 0){
					foreach($group_data_ary as $k2 => $d2)
					{
						# Skip if not stored groups
						if(!in_array($d2['GroupID'],$shared_groups))
							continue;
//						$group_ary[$d['CategoryName']][$d2['GroupID']] = $d2[$title_field];
						$group_ary[$d2['GroupID']] = "(".$d['CategoryName'].")".$d2[$title_field];
					}
				}
			}	
		}
		
		if($sys_custom['DHL']){
			$selected_group_ids = $shared_groups;
			$filter_map = array('GroupIDToDisplayTitle'=>1);
			if(count($selected_group_ids)>0){
				$filter_map['GroupID'] = $selected_group_ids;
			}else{
				$filter_map['GroupID'] = array(-1);
			}
			$group_ary = $libdhl->getIntranetGroupRecords($filter_map);
		}
		
		$x .= "<table id='select_group_lists' ".($shared_groups?"":"style='display:none'").">
					<td width='27%'>
						<span class='album_form_textarea'>";
//						$x .="<select name='select_groups[]' class='select_groups' size='10' multiple='multiple'>\n";
//	    if(count($groupList) > 0){
//		    foreach($groupList as $targetGroup){
//		    	$x .= "		<option value=".$targetGroup['GroupID'].">".$targetGroup['Title']."</option>";
//		    }
//	    }
//		$x .= "			</select>";
		$x .= getSelectByAssoArray($group_ary, "name='select_groups[]' id='select_groups' class='select_groups album_form_textarea_select' size='10' multiple='multiple' style='width:260px;'", "", 0, 1);
		$x.="</span>";
		
		if($sys_custom['DHL']){
			$x .= "<div style=\"clear:both\">";
			$x .= $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=select_groups[]&SelectGroupOnly=1&page_title=SelectAudience&DisplayGroupCategory=1$urlParms',16);");
			$x .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "remove_selection_option('select_groups',0);");
			$x .= "</div>";
			$x .= "</td>\n";
			$x .= "</table>";
			
			return $x;
		}
		
		$x.="</td>\n";
					
		# Transfer Buttons
		$x .= "		<td width='12%' align='center'>";
        $x .= $linterface->GET_BTN("<< ".$Lang['DigitalChannels']['General']['Select'], "submit", "checkOptionTransfer(this.form.elements['groups[]'],this.form.elements['select_groups[]']);return false;", "submit2") . "<br /><br />";
        $x .= $linterface->GET_BTN($Lang['DigitalChannels']['General']['Remove']." >>", "submit", "checkOptionTransfer(this.form.elements['select_groups[]'],this.form.elements['groups[]']);return false;", "submit23");
        $x .= "		</td>\n";
        
        # List of Target Group
		$x .= "		<td width='27%'>";
		$group_ary = array();
		if(count($group_category_ary) > 0){
			foreach($group_category_ary as $k=>$d)
			{
				$pass_CurrentAcademicYearID = $d['GroupCategoryID']==0 ? "" : $CurrentAcademicYearID;
				$group_data_ary = $lg->returnGroupsByCategory($d['GroupCategoryID'], $pass_CurrentAcademicYearID);

				if($junior_mck > 0){ // EJ
					foreach($group_data_ary as $i=> $r){
						$group_data_ary[$i][1] = convert2unicode($r[1]);
						$group_data_ary[$i][$title_field] = convert2unicode($r[$title_field]);
					}
				}
				
				if(count($group_data_ary) > 0){
					foreach($group_data_ary as $k2 => $d2)
					{
						# Skip if duplicate
						if(isset($selectedList[$d2['GroupID']]))
							continue;
//						$group_ary[$d['CategoryName']][$d2['GroupID']] = $d2[$title_field];
						$group_ary[$d2['GroupID']] = "(".$d['CategoryName'].")".$d2[$title_field];
					}
				}
			}	
		}
		
		if($sys_custom['DHL']){
			$exclude_group_ids = Get_Array_By_Key($groupList,'GroupID');
			$filter_map = array('GroupIDToDisplayTitle'=>1);
			if(count($exclude_group_ids)>0){
				$filter_map['ExcludeGroupID'] = $exclude_group_ids;
			}
			$group_ary = $libdhl->getIntranetGroupRecords($filter_map);
		}
		
		$x .= getSelectByAssoArray($group_ary, "multiple='multiple' size='10' name='groups[]' class='groups album_form_textarea_select' style='width:260px;'", "", 0, 1);
		
		$x .= "		</td>
					<td width='36%'>
						&nbsp;
					</td>
				</table>";						
		return $x;
	}
	
	function time_passed($timestamp){
		global $Lang;
		
	    //type cast, current time, difference in timestamps
	    $timestamp      = (int) $timestamp;
	    $current_time   = time();
	    $diff           = $current_time - $timestamp;
	   
	    //intervals in seconds
	    $intervals      = array (
	        'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
	    );
	   
	    //now we just find the difference
	    if ($diff == 0)
	    {
	        return $Lang['DigitalChannels']['General']['JustNow'];
	    }   
	
	    if ($diff < 60)
	    {
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['SecondAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['SecondsAgo'];
	    }       
	
	    if ($diff >= 60 && $diff < $intervals['hour'])
	    {
	        $diff = floor($diff/$intervals['minute']);
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['MinuteAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['MinutesAgo'];
	    }       
	
	    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
	    {
	        $diff = floor($diff/$intervals['hour']);
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['HourAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['HoursAgo'];
	    }   
	
	    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
	    {
	        $diff = floor($diff/$intervals['day']);
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['DayAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['DaysAgo'];
	    }   
	
	    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
	    {
	        $diff = floor($diff/$intervals['week']);
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['WeekAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['WeeksAgo'];
	    }   
	
	    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
	    {
	        $diff = floor($diff/$intervals['month']);
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['MonthAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['MonthsAgo'];
	    }   
	
	    if ($diff >= $intervals['year'])
	    {
	        $diff = floor($diff/$intervals['year']);
	        return $diff == 1 ? $diff . ' '.$Lang['DigitalChannels']['General']['YearAgo'] : $diff . ' '.$Lang['DigitalChannels']['General']['YearsAgo'];
	    }
	}
	
	function Get_Add_Category_Pic_Form($CategoryCode) {
		global $PATH_WRT_ROOT, $Lang, $sys_custom;

		$ldc = new libdigitalchannels();

		//-- get group member list
		$GroupMemberList = $ldc->Get_Category_Pic($CategoryCode);
		//-- get group member list		
		
		$x = '<input type="hidden" name="CategoryCode" id="CategoryCode" value="'.$CategoryCode.'">
					<div class="edit_pop_board" style="height:440px;">
						<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:380px;">
					    <table class="form_table">
					    	<tr><td>'.$Lang['DigitalChannels']['Settings']['Helper'].': </td></tr>
							<tr>

							    <td >
							    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
				              <tr>
				              	<td>
				              		<table width="100%" border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td width="50%" bgcolor="#EEEEEE">';
												if (!$sys_custom['DHL']) {
													$x .= $Lang['DigitalChannels']['Settings']['UserType'].':
															<select name="IdentityType" id="IdentityType" onchange="Get_User_List();">
																<option value="Teaching">'.$Lang['DigitalChannels']['Settings']['TeachingStaff'].'</option>
																<option value="NonTeaching">'.$Lang['DigitalChannels']['Settings']['SupportStaff'].'</option>
															</select>
															';
												}	

		// get class list
		$CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
		$CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$fcm = new form_class_manage();
		$YearClassList = $fcm->Get_Class_List_By_Academic_Year($CurrentAcademicYearID);
		$x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_User_List();" style="display:none;">';
		$x .= '<option value="">'.$Lang['DigitalChannels']['Settings']['SelectClass'].'</option>';
		  for ($i=0; $i< sizeof($YearClassList); $i++) {
		  	if ($YearClassList[$i]['YearID'] != $YearClassList[$i-1]['YearID']) {
		  		if ($i == 0)
		  			$x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
		  		else {
		  			$x .= '</optgroup>';
		  			$x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
		  		}
		  	}
			
		  	//$x .= '<option value="'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
			$x .= '<option value="'.$YearClassList[$i]['ClassTitleEN'].'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
	
		  	if ($i == (sizeof($YearClassList)-1))
		  		$x .= '</optgroup>';
		  }
	  $x .= '</select>';

		$x .= '<div id="ParentStudentLayer" name="ParentStudentLayer">';
		$x .= '</div>';
		$x .= '
														</td>
													<td width="40"></td>
													<td width="50%" bgcolor="#EFFEE2" class="steptitletext">'.$Lang['DigitalChannels']['Settings']['SelectedUser'].' </td>
												</tr>
												<tr>
												<td bgcolor="#EEEEEE" align="center">';
		// get user list with selected criteria
		$x .= '							<div id="AvalUserLayer">';
		for ($i=0; $i< sizeof($GroupMemberList); $i++) {
			$AddUserID[] = $GroupMemberList[$i]['UserID'];
		}
		$x .= $this->Get_User_Selection($AddUserID,"Teaching","","",$CategoryCode);
		$x .= '
												</div>';

		$x .= '
												<span class="tabletextremark">'.$Lang['DigitalChannels']['Settings']['CtrlMultiSelectMessage'].' </span>
												</td>
												<td><input name="AddAll" onclick="Add_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="'.$Lang['Btn']['AddAll'].'"/>
													<br />
													<input name="Add" onclick="Add_Selected_Class_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="'.$Lang['Btn']['AddSelected'].'"/>
													<br /><br />
													<input name="Remove" onclick="Remove_Selected_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveSelected'].'"/>
													<br />
													<input name="RemoveAll" onclick="Remove_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveAll'].'"/>
												</td>
												<td bgcolor="#EFFEE2" id="SelectedUserCell">';

			// selected User list
			$x .= '<select name="AddUserID[]" id="AddUserID[]" class="AddUserID" size="10" style="width:99%" multiple="true">';
			
			for ($i=0; $i< sizeof($GroupMemberList); $i++) {
				$class_info = ($GroupMemberList[$i]['ClassName']!="" && $GroupMemberList[$i]['ClassNumber']!="") ?  $GroupMemberList[$i]['ClassName']."-".$GroupMemberList[$i]['ClassNumber']. " " : "";			
				$x .= '<option value="'.$GroupMemberList[$i]['UserID'].'" >';
				$x .= $class_info . $GroupMemberList[$i]['PicName'];
				$x .= '</option>';
			}
			
			$x .= '</select>';
			$x .= '						</td>
											</tr>
											<tr>
												<td width="50%" bgcolor="#EEEEEE">';
		$x .= $Lang['DigitalChannels']['Settings']['Or'].'<Br>';
		$x .= $Lang['DigitalChannels']['Settings']['SearchUser'].'<br>';

		$x .= '
												<!--<div class="Conntent_search" style="float:left;">-->
												<div style="float:left;">
													<input type="text" id="UserSearch_'.$CategoryCode.'" name="UserSearch_'.$CategoryCode.'" value=""/>
												</div>
											</td>
											<td>&nbsp;</td>
										</tr>
									</table>
									<p class="spacer"></p>
	              	</td>
	              </tr>
							</table>
						  <br />
						</td>
						</tr>
						</table>
						</div>
					  <div class="edit_bottom">
							<span> </span>
							<p class="spacer"></p>
							<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" type="button" class="formbutton" onclick="Add_Category_Pic(\''.$CategoryCode.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />
						  <input name="AddMemberCancelBtn" id="AddMemberCancelBtn" type="button" class="formbutton" onclick="window.top.tb_remove(); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
					    <p class="spacer"></p>
						</div>
					</div>';

		return $x;
	}
	
	function Get_User_Selection($AddUserID,$IdentityType,$YearClassSelect,$ParentStudentID,$CategoryCode) {
		global $Lang;

		if ($AddUserID[0] == "")
			$AddUserID = array();

		$ldc = new libdigitalchannels();

		//-- get group member list
		$GroupMemberList = $ldc->Get_Category_Pic($CategoryCode);
		for ($i=0; $i< sizeof($GroupMemberList); $i++) {
			$AddUserID1[] = $GroupMemberList[$i]['UserID'];
		}
		//-- get group member list		

		$AddUserID = array_merge((array)$AddUserID, (array)$AddUserID1);

		$UserList = $ldc->Get_Avaliable_User_List($AddUserID,$IdentityType,$YearClassSelect,$ParentStudentID);

		if ($IdentityType == "Parent" && $YearClassSelect != "" && trim($ParentStudentID) == "") {
			$x .= '<select name="ParentStudentID" id="ParentStudentID" style="width:99%" onchange="Get_User_List();">';
			$x .= '<option value="all">'.$Lang['libms']['GroupManagement']['SelectAll'].'</option>';
		}
		else {
			$x .= '<select name="AvalUserList[]" class="AvalUserList" id="AvalUserList[]" size="10" style="width:99%" multiple="true">';
		}
		for ($i=0; $i< sizeof($UserList); $i++) {
			$class_info = ($UserList[$i]['ClassName']!="" && $UserList[$i]['ClassNumber']!="") ?  $UserList[$i]['ClassName']."-".$UserList[$i]['ClassNumber']. " " : "";			
			$x .= '<option value="'.$UserList[$i]['UserID'].'">';
			$x .= $class_info . $UserList[$i]['Name'];
			$x .= '</option>';
		}
		$x .= '</select>';

		return $x;
	}
	
	# JQuery Datepicker generator
	function GET_DATE_PICKER($Name,$DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum") {
	    global $Defined,$PATH_WRT_ROOT,$image_path,$LAYOUT_SKIN,$Lang;
	    
	    $DateFormat = ($DateFormat == "")? "yy-mm-dd":$DateFormat;
	    if(!$CanEmptyField)
	        $DefaultValue = ($DefaultValue == "")? date('Y-m-d'):$DefaultValue;
	        $ID = ($ID == "")? $Name:$ID;
	        $WarningLayerID = ($ExtWarningLayerID == "")? 'DPWL-'.$ID:$ExtWarningLayerID;
	        $Disabled = ($Disable)? 'disabled' : '';
	        
	        $x = "";
	        if(!$CanEmptyField)
	            $x .= '<input type=text name="'.$Name.'" id="'.$ID.'" value="'.$DefaultValue.'" size=10 maxlength=10 class="'.$cssClass.'" '.$OtherMember.' onkeyup="Date_Picker_Check_Date_Format(this,\''.$WarningLayerID.'\',\''.$ExtWarningLayerContainer.'\');" '.$Disabled.'>';
	            else
	                $x .= '<input type=text name="'.$Name.'" id="'.$ID.'" value="'.$DefaultValue.'" size=10 maxlength=10 class="'.$cssClass.'" '.$OtherMember.' onkeyup="Date_Picker_Check_Date_Format_CanEmpty(this,\''.$WarningLayerID.'\',\''.$ExtWarningLayerContainer.'\');" '.$Disabled.'>';
	                if ($ExtWarningLayerID == "")
	                    $x .= '<span style="color:red;" id="'.$WarningLayerID.'"></span>';
	                    $x .= "\n";
	                    
	                    if ($Defined<1) {
	                        if($SkipIncludeJS!=1)
	                        {
	                            $x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.js"></script>
						<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.css" type="text/css" />';
	                        }
	                        
	                        $x .= '<script>
                            jQuery(document).ready(function($){
    							$.datepick.setDefaults({
    								showOn: \'both\',
    								buttonImageOnly: true,
    								buttonImage: \''.$image_path.'/'.$LAYOUT_SKIN.'/icon_calendar_off.gif\',
    								buttonText: \'Calendar\'});
                            });
								    
						function Date_Picker_Check_Date_Format(DateObj,WarningLayer,WarningLayerContainer) { ';
	                        
	                        $x .= 'if (!check_date_without_return_msg(DateObj)) {';
	                        
	                        if ($ExtWarningLayerContainer != "") {
	                            $x .= '	if (WarningLayerContainer != "")
									$(\'#\'+WarningLayerContainer).css(\'display\',\'\');';
	                        }
	                        $x .= '		$(\'#\'+WarningLayer).html(\''.$Lang['General']['InvalidDateFormat'].'\');
							}
							else {';
	                        if ($ExtWarningLayerContainer != "") {
	                            $x .= '	if (WarningLayerContainer != "")
									$(\'#\'+WarningLayerContainer).css(\'display\',\'none\');';
	                        }
	                        $x .= '		$(\'#\'+WarningLayer).html(\'\');
							}
						}
	                            
						function Date_Picker_Check_Date_Format_CanEmpty(DateObj,WarningLayer,WarningLayerContainer) {
							';
	                        
	                        $x .= 'if (!check_date_allow_null_30(DateObj)) {';
	                        
	                        if ($ExtWarningLayerContainer != "") {
	                            $x .= '	if (WarningLayerContainer != "")
									$(\'#\'+WarningLayerContainer).css(\'display\',\'\');';
	                        }
	                        $x .= '		$(\'#\'+WarningLayer).html(\''.$Lang['General']['InvalidDateFormat'].'\');
							}
							else {';
	                        if ($ExtWarningLayerContainer != "") {
	                            $x .= '	if (WarningLayerContainer != "")
									$(\'#\'+WarningLayerContainer).css(\'display\',\'none\');';
	                        }
	                        $x .= '		$(\'#\'+WarningLayer).html(\'\');
							}
						}
	                            
						</script>
						';
	                    }
	                    $x .= "<script>
						jQuery('input#".$ID."').ready(function($){
							$('input#".$ID."').datepick({
								".(($MaskFunction != "")? 'beforeShowDay: '.$MaskFunction.',':'')."
								dateFormat: '".$DateFormat."',
								dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
								changeFirstDay: false,
								firstDay: 0,
								onSelect: function(dateText, inst) {
				";
	                    if($CanEmptyField)
	                        $x .= "Date_Picker_Check_Date_Format_CanEmpty(document.getElementById('".$ID."'),'".$WarningLayerID."','".$ExtWarningLayerContainer."');";
	                        else
	                            $x .= "Date_Picker_Check_Date_Format(document.getElementById('".$ID."'),'".$WarningLayerID."','".$ExtWarningLayerContainer."');";
	                            
	                            $x .= $OnDatePickSelectedFunction."
									}
								});
							});
					</script>
					";
	                            
	                            $Defined++;
	                            //debug_pr($ID);
	                            return $x;
	}

    function Display_Index_Page_highLight(){
        global $PATH_WRT_ROOT, $Lang, $UserID, $userBrowser, $file_path;

        $ldc = new libdigitalchannels();


        $recommendPhotoArray = $ldc->Get_Recommend_Photo_Array_New();
        $recommendPhotoCount = count($recommendPhotoArray);

        $IsRecommendPhoto = true;
        if(!$recommendPhotoArray){
            $recommendPhotoArray = $ldc->Get_Recommend_Photo_Array();
            $recommendPhotoCount = 10;
            $IsRecommendPhoto = false;
        }
        $photoMostHitArray = $ldc->Get_Most_Hit_Array('Photo');
        $videoMostHitArray = $ldc->Get_Most_Hit_Array('Video');
        $lastestPhotoArray = $ldc->Get_Lastest_Photo_Array();
        $lastestAlbumArray = $ldc->Get_Lastest_Album_Array();

        //debug_pr($lastestAlbumArray);

        $x ='<div>
		      <div class="photo_container">';
        for($i=0; $i<$recommendPhotoCount; $i++){
            if(!$recommendPhotoArray[$i]){
                break;
            }

            #check user enjoy or not
            $enjoyed = count($ldc->Get_Album_Photo_Favorites_User($recommendPhotoArray[$i]['id'], $UserID)) > 0;

            //		         			if($recommendPhotoArray[$i]['TotalFav'] <= 0){
            //		         				break;
            //		         			}
            if($recommendPhotoArray[$i]['type'] == 'Video' && false){
                $videoDetails		 	= libdigitalchannels::getVideoDetails($file_path.$recommendPhotoArray[$i]['FilePath']);
            }
            else{
                list($width, $height) 	= getimagesize($file_path.$recommendPhotoArray[$i]['FilePath']);
            }

            # Define photo or video
            if($recommendPhotoArray[$i]['type'] == 'Video'){
                if($height > $width){
                    $photo_style = "style='max-height:450px; height:100%'";
                }
                else{
                    $photo_style = "style='max-width:678px; width:100%'";
                }
                $photoDisplay = '<a href="categories/recommend_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&RecommendPhotoID='.$recommendPhotoArray[$i]['id'].'" target="_blank">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.($recommendPhotoArray[$i]['type'] == 'Video'?str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])):$recommendPhotoArray[$i]['FilePath']).'" alt="" data-description="" '.$photo_style.' /></a>';
                if(!$IsRecommendPhoto){
                    $photoDisplay = '<a href="categories/category_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$recommendPhotoArray[$i]['id'].'" target="_blank">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.($recommendPhotoArray[$i]['type'] == 'Video'?str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])):$recommendPhotoArray[$i]['FilePath']).'" alt="" data-description="" '.$photo_style.' /></a>';
                }
            }else{
                if($height > $width){
                    $photo_style = "style='max-height:450px;max-width:678px; height:100%'";
                }
                else{
                    $photo_style = "style='max-width:678px;max-height:450px; width:100%'";
                }
                //$photoDisplay = '<a href="'.$photoViewPage.'?AlbumID='.$AlbumID.'&AlbumPhotoID='.$next_photo_id.'"><img src="'.$photo_url.'"  alt="" data-description="" '.$photo_style.'/></a>';
                $photoDisplay = '<div style="position: relative;' . ($userBrowser->browsertype == "Safari" ? 'top: 50%;-webkit-transform: translateY(-50%);' : 'top: 50%;transform: translateY(-50%);') . '">';
                if(!$IsRecommendPhoto){
                    $photoDisplay .= '<a href="categories/category_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&AlbumPhotoID='.$recommendPhotoArray[$i]['id'].' " target="_blank">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])).'" alt="" data-description="" '.$photo_style.' /></a>';
                }else{
                    $photoDisplay .= '<a href="categories/recommend_album_photo_view.php?AlbumID='.$recommendPhotoArray[$i]['album_id'].'&RecommendPhotoID='.$recommendPhotoArray[$i]['id'].'" target="_blank">'.($recommendPhotoArray[$i]['type'] == 'Video'?'<div class="play_button" style="position: absolute; top: 133px; left: 247px"></div>':'').'<img src="'.$PATH_WRT_ROOT.str_replace("/photo/", "/thumbnail/", str_replace(".mp4", ".jpg", $recommendPhotoArray[$i]['FilePath'])).'" alt="" data-description="" '.$photo_style.' /></a>';
                }

                $photoDisplay .= '</div>';
            }

            $x.='<div class="photo_block photo_hover" '.($i>0?'style="display:none"':'').'>
		               				
		             			<div class="photo_images_list">
		                        	<ul>
		                            	<li><span>'.$photoDisplay.'</span></li>
		                            </ul>
		               			</div>
		            		</div>';
        }
        $x.='<div class="clearfix"></div>
		            		<p class="spacer"></p>
		             		<div class="photo_gallery_title_recommend">
		            			<span> <input type="button" class="btn_arrow_right" onclick="NextPhoto()" value="" /></span>
		             			<span><input type="button" class="btn_arrow_left" onclick="PrevPhoto()" value="" /></span>
		             		</div>
		         			';
        $x .="</div></div>";
        echo $x;
    }

}
?>