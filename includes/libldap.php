<?php
# using by : 

/*        Note: At the time preparing this class, libldap is still in development and methods
        (esp. openAccount(), removeAccount(), changePassword()) may not be stable. For the usage
        in PLK Castar Primary School, only user authentication is required and account
        manipulations are NOT necessary.
*/

/* Version */
/*
 * 2019-01-04 (Carlos): Modified libldap() and validate($login,$password), added validateDomainUser($login, $password) to support direct validate with domain, do not need to search DN. 
 * 2015-08-17 (Carlos): Added getLdapHosts() for finding ldap server hosts with nslookup. Added changeToHost() to switch host and port.
 * 2012-12-07 (Yuen): modified libldap() to support using of multiple OU (login page should be customized to let user to choose the corresponding OU)
 * 2012-09-11 (Carlos): fix $this->account_control_type at constructor, fix @bind() to $this->bind()
 * 20051228 (Kenneth Wong): Change validate() - Wah Yan College's case
*/


class libldap  {

        var $host;
        var $port;
        var $account_control_type;
        var $admin_dn;                // Should use DN (distinguished name). Otherwise the add / remove user operation will not be successful.
        var $admin_passwd;

        var $isDebug;                // Specify whether it is in debug mode (true). Default is 'false'.
        var $connection;        // Holds the current connection.
        var $base_dn;                // The base DN used in searching.
        var $rdn_attr_name;        // The attribute name of the RDN. Ususally is either 'uid' or 'cn'. (This attribute may be hardcoded)
        var $loginname_prefix;
        var $protocol_set_required;
        var $base_dn_array = array();

		var $domain = ''; // Added to support validate user with domain

        function libldap()
        {
        	global $intranet_root, $PATH_WRT_ROOT;
            global $ldap_host,$ldap_port,$ldap_base_dn,$ldap_account_control_type,
                       $ldap_admin_dn,$ldap_admin_passwd,$ldap_debug,$ldap_rdn_attr_name,
                       $ldap_loginname_prefix, $ldap_protocol_set_required, 
                       $ldap_domain; // added on 2018-01-04
			include_once($intranet_root."/plugins/ldap_conf.php");
        	//echo 1;
                $this->host = $ldap_host;
                $this->port = ($ldap_port==""? 389 :intval($ldap_port));

				if($ldap_domain != ''){
					// new approach
					$this->domain = $ldap_domain;
					$this->protocol_set_required = $ldap_protocol_set_required;
					
				}else
				{
					// old approach
	                # Customized dn settings for different type of users
	                global $special_ldap_dn, $ldap_cname_input;
	                $dns_ini = (($PATH_WRT_ROOT=="./") ? "" : $PATH_WRT_ROOT) . "../intranetdata/custom/ldap_conf.ini";
	                if (file_exists($dns_ini))
	                {
	                	$dns_config = get_file_content($dns_ini);
	                	$base_dn_array = explode("\n", $dns_config);
	                	for ($i=0; $i<sizeof($base_dn_array); $i++)
	                	{
	                		$i_value = trim($base_dn_array[$i]);
	                		if ($i_value!="")
	                		{
	                			$this->base_dn_array[] = $i_value;
	                		} 
	                	}
	                } elseif ($special_ldap_dn != "")
	                {
	                    $this->base_dn = $special_ldap_dn;
	                    $this->base_dn_array[] = $this->base_dn;
	                } else
	                {
	                	# follow the OU chosen by user in login page if exists
	                	$uname = "";
						if ($ldap_cname_input == 'teacher')
						    $uname = 'ou=teacher,';
						else if ($ldap_cname_input == 'guest')
						    $uname = 'ou=guest,ou=teacher,';
						else if ($ldap_cname_input == 'g99')
						    $uname = 'ou=g99,ou=student,';
						else if ($ldap_cname_input == 'g00')
						    $uname = 'ou=g00,ou=student,';
						else if ($ldap_cname_input == 'g01')
						    $uname = 'ou=g01,ou=student,';
	
	                    $this->base_dn = $uname . $ldap_base_dn;
	                    $this->base_dn_array[] = $this->base_dn;
	                }
	
	                $this->rdn_attr_name = $ldap_rdn_attr_name;
	                $this->account_control_type = $ldap_account_control_type;
	                if ($this->account_control_type != 2 && $this->account_control_type != 1)
	                    $this->account_control_type = 3;
	                $this->admin_dn = $ldap_admin_dn;
	                $this->admin_passwd = $ldap_admin_passwd;
	                $this->isDebug = $ldap_debug;
	                $this->loginname_prefix = $ldap_loginname_prefix;
	                $this->protocol_set_required = $ldap_protocol_set_required;
                
				}
        }

        function isAccountControlNeeded()
        {
                 return ($this->account_control_type==1);
        }
        function isPasswordChangeNeeded()
        {
                 return (!$this->account_control_type==3);
        }

        // Attempt to connection to the specified LDAP server.
        // Returns a positive LDAP link identifier on success, or FALSE on error
        function connect()
        {
                # Fill code of Connect LDAP Server to $this->host , $this-port
                # return connection successful or not
                if ($this->isDebug) echo "Connecting to ".$this->host.", port ".$this->port.".<br>";
                $this->connection = @ldap_connect($this->host, $this->port);
                if ($this->protocol_set_required)
                {
                    @ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);
                }
                if ($this->isDebug) echo "Connect result is ".$this->connection.".<br>";

                return $this->connection;
        }

        // Attempt to close the connection to the LDAP server.
        // Returns TRUE on success or FALSE on failure.
        function close()
        {
                # Fill code of close the connection
                if ($this->isDebug) echo "Closing connection at ".$this->host.", port ".$this->port.".<br>";
                $close = @ldap_close($this->connection);
                if ($this->isDebug) echo "Close connection is ".$close.".<br>";

                return $close;
        }

        // This function performs the actual binding (authentication) internally.
        // Return TRUE on success and FALSE on failure.
        function bind($dn = "",$password = "")
        {
                if ($this->isDebug) echo "Bind: DN = $dn, Password = $password. <br>";
                # Fill code of binding account
                # return bind result
                if (false) //(empty($dn) || empty($password))
                {
                        if ($this->isDebug) echo "Anonymous bind. <br>";
                        $isBinded = @ldap_bind($this->connection);
                }
                else
                {
                        if ($this->isDebug) echo "Binding user dn= ".$dn.".<br>";
                        $isBinded = @ldap_bind($this->connection, $dn, $password);
                }

                if ($this->isDebug) echo "Bind result is ".$isBinded."<br>";

                return $isBinded;
        }

        // This function validates the user login against the given password.
        // It first search for the DN of the given $login, and then call bind() to
        // perform the actual binding.

        // If the search cannot find exactly 1 user, (i.e. either no user using this $login
        // or more than 1 user having the same $login as part of their DN -- see below),
        // the function will return false.
        // (e.g. A teacher called "Tom" may have DN -> CN=Tom,ou=Teacher,o=Hong Kong College
        //       A student called "Tom" may also be -> CN=Tom,ou=Student,o=Hong Kong College
        //  whether such case exists is depended on the LDAP schema)
        # Old method
        function validate2($login,$password)
        {
                # Fill code of validation of login , password in param
                # return validation result
                if ($this->isDebug) echo "Valdating $login <br>";
                $search = $this->searchDN($this->loginname_prefix.$login);
                $search_count = @ldap_count_entries($this->connection, $search);
                if ($this->isDebug) echo "Number of entires returned are ".$search_count."<br>";

                // Return false if not exactly 1 people found
                if ($search_count != 1) return false;

                // Perform actual validation
                $entries = @ldap_get_entries($this->connection, $search);
                $dn = $entries[0]['dn'];
                if ($this->isDebug) echo "DN is ".$dn."<br>";
                $bind_result = $this->bind($dn, $password);
                if ($this->isDebug) echo "Validate result is ".$bind_result."<br>";

                return $bind_result;
        }
        
        function validate_try_all_ou($login, $password)
        {
        	if (sizeof($this->base_dn_array)>0)
        	{
        		for ($i=0; $i<sizeof($this->base_dn_array); $i++)
        		{
        			$this->base_dn = $this->base_dn_array[$i];
        			$bind_result = $this->validate($login, $password);
        			if ($bind_result)
        			{
        				//die("found!");
        				return $bind_result;
        			}
        		}
        	}
        	//die();
        	return false;
       	
        }
        
        
        function validate($login,$password)
        {
        		if($this->domain != ''){
					return false;
				}
        	
                # Fill code of validation of login , password in param
                # return validation result

                # Bind
                if ($this->isDebug) echo "Try to bind:<br> ";
                $bind_dn = $this->rdn_attr_name."=".$this->loginname_prefix.$login.",".$this->base_dn;

                if ($this->isDebug)
                {
                    echo "DN: $bind_dn<br>";
                    echo "password: $password<br>";
                }
                $bind_result = $this->bind($bind_dn, $password);
                if ($this->isDebug) echo "Bind result: ".$bind_result."<br>";

                if ($this->isDebug) echo "Search <br>";
                $search = $this->searchDN($this->loginname_prefix.$login);
                $search_count = @ldap_count_entries($this->connection, $search);
                if ($this->isDebug) echo "Number of entires returned are ".$search_count."<br>";

                // Return false if not exactly 1 people found
                if ($search_count != 1) return false;

                // Perform actual validation
                /*
                $entries = @ldap_get_entries($this->connection, $search);
                $dn = $entries[0]['dn'];
                if ($this->isDebug) echo "DN is ".$dn."<br>";
                $bind_result = $this->bind($dn, $password);
                if ($this->isDebug) echo "Validate result is ".$bind_result."<br>";
                */

                return $bind_result;
        }
		
		function validateDomainUser($login, $password)
		{
			if($this->domain == ''){
				return false;
			}
			
			$bind_dn = $login.'@'.$this->domain;
		    if ($this->isDebug)
            {
                echo "DN: $bind_dn<br>";
                echo "password: $password<br>";
            }
            $bind_result = $this->bind($bind_dn, $password);
            if ($this->isDebug) echo "Bind result: ".$bind_result."<br>";
            
            return $bind_result;
		}
		
        // Attempt to create an account on LDAP server.
        // Return TRUE on success and FALSE on failure.
        function openAccount($newLogin,$newPassword)
        {
                if ($this->isDebug) echo "Opening user account $newLogin.<br>";
                # Fill code of create new account
                if ($this->isAccountControlNeeded())
                {
                        // Use admin account to bind the LDAP server
                        $bind_result = $this->bind($this->admin_dn, $this->admin_passwd);
                        if (!$bind_result) {
                                if ($this->isDebug) echo "Unable to use admin account to bind LDAP server.<br>";
                                return false;
                        }

                        $newLogin = $this->loginname_prefix.$newLogin;
                        // Prepare user data (this is schema specifc)
                        // Values other then username and password can be feteched from global
                        // just like admin account, host info...etc.
                        $dn = "$this->rdn_attr_name=".$newLogin.",cn=";
                        $user["$this->rdn_attr_name"] = $newLogin;
                        $user["userPassword"] = $newPassword;
                        $user["description"] = "description";
                        $user["ou"] = "other";
                        $user["objectclass"] = "eAccount";
                        $user["o"] = "broadlearning";

                        // Send the add user request
                        $add_result = @ldap_add ($this->connection, $dn, $user);
                        if ($this->isDebug) echo "Create user result is ".$add_result.".<br>";

                        return $add_result;

                } else {
                        if ($this->isDebug) echo "User not permitted to create user.<br>";
                        return false;
                }
        }

        // Attempt to delete an account on LDAP server.
        // Return TRUE on success and FALSE on failure.
        // Also return FALSE if $login not refers to exactly 1 user. (see validate())
        function removeAccount($login)
        {
                if ($this->isDebug) echo "Removing user account $newLogin.<br>";
                # Fill code of remove account
                if ($this->isAccountControlNeeded())
                {
                        // Use admin account to bind the LDAP server
                        $bind_result = $this->bind($this->admin_dn, $this->admin_passwd);
                        if (!$bind_result) {
                                if ($this->isDebug) echo "Unable to use admin account to bind LDAP server.<br>";
                                return false;
                        }
                        $login = $this->loginname_prefix.$login;

                        // Find out the DN of the user.
                        $search = $this->searchDN($login);
                        $search_count = @ldap_count_entries($this->connection, $search);
                        if ($this->isDebug) echo "Number of entires returned is ".$search_count."<br>";

                        // Return false if not exactly 1 people found
                        if ($search_count != 1) return false;

                        // Find out the DN
                        $entries = @ldap_get_entries($this->connection, $search);
                        $dn = $entries[0]['dn'];

                        // Send the add user request
                        $delete_result = @ldap_delete ($this->connection, $dn);
                        if ($this->isDebug) echo "Remove user result is ".$delete_result.".<br>";

                        return $delete_result;
                } else {
                        if ($this->isDebug) echo "User not permitted to remove user.<br>";
                        return false;
                }
        }

        // Attempt to change the password of the specified user.
        // Return TRUE if password is changed successfully, FALSE otherwise.
        function changePassword($login, $newPassword)
        {
                # Fill code of changing password
                # return result
                // Pass the authentication first.
                if ($this->isDebug) echo "Changing password for user $login.<br>";
                /*
                $validate = validate($login, $oldPassword);
                if (!$validate) {
                        if ($this->isDebug) echo "User authentication failed.<br>";
                        return false;
                }
                */

                // Use admin account to bind the LDAP server
                $bind_result = $this->bind($this->admin_dn, $this->admin_passwd);
                if (!$bind_result) {
                     if ($this->isDebug) echo "Unable to use admin account to bind LDAP server.<br>";
                     return false;
                }

                $login = $this->loginname_prefix.$login;

                // Retrieve the DN of the user
                $search = $this->searchDN($login);
                $entries = @ldap_get_entries($this->connection, $search);
                $dn = $entries[0]['dn'];

                // Attempt to change the password.
                // "userPassword" is the field corresponding to password on the server
                $value["userPassword"] = $newPassword;
                $replace_result = @ldap_mod_replace ($this->connection, $dn, $value);
                echo "Change password result is ".$replace_result.".<br>";

                return $replace_result;
        }

        // Search the DN (distinguish name) from the specified $login (e.g. uid=$login)
        // Returns a search result identifier or FALSE on error.
        function searchDN($login)
        {
                 #$login = $this->loginname_prefix.$login;

                if ($this->isDebug) echo "Searching rdn: ".$this->rdn_attr_name."=".$login.", base = ".$this->base_dn."<br>";
                $search = @ldap_search($this->connection, $this->base_dn, $this->rdn_attr_name."=".$login);
                if ($this->isDebug) echo "Search completion is ".$search."<br>";

                return $search;
        }
        
        // find ldap servers with nslookup command
        function getLdapHosts($searchKey)
		{
			global $ldap_host, $ldap_port; ### defined in /plugins/ldap_conf.php
			$hostAry = array(); // [0 is domain, 1 is port number]
			$raw_host_strs = explode("\n", trim(shell_exec("nslookup -q=srv $searchKey | grep $searchKey")));
			for($i=0;$i<count($raw_host_strs);$i++)
			{
				sscanf($raw_host_strs[$i],'%s service = %d %d %d %s', $site, $dummy1, $dummy2, $port, $host);
				$l = strlen($host);
				if($host[$l-1] == '.'){ // remove the dot at the very end of each line
					$host = substr($host, 0, $l-1);
				}
				$hostAry[] = array($host, $port);
				if ($this->isDebug){
					echo "Found ldap server host ".$host." with port ".$port."<br>";
				}
			}
			if(count($hostAry)==0 && $ldap_host != ''){
				$hostAry[] = array($ldap_host, $ldap_port);
			}
			
			return $hostAry;
		}
		
		// fast switch host, need to call connect and bind afterwards
		function changeToHost($host, $port)
		{
			$this->host = $host;
			$this->port = $port;
		}
		/*
		// strip the @domain part
		function filterUserlogin($login)
		{
			$filtered_login = $login;
			$at_pos = strpos($login,'@');
			if($at_pos > 0)
			{
				$filtered_login = substr($login,0, $at_pos);
			}
			
			return $filtered_login;
		}
		*/
}
?>