<?php
// using kenneth chung
if (!defined("LIBSTUDENTPROFILE_DEFINED"))                     // Preprocessor directive
{
define("LIBSTUDENTPROFILE_DEFINED", true);

class libstudentprofile extends libdb {

      # Display settings
      var $is_frontend_attendance_hidden;
      var $is_frontend_merit_hidden;
      var $is_frontend_service_hidden;
      var $is_frontend_activity_hidden;
      var $is_frontend_award_hidden;
      var $is_printpage_attendance_hidden;
      var $is_printpage_merit_hidden;
      var $is_printpage_service_hidden;
      var $is_printpage_activity_hidden;
      var $is_printpage_award_hidden;

      # Field using settings
      var $is_merit_disabled;
      var $is_min_merit_disabled;
      var $is_maj_merit_disabled;
      var $is_sup_merit_disabled;
      var $is_ult_merit_disabled;
      var $is_black_disabled;
      var $is_min_demer_disabled;
      var $is_maj_demer_disabled;
      var $is_sup_demer_disabled;
      var $is_ult_demer_disabled;
      var $col_merit;
      var $col_demerit;


      # Admin control (suspended)
      var $is_class_allowed;
      var $class_adminlevel;
      var $is_class_attend_allowed;
      var $is_class_merit_allowed;
      var $is_class_service_allowed;
      var $is_class_activity_allowed;
      var $is_class_award_allowed;
      var $is_class_assessment_allowed;
      var $is_class_file_allowed;

      var $is_subj_allowed;
      var $subj_adminlevel;
      var $is_subj_attend_allowed;
      var $is_subj_merit_allowed;
      var $is_subj_service_allowed;
      var $is_subj_activity_allowed;
      var $is_subj_award_allowed;
      var $is_subj_assessment_allowed;
      var $is_subj_file_allowed;

      var $attendance_count_method;


        function libstudentprofile ($UserID="")
        {
                $this->libdb();
                $this->retrieveSettings();
                #$this->retrieveACLSettings();
        }
        function retrieveSettings()
        {
                 global $intranet_root;
                 $setting_file = "$intranet_root/file/std_profile_settings.txt";
                 $content = get_file_content($setting_file);
                 $array = explode("\n",$content);
                 # Display settings
                 $this->is_frontend_attendance_hidden = ($array[0][0]==1);
                 $this->is_printpage_attendance_hidden = ($array[0][1]==1);
                 $this->is_frontend_merit_hidden = ($array[1][0]==1);
                 $this->is_printpage_merit_hidden = ($array[1][1]==1);
                 $this->is_frontend_service_hidden = ($array[2][0]==1);
                 $this->is_printpage_service_hidden = ($array[2][1]==1);
                 $this->is_frontend_activity_hidden = ($array[3][0]==1);
                 $this->is_printpage_activity_hidden = ($array[3][1]==1);
                 $this->is_frontend_award_hidden = ($array[4][0]==1);
                 $this->is_printpage_award_hidden = ($array[4][1]==1);

                 # Field using settings
                 $field_setting_file = "$intranet_root/file/std_profile_field.txt";
                 $content = get_file_content($field_setting_file);
                 $array = explode("\n",$content);
                 $this->is_merit_disabled = ($array[0][0]==1);
                 $this->is_min_merit_disabled = ($array[0][1]==1);
                 $this->is_maj_merit_disabled = ($array[0][2]==1);
                 $this->is_sup_merit_disabled = ($array[0][3]==1);
                 $this->is_ult_merit_disabled = ($array[0][4]==1);
                 $this->is_black_disabled = ($array[1][0]==1);
                 $this->is_min_demer_disabled = ($array[1][1]==1);
                 $this->is_maj_demer_disabled = ($array[1][2]==1);
                 $this->is_sup_demer_disabled = ($array[1][3]==1);
                 $this->is_ult_demer_disabled = ($array[1][4]==1);
                 $this->col_merit = 0;
                 $this->col_demerit = 0;
                 if (!$this->is_merit_disabled) $this->col_merit++;
                 if (!$this->is_min_merit_disabled) $this->col_merit++;
                 if (!$this->is_maj_merit_disabled) $this->col_merit++;
                 if (!$this->is_sup_merit_disabled) $this->col_merit++;
                 if (!$this->is_ult_merit_disabled) $this->col_merit++;
                 if (!$this->is_black_disabled) $this->col_demerit++;
                 if (!$this->is_min_demer_disabled) $this->col_demerit++;
                 if (!$this->is_maj_demer_disabled) $this->col_demerit++;
                 if (!$this->is_sup_demer_disabled) $this->col_demerit++;
                 if (!$this->is_ult_demer_disabled) $this->col_demerit++;

                 # Count Settings
                 include_once('libgeneralsettings.php');
                 $GeneralSetting = new libgeneralsettings();
                 $SettingList[] = "'ProfileAttendCount'";
                 $Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
                 $this->attendance_count_method = ($Settings['ProfileAttendCount'] > 0? $Settings['ProfileAttendCount'] : 0);
        }
        function retrieveACLSettings()
        {
                 global $intranet_root;
                 $setting_file = "$intranet_root/file/std_profile_acl.txt";
                 $content = get_file_content($setting_file);
                 $array = explode("\n",$content);
                 $this->class_adminlevel = $array[0][0];
                 $this->is_class_attend_allowed = ($array[0][1]==1);
                 $this->is_class_merit_allowed = ($array[0][2]==1);
                 $this->is_class_service_allowed = ($array[0][3]==1);
                 $this->is_class_activity_allowed = ($array[0][4]==1);
                 $this->is_class_award_allowed = ($array[0][5]==1);
                 $this->is_class_assessment_allowed = ($array[0][6]==1);
                 $this->is_class_file_allowed = ($array[0][7]==1);
                 $this->is_class_allowed = (
                        $this->is_class_attend_allowed ||
                        $this->is_class_merit_allowed ||
                        $this->is_class_service_allowed ||
                        $this->is_class_activity_allowed ||
                        $this->is_class_award_allowed ||
                        $this->is_class_assessment_allowed ||
                        $this->is_class_file_allowed
                        );
                 $this->subj_adminlevel = $array[1][0];
                 $this->is_subj_attend_allowed = ($array[1][1]==1);
                 $this->is_subj_merit_allowed = ($array[1][2]==1);
                 $this->is_subj_service_allowed = ($array[1][3]==1);
                 $this->is_subj_activity_allowed = ($array[1][4]==1);
                 $this->is_subj_award_allowed = ($array[1][5]==1);
                 $this->is_subj_assessment_allowed = ($array[1][6]==1);
                 $this->is_subj_file_allowed = ($array[1][7]==1);
                 $this->is_subj_allowed = (
                        $this->is_subj_attend_allowed ||
                        $this->is_subj_merit_allowed ||
                        $this->is_subj_service_allowed ||
                        $this->is_subj_activity_allowed ||
                        $this->is_subj_award_allowed ||
                        $this->is_subj_assessment_allowed ||
                        $this->is_subj_file_allowed
                        );
        }

        function returnClassTeacherCID($uid)
        {
            $sql = "SELECT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID = '".IntegerSafe($uid)."'";
                 $temp = $this->returnVector($sql);
                 return ($temp[0]);
        }
        function returnSubjectTeacherCID($uid)
        {
            $sql = "SELECT DISTINCT ClassID FROM INTRANET_SUBJECT_TEACHER WHERE UserID = '".IntegerSafe($uid)."'";
                 return $this->returnVector($sql);
        }
        function isAllowedManage($uid)
        {
                 if ($this->is_class_allowed)
                 {
                     # Check Class teacher or not
                     if ($this->returnClassTeacherCID($uid))
                     {
                         return true;
                     }
                 }
                 if ($this->is_subj_allowed)
                 {
                     $temp = $this->returnSubjectTeacherCID($uid);
                     return (sizeof($temp)!=0);
                 }
                 return false;
        }
        function returnProfileRightsForClassTeacher($uid)
        {
                 if ($this->is_class_allowed)
                 {
                     # Check Class teacher or not
                     if ($this->returnClassTeacherCID($uid))
                     {
                         return array($this->is_class_attend_allowed ,
                        $this->is_class_merit_allowed ,
                        $this->is_class_service_allowed ,
                        $this->is_class_activity_allowed ,
                        $this->is_class_award_allowed ,
                        $this->is_class_assessment_allowed ,
                        $this->is_class_file_allowed);
                     }
                 }
                 return;
        }
        function returnProfileRightsForSubjectTeacher($uid)
        {
                 if ($this->is_subj_allowed)
                 {
                     $temp = $this->returnSubjectTeacherCID($uid);
                     if (sizeof($temp)!=0)
                     {
                         return array(
                         $this->is_subj_attend_allowed ,
                        $this->is_subj_merit_allowed ,
                        $this->is_subj_service_allowed ,
                        $this->is_subj_activity_allowed ,
                        $this->is_subj_award_allowed ,
                        $this->is_subj_assessment_allowed ,
                        $this->is_subj_file_allowed
                         );
                     }
                 }
                 return;
        }
        # Return:
        # 0 - No right to edit
        # 1 - Normal right
        # 2 - Full right
        function returnAdminLevelForSetAdmin($uid, $ClassID="")
        {
                 if ($ClassID != "")
                 {
                     $sql = "SELECT b.AdminID, b.AdminLevel FROM INTRANET_PROFILE_ADMIN_CLASS as a
                                    LEFT OUTER JOIN INTRANET_ADMIN_USER as b
                                         ON a.UserID = b.UserID AND b.RecordType = 2
                                    WHERE a.UserID = $uid AND a.ClassID = $ClassID";
                 }
                 else
                 {
                     $sql = "SELECT AdminID, AdminLevel FROM INTRANET_ADMIN_USER WHERE RecordType = 2 AND UserID = '".IntegerSafe($uid)."'";
                 }
                 $temp = $this->returnArray($sql,2);
                 if ($temp[0][0]=="") return 0;
                 else return $temp[0][1]+1;
        }
        # Return:
        # 0 - No right to edit
        # 1 - Normal right
        # 2 - Full right
        function returnProfileAdminLevel($uid, $ClassID)
        {
                 $result_lvl = 0;
                 # Class Teacher
                 if ($this->is_class_allowed)
                 {
                     if ($this->returnClassTeacherCID($uid)==$ClassID)
                     {
                         if ($this->class_adminlevel == 1)   # Full right
                         {
                             return 2;
                         }
                         else
                         {
                             $result_lvl = 1;
                         }
                     }
                 }
                 # Subject Teacher
                 if ($this->is_subj_allowed)
                 {
                     $teaching = $this->returnSubjectTeacherCID($uid);
                     if (in_array($ClassID, $teaching))
                     {
                         if ($this->subj_adminlevel == 1)   # Full right
                         {
                             return 2;
                         }
                         else
                         {
                             $result_lvl = 1;
                         }
                     }
                 }
                 # Set Admin
                 $setAdmin = $this->returnAdminLevelForSetAdmin($uid, $ClassID);
                 if ($setAdmin == 0)
                 {
                     return $result_lvl;
                 }
                 else if ($setAdmin == 2)
                 {
                      return 2;
                 }
                 else
                 {
                     return 1;
                 }
        }
        function returnClassIDForStudent($studentid)
        {
                 $sql = "SELECT b.ClassID FROM INTRANET_USER as a
                                LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName
                                WHERE UserID = $studentid";
                 $temp = $this->returnVector($sql);
                 return $temp[0];
        }
        function returnProfileAdminLevelForStudent($uid, $studentid)
        {
                 $classid = $this->returnClassIDForStudent($studentid);
                 return $this->returnProfileAdminLevel($uid, $classid);
        }

        # Return Fields to be used in merit
       function getSelectMeritType ($tags,$selected="")
       {
                global $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit,
                       $i_Merit_SuperCredit, $i_Merit_UltraCredit, $i_Merit_BlackMark,
                       $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit,
                       $i_Merit_UltraDemerit;
                $x = "<SELECT $tags>\n";
                if (!$this->is_merit_disabled)
                {
                     $x .= "<OPTION value=1 ".($selected==1?"SELECTED":"").">$i_Merit_Merit</OPTION>\n";
                }
                if (!$this->is_min_merit_disabled)
                {
                     $x .= "<OPTION value=2 ".($selected==2?"SELECTED":"").">$i_Merit_MinorCredit</OPTION>\n";
                }
                if (!$this->is_maj_merit_disabled)
                {
                     $x .= "<OPTION value=3 ".($selected==3?"SELECTED":"").">$i_Merit_MajorCredit</OPTION>\n";
                }
                if (!$this->is_sup_merit_disabled)
                {
                     $x .= "<OPTION value=4 ".($selected==4?"SELECTED":"").">$i_Merit_SuperCredit</OPTION>\n";
                }
                if (!$this->is_ult_merit_disabled)
                {
                     $x .= "<OPTION value=5 ".($selected==5?"SELECTED":"").">$i_Merit_UltraCredit</OPTION>\n";
                }
                /*
                if (!$this->is_warning_disabled)
                {
                     $x .= "<OPTION value=0 ".($selected==-1?"SELECTED":"").">$i_Merit_Warning</OPTION>\n";
                }
                */
                if (!$this->is_black_disabled)
                {
                     $x .= "<OPTION value=-1 ".($selected==-1?"SELECTED":"").">$i_Merit_BlackMark</OPTION>\n";
                }
                if (!$this->is_min_demer_disabled)
                {
                     $x .= "<OPTION value=-2 ".($selected==-2?"SELECTED":"").">$i_Merit_MinorDemerit</OPTION>\n";
                }
                if (!$this->is_maj_demer_disabled)
                {
                     $x .= "<OPTION value=-3 ".($selected==-3?"SELECTED":"").">$i_Merit_MajorDemerit</OPTION>\n";
                }
                if (!$this->is_sup_demer_disabled)
                {
                     $x .= "<OPTION value=-4 ".($selected==-4?"SELECTED":"").">$i_Merit_SuperDemerit</OPTION>\n";
                }
                if (!$this->is_ult_demer_disabled)
                {
                     $x .= "<OPTION value=-5 ".($selected==-5?"SELECTED":"").">$i_Merit_UltraDemerit</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }

       function GET_MODULE_OBJ_ARR()
        {
                global $plugin, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $hasRight;
                
                ### wordings
                global $i_Profile_Student_Profile, $OverallRecord, $i_Profile_Attendance, $i_Profile_Merit, $i_Profile_Activity, $i_Profile_Service, $i_Profile_Award;
                
                # Current Page Information init
                $PageOverall 		= 0;
                $PageAttendanceRecord 	= 0;
                $PageMeritDemerit 	= 0;
                $PageActivity		= 0;
                $PageService		= 0;
                $PageAward		= 0;
                
                switch ($CurrentPage) {
                	case "PageOverall":
                                $PageOverall = 1;
                                break;
                                
			case "PageAttendanceRecord":
                                $PageOverall = 1;
                                $PageAttendanceRecord = 1;
                                break;
                                
			case "PageMeritDemerit":
                                $PageOverall = 1;
                                $PageMeritDemerit = 1;
                                break;
                                
			case "PageActivity":
                                $PageOverall = 1;
                                $PageActivity = 1;
                                break;
                                
			case "PageService":
                                $PageOverall = 1;
                                $PageService = 1;
                                break;
			
                        case "PageAward":
                                $PageOverall = 1;
                                $PageAward = 1;
                                break;
                }

                # Menu information
                $MenuArr["Overall"] 					= array($OverallRecord, $PATH_WRT_ROOT."home/studentprofile/index.php", $PageOverall, "$image_path/2007a/student_profile/icon_overall.gif");
                if($hasRight) {
                        $MenuArr["Overall"]["Child"]["AttendanceRecord"] 	= array($i_Profile_Attendance, $PATH_WRT_ROOT."home/studentprofile/attendance_record.php", $PageAttendanceRecord, "$image_path/2007a/smartcard/icon_attendance.gif");
                        $MenuArr["Overall"]["Child"]["MeritDemerit"] 		= array($i_Profile_Merit, $PATH_WRT_ROOT."home/studentprofile/merit_demerit.php", $PageMeritDemerit, "$image_path/2007a/student_profile/icon_merit.gif");
                        $MenuArr["Overall"]["Child"]["Activity"] 		= array($i_Profile_Activity, $PATH_WRT_ROOT."home/studentprofile/activity.php", $PageActivity, "$image_path/2007a/student_profile/icon_activity.gif");
                        $MenuArr["Overall"]["Child"]["Service"] 		= array($i_Profile_Service, $PATH_WRT_ROOT."home/studentprofile/service.php", $PageService, "$image_path/2007a/student_profile/icon_service.gif");
                        $MenuArr["Overall"]["Child"]["Award"] 			= array($i_Profile_Award, $PATH_WRT_ROOT."home/studentprofile/award.php", $PageAward, "$image_path/2007a/student_profile/icon_award.gif");
		}
                
                # module information
                $MODULE_OBJ['title'] = $i_Profile_Student_Profile;
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_studentprofile.gif";
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/studentprofile/index.php";
                $MODULE_OBJ['menu'] = $MenuArr;
				
                return $MODULE_OBJ;
        }
        
        


}


}        // End of directive
?>