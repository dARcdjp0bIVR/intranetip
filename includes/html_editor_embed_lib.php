<?php

#Modifing by 

# Add the HTML JS Editor Class
# Modified by key (2008-07-24) / copy based on html_editor_embed2.php

class html_editor_embed_lib
{
	var $is_included_before = false;
	var $editor_width = "100%";
	var $editor_height = "320px";
	var $equation_editor_js = "";
	var $insert_image_js = "";
	var $lang_used;
	var $obj_name;
	var $init_html_content = "";
	var $hidden_text = "";
	var $isTextMode;
	var $html_opt_name;
	
	function html_editor_embed_lib($obj_name="", $is_included_before="", $editor_width="", $editor_height="", $lang_used="", $init_html_content="", $hidden_text="", $equation_editor_js="", $insert_image_js="", $isTextMode="", $html_opt_name="")
	{
		$this->obj_name = $obj_name;
		$this->is_included_before = $is_included_before;
		$this->editor_width = $editor_width;
		$this->editor_height = $editor_height;
		$this->lang_used = $lang_used;
		$this->init_html_content = $init_html_content;
		$this->hidden_text = $hidden_text;
		$this->equation_editor_js = $equation_editor_js;
		$this->insert_image_js = $insert_image_js;
		$this->isTextMode = $isTextMode;
		$this->html_opt_name = $html_opt_name;
		
		$this->INIT_HTML_EDITOR();
	}
	
	function GET_HTML_EDITOR($obj_name="", $init_html_content="", $editor_width="", $editor_height="")
	{
		global $is_included_before;
		
		if($obj_name != "")
		$this->obj_name = $obj_name;
		
		if($init_html_content != "")
		$this->init_html_content = $init_html_content;
		
		if($editor_width != "")
		$this->editor_width = $editor_width;
		
		if($editor_height != "")
		$this->editor_height = $editor_height;
		
		$x = "";
		
		if(!$this->is_included_before)
		{
			$x .= $this->GET_STYLE();
			$x .= $this->GET_BROWSER_JAVASCRIPT();
			$x .= $this->GET_HTML_EDITOR_LAYER();
		}

		$x .= $this->GET_EDITOR_TABLE();
		$x .= $this->GET_HIDDEN_LAYER();
		$x .= $this->GET_JAVASCRIPT();
		$x .= $this->GET_HIDDEN_OPTION();
		
		$this->is_included_before = true;
		$is_included_before = true;
		
		return $x;
		//echo $x;
	}
	
	function genButton($btn_name, $btn_action)
	{
		$rx = "<button class='btnA' onClick=\"$btn_action\" onmouseover=\"if(this.className=='btnA'){this.className='btnAOver'}\" onmouseout=\"if(this.className=='btnAOver'){this.className='btnA'}\" unselectable='on'>&nbsp; $btn_name &nbsp;</button>";
		return $rx;
	}
	
	function INIT_HTML_EDITOR()
	{
		global $intranet_session_language;
		
		if (!$this->is_included_before)
		{

        	if ($this->editor_width=="")
        	{
                $this->editor_width = "100%";
        	}
       		else
        	{
            	if (substr($this->editor_width,-1,1)!='%')
            	{
                	$this->editor_width = ($this->equation_editor_js!="") ? ((int) $this->editor_width) + 10 : ((int) $this->editor_width) - 10;
                	$this->editor_width .= "px";
            	}
        	}

        	if ($this->editor_height=="")
        	{
            	$this->editor_height = "320px";
        	}
       	 	else
        	{
        		if (substr($this->editor_width,-2,2)!='px')
            	{
              		 $this->editor_height .= "px";
            	}
        	}
        
            switch ($intranet_session_language)
        	{
                case "en": $this->lang_used = "eng"; break;
                case "b5": $this->lang_used = "chib5"; break;
                case "gb": $this->lang_used = "chigb"; break;
        	}
    	} // end if include before
	} // end function init value
	
	function GET_STYLE()
	{
		$x = "";
		
		$x .= "<style type=\"text/css\"><!--
  		.subhead { font-family: arial, arial; font-size: 18px; font-weight: bold; font-style: italic; }
  		.backtotop { font-family: arial, arial; font-size: xx-small;  }
  		.code { background-color: #EEEEEE; font-family: Courier New; font-size: x-small;
            	margin: 5px 0px 5px 0px; padding: 5px;
            	border: black 1px dotted;
              }
            
  		.btnA { height: 21px; border: 1px solid #FFFFFF; background-color: #CEECFF; margin: 0; padding: 0; }
  		.btnAOver { height: 21px; border: 1px outset #FFFFFF; background-color: #F6FAFF;}
  		.btnADown { height: 21px; border: 1px inset #FFFFFF; background-color: #89C7FF; }

		#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:2; visibility:hidden;}
		--></style>";
		
		return $x;
	}
	
	function GET_HTML_EDITOR_LAYER()
	{
		$x = "";
		$x .= "<div id=\"ToolMenu\"></div>";
		return $x;
	}
	
	function GET_BROWSER_JAVASCRIPT()
	{
		global $admin_url_path, $eclass_url_root, $ecHTTP, $categoryID;
		
		$x = "";
		
		$x .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"$eclass_url_root/src/includes/js/tooltip.js\"></script> \n";
		$x .= "<script language=\"Javascript\"><!-- // load htmlarea \n";
		$x .= "var ecHTTP = \"$ecHTTP\"; \n";
		$x .= "_editor_url = \"$eclass_url_root/\"; \n";
		$x .= "var win_ie_ver = parseFloat(navigator.appVersion.split(\"MSIE\")[1]); \n";
		$x .= "if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; } \n";
		$x .= "if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; } \n";
		$x .= "if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; } \n";

		$x .= "if (win_ie_ver >= 5.5) { \n";
  		$x .= "document.write('<scr' + 'ipt src=\"' +_editor_url+ 'src/includes/js/html_editor2.php?lang_used=$this->lang_used\"'); \n";
  		$x .= "document.write(' language=\"Javascript1.2\"></scr' + 'ipt>'); \n";
		$x .= "} else { document.write('<br><br><br><br><br>Please upgrade to <b>IE 6.0 or above</b> in order to use the HTML editor!'); } \n";
		
		$x .= "var diagObj = null; \n";
		$x .= "function browseFile(myAtt, dObj){ \n";
        $x .= " diagObj = dObj; \n";
        $x .= " var url = \"$admin_url_path/src/course/resources/files/index.php?category=$categoryID&attach=1&isImageOnly=1&attachment=\" + myAtt; \n";
        $x .= " newWindow(url,1); \n";
		$x .= "} \n";

		$x .= "function getFilePath(rPath){ \n";
        $x .= "diagObj.value = rPath; \n";
        $x .= "return; \n";
		$x .= "} \n";

		$x .= "function browseFileLink(myAtt, dObj){ \n";
        $x .= "diagObj = dObj; \n";
        $x .= " var url = \"$admin_url_path/src/course/resources/files/index.php?category=$categoryID&attach=1&fieldname=html_editor_diagObj&attachment=\" + myAtt; \n";
        $x .= "newWindow(url,1); \n";
		$x .= "} \n";
		$x .= "// --></script> \n";
		
		return $x;
	}
	
	function GET_EDITOR_TABLE()
	{
		$x = "";
		$x .= "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">";
		$x .= "<tr>";
		$x .= "<td bgcolor=\"white\" style=\"border-left:solid 1px #a5acb2; border-top:solid 1px #a5acb2; border-right:solid 1px #a5acb2; border-bottom:solid 1px #a5acb2; \">";
		$x .= "<textarea name=\"$this->obj_name\" id=\"$this->obj_name\" cols=\"70\">";
		$x .= $this->init_html_content;
		$x .= "</textarea>";
		$x .= "</td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function GET_HIDDEN_LAYER()
	{
		$x = "";
		$x .= "<div style=\"position:absolute; visibility:hidden\">";
		$x .= "<textarea name=\"hidden_text\">".$this->hidden_text."</textarea>";
		$x .= "</div>";
		
		return $x;
	}
	
	function GET_JAVASCRIPT()
	{
		$x = "";
		
		$x .= "<script language=\"javascript1.2\"> \n";
		$x .= "isMenu = true; \n";
		$x .= "toolLeftAdd = -8; \n";
		$x .= "toolTopAdd = 12; \n";
		$x .= "var config = new Object(); \n";

		$x .= "config.width = '".$this->editor_width."'; \n";
		$x .= "config.height = '".$this->editor_height."'; \n";
		$x .= "config.bodyStyle = 'background-color: white; font-family: \"Verdana\"; font-size: x-small;'; \n";
	
		// NOTE:  You can remove any of these blocks and use the default config!

		$x .= "config.toolbar = [ \n";
    	$x .= "['fontname','fontsize','separator','HorizontalRule','Createlink' $insert_image_js, 'InsertTable' $equation_editor_js], \n";
    	$x .= "['linebreak'], \n";
    	$x .= "['undo','redo','separator','bold','italic','underline','strikethrough','subscript','superscript','separator','forecolor','backcolor','separator','justifyleft','justifycenter','justifyright','justifyfull','separator','OrderedList','UnOrderedList','Outdent','Indent'] \n";
		$x .= "]; \n";

		$x .= "var isInitFocus = false; \n";
		$x .= "var isTextMode = '".$this->isTextMode."'; \n";
		$x .= "menuPos = [null, null]; \n";
		$x .= "editor_generate('".$this->obj_name."', config); \n";
	
		$x .= "</script> \n";
		
		return $x;
	}
	
	function GET_HIDDEN_OPTION()
	{
		$x = "";
		
		if ($this->html_opt_name != "")
		$x .= "<input type='hidden' name='$html_opt_name' value='1' />";
		
		return $x;
	}
	
} // end class
?>
