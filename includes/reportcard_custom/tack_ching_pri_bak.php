<?php
# Editing by Andy

####################################################
# Library for Tack Ching Primary School
####################################################

include_once($intranet_root."/lang/reportcard_custom/tack_ching_pri.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","merit","eca","interschool","schoolservice","attendance");
	}
	
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$photoArea = "<div style='border:1px solid black;height:130px;width:100px'></div>";
		
		$x = "<tr><td width='80%'>$TitleTable</td><td width='' align='right' rowspan='3'>$photoArea</td></tr>";
		$x .= "<tr><td>&nbsp;</td></tr>";
		$x .= "<tr><td>$StudentInfoTable</td></tr>";
		$x .= "<tr><td colspan='2'>&nbsp;</td></tr>";
		$x .= "<tr><td colspan='2'>";
		$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
		$x .= "<tr><td class='bold_label' width='67%'>&nbsp;&nbsp;".$eReportCard['Template']['AcademicResults']."</td>";
		$x .= "<td width='3%'>&nbsp;</td>";
		$x .= "<td class='bold_label'>&nbsp;&nbsp;".$eReportCard['Template']['ConductAssessment']."</td></tr>";
		$x .= "<tr><td valign='top' class='bold_label' width='67%'>".$MSTable.$SignatureTable."</td>";
		$x .= "<td width='3%'>&nbsp;</td>";
		$x .= "<td valign='top' class='bold_label'>$MiscTable</td></tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "$FooterRow";
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID) {
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			if (function_exists("mb_strtoupper")) {
				$ReportTitle = mb_strtoupper($ReportTitle);
			} else {
				$ReportTitle = strtoupper($ReportTitle);
			}
			
			if(!empty($ReportTitle)) {
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td width='25%'>&nbsp;</td><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
			}
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal		= "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['NameEn'] 	= $lu->EnglishName;
				$data['NameCh'] 	= $lu->ChineseName;
				$data['ClassNo'] 	= $lu->ClassNumber; 
				$data['Class'] 		= $lu->ClassName;
				$data['DOB'] = $lu->DateOfBirth;
				$data['DOI'] = $ReportSetting["Issued"];
				$data['Gender'] 	= $lu->Gender;
				$data['STRN']       = str_replace("#", "", $lu->WebSamsRegNo);
								
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			// Hardcoded student info, won't affect by setting
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$StudentInfoTable .= "<tr><td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['NameEn']." : ".$data['NameEn']."</td>";
			$StudentInfoTable .= "<td width='35%' class='student_info_text'>&nbsp;</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['NameCh']." : ".$data['NameCh']."</td>";
			$StudentInfoTable .= "<td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['Class']." : ".$data['Class']."</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['ClassNo']." : ".$data['ClassNo']."</td>";
			$StudentInfoTable .= "<td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['Gender']." : ".$data['Gender']."</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['STRN']." : ".$data['STRN']."</td>";
			$StudentInfoTable .= "<td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['DOB']." : ".$data['DOB']."</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text'>&nbsp;</td>";
			$StudentInfoTable .= "<td class='student_info_text'>".$eReportCard['Template']['StudentInfo']['DOI']." : ".$data['DOI']."</td></tr>";
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2) = $ColHeaderAry;
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MarksDisplayAry = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='3' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			$Droped = 1;
			if (sizeof($MarksAry[$thisSubjectID]) > 0) {
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			$DetailsTable .= "<tr>";
			# Subject 
			$DetailsTable .= $SubjectCol[$i];;
			# Marks
			$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
			$DetailsTable .= "</tr>";
			$isFirst = 0;
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass = $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm = $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal = $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg = $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$FormSubjectGradingScheme = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Check if all grading schemes of all subjects are display as Grade
		// If yes, this report is assume to be a P5-P6 report, need different handling than P1-P4 report
		$isSchemeAllGrade = true;
		foreach($FormSubjectGradingScheme as $tmpSubjectID => $tmpSchemeInfo) {
			if ($tmpSchemeInfo["scaleDisplay"] != "G") {
				$isSchemeAllGrade = false;
				break;
			}
		}
		#$isSchemeAllGrade = true;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		
		############## Marks START ##############
		$row2 = "";
		if($ReportType=="T")	# Temrs Report Type
		{
			$e++;
			# Never show detail assessment result for Tack Ching
		} else {				# Whole Year Report Type
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1) {
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++) {
						$row2 .= "<td class='border_left reportcard_text' align='center' height='{$LineHeight}' width=''>". $ColumnTitle[$j] . "</td>";
						$n++;
						$e++;
					}
				} else {
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='".(70/(1+sizeof($ColumnData)))."%'>". $SemName ."</td>";
			}
		}
		############## Marks END ##############
		$x .= "<tr>";
		
		# Subject 
		$PercentageSign = "";
		if ($isSchemeAllGrade)
			$PercentageSign = "<div id='percentage_sign'>%</div>";
		$SubjectTitle = "<div class='col_header'>$PercentageSign<p>".$eReportCard['Template']['SubjectEng']."</p></div>";
		$x .= "<td align='center' valign='middle' class='small_title' width='30%'>". $SubjectTitle . "</td>";
		$n++;
		
		# Full Mark
		if ($ShowSubjectFullMark) {
			$FullMarkTitle = "<div class='col_header'><p>".$eReportCard['SchemesFullMark']."</p></div>";
			$x .= "<td align='center' valign='middle' class='border_left small_title' height='{$LineHeight}' width='".(70/(1+$e))."%'>". $FullMarkTitle . "</td>";
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn) {
			if($ReportType=="T")
				$SubjectOverallTitle = "<div class='col_header'><p>".$this->returnSemesters($SemID)."</p></div>";
			else
				$SubjectOverallTitle = "<div class='col_header'><p>".$eReportCard['Template']['SubjectOverall']."</p></div>";
			$x .= "<td align='center' valign='middle' width='".(70/(1+$e))."%' class='border_left small_title' align='center'>". $SubjectOverallTitle ."</td>";
			$n++;
		}
		
		$x .= "</tr>";
		
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		
		// Check if all grading schemes of all subjects are display as Grade
		// If yes, this report is assume to be a P5-P6 report, need different handling than P1-P4 report
		$FormSubjectGradingScheme = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		$isSchemeAllGrade = true;
		foreach($FormSubjectGradingScheme as $tmpSubjectID => $tmpSchemeInfo) {
			if ($tmpSchemeInfo["scaleDisplay"] != "G") {
				$isSchemeAllGrade = false;
				break;
			}
		}
		#$isSchemeAllGrade = true;
		
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		
		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnID = array();
		$ColumnTitle = array();
		if(sizeof($ColoumnTitle) > 0)
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
		
 		$x = array(); 
		$isFirst = 1;
		
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
					$isSub = true;
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
						$Prefix = "";
						$isSub = false;
			 		}
			 		
			 		$css_border_top = ($Prefix)? "" : "border_top";
			 		
					$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
					
					$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
					
					$t .= "<span style='width:88%;'>";
					$t .= $Prefix.$SubjectEng;
					$t .= "</span>";
					
					if($isSub && $isSchemeAllGrade) {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[0] ." and SubjectID=$SubSubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$tmpSubjectFullMark = $SubjectFullMarkAry[$SubSubjectID];
						if ($UseWeightedMark) {
							$FullMark = $tmpSubjectFullMark*$thisSubjectWeight;
						} else {
							$FullMark = $tmpSubjectFullMark;
						}
						
						$t .= "<span style='vertical-align:top;width:12%;'>";
						$t .= $FullMark;
						$t .= "</span>";
					}
					$t .= "</td>";
						
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table id='signature_table' width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		$SignatureTable .= "<td valign='top' width='30%' rowspan='2'>";
		$SignatureTable .= "<table width='100%' cellspacing='0' cellpadding='2' border='0'>";
		$SignatureTable .= "<tr><td class='tabletext'>Promoted to /</td></tr>";
		$SignatureTable .= "<tr><td class='tabletext'>Retained in /</td></tr>";
		$SignatureTable .= "<tr><td class='tabletext'>Pass to</td></tr>";
		$SignatureTable .= "<tr><td class='tabletext' align='center'>Primary _____</td></tr>";
		$SignatureTable .= "</table>";
		$SignatureTable .= "</td>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$SignatureTable .= "<td width='".(70/3)."%' class='small_title border_left' align='center'>";
			$SignatureTable .= $Title;
			$SignatureTable .= "</td>";
		}
		$SignatureTable .= "</tr>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SignatureTable .= "<td class='small_title border_left border_top' align='center'>";
			$SignatureTable .= "<div style='font-size:45px;'>&nbsp;</div>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
		
		$FormNum = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
		$ClassNum = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
				$columnClassNum[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
				$columnFormNum[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
			}
		}
		
		
		// Find out the Full mark of grand total by adding all subject's full mark (weight)
		$GrandTotalFullMark = 0;
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 0);
		$tmpOtherCond = "SubjectID IS NOT NULL AND SubjectID != '0' AND ReportColumnID IS NOT NULL AND ReportColumnID != '0'";
		$SubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, $tmpOtherCond);
		$SubjectWeightMap = array();
		
		for($i=0; $i<sizeof($SubjectWeightData); $i++) {
			$SubjectWeightMap[$SubjectWeightData[$i]["SubjectID"]] = $SubjectWeightData[$i]["Weight"];
		}
		foreach($SubjectFullMarkAry as $tmpSubjectID => $tmpFullMark) {
			$GrandTotalFullMark += $tmpFullMark * $SubjectWeightMap[$tmpSubjectID];
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['GrandTotal'];
			$x .= "</td>";
			
			$x .= "<td align='center' class='tabletext border_left {$border_top}' height='{$LineHeight}'>";
			$x .= $GrandTotalFullMark;
			$x .= "</td>";
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnTotal[$ColumnID]."</td>";
					}
				}
			}
			
			if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $GrandTotal ."</td>";
			$x .= "</tr>";
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['AverageMark'];
			$x .= "</td>";
			$x .= "<td align='center' class='tabletext border_left {$border_top}' height='{$LineHeight}'>";
			$x .= 100;
			$x .= "</td>";
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnAverage[$ColumnID]."</td>";
					}
				}
			}
			
			if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $AverageMark ."</td>";
			$x .= "</tr>";
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td colspan='2' class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['Position'];
			$x .= "</td>";
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnClassPos[$ColumnID]."</td>";
					}
				}
			}
			
			if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $ClassPosition ."</td>";
			$x .= "</tr>";
		}
		
		# NO Position in Form 
		
		# No. of students in Class
		$border_top = "border_top";
		$first = 0;
		$x .= "<tr>";
		$x .= "<td colspan='2' class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
		$x .= $eReportCard['Template']['NoOfStudent'];
		$x .= "</td>";
		
		if ($ReportType == "W") {
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>--</td>";
			} else {
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnClassNum[$ColumnID]."</td>";
				}
			}
		}
		
		if ($ShowRightestColumn)
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $ClassNum ."</td>";
		$x .= "</tr>";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='')
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		// Check if all grading schemes of all subjects are display as Grade
		// If yes, this report is assume to be a P5-P6 report, need different handling than P1-P4 report
		$FormSubjectGradingScheme = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		$isSchemeAllGrade = true;
		foreach($FormSubjectGradingScheme as $tmpSubjectID => $tmpSchemeInfo) {
			if ($tmpSchemeInfo["scaleDisplay"] != "G") {
				$isSchemeAllGrade = false;
				break;
			}
		}
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Terms Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName) {
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				# define css
				$css_border_top = ($isSub == 1) ? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
				
				if ($isSchemeAllGrade) {
					$cellAlign = "center";
				} else {
					$cellAlign = ($isSub == 1) ? "left" : (($ScaleDisplay == "M") ? "center" : "right");
				}
				
				# Assessment Marks & Display
				# No Assessment mark for Tack Ching
				if (!$ShowSubjectOverall) {
					for($i=0;$i<sizeof($ColumnID);$i++) {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : ""; 
						
						# for preview purpose
						if(!$StudentID) {
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						if($needStyle) {
							$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						} else {
							$thisMarkDisplay = $thisMark;
						}
						
						if($ShowSubjectFullMark) {
							//$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
		  					//$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
		  					//$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
							$thisFullMark = $SubjectFullMarkAry[$SubjectID];
							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
							
							$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
							$x[$SubjectID] .= $FullMark;
							$x[$SubjectID] .= "</td>";
						}
							
						$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= $thisMarkDisplay;
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				#if($ShowSubjectOverall && $CalculationOrder == 1) {
				if($ShowSubjectOverall) {
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID) {
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle) {
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
					
					if($ShowSubjectFullMark) {
						//$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					//$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					//$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$thisFullMark = $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= $thisFullMark;
						$x[$SubjectID] .= "</td>";
					}
						
					$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
					$x[$SubjectID] .= $thisMarkDisplay;
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>--</td>";
				}
				$isFirst = 0;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
				
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				if ($isSchemeAllGrade) {
					$cellAlign = "center";
				} else {
					$cellAlign = ($isSub == 1) ? "left" : (($ScaleDisplay == "M") ? "center" : "right");
				}
				$haveShowSubjectFullMark = 0;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$css_border_top = ($isFirst == 1 or $isSub == 0)? "border_top" : "";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>--</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
								$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
								
								$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
								$thisGrade = $ScaleDisplay=="G" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
								
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
								}
								$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
								if($needStyle)
								{
									$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
									$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								} else {
									$thisMarkDisplay = $thisMark;
								}
								
								if($haveShowSubjectFullMark == 0 && $ShowSubjectFullMark)
			  					{
									//$existNewFullMark = $this->returnStudentSubjectSPFullMark($thisReportID, $SubjectID, $StudentID, $ColumnID[$j]);
				  					//$tmpSubjectFullMark = (sizeof($existNewFullMark) > 0) ? $existNewFullMark[0] : $SubjectFullMarkAry[$SubjectID];
									$tmpSubjectFullMark = $SubjectFullMarkAry[$SubjectID];
									$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $tmpSubjectFullMark*$thisSubjectWeight : $tmpSubjectFullMark) : $tmpSubjectFullMark;
									$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
									$x[$SubjectID] .= $FullMark;
									$x[$SubjectID] .= "</td>";
									$haveShowSubjectFullMark = 1;
								}
								
								$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
								$x[$SubjectID] .= $thisMarkDisplay;
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
						
						# if no term reports, the term mark should be entered directly
						if (empty($thisReport)) {
							$thisColumnID = $ColumnData[$i]["ReportColumnID"];
							$thisReportID = $ReportID;
						} else {
							$thisColumnID = 0;
							$thisReportID = $thisReport['ReportID'];
						}
						
						$MarksAry = $this->getMarks($thisReportID, $StudentID);
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$thisColumnID][Mark] : "";
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$thisColumnID][Grade] : ""; 
 						
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						# for preview purpose
						if(!$StudentID) {
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$thisColumnID]['Grade']);
						if($needStyle)
						{
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						} else {
							$thisMarkDisplay = $thisMark;
						}
						
						if($haveShowSubjectFullMark == 0 && $ShowSubjectFullMark)
	  					{
							$tmpSubjectFullMark = $SubjectFullMarkAry[$SubjectID];
							if ($ScaleDisplay=="M" && $UseWeightedMark && $isSub) {
								$FullMark = $tmpSubjectFullMark*$thisSubjectWeight;
							} else {
								$FullMark = $tmpSubjectFullMark;
							}
 							
							$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
							$x[$SubjectID] .= $FullMark;
							$x[$SubjectID] .= "</td>";
							$haveShowSubjectFullMark = 1;
						}
						
						$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= $thisMarkDisplay;
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall && $CalculationOrder == 1)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
					
					$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
					$x[$SubjectID] .= $thisMarkDisplay;
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}' align='$cellAlign'>--</td>";
				}	
				$isFirst = 0;
			}
		}	# End Whole Year Report Type
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			if ($BasicInfo['Semester'] == "F") {
				$csvData = $this->getOtherInfoData($Type, 0, $ClassName);
				if(empty($csvData)) {
					$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);
				}
			} else {
				$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);
			}
			
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$Type][$key] = $val;
					}
				}
			}
		}
		
		######### conduct assessment table #########
		
		# summary
		$summary = "";
		if (isset($ary["summary"]) && sizeof($ary["summary"]) > 0) {
			$summary .= "<table style='border-width:0 1px 1px 0;border-style:solid;border-color:#000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
			foreach($ary["summary"] as $key => $value) {
				if (!is_numeric($key)) {
					if (strlen($value) == 2) {
						$value = $value[0]."<sup>".$value[1]."</sup>";
					}
					$summary .= "<tr>";
					$summary .= "<td width='80%' class='tabletext bold_text border_left border_top'>$key</td>";
					$summary .= "<td align='center' class='tabletext border_left border_top'>$value</td>";
					$summary .= "</tr>";
				}
			}
			$summary .= "</table>";
		}
		
		# attendance
		$attendance = "";
		if (isset($ary["attendance"]) && sizeof($ary["attendance"]) > 0) {
			$attendance .= "<table style='border-width:0 1px 1px 0;border-style:solid;border-color:#000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
			foreach($ary["attendance"] as $key => $value) {
				if (!is_numeric($key)) {
					$attendance .= "<tr>";
					$attendance .= "<td width='80%' class='tabletext bold_text border_left border_top'>$key</td>";
					$attendance .= "<td align='center' class='tabletext border_left border_top'>$value</td>";
					$attendance .= "</tr>";
				}
			}
			$attendance .= "</table>";
		}
		
		# merits & demerits
		$merits_items = "";
		if (isset($ary["merit"]) && sizeof($ary["merit"]) > 0) {
			if (isset($ary["merit"]["Merits"]) && is_numeric($ary["merit"]["Merits"])) {
				$merits_items .= $ary["merit"]["Merits"]." merit(s)<br />";
			}
			if (isset($ary["merit"]["Demerits"]) && is_numeric($ary["merit"]["Demerits"])) {
				$merits_items .= $ary["merit"]["Demerits"]." demerit(s)<br />";
			}
			/*
			if (isset($ary["merit"]["Minor Credit"]) && is_numeric($ary["merit"]["Minor Credit"])) {
				$merits_items .= $ary["merit"]["Minor Credit"]." minor credit(s)<br />";
			}
			if (isset($ary["merit"]["Major Credit"]) && is_numeric($ary["merit"]["Major Credit"])) {
				$merits_items .= $ary["merit"]["Major Credit"]." major credit(s)<br />";
			}
			if (isset($ary["merit"]["Minor Fault"]) && is_numeric($ary["merit"]["Minor Fault"])) {
				$merits_items .= $ary["merit"]["Minor Fault"]." minor fault(s)<br />";
			}
			if (isset($ary["merit"]["Major Fault"]) && is_numeric($ary["merit"]["Major Fault"])) {
				$merits_items .= $ary["merit"]["Major Fault"]." major fault(s)<br />";
			}
			*/
		}
		if ($merits_items == "") $merits_items = "---";
		$merits = "<tr>";
		$merits .= "<td align='center' class='tabletext bold_text border_left border_top'>Merits & Demerits</td>";
		$merits .= "</tr>";
		$merits .= "<tr>";
		$merits .= "<td align='left' style='padding-left:7px;' class='tabletext border_left border_top'>$merits_items</td>";
		$merits .= "</tr>";
		
		# activities & services
		$activities_items = "";
		
		# interschool competition awards
		if (isset($ary["interschool"]) && sizeof($ary["interschool"]) > 0) {
			if (is_array($ary["interschool"]["Record/Award"])) {
				for($i=0; $i<sizeof($ary["interschool"]["Record/Award"]); $i++) {
					$activities_items .= "<li>".$ary["interschool"]["Record/Award"][$i]."</li>";
				}
			} else {
				$activities_items .= "<li>".$ary["interschool"]["Record/Award"]."</li>";
			}
		}
		
		# school services
		if (isset($ary["schoolservice"]) && sizeof($ary["schoolservice"]) > 0) {
			if (is_array($ary["schoolservice"]["Service"])) {
				for($i=0; $i<sizeof($ary["schoolservice"]["Service"]); $i++) {
					$activities_items .= "<li>".$ary["schoolservice"]["Service"][$i]."</li>";
				}
			} else {
				$activities_items .= "<li>".$ary["schoolservice"]["Service"]."</li>";
			}
		}
		
		# extra curricular activities
		if (isset($ary["eca"]) && sizeof($ary["eca"]) > 0) {
			if (is_array($ary["eca"]["ECA"])) {
				for($i=0; $i<sizeof($ary["eca"]["ECA"]); $i++) {
					$activities_items .= "<li>".$ary["eca"]["ECA"][$i]."</li>";
				}
			} else {
				$activities_items .= "<li>".$ary["eca"]["ECA"]."</li>";
			}
		}
		
		if ($activities_items == "") {
			$activities_items = "---";
		} else {
			$activities_items = "<ul style='list-style-type:disc;'>$activities_items</ul>";
		}
		
		$activities = "<tr>";
		$activities .= "<td align='center' class='tabletext bold_text border_left border_top'>Activities & Services</td>";
		$activities .= "</tr>";
		$activities .= "<tr>";
		$activities .= "<td align='left' valign='top' class='tabletext border_left border_top' style='height:100%;padding-left:7px;'>$activities_items</td>";
		$activities .= "</tr>";
		
		
		$merits_and_activities = "<table style='border-width:0 1px 1px 0;border-style:solid;border-color:#000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
		$merits_and_activities .= $merits.$activities;
		$merits_and_activities .= "</table>";
		
		$x = $summary."<br/>".$attendance."<br/><br/><br/>".$merits_and_activities;
		
		return $x;
	}
	
	function getFooter($ReportID) {
		global $eReportCard;
		
		$markTable = "<table style='border-width: 0 1px 1px 0;border-style: solid;border-color: #000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
		$markTable .= "<tr>";
		$markTable .= 	"<td width='15%' align='center' class='tabletext border_left border_top'>Marks</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>90 - 100</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>75 - 89</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>60 - 74</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>30 - 59</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>Below 30</td>
						";
		$markTable .= "</tr>";
		$markTable .= "<tr>";
		$markTable .= 	"<td align='center' class='tabletext border_left border_top'>Grade</td>
						<td align='center' class='tabletext border_left border_top'>A</td>
						<td align='center' class='tabletext border_left border_top'>B</td>
						<td align='center' class='tabletext border_left border_top'>B</td>
						<td align='center' class='tabletext border_left border_top'>D</td>
						<td align='center' class='tabletext border_left border_top'>E</td>
						";
		$markTable .= "</tr>";
		$markTable .= "</table>";
		
		$x = "<tr>";
		$x .= "<td colspan='2'>";
		$x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' valign='top' width='10%'>Remarks</td>";
		$x .= "<td width='80%'>";
		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>1.</td><td class='tabletext'>Grading System:</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>&nbsp;</td><td class='tabletext'>$markTable</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>2.</td><td class='tabletext'>Pass Mark: 60</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>3.</td><td class='tabletext'>Conduct Assessment: A to D with sub-grades</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		
		return $x;
	}
	
	########### END Template Related

}
?>