<?php
# Editing by ivan

/*
 * Modification Log:
 * 2015-02-04 Bill [2014-1014-1144-21164]
 * 	- modified Get_Page2_Tables(), Get_OLE_Table()
 * 
 * 2013-07-03 Siuwan [2013-0703-0915-58167] 
 *	- edit spacing of schoolname in report title and academic performance
 *
 * 2012-02-22 Ivan [2012-0222-1122-49132]
 * 	- modified max award for Term Report from 10 to 14
 */

$ReportCardCustomSchoolName = ($ReportCardCustomSchoolName == '')? 'general' : $ReportCardCustomSchoolName;
include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		//$this->configFilesType = array("summary", "award", "merit", "attendance", "OLE");
		$this->configFilesType = array("award", "merit", "attendance", "OLE");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "－";
		$this->PositionDisplayUpperPercentage = 75;
		$this->ColorBlue = '#0023fa';
		
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->DisplayCharItemInReverseOrder = 0;
		$this->DefaultPersonalCharGrade = 30;
		$this->PersonalCharGradeArr['50'] = 'Excellent';
		$this->PersonalCharGradeArr['40'] = 'Very Good';
		$this->PersonalCharGradeArr['30'] = 'Good';
		$this->PersonalCharGradeArr['20'] = 'Fair';
		$this->PersonalCharGradeArr['10'] = 'Poor';
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID, $SubjectID, $SubjectSectionOnly, $PrintTemplateType, $numOfStudent, $studentCounter) {
		global $eReportCard, $PATH_WRT_ROOT;
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$AllSem = array_keys($this->GET_ALL_SEMESTERS());
		$isSecondTerm = $AllSem[1] == $SemID;
		
		// 2011-0221-1755-45096 point 1
		//if ($ReportType=='W' || $ReportType=='T' && ($FormNumber==5 || $FormNumber==7 || $isSecondTerm))
		// 2012-0203-1505-52132 
		//if ($ReportType=='W' || $ReportType=='T' && ($FormNumber==7 || $isSecondTerm))
		if ($ReportType=='W' || $ReportType=='T' && ($FormNumber==6 || $FormNumber==7 || $isSecondTerm))
			$showOLEPage = true;
		else
			$showOLEPage = false;
			
		$AttendanceTable = $MiscTable['AttendanceTable'];
		$PageTwoTables = $MiscTable['PageTwoTable'];
		$OLETable = $MiscTable['OLETable'];
		
		$pageCount = 1;
		
		### Page 1
		$PageOneTableTop = '';
		$PageOneTableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$PageOneTableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$PageOneTableTop .= "<tr><td class='student_info_text' colspan='2'><img src='".$emptyImagePath."' height='5'></td></tr>";
			$PageOneTableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$PageOneTableTop .= "<tr><td>&nbsp;</td></tr>";
			$PageOneTableTop .= "<tr><td>".$MSTable."</td></tr>";
			$PageOneTableTop .= "<tr><td>&nbsp;</td></tr>";
			$PageOneTableTop .= "<tr><td>".$AttendanceTable."</td></tr>";
		$PageOneTableTop .= "</table>";
		
		$PageOneTable = '';
		//$PageOneTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='page-break-after:always'>";
		$PageOneTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			//$PageOneTable .= "<tr><td valign='top' height='1004'>".$PageOneTableTop."</td></tr>";
			$PageOneTable .= "<tr><td valign='top' height='1055'>".$PageOneTableTop."</td></tr>";
			$PageOneTable .= "<tr><td valign='bottom'>".$this->Get_Page_Number_Table($pageCount++)."</td></tr>";
		$PageOneTable .= "</table>";
		
		### Page 2
		$PageTwoTableTop = '';
		$PageTwoTableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$PageTwoTableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$PageTwoTableTop .= "<tr><td class='student_info_text' colspan='2'><img src='".$emptyImagePath."' height='5'></td></tr>";
			$PageTwoTableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$PageTwoTableTop .= "<tr><td>&nbsp;</td></tr>";
			
			if ($showOLEPage) {
				$tableHeight = "height='893px'";
			}
			else {
				$tableHeight = "height='762px'";
			}
				
			$PageTwoTableTop .= "<tr><td $tableHeight valign='top'>".$PageTwoTables."</td></tr>";
			
			if ($showOLEPage == false)
			{
				# Display Signature in Page 2 if do not display OLE Page
				//$PageTwoTableTop .= "<tr><td>&nbsp;</td></tr>";
				$PageTwoTableTop .= "<tr><td valign='bottom'>".$SignatureTable."</td></tr>";
			}
		$PageTwoTableTop .= "</table>";
		
		if ($showOLEPage == false)
			$page_break_style = "";
		else
			$page_break_style = "style='page-break-after:always'";
			
		$PageTwoTable = '';
		//$PageTwoTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' $page_break_style>";
		$PageTwoTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='2' align='center' valign='top'>";
			$PageTwoTable .= "<tr><td valign='top' height='1055'>".$PageTwoTableTop."</td></tr>";
			$PageTwoTable .= "<tr><td valign='bottom'>".$this->Get_Page_Number_Table($pageCount++)."</td></tr>";
		$PageTwoTable .= "</table>";
		
//		if ($showOLEPage == true)
//		{
//			### Page 3
//			$PageThreeTableTop = '';
//			$PageThreeTableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
//				$PageThreeTableTop .= "<tr><td>".$TitleTable."</td></tr>";
//				$PageThreeTableTop .= "<tr><td class='student_info_text' colspan='2'><img src='".$emptyImagePath."' height='5'></td></tr>";
//				$PageThreeTableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
//				$PageThreeTableTop .= "<tr><td>&nbsp;</td></tr>";
//				//$PageThreeTableTop .= "<tr><td height='702px' valign='top'>".$OLETable."</td></tr>";
//				$PageThreeTableTop .= "<tr><td height='742px' valign='top'>".$OLETable."</td></tr>";
//				//$PageThreeTableTop .= "<tr><td><img src='".$emptyImagePath."' height='6'></td></tr>";
//				$PageThreeTableTop .= "<tr><td>&nbsp;</td></tr>";
//				$PageThreeTableTop .= "<tr><td>".$SignatureTable."</td></tr>";
//			$PageThreeTableTop .= "</table>";
//			
//			$PageThreeTable = '';
//			$PageThreeTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='2' align='center' valign='top' >";
//				$PageThreeTable .= "<tr><td valign='top' height='1055'>".$PageThreeTableTop."</td></tr>";
//				$PageThreeTable .= "<tr><td valign='bottom'>".$this->Get_Page_Number_Table($pageCount++)."</td></tr>";
//			$PageThreeTable .= "</table>";
//		}
		
		$x = "";
		$x .= "<tr><td valign='top'>";
			$x .= $PageOneTable;
		$x .= "</td></tr></table></div>";
		if ($studentCounter == $numOfStudent - 1) {
			$x .= "<div style='page-break-after:always;'>".$this->Get_Empty_Image(1)."</div>";
		}
		
		if (!$showOLEPage) {
			if ($studentCounter == $numOfStudent - 1) {
				$pageBreakStyle = '';
			}
			else {
				$pageBreakStyle = ' style="page-break-after:always;" ';
			}
		}
		else {
			$pageBreakStyle = ' style="page-break-after:always;" ';
		}
		
		//$x .= "<tr><td valign='top'>";
			$x .= '<div id="container" '.$pageBreakStyle.'>'.$PageTwoTable.'</div>';
		//$x .= "</td></tr>";
			
		if ($showOLEPage)
		{
			//$x .= "<tr><td valign='top'>";
			//$x .= $PageThreeTable;
			//$x .= "</td></tr>";
			$numOfOlePage = count($OLETable);
			for ($i=0; $i<$numOfOlePage; $i++) {
				$_oleTableHtml = $OLETable[$i];
				
				if ($i == $numOfOlePage - 1) {
					// last page
					if ($studentCounter == $numOfStudent - 1) {
						$pageBreakStyle = '';
					}
					else {
						$pageBreakStyle = ' style="page-break-after:always;" ';
					}
					$showSignatureTbl = true;
				}
				else {
					// normal page
					$pageBreakStyle = ' style="page-break-after:always;" ';
					
					$showSignatureTbl = false;
				}
				
				$_olePageTopHtml = '';
				$_olePageTopHtml .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
					$_olePageTopHtml .= "<tr><td>".$TitleTable."</td></tr>";
					$_olePageTopHtml .= "<tr><td class='student_info_text' colspan='2'><img src='".$emptyImagePath."' height='5'></td></tr>";
					$_olePageTopHtml .= "<tr><td>".$StudentInfoTable."</td></tr>";
					$_olePageTopHtml .= "<tr><td>&nbsp;</td></tr>";
					$_olePageTopHtml .= "<tr><td height='742px' valign='top'>".$_oleTableHtml."</td></tr>";
					$_olePageTopHtml .= "<tr><td>&nbsp;</td></tr>";
					if ($showSignatureTbl) {
						$_olePageTopHtml .= "<tr><td>".$SignatureTable."</td></tr>";
					}
				$_olePageTopHtml .= "</table>";
				
				$_olePageHtml = '';
				$_olePageHtml .= "<table width='100%' border='0' cellspacing='0' cellpadding='2' align='center' valign='top' >";
					$_olePageHtml .= "<tr><td valign='top' height='1055'>".$_olePageTopHtml."</td></tr>";
					$_olePageHtml .= "<tr><td valign='bottom'>".$this->Get_Page_Number_Table($pageCount++)."</td></tr>";
				$_olePageHtml .= "</table>";
				
				$x .= '<div id="container" '.$pageBreakStyle.'>'.$_olePageHtml.'</div>';
			}
		}
		
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard, $intranet_root;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitleArr = explode(':_:', $ReportTitle);
			$ReportTitle1 = $ReportTitleArr[0];
			$ReportTitle2 = $ReportTitleArr[1];
			
			# e.g. "2009-2010 Yearly", 2009-2010 is black and Yearly is blue
			$ReportTitle2Arr = explode(' ', $ReportTitle2);
			$ReportTitle2 = '<span style="color:#000000">'.array_shift($ReportTitle2Arr).'</span> '.implode(" ",$ReportTitle2Arr);
			
			$ReportTitle = $ReportTitle1.'<br />'.$ReportTitle2;
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			
			//2011-0221-1556-42073
			//$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
			//$SchoolLogo = ($imgfile != "") ? "<img src=\"/file/{$imgfile}\" width=90>\n" : "";
			//$imgfile = "/file/reportcard2008/templates/sha_tin_methodhist.gif";
			//$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" width=90>\n" : "";
			$imgfile = "/file/reportcard2008/templates/sha_tin_methodhist.png";
			$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" height=91.344>\n" : "";
			
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();
			$SchoolName = '';
			$SchoolName .= '<b>'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</b>';
			$SchoolName .= $this->Get_Empty_Image_Div('4px','4px');  //2013-0703-0915-58167 define div height for spacing
			$SchoolName .= $eReportCard['Template']['SchoolNameCh'];
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' style='color:".$this->ColorBlue."'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='font_16px' align='center' valign='top' style='color:".$this->ColorBlue."'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			
			
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			$data['DateOfIssue'] = date('d/n/Y', strtotime($data['DateOfIssue']));
			
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				$thisStudentNameEn = $StudentInfoArr[0]['EnglishName'];
				$thisStudentNameCh = $StudentInfoArr[0]['ChineseName'];
				if($eRCTemplateSetting['StudentInfo']['ChineseNameDisplay'][$StudentID]) {
					$thisStudentNameCh = $eRCTemplateSetting['StudentInfo']['ChineseNameDisplay'][$StudentID];
				}
				
				// 2011-0222-1604-48096
				//$data['Name'] = $lu->UserName2Lang('en', 2);
				if (strlen($thisStudentNameEn) >= 26)
					$data['Name'] = $thisStudentNameEn.'<br /><span class="font_chi">'.$thisStudentNameCh.'</span>';
				else
					$data['Name'] = $thisStudentNameEn.' <span class="font_chi">'.$thisStudentNameCh.'</span>';				
				
				//$data['ClassNo'] = $lu->ClassNumber;
				$data['ClassNo'] = $thisClassNumber;
				$data['StudentNo'] = $data['ClassNo'];
				//$data['Class'] = $lu->ClassName;
				$data['Class'] = $thisClassName;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = $lu->STRN;
				$data['RegNo'] = str_replace("#", "", $lu->WebSamsRegNo);
			}
			
			# hardcode the 1st six items
			$defaultInfoArray = array("Name", "Class", "RegNo", "STRN", "ClassNo", "DateOfIssue");
						
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
						$dataWidth = '310px';
					}
					else if(($count+1)%$StudentInfoTableCol==0)
						$titleWidth = '75px';	// last column
					else
					{
						$dataWidth = '80px';
					}
					
					if($count%$StudentInfoTableCol==0)
						$titleWidth = '70';	// first column
					else if(($count+1)%$StudentInfoTableCol==0)
						$titleWidth = '110';	// last column
					else
						$titleWidth = '70';
					
					$StudentInfoTable .= "<td class='tabletext2' valign='top' height='{$LineHeight}'>";
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
								              <tr>
								              	<td nowrap class='font_16px' width='".$titleWidth."' style='color:".$this->ColorBlue."; vertical-align:top;'>".$Title."</td>
								                <td width='2' style='color:".$this->ColorBlue."; vertical-align:top;'>";
					if ($Title != "") $StudentInfoTable .= ":&nbsp;";
					$StudentInfoTable .= "</td><td width='$dataWidth' class='font_16px' style='vertical-align:top;' nowrap>";
					$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
					$StudentInfoTable .= "</td></tr></table>\n";
					
					
					$StudentInfoTable .= "</td>";
										
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					} 
					$count++;
				}
			}
			
			# Display student info selected by the user other than the 1st six items
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						if(($count+1)%$StudentInfoTableCol==0)
							$titleWidth = '90';	// last column
						else
							$titleWidth = '60';
						
						$StudentInfoTable .= "<td class='tabletext2' width='33%' valign='top' height='{$LineHeight}'>";
						
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
									              <tr>
									              	<td nowrap class='font_16px' width='".$titleWidth."'>".$Title."</td>
									                <td width='2'>";
						if ($Title != "") $StudentInfoTable .= ":&nbsp;";
						$StudentInfoTable .= "</td><td nowrap class='font_16px' width='180'>";
						$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
						$StudentInfoTable .= "</td></tr></table>\n";
												
						$StudentInfoTable .= "</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}				
			}
			
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		if ($ReportType=='W') {
			$TermReportIDArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1);
			$LastTermReportID = $TermReportIDArr[count((array)$TermReportIDArr)-1]['ReportID'];
		}
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		//$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
		$BatchMarkArr = $this->getMarks($ReportID, '','',0,1);
		$MarksAry = $BatchMarkArr[$StudentID];
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];

		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllNAAry);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		$SubjectCodeIDAssoArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=1);
		
		##########################################
		# Start Generate Table
		##########################################
		
		$SessionTitleTable = $this->Get_Session_Title_Table(1);
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border'>";
		
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		$numSubjectDisplayed = 0;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
				
			$thisSubjectCode = $SubjectCodeIDAssoArr[$thisSubjectID]['CMP_CODEID'];
			$thisIsHideCmp = (in_array($thisSubjectCode, (array)$eRCTemplateSetting['HideComponentRowSubjectCodeArr']))? true : false;
			if ($thisIsHideCmp)
				continue;
				
			// Hide Subject if dropped in Second Term
			$LastTermHasStudy = true;
			if (!$isSub && $ReportType=='W') {
				$LastTermHasStudy = $this->Is_Student_In_Subject_Group_Of_Subject($LastTermReportID, $StudentID, $thisSubjectID);
			}
			
			# check if displaying subject row with all marks equal "N.A."
			if ($LastTermHasStudy && ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false))
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
				$numSubjectDisplayed++;
			}
		}
		
		### an empty row
		$numOfEmptyRow = 28 - $numSubjectDisplayed;
		
		for ($i=0; $i<$numOfEmptyRow; $i++)
		{
			$DetailsTable .= '<tr>';
				$DetailsTable .= '<td>&nbsp;</td>';
				$DetailsTable .= '<td class="border_left">&nbsp;</td>';
				$DetailsTable .= $this->Get_Double_Line_Td();
				$DetailsTable .= '<td>&nbsp;</td>';
				$DetailsTable .= '<td class="border_left">&nbsp;</td>';
				
				if ($ReportType == 'W')
				{
					$DetailsTable .= $this->Get_Double_Line_Td();
					$DetailsTable .= '<td>&nbsp;</td>';
					$DetailsTable .= '<td class="border_left">&nbsp;</td>';
				}
				
			$DetailsTable .= '</tr>';
		}
		
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $SessionTitleTable.$DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		if ($ReportType == 'W')
		{
			$markColumnWidth = '100';
			$rowspan = "rowspan='2'";
		}
		else
		{
			$markColumnWidth = '22%';
			$rowspan = '';
		}
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$n = 0;
		$e = 0; # column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		
		$x = '';
		$x .= "<tr>";
		
			# Subject
			$x .= "<td $rowspan valign='middle' align='left' class='small_title' height='{$LineHeight}' width='300' style='color:".$this->ColorBlue."'>";
				$x .= '&nbsp;&nbsp;'.$eReportCard['Template']['SubjectEn'];
			$x .= "</td>";
			
			# Weight Column
			$x .= "<td $rowspan class='border_left weighting_title' valign='middle' align='center' height='{$LineHeight}' width='50' style='color:".$this->ColorBlue."'>";
				$x .= $eReportCard['Template']['WeightingEn'];
			$x .= "</td>";
			
			/*
			# First Term
			$x .= "<td $rowspan valign='middle' align='center' class='tabletext' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
				$x .= $eReportCard['Template']['FirstTermEn'];
			$x .= "</td>";
			*/
			
			if ($ReportType == 'T')
			{
				# Double Line separator
				$x .= $this->Get_Double_Line_Td(1);
			
				# First Term Grade and Position
				//2011-0221-1703-15067
				//$x .= "<td valign='middle' align='center' class='tabletext' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
				$x .= "<td valign='middle' align='center' class='font_16px' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['GradeEn'];
				$x .= "</td>";
				
				//2011-0221-1703-15067
				//$x .= "<td valign='middle' align='center' class='border_left tabletext' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
				$x .= "<td valign='middle' align='center' class='border_left font_16px' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['PositionEn'];
				$x .= "</td>";
			}
			
			if ($ReportType == 'W')
			{
				# Double Line separator
				$x .= $this->Get_Double_Line_Td(2);
				
				# First Term
				$x .= "<td colspan='2' valign='middle' align='center' class='font_16px' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['FirstTermEn'];
				$x .= "</td>";
			
				# Double Line separator
				$x .= $this->Get_Double_Line_Td(2);
			
				# Second Term
				$x .= "<td colspan='2' valign='middle' align='center' class='font_16px' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['SecondTermEn'];
				$x .= "</td>";
			}
		$x .= "</tr>";
		
		if ($ReportType == 'W')
		{
			$x .= "<tr>";
				
				# First Term Grade and Position
				$x .= "<td valign='middle' align='center' class='border_top font_16px' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['GradeEn'];
				$x .= "</td>";
				$x .= "<td valign='middle' align='center' class='border_top border_left font_16px' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['PositionEn'];
				$x .= "</td>";
				
				# Second Term Grade and Position
				$x .= "<td valign='middle' align='center' class='border_top font_16px' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['GradeEn'];
				$x .= "</td>";
				$x .= "<td valign='middle' align='center' class='border_top border_left font_16px' width='".$markColumnWidth."' height='{$LineHeight}' style='color:".$this->ColorBlue."'>";
					$x .= $eReportCard['Template']['PositionEn'];
				$x .= "</td>";
			
			$x .= "</tr>";
		}
		
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID,$isAllNAAry = array())
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp; -&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
				 		$class_css = 'subject_title';
			 		}
			 		else
			 		{
				 		$class_css = 'sub_subject_title';
			 		}
			 		
			 		$css_border_top = ($isFirst)? "border_top" : "";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='".$class_css."'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					if(!$isAllNAAry[$SubSubjectID])
						$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if ($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$AllSem = array_keys($this->GET_ALL_SEMESTERS());
			$isSecondTerm = $AllSem[1] == $SemID;
		}
		
		if ($ReportType == 'T' && !$isSecondTerm)
		{
			$BoxWidth = '200';
			$border_left_css = '';
		}
		else
		{
			$BoxWidth = '130';
			$border_left_css = 'border_left';
		}
 		
		$x = '';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center" valign="top" class="report_border">'."\n";
			$x .= '<tr height="130">';
				
				if ($ReportType == 'W' || $isSecondTerm)
				{
					# Promotion Status Box
					$x .= '<td class="ms_footer_score" valign="top" style="color:'.$this->ColorBlue.'">';
						$x .= $eReportCard['Template']['PromotedTo'];
						$x .= '<br /><br /><br />';
						$x .= $eReportCard['Template']['PromotedToOnDiscretion'];
						$x .= '<br /><br /><br />';
						$x .= $eReportCard['Template']['RetainedIn'];
						$x .= '<br /><br />';
					$x .= '</td>';
				}
				
				$x .= '<td class="ms_footer_score '.$border_left_css 	.'" align="center" valign="top" style="color:'.$this->ColorBlue.'" width="'.$BoxWidth.'">'.$eReportCard['Template']['SchoolChop'].'</td>';
				$x .= '<td class="ms_footer_score border_left" align="center" valign="top" style="color:'.$this->ColorBlue.'" width="'.$BoxWidth.'">'.$eReportCard['Template']['Principal'].'</td>';
				$x .= '<td class="ms_footer_score border_left" align="center" valign="top" style="color:'.$this->ColorBlue.'" width="'.$BoxWidth.'">'.$eReportCard['Template']['ClassTeacher'].'</td>';
				$x .= '<td class="ms_footer_score border_left" align="center" valign="top" style="color:'.$this->ColorBlue.'" width="'.$BoxWidth.'">'.$eReportCard['Template']['ParentGuardian'].'</td>';
			$x .= '</tr>';
		$x .= '</table>';
			
		return $x;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		
  		// 2014-0306-0955-18066
  		if ($this->schoolYear=='2013' && $ReportID == 6) {
  			$FormNumOfStudent = 193;
  		}
  		else {
  			$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
  		}
  		
		
		foreach ($ColumnTitle as $ColumnID => $ColumnName) {
			
			$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
			
			$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
			$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
			
			$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
			$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
			$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
			$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
		}
		
		$first = 1;
		# Overall Position in Form
		if($ShowOverallPositionForm || $ShowNumOfStudentForm)
		{
			$border_top_css = ($first)? "border_top" : "";
			$thisTitleEn = $eReportCard['Template']['OverallPositionEn'];
			
			$x .= '<tr>';
				$x .= '<td colspan="2" class="'.$border_top_css.' subject_score" style="color:'.$this->ColorBlue.'">&nbsp;&nbsp;'.$thisTitleEn.'</td>';
				
				if ($ReportType == 'T')
				{
					$x .= $this->Get_Double_Line_Td();
					$x .= '<td colspan="2" class="'.$border_top_css.' ms_footer_score" align="center">';
						$x .= $this->Get_Position_Display($FormPosition, $FormNumOfStudent, $OverallPositionRangeForm);
					$x .= '</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$thisFormPosition = $columnFormPos[$ColumnID];
						$thisFormNumOfStudent = $columnFormNumOfStudent[$ColumnID];
						
						$x .= $this->Get_Double_Line_Td();
						$x .= '<td colspan="2" class="'.$border_top_css.' ms_footer_score" align="center">';
							$x .= $this->Get_Position_Display($thisFormPosition, $thisFormNumOfStudent, $OverallPositionRangeForm);
						$x .= '</td>';
					}
				}
			$x .= '</tr>';
			
			$first = 0;
		}
		
		# Overall Position in Class
		if($ShowOverallPositionClass || $ShowNumOfStudentClass)
		{
			$border_top_css = ($first)? "border_top" : "";
			$thisTitleEn = $eReportCard['Template']['OverallPositionEn'];
			
			$x .= '<tr>';
				$x .= '<td colspan="2" class="'.$border_top_css.' subject_score" style="color:'.$this->ColorBlue.'">&nbsp;&nbsp;'.$thisTitleEn.'</td>';
				
				if ($ReportType == 'T')
				{
					$x .= $this->Get_Double_Line_Td();
					$x .= '<td colspan="2" class="'.$border_top_css.' ms_footer_score" align="center">';
						$x .= $this->Get_Position_Display($ClassPosition, $ClassNumOfStudent, $OverallPositionRangeClass);
					$x .= '</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$thisClassPosition = $columnClassPos[$ColumnID];
						$thisClassNumOfStudent = $columnClassNumOfStudent[$ColumnID];
						
						$x .= $this->Get_Double_Line_Td();
						$x .= '<td colspan="2" class="'.$border_top_css.' ms_footer_score" align="center">';
							$x .= $this->Get_Position_Display($thisClassPosition, $thisClassNumOfStudent, $OverallPositionRangeClass);
						$x .= '</td>';
					}
				}
			$x .= '</tr>';
			
			$first = 0;
		}
		
		$x .= '<tr><td colspan="10" class="border_top">';
            //$x .= $this->Get_Grading_Scheme_Info_Table();
            $x .= $this->Get_Grading_Scheme_Info_Table($ReportID);
		$x .= '</td></tr>';
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
					$css_score_class = 'subject_score';
				}
				else
				{
					$css_score_class = 'sub_subject_score';
				}
				
				# define css
				$css_border_top = ($isFirst)? "border_top" : "";
	
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, $withGrandResult=0, $returnAsso=0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
									
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				//if($ShowSubjectOverall)
				//{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
					$thisFormPosition = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$thisFormNoOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
					
					//2016-0630-0913-38236
					//(Internal) Follow-up by ivanko on 2016-07-04 10:33 <-- fixed in this reply and therefore no need hardcode now
//					if ($ReportID == 14 && ($SubjectID == 37 || $SubjectID == 54)) {
//						$thisFormNoOfStudent = 140;
//					}
//					if ($ReportID == 18 && ($SubjectID == 37 || $SubjectID == 54)) {
//						$thisFormNoOfStudent = 134;
//					}
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					if($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if(in_array($thisMark, $this->Get_Exclude_Ranking_Special_Case()) && !in_array($thisMark, (array)$eRCTemplateSetting['ReportGeneration']['NeedDisplaySpecialCaseAry']))
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					
					# Weight
					if ($isSub)
					{
						$thisWeightDisplay = '&nbsp;';
					}
					else
					{
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID is null Or ReportColumnID = '') and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisWeightDisplay = $thisSubjectWeight;
					}
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}' align='center'>".$thisWeightDisplay."</td>";
					
					# Double Line Cell
					$x[$SubjectID] .= $this->Get_Double_Line_Td();
						
					# Grade
					$thisMarkDisplay = ($thisMarkDisplay=='')? '&nbsp;' : $thisMarkDisplay;
					$x[$SubjectID] .= "<td class='{$css_score_class} {$css_border_top}' align='center'>". $thisMarkDisplay ."</td>";
					
					# Position
					$thisPositionDisplay = $this->Get_Position_Display($thisFormPosition, $thisFormNoOfStudent, $OverallPositionRangeForm);
					$x[$SubjectID] .= "<td class='{$css_score_class} {$css_border_top} border_left' align='center'>". $thisPositionDisplay ."</td>";
					
				//} else if ($ShowRightestColumn) {
				//	$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				//}
				if(!$isAllNA)
					$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, $withGrandResult=0, $returnAsso=0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
					
					$css_score_class = 'subject_score';
				}
				else
				{
					$css_score_class = 'sub_subject_score';
				}
				
				$isAllNA = true;
				$css_border_top = $isFirst? "border_top" : "";
				
				# Weight
				if ($isSub)
				{
					$thisWeightDisplay = '&nbsp;';
				}
				else
				{
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID is null Or ReportColumnID = '') and SubjectID = '$SubjectID' ");
					$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
					
					$thisWeightDisplay = $thisSubjectWeight;
				}
				$x[$SubjectID] .= "<td class='border_left {$css_border_top}' align='center'>".$thisWeightDisplay."</td>";

				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					$css_border_top = $isFirst? "border_top" : "";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					# Retrieve Terms Overall marks
					//{
						//if ($isParentSubject && $CalculationOrder == 1) {
						//	$thisMarkDisplay = $this->EmptySymbol;
						//} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
//								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								$BatchMarkArr = $this->getMarks($thisReportID, '',"","",1);
								$MarksAry = $BatchMarkArr[$StudentID];
								
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								$thisFormPosition = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]['OrderMeritForm'];
								$thisFormNoOfStudent = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]['FormNoOfStudent'];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
//								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								$BatchMarkArr = $this->getMarks($thisReportID, '',"","",1);
								$MarksAry = $BatchMarkArr[$StudentID];

								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
								$thisFormPosition = $MarksAry[$SubjectID][0]['OrderMeritForm'];
								$thisFormNoOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
								
								//2016-0630-0913-38236
								//(Internal) Follow-up by ivanko on 2016-07-04 10:33 <-- fixed in this reply and therefore no need hardcode now
//								if ($thisReportID == 14 && ($SubjectID == 37 || $SubjectID == 54)) {
//									$thisFormNoOfStudent = 140;
//								}
//								if ($thisReportID == 18 && ($SubjectID == 37 || $SubjectID == 54)) {
//									$thisFormNoOfStudent = 134;
//								}
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						//}
							
						
						# Double Line Cell
						$x[$SubjectID] .= $this->Get_Double_Line_Td();
							
						# Grade
						$x[$SubjectID] .= "<td class='{$css_score_class} {$css_border_top}' align='center'>". $thisMarkDisplay ."</td>";
						
						# Position
						$thisPositionDisplay = $this->Get_Position_Display($thisFormPosition, $thisFormNoOfStudent, $OverallPositionRangeForm);
						$x[$SubjectID] .= "<td class='{$css_score_class} {$css_border_top} border_left' align='center'>". $thisPositionDisplay ."</td>";
						
					//}
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		$TableArr['AttendanceTable'] = $this->Get_Attendance_Table($ReportID, $StudentID);
		$TableArr['PageTwoTable'] = $this->Get_Page2_Tables($ReportID, $StudentID);
		$TableArr['OLETable'] = $this->Get_OLE_Table($ReportID, $StudentID);
		
		return $TableArr;
	}
	
	function Get_Attendance_Table($ReportID, $StudentID='')
	{
		global $eReportCard;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
 		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'attendance');
		$AttendanceInfoArr = $OtherInfoDataAry[$StudentID];
		
		$markColumnWidth = ($ReportType == 'W')? '200' : '400';
		
		$SemArr = array();
		if ($ReportType == 'W')
		{
			$ColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnInfoArr);
			
			for ($i=0; $i<$numOfColumn; $i++)
				$SemArr[] = $ColumnInfoArr[$i]['SemesterNum'];
		}
		else
		{
			$numOfColumn = 1;
			$SemArr[] = $SemID;
		}
		
		$x = '';
		$x .= $this->Get_Session_Title_Table(2);
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" valign="top">'."\n";
			if ($ReportType == 'W')
			{
				$x .= '<tr>';
					$x .= '<td width="360">&nbsp;</td>';
					$border_right_css = ($ReportType == 'W')? '' : 'border_right';
					$x .= '<td class="subject_score border_left border_top '.$border_right_css.'" width="'.$markColumnWidth.'" align="center" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['FirstTermEn'].'&nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['FirstTermCh'].'</td>';
					if ($ReportType == 'W')
						$x .= '<td class="subject_score border_left border_right border_top" width="'.$markColumnWidth.'" align="center" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['SecondTermEn'].'&nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['SecondTermCh'].'</td>';
				$x .= '</tr>';
			}
			
			# Days Absent
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left border_top" style="color:'.$this->ColorBlue.'" width="360">'.$eReportCard['Template']['DaysAbsentEn'].'</td>';
				
				for ($i=0; $i<$numOfColumn; $i++)
				{
					$border_right_css = ($ReportType == 'W')? '' : 'border_right';
					
					if ($ReportType == 'W' && $i == 1)
						$border_right_css = 'border_right';
					
					$thisSemID = $SemArr[$i];
					$x .= '<td class="subject_score border_left border_top '.$border_right_css.'" align="center">';
						if ($StudentID != '')
							$thisDisplay = $AttendanceInfoArr[$thisSemID]['Days Absent'];
						else
							$thisDisplay = 'xxx';
							
						$thisDisplay = ($thisDisplay == '')? $this->EmptySymbol : $thisDisplay;
						$x .= $thisDisplay;
					$x .= '</td>';
				}
				
			$x .= '</tr>';
			
			# Times Late
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left border_top border_bottom" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['TimesLateEn'].'</td>';
				
				for ($i=0; $i<$numOfColumn; $i++)
				{
					$border_right_css = ($ReportType == 'W')? '' : 'border_right';
					
					if ($ReportType == 'W' && $i == 1)
						$border_right_css = 'border_right';
					
					$thisSemID = $SemArr[$i];
					$x .= '<td class="subject_score border_left border_top border_bottom '.$border_right_css.'" align="center">';
						if ($StudentID != '')
							$thisDisplay = $AttendanceInfoArr[$thisSemID]['Times Late'];
						else
							$thisDisplay = 'xxx';
							
						$thisDisplay = ($thisDisplay == '')? $this->EmptySymbol : $thisDisplay;
						$x .= $thisDisplay;
					$x .= '</td>';
				}
				
			$x .= '</tr>';
			
		$x .= '</table>';
			
		return $x;
	}
	
	# Personal Qualities & Merits and Demerits & Awards Tables
	function Get_Page2_Tables($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_award.php');
		$lreportcard_award = new libreportcard_award();
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
 		
		$markColumnWidth = ($ReportType == 'W')? '200' : '400';
		$SemArr = array();
		if ($ReportType == 'W')
		{
			$ColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnInfoArr);
			
			for ($i=0; $i<$numOfColumn; $i++)
			{
				$SemArr[] = $ColumnInfoArr[$i]['SemesterNum'];
				$TermReportID = $this->getCorespondingTermReportID($ReportID,$ColumnInfoArr[$i]['ReportColumnID']);
				$SummaryInfoArr[$ColumnInfoArr[$i]['SemesterNum']] =  $this->getPersonalCharacteristicsProcessedData($StudentID, $TermReportID, 0);
			}
		}
		else
		{
			$numOfColumn = 1;
			$SemArr[] = $SemID;
			$SummaryInfoArr[$SemID] = $this->getPersonalCharacteristicsProcessedData($StudentID, $ReportID, 0);
			$AllSem = array_keys($this->GET_ALL_SEMESTERS());
			$isSecondTerm = $AllSem[1] == $SemID;
		}
		
		
		### Personal Qualities // modified by Marcus, check from CSV to Personal Characteristic
		//$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'summary');
		//$SummaryInfoArr = $OtherInfoDataAry[$StudentID];
		
		$x = '';
		$x .= $this->Get_Session_Title_Table(3);
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center" valign="top">'."\n";
			$x .= '<tr>';
				$x .= '<td width="100">&nbsp;</td>';
				$x .= '<td class="subject_score border_left border_top" width="360" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['SesseionTitle'][3].'</td>';
				$x .= $this->Get_Double_Line_Td('', 1);
				
				$border_right_css = ($ReportType == 'W')? '' : 'border_right';
				$x .= '<td class="subject_score border_top '.$border_right_css.'" width="'.$markColumnWidth.'" align="center" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['CommentsEn'].'</td>';
				
				if ($ReportType == 'W')
					$x .= '<td class="subject_score border_left border_right border_top" width="'.$markColumnWidth.'" align="center" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['SecondTermEn'].'</td>';
			$x .= '</tr>';
			
			// Conduct -> Trustworthiness
			$x .= '<tr>';
				$x .= '<td rowspan="3" class="subject_score border_left border_top" style="color:'.$this->ColorBlue.'" align="center" valign="middle">'.$eReportCard['Template']['ConductEn'].'</td>';
				$x .= '<td class="subject_score border_left border_top" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['TrustworthinessEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Trustworthiness', 1);
			$x .= '</tr>';
			
			// Conduct -> Self-Discipline
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['SelfDisciplineEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Self-discipline');
			$x .= '</tr>';
			
			// Conduct -> Politeness
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['PolitenessEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Politeness');
			$x .= '</tr>';
			
			// Ability -> Analytical and Critical Thinking
			$x .= '<tr>';
				$x .= '<td rowspan="3" class="subject_score border_left border_top" style="color:'.$this->ColorBlue.'" align="center" valign="middle">'.$eReportCard['Template']['AbilityEn'].'</td>';
				$x .= '<td class="subject_score border_left border_top" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['AnalyticalAndCriticalThinkingEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Analytical and Critical Thinking', 1);
			$x .= '</tr>';
			
			// Ability -> Interpersonal Skill
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['InterpersonalSkillEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Interpersonal Skill');
			$x .= '</tr>';
			
			// Ability -> Leadership
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['LeadershipEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Leadership');
			$x .= '</tr>';
			
			// Attitude -> Learning Attitude
			$x .= '<tr>';
				$x .= '<td rowspan="3" class="subject_score border_left border_top border_bottom" style="color:'.$this->ColorBlue.'" align="center" valign="middle">'.$eReportCard['Template']['AttitudeEn'].'</td>';
				$x .= '<td class="subject_score border_left border_top" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['LearningAttitudeEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Learning Attitude', 1);
			$x .= '</tr>';
			
			// Attitude -> Willingness To Serve
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['WillingnessToServeEn'].'</td>';
				$x .= $this->Get_Double_Line_Td();
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Willingness to Serve');
			$x .= '</tr>';
			
			// Attitude -> Initiative
			$x .= '<tr>';
				$x .= '<td class="subject_score border_left border_bottom" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['InitiativeEn'].'</td>';
				$x .= $this->Get_Double_Line_Td('', 0, 1);
				$x .= $this->Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $SummaryInfoArr, 'Initiative', 0, 1);
			$x .= '</tr>';
		$x .= '</table>';
		$x .= '<br />';
		
		
		### Merits and Demerits
		// display last term merit data if it is a consolidated report
		$thisSemID = ($ReportType == 'T')? $SemID : $ColumnInfoArr[$numOfColumn-1]['SemesterNum'];
		
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'merit');
		$MeritInfoArr = $OtherInfoDataAry[$StudentID];
		$MeritInfoArr[$thisSemID]['Merit'] = ($MeritInfoArr[$thisSemID]['Merit'] == '')? $this->EmptySymbol : $MeritInfoArr[$thisSemID]['Merit'];
		$MeritInfoArr[$thisSemID]['Contribution'] = ($MeritInfoArr[$thisSemID]['Contribution'] == '')? $this->EmptySymbol : $MeritInfoArr[$thisSemID]['Contribution'];
		$MeritInfoArr[$thisSemID]['Major Contribution'] = ($MeritInfoArr[$thisSemID]['Major Contribution'] == '')? $this->EmptySymbol : $MeritInfoArr[$thisSemID]['Major Contribution'];
		$MeritInfoArr[$thisSemID]['Demerit'] = ($MeritInfoArr[$thisSemID]['Demerit'] == '')? $this->EmptySymbol : $MeritInfoArr[$thisSemID]['Demerit'];
		$MeritInfoArr[$thisSemID]['Offense'] = ($MeritInfoArr[$thisSemID]['Offense'] == '')? $this->EmptySymbol : $MeritInfoArr[$thisSemID]['Offense'];
		$MeritInfoArr[$thisSemID]['Major Offense'] = ($MeritInfoArr[$thisSemID]['Major Offense'] == '')? $this->EmptySymbol : $MeritInfoArr[$thisSemID]['Major Offense'];
		
		$x .= $this->Get_Session_Title_Table(4);
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center" valign="top">'."\n";
			$x .= '<tr>';
				//$x .= '<td width="150">&nbsp;</td>';
				//$x .= $this->Get_Double_Line_Td('', 1);
				$x .= '<td class="subject_score border_left border_top" align="center" width="17%" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Merit'].'</td>';
				$x .= '<td class="subject_score border_left border_top" align="center" width="17%" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Contribution'].'</td>';
				$x .= '<td class="subject_score border_left border_top" align="center" width="17%" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['MajorContribution'].'</td>';
				$x .= $this->Get_Double_Line_Td('', 1);
				$x .= '<td class="subject_score border_top" align="center" width="17%" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Demerit'].'</td>';
				$x .= '<td class="subject_score border_left border_top" align="center" width="17%" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Offense'].'</td>';
				$x .= '<td class="subject_score border_left border_top border_right" align="center" width="17%" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['MajorOffense'].'</td>';
			$x .= '</tr>';
			
			$x .= '<tr>';
				//$x .= '<td class="grading_scheme_text border_left border_top border_bottom" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['NumberOf'].'<br />'.$eReportCard['Template']['Merits&Demerits'].'</td>';
				//$x .= $this->Get_Double_Line_Td('', 0, 1);
				$x .= '<td class="subject_score border_left border_top border_bottom" align="center" >'.$MeritInfoArr[$thisSemID]['Merit'].'</td>';
				$x .= '<td class="subject_score border_left border_top border_bottom" align="center" >'.$MeritInfoArr[$thisSemID]['Contribution'].'</td>';
				$x .= '<td class="subject_score border_left border_top border_bottom" align="center" >'.$MeritInfoArr[$thisSemID]['Major Contribution'].'</td>';
				$x .= $this->Get_Double_Line_Td('', 0, 1);
				$x .= '<td class="subject_score border_top border_bottom" align="center" >'.$MeritInfoArr[$thisSemID]['Demerit'].'</td>';
				$x .= '<td class="subject_score border_left border_top border_bottom" align="center" >'.$MeritInfoArr[$thisSemID]['Offense'].'</td>';
				$x .= '<td class="subject_score border_left border_top border_right border_bottom" align="center" >'.$MeritInfoArr[$thisSemID]['Major Offense'].'</td>';
			$x .= '</tr>';
		$x .= '</table>';
		$x .= '<br />';
		
		
		### List of Awards and Major Achievements
		# Get Awards from Award Generation
		$FirstTermID = $SemArr[0];
		$FirstTermReportInfoArr = $this->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, $FirstTermID, $isMainReport=1, $excludeSemester='');
		$FirstTermReportID = $FirstTermReportInfoArr['ReportID'];
		$FirstTermGeneratedAwardArr = $lreportcard_award->Get_Award_Student_Record($FirstTermReportID, $StudentID, $ReturnAsso=0);
		$FirstTermGeneratedAwardArr = explode("\n", $FirstTermGeneratedAwardArr[0]['AwardText']);
		
		// Consolidated Report - set TermID as 'W' to get award [2014-1014-1144-21164]
		if ($ReportType == 'W') {
			$SecondTermID = 'F';
		}
		else {
			$SecondTermID = $SemArr[1];
		}
		$SecondTermReportInfoArr = $this->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, $SecondTermID, $isMainReport=1, $excludeSemester='');
		$SecondTermReportID = $SecondTermReportInfoArr['ReportID'];
		$SecondTermGeneratedAwardArr = $lreportcard_award->Get_Award_Student_Record($SecondTermReportID, $StudentID, $ReturnAsso=0);
		$SecondTermGeneratedAwardArr = explode("\n", $SecondTermGeneratedAwardArr[0]['AwardText']);
		
		// Second Term Report - get awards from consolidated report [2014-1014-1144-21164]
		if($isSecondTerm){
			
			// get condolidated report ID
			$relatedConsolidatedReport = $this->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, 'F');
			$consolidatedReportID = $relatedConsolidatedReport['ReportID'];
			
			// get award data -> replace award data of second term
			$FirstTermGeneratedAwardArr = $lreportcard_award->Get_Award_Student_Record($consolidatedReportID, $StudentID, $ReturnAsso=0);
			$FirstTermGeneratedAwardArr = explode("\n", $FirstTermGeneratedAwardArr[0]['AwardText']);
			
		}
		
		# Get Awards from csv
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'award');
		$AwardInfoArr = $OtherInfoDataAry[$StudentID];
		
		$FirstTermCsvAwardArr = $AwardInfoArr[$SemArr[0]]['Award'];
		$SecondTermCsvAwardArr = $AwardInfoArr[$SemArr[1]]['Award'];
		
		$FirstTermAwardArr = array_values(array_remove_empty(array_merge((array)$FirstTermGeneratedAwardArr, (array)$FirstTermCsvAwardArr)));
		$YearlyAwardArr = array_values(array_remove_empty(array_merge((array)$SecondTermGeneratedAwardArr, (array)$SecondTermCsvAwardArr)));
		
			
		
		# maximum display 10 items
		$numOfRow = ($ReportType=='T')? count($FirstTermAwardArr) : max(count($FirstTermAwardArr), count($YearlyAwardArr));
//		if ($numOfRow > 10)
//		{
//			$numOfRow = 10;
//		}
		
		// 2012-0222-1122-49132
		//$maxRow = $isSecondTerm? 15 : 10;
		//2014-1014-1144-21164
		//$maxRow = $isSecondTerm? 15 : 14;
		$maxRow = $isSecondTerm? 20 : 15;
		$numOfRow = min($maxRow,$numOfRow);
		$x .= $this->Get_Session_Title_Table(5, $isSecondTerm);
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">'."\n";
			$x .= '<tr>';
				$x .= '<td width="4%">&nbsp</td>'."\n";
				$x .= '<td>'."\n";
					if ($ReportType=='T')
					{
						### Number List
						$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center" valign="top">'."\n";
							for($i=0; $i<$numOfRow; $i++)
							{
								$thisNumber = $i + 1;
								$thisFirstTermAward = $FirstTermAwardArr[$i];
								
								$x .= '<tr class="subject_score">';
									$x .= '<td width="10">'.$thisNumber.'. </td>';
									$x .= '<td >'.$thisFirstTermAward.'</td>';
								$x .= '</tr>';
							}
							$x .= '<tr><td width="10">&nbsp;</td><td class="grading_scheme_text">***</td></tr>';
						$x .= '</table>';
					}
					else
					{
						### Table with Two Columns
						$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" valign="top" class="report_border">'."\n";
							$x .= '<tr>';
								$x .= '<td align="center" width="50%" class="grading_scheme_text" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['FirstTermEn'].'</td>';
								if ($ReportType=='W')
									$x .= '<td align="center" width="50%" class="border_left grading_scheme_text" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['YearlyEn'].'</td>';
							$x .= '</tr>';
							
							for ($i=0; $i<$numOfRow; $i++)
							{
								$thisFirstTermAward = $FirstTermAwardArr[$i];
								$thisYearlyAward = $YearlyAwardArr[$i];
								
								$thisFirstTermAlign = '';
								if ($thisFirstTermAward == '')
								{
									$thisFirstTermAward = $this->EmptySymbol;
									$thisFirstTermAlign = 'align="center"';
								}
								
								$thisYearlyTermAlign = '';
								if ($thisYearlyAward == '')
								{
									$thisYearlyAward = $this->EmptySymbol;
									$thisYearlyTermAlign = 'align="center"';
								}
								
								$x .= '<tr>';
									$x .= '<td '.$thisFirstTermAlign.' class="border_top grading_scheme_text">'.$thisFirstTermAward.'</td>';
									if ($ReportType=='W')
										$x .= '<td '.$thisYearlyTermAlign.' class="border_top border_left grading_scheme_text">'.$thisYearlyAward.'</td>';
								$x .= '</tr>';
							}
							
						$x .= '</table>';
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_OLE_Table($ReportID, $StudentID='')
	{
		global $eReportCard;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
 		### Determine which term info to be displayed
 		if ($ReportType == 'W')
 		{
			$ColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnInfoArr) - 1;
			// Consolidated Report - show OLE Record of second term [2014-1014-1144-21164]
//			$thisSemID = $ColumnInfoArr[$numOfColumn-1]['SemesterNum'];
			$thisSemID = $ColumnInfoArr[$numOfColumn]['SemesterNum'];
		}
		else
		{
			$thisSemID = $SemID;
		}
		
		### Get csv data
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', 'OLE');
		$OleInfoArr = $OtherInfoDataAry[$StudentID][$thisSemID];
		$numOfOleInfo = count($OleInfoArr['Programme']);
		
		if (!is_array($OleInfoArr['Programme']))
			$OleInfoArr['Programme'] = array($OleInfoArr['Programme']);
//		if (!is_array($OleInfoArr['Roles/Awards']))
//			$OleInfoArr['Roles/Awards'] = array($OleInfoArr['Roles/Awards']);
		if (!is_array($OleInfoArr['Roles']))
			$OleInfoArr['Roles'] = array($OleInfoArr['Roles']);
		if (!is_array($OleInfoArr['Awards']))
			$OleInfoArr['Awards'] = array($OleInfoArr['Awards']);
		if (!is_array($OleInfoArr['Remarks']))
			$OleInfoArr['Remarks'] = array($OleInfoArr['Remarks']);
		
		
		//2014-0618-0946-13071 - add one more page if student having more than 12 OLE records	
//		# if no record => first line show "xxx"
//		if ($numOfOleInfo == 0)
//		{
//			$OleInfoArr['Programme'] = array('***');
////			$OleInfoArr['Roles/Awards'] = array('***');
//			$OleInfoArr['Roles'] = array('***');
//			$OleInfoArr['Awards'] = array('***');
//			$OleInfoArr['Remarks'] = array('***');
//			
//			$numOfOleInfo = 1;
//		}
//		
//		# maximum display 15 items
//		$maxOleRow = 15;
//		if ($numOfOleInfo > $maxOleRow)
//			$numOfOleInfo = $maxOleRow;
//			
//		$programWidth = 38;
//		$roleWidth = 15;
//		//2012-0628-0948-00132
//		//$awardWidth = 15;
//		//$remarksWidth = 32;
//		$awardWidth = 23;
//		$remarksWidth = 24;
//		
//		$x = '';
//		$x .= $this->Get_Session_Title_Table(6);
//		
//		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" valign="top" class="report_border">'."\n";
//			$x .= '<tr>';
//				$x .= '<td width="'.$programWidth.'%" class="ms_footer_score" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['ProgrammesEn'].'</td>';
//				$x .= '<td align="center" width="'.$roleWidth.'%" class="ms_footer_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Role'].'</td>';
//				$x .= '<td align="center" width="'.$awardWidth.'%" class="ms_footer_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Awards'].'</td>';
//				$x .= '<td align="center" width="'.$remarksWidth.'%" class="ms_footer_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['RemarksEn'].'</td>';
//			$x .= '</tr>';
//			
//			for ($i=0; $i<$numOfOleInfo; $i++)
//			{
//				$thisProgramme = $OleInfoArr['Programme'][$i];
//				$thisRole = nl2br($OleInfoArr['Roles'][$i]);
//				$thisAward = nl2br($OleInfoArr['Awards'][$i]);
//				$thisRemarks = nl2br($OleInfoArr['Remarks'][$i]);
//				
//				$thisProgrammeAlign = '';
//				if ($thisProgramme == '')
//				{
//					$thisProgramme = $this->EmptySymbol;
//					$thisProgrammeAlign = 'align="center"';
//				}
//				
//				if ($thisRole == '')
//				{
//					$thisRole = $this->EmptySymbol;
//				}
//
//				if ($thisAward == '')
//				{
//					$thisAward = $this->EmptySymbol;
//				}
//				
//				if ($thisRemarks == '')
//				{
//					$thisRemarks = $this->EmptySymbol;
//				}
//				
//				$x .= '<tr height="40">';
//					$x .= '<td '.$thisProgrammeAlign.' class="border_top ms_footer_score">'.$thisProgramme.'</td>';
//					$x .= '<td class="border_top border_left ms_footer_score" align="center">'.$thisRole.'</td>';
//					$x .= '<td class="border_top border_left ms_footer_score" align="center">'.$thisAward.'</td>';
//					$x .= '<td class="border_top border_left ms_footer_score" align="center">'.$thisRemarks.'</td>';
//				$x .= '</tr>';
//			}
//			
//			/*
//			$numOfEmptyRow = $maxOleRow - $numOfOleInfo;
//			for ($i=0; $i<$numOfEmptyRow; $i++)
//			{
//				$x .= '<tr height="45">';
//					$x .= '<td class="ms_footer_score border_top" style="color:'.$this->ColorBlue.'">&nbsp;</td>';
//					$x .= '<td align="center" width="20%" class="ms_footer_score border_left border_top" style="color:'.$this->ColorBlue.'">&nbsp;</td>';
//					$x .= '<td align="center" width="50%" class="ms_footer_score border_left border_top" style="color:'.$this->ColorBlue.'">&nbsp;</td>';
//				$x .= '</tr>';
//			}
//			*/
//		$x .= '</table>';
		
		
		// 2014-1014-1144-21164
		// consolidate data for one program multiple awards
		$consolidatedOleAry = array();
		$numOfProgramme = count((array)$OleInfoArr['Programme']);
		$programCount = -1;
		for ($i=0; $i<$numOfProgramme; $i++) {
			$_programme = trim($OleInfoArr['Programme'][$i]);
			$_role = trim($OleInfoArr['Roles'][$i]);
			$_awards = trim($OleInfoArr['Awards'][$i]);
			$_remarks = trim($OleInfoArr['Remarks'][$i]);
			
			if ($_programme == '') {
				// append data to previous record
				$consolidatedOleAry['Roles'][$programCount] .= '<br />'.$_role;
				$consolidatedOleAry['Awards'][$programCount] .= '<br />'.$_awards;
				$consolidatedOleAry['Remarks'][$programCount] .= '<br />'.$_remarks;
			}
			else {
				// new programme data
				$programCount++;
				$consolidatedOleAry['Programme'][$programCount] = $_programme;
				$consolidatedOleAry['Roles'][$programCount] = $_role;
				$consolidatedOleAry['Awards'][$programCount] = $_awards;
				$consolidatedOleAry['Remarks'][$programCount] = $_remarks;
			}
		}
		
		$OleInfoArr = array();
		//2014-1014-1144-21164
		//$maxNumOfItem = 15;
		$maxNumOfItem = 12;
		// add (array) to prevent error
		$OleInfoArr['Programme'] = array_chunk((array)$consolidatedOleAry['Programme'], $maxNumOfItem);
		$OleInfoArr['Roles'] = array_chunk((array)$consolidatedOleAry['Roles'], $maxNumOfItem);
		$OleInfoArr['Awards'] = array_chunk((array)$consolidatedOleAry['Awards'], $maxNumOfItem);
		$OleInfoArr['Remarks'] = array_chunk((array)$consolidatedOleAry['Remarks'], $maxNumOfItem);
		$numOfPages = count($OleInfoArr['Programme']);
		
		$oleTableHtmlAry = array();
		//2015-0706-1521-39207: added zero OLE records handling
		if ($numOfPages==0) {
			$oleTableHtmlAry[] = $this->Generate_OLE_Table_Html($ReportID, array());
		}
		else {
			for ($i=0; $i<$numOfPages; $i++) {
				$_tmpOleAry = array();
				$_tmpOleAry['Programme'] = $OleInfoArr['Programme'][$i];
				$_tmpOleAry['Roles'] = $OleInfoArr['Roles'][$i];
				$_tmpOleAry['Awards'] = $OleInfoArr['Awards'][$i];
				$_tmpOleAry['Remarks'] = $OleInfoArr['Remarks'][$i];
				
				$oleTableHtmlAry[] = $this->Generate_OLE_Table_Html($ReportID, $_tmpOleAry);
			}
		}
		
		
		return $oleTableHtmlAry;
	}
	
	function Generate_OLE_Table_Html($reportId, $OleInfoArr) {
		global $eReportCard;
		
		$numOfOleInfo = count($OleInfoArr['Programme']);
			
		# if no record => first line show "xxx"
		if ($numOfOleInfo == 0)
		{
			$OleInfoArr['Programme'] = array('***');
//			$OleInfoArr['Roles/Awards'] = array('***');
			$OleInfoArr['Roles'] = array('***');
			$OleInfoArr['Awards'] = array('***');
			$OleInfoArr['Remarks'] = array('***');
			
			$numOfOleInfo = 1;
		}
		
		# maximum display 15 items
		$maxOleRow = 15;
		if ($numOfOleInfo > $maxOleRow) {
			$numOfOleInfo = $maxOleRow;
		}
			
		$programWidth = 38;
		$roleWidth = 15;
		//2012-0628-0948-00132
		//$awardWidth = 15;
		//$remarksWidth = 32;
		$awardWidth = 23;
		$remarksWidth = 24;
		
		$x = '';
		$x .= $this->Get_Session_Title_Table(6);
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" valign="top" class="report_border">'."\n";
			$x .= '<tr>';
				$x .= '<td width="'.$programWidth.'%" class="ms_footer_score" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['ProgrammesEn'].'</td>';
				$x .= '<td align="center" width="'.$roleWidth.'%" class="ms_footer_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Role'].'</td>';
				$x .= '<td align="center" width="'.$awardWidth.'%" class="ms_footer_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['Awards'].'</td>';
				$x .= '<td align="center" width="'.$remarksWidth.'%" class="ms_footer_score border_left" style="color:'.$this->ColorBlue.'">'.$eReportCard['Template']['RemarksEn'].'</td>';
			$x .= '</tr>';
			for ($i=0; $i<$numOfOleInfo; $i++)
			{
				$thisProgramme = $OleInfoArr['Programme'][$i];
				$thisRole = nl2br($OleInfoArr['Roles'][$i]);
				$thisAward = nl2br($OleInfoArr['Awards'][$i]);
				$thisRemarks = nl2br($OleInfoArr['Remarks'][$i]);
				
				$thisProgrammeAlign = '';
				if ($thisProgramme == '')
				{
					$thisProgramme = $this->EmptySymbol;
					$thisProgrammeAlign = 'align="center"';
				}
				
				if ($thisRole == '')
				{
					$thisRole = $this->EmptySymbol;
				}

				if ($thisAward == '')
				{
					$thisAward = $this->EmptySymbol;
				}
				
				if ($thisRemarks == '')
				{
					$thisRemarks = $this->EmptySymbol;
				}
				
				//2014-1014-1144-21164
				//$x .= '<tr height="40">';
				$x .= '<tr height="40" style="vertical-align:top;">';
					$x .= '<td '.$thisProgrammeAlign.' class="border_top ms_footer_score">'.$thisProgramme.'</td>';
					$x .= '<td class="border_top border_left ms_footer_score" align="center">'.$thisRole.'</td>';
					$x .= '<td class="border_top border_left ms_footer_score" align="center">'.$thisAward.'</td>';
					$x .= '<td class="border_top border_left ms_footer_score" align="center">'.$thisRemarks.'</td>';
				$x .= '</tr>';
			}
			
			/*
			$numOfEmptyRow = $maxOleRow - $numOfOleInfo;
			for ($i=0; $i<$numOfEmptyRow; $i++)
			{
				$x .= '<tr height="45">';
					$x .= '<td class="ms_footer_score border_top" style="color:'.$this->ColorBlue.'">&nbsp;</td>';
					$x .= '<td align="center" width="20%" class="ms_footer_score border_left border_top" style="color:'.$this->ColorBlue.'">&nbsp;</td>';
					$x .= '<td align="center" width="50%" class="ms_footer_score border_left border_top" style="color:'.$this->ColorBlue.'">&nbsp;</td>';
				$x .= '</tr>';
			}
			*/
		$x .= '</table>';
		
		return $x;
	}
	
	########### END Template Related
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Double_Line_Td($rowspan='', $has_border_top=0, $has_border_bottom=0)
	{
		global $PATH_WRT_ROOT;
		
		$rowspan_tag = '';
		if ($rowspan != '')
			$rowspan_tag = 'rowspan="'.$rowspan.'"';
			
		$border_top_css = '';
		if ($has_border_top)
			$border_top_css = 'border_top';
			
		$border_bottom_css = '';
		if ($has_border_bottom)
			$border_bottom_css = 'border_bottom';
			
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		$x = '';
		$x .= '<td '.$rowspan_tag.' width="1" class="double_border_td '.$border_top_css.' '.$border_bottom_css.'"><img src="'.$emptyImagePath.'" width="1"></td>'."\n";
		
		return $x;
	}
	
	function Get_Grading_Scheme_Info_Table($ReportID='')
	{
		$x = '';

		// [2020-0506-1549-59207]
		$display2019SpecialRemarks = false;
		if($ReportID)
        {
            $ReportSetting  = $this->returnReportTemplateBasicInfo($ReportID);
            $ClassLevelID   = $ReportSetting['ClassLevelID'];
            $FormNumber     = $this->GET_FORM_NUMBER($ClassLevelID);
            $SemID 			= $ReportSetting['Semester'];
            $ReportType 	= $SemID == "F" ? "W" : "T";
            $SemSeqNumber   = $this->Get_Semester_Seq_Number($SemID);

            $display2019SpecialRemarks = $this->schoolYear == '2019' && ($ReportType == 'W' || $SemSeqNumber == 2) && $FormNumber;
        }

		//2012-1003-1209-17072
//		$numOfCol = 12;
		$numOfCol = 13;
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" style="color:'.$this->ColorBlue.'">'."\n";
			$x .= '<tr class="grading_scheme_text">';
				$x .= '<td colspan="'.$numOfCol.'">&nbsp;&nbsp;Academic Performance</td>';
			$x .= '</tr>';
			
			//$x .= '<tr class="grading_scheme_text"><td colspan="'.$numOfCol.'">&nbsp;</td></tr>';
			
			//2012-1003-1209-17072
//			$x .= '<tr class="grading_scheme_text">';
//				$x .= '<td width="35">&nbsp;&nbsp;A</td>';
//				$x .= '<td width="85">Top 5%</td>';
//				$x .= '<td width="35">B</td>';
//				$x .= '<td width="85">Next 15%</td>';
//				$x .= '<td width="35">C</td>';
//				$x .= '<td width="85">Next 20%</td>';
//				$x .= '<td width="35">D</td>';
//				$x .= '<td width="85">Next 30%</td>';
//				$x .= '<td width="35">E</td>';
//				$x .= '<td width="85">Next 15% - 30%</td>';
//				$x .= '<td width="35">F</td>';
//				$x .= '<td width="85">Next 0% - 15%</td>';
//			$x .= '</tr>';
			
			//2014-1014-1144-21164
//			$x .= '<tr class="grading_scheme_text" style="padding-top:12px;">';
//				$x .= '<td width="75">&nbsp;&nbsp;F.1 - F.3</td>';
//				$x .= '<td width="30">A</td>';
//				$x .= '<td width="70">Top 10%</td>';
//				$x .= '<td width="30">B</td>';
//				$x .= '<td width="70">Next 15%</td>';
//				$x .= '<td width="30">C</td>';
//				$x .= '<td width="70">Next 20%</td>';
//				$x .= '<td width="30">D</td>';
//				$x .= '<td width="75">Next 25%</td>'; //2013-0703-0915-58167 width="85"
//				$x .= '<td width="30">E</td>';
//				$x .= '<td width="100">Next 15% - 30%</td>';
//				$x .= '<td width="30">F</td>';
//				$x .= '<td width="110">Next 0% - 15%</td>'; //2013-0703-0915-58167 width="100" 
//			$x .= '</tr>';
//			$x .= '<tr class="grading_scheme_text" style="padding-top:6px;">';
//				$x .= '<td width="75">&nbsp;&nbsp;F.4 - F.6</td>';
//				$x .= '<td width="30">A</td>';
//				$x .= '<td width="70">Top 5%</td>';
//				$x .= '<td width="30">B</td>';
//				$x .= '<td width="70">Next 15%</td>';
//				$x .= '<td width="30">C</td>';
//				$x .= '<td width="70">Next 20%</td>';
//				$x .= '<td width="30">D</td>';
//				$x .= '<td width="75">Next 30%</td>';  //2013-0703-0915-58167 width="85"
//				$x .= '<td width="30">E</td>';
//				$x .= '<td width="100">Next 15% - 30%</td>';
//				$x .= '<td width="30">F</td>';
//				$x .= '<td width="110">Next 0% - 15%</td>'; //2013-0703-0915-58167 width="100"
//			$x .= '</tr>';
			$x .= '<tr class="grading_scheme_text">';
				$x .= '<td width="20" style="padding-top:5px;">&nbsp;&nbsp;A</td>';
				$x .= '<td width="85" style="padding-top:5px;">Top 10%</td>';
				$x .= '<td width="25" style="padding-top:5px;">B</td>';
				$x .= '<td width="85" style="padding-top:5px;">Next 15%</td>';
				$x .= '<td width="25" style="padding-top:5px;">C</td>';
				$x .= '<td width="85" style="padding-top:5px;">Next 25%</td>';
				$x .= '<td width="25" style="padding-top:5px;">D</td>';
				$x .= '<td width="85" style="padding-top:5px;">Next 20%</td>';
				$x .= '<td width="25" style="padding-top:5px;">E</td>';
				$x .= '<td width="100" style="padding-top:5px;">Next 15% - 30%</td>';
				$x .= '<td width="25" style="padding-top:5px;">F</td>';
				$x .= '<td width="100" style="padding-top:5px;">Next 0% - 15%</td>';
			$x .= '</tr>';
			
			//2014-1014-1144-21164
			$x .= '<tr class="grading_scheme_text">';
				$x .= '<td style="padding-bottom:5px;" colspan="13">&nbsp;&nbsp;(Minor adjustment of the above percentages for subjects with less than 10 students)</td>';
			$x .= '</tr>';
			
			//$x .= '<tr class="grading_scheme_text"><td colspan="'.$numOfCol.'">&nbsp;</td></tr>';
			
			$x .= '<tr class="grading_scheme_text" style="padding-top:12px;">';
//				$x .= '<td>&nbsp;&nbsp;A-E</td>';
//				$x .= '<td>Pass</td>';
//				$x .= '<td>F</td>';
//				$x .= '<td>Fail</td>';
				
				//2014-1014-1144-21164
				//$x .= '<td colspan="5">&nbsp;&nbsp;'.'A-E'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Pass'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'F'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Fail'.'</td>';
				$x .= '<td colspan="5">&nbsp;&nbsp;'.'A-E'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Pass'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'F'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Fail'.'</td>';
				
				//2014-1014-1144-21164
				//$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>Paper Grade</td>';
				$x .= '<td colspan="4">Based on examination mark only</td>';
			$x .= '</tr>';
			
			//$x .= '<tr class="grading_scheme_text"><td colspan="'.$numOfCol.'">&nbsp;</td></tr>';
			
			$x .= '<tr class="grading_scheme_text" style="padding-top:12px;">';
				$x .= '<td colspan="2">&nbsp;&nbsp;Position</td>';
				//2014-1014-1144-21164
				//$x .= '<td colspan="6">Shown for top 75% students only</td>';
				$x .= '<td colspan="5">Shown for top 75% students only</td>';
				$x .= '<td>Subject Grade</td>';
				$x .= '<td colspan="4">Based on daily mark, test mark and examination mark</td>';
			$x .= '</tr>';

            // [2020-0506-1549-59207]
			if($display2019SpecialRemarks)
			{
                $specialRemarks = '';
                if($FormNumber > 3) {
                    $specialRemarks = 'Due to the COVID-19 and the school suspension, subjects including Music and PE are not assessed in the second term.';
                }
                else {
                    $specialRemarks = 'Due to the COVID-19 and the school suspension, subjects including Music, Visual Arts, PE, RME and D&T are not assessed in the second term.';
                }

                $x .= '<tr class="grading_scheme_text">';
                    $x .= '<td colspan="'.$numOfCol.'">&nbsp;&nbsp;'.$specialRemarks.'</td>';
                $x .= '</tr>';
            }

		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Session_Title_Table($SessionNumber,$isSecondTerm=0)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		switch ($SessionNumber)
		{
			case 1:
				$SessionRoma = 'I';
				break;
			case 2:
				$SessionRoma = 'II';
				break;
			case 3:
				$SessionRoma = 'III';
				break;
			case 4:
				$SessionRoma = 'IV';
				break;
			case 5:
				$SessionRoma = 'V';
				break;
			case 6:
				$SessionRoma = 'VI';
				break;
		}

		if($isSecondTerm)
			$Yearly = " (".$eReportCard['Template']['YearlyEn'].")";
		$SessionTitleTable = '';
		$SessionTitleTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='2'>";
			$SessionTitleTable .= "<tr>";
				$SessionTitleTable .= "<td class='session_title' style='color:".$this->ColorBlue."' width='30'>".$SessionRoma.". </td>";
				$SessionTitleTable .= "<td class='session_title' style='color:".$this->ColorBlue."'>".$eReportCard['Template']['SesseionTitle'][$SessionNumber].$Yearly."</td>";
			$SessionTitleTable .= "</tr>";
/* 			$SessionTitleTable .= "<tr>";
				$SessionTitleTable .= "<td><img src='".$emptyImagePath."' width='2'></td>";
			$SessionTitleTable .= "</tr>"; */ //2013-0703-0915-58167
		$SessionTitleTable .= "</table>";
		
		return $SessionTitleTable;
	}
	
	function Get_Position_Display($Position, $NumOfStudent, $PositionUpperLimit=0)
	{
		if ($Position == -1)
			return $this->EmptySymbol;
		
		if ($NumOfStudent > 0)
			$PositionPercentage = ($Position / $NumOfStudent) * 100;
			
		// 2011-0222-1002-18096 point 1
		//if ($PositionPercentage >= $this->PositionDisplayUpperPercentage)
		if ($PositionPercentage > $this->PositionDisplayUpperPercentage)
			return $this->EmptySymbol;
		else if ($PositionUpperLimit != 0 && $Position > $PositionUpperLimit)
			return $this->EmptySymbol;
		else
			return $Position.'/'.$NumOfStudent;
	}
	
	function Get_Page_Number_Table($PageNum)
	{
		$x = '';
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$x .= "<tr><td align='center' style='color:".$this->ColorBlue."'>- ".$PageNum." -</td></tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function Get_Personal_Quality_Result_Cell($ReportID, $StudentID, $CsvInfoArr, $CsvInfoType, $has_border_top=0, $has_border_bottom=0)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
 		$SemArr = array();
		if ($ReportType == 'W')
		{
			$ColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnInfoArr);
			
			for ($i=0; $i<$numOfColumn; $i++)
				$SemArr[] = $ColumnInfoArr[$i]['SemesterNum'];
		}
		else
		{
			$numOfColumn = 1;
			$SemArr[] = $SemID;
		}
		
		$border_top_css = '';
		if ($has_border_top)
			$border_top_css = 'border_top';
		
		$border_bottom_css = '';
		if ($has_border_bottom)
			$border_bottom_css = 'border_bottom';
		
		$x = '';
		for ($i=0; $i<$numOfColumn; $i++)
		{
			$border_right_css = ($ReportType == 'W')? '' : 'border_right';
			
			if ($ReportType == 'W' && $i == 1)
				$border_right_css = 'border_right';
				
			$border_left_css = 'border_left';
			if ($i == 0)
				$border_left_css = '';
			
			$thisSemID = $SemArr[$i];
			$x .= '<td class="subject_score '.$border_bottom_css.' '.$border_top_css.' '.$border_left_css.' '.$border_right_css.'" align="center">';
				if ($StudentID != '')
					$thisDisplay = $CsvInfoArr[$thisSemID][$CsvInfoType];
				else
					$thisDisplay = 'xxx';
					
				$thisDisplay = ($thisDisplay == '')? $this->EmptySymbol : $thisDisplay;
				$x .= $thisDisplay;
			$x .= '</td>';
		}
		
		return $x;
	}
}
?>