<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "merit", "promotion");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
	
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID="", $StudentID="", $SubjectID="", $SubjectSectionOnly="")
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		if($ReportID) {
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$isYearReport = $ReportSetting["Semester"] == "F";
			$ClassLevelID = $ReportSetting["ClassLevelID"];
			$FormNumber	= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		}
		
		# Define Height
		$main_table_height = "745px";
		$misc_table_height = "204px";
		$bottom_table_height = "";
		if($isYearReport) {
			$main_table_height = "826px";
			$misc_table_height = "";
			$bottom_table_height = "204px";
		}
		
		# Misc Table Display
		$misc_in_footer = $MiscTable[0];
		if($isYearReport) {
			$misc_in_main_table = $MiscTable[1];
		}

		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		if($misc_in_main_table) {
			$TableTop .= "<tr><td>".$misc_in_main_table."</td></tr>";
		}
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' height='".$bottom_table_height."' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom' height='".$misc_table_height."'><td>".$misc_in_footer."</td></tr>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='".$main_table_height."' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		
		$TitleTable = "";
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SemID = $ReportSetting["Semester"];
			
			# Report Title
			$ReportType = $SemID == "F" ? "YearReport" : "TermReport";
			$ReportTitleEn = $eReportCard["Template"][$ReportType."TitleEn"];
			$ReportTitleCh = $eReportCard["Template"][$ReportType."TitleCh"];
			$ReportYearName = $this->GET_ACTIVE_YEAR_NAME("en");
			$ReportYearName = str_replace("-", " - ", $ReportYearName);
			
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' style='padding-top:55px;'>\n";
			$TitleTable .= "<tr>";
				$TitleTable .= "<td width='150' align='center'>&nbsp;</td>";
				$TitleTable .= "<td>";
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitleEn."</td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitleCh."</td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportYearName."</td></tr>\n";
					$TitleTable .= "</table>\n";
				$TitleTable .= "</td>";
				$TitleTable .= "<td width='150' align='center'>&nbsp;</td>";
			$TitleTable .= "</tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID="")
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoFieldArray = $eRCTemplateSetting["StudentInfo"]["Selection"];
			$StudentInfoTableCol   = $eRCTemplateSetting["ColumnWidth"]["StudentInfo"];
			$StudentInfoTableColSize = count($StudentInfoTableCol);
			
			# Define Default Value
			$defaultVal = $StudentID == "" ? "XXX" : "";

			# Retrieve Student Info
			if($StudentID)
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");				
				$lu = new libuser($StudentID);
				$data["Name"] = $lu->UserName2Lang();
				$thisAdmissionNum = $lu->WebSamsRegNo;
				$data["AdmissionNum"] = str_replace("#", "", $thisAdmissionNum);
				$thisDateOfBirth = $lu->DateOfBirth;
				$data["DateOfBirth"] = !is_date_empty($thisDateOfBirth) ? date("d/m/Y", strtotime($thisDateOfBirth)) : "";
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]["ClassName"];
				$thisClassNumber = $StudentInfoArr[0]["ClassNumber"];
				$data["Class"] = $thisClassName;
				$data["ClassNum"] = $thisClassNumber;

				if($StudentID == 5096) {
                    $data["Name"] = "<span style='font-size: 12px'>".$data["Name"]."</span>";
                }
			}
			
			# Student Info Table
			$fieldCount = 0;
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='student_info'>";
			for($i=0; $i<sizeof($StudentInfoFieldArray); $i++)
			{
				$SettingID = trim($StudentInfoFieldArray[$i]);
				if($SettingID != "")
				{
					$fieldLocation = ($fieldCount % $StudentInfoTableColSize);
					if($fieldLocation == 0) {
						$StudentInfoTable .= "<tr>";
					}
					
					# Student Info Data
					$Title = $eReportCard["Template"]["StudentInfo"][$SettingID];
					$TitleData = $data[$SettingID];
					$TitleData = $TitleData ? $TitleData : $defaultVal;
					
					# Define Info Style
					$colwidth = $StudentInfoTableCol[$fieldLocation];
					$colspan = $SettingID == "Name" ? " colspan='2' " : "";
					
					$StudentInfoTable .= "<td class='tabletext' width='".$colwidth."' ".$colspan." valign='top'>";
						$StudentInfoTable .= $Title." : ".$TitleData; 
					$StudentInfoTable .= "</td>";
						
					if((($fieldCount + 1) % $StudentInfoTableColSize) == 0) {
						$StudentInfoTable .= "</tr>";
					}
				}
				$fieldCount++;
			}
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID="")
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
		# Retrieve Column Header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# Retrieve Subject Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0) {
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0) {
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		}
		
		# Retrieve Subject Column
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeOfSubjectCol = sizeof($SubjectCol);
		
		# Retrieve Subject Marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, "", 0, 1);
		
		# Retrieve Mark Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned["HTML"];
		$isAllNAAry = $MSTableReturned["isAllNA"];
		
		# Retrieve Subject Teacher's Comment
//		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		
		# Define Table Style
		$main_table_class = $isYearReport ? "info_table2" : "info_table";
		$table_padding_top = $isSeniorLevel && !$isYearReport ? " style='padding-top: 16px;' " : " style='padding-top: 12px;' ";
		
		$DetailsTable = "<table width='90%' border='0' cellspacing='0' cellpadding='2' align='center' class='".$main_table_class."' ".$table_padding_top.">";
		
		# Table Header Column
		$DetailsTable .= $ColHeader;
		
		# Table Content
		$isFirst = 1;
		for($i=0; $i<$sizeOfSubjectCol; $i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
//			# if all marks is "*", then don't display
//			if (sizeof((array)$MarksAry[$thisSubjectID]) > 0) 
//			{
//				$Droped = 1;
//				foreach((array)$MarksAry[$thisSubjectID] as $cid => $da) {
//					if($da["Grade"] != "*")	$Droped = 0;
//				}
//			}
//			if($Droped) continue;
			
			# if any marks is "*", then don't display
			//$isExempted = true;
			$isExempted = false;
			if(sizeof((array)$MarksAry[$thisSubjectID]) > 0)
			{
				foreach((array)$MarksAry[$thisSubjectID] as $cid => $da) {
					if($da["Grade"] == "*") {
						$isExempted = true;
						break;
					}
				}
			}
			if($isExempted)	continue;
			
			if(in_array($thisSubjectID, (array)$MainSubjectIDArray) != true)	
				$isSub = 1;
			if ($eRCTemplateSetting["HideComponentSubject"] && $isSub)
				continue;
			
			# Check if display subject row with all marks equal "N.A."
			if ($eRCTemplateSetting["DisplayNA"] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
					$DetailsTable .= $SubjectCol[$i];
					$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
		# Define Table Style
		$displayInfoTableType = $isYearReport ? "InfoTable2" : "InfoTable";
		$StudentInfoTableCol = $eRCTemplateSetting["ColumnWidth"][$displayInfoTableType];
		$defaultMarkColWidth = $StudentInfoTableCol[3];
		
		# Define CSS Style
		$css_border_left = $isYearReport ? "border_left" : "";
		$css_border_right = $isYearReport ? "border_right" : "";
		$css_border_top = $isYearReport ? "border_top" : "";
		
		$n = 0;
		$e = 0;			# Column within Term / Assesment
		$t = array(); 	# Number of Assessments of each Term (for consolidated report only)
		
		#########################################################
		###########		Mark Columns [Start]
		$row2 = "";
		if($isYearReport)			# Whole Year Report
		{
			// Get Term Mark Data
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$ColumnSize = sizeof($ColumnData);
			
			// Define Column Size
			$defaultColSize = 2;
			if($ColumnSize > 1) {
				$defaultColSize += $ColumnSize;
			}
			$defaultMarkColWidth = $defaultMarkColWidth / $defaultColSize;
			
			# Term Mark Columns
			if($ColumnSize > 1 && $FormNumber != 6)
			{
				for($i=0; $i<$ColumnSize; $i++) {
					$SemNum = $ColumnData[$i]["SemesterNum"];
					$SemNum = $this->Get_Semester_Seq_Number($SemNum);
					$SemNum = "Term".$SemNum;
					$TermColEng = $eReportCard["Template"][$SemNum."En"];
					$TermColCh  = $eReportCard["Template"][$SemNum."Ch"];
					$TermColTitle = $TermColEng."<br/>".$TermColCh;
					$row2 .= "<td class='border_left border_bottom small_title' align='center' width='".$defaultMarkColWidth."%'>".$TermColTitle."</td>";
					$e++;
				}
			}
			
			# Yearly Mark Column
			$YearlyColEng = $eReportCard["Template"]["YearlyEn"];
			$YearlyColCh  = $eReportCard["Template"]["YearlyCh"];
			$YearlyColTitle = $YearlyColEng."<br/>".$YearlyColCh;
			$row2 .= "<td class='border_left border_bottom small_title' align='center' width='".$defaultMarkColWidth."%'>".$YearlyColTitle."</td>";
		}
		else 						# Term Report
		{
			$defaultColSize = 2;
			if($isJuniorLevel) {
				$defaultColSize++;
			}
			$defaultMarkColWidth = $defaultMarkColWidth / $defaultColSize;
			$StudentInfoTableCol[2] = $defaultMarkColWidth;
		}
		
		# Define span Settings
		$row_span = $row2 ? "rowspan='2'" : "";
		$col_span = $row2 ? "colspan='".($e + 1)."'" : "";
		###########		Mark Columns [End]
		#########################################################
		
		$x = "<tr>";
		
		# Subject Column
		$SubjectColEng = $eReportCard["Template"]["SubjectEn"];
		$SubjectColCh  = $eReportCard["Template"]["SubjectCh"];
		$SubjectColWidthCh = $StudentInfoTableCol[1];
		$SubjectColWidthCh += $isSeniorLevel && $isYearReport ? $StudentInfoTableCol[2] : 0;
		$x .= "<td valign='bottom' class='{$css_border_left} {$css_border_top} border_bottom small_title' width='".$StudentInfoTableCol[0]."' ".$row_span.">".$SubjectColEng."</td>";
		$x .= "<td valign='bottom' class='{$css_border_top} border_bottom small_title' width='".$SubjectColWidthCh."%' ".$row_span.">".$SubjectColCh."</td>";
		$n++;
		$n++;
		
		# Mark Column
		$MarkColEng = $eReportCard["Template"]["MarkEn"];
		$MarkColCh  = $eReportCard["Template"]["MarkCh"];
		$MarkPercent = $eReportCard["Template"]["MarkPercentage"];
		if($isJuniorLevel && !$isYearReport) {
			$MarkColTitle = $MarkColEng." ".$MarkPercent."<br/>　　".$MarkColCh;
			$column_align = "";
		}
		else {
			$seperator = $isYearReport ? " 　 " : " ";
			if($isYearReport && $FormNumber == 6) {
				$seperator = "　";
			}
			$MarkColTitle = $MarkColEng . $seperator . $MarkColCh . $seperator . $MarkPercent;
			$column_align = " align='center' ";
		}
		$row1 .= "<td valign='bottom' class='{$css_border_left} {$css_border_top} border_bottom small_title' width='".$defaultMarkColWidth."%' ".$column_align." ".$col_span.">".$MarkColTitle."</td>";
		$n++;
		
		# Position Column
		if($isYearReport) {
			$PositionColEng = $eReportCard["Template"]["YearPositionEn"];
			$PositionColCh  = $eReportCard["Template"]["YearPositionCh"];
		}
		else {
			$PositionColEng = $eReportCard["Template"]["PositionEn"];
			$PositionColCh  = $eReportCard["Template"]["PositionCh"];
		}
		if($isSeniorLevel && !$isYearReport) {
			$PositionColTitle = $PositionColEng." ".$PositionColCh;
		}
		else {
			$PositionColTitle = $PositionColEng."<br/>".$PositionColCh;
		}
		$row1 .= "<td valign='bottom' class='{$css_border_left} {$css_border_right} {$css_border_top} border_bottom small_title' width='".$defaultMarkColWidth."%' align='center' ".$row_span.">".$PositionColTitle."</td>";
		$n++;
		
		# Weighting Column
		$WeightCol_td = "";
		if($isJuniorLevel) {
			$WeightColEng = $eReportCard["Template"]["WeightingEn"];
			$WeightColCh  = $eReportCard["Template"]["WeightingCh"];
			$WeightColTitle = $WeightColEng."<br/>".$WeightColCh;
			$StudentInfoTableCol[2] += 0;
			$WeightCol_td = "<td {$row_span} valign='bottom' class='{$css_border_left} {$css_border_top} border_bottom small_title' width='".$StudentInfoTableCol[2]."%' align='center'>".$WeightColTitle."</td>";
			$n++;
		}
		
		if($isYearReport) {
			$x .= $WeightCol_td . $row1;
		}
		else {
			$x .= $row1 . $WeightCol_td;
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
 		$x = array();
		$isFirst = 1;
		
		# Report Subjects
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
		{
	 		foreach($SubjectArray as $SubjectID => $SubSubjectArray) {
		 		foreach($SubSubjectArray as $SubSubjectID => $SubSubject)
		 		{
			 		# Subject Display Style
			 		$PrefixEng = "&nbsp;&nbsp;&nbsp;";
			 		$PrefixCh  = "　";
			 		if($SubSubjectID == 0) {
				 		$SubSubjectID = $SubjectID;
				 		$PrefixEng = "&nbsp;";
				 		$PrefixCh  = "";
			 		}
			 		
					# Define CSS Style
			 		$css_border_left = $isYearReport ? "border_left" : "";
			 		
			 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 			$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 			
			 		$t = "";
					$t .= "<td class='tabletext {$css_border_left}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>".$PrefixEng."</td><td class='tabletext report_left_col'>".$SubjectEng."</td></tr>";
						$t .= "</table>";
					$t .= "</td>";
		 			$t .= "<td class='tabletext {$css_border_top} report_left_col report_left_col2' valign='middle'>".$PrefixCh.$SubjectChn."</td>";
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID="", $StudentID="")
	{
 		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
 		
		# Retrieve Display Settings
 		if($ReportID) {
			$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
			$SemID 			= $ReportSetting["Semester"];
			$isYearReport 	= $SemID == "F";
			$ClassLevelID 	= $ReportSetting["ClassLevelID"];
			$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
			$isJuniorLevel	= $FormNumber < 4;
			$isSeniorLevel  = !$isJuniorLevel;
			$ReportIssueDate = $ReportSetting["Issued"];
			$ReportIssueDate = is_date_empty($ReportIssueDate) ? "&nbsp;" : date("d/m/Y", strtotime($ReportIssueDate));
			$SignatureTitleArray = $eRCTemplateSetting["Signature"];
			$SignatureTitleSize  = count($SignatureTitleArray);
		}
 		
		// Get Student Class Teacher
		$ClassTeacher = "";
 		$ClassTeacherCount = 0;
		if($StudentID)
		{
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassName = $StudentInfoArr[0]["ClassName"];
			
			include_once($PATH_WRT_ROOT."includes/libclass.php");
			$lclass = new libclass();
			$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
            $ClassTeacherAry = sortByColumn($ClassTeacherAry, 'EnglishName');
			foreach((array)$ClassTeacherAry as $val)
			{
			    $CTeacherGender = $lclass->getGenderByStudentID($val["UserID"]);
			    $CTeacherGender = $CTeacherGender[0];
			    if($CTeacherGender == 'M') {
			        $CTeacherTitle = 'Mr.';
			    }
			    else {
			        $CTeacherTitle = 'Ms.';
			    }
			    // $CTeacherDisplay = $CTeacherTitle.' '.$val["CTeacherEn"];
                $CTeacherDisplay = $CTeacherTitle.' '.$val["EnglishName"];
			    
			    $CTeacher[] = $CTeacherDisplay;
				continue;
			}
            $ClassTeacherCount = count($ClassTeacherAry);

			// $ClassTeacher = !empty($CTeacher) ? implode(", ", (array)$CTeacher) : "";
            $ClassTeacher = !empty($CTeacher) ? implode("<br/>", (array)$CTeacher) : "";
		}

		if($ClassTeacherCount == 0 || $ClassTeacherCount == 1)
		{
            $SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='signature_table'>";

            for($k=0; $k<$SignatureTitleSize; $k++)
            {
                $SignatureCol1ID = trim($SignatureTitleArray[$k]);
                $SignatureTitleCol1En = $eReportCard["Template"][$SignatureCol1ID."En"];
                $SignatureTitleCol1Ch = $eReportCard["Template"][$SignatureCol1ID."Ch"];
                $SignatureTitleCol1Ch = $SignatureCol1ID == "FormTeacher" ? "　 ".$SignatureTitleCol1Ch : $SignatureTitleCol1Ch;
                // $SignatureTitleCol1Value = $SignatureCol1ID == "FormTeacher" ? "　　".$ClassTeacher : "&nbsp;";
                $SignatureTitleCol1Value = $SignatureCol1ID == "FormTeacher" ? $ClassTeacher : "&nbsp;";
                $SignatureTitleCol1Style = $SignatureCol1ID == "FormTeacher" ? " align='center' " : "";

                $SignatureCol2ID = trim($SignatureTitleArray[($k + 1)]);
                $SignatureTitleCol2En = $eReportCard["Template"][$SignatureCol2ID."En"];
                $SignatureTitleCol2Ch = $eReportCard["Template"][$SignatureCol2ID."Ch"];
                $SignatureTitleCol2Value1 = $SignatureCol2ID == "Date" ? $ReportIssueDate : "____________________";
                $SignatureTitleCol2Value2 = $SignatureCol2ID == "Principal" ? "　　".$eReportCard["Template"]["PrincipalName"] : "&nbsp;";
                $SignatureTitleCol2Style  = $SignatureCol2ID == "Principal" ? " align='center' " : "";

                $SignatureTable .= "<tr>";
                    $SignatureTable .= "<td valign='bottom' width='20%' class='signatre_title'>".$SignatureTitleCol1En."</td>";
                    $SignatureTable .= "<td valign='bottom' width='32%'>： ___________________</td>";
                    $SignatureTable .= "<td valign='bottom' width='12%' class='signatre_title'>".$SignatureTitleCol2En."</td>";
                    $SignatureTable .= "<td valign='bottom' width='34%'>： ".$SignatureTitleCol2Value1."</td>";
                    $SignatureTable .= "<td width='1' style='padding-left:0px; padding-right:0px; padding-bottom: 13px; font-size: 12px;'>&nbsp;</td>";
                $SignatureTable .= "</tr>";

                $SignatureTable .= "<tr>";
                    $SignatureTable .= "<td valign='top' class='signatre_title'>".$SignatureTitleCol1Ch."</td>";
                    $SignatureTable .= "<td valign='top' ".$SignatureTitleCol1Style.">".$SignatureTitleCol1Value."</td>";
                    $SignatureTable .= "<td valign='top' class='signatre_title' ".$SignatureTitleCol2Style.">".$SignatureTitleCol2Ch."</td>";
                    $SignatureTable .= "<td valign='top'>".$SignatureTitleCol2Value2."</td>";
                $SignatureTable .= "</tr>";

                $k++;
            }
        }
        else if($ClassTeacherCount >= 2)
        {
            $SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='signature_table_2'>";

            for($k=0; $k<$SignatureTitleSize; $k++)
            {
                $SignatureCol1ID = trim($SignatureTitleArray[$k]);
                $SignatureTitleCol1En = $eReportCard["Template"][$SignatureCol1ID."En"];
                $SignatureTitleCol1Ch = $eReportCard["Template"][$SignatureCol1ID."Ch"];
                $SignatureTitleCol1Ch = $SignatureCol1ID == "FormTeacher" ? "　 ".$SignatureTitleCol1Ch : $SignatureTitleCol1Ch;
                $SignatureTitleCol1Value = $SignatureCol1ID == "FormTeacher" ? $CTeacher : "&nbsp;";
                $SignatureTitleCol1Style = $SignatureCol1ID == "FormTeacher" ? " align='center' " : "";

                $SignatureCol2ID = trim($SignatureTitleArray[($k + 1)]);
                $SignatureTitleCol2En = $eReportCard["Template"][$SignatureCol2ID."En"];
                $SignatureTitleCol2Ch = $eReportCard["Template"][$SignatureCol2ID."Ch"];
                $SignatureTitleCol2Value1 = $SignatureCol2ID == "Date" ? $ReportIssueDate : "_____________________";
                $SignatureTitleCol2Value2 = $SignatureCol2ID == "Principal" ? "　".$eReportCard["Template"]["PrincipalName"] : "&nbsp;";
                $SignatureTitleCol2Style  = $SignatureCol2ID == "Principal" ? " align='center' " : "";

                $SignatureTable .= "<tr>";
                    $SignatureTable .= "<td valign='bottom' width='17%' class='signatre_title'>".$SignatureTitleCol1En."</td>";
                    if($k == 0) {
                        $SignatureTable .= "<td valign='bottom' width='23%'>：___________________</td>";
                        $SignatureTable .= "<td valign='bottom' width='22%'>____________________</td>";
                    }
                    else {
                        $SignatureTable .= "<td valign='bottom' width='45%' colspan='2'>：________________________________________</td>";
                    }
                    $SignatureTable .= "<td valign='bottom' width='10%' class='signatre_title'>".$SignatureTitleCol2En."</td>";
                    $SignatureTable .= "<td valign='bottom' width='25%'>：".$SignatureTitleCol2Value1."</td>";
                    $SignatureTable .= "<td width='1%' style='padding-left:0px; padding-right:0px; padding-bottom: 13px; font-size: 12px;'>&nbsp;</td>";
                $SignatureTable .= "</tr>";

                $SignatureTable .= "<tr>";
                    $SignatureTable .= "<td valign='top' class='signatre_title'>".$SignatureTitleCol1Ch."</td>";
                    if($k == 0) {
                        $classTeacherNameCol1 = $SignatureTitleCol1Value[0];
                        $classTeacherNameCol2 = $SignatureTitleCol1Value[1];

                        $SignatureTable .= "<td valign='top' ".$SignatureTitleCol1Style.">　".$classTeacherNameCol1."</td>";
                        $SignatureTable .= "<td valign='top' ".$SignatureTitleCol1Style.">".$classTeacherNameCol2."</td>";
                    }
                    else {
                        $SignatureTable .= "<td valign='top' ".$SignatureTitleCol1Style." colspan='2'>".$SignatureTitleCol1Value."</td>";
                    }
                    $SignatureTable .= "<td valign='top' class='signatre_title' ".$SignatureTitleCol2Style.">".$SignatureTitleCol2Ch."</td>";
                    $SignatureTable .= "<td valign='top' ".$SignatureTitleCol2Style.">".$SignatureTitleCol2Value2."</td>";
                    $SignatureTable .= "<td style='padding-left:0px; padding-right:0px; padding-bottom: 4px; font-size: 12px;'>&nbsp;</td>";
                $SignatureTable .= "</tr>";

                $k++;
            }
        }
		
		$SignatureTable .= "</table>";

		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
		# Retrieve Grand Result Data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, "", 0, 1);
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result["GrandAverage"], $ReportID, $ClassLevelID, "", "", "GrandAverage") : "S";
  		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result["OrderMeritClass"], $ReportID, $ClassLevelID, "", "", "ClassPosition") : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result["ClassNoOfStudent"], $ReportID, $ClassLevelID, "", "", "ClassNoOfStudent") : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result["OrderMeritForm"], $ReportID, $ClassLevelID, "", "", "FormPosition") : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result["FormNoOfStudent"], $ReportID, $ClassLevelID, "", "", "FormNoOfStudent") : "#";
  		
  		# Retrieve Term Grand Result Data
  		if ($isYearReport)
  		{
			$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName)
			{
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, "", 1, 1);
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult["GrandAverage"], $ReportID, $ClassLevelID, "", "", "GrandAverage") : "S";
				$columnFormPos[$ColumnID] = "";
			}
			$ColumnSize = sizeof($ColumnTitle);
		}
		
		$first = 1;
		// $ShowGrandAvg = true;
        //$ShowOverallPositionForm = $isJuniorLevel;
        $ShowGrandAvg = $isJuniorLevel;
		$ShowOverallPositionForm = false;
		
		# Average Mark & Position
		if($ShowGrandAvg)
		{
			$thisTitleEn = $eReportCard["Template"]["AverageEn"];
			$thisTitleCh = $eReportCard["Template"]["AverageCh"];

			# Position in Class (Junior Level)
			if($ShowOverallPositionForm)
            {
                $ClassPositionDisplay = $StudentID ? "&nbsp;" : $ClassPosition;
                if($StudentID && $ClassPosition > 0 && $ClassPosition != $this->EmptySymbol) {
                    $ClassPositionDisplay = $ClassPosition."/".$ClassNumOfStudent;
                }
                $PositionDisplay = $ClassPositionDisplay;
            }
            # Position in Form (Senior Level)
            else
            {
                $FormPositionDisplay = $StudentID ? "&nbsp;" : $FormPosition;
                if($StudentID && $FormPosition > 0 && $FormPosition != $this->EmptySymbol) {
                    $FormPositionDisplay = $FormPosition."/".$FormNumOfStudent;
                }
                $PositionDisplay = $FormPositionDisplay;
            }

			// $x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $ClassPositionDisplay, $first);
            $x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $PositionDisplay, $first);
			
			$first = 0;
		}
		
		# Position in Form (Junior Level)
		if($ShowOverallPositionForm)
		{
			$thisTitleEn = $eReportCard["Template"]["PositionInFormEn"];
			$thisTitleCh = $eReportCard["Template"]["PositionInFormCh"];
			$FormPositionDisplay = $StudentID ? "&nbsp;" : $FormPosition;
			//if($StudentID && $FormPosition > 0 && $FormPosition <= 50 && $FormPosition != $this->EmptySymbol) {
			if($StudentID && $FormPosition > 0 && $FormPosition != $this->EmptySymbol) {
				$FormPositionDisplay = $FormPosition."/".$FormNumOfStudent;
			}
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, "", $FormPositionDisplay, $first);
			
			$first = 0;
		}
		
		# Mark Remarks
		$col_span = 4;
		$col_span += $isJuniorLevel ? 1 : 0;
		$col_span += $isYearReport && $FormNumber != 6 && $ColumnSize > 0 ? $ColumnSize : 0;
		$remark_font_size = $isYearReport ? " style='font-size:11px;'" : " style='font-size:13px;'";
		$x .= "<tr>";
			$x .= "<td class='tabletext border_top info_table_footer footer_remark' align='right' colspan='".$col_span."' ".$remark_font_size.">( ".$eReportCard["Template"]["FailedRemark"]." ) </td>";
		$x .= "</tr>";
		
		##################################################################################
		# CSV Related
		##################################################################################
		
		# Year Report
		if($isYearReport)
		{
			# Define CSS Style
			$border_top = $first ? "border_top" : "";
			
			# Build Data Array
			$OtherInfoDataAry = array();
			if ($StudentID != "")
			{
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
				$OtherInfoDataAry = $OtherInfoDataAry[$StudentID];
				
				$allTermReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
				foreach($allTermReport as $thisTermReport)
				{
					$thisTermReportID = $thisTermReport["ReportID"];
					$thisTermID = $thisTermReport["Semester"];
					
					$TermAttendanceData = $this->Get_Student_Profile_Data($thisTermReportID, $StudentID, false, false, false, "", true);
					$TermAttendanceData = $TermAttendanceData["AttendanceAry"][$StudentID];
					if($FormNumber == 6)
                    {
                        $OtherInfoDataAry[$thisTermID]["Conduct"] = $OtherInfoDataAry[0]["Conduct"];
                        $OtherInfoDataAry[$thisTermID]["Times Late"] = isset($OtherInfoDataAry[0]["Times Late"]) ? $OtherInfoDataAry[0]["Times Late"] : $TermAttendanceData["Time Late"];
                        $OtherInfoDataAry[$thisTermID]["Days Absent"] = isset($OtherInfoDataAry[0]["Days Absent"]) ? $OtherInfoDataAry[0]["Days Absent"] : $TermAttendanceData["Days Absent"];
                        // $OtherInfoDataAry[$thisTermID]["Days Absent"] = my_round($OtherInfoDataAry[$thisTermID]["Days Absent"], 2);
                        $OtherInfoDataAry[$thisTermID]["Days Absent"] = my_round($OtherInfoDataAry[$thisTermID]["Days Absent"], 1);
                    }
                    else
                    {
                        $OtherInfoDataAry[$thisTermID]["Times Late"] = isset($OtherInfoDataAry[$thisTermID]["Times Late"]) ? $OtherInfoDataAry[$thisTermID]["Times Late"] : $TermAttendanceData["Time Late"];
                        $OtherInfoDataAry[$thisTermID]["Days Absent"] = isset($OtherInfoDataAry[$thisTermID]["Days Absent"]) ? $OtherInfoDataAry[$thisTermID]["Days Absent"] : $TermAttendanceData["Days Absent"];
                        // $OtherInfoDataAry[$thisTermID]["Days Absent"] = my_round($OtherInfoDataAry[$thisTermID]["Days Absent"], 2);
                        $OtherInfoDataAry[$thisTermID]["Days Absent"] = my_round($OtherInfoDataAry[$thisTermID]["Days Absent"], 1);
                    }
				}
			}

			# Conduct 
			$thisTitleEn = $eReportCard["Template"]["ConductEn"];
			$thisTitleCh = $eReportCard["Template"]["ConductCh"];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $OtherInfoDataAry, true);
			
			# Times Late 
			$thisTitleEn = $eReportCard["Template"]["TimesLateEn"];
			$thisTitleCh = $eReportCard["Template"]["TimesLateCh"];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Times Late", $OtherInfoDataAry);
			
			# Days Absent 
			$thisTitleEn = $eReportCard["Template"]["DaysAbsentEn"];
			$thisTitleCh = $eReportCard["Template"]["DaysAbsentCh"];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $OtherInfoDataAry, false, true);
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID="", $NumOfAssessment=array())
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting  = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
 		$ReportType 	= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting["UseWeightedMark"];
		
		# Retrieve Storage Settings
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		# Report Subjects
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		
		# Report Subject Weight
		$SubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, " ReportColumnID IS NULL ");
		$SubjectWeightData = BuildMultiKeyAssoc($SubjectWeightData, array("SubjectID"), "Weight");
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		
		if($ReportType=="T")		# Term Report
		{
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray) != true) $isSub = 1;
				
				// if parent subject, if yes find its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when Scale Input of component subjects is Mark (M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Subject Scheme Settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0 , 0, $ReportID);
				$ScaleDisplay = $SubjectFormGradingSettings["ScaleDisplay"];
				
				# Subject Weight
				$thisSubjectWeight = $SubjectWeightData[$SubjectID]["Weight"];
				
				# Subject Mark
				$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
				$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="" ) ? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
				
				# for Preview Purpose
				if(!$StudentID) {
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				# for N.A. Checking
				$isAllNA = true;
				if ($thisMark != "N.A.") {
					$isAllNA = false;
				}
				
				# Subject Mark Display
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $thisMSGrade);
				if($needStyle) {
					if($thisSubjectWeight > 0 && $ScaleDisplay == "M") {
						$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark / $thisSubjectWeight : $thisMark;
					}
					else {
						$thisMarkTemp = $thisMark;
					}
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
				}
				else {
					$thisMarkDisplay = $thisMark;
				}
				
				# Subject Form Ordering
				// $thisClassOrdering = $MarksAry[$SubjectID][0]["OrderMeritClass"];
				// $thisClassTotalCount = $MarksAry[$SubjectID][0]["ClassNoOfStudent"];
                $thisFormOrdering = $MarksAry[$SubjectID][0]["OrderMeritForm"];
                $thisFormTotalCount = $MarksAry[$SubjectID][0]["FormNoOfStudent"];
				
				# for Preview Purpose
				$thisFormOrderingDisplay = "&nbsp;";
				if(!$StudentID && !$isSub) {
					$thisFormOrderingDisplay = "#";
				}
				
				# Subject Form Ordering Display
				if($StudentID && !$isSub && $thisFormOrdering > 0) {
					$thisFormOrderingDisplay = $thisFormOrdering."/".$thisFormTotalCount;
				}
				
				# Subject Weight Display
				$thisWeightDisplay = "&nbsp;";
				if(!$isSub) {
					$thisWeightDisplay = $SubjectWeightData[$SubjectID]["Weight"];
				}
				
				$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>".$thisMarkDisplay."</td>";
				$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>".$thisFormOrderingDisplay."</td>";
				if($isJuniorLevel) {
					$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>". $thisWeightDisplay ."</td>";
				}
				$isFirst = 0;
				
				# Construct an array to return
				$returnArr["HTML"][$SubjectID] = $x[$SubjectID];
				$returnArr["isAllNA"][$SubjectID] = $isAllNA;
			}
		}
		else						# Whole Year Report
		{
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray) != true) $isSub = 1;
				
				// if parent subject, if yes find its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# Subject Scheme Settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$ScaleDisplay = $SubjectFormGradingSettings["ScaleDisplay"];
				
				# Subject Weight
				if($isJuniorLevel)
				{
					$thisWeightDisplay = "&nbsp;";
					if(!$isSub) {
						$thisWeightDisplay = $SubjectWeightData[$SubjectID]["Weight"];
					}
					$x[$SubjectID] .= "<td class='border_left  tabletext' align='center'>".$thisWeightDisplay."</td>";
				}
				
				# Subject Term Mark
				$isAllNA = true;
				if($FormNumber != 6)
				{
					for($i=0; $i<sizeof($ColumnData); $i++)
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("", " Semester = '".$ColumnData[$i]["SemesterNum"]."' AND ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, term mark should be entered directly
						if (empty($thisReport))
						{
							$thisReportID = $ReportID;
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
							
							$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
							$thisMSMark = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="") ? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
						}
						else
						{
							$thisReportID = $thisReport["ReportID"];
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
							
							$thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
							$thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="") ? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
						}
						
						# for Preview Purpose
						if(!$StudentID) {
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
						
						# for N.A. Checking
						if ($thisMark != "N.A.") {
							$isAllNA = false;
						}
						
						# Subject Mark Display
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
						if($needStyle) {
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else {
							$thisMarkDisplay = $thisMark;
						}
						$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>".$thisMarkDisplay."</td>";
					}
				}
				
				# Subject Mark Display
				$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
				$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!="") ? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=="") ? $thisMSMark : "";
				
				# for Preview Purpose
				if(!$StudentID) {
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				# for N.A. Checking
				if ($thisMark != "N.A.") {
					$isAllNA = false;
				}
				
				# Subject Mark Display
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $thisMSGrade);
				if($needStyle) {
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
				}
				else {
					$thisMarkDisplay = $thisMark;
				}
				
				# Subject Form Ordering
				// $thisClassOrdering = $MarksAry[$SubjectID][0]["OrderMeritClass"];
				// $thisClassTotalCount = $MarksAry[$SubjectID][0]["ClassNoOfStudent"];
                $thisFormOrdering = $MarksAry[$SubjectID][0]["OrderMeritForm"];
                $thisFormTotalCount = $MarksAry[$SubjectID][0]["FormNoOfStudent"];
				
				# for Preview Purpose
				$thisFormOrderingDisplay = "&nbsp;";
				if(!$StudentID && !$isSub) {
					$thisFormOrderingDisplay = "#";
				}
				
				# Subject Group Order Display
				if($StudentID && !$isSub && $thisFormOrdering > 0) {
					$thisFormOrderingDisplay = $thisFormOrdering."/".$thisFormTotalCount;
				}
				
				$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>".$thisMarkDisplay."</td>";
				$x[$SubjectID] .= "<td class='border_left border_right tabletext' align='center'>".$thisFormOrderingDisplay."</td>";
				$isFirst = 0;
				
				# Construct an array to return
				$returnArr["HTML"][$SubjectID] = $x[$SubjectID];
				$returnArr["isAllNA"][$SubjectID] = $isAllNA;
			}
		}
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;

		# Other Info Table
		# Year Report
		$OtherInfoDataAry = array();
		if($isYearReport)
		{
			if($StudentID != "")
			{
				$lastTermID = $this->Get_Last_Semester_Of_Report($ReportID);
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
				$OtherInfoDataAry = $OtherInfoDataAry[$StudentID][$lastTermID];
			}
			if(!empty($OtherInfoDataAry))
			{
				$PromotedTo = ($OtherInfoDataAry["Promoted to"]) ? $OtherInfoDataAry["Promoted to"] : "&nbsp;";
			}

			# Promotion Status Table
//			$PromotionTable .= "<tr>";
//				$PromotionTable .= "<td class='tabletext signatre_title' width='16%'>".$eReportCard["Template"]["PromotedToEn"]."</td>";
//				$PromotionTable .= "<td class='tabletext signatre_title' width='84%' style='padding-left: 4px'>：　".$PromotedTo."</td>";
//			$PromotionTable .= "</tr>";
//			$PromotionTable .= "<tr>";
//				$PromotionTable .= "<td class='tabletext signatre_title' colspan='2'>".$eReportCard["Template"]["PromotedToCh"]."</td>";
//			$PromotionTable .= "</tr>";
			$PromotionTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='signature_table' style='padding-top: 6px;'>";
			$PromotionTable .= "<tr>";
				$PromotionTable .= "<td class='tabletext signatre_title' width='100%' style='padding-left: 4px'>".$PromotedTo."</td>";
			$PromotionTable .= "</tr>";
			$PromotionTable .= "</table>";
			
			# Empty Table
			if($FormNumber == 6)
			{
				$PromotionTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' height='10' class='footer_table'>";
				$PromotionTable .= "<tr>";
					$PromotionTable .= "<td class='tabletext'></td>";
				$PromotionTable .= "</tr>";
				$PromotionTable .= "</table>";
			}
		}
		# Term Report
		else
		{
			if ($StudentID != "")
			{
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
				$OtherInfoDataAry = $OtherInfoDataAry[$StudentID][$SemID];
				
				$AttendanceDataAry = $this->Get_Student_Profile_Data($ReportID, $StudentID, false, false, false, "", true);
				$AttendanceDataAry = $AttendanceDataAry["AttendanceAry"][$StudentID];
				$OtherInfoDataAry["Times Late"] = isset($OtherInfoDataAry["Times Late"]) ? $OtherInfoDataAry["Times Late"] : $AttendanceDataAry["Time Late"];
				$OtherInfoDataAry["Days Absent"] = isset($OtherInfoDataAry["Days Absent"]) ? $OtherInfoDataAry["Days Absent"] : $AttendanceDataAry["Days Absent"];
				// $OtherInfoDataAry["Days Absent"] = my_round($OtherInfoDataAry["Days Absent"], 2);
                $OtherInfoDataAry["Days Absent"] = my_round($OtherInfoDataAry["Days Absent"], 1);
			}
			if(!empty($OtherInfoDataAry))
			{
				// $DaysAbsence = ($OtherInfoDataAry["Days Absent"])? $OtherInfoDataAry["Days Absent"] : "0.00";
                $DaysAbsence = ($OtherInfoDataAry["Days Absent"])? $OtherInfoDataAry["Days Absent"] : "0.0";
				$TimesLate = ($OtherInfoDataAry["Times Late"])? $OtherInfoDataAry["Times Late"] : "0";
				$Conduct = ($OtherInfoDataAry["Conduct"])? $OtherInfoDataAry["Conduct"] : $this->EmptySymbol;
			}
			
			# Attendance & Conduct Table
			$OtherInfoTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='other_info_table'>";
			$OtherInfoTable .= "<tr>";
				$OtherInfoTable .= "<td class='tabletext' width='28%' align='center'>".$eReportCard["Template"]["DaysAbsentEn"]." : "."</td>";
				$OtherInfoTable .= "<td class='tabletext' width='11%'>".$DaysAbsence."</td>";
				$OtherInfoTable .= "<td class='tabletext' width='28%' align='center'>".$eReportCard["Template"]["TimesLateEn"]." : "."</td>";
				$OtherInfoTable .= "<td class='tabletext' width='9%'>".$TimesLate."</td>";
				$OtherInfoTable .= "<td class='tabletext' width='16%' align='center'>　".$eReportCard["Template"]["ConductEn"]." : "."</td>";
				$OtherInfoTable .= "<td class='tabletext' width='6%'>　".$Conduct."</td>";
				$OtherInfoTable .= "<td class='tabletext' width='1%'>&nbsp;</td>";
			$OtherInfoTable .= "</tr>";
			$OtherInfoTable .= "<tr>";
				$OtherInfoTable .= "<td class='tabletext' align='center'>".$eReportCard["Template"]["DaysAbsentCh"]."</td>";
				$OtherInfoTable .= "<td class='tabletext'>&nbsp;</td>";
				$OtherInfoTable .= "<td class='tabletext' align='center'>".$eReportCard["Template"]["TimesLateCh"]."</td>";
				$OtherInfoTable .= "<td class='tabletext'>&nbsp;</td>";
				$OtherInfoTable .= "<td class='tabletext' align='center'>".$eReportCard["Template"]["ConductCh"]."</td>";
				$OtherInfoTable .= "<td class='tabletext'>&nbsp;</td>";
				$OtherInfoTable .= "<td class='tabletext'>&nbsp;</td>";
			$OtherInfoTable .= "</tr>";
			$OtherInfoTable .= "<tr>";
				$OtherInfoTable .= "<td class='tabletext' colspan='7'>&nbsp;</td>";
			$OtherInfoTable .= "</tr>";
			$OtherInfoTable .= "<tr>";
				$OtherInfoTable .= "<td class='tabletext' colspan='7'>&nbsp;</td>";
			$OtherInfoTable .= "</tr>";
			$OtherInfoTable .= "<tr>";
				$OtherInfoTable .= "<td class='tabletext' colspan='7'>&nbsp;</td>";
			$OtherInfoTable .= "</tr>";
			$OtherInfoTable .= "</table>";
			
			# Empty Table
			$PromotionTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' height='10' class='footer_table'>";
			$PromotionTable .= "<tr>";
				$PromotionTable .= "<td class='tabletext'></td>";
			$PromotionTable .= "</tr>";
			$PromotionTable .= "</table>";
		}
		
		# System Data Table
		if($isYearReport)
		{
			$OLEDataSize = 0;
			$MeritAwardDataSize = 0;
			if($StudentID != "")
			{
				$ProfilePortfolioData = $this->Get_Profile_Portfolio_Data($ReportID, $StudentID);
				//$OLEDataAry = $ProfilePortfolioData["OLEService"];
				//$OLEDataSize = sizeof($OLEDataAry);
				$MeritAwardDataAry = $ProfilePortfolioData["MeritAward"];
				$MeritAwardDataSize = sizeof($MeritAwardDataAry);
				
				$allTermIdAry = $this->returnReportInvolvedSem($ReportID);
				$allTermIdAry = array_keys($allTermIdAry);
				$allTermIdAry[] = '0';
				$OLEDataAry = $this->Get_eEnrolment_Data($StudentID, $allTermIdAry);
				$OLEDataSize = sizeof($OLEDataAry);
			}
			
			# ECA / Service Table
			$OLEDataTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='footer_table footer_table2' style='padding-bottom: 6px'>";
			$OLEDataTable .= "<tr>";
				$OLEDataTable .= "<td class='tabletext' colspan='2'><u>".$eReportCard["Template"]["ECAService"]."</u></td>";
			$OLEDataTable .= "</tr>";
			
			for($i=0; $i<$OLEDataSize; $i++)
			{
				$fieldLocation = ($i % 2);
				if($fieldLocation == 0) {
					$OLEDataTable .= "<tr>";
				}
				
				$thisOLEData = $OLEDataAry[$i];
				$thisRole = trim($thisOLEData["RoleTitle"]);
				$thisClubTitle = trim($thisOLEData["ClubTitle"]);
				$thisPerformance = trim($thisOLEData["Performance"]);
				
				// Display : Role of Club name - Performance
				$thisOLEDisplay = "";
				$thisOLEDisplay .= $thisRole != "" && $thisRole != "N/A" ? $thisRole." of " : "";
				$thisOLEDisplay .= $thisClubTitle != "" ? $thisClubTitle : "";
				$thisOLEDisplay .= $thisPerformance != "" ? " - ".$thisPerformance : "";
				$thisOLEDisplay = $thisOLEDisplay != "" ? $thisOLEDisplay : "&nbsp;";
				$thisOLEDisplay = stripslashes($thisOLEDisplay);
				
				$needSmallerSpacing = strlen($thisOLEDisplay) > 59;
				$thisDataStyle = $needSmallerSpacing? " style='letter-spacing: -0.85px;' " : '';
				$OLEDataTable .= "<td class='tabletext record_details' width='50%' valign='top' ".$thisDataStyle.">".$thisOLEDisplay."</td>";
				
				if(($i + 1) % 2 == 0 || ($i + 1) == $OLEDataSize) {
					$OLEDataTable .= "</tr>";
				}
			}
			if($OLEDataSize == 0)
			{
			    $OLEDataTable .= "<tr><td class='tabletext record_details'> Nil </td></tr>";
			}
			$OLEDataTable .= "</table>";
			
			# Award / Punishment Table
			$MeritAwardTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='footer_table footer_table2' style='padding-bottom: 6px'>";
			$MeritAwardTable .= "<tr>";
				$MeritAwardTable .= "<td class='tabletext' colspan='2'><u>".$eReportCard["Template"]["AwardPunishment"]."<u></td>";
			$MeritAwardTable .= "</tr>";
			for($i=0; $i<$MeritAwardDataSize; $i++)
			{
				$fieldLocation = ($i % 2);
				if($fieldLocation == 0) {
					$MeritAwardTable .= "<tr>";
				}
				
				$thisMeritData = $MeritAwardDataAry[$i];
				$thisMeritData = !empty($thisMeritData)? $thisMeritData : "&nbsp;";
				$thisMeritData = stripslashes(nl2br($thisMeritData));
				
				$needSmallerSpacing = strlen($thisMeritData) > 59;
				$thisDataStyle = $needSmallerSpacing? " style='letter-spacing: -0.85px;' " : '';
				$MeritAwardTable .= "<td class='tabletext record_details' width='50%' valign='top' ".$thisDataStyle.">".$thisMeritData."</td>";
				
				if(($i + 1) % 2 == 0 || ($i + 1) == $MeritAwardDataSize) {
					$MeritAwardTable .= "</tr>";
				}
			}
			if($MeritAwardDataSize == 0)
			{
			    $MeritAwardTable .= "<tr><td class='tabletext record_details'> Nil </td></tr>";
			}
			$MeritAwardTable .= "</table>";
		}
		
		# Class Teacher Comment
		if($isYearReport)
		{
			$allTermReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$allTermReport = BuildMultiKeyAssoc((array)$allTermReport, "Semester");
			$lastTermID = $this->Get_Last_Semester_Of_Report($ReportID);
			$ReportID = $allTermReport[$lastTermID]["ReportID"];
		}
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = $CommentAry[0];
		$ClassTeacherComment = $ClassTeacherComment? $ClassTeacherComment : "&nbsp;";
		
		# Define Class Teacher Comment Table Style
		$comment_table_class = $isYearReport? "footer_table2" : "";
		$comment_table_style = $isYearReport? "padding-bottom: 5px;" : "padding-bottom: 10px;";
		$comment_table_height = $isYearReport? "29" : "40";
		
		# Class Teacher Comment Table
		$CommentTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='footer_table ".$comment_table_class."'>";
		$CommentTable .= "<tr>";
			$CommentTable .= "<td class='tabletext' style='".$comment_table_style."'><u>".$eReportCard["Template"]["ClassTeacherComment"]."</u></td>";
		$CommentTable .= "</tr>";
		$CommentTable .= "<tr>";
			$CommentTable .= "<td class='tabletext border_bottom' height='".$comment_table_height."' style='font-size: 13px; padding-bottom: 14px;'>".stripslashes(nl2br($ClassTeacherComment))."</td>";
		$CommentTable .= "</tr>";
		$CommentTable .= "</table>";
		
		# Misc Table in Footer
		$misc_table_footer = "";
		$misc_table_footer .= "<table width=100% border='0' cellpadding='0' cellspacing='0'>";
		if($OtherInfoTable)
		{
			$misc_table_footer .= "<tr>";
			$misc_table_footer .= "<td valign='top'>";
				$misc_table_footer .= $OtherInfoTable;	
			$misc_table_footer .= "</td>";
			$misc_table_footer .= "</tr>";
		}
		if($CommentTable)
		{
			$misc_table_footer .= "<tr>";
			$misc_table_footer .= "<td valign='top'>";
				$misc_table_footer .= $CommentTable;	
			$misc_table_footer .= "</td>";
			$misc_table_footer .= "</tr>";
		}
		if($PromotionTable)
		{
			$misc_table_footer .= "<tr>";
			$misc_table_footer .= "<td valign='top'>";
				$misc_table_footer .= $PromotionTable;	
			$misc_table_footer .= "</td>";
			$misc_table_footer .= "</tr>";
		}
		$misc_table_footer .= "</table>";
		
		# Misc Table in Main Table
		$misc_table_main_table = "";
		$misc_table_main_table .= "<br>";
		$misc_table_main_table .= "<table width=100% border='0' cellpadding='0' cellspacing='0' height='150px'>";
		if($OLEDataTable)
		{
			$misc_table_main_table .= "<tr>";
			$misc_table_main_table .= "<td valign='top' align='center'>";
				$misc_table_main_table .= $OLEDataTable;	
			$misc_table_main_table .= "</td>";
			$misc_table_main_table .= "</tr>";
		}
		if($MeritAwardTable)
		{
			$misc_table_main_table .= "<tr>";
			$misc_table_main_table .= "<td valign='top' align='center'>";
				$misc_table_main_table .= $MeritAwardTable;	
			$misc_table_main_table .= "</td>";
			$misc_table_main_table .= "</tr>";
		}
		$misc_table_main_table .= "</table>";
		
		return array($misc_table_footer, $misc_table_main_table);
	}
	
	########### END Template Related
	
	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr, $isFirst=0, $isLast=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
		# Define CSS Style
		$border_top = $isFirst ? "border_top" : "";
		$border_bottom = $isLast ? "border_bottom" : "";
		$border_left = $isYearReport ? "border_left" : "";
		$border_right = $isYearReport? "border_right" : "";
		
		# Define Default Value
		$defaultValueArr = array();
		$defaultValueArr[""] = $this->EmptySymbol;
		$defaultValueArr["Conduct"] = $this->EmptySymbol;
		$defaultValueArr["Times Late"] = "0";
		$defaultValueArr["Days Absent"] = "0.0";
		
		$x = "";
		$x .= "<tr>";
		$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst, $isYearReport, ($isJuniorLevel && $isYearReport), $isLast);
			
		if($isYearReport)		# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1 => $d1)
			{
				# Get Term Value
				$TermID = $d1["SemesterNum"];
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $defaultValueArr[$InfoKey]) : "#";
				$x .= "<td class='tabletext {$border_top} {$border_bottom} border_left' align='center'>".$thisValue."</td>";
			}
			
			if($FormNumber == 6) {
				$x .= "<td class='tabletext border_left'>&nbsp;</td>";
			}
			else if(sizeof($ColumnData) > 1) {
				$x .= "<td class='tabletext border_left' colspan='2'>&nbsp;</td>";
			}
		}
		else					# Term Report
		{
			# Get Term Value
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			$x .= "<td class='tabletext {$border_top} border_left' align='center'>".$thisValue."</td>";
		}
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $ExtraValue="", $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$isYearReport 	= $SemID == "F";
		$ClassLevelID 	= $ReportSetting["ClassLevelID"];
		$FormNumber		= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isJuniorLevel	= $FormNumber < 4;
		$isSeniorLevel  = !$isJuniorLevel;
		
		# Define CSS Style
		$border_top = $isFirst ? "border_top" : "";
		$border_left = $isYearReport ? "border_left" : "";
		$border_right = $isYearReport? "border_right" : "";
		
		$x .= "<tr>";
		$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst, $isYearReport, ($isJuniorLevel && $isYearReport), false, true);
		
		if ($isYearReport)
		{
			$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
			if($FormNumber != 6)
			{
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					$thisValue = $ValueArr[$ColumnID] ? $ValueArr[$ColumnID] : "&nbsp;";
					$x .= "<td class='tabletext {$border_top} {$border_left} info_table_footer' align='center'>".$thisValue."</td>";
				}
			}
			$OverallValue = $OverallValue ? $OverallValue : "&nbsp;";
			$x .= "<td class='tabletext {$border_top} {$border_left} info_table_footer' align='center'>".$OverallValue."</td>";
			if($ExtraValue) {
				$x .= "<td class='tabletext {$border_top} {$border_left} {$border_right} info_table_footer' align='center'>".$ExtraValue."</td>";
			}
		}
		else
		{
			$OverallValue = $OverallValue ? $OverallValue : "&nbsp;";
			$x .= "<td class='tabletext {$border_top} {$border_left} info_table_footer' align='center'>".$OverallValue."&nbsp;</td>";
			if($ExtraValue) {
				$x .= "<td class='tabletext {$border_top} {$border_left} {$border_right} info_table_footer' align='center'>".$ExtraValue."&nbsp;</td>";
			}
			if($isJuniorLevel && !$isYearReport) {
				$x .= "<td class='tabletext {$border_top} {$border_left} {$border_right} info_table_footer' align='center'>&nbsp;</td>";
			}
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0, $hasBorderLeft=0, $applyWeightColspan=false, $hasBorderBottom=0, $hasLargerFont=false)
	{
		# Define CSS Style
		$font_class = $hasLargerFont ? "info_table_footer" : "";
		$border_left = $hasBorderLeft ? "border_left" : "";
		$border_top = $hasBorderTop ? "border_top" : "";
		$border_bottom = $hasBorderBottom ? "border_bottom" : "";
		
		$x = "";
		$x .= "<td class='tabletext {$border_top} {$border_bottom} {$border_left}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;</td><td class='tabletext report_left_col ".$font_class."'>".$TitleEn."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		$x .= "<td class='tabletext {$border_top} {$border_bottom}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;</td><td class='tabletext report_left_col ".$font_class."'>".$TitleCh."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		if($applyWeightColspan)
		{
			$x .= "<td class='tabletext {$border_top} {$border_bottom}'>";
				$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
					$x .= "<tr><td>&nbsp;</td><td class='tabletext'>&nbsp;</td></tr>";
				$x .= "</table>";
			$x .= "</td>";
		}
		
		return $x;
	}
	
	// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		return false;
	}
	
	function Get_Profile_Portfolio_Data($ReportID, $StudentID)
	{
		global $PATH_WRT_ROOT, $eclass_db;
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		$dataAry = array();
		$dataAry["MeritAward"] = array();
		$dataAry["OLEService"] = array();
		
		# Retrieve Display Settings
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportInfoArr["Semester"];
		$ReportType = $SemID == "F" ? "W" : "T";
		$TermStartDate = $ReportInfoArr["TermStartDate"];
		$TermEndDate   = $ReportInfoArr["TermEndDate"];
		
		// Term Report no need to display
		if($ReportType == "T") {
			return $dataAry;
		}
		
		# Report Term Range
		$TermStartDate = getStartDateOfAcademicYear($this->GET_ACTIVE_YEAR_ID());
		$TermStartDate = substr($TermStartDate, 0, 10);
		$TermEndDate = getEndDateOfAcademicYear($this->GET_ACTIVE_YEAR_ID());
		$TermEndDate = substr($TermEndDate, 0, 10);
		
// 		if (is_date_empty($TermStartDate) && is_date_empty($TermEndDate))
// 		{
// 			$allTermReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
// 			$YearTermIDArr = Get_Array_By_Key($allTermReport, "Semester");
// 			$numOfTerm = count($YearTermIDArr);
// 			for ($i=0; $i<$numOfTerm; $i++)
// 			{
// 				$_yearTermId = $YearTermIDArr[$i];
//				
// 				$_objYearTerm = new academic_year_term($_yearTermId);
// 				$_termStartDate = substr($_objYearTerm->TermStart, 0, 10);
// 				$_termEndDate = substr($_objYearTerm->TermEnd, 0, 10);
//				
// 				if ($TermStartDate == "" || $_termStartDate < $TermStartDate) {
// 					$TermStartDate = $_termStartDate;
// 				}
// 				if ($TermEndDate == "" || $_termEndDate > $TermEndDate) {
// 					$TermEndDate = $_termEndDate;
// 				}
// 			}
// 		}
		
		# Get ECA / Service Records
		/*
		$sql = "SELECT
					CONCAT('', c.Title, '')
				FROM
					{$eclass_db}.OLE_STUDENT as a
				INNER JOIN {$eclass_db}.OLE_PROGRAM as c
					ON a.ProgramID = c.ProgramID
				WHERE
					a.UserID = '".$StudentID."' AND 
					a.RecordStatus = 2 AND 
					c.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."' AND 
					c.IntExt = 'INT'
				ORDER BY
					CONCAT('', c.Title, '')
				LIMIT 12";
		$OLEDataAry = $this->returnVector($sql);
		*/
		
		# Get Punishment Records
		$MeritDataArr = array();
		$OtherInfoMeritAry = $this->getReportOtherInfoData($ReportID, $StudentID, "", "merit");
		$OtherInfoMeritAry = $OtherInfoMeritAry[$StudentID];
		if(!empty($OtherInfoMeritAry))
		{
		    foreach($OtherInfoMeritAry as $thisTermMeritInfoAry) {
                if(!empty($thisTermMeritInfoAry["Punishment"])) {
                    $MeritDataArr = array_merge($MeritDataArr, (array)$thisTermMeritInfoAry["Punishment"]);
				}
			}
		}
		$MeritDataArr = array_filter($MeritDataArr);
		$MeritDataArr = asorti($MeritDataArr);
		$MeritDataArr = array_values($MeritDataArr);
		
		/*
		$sql = "SELECT
					Reason
				FROM 
					PROFILE_STUDENT_MERIT
				WHERE
					UserID = '".$StudentID."' AND 
					RecordStatus = '1' AND 
					DATE(MeritDate) BETWEEN '".$TermStartDate."' AND '".$TermEndDate."'
				ORDER BY
					Reason
				LIMIT 16 ";
		$MeritDataArr = $this->returnVector($sql);
		*/
		
		# Get Award Records
		$sql = "SELECT
					AwardName
				FROM
					{$eclass_db}.AWARD_STUDENT
				WHERE
					UserID = '".$StudentID."' AND
					AwardDate BETWEEN '".$TermStartDate."' AND '".$TermEndDate."'
				ORDER BY
					AwardName
				LIMIT 16 ";
		$AwardDataAry = $this->returnVector($sql);
		$AwardDataAry = array_filter($AwardDataAry);
		$AwardDataAry = asorti($AwardDataAry);
		$AwardDataAry = array_values($AwardDataAry);
		
		$MeritAwardDataAry = array_merge($AwardDataAry, $MeritDataArr);
		if(count($MeritAwardDataAry) > 16) {
			$MeritAwardDataAry = array_slice($MeritAwardDataAry, 0, 16);
		}
		
		$dataAry["MeritAward"] = $MeritAwardDataAry;
		//$dataAry["OLEService"] = $OLEDataAry;
		
		return $dataAry;
	}
}
?>