<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/sung_tsun.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "eca", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 1;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID) {
		global $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$TableWidth = ($isMainReport)? '775px' : '670px';
		
		$TableTop = "<table width='$TableWidth' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		//$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		//$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		//$TableBottom .= $FooterRow;
		//$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				//$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			
			if(!$ReportSetting['isMainReport']) //Extra Report
			{
				$HeaderHeight = $ReportSetting['HeaderHeight'];
				//$ReportTitle =  $ReportSetting['ReportTitle'];
				//$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
				$ReportTitle = $eReportCard['Template']['ExtraReportTitleEn'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['ExtraReportTitleCh'];
				
				# get school badge
				$SchoolLogo = GET_SCHOOL_BADGE();
					
				# get school name
				$SchoolNameCh = $eReportCard['Template']['SchoolInfo']['SchoolNameCh'];
				$SchoolNameEn = $eReportCard['Template']['SchoolInfo']['SchoolNameEn'];
				
				$imgfile = "/file/reportcard2008/templates/sung_tsun.jpg";
				$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" height='100px'>\n" : "";
				$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
				
				if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
		
				$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
				$TitleTable .= "<tr>";
						$TitleTable .= "<td width='120px' align='center'>".$TempLogo."</td>";
						$TitleTable .= "<td>";
							$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
								$TitleTable .= "<tr><td nowrap='nowrap' class='text_16px text_bold'>".$SchoolNameCh."</td></tr>\n";
								$TitleTable .= "<tr><td nowrap='nowrap' class='text_16px text_bold'>".$SchoolNameEn."</td></tr>\n";
								$TitleTable .= "<tr><td nowrap='nowrap' class='text_16px text_bold'>".$ReportTitle."</td></tr>\n";
							$TitleTable .= "</table>\n";
						$TitleTable .= "</td>";
					$TitleTable .= "</tr>";
				$TitleTable .= "</table><br><br>";
			}
			else // Main Report
			{
				# spare space for preprinted header
				$TitleTable = "<table class='schoolinfo'><tr><td>&nbsp;</td></tr></table>";
			}
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol 	= $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray 		= $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting 			= $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo 	= unserialize($ReportSetting['DisplaySettings']);
			$LineHeight			 	= $ReportSetting['LineHeight'];
			$ClassLevelID 			= $ReportSetting['ClassLevelID'];
			$FormNumber 			= $this->GET_FORM_NUMBER($ClassLevelID);
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$isMainReport = $ReportSetting['isMainReport'];
			
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();

			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('en', 2);
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				/*
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= "-".$thisClassNumber;
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= "-".$thisClassNumber;
				}
				*/
				$data['Class'] .= "-".$thisClassNumber;
				
				$data['DateOfBirth'] = $this->FormatDate($lu->DateOfBirth,"D/M/Y");
				$data['DateOfBirth'] = $data['DateOfBirth']==''?"&nbsp;":$data['DateOfBirth'];
				$data['Gender'] = $eReportCard['GenderArr'][$lu->Gender];
				$data['HKID'] = $lu->HKID;
				$data['StudentAdmNo'] = $data['STRN'];
				
				include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
				$ObjYear = new academic_year($this->schoolYearID);
				$AcademicYear = $ObjYear->Get_Academic_Year_Name();
				$data['SchoolYear'] = $AcademicYear;
				
				$data['DateOfIssue'] = $this->FormatDate($ReportSetting['Issued'],"D/M/Y");
				
				$ClassTeacherAry = $lclass->returnClassTeacherID($thisClassName, $this->schoolYearID);
				
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				foreach($ClassTeacherAry as $key=>$val)
				{
					$luser = new libuser($val["UserID"]);	
					$CTeacher[] = $luser->EnglishName." ".$luser->ChineseName;
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";

//				2011-0705-1646-01067 - 西貢崇真天主教中學 - about the xxx marks in the report card 				
//				2012-0320-1123-50071 - 西貢崇真天主教中學 - Cross missing in report for F6 and F7 // uncomment the coding as client claim CS mistake in the above case.  				
				if($ReportType == "W" && ($FormNumber == 6 ||$FormNumber == 7))
					$data['deleteline'] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
				else
					$data['deleteline'] = "&nbsp;";
			}
			
			if(!$ReportSetting['isMainReport']) 
				$StudentTitleArray = array("Name","Gender","HKID","DateOfBirth","Class","ClassTeacher","DateOfIssue");
			else
				$StudentTitleArray = array("Name","Gender","HKID","DateOfBirth","Class","ClassTeacher","AcademicYear","DateOfIssue","deleteline");
				
			if(!empty($StudentTitleArray))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='studentinfo tabletext'>";
				
				# move down 1mm for Form 7 Consolidated Report
				if (($FormNumber==7 || $FormNumber==6 || $FormNumber==5) && $ReportType=='W' && $isMainReport==true)
				{
					$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
					$StudentInfoTable .= "<tr><td height='8'><img src='".$emptyImagePath."' width='1'></td></tr>";
				}
				
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(!$ReportSetting['isMainReport']) // extra report
					{
						$Title = "<td class='studentinfolabel tabletext'>".$eReportCard['Template']['StudentInfo'][$SettingID."En"]."</td>";
						$Title .= "<td class='studentinfolabel tabletext'>".$eReportCard['Template']['StudentInfo'][$SettingID."Ch"]."</td>";
						$Title .= "<td width='25px'>:</td>";
					}
					else //Main Report
					{
						$Title = "<td class='studentinfolabel'>&nbsp;</td><td class='studentinfolabel'>&nbsp;</td><td width='25px'>&nbsp;</td>";
					}
					
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "$Title<td height='12' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
					$StudentInfoTable .= "</tr>";
			
					$count++;
				}
				
				if (($FormNumber==7 || $FormNumber==6 ) && $ReportType=='W' && $isMainReport==true)
				{
					$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
					$StudentInfoTable .= "<tr><td height='5'><img src='".$emptyImagePath."' width='1'></td></tr>";
				}
				$StudentInfoTable .= "</table>";
				
				if(!$ReportSetting['isMainReport']) 
					$StudentInfoTable.="<br><br>";
				
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$isMainReport = $ReportSetting['isMainReport'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		
		if ($FormNumber == 7  || $FormNumber==6 )
			$thisClass = 'ColHeaderPadding_F7';
		else
			$thisClass = 'ColHeaderPadding';
			
		$DetailsTable = "<div class='$thisClass'></div>";
		if(!$ReportSetting['isMainReport']) 
		{
			$report_border = " class='report_border ' ";
			$width = "100%";
		}
		else
		{
			$report_border = "";
			if($ReportType=="W")
				$width = "670px";
			else
				$width = "655px";
		}
		
		$DetailsTable .= "<table width='$width' border='0' cellspacing='0' cellpadding='0' $report_border valign='top'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		$LastCatID = 0;
		$ctr=0;
		
		if ($FormNumber == 5)	// form5 => the footer part need to move upper than usual
		{
			$tablerow = $eRCTemplateSetting['MSTableRow'] ;
			$rowHeight = '16px';
		}
		elseif ($FormNumber == 6 || $FormNumber == 7)		// form6 => the footer part need to move lower than usual
		{
			$tablerow = $eRCTemplateSetting['MSTableRow'] + 1;
			$rowHeight = '16px';
		}
		else
		{
			$tablerow = $eRCTemplateSetting['MSTableRow'];
			$rowHeight = '16px';
		}
		
		for($i=0;$i<$sizeofSubjectCol||($ctr<$tablerow && $ReportSetting['isMainReport']);$i++) # if(is Main Report) print at least {$tablerow} lines, to control the position of msfooter
		{
			$isSub = 0;
			$LearningCatName='';
			$thisSubjectID = $SubjectIDArray[$i];
			
			if(!$thisSubjectID)
			{
				if ($isMainReport)
					$DetailsTable .= "<tr><td colspan='2' height='".$rowHeight."'>&nbsp;</td></tr>";
					
				$ctr++;
				continue;
			}
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# Hide component subj of F1-3 and all extra-term report
			if(($FormNumber <=3 || $isMainReport==false) && $isSub)	continue;
			
//			# Get Learning Cat
//			if($FormNumber >= 6 )
//			{
//				include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
//				$SubjObj = new subject($thisSubjectID);
//				$LearnCatObj = new Learning_Category($SubjObj->LearningCategoryID);
//				if($SubjObj->LearningCategoryID != $LastCatID)
//				{
//					$LearningCatName = $LearnCatObj->NameEng;
//				}
//				if(!$isSub) $LastCatID = $SubjObj->LearningCategoryID;
//			}
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
//				if($LearningCatName && $FormNumber >= 6)
//				{
//					if(!$isFirst)
//					{
//						if ($isMainReport)
//							$DetailsTable .= "<tr><td colspan='2' height='$LineHegiht'>&nbsp;</td></tr>";
//						else
//						{
//							$DetailsTable .= "<tr>";
//								$DetailsTable .= "<td colspan='2' height='$LineHegiht' class='tabletext border_top'>&nbsp;</td>";
//								$DetailsTable .= "<td height='$LineHegiht' class='tabletext border_top border_left'>&nbsp;</td>";
//								$DetailsTable .= "<td height='$LineHegiht' class='tabletext border_top border_left'>&nbsp;</td>";
//								$DetailsTable .= "<td height='$LineHegiht' class='tabletext border_top border_left'>&nbsp;</td>";
//							$DetailsTable .= "</tr>";
//						}
//						$ctr++;
//					}
//					
//					$DetailsTable .= "<tr>";
//						$thisClass = ($isMainReport)? '' : 'border_top';
//						$DetailsTable .= "<td colspan='2' height='$LineHegiht' class='tabletext $thisClass'>$LearningCatName</td>";
//						
//						if ($isMainReport==false)
//						{
//							$DetailsTable .= "<td height='$LineHegiht' class='tabletext border_top border_left'>&nbsp;</td>";
//							$DetailsTable .= "<td height='$LineHegiht' class='tabletext border_top border_left'>&nbsp;</td>";
//							$DetailsTable .= "<td height='$LineHegiht' class='tabletext border_top border_left'>&nbsp;</td>";
//						}
//					$DetailsTable .= "</tr>";
//					$ctr++;
//				} 
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				
				$DetailsTable .= "</tr>";
				$ctr++;
				$isFirst = 0;
			}
			
		}
		
//		if (($FormNumber==7  || $FormNumber==6 )&& $isMainReport && $ReportType=='W')
//		{
//			$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
//			$DetailsTable .= "<tr><td height='20'><img src='".$emptyImagePath."' width='1'></td></tr>";
//		}
				
		# MS Table Footer
		if($ReportSetting['isMainReport'])
			$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		// [2016-0530-1501-25206] moved below footer
		if (($FormNumber==7  || $FormNumber==6 )&& $isMainReport && $ReportType=='W')
		{
			$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
			$DetailsTable .= "<tr><td height='20'><img src='".$emptyImagePath."' width='1'></td></tr>";
		}
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if(!$ReportSetting['isMainReport'])	# Extra Report
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			$e = 0;
			$row1 = "<td valign='middle' height='{$LineHeight}' class='tabletext border_left' align='center' colspan='3'>".$eReportCard['Template']['AcademicPerformance']."</td>";
			
			$row2 .= "<td valign='middle' height='{$LineHeight}' class='tabletext border_left border_top' align='center' width='".$eRCTemplateSetting['ColumnWidth']['TermReport']['Term']."'>".$eReportCard['Template']['LearningAttitude']."</td>";
			$row2 .= "<td valign='middle' height='{$LineHeight}' class='tabletext border_left border_top' align='center' width='".$eRCTemplateSetting['ColumnWidth']['TermReport']['Term']."'>".$eReportCard['Template']['CourseWork']."</td>";
			$row2 .= "<td valign='middle' height='{$LineHeight}' class='tabletext border_left border_top' align='center' width='".$eRCTemplateSetting['ColumnWidth']['TermReport']['Term']."'>".$eReportCard['Template']['BasicCompetence']."</td>";
			
			$row1 .= "<td valign='middle' height='{$LineHeight}' class='tabletext border_left' align='center' rowspan='2'>".$eReportCard['Template']['UniformTestResult']."</td>";
			$needRowspan = 1;
		}
		else
		{
			if($ReportType=="T")	# Terms Report Type
			{
				# Retrieve Invloved Assesment
				$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
				$ColumnID = array();
				$ColumnTitle = array();
				if (sizeof($ColoumnTitle) > 0)
					foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
					
				$e = 0;
				for($i=0;$i<sizeof($ColumnTitle);$i++)
				{
					//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
					$ColumnTitleDisplay = $ColumnTitle[$i];
					$row1 .= "<td valign='middle' height='{$LineHeight}' class='tabletext' align='center' width='".$eRCTemplateSetting['ColumnWidth']['TermReport']['Term']."'>&nbsp;</td>";
					$n++;
	 				$e++;
				}
				$row1 .= "<td width='".$eRCTemplateSetting['ColumnWidth']['TermReport']['Term']."'>&nbsp;</td>";
			}
			else					# Whole Year Report Type
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
	 			$needRowspan=0;
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
	
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
					$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='small_title' align='center' width='".$eRCTemplateSetting['ColumnWidth']['ConsolidateReport']['Term']."'>&nbsp;</td>";
				}
				
				#delete 2nd term of F7
				if($FormNumber==7 || $FormNumber==6 )
				{
					$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
					$deleteLines = $eReportCard['DeleteLine'];
					$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='small_title' align='center' width='".$eRCTemplateSetting['ColumnWidth']['ConsolidateReport']['Term']."' valign='top'>";
						$row1 .= "<table width='100% cellpadding='0' cellspacing='0' border='0' align='center' valign='top'>";
							//$row1 .= "<tr><td height='1'><img src='".$emptyImagePath."' width='1'></td></tr>";
							//$row1 .= "<tr><td align='center' style='font-size:14px' valign='top'>".$deleteLines."</td></tr>";
							//$row1 .= "<tr><td align='center' style='font-size:50px' valign='top'>X</td></tr>";
						$row1 .= "</table>";
					$row1 .= "</td>";
				}
				
				$row1 .= "<td width='".$eRCTemplateSetting['ColumnWidth']['ConsolidateReport']['Term']."'>&nbsp;</td>";
				$row1 .= "<td width='".$eRCTemplateSetting['ColumnWidth']['ConsolidateReport']['Term']."'>&nbsp;</td>";
				
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		if($ReportSetting['isMainReport'])
		{
			# Subject 
			$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
			for($i=0;$i<sizeof($SubjectColAry);$i++)
			{
				$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
				$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
				$SubjectTitle = $SubjectColAry[$i];
				$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
				$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
				
				if ($ReportType=='W')
					$thisWidth = $eRCTemplateSetting['ColumnWidth']['ConsolidateReport']['Subject'];
				else
					$thisWidth = $eRCTemplateSetting['ColumnWidth']['Subject'];
				
				$x .= "<td {$Rowspan}  valign='middle' class='small_title ColHeader' height='{$LineHeight}' width='$thisWidth'>&nbsp; </td>";
				$n++;
			}
		}
		else
		{
			$SubjectTitle = $eReportCard['Template']['SubjectEn']." ".$eReportCard['Template']['SubjectCh'];
			$x .= "<td $Rowspan colspan='2'  valign='top' align='center' class='tabletext' height='{$LineHeight}' width='". ($eRCTemplateSetting['ColumnWidth']['Subject']*2) ."'>".$SubjectTitle." </td>";
		}
		# Marks
		$x .= $row1;
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting,$PATH_WRT_ROOT;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$LineHeight = $ReportSetting['LineHeight'];
		$isMainReport = $ReportSetting['isMainReport'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		if($FormNumber <=5)
			 		{
			 			$border_left='';
			 			if(!$ReportSetting['isMainReport'])
							$border_top = ' border_top ';
							
				 		foreach($SubjectDisplay as $k=>$v)
				 		{
					 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
			 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
			 				
			 				$v = str_replace("SubjectEng", $SubjectEng, $v);
			 				$v = str_replace("SubjectChn", $SubjectChn, $v);
			 				
				 			$t .= "<td class='tabletext $border_left $border_top' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext' nowrap>$v</td>";
							$t .= "</tr></table>";
							$t .= "</td>";
							if(!$ReportSetting['isMainReport'])
								$border_left = ' border_left ';
				 		}
			 		}
			 		else // Form 6-7 , Show Eng Name Only
			 		{
			 			# add indent before subjects which has learning category
 						include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
						$SubjObj = new subject($SubSubjectID);
						if(!empty($SubjObj->LearningCategoryID))
							$LearningCatIndent = "&nbsp;&nbsp;&nbsp;&nbsp;";
							
						if(!$ReportSetting['isMainReport'])
							$border_top = ' border_top ';
						 		
			 			# Show Eng only
			 			$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$v =$SubjectEng;
		 				
			 			$t .= "<td class='tabletext $border_top' height='{$LineHeight}' valign='middle' colspan=2>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>{$LearningCatIndent}&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
						
						
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		if($FormNumber >= 4) // hide Grand total and AVG for F4 - F7
		{
			$GrandTotal = $this->EmptySymbol;
			$GrandAverage = $this->EmptySymbol;		
		}
		else
		{
			$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
			$GrandAverage = $StudentID ? $result['GrandAverage']: "S";
		}
	
		foreach ($ColumnTitle as $ColumnID => $ColumnName) {
			
			$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
			
			if($FormNumber<=3 && $ReportType=="W")
			{
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
			}
			else
			{
				$columnTotal[$ColumnID] = $this->EmptySymbol;
				$columnAverage[$ColumnID] = $this->EmptySymbol;	
			}
		}
		
		$first = 1;
		# Overall Result
			$thisTitleEn = "&nbsp;";
			$thisTitleCh = "&nbsp;";
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
			
			$first = 0;

			# Average Mark 
			$thisTitleEn = "&nbsp;";
			$thisTitleCh = "&nbsp;";
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
			
			$first = 0;
			
			
			##################################################################################
			# CSV related
			##################################################################################
			# build data array
			$ary = array();
			$csvType = $this->getOtherInfoType();
			
			//modified by Marcus 20/8/2009
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
			$ary=$OtherInfoDataAry[$StudentID];
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
					}
					else
						$ColNum2Ary[$TermID] = 0;
				}
			}

			$border_top = $first ? "border_top" : "";
			
			# Days Absent 
			$thisTitleEn = "&nbsp;";
			$thisTitleCh = "&nbsp;";
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
			
			# Times Late 
			$thisTitleEn = "&nbsp;";
			$thisTitleCh = "&nbsp;";
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary);
			
			# Total Number of School Days
			$thisTitleEn = "&nbsp;";
			$thisTitleCh = "&nbsp;";
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Total Number of School Days", $ary);
			
			# Conduct 
			$thisTitleEn = "&nbsp;";
			$thisTitleCh = "&nbsp;";
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
		//}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$isMainReport 				= $ReportSetting['isMainReport'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);	
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			if(!$ReportSetting['isMainReport'])
				$border = ' border_top border_left ';
			else
				$border = '';
				
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isSub)? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
									
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					 else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
							
							# check special case 
							//2012-0503-0959-39066 - 西貢崇真天主教中學 - Revise 持續評估報告 template
							// do not convert N.A. to EmptySymbol for extra term report 
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);

						}
						
						if ($isMainReport)
						{
							$thisMarkDisplay = $thisMark;
					 	}
						else
						{
							if ($ScaleDisplay=='M')
							{
								$thisMarkDisplay = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID, $thisColumnID);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
						
					}
						
						
					$x[$SubjectID] .= "<td>";
					$x[$SubjectID] .= "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext $border' align='center' width='100%'>". ($thisMarkDisplay==''?'--':$thisMarkDisplay) ."</td>";
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				
				# Uniform Test Subject Score Display
				if(!$isMainReport)
				{
					// Get Uniform Test Score
					$thisStudentTestMark = "N.A.";
					$UniformTestMarksResult = $this->GET_MANUAL_ADJUSTED_POSITION($StudentID, $SubjectID, $ReportID);
					$UniformTestMarksResult = $UniformTestMarksResult[$StudentID]["Info"];
					if(!empty($UniformTestMarksResult) && $UniformTestMarksResult >= 0) {
						$thisStudentTestMark = $UniformTestMarksResult;
					}
					if ($thisStudentTestMark != "N.A.") {
						$isAllNA = false;
					}
					
					$x[$SubjectID] .= "<td>";
					$x[$SubjectID] .= "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext $border' align='center' width='100%'>".$thisStudentTestMark."</td>";
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall
				if($isMainReport)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					if ($FormNumber >= 4 && $isSub)
					{
						$thisMarkDisplay = "&nbsp;";
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
							
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
					}
					
					# add space between * and mark, add 2 space if no *
					$thisMarkDisplay = str_replace("*","*&nbsp;",$thisMarkDisplay);
					$thisMarkDisplay='<div align="right" style=" width: 25%;">'.$thisMarkDisplay.'</div>';
					
					$x[$SubjectID] .= "<td>";
					$x[$SubjectID] .= "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>". $thisMarkDisplay ."</td>";
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";

				}
				$isFirst = 0;
				
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					$css_border_top = $isSub? "" : "border_top";
					
					//$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only

					# See if any term reports available
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
					
					# if no term reports, the term mark should be entered directly
					
					if (empty($thisReport)) {
						$thisReportID = $ReportID;
						$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
						//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
						#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
						//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							
					} else {
						$thisReportID = $thisReport['ReportID'];
						$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
						//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
						#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
						//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
						
						$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
						$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					}
					
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
					$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
												
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A." && $thisMark != "")
					{
						$isAllNA = false;
					}
					
					# check special case
					if($FormNumber == 5 && in_array($SubjectID,array(13,68))&& $thisMark == "N.A.")
						$thisMark = '--';
					else
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
					
					$thisMarkDisplay = $thisMark;
					//$thisMarkDisplay = str_replace("*","*&nbsp;",$thisMarkDisplay);
					//$thisMarkDisplay='<div align="right" style=" width: 25%;">'.$thisMarkDisplay.'</div>';
					
					# hard code hide project learn term marks
					if($SubjectID==89 && $FormNumber==3)
							$thisMarkDisplay = "&nbsp;";
					
						
					$x[$SubjectID] .= "<td>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>". ($thisMarkDisplay==''||$thisMarkDisplay=='N.A.'?'--':$thisMarkDisplay) ."</td>";
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";

				}
				
				# print empty symbol to 2nd term result of F7
				if($FormNumber == 7 || $FormNumber==6 )
				{
					$x[$SubjectID] .= "<td>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>". $this->EmptySymbol ."</td>";
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
	  			}
				
				# Subject Overall
				$MarksAry = $this->getMarks($ReportID, $StudentID,"","",1);		
				
				$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
				$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
				
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
				
				# for preview purpose
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				if ($thisMark != "N.A." && $thisMark != "")
				{
					$isAllNA = false;
				}
				
					
				# check special case
				if($FormNumber == 5 && in_array($SubjectID,array(13,68))&& $thisMark == "N.A.")
					$thisMark = '--';
				else
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
				
				if($needStyle)
				{
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
				}
				else
					$thisMarkDisplay = $thisMark;

				if($FormNumber>=4 && $isSub)
					$thisMarkDisplay = "&nbsp;";
				else if (($FormNumber==7 || $FormNumber==6)  && $ScaleInput=='G')
					$thisMarkDisplay = $this->EmptySymbol;

				$thisMarkDisplay = ($thisMarkDisplay==''||$thisMarkDisplay=='N.A.'?'--':$thisMarkDisplay);
					
				$thisMarkDisplay = str_replace("*","*&nbsp;",$thisMarkDisplay);
				$thisMarkDisplay='<div align="right" style=" width: 30%;">'.$thisMarkDisplay.'</div>';
					
				$x[$SubjectID] .= "<td>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
				$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>". $thisMarkDisplay ."</td>";
				  					
				$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				$thisMarkDisplay='';
				### Grade Display
				if($FormNumber>=4 && $isSub)
				{
					$thisMarkDisplay = "&nbsp;";
				}
				else
				{
					# Use uccke Effort for Grade Manual Adjustment
					$temp = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
					$ManualGrade = $temp[$StudentID]['Info'];
					if ($ManualGrade != '')
					{
						$thisMarkDisplay = $ManualGrade;
					}
					else
					{
						if(($FormNumber == 5 || $FormNumber == 7 || $FormNumber==6 )&& $ScaleInput=="G")
						{
							$thisMarkDisplay = "--";
						}
						else
						{
							if ($thisGrade == 'N.A.')
								$thisMarkDisplay = "--";
							else if ($thisGrade != '')
								$thisMarkDisplay = $thisGrade;
							else
								$thisMarkDisplay = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID,$StudentID,$SubjectID,$ClassLevelID,0);
						}
					}
				}

				if($ReportType=="W" &&($FormNumber == 5||$FormNumber == 7 || $FormNumber==6 ))
				{
					//$thisMarkDisplay = str_replace("*","*&nbsp;",$thisMarkDisplay);
					$thisMarkDisplay='<div align="right" style=" width: 30%;">'.$thisMarkDisplay.'</div>';
				}
				$x[$SubjectID] .= "<td>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
				$x[$SubjectID] .= "<td class='tabletext' align='center' width='100%'>". $thisMarkDisplay ."</td>";
				  					
				$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";	
 		$ClassLevelID					= $ReportSetting['ClassLevelID'];	
 		$FormNumber					= $this->GET_FORM_NUMBER($ClassLevelID);
		
		if($ReportSetting['isMainReport'])
		{
			
			$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
	
			//modified by marcus 20/8/2009
			//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
			
			# retrieve the latest Term
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($ReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			//$latestTerm++;
			
			# retrieve result data
			$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
			
			$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
			$classteachercomment = $CommentAry[0];
			
			$eca = $ary['ECA'];
			$post = $ary['Post'];
			$numOfECA_fromCSV = empty($eca)?0:count($eca); 
			
			$eEnrolData = $this->Get_eEnrolment_Data($StudentID);
			$numOfECA_from_eEnrol = empty($eEnrolData)?0:count($eEnrolData);
			
			### F4-7 show csv data only
			### F1-3 show eEnrol data then ECA data
			//if ($FormNumber >= 4)
			//	$numOfECA = $numOfECA_fromCSV;
			//else
			//	$numOfECA = $numOfECA_fromCSV + $numOfECA_from_eEnrol; 
			// [CRM Ref No.: 2010-0610-1104] Disable the function about grep the enrolment records to lower Form until client migrate to IP2.5 completely. 
			$numOfECA = $numOfECA_fromCSV;

			$ECA .= "<table width='100%' border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='tabletext'>"; 
				//if(empty($eca))
				if ($numOfECA == 0)
				{
					$ECA .= "<tr><td>".$eReportCard['NIL']."</td></tr>";
				}
				else
				{
					### Show data from eEnrol For F1-3
					// [CRM Ref No.: 2010-0610-1104] Disable the function about grep the enrolment records to lower Form until client migrate to IP2.5 completely.
					/*
					if ($FormNumber < 4)
					{
						for($i=0; $i<$numOfECA_from_eEnrol; $i++)
						{
							$thisECATitle = $eEnrolData[$i]['ClubTitle'];
							$thisECARole = $eEnrolData[$i]['RoleTitle'];
							
							$ECA .= "<tr>";
								$ECA .= "<td>{$thisECATitle}</td><td>{$thisECARole}</td>";
							$ECA .= "</tr>";
						}
					}
					*/
					
					$eca = is_array($eca)?$eca:array($eca);
					$post = is_array($post)?$post:array($post);
					for($i=0; $i<$numOfECA_fromCSV; $i++)
					{
						$ECA .= "<tr valign='top'>";
							$ECA .= "<td width='150px'>{$eca[$i]}</td><td>{$post[$i]}</td>";
						$ECA .= "</tr>";
					}
				}
			$ECA .= "</table>";
		
			if ($ReportType =='W')
			{
				$thisClass = 'Remarks_Consolidate';
				$divwidth = " style='width:98%'";
			}
			else
				$thisClass = 'Remarks';
			$x = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=' $thisClass tabletext'>";
				$x .= "<tr>";
					if($ReportType =='W')
					{
						$tablewidth = " width='445px' ";
						$x .= "<td align='center' $tablewidth>";
							$x .= "<div $divwidth>";
								$x .= $ECA;
							$x .= "</div>";
						$x .= "</td>";
					}
					$x .= "<td align='center'>";
						$x .= "<div $divwidth>";
							$x .= stripslashes(nl2br(nl2br($classteachercomment)));
						$x .= "</div>";
					$x .= "</td>";
				$x .= "</tr>";
			$x .= "</table>";
		}
		else
		{
			$x = "<br><br><br>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='tabletext'>";
				$x .= "<tr>";
					$x .= "<td colspan='2'>".$eReportCard['Template']['Remarks']."<br><br></td>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<td width='20px'>".$eReportCard['Template']['1st']."</td>";
					$x .= "<td>".$eReportCard['Template']['Grading']."</td>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<td>&nbsp;</td>";
					$x .= "<td>";
						$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='tabletext'>";
							$x .= "<tr>";
								$x .= "<td>".$eReportCard['Template']['Excellent']."</td>";
								$x .= "<td>".$eReportCard['Template']['Good']."</td>";
								$x .= "<td>".$eReportCard['Template']['Fair']."</td>";
								$x .= "<td>".$eReportCard['Template']['Pass']."</td>";
								$x .= "<td>".$eReportCard['Template']['Fail']."</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td colspan=5>".$eReportCard['Template']['NA']."<br><br></td>";
							$x .= "</tr>";
						$x .= "</table>";
					$x .= "</td>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<td valign='top'>".$eReportCard['Template']['2nd']."</td>";
					$x .= "<td>".$eReportCard['Template']['NoSignRequire']."</td>";
				$x .= "</tr>";
			$x .= "</table>"; 
			
			$x .= "<table width=\"52%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='tabletext' align='right' style='padding-top:115px;'>";
				$x .= "<tr>";
					$x .= "<td colspan='2' style='padding-bottom:5px'>".$eReportCard['Template']['GuardianSignatureCh']."</td>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<td width='42%'>".$eReportCard['Template']['GuardianSignatureEn']."</td>";
					$x .= "<td>_______________________________</td>";
				$x .= "</tr>";
			$x .= "</table>"; 
		}
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber					= $this->GET_FORM_NUMBER($ClassLevelID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
		
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ==0 || $ValueArr[$TermID][$InfoKey] != '' ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
					
				# display this term value
				$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# add empty symbol to 2nd term
			if($FormNumber == 7 || $FormNumber==6 )
				$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			
			$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>&nbsp;</td>";
			$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>&nbsp;</td>";
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] == 0 || $ValueArr[$SemID][$InfoKey] != '' ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$ClassLevelID = $eRCTemplateSetting["ClassLevelID"];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$x .= "<tr class='footer_row'>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
			if($ReportType == 'T')
			{
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				if($FormNumber>=4)
					$thisValue = $this->EmptySymbol; 
				else
					$thisValue = $OverallValue;
					
				if ($thisValue==-1)
					$thisValue = $this->EmptySymbol; 
				$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
			}
			else
			{
				if($FormNumber >=4)
				{
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
					# add empty symbol to 2nd term
					if($FormNumber == 7 || $FormNumber==6 )
						$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
				else
				{
					foreach((array)$ValueArr as $SemOverall)
					{
						if ($SemOverall==-1)
							$SemOverall = $this->EmptySymbol; 
							
						$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$SemOverall."</td>";
					}
					
					if ($OverallValue==-1)
							$OverallValue = $this->EmptySymbol; 
							
//					$x .= "<td class='tabletext' align='center' height='{$LineHeight}'><div align='right' style=' width: 30%;'>".$OverallValue."</div></td>";
					$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>".$OverallValue."</td>";
				}
				$x .= "<td class='tabletext' align='center' height='{$LineHeight}'>&nbsp;</td>";
			}
			
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
}
?>