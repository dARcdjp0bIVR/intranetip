<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/chinese_ymca_secondary.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "award");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "-";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='910px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$isAnnual = false;
			
			if($SemID=='F')
			{
				$TermTitleEn = $eReportCard['Template']['AnnualTitleEn'];
				$TermTitleCh = $eReportCard['Template']['AnnualTitleCh'];
				$isAnnual = true;
			}
			else
			{
				$SemesterNumber = $this-> Get_Semester_Seq_Number($SemID);	
			}
			
			$TermTitle = '';	
			if($SemesterNumber==1)
			{
				$TermTitleEn = $eReportCard['Template']['FirstTermEn'];
				$TermTitleCh = $eReportCard['Template']['FirstTermCh'];
				$isAnnual = false;
			}	
			else if($SemesterNumber==2)
			{
				$TermTitleEn = $eReportCard['Template']['AnnualTitleEn'];
				$TermTitleCh = $eReportCard['Template']['AnnualTitleCh'];
				$isAnnual = true;
			}
			
			$YearName = $this->GET_ACTIVE_YEAR_NAME();
			
			preg_match_all('/([\w]+)|(.)/u', $TermTitleCh, $TermTitleChArr); // used to split Chinese Character
			
			if(is_array($TermTitleChArr))
			{
				$TermTitleChArr = current($TermTitleChArr);
				
				$space_html = '&nbsp;';
				
				if($isAnnual)
				{
					$space_html = '&nbsp;&nbsp;&nbsp;';
				}
				
				$TermTitleCh = implode($space_html,(array)$TermTitleChArr);
			}
		
	
			$TitleTable = '';
			$TitleTable .=$this->Get_Empty_Image_Div('42px');
			$TitleTable .= "<table class='bold' width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
								<col width='50%'/>
								<col width='50%'/>		
								<tr>
									<td class='text_14px' align='right'>".$eReportCard['Template']['SchoolNameCh']."</td>
									<td class='text_14px' align='left'>".strtoupper($eReportCard['Template']['SchoolNameEn'])."</td>
								</tr>
								<tr>
									<td class='text_14px' align='right'>".$eReportCard['Template']['AcademicReportCh']."</td>
									<td class='text_14px' align='left'>".strtoupper($eReportCard['Template']['AcademicReportEn'])."</td>
								</tr>
								<tr>
									<td  align='center' colspan='2' class='text_16px'>".$YearName."</td>
								</tr>
								<tr>
									<td class='text_14px' align='right'>".$TermTitleCh."</td>
									<td class='text_14px' align='left'>".$TermTitleEn."</td>
								</tr>
							</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';

			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$DOI = $ReportSetting['Issued'];
			if (date($DOI)==0) {
				$data['DateOfIssue']  =  ($StudentID=='')? "XXX" : $this->EmptySymbol;
			} else {
				$data['DateOfIssue']  = date('d/m/Y',strtotime($DOI));
			} 
			
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->ChineseName;
				$data['EnglishName'] = $lu->EnglishName;

				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				$thisClassID = $StudentInfoArr[0]['ClassID'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['RegNo'] = str_replace("#", "", $lu->WebSamsRegNo);
				$DOB= $lu->DateOfBirth;
				if (date($DOB)==0) {
					$data['DateOfBirth']  = $this->EmptySymbol;
				} else {
					$data['DateOfBirth']  = date('d/m/Y',strtotime($DOB));
				} 
				
				
				$data['STRN'] = $lu->STRN;
				
				$data['Gender'] = $lu->Gender;
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			
			}
			
			$StudentTitleArray = array("Name", "Gender", "Class",  "STRN",  "DateOfBirth", "ClassNo","RegNo", "DateOfIssue");

			$count = 0;
			$StudentInfoTable .= "<table class='border_table' width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>";
			for($i=0,$i_MAX=count($StudentTitleArray); $i<$i_MAX; $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);

				$TitleEn = $eReportCard['Template']['StudentInfo'][$SettingID.'En'];
				$TitleCh = $eReportCard['Template']['StudentInfo'][$SettingID.'Ch'];
				
				$TitleChWidth = '8%';
				$TitleEnWidth = '7%';
				$ValueWidth = '11%';
				$border_right = '';
				if($count % $StudentInfoTableCol == 0) {
					$StudentInfoTable .= "<tr>";
					$border_right = 'border_right';
				}
				else if(($count+1)%$StudentInfoTableCol==0) {
					$TitleEnWidth = '17%';
					$ValueWidth = '5%';
				}			
				else {
					$border_right = 'border_right';
				}
				
				$border_bottom = 'border_bottom';
				if ($i >= $i_MAX - $StudentInfoTableCol) {
					$border_bottom = '';
				}
				
				if ($SettingID=='Name')
				{			
					$StudentInfoTable .= "<td class='text_10px bold colorGray border_bottom' width='$TitleChWidth' valign='bottom' height='{$LineHeight}' nowrap>".$TitleCh."</td>";
					$StudentInfoTable .= "<td class='text_10px bold colorGray  border_right border_bottom' width='$TitleEnWidth' valign='top' height='{$LineHeight}' nowrap>".$TitleEn."</td>";
					$StudentInfoTable .= "<td class='text_10px bold border_bottom' width='$ValueWidth' valign='top' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
					$StudentInfoTable .= "<td class='text_10px bold border_bottom border_right' colspan='3' valign='top' height='{$LineHeight}' nowrap>".$data['EnglishName']."</td>";
					$count++;
				}
				else
				{
					$StudentInfoTable .= "<td class='text_10px bold colorGray ".$border_bottom."' width='$TitleChWidth' valign='bottom' height='{$LineHeight}' nowrap>".$TitleCh."</td>";
					$StudentInfoTable .= "<td class='text_10px bold colorGray  border_right ".$border_bottom."' width='$TitleEnWidth' valign='top' height='{$LineHeight}' nowrap>".$TitleEn."</td>";
					$StudentInfoTable .= "<td class='text_10px bold ".$border_right." ".$border_bottom."' width='$ValueWidth' valign='top' height='{$LineHeight}' nowrap>".($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
				}
					
				if(($count+1)%$StudentInfoTableCol==0) {
					$StudentInfoTable .= "</tr>";
				}
				$count++;
				
			}
			$StudentInfoTable .= "</table>";
			
		}
		
		$StudentInfoTable .= $this->Get_Empty_Image_Div('10px');
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# define 	
		$ColHeader = $this->genMSTableColHeader($ReportID);					
		
		# retrieve Marks html
		$MSTableReturned = $this->genMSTableMarks($ReportID, $StudentID);


		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='90%' height='684px' border='0' cellspacing='0' cellpadding='3' class='border_table'  align='center'>";
		 
		$DetailsTable .="<col width='16%'/>
						 <col width='5%'/>
						 <col width='11%'/>
						 <col width='16%'/>
						 <col width='7%'/>
						 <col width='9%'/>
						 <col width='9%'/>
						 <col width='9%'/>
						 <col width='9%'/>
						 <col width='9%'/>
						";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$DetailsTable .=$MSTableReturned;
		
		
		$DetailsTable .= "</table>";
		
		$DetailsTable .="<br/><br/>";
		
		$DetailsTable .=$this->getSignaturePart($ReportID,$StudentID);
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;

        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=1);
        $ActiveYearID = $this->GET_ACTIVE_YEAR_ID();

		$x = '';
		$x .= "<tr>";

			# Subject
			$x .= "<td rowspan='2' colspan='2' valign='middle' class='text_14px colorGray bold' height='{$LineHeight}' style='padding-left:7px;' >". $eReportCard['Template']['SubjectChn'] . "</td>";
			$x .= "<td rowspan='2' colspan='3' valign='middle' class='text_14px colorGray bold' height='{$LineHeight}' >". $eReportCard['Template']['SubjectEng'] . "</td>";

			# Term Report
			$thisWidth = '12%';

			// first term
			$thisDisplay = $eReportCard['Template']['FirstTermCh'].'<br />'.$eReportCard['Template']['FirstTermEn'];
			$x .= "<td colspan='2' class='text_13px bold border_left colorGray' align='center' valign='middle' height='{$LineHeight}' >". $thisDisplay . "</td>";
			
			// second term
			$thisDisplay = $eReportCard['Template']['SecondTermCh'].'<br />'.$eReportCard['Template']['SecondTermEn'];
			$x .= "<td colspan='2' class='text_13px bold border_left colorGray' align='center' valign='middle' height='{$LineHeight}' >". $thisDisplay . "</td>";
	
			// annual
			$thisDisplay = $eReportCard['Template']['AnnualCh'].'<br />'.$eReportCard['Template']['AnnualEn'];
			$x .= "<td  rowspan='2' class='text_13px bold border_left colorGray' align='center' valign='middle' height='{$LineHeight}'>". $thisDisplay . "</td>";
					
		$x .= "</tr>";
		
		$x .= "<tr>";

			// first term
			$thisDisplay = $eReportCard['Template']['ContinousAssessmentCh'].'<br />'.$eReportCard['Template']['ContinousAssessmentEn'];
			$x .= "<td class='text_9px bold border_left colorGray border_top' align='center' valign='middle' height='{$LineHeight}' >". $thisDisplay . "</td>";
			$thisDisplay = $eReportCard['Template']['ExaminationCh'].'<br />'.$eReportCard['Template']['ExaminationEn'];
			if($ActiveYearID == 11 && $FormNumber == 6) {
                $thisDisplay = $eReportCard['Template']['TestCh'].'<br />'.$eReportCard['Template']['TestEn'];
                $colStyle = " style='min-width: 53.8px;' ";
            }
			$x .= "<td class='text_9px bold border_left colorGray border_top' align='center' valign='middle' height='{$LineHeight}' {$colStyle} >". $thisDisplay . "</td>";
			
			// second term
			$thisDisplay = $eReportCard['Template']['ContinousAssessmentCh'].'<br />'.$eReportCard['Template']['ContinousAssessmentEn'];
			$x .= "<td class='text_9px bold border_left colorGray border_top' align='center' valign='middle' height='{$LineHeight}'>". $thisDisplay . "</td>";
			$thisDisplay = $eReportCard['Template']['ExaminationCh'].'<br />'.$eReportCard['Template']['ExaminationEn'];
            if($ActiveYearID == 11 && $FormNumber == 6) {
                $thisDisplay = $eReportCard['Template']['TestCh'].'<br />'.$eReportCard['Template']['TestEn'];
            }
			$x .= "<td class='text_9px bold border_left colorGray border_top' align='center' valign='middle' height='{$LineHeight}' {$colStyle} >". $thisDisplay . "</td>";

		$x .= "</tr>";

		return $x;
	}
	
	function getSignatureTable($ReportID='',$StudentID='')
	{	
	//	$SignatureTable = $this->getSignaturePart($ReportID,$StudentID);
		
	//	return $SignatureTable;
	}
	
	function getSignaturePart($ReportID='',$StudentID='')
	{
		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
		
		### Class Teacher Name
		$ClassTeacherInfoArr = $this->Get_Student_Class_Teacher_Info($StudentID);		
		$ClassTeacherNameArr =array();		
		foreach($ClassTeacherInfoArr as $key=>$valueArr)
		{
			$thisTeacherNameString="";
			$thisTeacherNameString=$valueArr["ChineseName"]."<br/>".$valueArr["EnglishName"];
			$ClassTeacherNameArr[]=$thisTeacherNameString;
		}
		$ClassTeacherNameOne = ($ClassTeacherNameArr[0]=='')? $this->EmptySymbol :$ClassTeacherNameArr[0];
		$ClassTeacherNameTwo = ($ClassTeacherNameArr[1]=='')? $this->EmptySymbol :$ClassTeacherNameArr[1];
		
		$ClassTeacherNameDisplay = implode('<br />', Get_Array_By_Key($ClassTeacherInfoArr, 'EnglishName'));
		$ClassTeacherNameDisplay = ($ClassTeacherNameDisplay=='')? '&nbsp;' : $ClassTeacherNameDisplay;
		
		
		### Principal
//		$PrincipalArr = $this->Get_Principal_Info();
//		$PrincipalNameArr =array();		
//		foreach($PrincipalArr as $key=>$valueArr)
//		{
//			$thisPrincipalString="";
//			$thisPrincipalString=$valueArr["ChineseName"]."<br/>".$valueArr["EnglishName"];
//			$PrincipalNameArr[]=$thisPrincipalString;
//		}
//		$PrincipalDisplay = ($PrincipalNameArr[0]=='')? $this->EmptySymbol : $PrincipalNameArr[0];
		$PrincipalDisplay = 'K. W. Dennis Chan';

		// 2015-0604-1552-39071
		$padding_style_width = '19px';
		
		$SignatureTable = "";
		$SignatureTable .='<table class="border_table" width="90%" border="0" cellpadding="4" cellspacing="0" align="center">
							  <col width="35%"/>
							  <col width="33%"/>
							  <col width="32%"/>
							  <tr>
									<td class="bold text_11px border_right colorGray" style="text-align:center;">'.$eReportCard['Template']['Signature']['NameOfClassTeacherCh'].' '.$eReportCard['Template']['Signature']['NameOfClassTeacherEn'].'</td>
									<td class="bold text_11px border_right colorGray" style="text-align:center;">'.$eReportCard['Template']['Signature']['PrincipalNameCh'].' '.$eReportCard['Template']['Signature']['PrincipalNameEn'].'</td>
									<td class="bold text_11px colorGray" style="text-align:center;">'.$eReportCard['Template']['Signature']['PrincipalSignCh'].' '.$eReportCard['Template']['Signature']['PrincipalSignEn'].'</td>
							  </tr>
							   <tr>
									<!--td class="bold text_12px border_right border_top" style="text-align:center;height:66px;">'.$ClassTeacherNameDisplay.'</td-->
									<td class="bold text_12px border_right border_top" style="height:66px; padding-left:'.$padding_style_width.';">'.$ClassTeacherNameDisplay.'</td>
									<!--td class="bold text_12px border_right border_top" style="text-align:center;height:66px;">'.$ClassTeacherNameOne.'</td-->
									<!--td class="bold text_12px border_right border_top" style="text-align:center;">'.$ClassTeacherNameTwo.'</td-->
									<td class="bold text_12px border_right border_top" style="text-align:center;">'.$PrincipalDisplay.'</td>
									<td class="bold text_12px border_top">&nbsp;</td>
							  </tr>
						   </table>';
		
		return $SignatureTable;
	}
	
	function genMSTableMarks($ReportID,  $StudentID='')
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$mainYearTermID 	= $ReportSetting['Semester'];
 		$ReportType 		= ($mainYearTermID == "F") ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$IsMainReport 		= $ReportSetting['isMainReport'];
		$AddStarPercentage 	= $ReportSetting['SpecialPercentage'];
		
		$SemID = $ReportSetting['Semester'];
		$otherInfoSemId = $this->Get_Last_Semester_Of_Report($ReportID);
	
		$SubjectWeightDataArr = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID=0);
		$SubjectWeightDataAssoArr = BuildMultiKeyAssoc($SubjectWeightDataArr, 'SubjectID', array('Weight'), $SingleValue=1, $BuildNumericArray=0);

		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=1);
		
		### Get subject full mark
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 1, $ReportID);	
	
		### Get All CSV Data
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$otherInfoSemId];
		
		
//debug_r($OtherInfoDataArr);		
		$IntegrityStr = $OtherInfoDataArr['Integrity'];
		$ConfidenceStr = $OtherInfoDataArr['Confidence'];
		$HumilityStr = $OtherInfoDataArr['Humility'];
		$GratitudeStr = $OtherInfoDataArr['Gratitude'];
		$AttendanceStr = $OtherInfoDataArr['Attendance (%)'];
		$PunctualityStr = $OtherInfoDataArr['Punctuality (%)'];
		$AwardMeritsArr = $OtherInfoDataArr['Awards/Merits/Duties'];

		if(is_array($AwardMeritsArr)) {
			//$AwardMeritsArr = current($AwardMeritsArr);
			$AwardMeritsStr = implode('<br/>', (array)$AwardMeritsArr);		
		}
		else {
			$AwardMeritsStr = $AwardMeritsArr;
		}
		$AwardMeritsStr = nl2br($AwardMeritsStr);

		$IntegrityStr = ($IntegrityStr=='')?$this->EmptySymbol:$IntegrityStr;
		$ConfidenceStr = ($ConfidenceStr=='')?$this->EmptySymbol:$ConfidenceStr;
		$HumilityStr = ($HumilityStr=='')?$this->EmptySymbol:$HumilityStr;
		$GratitudeStr = ($GratitudeStr=='')?$this->EmptySymbol:$GratitudeStr;
		$AttendanceStr = ($AttendanceStr=='')?$this->EmptySymbol:$AttendanceStr;
		$PunctualityStr = ($PunctualityStr=='')?$this->EmptySymbol:$PunctualityStr;
		$AwardMeritsStr = ($AwardMeritsStr=='')?$this->EmptySymbol:$AwardMeritsStr;
	
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);

		
		### Get Subject Statistics Info   
		$MainSubjectInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$MainSubjectInfoArrChi = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='ch', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		
		$AllReportInfoArr = array();
		if ($ReportType == 'T') {
			$AllReportInfoArr[0]['ReportID'] = $ReportID;
			$AllReportInfoArr[0]['YearTermID'] = $mainYearTermID;
			$thisColumnData = $this->returnReportTemplateColumnData($ReportID);	
			for($a=0,$a_MAX=count($thisColumnData);$a<$a_MAX;$a++)
			{
				$AllReportInfoArr[0]['ReportColumnID'][] = $thisColumnData[$a]['ReportColumnID'];
			}
			$SemTwoReportID = $ReportID;
		}
		else {
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					
			for($i=0,$i_MAX=count($ColumnData);$i<$i_MAX;$i++)
			{
				//	$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
				$thisReportColumnID = 0;
				
				$thisYearTermID = $ColumnData[$i]['SemesterNum'];
				$thisReport = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisYearTermID, $isMainReport=1);
				
				$thisColumnData = $this->returnReportTemplateColumnData($thisReport['ReportID']);	
//debug_r($thisColumnData);				
				$AllReportInfoArr[$i]['ReportID'] = $thisReport['ReportID'];
				$AllReportInfoArr[$i]['YearTermID'] = $thisYearTermID;
				
				for($a=0,$a_MAX=count($thisColumnData);$a<$a_MAX;$a++)
				{
					$AllReportInfoArr[$i]['ReportColumnID'][] = $thisColumnData[$a]['ReportColumnID'];
				}
				
	
				if($i==1)
				{
					$SemTwoReportID =$thisReport['ReportID'];
				}
				else if($i==0)
				{
					$SemOneReportID =$thisReport['ReportID'];
				}
			}
			
			$numOfReport = count($AllReportInfoArr);
			$AllReportInfoArr[$numOfReport]['ReportID'] = $ReportID;
			$AllReportInfoArr[$numOfReport]['YearTermID'] = 0;
			$AllReportInfoArr[$numOfReport]['ReportColumnID'][]=0;
		}


		$numOfReport = count($AllReportInfoArr);
		
		### Get Data
		$ReportIDArr = Get_Array_By_Key($AllReportInfoArr,'ReportID');

		$thisClassTeacherCommentArr=array();
		
		for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++) {
			
			$thisReportID = $AllReportInfoArr[$i]['ReportID'];
			$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
		
			$thisGrandMarkArr = $this->getReportResultScore($thisReportID, 0, $StudentID);	
					
			### Get Student Average Marks and Position
			$MarksAry[$thisReportID] = $this->getMarks($thisReportID, $StudentID,"","",1);
			
			### Get Class Teacher Comment	
			
			if($thisYearTermID!=0)
			{
				$thisCommentArr = $this->returnSubjectTeacherCommentByBatch($thisReportID, $StudentID);
				$thisClassTeacherCommentArr[$thisReportID] = nl2br(trim($thisCommentArr[0][$StudentID]['Comment']));
				$thisClassTeacherCommentArr[$thisReportID] = ($thisClassTeacherCommentArr[$thisReportID]=='')?$this->EmptySymbol:$thisClassTeacherCommentArr[$thisReportID];
			}
					
		}
		
		
		### Reorder the Subject Display Order
		$NewSubjectSequenceArr = array();		
		$PercentailArr = array();
		$SubjectObjArr = array(); 
	
		foreach((array)$MainSubjectInfoArr as $thisParentSubjectID => $thisSubjectInfoArr)
		{

			foreach((array)$thisSubjectInfoArr as $thisSubjectID => $thisSubjectName)
			{ 
				if ($thisSubjectID==0) {
					$thisIsComponentSubject = false;
					$thisSubjectID = $thisParentSubjectID;
				}
				else {
					$thisIsComponentSubject = true;
					 continue;  // if you do not want to show the component subject, just uncomment it.
				}
				
				$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
				$thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];
				
				for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++) {
			
					$thisReportID = $AllReportInfoArr[$i]['ReportID'];
					$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
					
					$thisColumnDataArr = $AllReportInfoArr[$i]['ReportColumnID'];
			
					for($a=0,$a_MAX=count($thisColumnDataArr);$a<$a_MAX;$a++)
					{
						$ReportColumnID = $thisColumnDataArr[$a];					
						$thisMSGrade = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]["Grade"];
						$thisMSMark = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]["Mark"]; 
						$thisFormRanking = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]["OrderMeritForm"];
						$thisFormNumOfStudent = $MarksAry[$thisReportID][$thisSubjectID][$ReportColumnID]["FormNoOfStudent"];
	
						$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
	
						$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($StudentID) {
							
							//2015-0702-1357-27073
							//if ($thisMark == 'N.A.') {
							if ($thisMark == 'N.A.' || $thisMark == '') {
								continue;
							}	
	
			
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMSGrade);
							
							// 2012-0113-0955-41071 dispaly A* for top x% of the form
							$thisMarkOriginal = $thisMark;
							if ($thisFormNumOfStudent == 0) {
								$thisRankPercentage = 0;
							}
							else {
								$thisRankPercentage = ($thisFormRanking / $thisFormNumOfStudent) * 100;
							}
							if ($AddStarPercentage > 0 && $thisMark=='A' && floatcmp((float)$thisRankPercentage, '<=', (float)$AddStarPercentage)) {
								$thisMark = 'A*';
							}
							
							if($needStyle) {
								$thisNatureDetermineScore = ($thisScaleDisplay=='G')? $thisMarkOriginal : $thisMSMark;
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisNatureDetermineScore);
							}
							else {
								$thisMarkDisplay = $thisMark;
							}						
						}
						else {
							$thisMarkDisplay = $this->EmptySymbol;
						}					
			
						$NewSubjectSequenceArr[$thisSubjectID]['thisIsComponentSubject']=$thisIsComponentSubject;
						if($thisIsComponentSubject)
						{										
							$NewSubjectSequenceArr[$thisSubjectID][$thisReportID][$ReportColumnID]['Mark'] = $this->LongSpace.$thisMarkDisplay;
						}
						else
						{	
							$NewSubjectSequenceArr[$thisSubjectID][$thisReportID][$ReportColumnID]['Mark']= $thisMarkDisplay;
						}
						
						if($thisIsComponentSubject)
						{
							$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $this->LongSpace.$MainSubjectInfoArr[$thisParentSubjectID][$thisSubjectID];
							$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $this->LongSpace.$MainSubjectInfoArrChi[$thisParentSubjectID][$thisSubjectID];
							
							if (strlen($MainSubjectInfoArr[$thisParentSubjectID][$thisSubjectID]) > 40) {
								$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = '<span class="text_9px">'.$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'].'</span>';
							}
							else {
								$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = '<span class="text_10px">'.$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'].'</span>';
							}					
						}
						else
						{						
							$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $this->ShortSpace.$MainSubjectInfoArr[$thisSubjectID][0];
							$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $this->ShortSpace.$MainSubjectInfoArrChi[$thisSubjectID][0];
							
							if (strlen($MainSubjectInfoArr[$thisSubjectID][0]) > 40) {
								$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = '<span class="text_9px">'.$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'].'</span>';
							}
							else {
								$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = '<span class="text_10px">'.$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'].'</span>';
							}
						}
					}
				
				}

			}
		}

		if($SemID=='F')
		{
		}
		else
		{
			$SemesterNumber = $this-> Get_Semester_Seq_Number($SemID);	
		}		

		### Comment
//		$CommentArr =  $this->GET_TEACHER_COMMENT($StudentID, $SubjectIDArr='', $ReportID) ;
//		$CommentStr = $CommentArr[$StudentID]['Comment'];
//		$CommentStr = ($CommentStr=='')? $this->EmptySymbol:$CommentStr;
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$CommentStr = nl2br(trim($CommentAry[0]));
		$CommentStr = ($CommentStr=='')? $this->EmptySymbol:$CommentStr;



		$padding_style_width = '7px';
		$shortSpace = '&nbsp;&nbsp;&nbsp;&nbsp;';
		
		### Here are the "Subject", and "Marks" rows
			
		$IsSetBorderTop=false;
		
		$count_print=0;
		$j=0;
		foreach ((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr) 
		{
			$count_print=0;
			$border_top = 'border_top';
			if($j!=0)
			{
				$border_top = 'border_top_dot';
			}

			$x .= '<tr style="text-align:center; height:20px;">'."\n";
								
				$x .= '<td colspan="2" class="'.$border_top.' bold text_10px" style="text-align:left; padding-left:'.$padding_style_width.';">'.$thisSubjectInfoArr['SubjectNameCh'].'</td>'."\n";
				$x .= '<td colspan="3" class=" bold text_10px '.$border_top.'" style="text-align:left;">'.$thisSubjectInfoArr['SubjectNameEn'].'</td>'."\n";	
	
				if($StudentID=='')
				{
					for ($k=0,$k_MAX=5; $k<$k_MAX; $k++)
					{
						$x .= '<td class="border_left '.$border_top.'">'.$this->EmptySymbol.'</td>'."\n";	
						$count_print++;
					}				
				}
				else
				{
					if($SemesterNumber==2)
					{
						for ($k=0,$k_MAX=2; $k<$k_MAX; $k++)
						{
							$x .= '<td class="border_left '.$border_top.'">'.$this->EmptySymbol.'</td>'."\n";	
							$count_print++;
						}
					}

					for ($i=0,$i_MAX=count($AllReportInfoArr); $i<$i_MAX; $i++)
					{		
						$thisReportID = $AllReportInfoArr[$i]['ReportID'];
						$thisYearTermID = $AllReportInfoArr[$i]['YearTermID'];
								
						$thisColumnDataArr = $AllReportInfoArr[$i]['ReportColumnID'];
	
						if($i==2)
						{
							$thisMaxCount = 1;
						}
						else
						{
							$thisMaxCount = 2;
						}

				//		for($a=0,$a_MAX=count($thisColumnDataArr);$a<$a_MAX;$a++)
						for($a=0,$a_MAX=$thisMaxCount;$a<$a_MAX;$a++)
						{
							$ReportColumnID = $thisColumnDataArr[$a];
							
							$thisDisplay = $thisSubjectInfoArr[$thisReportID][$ReportColumnID]['Mark'];
							$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];					
							
							$thisLongSpace = '';
							if($thisIsComponentSubject)
							{
								$thisLongSpace = $this->LongSpace;
							}
							
							$thisDisplay = ($thisDisplay=='')? $thisLongSpace.$this->EmptySymbol : $thisDisplay;	
																		
							$x .= '<td class="border_left '.$border_top.'"  style="padding-top:'.$padding_style_width.';padding-bottom:'.$padding_style_width.';">'.$thisDisplay.'</td>'."\n";	
							$count_print++;
						}	
					}
					
					if($SemesterNumber==1)
					{
						for ($k=0,$k_MAX=2; $k<$k_MAX; $k++)
						{
							$x .= '<td class="border_left '.$border_top.'">'.$this->EmptySymbol.'</td>'."\n";	
							$count_print++;
						}
					}
					
					if($SemID!='F')
					{
						$x .= '<td class="border_left '.$border_top.'">'.$this->EmptySymbol.'</td>'."\n";	
						$count_print++;				
					}
			
					if($count_print<5)
					{
						$count_remain = 5-$count_print;
						for($kk=0;$kk<$count_remain;$kk++)
						{
							$x .= '<td class="border_left '.$border_top.'">'.$this->EmptySymbol.'</td>'."\n";
						}
										
					}
				}
				
			$x .= '</tr>'."\n";					
			$j++;
		}
		### End the "Subject", and "Marks" rows

		### CSV
		// Personal Development		
		$x .= ' <tr>
					<td colspan="6" class="border_top colorGray bold text_12px" style="padding-left:'.$padding_style_width.';">'.$eReportCard['Template']['PersonalDevelopmentCh'] .$shortSpace.$eReportCard['Template']['PersonalDevelopmentEn'] .'</td>
					<td colspan="2" rowspan="2" class="border_top border_left colorGray bold text_12px" style="text-align:center;">'.$eReportCard['Template']['AttendanceCh'] .'<br/>'.$eReportCard['Template']['AttendanceEn'] .'</td>	
					<td colspan="2" rowspan="2" class="border_top border_left colorGray bold text_12px" style="text-align:center;">'.$eReportCard['Template']['PunctualityCh'] .'<br/>'.$eReportCard['Template']['PunctualityEn'] .'</td>
				</tr>
				<tr>
					<td class="border_top bold text_12px colorGray" style="text-align:center;">'.$eReportCard['Template']['IntegrityCh'] .'<br/>'.$eReportCard['Template']['IntegrityEn'] .'</td>
					<td class="border_top border_left bold text_12px colorGray" style="text-align:center;" colspan="2">'.$eReportCard['Template']['ConfidenceCh'] .'<br/>'.$eReportCard['Template']['ConfidenceEn'] .'</td>
					<td class="border_top border_left bold text_12px colorGray" style="text-align:center;">'.$eReportCard['Template']['HumilityCh'] .'<br/>'.$eReportCard['Template']['HumilityEn'] .'</td>
					<td class="border_top border_left bold text_12px colorGray" colspan="2" style="text-align:center;">'.$eReportCard['Template']['GratitudeCh'] .'<br/>'.$eReportCard['Template']['GratitudeEn'] .'</td>
				</tr>
				<tr>
					<td class="border_top bold text_10px" style="text-align:center;height:25px;">'.$IntegrityStr .'</td>
					<td class="border_top border_left bold text_10px" style="text-align:center;" colspan="2">'.$ConfidenceStr .'</td>
					<td class="border_top border_left bold text_10px" style="text-align:center;">'.$HumilityStr .'</td>
					<td class="border_top border_left bold text_10px" style="text-align:center;" colspan="2">'.$GratitudeStr .'</td>
					<td class="border_top border_left bold text_10px" style="text-align:center;" colspan="2">'.$AttendanceStr .'</td>	
					<td class="border_top border_left bold text_10px" style="text-align:center;" colspan="2">'.$PunctualityStr .'</td>
				</tr>
					
				<tr>	
					<td colspan="10" class="border_top colorGray bold" style="padding-left:'.$padding_style_width.';">'.$eReportCard['Template']['AwardsCh'].$shortSpace.$eReportCard['Template']['AwardsEn']  .'</td>
				</tr>
				<tr>	
					<td colspan="10" class="border_top" style="width:100%;padding-left:'.$padding_style_width.';padding-right:'.$padding_style_width.';padding-top:'.$padding_style_width.';padding-bottom:'.$padding_style_width.';">'.$AwardMeritsStr.'</td>					
				</tr>
				<tr>	
					<td colspan="10" class="border_top colorGray bold" style="padding-left:'.$padding_style_width.';">'.$eReportCard['Template']['CommentsCh'].$shortSpace.$eReportCard['Template']['CommentsEn']  .'</td>
				</tr>
				<tr>	
					<td colspan="10" class="border_top" style="width:100%; height:100%; vertical-align:middle; padding-left:19px; padding-right:19px; padding-top:19px; padding-bottom:19px;"><div style="text-align:justify;">'.$CommentStr.'</div></td>					
				</tr>
				<tr>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
					 <td></td>
				</tr>
			 ';

		
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{

	}
	
	########### END Template Related
	
}
?>