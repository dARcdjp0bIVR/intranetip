<?php
# Editing by Marcus

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/kwai_chung_methodist.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "merit");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->PersonalCharGradeArr['10'] = 'NotApplicable';
		$this->PersonalCharGradeArr['20'] = 'NeedsImprovement';
		$this->PersonalCharGradeArr['30'] = 'Satisfactory';
		$this->PersonalCharGradeArr['40'] = 'Good';
		$this->PersonalCharGradeArr['50'] = 'Excellent';
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard, $ReportSetting ;
		
		$IsMainReport = $ReportSetting['isMainReport'];
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr valign='top'><td><div class='TitleTable'>".$TitleTable."</div></td></tr>";
		$TableTop .= "<tr valign='top'><td><div class='StudentInfoTable'>".$StudentInfoTable."</div></td></tr>";
		$TableTop .= "<tr valign='top'><td><div class='MSTable'>".$MSTable."</div></td></tr>";
		if($IsMainReport)
		{
			$TableTop .= "<tr valign='top'><td><div class='MiscTable'>".$MiscTable."</div></td></tr>";
		}
		$TableTop .= "</table>";

		if($IsMainReport)
		{
			$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
			$TableBottom .= $FooterRow;
			$TableBottom .= "</table>";
		}
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table height='1068px' width='710px' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard,$PATH_WRT_ROOT;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = explode(":_:", $ReportTitle);
			$IsMainReport = $ReportSetting['isMainReport'];
			
			# get school badge
			// [2015-1207-1507-05164] Show Logo for Extra Term Report only
//			$SchoolLogo = GET_SCHOOL_BADGE();
			if(!$IsMainReport){
				$SchoolLogo = "<img src ='".$PATH_WRT_ROOT."file/reportcard2008/templates/kwai_chung_methodist.jpg'>";
			}
			
			# get school name
//			$SchoolName = GET_SCHOOL_NAME();	
			$SchoolNameEn = $eReportCard['SchoolNameEn'];
			$SchoolNameCh = $eReportCard['SchoolNameCh'];
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			if($IsMainReport)
			{
				$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
				$TitleTable .= "<tr valign='top'><td width='120'><div class='SchoolLogo'>".$TempLogo."</div></td>";
				
				if(!empty($ReportTitle) || !empty($SchoolName))
				{
					$TitleTable .= "<td>";
					if ($HeaderHeight == -1) {
						$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
							$TitleTable .= "<tr><td nowrap='nowrap' align='center'><div class='SchoolNameEn'>".$SchoolNameEn."</div></td></tr>\n";
							$TitleTable .= "<tr><td nowrap='nowrap' align='center'><div class='SchoolNameCh'>".$SchoolNameCh."</div></td></tr>\n";
						if(!empty($ReportTitle[0]))
							$TitleTable .= "<tr><td nowrap='nowrap' align='center'><div class='ReportTitle1'>".$ReportTitle[0]."</div></td></tr>\n";
						if(!empty($ReportTitle[1]))
							$TitleTable .= "<tr><td nowrap='nowrap' align='center'><div class='ReportTitle2'>".$ReportTitle[1]."</div></td></tr>\n";
						$TitleTable .= "</table>\n";
					} else {
						for ($i = 0; $i < $HeaderHeight; $i++) {
							$TitleTable .= "<br/>";
						}
					}
					$TitleTable .= "</td>";
				}
				$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
				$TitleTable .= "</table>";
			}
			else
			{
				$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr>";
						$TitleTable .= "<td>";
							$TitleTable .= "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
								$TitleTable .= "<tr>";
									$TitleTable .= "<td width='10%'>";
										$TitleTable .= "<div class='ExtraSchoolLogo'>".$SchoolLogo."</div>";
									$TitleTable .= "</td>";
									$TitleTable .= "<td align='center'>";
										$TitleTable .= "<div class='SchoolNameCh'>".$SchoolNameCh."</div>";
										if(!empty($ReportTitle[0]))
											$TitleTable .= "<div class='SchoolNameCh'>".$ReportTitle[0]."</div>";
										if(!empty($ReportTitle[1]))
											$TitleTable .= "<div class='SchoolNameCh'>".$ReportTitle[1]."</div>";
									$TitleTable .= "</td>";
									$TitleTable .= "<td width='10%'>";
										$TitleTable .= "&nbsp;";
									$TitleTable .= "</td>";
								$TitleTable .= "</tr>"; 
							$TitleTable .= "</table>";	
						$TitleTable .= "</td>";
					$TitleTable .= "</tr>"; 
				$TitleTable .= "</table>";	
			}
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			$IsMainReport = $ReportSetting['isMainReport'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			
			$data['DateOfIssue'] = $this->FormatDate($ReportSetting['Issued'],"D/M/Y");
			$data['ExtraDateOfIssue'] = $this->FormatDate($ReportSetting['Issued'],"Y/M/D");
			
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 1);
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				if($IsMainReport)
				{
					if (is_array($SettingStudentInfo))
					{
						if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
							$data['Class'] .= " (".$thisClassNumber.")";
					}
					else
					{
						if ($thisClassNumber != "")
							$data['Class'] .= " (".$thisClassNumber.")";
					}
				}
				else
				{
					$data['Class'] = "$thisClassName (".$thisClassNumber.")";
				}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $eReportCard['Template']['StudentInfo']['GenderDisplay'][$lu->Gender];
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}

			
			if($IsMainReport)
			{
				if(!empty($SettingStudentInfo))
				{
					$count = 0;
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
					
					for($i=0; $i<$StudentInfoTableCol; $i++)
					{
						$StudentInfoTable.= "<col class='StudentInfoTitleChCol$i'>";
						$StudentInfoTable.= "<col class='StudentInfoTitleEnCol$i'>";
						$StudentInfoTable.= "<col class='StudentInfoSeparatorCol$i'>";
						$StudentInfoTable.= "<col class='StudentInfoDataCol$i'>";
						if(($i+1)%$StudentInfoTableCol>0) 
							$StudentInfoTable.= "<col class='StudentInfoSpacerCol$i'>";
					}
					
					for($i=0; $i<sizeof($StudentTitleArray); $i++)
					{
						$SettingID = trim($StudentTitleArray[$i]);
						if(in_array($SettingID, $SettingStudentInfo)===true)
						{
							$TitleEn = $eReportCard['Template']['StudentInfo'][$SettingID."En"];
							$TitleCh = $eReportCard['Template']['StudentInfo'][$SettingID."Ch"];
							if($count%$StudentInfoTableCol==0) {
								$StudentInfoTable .= "<tr>";
							}
							
	//						$colspan = $SettingID=="Name" ? " colspan='10' " : "";
	//						$StudentInfoTable .= "<td height='{$LineHeight}'><div class='StudentInfoTitleCh'>".$TitleCh."</div></td>";
	//						$StudentInfoTable .= "<td height='{$LineHeight}'><div class='StudentInfoTitleEn'>".$TitleEn."</div></td>";
	//						$StudentInfoTable .= "<td height='{$LineHeight}'><div class='StudentInfoSeparator'>:</div></td>";
	//						$StudentInfoTable .= "<td height='{$LineHeight}'><div class='StudentInfoData'>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</div></td>";
							$StudentInfoTable .= "<td ><div class='StudentInfoTitleCh'>".$TitleCh."</div></td>";
							$StudentInfoTable .= "<td ><div class='StudentInfoTitleEn'>".$TitleEn."</div></td>";
							$StudentInfoTable .= "<td ><div class='StudentInfoSeparator'>:</div></td>";
							$StudentInfoTable .= "<td ><div class='StudentInfoData'>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</div></td>";
								
							if(($count+1)%$StudentInfoTableCol==0) {
								$StudentInfoTable .= "</tr>";
							} else {
								$StudentInfoTable .= "<td><div class='StudentInfoSpacer'></div></td>";
							}
							$count++;
						}
					}
					$StudentInfoTable .= "</table>";
				}
			}
			else
			{
				$StudentInfoTable = "<div class='ExtraIssueDate'>".$data['ExtraDateOfIssue']."</div>";
				$StudentInfoTable .= "<table width='100%' cellpadding='2' cellspacing='0' border='0' class='ExtraStudentInfo'>";
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td width='50%'>";
							$StudentInfoTable .= $eReportCard['Template']['StudentInfo']["NameCh"]." : ".$data['Name'];	
						$StudentInfoTable .= "</td>";	
						$StudentInfoTable .= "<td width='50%'>";
							$StudentInfoTable .= $eReportCard['Template']['StudentInfo']["ClassCh"]." : ".$data['Class'];
						$StudentInfoTable .= "</td>";	
					$StudentInfoTable .= "</tr>";	
				$StudentInfoTable .= "</table>";
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		$SemID = $ReportSetting['Semester'];
		$IsMainReport  = $ReportSetting['isMainReport'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		$FirstGradeRow = $MSTableReturned['FirstGradeRow'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$ReportCss = $this->IsGraduateReport($ClassLevelID, $ReportType)?"G":$ReportType;
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border {$ReportCss}Report'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol-1;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	
				$isSub=1;
			if ($ShowSubjectComponent==0 && $isSub)
				continue;
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			if(!$IsMainReport)
				$valign = "valign='top'";
			
			# check if displaying subject row with all marks equal "N.A."
			if($isFirst)
				$DetailsTable .= "<tr>".$SubjectCol['FirstRow'].$MSTableReturned['FirstRow']."</tr>";
			
			# for form 1-3 template, show all subject which was selected in Subjects & Class Level Settings
			if ($eRCTemplateSetting['DisplayNA'] || ($this->Get_Form_Number($ClassLevelID)<=3 && $ReportType=="T") || $isAllNAAry[$thisSubjectID]==false)
			{
//				2011-0705-1028-17069 - 葵涌循道中學 - Improvement for the customization on eRC template
//				$FirstGradeRowClass = ($FirstGradeRow==$thisSubjectID)?"class='FirstGradeRow'":"";
					
				$DetailsTable .= "<tr $FirstGradeRowClass $valign>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment && $IsMainReport) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				
			}
			$isFirst = 0;
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		
		if($ReportType == 'W' && !$this->IsGraduateReport($ClassLevelID, $ReportType))
		{
			$DetailsTable = '<div class="WMSTableWrapper">'.$DetailsTable.'</div>';
		}
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$PercentageOnColumnWeight	= $ReportSetting['PercentageOnColumnWeight'];
		$LineHeight = $ReportSetting['LineHeight'];
		$IsMainReport = $ReportSetting['isMainReport'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if(!$IsMainReport)
		{
			$x .= "<td class='ExtraColSubTitle' align='center' width='60%'>".$eReportCard['ExtraTemplate']['Subject']."</td>";
			$x .= "<td class='border_left ExtraColSubTitle' align='center' width='20%'>".$eReportCard['ExtraTemplate']['FullMarkCh']."</td>";
			$x .= "<td class='border_left ExtraColSubTitle' align='center' width='20%'>".$eReportCard['ExtraTemplate']['MarkCh']."</td>";
		}
		else
		{
			if($ReportType=="T"||$this->IsGraduateReport($ClassLevelID, $ReportType))	# Terms Report Type
			{
	//			# Retrieve Invloved Assesment
	//			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
	//			$ColumnID = array();
	//			$ColumnTitle = array();
	//			if (sizeof($ColoumnTitle) > 0)
	//				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
	//				
	//			$e = 0;
	//			for($i=0;$i<sizeof($ColumnTitle);$i++)
	//			{
	//				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
	//				$ColumnTitleDisplay = $ColumnTitle[$i];
	//				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
	//				$n++;
	// 				$e++;
	//			}
			}
			else					# Whole Year Report Type
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				$SubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, " SubjectID IS NULL ");
				$SubjectWeightData = BuildMultiKeyAssoc($SubjectWeightData,"ReportColumnID","Weight",1);
				
				$needRowspan=0;
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$thisYearTermID = $ColumnData[$i]['SemesterNum'];
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					
					$SemNameEn = $this->returnSemesters($thisYearTermID, 'en');
					$SemNameCh = $this->returnSemesters($thisYearTermID, 'b5');
						
	//				if($isDetails==1)
	//				{
	//					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$thisYearTermID ." and ClassLevelID=".$ClassLevelID);
	//					$thisReportID = $thisReport['ReportID'];
	//					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
	//					$ColumnID = array();
	//					$ColumnTitle = array();
	//					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
	//					for($j=0;$j<sizeof($ColumnTitle);$j++)
	//					{
	//						//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
	//						$ColumnTitleDisplay =  $ColumnTitle[$j];
	//						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
	//						$n++;
	//						$e++;
	//					}
	//					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
	//					$Rowspan = "";
	//					$needRowspan++;
	//					$t[$i] = sizeof($ColumnTitle);
	//				}
	//				else
	//				{
	//					
	//					$colspan = "";
	//					$Rowspan = "rowspan='2'";
	//					$e++;
	//				}
					
					
					# Rank
	//				$row2 .= "<td class='border_top reportcard_text' align='center' height='{$LineHeight}'><div class='TermColSubTitle'>". $eReportCard['Template']['SubjectRank'] . "</div></td>";
					$thisWeight = $SubjectWeightData[$ColumnData[$i]['ReportColumnID']]*($SubjectWeightData[$ColumnData[$i]['ReportColumnID']]>1?1:100);
					$row2 .= "<td {$Rowspan}  height='{$LineHeight}' class='border_left border_top TermColSubTitle' align='center'>". $SemNameCh.'<br />'.$SemNameEn.'<br />('.$thisWeight."%)</td>";
					
					$n++;
					$e++;
				}
				
				$ResultColSpan = count($ColumnData) + ($ShowRightestColumn?1:0);
				# Mark
				$row1 .= "<td class=' border_left reportcard_text ResultColTitle' align='center' height='{$LineHeight}' colspan='$ResultColSpan'><div class='TermColTitle'>". $eReportCard['Template']['Mark'] . "</div></td>";
				
			}
			############## Marks END
			#########################################################
			$Rowspan = $row2 ? "rowspan='2'" : "";
	
			if(!$needRowspan)
				$row1 = str_replace("rowspan='2'", "", $row1);
			
			$x = "<tr>";
			# Subject 
			$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
			
			$ReportCss = $this->IsGraduateReport($ClassLevelID, $ReportType)?"G":$ReportType;
			for($i=0;$i<sizeof($SubjectColAry);$i++)
			{
				$SubjectEng = "<div class='SubjectHeaderEn'>".$eReportCard['Template']['SubjectEng']."</div>";
				$SubjectChn = "<div class='SubjectHeaderCh'>".$eReportCard['Template']['SubjectChn']."</div>";
				$SubjectTitle = $SubjectColAry[$i];
				$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
				$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
				$x .= "<td {$Rowspan}  valign='middle' height='{$LineHeight}' class='SubjectColumn{$i}'>". $SubjectTitle . "</td>";
				$n++;
			}
			
			$ReportCss = $this->IsGraduateReport($ClassLevelID, $ReportType)?"G":"";
			
			#FullMark
			if($ShowSubjectFullMark)
			{
				$x .= "<td {$Rowspan}  valign='middle' class='border_left FullMarkHeader' align='center' ><div>". $eReportCard['Template']['FullMark'] ."</div></td>";
				$n++;
			}	
			
			# Marks
			$x .= $row1;
			
			# Subject Overall 
			if($ShowRightestColumn)
			{
				if($ReportType=="T" ||$this->IsGraduateReport($ClassLevelID, $ReportType))	# Terms Report Type
				{
					
					$x .= "<td {$Rowspan}  valign='middle' class='border_left' align='center'><div class='SubjectOverallHeader'>". $eReportCard['Template']['SubjectOverall'] ."</div></td>";
					if($this->IsGraduateReport($ClassLevelID, $ReportType))
						$x .= "<td {$Rowspan}  valign='middle' class='' align='center'><div class='SubjectRankHeader'>". $eReportCard['Template']['SubjectRank'] ."</div></td>";
				}
				else
				{
					$row2 .= "<td valign='middle'  class='border_top border_left' align='center'><div class='WholeYearSubHeader'>". $eReportCard['Template']['WholeYear'] ."<br>(100%)</div></td>";
					
					# Mark
	//				$row2 .= "<td class=' border_left reportcard_text' align='center' height='{$LineHeight}'><div class='WholeYearSubHeader'>". $eReportCard['Template']['Mark'] . "</div></td>";
					
					# Rank
	//				$row2 .= "<td class='border_top reportcard_text' align='center' height='{$LineHeight}'><div class='WholeYearSubHeader'>". $eReportCard['Template']['SubjectRank'] . "</div></td>";
					
				}
	
				$n++;
			}
			else
			{
				//$e--;	
			}
			
			# Subject Teacher Comment
			if ($AllowSubjectTeacherComment) {
				$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
			}
			
			$x .= "</tr>";
		}
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting,$image_path,$LAYOUT_SKIN;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$IsMainReport = $ReportSetting['isMainReport'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$CmpCss = "Cmp";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$CmpCss = "";
			 		}

		 			if($IsMainReport)
		 			{
				 		
	//			 		$css_border_top = ($isFirst)? "border_top" : "";
				 		foreach($SubjectDisplay as $k=>$v)
				 		{
					 		$SubjectEng = "<div class='SubjectNameEn$CmpCss'>".$this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN")."</div>";
			 				$SubjectChn = "<div class='SubjectNameCh$CmpCss'>".$this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH")."</div>";
			 				
			 				$v = str_replace("SubjectEng", $SubjectEng, $v);
			 				$v = str_replace("SubjectChn", $SubjectChn, $v);
			 				
					 		if($isFirst)
					 			$x['FirstRow'] .= "<td class='tabletext border_top' height='0'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
				 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
							$t .= "</tr></table>";
							$t .= "</td>";
				 		}
						$x[] = $t;
		 			}
		 			else
		 			{
		 				$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				$t = "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td class='tabletext ExtraSubjectCol'>$SubjectChn ($SubjectEng)</td>";
							$t .= "</tr></table>";
						$t .= "</td>";
						
						$x[] = $t;

				 		if($isFirst)
				 			$x['FirstRow'] = "<td class='tabletext border_top' height='0'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";

		 			}

					$isFirst = 0;

				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='',$StudentID='')
	{
 		global $eReportCard, $eRCTemplateSetting,$PATH_WRT_ROOT;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$SignatureTable .= "<tr>";
				for($k=0; $k<sizeof($SignatureTitleArray); $k++)
				{
					$ExtraInfo = '';
					$SettingID = trim($SignatureTitleArray[$k]);
		
					$Title = "<div class='SignatureTitleEn'>".$eReportCard['Template'][$SettingID."En"]."</div>";
					$Title .= "<div class='SignatureTitleCh'>".$eReportCard['Template'][$SettingID."Ch"]."</div>";
					if($SettingID=="ClassTeacher" )
					{
						$thisClassName = $this->Get_Student_ClassName($StudentID);
						include_once($PATH_WRT_ROOT."includes/libclass.php");
						$lclass = new libclass();
						global $ClassTeacherAry;
						if(!$ClassTeacherAry[$thisClassName])
							$ClassTeacherAry[$thisClassName] =  $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
						$thisClassTeacher = $ClassTeacherAry[$thisClassName];
						$ClassTeacherNameArr = Get_Array_By_Key($thisClassTeacher,"ChineseName");
						
						foreach($ClassTeacherNameArr as $ClassTeacherName)
							$ExtraInfo .= "<div class='SignatureClassTeacher'>".$ClassTeacherName.$eReportCard['Template']['TeacherCh']."</div>";
					}
					if($SettingID=="Principal" )
					{
						$ExtraInfo = $eReportCard['Template']['PrincipalName']; 
					}
								
					$IssueDate = ($SettingID == "IssueDate" && $Issued) ? $Issued:"";
					
					$SignatureTable.= "<td align='center'>";
						$SignatureTable .= "<div class='SignatureArea'>$IssueDate</div>";
						$SignatureTable .= "<div class='SignatureTitleArea'>".$Title.$ExtraInfo."</div>";
					$SignatureTable.= "</td>";
				}			
			$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
//		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
//		$SignatureTable .= "<tr>";
//		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
//		{
//			$SettingID = trim($SignatureTitleArray[$k]);
//
//			$Title = "<div class='SignatureTitleEn'>".$eReportCard['Template'][$SettingID."En"]."</div>";
//			$Title .= "<div class='SignatureTitleCh'>".$eReportCard['Template'][$SettingID."Ch"]."</div>";
//			if($SettingID=="ClassTeacher" )
//			{
//				$thisClassName = $this->Get_Student_ClassName($StudentID);
//				include_once($PATH_WRT_ROOT."includes/libclass.php");
//				$lclass = new libclass();
//				global $ClassTeacherAry;
//				if(!$ClassTeacherAry[$thisClassName])
//					$ClassTeacherAry[$thisClassName] =  $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
//				$thisClassTeacher = $ClassTeacherAry[$thisClassName];
//				$ClassTeacherStr = implode("<br>",Get_Array_By_Key($thisClassTeacher,"CTeacher")); 
//			}
//						
//			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
//			$SignatureTable .= "<td valign='bottom' align='center'>";
//			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
//			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
//			$SignatureTable .= "<tr><td align='center' valign='bottom'>".$Title."</td></tr>";
//			$SignatureTable .= "</table>";
//			$SignatureTable .= "</td>";
//		}
//
//		$SignatureTable .= "</tr>";
//		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$IsMainReport 				= $ReportSetting['isMainReport'];

		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# Retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
            $WebSamsRegNo = $lu->WebSamsRegNo;

            $ClassName = $this->Get_Student_ClassName($StudentID);
            $ClassInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
            $ClassID   = $ClassInfo[0]['ClassID'];
		}
		
		# Retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
   		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$ClassPosition = $ClassPosition."/".$ClassNumOfStudent;
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		$FormPosition = $FormPosition."/".$FormNumOfStudent;

        # Retrieve Student Attendance
        $StudentAttendanceData = array();
        $StudentAttendanceData[0] = $StudentID ? $this->Get_Student_Profile_Attendance_Info($ReportID, $StudentID, $ClassID) : array();

//		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName)
			{
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";

                $columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
                $columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";

                if($ReportType == 'W')
                {
                    $columnTermReports = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly = 1, $ColumnID);
                    $columnTermReportID = $columnTermReports[0]['ReportID'];
                    
                    $columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $columnTermReportID, $ClassLevelID, '', '', 'ClassPosition')."/".$columnClassNumOfStudent[$ColumnID] : "S";
                    $columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $columnTermReportID, $ClassLevelID, '', '', 'FormPosition')."/".$columnFormNumOfStudent[$ColumnID] : "S";

                    $colSemID = $columnTermReports[0]['Semester'];
                    $StudentAttendanceData[$colSemID] = $StudentID ? $this->Get_Student_Profile_Attendance_Info($columnTermReportID, $StudentID, $ClassID) : array();
                }
                else
                {
                    $columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition')."/".$columnClassNumOfStudent[$ColumnID] : "S";
                    $columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition')."/".$columnFormNumOfStudent[$ColumnID] : "S";
                }
			}
//		}
		
		$first = 1;
		# Overall Result
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(-1,-2,-3), 0, $ReportID);
		
		if($IsMainReport)
		{
			if($ShowGrandTotal)
			{
				$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
				$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
				$GrandTotalFullMark = $this->returnGrandTotalFullMark($ReportID, $StudentID,1);
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first, $GrandTotalFullMark);
				
				$first = 0;
			}
			
			if ($eRCTemplateSetting['HideCSVInfo'] != true)
			{
				# Average Mark 
				if($ShowGrandAvg)
				{
					$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
					$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
					
					$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first,$SubjectFullMarkAry[-1]);
					
					$first = 0;
				}
				
				# Position in Class 
				if($ShowOverallPositionClass)
				{
					$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
					$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
					$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first);
					
					$first = 0;
				}
				
				# Number of Students in Class 
				if($ShowNumOfStudentClass)
				{
					$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
					$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
					$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
					
					$first = 0;
				}
				
				# Position in Form 
				if($ShowOverallPositionForm)
				{
					$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
					$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
					$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first);
					
					$first = 0;
				}
				
				# Number of Students in Form 
				if($ShowNumOfStudentForm)
				{
					$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
					$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
					$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
					
					$first = 0;
				}
				
				##################################################################################
				# CSV related
				##################################################################################
				# build data array
				$ary = array();
				//$csvType = $this->getOtherInfoType();
				
				//modified by Marcus 20/8/2009
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
				$ary = $OtherInfoDataAry[$StudentID];

				if($SemID=="F")
				{
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					
					/*
					foreach($ColumnData as $k1=>$d1)
					{
						$TermID = $d1['SemesterNum'];
						$InfoTermID = $TermID;
						
						if(!empty($csvType))
						{
							foreach($csvType as $k=>$Type)
							{
								$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
								if(!empty($csvData)) 
								{
									foreach($csvData as $RegNo=>$data)
									{
										if($RegNo == $WebSamsRegNo)
										{
											foreach($data as $key=>$val)
												$ary[$TermID][$key] = $val;
										}
									}
								}
							}
						}
					}
					*/
					
					# calculate sems/assesment col#
					$ColNum2Ary = array();
					foreach($ColumnData as $k1=>$d1)
					{
						$TermID = $d1['SemesterNum'];
	//					if($d1['IsDetails']==1)
	//					{
	//						# check sems/assesment col#
	//						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
	//						$thisReportID = $thisReport['ReportID'];
	//						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
	//						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
	//					}
	//					else
                        $ColNum2Ary[$TermID] = 0;
					}
				}
				/*
				else
				{
					$InfoTermID = $SemID;
					
					if(!empty($csvType))
					{
						foreach($csvType as $k=>$Type)
						{
							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
							
							if(!empty($csvData)) 
							{
								foreach($csvData as $RegNo=>$data)
								{
									if($RegNo == $WebSamsRegNo)
									{
										foreach($data as $key=>$val)
											$ary[$SemID][$key] = $val;
									}
								}
							}
						}
					}
				}
				*/
	
				$border_top = $first ? "border_top" : "";
				
				# Days Absent 
				$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
				$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
				$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary, $StudentAttendanceData);
				
				# Times Late 
				$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
				$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
				$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary, $StudentAttendanceData);
				
	//			# Conduct 
	//			$thisTitleEn = $eReportCard['Template']['ConductEn'];
	//			$thisTitleCh = $eReportCard['Template']['ConductCh'];
	//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
			}
		}
		else // extra report
		{
			$x .= "<tr><td colspan=3>";
				$x .= "<div class='ExtraMSTableFooter'>";
					$x .= "<div class='ExtraGrandMark'>";
						$x .= "<table cellpadding=2 cellspacing=2 border=0>";
//							$x .= "<col width='70px'>";
//							$x .= "<col width='5%'>";
//							$x .= "<col>";
//							$x .= "<tr>";
//								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['GrandTotalCh']."</td>";
//								$x .= "<td>: </td>";
//								$x .= "<td align='left'>".$GrandTotal."</td>";
//							$x .= "</tr>";
//							$x .= "<tr>";
//								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['GrandAverageCh']."</td>";
//								$x .= "<td>: </td>";
//								$x .= "<td align='left'>".$GrandAverage."</td>";
//							$x .= "</tr>";
//							$x .= "<tr>";
//								$x .= "<td colspan=3><br></td>";
//							$x .= "</tr>";
//							$x .= "<tr>";
//								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['SignatureCh']."</td>";
//								$x .= "<td>: </td>";
//								$x .= "<td align='left'>________________________</td>";
//							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['GrandTotalCh'].": ".$GrandTotal."</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['GrandAverageCh'].": ".$GrandAverage."</td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td colspan=3><br></td>";
							$x .= "</tr>";
							$x .= "<tr>";
								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['SignatureCh'].": ________________________</td>";
							$x .= "</tr>";

						$x .= "</table>";
					$x .= "</div>";
//					$x .= "<div class='ExtraSignature'>";
////						$x .= "<table cellpadding=2 cellspacing=2 border=0>";
////							$x .= "<col width='35%'>";
////							$x .= "<col width='5%'>";
////							$x .= "<col width='60%'>";
////							$x .= "<tr>";
////								$x .= "<td align='right'>".$eReportCard['ExtraTemplate']['GrandTotalCh']."</td>";
////								$x .= "<td>: </td>";
////								$x .= "<td align='left'>".$GrandTotal."</td>";
////							$x .= "</tr>";
////						$x .= "</table>";
//						$x .= $eReportCard['ExtraTemplate']['SignatureCh'];
//					$x .= "</div>";
				$x .= "</div>";
			$x .= "</td></tr>";
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		
		global $eReportCard, $image_path, $LAYOUT_SKIN;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$IsMainReport 				= $ReportSetting['isMainReport'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T" || $this->IsGraduateReport($ClassLevelID, $ReportType))	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$row1 = '';
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
					$CmpCss = "";
				}
				else
				{
					$CmpCss= "Cmp";
				}
				
				# define css
//				$css_border_top = $isFirst?"border_top":"";
//				$css_border_top = ($isSub)? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
									
				# Assessment Marks & Display
//				for($i=0;$i<sizeof($ColumnID);$i++)
//				{
//					$thisColumnID = $ColumnID[$i];
//					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//					
//					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//
//					if ($isSub && $columnSubjectWeightTemp == 0)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//						$thisMarkDisplay = $this->EmptySymbol;
//					} else {
//						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
//						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//						
//						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
//						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
//						
//						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//						
//						# for preview purpose
//						if(!$StudentID)
//						{
//							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//						}
//						
//						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//						
//						if ($thisMark != "N.A.")
//						{
//							$isAllNA = false;
//						}
//						
//						# check special case
//						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
//						
//						if($needStyle)
//						{
//							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//							{
//								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//							}
//							else
//							{
//								$thisMarkTemp = $thisMark;
//							}
//							
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//						}
//						else
//						{
//							$thisMarkDisplay = $thisMark;
//						}
//						
//					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
//					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
//					}
//						
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//  					
//  					if($ShowSubjectFullMark)
//  					{
//	  					# check special full mark
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//	  					
//						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
//						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//					}
//					
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
//				}
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
					$thisFormRank = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$thisFormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";

					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}

					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;

					if ($thisMark != "N.A." && trim($thisMark)!=="")
					{
						$isAllNA = false;
					}

					$specialDisplayForNA = false;
                    if($this->schoolYear == '2019' && $isSub && $thisMark == 'N.A.')
                    {
                        $isAllNA = false;
                        $specialDisplayForNA = true;
                    }

					# check special case
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
					{
						if(trim($thisMark)=='')
						{
							$MarksAry[$SubjectID][0]['Grade'] = "N.A.";
							$thisMark = "-1";
						}

						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);

						if($needStyle)
						{
//	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
//								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//							else
								$thisMarkTemp = $thisMark;

							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
                        {
							$thisMarkDisplay = $thisMark;
                        }
					}

					if($specialDisplayForNA)
                    {
                        $thisMarkDisplay = '/';
                    }
					
 					if($ShowSubjectFullMark || !$IsMainReport)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='center' ><div class='SubjectFullMark$CmpCss'>". $thisFullMark ."</div></td>";
						$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
					}					
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>". $thisMarkDisplay ."</td>";
//  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>". $thisFormRank."/ ".$thisFormNumOfStudent ."</td>";
// 
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
					
					$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='center' ><div class='SubjectMark$CmpCss'>". $thisMarkDisplay .$br."</div></td>";
					$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
					
					$thisRank = ($thisFormRank>0)?"$thisFormRank/$thisFormNumOfStudent":$this->EmptySymbol."&nbsp;";
					if($this->IsGraduateReport($ClassLevelID, $ReportType)) # Show Ranking for F5,7 Report
					{
						$x[$SubjectID] .= "<td class=' tabletext {$css_border_top}' align='center' ><div class='SubjectRank$CmpCss'>". $thisRank ."</div></td>";
						$row1 .= "<td class='border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
					}
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
					$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['FirstRow'] = $row1;
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$row1='';
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
//				$css_border_top = $isFirst? "border_top" : "";
				if($ShowSubjectFullMark)
				{
					$thisSubjectWeight = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID='0' OR ReportColumnID IS NULL) and SubjectID = '$SubjectID' ");
  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
					$x[$SubjectID] .= "<td class='$css_border_top border_left tabletext' align='center'><div class='WSubjectFullMark'>". $FullMark ."</div></td>";
					$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
				}
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
//					$css_border_top = $isSub? "" : "border_top";
					
//					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
//					if($isDetails==1)		# Retrieve assesments' marks
//					{
//						# See if any term reports available
//						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
//						
//						# if no term reports, CANNOT retrieve assessment result at all
//						if (empty($thisReport)) {
//							for($j=0;$j<sizeof($ColumnID);$j++) {
//								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//							}
//						} else {
//							$thisReportID = $thisReport['ReportID'];
//							
//							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
//							$ColumnID = array();
//							$ColumnTitle = array();
//							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
//							
//							$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
//							
//							for($j=0;$j<sizeof($ColumnID);$j++)
//							{
//								$thisColumnID = $ColumnID[$j];
//								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
//								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
//								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
//								
//								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
//								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
//					
//								if ($isSub && $columnSubjectWeightTemp == 0)
//								{
//									$thisMarkDisplay = $this->EmptySymbol;
//								}
//								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
//									$thisMarkDisplay = $this->EmptySymbol;
//								} else {
//									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
//									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//									
//									$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
//									$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
//									
//									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//									
//									# for preview purpose
//									if(!$StudentID)
//									{
//										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//									}
//									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
//													
//									if ($thisMark != "N.A.")
//									{
//										$isAllNA = false;
//									}
//									
//									# check special case
//									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
//									if($needStyle)
//									{
//										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
//										{
//											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
//										}
//										else
//										{
//											$thisMarkTemp = $thisMark;
//										}
//										
//										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
//									}
//									else
//									{
//										$thisMarkDisplay = $thisMark;
//									}
//								}
//									
//								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
//			  					
////			  					if($ShowSubjectFullMark)
////			  					{
////				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
////									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
////								}
//								
//								$x[$SubjectID] .= "</tr></table>";
//								$x[$SubjectID] .= "</td>";
//							}
//						}
//					}
//					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = $this->EmptySymbol;
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								$thisFormRank = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]['OrderMeritForm'];
								$thisFormNumOfStudent = $thisMarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]['FormNoOfStudent'];
																
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
								$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
								$thisFormRank = $thisMarksAry[$SubjectID][0]['OrderMeritForm'];
								$thisFormNumOfStudent = $thisMarksAry[$SubjectID][0]['FormNoOfStudent'];
																					
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							}
							
							$thisRank = $thisFormRank>0?"$thisFormRank/ $thisFormNumOfStudent":$this->EmptySymbol;
							
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A." && trim($thisMark)!=="")
							{
								$isAllNA = false;
							}
						
							# check special case
							if(trim($thisMark)=='')
							{
								$thisGrade = "N.A.";
								$thisMark = "-1";
							}
							
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
						
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}' align='center'><div class='WMsMarks'>$thisMarkDisplay</div></td>";
//						$x[$SubjectID] .= "<td class=' {$css_border_top}' align='center'><div class='WMsMarks'>$thisRank </div></td>";
						
//						$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
						$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
//						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
//	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
//	  					if($ShowSubjectFullMark)
//	  					{
// 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
//							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//						}
						
//						$x[$SubjectID] .= "</tr></table>";
//						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					$thisMSFormRank = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$thisMSFormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];	
								
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A.") 
					{
						$isAllNA = false;
					}
						
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
					{
						# check special case
//						if(trim($thisMark)=='')
//						{
//							$MarksAry[$SubjectID][0]['Grade'] = "N.A.";
//							$thisMark = "-1";
//						}
						if($thisMark == "N.A.")
						{
							$thisMarkDisplay = '---';
						}
						else
						{
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
							}
							else
								$thisMarkDisplay = $thisMark;
						}
					}
					
					$thisMSRank = $thisMSFormRank>0?"$thisMSFormRank/ $thisMSFormNumOfStudent":$this->EmptySymbol;
						
//					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
//					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'><div class='WMsOverallMarks'>". $thisMarkDisplay ."</div></td>";
//  					$x[$SubjectID] .= "<td class='tabletext {$css_border_top}' align='center'><div class='WMsOverallMarks'>". $thisMSRank ."</div></td>";
//  					$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
  					$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
  					
//  					if($ShowSubjectFullMark)
//  					{
//	  					# check special full mark
//	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
//	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
//	  					
//						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
//						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
//					}
  					
//					$x[$SubjectID] .= "</tr></table>";
//					$x[$SubjectID] .= "</td>";
					
					
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
					$row1 .= "<td class='border_left border_top ' height=0><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='0' height='0'/></td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['FirstRow'] = $row1;
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				if(!$returnArr['FirstGradeRow'] && $LastScaleDisplay == "M" && $ScaleDisplay == "G" && !$isAllNA)
					$returnArr['FirstGradeRow'] = $SubjectID;
				
				if(!$isAllNA)
					$LastScaleDisplay = $ScaleDisplay;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;

		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}


		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$SemID 						= $ReportSetting['Semester'];
		$ClassLevelID				= $ReportSetting['ClassLevelID'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		// [2016-0413-1012-47206]
 		//$semesterNum 				= $this->Get_Semester_Seq_Number($SemID);

		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		//modified by marcus 20/8/2009
		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		//$latestTerm++;

		# retrieve result data
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];

		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		if (is_array($ECA))
			$ecaList = implode("<br>", $ECA);
		else
			$ecaList = $ECA;

		$remark = $ary['Remark'];

        $eca = $ary['ECA'];

		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = nl2br($CommentAry[0]);

		if($ReportType=="T" || $this->IsGraduateReport($ClassLevelID, $ReportType)) 
		{
			$ConductTable = "<table>";
				$ConductTable .= "<tr>";
					$ConductTable .= "<td><div class='MiscTitleCh'>".$eReportCard['Template']['ConductCh']."</div></td>";
					$ConductTable .= "<td><div class='MiscTitleEn'>".$eReportCard['Template']['ConductEn']."</div></td>";
					$ConductTable .= "<td><div class='MiscData'>". ($ary['Conduct']? $ary['Conduct'] : $this->EmptySymbol) ."</div></td>";
					$ConductTable .= "<td><div class='MiscSpaser'>&nbsp;</div></td>";
				$ConductTable .= "</tr>";
			$ConductTable .= "</table>";

		
			$LearningAtt = "<table>";
				$LearningAtt .= "<tr>";
					$LearningAtt .= "<td><div class='MiscTitleCh'>".$eReportCard['Template']['LearningAttitudeCh']."</div></td>";
					$LearningAtt .= "<td><div class='MiscTitleEn'>".$eReportCard['Template']['LearningAttitudeEn']."</div></td>";
					$LearningAtt .= "<td><div class='MiscData'>". ($ary['Learning attitude']? $ary['Learning attitude'] : $this->EmptySymbol) ."</div></td>";
					$LearningAtt .= "<td><div class='MiscSpaser'>&nbsp;</div></td>";
				$LearningAtt .= "</tr>";
			$LearningAtt .= "</table>";

			if($this->Get_Form_Number($ClassLevelID)==5 && $this->IsGraduateReport($ClassLevelID, $ReportType))
			{
				$PromotionStr = "<div class='PromotionRemark'>".$eReportCard['Template']['PromotionRemarkCh']."</div>";
				$PromotionStr .= "<div class='PromotionRemark'>".$eReportCard['Template']['PromotionRemarkEn']."</div>";
			}

			$x .= "<div class='MiscConduct'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='' class='report_border'>";
				$x .= "<tr>";
					$x .= "<td>$ConductTable</td>";
					$x .= "<td>$LearningAtt</td>";
					$x .= "<td>$PromotionStr</td>";
				$x .= "</tr>"; 
			$x .= "</table>";
			$x .= "</div>";

			# Comment
			$x .= "<div class='MiscCommentDiv'>";
				$x .= "<div class='MiscCommentTitle'>";
					$x .= "<div class='MiscCommentTitleCh'>".$eReportCard['Template']['CommentCh']."</div>";
					$x .= "<div class='MiscCommentTitleEn'>".$eReportCard['Template']['CommentEn']."</div>";
					$x .= "<div class='spacer'></div>";
				$x .= "</div>";
				$x .= "<div class='MiscCommentContent'>";
					$x .= $classteachercomment;
				$x .= "</div>";
			$x .= "</div>";
			
			#ECA
			$ECAItem = '';
			foreach((array)$eca as $thisECA)
			{
				$ECAItem .= "<div class='ECAItem'>".$thisECA."</div>";
			}

			if($StudentID)
			{	
				$StudentProfileActivity = $this->Get_Student_Profile_Data("ACTIVITY",$StudentID);
				$StudentProfileService = $this->Get_Student_Profile_Data("SERVICE",$StudentID);
				// [2016-0413-1012-47206] display award in ECA again
				$StudentProfileAward = $this->Get_Student_Profile_Data("AWARD",$StudentID);	
			}
			
			foreach((array)$StudentProfileActivity as $thisActivity)
				$ECAItem .= "<div class='ECAItem'>".$thisActivity['ActivityName']." ".$thisActivity['Role']."</div>"; 
			foreach((array)$StudentProfileService as $thisService)
				$ECAItem .= "<div class='ECAItem'>".$thisService['ServiceName']." ".$thisService['Role']."</div>";
			// [2016-0413-1012-47206] added award items to ECA
			foreach((array)$StudentProfileAward as $thisAward)
				$ECAItem .= "<div class='ECAItem'>".$thisAward['AwardName']."</div>";
				
			if(!empty($ECAItem))
			{
				$ECATable = "<div class='ECATable'>";
					$ECATable .= "<div class='ECATableTitle'>".$eReportCard['Template']['eca']."</div>";
					$ECATable .= $ECAItem;
				$ECATable .= "</div>";
				$ECATable .= "<div class='spacer'></div>";
			}
			
			# Merit
			$MeritArr["Merit"] = array("Merits" => "Merits","MinorCredit" => "Minor Credit","MajorCredit" => "Major Credit");
			$MeritArr["Demerit"] = array("Demerits" => "Demerits","MinorFault" => "Minor Fault","MajorFault" => "Major Fault");
			foreach($MeritArr as $MeritType =>$thisMeritArr)
			{
				$MeritTypeItem = '';
				foreach($thisMeritArr as $LangKey => $ValKey)
				{
					if($ary[$ValKey])
					{
						$MeritTypeItem .= "<div class='MeritItem'>";
							$MeritTypeItem .= $eReportCard['Template'][$LangKey]." ";
							$MeritTypeItem.= $ary[$ValKey]." ";
							$MeritTypeItem .= $eReportCard['Template']['Times'];
						$MeritTypeItem .= "</div>";
					}
				}
				if($MeritTypeItem)
					$MeritTableItem .= $MeritTypeItem."<div class='spacer'></div>";
			}
			if($MeritTableItem)
			{
				$MeritTable .= "<div class='ECATable'>";
					$MeritTable .= "<div class='ECATableTitle'>".$eReportCard['Template']['MeritsDemerits']."</div>";
					$MeritTable .= $MeritTableItem;
				$MeritTable .= "</div>";
			}
			
			if(!$MeritTableItem && !$ECATable)
				$Nil = "<div class='MiscOtherInfoNil'>".$eReportCard['Template']['NIL']."</div>";
				
			$x .= "<div class='MiscOtherInfoDiv'>";
				$x .= "<div class='MiscOtherInfoTitle'>";
					$x .= "<div class='MiscCommentTitleCh'>".$eReportCard['Template']['OtherInfoCh']."</div>";
					$x .= "<div class='MiscCommentTitleEn'>".$eReportCard['Template']['OtherInfoEn']."</div>";
					$x .= "<div class='spacer'></div>";
				$x .= "</div>";
				$x .= "<div class='MiscOtherInfoContent'>";
					$x .= $ECATable;
					$x .= $MeritTable;
					$x .= $Nil;
				$x .= "</div>";
			$x .= "</div>";
			
		}
		else
		{
			#ConductTable
			foreach($sems as $TermID=>$TermName)
			{	
				$Conduct = $OtherInfoDataAry[$StudentID][$TermID]["Conduct"];
				$LearningAttitude = $OtherInfoDataAry[$StudentID][$TermID]["Learning attitude"];
				
				$SemNameEn = $this->returnSemesters($TermID, 'en');
				$SemNameCh = $this->returnSemesters($TermID, 'b5');
				
				$Row1Col .= "<th align='center'>".$SemNameCh." ".$SemNameEn."</th>";
				$Row2Col .= "<td align='center'>". ($Conduct? $Conduct : $this->EmptySymbol) ."</td>";
				$Row3Col .= "<td align='center'>". ($LearningAttitude? $LearningAttitude : $this->EmptySymbol) ."</td>";
				
				$Row4Col .= "<td align='center'>";
				$MeritArr["Merit"] = array("Merits" => "Merits","MinorCredit" => "Minor Credit","MajorCredit" => "Major Credit");
				$MeritArr["Demerit"] = array("Demerits" => "Demerits","MinorFault" => "Minor Fault","MajorFault" => "Major Fault");
				$MeritItem = '';
				$isFirst = 1;
				foreach($MeritArr as $MeritType =>$thisMeritArr)
				{
					foreach($thisMeritArr as $LangKey => $ValKey)
					{
						if($OtherInfoDataAry[$StudentID][$TermID][$ValKey])
						{
							$paddingTop = $isFirst==1?'':'padding-top:6px;';
							$MeritItem .= "<div class='WMeritItem' style='$paddingTop'>";
								$MeritItem .= sprintf($eReportCard['Template']['WReportArr'][$LangKey],$OtherInfoDataAry[$StudentID][$TermID][$ValKey],$OtherInfoDataAry[$StudentID][$TermID][$ValKey]);
							$MeritItem .= "</div>";
							$isFirst=0;
						}
					}
				}
				$Row4Col .= empty($MeritItem)?$eReportCard['Template']['NIL']:$MeritItem;
				$Row4Col .= "</td>";
				
			}
			
//			$PromotionStr = "<div class='PromotionRemark'>".$eReportCard['Template']['PromotionRemarkCh']."</div>";
//			$PromotionStr .= "<div class='PromotionRemark'>".$eReportCard['Template']['PromotionRemarkEn']."</div>";
			

			$x .= "<div class='MiscRemarks' style='page-break-after:always;'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='' class='MiscRemarksTable'>";
				$x .= "<tr valign=top>";
					$x .= "<td width='80px'>".$eReportCard['Template']['PassRemarksTitleCh']."</td>";
					$x .= "<td>".$eReportCard['Template']['PassRemarksCh']."</td>";
				$x .= "</tr>";
				$x .= "<tr valign=top>";
					$x .= "<td width='80px'>".$eReportCard['Template']['PassRemarksTitleEn']."</td>";
					$x .= "<td>".$eReportCard['Template']['PassRemarksEn']."</td>";
				$x .= "</tr>"; 
			$x .= "</table>";
			$x .= "</div>";
			$x .= "<div></div>";

//			$x .= "<div class='page_breaker' style='page-break-before:always;'>breaker</div>";

			$ConductTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='' class='WConductTable'>";
			$ConductTable .= "<col width='34%'>";
			$ConductTable .= "<col width='33%'>";
			$ConductTable .= "<col width='33%'>";
			$ConductTable .= "<tr>";
				$ConductTable .= "<th>&nbsp;</th>".$Row1Col;
			$ConductTable .= "</tr>";
			# Conduct
			$ConductTable .= "<tr>";
				$ConductTable .= "<td><table width='100%' cellpadding=0 cellspacing=0><tr><td width='45%'>".$eReportCard['Template']['ConductCh']."</td><td width='55%'>".$eReportCard['Template']['ConductEn']."</td></tr></table></td>".$Row2Col;
			$ConductTable .= "</tr>";
			# Learning Attitude
			$ConductTable .= "<tr>";
				$ConductTable .= "<td><table width='100%' cellpadding=0 cellspacing=0><tr><td width='45%'>".$eReportCard['Template']['LearningAttitudeCh']."</td><td width='55%'>".$eReportCard['Template']['LearningAttitudeEn']."</td></tr></table></td>".$Row3Col;
			$ConductTable .= "</tr>";
			# Merit Demerit
			$ConductTable .= "<tr valign='top'>";
				$ConductTable .= "<td><table width='100%' cellpadding=0 cellspacing=0><tr><td width='45%'>".$eReportCard['Template']['MeritsDemeritsCh']."</td><td width='55%'>".$eReportCard['Template']['MeritsDemeritsEn']."</td></tr></table></td>".$Row4Col;
			$ConductTable .= "</tr>";
			$ConductTable .= "</table>";
			
			$x .= "<div class='WMiscDiv'>";			
				$x .= "<div class='WMiscConduct'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=''>";
					$x .= "<tr valign=top>";
						$x .= "<td>$ConductTable</td>";
					$x .= "</tr>"; 
				$x .= "</table>";
				$x .= "</div>";

				if($StudentID)
				{	
					$StudentProfileActivity = $this->Get_Student_Profile_Data("ACTIVITY",$StudentID);
					$StudentProfileService = $this->Get_Student_Profile_Data("SERVICE",$StudentID);
					$StudentProfileAward = $this->Get_Student_Profile_Data("AWARD",$StudentID);	
				}
				
				$ECAItem = '';
				foreach((array)$StudentProfileActivity as $thisActivity)
					$ECAItem .= "<div class='WECAItem'>".$thisActivity['ActivityName']." ".$thisActivity['Role']."</div>"; 
				foreach((array)$StudentProfileService as $thisService)
					$ECAItem .= "<div class='WECAItem'>".$thisService['ServiceName']." ".$thisService['Role']."</div>"; 
				foreach((array)$StudentProfileAward as $thisAward)
					$ECAItem .= "<div class='WECAItem'>".$thisAward['AwardName']."</div>"; 
				if(empty($ECAItem))
					$ECAItem = "<div class='WECAItem'>".$eReportCard['Template']['NIL']."</div>";
				
				$x .= "<div class='WMiscECAService'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" height='' class='WMiscECATable'>";
					$x .= "<tr valign=top>";
						$x .= "<th align='center'>".$eReportCard['Template']['WReportArr']['eca']."</th>";
					$x .= "</tr>"; 
					$x .= "<tr valign=top>";
						$x .= "<td>";
							$x .= "<div class='WECAItemList'>";
							$x .= $ECAItem;
							$x .= "</div>";
						$x .= "</td>";
					$x .= "</tr>"; 
						$x .= "</td>";
					$x .= "</tr>"; 
				$x .= "</table>";
				$x .= "</div>";
	
				$x .= "<div class='WMiscComment'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" height='' class='WMiscCommentTable'>";
					$x .= "<tr valign=top>";
						$x .= "<th align='center'>".$eReportCard['Template']['CommentCh']." ".$eReportCard['Template']['CommentEn']."</th>";
					$x .= "</tr>"; 
					$x .= "<tr valign=top>";
						$x .= "<td class='WClassTeacherComment'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$classteachercomment."<br></td>";
					$x .= "</tr>"; 
				$x .= "</table>";
				$x .= "</div>";
				
				$x .= "<div class='WMiscPromotion'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" height='' class='WMiscPromotionTable'>";
					$x .= "<tr>";
						$x .= "<td align='right'>";
							$x .= "<table width=\"40%\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" height='' class='report_border'><tr>";			
								$x .= "<td>".$eReportCard['Template']['PromotionRemarkCh']."</td>";
								$x .= "<td>".$eReportCard['Template']['PromotionRemarkEn']."</td>";
							$x .= "</tr></table>";			
						$x .= "</td>";
					$x .= "</tr>"; 
				$x .= "</table>";
				$x .= "</div>";
				
			$x .= "</div>";
		}
		

		
//		$x .= "<br>";
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
//		$x .= "<tr>";
//		$x .= "<td width='50%' valign='top'>";
//			# Merits & Demerits 
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['MeritsDemerits']."</b><br>".$merit."</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .= "</td></tr></table>";	
//		$x .="</td>";
//		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
//			# Remark 
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .= "</td></tr></table>";	
//		$x .="</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
//		
//		$x .= "<br>";
//		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
//		$x .= "<tr>";
//		if($AllowClassTeacherComment)
//		{
//			$x .= "<td width='50%' valign='top'>";
//				# Class Teacher Comment 
//				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//				$x .= "<tr>";
//					$x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".stripslashes(nl2br($classteachercomment))."</td>";
//				$x .= "</tr>";
//				$x .= "</table>";	
//				$x .= "</td></tr></table>";	
//			$x .="</td>";
//			
//			$x .= "<td width='50%' class='border_left' valign='top'>";
//		} else {
//			$x .= "<td width='50%' valign='top'>";
//		}
//			# ECA 
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
//			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//			$x .= "<tr>";
//				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$ecaList."</td>";
//			$x .= "</tr>";
//			$x .= "</table>";	
//			$x .= "</td></tr></table>";	
//		$x .="</td>";
//		$x .= "</tr>";
//		$x .= "</table>";
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr, $extraValueArr=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			$x .= "<td class='tabletext border_left' align='center' height='{$LineHeight}'><div class='FooterFullMark'>".$this->EmptySymbol."</div></td>";

        # Whole Year Report
		if($ReportType=='W' && !$this->IsGraduateReport($ClassLevelID, $ReportType))
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];

                //$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : 0) : "#";
                $thisValue = "#";
                if($StudentID) {
                    $thisValue = (!empty($extraValueArr) && $extraValueArr[$TermID][$InfoKey]) ? $extraValueArr[$TermID][$InfoKey] : 0;
                    $thisValue = $ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $thisValue;
                }
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' colspan=1 align='center' height='{$LineHeight}'><div class='FooterTermOverall'>". $thisValue ."</div></td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue == "") {
                    //$thisTotalValue = ($ValueArr[0][$InfoKey] == "") ? 0 : $ValueArr[0][$InfoKey];
                    $thisTotalValue = (!empty($extraValueArr) && $extraValueArr[0][$InfoKey]) ? $extraValueArr[0][$InfoKey] : 0;
                    $thisTotalValue = $ValueArr[0][$InfoKey] ? $ValueArr[0][$InfoKey] : $thisTotalValue;
                }
				$x .= "<td class='tabletext {$border_top} border_left' align='center' colspan=2 height='{$LineHeight}' colspan=2><div class='FooterOverall'>".$thisTotalValue."</div></td>";
			}
		}
        # Term Report
		else
		{
			# get value of this term
			//$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : 0) : "#";
            $thisValue = "#";
            if($StudentID) {
                $thisValue = (!empty($extraValueArr) && $extraValueArr[0][$InfoKey]) ? $extraValueArr[0][$InfoKey] : 0;
                $thisValue = $ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $thisValue;
            }
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan=2><div class='FooterOverall'>". $thisValue ."</div></td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0,$FullMark='')
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		if($ShowSubjectFullMark)
		{	
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'><div class='FooterFullMark'>".($FullMark?$FullMark:$this->EmptySymbol)."</div></td>";
		}
		
//		if ($CalOrder == 1) {
//			for($i=0;$i<$ColNum2;$i++)
//				$x .= "<td class='tabletext {$border_top} border_left'  colspan=2 align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
//		} else
		if($ReportType=='W' && !$this->IsGraduateReport($ClassLevelID, $ReportType)) 
		{
			
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
				
				$thisValue = $ValueArr[$ColumnID];
				$x .= "<td class='tabletext {$border_top} border_left' colspan=1 align='center' height='{$LineHeight}'><div class='FooterTermOverall'>".$thisValue."</div></td>";
				$curColumn++;
			}
		}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			/*
			if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
			{
				$thisDisplay = $thisValue;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			*/
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan=2><div class='FooterOverall'>". $thisValue ."</div></td>";
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}' colspan=2>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td height='{$LineHeight}'class='tabletext'><div class='FooterTitleCh'>". $TitleCh."</div></td><td><div class='FooterTitleEn'>".$TitleEn ."</div></td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
//		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
//			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
//				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
//			$x .= "</table>";
//		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
		// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
	
	function IsGraduateReport($ClassLevelID, $ReportType)
	{
		return $ReportType=='W' && in_array($this->Get_Form_Number($ClassLevelID),array(7));
	}
	
//	function Get_Student_Profile_Activity($StudentID='', $YearTermID='')
//	{
//		if(trim($StudentID)!='')
//			$cond_StudentID = " AND psa.UserID IN (".implode(",",(array)$StudentID).") ";
//		if(trim($YearTermID)!='')
//			$cond_YearTermID = " AND ayt.YearTermID IN (".implode(",",(array)$YearTermID).") ";
//		
//		$sql = "
//			SELECT
//				psa.*
//			FROM
//				PROFILE_STUDENT_ACTIVITY psa
//				INNER JOIN ACADEMIC_YEAR ay ON ay.YearNameEN = psa.Year
// 				LEFT JOIN ACADEMIC_YEAR_TERM ayt ON psa.Semester = ayt.YearTermNameEN AND ayt.AcademicYearID = ay.AcademicYearID
//			WHERE
//				ay.AcademicYearID = ".$this->schoolYearID."
//				$cond_StudentID
//				$cond_YearTermID
//		";
//		$result = $this->returnArray($sql);
//		
//		return $result;
//	}
//	
//	function Get_Student_Profile_Award($StudentID='', $YearTermID='')
//	{
//		if(trim($StudentID)!='')
//			$cond_StudentID = " AND psa.UserID IN (".implode(",",(array)$StudentID).") ";
//		if(trim($YearTermID)!='')
//			$cond_YearTermID = " AND ayt.YearTermID IN (".implode(",",(array)$YearTermID).") ";
//		
//		$sql = "
//			SELECT
//				psa.*
//			FROM
//				PROFILE_STUDENT_AWARD psa
//				INNER JOIN ACADEMIC_YEAR ay ON ay.YearNameEN = psa.Year
// 				LEFT JOIN ACADEMIC_YEAR_TERM ayt ON psa.Semester = ayt.YearTermNameEN AND ayt.AcademicYearID = ay.AcademicYearID
//			WHERE
//				ay.AcademicYearID = ".$this->schoolYearID."
//				$cond_StudentID
//				$cond_YearTermID
//		";
//
//		$result = $this->returnArray($sql);
//		
//		return $result;
//	}
//	
//	function Get_Student_Profile_Service($StudentID='', $YearTermID='')
//	{
//		if(trim($StudentID)!='')
//			$cond_StudentID = " AND psa.UserID IN (".implode(",",(array)$StudentID).") ";
//		if(trim($YearTermID)!='')
//			$cond_YearTermID = " AND ayt.YearTermID IN (".implode(",",(array)$YearTermID).") ";
//		
//		$sql = "
//			SELECT
//				psa.*
//			FROM
//				PROFILE_STUDENT_SERVICE psa
//				INNER JOIN ACADEMIC_YEAR ay ON ay.YearNameEN = psa.Year
// 				LEFT JOIN ACADEMIC_YEAR_TERM ayt ON psa.Semester = ayt.YearTermNameEN AND ayt.AcademicYearID = ay.AcademicYearID
//			WHERE
//				ay.AcademicYearID = ".$this->schoolYearID."
//				$cond_StudentID
//				$cond_YearTermID
//		";
//		$result = $this->returnArray($sql);
//		
//		return $result;
//	}	
	function Get_Student_Profile_Data($type, $StudentID='', $YearTermID='')
	{
		if(trim($StudentID)!='')
			$cond_StudentID = " AND psa.UserID IN (".implode(",",(array)$StudentID).") ";
		if(trim($YearTermID)=='F')
			$cond_YearTermID = " AND (ayt.YearTermID = '' OR ayt.YearTermID IS NULL) ";
		else if(trim($YearTermID)!='')
			$cond_YearTermID = " AND ayt.YearTermID IN (".implode(",",(array)$YearTermID).") ";
		
		$sql = "
			SELECT
				psa.*
			FROM
				PROFILE_STUDENT_$type psa
				INNER JOIN ACADEMIC_YEAR ay ON ay.YearNameEN = psa.Year
 				LEFT JOIN ACADEMIC_YEAR_TERM ayt ON psa.Semester = ayt.YearTermNameEN AND ayt.AcademicYearID = ay.AcademicYearID
			WHERE
				ay.AcademicYearID = '".$this->schoolYearID."'
				$cond_StudentID
				$cond_YearTermID
		";
		$result = $this->returnArray($sql);
		
		return $result;
	}

    function Get_Student_Profile_Attendance_Info($ReportID, $StudentID, $ClassID)
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT."includes/libclass.php");
        include_once ($PATH_WRT_ROOT."includes/libattendance.php");

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $TermStartDate = $ReportInfoArr['TermStartDate'];
        $TermEndDate = $ReportInfoArr['TermEndDate'];
        $SemID = $ReportInfoArr['Semester'];
        $ReportType = ($SemID == 'F') ? 'W' : 'T';

        if (is_date_empty($TermStartDate) && is_date_empty($TermEndDate)) {
            if ($ReportType == 'T') {
                $objYearTerm = new academic_year_term($SemID);
                $TermStartDate = substr($objYearTerm->TermStart, 0, 10);
                $TermEndDate = substr($objYearTerm->TermEnd, 0, 10);
            } else if ($ReportType == 'W') {
                $YearTermIDArr = Get_Array_By_Key($this->Get_Related_TermReport_Of_Consolidated_Report($ReportID), 'Semester');
                $numOfTerm = count($YearTermIDArr);
                for ($i = 0; $i < $numOfTerm; $i ++) {
                    $_yearTermId = $YearTermIDArr[$i];

                    $_objYearTerm = new academic_year_term($_yearTermId);
                    $_termStartDate = substr($_objYearTerm->TermStart, 0, 10);
                    $_termEndDate = substr($_objYearTerm->TermEnd, 0, 10);
                    if ($TermStartDate == '' || $_termStartDate < $TermStartDate) {
                        $TermStartDate = $_termStartDate;
                    }
                    if ($TermEndDate == '' || $_termEndDate > $TermEndDate) {
                        $TermEndDate = $_termEndDate;
                    }
                }
            }
        }

        $lattend = new libattendance();
        $result = $lattend->getAttendanceListByClass($lattend->getClassName($ClassID), $TermStartDate, $TermEndDate, '', array($StudentID));
        list ($id, $name, $classnumber, $absence, $late, $earlyleave) = $result[0];

        $ReturnArr = array();
        $ReturnArr["Days Absent"] = $absence;
        $ReturnArr["Time Late"] = $late;
        //$ReturnArr["Early Leave"] = $earlyleave;

        return $ReturnArr;
    }
}
?>