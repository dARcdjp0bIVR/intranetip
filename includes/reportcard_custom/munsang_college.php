<?php 
# Editing by Bill

/*
 * 2018-12-19 Bill [2018-1003-1539-26277]
 * Improved: modified genMSTableFooter(), to add row [Absent from CCA without application for leave]
 * 
 * 2014-08-18 Bill [民生書院 - eRC - Form Test Template]
 * Improved: Extra Term Report
 * 
 * 2012-02-23 Ivan [2012-0223-1022-41066 - 民生書院 - eRC how to copy the report card template?]
 * Improved: apply F7 template to F6
 * 
 * 2011-07-05 Ivan [2011-0705-1100-34071 - 民生書院 - Pricipal overlap with preprinted wordings in report]
 * Improved: The white space between "the Title and the Student Info" and "the Late and the Conduct" has been decreased as requested.
 */

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/munsang_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "remark", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='') {
		
		global $eReportCard;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			# Extra Report Checking
			$ReportExtra = !$ReportSetting['isMainReport'];
		}
		
		# Extra Report Layout
		if($ReportExtra){
			
			$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableTop .= "<tr><td height='165px' valign='bottom'>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "</table>";
			
			$TableBottom = "<table width='73%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableBottom .= "<tr valign='top'><td>".$SignatureTable."</td></tr>";
			$TableBottom .= $FooterRow;
			$TableBottom .= "</table>";
			
			$x = "";
			$x .= "<tr><td>";
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					$x .= "<tr height='750px' valign='top'><td>".$TableTop."</td></tr>";
					$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
		
		# Main Report
		} else {
			
			$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$TableTop .= "<tr><td height='210' valign='bottom'>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			//$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
			$TableTop .= "</table>";
			
			$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
			$TableBottom .= $FooterRow;
			$TableBottom .= "</table>";
			
			$x = "";
			$x .= "<tr><td>";
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					$x .= "<tr height='865px' valign='top'><td>".$TableTop."</td></tr>";
					$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
		}
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		/*
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			$SchoolName = '';
			//$SchoolName = GET_SCHOOL_NAME();
			$ChineseDisplay = str_replace(' ', "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $eReportCard['Template']['SchoolNameCh']);
			$SchoolName .= '<span class="school_name_title_ch">'.$ChineseDisplay.'</span>';
			$SchoolName .= '<br />';
			$SchoolName .= '<span class="school_name_title_en">'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</span>';
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='school_name_title' align='center'>".$SchoolName."</td></tr>\n";
					
					$TitleTable .= "<tr><td nowrap='nowrap' class='student_report_title' align='center'>".$eReportCard['Template']['ReportTitleCh']."</td></tr>\n";
					
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					
						
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
		*/
		
//		$this->General_Setting[Calculation][OrderTerm] = 1;
//		debug_pr($this->General_Setting); 

		if($ReportID)
		{	
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			# Extra Report Checking
			$ReportExtra = !$ReportSetting['isMainReport'];
			
			$TitleTable = '';
			$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";

			# Extra Report
			if($ReportExtra){
			
				global $PATH_WRT_ROOT;
				
				# Get school year
				$ObjYear = new academic_year($this->schoolYearID);
				$targetSchoolYear = $ObjYear->Get_Academic_Year_Name();
				# Get Semester
				$targetSemester = $ReportSetting['Semester'];
				$targetSemester = $this->returnSemesters($targetSemester);
				
				# Dispaly School Term 
				if(strpos($targetSemester,'1') !== false || strpos($targetSemester,'First') !== false || strpos($targetSemester,'上') !== false)
					$targetSemester = 'First';
				else 
					$targetSemester = 'Second';
					
				# Construct table
				$TitleTable .= "<tr><td width='7%'>&nbsp;</td>";
					$TitleTable .= "<td><img src='$PATH_WRT_ROOT/file/reportcard2008/templates/munsang_college.png' width='102px' height='117px'></td>";
					$TitleTable .= "<td align='center'>";
						$TitleTable .= "<div class='form_test_cust_report_title' style='display:table'><div style='vertical-align: middle; display: table-cell;'><span>Munsang College</span></div></div>";
						$TitleTable .= "<div class='form_test_cust_report_title' style='display:table'><div style='vertical-align: middle; display: table-cell;'><span>".$targetSemester." Term Form Test Report (".$targetSchoolYear.")</span></div></div>";
					$TitleTable .= "</td>";
					$TitleTable .= "<td width='20%'>&nbsp;</td>";
				$TitleTable .= "</tr>";
				
			# Main Report
			} else {
				$TitleTable .= "<tr class='student_report_title'><td align='center' valign='bottom'>".$ReportTitle."</td></tr>";
			}
			$TitleTable .= "</table>";
		}
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			# Extra Report Checking
			$ReportExtra = !$ReportSetting['isMainReport'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				//$data['Name'] = $lu->UserName2Lang('ch', 2);
				$data['Name'] = $lu->EnglishName;
				
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				
				# added 'F.' in front of the class name as munsang only input '1' as the class name
				$data['Class'] = 'F.'.$thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				/*
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				*/
				$data['Class'] .= " (".$thisClassNumber.")";
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}

			$StudentInfoTable = '';
			$DefaultStudentInfo = array('Class', 'Name');
			if(!empty($DefaultStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= $ReportExtra? "<table width='73%' border='0' cellpadding='0' cellspacing='0' align='center'>" : "<table width='65%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				
				for($i=0; $i<sizeof($DefaultStudentInfo); $i++)
				{
					$SettingID = trim($DefaultStudentInfo[$i]);
					if(in_array($SettingID, $DefaultStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
							$align = 'left';
							$width = $ReportExtra? '30%' : '40%';
						}
						else {
							$align = 'right';
							$width = $ReportExtra? '70%' : '60%';
						}
						
						//$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						
						# Style for Report
						$reportClass = $ReportExtra? 'form_test_cust_student_info_text' : 'student_info_text';
						$LineHeight = $ReportExtra? '12px' : $LineHeight;
						
						# Info Display
						$StudentInfoTable .= "<td class='".$reportClass."' $colspan width='".$width."' valign='top' align='".$align."' height='{$LineHeight}'>".$Title;							
						
						if ($SettingID != 'ClassNo'){
							$StudentInfoTable .= " : ";
							if($ReportExtra){
								$StudentInfoTable .= "　";
							}
						}
						
						$StudentInfoTable .= ($data[$SettingID] ? strtoupper($data[$SettingID]) : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
			
			/*
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width=60%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && in_array($SettingID, $DefaultStudentInfo)===false)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						$StudentInfoTable .= "<td class='student_info_text' $colspan width='25%' valign='top' align='center' height='{$LineHeight}'>".$Title." : ";
						$StudentInfoTable .= ($data[$SettingID] ? strtoupper($data[$SettingID]) : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
			*/
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		# Extra Report Checking
		$ReportExtra = !$ReportSetting['isMainReport'];
		
		# define table header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		

		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);

		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
		
		# define subject column			
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		
		$tableWidth = ($ReportType == 'T')? '65%' : '80%';
		$tableWidth = $ReportExtra? '73%' : $tableWidth;
		
		$DetailsTable = "";
		$DetailsTable .= "<table width='$tableWidth' align='center' border='0' cellspacing='0' cellpadding='0' >";
		
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		# Line Seperator for extra report
		if($ReportExtra){
			$lineSepar = "<td style='border-bottom: 2px solid black; font-size: 5px;'>&nbsp;</td>";
			$DetailsTable .= "<tr>".$lineSepar.$lineSepar.$lineSepar.$lineSepar.$lineSepar."</tr>";
			$DetailsTable .= "<tr><td>&nbsp;</td></tr>";
		}
				
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# If extra report, then don't display subject component
			if($ReportExtra && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID] == false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment && !$ReportExtra) {
					//$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext  $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext  $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		if(!$ReportExtra){
			$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		} else {
			# Line Seperator for extra report
			$lineSepar = "<td style='border-bottom: 2px solid black;'>&nbsp;</td>";
			$DetailsTable .= "<tr>".$lineSepar.$lineSepar.$lineSepar.$lineSepar.$lineSepar."</tr><tr><td>&nbsp;</td></tr></table>";
		}
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$LineHeight = $ReportSetting['LineHeight'];
		# Extra Report Checking
		$ReportExtra = !$ReportSetting['isMainReport'];
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$x = '';
		
		if ($ReportType=="T")
		{	
			# Extra Report Table Column
			if($ReportExtra){
				
				$LineHeight = "12px";
				
				$x = '';
				$x .= "<tr>";
					# Subject
					$x .= "<td valign='middle' align='center' class='form_test_cust_small_title' height='{$LineHeight}' with='40%'><u>".$eReportCard['Template']['Subject']."</u></td>";
					# Score
					$x .= "<td valign='middle' align='center' class='form_test_cust_small_title' height='{$LineHeight}' width='15%'><u>".$eReportCard['Template']['Score']."</u></td>";
					# Grade
					$x .= "<td valign='middle' align='center' class='form_test_cust_small_title' height='{$LineHeight}' width='16%'><u>".$eReportCard['Template']['Grade']."</u></td>";
					# Position
					$x .= "<td valign='middle' align='center' class='form_test_cust_small_title' height='{$LineHeight}' width='15%'><u>".$eReportCard['Template']['Position']."</u></td>";
					# No. of pupils
					$x .= "<td valign='middle' align='center' class='form_test_cust_small_title' height='{$LineHeight}' width='14%'><u>".$eReportCard['Template']['NoOfPupils']."</u></td>";
				$x .= "</tr>";
			
			} else {
				# Main Term Report Table Column
				
				$x .= "<tr>";
					# Empty Column for Subject
					$x .= "<td valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>&nbsp;</td>";
					# Empty Column for Subject Component Mark
					$x .= "<td valign='middle' class='small_title' height='{$LineHeight}' width='5%'>&nbsp;</td>";
					# Columns for First Examination
					//$thisTitle = ($FormNumber==7 || $FormNumber==5)? $eReportCard['Template']['YearResult'] : $eReportCard['Template']['FirstExamination'];
					//2012-0223-1022-41066
					//$thisTitle = ($FormNumber==7)? $eReportCard['Template']['YearResult'] : $eReportCard['Template']['FirstExamination'];
					$thisTitle = ($FormNumber==7 || $FormNumber==6)? $eReportCard['Template']['YearResult'] : $eReportCard['Template']['FirstExamination'];
					$x .= "<td colspan='3' valign='middle' align='center' class='small_title' height='{$LineHeight}'><u>".$thisTitle."</u></td>";
				$x .= "</tr>";
				
				$x .= "<tr>";
					# Subject
					$x .= "<td valign='middle' class='small_title' height='{$LineHeight}'>".$eReportCard['Template']['Subject']."</td>";
					# Empty Column for Subject Component Mark
					$x .= "<td valign='middle' class='small_title' height='{$LineHeight}'>&nbsp;</td>";
					# Grade
					$x .= "<td valign='middle' align='center' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'><u>".$eReportCard['Template']['Grade']."</u></td>";
					# Position
					$x .= "<td valign='middle' align='center' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'><u>".$eReportCard['Template']['Position']."</u></td>";
					# No. of pupils
					$x .= "<td valign='middle' align='center' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' nowrap><u>".$eReportCard['Template']['NoOfPupils']."</u></td>";
				$x .= "</tr>";
				
				$e = 2;
			
			}
		}
		else
		{
			$mark_width = '10%';
			$subject_width = '35%';
			
			$x .= "<tr>";
				# Empty Column for Subject
				$x .= "<td valign='middle' class='small_title_consolidated' height='{$LineHeight}' width='$subject_width'>&nbsp;</td>";
				# Columns for First Exam
				$x .= "<td colspan='2' valign='middle' align='center' class='small_title_consolidated' height='{$LineHeight}'>".$eReportCard['Template']['FirstExam']."</td>";
				# Columns for Second Exam
				$x .= "<td colspan='2' valign='middle' align='center' class='small_title_consolidated' height='{$LineHeight}'>".$eReportCard['Template']['SecondExam']."</td>";
				# Columns for Year Result
				$x .= "<td colspan='2' valign='middle' align='center' class='small_title_consolidated' height='{$LineHeight}'>".$eReportCard['Template']['YearResult']."</td>";
				# Empty Column for No. of pupils
				$x .= "<td valign='middle' align='center' class='small_title_consolidated' height='{$LineHeight}'>&nbsp;</td>";
			$x .= "</tr>";
			
			$x .= "<tr class='small_title_consolidated'>";
				# Subject
				$x .= "<td valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>".$eReportCard['Template']['Subject']."</td>";
				# Grade for First Exam
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' width='$mark_width'><u>".$eReportCard['Template']['Grade']."</u></td>";
				# Mark for First Exam
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' width='$mark_width'><u>".$eReportCard['Template']['Position']."</u></td>";
				# Grade for Second Exam
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' width='$mark_width'><u>".$eReportCard['Template']['Grade']."</u></td>";
				# Mark for Second Exam
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' width='$mark_width'><u>".$eReportCard['Template']['Position']."</u></td>";
				# Grade for Year Result
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' width='$mark_width'><u>".$eReportCard['Template']['Grade']."</u></td>";
				# Mark for Year Result
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' width='$mark_width'><u>".$eReportCard['Template']['Position']."</u></td>";
				
				# No. of pupils
				$x .= "<td valign='middle' align='center' height='{$LineHeight}' nowrap><u>".$eReportCard['Template']['NoOfPupils']."</u></td>";
			$x .= "</tr>";
		}
		
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		# Extra Report Checking
		$ReportExtra = !$ReportSetting['isMainReport'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
		
 		$x = array(); 
		$isFirst = 1;
		
		# Style
		$titleClass = $ReportExtra? "form_test_cust_tabletext" : "tabletext";
		$LineHeight = $ReportExtra? "12px" : $LineHeight;
		
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
				 		
				 		$style_prefix = '';
				 		$style_suffix = '';
			 		}
			 		else
			 		{
				 		$style_prefix = '<i>';
				 		$style_suffix = '</i>';
			 		}
			 		
			 		//$css_bo = ($Prefix)? "" : "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
		 				$v = $style_prefix.$v.$style_suffix;
		 				
		 				$t .= "<td class='{$titleClass} {$css_border_top}' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='{$titleClass}' nowrap>$v</td>";
							$t .= "</tr></table>";
						$t .= "</td>\n";
						
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='')
	{
 		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
 		
 		if($ReportID)
 		{	
 			# Retrieve Display Settings
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$SemID = $ReportSetting['Semester'];
 			$ReportType = $SemID == "F" ? "W" : "T";
			# Extra Report Checking
			$ReportExtra = !$ReportSetting['isMainReport'];
		}
		
		$ClassTeacherDisplay = 'XXX XXX XXX';
		if ($StudentID)
		{
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$thisClassID = $StudentInfoArr[0]['ClassID'];
			
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			$ObjYearClass = new year_class($thisClassID, $GetYearDetail=false, $GetClassTeacherList=true);
			
			# Assuming one class teacher only
			$TeacherArr = $ObjYearClass->ClassTeacherList;
			$thisTeacherID = $TeacherArr[0]['UserID'];
			
			$lu = new libuser($thisTeacherID);
			$ClassTeacherDisplay = '('.$lu->EnglishName.")";
		}
 		
		$signatureLine = '_________________________________';	
		$signatureLine = $ReportExtra? '_______________________________' : $signatureLine;	
		
		# Style
		$classStyle = $ReportExtra? 'form_test_cust_signature_title' : 'signature_title';
		$principle = $ReportExtra? $eReportCard['Template']['PrincipalChop'] : $eReportCard['Template']['Principal'];

		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			$SignatureTable .= "<tr>";
				$SignatureTable .= "<td width='10%'>&nbsp;</td>";
				
				### Parent's/Guardian's Signature
				$SignatureTable .= "<td valign='bottom' align='center' width='30%'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' height='50' valign='bottom'>".$signatureLine."</td></tr>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' valign='bottom'>".$eReportCard['Template']['ParentGuardian']."</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
				
				$SignatureTable .= "<td width='20%'>&nbsp;</td>";
				
				/* Moved to the right hand side of ECA display
				 * Replaced by the Form Teacher Display
				### Remarks (Promotion) for Consolidated report
				if ($ReportType == 'W')
				{
					$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
					$ary = $OtherInfoDataAry[$StudentID];
					$LastestTermID = $this->Get_Last_Semester_Of_Report($ReportID);
					$Promotion = $ary[$LastestTermID]['Promotion'];
					
					if(trim($Promotion) == "")
					{
						# commented by Ivan on 20091221
						# Get the number from 1st Term + 2nd Term instead of 2nd Term only (TBD)
						$Promotion = $this->getPromotionStatus($ReportID, $StudentID, $ary, $LastestTermID);
					}
					
					$SignatureTable .= "<td valign='bottom' align='center'>";
						$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
							$SignatureTable .= "<tr><td align='center' class='small_title' height='50' valign='bottom'>";
								$SignatureTable .= $eReportCard['Template']['Remark'].': <u>'.$Promotion.'&nbsp;&nbsp;</u>';
							$SignatureTable .= "</td></tr>";
							$SignatureTable .= "<tr><td align='center' class='signature_title' valign='bottom'>&nbsp;</td></tr>";
						$SignatureTable .= "</table>";
					$SignatureTable .= "</td>";
				}
				else
				{
					$SignatureTable .= "<td>&nbsp;</td>";
				}
				*/
				
				### Form Teacher
				$SignatureTable .= "<td valign='bottom' align='center'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' height='50' valign='bottom'>".$signatureLine."</td></tr>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' valign='bottom'>";
							$SignatureTable .= $eReportCard['Template']['ClassTeacher'].' '.$ClassTeacherDisplay;
						$SignatureTable .= "</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
				
				$SignatureTable .= "<td width='10%'>&nbsp;</td>";
			$SignatureTable .= "</tr>";
			
			$SignatureTable .= "<tr>";
				$SignatureTable .= "<td width='20%'>&nbsp;</td>";
				
				/* Replaced by Issue Date Display
				### Form Teacher
				$SignatureTable .= "<td valign='bottom' align='center'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
						$SignatureTable .= "<tr><td align='center' class='signature_title' height='50' valign='bottom'>".$signatureLine."</td></tr>";
						$SignatureTable .= "<tr><td align='center' class='signature_title' valign='bottom'>";
							$SignatureTable .= $eReportCard['Template']['ClassTeacher'].' '.$ClassTeacherDisplay;
						$SignatureTable .= "</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
				*/
				
				### Issue Date
				$SignatureTable .= "<td valign='bottom' align='center'>";
					$SignatureTable .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle." border_bottom' height='50' valign='bottom' width='100%'>".date('j-n-Y', strtotime($Issued))."</td></tr>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' valign='bottom'>";
							$SignatureTable .= $eReportCard['Template']['IssueDate'];
						$SignatureTable .= "</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
				
				$SignatureTable .= "<td>&nbsp;</td>";
				
				### Principal
				$SignatureTable .= "<td valign='bottom' align='center'>";
					$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' height='50' valign='bottom'>".$signatureLine."</td></tr>";
						$SignatureTable .= "<tr><td align='center' class='".$classStyle."' valign='bottom'>".$principle."</td></tr>";
					$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
				
				$SignatureTable .= "<td width='20%'>&nbsp;</td>";
			$SignatureTable .= "</tr>";
			
			### Issue Date & Ket to grade table
			$SignatureTable .= "<tr>";
				$SignatureTable .= "<td>&nbsp;</td>";
				$SignatureTable .= "<td class='signature_title' align='center' valign='top'>";
					//$SignatureTable .= $eReportCard['Template']['IssueDate'].': '.date('j-n-Y', strtotime($Issued));
					$SignatureTable .= '&nbsp;';
				$SignatureTable .= "</td>";
				$SignatureTable .= "<td>&nbsp;</td>";
				$SignatureTable .= "<td colspan='2'>";
					//$SignatureTable .= $this->Get_Key_To_Grade_Table();
					$SignatureTable .= "&nbsp;";
				$SignatureTable .= "</td>";
			$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	# Footer for Extra Term Report
	function getFooter($ReportID='',$PrintTemplateType=''){
		
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$x = '';
		
		if ($ReportID){
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID); 
			$ReportExtra = !$ReportSetting['isMainReport'];
		}
		
		if($ReportExtra){
			$x .= "<tr height='130px' valign='top'>
					<td>
						<table width='100%'>
							<tr>";
							$x .= "<td width='50%' height='75px'>&nbsp;</td>";
							$x .= "<td height='75px'>
									<table class='form_test_cust_footer'>";
									$x .= "<tr>
											<td>Key to Grade:</td>
											<td>A=Excellent</td>
											<td>B=Very good</td>
										</tr>";
									$x .= "<tr>
												<td>&nbsp;</td>
												<td>C=Good</td>
												<td>D=fair</td>
										</tr>";
									$x .= "<tr>
												<td>&nbsp;</td>
												<td>*E=Unsatisfactory</td>
										</tr>";
								$x .= "</table>
									</td>";
						$x .= "</tr>
							</table>
						</td>
					</tr>";
		}
		return $x;
		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ColumnData);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		//if ($CalOrder == 2) {
		
			//foreach ($ColumnTitle as $ColumnID => $ColumnName) {
			for ($i=0; $i<$numOfColumn; $i++) {
				$thisReportColumnID = $ColumnData[$i]['ReportColumnID'];
				$thisTermID = $ColumnData[$i]['SemesterNum'];
				
				$thisTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisTermID, $isMainReport=1);
				$thisTermReportID = $thisTermReportInfoArr['ReportID'];
				
				$thisColumnResult = $this->getReportResultScore($thisTermReportID, 0, $StudentID, '', 1,1);
				
				$columnTotal[$thisReportColumnID] = $StudentID ? $this->Get_Score_Display_HTML($thisColumnResult['GrandTotal'], $thisTermReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$thisReportColumnID] = $StudentID ? $this->Get_Score_Display_HTML($thisColumnResult['GrandAverage'], $thisTermReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
				
				$columnClassPos[$thisReportColumnID] = $StudentID ? $this->Get_Score_Display_HTML($thisColumnResult['OrderMeritClass'], $thisTermReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$thisReportColumnID] = $StudentID ? $this->Get_Score_Display_HTML($thisColumnResult['OrderMeritForm'], $thisTermReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnClassNumOfStudent[$thisReportColumnID] = $StudentID ? $this->Get_Score_Display_HTML($thisColumnResult['ClassNoOfStudent'], $thisTermReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$thisReportColumnID] = $StudentID ? $this->Get_Score_Display_HTML($thisColumnResult['FormNoOfStudent'], $thisTermReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			}
		//}
		
		$first = 1;
		
		if ($ReportType=='T')
		{
			/*
			# Overall Result
			if($ShowGrandTotal)
			{
				$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
				$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
				
				$first = 0;
			}
		
			# Average Mark 
			if($ShowGrandAvg)
			{
				$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
				
				$first = 0;
			}
			
			# Number of Students in Class 
			if($ShowNumOfStudentClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
				
				$first = 0;
			}
			*/
			
			# Position in Class 
			if($ShowOverallPositionClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first, $ClassNumOfStudent);
				
				$first = 0;
			}
			
			# Position in Form 
			if($ShowOverallPositionForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first, $FormNumOfStudent);
				
				$first = 0;
			}
			
			/*
			# Number of Students in Form 
			if($ShowNumOfStudentForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
				
				$first = 0;
			}
			*/
		}
		else
		{
			### Position in class
			if($ShowOverallPositionClass)
			{
				$x .= "<tr>";
					$x .= "<td class='tabletext border_top_dotted' height='{$LineHeight}'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ClassPositionEn'] ."</td></tr>";
						$x .= "</table>";
					$x .= "</td>";
					
					# Term Position in Form
					$NumOfStudentArr = array();
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					
						$x .= '<td class="tabletext border_top_dotted">&nbsp;</td>';
						$x .= '<td class="tabletext border_top_dotted" align="center">'.$columnClassPos[$ColumnID].'</td>';
						
						$NumOfStudentArr[] = $columnClassNumOfStudent[$ColumnID];
					}
					
					# Year Result Position in Class
					$x .= '<td class="tabletext border_top_dotted">&nbsp;</td>';
					$x .= '<td class="tabletext border_top_dotted" align="center">'.$ClassPosition.'</td>';
					
					# Num Of Class Student
					$NumOfPupilDisplay = $NumOfStudentArr[0].' / '.$NumOfStudentArr[1].' / '.$ClassNumOfStudent;
					$x .= '<td class="tabletext border_top_dotted" align="center">'.$NumOfPupilDisplay.'</td>';
					
				$x .= "</tr>";
			}
			
			if($ShowOverallPositionForm)
			{
				$x .= "<tr>";
					$x .= "<td class='tabletext border_top_dotted' height='{$LineHeight}'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['FormPositionEn'] ."</td></tr>";
						$x .= "</table>";
					$x .= "</td>";
					
					# Term Position in Form
					$NumOfStudentArr = array();
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					
						$x .= '<td class="tabletext border_top_dotted">&nbsp;</td>';
						$x .= '<td class="tabletext border_top_dotted" align="center">'.$columnFormPos[$ColumnID].'</td>';
						
						$NumOfStudentArr[] = $columnFormNumOfStudent[$ColumnID];
					}
					
					# Year Result Position in Form
					$x .= '<td class="tabletext border_top_dotted">&nbsp;</td>';
					$x .= '<td class="tabletext border_top_dotted" align="center">'.$FormPosition.'</td>';
					
					# Num Of Form Student
					$NumOfPupilDisplay = $NumOfStudentArr[0].' / '.$NumOfStudentArr[1].' / '.$FormNumOfStudent;
					$x .= '<td class="tabletext border_top_dotted" align="center">'.$NumOfPupilDisplay.'</td>';
					
				$x .= "</tr>";
			}
		}
		
		### An empty row
		$x .= "<tr><td colspan='20'>&nbsp;</td></tr>";
		
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
		$OtherInfoDataAry = $OtherInfoDataAry[$StudentID];
		
		
		### Absent
		$x .= "<tr height='{$LineHeight}' class='tabletext'>";
			
			if ($ReportType=='T')
			{
				$thisLate = $OtherInfoDataAry[$SemID]['Days Absent']?$OtherInfoDataAry[$SemID]['Days Absent']:0;
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['DayEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}'  width='52' class='tabletext'>". $eReportCard['Template']['DaysAbsentEn'] ."</td>";
							$x .= "<td width='15'>&nbsp;</td>";
							$x .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
							
				//$x .= "<td align='center'>".$thisDisplay."</td>";
				//$x .= "<td>&nbsp;</td>";
				//$x .= "<td>&nbsp;</td>";
			}
			else
			{
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['DaysAbsentEn'] ."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisLate = $OtherInfoDataAry[$thisTermID]['Days Absent']?$OtherInfoDataAry[$thisTermID]['Days Absent']:0;
					$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['DayEn']);
					$thisDisplay = $thisLate.' '.$thisUnit;
					
					$x .= "<td>&nbsp;</td>";
					$x .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
				}
				
				# Overall Value
				$thisLate = $OtherInfoDataAry[0]['Days Absent']?$OtherInfoDataAry[0]['Days Absent']:0;
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['DayEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$x .= "<td>&nbsp;</td>";
				$x .= "<td align='left'>".$thisDisplay."</td>";
			}
		$x .= "</tr>";
		
		### Late
		$x .= "<tr height='{$LineHeight}' class='tabletext'>";
			if ($ReportType=='T')
			{
				$thisLate = $OtherInfoDataAry[$SemID]['Time Late']?$OtherInfoDataAry[$SemID]['Time Late']:0;
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}'  width='52' class='tabletext'>". $eReportCard['Template']['TimesLateEn'] ."</td>";
							$x .= "<td width='15'>&nbsp;</td>";
							$x .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
							
				//$x .= "<td align='center'>".$thisDisplay."</td>";
				//$x .= "<td>&nbsp;</td>";
				//$x .= "<td>&nbsp;</td>";
			}
			else
			{
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['TimesLateEn'] ."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisLate = $OtherInfoDataAry[$thisTermID]['Time Late']?$OtherInfoDataAry[$thisTermID]['Time Late']:0;
					$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
					$thisDisplay = $thisLate.' '.$thisUnit;
					
					$x .= "<td>&nbsp;</td>";
					$x .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
				}
				
				# Overall Value
				$thisLate = $OtherInfoDataAry[0]['Time Late']?$OtherInfoDataAry[0]['Time Late']:0;
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$x .= "<td>&nbsp;</td>";
				$x .= "<td align='left'>".$thisDisplay."</td>";
			}
		$x .= "</tr>";
		
		
		### An empty row
		//$x .= "<tr><td colspan='20'>&nbsp;</td></tr>";
		
		### Conduct
		if ($ReportType=='T')
		{
			$eDisDataArr = $this->Get_eDiscipline_Data($SemID, $StudentID);
		
			# Conduct Title and Diligence
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}' class='tabletext' width='52'>". $eReportCard['Template']['ConductEn'] ." </td>";
							$x .= "<td width='15'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}' class='tabletext'><i>". $eReportCard['Template']['DiligenceEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td align='center' nowrap><i>".$this->Get_Conduct_Wordings($eDisDataArr['Diligence'])."</i></td>";
			$x .= "</tr>";
			
			# Discipline
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}'class='tabletext' width='52'>&nbsp;</td>";
							$x .= "<td width='15'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['DisciplineEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td align='center' nowrap><i>".$this->Get_Conduct_Wordings($eDisDataArr['Discipline'])."</i></td>";
			$x .= "</tr>";
			
			# Manner
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}'class='tabletext' width='52'>&nbsp;</td>";
							$x .= "<td width='15'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['MannerEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td align='center' nowrap><i>".$this->Get_Conduct_Wordings($eDisDataArr['Manner'])."</i></td>";
			$x .= "</tr>";
			
			# Sociability
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}'class='tabletext' width='52'>&nbsp;</td>";
							$x .= "<td width='15'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['SociabilityEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td align='center' nowrap><i>".$this->Get_Conduct_Wordings($eDisDataArr['Sociability'])."</i></td>";
			$x .= "</tr>";
		}
		else
		{
			foreach($ColumnData as $k1 => $d1)
			{
				$thisTermID = $d1['SemesterNum'];
				$eDisDataArr[$thisTermID] = $this->Get_eDiscipline_Data($thisTermID, $StudentID);
			}
			
			# Conduct Title
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['ConductEn'] ." </td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			
			# Diligence
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td width='45'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['DiligenceEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				$x .= "<td>&nbsp;</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisValue = $this->Get_Conduct_Wordings($eDisDataArr[$thisTermID]['Diligence']);
					
					//$x .= "<td>&nbsp;</td>";
					$x .= "<td colspan='2' align='left'><i>".$thisValue."</i></td>";
				}
			$x .= "</tr>";
			
			# Discipline
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td width='45'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['DisciplineEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				$x .= "<td>&nbsp;</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisValue = $this->Get_Conduct_Wordings($eDisDataArr[$thisTermID]['Discipline']);
					
					//$x .= "<td>&nbsp;</td>";
					$x .= "<td colspan='2' align='left'><i>".$thisValue."</i></td>";
				}
			$x .= "</tr>";
			
			# Manner
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td width='45'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['MannerEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				$x .= "<td>&nbsp;</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisValue = $this->Get_Conduct_Wordings($eDisDataArr[$thisTermID]['Manner']);
					
					//$x .= "<td>&nbsp;</td>";
					$x .= "<td colspan='2' align='left'><i>".$thisValue."</i></td>";
				}
			$x .= "</tr>";
			
			# Sociability
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td width='45'>&nbsp;</td>";
							$x .= "<td height='{$LineHeight}'class='tabletext'><i>". $eReportCard['Template']['SociabilityEn'] ."</i></td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				$x .= "<td>&nbsp;</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisValue = $this->Get_Conduct_Wordings($eDisDataArr[$thisTermID]['Sociability']);
					
					//$x .= "<td>&nbsp;</td>";
					$x .= "<td colspan='2' align='left'><i>".$thisValue."</i></td>";
				}
			$x .= "</tr>";
		}
		
		### An empty row
		$x .= "<tr><td colspan='20'>&nbsp;</td></tr>";
		
		$StudentProfileDataArr = $this->Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=true);
		### Merit
		$displayMeritRow = false;
		$meritRow = '';
		$meritRow .= "<tr height='{$LineHeight}' class='tabletext'>";
			if ($ReportType=='T')
			{
				//$thisLate = $OtherInfoDataAry[$SemID]['Merits'];
				$thisLate = $StudentProfileDataArr['Merits'];
				$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				if ($thisLate != '' && $thisLate > 0)
					$displayMeritRow = true;
				
				$meritRow .= "<td>";
					$meritRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$meritRow .= "<tr>";
							$meritRow .= "<td height='{$LineHeight}'  width='52' class='tabletext'>". $eReportCard['Template']['MeritEn'] ."</td>";
							$meritRow .= "<td width='15'>&nbsp;</td>";
							$meritRow .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
						$meritRow .= "</tr>";
					$meritRow .= "</table>";
				$meritRow .= "</td>";
								
				//$meritRow .= "<td align='center'>".$thisDisplay."</td>";
				//$meritRow .= "<td>&nbsp;</td>";
				//$meritRow .= "<td>&nbsp;</td>";
			}
			else
			{
				$meritRow .= "<td>";
					$meritRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$meritRow .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['MeritEn'] ."</td></tr>";
					$meritRow .= "</table>";
				$meritRow .= "</td>";
				
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisTermID, $isMainReport=1);
					$thisReportID = $thisReportInfoArr['ReportID'];
										
					//$thisLate = $OtherInfoDataAry[$thisTermID]['Merits'];
					$StudentProfileTermDataArr = $this->Get_Student_Profile_Data($thisReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=true);
					$thisLate = $StudentProfileTermDataArr['Merits'];
					$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
					$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
					$thisDisplay = $thisLate.' '.$thisUnit;
					
					if ($thisLate != '' && $thisLate > 0)
						$displayMeritRow = true;
					
					$meritRow .= "<td>&nbsp;</td>";
					$meritRow .= "<td align='left'>".$thisDisplay."</td>";
				}
				
				# Overall Value
				//$thisLate = $OtherInfoDataAry[0]['Merits'];
				$thisLate = $StudentProfileDataArr['Merits'];
				//$thisLate = ($thisLate == '')? 0 : $thisLate;
				$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$meritRow .= "<td>&nbsp;</td>";
				$meritRow .= "<td align='left'>".$thisDisplay."</td>";
			}
		$meritRow .= "</tr>";
		
		if ($displayMeritRow == true)
			$x .= $meritRow;
			
			
		### Minor achievement
		$displayMeritRow = false;
		$meritRow = '';
		$meritRow .= "<tr height='{$LineHeight}' class='tabletext'>";
			
			if ($ReportType=='T')
			{
				//$thisLate = $OtherInfoDataAry[$SemID]['MinorAchievement'];
				$thisLate = $StudentProfileDataArr['MinorAchievement'];
				$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				if ($thisLate != '' && $thisLate > 0)
					$displayMeritRow = true;
					
				$meritRow .= "<td>";
					$meritRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$meritRow .= "<tr>";
							$meritRow .= "<td height='{$LineHeight}'  width='52' class='tabletext'>". $eReportCard['Template']['MinorAchievementEn'] ."</td>";
							$meritRow .= "<td width='15'>&nbsp;</td>";
							$meritRow .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
						$meritRow .= "</tr>";
					$meritRow .= "</table>";
				$meritRow .= "</td>";
				
				//$meritRow .= "<td align='center'>".$thisDisplay."</td>";
				//$meritRow .= "<td>&nbsp;</td>";
				//$meritRow .= "<td>&nbsp;</td>";
			}
			else
			{
				$meritRow .= "<td>";
					$meritRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$meritRow .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['MinorAchievementEn'] ."</td></tr>";
					$meritRow .= "</table>";
				$meritRow .= "</td>";
			
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisTermID, $isMainReport=1);
					$thisReportID = $thisReportInfoArr['ReportID'];
					
					//$thisLate = $OtherInfoDataAry[$thisTermID]['MinorAchievement'];
					$StudentProfileTermDataArr = $this->Get_Student_Profile_Data($thisReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=true);
					$thisLate = $StudentProfileTermDataArr['MinorAchievement'];
					$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
					$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
					$thisDisplay = $thisLate.' '.$thisUnit;
					
					if ($thisLate != '' && $thisLate > 0)
						$displayMeritRow = true;
					
					$meritRow .= "<td>&nbsp;</td>";
					$meritRow .= "<td align='left'>".$thisDisplay."</td>";
				}
				
				# Overall Value
				//$thisLate = $OtherInfoDataAry[0]['MinorAchievement'];
				$thisLate = $StudentProfileDataArr['MinorAchievement'];
				//$thisLate = ($thisLate == '')? 0 : $thisLate;
				$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$meritRow .= "<td>&nbsp;</td>";
				$meritRow .= "<td align='left'>".$thisDisplay."</td>";
			}
		$meritRow .= "</tr>";
		
		if ($displayMeritRow == true)
			$x .= $meritRow;
			
			
		### Major Achievement
		$displayMeritRow = false;
		$meritRow = '';
		$meritRow .= "<tr height='{$LineHeight}' class='tabletext'>";
			if ($ReportType=='T')
			{
				//$thisLate = $OtherInfoDataAry[$SemID]['MajorAchievement'];
				$thisLate = $StudentProfileDataArr['MajorAchievement'];
				$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				if ($thisLate != '' && $thisLate > 0)
					$displayMeritRow = true;
					
				$meritRow .= "<td>";
					$meritRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$meritRow .= "<tr>";
							$meritRow .= "<td height='{$LineHeight}'  width='52' class='tabletext'>". $eReportCard['Template']['MajorAchievementEn'] ."</td>";
							$meritRow .= "<td width='15'>&nbsp;</td>";
							$meritRow .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
						$meritRow .= "</tr>";
					$meritRow .= "</table>";
				$meritRow .= "</td>";
				
//				$meritRow .= "<td align='center'>".$thisDisplay."</td>";
//				$meritRow .= "<td>&nbsp;</td>";
//				$meritRow .= "<td>&nbsp;</td>";
			}
			else
			{
				$meritRow .= "<td>";
					$meritRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$meritRow .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $eReportCard['Template']['MajorAchievementEn'] ."</td></tr>";
					$meritRow .= "</table>";
				$meritRow .= "</td>";
			
				# Term Value
				foreach($ColumnData as $k1 => $d1)
				{
					$thisTermID = $d1['SemesterNum'];
					$thisReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisTermID, $isMainReport=1);
					$thisReportID = $thisReportInfoArr['ReportID'];
					
					//$thisLate = $OtherInfoDataAry[$thisTermID]['MajorAchievement'];
					$StudentProfileTermDataArr = $this->Get_Student_Profile_Data($thisReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true, $returnTotalUnit=true);
					$thisLate = $StudentProfileTermDataArr['MajorAchievement'];
					$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
					$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
					$thisDisplay = $thisLate.' '.$thisUnit;
					
					if ($thisLate != '' && $thisLate > 0)
						$displayMeritRow = true;
					
					$meritRow .= "<td>&nbsp;</td>";
					$meritRow .= "<td align='left'>".$thisDisplay."</td>";
				}
				
				# Overall Value
				//$thisLate = $OtherInfoDataAry[0]['MajorAchievement'];
				$thisLate = $StudentProfileDataArr['MajorAchievement'];
				//$thisLate = ($thisLate == '')? 0 : $thisLate;
				$thisLate = ($thisLate == '')? 0 : $this->TrimDeMeritNumber_00($thisLate);
				$thisUnit = $this->Get_Unit_Wording($thisLate, $eReportCard['Template']['TimeEn']);
				$thisDisplay = $thisLate.' '.$thisUnit;
				
				$meritRow .= "<td>&nbsp;</td>";
				$meritRow .= "<td align='left'>".$thisDisplay."</td>";
			}
		$meritRow .= "</tr>";
		
		if ($displayMeritRow == true)
			$x .= $meritRow;
		
		
		### ECA & Promotion Remarks
		//if ($ReportType=='W' || $FormNumber==7 || $FormNumber==5)
		//2012-0223-1022-41066
		//if ($ReportType=='W' || $FormNumber==7)
		if ($ReportType=='W' || $FormNumber==7 || $FormNumber==6)
		{
			/* 20100304 Ivan - Get Promotion Status in DB now
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
			$ary = $OtherInfoDataAry[$StudentID];
			$LastestTermID = $this->Get_Last_Semester_Of_Report($ReportID);
			$Promotion = $ary[$LastestTermID]['Promotion'];
			if(trim($Promotion) == "")
			{
				# commented by Ivan on 20091221
				# Get the number from 1st Term + 2nd Term instead of 2nd Term only (TBD)
				$Promotion = $this->getPromotionStatus($ReportID, $StudentID, $ary, $LastestTermID);
			}
			*/
			
			$PromotionInfoArr = $this->GetPromotionStatusList('', '', $StudentID, $ReportID);
			$Promotion = $PromotionInfoArr[$StudentID]["FinalStatus"]?$eReportCard['Template']['Remark'].": ".$PromotionInfoArr[$StudentID]["FinalStatus"]:'';
					
			### An empty row
			$x .= "<tr><td colspan='20'>&nbsp;</td></tr>";
		
			$x .= "<tr height='{$LineHeight}' class='tabletext'>";
				$x .= "<td colspan='20'>";
					$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr>";
							$x .= "<td height='{$LineHeight}' class='tabletext' width='55%'>". $eReportCard['Template']['ECAEn'] ."&nbsp;&nbsp;</td>";
							if ($ReportType=='W')
								$x .= "<td height='{$LineHeight}' class='small_title' align='center'>". $Promotion."&nbsp;&nbsp;</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
				
			/* Retrieve from eEnrol instead of csv now (20100301 Ivan)
			$x .= "<tr>";
				$LastestTermID = $this->Get_Last_Semester_Of_Report($ReportID);
				$thisECATitleArr = $OtherInfoDataAry[$LastestTermID]['ECA Title'];
				$thisPositionArr = $OtherInfoDataAry[$LastestTermID]['Position'];
				$thisPerformanceArr = $OtherInfoDataAry[$LastestTermID]['Performance'];
				
				if (is_array($thisECATitleArr) == false)
					$thisECATitleArr = array($thisECATitleArr);
				if (is_array($thisPositionArr) == false)
					$thisPositionArr = array($thisPositionArr);
				if (is_array($thisPerformanceArr) == false)
					$thisPerformanceArr = array($thisPerformanceArr);
					
				$thisECATitleArr = trim_array($thisECATitleArr);
				$thisPerformanceArr = trim_array($thisPerformanceArr);
				$thisPositionArr = trim_array($thisPositionArr);
				$numOfECA = count($thisECATitleArr);
				
				$x .= "<td colspan='20'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='left'>";
						
						### ECA Display (2 ECA per row)
						$displayCounter = 0;
						for($i=0; $i<$numOfECA; $i++)
						{
							if ($thisECATitleArr[$i] == '')
								continue;
								
							$thisECATitle = $thisECATitleArr[$i];
							$thisPosition = $thisPositionArr[$i];
							$thisPerformance = $thisPerformanceArr[$i];
							$thisDisplay = $thisECATitle.' ('.$thisPosition.') : '.$thisPerformance;
								
							// First cell => New row
							if ($i % 2 == 0)
							{
								$x .= "<tr>";
									$x .= "<td width='42'>&nbsp;</td>";
							}
								
							$x .= "<td class='tabletext'><i>".$thisDisplay."</i></td>";
						
							// Last cell => End row
							if ($i % 2 == 1 || $i == ($numOfECA - 1))
								$x .= "</tr>";
								
							$displayCounter++;
						}
						
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			*/
			
			$x .= "<tr>";
				$eEnrolDataArr = $this->Get_eEnrolment_Data($StudentID);
				$numOfECA = count($eEnrolDataArr);
				
				$x .= "<td colspan='20'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='left'>";
						
						### ECA Display
						if ($numOfECA == 0)
						{
							$x .= "<tr><td width='67'>&nbsp;</td><td class='tabletext'><i>Nil</i></td></tr>";
						}
						else
						{
							$displayCounter = 0;
							for($i=0; $i<$numOfECA; $i++)
							{
								$thisECATitle = $eEnrolDataArr[$i]['ClubTitle'];
								$thisPosition = $eEnrolDataArr[$i]['RoleTitle'];
								$thisPerformance = $eEnrolDataArr[$i]['Performance'];
								$thisDisplay = $thisECATitle.' ('.$thisPosition.') : '.$thisPerformance;

								// [2020-0515-1448-47235] 1. 如沒有課外活動的表現紀錄便不顯示冒號
								if($this->schoolYear == '2019' && $ReportType == 'W' && $FormNumber > 0 && $FormNumber <= 3) {
								    if($thisPerformance == '') {
                                        $thisDisplay = $thisECATitle.' ('.$thisPosition.')';
                                    }
                                }

								// First cell => New row
								//if ($i % 2 == 0)
								//{
									$x .= "<tr>";
										$x .= "<td width='67'>&nbsp;</td>";
								//}
								        $x .= "<td class='tabletext'><i>".$thisDisplay."</i></td>";
							
								// Last cell => End row
								//if ($i % 2 == 1 || $i == ($numOfECA - 1))
									$x .= "</tr>";
									
								$displayCounter++;
							}
						}
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
		}

		$CCACountArr = $this->Get_Discipline_CCA_Count($ReportID, $StudentID);
		
		### Absent from CCA without application for leave
		$x .= "<tr height='{$LineHeight}' class='tabletext'>";
		
    		if ($ReportType=='T')
    		{
    		    $thisCCACount = $CCACountArr[$SemID];
    		    $thisCCACount = $thisCCACount? $this->TrimDeMeritNumber_00($thisCCACount) : 0;
    		    $thisUnit = $this->Get_Unit_Wording($thisCCACount, $eReportCard['Template']['TimeEn']);
    		    $thisDisplay = $thisCCACount.' '.$thisUnit;
    		    
    		    $x .= "<td colspan='4'>";
        		    $x .= "<table border='0' cellpadding='0' cellspacing='0'>";
            		    $x .= "<tr>";
                		    $x .= "<td height='{$LineHeight}' class='tabletext'>".$CCACountArr['CatName']."</td>";
                		    $x .= "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                		    $x .= "<td height='{$LineHeight}' class='tabletext'>".$thisDisplay."</td>";
            		    $x .= "</tr>";
        		    $x .= "</table>";
    		    $x .= "</td>";
    		}
    		else
    		{
    		    $x .= "<td colspan='2'>";
        		    $x .= "<table border='0' cellpadding='0' cellspacing='0'>";
                        $x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>".$CCACountArr['CatName']."</td></tr>";
        		    $x .= "</table>";
    		    $x .= "</td>";

                // [2020-0515-1448-47235]
    		    $isHideOverallCCADisplay = false;
    		    
    		    # Term Value
    		    foreach($ColumnData as $k1 => $d1)
    		    {
    		        $thisTermID = $d1['SemesterNum'];
    		        
    		        $thisCCACount = $CCACountArr[$thisTermID];
    		        $thisCCACount = $thisCCACount? $this->TrimDeMeritNumber_00($thisCCACount) : 0;
    		        $thisUnit = $this->Get_Unit_Wording($thisCCACount, $eReportCard['Template']['TimeEn']);
    		        $thisDisplay = $thisCCACount.' '.$thisUnit;

                    // [2020-0515-1448-47235] 2. 如下學期沒有"CCA01 - Absent from CCA without application for leave"的紀錄，需要顯示空白
                    if($this->schoolYear == '2019' && $k1 > 0 && $FormNumber > 0 && $FormNumber <= 3) {
                        if($thisCCACount == 0) {
                            $thisDisplay = '&nbsp;';
                            $isHideOverallCCADisplay = true;
                        }
                    }
    		        
    		        $x .= "<td align='left' class='tabletext'>".$thisDisplay."</td>";
    		        $x .= "<td>&nbsp;</td>";
    		    }
    		    
    		    # Overall Value
    		    $thisCCACount = $CCACountArr[0];
    		    $thisCCACount = $thisCCACount? $this->TrimDeMeritNumber_00($thisCCACount) : 0;
    		    $thisUnit = $this->Get_Unit_Wording($thisCCACount, $eReportCard['Template']['TimeEn']);
    		    $thisDisplay = $thisCCACount.' '.$thisUnit;
                // [2020-0515-1448-47235] 2. 如下學期沒有"CCA01 - Absent from CCA without application for leave"的紀錄，需要顯示空白
    		    if($isHideOverallCCADisplay) {
                    $thisDisplay = '&nbsp;';
                }
    		    
    		    $x .= "<td align='left'>".$thisDisplay."</td>";
    		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard,$eRCTemplateSetting;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		# Extra Report Checking
		$ReportExtra 				= !$ReportSetting['isMainReport'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];// Change!
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		# Retrieve Score Settings
		$ScoreSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectScore = $ScoreSetting['SubjectScore']; // 0: Integer , 1: 1 decimal places , 2: 2 decimal places
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);

			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
//					debug_pr($CmpSubjectArr);
				}
				
				# define css
				//$css_border_top = ($isSub)? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
									
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
				$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
				$thisClassPosition = $MarksAry[$SubjectID][0]['OrderMeritClass'];
				$thisClassNumOfStudent = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
				$thisFormPosition = $MarksAry[$SubjectID][0]['OrderMeritForm'];
				$thisFormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];

//				if(!$ReportExtra){
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//				}
//				$thisGrade = $thisMSGrade;
//				$thisMark = $thisMSMark;
				
				# for preview purpose
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				
				#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
									
				if ($thisMark != "N.A.")
				{
					$isAllNA = false;
				}
				
				# check special case
				//if ($CalculationMethod==2 && $isSub)
				//{
				//	$thisMarkDisplay = $this->EmptySymbol;
				//}
				//else
				//{
					# Convert mark to grade
					$thisStyleDetermineMark = $thisMark;
//					debug_pr($SubjectFormGradingSettings);
					if ($thisGrade=="") {
						//if ($isSub == false)
							$thisMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark,$ReportID,$StudentID,$SubjectID,$ClassLevelID,0);
//							debug_pr($SubjectFormGradingSettings["SchemeID"]);
					} else {
						$thisMark = $thisGrade;
					}
				
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisStyleDetermineMark/$thisSubjectWeight : $thisStyleDetermineMark;
						else
							$thisMarkTemp = $thisStyleDetermineMark;
							
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
					}
					else
						$thisMarkDisplay = $thisMark;
				//}

				/*
				if ($isSub)
				{
					# Special handling of component marks display
					$thisColspan = "colspan='2'";
					//$thisAlign = "align='left'";
					$thisAlign = "align='center'";
					$thisFullMark = $SubjectFullMarkAry[$SubjectID];
					
					//$thisMarkDisplay = '<i>'.$thisMarkDisplay.' '.$eReportCard['Template']['Marks'].'/ '.$thisFullMark.'</i>';
					$thisMarkDisplay = '<i>'.$thisMarkDisplay.'</i>';
				}
				else
				{
					$thisColspan = "";
					$thisAlign = "align='center'";
					
					$x[$SubjectID] .= "<td>&nbsp;</td>";
				}
				*/
								
				$thisColspan = "";
				$thisAlign = "align='center'";
				
				# Extra Report Table Content
				if($ReportExtra){
					
					# Display empty symbol
					$thisMSMark = ($thisMSMark === null || $thisMSMark == '/' || $thisMSMark < 0 || $thisMarkDisplay == '/' || $thisMarkDisplay == 'ABS' || $ScaleInput!="M")? $this->EmptySymbol : $thisMSMark;
					$thisFormPosition = ($thisFormPosition === null || $thisFormPosition == '/' || $thisFormPosition < 0 || $thisMarkDisplay == '/')? $this->EmptySymbol : $thisFormPosition;
					$thisNumOfStudent = ($thisNumOfStudent === null  || $thisNumOfStudent == '/' || $thisNumOfStudent < 0)? $this->EmptySymbol : $thisNumOfStudent;
					$fullMarks = $this->GET_SUBJECT_FULL_MARK($SubjectID, $ClassLevelID, $ReportID);
					
//					# Handle overall marks of parent subject
					if(is_numeric($thisMSMark) && is_numeric($fullMarks) && ($fullMarks > 0)){
						
						$thisMSMark = $thisMSMark / ($fullMarks / 100);
						$thisMSMark = number_format($thisMSMark, $SubjectScore);
//						list($thisMSMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMSMark, $thisMSMark);
					}
					
					# Mark
					$x[$SubjectID] .= "<td $thisColspan class=' {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='form_test_cust_tabletext' $thisAlign >";
//							$x[$SubjectID] .= ($isSub)? '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
							$x[$SubjectID] .= $thisMSMark;
//							$x[$SubjectID] .= ($isSub)? '</i>' : '';
						$x[$SubjectID] .= "</td>";
	  					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					# Grade
					$x[$SubjectID] .= "<td $thisColspan class=' {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='form_test_cust_tabletext' $thisAlign >";
//							$x[$SubjectID] .= ($isSub)? '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
							$x[$SubjectID] .= $thisMarkDisplay;
//							$x[$SubjectID] .= ($isSub)? '</i>' : '';
						$x[$SubjectID] .= "</td>";
	  					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					# Form Position
					$thisPosition = $thisFormPosition;
					$thisNumOfStudent = $thisFormNumOfStudent;
					$x[$SubjectID] .= "<td class='{$css_border_top} form_test_cust_tabletext' align='center'>";
//						$x[$SubjectID] .= ($isSub)? '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
							$x[$SubjectID] .= $thisPosition;
//						$x[$SubjectID] .= ($isSub)? '</i>' : '';
		  			$x[$SubjectID] .= "</td>";
		  			
		  			# Form number of student
		  			$thisNumOfStudent = ($isSub)? '&nbsp;' : $thisNumOfStudent;
		  			$x[$SubjectID] .= "<td class='{$css_border_top} form_test_cust_tabletext' align='center'>";
						$x[$SubjectID] .= $thisNumOfStudent;
		  			$x[$SubjectID] .= "</td>";

				} else {
					
					$x[$SubjectID] .= "<td>&nbsp;</td>";
					
					$x[$SubjectID] .= "<td $thisColspan class=' {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' $thisAlign >";
							$x[$SubjectID] .= ($isSub)? '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
							$x[$SubjectID] .= $thisMarkDisplay;
							$x[$SubjectID] .= ($isSub)? '</i>' : '';
						$x[$SubjectID] .= "</td>";
	  					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					
					# Display Position and No. of Pupils for Main Subjects only
					//if ($isSub == false)
					//{
						# [CRM Ref No.: 2010-0705-1039] always show Form Position
						# Form Position
						$thisPosition = $thisFormPosition;
						$thisNumOfStudent = $thisFormNumOfStudent;
						
						# Display empty symbol
						$thisPosition = ($thisPosition === null || $thisPosition == '/' || $thisPosition < 0)? $this->EmptySymbol : $thisPosition;
						$thisNumOfStudent = ($thisNumOfStudent === null  || $thisNumOfStudent == '/' || $thisNumOfStudent < 0)? $this->EmptySymbol : $thisNumOfStudent;
						
						/*
						if ($ShowOverallPositionClass)
						{
							$thisPosition = $thisClassPosition;
							$thisNumOfStudent = $thisClassNumOfStudent;
						}
						else if ($ShowOverallPositionForm)
						{
							$thisPosition = $thisFormPosition;
							$thisNumOfStudent = $thisFormNumOfStudent;
						}
						*/
						//$thisFormPosition = ($isSub)? '&nbsp;' : $thisFormPosition;
						$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>";
							$x[$SubjectID] .= ($isSub)? '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
								$x[$SubjectID] .= $thisPosition;
							$x[$SubjectID] .= ($isSub)? '</i>' : '';
		  				$x[$SubjectID] .= "</td>";
		  				
		  				# Form number of student
		  				$thisNumOfStudent = ($isSub)? '&nbsp;' : $thisNumOfStudent;
		  				$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>";
							$x[$SubjectID] .= $thisNumOfStudent;
		  				$x[$SubjectID] .= "</td>";
					//}
				
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
			
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				$thisStylePrefix = '';
				$thisStyleSuffix = '';
				if ($isSub)
				{
					$thisStylePrefix = '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					$thisStyleSuffix = '</i>';
				}
				
				$NoOfPupilArr = array();
				$isSubjectDropped = false;
				### Overall Results of each terms
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
					
					if ($i == sizeof($ColumnData)-1)
						$isSecondTerm = true;
					else
						$isSecondTerm = false;
					
					//if ($isParentSubject && $CalculationOrder == 1) {
					//	$thisMarkDisplay = $this->EmptySymbol;
					//} else {
						# See if any term reports available
						//$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
						$thisReport = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $ColumnData[$i]['SemesterNum'], $isMainReport=1);
						
						if (empty($thisReport)) {
							$thisReportID = $ReportID;
							$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
							//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
							#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
							//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							
							$thisMSGrade = $MarksAry[$SubjectID][$thisReportColumnID]["Grade"];
							$thisMSMark = $MarksAry[$SubjectID][$thisReportColumnID]["Mark"];
							$thisClassPosition = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritClass'];
							$thisClassNumOfStudent = $MarksAry[$SubjectID][$thisReportColumnID]['ClassNoOfStudent'];
							$thisFormPosition = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritForm'];
							$thisFormNumOfStudent = $MarksAry[$SubjectID][$thisReportColumnID]['FormNoOfStudent'];
							
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
								
						} else {
							$thisReportID = $thisReport['ReportID'];
							$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
							//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
							#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
							//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							
							$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
							$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
							$thisClassPosition = $MarksAry[$SubjectID][0]['OrderMeritClass'];
							$thisClassNumOfStudent = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
							$thisFormPosition = $MarksAry[$SubjectID][0]["OrderMeritForm"];
							$thisFormNumOfStudent = $MarksAry[$SubjectID][0]["FormNoOfStudent"];
							
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						}
						
						
						if ($isSecondTerm && $thisGrade=='*') {
							$isSubjectDropped = true;
						}
						
 						
						### Record number of student for last column display
						# [CRM Ref No.: 2010-0705-1039] always show Form Position
						/*
						if ($ShowOverallPositionClass)
							$NoOfPupilArr[$SubjectID][$i] = $thisClassNumOfStudent;
						else if ($ShowOverallPositionForm)
							$NoOfPupilArr[$SubjectID][$i] = $thisFormNumOfStudent;
						*/
						$NoOfPupilArr[$SubjectID][$i] = $thisFormNumOfStudent;
							
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
													
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
							
							# Convert mark to grade
							$thisStyleDetermineMark = $thisMark;
							if ($thisGrade=="") {
								//if ($isSub == false)
									$thisMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark, $thisReportID, $StudentID, $SubjectID, $ClassLevelID, 0);
							} else {
								$thisMark = $thisGrade;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisStyleDetermineMark);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
						else
						{
							$thisMarkDisplay = '&nbsp;';
						}
						
						# [CRM Ref No.: 2010-0705-1039] always show Form Position
						/*
						if ($ShowOverallPositionClass)
							$thisPosition = ($thisClassPosition=='-1')? '&nbsp;' : $thisClassPosition;
						else if ($ShowOverallPositionForm)
							$thisPosition = ($thisFormPosition=='-1')? '&nbsp;' : $thisFormPosition;
						*/
//						$thisPosition = ($thisFormPosition=='-1')? '&nbsp;' : $thisFormPosition;
						# Display empty symbol
						$thisPosition = ($thisFormPosition === null || $thisFormPosition == '/' || $thisFormPosition < 0)? $this->EmptySymbol : $thisFormPosition;
					//}
					
					/*
					if ($isSub)
					{
						### mark (show full mark if second term)
						$thisFullMark = $SubjectFullMarkAry[$SubjectID];
					
						//$thisMarkDisplay = '<i>'.$thisMarkDisplay.' '.$eReportCard['Template']['Marks'];
						
						//if ($isSecondTerm)
						//	$thisMarkDisplay .= '/ '.$thisFullMark;
							
						//$thisMarkDisplay .= '</i>';
						
						$thisMarkDisplay = '<i>'.$thisMarkDisplay.'</i>';
						
						$x[$SubjectID] .= "<td class='tabletext' align='center' colspan='2'>";
							$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
								$x[$SubjectID] .= "<tr><td class='tabletext' align=\"center\">".$thisMarkDisplay."</td></tr>";
							$x[$SubjectID] .= "</table>";
						$x[$SubjectID] .= "</td>";
					}
					else
					{
						### Grade and Position
						$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisFormPosition ."</td>";
					}
					*/
					
					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisStylePrefix.$thisMarkDisplay.$thisStyleSuffix ."</td>";
	  				$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisStylePrefix.$thisPosition.$thisStyleSuffix ."</td>";
				}
				
				# Subject Overall
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID,"","",1);		
					
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					$thisClassPosition = $MarksAry[$SubjectID][0]["OrderMeritClass"];
					$thisClassNumOfStudent = $MarksAry[$SubjectID][0]["ClassNoOfStudent"];
					$thisFormPosition = $MarksAry[$SubjectID][0]["OrderMeritForm"];
					$thisFormNumOfStudent = $MarksAry[$SubjectID][0]["FormNoOfStudent"];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					//if ($CalculationMethod==2 && $isSub)
					//{
					//	$thisMarkDisplay = $this->EmptySymbol;
					//}
					//else
					//{
						$thisStyleDetermineMark = $thisMark;
						# Convert mark to grade
						if ($thisGrade=="") {
							$thisMark = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID, 0);
						} else {
							$thisMark = $thisGrade;
						}
					
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if ($thisMark == 'N.A.')
						{
							$thisMarkDisplay = '&nbsp;';
						}
						else
						{
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisStyleDetermineMark);
							}
							else
								$thisMarkDisplay = $thisMark;
						}
						
						# [CRM Ref No.: 2010-0705-1039] always show Form Position
						/*
						if ($ShowOverallPositionClass)
						{
							$thisPosition = ($thisClassPosition=='-1')? '&nbsp;' : $thisClassPosition;
							$thisNumOfStudent = $thisClassNumOfStudent;
						}
						else if ($ShowOverallPositionForm)
						{
							$thisPosition = ($thisFormPosition=='-1')? '&nbsp;' : $thisFormPosition;
							$thisNumOfStudent = $thisFormNumOfStudent;
						}
						*/
//						$thisPosition = ($thisFormPosition=='-1')? '&nbsp;' : $thisFormPosition;
//						$thisNumOfStudent = $thisFormNumOfStudent;
						
						# Display empty symbol
						$thisPosition = ($thisFormPosition === null || $thisFormPosition == '/' || $thisFormPosition < 0)? $this->EmptySymbol : $thisFormPosition;
						$thisNumOfStudent = ($thisFormNumOfStudent === null || $thisFormNumOfStudent == '/' || $thisFormNumOfStudent < 0)? $this->EmptySymbol : $thisFormNumOfStudent;
					//}
					
					//if ($isSub)
					//{
						# Grade
					//	$x[$SubjectID] .= "<td>&nbsp;</td>";
						# Position
					//	$x[$SubjectID] .= "<td>&nbsp;</td>";
						# No. of pupils
					//	$x[$SubjectID] .= "<td>&nbsp;</td>";
					//}
					//else
					//{
						$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisStylePrefix.$thisMarkDisplay.$thisStyleSuffix ."</td>";
						$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisStylePrefix.$thisPosition.$thisStyleSuffix ."</td>";
						
						if ($NoOfPupilArr[$SubjectID][0]=='')
							$NoOfPupilArr[$SubjectID][0] = 0;
						if ($NoOfPupilArr[$SubjectID][1]=='')
							$NoOfPupilArr[$SubjectID][1] = 0;
						
						$thisPupilsDisplay = '';
						if (!$isSub)
							$thisPupilsDisplay = $NoOfPupilArr[$SubjectID][0].' / '.$NoOfPupilArr[$SubjectID][1].' / '.$thisNumOfStudent;
						
						$x[$SubjectID] .= "<td class='tabletext' align='center' nowrap>". $thisPupilsDisplay ."</td>";
					//}
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class=' {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				$isFirst = 0;
				
				if ($isSubjectDropped) {
					$isAllNA = true;
				}
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		### Display ECA Info
		
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class=' $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $NumOfStudent='')
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top_dotted" : "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		
		if ($ReportType == 'T')
		{
			$thisValue = ($OverallValue=='')? '&nbsp;' : $OverallValue;
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>&nbsp;</td>";
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			
			$x .= "<td class='tabletext {$border_top} ' align='center' height='{$LineHeight}'>". $NumOfStudent ."</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top_dotted" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>&nbsp;</td>";
		
		/*
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		*/
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Key_To_Grade_Table()
	{
		global $eReportCard;
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0" class="key_to_grade_text">';
			# A & B
			$x .= '<tr>';
				$x .= '<td>'.$eReportCard['Template']['KeyToGrade'].': </td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>'.$eReportCard['Template']['Excellent'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['VeryGood'].'</td>';
			$x .= '</tr>';
			
			# C & D
			$x .= '<tr>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>'.$eReportCard['Template']['Good'].'</td>';
				$x .= '<td>'.$eReportCard['Template']['Fair'].'</td>';
			$x .= '</tr>';
			
			# Empty line
			$x .= '<tr><td colspan="4">&nbsp;</td></tr>';
			
			# E
			$x .= '<tr>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>*</td>';
				$x .= '<td>'.$eReportCard['Template']['Unsatisfactory'].'</td>';
				$x .= '<td>&nbsp;</td>';
			$x .= '</tr>';
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Unit_Wording($Value, $Wording)
	{
		if ($Value > 1)
			$Wording .= 's';
			
		return $Wording;
	}
	
	function Get_Conduct_Wordings($ConductGrade)
	{
		global $eReportCard;
		
		$ConductGrade = strtoupper($ConductGrade);
		switch ($ConductGrade)
		{
			case "A": 	$ConductWording = $eReportCard['Template']['ConductGrade']['Excellent']; 		break;
			case "B": 	$ConductWording = $eReportCard['Template']['ConductGrade']['VeryGood']; 		break;
			case "C+": 	$ConductWording = $eReportCard['Template']['ConductGrade']['Good']; 			break;
			case "C-": 	$ConductWording = $eReportCard['Template']['ConductGrade']['FairlyGood']; 		break;
			case "D": 	$ConductWording = $eReportCard['Template']['ConductGrade']['Fair']; 			break;
			case "E": 	$ConductWording = $eReportCard['Template']['ConductGrade']['NeedsImprovement']; break;
			case "F": 	$ConductWording = $eReportCard['Template']['ConductGrade']['Poor']; 			break;
			default:	$ConductWording = '';
		}
		
		return $ConductWording;
	}
	
	// copied from eDis library
	function TrimDeMeritNumber_00($c, $AllowReturnNull=0)
	{
		if($c)
		{
			$n = str_replace(".00", "", $c);
			$n = str_replace(".50", ".5", $n);
			return $n;
		}
		else
			return $AllowReturnNull ? "" : "--";
	}
	
}
?>