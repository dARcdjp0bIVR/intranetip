<?php
# Editing by Bill

// Please remove hard code content before upload

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("promotion", "attendance", "demerit");

		/* Main Subject List */
		// 146
		//$this->MainSubjectList = array("080", "N165", "280");
		//$this->MainSubjectList2 = array("080", "N165", "280");
		// Client Site
		$this->MainSubjectList = array("S03", "S10", "S12");
		$this->MainSubjectList2 = array("S03", "S10", "S12", "S28");

		/* Competition Subject List */
        /*
        // [2017-1023-1027-51096] Support 2017 Subject Change
        if($this->GET_ACTIVE_YEAR() == "2017" || $this->GET_ACTIVE_YEAR() == "2018") {
            $this->CompletitionSubjectList = array("C03", "C10", "C12", "C21", "C24", "C25", "C30", "S42", "S43", "S51", "S55", "S56", "S57", "S58", "S59");
            $this->CompletitionSubjectDisplayLimit = array("C03"=>90, "C10"=>80, "C12"=>90, "C21"=>90, "C24"=>90, "C25"=>90, "C30"=>90, "S42"=>0, "S43"=>0, "S51"=>0, "S55"=>0, "S56"=>0, "S57"=>0, "S58"=>0, "S59"=>0);
        }
        else {
            $this->CompletitionSubjectList = array("C03", "C10", "C12", "C21", "C24", "C25", "C30", "S43", "S55", "S56");
            $this->CompletitionSubjectDisplayLimit = array("C03"=>90, "C10"=>80, "C12"=>90, "C21"=>90, "C24"=>90, "C25"=>90, "C30"=>90, "S43"=>0, "S55"=>0, "S56"=>0);
        }
        */
		$this->Set_Main_Competition_Subject_List();
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 0;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
		$this->absentSubjectAry = array();
		
		// Related to $eRCTemplateSetting['ReportGeneration']['PromoteInputMapping']
		$this->customizedPromotionStatus = array("Promoted"=>1, "Promoted_with_Retention"=>2, "Graduate"=>3, "Retained"=>4, "Dropout"=>5, "Waiting_Makeup"=>6);
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard;

		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				//$x .= "<tr height='904px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td style='padding-top: 10px'>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
        if($ReportID)
        {
            // Set Up Main / Competition Subject List
            $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
            $this->Set_Main_Competition_Subject_List($ReportSetting['ClassLevelID']);
        }
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
			
			# Retrieve Required Variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			
			# Retrieve Data Info
			if($StudentID){
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$data['Name'] = $lu->UserName2Lang("b5", 2);
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$data['Class'] = $StudentInfoArr[0]['ClassName'];
				$data['ClassNo'] = $StudentInfoArr[0]['ClassNumber'];
			}
			
			$data['SchoolYear'] = $this->GET_ACTIVE_YEAR_NAME();
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			
			if($SchoolType=="S"){
				$SchoolName = "<span>".$eReportCard['Template']['SchoolNameCh']."</span>&nbsp;<span style='font-size:11pt'>".$eReportCard['Template']['SchoolNameEn']."</span>";
			}
			else{
				$SchoolName = "<span>".$eReportCard['Template']['PSchoolNameCh']."</span>&nbsp;<span style='font-size:11pt'>".$eReportCard['Template']['PSchoolNameEn']."</span>";
			}
				
			# Build Table
			$StudentInfoTable .= "<table class='report_header' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td rowspan='2' width='14%'>&nbsp;</td>";
					$StudentInfoTable .= "<td class='school_name' width='68%'>".$SchoolName."</td>";
					$StudentInfoTable .= "<td class='report_type' width='16%' align='right'>".$eReportCard['Template']['ReportTypeCh']."</td>";
					$StudentInfoTable .= "<td width='2%'>&nbsp;</td>";
				$StudentInfoTable .= "</tr>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td>http://www.puiching.edu.mo</td>";
					$StudentInfoTable .= "<td align='right'>".$eReportCard['Template']['ReportTypeEn']."</td>";
					$StudentInfoTable .= "<td>&nbsp;</td>";
				$StudentInfoTable .= "</tr>";
			$StudentInfoTable .= "</table>";
			
			$StudentInfoTable .= "<table class='report_header2' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			
			// loop Student Info
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				
				$Title = $eReportCard['Template']['StudentInfo'][$SettingID."Ch"]."&nbsp;".$eReportCard['Template']['StudentInfo'][$SettingID."En"];
				//$Title .= ($i==2 || $i==3)? "&nbsp;" : ": &nbsp;";
				$Title .= ": &nbsp;";
				
				$studentdata = $data[$SettingID] ? $data[$SettingID] : $defaultVal;
				if($SettingID=="DateOfIssue" && $studentdata!=""){
					$studentdata = date("Y/m/d", strtotime($studentdata));
				}
				
				if($i%$StudentInfoTableCol == 0) {
					$StudentInfoTable .= "<tr>";
				}				
					$StudentInfoTable .= "<td class='tabletext'>".$Title.$studentdata."</td>";
				if(($i+1)%$StudentInfoTableCol == 0) {
					$StudentInfoTable .= "</tr>";
				}
			}
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='') {
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
		$term_seq 					= $this->Get_Semester_Seq_Number($SemID);
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SchoolType					= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectComponent 		= $ReportSetting['ShowSubjectComponent'];
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
//		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# Retrieve Table Header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
		
		# Retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# Retrieve Subject Columns
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		
		# Retrieve Marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		
		# Retrieve Marks HTML
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# Retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################		
		$DetailsTable = "<table width='100%' border='1' cellspacing='0' cellpadding='2' class='report_content' style=''>";
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		$displaySubjCount = 0;
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		// loop all Subjects & add empty row
		//for($i=0; ($i<$sizeofSubjectCol || $displaySubjCount<$eRCTemplateSetting['MSTableRow']); $i++)
		for($i=0; $i<$sizeofSubjectCol; $i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			// Empty Row
			if(!$thisSubjectID)
			{
				$DetailsTable .= "<tr>";
					$DetailsTable .= "<td colspan='2' class='border_top border_left' height='".$LineHeight."' style='padding:2px'>".$this->EmptySymbol."</td>";
					for($j=1; $j<$ColNum; $j++){
						$DetailsTable .= "<td class='border_top border_left' height='".$LineHeight."'>".$this->EmptySymbol."</td>";
					}
				$DetailsTable .= "</tr>";
					
				$displaySubjCount++;
				continue;
			}
			
			# If all the marks is "*", then don't display
			if (sizeof((array)$MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach((array)$MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			
			// Component Subject
			if(in_array($thisSubjectID, (array)$MainSubjectIDArray)!=true){
				$isSub = 1;
				$parentSubjectID = $this->GET_PARENT_SUBJECT_ID($thisSubjectID);
			}
			
			// Component Subject Display Checking
			// Hide Component - Template Settings
			if ($ShowSubjectComponent==0 && $isSub)
				continue;
			// Hide Component - Hide Component Subject
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				$DetailsTable .= "</tr>";
				
				$isFirst = 0;
				$displaySubjCount++;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID) {
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$SemesterList 				= getSemesters($this->schoolYearID, 0);
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SchoolType					= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
//		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
//		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
//		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$n = 0;
		$e = 0;	# column within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
						
		#########################################################
		# Marks START
		#########################################################
		
		$row1 = "";
		$row2 = "";
		
		// Subjects Column
		$row1 .= "<td colspan='2' rowspan='2' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Subject']."'>";
				$row1 .= $eReportCard['Template']['SubjectCh']."<br>".$eReportCard['Template']['SubjectEn'];
		$row1 .="</td>";
		$n++;
		
		// loop Semester
		$totalWeight = 0;
		for($i=1; $i<4; $i++)
		{
			// Get Semester Report Info
			$thisSemester = $SemesterList[($i-1)]["YearTermID"];
			$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
			$thisReportID = $thisReport["ReportID"];
			$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
			
			// No. of Half Column
			$row1 .= "<td colspan='2' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Mark']."' class='border_left'>";
					$row1 .= $eReportCard['Template']["Half".$i."Ch"]."<br>".$eReportCard['Template']["Half".$i."En"];
			$row1 .="</td>";
			
			// General Column
			$currentWeight = $thisColumnData[0]["DefaultWeight"];
			$currentWeight = $currentWeight? ($currentWeight * 100) : $this->EmptySymbol;
			$totalWeight += ($currentWeight!=$this->EmptySymbol)? $currentWeight : 0;
			$row2 .= "<td height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Mark2']."' class='border_top border_left'>";
					$row2 .= $eReportCard['Template']['GeneralCh']."<br>".$eReportCard['Template']['GeneralEn']."<br>(".$currentWeight."%)";
			$row2 .="</td>";
			$n++;
			$e++;
			
			// Exam Column
			$currentWeight = $thisColumnData[1]["DefaultWeight"];
			$currentWeight = $currentWeight? ($currentWeight * 100) : $this->EmptySymbol;
			$totalWeight += ($currentWeight!=$this->EmptySymbol)? $currentWeight : 0;
			$row2 .= "<td height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Mark2']."' class='border_top border_left'>";
					$row2 .= $eReportCard['Template']['ExamCh']."<br>".$eReportCard['Template']['ExamEn']."<br>(".$currentWeight."%)";
			$row2 .="</td>";
			$n++;
			$e++;
		}
		
		// Overall Column
		$overall_width = $eRCTemplateSetting['ColumnWidth']['Overall'];
		$overall_width = $SchoolType!="S"? ($overall_width * 2)."%" : $overall_width;
		$row1 .= "<td rowspan='2' height='{$LineHeight}' width='".$overall_width."' class='border_left'>";
				$row1 .= $eReportCard['Template']['TotalCh']."<br>".$eReportCard['Template']['TotalEn']."<br>(".$totalWeight."%)";
		$row1 .="</td>";
		$n++;
		
		// Graduate Exam Column
		//$SchoolType = "F";
		if($SchoolType=="S")
		{
			$row1 .= "<td rowspan='2' height='{$LineHeight}' width='".$eRCTemplateSetting['ColumnWidth']['Overall']."' class='border_left'>";
					$row1 .= $eReportCard['Template']['GradExamCh']."<br>".$eReportCard['Template']['GradExamEn']."<br>(".$totalWeight."%)";
			$row1 .="</td>";
			$n++;
		}
		
		$x = "<tr class='table_header'>". $row1 ."</tr>";
		if($row2)	$x .= "<tr class='table_header'>". $row2 ."</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];

        // Set Up Main / Competition Subject List
        $this->Set_Main_Competition_Subject_List($ClassLevelID);

		// Completition Subject List
		$CompletitionSubjectArr = $this->CompletitionSubjectList;

		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
		
		// Subject Code Mapping
		$SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP(0, 0);
 		
 		// loop Subject
 		$x = array();
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID => $Ary)
	 		{
		 		foreach($Ary as $SubSubjectID => $Subjs)
		 		{	
		 			$postfx = "";	 		
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
					
						// Get Subject Code
						$thisSubjectCode = $SubjectCodeMapping[$SubSubjectID];
						
						// Completition Subject Result is excluded
						if(in_array($thisSubjectCode, $CompletitionSubjectArr)){
							$postfx = "#";
						}
			 		}
			 		
			 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
	 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 			
			 		$t = "";		
		 			$t .= "<td colspan='2' class='tabletext border_top' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>&nbsp;</td><td height='{$LineHeight}' class='tabletext' style='text-align:left'>".$SubjectChn."&nbsp;".$SubjectEng.$postfx."</td></tr>";
						$t .= "</table>";
					$t .= "</td>";
					
					$x[] = $t;
				}
		 	}
		}
		
 		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$SemesterList 				= getSemesters($this->schoolYearID, 0);
		$SemesterSeq	 			= $this->Get_Semester_Seq_Number($SemID);
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SchoolType					= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
//		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
//		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
//		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
//		$ShowSubjectOverall 		= $ShowRightestColumn;
//		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
//		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
//		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
//		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
//		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		//$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		$CalculationOrder = $CalSetting["OrderFullYear"];
		
		# Retrieve Display Settings
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		# Retrieve Subject Array
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);

        // Set Up Main / Competition Subject List
        $this->Set_Main_Competition_Subject_List($ClassLevelID);
			
		// Get Competition Subject List
		$CompletitionSubjectArr = $this->CompletitionSubjectList;
		
		// Get Subject Code & ID Mapping
		$MappingAry = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		$n = 0;
		$isFirst = 1;
		$x = array();
		
		// loop Subject
		foreach($SubjectArray as $SubjectID => $SubjectName)
		{
			// Get Subject Code
			$SubjectCode = $MappingAry[$SubjectID];
			
			// Retrieve Subject Scheme ID & settings
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			//$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			//$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
						
			$t = "";
			$isSub = 0;
			$isAllNA = true;
			if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			// check if it is a parent subject, if yes find info of its components subjects
			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
			if (!$isSub) {
//				$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
			}
			
			// Check if it is completition subject
			$isCompletitionSubject = in_array($SubjectCode, (array)$CompletitionSubjectArr);
			
			# Semester Report Column
			// loop Semester
			for($i=0; $i<3; $i++)
			{
				$css_border_top = $isSub? "" : "border_top";
				
				// Get Semester Report Info
				$thisSemester = $SemesterList[$i]["YearTermID"];
				$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
				$thisReportID = $thisReport["ReportID"];
				$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
				$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
				
				# Retrieve Subject Scheme ID & settings
				$thisSubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $thisReportID);
				$thisSchemeID = $thisSubjectFormGradingSettings['SchemeID'];
				//$thisSchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($thisSchemeID);
				$thisScaleDisplay = $thisSubjectFormGradingSettings['ScaleDisplay'];
				$thisScaleInput = $thisSubjectFormGradingSettings['ScaleInput'];
				$thisCalculationOrder = $CalSetting["OrderTerm"];
					
				// Check if all Zero Weight
				$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
				$isAllCmpZeroWeight = !in_array(false, $isAllCmpZeroWeightArr);
					
				// loop Report Column
				$ColumnID = array();
				$ColumnTitle = array();
				foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
				
				// loop Report Column
				for($j=0; $j<2; $j++)
				{
					// Get Report Column Info
					$thisColumnID = $ColumnID[$j];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
		
					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $thisCalculationOrder == 1 && !$isAllCmpZeroWeight) 
					{
						$thisMarkDisplay = $this->EmptySymbol;
					} 
					else 
					{
						// Get Subject Weight
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$thisColumnID."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $thisMarksAry[$SubjectID][$thisColumnID]['Grade'];
						$thisMSMark = $thisMarksAry[$SubjectID][$thisColumnID]['Mark'];
						
						$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						// Competitive Subject Display
						if($isCompletitionSubject)
						{
							$thisSubjectDisplayLimit = $this->CompletitionSubjectDisplayLimit;
							$thisSubjectDisplayLimit = $thisSubjectDisplayLimit[$SubjectCode];
							$thisMark = $thisSubjectDisplayLimit && ($thisMark == "N.A." || ($thisMSMark && $thisMSMark < $thisSubjectDisplayLimit)) ? $this->EmptySymbol : $thisMark;
						}
						//$thisMark = $isCompletitionSubject && $thisMSMark && $thisMSMark < 80 ? $this->EmptySymbol : $thisMark;
						
						if ($thisMark != "N.A." && $thisMark != "" && ($ReportType=="T" && $SemesterSeq>=($i+1) || $ReportType=="W"))
						{
							if($isCompletitionSubject)
							{
								if($thisMark != $this->EmptySymbol) {
									$isAllNA = false;
								}
							}
							else
							{
								$isAllNA = false;
							}
						}
						
						if (($ReportType=="T" && $SemesterSeq<($i+1)) || $thisMark == $this->EmptySymbol)
						{
							$thisMarkDisplay = $this->EmptySymbol;
						}
						else
						{
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$thisColumnID]['Grade']);
							if($needStyle)
							{
								if ($thisSubjectWeight>0 && $thisScaleDisplay=="M")
								{
									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
								}
								else
								{
									$thisMarkTemp = $thisMark;
								}
								
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  						$x[$SubjectID] .= "<td class='tabletext' align='center'>".$thisMarkDisplay."</td>";					
						$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
			}
						
			# Subject Overall Column
			$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
			$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
			
			$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
			$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
			
			# for preview purpose
			if(!$StudentID)
			{
				$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
				$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
			}
			//debug_pr($thisMarksAry[$SubjectID][0]);
			$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
			
			// Competitive Subject Display
			if($isCompletitionSubject)
			{
				$thisSubjectDisplayLimit = $this->CompletitionSubjectDisplayLimit;
				$thisSubjectDisplayLimit = $thisSubjectDisplayLimit[$SubjectCode];
				//$thisMark = $thisMSMark && $thisSubjectDisplayLimit && $thisMSMark < $thisSubjectDisplayLimit ? $this->EmptySymbol : $thisMark;
				$thisMark = $thisSubjectDisplayLimit > 0? $this->EmptySymbol : $thisMark;
			}
			//$thisMark = $isCompletitionSubject && $thisMSMark && $thisMSMark < 80 ? $this->EmptySymbol : $thisMark;
						
			if ($thisMark != "N.A." && $thisMark != "" && $ReportType=="W") 
			{
				if($isCompletitionSubject)
				{
					if($thisMark != $this->EmptySymbol) {
						$isAllNA = false;
					}
				}
				else
				{
					$isAllNA = false;
				}
			}
				
			if (($CalculationMethod==2 && $isSub) || $ReportType!="W" || $thisMark == $this->EmptySymbol)
			{
				$thisMarkDisplay = $this->EmptySymbol;
			}
			else
			{
				# check special case
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
				if($needStyle)
				{
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
				}
				else
					$thisMarkDisplay = $thisMark;
					
				// Display symbol for student attend Mark-up Exam
				if($StudentID){
					if($thisGrade == "D") {
						$thisMarkDisplay .= "△";
					}
					else {
						$isSubjectFail = is_numeric($thisMark) && $thisMark < 60;
						if($isSubjectFail)
						{
							if($SchoolType=="S" && $FormNumber==6) {
								// Get Graduate Exam Info
								$thisSemester = $SemesterList[2]["YearTermID"];
								$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
								$thisReportID = $thisReport["ReportID"];
								$targetReportColumnID = $this->returnReportTemplateColumnData($thisReportID, $other=" DisplayOrder = 1");
								$targetReportColumnID = !empty($targetReportColumnID)? $targetReportColumnID[0]["ReportColumnID"] : 0;
								
								// Get Graduate Exam Result
								$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
								$MarkupSubjectMark = $thisMarksAry[$SubjectID][$targetReportColumnID]['Mark'];
								
								// Get Mark-up Exam Result
								$MarkupSubjectMark2 = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
								$MarkupSubjectMark2 = $MarkupSubjectMark2[$StudentID]['Info'];
								
								$thisMarkDisplay .= ((isset($MarkupSubjectMark) && $MarkupSubjectMark!="" && $MarkupSubjectMark >= 60) || (isset($MarkupSubjectMark2) && $MarkupSubjectMark2!="" && $MarkupSubjectMark2 >= 60)) ? "△" : "";
							}
							else {
								// Get Mark-up Exam Result
								$MarkupSubjectMark = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
								$MarkupSubjectMark = $MarkupSubjectMark[$StudentID]['Info'];
								
								$thisMarkDisplay .= (isset($MarkupSubjectMark) && $MarkupSubjectMark!="" && $MarkupSubjectMark >= 60) ? "△" : "";
							}
						}
					}
				}
			}
			
			$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
					$x[$SubjectID] .= "<td class='tabletext' align='center'>".$thisMarkDisplay."</td>";			
				$x[$SubjectID] .= "</tr></table>";
			$x[$SubjectID] .= "</td>";
			
			# Graduation Mark Column
			//$SchoolType = "F";
			if($SchoolType=="S")
			{
				$thisMarkDisplay = $this->EmptySymbol;
				
				// Form 6 only
				//$FormNumber = 6;
				//if($FormNumber==6 && ($ReportType=="T" && $SemesterSeq==3 || $ReportType=="W"))
				if($FormNumber==6 && $ReportType=="W")
				{
					$css_border_top = $isSub? "" : "border_top";
					
					// Get Semester Report Info
					$thisSemester = $SemesterList[2]["YearTermID"];
					$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
					$thisReportID = $thisReport["ReportID"];
					$targetReportColumnID = $this->returnReportTemplateColumnData($thisReportID, $other=" DisplayOrder = 1");
					$targetReportColumnID = !empty($targetReportColumnID)? $targetReportColumnID[0]["ReportColumnID"] : 0;
					$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
					
					# Retrieve Subject Scheme ID & settings
					$thisSubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $thisReportID);
					$thisSchemeID = $thisSubjectFormGradingSettings['SchemeID'];
					//$thisSchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($thisSchemeID);
					$thisScaleDisplay = $thisSubjectFormGradingSettings['ScaleDisplay'];
					$thisScaleInput = $thisSubjectFormGradingSettings['ScaleInput'];
					$thisCalculationOrder = $CalSetting["OrderTerm"];
					
					// Check if all Zero Weight
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false, $isAllCmpZeroWeightArr);
				
					// Get Report Column Info
					$columnWeightConds = " ReportColumnID = '$targetReportColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[$targetReportColumnID]['Weight'];
		
					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $thisCalculationOrder == 1 && !$isAllCmpZeroWeight) 
					{
						$thisMarkDisplay = $this->EmptySymbol;
					} 
					else 
					{
						// Get Subject Weight
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID = '$targetReportColumnID' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[$targetReportColumnID]['Weight'];
						
						$thisMSGrade = $thisMarksAry[$SubjectID][$targetReportColumnID]['Grade'];
						$thisMSMark = $thisMarksAry[$SubjectID][$targetReportColumnID]['Mark'];
						
						$thisGrade = ($thisScaleDisplay=="G" || $thisScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($thisScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $thisScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $thisScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($thisScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						// Competitive Subject Display
						if($isCompletitionSubject)
						{
							$thisSubjectDisplayLimit = $this->CompletitionSubjectDisplayLimit;
							$thisSubjectDisplayLimit = $thisSubjectDisplayLimit[$SubjectCode];
							//$thisMark = $thisMSMark && $thisSubjectDisplayLimit && $thisMSMark < $thisSubjectDisplayLimit ? $this->EmptySymbol : $thisMark;
							$thisMark = $thisSubjectDisplayLimit > 0? $this->EmptySymbol : $thisMark;
						}
						//$thisMark = $isCompletitionSubject && $thisMSMark && $thisMSMark < 80 ? $this->EmptySymbol : $thisMark;
						
						if ($thisMark != "N.A." && $thisMark != "")
						{
							if($isCompletitionSubject)
							{
								if($thisMark != $this->EmptySymbol) {
									$isAllNA = false;
								}
							}
							else
							{
								$isAllNA = false;
							}
						}
						
						if ($thisMark == $this->EmptySymbol)
						{
							$thisMarkDisplay = $this->EmptySymbol;
						}
						else
						{
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$targetReportColumnID]['Grade']);
							if($needStyle)
							{
								if ($thisSubjectWeight>0 && $thisScaleDisplay=="M")
								{
									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
								}
								else
								{
									$thisMarkTemp = $thisMark;
								}
								
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
					}
					
					// Display symbol for student attend Mark-up Exam
					if($StudentID){
						if($thisGrade == "D") {
							$thisMarkDisplay .= "△";
						}
						else {
							// Get Mark-up Exam Result
							$MarkupSubjectMark = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
							$MarkupSubjectMark = $MarkupSubjectMark[$StudentID]['Info'];
							$thisMarkDisplay .= (isset($MarkupSubjectMark) && $MarkupSubjectMark!="" && $MarkupSubjectMark >= 60) ? "△" : "";
						}
					}
				}
				
				$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center'>".$thisMarkDisplay."</td>";
					$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
			}
			
			$isFirst = 0;
			
			# construct an array to return
			$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
			$returnArr['isAllNA'][$SubjectID] = $isAllNA;
		}
		
		return $returnArr;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SchoolType					= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
		$SemesterList 				= getSemesters($this->schoolYearID, 0);
		$term_num 					= $this->Get_Semester_Seq_Number($SemID);
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight					= $ReportSetting['LineHeight'];
//		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_award.php');
		$lreportcard_award = new libreportcard_award();
		
		// Subject Code & ID Mapping
		$MappingAry = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		// Conduct List
		$ConductArr = $lreportcard_extrainfo->Get_Conduct();
		$ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);
		
		// Get award info
		$awardInfoAry = $lreportcard_award->Get_Report_Award_Info($ReportID);
		$awardIdAry = array_keys((array)$awardInfoAry);
					
		// Get students with award
		$awardStudentAry = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, array($StudentID), $ReturnAsso=false, $AwardNameWithSubject=1, $awardIdAry);
		$awardStudentAssoAry = BuildMultiKeyAssoc((array)$awardStudentAry, array('StudentID', 'AwardID'));
		
		// loop Semester
		$SemReportAry = array();
		$SemMarksAry = array();
		$SemMainSubject = array();
		$OtherInfoDataAry = array();
		$ConductDataAry = array();
		for($j=0; $j<3; $j++)
		{
			// Get Semester Report Info
			$thisSemester = $SemesterList[$j]["YearTermID"];
			$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
			$thisReportID = $thisReport["ReportID"];
			$SemReportAry[$j] = $thisSemester;
			
			// Get Other Info Data & Class Teacher Comment
			if ($StudentID != '' && ($ReportType=="T" && $term_num>=($j+1) || $ReportType=="W")) {
				$SemMarksAry[$j] = $this->getMarks($thisReportID, $StudentID, "", "", 1);
				
				$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $thisReportID);
				if(sizeof($MainSubjectArray) > 0)
					foreach($MainSubjectArray as $SemMainSubject[$j][] => $MainSubjectNameArray[]);
				
				$thisOtherInfoData = $this->getReportOtherInfoData($thisReportID, $StudentID);
				$SubjectTeacherCommentAry[$j] = $this->returnSubjectTeacherComment($thisReportID, $StudentID); 
				
				// Get data from eDiscipline
				$edisData = $this->Get_Student_Profile_Data($thisReportID, $StudentID, true, true, true, $thisSemester, false);
				
				// Merge discipline data
				$OtherInfoDataAry[$j] = $this->MergeDisciplineData($thisOtherInfoData, $edisData, $StudentID, $thisSemester);
				
				// Get Conduct
				$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($thisReportID, array($StudentID));
				$StudentConductID = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
				$StudentConductID = $StudentConductID[$StudentID];
				$ConductDataAry[$j] = $ConductArr[$StudentConductID];
			}
			
		}
		
		// Table Footer Fields
		$FooterFieldCount = count($eRCTemplateSetting['MSTableFooter']['Fields']);
		for($i=0; $i<$FooterFieldCount; $i++)
		{
			$Key = $eRCTemplateSetting['MSTableFooter']['Fields'][$i];
			$LangKey = str_replace(" ", "", $Key);
			
			$x .= "<tr>";
	 			$x .= "<td colspan='2' class='tabletext border_top' height='{$LineHeight}' valign='middle'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr><td>&nbsp;</td><td height='{$LineHeight}'class='tabletext'>".$eReportCard['Template'][$LangKey.'Ch']." ".$eReportCard['Template'][$LangKey.'En']."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
			
			// loop Semester
			for($j=0; $j<3; $j++)
			{
				// Conduct
				if($i==0)
				{
					$ConductDisplay = $ConductDataAry[$j]? $ConductDataAry[$j] : $this->EmptySymbol;
		 			$x .= "<td colspan='2' class='tabletext border_top border_left' height='{$LineHeight}'>".$ConductDisplay."</td>";
				}
				// Others
				else
				{
					// retrieve result data
					$ary = $OtherInfoDataAry[$j][$StudentID][$SemReportAry[$j]];
					$OtherInfoData = $ary[$Key]? $ary[$Key] : $this->EmptySymbol;
					
		 			$x .= "<td colspan='2' class='tabletext border_top border_left' height='{$LineHeight}'>".$OtherInfoData."</td>";
				}
			}
			
			// Overall
			// Conduct
			if($i==0)
			{
				$OverallConduct = $this->calculateOverallConduct($ConductDataAry, $ConductArr);
				$OverallConduct = $OverallConduct? $OverallConduct : $this->EmptySymbol;
	 			
	 			// Total Column
	 			$x .= "<td class='tabletext border_top border_left' height='{$LineHeight}'>".$OverallConduct."</td>";
				
				// Graduation Exam Column
	 			// F.6
//				if($SchoolType=="S" && $FormNumber==6)
//				{
//					$ConductDisplay = $ConductDataAry[2]? $ConductDataAry[2] : $this->EmptySymbol;
//	 				$x .= "<td class='tabletext border_top border_left' height='{$LineHeight}'>".$ConductDisplay."</td>";
//				}
//				else
				if($SchoolType=="S")
				{
	 				$x .= "<td class='tabletext border_top border_left' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
			}
			// Award Column
			else if($i==1)
			{
				$rowNum = $FooterFieldCount - 1 + 6;
				
				// Get awards for final report
				$awardDisplay = "&nbsp;";
				if($SemID=="F" && $StudentID){
					$StudentAwards = $awardStudentAssoAry[$StudentID];
					$awardDisplay = "";
					foreach((array)$StudentAwards as $AwardID => $CurrentAward)
					{
						$awardDisplay .= $awardDisplay==""? "" : "<br/>";
						$awardDisplay .= $CurrentAward["AwardName"];
					} 
				}
				
				$x .= "<td rowspan='".$rowNum."' colspan='2' height='{$LineHeight}' valign='top' class='tabletext border_top border_left'>";
					$x .= "<table width='100%'>";
						$x .= "<tr><td width='100%'>".$eReportCard['Template']['AwardCol']."</td></tr>";
						$x .= "<tr><td width='100%' style='text-align:left'>".$awardDisplay."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
			}
			
			$x .= "</tr>";
		}
		
		// loop Semester
		for($i=1; $i<4; $i++)
		{
			// get Award
			list($goodAcademic, $excellentAcademic, $excellentConduct) = $this->returnReceivedCommentAward($SchoolType, $FormNumber, $SemMarksAry[$i-1], $SemMainSubject[$i-1], $ConductDataAry[$i-1], $MappingAry);
			
			// 1st row Award
			$award = "";
			if($excellentAcademic && $excellentConduct)
				$award = $eReportCard['Template']['ExcellentAcademicConduct'];
			else if($excellentAcademic)
				$award = $eReportCard['Template']['ExcellentAcademic2'];
			else if($excellentConduct)
				$award = $eReportCard['Template']['ExcellentConduct'];
			
			// 2nd row Award
			if(!$excellentAcademic && $goodAcademic){
				$award .= $award==""? "" : "<br>";
				$award .= $eReportCard['Template']['GoodAcademic'];
			}
			
			// Class Teacher Comment
			$comment = $SubjectTeacherCommentAry[($i-1)][0];
			$comment = strlen($comment)>0? $comment : "&nbsp;";
			
			// Comment Box display
			$commentDisplay = $award==""? "" : $award."<br>";
			$commentDisplay .= $comment;
			
			$x .= "<tr class='comment_row'>";
	 			$x .= "<td class='tabletext border_top border_left border_bottom' height='{$LineHeight}' width='17%' valign='middle'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$x .= "<tr><td></td><td height='{$LineHeight}'class='tabletext'>".$eReportCard['Template']['Half'.$i.'Comment']."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
		 		$x .= "<td colspan='7' class='tabletext border_top border_left border_bottom' height='{$LineHeight}' valign='middle'>".$commentDisplay."</td>";
			$x .= "</tr>";
		}
		
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType=''){
		return "";
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='') {
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SchoolType					= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isPrimaryLevel 			= $SchoolType == "P";
		$isSecondaryLevel 			= $SchoolType == "S";
		$isPrimaryGraduate 			= $isPrimaryLevel && $FormNumber == 6;
		$isSecondaryJuniorGrad 		= $isSecondaryLevel && $FormNumber == 3;
		$isSecondaryGraduate 		= $isSecondaryLevel && $FormNumber == 6;
		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		
		// get Clas Teacher Name
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$thisClassName = $StudentInfoArr[0]['ClassName'];
		$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
		if(is_array($ClassTeacherAry) && count($ClassTeacherAry)>0)
		{
			$ClassTeacherAry = Get_Array_By_Key($ClassTeacherAry, "ChineseName");
			$ClassTeacher = implode(",&nbsp;", (array)$ClassTeacherAry);
		}
		else if($StudentID != "")
		{
			$ClassTeacher = $this->emptySymbol;
		}
		else
		{
			$ClassTeacher = "xxx";
		}
		
		// Get Promotion Status
		// Default Status
		$PromotionStatus = "&nbsp;";
		if($ReportID != "" && $StudentID != "" && $ReportType=="W")
		{
			$promotionStatusAry = $this->customizedPromotionStatus;
			$PromotionStatus = $this->GetPromotionStatusList($ClassLevelID, "", $StudentID, $ReportID);
			$PromotionStatus = $PromotionStatus[$StudentID]["Promotion"];
			switch ($PromotionStatus)
			{
				case $promotionStatusAry['Promoted']:
				case $promotionStatusAry['Promoted_with_Retention']:
					$PromotionStatus = $eReportCard['Template']['Promotion'];
					break;
				case $promotionStatusAry['Graduate']:
					if($isPrimaryGraduate)
						$PromotionStatus = $eReportCard['Template']['GraduationPriSch'];
					else if($isSecondaryJuniorGrad)
						$PromotionStatus = $eReportCard['Template']['GraduationSecondSch'];
					else if($isSecondaryGraduate)
						$PromotionStatus = $eReportCard['Template']['GraduationHighSch'];
					 break;
				case $promotionStatusAry['Retained']:
				case $promotionStatusAry['Dropout']:
					 $PromotionStatus = $eReportCard['Template']['Retention'];
					 break;
				default:
					 $PromotionStatus = $eReportCard['Template']['SchImcomplete'];
					 break;
			}
		}
		
		$SignatureTable .= "<table class='signature_table' width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$SignatureTable .= "<tr>";
		
		$SignatureTable .= "<td class='SchoolCrop' width='".$eRCTemplateSetting['ColumnWidth'][$SignatureTitleArray[0]]."' style='vertical-align:middle;'>";
			$SignatureTable .= "<div><br>".$eReportCard['Template']['SchoolCropCh']."<br>".$eReportCard['Template']['SchoolCropEn']."</div>";
		$SignatureTable .= "</td>";
		
		$SettingID = trim($SignatureTitleArray[1]);
		$TitleEn = $eReportCard["Template"][$SettingID."En"];
		$TitleCh = $eReportCard["Template"][$SettingID."Ch"];
		
		$SignatureTable .= "<td class='border_top border_left border_right border_bottom' width='".$eRCTemplateSetting['ColumnWidth'][$SettingID]."' align='center'>";
			$SignatureTable .= "<table class='signature_cell' cellspacing='0' cellpadding='0' border='0'>";
				$SignatureTable .= "<tr><td>$TitleCh $TitleEn</td></tr>";
				$SignatureTable .= "<tr><td>&nbsp;</td></tr>";
				$SignatureTable .= "<tr><td><img width='50%' src='/file/reportcard2008/templates/escola_pui_ching_signature_v2.bmp'></td></tr>";
			$SignatureTable .= "</table>";
		$SignatureTable .= "</td>";
		
		$SignatureTable .= "<td class='border_top border_left border_right border_bottom' width='".$eRCTemplateSetting['ColumnWidth'][$SignatureTitleArray[2]]."' align='center'>";
			$SignatureTable .= "<table class='signature_cell' cellspacing='0' cellpadding='0' border='0'>";
				$SignatureTable .= "<tr><td>".$eReportCard['Template']['VicePrincipal_AdminHead'].": ".$eReportCard['Template']['VicePrincipal_AdminHead_Name']."</td></tr>";
				$SignatureTable .= "<tr><td>".$eReportCard['Template']['VicePrincipal'].": ".$eReportCard['Template']['VicePrincipalName']."</td></tr>";
				$SignatureTable .= "<tr><td>".$eReportCard['Template']['MoralHead'].": ".$eReportCard['Template']['MoralHeadName']."</td></tr>";
				$SignatureTable .= "<tr><td>".$eReportCard['Template']['TeachingHead'].": ".$eReportCard['Template']['TeachingHeadName']."</td></tr>";
				$SignatureTable .= "<tr><td>".$eReportCard['Template']['ClassTeacher'].": ".$ClassTeacher."</td></tr>";
			$SignatureTable .= "</table>";
		$SignatureTable .= "</td>";
		
		$SettingID = trim($SignatureTitleArray[3]);
		$TitleEn = $eReportCard["Template"][$SettingID."En"];
		$TitleCh = $eReportCard["Template"][$SettingID."Ch"];
		
		$SignatureTable .= "<td class='border_top border_left border_right border_bottom' width='".$eRCTemplateSetting['ColumnWidth'][$SettingID]."' align='center'>";
			$SignatureTable .= "<table class='signature_cell' cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td>$TitleCh $TitleEn</td></tr>";
			$SignatureTable .= "<tr><td><br>".$PromotionStatus."</td></tr>";
			$SignatureTable .= "</table>";
		$SignatureTable .= "</td>";

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		$SignatureTable .= "<table class='table_footer' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$SignatureTable .= "<tr><td>";
				$SignatureTable .= $eReportCard['Template']['Remarks2Ch'].$eReportCard['Template']['Remarks2En'];
			$SignatureTable .= "</td></tr>";
			$SignatureTable .= "<tr><td>";
				$SignatureTable .= $eReportCard['Template']['Remarks3Ch']."&nbsp;".$eReportCard['Template']['Remarks3En'];
			$SignatureTable .= "</td></tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	########### END Template Related ##############
	
	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $OtherArr=array(), $otherOverall="")
	{		
		return "";
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
				
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$SemID 						= $ReportSetting['Semester'];
		$term_num 					= $SemID == "F" ? "W" : $this->Get_Semester_Seq_Number($SemID);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		return false;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		
		return $td;
	}		
	
	function Get_Ranking_Display($Rank, $NumOfStudent, $DisplayRank, $DisplayNumOfStudent)
	{
		$Display = '';
		$DisplayRank = ($Rank != "-1" && $Rank != $this->EmptySymbol && $DisplayRank == true);
		
		if ($DisplayRank==true && $DisplayNumOfStudent==true)
		{
			$Display = $Rank.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==true && $DisplayNumOfStudent==false)
		{
			$Display = $Rank;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==true)
		{
			$Display = $this->EmptySymbol.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==false)
		{
			$Display = $this->EmptySymbol;
		}
		
		return $Display;
	}
	
	// Customized Logic for Award Generation
	public function Generate_Report_Award_Customized($ReportID)
	{
		global $PATH_WRT_ROOT, $eRCTemplateSetting, $lreportcard_award;
		
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
		
		// Get Report Info
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$Semester = $ReportInfoArr['Semester'];
		$isMainReport = $ReportInfoArr['isMainReport'];
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isSecondaryGraduate = $SchoolType=="S" && $FormNumber==6;
		
		// Generate Award for Year Report only
		if(!$isMainReport || $Semester!="F")
			return;
		
		// Generate Award for generated Year Report
		$thisReportGenDate = $ReportInfoArr["LastGenerated"];
		if($thisReportGenDate=="0000-00-00 00:00:00" || $thisReportGenDate=="")
			return;
		
		// Get Form Students
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0);
		$StudentIDArr = Get_Array_By_Key($StudentInfoArr,"UserID");
		$numOfStudent = count($StudentIDArr);
		if($numOfStudent==0)
			return;
	
		// Get Semesters
		$SemesterList = getSemesters($this->schoolYearID, 0);
				
		// Main Subject List
		$YearMainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($YearMainSubjectArray) > 0)
			foreach($YearMainSubjectArray as $YearMainSubjectIDAry[] => $YearMainSubjectNameArray[]);

        // Set Up Main / Competition Subject List
        $this->Set_Main_Competition_Subject_List($ClassLevelID);
		
		// Subject Code Mapping
		$SubjectCodeMapping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		// Get Year Marks
		$YearMarksAry = $this->getMarks($ReportID, "", $cons="", $ParentSubjectOnly=1);
		
		// Mark-up Exam Result
		$thisMarkUpResult = $this->GET_EXTRA_SUBJECT_INFO($StudentIDArr, "", $ReportID);
		
		// Conduct List
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		$ConductArr = $lreportcard_extrainfo->Get_Conduct();
		$ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);
		
		// loop Students
		foreach((array)$StudentIDArr as $thisStudentID)
		{
			$SemAcademicExcellent = 0;
			$ConductDataAry = array();
			$thisStudentList = array($thisStudentID);
			
			// loop Semester
			for($j=0; $j<3; $j++)
			{
				$SemMarksAry = array();
				$MainSubjectIDAry = array();
				
				// Get Semester Report
				$thisSemester = $SemesterList[$j]["YearTermID"];
				$thisReportInfo = $this->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
				$thisReportID = $thisReportInfo["ReportID"];
				
				// Check if Semester Report Generated
				$thisReportGenDate = $thisReportInfo["LastGenerated"];
				if($thisReportGenDate=="0000-00-00 00:00:00" || $thisReportGenDate=="")
					break;
				
				// Main Subject List
				$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $thisReportID);
				if(sizeof($MainSubjectArray) > 0)
					foreach($MainSubjectArray as $MainSubjectIDAry[] => $MainSubjectNameArray[]);
				
				// Get Marks
				$SemMarksAry = $this->getMarks($thisReportID, "", $cons="", $ParentSubjectOnly=1);
				$SemMarksAry = $SemMarksAry[$thisStudentID];
				if($isSecondaryGraduate) {
					$thisMarkUpResult[$thisStudentID] = $SemMarksAry;
				}
				
				// Get Conduct
				$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($thisReportID, $thisStudentList);
				$StudentConductID = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
				$StudentConductID = $StudentConductID[$thisStudentID];
				$ConductData = $ConductArr[$StudentConductID];
				$ConductDataAry[$j] = $ConductData;
				
				// Get Award in Comments
				list($goodAcademic, $excellentAcademic, $excellentConduct) = $this->returnReceivedCommentAward($SchoolType, $FormNumber, $SemMarksAry, $MainSubjectIDAry, $ConductData, $SubjectCodeMapping);
				
				// Excellent Academic Award in Each Term
				if($excellentAcademic)
					$SemAcademicExcellent++;
			}
			
			// Receive Execellent Academic Award (Whole Year)
			$AcademicAward = $SemAcademicExcellent==3;
			
			// Get Year Marks
			$YearStudentMarksAry = $YearMarksAry[$thisStudentID];
			
			// Get Mark-up Exam Marks
			$thisMarkUpResult = $thisMarkUpResult[$thisStudentID];
			
			// Receive Execellent Conduct Award (Whole Year)
			$ConductAward = $this->returnReceivedConductAward($YearStudentMarksAry, $thisMarkUpResult, $YearMainSubjectIDAry, $ConductDataAry, $SubjectCodeMapping, $isSecondaryGraduate);

			// 全年操行成績優良獎
			$thisAwardID = $this->Get_AwardID_By_AwardCode("G");
			if($ConductAward){
				$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = "";
			}
			
			// 全年學業成績優異獎
			$thisAwardID = $this->Get_AwardID_By_AwardCode("H");
			if($AcademicAward){
				$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = "";
			}
		}
		
		// Testing
//		$AwardStudentInfoArr[725][0][1971]['DetermineValue'] = "";
//		$AwardStudentInfoArr[726][0][1971]['DetermineValue'] = "";
//		$AwardStudentInfoArr[725][0][1972]['DetermineValue'] = "";
		
		// System Generated Awards
		$GeneratedAwardIDArr = array();
		$thisAwardID = $this->Get_AwardID_By_AwardCode("G");	// 全年操行成績優良獎
		$GeneratedAwardIDArr[] = $thisAwardID;
		$thisAwardID = $this->Get_AwardID_By_AwardCode("H");	// 全年學業成績優異獎
		$GeneratedAwardIDArr[] = $thisAwardID;
		
		### Convert to Update function format and Save to DB
		$SuccessArr['Delete_Old_Report_Award'] = $this->Delete_Award_Generated_Student_Record($ReportID, "", $GeneratedAwardIDArr);
		foreach ((array)$AwardStudentInfoArr as $thisAwardID => $thisAwardStudentInfoArr) {
			$thisAwardStudentInfoArr = $this->Sort_And_Add_AwardRank_In_InfoArr($thisAwardStudentInfoArr, 'DetermineValue', 'desc');
			
			$AwardUpdateDBInfoArr = array();
			foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
				foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
					$thisUpdateDBInfoArr = array();
					$thisUpdateDBInfoArr = $thisAwardSubjectStudentInfoArr;
					$thisUpdateDBInfoArr['SubjectID'] = $thisSubjectID;
					$thisUpdateDBInfoArr['StudentID'] = $thisStudentID;
					$AwardUpdateDBInfoArr[] = $thisUpdateDBInfoArr;
					
					unset($thisUpdateDBInfoArr);
				}
			}
			
			if (count((array)$AwardUpdateDBInfoArr) > 0) {
				$SuccessArr['Update_Award_Generated_Student_Record'][$thisAwardID] = $this->Update_Award_Generated_Student_Record($thisAwardID, $ReportID, $AwardUpdateDBInfoArr);
			}
			unset($AwardUpdateDBInfoArr);
		}
	
		### Save the Award Text
		//$SuccessArr['Delete_Old_Report_Award_Text'] = $this->Delete_Award_Student_Record($ReportID);
		
		### Update Last Generated Date 
		$SuccessArr['Update_LastGeneratedAward_Date'] = $this->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGeneratedAward');
		
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	// Customized Logic for Promotion Status Generation
	function getPromotionStatusCustomized($ReportID, $StudentID, $OtherInfoAry=array(), $LastestTermID=0){
		global $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		// for 146
		//$ReportID = 44;
		
		// Student List
		$thisStudentList = array($StudentID);
		
		// Get Class Level Info
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
		$SchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
		$currentFormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$isPrimaryLevel = $SchoolType == "P";
		$isSecondaryLevel = $SchoolType == "S";
		$isPrimaryGraduate = $isPrimaryLevel && $currentFormNumber == 6;
		$isSecondaryJuniorGrad = $isSecondaryLevel && $currentFormNumber == 3;
		$isSecondaryGraduate = $isSecondaryLevel && $currentFormNumber == 6;

        // Set Up Main / Competition Subject
        $this->Set_Main_Competition_Subject_List($ClassLevelID);
		
		// for 146
		//$currentFormNumber = 6;
		//$isSecondaryGraduate = true;
		//$currentFormNumber = 5;
		//$isSecondaryGraduate = false;
		
		// Get Conduct Grades
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		$ConductArr = $lreportcard_extrainfo->Get_Conduct();
		$ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);
		
		// Get Term Reports
		$TermReportList = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
		$TermReportList = BuildMultiKeyAssoc($TermReportList, "Semester");
		
		// loop Term Reports
		$skipConductChecking = false;
		$TermReportArr = array();
		$TermConductArr = array();
		$TermCount = 0;
		foreach((array)$TermReportList as $ReportInfo)
		{
			// Get Term ReportIDs
			$TermReportID = $ReportInfo["ReportID"];
			$TermReportArr[$TermCount] = $TermReportID;
			
			// Get Term Conduct
			$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($TermReportID, $thisStudentList);
			$StudentConductID = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
			$StudentConductID = $StudentConductID[$StudentID];
			$TermConductArr[$TermCount++] = $ConductArr[$StudentConductID];
			
			// Skip Conduct Checking if any empty Conduct
			if(empty($ConductArr[$StudentConductID])) {
				$skipConductChecking = true;
			}
		}
		
		// Get Overall Conduct
		$OverallConduct = $this->calculateOverallConduct($TermConductArr, $ConductArr);
		// Pass (Rule 1 - Conduct : C- or Upper)
		$passConductReq = $skipConductChecking || strpos($OverallConduct, "A")!==false || strpos($OverallConduct, "B")!==false || strpos($OverallConduct, "C")!==false;
		
		// Get Subject Marks
		$Marks = $this->getMarks($ReportID, $StudentID, $cons=" AND a.IsOverall = '1' ", $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy="", $SubjectID="", $ReportColumnID=0);
		$SubjectIDArr = array_keys((array)$Marks);
		$studentHasSubject = count($SubjectIDArr) > 0;
		
		foreach((array)$SubjectIDArr as $thisIndex => $thisSubjectID){
		    if(!$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $thisSubjectID)){
		        unset($SubjectIDArr[$thisIndex]);
		    }
		}
		$SubjectIDArr = array_values((array)$SubjectIDArr);
		
		// Get Graduate Exam Marks
		if($isSecondaryGraduate){
			$GradExamReportID = $TermReportArr[2];
			$GradExamMarks = $this->getMarks($GradExamReportID, $StudentID, $cons=" AND b.DisplayOrder = 1 ", $ParentSubjectOnly=1);
			$SubjectIDArr = array_keys((array)$GradExamMarks);
			
			foreach((array)$SubjectIDArr as $thisIndex => $thisSubjectID){
			    if(!$this->Is_Student_In_Subject_Group_Of_Subject($GradExamReportID, $StudentID, $thisSubjectID)){
			        unset($SubjectIDArr[$thisIndex]);
			    }
			}
			$SubjectIDArr = array_values((array)$SubjectIDArr);
		}
		
		// Get Mark-up Exam Marks
		$thisMarkUpResult = $this->GET_EXTRA_SUBJECT_INFO($thisStudentList, $SubjectID="", $ReportID);
		$thisMarkUpResult = $thisMarkUpResult[$StudentID];
		
		// Clear Current Year Failed Subjects
		$this->REMOVE_ALL_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $StudentID);
		
		// Get Failed Subject Data
		if($isSecondaryLevel)
		{
			// /* Get Previous Year Failed and Retented Subjects */
			// Get Previous Year Retented Subjects
			$failedSubjectAry = $this->GET_STUDENT_PAST_YEAR_PROMOTION_RETENTION_SUBJECT_RECORD($StudentID, "", array("2"));
			
			// /* Get Last Year Failed Subjects (both Promoted with Subject Retention and Retained) */
			// Get Last Year Failed Subjects (Promoted with Subject Retention only)
			$lastActiveYearID = $this->Get_Previous_YearID_By_Active_Year();
			$lastYearFailedSubject = BuildMultiKeyAssoc($failedSubjectAry, array("Year", "SubjectID"), "SubjectID", 1);
			//$lastYearFailedSubject = $lastYearFailedSubject[2];
			$lastYearFailedSubject = $lastYearFailedSubject[$lastActiveYearID];
			$lastYearFailedSubjectIds = array_values((array)$lastYearFailedSubject);
			
			// Get Failed Subjects without exam this year (Promoted with Subject Retention only)
			$allFailedSubjectIds = BuildMultiKeyAssoc($failedSubjectAry, array("SubjectType", "SubjectID"), "SubjectID", 1);
			$allFailedSubjectIds = array_values((array)$allFailedSubjectIds[2]);
			$allFailedSubjectIds = array_unique((array)$allFailedSubjectIds);
			$notTakenFailedSubjectIds = array_diff($allFailedSubjectIds, $SubjectIDArr);
			
			// Get Failed Subject RecordID (Promoted with Subject Retention only)
			$failedSubjectRecordIds = BuildMultiKeyAssoc($failedSubjectAry, array("SubjectType", "RecordID"), "RecordID", 1);
			$failedSubjectRecordIds = array_values((array)$failedSubjectRecordIds[2]);
		}
		
		if($studentHasSubject)
		{
			$failUnit = 0;
			$failMarkupUnit = 0;
			$failGradExamUnit = 0;
			$failMainSubject = 0;
			$failMainSubject2 = 0;
			$failNormalSubject = 0;
			$failMarkupSubject = 0;
			$isNeedMarkupExam = false;
			$directPromotion = false;
			$trialPromotion = false;
			$isNeedRetention = false;
			$isNoPromotionStatus = false;
			$isDirectReturnEmptyStatus = false;
			$retentionReasonSymbol = "";
			$failedSubjectInfoArr = array();
			$failedRetentedSubjectInfoArr = array();
			$passedRetentedSubjectIDArr = array();
			$takeMarkupSubjectIDArr = array();
			$passedMarkupExamSubjectIDArr = array();
			$displayABSSubjectInfoArr = array();
			
			// Get Subject Code Mapping
			$SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP(0, 0);
			
			// Get Main Subjects
			$MainSubjectArr = ($isPrimaryLevel && $currentFormNumber <= 4)? $this->MainSubjectList2 : $this->MainSubjectList;
			$MainSubjectCount = count($MainSubjectArr);
			
			// Get Competition Subjects
			$CompletitionSubjectArr = $this->CompletitionSubjectList;
			
			// loop Subject Marks
			foreach($Marks as $thisSubjectID => $thisSubjectScore)
			{
//				if(empty($SubjectMark)){
//					continue;
//				}
				
				// Get Subject Code
				$thisSubjectCode = $SubjectCodeMapping[$thisSubjectID];
				
				// Exclude Subject Components
				if($this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID)) {
					continue;
				}
				
				// Exclude Competition Subjects
				if(in_array($thisSubjectCode, $CompletitionSubjectArr)) {
					continue;
				}
				
				// Normal
				if(!$isSecondaryGraduate) {
					// Get Subject Overall Score
					$thisSubjectScore = $thisSubjectScore[0];
					$thisSubjectMark = $thisSubjectScore['Mark'];
					$thisSubjectGrade = $thisSubjectScore['Grade'];
					
					if($thisSubjectGrade == "-") {
						$displayABSSubjectInfoArr[] = array($thisSubjectID, "", "");
						continue;
					}
					
					if($thisSubjectGrade == "*") {
						$isDirectReturnEmptyStatus = true;
						break;
					}
					
					// Skip Special Case (except "+")
					if($this->Check_If_Grade_Is_SpecialCase($thisSubjectGrade) && $thisSubjectGrade != "+")
						continue;
					
					// Get Score Display Settings
					$thisSubjectDisplayType = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 1, 0, $ReportID);
					$thisScaleDisplay = $thisSubjectDisplayType['ScaleDisplay'];
					
					// Get Mark Nature
					$SubjectDisplayMark = $thisScaleDisplay=="M"? $thisSubjectMark : $thisSubjectGrade;
					$SubjectMarkNature = $this->returnMarkNature($ClassLevelID, $thisSubjectID, $SubjectDisplayMark, $ReportColumnID=0, $ConvertBy="", $ReportID);
					
					// Special Case "+" : Absent (Zero mark)
					if($thisSubjectGrade=="+") {
						$thisSubjectMark = 0;
						$SubjectMarkNature = "Fail";
					}
					
					// Failed Subject
					if($SubjectMarkNature=="Fail") {
						// Retention (Rule 3 - Subject Mark : lesser than 30)
						if($thisSubjectMark < 30) {
							$isNeedRetention = true;
							if($retentionReasonSymbol == "") {
								$retentionReasonSymbol = "30";
							}
						}
						
						// Main Subject (Subject Unit : 2)
						if(in_array($thisSubjectCode, $MainSubjectArr)) {
							$failMainSubject++;
							//$failUnit += 2;

                            $failMainSubjectUnit = 2;
                            if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                $failMainSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                $failMainSubjectUnit = $failMainSubjectUnit ? $failMainSubjectUnit : 0;
                            }
                            $failUnit += $failMainSubjectUnit;

							// Main Subject Mark : lesser than 50
							if($thisSubjectMark < 50) {
								$failMainSubject2++;
							}
						}
						// Other Subject (Subject Unit : 1)
						else {
							$failNormalSubject++;
							//$failUnit++;

                            $failSubjectUnit = 1;
                            if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                $failSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                $failSubjectUnit = $failSubjectUnit ? $failSubjectUnit : 0;
                            }
                            $failUnit += $failSubjectUnit;
						}
						$failedSubjectInfoArr[] = array($thisSubjectID, $thisSubjectMark, $thisSubjectGrade);
					}

					// Grade Retention Subejct (Secondary Level only)
					if($isSecondaryLevel)
					{
						// Retention (Rule 2(a)(4) - Subject Mark : lesser than 50 + Fail in 2 consecutive years) - for Promoted with Subject Retention only
						if(!empty($lastYearFailedSubjectIds) && in_array($thisSubjectID, (array)$lastYearFailedSubjectIds) && $thisSubjectMark < 50) {
							$isNeedRetention = true;
							$retentionReasonSymbol = "2Y";
						}
						
						// Retented Subject Handling
						if(!empty($allFailedSubjectIds) && in_array($thisSubjectID, (array)$allFailedSubjectIds))
						{
							// Retented Subject Pass (Rule 2(a)(5)(b) - Subject Mark : equal to or greater than 50)
							if($thisSubjectMark >= 50) {
								$passedRetentedSubjectIDArr[] = $thisSubjectID;
							}
							// Retented Subject Fail
							else {
								// Main Subject (Subject Unit : 2)
								if(in_array($thisSubjectCode, $MainSubjectArr)) {
									//$failUnit += 2;
                                    $failMainSubjectUnit = 2;
                                    if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                        $failMainSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                        $failMainSubjectUnit = $failMainSubjectUnit ? $failMainSubjectUnit : 0;
                                    }
                                    $failUnit += $failMainSubjectUnit;
								}
								// Other Subject (Subject Unit : 1)
								else {
									//$failUnit++;
                                    $failSubjectUnit = 1;
                                    if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                        $failSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                        $failSubjectUnit = $failSubjectUnit ? $failSubjectUnit : 0;
                                    }
                                    $failUnit += $failSubjectUnit;
								}
							}
						}
					}
				}
				// Form 6
				else {
					// Get Graduate Exam Mark
					$thisSubjectScore = $GradExamMarks[$thisSubjectID];
					$thisSubjectScore = array_shift(array_slice((array)$thisSubjectScore, 0, 1));
					$thisSubjectMark = $thisSubjectScore['Mark'];
					$thisSubjectGrade = $thisSubjectScore['Grade'];
					
					if($thisSubjectGrade == "-") {
						$displayABSSubjectInfoArr[] = array($thisSubjectID, "", "");
						continue;
					}
					
					if($thisSubjectGrade == "*") {
						$isDirectReturnEmptyStatus = true;
						break;
					}
					
					// Skip Special Case (except "+")
					if($this->Check_If_Grade_Is_SpecialCase($thisSubjectGrade) && $thisSubjectGrade!="+")
						continue;
					
					// Get Score Display Settings
					$thisSubjectDisplayType = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 1, 0, $GradExamReportID);
					$thisScaleDisplay = $thisSubjectDisplayType['ScaleDisplay'];
					
					// Get Mark Nature
					$SubjectDisplayMark = $thisScaleDisplay=="M"? $thisSubjectMark : $thisSubjectGrade;  
					$SubjectMarkNature = $this->returnMarkNature($ClassLevelID, $thisSubjectID, $SubjectDisplayMark, $ReportColumnID=0, $ConvertBy="", $GradExamReportID);
					
					// Special Case "+" : Absent (Zero mark)
					if($thisSubjectGrade=="+") {
						$thisSubjectMark = 0;
						$SubjectMarkNature = "Fail";
					}
					
					// Failed Subject
					if($SubjectMarkNature=="Fail") {
						// Retention (Rule 3 - Subject Mark : lesser than 30)
						if($thisSubjectMark < 30) {
							$isNeedRetention = true;
							$retentionReasonSymbol = "30";
						}
						
						// Main Subject (Subject Unit : 2)
						if(in_array($thisSubjectCode, $MainSubjectArr)) {
							$failMainSubject++;
							//$failGradExamUnit += 2;

                            $failMainSubjectUnit = 2;
                            if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                $failMainSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                $failMainSubjectUnit = $failMainSubjectUnit ? $failMainSubjectUnit : 0;
                            }
                            $failGradExamUnit += $failMainSubjectUnit;
						}
						// Other Subject (Subject Unit : 1)
						else {
							$failNormalSubject++;
							//$failGradExamUnit++;

                            $failSubjectUnit = 1;
                            if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                $failSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                $failSubjectUnit = $failSubjectUnit ? $failSubjectUnit : 0;
                            }
                            $failGradExamUnit += $failSubjectUnit;
						}
						$failedSubjectInfoArr[] = array($thisSubjectID, $thisSubjectMark, $thisSubjectGrade);
					}
					
					// Grade Retention Subejct
					// Retention (Rule 2(a)(4) - Subject Mark : lesser than 50 + Fail in 2 consecutive years)
//					if(!empty($lastYearFailedSubjectIds) && in_array($thisSubjectID, (array)$lastYearFailedSubjectIds) && $thisSubjectMark < 50) {
//						$isNeedRetention = true;
//					}
					if(!empty($allFailedSubjectIds) && in_array($thisSubjectID, (array)$allFailedSubjectIds))
					{
						// Retented Subject Pass (Rule 2(a)(5)(b) - Subject Mark : equal to or greater than 50)
						if($thisSubjectMark >= 50) {
							$passedRetentedSubjectIDArr[] = $thisSubjectID;
						}
						// Retented Subject Fail
						else {
							// Main Subject (Subject Unit : 2)
							if(in_array($thisSubjectCode, $MainSubjectArr)) {
								//$failGradExamUnit += 2;
                                $failMainSubjectUnit = 2;
                                if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                    $failMainSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                    $failMainSubjectUnit = $failMainSubjectUnit ? $failMainSubjectUnit : 0;
                                }
                                $failGradExamUnit += $failMainSubjectUnit;
							}
							// Other Subject (Subject Unit : 1)
							else {
								//$failGradExamUnit++;
                                $failSubjectUnit = 1;
                                if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                    $failSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                    $failSubjectUnit = $failSubjectUnit ? $failSubjectUnit : 0;
                                }
                                $failGradExamUnit += $failSubjectUnit;
							}
						}
					}
				}
				
				// Failed Subject
				if($SubjectMarkNature=="Fail") {
					// Get Mark-up Exam Mark
					$MarkupSubjectMark = trim($thisMarkUpResult[$thisSubjectID]["Info"]);
					$thisSubjectWithMarkup = isset($thisMarkUpResult[$thisSubjectID]) && $MarkupSubjectMark!="";
					
					if($thisSubjectWithMarkup) {
						// Get Mark-up Exam Status
						$thisMarkupExamStatus = $MarkupSubjectMark < 60? "F" : "P";
						$takeMarkupSubjectIDArr[$thisSubjectID] = $thisMarkupExamStatus;
						
						// Fail Mark-up Subject (lesser than 60)
						if($thisMarkupExamStatus == "F")
						{
							// Primary / Form 3 / 6
							if($isPrimaryLevel || $isSecondaryJuniorGrad || $isSecondaryGraduate) {
								// Retention (Rule 1 - Fail any Subjects)
								$failMarkupSubject++;
							}
							// Other Levels
							else {
								// Main Subject (Subject Unit : 2)
								if(in_array($thisSubjectCode, $MainSubjectArr)) {
									//$failMarkupUnit += 2;
                                    $failMainSubjectUnit = 2;
                                    if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                        $failMainSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                        $failMainSubjectUnit = $failMainSubjectUnit ? $failMainSubjectUnit : 0;
                                    }
                                    $failMarkupUnit += $failMainSubjectUnit;
								}
								// Other Subject (Subject Unit : 1)
								else {
									//$failMarkupUnit++;
                                    $failSubjectUnit = 1;
                                    if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisSubjectCode])) {
                                        $failSubjectUnit = $this->SubjectUnitList[$thisSubjectCode];
                                        $failSubjectUnit = $failSubjectUnit ? $failSubjectUnit : 0;
                                    }
                                    $failMarkupUnit += $failSubjectUnit;
								}
								$failMarkupSubject++;
								$failedRetentedSubjectInfoArr[] = array($thisSubjectID, $thisSubjectMark, $thisSubjectGrade);
							}
						}
						else
						{
							$passedMarkupExamSubjectIDArr[] = $thisSubjectID;
							$passedRetentedSubjectIDArr[] = $thisSubjectID;
						}
					}
				}
			}
			
			if($isDirectReturnEmptyStatus) {
				// Store Promotion Details (for RC_STUDENT_PROMOTION_RETENTION_STATUS)
				$this->UPDATE_PROMOTION_RETENTION_STATUS($ReportID, $ClassLevelID, $SchoolType, $currentFormNumber, $StudentID, 0, $retentionReasonSymbol, $isNeedMarkupExam);
				
				$PromotionStatus = 0;
				return $PromotionStatus;
			}
			
			if(!empty($displayABSSubjectInfoArr)) {
				$isNoPromotionStatus = true;
			}
			
			// Fail Subject Count
			$totalFailSubject = $failMainSubject + $failNormalSubject;
			$withFailSubject = $totalFailSubject > 0;
			
			// Check if completed all required Mark-up Exam
			$totalTakeMarkupSubjectCount = count((array)$takeMarkupSubjectIDArr);
			$completedMarkupExam = $withFailSubject && $totalTakeMarkupSubjectCount==$totalFailSubject;
			
			// Testing
			//$passConductReq = true;
			
			if($isNoPromotionStatus) {
				// do nothing
			}
			// Secondary School
			else if($isSecondaryLevel) {
				// Promotion and Grade Retention Subejct (without exam this year)
				if(!empty($notTakenFailedSubjectIds)) {
					foreach($notTakenFailedSubjectIds as $thisRetentionSubjectID)
					{
						// Failed Subjects
						if($withFailSubject) {
							// Get Retention Subject Code
							$thisRetentionSubjectCode = $SubjectCodeMapping[$thisRetentionSubjectID];
							
							// Main Subject (Subject Unit : 2)
							if(in_array($thisRetentionSubjectCode, $MainSubjectArr)) {
								//$failUnit += 2;
								//$failGradExamUnit += 2;
								//if($completedMarkupExam)
								//	$failMarkupUnit += 2;

                                $failMainSubjectUnit = 2;
                                if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisRetentionSubjectCode])) {
                                    $failMainSubjectUnit = $this->SubjectUnitList[$thisRetentionSubjectCode];
                                    $failMainSubjectUnit = $failMainSubjectUnit ? $failMainSubjectUnit : 0;
                                }
                                $failUnit += $failMainSubjectUnit;
                                $failGradExamUnit += $failMainSubjectUnit;
                                if($completedMarkupExam) {
                                	$failMarkupUnit += $failMainSubjectUnit;
                                }
							}
							// Other Subject (Subject Unit : 1)
							else {
								//$failUnit++;
								//$failGradExamUnit++;
								//if($completedMarkupExam)
								//	$failMarkupUnit++;

                                $failSubjectUnit = 1;
                                if(!empty($this->SubjectUnitList) && isset($this->SubjectUnitList[$thisRetentionSubjectCode])) {
                                    $failSubjectUnit = $this->SubjectUnitList[$thisRetentionSubjectCode];
                                    $failSubjectUnit = $failSubjectUnit ? $failSubjectUnit : 0;
                                }
                                $failUnit += $failSubjectUnit;
                                $failGradExamUnit += $failSubjectUnit;
                                if($completedMarkupExam) {
                                    $failMarkupUnit += $failSubjectUnit;
                                }
							}
						}
						// Retented Subject Pass (Rule 2(a)(5)(c) - Pass all Subjects)
						else {
							$passedRetentedSubjectIDArr[] = $thisRetentionSubjectID;
						}
					}
				}
				
				// Form 6
				if($isSecondaryGraduate)
				{
					/*
					 * Retention Condition
					 * 1. (Rule 11(1)) Fail Subject Unit : 6 or more		($failGradExamUnit >= 6)
					 * 2. (Rule 11(2)) Any Subject Mark : lesser than 30	($isNeedRetention)
					 * 3. (Rule 11(3)) Fail Mark-up Subject : 1 or more		($completedMarkupExam && $failMarkupSubject > 0)
					 */
					//$isNeedRetention = $isNeedRetention || $failGradExamUnit >= 6 || ($withMarkUpExam && $totalFailSubject>0 && $failMarkupSubject > 0);
					$isNeedRetention = $isNeedRetention || $failGradExamUnit >= 6 || ($completedMarkupExam && $failMarkupSubject > 0);
                    // [2020-0717-1026-48164] 2019 : exclude condition - 1 & 2
                    if($this->schoolYear == '2019' && $isNeedRetention)
                    {
                        $isNeedRetention = ($completedMarkupExam && $failMarkupSubject > 0);
                    }
					if(!$isNeedRetention) {
						$isNeedMarkupExam = $withFailSubject;
						$directPromotion = !$withFailSubject || ($completedMarkupExam && $failMarkupSubject==0);
						$isNeedRetention = $completedMarkupExam && !$directPromotion;
					}

                    if($this->schoolYear == '2019') {
                        $retentionReasonSymbol = "";
                    } else if($failGradExamUnit >= 6) {
						$retentionReasonSymbol = "6U";
					}
				}
				// Form 3
				if($isSecondaryJuniorGrad)
				{
					/*
					 * Retention Condition
					 * 1. (Rule 1) 		 Any Conduct Grade : lesser than C-									($passConductReq)
					 * 2. (Rule 1) 		 Fail Mark-up Subject : 1 or more									($completedMarkupExam && $failMarkupSubject > 0)
					 * 3. (Rule 2(a)(3)) Fail Subject Unit : 6 or more										($failUnit >= 6)
					 * 4. (Rule 2(a)(4)) Any Subject Mark : lesser than 50 + Fail in 2 consecutive years	($isNeedRetention)
					 * 5. (Rule 3) 		 Any Subject Mark : lesser than 30									($isNeedRetention)
					 */
					$isNeedRetention = $isNeedRetention || !$passConductReq || ($completedMarkupExam && $failMarkupSubject > 0) || $failUnit >= 6;
                    // [2020-0717-1026-48164] 2019 : exclude condition - 1 & 3 & 4 & 5
                    if($this->schoolYear == '2019' && $isNeedRetention)
                    {
                        $isNeedRetention = ($completedMarkupExam && $failMarkupSubject > 0);
                    }
					if(!$isNeedRetention) {
						$isNeedMarkupExam = $withFailSubject;
						$directPromotion = !$withFailSubject || ($completedMarkupExam && $failMarkupSubject==0);
						$isNeedRetention = $completedMarkupExam && !$directPromotion;
					}

                    if($this->schoolYear == '2019') {
                        $retentionReasonSymbol = "";
                    } else if($failUnit >= 6) {
						$retentionReasonSymbol = "6U";
					}
				}
				// Other Level
				else
                {
					/*
					 * Retention Condition
					 * 1. (Rule 1) 		 Any Conduct Grade : lesser than C-											($passConductReq)
					 * 2. (Rule 2(a)(2)) Any Main Subject Mark : lesser than 50 (at least 2 Main Subjects Fail)		($failMainSubject >= 2 && $failMainSubject2 >= 1)
					 * 3. (Rule 2(a)(3)) Fail Subject Unit : 5 or more												($failUnit >= 5)
					 * 4. (Rule 2(a)(4)) Any Subject Mark : lesser than 50 + Fail in 2 consecutive years			($isNeedRetention)
					 * 5. (Rule 2(a)(5)) Fail Mark-up Subject Unit : 2 or more										($completedMarkupExam && $failMarkupUnit > 2)
					 * 6. (Rule 3) 		 Any Subject Mark : lesser than 30											($isNeedRetention)
					 */
					$isNeedRetention = $isNeedRetention || !$passConductReq || ($failMainSubject >= 2 && $failMainSubject2 >= 1) || $failUnit >= 5 || ($completedMarkupExam && $failMarkupUnit > 2);
                    // [2020-0717-1026-48164] 2019 : exclude condition - 1 & 2 & 3 & 4 & 6
                    if($this->schoolYear == '2019' && $isNeedRetention)
                    {
                        $isNeedRetention = ($completedMarkupExam && $failMarkupUnit > 2);
                    }
					if(!$isNeedRetention) {
						$isNeedMarkupExam = $withFailSubject;
						$directPromotion = !$withFailSubject || ($completedMarkupExam && $failMarkupSubject==0);
						$trialPromotion = !$directPromotion && $completedMarkupExam && $failMarkupUnit <= 2;
						//$isNeedRetention = $completedMarkupExam && !$directPromotion && !$trialPromotion;
					}

                    if($this->schoolYear == '2019') {
                        $retentionReasonSymbol = "";
                    } else {
                        if($failMainSubject >= 2 && $failMainSubject2 >= 1) {
                            $retentionReasonSymbol = "50";
                        }
                        if($failUnit >= 5) {
                            $retentionReasonSymbol = "5U";
                        }
                    }
				}
				
				// Promotion and Grade Retention Subject (without exam this year)
				if($completedMarkupExam && $failMarkupSubject==0) {
					// Retented Subject Pass (Rule 2(a)(5)(c) - Pass all Mark-up Exam)
					if(!empty($notTakenFailedSubjectIds)) {
						foreach($notTakenFailedSubjectIds as $thisRetentionSubjectID) {
							$passedRetentedSubjectIDArr[] = $thisRetentionSubjectID;
						}
					}
				}
			}
			// Primary School
			else {
				// Level 5 & 6
				if($currentFormNumber==5 || $currentFormNumber==6) {
					/*
					 * Retention Condition
					 * 1. (Rule 1) 		 Any Conduct Grade : lesser than C-			($passConductReq)
					 * 2. (Rule 2(b)(2)) Fail Subject Unit : 5 or more				($failUnit >= 5)
					 * 3. (Rule 2(b)(3)) Fail Mark-up Subject : 1 or more			($completedMarkupExam && $failMarkupSubject > 0)
					 * 4. (Rule 3) 		 Any Subject Mark : lesser than 30			($isNeedRetention)
					 */
					$isNeedRetention = $isNeedRetention || !$passConductReq || $failUnit >= 5 || ($completedMarkupExam && $failMarkupSubject > 0);
					if(!$isNeedRetention){
						$isNeedMarkupExam = $withFailSubject;
						$directPromotion = !$withFailSubject || ($completedMarkupExam && $failMarkupSubject==0);
						$isNeedRetention = $completedMarkupExam && !$directPromotion;
					}
					
					if($failUnit >= 5) {
						$retentionReasonSymbol = "5U";
					}
				}
				// Level 1 - 4
				else {
					/*
					 * Retention Condition
					 * 1. (Rule 1) 		 Any Conduct Grade : lesser than C-							($passConductReq)
					 * 2. (Rule 2(b)(2)) Fail Subject : 3 or more (at least 2 Main Subjects)		($failMainSubject >= 2 && ($totalFailSubject - 2) >= 1)
					 * 3. (Rule 2(b)(3)) Fail Subject : 4 or more									($totalFailSubject >= 4)
					 * 4. (Rule 2(b)(4)) Fail Mark-up Subject : 1 or more							($completedMarkupExam && $failMarkupSubject > 0)
					 * 5. (Rule 3) 		 Any Subject Mark : lesser than 30							($isNeedRetention)
					 */
					$isNeedRetention = $isNeedRetention || !$passConductReq || ($failMainSubject >= 2 && ($totalFailSubject - 2) >= 1) || $totalFailSubject >= 4 || ($completedMarkupExam && $failMarkupSubject > 0);
					if(!$isNeedRetention) {
						$isNeedMarkupExam = $withFailSubject;
						$directPromotion = !$withFailSubject || ($completedMarkupExam && $failMarkupSubject==0);
						$isNeedRetention = $completedMarkupExam && !$directPromotion;
					}
				}
			}
		}
		
		// Get Promotion Status
		$promotionStatusAry = $this->customizedPromotionStatus;
		$PromotionStatus = 0;
		// Waiting Make-up Exam (6)
		if($isNeedMarkupExam && !$completedMarkupExam)
			$PromotionStatus = $promotionStatusAry['Waiting_Makeup'];
		// Graduate (P6 / S3 / S6) (3)
		else if($directPromotion && ($isPrimaryGraduate || $isSecondaryJuniorGrad || $isSecondaryGraduate))
			$PromotionStatus = $promotionStatusAry['Graduate'];
		// Promoted (1)
		else if ($directPromotion)
			$PromotionStatus = $promotionStatusAry['Promoted'];
		// Promoted with Subject Retention (2)
		else if($trialPromotion)
			$PromotionStatus = $promotionStatusAry['Promoted_with_Retention'];
		// Retained (4)
		else if($isNeedRetention)
			$PromotionStatus = $promotionStatusAry['Retained'];

		// [2020-0717-1026-48164] "留班" 的同學的狀況轉為 "等候補考"
		if($this->schoolYear == '2019' && $isNeedRetention)
        {
            $PromotionStatus = $promotionStatusAry['Waiting_Makeup'];
            $isNeedMarkupExam = true;

            // Clear retention related data
            $isNeedRetention = false;
            $retentionReasonSymbol = "";
        }

		// Handle Subject Retention (Secondary only)
		if($isSecondaryLevel)
		{
			// Handle Passed Retented Subjects
			$this->UPDATE_RETENTION_SUBJECT_TO_PASS($StudentID, $passedRetentedSubjectIDArr, $failedSubjectRecordIds);
			
			// Clear Current Year Failed Subjects
			//$this->REMOVE_ALL_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $StudentID);
			
			// Clear all Retented Subjects when retained
			if($isNeedRetention){
			    $this->CLEAR_ALL_RETENTION_SUBJECT($StudentID, $failedSubjectRecordIds);
			}
		}

		// Promoted with Subject Retention : Store Retented Subjects
		if($trialPromotion && count($failedRetentedSubjectInfoArr) > 0) {
			$this->INSERT_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $ClassLevelID, $StudentID, $failedRetentedSubjectInfoArr, 2);
		}
		// Have Make-up Exam / Retained : Store Failed Subjects
		//else if(($isNeedMarkupExam && !$completedMarkupExam) || $isNeedRetention){
		else if($isNeedRetention || $isNeedMarkupExam || $isNoPromotionStatus) {
			$this->INSERT_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $ClassLevelID, $StudentID, $failedSubjectInfoArr, 1);
			
			// Store Passed Mark-up Exam Subject
			if(count((array)$passedMarkupExamSubjectIDArr) > 0){
				$this->UPDATE_FAILED_SUBJECT_TO_PASS($ReportID, $ClassLevelID, $StudentID, $passedMarkupExamSubjectIDArr);
			}
		}
		
		// Dropout Checking
		if($isNeedRetention)
		{
			$needToDropout = $this->IS_STUDENT_NEED_TO_DROPOUT($StudentID, $SchoolType, $currentFormNumber);
			$PromotionStatus = $needToDropout? $promotionStatusAry['Dropout'] : $PromotionStatus;
		}
		
		// No Promotion Checking (ABS)
		if($isNoPromotionStatus) {
			$PromotionStatus = 0;
			$this->INSERT_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $ClassLevelID, $StudentID, $displayABSSubjectInfoArr, 5);
		}
		
		// Store Promotion Details (for RC_STUDENT_PROMOTION_RETENTION_STATUS)
		$this->UPDATE_PROMOTION_RETENTION_STATUS($ReportID, $ClassLevelID, $SchoolType, $currentFormNumber, $StudentID, $PromotionStatus, $retentionReasonSymbol, $isNeedMarkupExam);
		
		return $PromotionStatus;
	}
	
	function GET_STUDENT_PAST_YEAR_PROMOTION_RETENTION_SUBJECT_RECORD($StudentIDAry="", $SubjectIDAry="", $SubjectTarget="1", $TargetYearID="")
	{
		$PreviousYearID = $this->Get_Previous_YearID_By_Active_Year();
		$PreviousYearDB = $this->GET_TARGET_YEAR_DATABASE_NAME($PreviousYearID);
		$table = $PreviousYearDB.".RC_PROMOTION_RETENTION_FAILED_SUBJECT";
		
		// Condition
		$conds = "";
		if($StudentIDAry != ""){
			$conds .= " AND StudentID IN (".implode(",", (array)$StudentIDAry).") ";
		}
		if($SubjectIDAry != ""){
			$conds .= " AND SubjectID IN (".implode(",", (array)$SubjectIDAry).") ";
		}
		if($SubjectTarget != ""){
			$conds .= " AND SubjectType IN (".implode(",", (array)$SubjectTarget).") ";
		}
		if($TargetYearID != ""){
			$conds .= " AND Year IN (".implode(",", (array)$TargetYearID).") ";
		}
		
		$sql = "SELECT RecordID, StudentID, ReportID, ClassLevelID, SubjectID, Mark, Grade, SubjectType, Year FROM $table WHERE 1 $conds ";
		return $this->returnResultSet($sql);
	}
	
	function INSERT_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $ClassLevelID, $StudentID, $SubjectInfoArr, $SubjectType=1)
	{
		$result = array();
		$values = array();
		
		# Load Score Settings - Storage & Display
		$SettingStorDisp = 'Storage&Display';
		$storDispSettings = $this->LOAD_SETTING($SettingStorDisp);
		
		$table = $this->DBName.".RC_PROMOTION_RETENTION_FAILED_SUBJECT";
		$targetYearID = $this->schoolYearID;
		
		# Insert Records
		$data_count = count((array)$SubjectInfoArr);
		for($i=0; $i<$data_count; $i++) {
			$thisData = $SubjectInfoArr[$i];
			$thisScore = $thisData[1];
			$thisScore = $this->ROUND_MARK($thisScore, "SubjectTotal");
			$values[] = "('$StudentID', '$ReportID', '$ClassLevelID', '".$thisData[0]."', '$thisScore', '".$thisData[2]."', '$SubjectType', '$targetYearID', '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."', NOW())";
		}
		if(count($values) > 0) {
			$sql = "INSERT INTO $table (StudentID, ReportID, ClassLevelID, SubjectID, Mark, Grade, SubjectType, Year, InputBy, DateInput, ModifiedBy, DateModified) VALUES ";
			$sql .= implode(",", $values);
			$result = $this->db_db_query($sql);
		}
		
		return $result;
	}
	
	function UPDATE_RETENTION_SUBJECT_TO_PASS($StudentID, $passedSubjectIDAry, $existingRecordIDAry)
	{
		$table = $this->DBName.".RC_PROMOTION_RETENTION_FAILED_SUBJECT";
		
		// Reset Retented Subjects to fail first
		$sql = "UPDATE $table SET SubjectType = '2' Where StudentID = '$StudentID' AND RecordID IN ('".implode("','", (array)$existingRecordIDAry)."')";
		$this->db_db_query($sql);
		
		// Update Passed Retented Subjects
		if(count($passedSubjectIDAry) > 0) {
			$passedSubjectIDAry = array_unique($passedSubjectIDAry);
			$sql = "UPDATE $table SET SubjectType = '3' Where StudentID = '$StudentID' AND SubjectID IN ('".implode("','", (array)$passedSubjectIDAry)."') AND RecordID IN ('".implode("','", (array)$existingRecordIDAry)."')";
			$this->db_db_query($sql);
		}
	}
	
	function CLEAR_ALL_RETENTION_SUBJECT($StudentID, $existingRecordIDAry)
	{
	    $table = $this->DBName.".RC_PROMOTION_RETENTION_FAILED_SUBJECT";
	    
	    // Clear all Retented Subjects
	    $sql = "UPDATE $table SET SubjectType = '6' Where StudentID = '$StudentID' AND RecordID IN ('".implode("','", (array)$existingRecordIDAry)."') AND SubjectType != '3' ";
	    $this->db_db_query($sql);
	}
	
	function UPDATE_FAILED_SUBJECT_TO_PASS($ReportID, $ClassLevelID, $StudentID, $PassedSubjectIDArr)
	{
		$table = $this->DBName.".RC_PROMOTION_RETENTION_FAILED_SUBJECT";
		$targetYearID = $this->schoolYearID;
		
		# Insert Records
		$data_count = count((array)$PassedSubjectIDArr);
		if(count($data_count) > 0) {
			$sql = "UPDATE $table SET SubjectType = 4 WHERE Year = '$targetYearID' AND ReportID = '$ReportID' AND StudentID = '$StudentID' AND SubjectID IN ('".implode("', '", $PassedSubjectIDArr)."')";
			$result = $this->db_db_query($sql);
		}
		
		return $result;
	}
	
	function UPDATE_PROMOTION_RETENTION_STATUS($ReportID, $ClassLevelID, $SchoolType, $FormNumber, $StudentID, $PromotionStatus, $retentionReasonSymbol, $IsNeedMarkupExam)
	{
		$retentionReasonSymbol = $retentionReasonSymbol!=""? "'$retentionReasonSymbol'" : "NULL";
		
		$table = $this->DBName.".RC_STUDENT_PROMOTION_RETENTION_STATUS";
		$targetYearID = $this->schoolYearID;
		
		// Check if record exist
		$sql = "SELECT COUNT(RecordID) FROM $table WHERE ClassLevelID = '$ClassLevelID' AND StudentID = '$StudentID' AND Year = '$targetYearID' ";
		$count = $this->returnVector($sql);
		$count = $count[0];
		
		// Insert
		if($count == 0) {
			$sql = "INSERT INTO $table (StudentID, ReportID, ClassLevelID, SchoolType, FormNumber, Year, PromotionStatus, StatusReason, IsNeedMarkupExam, InputBy, DateInput, ModifiedBy, DateModified) VALUES ";
			$sql .= "('$StudentID', '$ReportID', '$ClassLevelID', '$SchoolType', '$FormNumber', '$targetYearID', '$PromotionStatus', $retentionReasonSymbol, '$IsNeedMarkupExam', '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."', NOW())";
			$this->db_db_query($sql);
		}
		// Update
		else if($count > 0) {
			$sql = "UPDATE $table SET PromotionStatus = '$PromotionStatus', StatusReason = $retentionReasonSymbol, IsNeedMarkupExam = '$IsNeedMarkupExam', ModifiedBy = '".$_SESSION['UserID']."', DateModified = NOW() ";
			$sql .= "WHERE ClassLevelID = '$ClassLevelID' AND StudentID = '$StudentID' AND Year = '$targetYearID' ";
			$this->db_db_query($sql);
		}
	}
	
	function REMOVE_ALL_PROMOTION_RETENTION_STUDENT_RECORD($ReportID, $StudentID)
	{
		$table = $this->DBName.".RC_PROMOTION_RETENTION_FAILED_SUBJECT";
		$targetYearID = $this->schoolYearID;
		
		$sql = "DELETE FROM $table Where ReportID = '$ReportID' AND StudentID = '$StudentID' AND Year = '$targetYearID'";
		$this->db_db_query($sql);
	}
	
	function IS_STUDENT_NEED_TO_DROPOUT($StudentID, $SchoolType, $FormNumber, $returnFormNumber=false)
	{
		$RetainCount = 1;
		$RetainForm = array();
		$isDropout = false;
		$isPrimaryLevel = $SchoolType=="P";
		$promotionStatusAry = $this->customizedPromotionStatus;
		
		// loop Previous Years
		$LastYearIDList = $this->Get_Previous_YearID_By_Active_Year(true);
		$LastYearCount = count((array)$LastYearIDList);
		for($i=0; $i<$LastYearCount; $i++)
		{
			// Initiate libreportcard Object
			$LastYearID = $LastYearIDList[$i];
			$lreportcardObj = new libreportcard($LastYearID);
			
			// Get ReportID
			$table = $lreportcardObj->DBName.".RC_REPORT_RESULT";
			$table_report = $lreportcardObj->DBName.".RC_REPORT_TEMPLATE";
			$sql = "SELECT
						report_result.ReportID
					FROM
						$table report_result
						INNER JOIN $table_report report_template ON (report_result.ReportID = report_template.ReportID)
					WHERE
						report_result.StudentID = '$StudentID' AND report_result.ReportColumnID = '0' AND report_result.Semester = 'F' AND report_template.Semester = 'F'";
			$ReportID = $lreportcardObj->returnVector($sql);
			$ReportID = $ReportID[0];
			
			if($ReportID)
			{
				// Get Student Promotion Status
				$promotionStatus = $lreportcardObj->GetPromotionStatusList('', '', $StudentID, $ReportID);
				if($promotionStatus[$StudentID]["Promotion"] == $promotionStatusAry['Retained'])
				{
					// Get School Type and Form Number
					$ReportSetting = $lreportcardObj->returnReportTemplateBasicInfo($ReportID);
					$ClassLevelID = $ReportSetting['ClassLevelID'];
					$thisSchoolType	= $this->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
					$thisFormNumber = $lreportcardObj->GET_FORM_NUMBER($ClassLevelID, 1);
					$isThisSameSchoolType = $SchoolType==$thisSchoolType;
					$isThisSameFormNumber = $FormNumber==$thisFormNumber;
					$RetainForm[] = $thisFormNumber;
					
					// Dropout (Rule 1: Retained in same level)
					if($isThisSameSchoolType && $isThisSameFormNumber) {
						$isDropout = true;
						break;
					}
					// Primary Level only
					if($isPrimaryLevel && $isThisSameSchoolType)
					{
						// Dropout (Rule 3: Retained more than 2 times)
						$RetainCount++;
						if($RetainCount > 2) {
							$isDropout = true;
							break;
						}
						// Dropout (Rule 4: Retained in 2 consecutive levels)
						if($thisFormNumber==($FormNumber-1)) {
							$isDropout = true;
							break;
						}
					}
				}
			}
			
			if($SchoolType == "S" || $isDropout) {
				break;
			}
		}
		
		if($returnFormNumber)
			return array($isDropout, $RetainForm);
		else
			return $isDropout;
	}
	
	// Return Fail Subject Count
	// [0] => Fail Count of Main Subject, [1] => Fail Count of Subject
	function RETURN_FAIL_SUBJECT_COUNT($ReportID, $ClassLevelID, $StudentIDArr, $skipTechnicalSubject=false)
    {
		return false;

		/*
		$returnArr = array();
		
		# Subject Related Info
		$SubjectClassInfoArr = $this->Get_Form_Main_Subject($ClassLevelID, 1);
		$SubjectClassInfoArr = BuildMultiKeyAssoc((array)$SubjectClassInfoArr, array("SubjectID"));
		
		# Form
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SubjectCodeMapping = $FormNumber>0 && $FormNumber<4? $this->GET_SUBJECTS_CODEID_MAP(0, 0) : "";
			
		// Completition Subject List
		$CompletitionSubjectArr = $this->CompletitionSubjectList;
		
		// loop Student
		for($i=0; $i<count($StudentIDArr); $i++){
			$StudentID = $StudentIDArr[$i];
			$failMainSubject = 0;
			$failSubject = 0;
			$specialScoreArr = array();
			
			// Get Marks
			$Marks = $this->getMarks($ReportID, $StudentID, '', 1, 1, '', '', 0, 0, '', true);
			
			// loop Subject
			$totalSubject = count($Marks);
			if($totalSubject > 0){
				foreach($Marks as $curSubjectID => $SubjectMark){
					if(empty($SubjectMark)){
						continue;
					}
					
					// Get Subject Code
					$thisSubjectCode = $SubjectCodeMapping[$curSubjectID];
				
					// Completition Subject Result is excluded
					if(in_array($thisSubjectCode, $CompletitionSubjectArr)){
						continue;
					}
					
					// Get Subject Marks
					$SubjectMark = $SubjectMark[0];
					
					// Get Subject Type
					$SubjectType = $SubjectClassInfoArr[$curSubjectID]["DisplayStatus"];
					
					// Get Score Display Settings
					$subjectSetting = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $curSubjectID, 1, 0, $ReportID);
					$ScaleDisplay = $subjectSetting['ScaleDisplay'];
					
					// Skip if Special Case Or Technical Subject
					if(($this->Check_If_Grade_Is_SpecialCase($SubjectMark['Grade']) && $SubjectMark['Grade']!="+") || ($skipTechnicalSubject && $SubjectType==2))
						continue;
					
					// Get Mark Nature
					$SubjectMark = ($SubjectType==1 || $ScaleDisplay=="M")? $SubjectMark["Mark"] : $SubjectMark["Grade"];
					$SubjectMarkNature = $this->returnMarkNature($ClassLevelID, $curSubjectID, $SubjectMark, 0, '', $ReportID);
					// for "+" - absent (zero mark)
					if($SubjectMark['Grade']=="+"){
						$SubjectMarkNature = "Fail";
						$SubjectMark = 0;
					}
					
					// F1-F3 - Phy, Chem, Bio
					if($FormNumber>0 && $FormNumber<4 && in_array($thisSubjectCode, $this->SpecialSubjectList)){
						$specialScoreArr[] = $SubjectMark;
					}
					// Other Subjects
					// Count Fail Number
					else if($SubjectMarkNature=="Fail"){
						// Main Subject
						if($SubjectType==0){
							$failMainSubject++;
						}
						// Other Subject
						else{
							$failSubject++;
						}
					}
				}
				
				// F1-F3 - Phy, Chem, Bio
				// Check if fail
				if(count($specialScoreArr) > 0){
					$isSubjectFail = (array_sum((array)$specialScoreArr) / count($specialScoreArr)) < 50;
					if($isSubjectFail)
						$failSubject++;
				}
			}
			$returnArr[$StudentID] = array($failMainSubject, $failSubject);
		}
		return $returnArr;
		*/
	}
	
	function returnReceivedCommentAward($SchoolType, $FormNumber, $MarkAry, $MainSubjectIDary, $ConductGrade, $SubjectMapping, $debug=0, $skipConductChecking=false){
		global $eRCTemplateSetting;
		
		$formLimitKey = $SchoolType=="S"? $SchoolType."$FormNumber" : $SchoolType;
		$OtherSubjectLimit = $eRCTemplateSetting['Report']['ReportGeneration']['PassSubjectLimit'][$formLimitKey];
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
			
		// Completition Subject List
		$CompletitionSubjectArr = $this->CompletitionSubjectList;
		
		$goodAry = array();
		$excellentAry = array();
		$goodAry["MainSubject"]["BasicReq"] = 0;
		$goodAry["MainSubject"]["AdvancedReq"] = 0;
		$goodAry["OtherSubject"] = 0;
		$excellentAry["MainSubject"]["BasicReq"] = 0;
		$excellentAry["MainSubject"]["AdvancedReq"] = 0;
		$excellentAry["OtherSubject"] = 0;
		
		$isFailSubject = false;
		$GoodConductGrade = false;
		
		// loop main subject
		foreach ((array)$MainSubjectIDary as $thisMainSubjectID)
		{
			$SubjectCode = $SubjectMapping[$thisMainSubjectID];
			$SubjectMarks = $MarkAry[$thisMainSubjectID];
				
			// Completition Subject Result is excluded
			if(in_array($SubjectCode, $CompletitionSubjectArr)){
				continue;
			}
					
			if(!empty($SubjectMarks))
			{	
				// loop report column
				$columnCount = 0;
				foreach ((array)$SubjectMarks as $thisReportColumnID => $SubjectColumnMarkContent)
				{
					$SubjectColumnMark = $SubjectColumnMarkContent["Mark"];
					$SubjectColumnGrade = $SubjectColumnMarkContent["Grade"];
					
					if($columnCount>=2 && $thisReportColumnID!==0)
						continue;
						
					// Check any column fail
					if(($SubjectColumnMark < 60 && !in_array($SubjectColumnGrade, $SpecialCaseArrH) && trim($SubjectColumnGrade)=="") || $SubjectColumnGrade == "+" || $SubjectColumnGrade == "F")
					{
						$isFailSubject = true;
						break;
					}
					
					// Overall column
					if($thisReportColumnID==0)
					{
						$subjectOverallMark = $SubjectColumnMarkContent["Mark"];
						
						// Main Subject
						if(in_array($SubjectCode, $this->MainSubjectList))
						{
							if($subjectOverallMark >= 65){
								$goodAry["MainSubject"]["BasicReq"]++;
							}
							if($subjectOverallMark >= 70){
								$goodAry["MainSubject"]["AdvancedReq"]++;
								$excellentAry["MainSubject"]["BasicReq"]++;
							}
							if($subjectOverallMark >= 80){
								$excellentAry["MainSubject"]["AdvancedReq"]++;
							}
						}
						// Other Subject
						else
						{
							if($subjectOverallMark >= 70){
								$goodAry["OtherSubject"]++;
							}
							if($subjectOverallMark >= 80){
								$excellentAry["OtherSubject"]++;
							}
						}
					}
					$columnCount++;
				}
			}
			
			if($isFailSubject)
			{
				break;
			}
		}
		
		// Academic Award
		// Grade Level >= B
		$goodAcademicAward = false;
		$excellentAcademicAward = false;
		if(!$isFailSubject && (strpos($ConductGrade, "A")!==false || $ConductGrade=="B+" || $ConductGrade=="B" || $skipConductChecking))
		{
			$goodAcademicAward = $goodAry["MainSubject"]["BasicReq"]==3 && $goodAry["MainSubject"]["AdvancedReq"]>=2 && $goodAry["OtherSubject"]>=$OtherSubjectLimit;
			$excellentAcademicAward = $excellentAry["MainSubject"]["BasicReq"]==3 && $excellentAry["MainSubject"]["AdvancedReq"]>=2 && $excellentAry["OtherSubject"]>=$OtherSubjectLimit;
		}
		
		// Grade Level >= A
		$ExcellentConductAward = false;
		if($ConductGrade=="A" || $ConductGrade=="A+"){
			$ExcellentConductAward = true;
		}
		
		return array($goodAcademicAward, $excellentAcademicAward, $ExcellentConductAward);
	}
	
	function returnReceivedConductAward($MarkAry, $MarkupExamMarkAry, $MainSubjectIDAry, $ConductDataAry, $SubjectMapping, $isSecondaryGraduate=false){
		
		$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
			
		// Completition Subject List
		$CompletitionSubjectArr = $this->CompletitionSubjectList;
		
		// loop main subject
		$isFailSubject = false;
		foreach ((array)$MainSubjectIDAry as $thisMainSubjectID)
		{
			// Overall Mark
			$SubjectCode = $SubjectMapping[$thisMainSubjectID];
			$SubjectMarks = $MarkAry[$thisMainSubjectID][0];
			$MarkupExamMarks = $MarkupExamMarkAry[$thisMainSubjectID]["Info"];
			if($isSecondaryGraduate) {
				$MarkupExamMarks = $MarkupExamMarkAry[$thisMainSubjectID][0]["Mark"];
			}
			
			// Completition Subject Result is excluded
			if(in_array($SubjectCode, $CompletitionSubjectArr)){
				continue;
			}
			
			// Main Subject
			// Check overall score fail
			if(in_array($SubjectCode, $this->MainSubjectList))
			{
				$SubjectOverallMark = $SubjectMarks["Mark"];
				$SubjectOverallGrade = $SubjectMarks["Grade"];
				if(($SubjectOverallMark < 60 && $MarkupExamMarks < 60 && !in_array($SubjectOverallGrade, $SpecialCaseArrH) && trim($SubjectOverallGrade)=="") || $SubjectOverallGrade == "+" || $SubjectOverallGrade == "F")
				{
					$isFailSubject = true;
					break;
				}
			}
		}
		
		// loop semester
		$isAllExcellentConduct = true;
		for($i=0; $i<3; $i++)
		{
			$ConductGrade = $ConductDataAry[$i];
			
			// at least A-
			$isAllExcellentConduct = $isAllExcellentConduct && strpos($ConductGrade, "A")!==false;
			
			// Term 3: at least A
			if($i==2)
				$isAllExcellentConduct = $isAllExcellentConduct && ($ConductGrade=="A" || $ConductGrade=="A+");
			
			if(!$isAllExcellentConduct)
				break;
		}
		
		return $isAllExcellentConduct && !$isFailSubject;
	}
	
	function MergeDisciplineData($OtherInfoData, $eDisData, $sid, $SemID){
		$dataAry = $OtherInfoData;
		$userOtherInfoAry = $dataAry[$sid][$SemID];
		
		// Get discipline config
		$config = $this->getOtherInfoConfig("demerit");
		$ConfigTitleArr = Get_Array_By_Key($config, "EnglishTitle");
		$demeritValArr = array("1", "2", "3", "-2", "-3", "-4", "-5");
		$upgradeValArr = array(4, 4, "", 4, 4, 4, "");
		
		// loop config title
		$configTitleCount = count($ConfigTitleArr);
		for($i=5; $i<$configTitleCount; $i++){
			$currentTitle = $ConfigTitleArr[$i];
			
			// use value in Other Info first
			if(isset($userOtherInfoAry[$currentTitle])){
				continue;
			}
			else{
				// use value from eDiscipline
				$currentVal = $demeritValArr[($i-5)];
				if(isset($eDisData[$currentVal])){
					$dataAry[$sid][$SemID][$currentTitle] = intval($eDisData[$currentVal]);
				}
			}
		}
		
		// loop config title again to handle AP conversion
		for($i=5; $i<$configTitleCount; $i++){
			$currentTitle = $ConfigTitleArr[$i];
			$currentVal = $dataAry[$sid][$SemID][$currentTitle];
			$currentMeritType = $demeritValArr[($i-5)];
			$currentUpdateSettings = $upgradeValArr[($i-5)];
			
			// handle AP upgrade
			if($currentVal > 0 && $currentUpdateSettings > 0 && ($currentMeritType!="3" && $currentMeritType!="-5"))
			{
				$UpgradeVal = floor($currentVal / $currentUpdateSettings);
				$RemainVal = $currentVal % $currentUpdateSettings;
				
				if($UpgradeVal > 0)
				{
					$nextTitle = $ConfigTitleArr[$i+1];
					$dataAry[$sid][$SemID][$currentTitle] = $RemainVal;
					$dataAry[$sid][$SemID][$nextTitle] += $UpgradeVal;
				}
			}
		}
		
		return $dataAry;
	}
	
	function calculateOverallConduct($TermConductAry, $AllTermConductAry)
	{
		global $eRCTemplateSetting;
		$GradeSettingAry = $eRCTemplateSetting['ConductGradePointMapping'];
		
		// Calculate Total Conduct Point
		$GradeTotalPoint = 0;
		for($i=0; $i<3; $i++)
		{
			$thisTermConductGrade = $TermConductAry[$i];
			$thisTermConductPoint = $GradeSettingAry[$thisTermConductGrade];
			if(isset($thisTermConductGrade) && is_int($thisTermConductPoint)) {
				$GradeTotalPoint += $i==2? $thisTermConductPoint * 2 : $thisTermConductPoint;
			}
			else {
				$GradeTotalPoint = $this->EmptySymbol;
				break;
			}
		}
		
		// Calculate Final Conduct Grade
		if($GradeTotalPoint != $this->EmptySymbol)
		{
			// Handle if Total Conduct Point between 2 Grades
			$GradeTotalPoint = $GradeTotalPoint / 4;
			if(is_float($GradeTotalPoint))
			{
				$lastTermConductGrade = $TermConductAry[2];
				$lastTermConductPoint = $GradeSettingAry[$lastTermConductGrade];
				if($lastTermConductPoint > $GradeTotalPoint)
					$GradeTotalPoint = ceil($GradeTotalPoint);
				else
					$GradeTotalPoint = floor($GradeTotalPoint);
			}
			
			// Return Matched Grade
			$GradeSettingAry = array_flip($GradeSettingAry);
			$GradeTotalPoint = $GradeSettingAry[$GradeTotalPoint];
			$GradeTotalPoint = $GradeTotalPoint? $GradeTotalPoint : $this->EmptySymbol;
		}
		
		return $GradeTotalPoint;
	}
	
	function Get_Subject_Stat_Report($StudentDisplayArr,$StatisticDisplayAry,$MasterReportSetting, $isSubjectAllNA, $CustValue=''){
		global $eReportCard, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $eRCTemplateSetting;
		
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		// Check is Whole Year Report
		$isWholeYearReport = $MasterReportSetting["TermID"]=="F";
		
		// Check is Graudate Exam Report
		$isGraduateTerm = $MasterReportSetting["isGraduateTerm"];
		
		# Style Settings
		$SubjectDataBorder = true;
		$BorderBetweenSubject = "border_right";
		$BorderBetweenComponent = "border_right";
		$BorderBetweenColumn = "border_right";
		$HeaderBorder = "border_bottom";
		$valignTop = "top";	
		$page_break = '';
		
		# Column Settings
		$HideReportColumn = false;
		$nonSubjectDataRowSpan = 2; 
		$SubjectColspan = $isWholeYearReport? "1" : "2";
		
		# Get Form Name and Class Name 
		$ClassLevelName = $this->returnClassLevel($MasterReportSetting["ClassLevelID"]); 
		$ClassInfo = $this->GET_CLASSES_BY_FORM($MasterReportSetting["ClassLevelID"]);
		$ClassInfo = BuildMultiKeyAssoc($ClassInfo, "ClassID");
		
		# Get Subject
		$thisDisplaySubjectArr = array_values((array)$MasterReportSetting["SubjectIDArr"]);
		for($i=0; $NextSubjIDArr[$thisDisplaySubjectArr[$i]]=$thisDisplaySubjectArr[$i+1]; $i++);
		
		# Get Left Student
		if($eRCTemplateSetting['MasterReport']['ShowEmptyRowForLeftStudentInReport']) {
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			$lu = new libuser();
			$leftStudentIDList = $lu->returnLeftStudentIDList(); 
		}

		# Get Promotion Status
		$promotionStatusAry = $this->customizedPromotionStatus;
		
		// loop Class
		foreach((array)$StudentDisplayArr as $StudentGroupingID => $StudentList)
		{
			# for Student Row Count
			$row = 0;
			
			// Get Class Teacher Name
			$ClassObj = new year_class($StudentGroupingID, 0, 1);
			$TeacherArr = Get_Array_By_Key($ClassObj->ClassTeacherList, "TeacherName");
			$TeacherArr = array_values(array_unique($TeacherArr));
			$TeacherList = implode(", ", $TeacherArr);
			
			// Report Title
			$KeyName = $MasterReportSetting["isGraduateTerm"]? "GT" : $MasterReportSetting["SemesterNum"];
			
			$html .= "	<div class='report_title' $page_break>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
								<tr height='1024px' valign='top'><td>";
			
			// Table Header
			$html .= "		<table class='report_header' width='100%'>
								<tr>
									<td width='30%'>".$eReportCard["SubjStatReport"]["Class"]." : ".$ClassInfo[$StudentGroupingID]["ClassName"]."</td>
									<td width='30%' align='center'>".$this->GET_ACTIVE_YEAR_NAME()." ".$eReportCard["SubjStatReport"]["Title"]."</td>
									<td width='30%'>&nbsp;</td>
								</tr>
								<tr>
									<td>".$eReportCard['ClassTeacher']." : ".$TeacherList."</td>
									<td align='center'>".$eReportCard['Template']["Half".$KeyName."Ch"]." ".$eReportCard["SubjStatReport"]["TitleResult"]."</td>
									<td>&nbsp;</td>
								</tr>
							</table>";
			
			# Prepare Header Row
			$trow1Arr = array();
			$trow2Arr = array();
			
			# Column Span
			$totalDataSpan = 0;
			
			$html .= "<table id='MasterReport".$StudentGroupingID."' cellpadding=1 cellspacing=0 border=0 class='report_border MasterReport' style='border-collapse:collapse;'>"; // add 1 px width to minimize the table width
			
			################### Header > Student Info Start ###################
			
			foreach((array)$MasterReportSetting["StudentData"] as $StudentData)
			{
				$DisplayStudentData = $eReportCard['SubjStatReport'][$StudentData];
				$SubjectDataColSpan = $StudentData=="ClassNumber"? 1 : (($isWholeYearReport || $isGraduateTerm)? 3 : 2);
				
				$trow1Arr[$StudentData] .= "<td colspan='$SubjectDataColSpan' rowspan='$nonSubjectDataRowSpan' valign='bottom' class='$BorderBetweenSubject border_bottom' style='width:14px'>$DisplayStudentData</td>";
				$totalDataSpan += $SubjectDataColSpan;
			}
			
			################### Header > Student Info End ###################
			
			################### Header > Subject Marks Start ###################
			
			# Get Column Title
			$ColumnTitleArr = $this->returnReportColoumnTitle($MasterReportSetting["ReportID"]);
			$ColumnTitleArr[0] = $eReportCard['Overall'];
			
			# loop Subject
			foreach((array)$thisDisplaySubjectArr as $SubjectID)
			{
				# Skip Subject with all NA
				if(!$MasterReportSetting["DisplayAllNASubject"] && $isSubjectAllNA[$StudentGroupingID][$SubjectID])
					continue;

				# Check Component Subject
				if(!isset($isComponent[$SubjectID]))
					$isComponent[$SubjectID] = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		
				# Check Next Subject is Component
				$NextSubjID = $NextSubjIDArr[$SubjectID];
				if(!empty($NextSubjID) && !isset($isComponent[$NextSubjID]))
					$isComponent[$NextSubjID] = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($NextSubjID);
				
				# Column Style
				$subjectcss = $isComponent[$SubjectID]? "subjectcomponent" : "subject";
				$ColumnBorder = $BorderBetweenColumn;
				$SubjectBorder = $isComponent[$NextSubjID]? $BorderBetweenComponent : $BorderBetweenSubject;
				$SubjectSpacer = $isComponent[$NextSubjID]? $SpaceBetweenComponent : $SpaceBetweenSubject;
				$SubjectRowSpan = ($isWholeYearReport || $isGraduateTerm)? 2 : 1;
				$SubjectColspan = ($isWholeYearReport || $isGraduateTerm)? 1 : 2;
				
				# Get Subject Name
				$SubjectName = $this->GET_SUBJECT_NAME_LANG($SubjectID, $ParLang="b5", $ParShortName=($MasterReportSetting["SubjectDisplay"]=="ShortName"), $ParAbbrName=($MasterReportSetting["SubjectDisplay"]=="Abbr"));
				$trow1Arr[$SubjectID] .= "<td colspan='$SubjectColspan' rowspan='$SubjectRowSpan' class='$borderleft $HeaderBorder $subjectcss $BorderBetweenSubject' valign='bottom' style='width:18px'>$SubjectName</td>";
				$totalDataSpan += $SubjectColspan; 
				
				# Get Column Title
				if(!$isWholeYearReport && !$isGraduateTerm)		// Term Report
				{
					// loop Columns
					$column_ctr = 1;
					foreach((array)$MasterReportSetting["ReportColumnID"] as $ColumnID)
					{
						$ColumnTitle = $ColumnTitleArr[$ColumnID];
						$ColumnTitle = substr($ColumnTitle, 0, 3);
						
						$trow2Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss $BorderBetweenSubject' valign='bottom' style='width:20px'>$ColumnTitle</td>";
						$column_ctr++;
						if($column_ctr==3)
							break;
					}
				}
			}
			
			################### Header > Subject Marks End ###################
						
			$trow1Arr["EmptyHeader"] .= "<td rowspan='2' class='$borderleft $HeaderBorder $subjectcss $BorderBetweenSubject' style='width:1'></td>";
			$totalDataSpan++;
			
			################### Header > Other Info Start ###################
			
			// Header > Other Info
			
			################### Header > Other Info End ###################
			
			################### Header > Grand Marks Start ###################
			
			foreach((array)$MasterReportSetting["GrandMarkDataAry"] as $GrandMarkData)
			{
				$DisplayGrandMarkData = $eReportCard['SubjStatReport'][$GrandMarkData];
				$trow1Arr[$GrandMarkData] .= "<td rowspan=".$nonSubjectDataRowSpan." valign=bottom class='border_bottom $BorderBetweenSubject' nowrap style='width:30px'>$DisplayGrandMarkData</td>";
				$totalDataSpan++; 
			}
			
			################### Header > Grand Marks End ###################
			
			################### Header > Promotion Status Mark Start ###################
			
			foreach((array)$MasterReportSetting["ExtraDataAry"] as $ExtraData)
			{
				$DisplayExtraData = $eReportCard['SubjStatReport'][$ExtraData];
				
				if($ExtraData=="PromotionType")
				{
					$trow1Arr["EmptyHeader2"] .= "<td rowspan='2' class='$borderleft $HeaderBorder $subjectcss $BorderBetweenSubject' style='width:1'></td>";
					$trow1Arr[$ExtraData] .= "<td valign=bottom class='border_bottom subject $BorderBetweenSubject' colspan='5'>$DisplayExtraData</td>";
					$totalDataSpan++;
					
					// loop Promotion Type
					for($promoteType=1; $promoteType<=4; $promoteType++)
					{
						$PromotionKey = $ExtraData.$promoteType;
						if($MasterReportSetting["isSecondaryGraduate"] && $promoteType==1) {
							$PromotionKey .= "_S6";
						}
						
						$DisplayExtraData = $eReportCard['SubjStatReport'][$PromotionKey];
						$trow2Arr[$PromotionKey] .= "<td valign=bottom class='border_bottom $BorderBetweenSubject'>$DisplayExtraData</td>";
					}
					
					$trow2Arr[$ExtraData.$promoteType] .= "<td valign=bottom class='border_bottom $BorderBetweenSubject'>　　</td>";
					$totalDataSpan += 5;
				}
				else if($ExtraData=="AwardType")
				{
					$trow1Arr[$ExtraData] .= "<td rowspan=".$nonSubjectDataRowSpan." valign=bottom class='border_bottom $BorderBetweenSubject' style='width:30px'>$DisplayExtraData</td>";
					$totalDataSpan++;
				}
			}
			
			################### Body > Extra Mark End ###################
			
			$trow1 = "<tr>"; 	// Subject 
			$trow2 = "<tr>";	// Column 
			
			foreach($trow1Arr as $trow1C)
				$trow1 .= $trow1C;
			foreach($trow2Arr as $trow2C)
				$trow2 .= $trow2C;
			
			$trow1 .= "</tr>\n";
			$trow2 .= "</tr>\n";
			$html .= $trow1.($HideReportColumn? "" : $trow2);
			
			# Column Style
			$border_top = "border_top";
			
			// loop Student
			$promotionStatusArr = array(0, 0, 0, 0);
			foreach((array)$StudentList as $StudentID => $DataArr)
			{
				$row++;
				$htmlArr = array();
				
				# Column Style
				if($row==count($StudentList))
					$border_bottom = "border_bottom";
				else if($MasterReportSetting["BottomLineNumber"]==0)
					$border_bottom = "";
				else
					$border_bottom = $row % $MasterReportSetting["BottomLineNumber"]==0? "border_bottom" : "";	
				
				$html .= "<tr>";
				
				################### Body > Student Info Start ###################
				
				foreach((array) $DataArr["StudentInfoData"] as $DisplayFieldName => $StudentInfo)
				{
					$nowrap = "nowrap";
					
					if($DisplayFieldName=="ChineseName")
					{
						# Student Status
						$prefix = "";
						if(!empty($DataArr["SubjectExtraData"]["StudentStatus"])) {
							$prefix = $DataArr["SubjectExtraData"]["StudentStatus"];
						}
						else {
							$prefix = "&nbsp;";
						}
						
						$postfix = "";
						if($MasterReportSetting['SchoolType']=="S")
						{
							# Retented Subject
							$retentionSubjectList = $DataArr["SubjectExtraData"]["retentionSubject"];
							if(!empty($retentionSubjectList) && ($isWholeYearReport || $isGraduateTerm))
								$postfix = $retentionSubjectList;
							else
								$postfix = "&nbsp;";
						}
						else if($MasterReportSetting['SchoolType']=="P")
						{
							# Retented Level
							$retentedFormLevelList = $DataArr["SubjectExtraData"]["retentedFormLevel"];
							if(!empty($retentedFormLevelList) && $isWholeYearReport)
								$postfix = $retentedFormLevelList;
							else
								$postfix = "&nbsp;";
						}
						
						$htmlArr[$DisplayFieldName] .= "<td $nowrap valign='$valignTop' class='$border_bottom' style='width:13px;'>$prefix</td>\n";
						$htmlArr[$DisplayFieldName] .= "<td valign='$valignTop' class='$border_bottom' style='text-align:left; min-width:60px;'>$StudentInfo</td>\n";
						if($isWholeYearReport || $isGraduateTerm)
							$htmlArr[$DisplayFieldName] .= "<td $nowrap valign='$valignTop' class='$border_bottom $BorderBetweenSubject' style='width:28px;'>$postfix</td>\n";
					}
					else
					{
						$htmlArr[$DisplayFieldName] .= "<td $nowrap align='left' valign='$valignTop' class='$border_bottom $BorderBetweenSubject'>$StudentInfo</td>\n";
					}
				}
				
				################### Body > Student Info End ###################
				
				################### Body > Subject Marks Start ###################
				
				// Show Empty Row for Left Student
				if($eRCTemplateSetting['MasterReport']['ShowEmptyRowForLeftStudentInReport'] && !empty($leftStudentIDList) && in_array($StudentID, (array)$leftStudentIDList))
				{
					$htmlArr[$DisplayFieldName] .= "<td colspan='$totalDataSpan' class='border_top border_bottom' align='center'>".$eReportCard['MasterReport']['LeftStudent']."</td>";
				}
				// Normal Case
				else
				{
					$SubjectMarksDisplay = "";
				
					# Column Style
					$borderleft = "border_left";
					
					// loop Subject
					foreach((array)$thisDisplaySubjectArr as $SubjectID)
					{
						# Skip Subject with all NA
						if(!$MasterReportSetting["DisplayAllNASubject"] && $isSubjectAllNA[$StudentGroupingID][$SubjectID])
							continue;
						
						# Column Style
						$subjectcss = $isComponent[$SubjectID]? "subjectcomponent" : "subject";
						$NextSubjID = $NextSubjIDArr[$SubjectID];
						$ColumnBorder = $BorderBetweenColumn;
						$SubjectBorder = $isComponent[$NextSubjID] ? $BorderBetweenComponent : $BorderBetweenSubject;
						$SubjectSpacer = $isComponent[$NextSubjID] ? $SpaceBetweenComponent:$SpaceBetweenSubject;
						
						// loop Report Column
						$column_ctr = 1;
						$ColumnArr = $DataArr["SubjectMarksData"][$SubjectID];
						foreach((array) $ColumnArr as $ReportColumnID => $SubjectMarksArr)
						{
							# Get Column Title
							$ColumnTitle = $ColumnTitleArr[$ReportColumnID];
							
							// loop Student Marks
							foreach((array) $SubjectMarksArr as $SubjectMarks)
							{
								# Column Style
								$borderleft = $SubjectDataBorder?"border_left":"";
								$border_right = $BorderBetweenSubject;
								if(!$isWholeYearReport && !$isGraduateTerm)
								{
									$borderleft = $SubjectDataBorder && $column_ctr==2?"":$borderleft;
									$border_right = $column_ctr==1?"":$border_right;
								}
								
								$htmlArr[$SubjectID] .= "<td class='$borderleft $border_bottom $border_top $subjectcss $border_right' valign='$valignTop' nowrap>$SubjectMarks</td>\n";
							}
							
							$column_ctr++;
							if($column_ctr==3) {
								break;
							}
							
							if($isGraduateTerm)
								break;
						}
					}
				
					if($row % $MasterReportSetting["BottomLineNumber"]===1){
						$EmptyRowSpan = (count((array)$StudentList) - $row + 1 >= 5)? 5 : (count((array)$StudentList) - $row + 1);
						$htmlArr["EmptyHeader"] .= "<td class='$borderleft $border_bottom $border_top $subjectcss $BorderBetweenSubject' style='width:1' rowspan='$EmptyRowSpan'></td>";
					}
			
				################### Body > Subject Marks End ###################
				
				################### Body > Other Info Start ###################

				//	Body > Other Info
				
				################### Body > Other Info End ###################
				
				################### Body > Grand Mark Start ###################
				
					$GrandMarkDisplay = "";
					foreach((array) $DataArr["GrandMarkData"] as $GrandMarkField => $GrandMark)
					{
						$nowrap = $MasterReportSetting["GM".$GrandMarkField."NoWrap"]? "nowrap" : "";
						$nowrap = "nowrap";
						
						// Display "#" if no Fail Subject + Fail Subject Column 
						if($GrandMarkField=="NoOfFailSubject")
						{
							if($DataArr["SubjectExtraData"]["StudentWithFailSubjectCol"] && $GrandMark===0)
								$GrandMark = "#";
						}
						
						$htmlArr[$GrandMarkField] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>$GrandMark</td>\n";
					}
					
				################### Body > Grand Mark End ###################
				
				################### Body > Extra Mark Start ###################
					
					$ExtraDataType = $MasterReportSetting["ExtraDataAry"][0];
					
					# Comment Award Type
					if($ExtraDataType == "AwardType")
					{
						$CommentAward = "";
						$CommentAward .= $DataArr["SubjectExtraData"]["Award_ExcellentAcademic"]? "優" : "　";
						$CommentAward .= "&nbsp;";
						$CommentAward .= $DataArr["SubjectExtraData"]["Award_GoodAcademic"]? "勤" : "　";
						
						$htmlArr["AwardField"] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>$CommentAward</td>\n";
					}
					
					# Promotion Status
					if($ExtraDataType == "PromotionType")
					{
						if($row % $MasterReportSetting["BottomLineNumber"] === 1){
							$EmptyRowSpan = (count((array)$StudentList) - $row + 1 >= 5)? 5 : count((array)$StudentList) - $row + 1;
							$htmlArr["EmptyHeader2"] .= "<td class='$borderleft $border_bottom $border_top $subjectcss $BorderBetweenSubject' style='width:1' rowspan='$EmptyRowSpan'></td>";
						}
						
						$htmlArr["AwardField"] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>".(($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Promoted"] || $DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Graduate"])? "x" : "")."</td>\n";
						$htmlArr["AwardField"] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>".($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Retained"]? "x" : "")."</td>\n";
						$htmlArr["AwardField"] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>".($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Dropout"]? "x" : "")."</td>\n";
						$htmlArr["AwardField"] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>".($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Waiting_Makeup"]? "x" : "")."</td>\n";
						$htmlArr["AwardField"] .= "<td $nowrap valign='$valignTop' class=' $border_bottom $BorderBetweenSubject'>".(!empty($DataArr["SubjectExtraData"]["retentionReason"])? $DataArr["SubjectExtraData"]["retentionReason"] : "")."</td>\n";
						
						if($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Promoted"] || $DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Graduate"])
							$promotionStatusArr[0]++;
						else if($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Retained"])
							$promotionStatusArr[1]++;
						else if($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Dropout"])
							$promotionStatusArr[2]++;
						else if($DataArr["SubjectExtraData"]["PromotionStatus"] == $promotionStatusAry["Waiting_Makeup"])
							$promotionStatusArr[3]++;
					}
				}
					
				################### Body > Extra Mark End ###################
				
				foreach((array)$htmlArr as $idx => $htmlContent)
				{
					$html .= $htmlContent;
				}
				
				$html .= "</tr>";
				
				$border_top = '';
			}
			
			################ Statistic Start #################
			
			$html .= "<tr style='height:1px'>\n";
				$html .= "<td colspan=$totalDataSpan' class='border_bottom $BorderBetweenSubject' style='height:1px'></td>\n";
			$html .= "</tr>";
				
			$isFirstRow = true;
			foreach((array)$StatisticDisplayAry[$StudentGroupingID] as $DisplayStatisticType => $StatisticDataArr)
			{
				$htmlArr = array();
				
				# Stat Colspan
				$StatValueColspan = count($MasterReportSetting["SubjectData"]);
				$StatValueColspan = !$isWholeYearReport && !$isGraduateTerm? 2 : 1;

				$StatisticTitle = $eReportCard["SubjStatReport"][$DisplayStatisticType];
				if($eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage'] && empty($StatisticTitle))
				{
					$CustDisplayStatisticType = str_replace("Cust", "", $DisplayStatisticType);
					$StatisticTitle = $eReportCard['MasterReport']['DataShortName'][$CustDisplayStatisticType];
					if($CustValue["Stat".$DisplayStatisticType."_val"])
						$StatisticTitle .= ' ( '.$eReportCard['MasterReport']['Marks'].' >= '.$eReportCard['MasterReport']['PercentageOfCh'].$CustValue["Stat".$DisplayStatisticType."_val"].'% '.$eReportCard['MasterReport']['PercentageOfEn'].')';
				}
				
				# Subject Marks Statistic
				foreach((array)$thisDisplaySubjectArr as $SubjectID)
				{
					# Skip Subject with all NA
					if(!$MasterReportSetting["DisplayAllNASubject"] && $isSubjectAllNA[$StudentGroupingID][$SubjectID])
						continue;
					
					# Column Style
					$subjectcss = $isComponent[$SubjectID]? "subjectcomponent" : "subject";
					$NextSubjID = $NextSubjIDArr[$SubjectID];
					$ColumnBorder = $BorderBetweenColumn;
					$SubjectBorder = $isComponent[$NextSubjID]? $BorderBetweenComponent : $BorderBetweenSubject;				
					
					$ReportColumnID = 0;
					$thisColumnTitle = $ColumnTitleArr[$ReportColumnID];
					$thisColStatistic = $StatisticDataArr["SubjectMark"][$SubjectID][$ReportColumnID];
					$htmlArr[$SubjectID] .= "<td class='$ColumnBorder $borderleft border_bottom $subjectcss $BorderBetweenSubject' colspan='$StatValueColspan'>$thisColStatistic</td>\n";
					
					# Column Style
					$borderleft = '';
				}
					
				# Other Info
				$OtherInfoDisplay = "";
				foreach((array) $MasterReportSetting["OtherInfoData"] as $OtherInfo)
				{
					$htmlArr[$OtherInfo] .= "<td class='$borderleft border_bottom'></td>\n";
					$htmlArr[$OtherInfo] .= "<td class='$BorderBetweenSubject border_bottom'>$SpaceBetweenSubject</td>\n";						
					$borderleft='';					
				}
				
				# Seperator
				if($isFirstRow){
					$htmlArr["EmptGrandMark"] .= "<td rowspan='2' class='$ColumnBorder $borderleft border_bottom $subjectcss $BorderBetweenSubject' style='width:1'></td>\n";
				}
				
				# Grand Mark Statistic
				$GrandDisplayed = false;
				foreach((array) $MasterReportSetting["GrandMarkDataAry"] as $GrandMarkDisplay)
				{
					# Colspan
					$StatOverallSpan = $GrandDisplayed? (!$isWholeYearReport && !$isGraduateTerm? 2 : 3) : 1;
					$thisGrandMarkStat = $StatisticDataArr["GrandMark"][$GrandMarkDisplay];
					
					$htmlArr[$GrandMarkDisplay] .= "<td class='$borderleft $BorderBetweenSubject border_bottom' colspan='$StatOverallSpan'> $thisGrandMarkStat</td>\n";
					
					if($GrandDisplayed)
						break;
					else
						$GrandDisplayed = true;
				}
				
				# Promotion Status
				if($isWholeYearReport || $isGraduateTerm)
				{
					# Seperator
					if($isFirstRow){
						$htmlArr["EmptGrandMark2"] .= "<td rowspan='2' class='$ColumnBorder $borderleft border_bottom $subjectcss $BorderBetweenSubject' style='width:1'></td>\n";
					}
				
					// loop Promotion Type
					for($promoteType=1; $promoteType<6; $promoteType++)
					{
						$htmlArr["Award"] .= "<td class='$borderleft $BorderBetweenSubject border_bottom'>".$promotionStatusArr[$promoteType-1]."</td>\n";
					}
				}
				
				$ColumnHtml = '';
				$StatTitleColspan = 0;
				$SubjectMarksStart = false;
				foreach($htmlArr as $idx => $htmlContent)
				{
					// Special Handling for Student Data in Statistic Rows
					if(in_array($idx, (array)array_values((array)$MasterReportSetting["StudentData"])))
					{
						if(!$SubjectMarksStart) // Not need to add colspan for studentData among SubjectMarks
						{
							$StatTitleColspan+=2;
							continue;
						}
						else // Add Empty Column
						{
							$ColumnHtml .= "<td class='$borderleft $BorderBetweenSubject border_bottom'> </td>\n";
						}
						
					}
					
					$SubjectMarksStart = true;
					$ColumnHtml .= $htmlContent;
				}
				$StatTitleColspan -= 1; // Exclude Final Separator of Student Data
				
				$StatTitleColspan = !$isWholeYearReport && !$isGraduateTerm? 3 : 4;
				$html .= "<tr>\n";
					$html .= "<td colspan='$StatTitleColspan' class='border_bottom $BorderBetweenSubject' style='white-space: nowrap'>$StatisticTitle</td>\n";
					$html .= $ColumnHtml;
				$html .= "</tr>";
				
				$isFirstRow = false;			
			}
			
			################ Statistic End #################
			
			$html .= "	</table><br>
					</td></tr>";
					
			$html .= "<tr><td><table width='100%' align='center' class='MasterReportFooter border_top'>
							<tr>
								<td width='25%' align='left'>Page: 1</td>
								<td width='45%' align='left'>班主任簽署 Master's/Mistress Signature :</td>
								<td width='30%' align='right'>Printed on: ".date("Y.m.d h:i:s A")."</td>
							</tr>
						</table>
					</td></tr>";
					
			$html .= "</table>
					</div>";
			
			# Page Break before Report Title
			$page_break = "style='page-break-before:always;' ";
		}
		
		# Report CSS Style
		$css .= "<style>\n";
		$css .= "body { margin: 0 auto; }\n";
		$css .= ".report_title  { font-size:16px; font-weight:bold; width:743px; }\n";
		$css .= ".report_header { font-size:13px; }";
		$css .= ".MasterReport td { padding:1.5px 2px; text-align:center; vertical-align:middle; }";
		$css .= ".report_border { border:1px solid #000; }\n";
		$css .= ".border_right { border-right:1px solid #000; }\n";
		$css .= ".border_left { border-left:1px solid #000; }\n";
		$css .= ".border_bottom { border-bottom:1px solid #000; }\n";
		$css .= ".border_top { border-top:1px solid #000; }\n";
		$css .= ".MasterReport { font-size:11px; }\n";
		$css .= ".subjectcomponent { font-size:12px; }\n";
		$css .= ".MasterReportFooter { font-size:11px; }\n";
		$css .= "</style>";
		$html .= $css;
		
		return $html;
	}

	# Map Subject Code and Subject ID
	function GET_SUBJECT_SUBJECTCODE_MAPPING(){
		
		$sql = "SELECT 
					CODEID,
					RecordID
				FROM
					ASSESSMENT_SUBJECT 
				WHERE
					EN_DES IS NOT NULL
					AND RecordStatus = 1
					AND (CMP_CODEID IS NULL || CMP_CODEID = '')
				ORDER BY
					DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 2);
		
		$ReturnArr = array();
		if (sizeof($SubjectArr) > 0) {
			for($i=0; $i<sizeof($SubjectArr); $i++) {
				$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
			}
		}
		return $ReturnArr;
	}
	
// 	function GET_FORM_BASED_SUBJECT_TYPE_SETTING($ClassLevelID, $ReportID='')
// 	{
// 	    $PreloadArrKey = 'GET_FORM_BASED_SUBJECT_TYPE_SETTING';
// 	    $FuncArgArr = get_defined_vars();
// 	    $returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
// 	    if ($returnArr !== false) {
// 	        return $returnArr;
// 	    }
	    
// 	    $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SchoolTypeCode);
// 	    return $SchoolTypeCode;
// 	}
	
	// Return AwardID
	private function Get_AwardID_By_AwardCode($AwardCode) {
		global $lreportcard_award;
		$targetAward = $lreportcard_award->Get_Award_Info_By_Code($AwardCode);
		return $targetAward['AwardID'];
	}
	
	function GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID) {
		$PreloadArrKey = 'GET_SCHOOL_TYPE_BY_CLASSLEVEL';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		$objYear = new Year($ClassLevelID);
		$FormWebSAMSCode = $objYear->WEBSAMSCode;
		$SchoolTypeCode = substr($FormWebSAMSCode, 0, 1);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SchoolTypeCode);
		return $SchoolTypeCode;
	}

	function Set_Main_Competition_Subject_List($curClassLevelId='')
    {
        global $eRCTemplateSetting;

        $isApplyHardCodeSettings = true;
        if($curClassLevelId != '')
        {
            $subjectDataArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($curClassLevelId, 0);
            if(!empty($subjectDataArr))
            {
                $subjectIDArr = array();
                foreach((array)$subjectDataArr as $subjectID => $subjectData) {
                    if($subjectData['is_cmpSubject'] != 1) {
                        $subjectIDArr[] = $subjectID;
                    }
                }

                $subjectTypeSettings = $this->Get_Report_Credit($curClassLevelId, null, $subjectIDArr);
                if(!empty($subjectTypeSettings))
                {
                    // Subject Code Mapping
                    $subjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP();

                    $this->CompletitionSubjectList = array();
                    $this->CompletitionSubjectDisplayLimit = array();
                    $this->SubjectUnitList = array();
                    foreach((array)$subjectTypeSettings as $thisTypeSettings)
                    {
                        $thisSubjectCode = $subjectCodeMapping[$thisTypeSettings['SubjectID']];
                        $thisSubjectType = $thisTypeSettings['Type'];

                        // Core Subjects
                        if($thisSubjectType == $eRCTemplateSetting['Settings']['SubjectType']['Core']) {
                            $this->SubjectUnitList[$thisSubjectCode] = $thisTypeSettings['Credit'];
                        }
                        // Elective Subjects
                        else if($thisSubjectType == $eRCTemplateSetting['Settings']['SubjectType']['Elective']) {
                            $this->CompletitionSubjectList[] = $thisSubjectCode;
                            $this->CompletitionSubjectDisplayLimit[$thisSubjectCode] = 0;
                        }
                        // Competition Subjects
                        else if($thisSubjectType == $eRCTemplateSetting['Settings']['SubjectType']['Competitive']) {
                            $this->CompletitionSubjectList[] = $thisSubjectCode;
                            $this->CompletitionSubjectDisplayLimit[$thisSubjectCode] = $thisTypeSettings['LowerLimit'];
                        }
                    }

                    $isApplyHardCodeSettings = false;
                }
            }
        }

        // Apply Hard Code Settings
        if($isApplyHardCodeSettings) {
            $this->Set_Hardcode_Main_Competition_Subject_List();
        }
    }

    function Set_Hardcode_Main_Competition_Subject_List()
    {
        // Competition Subjects
        // for 2017 & 2018
        if($this->GET_ACTIVE_YEAR() == "2017" || $this->GET_ACTIVE_YEAR() == "2018") {
            $this->CompletitionSubjectList = array("C03", "C10", "C12", "C21", "C24", "C25", "C30", "S42", "S43", "S51", "S55", "S56", "S57", "S58", "S59");
            $this->CompletitionSubjectDisplayLimit = array("C03"=>90, "C10"=>80, "C12"=>90, "C21"=>90, "C24"=>90, "C25"=>90, "C30"=>90, "S42"=>0, "S43"=>0, "S51"=>0, "S55"=>0, "S56"=>0, "S57"=>0, "S58"=>0, "S59"=>0);
        }
        else {
            $this->CompletitionSubjectList = array("C03", "C10", "C12", "C21", "C24", "C25", "C30", "S43", "S55", "S56");
            $this->CompletitionSubjectDisplayLimit = array("C03"=>90, "C10"=>80, "C12"=>90, "C21"=>90, "C24"=>90, "C25"=>90, "C30"=>90, "S43"=>0, "S55"=>0, "S56"=>0);
        }

        // Subject Units
        $this->SubjectUnitList = array();
    }
}
?>