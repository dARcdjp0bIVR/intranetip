<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
# 
# Building layout function :
# 1. ES Progress Report
#	- getReportHeader() - Header
#	- getMSTable() - Student Info & Content & Signature
# 2. ES Semester Report, KG Report
#	- getReportStudentInfo() - Header & Student Info & Remarks
#	- genMSTableMarks() - Content & Signature
# 3. MS Progress Report
#	- getReportHeader() - Header
#	- getReportStudentInfo() - Student Info
#	- getMSTable() - Content & Signature
# 4. MS Transcript (Semester Report)
#	- getReportStudentInfo() - Header & Student Info - mst1
#	- genMSTableMarks() - Content & Attendance & Remarks & Signature - mst2
#
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {
	// store eAttendance settings
	var $EnableEntryLeavePeriod;
	var $ProfileAttendCount;
	var $attendanceMonthlyArr;
	var $StudentGPAArr;
	
	function libreportcardcustom($type="") {
		global $PATH_WRT_ROOT;
		
		$this->libreportcard();
		
		if($type)
		{
			include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");
			$this->subjectTopicObj = new libreportcard_subjectTopic();
			
			include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");
			$this->rc_fileObj = new libreportcard_file();
		}
		
		$this->configFilesType = array("summary", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->attendanceMonthlyArr = array();
		$this->StudentGPAArr = array();
		
		// Count or not count only 
		//$this->specialCasesSet1 = array("-", "N.A.");
		
		//$this->ESChiVersionSubjectComment = array("Chi"=>array("ES G[1-5] Chi.", "ES G[1-5] CSL"), "Math"=>array("G[1-5] ChMath", "G[1-5] CslMath"));
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard;
		
//		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
//		$TableTop .= "<thead>";
//		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
//		$TableTop .= "</thead>";
//		$TableTop .= "<tbody>";
//		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
//		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
//		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
//		$TableTop .= "</tbody>";
//		$TableTop .= "</table>";
//		
//		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
//		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
//		$TableBottom .= $FooterRow;
//		$TableBottom .= "</table>";
//		
//		$x = "";
//		$x .= "<tr><td>";
//			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
//				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
//				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
//			$x .= "</table>";
//		$x .= "</td></tr>";

		$x = "";
		
		$x .= "<tr><td>$TitleTable</td><tr>";
		$x .= "<tr><td style='padding:0px'>$StudentInfoTable</td><tr>";
		$x .= "<tr><td style='padding:0px'>$MSTable</td><tr>";
		$x .= "<tr><td>$MiscTable</td><tr>";
		$x .= "<tr><td>$SignatureTable</td><tr>";
		$x .= "<tr height='624px'><td>&nbsp;</td><tr>";
		$x .= "<tr><td>$FooterRow</td><tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
		global $intranet_root;
		
		$TitleTable = "";
		
		if($ReportID){
			# Report Info
			$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
			$isProgressReport = !$ReportBasicInfo['isMainReport'];
	 		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			// [2015-1013-1434-16164] get Report Type
			// ES/MS Report
			if (is_numeric($FormNumber)) {
				$FormNumber = intval($FormNumber);
				$isESReport = $FormNumber > 0 && $FormNumber < 6;
				$isMSReport = !$isESReport;
			}
			// KG Report
			else {
				$isKGReport = true;
			}
			$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
			
			$obj_year = new academic_year($this->schoolYearID);
			$year_name = $obj_year->YearNameEN;
			$SemID = $ReportBasicInfo['Semester'];

			// report header for Progress Report
			if($isProgressReport)
			{
				$title_td_width = $isESReport? 60 : 46;
				//$term_name = "Quarter ".($this->Get_Semester_Seq_Number($SemID)==1? 1 : 3);
				$term_name = "Quarter ".$this->Get_Semester_Seq_Number($SemID);
				
				// Get Report Logo Image 
				$ReportLogoInfoAry = $this->rc_fileObj->returnFileInfoAry($ReportID, "ReportLogo");
				$LogoFilePath = $ReportLogoInfoAry[0]["FilePath"];
				//$LogoFilePath = $LogoFilePath? (get_website().$LogoFilePath) : $intranet_root."/file/reportcard2008/templates/biba_l_logo.jpg";
				$LogoFilePath = $LogoFilePath? ($intranet_root.$LogoFilePath) : $intranet_root."/file/reportcard2008/templates/biba_l_logo.jpg";
				
				$TitleTable .= "<table class='report_header' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr>";
						$TitleTable .= "<td width='32%'><img height='75px' src='".$LogoFilePath."'></td>";
						$TitleTable .= "<td width='$title_td_width%' align='center' style='background-color: lightgray; vertical-align:bottom'>";
							$TitleTable .= "<div class='form_test_cust_report_title'>
												<span class='report_title'>$year_name $term_name Progress Report Card</span>
											</div>";
						$TitleTable .= "</td>";
						$TitleTable .= "<td>&nbsp;</td>";
					$TitleTable .= "</tr>";
				$TitleTable .= "</table>";
				$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' style='border-bottom:1px solid #000000'>\n";
					$TitleTable .= "<tr><td align='center'>&nbsp;</td></tr>";
				$TitleTable .= "</table>";
			}
		}
		return $TitleTable;
	}
	
	function getFooter($ReportID='', $PrintTemplateType=''){
		$x = "";
		
		if($ReportID)
		{
			# Report Info
			$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
			$isMainReport = $ReportBasicInfo['isMainReport'];
			$SemID = $ReportBasicInfo['Semester'];
 			$SemesterNo = $this->Get_Semester_Seq_Number($SemID);
	 		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
	 		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			// [2015-1013-1434-16164] get Report Type
			// ES / MS Report
			if (is_numeric($FormNumber)) {
				// do nothing
			}
			// KG Report
			else {
				$isKGReport = true;
				$isSemesterReport = $SemID=="F" || ($SemesterNo > 0 && $SemesterNo % 2==0);
			}
			$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
			
			// Page Number Footer
			if($isMainReport && $isKGReport && $isSemesterReport)
			{
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					$x .= "<tr>";
						$x .= "<td align='center'>P.{PAGENO}</td>";
					$x .= "</tr>";
				$x .= "</table>";
			}
		}

		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='', $bilingual=false) {
		global $PATH_WRT_ROOT, $intranet_root, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Report Info
			$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
			$isMainReport = $ReportBasicInfo['isMainReport'];
			$isProgressReport = !$isMainReport;
			$obj_year = new academic_year($this->schoolYearID);
			$year_name = $obj_year->YearNameEN;
			$SemID = $ReportBasicInfo['Semester'];
 			$SemesterNo = $this->Get_Semester_Seq_Number($SemID);
	 		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
			$FormName = $this->returnClassLevel($ClassLevelID);
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			// [2015-1013-1434-16164] get Report Type
			// ES/MS Report
			if (is_numeric($FormNumber)) {
				$FormNumber = intval($FormNumber);
				$isESReport = $FormNumber > 0 && $FormNumber < 6;
				$isMSReport = !$isESReport;
			}
			// KG Report
			else {
				$isKGReport = true;
				$isSemesterReport = $SemID=="F" || $SemesterNo>0 && $SemesterNo%2==0;
			}
			$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
			
 			// get report issue date
			$issueDate = $ReportBasicInfo['Issued']? $ReportBasicInfo['Issued'] : $this->EmptySymbol;
			
			// ES Progress Report
			if($isProgressReport && $isESReport) {
				return "";
			}
			
			// default value - for preview
			$studentInfo = array();
			$studentInfo['Name'] = "XXX";
			$studentInfo['EN_Name'] = "XXX XXX";
			$studentInfo['STRN'] = "XXX";
			$studentInfo['ClassName'] = "XXX";
			//$studentInfo['ClassTeacher'] = "XXX XXX";
			$studentInfo['ClassTeacher'] = "<span><b>Class Teacher 老师姓名: </b></span>";
			$studentInfo['ClassTeacherName'] = "<span><b>XXX XXX</b></span>";
			$studentInfo['ClassTeacherCh'] = "XXX XXX";
			
			# retrieve Student Info
			if($StudentID)		
			{
//				include_once($PATH_WRT_ROOT."includes/libuser.php");
//				$lu = new libuser($StudentID);
//				$studentInfo['Name'] = $lu->ChineseName;
//				$studentInfo['EN_Name'] = $lu->EnglishName." ".$lu->ChineseName;
//				$studentInfo['STRN'] = $lu->STRN;
//				$studentInfo['ClassName'] = $lu->ClassName;
				$currentStudent = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);
				$studentInfo['Name'] = $currentStudent[0]["StudentNameCh"];
				$studentInfo['EN_Name'] = $currentStudent[0]["StudentNameEn"]." ".$currentStudent[0]["StudentNameCh"];
				$studentInfo['STRN'] = $currentStudent[0]["WebSAMSRegNo"];
				$studentInfo['ClassName'] = $currentStudent[0]["ClassName"];
				
				$classTeacherName = "";
				$TeacherName = $this->Get_Student_Class_Teacher_Info($StudentID);
				foreach((array)$TeacherName as $thisTeacherName) {
					if($classTeacherName != "") {
						$classTeacherName .= "<br/>";
						$classTeacherName .= "<span style='visibility: hidden;'><b>Class Teacher 老师姓名: </b></span>";
					}
					$classTeacherName .= "<span><b>".$thisTeacherName["EnglishName"]."</b></span>";
				}
				$studentInfo['ClassTeacherName'] = $classTeacherName;
				//$studentInfo['ClassTeacher'] = $TeacherName[0]["EnglishName"];
				//$studentInfo['ClassTeacherCh'] = $isMainReport && $isESReport && !$bilingual? $this->returnChineseTeacherName($ReportID, $StudentID) : "";
			}
			$studentInfo['ClassTeacher'] .= $studentInfo['ClassTeacherName'];
			
			$StudentInfoTable = "";
			if(!empty($studentInfo))
			{
				$count = 0;
//				$StudentInfoTable .= "<table class='student_info' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'><tr>";
//				$StudentInfoTable .= "<td><table>";
//					$StudentInfoTable .= "<tr><td class='bold_cell'>".$studentInfo['Name']."</td></tr>";
//					$StudentInfoTable .= "<tr><td style='font-size:7pt;'>".$studentInfo['School']."</td></tr>";
//				$StudentInfoTable .= "</table></td>";
//				$StudentInfoTable .= "<td class='bold_cell' align='right'>".$studentInfo['STRN']."</td>";
//				$StudentInfoTable .= "</tr></table>";
				
				// MS Progress Report
				if($isProgressReport && $isMSReport)
				{
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' style='border: 0.05mm solid #BDBDBD; border-bottom: 0mm solid #000000;'>";
						$StudentInfoTable .= "<tr>";
							$StudentInfoTable .= "<td class='bold_cell' width='65%'><b>Student Name: ".$studentInfo['Name']."</b></td>";
							$StudentInfoTable .= "<td class='bold_cell'><b>StudentID: ".$studentInfo['STRN']."</b></td>";
						$StudentInfoTable .= "</tr>";
					$StudentInfoTable .= "</table>";
				}
				// ES Semester Report - Chinese
				else if($isMainReport && $isESReport && !$bilingual)
				{
					// Get Report Logo Image 
					$ReportLogoInfoAry = $this->rc_fileObj->returnFileInfoAry($ReportID, "ReportLogo");
					$LogoFilePath = $ReportLogoInfoAry[0]["FilePath"];
					$LogoFilePath = $LogoFilePath? ($intranet_root.$LogoFilePath) : $PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo.jpg";
					
					// Report Header
					$StudentInfoTable .= "<table class='report_header' width='100%' border='0' cellpadding='2' cellspacing='2' align='center'>\n";
						$StudentInfoTable .= "<tr>";
							$StudentInfoTable .= "<td width='60%' style='vertical-align:top'>";
								$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
									$StudentInfoTable .= "<tr><td><img src='".$LogoFilePath."' style='width:350px; max-height:131px;'></td></tr>";
									$StudentInfoTable .= "<tr><td height='50px'>&nbsp;</td></tr>";
								$StudentInfoTable .= "</table>";
							$StudentInfoTable .= "</td>";
							
							// Student Info
							$StudentInfoTable .= "<td style='vertical-align:bottom'>";
								$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='2'>";
									$StudentInfoTable .= "<tr><td class='report_info_title'>".$this->Get_Chinese_Num($FormNumber)."年级学期成绩报告</td></tr>";
									$StudentInfoTable .= "<tr><td><b>日期: ".$issueDate."</b></td></tr>";
									$StudentInfoTable .= "<tr><td><b>学生姓名: ".$studentInfo['Name']."</b></td></tr>";
									$StudentInfoTable .= "<tr><td><b>班级: ".$studentInfo['ClassName']."</b></td></tr>";
									//$StudentInfoTable .= "<tr><td><b>班主任老师姓名: ".$studentInfo['ClassTeacherCh']."</b></td></tr>";
									//$StudentInfoTable .= "<tr><td><b>老师姓名: ".$studentInfo['ClassTeacherCh']."</b></td></tr>";
								$StudentInfoTable .= "</table>";
							$StudentInfoTable .= "</td>";
							
						$StudentInfoTable .= "</tr>";
					$StudentInfoTable .= "</table>";
					
					// Report Remarks
					$StudentInfoTable .= "<table class='header_describe' width='100%' border='0' cellpadding='2' cellspacing='4' align='center'>\n";
						$StudentInfoTable .= "<tr><td><b>等级标准：</b></td></tr>";
						$StudentInfoTable .= "<tr><td>4- 学生深刻理解本学期的教学相关概念、技能和学习过程，表现<font size='+1'><b>优秀</b></font>。</td></tr>";
						$StudentInfoTable .= "<tr><td>3- 学生对本学期的教学相关概念、技能和学习过程具有<b>良好</b>、一致性的理解。</td></tr>";
						$StudentInfoTable .= "<tr><td>2- 学生开始理解本学期的教学相关概念、技能和学习过程，<font size='+1'><b>达到标准</b></font>。</td></tr>";
						$StudentInfoTable .= "<tr><td>1- 学生不理解本学期的教学相关概念、技能和学习过程，<font size='+1'><b>需要提高</b></font>。</td></tr>";
						$StudentInfoTable .= "<tr><td>NA- 本阶段未涉及</td></tr>";
					$StudentInfoTable .= "</table>";
				} 
				// ES Semester Report - English
				else if($isMainReport && $isESReport && $bilingual)
				{
					$FormNumber = $FormNumber>0? $FormNumber : 1;
					
					// get Attendance Days
					$thisReportAttendanceDays = $ReportBasicInfo["AttendanceDays"];
					$TermTotal = $thisReportAttendanceDays? $thisReportAttendanceDays : 0;
					
					// attendance records
					$attendanceMonthlyAry = $this->attendanceMonthlyArr[$StudentID];
					//$TermTotal = $attendanceMonthlyAry[$SemID]["Total"]? $attendanceMonthlyAry[$SemID]["Total"] : 0;
					$TermAbsent = $attendanceMonthlyAry[$SemID]["Absent"]? $attendanceMonthlyAry[$SemID]["Absent"] : 0;				
					$TermLate = $attendanceMonthlyAry[$SemID]["Late"]? $attendanceMonthlyAry[$SemID]["Late"] : 0;
					
					// attendance records for whole year report
					if($SemID=="F")
					{
 						$ReportColumnAry = $this->returnReportTemplateColumnData($ReportID);
						$firstTermID = $ReportColumnAry[0]["SemesterNum"];
						$secondTermID = $ReportColumnAry[1]["SemesterNum"];
					
						// for dev
//						$firstTermID = 30;
//						$secondTermID = 32;
						//$firstTermTotal = $attendanceMonthlyAry[$firstTermID]["Total"]? $attendanceMonthlyAry[$firstTermID]["Total"] : 0;
						//$secondTermTotal = $attendanceMonthlyAry[$secondTermID]["Total"]? $attendanceMonthlyAry[$secondTermID]["Total"] : 0;
						$firstTermAbsent = $attendanceMonthlyAry[$firstTermID]["Absent"]? $attendanceMonthlyAry[$firstTermID]["Absent"] : 0;
						$secondTermAbsent = $attendanceMonthlyAry[$secondTermID]["Absent"]? $attendanceMonthlyAry[$secondTermID]["Absent"] : 0;
						$firstTermLate = $attendanceMonthlyAry[$firstTermID]["Late"]? $attendanceMonthlyAry[$firstTermID]["Late"] : 0;
						$secondTermLate = $attendanceMonthlyAry[$secondTermID]["Late"]? $attendanceMonthlyAry[$secondTermID]["Late"] : 0;
						
						//$TermTotal = $firstTermTotal + $secondTermTotal;
						$TermAbsent = $firstTermAbsent + $secondTermAbsent;
						$TermLate = $firstTermLate + $secondTermLate;
					}
					
					// Get Report Logo Image 
					$ReportLogoInfoAry = $this->rc_fileObj->returnFileInfoAry($ReportID, "ReportLogo");
					$LogoFilePath = $ReportLogoInfoAry[0]["FilePath"];
					$LogoFilePath = $LogoFilePath? ($intranet_root.$LogoFilePath) : $PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo.jpg";
					
					// Report Header
					$StudentInfoTable .= "<table class='report_header' width='100%' border='0' cellpadding='2' cellspacing='2' align='center'>\n";
						$StudentInfoTable .= "<tr>";
							$StudentInfoTable .= "<td width='50%' style='vertical-align:top'>";
								$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
									$StudentInfoTable .= "<tr><td><img src='".$LogoFilePath."' style='width:350px; max-height:131px;'></td></tr>";
									$StudentInfoTable .= "<tr><td height='50px'>&nbsp;</td></tr>";
								$StudentInfoTable .= "</table>";
							$StudentInfoTable .= "</td>";
							
							// Student Info
							$StudentInfoTable .= "<td style='vertical-align:bottom'>";
								$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='2'>";
									$StudentInfoTable .= "<tr><td class='report_info_title'>Grade $FormNumber Semester Report</td></tr>";
									$StudentInfoTable .= "<tr><td class='report_info_title'>".$this->Get_Chinese_Num($FormNumber)."年级学期成绩报告</td></tr>";
									$StudentInfoTable .= "<tr><td><b>Date 日期: ".$issueDate."</b></td></tr>";
									$StudentInfoTable .= "<tr><td><b>Student Name 学生姓名: ".$studentInfo['EN_Name'] ."</b></td></tr>";
									$StudentInfoTable .= "<tr><td>".$studentInfo['ClassTeacher']."</td></tr>";
									$StudentInfoTable .= "<tr><td><b>Number of School Days 应出席天数: $TermTotal</b></td></tr>";
									$StudentInfoTable .= "<tr><td><b>Number of Days Absent 缺勤天数: $TermAbsent</b></td></tr>";
									$StudentInfoTable .= "<tr><td><b>Number of Days Tardy 迟到天数: $TermLate</b></td></tr>";
								$StudentInfoTable .= "</table>";
							$StudentInfoTable .= "</td>";
							
						$StudentInfoTable .= "</tr>";
					$StudentInfoTable .= "</table>";
					
					// Report Remarks
					$StudentInfoTable .= "<table class='header_describe' width='100%' border='0' cellpadding='2' cellspacing='2' align='center'>\n";
						$StudentInfoTable .= "<tr><td class='engheader_describe'><b>Performance Level Descriptors:</b></td></tr>";
						$StudentInfoTable .= "<tr><td class='engheader_describe'><b>4</b>- Student Demonstrates an in-depth understanding of concepts, skills and processes taught during this reporting period and exceeds the required performance</td></tr>";
						$StudentInfoTable .= "<tr><td class='engheader_describe'><b>3</b>- Student consistently demonstrates an understanding of concepts, skills and processes taught during this reporting period</td></tr>";
						$StudentInfoTable .= "<tr><td class='engheader_describe'><b>2</b>- Student is beginning to demonstrate an understanding of concepts, skills and processes taught during this reporting period</td></tr>";
						$StudentInfoTable .= "<tr><td class='engheader_describe'><b>1</b>- Student does not yet demonstrate an understanding of concepts, skills and processes taught during this reporting period </td></tr>";
						$StudentInfoTable .= "<tr><td class='engheader_describe'><b>NA</b>- Not Assessed</td></tr>";
						$StudentInfoTable .= "<tr><td><b>&nbsp;</td></tr>";
						$StudentInfoTable .= "<tr><td><b>表现等级：</b></td></tr>";
						$StudentInfoTable .= "<tr><td>4- 学生深刻理解本学期的教学相关概念、技能和学习过程，表现优异。</td></tr>";
						$StudentInfoTable .= "<tr><td>3- 学生对本学期的教学相关概念、技能和学习过程具有良好、一致性的理解。</td></tr>";
						$StudentInfoTable .= "<tr><td>2- 学生开始理解本学期的教学相关概念、技能和学习过程。</td></tr>";
						$StudentInfoTable .= "<tr><td>1- 学生不理解本学期的教学相关概念、技能和学习过程。</tr>";
						$StudentInfoTable .= "<tr><td>NA- 本阶段未涉及</tr>";
						$StudentInfoTable .= "<tr><td><b>&nbsp;</td></tr>";
						$StudentInfoTable .= "<tr><td>";
							$StudentInfoTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='2' align='center'>\n";
								$StudentInfoTable .= "<tr><td colspan='5'>The following scale is used to describe student performance in work habits and character.</td></tr>";
								$StudentInfoTable .= "<tr><td colspan='5'>以下标准用于测评学生的学习习惯和性格品质</td></tr>";
								//$StudentInfoTable .= "<tr><td align='center'>O= Outstanding</td><td align='center'>S= Satisfactory</td><td align='center'>I= Inconsistent</td><td align='center'>NI=  Needs Improvement</td></tr>";
								$StudentInfoTable .= "<tr><td align='center'>O= Outstanding</td><td align='center'>S= Satisfactory</td><td align='center'>I= Inconsistent</td><td align='center'>N=  Needs Improvement</td><td align='center'>NA= Not Assessed</td></tr>";
								$StudentInfoTable .= "<tr><td align='center'>出色</td><td align='center'>满意 </td><td align='center'>时好时坏</td><td align='center'>需要进步</td><td align='center'>未涉及</td></tr>";
							$StudentInfoTable .= "</table>";
						$StudentInfoTable .= "</td></tr>";
					$StudentInfoTable .= "</table>";
				}
				// MS Transcript (Semester Report) - mst1
				else if($isMainReport && $isMSReport)
				{
					// student data
					$studentInfo['EN_Name'] = $studentInfo['EN_Name']=="XXX XXX"? "XXX" : $studentInfo['EN_Name'];
					$studentInfo['ClassTeacher'] = $StudentID? $TeacherName[0]["EnglishName"] : "XXX";
					// display "---" for S2 report
					//$StudentGPA = $StudentID? ($SemesterNo!=2? sprintf("%02.2f", $this->StudentGPAArr[$StudentID]["GPA"]) : $this->EmptySymbol) : "S";
					$StudentGPA = $StudentID? ($SemesterNo!=4? sprintf("%02.2f", $this->StudentGPAArr[$StudentID]["GPA"]) : $this->EmptySymbol) : "S";
					
					// Get Report Logo Image 
					$ReportLogoInfoAry = $this->rc_fileObj->returnFileInfoAry($ReportID, "ReportLogo");
					$LogoFilePath = $ReportLogoInfoAry[0]["FilePath"];
					$LogoFilePath = $LogoFilePath? ($intranet_root.$LogoFilePath) : $PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo.png";
		
					// Report Header
					$StudentInfoTable .= "<table class='mstable_header' width='100%' border='0' cellpadding='2' cellspacing='2' align='center'>\n";
						$StudentInfoTable .= "<tr>";
							$StudentInfoTable .= "<td width='26%' style='vertical-align:center'>";
									$StudentInfoTable .= "<img width='250px' src='".$LogoFilePath."'>";
							$StudentInfoTable .= "</td>";
							$StudentInfoTable .= "<td width='24%' align='right' style='font-size:7pt; vertical-align:bottom;'><b>Beijing International Bilingual Academy</b><br>Monet garden Main Campus Building 11<br>No. 5 Yumin Road, Houshayu, Shuny<br>101300 Beijing<br>Tel: +86 10 80410390 - http://www.bibachina.org</td>";
							
							// Student Info
							$StudentInfoTable .= "<td width='50%' class='ms_address' style='vertical-align:top'>";
								$StudentInfoTable .= "<table class='report_info_mstable' width='100%' border='0' cellpadding='4' cellspacing='4'>";
									$StudentInfoTable .= "<tr>";
										$StudentInfoTable .= "<td width='25%'>School Year</td>";
										$StudentInfoTable .= "<td width='25%'>$year_name</td>";
										$StudentInfoTable .= "<td width='25%'>Homeroom</td>";
										$StudentInfoTable .= "<td width='25%'>".$studentInfo['ClassTeacher']."</td>";
									$StudentInfoTable .= "</tr>";
									$StudentInfoTable .= "<tr>";
										$StudentInfoTable .= "<td>Grading Period</td>";
										$StudentInfoTable .= "<td>$FormName</td>";
										$StudentInfoTable .= "<td>Grade Level</td>";
										$StudentInfoTable .= "<td>$FormNumber</td>";
									$StudentInfoTable .= "</tr>";
									$StudentInfoTable .= "<tr>";
										$StudentInfoTable .= "<td>Student Name</td>";
										$StudentInfoTable .= "<td colspan='3' align='left' style='paddding-left:4px;'>".$studentInfo['EN_Name']."</td>";
									$StudentInfoTable .= "</tr>";
									$StudentInfoTable .= "<tr>";
										$StudentInfoTable .= "<td>Student ID</td>";
										$StudentInfoTable .= "<td>".$studentInfo['STRN']."</td>";
										$StudentInfoTable .= "<td>GPA</td>";
										$StudentInfoTable .= "<td>$StudentGPA</td>";
									$StudentInfoTable .= "</tr>";
								$StudentInfoTable .= "</table>";
							$StudentInfoTable .= "</td>";
							
						$StudentInfoTable .= "</tr>";
					$StudentInfoTable .= "</table>";
				}
				// KG Progress & Semester Report
				else if($isMainReport && $isKGReport)
				{
					// get Attendance Days
					$thisReportAttendanceDays = $ReportBasicInfo["AttendanceDays"];
					$TermTotal = $thisReportAttendanceDays? $thisReportAttendanceDays : 0;
					
					// attendance records
					$attendanceMonthlyAry = $this->attendanceMonthlyArr[$StudentID];
					//$TermTotal = $attendanceMonthlyAry[$SemID]["Total"]? $attendanceMonthlyAry[$SemID]["Total"] : 0;
					$TermAbsent = $attendanceMonthlyAry[$SemID]["Absent"]? $attendanceMonthlyAry[$SemID]["Absent"] : 0;				
					$TermLate = $attendanceMonthlyAry[$SemID]["Late"]? $attendanceMonthlyAry[$SemID]["Late"] : 0;
					
					// attendance records for whole year report
					if($SemID=="F")
					{
 						$ReportColumnAry = $this->returnReportTemplateColumnData($ReportID);
						$firstTermID = $ReportColumnAry[0]["SemesterNum"];
						$secondTermID = $ReportColumnAry[1]["SemesterNum"];
					
						// for dev
//						$firstTermID = 30;
//						$secondTermID = 32;
						//$firstTermTotal = $attendanceMonthlyAry[$firstTermID]["Total"]? $attendanceMonthlyAry[$firstTermID]["Total"] : 0;
						//$secondTermTotal = $attendanceMonthlyAry[$secondTermID]["Total"]? $attendanceMonthlyAry[$secondTermID]["Total"] : 0;
						$firstTermAbsent = $attendanceMonthlyAry[$firstTermID]["Absent"]? $attendanceMonthlyAry[$firstTermID]["Absent"] : 0;
						$secondTermAbsent = $attendanceMonthlyAry[$secondTermID]["Absent"]? $attendanceMonthlyAry[$secondTermID]["Absent"] : 0;
						$firstTermLate = $attendanceMonthlyAry[$firstTermID]["Late"]? $attendanceMonthlyAry[$firstTermID]["Late"] : 0;
						$secondTermLate = $attendanceMonthlyAry[$secondTermID]["Late"]? $attendanceMonthlyAry[$secondTermID]["Late"] : 0;
						
						//$TermTotal = $firstTermTotal + $secondTermTotal;
						$TermAbsent = $firstTermAbsent + $secondTermAbsent;
						$TermLate = $firstTermLate + $secondTermLate;
					}
					
					// Semseter Report Header
					if($isSemesterReport)
					{
						$seperate = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					
						// Get Report Logo Image 
						$ReportLogoInfoAry = $this->rc_fileObj->returnFileInfoAry($ReportID, "ReportLogo");
						$LogoFilePath = $ReportLogoInfoAry[0]["FilePath"];
						$LogoFilePath = $LogoFilePath? ($intranet_root.$LogoFilePath) : $PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo.jpg";
						
						$StudentInfoTable .= "<table class='kg_report_header' width='100%' border='0' cellpadding='2' cellspacing='2' align='center'>\n";
							$StudentInfoTable .= "<tr>";
								$StudentInfoTable .= "<td width='55%' style='vertical-align:top'>";
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
										$StudentInfoTable .= "<tr><td><img src='".$LogoFilePath."' style='width:400px; max-height:292px;'></td></tr>";
										$StudentInfoTable .= "<tr><td height='50px'>&nbsp;</td></tr>";
									$StudentInfoTable .= "</table>";
								$StudentInfoTable .= "</td>";
								
								// Student Info
								$StudentInfoTable .= "<td class='kg_report_header_table' style='vertical-align:bottom; padding-top:20px;'>";
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='2'>";
										$StudentInfoTable .= "<tr><td class='report_info_title' style='padding-left:15px;'>Semester Report for KG</td></tr>";
										$StudentInfoTable .= "<tr><td class='report_info_title' style='padding-left:45px;'>KG学期成绩报告</td></tr>";
										$StudentInfoTable .= "<tr><td><b>Date 日期: ".$issueDate."</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Student Name 学生姓名: ".$studentInfo['EN_Name'] ."</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Class 班级: ".$studentInfo['ClassName']."</b></td></tr>";
										$StudentInfoTable .= "<tr><td style='line-height:normal; padding-top: 7.5px; padding-bottom: 7.5px;'>".$studentInfo['ClassTeacher']."</td></tr>";
										$StudentInfoTable .= "<tr><td><b>Number of School Days 应出席天数: $TermTotal</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Number of Days Absent 缺勤天数: $TermAbsent</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Number of Days Tardy 迟到天数: $TermLate</b></td></tr>";
									$StudentInfoTable .= "</table>";
								$StudentInfoTable .= "</td>";
								
							$StudentInfoTable .= "</tr>";
						$StudentInfoTable .= "</table>";
					
						// Report Remarks
						$StudentInfoTable .= "<table class='header_describe' width='100%' border='0' cellpadding='2' cellspacing='2' align='center' style='margin-top:4mm; padding:4px;'>\n";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>Performance Level Descriptors:</b></td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>4</b>- Student Demonstrates an in-depth understanding of concepts, skills and processes taught during this reporting period and exceeds the required performance</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>3</b>- Student consistently demonstrates an understanding of concepts, skills and processes taught during this reporting period</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>2</b>- Student is beginning to demonstrate an understanding of concepts, skills and processes taught during this reporting period</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>1</b>- Student does not yet demonstrate an understanding of concepts, skills and processes taught during this reporting period </td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>NA</b>- Not Assessed</tr>";
							$StudentInfoTable .= "<tr><td><b>&nbsp;</td></tr>";
							//$StudentInfoTable .= "<tr><td class='engheader_describe'><b>Behaviors that support Learning:</b>$seperate O=Outstanding$seperate S=Satisfactory$seperate I=Inconsistent$seperate N=Needs Improvement</td></tr>";
							//$StudentInfoTable .= "<tr><td class='engheader_describe'><b>Shaded Categories:</b>  skill/concept not assessed in this reporting in this period</td></tr>";
							//$StudentInfoTable .= "<tr><td><b>&nbsp;</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'><b>表现等级：</b></td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'>4- 学生深刻理解本学期的教学相关概念、技能和学习过程，表现优异。</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'>3- 学生对本学期的教学相关概念、技能和学习过程具有良好、一致性的理解。</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'>2- 学生开始理解本学期的教学相关概念、技能和学习过程。</td></tr>";
							$StudentInfoTable .= "<tr><td class='engheader_describe'>1- 学生不理解本学期的教学相关概念、技能和学习过程。</tr>";
							$StudentInfoTable .= "<tr><td>NA- 本阶段未涉及</td></tr>";
							//$StudentInfoTable .= "<tr><td><b>&nbsp;</td></tr>";
							//$StudentInfoTable .= "<tr><td class='engheader_describe'><b>行为等级测定：</b>$seperate O=出色$seperate S=满意$seperate I=时好时坏$seperate N=需要进步</td></tr>";
							//$StudentInfoTable .= "<tr><td class='engheader_describe'><b>灰色条目：</b>本学期不涉及此条。</td></tr>";
						$StudentInfoTable .= "</table>";
					}
					// Progress Report Header
					else
					{
						// Get Report Logo Image 
						$ReportLogoInfoAry = $this->rc_fileObj->returnFileInfoAry($ReportID, "ReportLogo");
						$LogoFilePath = $ReportLogoInfoAry[0]["FilePath"];
						$LogoFilePath = $LogoFilePath? ($intranet_root.$LogoFilePath) : $PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo.jpg";
						
						$StudentInfoTable .= "<table class='kg_report_header' width='100%' border='0' cellpadding='2' cellspacing='2' align='center'>\n";
							$StudentInfoTable .= "<tr>";
								$StudentInfoTable .= "<td width='55%' style='vertical-align:top'>";
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
										$StudentInfoTable .= "<tr><td><img src='".$LogoFilePath."' style='width:400px; max-height:292px;'></td></tr>";
										$StudentInfoTable .= "<tr><td height='50px'>&nbsp;</td></tr>";
									$StudentInfoTable .= "</table>";
								$StudentInfoTable .= "</td>";
								
								// Student Info
								$StudentInfoTable .= "<td class='kg_report_header_table' style='vertical-align:bottom; padding-top:20px;'>";
									$StudentInfoTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='2'>";
										$StudentInfoTable .= "<tr><td class='report_info_title' style='padding-left:15px;'>Progress Report for KG</td></tr>";
										$StudentInfoTable .= "<tr><td class='report_info_title' style='padding-left:45px;'>学前班进度报告</td></tr>";
										$StudentInfoTable .= "<tr><td><b>Date 日期: ".$issueDate."</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Student Name 学生姓名: ".$studentInfo['EN_Name'] ."</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Class 班级: ".$studentInfo['ClassName']."</b></td></tr>";
										$StudentInfoTable .= "<tr><td style='line-height:normal; padding-top: 7.5px; padding-bottom: 7.5px;'>".$studentInfo['ClassTeacher']."</td></tr>";
										$StudentInfoTable .= "<tr><td><b>Number of School Days 应出席天数: $TermTotal</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Number of Days Absent 缺勤天数: $TermAbsent</b></td></tr>";
										$StudentInfoTable .= "<tr><td><b>Number of Days Tardy 迟到天数: $TermLate</b></td></tr>";
									$StudentInfoTable .= "</table>";
								$StudentInfoTable .= "</td>";
								
							$StudentInfoTable .= "</tr>";
						$StudentInfoTable .= "</table>";
						
						// Report Remarks
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' style='padding:0px; padding-top:2px; padding-bottom:2px;'>\n";
							$StudentInfoTable .= "<tr><td width='100%'>";
								$StudentInfoTable .= "<table class='kg_header_describe' width='100%' border='0' cellpadding='1' cellspacing='2' align='center'>\n";
									$StudentInfoTable .= "<tr><td colspan='5'>The following scale is used to describe student performance in \"WORK HABITS AND VIRTUES\" and \"KINDERGARTEN SKILLS\"</td></tr>";
									$StudentInfoTable .= "<tr><td colspan='5'>以下标准用于测评学生的\"学习习惯和品德\"以及\"幼儿园技能\"</td></tr>";
									//$StudentInfoTable .= "<tr><td style='padding-bottom:4px;'>O= Outstanding</td><td style='padding-bottom:4px;'>S= Satisfactory</td><td style='padding-bottom:4px;'>I= Inconsistent</td><td style='padding-bottom:4px;'>NI=  Needs Improvement</td></tr>";
									$StudentInfoTable .= "<tr><td style='padding-bottom:4px;'>O= Outstanding</td><td style='padding-bottom:4px;'>S= Satisfactory</td><td style='padding-bottom:4px;'>I= Inconsistent</td><td style='padding-bottom:4px;'>NI=  Needs Improvement</td><td style='padding-bottom:4px;'>NA= Not Assessed</td></tr>";
								$StudentInfoTable .= "</table>";
							$StudentInfoTable .= "</td></tr>";
						$StudentInfoTable .= "</table>";
					}
				}
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='', $bilingual=false) {
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$isProgressReport = !$isMainReport;
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		// [2015-1013-1434-16164] get Report Type
		// ES/MS Report
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
		}
		$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
		
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$sizeofSubjectCol = 0;
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		// Handle archive students
		if(empty($StudentInfoArr) && $StudentID!=""){
			$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);
			
			// Get ClassID
			$StudentInfoArr[0]["ClassID"] = $lclass->getClassID($StudentInfoArr[0]['ClassName'], $this->schoolYearID);
		}
		
		$thisClassName = $StudentInfoArr[0]['ClassName'];
		
		# retrieve Instructor
		$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
		foreach($ClassTeacherAry as $key=>$val)
		{
			$CTeacher[] = $val['CTeacher'];
		}
		$instructor = !empty($CTeacher) ? implode(", ", $CTeacher) : $this->EmptySymbol;
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		# retrive Student Attendance Array
//		$attendanceAry = $this->getStudentListAttendanceRecords((array)$StudentID);
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID, "en");
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		$sizeofSubjectCol = sizeof($SubjectArray);
		
		# retrieve marks
		$studentIDOrder = $StudentID==""? "-" : $StudentID;
		$MarksAry = $this->getMarks($ReportID, $studentIDOrder, '', 0, 1);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $SubjectIDArray, $SubjectTeacherCommentAry, $attendanceAry, $bilingual);
		$MarksDisplayAry = $MSTableReturned;
				
		##########################################
		# Start Generate Table
		##########################################
		
		// Semester Report
		if($isMainReport)
		{
			$DetailsTable = $MarksDisplayAry;
		}
		// Progress Report
		else 
		{
			$DetailsTable = "";
			$DetailsTable .= "<table class='result_table' width='100%' align='center' border='0' cellspacing='0' cellpadding='0' style='margin-bottom:-2mm'>";
			$DetailsTable .= "<thead>";
				$DetailsTable .= "<tr>";
				
				// MS Progress Report
				if($isMSReport)
				{
					// Table Header
					$DetailsTable .= "<th width='67%' class='bold_cell'><b>Class</b></td>";
					$DetailsTable .= "<th width='20%' class='bold_cell'><b>Instructor</b></td>";
					$DetailsTable .= "<th width='13%' class='bold_cell'><b>GPA</b></td>";
				}
				// ES Progress Report
				else 
				{
					// for Preview only 
					$studentInfo = array();
					$studentInfo['Name'] = "XXX";
					$studentInfo['ClassName'] = "XXX";
					
					# Retrieve Student Info
					if($StudentID)
					{
//						include_once($PATH_WRT_ROOT."includes/libuser.php");
//						$lu = new libuser($StudentID);
//						$studentInfo['Name'] = $lu->EnglishName;
//						$studentInfo['ClassName'] = $lu->ClassName;
						$currentStudent = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);
						$studentInfo['Name'] = $currentStudent[0]["StudentNameEn"];
						$studentInfo['ClassName'] = $currentStudent[0]["ClassName"];
					}
					
					// Table Header (with Student info)
					$StudentInfoTable .= "<table class='student_info' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
						$StudentInfoTable .= "<tr>";
							$StudentInfoTable .= "<td width='75%' class='bold_cell'><b>Student: ".$studentInfo['Name']."</b></td>";
							$StudentInfoTable .= "<td class='bold_cell'><b>Class: ".$studentInfo['ClassName']."</b></td>";
						$StudentInfoTable .= "</tr>";
					$StudentInfoTable .= "</table>";
					
					$DetailsTable .= "<td width='80%'>$StudentInfoTable</td>";
					$DetailsTable .= "<td width='20%' class='bold_cell'><b>Teacher</b></td>";
				}
				
				$DetailsTable .= "</tr>";
			$DetailsTable .= "</thead>";
			
			// Get Last Display Subject first
			// loop Subjects
			$LastSubjectID = 0;
			for($i=0; $i<$sizeofSubjectCol; $i++)
			{
				$thisSubjectID = $SubjectIDArray[$i];
				
				// Major Subject only
				if(in_array($thisSubjectID, $MainSubjectIDArray) != true)	continue;
				
				// for Preview only
				if($StudentID=="")	continue;
				
				# If all marks is "*", then don't display
				if (sizeof($MarksAry[$thisSubjectID]) > 0)
				{
					$Droped = 1;
					foreach($MarksAry[$thisSubjectID] as $cid => $da) {
						if($da['Grade']!="*")	$Droped=0;
					}
				}
				if($Droped)	continue;
				
				# Empty Teacher Comment
				if(trim($SubjectTeacherCommentAry[$thisSubjectID])=="" || trim($SubjectTeacherCommentAry[$thisSubjectID])=="\n")
					continue;
				
				# Not In Subject Group
				$SubjectGroups = $this->Get_Student_Studying_Subject_Group($SemID, $StudentID, $thisSubjectID, true);
				if($SubjectGroups == null) continue;
				
				$LastSubjectID = $thisSubjectID;
			}
			
			// loop Subjects
			for($i=0; $i<$sizeofSubjectCol; $i++)
			{
				$thisSubjectID = $SubjectIDArray[$i];
				
				# Major Subject only
				if(in_array($thisSubjectID, $MainSubjectIDArray) != true)	continue;
				
				# Empty Subject row - for Preview only
				if($StudentID=="")
				{
					$DetailsTable .= "<tr>";
					$DetailsTable .= "<td class='bold_cell' style='padding:0px'>";
						$DetailsTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
							$DetailsTable .= "<tr><td class='bold_cell'>XXX ".$SubjectNameArray[$i]."</td></tr>";
							$DetailsTable .= "<tr><td style='font-size:10pt;'>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX</td></tr>";
						$DetailsTable .= "</table>";
					$DetailsTable .= "</td>";
					$DetailsTable .= "<td class='bold_cell'>XXX</td>";
					
					// MS Progress Report
					if($isMSReport){
						$DetailsTable .= "<td class='bold_cell' style='padding:0px'>".$MarksDisplayAry[$thisSubjectID]."</td>";
					}
					$DetailsTable .= "</tr>";
					
					continue;
				}
				
				# If all marks is "*", then don't display
				if (sizeof($MarksAry[$thisSubjectID]) > 0)
				{
					$Droped = 1;
					foreach($MarksAry[$thisSubjectID] as $cid => $da) {
						if($da['Grade']!="*")	$Droped=0;
					}
				}
				if($Droped)	continue;
				
				# Empty Teacher Comment
				if(trim($SubjectTeacherCommentAry[$thisSubjectID])=="" || trim($SubjectTeacherCommentAry[$thisSubjectID])=="\n")
					continue;
				
				# Not in Subject Group
				$SubjectGroups = $this->Get_Student_Studying_Subject_Group($SemID, $StudentID, $thisSubjectID, true);
				if($SubjectGroups == null) continue;
				
				// Subject Name
				$SubjectDisplayName = "";
				if($ReportType!="W") {
					$SubjectDisplayName = $SubjectGroups[0]['ClassTitleEN'];
				}
				$SubjectDisplayName = $SubjectDisplayName==""? $thisClassName." ".$SubjectNameArray[$i] : $SubjectDisplayName;
				
				// Subject Teacher
				$instructor = $this->returnSubjectTeacher($StudentInfoArr[0]["ClassID"], $thisSubjectID, $ReportID, $StudentID, $FullTeacherInfo=1);
				if(count($instructor) > 0) {
					$instructor = Get_Array_By_Key($instructor, "EnglishName");
					$instructor = implode(", ", $instructor);
				}
				else {
					$instructor = $this->EmptySymbol;
				}
				
				// Subject Teacher Comment
				$currentTeacherComment = $SubjectTeacherCommentAry[$thisSubjectID];
				// Handle word break problem for Chinese Comment
				if(isBig5(trim($currentTeacherComment))) {
					$currentTeacherComment = $this->getCurrentComment($currentTeacherComment);
				}
				// replace "‘" and "’" by "'" in Comment
				$currentTeacherComment = str_replace("‘", "'", $currentTeacherComment);
				$currentTeacherComment = str_replace("’", "'", $currentTeacherComment);
				$currentTeacherComment = str_replace("“", "\"", $currentTeacherComment);
				$currentTeacherComment = str_replace("”", "\"", $currentTeacherComment);
				$currentTeacherComment = nl2br($currentTeacherComment);
				
				// Subject Comment Display
				// for Last Display Subject only
				if($thisSubjectID == $LastSubjectID)
				{
					$colspan = $isMSReport? "3" : "2";
					$colwidth = $isMSReport? "67" : "80";
					
					$DetailsTable .= "<tr>";
					$DetailsTable .= "<td style='padding:0px; border:0px' colspan='$colspan'>";
						
						// Subject row
						$DetailsTable .= "<table class='result_table' width='100%' align='center' border='0' cellspacing='0' cellpadding='0' style='margin:0mm'>";
							$DetailsTable .= "<tr>";
							$DetailsTable .= "<td width='$colwidth%' class='bold_cell' style='height:50px; padding:0px; border: 0.05mm solid #BDBDBD;'>";
								$DetailsTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
									$DetailsTable .= "<tr><td class='bold_cell'>$SubjectDisplayName</td></tr>";
									$DetailsTable .= "<tr><td style='font-size:10pt;'>$currentTeacherComment</td></tr>";
								$DetailsTable .= "</table>";
							$DetailsTable .= "</td>";
							$DetailsTable .= "<td width='20%' class='bold_cell' style='border: 0.05mm solid #BDBDBD;'>$instructor</td>";
							// MS Progress Report
							if($isMSReport){
								$DetailsTable .= "<td width='13%' class='bold_cell' style='padding:0px; border: 0.05mm solid #BDBDBD;'>".$MarksDisplayAry[$thisSubjectID]."</td>";
							}
							$DetailsTable .= "</tr>";
						$DetailsTable .= "</table>";
						
						// Signature Display
						$DetailsTable .= "<table class='sign_table' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom' autosize='0'>";
						$DetailsTable .= "<tr>";
							$DetailsTable .= "<td width='7.5%' height='14mm'>&nbsp;</td>";
							$DetailsTable .= "<td width='25.5%'>&nbsp;</td>";
							$DetailsTable .= "<td width='14.5%'>&nbsp;</td>";
							$DetailsTable .= "<td width='45%'>&nbsp;</td>";
							$DetailsTable .= "<td width='7.5%'>&nbsp;</td>";
						$DetailsTable .= "<tr>";
						$DetailsTable .= "<tr>";
							$DetailsTable .= "<td width='7.5%'>&nbsp;</td>";
							$DetailsTable .= "<td width='25.5%'><hr></td>";
							$DetailsTable .= "<td width='14.5%'>&nbsp;</td>";
							$DetailsTable .= "<td width='45%'><hr></td>";
							$DetailsTable .= "<td width='7.5%'>&nbsp;</td>";
						$DetailsTable .= "<tr>";
						$DetailsTable .= "<tr>";
							$DetailsTable .= "<td width='7.5%'>&nbsp;</td>";
							$DetailsTable .= "<td width='25.5%' align='center'>Principal's Signature</td>";
							$DetailsTable .= "<td width='14.5%'>&nbsp;</td>";
							$DetailsTable .= "<td width='45%' align='center'>Date</td>";
							$DetailsTable .= "<td width='7.5%'>&nbsp;</td>";
						$DetailsTable .= "</tr>";
						$DetailsTable .= "</table>";
						
					$DetailsTable .= "</td>";
					$DetailsTable .= "</tr>";
				}
				// Normal Subjects
				else
				{
					// Subject row
					$DetailsTable .= "<tr>";
					$DetailsTable .= "<td class='bold_cell' style='height:50px; padding:0px; $style2'>";
						$DetailsTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
							$DetailsTable .= "<tr><td class='bold_cell'>$SubjectDisplayName</td></tr>";
							$DetailsTable .= "<tr><td style='font-size:10pt;'>$currentTeacherComment</td></tr>";
						$DetailsTable .= "</table>";
					$DetailsTable .= "</td>";
					$DetailsTable .= "<td class='bold_cell'>$instructor</td>";
					// MS Progress Report
					if($isMSReport){
						$DetailsTable .= "<td class='bold_cell' style='padding:0px'>".$MarksDisplayAry[$thisSubjectID]."</td>";
					}
					$DetailsTable .= "</tr>";
				}
			}
			$DetailsTable .= "</table>";
			
			// Signature Display (No Subject Display only)
			if($LastSubjectID == 0)
			{
				$DetailsTable .= "<table class='sign_table' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom' autosize='0'>";
				$DetailsTable .= "<tr>";
					$DetailsTable .= "<td width='7%' height='14mm'>&nbsp;</td>";
					$DetailsTable .= "<td width='37%'>&nbsp;</td>";
					$DetailsTable .= "<td width='12%'>&nbsp;</td>";
					$DetailsTable .= "<td width='37%'>&nbsp;</td>";
					$DetailsTable .= "<td width='7%'>&nbsp;</td>";
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<tr>";
					$DetailsTable .= "<td width='7%'>&nbsp;</td>";
					$DetailsTable .= "<td width='37%'><hr></td>";
					$DetailsTable .= "<td width='12%'>&nbsp;</td>";
					$DetailsTable .= "<td width='37%'><hr></td>";
					$DetailsTable .= "<td width='7%'>&nbsp;</td>";
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<tr>";
					$DetailsTable .= "<td width='7%'>&nbsp;</td>";
					$DetailsTable .= "<td width='37%' align='center'>Principal's Signature</td>";
					$DetailsTable .= "<td width='12%'>&nbsp;</td>";
					$DetailsTable .= "<td width='37%' align='center'>Date</td>";
					$DetailsTable .= "<td width='7%'>&nbsp;</td>";
				$DetailsTable .= "</td>";
				$DetailsTable .= "</tr>";
				$DetailsTable .= "</table>";
			}
		}
		
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID) {
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array(), $commentAry=array(), $attendanceAry=array(), $bilingual=false) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$isProgressReport = !$isMainReport;
 		$ReportColumnAry = $this->returnReportTemplateColumnData($ReportID);
		$SemID = $ReportSetting['Semester'];
        $SemesterNo = $this->Get_Semester_Seq_Number($SemID);
 		$ReportType = $SemID == "F" ? "W" : "T";
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		// [2015-1013-1434-16164] get Report Type
		// ES/MS Report
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
			$isSemesterReport = $ReportType=="W" || $SemesterNo>0 && $SemesterNo%2==0;
		}
		$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
		
		// TermID
		//$firstTermID = $SemesterNo==1? $SemID : -1;
		//$secondTermID = $SemesterNo==2? $SemID : -1;
		// ES/MS Report - Term 2 and 4 need to display attendance records
		$firstTermID = $SemesterNo==2? $SemID : -1;
		$secondTermID = $SemesterNo==4? $SemID : -1;
		// KG Report - all terms need to display attendance records
		if($isKGReport) {
			$firstTermID = $SemesterNo==1? $SemID : $firstTermID;
			$secondTermID = $SemesterNo==3? $SemID : $secondTermID;
		}
		if($ReportType=="W") {
			$firstTermID = $ReportColumnAry[0]["SemesterNum"];
			$secondTermID = $ReportColumnAry[1]["SemesterNum"];
		}

		// [2020-0915-1830-00164]
        $targetTermSeq = 0;
        if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'] && $ReportType == "T") {
            $targetTermSeq = ($SemesterNo == 1 || $SemesterNo == 2) ? 1 : 2;
        }
		
 		# Retrieve Score Settings
		$ScoreSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectScore = $ScoreSetting['SubjectScore']; // 0: Integer , 1: 1 decimal places , 2: 2 decimal places
		
		$titleLang			= $bilingual || $isKGReport? "" : ($isMainReport && $isMSReport? "en" : "b5");
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID, $titleLang);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
		    foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
	    $bothLangSubjectArray = array();
	    $bothLangSubjectArray['en'] = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID, 'en');
	    $bothLangSubjectArray['b5'] = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID, 'b5');

		// for preview			
		$studentInfo = array();
		$studentInfo['Name'] = "XXX";
		$studentInfo['ClassName'] = "XXX";
		$studentInfo['EN_Name'] = "XXX";
		$studentInfo['CH_Name'] = "XXX";
		
		# retrieve Student Info
		if($StudentID)
		{
//			include_once($PATH_WRT_ROOT."includes/libuser.php");
//			$lu = new libuser($StudentID);
//			$studentInfo['Name'] = $lu->EnglishName;
//			$StudentInfo['EN_Name'] = $lu->EnglishName." ".$lu->ChineseName;
//			$studentInfo['ClassName'] = $lu->ClassName;
			$currentStudent = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);
			$studentInfo['Name'] = $currentStudent[0]["StudentNameEn"];
			$studentInfo['EN_Name'] = $currentStudent[0]["StudentNameEn"]." ".$currentStudent[0]["StudentNameCh"];
			$studentInfo['CH_Name'] = $currentStudent[0]["StudentNameCh"];
			$studentInfo['ClassName'] = $currentStudent[0]["ClassName"];
		}
		
 		$x = "";
 		$returnArr = array();

 		// ES Semester Report - Chinese
 		if($isMainReport && $isESReport && !$bilingual && is_array($SubjectArray))
 		{
 			$returnArr = "";
 			
 			// get ReportColumnIDs for Whole Year Report
	 		$termReportAry = array();
 			if($ReportType=="W"){
	 			$relatedTermReports = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
	 			for($reportCount=0; $reportCount<count($relatedTermReports); $reportCount++){
	 				$currentReportID = $relatedTermReports[$reportCount]["ReportID"];
	 				$ReportColumnInfoAry = $this->returnReportTemplateColumnData($currentReportID);
	 				
	 				$termReportAry[$reportCount] = $ReportColumnInfoAry[0]["ReportColumnID"];
	 			}
	 			// get teacher comment from semester 2 report
	 			if(count($relatedTermReports)>0)
	 				$commentAry = $this->returnSubjectTeacherComment($currentReportID,$StudentID);
 			}
			
			// Subject Teachers
			$SubjectTeacherAry = $this->returnChineseTeacherName($ReportID, $StudentID);
 			
 			// Subject Code Mapping
			$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
			//$SearchSyntax = $this->ESChiVersionSubjectComment;
 			
 			// Subject Topics
            // [2020-0915-1830-00164]
            //$SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray));
 			$SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray), '', $targetTermSeq);
 			$SubjectTopicArr = BuildMultiKeyAssoc((array)$SubjectTopicArr, array("SubjectID", "SubjectTopicID"));

 			// Subject Settings
 			$Subject_Settings = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,'',$ReportID);

 			$isFirst = 1;
 			$comp_count = -1;
 			
 			// loop Subject
 			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

				// only display chinese subject & skip if student not in subject group
				$currentSubjectSetting = $Subject_Settings[$SubjectID];
				if(!$isSub){
					if($currentSubjectSetting["LangDisplay"]!="ch")	continue;
					if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID)) continue;
				}
				else {
					$CurrentParentSubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
					if($Subject_Settings[$CurrentParentSubjectID]["LangDisplay"]!="ch") continue;
					if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $CurrentParentSubjectID)) continue;
				}
				
				// if parent subject, find its components subjects
				$isParentSubject = 0;
				$parentSubjectWithTopics = false;
				$CmpSubjectArr = array();
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) {
						$isParentSubject = 1;
						
						for($i=0; $i<count($CmpSubjectArr); $i++){
							$parentSubjectWithTopics = count($SubjectTopicArr[$CmpSubjectArr[$i]["SubjectID"]])>0;
							if($parentSubjectWithTopics==true)	break;
						}
						$comp_count = $parentSubjectWithTopics? count($CmpSubjectArr) : -1;
					} else {
						$comp_count = count($SubjectTopicArr[$SubjectID])>0? 1 : -1;
					}
				}
				
				// Parent Subject, Component Subject with Subject Topics
				if($isParentSubject && $parentSubjectWithTopics)
				{
					// First Display Subject
					if($isFirst)
					{
						// Subject Header
						$returnArr .= "<table class='ES_ParentSubject' width='92%' border='0' cellspacing='0' cellpadding='0'>";
							$returnArr .= "<tr>";
								$returnArr .= "<td width='35%'>学科: $SubjectName</td>";
								$returnArr .= "<td width='65%'>教师姓名: ".$SubjectTeacherAry[$SubjectID]."</td>";
								$returnArr .= "<td width='1'>&nbsp;</td>";
							$returnArr .= "</tr>";
						$returnArr .= "</table>";
						
						$isFirst = 0;
					} 
					else
					{
						$returnArr .= "</div>";
						
						// new page
						$returnArr .= "</div>";
						$returnArr .= "<pagebreak />";
						$returnArr .= "<div id=\"container\" style=\"width:210mm; height:297mm; background-color:white;\">";
			
						// Subject Header
						$returnArr .= "<table class='ES_ParentSubject_s' width='92%' border='0' cellspacing='0' cellpadding='0'>";
							$returnArr .= "<tr>";
								//$returnArr .= "<td>学科: $SubjectName</td>";
								//$returnArr .= "<td>姓名: ".$studentInfo['CH_Name']."</td>";
								//$returnArr .= "<td>班级: ".$studentInfo['ClassName']."</td>";
								$returnArr .= "<td width='35%'>学科: $SubjectName</td>";
								$returnArr .= "<td width='65%'>教师姓名: ".$SubjectTeacherAry[$SubjectID]."</td>";
								$returnArr .= "<td width='1'>&nbsp;</td>";
							$returnArr .= "</tr>";
						$returnArr .= "</table>";
					}
				}
				// Non-Parent Subject
				else if(!$isParentSubject)
				{
					$comp_count--;
					
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						// subject topic score
						if($ReportType=="W"){
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							$SubjectTopicScoreArr = array();
							// ES Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr[(($SemesterNo/2)-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}
						
						// Major Subject
						if(!$isSub)
						{
							// First Display Subject
							if ($isFirst) {
								$returnArr .= "<table class='ES_ParentSubject' width='92%' border='0' cellspacing='0' cellpadding='0'>";
									$returnArr .= "<td width='35%'>学科: $SubjectName</td>";
									$returnArr .= "<td width='65%'>教师姓名: ".$SubjectTeacherAry[$SubjectID]."</td>";
									$returnArr .= "<td width='1'>&nbsp;</td>";
								$returnArr .= "</table>";
								
								$isFirst = 0;
								$SubjectName = "";
							}
							else {
								$returnArr .= "</div>";
								
								// new page
								$returnArr .= "</div>";
								$returnArr .= "<pagebreak />";
								$returnArr .= "<div id=\"container\" style=\"width:210mm; height:297mm; background-color:white;\">";
								
								// Subject Header
								$returnArr .= "<table class='ES_ParentSubject_s' width='92%' border='0' cellspacing='0' cellpadding='0'>";
									$returnArr .= "<tr>";
										$returnArr .= "<td width='35%'>学科: $SubjectName</td>";
										$returnArr .= "<td width='65%'>教师姓名: ".$SubjectTeacherAry[$SubjectID]."</td>";
										$returnArr .= "<td width='1'>&nbsp;</td>";
									$returnArr .= "</tr>";
								$returnArr .= "</table>";
								
								$SubjectName = "";
							}
						}
						
						// Content Table
						$returnArr .= "<table class='ES_CompSubject' width='92%' border='0' cellspacing='0' cellpadding='0'>";
							$returnArr .= "<tr>";
								$returnArr .= "<td class='comp_title comp_bold' width='80%'>$SubjectName</td>";
								$returnArr .= "<td class='comp_title comp_bold' width='10%' align='center'>S1</td>";
								$returnArr .= "<td class='comp_title comp_bold' width='10%' align='center'>S2</td>";
							$returnArr .= "</tr>";
							
							// loop subject topics
							foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
							{
								$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
								$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
								$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
								$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
								
								// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
								$topicNameDisplay = "";
								$topicNameDisplayCh = trim($currentTopic["NameCh"]);
								$topicNameDisplayEn = trim($currentTopic["NameEn"]);
								$topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

								if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
									$topicNameDisplay = "";
								else if(empty($topicNameDisplayEn))
									$topicNameDisplay = $topicNameDisplayCh;
								else if(empty($topicNameDisplayCh))
									$topicNameDisplay = $topicNameDisplayEn;
								else
									$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

								// [2020-0915-1830-00164]
								$firstTermRawStyle = "";
								$secondTermRawStyle = "";
								if ($topicTargetTermSeq == 1) {
                                    $secondTermRawStyle = " style='background-color: grey;' ";
                                    $secondTermRaw = "";
                                }
                                if ($topicTargetTermSeq == 2) {
                                    $firstTermRawStyle = " style='background-color: grey;' ";
                                    $firstTermRaw = "";
                                }

								$returnArr .= "<tr>	";
									$returnArr .= "<td width='80%'>$topicNameDisplay</td>";
									$returnArr .= "<td align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
									$returnArr .= "<td align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
								$returnArr .= "</tr>";
							}
						$returnArr .= "</table>";
					}
					
					// last subject table before new page
					if($comp_count == 0)
					{
						// subject comment
						$commentSubject = $isSub? $this->GET_PARENT_SUBJECT_ID($SubjectID) : $SubjectID;
						$commentDisplay = $StudentID? nl2br($commentAry[$commentSubject]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
						if(isBig5(trim($commentDisplay))){
							$commentDisplay = $this->getCurrentComment($commentDisplay);
						}
						// replace "‘" and "’" by "'"
						$commentDisplay = str_replace("‘", "'", $commentDisplay);
						$commentDisplay = str_replace("’", "'", $commentDisplay);
						$commentDisplay = str_replace("“", "\"", $commentDisplay);
						$commentDisplay = str_replace("”", "\"", $commentDisplay);
						
						// use div to prevent pagebreak between Comment and Signature
						$returnArr .= "<div width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";
						
						// comment box
						$returnArr .= "<table class='ES_Comment' width='92%' border='0' cellspacing='2' cellpadding='2'>";
							$returnArr .= "<tr><td><b>教师评语</b></td></tr>";
							$returnArr .= "<tr><td valign='top' height='100px' style='font-size:10pt;'>$commentDisplay</td></tr>";
						$returnArr .= "</table>";
						
						$currentSubjectCode = $SubjectCodeMappping[$commentSubject];
						if($currentSubjectCode==("ES G".$FormNumber." Chi.") || $currentSubjectCode==("ES G".$FormNumber." CSL"))
						{
							//$signature_td = "语文学科主任签字：______________";
							$signature_td = "&nbsp;　中文校长签字：______________";
						}
						else if($currentSubjectCode==("G".$FormNumber." ChMath") || $currentSubjectCode==("G".$FormNumber." CslMath"))
						{
							//$signature_td = "数学学科主任签字：______________";
							$signature_td = "&nbsp;　中文校长签字：______________";
						}
						else
						{
							$signature_td = "&nbsp;";
						}
						
						// signature
						$returnArr .= "<table class='ES_Sign' width='100%' border='0' cellspacing='2' cellpadding='2' autosize='1'>";
							$returnArr .= "<tr>";
								$returnArr .= "<td width='30%'>任课教师签字：______________</td>";
								$returnArr .= "<td width='8%'>&nbsp; </td>";
								//$returnArr .= "<td width='22%'>日期：______________</td>";
								$returnArr .= "<td width='22%'>".$signature_td."</td>";
							$returnArr .= "</tr>";
						$returnArr .= "</table>";
						
						$comp_count = -1;
					}
				}
			}
						
			// signature
//			$returnArr .= "<table class='ES_Sign' width='100%' border='0' cellspacing='2' cellpadding='2' style='padding-top: 35px;' autosize='1'>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='30%'>语文学科主任签字：______________</td>";
//					$returnArr .= "<td width='8%'>&nbsp; </td>";
//					$returnArr .= "<td width='22%'>日期：______________</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='30%'>&nbsp;</td>";
//					$returnArr .= "<td width='8%'>&nbsp; </td>";
//					$returnArr .= "<td width='22%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='30%'>数学学科主任签字：______________</td>";
//					$returnArr .= "<td width='8%'>&nbsp; </td>";
//					$returnArr .= "<td width='22%'>日期：______________</td>";
//				$returnArr .= "</tr>";
//			$returnArr .= "</table>";
			
			if (!$isFirst)
			{
				$returnArr .= "</div>";
			}
 		}
 		// ES Semester Report - English
 		else if($isMainReport && $isESReport && $bilingual && is_array($SubjectArray))
 		{
 			$returnArr = "";
 			$commentContent = "";
 			
 			// get ReportColumnIDs for Whole Year Report
 			$termReportAry = array();
 			if($ReportType=="W")
 			{
	 			$relatedTermReports = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
	 			for($reportCount=0; $reportCount<count($relatedTermReports); $reportCount++){
	 				$currentReportID = $relatedTermReports[$reportCount]["ReportID"];
	 				$ReportColumnInfoAry = $this->returnReportTemplateColumnData($currentReportID);
	 				
	 				$termReportAry[$reportCount] = $ReportColumnInfoAry[0]["ReportColumnID"];
	 			}
	 			// get teacher comment from semester 2 report
	 			if(count($relatedTermReports)>0)
	 				$commentAry = $this->returnSubjectTeacherComment($currentReportID,$StudentID);
 			}
 			
 			// Subject Topics
            // [2020-0915-1830-00164]
 			//$SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray));
            $SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray), '', $targetTermSeq);
 			$SubjectTopicArr = BuildMultiKeyAssoc((array)$SubjectTopicArr, array("SubjectID", "SubjectTopicID"));
 			
 			// Subject Settings
 			$Subject_Settings = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,'',$ReportID);
 			
 			// Subject Code Mapping
			$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
 			
 			$isFirst = 1;
 			$comp_count = -1;
 			$displaySubjectCount = 0;
 			
 			// loop subjects
 			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$SubjectName = str_replace("<br />", " ", $SubjectName);
				
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// only display english subject & skip if student not in subject group
				$currentSubjectSetting = $Subject_Settings[$SubjectID];
				if(!$isSub) {
					if($currentSubjectSetting["LangDisplay"]=="ch")	continue;
					if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID)) continue;
				}
				else {
					$CurrentParentSubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
					if($Subject_Settings[$CurrentParentSubjectID]["LangDisplay"]=="ch") continue;
					if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $CurrentParentSubjectID)) continue;
				}
				
				// if parent subject, find its components subjects
				$isParentSubject = 0;
				$parentSubjectWithTopics = false;
				$CmpSubjectArr = array();
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) {
						$isParentSubject = 1;
						
						for($i=0; $i<count($CmpSubjectArr); $i++){
							$parentSubjectWithTopics = count($SubjectTopicArr[$CmpSubjectArr[$i]["SubjectID"]])>0;
							if($parentSubjectWithTopics==true)	break;
						}
						$comp_count = $parentSubjectWithTopics? count($CmpSubjectArr) : -1;
					}
					else {
						$comp_count = count($SubjectTopicArr[$SubjectID])>0? 1 : -1;
					}
				}
				
				// Parent Subject, Component Subject with Subject Topics
				if($isParentSubject && $parentSubjectWithTopics)
				{
					// First Display Subject
					if($isFirst)
					{
						// Semester Remarks
						$returnArr .= "<table width='92%' border='0' cellspacing='0' cellpadding='0' style='margin-bottom:2px;'>";
							$returnArr .= "<tr>";
								$returnArr .= "<td width='55%'>&nbsp;</td>";
								$returnArr .= "<td class='Eng_CompSubject_Title' width='32%'><i>Semester学期: </i></td>";
										$returnArr .= "<td class='Eng_CompSubject_Title' width='7%'>1</td>";
										$returnArr .= "<td class='Eng_CompSubject_Title' width='4%'>2</td>";
										$returnArr .= "<td>&nbsp;</td>";
							$returnArr .= "</tr>";
						$returnArr .= "</table>";
						
						// Subject Header
						$returnArr .= "<table class='ES_ParentSubject English_ES' width='92%' border='1' cellspacing='0' cellpadding='0'>";
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_Subject_Title' width='84%'>$SubjectName</td>";
								$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
								$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
							$returnArr .= "</tr>";
						
						$isFirst = 0;
						$displaySubjectCount++;
					} 
					else
					{
//						// Header for Co-Curricular Classes
//						if($displaySubjectCount==4){
//							$returnArr .= "<tr>";
//								$returnArr .= "<td class='comp_title Eng_Subject_ccc'>CO-CURRICULAR CLASSES 其他课程 <br>&nbsp;</td>";
//								$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//								$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//							$returnArr .= "</tr>";
//						}
						
						// Subject Header
						$returnArr .= "<tr>";
							$returnArr .= "<td class='Eng_Subject_Title' width='84%'>$SubjectName</td>";
							$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
							$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
						$returnArr .= "</tr>";
							
						$displaySubjectCount++;
					}
							
					if(!empty($commentAry[$SubjectID]))
					{
						if(!empty($commentContent)){
							$commentContent .= "<br />";
						}
						// replace "‘" and "’" by "'"
						$commentAry[$SubjectID] = str_replace("‘", "'", $commentAry[$SubjectID]);
						$commentAry[$SubjectID] = str_replace("’", "'", $commentAry[$SubjectID]);
						$commentAry[$SubjectID] = str_replace("“", "\"", $commentAry[$SubjectID]);
						$commentAry[$SubjectID] = str_replace("”", "\"", $commentAry[$SubjectID]);
						$commentContent .= substr($SubjectName, 0, strpos($SubjectName, " "))." : ".$commentAry[$SubjectID];
					}
				}
				// Non-Parent Subject
				else if(!$isParentSubject)
				{
					$comp_count--;
					
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr) > 0)
					{
						if($ReportType=="W") {
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							$SubjectTopicScoreArr = array();
							// ES Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr[(($SemesterNo/2)-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}

						// Major Subject
						if(!$isSub)
						{
							if(!empty($commentAry[$SubjectID]))
							{
								if(!empty($commentContent)){
									$commentContent .= "<br />";
								}
								// replace "‘" and "’" by "'"
								$commentAry[$SubjectID] = str_replace("‘", "'", $commentAry[$SubjectID]);
								$commentAry[$SubjectID] = str_replace("’", "'", $commentAry[$SubjectID]);
								$commentAry[$SubjectID] = str_replace("“", "\"", $commentAry[$SubjectID]);
								$commentAry[$SubjectID] = str_replace("”", "\"", $commentAry[$SubjectID]);
								$commentContent .= substr($SubjectName, 0, strpos($SubjectName, " "))." : ".$commentAry[$SubjectID];
							}
							
							// First Display Subject
							if ($isFirst)
							{
								// Semester Remarks
								$returnArr .= "<table width='92%' border='0' cellspacing='0' cellpadding='0' style='margin-bottom:2px;'>";
									$returnArr .= "<tr>";
										$returnArr .= "<td width='55%'>&nbsp;</td>";
										$returnArr .= "<td class='Eng_CompSubject_Title' width='32%'><i>Semester学期: </i></td>";
										$returnArr .= "<td class='Eng_CompSubject_Title' width='7%'>1</td>";
										$returnArr .= "<td class='Eng_CompSubject_Title' width='4%'>2</td>";
										$returnArr .= "<td>&nbsp;</td>";
									$returnArr .= "</tr>";
								$returnArr .= "</table>";
								
								// Subject Header
								$returnArr .= "<table class='ES_ParentSubject English_ES' width='92%' border='1' cellspacing='0' cellpadding='0'>";
									$returnArr .= "<tr>";
										$returnArr .= "<td class='Eng_Subject_Title' width='84%'>$SubjectName</td>";
										$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
										$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
									$returnArr .= "</tr>";
								
								$isFirst = 0;
								$SubjectName = "";
								$displaySubjectCount++;
							}
							else
							{
//								// Header for Co-Curricular Classes
//								if($displaySubjectCount==4){
//									$returnArr .= "<tr>";
//										$returnArr .= "<td class='comp_title Eng_Subject_ccc'>CO-CURRICULAR CLASSES 其他课程 <br>&nbsp;</td>";
//										$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//										$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//									$returnArr .= "</tr>";
//								}				
											
								// Subject Header
								$returnArr .= "<tr>";
									$returnArr .= "<td class='Eng_Subject_Title' width='84%'>$SubjectName</td>";
									$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
									$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
								$returnArr .= "</tr>";
								
								$SubjectName = "";
								$displaySubjectCount++;
							}
						}
						// Component Subject
						else
						{
							// Subject Header
							$returnArr .= "<tr>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'><b>$SubjectName</b></td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
							$returnArr .= "</tr>";
						}
						
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

							if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
				}
			}
			if($isFirst!=1) {
				$returnArr .= "</table>";
			}
			
			// Comment box
			//$commentDisplay = $StudentID? nl2br($commentAry[0]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
			$commentDisplay = $StudentID? nl2br($commentContent) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
			if(isBig5(trim($commentDisplay))) {
				$commentDisplay = $this->getCurrentComment($commentDisplay);
			}
			// replace "‘" and "’" by "'"
			$commentDisplay = str_replace("‘", "'", $commentDisplay);
			$commentDisplay = str_replace("’", "'", $commentDisplay);
			$commentDisplay = str_replace("“", "\"", $commentDisplay);
			$commentDisplay = str_replace("”", "\"", $commentDisplay);
			
			// use div to prevent pagebreak between Comment and Signature
			$returnArr .= "<div width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";		
//			$returnArr .= "<table width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'><tr><td width='100%' style='padding:0'>";
			$returnArr .= "<table class='ES_Comment_Eng' width='100%' border='0' cellspacing='0' cellpadding='0'>";
				$returnArr .= "<tr><td class='Eng_Subject_Title'>COMMENTS 评语: </td></tr>";
				$returnArr .= "<tr><td valign='top' height='100px' style='font-size:10pt;'>$commentDisplay</td></tr>";
			$returnArr .= "</table>";
			
			// Signature
			$returnArr .= "<table class='ES_Eng_Sign' width='100%' border='0' cellspacing='2' cellpadding='2' autosize='1'>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>&nbsp;</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					$returnArr .= "<td width='45%'>&nbsp;</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>__________________________________</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					$returnArr .= "<td width='45%'>__________________________________</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>教师签字</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					//$returnArr .= "<td width='45%'>日期</td>";
					$returnArr .= "<td width='45%'>管理组签字</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>Teacher Signature</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					//$returnArr .= "<td width='45%'>Date</td>";
					$returnArr .= "<td width='45%'>Principal's Signature</td>";
				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>__________________________________</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>__________________________________</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>管理组签字</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>日期</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>Administrative Signature</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>Date</td>";
//				$returnArr .= "</tr>";
			$returnArr .= "</table>";
			$returnArr .= "</div>";
//			$returnArr .= "</td></tr></table>";
 		}
 		// KG Progress Report
 		else if($isMainReport && $isKGReport && !$isSemesterReport && is_array($SubjectArray))
 		{
 			$returnArr = "";
 			
 			// get ReportColumnIDs for Quarter 3 Progress Report
 			$termReportAry = array();
 			if($SemesterNo==3)
 			{
				$termReportAry[1] = $ReportColumnAry[0]["ReportColumnID"];
				
				$RelatedReportList = $this->Get_Report_List($ClassLevelID, 'T');
				for($reportCount=0; $reportCount<count($RelatedReportList); $reportCount++)
				{
					$relatedReportID = $RelatedReportList[$reportCount]['ReportID'];
 					$relatedReportColumnInfoAry = $this->returnReportTemplateColumnData($relatedReportID);
					$relatedReportSemester = $RelatedReportList[$reportCount]['Semester'];
					$relatedReportSemSeq = $this->Get_Semester_Seq_Number($relatedReportSemester);
					
					if($relatedReportSemSeq==1 && !isset($termReportAry[0])){
						$termReportAry[0] = $relatedReportColumnInfoAry[0]["ReportColumnID"];
					}
					else if($relatedReportSemSeq==2 && !isset($termReportAry[2])){
						$termReportAry[2] = $relatedReportColumnInfoAry[0]["ReportColumnID"];
					}
				}
 			}
 			
 			// get Subject Comment
 			$commentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
 			
 			// Subject Topics
            // [2020-0915-1830-00164]
 			//$SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray));
            $SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray), '', $targetTermSeq);
 			$SubjectTopicArr = BuildMultiKeyAssoc((array)$SubjectTopicArr, array("SubjectID", "SubjectTopicID"));
 			
 			// Subject Settings
 			$Subject_Settings = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,'',$ReportID);
 			
 			// Subject Code Mapping
			$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
			$SubjectCodeIDMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING(1);
						
			// Subject Ordering
			$SubjectIDArr = array();
 			$SubjectIDArr[$SubjectCodeIDMappping["KG Culture"]] = "KG Culture";
 			$SubjectIDArr[$SubjectCodeIDMappping["KG Skills"]] = "KG Skills";
 			$SubjectIDArr[$SubjectCodeIDMappping["KG Writing"]] = "KG Writing";
 			
 			$isFirst = 1;
 			$comp_count = -1;
 			$displaySubjectCount = 0;
 			$MainSubjectID = "";
 			
 			// loop subjects
 			//foreach($SubjectArray as $SubjectID => $SubjectName)
 			foreach($SubjectIDArr as $SubjectID => $SubjectCode)
			{
				$SubjectName = $SubjectArray[$SubjectID];
				$SubjectName = str_replace("<br />", " ", $SubjectName);
				$currentSubjectSetting = $Subject_Settings[$SubjectID];
				
				// skip if subject is not Work habits and virtues
				//if($SubjectCodeMappping[$SubjectID] != "KG Culture") continue;
				if($SubjectID=="" || $SubjectName=="") continue;
				
				if($SubjectCode=="KG Skills"){
					$SubjectName .= "<br><span style='font-size:8pt'><br>*Your assistance in helping your child master these important skills supports the development of student independence
									and self-confidence.在孩子学习掌握这些重要技能时，您的帮助将会使孩子进一步增加独立性和自信心。</span><br>";
				}
				else if($SubjectCode=="KG Writing"){
					$SubjectName .= "<br>(refer to the table below, 请参阅下面的说明)";
				}
				
				$td_style = $SubjectCode=="KG Writing"? "" : "KG_ParentSubject";
				
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// if parent subject, find its components subjects
				$isParentSubject = 0;
				$parentSubjectWithTopics = false;
				$CmpSubjectArr = array();
				if (!$isSub)
				{
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) {
						$isParentSubject = 1;
						
						for($i=0; $i<count($CmpSubjectArr); $i++){
							$parentSubjectWithTopics = count($SubjectTopicArr[$CmpSubjectArr[$i]["SubjectID"]])>0;
							if($parentSubjectWithTopics==true)	break;
						}
						$comp_count = $parentSubjectWithTopics? count($CmpSubjectArr) : -1;
					}
					else {
						$comp_count = count($SubjectTopicArr[$SubjectID])>0? 1 : -1;
					}
				}
				
				// Parent Subject has Component Subjects with Subject Topics
				if($isParentSubject && $parentSubjectWithTopics)
				{
					// display Interventions for previous main subject
					if(!$isFirst && $SubjectID!=$MainSubjectID){
						$returnArr .= "</table>";
						
						// Interventions
//						$commentDisplay = $StudentID? nl2br($commentAry[$MainSubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
//						$returnArr .= "<table class='ES_Comment_Eng kg_comment_style' width='92%' border='0' cellspacing='0' cellpadding='0'>";
//							$returnArr .= "<tr><td style='font-size:13px; padding-bottom:6px;'><b>Interventions:</b></td></tr>";
//							$returnArr .= "<tr><td valign='top' height='60px' style='font-size:10pt;'>$commentDisplay</td></tr>";
//						$returnArr .= "</table>";
					}
					$MainSubjectID = $SubjectID;
					
					// Semester Remarks
					if($isFirst)
					{
						$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding-bottom:12px; font-size:10pt;'>";
							$returnArr .= "<tr>";
								$returnArr .= "<td width='60%'>&nbsp;</td>";
								$returnArr .= "<td width='16%' align='right'>Quarter 学季: </td>";
								$returnArr .= "<td width='8%' align='center'>1</td>";
								$returnArr .= "<td width='8%' align='center'>2</td>";
								$returnArr .= "<td width='8%' align='center'>3</td>";
							$returnArr .= "</tr>";
						$returnArr .= "</table>";
					}
					
					$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0'>";
					//$returnArr .= "<table class='ES_ParentSubject English_ES' width='99.6%' border='1' cellspacing='0' cellpadding='0' align='center'>";
					
//					// Header for Co-Curricular Classes
//					if($displaySubjectCount==4){
//						$returnArr .= "<tr>";
//							$returnArr .= "<td class='comp_title Eng_Subject_ccc'>CO-CURRICULAR CLASSES 其他课程 <br>&nbsp;</td>";
//							$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//							$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//						$returnArr .= "</tr>";
//					}
					// Subject Header
					$returnArr .= "<tr>";
						$returnArr .= "<td class='Eng_Subject_Title $td_style' width='76%'>$SubjectName</td>";
						$returnArr .= "<td class='Eng_Subject_Title $td_style' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title $td_style' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title $td_style' width='8%'>&nbsp;</td>";
					$returnArr .= "</tr>";
					
					$isFirst = 0;
					$displaySubjectCount++;
				}
				// Non-Parent Subject
				else if(!$isParentSubject)
				{
					$comp_count--;
					
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($SemesterNo==3) {
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							$firstSemReportColumnID = $termReportAry[2];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[2] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstSemReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 1 and 3
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==1? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}

						// Major Subject
						if(!$isSub)
						{
							// display Interventions for previous main subject
							if(!$isFirst && $SubjectID!=$MainSubjectID)
							{
								$returnArr .= "</table>";
								
								// Interventions
//								$commentDisplay = $StudentID? nl2br($commentAry[$MainSubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
//								$returnArr .= "<table class='ES_Comment_Eng kg_comment_style' width='92%' border='0' cellspacing='0' cellpadding='0'>";
//									$returnArr .= "<tr><td style='font-size:13px; padding-bottom:6px;'><b>Interventions:</b></td></tr>";
//									$returnArr .= "<tr><td valign='top' height='60px' style='font-size:10pt;'>$commentDisplay</td></tr>";
//								$returnArr .= "</table>";
							}
							$MainSubjectID = $SubjectID;
							
							// Semester Remarks
							if($isFirst)
							{
								$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding-bottom:12px; font-size:10pt;'>";
									$returnArr .= "<tr>";
										$returnArr .= "<td width='60%'>&nbsp;</td>";
										$returnArr .= "<td width='16%' align='right'>Quarter 学季: </td>";
										$returnArr .= "<td width='8%' align='center'>1</td>";
										$returnArr .= "<td width='8%' align='center'>2</td>";
										$returnArr .= "<td width='8%' align='center'>3</td>";
									$returnArr .= "</tr>";
								$returnArr .= "</table>";
							}
							
							$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0' align='center'>";
							
//							// Header for Co-Curricular Classes
//							if($displaySubjectCount==4){
//								$returnArr .= "<tr>";
//									$returnArr .= "<td class='comp_title Eng_Subject_ccc'>CO-CURRICULAR CLASSES 其他课程 <br>&nbsp;</td>";
//									$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//									$returnArr .= "<td class='comp_title Eng_Subject_ccc'>&nbsp;</td>";
//								$returnArr .= "</tr>";
//							}
							// Subject Header
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_Subject_Title $td_style' width='76%'>$SubjectName</td>";
								$returnArr .= "<td class='Eng_Subject_Title $td_style' width='8%'>&nbsp;</td>";
								$returnArr .= "<td class='Eng_Subject_Title $td_style' width='8%'>&nbsp;</td>";
								$returnArr .= "<td class='Eng_Subject_Title $td_style' width='8%'>&nbsp;</td>";
							$returnArr .= "</tr>";
											
							$isFirst = 0;
							$SubjectName = "";
							$displaySubjectCount++;
						}
						// Component Subject
						else
						{
							// Subject Header
							$returnArr .= "<tr>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'><b>$SubjectName</b></td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
							$returnArr .= "</tr>";
						}
						
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstSemRaw = $SubjectTopicScoreArr[2][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							$firstSemRaw = $StudentID? ($firstSemRaw? $firstSemRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

                            if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstTermRaw = "";
                                $firstSemRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstSemRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}
					}
				}
			}
			if($isFirst!=1) {
				$returnArr .= "</table>";
			}
					
			// Developmental Writing Level Remarks
			$returnArr .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='left' style='padding:0px; padding-top:2px; padding-bottom:2px; margin-top:5mm'>\n";
				$returnArr .= "<tr><td>";
					$returnArr .= "<table class='kg_writing_describe' width='100%' border='1' cellpadding='0' cellspacing='0' align='center'>\n";
						$returnArr .= "<tr class='gray_header'><td align='center' style='padding-bottom:5px' colspan='7'>*   Developmental Stages of Writing 写作阶段说明</td></tr>";
						$returnArr .= "<tr class='gray_header'><td width='13.5%' align='center'>1</td>
											<td width='12.5%' align='center'>2</td>
											<td width='16%' align='center'>3</td>
											<td width='15%' align='center'>4</td>
											<td width='13%' align='center'>5</td>
											<td width='15.7%' align='center'>6</td>
											<td width='14.3%' align='center'>7</td>
										</tr>";
						$returnArr .= "<tr><td align='center'>Pictures<br>图画<br><img width='45px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_pics.png'></td>
											<td align='center'>Scribbles<br>潦草涂写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_scribbles.png'></td>
											<td align='center'>Random<br>Letters<br>潦草涂写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_randword.png'></td>
											<td align='center'>Letters<br>Represent<br>Word<br>字母代表单词<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_letters.png'></td>
											<td align='center'>Beginning<br>Sounds<br>Represent<br>Words<br>首字发音<br>代表单词<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_pronoun.png'></td>
											<td align='center'>Kid<br>Spelling<br>儿童式<br>简单拼写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_spelling.png'></td>
											<td align='center'>Kid Spelling<br>with standard<br>Spelling<br>儿童式简单拼写<br>加标准拼写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_spelling2.png'></td>
										</tr>";
					$returnArr .= "</table>";
				$returnArr .= "</td></tr>";
			$returnArr .= "</table>";
			
			// Interventions
			// as hide Teacher Comment
			//$commentDisplay = $StudentID? nl2br($commentAry[$MainSubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
//			$commentDisplay = $StudentID? nl2br($commentAry[0]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
//			$returnArr .= "<table class='ES_Comment_Eng kg_comment_style' width='92%' border='0' cellspacing='0' cellpadding='0'>";
//				$returnArr .= "<tr><td style='font-size:13px; padding-bottom:6px;'><b>Interventions:</b></td></tr>";
//				$returnArr .= "<tr><td valign='top' height='60px' style='font-size:10pt;'>$commentDisplay</td></tr>";
//			$returnArr .= "</table>";
			
			// Signature
			$returnArr .= "<table class='ES_Eng_Sign' width='100%' border='0' cellspacing='2' cellpadding='2' autosize='1'>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>&nbsp;</td>";
					$returnArr .= "<td width='10%'>&nbsp;</td>";
					$returnArr .= "<td width='45%'>&nbsp;</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>__________________________________</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					$returnArr .= "<td width='45%'>__________________________________</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>教师签字</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					//$returnArr .= "<td width='45%'>日期</td>";
					$returnArr .= "<td width='45%'>校长签字</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>Teachers' Signature</td>";
					$returnArr .= "<td width='10%'>&nbsp;</td>";
					//$returnArr .= "<td width='45%'>Date</td>";
					$returnArr .= "<td width='45%'>Principal's Signature</td>";
				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//					$returnArr .= "<td width='10%'>&nbsp;</td>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//					$returnArr .= "<td width='10%'>&nbsp;</td>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>__________________________________</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>__________________________________</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>管理组签字</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>日期</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>Administrative Signature</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>Date</td>";
//				$returnArr .= "</tr>";
			$returnArr .= "</table>";
 		}
 		// KG Semester Report
 		else if($isMainReport && $isKGReport && $isSemesterReport && is_array($SubjectArray))
 		{
 			$returnArr = "";
 			
 			// get ReportColumnIDs and Teacher Comment for Whole Year Report
 			$termReportAry = array();
 			if($ReportType=="W")
 			{
	 			$relatedTermReports = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
	 			for($reportCount=0; $reportCount<count($relatedTermReports); $reportCount++){
	 				$currentReportID = $relatedTermReports[$reportCount]["ReportID"];
	 				$ReportColumnInfoAry = $this->returnReportTemplateColumnData($currentReportID);
	 				
	 				$termReportAry[$reportCount] = $ReportColumnInfoAry[0]["ReportColumnID"];
	 			}
	 			
	 			// get teacher comment from semester 2 report
	 			if(count($relatedTermReports)>0)
	 				$commentAry = $this->returnSubjectTeacherComment($currentReportID,$StudentID);
 			}
 			// get Subject Comment
 			else
 			{
 				$commentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
 			}
 			
 			// get ReportColumnIDs for Progress Report	
 			$progressReportAry = array();	
			$RelatedReportList = $this->Get_Report_List($ClassLevelID, 'T');
			for($reportCount=0; $reportCount<count($RelatedReportList); $reportCount++)
			{
				$relatedReportID = $RelatedReportList[$reportCount]['ReportID'];
				$relatedReportColumnInfoAry = $this->returnReportTemplateColumnData($relatedReportID);
				$relatedReportSemester = $RelatedReportList[$reportCount]['Semester'];
				$relatedReportSemSeq = $this->Get_Semester_Seq_Number($relatedReportSemester);
				
				if($SemesterNo!=4 && $relatedReportSemSeq==1 && !isset($progressReportAry[0])){
					$progressReportAry[0] = $relatedReportColumnInfoAry[0]["ReportColumnID"];
				}
				else if($SemesterNo!=2 && $relatedReportSemSeq==3 && !isset($progressReportAry[1])){
					$progressReportAry[1] = $relatedReportColumnInfoAry[0]["ReportColumnID"];
				}
			}
 			
 			// Subject Topics
            // [2020-0915-1830-00164]
 			//$SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray));
            $SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray), '', $targetTermSeq);
 			$SubjectTopicArr = BuildMultiKeyAssoc((array)$SubjectTopicArr, array("SubjectID", "SubjectTopicID"));
 			
 			// Subject Settings
 			$Subject_Settings = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,'',$ReportID);
 			
 			// Subject Code Mapping
			$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
			$SubjectCodeIDMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING(1);
						
			# ENGLISH LITERACY
			// get Component Subjects of ENGLISH LITERACY
 			$EngSubjectID = $SubjectCodeIDMappping["KG English"];
 			$EngComponentIDs = Get_Array_By_Key($this->GET_COMPONENT_SUBJECT(array($EngSubjectID), $ClassLevelID, "", "Desc", $ReportID), "SubjectID");
 			$EngComponentIDs = (array)$EngComponentIDs;
 			array_unshift($EngComponentIDs, $EngSubjectID);
 			
 			// loop subjects related to ENGLISH LITERACY
 			for($engCount=0; $engCount<count($EngComponentIDs); $engCount++)
			{
				$SubjectID = $EngComponentIDs[$engCount];
				$SubjectName = $SubjectArray[$SubjectID];
				if($SubjectID=="" || $SubjectName=="")
					continue;
				
				$SubjectName = str_replace("<br />", " ", $SubjectName);
				$currentSubjectSetting = $Subject_Settings[$SubjectID];
				
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// Main Subject
				if(!$isSub)
				{
					$MainSubjectID = $SubjectID;
					
					// Semester Header
					$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding-top:30px; padding-bottom:12px; font-size:10pt;'>";
						$returnArr .= "<tr>";
							$returnArr .= "<td width='65%'>&nbsp;</td>";
							$returnArr .= "<td width='19%'>Semester学期 : </td>";
							$returnArr .= "<td width='8%' align='center'>1</td>";
							$returnArr .= "<td width='8%' align='center'>2</td>";
						$returnArr .= "</tr>";
					$returnArr .= "</table>";
					
					$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0'>";
					
					// Subject Header
					$returnArr .= "<tr>";
						$returnArr .= "<td class='Eng_Subject_Title' width='84%'>$SubjectName</td>";
						$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title' width='8%'>&nbsp;</td>";
					$returnArr .= "</tr>";
					
					// Subject Topics
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($ReportType=="W"){
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}

						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

							if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
				}
				// Component Subject
				else if($isSub)
				{
					// Subject Topics			
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($ReportType=="W"){
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}

						// Component Subject
						// Subject Header
						$returnArr .= "<tr>";
							$returnArr .= "<td class='comp_title Eng_CompSubject_Title'><b>$SubjectName</b></td>";
							$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
							$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
						$returnArr .= "</tr>";
						
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

							if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
				}
			}
			if($returnArr!="") {
				$returnArr .= "</table>";	
			}
			
			# Common Subject
 			$isFirst = 1;
 			$comp_count = -1;
 			$displaySubjectCount = 0;
 			$MainSubjectID = "";
 			
 			// loop subjects
 			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$SubjectName = str_replace("<br />", " ", $SubjectName);
				$currentSubjectSetting = $Subject_Settings[$SubjectID];
				
				// skip if subject related to ENGLISH LITERACY
				if(is_array($EngComponentIDs) && in_array($SubjectID, $EngComponentIDs)) continue;
				
				// skip if subject is Developmental Writing Level
				if($SubjectCodeMappping[$SubjectID] == "KG Writing") continue;
				
				// skip if subject is Kindergarten Skills
				if($SubjectCodeMappping[$SubjectID] == "KG Skills") continue;
				
				// skip if subject is Work habits and Virtues
				if($SubjectCodeMappping[$SubjectID] == "KG Culture") continue;
				
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// skip if student not in subject group
				if (!$isSub) {
					if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID)) continue;
				}
				else{
					$CurrentParentSubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
					if($StudentID && !$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $CurrentParentSubjectID)) continue;
				}
				
				// if parent subject, find its components subjects
				$isParentSubject = 0;
				$parentSubjectWithTopics = false;
				$CmpSubjectArr = array();
				if (!$isSub)
				{
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) {
						$isParentSubject = 1;
						
						for($i=0; $i<count($CmpSubjectArr); $i++){
							$parentSubjectWithTopics = count($SubjectTopicArr[$CmpSubjectArr[$i]["SubjectID"]])>0;
							if($parentSubjectWithTopics==true)	break;
						}
						$comp_count = $parentSubjectWithTopics? count($CmpSubjectArr) : -1;
					} else {
						$comp_count = count($SubjectTopicArr[$SubjectID])>0? 1 : -1;
					}
				}
				
				// Parent Subject has Component Subjects with Subject Topics
				if($isParentSubject && $parentSubjectWithTopics)
				{
					// for previous main subject
					if(!$isFirst && $SubjectID!=$MainSubjectID){
						$returnArr .= "</table>";
					}
					$MainSubjectID = $SubjectID;
					
					$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0' style='overflow: wrap'>";
					
					// Subject Header
//					if($isFirst){
//						$returnArr .= "<tr>";
//							$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='84%'>$SubjectName</td>";
//							//$returnArr .= "<td class='Eng_CompSubject_Content' width='9%' align='center' valign='top'>S1</td>";
//							//$returnArr .= "<td class='Eng_CompSubject_Content' width='9%' align='center' valign='top'>S2</td>";
//							$returnArr .= "<td class='Eng_CompSubject_Content' width='8%' align='center' valign='top'>&nbsp;</td>";
//							$returnArr .= "<td class='Eng_CompSubject_Content' width='8%' align='center' valign='top'>&nbsp;</td>";
//						$returnArr .= "</tr>";
//					}
//					else{
					$returnArr .= "<tr>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='84%'>$SubjectName</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
					$returnArr .= "</tr>";
//					}
					
					$isFirst = 0;
					$displaySubjectCount++;
				}
				// Non-Parent Subject
				else if(!$isParentSubject)
				{
					$comp_count--;
					
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($ReportType=="W"){
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}

						// Major Subject
						if(!$isSub)
						{
							// for previous main subject
							if(!$isFirst && $SubjectID!=$MainSubjectID){
								$returnArr .= "</table>";
							}
							$MainSubjectID = $SubjectID;
							
							$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0' style='overflow: wrap'>";
					
							// Subject Header
//							if($isFirst){
//								$returnArr .= "<tr>";
//									$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='84%'>$SubjectName</td>";
//									//$returnArr .= "<td class='Eng_CompSubject_Content' width='8%' align='center' valign='top'>S1</td>";
//									//$returnArr .= "<td class='Eng_CompSubject_Content' width='8%' align='center' valign='top'>S2</td>";
//									$returnArr .= "<td class='Eng_CompSubject_Content' width='8%' align='center' valign='top'>&nbsp;</td>";
//									$returnArr .= "<td class='Eng_CompSubject_Content' width='8%' align='center' valign='top'>&nbsp;</td>";
//								$returnArr .= "</tr>";
//							}
//							else{
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='84%'>$SubjectName</td>";
								$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
								$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
							$returnArr .= "</tr>";
//							}
											
							$isFirst = 0;
							$SubjectName = "";
							$displaySubjectCount++;
						}
						// Component Subject
						else
						{
							// Subject Header
							$returnArr .= "<tr>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'><b>$SubjectName</b></td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
								$returnArr .= "<td class='comp_title Eng_CompSubject_Title'>&nbsp;</td>";
							$returnArr .= "</tr>";
						}
						
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

							if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
				}
			}
			if($isFirst!=1){
				$returnArr .= "</table>";
			}
			
			# KG Subject Scale
 			$CulSubjectID = $SubjectCodeIDMappping["KG Culture"];
 			$WritingSubjectID = $SubjectCodeIDMappping["KG Writing"];
			$KSkillID = $SubjectCodeIDMappping["KG Skills"];
			if($CulSubjectID!="" || $WritingSubjectID!="" || $KSkillID!="")
			{
				// use div to prevent pagebreak between Subject Scale Remarks and Result Scores
				$returnArr .= "<div width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";
				
				// Report Remarks
				$returnArr .= "<table class='header_describe' width='100%' border='0' cellpadding='0' cellspacing='0' align='left' style='padding: 2px; margin-top:5mm'>\n";
						$returnArr .= "<tr><td>";
							$returnArr .= "<table class='engheader_describe' width='100%' border='0' cellpadding='1' cellspacing='2' align='center'>\n";
								$returnArr .= "<tr><td colspan='5' align='center' style='padding-bottom:5px'>The following scale is used to describe student performance in \"WORK HABITS AND VIRTUES\" and \"KINDERGARTEN SKILLS\"</td></tr>";
								$returnArr .= "<tr><td colspan='5' align='center' style='padding-bottom:5px'>以下标准用于测评学生的\"学习习惯和品德\"以及\"幼儿园技能\"</td></tr>";
								//$returnArr .= "<tr><td align='center'>O= Outstanding</td><td align='center'>S= Satisfactory</td><td align='center'>I= Inconsistent</td><td align='center'>NI=  Needs Improvement</td></tr>";
								$returnArr .= "<tr><td align='center'>O= Outstanding</td><td align='center'>S= Satisfactory</td><td align='center'>I= Inconsistent</td><td align='center'>NI=  Needs Improvement</td><td align='center'>NA= Not Assessed</td></tr>";
							$returnArr .= "</table>";
						$returnArr .= "</td></tr>";
				$returnArr .= "</table>";
					
				// Semester Header
				$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding-top:30px; padding-bottom:12px; font-size:10pt;'>";
					$returnArr .= "<tr>";
						$returnArr .= "<td width='52%'>&nbsp;</td>";
						$returnArr .= "<td width='16%' align='right'>Quarter 学季: </td>";
						$returnArr .= "<td width='8%' align='center'>1</td>";
						$returnArr .= "<td width='8%' align='center'>2</td>";
						$returnArr .= "<td width='8%' align='center'>3</td>";
						$returnArr .= "<td width='8%' align='center'>4</td>";
					$returnArr .= "</tr>";
				$returnArr .= "</table>";
				
				# WORK HABITS AND VIRTUES
				$isFirstTable = true;
	 			if($CulSubjectID!="")
				{
					$SubjectID = $CulSubjectID;
					$SubjectName = $SubjectArray[$SubjectID];
					if($SubjectName=="")
						continue;
					
					$SubjectName = str_replace("<br />", " ", $SubjectName);
					$currentSubjectSetting = $Subject_Settings[$SubjectID];
					
					$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0'>";
					
					// Subject Header
					$returnArr .= "<tr>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='68%'>$SubjectName</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
					$returnArr .= "</tr>";
					
					// Subject Topics
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($ReportType=="W") {
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							
							$firstProgressReportColumnID = $progressReportAry[0];
							$secondProgressReportColumnID = $progressReportAry[1];
							
							$SubjectTopicProgressScoreArr = array();
							$SubjectTopicProgressScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicProgressScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							
							$ProgressReportColumnID = $progressReportAry[($SemesterNo==2? 0 : 1)];
							
							$SubjectTopicProgressScoreArr = array();
							$SubjectTopicProgressScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}
	
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							$firstProgressRaw = $SubjectTopicProgressScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondProgressRaw = $SubjectTopicProgressScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstProgressRaw = $StudentID? ($firstProgressRaw? $firstProgressRaw : "&nbsp;") : "S";
							$secondProgressRaw = $StudentID? ($secondProgressRaw? $secondProgressRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

							if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondProgressRaw = "";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstProgressRaw = "";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstProgressRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondProgressRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
					$returnArr .= "</table>";
					
					if($isFirstTable) {
						$returnArr .= "</div>";
						$isFirstTable = false;
					}
				}
				
				# Kindergarten Skills
	 			if($KSkillID!="")
				{
					$SubjectID = $KSkillID;
					$SubjectName = $SubjectArray[$SubjectID];
					if($SubjectName=="")
						continue;
					
					$SubjectName = str_replace("<br />", " ", $SubjectName);
					$currentSubjectSetting = $Subject_Settings[$SubjectID];
						
					$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0'>";
					
					// Subject Header
					$returnArr .= "<tr>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='68%'>
											$SubjectName <i><br><br>
											<span style='font-size:8pt'>*Your assistance in helping your child master these important skills supports the development of student independence and
											self-confidence.在孩子学习掌握这些重要技能时，您的帮助将会使孩子进一步增加独立性和自信心。</i></span>
										</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
					$returnArr .= "</tr>";
					
					// Subject Topics
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($ReportType=="W"){
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							
							$firstProgressReportColumnID = $progressReportAry[0];
							$secondProgressReportColumnID = $progressReportAry[1];
							
							$SubjectTopicProgressScoreArr = array();
							$SubjectTopicProgressScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicProgressScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							
							$ProgressReportColumnID = $progressReportAry[($SemesterNo==2? 0 : 1)];
							
							$SubjectTopicProgressScoreArr = array();
							$SubjectTopicProgressScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}
	
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							$firstProgressRaw = $SubjectTopicProgressScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondProgressRaw = $SubjectTopicProgressScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstProgressRaw = $StudentID? ($firstProgressRaw? $firstProgressRaw : "&nbsp;") : "S";
							$secondProgressRaw = $StudentID? ($secondProgressRaw? $secondProgressRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

                            if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondProgressRaw = "";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstProgressRaw = "";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstProgressRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondProgressRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
					$returnArr .= "</table>";
					
					if($isFirstTable) {
						$returnArr .= "</div>";
						$isFirstTable = false;
					}
				}
				
				# Developmental Writing Level
	 			if($WritingSubjectID!="")
				{
					$SubjectID = $WritingSubjectID;
					$SubjectName = $SubjectArray[$SubjectID];
					if($SubjectName=="")
						continue;
					
					$SubjectName = str_replace("<br />", " ", $SubjectName);
					$currentSubjectSetting = $Subject_Settings[$SubjectID];
					
					$returnArr .= "<table class='ES_ParentSubject English_ES' width='100%' border='1' cellspacing='0' cellpadding='0' style='margin-bottom:9mm'>";
					
					// Subject Header
					$returnArr .= "<tr>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='68%'>$SubjectName</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
						$returnArr .= "<td class='Eng_Subject_Title KG_ParentSubject' width='8%'>&nbsp;</td>";
					$returnArr .= "</tr>";
					
					// Subject Topics	
					$currentSubjectTopicArr = $SubjectTopicArr[$SubjectID];
					if(count($currentSubjectTopicArr)>0)
					{
						if($ReportType=="W"){
							$firstReportColumnID = $termReportAry[0];
							$secondReportColumnID = $termReportAry[1];
							
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							
							$firstProgressReportColumnID = $progressReportAry[0];
							$secondProgressReportColumnID = $progressReportAry[1];
							
							$SubjectTopicProgressScoreArr = array();
							$SubjectTopicProgressScoreArr[0] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $firstProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							$SubjectTopicProgressScoreArr[1] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $secondProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						} 
						else {
							$ReportColumnID = $ReportColumnAry[0]["ReportColumnID"];
							
							// KG Semester Report only for Term 2 and 4
							//$SubjectTopicScoreArr[($SemesterNo-1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting);
							$SubjectTopicScoreArr = array();
							$SubjectTopicScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
							
							$ProgressReportColumnID = $progressReportAry[($SemesterNo==2? 0 : 1)];
							
							$SubjectTopicProgressScoreArr = array();
							$SubjectTopicProgressScoreArr[($SemesterNo==2? 0 : 1)] = $this->subjectTopicObj->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $currentSubjectTopicArr, $ProgressReportColumnID, 0, '', 0, 1, $currentSubjectSetting, true);
						}
	
						// loop Subject Topics
						foreach((array)$currentSubjectTopicArr as $currentTopicID => $currentTopic)
						{
							$firstTermRaw = $SubjectTopicScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondTermRaw = $SubjectTopicScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstTermRaw = $StudentID? ($firstTermRaw? $firstTermRaw : "&nbsp;") : "S";
							$secondTermRaw = $StudentID? ($secondTermRaw? $secondTermRaw : "&nbsp;") : "S";
							
							$firstProgressRaw = $SubjectTopicProgressScoreArr[0][$currentTopicID]["DisplayContent"];
							$secondProgressRaw = $SubjectTopicProgressScoreArr[1][$currentTopicID]["DisplayContent"];
							$firstProgressRaw = $StudentID? ($firstProgressRaw? $firstProgressRaw : "&nbsp;") : "S";
							$secondProgressRaw = $StudentID? ($secondProgressRaw? $secondProgressRaw : "&nbsp;") : "S";
							
							// [2017-0424-1653-41073] Use "<span>Topic [English]</span>　<span>Topic [Chinese]</span>" to prevent incorrect line break in td 
							$topicNameDisplay = "";
							$topicNameDisplayCh = trim($currentTopic["NameCh"]);
							$topicNameDisplayEn = trim($currentTopic["NameEn"]);
                            $topicTargetTermSeq = trim($currentTopic["TargetTermSeq"]);

							if(empty($topicNameDisplayCh) && empty($topicNameDisplayEn))
								$topicNameDisplay = "";
							else if(empty($topicNameDisplayEn))
								$topicNameDisplay = $topicNameDisplayCh;
							else if(empty($topicNameDisplayCh))
								$topicNameDisplay = $topicNameDisplayEn;
							else
								$topicNameDisplay = "<span>".$topicNameDisplayEn."</span><span>　".$topicNameDisplayCh."</span>";

							// [2020-0915-1830-00164]
                            $firstTermRawStyle = "";
                            $secondTermRawStyle = "";
                            if ($topicTargetTermSeq == 1) {
                                $secondTermRawStyle = " style='background-color: grey;' ";
                                $secondProgressRaw = "";
                                $secondTermRaw = "";
                            }
                            if ($topicTargetTermSeq == 2) {
                                $firstTermRawStyle = " style='background-color: grey;' ";
                                $firstProgressRaw = "";
                                $firstTermRaw = "";
                            }
							
							$returnArr .= "<tr>";
								$returnArr .= "<td class='Eng_CompSubject_Content'>$topicNameDisplay</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstProgressRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$firstTermRawStyle.">$firstTermRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondProgressRaw</td>";
								$returnArr .= "<td class='Eng_CompSubject_Content' align='center' ".$secondTermRawStyle.">$secondTermRaw</td>";
							$returnArr .= "</tr>";
						}	
					}
					$returnArr .= "</table>";
					
					if($isFirstTable) {
						$returnArr .= "</div>";
						$isFirstTable = false;
					}
				}
					
				// Developmental Writing Level Remarks
				$returnArr .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='left' style='padding:0px; padding-top:2px; padding-bottom:2px; margin-top:5mm'>\n";
					$returnArr .= "<tr><td width='100%'>";
						$returnArr .= "<table class='engheader_describe' width='100%' border='0' cellpadding='1' cellspacing='2' align='center' style='border: 1px solid black'>\n";
							$returnArr .= "<tr><td width='100%' align='center' style='padding-bottom:5px'>Developmental Stages of Writing 写作提高的不同阶段</td></tr>";
						$returnArr .= "</table>";
					$returnArr .= "</td></tr>";
					$returnArr .= "<tr><td width='100%'>&nbsp;</td></tr>";
					$returnArr .= "<tr><td width='100%'>";
						$returnArr .= "<table class='kg_writing_describe' width='100%' border='1' cellpadding='0' cellspacing='0' align='center'>\n";
							$returnArr .= "<tr class='gray_header'><td width='13.5%' align='center'>1</td>
												<td width='12.5%' align='center'>2</td>
												<td width='16%' align='center'>3</td>
												<td width='15%' align='center'>4</td>
												<td width='13%' align='center'>5</td>
												<td width='15.7%' align='center'>6</td>
												<td width='14.3%' align='center'>7</td>
											</tr>";
							$returnArr .= "<tr><td align='center'>Pictures<br>图画<br><img width='45px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_pics.png'></td>
												<td align='center'>Scribbles<br>潦草涂写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_scribbles.png'></td>
												<td align='center'>Random<br>Letters<br>潦草涂写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_randword.png'></td>
												<td align='center'>Letters<br>Represent<br>Word<br>字母代表单词<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_letters.png'></td>
												<td align='center'>Beginning<br>Sounds<br>Represent<br>Words<br>首字发音<br>代表单词<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_pronoun.png'></td>
												<td align='center'>Kid<br>Spelling<br>儿童式<br>简单拼写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_spelling.png'></td>
												<td align='center'>Kid Spelling<br>with standard<br>Spelling<br>儿童式简单拼写<br>加标准拼写<br><img width='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_kg_spelling2.png'></td>
											</tr>";
						$returnArr .= "</table>";
					$returnArr .= "</td></tr>";
				$returnArr .= "</table>";
			}
			
			if($isTIBASchoolReport)
			{
			    // loop subjects
			    foreach($SubjectArray as $SubjectID => $SubjectName)
			    {
			        $thisSubjectNameDisplayEn = strtoupper($bothLangSubjectArray['en'][$SubjectID]);
			        $thisSubjectNameDisplayB5 = $bothLangSubjectArray['b5'][$SubjectID];
			        
			        // SKIP - Component subject / Students not in subject group / Empty comment
			        $isMainSubject = in_array($SubjectID, $MainSubjectIDArray);
			        if(!$isMainSubject || ($StudentID && (!$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID) || empty($commentAry[$SubjectID])))) {
			            continue;
			        }
			        
		            // Comment box
		            $commentDisplay = $StudentID? nl2br($commentAry[$SubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
		            if(isBig5(trim($commentDisplay))){
		                $commentDisplay = $this->getCurrentComment($commentDisplay);
		            }
		            // replace "‘" and "’" by "'"
		            $commentDisplay = str_replace("‘", "'", $commentDisplay);
		            $commentDisplay = str_replace("’", "'", $commentDisplay);
		            $commentDisplay = str_replace("“", "\"", $commentDisplay);
		            $commentDisplay = str_replace("”", "\"", $commentDisplay);
		            
		            $returnArr .= "<table class='ES_Comment_Eng' width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";
		                $returnArr .= "<tr><td class='Eng_Subject_Title'>".$thisSubjectNameDisplayEn."TEACHER'S  COMMENTS".$thisSubjectNameDisplayB5."老师评语 </td></tr>";
			            $returnArr .= "<tr><td valign='top' height='100px' style='font-size:10pt;'>$commentDisplay</td></tr>";
		            $returnArr .= "</table>";
			    }
			}
			else
			{
    			// Comment box for English Teacher
    			$EnglishSubjectID = $SubjectCodeIDMappping["KG English"];
    			$commentDisplay = $StudentID? nl2br($commentAry[$EnglishSubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
    			if(isBig5(trim($commentDisplay))){
    				$commentDisplay = $this->getCurrentComment($commentDisplay);
    			}
    			// replace "‘" and "’" by "'"
    			$commentDisplay = str_replace("‘", "'", $commentDisplay);
    			$commentDisplay = str_replace("’", "'", $commentDisplay);
    			$commentDisplay = str_replace("“", "\"", $commentDisplay);
    			$commentDisplay = str_replace("”", "\"", $commentDisplay);
    						
    			$returnArr .= "<table class='ES_Comment_Eng' width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";
    				$returnArr .= "<tr><td class='Eng_Subject_Title'>ENGLISH TEACHER'S  COMMENTS英文老师评语 </td></tr>";
    				$returnArr .= "<tr><td valign='top' height='100px' style='font-size:10pt;'>$commentDisplay</td></tr>";
    			$returnArr .= "</table>";
    			
    			// Comment box for Chinese Teacher
    			$ChineseSubjectID = $SubjectCodeIDMappping["KG Chinese"];
    			$commentDisplay = $StudentID? nl2br($commentAry[$ChineseSubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
    			if(isBig5(trim($commentDisplay))){
    				$commentDisplay = $this->getCurrentComment($commentDisplay);
    			}
    			// replace "‘" and "’" by "'"
    			$commentDisplay = str_replace("‘", "'", $commentDisplay);
    			$commentDisplay = str_replace("’", "'", $commentDisplay);
    			$commentDisplay = str_replace("“", "\"", $commentDisplay);
    			$commentDisplay = str_replace("”", "\"", $commentDisplay);
    			
    			// use div to prevent pagebreak between Comment and Signature
    			$returnArr .= "<div width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";
    						
    			$returnArr .= "<table class='ES_Comment_Eng' width='100%' border='0' cellspacing='0' cellpadding='0'>";
    				$returnArr .= "<tr><td class='Eng_Subject_Title'>CHINESE TEACHER'S COMMENTS 中文老师评语</td></tr>";
    				$returnArr .= "<tr><td valign='top' height='100px' style='font-size:10pt;'>$commentDisplay</td></tr>";
    			$returnArr .= "</table>";
			}
			
			// Signature
			$returnArr .= "<table class='ES_Eng_Sign' width='100%' border='0' cellspacing='2' cellpadding='2' autosize='1'>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>&nbsp;</td>";
					$returnArr .= "<td width='10%'>&nbsp;</td>";
					$returnArr .= "<td width='45%'>&nbsp;</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>__________________________________</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
					$returnArr .= "<td width='45%'>__________________________________</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>教师签字</td>";
					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>日期</td>";
					$returnArr .= "<td width='45%'>校长签字</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='45%'>Teachers' Signature</td>";
					$returnArr .= "<td width='10%'>&nbsp;</td>";
//					$returnArr .= "<td width='45%'>Date</td>";
					$returnArr .= "<td width='45%'>Principal's Signature</td>";
				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//					$returnArr .= "<td width='10%'>&nbsp;</td>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//					$returnArr .= "<td width='10%'>&nbsp;</td>";
//					$returnArr .= "<td width='45%'>&nbsp;</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>__________________________________</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>__________________________________</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>管理组签字</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>日期</td>";
//				$returnArr .= "</tr>";
//				$returnArr .= "<tr>";
//					$returnArr .= "<td width='45%'>Administrative Signature</td>";
//					$returnArr .= "<td width='10%'>&nbsp; </td>";
//					$returnArr .= "<td width='45%'>Date</td>";
//				$returnArr .= "</tr>";
			$returnArr .= "</table>";
			
			$returnArr .= "</div>";
 		}
 		# MS Transcript (Semester Report) - mst2
 		else if($isMainReport && $isMSReport && is_array($SubjectArray))
 		{
 			$isFirst = 1;
 			$comp_count = -1;
 			$displaySubjectCount = 0;
 			
 			$returnArr = "";
 			
 			// get related reports for Whole Year Report
	 		$termReportAry = array();
 			if($ReportType=="W"){
	 			$relatedTermReports = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
 			}
			
			// target semester id of progress report
			$target_sem_id = $SemID;
			if($ReportType=="W"){
		 		$target_sem_id = $secondTermID;
	 		}

			// condition for progress report query
			//$other_field = " ReportSequence = '".($SemesterNo==1? "2" : "4")."'";
			
			// get comments from related progress report
			// after using 4 terms, report can be distinished using semester and report type
			//$commentFromReport = $this->returnReportTemplateBasicInfo("", $other_field, $ClassLevelID, $target_sem_id, 0);
			$commentFromReport = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $target_sem_id, 1);
			$commentAry = $this->returnSubjectTeacherComment($commentFromReport["ReportID"],$StudentID);
			
 			// class name
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			// Handle archive students
			if(empty($StudentInfoArr) && $StudentID!=""){
				$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);
				
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lclass = new libclass();
				$StudentInfoArr[0]["ClassID"] = $lclass->getClassID($StudentInfoArr[0]['ClassName'], $this->schoolYearID);
			}
			
			$thisClassName = $StudentID? $StudentInfoArr[0]['ClassName'] : "XXX";
 			
 			// subject result table
 			$returnArr .= "<table class='ms_result_table' width='100%' border='0' cellspacing='0' cellpadding='0'>";
				$returnArr .= "<thead>";
				$returnArr .= "<tr>";
					$returnArr .= "<th width='26%' rowspan='2'>Class Name</td>";
					$returnArr .= "<th width='34%' rowspan='2'>Teacher Name</td>";
					$returnArr .= "<th width='20%' colspan='2'>1st Semester</td>";
					$returnArr .= "<th width='20%' colspan='2'>2nd Semester</td>";
				$returnArr .= "</tr>";
				$returnArr .= "<tr>";
					$returnArr .= "<th width='10%'>Percent</th>";
					$returnArr .= "<th width='10%' style='border-left:1px solid #000000'>Grade</th>";
					$returnArr .= "<th width='10%'>Percent</th>";
					$returnArr .= "<th width='10%' style='border-left:1px solid #000000'>Grade</th>";
				$returnArr .= "</tr>";
			$returnArr .= "</thead>";
			
			// loop subject
 			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# If all the marks is "*", then don't display
				if (sizeof($MarksAry[$SubjectID]) > 0) 
				{
					$Droped = 1;
					foreach($MarksAry[$SubjectID] as $cid=>$da)
						if($da['Grade']!="*")	$Droped=0;
				}
				if($Droped && $StudentID != "")	continue;
				
				// only display main subject
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	continue;
				
				// subject display name
				$SubjectName = str_replace("<br />", " ", $SubjectName);
				$SubjectDisplayName = "";
				
				// get subject group for semester report
				if($ReportType!="W" && $StudentID != ""){
					$SubjectGroups = $this->Get_Student_Studying_Subject_Group($SemID, $StudentID, $SubjectID, true);
					if($SubjectGroups == null) continue;
					
					$SubjectDisplayName = $SubjectGroups[0]['ClassTitleEN'];
				}
//				$SubjectDisplayName = $SubjectDisplayName==""? $thisClassName." ".$SubjectName : $SubjectDisplayName;
				$SubjectDisplayName = $SubjectDisplayName==""? $SubjectName : $SubjectDisplayName;
				
				// subject teacher
//				$SubjectTeacher = $this->Get_Student_Subject_Teacher($SemID, (array)$StudentID, $SubjectID);
//				$SubjectTeacher = Get_Array_By_Key($SubjectTeacher,"EnglishName");
//				$SubjectTeacher = $StudentID? implode($SubjectTeacher,",") : "XXX";
				if($ReportType != "W"){
					$SubjectTeacher = $this->returnSubjectTeacher($StudentInfoArr[0]["ClassID"], $SubjectID, $ReportID, $StudentID, $FullTeacherInfo=1);
					
					if(count($SubjectTeacher) > 0){
						$SubjectTeacher = Get_Array_By_Key($SubjectTeacher, "EnglishName");
						$SubjectTeacher = implode(", ", $SubjectTeacher);
					} else {
						$SubjectTeacher = $this->EmptySymbol;
					}
				} else {
					$termSubjectTeacher = array();
					$SubjectTeacher = $this->EmptySymbol;
					
					for($reportcount=0; $reportcount<count($relatedTermReports); $reportcount++){
						$termReportInfo = $relatedTermReports[$reportcount];
						$termSubjectTeacher = array_merge($termSubjectTeacher, $this->returnSubjectTeacher($StudentInfoArr[0]["ClassID"], $SubjectID, $termReportInfo["ReportID"], $StudentID, $FullTeacherInfo=1));
					}
					$termSubjectTeacher = array_unique($termSubjectTeacher);
					
					if(count($termSubjectTeacher)>0){
						$SubjectTeacher = Get_Array_By_Key($termSubjectTeacher, "EnglishName");
						$SubjectTeacher = implode(", ", $SubjectTeacher);
					}
				}
				
				// subject score
				$subjectContent = array();
				$subjectContent[1] = array("&nbsp;","&nbsp;");
				$subjectContent[2] = array("&nbsp;","&nbsp;");
				// Term 2
				if($SemesterNo == 2){
					$subjectContent[1][0] = $StudentID? $MarksAry[$SubjectID][0]["Mark"] : "S";
					$subjectContent[1][1] = $StudentID? $MarksAry[$SubjectID][0]["Grade"] : "G";
				}
				// Term 4
				else if($SemesterNo == 4){
					$subjectContent[2][0] = $StudentID? $MarksAry[$SubjectID][0]["Mark"] : "S";
					$subjectContent[2][1] = $StudentID? $MarksAry[$SubjectID][0]["Grade"] : "G";
				} 
				else {
					$firstColumnID = $ReportColumnAry[0]["ReportColumnID"];
					$secondColumnID = $ReportColumnAry[1]["ReportColumnID"];
					
					$subjectContent[1][0] = $StudentID? $MarksAry[$SubjectID][$firstColumnID]["Mark"] : "S";
					$subjectContent[1][1] = $StudentID? $MarksAry[$SubjectID][$firstColumnID]["Grade"] : "G";
					$subjectContent[2][0] = $StudentID? $MarksAry[$SubjectID][$secondColumnID]["Mark"] : "S";
					$subjectContent[2][1] = $StudentID? $MarksAry[$SubjectID][$secondColumnID]["Grade"] : "G";
				}
				
				// subject row
				$returnArr .= "<tr>";
					$returnArr .= "<td align='left' style='padding-left:10px;'>$SubjectDisplayName</td>";
					$returnArr .= "<td align='left' style='padding-left:10px;'>$SubjectTeacher</td>";
					$returnArr .= "<td>".$subjectContent[1][0]."</td>";
					$returnArr .= "<td style='border-left:1px solid #000000'>".$subjectContent[1][1]."</td>";
					$returnArr .= "<td>".$subjectContent[2][0]."</td>";
					$returnArr .= "<td style='border-left:1px solid #000000'>".$subjectContent[2][1]."</td>";
				$returnArr .= "</tr>";
			}
			$returnArr .= "</table>";
			
			// attendance records
			// for dev
//			$firstTermID = 30;
//			$secondTermID = 32;
			$attendanceMonthlyAry = $this->attendanceMonthlyArr[$StudentID];
			$firstTermTotal = $attendanceMonthlyAry[$firstTermID]["Total"]? $attendanceMonthlyAry[$firstTermID]["Total"] : 0;
			$secondTermTotal = $attendanceMonthlyAry[$secondTermID]["Total"]? $attendanceMonthlyAry[$secondTermID]["Total"] : 0;
			$firstTermPresent = $attendanceMonthlyAry[$firstTermID]["Present"]? $attendanceMonthlyAry[$firstTermID]["Present"] : 0;
			$secondTermPresent = $attendanceMonthlyAry[$secondTermID]["Present"]? $attendanceMonthlyAry[$secondTermID]["Present"] : 0;
			$firstTermAbsent = $attendanceMonthlyAry[$firstTermID]["Absent"]? $attendanceMonthlyAry[$firstTermID]["Absent"] : 0;
			$secondTermAbsent = $attendanceMonthlyAry[$secondTermID]["Absent"]? $attendanceMonthlyAry[$secondTermID]["Absent"] : 0;
			
			$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			$returnArr .= "<tr>";
				$returnArr .= "<td width='72%' valign='top'>";
				$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
					$returnArr .= "<tr>";
				
					// attendance table
					$returnArr .= "<td width=63%'>";
					$returnArr .= "<table class='ms_attendance_table' width='100%' border='0' cellspacing='0' cellpadding='0'>";
						$returnArr .= "<tr>";
							$returnArr .= "<td align='center' colspan='4' style='background-color: #bdbdbd;'>Attendance</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td width='34%'>&nbsp;</td>";
							$returnArr .= "<td width='22%'>Semester 1</td>";
							$returnArr .= "<td width='22%'>Semester 2</td>";
							$returnArr .= "<td width='22%'>Total</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td>Present</td>";
							$returnArr .= "<td>$firstTermPresent</td>";
							$returnArr .= "<td>$secondTermPresent</td>";
							$returnArr .= "<td>".($firstTermPresent+$secondTermPresent)."</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td>Absent</td>";
//								$returnArr .= "<td>".intval($attendanceAry[$StudentID][$firstTermID]["NumAbs"]/2)."</td>";
//								$returnArr .= "<td>".intval($attendanceAry[$StudentID][$secondTermID]["NumAbs"]/2)."</td>";
//								$returnArr .= "<td>".(intval($attendanceAry[$StudentID][$firstTermID]["NumAbs"]/2)+intval($attendanceAry[$StudentID][$secondTermID]["NumAbs"]/2))."</td>";
							$returnArr .= "<td>$firstTermAbsent</td>";
							$returnArr .= "<td>$secondTermAbsent</td>";
							$returnArr .= "<td>".($firstTermAbsent+$secondTermAbsent)."</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td>School days</td>";
							$returnArr .= "<td>$firstTermTotal</td>";
							$returnArr .= "<td>$secondTermTotal</td>";
							$returnArr .= "<td>".($firstTermTotal+$secondTermTotal)."</td>";
						$returnArr .= "</tr>";
					$returnArr .= "</table>";
					$returnArr .= "</td>";
			
					$returnArr .= "<td width='5%'>";
					$returnArr .= "</td>";
					
					// remark table
					$returnArr .= "<td width='32%'>";
					$returnArr .= "<table class='ms_remarks_table' width='100%' border='0' cellspacing='4' cellpadding='4'>";
						$returnArr .= "<tr>";
							$returnArr .= "<td class='border_bottom' colspan='3' style='text-align:center; background-color: #bdbdbd; padding-top: 2px;'>Grading Scale</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td width='9%'>&nbsp;</td>";
							$returnArr .= "<td width='46%'>A: 90-100</td>";
							$returnArr .= "<td width='45%'>B: 80-89</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td>&nbsp;</td>";
							$returnArr .= "<td>C: 70-79</td>";
							$returnArr .= "<td>D: 60-69</td>";
						$returnArr .= "</tr>";
						$returnArr .= "<tr>";
							$returnArr .= "<td>&nbsp;</td>";
							$returnArr .= "<td colspan='2' style='font-size:7pt; padding-left:25px;'>Needs Improvement<br>(NI): Below 59</td>";
						$returnArr .= "</tr>";
					$returnArr .= "</table>";
					$returnArr .= "</td>";
					
				$returnArr .= "</tr>";
				$returnArr .= "</table>";
					
				// signature
				$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='page-break-inside: avoid;' autosize='1'>";
					$returnArr .= "<tr>";
						$returnArr .= "<td width='16%'>Principal Signature</td>";
						$returnArr .= "<td width='45%' style='border-bottom:1px solid black;'>&nbsp;</td>";
						$returnArr .= "<td width='5%'>&nbsp;</td>";
						$returnArr .= "<td width='7%'>Date</td>";
						$returnArr .= "<td width='27%' style='border-bottom:1px solid black;'>&nbsp;</td>";
					$returnArr .= "</tr>";
				$returnArr .= "</table>";

				$returnArr .= "</td>";
			
				$returnArr .= "<td width='4%'>&nbsp;</td>";
				
				// icons
				$returnArr .= "<td width='24%'>";
					$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding-bottom:8px;'>";
						$returnArr .= "<tr>";
							$returnArr .= "<td width='42%'><img width='95px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_ib.png'></td>";
							$returnArr .= "<td width='12%'>&nbsp;</td>";
							$returnArr .= "<td width='46%'><img width='70px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_imyc.jpg'></td>";
						$returnArr .= "</tr>";
					$returnArr .= "</table>";
					$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
						$returnArr .= "<tr>";
							$returnArr .= "<td width='100%' align='center'><img width='132px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_cambridge.png'></td>";
						$returnArr .= "</tr>";
					$returnArr .= "</table>";
				$returnArr .= "</td>";
				
			$returnArr .= "</tr>";
			$returnArr .= "</table>";
			
			// next page
			$returnArr .= "<pagebreak />";
			
			// comment table
			$returnArr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
				$returnArr .= "<tr>";
					$returnArr .= "<td width='7%' align='center'><img height='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo_only.png'></td>";
					$returnArr .= "<td style='padding-top:10px;'><b>Teacher's Comments</b></td>";
				$returnArr .= "</tr>";
			$returnArr .= "</table>";
			
			$returnArr .= "<table class='ms_result_table' width='100%' border='0' cellspacing='0' cellpadding='0'>";
				$returnArr .= "<thead>";
				$returnArr .= "<tr>";
					$returnArr .= "<th width='15%' align='center' style='font-size:10pt; border-bottom:3px solid #000000'>Class</td>";
					$returnArr .= "<th width='85%' align='center' style='font-size:10pt; border-bottom:3px solid #000000'>Comments</td>";
				$returnArr .= "</tr>";
				$returnArr .= "</thead>";
				
				// loop subject
				foreach($SubjectArray as $SubjectID => $SubjectName)
				{
					# If all the marks is "*", then don't display
					if (sizeof($MarksAry[$SubjectID]) > 0) 
					{
						$Droped = 1;
						foreach($MarksAry[$SubjectID] as $cid=>$da)
							if($da['Grade']!="*")	$Droped=0;
					}
					if($Droped)	continue;
					
					// only display main subject
					if(in_array($SubjectID, $MainSubjectIDArray)!=true)	continue;
					
					// subject display name
					$SubjectName = str_replace("<br />", " ", $SubjectName);
					$SubjectDisplayName = "";
					// get subject group for semester report
					if($ReportType!="W"){
						$SubjectGroups = $this->Get_Student_Studying_Subject_Group($SemID, $StudentID, $SubjectID, true);
						if($SubjectGroups == null && $StudentID!="") continue;
						
						$SubjectDisplayName = $SubjectGroups[0]['ClassTitleEN'];
					}
//					$SubjectDisplayName = $SubjectDisplayName==""? $thisClassName." ".$SubjectName : $SubjectDisplayName;
					$SubjectDisplayName = $SubjectDisplayName==""? $SubjectName : $SubjectDisplayName;
					
					$commentDisplay = $StudentID? nl2br($commentAry[$SubjectID]) : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX";
					if(isBig5(trim($commentDisplay))){
						$commentDisplay = $this->getCurrentComment($commentDisplay);
					}
					// replace "‘" and "’" by "'"
					$commentDisplay = str_replace("‘", "'", $commentDisplay);
					$commentDisplay = str_replace("’", "'", $commentDisplay);
					$commentDisplay = str_replace("“", "\"", $commentDisplay);
					$commentDisplay = str_replace("”", "\"", $commentDisplay);
					
					// comment row
					$returnArr .= "<tr>";
						$returnArr .= "<td align='left' valign='top' style='font-size:10pt; padding-left:10px; border-bottom:3px solid #000000;'>$SubjectDisplayName</td>";
						$returnArr .= "<td align='left' style='font-size:10pt; padding-left:10px; border-bottom:3px solid #000000;'>$commentDisplay</td>";
					$returnArr .= "</tr>";
				}
			$returnArr .= "</table>";
 		}
 		// Progress Report
 		else 
 		{
 			// loop subject
 			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isAllNA = true;
				
				// only display main subject
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	
					continue;
				
				// for preview
				if($StudentID==""){
					$x  = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>";
					$x .= "<td class='bold_cell'>X</td>";
					$x .= "<td class='bold_cell' align='right'>XX%</td>";
					$x .= "</tr></table>";
					
					$returnArr[$SubjectID] = $x;
					continue;
				}
				
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				$ScaleFullMark = $SchemeInfo['FullMark'];
				
				// Subject mark and grade
				$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
				$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
				
				// Convert mark to grade
				$thisStyleDetermineMark = $thisMSMark;
				if ($thisMSMark!="" && $thisMSGrade=="") {
					$thisMSGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SubjectFormGradingSettings["SchemeID"], $thisMSMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID, 0);
				}
				
				// calculate mark percentage
				if($thisMSMark != "" && $ScaleFullMark!="") {
					$thisMSMark = $ScaleFullMark>0? round(($thisMSMark / $ScaleFullMark * 100), $SubjectScore) : 0;
				}
				
				// Special Case
				if($thisMSGrade!="" && $this->Check_If_Grade_Is_SpecialCase($thisMSGrade)){
					$thisMSMark = "-";
					$thisMSGrade = "-";
				} else {
					// Empty Mark
					if($thisMSMark == ""){
						$thisMSMark = "-";
					}
					// Empty Grade
					if($thisMSGrade == ""){
						$thisMSGrade = "-";
					}
				}
				
				// subject row
				$x  = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>";
				$x .= "<td class='bold_cell'>$thisMSGrade</td>";
				$x .= "<td class='bold_cell' align='right'>$thisMSMark%</td>";
				$x .= "</tr></table>";
				
				$returnArr[$SubjectID] = $x;
			}
 		}
		return $returnArr;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType='', $bilingual=false) {
		
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='', $bilingual=false) {
 		
	}
	
	function getCurrentComment($Comment){
		$returnComment = "";
		$delim = "";
		
		// remove &shy; from comment before checking
		$Comment = str_replace("­", "", $Comment);
		$CommentAry = explode("<br />",$Comment);
		foreach((array)$CommentAry as $CommentContent){
			//if(trim($CommentContent)!="" && isBig5($CommentContent)){
			if(trim($CommentContent)!="" && preg_match("/[\x{4e00}-\x{9fa5}]+/u", $CommentContent)) {
				$CommentContent = str_replace(" ", "", $CommentContent);
			}
			$returnComment .= $delim.$CommentContent;
			$delim = "<br>";
		}
		return $returnComment;
	}
	
	function getCSSContent($ReportID=""){
		
		# Report Info
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$isProgressReport = !$ReportBasicInfo['isMainReport'];
 		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		// [2015-1013-1434-16164] get Report Type
		// ES/MS Report
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
		}
		
		$style = "
				<head>
					<style TYPE='text/css'>
						
						body {
							font-family: msjh; 
							font-size: 8.5pt;
						}
						
						hr {
							height:1px;
							color:black; 
						}";

		if($isProgressReport && $isESReport){	
			$style .=	".report_header {
							font-size: 14pt;
						}";
		} else {
			$style .=	".report_header {
							font-size: 10pt;
						}";
		}
		$style .= "		.header_describe {
							border:3px solid black;
							margin-bottom: 5mm;	
							font-size: 9.5pt;
						}

						.header_describe .engheader_describe {
							font-size: 8.5pt;
						}
						
						.report_header .report_info_title {
							font-size: 14pt;
							font-weight: bold;
						}

						.mstable_header {
							font-size: 9pt;
						}

						.mstable_header .ms_address {
							padding: 2px 5px 0px 5px;;
						}

						.mstable_header .report_info_mstable {
							border: 3px solid #000000;
							border-collapse:collapse;
						}

						.mstable_header .report_info_mstable td {
							border: 1px solid #000000;
							border-left: 3px solid #000000;
							text-align:center;
							padding: 4px;
						}

						.kg_report_header {
							font-size: 10pt;
							line-height: 20px;
						}
						
						.kg_report_header .kg_report_header_table td {
							line-height: 30px;
						}
						
						.kg_report_header .report_info_title {
							font-size: 14pt;
							font-weight: bold;
							line-height: 35px;
						}

						.kg_header_describe {
							border:3px solid black;
							margin-bottom: 2mm;	
							font-size: 9.5pt;
							text-align: center;
						}

						.kg_header_describe td {
							padding-bottom: 10px;
						}

						.student_info {
							font-size: 10pt;
							font-weight: bold;
							/*border-left: 0.2mm solid #000000;*/
							border: 0.2mm solid #000000;
							border-bottom: 0mm solid #000000;
						}
						
						.bold_cell {
							font-size: 11pt;
							font-weight: normal;	
						}
						
						.result_table th, .result_table td {
							border: 0.05mm solid #BDBDBD;
							text-align: left;
						}

						.result_table th, .result_table td {
							padding: 2px;
							padding-left: 4px;
						}

						.result_table .table_header {
							font-size: 5pt;
							font-weight: normal;
						}

						.result_table td table, .result_table td table td, .result_table .student_info, .result_table .student_info td {
							border: 0mm solid #000000;
						}
						
						.ms_result_table {
							border: 3px solid #000000;
							border-collapse: collapse;
							margin-top: 3mm;
							margin-bottom: 3mm;
						}

						.ms_result_table th	{
							background-color: #bdbdbd;
						}

						.ms_attendance_table, .ms_remarks_table {
							border: 3px solid #000000;
							border-collapse: collapse;
							margin-bottom: 8mm;
							text-align: center;
						}

						.ms_result_table th, .ms_result_table td, .ms_attendance_table td {
							border: 1px solid #000000;
							text-align: center;
							padding: 2px;
							font-weight:normal;
						}					

						.ms_result_table th, .ms_result_table td {
							border-left: 3px solid #000000;
						}				

						.ms_attendance_table td, .ms_remarks_table td {
							padding: 2px;
						}

						.ms_remarks_table td {
							padding: 6px 2px 2px 2px;
							text-align: left;
    						vertical-align: center;
						}

						.border_bottom {
							border-bottom: 1px solid #000000;
						}
						
						.ES_ParentSubject, .ES_ParentSubject_s {
							font-size: 12pt;
							font-weight: bold;
						}

						.ES_ParentSubject_s {
							padding-top:5mm;
							border-top: 0.05mm solid black;
						}
						
						.English_ES {
							font-weight: normal;
							border: 0.05mm solid black;
							border-collapse:collapse;
						}

						.ES_CompSubject {
							font-size: 10pt;
							margin-bottom: 6mm;
							border: 0.2mm solid black;
							border-collapse:collapse;
						}

						.ES_CompSubject td {
							border: 0.2mm solid black;
						}

						.ES_CompSubject .comp_title {
							font-size: 12pt;
						}
						
						.ES_CompSubject .comp_bold {
							font-weight: bold;
						}
						
						.Eng_Subject_Title {
							background-color: black;
 							color: white;
							font-size: 10pt;
							padding-left: 3px;
						}
						
						.Eng_CompSubject_Title {
							background-color: lightgray;
							font-size: 10pt;
							padding-left: 3px;
						}

						.Eng_CompSubject_Content {
							background-color: white;
							font-size: 9pt;
							padding-left: 3px;
						}
						
						.Eng_Subject_ccc {
							background-color: gray;
 							color: white;
						}

						.KG_ParentSubject {
							padding-bottom: 16px;
						}

						.table_pad_top {
							margin-top: 10mm;
							margin-bottom: 5mm;
						}

						.kg_writing_describe {
							border: 0.2mm solid black;
							border-collapse: collapse;
							line-height: 2;
							margin-bottom: 6mm;
						}

						.kg_writing_describe td {
							padding: 2px;
							padding-bottom: 5px
							text-align:center;
							vertical-align: top;
						}

						.gray_header td {
							background-color: lightgray;
						}

						.ES_Comment {
							border: 0.2mm solid black;		
							margin-bottom: 6mm;		
							font-size: 12pt;
						}

						.ES_Comment_Eng {
							border: 0.2mm solid black;		
							margin-top: 6mm;			
						}

						.kg_comment_style {
							border: 0.6mm solid black;
							padding: 6px;
							margin-bottom: 10px;
							page-break-inside: avoid;
						}

						.ES_Sign {
							font-size: 11pt;
							page-break-inside: avoid;
						}

						.ES_Eng_Sign {
							margin-top: 18mm;
							text-align: center;
							font-size: 11pt;
							page-break-inside: avoid;
						} 

						.sign_table {
							margin-top: 2.6mm;
							font-size: 10pt;
							page-break-inside: avoid;
						}

						.sign_table th, .sign_table td {
							padding: 0px;
							padding-left: 0px;
						}
					</style>
				</head>";
				
		return $style;
	}
	########## END Template Related ##############
	
	########## START PDF Generation ##############
	function reportObjectSetUp($ReportID, $bilingual=false){
		# Report Info
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportBasicInfo['isMainReport'];
		$isProgressReport = !$isMainReport;
		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$SemID = $ReportBasicInfo['Semester'];
		$SemesterNo = $this->Get_Semester_Seq_Number($SemID);
		
		// [2015-1013-1434-16164] get Report Type
		// ES/MS Report
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
			$isSemesterReport = $SemID=="F" || ($SemesterNo > 0 && $SemesterNo % 2==0);
		}
		$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
		
		// margin (in mm)
		// Progress Report
		if($isProgressReport) 
		{
			$report_format = 'A4';
			$margin_top = 22.5;
			$margin_bottom = 24;
			$margin_left = 14;
			$margin_right = 11;
			$margin_header = 21;
			$margin_footer = 24;
		}
		// MS Transcript
		else if($isMSReport)
		{
			$report_format = 'A4-L';
			$margin_top = 18;
			$margin_bottom = 16;
			$margin_left = 28;
			$margin_right = 24;
		}
		// ES Semester / KG Report
		else
		{
			$report_format = 'A4';
			$margin_top = 12;
			$margin_bottom = 13;
			$margin_left = 8;
			$margin_right = 8;
			if($isMainReport && $isKGReport && $isSemesterReport)
				$margin_footer = 10;
		}
		
		// Create mPDF object
		$this->pdf = new mPDF('', $report_format, 0, '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);

		// PDF Setting
		$this->pdf->setAutoTopMargin = 'stretch';
		$this->pdf->splitTableBorderWidth = 0.1; //split 1 table into 2 pages add border at the bottom
	}
	########## END PDF Generation #############
		
	function GenerateBilingualReport($ReportID){
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		// [2015-1013-1434-16164] get Report Type
		// ES/MS Report
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
		}
		$isTIBASchoolReport = $this->returnThisReportSchoolType($ClassLevelID);
		
		$bilingual = false;
		// return true for ES Semester Report
		if($isMainReport && $isESReport)
			$bilingual = true;
		
		return $bilingual;
	}
	
	function retrieveGPAData($ReportID, $UserArray) {
		// UserID
		$UserList = "-1";
		if(count($UserArray) > 0){
			$UserList = "";
			$delim = "";
			for($i=0; $i<count($UserArray); $i++){
				$UserList .= $delim."'".$UserArray[$i]["UserID"]."'";
				$delim = ", ";
			}
		}
		
		$table = $this->DBName.".RC_REPORT_RESULT a";
		$sql = "SELECT StudentID, GPA FROM $table WHERE ReportID = '$ReportID' AND ReportColumnID = '0' AND StudentID IN ($UserList)";
		
		$this->StudentGPAArr = BuildMultiKeyAssoc($this->returnArray($sql), array("StudentID"));
	}
	
//	function getStudentListAttendanceRecords($StudentIDAry){	
//		$sql = "SELECT 
//					UserID,
//					YearTermID,
//					COUNT(IF(RecordType=1 AND (DayType=2 OR DayType=3),0.5,NULL)) AS NumAbs   
//				FROM 
//					PROFILE_STUDENT_ATTENDANCE 
//				WHERE 
//					UserID IN ('".implode('\',\'',$StudentIDAry)."')
//					AND AcademicYearID = '13'
//				GROUP BY UserID, YearTermID";
//		
//		$result = $this->returnResultSet($sql);
//		
//		return BuildMultiKeyAssoc($result, array("UserID","YearTermID"));
//	}
	
	function retrieveAttandanceMonthData($UserArray, $semID, $SemNo) {
		// eAttendance define
//		define("PROFILE_DAY_TYPE_WD",1);
//		define("PROFILE_DAY_TYPE_AM",2);
//		define("PROFILE_DAY_TYPE_PM",3);
//		define("PROFILE_TYPE_ABSENT",1);
		
		$month_year_Ary = array();
		$result = array();
		
		// count (0.5/1) - according to eAttendance settings
		$eachCount = $this->ProfileAttendCount==1? 0.5 : 1;
		
		// UserID
		$UserList = "-1";
		if(count($UserArray) > 0){
			$UserList = "";
			$delim = "";
			for($i=0; $i<count($UserArray); $i++){
				$UserList .= $delim."'".$UserArray[$i]["UserID"]."'";
				$delim = ", ";
			}
		}
		else {
			return "";
		}
		
		// Current Seq Number
		$currentSeq = $this->Get_Semester_Seq_Number($semID);
		
		// Semester ID and Seq Number Mapping
		$semid_mapping = array();
		$sem_ary = $this->returnSemesters();
		foreach((array)$sem_ary as $cur_semID => $SemName){
			$semid_mapping[$this->Get_Semester_Seq_Number($cur_semID)] = $cur_semID;
		}
		
		// senster period range
		// for Dev
//		$year_period = getPeriodOfAcademicYear(13, $semID=="F"? "" : 32);
		$year_period = getPeriodOfAcademicYear($this->GET_ACTIVE_YEAR_ID(), $semID=="F"? "" : $semID);
		$year_start = $year_period["StartDate"];
		$year_end = $year_period["EndDate"];
		// Set Start date to previous term if Term 2 or 4
		if($semID!="F" && ($currentSeq==2 || $currentSeq==4)){
			$previousTermID = $semid_mapping[$currentSeq-1];
			$year_period = getPeriodOfAcademicYear($this->GET_ACTIVE_YEAR_ID(), $previousTermID);
			$year_start = $year_period["StartDate"];
		}
		$year_start_f = explode(" ",$year_start);
		$year_end_f = explode(" ",$year_end);
		list($start_year,$start_month,$start_date) = explode("-",$year_start_f[0]);
		list($end_year,$end_month,$end_date) = explode("-",$year_end_f[0]);

		// build array for query data in CARD_STUDENT_DAILY_LOG_y_m
		// year
		while("$start_year" <= "$end_year"){
			$month_year_Ary[$start_year] = array();
			
			// month
			$check_month = "$start_year"=="$end_year"? $end_month : "12";
			while("$start_year$start_month" <= "$start_year$check_month"){
				$month_year_Ary[$start_year][] = $start_month;
				
				$start_month++;
				$start_month = $start_month<10? "0$start_month" : $start_month;
				if(count($month_year_Ary[$start_year]) > 12)
					break;
			}
			$start_month = "01";
			$start_year++;
		}
		
		// whole year report - semester period range
		if($semID=="F"){
			//$first_sem = getAcademicYearAndYearTermByDate($year_start);
			$first_sem = $semid_mapping[2];
			// for Dev
//			$first_sem_period = getPeriodOfAcademicYear(13, $first_sem["YearTermID"]);
			//$first_sem_period = getPeriodOfAcademicYear($this->GET_ACTIVE_YEAR_ID(), $first_sem["YearTermID"]);
			$first_sem_period = getPeriodOfAcademicYear($this->GET_ACTIVE_YEAR_ID(), $first_sem);
			$first_sem_period_end = $first_sem_period["EndDate"];
			$first_sem_period_end = explode(" ",$first_sem_period_end);
//			list($first_sem_endyear,$first_sem_endmonth,$first_sem_enddate) = explode("-",$first_sem_period_end[0]);
			
			//$second_sem = getAcademicYearAndYearTermByDate($year_end);
			$second_sem = $semid_mapping[4];
		}
		
		if(count($month_year_Ary) > 0){
			foreach($month_year_Ary as $year => $month_Ary){
				foreach($month_Ary as $month){
					// Table
					$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
					// Log date in (Y-m-d)
					$log_date_field = "CONCAT('".$year."','-','".$month."','-',IF(b.DayNumber<10, CONCAT('0',b.DayNumber), b.DayNumber))";
					
				  	$sql = "SELECT 
					            b.UserID,
								IF(b.DayNumber<10, CONCAT('0',b.DayNumber), b.DayNumber) AS DayNumber,
								$log_date_field AS LogDate,
					            b.AMStatus,
					            b.PMStatus,
					            b.LeaveStatus,
								c.RecordStatus as am_late_waive,
								d.RecordStatus as pm_late_waive,
								e.RecordStatus as am_absent_waive,
								f.RecordStatus as pm_absent_waive
								/*
									g.RecordStatus as am_early_leave_waive,
									h.RecordStatus as pm_early_leave_waive,
									c.Reason as am_late_reason,
									d.Reason as pm_late_reason,
									e.Reason as am_absent_reason,
									f.Reason as pm_absent_reason,
									g.Reason as am_early_leave_reason,
									h.Reason as pm_early_leave_reason,
									i.LateSession as am_late_session,
									i.RequestLeaveSession as am_request_leave_session,
									i.PlayTruantSession as am_play_truant_session,
									i.OfficalLeaveSession as am_offical_leave_session,
									j.LateSession as pm_late_session,
									j.RequestLeaveSession as pm_request_leave_session,
									j.PlayTruantSession as pm_play_truant_session,
									j.OfficalLeaveSession as pm_offical_leave_session
								*/
									FROM 
										$card_log_table_name as b ";
					if ($this->EnableEntryLeavePeriod) {
						$sql .= "
										INNER JOIN 
										CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
										on b.UserID = selp.UserID 
											and 
											DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
										";
					}
					$sql .= "			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
										ON 
											c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
											AND c.StudentID = b.UserID 
											AND c.DayType = '2' AND c.RecordType = '2' 
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
										ON 
											d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
											AND d.StudentID = b.UserID 
											AND d.DayType = '3' AND d.RecordType = '2' 
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
										ON 
											e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
											AND e.StudentID = b.UserID 
											AND e.DayType = '2' AND e.RecordType = '1' 
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
										ON 
											f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
											AND f.StudentID = b.UserID 
											AND f.DayType = '3' AND f.RecordType = '1'
								/* 
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
										ON 
											g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
											AND g.StudentID = b.UserID 
											AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
										ON 
											h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
											AND h.StudentID = b.UserID 
											AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
										LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
										ON 
											i.StudentID = b.UserID 
											AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
											AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
											AND i.DayType = '2' 
										LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
										ON 
											j.StudentID = b.UserID 
											AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
											AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
											AND j.DayType = '3' 
								*/
								where 
									b.UserID in (".$UserList.") AND $log_date_field >= '".$year_start_f[0]."' AND $log_date_field <= '".$year_end_f[0]."'
								ORDER BY b.DayNumber";
					$log_entries = $this->returnArray($sql);

					for($j=0; $j<count($log_entries); $j++){
						$log_records = $log_entries[$j];
						
						// current SemID
						$targetSem = $semID;
						if($semID=="F"){
							if($log_records['LogDate'] <= $first_sem_period_end[0]){
								//$targetSem = $first_sem["YearTermID"];
								$targetSem = $first_sem;
							} else {
								//$targetSem = $second_sem["YearTermID"];
								$targetSem = $second_sem;
							}
						}
						
						// init $result array of current sem
						if(!isset($result[$log_records['UserID']][$targetSem])){
							$result[$log_records['UserID']][$targetSem]["Total"] = 0;
							$result[$log_records['UserID']][$targetSem]["Present"] = 0;
							$result[$log_records['UserID']][$targetSem]["Absent"] = 0;
							// [2015-1013-1434-16164]
							$result[$log_records['UserID']][$targetSem]["Late"] = 0;
						}
						
						// Total
						//$result[$log_records['UserID']][$targetSem]["Total"] += $eachCount;
						// [2016-0201-1531-49164] Hard Code for 2015-16 1st Tern
						if($semID=="F"){
							//$result[$log_records['UserID']][$targetSem]["Total"] = 93 + 92;
							// Current Seq Number
							$currentSeq = $this->Get_Semester_Seq_Number($targetSem);
						}
//						if($currentSeq==1 || $currentSeq==2)
//							$result[$log_records['UserID']][$targetSem]["Total"] = 93;
//						else if($currentSeq==3)
//							$result[$log_records['UserID']][$targetSem]["Total"] = 47;
//						else if($currentSeq==4) 
//							$result[$log_records['UserID']][$targetSem]["Total"] = 92;
//						$result[$log_records['UserID']][$targetSem]["Total"] = $thisReportAttendanceDays;
						
						// Present (On Time, Absent with reason, Late, Outgoing)
						if(isset($log_records["AMStatus"]) && ($log_records["AMStatus"]==0 || 
								($log_records["AMStatus"]==1 && $log_records["am_absent_waive"]==1)	|| $log_records["AMStatus"]==2 || $log_records["AMStatus"]==3)){
							$result[$log_records['UserID']][$targetSem]["Present"] += $eachCount;
						}
						if(isset($log_records["PMStatus"]) && ($log_records["PMStatus"]==0 || 
								($log_records["PMStatus"]==1 && $log_records["pm_absent_waive"]==1) || $log_records["PMStatus"]==2 || $log_records["PMStatus"]==3)){
							$result[$log_records['UserID']][$targetSem]["Present"] += $eachCount;
						}
						
						// Absent (Absent without reason)
						if($log_records["AMStatus"]==1 && $log_records["am_absent_waive"]!=1){
							$result[$log_records['UserID']][$targetSem]["Absent"] += $eachCount;
						}
						if($log_records["PMStatus"]==1 && $log_records["pm_absent_waive"]!=1){
							$result[$log_records['UserID']][$targetSem]["Absent"] += $eachCount;
						}
						
						// [2015-1013-1434-16164]
						// Late (Late without reason)
						if($log_records["AMStatus"]==2 && $log_records["am_late_waive"]!=1){
							$result[$log_records['UserID']][$targetSem]["Late"] += $eachCount;
						}
						if($log_records["PMStatus"]==2 && $log_records["pm_late_waive"]!=1){
							$result[$log_records['UserID']][$targetSem]["Late"] += $eachCount;
						}
					}
				}
			}
		}
		$this->attendanceMonthlyArr = $result;
	}
	
	function returnChineseTeacherName($ReportID, $StudentID){
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Report Info
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportBasicInfo['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
 		$ClassLevelID = $ReportBasicInfo['ClassLevelID'];
			
		// get Report list
 		$termReportAry = array();
 		$termIDAry = array();
		if($ReportType=="W"){
 			$relatedTermReports = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
 			
 			for($reportCount=0; $reportCount<count($relatedTermReports); $reportCount++){
 				$termReportAry[] = $relatedTermReports[$reportCount]["ReportID"];
 				$termIDAry[] = $relatedTermReports[$reportCount]["Semester"];
 			}
		}
		else {
			$termReportAry[] = $ReportID;
			$termIDAry[] = $SemID;
		}
		
		$SubjectTeacherName = array();
		for($i=0; $i<count($termReportAry); $i++)
		{
			$ReportID = $termReportAry[$i];
			$SemID = $termIDAry[$i];
            $SemesterNo = $this->Get_Semester_Seq_Number($SemID);
            $targetTermSeq = 0;
            if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'] && $SemID != "F") {
                $targetTermSeq = ($SemesterNo == 1 || $SemesterNo == 2) ? 1 : 2;
            }

	 		// Subject
			$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID, "b5");
			$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
			if(sizeof($MainSubjectArray) > 0) {
				foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
            }
            // [2020-0915-1830-00164]
			// $SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray));
            $SubjectTopicArr = $this->subjectTopicObj->getSubjectTopic(array($ClassLevelID), array_keys($SubjectArray), '', $targetTermSeq);
			$SubjectTopicArr = BuildMultiKeyAssoc((array)$SubjectTopicArr, array("SubjectID", "SubjectTopicID"));
			
			// Subject Settings
			$Subject_Settings = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, '', $ReportID);
			
			// loop Subject
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				// skip if component subject
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	continue;
				
				// only display chinese subject & skip if student not in subject group
				if($Subject_Settings[$SubjectID]["LangDisplay"]!="ch")	continue;
				if(!$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID)) continue;
				
				// current subject without subject topic => check if component subject with subject topic
				if(count((array)$SubjectTopicArr[$SubjectID])==0)
				{
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr))
					{
						$parentSubjectWithTopics = false;
						
						// loop component subjects with subject topic
						for($j=0; $j<count($CmpSubjectArr); $j++)
						{
							$parentSubjectWithTopics = count((array)$SubjectTopicArr[$CmpSubjectArr[$j]["SubjectID"]])>0;
							if($parentSubjectWithTopics)	break;
						}
						
						// skip if all component subjects without subject topic
						if(!$parentSubjectWithTopics) continue;
					}
					// skip if without component subject
					else {
						continue;
					}
				}
				
				$SubjectTeacher = Get_Array_By_Key($this->Get_Student_Subject_Teacher($SemID, $StudentID, $SubjectID), "ChineseName");
				//$SubjectTeacherName = array_merge($SubjectTeacherName, (array)$SubjectTeacher);
				
				// return array with SubjectID
				$SubjectTeacher = implode("、", (array)$SubjectTeacher);
				$SubjectTeacherName[$SubjectID] = $SubjectTeacher;
			}
		}
		//$SubjectTeacherName = array_unique((array)$SubjectTeacherName);
		//$SubjectTeacherName = implode("、", (array)$SubjectTeacherName);
		
		return $SubjectTeacherName;
	}

	# Map Subject Code and Subject ID
	function GET_SUBJECT_SUBJECTCODE_MAPPING($reverse=0){
		
		$sql = "SELECT 
					CODEID,
					RecordID
				FROM
					ASSESSMENT_SUBJECT 
				WHERE
					EN_DES IS NOT NULL
					AND RecordStatus = 1
					AND (CMP_CODEID IS NULL || CMP_CODEID = '')
				ORDER BY
					DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 2);
		
		$ReturnArr = array();
		if (sizeof($SubjectArr) > 0) {
			for($i=0; $i<sizeof($SubjectArr); $i++) {
				if($reverse){
					$ReturnArr[$SubjectArr[$i]["CODEID"]] = $SubjectArr[$i]["RecordID"];
				}
				else{
					$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
				}
			}
		}
		return $ReturnArr;
	}
	
	function Get_Chinese_Num($num){
		$chineseNumberArr = array();
		$chineseNumberArr[0] = "一";
		$chineseNumberArr[1] = "一";
		$chineseNumberArr[2] = "二";
		$chineseNumberArr[3] = "三";
		$chineseNumberArr[4] = "四";
		$chineseNumberArr[5] = "五";
		
		return $chineseNumberArr[$num];
	}		
	
	function returnThisReportLevelType($FormNumber, $SemesterType, $SemesterNumber) {
		// Get Report Type
		// ES / MS Report
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
			$isKGSemesterReport = $SemesterType=="W" || ($SemesterNumber > 0 && $SemesterNumber % 2==0);
		}
		
		$thisReportType = array();
		$thisReportType["ES_Report"] = $isESReport;
		$thisReportType["MS_Report"] = $isMSReport;
		$thisReportType["KG_Report"] = $isKGReport;
		$thisReportType["KG_SemReport"] = $isKGSemesterReport;
		return $thisReportType;
	}
	
	function returnThisReportSemesterType() {
		
	}
	
	function returnThisReportSchoolType($ClassLevelID) {
	    $FormName = $this->returnClassLevel($ClassLevelID);
	    return strpos($FormName, 'TIBA') !== false;
	}
}
?>