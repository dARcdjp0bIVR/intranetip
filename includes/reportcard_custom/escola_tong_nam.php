<?php
# Editing by 

### Customization library for Escola Tong Nam (Macau) ###

include_once($intranet_root."/lang/reportcard_custom/escola_tong_nam.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "award", "merit", "eca", "remark", "attendance", "dailyPerformance");
		$this->OtherInfoInGrandMS = array("Conduct", "Merits", "Minor Credit", "Major Credit", "Demerits", "Minor Fault", "Major Fault", "Days Absent", "Time Late", "GRADE");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->SpaceWidth = "2%";
		$this->TotalSubjectColumn = 3;
		$this->TotalTermColumn = 7;
		$this->OrderDisplayMax = 10;
		$this->EmptySymbol = "----";
		
		$this->GraduationReport["CenterSpace"] = "20%";
		$this->GraduationReport["Colspan"] = 7;
		
		# display format
		$this->DisplayInfo = array("InfoCh", "InfoEn");
	}
	
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID) {
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		if ($isGraduationReport)
			$table_css = "";
		else
			$table_css = "";
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$x = "";
		$x .= "<tr><td>";
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' $table_css>";
		$x .= "<tr><td>".$TitleTable."</td></tr>";
		$x .= "<tr><td height='1'>&nbsp;</td></tr>";
		$x .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$x .= "<tr><td><img src='".$emptyImagePath."' height='5'></td></tr>";
		$x .= "<tr><td>".$MSTable."</td></tr>";
		$x .= "<tr><td>".$SignatureTable."</td></tr>";
		$x .= $FooterRow;
		$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
		
	function getReportHeader($ReportID, $StudentID="")
	{
		global $eReportCard, $intranet_root;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
			
			# get school badge
			$SchoolLogo = "/file/reportcard2008/templates/escola_tong_nam/image/logo.jpg";
						
			# get school title banner depends on student's class
			if ($this->IS_SECONDARY($ClassLevelID))
			{
				// secondary class => Use banner with Secondary
				$SchoolBanner = "/file/reportcard2008/templates/escola_tong_nam/image/banner_mid.jpg";
			}
			else
			{
				$SchoolBanner = "/file/reportcard2008/templates/escola_tong_nam/image/banner_mid_schname.jpg";
			}
			
			$SpaceForCenter = 120;
			
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='middle'>\n";
			$TitleTable .= "<tr><td width='120' align='center' valign='middle'>".$SchoolLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable = "";
				$TitleTable .= "
						        <table width='100%'  border='0' cellpadding='0' cellspacing='0'>\n
						          <tr>\n
						          	<td width='10' valign='top'>&nbsp;</td>\n
						            <td width='120' valign='top'><img height='105' src='$SchoolLogo' vspace='5'></td>\n
						            <td align='center' valign='top'>\n
						            	<img height='63' src='$SchoolBanner'>\n
						            	<br>\n
						            	<span style='color: #309420; font: bold; font-size: 22px;font-family:標楷體;'>".$eReportCard['Template']['ReportTitleCh']."</span>\n
						            	<br>\n
								";
				$TitleTable .= '<span>'.$this->Get_Empty_Image_Div('5px').'</span>';
				$TitleTable .= "		
						            	<span style='color: #309420; font: bold; font-size: 16px; font-family: arial'>".$eReportCard['Template']['ReportTitleEn']."</span>\n
			            			</td>\n
						            <td width='{$SpaceForCenter}' align='right' valign='top'>";
						            
				if ($isGraduationReport)
				{
					# add photo frame for F6 Graduation Report
					$TitleTable .= "<table height='120px' width='100px' border='0' cellpadding='0' cellspacing='0' class='report_border' valign='middle'>\n";
						$TitleTable .= "<tr>";
							$TitleTable .= "<td>";
								$TitleTable .= "&nbsp;";
							$TitleTable .= "</td>";
						$TitleTable .= "</tr>";
					$TitleTable .= "</table>";
				}
				else
				{
					$TitleTable .= "&nbsp;";
				}    
				$TitleTable .= " </td>\n";       
			}
			$TitleTable .= "</table>\n";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			
			
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR_NAME();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();

				$data['Name'] = $lu->UserName2Lang('ch', 2);
				//$data['ClassNo'] = $lu->ClassNumber;
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['StudentNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				
			}
			
			# hardcode the 1st six items (Name, ClassNumber, AcademicYear, ClassName, STRN, DateOfIssue)
			$defaultInfoArray = array("Name", "ClassNo", "AcademicYear", "Class", "STRN", "DateOfIssue");
						
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
						$dataWidth = '200px';
					}
					else
					{
						$dataWidth = '130px';
					}
					
					$StudentInfoTable .= "<td class='tabletext2' valign='top' height='{$LineHeight}'>";
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
								              <tr>
								              	<td width='2'>&nbsp;</td>
								                <td nowrap width='50'>".$Title."</td>
								                <td width='2'>";
					if ($Title != "") $StudentInfoTable .= ":&nbsp;";
					$StudentInfoTable .= "</td><td width='$dataWidth' nowrap>";
					$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
					$StudentInfoTable .= "</td></tr></table>\n";
					
					
					$StudentInfoTable .= "</td>";
										
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					} 
					$count++;
				}
			}
			
			# Display student info selected by the user other than the 1st six items
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext2' width='33%' valign='top' height='{$LineHeight}'>";
						
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
									              <tr>
									              	<td width='2'>&nbsp;</td>
									                <td nowrap width='50'>".$Title."</td>
									                <td width='2'>";
						if ($Title != "") $StudentInfoTable .= ":&nbsp;";
						$StudentInfoTable .= "</td><td nowrap width='180'>";
						$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
						$StudentInfoTable .= "</td></tr></table>\n";
												
						$StudentInfoTable .= "</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						$count++;
					}
				}				
			}
			
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$space = $this->SpaceWidth;
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
						
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		//$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$table_css = "";
		//if ($this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID))
		//{
		//	$table_css = "class='bg_F6_GraduationReport'";
		//}
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		if ($FormNumber == 6)
		{
			$table_css = "class='bg_F6_GraduationReport'";
		}
		else
		{
			$table_css = "class='bg'";
		}
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='1' $table_css>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$graduationReportColspan = $this->GraduationReport["Colspan"];
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		if ($isGraduationReport)
		{
			# 3 empty rows
			//$DetailsTable .= $this->Generate_Empty_Row(3, $graduationReportColspan);
		}
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# check if displaying subject row with all marks equal "N.A."
			if (($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false) && !$isSub)
			{
				$DetailsTable .= "<tr><td class='box_L' width='$space'>&nbsp;<td>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment (never allow subject teacher comment for Escola Tong Nam)
				/*
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext2 border_left'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext2 border_left' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				*/
				$DetailsTable .= "</td></tr>";
				$isFirst = 0;
			}
		}
				
		if ($isGraduationReport)
		{
			# 9 empty rows
			$DetailsTable .= $this->Generate_Empty_Row(4, $graduationReportColspan);
		}
		else
		{
			//G90916: enable ECA for all templates
			// if (!$this->IS_KINDERGARTEN($ClassLevelID) && !$this->IS_PRIMARY($ClassLevelID)) {
				# 2 empty rows
				$colspan = $this->TotalTermColumn + 5;	// total number of term columns + two empty columns + 3 subject columns (Chi, Eng, Space)
				$DetailsTable .= $this->Generate_Empty_Row(2, $colspan);
				
				# ECA row
				$DetailsTable .= $this->genECARow($ReportID, $StudentID, $ClassLevelID);
			//}
			
			# 1 empty row
			$colspan = $this->TotalTermColumn + 5;	// total number of term columns + two empty columns + 3 subject columns (Chi, Eng, Space)
			$DetailsTable .= $this->Generate_Empty_Row(1, $colspan);
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		# Merits Rows
		$DetailsTable .= $this->getMiscTable($ReportID, $StudentID);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
			
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		$SubjectWidth = $eRCTemplateSetting['ColumnWidth']['Subject'];
		
		$MarkWidth = $eRCTemplateSetting['ColumnWidth']['Mark'];
		$space = $this->SpaceWidth;
		
		//$ResultTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='box_L_R'>";
		//$ResultTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		
		if ($this->IS_FORM_SIX($ClassLevelID))
		{
			$graduationReportSpace = $this->GraduationReport["CenterSpace"];
			if ($isGraduationReport)
			{
				$SubjectWidth = "28%";
			}
			else
			{
				$SubjectWidth = "35%";
			}
			
			//  ----------------------------------------- first row start --------------------------------------------------
			$ResultTable .= "	<tr>
									<td width='$space' class='box_L body_title_empty' height='100%'>&nbsp;</td>
									<td class='body_title' height='100%' colspan='3' width='$SubjectWidth'>
										".$eReportCard['Template']['SubjectCh']."
									</td>
							";
							
			if ($isGraduationReport)
			{
				$this->TotalTermColumn = 2;
				$ResultTable .= "	<td class='body_title' height='100%' colspan='1' align='center' width='".$graduationReportSpace."'>
										"."&nbsp;"."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['GraduateExamCh']."
									</td>
								";
			}
			else
			{
				$this->TotalTermColumn = 4;
				$ResultTable .= "	<td class='body_title' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['1stHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['2ndHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['3rdHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['WholeYearCh']."
									</td>
								";
			}
			$ResultTable .= "		<td width='$space' class='box_R body_title_empty' height='100%'>&nbsp;</td>
								</tr>
							";
			
			//  ----------------------------------------- first row end --------------------------------------------------
			
			//  ----------------------------------------- second row start --------------------------------------------------
			$ResultTable .= "	<tr>
									<td width='$space' class='box_L body_subtitle' height='100%'>&nbsp;</td>
									<td class='body_subtitle' height='100%' colspan='3' width='$SubjectWidth' valign='top'>
										".$eReportCard['Template']['SubjectEn']."
									</td>
							";
							
			if ($isGraduationReport)
			{
				$ResultTable .= "	<td class='body_subtitle' height='100%' colspan='1' align='center' width='".$graduationReportSpace."'>
										"."&nbsp;"."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'  width='50%'>
										".$eReportCard['Template']['GraduateExamEn']."
									</td>
								";
			}
			else
			{
				$ResultTable .= "	<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['1stTermEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['2ndTermEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['3rdTermEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['WholeYearEn']."
									</td>
								";
			}
			
			$ResultTable .= "		<td width='$space' class='box_R body_subtitle' height='100%'>&nbsp;</td>
								</tr>
							";
									
			//  ----------------------------------------- second row end --------------------------------------------------
		}
		else if ($this->IS_KINDERGARTEN($ClassLevelID) || $this->IS_PRIMARY($ClassLevelID))
		{
			$this->TotalTermColumn = 7;
			
			//  ----------------------------------------- first row start --------------------------------------------------
			$ResultTable .= "	<tr>
									<td width='$space' class='box_L body_title_empty' height='100%'>&nbsp;</td>
									<td class='body_title' height='100%' colspan='3' width='$SubjectWidth' nowrap>
										".$eReportCard['Template']['SubjectCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stSemesterCh'].$eReportCard['Template']['SemesterTestCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stSemesterCh'].$eReportCard['Template']['SemesterExamCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stSemesterCh'].$eReportCard['Template']['WholeSemesterCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndSemesterCh'].$eReportCard['Template']['SemesterTestCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndSemesterCh'].$eReportCard['Template']['SemesterExamCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndSemesterCh'].$eReportCard['Template']['WholeSemesterCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['WholeYearCh']."
									</td>
									<td width='$space' class='box_R body_title_empty' height='100%'>&nbsp;</td>
								</tr>
								";
			//  ----------------------------------------- first row end --------------------------------------------------
			
			//  ----------------------------------------- second row start --------------------------------------------------
			$ResultTable .= "	<tr>
									<td width='$space' class='box_L body_subtitle' height='100%'>&nbsp;</td>
									<td class='body_subtitle' height='100%' colspan='3' width='$SubjectWidth' valign='top'>
										".$eReportCard['Template']['SubjectEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stSemesterEn']."<br />".$eReportCard['Template']['SemesterTestEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stSemesterEn']."<br />".$eReportCard['Template']['SemesterExamEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".str_replace(' ', '<br />', $eReportCard['Template']['WholeSemesterEn'])."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndSemesterEn']."<br />".$eReportCard['Template']['SemesterTestEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndSemesterEn']."<br />".$eReportCard['Template']['SemesterExamEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".str_replace(' ', '<br />', $eReportCard['Template']['WholeSemesterEn'])."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center' nowrap>
										".str_replace(' ', '<br />', $eReportCard['Template']['WholeYearEn'])."
									</td>
									<td width='$space' class='box_R body_subtitle' height='100%'>&nbsp;</td>
								</tr>
								";
			//  ----------------------------------------- second row end --------------------------------------------------
		}
		else
		{
			$this->TotalTermColumn = 7;
			
			//  ----------------------------------------- first row start --------------------------------------------------
			$ResultTable .= "	<tr>
									<td width='$space' class='box_L body_title_empty' height='100%'>&nbsp;</td>
									<td class='body_title' height='100%' colspan='3' width='$SubjectWidth' nowrap>
										".$eReportCard['Template']['SubjectCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stTermCh'].$eReportCard['Template']['1stHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stTermCh'].$eReportCard['Template']['2ndHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['1stTermCh'].$eReportCard['Template']['WholeTermCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndTermCh'].$eReportCard['Template']['1stHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndTermCh'].$eReportCard['Template']['2ndHalfCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['2ndTermCh'].$eReportCard['Template']['WholeTermCh']."
									</td>
									<td class='body_title' height='100%' colspan='1' align='center' nowrap>
										".$eReportCard['Template']['WholeYearCh']."
									</td>
									<td width='$space' class='box_R body_title_empty' height='100%'>&nbsp;</td>
								</tr>
								";
			//  ----------------------------------------- first row end --------------------------------------------------
			
			//  ----------------------------------------- second row start --------------------------------------------------
			$ResultTable .= "	<tr>
									<td width='$space' class='box_L body_subtitle' height='100%'>&nbsp;</td>
									<td class='body_subtitle' height='100%' colspan='3' width='$SubjectWidth' valign='top'>
										".$eReportCard['Template']['SubjectEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['1stTermEn']."<br />".$eReportCard['Template']['1stHalfEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['1stTermEn']."<br />".$eReportCard['Template']['2ndHalfEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".str_replace(' ', '<br />', $eReportCard['Template']['WholeTermEn'])."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['2ndTermEn']."<br />".$eReportCard['Template']['1stHalfEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".$eReportCard['Template']['2ndTermEn']."<br />".$eReportCard['Template']['2ndHalfEn']."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".str_replace(' ', '<br />', $eReportCard['Template']['WholeTermEn'])."
									</td>
									<td class='body_subtitle' height='100%' colspan='1' align='center'>
										".str_replace(' ', '<br />', $eReportCard['Template']['WholeYearEn'])."
									</td>
									<td width='$space' class='box_R body_subtitle' height='100%'>&nbsp;</td>
								</tr>
								";
			//  ----------------------------------------- second row end --------------------------------------------------
		}
				
		
		return array($ResultTable, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$isF6GraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		if ($this->IS_FORM_SIX($ClassLevelID))
 		{
	 		if ($isF6GraduationReport)
	 		{
		 		$firstSuffix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		 		$secondSuffix = "";
	 		}
	 		else
	 		{
		 		$firstSuffix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		 		$secondSuffix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	 		}
 		}
 		else
 		{
	 		$firstSuffix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	 		$secondSuffix = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
 		}
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		// no border for this school
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH").$firstSuffix;
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext2 {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2' nowrap>$v".$secondSuffix."</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID	= $ReportSetting['ClassLevelID'];
		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='box_L_R'>";
		$SignatureTable .= "<table width='100%'  border='0' cellpadding='0' cellspacing='0' class='box_B'>
    		          		<tr valign='middle' class='remarks_title'>";
				
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			
			// [2017-0213-1134-48206]
			/*
			if ($SettingID == "GeneralInstruction")
			{
				$SignatureTable .= "<td class='boxbg_T_B'>".$Title."</td>";
			}
			else if($this->IS_KINDERGARTEN($ClassLevelID) && $SettingID == "ClassTeacher")
			{
				$SignatureTable .= "<td width='128' align='center' class='boxbg_T_B'>".$Title."</td>";
			}
			else
			{
				$SignatureTable .= "<td width='85' align='center' class='boxbg_T_B'>".$Title."</td>";
			}
			*/
			if($this->IS_KINDERGARTEN($ClassLevelID))
			{
				if ($SettingID == "GeneralInstruction") {
					$SignatureTable .= "<td class='boxbg_T_B'>".$Title."</td>";
				}
				else if($SettingID == "ClassTeacher") {
					$SignatureTable .= "<td width='128' align='center' class='boxbg_T_B'>".$Title."</td>";
				}
				else {
					$SignatureTable .= "<td width='85' align='center' class='boxbg_T_B'>".$Title."</td>";
				}
			}
			else
			{
				if ($SettingID == "GeneralInstruction") {
					$SignatureTable .= "<td class='boxbg_T_B' style='padding-left: 5px;'>".$Title."</td>";
				}
				else {
					$SignatureTable .= "<td width='95' align='center' class='boxbg_T_B'>".$Title."</td>";
				}
			}
		}
		
		$SignatureTable .= "<tr>";
		
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			
			if ($SettingID == "GeneralInstruction")
			{
				$SignatureTable .= "<td height='100' class='remarks_padding'>".$this->GENERATE_MARKS_REMARK_TABLE()."</td></table></td></tr></table>";
			}
			else
			{
				$SignatureTable .= "<td height='100' class='box_R'>&nbsp;</td>";
			}
		}
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
        $SchoolType = $this->GET_SCHOOL_LEVEL($ClassLevelID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		$space = $this->SpaceWidth;
		
		# retrieve Student Class
		if($StudentID)
		{
			//include_once($PATH_WRT_ROOT."includes/libuser.php");
			//$lu = new libuser($StudentID);
			//$ClassName 		= $lu->ClassName;
			//$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# get all reports info
		$ReportArray = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$isFormSixWholeTermReport = $this->Is_Form_Six_Whole_Term_Report($ReportID, $ClassLevelID);
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		$displayInfo = $this->DisplayInfo;
		
		for ($i=0; $i<sizeof($ReportArray); $i++)
		{
			$thisReportID = $ReportArray[$i]['ReportID'];
			
			# retrieve result data
			$result = $this->getReportResultScore($thisReportID, 0, $StudentID);
			
			$GrandTotalArr[$i] = $StudentID ? ($result['GrandTotal']==-1)? $this->EmptySymbol : $result['GrandTotal'] : "S";
			$AverageMarkArr[$i] = $StudentID ? ($result['GrandAverage']==-1)? $this->EmptySymbol : $result['GrandAverage'] : "S";
			
	 		$FormPositionArr[$i] = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol : $result['OrderMeritForm']) : $this->EmptySymbol ) : "#";
	 		$ClassPositionArr[$i] = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol : $result['OrderMeritClass']) : $this->EmptySymbol ) : "#";
	  		$ClassNoOfStudentArr[$i] = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol : $result['ClassNoOfStudent']) : $this->EmptySymbol ) : "#";
	  		$FormNoOfStudentArr[$i] = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol : $result['FormNoOfStudent']) : $this->EmptySymbol ) : "#";
	  		
		}
		
		//$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{			
			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
			
			$x .= $this->Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $thisTitleEn, $thisTitleCh, $GrandTotalArr);
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
			$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
			
			$x .= $this->Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $thisTitleEn, $thisTitleCh, $AverageMarkArr);
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
			$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
			
			$x .= $this->Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $thisTitleEn, $thisTitleCh, $ClassPositionArr, $this->OrderDisplayMax);
		}
		
		# Number of Students in Class 
		if($ShowNumOfStudentClass)
		{
			$thisTitleEn = $eReportCard['Template']['ClassNoStudentEn'];
			$thisTitleCh = $eReportCard['Template']['ClassNoStudentCh'];
			
			$x .= $this->Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $thisTitleEn, $thisTitleCh, $ClassNoOfStudentArr);
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
			$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
			
			$x .= $this->Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $thisTitleEn, $thisTitleCh, $FormPositionArr, $this->OrderDisplayMax);
		}
		
		# Number of Students in Form 
		if($ShowNumOfStudentForm)
		{
			$thisTitleEn = $eReportCard['Template']['FormNoStudentEn'];
			$thisTitleCh = $eReportCard['Template']['FormNoStudentCh'];
			
			$x .= $this->Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $thisTitleEn, $thisTitleCh, $FormNoOfStudentArr);
		}
		
		
		##################################################################################
		# CSV related
		##################################################################################
		# build data array
		$ary = array();
		//$csvType = $this->getOtherInfoType();
		
		for ($i=0; $i<sizeof($ReportArray); $i++)
		{
			$thisReportID = $ReportArray[$i]['ReportID'];
			
			# retrieve the latest Term
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($thisReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			//$latestTerm++;			
			
			/*
			
			# build data array
			$ary = array();
			$csvType = $this->getOtherInfoType();
			foreach($csvType as $k=>$Type)
			{
				$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
				if(!empty($csvData)) 
				{
					foreach($csvData as $RegNo=>$data)
					{
						if($RegNo == $WebSamsRegNo)
						{
		 					foreach($data as $key=>$val)
			 					$ary[$key] = $val;
						}
					}
				}
			}
			*/			
			
			$OtherInfoDataAry = $this->getReportOtherInfoData($thisReportID, $StudentID);
			$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
			
			# consolidate data into arrays
			$ConductGradeArr[$i] = $ary['Conduct'];
			$AbsentArr[$i] = $ary['Days Absent'];
			$LateArr[$i] = $ary['Time Late'];
			$PolitenessArr[$i] = $ary['Politeness'];
			$DisciplineArr[$i] = $ary['Discipline'];
			$ConcentrationArr[$i] = $ary['Concentration'];
			$TidinessArr[$i] = $ary['Tidiness'];
		}
		
		# Conduct 
		$x .= "<tr>";
		$x .= "<td class='box_L' height='{$LineHeight}' width='$space'>&nbsp;</td>";		
		$counter=0;
		foreach($displayInfo as $k=>$v)
 		{
	 		$thisColspan = ($counter==0)? 2 : 1;
	 		$counter++;
	 		$infoEn = $eReportCard['Template']['ConductEn'];
			$infoCh = $eReportCard['Template']['ConductCh'];
			 				
			$v = str_replace("InfoCh", $infoCh, $v);
			$v = str_replace("InfoEn", $infoEn, $v);
				 				
 			$x .= "<td class='tabletext2 {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='$thisColspan'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2'>$v</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
 		} 		
 		if ($isGraduationReport)
 		{
	 		$GraduationReportIndex = 3;		// 4th Term Report
	 		$thisDisplay = $ConductGradeArr[$GraduationReportIndex];
	 		$thisDisplay = ($thisDisplay=="")? $this->EmptySymbol : $thisDisplay;
	 		
	 		$x .= "<td class='tabletext2' align='center' height='{$LineHeight}'>&nbsp;</td>";
	 		$x .= "<td class='tabletext2' align='center' height='{$LineHeight}'>".$thisDisplay."</td>";
		}
		else
		{
			for ($i=0; $i<sizeof($ReportArray); $i++)
			{
				if ($this->IS_FORM_SIX($ClassLevelID))
				{
					if ($i==3)
					{
						# conduct grade of whole term is equal to that of 3rd term
						$thisConductGrade = $ConductGradeArr[$i-1];
					}
					else
					{
						$thisConductGrade = $ConductGradeArr[$i];
					}
				}
				else
				{
					if ($i==2 || $i==5) # Whole Term
					{
						# conduct grade of whole term is equal to the 2nd half of that term
						$thisConductGrade = $ConductGradeArr[$i-1];
					}
					elseif ($i==6) # Whole Year
					{
						# conduct grade of whole year is equal to that of 2nd term 2nd half
						$thisConductGrade = $ConductGradeArr[$i-2];

						// [2020-0615-0944-47066] Request to display conduct result of 1st semester to whole year column (same as $i==2)
						if($this->schoolYear == '2019' && $SchoolType == 'K') {
                            $thisConductGrade = $ConductGradeArr[1];
                        }
					}
					else
					{
						$thisConductGrade = $ConductGradeArr[$i];
					}
				}
							
				if ($isFormSixWholeTermReport && $i==2)
				{
					$x .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				}
				
				$thisConductGrade = ($thisConductGrade=="")? $this->EmptySymbol : $thisConductGrade;			
				$x .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $thisConductGrade ."</td>";
			}		
			
			if (!$isFormSixWholeTermReport)
			{
				$numEmpty = $this->TotalTermColumn - sizeof($ReportArray);
				for ($i=0; $i<$numEmpty; $i++)
				{
					$x .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				}
			}
		}
		
		$x .= "<td class='box_R'>&nbsp</td>\n";
		$x .= "</tr>";
		
		# Show Politeness, Discipline, Concentration and Tidiness for kindergarten students
		if ($this->IS_KINDERGARTEN($ClassLevelID))
		{
			# Politeness 
			$thisTitleEn = $eReportCard['Template']['PolitenessEn'];
			$thisTitleCh = $eReportCard['Template']['PolitenessCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $PolitenessArr, $isAccumulative=0, $needBg=0, $isTop=1, $isBottom=0);
			
			# Discipline 
			$thisTitleEn = $eReportCard['Template']['DisciplineEn'];
			$thisTitleCh = $eReportCard['Template']['DisciplineCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $DisciplineArr, $isAccumulative=0, $needBg=0, $isTop=0, $isBottom=0);
			
			# Concentration 
			$thisTitleEn = $eReportCard['Template']['ConcentrationEn'];
			$thisTitleCh = $eReportCard['Template']['ConcentrationCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $ConcentrationArr, $isAccumulative=0, $needBg=0, $isTop=0, $isBottom=0);
			
			# Tidiness 
			$thisTitleEn = $eReportCard['Template']['TidinessEn'];
			$thisTitleCh = $eReportCard['Template']['TidinessCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $TidinessArr, $isAccumulative=0, $needBg=0, $isTop=0, $isBottom=0);
		}

		# Days Absent 
		$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
		$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
		$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $AbsentArr, $isAccumulative=1, $needBg=0, $isTop=0, $isBottom=0);
		
		# Times Late 
		$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
		$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
		$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $LateArr, $isAccumulative=1, $needBg=0, $isTop=0, $isBottom=0);
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		# get all reports info
		$ReportArray = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$isFormSixWholeTermReport = $this->Is_Form_Six_Whole_Term_Report($ReportID, $ClassLevelID);
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			
		# show the overall result of this term only if it is F6 graduation report
		$graduationReportColspan = $this->GraduationReport["Colspan"];
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		if ($isGraduationReport)
		{
			$ReportArray = array(array("ReportID"=>$ReportID));
		}
		
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		# check if need 插班生
		$IsAllDisplaySlashesArr = $this->Is_All_Mark_Empty_NA_Exempted($ReportArray, $StudentID, $ClassLevelID);

        # check if left student   [2019-0708-1145-44066]
        if($StudentID) {
            include_once($PATH_WRT_ROOT."includes/libuser.php");
            $lu = new libuser($StudentID);
            $isLeftStudent = $lu->RecordType == 2 && $lu->RecordStatus == 3;
        }

        # display "----" in Whole Year Result if left student   [2019-0708-1145-44066]
        $displaySlashesReportAssoc = array();
        for ($i=0; $i<sizeof($ReportArray); $i++)
        {
            $thisReportID = $ReportArray[$i]['ReportID'];
            $displaySlashesReportAssoc[$thisReportID] = $isLeftStudent && $this->Is_Form_Whole_Year_Report($thisReportID, $ClassLevelID);
        }

		foreach($SubjectArray as $SubjectID => $SubjectName)
		{
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			// check if it is a parent subject, if yes find info of its components subjects
			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
			if (!$isSub) {
				$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
			}
			
			# define css ( no border for this school)
			//$css_border_top = ($isSub)? "" : "border_top";
	
			# Retrieve Subject Scheme ID & settings
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			$isAllNA = true;
			
			if ($isGraduationReport)
			{
				$x[$SubjectID] .= "<td class='{$css_border_right}'> &nbsp; </td>";
			}
			
			# Show Subject Overall of each report
			for ($i=0; $i<sizeof($ReportArray); $i++)
			{
				if ($isFormSixWholeTermReport && $i==2)
				{
					$x[$SubjectID] .= "<td class='{$css_border_right}' align='center'>".$this->EmptySymbol."</td>";
				}
				
				$thisReportID = $ReportArray[$i]['ReportID'];
				
				$thisReportSetting	= $this->returnReportTemplateBasicInfo($thisReportID);
				$thisSemID			= $thisReportSetting['Semester'];
		 		$thisReportType 	= $thisSemID == "F" ? "W" : "T";
		 		
				# retrieve marks
				$MarksAry = $this->getMarks($thisReportID, $StudentID);
				
				$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
				
				# for preview purpose
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}
				
				// append the ".0" for integral marks
				if ($thisReportType == "W")
				{
					$thisMark = my_round($thisMark, $eRCTemplateSetting['ConsolidatedSubjectTotalRounding']);
				}
				
				#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
				
				if ($thisMark != "N.A.")
				{
					$isAllNA = false;
				}
				
				$thisMark = ($thisMark=="")? $this->EmptySymbol : $thisMark;
				
				# check special case
				list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);

				if ($IsAllDisplaySlashesArr[$thisReportID] == true || (isset($displaySlashesReportAssoc[$thisReportID]) && $displaySlashesReportAssoc[$thisReportID] == true))
				{
					// [2016-0420-0949-55207] always display "----" for 插班生		[removed]
					$thisMarkDisplay = ($thisReportType=="T")? "/" : $this->EmptySymbol;
					//$thisMarkDisplay = $this->EmptySymbol;
				}
				elseif ($thisMark == "*" || $thisMark == "/")
				{
					$thisMarkDisplay = "免修";
				}
				else
				{
					if($needStyle)
					{
						if($thisSubjectWeight > 0)
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
							
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$thisReportID);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
				}
				$thisMarkDisplay = ($thisMarkDisplay==NULL)? $this->EmptySymbol : $thisMarkDisplay;
					
				$x[$SubjectID] .= "<td class='{$css_border_right}'>";
				$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
					$x[$SubjectID] .= "<td class='tabletext2' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
					/*
					if($ShowSubjectFullMark)
					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext2' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					*/
				$x[$SubjectID] .= "</tr></table>";
				$x[$SubjectID] .= "</td>";
			}
			
			if (!$isGraduationReport && !$isFormSixWholeTermReport)
			{
				$numEmptyColumn = $this->TotalTermColumn - sizeof($ReportArray);
				for ($i=0; $i<$numEmptyColumn; $i++)
				{
					$x[$SubjectID] .= "<td class='{$css_border_right}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext2' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $this->EmptySymbol ."</td>";
						/*
						if($ShowSubjectFullMark)
						{
		  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
		  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
		  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext2' align='center' width='50%'>(". $thisFullMark .")</td>";
						}
						*/
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
			}
			
			$x[$SubjectID] .= "<td class='box_R'>&nbsp</td>\n";
			 
			$isFirst = 0;
			
			# construct an array to return
			$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
			$returnArr['isAllNA'][$SubjectID] = $isAllNA;
		}
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$LineHeight 				= $ReportSetting['LineHeight'];
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# get all reports info
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		$ReportArray = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		for ($i=0; $i<sizeof($ReportArray); $i++)
		{
			$thisReportID = $ReportArray[$i]['ReportID'];
			$ReportIDArr[$i] = $thisReportID;
		}
		$ReportKey = array_search($ReportID, $ReportIDArr);
		
		# display format
		$displayInfo = array("InfoCh", "InfoEn");

		# retrieve Student Class
		if($StudentID)
		{
			/*
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
			*/
		}
		
		
		# build data array
		# get class teacher comments
		# if the report is Whole Term report => get the comment from 2nd Half of the term
		# if the report is Whole Year => get the comment from the 2nd Term
		if ($ReportKey == 2 || $ReportKey == 5)
		{
			$commentReportID = $ReportIDArr[$ReportKey-1];
		}
		else if ($ReportKey == 6)
		{
			$commentReportID = $ReportIDArr[4];
		}
		else
		{
			$commentReportID = $ReportID;
		}
		$CommentAry = $this->returnSubjectTeacherComment($commentReportID, $StudentID);
		$ClassTeacherComment = $CommentAry[0];
						
		# get CSV data	
		$ary = array();
		$csvType = $this->getOtherInfoType();
		for ($i=0; $i<sizeof($ReportArray); $i++)
		{
			$thisReportID = $ReportArray[$i]['ReportID'];
						
			# retrieve the latest Term
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($thisReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			//$latestTerm++;			
			
			# build data array
			$ary = array();
			/*
			$csvType = $this->getOtherInfoType();
			foreach($csvType as $k=>$Type)
			{
				$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
				if(!empty($csvData)) 
				{
					foreach($csvData as $RegNo=>$data)
					{
						if($RegNo == $WebSamsRegNo)
						{
		 					foreach($data as $key=>$val)
			 					$ary[$key] = $val;
						}
					}
				}
			}
			*/
			
			$OtherInfoDataAry = $this->getReportOtherInfoData($thisReportID, $StudentID);
			$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
			
			# consolidate data into arrays
			$MeirtsArr[$i] = $ary['Merits'];
			$MinorCreditArr[$i] = $ary['Minor Credit'];
			$MajorCreditArr[$i] = $ary['Major Credit'];
			$DemeritsArr[$i] = $ary['Demerits'];
			$MinorFaultArr[$i] = $ary['Minor Fault'];
			$MajorFaultArr[$i] = $ary['Major Fault'];
			$AwardsArr[$i] = $ary['Awards'];
			$RemarksArr[$i] = $ary['Remark'];			
		}
		
		if ($isGraduationReport)
		{
			# 4 empty rows
			$graduationReportColspan = $this->GraduationReport["Colspan"];
			$x .= $this->Generate_Empty_Row(2, $graduationReportColspan);
		}
		else
		{
			# Merits
			$thisTitleEn = $eReportCard['Template']['MeritsEn'];
			$thisTitleCh = $eReportCard['Template']['MeritsCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $MeirtsArr, 1, 1, 1);
			
			# Minor Credit
			$thisTitleEn = $eReportCard['Template']['MinorCreditEn'];
			$thisTitleCh = $eReportCard['Template']['MinorCreditCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $MinorCreditArr, 1, 1);
			
			# Major Credit
			$thisTitleEn = $eReportCard['Template']['MajorCreditEn'];
			$thisTitleCh = $eReportCard['Template']['MajorCreditCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $MajorCreditArr, 1, 1);
			
			# Demerits
			$thisTitleEn = $eReportCard['Template']['DemeritsEn'];
			$thisTitleCh = $eReportCard['Template']['DemeritsCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $DemeritsArr, 1, 1);
			
			# Minor Fault
			$thisTitleEn = $eReportCard['Template']['MinorFaultEn'];
			$thisTitleCh = $eReportCard['Template']['MinorFaultCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $MinorFaultArr, 1, 1);
			
			# Major Fault
			$thisTitleEn = $eReportCard['Template']['MajorFaultEn'];
			$thisTitleCh = $eReportCard['Template']['MajorFaultCh'];
			$x .= $this->Generate_CSV_Info_Row($ClassLevelID, $ReportID, $thisTitleEn, $thisTitleCh, $MajorFaultArr, 1, 1, 0, 1);
			
			# Class Teacher Comment
			if ($AllowClassTeacherComment)
			{
				$x .= "<tr>";
				$x .= "<td class='box_L' height='{$LineHeight}' width='$space' style='padding:2px;'>&nbsp;</td>";		
				$counter=0;
				foreach($displayInfo as $k=>$v)
		 		{
			 		$thisColspan = ($counter==0)? 2 : 1;
			 		$counter++;
			 		$infoEn = $eReportCard['Template']['ClassTeacherCommentEn'];
					$infoCh = $eReportCard['Template']['ClassTeacherCommentCh'];
					 				
					$v = str_replace("InfoCh", $infoCh, $v);
					$v = str_replace("InfoEn", $infoEn, $v);
						 				
		 			$x .= "<td class='tabletext2  {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='$thisColspan'>";
					$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
					$x .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2' nowrap>$v</td>";
					$x .= "</tr></table>";
					$x .= "</td>";
		 		} 		
		 		
		 		//2012-0611-1016-34132
		 		//$thisComment = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".nl2br($ClassTeacherComment);
		 		$thisComment = nl2br($ClassTeacherComment);
		 		
		 		//$x .= "<td width='30px'>&nbsp</td>\n";
		 		$colspan = $this->TotalTermColumn;
		 		
		 		$x .= "<td class='tabletext2' height='{$LineHeight}' colspan='$colspan' style='padding-left:20px;'>". $thisComment ."</td>";
				$x .= "<td class='box_R '>&nbsp</td>\n";
				$x .= "</tr>";
			}
		}
		
		# Awards
		$x .= "<tr>";
		$x .= "<td class='box_L' height='{$LineHeight}' width='$space' style='padding:2px;'>&nbsp;</td>";		
		$counter=0;
		foreach($displayInfo as $k=>$v)
 		{
	 		$thisColspan = ($counter==0)? 2 : 1;
	 		$counter++;
	 		$infoEn = $eReportCard['Template']['AwardsEn'];
			$infoCh = $eReportCard['Template']['AwardsCh'];
			 				
			$v = str_replace("InfoCh", $infoCh, $v);
			$v = str_replace("InfoEn", $infoEn, $v);
				 				
 			$x .= "<td class='tabletext2  {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='$thisColspan'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2'>$v</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
 		} 		
 		
 		if ($this->IS_FORM_SIX($ClassLevelID))
 		{
	 		$Awards = $AwardsArr[$ReportKey];
 		}
 		else
 		{
	 		# if the report is Whole Term report => get the awards info from 2nd Half of the term
			if ($ReportKey == 2 || $ReportKey == 5)
			{
				$Awards = $AwardsArr[$ReportKey-1];
			}
			else
			{
				$Awards = $AwardsArr[$ReportKey];
			}
 		}
 		
 		if (is_array($Awards))
 		{
	 		$thisDisplay = implode("<br>", $Awards);
 		}
 		else
 		{
	 		$thisDisplay = $Awards;
 		}
 		
 		//2012-0611-1016-34132
 		//$thisDisplay = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$thisDisplay;
 		
 		$colspan = $this->TotalTermColumn; 		
 		$x .= "<td class='tabletext2' height='{$LineHeight}' colspan='$colspan' style='padding-left:20px;'>". $thisDisplay ."</td>";
		$x .= "<td class='box_R '>&nbsp</td>\n";
		$x .= "</tr>";
		
		
		# Remarks
		$x .= "<tr>";
		$x .= "<td class='box_L' height='{$LineHeight}' width='$space' style='padding:2px;'>&nbsp;</td>";		
		$counter=0;
		foreach($displayInfo as $k=>$v)
 		{
	 		$thisColspan = ($counter==0)? 2 : 1;
	 		$counter++;
	 		$infoEn = $eReportCard['Template']['RemarksEn'];
			$infoCh = $eReportCard['Template']['RemarksCh'];
			 				
			$v = str_replace("InfoCh", $infoCh, $v);
			$v = str_replace("InfoEn", $infoEn, $v);
				 				
 			$x .= "<td class='tabletext2  {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='$thisColspan'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2'>$v</td>";
			$x .= "</tr></table>";
			$x .= "</td>";
 		} 		
 		
 		if ($this->IS_FORM_SIX($ClassLevelID))
 		{
	 		$Remarks = $RemarksArr[$ReportKey];
 		}
 		else
 		{
	 		# if the report is Whole Term report => get the remarks info from 2nd Half of the term
			if ($ReportKey == 2 || $ReportKey == 5)
			{
				$Remarks = $RemarksArr[$ReportKey-1];
			}
			else
			{
				$Remarks = $RemarksArr[$ReportKey];
			}
 		}
 		
 		if (is_array($Remarks))
 		{
	 		$thisDisplay = implode("<br>", $Remarks);
 		}
 		else
 		{
	 		$thisDisplay = $Remarks;
 		}
 		
 		if ($thisDisplay == "" && $StudentID)
 		{
 			# check if the report use the auto-generated remarks
 			if ($this->Is_Display_Remarks_Of_GraduationExam($ReportID))
	 		{
		 		$thisDisplay = $this->Get_GraduationExam_Remark($ReportID, $StudentID);
	 		}
	 		else if ($this->Is_Display_Remarks_Of_Graduation($ReportID))
	 		{
		 		$thisDisplay = $this->Get_Graduation_Remark($ReportID, $StudentID);
	 		}
	 		else if ($this->Is_Display_Remarks_Of_Promotion($ReportID))
	 		{
		 		$thisDisplay = $this->Get_Promotion_Remark($ReportID, $StudentID);
	 		}
 		}
 		
 		//2012-0611-1016-34132
 		//$thisDisplay = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$thisDisplay;
 		
 		$colspan = $this->TotalTermColumn; 		
 		$x .= "<td class='tabletext2' height='{$LineHeight}' colspan='$colspan' style='padding-left:20px;'>". $thisDisplay ."</td>";
		$x .= "<td class='box_R '>&nbsp</td>\n";
		$x .= "</tr>";
		
		
		if ($isGraduationReport)
		{
			# 2 empty rows
			$graduationReportColspan = $this->GraduationReport["Colspan"];
			$x .= $this->Generate_Empty_Row(2, $graduationReportColspan);
		}
		
		return $x;
	}
	
	function GENERATE_MARKS_REMARK_TABLE()
	{
		$ReturnStr = "
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	                  <tr>
	                    <td><table width='100%' border='0' cellpadding='0' cellspacing='0' class='remarks'>
	                      <tr>
	                        <td width='120'>100 - 滿分 Full Mark </td>
	                        <td >60 - 及格 Pass Mark </td>
	                      </tr>
	                    </table></td>
	                  </tr>
	                  <tr>
	                    <td><table width='100%'  border='0' cellpadding='0' cellspacing='0' class='remarks'>
	                      <tr>
	                        <td width='30'>A+</td>
	                        <td width='50' align='center'>100</td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>B+</td>
	                        <td width='50'>85 - 89 </td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>C+</td>
	                        <td width='50'>70 - 74 </td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>D</td>
	                        <td width='100'>不及格 Fail </td>
	                        <td>&nbsp;</td>
	                      </tr>
	                      <tr>
	                        <td width='30'>A</td>
	                        <td width='50'>95 - 99</td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>B</td>
	                        <td width='50'>80 - 84</td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>C</td>
	                        <td width='50'>65 - 69</td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                      </tr>
	                      <tr>
	                        <td width='30'>A-</td>
	                        <td width='50'>90 - 94 </td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>B-</td>
	                        <td width='50'>75 - 79 </td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>C-</td>
	                        <td width='50'>60 - 64 </td>
	                        <td width='40'>&nbsp;</td>
	                        <td width='30'>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                      </tr>
	                    </table></td>
	                  </tr>
	                  <tr>
	                    <td><table width='100%'  border='0' cellpadding='0' cellspacing='0' class='remarks'>
	                      <tr>
	                        <td width='240'>不及格科目以星號“<span class='remarks_big'>*</span>”表示 </td>
	                        <td><span class='remarks_big'>*</span> Subject(s) of Failure </td>
	                      </tr>
	                    </table></td>
	                  </tr>
	                </table>
	                ";
		return $ReturnStr;
	}
	
	function genECARow($ReportID, $StudentID, $ClassLevelID)
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$LineHeight 				= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		$SubjectWidth = $eRCTemplateSetting['ColumnWidth']['Subject'];
		
		$isFormSixWholeYearReport = $this->Is_Form_Six_Whole_Year_Report($ReportID, $ClassLevelID);
        $isFormSixWholeTermReport = $this->Is_Form_Six_Whole_Term_Report($ReportID, $ClassLevelID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		# retrieve Student Class
		if($StudentID)
		{
			/*
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
			*/
		}
		
		# get all reports info
		$ReportArray = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		
		$ecaTitleArr = array();
		for ($i=0; $i<sizeof($ReportArray); $i++)
		{
			$thisReportID = $ReportArray[$i]['ReportID'];
			
			# retrieve the latest Term
			$latestTerm = "";
			$sems = $this->returnReportInvolvedSem($thisReportID);
			foreach($sems as $TermID=>$TermName)
				$latestTerm = $TermID;
			//$latestTerm++;
			
			# build data array
			$ary = array();
			/*
			$csvType = $this->getOtherInfoType();
			foreach($csvType as $k=>$Type)
			{
				$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
				if(!empty($csvData)) 
				{
					foreach($csvData as $RegNo=>$data)
					{
						if($RegNo == $WebSamsRegNo)
						{
		 					foreach($data as $key=>$val)
			 					$ary[$key] = $val;
						}
					}
				}
			}
			*/
			
			$OtherInfoDataAry = $this->getReportOtherInfoData($thisReportID, $StudentID);
			$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
			$ecaTitle = $ary['ECA'];
			$ecaGrade = $ary['GRADE'];
			
			if (is_array($ecaTitle))
			{
				for ($j=0; $j<sizeof($ecaTitle); $j++)
				{
					$thisECATitle = trim($ecaTitle[$j]);
					$thisECAGrade = $ecaGrade[$j];
					if (!in_array($thisECATitle,$ecaTitleArr))
					{
						$ecaTitleArr[] = $thisECATitle;
					}
					
					# ecaGrade[#reportColumn]['eca title'] = #grade
					$ecaReportGradeArr[$i][$thisECATitle] = $thisECAGrade;
				}
			}
			else
			{
				if (!in_array($ecaTitle,$ecaTitleArr))
				{
					$ecaTitleArr[] = $ecaTitle;
				}
				$ecaReportGradeArr[$i][$ecaTitle] = $ecaGrade;
			}
		}
		
		# Generate Row 
		$space = $this->SpaceWidth;
		$colspan = $this->TotalSubjectColumn + $this->TotalTermColumn; 
		$x .= "<tr>";
			$x .= "<td width='$space' class='box_L'>&nbsp;</td>";
			$x .= "<td class='body_midtitle_line tabletext2' colspan='$colspan'>". $eReportCard['Template']['eca']."</td>";
			$x .= "<td width='$space' class='box_R'>&nbsp;</td>";
		$x .= "</tr>";
				
		$colspanTitle = $this->TotalSubjectColumn;
		
		for ($i=0; $i<sizeof($ecaTitleArr); $i++)
		{
			$thisTitle = $ecaTitleArr[$i];
			
			if ($thisTitle == "")
			{
				continue;
			}
			
			$x .= "<tr>";
			$x .= "<td width='$space' class='box_L' style='padding:2px;'>&nbsp;</td>";
			$x .= "<td class='tabletext2' colspan='$colspanTitle'>". $thisTitle."</td>";
			for ($j=0; $j<sizeof($ReportArray); $j++)
			{
			    // 2018-0712-1037-02066 enable the below commented conditional if the client confirmed to do the case
			    //if ( (!$isFormSixWholeYearReport && ($FormNumber!=6 && ($j==2 || $j==5 || $j==6)) || ($FormNumber==6 && $j==3)) || ($isFormSixWholeYearReport && ($j==3)) )
			    //if ( (!$isFormSixWholeYearReport && (($FormNumber!=6 || $this->IS_PRIMARY($ClassLevelID)) && ($j==2 || $j==5 || $j==6)) || ($FormNumber==6 && $j==3)) || ($isFormSixWholeYearReport && ($j==3)) )
                // [2020-0420-1025-40066]
                if ( (!$isFormSixWholeYearReport && ($FormNumber!=6 && ($j==2 || $j==5 || $j==6)) || ($FormNumber==6 && $j==3)) || ($isFormSixWholeYearReport && ($j==3)) || ($isFormSixWholeTermReport && $j==2) )
			    {
					$thisGrade = $this->EmptySymbol;
				}
				else
				{
					$thisGrade = $ecaReportGradeArr[$j][$thisTitle];
				}
				
				$thisGrade = ($thisGrade=="")? $this->EmptySymbol : $thisGrade;
				$x .= "<td class='tabletext2' align='center'>". $thisGrade."</td>";
			}
			
			$numEmtpyColumn = $this->TotalTermColumn - sizeof($ReportArray);
			
			for ($j=0; $j<$numEmtpyColumn; $j++)
			{
				$x .= "<td class='tabletext2' align='center'>". $this->EmptySymbol."</td>";
			}
			
			$x .= "<td width='$space' class='box_R'>&nbsp;</td>";
			$x .= "</tr>";
		}
				
		return $x;
	}
	
	########### END Template Related
	
	
	####### Generate Row Info functions #######
	function Generate_Footer_Row($ClassLevelID, $StudentID, $ReportID, $TitleEn, $TitleCh, $valueArr, $valueRange="")
	{
	    global $PATH_WRT_ROOT;

		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		# get all reports info
		$ReportArray = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$isFormSixWholeTermReport = $this->Is_Form_Six_Whole_Term_Report($ReportID, $ClassLevelID);
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		$footerRow = "";
		$footerRow .= "<tr>";
		$footerRow .= "<td class='box_L' height='{$LineHeight}' width='$space' style='padding:2px;'>&nbsp;</td>";
		
		$counter=0;
		foreach($this->DisplayInfo as $k=>$v)
 		{
	 		$thisColspan = ($counter==0)? 2 : 1;
	 		$counter++;
	 		$infoEn = $TitleEn;
			$infoCh = $TitleCh;
			 				
			$v = str_replace("InfoCh", $infoCh, $v);
			$v = str_replace("InfoEn", $infoEn, $v);
				 				
 			$footerRow .= "<td class='tabletext2 {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='$thisColspan'>";
			$footerRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$footerRow .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2' nowrap>$v</td>";
			$footerRow .= "</tr></table>";
			$footerRow .= "</td>";
 		}
 		
 		if ($isGraduationReport)
 		{
	 		$GraduationReportIndex = 3;		// 4th Term Report
	 		//$thisDisplay = $valueArr[$GraduationReportIndex];
	 		//$thisDisplay = ($thisDisplay=="")? $this->EmptySymbol : $thisDisplay;
	 		
	 		if ($valueRange!="")
			{
				$thisDisplay = ($valueArr[$GraduationReportIndex] > $valueRange)? $this->EmptySymbol : $valueArr[$GraduationReportIndex];
			}
			else
			{
				$thisDisplay = $valueArr[$GraduationReportIndex];
			}
			
	 		$footerRow .= "<td class='tabletext2' align='center' height='{$LineHeight}'>&nbsp;</td>";
	 		$footerRow .= "<td class='tabletext2' align='center' height='{$LineHeight}'>".$thisDisplay."</td>";
 		}
 		else
 		{
	 		# check if need 插班生
			$IsAllDisplaySlashesArr = $this->Is_All_Mark_Empty_NA_Exempted($ReportArray, $StudentID, $ClassLevelID);

            # check if left student   [2019-0708-1145-44066]
            if($StudentID) {
                include_once($PATH_WRT_ROOT."includes/libuser.php");
                $lu = new libuser($StudentID);
                $isLeftStudent = $lu->RecordType == 2 && $lu->RecordStatus == 3;
            }
			
	 		for ($i=0; $i<sizeof($ReportArray); $i++)
			{
				$thisReportID = $ReportArray[$i]["ReportID"];

				# display "----" in Whole Year Result if left student   [2019-0708-1145-44066]
				$isDisplaySlashesForLeftStudent = $isLeftStudent && $this->Is_Form_Whole_Year_Report($thisReportID, $ClassLevelID);

				if ($IsAllDisplaySlashesArr[$thisReportID] || $isDisplaySlashesForLeftStudent)
				{
					$footerRow .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				}
				else
				{
					if ($isFormSixWholeTermReport && $i==2)
					{
						$footerRow .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
					}
					
					if ($valueRange!="")
					{
						$thisDisplay = ($valueArr[$i] > $valueRange)? $this->EmptySymbol : $valueArr[$i];
					}
					else
					{
						$thisDisplay = $valueArr[$i];
					}
					
					$thisDisplay = ($thisDisplay=="")? $this->EmptySymbol : $thisDisplay;
					$footerRow .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
				}
			}
			
			if (!$isFormSixWholeTermReport)
			{
				$numEmpty = $this->TotalTermColumn - sizeof($ReportArray);
				for ($i=0; $i<$numEmpty; $i++)
				{
					$footerRow .= "<td class='tabletext2' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				}
			}
 		}
 		
		$footerRow .= "<td class='box_R'>&nbsp</td>\n";
		$footerRow .= "</tr>";
		
		return $footerRow;
	}
	
	function Generate_CSV_Info_Row($ClassLevelID, $ReportID, $TitleEn, $TitleCh, $valueArr, $isAccumulative=0, $needBg=0, $isTop=0, $isBottom=0)
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		# get all reports info
		$ReportArray = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$isFormSixWholeTermReport = $this->Is_Form_Six_Whole_Term_Report($ReportID, $ClassLevelID);
		$isGraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		if ($needBg)
		{
			$css = ($isTop)? "boxbg_T" : "boxbg"; 
			$css = ($isBottom)? "boxbg_B" : $css; 
		}
		else
		{
			$css = ($isTop)? "box_T" : "";
		}
		
		$CsvRow = "";
		$CsvRow .= "<tr>";
		$CsvRow .= "<td class='box_L {$css}' height='{$LineHeight}' width='$space' style='padding:2px;'>&nbsp;</td>";		
		$counter=0;
		foreach($this->DisplayInfo as $k=>$v)
 		{
	 		$thisColspan = ($counter==0)? 2 : 1;
	 		$counter++;
	 		$infoEn = $TitleEn;
			$infoCh = $TitleCh;
			 				
			$v = str_replace("InfoCh", $infoCh, $v);
			$v = str_replace("InfoEn", $infoEn, $v);
				 				
 			$CsvRow .= "<td class='tabletext2 {$css} {$css_border_top}' height='{$LineHeight}' valign='middle' colspan='$thisColspan'>";
			$CsvRow .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$CsvRow .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext2'>$v</td>";
			$CsvRow .= "</tr></table>";
			$CsvRow .= "</td>";
 		}
 		
 		if ($isGraduationReport)
 		{
	 		$GraduationReportIndex = 3;		// 4th Term Report
	 		$thisDisplay = $valueArr[$GraduationReportIndex];
	 		$thisDisplay = ($thisDisplay=="")? $this->EmptySymbol : $thisDisplay;
	 		
	 		$CsvRow .= "<td class='tabletext2 $css' align='center' height='{$LineHeight}'>&nbsp;</td>";
	 		$CsvRow .= "<td class='tabletext2 $css' align='center' height='{$LineHeight}'>".$thisDisplay."</td>";
 		}
 		else
 		{
	 		for ($i=0; $i<sizeof($ReportArray); $i++)
			{
				if ($isAccumulative)
				{
					# Days Absent, Times Late, Merits, Minor Merits, Major Merits...
					if ($this->IS_FORM_SIX($ClassLevelID))
					{
						if ($i==3)	# Whole Year
						{
							# No. of Merits is the sum of 1st, 2nd, and the 3rd Term
							$thisDisplay = $valueArr[$i-3] + $valueArr[$i-2] + $valueArr[$i-1];
						}
						else
						{
							$thisDisplay = $valueArr[$i];
						}
					}
					else
					{
						if ($i==2 || $i==5) # Whole Term
						{
							# No. of Merits of whole term is calculated from the 1st half and the 2nd half
							$thisDisplay = $valueArr[$i-2] + $valueArr[$i-1];
						}
						else if ($i==6) # Whole Year
						{
							# No. of Merits is the sum of all terms
							$thisDisplay = $valueArr[0] + $valueArr[1] + $valueArr[3] + $valueArr[4];
						}
						else
						{
							$thisDisplay = $valueArr[$i];
						}
					}
					$thisDisplay = ($thisDisplay==0)? $this->EmptySymbol : $thisDisplay;
				}
				else
				{
					# Politeness, Discipline, Concentration and Tidiness
					if ($i==2 || $i==5 || $i==6)
					{
						$thisDisplay = $this->EmptySymbol;
					}
					else
					{
						$thisDisplay = $valueArr[$i];
					}
					$thisDisplay = ($thisDisplay=="")? $this->EmptySymbol : $thisDisplay;
				}
				
				if ($isFormSixWholeTermReport && $i==2)
				{
					$CsvRow .= "<td class='tabletext2 {$css}' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				}
				
				$CsvRow .= "<td class='tabletext2 {$css}' align='center' height='{$LineHeight}'>". $thisDisplay ."</td>";
			}		
			
			if (!$isFormSixWholeTermReport)
			{
				$numEmpty = $this->TotalTermColumn - sizeof($ReportArray);
				for ($i=0; $i<$numEmpty; $i++)
				{
					$CsvRow .= "<td class='tabletext2 {$css}' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				}
			}
 		}
			
		$CsvRow .= "<td class='box_R {$css}'>&nbsp</td>\n";
		$CsvRow .= "</tr>";
		
		return $CsvRow;
	}
	
	function Generate_Empty_Row($numRow, $colspan)
	{
		$emptyRow = "";
		
		for ($i=0; $i<$numRow; $i++)
		{
			$emptyRow .= "<tr><td class='box_L_R' colspan='".$colspan."' style='padding:2px;'>&nbsp;</td></tr>\n";
		}
		
		return $emptyRow;
	}
	##### End of Generate Row Info functions #####
	
	
	
	####### Report Type checking function #######
	function RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID)
	{			
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
//		$sql = "	
//			SELECT 
//				a.ReportID,
//				a.Semester
//			FROM 
//				$table as a
//				left join YEAR as b on a.ClassLevelID = b.YearID
//			WHERE 
//				a.ClassLevelID = $ClassLevelID
//				AND
//				a.ReportID
//				And
//				a.isMainReport = 1
//			ORDER BY
//				a.Semester
//		";
		$sql = "	
			SELECT 
				a.ReportID,
				a.Semester,
				If (a.Semester = 'F', 999, a.Semester) as DisplayOrder
			FROM 
				$table as a
				left join ACADEMIC_YEAR_TERM as ayt on (a.Semester = ayt.YearTermID)
			WHERE 
				a.ClassLevelID = '$ClassLevelID'
				AND a.ReportID
				And a.isMainReport = 1
			ORDER BY
				DisplayOrder + 0, ayt.TermStart
		";
		$allReportIDArr = $this->returnArray($sql);
		
		if ($this->IS_FORM_SIX($ClassLevelID))
		{
			# discard any report after this ID
			$ReportType = $this->Return_Form_Six_Report_Type($ReportID);
			
			$returnReportIDArr = array();
			if ($ReportType == "T")
			{
				for ($i=0; $i<sizeof($allReportIDArr); $i++)
				{
					$thisReportID = $allReportIDArr[$i]['ReportID'];
					if ($ReportID == $thisReportID)
					{
						$returnReportIDArr[] = $allReportIDArr[$i];
						break;
					}
					else
					{
						$returnReportIDArr[] = $allReportIDArr[$i];
					}
				}
			}
			else if ($ReportType == "WT")	// Whole Term Report
			{
				$returnReportIDArr[] = $allReportIDArr[0];
				$returnReportIDArr[] = $allReportIDArr[1];
				$returnReportIDArr[] = array("ReportID"=>$ReportID, "Semester"=>"F");
			}
			else if ($ReportType == "WY")	// Whole Year Report
			{
				$returnReportIDArr[] = $allReportIDArr[0];
				$returnReportIDArr[] = $allReportIDArr[1];
				$returnReportIDArr[] = $allReportIDArr[2];
				$returnReportIDArr[] = array("ReportID"=>$ReportID, "Semester"=>"F");
			}
			
			return $returnReportIDArr;
		}
		else
		{
			# 3 consolidated reports, need to sort the order
			$consolidatedReportArr = array();
			$sortedTermsReportArr = array();
			for ($i=0; $i<sizeof($allReportIDArr); $i++)
			{
				if ($allReportIDArr[$i]['Semester'] == "F")
				{
					$consolidatedReportArr[] = $allReportIDArr[$i]['ReportID'];
				}
				else
				{
					$sortedTermsReportArr[] = array("ReportID"=>$allReportIDArr[$i]['ReportID'], "Semester"=>$allReportIDArr[$i]['Semester']);
				}
			}
			
			# get column data of consolidated reports
			$SemesterArr = array();
			$SemesterReportIDMapArr = array();
			$NumColArr = array();
			for ($i=0; $i<sizeof($consolidatedReportArr); $i++)
			{
				$thisReportID = $consolidatedReportArr[$i];
				
				$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";		
				$sql = "	
					SELECT 
						LPAD(SemesterNum, 8, '0')	/* pad str so that 0009 is smaller than 0010 in the later php asort */	
					FROM 
						$table as rtc
						inner join ACADEMIC_YEAR_TERM as ayt On (rtc.SemesterNum = ayt.YearTermID)
					WHERE
						ReportID = '$thisReportID'
					Order By
						ayt.TermStart
				";
				$tempArr = $this->returnVector($sql);
				
				$thisSemIDList = implode(",", $tempArr);
				$SemesterArr[$i] = $thisSemIDList;
				$SemesterReportIDMapArr[$thisSemIDList] = $thisReportID;
				$NumColArr[$thisReportID] = sizeof($tempArr);
			}
						
			# sort consolidated reports
			# Because "1st Term Whole Term" and "Whole Year" should have the same starting semester,
			# So, after sorting:
			# [0] = 1st Term Whole Term ReportID
			# [1] = Whole Year ReportID
			# [2] = 2nd Term Whole Term ReportID
			sort($SemesterArr);
			$sortedConsolidatedReportArr = array();			
			for ($i=0; $i<sizeof($SemesterArr); $i++)
			{
				$thisSemIDList = $SemesterArr[$i];
				$thisReportID = $SemesterReportIDMapArr[$thisSemIDList];
				$sortedConsolidatedReportArr[] = $thisReportID;
			}
			
			# consolidate the list of all reports - hardcoded
			$sortedReportIDArr = array();
			
			# 1st Term 1st Half
			if ($sortedTermsReportArr[0]['ReportID'] != "") 
				$sortedReportIDArr[] = $sortedTermsReportArr[0];
			# 1st Term 2nd Half
			if ($sortedTermsReportArr[1]['ReportID'] != "") 
				$sortedReportIDArr[] = $sortedTermsReportArr[1];
			# 1st Term Whole Term
			if ($sortedConsolidatedReportArr[0] != "")
				$sortedReportIDArr[] = array("ReportID"=>$sortedConsolidatedReportArr[0], "Semester"=>"F");
			# 2nd Term 1st Half
			if ($sortedTermsReportArr[2]['ReportID'] != "")
				$sortedReportIDArr[] = $sortedTermsReportArr[2];
			# 2nd Term 2nd Half
			if ($sortedTermsReportArr[3]['ReportID'] != "")
				$sortedReportIDArr[] = $sortedTermsReportArr[3];
			# 2nd Term Whole Term
			if ($sortedConsolidatedReportArr[2] != "")
				$sortedReportIDArr[] = array("ReportID"=>$sortedConsolidatedReportArr[2], "Semester"=>"F");
			# Whole Year
			if ($sortedConsolidatedReportArr[1] != "")
				$sortedReportIDArr[] = array("ReportID"=>$sortedConsolidatedReportArr[1], "Semester"=>"F");
			
			
			# find the current report ID in the array
			# discard any report after this ID
			$returnReportIDArr = array();
			for ($i=0; $i<sizeof($sortedReportIDArr); $i++)
			{
				$thisReportID = $sortedReportIDArr[$i]['ReportID'];
				if ($ReportID == $thisReportID)
				{
					$returnReportIDArr[] = $sortedReportIDArr[$i];
					break;
				}
				else
				{
					$returnReportIDArr[] = $sortedReportIDArr[$i];
				}
			}
			return $returnReportIDArr;	
			
			//return $sortedReportIDArr;	
		}
	}
	
	/*
	 *	return: "T"		Term Report
	 *			"WT"	Whole Term Report
	 *			"WY"	Whole Year Report
	 *			"N"		No match report
	 */
	function Return_Form_Six_Report_Type($ParReportID)
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ParReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		if ($ReportType == "T")
		{
			# term report
			$reportType = "T";
		}
		else
		{
			$reportTemplateColumnDataArr = $this->returnReportTemplateColumnData($ParReportID);
			if (sizeof($reportTemplateColumnDataArr) == 2)
			{
				# whole term report
				$reportType = "WT";
			}
			else if (sizeof($reportTemplateColumnDataArr) == 3)
			{
				# whole year report
				$reportType = "WY";
			}
			else
			{
				# no report matched
				$reportType = "N";
			}
		}
		
		return $reportType;
	}
	
	function Is_Form_Six_Whole_Term_Report($ReportID, $ClassLevelID)
	{
		$ReportType = $this->Return_Form_Six_Report_Type($ReportID);
		$isFormSixWholeTermReport = ($ReportType=="WT" && $this->IS_FORM_SIX($ClassLevelID))? 1 : 0;
		
		return $isFormSixWholeTermReport;
	}
	
	function Is_Form_Six_Whole_Year_Report($ReportID, $ClassLevelID)
	{
		$ReportType = $this->Return_Form_Six_Report_Type($ReportID);
		$isFormSixWholeYearReport = ($ReportType=="WY" && $this->IS_FORM_SIX($ClassLevelID))? 1 : 0;
		
		return $isFormSixWholeYearReport;
	}
	
	function Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID)
	{
		$isFormSix = $this->IS_FORM_SIX($ClassLevelID);
		$numOfTermReport = count($this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID));
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		if ( $isFormSix && $numOfTermReport==4 && $ReportType=="T")
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	function Is_Form_Whole_Year_Report($ReportID, $ClassLevelID)
    {
        // Special Handling for F.6
        $isFormSix = $this->IS_FORM_SIX($ClassLevelID);
        if($isFormSix) {
            return $this->Is_Form_Six_Whole_Year_Report($ReportID, $ClassLevelID);
        }
        $isFormWYReport = false;

        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportSetting['Semester'];
        $ReportType = $SemID == "F" ? "W" : "T";
        if ($ReportType == "W")
        {
            // 4 Columns > Whole Year Report
            $reportTemplateColumnDataArr = $this->returnReportTemplateColumnData($ReportID);
            if (sizeof($reportTemplateColumnDataArr) == 4) {
                $isFormWYReport = true;
            }
        }

        return $isFormWYReport;
    }
	
	# Decide ReExam Report includes:
	# 1. F1, F2, F4, F5 第四段
	# 2. F3 第四段 -> (changed to 學年成績表 as requested [CRM Ref No.: 2009-0604-1650])
	# 3. F6 畢試試
	function Is_Decide_ReExam_Report($ReportID)
	{
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		
		if (!$this->IS_SECONDARY($ClassLevelID))
		{
			# only secondary forms have re-exam
			$isDecide_ReExam_Report = 0;
		}
		else
		{
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			if ($FormNumber == 6)
			{
				# check if graduation report
				if ( $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID) )
				{
					$isDecide_ReExam_Report = 1;
				}
				else
				{
					$isDecide_ReExam_Report = 0;
				}
			}
			else if ($FormNumber == 3)
			{
				# check if 全年成績表
				if ( ($ReportType=="W" && count($ReportArr)==7) )
				{
					$isDecide_ReExam_Report = 1;
				}
				else
				{
					$isDecide_ReExam_Report = 0;
				}
			}
			else
			{
				# check if 4th term
				//if ( ($ReportType=="T" && count($ReportArr)==5) )
				# check if 全年成績表 (東南學校-中學部 - eRC -- comment [CRM Ref No.: 2009-0609-0952])
				if ( ($ReportType=="W" && count($ReportArr)==7) )
				{
					$isDecide_ReExam_Report = 1;
				}
				else
				{
					$isDecide_ReExam_Report = 0;
				}
			}
		}
		
		
		# for dev
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		if ($ClassLevel == "F1" || $ClassLevel == "A2")
		{
			return $isDecide_ReExam_Report = 1;
		}
		
		
		return $isDecide_ReExam_Report;
	}
	####### End of Report Type checking function #######
	
	
	####### Form Checking Functions ########
	function IS_FORM_SIX($ClassLevelID)
	{
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		if ($ClassLevel == "A2" || $ClassLevel == "S6" ||substr($ClassLevel,0,6) == "Form 6")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function IS_KINDERGARTEN($ClassLevelID)
	{
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$SchoolLevel = strtoupper(substr($ClassLevel,0,1));
				
		if ($SchoolLevel == "K")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function IS_PRIMARY($ClassLevelID)
	{
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$SchoolLevel = strtoupper(substr($ClassLevel,0,1));
		
		if ($SchoolLevel == "P")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function IS_SECONDARY($ClassLevelID)
	{
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$SchoolLevel = strtoupper(substr($ClassLevel,0,1));
		
		if ($SchoolLevel == "F" || $SchoolLevel == "S")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function GET_SCHOOL_LEVEL_NAME($ClassLevelID)
	{
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$SchoolLevel = strtoupper(substr($ClassLevel,0,1));
		
		return $SchoolLevel;
	}
	
	function GET_CLASSLEVEL_NAME($ClassLevelID)
	{
		$SchoolLevel = $this->GET_SCHOOL_LEVEL_NAME($ClassLevelID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		return $SchoolLevel.$FormNumber;
	}
	
	function GET_FORM_NUMBER($ClassLevelID)
	{
		if ($this->IS_FORM_SIX($ClassLevelID))
		{
			// Form 6文 or Form 6理 (6 is not the last word => need special handling)
			$FormNumber = 6;
		}
		else
		{
			$ClassLevelName = $this->returnClassLevel($ClassLevelID);
			$FormNumber = substr($ClassLevelName, strlen($ClassLevelName)-1, 1);
		}
		
		return $FormNumber;
	}
	
	function Is_ClassLevel_Has_ReExam_Report($ClassLevelID)
	{
		$ReportTypeArr = $this->GET_REPORT_TYPES($ClassLevelID, '', '', $MainReportOnly=1);
		if(count($ReportTypeArr) > 0){
			for($i=0 ; $i<count($ReportTypeArr) ; $i++){
				if ($this->Is_Decide_ReExam_Report($ReportTypeArr[$i]['ReportID']))
					return true;
			}
		}
		/*
		# for dev
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		if ($ClassLevel == "A1" || $ClassLevel == "A2")
		{
			return true;
		}
		*/
		return false;
	}
	####### End of Form Checking Functions ########
	
	
	####### Is Display Remarks Functions #######
	/*
	 * Only F6 Whole Year Report (學年成績表) shows the remarks about graduation exam
	 */
	function Is_Display_Remarks_Of_GraduationExam($ReportID)
	{
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		
		$isFormSix = $this->IS_FORM_SIX($ClassLevelID);
		$ReportType = $this->Return_Form_Six_Report_Type($ReportID);
		
		/*
		# for dev
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		if ($ClassLevel == "A1" || $ClassLevel == "A2")
		{
			return true;
		}
		*/
		
		if ($isFormSix && $ReportType=="WY")
			return true;
		else
			return false;
	}
	
	/*
	 * F3 Whole Year and F6 Graduation Report displays the remarks about graduation
	 */
	function Is_Display_Remarks_Of_Graduation($ReportID)
	{
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$isForm6_GraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		
		$formNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$numColumn = count($ReportArr);
		
		$isForm3_WholeYearReport = false;
		if ($formNumber==3 && $ReportType=="W" && $numColumn==7)
			$isForm3_WholeYearReport = true;
		
		/*
		# for dev
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		if ($ClassLevel == "A1" || $ClassLevel == "A2")
		{
			return true;
		}
		*/
		
		if ($isForm6_GraduationReport || $isForm3_WholeYearReport)
			return true;
		else
			return false;
		
	}
	
	/*
	 * F1, 2, 4, 5 Whole Year displays the remarks about promotion
	 * # changed to 學年成績表 displays the remarks about promotion (東南學校-中學部 - eRC -- comment [CRM Ref No.: 2009-0609-0952])
	 */
	function Is_Display_Remarks_Of_Promotion($ReportID)
	{
		$displayFormArr = array(1, 2, 4, 5);
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$formNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$numColumn = count($ReportArr);
		
		/*
		# for dev
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		if ($ClassLevel == "A1" || $ClassLevel == "A2")
		{
			return true;
		}
		*/
		
		if (in_array($formNumber, $displayFormArr) && $ReportType=="W" && $numColumn==7)
			return true;
		else
			return false;
			
	}
	####### End of Is Display Remarks Functions #######
	
	
	
	####### Is Determine Remarks Functions #######
	function Is_Determine_Remarks_Of_Graduation($ReportID)
	{
		# F3 4th Term Report and F6 Graduation Report determine the remarks of graduation
		# F3 changed to 學年成績表 as requested [CRM Ref No.: 2009-0604-1650]
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		
		if (!$this->IS_SECONDARY($ClassLevelID))
		{
			# only secondary forms have re-exam
			$returnFlag = 0;
		}
		else
		{
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
			if ($FormNumber == 6)
			{
				# check if graduation report
				$returnFlag = ($this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID))? 1 : 0;
			}
			else if ($FormNumber == 3)
			{
				# check if 4th term
				$returnFlag = ($ReportType=="W" && count($ReportArr)==7)? 1 : 0;
			}
			else
			{
				$returnFlag = 0;
			}
		}
		
		return $returnFlag;
	}
	
	function Is_Determine_Remarks_Of_Promotion($ReportID)
	{
		# F1, F2, F4, F5 4th Term determines the remarks of promotion
		# changed to 學年成績表 determines the remarks of promotion (東南學校-中學部 - eRC -- comment [CRM Ref No.: 2009-0609-0952])
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		
		if (!$this->IS_SECONDARY($ClassLevelID))
		{
			# only secondary forms have re-exam
			$returnFlag = 0;
		}
		else
		{
			$formArr = array(1, 2, 4, 5);
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
			if (in_array($FormNumber, $formArr))
			{
				# check if 4rd term report
				//$returnFlag = ($ReportType=="T" && count($ReportArr)==5)? 1 : 0;
				$returnFlag = ($ReportType=="W" && count($ReportArr)==7)? 1 : 0;
			}
			else
			{
				$returnFlag = 0;
			}
		}
		
		return $returnFlag;
	}
	####### End of Is Determine Remarks Functions #######
	
	
	
	####### Get Remarks Functions #######
	
	/*
	 *	Generate remarks bases on F6 3rd Term result
	 *	Display remarks in F6 Whole Year Report
	 *
	 *  ##### Changed on 24th Apr 2009 東南學校-中學部 - 高三補考名單 + 自動評語 problem [CRM Ref No.: 2009-0424-1012]
	 *	New: Generate remarks bases on F6 Whole Year Report
	 */
	function Get_GraduationExam_Remark($ReportID, $StudentID)
	{
		global $eReportCard;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		//$targetReportID = $ReportArr[2]['ReportID'];	// 3rd Term ReportID
		$targetReportID = $ReportArr[3]['ReportID'];	// Whole Year Report
		
		$failedSubjectInfoArr = $this->Get_Failed_Subject_InfoArr($targetReportID, $StudentID);
		
		if (count($failedSubjectInfoArr)==0)
		{
			# All Passed => can take the graduation exam
			$thisRemark = $eReportCard['Template']['RemarkGraduationExam'][0];
		}
		else if ( (count($failedSubjectInfoArr) >= 4) || $this->Has_Mark_Less_Than_School_Requirement($failedSubjectInfoArr))
		{
			# 4 or more subjects failed or 1 subject less than 40 => Retained
			$thisRemark = $eReportCard['Template']['RemarkGraduationExam'][2];
		}
		else
		{
			# 1-3 subjects failed, but still can take the graduation exam
			$thisRemark = $eReportCard['Template']['RemarkGraduationExam'][1];
		}
		
		return $thisRemark;
	}
	
	/*
	 *	Generate remarks based on F6 Graduation Report for F6 Graduation Report 
	 *	OR
	 *	Generate remarks based on F3 4th Term Report for F3 Whole Year Report
	 */
	function Get_Graduation_Remark($ReportID, $StudentID)
	{
		global $eReportCard, $intranet_root, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$isForm6_GraduationReport = $this->Is_Form_Six_Graduation_Report($ClassLevelID, $ReportID);
		//$targetReportID = ($isForm6_GraduationReport)? $ReportID : $ReportArr[4]['ReportID'];
		$targetReportID = $ReportID;
		
		$failedSubjectInfoArr = $this->Get_Failed_Subject_InfoArr($targetReportID, $StudentID);
		
		if (count($failedSubjectInfoArr)==0)
		{
			# All Passed
			$thisRemark = $eReportCard['Template']['RemarkGraduation'][0];
		}
		else if ( (count($failedSubjectInfoArr) >= 4) || $this->Has_Mark_Less_Than_School_Requirement($failedSubjectInfoArr) )
		{
			# 4 or more subjects failed or 1 subject less than 40 => Retained
			$thisRemark = $eReportCard['Template']['RemarkGraduation'][1];
		}
		else
		{
			# 1-3 subjects failed => take the graduation exam => Remarks depends on the re-exam CSV score
			$StudentScoreInfoArr = $this->Get_Student_ReExam_ScoreInfoArr($targetReportID, $StudentID);
			$thisRemark = $this->Get_Graduation_ReExam_Remark($ClassLevelID, $StudentScoreInfoArr, $targetReportID);
		}
		
		return $thisRemark;
	}
	
	/*
	 *	Generate remarks based on Whole Year Report of F1, 2, 4, 5
	 */
	function Get_Promotion_Remark($ReportID, $StudentID)
	{
		global $eReportCard, $intranet_root, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		
		$ReportArr = $this->RETURN_DISPlAY_REPORT_LIST($ClassLevelID, $ReportID);
		$targetReportID = $ReportArr[6]['ReportID'];
		
		$failedSubjectInfoArr = $this->Get_Failed_Subject_InfoArr($targetReportID, $StudentID);
		
		if (count($failedSubjectInfoArr)==0)
		{
			# All Passed
			$thisRemark = $eReportCard['Template']['RemarkPromotion'][0];
		}
		else if ( (count($failedSubjectInfoArr) >= 4) || $this->Has_Mark_Less_Than_School_Requirement($failedSubjectInfoArr) )
		{
			# 4 or more subjects failed or 1 subject less than 40 => Retained
			$thisRemark = $eReportCard['Template']['RemarkPromotion'][1];
		}
		else
		{
			# 1-3 subjects failed => take the graduation exam => Remarks depends on the re-exam CSV score
			$StudentScoreInfoArr = $this->Get_Student_ReExam_ScoreInfoArr($targetReportID, $StudentID);
			$thisRemark = $this->Get_Promotion_ReExam_Remark($ClassLevelID, $StudentScoreInfoArr, $targetReportID);
		}
		
		return $thisRemark;
	}
	
	function Has_Mark_Less_Than_School_Requirement($InfoArr=array())
	{
		$minimunMark = 40;
		
		$hasMarkLessThanMinimum = false;
		
		foreach ($InfoArr as $key => $valueArr)
		{
			$thisMarks = $valueArr["Mark"];
			
			if ($thisMarks < $minimunMark)
			{
				$hasMarkLessThanMinimum = true;
				break;
			}
		}
		
		return $hasMarkLessThanMinimum;
	}
	
	function Get_Failed_Subject_InfoArr($ReportID, $StudentID, $isAssociative=0)
	{
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		# retrieve subjects
		$SubjectArr = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArr) > 0)
			foreach($MainSubjectArr as $MainSubjectIDArr[] => $MainSubjectNameArr[]);
			
		$failedSubjectInfoArr = array();
		foreach($SubjectArr as $SubjectID => $SubjectName)
		{
			if (in_array($SubjectID, $MainSubjectIDArr)!=true)
			{
				# Do no consider component subjects
				continue;
			}
			
			# Retrieve Subject Scheme ID & settings
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
			$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0]["Grade"] : ""; 
			
			$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
			
			# check special case
			list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
			$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
			
			if ($thisNature == "Fail" && is_numeric($thisMark))
			{
				if ($isAssociative)
				{
					$failedSubjectInfoArr[$SubjectID] = array(
												"SubjectName" 	=> $SubjectName,
												"Mark" 			=>$thisMark
												);
				}
				else
				{
					$failedSubjectInfoArr[] = array(
												"SubjectID" 	=> $SubjectID,
												"SubjectName" 	=> $SubjectName,
												"Mark" 			=>$thisMark
												);
				}
			}
		}
		
		return $failedSubjectInfoArr;
	}
	
	function Get_Student_ReExam_ScoreInfoArr($ReportID, $StudentID)
	{
		global $eReportCard, $intranet_root, $PATH_WRT_ROOT;
		
		$StudentScoreInfoArr = array();
		
		$ClassInfoArr = $this->Get_ClassInfoArr_By_StudentID($StudentID);
		//list($ClassID, $ClassName, $ClassNumber) = $ClassInfoArr[0];
		$ClassID = $ClassInfoArr[0]['ClassID'];
		$ClassName = $ClassInfoArr[0]['ClassName'];
		$ClassNumber = $ClassInfoArr[0]['ClassNumber'];
			
		## check if the re-exam score csv is uploaded
		$targetFolderPath = $intranet_root."/file/reportcard2008/".$this->schoolYear."/re_exam";
		$targetFileName = $ReportID."_".$ClassID."_unicode.csv";
		$fileName = $targetFolderPath."/".$targetFileName;
		
		if (file_exists($fileName)) {
			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			$limport = new libimporttext();
			
			$data = $limport->GET_IMPORT_TXT($fileName);
			$header = array_shift($data);
			if (sizeof($data) > 0) 
			{
				for($i=0; $i<sizeof($data); $i++) 
				{
					for($j=0; $j<sizeof($header); $j++) 
					{
						$thisHeader = $header[$j];
						
						if ($j < 3)
						{
							# ClassName, ClassNumber, StudentName
							$StudentScoreInfoArr[$i][$thisHeader] = $data[$i][$j];
						}
						else
						{
							$thisScore = $data[$i][$j];
							
							if ( strtoupper(trim($thisScore)) == "N/A" )
								continue;
							
							# $thisHeader = [SubjectNameEn_SubjectNameCh_SubjectID]
							$thisSubjectInfoArr = explode("_", $thisHeader);
							list($thisSubjectNameEn, $thisSubjectNameCh, $thisSubjectID) = $thisSubjectInfoArr;
							
							$StudentScoreInfoArr[$i]['SubjectInfoArr'][$thisSubjectID]['Mark'] = $thisScore;
							$StudentScoreInfoArr[$i]['SubjectInfoArr'][$thisSubjectID]['SubjectNameEn'] = $thisSubjectNameEn;
							$StudentScoreInfoArr[$i]['SubjectInfoArr'][$thisSubjectID]['SubjectNameCh'] = $thisSubjectNameCh;
						}
						
					}
				}
			}
		}
		
		$returnStudentScoreInfoArr = array();
		$numOfStudentScoreInfo = count($StudentScoreInfoArr);
		for ($i=0; $i<$numOfStudentScoreInfo; $i++)
		{
			$thisClassName = $StudentScoreInfoArr[$i]['Class Name'];
			$thisClassNumber = "0".$StudentScoreInfoArr[$i]['Class Number'];
			$thisStudentID = $this->Get_StudentID_By_ClassName_And_ClassNumber($thisClassName, $thisClassNumber);
						
			if ($thisStudentID == $StudentID)
			{
				$returnStudentScoreInfoArr = $StudentScoreInfoArr[$i]['SubjectInfoArr'];
				break;
			}
		}
		
		return $returnStudentScoreInfoArr;
	}
	
	function Get_Graduation_ReExam_Remark($ClassLevelID, $ScoreInfoArr=array(), $ReportID='')
	{
		global $eReportCard;
		
		if (count($ScoreInfoArr)==0)
			return "";
		
		## analyst the re-exam result to separate pass and failed subjects
		$SubjectPassedArr = array();
		$SubjectFailedArr = array();
		foreach($ScoreInfoArr as $SubjectID => $SubjectScoreInfoArr)
		{
			# Retrieve Subject Scheme ID & settings
			$thisMark = $SubjectScoreInfoArr['Mark'];
			$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
			if ($thisNature == "Fail")
			{
				$SubjectFailedArr[] = $SubjectScoreInfoArr['SubjectNameCh'];
			}
			else
			{
				$SubjectPassedArr[] = $SubjectScoreInfoArr['SubjectNameCh'];
			}
		}
				
		## build remark text
		$thisRemark = "";
		$SubjectPassedText = implode($eReportCard['Template']['SubjectSeparator'], $SubjectPassedArr);
		$SubjectFailedText = implode($eReportCard['Template']['SubjectSeparator'], $SubjectFailedArr);
		
		if (count($SubjectFailedArr) == 0)
		{
			# All passed => can graduate
			$thisRemark = $eReportCard['Template']['RemarkGraduation'][2];
		}
		else
		{
			# Re-exam failed => cannot graduate
			$thisRemark = (count($SubjectPassedArr) > 0)? $eReportCard['Template']['RemarkGraduation'][3][0] : "";
			$thisRemark .= $eReportCard['Template']['RemarkGraduation'][3][1];
		}
		
		$thisRemark = str_replace("<!--PassSubjects-->", $SubjectPassedText, $thisRemark);
		$thisRemark = str_replace("<!--FailedSubjects-->", $SubjectFailedText, $thisRemark);
		
		return $thisRemark;
	}
	
	function Get_Promotion_ReExam_Remark($ClassLevelID, $ScoreInfoArr=array(), $ReportID='')
	{
		global $eReportCard;
		
		if (count($ScoreInfoArr)==0)
			return "";
			
		$MainSubjectNameArr = array("語文", "英文", "數學");
		
		## analyst the re-exam result to separate pass and failed subjects
		$SubjectPassedArr = array();
		$SubjectFailedArr = array();
		foreach($ScoreInfoArr as $SubjectID => $SubjectScoreInfoArr)
		{
			# Retrieve Subject Scheme ID & settings
			$thisMark = $SubjectScoreInfoArr['Mark'];
			$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
			
			if ($thisNature == "Fail")
			{
				$SubjectFailedArr[] = $SubjectScoreInfoArr['SubjectNameCh'];
			}
			else
			{
				$SubjectPassedArr[] = $SubjectScoreInfoArr['SubjectNameCh'];
			}
		}
		
		## build remark text
		$thisRemark = "";
		$SubjectPassedText = implode($eReportCard['Template']['SubjectSeparator'], $SubjectPassedArr);
		$SubjectFailedText = implode($eReportCard['Template']['SubjectSeparator'], $SubjectFailedArr);
		
		if (count($SubjectFailedArr) == 0)
		{
			# Promote
			$thisRemark = $eReportCard['Template']['RemarkPromotion'][2];
		}
		else if ( 	count($SubjectFailedArr) == 1
					&& !(in_array($SubjectFailedText, $MainSubjectNameArr)) 
					&& !($this->Has_Mark_Less_Than_School_Requirement($ScoreInfoArr)) )
		{
			# 1. Only one subject failed
			# 2. The failed subject is not Chi, Eng, Math
			# 3. The failed mark must be >= 40
			# 試升
			$thisRemark = (count($SubjectPassedArr) > 0)? $eReportCard['Template']['RemarkPromotion'][4][0] : "";
			$thisRemark .= $eReportCard['Template']['RemarkPromotion'][4][1];
		}
		else
		{
			# Retained
			$thisRemark = (count($SubjectPassedArr) > 0)? $eReportCard['Template']['RemarkPromotion'][3][0] : "";
			$thisRemark .= $eReportCard['Template']['RemarkPromotion'][3][1];
		}
		
		$thisRemark = str_replace("<!--PassSubjects-->", $SubjectPassedText, $thisRemark);
		$thisRemark = str_replace("<!--FailedSubjects-->", $SubjectFailedText, $thisRemark);
		
		return $thisRemark;
	}
	
	#### End of Get Remarks Functions ####
	
	
	function Get_ClassInfoArr_By_StudentID($StudentID)
	{
		/*
		$sql = "	SELECT
							b.ClassID, a.ClassName, a.ClassNumber
					FROM
							INTRANET_USER as a
						LEFT OUTER JOIN
							INTRANET_CLASS as b ON (a.ClassName = b.ClassName)
					WHERE
							a.UserID = '".$StudentID."'
				";
		$resultArr = $this->returnArray($sql, 3);
		
		return $resultArr;
		*/
		return $this->Get_Student_Class_ClassLevel_Info($StudentID);
	}
	
	function Get_StudentID_By_ClassName_And_ClassNumber($ClassName, $ClassNumber)
	{
		/*
		$sql = " select UserID from INTRANET_USER where ClassName='$ClassName' and ClassNumber=$ClassNumber and RecordType=2 ";
		$x = $this->returnVector($sql,1);
		*/
		
		$x = $this->Get_Student_Info_By_ClassName_ClassNumber($ClassName, $ClassNumber);
		return $x[0]['UserID'];
	}
	
	function Is_All_Mark_Empty_NA_Exempted($ReportArray, $StudentID, $ClassLevelID)
	{
		$IsAllDisplaySlashesArr = array();
		for ($i=0; $i<sizeof($ReportArray); $i++)
		{
			$thisReportID = $ReportArray[$i]['ReportID'];
			$MarksAry = $this->getMarks($thisReportID, $StudentID);
			
			
			$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $thisReportID);
			$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $thisReportID);
			if(sizeof($MainSubjectArray) > 0)
				foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			
			$isAllSubjectEmpty = true;
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# for main subject only
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)
					continue;
					
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $thisReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
				$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : "";
				
				if ($thisMark!="" || $thisGrade!="")
				{
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					# check special case
					list($thisMarkConverted, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					
					// [2016-0420-0949-55207] for Tong Nam, "----" is equal to "N.A."
					//if ($thisMarkConverted!="/" && $thisMarkConverted!="N.A." && $thisMarkConverted!="*")
					if ($thisMarkConverted!="/" && $thisMarkConverted!="N.A." && $thisMarkConverted!="----" && $thisMarkConverted!="*")
					{
						$isAllSubjectEmpty = false;
						break;
					}
				}
			}
			
			$IsAllDisplaySlashesArr[$thisReportID] = $isAllSubjectEmpty;
		}
		
		return $IsAllDisplaySlashesArr;
	}
}
?>