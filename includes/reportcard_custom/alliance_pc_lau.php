<?php
# Editing by

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

    function libreportcardcustom($type="")
    {
        $this->libreportcard();
        $this->configFilesType = array("attendance", "award", "promotion");

        // Temp control variables to enable/disaable features
        $this->IsEnableSubjectTeacherComment = 1;
        $this->IsEnableMarksheetFeedback = 1;
        $this->IsEnableMarksheetExtraInfo = 0;
        $this->IsEnableManualAdjustmentPosition = 0;

        $this->EmptySymbol = "---";
    }

    ########## START Template Related ##############
    function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='')
    {
        return "";
    }

    function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='', $skipGradeRemarks=false)
    {
        global $PATH_WRT_ROOT, $intranet_root;

        $x = "";

        if($ReportID)
        {
            # Report Info
            $ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
            $ClassLevelID = $ReportBasicInfo['ClassLevelID'];

            # Report Title
            $ReportTitle = $ReportBasicInfo['ReportTitle'];
            $ReportTitle = str_replace(':_:', ' ', $ReportTitle);

            # School Logo
            $SchoolLogo = $intranet_root."/file/reportcard2008/templates/alliance_pc_lau_logo.png";

            # Retrieve Required Variables
            $defaultVal = ($StudentID=='') ? "XXX" : '';

            # Retrieve Student Info
            $StudentInfo = "$defaultVal $defaultVal ($defaultVal)";
            $StudentSTRN = $defaultVal;
            $ClassTeacherInfo = "$defaultVal ($defaultVal)";
            if($StudentID)
            {
                include_once($PATH_WRT_ROOT."includes/libclass.php");
                $lclass = new libclass();

                ### Student Info
                $StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);
                $StudentName = $StudentInfoArr[0]['StudentNameEn'];
                $ClassName   = $StudentInfoArr[0]['ClassName'];
                $ClassNumber = $StudentInfoArr[0]['ClassNumber'];
                $StudentInfo = "$StudentName $ClassName ($ClassNumber)";

                ### Student STRN
                $StudentSTRN = $StudentInfoArr[0]['STRN'];
                $StudentSTRN = trim($StudentSTRN) != "" ? $StudentSTRN : $this->EmptySymbol;

                ### Class Teacher
                $ClassTeacherArr = $lclass->returnClassTeacher($ClassName, $this->schoolYearID);
                if(is_array($ClassTeacherArr) && count($ClassTeacherArr) > 0)
                {
                    $ChineseTeacherInfoArr = array();
                    $HomeroomTeacherInfoArr = array();
                    foreach((array)$ClassTeacherArr as $thisClassTeacher)
                    {
                        $ClassTeacherUserID = $thisClassTeacher['UserID'];

                        ### Class Teacher Info
                        include_once($PATH_WRT_ROOT."includes/libuser.php");
                        $lu = new libuser($ClassTeacherUserID);
                        $ClassTeacherNameEN = $lu->EnglishName;
                        $ClassTeacherNameCH = $lu->ChineseName;
                        $ClassTeacherEmail  = $lu->UserEmail;

                        ### Class Teacher Type
                        $ClassTeacherTitle = $lu->TitleEnglish;
                        $isChineseTeacher = strpos($ClassTeacherTitle, 'Chinese Teacher') !== false;
                        $isHomeroomTeacher = strpos($ClassTeacherTitle, 'Homeroom Teacher') !== false;

                        ### Class Teacher Title
                        $ClassTeacherTitleEN = $lu->TitleEnglish;
                        $ClassTeacherTitleCH = $lu->TitleChinese;
                        $ClassTeacherTitle = $isChineseTeacher ? $ClassTeacherTitleCH : $ClassTeacherTitleEN;
                        $ClassTeacherTitleArr = explode(',', $ClassTeacherTitle);
                        if (isset($ClassTeacherTitleArr[1])) {
                            $ClassTeacherTitle = $ClassTeacherTitleArr[1];
                        } else {
                            $ClassTeacherTitle = $ClassTeacherTitleArr[0];
                        }
                        $ClassTeacherTitle = trim($ClassTeacherTitle);

                        if ($isHomeroomTeacher) {
                            $thisClassTeacherInfo = "$ClassTeacherTitle $ClassTeacherNameEN ($ClassTeacherEmail)";
                            $HomeroomTeacherInfoArr[] = $thisClassTeacherInfo;
                        } else if ($isChineseTeacher) {
                            $thisClassTeacherInfo = "$ClassTeacherNameCH$ClassTeacherTitle ($ClassTeacherEmail)";
                            $ChineseTeacherInfoArr[] = $thisClassTeacherInfo;
                        }
                    }

                    // Homeroom Teacher > Chinese Teacher
                    $ClassTeacherInfo = "";
                    $HomeroomTeacherInfoArr = array_merge($HomeroomTeacherInfoArr, $ChineseTeacherInfoArr);
                    if(!empty($HomeroomTeacherInfoArr)) {
                        $ClassTeacherInfo = implode(' and ', (array)$HomeroomTeacherInfoArr);
                    }
                }
                else
                {
                    $ClassTeacherInfo = $this->emptySymbol;
                }
            }

            $x .= "<table class='' width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
                $x .= "<tr>";
                    $x .= "<td width='100%'>";
                        $x .= "<img width='90.2mm' style='padding-bottom: 0.8mm' src='".$SchoolLogo."'>";
                    $x .= "</td>";
                $x .= "</tr>";
            $x .= "</table>";

            $x .= "<table class='' width='100%' border='0' cellpadding='0' cellspacing='0' style='border-top: 0.5px solid black; border-bottom: 0.5px solid black; padding-top: 2mm; padding-bottom: 2mm;'>\n";
                $x .= "<tr>";
                    $x .= "<td width='100%' style='font-size: 11pt;'>";
                        $x .= "<b>".$ReportTitle."</b>";
                    $x .= "</td>";
                $x .= "</tr>";
                $x .= "<tr>";
                    $x .= "<td width='100%' style='font-size: 8pt;'>";
                        $x .= "<b>Alberta No. ($StudentSTRN)</b>";
                    $x .= "</td>";
                $x .= "</tr>";
                $x .= "<tr>";
                    $x .= "<td width='100%' style='font-size: 8pt;'>";
                        $x .= "<b>Student: <span style='color: #EF5B7C0'>".$StudentInfo."</span></b>";
                    $x .= "</td>";
                $x .= "</tr>";
                $x .= "<tr>";
                    $x .= "<td width='100%' style='font-size: 8pt;'>";
                        $x .= "<b>Teacher: ".$ClassTeacherInfo."</b>";
                    $x .= "</td>";
                $x .= "</tr>";
            $x .= "</table>";

            if($skipGradeRemarks)
            {
                // do nothing
            }
            else
            {
                $x .= "<br/>";

                $x .= "<table class='' width='100%' border='0' cellpadding='0' cellspacing='0' style='padding-top: 1mm'>\n";
                    $x .= "<tr>";
                        /*
                        $x .= "<td width='22%' style='font-size: 7pt;' valign='top'>";
                            $x .= "Key to the Interim Assessments<br/>";
                            $x .= "<span style='font-size: 5.5pt;'>Academic achievement key</span>";
                        $x .= "</td>";
                        */
                        $x .= "<td width='10%' height='14mm' style='font-size: 22pt; background-color: #00660b; color: white;' align='center'>";
                            $x .= "RS";
                        $x .= "</td>";
                        $x .= "<td width='15%' style='font-size: 7pt; background-color: #00660b; color: white;'>";
                            $x .= "Requires Substantial<br/>Support To Meet<br/>Grade Expectations";
                        $x .= "</td>";
                        $x .= "<td width='3%'>&nbsp;</td>";
                        $x .= "<td width='10%' style='font-size: 22pt; background-color: #00660b; color: white;' align='center'>";
                            $x .= "WT";
                        $x .= "</td>";
                        $x .= "<td width='12%' style='font-size: 7pt; background-color: #00660b; color: white;'>";
                            $x .= "Working<br/>Towards Grade<br/>Expectations";
                        $x .= "</td>";
                        $x .= "<td width='3%'>&nbsp;</td>";
                        $x .= "<td width='11%' style='font-size: 22pt; background-color: #00660b; color: white;' align='center'>";
                            $x .= "WW";
                        $x .= "</td>";
                        $x .= "<td width='11%' style='font-size: 7pt; background-color: #00660b; color: white;'>";
                            $x .= "Working<br/>Within Grade<br/>Expectations";
                        $x .= "</td>";
                        $x .= "<td width='3%'>&nbsp;</td>";
                        $x .= "<td width='11%' style='font-size: 22pt; background-color: #00660b; color: white;' align='center'>";
                            $x .= "WA";
                        $x .= "</td>";
                        $x .= "<td width='11%' style='font-size: 7pt; background-color: #00660b; color: white;'>";
                            $x .= "Working<br/>Above Grade<br/>Expectations";
                        $x .= "</td>";
                    $x .= "</tr>";
                $x .= "</table>";

                $x .= "<br/>";
            }
        }

        return $x;
    }

    function getFooter($ReportID='', $PrintTemplateType='')
    {
        global $intranet_root;

        # Footer icons
        $FooterIcon1 = $intranet_root."/file/reportcard2008/templates/alliance_pc_lau_footer_logo_1.png";
        $FooterIcon2 = $intranet_root."/file/reportcard2008/templates/alliance_pc_lau_footer_logo_2.png";
        $FooterIcon3 = $intranet_root."/file/reportcard2008/templates/alliance_pc_lau_footer_logo_3.png";
        $FooterIcon4 = $intranet_root."/file/reportcard2008/templates/alliance_pc_lau_footer_logo_4.png";

        $x = "";
        $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
            $x .= "<tr>";
                $x .= "<td colspan='6' align='center' style='font-size: 9pt;'>";
                    $x .= "2 Fu Ning Street, Kowloon, Hong Kong　　香港九龍富寧街2號";
                $x .= "</td>";
            $x .= "</tr>";
            $x .= "<tr>";
                $x .= "<td width='10%'>&nbsp;</td>";
                $x .= "<td width='20%' style='vertical-align: bottom'>";
                    $x .= "<img width='3.4mm' src='".$FooterIcon1."' style='font-size: 10pt;'> (852) 2713 3733";
                $x .= "</td>";
                $x .= "<td width='20%' style='vertical-align: bottom'>";
                    $x .= "<img width='3.4mm' src='".$FooterIcon2."' style='font-size: 10pt;'> (852) 2362 2328";
                $x .= "</td>";
                $x .= "<td width='20%' style='vertical-align: bottom'>";
                    $x .= "<img width='3.4mm' src='".$FooterIcon3."' style='font-size: 10pt;'> <u>info@capcl.edu.hk</u>";
                $x .= "</td>";
                $x .= "<td width='20%' style='vertical-align: bottom'>";
                    $x .= "<img width='3.4mm' src='".$FooterIcon4."' style='font-size: 10pt;'> www.capcl.edu.hk";
                $x .= "</td>";
                $x .= "<td width='10%' style='vertical-align: bottom'>";
                    $x .= "Page {PAGENO} of {nbpg}";
                $x .= "</td>";
            $x .= "</tr>";
        $x .= "</table>";

        return $x;
    }

    function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='', $bilingual=false)
    {
        return "";
    }

    function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='', $bilingual=false)
    {
        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportSetting['ClassLevelID'];

        # Retrieve Subjects
        $SubjectEnArr = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=1, $ReportID, 'en');
        $SubjectChArr = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=1, $ReportID, 'b5');

        # Retrieve Marks
        $MarksArr = array();
        if($StudentID) {
            $MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
        }

        # Retrieve Report Content
        $reportContent = $this->genMSTableMarks($ReportID, $MarksArr, $StudentID, $SubjectEnArr, $SubjectChArr);

        return $reportContent;
    }

    function genMSTableColHeader($ReportID)
    {
        return "";
    }

    function returnTemplateSubjectCol($ReportID, $ClassLevelID)
    {
        return "";
    }

    function genMSTableMarks($ReportID, $MarksArr=array(), $StudentID='', $SubjectEnArr=array(), $SubjectChArr=array())
    {
        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportSetting['Semester'];
        $SemesterNo = $this->Get_Semester_Seq_Number($SemID);
        $ClassLevelID = $ReportSetting['ClassLevelID'];

        # Retrieve Semesters
        $SemesterArr = $this->GET_ALL_SEMESTERS();
        $SemesterArr = array_values(array_flip($SemesterArr));
        $firstTermID = $SemesterNo == 1 ? $SemID : $SemesterArr[1];
        $secondTermID = $SemesterNo > 1 ? $SemID : -1;

        # Retrieve Semester Reports
        $TermReportArr = $this->Get_Report_List($ClassLevelID, 'T', true);
        $TermReportArr = BuildMultiKeyAssoc($TermReportArr, 'Semester');
        $firstTermReportID = $firstTermID > 0 ? $TermReportArr[$firstTermID]['ReportID'] : -1;
        $secondTermReportID = $secondTermID > 0 ? $TermReportArr[$secondTermID]['ReportID'] : -1;

        # Retrieve Term Report Marks
        $firstTermMarksArr = array();
        if($StudentID && $firstTermReportID > 0) {
            if($SemesterNo > 1) {
                $firstTermMarksArr = $this->getMarks($firstTermReportID, $StudentID, '', 0, 1);
            } else {
                $firstTermMarksArr = $MarksArr;
            }
        }
        $secondTermMarksArr = array();
        if($StudentID && $secondTermReportID > 0) {
            if($SemesterNo > 1) {
                $secondTermMarksArr = $MarksArr;
            }
        }

        # Retrieve Subject Settings
        $SubjectSettingArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, '', $ReportID);

        # Retrieve Required Variables
        $defaultVal = ($StudentID=='') ? "XXX" : '';
        $defaultCommentVal = ($StudentID=='') ? "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br>XXXXXXXXXXXXXXXXXXXXXX" : '';

        // loop Subject
        $x = "";
        foreach((array)$SubjectEnArr as $SubjectID => $SubjectNameEn)
        {
            $SubjectNameCh = trim($SubjectChArr[$SubjectID]);

            // Subject Display Lang
            $SubjectDisplayLang = $SubjectSettingArr[$SubjectID]['LangDisplay'];
            $isChineseSubject = $SubjectDisplayLang == "ch";

            // Parent subject > find component subjects
            $isParentSubject = false;
            $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $SubjectDisplayLang);
            if(!empty($CmpSubjectArr)) {
                $isParentSubject = true;
                $CmpSubjectArr = array_slice($CmpSubjectArr, 0, 4);
            }
            $CmpSubjectCount = count($CmpSubjectArr);

            // Subject Name
            $SubjectName = $isChineseSubject ? $SubjectNameCh : $SubjectNameEn;

            /*
            // Subject Teacher
            $SubjectTeacherName = $defaultVal;
            if($StudentID)
            {
                ### Subject Teacher Info
                $SubjectTeacherInfo = $this->Get_Student_Subject_Teacher($SemID, $StudentID, $SubjectID);
                $SubjectTeacherNameEN = $SubjectTeacherInfo[0]['EnglishName'];
                $SubjectTeacherNameCH = $SubjectTeacherInfo[0]['ChineseName'];
                $SubjectTeacherName  = $isChineseSubject ? $SubjectTeacherNameCH : $SubjectTeacherNameEN;

                ### Class Teacher Title
                $SubjectTeacherTitleEN = $SubjectTeacherInfo[0]['TitleEnglish'];
                $SubjectTeacherTitleCH = $SubjectTeacherInfo[0]['TitleChinese'];
                $SubjectTeacherTitle = $isChineseSubject ? $SubjectTeacherTitleCH : $SubjectTeacherTitleEN;
                $SubjectTeacherTitleArr = explode(',', $SubjectTeacherTitle);
                if(isset($SubjectTeacherTitleArr[1])) {
                    $SubjectTeacherTitle = $SubjectTeacherTitleArr[1];
                } else {
                    $SubjectTeacherTitle = $SubjectTeacherTitleArr[0];
                }
                $SubjectTeacherTitle = trim($SubjectTeacherTitle);

                if ($isChineseSubject) {
                    $SubjectTeacherName = "$SubjectTeacherName$SubjectTeacherTitle";
                }
                else {
                    $SubjectTeacherName = "$SubjectTeacherTitle $SubjectTeacherName";
                }
            }
            */

            // Student Subject Result (Grade)
            $firstTermGrade = ($StudentID=='') ? "G" : $this->EmptySymbol;
            $secondTermGrade = ($StudentID=='') ? "G" : $this->EmptySymbol;
            if($StudentID)
            {
                if($firstTermMarksArr[$SubjectID][0]['Grade'] != "" && !$this->Check_If_Grade_Is_SpecialCase($firstTermMarksArr[$SubjectID][0]['Grade'])){
                    $firstTermGrade = $firstTermMarksArr[$SubjectID][0]['Grade'];
                }
                if($secondTermMarksArr[$SubjectID][0]['Grade'] != "" && !$this->Check_If_Grade_Is_SpecialCase($secondTermMarksArr[$SubjectID][0]['Grade'])){
                    $secondTermGrade = $secondTermMarksArr[$SubjectID][0]['Grade'];
                }
            }

            // Subject Group Description
            $SubjectGroupDescription = $defaultCommentVal;
            if($StudentID)
            {
                // skip if student not studying subject group
                $StudentSubjectGroupArr = $this->Get_Student_Studying_Subject_Group_Info($SemID, $StudentID, $SubjectID);
                $StudentSubjectGroupID = $StudentSubjectGroupArr[0]['SubjectGroupID'];
                if($StudentSubjectGroupID > 0) {
                    // do nothing
                } else {
                    continue;
                }

                $SubjectGroupDescription = $this->getSubjectGroupDescription($ReportID, $StudentSubjectGroupID);
                $SubjectGroupDescription = $this->HandleCommentDisplay($SubjectGroupDescription[0]['Description']);
            }

            // use div to prevent pagebreak
            //$x .= "<div width='100%' style='page-break-inside: avoid; font-family: centuryschbook, msjh;' border='0' cellspacing='0' cellpadding='0'>";
            //$x .= "<table class='' width='100%' border='0' cellspacing='0' cellpadding='0'>";
            //$x .= "<tr><td width='100%' style='pending: 0;'>";

            // Subject result
            if($isParentSubject && $CmpSubjectCount > 0)
            {
                $CmpColWidth = round(86 / ($CmpSubjectCount * 2));

                $x .= "<table class='' width='100%' border='0' cellspacing='0' cellpadding='0' style='page-break-inside: avoid;'>";
                    $x .= "<tr>";
                        $x .= "<td width='100%' style='background-color: #92d050; color: white; font-size: 9.5pt; padding-left: 4px; padding-top: 3px; padding-bottom: 2px;' colspan='".(($CmpSubjectCount * 2 ) + 2)."'>";
                            $x .= "<b>".$SubjectName."</b>";
                            //$x .= "　".$SubjectTeacherName;
                        $x .= "</td>";
                    $x .= "</tr>";
                    $x .= "<tr>";
                        $x .= "<td width='100%' style='font-size: 9.5pt; height: 16mm; padding-left: 4px; padding-top: 3px;' valign='top' colspan='".(($CmpSubjectCount * 2 ) + 2)."'>";
                            $x .= $SubjectGroupDescription;
                            $x .= "<br/>";
                            $x .= "<br/>";
                $x .= "<br/>";
                $x .= "<br/>";
                        $x .= "</td>";
                    $x .= "</tr>";
                    $x .= "<tr>";
                        $x .= "<td>&nbsp;</td>";
                        foreach((array)$CmpSubjectArr as $thisCmpSubject) {
                            $CmpSubjectName = $thisCmpSubject['SubjectName'];

                            $x .= "<td colspan='2' style='background-color: #92d050; color: white; font-size: 9pt; padding-top: 3px; padding-bottom: 2px;' align='center' class=''>";
                                $x .= "<b>".$CmpSubjectName."</b>";
                            $x .= "</td>";
                        }
                        $x .= "<td>&nbsp;</td>";
                    $x .= "</tr>";

                    $x .= "<tr>";
                        $x .= "<td width='7%'>&nbsp;</td>";
                        foreach((array)$CmpSubjectArr as $thisCmpSubject) {
                            $thisCmpSubjectID = $thisCmpSubject['SubjectID'];

                            $firstTermGrade = ($StudentID=='') ? "G" : $this->EmptySymbol;
                            $secondTermGrade = ($StudentID=='') ? "G" : $this->EmptySymbol;
                            if($StudentID)
                            {
                                if($firstTermMarksArr[$thisCmpSubjectID][0]['Grade'] != "" && !$this->Check_If_Grade_Is_SpecialCase($firstTermMarksArr[$thisCmpSubjectID][0]['Grade'])){
                                    $firstTermGrade = $firstTermMarksArr[$thisCmpSubjectID][0]['Grade'];
                                }
                                if($secondTermMarksArr[$thisCmpSubjectID][0]['Grade'] != "" && !$this->Check_If_Grade_Is_SpecialCase($secondTermMarksArr[$thisCmpSubjectID][0]['Grade'])){
                                    $secondTermGrade = $secondTermMarksArr[$thisCmpSubjectID][0]['Grade'];
                                }
                            }

                            $x .= "<td width='$CmpColWidth%' style='font-size: 16pt; height: 18mm; padding-top: 4px;' align='center' valign='top'>";
                                $x .= "<b>".$firstTermGrade."</b>";
                                $x .= "<br/>";
                                $x .= "<span style='font-size: 8pt;'>1</span><span style='font-size: 7pt;'>st</span><span style='font-size: 8pt;'> Sem Mark</span>";
                            $x .= "</td>";
                            $x .= "<td width='$CmpColWidth%' style='font-size: 16pt; height: 18mm; padding-top: 4px;' align='center' valign='top'>";
                                $x .= "<b>".$secondTermGrade."</b>";
                                $x .= "<br/>";
                                $x .= "<span style='font-size: 8pt;'>2</span><span style='font-size: 7pt;'>nd</span><span style='font-size: 8pt;'> Sem Mark</span>";
                            $x .= "</td>";
                        }
                        $x .= "<td width='7%'>&nbsp;</td>";
                    $x .= "</tr>";
                $x .= "</table>";
            }
            else
            {
                $x .= "<table class='' width='100%' border='0' cellspacing='0' cellpadding='0' style='page-break-inside: avoid;'>";
                    $x .= "<tr>";
                        $x .= "<td width='100%' style='background-color: #92d050; color: white; font-size: 9.5pt; padding-left: 4px; padding-top: 3px; padding-bottom: 2px;' colspan='3'>";
                            $x .= "<b>".$SubjectName."</b>";
                            //$x .= "　".$SubjectTeacherName;
                        $x .= "</td>";
                    $x .= "</tr>";
                    $x .= "<tr>";
                        $x .= "<td width='64%' style='font-size: 9.5pt; height: 18mm; padding-left: 4px; padding-top: 3px;' valign='top'>";
                            $x .= $SubjectGroupDescription;
                            $x .= "<br/>";
                            $x .= "<br/>";
                        $x .= "</td>";
                        $x .= "<td width='18%' style='font-size: 16pt; height: 18mm; padding-top: 4px;' align='center' valign='top'>";
                            $x .= "<b>".$firstTermGrade."</b>";
                            $x .= "<br/>";
                            $x .= "<span style='font-size: 8pt;'>1</span><span style='font-size: 7pt;'>st</span><span style='font-size: 8pt;'> Sem Mark</span>";
                        $x .= "</td>";
                        $x .= "<td width='18%' style='font-size: 16pt; height: 18mm; padding-top: 4px;' align='center' valign='top'>";
                            $x .= "<b>".$secondTermGrade."</b>";
                            $x .= "<br/>";
                            $x .= "<span style='font-size: 8pt;'>2</span><span style='font-size: 7pt;'>nd</span><span style='font-size: 8pt;'> Sem Mark</span>";
                        $x .= "</td>";
                    $x .= "</tr>";
                $x .= "</table>";
                $x .= "<br/>";
            }

            //$x .= "</td></tr>";
            //$x .= "</table>";
            //$x .= "</div>";
        }

        return $x;
    }

    function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
    {
        return "";
    }

    function getMiscTable($ReportID, $StudentID='', $PrintTemplateType='', $bilingual=false)
    {
        global $PATH_WRT_ROOT;

        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportSetting['Semester'];
        $SemesterNo = $this->Get_Semester_Seq_Number($SemID);
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        $ReportIssueDate = $ReportSetting['Issued'];
        $ReportStartDate = $ReportSetting['TermStartDate'];
        $ReportEndDate = $ReportSetting['TermEndDate'];

        # Retrieve Semesters
        $SemesterArr = $this->GET_ALL_SEMESTERS();
        $SemesterArr = array_values(array_flip($SemesterArr));
        
        # Retrieve School Day
        $ReportStartSchDay = getStartDateOfAcademicYear($this->schoolYearID, $SemesterArr[0]);
        $ReportStartSchDayTs = strtotime($ReportStartSchDay);
        $ReportEndSchDay = getEndDateOfAcademicYear($this->schoolYearID, $SemID);
        $ReportEndSchDayTs = strtotime($ReportEndSchDay);
        $ReportStartSchDay = date('F j, Y', $ReportStartSchDayTs);
        $ReportEndSchDay = date('F j, Y', $ReportEndSchDayTs);
        $ReportStartDateTs = strtotime($ReportStartDate);
        $ReportEndDateTs = strtotime($ReportEndDate);

        # Retrieve Student Admission & Graduation Date
        if($StudentID) {
            $isTransferStudent = false;
            $isTermTransferStudent = false;
            $isLeaveStudent = false;
            $isTermLeaveStudent = false;

            include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
            $laccount = new libaccountmgmt();

            $userPersonalInfo = $laccount->getUserPersonalSettingByID($StudentID);
            $userAdmissionDate = $userPersonalInfo['AdmissionDate'];
            if(!is_date_empty($userAdmissionDate)) {
                $userAdmissionDateTs = strtotime($userAdmissionDate);
                if($ReportStartSchDayTs < $userAdmissionDateTs && $userAdmissionDateTs < $ReportEndSchDayTs) {
                    $isTransferStudent = true;
                }
                if($ReportStartDateTs < $userAdmissionDateTs && $userAdmissionDateTs < $ReportEndDateTs) {
                    $isTermTransferStudent = true;
                }
            }
            $userGraduationDate = $userPersonalInfo['GraduationDate'];
            if(!is_date_empty($userGraduationDate)) {
                $userGraduationDateTs = strtotime($userGraduationDate);
                if($ReportStartSchDayTs < $userGraduationDateTs && $userGraduationDateTs < $ReportEndSchDayTs) {
                    $isLeaveStudent = true;
                }
                if($ReportStartDateTs < $userGraduationDateTs && $userGraduationDateTs < $ReportEndDateTs) {
                    $isTermLeaveStudent = true;
                }
            }

            if ($isTransferStudent) {
                $ReportStartSchDay = date('F j, Y', $userAdmissionDateTs);
                $ReportStartSchDayTs = $userAdmissionDateTs;
            }
            if ($isLeaveStudent) {
                $ReportEndSchDay = date('F j, Y', $userGraduationDateTs);
                $ReportEndSchDayTs = $userGraduationDateTs;
            }
            if ($isTermTransferStudent) {
                $ReportStartDateTs = $userAdmissionDateTs;
            }
            if ($isTermLeaveStudent) {
                $ReportEndDateTs = $userGraduationDateTs;
            }
        }

        # Retrieve Class Teacher Comment
        $ClassTeacherCommentArr = array();
        if($StudentID) {
            $ClassTeacherCommentArr = $this->GET_TEACHER_COMMENT($StudentID, '0', $ReportID);
        }

        # Retrieve Student Attendance
        $StudentAttendanceArr = array();
        $StudentAttendanceTotalArr = array();
        if($StudentID) {
            $StudentAttendanceArr = $this->retrieveAttandanceMonthData($ReportID, $StudentID, $ReportStartDateTs, $ReportEndDateTs);
            $StudentAttendanceTotalArr = $this->retrieveAttandanceMonthData($ReportID, $StudentID, $ReportStartSchDayTs, $ReportEndSchDayTs);
        }

        # Retrieve Class Teacher Comment
        $ClassTeacherCommentEn = $defaultCommentVal;
        $ClassTeacherCommentCh = $defaultCommentVal;
        if($StudentID)
        {
            $ClassTeacherCommentEn = $ClassTeacherCommentArr[$StudentID]['Comment'];
            $ClassTeacherCommentEn = trim($ClassTeacherCommentEn) != "" ? $ClassTeacherCommentEn : $this->EmptySymbol;
            $ClassTeacherCommentCh = $ClassTeacherCommentArr[$StudentID]['AdditionalComment'];
            $ClassTeacherCommentCh = trim($ClassTeacherCommentCh) != "" ? $ClassTeacherCommentCh : $this->EmptySymbol;

            $ClassTeacherCommentEn = $this->HandleCommentDisplay($ClassTeacherCommentEn);
            $ClassTeacherCommentCh = $this->HandleCommentDisplay($ClassTeacherCommentCh);
            /*
            $ClassTeacherCommentEn = nl2br($ClassTeacherCommentEn);
            if(isBig5(trim($ClassTeacherCommentEn))){
                $ClassTeacherCommentEn = $this->getCurrentComment($ClassTeacherCommentEn);
            }
            // replace "‘" and "’" by "'"
            $ClassTeacherCommentEn = str_replace("‘", "'", $ClassTeacherCommentEn);
            $ClassTeacherCommentEn = str_replace("’", "'", $ClassTeacherCommentEn);
            $ClassTeacherCommentEn = str_replace("“", "\"", $ClassTeacherCommentEn);
            $ClassTeacherCommentEn = str_replace("”", "\"", $ClassTeacherCommentEn);

            $ClassTeacherCommentCh = nl2br($ClassTeacherCommentCh);
            if(isBig5(trim($ClassTeacherCommentCh))){
                $ClassTeacherCommentCh = $this->getCurrentComment($ClassTeacherCommentCh);
            }
            // replace "‘" and "’" by "'"
            $ClassTeacherCommentCh = str_replace("‘", "'", $ClassTeacherCommentCh);
            $ClassTeacherCommentCh = str_replace("’", "'", $ClassTeacherCommentCh);
            $ClassTeacherCommentCh = str_replace("“", "\"", $ClassTeacherCommentCh);
            $ClassTeacherCommentCh = str_replace("”", "\"", $ClassTeacherCommentCh);
            */
        }

        # Retrieve Class Teacher Signature
        $ChineseTeacherSignature = '';
        $HomeroomTeacherSignature = '';
        if($StudentID)
        {
            include_once($PATH_WRT_ROOT."includes/libclass.php");
            $lclass = new libclass();

            ### Student Info
            $StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", $StudentID);

            // Get Class Teacher
            $ClassTeacherArr = $lclass->returnClassTeacher($StudentInfoArr[0]['ClassName'], $this->schoolYearID);
            if(is_array($ClassTeacherArr) && count($ClassTeacherArr) > 0)
            {
                foreach((array)$ClassTeacherArr as $thisClassTeacher)
                {
                    $ClassTeacherUserID = $thisClassTeacher['UserID'];

                    ### Class Teacher Info
                    include_once($PATH_WRT_ROOT."includes/libuser.php");
                    $lu = new libuser($ClassTeacherUserID);
                    $ClassTeacherUserLogin = $lu->UserLogin;
                    $ClassTeacherTitle = $lu->TitleEnglish;

                    ### Class Teacher Type
                    $isChineseTeacher = strpos($ClassTeacherTitle, 'Chinese Teacher') !== false;
                    $isHomeroomTeacher = strpos($ClassTeacherTitle, 'Homeroom Teacher') !== false;

                    ### Class Teacher Signature
                    $ClassTeacherSignature = $this->Get_Signature_Image($ClassTeacherUserLogin);
                    if($ClassTeacherSignature != '') {
                        if ($isHomeroomTeacher) {
                            $HomeroomTeacherSignature = $ClassTeacherSignature;
                        } else if ($isChineseTeacher) {
                            $ChineseTeacherSignature = $ClassTeacherSignature;
                        }
                    }
                }
            }
        }

        # Retrieve Personal Characteristics
        $PersonalCharArr = $this->returnPersonalCharSettingData($ClassLevelID, '0');
        $PersonalCharOptionArr = $this->returnPersonalCharOptions();
        $PersonalCharDataArr = array();
        if($StudentID) {
            $PersonalCharDataArr = $this->getPersonalCharacteristicsProcessedData($StudentID, $ReportID, 0);
        }

        # Retrieve Other Info
        $OtherInfoDataArr = array();
        if($StudentID) {
            $OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
            $OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$SemID];
        }

        # Retrieve Attendance Info
        $TotalNumber = ($StudentID=='') ? "#" : 0;
        $DaysAbsent = ($StudentID=='') ? "#" : 0;
        $Lates = ($StudentID=='') ? "#" : 0;
        if($StudentID)
        {
            $TotalNumber = $OtherInfoDataArr['Total Number of Full Instructional Days'];
            if($StudentAttendanceTotalArr['Total'] > 0) {
                $TotalNumber = $TotalNumber ? $TotalNumber : $StudentAttendanceTotalArr['Total'];
            }
            $TotalNumber = $TotalNumber ? $TotalNumber : 0;

            $DaysAbsent = $OtherInfoDataArr['Days Absent'];
            if($StudentAttendanceArr['Absent'] > 0) {
                $DaysAbsent = $DaysAbsent ? $DaysAbsent : $StudentAttendanceArr['Absent'];
            }
            $DaysAbsent = $DaysAbsent ? $DaysAbsent : 0;

            $Lates = $OtherInfoDataArr['Lates'];
            if($StudentAttendanceArr['Lates'] > 0) {
                $Lates = $Lates ? $Lates : $StudentAttendanceArr['Lates'];
            }
            $Lates = $Lates ? $Lates : 0;
        }

        # Retrieve Report Date
        if(is_date_empty($ReportEndDate)) {
            $ReportEndDate = $this->EmptySymbol;
        } else {
            $ReportEndDate = date('F j, Y', strtotime($ReportEndDate));
        }
        if(is_date_empty($ReportIssueDate)) {
            $ReportIssueDate = $this->EmptySymbol;
        } else {
            $ReportIssueDate = date('F j, Y', strtotime($ReportIssueDate));
        }

        # Check Icon
        $checkIcon = "<img height='8mm' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/alliance_pc_lau_check_icon.gif'>";

        // use div to prevent pagebreak between Comment and Signature
        $x .= "<pagebreak />";
        //$x .= "<div width='100%' style='page-break-inside: avoid;' border='0' cellspacing='0' cellpadding='0'>";

        $x .= "<table class='' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='padding-top: 4mm; page-break-inside: avoid;'>";
            $x .= "<tr>";
                $x .= "<td width='55%' style='font-size: 12pt; height: 12mm; border-bottom: 0.5px solid black;' valign='middle'>";
                    $x .= "<b>Personal Development and Learning Skills</b>";
                $x .= "</td>";

                $charOptCount = 1;
                foreach((array)$PersonalCharOptionArr as $thisCharOpt) {
                    $x .= "<td width='15%' style='font-size: 12pt; height: 12mm; border-bottom: 0.5px solid black;' align='center' valign='middle'>";
                        $x .= "<i>".$thisCharOpt."</i>";
                    $x .= "</td>";

                    $charOptCount++;
                    if($charOptCount > 3) {
                        break;
                    }
                }
            $x .= "</tr>";

        foreach((array)$PersonalCharArr as $thisPersonalChar)
        {
            $thisPersonalCharTitle = $thisPersonalChar['Title_EN'];
            $thisPersonalCharDescription = $thisPersonalChar['Description'];
            $thisPersonalCharDescription = $this->HandleCommentDisplay($thisPersonalCharDescription);
            /*
            $thisPersonalCharDescription = nl2br($thisPersonalCharDescription);
            if(isBig5(trim($thisPersonalCharDescription))){
                $thisPersonalCharDescription = $this->getCurrentComment($thisPersonalCharDescription);
            }
            // replace "‘" and "’" by "'"
            $thisPersonalCharDescription = str_replace("‘", "'", $thisPersonalCharDescription);
            $thisPersonalCharDescription = str_replace("’", "'", $thisPersonalCharDescription);
            $thisPersonalCharDescription = str_replace("“", "\"", $thisPersonalCharDescription);
            $thisPersonalCharDescription = str_replace("”", "\"", $thisPersonalCharDescription);
            */

            $x .= "<tr>";
                $x .= "<td width='70%' style='font-size: 10pt; height: 12mm; border-bottom: 0.5px solid black; padding-top: 2mm; padding-bottom: 2mm' valign='top'>";
                    $x .= $thisPersonalCharTitle."<br/>";
                    $x .= "<span style='font-size: 8pt;'>".$thisPersonalCharDescription."</span>";
                $x .= "</td>";

                $charOptCount = 1;
                foreach((array)$PersonalCharOptionArr as $thisCharOpt) {
                    $isSelected = isset($PersonalCharDataArr[$thisPersonalCharTitle]) && $PersonalCharDataArr[$thisPersonalCharTitle] == $thisCharOpt;
                    $displayForCharOpt = $isSelected ? $checkIcon : "&nbsp;";

                    $x .= "<td align='center' valign='middle' style='border-bottom: 0.5px solid black; height: 14mm;'>";
                        $x .= $displayForCharOpt;
                    $x .= "</td>";

                    $charOptCount++;
                    if($charOptCount > 3) {
                        break;
                    }
                }
            $x .= "</tr>";
        }
        $x .= "</table>";
        //$x .= "</div>";

        // use div to prevent pagebreak between Comment and Signature
        $x .= "<br />";
        //$x .= "<div width='100%' style='page-break-inside: avoid; background-color: rgba(172, 193, 55, 0.3); padding: 9px 7px;' border='0' cellspacing='0' cellpadding='0'>";

        $x .= "<table class='' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='page-break-inside: avoid; background-color: rgba(172, 193, 55, 0.3); padding: 3mm 2mm;'>";
        $x .= "<tr>";
            $x .= "<td width='100%' style='font-size: 12pt; height: 10mm;' valign='top'>";
                $x .= "<b>General Comments</b>";
            $x .= "</td>";
        $x .= "</tr>";
        if ($SemesterNo == 2 || $SemesterNo == 4)
        {
            $x .= "<tr>";
                $x .= "<td width='100%' style='font-size: 10pt; height: 12mm;' valign='top'>";
                    $x .= $ClassTeacherCommentEn;
                $x .= "</td>";
            $x .= "</tr>";
            if($HomeroomTeacherSignature != '')
            {
                $x .= "<tr>";
                    $x .= "<td width='100%' style='height: 55px;' valign='top'>";
                        $x .= $HomeroomTeacherSignature;
                    $x .= "</td>";
                $x .= "</tr>";
            }

            $x .= "<tr>";
                $x .= "<td width='100%' style='font-size: 10pt; height: 12mm;' valign='top'>";
                    $x .= "<br/>".$ClassTeacherCommentCh;
                $x .= "</td>";
            $x .= "</tr>";
            if($ChineseTeacherSignature != '')
            {
                $x .= "<tr>";
                    $x .= "<td width='100%' style='height: 55px;' valign='top'>";
                        $x .= $ChineseTeacherSignature;
                    $x .= "</td>";
                $x .= "</tr>";
            }
        }
        else
        {
            $ReportRemarkContent = $ReportSetting['Description'];
            $ReportRemarkContent = $this->HandleCommentDisplay($ReportRemarkContent);

            $x .= "<tr>";
                $x .= "<td width='100%' style='font-size: 10pt; height: 8mm;' valign='top'>";
                    $x .= $ReportRemarkContent;
                $x .= "</td>";
            $x .= "</tr>";
        }
        $nextline = "<br/>";
        
        if($SemesterNo == 4)
        {
            if($OtherInfoDataArr['Academic Proficiency Award'] == 'Y')
            {
                $x .= "<tr>";
                    $x .= "<td width='100%' style='font-size: 8.5pt;'>";
                        $x .= $nextline;
                        $x .= "The student is presented with:<br/>　Academic Proficiency Award";
                    $x .= "</td>";
                $x .= "</tr>";

                $nextline = "";
            }
        }

        $x .= "<tr>";
            $x .= "<td width='100%' style='font-size: 8.5pt;'>";
                $x .= $nextline;
                $x .= "First School Day: ".$ReportStartSchDay;
            $x .= "</td>";
        $x .= "</tr>";

        $x .= "<tr>";
            $x .= "<td width='100%' style='font-size: 8.5pt;'>";
                $x .= "Last Day of Quarter ".$SemesterNo.": ".$ReportEndSchDay;
            $x .= "</td>";
        $x .= "</tr>";
        
        $x .= "<tr>";
            $x .= "<td width='100%' style='font-size: 8.5pt;'>";
                $x .= "Total Number of Full Instructional Days by ".$ReportEndDate.": ".$TotalNumber;
                $x .= "<br/>";
                $x .= "Days Absent: ".$DaysAbsent;
                $x .= "<br/>";
                $x .= "Lates: ".$Lates;
                $x .= "<br/>";
                $x .= "Report Printed: ".$ReportIssueDate;
            $x .= "</td>";
        $x .= "</tr>";

        if($SemesterNo == 4)
        {
            if($OtherInfoDataArr['Promotion'] != '' && ($OtherInfoDataArr['Promotion'] == 'met' || $OtherInfoDataArr['Promotion'] == 'unmeet'))
            {
                $x .= "<tr>";
                if($OtherInfoDataArr['Promotion'] == 'met') {
                    $x .= "<td width='100%' style='font-size: 8.5pt;'>";
                        $x .= "<br/>This student has met the academic requirement for this year";
                    $x .= "</td>";
                } else {
                    $x .= "<td width='100%' style='font-size: 8.5pt;'>";
                        $x .= "<br/>This student unmeet the academic requirement for this year.";
                    $x .= "</td>";
                }
                $x .= "</tr>";
            }
        }
        $x .= "<tr>";
            $x .= "<td width='100%' align='center' style='font-size: 10pt; padding-top: 5mm;'>";
                $x .= "<br/>";
                $x .= "<i>End of Report</i>";
            $x .= "</td>";
        $x .= "</tr>";
        $x .= "</table>";
        //$x .= "</div>";

        return $x;
    }

    function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='', $bilingual=false)
    {
        return '';
    }

    function getCSSContent($ReportID="")
    {
        $style = "
				<head>
					<style TYPE='text/css'>
						
						body {
							font-family: centuryschbook, msjh; 
							font-size: 8.5pt;
							color: #00660b;
						}
						
						td {
							font-family: centuryschbook, msjh; 
						}
						
						hr {
							height:1px;
							color:black; 
						}
						
                        .report_header {
							font-size: 14pt;
						}
						
						.header_describe {
							border:3px solid black;
							margin-bottom: 5mm;	
							font-size: 9.5pt;
						}

						.header_describe .engheader_describe {
							font-size: 8.5pt;
						}
						
						.report_header .report_info_title {
							font-size: 14pt;
							font-weight: bold;
						}

						.mstable_header {
							font-size: 9pt;
						}

						.mstable_header .ms_address {
							padding: 2px 5px 0px 5px;;
						}

						.mstable_header .report_info_mstable {
							border: 3px solid #000000;
							border-collapse:collapse;
						}

						.mstable_header .report_info_mstable td {
							border: 1px solid #000000;
							border-left: 3px solid #000000;
							text-align:center;
							padding: 4px;
						}

						.kg_report_header {
							font-size: 10pt;
							line-height: 20px;
						}
						
						.kg_report_header .kg_report_header_table td {
							line-height: 30px;
						}
						
						.kg_report_header .report_info_title {
							font-size: 14pt;
							font-weight: bold;
							line-height: 35px;
						}

						.kg_header_describe {
							border:3px solid black;
							margin-bottom: 2mm;	
							font-size: 9.5pt;
							text-align: center;
						}

						.kg_header_describe td {
							padding-bottom: 10px;
						}

						.student_info {
							font-size: 10pt;
							font-weight: bold;
							/*border-left: 0.2mm solid #000000;*/
							border: 0.2mm solid #000000;
							border-bottom: 0mm solid #000000;
						}
						
						.bold_cell {
							font-size: 11pt;
							font-weight: normal;	
						}
						
						.result_table th, .result_table td {
							border: 0.05mm solid #BDBDBD;
							text-align: left;
						}

						.result_table th, .result_table td {
							padding: 2px;
							padding-left: 4px;
						}

						.result_table .table_header {
							font-size: 5pt;
							font-weight: normal;
						}

						.result_table td table, .result_table td table td, .result_table .student_info, .result_table .student_info td {
							border: 0mm solid #000000;
						}
						
						.ms_result_table {
							border: 3px solid #000000;
							border-collapse: collapse;
							margin-top: 3mm;
							margin-bottom: 3mm;
						}

						.ms_result_table th	{
							background-color: #bdbdbd;
						}

						.ms_attendance_table, .ms_remarks_table {
							border: 3px solid #000000;
							border-collapse: collapse;
							margin-bottom: 8mm;
							text-align: center;
						}

						.ms_result_table th, .ms_result_table td, .ms_attendance_table td {
							border: 1px solid #000000;
							text-align: center;
							padding: 2px;
							font-weight:normal;
						}					

						.ms_result_table th, .ms_result_table td {
							border-left: 3px solid #000000;
						}				

						.ms_attendance_table td, .ms_remarks_table td {
							padding: 2px;
						}

						.ms_remarks_table td {
							padding: 6px 2px 2px 2px;
							text-align: left;
    						vertical-align: center;
						}

						.border_bottom {
							border-bottom: 1px solid #000000;
						}
						
						.ES_ParentSubject, .ES_ParentSubject_s {
							font-size: 12pt;
							font-weight: bold;
						}

						.ES_ParentSubject_s {
							padding-top:5mm;
							border-top: 0.05mm solid black;
						}
						
						.English_ES {
							font-weight: normal;
							border: 0.05mm solid black;
							border-collapse:collapse;
						}

						.ES_CompSubject {
							font-size: 10pt;
							margin-bottom: 6mm;
							border: 0.2mm solid black;
							border-collapse:collapse;
						}

						.ES_CompSubject td {
							border: 0.2mm solid black;
						}

						.ES_CompSubject .comp_title {
							font-size: 12pt;
						}
						
						.ES_CompSubject .comp_bold {
							font-weight: bold;
						}
						
						.Eng_Subject_Title {
							background-color: black;
 							color: white;
							font-size: 10pt;
							padding-left: 3px;
						}
						
						.Eng_CompSubject_Title {
							background-color: lightgray;
							font-size: 10pt;
							padding-left: 3px;
						}

						.Eng_CompSubject_Content {
							background-color: white;
							font-size: 9pt;
							padding-left: 3px;
						}
						
						.Eng_Subject_ccc {
							background-color: gray;
 							color: white;
						}

						.KG_ParentSubject {
							padding-bottom: 16px;
						}

						.table_pad_top {
							margin-top: 10mm;
							margin-bottom: 5mm;
						}

						.kg_writing_describe {
							border: 0.2mm solid black;
							border-collapse: collapse;
							line-height: 2;
							margin-bottom: 6mm;
						}

						.kg_writing_describe td {
							padding: 2px;
							padding-bottom: 5px
							text-align:center;
							vertical-align: top;
						}

						.gray_header td {
							background-color: lightgray;
						}

						.ES_Comment {
							border: 0.2mm solid black;		
							margin-bottom: 6mm;		
							font-size: 12pt;
						}

						.ES_Comment_Eng {
							border: 0.2mm solid black;		
							margin-top: 6mm;			
						}

						.kg_comment_style {
							border: 0.6mm solid black;
							padding: 6px;
							margin-bottom: 10px;
							page-break-inside: avoid;
						}

						.ES_Sign {
							font-size: 11pt;
							page-break-inside: avoid;
						}

						.ES_Eng_Sign {
							margin-top: 18mm;
							text-align: center;
							font-size: 11pt;
							page-break-inside: avoid;
						} 

						.sign_table {
							margin-top: 2.6mm;
							font-size: 10pt;
							page-break-inside: avoid;
						}

						.sign_table th, .sign_table td {
							padding: 0px;
							padding-left: 0px;
						}
					</style>
				</head>";

        return $style;
    }
    ########## END Template Related ##############

    ########## START PDF Generation ##############
    function reportObjectSetUp($ReportID, $bilingual=false)
    {
        global $PATH_WRT_ROOT;
        $returnArr .= "<td width='7%' align='center'><img height='60px' src='".$PATH_WRT_ROOT."/file/reportcard2008/templates/biba_logo_only.png'></td>";

        $report_format = 'A4';
        $margin_top = 10;
        $margin_bottom = 30;
        $margin_left = 14;
        $margin_right = 11;
        $margin_header = 10;
        $margin_footer = 12;

        // Create mPDF object
        $this->pdf = new mPDF('', "A4", 0, "", $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);

        // PDF Setting
        //$this->pdf->charset_in = "UTF-8";
        $this->pdf->allow_charset_conversion = true;
        //$this->pdf->list_auto_mode = "mpdf";
        $this->pdf->useAdobeCJK = true;
        $this->pdf->backupSubsFont = array('msjh');
        $this->pdf->useSubstitutions = true;
        //$this->pdf->autoScriptToLang = true;
        //$this->pdf->autoLangToFont = true;

        $this->pdf->setAutoTopMargin = 'stretch';
        $this->pdf->splitTableBorderWidth = 0.1; //split 1 table into 2 pages add border at the bottom
        $this->pdf->shrink_tables_to_fit = 1;

        // Watermark Imae
        $watermarkImage = $PATH_WRT_ROOT."/file/reportcard2008/templates/alliance_pc_lau_watermark.png";
        $this->pdf->SetWatermarkImage($watermarkImage, 1, 'P', 'P');
        $this->pdf->showWatermarkImage = true;
    }
    ########## END PDF Generation #############

    function HandleCommentDisplay($CommentText)
    {
        // Next line
        $CommentText = nl2br($CommentText);

        // Handle Chinese
        if(isBig5(trim($CommentText)))
        {
            // Remove &shy; from comment before checking
            $CommentText = str_replace("­", "", $CommentText);

            $tempCommentText = "";
            $delim = "";
            $CommentTextArr = explode("<br />", $CommentText);
            foreach((array)$CommentTextArr as $thisCommentText){
                if(trim($thisCommentText) != "" && preg_match("/[\x{4e00}-\x{9fa5}]+/u", $thisCommentText)) {
                    $thisCommentText = str_replace(" ", "", $thisCommentText);
                }
                $tempCommentText .= $delim.$thisCommentText;
                $delim = "<br>";
            }

            $CommentText = $tempCommentText;
        }

        // replace "‘" and "’" by "'"
        $CommentText = str_replace("‘", "'", $CommentText);
        $CommentText = str_replace("’", "'", $CommentText);
        $CommentText = str_replace("“", "\"", $CommentText);
        $CommentText = str_replace("”", "\"", $CommentText);

        return $CommentText;
    }

    function retrieveAttandanceMonthData($ReportID, $StudentID, $targetStartDateTs='', $targetEndDateTs='')
    {
        global $PATH_WRT_ROOT, $eRCTemplateSetting;
        include_once($PATH_WRT_ROOT.'includes/libcardstudentattend2.php');

        # Retrieve eAttendance Config
        $lattend = new libcardstudentattend2();
        $attendance_mode = $lattend->attendance_mode;
        $record_count_value = $lattend->ProfileAttendCount == 1 ? 0.5 : 1;

        $present_count_value = $record_count_value;
        $absent_count_value = $record_count_value;
        $late_count_value = 1;
        $early_leave_count_value = 1;

        # Retrieve Report Period
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportStartDate = $ReportInfoArr['TermStartDate'];
        $ReportEndDate = $ReportInfoArr['TermEndDate'];
        if ($targetStartDateTs != '') {
            $ReportStartDate = date('Y-m-d', $targetStartDateTs);
        }
        if ($targetEndDateTs != '') {
            $ReportEndDate = date('Y-m-d', $targetEndDateTs);
        }

        # Get Term Reports
        //$TermReportIDArr = $this->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);

        // loop Term Reports
        $dataAry = array();

        # Get related db table
        $table = array();
        if (strtotime($ReportStartDate) == strtotime($ReportEndDate))
        {
            $year = date('Y', strtotime($ReportStartDate));
            $month = date('m', strtotime($ReportEndDate));

            // $lattend->createTable_Card_Student_Daily_Log($year, $month);
            $table[] = "CARD_STUDENT_DAILY_LOG_" . $year . _ . $month;
        }
        else
        {
            $yearMonthArr = $this->Get_Year_Month_By_Date_Range($ReportStartDate, $ReportEndDate);
            foreach(array_keys((array)$yearMonthArr) as $year) {
                foreach ((array)$yearMonthArr[$year] as $month) {
                    // $this->createTable_Card_Student_Daily_Log($year, $month);
                    $temp_table = "CARD_STUDENT_DAILY_LOG_" . $year . _ . $month;
                    $table[] = $temp_table;
                }
            }
        }

        # Get Attendance data
        $monthDataAry = $lattend->retrieveCustomizedIndividualMonthDataByDateRange(array($StudentID), $table);

        # Init data array
        $reportData = array();
        $reportData['Total'] = 0;
        $reportData['Lates'] = 0;
        $reportData['Absent'] = 0;

        // loop data records
        $iniVal = strtotime($ReportStartDate);
        $target = strtotime($ReportEndDate);
        for ($li = $iniVal; $li <= $target; $li += 86400)
        {
            $j = date('Y-m-d', $li);

            if (sizeof($monthDataAry[$StudentID][$j]) != 0)
            {
                list($am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive,
                    $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason,
                    $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session,
                    $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $monthDataAry[$StudentID][$j];

                // $reportData['Lateness'] += ($am_late_session + $pm_late_session);
                // $reportData['RequestLeave'] += ($am_request_leave_session + $pm_request_leave_session);
                // $reportData['OfficalLeave'] += ($am_offical_leave_session + $pm_offical_leave_session);
                // $reportData['Absenteeism'] += $am_absent_session + $pm_absent_session;

                # AM Data
                if ($am != "")
                {
                    # Absent (with Reason)
                    if($am == '1' && $am_absent_waive != 1) {
                        $reportData['Absent'] += $absent_count_value;
                    }

                    # Lateness
                    if ($am == '2' && $am_late_waive != 1) {
                        $reportData['Lates'] += $late_count_value;
                    }

                    # Unexcused Absent
                    //$reportData['UnexcusedAbsent'] += $am_play_truant_session;
                }

                # PM Data
                if ($pm!= "")
                {
                    # Absent (with Reason)
                    if($pm == '1' && $pm_absent_waive != 1) {
                        $reportData['Absent'] += $absent_count_value;
                    }

                    # Lateness
                    if ($pm == '2' && $pm_late_waive != 1) {
                        $reportData['Lateness'] += $late_count_value;
                    }
                }

                $reportData['Total']++;
            }
        }
        //$reportData['Total'] = $lattend->countSchoolDayDiff($ReportStartDate, $ReportEndDate);

        $dataAry = $reportData;

        return $dataAry;
    }

    # Map Subject Code and Subject ID
    function GET_SUBJECT_SUBJECTCODE_MAPPING($reverse=0){

        $sql = "SELECT 
					CODEID,
					RecordID
				FROM
					ASSESSMENT_SUBJECT 
				WHERE
					EN_DES IS NOT NULL
					AND RecordStatus = 1
					AND (CMP_CODEID IS NULL || CMP_CODEID = '')
				ORDER BY
					DisplayOrder
				";
        $SubjectArr = $this->returnArray($sql, 2);

        $ReturnArr = array();
        if (sizeof($SubjectArr) > 0) {
            for($i=0; $i<sizeof($SubjectArr); $i++) {
                if($reverse){
                    $ReturnArr[$SubjectArr[$i]["CODEID"]] = $SubjectArr[$i]["RecordID"];
                }
                else{
                    $ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
                }
            }
        }
        return $ReturnArr;
    }

    function Get_Signature_Image($ParUserLogin)
    {
        global $special_feature;

        if(!$special_feature['signature_upload']) {
            return "";
        }

        $sql = "SELECT SignatureLink FROM INTRANET_USER_SIGNATURE WHERE UserLogin = '$ParUserLogin' ";
        $Result = $this->returnVector($sql);
        if($Result[0]) {
            return '<img src="'.$Result[0].'" height="55px" width="120px">';
        }
    }

    function Get_Year_Month_By_Date_Range($startDate, $endDate)
    {
        $time1 = strtotime($startDate);
        $time2 = strtotime($endDate);

        $months = array();
        while ($time1 < $time2) {
            $y = date("Y", $time1);
            $m = date("m", $time1);

            if (!isset($months[$y])) {
                $months[$y] = array();
            }
            $months[$y][] = $m;
            $time1 = mktime(0, 0, 0, intval($m) + 1, 1, intval($y));
        }

        return $months;
    }
}
?>