<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/carmel_alison.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {
    
    function libreportcardcustom() {
        $this->libreportcard();
        $this->configFilesType = array("summary", "award", "merit", "eca", "remark", "attendance", "post");
        
        // Temp control variables to enable/disaable features
        $this->IsEnableSubjectTeacherComment = 1;
        $this->IsEnableMarksheetFeedback = 1;
        $this->IsEnableMarksheetExtraInfo = 0;
        $this->IsEnableManualAdjustmentPosition = 0;
        
        $this->EmptySymbol = "&nbsp;";
        $this->EmptySymbolLine = "---";
    }
    
    ########## START Template Related ##############
    function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow,$ReportID, $StudentID) {
        global $eReportCard;
        
        
        # Page 1
        $Page1TableTop = '';
        $Page1TableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
        $Page1TableTop .= "<tr><td>".$TitleTable."</td></tr>";
        $Page1TableTop .= "<tr height='60px'><td>".$StudentInfoTable."</td></tr>";
        $Page1TableTop .= "<tr><td>".$MSTable."</td></tr>";
        $Page1TableTop .= "<tr><td valign='top'>".$MiscTable."</td></tr>";
        $Page1TableTop .= "</table>";
        
        $Page1TableBottom = '';
        $Page1TableBottom .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
        $Page1TableBottom .= "<tr><td class='tabletext' align='center'>1/2</td></tr>";
        $Page1TableBottom .= "</table>";
        
        $Page1Table = '';
        $Page1Table .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
        $Page1Table .= "<tr height='1000px'><td valign='top'>".$Page1TableTop."</td></tr>";
        $Page1Table .= "<tr><td valign='bottom'>".$Page1TableBottom."</td></tr>";
        $Page1Table .= "</table>";
        
        
        # Page 2
        $Page2TableTop = '';
        $Page2TableTop .= $this->Get_Page2_Table($ReportID, $StudentID);
        
        $Page2TableBottom = '';
        $Page2TableBottom .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom' >";
        $Page2TableBottom .= "<tr><td class='tabletext' align='center'>2/2</td></tr>";
        $Page2TableBottom .= "</table>";
        
        $Page2Table = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
        $Page2Table .= "<tr height='805px'><td valign='top'>".$Page2TableTop."</td></tr>";
        $Page2Table .= "<tr height='170px'><td valign='bottom'>".$SignatureTable."</td></tr>";
        $Page2Table .= "<tr height='40px'><td valign='bottom'>".$Page2TableBottom."</td></tr>";
        $Page2Table .= "</table>";
        
        
        $SDAvgTable = $this->getSDAvgTable($ReportID,$StudentID);
        $Page3Table = '';
        $Page3Table .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
        $Page3Table .= "<tr valign='bottom'><td>".$SDAvgTable."</td></tr>";
        $Page3Table .= "</table>";
        
        $x = "";
        $x .= "<tr valign='top'><td>".$Page1Table."</td></tr>";

        // 2020-07-17 (Philips) - use page-break-before instead of page-break-after
        $x .= "<div id=\"container\" style='page-break-before:always;'>";
        $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
        $x .= "<tr valign='top'><td>".$Page2Table."</td></tr>";
        $x .= "</table>";
        $x .= "</div>";
        
        $x2 .= "<tr><td valign='top'>";
        $x2 .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
        $x2 .= "<tr valign='top'><td>".$Page3Table."</td></tr>";
        $x2 .= "</table>";
        $x2 .= "</td></tr>";
        
        //return $x;
        return array($x,$x2);
    }
    
    function getSimpleLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow,$ReportID, $StudentID)
    {
        
        # Page 1
        $TableTop = '';
        $TableTop .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
        $TableTop .= "<tr><td>".$TitleTable."</td></tr>";
        $TableTop .= "<tr height='60px'><td>".$StudentInfoTable."</td></tr>";
        $TableTop .= "<tr><td align='center'>".$MSTable."</td></tr>";
        $TableTop .= "</table>";
        
        $Table = '';
        $Table .= "<table width='650px' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='page-break-after:always'>";
        $Table .= "<tr height='800px'><td valign='top'>".$TableTop."</td></tr>";
        $Table .= "<tr height='170px'><td valign='bottom'>".$SignatureTable."</td></tr>";
        $Table .= "</table>";
        
        return $Table;
    }
    
    function getReportHeader($ReportID)
    {
        global $eReportCard,$PATH_WRT_ROOT, $isSimpleReport;
        
        $TitleTable = "";
        
        if($ReportID)
        {
            # Retrieve Display Settings
            $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
            $Semester = $ReportSetting["Semester"];
            $SemName = $Semester=="F"?$eReportCard['Template']['WholeYear']:$this->returnSemesters($Semester);
            
            $HeaderHeight = $ReportSetting['HeaderHeight'];
            $ReportTitle =  $ReportSetting['ReportTitle'];
            $ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
            
            # Get AcademicYearName
            include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
            $ObjYear = new academic_year($this->schoolYearID);
            $AcademicYear = $ObjYear->Get_Academic_Year_Name();
            
            # get school badge
            $imgfile = "/file/reportcard2008/templates/carmel_alison.jpg";
            $SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" width=95>\n" : "";
            # get school name
            $SchoolName = "<b>".$eReportCard['Template']['SchoolInfo']['SchoolNameCh']."</b><br>".$eReportCard['Template']['SchoolInfo']['SchoolNameEn'];
            
            $TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
            if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
            
            $TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
            $TitleTable .= "<tr><td width='100' align='center'>".$TempLogo."</td>";
            
            if(!empty($ReportTitle) || !empty($SchoolName))
            {
                $TitleTable .= "<td>";
                if ($HeaderHeight == -1) {
                    $TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
                    if(!empty($SchoolName))
                        $TitleTable .= "<tr><td nowrap='nowrap' class='schoolname' align='left' style='padding-top:4px;padding-bottom:4px;'>".$SchoolName."</td></tr>\n";
                        if($this->getSchoolInfo() && !$isSimpleReport)
                            $TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='left' style='padding-top:0px; padding-bottom:0px; line-height:12px;'>".$this->getSchoolInfo()."</td></tr>\n";
                            $TitleTable .= "</table>\n";
                } else {
                    for ($i = 0; $i < $HeaderHeight; $i++) {
                        $TitleTable .= "<br/>";
                    }
                }
                $TitleTable .= "</td>";
            }
            $TitleTable .= "<td align='right' valign=top>";
            $TitleTable .= "<span class='fontsize16'>".$eReportCard['Template']['Reportcard']."</span><br>";
//             $TitleTable .= "<span class='graybg fontsize14'>".$AcademicYear." ".$SemName."</span><br>";
            $TitleTable .= "<span class='graybg fontsize14'>".$AcademicYear."</span><br>";
            $TitleTable .= "<span class='fontsize14'>".$ReportSetting["Description"]."</span><br>";
            $TitleTable .= "</td>";
            $TitleTable .= "</tr>";
            $TitleTable .= "</table>";
        }
        
        return $TitleTable;
    }
    
    function getReportStudentInfo($ReportID, $StudentID='', $forPage3='')
    {
        global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting, $isSimpleReport;
        
        if($isSimpleReport)
            return $this->GetSimpleReportStudentInfo($StudentID);
            
            if($ReportID)
            {
                # Retrieve Display Settings
                $StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
                //$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
                $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
                $SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
                $LineHeight = $ReportSetting['LineHeight'];
                
                # retrieve required variables
                $defaultVal = ($StudentID=='')? "XXX" : '';
                //$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
                include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
                $ObjYear = new academic_year($this->schoolYearID);
                $data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
                
                $data['DateOfIssue'] = $ReportSetting['Issued'];
                if($StudentID)		# retrieve Student Info
                {
                    include_once($PATH_WRT_ROOT."includes/libuser.php");
                    include_once($PATH_WRT_ROOT."includes/libclass.php");
                    $lu = new libuser($StudentID);
                    $lclass = new libclass();
                    
                    $data['Name'] = $lu->UserName2Lang('en', 2);
                    //$data['ClassNo'] = $lu->ClassNumber;
                    //$data['Class'] = $lu->ClassName;
                    
                    $StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
                    //$thisClassName = $StudentInfoArr[0]['ClassName'];
                    $thisClassNumber = $StudentInfoArr[0]['ClassName']."(".$StudentInfoArr[0]['ClassNumber'].")";
                    
                    $data['ClassNo'] = $thisClassNumber;
                    //$data['Class'] = $thisClassName;
                    
                    $data['StudentNo'] = $thisClassNumber;
                    
                    /*if (is_array($SettingStudentInfo))
                     {
                     if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
                     $data['Class'] .= " (".$thisClassNumber.")";
                     }
                     else
                     {
                     if ($thisClassNumber != "")
                     $data['Class'] .= " (".$thisClassNumber.")";
                     }*/
                     
                     $tmpdate = $lu->DateOfBirth;
                     $tmpdate = explode("-",$tmpdate);
                     $tmpdate = implode("/",$tmpdate);
                     
                     $data['DateOfBirth'] = $tmpdate?$tmpdate:"&nbsp;";
                     $data['Gender'] = $lu->Gender;
                     
                     $data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
                     $data['StudentAdmNo'] = $data['STRN'];
                     
                     $ClassTeacherAry = $lclass->returnClassTeacher($StudentInfoArr[0]['ClassName'], $this->schoolYearID);
                     foreach($ClassTeacherAry as $key=>$val)
                     {
                         $CTeacher[] = $val['CTeacher'];
                     }
                     $data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
                }
                
                $defaultInfoArray = array("Name", "ClassNo", "DateOfBirth", "Gender");
                
                $Page3Array = array("Name", "ClassNo");
                if(!empty($defaultInfoArray))
                {
                    $count = 0;
                    $StudentInfoTable .= "<table width='80%' border='0' cellpadding='0' cellspacing='0' align='left'>";
                    
                    if(!$forPage3)
                    {
                        $StudentInfoTable .= "<thead><th class='tabletext fontsize14'>".$eReportCard['Template']['SubTitle']['StudentInfo']."</th></thead>";
                    }
                    
                    for($i=0; $i<sizeof($defaultInfoArray); $i++)
                    {
                        $SettingID = trim($defaultInfoArray[$i]);
                        
                        if(!in_array("DateOfBirth&Gender",(array)$SettingStudentInfo)&&in_array($SettingID,array("DateOfBirth","Gender")))
                            continue;
                            
                            if($forPage3==1 && !in_array($SettingID,$Page3Array)) continue;
                            
                            if(in_array($SettingID, $defaultInfoArray)===true)
                            {
                                $Title = $eReportCard['Template']['StudentInfo'][$SettingID];
                                if($count%$StudentInfoTableCol==0) {
                                    $StudentInfoTable .= "<tr>";
                                }
                                
                                $colspan = $SettingID=="Name" ? " colspan='4' " : "";
                                $align = $SettingID!="Name" ? " align='center' " : "";
                                $YMD = $SettingID=="DateOfBirth" ? "(Y/M/D)" : "&nbsp;";
                                $datewidth = $SettingID=="DateOfBirth" ? " width='80px' ":"";
                                
                                $StudentInfoTable .= "<td class='tabletext fontsize12' valign='top' height='{$LineHeight}' align=right>".$Title." :</td>";
                                $StudentInfoTable .= "<td class='tabletext tableline fontsize12' $align $colspan valign='top' height='{$LineHeight}' $datewidth>".($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
                                $StudentInfoTable .= "<td class='tabletext fontsize12'>$YMD</td>";
                                
                                if(($count+1)%$StudentInfoTableCol==0) {
                                    $StudentInfoTable .= "</tr>";
                                } else {
                                    //if($SettingID=="Name")
                                    //{
                                    $count++;
                                    //	$StudentInfoTable .= "</tr>";
                                    //}
                                }
                                $count++;
                            }
                            
                    }
                    $StudentInfoTable .= "</table>";
                    
                    
                }
            }
            return $StudentInfoTable;
    }
    
    function getMSTable($ReportID, $StudentID='')
    {
        global $eRCTemplateSetting, $eReportCard, $isSimpleReport;
        
        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $LineHeight = $ReportSetting['LineHeight'];
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        $ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
        $ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
        $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
        $ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
        
        # define
        $ColHeaderAry = $this->genMSTableColHeader($ReportID);
        list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
        
        # retrieve SubjectID Array
        $MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
        if (sizeof($MainSubjectArray) > 0)
            foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
            $SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
            if (sizeof($SubjectArray) > 0)
                foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
                
                # retrieve marks
                $MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
                
                $SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
                $sizeofSubjectCol = sizeof($SubjectCol);
                
                # retrieve Marks Array
                $MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
                $MarksDisplayAry = $MSTableReturned['HTML'];
                $isAllNAAry = $MSTableReturned['isAllNA'];
                
                # retrieve Subject Teacher's Comment
                $SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
                
                ##########################################
                # Start Generate Table
                ##########################################
                $DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border'>";
                # ColHeader
                $DetailsTable .= $ColHeader;
                
                $isFirst = 1;
                for($i=0;$i<$sizeofSubjectCol;$i++)
                {
                    $isSub = 0;
                    $thisSubjectID = $SubjectIDArray[$i];
                    
                    # If all the marks is "*", then don't display
                    if (sizeof($MarksAry[$thisSubjectID]) > 0)
                    {
                        $Droped = 1;
                        foreach($MarksAry[$thisSubjectID] as $cid=>$da)
                            if($da['Grade']!="*")	$Droped=0;
                    }
                    if($Droped)	continue;
                    if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
                    
                    if (($ShowSubjectComponent==0 || $eRCTemplateSetting['HideComponentSubject']) && $isSub)
                        continue;
                        
                        # check if displaying subject row with all marks equal "N.A."
                        if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
                        {
                            $DetailsTable .= "<tr>";
                            # Subject
                            $DetailsTable .= $SubjectCol[$i];
                            # Marks
                            $DetailsTable .= $MarksDisplayAry[$thisSubjectID];
                            # Subject Teacher Comment
                            if ($AllowSubjectTeacherComment) {
                                $css_border_top = "border_top";
                                if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
                                    $DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
                                    $DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
                                } else {
                                    $DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
                                    $DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
                                }
                                $DetailsTable .= "</span></td>";
                            }
                            
                            $DetailsTable .= "</tr>";
                            $isFirst = 0;
                        }
                }
                
                # MS Table Footer
                $DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
                
                $DetailsTable .= "</table>";
                ##########################################
                # End Generate Table
                ##########################################
                if($isSimpleReport)
                {
                    $msTableWidth = "450px";
                    $msTableTitle = "&nbsp;";
                }
                else
                {
                    $msTableWidth = "100%";
                    $msTableTitle = $eReportCard['Template']['SubTitle']['MsTable'];
                }
                
                $MSTable = "<table width='$msTableWidth' border='0' cellspacing='0' cellpadding='0'> ";
                
                $MSTable .= "<thead>";
                $MSTable .= "<th class='fontsize14 '>".$msTableTitle."</th>";
                $MSTable .= "</thead>";
                $MSTable .= "<tr>";
                $MSTable .= "<td>".$DetailsTable."</td>";
                $MSTable .= "</tr>";
                $MSTable .= "</table> ";
                
                return $MSTable;
    }
    
    function genMSTableColHeader($ReportID)
    {
        global $eReportCard, $eRCTemplateSetting, $isSimpleReport;
        
        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
        $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
        $SemID = $ReportSetting['Semester'];
        $ReportType = $SemID == "F" ? "W" : "T";
        $ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
        $ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
        $ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
        $ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
        $ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
        $ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
        $LineHeight = $ReportSetting['LineHeight'];
        $IsMainReport = 1;
        
        # updated on 08 Dec 2008 by Ivan
        # if subject overall column is not shown, grand total, grand average... also cannot be shown
        //$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
        $ShowRightestColumn = $ShowSubjectOverall;
        
        $n = 0;
        $e = 0;	# column# within term/assesment
        $t = array(); # number of assessments of each term (for consolidated report only)
        #########################################################
        ############## Marks START
        $row2 = "";
        /*if($ReportType=="T")	# Terms Report Type
         {
         # Retrieve Invloved Assesment
         $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
         $ColumnID = array();
         $ColumnTitle = array();
         if (sizeof($ColoumnTitle) > 0)
         foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
         
         $e = 0;
         for($i=0;$i<sizeof($ColumnTitle);$i++)
         {
         //$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
         $ColumnTitleDisplay = $ColumnTitle[$i];
         $row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
         $n++;
         $e++;
         }
         }
         else					# Whole Year Report Type
         {*/
         $ColumnData = $this->returnReportTemplateColumnData($ReportID);
         $needRowspan=0;
         /*for($i=0;$i<sizeof($ColumnData);$i++)
          {
          $SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
          $isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
          if($isDetails==1)
          {
          $thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
          $thisReportID = $thisReport['ReportID'];
          $thisReportTemplateData = $this->returnReportTemplateColumnData($thisReportID);
          $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
          $ColumnID = array();
          $ColumnTitle = array();
          foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
          for($j=0;$j<sizeof($ColumnTitle);$j++)
          {
          //$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
          $ColumnTitleDisplay =  $ColumnTitle[$j]."<br>".$thisReportTemplateData[$j]["DefaultWeight"];
          $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
          $n++;
          $e++;
          }
          $colspan = "colspan='". (sizeof($ColumnTitle)+2) ."'";
          $Rowspan = "";
          $needRowspan++;
          $t[$i] = sizeof($ColumnTitle);
          }
          else
          {
          $colspan = "";
          $Rowspan = "rowspan='2'";
          $e++;
          }
          $row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ." ".$ColumnData[$i]["DefaultWeight"]."</td>";
          $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['OverallResultCh']."/<br>".$eReportCard['Template']['Grade']. "</td>";
          $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";
          }*/
         $br = $ReportType=="W"?"<br>":"";
         
         $Semester = $ReportSetting["Semester"];
         $SemName = $Semester != "F" && in_array($this->GET_FORM_NUMBER($ClassLevelID), array(6,7)) ? $this->returnSemesters($Semester) : $eReportCard["Template"]["FirstTerm"];
         $ColHeaderClass = "fontsize16";
         if($isSimpleReport)
         {
             $ColHeaderClass = "simple_ms_col_header";
             $row1 = "<td class='border_left reportcard_text $ColHeaderClass' align='center' style='padding:10px 0'>".$eReportCard['Template']['Result']."</td>";
             $row1 .= "<td class='border_left reportcard_text $ColHeaderClass' align='center' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";
             
             $col .= "<col width='45%'>";
             $col .= "<col width='25%'>";
             $col .= "<col width='30%'>";
         }
         else if($IsMainReport)
         {
             // [2020-0708-0943-22235] 2019-2020 S1 - S5 Yearly Report
             if($this->schoolYear == '2019' && $ReportType == "W" && $FormNumber > 0 && $FormNumber < 6)
             {
                 $row1 .= "<td {$Rowspan} colspan='4' height='{$LineHeight}' class='border_left reportcard_text' align='center'>". $SemName ." 60%</td>";
             }
             else
             {
                 $row1 .= "<td {$Rowspan} colspan='4' height='{$LineHeight}' class='border_left reportcard_text' align='center'>". $SemName ." 40%</td>";
             }
             $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard["Template"]["Daily"].$br."40%</td>";
             $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard["Template"]["Exam"].$br."60%</td>";
             $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['OverallResultCh']."/".$br.$eReportCard['Template']['Grade']. "</td>";
             $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";

             if($ReportType == "W")
             {
                 # term 2
                 // [2020-0708-0943-22235] 2019-2020 S1 - S5 Yearly Report
                 if($this->schoolYear == '2019' && $ReportType == "W" && $FormNumber > 0 && $FormNumber < 6)
                 {
                     $row1 .= "<td {$Rowspan} colspan='4' height='{$LineHeight}' class='border_left reportcard_text' align='center'>". $eReportCard["Template"]["SecondTerm"] ." 40%</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard["Template"]["Daily"]."<br>30%</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard["Template"]["Exam"]."<br>70%</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['OverallResultCh']."/<br>".$eReportCard['Template']['Grade']. "</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";
                 }
                 else
                 {
                     $row1 .= "<td {$Rowspan} colspan='4' height='{$LineHeight}' class='border_left reportcard_text' align='center'>". $eReportCard["Template"]["SecondTerm"] ." 60%</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard["Template"]["Daily"]."<br>40%</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard["Template"]["Exam"]."<br>60%</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['OverallResultCh']."/<br>".$eReportCard['Template']['Grade']. "</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";
                 }

                 #whole year
                 $row1 .= "<td {$Rowspan} colspan=2 height='{$LineHeight}' class='border_left reportcard_text' align='center'>".$eReportCard['Template']['WholeYear']."</td>";
                 $row2 .= "<td valign='middle' class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
                 $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";
             }
         }
         else
         {
             $row1 .= "<td {$Rowspan} colspan='2' height='{$LineHeight}' class='border_left reportcard_text' align='center'>". $eReportCard["Template"]["MidtermTest"] ."</td>";
             $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['TestResult']."</td>";
             $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormPositionCh']."/".$eReportCard['Template']['FormNumOfStudentCh']. "</td>";
         }
         //}
         ############## Marks END
         #########################################################
         $Rowspan = $row2 ? "rowspan='2'" : "";
         
         if(!$needRowspan)
             $row1 = str_replace("rowspan='2'", "", $row1);
             
             $x = $col."<tr>";
             # Subject
             $SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
             for($i=0;$i<sizeof($SubjectColAry);$i++)
             {
                 $SubjectEng = $eReportCard['Template']['SubjectEng'];
                 $SubjectChn = $eReportCard['Template']['SubjectChn'];
                 $SubjectTitle = $SubjectColAry[$i];
                 $SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
                 $SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
                 //			$SubjectTitle = $eReportCard['Template']['SubjectChn'];
                 $x .= "<td {$Rowspan} align='center' valign='middle' class='$ColHeaderClass' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
                 $n++;
             }
             
             
             
             # Subject Overall
             //if($ShowRightestColumn)
             //{
             $n++;
             //}
             //else
             //{
                 //$e--;
                 //}
                 
                 # Subject Teacher Comment
                 if ($AllowSubjectTeacherComment) {
                     $row1 .= "<td valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['Template']['SubjectTeachersCommentCh']."</td>";
                     $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>&nbsp;</td>";
                 }
                 
                 # Marks
                 $x .= $row1;
                 
                 $x .= "</tr>";
                 if($row2)	$x .= "<tr>". $row2 ."</tr>";
                 return array($x, $n, $e, $t);
    }
    
    function returnTemplateSubjectCol($ReportID, $ClassLevelID)
    {
        global $eRCTemplateSetting,$eReportCard, $isSimpleReport;
        
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $LineHeight = $ReportSetting['LineHeight'];
        
        $SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
        $SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
        
        $msTableMarksFontClass = $isSimpleReport?"simple_ms_subject_title_mark":"tabletext";
        
        $x = array();
        $isFirst = 1;
        if (sizeof($SubjectArray) > 0) {
            foreach($SubjectArray as $SubjectID=>$Ary)
            {
                foreach($Ary as $SubSubjectID=>$Subjs)
                {
                    $t = "";
                    $Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
                    $CmpSubjWeight='';
                    if($SubSubjectID==0)		# Main Subject
                    {
                        $SubSubjectID=$SubjectID;
                        $Prefix = "";
                    }
//			 		else
//			 		{
//				 		$columnWeightConds = " (ReportColumnID = '0' OR ReportColumnID IS NULL) AND SubjectID = '$SubSubjectID' " ;
//						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//						$PercentWeight = ($columnSubjectWeightArr[0]["Weight"]*100)."%";
//						$CmpSubjWeight="(".$PercentWeight.")";
//			 		}
                    $css_border_top =  "border_top";
                    foreach($SubjectDisplay as $k=>$v)
                    {
                        $SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
                        $SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
                        
                        $v = str_replace("SubjectEng", $SubjectEng, $v);
                        $v = str_replace("SubjectChn", $SubjectChn, $v);
                        //		 				$v .= $CmpSubjWeight;
                        
                        $t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
                        $t .= "<table border='0' cellpadding='0' cellspacing='0'>";
                        $t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='$msTableMarksFontClass'>$v</td>";
                        $t .= "</tr></table>";
                        $t .= "</td>";
                    }
                    $x[] = $t;
                    $isFirst = 0;
                }
            }
        }
        
        return $x;
    }
    
    function getSignatureTable($ReportID='',$StudentID='')
    {
        global $eReportCard, $eRCTemplateSetting,$PATH_WRT_ROOT, $isSimpleReport;
        
        if($ReportID)
        {
            $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
            $Issued = $ReportSetting['Issued'];
            if($Issued){
                $Issued=explode("-",$Issued);
                $Issued=implode("/",$Issued);
            }
        }
        
        $SignatureTitleArray = $eRCTemplateSetting['Signature'];
        $SignatureTable = "";
        $SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
        $SignatureTable .= "<tr>";
        
        $emptytdwidth = $isSimpleReport?'15px':'200px';
        $emptytd = "<td width='$emptytdwidth'>&nbsp;</td>";
        
        include_once($PATH_WRT_ROOT."includes/libclass.php");
        $lclass = new libclass();
        $ClassTeacherAry =  $lclass->returnClassTeacher($this->Get_Student_ClassName($StudentID), $this->schoolYearID);
        
        $ClassTeacherStr = implode("．",Get_Array_By_Key($ClassTeacherAry,"CTeacher"));
        $TeacherSignature = '';
        foreach($ClassTeacherAry as $ClassTeacher)
        {
            $ClassTeacherLogin = $ClassTeacher["UserLogin"];
            $TeacherSignature .=  $this->Get_Signature_Image($ClassTeacherLogin);
        }
        
        for($k=0; $k<sizeof($SignatureTitleArray); $k++)
        {
            $SettingID = trim($SignatureTitleArray[$k]);
            
            $tdwidth = "width='".(100/(sizeof($SignatureTitleArray)+1))."%'";
            $ContentOnTop = '';
            switch($SettingID)
            {
                case "Principal":
                    $DisplayName = "(".$eReportCard['Template']['SchoolInfo']['Principal'].")";
                    $imgfile = "/file/reportcard2008/templates/carmel_alison_principal.png";
                    $ContentOnTop = "<img src=\"{$imgfile}\" width=\"120\">\n";
                    break;
                case "ClassTeacher":
                    $DisplayName = "(".$ClassTeacherStr.")";
                    $ContentOnTop = $TeacherSignature;
                    $tdwidth = '';
                    //					$ContentOnTop = "<img src=\"{$imgfile}\" width=\"120\">\n";
                    break;
                case "IssueDate":
                    $ContentOnTop = ($Issued)? $Issued : "&nbsp;";
                    $DisplayName = '&nbsp;';
                    break;
                default:
                    $DisplayName = '&nbsp;';
            }
            
            $Title = $eReportCard['Template'][$SettingID];
            //$IssueDate = ($SettingID == "IssueDate" && $Issued)	? $Issued : "&nbsp;";
            $underlineWidth = ($SettingID == "IssueDate" )? "width='100%'" : "width='90%'";
            
            $SignatureTable .= "<td valign='bottom' align='center' $tdwidth>";
            $SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' $underlineWidth>";
            $SignatureTable .= "<tr><td align='center' class='fontsize14' valign='bottom'>". $ContentOnTop ."</td></tr>";
            $SignatureTable .= "<tr><td align='center' class='fontsize10 border_top' height='25' valign='top' nowrap>".$DisplayName."</td></tr>";
            $SignatureTable .= "<tr><td align='center' class='fontsize14' valign='bottom' nowrap>".$Title."</td></tr>";
            $SignatureTable .= "</table>";
            $SignatureTable .= "</td>";
            $SignatureTable .= ($SettingID == "IssueDate" && $Issued)?$emptytd:"";
        }
        
        $SignatureTable .= "</tr>";
        $SignatureTable .= "</table>";
        
        return $SignatureTable;
    }
    
    function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
    {
        global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT, $isSimpleReport;
        
        $ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
        $LineHeight 				= $ReportSetting['LineHeight'];
        $ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
        $ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
        $ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
        $ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
        $ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
        $ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
        $ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
        $ClassLevelID 				= $ReportSetting['ClassLevelID'];
        $SemID 						= $ReportSetting['Semester'];
        $ReportType 				= $SemID == "F" ? "W" : "T";
        $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
        $OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
        $OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
        $isMainReport				= 1;
        
        $ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
        
        $CalSetting = $this->LOAD_SETTING("Calculation");
        $CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
        
        $ColumnTitle = $this->returnReportColoumnTitle($ReportID);
        
        # retrieve Student Class
        if($StudentID)
        {
            include_once($PATH_WRT_ROOT."includes/libuser.php");
            $lu = new libuser($StudentID);
            $ClassName 		= $this->Get_Student_ClassName($StudentID);
            $WebSamsRegNo 	= $lu->WebSamsRegNo;
        }
        
        # retrieve result data
        $result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
        $GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
        $GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
        
        $ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
        $FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
        $ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
        $FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
        
        $ClassPosition = $ClassPosition."/".$ClassNumOfStudent;
        $FormPosition = $FormPosition."/".$FormNumOfStudent;
        //if ($CalOrder == 2)
        if ($ReportType == "W")
        {
            foreach ($ColumnTitle as $ColumnID => $ColumnName) {
                
                $ColumnData = $this->returnReportTemplateColumnData($ReportID, " ReportColumnID='$ColumnID' ");
                $thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[0]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
                
                $thisReportID = $thisReport['ReportID'];
                
                $columnResult = $this->getReportResultScore($thisReportID, 0, $StudentID, '', 1,1);
                
                $columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $thisReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
                $columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $thisReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
                $columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $thisReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
                $columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $thisReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
                $columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $thisReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
                $columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $thisReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
                
                $columnClassPos[$ColumnID] = $columnClassPos[$ColumnID]."/".$columnClassNumOfStudent[$ColumnID];
                $columnFormPos[$ColumnID] = $columnFormPos[$ColumnID]."/".$columnFormNumOfStudent[$ColumnID];
            }
        }
        else
        {
            $columnResult = $this->getReportResultScore($ReportID, 0, $StudentID, '', 1,1);
            
            $columnTotal[0] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
            $columnAverage[0] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
            
            $columnClassPos[0] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
            $columnFormPos[0] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
            $columnClassNumOfStudent[0] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
            $columnFormNumOfStudent[0] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
            
            $columnClassPos[0] = $columnClassPos[0]."/".$columnClassNumOfStudent[0];
            $columnFormPos[0] = $columnFormPos[0]."/".$columnFormNumOfStudent[0];
            
        }
        
        $first = 1;
        # Overall Result
        /*if($ShowGrandTotal)
         {
         $thisTitleEn = $eReportCard['Template']['OverallResultEn'];
         $thisTitleCh = $eReportCard['Template']['OverallResultCh'];
         $x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
         
         $first = 0;
         }*/
        
        if ($eRCTemplateSetting['HideCSVInfo'] != true)
        {
            
            # Position in Form
            //hdebug_pr($ShowOverallPositionForm);
            if($ShowOverallPositionForm)
            {
                if($isSimpleReport)
                {
                    $thisTitleEn = $eReportCard['Template']['SimpleReport']['PositionEn'];
                    $thisTitleCh = $eReportCard['Template']['SimpleReport']['PositionCh'];
                }
                else
                {
                    $thisTitleEn = $eReportCard['Template']['FormPositionEn'];
                    $thisTitleCh = $eReportCard['Template']['FormPositionCh'];
                }
                $x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first);
                
                $first = 0;
            }
            
            # Position in Class
            if($ShowOverallPositionClass && !$isSimpleReport)
            {
                $thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
                $thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
                $x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first);
                
                $first = 0;
            }
            
            ##################################################################################
            # CSV related
            ##################################################################################
            # build data array
            $ary = array();
            $csvType = $this->getOtherInfoType();
            
            //modified by Marcus 20/8/2009
            $OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
            $ary=$OtherInfoDataAry[$StudentID];
            
            if($SemID=="F")
            {
                $ColumnData = $this->returnReportTemplateColumnData($ReportID);
                
                /*
                 foreach($ColumnData as $k1=>$d1)
                 {
                     $TermID = $d1['SemesterNum'];
                     $InfoTermID = $TermID;
                 
                    if(!empty($csvType))
                    {
                        foreach($csvType as $k=>$Type)
                        {
                            $csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
                            if(!empty($csvData))
                            {
                                foreach($csvData as $RegNo=>$data)
                                {
                                    if($RegNo == $WebSamsRegNo)
                                    {
                                        foreach($data as $key=>$val)
                                            $ary[$TermID][$key] = $val;
                                    }
                                }
                            }
                        }
                    
                 }
                 */
                 
                 # calculate sems/assesment col#
                 $ColNum2Ary = array();
                 foreach($ColumnData as $k1=>$d1)
                 {
                     $TermID = $d1['SemesterNum'];
                     if($d1['IsDetails']==1)
                     {
                         # check sems/assesment col#
                         $thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
                         $thisReportID = $thisReport['ReportID'];
                         $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                         $ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
                     }
                     else
                         $ColNum2Ary[$TermID] = 0;
                 }
            }
            /*
             else
             {
                 $InfoTermID = $SemID;
                 
                 if(!empty($csvType))
                 {
                    foreach($csvType as $k=>$Type)
                    {
                        $csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
                        if(!empty($csvData))
                        {
                            foreach($csvData as $RegNo=>$data)
                            {
                                if($RegNo == $WebSamsRegNo)
                                {
                                    foreach($data as $key=>$val)
                                        $ary[$SemID][$key] = $val;
                                }
                            }
                         }
                     }
                 }
             }
             */
             
             $border_top = $first ? "border_top" : "";
             
             /*# Days Absent
              $thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
              $thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
              $x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
              
              # Times Late
              $thisTitleEn = $eReportCard['Template']['TimesLateEn'];
              $thisTitleCh = $eReportCard['Template']['TimesLateCh'];
              $x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Time Late", $ary);
              
              # Conduct
              $thisTitleEn = $eReportCard['Template']['ConductEn'];
              $thisTitleCh = $eReportCard['Template']['ConductCh'];
              $x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);*/
        }
        
        return $x;
    }
    
    function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
    {
        global $eReportCard, $isSimpleReport;
        
        # Retrieve Display Settings
        $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID 				= $ReportSetting['Semester'];
        $ReportType 		= $SemID == "F" ? "W" : "T";
        $ClassLevelID 		= $ReportSetting['ClassLevelID'];
        $ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
        $ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
        $ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
        $ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
        $ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
        $ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
        $isMainReport				= 1;
        $formNumber = $this->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
        
        # updated on 08 Dec 2008 by Ivan
        # if subject overall column is not shown, grand total, grand average... also cannot be shown
        //$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
        $ShowRightestColumn = $ShowSubjectOverall;
        
        # Retrieve Calculation Settings
        $CalSetting = $this->LOAD_SETTING("Calculation");
        $UseWeightedMark = $CalSetting['UseWeightedMark'];
        $CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
        
        $StorageSetting = $this->LOAD_SETTING("Storage&Display");
        $SubjectDecimal = $StorageSetting["SubjectScore"];
        $SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
        
        $SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
        $MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
        
        $subjectCodeMappingAry = $this->GET_SUBJECTS_CODEID_MAP($withComponent = 0, $mapByCode = 0);
        
        if(sizeof($MainSubjectArray) > 0) {
            foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
        }
        
        $n = 0;
        $x = array();
        $isFirst = 1;
        
        //$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
        
        ########################### Temrs Report Type ##############################
        /*if($ReportType=="T")
         {
             $CalculationOrder = $CalSetting["OrderTerm"];
             
             $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
             $ColumnID = array();
             $ColumnTitle = array();
             if(sizeof($ColoumnTitle) > 0)
                 foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
                 
             foreach($SubjectArray as $SubjectID => $SubjectName)
             {
                 $isSub = 0;
                 if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
                 
                 // check if it is a parent subject, if yes find info of its components subjects
                 $CmpSubjectArr = array();
                 $isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
                 if (!$isSub) {
                     $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                     if(!empty($CmpSubjectArr)) $isParentSubject = 1;
                 }
                 
                 # define css
                 $css_border_top =  "border_top";
                 
                 # Retrieve Subject Scheme ID & settings
                 $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
                 $SchemeID = $SubjectFormGradingSettings['SchemeID'];
                 $SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
                 $ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
                 $ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
                 
                 $isAllNA = true;
                 
                 # Assessment Marks & Display
                 for($i=0;$i<sizeof($ColumnID);$i++)
                 {
                     $thisColumnID = $ColumnID[$i];
                     $columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
                     $columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
                     $columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
                     
                     $isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
                     $isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
                 
                     if ($isSub && $columnSubjectWeightTemp == 0)
                     {
                        $thisMarkDisplay = $this->EmptySymbol;
                     }
                     else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
                        $thisMarkDisplay = $this->EmptySymbol;
                     } else {
                         $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
                         $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
                         
                         $thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
                         $thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
                         
                         $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                         $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                         
                         # for preview purpose
                         if(!$StudentID)
                         {
                             $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                             $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                         }
                         
                         $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
                         
                         if ($thisMark != "N.A.")
                         {
                            $isAllNA = false;
                         }
                         
                         # check special case
                         list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
                         
                         if($needStyle)
                         {
                            if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
                            {
                                $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                            }
                            else
                            {
                                $thisMarkTemp = $thisMark;
                            }
                         
                            $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
                         }
                         else
                         {
                             $thisMarkDisplay = $thisMark;
                         }
                         
                         //	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
                         //	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
                         //	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark;
                    }
                     
                    $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                    $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                    $x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                    
                    if($ShowSubjectFullMark)
                    {
                         # check special full mark
                         $SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
                         $SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
                         $thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
                         
                         $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
                         $x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
                     }
                     
                     $x[$SubjectID] .= "</tr></table>";
                     $x[$SubjectID] .= "</td>";
                 }
                 
                 # Subject Overall (disable when calculation method is Vertical-Horizontal)
                 if($ShowSubjectOverall)
                 {
                     $thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
                     $thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
                     
                     $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                     $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                     
                     # for preview purpose
                     if(!$StudentID)
                     {
                         $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                         $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                     }
                     
                     #$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
                     $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
                     
                     if ($thisMark != "N.A.")
                     {
                         $isAllNA = false;
                     }
                     
                     # check special case
                     if ($CalculationMethod==2 && $isSub)
                     {
                         $thisMarkDisplay = $this->EmptySymbol;
                     }
                     else
                     {
                         list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
                         if($needStyle)
                         {
                             if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
                                $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                             else
                                $thisMarkTemp = $thisMark;
                             
                             $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
                         }
                         else
                            $thisMarkDisplay = $thisMark;
                     }
                     
                     $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                     $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                     $x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                     if($ShowSubjectFullMark)
                     {
                         $SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
                         $SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
                         $thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
                         $x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
                     }
                     $x[$SubjectID] .= "</tr></table>";
                     $x[$SubjectID] .= "</td>";
                     } else if ($ShowRightestColumn) {
                        $x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
                     }
                     $isFirst = 0;
                     
                     # construct an array to return
                     $returnArr['HTML'][$SubjectID] = $x[$SubjectID];
                     $returnArr['isAllNA'][$SubjectID] = $isAllNA;
                 }
             } # End if($ReportType=="T")
         else
         {
         */
         ################################## Whole Year Report Type######################################
         
         $CalculationOrder = $CalSetting["OrderFullYear"];
         
         # Retrieve Invloved Temrs
         $ColumnData = $this->returnReportTemplateColumnData($ReportID);
         
         $SubjectFormGradingSettingsAry = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '' ,0 ,0, $ReportID);
         $SubjectFormGradingSettingsAry = BuildMultiKeyAssoc($SubjectFormGradingSettingsAry, array("SubjectID"));
         
         $msTableMarksFontClass = $isSimpleReport?"simple_ms_subject_title_mark":"tabletext";
         
         foreach($SubjectArray as $SubjectID => $SubjectName)
         {
             # Retrieve Subject Scheme ID & settings
             //$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
             $SubjectFormGradingSettings = $SubjectFormGradingSettingsAry[$SubjectID];
             $SchemeID = $SubjectFormGradingSettings['SchemeID'];
             //$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
             $ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
             $ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
             
             //2018-0213-1228-07066: show English subject score as "#" for Form1 report only
             $_subjectWebSAMSCode = $subjectCodeMappingAry[$SubjectID];
             if ($formNumber==1 && $_subjectWebSAMSCode=='165') {
                 $_hideMarkDisplay = true;
             }
             else {
                 $_hideMarkDisplay = false;
             }
             
             $t = "";
             $isSub = 0;
             if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
             
             // check if it is a parent subject, if yes find info of its components subjects
             $CmpSubjectArr = array();
             $isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
             if (!$isSub) {
                 $CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
                 if(!empty($CmpSubjectArr)) $isParentSubject = 1;
             }
             
             $isAllNA = true;
             $css_border_top =  "border_top";
             if(!$isSimpleReport)
             {
                 # Terms's Assesment / Terms Result
                 for($i=0;$i<sizeof($ColumnData);$i++)
                 {
                     #$css_border_top = ($isFirst or $isSub)? "" : "border_top";
                     
                     $isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
                     //if($isDetails==1)		# Retrieve assesments' marks
                     if($ReportType=="W")
                     {
                         # See if any term reports available
                         $thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
                         
                         # if no term reports, CANNOT retrieve assessment result at all
                         if (empty($thisReport['ReportID'])) {
                             for($j=0;$j<sizeof($ColumnID);$j++) {
                                 $x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
                             }
                         } else {
                             $thisReportID = $thisReport['ReportID'];
                             
                             # gen SD and Average
                             if(!isset($SDAry[$thisReportID])||!isset($AvgAry[$thisReportID])) {
                                 list($SDAry[$thisReportID],$AvgAry[$thisReportID]) = $this->getFormSubjectSDAndAverage($thisReportID);
                             }
                             $SD = $SDAry[$thisReportID][$SubjectID];
                             $AVG = $AvgAry[$thisReportID][$SubjectID];
                             
                             # Get GrandSDScore
                             $thisGrandScore = $this->getReportResultScore($thisReportID, 0, $StudentID);
                             $GrandSDScoreArr[$thisReportID] = $thisGrandScore["GrandSDScore"];
                             
                             $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                             $ColumnID = array();
                             $ColumnTitle = array();
                             foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
                             
                             $thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
                             if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)))
                             {
                                 $x[$SubjectID] .= "<td  class='border_left {$css_border_top}'>&nbsp;</td>";
                                 $x[$SubjectID] .= "<td  class='border_left {$css_border_top}'>&nbsp;</td>";
                                 $x[$SubjectID] .= "<td  class='border_left {$css_border_top}'>&nbsp;</td>";
                                 $x[$SubjectID] .= "<td  class='border_left {$css_border_top}'>&nbsp;</td>";
                             }
                             
                             for($j=0;$j<sizeof($ColumnID);$j++)
                             {
                                 $thisColumnID = $ColumnID[$j];
                                 $columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
                                 $columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
                                 $columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
                                 
                                 $isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
                                 $isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
                                 
                                 /*if ($isSub && $columnSubjectWeightTemp == 0)
                                  {
                                  $thisMarkDisplay = $this->EmptySymbol;
                                  }
                                  else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
                                  $thisMarkDisplay = $this->EmptySymbol;
                                  } else {*/
                                 $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "(ReportColumnID=0 Or ReportColumnID is Null) and SubjectID = '$SubjectID' ");
                                 $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
                                 
                                 $thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
                                 $thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
                                 
                                 $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                                 $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                                 
                                 # for preview purpose
                                 if(!$StudentID)
                                 {
                                     $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                                     $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                                 }
                                 $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
                                 
                                 if ($thisMark != "N.A.")
                                 {
                                     $isAllNA = false;
                                 }
                                 
                                 # check special case
                                 list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
                                 if($needStyle)
                                 {
                                     if ($thisSubjectWeight>0 && $ScaleDisplay=="M") {
                                         $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                                     }
                                     else {
                                         $thisMarkTemp = $thisMark;
                                     }
                                     $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, $thisMarkTemp,'',$StudentID,$thisColumnID);
                                         
//									 $oldMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp,'',$StudentID);
//									 global $wrongStdID;
//									 if($thisMarkDisplay!=$oldMarkDisplay)
//									 {
//									     $wrongStdID[$StudentID][$SubjectID][Column] = $thisColumnID;
//										 $wrongStdID[$StudentID][$SubjectID][From] = $oldMarkDisplay;
//										 $wrongStdID[$StudentID][$SubjectID][To] = $thisMarkDisplay;
//								     }
                                 }
                                 else
                                 {
                                     $thisMarkDisplay = $thisMark;
                                 }
//								 $thisPosition = $thisMarksAry[$SubjectID][$ColumnID[$j]]["OrderMeritForm"];
//								 debug_pr($thisMarksAry[$SubjectID]);
//								 $thisNumOfStudent = $thisMarksAry[$SubjectID][$ColumnID[$j]]["FormNoOfStudent"];
                                 $thisOverallMark = $thisMarksAry[$SubjectID][0]["FormNoOfStudent"];
                                 //}
                                 
                                 if ($_hideMarkDisplay) {
                                     $thisMarkDisplay = '#';
                                 }
                                 
                                 $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                                 $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                                 $x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                                 
                                 if($ShowSubjectFullMark)
                                 {
                                     $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
                                     $x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
                                 }
                                 
                                 $x[$SubjectID] .= "</tr></table>";
                                 
                                 $x[$SubjectID] .= "</td>";
                             }
                             
                             # show term subject Overall ans position
                             $thisMSGrade = $thisMarksAry[$SubjectID][0]['Grade'];
                             $thisMSMark = $thisMarksAry[$SubjectID][0]['Mark'];
                             $thisSDScore= $thisMarksAry[$SubjectID][0]['SDScore'];
                             
                             $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                             $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                             
                             # for preview purpose
                             if(!$StudentID)
                             {
                                 $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                                 $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                             }
                             $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
                             
                             $StandardMark = 0;
                             if ($thisMark != "N.A.")
                             {
                                 $isAllNA = false;
                                 $StandardMark = is_numeric($thisMark)?$thisMark:0;
                             }
                             
                             # check special case
                             list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][0]['Grade'],'','',$StudentID);
                             if($needStyle)
                             {
                                 if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
                                 {
                                     $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                                 }
                                 else
                                 {
                                     $thisMarkTemp = $thisMark;
                                 }
                                 
                                 $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, $thisMarkTemp,'',$StudentID);
                             }
                             else
                             {
                                 $thisMarkDisplay = $thisMark;
                             }
                             
                             if ($_hideMarkDisplay) {
                                 $thisMarkDisplay = '#';
                             }
                             
                             $thisPosition = $thisMarksAry[$SubjectID][0]["OrderMeritForm"];
                             $thisNumOfStudent = $thisMarksAry[$SubjectID][0]["FormNoOfStudent"];
                             //client request to hide this cust, 2011-0216-1138-00096 - 迦密愛禮信中學 - data display problem when report card print
                             //							$star = ($ScaleDisplay=="M" && $thisNumOfStudent>0 && ($thisPosition / $thisNumOfStudent) <= 0.1)? "*" : "";
                             
                             $thisRankingDisplay = $thisPosition > 0 && $thisNumOfStudent > 0? $thisPosition."/".$thisNumOfStudent : $this->EmptySymbolLine;
                             if ($_hideMarkDisplay) {
                                 $thisRankingDisplay = '--';
                             }
                             
                             $x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>".($thisMarkDisplay?$thisMarkDisplay.$star:$this->EmptySymbol) ."</td>";
                             $x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>". $thisRankingDisplay ."</td>";
                             
                             if ($ScaleDisplay == 'G' || in_array($thisMark,$this->GET_SPECIAL_CASE_SET2_STRING_ARR())||in_array($thisMark,$this->GET_SPECIAL_CASE_SET1_STRING_ARR()))
                             {
                                 $thisSDScoreDisplay = '--';
                                 $thisSDScoreAverageDisplay = '--';
                             }
                             else
                             {
                                 $thisSDScoreDisplay = ($thisSDScore)? my_round($thisSDScore * $thisSubjectWeight, 4) : 0;
                                 $thisSDScoreAverageDisplay = my_round($AVG, 2);
                             }
                             
                             if ($_hideMarkDisplay) {
                                 $thisSDScoreDisplay = '#';
                                 $thisSDScoreAverageDisplay = '#';
                             }
                             
                             if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)))
                             {
                                 $SDAvgTable[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>&nbsp;</td>";
                                 $SDAvgTable[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>&nbsp;</td>";
                             }
                             
                             $SDAvgTable[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>".$thisSDScoreDisplay."</td>";
                             $SDAvgTable[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>".$thisSDScoreAverageDisplay."</td>";
                             
                             if(!$isSub)
                             {
                                 $totalweight[$thisReportID] += $columnSubjectWeightTemp;
                                 $weightedSD[$thisReportID] += $columnSubjectWeightTemp*$StandardMark;
                             }
                         }
                     }
                     else	# Retrieve Terms Overall marks (Term Report/ ExtraReport)
                     {
                         /*if ($isParentSubject && $CalculationOrder == 1) {
                          $thisMarkDisplay = $this->EmptySymbol;
                          } else {*/
                         # See if any term reports available
                         $thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
                         
                         # if no term reports, the term mark should be entered directly
                         if (empty($thisReport['ReportID'])) {
                             $thisReportID = $ReportID;
                             $MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
                             //$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
                             #$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : "";
                             //$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
                             
                             $thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
                             $thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
                             
                             $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                             $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                         } else {
                             $thisReportID = $thisReport['ReportID'];
                             $MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
                             //$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
                             #$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : "";
                             //$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
                             
                             $thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
                             $thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
                             
                             $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                             $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                         }
                         
                         # Get GrandSDScore
                         $thisGrandScore = $this->getReportResultScore($thisReportID, 0, $StudentID);
                         $GrandSDScoreArr[$thisReportID] = $thisGrandScore["GrandSDScore"];
                         
                         //$thisColumnID = $ColumnID[$i];
                         $columnWeightConds = " (ReportColumnID = '0' OR ReportColumnID IS NULL) AND SubjectID = '$SubjectID' " ;
                         $columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
                         $columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
                         
                         $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
                         $thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
                         
                         # for preview purpose
                         if(!$StudentID)
                         {
                             $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                             $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                         }
                         # If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
                         $thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
                         if ($thisMark != "N.A.")
                         {
                             $isAllNA = false;
                         }
                         
                         # check special case
                         list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
                         if($needStyle)
                         {
                             if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
                             {
                                 $thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
                             }
                             else
                             {
                                 $thisMarkTemp = $thisMark;
                             }
                             
                             $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp,'',$StudentID,$ColumnData[$i]['ReportColumnID']);
                         }
                         else
                         {
                             $thisMarkDisplay = $thisMark;
                         }
                         //}
                         
                         if ($_hideMarkDisplay) {
                             $thisMarkDisplay= '#';
                         }
                         
                         $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                         $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                         $x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
                         
                         /*if($ShowSubjectFullMark)
                          {
                          $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
                          $x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
                          }*/
                         
                         $x[$SubjectID] .= "</tr></table>";
                         $x[$SubjectID] .= "</td>";
                     }
                 }
             }
             
             $thisMarksAry = $this->getMarks($ReportID, $StudentID,"","",1);
             
             # gen SD and Average
             if(!isset($SDAry[$ReportID])||!isset($AvgAry[$ReportID]))
             {
                 list($SDAry[$ReportID],$AvgAry[$ReportID]) = $this->getFormSubjectSDAndAverage($ReportID);
             }
             $SD = $SDAry[$ReportID][$SubjectID];
             $AVG = $AvgAry[$ReportID][$SubjectID];
             
             $thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
             $thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
             $thisSDScore = $thisMarksAry[$SubjectID][0]["SDScore"];
             $thisPosition = $thisMarksAry[$SubjectID][0]["OrderMeritForm"];
             $thisNumOfStudent = $thisMarksAry[$SubjectID][0]["FormNoOfStudent"];
             
             # Subject Overall (hide in Extra Report)
             if($isMainReport)
             {
//			     $thisMarksAry = $this->getMarks($ReportID, $StudentID,"","",1);
//
//				 #gen SD and Average
//				 if(!isset($SDAry[$ReportID])||!isset($AvgAry[$ReportID]))
//				 {
//				      list($SDAry[$ReportID],$AvgAry[$ReportID]) = $this->getFormSubjectSDAndAverage($ReportID);
//				 }
//				 $SD = $SDAry[$thisReportID][$SubjectID];
//			     $AVG = $AvgAry[$thisReportID][$SubjectID];
//
//			     $thisMSGrade = $thisMarksAry[$SubjectID][0]["Grade"];
//			     $thisMSMark = $thisMarksAry[$SubjectID][0]["Mark"];
//			     $thisSDScore = $thisMarksAry[$SubjectID][0]["SDScore"];
//			     $thisPosition = $thisMarksAry[$SubjectID][0]["OrderMeritForm"];
//			     $thisNumOfStudent = $thisMarksAry[$SubjectID][0]["FormNoOfStudent"];
                 
                 $thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
                 $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
                 
                 # for preview purpose
                 if(!$StudentID)
                 {
                     $thisMark 	= $ScaleDisplay=="M" ? "S" : "";
                     $thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
                 }
                 
                 $thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? $thisMark : $thisMark) : $thisGrade;
                 if ($thisMark != "N.A.")
                 {
                     $isAllNA = false;
                     $StandardMark = is_numeric($thisMark)?$thisMark:0;
                 }
                 
                 //if ($CalculationMethod==2 && $isSub)
                 //{
                 //	$thisMarkDisplay = $this->EmptySymbol;
                 //}
                 //else
                 //{
                        # check special case
                        list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
                        if ($_hideMarkDisplay) {
                             $thisMark = '#';
                        }
                         
                        if($needStyle) {
                             $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID,'','',$StudentID);
                        }
                        else {
                             $thisMarkDisplay = $thisMark;
                        }
                 //}
                 
                 // client request to hide this cust, 2011-0216-1138-00096 - 迦密愛禮信中學 - data display problem when report card print
                 //					$star = ($ScaleDisplay=="M" && $thisNumOfStudent>0 && ($thisPosition / $thisNumOfStudent) <= 0.1)? "*" : "";
                     
                 $x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
                 $x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
                 $x[$SubjectID] .= "<td class='$msTableMarksFontClass' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay.$star."</td>";
                 
                 if ($ScaleDisplay == 'G' || in_array($thisMark,$this->GET_SPECIAL_CASE_SET2_STRING_ARR())||in_array($thisMark,$this->GET_SPECIAL_CASE_SET1_STRING_ARR()))
                 {
                     $thisSDScoreDisplay = '--';
                     $thisSDScoreAverageDisplay = '--';
                 }
                 else
                 {
                     $thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID=0 Or ReportColumnID is Null) and SubjectID = '$SubjectID' ");
                     $columnSubjectWeightTemp = $thisSubjectWeightData[0]['Weight'];
                     
                     $thisSDScoreDisplay = ($thisSDScore)? my_round($thisSDScore * $columnSubjectWeightTemp, 4) : 0;
                     //hdebug_pr($columnSubjectWeightTemp);
                     $thisSDScoreAverageDisplay = my_round($AVG, 2);
                 }
                 
                 if ($_hideMarkDisplay) {
                     $thisSDScoreDisplay = '#';
                     $thisSDScoreAverageDisplay = '#';
                 }
                 
                 # for SDAvgTable
                 $SDAvgTable[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>".$thisSDScoreDisplay."</td>";
                 $SDAvgTable[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='center'>".$thisSDScoreAverageDisplay."</td>";
                 
                 if(!$isSub)
                 {
                     $totalweight[0] += $columnSubjectWeightTemp;
                     $weightedSD[0] += $columnSubjectWeightTemp*$StandardMark;
                 }
                 
                 /*if($ShowSubjectFullMark)
                  {
                  # check special full mark
                  $SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
                  $SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
                  $thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
                  
                  $FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
                  $x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
                  }*/
                 
                 $x[$SubjectID] .= "</tr></table>";
                 $x[$SubjectID] .= "</td>";
            }
            
            # Ranking and NumOfStudent
            if ($thisPosition > 0) {
                 $thisRanking = $ScaleDisplay=="M"?$thisPosition."/".$thisNumOfStudent:$this->EmptySymbol;
            }
            else {
                 $thisRanking = $this->EmptySymbolLine;
            }
            
            if ($_hideMarkDisplay) {
                 $thisRanking = '--';
            }
             
            if(!$isMainReport && $thisMarkDisplay=="/") {
                 $thisRanking = $this->EmptySymbolLine;
            }
            
            $x[$SubjectID] .= "<td class='$msTableMarksFontClass border_left {$css_border_top}' align='center'>". $thisRanking ."</td>";
             
            $isFirst = 0;
             
            # construct an array to return
            $returnArr['HTML'][$SubjectID] = $x[$SubjectID];
            $returnArr['isAllNA'][$SubjectID] = $isAllNA;
            $returnArr['SDAvgTable'][$SubjectID] = $SDAvgTable[$SubjectID];
        }
             
        if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7))&&$ReportType=="W") {
            $overallStandardMark .= "<td class='tabletext border_left {$css_border_top}' align='center' colspan='2'>&nbsp;</td>";
        }
        
        foreach((array)$GrandSDScoreArr as $thisReportID =>$thisGrandSDScore)
        {
            $thisGrandSDScoreDisplay = ($thisGrandSDScore=='')? 0 : my_round($thisGrandSDScore, 4);
            $overallStandardMark .= "<td class='tabletext border_left {$css_border_top}' align='center' colspan='2'>".$thisGrandSDScoreDisplay."</td>";
        }
        
        if($ReportType=="W")
        {
             # Get GrandSDScore
             $thisGrandScore = $this->getReportResultScore($ReportID, 0, $StudentID);
             $thisGrandSDScore = $thisGrandScore["GrandSDScore"];
             $thisGrandSDScoreDisplay = ($thisGrandSDScore=='')? 0 : my_round($thisGrandSDScore, 4);
             $overallStandardMark .= "<td class='tabletext border_left {$css_border_top}' align='center' colspan='2'>".$thisGrandSDScoreDisplay."</td>";
        }
         
        $returnArr['SDAvgTable'][0] = $overallStandardMark;
        //}	# End Whole Year Report Type
        
        return $returnArr;
    }
     
    function getMiscTable($ReportID, $StudentID='')
    {
         global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
         
         if ($eRCTemplateSetting['HideCSVInfo'] == true)
         {
             return "";
         }
         
         # Retrieve Basic Information of Report
         $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
         $SemID 						= $ReportSetting['Semester'];
         $ReportType 				= $SemID == "F" ? "W" : "T";
         $LineHeight					= $ReportSetting['LineHeight'];
         $AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
         $ClassLevelID = $ReportSetting['ClassLevelID'];
         
         $ColumnTitle = $this->returnReportColoumnTitle($ReportID);
         
         $Semester = $ReportSetting["Semester"];
         $SemName = $Semester!="F"&&in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7))?$this->returnSemesters($Semester):$eReportCard["Template"]["FirstTerm"];
         
         //modified by marcus 20/8/2009
         //return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
         $OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
         
         # retrieve the latest Term
         $latestTerm = "";
         $sems = $this->returnReportInvolvedSem($ReportID);
         foreach($sems as $TermID=>$TermName)
         {
             $latestTerm = $TermID;
         }
         //$latestTerm++;
         
         /*
          # retrieve Student Class
          if($StudentID)
          {
              include_once($PATH_WRT_ROOT."includes/libuser.php");
              $lu = new libuser($StudentID);
              $ClassName 		= $this->Get_Student_ClassName($StudentID);
              $WebSamsRegNo 	= $lu->WebSamsRegNo;
          }
          
          # build data array
          $ary = array();
          $csvType = $this->getOtherInfoType();
          foreach($csvType as $k=>$Type)
          {
              $csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);
              if(!empty($csvData))
              {
                  foreach($csvData as $RegNo=>$data)
                  {
                      if($RegNo == $WebSamsRegNo)
                      {
                          foreach($data as $key=>$val)
                             $ary[$key] = $val;
                      }
                  }
              }
          }
          */
          
          # retrieve result data
          /*foreach($sems as $TermID=>$TermName)
           {
               $latestTerm = $TermID;
               
               $ary = $OtherInfoDataAry[$StudentID][$TermID];
               
               # attendance
               $Absence[$TermID] = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
               $AbsenceComment[$TermID] = ($ary['Absent Comment'])? $ary['Absent Comment'] : 0;
               $Lateness[$TermID] = ($ary['Time Late'])? $ary['Time Late'] : 0;
               $LatenessComment[$TermID] = ($ary['Late Comment'])? $ary['Late Comment'] : 0;
               $SkipSchool[$TermID] = ($ary['Days Skip Sccool'])? $ary['Days Skip Sccool'] : 0;
               $SkipSchoolComment[$TermID] = ($ary['Skip Sccool Comment'])? $ary['Skip Sccool Comment'] : 0;
               $Presence[$TermID] = ($ary['Days Present'])? $ary['Days Present'] : 0;
               $PresenceComment[$TermID] = ($ary['Present Comment'])? $ary['Present Comment'] : 0;
               
               # summary
               $Diligence[$TermID] = ($ary['Diligence'])? $ary['Diligence'] : $this->EmptySymbol;
               $DiligenceComment[$TermID] = ($ary['Diligence Comment'])? $ary['Diligence Comment'] : $this->EmptySymbol;
               $Manner[$TermID] = ($ary['Manner'])? $ary['Manner'] : $this->EmptySymbol;
               $MannerComment[$TermID] = ($ary['Manner Comment'])? $ary['Manner Comment'] : $this->EmptySymbol;
               $Responsibilty[$TermID] = ($ary['Responsibilty'])? $ary['Responsibilty'] : $this->EmptySymbol;
               $ResponsibiltyComment[$TermID] = ($ary['Responsibilty Comment'])? $ary['Responsibilty Comment'] : $this->EmptySymbol;
               $Service[$TermID] = ($ary['Service'])? $ary['Service'] : $this->EmptySymbol;
               $ServiceComment[$TermID] = ($ary['Service Comment'])? $ary['Service Comment'] : $this->EmptySymbol;
               $ECAPerformance[$TermID] = ($ary['ECA Performance'])? $ary['ECA Performance'] : $this->EmptySymbol;
               $ECAPerformanceComment[$TermID] = ($ary['ECA Performance Comment'])? $ary['ECA Performance Comment'] : $this->EmptySymbol;
               $ReadingScheme[$TermID] = ($ary['Reading Scheme'])? $ary['Reading Scheme'] : $this->EmptySymbol;
               $ReadingSchemeComment[$TermID] = ($ary['Reading Scheme Comment'])? $ary['Reading Scheme Comment'] : $this->EmptySymbol;
               $StudentScheme[$TermID] = ($ary['Student Scheme'])? $ary['Student Scheme'] : $this->EmptySymbol;
               $StudentSchemeComment[$TermID] = ($ary['Student Scheme Comment'])? $ary['Student Scheme Comment'] : $this->EmptySymbol;
               
               # Merit
               $Merits[$TermID] = ($ary['Merits'])? $ary['Merits'] : 0;
               $MinorCredit[$TermID] = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
               $MajorCredit[$TermID] = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
               $Demerits[$TermID] = ($ary['Merits'])? $ary['Demerits'] : 0;
               $MinorFault[$TermID] = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
               $MajorFault[$TermID] = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
               $Remark[$TermID] = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
               $ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
           }
           
           if (is_array($ECA))
               $ecaList = implode("<br>", $ECA);
           else
               $ecaList = $ECA;
           */
           /*
            $merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
            $merit .= "<tr>";
            $merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
            $merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
            $merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
            $merit .= "</tr>";
            $merit .= "<tr>";
            $merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
            $merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
            $merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
            $merit .= "</tr>";
            $merit .= "</table>";
            $remark = $ary['Remark'];
            $CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
            $classteachercomment = $CommentAry[0];
            $eca = $ary['ECA'];
            if (is_array($eca))
                $ecaList = implode("<br>", $eca);
            else
                $ecaList = $eca;
            */
         $x .= "<br>";
            
         $Others = "<div class='fontsize14 small_title'>".$eReportCard['Template']['SubTitle']['OtherPerformance']."</div>";
         $Others .= "<table width=\"85%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class='report_border'>";
         $Others .= "<tr>";
         $Others .= "<td  width='45%'>&nbsp;</td>";
         /*foreach($sems as $TermID=>$TermName)
         {
         $Others .= "<td align='center' class='tabletext border_left' width='10%' nowrap>$TermName</td>";
         $emptyTD .="<td class='tabletext border_left border_top'>&nbsp;</td>";
         }*/
         $Others .= "<td align='center' class='tabletext border_left' width='50px' nowrap>".$SemName."</td>";
         if($ReportType=="W")
         {
            $Others .= "<td align='center' class='tabletext border_left' width='50px' nowrap>".$eReportCard['Template']['SecondTerm']."</td>";
            $emptyTD .="<td class='tabletext border_left border_top'>&nbsp;</td>";
         }
         $emptyTD .="<td class='tabletext border_left border_top'>&nbsp;</td>";
         $Others .= "<td align='center' class='tabletext border_left' width='35%'>".$eReportCard['Template']['CommentCh']."</td>";
         $Others .= "</tr>";
            
         //if($ReportType=="T")
         //$emptyInfo ="<td class='tabletext border_left border_top'>&nbsp;</td>";
            
         #attendance
         $attendanceOrder = array('Time Late','Days Absent','Days Skip School','Days Present');
         $Others .= "<tr>";
         $Others .= "<td align='left' class='tabletext border_top'>".$eReportCard["Template"]["Attendance"]."</td>$emptyTD<td class='tabletext border_left border_top'>&nbsp;</td>";
         $Others .= "</tr>";
         foreach($attendanceOrder as $OtherInfoType)
         {
            $Others .= "<tr>";
            $Others .= "<td align='left' class='tabletext border_top'>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard["Template"][$OtherInfoType]."</td>";
            if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)) && $ReportType=="W") {
                $Others .= "<td align='center' class='tabletext border_left border_top' width='10%'>&nbsp;</td>";
            }
                
            foreach($sems as $TermID=>$TermName)
            {
                $thisInfo = $OtherInfoDataAry[$StudentID][$TermID][$OtherInfoType];
                $Others .= "<td align='center' class='tabletext border_left border_top' width='10%'>".($thisInfo?$thisInfo:0)."</td>";
            }
            $Others .=$emptyInfo;
            switch($OtherInfoType)
            {
                case "Time Late": $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm]["Late Comment"]; break;
                case "Days Absent": $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm]["Absent Comment"]; break;
                case "Days Skip School": $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm]["Skip School Comment"]; break;
                case "Days Present": $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm]["Present Comment"]; break;
            }
            //$thisComment = $OtherInfoDataAry[$StudentID][$latestTerm][$OtherInfoType." Comment"];
            $Others .= "<td align='center' class='tabletext border_left border_top' width='35%'>".($thisComment?$thisComment:$this->EmptySymbol)."</td>";
            $Others .= "</tr>";
        }
        
        # Conducts
        $conductOrder = array('Diligence','Manner','Responsibility','Service');
        $Others .= "<tr><td align='left' class='tabletext border_top'>".$eReportCard["Template"]["Conduct"]."</td>$emptyTD<td class='tabletext border_left border_top'>&nbsp;</td></tr>";
        foreach($conductOrder as $OtherInfoType)
        {
            $Others .= "<tr>";
            $Others .= "<td align='left' class='tabletext border_top'>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard["Template"][$OtherInfoType]."</td>";
            if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)) && $ReportType=="W")
                $Others .= "<td align='center' class='tabletext border_left border_top' width='10%'>&nbsp;</td>";
                foreach($sems as $TermID=>$TermName)
                {
                    $thisInfo = $OtherInfoDataAry[$StudentID][$TermID][$OtherInfoType];
                    $Others .= "<td align='center' class='tabletext border_left border_top' width='15%'>".($thisInfo?$thisInfo:0)."</td>";
                }
                $Others .=$emptyInfo;
                $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm][$OtherInfoType." Comment"];
                $Others .= "<td align='center' class='tabletext border_left border_top' width='25%'>".($thisComment?$thisComment:$this->EmptySymbol)."</td>";
                $Others .= "</tr>";
        }
        
        # ECA Performance & Scheme
        $SchemeOrder = array('ECA Performance','Reading Scheme','Student Scheme');
        foreach($SchemeOrder as $OtherInfoType)
        {
            $Others .= "<tr>";
            $Others .= "<td align='left' class='tabletext border_top'>".$eReportCard["Template"][$OtherInfoType]."</td>";
            if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)) && $ReportType=="W")
                $Others .= "<td align='center' class='tabletext border_left border_top' width='10%'>&nbsp;</td>";
                foreach($sems as $TermID=>$TermName)
                {
                    $thisInfo = $OtherInfoDataAry[$StudentID][$TermID][$OtherInfoType];
                    $Others .= "<td align='center' class='tabletext border_left border_top' width='10%'>".($thisInfo?$thisInfo:$this->EmptySymbol)."</td>";
                }
                $Others .=$emptyInfo;
                $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm][$OtherInfoType." Comment"];
                $Others .= "<td align='center' class='tabletext border_left border_top' width='35%'>".($thisComment?$thisComment:$this->EmptySymbol)."</td>";
                $Others .= "</tr>";
        }
        $Others .= "</table>";
        
        # Award Punishment Record
        //$tdwidth = "width=".(85/(sizeof($sems)+2))."%";
        $AP = "<div align='left' class='fontsize14 small_title'>".$eReportCard['Template']['SubTitle']['AwardPunishment']."</div>";
        $AP .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class='report_border'>";
        $AP .= "<tr>";
        $AP .= "<td width='10%'>&nbsp;</td>";
        $AP .= "<td class='border_left' width='20%'>&nbsp;</td>";
        /*foreach($sems as $TermID=>$TermName)
         {
         $AP .= "<td align='center' class='tabletext border_left' $tdwidth>$TermName</td>";
         $emptyTD .="<td class='tabletext border_left border_top'>&nbsp;</td>";
         }*/
        $AP .= "<td align='center' class='tabletext border_left' width='20%' nowrap>".$SemName."</td>";
        if($ReportType=="W") {
            $AP .= "<td align='center' class='tabletext border_left' width='20%' nowrap>".$eReportCard['Template']['SecondTerm']."</td>";
        }
        $emptyTD .="<td class='tabletext border_left border_top'>&nbsp;</td>";
        $emptyTD .="<td class='tabletext border_left border_top'>&nbsp;</td>";
        
        if($ReportType=="W") {
            $AP .= "<td align='center' class='tabletext border_left' width='20%'>".$eReportCard['Template']['WholeYearTotal']."</td>";
        }
        $AP .= "</tr>";
        
        //if($ReportType=="T")
        //$emptyInfo ="<td class='tabletext border_left border_top'>&nbsp;</td>";
        
        #merit
        $meritOrder = array('Merits','Minor Credit','Major Credit');
        $firstcol .= "<td align='center' class='tabletext border_top' rowspan='3'>".$eReportCard["Template"]["MeritType"]."</td>";
        foreach($meritOrder as $OtherInfoType)
        {
            $AP .= "<tr>";
            $AP .= $firstcol;
            $firstcol='';
            $thisTitleField = str_replace(' ', '', $OtherInfoType);
            $AP .= "<td align='center' class='tabletext border_top border_left'>".$eReportCard["Template"][$thisTitleField]."</td>";
            if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)) && $ReportType=="W") {
                $AP .= "<td align='center' class='tabletext border_left border_top' width='10%'>&nbsp;</td>";
            }
            $wholtyeartotal = 0;
            foreach($sems as $TermID=>$TermName)
            {
                $thisInfo = $OtherInfoDataAry[$StudentID][$TermID][$OtherInfoType];
                $wholtyeartotal += $thisInfo;
                $AP .= "<td align='center' class='tabletext border_left border_top' width='10%'>".($thisInfo?$thisInfo:0)."</td>";
            }
            $thisComment = $OtherInfoDataAry[$StudentID][$latestTerm][$OtherInfoType." Comment"];
            $AP .= $emptyInfo;
            
            if($ReportType=="W") {
                $AP .= "<td align='center' class='tabletext border_left border_top' width='20%'>".($wholtyeartotal?$wholtyeartotal:0)."</td>";
            }
            $AP .= "</tr>";
        }
        
        # demerit
        $demeritOrder = array('Demerits','Minor Fault','Major Fault');
        $firstcol .= "<td align='center' class='tabletext border_top' rowspan='3'>".$eReportCard["Template"]["DemeritType"]."</td>";
        $wholtyeartotal = 0;
        foreach($demeritOrder as $OtherInfoType)
        {
            $AP .= "<tr>";
            $AP .= $firstcol;
            $firstcol = '';
            $thisTitleField = str_replace(' ', '', $OtherInfoType);
            $AP .= "<td align='center' class='tabletext border_top border_left'>".$eReportCard["Template"][$thisTitleField]."</td>";
            if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)) && $ReportType=="W") {
                $AP .= "<td align='center' class='tabletext border_left border_top' width='10%'>&nbsp;</td>";
            }
            $wholtyeartotal = 0;
            foreach($sems as $TermID=>$TermName)
            {
                $thisInfo = $OtherInfoDataAry[$StudentID][$TermID][$OtherInfoType];
                $wholtyeartotal += $thisInfo;
                $AP .= "<td align='center' class='tabletext border_left border_top' width='10%'>".($thisInfo?$thisInfo:0)."</td>";
            }
            $AP .= $emptyInfo;
            
            if($ReportType=="W") {
                $AP .= "<td align='center' class='tabletext border_left border_top' width='20%'>".($wholtyeartotal?$wholtyeartotal:0)."</td>";
            }
            $AP .= "</tr>";
        }
        $AP .= "</table>";
        
        # Promotion
        $PromotionStatus = "&nbsp;";
        if($ReportType == "W")
        {
            $PromotionStatus = $OtherInfoDataAry[$StudentID][$latestTerm]["Remark"]?$OtherInfoDataAry[$StudentID][$TermID]["Remark"]:"&nbsp;";
            $Promotion = "<div align='left' class='fontsize14 small_title'>".$eReportCard['Template']['SubTitle']['PromoteRetain']."</div>";
            $Promotion .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class='report_border' height='50px'>";
            $Promotion .= "<tr>";
            $Promotion .= "<td align='center' class='fontsize14 small_title'>".$PromotionStatus."</td>";
            $Promotion .= "</tr>";
            $Promotion .= "</table>";
        }
        
        $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
        $x .= "<tr>";
        $x .= "<td width='65%' rowspan='2'>$Others</td>";
        $x .= "<td width='35%' valign='top'>$AP</td>";
        $x .= "</tr>";
        $x .= "<tr>";
        $x .= "<td valign='bottom'>$Promotion</td>";
        $x .= "</tr>";
        $x .= "</table>";
        
        /*$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
         $x .= "<tr>";
         $x .= "<td width='50%' valign='top'>";
         # Merits & Demerits
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
         $x .= "<tr>";
         $x .= "<td class='tabletext'><b>". $eReportCard['Template']['MeritsDemerits']."</b><br>".$merit."</td>";
         $x .= "</tr>";
         $x .= "</table>";
         $x .= "</td></tr></table>";
         $x .="</td>";
         $x .= "<td width='50%' class=\"border_left\" valign='top'>";
         # Remark
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
         $x .= "<tr>";
         $x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
         $x .= "</tr>";
         $x .= "</table>";
         $x .= "</td></tr></table>";
         $x .="</td>";
         $x .= "</tr>";
         $x .= "</table>";
         
         $x .= "<br>";
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
         $x .= "<tr>";
         if($AllowClassTeacherComment)
         {
         $x .= "<td width='50%' valign='top'>";
         # Class Teacher Comment
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
         $x .= "<tr>";
         $x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".stripslashes(nl2br($classteachercomment))."</td>";
         $x .= "</tr>";
         $x .= "</table>";
         $x .= "</td></tr></table>";
         $x .="</td>";
         
         $x .= "<td width='50%' class='border_left' valign='top'>";
         } else {
         $x .= "<td width='50%' valign='top'>";
         }
         # ECA
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
         $x .= "<tr>";
         $x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$ecaList."</td>";
         $x .= "</tr>";
         $x .= "</table>";
         $x .= "</td></tr></table>";
         $x .="</td>";
         $x .= "</tr>";
         $x .= "</table>";*/
        
         return $x;
     }
     
     function Get_Page2_Table($ReportID, $StudentID='')
     {
         global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
         
         if ($eRCTemplateSetting['HideCSVInfo'] == true)
         {
             return "";
         }
         
         # Retrieve Basic Information of Report
         $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
         $SemID 						= $ReportSetting['Semester'];
         $ReportType 				= $SemID == "F" ? "W" : "T";
         $LineHeight					= $ReportSetting['LineHeight'];
         $AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
         
         //modified by marcus 20/8/2009
         //return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
         $OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
         # retrieve the latest Term
         $latestTerm = "";
         $sems = $this->returnReportInvolvedSem($ReportID);
         foreach($sems as $TermID=>$TermName)
         {
             $latestTerm = $TermID;
         }
         
         $OtherInfoArr = array("ECA","Post","Award","ClassTeacherComment");
         
         $x = '';
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
         $x .= "<tr>";
         $x .= "<td align='right' height='70px'>".$this->getReportStudentInfo($ReportID, $StudentID,1)."</td>";
         $x .= "</tr>";
         $x .= "<tr>";
         $x .= "<td valign='top'>";
         
         foreach($OtherInfoArr as $OtherInfoType)
         {
             if ($OtherInfoType == 'ClassTeacherComment')
             {
                 # Class Teacher Comment
                 $CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
                 $thisinfo = nl2br($CommentAry[0]);
             }
             else
             {
                 $thisinfo = $OtherInfoDataAry[$StudentID][$latestTerm][$OtherInfoType];
             }
             $x .= "<div class='fontsize14' style='padding-top:15px'>".$eReportCard["Template"][$OtherInfoType]."</div>";
             $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class='report_border' height='130px'>";
             $x .= "<tr>";
             $x .= "<td valign='top' class='fontsize13' height='130px'>";
             if(is_array($thisinfo))
                 $x .= implode("<br>",$thisinfo);
             else
                 $x .= $thisinfo?$thisinfo:$this->EmptySymbol;
                 $x .= "</td>";
                 $x .= "</tr>";
                 $x .= "</table>";
         }
         
         $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >";
         $x .= "<tr>";
         $x .= "<td valign='top' class='fontsize10'>"."<br>".nl2br($ReportSetting["Footer"])."</td>";
         $x .= "</tr>";
         $x .= "<tr>";
         $x .= "<td valign='top' class='fontsize14'>".$eReportCard['Template']['ItemsDisplayEnd']."</td>";
         $x .= "</tr>";
         $x .= "</table>";
         $x .= "</td>";
         $x .= "</tr>";
         $x .= "</table>";
         
         return $x;
     }
     
     ########### END Template Related
     
     function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
     {
         global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
         
         $ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
         $LineHeight 				= $ReportSetting['LineHeight'];
         $ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
         $ClassLevelID 				= $ReportSetting['ClassLevelID'];
         $SemID 						= $ReportSetting['Semester'];
         $ReportType 				= $SemID == "F" ? "W" : "T";
         $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
         
         # updated on 08 Dec 2008 by Ivan
         # if subject overall column is not shown, grand total, grand average... also cannot be shown
         //$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
         $ShowRightestColumn = $ShowSubjectOverall;
         
         # initialization
         $border_top = "";
         $x = "";
         
         $x .= "<tr>";
         $x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
         
         if($ReportType=="W")	# Whole Year Report
         {
             $ColumnData = $this->returnReportTemplateColumnData($ReportID);
             
             $thisTotalValue = "";
             foreach($ColumnData as $k1=>$d1)
             {
                 # get value of this term
                 $TermID = $d1['SemesterNum'];
                 
                 $thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
                 
                 # calculation the overall value
                 if (is_numeric($thisValue)) {
                     if ($thisTotalValue == "") {
                         $thisTotalValue = $thisValue;
                     }
                     else if (is_numeric($thisTotalValue)) {
                         $thisTotalValue += $thisValue;
                     }
                 }
                 
                 # insert empty cell for assessments
                 for($i=0;$i<$ColNum2Ary[$TermID];$i++) {
                     $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
                 }
                 
                 # display this term value
                 $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
             }
             
             # display overall year value
             if($ShowRightestColumn)
             {
                 if($thisTotalValue=="") {
                     $thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
                 }
                 $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
             }
         }
         else				# Term Report
         {
             # get value of this term
             $thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
             
             # insert empty cell for assessments
             for($i=0;$i<$ColNum2;$i++) {
                 $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
             }
             
             # display this term value
             if($ShowRightestColumn) {
                 $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
             }
         }
         
         if ($AllowSubjectTeacherComment) {
             $x .= "<td class='border_left $border_top' align='center'>";
             $x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
             $x .= "</td>";
         }
         $x .= "</tr>";
         
         return $x;
     }
     
     function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
     {
         global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT, $isSimpleReport;
         
         $ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
         $LineHeight 				= $ReportSetting['LineHeight'];
         $SemID 						= $ReportSetting['Semester'];
         $ReportType 				= $SemID == "F" ? "W" : "T";
         $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
         $isMainReport				= 1;
         
         $ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
         $CalOrder = $this->Get_Calculation_Setting($ReportID);
         $ColumnTitle = $this->returnReportColoumnTitle($ReportID);
         $ClassLevelID = $ReportSetting['ClassLevelID'];
         
         $border_top = "border_top" ;
         
         $x .= "<tr>";
         $x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, 1);
         
         
         /*if ($CalOrder == 1) {
          for($i=0;$i<$ColNum2;$i++)
          $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
          } else {*/
          if($ReportType == "W")
          {
              if(in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7)))
              {
                  $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan=4>".$this->EmptySymbol."</td>";
              }
              $curColumn = 0;
              foreach ($ColumnTitle as $ColumnID => $ColumnName) {
                  for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
                  {
                      $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
                  }
                  
                  $thisValue = $ValueArr[$ColumnID];
                  $x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan=4>".$thisValue."&nbsp;</td>";
                  $curColumn++;
              }
          }
          //}
          
          if ($ShowRightestColumn || 1)
          {
              $thisValue = $OverallValue;
              /*
               if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
               {
               $thisDisplay = $thisValue;
               }
               else
               {
               $thisDisplay = $this->EmptySymbol;
               }
               */
              $footerFontClass= ($isSimpleReport)?"simple_ms_table_footer":"tabletext";
              $colspan = ($isMainReport)?4:2;
              $x .= "<td class='$footerFontClass {$border_top} border_left' align='center' height='{$LineHeight}' colspan=$colspan>". $thisValue ."&nbsp;</td>";
          }
          
          if ($AllowSubjectTeacherComment) {
              $x .= "<td class='border_left $border_top' align='center'>";
              $x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
              $x .= "</td>";
          }
          $x .= "</tr>";
          
          return $x;
     }
     
     function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
     {
         global $isSimpleReport;
         
         $footerFontClass= ($isSimpleReport)?"simple_ms_table_footer":"tabletext";
         $x = "";
         $border_top = ($hasBorderTop)? "border_top" : "";
         
         //		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
         //			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
         //				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
         //			$x .= "</table>";
         //		$x .= "</td>";
         $x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
         $x .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
         $x .= "<tr><td height='{$LineHeight}'class='$footerFontClass' align='right'>". $TitleCh .":</td></tr>";
         $x .= "</table>";
         $x .= "</td>";
         
         return $x;
     }
     
     function Is_Show_Rightest_Column($ReportID)
     {
         $ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
         $ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
         $ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
         $ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
         $ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
         $ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
         $ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
         $ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
         $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
         
         //$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
         $ShowRightestColumn = $ShowSubjectOverall;
         
         return $ShowRightestColumn;
     }
     
     function Get_Calculation_Setting($ReportID)
     {
         $ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
         $SemID 						= $ReportSetting['Semester'];
         $ReportType 				= $SemID == "F" ? "W" : "T";
         
         $CalSetting = $this->LOAD_SETTING("Calculation");
         $CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
         
         return $CalOrder;
     }
     
     function getSchoolInfo()
     {
         global $eReportCard;
         
         $SchoolInfo = "<table cellspacing=0 cellpadding=0 border=0 class='fontsize10'>";
         $SchoolInfo .= "<tr>";
         $SchoolInfo .= "<td style='padding-top:0px;padding-bottom:0px;'>".$eReportCard['Template']['SchoolInfo']['AddressEn'].": ".$eReportCard['Template']['SchoolInfo']['SchoolAddrEn']."</td>";
         $SchoolInfo .= "</tr>";
         $SchoolInfo .= "<tr>";
         $SchoolInfo .= "<td style='padding-top:0px;padding-bottom:0px;'>";
         $SchoolInfo .= $eReportCard['Template']['SchoolInfo']['AddressCh'].": ".$eReportCard['Template']['SchoolInfo']['SchoolAddrCh'];
         $SchoolInfo .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
         $SchoolInfo .= $eReportCard['Template']['SchoolInfo']['TelNoCh']." ".$eReportCard['Template']['SchoolInfo']['TelNoEn']." : ".$eReportCard['Template']['SchoolInfo']['SchoolTel'] ;
         $SchoolInfo .= "</td>";
         $SchoolInfo .= "</tr>";
         $SchoolInfo .= "<tr>";
         $SchoolInfo .= "<td style='padding-top:0px;padding-bottom:0px;'>";
         $SchoolInfo .= $eReportCard['Template']['SchoolInfo']['FaxNoCh']." ".$eReportCard['Template']['SchoolInfo']['FaxNoEn']." : ".$eReportCard['Template']['SchoolInfo']['SchoolFax'] ;
         $SchoolInfo .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
         $SchoolInfo .= $eReportCard['Template']['SchoolInfo']['EmailCh']." ".$eReportCard['Template']['SchoolInfo']['EmailEn']." : <u>".$eReportCard['Template']['SchoolInfo']['SchoolEmail']."</u>" ;
         $SchoolInfo .="</td>";
         $SchoolInfo .= "</tr>";
         $SchoolInfo .= "</table>";
         
         return $SchoolInfo;
         
     }
     
     function getSDAvgTable($ReportID, $StudentID)
     {
         global $eReportCard, $eRCTemplateSetting,$PATH_WRT_ROOT;
         
         # Retrieve Display Settings
         $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
         $LineHeight = $ReportSetting['LineHeight'];
         $ClassLevelID = $ReportSetting['ClassLevelID'];
         $ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
         $ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
         $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
         
         # define
         $ColHeaderAry = $this->genSDAvgTableColHeader($ReportID,$StudentID);
         list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
         
         # retrieve SubjectID Array
         $MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
         if (sizeof($MainSubjectArray) > 0) {
             foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
         }
         $SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
         if (sizeof($SubjectArray) > 0) {
             foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
         }
         
         # retrieve marks
         $MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
         //debug_pr($MarksAry); //(mark - average(mark))/ standard deviation
         
         $SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
         $sizeofSubjectCol = sizeof($SubjectCol);
         
         # retrieve Marks Array
         $MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
         $MarksDisplayAry = $MSTableReturned['SDAvgTable'];
         $isAllNAAry = $MSTableReturned['isAllNA'];
         /*
          # retrieve Subject Teacher's Comment
          $SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
          */
         
         ##########################################
         # Start Generate Table
         ##########################################
         $DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
         # ColHeader
         $DetailsTable .= $ColHeader;
         
         $isFirst = 1;
         for($i=0;$i<$sizeofSubjectCol;$i++)
         {
             $isSub = 0;
             $thisSubjectID = $SubjectIDArray[$i];
             
             # If all the marks is "*", then don't display
             if (sizeof($MarksAry[$thisSubjectID]) > 0)
             {
                 $Droped = 1;
                 foreach($MarksAry[$thisSubjectID] as $cid=>$da) {
                     if($da['Grade']!="*")	$Droped=0;
                 }
             }
             if($Droped)	continue;
             if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
             
             if ($eRCTemplateSetting['HideComponentSubject'] && $isSub) {
                 continue;
             }
             
             # check if displaying subject row with all marks equal "N.A."
             if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
             {
                 $DetailsTable .= "<tr>";
                 # Subject
                 $DetailsTable .= $SubjectCol[$i];
                 
                 $ColumnData = $this->returnReportTemplateColumnData($ReportID);
                 
                 # Marks
                 $DetailsTable .= $MarksDisplayAry[$thisSubjectID];
                 # Subject Teacher Comment
                 /*if ($AllowSubjectTeacherComment) {
                  $css_border_top = "border_top";
                  if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
                  $DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
                  $DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
                  } else {
                  $DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
                  $DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
                  }
                  $DetailsTable .= "</span></td>";
                  }*/
                 
                 $DetailsTable .= "</tr>";
                 $isFirst = 0;
             }
         }
         #weighted standard score
         $DetailsTable .= "<tr>";
         $DetailsTable .= "<td align='right' class='border_top'>".$eReportCard["Template"]["WeightedStandardScore"].":</td>";
         $DetailsTable .= $MarksDisplayAry[0];
         $DetailsTable .= "</tr>";
         
         # MS Table Footer
         //$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
         
         $DetailsTable .= "</table>";
         
         $instruction = $this->getInstruction();
         ##########################################
         # End Generate Table
         ##########################################
         include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
         $ObjYear = new academic_year($this->schoolYearID);
         $AcademicYear = $ObjYear->Get_Academic_Year_Name();
         
         $MSTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'> ";
         $MSTable .= "<tr>";
         $MSTable .= "<td align='center' class='small_title fontsize14'><u>".$eReportCard['Template']['SubTitle']['Reference']."</u></td>";
         $MSTable .= "</tr>";
         $MSTable .= "<tr>";
         $MSTable .= "<td align='right'>".$AcademicYear."</td>";
         $MSTable .= "</tr>";
         $MSTable .= "<tr>";
         $MSTable .= "<td align='right' height='70px'>".$this->getReportStudentInfo($ReportID, $StudentID,1)."</td>";
         $MSTable .= "</tr>";
         $MSTable .= "<tr>";
         $MSTable .= "<td>".$DetailsTable."</td>";
         $MSTable .= "</tr>";
         $MSTable .= "<tr>";
         $MSTable .= "<td>".$instruction."</td>";
         $MSTable .= "</tr>";
         
         $MSTable .= "</table> ";
         
         return $MSTable;
     }
     
     function genSDAvgTableColHeader($ReportID,$StudentID)
     {
         global $eReportCard, $eRCTemplateSetting;
         
         # Retrieve Display Settings
         $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
         $ClassLevelID = $ReportSetting['ClassLevelID'];
         $AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
         $SemID = $ReportSetting['Semester'];
         $ReportType = $SemID == "F" ? "W" : "T";
         $ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
         $ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
         $ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
         $ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
         $ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
         $ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
         $LineHeight = $ReportSetting['LineHeight'];
         
         # updated on 08 Dec 2008 by Ivan
         # if subject overall column is not shown, grand total, grand average... also cannot be shown
         //$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
         $ShowRightestColumn = $ShowSubjectOverall;
         
         $Semester = $ReportSetting["Semester"];
         $SemName = $Semester!="F"&&in_array($this->GET_FORM_NUMBER($ClassLevelID),array(6,7))?$this->returnSemesters($Semester):$eReportCard["Template"]["FirstTerm"];
         
         $n = 0;
         $e = 0;	# column# within term/assesment
         $t = array(); # number of assessments of each term (for consolidated report only)
         #########################################################
         ############## Marks START
         $row2 = "";
         /*if($ReportType=="T")	# Terms Report Type
          {
          # Retrieve Invloved Assesment
          $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
          $ColumnID = array();
          $ColumnTitle = array();
          if (sizeof($ColoumnTitle) > 0)
          foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
          
          $e = 0;
          for($i=0;$i<sizeof($ColumnTitle);$i++)
          {
          //$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
          $ColumnTitleDisplay = $ColumnTitle[$i];
          $row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
          $n++;
          $e++;
          }
          }
          else					# Whole Year Report Type
          {*/
          $ColumnData = $this->returnReportTemplateColumnData($ReportID);
          for($i=0;$i<sizeof($ColumnData);$i++)
          {
              #$css_border_top = ($isFirst or $isSub)? "" : "border_top";
              $css_border_top =  "border_top";
              
              $isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
              if($isDetails==1)		# Retrieve assesments' marks
              {
                  # See if any term reports available
                  $thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
                  
                  # if no term reports, CANNOT retrieve assessment result at all
                  if (empty($thisReport['ReportID'])) {
                      for($j=0;$j<sizeof($ColumnID);$j++) {
                          $x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
                      }
                  } else {
                      $thisReportID = $thisReport['ReportID'];
                      $ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
                      $ColumnID = array();
                      $ColumnTitle = array();
                      foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
                      
                      $thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
                  }
              }
          }
          
          $needRowspan=0;
          /*for($i=0;$i<sizeof($ColumnData);$i++)
           {
           $SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
           $row1 .= "<td height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ."</td><td {$Rowspan} height='{$LineHeight}' class='border_left'>&nbsp;</td>";
           $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['StandardDeviation']. "</td>";
           $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormAverage']. "</td>";
           }*/
          $row1 .= "<td height='{$LineHeight}' class='border_left reportcard_text' align='center' colspan='2'>". $SemName ."</td>";
          $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['StandardDeviation']. "</td>";
          $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormAverage']. "</td>";
          
          if($ReportType=="W")
          {
              $row1 .= "<td height='{$LineHeight}' class='border_left reportcard_text' align='center' colspan='2'>". $eReportCard['Template']['SecondTerm'] ."</td>";
              $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['StandardDeviation']. "</td>";
              $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormAverage']. "</td>";
              
              $row1 .= "<td height='{$LineHeight}' class='border_left reportcard_text' align='center' colspan='2'>".$eReportCard['Template']['WholeYear']."</td>";
              $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $eReportCard['Template']['StandardDeviation']. "</td>";
              $row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}' nowrap>". $eReportCard['Template']['FormAverage']. "</td>";
          }
          
          //}
          ############## Marks END
          #########################################################
          $Rowspan = $row2 ? "rowspan='2'" : "";
          
          if(!$needRowspan) {
              $row1 = str_replace("rowspan='2'", "", $row1);
          }
          
          $x = "<tr>";
          # Subject
          $SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
          for($i=0;$i<sizeof($SubjectColAry);$i++)
          {
              /*$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
               $SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
               $SubjectTitle = $SubjectColAry[$i];
               $SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
               $SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);*/
              $SubjectTitle = $eReportCard['Template']['SubjectChn'];
              $x .= "<td {$Rowspan} align='center' valign='middle' class='fontsize16' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
              $n++;
          }
          
          # Marks
          $x .= $row1;
          
          $x .= "</tr>";
          if($row2)	$x .= "<tr>". $row2 ."</tr>";
          return array($x, $n, $e, $t);
     }
     
     function getInstruction()
     {
         global $eReportCard;
         //$page_break_css = "style='page-break-after:always'";
         
         $SD_DivisionArr = array();
         $SD_DivisionArr['2.33'] 	= 99;
         $SD_DivisionArr['1.96'] 	= 97.5;
         $SD_DivisionArr['1.64'] 	= 95;
         $SD_DivisionArr['1.28'] 	= 90;
         $SD_DivisionArr['1.04'] 	= 85;
         $SD_DivisionArr['0.84'] 	= 80;
         $SD_DivisionArr['0.68'] 	= 75;
         $SD_DivisionArr['0.52'] 	= 70;
         $SD_DivisionArr['0.25'] 	= 60;
         $SD_DivisionArr['0'] 		= 50;
         $SD_DivisionArr['-0.25'] 	= 40;
         $SD_DivisionArr['-0.52'] 	= 30;
         $SD_DivisionArr['-0.68'] 	= 25;
         $SD_DivisionArr['-0.84'] 	= 20;
         $SD_DivisionArr['-1.04'] 	= 15;
         $SD_DivisionArr['-1.28'] 	= 10;
         $SD_DivisionArr['-1.64'] 	= 5;
         $SD_DivisionArr['-1.96'] 	= 2.5;
         $SD_DivisionArr['-2.33'] 	= 1;
         
         $html = "<table width='100%' border='0' cellspacing='0' cellpadding='0' $page_break_css>";
         for($i=0;$i<sizeof($eReportCard["Template"]["Instruction"]["Row"]);$i++)
         {
             $html .= "<tr>";
             $html .= "<td height='35px' valign='bottom' class='fontsize14'>".$eReportCard["Template"]["Instruction"]["Row"][$i]."</td>";
             $html .= "</tr>";
         }
         $html .= "<tr>";
         $html .= "<td valign='top'>";
         $html .= "<table width='95%' border='0' cellspacing='0' cellpadding='2' class='report_border' style='margin-left:18px' >";
         $html .= "<tr>";
         $html .= "<td align='center' class='fontsize14' width='20%'>A</td>";
         $html .= "<td align='center' class='border_left fontsize14' width='20%'>B</td>";
         $html .= "<td align='center' class='border_left fontsize14' width='20%'>C</td>";
         $html .= "<td align='center' class='border_left fontsize14' width='20%'>D</td>";
         $html .= "<td align='center' class='border_left fontsize14' width='20%'>E</td>";
         $html .= "</tr>";
         $html .= "<tr>";
         $html .= "<td align='center' class='border_top fontsize14'>".$eReportCard['Template']['Instruction']['Grade']['A']."</td>";
         $html .= "<td align='center' class='border_left border_top fontsize14'>".$eReportCard['Template']['Instruction']['Grade']['B']."</td>";
         $html .= "<td align='center' class='border_left border_top fontsize14'>".$eReportCard['Template']['Instruction']['Grade']['C']."</td>";
         $html .= "<td align='center' class='border_left border_top fontsize14'>".$eReportCard['Template']['Instruction']['Grade']['D']."</td>";
         $html .= "<td align='center' class='border_left border_top fontsize14'>".$eReportCard['Template']['Instruction']['Grade']['E']."</td>";
         $html .= "</tr>";
         $html .= "<tr>";
         $html .= "<td colspan='3' align='center' class=' border_top fontsize14'>".$eReportCard['Template']['Instruction']['Pass']."</td>";
         $html .= "<td colspan='2' align='center' class='border_left border_top fontsize14'>".$eReportCard['Template']['Instruction']['Fail']."</td>";
         $html .= "</tr>";
         $html .= "</table>";
         $html .= "</td>";
         $html .= "</tr>";
         
         $html .= "<tr>";
         $html .= "<td height='20px' valign='bottom' class='fontsize14'>&nbsp;</td>";
         $html .= "</tr>";
         
         $html .= "<tr>";
         $html .= "<td height='35px' valign='bottom' class='fontsize14'>";
         $html .= "<table border='0' cellpadding='0' cellspacing='0'>";
         $html .= "<tr>";
         $html .= "<td class='fontsize14' width='1' valign='top' nowrap>5.&nbsp;</td>";
         $html .= "<td class='fontsize14'>".$eReportCard["Template"]["Instruction"]["ExtraRow"][5]."</td>";
         $html .= "</tr>";
         $html .= "</table>";
         $html .= "</td>";
         $html .= "</tr>";
         
         $html .= "<tr>";
         $html .= "<td valign='top'>";
         $html .= "<table width='95%' border='0' cellspacing='0' cellpadding='2' class='report_border' style='margin-left:18px' >";
         $html .= "<tr>";
         $html .= "<td align='center' class='fontsize14' width='8%'>".$eReportCard['Template']['StandardDeviation']."</td>";
         
         foreach ($SD_DivisionArr as $key => $value)
         {
             $html .= "<td align='center' class='border_left fontsize12' width='5%' nowrap>".$key."</td>";
         }
         $html .= "</tr>";
         $html .= "<tr>";
         $html .= "<td align='center' class='border_top fontsize14'>".$eReportCard['Template']['Instruction']['PercentageDivisionCh']."</td>";
         
         foreach ($SD_DivisionArr as $key => $value)
         {
             $html .= "<td align='center' class='border_left border_top fontsize12' width='5%' nowrap>".$value."</td>";
         }
         $html .= "</tr>";
         $html .= "</table>";
         $html .= "</td>";
         $html .= "</tr>";
         
         $html .= "<tr>";
         $html .= "<td height='35px' valign='bottom' class='fontsize14'>".$eReportCard["Template"]["Instruction"]["ExtraRow"]["Reference"]."</td>";
         $html .= "</tr>";
          
         $html .= "</table>";
         
         return $html;
     }
     
     function Get_Signature_Image($ParUserLogin)
     {
         global $special_feature;
         
         if(!$special_feature['signature_upload']) {
             return "";
         }
             
         $sql = "
             SELECT
             SignatureLink
             FROM
             INTRANET_USER_SIGNATURE
             WHERE
             UserLogin = '$ParUserLogin'
         ";
         $Result = $this->returnVector($sql);
         
         if($Result[0]) {
             return '<img src="'.$Result[0].'" height="55px" width="120px">';
         }
     }
     
     // Simple Report
     function getSimpleReportStudentInfo($StudentID)
     {
         global $PATH_WRT_ROOT, $eReportCard;
         
         include_once($PATH_WRT_ROOT."includes/libuser.php");
         include_once($PATH_WRT_ROOT."includes/libclass.php");
         $lu = new libuser($StudentID);
         $lclass = new libclass();
         
         $data['Name'] = $lu->UserName2Lang('en', 2);
         
         $StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
         $thisClassNumber = $StudentInfoArr[0]['ClassName']."(".$StudentInfoArr[0]['ClassNumber'].")";
         
         $data['ClassNo'] = $thisClassNumber;
         
         $StudentInfoTable .= "<table width='95%' border='0' cellpadding='0' cellspacing='0' align='left'>";
         $StudentInfoTable .= '<tr>'."\n";
         $StudentInfoTable .= '<td width="100px" align="right" class="simple_student_info">'.$eReportCard['Template']['StudentInfo']['Name'].'&nbsp;:&nbsp;</td>'."\n";
         $StudentInfoTable .= '<td width="400px" class="tableline simple_student_info">'.$data['Name'] .'</td>'."\n";
         $StudentInfoTable .= '<td>&nbsp;</td>'."\n";
         $StudentInfoTable .= '<td width="120px" align="right" class="simple_student_info">'.$eReportCard['Template']['StudentInfo']['ClassNo'].'&nbsp;:</td>'."\n";
         $StudentInfoTable .= '<td width="90px" align="center" class="tableline simple_student_info">'.$data['ClassNo'].'</td>'."\n";
         $StudentInfoTable .= '</tr>'."\n";
         $StudentInfoTable .= '</table>'."\n";
         
         return $StudentInfoTable;
     }
}

?>