<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/confucius_hall.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "merit", "eca", "remark", "attendance", "others");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		
		$this->OtherInfoInGrandMS = array("Conduct Average");
		
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		// Added 15px - move the whole block down by 1 line - [Case #T72538]
		$TableTop .= "<tr><td height='195px' valign='top'>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr><td valign='bottom'>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' height='1070px' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='800px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td >".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
			$imgfile = "/file/reportcard2008/templates/confucius_hall_v2.jpg";
			$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" height=150>\n" : "";
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();
			$SchoolName = $eReportCard['Template']['SchoolInfo']['SchoolNameCh']."<br>";
			$SchoolName .= $eReportCard['Template']['SchoolInfo']['SchoolNameEn']."<br>";
				
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='115' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='130px' height='160px' align='center' class='report_border'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $thisClassNumber;
				
				if (is_array($SettingStudentInfo))
				{
					if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				else
				{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			$defaultStudentInfo = array("Name","Class","STRN");
			if(!empty($defaultStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $defaultStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$tdwidth = $SettingID=="Name" ? " width='50%' " : "";
						$alignright = $SettingID=="STRN" ? " align='right' " : "";
						$StudentInfoTable .= "<td class='tabletext' $tdwidth $alignright valign='top' height='{$LineHeight}'>".$Title." : ";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} else {
							/*if($SettingID=="Name")
							{
								$count=-1;
								$StudentInfoTable .= "</tr>";
							}*/
						}
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
						
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		$AnyAbsent = $MSTableReturned['AnyAbsent'];
		$AnyOverallAbsent = $MSTableReturned['AnyOverallAbsent'];
		$isFirstAry = $MSTableReturned['isFirst'];
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $isFirstAry);
		$sizeofSubjectCol = sizeof($SubjectCol);

		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
//			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
//			{
//				$Droped = 1;
//				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
//					if($da['Grade']!="*")	$Droped=0;
//			}
//			if($Droped)	continue;
			if($MarksAry[$thisSubjectID][0]['Grade']=="*")	continue;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $this->Get_Double_Line_Td().$MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr, $AnyAbsent,$AnyOverallAbsent);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		/*if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				$ColumnTitleDisplay = $ColumnTitle[$i];
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay =  $ColumnTitle[$j];
						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ."</td>";
			}
		}*/
				$br = $ReportType=="W"?"<br>":"&nbsp;";
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='fontsize14 reportcard_text' align='center'>". $eReportCard['Template']['ColHeader']['FullMarkCh'].$br.$eReportCard['Template']['ColHeader']['FullMarkEn'] ."</td>";
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left fontsize14 reportcard_text' align='center'>". $eReportCard['Template']['ColHeader']['ResultCh'].$br.$eReportCard['Template']['ColHeader']['ResultEn'] ."</td>";
				if($ReportType=="W")	# Terms Report Type
				{
					$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left fontsize14 reportcard_text' align='center'>". $eReportCard['Template']['ColHeader']['PositionCh'].$br.$eReportCard['Template']['ColHeader']['PositionEn'] ."</td>";
					$row1 .= $this->Get_Double_Line_Td()."<td {$Rowspan} {$colspan} height='{$LineHeight}' class='fontsize14 reportcard_text' align='center'>". $eReportCard['Template']['ColHeader']['AnnualResultCh'].$br.$eReportCard['Template']['ColHeader']['AnnualResultEn'] ."</td>";
					$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left fontsize14 reportcard_text' align='center'>". $eReportCard['Template']['ColHeader']['RankCh'].$br.$eReportCard['Template']['ColHeader']['RankEn'] ."</td>";
				}
				else
				{
					$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left fontsize14 reportcard_text' align='center'>". $eReportCard['Template']['ColHeader']['RankCh']." ".$eReportCard['Template']['ColHeader']['RankEn'] ."</td>";				
				}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];

			$SubjectColWidth = "width='". $eRCTemplateSetting['ColumnWidth'][$SubjectTitle]."'";
			//$SubjectColWidth = "width='". $eRCTemplateSetting['ColumnWidth']['SubjectEng']."'";

			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			
			$x .= "<td {$Rowspan}  valign='middle' class='fontsize15' height='{$LineHeight}' $SubjectColWidth>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $this->Get_Double_Line_Td().$row1;
		
		/*# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}*/
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID, $isFirstAry='')
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
			//$lastisSub=1;
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
	 			$SubjectCount = 0;
	 			foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;";
			 		$isSub = 1;
			 		$SubjectCount++;
			 		$css_border_bottom = '';
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
				 		$isSub = 0;
			 		}
			 		else
			 		{
			 			if ($SubjectCount==count($Ary))		// Add border bottom after the last component subject
			 				$css_border_bottom = 'border_bottom';
			 		}
			 		
			 		//$css_border_top = (!$isSub&&$lastisSub)? "border_top" : "";
			 		$css_border_top = ($isFirst || $isFirstAry[$SubSubjectID]==1)? "border_top" : "";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top} {$css_border_bottom}'  valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
				//$lastisSub = $isSub;
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='',$StudentID)
	{
 		global $eReportCard, $eRCTemplateSetting,$PATH_WRT_ROOT;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$principalName = $ReportSetting['PrincipalName'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		
		$tdwidth = "width='".(100/(sizeof($SignatureTitleArray)))."%'";
		
		//2013-0131-1018-23156  No need to show the Chinese Name if there's no Chinese Name in the system.
		//include_once($PATH_WRT_ROOT."includes/libclass.php");
		//$lclass = new libclass();
		//$ClassTeacherAry =  $lclass->returnClassTeacher($this->Get_Student_ClassName($StudentID), $this->schoolYearID);
		
		# request changed on case 2010-1117-1052-04054.
//		$ClassTeacherStr = implode("．",Get_Array_By_Key($ClassTeacherAry,"CTeacher"));
//		$ClassTeacherStr = $ClassTeacherAry[0]["CTeacherCh"]." ".$ClassTeacherAry[0]["CTeacherEn"];

		$ClassTeacherAry = $this->Get_Student_Class_Teacher_Info($StudentID);
		$ClassTeacherStr = '';
		if ($ClassTeacherAry[0]['ChineseName'] != '') {
			$ClassTeacherStr .= $ClassTeacherAry[0]['ChineseName'].$ClassTeacherAry[0]['TitleChinese'].' ';
		}
		$ClassTeacherStr .= $ClassTeacherAry[0]['TitleEnglish'].' '.$ClassTeacherAry[0]['EnglishName'];
		
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			
			switch($SettingID)
			{
//				case "Principal": $DisplayName = $eReportCard['Template']['SchoolInfo']['Principal']; break;
				case "Principal": $DisplayName = $principalName; break;
				case "ClassTeacher": $DisplayName = "(".$ClassTeacherStr.")"; break;
				default: $DisplayName = '&nbsp;';
			}	
			
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? $this->FormatDate($Issued,"D/M/Y") : "&nbsp;";
			
			$SignatureTable .= "<td valign='bottom' align='center' $tdwidth>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' width='90%'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='100px' valign='bottom'>". $IssueDate ."</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='fontsize10 signatureline' valign='middle' nowrap style='height:18px;'>".$DisplayName."</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='fontsize15 reportcard_text' valign='top'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array(),$AnyAbsent='',$AnyOverallAbsent='')
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML(number_format($result['GrandTotal'],1,'.',''), $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML(number_format($result['GrandAverage'],1,'.',''), $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		//if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
				
				#get Grand Full Total
				$thisReportID = $this->getCorespondingTermReportID($ReportID, $ColumnID);
				$GranFullTotal = $this->returnGrandTotalFullMark($thisReportID, $StudentID, $checkExemption=1);
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML(number_format($columnResult['GrandTotal'],1,'.',''), $ReportID, $ClassLevelID, '', '', 'GrandTotal').'/'.$GranFullTotal : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML(number_format($columnResult['GrandAverage'],1,'.',''), $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
				
				$displayClassNoOfStudent = $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent');
				$displayFormNoOfStudent = $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent');
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition').'/'.$displayClassNoOfStudent : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition').'/'.$displayFormNoOfStudent : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			}
		//}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
			$GranFullTotal = $this->returnGrandTotalFullMark($ReportID, $StudentID, $checkExemption=1);
			$GrandTotal = $GrandTotal."/".$GranFullTotal;
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleCh, $thisTitleEn, $columnTotal, $GrandTotal, $first);
			
			$first = 0;
		}
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark 
			if($ShowGrandAvg)
			{
				$thisTitleEn = $eReportCard['Template']['AvgMarkEn'];
				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleCh, $thisTitleEn, $columnAverage, $GrandAverage, $first, $AnyAbsent ,$AnyOverallAbsent);
				
				$first = 0;
			}
			
			# Position in Class 
			if($ShowOverallPositionClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
				$ClassPosition = $ClassPosition."/".$ClassNumOfStudent;
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleCh, $thisTitleEn, $columnClassPos, $ClassPosition, $first, $AnyAbsent ,$AnyOverallAbsent);
				
				$first = 0;
			}
			
			# Number of Students in Class 
			if($ShowNumOfStudentClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleCh, $thisTitleEn, $columnClassNumOfStudent, $ClassNumOfStudent, $first, $AnyAbsent ,$AnyOverallAbsent);
				
				$first = 0;
			}
			
			# Position in Form 
			if($ShowOverallPositionForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
				$FormPosition = $FormPosition."/".$FormNumOfStudent;
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleCh, $thisTitleEn, $columnFormPos, $FormPosition, $first, $AnyAbsent ,$AnyOverallAbsent);
				
				$first = 0;
			}
			
			# Number of Students in Form 
			if($ShowNumOfStudentForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleCh, $thisTitleEn, $columnFormNumOfStudent, $FormNumOfStudent, $first, $AnyAbsent ,$AnyOverallAbsent);
				
				$first = 0;
			}
			
			##################################################################################
			# CSV related
			##################################################################################
			# build data array
			$ary = array();
			$csvType = $this->getOtherInfoType();
			
			//modified by Marcus 20/8/2009
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
			$ary=$OtherInfoDataAry[$StudentID];
			
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				
				/*
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					$InfoTermID = $TermID;
					
					if(!empty($csvType))
					{
						foreach($csvType as $k=>$Type)
						{
							$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
							if(!empty($csvData)) 
							{
								foreach($csvData as $RegNo=>$data)
								{
									if($RegNo == $WebSamsRegNo)
									{
										foreach($data as $key=>$val)
											$ary[$TermID][$key] = $val;
									}
								}
							}
						}
					}
				}
				*/
				
				# calculate sems/assesment col#
				$ColNum2Ary = array();
				foreach($ColumnData as $k1=>$d1)
				{
					$TermID = $d1['SemesterNum'];
					/*if($d1['IsDetails']==1)
					{
						# check sems/assesment col#
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
					}
					else*/
						$ColNum2Ary[$TermID] = 0;
				}
			}
			/*
			else
			{
				$InfoTermID = $SemID;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);
						
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$SemID][$key] = $val;
								}
							}
						}
					}
				}
			}
			*/

			$border_top = $first ? "border_top" : "";
			
			# Days Absent 
			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleCh, $thisTitleEn, "Days Absent", $ary);
			
			# Times Late 
			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleCh, $thisTitleEn, "Time Late", $ary);
			
			# Early Leave 
			$thisTitleEn = $eReportCard['Template']['EarlyLeaveEn'];
			$thisTitleCh = $eReportCard['Template']['EarlyLeaveCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleCh, $thisTitleEn, "Early Leave", $ary);
			
			# Conduct 
			/*$thisTitleEn = $eReportCard['Template']['ConductEn'];
			$thisTitleCh = $eReportCard['Template']['ConductCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);*/
		}
				
		$x .= "";
			
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $PATH_WRT_ROOT;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		## Prepare otherinfo array to Add * to absent subject marks (imported by csv)
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		
		# retrieve result data
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		if(!empty($ary["SubjectCode"]))
		{
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$lsubj = new subject();
			$AbsentSubjectIDAry = array();
			
			$SubjectCodeAry =  $ary["SubjectCode"];
			$SubjectComponentCodeAry =  $ary["SubjectComponentCode"];
			if(!is_array($SubjectCodeAry))
			{	
				$SubjectCodeAry = array($SubjectCodeAry);
				$SubjectComponentCodeAry = array($SubjectComponentCodeAry);
			}
			
			for($i=0; $i<sizeof($SubjectCodeAry);$i++)
			{
				if(trim($SubjectComponentCodeAry[$i])!='')
				{
					$isComponent = 1;
					$parentcode = $SubjectCodeAry[$i];
					$code = $SubjectComponentCodeAry[$i];
				}
				else
				{
					$isComponent = 0;
					$code = $SubjectCodeAry[$i];
					$parentcode = '';
				}
				$SubjectInfo = $lsubj->Get_Subject_By_SubjectCode($code,$isComponent,$parentcode);
				$AbsentSubjectIDAry[] = $SubjectInfo[0]['RecordID'];
			}
		}
			
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
					
			$lastisSub =1;	
			$LastCmpSubjectIDArr = array();			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				$thisAbsent=false;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) 
					{
						$isParentSubject = 1;
						$IndexOfLastCmp = count($CmpSubjectArr) - 1;
						$LastCmpSubjectIDArr[] = $CmpSubjectArr[$IndexOfLastCmp]['SubjectID'];
					}
				}
				
				# define css
				//$css_border_top = (!$isSub&&$lastisSub)? "border_top" : "";
				$css_border_top = ($isFirst)? "border_top" : "";
				$css_border_bottom = (in_array($SubjectID, $LastCmpSubjectIDArr))? "border_bottom" : "";
				
				
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
								
				# ShowFullMark
				if($ShowSubjectFullMark)
				{
  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
					$x[$SubjectID] .= "<td class=' tabletext $css_border_top $css_border_bottom' align='center' >". $thisFullMark ."</td>";
				}
				
				# Assessment Marks & Display
				/*for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
						$thisMarkDisplay = $this->EmptySymbol;
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}*/
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? number_format($thisMark,1,'.','') : $thisMark) : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if($thisMark=='-' || $thisMark=='+' ) 
					{
						$thisAbsent = true;
						$returnArr['AnyAbsent'] = true;
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
							
						if(in_array($SubjectID,(array)$AbsentSubjectIDAry))
							$thisMarkDisplay = $thisMarkDisplay."*";
							
						if($ScaleDisplay=="G")
							$thisMarkDisplay = "<div align='left' style='width:17px; white-space: nowrap;'>$thisMarkDisplay</div>";	
					}
					
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_border_bottom}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' >". $thisMarkDisplay ."</td>";
  					/*if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}*/
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top} {$css_border_bottom}'>".$this->EmptySymbol."</td>";
				}
				//debug_pr($MarksAry[$SubjectID][0]);
				
				$thisFormNoOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
				$thisOrderMeritForm = $MarksAry[$SubjectID][0]['OrderMeritForm'];
				$thisRank = ($thisFormNoOfStudent>0&&$thisOrderMeritForm>0&&!$thisAbsent)?$MarksAry[$SubjectID][0]['OrderMeritForm']."/".$MarksAry[$SubjectID][0]['FormNoOfStudent']:$this->EmptySymbol;
				$thisRank = $ScaleDisplay=="G" && $ScaleInput == "G"? $this->EmptySymbol:$thisRank;
				$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top} {$css_border_bottom}'>$thisRank</td>";
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$returnArr['isFirst'][$SubjectID] = $isFirst;
				
				if (!$isAllNA) {
					$isFirst = 0;
				}
				
				$lastisSub = $isSub;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$lastisSub=1;
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr))
					{
						$isParentSubject = 1;
						$IndexOfLastCmp = count($CmpSubjectArr) - 1;
						$LastCmpSubjectIDArr[] = $CmpSubjectArr[$IndexOfLastCmp]['SubjectID'];
					}
				}
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				$isFirstTerm = 1;
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					if($isFirstTerm)
					{
						$isFirstTerm=0;
						continue;
					}						
					
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					//$css_border_top = !$isSub&&$lastisSub? "border_top" : "";
					$css_border_top = ($isFirst)? "border_top" : "";
					$css_border_bottom = (in_array($SubjectID, $LastCmpSubjectIDArr))? "border_bottom" : "";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					
/*					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$i];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = $this->EmptySymbol;
								}
								else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
									$thisMarkDisplay = $this->EmptySymbol;
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
									$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
									
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A.")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
									if($needStyle)
									{
										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										}
										else
										{
											$thisMarkTemp = $thisMark;
										}
										
										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
								}
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{*/
						$thisMarkDisplay = '';
						$thisMark = '';
//						if ($isParentSubject && $CalculationOrder == 1) {
//							$thisMarkDisplay = $this->EmptySymbol;
//						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								// $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? number_format($thisMSMark,1,'.','') : "";
                                $thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? ($StudentID ? number_format($thisMSMark,1,'.','') : $thisMSMark) : "";
								
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
                            // $thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? number_format($thisMark,1,'.','') : $thisGrade;
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? ($StudentID ? number_format($thisMark,1,'.','') : $thisMark) : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							if($thisMark=='-'||$thisMark=='+')
							{
								$thisMarkDisplay = $this->EmptySymbol;
								$returnArr['AnyAbsent'] = true;
							}
							else
							{
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
								if($needStyle)
								{
									$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
								}
								else
								{
									$thisMarkDisplay = $thisMark;
								}
								
								if(in_array($SubjectID, (array)$AbsentSubjectIDAry))
									$thisMarkDisplay = $thisMarkDisplay."*";
									
								if($ScaleDisplay=="G")
									$thisMarkDisplay = "<div align='left' style='width:17px; white-space: nowrap;'>$thisMarkDisplay</div>";
							}
//						}
											
						# Full mark
						if($ShowSubjectFullMark=true)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext {$css_border_top} {$css_border_bottom}' align=center>". $FullMark ."</td>";
						}
						
						# Result
						$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_border_bottom}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align=center>". $thisMarkDisplay ."</td>";
	  					$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
						
						# Position
						
						$thisFormNoOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
						$thisOrderMeritForm = $MarksAry[$SubjectID][0]['OrderMeritForm'];
						$thisRank = $thisFormNoOfStudent>0&&$thisOrderMeritForm>0?$MarksAry[$SubjectID][0]['OrderMeritForm']."/".$MarksAry[$SubjectID][0]['FormNoOfStudent']:$this->EmptySymbol;
						$thisRank = $ScaleDisplay=="G" && $ScaleInput == "G"? $this->EmptySymbol:$thisRank;
						
						$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_border_bottom}' align=center>$thisRank</td>";
					}
				//}
				
				# Subject Overall
				if($ShowSubjectOverall)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID,"","",1);		
					
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? number_format($thisMark,1,'.','') : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					if ($thisMark == '-' || $thisMark=='+')
					{
						$thisMarkDisplay = $this->EmptySymbol;
						$returnArr['AnyOverallAbsent'] = true;
					}
					else if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					
					if($ScaleDisplay=="G")
						$thisMarkDisplay = "<div align='left' style='width:17px; white-space: nowrap;'>$thisMarkDisplay</div>";
					
					$x[$SubjectID] .= $this->Get_Double_Line_Td();	
					$x[$SubjectID] .= "<td class='{$css_border_top} {$css_border_bottom}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center'>". $thisMarkDisplay ."</td>";
  					  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					# Position
					$thisFormNoOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
					$thisOrderMeritForm = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$thisRank = $thisFormNoOfStudent>0&&$thisOrderMeritForm>0?$MarksAry[$SubjectID][0]['OrderMeritForm']."/".$MarksAry[$SubjectID][0]['FormNoOfStudent']:$this->EmptySymbol;
					$thisRank = $ScaleDisplay=="G" && $ScaleInput == "G"? $this->EmptySymbol:$thisRank;
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} {$css_border_bottom}' align='center'>$thisRank</td>";
					
					
					
					
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top} {$css_border_bottom}'>".$this->EmptySymbol."</td>";
				}
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$returnArr['isFirst'][$SubjectID] = $isFirst;
				
				if (!$isAllNA) {
					$isFirst = 0;
				}
				
				$lastisSub = $isSub;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		//modified by marcus 20/8/2009
		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
		
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		//$latestTerm++;
		
		/*
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		*/
		
		# retrieve result data
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : '&nbsp;';
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : '&nbsp;';
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : '&nbsp;';
		$Demerits = ($ary['Demerits'])? $ary['Demerits'] : '&nbsp;';
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : '&nbsp;';
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : '&nbsp;';
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		$ECA = ($ary['ECA'])? $ary['ECA'] : $this->EmptySymbol;
		if (!is_array($ECA))
			$ECA = array($ECA);
		
		
		/*$merit = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Merits'] . ": " . ($ary['Merits'] ? $ary['Merits'] :0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'] . ": " . ($ary['Demerits'] ? $ary['Demerits'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'] . ": " . ($ary['Minor Credit'] ? $ary['Minor Credit'] :0) ."</td>";
		$merit .= "</tr>";
		$merit .= "<tr>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'] . ": " . ($ary['Major Credit'] ? $ary['Major Credit'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'] . ": " . ($ary['Minor Fault'] ? $ary['Minor Fault'] : 0) ."</td>";
		$merit .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'] . ": " . ($ary['Major Fault'] ? $ary['Major Fault'] : 0) ."</td>";
		$merit .= "</tr>";
		$merit .= "</table>";
		$remark = $ary['Remark'];
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$eca = $ary['ECA'];
		if (is_array($eca))
			$ecaList = implode("<br>", $eca);
		else
			$ecaList = $eca;
		
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		$x .= "<td width='50%' valign='top'>";
			# Merits & Demerits 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['MeritsDemerits']."</b><br>".$merit."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "<td width='50%' class=\"border_left\" valign='top'>";
			# Remark 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['Remark']."</b><br>".$remark."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='60' class='report_border'>";
		$x .= "<tr>";
		if($AllowClassTeacherComment)
		{
			$x .= "<td width='50%' valign='top'>";
				# Class Teacher Comment 
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$x .= "<tr>";
					$x .= "<td class='tabletext'><b>". $eReportCard['Template']['ClassTeacherComment']."</b><br>".stripslashes(nl2br($classteachercomment))."</td>";
				$x .= "</tr>";
				$x .= "</table>";	
				$x .= "</td></tr></table>";	
			$x .="</td>";
			
			$x .= "<td width='50%' class='border_left' valign='top'>";
		} else {
			$x .= "<td width='50%' valign='top'>";
		}
			# ECA 
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;&nbsp;</td><td width='100%'>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td class='tabletext'><b>". $eReportCard['Template']['eca']."</b><br>".$ecaList."</td>";
			$x .= "</tr>";
			$x .= "</table>";	
			$x .= "</td></tr></table>";	
		$x .="</td>";
		$x .= "</tr>";
		$x .= "</table>";*/
		
		$ConductItemArray = array("Politeness","Responsibility","Study Attitude","Discipline");
		
		# Conduct Merit Table
		$ConduutMeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='100%'>";
			
			# Conduct Table
			for($i=0; $i<sizeof($ConductItemArray);$i++)
			{
				$thisDisplay = $ary[$ConductItemArray[$i]];
				$thisDisplay = ($thisDisplay=='')? '&nbsp;' : $thisDisplay;
				
				$ConduutMeritTable .= "<tr>";
//					$ConduutMeritTable .= "<td width='10px'>&nbsp;</td>";
//					$ConduutMeritTable .= "<td><div class='justify tabletext'>".$eReportCard['Template'][$ConductItemArray[$i]."Ch"]."</div></td>";
//					$ConduutMeritTable .= "<td class='tabletext'>".$eReportCard['Template'][$ConductItemArray[$i]."En"]."</td>";
//					$ConduutMeritTable .= "<td class='border_left tabletext' align='center'><div align='left' style='width:17px; white-space: nowrap;float:center;'>".$thisDisplay."</div></td>";
					
					$ConduutMeritTable .= "<td class='tabletext' style='padding-left:5px;' width='50%'>";
						$ConduutMeritTable .= $eReportCard['Template'][$ConductItemArray[$i]."Ch"];
						$ConduutMeritTable .= " ";
						$ConduutMeritTable .= $eReportCard['Template'][$ConductItemArray[$i]."En"];
					$ConduutMeritTable .= "</td>";
					
					$ConduutMeritTable .= "<td class='border_left tabletext' align='center' width='50%'>";
						$ConduutMeritTable .= "<div style='white-space: nowrap; float:center;'>";
							$ConduutMeritTable .= $thisDisplay;
						$ConduutMeritTable .= "</div>";
					$ConduutMeritTable .= "</td>";
					
				$ConduutMeritTable .= "</tr>";
			}
			
			# Conduct Average
			$thisDisplay = $ary['Conduct Average'];
			$thisDisplay = ($thisDisplay=='')? '&nbsp;' : $thisDisplay;
			
			$ConduutMeritTable .= "<tr>";
//				$ConduutMeritTable .= "<td class='border_top tabletext' width='10px'>&nbsp;</td>";
//				$ConduutMeritTable .= "<td class='border_top tabletext'>".$eReportCard['Template']['Conduct AverageCh']."</td>";
//				$ConduutMeritTable .= "<td class='border_top tabletext'>".$eReportCard['Template']['Conduct AverageEn']."</td>";
//				$ConduutMeritTable .= "<td class='border_left border_top tabletext' align='center' width='40%'><div align='left' style='width:17px; white-space: nowrap;float:center;'>".$thisDisplay."</div></td>";
				
				$ConduutMeritTable .= "<td class='border_top tabletext' style='padding-left:5px;' width='50%'>";
					$ConduutMeritTable .= $eReportCard['Template']['Conduct AverageCh'];
					$ConduutMeritTable .= " ";
					$ConduutMeritTable .= $eReportCard['Template']['Conduct AverageEn'];
				$ConduutMeritTable .= "</td>";
				
				$ConduutMeritTable .= "<td class='border_top border_left tabletext' align='center' width='50%'>";
					$ConduutMeritTable .= "<div style='white-space: nowrap; float:center;'>";
						$ConduutMeritTable .= $thisDisplay;
					$ConduutMeritTable .= "</div>";
				$ConduutMeritTable .= "</td>";
			
			$ConduutMeritTable .= "</tr>";
			
//			# build merit table
//			$MeritTable = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
//				$MeritTable .= "<tr>";
//					$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'].": $MajorCredit</td>";
//					$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'].": $MajorFault</td>";
//					//$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MajorCredit'].":</td><td>$MajorCredit</td>";
//					//$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MajorFault'].":</td><td>$MajorFault</td>";
//				$MeritTable .= "</tr>";
//				$MeritTable .= "<tr>";
//					$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'].": $MinorCredit</td>";
//					$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'].": $MinorFault</td>";
//					//$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MinorCredit'].":</td><td>$MinorCredit</td>";
//					//$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['MinorFault'].":</td><td>$MinorFault</td>";
//				$MeritTable .= "</tr>";
//				$MeritTable .= "<tr>";
//					$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['Merits'].": $Merits</td>";
//					$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'].": $Demerits</td>";
//					//$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['Merits'].":</td><td>$Merits</td>";
//					//$MeritTable .= "<td class='tabletext'>".$eReportCard['Template']['Demerits'].":</td><td>$Demerits</td>";
//				$MeritTable .= "</tr>";
//			$MeritTable .= "</table>";
			
			$LineHeightStyle = "line-height:16px;";
			$TablePadding = "padding-top:4px; padding-bottom:1px; padding-left:5px;";
			
			# Merit Table
			$MeritTable = "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			
				$MeritTable .= "<tr>";
					$MeritTable .= "<td class='border_top tabletext' height='30px' width='32%' style='$TablePadding'>";
						$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='bottom'>".$eReportCard['Template']['MajorCredit']."</td>";
							$MeritTable .= "</tr>";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='top'>".$eReportCard['Template']['MajorCreditEn']."</td>";
							$MeritTable .= "</tr>";
						$MeritTable .= "</table>";
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_left border_top tabletext' width='18%' align='center'>";
						$MeritTable .= $MajorCredit;
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_left border_top tabletext' height='30px' width='32%' style='$TablePadding'>";
						$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='bottom'>".$eReportCard['Template']['MajorFault']."</td>";
							$MeritTable .= "</tr>";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle'>".$eReportCard['Template']['MajorFaultEn']."</td>";
							$MeritTable .= "</tr>";
						$MeritTable .= "</table>";
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_left border_top tabletext' width='18%' align='center'>";
						$MeritTable .= $MajorFault;
					$MeritTable .= "</td>";
				$MeritTable .= "</tr>";
				
				$MeritTable .= "<tr>";
					$MeritTable .= "<td class='border_top tabletext' width='32%' height='30px' style='$TablePadding'>";
						$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='bottom'>".$eReportCard['Template']['MinorCredit']."</td>";
							$MeritTable .= "</tr>";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle'>".$eReportCard['Template']['MinorCreditEn']."</td>";
							$MeritTable .= "</tr>";
						$MeritTable .= "</table>";
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_top border_left tabletext' width='18%' align='center'>";
						$MeritTable .= $MinorCredit;
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_top border_left tabletext' height='30px' width='32%' style='$TablePadding'>";
						$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='bottom'>".$eReportCard['Template']['MinorFault']."</td>";
							$MeritTable .= "</tr>";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle'>".$eReportCard['Template']['MinorFaultEn']."</td>";
							$MeritTable .= "</tr>";
						$MeritTable .= "</table>";
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_top border_left tabletext' width='18%' align='center'>";
						$MeritTable .= $MinorFault;
					$MeritTable .= "</td>";
				$MeritTable .= "</tr>";
				
				$MeritTable .= "<tr>";
//					$MeritTable .= "<td class='border_top tabletext' width='32%' style='padding-top:2px; padding-left:5px;'>";
					$MeritTable .= "<td class='border_top tabletext' width='32%' height='30px' style='$TablePadding'>";
						$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='bottom'>".$eReportCard['Template']['Merits']."</td>";
							$MeritTable .= "</tr>";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle'>".$eReportCard['Template']['MeritsEn']."</td>";
							$MeritTable .= "</tr>";
						$MeritTable .= "</table>";
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_top border_left tabletext' width='18%' align='center'>";
						$MeritTable .= $Merits;
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_top border_left tabletext' width='32%' height='30px' style='$TablePadding'>";
						$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle' valign='bottom'>".$eReportCard['Template']['Demerits']."</td>";
							$MeritTable .= "</tr>";
							$MeritTable .= "<tr>";
								$MeritTable .= "<td class='tabletext' style='$LineHeightStyle'>".$eReportCard['Template']['DemeritsEn']."</td>";
							$MeritTable .= "</tr>";
						$MeritTable .= "</table>";
					$MeritTable .= "</td>";
					$MeritTable .= "<td class='border_top border_left tabletext' width='18%' align='center'>";
						$MeritTable .= $Demerits;
					$MeritTable .= "</td>";
				$MeritTable .= "</tr>";
				
			$MeritTable .= "</table>";
			
			# Merit Table Header
			$ConduutMeritTable .= "<tr>";
//				$ConduutMeritTable .= "<td class='border_top tabletext' >&nbsp;</td>";
//				$ConduutMeritTable .= "<td class='border_top tabletext' >".$eReportCard['Template']['MeritDemeritCh']."</td>";
//				$ConduutMeritTable .= "<td class='border_top tabletext' >".$eReportCard['Template']['MeritDemeritEn']."</td>";
//				$ConduutMeritTable .= "<td class='border_top tabletext' >".$eReportCard['Template']['MeritDemeritTime']."</td>";
//				$ConduutMeritTable .= "<td class='border_left border_top tabletext' rowspan='3' align='center' width='40%'>".$MeritTable."</td>";

				$ConduutMeritTable .= "<td class='border_top tabletext' align='center' colspan='4' width='100%'>";
					$ConduutMeritTable .= $eReportCard['Template']['MeritDemeritCh'];
					$ConduutMeritTable .= " ";
					$ConduutMeritTable .= $eReportCard['Template']['MeritDemeritEn'];
					$ConduutMeritTable .= " ";
					$ConduutMeritTable .= $eReportCard['Template']['MeritDemeritTime'];
				$ConduutMeritTable .= "</td>";
			$ConduutMeritTable .= "</tr>";
			
			$ConduutMeritTable .= "<tr>";
				$ConduutMeritTable .= "<td class='tabletext' colspan='4'>".$MeritTable."</td>";
//				$ConduutMeritTable .= "<td class='border_left border_top tabletext' rowspan='3' align='center'>".$MeritTable."</td>";
//				$ConduutMeritTable .= "<td>&nbsp;</td>";
//				$ConduutMeritTable .= "<td colspan='2' class='tabletext'>".$eReportCard['Template']['MeritDemeritTime']."</td>";
			$ConduutMeritTable .= "</tr>";
			
//			$ConduutMeritTable .= "<tr>";
//				$ConduutMeritTable .= "<td>&nbsp;</td>";
//				$ConduutMeritTable .= "<td colspan='2'>&nbsp;</td>";
//			$ConduutMeritTable .= "</tr>";

		$ConduutMeritTable .= "</table>";
		
		$EcaArr = (is_array($ary['ECA']))? $ary['ECA'] : array($ary['ECA']);
		
		$ecaTable = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='' valign='bottom' >";
//			for($i=0;$i<sizeof($EcaArr)&& $i<16;$i++)
			for($i=0; $i<sizeof($EcaArr) && $i<12; $i++)
			{
//				if($i%2==0){
//					$ecaTable .= "<tr>";
//						$ecaTable .= "<td width='20'>&nbsp;</td>";
//				}
//				
//				$ecaTable .= "<td width='48%' class='tabletext ' style='padding-top:1px'>".$EcaArr[$i]."</td>";
//					
//				if($i%2==1){			
//					$ecaTable .= "</tr>";
//				}
				$ecaTable .= "<tr>";
					$ecaTable .= "<td class='tabletext' style='padding-left:5px'>".$EcaArr[$i]."</td>";
				$ecaTable .= "</tr>";
			}
//			if($i%2==1)	$ecaTable .="<td>&nbsp;</td></tr>";
		$ecaTable .= "</table>";
		
		$cellpaddingOfMSTable = 8;
		$x = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
			$x .= "<tr>";
				$x .= "<td width='".($eRCTemplateSetting['ColumnWidth']['SubjectEng']+$eRCTemplateSetting['ColumnWidth']['SubjectChn']+$cellpaddingOfMSTable)."' align='center' class='padding_top tabletext'>".$eReportCard['Template']['PersonalConduct']."</td>";
				$x .= "<td align='center' class='border_left padding_top tabletext'>".$eReportCard['Template']['eca']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td align='center' class='border_top' width='45%'>$ConduutMeritTable</td>";
				$x .= "<td align='center' class='border_left tinymargin border_top' valign='top' width='55%'>$ecaTable</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		# teacher comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border tinymargin padding_top'>";
			$x .= "<tr valign='top'>";
//				$x .= "<td width='10px' height='40px' >&nbsp;</td><td class='tabletext'>".$eReportCard['Template']['ClassTeacherComment'].": ".nl2br($SubjectTeacherCommentAry[0])."</td>";
				$x .= "<td class='tabletext' width='14%' height='40px' style='padding-left:5px;'>".$eReportCard['Template']['ClassTeacherComment'].": </td>";
				$x .= "<td class='tabletext' width='86%'>".nl2br($SubjectTeacherCommentAry[0])."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		# remarks
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border tinymargin padding_top'>";
			$x .= "<tr>";
//				$x .= "<td width='10px'>&nbsp;</td><td class='tabletext'>".$eReportCard['Template']['Remark'].": ".$ary['Remark']."</td>";
				$x .= "<td class='tabletext' width='100%' style='padding-left:5px;'>".$eReportCard['Template']['Remark'].": ".$ary['Remark']."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		
		return $x;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$x = "";
		$border_top = "";
		$first = 0;
		
		# Show top border for Days Absent
		if($InfoKey == 'Days Absent'){
			$first = 1;
			$border_top = "border_top";
		}
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $first);
			$x .= $this->Get_Double_line_Td();
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			$isFirstTerm =1;
			foreach($ColumnData as $k1=>$d1)
			{
				
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				if($isFirstTerm)
				{
					$isFirstTerm = 0;
					continue;
				}
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= $this->Get_Double_Line_Td();	
				$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
			{
				$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."</td>";
				
			}
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0,$AnyAbsent='',$AnyOverallAbsent='')
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
			$x .= $this->Get_Double_Line_Td();
		/*if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		} else {*/
		if($ReportType=="W"){
			$curColumn = 0;
			$isFirstTerm = 1;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				if($isFirstTerm)
				{
					$isFirstTerm=0;
					continue;
				}
				
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
				if($AnyAbsent)
					$thisValue = $this->EmptySymbol;
				else
					$thisValue = $ValueArr[$ColumnID];
					
				$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}'>".$this->EmptySymbol."&nbsp;</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisValue."&nbsp;</td>";
				$curColumn++;
			}
		}
		
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			
			/*
			if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
			{
				$thisDisplay = $thisValue;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			*/
			if($ReportType=="W"){
				if($AnyOverallAbsent)
					$thisValue=$this->EmptySymbol;
				
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."&nbsp;</td>";
				$x .= $this->Get_Double_Line_Td();
				$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."&nbsp;</td>";
			}
			else
			{	
				if($AnyAbsent)
					$thisValue=$this->EmptySymbol;
				
				$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."&nbsp;</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $this->EmptySymbol ."&nbsp;</td>";
			}
		}
			
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Double_Line_Td($rowspan='', $has_border_top=0, $has_border_bottom=0)
	{
		global $PATH_WRT_ROOT;
		
		$rowspan_tag = '';
		if ($rowspan != '')
			$rowspan_tag = 'rowspan="'.$rowspan.'"';
			
		$border_top_css = '';
		if ($has_border_top)
			$border_top_css = 'border_top';
			
		$border_bottom_css = '';
		if ($has_border_bottom)
			$border_bottom_css = 'border_bottom';
			
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		$x = '';
		$x .= '<td '.$rowspan_tag.' width="1" class="double_border_td '.$border_top_css.' '.$border_bottom_css.'"><img src="'.$emptyImagePath.'" width="1"></td>'."\n";
		
		return $x;
	}
}


?>