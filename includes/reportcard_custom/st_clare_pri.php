<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/st_clare_pri.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "remark", "eca");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		
		$this->EmptySymbol = "---";
		
		$this->MarkingSchemeHeight = "74";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		//$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='950px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			# get school Info
			//$SchoolLogo = GET_SCHOOL_BADGE();
			$SchoolLogo = $image_path."/file/reportcard2008/templates/st_clare_pri_logo.jpg";
			$SchoolName = $eReportCard['Template']['SchoolNameEn']."<br />".$eReportCard['Template']['SchoolNameCh'];
			$SchoolAddress = $eReportCard['Template']['AddressEn']."<br />".$eReportCard['Template']['AddressCh'];
			$SchoolTelephone = $eReportCard['Template']['Telephone'];
			$SchoolFax = $eReportCard['Template']['Fax'];
			$SchoolEmail = $eReportCard['Template']['Email'];
			$SchoolWebsite = $eReportCard['Template']['Website'];
			
			if ($ReportType == "T")
				$ReportTitleBig = $eReportCard['Template']['TermReportTitleEn']."<br />".$eReportCard['Template']['TermReportTitleCh'];
			else
				$ReportTitleBig = $eReportCard['Template']['ConsolidatedReportTitleEn']."<br />".$eReportCard['Template']['ConsolidatedReportTitleCh'];
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
			
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'><img height='100' src='$TempLogo'></td>";
			$TitleTable .= "<td>";
			if ($HeaderHeight == -1) {
				$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='school_title' align='center' colspan='3'>".$SchoolName."</td></tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='title_small' align='center' colspan='3'>".$SchoolAddress."</td></tr>\n";
					$TitleTable .= "<tr>";
					$TitleTable .= "<td nowrap='nowrap' class='title_small' align='right'>".$SchoolTelephone."</td>";
					$TitleTable .= "<td nowrap='nowrap' class='title_small' align='center' width='1%'>"."&nbsp;&nbsp;&nbsp;&nbsp;"."</td>";
					$TitleTable .= "<td nowrap='nowrap' class='title_small' align='left'>".$SchoolFax."</td>";
					$TitleTable .= "</tr>\n";
					$TitleTable .= "<tr>";
					$TitleTable .= "<td nowrap='nowrap' class='title_small' align='right'>".$SchoolEmail."</td>";
					$TitleTable .= "<td nowrap='nowrap' class='title_small' align='center' width='1%'>"."&nbsp;&nbsp;&nbsp;&nbsp;"."</td>";
					$TitleTable .= "<td nowrap='nowrap' class='title_small' align='left'>".$SchoolWebsite."</td>";
					$TitleTable .= "</tr>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_sub_title' align='center' colspan='3'>".$ReportTitleBig."</td></tr>\n";
					
					if(!empty($ReportTitle))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_sub_title' align='center' colspan='3'>".$ReportTitle."</td></tr>\n";
					
				$TitleTable .= "</table>\n";
			} else {
				for ($i = 0; $i < $HeaderHeight; $i++) {
					$TitleTable .= "<br/>";
				}
			}
			$TitleTable .= "</td>";
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
			
			/*
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
			*/
			
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				$data['ClassNo'] = $lu->ClassNumber;
				$data['Class'] = $lu->ClassName;
				$data['Class'] .= " (".$lu->ClassNumber.")";
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['StudentNo'] = str_replace("#", "", $lu->WebSamsRegNo);
				
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				$data['DateOfIssue'] = $ReportSetting['Issued'];
			}
			
			# hardcode the 1st six items (Name, ClassName (ClassNumber), WebSAMSNo, DateOfIssue, Date of Birth, Gender)
			$defaultInfoArray = array("Name", "Class", "StudentNo", "DateOfIssue", "DateOfBirth", "Gender");
			
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
					}
					
					$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>";
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tabletext'>
								              <tr>
								              	<td width='2'>&nbsp;</td>
								                <td nowrap width='130'>".$Title."</td>
								                <td width='2'>";
					if ($Title != "") $StudentInfoTable .= ":&nbsp;";
					$StudentInfoTable .= "</td><td width='200' nowrap>";
					$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
					$StudentInfoTable .= "</td></tr></table>\n";
					
					
					$StudentInfoTable .= "</td>";
										
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					} 
					else
					{
						$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>&nbsp;</td>";
					}
					$count++;
				}
			}
			
			# Display student info selected by the user other than the 1st six items
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='tabletext' width='100%' valign='top' height='{$LineHeight}'>";
						
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tabletext'>
									              <tr>
									              	<td width='2'>&nbsp;</td>
									                <td nowrap width='130'>".$Title."</td>
									                <td width='2'>";
						if ($Title != "") $StudentInfoTable .= ":&nbsp;";
						$StudentInfoTable .= "</td><td width='200' nowrap>";
						$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal ;
						$StudentInfoTable .= "</td></tr></table>\n";
												
						$StudentInfoTable .= "</td>";
											
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						}
						else
						{
							$StudentInfoTable .= "<td class='tabletext' width='33%' valign='top' height='{$LineHeight}'>&nbsp;</td>";
						}
						$count++;
					}
				}				
			}
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
				
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
						
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			# P5,6 are set to display subjects ONLY
			# Client requests to let P1 to P4 reports to display component subjects as well.
			if ($isSub && ($FormNumber > 4)) continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		
		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		$ColumnWeightMap = array();
		for($i=0; $i<sizeof($ColumnData); $i++) {
			$ColumnWeightMap[$ColumnData[$i]["ReportColumnID"]] = $ColumnData[$i]["DefaultWeight"];
		}
		
		
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$e = 0;
			
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				$thisWeight = $ColumnWeightMap[$ColumnID[$i]] * 100;
				$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$i], 1, 2))." ".$thisWeight."%";
				$row1 .= "<td valign='middle' class='border_left tabletext' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
			
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				$ColumnWeight = ($ColumnWeightMap[$ColumnData[$i]["ReportColumnID"]] * 100);
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					
					$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
					$thisColumnWeightMap = array();
					for($j=0; $j<sizeof($thisColumnData); $j++) {
						$thisColumnWeightMap[$thisColumnData[$j]["ReportColumnID"]] = $thisColumnData[$j]["DefaultWeight"];
					}
					
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$ColumnTitleDisplay = convert2unicode($ColumnTitle[$j], 1, 2);
						//$thisColumnWeight = ($thisColumnWeightMap[$ColumnID[$j]] * 100)."%";
						if ($FormNumber > 4)
						{
							$row1 .= "<td class='border_left reportcard_text' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'><b>". $ColumnTitleDisplay ."</b></td>";
						}
						else
						{
							$row2 .= "<td class='border_top border_left reportcard_text' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitleDisplay ."</td>";
						}
						
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				
				if ($FormNumber < 5)
				{
					//$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ." ".$ColumnWeight."%</td>";
					$row1 .= "<td {$Rowspan} {$colspan} class='border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName. "</td>";
				}
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		$css_border_top = "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td class='tabletext'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='')
	{
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' valign='bottom'>\n";
		$SignatureTable .= "<tr>\n";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>\n";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>\n";
			$SignatureTable .= "<tr><td align='center' class='signature_title' valign='bottom'>_____". $IssueDate ."_____</td></tr>\n";
			
			if ($SettingID == "Principal")
			{
				$thisTitle = $Title;
				$thisInfoDisplay = $eReportCard['Template']['PrincipalNameCh']."<br />".$eReportCard['Template']['PrincipalNameEn'];
			}
			else if ($SettingID == "ClassTeacher")
			{
				if($StudentID)		# retrieve Student Info
				{
					include_once($PATH_WRT_ROOT."includes/libuser.php");
					include_once($PATH_WRT_ROOT."includes/libclass.php");
					$lu = new libuser($StudentID);
					$lclass = new libclass();
					
					$ClassTeacherAry = $lclass->returnClassTeacherID($lu->ClassName);
					
					# Assuming one class teacher only
					$thisTeacherID = $ClassTeacherAry[0][0];
					
					$lu = new libuser($thisTeacherID);
					$thisChineseName = $lu->ChineseName;
					switch ($lu->Title)
	                {
	                    case 0: $teacherTitle = $eReportCard['Template']['Mr']; break;
	                    case 1: $teacherTitle = $eReportCard['Template']['Miss']; break;
	                    case 2: $teacherTitle = $eReportCard['Template']['Mrs']; break;
	                    case 3: $teacherTitle = $eReportCard['Template']['Ms']; break;
	                    case 4: $teacherTitle = $eReportCard['Template']['Dr']; break;
	                    case 5: $teacherTitle = $eReportCard['Template']['Prof']; break;
	                	default: $teacherTitle = ""; break;
	                }
	                
	                if ($lu->Gender == "M" && $thisTeacherID!="")
	                {
		                $thisTitle = $eReportCard['Template']['ClassTeacherMale'];
	                }
	                else
	                {
		                $thisTitle = $eReportCard['Template']['ClassTeacherFemale'];
	                }	    
	                            
	                $thisInfoDisplay = $teacherTitle." ".$lu->EnglishName."<br />".$lu->ChineseName;
				}
				else
				{
					$thisTitle = $eReportCard['Template']['ClassTeacherMale'];
					$thisInfoDisplay = "Mr. XXXXXX<br />XXX";
				}
				
			}
			else
			{
				$thisTitle = $Title;
				$thisInfoDisplay = "&nbsp;<br />&nbsp;";
			}
			
			$SignatureTable .= "<tr><td align='center' class='signature_title' valign='bottom'>".$thisTitle."&nbsp;</td></tr>\n";
			$SignatureTable .= "<tr><td align='center' class='signature_title' valign='bottom'>".$thisInfoDisplay."&nbsp;</td></tr>\n";
			
			$SignatureTable .= "</table>\n";
			$SignatureTable .= "</td>\n";
		}

		$SignatureTable .= "</tr>\n";
		$SignatureTable .= "</table>\n";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		/*
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
		$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
		
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
			}
		}
		
		# calculate total number of columns
		$totalNumColumn = 0;
		$totalNumColumn = $ColNum2;
		if ($ShowRightestColumn) $totalNumColumn++;
		if ($AllowSubjectTeacherComment) $totalNumColumn++;
		$curColumn = 0;
		foreach ($ColumnTitle as $ColumnID => $ColumnName) {
			$totalNumColumn += sizeof($NumOfAssessment[$curColumn]);	# add number of assessments
			$totalNumColumn++;	# add number of terms
			$curColumn++;
		}
		$totalNumColumn += 2; // Subject Columns
		
		$x .= "<tr>";
		$x .= "<td class='tabletext border_top' colspan='$totalNumColumn'>";
		$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
		
		$first = 1;
		$counter = 0;
		$NumDisplayPerRow = 2;
		$width = round(100/$NumDisplayPerRow)."%";
		
		*/
		
		if ($ReportType=="W")
		{
			$ReportIDArr = $this->RETURN_REPORT_LIST($ClassLevelID);
			$ClassLevel = $this->returnClassLevel($ClassLevelID);
			$ActiveYear = $this->GET_ACTIVE_YEAR();
			
			# special handling for  ( do not show subject mark for not examed subject)
			$hideGrandInfo = false;
			if (is_array($eRCTemplateSetting['ExcludeSubjectInGrandMark'][$ActiveYear][$ClassLevel]))
				$hideGrandInfo = true;
			
			for ($i=0; $i<sizeof($ReportIDArr); $i++)
			{
				$thisReportID = $ReportIDArr[$i];
				
				# retrieve result data
				$result = $this->getReportResultScore($thisReportID, 0, $StudentID);
				
				$GrandTotalArr[$i] = $StudentID ? $result['GrandTotal'] : "S";
				$AverageMarkArr[$i] = $StudentID ? $result['GrandAverage'] : "S";
				
		 		$FormPositionArr[$i] = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol : $result['OrderMeritForm']) : $this->EmptySymbol ) : "#";
		 		$ClassPositionArr[$i] = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol : $result['OrderMeritClass']) : $this->EmptySymbol ) : "#";
		  		$ClassNoOfStudentArr[$i] = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol : $result['ClassNoOfStudent']) : $this->EmptySymbol ) : "#";
		  		$FormNoOfStudentArr[$i] = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol : $result['FormNoOfStudent']) : $this->EmptySymbol ) : "#";
		  		
		  		// Second Term => check if need special handling of class suspension
				if ($i==1 && $hideGrandInfo)
				{
					$GrandTotalArr[$i] = $this->EmptySymbol;
					$AverageMarkArr[$i] = $this->EmptySymbol;
					$FormPositionArr[$i] = $this->EmptySymbol;
					$ClassPositionArr[$i] = $this->EmptySymbol;
				}
		  	}
		}
		else
		{
			# retrieve result data
			$result = $this->getReportResultScore($ReportID, 0, $StudentID);
			$GrandTotal = $StudentID ? $result['GrandTotal'] : "S";
			$AverageMark = $StudentID ? $result['GrandAverage'] : "S";
			
	 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? "--": $result['OrderMeritForm']) : "--") : "#";
	  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
	  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? "--": $result['ClassNoOfStudent']) : "--") : "#";
	  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? "--": $result['FormNoOfStudent']) : "--") : "#";
			
			if ($CalOrder == 2) {
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
					$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] : "S";
					$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] : "S";
					$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
					$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
					$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
					$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
				}
			}
		}
		
	  	$counter = 0;
	  	# Average Mark
		if($ShowGrandAvg)
		{
			//$AverageMarkArr = array(80, 70, 75);
			$counter++;
			$thisTitle = $eReportCard['Template']['AvgMarkEn']." ".$eReportCard['Template']['AvgMarkCh'];
			$thisValue = ($ReportType=="W")? $AverageMarkArr : $AverageMark;
			$x .= $this->genFooterRow($ReportID, $counter, $thisTitle, $thisValue, $ColNum2, "", $checkAverage=1);
		}
		
		# Position in Class
		if($ShowOverallPositionClass)
		{
			//$ClassPositionArr = array(10, 15, 12);
			$counter++;
			$thisTitle = $eReportCard['Template']['ClassPositionEn']." ".$eReportCard['Template']['ClassPositionCh'];
			$thisValue = ($ReportType=="W")? $ClassPositionArr : $ClassPosition;
			$x .= $this->genFooterRow($ReportID, $counter, $thisTitle, $thisValue, $ColNum2, $OverallPositionRangeClass);
		}
		
		# Number of Students in Class
		if($ShowNumOfStudentClass)
		{
			//$ClassNoOfStudentArr = array(202, 201, 201);
			$counter++;
			$thisTitle = $eReportCard['Template']['ClassNumOfStudentEn']." ".$eReportCard['Template']['ClassNumOfStudentCh'];
			$thisValue = ($ReportType=="W")? $ClassNoOfStudentArr : $ClassNumOfStudent;
			$x .= $this->genFooterRow($ReportID, $counter, $thisTitle, $thisValue, $ColNum2);
		}
		
		# Position in Form
		if($ShowOverallPositionForm)
		{
			//$ClassPositionArr = array(10, 15, 12);
			$counter++;
			$thisTitle = $eReportCard['Template']['FormPositionEn']." ".$eReportCard['Template']['FormPositionCh'];
			$thisValue = ($ReportType=="W")? $FormPositionArr : $FormPosition;
			$x .= $this->genFooterRow($ReportID, $counter, $thisTitle, $thisValue, $ColNum2, $OverallPositionRangeForm);
		}
		
		# Number of Students in Form
		if($ShowNumOfStudentForm)
		{
			//$ClassNoOfStudentArr = array(202, 201, 201);
			$counter++;
			$thisTitle = $eReportCard['Template']['FormNumOfStudentEn']." ".$eReportCard['Template']['FormNumOfStudentCh'];
			$thisValue = ($ReportType=="W")? $FormNoOfStudentArr : $FormNumOfStudent;
			$x .= $this->genFooterRow($ReportID, $counter, $thisTitle, $thisValue, $ColNum2);
		}
		
		# Promote / Retain
		# get csv info
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# get consoildated csv if any
			foreach($csvType as $k=>$Type)
			{
				$ConsolidatedInfoTermID = 0;
				$csvData = $this->getOtherInfoData($Type, $ConsolidatedInfoTermID, $ClassName);	
				if(!empty($csvData)) 
				{
					foreach($csvData as $RegNo=>$data)
					{
						if($RegNo == $WebSamsRegNo)
						{
							foreach($data as $key=>$val)
								$ary[0][$key] = $val;
						}
					}
				}
			}
		}
		
		
		$PromotionStatus = ($ary[$TermID]['Promotion Status'])? $ary[$TermID]['Promotion Status'] : "";
		if (is_array($PromotionStatus))
			$PromotionStatus = array_remove_empty($PromotionStatus);
		//$PromotionStatus = ($ary[0]['Promotion Status'])? $ary[0]['Promotion Status'] : "";
		if ($ReportType == "W" && $PromotionStatus!="")
		{
			if (is_array($PromotionStatus))
			{
				$numOfPromotionStatus = count($PromotionStatus);
				for ($i=0; $i<$numOfPromotionStatus; $i++)
				{
					$thisStatus = $PromotionStatus[$i];
					$x .= "<tr>";
						$x .= "<td class='tabletext border_top' colspan='7'>&nbsp;&nbsp;&nbsp;".$thisStatus."</td>";
					$x .= "</tr>";
				}
			}
			else
			{
				$x .= "<tr>";
					$x .= "<td class='tabletext border_top' colspan='7'>&nbsp;&nbsp;&nbsp;".$PromotionStatus."</td>";
				$x .= "</tr>";
			}
		}
		
		
		/*
		# Overall Result
		if ($ShowGrandTotal)
		{
			$counter++;
			$thisTitle = $eReportCard['Template']['OverallResultEn']." ".$eReportCard['Template']['OverallResultCh'];
			$x .= $this->genFooterTd($counter, $thisTitle, $GrandTotal);
		}
		
		# Average Mark
		if($ShowGrandAvg)
		{
			$counter++;
			$thisTitle = $eReportCard['Template']['AvgMarkEn']." ".$eReportCard['Template']['AvgMarkCh'];
			$x .= $this->genFooterTd($counter, $thisTitle, $AverageMark, $ClassLevelID);
		}
		
		# Position in Class
		if($ShowOverallPositionClass)
		{
			$counter++;
			if ($OverallPositionRangeClass=="" || $OverallPositionRangeClass==0 || $ClassPosition <= $OverallPositionRangeClass)
			{
				$thisDisplay = $ClassPosition;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			$thisTitle = $eReportCard['Template']['ClassPositionEn']." ".$eReportCard['Template']['ClassPositionCh'];
			$x .= $this->genFooterTd($counter, $thisTitle, $thisDisplay);
		}
		
		# Number of Students in Class
		if($ShowNumOfStudentClass)
		{
			$counter++;
			$thisTitle = $eReportCard['Template']['ClassNumOfStudentEn']." ".$eReportCard['Template']['ClassNumOfStudentCh'];
			$x .= $this->genFooterTd($counter, $thisTitle, $ClassNumOfStudent);
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$counter++;
			if ($OverallPositionRangeForm=="" || $OverallPositionRangeForm==0 || $FormPosition <= $OverallPositionRangeForm)
			{
				$thisDisplay = $FormPosition;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			$thisTitle = $eReportCard['Template']['FormPositionEn']." ".$eReportCard['Template']['FormPositionCh'];
			$x .= $this->genFooterTd($counter, $thisTitle, $thisDisplay);
		}
		
		# Number of Students in Form 
		if($ShowOverallPositionForm)
		{
			$counter++;
			$thisTitle = $eReportCard['Template']['FormNumOfStudentEn']." ".$eReportCard['Template']['FormNumOfStudentCh'];
			$x .= $this->genFooterTd($counter, $thisTitle, $FormNumOfStudent);
		}
		
		##################################################################################
		# CSV related
		##################################################################################
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# calculate sems/assesment col#
			$ColNum2Ary = array();
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				if($d1['IsDetails']==1)
				{
					# check sems/assesment col#
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				}
				else
					$ColNum2Ary[$TermID] = 0;
			}
		}
		else
		{
			$InfoTermID = $SemID+1;
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		
		# Conduct
		$counter++;
		$thisTitle = $eReportCard['Template']['ConductEn']." ".$eReportCard['Template']['ConductCh'];
		if($ReportType=="W")	# Whole Year Report
		{
			$TermID = $d1['SemesterNum'];
			$thisValue = $StudentID ? ($ary[$TermID]['Conduct'] ? $ary[$TermID]['Conduct'] : $this->EmptySymbol) : "G";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID]['Conduct'] ? $ary[$SemID]['Conduct'] : $this->EmptySymbol) : "G";
		}
		$x .= $this->genFooterTd($counter, $thisTitle, $thisValue);
		
		# Neatness
		$counter++;
		$thisTitle = $eReportCard['Template']['NeatnessEn']." ".$eReportCard['Template']['NeatnessCh'];
		if($ReportType=="W")	# Whole Year Report
		{
			$TermID = $d1['SemesterNum'];
			$thisValue = $StudentID ? ($ary[$TermID]['Neatness'] ? $ary[$TermID]['Neatness'] : $this->EmptySymbol) : "G";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID]['Neatness'] ? $ary[$SemID]['Neatness'] : $this->EmptySymbol) : "G";
		}
		$x .= $this->genFooterTd($counter, $thisTitle, $thisValue);
		
		# Early Leave
		$counter++;
		$thisTitle = $eReportCard['Template']['EarlyLeaveEn']." ".$eReportCard['Template']['EarlyLeaveCh'];
		if($ReportType=="W")	# Whole Year Report
		{
			$TermID = $d1['SemesterNum'];
			$thisValue = $StudentID ? ($ary[$TermID]['Early Leave'] ? $ary[$TermID]['Early Leave'] : "0") : "G";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID]['Early Leave'] ? $ary[$SemID]['Early Leave'] : "0") : "G";
		}
		$x .= $this->genFooterTd($counter, $thisTitle, $thisValue);
		
		# Times Late
		$counter++;
		$thisTitle = $eReportCard['Template']['TimesLateEn']." ".$eReportCard['Template']['TimesLateCh'];
		if($ReportType=="W")	# Whole Year Report
		{
			$TermID = $d1['SemesterNum'];
			$thisValue = $StudentID ? ($ary[$TermID]['Time Late'] ? $ary[$TermID]['Time Late'] : "0") : "G";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID]['Time Late'] ? $ary[$SemID]['Time Late'] : "0") : "G";
		}
		$x .= $this->genFooterTd($counter, $thisTitle, $thisValue);
		
		# Days Absent
		$counter++;
		$thisTitle = $eReportCard['Template']['DaysAbsentEn']." ".$eReportCard['Template']['DaysAbsentCh'];
		if($ReportType=="W")	# Whole Year Report
		{
			$TermID = $d1['SemesterNum'];
			$thisValue = $StudentID ? ($ary[$TermID]['Days Absent'] ? $ary[$TermID]['Days Absent'] : "0") : "G";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID]['Days Absent'] ? $ary[$SemID]['Days Absent'] : "0") : "G";
		}
		$x .= $this->genFooterTd($counter, $thisTitle, $thisValue);
		
		
		if ($counter % $NumDisplayPerRow == 1)
		{
			# add a empty right cell
			$x .= "<td>&nbsp;</td>";
			# end row
			$x .= "</tr>";
		}
		
		$x .= "</table>";
		$x .= "</td></tr>";
		*/
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard, $PATH_WRT_ROOT, $ReportCardCustomSchoolName;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
 		$ClassLevel 		= $this->returnClassLevel($ClassLevelID);
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		$ActiveYear = $this->GET_ACTIVE_YEAR();
		
		$ShowRightestColumn = $ShowSubjectOverall;
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				//$css_border_top = ($isSub)? "" : "border_top";
				$css_border_top = "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				$isFirstColumnWeightZero = false;
				$lastNeedColspan = false;
				$needColspan = false;
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					if ($i==0 && $columnSubjectWeightTemp==0)
					{
						$isFirstColumnWeightZero = true;
					}
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
					$thisColumnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID IS NULL ";
					$thisColumnWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $thisColumnWeightConds);
					$thisColumnWeight = $thisColumnWeightArr[0]['Weight'];

					$lastNeedColspan = $needColspan;
					
					# show the grade of exam column even if the subject weight is zero
					if ($isSub && $columnSubjectWeightTemp == 0 && ($i != (sizeof($ColumnID) - 1)))
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
						$thisMarkDisplay = $this->EmptySymbol;
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						if (is_numeric($thisMark) && (!$isSub) && (!$isFirstColumnWeightZero))
						{
							$thisWeightedMark = $this->getDisplayMarkdp("SubjectScore", $thisMark * $thisColumnWeight);
						}
						else
						{
							$thisWeightedMark = $thisMark;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						if($needStyle)
						{
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisWeightedMark, 'HighLight', $thisNature);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
					
					if ( ($FormNumber < 5) && $isFirstColumnWeightZero && !$isSub && $i==0)
					{
						continue;
					}
					else if ( ($FormNumber < 5) && $isFirstColumnWeightZero && !$isSub && $i!=0)
					{
						$colspan = " colspan='2' ";
					}
					else
					{
						$colspan = "";
					}
					
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}' $colspan>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
					}
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
				
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$css_border_top = ($isSub && !$isLastParentSubject)? "" : "border_top";
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						if ($thisSubjectWeight == 0)
						{
							$thisMark = $this->EmptySymbol;
							$thisGrade = $this->EmptySymbol;
						}
						else
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
 						if($thisSubjectWeight > 0)
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					# hide the total of the component subject [2009-0206-1802]
					if ($isSub)
					{
						$thisMarkDisplay = "&nbsp;";
					}
					
					# show "---" for PE, Putonghua and IT
					if ($isFirstColumnWeightZero && !$isSub && $ScaleInput=="G")
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>--</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
				$isLastParentSubject = $isParentSubject;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# special handling for Class suspension (do not show subject mark for not examed subject)
			$excludeSubjectIDArr = $eRCTemplateSetting['ExcludeSubjectInGrandMark'][$ActiveYear][$ClassLevel];
			$isHideMark = false;
			if (is_array($excludeSubjectIDArr))
				$isHideMark = true;
				
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					//$css_border_top = $isSub? "" : "border_top";
					$css_border_top = "border_top";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>--</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);		
							
							$isFirstColumnWeightZero = false; 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								// Second Term => check if need special handling of class suspension
								if ($i==1 && $isHideMark && in_array($SubjectID, $excludeSubjectIDArr))
								{
									$thisMarkDisplay = $this->EmptySymbol;
								}
								else
								{
									// Normal calculation
									$thisColumnID = $ColumnID[$j];
									$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
									$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
									$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
									
									if ($j==0 && $columnSubjectWeightTemp==0)
									{
										$isFirstColumnWeightZero = true;
									}
									
									$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
									$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
									
									$thisColumnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID IS NULL ";
									$thisColumnWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $thisColumnWeightConds);
									$thisColumnWeight = $thisColumnWeightArr[0]['Weight'];
									
									if ($isSub && $columnSubjectWeightTemp == 0 && ($j != (sizeof($ColumnID) - 1)))
									{
										$thisMarkDisplay = "---";
									}
									else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
										$thisMarkDisplay = "---";
									} else {
										$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
										$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
										
										$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
										$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
										
										# for preview purpose
										if(!$StudentID)
										{
											$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
											$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
										}
										
										$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
														
										if ($thisMark != "N.A.")
										{
											$isAllNA = false;
										}
										
										if (is_numeric($thisMark) && (!$isSub) && (!$isFirstColumnWeightZero))
										{
											$thisWeightedMark = $this->getDisplayMarkdp("SubjectScore", $thisMark * $thisColumnWeight);
										}
										else
										{
											$thisWeightedMark = $thisMark;
										}
										
										# check special case
										list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
										if($needStyle)
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
											$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
											$thisMarkDisplay = $this->ReturnTextwithStyle($thisWeightedMark, 'HighLight', $thisNature);
										}
										else
										{
											$thisMarkDisplay = $thisMark;
										}
									}
								}
								
								if ( ($FormNumber < 5) && $isFirstColumnWeightZero && !$isSub && $j==0)
								{
									continue;
								}
								else if ( ($FormNumber < 5) && $isFirstColumnWeightZero && !$isSub && $j!=0)
								{
									$colspan = " colspan='2' ";
								}
								else
								{
									$colspan = "";
								}
									
								$x[$SubjectID] .= "<td class='border_left {$css_border_top}' ".$colspan.">";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
								}
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = "---";
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID);
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
							}
	 						
							$thisColumnID = $ColumnData[$i]['ReportColumnID'];
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
							
							$thisColumnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID IS NULL ";
							$thisColumnWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $thisColumnWeightConds);
							$thisColumnWeight = $thisColumnWeightArr[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								if ($thisSubjectWeight == 0)
								{
									$thisMark = $this->EmptySymbol;
									$thisGrade = $this->EmptySymbol;
								}
								else
								{
									$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
								}
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
							
							if (is_numeric($thisMark) && (!$isSub))
							{
								$thisWeightedMark = $this->getDisplayMarkdp("SubjectScore", $thisMark * $thisColumnWeight);
							}
							else
							{
								$thisWeightedMark = $thisMark;
							}
							
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
	 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisWeightedMark, 'HighLight', $thisNature);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $FullMark .")</td>";
						}
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$css_border_top = ($isSub && !$isLastParentSubject)? "" : "border_top";
					
					$MarksAry = $this->getMarks($ReportID, $StudentID);
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="")? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
						
					# hide the total of the component subject [2009-0206-1802]
					if ($isSub)
					{
						$thisMarkDisplay = "&nbsp;";
					}
					
					if ($isFirstColumnWeightZero && !$isSub && $j!=0 && $ScaleInput=="G")
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
						$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
						if($ShowSubjectFullMark)
						{
						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $SubjectFullMarkAry[$SubjectID] .")</td>";
					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>--</td>";
				}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
				$isLastParentSubject = $isParentSubject;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		//$FormNumber = 5;
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			$csvData = $this->getOtherInfoData($Type, $latestTerm, $ClassName);	
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$key] = $val;
					}
				}
			}
		}
		
		# retrieve result data
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		$Lateness = ($ary['Time Late'])? $ary['Time Late'] : 0;
		$EarlyLeave = ($ary['Early Leave'])? $ary['Early Leave'] : 0;
		$Promotion = ($ary['Promotion'])? $ary['Promotion'] : $this->EmptySymbol;
		$Merits = ($ary['Merits'])? $ary['Merits'] : 0;
		$MinorCredit = ($ary['Minor Credit'])? $ary['Minor Credit'] : 0;
		$MajorCredit = ($ary['Major Credit'])? $ary['Major Credit'] : 0;
		$Demerits = ($ary['Merits'])? $ary['Demerits'] : 0;
		$MinorFault = ($ary['Minor Fault'])? $ary['Minor Fault'] : 0;
		$MajorFault = ($ary['Major Fault'])? $ary['Major Fault'] : 0;
		$Remark = ($ary['Remark'])? $ary['Remark'] : "";
		$ECA = ($ary['ECA'])? $ary['ECA'] : "";
		
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2007a/10x10.gif";
		
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr><td><img src='".$emptyImagePath."' height='5'></td></tr>";
		$x .= "</table>";
		
		# Comment Table
		$ReportIDArr = $this->RETURN_REPORT_LIST($ClassLevelID, 1);
		$TermCommentArr = array();
		for ($i=0; $i<count($ReportIDArr); $i++)
		{
			$thisReportID = $ReportIDArr[$i];
			$thisTermCommentArr = $this->returnSubjectTeacherComment($thisReportID, $StudentID);
			$TermCommentArr[] = $thisTermCommentArr[0];
		}
		
		//$ClassTeacherComment = "comment 1"."\n"."comment 2";
		$thisTitle = $eReportCard['Template']['ClassTeacherComment'];
		$commentTable = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
			$commentTable .= "<tr>";
				$commentTable .= "<td class='tabletext' valign='top' width='2'>&nbsp;&nbsp;</b></td>";
				$commentTable .= "<td class='tabletext' valign='top' width='100%'><b>". $thisTitle ."</b></td>";
			$commentTable .= "</tr>";
			
			$commentTable .= "<tr>";
				$commentTable .= "<td class='tabletext' valign='top'>&nbsp;&nbsp;</b></td>";
				$commentTable .= "<td valign='top'>";
					$commentTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top'>";
					
			
			$DisplayedCommentNumber = 0;
			$nextLineConstantLength = 81;
			$nextLineConstantLength2 = 75;
			$meritTableExtraHeight = 0;
			for ($j=0; $j<count($ReportIDArr); $j++)
			{
				$ClassTeacherComment = $TermCommentArr[$j];
				$CommentArr = explode("\n", $ClassTeacherComment);
				
				if ($ReportType == "W")
				{
					$separator = "&nbsp;&nbsp;&nbsp;&nbsp;";
					$thisSemTitle = ($j==0)? $eReportCard['Template']['1stTermEn'].$separator.$eReportCard['Template']['1stTermCh'] : $eReportCard['Template']['2ndTermEn'].$separator.$eReportCard['Template']['2ndTermCh'];
					
					$commentTable .= "<tr valign='top'>";
						$commentTable .= "<td class='tabletext' valign='top'>". $thisSemTitle ."</td>";
					$commentTable .= "</tr>";
				}
				
				for ($i=0; $i<count($CommentArr); $i++)
				{
					$thisComment = stripslashes($CommentArr[$i]);
					if ($thisComment=="") continue;
					
					# count number of rows for the comment
					$thisCommentArr = array_remove_empty(explode(".", $thisComment));
					$numSentence = count($thisCommentArr);
					$numWord = substr_count($thisComment," ");
					if (strlen($thisComment) > $nextLineConstantLength)
					{
						$numExtraLine = floor(strlen($thisComment) / $nextLineConstantLength);
						$meritTableExtraHeight += 12 * $numExtraLine;
					}
					elseif (strlen($thisComment) > $nextLineConstantLength2)
					{
						if ($numSentence > 2 && $numWord > 10)
						{
							# add one more line if the comment is consisted from 3 sentences and there are more than 10 space bar in the comment
							$meritTableExtraHeight += 12;
						}
						else if (($numWord > 13) && (substr_count(strtolower($thisComment),"a ") < 2))
						{
							$meritTableExtraHeight += 12;
						}
					}
					
					$commentTable .= "<tr valign='top'>";
						$commentTable .= "<td class='comment_text' valign='top'>". $thisComment ."</td>";
					$commentTable .= "</tr>";
					
					$DisplayedCommentNumber++;
				}
				
				if ($ReportType == "W" && $j==0)
				{
					$commentTable .= $this->Get_Empty_Row(2);
				}
			}
			
			$TotalCommentQuota = 3;
			$BlankRowNumber = $TotalCommentQuota - $DisplayedCommentNumber;
			for ($i=0; $i<$BlankRowNumber; $i++)
			{
				$commentTable .= "<tr>";
				$commentTable .= "<td class='comment_text' valign='top'>&nbsp;</td>";
				$commentTable .= "</tr>";
			}
			
					$commentTable .= "</table>";
				$commentTable .= "</td>";
			$commentTable .= "</tr>";
			
			//$commentTable .= "<tr><td class='comment_text' height='100%' valign='top'>&nbsp;</td></tr>";
		$commentTable .= "</table>";
		
		# ECA Table for (P5-6)
		if ($FormNumber > 4)
		{
			//$ECA = array("ECA 1", "ECA 2", "ECA 3", "ECA 4");
			$thisTitle = $eReportCard['Template']['eca'];
			$ECA_Table .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
				$ECA_Table .= "<tr>";
					$ECA_Table .= "<td class='tabletext' valign='top' width='2'>&nbsp;&nbsp;</b></td>";
					$ECA_Table .= "<td class='tabletext' valign='top' width='100%'><b>". $thisTitle ."</b></td>";
				$ECA_Table .= "</tr>";
				
				$ECA_Table .= "<tr>";
					$ECA_Table .= "<td class='tabletext' valign='top'>&nbsp;&nbsp;</b></td>";
					$ECA_Table .= "<td valign='top'>";
						$ECA_Table .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top'>";
						
				$DisplayedECANumber = 0;
				if (is_array($ECA))
				{
					for ($i=0; $i<count($ECA); $i++)
					{
						$thisECA = stripslashes($ECA[$i]);
						if ($thisECA=="") continue;
						
						$ECA_Table .= "<tr valign='top'>";
						$ECA_Table .= "<td class='comment_text' valign='top'>". $thisECA ."</td>";
						$ECA_Table .= "</tr>";
						
						$DisplayedECANumber++;
					}
				}
				else
				{
					$ECA_Table .= "<tr>";
					$ECA_Table .= "<td class='comment_text' valign='top'>". stripslashes($ECA) ."</td>";
					$ECA_Table .= "</tr>";
					
					$DisplayedECANumber++;
				}
				
				$TotalECAQuota = 4;
				$BlankRowNumber = $TotalECAQuota - $DisplayedECANumber;
				for ($i=0; $i<$BlankRowNumber; $i++)
				{
					$ECA_Table .= "<tr>";
					$ECA_Table .= "<td class='comment_text' valign='top'>&nbsp;</td>";
					$ECA_Table .= "</tr>";
				}
				
						$ECA_Table .= "</table>";
					$ECA_Table .= "</td>";
				$ECA_Table .= "</tr>";
				
				//$ECA_Table .= "<tr><td class='comment_text' height='100%' valign='top'>&nbsp;</td></tr>";
			$ECA_Table .= "</table>";
		}
		
		# CSV Info Table (Conduct, Neatness, Early Leave, Times Late, Days Absent)
		# get CSV data	
		$ary = array();
		$csvType = $this->getOtherInfoType();
		
		if($SemID=="F")
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$InfoTermID = $TermID+1;
				
				if(!empty($csvType))
				{
					foreach($csvType as $k=>$Type)
					{
						$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
						if(!empty($csvData)) 
						{
							foreach($csvData as $RegNo=>$data)
							{
								if($RegNo == $WebSamsRegNo)
								{
									foreach($data as $key=>$val)
										$ary[$TermID][$key] = $val;
								}
							}
						}
					}
				}
			}
			
			# get consoildated csv if any
			foreach($csvType as $k=>$Type)
			{
				$ConsolidatedInfoTermID = 0;
				$csvData = $this->getOtherInfoData($Type, $ConsolidatedInfoTermID, $ClassName);	
				if(!empty($csvData)) 
				{
					foreach($csvData as $RegNo=>$data)
					{
						if($RegNo == $WebSamsRegNo)
						{
							foreach($data as $key=>$val)
								$ary[0][$key] = $val;
						}
					}
				}
			}
		}
		else
		{
			$InfoTermID = $SemID+1;
			
			if(!empty($csvType))
			{
				foreach($csvType as $k=>$Type)
				{
					$csvData = $this->getOtherInfoData($Type, $InfoTermID, $ClassName);	
					if(!empty($csvData)) 
					{
						foreach($csvData as $RegNo=>$data)
						{
							if($RegNo == $WebSamsRegNo)
							{
								foreach($data as $key=>$val)
									$ary[$SemID][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		# Remarks Row
		if ($SemID=="F")
		{
			$Remark = ($ary[$TermID]['Remark'])? $ary[$TermID]['Remark'] : "";
			//$Promotion = ($ary[$TermID]['Promotion Status'])? $ary[$TermID]['Promotion Status'] : "";
		}
		else
		{
			$Remark = ($ary[$SemID]['Remark'])? $ary[$SemID]['Remark'] : "";
			//$Promotion = ($ary[$SemID]['Promotion Status'])? $ary[$SemID]['Promotion Status'] : "";
		}
		//$Remark = array("Remark 1", "Remark 2", "Remark 3");
		$Remark_Row .= "<tr>";
			$thisTitle = $eReportCard['Template']['Remark'];
			$Remark_Row .= "<td width='100%' valign='top' colspan='3'>";
				$Remark_Row .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
					$Remark_Row .= "<tr>";
						$Remark_Row .= "<td class='tabletext' height='$LineHeight' valign='top' colspan='2'><b>". $thisTitle ."</b></td>";
					$Remark_Row .= "</tr>";
					/*
					if (is_array($Promotion))
					{
						$Remark_Row .= "<tr><td class='tabletext' valign='top'>";
						for ($i=0; $i<count($Promotion); $i++)
						{
							$thisRemark = $Promotion[$i];
							if ($thisRemark!="")
							{
								$Remark_Row .= "<li>". $thisRemark ."</li><br />";
							}
						}
						$Remark_Row .= "</td></tr>\n";
					}
					else
					{
						if ($Promotion!="")
						{
							$Remark_Row .= "<tr>";
								$Remark_Row .= "<td class='tabletext' valign='top'><li>". $Promotion ."</li></td>";
							$Remark_Row .= "</tr>\n";
						}
					}
					*/
					if (is_array($Remark))
					{
						for ($i=0; $i<count($Remark); $i++)
						{
							$thisRemark = $Remark[$i];
							if ($thisRemark!="")
							{
								$Remark_Row .= "<tr>";
									$Remark_Row .= "<td class='tabletext' valign='top' width='1'><li>&nbsp;</li></td>";
									$Remark_Row .= "<td class='tabletext' valign='top' width='99%'>". $thisRemark ."</li></td>";
								$Remark_Row .= "</tr>\n";
							}
						}
					}
					else
					{
						if ($Remark!="")
						{
							$Remark_Row .= "<tr>";
								$Remark_Row .= "<td class='tabletext' valign='top' width='1'><li>&nbsp;</li></td>";
								$Remark_Row .= "<td class='tabletext' valign='top' width='99%'>". $Remark ."</li></td>";
							$Remark_Row .= "</tr>\n";
						}
					}
				$Remark_Row .= "</table>\n";
			$Remark_Row .= "</td>\n";
		$Remark_Row .= "</tr>\n";
		
		## Merit / Demerit Table
		if ($FormNumber > 4)
		{
			if ( $ECA!=NULL || $ECA!="" )
			{
				$MeritTableHeight = "232";
				$TitleHeight = "30px";
			}
			else
			{
				$MeritTableHeight = "222";
				$TitleHeight = "30px";
			}
			$cellpadding = "5";
		}
		else
		{
			$MeritTableHeight = "130";
			$cellpadding = "2";
		}
		$MeritTableHeight += $meritTableExtraHeight;
		$MeritTableHeight = $MeritTableHeight."px";
		
		$MeritTable = "";
		$MeritTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"".$cellpadding."\" cellspacing=\"0\" class='report_border' height='".$MeritTableHeight."'>";
		# Title
		$thisTitle = "&nbsp;";
		$thisFirstTerm = $eReportCard['Template']['1stTermEn']."<br />".$eReportCard['Template']['1stTermCh'];
		$thisSecondTerm = $eReportCard['Template']['2ndTermEn']."<br />".$eReportCard['Template']['2ndTermCh'];
		$thisWholeYear = $eReportCard['Template']['WholeYearEn']."<br />".$eReportCard['Template']['WholeYearCh'];
		
		if ($ReportType == "W")
		{
			$MeritTable .= "<tr>";
				$MeritTable .= "<td class='merit_tabletext {$border_top}' height='".$TitleHeight."' width='40%' align='center' colspan='2'><b>".$thisTitle."</b></td>";
				$MeritTable .= "<td class='merit_tabletext border_left {$border_top}' height='".$TitleHeight."' width='30%' align='center'><b>".$thisFirstTerm."</b></td>";
				$MeritTable .= "<td class='merit_tabletext border_left {$border_top}' height='".$TitleHeight."' width='30%' align='center'><b>".$thisSecondTerm."</b></td>";
				//$MeritTable .= "<td class='merit_tabletext border_left {$border_top}' width='20%' align='center'><b>".$thisWholeYear."</b></td>";
			$MeritTable .= "</tr>";
		}
		else
		{
			$MeritTable .= "<tr>";
				$MeritTable .= "<td class='merit_tabletext {$border_top}' height='".$TitleHeight."' width='40%' align='center' colspan='2'><b>".$thisTitle."</b></td>";
				$MeritTable .= "<td class='merit_tabletext border_left {$border_top}' height='".$TitleHeight."' width='20%' align='center'><b>".$thisFirstTerm."</b></td>";
			$MeritTable .= "</tr>";
		}
		
		# Conduct
		$thisSetting = "Conduct";
		$MeritTable .= "<tr>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['ConductEn']."</td>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>".$eReportCard['Template']['ConductCh']."&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
			}
			//$thisTotal = $StudentID ? ($ary[0][$thisSetting])? $ary[0][$thisSetting] : $this->EmptySymbol : "#";
			//$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>".$thisTotal."&nbsp;</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		$MeritTable .= "</tr>";
		
		# Neatness
		$thisSetting = "Neatness";
		$MeritTable .= "<tr>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['NeatnessEn']."</td>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>".$eReportCard['Template']['NeatnessCh']."&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ary[$TermID][$thisSetting] ? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
			}
			//$thisTotal = $StudentID ? ($ary[0][$thisSetting])? $ary[0][$thisSetting] : $this->EmptySymbol : "#";
			//$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>".$thisTotal."&nbsp;</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? ($ary[$SemID][$thisSetting] ? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		$MeritTable .= "</tr>";
		
		# Early Leave
		$thisSetting = "Early Leave";
		$MeritTable .= "<tr>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['EarlyLeaveEn']."</td>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>".$eReportCard['Template']['EarlyLeaveCh']."&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ( ($ary[$TermID][$thisSetting]!="")? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
			}
			if ($thisTotal == "") 
			{
				$thisTotal = $StudentID ? $this->EmptySymbol : "#";
			}
			//$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>".$thisTotal."&nbsp;</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? (($ary[$SemID][$thisSetting]!="")? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		$MeritTable .= "</tr>";
		
		# Time Late
		$thisSetting = "Time Late";
		$MeritTable .= "<tr>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['TimesLateEn']."</td>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>".$eReportCard['Template']['TimesLateCh']."&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? (($ary[$TermID][$thisSetting]!="")? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
			}
			if ($thisTotal == "") 
			{
				$thisTotal = $StudentID ? $this->EmptySymbol : "#";
			}
			//$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>".$thisTotal."&nbsp;</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? (($ary[$SemID][$thisSetting]!="")? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		$MeritTable .= "</tr>";
		
		# Days Absent
		$thisSetting = "Days Absent";
		$MeritTable .= "<tr>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Template']['DaysAbsentEn']."</td>";
			$MeritTable .= "<td class='merit_tabletext border_top' nowrap>".$eReportCard['Template']['DaysAbsentCh']."&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		if($ReportType=="W")	# Whole Year Report
		{
			$thisTotal = "";
			foreach($ColumnData as $k1=>$d1)
			{
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? (($ary[$TermID][$thisSetting]!="")? $ary[$TermID][$thisSetting] : $this->EmptySymbol) : "#";
				if (is_numeric($thisValue)) {
					if ($thisTotal == "")
						$thisTotal = $thisValue;
					else if (is_numeric($thisTotal))
						$thisTotal += $thisValue;
				}
				$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
			}
			if ($thisTotal == "") 
			{
				$thisTotal = $StudentID ? $this->EmptySymbol : "#";
			}
			//$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>".$thisTotal."&nbsp;</td>";
		}
		else				# Term Report
		{
			$thisValue = $StudentID ? (($ary[$SemID][$thisSetting]!="")? $ary[$SemID][$thisSetting] : $this->EmptySymbol) : "#";
			$MeritTable .= "<td class='merit_tabletext border_top border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		$MeritTable .= "</tr>";
		$MeritTable .= "</table>";
		
		
		# Build Main Table
		# Left: Demerit/merit Table
		# Right: Grading Scheme, Comment, ECA (for P5-6)
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				$x .= "<td width='49%' valign='top' height=\"100%\">";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						$x .= "<tr>";
							$x .= "<td valign='top'>".$MeritTable."</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td width='1%' valign='top'>&nbsp;</td>";
				$x .= "<td width='49%' valign='top' height=\"100%\">";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\">";
						$x .= "<tr height=\"100%\"><td valign='top'>".$this->genMarkingSchemeTable($FormNumber)."</td></tr>";
						$x .= "<tr height=\"100%\"><td><img src='".$emptyImagePath."' height='3'></td></tr>";
						$x .= "<tr height=\"100%\"><td valign='middle'>".$commentTable."</td></tr>";
						$x .= "<tr height=\"100%\"><td><img src='".$emptyImagePath."' height='3'></td></tr>";
						if ($FormNumber > 4)
						{
							$x .= "<tr height=\"100%\"><td valign='bottom'>".$ECA_Table."</td></tr>";
						}
						
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr><td colspan='2'><img src='".$emptyImagePath."' height='3'></td></tr>";
			$x .= "<tr>";
				$x .= "<td valign='top' colspan='2'>";
					$x .= $Remark_Row;
				$x .= "</td>";
			$x .= "</tr>";
		$x .= "</table>";
		/*
		# Comment and Marking Scheme
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr>";
				# Comment
				$thisTitle = $eReportCard['Template']['ClassTeacherComment'];
				$x .= "<td width='49%' valign='top'>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='".$this->MarkingSchemeHeight."' class='report_border'>";
						$x .= "<tr>";
							$x .= "<td class='tabletext' valign='top' width='2'>&nbsp;&nbsp;</b></td>";
							$x .= "<td class='tabletext' valign='top' width='100%'><b>". $thisTitle ."</b></td>";
						$x .= "</tr>";
						
						$x .= "<tr>";
							$x .= "<td class='tabletext' valign='top'>&nbsp;&nbsp;</b></td>";
							$x .= "<td valign='top'>";
								$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top'>";
								
						$DisplayedCommentNumber = 0;
						$CommentArr = explode("\n", $ClassTeacherComment);
						if (is_array($CommentArr))
						{
							for ($i=0; $i<count($CommentArr); $i++)
							{
								$thisComment = stripslashes($CommentArr[$i]);
								if ($thisComment=="") continue;
								
								$x .= "<tr valign='top'>";
								$x .= "<td class='comment_text' valign='top'>". $thisComment ."</td>";
								$x .= "</tr>";
								
								$DisplayedCommentNumber++;
							}
						}
						else
						{
							$x .= "<tr>";
							$x .= "<td class='comment_text' valign='top'>". stripslashes($ClassTeacherComment) ."</td>";
							$x .= "</tr>";
							$DisplayedCommentNumber++;
						}
						
						$TotalCommentQuota = 3;
						$BlankRowNumber = $TotalCommentQuota - $DisplayedCommentNumber;
						for ($i=0; $i<$BlankRowNumber; $i++)
						{
							$x .= "<tr>";
							$x .= "<td class='comment_text' valign='top'>&nbsp;</td>";
							$x .= "</tr>";
						}
						
								$x .= "</table>";
							$x .= "</td>";
						$x .= "</tr>";
						
						//$x .= "<tr><td class='comment_text' height='100%' valign='top'>&nbsp;</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				# Empty Column
				$x .= "<td>&nbsp;</td>";
				# Marking Scheme
				$x .= "<td width='49%' valign='top'>";
					$x .= $this->genMarkingSchemeTable($FormNumber);
				$x .= "</td>";
			$x .= "</tr>";
			
			$emptyImagePath = $PATH_WRT_ROOT."images/2007a/10x10.gif";
			$x .= "<tr><td><img src='".$emptyImagePath."' height='5'></td></tr>";
			//$x .= "<tr><td height='$LineHeight'>&nbsp;</td></tr>";
			
			if ($FormNumber > 4)
			{
				# ECA
				$thisTitle = $eReportCard['Template']['eca'];
				$x .= "<td width='49%' valign='top'>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='".$this->MarkingSchemeHeight."' class='report_border'>";
						$x .= "<tr>";
							$x .= "<td class='tabletext' valign='top' width='2'>&nbsp;&nbsp;</b></td>";
							$x .= "<td class='tabletext' valign='top' width='100%'><b>". $thisTitle ."</b></td>";
						$x .= "</tr>";
						
						$x .= "<tr>";
							$x .= "<td class='tabletext' valign='top'>&nbsp;&nbsp;</b></td>";
							$x .= "<td valign='top'>";
								$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top'>";
								
						$DisplayedECANumber = 0;
						if (is_array($ECA))
						{
							for ($i=0; $i<count($ECA); $i++)
							{
								$thisECA = stripslashes($ECA[$i]);
								if ($thisECA=="") continue;
								
								$x .= "<tr valign='top'>";
								$x .= "<td class='comment_text' valign='top'>". $thisECA ."</td>";
								$x .= "</tr>";
								
								$DisplayedECANumber++;
							}
						}
						else
						{
							$x .= "<tr>";
							$x .= "<td class='comment_text' valign='top'>". stripslashes($ECA) ."</td>";
							$x .= "</tr>";
							
							$DisplayedECANumber++;
						}
						
						$TotalECAQuota = 4;
						$BlankRowNumber = $TotalECAQuota - $DisplayedECANumber;
						for ($i=0; $i<$BlankRowNumber; $i++)
						{
							$x .= "<tr>";
							$x .= "<td class='comment_text' valign='top'>&nbsp;</td>";
							$x .= "</tr>";
						}
						
								$x .= "</table>";
							$x .= "</td>";
						$x .= "</tr>";
						
						//$x .= "<tr><td class='comment_text' height='100%' valign='top'>&nbsp;</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
					# Empty Column
					$x .= "<td>&nbsp;</td>";
					# Empty Column
					$x .= "<td>&nbsp;</td>";
				$x .= "</tr>";
				
				$emptyImagePath = $PATH_WRT_ROOT."images/2007a/10x10.gif";
				$x .= "<tr><td><img src='".$emptyImagePath."' height='5'></td></tr>";

				//$x .= "<tr><td height='$LineHeight'>&nbsp;</td></tr>";
			}
			
			$x .= "<tr>";
				# Remarks
				$thisTitle = $eReportCard['Template']['Remark'];
				$x .= "<td width='100%' valign='top' colspan='3'>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						$x .= "<tr>";
							$x .= "<td class='tabletext' height='$LineHeight' valign='top'><b>". $thisTitle ."</b></td>";
						$x .= "</tr>";
						if (is_array($Remark))
						{
							$x .= "<tr><td class='tabletext' height='$LineHeight' valign='top'>";
							for ($i=0; $i<count($Remark); $i++)
							{
								$thisRemark = $Remark[$i];
								if ($thisRemark!="")
								{
									$x .= "<li>". $thisRemark ."</li><br />";
								}
							}
							$x .= "</td></tr>\n";
						}
						else
						{
							if ($Remark!="")
							{
								$x .= "<tr>";
									$x .= "<td class='tabletext' height='$LineHeight' valign='top'><li>". $Remark ."</li></td>";
								$x .= "</tr>\n";
							}
						}
					$x .= "</table>\n";
				$x .= "</td>\n";
				
			$x .= "</tr>\n";
		
		$x .= "</table>\n";
		*/
		
		return $x;
	}
	
	function genFooterTd($Counter, $Title, $Value, $ClassLevelID=0)
	{
		$NumDisplayPerRow = 2;
		$x = "";
		
		if ($Counter % $NumDisplayPerRow == 1)
		{
			# new row
			$x .= "<tr>\n";
		}
		else
		{
			# add empty space
			$x .= "<td height='{$LineHeight}' width='8%'>&nbsp;</td>";
		}
		
		# add style to average marks if failed
		if ($ClassLevelID)
		{
			/*
			$FormNum = $this->GET_FORM_NUMBER($ClassLevelID);
			if ($FormNum > 4)
			{
				$PassingMark = 60;
			}
			else
			{
				$PassingMark = 50;
			}
			*/
			$PassingMark = 60;
			
			if ($Value < $PassingMark)
			{
				$Value = $this->ReturnTextwithStyle($Value, 'HighLight', 'Fail');
			}
			
		}
		
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}' width='46%'>\n";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>\n";
				$x .= "<tr>\n";
					$x .= "<td>&nbsp;&nbsp;</td>\n";
					$x .= "<td height='{$LineHeight}' class='tabletext report_footer_text' width='65%'>". $Title ."</td>\n";
					$x .= "<td height='{$LineHeight}' class='tabletext' width='35%'>: ". $Value ."</td>\n";
				$x .= "</tr>\n";
			$x .= "</table>\n";
		$x .= "</td>\n";
		
		if ($Counter % $NumDisplayPerRow == 0)
		{
			# end row
			$x .= "</tr>";
		}		
		
		return $x;
	}
	
	function genFooterRow($ReportID, $RowCounter, $Title, $Value, $ColNum2=1, $DisplayRange=0, $CheckAverage=0)
	{
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		if ($ReportType=="W")
		{
			if (count($ColumnTitle) > 0)
			{
				$colspan = ceil($ColNum2 / count($ColumnTitle));
			}
			else
			{
				$colspan = 1;
			}
		}
		else
		{
			$colspan = count($ColumnTitle);
		}
		
		$x = "";
		
		if ($RowCounter == 1)
		{
			$border_top = "border_top";
		}
		
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}' colspan='2'>&nbsp;&nbsp;&nbsp;".$Title."</td>";
		
		if ($ReportType == "W")
		{
			$thisValue = $this->Return_Display_Position($Value[0], $DisplayRange);
			if ($CheckAverage == 1 && is_numeric($thisValue))
			{
				# add style to average marks if failed
				$thisValue = $this->Apply_Average_Style($thisValue);
			}
			$x .= "<td class='tabletext {$border_top} border_left' height='{$LineHeight}' colspan='".$colspan."' align='center'>".$thisValue."</td>";
			
			
			$thisValue = $this->Return_Display_Position($Value[1], $DisplayRange);
			if ($CheckAverage == 1 && is_numeric($thisValue))
			{
				# add style to average marks if failed
				$thisValue = $this->Apply_Average_Style($thisValue);
			}
			$x .= "<td class='tabletext {$border_top} border_left' height='{$LineHeight}' colspan='".$colspan."' align='center'>".$thisValue."</td>";
			
			
			if ($ShowSubjectOverall)
			{
				$thisValue = $this->Return_Display_Position($Value[2], $DisplayRange);
				if ($CheckAverage == 1  && is_numeric($thisValue))
				{
					# add style to average marks if failed
					$thisValue = $this->Apply_Average_Style($thisValue);
				}
				$x .= "<td class='tabletext {$border_top} border_left' height='{$LineHeight}' align='center'>".$thisValue."</td>";
			}
		}
		else
		{
			if ($ShowSubjectOverall)
			{
				$x .= "<td class='tabletext {$border_top} border_left' height='{$LineHeight}' colspan='".$colspan."' align='center'>&nbsp;</td>";
			}
			if ($CheckAverage == 1 && is_numeric($thisValue))
			{
				# add style to average marks if failed
				$Value = $this->Apply_Average_Style($Value);
			}
			$x .= "<td class='tabletext {$border_top} border_left' height='{$LineHeight}' align='center'>".($this->Return_Display_Position($Value, $DisplayRange))."</td>";
		}
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Return_Display_Position($position, $limit=0)
	{
		if ($limit == 0 || $position <= $limit)
		{
			return $position;
		}
		else
		{
			return $this->EmptySymbol;
		}
	}
	
	function genMarkingSchemeTable($Form)
	{
		global $eReportCard;
		
		$x = "";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height='100%' class='report_border'>\n";
			# Title
			$x .= "<tr>";
				$x .= "<td class='marking_scheme_text' width='15%'>&nbsp;</td>\n";
				$x .= "<td class='marking_scheme_text'>".$eReportCard['Template']['MarkingSchemeMarks']."</td>\n";
				$x .= "<td class='marking_scheme_text' align='center'>".$eReportCard['Template']['MarkingSchemeGrade']."</td>\n";	
				$x .= "<td class='marking_scheme_text' width='5%'>&nbsp;</td>\n";
			$x .= "</tr>";
			
			$x .= $this->genMarkingSchemeRow("90-100", "A", $border_top=0);
			$x .= $this->genMarkingSchemeRow("80-89", "B", $border_top=0);
			$x .= $this->genMarkingSchemeRow("70-79", "C", $border_top=0);
			$x .= $this->genMarkingSchemeRow("60-69", "D", $border_top=0);
			
			# �t�Űǭ^��p�� - eRC - template modified [CRM Ref No.: 2009-0121-0943]
			# All forms have the same marking scheme
			/*
			if ($Form < 5)
			{
				$x .= $this->genMarkingSchemeRow("50-59", "E", $border_top=1);
				$x .= $this->genMarkingSchemeRow("Under 50", "F", $border_top=0);
			}
			else
			{
				$x .= $this->genMarkingSchemeRow("Under 60", "E", $border_top=1);
			}
			*/
			$x .= $this->genMarkingSchemeRow("Under 60", "E", $border_top=1);
		$x .= "</table>";
		
		return $x;
	}
	
	function genMarkingSchemeRow($Range, $Grade, $isBorderTop)
	{
		if ($isBorderTop)
		{
			$border_top = "border_top";
		}
		else
		{
			$border_top = "";
		}
		
		$x .= "<tr>";
			$x .= "<td class='marking_scheme_text' width='15%'>&nbsp;</td>\n";
			$x .= "<td class='marking_scheme_text {$border_top}'>".$Range."</td>\n";
			$x .= "<td class='marking_scheme_text {$border_top}' align='center'>".$Grade."</td>\n";	
			$x .= "<td class='marking_scheme_text' width='15%'>&nbsp;</td>\n";
		$x .= "</tr>";
		
		return $x;
	}
	
	########### END Template Related
	
	function GET_FORM_NUMBER($ClassLevelID)
	{
		$ClassLevelName = $this->returnClassLevel($ClassLevelID);
		$FormNumber = substr($ClassLevelName,strlen($ClassLevelName)-1,1);
		
		return $FormNumber;
	}
	
	function RETURN_REPORT_LIST($ClassLevelID, $TermReportOnly=0)
	{			
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$semesterConds = "";
		if ($TermReportOnly)
		{
			$semesterConds = " AND a.Semester != 'F' ";
		}
		
		$sql = "	
			SELECT 
					a.ReportID
			FROM 
					$table as a
					left join INTRANET_CLASSLEVEL as b on a.ClassLevelID = b.ClassLevelID
			WHERE 
					a.ClassLevelID = $ClassLevelID
				AND
					a.ReportID
				$semesterConds
			ORDER BY
					a.Semester
		";
		$TermReportIDArr = $this->returnVector($sql);
		
		return $TermReportIDArr;
	}	
	
	function Apply_Average_Style($value)
	{
		$PassingMark = 60;
		
		if ($value < $PassingMark)
		{
			$value = $this->ReturnTextwithStyle($value, 'HighLight', 'Fail');
		}
		
		return $value;
	}

}
?>