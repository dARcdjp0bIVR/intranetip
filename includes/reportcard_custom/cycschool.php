<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "merit","behaviour","otherinfor");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0; 
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard;
		
		$Comment = $this->Get_Class_Teacher_Comment_Table($StudentID,'',$ReportID);
		$TableTop = "<table height='600px' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableTop .= "<tr><td colspan='3'>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td colspan='3'>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td colspan='3'>"."</br>"."</td></tr>";
			$TableTop .= "<tr>";
				$TableTop .= "<td height='100%' width='48%' valign='top'>".$MSTable."</td>";
				$TableTop .= "<td height='100%' width='2%'>"." "."</td>";
				$TableTop .= "<td height='100%' width='48%' valign='top'>".$MiscTable."</td>";
			$TableTop .= "</tr>";
			$TableTop .= "<tr>";
				$TableTop .= "<td colspan='3'>"."</br>"."</td>";
			$TableTop .= "</tr>";
			$TableTop .= "<tr>";
				$TableTop .= "<td colspan='3'>".$Comment."</td>";
			$TableTop .= "</tr>";
		$TableTop .= "</table>";

		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
// 		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
		
			# get school badge
			$imgfile = "/reportcard2008/templates/cycschool_logo.jpg";
			$SchoolLogo = ($imgfile != "") ? "<img src=\"/file/{$imgfile}\" width=120 height=108>\n" : "";
		
			# get school name
			$SchoolName_CN = $eReportCard['Template']['SchoolInfo']['NameCh'] ;
			$SchoolName_EN = $eReportCard['Template']['SchoolInfo']['NameEn'] ;
				
			#$AcademicYear
			$AcademicYear = GET_ACADEMIC_YEAR();
			
			#Academic Semester
			$SemID =  $ReportSetting['Semester'];
			$Semester = $this->returnSemesters($SemID);
		
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
		
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120px' align='center'>".$TempLogo."</td>";
			$TitleTable .= "<td width='600px'>";
				$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td width='100%' colspan='3'nowrap='nowrap'  class='text20px' align='center'>".$SchoolName_CN."</td></tr>";
					$TitleTable .= "<tr><td width='100%' colspan='3'nowrap='nowrap'  class='text16px' align='center'>".$SchoolName_EN."</td></tr>";
					$TitleTable .= "<tr><td width='100%' colspan='3' nowrap='nowrap' class='text20px tableline' align='center'></td></tr>"; // underline
					$TitleTable .= "<tr>";
						$TitleTable .= "<td width='33%' align='center'>".$eReportCard['Template']['SchoolInfo']['Address'] ."</td>";
						$TitleTable .= "<td width='33%' align='center'>".$eReportCard['Template']['SchoolInfo']['Tel']."</td>";
						$TitleTable .= "<td width='33%' align='center'>".$eReportCard['Template']['SchoolInfo']['Fax']."</td>";
					$TitleTable .= "</tr>";
					$TitleTable .= "<tr>";
						$TitleTable .= "<td></td>";
					$TitleTable .= "</tr>";
					$TitleTable .= "<tr>";
						$TitleTable .= "<td width='33%' align='center'>".$AcademicYear."</td>";
						$TitleTable .= "<td width='33%' align='center'>".$Semester."</td>";
						$TitleTable .= "<td width='33%' align='center'>".$eReportCard['Template']['SchoolInfo']['ReportCard']."</td>";
					$TitleTable .= "</tr>";
				$TitleTable .= "</table>";
			$TitleTable .= "</td>";
// 			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
		
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				$data['NameCh'] = $lu->ChineseName;
				$data['NameEn'] = $lu->EnglishName;
				$data['ClassNo'] = $lu->ClassNumber;
				$data['StudentNo'] = $data['ClassNo'];
				$data['Class'] = $lu->ClassName;
				$data['DateOfBirth'] = $lu->DateOfBirth;
// 				$data['Gender'] = $lu->Gender;
// 				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
// 				$data['StudentAdmNo'] = $data['STRN'];
		
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				$sql = "SELECT 
							AdmissionDate 
						FROM 
							INTRANET_USER_PERSONAL_SETTINGS
						WHERE 
							UserID = '$StudentID'";
				$rs = $lu->returnResultSet($sql);
				$data['AdmissionDate'] = $rs[0]['AdmissionDate'];
			}
				
			$StudentInfoTable .= "<table class='report_border' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='span_left_1'>".$eReportCard['Template']['StudentInfo']['NameCh'].$data['NameCh']."</td>";
					$StudentInfoTable .= "<td class='span_left_1'>".$eReportCard['Template']['StudentInfo']['NameEn'].$data['NameEn']."</td>";
					$StudentInfoTable .= "<td class='span_left_1'>".$eReportCard['Template']['StudentInfo']['DateOfEnter'].$data['AdmissionDate']."</td>";
				$StudentInfoTable .= "</tr>";
				$StudentInfoTable .= "<tr>";
					$StudentInfoTable .= "<td class='span_left_1'>".$eReportCard['Template']['StudentInfo']['ClassCh'].$data['Class']."</td>";
					$StudentInfoTable .= "<td class='span_left_1'>".$eReportCard['Template']['StudentInfo']['ClassNumCh'].$data['StudentNo']."</td>";
					$StudentInfoTable .= "<td class='span_left_1'>".$eReportCard['Template']['StudentInfo']['DateOfPublish'].$data['DateOfIssue']."</td>";
				$StudentInfoTable .= "</tr>";
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='') {
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
		foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID,$StudentID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve SubjectID Array
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID,0 , 0, $ReportID);
			if (sizeof($SubjectArray) > 0)
				foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
				# retrieve marks
				$MarksAry = $this->getMarks($ReportID, $StudentID);
		
				$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
				$sizeofSubjectCol = sizeof($SubjectCol);
		
				# retrieve Marks Array
				$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
				$MarksDisplayAry = $MSTableReturned['HTML'];
				$isAllNAAry = $MSTableReturned['isAllNA'];
		
				# retrieve Subject Teacher's Comment
				$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
				##########################################
				# Start Generate Table
				##########################################
				$DetailsTable = "<table height=\"100%\" width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border '>";
				# ColHeader
				$DetailsTable .= "<tr>";
					$DetailsTable .= "<td class='lite_grey_bg span_left_title'><b>";
					$DetailsTable .= $eReportCard['Template']['SubjectCh'];
					$DetailsTable .= "</b></td>";
					$DetailsTable .= "<td class='border_left lite_grey_bg span_left_title'><b>";
					$DetailsTable .= $eReportCard['Template']['Result'];
					$DetailsTable .= "</b></td>";
				$DetailsTable .= "</tr>";
		
				
				foreach($SubjectCol as $SubjectID=>$_SubjectCol){
					$isFirst = 1;
					$isMain = (count($_SubjectCol) > 1)?1:0;
					foreach($_SubjectCol as $SupSubjectID=>$__SubjectCol){
						if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
						{
							if($isFirst){
							
								$span_left = "span_left_1";
								if($isMain){
									$bold_start = "<b>";
									$bold_end = "</b>";
								}else{
									$bold_start = "";
									$bold_end = "";
								}
								if($previousISMain||$isMain){
									$css_border_top = "border_top";
								}else{
									$css_border_top = "";
								}
							}else{
								$css_border_top = "";
								$bold_start = "";
								$bold_end = "";
								$span_left = "span_left_2";
							}
							$DetailsTable .= "<tr>";
							# Subject
							$DetailsTable .= "<td class='$css_border_top $span_left'>";
							$DetailsTable .= $bold_start;
							$DetailsTable .= $__SubjectCol;
							$DetailsTable .= $bold_end;
							$DetailsTable .= "</td>";
							# Marks
// 							$DetailsTable .= "<td class='border_left border_top' align='center'>";
// 							$DetailsTable .= $bold_start;
// 							$DetailsTable .= "C";
// 							$DetailsTable .= $bold_end;
// 							$DetailsTable .= "</td>";
							$DetailsTable .= $MarksDisplayAry[$SupSubjectID];
						
							$DetailsTable .= "</tr>";
							$isFirst = 0;
							$previousISMain = $isMain;
						}
					}
				}
		
				# MS Table Footer
				$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
				$DetailsTable .= "</table>";
				##########################################
				# End Generate Table
				##########################################
		
				return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID) {
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		global $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID = $ReportSetting['Semester']; //for DGS, No cosolidate report, SemID must be number
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
			
		$x = array();
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
			foreach($SubjectArray as $SubjectID=>$Ary)
			{	
				$subject = "";
				foreach($Ary as $SubSubjectID=>$Subjs)
				{
					$t = "";
					$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
					if($SubSubjectID==0)		# Main Subject
					{
						$SubSubjectID=$SubjectID;
						$Prefix = "";
					}
		
					$css_border_top = ($isFirst)? "border_top" : "";
					$rowheight = (!$Prefix)?" height='30px' ":"";
					$isFirst=0;
					//foreach($SubjectDisplay as $k=>$v)
					//{
					$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
					$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		
					if($SubjectEng=="General Skills") $SubjectEng .= "*";
				 	
					//$v = str_replace("SubjectEng", $SubjectEng, $v);
					//$v = str_replace("SubjectChn", $SubjectChn, $v);
					$LangDisplay = $this->GetFormSubjectDisplayLang($SubjectID,$ClassLevelID,$ReportID);
					$DisplaySubjectGroup = $this->GetDisplaySubjectGroup($SubjectID,$ClassLevelID,$ReportID);
		
					if($DisplaySubjectGroup=='Y' && !$Prefix)
					{
						$colspan=" colspan='1' ";
						$SubjectGroupID = $this->Get_Student_Studying_Subject_Group($SemID,$StudentID,$SubjectID);
						include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
						$scm = new subject_term_class($SubjectGroupID);
						$SubjGroupTitle = $LangDisplay == 'ch'?$scm->ClassTitleB5:$scm->ClassTitleEN;
						$td = "<td align='right' valign='top' class='tabletext'>$SubjGroupTitle</td>";
					}
					else
					{
						$colspan=" colspan='2' ";
						$td = '';
					}
				 	
					$v = $LangDisplay == 'ch'?$SubjectChn:$SubjectEng;
				 	
					
					//}
					$subject[$SubSubjectID] = $v;
					$isFirst = 0;
				}
				$x[$SubjectID] = $subject;
			}
		}
		
		return $x;
		
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		global $eReportCard;
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
		$ReportType 		= $SemID == "F" ? "W" : "T";
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0){
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		$CalculationOrder = $CalSetting["OrderTerm"];
			
		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnID = array();
		$ColumnTitle = array();
		
		if(sizeof($ColoumnTitle) > 0)
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
		
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
					
					if(!empty($CmpSubjectArr)){
						$HasCmpSubject = true;
					}else{
						$HasCmpSubject = false;
					}
				}
				
				# define css
				if(!$isSub){
					if($HasCmpSubject){
						$css_border_top = "border_top";
						$bolder_start =  "<b>";
						$bolder_end = "</b>";
					}elseif($PreHasCmpSubject){
						$css_border_top = "border_top";
					}else{
						$css_border_top = "";
						$bolder_start = "" ;
						$bolder_end = "";
					}
				}else{
					$css_border_top = "";
					$bolder_start = "" ;
					$bolder_end = "";
				}
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
		
				$isAllNA = true;
					
				# Assessment Marks & Display
// 				for($i=0;$i<sizeof($ColumnID);$i++)
// 				{
// 					$thisColumnID = $ColumnID[$i];
// 					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
// 					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
// 					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
						
// 					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
// 					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
		
// 					if ($isSub && $columnSubjectWeightTemp == 0)
// 					{
// 						$thisMarkDisplay = $this->EmptySymbol;
// 					}
// 					else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
// 						$thisMarkDisplay = $this->EmptySymbol;
// 					} else {
// 						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
// 						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
		
// 						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
// 						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
		
// 						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
// 						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
		
// 						# for preview purpose
// 						if(!$StudentID)
// 						{
// 							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
// 							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
// 						}
		
// 						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
		
// 						if ($thisMark != "N.A.")
// 						{
// 							$isAllNA = false;
// 						}
		
// 						# check special case
// 						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
		
// 						if($needStyle)
// 						{
// 							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
// 							{
// 								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
// 							}
// 							else
// 							{
// 								$thisMarkTemp = $thisMark;
// 							}
								
// 							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
// 						}
// 						else
// 						{
// 							$thisMarkDisplay = $thisMark;
// 						}
		
// 						//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
// 						//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
// 						//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark;
// 					}
		
// 					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
// 					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
// 					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
						
// 					if($ShowSubjectFullMark)
// 					{
// 						# check special full mark
// 						$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
// 						$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
// 						$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
		
// 						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
// 						$x[$SubjectID] .= "<td class='tabletext' align='center' width='50%'>(". $FullMark .")</td>";
// 					}
						
// 					$x[$SubjectID] .= "</tr></table>";
// 					$x[$SubjectID] .= "</td>";
// 				}
		
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
		
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
						
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
		
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
		
					# check special case
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
							if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
								else
									$thisMarkTemp = $thisMark;
		
									$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
					$x[$SubjectID] .= "<td class='tabletext' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $bolder_start.$thisMarkDisplay.$bolder_end ."</td>";
// 					if($ShowSubjectFullMark)
// 					{
// 						$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
// 						$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
// 						$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
// 						$x[$SubjectID] .= "<td class=' tabletext' align='center' width='50%'>(". $thisFullMark .")</td>";
// 					}
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$PreHasCmpSubject = $HasCmpSubject;
			}
			return $returnArr;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$Semester = $this->returnSemesters($SemID);
		
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		
		if(strpos($Semester,'3')){
			$FullYearReport = $this->returnReportTemplateBasicInfo('','',$ClassLevelID,'F');
			$FullYearReportID = $ReportSetting['ReportID'];
			$WholeYearAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $FullYearReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		}
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		if ($CalOrder == 2) {
			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName) {
		
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
		
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
			}
		}
		
		
		$first = 1;
		# Overall Result
// 		if($ShowGrandTotal)
// 		{
// 			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
// 			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
// // 			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
				
// 			$first = 0;
// 		}
		
		
		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark
			if(!strpos($Semester,'3')){
				$thisTitleCh = $eReportCard['Template']['AvgMarkCh'];
				$x .=  "<tr>";
					$x .= "<td class='border_top span_left_1'>";
						$x .= $thisTitleCh;
					$x .= "</td>";
					$x .= "<td class='border_top border_left' align='center'>";
						$x .= $GrandAverage;
					$x .= "</td>";
				$x .=  "</tr>";
			}
// 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
		
				$first = 0;
			if(strpos($Semester,'3')){			
				$thisTitleCh = $eReportCard['Template']['OverallAvgMarkCh'];
				$x .=  "<tr>";
					$x .= "<td class='border_top span_left_1'>";
						$x .= $thisTitleCh;
					$x .= "</td>";
					$x .= "<td class='border_top border_left' align='center'>";
						$x .= Get_String_Display($WholeYearAverage);
					$x .= "</td>";
				$x .=  "</tr>";
			}
				
			
// 			# Position in Class
// 			if($ShowOverallPositionClass)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
// 				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
// // 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first);
		
// 				$first = 0;
// 			}
				
// 			# Number of Students in Class
// 			if($ShowNumOfStudentClass)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
// 				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
// // 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
		
// 				$first = 0;
// 			}
				
// 			# Position in Form
// 			if($ShowOverallPositionForm)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
// 				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
// // 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first);
		
// 				$first = 0;
// 			}
				
// 			# Number of Students in Form
// 			if($ShowNumOfStudentForm)
// 			{
// 				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
// 				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
// // 				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
		
// 				$first = 0;
// 			}
				
			##################################################################################
			# CSV related
			##################################################################################
			# build data array
			$ary = array();
			//$csvType = $this->getOtherInfoType();
				
			//modified by Marcus 20/8/2009
			$ary = array();
			if ($StudentID != '') {
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
				$ary = $OtherInfoDataAry[$StudentID];
			}
				
			if($SemID=="F")
			{
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		
				 # calculate sems/assesment col#
				 $ColNum2Ary = array();
				 foreach($ColumnData as $k1=>$d1)
				 {
				 	$TermID = $d1['SemesterNum'];
				 	if($d1['IsDetails']==1)
				 	{
				 		# check sems/assesment col#
				 		$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
				 		$thisReportID = $thisReport['ReportID'];
				 		$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
				 		$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
				 	}
				 	else
				 		$ColNum2Ary[$TermID] = 0;
				 }
			}
		}
		
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType='') {
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		// also cater office.broadlearning to display comment box only
		if ($eRCTemplateSetting['HideCSVInfo'] && !$eRCTemplateSetting['JustShowComment'])
		{
			return "";
		}
		if ($StudentID != '') {
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SemID = $ReportSetting['Semester'];
		}
		###Attendance Start ###
		$attendance = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$attendance .= "<tr>";
				$attendance .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['ShouldAttend'] . ": " . $OtherInfoDataAry[$StudentID][$SemID]['School Days'] ."</td>";
				$attendance .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['AttendancePercent']. ": " . $OtherInfoDataAry[$StudentID][$SemID]['Attendance Percentage']."%"."</td>";
			$attendance .= "</tr>";
			$attendance .= "<tr>";
				$attendance .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['TimesLateCh']  . ": " . $OtherInfoDataAry[$StudentID][$SemID]['Absentee Days']  ."</td>";
				$attendance .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['DaysAbsenteeCh'] . ": " . $OtherInfoDataAry[$StudentID][$SemID]['Times of Late'] ."</td>";
			$attendance .= "</tr>";
		$attendance .= "</table>";
		###Attendance END ###
		
		###Behaviour START ###
		$behaviour = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >";
			$behaviour .= "<tr>";
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Appearance']."		".$OtherInfoDataAry[$StudentID][$SemID]['Appearance']."</td>" ;
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Mood']."		".$OtherInfoDataAry[$StudentID][$SemID]['Mood']."</td>" ;
			$behaviour .= "</tr>";
			$behaviour .= "<tr>";
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Courtesy']."		".$OtherInfoDataAry[$StudentID][$SemID]['Courtesy']."</td>" ;
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Gregarious']."		".$OtherInfoDataAry[$StudentID][$SemID]['Gregarious']."</td>" ;
			$behaviour .= "</tr>";
			$behaviour .= "<tr>";
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Discipline']."		".$OtherInfoDataAry[$StudentID][$SemID]['Discipline']."</td>" ;
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Honesty'] ."		".$OtherInfoDataAry[$StudentID][$SemID]['Honesty']."</td>" ;
			$behaviour .= "</tr>";
			$behaviour .= "<tr>";
				$behaviour .= "<td class='tabletext span_left_1'>".$eReportCard['Template']['Service']."		".$OtherInfoDataAry[$StudentID][$SemID]['Service']."</td>" ;
			$behaviour .= "</tr>";
			$behaviour .= "<tr>";
				$behaviour .= "<td class='border_top tabletext span_left_1'>".$eReportCard['Template']['AppearanceOverall']."		".$OtherInfoDataAry[$StudentID][$SemID]['AppearanceOverall']."</td>" ;
				$behaviour .= "<td class='border_top tabletext span_left_1'>".$eReportCard['Template']['AppearanceOverallYear']."		".$OtherInfoDataAry[$StudentID][$SemID]['AppearanceOverallYear']."</td>" ;
			$behaviour .= "</tr>";
		$behaviour .= "</table>";
		###Behaviour END ###
		
		###Other Information START ###
		$PELesson = '';
		foreach ((array)$OtherInfoDataAry[$StudentID][$SemID]['PELesson'] as $data){
			$PELesson .= $data;
			$PELesson .= "</br>";
		}
		
		$ServiceItem = '';
		foreach((array)$OtherInfoDataAry[$StudentID][$SemID]['ServiceItem'] as $data){
			$ServiceItem .= $data;
			$ServiceItem .= "</br>";
		}
		
// 		$count = 3;
		$Remarks = '';
		foreach((array)$OtherInfoDataAry[$StudentID][$SemID]['Remarks'] as $data){
			$Remarks .= $data;
			$Remarks .= "</br>";
// 			$count--;
		}
// 		for($i=0;$i<$count;$i++){
// 			$Remarks .="</br>";
// 		}
		
// 		$count = 11;
		$Award = '';
		foreach((array)$OtherInfoDataAry[$StudentID][$SemID]['Award'] as $data){
			$Award .= $data;
			$Award .= "</br>";
// 			$count--;
		}
// 		for($i=0;$i<$count;$i++){
// 			$Award .="</br>";
// 		}
		
		$otherinformation = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  >";
			$otherinformation .= "<tr>";
				$otherinformation .= "<td class='tabletext span_left_1' valign ='top'>".$eReportCard['Template']['PELesson']."</td>";
				$otherinformation .= "<td class='tabletext span_left_1'>".$PELesson."</td>";
			$otherinformation .= "</tr>";
			$otherinformation .= "<tr>";
				$otherinformation .= "<td class='tabletext border_top span_left_1' valign ='top'>".$eReportCard['Template']['ServiceItem']."</td>";
				$otherinformation .= "<td class='tabletext border_top span_left_1'>".$ServiceItem."</td>";
			$otherinformation .= "</tr>";
			$otherinformation .= "<tr>";
				$otherinformation .= "<td class='tabletext border_top span_left_1'  valign ='top'>".$eReportCard['Template']['Award']."</td>";
				$otherinformation .= "<td class='tabletext border_top span_left_1'>".$Award."</td>";
			$otherinformation .= "</tr>";
			$otherinformation .= "<tr>";
				$otherinformation .= "<td class='tabletext border_top span_left_1'  valign ='top'>".$eReportCard['Template']['Remarks']."</td>";
				$otherinformation .= "<td class='tabletext border_top span_left_1'>".$Remarks."</td>";
			$otherinformation .= "</tr>";
		$otherinformation .= "</table>";
		###Other Information END ###
		
		$x .= "<table height=\"100%\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  class='report_border' valign='top'>";
		
		##Attendance Start##
		$x .= "<tr class='lite_grey_bg'>";
			$x .= "<td class='span_left_title'><b>". $eReportCard['Template']['Attendance']."</b>"."</td>";
		$x .="</tr>";
		$x .= "<tr>";
			$x .= "<td class='tabletext border_top'>".$attendance."</td>";
		$x .="</tr>";
		##Attendance END##
			
		##Behaviour Start##
		$x .="<tr class='lite_grey_bg'>";
			$x .= "<td class='border_top span_left_title'><b>". $eReportCard['Template']['Behaviour']."</b>"."</td>";
		$x .="</tr>";
		$x .="<tr>";
			$x .= "<td class='tabletext border_top'>".$behaviour."</td>";
		$x .="</tr>";
		##Behaviour END##
			
		##Other Information Start##
		$x .="<tr class='lite_grey_bg'>";
			$x .= "<td class='border_top span_left_title'><b>". $eReportCard['Template']['OtherInformation']."</b></td>";
		$x .="</tr>";
		$x .="<tr>";
			$x .= "<td class='tabletext border_top'>".$otherinformation."</td>";
		$x .="</tr>";
		##Other Information End##
		$x .= "</table>";
		
		
		return $x;
	}
	
	function Get_Class_Teacher_Comment_Table($StudentID,$SubjectID='',$ReportID){
		global $eReportCard, $eRCTemplateSetting;
		$ClassTeacherComment = $this->GET_TEACHER_COMMENT($StudentID,'',$ReportID);
		$x = "";
		$x .= "<table class='report_border' width='100%' cellspacing='0' cellpadding='0'>";
			$x .= "<tr>";
				$x .= "<td class='lite_grey_bg span_left_title' width='100%'><b>".$eReportCard['Template']['Comment']."</b></td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td class='border_top span_left_1' width='100%'>".$ClassTeacherComment[$StudentID]['Comment']."</td>";
			$x .= "</tr>";
		$x .= "</table>";
	
		return $x;
	}
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='') {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		if($ReportID)
		{
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
		}
		if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				$data['Class'] = $lu->ClassName;
		
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['0']['Name'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				$data['1']['Name'] = "歐栢青";
			}
		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='130' valign='bottom'>_____". $IssueDate ."_____</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."  ".$data[$k]['Name']."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}
		
		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
	
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
	
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
	
		$border_top = $isFirst ? "border_top" : "";
	
		$x .= "<tr>";
		$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh, $isFirst);
		if ($CalOrder == 1) {
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
		} else {
			$curColumn = 0;
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				for ($i=0; $i<$NumOfAssessment[$curColumn]-1 ; $i++)
				{
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				}
	
				$thisValue = $ValueArr[$ColumnID];
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisValue."&nbsp;</td>";
				$curColumn++;
			}
		}
	
		if ($ShowRightestColumn)
		{
			$thisValue = $OverallValue;
			/*
				if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
				{
				$thisDisplay = $thisValue;
				}
				else
				{
				$thisDisplay = $this->EmptySymbol;
				}
				*/
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."&nbsp;</td>";
		}
			
// 		if ($AllowSubjectTeacherComment) {
// 			$x .= "<td class='border_left $border_top' align='center'>";
// 			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
// 			$x .= "</td>";
// 		}
		$x .= "</tr>";
	
		return $x;
	}
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
	
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
	
		return $ShowRightestColumn;
	}
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
	
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
		$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
		$x .= "</table>";
		$x .= "</td>";
	
		return $x;
	}
	########### END Template Related
}
?>