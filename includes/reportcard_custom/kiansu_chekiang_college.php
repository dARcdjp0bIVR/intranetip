<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/kiansu_chekiang_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "merit", "remark", "attendance", "assessment");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "--";
		$this->LeftPaddingWidth = '5';
		
		$this->OtherInfoInGrandMS = array("Conduct","Promotion");
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID) {
		global $eReportCard;
		
		//$MiscTable = $this->getMiscTable($ReportID, $StudentID);
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td style='line-height:19px;'>".$this->Get_Empty_Image('19px')."</td></tr>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		//$TableTop .= "<tr><td>&nbsp;</td></tr>";
		$TableTop .= "<tr><td style='line-height:9px;'>".$this->Get_Empty_Image('9px')."</td></tr>";
		
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		//$TableTop .= "<tr><td>&nbsp;</td></tr>";
		$TableTop .= "<tr><td style='line-height:9px;'>".$this->Get_Empty_Image('9px')."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable[0]."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		if(trim($MiscTable[1])!='')
		{
			$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			//$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td style='line-height:9px;'>".$this->Get_Empty_Image('9px')."</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			//$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td style='line-height:9px;'>".$this->Get_Empty_Image('9px')."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable[1]."</td></tr>";
			$TableTop .= "</table>";	
			
			$x .= "<tr><td>";
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
					
		}
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard, $intranet_root;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			//$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			$ReportTitleArr = explode(':_:', $ReportSetting['ReportTitle']);
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='font14px' align='center'>".$eReportCard['Template']['SchoolNameCh']."</td></tr>\n";
						$TitleTable .= "<tr><td nowrap='nowrap' class='font14px' align='center'>".$eReportCard['Template']['SchoolNameEn']."</td></tr>\n";
						if(!empty($ReportTitle))
						{
							$TitleTable .= "<tr><td nowrap='nowrap' class='font14px' align='center'>".$ReportTitleArr[0]."</td></tr>\n";
							if ($ReportTitleArr[1] != '')
								$TitleTable .= "<tr><td nowrap='nowrap' class='font14px' align='center'>".$ReportTitleArr[1]."</td></tr>\n";
						}
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			$data['DateOfIssue'] = date('j-n-Y', strtotime($ReportSetting['Issued']));
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('en', 2);
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				//$data['StudentNo'] = $thisClassNumber;
				
				//if (is_array($SettingStudentInfo))
				//{
				//	if (!in_array("ClassNo", $SettingStudentInfo) && !in_array("StudentNo", $SettingStudentInfo) && ($thisClassNumber != ""))
				//		$data['Class'] .= " (".$thisClassNumber.")";
				//}
				//else
				//{
					if ($thisClassNumber != "")
						$data['Class'] .= " (".$thisClassNumber.")";
				//}
				
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['StudentNo'] = str_replace("#", "", $lu->WebSamsRegNo);
				$data['STRN'] = $lu->STRN;
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
			}
			
			
			$SettingStudentInfo = array('Name', 'Class', 'StudentNo', 'STRN', 'Gender', 'DateOfIssue');
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($SettingStudentInfo[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						if($count%$StudentInfoTableCol==0) {
							# first column
							$StudentInfoTable .= "<tr>";
							$thisWidth = '40%';
						}
						else if(($count+1)%$StudentInfoTableCol==0) {
							# last column
							$thisWidth = '30%';
						}
						else
						{
							$thisWidth = '25%';
						}
						
						//$colspan = $SettingID=="Name" ? " colspan='10' " : "";
						$StudentInfoTable .= "<td class='font9px' $colspan width='".$thisWidth."' valign='top' height='{$LineHeight}'>".$Title." : ";
						$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						} 
						/*
						else 
						{
							if($SettingID=="Name")
							{
								$count=-1;
								$StudentInfoTable .= "</tr>";
							}
						}
						*/
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', $ReportColumnID='', $CheckPositionDisplay=1);
		//$MarksAry = $this->getMarks($ReportID, $StudentID,'',0,1);

		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		# Empty Line
		$DetailsTable .= $this->Get_Grey_Empty_Line($ReportType);
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			/*
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			*/
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isSub?"":"border_top";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='font9px border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='font9px border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		# Empty Line
		$DetailsTable .= $this->Get_Grey_Empty_Line($ReportType);
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$SubjectWidth = ($ReportType == 'T')? '45%' : '45%';
		$x = '';
		
		# Subject 
		$x .= "<tr class='font9px' height='{$LineHeight}'>";
			$x .= "<td rowspan='2' colspan='2' valign='middle' align='left' width='".$SubjectWidth."'>";
				$x .= "<b>&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'].' ('.$eReportCard['Template']['SubjectChn']. ")</b>";
			$x .= "</td>";
			
			# Full Mark
			if ($ShowSubjectFullMark)
			{
				$FullMarkTitleEnArr = explode(' ', $eReportCard['Template']['FullMarkEn']);
				$FullMarkTitleEnDisplay = $FullMarkTitleEnArr[0].'<br />'.$FullMarkTitleEnArr[1];
				$x .= "<td rowspan='2' valign='middle' align='center' width='40'>";
					$x .= $eReportCard['Template']['FullMarkEn'].'<br />'.$eReportCard['Template']['FullMarkCh'];
				$x .= "</td>";
			}
			
			# Weight
			$x .= "<td rowspan='2' valign='middle' align='center' width='40'>";
				$x .= $eReportCard['Template']['WeightEn'].'<br />'.$eReportCard['Template']['WeightCh'];
			$x .= "</td>";
		
			# Get Column Info & Weight
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$tempArr = array();
			if(sizeof($ColumnTitle) > 0)
				foreach ($ColumnTitle as $ColumnID[] => $tempArr[]);
				
				
			# Daily
			if ($ReportType == 'T')
			{
					# 1st Term
					$SemSequence = $this->Get_Semester_Seq_Number($SemID);
					if ($SemSequence==1) {
						$thisTitle = $eReportCard['Template']['1stTermEn'].'<br />'.$eReportCard['Template']['1stTermCh'];
					}
					else {
						$thisTitle = $eReportCard['Template']['2ndTermEn'].'<br />'.$eReportCard['Template']['2ndTermCh'];
					}
					$x .= "<td class='border_left_thick font8px' colspan='3' valign='middle' align='center'>";
						$x .= $thisTitle;
					$x .= "</td>";
				$x .= "</tr>";
				
				$x .= "<tr class='font7px' height='{$LineHeight}'>";
					# Daily
					$thisTitle = $eReportCard['Template']['DailyEn'].'<br />'.$eReportCard['Template']['DailyCh'];
					if ($CalculationMethod==1)
						$thisWeight = $ColumnData[0]["DefaultWeight"];
					else
					{
						$columnWeightConds = " ReportColumnID = '".$ColumnID[0]."' AND (SubjectID = '0' || SubjectID Is Null) " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$thisWeight = $columnSubjectWeightArr[0]['Weight'];
					}
					
					$x .= "<td class='border_left_thick border_top' valign='middle' align='center' width='100'>";
						$x .= $thisTitle.'<br />( '.($thisWeight * 100).'% )';
					$x .= "</td>";
					
					# Exam
					$thisTitle = $eReportCard['Template']['ExamEn'].'<br />'.$eReportCard['Template']['ExamCh'];
					if ($CalculationMethod==1)
						$thisWeight = $ColumnData[1]["DefaultWeight"];
					else
					{
						$columnWeightConds = " ReportColumnID = '".$ColumnID[1]."' AND (SubjectID = '0' || SubjectID Is Null) " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$thisWeight = $columnSubjectWeightArr[0]['Weight'];
					}
					$x .= "<td class='border_left_thick border_top' valign='middle' align='center' width='100'>";
						$x .= $thisTitle.'<br />( '.($thisWeight * 100).'% )';
					$x .= "</td>";
					
					# Total
					$thisTitle = $eReportCard['Template']['TotalEn'].'<br />'.$eReportCard['Template']['TotalCh'];
					$x .= "<td class='border_left_thick border_top' valign='middle' align='center' width='100'>";
						$x .= $thisTitle.'<br />( 100% )';
					$x .= "</td>";
			}
			else
			{
				
					$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
					$ColumnID = array();
					$tempArr = array();
					if(sizeof($ColumnTitle) > 0)
						foreach ($ColumnTitle as $ColumnID[] => $tempArr[]);
				
					# 1st Term
					$thisTitle = $eReportCard['Template']['1stTermEn'].'<br />'.$eReportCard['Template']['1stTermCh'];
					if ($CalculationMethod==1)
						$thisWeight = $ColumnData[0]["DefaultWeight"];
					else
					{
						$columnWeightConds = " ReportColumnID = '".$ColumnID[0]."' AND (SubjectID = '0' || SubjectID Is Null) " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$thisWeight = $columnSubjectWeightArr[0]['Weight'];
					}
					
					$x .= "<td class='border_left_thick font8px' colspan='2' valign='middle' align='center'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$x .= "<tr class='font9px'>";
								$x .= "<td width='50%' align='center'>".$thisTitle."</td>";
								$x .= "<td width='50%' align='right'>( ".($thisWeight * 100)." %)</td>";
							$x .= "</tr>";
						$x .= "</table>";
					$x .= "</td>";
					
					# 2nd Term
					$thisTitle = $eReportCard['Template']['2ndTermEn'].'<br />'.$eReportCard['Template']['2ndTermCh'];
					if ($CalculationMethod==1)
						$thisWeight = $ColumnData[1]["DefaultWeight"];
					else
					{
						$columnWeightConds = " ReportColumnID = '".$ColumnID[1]."' AND (SubjectID = '0' || SubjectID Is Null) " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$thisWeight = $columnSubjectWeightArr[0]['Weight'];
					}
					//$thisWeight = ($ColumnData[1]['DefaultWeight'] * 100).'%';
					$x .= "<td class='border_left_thick font8px' colspan='2' valign='middle' align='center'>";
						$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$x .= "<tr class='font9px'>";
								$x .= "<td width='50%' align='center'>".$thisTitle."</td>";
								$x .= "<td width='50%' align='right'>( ".($thisWeight * 100)." %)</td>";
							$x .= "</tr>";
						$x .= "</table>";
					$x .= "</td>";
					
					# Yearly Total
					$thisTitle = $eReportCard['Template']['YearlyTotalEn'].'<br />'.$eReportCard['Template']['YearlyTotalCh'];
					$x .= "<td class='border_left_thick' rowspan='2' valign='middle' align='center' width='10%'>";
						$x .= $thisTitle."<br />( 100% )";
					$x .= "</td>";
				$x .= "</tr>";
				
				### 1st Term & 2nd Term Daily and Exam Mark
				$x .= "<tr class='font7px'>";
				$numOfColumn = count($ColumnData);
				for($i=0; $i<$numOfColumn; $i++)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
						
					$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
					
					$ColumnTitle = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$tempArr = array();
					if(sizeof($ColumnTitle) > 0)
						foreach ($ColumnTitle as $ColumnID[] => $tempArr[]);
					
					# Daily
					$thisTitle = $eReportCard['Template']['DailyEn'].'<br />'.$eReportCard['Template']['DailyCh'];
					if ($CalculationMethod==1)
						$thisWeight = $thisColumnData[0]["DefaultWeight"];
					else
					{
						$columnWeightConds = " ReportColumnID = '".$ColumnID[0]."' AND (SubjectID = '0' || SubjectID Is Null) " ;
						//$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds, 1);
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
						$thisWeight = $columnSubjectWeightArr[0]['Weight'];
					}
					$x .= "<td class='border_left_thick border_top' valign='middle' align='center' width='100'>";
						$x .= $thisTitle.'<br />( '.($thisWeight * 100).'% )';
					$x .= "</td>";
					
					# Exam
					$thisTitle = $eReportCard['Template']['ExamEn'].'<br />'.$eReportCard['Template']['ExamCh'];
					if ($CalculationMethod==1)
						$thisWeight = $thisColumnData[1]["DefaultWeight"];
					else
					{
						$columnWeightConds = " ReportColumnID = '".$ColumnID[1]."' AND (SubjectID = '0' || SubjectID Is Null) " ;
						//$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds, 1);
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
						$thisWeight = $columnSubjectWeightArr[0]['Weight'];
					}
					$x .= "<td class='border_top' valign='middle' align='center' width='100'>";
						$x .= $thisTitle.'<br />( '.($thisWeight * 100).'% )';
					$x .= "</td>";
				}
				$x .= "</tr>";
			}
		$x .= "</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnID = array();
		$ColumnTitle = array();
		if(sizeof($ColoumnTitle) > 0)
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
		$lastColumnID = $ColumnID[count($ColumnID)-1];
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
				 		
				 		$style_prefix = '<b>';
				 		$style_suffix = '</b>';
				 		
				 		$weightDisplay = '';
				 		$nowrap = '';
			 		}
			 		else
			 		{
				 		$style_prefix = '';
				 		$style_suffix = '';
				 		$nowrap = 'nowrap';
				 		
				 		if ($CalculationMethod==1)
				 			$columnWeightConds = " (ReportColumnID = '0' Or ReportColumnID is Null) AND SubjectID = '$SubSubjectID' " ;
				 		else
				 			$columnWeightConds = " ReportColumnID = '$lastColumnID' AND SubjectID = '$SubSubjectID' " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$weightDisplay = ' ( '.($columnSubjectWeightArr[0]['Weight'] * 100).'% )';
			 		}
			 		
			 		$css_border_top = ($Prefix)? "" : "border_top";
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $style_prefix.$this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN").$style_suffix;
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH").$weightDisplay; 
		 				
		 				$width = '';
		 				if ($ReportType=='W' && $v=='SubjectEng')
		 					$width = 'width="27%"';
		 				else if ($ReportType=='W' && $v=='SubjectChn')
		 					$width = 'width="18%"';
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
		 				$t .= "<td class='font9px {$css_border_top}' height='{$LineHeight}' valign='middle' $width>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='font9px' ".$nowrap.">".$v."</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$ClassLevelID = $ReportSetting['ClassLevelID'];
		}
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$TableHeight = ($FormNumber==3)? 90 : 130;
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template']['Report_'.$SettingID];
			$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='font9px' height='".$TableHeight."' valign='bottom'>_________". $IssueDate ."_________</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='font9px' height='12px' valign='top'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";

		//	$SignatureTable .= '<td class="border_bottom">aaa</td>';
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentStream 	= $ReportSetting['ShowNumOfStudentStream'];
		$ShowOverallPositionStream 	= $ReportSetting['ShowOverallPositionStream'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];		
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$OverallPositionRangeStream 	= $ReportSetting['OverallPositionRangeStream'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, $other_conds='', $debug=0, $includeAdjustedMarks=1, $CheckPositionDisplay=1, $FilterNoRanking=1);
		//$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$StreamPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritStream'], $ReportID, $ClassLevelID, '', '', 'StreamPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
  		$StreamNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['StreamNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'StreamNoOfStudent') : "#";
  		if ($OverallPositionRangeClass != 0 && $ClassPosition > $OverallPositionRangeClass)
  		{
	  		$ClassPosition = $this->EmptySymbol;
  		}
  		if ($OverallPositionRangeForm != 0 && $FormPosition > $OverallPositionRangeForm)
  		{
	  		$FormPosition = $this->EmptySymbol;
  		}
		if ($OverallPositionRangeStream != 0 && $StreamPosition > $OverallPositionRangeStream)
  		{
	  		$StreamPosition = $this->EmptySymbol;
  		}
		//if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				
				if($ReportType=="W")
				{
					global $GlobalRelatedTermReportID;
					if(!isset($GlobalRelatedTermReportID[$ReportID][$ColumnID]))
					{
						$GlobalRelatedTermReportID[$ReportID][$ColumnID] = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID,1,$ColumnID);
					}
					$RelatedReport = $GlobalRelatedTermReportID[$ReportID][$ColumnID];
					$thisReportID = $RelatedReport[0]["ReportID"];
					$thisColumnID = 0;
				}
				else
				{
					$thisReportID = $ReportID;
					$thisColumnID = $ColumnID;
				}

				$columnResult = $this->getReportResultScore($thisReportID, $thisColumnID, $StudentID, '', 0, $includeAdjustedMarks=1, $CheckPositionDisplay=1, $FilterNoRanking=1);
				
				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
				
				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
				$columnStreamPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritStream'], $ReportID, $ClassLevelID, '', '', 'StreamPosition') : "S";
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
				$columnStreamNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['StreamNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'StreamNoOfStudent') : "S";
				
				if ($OverallPositionRangeClass != 0 && $columnClassPos[$ColumnID] > $OverallPositionRangeClass)
		  		{
			  		$columnClassPos[$ColumnID] = $this->EmptySymbol;
		  		}
		  		if ($OverallPositionRangeForm != 0 && $columnFormPos[$ColumnID] > $OverallPositionRangeForm)
		  		{
			  		$columnFormPos[$ColumnID] = $this->EmptySymbol;
		  		}
		  		if ($OverallPositionRangeStream != 0 && $columnStreamPos[$ColumnID] > $OverallPositionRangeStream)
		  		{
			  		$columnStreamPos[$ColumnID] = $this->EmptySymbol;
		  		}
			}
		//}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['WeightedGrandTotalEn'];
			$thisTitleCh = $eReportCard['Template']['WeightedGrandTotalCh'];
			$GrandTotalFullMark = $this->returnGrandTotalFullMark($ReportID, $StudentID, $checkExemption=1, $excludeDisplayGradeSubject=0, $excludeSPFullChecking=1);
			
			$x .= '<tr>';
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
				$x .= '<td class="font9px border_top" align="center">'.$GrandTotalFullMark.'</td>';
				$x .= '<td class="font9px border_top">&nbsp;</td>';
				
				if ($ReportType == 'T')
				{
					$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$GrandTotal.'</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnTotal[$ColumnID].'</td>';
					}
					$x .= '<td class="border_left_thick border_top font9px" align="center">'.$GrandTotal.'</td>';
				}
				
			$x .= '</tr>';
			
			$first = 0;
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			$thisTitleEn = $eReportCard['Template']['WeightedAverageEn'];
			$thisTitleCh = $eReportCard['Template']['WeightedAverageCh'];
			
			$x .= '<tr>';
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
				$x .= '<td class="font9px border_top" align="center">100</td>';
				$x .= '<td class="border_top">&nbsp;</td>';
				
				if ($ReportType == 'T')
				{
					$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$GrandAverage.'</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnAverage[$ColumnID].'</td>';
					}
					$x .= '<td class="border_left_thick border_top font9px" align="center">'.$GrandAverage.'</td>';
				}
				
			$x .= '</tr>';
			$first = 0;
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
			$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
			
			$x .= '<tr>';
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
				$x .= '<td class="font9px border_top" align="center">&nbsp;</td>';
				$x .= '<td class="border_top">&nbsp;</td>';
				
				if ($ReportType == 'T')
				{
					$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$ClassPosition.'</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnClassPos[$ColumnID].'</td>';
					}
					$x .= '<td class="border_left_thick border_top font9px" align="center">'.$ClassPosition.'</td>';
				}
				
			$x .= '</tr>';
			
			$first = 0;
		}
		
		# Number of Students in Class 
		if($ShowNumOfStudentClass)
		{
			$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
			$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
			
			$x .= '<tr>';
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
				$x .= '<td class="font9px border_top" align="center">&nbsp;</td>';
				$x .= '<td class="border_top">&nbsp;</td>';
				
				if ($ReportType == 'T')
				{
					$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$ClassNumOfStudent.'</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnClassNumOfStudent[$ColumnID].'</td>';
					}
					$x .= '<td class="border_left_thick border_top font9px" align="center">'.$ClassNumOfStudent.'</td>';
				}
				
			$x .= '</tr>';
			
			$first = 0;
		}
		
		# Position in Form 
		if($ShowOverallPositionForm)
		{
			$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
			$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
			
			$x .= '<tr>';
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
				$x .= '<td class="font9px border_top" align="center">&nbsp;</td>';
				$x .= '<td class="border_top">&nbsp;</td>';
				
				if ($ReportType == 'T')
				{
					$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$FormPosition.'</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnFormPos[$ColumnID].'</td>';
					}
					$x .= '<td class="border_left_thick border_top font9px" align="center">'.$FormPosition.'</td>';
				}
				
			$x .= '</tr>';
			
			$first = 0;
		}
		
		# Number of Students in Form 
		if($ShowNumOfStudentForm)
		{
			$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
			$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
			
			$x .= '<tr>';
				$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
				$x .= '<td class="font9px border_top" align="center">&nbsp;</td>';
				$x .= '<td class="border_top">&nbsp;</td>';
				
				if ($ReportType == 'T')
				{
					$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$FormNumOfStudent.'</td>';
				}
				else
				{
					foreach ($ColumnTitle as $ColumnID => $ColumnName)
					{
						$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnFormNumOfStudent[$ColumnID].'</td>';
					}
					$x .= '<td class="border_left_thick border_top font9px" align="center">'.$FormNumOfStudent.'</td>';
				}
				
			$x .= '</tr>';
			
			$first = 0;
		}
		
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		### Show Stream Info for F1-F3 only
		//if ($ReportType == 'W' && $FormNumber < 4)
		if ($FormNumber < 4)
		{
			//if($ShowOverallPositionStream)
			{
				$thisTitleEn = $eReportCard['Template']['PositionInStreamEn'];
				$thisTitleCh = $eReportCard['Template']['PositionInStreamCh'];
				
				$x .= '<tr>';
					$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
					$x .= '<td class="font9px border_top" align="center">&nbsp;</td>';
					$x .= '<td class="border_top">&nbsp;</td>';
					
					if ($ReportType == 'T')
					{
						$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$StreamPosition.'</td>';
					}
					else
					{
						foreach ($ColumnTitle as $ColumnID => $ColumnName)
						{
							$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnStreamPos[$ColumnID].'</td>';
						}
						$x .= '<td class="border_left_thick border_top font9px" align="center">'.$StreamPosition.'</td>';
					}
					
				$x .= '</tr>';
				
				$first = 0;
			}
			
			/*
			//if($ShowNumOfStudentStream)
			{
				$thisTitleEn = $eReportCard['Template']['StreamNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['StreamNumOfStudentCh'];
				
				$x .= '<tr>';
					$x .= $this->Generate_Info_Title_td($thisTitleEn, $thisTitleCh);
					$x .= '<td class="font9px border_top" align="center">&nbsp;</td>';
					$x .= '<td class="border_top">&nbsp;</td>';
					
					if ($ReportType == 'T')
					{
						$x .= '<td colspan="3" class="border_left_thick border_top font9px" align="center">'.$StreamNumOfStudent.'</td>';
					}
					else
					{
						foreach ($ColumnTitle as $ColumnID => $ColumnName)
						{
							$x .= '<td class="border_left_thick border_top font9px" align="center" colspan="2">'.$columnStreamNumOfStudent[$ColumnID].'</td>';
						}
						$x .= '<td class="border_left_thick border_top font9px" align="center">'.$StreamNumOfStudent.'</td>';
					}
					
				$x .= '</tr>';
				
				$first = 0;
			}
			*/
		}
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$VerticalWeightAssoArr = $this->Get_Assessment_Subject_Vertical_Weight($ReportID, 0, $ReturnAsso=true);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				$css_border_top = ($isSub)? "" : "border_top";
				//$css_border_top = "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				
				### Full Mark
				$SubjectFullMark = $SubjectFullMarkAry[$SubjectID];
				if ($ShowSubjectFullMark) {
					$x[$SubjectID] .= "<td class='{$css_border_top} font9px' align='center'>".$SubjectFullMark."</td>";
				}
				
				### Weight
				$lastColumnID = $ColumnID[count($ColumnID)-1];
				if ($isSub == false && $ScaleDisplay == 'M')
				{
//					if ($CalculationOrder == 1) 
//						$columnWeightConds = " (ReportColumnID = '0' Or ReportColumnID is Null) AND SubjectID = '$SubjectID' " ;
//					else
//						$columnWeightConds = " ReportColumnID = '$lastColumnID' AND SubjectID = '$SubjectID' " ;
//						
//					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//					$thisSubjectWeightDisplay = $columnSubjectWeightArr[0]['Weight'];
					
					if ($CalculationOrder == 1) {
						$columnWeightConds = " (ReportColumnID = '0' Or ReportColumnID is Null) AND SubjectID = '$SubjectID' " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$thisSubjectWeightDisplay = $columnSubjectWeightArr[0]['Weight'];
					}
					else {
						$thisSubjectWeightDisplay = $VerticalWeightAssoArr[0][$SubjectID]['Weight'];
					}
					$thisSubjectWeightDisplay = ($thisSubjectWeightDisplay=='')? '&nbsp;' : $thisSubjectWeightDisplay;
				}
				else
				{
					$thisSubjectWeightDisplay = "&nbsp;";
				}
				$x[$SubjectID] .= "<td class='{$css_border_top} font9px' align='center'>".$thisSubjectWeightDisplay."</td>";
									
				# Assessment Marks & Display
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					//2012-0209-0955-43132
					//}
					//else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
					//	$thisMarkDisplay = $this->EmptySymbol;
					} else {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
						
					$x[$SubjectID] .= "<td class='border_left_thick {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='font9px' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='font9px' align='center' width='50%'>(". $FullMark .")</td>";
					}
					*/
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				}
								
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
					
					$x[$SubjectID] .= "<td class='border_left_thick {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='font9px' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						$x[$SubjectID] .= "<td class=' font9px' align='center' width='50%'>(". $thisFullMark .")</td>";
					}
					*/
					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			$ReportMarksArr[$ReportID] = $MarksAry;
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$isAllNA = true;
				$css_border_top = $isSub? "" : "border_top";
				
				### Full Mark
				$SubjectFullMark = $SubjectFullMarkAry[$SubjectID];
				$x[$SubjectID] .= "<td class='{$css_border_top} font9px' align='center'>".$SubjectFullMark."</td>";
				
				### Weight
				$lastColumnID = $ColumnData[count($ColumnData)-1]['ReportColumnID']; 
				if ($isSub == false && $ScaleDisplay == 'M')
				{			
//					if ($CalculationOrder == 1)
//						$columnWeightConds = " (ReportColumnID = '0' Or ReportColumnID is Null) AND SubjectID = '$SubjectID' " ;
//					else
//						$columnWeightConds = " ReportColumnID = '$lastColumnID' AND SubjectID = '$SubjectID' " ;
//					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
//					$thisSubjectWeightDisplay = $columnSubjectWeightArr[0]['Weight'];

					if ($CalculationOrder == 1) {
						$columnWeightConds = " (ReportColumnID = '0' Or ReportColumnID is Null) AND SubjectID = '$SubjectID' " ;
						$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$thisSubjectWeightDisplay = $columnSubjectWeightArr[0]['Weight'];
					}
					else {
						$thisSubjectWeightDisplay = $VerticalWeightAssoArr[0][$SubjectID]['Weight'];
					}
					$thisSubjectWeightDisplay = ($thisSubjectWeightDisplay=='')? '&nbsp;' : $thisSubjectWeightDisplay;
				}
				else {
					$thisSubjectWeightDisplay = "&nbsp;";
				}
				$x[$SubjectID] .= "<td class='{$css_border_top} font9px' align='center'>".$thisSubjectWeightDisplay."</td>";
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					//$css_border_top = "border_top";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					//if($isDetails==1)		# Retrieve assesments' marks
					//{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='font9px border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							if (!isset($ReportMarksArr[$thisReportID]))
								$ReportMarksArr[$thisReportID] = $this->getMarks($thisReportID, $StudentID, $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', '', $CheckPositionDisplay=1);
							
							//$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);		
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisColumnID = $ColumnID[$j];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								
								$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
								$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
								
								if ($isSub && $columnSubjectWeightTemp == 0)
								{
									$thisMarkDisplay = $this->EmptySymbol;
								//2012-0209-0955-43132
								//}
								//else if ($isParentSubject && $CalculationOrder == 1 && !$isAllCmpZeroWeight) {
								//	$thisMarkDisplay = $this->EmptySymbol;
								} else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
									
									### CRM 2010-0630-1457: For Grade input subject, display the overall grade instead of assessment grade for the exam column
									$tmpColumnID = ($j==1 && $ScaleDisplay=='G')? 0 : $ColumnID[$j];
									$thisMSGrade = $ReportMarksArr[$thisReportID][$SubjectID][$tmpColumnID]['Grade'];
									$thisMSMark = $ReportMarksArr[$thisReportID][$SubjectID][$tmpColumnID]['Mark'];
									
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
													
									if ($thisMark != "N.A." && $thisMark != "")
									{
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $ReportMarksArr[$thisReportID][$SubjectID][$tmpColumnID]['Grade']);
									if($needStyle)
									{
										if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
										{
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										}
										else
										{
											$thisMarkTemp = $thisMark;
										}
										
										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
									}
									else
									{
										$thisMarkDisplay = $thisMark;
									}
								}
								
								$css_border_left = ($j==0)? 'border_left_thick' : '';
								
								$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top}'>";
								$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
			  					$x[$SubjectID] .= "<td class='font9px' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
			  					
			  					/*
			  					if($ShowSubjectFullMark)
			  					{
				  					$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID]) : $SubjectFullMarkAry[$SubjectID];
									$x[$SubjectID] .= "<td class=' font9px' align='center' width='50%'>(". $FullMark .")</td>";
								}
								*/
								
								$x[$SubjectID] .= "</tr></table>";
								$x[$SubjectID] .= "</td>";
							}
						}
					//}
					/*
					else					# Retrieve Terms Overall marks
					{
						if ($isParentSubject && $CalculationOrder == 1) {
							$thisMarkDisplay = $this->EmptySymbol;
						} else {
							# See if any term reports available
							$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
							
							# if no term reports, the term mark should be entered directly
							
							if (empty($thisReport)) {
								$thisReportID = $ReportID;
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]][Grade] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"] ;
								
								$thisMSGrade = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][$ColumnData[$i]["ReportColumnID"]]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
									
							} else {
								$thisReportID = $thisReport['ReportID'];
								$MarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
								//$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0]["Mark"] : "";
								#$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0]["Grade"] : ""; 
								//$thisGrade = $MarksAry[$SubjectID][0]["Grade"];
								
								$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
								$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							}
	 						
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
							$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
							
							if ($thisMark != "N.A.")
							{
								$isAllNA = false;
							}
						
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
							if($needStyle)
							{
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
							}
							else
							{
								$thisMarkDisplay = $thisMark;
							}
						}
							
						$x[$SubjectID] .= "<td class='border_left {$css_border_top}'>";
						$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	  					$x[$SubjectID] .= "<td class='font9px' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
	  					
	  					if($ShowSubjectFullMark)
	  					{
 							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1 && $UseWeightedMark) ? $SubjectFullMarkAry[$SubjectID]*$thisSubjectWeight : $SubjectFullMarkAry[$SubjectID];
							$x[$SubjectID] .= "<td class=' font9px' align='center' width='50%'>(". $FullMark .")</td>";
						}
						
						$x[$SubjectID] .= "</tr></table>";
						$x[$SubjectID] .= "</td>";
					}
					*/
				}
								
				# Subject Overall
				if($ShowSubjectOverall)
				{
					//$MarksAry = $this->getMarks($ReportID, $StudentID, $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', $ReportColumnID='', $CheckPositionDisplay=1);
					//$MarksAry = $this->getMarks($ReportID, $StudentID,"","",1);
					$MarksAry = $ReportMarksArr[$ReportID];
					
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? $thisMark : $thisMark) : $thisGrade;
					
					if ($thisMark != "N.A." && $thisMark != "")
					{
						$isAllNA = false;
					}
						
					if ($CalculationMethod==2 && $isSub)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					}
					else
					{
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
						}
						else
							$thisMarkDisplay = $thisMark;
					}
						
					$x[$SubjectID] .= "<td class='border_left_thick {$css_border_top}'>";
					$x[$SubjectID] .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
  					$x[$SubjectID] .= "<td class='font9px' align='center' width='". ($ShowSubjectFullMark ? "50%":"100%") ."'>". $thisMarkDisplay ."</td>";
  					
  					/*
  					if($ShowSubjectFullMark)
  					{
	  					# check special full mark
	  					$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
	  					
						$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
						$x[$SubjectID] .= "<td class='font9px' align='center' width='50%'>(". $FullMark .")</td>";
					}
  					*/
  					
					$x[$SubjectID] .= "</tr></table>";
					$x[$SubjectID] .= "</td>";
					
					
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				
				$isFirst = 0;
				
				
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
			
		}	# End Whole Year Report Type
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
	
		if ($eRCTemplateSetting['HideCSVInfo'] == true)
		{
			return "";
		}
			
		
		# Retrieve Basic Information of Report
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID				= $ReportSetting['ClassLevelID'];
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		//modified by marcus 20/8/2009
		//return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID, '', ''); 
		
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
		{
			$latestTerm = $TermID;
			//$TotalAbsent += $OtherInfoDataAry[$StudentID][$TermID]['Days Absent'];
			//$TotalLate += $OtherInfoDataAry[$StudentID][$TermID]['Times Late'];
			//$Remark = array_merge((array)$Remark,(array)$OtherInfoDataAry[$StudentID][$TermID]['Remark']);
		}
		
		
		# retrieve result data
		if ($ReportType=='W')
			$latestTerm = 0;
		$ary = $OtherInfoDataAry[$StudentID][$latestTerm];
		
		$Conduct = ($ary['Conduct'])? $ary['Conduct'] : 0;
		$Absence = ($ary['Days Absent'])? $ary['Days Absent'] : 0;
		//$Absence = ($TotalAbsent)? $TotalAbsent : 0;
		$Lateness = ($ary['Times Late'])? $ary['Times Late'] : 0;
		//$Lateness = ($TotalLate)? $TotalLate : 0;
		$MajorMerit = ($ary['MajorMerit'])? $ary['MajorMerit'] : 0;
		$MinorMerit = ($ary['MinorMerit'])? $ary['MinorMerit'] : 0;
		$MajorDemerit = ($ary['MajorDemerit'])? $ary['MajorDemerit'] : 0;
		$MinorDemerit = ($ary['MinorDemerit'])? $ary['MinorDemerit'] : 0;
		$SelfDiscipline = ($ary['SelfDiscipline'])? $ary['SelfDiscipline'] : $this->EmptySymbol;
		$Manners = ($ary['Manners'])? $ary['Manners'] : $this->EmptySymbol;
		$SenseOfResponsibility = ($ary['SenseOfResponsibility'])? $ary['SenseOfResponsibility'] : $this->EmptySymbol;
		$LearningAttitudes = ($ary['LearningAttitudes'])? $ary['LearningAttitudes'] : $this->EmptySymbol;
		$Remark = ($ary['Remark'])? $ary['Remark'] : $this->EmptySymbol;
		
		//$Remark = ($Remark)? $Remark : $this->EmptySymbol;
		if (!is_array($Remark))
			$Remark = array($Remark);
		
		
		$x = '';
		$x .= "<br>";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class='report_border'>";
			# Conduct, Days Absent, Times Late
			$x .= "<tr>";
				$x .= "<td>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
						$x .= "<tr class='font9px' style='line-height:10px;'>";
							//$x .= "<td width='5'>&nbsp;</td>";
							$x .= "<td width='5'>".$this->Get_Empty_Image('10px')."</td>";
							# Conduct
							$thisTitle = $eReportCard['Template']['ConductEn'].' ('.$eReportCard['Template']['ConductCh'].')';
							$x .= "<td>";
								$x .= $thisTitle.' : '.$Conduct;
							$x .= "</td>";
							# Days Absent
							$thisTitle = $eReportCard['Template']['DaysAbsentEn'].' ('.$eReportCard['Template']['DaysAbsentCh'].')';
							$x .= "<td>";
								$x .= $thisTitle.' : '.$Absence;
							$x .= "</td>";
							# Times Late
							$thisTitle = $eReportCard['Template']['TimesLateEn'].' ('.$eReportCard['Template']['TimesLateCh'].')';
							$x .= "<td>";
								$x .= $thisTitle.' : '.$Lateness;
							$x .= "</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			
			# Major Merit, Minor Merit, Major Demerit, Minor Demerit
			$x .= "<tr>";
				$x .= "<td>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
						$x .= "<tr class='font9px' style='line-height:10px;'>";
							//$x .= "<td width='5' class='border_top'>&nbsp;</td>";
							$x .= "<td width='5' class='border_top'>".$this->Get_Empty_Image('10px')."</td>";
							# Major Merit
							$thisTitle = $eReportCard['Template']['MajorMeritEn'].' ('.$eReportCard['Template']['MajorMeritCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$MajorMerit;
							$x .= "</td>";
							# Minor Merit
							$thisTitle = $eReportCard['Template']['MinorMeritEn'].' ('.$eReportCard['Template']['MinorMeritCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$MinorMerit;
							$x .= "</td>";
							# Major Demerit
							$thisTitle = $eReportCard['Template']['MajorDemeritEn'].' ('.$eReportCard['Template']['MajorDemeritCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$MajorDemerit;
							$x .= "</td>";
							# Minor Demerit
							$thisTitle = $eReportCard['Template']['MinorDemeritEn'].' ('.$eReportCard['Template']['MinorDemeritCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$MinorDemerit;
							$x .= "</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			
			# Self-discipline, Manners, Sense of Responsibility, Learning Attitudes
			$x .= "<tr>";
				$x .= "<td>";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
						$x .= "<tr class='font9px' style='line-height:10px;'>";
							//$x .= "<td width='5' class='border_top'>&nbsp;</td>";
							$x .= "<td width='5' class='border_top'>".$this->Get_Empty_Image('10px')."</td>";
							# Self-discipline
							$thisTitle = $eReportCard['Template']['SelfDisciplineEn'].' ('.$eReportCard['Template']['SelfDisciplineCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$SelfDiscipline;
							$x .= "</td>";
							# Manners
							$thisTitle = $eReportCard['Template']['MannersEn'].' ('.$eReportCard['Template']['MannersCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$Manners;
							$x .= "</td>";
							# Sense of Responsibility
							$thisTitle = $eReportCard['Template']['SenseOfResponsibilityEn'].' ('.$eReportCard['Template']['SenseOfResponsibilityCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$SenseOfResponsibility;
							$x .= "</td>";
							# Learning Attitudes
							$thisTitle = $eReportCard['Template']['LearningAttitudesEn'].' ('.$eReportCard['Template']['LearningAttitudesCh'].')';
							$x .= "<td class='border_top'>";
								$x .= $thisTitle.' : '.$LearningAttitudes;
							$x .= "</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		### Promote Grade
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		/* CRM 2010-0630-1457: Promotion status is changed to csv import now
		$GrandMarkGradingScheme = $this->Get_Grand_Mark_Grading_Scheme($ClassLevelID);
		$GrandAverageGradingSchemeID = $GrandMarkGradingScheme[-1]['SchemeID'];
		$result = $this->getReportResultScore($ReportID, 0, $StudentID,'',0,1);
		$PromotedGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandAverageGradingSchemeID, $result['GrandAverage']);
		*/
		$PromotedGrade = ($ary['Promotion'])? $ary['Promotion'] : '&nbsp;';
		
		### Class Teacher Comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = nl2br($CommentAry[0]);
		//$x .= "<br />";
		$x .= $this->Get_Empty_Image_Div('9px');
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr style='line-height:10px;'>";
				//$x .= "<td width='10'>&nbsp;</td>";
				$x .= "<td width='10'>".$this->Get_Empty_Image('10px')."</td>";
				$x .= "<td class='font9px'>".$eReportCard['Template']['CommentsEn'].' ('.$eReportCard['Template']['CommentsCh'].") :</td>";
				$x .= "<td class='font9px' width='35%' align='center'>".($ReportType=='W'?$PromotedGrade:"&nbsp;")."</td>";
			$x .= "<tr>";
			$x .= "<tr style='line-height:10px;'>";
				//$x .= "<td width='10'>&nbsp;</td>";
				$x .= "<td width='10'>".$this->Get_Empty_Image('10px')."</td>";
				$x .= "<td style='padding-top:4px; padding-left:2px;' class='font9px' colspan='2'>".$ClassTeacherComment."</td>";
			$x .= "<tr>";
		$x .= "</table>";
			
			
		### Remarks
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		//2015-0716-1232-50207
		//$maxOfRemark = ($FormNumber == 4)? 14 : 10;
		//$numOfRow = $maxOfRemark / 2;
		$maxOfRemark = 5;
		$numOfRow = 5;
		
		//$Remark = array(1,2,3,4,5,6,7,8,9,10,11);
		//$x .= "<br />";
		$x .= $this->Get_Empty_Image_Div('9px');
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
			$x .= "<tr style='line-height:10px;'>";
				//$x .= "<td width='10'>&nbsp;</td>";
				$x .= "<td width='10'>".$this->Get_Empty_Image('10px')."</td>";
				$x .= "<td class='font9px'>".$eReportCard['Template']['RemarksEn'].' ('.$eReportCard['Template']['RemarksCh'].") :</td>";
			$x .= "</tr>";
			$x .= "<tr style='line-height:10px;'>";
				//$x .= "<td>&nbsp;</td>";
				$x .= "<td>".$this->Get_Empty_Image('10px')."</td>";
				$x .= "<td class='font9px'>";
					if(sizeof($Remark)<=$maxOfRemark)
					{
						$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
							//2015-0716-1232-50207
//							for ($i=0; $i<$numOfRow; $i++)
//							{
//								$leftRemarkIndex = $i;
//								$rightRemarkIndex = $i + $numOfRow;
//								
//								$leftRemark = $Remark[$leftRemarkIndex];
//								$rightRemark = $Remark[$rightRemarkIndex];
//								
//								$x .= "<tr class='font9px'>";
//									$x .= "<td width='50%'>".$leftRemark."</td>";
//									$x .= "<td width='50%'>".$rightRemark."</td>";
//								$x .= "</tr>";
//							}
							for ($i=0; $i<$numOfRow; $i++) {
								$x .= "<tr class='font9px'>";
									$x .= "<td>".$Remark[$i]."</td>";
								$x .= "</tr>";
							}
						$x .= "</table>";
					}
					else
					{
						$x.= $eReportCard['Template']['SeeAppendixCh'];
						$extraPage = true;
					}
				$x .= "</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		if($extraPage)
		{
			$y .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
				$y .= "<tr style='line-height:10px;'>";
					//$y .= "<td width='10'>&nbsp;</td>";
					$y .= "<td width='10'>".$this->Get_Empty_Image('10px')."</td>";
					$y .= "<td class='font9px'>".$eReportCard['Template']['RemarksEn'].' ('.$eReportCard['Template']['RemarksCh'].") :</td>";
				$y .= "</tr>";
				$y .= "<tr style='line-height:10px;'>";
					//$y .= "<td>&nbsp;</td>";
					$y .= "<td>".$this->Get_Empty_Image('10px')."</td>";
					$y .= "<td class='font9px'>";
						$y .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
							for ($i=0; $i<sizeof($Remark); $i++)
							{					
								$y .= "<tr class='font9px'>";
									$y .= "<td width='100%'>".$Remark[$i]."</td>";
								$y .= "</tr>";
							}
						$y .= "</table>";
					$y .= "</td>";
				$y .= "</tr>";
			$y .= "</table>";
			
		}
		
		$ReturnArr = array();
		$ReturnArr[] = $x;
		$ReturnArr[] = $y;
		
		return $ReturnArr;
	}
	
	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# initialization
		$border_top = "";
		$x = "";
		
		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($TitleEn, $TitleCh);
			
		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			$thisTotalValue = "";
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey] ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) : "#";
				
				# calculation the overall value
				if (is_numeric($thisValue)) {
					if ($thisTotalValue == "")
						$thisTotalValue = $thisValue;
					else if (is_numeric($thisTotalValue))
						$thisTotalValue += $thisValue;
				}
				
				# insert empty cell for assessments
				for($i=0;$i<$ColNum2Ary[$TermID];$i++)
					$x .= "<td class='font9px {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
					
				# display this term value
				$x .= "<td class='font9px {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
			}
			
			# display overall year value
			if($ShowRightestColumn)
			{
				if($thisTotalValue=="")
					$thisTotalValue = ($ValueArr[0][$InfoKey]=="")? $this->EmptySymbol : $ValueArr[0][$InfoKey];
				$x .= "<td class='font9px {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisTotalValue."</td>";
			}
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey] ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) : "#";
			
			# insert empty cell for assessments
			for($i=0;$i<$ColNum2;$i++)
				$x .= "<td class='font9px {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				
			# display this term value
			if($ShowRightestColumn)
				$x .= "<td class='font9px {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
		}
		
		if ($AllowSubjectTeacherComment) {
			$x .= "<td class='border_left $border_top' align='center'>";
			$x .= "<span style='padding-left:4px;padding-right:4px;'>".$this->EmptySymbol."</span>";
			$x .= "</td>";
		}
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		//$border_top = ($hasBorderTop)? "border_top" : "";
		$border_top = "border_top";
		
		$x .= "<td class='font9px {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='font9px'><b>". $TitleEn ."</b></td></tr>";
			$x .= "</table>";
		$x .= "</td>";		
		$x .= "<td class='font9px {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}'class='font9px'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Grey_Empty_Line($ReportType)
	{
		global $PATH_WRT_ROOT;
		
		# Empty Line with grey bg
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$x = '';
		if ($ReportType == 'T')
		{
			$x .= "<tr style='line-height:2px; height: 2px;'>";
				$x .= "<td class='border_top lite_grey_bg' colspan='4' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
				$x .= "<td class='border_top border_left_thick lite_grey_bg' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
				$x .= "<td class='border_top border_left_thick lite_grey_bg' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
				$x .= "<td class='border_top border_left_thick lite_grey_bg' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
			$x .= "</tr>";
		}
		else
		{
			$x .= "<tr style='line-height:2px; height: 2px;'>";
				$x .= "<td class='border_top lite_grey_bg' colspan='4' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
				$x .= "<td class='border_top border_left_thick lite_grey_bg' colspan='2' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
				$x .= "<td class='border_top border_left_thick lite_grey_bg' colspan='2' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
				$x .= "<td class='border_top border_left_thick lite_grey_bg' style='line-height:2px; height: 2px;'><img src='".$emptyImagePath."' height='2'></td>";
			$x .= "</tr>";
		}
		
		return $x;
	}
}
?>