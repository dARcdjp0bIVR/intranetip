<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "merit");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td>".$this->Get_Empty_Image('110')."</td></tr>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>&nbsp;</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>&nbsp;</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='990px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID) {
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$reportTitle =  $reportInfoAry['ReportTitle'];
		$reportTitle = str_replace(":_:", "<br>", $reportTitle);
		
		$x = '';
		$x .= '<table class="font_12pt" style="width:100%; text-align:center;">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$reportTitle.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='') {
		global $PATH_WRT_ROOT, $eReportCard;
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		
		$dataAry = array();
		
		if (is_date_empty($reportInfoAry['Issued'])) {
			$dataAry['DateOfIssue'] = $this->EmptySymbol;
		}
		else {
			$dataAry['DateOfIssue'] = date('d/m/Y', strtotime($reportInfoAry['Issued']));
		}
		
		$defaultVal = 'XXX';
		if ($StudentID) {
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$ClassID = $StudentInfoArr[0]['ClassID'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			$ClassNumber = $StudentInfoArr[0]['ClassNumber'];
			
			$dataAry['EnglishName'] = $lu->EnglishName;
			$dataAry['ChineseName'] = $lu->ChineseName;
			$dataAry['ClassName'] = $ClassName;
			$dataAry['ClassNumber'] = $ClassNumber;
			
			if ($lu->Gender == 'M') {
				$dataAry['Gender'] = $eReportCard['Template']['MaleCh'].' '.$eReportCard['Template']['MaleEn'];
			}
			else if ($lu->Gender == 'F') {
				$dataAry['Gender'] = $eReportCard['Template']['FemaleCh'].' '.$eReportCard['Template']['FemaleEn'];
			}
			else {
				$dataAry['Gender'] = $this->EmptySymbol;
			}
			
			if (is_date_empty($lu->DateOfBirth)) {
				$dataAry['DateOfBirth'] = $this->EmptySymbol;
			}
			else {
				$dataAry['DateOfBirth'] = date('d/m/Y', strtotime($lu->DateOfBirth));
			}
			
			$dataAry['WebSAMSRegNo'] = $lu->WebSamsRegNo;
			
			$grandFieldSubjectId = $this->Get_Grand_Field_SubjectID($this->Get_Grand_Position_Determine_Field());
			$grandMarkStatisticsAry = $this->Get_Mark_Statistics_Info($ReportID, array($grandFieldSubjectId));
			$dataAry['HighestAverageInClass'] = my_round($grandMarkStatisticsAry[$grandFieldSubjectId][$ClassID]['MaxMark'], 2);
		}
		else {
			$dataAry['EnglishName'] = $defaultVal;
			$dataAry['ChineseName'] = $defaultVal;
			$dataAry['ClassName'] = $defaultVal;
			$dataAry['ClassNumber'] = $defaultVal;
			$dataAry['Gender'] = $defaultVal;
			$dataAry['DateOfBirth'] = $defaultVal;
			$dataAry['WebSAMSRegNo'] = $defaultVal;
			$dataAry['HighestAverageInClass'] = $defaultVal;
		}
		
		$x = '';
		$x .= '<table class="font_10pt" style="width:100%;">'."\r\n";
			$x .= '<col width="11%">'."\r\n";
			$x .= '<col width="7%">'."\r\n";
			$x .= '<col width="2%">'."\r\n";
			$x .= '<col width="14%">'."\r\n";
			$x .= '<col width="10%">'."\r\n";
			$x .= '<col width="8%">'."\r\n";
			$x .= '<col width="2%">'."\r\n";
			$x .= '<col width="14%">'."\r\n";
			$x .= '<col width="10%">'."\r\n";
			$x .= '<col width="12%">'."\r\n";
			$x .= '<col width="2%">'."\r\n";
			$x .= '<col width="5%">'."\r\n";
			$x .= '<col width="3%">'."\r\n";
			
			$x .= '<tr>'."\r\n";
				### Student Name
				$x .= '<td>'.$eReportCard['Template']['NameCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['NameEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td>'.$dataAry['ChineseName'].'</td>'."\r\n";
				$x .= '<td colspan="4">('.$dataAry['EnglishName'].')</td>'."\r\n";
				
				### Class Name
				$x .= '<td>'.$eReportCard['Template']['ClassNameCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['ClassNameEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td>'.$dataAry['ClassName'].'</td>'."\r\n";
				$x .= '<td>('.$dataAry['ClassNumber'].')</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				### Sex
				$x .= '<td>'.$eReportCard['Template']['SexCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['SexEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td>'.$dataAry['Gender'].'</td>'."\r\n";
				### Highest Average in Class
				$x .= '<td colspan="6">'.$eReportCard['Template']['HighestAverageInClassCh'].'&nbsp;&nbsp;'.$eReportCard['Template']['HighestAverageInClassEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td colspan="2">'.$dataAry['HighestAverageInClass'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				### DOB
				$x .= '<td>'.$eReportCard['Template']['DOBCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['DOBEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td>'.$dataAry['DateOfBirth'].'</td>'."\r\n";
				
				### Reg. No.
				$x .= '<td>'.$eReportCard['Template']['RegNoCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['RegNoEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td>'.$dataAry['WebSAMSRegNo'].'</td>'."\r\n";
				
				### Date of Issue
				$x .= '<td>'.$eReportCard['Template']['DateOfIssueCh'].'</td>'."\r\n";
				$x .= '<td>'.$eReportCard['Template']['DateOfIssueEn'].'</td>'."\r\n";
				$x .= '<td>:</td>'."\r\n";
				$x .= '<td colspan="2">'.$dataAry['DateOfIssue'].'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	function getMSTable($ReportID, $StudentID='') {
		global $eRCTemplateSetting, $eReportCard;
		
		### Retrieve Display Settings
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$classLevelID = $reportInfoAry['ClassLevelID'];
		$semID = $reportInfoAry['Semester'];
		$reportType = $semID == "F" ? "W" : "T";
		$showOverallPositionClass = $reportInfoAry['ShowOverallPositionClass'];
		$showOverallPositionForm = $reportInfoAry['ShowOverallPositionForm'];
		
		### Get the Report Info to be displayed
		$reportIdAry = array();
		if ($reportType == 'T') {
			$reportIdAry[] = $ReportID;
		}
		else if ($reportType == 'W') {
			$termReportInfoAry = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
			$reportIdAry = Get_Array_By_Key($termReportInfoAry, 'ReportID');
			$reportIdAry[] = $ReportID;
		}
		$numOfReport = count($reportIdAry);
		
		### Get data for each report
		$reportColumnAssoAry = array();
		$reportInfoAssoAry = array();
		$numOfTotalReportColumnCount = 0;
		for ($i=0; $i<$numOfReport; $i++) {
			$_reportId = $reportIdAry[$i];
			
			// Show overall column only for this client
			$reportColumnAssoAry[$_reportId][] = 0;
			$numOfTotalReportColumnCount++;
			
			// Get Report Info
			$reportInfoAssoAry[$_reportId]['basicInfoAry'] = $this->returnReportTemplateBasicInfo($_reportId);
			
			// Get Grading Schemes of all Subjects
			$reportInfoAssoAry[$_reportId]['gradingSchemeAry'] = $this->GET_SUBJECT_FORM_GRADING($classLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=1, $_reportId);
			
			// Get Student Marks
			// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
			$reportInfoAssoAry[$_reportId]['markAry'] = $this->getMarks($_reportId, $StudentID, '', 0, 1, '', '', 0, $CheckPositionDisplay=1);		// Get the overall column only
			
			// Get Student Grand Marks
			$reportInfoAssoAry[$_reportId]['grandMarkAry'] = $this->getReportResultScore($_reportId, 0, $StudentID);
			
			// Get Student Other Info Data
			$reportInfoAssoAry[$_reportId]['otherInfoAry'] = $this->getReportOtherInfoData($ReportID, $StudentID);
		}
		
		
		### Get all Subjects
		$subjectInOrderArr = $this->returnSubjectwOrder($classLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		### Get Subject Full Marks
		$subjectFullMarkAssoArr = $this->returnSubjectFullMark($classLevelID, 1, array(), 0, $ReportID);
		
		
		### Analyze the Marks array to get the colspan of the Subject Teacher Comments cell
		$subjectDisplayInfoAssoAry = array();
		foreach ((array)$subjectInOrderArr as $_parentSubjectId => $_cmpSubjectArr) {
			foreach ((array)$_cmpSubjectArr as $__cmpSubjectID => $__cmpSubjectName) {
				$isAllNA = true;
				
				$__isComponentSubject = ($__cmpSubjectID == 0)? false : true;
				$__subjectId = ($__isComponentSubject)? $__cmpSubjectID : $_parentSubjectId;
				
				$__subjectPrefix = '';
				if ($__isComponentSubject) {
					$__subjectPrefix = $eReportCard['Template']['ComponentSubjectPrefix'];
				}
				
				$__isAllNA = true;
				$__subjectDisplayInfo = array();
				foreach ((array)$reportColumnAssoAry as $___reportId => $___reportColumnIdAry) {
					$___numOfReportColumn = count($___reportColumnIdAry);
					$___markAry = $reportInfoAssoAry[$___reportId]['markAry'];
					$___scaleDisplay = $reportInfoAssoAry[$___reportId]['gradingSchemeAry'][$__subjectId]['ScaleDisplay'];
					
					for ($i=0; $i<$___numOfReportColumn; $i++) {
						$____reportColumnId = $___reportColumnIdAry[$i];
						
						$___msGrade = $___markAry[$__subjectId][$____reportColumnId]['Grade'];
						$___msMark = $___markAry[$__subjectId][$____reportColumnId]['Mark'];
						$___classRanking = $___markAry[$__subjectId][$____reportColumnId]['OrderMeritClass'];
						$___classNumOfStudent = $___markAry[$__subjectId][$____reportColumnId]['ClassNoOfStudent'];
						$___formRanking = $___markAry[$__subjectId][$____reportColumnId]['OrderMeritForm'];
						$___formNumOfStudent = $___markAry[$__subjectId][$____reportColumnId]['FormNoOfStudent'];
						
						$___grade = ($___scaleDisplay=="G" || $___scaleDisplay=="" || $___msGrade!='' )? $___msGrade : "";
						$___mark = ($___scaleDisplay=="M" && $___grade=='') ? $___msMark : "";
						
						# for preview purpose
						if(!$StudentID) {
							$___mark 	= $___scaleDisplay=="M" ? "S" : "";
							$___grade 	= $___scaleDisplay=="G" ? "G" : "";
						}
						
						$___mark = ($___scaleDisplay=="M" && strlen($___mark)) ? $___mark : $___grade;
						
						if ($___mark != '' && $___mark != 'N.A.') {
							$__isAllNA = false;
						}
						
						# check special case
						list($___mark, $___needStyle) = $this->checkSpCase($___reportId, $__subjectId, $___mark, $___msGrade);
						if($___needStyle) {
							$___markDisplay = $this->Get_Score_Display_HTML($___mark, $___reportId, $classLevelID, $__subjectId, $___mark);
						}
						else {
							$___markDisplay = $___mark;
						}
						$___markDisplay = ($___markDisplay == '')? $this->EmptySymbol : $___markDisplay;
						$__subjectDisplayInfo[$___reportId][$____reportColumnId]['markDisplay'] = $___markDisplay;
						
						
						### Rank display handling
						$___showRank = $showOverallPositionForm;
						$___rank = $___formRanking;
						$___rankNumOfStudent = $___formNumOfStudent;
						if (!$___showRank || $___rank == -1 || $___rank == '') {
							$___rankDisplay = $this->EmptySymbol;
						}
						else {
							$___rankDisplay = $___rank.'/'.$___rankNumOfStudent;
						}
						$__subjectDisplayInfo[$___reportId][$____reportColumnId]['rankDisplay'] = $___rankDisplay;
					}
				}
				
				if ($StudentID=='' || !$__isAllNA) {
					$subjectDisplayInfoAssoAry[$__subjectId]['chineseName'] = $__subjectPrefix.$this->GET_SUBJECT_NAME_LANG($__subjectId, 'b5');
					$subjectDisplayInfoAssoAry[$__subjectId]['englishName'] = $__subjectPrefix.$this->GET_SUBJECT_NAME_LANG($__subjectId, 'en');
					$subjectDisplayInfoAssoAry[$__subjectId]['fullMark'] = $subjectFullMarkAssoArr[$__subjectId];
					$subjectDisplayInfoAssoAry[$__subjectId]['markInfoAry'] = $__subjectDisplayInfo;
				}
			}
		}
		
		
		$maxNumOfRow = 24;
		$leftPadding = 10;
		$rankColBorderLeft = ($reportType=='W')? '' : 'border_left';
		
		$x = '';
		$x .= '<table class="border_table font_10pt" cellpadding="0" style="width:100%; text-align:center;">'."\n";
			$x .= '<tr>'."\n";
				if ($reportType == 'T') {
					$subjectChWidth = 25;
					$subjectEnWidth = 30;
					$numOfTotalColumn = $numOfTotalReportColumnCount + 2;	// Full Mark and Rank
					$columnWidth = ($numOfTotalReportColumnCount > 0)? floor((100 - $subjectChWidth - $subjectEnWidth) / $numOfTotalColumn) : 0;
					$rankWidth = $columnWidth;
					
					$x .= '<td style="width:'.$subjectChWidth.'%; text-align:left; padding-left:'.$leftPadding.'px;">'.$eReportCard['Template']['SubjectCh'].'</td>'."\n";
					$x .= '<td style="width:'.$subjectEnWidth.'%; text-align:left;">'.$eReportCard['Template']['SubjectEn'].'</td>'."\n";
					$x .= '<td class="border_left" style="width:'.$columnWidth.'%;">'.$eReportCard['Template']['FullMarkCh'].'<br />'.$eReportCard['Template']['FullMarkEn'].'</td>'."\n";
					$x .= '<td class="border_left" style="width:'.$columnWidth.'%;">'.$eReportCard['Template']['ResultsCh'].'<br />'.$eReportCard['Template']['ResultsEn'].'</td>'."\n";
					$x .= '<td class="'.$rankColBorderLeft.'" style="width:'.$rankWidth.'%;">'.$eReportCard['Template']['RankCh'].'<br />'.$eReportCard['Template']['RankEn'].'</td>'."\n";
				}
				else if ($reportType == 'W') {
					$subjectChWidth = 15;
					$subjectEnWidth = 19;
					$rankWidth = 8;
					$numOfTotalColumn = $numOfTotalReportColumnCount + 2;	// Full Mark and Rank
					$columnWidth = ($numOfTotalReportColumnCount > 0)? floor((100 - $subjectChWidth - $subjectEnWidth - $rankWidth) / $numOfTotalColumn) : 0;
					
					$reportColumnInfoAry = $this->returnReportTemplateColumnData($ReportID);
					$numOfReportColumn = count($reportColumnInfoAry);
					$columnWeightAry = array();
					for ($i=0; $i<$numOfReportColumn; $i++) {
						$_reportColumnId = $reportColumnInfoAry[$i]['ReportColumnID'];
						$_subjectWeightAry = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", '', $_reportColumnId, $convertEmptyToZero=true);
						$_subjectWeightAssoAry = BuildMultiKeyAssoc($_subjectWeightAry, 'SubjectID');
						
						$columnWeightAry[] = $_subjectWeightAssoAry[0]['Weight'] * 100;
					}
					$annualTitleRemarks = '';
					$annualTitleRemarks .= '('.$eReportCard['Template']['FirstTermShortEn'].'x'.$columnWeightAry[0].'%+'.$eReportCard['Template']['SecondTermShortEn'].'x'.$columnWeightAry[1].'%)';
					
					$x .= '<td style="width:'.$subjectChWidth.'%; text-align:left; padding-left:'.$leftPadding.'px;">'.$eReportCard['Template']['SubjectCh'].'</td>'."\n";
					$x .= '<td style="width:'.$subjectEnWidth.'%; text-align:left;">'.$eReportCard['Template']['SubjectEn'].'</td>'."\n";
					$x .= '<td class="border_left" style="width:'.$columnWidth.'%;">'.$eReportCard['Template']['FullMarkCh'].'<br />'.$eReportCard['Template']['FullMarkEn'].'</td>'."\n";
					$x .= '<td class="border_left" style="width:'.$columnWidth.'%;">'.$eReportCard['Template']['FirstTermCh'].'<br /><span class="font_8pt">'.$eReportCard['Template']['FirstTermEn'].'<br />('.$eReportCard['Template']['FirstTermShortEn'].')</span></td>'."\n";
					$x .= '<td class="border_left" style="width:'.$columnWidth.'%;">'.$eReportCard['Template']['SecondTermCh'].'<br /><span class="font_8pt">'.$eReportCard['Template']['SecondTermEn'].'<br />('.$eReportCard['Template']['SecondTermShortEn'].')</span></td>'."\n";
					$x .= '<td class="border_left" style="width:'.$columnWidth.'%;">'.$eReportCard['Template']['AnnualCh'].'<br /><span class="font_8pt">'.$eReportCard['Template']['AnnualEn'].'<br />'.$annualTitleRemarks.'</span></td>'."\n";
					$x .= '<td class="'.$rankColBorderLeft.'" style="width:'.$rankWidth.'%;">'.$eReportCard['Template']['RankCh'].'<br />'.$eReportCard['Template']['RankEn'].'</td>'."\n";
				}
			$x .= '</tr>'."\n";
			
			### Subject Mark Info
			$rowCount = 0;
			foreach ((array)$subjectDisplayInfoAssoAry as $_subjectId => $_subjectDisplayInfoAry) {
				$_subjectNameCh = $_subjectDisplayInfoAry['chineseName'];
				$_subjectNameEn = $_subjectDisplayInfoAry['englishName'];
				$_subjectFullMark = $_subjectDisplayInfoAry['fullMark'];
				
				if ($rowCount >= $maxNumOfRow) {
					break;
				}
				
				$border_top = ($rowCount==0)? 'border_top' : '';
				
				$x .= '<tr>'."\n";
					$x .= '<td class="'.$border_top.'" style="text-align:left; padding-left:'.$leftPadding.';">'.$_subjectNameCh.'</td>'."\n";
					$x .= '<td class="'.$border_top.'" style="text-align:left;">'.$_subjectNameEn.'</td>'."\n";
					$x .= '<td class="border_left '.$border_top.'">'.$_subjectFullMark.'</td>'."\n";
					
					foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
						foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
							$___markDisplay = ($___columnMarkAry['markDisplay']=='')? '&nbsp;' : $___columnMarkAry['markDisplay'];
							$___rankDisplay = ($___columnMarkAry['rankDisplay']=='')? '&nbsp;' : $___columnMarkAry['rankDisplay'];
							
							$x .= '<td class="border_left '.$border_top.'">'.$___markDisplay.'</td>'."\n";
						}
					}
					
					// display the ranking of last column
					$x .= '<td class="'.$rankColBorderLeft.' '.$border_top.'">'.$___rankDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
				
				$rowCount++;
			}
			
			### Star Rows
			$x .= '<tr>'."\n";
				$x .= '<td style="text-align:left; padding-left:'.$leftPadding.';">***********</td>'."\n";
				$x .= '<td style="text-align:left;">*******************</td>'."\n";
				$x .= '<td class="border_left">*******</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						$x .= '<td class="border_left">*******</td>'."\n";
					}
				}
				$x .= '<td class="'.$rankColBorderLeft.'">*******</td>'."\n";
			$x .= '</tr>'."\n";
			$rowCount++;
			
			### Empty Rows
			$numOfEmptyRow = $maxNumOfRow - $rowCount;
			for ($i=0; $i<$numOfEmptyRow; $i++) {
				$x .= '<tr>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td class="border_left">&nbsp;</td>'."\n";
					foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
						foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
							$x .= '<td class="border_left">&nbsp;</td>'."\n";
						}
					}
					$x .= '<td class="'.$rankColBorderLeft.'">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				
				$rowCount++;
			}
			
			### Grand Total
			$x .= '<tr>'."\n";
				$x .= '<td class="border_top" style="text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['GrandTotalCh'].'</td>'."\n";
				$x .= '<td class="border_top" style="text-align:left;">'.strtoupper($eReportCard['Template']['GrandTotalEn']).'</td>'."\n";
				$x .= '<td class="border_top border_left">'.$this->EmptySymbol.'</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___value = $this->Get_Score_Display_HTML($reportInfoAssoAry[$__reportId]['grandMarkAry']['GrandTotal'], $__reportId, $classLevelID, '', '', 'GrandTotal');
						}
						else {
							$___value = 'S';
						}
						$x .= '<td class="border_top border_left">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="border_top '.$rankColBorderLeft.'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Grand Average
			$x .= '<tr>'."\n";
				$x .= '<td style="text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['GrandAverageCh'].'</td>'."\n";
				$x .= '<td style="text-align:left;">'.strtoupper($eReportCard['Template']['GrandAverageEn']).'</td>'."\n";
				$x .= '<td class="border_left">'.$this->EmptySymbol.'</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___value = $this->Get_Score_Display_HTML($reportInfoAssoAry[$__reportId]['grandMarkAry']['GrandAverage'], $__reportId, $classLevelID, '', '', 'GrandAverage');
						}
						else {
							$___value = 'S';
						}
						$x .= '<td class="border_left">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="'.$rankColBorderLeft.'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		
		
		$x .= '<br />'."\n";
		
		$x .= '<table class="border_table font_10pt" cellpadding="0" style="width:100%; text-align:center;">'."\n";
			### Position in class
			$x .= '<tr>'."\n";
				$x .= '<td class="border_top" style="width:'.$subjectChWidth.'%; text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['PositionInClassCh'].'</td>'."\n";
				$x .= '<td class="border_top" style="width:'.$subjectEnWidth.'%; text-align:left;">'.strtoupper($eReportCard['Template']['PositionInClassEn']).'</td>'."\n";
				$x .= '<td class="border_top border_left" style="width:'.$columnWidth.'%;">'.$this->EmptySymbol.'</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___rank = $reportInfoAssoAry[$__reportId]['grandMarkAry']['OrderMeritClass'];
							$___rankDisplay = $this->Get_Score_Display_HTML($reportInfoAssoAry[$__reportId]['grandMarkAry']['OrderMeritClass'], $__reportId, $classLevelID, '', '', 'ClassPosition');
							$___numOfStudent = $this->Get_Score_Display_HTML($reportInfoAssoAry[$__reportId]['grandMarkAry']['ClassNoOfStudent'], $__reportId, $classLevelID, '', '', 'ClassNoOfStudent');
							
							if ($___rank == '' || $___rank == -1) {
								$___value = $this->EmptySymbol;
							}
							else {
								$___value = $___rankDisplay.' / '.$___numOfStudent;
							}
						}
						else {
							$___value = '#';
						}
						$x .= '<td class="border_top border_left" style="width:'.$columnWidth.'%;">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="border_top '.$rankColBorderLeft.'" style="width:'.$rankWidth.'%;">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Position in level
			$x .= '<tr>'."\n";
				$x .= '<td style="text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['PositionInLevelCh'].'</td>'."\n";
				$x .= '<td style="text-align:left;">'.strtoupper($eReportCard['Template']['PositionInLevelEn']).'</td>'."\n";
				$x .= '<td class="border_left">'.$this->EmptySymbol.'</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___rank = $reportInfoAssoAry[$__reportId]['grandMarkAry']['OrderMeritForm'];
							$___rankDisplay = $this->Get_Score_Display_HTML($reportInfoAssoAry[$__reportId]['grandMarkAry']['OrderMeritForm'], $__reportId, $classLevelID, '', '', 'FormPosition');
							$___numOfStudent = $this->Get_Score_Display_HTML($reportInfoAssoAry[$__reportId]['grandMarkAry']['FormNoOfStudent'], $__reportId, $classLevelID, '', '', 'FormNoOfStudent');
							
							if ($___rank == '' || $___rank == -1) {
								$___value = $this->EmptySymbol;
							}
							else {
								$___value = $___rankDisplay.' / '.$___numOfStudent;
							}
						}
						else {
							$___value = '#';
						}
						$x .= '<td class="border_left">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="'.$rankColBorderLeft.'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Days Absent
			$x .= '<tr>'."\n";
				$x .= '<td style="text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['DaysAbsentCh'].'</td>'."\n";
				$x .= '<td style="text-align:left;">'.strtoupper($eReportCard['Template']['DaysAbsentEn']).'</td>'."\n";
				$x .= '<td class="border_left">'.$this->EmptySymbol.'</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___semester = $reportInfoAssoAry[$__reportId]['basicInfoAry']['Semester'];
							if ($___semester == 'F') {
								$___semester = 0;
							}
							$___value = $reportInfoAssoAry[$__reportId]['otherInfoAry'][$StudentID][$___semester]['Days Absent'];
							$___value = ($___value=='' || $___value==0)? $this->EmptySymbol : number_format($___value, 1);
						}
						else {
							$___value = '#';
						}
						$x .= '<td class="border_left">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="'.$rankColBorderLeft.'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Times Late
			$x .= '<tr>'."\n";
				$x .= '<td style="text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['TimesLateCh'].'</td>'."\n";
				$x .= '<td style="text-align:left;">'.strtoupper($eReportCard['Template']['TimesLateEn']).'</td>'."\n";
				$x .= '<td class="border_left">'.$this->EmptySymbol.'</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___semester = $reportInfoAssoAry[$__reportId]['basicInfoAry']['Semester'];
							if ($___semester == 'F') {
								$___semester = 0;
							}
							$___value = $reportInfoAssoAry[$__reportId]['otherInfoAry'][$StudentID][$___semester]['Time Late'];
							$___value = ($___value=='' || $___value==0)? $this->EmptySymbol : $___value;
						}
						else {
							$___value = '#';
						}
						$x .= '<td class="border_left">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="'.$rankColBorderLeft.'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Conduct
			$x .= '<tr>'."\n";
				$x .= '<td style="text-align:left; padding-left:'.$leftPadding.';">'.$eReportCard['Template']['ConductCh'].'</td>'."\n";
				$x .= '<td style="text-align:left;">'.$eReportCard['Template']['ConductEn'].'</td>'."\n";
				$x .= '<td class="border_left">A</td>'."\n";
				foreach ((array)$_subjectDisplayInfoAry['markInfoAry'] as $__reportId => $__reportColumnMarkAry) {
					foreach ((array)$__reportColumnMarkAry as $___reportColumnId => $___columnMarkAry) {
						if ($StudentID) {
							$___semester = $reportInfoAssoAry[$__reportId]['basicInfoAry']['Semester'];
							if ($___semester == 'F') {
								$___semester = 0;
							}
							$___value = $reportInfoAssoAry[$__reportId]['otherInfoAry'][$StudentID][$___semester]['Conduct'];
							$___value = ($___value=='' || $___value==0)? $this->EmptySymbol : $___value;
						}
						else {
							$___value = '#';
						}
						$x .= '<td class="border_left">'.$___value.'</td>'."\n";
					}
				}
				$x .= '<td class="'.$rankColBorderLeft.'">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function genMSTableColHeader($ReportID) {
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		
	}
	
	function getMiscTable($ReportID, $StudentID='') {
		global $eReportCard;
		
		$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportID);
		$semester = $reportInfoAry['Semester'];
		$reportType = ($semester=='F')? 'W' : 'T';
		$classLevelId = $reportInfoAry['ClassLevelID'];
		$formNumber = $this->GET_FORM_NUMBER($classLevelId);
		
		### Get Passing %
		$passPercentage = $this->Get_Template_Form_Passing_Percentage($formNumber);
		
		### Get Demerit Info
		$otherInfoAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		$minorDemerit = $otherInfoAry[$StudentID][$semester]['Minor Demerit'];
		$minorDemerit = ($minorDemerit=='')? 'Nil' : $minorDemerit;
		$majorDemerit = $otherInfoAry[$StudentID][$semester]['Major Demerit'];
		$majorDemerit = ($majorDemerit=='')? 'Nil' : $majorDemerit;
		
		### Get Class Teacher Comment
		$commentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classTeacherComment = nl2br(stripslashes($commentAry[0]));
		
		$commentColspan = ($reportType=='T')? 3 : 2;
		$x = '';
		
		### Legand
		$x .= '<table class="font_10pt" align="right" style="width:30%;">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>Keys: ( ) Failed</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				//$x .= '<td>Passing %: '.$passPercentage.'</td>'."\n";
				$x .= '<td>'.$eReportCard['Template']['PassingPercentageRemarkEn'].'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		### Demerit, Comment & Promotion
		$x .= '<table class="font_10pt" cellpadding="0" cellspacing="0" style="width:100%;">'."\n";
			// Demerit
			$x .= '<tr>'."\n";
				$x .= '<td style="width:50%">&nbsp;</td>'."\n";
				$x .= '<td style="width:23%">'.$eReportCard['Template']['MinorDemeritCh'].' '.$eReportCard['Template']['MinorDemeritEn'].'&nbsp;&nbsp;&nbsp;&nbsp;'.$minorDemerit.'</td>'."\n";
				$x .= '<td style="width:27%">'.$eReportCard['Template']['MajorDemeritCh'].' '.$eReportCard['Template']['MajorDemeritEn'].'&nbsp;&nbsp;&nbsp;&nbsp;'.$majorDemerit.'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr style="height:1px;">'."\n";
				// Comment Title
				$x .= '<td colspan="'.$commentColspan.'" style="vertical-align:top;">'."\n";
					$x .= '<table class="font_10pt" cellpadding="0" cellspacing="0" style="width:100%;">'."\n";
						$x .= '<tr><td>'.$eReportCard['Template']['CommentCh'].' '.$eReportCard['Template']['CommentEn'].'</td></tr>'."\n";
						$x .= '<tr><td style="padding-left:5px;">'.$classTeacherComment.'</td></tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
				
				if ($reportType=='W') {
					$curFormInfoAry = $this->Get_Student_Class_ClassLevel_Info($StudentID);
					$curFormName = $curFormInfoAry[0]['ClassLevelName'];
					$promotionFormInfoAry = $this->Get_Promotion_Form($classLevelId);
					$promotionFormName = $promotionFormInfoAry['YearName'];
					
					$x .= '<td style="vertical-align:top; padding-top:2px;">'."\n";
						$x .= '<div style="float:left; border:1px solid #000000; width:12px; height:12px;">'.$this->Get_Empty_Image('5px').'</div><div style="float:left;">&nbsp;'.$eReportCard['Template']['HasCompletedFormCh'].$curFormName.' '.$eReportCard['Template']['HasCompletedFormEn'].' '.$curFormName.'</div>';
						$x .= '<br style="clear:both;" />'."\n";
						$x .= '<div style="float:left; border:1px solid #000000; width:12px; height:12px;">'.$this->Get_Empty_Image('5px').'</div><div style="float:left;">&nbsp;'.$eReportCard['Template']['PromotedToFormCh'].$promotionFormName.' '.$eReportCard['Template']['PromotedToFormEn'].' '.$promotionFormName.'</div>';
						$x .= '<br style="clear:both;" />'."\n";
						$x .= '<div style="float:left; border:1px solid #000000; width:12px; height:12px;">'.$this->Get_Empty_Image('5px').'</div><div style="float:left;">&nbsp;'.$eReportCard['Template']['ToRepeatInFormCh'].$curFormName.' '.$eReportCard['Template']['ToRepeatInFormEn'].' '.$curFormName.'</div>';
					$x .= '</td>'."\n";
				}
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='') {
		global $eReportCard;
		
 		$classTeacherInfoAry = $this->Get_Student_Class_Teacher_Info($StudentID);
 		$classTeacherChineseNameAry = Get_Array_By_Key($classTeacherInfoAry, 'ChineseName');
 		$classTeacherDisplay = implode(' / ', $classTeacherChineseNameAry);
 		
 		$principalInfoAry = $this->Get_Principal_Info();
 		$principalChineseNameAry = Get_Array_By_Key($principalInfoAry, 'ChineseName');
 		$principalDisplay = implode(' / ', $principalChineseNameAry);
 		
 		$x = '';
 		$x .= '<table class="font_10pt" cellpadding="0" cellspacing="0" style="width:100%; text-align:center;">'."\n";
 			$x .= '<tr>'."\n";
 				$x .= '<td class="border_top" style="width:26%;">'.$eReportCard['Template']['ClassTeacherEn'].' '.$eReportCard['Template']['ClassTeacherCh'].'</td>'."\n";
 				$x .= '<td style="width:11%;">&nbsp;</td>'."\n";
 				$x .= '<td class="border_top" style="width:26%;">'.$eReportCard['Template']['PrincipalEn'].' '.$eReportCard['Template']['PrincipalCh'].'</td>'."\n";
 				$x .= '<td style="width:11%;">&nbsp;</td>'."\n";
 				$x .= '<td class="border_top" style="width:26%;">'.$eReportCard['Template']['ParentEn'].'/'.$eReportCard['Template']['GuardianEn'].' '.$eReportCard['Template']['ParentCh'].' / '.$eReportCard['Template']['GuardianCh'].'</td>'."\n";
 			$x .= '</tr>'."\n";
 			$x .= '<tr>'."\n";
 				$x .= '<td>'.$classTeacherDisplay.'</td>'."\n";
 				$x .= '<td>&nbsp;</td>'."\n";
 				$x .= '<td>'.$principalDisplay.'</td>'."\n";
 				$x .= '<td>&nbsp;</td>'."\n";
 				$x .= '<td>&nbsp;</td>'."\n";
 			$x .= '</tr>'."\n";
 		$x .= '</table>'."\n"; 		
 		$x .= '<table class="font_8pt" style="width:100%;">'."\n";
 			$x .= '<tr>'."\n";
 				$x .= '<td style="width:13%;">NB: Conduct Grade</td>'."\n";
 				$x .= '<td style="width:13%;">A: '.$eReportCard['Template']['ExcellentEn'].'('.$eReportCard['Template']['ExcellentCh'].')</td>'."\n";
 				$x .= '<td style="width:13%;">B: '.$eReportCard['Template']['GoodEn'].'('.$eReportCard['Template']['GoodCh'].')</td>'."\n";
 				$x .= '<td style="width:13%;">C: '.$eReportCard['Template']['AverageEn'].'('.$eReportCard['Template']['AverageCh'].')</td>'."\n";
 				$x .= '<td style="width:23%;">D: '.$eReportCard['Template']['UnsatisfactoryEn'].'('.$eReportCard['Template']['UnsatisfactoryCh'].')</td>'."\n";
 				$x .= '<td style="width:25%; text-align:center;">** '.$eReportCard['Template']['SeeExplanatoryNotesEn'].' **<br />'.$eReportCard['Template']['SeeExplanatoryNotesCh'].'</td>'."\n";
 			$x .= '</tr>'."\n";
 		$x .= '</table>'."\n";
 		return $x;
	}
	########### END Template Related
	
	function Get_Template_Passing_Percentage_Array() {
		$infoAry = array();
		$infoAry[1] = 50;
		$infoAry[2] = 50;
		$infoAry[3] = 50;
		$infoAry[4] = 40;
		$infoAry[5] = 40;
		$infoAry[6] = 40;
		$infoAry[9] = 40;	// for dev
		
		return $infoAry;
	}
	
	function Get_Template_Form_Passing_Percentage($FormNumber) {
		$infoAry = $this->Get_Template_Passing_Percentage_Array();
		return $infoAry[$FormNumber];
	}
}
?>