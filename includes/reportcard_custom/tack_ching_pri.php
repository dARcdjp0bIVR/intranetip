<?php
# Editing by 

####################################################
# Library for Tack Ching Primary School
####################################################

include_once($intranet_root."/lang/reportcard_custom/tack_ching_pri.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","eca","interschool","schoolservice","attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		
		$this->EmptySymbol = '---';
	}
	
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$photoArea = "<div style='border:1px solid black;height:150px;width:115'></div>";
		
		$x = "<tr><td width='80%'>$TitleTable</td><td width='' align='right' rowspan='3'>$photoArea</td></tr>";
		$x .= "<tr><td>&nbsp;</td></tr>";
		$x .= "<tr><td>$StudentInfoTable</td></tr>";
		$x .= "<tr><td colspan='2' height='1'>&nbsp;</td></tr>";
		$x .= "<tr><td colspan='2'>";
		$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='bg'>";
		$x .= "<tr><td class='bold_label' width='67%'>&nbsp;&nbsp;".$eReportCard['Template']['AcademicResults']."</td>";
		$x .= "<td width='3%'>&nbsp;</td>";
		$x .= "<td class='bold_label'>&nbsp;&nbsp;".$eReportCard['Template']['ConductAssessment']."</td></tr>";
		$x .= "<tr><td valign='top' class='bold_label' width='67%'>".$MSTable.$SignatureTable."</td>";
		$x .= "<td width='3%'>&nbsp;</td>";
		$x .= "<td valign='top' class='bold_label' height='100%'>$MiscTable</td></tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "$FooterRow";
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID) {
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			if (function_exists("mb_strtoupper")) {
				$ReportTitle = mb_strtoupper($ReportTitle);
			} else {
				$ReportTitle = strtoupper($ReportTitle);
			}
			
			if(!empty($ReportTitle)) {
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td width='25%'>&nbsp;</td><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
			}
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal		= "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['NameEn'] 	= $lu->EnglishName;
				$data['NameCh'] 	= $lu->ChineseName;
				$data['ClassNo'] 	= $lu->ClassNumber; 
				$data['Class'] 		= $lu->ClassName;
				$data['DOB'] 		= date("d-m-Y", strtotime($lu->DateOfBirth));
				$data['DOI'] 		= date("d-m-Y", strtotime($ReportSetting["Issued"]));
				$data['Gender'] 	= $lu->Gender;
				$data['STRN']       = str_replace("#", "", $lu->WebSamsRegNo);
								
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : $this->EmptySymbol;
			}
			
			$emptyImagePath = $PATH_WRT_ROOT."images/2007a/10x10.gif";
			// Hardcoded student info, won't affect by setting
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$StudentInfoTable .= "<tr><td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['NameEn']." : ".$data['NameEn']."</td>";
			$StudentInfoTable .= "<td width='33%' class='student_info_text' height='{$LineHeight}'>&nbsp;</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['NameCh']." : <span style='font-family: �з���;'>".$data['NameCh']."</span></td>";
			$StudentInfoTable .= "<td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['Class']." : ".$data['Class']."</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['ClassNo']." : ".$data['ClassNo']."</td>";
			$StudentInfoTable .= "<td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['Gender']." : ".$data['Gender']."</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['STRN']." : ".$data['STRN']."</td>";
			$StudentInfoTable .= "<td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['DOB']." : ".$data['DOB']."</td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text' colspan='2'><img src='".$emptyImagePath."' height='5'></td></tr>";
			$StudentInfoTable .= "<tr><td class='student_info_text' height='{$LineHeight}'>&nbsp;</td>";
			$StudentInfoTable .= "<td class='student_info_text' height='{$LineHeight}'>".$eReportCard['Template']['StudentInfo']['DOI']." : ".$data['DOI']."</td></tr>";
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2) = $ColHeaderAry;
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MarksDisplayAry = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		
		$isSchemeAllGrade = $this->returnIsSchemeAllGrade($ClassLevelID);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='3' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
				
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
				{
					if($da['Grade']!="*")	$Droped=0;
				}
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			$DetailsTable .= "<tr>";
			# Subject 
			$DetailsTable .= $SubjectCol[$i];
			# Marks
			$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
			$DetailsTable .= "</tr>";
			$isFirst = 0;
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2);
		
		//$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass = $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm = $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal = $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg = $ReportSetting['ShowGrandAvg'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$isSchemeAllGrade = $this->returnIsSchemeAllGrade($ClassLevelID);
		
		$n = 0;
		$e = 0;	# column# within term/assesment
		
		############## Marks START ##############
		$row1 = "";
		$row2 = "";
		if($ReportType=="T")	# Temrs Report Type
		{
			$e++;
			# Never show detail assessment result for Tack Ching
		} else {				# Whole Year Report Type
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1) {
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++) 
					{
						# convert to big5 display
						# $ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay = $ColumnTitle[$j];
						$row2 .= "<td class='border_left reportcard_text' align='center' height='{$LineHeight}' width=''>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
				} else {
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center' width='".(70/(1+sizeof($ColumnData)))."%'>". $SemName ."</td>";
			}
		}
		############## Marks END ##############
		$x .= "<tr>";
		
		# Subject 
		$PercentageSign = "";
		if ($isSchemeAllGrade)
			$PercentageSign = "<div id='percentage_sign'>%</div>";
		$SubjectTitle = "<div class='col_header'>$PercentageSign<p>".$eReportCard['Template']['SubjectEng']."</p></div>";
		$x .= "<td align='center' valign='middle' class='small_title' width='25%'>". $SubjectTitle . "</td>";
		$n++;
		
		# Full Mark
		if ($ShowSubjectFullMark) {
			$FullMarkTitle = "<div class='col_header'><p>".$eReportCard['SchemesFullMark']."</p></div>";
			//$x .= "<td align='center' valign='middle' class='border_left small_title' height='{$LineHeight}' width='".(70/(1+$e))."%'>". $FullMarkTitle . "</td>";
			$x .= "<td align='center' valign='middle' class='border_left small_title' height='{$LineHeight}' width='25%'>". $FullMarkTitle . "</td>";
		}
		else
		{
			if ($ReportType=="T")
				$x .= "<td align='center' valign='middle' class='small_title' height='{$LineHeight}' width='25%'>&nbsp;</td>";
		}
		
		# Marks
		$x .= $row1;
						
		# Subject Overall 
		if ($ReportType=="T")
		{
			$SubjectOverallTitle = "<div class='col_header'><p>".$this->returnSemesters($SemID)."</p></div>";
			$x .= "<td align='center' valign='middle' width='25%' class='border_left small_title' align='center'>". $SubjectOverallTitle ."</td>";
			$n++;
		}
		else
		{
			if($ShowRightestColumn) {
				$SubjectOverallTitle = "<div class='col_header'><p>".$eReportCard['Template']['SubjectOverall']."</p></div>";
				//$x .= "<td align='center' valign='middle' width='".(70/(1+$e))."%' class='border_left small_title' align='center'>". $SubjectOverallTitle ."</td>";
				$x .= "<td align='center' valign='middle' width='25%' class='border_left small_title' align='center'>". $SubjectOverallTitle ."</td>";
				$n++;
			}
		}
				
		$x .= "</tr>";
		
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n, $e);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		
		$isSchemeAllGrade = $this->returnIsSchemeAllGrade($ClassLevelID);
		
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		
		$SubjectWeightAry = $this->returnReportTemplateSubjectWeightData($ReportID);
		
		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnID = array();
		$ColumnTitle = array();
		if(sizeof($ColoumnTitle) > 0)
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
					
 		$x = array(); 
		$isFirst = 1;
		
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
					$isSub = true;
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
						$Prefix = "";
						$isSub = false;
			 		}
			 		
			 		$css_border_top = ($Prefix)? "" : "border_top";
			 		
					$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
					
					$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle' nowrap>";
					
					$t .= "<span style='width:88%;'>";
					$t .= $Prefix.$SubjectEng;
					$t .= "</span>";
					
					if($isSub && $isSchemeAllGrade) {
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID is NULL AND SubjectID=$SubSubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisDisplayWeight = ($thisSubjectWeight == 0)? "" : $thisSubjectWeight * 100;
						
						
						/*
						$tmpSubjectFullMark = $SubjectFullMarkAry[$SubSubjectID];						
						
						if ($UseWeightedMark) {
							$FullMark = $tmpSubjectFullMark*$thisSubjectWeight;
						} else {
							$FullMark = $tmpSubjectFullMark;
						}
							
						$t .= "<span style='vertical-align:top;width:12%;'>";
						$t .= $FullMark ? $FullMark : "";
						$t .= "</span>";
						*/
						
						$t .= "<span style='vertical-align:top;width:12%;align:right'>";
						$t .= $thisDisplayWeight;
						$t .= "</span>";
					}
					$t .= "</td>";
						
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		# Check the report type to determine display "promote" cell or not
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
 			$ClassLevelID = $ReportSetting['ClassLevelID'];
 			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			$Issued = $ReportSetting['Issued'];
			$SemID 		= $ReportSetting['Semester'];
 			$ReportType = $SemID == "F" ? "W" : "T";
		}
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		$SignatureTable = "";
		//$SignatureTable = "<table id='signature_table' width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
		
		$showPromotionBox = false;
		# Show promotion box in P1-5 senond term report and whole year report
		if( ($FormNumber!=6) && ($ReportType=="W" || ($ReportType=="T" && $SemID>0)) )
			$showPromotionBox = true;
			
		if($showPromotionBox)
		{
			# Promotion cell
			$SignatureTable .= "<td valign='top' width='30%' rowspan='2' class='border_top'>";
			$SignatureTable .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td class='tabletext'>Promoted to /</td></tr>";
			$SignatureTable .= "<tr><td class='tabletext'>Retained in /</td></tr>";
			$SignatureTable .= "<tr><td class='tabletext'>Pass to</td></tr>";
			$SignatureTable .= "<tr><td class='tabletext' align='center'>Primary _____</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}
				
		if ($showPromotionBox)
		{
			$SignatureTable .= "<td colspan='2' class='border_left border_top' style='padding:0px'>";
				$SignatureTable .= "<table width='100%' cellspacing='0' cellpadding='2' border='0'>";
					$SignatureTable .= "<tr>";
		}
		
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$border_left = ($k) ? "border_left" : "";
			$border_top = ($showPromotionBox)? "" : "border_top";
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$SignatureTable .= "<td class='small_title $border_left $border_top' align='center' width='33%'>";
			$SignatureTable .= $Title;
			$SignatureTable .= "</td>";
		}
		$SignatureTable .= "</tr>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$border_left = ($k) ? "border_left" : "";
			$SignatureTable .= "<td class='small_title $border_left border_top' align='center'>";
			$SignatureTable .= "<div style='font-size:65px;'>&nbsp;</div>";
			$SignatureTable .= "</td>";
		}
		
		if ($showPromotionBox)
		{
					$SignatureTable .= "</tr>";
				$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
					
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		$isSchemeAllGrade = $this->returnIsSchemeAllGrade($ClassLevelID);
				
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		
		$GrandTotal = $StudentID ? $this->getDisplayMarkdp("GrandTotal",  $result['GrandTotal']) : "S";
		$AverageMark = $StudentID ? $this->getDisplayMarkdp("GrandAverage",  $result['GrandAverage']) : "S";		
		
		if ($this->GET_FORM_NUMBER($ClassLevelID) > 4)
		{
			$AverageMark = $this->convertAverageMarkToGrade($AverageMark);
			$GrandTotal = $this->convertAverageMarkToGrade($GrandTotal);
		}
			
		if($isSchemeAllGrade)
		{
			$GrandTotal = $this->EmptySymbol;
		}
				
 		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) : "#";
  		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) : "#";
  		
		$FormNum = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) : "#";
		$ClassNum = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) : "#";
		
		if ($CalOrder == 2) {
			foreach ($ColumnTitle as $ColumnID => $ColumnName) {
				
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				$columnTotal[$ColumnID] = $StudentID ? $this->getDisplayMarkdp("GrandAverage",  $columnResult['GrandTotal']) : "S";
				$columnAverage[$ColumnID] = $StudentID ? $this->getDisplayMarkdp("GrandAverage",  $columnResult['GrandAverage']) : "S";
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] : "S";
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] : "S";
				$columnClassNum[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] : "S";
				$columnFormNum[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] : "S";
				
				if ($isSchemeAllGrade)
				{
					$columnClassPos[$ColumnID] = $this->EmptySymbol;
					$columnFormPos[$ColumnID] = $this->EmptySymbol;
				}
			}
		}
		
		
		// Find out the Full mark of grand total by adding all subject's full mark (weight)
		$GrandTotalFullMark = 0;
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 0);
		$tmpOtherCond = "SubjectID IS NOT NULL AND SubjectID != '0' AND ReportColumnID IS NOT NULL AND ReportColumnID != '0'";
		$SubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, $tmpOtherCond);
		$SubjectWeightMap = array();
		
		for($i=0; $i<sizeof($SubjectWeightData); $i++) {
			$SubjectWeightMap[$SubjectWeightData[$i]["SubjectID"]] = $SubjectWeightData[$i]["Weight"];
		}
		foreach($SubjectFullMarkAry as $tmpSubjectID => $tmpFullMark) {
			$GrandTotalFullMark += $tmpFullMark * $SubjectWeightMap[$tmpSubjectID];
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['GrandTotal'];
			$x .= "</td>";
			
			if($ShowSubjectFullMark)
			{
				$x .= "<td align='center' class='tabletext border_left {$border_top}' height='{$LineHeight}'>";
				if ($isSchemeAllGrade)
				{
					$x .= $this->EmptySymbol;
				}
				else
				{
					$x .= $GrandTotalFullMark;
				}
				
				$x .= "</td>";
			}
			else
			{
				if ($ReportType == "T")
				{
					$x .= "<td align='center' class='tabletext {$border_top}' height='{$LineHeight}'>";
					$x .= "&nbsp;";
					$x .= "</td>";
				}
			}
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnTotal[$ColumnID]."&nbsp;</td>";
					}
				}
			}
						
			//if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $GrandTotal ."&nbsp;</td>";
			
			$x .= "</tr>";
		}
		
		# Average Mark 
		if($ShowGrandAvg)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			$x .= "<td class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['AverageMark'];
			$x .= "</td>";
			
			if($ShowSubjectFullMark)
			{
				$x .= "<td align='center' class='tabletext border_left {$border_top}' height='{$LineHeight}'>";
				if ($isSchemeAllGrade)
				{
					$x .= "A";
				}
				else
				{
					$x .= 100;
				}
				
				$x .= "</td>";
			}
			else
			{
				if ($ReportType == "T")
				{
					$x .= "<td align='center' class='tabletext {$border_top}' height='{$LineHeight}'>";
					$x .= "&nbsp;";
					$x .= "</td>";
				}
				
			}
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnAverage[$ColumnID]."&nbsp;</td>";
					}
				}
			}
			
			//if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $AverageMark ."&nbsp;</td>";
			
			$x .= "</tr>";
		}
		
		# Position in Class 
		if($ShowOverallPositionClass)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			
			if ($ReportType == "T") 
			{
				$colspan = 2;	
			}
			else
			{
				if($ShowSubjectFullMark)
				{
					$colspan = 2;	
				}
				else
				{
					$colspan = 1;
				}
			}
			
			$x .= "<td colspan='$colspan' class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['Position'];
			$x .= "</td>";
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$thisPosition = $columnClassPos[$ColumnID];
						if ($OverallPositionRangeClass=="" || $OverallPositionRangeClass==0 || $thisPosition <= $OverallPositionRangeClass)
						{
							$thisDisplay = $thisPosition;
						}
						else
						{
							$thisDisplay = $this->EmptySymbol;
						}
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisDisplay."&nbsp;</td>";
					}
				}
			}
			
			$thisPosition = $ClassPosition;
			if ($OverallPositionRangeClass=="" || $OverallPositionRangeClass==0 || $thisPosition <= $OverallPositionRangeClass)
			{
				$thisDisplay = $thisPosition;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			
			//if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisDisplay ."&nbsp;</td>";
			$x .= "</tr>";
		}
		
		# NO Position in Form 
		if($ShowOverallPositionForm)
		{
			//$border_top = $first ? "border_top" : "";
			$border_top = "border_top";
			$first = 0;
			$x .= "<tr>";
			
			if ($ReportType == "T") 
			{
				$colspan = 2;	
			}
			else
			{
				if($ShowSubjectFullMark)
				{
					$colspan = 2;	
				}
				else
				{
					$colspan = 1;
				}
			}
			
			$x .= "<td colspan='$colspan' class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
			$x .= $eReportCard['Template']['Position'];
			$x .= "</td>";
			
			if ($ReportType == "W") {
				if ($CalOrder == 1) {
					for($i=0;$i<$ColNum2;$i++)
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
				} else {
					foreach ($ColumnTitle as $ColumnID => $ColumnName) {
						$thisPosition = $columnFormPos[$ColumnID];
						if ($OverallPositionRangeForm=="" || $OverallPositionRangeForm==0 || $thisPosition <= $OverallPositionRangeForm)
						{
							$thisDisplay = $thisPosition;
						}
						else
						{
							$thisDisplay = $this->EmptySymbol;
						}
						$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$thisDisplay."&nbsp;</td>";
					}
				}
			}
			
			$thisPosition = $FormPosition;
			if ($OverallPositionRangeForm=="" || $OverallPositionRangeForm==0 || $thisPosition <= $OverallPositionRangeForm)
			{
				$thisDisplay = $thisPosition;
			}
			else
			{
				$thisDisplay = $this->EmptySymbol;
			}
			
			//if ($ShowRightestColumn)
				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $thisDisplay ."&nbsp;</td>";
			$x .= "</tr>";
		}
		
		# No. of students in Class
		$border_top = "border_top";
		$first = 0;
		$x .= "<tr>";
		
		if ($ReportType == "T") 
		{
			$colspan = 2;	
		}
		else
		{
			if($ShowSubjectFullMark)
			{
				$colspan = 2;	
			}
			else
			{
				$colspan = 1;
			}
		}
			
		$x .= "<td colspan='$colspan' class='tabletext bold_text {$border_top}' height='{$LineHeight}'>";
		$x .= $eReportCard['Template']['NoOfStudent'];
		$x .= "&nbsp;</td>";
		
		if ($ReportType == "W") {
			
			if ($CalOrder == 1) {
				for($i=0;$i<$ColNum2;$i++)
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
			} else {
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					//$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnClassNum[$ColumnID]."</td>";
					$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$columnFormNum[$ColumnID]."&nbsp;</td>";
				}
			}
		}
		
		//if ($ShowRightestColumn)
		//{
			//$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $ClassNum ."</td>";
			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>". $FormNum ."&nbsp;</td>";
		//}
		$x .= "</tr>";
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='')
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$isSchemeAllGrade = $this->returnIsSchemeAllGrade($ClassLevelID);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Terms Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);

			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName) 
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				# define css
				$css_border_top = ($isSub == 1) ? "" : "border_top";
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
				
				# Retrieve Subject Weight	
				# for tack ching only
				if($CalculationOrder==2)		
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[0]['ReportColumnID'] ." and SubjectID=$SubjectID");
				else
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID IS NULL or  ReportColumnID=0)  and SubjectID=$SubjectID");
				$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
				
				if ($isSchemeAllGrade) {
					$cellAlign = "center";
				} else {
					//$cellAlign = ($isSub == 1) ? "left" : (($ScaleDisplay == "M") ? "center" : "right");
					$cellAlign = ($isSub == 1) ? (($ScaleDisplay == "M") ? "left" : "right") : (($ScaleDisplay == "M") ? "center" : "right");
				}
				
				# Assessment Marks & Display
				# No Assessment mark for Tack Ching
				# need to check later if Tack Ching need this 
				# check: thisSubjectWeight
				/*
				if($CalculationOrder==2)		
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[0]['ReportColumnID'] ." and SubjectID=$SubjectID");
				else
					$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID IS NULL or  ReportColumnID=0)  and SubjectID=$SubjectID");
				
				*/
				if (!$ShowSubjectOverall) 
				{
					for($i=0;$i<sizeof($ColumnID);$i++) 
					{
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnID[$i] ." and SubjectID=$SubjectID");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
						$thisMark = $this->getDisplayMarkdp("SubjectScore", $thisMark);
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : ""; 
						
						# for preview purpose
						if(!$StudentID) 
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						if($needStyle) {
							if($thisSubjectWeight > 0 and $ScaleDisplay=="M")
								$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						} else {
							$thisMarkDisplay = $thisMark;
						}
						
						if($ShowSubjectFullMark) {
							//$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColumnID[$i]);
		  					//$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
		  					//$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
							$thisFullMark = $SubjectFullMarkAry[$SubjectID];
							$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $thisFullMark*$thisSubjectWeight : $thisFullMark) : $thisFullMark;
							
							$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
							$x[$SubjectID] .= $FullMark;
							$x[$SubjectID] .= "</td>";
						}
						else
						{
							$x[$SubjectID] .= "<td class='tabletext {$css_border_top}' align='$cellAlign'>";
							$x[$SubjectID] .= "&nbsp;";
							$x[$SubjectID] .= "</td>";
						}
						
							
						$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= $thisMarkDisplay."&nbsp;";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				#if($ShowSubjectOverall && $CalculationOrder == 1) 
				if($ShowSubjectOverall) 
				{
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisMark = $this->getDisplayMarkdp("SubjectScore", $thisMark);
					$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if($StudentID == "") {
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					//$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? $thisMark : $thisMark) : $thisGrade;
					
					# added by Yat Woon 20080813 - display the marks with subject weight calculation
					if($StudentID != "") 
					{	
	 					# Calculate with weight
	 					$tmpMark = $thisMark;
	 					if ($ScaleDisplay=="M" && $UseWeightedMark && $isSub) {
	 						$thisMark2 = $tmpMark*$thisSubjectWeight;
	 						$thisMark2 = $this->getDisplayMarkdp("SubjectScore", $thisMark2);
	 					} else {
	 						$thisMark2 = $tmpMark;
	 					}
	 					$thisMark = $thisMark2;
					}
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle) {
						if($thisSubjectWeight > 0 and $ScaleDisplay=="M")
							$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
 						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
					
					if($ShowSubjectFullMark) {
						//$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
	  					//$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  					//$thisFullMark = $SpFullMark ? $SpFullMark : $SubjectFullMarkAry[$SubjectID];
						//$thisFullMark = $SubjectFullMarkAry[$SubjectID];
						
						# Yat Woon 20080813 - display the full marks with subject weight calculation
						$tmpSubjectFullMark = $SubjectFullMarkAry[$SubjectID];
						if ($ScaleDisplay=="M" && $UseWeightedMark && $isSub) {
							$FullMark = $tmpSubjectFullMark*$thisSubjectWeight;
						} else {
							$FullMark = $tmpSubjectFullMark;
						}
							
						$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= $FullMark;
						$x[$SubjectID] .= "</td>";
					}
					else
					{
						$x[$SubjectID] .= "<td class='tabletext {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= "&nbsp;";
						$x[$SubjectID] .= "</td>";
					}
						
					$x[$SubjectID] .= "<td class='border_left tabletext {$css_border_top}' align='$cellAlign'>";
					$x[$SubjectID] .= $thisMarkDisplay."&nbsp;";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				}
				$isFirst = 0;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
				
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				if ($isSchemeAllGrade) {
					$cellAlign = "center";
				} else {
					//$cellAlign = ($isSub == 1) ? "left" : (($ScaleDisplay == "M") ? "center" : "right");
					$cellAlign = ($isSub == 1) ? (($ScaleDisplay == "M") ? "left" : "right") : (($ScaleDisplay == "M") ? "center" : "right");
				}
				$haveShowSubjectFullMark = 0;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$css_border_top = ($isFirst == 1 or $isSub == 0)? "border_top" : "";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='tabletext border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);
								 
							for($j=0;$j<sizeof($ColumnID);$j++)
							{
								$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID=".$ColumnID[$j] ." and SubjectID=$SubjectID");
								$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
								
								$thisMark = $ScaleDisplay=="M" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Mark] : "";
								$thisMark = $this->getDisplayMarkdp("SubjectScore", $thisMark);
								$thisGrade = $ScaleDisplay=="G" ? $thisMarksAry[$SubjectID][$ColumnID[$j]][Grade] : ""; 
								
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
								}
								$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
								if($needStyle)
								{
									if($thisSubjectWeight > 0 and $ScaleDisplay=="M")
										$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
									else
										$thisMarkTemp = $thisMark;
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
									$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								} else {
									$thisMarkDisplay = $thisMark;
								}
								
								if($haveShowSubjectFullMark == 0 && $ShowSubjectFullMark)
			  					{
									//$existNewFullMark = $this->returnStudentSubjectSPFullMark($thisReportID, $SubjectID, $StudentID, $ColumnID[$j]);
				  					//$tmpSubjectFullMark = (sizeof($existNewFullMark) > 0) ? $existNewFullMark[0] : $SubjectFullMarkAry[$SubjectID];
									$tmpSubjectFullMark = $SubjectFullMarkAry[$SubjectID];
									$FullMark = ($ScaleDisplay=="M" && $CalculationOrder == 1) ? ($UseWeightedMark ? $tmpSubjectFullMark*$thisSubjectWeight : $tmpSubjectFullMark) : $tmpSubjectFullMark;
									$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
									$x[$SubjectID] .= $FullMark;
									$x[$SubjectID] .= "</td>";
									$haveShowSubjectFullMark = 1;
								}
								
								$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
								$x[$SubjectID] .= $thisMarkDisplay."&nbsp;";
								$x[$SubjectID] .= "</td>";
							}
						}
					}
					else					# Retrieve Terms Overall marks
					{
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
						
						# if no term reports, the term mark should be entered directly
						if (empty($thisReport)) {
							$thisColumnID = $ColumnData[$i]["ReportColumnID"];
							$thisReportID = $ReportID;
						} else {
							$thisColumnID = 0;
							$thisReportID = $thisReport['ReportID'];
						}
						
						//$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
						if($CalculationOrder==2)		
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID'] ." and SubjectID=$SubjectID");
						else
							$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "(ReportColumnID IS NULL or  ReportColumnID=0)  and SubjectID=$SubjectID");
					
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						
						$MarksAry = $this->getMarks($thisReportID, $StudentID);
						$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$thisColumnID][Mark] : "";
						
						if ($ScaleDisplay=="M" && $UseWeightedMark) 
								$thisMark = $thisMark*$thisSubjectWeight;
							
						
						$thisMark = $this->getDisplayMarkdp("SubjectScore", $thisMark);
						$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$thisColumnID][Grade] : ""; 
 						
						
						# for preview purpose
						if(!$StudentID) {
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$thisColumnID]['Grade']);
						if($needStyle)
						{
							if($thisSubjectWeight > 0 and $ScaleDisplay=="M")
								$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
							//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
 							//$thisMark = ($ScaleDisplay=="M" && $StudentID) ? my_round($thisMark*$thisSubjectWeight,2) : $thisMark;
							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
						} else {
							$thisMarkDisplay = $thisMark;
						}
						
						if($haveShowSubjectFullMark == 0 && $ShowSubjectFullMark)
	  					{
							$tmpSubjectFullMark = $SubjectFullMarkAry[$SubjectID];
							if ($ScaleDisplay=="M" && $UseWeightedMark && $isSub) {
								$FullMark = $tmpSubjectFullMark*$thisSubjectWeight;
							} else {
								$FullMark = $tmpSubjectFullMark;
							}
 							
							$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
							$x[$SubjectID] .= $FullMark;
							$x[$SubjectID] .= "</td>";
							$haveShowSubjectFullMark = 1;
						}
						
						$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
						$x[$SubjectID] .= $thisMarkDisplay."&nbsp;";
						$x[$SubjectID] .= "</td>";
					}
				}
				
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				if($ShowSubjectOverall && $CalculationOrder == 1)
				{
					$MarksAry = $this->getMarks($ReportID, $StudentID);		
					
					$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
					$thisMark = $this->getDisplayMarkdp("SubjectScore", $thisMark);
					$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : ""; 
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						if($thisSubjectWeight > 0 and $ScaleDisplay=="M")
							$thisMarkTemp = $UseWeightedMark ? $thisMark/$thisSubjectWeight : $thisMark;
						else
							$thisMarkTemp = $thisMark;
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp);
						//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
						$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
					}
					else
						$thisMarkDisplay = $thisMark;
					
					$x[$SubjectID] .= "<td class='tabletext border_left {$css_border_top}' align='$cellAlign'>";
					$x[$SubjectID] .= $thisMarkDisplay."&nbsp;";
					$x[$SubjectID] .= "</td>";
				} else if ($ShowRightestColumn) {
					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}' align='$cellAlign'>".$this->EmptySymbol."</td>";
				}	
				$isFirst = 0;
			}
		}	# End Whole Year Report Type
		return $x;
	}
	
	# Get the data store in CSV file, specifiy by Type, Term and Class
	# All Student List, even if no data for the student
	function getOtherInfoDataByStudent($UploadType, $Term, $ClassName='') 
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		# Verify if $UploadType is valid
		$configFilesType = $this->getOtherInfoType();
		if (!in_array($UploadType, $configFilesType)) {
			return false;
		}
		
		$config = $this->getOtherInfoConfig($UploadType);
		
		# For Preview
		if($ClassName=="")
		{
			for($i=1;$i<sizeof($config);$i++)
			{
				$returnArr[0][$i] = "&nbsp;";
				$returnArr[0][$config[$i][EnglishTitle]] = "&nbsp;";
			}
			return $returnArr;
		}
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$ClassID = $lclass->getClassID($ClassName);
				
		$StudentList = array();
		$StudentList = $this->GET_STUDENT_BY_CLASS($ClassID);
		
		$returnArr = array();
		
		
		# built the result array
		foreach($StudentList as $k=>$studentinfo)
		{
			list($tempUserID, $tempWebSAMSRegNo, $tempClassNo, $tempStudentName) = $studentinfo;
			for($i=1;$i<sizeof($config);$i++)
			{
				$returnArr[$tempWebSAMSRegNo][$i] = "&nbsp;";
				$returnArr[$tempWebSAMSRegNo][$config[$i][EnglishTitle]] = "&nbsp;";
			}
		}	
		
		$dataFilesPath = $this->dataFilesPath.$UploadType."/";
		$fileName = $dataFilesPath.($this->schoolYear)."_".$Term."_".$ClassName.".csv";
		if (file_exists($fileName)) 
		{
			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			$limport = new libimporttext();
			$data = $limport->GET_IMPORT_TXT($fileName);
			
			# remove the header
			$header = array_shift($data);
			
			# remove empty item (line)
			$data = array_filter($data);
			
			# Loop and build the associate array
			for($i=0; $i<sizeof($data); $i++) 
			{
				// The first column is WebSAMSRegNo
				for($j=1; $j<sizeof($data[$i]); $j++) 
				{
					$returnArr[$data[$i][0]][$j] = $data[$i][$j];
					// if same REGNO have more than one entry, use an array to store all entries, if not, just store the one entry directly
					if ($returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] != "&nbsp;") 
					{
						if (!is_array($returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]])) {
							$tmpValue = $returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]];
							$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] = array();
							$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]][] = $tmpValue;
						}
						$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]][] = $data[$i][$j];
					} else {
						$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] = $data[$i][$j];
					}
				}
			}
		}
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
		$latestTerm++;
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $lu->ClassName;
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# build data array
		$ary = array();
		$csvType = $this->getOtherInfoType();
		foreach($csvType as $k=>$Type)
		{
			if ($BasicInfo['Semester'] == "F") {
				$csvData = $this->getOtherInfoDataByStudent($Type, 0, $ClassName);
				if(empty($csvData)) {
					$csvData = $this->getOtherInfoDataByStudent($Type, $latestTerm, $ClassName);
				}
			} else {
				$csvData = $this->getOtherInfoDataByStudent($Type, $latestTerm, $ClassName);
			}
			//debug_r($csvData);
			if(!empty($csvData)) 
			{
				foreach($csvData as $RegNo=>$data)
				{
					if($RegNo == $WebSamsRegNo)
					{
	 					foreach($data as $key=>$val)
		 					$ary[$Type][$key] = $val;
					}
				}
			}
		}
		
		######### conduct assessment table #########
		
		# summary
		$summary = "";
		if (isset($ary["summary"]) && sizeof($ary["summary"]) > 0) {
			$summary .= "<table border=0 style='border-width:0 1px 1px 0;border-style:solid;border-color:#000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
			foreach($ary["summary"] as $key => $value) {
				if (!is_numeric($key)) {
					if (strlen($value) == 2) {
						$value = $value[0]."<sup>".$value[1]."</sup>";
					}
					$summary .= "<tr>";
					$summary .= "<td width='80%' class='tabletext bold_text border_left border_top'>$key</td>";
					$summary .= "<td align='center' class='tabletext border_left border_top'>";
					$summary .= "<table border=0  cellpadding='0' cellspacing='0' align='center'>";
					$summary .= "<tr><td class='tabletext' width='15'>&nbsp;$value</td></tr>";
					$summary .= "</table>";
					$summary .= "</td>";
					$summary .= "</tr>";
				}
			}
			$summary .= "</table>";
		} 
		
		# attendance
		$attendance = "";
		if (isset($ary["attendance"]) && sizeof($ary["attendance"]) > 0) {
			$attendance .= "<table style='border-width:0 1px 1px 0;border-style:solid;border-color:#000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
			foreach($ary["attendance"] as $key => $value) {
				if (!is_numeric($key)) {
					$attendance .= "<tr>";
					$attendance .= "<td width='80%' class='tabletext bold_text border_left border_top'>$key</td>";
					$attendance .= "<td align='center' class='tabletext border_left border_top'>$value</td>";
					$attendance .= "</tr>";
				}
			}
			$attendance .= "</table>";
		}
		
		/* Updated on 08 Dec 2008 by Ivan - do not display merit
		# merits & demerits
		# updated on 08 Dec 2008 by Ivan - changed the display style to point form
		if (isset($ary["merit"]) && sizeof($ary["merit"]) > 0) {
			if (is_array($ary["merit"]["Record/Award"])) {
				for($i=0; $i<sizeof($ary["merit"]["Record/Award"]); $i++) {
					if($ary["merit"]["Record/Award"][$i] == "&nbsp;") continue;
					$merits_items .= "<li>".$ary["merit"]["Record/Award"][$i]."&nbsp;</li>";
				}
			} else {
				if($ary["merit"]["Record/Award"] != "&nbsp;") 
					$merits_items .= "<li>".$ary["merit"]["Record/Award"]."&nbsp;</li>";
			}
		}
		$merits = "<tr>";
		$merits .= "<td align='center' class='tabletext bold_text border_left border_top'>Merits & Demerits</td>";
		$merits .= "</tr>";
		$merits .= "<tr>";
		$merits .= "<td height='40' valign='top' align='left' style='padding-left:7px;' class='tabletext border_left border_top'>".$merits_items."&nbsp;</td>";
		$merits .= "</tr>";
		*/
		
		/*
		$merits_items = "";
		if (isset($ary["merit"]) && sizeof($ary["merit"]) > 0) {
			if (isset($ary["merit"]["Merits"]) && is_numeric($ary["merit"]["Merits"])) {
				$merits_items .= $ary["merit"]["Merits"]." merit(s)<br />";
			}
			if (isset($ary["merit"]["Demerits"]) && is_numeric($ary["merit"]["Demerits"])) {
				$merits_items .= $ary["merit"]["Demerits"]." demerit(s)<br />";
			}
		}
		if ($merits_items == "") $merits_items = $this->EmptySymbol;
		$merits = "<tr>";
		$merits .= "<td align='center' class='tabletext bold_text border_left border_top'>Merits & Demerits</td>";
		$merits .= "</tr>";
		$merits .= "<tr>";
		$merits .= "<td height='40' valign='top' align='left' style='padding-left:7px;' class='tabletext border_left border_top'>$merits_items</td>";
		$merits .= "</tr>";
		*/
		# activities & services
		$interschool_items = "";
		
		# interschool competition awards
		if (isset($ary["interschool"]) && sizeof($ary["interschool"]) > 0) {
			if (is_array($ary["interschool"]["Record/Award"])) {
				for($i=0; $i<sizeof($ary["interschool"]["Record/Award"]); $i++) {
					if($ary["interschool"]["Record/Award"][$i] == "&nbsp;") continue;
					$interschool_items .= "<li>".$ary["interschool"]["Record/Award"][$i]."</li>";
				}
			} else {
				if($ary["interschool"]["Record/Award"] != "&nbsp;") 
					$interschool_items .= "<li>".$ary["interschool"]["Record/Award"]."</li>";
			}
		}
		if ($interschool_items == "") $interschool_items = $this->EmptySymbol;
		$merits = "<tr>";
		$merits .= "<td align='center' class='tabletext bold_text border_left border_top'>Merits & Demerits</td>";
		$merits .= "</tr>";
		$merits .= "<tr>";
		$merits .= "<td height='40' valign='top' align='left' style='padding-left:7px;' class='tabletext border_left border_top'>$interschool_items</td>";
		$merits .= "</tr>";
		
		
		# school services
		$activities_items = "";
		if (isset($ary["schoolservice"]) && sizeof($ary["schoolservice"]) > 0) {
			if (is_array($ary["schoolservice"]["Service"])) {
				for($i=0; $i<sizeof($ary["schoolservice"]["Service"]); $i++) {
					if($ary["schoolservice"]["Service"][$i] == "&nbsp;") continue;
					$activities_items .= "<li>".$ary["schoolservice"]["Service"][$i]."</li>";
				}
			} else {
				if($ary["schoolservice"]["Service"] != "&nbsp;") 
					$activities_items .= "<li>".$ary["schoolservice"]["Service"]."</li>";
			}
		}
		
		# extra curricular activities
		if (isset($ary["eca"]) && sizeof($ary["eca"]) > 0) {
			if (is_array($ary["eca"]["ECA"])) {
				for($i=0; $i<sizeof($ary["eca"]["ECA"]); $i++) {
					if($ary["eca"]["ECA"][$i] == "&nbsp;") continue;
					$activities_items .= "<li>".$ary["eca"]["ECA"][$i]."</li>";
				}
			} else {
				if($ary["eca"]["ECA"] != "&nbsp;") 
					$activities_items .= "<li>".$ary["eca"]["ECA"]."</li>";
			}
		}
		
		if ($activities_items == "") {
			$activities_items = $this->EmptySymbol;
		} else {
			$activities_items = "<ul style='list-style-type:disc;'>$activities_items</ul>";
		}
		
		$activities = "<tr>";
		$activities .= "<td align='center' class='tabletext bold_text border_left border_top'>Activities & Services</td>";
		$activities .= "</tr>";
		$activities .= "<tr>";
		$activities .= "<td align='left' valign='top' class='tabletext border_left border_top' style='height:100%;padding-left:7px;'>$activities_items</td>";
		$activities .= "</tr>";
		
		
		$merits_and_activities = "<table height='100%' style='border-width:0 1px 1px 0;border-style:solid;border-color:#000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
		$merits_and_activities .= $merits.$activities;
		$merits_and_activities .= "</table>";
		
		$x = "<table width='100%' height='100%' cellpadding='0' cellspacing='0' align='center' border='0'>";
		$x .= "<tr>";
		$x .= "<td height='39%' valign='top'>".$summary."<br/>".$attendance."<br/></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td height='63%' valign='top'>". $merits_and_activities ." </td>";
		$x .= "</tr>";		
		$x .= "</table>";
		
		#$x = $summary."<br/>".$attendance."<br/><br/><br/>".$merits_and_activities;
		
		return $x;
	}
	
	function getFooter($ReportID) {
		global $eReportCard;
		
		$markTable = "<table style='border-width: 0 1px 1px 0;border-style: solid;border-color: #000;' width='100%' cellpadding='2' cellspacing='0' align='center'>";
		$markTable .= "<tr>";
		$markTable .= 	"<td width='15%' align='center' class='tabletext border_left border_top'>Marks</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>90 - 100</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>75 - 89</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>60 - 74</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>30 - 59</td>
						<td width='17%' align='center' class='tabletext border_left border_top'>Below 30</td>
						";
		$markTable .= "</tr>"; 
		$markTable .= "<tr>";
		$markTable .= 	"<td align='center' class='tabletext border_left border_top'>Grade</td>
						<td align='center' class='tabletext border_left border_top'>A</td>
						<td align='center' class='tabletext border_left border_top'>B</td>
						<td align='center' class='tabletext border_left border_top'>C</td>
						<td align='center' class='tabletext border_left border_top'>D</td>
						<td align='center' class='tabletext border_left border_top'>E</td>
						";
		$markTable .= "</tr>";
		$markTable .= "</table>";
		
		$x = "<tr>";
		$x .= "<td colspan='2'>";
		$x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' valign='top' width='10%'>Remarks</td>";
		$x .= "<td width='80%'>";
		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>1.</td><td class='tabletext'>Grading System:</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>&nbsp;</td><td class='tabletext'>$markTable</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>2.</td><td class='tabletext'>Pass Mark: 60</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class='tabletext' width='5%'>3.</td><td class='tabletext'>Conduct Assessment: A to D with sub-grades</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		
		return $x;
	}
	########### END Template Related
	
	function returnIsSchemeAllGrade($ClassLevelID)
	{
		// Check if all grading schemes of all subjects are display as Grade
		// If yes, this report is assume to be a P5-P6 report, need different handling than P1-P4 report
		
		$FormSubjectGradingScheme = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		$isSchemeAllGrade = true;
		foreach($FormSubjectGradingScheme as $tmpSubjectID => $tmpSchemeInfo) {
			if ($tmpSchemeInfo["scaleDisplay"] != "G") {
				$isSchemeAllGrade = false;
				break;
			}
		}
		#$isSchemeAllGrade = true;	
		
		return $isSchemeAllGrade;
	}
	
	function convertAverageMarkToGrade($Mark)
	{
		$lowerLimitArr = array(90, 75, 60, 30, 0);
		$gradeArr = array("A", "B", "C", "D", "E");
		$failedGradeArr = array("D", "E");
		
		for ($i=0; $i<sizeof($lowerLimitArr); $i++)
		{
			$thisLowerLimit = $lowerLimitArr[$i];
			$thisGrade = $gradeArr[$i];
			if ($Mark > $thisLowerLimit)
			{
				if (in_array($thisGrade, $failedGradeArr))
				{
					$thisMarkDisplay = $this->ReturnTextwithStyle($thisGrade, 'HighLight', 'Fail');
				}
				else
				{
					$thisMarkDisplay = $thisGrade;
				}
				return $thisMarkDisplay;
			}
		}
		
		return "E";
	}
	
	function GET_FORM_NUMBER($ClassLevelID)
	{
		$ClassLevelName = $this->returnClassLevel($ClassLevelID);
		$FormNumber = substr($ClassLevelName,strlen($ClassLevelName)-1,1);
		
		return $FormNumber;
	}
}
?>