<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "-";
		
		//$this->textAreaMaxChar = 765;
		//$this->textAreaMaxChar_SubjectTeacherComment = 765;
		
		$this->OverallResultLowerLimitArr[38] = 'Excellent';
		$this->OverallResultLowerLimitArr[31] = 'VeryGood';
		$this->OverallResultLowerLimitArr[21] = 'Good';
		$this->OverallResultLowerLimitArr[16] = 'Satisfactory';
		$this->OverallResultLowerLimitArr[12] = 'Fair';
		$this->OverallResultLowerLimitArr[06] = 'Weak';
		
		// 2012-0625-1758-22071: disabled settings since added 
		/*
		 * UserID:
		 * 
		 * Principal						83		Chia Loy Tian
		 * Vice-Principal					84		Pearl Moses
		 * Upper Secondary Supervisor		52		Jamaliah Ali
		 * Lower Secondary Supervisor		59		Amaranathan
		 */
//		 $this->GenerateReportUserInfoArr = array();
//		 $this->GenerateReportUserInfoArr[83]['Post'] = 'Principal';
//		 $this->GenerateReportUserInfoArr[84]['Post'] = 'VicePrincipal';
//		 $this->GenerateReportUserInfoArr[52]['Post'] = 'UpperSecondarySupervisor';
//		 $this->GenerateReportUserInfoArr[59]['Post'] = 'LowerSecondarySupervisor';
//		 $this->GenerateReportUserInfoArr[1063]['Post'] = 'LowerSecondarySupervisor';
//		 //2012-0625-1758-22071
//		 $this->GenerateReportUserInfoArr[38]['Post'] = 'FormSupervisor';
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$AcademicResultPage = '';
		$AcademicResultPage .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' style='page-break-after:always;'>";
		$AcademicResultPage .= "<tr><td>".$this->Get_Empty_Image('60px')."</td></tr>";
		$AcademicResultPage .= "<tr><td>".$TitleTable."</td></tr>";
		$AcademicResultPage .= "<tr><td>&nbsp;</td></tr>";
		$AcademicResultPage .= "<tr><td>".$MSTable."</td></tr>";
		$AcademicResultPage .= "</table>";
		
		$SubjectCommentPage = $this->Get_Subject_Comment_Page($ReportID, $StudentID);
		
		
		$x = '';
		$x .= '<tr>'."\r\n";
			$x .= '<td style="vertical-align:top;">'."\r\n";
				$x .= $AcademicResultPage;
				$x .= $SubjectCommentPage;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		return $x;
	}
	
	function getReportHeader($ReportID) //aaaa
	{
		global $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle =  $ReportSetting['ReportTitle'];
		$ReportTitleArr = array_remove_empty(explode(":_:", $ReportTitle));
		$ReportTitle = implode('<br />', (array)$ReportTitleArr);
		
		$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma.png';
		$SchoolLogoWidth = 110;
		$SchoolLogo = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:'.$SchoolLogoWidth.'px;">' : '&nbsp;';
	
		$x = '';
		$x .= '<table class="font_13pt" style="width:100%; text-align:center;" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>
					 <td rowspan="5" style="width:'.($SchoolLogoWidth+50).'px;">'.$SchoolLogo.'</td>
					 <td>'.'&nbsp;'.'</td>				 
					 <td rowspan="5" style="width:'.($SchoolLogoWidth+50).'px;">&nbsp;</td>
				   </tr>'."\n";		
			$x .= '<tr><td>'.'&nbsp;'.'</td></tr>'."\n";
			$x .= '<tr><td><b>'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</b></td></tr>'."\n";
			$x .= '<tr><td><b>'.strtoupper($eReportCard['Template']['AcademicProgressReportEn']).'</b></td></tr>'."\n";		
			$x .= '<tr><td><b>'.$ReportTitle.'</b></td></tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_teacherExtraInfo.php');
		
		$lreportcard_teacherExtraInfo = new libreportcard_teacherExtraInfo();
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		$AttendanceDays = $ReportInfoArr['AttendanceDays'];
		
		$YearTermID = $ReportInfoArr['Semester'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T';
		$ReportTypeCode = $this->Get_School_Internal_Report_Type_Code($ReportID);
		
		$IssueDate = $ReportInfoArr['Issued'];
		
		$LastGeneratedBy = $ReportInfoArr['LastGeneratedBy'];
		$infoAry = $lreportcard_teacherExtraInfo->returnInfoByTeacherId($LastGeneratedBy);
		$recordId = $infoAry[0]['RecordID'];
		$teacherExtraInfoObj = new libreportcard_teacherExtraInfo($recordId);
		
		
		### Get Student Info
		$defaultVal = ($StudentID=='')? "XXX" : '';
		if ($StudentID) {
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$StudentNameEn = $StudentInfoArr[0]['EnglishName'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			//$STRN = $StudentInfoArr[0]['STRN'];
			$WebSAMSRegNo = $StudentInfoArr[0]['WebSAMSRegNo'];
		}
		$StudentNameEn = ($StudentNameEn=='')? $defaultVal : $StudentNameEn;
		$ClassName = ($ClassName=='')? $defaultVal : $ClassName;
		//$STRN = ($STRN=='')? $defaultVal : $STRN;
		$WebSAMSRegNo = ($WebSAMSRegNo=='')? $defaultVal : $WebSAMSRegNo;
		
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		// $GrandMarkArr[Key] = Data
		$GrandMarkArr = $this->getReportResultScore($ReportID, 0, $StudentID);
		
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		foreach ((array)$GradingSchemeInfoArr as $thisSubjectID => $thisSchemeRangeInfoArr) {
			$FirstSchemeID = $thisSchemeRangeInfoArr['SchemeID'];
			break;
		}
		$SchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($FirstSchemeID);
		$SchemeGradeArr = Get_Array_By_Key($SchemeRangeInfoArr, 'Grade');
		$numOfSchemeGrade = count($SchemeGradeArr);
		
		### Get Subject Statistics Info
		$MainSubjectInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		$SubjectStatInfoArr = $this->Get_Mark_Statistics_Info($ReportID, $MainSubjectIDArr, $Average_Rounding=2, $SD_Rounding=2, $Percentage_Rounding=2, $SubjectMarkRounding=0, $IncludeClassStat=1);
		
		
		### Get Student csv data
		// $OtherInfoDataArr[$StudentID][$YearTermID][Key] = Data
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$OtherInfoDataArr = $OtherInfoDataArr[$StudentID][$YearTermID];
		
		
		### Get Class Teacehr Comment
		$CommentArr = $this->returnSubjectTeacherCommentByBatch($ReportID, $StudentID);
		$ClassTeacherComment = nl2br(trim($CommentArr[0][$StudentID]['Comment']));
		$PrincipalComment = nl2br(trim($CommentArr[0][$StudentID]['AdditionalComment']));
		
		
		### Signauture and School Chop
		// Class Teacher Name
		$ClassTeacherInfoArr = $this->Get_Student_Class_Teacher_Info($StudentID);
		$ClassTeacherNameArr = Get_Array_By_Key($ClassTeacherInfoArr, 'EnglishName');
		$ClassTeacherNameDisplay = implode('<br />', (array)$ClassTeacherNameArr);
		
		// Get Post of generated report user
		// 2012-0625-1758-22071: restructure coding to support signature and school chop upload
//		//$LastGeneratedBy = 52;
//		$GeneratedPersonPost = $this->GenerateReportUserInfoArr[$LastGeneratedBy]['Post'];
//		$SignaturePost = $eReportCard['Template'][$GeneratedPersonPost.'En'];
//		$SignaturePost = ($SignaturePost == '')? '&nbsp;' : $SignaturePost;
//		
//		// Get Chop and Signature of generated report user
//		$CanGenerateReportUserIDArr = array_keys((array)$this->GenerateReportUserInfoArr);
//		if (in_array($LastGeneratedBy, (array)$CanGenerateReportUserIDArr)) {
//			$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma/signature_'.$LastGeneratedBy.'.png';
//			$SignatureImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:120px;">' : '&nbsp;';
//			
//			$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma/chop_'.$LastGeneratedBy.'.png';
//			$ChopImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:165px;">' : '&nbsp;';
//		}
//		else {
//			$SignatureImage = $this->Get_Empty_Image('40px');
//			$ChopImage = $this->Get_Empty_Image('30px');
//		}
		$SignaturePost = $teacherExtraInfoObj->getTeacherTitle();
		$SignatureImage = ($teacherExtraInfoObj->getSignaturePath()=='')? $this->Get_Empty_Image('40px') : $teacherExtraInfoObj->returnSignatureImage('120px');
		$ChopImage = ($teacherExtraInfoObj->getSchoolChopPath()=='')? $this->Get_Empty_Image('30px') : $teacherExtraInfoObj->returnSchoolChopImage('165px');
		
		
		### Reorder the Subject Display Order
		$NewSubjectSequenceArr = array();
		$AchievementAssoArr = array();
		$SubjectObjArr = array();
		foreach((array)$MainSubjectInfoArr as $thisSubjectID => $thisSubjectInfoArr)
		{
			$SubjectObjArr[$thisSubjectID] = new subject($thisSubjectID);
			$thisSubjectObj = $SubjectObjArr[$thisSubjectID];
			$thisLearningCategoryID = $thisSubjectObj->LearningCategoryID;
			
			$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
			$thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];
			
			$thisMarkDisplay = '';
			$thisGradeDisplay = '';
			
			if ($StudentID) {
				$ReportColumnID = 0;	// Overall Result
				$thisMark = $MarksArr[$thisSubjectID][$ReportColumnID]['Mark'];
				$thisGrade = $MarksArr[$thisSubjectID][$ReportColumnID]['Grade'];
				
				if ($thisGrade == 'N.A.') {
					continue;
				}
				if ($thisGrade!='' && !$this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
					$AchievementAssoArr[$thisGrade]++; 
				}
				
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisGrade);
				if($needStyle) {
					$thisNatureDetermineScore = ($thisScaleDisplay=='G')? $thisGrade : $thisMark;
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisNatureDetermineScore);
					$thisGradeDisplay = $this->Get_Score_Display_HTML($thisGrade, $ReportID, $ClassLevelID, $thisSubjectID, $thisNatureDetermineScore);
				}
				
				if ($thisGrade == '+') {
					$thisMarkDisplay = $eReportCard['RemarkAbsentZeorMark'];
					$thisGradeDisplay = $eReportCard['RemarkAbsentZeorMark'];
				}
				else {
					if ($thisScaleInput=='G' || $this->Check_If_Grade_Is_SpecialCase($thisGrade)) {
						$thisMarkDisplay = $this->EmptySymbol;
					}
					if ($thisScaleDisplay=='M') {
						$thisGradeDisplay = $this->EmptySymbol;
					}
				}
			}
			else {
				$thisMarkDisplay = $this->EmptySymbol;
				$thisGradeDisplay = $this->EmptySymbol;
			}
			
			$thisMarkDisplay = ($thisMarkDisplay == '')? $this->EmptySymbol :  $thisMarkDisplay;
			$thisGradeDisplay = ($thisGradeDisplay == '')? $this->EmptySymbol :  $thisGradeDisplay;
			
			
			// $SubjectStatInfoArr[$SubjectID][$YearClassID=0]['MinMark', 'MaxMark', 'Average_Raw']
			$YearClassID = 0;	// Form Statistics
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Highest'] = ($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MaxMark']=='')? $this->EmptySymbol : $SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MaxMark'];
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Lowest'] = ($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MinMark']=='')? $this->EmptySymbol : $SubjectStatInfoArr[$thisSubjectID][$YearClassID]['MinMark'];
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Average'] = ($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['Average_Raw']=='')? $this->EmptySymbol : my_round($SubjectStatInfoArr[$thisSubjectID][$YearClassID]['Average_Raw'], 0);
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Mark'] = $thisMarkDisplay;
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['Grade'] = $thisGradeDisplay;
			
			$NewSubjectSequenceArr[$thisLearningCategoryID][$thisSubjectID]['SubjectName'] = $MainSubjectInfoArr[$thisSubjectID][0];
		}
		
		$TotalNumOfColumn = ($FormLevelCode=='L')? 8 : 9;
		
		$x = '';
		$x .= '<table cellpadding="0" cellspacing="0" class="font_10pt report_border_left report_border_right report_border_top" style="width:100%; text-align:left;">'."\n";
			if ($FormLevelCode == 'L') {
				$x .= '<col width="4%" />'."\n";
				$x .= '<col width="38%" />'."\n";
				$x .= '<col width="6%" />'."\n";
				$x .= '<col width="6%" />'."\n";
				$x .= '<col width="12%" />'."\n";
				$x .= '<col width="12%" />'."\n";
				$x .= '<col width="12%" />'."\n";
				$x .= '<col width="10%" />'."\n";
			}
			else if ($FormLevelCode == 'H') {
				$x .= '<col width="14%" />'."\n";
				$x .= '<col width="4%" />'."\n";
				$x .= '<col width="28%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="9%" />'."\n";
				$x .= '<col width="10%" />'."\n";
				$x .= '<col width="8%" />'."\n";
			}
			
			
			### Student Info & Report Card Type
			$ReportTypePaddingLeft = 5;
			$Colspan = ($FormLevelCode=='L')? 3 : 4;
			$thisTickDisplay = ($ReportTypeCode=='FirstTermExam') ? $this->Get_Tick_Symbol() : '&nbsp;';
			$x .= '<tr>'."\n";
				$x .= '<td rowspan="4" colspan="'.$Colspan.'" style="height:100%; padding-left:5px;">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; height:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td colspan="2" style="vertical-align:top; padding-top:10px;">'.$eReportCard['Template']['StudentNameEn'].': <b><u>'.$StudentNameEn.'</u></b></td>'."\n";
						$x .= '<tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td style="width:50%; vertical-align:bottom;">'.$eReportCard['Template']['ClassEn'].': <b><u>'.$ClassName.'</u></b></td>'."\n";
							$x .= '<td style="width:50%; vertical-align:bottom;">'.$eReportCard['Template']['FileNoEn'].': <b><u>'.$WebSAMSRegNo.'</u></b></td>'."\n";
						$x .= '<tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td colspan="4" class="border_left" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$eReportCard['Template']['FirstTermExaminationEn'].'</td>'."\n";
				$x .= '<td class="border_left" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$thisTickDisplay = ($ReportTypeCode=='SecondTermExam') ? $this->Get_Tick_Symbol() : '&nbsp;';
			$x .= '<tr>'."\n";
				$x .= '<td colspan="4" class="border_left border_top" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$eReportCard['Template']['MidTermExaminationEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$thisTickDisplay = ($ReportTypeCode=='PMR_TrialExam' || $ReportTypeCode=='SMR_TrialExam')? $this->Get_Tick_Symbol() : '&nbsp;';
			$thisExamDisplay = ($FormLevelCode=='L')? $eReportCard['Template']['PMRTrialExaminationEn'] : $eReportCard['Template']['SPMTrialExaminationEn'];
			$x .= '<tr>'."\n";
				$x .= '<td colspan="4" class="border_left border_top" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$thisExamDisplay.'</td>'."\n";
				$x .= '<td class="border_left border_top" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$thisTickDisplay = ($ReportTypeCode=='FinalTermExam')? $this->Get_Tick_Symbol() : '&nbsp;';
			$x .= '<tr>'."\n";
				$x .= '<td colspan="4" class="border_left border_top" style="padding-left:'.$ReportTypePaddingLeft.'px;">'.$eReportCard['Template']['FinalTermExaminationEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top" style="text-align:center; vertical-align:bottom;">'.$thisTickDisplay.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Mark Info
			$x .= '<tr style="text-align:center;">'."\n";
				if ($FormLevelCode == 'H') {
					$x .= '<td rowspan="2" class="border_top">'.$eReportCard['Template']['GroupingEn'].'</td>'."\n";
				}
				$border_left = ($FormLevelCode=='L')? '' : 'border_left';
				$x .= '<td rowspan="2" class="'.$border_left.' border_top">&nbsp;</td>'."\n";
				$x .= '<td rowspan="2" class="border_left border_top">'.$eReportCard['Template']['SubjectsEn'].'</td>'."\n";
				$x .= '<td colspan="4" class="border_left border_top">'.$eReportCard['Template']['OverallMarksForFormEn'].'</td>'."\n";
				$x .= '<td rowspan="2" class="border_left border_top">'.$eReportCard['Template']['StudentMarkEn'].'</td>'."\n";
				$x .= '<td rowspan="2" class="border_left border_top">'.$eReportCard['Template']['GradeEn'].'</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr style="text-align:center;">'."\n";
				$x .= '<td colspan="2" class="border_left border_top">'.$eReportCard['Template']['HighestEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top">'.$eReportCard['Template']['LowestEn'].'</td>'."\n";
				$x .= '<td class="border_left border_top">'.$eReportCard['Template']['AverageEn'].'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$SubjectDisplayCount = 0;
			foreach ((array)$NewSubjectSequenceArr as $thisLearningCategoryID => $thisLearningCategorySubjectArr) {
				$thisIsFirstSubjectInLearningCategory = true;
				$thisLC_Colspan = count($thisLearningCategorySubjectArr);
				
				$thisLearningCategoryObj = new Learning_Category($thisLearningCategoryID);
				$thisLC_NameEn = $thisLearningCategoryObj->NameEng;
				
				foreach ((array)$thisLearningCategorySubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
					$x .= '<tr style="text-align:center;">'."\n";
						// Show Learning Category Info if this is the first Subject
						if ($FormLevelCode == 'H' && $thisIsFirstSubjectInLearningCategory) {
							$x .= '<td rowspan="'.$thisLC_Colspan.'" class="border_top">'.$thisLC_NameEn.'</td>'."\n";
							$thisIsFirstSubjectInLearningCategory = false;
						}
						
						$border_left = ($FormLevelCode=='L')? '' : 'border_left';
						$x .= '<td class="border_top '.$border_left.'" style="text-align:left; padding-left:5px;">'.(++$SubjectDisplayCount).'</td>'."\n";
						$x .= '<td class="border_top border_left" style="text-align:left; padding-left:5px;">'.$thisSubjectInfoArr['SubjectName'].'</td>'."\n";
						$x .= '<td colspan="2" class="border_top border_left">'.$thisSubjectInfoArr['Highest'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Lowest'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Average'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Mark'].'</td>'."\n";
						$x .= '<td class="border_top border_left">'.$thisSubjectInfoArr['Grade'].'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			}
			
			
			### Footer Info
			// Achievement
			$AchievementDisplayArr = array();
			for ($i=0; $i<$numOfSchemeGrade; $i++) {
				$thisGrade = $SchemeGradeArr[$i];
				$thisGradeCount = $AchievementAssoArr[$thisGrade];
				
				if ($thisGradeCount != '' && $thisGradeCount > 0) {
					$AchievementDisplayArr[] = $thisGradeCount.$thisGrade;
				}
			}
			$AchievementDisplay = Get_Table_Display_Content(implode(' ', (array)$AchievementDisplayArr));
			
			$LeftPadding = 5;
			$RightPadding = 20;
			$x .= '<tr>'."\n";
				$x .= '<td colspan="'.$TotalNumOfColumn.'" class="border_top">'."\n";
					// 2012-0419-1017-02067
					// 20120502: Email "RE: Malaysia Wesley's eRC fix"
					// 6) hide all items but display only Achievement and Overall result (display N/A if any subject is showing absent)
					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						// Total Mark and Achievement
						$x .= '<tr>'."\n";
							//$x .= '<td style="width:60%; padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['TotalMarkEn'].': '.$GrandMarkArr['GrandTotal'].'</td>'."\n";
							$x .= '<td style="width:60%; padding-left:'.$LeftPadding.'px;">&nbsp;</td>'."\n";
							$x .= '<td>'.$eReportCard['Template']['AchievementEn'].': '.$AchievementDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";
						// Percentage and GPMP
						$x .= '<tr>'."\n";
							//$x .= '<td style="padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['PercentageEn'].': '.$GrandMarkArr['GrandAverage'].'</td>'."\n";
							//$x .= '<td>'.$eReportCard['Template']['GPMPEn'].': '.$GrandMarkArr['GPA'].'</td>'."\n";
							$x .= '<td style="padding-left:'.$LeftPadding.'px;">&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
						// No of Subject Taken and Overall Results
						$x .= '<tr>'."\n";
							//$x .= '<td style="padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['NoOfSubjectTakenEn'].': '.$SubjectDisplayCount.'</td>'."\n";
							$x .= '<td style="padding-left:'.$LeftPadding.'px;">&nbsp;</td>'."\n";
							if ($FormLevelCode == 'L') {
								$OverallDisplay = $this->Get_Overall_Result_Display($ReportID, $StudentID);
								if ($OverallDisplay == -1) {
									$x .= '<td>&nbsp;</td>'."\n";
								}
								else {
									$OverallDisplay = ($OverallDisplay=='')? '&nbsp;' : $OverallDisplay;
									$x .= '<td>'.$eReportCard['Template']['OverallResultsEn'].': '.$OverallDisplay.'</td>'."\n";
								}
							}
							else {
								$x .= '<td>&nbsp;</td>'."\n";
							}
						$x .= '</tr>'."\n";
						// Placement (Class)
						$x .= '<tr>'."\n";
							$thisDisplay = ($GrandMarkArr['OrderMeritClass']=='' || $GrandMarkArr['OrderMeritClass']==-1)? $this->EmptySymbol : $GrandMarkArr['OrderMeritClass'];
							//$x .= '<td style="padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['PlacementClassEn'].': '.$thisDisplay.'</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";

//					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
//						// Achievement and Overall Result
//						$x .= '<tr>'."\n";
//							$x .= '<td style="width:60%; padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['AchievementEn'].': '.$AchievementDisplay.'</td>'."\n";
//							if ($FormLevelCode == 'L') {
//								$OverallDisplay = $this->Get_Overall_Result_Display($ReportID, $StudentID);
//								$OverallDisplay = ($OverallDisplay=='')? '&nbsp;' : $OverallDisplay;
//								
//								$x .= '<td>'.$eReportCard['Template']['OverallResultsEn'].': '.$OverallDisplay.'</td>'."\n";
//							}
//							else {
//								$x .= '<td>&nbsp;</td>'."\n";
//							}
//						$x .= '</tr>'."\n";
//					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Attendance and Conduct
			$Attendance = ($OtherInfoDataArr['Attendance']=='')? $this->EmptySymbol : $OtherInfoDataArr['Attendance'];
			//2012-0419-1017-02067
			//$TotalNumOfSchoolDay = ($OtherInfoDataArr['Total Number of School Days']=='')? $this->EmptySymbol : $OtherInfoDataArr['Total Number of School Days'];
			$TotalNumOfSchoolDay = ($AttendanceDays=='')? $this->EmptySymbol : $AttendanceDays;
			
			if (is_array($OtherInfoDataArr['Conduct'])) {
				$OtherInfoDataArr['Conduct'] = $OtherInfoDataArr['Conduct'][0];
			}
			$Conduct = ($OtherInfoDataArr['Conduct']=='')? $this->EmptySymbol : $OtherInfoDataArr['Conduct'];
			$x .= '<tr>'."\n";
				$x .= '<td colspan="'.$TotalNumOfColumn.'" class="border_top">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td style="width:60%; padding-left:'.$LeftPadding.'px;">'.$eReportCard['Template']['AttendanceEn'].': &nbsp;&nbsp;&nbsp;&nbsp;'.$Attendance.'/'.$TotalNumOfSchoolDay.' '.$eReportCard['Template']['DaysEn'].'</td>'."\n";
							$x .= '<td>'.$eReportCard['Template']['ConductEn'].': '.$Conduct.'</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			### Comments, Issue Date and Signature
			$x .= '<tr>'."\n";
				$x .= '<td colspan="'.$TotalNumOfColumn.'" class="border_top border_bottom" style="padding-left:'.$LeftPadding.'px; padding-right:'.$RightPadding.'px;">'."\n";
					$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr><td colspan="2">&nbsp;</td></tr>'."\n";
//						$x .= '<tr>'."\n";
//							// Comment and Issue Date
//							$x .= '<td style="width:80%; padding-left:'.$LeftPadding.'px;">'."\n";
//								$x .= '<table class="font_10pt" style="width:100%; height:276px; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
//									$x .= '<tr><td style="height:12px; vertical-align:top;">'.$eReportCard['Template']['CommentsEn'].': </td></tr>'."\n";
//									$x .= '<tr><td style="vertical-align:top;">'."\n";
//										$x .= $ClassTeacherComment;
//										$x .= '<br />';
//										$x .= $PrincipalComment;
//									$x .= '</td></tr>'."\n";
//									$x .= '<tr><td style="height:60px; vertical-align:top;">'.$eReportCard['Template']['DateEn'].': '.$IssueDate.'</td></tr>'."\n";
//								$x .= '</table>'."\n";
//							$x .= '</td>'."\n";
//							
//							// Signature
//							$x .= '<td>'."\n";
//								$x .= '<table class="font_10pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
//									$x .= '<tr><td style="height:100px; text-align:center; vertical-align:bottom;">'.$ClassTeacherNameDisplay.'</td></tr>'."\n";
//									$x .= '<tr><td class="border_top" style="vertical-align:top; text-align:center;">'.$eReportCard['Template']['HomeroomTeacherEn'].'</td></tr>'."\n";
//									$x .= '<tr><td style="height:75px; text-align:center; vertical-align:bottom;">'.$SignatureImage.'</td></tr>'."\n";
//									$x .= '<tr><td class="border_top" style="vertical-align:top; text-align:center;">'."\n";
//										$x .= $SignaturePost."\n";
//									$x .= '</td></tr>'."\n";
//									$x .= '<tr><td style="text-align:center; vertical-align:top;">'.$ChopImage.'</td></tr>'."\n";
//								$x .= '</table>'."\n";
//							$x .= '</td>'."\n";
//							
//							$x .= '<td style="width:4%;">&nbsp;</td>'."\n";
//						$x .= '</tr>'."\n";


						// Class Teacher Comment and Class Teacher Signature
						$x .= '<tr>'."\n";
							$x .= '<td style="width:80%;">'.$eReportCard['Template']['CommentsEn'].': </td>'."\n";
							$x .= '<td style="width:20%;">&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr style="height:50px; vertical-align:top;">'."\n";
							$x .= '<td><b>'.$ClassTeacherComment.'</b></td>'."\n";
							$x .= '<td style="text-align:center; vertical-align:bottom;">'.$ClassTeacherNameDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td class="border_top" style="vertical-align:top; text-align:center;">'.$eReportCard['Template']['HomeroomTeacherEn'].'</td>'."\n";
						$x .= '</tr>'."\n";
						
						// Principal Comment and Generated User Signature and Chop
						$x .= '<tr>'."\n";
							$x .= '<td>'.$eReportCard['Template']['PrincipalCommentsEn'].': </td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr style="height:50px; vertical-align:top;">'."\n";
							$x .= '<td><b>'.$PrincipalComment.'</b></td>'."\n";
							$x .= '<td style="vertical-align:bottom; text-align:center;">'.$SignatureImage.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>'.$eReportCard['Template']['DateEn'].': '.$IssueDate.'</td>'."\n";
							$x .= '<td class="border_top" style="vertical-align:top; text-align:center;">'.$SignaturePost.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td style="vertical-align:top; text-align:center;">'.$ChopImage.'</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_Subject_Comment_Page($ReportID, $StudentID) {
		global $Lang, $intranet_httppath;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$DisplayOption = $ReportInfoArr['DisplayOption'];
		$PrintFailedSubjectOnly = ($DisplayOption==2)? true : false;
		
		$MainSubjectInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		$numOfSubject = count($MainSubjectIDArr);
		
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Student Marks
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		
		### Get Subject Teacehr Comment
		// $CommentArr[$SubjectID] = Data
		$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		
		$SubjectCounter = 0;
		$x = '';
		
		//$x .='<div>page-break-after:always;</div>';
		
		for ($i=0; $i<$numOfSubject; $i++) {
			$thisSubjectID = $MainSubjectIDArr[$i];
			
			//debug_r($MarksArr[$thisSubjectID]);
			
			$ReportColumnID = 0;	// Overall Result
			$thisMark = $MarksArr[$thisSubjectID][$ReportColumnID]['Mark'];
			$thisGrade = $MarksArr[$thisSubjectID][$ReportColumnID]['Grade'];
			$thisComment = nl2br(trim($CommentArr[$thisSubjectID]));
			
			$thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
			$thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];
			$thisMarkNature = $this->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMark, $ReportColumnID, '', $ReportID);
			
			// 2012-0419-1017-02067
//			if ($thisGrade == 'N.A.' || $thisMarkNature != 'Fail') {
//				continue;
//			}
			if ($thisGrade == 'N.A.') {
				continue;
			}
			if ($PrintFailedSubjectOnly && $thisMarkNature != 'Fail') {
				continue;
			}
			
			
			$SubjectCounter++;
	
			if ($SubjectCounter % 2 == 0 ) {
				// The second Subject of the page
				$thisPageBreakCSS = 'page-break-after:always;';
				$thisTableBorderBottom = '';
				
			}
			else {
				$thisPageBreakCSS = '';
				$thisTableBorderBottom = 'border_bottom';
				$x .="<br/><br/><br/><br/>";
			}
					
			$x .="<br/><br/>";	
			$x .= '<table cellpadding="0" cellspacing="0" class="'.$thisTableBorderBottom.'" style="width:100%; text-align:left;'.$thisPageBreakCSS.'">'."\n";	
				$x .= '<tr>';	
					$x .= '<td>';	
						$x .=$this->Get_Subject_Comment_Page_Header($ReportID, $thisSubjectID);
					$x .= '</td>';
				$x .= '</tr>';
				$x .= '<tr>';	
					$x .= '<td>';	
						$x .=$this->Get_Subject_Comment_Page_Content($ReportID, $StudentID, $thisSubjectID, $thisMark, $thisGrade, $thisComment);
					$x .= '</td>';
				$x .= '</tr>';
				$x .= '<tr>';	
					$x .= '<td>';	
						$x .=$this->Get_Subject_Comment_Page_Footer($ReportID, $StudentID, $thisSubjectID);	
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</table>';
		}
		
		
		return $x;
	}
	
	private function Get_Subject_Comment_Page_Header($ReportID, $SubjectID) { //aaaa
		global $eReportCard;
		
		// Get School Logo
		$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma.png';
		$SchoolLogoWidth = 110;
		$SchoolLogo = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:'.$SchoolLogoWidth.'px;">' : '&nbsp;';
		
		
		// Get Subject Name
		$SubjectNameEn = $this->GET_SUBJECT_NAME_LANG($SubjectID, 'en'); 
		
		// Get tick symbol
		$TickSymbol = $this->Get_Tick_Symbol();
		
		// Get Form Type
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		$FormTypeDisplay = ($FormLevelCode=='L')? $eReportCard['Template']['LowerSecondaryEn'] : $eReportCard['Template']['UpperSecondaryEn'];
				
		// Get Report Title
		$ReportTitle =  $ReportInfoArr['ReportTitle'];
		$ReportTitleArr = array_remove_empty(explode(":_:", $ReportTitle));
		$ReportTitle = implode('<br/>', (array)$ReportTitleArr);
		
		// Get Report Type
		$ReportTypeCode = $this->Get_School_Internal_Report_Type_Code($ReportID);
		
		$ExamTypeWidth = 45;
		$TickWidth = 5;
		
		$x = '';//class="border_table"
		
		
		
		$x .= '<table style="width:100%; text-align:center;" cellpadding="0" cellspacing="0">'."\n";
		
			$x .= '<tr class="border_left border_top" style="text-align:center; vertical-align:middle;">'."\n";
				$x .= '<td rowspan="4" style="width:'.($SchoolLogoWidth+50).'px;">'.$SchoolLogo.'</td>'."\n";
				$x .= '<td class="font_13pt" style="font-weight:bold;">'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</td>'."\n";
				$x .= '<td rowspan="4" style="width:'.($SchoolLogoWidth+50).'px;">&nbsp</td>'."\n";
			$x .= '</tr>'."\n";
		
		
			$x .= '<tr class="border_left border_top" style="text-align:center; vertical-align:top;">'."\n";
				$x .= '<td class="font_13pt">'.strtoupper($SubjectNameEn." ".$FormTypeDisplay." ".$eReportCard['Template']['CommentSheetEn']).'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr class="border_left border_top" style="text-align:center; vertical-align:top;">'."\n";
				$x .= '<td class="font_13pt">'.strtoupper($ReportTitle).'</td>'."\n";
			$x .= '</tr>'."\n";
		
		
			$x .= '<tr class="border_left border_top font_11pt">'."\n";
				$x .= '<td>';
				    $x .= '<table class="font_11pt border_table" style="width:100%; " cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr class="border_left border_top">'."\n";
							$thisTickDisplay = ($ReportTypeCode=='FirstTermExam') ? $this->Get_Tick_Symbol() : '&nbsp;';
							$x .= '<td style="text-align:left; padding-left:5px;">'.strtoupper($eReportCard['Template']['FirstTermExaminationEn']).'</td>'."\n";						
							$x .= '<td style="width:30px;">'.$thisTickDisplay.'</td>'."\n";
							
							$thisTickDisplay = ($ReportTypeCode=='PMR_TrialExam' || $ReportTypeCode=='SMR_TrialExam')? $this->Get_Tick_Symbol() : '&nbsp;';
							$txtPrmSpm = ($FormLevelCode=='L')? $eReportCard['Template']['PMRTrialExaminationEn'] : $eReportCard['Template']['SPMTrialExaminationEn'];
							
							$x .= '<td style="text-align:left; padding-left:5px;">'.strtoupper($txtPrmSpm).'</td>'."\n";
							$x .= '<td style="width:30px;">'.$thisTickDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";
						
						$x .= '<tr class="border_left border_top" style="text-align:center;">'."\n";						
						    $thisTickDisplay = ($ReportTypeCode=='SecondTermExam') ? $this->Get_Tick_Symbol() : '&nbsp;';
							$x .= '<td style="text-align:left; padding-left:5px;">'.strtoupper($eReportCard['Template']['MidTermExaminationEn']).'</td>'."\n";						
							$x .= '<td style="width:30px;">'.$thisTickDisplay.'</td>'."\n";
							
							$thisTickDisplay = ($ReportTypeCode=='FinalTermExam')? $this->Get_Tick_Symbol() : '&nbsp;';
							$x .= '<td style="text-align:left; padding-left:5px;">'.strtoupper($eReportCard['Template']['FinalTermExaminationEn']).'</td>'."\n";
							$x .= '<td style="width:30px;">'.$thisTickDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";					
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	private function Get_Subject_Comment_Page_Content($ReportID, $StudentID, $SubjectID, $Mark, $Grade, $Comment) {
		global $eReportCard;
		
		if ($StudentID) {
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$StudentNameEn = $StudentInfoArr[0]['EnglishName'];
			$ClassName = $StudentInfoArr[0]['ClassName'];
			$STRN = $StudentInfoArr[0]['STRN'];
			
		}
		else {
			$defaultVal = 'XXX';
			$StudentNameEn = $defaultVal;
			$ClassName = $defaultVal;
		}
		
		$x = '';
		
		$paddingSize = 30; //border_table
		
		$x .= '<table class="font_11pt" style="width:100%; text-align:left;" cellpadding="3" cellspacing="0">'."\n";
		
			$x .= '<tr>'."\n";
				$x .= '<td colspan="3" height="40px;">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:'.$paddingSize.'px;">'.strtoupper($eReportCard['Template']['StudentNameEn']).': '.$StudentNameEn.'</td>'."\n";
				$x .= '<td style="text-align:right;">'.strtoupper($eReportCard['Template']['ClassEn']).':'.'</td>'."\n";
				$x .= '<td style="padding-left:5px;">'.$ClassName.'</td>';
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:'.$paddingSize.'px;">'.strtoupper($eReportCard['Template']['MarkEn']).': '.$Mark.'</td>'."\n";
				$x .= '<td style="text-align:right;">'.strtoupper($eReportCard['Template']['GradeEn']).':'.'</td>'."\n";
				$x .= '<td style="padding-left:5px;">'.$Grade.'</td>';
			$x .= '</tr>'."\n";

			$x .= '<tr>'."\n";
				$x .= '<td colspan="3" style="padding-left:'.$paddingSize.'px; padding-right:'.$paddingSize.'px;">'.strtoupper($eReportCard['Template']['CommentsEn']).': '.'</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td colspan="3" height="180px" style="vertical-align:top; padding-left:'.$paddingSize.'px; padding-right:'.$paddingSize.'px;"><b>'.$Comment.'</b></td>'."\n";
			$x .= '</tr>'."\n";
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	private function Get_Subject_Comment_Page_Footer($ReportID, $StudentID, $SubjectID) {
		global $eReportCard;
		
		// Get Issue Date
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$IssueDate = $ReportInfoArr['Issued'];
		
		// Get Subject Teacher
		$SubjectTeacherNameArr = $this->returnSubjectTeacher('', $SubjectID, $ReportID, $StudentID, $FullTeacherInfo=0);
		$SubjectTeacherName = (count((array)$SubjectTeacherNameArr) > 0)? implode('<br />', (array)$SubjectTeacherNameArr) : '&nbsp;';
		
		$x = ''; 
		
		$paddingSize = 30; //border_table width:50%;
		
		$x .= '<table class="font_11pt" style="width:100%; text-align:left;" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr class="border_left border_top" style="height:50px; vertical-align:top;">'."\n";
				$x .= '<td style="width:9%; padding-left:'.$paddingSize.'px;">'.$eReportCard['Template']['DateEn'].': </td>';
				$x .= '<td style="width:41%;">'.$IssueDate.'</td>';
				$x .= '<td style="width:15%; text-align:right;">'.$eReportCard['Template']['SubjectTeacherEn'].': </td>';
				$x .= '<td style="width:35%; padding-left:5px;"">'.$SubjectTeacherName.'</td>';
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		
	}
	
	function genMSTableColHeader($ReportID)
	{
		
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		
	}
	
	function getSignatureTable($ReportID='')
	{
 		
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		
	}
	
	########### END Template Related
	
	// return "L" for Lower Form, "H" for Higher Form
	private function Get_Student_Form_Level_Code($ClassLevelID) {
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		if ($FormNumber <= 3) {
			return "L";
		}
		else {
			return "H";
		}
	}
	
	private function Get_Overall_Result_Display($ReportID, $StudentID) {
		global $eReportCard, $PATH_WRT_ROOT;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		// $MarksArr[$SubjectID][$ReportColumnID][Key] = Data
		$MarksArr = $this->getMarks($ReportID, $StudentID, '', 1, 1);
		
		// $GradingSchemeInfoArr[$SubjectID]['SchemeID'] = Data
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		// $GrandMarkArr[Key] = Data
		//$GrandMarkArr = $this->getReportResultScore($ReportID, 0, $StudentID);
		//$GPA = $GrandMarkArr['GPA'];
		
		$SubjectWeightDataArr = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID=0);
		$SubjectWeightDataAssoArr = BuildMultiKeyAssoc($SubjectWeightDataArr, array("SubjectID"), array("Weight"), 1);
		
		// Get the subjects which count the in the Overall Result 
		$includedSubjectIdAry = $this->Get_Overall_Result_Subject_SubjectID_Arr();
		
		
		$TotalSubjectWeight = 0;
		$TotalGPA = 0;
		$noOverallResult = false;
		foreach ((array)$MarksArr as $thisSubjectID => $thisSubjectMarksArr) {
			$thisGrade = $thisSubjectMarksArr[0]['Grade'];
			$thisSchemeID = $GradingSchemeInfoArr[$thisSubjectID]['SchemeID'];
			
			if (!in_array($thisSubjectID, (array)$includedSubjectIdAry)) {
				continue;
			}
			
			// do not display overall result if student absent in one of the required subjects
			if ($thisGrade=='abs' || $thisGrade=='-') {
				$noOverallResult = true;
				break;
			}
			
			if ($thisGrade != '') {
				$thisWeight = ($SubjectWeightDataAssoArr[$thisSubjectID])? $SubjectWeightDataAssoArr[$thisSubjectID] : 0;
				$TotalSubjectWeight += $thisWeight;
				
				$thisGpa = $this->CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($thisSchemeID, $thisGrade, $ReportID, $StudentID, $thisSubjectID, $ClassLevelID, $ReportColumnID=0);
				if ($thisGpa > 0 && is_numeric($thisGpa)) {
					$TotalGPA += $thisGpa;
				}
			}
		}
		
		if ($noOverallResult) {
			return 'N.A.';
		}
		else {
			//$OverallResultPoint = $GPA * $TotalSubjectWeight;
			//2013-0626-1055-34167
			//$OverallResultPoint = $TotalGPA * $TotalSubjectWeight;
			$OverallResultPoint = $TotalGPA;
			$OverallResultMappingArr = $this->Get_Overall_Result_Lower_Limit_Arr();
			foreach ((array)$OverallResultMappingArr as $thisLowerLimit => $thisOverallResultCode) {
				//if ($OverallResultPoint >= $thisLowerLimit) {
				if (floatcmp((float)$OverallResultPoint, ">=", (float)$thisLowerLimit)) {
					return $this->Get_Overall_Result_Display_By_Code($thisOverallResultCode);
				}
			}
		}
	}
	
	private function Get_Overall_Result_Lower_Limit_Arr() {
		return $this->OverallResultLowerLimitArr;
	}
	
	private static function Get_Overall_Result_Display_By_Code($Code) {
		global $eReportCard;
		return $eReportCard['Template']['OverallResultArr'][$Code];
	}
	
	private function Get_Overall_Result_Subject_WebSAMSCode_Arr() {
		global $eRCTemplateSetting;
		
		$subjectWebSAMSCodeArr = array();
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Bahasa_Melayu'];
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Bahasa_Inggeris'];
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Mathematics'];
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Science'];
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Sejarah'];
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Geografi'];
		$subjectWebSAMSCodeArr[] = $eRCTemplateSetting['SubjectWebSAMSCodeArr']['Kemahiran_Hidup'];
		
		return $subjectWebSAMSCodeArr;
	}
	
	private function Get_Overall_Result_Subject_SubjectID_Arr() {
		$includedSubjectWebSAMSCodeAry = $this->Get_Overall_Result_Subject_WebSAMSCode_Arr();
		$numOfSubject = count((array)$includedSubjectWebSAMSCodeAry);
		
		$subjectWebSAMSCodeMappingAry = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=1);
		
		$subjectIdAry = array();
		for ($i=0; $i<$numOfSubject; $i++) {
			$_subjectWebSAMSCode = $includedSubjectWebSAMSCodeAry[$i];
			$_subjectId = $subjectWebSAMSCodeMappingAry[$_subjectWebSAMSCode];
			
			$subjectIdAry[] = $_subjectId;
		}
		
		return $subjectIdAry;
	}
	
	private function Get_School_Internal_Report_Type_Code($ReportID) {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$YearTermID = $ReportInfoArr['Semester'];
		$ReportType = ($YearTermID=='F')? 'W' : 'T';
		$TermSequenceNumber = ($YearTermID == 'F')? -1 : $this->Get_Semester_Seq_Number($YearTermID);
		
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		
		$PMR_ApplicableFormArr = array(3);
		$SMR_ApplicableFormArr = array(5);
		$FinalTerm_ApplicableFormArr = array(1, 2, 4);
		
		if ($TermSequenceNumber==1) {
			$ReportTypeCode = 'FirstTermExam';	// First-Term Examination
		}
		else if ($TermSequenceNumber==2) {
			$ReportTypeCode = 'SecondTermExam';	// Second-Term Examination
		}
		else if ($TermSequenceNumber==3 && in_array($FormNumber, $PMR_ApplicableFormArr)) {
			$ReportTypeCode = 'PMR_TrialExam';	// PMR Trial Examination
		}
		else if ($TermSequenceNumber==3 && in_array($FormNumber, $SMR_ApplicableFormArr)) {
			$ReportTypeCode = 'SMR_TrialExam';	// SMR Trial Examination
		}
		else if ($TermSequenceNumber==3 && in_array($FormNumber, $FinalTerm_ApplicableFormArr)) {
			$ReportTypeCode = 'FinalTermExam';	// Final-Term Examination
		}
		
		return $ReportTypeCode;
	}
}
?>