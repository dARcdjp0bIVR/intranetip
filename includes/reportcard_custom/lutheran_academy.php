<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################


include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "merit", "themeForInvestigation", "TFIPerformance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 1;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		// Moved to config file
		//$this->textAreaMaxChar = '500';
		//$this->textAreaMaxChar_SubjectTeacherComment = '500';
		
		$this->EmptySymbol = "&nbsp;";
		
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->PersonalCharGradeArr['6'] = 'Excellent';
		$this->PersonalCharGradeArr['5'] = 'VeryGood';
		$this->PersonalCharGradeArr['4'] = 'Good';
		$this->PersonalCharGradeArr['3'] = 'Satisfactory';
		$this->PersonalCharGradeArr['2'] = 'Developing';
		$this->PersonalCharGradeArr['1'] = 'Unsatisfactory';
		$this->PersonalCharGradeArr['Nil'] = 'Nil';
		$this->PersonalCharGradeArr['N/A'] = 'NA';
	}
		
	########## START Template Related ##############
	public function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='') {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		
		$PageInfoArr = array();
		$PageInfoArr[] = $this->Get_Cover_Page_Table($ReportID);
		$PageInfoArr[] = $this->Get_Student_Info_Page_Table($ReportID, $StudentID);
		$PageInfoArr[] = $this->Get_Learner_Profile_Page_Table($ReportID, $StudentID);
		
		if ($FormLevelCode == 'L') {
			$PageInfoArr[] = $this->Get_Grading_Scheme_Page_Table($ReportID, $StudentID);
			$PageInfoArr[] = $this->Get_Theme_For_Investigation_Page_Table($ReportID, $StudentID);
		}
		
		$SubjectPagesInfoArr = $this->Get_Subject_Pages_Table_Array($ReportID, $StudentID);
		$PageInfoArr = array_merge((array)$PageInfoArr, Get_Array_By_Key($SubjectPagesInfoArr, 'html'));
		
		if ($FormLevelCode == 'H') {
			$DisplayedSubjectIDArr = Get_Array_By_Key($SubjectPagesInfoArr, 'SubjectID');
			$PageInfoArr[] = $this->Get_Learning_Attitudes_By_Subject_Page_Table($ReportID, $StudentID, $DisplayedSubjectIDArr);
		}
		
		$x = $this->Get_Report_Layout_By_Pages($PageInfoArr);
		
		return $x;
	}
	
	private function Get_Report_Layout_By_Pages($PageInfoArr) {
		
		$numOfPage = count($PageInfoArr);
		$x = '';
		
		for ($i=0; $i<$numOfPage; $i++) {
			$thisPageHTML = $PageInfoArr[$i];
			
			$thisStyle = '';
			if ($i < ($numOfPage - 1)) {
				$thisStyle = 'style="page-break-after:always;"';
			}
			
			$PageContentHeight = ($i==0)? 936 : 930;
			
			$x .= '<tr><td>'."\r\n";
				$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" '.$thisStyle.'>'."\r\n";
					$x .= '<tr><td>'.$this->Get_Empty_Image_Div('60px').'</td></tr>'."\r\n";								// Height of pre-print header 
					$x .= '<tr height="'.$PageContentHeight.'px" valign="top"><td>'.$thisPageHTML.'</td></tr>'."\r\n";							// Page Content
					$x .= '<tr valign="bottom"><td>'.$this->Get_Page_Number_Table($i+1, $numOfPage).'</td></tr>'."\r\n";	// Pasge Number
				$x .= '</table>'."\r\n";
			$x .= '</td></tr>'."\r\n";
		}
		
		return $x;
	}
	
	public function getReportHeader($ReportID)
	{
		
	}
	
	public function getReportStudentInfo($ReportID, $StudentID='')
	{
		
	}
	
	public function getMSTable($ReportID, $StudentID='')
	{
		
	}
	
	public function getSignatureTable($ReportID='')
	{
 		
	}
	
	public function getMiscTable($ReportID, $StudentID='')
	{
		
	}

	
	private function Get_Page_Number_Table($CurrentPageNum, $TotalPageNum) {
		global $eReportCard;
		
		$PageDisplay = $eReportCard['Template']['PageNumFooter'];
		$PageDisplay = str_replace('<!--CurPageNum-->', $CurrentPageNum, $PageDisplay);
		$PageDisplay = str_replace('<!--TotalNumOfPage-->', $TotalPageNum, $PageDisplay);
		
		$x = '';
		$x .= '<table style="width:100%; border:0px; padding:0px 0px 0px 0px;" valign="bottom" align="center">'."\r\n";
			$x .= '<tr><td valign="bottom" align="right"><i>'.$PageDisplay.'</i></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Cover_Page_Table($ReportID) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		
		### Get Term Name
		$Semester = $ReportInfoArr['Semester'];
		if ($Semester == 'F') {
			$SemesterName = $eReportCard['Template']['WholeYearEn'];
		}
		else {
			$SemesterName = $this->returnSemesters($Semester, 'en');
		}
		
		### Get Academic Year Name
		$AcademicYearName = $this->GET_ACTIVE_YEAR_NAME();
		
		### Get Form Name
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormName = $this->returnClassLevel($ClassLevelID);
		
		$x = '';
		$x .= '<table style="width:100%; text-align:center;">'."\r\n";
			$x .= '<tr><td class="font_58pt">'.$this->Get_Empty_Image('85px').'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_58pt">'.$SemesterName.'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_58pt">'.$this->Get_Empty_Image('28px').'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_58pt">'.$AcademicYearName.'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_58pt">'.$this->Get_Empty_Image('28px').'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_58pt">'.$FormName.'</td></tr>'."\r\n";
			
			$x .= '<tr><td class="font_58pt">'.$this->Get_Empty_Image('68px').'</td></tr>'."\r\n";
			
			$x .= '<tr><td style="text-align:left;">'.$this->Get_Cover_Page_Remarks_Table().'</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Cover_Page_Remarks_Table() {
		global $eReportCard;
		
		$x = '';
		$x .= '<table class="report_border" style="width:490px; text-align:left;">'."\r\n";
			$x .= '<tr><td>'.$this->Get_Empty_Image('10px').'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_10pt">'.$eReportCard['Template']['CoverPageRemarksArr'][0].'</td></tr>'."\r\n";
			$x .= '<tr><td>'.$this->Get_Empty_Image('15px').'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_10pt">'.$eReportCard['Template']['CoverPageRemarksArr'][1].'</td></tr>'."\r\n";
			$x .= '<tr><td>'.$this->Get_Empty_Image('10px').'</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Student_Info_Page_Table($ReportID, $StudentID) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		
		# Get CSV Data
		$LatestTerm = $this->Get_Last_Semester_Of_Report($ReportID);
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$StudentOtherInfoDataArr = $OtherInfoDataArr[$StudentID][$LatestTerm];
		
		# Get Student Info
		$StudentClassInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$StudentNameEn = ($StudentClassInfoArr[0]['EnglishName']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['EnglishName'];
		$StudentNameCh = ($StudentClassInfoArr[0]['ChineseName']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['ChineseName'];
		$ClassName = ($StudentClassInfoArr[0]['ClassName']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['ClassName'];
		$ClassNumber = ($StudentClassInfoArr[0]['ClassNumber']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['ClassNumber'];
		$ClassDisplay = $ClassName.' ('.$ClassNumber.')';
		$StudentNo = ($StudentClassInfoArr[0]['UserLogin']=='')? $this->EmptySymbol : strtoupper($StudentClassInfoArr[0]['UserLogin']);
		$HKID = ($StudentClassInfoArr[0]['HKID']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['HKID'];
		$DateOfBirth = ($StudentClassInfoArr[0]['DateOfBirth']=='')? $this->EmptySymbol : date('d-M-y', strtotime(substr($StudentClassInfoArr[0]['DateOfBirth'], 0, 10)));
		$Gender = ($StudentClassInfoArr[0]['Gender']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['Gender'];
		
		# Get Student House Info
		$StudentHouseInfoArr = $this->Get_Student_House_Info($StudentID);
		$HouseName = Get_Lang_Selection($StudentHouseInfoArr[0]['HouseTitleCh'], $StudentHouseInfoArr[0]['HouseTitleEn']);
		$HouseName = ($HouseName=='')? $this->EmptySymbol : $HouseName;
		
		# Total Num of Attendance Days
		$TotalNumOfAttendanceDay_Template = $ReportInfoArr['AttendanceDays'];
		$TotalNumOfAttendanceDay_CSV = trim($StudentOtherInfoDataArr['Total No. of Attendance Days']);
		$TotalNumOfAttendanceDay = ($TotalNumOfAttendanceDay_CSV != '')? $TotalNumOfAttendanceDay_CSV : $TotalNumOfAttendanceDay_Template;
		$TotalNumOfAttendanceDay = ($TotalNumOfAttendanceDay == '')? 0 : $TotalNumOfAttendanceDay;
		
		# Attendance Days
		$YearClassID = $StudentClassInfoArr[0]['ClassID'];
		$AttendanceInfoArr = $this->Get_Student_Profile_Attendance_Data($Semester, $YearClassID);
		$AbsentDays = $AttendanceInfoArr[$StudentID]['Days Absent'];
		$AbsentDays = ($AbsentDays == '')? 0 : $AbsentDays;
		$AttendanceDay_eAtt = $TotalNumOfAttendanceDay - $AbsentDays;
		$AttendanceDay_CSV = trim($StudentOtherInfoDataArr['Attendance (days)']);
		$AttendanceDay = ($AttendanceDay_CSV != '')? $AttendanceDay_CSV : $AttendanceDay_eAtt;
		$AttendanceDay = ($AttendanceDay == '')? 0 : $AttendanceDay;
		
		# Co-curricular Activities
		$ClubYearTermIDArr = array();
		if ($ReportType == 'T') {
			$ClubYearTermIDArr[] = $Semester;
		}
		$ClubYearTermIDArr[] = 0;	// Whole Year Club
		$CocurricularActivities_eEnrol = implode(', ', Get_Array_By_Key($this->Get_eEnrolment_Data($StudentID, $ClubYearTermIDArr), 'ClubTitle'));
		$CocurricularActivities_CSV = trim($StudentOtherInfoDataArr['Co-curricular Activities']);
		$CocurricularActivities = ($CocurricularActivities_CSV != '')? $CocurricularActivities_CSV : $CocurricularActivities_eEnrol;
		$CocurricularActivities = ($CocurricularActivities == '')? $this->EmptySymbol : $CocurricularActivities;
		
		# Award
		$Awards = trim($StudentOtherInfoDataArr['Awards']);
		$Awards = ($Awards == '')? '&nbsp;' : $Awards;
		
		# Get Term Info
		$TermStartDate_Template = $ReportInfoArr['TermStartDate'];
		$TermStartDate_CSV = trim($StudentOtherInfoDataArr['Term Start Date (YYYY-MM-DD)']);
		$TermStartDate = ($TermStartDate_CSV != '')? $TermStartDate_CSV : $TermStartDate_Template;
		$TermStartDateDisplay = date('jS F, Y', strtotime($TermStartDate));
		
		$TermEndDate_Template = $ReportInfoArr['TermEndDate'];
		$TermEndDate_CSV = trim($StudentOtherInfoDataArr['Term End Date (YYYY-MM-DD)']);
		$TermEndDate = ($TermEndDate_CSV != '')? $TermEndDate_CSV : $TermEndDate_Template;
		$TermEndDateDisplay = date('jS F, Y', strtotime($TermEndDate));
		
		
		$PageHeight = 960;
		$SignatureRowHeight = 128;
		$StudentInfoHeight = $PageHeight - ($SignatureRowHeight * 2) - 70;
		
		
		$x = '';
		$x .= '<table style="width:100%;">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td style="height:'.$StudentInfoHeight.'px;" valign="top">'."\r\n";
					### Student Info Table
					$x .= '<table style="width:100%;" class="student_info_table font_10pt" valign="top">'."\r\n";
						$x .= '<tr><td colspan="4" class="font_14pt"><b>'.$eReportCard['Template']['StudentRecordEn'].'</b></td></tr>'."\r\n";
						// English Name and Class Name
						$x .= '<tr>'."\r\n";
							$x .= '<td style="width:24%;">'.$eReportCard['Template']['NameEn'].': </td>'."\r\n";
							$x .= '<td style="width:35%;" class="border_bottom">'.$StudentNameEn.'</td>'."\r\n";
							$x .= '<td style="width:15%;">'.$eReportCard['Template']['ClassEn'].': </td>'."\r\n";
							$x .= '<td style="width:26%;" class="border_bottom">'.$ClassDisplay.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Chinese Name and Student No
						$x .= '<tr>'."\r\n";
							$x .= '<td>&nbsp;</td>'."\r\n";
							$x .= '<td class="border_bottom">'.$StudentNameCh.'</td>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['StudentNoEn'].': </td>'."\r\n";
							$x .= '<td class="border_bottom">'.$StudentNo.'</td>'."\r\n";
						$x .= '<tr>'."\r\n";
						
						// ID Passport No and House Name
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['I.D./PassportNoEn'].': </td>'."\r\n";
							$x .= '<td class="border_bottom">'.$HKID.'</td>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['HouseEn'].': </td>'."\r\n";
							$x .= '<td class="border_bottom">'.$HouseName.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Date of Birth
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['DateOfBirthEn'].': </td>'."\r\n";
							$x .= '<td class="border_bottom">'.$DateOfBirth.'</td>'."\r\n";
							$x .= '<td>&nbsp;</td>'."\r\n";
							$x .= '<td>&nbsp;</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Sex
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['SexEn'].': </td>'."\r\n";
							$x .= '<td class="border_bottom">'.$Gender.'</td>'."\r\n";
							$x .= '<td>&nbsp;</td>'."\r\n";
							$x .= '<td>&nbsp;</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Attendance (days)
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['AttendanceEn'].'</td>'."\r\n";
							$x .= '<td colspan="3" class="border_bottom">'.$AttendanceDay.' / '.$TotalNumOfAttendanceDay.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Co-curricular Activities
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['CocurricularActivitiesEn'].'</td>'."\r\n";
							$x .= '<td colspan="3" class="border_bottom">'.$CocurricularActivities.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Awards
						$x .= '<tr>'."\r\n";
							$x .= '<td>'.$eReportCard['Template']['AwardsEn'].'</td>'."\r\n";
							$x .= '<td colspan="3" class="border_bottom">'.$Awards.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</table>'."\r\n";
					
					### Disciplinary Reminder(s)
					if ($FormLevelCode == 'H') {
						$ItemArr = $StudentOtherInfoDataArr['Items'];
						$ReminderArr = $StudentOtherInfoDataArr['No. of reminders'];
						if (!is_array($ItemArr)) {
							$ItemArr = array($ItemArr);
						}
						if (!is_array($ReminderArr)) {
							$ReminderArr = array($ReminderArr);
						}
						$numOfItem = count($ItemArr);
						
						$x .= '<table style="width:100%; border-collapse:collapse;" class="student_info_table font_10pt" valign="top">'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td colspan="2">'.$eReportCard['Template']['DisciplinaryRemindersEn'].'</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td class="border_bottom_thick" style="width:75%; text-align:center;">'.$eReportCard['Template']['ItemsEn'].'</td>'."\r\n";
								$x .= '<td class="border_bottom_thick" style="width:25%; text-align:center;">'.$eReportCard['Template']['NoOfRemindersEn'].'</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							if ($numOfItem == 0) {
								$x .= '<tr>'."\r\n";
									$x .= '<td>'.$eReportCard['Template']['Nil'].'</td>'."\r\n";
									$x .= '<td style="text-align:center;">/</td>'."\r\n";
								$x .= '</tr>'."\r\n";
							}
							else {
								for ($i=0; $i<$numOfItem; $i++) {
									$thisItem = $ItemArr[$i];
									$thisReminder = $ReminderArr[$i];
									
									$x .= '<tr>'."\r\n";
										$x .= '<td>'.$thisItem.'</td>'."\r\n";
										$x .= '<td style="text-align:center;">'.$thisReminder.'</td>'."\r\n";
									$x .= '</tr>'."\r\n";
								}
							}
						$x .= '</table>'."\r\n";
					}
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
			
					### Signature Table
					$ClassTeacherArr = $this->Get_Student_Class_Teacher_Info($StudentID);
					$numOfClassTeacher = count($ClassTeacherArr);
					$ClassTeacherDisplayArr = array();
					for ($i=0; $i<$numOfClassTeacher; $i++) {
						$ClassTeacherDisplayArr[] = '<br />('.$ClassTeacherArr[$i]['TitleEnglish'].' '.$ClassTeacherArr[$i]['EnglishName'].')';
					}
					
					
					$x .= '<table class="font_10pt" style="width:100%;">'."\r\n";
						$x .= '<tr style="height:'.$SignatureRowHeight.'px;">'."\r\n";
							if ($ClassTeacherDisplayArr[0] != '') {
								$x .= '<td class="border_top_thick" style="width:40%;vertical-align:top;">'.$eReportCard['Template']['HomeroomTeacherEn'].$ClassTeacherDisplayArr[0].'</td>'."\r\n";
							}
							else {
								$x .= '<td style="width:40%;">&nbsp;</td>'."\r\n";
							}
							$x .= '<td style="width:20%;">&nbsp;</td>'."\r\n";
							$x .= '<td class="border_top_thick" style="width:40%;vertical-align:top;">'.$eReportCard['Template']['ParentGuardianEn'].'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						$x .= '<tr style="height:'.$SignatureRowHeight.'px;">'."\r\n";
							if ($ClassTeacherDisplayArr[1] != '') {
								$x .= '<td class="border_top_thick" style="vertical-align:top;">'.$eReportCard['Template']['HomeroomTeacherEn'].$ClassTeacherDisplayArr[1].'</td>'."\r\n";
							}
							else {
								$x .= '<td>&nbsp;</td>'."\r\n";
							}
							$x .= '<td>&nbsp;</td>'."\r\n";
							$x .= '<td class="border_top_thick" style="vertical-align:top;">'.$eReportCard['Template']['PrincipalEn'].'<br />('.$eReportCard['Template']['PrincipalNameEn'].')</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
					### Term Range Info Display
					$TermRangeDisplay = $eReportCard['Template']['RecordCalculationRemarks'];
					$TermRangeDisplay = str_replace('<!--TermStartDate-->', $TermStartDateDisplay, $TermRangeDisplay);
					$TermRangeDisplay = str_replace('<!--TermEndDate-->', $TermEndDateDisplay, $TermRangeDisplay);
					$x .= '<i>'.$TermRangeDisplay.'</i>';
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Learner_Profile_Page_Table($ReportID, $StudentID) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		
		$SubjectID = 0;		// Get the Overall Expectation and Comment
		
		### Get Learner Profile
		$LearningProfileInfoArr = $this->returnCurriculumExpectation($ClassLevelID, $SubjectID, $Semester);
		$LearningProfileDisplay = nl2br(trim($LearningProfileInfoArr[$SubjectID]));
		$LearningProfileDisplay = ($LearningProfileDisplay=='')? $this->EmptySymbol : $LearningProfileDisplay;
		
		### Get Develop Children Goal
		$GoalArr = $this->Get_Student_Development_Goal_Array();
		$numOfGoal = count($GoalArr);
		
		### Get Comment on Personal and Social Development
		$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherCommentDisplay = nl2br(trim($CommentArr[$SubjectID]));
		$ClassTeacherCommentDisplay = ($ClassTeacherCommentDisplay=='')? $this->EmptySymbol : $ClassTeacherCommentDisplay;
		
		### Get Class Teacher Info
		$ClassTeacherDisplay = $this->Get_Class_Teacher_Display($StudentID);
		
		
		$x = '';
		$x .= $this->Get_Top_Right_Student_Info($StudentID);
		
		$x .= '<table class="report_border_double font_10pt" style="width:100%;" cellpadding="8" cellspacing="0">'."\r\n";
			$x .= '<tr><td class="title_background" style="text-align:center;"><b>'.$eReportCard['Template']['LearnerProfileEn'].'</b></td></tr>'."\r\n";
			$x .= '<tr><td class="border_top_double">'.$LearningProfileDisplay.'</td></tr>'."\r\n";
			$x .= '<tr><td class="border_top_double title_background" style="text-align:center;"><b>'.$eReportCard['Template']['ItIsOurGoalToDevelopChilderWhoAreEn'].'</b></td></tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td class="border_top_double">'."\r\n";
					$x .= '<table class="font_10pt" style="width:100%;">'."\r\n";
						$GoalCounter = 0;
						$numOfGoalPerRow = floor($numOfGoal / 2);
						$ColWidth = floor(100 / $numOfGoalPerRow);
						foreach ((array)$GoalArr as $thisGoalKey => $thisGoalDisplay) {
							if ($GoalCounter % $numOfGoalPerRow == 0) {
								$x .= '<tr>'."\r\n";
							}
							
								$x .= '<td style="text-align:center;">'.$thisGoalDisplay.'</td>'."\r\n";
								
							if (($GoalCounter + 1) % $numOfGoalPerRow == 0 || $GoalCounter == ($numOfGoalPerRow - 1)) {
								$x .= '</tr>'."\r\n";
							}
							$GoalCounter++;
						}
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr><td class="border_top_double title_background" style="text-align:center;"><b>'.$eReportCard['Template']['CommentOnPersonalAndSocialDevelopmentEn'].'</b></td></tr>'."\r\n";
			$x .= '<tr><td class="border_top_double" style="height:208px;vertical-align:top;"><div style="text-align:justify;">'.$ClassTeacherCommentDisplay.'</div></td></tr>'."\r\n";
			$x .= '<tr><td class="border_top_double" style="text-align:right;">'.$ClassTeacherDisplay.'</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Class_Teacher_Display($StudentID) {
		$ClassTeacherArr = $this->Get_Student_Class_Teacher_Info($StudentID);
		$numOfClassTeacher = count($ClassTeacherArr);
		$ClassTeacherDisplayArr = array();
		for ($i=0; $i<$numOfClassTeacher; $i++) {
			$ClassTeacherDisplayArr[] = $ClassTeacherArr[$i]['TitleEnglish'].' '.$ClassTeacherArr[$i]['EnglishName'];
		}
		$ClassTeacherDisplay = Get_Table_Display_Content(implode('<br />', $ClassTeacherDisplayArr));
		
		
		return $ClassTeacherDisplay;
	}
	
	private function Get_Top_Right_Student_Info($StudentID) {
		$StudentClassInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$StudentNameEn = ($StudentClassInfoArr[0]['EnglishName']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['EnglishName'];
		$ClassName = ($StudentClassInfoArr[0]['ClassName']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['ClassName'];
		$ClassNumber = ($StudentClassInfoArr[0]['ClassNumber']=='')? $this->EmptySymbol : $StudentClassInfoArr[0]['ClassNumber'];
		$StudentNo = ($StudentClassInfoArr[0]['UserLogin']=='')? $this->EmptySymbol : strtoupper($StudentClassInfoArr[0]['UserLogin']);
		
		$x = '';
		$x .= '<table style="width:100%;">'."\r\n";
			$x .= '<tr><td style="text-align:right;"><i>'.$StudentNameEn.'</i></td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:right;"><i>'.$ClassName.'('.$ClassNumber.')</i></td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:right;">'.$StudentNo.'</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Student_Development_Goal_Array() {
		global $eReportCard;
		
		$GoalArr = array();
		$GoalArr['Inquiries'] = $eReportCard['Template']['InquiriesEn'];
		$GoalArr['Knowledgeable'] = $eReportCard['Template']['KnowledgeableEn'];
		$GoalArr['Thinkers'] = $eReportCard['Template']['ThinkersEn'];
		$GoalArr['Communicator'] = $eReportCard['Template']['CommunicatorEn'];
		$GoalArr['Principled'] = $eReportCard['Template']['PrincipledEn'];
		$GoalArr['OpenMinded'] = $eReportCard['Template']['OpenMindedEn'];
		$GoalArr['Caring'] = $eReportCard['Template']['CaringEn'];
		$GoalArr['RiskTakers'] = $eReportCard['Template']['RiskTakersEn'];
		$GoalArr['WellBalanced'] = $eReportCard['Template']['WellBalancedEn'];
		$GoalArr['Reflective'] = $eReportCard['Template']['ReflectiveEn'];
		
		return $GoalArr;
	}
	
	public function Get_Grading_Scheme_Page_Table($ReportID, $StudentID) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=1, $returnAsso=0, $ReportID);
		$GradingSchemeIDArr = array_values(array_unique(Get_Array_By_Key($GradingSchemeInfoArr, 'SchemeID')));
		$numOfGradingScheme = count($GradingSchemeIDArr);
		
		$x = '';
		for ($i=0; $i<$numOfGradingScheme; $i++) {
			$thisSchemeID = $GradingSchemeIDArr[$i];
			
			$thisSchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($thisSchemeID);
			$thisSchemeName = $thisSchemeInfoArr['SchemeTitle'];
			
			$thisSchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($thisSchemeID);
			$thisNumOfRange = count($thisSchemeRangeInfoArr);
			
			$x .= '<div style="width:100%; text-align:center;" class="font_14pt"><b>'.$thisSchemeName.'</b></div>'."\r\n";
			$x .= '<table class="report_border_double font_10pt" style="width:100%; text-align:center;" cellpadding="8px" cellspacing="0px">'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td class="title_background" style="width:20%;"><b>'.$eReportCard['Template']['LevelEn'].'</b></td>'."\r\n";
					$x .= '<td class="title_background border_left_double" style="width:80%;"><b>'.$eReportCard['Template']['DescriptorsEn'].'</b></td>'."\r\n";
				$x .= '</tr>'."\r\n";
				for ($j=0; $j<$thisNumOfRange; $j++) {
					$thisGrade = Get_Table_Display_Content($thisSchemeRangeInfoArr[$j]['Grade']);
					$thisDescription = Get_Table_Display_Content($thisSchemeRangeInfoArr[$j]['Description']);
					
					$x .= '<tr>'."\r\n";
						$x .= '<td class="border_top_double title_background"><b>'.$thisGrade.'</b></td>'."\r\n";
						$x .= '<td class="border_top_double border_left_double">'.$thisDescription.'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			$x .= '</table>'."\r\n";
			$x .= $this->Get_Empty_Image_Div('50px')."\r\n";
		}
		
		return $x;
	}
	
	private function Get_Theme_For_Investigation_Page_Table($ReportID, $StudentID) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		if ($Semester == 'F') {
			$SemesterName = $eReportCard['Template']['WholeYearEn'];
		}
		else {
			$SemesterName = $this->returnSemesters($Semester, 'en');
		}
		
		$StudentClassInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$YearClassID = $StudentClassInfoArr[0]['ClassID'];
		
		$TeachingBlockArr = $this->Get_Teacher_Block_Stage_Array();
		$numOfTeachingBlock = count($TeachingBlockArr);
		
		# Get CSV Data
		$ThemeForInvestigationArr = $this->getOtherInfoData('themeForInvestigation', $Semester, $YearClassID);
		
		$LatestTerm = $this->Get_Last_Semester_Of_Report($ReportID);
		$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID, $StudentID);
		$StudentOtherInfoDataArr = $OtherInfoDataArr[$StudentID][$LatestTerm];
		
		$DisplayTeachingBlockArr = array();
		for ($i=0; $i<$numOfTeachingBlock; $i++) {
			$thisPrefix = $TeachingBlockArr[$i];
			$thisThemeKey = $thisPrefix.' teaching block - Transdisciplinary theme';
			$thisUnitTitleKey = $thisPrefix.' teaching block - Unit title';
			
			$thisTheme = trim($ThemeForInvestigationArr[$thisThemeKey]);
			$thisUnitTitle = trim($ThemeForInvestigationArr[$thisUnitTitleKey]);
			
			if ($thisTheme != '' || $thisUnitTitle != '') {
				$DisplayTeachingBlockArr[] = $thisPrefix;
			}
		}
		$numOfDisplayPrefix = count($DisplayTeachingBlockArr);
		
		### Get Class Teacher Info
		$ClassTeacherDisplay = $this->Get_Class_Teacher_Display($StudentID);
		
		
		$x = '';
		$x .= $this->Get_Top_Right_Student_Info($StudentID);
		$x .= '<table class="report_border_double font_10pt" style="width:100%;" cellpadding="2" cellspacing="0">'."\r\n";
			$x .= '<tr><td colspan="'.$numOfDisplayPrefix.'" class="title_background font_12pt" style="text-align:center;">'.$eReportCard['Template']['ThemeForInvestigationEn'].'</td></tr>'."\r\n";
			$x .= '<tr><td colspan="'.$numOfDisplayPrefix.'" class="title_background font_11pt" style="text-align:center;"><b>'.$SemesterName.'</b></td></tr>'."\r\n";
			
			# Block Title
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfDisplayPrefix; $i++) {
					$thisPrefix = $DisplayTeachingBlockArr[$i];
					$thisDisplay = str_replace('<!--Sequence-->', $thisPrefix, $eReportCard['Template']['TeachingBlockDisplayEn']);
					$thisBorderLeft = ($i==0)? '' : 'border_left';
					
					$x .= '<td class="border_top_double '.$thisBorderLeft.'" style="text-align:center; padding-top:10px;"><b><i>'.$thisDisplay.'</i></b></td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			# Transdisciplinary theme
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfDisplayPrefix; $i++) {
					$thisBorderLeft = ($i==0)? '' : 'border_left';
					$x .= '<td class="border_top_double '.$thisBorderLeft.'">'.$eReportCard['Template']['TransdisciplinaryThemeEn'].'</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfDisplayPrefix; $i++) {
					$thisPrefix = $DisplayTeachingBlockArr[$i];
					$thisKey = $thisPrefix.' teaching block - Transdisciplinary theme';
					$thisDisplay = Get_Table_Display_Content($ThemeForInvestigationArr[$thisKey]);
					$thisBorderLeft = ($i==0)? '' : 'border_left';
					
					$x .= '<td class="'.$thisBorderLeft.'"><b>'.$thisDisplay.'</b></td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			# Unit Title
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfDisplayPrefix; $i++) {
					$thisBorderLeft = ($i==0)? '' : 'border_left';
					$x .= '<td class="border_top_double '.$thisBorderLeft.'">'.$eReportCard['Template']['UnitTitleEn'].'</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfDisplayPrefix; $i++) {
					$thisPrefix = $DisplayTeachingBlockArr[$i];
					$thisKey = $thisPrefix.' teaching block - Unit title';
					$thisDisplay = Get_Table_Display_Content($ThemeForInvestigationArr[$thisKey]);
					$thisBorderLeft = ($i==0)? '' : 'border_left';
					
					$x .= '<td class="'.$thisBorderLeft.'"><b>'.$thisDisplay.'</b></td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			# Overview of student's performance
			$ColumnHeight = 500;
			$x .= '<tr><td colspan="'.$numOfDisplayPrefix.'" class="border_top_double title_background" style="text-align:center;">'.$eReportCard['Template']['OverviewOfStudentPerformanceEn'].'</td></tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				for ($i=0; $i<$numOfDisplayPrefix; $i++) {
					$thisPrefix = $DisplayTeachingBlockArr[$i];
					$thisKey = $thisPrefix.' teaching block performance';
					$thisDisplay = Get_Table_Display_Content($StudentOtherInfoDataArr[$thisKey]);
					$thisBorderLeft = ($i==0)? '' : 'border_left';
					
					$x .= '<td class="border_top_double '.$thisBorderLeft.'" style="height:'.$ColumnHeight.'px; vertical-align:top;">'.$thisDisplay.'</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
			
			# Effort and Class Teacher's Name
			$x .= '<tr>'."\r\n";
				$x .= '<td class="border_top_double">'.$eReportCard['Template']['EffortIndicatorEn'].' : '.$StudentOtherInfoDataArr['TFI_Effort'].'</td>'."\r\n";
				$x .= '<td colspan="'.($numOfDisplayPrefix - 1).'" class="border_top_double" style="text-align:right;">'.$ClassTeacherDisplay.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function Get_Teacher_Block_Stage_Array() {
		$TeachingBlockArr = array();
		$TeachingBlockArr[] = 'First';
		$TeachingBlockArr[] = 'Second';
		$TeachingBlockArr[] = 'Third';
		$TeachingBlockArr[] = 'Forth';
		$TeachingBlockArr[] = 'Fifth';
		$TeachingBlockArr[] = 'Sixth';
		
		return $TeachingBlockArr;
	}
	
	private function Get_Subject_Pages_Table_Array($ReportID, $StudentID) {
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		
		$MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		$SubjectPageInfoArr = array();
		$SubjectPageCounter = 0;
		foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
			$SubjectDisplayLang = $this->GetFormSubjectDisplayLang($thisSubjectID, $ClassLevelID, $ReportID);
			if ($SubjectDisplayLang == 'ch') {
				$ParLang = 'ch';
			}
			else {
				$ParLang = 'en';
			}
			
			# Get Subject Component
			$CmpSubjectInfoArr = $this->GET_COMPONENT_SUBJECT($thisSubjectID, $ClassLevelID, $ParLang, $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
			$numOfCmpSubject = count($CmpSubjectInfoArr);
			
			if ($numOfCmpSubject == 0) {
				continue;
			}
			
			$SubjectPageArr[$SubjectPageCounter]['SubjectID'] = $thisSubjectID; 
			$SubjectPageArr[$SubjectPageCounter]['html'] = $this->Get_Subject_Page_Table($ReportID, $StudentID, $thisSubjectID, $CmpSubjectInfoArr);
			$SubjectPageCounter++;
		}
		
		return $SubjectPageArr;
	}
	
	private function Get_Subject_Page_Table($ReportID, $StudentID, $SubjectID, $CmpSubjectInfoArr) {
		global $eReportCard;
		
		$numOfCmpSubject = count($CmpSubjectInfoArr);
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$FormLevelCode = $this->Get_Student_Form_Level_Code($ClassLevelID);
		
		### Get Subject Name
		$SubjectNameEn = $this->GET_SUBJECT_NAME_LANG($SubjectID, $ParLang='en', $ParShortName=0, $ParAbbrName=0);
		$SubjectNameCh = $this->GET_SUBJECT_NAME_LANG($SubjectID, $ParLang='ch', $ParShortName=0, $ParAbbrName=0);
		$GradingSchemeInfoArr = $this->Get_Form_Grading_Scheme($ClassLevelID, $ReportID);
		$ScaleInput = $GradingSchemeInfoArr[$SubjectID]['ScaleInput'];
		$ScaleDisplay = $GradingSchemeInfoArr[$SubjectID]['ScaleDisplay'];
		$LangDisplay = $GradingSchemeInfoArr[$SubjectID]['LangDisplay'];
		if ($LangDisplay == 'ch') {
			$LangPrefix = 'Ch';
			$SubjectName = $SubjectNameCh;
		}
		else {
			$LangPrefix = 'En';
			$SubjectName = $SubjectNameEn;
		}
		
		### Get Descriptor from Grading Scheme
		$SchemeID = $GradingSchemeInfoArr[$SubjectID]['SchemeID'];
		$GradingSchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
		$numOfRange = count($GradingSchemeRangeInfoArr);
		$GradeDescriptionAssoArr = array();
		for ($i=0; $i<$numOfRange; $i++) {
			$thisGrade = $GradingSchemeRangeInfoArr[$i]['Grade'];
			$thisDescription = $GradingSchemeRangeInfoArr[$i]['Description'];
			
			$GradeDescriptionAssoArr[$thisGrade] = $thisDescription;
		}
		
		
		### Get Subject Description
		$SubjectDescriptionAssoArr = $this->returnCurriculumExpectation($ClassLevelID, $SubjectID, $Semester);
		$SubjectDescription = Get_Table_Display_Content(nl2br($SubjectDescriptionAssoArr[$SubjectID]));
		
		### Get Subject Teacher Comment
		$CommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$SubjectTeacherComment = Get_Table_Display_Content(nl2br(trim($CommentArr[$SubjectID])));
		
		### Get Effort
		$EffortInfoArr = $this->GET_EXTRA_SUBJECT_INFO(array($StudentID), $SubjectID, $ReportID);
		$Effort = Get_Table_Display_Content($EffortInfoArr[$StudentID]['Info']);
				
		### Get Subject Teacher Info
		$StudentClassInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$YearClassID = $StudentClassInfoArr[0]['ClassID'];
		$SubjectTeacherInfoArr = $this->returnSubjectTeacher($YearClassID, $SubjectID, $ReportID, $StudentID, $FullTeacherInfo=1);
		$numOfSubjectTeacher = count($SubjectTeacherInfoArr);
		$SubjectTeacherDisplayArr = array();
		for ($i=0; $i<$numOfSubjectTeacher; $i++) {
			$SubjectTeacherDisplayArr[] = $SubjectTeacherInfoArr[$i]['TitleEnglish'].' '.$SubjectTeacherInfoArr[$i]['EnglishName'];
		}
		$SubjectTeacherDisplay = Get_Table_Display_Content(implode('<br />', $SubjectTeacherDisplayArr));
		
		$Colspan = ($FormLevelCode=='L')? 3 : 4;
		
		$x = '';
		$x .= $this->Get_Top_Right_Student_Info($StudentID);
		
		$x .= '<table class="report_border_double font_10pt" style="width:100%;" cellpadding="5" cellspacing="0">'."\r\n";
			# Subject Name
			$x .= '<tr><td colspan="'.$Colspan.'" class="title_background" style="padding-top:10px; padding-bottom:10px;">'.$eReportCard['Template']['Subject'.$LangPrefix].' : '.$SubjectName.'</td></tr>'."\r\n";
			
			if ($FormLevelCode == 'L') {
				### Get SubMS Column Data ans Student Score
				$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
				$FirstReportColumnID = $ReportColumnInfoArr[0]['ReportColumnID'];
				$SubMSColumnInfoAssoArr = array();
				$SubMSStudentScoreAssoArr = array();
				for ($i=0; $i<$numOfCmpSubject; $i++) {
					$thisCmpSubjectID = $CmpSubjectInfoArr[$i]['SubjectID'];
					$SubMSColumnInfoAssoArr[$thisCmpSubjectID] = $this->GET_ALL_SUB_MARKSHEET_COLUMN($FirstReportColumnID, $thisCmpSubjectID);
					$SubMSStudentScoreAssoArr[$thisCmpSubjectID] = $this->GET_SUB_MARKSHEET_SCORE($FirstReportColumnID, $thisCmpSubjectID);
				}
				
				
				# Subject Description
				$x .= '<tr><td colspan="'.$Colspan.'" class="border_top_double"><div style="text-align:justify;">'.$SubjectDescription.'</div></td></tr>'."\r\n";
				
				# Component and SubMS Info
				$x .= '<tr>'."\r\n";
					$x .= '<td class="title_background border_top_double" style="width:25%; text-align:center;">'.strtoupper($eReportCard['Template']['Strands'.$LangPrefix]).'</td>'."\r\n";
					$x .= '<td class="title_background border_top_double border_left" style="width:60%; text-align:center;">'.strtoupper($eReportCard['Template']['Criteria'.$LangPrefix]).'</td>'."\r\n";
					$x .= '<td class="title_background border_top_double border_left" style="width:15%; text-align:center;">'.strtoupper($eReportCard['Template']['ProgressLevel'.$LangPrefix]).'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				for ($i=0; $i<$numOfCmpSubject; $i++) {
					$thisCmpSubjectID = $CmpSubjectInfoArr[$i]['SubjectID'];
					$thisCmpSubjectName = $CmpSubjectInfoArr[$i]['SubjectName'];
					$thisCmpSubMSColumnInfoArr = $SubMSColumnInfoAssoArr[$thisCmpSubjectID];
					$thisNumOfSubMSColumn = count($thisCmpSubMSColumnInfoArr);
					
					for ($j=0; $j<$thisNumOfSubMSColumn; $j++) {
						$thisColumnID = $thisCmpSubMSColumnInfoArr[$j]['ColumnID'];
						$thisColumnTitle = Get_Table_Display_Content($thisCmpSubMSColumnInfoArr[$j]['ColumnTitle']);
						$thisSubMSScore = Get_Table_Display_Content($SubMSStudentScoreAssoArr[$thisCmpSubjectID][$StudentID][$thisColumnID]);
						
						if ($i==0 && $j==0) {
							$border_top = 'border_top_double';
						}
						else {
							$border_top = 'border_top';
						}
						
						if ($j==0) {
							$thisCmpSubjectNameDisplay = Get_Table_Display_Content($thisCmpSubjectName);
						}
						else {
							$thisCmpSubjectNameDisplay = '&nbsp;';
						}
						
						$x .= '<tr>'."\r\n";
							$x .= '<td class="'.$border_top.'" style="vertical-align:top;">'.$thisCmpSubjectNameDisplay.'</td>'."\r\n";
							$x .= '<td class="'.$border_top.' border_left">'.$thisColumnTitle.'</td>'."\r\n";
							$x .= '<td class="'.$border_top.' border_left" style="text-align:center;"><b>'.$thisSubMSScore.'</b></td>'."\r\n";
						$x .= '</tr>'."\r\n";
					}
				}
			}
			else {
				$x .= '<tr>'."\r\n";
					$x .= '<td class="title_background border_top_double" style="width:20%; text-align:center;">'.$eReportCard['Template']['Criterion'.$LangPrefix].'</td>'."\r\n";
					$x .= '<td class="title_background border_top_double border_left" style="width:45%; text-align:center;">'.$eReportCard['Template']['Descriptor'.$LangPrefix].'</td>'."\r\n";
					$x .= '<td class="title_background border_top_double border_left" style="width:20%; text-align:center;">'.$eReportCard['Template']['HighestPossibleLevelOfAchievement'.$LangPrefix].'</td>'."\r\n";
					$x .= '<td class="title_background border_top_double border_left" style="width:15%; text-align:center;">'.$eReportCard['Template']['LevelAchievedByStudent'.$LangPrefix].'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				
				$ReportColumnID = 0;
				$MarkArr = $this->getMarks($ReportID, '', '', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', $ReportColumnID, $CheckPositionDisplay=0, $SubOrderArr='');
				for ($i=0; $i<$numOfCmpSubject; $i++) {
					$thisCmpSubjectID = $CmpSubjectInfoArr[$i]['SubjectID'];
					$thisCmpSubjectName = $CmpSubjectInfoArr[$i]['SubjectName'];
					
					// ascii of "A" start from 65
					$thisCriterion = chr($i + 65);
					$thisCmpSubjectNameDisplay = '';
					$thisCmpSubjectNameDisplay .= $eReportCard['Template']['Criterion'.$LangPrefix].' '.$thisCriterion.':';
					$thisCmpSubjectNameDisplay .= '<br />';
					$thisCmpSubjectNameDisplay .= $thisCmpSubjectName;
					
					$thisFullMark = $GradingSchemeInfoArr[$thisCmpSubjectID]['FullMark'];
					
					$thisMark = $MarkArr[$StudentID][$thisCmpSubjectID][$ReportColumnID]['Mark'];
					$thisGrade = $MarkArr[$StudentID][$thisCmpSubjectID][$ReportColumnID]['Grade'];
					
					# for preview purpose
					if(!$StudentID)	{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisCmpSubjectID, $thisMark, $MarkArr[$StudentID][$thisCmpSubjectID][$ReportColumnID]['Grade']);
					
					if($needStyle) {
						$thisMarkTemp = $thisMark;
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisCmpSubjectID, $thisMarkTemp);
					}
					else {
						$thisMarkDisplay = $thisMark;
					}
					
					//$thisCmpSubjectComment = Get_Table_Display_Content(nl2br(trim($CommentArr[$thisCmpSubjectID])));
					$thisDescription = Get_Table_Display_Content(nl2br(trim($GradeDescriptionAssoArr[$thisGrade])));
					
					$x .= '<tr>'."\r\n";
						$x .= '<td class="border_top_double" style="text-align:center;">'.$thisCmpSubjectNameDisplay.'</td>'."\r\n";
						$x .= '<td class="border_top_double border_left">'.$thisDescription.'</td>'."\r\n";
						$x .= '<td class="border_top_double border_left" style="text-align:center;">'.$thisFullMark.'</td>'."\r\n";
						$x .= '<td class="border_top_double border_left" style="text-align:center;"><b>'.$thisMarkDisplay.'</b></td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			}
			
			
			# Subject Teacher Comment
			$x .= '<tr><td colspan="'.$Colspan.'" class="border_top_double">'.$eReportCard['Template']['Comment'.$LangPrefix].' :</td></tr>'."\r\n";
			$x .= '<tr><td colspan="'.$Colspan.'" style="padding-top:0px;"><div style="text-align:justify;">'.$SubjectTeacherComment.'</div></td></tr>'."\r\n";
			
			# Effort and Class Teacher's Name
			$Colspan = ($FormLevelCode=='L')? '' : 'colspan="2"';
			$x .= '<tr>'."\r\n";
				$x .= '<td '.$Colspan.' class="border_top_double" style="vertical-align:top;">'.$eReportCard['Template']['EffortIndicator'.$LangPrefix].' : '.$Effort.'</td>'."\r\n";
				$x .= '<td colspan="2" class="border_top_double" style="text-align:right; vertical-align:top;">'.$SubjectTeacherDisplay.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";

		return $x;
	}
	
	private function Get_Learning_Attitudes_By_Subject_Page_Table($ReportID, $StudentID, $SubjectIDArr) {
		global $eReportCard;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		$SubjectPersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID);
		$PersonalCharIDArr = array_values(array_unique(Get_Array_By_Key($SubjectPersonalCharInfoArr, 'CharID')));
		$PersonalCharInfoArr = $this->returnPersonalCharData($PersonalCharIDArr);
		$PersonalCharIDArr = Get_Array_By_Key($PersonalCharInfoArr, 'CharID');	// Mark Sure the ordering of the Personal Char is correct
		$numOfPersonalChar = count($PersonalCharInfoArr);
		
		// $PersonalCharDataArr[$ReportID][$SubjectID][$StudentID][$CharID] = $GradeCode;
		$PersonalCharDataArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentID, $ReportID, '', $ReturnGradeCode=1, $ReturnCharID=1, $ReturnComment=1);
		
		### Check Display Subject
		// $SubjectIDArr is the SubjectID which displayed in the Academic Result pages
		// And add the Subject which have Personal Char in $DisplaySubject
		$DisplaySubjectIDArr = $SubjectIDArr;
		$MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
			$thisShowSubject = false;
			for ($i=0; $i<$numOfPersonalChar; $i++) {
				$thisCharID = $PersonalCharInfoArr[$i]['CharID'];
				$thisGradeCode = $PersonalCharDataArr[$ReportID][$thisSubjectID][$StudentID][$thisCharID];
				
				if ($thisGradeCode != '' && $thisGradeCode != 'N/A') {
					$thisShowSubject = true;
					break;
				}
			}
			
			if ($thisShowSubject) {
				$DisplaySubjectIDArr[] = $thisSubjectID;
			}
		}
		
		
		$WidthSubject = 23;
		$WidthComment = 23;
		$WidthPersonalChar = floor((100 - $WidthSubject - $WidthComment) / $numOfPersonalChar);
		
		$x = '';
		$x .= $this->Get_Top_Right_Student_Info($StudentID);
		$x .= '<div class="font_12pt" style="width:100%; text-align:center;">'.$eReportCard['Template']['LearningAttitudes&ApproachesChecklistEn'].'</div>'."\n";
		$x .= '<table class="report_border font_10pt" style="width:100%;" cellpadding="4" cellspacing="0">'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td style="width:'.$WidthSubject.'%;">&nbsp;</td>'."\r\n";
				for ($i=0; $i<$numOfPersonalChar; $i++) {
					$thisPersonalCharName = $PersonalCharInfoArr[$i]['Title_EN'];
					$x .= ' <!--[if IE]>
							<td class="border_right vertical_text_ie font_10pt" style="width:'.$WidthPersonalChar.'%; height:200px;">'.$thisPersonalCharName.'</td>
							<![endif]-->
							<!--[if !IE]><!-->
							<td class="border_left font_10pt" style="width:'.$WidthPersonalChar.'%;"><span class="vertical_text_ff">'.$thisPersonalCharName.'</span></td>
							<!--<![endif]-->
							';
				}
				
				$x .= ' <!--[if IE]>
						<td class="border_right vertical_text_ie font_10pt" style="width:'.$WidthComment.'%;">'.$eReportCard['Template']['OtherCommentsEn'].'</td>
						<![endif]-->
						<!--[if !IE]><!-->
						<td class="border_left font_10pt" style="width:'.$WidthComment.'%;"><span class="vertical_text_ff">'.$eReportCard['Template']['OtherCommentsEn'].'</span></td>
						<!--<![endif]-->
						';
			$x .= '</tr>'."\r\n";
			
			$numOfSubject = count((array)$DisplaySubjectIDArr);
			foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
				if (!in_array($thisSubjectID, (array)$DisplaySubjectIDArr)) {
					continue;
				}
				
				$SubjectNameEn = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, $ParLang='en', $ParShortName=0, $ParAbbrName=0);
				
				$x .= '<tr>'."\r\n";
					// Subject Name
					$x .= '<td class="border_top">'.$SubjectNameEn.'</td>'."\r\n";
					
					// Subject Personal Char Data
					for ($j=0; $j<$numOfPersonalChar; $j++) {
						$thisCharID = $PersonalCharInfoArr[$j]['CharID'];
						$thisGradeCode = $PersonalCharDataArr[$ReportID][$thisSubjectID][$StudentID][$thisCharID];
						$thisGradeCode = ($thisGradeCode=='')? 'N/A' : $thisGradeCode;
						
						$x .= '<td class="border_left border_top" style="text-align:center;"><b>'.$thisGradeCode.'</b></td>'."\r\n";
					}
					
					// Other Comments
					$CommentDisplay = Get_Table_Display_Content(nl2br(trim($PersonalCharDataArr[$ReportID][$thisSubjectID][$StudentID]['comment'])));
					$x .= '<td class="border_left border_top">'.$CommentDisplay.'</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			}
		$x .= '</table>'."\r\n";
		
		// Scale Appendix
		$PersonalCharScaleArr = $this->returnPersonalCharOptions($br=0, $order='', $Lang_Par='en', $ReturnNormalTeacherOnly=0);
		$x .= '<table class="font_8pt" style="width:100%; cellpadding="0" cellspacing="0">'."\r\n";
			foreach((array)$PersonalCharScaleArr as $thisScaleCode => $thisScaleDisplay) {
				if ($thisScaleCode == 'N/A' || $thisScaleCode == 'Nil') {
					continue;
				}
				
				$thisDisplay = $thisScaleCode.' = '.$thisScaleDisplay;
				$x .= '<tr><td style="padding-left:5px;">'.$thisDisplay.'</td></tr>'."\r\n";
			}
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	// return "L" for Lower Form, "H" for Higher Form
	private function Get_Student_Form_Level_Code($ClassLevelID) {
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
		if ($FormNumber <= 3) {
			return "L";
		}
		else {
			return "H";
		}
	}
}
?>