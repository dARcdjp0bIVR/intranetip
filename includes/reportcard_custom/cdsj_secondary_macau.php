<?php
# Editing by ivan

include_once($intranet_root."/lang/reportcard_custom/$ReportCardCustomSchoolName.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary","attendance", "merit", "remark", "eca");

		// [2016-0406-1633-32096]
		//$this->OtherInfoInGrandMS = array("Conduct", "FinalComments");
		$this->OtherInfoInGrandMS = array("Conduct", "TimesLate", "ExcusedAbsencePeriods", "UnexcusedAbsencePeriods", "FinalComments");

		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;

		$this->EmptySymbol = '--';
	}

	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;

		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>&nbsp;</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";

		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		//$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";

		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				$x .= "<tr height='920px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}

	// function getReportHeader($ReportID, $StudentID)
	function getReportHeader($ReportID, $StudentID='')
	{
		$html = '';
		
		//2013-0411-1229-44156
//		$html .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
//		$html .= "<tr><td style='height:100px;'>&nbsp;</td></tr>";
//		$html .= "</table>";
		
		return $html;
	}

	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;

		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitleArr = explode(":_:", $ReportTitle);
			
			### Ignore the empty second line
			if ($ReportTitleArr[1] == '')
				$ReportTitle = $ReportTitleArr[0];
			else
				$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			$SemID = $ReportSetting['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";

			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			
			$DateOfIssue = $ReportSetting['Issued'];
			
			if (date( $DateOfIssue)==0) {
			    $data['DateOfIssue'] = '';
			} else {
			    $data['DateOfIssue'] = date("d/m/Y", strtotime($DateOfIssue));
			}  

			if($StudentID)		# retrieve Student Info
			{			
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			
				$StudentNameEn = $StudentInfoArr[0]['EnglishName'];						
				$StudentNameCh = $StudentInfoArr[0]['ChineseName'];
				$data['Name'] = $StudentNameCh.' '.$StudentNameEn;
				
				$data['STRN'] = $StudentInfoArr[0]['STRN'];		
				$data['HKID'] = $StudentInfoArr[0]['HKID'];
				
				$ClassNameCh = $StudentInfoArr[0]['ClassNameCh'];
				$ClassNameEn = $StudentInfoArr[0]['ClassName'];
				$ClassNumber = $StudentInfoArr[0]['ClassNumber'];	
				$data['Class'] = $ClassNameCh.' '.$ClassNameEn.' ('.$ClassNumber.')';
				
				if(strtoupper($StudentInfoArr[0]['Gender'])=='M') {
					$GenderDisplay = $eReportCard['Template']['StudentInfo']['MaleCh'].' '.$eReportCard['Template']['StudentInfo']['MaleEn'];
				}
				else if(strtoupper($StudentInfoArr[0]['Gender'])=='F') {
					$GenderDisplay = $eReportCard['Template']['StudentInfo']['FemaleCh'].' '.$eReportCard['Template']['StudentInfo']['FemaleEn'];
				}
	
				$DateOfBirth =  $StudentInfoArr[0]['DateOfBirth']; 
		
				if (date( $DateOfBirth)==0) {
				    $data['DateOfBirth'] = '';
				} else {
				    $data['DateOfBirth'] = date("d/m/Y", strtotime($DateOfBirth));
				}  
				$data['WebSAMS'] = str_replace('#', '', $StudentInfoArr[0]['WebSAMSRegNo']);
			}
			else
			{
				$GenderDisplay = $defaultVal;
				$data['Name'] = $defaultVal;
				$data['ClassNo'] = $defaultVal;
				$data['Class'] = $defaultVal;
				$data['Gender'] = $defaultVal;
				$data['STRN'] = $defaultVal;
				$data['WebSAMS'] = $defaultVal;
				$data['HKID'] = $defaultVal;
			}
		
			//$PhotoImgPath = $this->GetStudentPhotoPath($StudentID);
			//$PhotoImg = '<img src="'.$PhotoImgPath.'" />';
			$PhotoImg = $this->Get_Student_Photo_Image($StudentID);
			
			$textAlign = " style='text-align:left;'";
			
			if($ReportType=='T')
			{
				$fontSize = "font_10pt";
			}
			else if($ReportType=='W')
			{
				$fontSize = "font_8pt";
			}
			
			$StudentInfoTable = '';
			$StudentInfoTable .= "<table class='$fontSize' width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
				$StudentInfoTable .= "<col width='13%'/>
									  <col width='12%'/>
									  <col width='2%'/>
									  <col width='25%'/>
									  <col width='13%'/>
									  <col width='13%'/>
									  <col width='2%'/>
									  <col width='20%'/>
									 ";
				
				$StudentInfoTable .= "<tr>";
					# Name
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['NameCh']."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['NameEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign colspan='5'>".$data['Name']."</td>";
			
				$StudentInfoTable .= "</tr>";
				
				$StudentInfoTable .= "<tr>";
					# Class
//					if ($ReportType == 'T') {
//						$thisTitleCh = str_replace(' ', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $eReportCard['Template']['StudentInfo']['ClassCh']);
//					}
//					else {
//						$thisTitleCh = str_replace(' ', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $eReportCard['Template']['StudentInfo']['ClassCh']);
//					}
					$thisTitleCh = str_replace(' ', '　　', $eReportCard['Template']['StudentInfo']['ClassCh']);
					
					$StudentInfoTable .= "<td $textAlign>".$thisTitleCh."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['ClassEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign>".$data['Class']."</td>";
					
					# Sex
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['GenderCh']."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['GenderEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign>".$GenderDisplay."</td>";
				$StudentInfoTable .= "</tr>";
				
				$StudentInfoTable .= "<tr>";
					# Student No.
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['StudentNoCh']."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['StudentNoEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign>".$data['STRN']."</td>";
					
					# DESJ No.
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['WebSAMSCh']."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['WebSAMSEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign>".$data['WebSAMS']."</td>";
				$StudentInfoTable .= "</tr>";
				
				$StudentInfoTable .= "<tr>";
					# ID
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['HKIDCh']."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['HKIDEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign>".$data['HKID']."</td>";
					
					# Date of Issue
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['DateOfIssueCh']."</td>";
					$StudentInfoTable .= "<td $textAlign>".$eReportCard['Template']['StudentInfo']['DateOfIssueEn']."</td>";
					$StudentInfoTable .= "<td>:</td>";
					$StudentInfoTable .= "<td $textAlign>".$data['DateOfIssue']."</td>";
				$StudentInfoTable .= "</tr>";
				
			$StudentInfoTable .= "</table>";
			
			if($ReportType=='T')
			{
				$PhotoImg = "";
				$stdColspan='3';
			}
			else if($ReportType=='W')
			{
				$stdColspan='2';
			}
			$html = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
				$html .= "<col width='15%'/>
						  <col width='70%'/>
						  <col width='15%'/>
						 ";
				$html .="<tr style='height:112px;'>";
					$html .="<td>&nbsp;</td>";
					$html .="<td class='font_12pt' style='vertical-align:bottom;'>$ReportTitle</td>";
					//2013-0607-0937-50073
					//$html .="<td rowspan='2' style='vertical-align:top; text-align:right;'>".$this->Get_Empty_Image_Div('33px')."$PhotoImg</td>";
					$html .="<td rowspan='2' style='vertical-align:top; text-align:right;'>$PhotoImg</td>";
				$html .="</tr>";
				$html .="<tr>";
					$html .="<td colspan='$stdColspan'>$StudentInfoTable</td>";
				$html .="</tr>";
			$html .="</table>";
			
		}
		return $html;
	}

	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;

		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";

		# define
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;

		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		
		$SubjectIDArray = array();
		if (sizeof($SubjectArray) > 0)
		{
			foreach($SubjectArray as $thisSubjectID => $SubjectNameArray[])
			{
				$SubjectIDArray[] = $thisSubjectID;			
			}
		}
			
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		$isAllColumnZeroWeightAry = $MSTableReturned['isAllColumnZeroWeight'];
		
		
		$SubjectCol = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllColumnZeroWeightAry);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);

		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		

		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];

			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0)
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($isSub == true)
				continue;

			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
				
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}

		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);

		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################

		return $DetailsTable;
	}

	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;

		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
		$SemesterSequence = $this->Get_Semester_Seq_Number($SemID);
		$ReportType = $SemID == "F" ? "W" : "T";
		$LineHeight = $ReportSetting['LineHeight'];
		

		$font_class = ($ReportType=='T')? 'mstable_header_term' : 'mstable_header_consolidate';
	
		### Get Column Weight
		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ColumnData);
		
		$ColumnWeightArr = array();
		if ($ReportType == 'T')
		{
			for($i=0; $i<$numOfColumn; $i++) {
				$ColumnWeightArr[] = $ColumnData[$i]["DefaultWeight"] * 100;
			}
		}
		else
		{
			for($i=0; $i<$numOfColumn; $i++)
			{
				$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
				$thisReportID = $thisReport['ReportID'];
				$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
				
				$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
				$thisNumOfColumn = count($thisColumnData);
				for($j=0; $j<$thisNumOfColumn; $j++) {
					$ColumnWeightArr[] = $thisColumnData[$j]["DefaultWeight"] * 100;
				}
			}
		}
		
		if ($ReportType == 'T')
		{
			$SubjectWidth = '50%';
			$UnitWidth = '10%';
			$MarkWidth = '20%';
		}
		else
		{
			//2014-0620-0929-12194
//			$SubjectWidth = '40%';
//			$UnitWidth = '6%';
//			$MarkWidth = '10%';
//			$FinalWidth = '14%';
			$SubjectWidth = '46%';
			$UnitWidth = '6%';
			$MarkWidth = '10%';
			$FinalWidth = '8%';
		}

		$x = '';
		$x .= '<tr>';
			### Subject
			$thisTitle = $eReportCard['Template']['SubjectCh'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['SubjectEn'];
			$x .= "<td colspan='2' rowspan='2' valign='middle' align='center' class='{$font_class}' height='{$LineHeight}' width='".$SubjectWidth."'>". $thisTitle . "</td>";
				
			### Units
			$thisTitle = $eReportCard['Template']['UnitCh'].'<br />'.$eReportCard['Template']['UnitEn'];
			$x .= "<td rowspan='2' valign='middle' align='center' class='{$font_class} border_left' height='{$LineHeight}' width='".$UnitWidth."'>". $thisTitle . "</td>";
			
			### 1st Term / 2nd Term
			if ($ReportType == 'T') {
				if ($SemesterSequence == 1) {
					$thisTitle = $eReportCard['Template']['1stTermCh'].' '.$eReportCard['Template']['1stTermEn'];
				}
				else {
					$thisTitle = $eReportCard['Template']['2ndTermCh'].' '.$eReportCard['Template']['2ndTermEn'];
				}
				$x .= "<td colspan='2' valign='middle' align='center' class='{$font_class} border_left' height='{$LineHeight}'>". $thisTitle . "</td>";
			}
			else if ($ReportType == 'W') {
				### 1st Term
				$thisTitle = $eReportCard['Template']['1stTermCh'].' '.$eReportCard['Template']['1stTermEn'];
				$x .= "<td colspan='2' valign='middle' align='center' class='{$font_class} border_left' height='{$LineHeight}'>". $thisTitle . "</td>";
				
				### 2nd Term
				$thisTitle = $eReportCard['Template']['2ndTermCh'].' '.$eReportCard['Template']['2ndTermEn'];
				$x .= "<td colspan='2' valign='middle' align='center' class='{$font_class} border_left' height='{$LineHeight}'>". $thisTitle . "</td>";
				
				### Final
				$thisTitle = $eReportCard['Template']['FinalCh'].' '.$eReportCard['Template']['FinalEn'].'<br />100%';
				$x .= "<td rowspan='2' colspan='2' valign='middle' align='center' class='{$font_class} border_left' height='{$LineHeight}'>". $thisTitle . "</td>";
			}
		$x .= '</tr>';
			
		if($ReportType=='W')
		{
			$break_html='<br/>';
		}
		else if($ReportType=='T')
		{
			$break_html=' ';
		}
		
		
			
		$x .= '<tr>';
			
			### First Term Daily
			$thisTitle = $eReportCard['Template']['DailyCh'].' '.$eReportCard['Template']['DailyEn'].$break_html.($ColumnWeightArr[0]/2).'%';
			$x .= "<td valign='middle' align='center' class='{$font_class} border_left border_top_1px  border_top' height='{$LineHeight}' width='$MarkWidth'>". $thisTitle . "</td>";
			
			### First Term Exam
			$thisTitle = $eReportCard['Template']['ExamCh'].' '.$eReportCard['Template']['ExamEn'].$break_html.($ColumnWeightArr[1]/2).'%';
			$x .= "<td valign='middle' align='center' class='{$font_class} border_top_1px border_left  border_top' height='{$LineHeight}' width='$MarkWidth'>". $thisTitle . "</td>";
			
			if ($ReportType == 'W')
			{
				### Second Term Daily
				$thisTitle = $eReportCard['Template']['DailyCh'].' '.$eReportCard['Template']['DailyEn'].$break_html.($ColumnWeightArr[2]/2).'%';
				$x .= "<td valign='middle' align='center' class='{$font_class} border_left border_top_1px border_top' height='{$LineHeight}' width='$MarkWidth'>". $thisTitle . "</td>";
				
				### Final
				$thisTitle = $eReportCard['Template']['ExamCh'].' '.$eReportCard['Template']['ExamEn'].$break_html.($ColumnWeightArr[3]/2).'%';
				$x .= "<td valign='middle' align='center' class='{$font_class} border_left border_top_1px border_left_1px border_top' height='{$LineHeight}' width='$MarkWidth'>". $thisTitle . "</td>";
			}
			
		$x .= '</tr>';
		
		
		return array($x, $n, $e, $t);
	}

	function returnTemplateSubjectCol($ReportID, $ClassLevelID, $isAllColumnZeroWeightAry=array())
	{
		global $eRCTemplateSetting;

		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID 		= $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";

		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 //		$font_class = ($ReportType=='T')? 'font16px' : 'font13px';
 		$font_class = ($ReportType=='T')? 'font16px' : 'font_8pt';

 		$x = array();
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
				 		
				 		$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
						$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubSubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID);
						if(!empty($CmpSubjectArr)) $isParentSubject = 1;
			 		}
	
		 			$css_border_top = ($isFirst)? "border_top" : "border_top_1px";
			 		$counter = 0;
			 		foreach($SubjectDisplay as $k=>$v) {
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
	
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
		 				$colspan = ($isAllColumnZeroWeightAry[$SubSubjectID] && $counter==1)? 2 : 1;
	 
					 	$t .= "<td colspan='{$colspan}' class='$font_class {$css_border_top} border_top' height='{$LineHeight}' valign='middle'  style=\"text-align:left;\">";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}' class='$font_class'  style=\"text-align:left;\">$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
						$counter++;
			 		}
					$x[] = $t;
					$isFirst = 0;
			 	}
		 	}
		}

 		return $x;
	}

	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		// 2013-0411-1229-44156
// 		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$SemID 		= $ReportSetting['Semester'];
// 		$ReportType = $SemID == "F" ? "W" : "T";
// 		
// 		$html = '';
// 		
//		if($ReportType=='T')
//		{
//
//		}
//		else if($ReportType=='W')
//		{		 
//	 		$html .= '<table width="100%" height="100%" cellspacing="0" cellpadding="2">';
//					$html .= '<col width="30%"/>';
//					$html .= '<col width="10%"/>';
//					$html .= '<col width="60%"/>';
//					$html .= '<tr class="font13px">';
//						$html .='<td class="border_bottom">&nbsp;</td>';
//						$html .='<td class="">&nbsp;</td>';
//						$html .='<td class="" style="text-align:left;">'.$eReportCard['Template']['RemarkStr'].'</td>';
//					$html .= '</tr>';
//					$html .= '<tr class="font13px">';
//						$html .='<td class="">'.$eReportCard['Template']['ClassTeacherSignStr'].'</td>';
//						$html .='<td class="">&nbsp;</td>';
//						$html .='<td class="">&nbsp;</td>';
//					$html .= '</tr>';
//					
//			$html .= '</table>';
//		}
		
 		return $html;
	}
	
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
//		$ShowNumOfStudentClass =true;
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
//		$ShowGrandTotal =false;
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];

		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);

		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];

		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
			
		/********** CSV Data *****************/
		# build data array
		$ary = $this->getReportOtherInfoData($ReportID, $StudentID);
		$ary = $ary[$StudentID];
		/********** End CSV Data *****************/
		
	

		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID);
		$GrandTotal = $StudentID ? $result['GrandTotal'] :  $this->EmptySymbol;
		$AverageMark = $StudentID ? $result['GrandAverage'] :  $this->EmptySymbol;
		
		$ClassPosition = $StudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? $this->EmptySymbol: $result['OrderMeritClass']) : $this->EmptySymbol) :  $this->EmptySymbol;
  		$ClassNumOfStudent = $StudentID ? ($result['ClassNoOfStudent'] ? ($result['ClassNoOfStudent']==-1? $this->EmptySymbol: $result['ClassNoOfStudent']) : $this->EmptySymbol) :  $this->EmptySymbol;
  		$FormPosition = $StudentID ? ($result['OrderMeritForm'] ? ($result['OrderMeritForm']==-1? $this->EmptySymbol: $result['OrderMeritForm']) : $this->EmptySymbol) :  $this->EmptySymbol;
  		$FormNumOfStudent = $StudentID ? ($result['FormNoOfStudent'] ? ($result['FormNoOfStudent']==-1? $this->EmptySymbol: $result['FormNoOfStudent']) : $this->EmptySymbol) :  $this->EmptySymbol;

		
		### Get Assessment Mark
  		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
  		foreach($ColumnData as $k1 => $dataArr)
		{
			$TermID = $dataArr['SemesterNum'];
			$ColumnID = $dataArr['ReportColumnID'];
			
			if($ReportType == 'T')
			{
				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID);
				
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] :  $this->EmptySymbol;
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] :  $this->EmptySymbol;
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] :  $this->EmptySymbol;
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] :  $this->EmptySymbol;
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] :  $this->EmptySymbol;
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] :  $this->EmptySymbol;
			}
			else
			{
				# check sems/assesment col#
				$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$TermID ."' and ClassLevelID=".$ClassLevelID);
				$thisReportID = $thisReport['ReportID'];
				
				$columnResult = $this->getReportResultScore($thisReportID, 0, $StudentID);
				
				// 2016-0621-0943-14207
				$isExcludedStudent = ($columnResult['OrderMeritClass']==-1)? true : false;
				if ($isExcludedStudent) {
					$columnResult['GrandTotal'] = -1;
					$columnResult['GrandAverage'] = -1;
					$columnResult['OrderMeritClass'] = -1;
					$columnResult['OrderMeritForm'] = -1;
					$columnResult['ClassNoOfStudent'] = -1;
					$columnResult['FormNoOfStudent'] = -1;
				}
				
				$columnTotal[$ColumnID] = $StudentID ? $columnResult['GrandTotal'] :  $this->EmptySymbol;
				$columnAverage[$ColumnID] = $StudentID ? $columnResult['GrandAverage'] :  $this->EmptySymbol;
				$columnClassPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritClass'] :  $this->EmptySymbol;
				$columnFormPos[$ColumnID] = $StudentID ? $columnResult['OrderMeritForm'] :  $this->EmptySymbol;
				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $columnResult['ClassNoOfStudent'] :  $this->EmptySymbol;
				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $columnResult['FormNoOfStudent'] :  $this->EmptySymbol;				
			}
		}
		
		$first = 1;
		# Overall Result
		if($ShowGrandTotal)
		{
			$thisTitleEn = $eReportCard['Template']['OverallResultEn'];
			$thisTitleCh = $eReportCard['Template']['OverallResultCh'];
			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
			
			$first = 0;
		}

//		if ($ReportType == 'T')
//		{
//			$thisTitleEn = $eReportCard['Template']['U_Prep_HistoryEn'];
//			$thisTitleCh = $eReportCard['Template']['U_Prep_HistoryCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "UniversityPrepHistory", $ary, 1);
//			
//			$thisTitleEn = $eReportCard['Template']['U_Prep_MathEn'];
//			$thisTitleCh = $eReportCard['Template']['U_Prep_MathCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "UniversityPrepMath", $ary, 1);
//		}

		if ($eRCTemplateSetting['HideCSVInfo'] != true)
		{
			# Average Mark
			if($ShowGrandAvg)
			{
				$thisTitleEn = $eReportCard['Template']['AverageEn'];
				$thisTitleCh = $eReportCard['Template']['AverageCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $AverageMark, $first, '', 'GrandAverage');

				$first = 0;
			}
		
			# Number of Students in Form
			if($ShowNumOfStudentForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['FormNumOfStudentCh'];
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);

				$first = 0;
			}

			# Position in Form
			if($ShowOverallPositionForm)
			{
				$thisTitleEn = $eReportCard['Template']['FormPositionEn'];
				$thisTitleCh = $eReportCard['Template']['FormPositionCh'];
				
				// 2012-1120-1441-30156: request to disable the customization
//				if($FormNumOfStudent!=0 && $FormNumOfStudent!='' && $FormNumOfStudent!=$this->EmptySymbol)
//				{
//					$FormPositionPercent = ($FormPosition/$FormNumOfStudent)*100;
//				}
//	
//				if($FormPositionPercent<=50)
//				{
//					
//				}
//				else
//				{
//					$FormPosition = $this->EmptySymbol;
//				}

				if ($OverallPositionRangeForm > 0 && $FormPosition > $OverallPositionRangeForm) {
					$FormPosition = $this->EmptySymbol;
				}
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first, $OverallPositionRangeForm);

				$first = 0;
			}
		
			# Number of Students in Class
			if($ShowNumOfStudentClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassNumOfStudentEn'];
				$thisTitleCh = $eReportCard['Template']['ClassNumOfStudentCh'];
				
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);

				$first = 0;
			}	
			
			# Position in Class
			if($ShowOverallPositionClass)
			{
				$thisTitleEn = $eReportCard['Template']['ClassPositionEn'];
				$thisTitleCh = $eReportCard['Template']['ClassPositionCh'];
				
				if ($OverallPositionRangeClass > 0 && $ClassPosition > $OverallPositionRangeClass) {
					$ClassPosition = $this->EmptySymbol;
				}
				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first, $OverallPositionRangeClass);

				$first = 0;
			}

			##################################################################################
			# CSV related
			##################################################################################

			$border_top = $first ? "border_top" : "";
		
			# Conduct
			$thisTitleEn = $eReportCard['Template']['ConductEn'];
			$thisTitleCh = $eReportCard['Template']['ConductCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary, 1);
				
			# Late Attendance
			$thisTitleEn = $eReportCard['Template']['TimesLateEn'];
			$thisTitleCh = $eReportCard['Template']['TimesLateCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "TimesLate", $ary);

			# Excused Absence (Periods)
			$thisTitleEn = $eReportCard['Template']['DaysAbsentEn'];
			$thisTitleCh = $eReportCard['Template']['DaysAbsentCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "ExcusedAbsencePeriods", $ary);

			# Unexcused Absence (Periods)
			$thisTitleEn = $eReportCard['Template']['UnexcusedDaysAbsentEn'];
			$thisTitleCh = $eReportCard['Template']['UnexcusedDaysAbsentCh'];
			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "UnexcusedAbsencePeriods", $ary);
		}
		
		if ($ReportType == 'W')
		{
			# Comments & Final Comments
			$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
			$ClassTeacherComment = trim($CommentAry[0]);
			$ClassTeacherComment = ($ClassTeacherComment=='')? $this->EmptySymbol : $ClassTeacherComment;
//			$ClassTeacherComment = convert2unicode($ClassTeacherComment, 1, 1);
			
			$FinalComment = nl2br(trim($ary[0]['FinalComments']));
			if ($FinalComment != '') {
				$FinalCommentDisplay = $FinalComment;
			}
			else {
				// do not default show generated comment. otherwise need to cust master report also 
				//$FinalCommentArr = $this->Get_Student_Final_Comment($ReportID, $StudentID);
				//$FinalCommentDisplay = implode('<br />', (array)$FinalCommentArr[$StudentID]);
			}
			
			$ExtraActivity_Str = $ary[0]['ExtraActivities'];
			$ExtraActivity_Str = ($ExtraActivity_Str=='')? $this->EmptySymbol:$ExtraActivity_Str;
			$Services_Str = $ary[0]['Services'];
			$Services_Str = ($Services_Str=='')? $this->EmptySymbol:$Services_Str;
			$AwardPunishment_Str = $ary[0]['AwardPunishment'];
			$AwardPunishment_Str = ($AwardPunishment_Str=='')? $this->EmptySymbol:$AwardPunishment_Str;
		
			$FinalCommentDisplay = ($FinalCommentDisplay=='')? $this->EmptySymbol:$FinalCommentDisplay;
			
			$thisTitleEn = $eReportCard['Template']['CommentEn'];
			$thisTitleCh = $eReportCard['Template']['CommentCh'];
			
			$x .='<tr height="40px">
					   <td class="font_8pt border_top" style="text-align:left;padding-left:8px;">'.$thisTitleCh.'</td>
					   <td class="font_8pt border_top" colspan="2" style="text-align:left;padding-left:8px;">'.$thisTitleEn.'</td>	
					   <td class="font_8pt border_top border_left" colspan="5" style="text-align:left;padding-left:8px;vertical-align:top;">'.$ClassTeacherComment.'</td>
				  </tr>';
			
			$x .= '<tr>';
				$x .= '<td colspan="8" class="border_top" style="padding:0px;">';
					# Comment
					$x .= '<table width="100%" height="100%" cellspacing="0" cellpadding="2" class="font_8pt">';
						$x .='<col width="25%"/>';
						$x .='<col width="25%"/>';
						$x .='<col width="25%"/>';
						$x .='<col width="25%"/>';
					
						$x .= '<tr>';
							$x .= '<td class="border_right" align="center">'.$eReportCard['Template']['ExtraActivityCh'].' '.$eReportCard['Template']['ExtraActivityEn'].'</td>';
							$x .= '<td class="border_right" align="center">'.$eReportCard['Template']['ServiceCh'].' '.$eReportCard['Template']['ServiceEn'].'</td>';
							$x .= '<td class="border_right" align="center">'.$eReportCard['Template']['AwardPunishCh'].' '.$eReportCard['Template']['AwardPunishEn'].'</td>';
							$x .= '<td class="" align="center">'.$eReportCard['Template']['FinalCommentCh'].' '.$eReportCard['Template']['FinalCommentEn'].'</td>';
						$x .='</tr>';
						
						$x .= '<tr style="height:50px; text-align:left;">';
							$x .= '<td class="border_right border_top">'.$ExtraActivity_Str.'</td>';
							$x .= '<td class="border_right border_top">'.$Services_Str.'</td>';
							$x .= '<td class="border_right border_top">'.$AwardPunishment_Str.'</td>';
							$x .= '<td class="border_top">'.$FinalCommentDisplay.'</td>';
						$x .= '</tr>';
					$x .= '</table>';
				$x .= '</td>';
				
			$x .= '</tr>';
		}

		return $x;
	}

	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;

		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];

		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];

		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
//debug_r($MarksAry); 
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		$font_class = ($ReportType=='T')? 'font16px' : 'font13px';

		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1);
		if($ReportType=="T")	# Terms Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];

			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$numOfColumn = count($ColumnID);
				
			$WeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID='', $convertEmptyToZero=true);
			$WeightAssoArr = BuildMultiKeyAssoc($WeightArr, array('SubjectID', 'ReportColumnID'), array('Weight'), $SingleValue=1);

			foreach($SubjectArray as $SubjectID => $SubjectName) {
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}

				# define css
				$css_border_top = ($isFirst)? "border_top" : "border_top_1px";

				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, $withGrandResult=0, $returnAsso=0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$thisSubjectWeight = $WeightAssoArr[$SubjectID][0];
				
				// for "升中 xxxxxx" Subject (Subject column weights are all zero)
				$thisAllColumnWeightZero = true;
				for ($i=0; $i<$numOfColumn; $i++) {
					$thisReportColumnID = $ColumnID[$i];
					$thisTmpSubjectWeight = $WeightAssoArr[$SubjectID][$thisReportColumnID];
					
					if ($thisTmpSubjectWeight > 0) {
						$thisAllColumnWeightZero = false;
						break;
					}
				}
				
				$isAllNA = true;
				if ($thisAllColumnWeightZero) {
					### for "升中 xxxxxx" Subject (Subject column weights are all zero)
					// no weight display
					// display overall column result diretcly
					$thisReportColumnID = 0;
					
					$thisMSGrade = $MarksAry[$SubjectID][$thisReportColumnID]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][$thisReportColumnID]['Mark'];
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID) {
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					//2014-0520-1508-34066
					//if ($thisMark != "N.A.") {
					if ($thisMark != '' && $thisMark != "N.A.") {
						$isAllNA = false;
					}
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $thisMSGrade);
					if($needStyle) {
						if ($thisSubjectWeight>0 && $ScaleDisplay=="M") {
							$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
						}
						else {
							$thisMarkTemp = $thisMark;
						}
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
					}
					else {
						$thisMarkDisplay = $thisMark;
					}
					$x[$SubjectID] .= "<td colspan='{$numOfColumn}' class='{$css_border_left} {$css_border_top} border_top border_left'>";
						$x[$SubjectID] .= $thisMarkDisplay;
					$x[$SubjectID] .= "</td>";
				}
				else {
					if ($ScaleInput == 'M')
						$thisDisplay = $thisSubjectWeight;
					else
						$thisDisplay = $this->EmptySymbol;
					$x[$SubjectID] .= "<td class='border_left border_top ".$font_class."' align='center'>".$thisDisplay."</td>";
					
					# Assessment Marks & Display
					for($i=0; $i<$numOfColumn; $i++) {
						$thisColumnID = $ColumnID[$i];
						$columnSubjectWeightTemp = $WeightAssoArr[$SubjectID][$thisColumnID];
	
						$css_border_left = ($i==0)? 'border_left' : 'border_left_1px';
	
						if ($isSub && $columnSubjectWeightTemp == 0) {
							$thisMarkDisplay = $this->EmptySymbol;
						}
						else {
							$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
							$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
							
							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
							
							# for preview purpose
							if(!$StudentID) {
								$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
								$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
							}
							
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
							
							//2014-0520-1508-34066
							//if ($thisMark != "N.A.") {
							if ($thisMark != '' && $thisMark != "N.A.") {
								$isAllNA = false;
							}
							
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
							
							if($needStyle) {
								if ($thisSubjectWeight>0 && $ScaleDisplay=="M") {
									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
								}
								else {
									$thisMarkTemp = $thisMark;
								}
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
							}
							else {
								$thisMarkDisplay = $thisMark;
							}
						}
						$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top} border_top border_left'>";
							$x[$SubjectID] .= $thisMarkDisplay;
						$x[$SubjectID] .= "</td>";
					}
				}
				
				$isFirst = 0;

				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$returnArr['isAllColumnZeroWeight'][$SubjectID] = $thisAllColumnWeightZero;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];

			# Retrieve Invloved Temrs
			$ColumnData[$ReportID] = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnData[$ReportID]);
			
			$WeightArr = array();
			$ReportWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID='', $convertEmptyToZero=true);
			$WeightArr[$ReportID] = BuildMultiKeyAssoc($ReportWeightArr, array('SubjectID', 'ReportColumnID'), array('Weight'), $SingleValue=1);
			
			$thisAllColumnWeightZero = true;
			for ($i=0; $i<$numOfColumn; $i++) {
				$thisSemester = $ColumnData[$ReportID][$i]['SemesterNum'];
				$thisTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisSemester);
				$thisReportID = $thisTermReportInfoArr['ReportID'];
				
				$thisReportWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, '', '', '', $convertEmptyToZero=true);
				$WeightArr[$thisReportID] = BuildMultiKeyAssoc($thisReportWeightArr, array('SubjectID', 'ReportColumnID'), array('Weight'), $SingleValue=1);
			
				$ColumnData[$thisReportID] = $this->returnReportTemplateColumnData($thisReportID);
			}

			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, $withGrandResult=0, $returnAsso=0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];

				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				$css_border_top = $isFirst? "border_top" : "border_top_1px";
				
				$thisAllColumnWeightZero = true;
				for ($i=0; $i<$numOfColumn; $i++) {
					$thisSemester = $ColumnData[$ReportID][$i]['SemesterNum'];
					$thisTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $thisSemester);
					$thisReportID = $thisTermReportInfoArr['ReportID'];
					$thisNumOfTermReportColumn = count($ColumnData[$thisReportID]);
					
					for ($j=0; $j<$thisNumOfTermReportColumn; $j++) {
						$thisTermReportColumnID = $ColumnData[$thisReportID][$j]['ReportColumnID'];
						$thisTmpSubjectWeight = $WeightArr[$thisReportID][$SubjectID][$thisTermReportColumnID];
						
						if ($thisTmpSubjectWeight > 0) {
							$thisAllColumnWeightZero = false;
							break(2);
						}
					}
				}
				
				$isAllNA = true;
				if ($thisAllColumnWeightZero) {
					### for "升中 xxxxxx" Subject (Subject column weights are all zero)
					// no weight display
					// display overall result only
					# Terms's Assesment / Terms Result
					for($i=0;$i<sizeof($ColumnData[$ReportID]);$i++)
					{
						#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
						
						# Retrieve assesments' marks
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$ReportID][$i]['SemesterNum'] ."' and ClassLevelID = '".$ClassLevelID."' ");
	
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='".$font_class." border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$thisReportColumnID = 0;
							
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);
	
							$css_border_left = ($j % 2 == 0)? "border_left" : "border_left_1px";
							
							$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, '', '', $thisReportColumnID);
							$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];

							if ($isSub && $columnSubjectWeightTemp == 0) {
								$thisMarkDisplay =  $this->EmptySymbol;
							}
							else {
								$thisSubjectWeight = $columnSubjectWeightTemp;

								$thisMSGrade = $thisMarksAry[$SubjectID][$thisReportColumnID]['Grade'];
								$thisMSMark = $thisMarksAry[$SubjectID][$thisReportColumnID]['Mark'];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";

								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
								}
								$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								//2014-0520-1508-34066
								//if ($thisMark != "N.A.") {
								if ($thisMark != '' && $thisMark != "N.A.") {
									$isAllNA = false;
								}
 
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$thisReportColumnID]['Grade']);
								if($needStyle) {
									if ($thisSubjectWeight>0 && $ScaleDisplay=="M") {
										$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
									}
									else {
										$thisMarkTemp = $thisMark;
									}
									$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
								}
								else {
									$thisMarkDisplay = $thisMark;	
								}
							}
							
							$thisNumOfTermReportColumn = count($ColumnData[$thisReportID]);
							$x[$SubjectID] .= "<td colspan='".$thisNumOfTermReportColumn."' class='{$css_border_left} {$css_border_top} border_top border_left'>";
								$x[$SubjectID] .= $thisMarkDisplay;
							$x[$SubjectID] .= "</td>";
						}
						
					}
				}
				else {
					# Units
					$SubjectWeight = $this->returnReportTemplateSubjectWeightData($ReportID, " SubjectID='$SubjectID' And (ReportColumnID='0' || ReportColumnID is Null)");
					$thisSubjectWeight = $SubjectWeight[0]['Weight'];
					
					if ($ScaleInput == 'M')
						$thisDisplay = $thisSubjectWeight;
					else
						$thisDisplay = $this->EmptySymbol;
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} ".$font_class." border_top' align='center'>".$thisDisplay."</td>";
					
					# Terms's Assesment / Terms Result
					for($i=0;$i<sizeof($ColumnData[$ReportID]);$i++)
					{
						#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
						
						# Retrieve assesments' marks
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $ColumnData[$ReportID][$i]['SemesterNum']);
						
						# if no term reports, CANNOT retrieve assessment result at all
						if (empty($thisReport)) {
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$x[$SubjectID] .= "<td align='center' class='".$font_class." border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
							}
						} else {
							$thisReportID = $thisReport['ReportID'];
							$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
							$ColumnID = array();
							$ColumnTitle = array();
							foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
	
							$thisMarksAry = $this->getMarks($thisReportID, $StudentID);
	
							for($j=0;$j<sizeof($ColumnID);$j++) {
								$css_border_left = ($j % 2 == 0)? "border_left" : "border_left_1px";
								
								$thisColumnID = $ColumnID[$j];
								$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
	
								if ($isSub && $columnSubjectWeightTemp == 0) {
									$thisMarkDisplay =  $this->EmptySymbol;
								}
								else {
									$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$thisColumnID ."' and SubjectID = '$SubjectID' ");
									$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
	
									$thisMSGrade = $thisMarksAry[$SubjectID][$thisColumnID]['Grade'];
									$thisMSMark = $thisMarksAry[$SubjectID][$thisColumnID]['Mark'];
									
									$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
									$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
	
									# for preview purpose
									if(!$StudentID)
									{
										$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
										$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
									}
									$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
									
									//2014-0520-1508-34066
									//if ($thisMark != "N.A.") {
									if ($thisMark != '' && $thisMark != "N.A.") {
										$isAllNA = false;
									}
									
									# check special case
									list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$thisColumnID]['Grade']);
									if($needStyle) {
										if ($thisSubjectWeight>0 && $ScaleDisplay=="M") {
											$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
										}
										else {
											$thisMarkTemp = $thisMark;
										}
										$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
									}
									else {
										$thisMarkDisplay = $thisMark;	
									}
								}
	
								$x[$SubjectID] .= "<td class='{$css_border_left} {$css_border_top} border_top border_left'>";
									$x[$SubjectID] .= $thisMarkDisplay;
								$x[$SubjectID] .= "</td>";
							}
						}
					}
				}
				
				
				# Subject Overall
				$MarksAry = $this->getMarks($ReportID, $StudentID);

				$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
				$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];
				
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";

				# for preview purpose
				if(!$StudentID)
				{
					$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
					$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
				}

				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? $thisMark : $thisMark) : $thisGrade;
				
				//2014-0520-1508-34066
				//if ($thisMark != "N.A.") {
				if ($thisMark != '' && $thisMark != "N.A.") {
					$isAllNA = false;
				}
				
				# check special case
				list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
				if($needStyle) {
					$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
				}
				else {
					$thisMarkDisplay = $thisMark;
				}

				//$x[$SubjectID] .= "<td class='border_left {$css_border_top} ".$font_class."' align='center'>". $thisMarkDisplay ."</td>";
				
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} border_top' ".$font_class."'>";
					$x[$SubjectID] .= $thisMarkDisplay;
				$x[$SubjectID] .= "</td>";
				
				$isFirst = 0;

				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				$returnArr['isAllColumnZeroWeight'][$SubjectID] = $thisAllColumnWeightZero;
			}
		}	# End Whole Year Report Type

		return $returnArr;
	}
	

	function getMiscTable($ReportID, $StudentID='')
	{
		
	}

	########### END Template Related

	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr, $isFirst=0)
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		# initialization
		$border_top = $isFirst ? "border_top" : "border_top_1px";
		$font_class = ($ReportType=='T')? 'font16px' : 'font13px';
		
		$x = "";

		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($ReportType, $TitleEn, $TitleCh, $isFirst);

		if($ReportType=="W")	# Whole Year Report
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);

			$thisTotalValue = "";
			$countColumn = 0;
			foreach($ColumnData as $k1=>$d1)
			{
				# get value of this term
				$TermID = $d1['SemesterNum'];
				$thisValue = $StudentID ? ($ValueArr[$TermID][$InfoKey]!='' ? $ValueArr[$TermID][$InfoKey] : $this->EmptySymbol) :  $this->EmptySymbol;
				$border_left = 'border_left';
				
				# display this term value
				$x .= "<td colspan='2' class='{$font_class} border_top {$border_left}' align='center' height='{$LineHeight}'>". $thisValue ."</td>";
				
				$countColumn++;
			}

			# display overall year value (always show '---')
			$thisTotalValue = $this->EmptySymbol;
			$x .= "<td class='{$font_class} border_top border_left' align='center' height='{$LineHeight}'>". $thisTotalValue ."</td>";
		}
		else				# Term Report
		{
			# get value of this term
			$thisValue = $StudentID ? ($ValueArr[$SemID][$InfoKey]!='' ? $ValueArr[$SemID][$InfoKey] : $this->EmptySymbol) :  $this->EmptySymbol;

			# display this term value
			$x .= "<td colspan='2' class='{$font_class} border_top border_left' align='center' height='{$LineHeight}' >". $thisValue ."</td>";
		}
		$x .= "</tr>";

		return $x;
	}

	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $UpperLimit="", $MarkType='')
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;

		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$SemID 						= $ReportSetting['Semester'];
		$ClassLevelID				= $ReportSetting['ClassLevelID'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$CalOrder = $this->Get_Calculation_Setting($ReportID);
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);

		$border_top = $isFirst ? "border_top" : "border_top_1px";
		$font_class = ($ReportType=='T')? 'font16px' : 'font13px';

		$x .= "<tr>";
			$x .= $this->Generate_Info_Title_td($ReportType, $TitleEn, $TitleCh, $isFirst);
			
			if ($ReportType == 'T')
			{
				if ($OverallValue == -1) {
					$OverallValue = $this->EmptySymbol;
				}
				else if ($MarkType == 'GrandAverage')
				{
					$thisNature = $this->returnMarkNature($ClassLevelID, '-1', $OverallValue);
					$thisDisplay = $this->ReturnTextwithStyle($OverallValue, 'HighLight', $thisNature);
				}	
				$x .= "<td colspan='2' class='{$font_class} border_top border_left' align='center' height='{$LineHeight}'>".$OverallValue."</td>";
			}
			else
			{
				$curColumn = 0;
				foreach ($ValueArr as $ColumnID => $thisValue) 
				{
					//$border_left = ($curColumn%2 == 0)? 'border_left' : 'border_left_1px';
					$border_left = 'border_left';
					
					if ($thisValue == -1) {
						$thisDisplay = $this->EmptySymbol;
					}
					else {
						if ($MarkType == 'GrandAverage')
						{
							$thisNature = $this->returnMarkNature($ClassLevelID, '-1', $thisValue);
							$thisDisplay = $this->ReturnTextwithStyle($thisValue, 'HighLight', $thisNature);
						}
						else
						{
							if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
							{
								if ($thisValue == '')
									$thisDisplay = $this->EmptySymbol;
								else
									$thisDisplay = $thisValue;
							}
							else
							{
								$thisDisplay = $this->EmptySymbol;
							}
						}
					}
					
					$x .= "<td colspan='2' class='{$font_class} border_top {$border_left}' align='center' height='{$LineHeight}'>".$thisDisplay."</td>";
					$curColumn++;
				}
				
				# Year Overall
				$thisValue = $OverallValue;
				
				if ($thisValue == -1) {
					$thisDisplay = $this->EmptySymbol;
				}
				else {
					if ($MarkType == 'GrandAverage')
					{
						$thisNature = $this->returnMarkNature($ClassLevelID, '-1', $thisValue);
						$thisDisplay = $this->ReturnTextwithStyle($thisValue, 'HighLight', $thisNature);
					}
					else
					{
						if ($UpperLimit=="" || $UpperLimit==0 || $thisValue <= $UpperLimit)
						{
							$thisDisplay = $thisValue;
						}
						else
						{
							$thisDisplay = $this->EmptySymbol;
						}
					}
				}
				$x .= "<td class='{$font_class} border_top border_left' align='center' height='{$LineHeight}'  style='text-align:center;'>". $thisDisplay ."</td>";
			}
		$x .= "</tr>";

		return $x;
	}

	function Generate_Info_Title_td($ReportType, $TitleEn, $TitleCh, $BorderTopThick=0)
	{
		$x = "";
		$border_top = ($BorderTopThick)? "border_top" : "border_top_1px";
		$font_class = ($ReportType=='T')? 'font16px' : 'font_8pt';

		$x .= "<td class='border_top' height='{$LineHeight}' style='text-align:left;'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}' class='".$font_class."'  style=\"text-align:left;\" >". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td class='border_top' height='{$LineHeight}' colspan='2' style='text-align:left;'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>&nbsp;&nbsp;</td><td height='{$LineHeight}' class='".$font_class."'  style=\"text-align:left;\">". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";

		return $x;
	}

	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];

		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;

		return $ShowRightestColumn;
	}

	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";

		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];

		return $CalOrder;
	}
	
	
	function Get_Grade_Remarks_Table($ReportType)
	{
		global $eReportCard;
		
		$align = ($ReportType=='T')? 'center' : 'left';
		
		$x = '';
		$x .= '<table cellspacing="0" cellpadding="0" align="'.$align.'">';
			
			if ($ReportType == 'T')
			{
				$x .= '<tr>';
					$x .= '<td width="25%" class="font14px">'.$eReportCard['Template']['RemarkCh'].': &nbsp;&nbsp;</td>';
					$x .= '<td class="font14px">'.$eReportCard['Template']['RemarkArr']['A'].'</td>';
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td class="font14px">'.$eReportCard['Template']['RemarkArr']['B'].'</td>';
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td class="font14px">'.$eReportCard['Template']['RemarkArr']['C'].'</td>';
				$x .= '</tr>';
			}
			else
			{
				$x .= '<tr>';
					$x .= '<td width="10%" class="font14px">'.$eReportCard['Template']['RemarkCh'].': </td>';
					$x .= '<td class="font14px">'.$eReportCard['Template']['RemarkArr']['A'].$eReportCard['Template']['RemarkArr']['B'].$eReportCard['Template']['RemarkArr']['C'].'</td>';
				$x .= '</tr>';
			}
			
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Student_Final_Comment($ReportID, $StudentIDArr) {
		global $lreportcard, $eReportCard;
		
		if (!is_array($StudentIDArr)) {
			$StudentIDArr = array($StudentIDArr);
		}
		$numOfStudent = count($StudentIDArr);
		
		### Get Report Info
		$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=true);
		
		### Get Marks
		//$MarkNatureArr[$StudentID][$SubjectID][$ReportColumnID]['Weight', 'Nature', ...] = data
		$MarkNatureArr = $lreportcard->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $lreportcard->getMarks($ReportID), $WithExtraData=true);
		//$GrandMarkArr[$StudentID][$ReportColumnID]['GrandAverage'] = data
		$GrandMarkArr = $lreportcard->getReportResultScore($ReportID);
		
		### Get Subject
		$SubjectAssoInfoArr = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=1, $ReportID, $_SESSION['intranet_session_language']);
		
		### Get Promotion Info
		$retainFormName = $lreportcard->returnClassLevel($ClassLevelID);
		$promotionFormInfoArr = $lreportcard->Get_Promotion_Form($ClassLevelID);
		$promotionFormName = $promotionFormInfoArr['YearName'];
		
		
		### Evaluate Student Mark Nature
		$StudentInfoAssoArr = array();
		$ReportColumnID = 0;	// check overall column only
		for ($i=0; $i<$numOfStudent; $i++) {
			$thisStudentID = $StudentIDArr[$i];
			
			// loop $SubjectAssoInfoArr to maintain Subject ordering
			foreach((array)$SubjectAssoInfoArr as $thisSubjectID => $thisSubjectName) {
				$thisNature = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID]['Nature'];
				$thisWeight = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID]['Weight'];
				$thisScaleInput = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID]['ScaleInput'];
				
				if ($thisScaleInput == 'M' && $thisNature == 'Fail') {
					$StudentInfoAssoArr[$thisStudentID]['FailedWeight'] += $thisWeight;
					$StudentInfoAssoArr[$thisStudentID]['FailedSubjectArr'][$thisSubjectID] = $thisSubjectName;
				}
			}
		}
		
		
		### Build Final Comment
		$CommentArr = array();
		for ($i=0; $i<$numOfStudent; $i++) {
			$thisStudentID = $StudentIDArr[$i];
			
			$ReportColumnID = 0;	// check overall column only
			$thisGrandAverage = $GrandMarkArr[$thisStudentID][$ReportColumnID]['GrandAverage'];
			$thisSubjectFailedWeight = $StudentInfoAssoArr[$thisStudentID]['FailedWeight'];
			$thisSubjectFailedSubjectAssoArr = $StudentInfoAssoArr[$thisStudentID]['FailedSubjectArr'];
			
			if ($FormNumber==3) {
				// 初三
				$SubjectFailedWeight = 5;
				
				if ($thisSubjectFailedWeight >= $SubjectFailedWeight) {
					// 條件1 (達至5單位)
	         		// 不及格科目共?單位, 次學年仍留xxx年級.
	
					$thisComment = $eReportCard['Template']['FinalCommentArr']['ExceededSubjectFailUnit'];
					$thisComment = str_replace('<!--FailedSubjectWeight-->', $thisSubjectFailedWeight, $thisComment);
					$thisComment = str_replace('<!--FormName-->', $retainFormName, $thisComment);
					$CommentArr[$thisStudentID][] = $thisComment;
				}
				else if (count((array)$thisSubjectFailedSubjectAssoArr) > 0) {
					// 條件2
	        		// 應補考xx,xx
	       			// 補考不及格, 次學年仍留xxx年級.  <-- cannot cater since no re-exam handling
	         		
					$CommentArr[$thisStudentID][] = str_replace('<!--SubjectNameList-->', implode(', ',(array)$thisSubjectFailedSubjectAssoArr), $eReportCard['Template']['FinalCommentArr']['ReExamSubject']);
					//$CommentArr[$thisStudentID][] = str_replace('<!--FormName-->', $retainFormName, $eReportCard['Template']['FinalCommentArr']['ReExamFailedAndRetain']);
				}
				else {
					// 條件3 (全科及格)
	       			// 准予畢業
	         		
	         		$CommentArr[$thisStudentID][] = $eReportCard['Template']['FinalCommentArr']['CanBeGraduate'];
				}
			}
			else if ($FormNumber==6) {
				// 高三
				$SubjectFailedWeight = 5;
				
				// 條件1
	       		// 不及格科目共?單位, 次學年仍留xxx年級.
				if ($thisSubjectFailedWeight >= $SubjectFailedWeight) {
					$thisComment = $eReportCard['Template']['FinalCommentArr']['ExceededSubjectFailUnit'];
					$thisComment = str_replace('<!--FailedSubjectWeight-->', $thisSubjectFailedWeight, $thisComment);
					$thisComment = str_replace('<!--FormName-->', $retainFormName, $thisComment);
					$CommentArr[$thisStudentID][] = $thisComment;
				}
				else if (count((array)$thisSubjectFailedSubjectAssoArr) > 0) {
					// 條件2 (沒有單位限制)
					// 應補考xx,xx
					// 補考不及格, 次學年? 敞dxxx年級 <-- cannot cater since no re-exam handling
					// 
					// 條件3 (沒有單位限制)
					// 應補考xx,xx
					// 各科補考及格, 准予畢業  <-- cannot cater since no re-exam handling
	
	         		$CommentArr[$thisStudentID][] = str_replace('<!--SubjectNameList-->', implode(', ',(array)$thisSubjectFailedSubjectAssoArr), $eReportCard['Template']['FinalCommentArr']['ReExamSubject']);
				}
			}
			else {
				// 初一、初二、高一、高二
				$GrandAverageLowerLimit = 60;
				$SubjectFailedWeight = 5;
				
				if ($thisGrandAverage != -1 && $thisGrandAverage < $GrandAverageLowerLimit) {
					// 條件1 (學年平均成績總計低於60分)
	          		// 學年平均成績不及格, 次學年仍留xxx年級.
	          		
					$CommentArr[$thisStudentID][] = str_replace('<!--FormName-->', $retainFormName, $eReportCard['Template']['FinalCommentArr']['GrandAverageFailed']);
				}
				else if ($thisSubjectFailedWeight >= $SubjectFailedWeight) {
					// 條件2 (達至5單位)(以分數計算的科目以不及格單位計算? d級, 中英數各佔2單位, 其他佔1單位, 達5單位為留級.)
	          		// 不及格科目共?單位, 次學年仍留xxx年級.
	          		
	          		$thisComment = $eReportCard['Template']['FinalCommentArr']['ExceededSubjectFailUnit'];
					$thisComment = str_replace('<!--FailedSubjectWeight-->', $thisSubjectFailedWeight, $thisComment);
					$thisComment = str_replace('<!--FormName-->', $retainFormName, $thisComment);
					$CommentArr[$thisStudentID][] = $thisComment;
				}
				else if (count((array)$thisSubjectFailedSubjectAssoArr) > 0) {
					// 條件3
	         		// 應補考xx, xx
	         		// 應入夏令班補修
	         		
					$CommentArr[$thisStudentID][] = str_replace('<!--SubjectNameList-->', implode(', ',(array)$thisSubjectFailedSubjectAssoArr), $eReportCard['Template']['FinalCommentArr']['ReExamSubject']);
					$CommentArr[$thisStudentID][] = $eReportCard['Template']['FinalCommentArr']['AttendSummerClass'];
				}
				else {
					// 條件4
	         		// 次學年准升xxxx年級
	         		
	         		$CommentArr[$thisStudentID][] = str_replace('<!--FormName-->', $promotionFormName, $eReportCard['Template']['FinalCommentArr']['PromoteToForm']);
				}
			}
		}
		
		return $CommentArr;
	}
}
?>