<?php
# Editing by Bill

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "promotion", "english_as_a_lang", "reading_academy", "remark");
		
		// F1 - F3 : Phy, Chem, Bio
		$this->SpecialSubjectList = array("045", "070", "315");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 0;
		$this->IsEnableMarksheetFeedback = 0;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		$this->absentSubjectAry = array();
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID='', $StudentID='', $SubjectID='', $SubjectSectionOnly='', $PrintTemplateType='') {
		global $eReportCard;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$isMainReport = $ReportSetting['isMainReport'];
		}
		
		// Semester / Consolidate Report - A3 Size
		if($isMainReport){
			$TableLeft = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableLeft .= "<tr><td>".$TitleTable."</td></tr>";
			$TableLeft .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableLeft .= "<tr><td>".$MSTable."</td></tr>";
			$TableLeft .= "<tr><td>".$MiscTable[0]."</td></tr>";
			$TableLeft .= "</table>";
			
			$TableRight = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
			$TableRight .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableRight .= "<tr><td height='805px' valign='top'>".$MiscTable[1]."</td></tr>";
			$TableRight .= "<tr><td valign='bottom'>".$SignatureTable."</td></tr>";
			$TableRight .= "</table>";
			
			$x = "";
			$x .= "<tr><td>";
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					$x .= "<tr><td width='49%' valign='top'>".$TableLeft."</td><td width='2%'>&nbsp;</td><td width='49%' valign='top'>".$TableRight."</td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
		}
		// Test Report - A4 Size
		else{
			$TableTop = "<table class='report_whole_table' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
			$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
			$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
			$TableTop .= "<tr><td>".$MSTable."</td></tr>";
			$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
			$TableTop .= "</table>";
			
			$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
			$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
			$TableBottom .= $FooterRow;
			$TableBottom .= "</table>";
			
			$x = "";
			$x .= "<tr><td>";
				$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
					//$x .= "<tr height='865px' valign='top'><td>".$TableTop."</td></tr>";
					$x .= "<tr height='899px' valign='top'><td>".$TableTop."</td></tr>";
					$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
				$x .= "</table>";
			$x .= "</td></tr>";
		}
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='', $PrintTemplateType='') {
		global $eReportCard;
		
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$isExtraReport = !$ReportSetting['isMainReport'];
			//$ReportTitle =  $ReportSetting['ReportTitle'];
			//$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			//$ReportTitle = str_replace(" ", "&nbsp;", $ReportTitle);
			$SemID 		 = $ReportSetting['Semester'];
			$ReportType  = $SemID == "F" ? "W" : "T";
			$term_seq 	 = $this->Get_Semester_Seq_Number($SemID);

			// Increase Photo Size
			$photo_extra = $isExtraReport? "test_photo" : "";
			// Increase line spacing
			$title_class = $isExtraReport? "_extra" : "";
			
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			//$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			//if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
				
			# get school name
			$SchoolName = $eReportCard['Template']['SchoolTitleCh'];
			$SchoolName .= "<br>";
			$SchoolName .= $eReportCard['Template']['SchoolTitleEn'];
			
			# get report title
			//$SemCh = $this->returnSemesters($SemID,'b5');
			//$SemEn = $this->returnSemesters($SemID,'en');
			// Only Test Report with Header
			if($isExtraReport){
				$sem_title = $eReportCard['Template']['Term'.$term_seq.'Ch']." ".$eReportCard['Template']['TestTitleCh'];
				$sem_title .= "<br>";
				$sem_title .= $eReportCard['Template']['Term'.$term_seq.'En']." ".$eReportCard['Template']['TestTitleEn'];
				
				$year_title = $eReportCard['Template']['AcademicYearEn']." ".$this->schoolYear." ".$eReportCard['Template']['AcademicYearCh'];
//			}
//			else {
////				$sem_title = $eReportCard['Template']['MSTable'][$KeyCh]."(".$eReportCard['Template']['MSTable'][$KeyEn].")";
//				if($ReportType=="W"){
//					$sem_title = $eReportCard['Template']['MSTable']['AnnualCh']."(".$eReportCard['Template']['MSTable']['AnnualEn'].")";
//				}
//				else{
//					$sem_title = "$SemCh ($SemEn)";
//				}
//			}
			
				# Build table
				$TitleTable = "<table class='report_header' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				$TitleTable .= "<tr>";
			
//				$TitleTable .= "<td class='header_photo_td' align='center'><img class='$photo_extra' src=\"$SchoolLogo\"></td>";
				
				$TitleTable .= "<td>";
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='test_report_header1'>".$eReportCard['Template']['SchoolTitleCh']."</td></tr>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='test_report_header2'>".$eReportCard['Template']['SchoolTitleEn']."</td></tr>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='test_report_header3'>".$eReportCard['Template']['Term'.$term_seq.'Ch']." ".$eReportCard['Template']['TestTitleCh']."</td></tr>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='test_report_header4'>".$eReportCard['Template']['Term'.$term_seq.'En']." ".$eReportCard['Template']['TestTitleEn']."</td></tr>";
						$TitleTable .= "<tr><td nowrap='nowrap' class='test_report_header3'>$year_title</td></tr>";
					$TitleTable .= "</table>";
				$TitleTable .= "</td>";
							
//				$TitleTable .= "<td class='header_address'>";
//					$TitleTable .= "葵涌安捷街十三至二十一號";
//					$TitleTable .= "<br>";
//					$TitleTable .= "13-21, ON CHIT STREET";
//					$TitleTable .= "<br>";
//					$TitleTable .= "SHEK YAM, N.T.";
//					$TitleTable .= "<br>";
//					$TitleTable .= "HONG KONG";
//				$TitleTable .= "</td>";
				
				$TitleTable .= "</tr>";
				$TitleTable .= "</table>";
			}
		}
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='', $forPage3='', $PageNum=1, $PrintTemplateType='') {
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$isExtraReport = !$ReportSetting['isMainReport'];
			$SemID 		   = $ReportSetting['Semester'];
			$ReportType    = $SemID == "F" ? "W" : "T";
			$term_seq 	   = $SemID == "F" ? 2 : $this->Get_Semester_Seq_Number($SemID);
			$ClassLevelID  = $ReportSetting['ClassLevelID'];
			$FormNumber    = $this->GET_FORM_NUMBER($ClassLevelID, 1);
			
			# Retrieve Required Variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			
			# Retrieve Data Info
			if($StudentID){
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$lu = new libuser($StudentID);
				
				$data['Name'] = $lu->UserName2Lang("b5", 2);
				$data['STRN'] = str_replace("#", "", $lu->WebSamsRegNo);
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$data['Class'] = $StudentInfoArr[0]['ClassName'];
				$data['ClassNo'] = $StudentInfoArr[0]['ClassNumber'];
				$data['ClassNo2'] = $StudentInfoArr[0]['ClassNumber'];
			}
			$data['SchoolYear'] = $this->GET_ACTIVE_YEAR_NAME();
			$data['Term'] = $eReportCard['Template']['Term'.$term_seq.'Ch'];
			if($ReportType=="W" && $FormNumber==6){
				$data['Term'] = $eReportCard['Template']['FinalCh'];
			}
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			
			if($isExtraReport){
				//$data['Name'] = $lu->ChineseName." ".$lu->EnglishName;
				$StudentTitleArray = array("Name", "Class", "ClassNo", "STRN", "DateOfIssue");
				$StudentInfoTableCol = 5;
				
				$info_table_class = "_test";
			}
			else{
				$StudentTitleArray = array("Name", "Class", "ClassNo2", "STRN", "SchoolYear", "Term", "DateOfIssue");
				$StudentInfoTableCol = 7;
				
				$info_table_class = "";
			}
			
			# Build Student Info Table
			$StudentInfoTable .= "<table class='info_table$info_table_class' width='100%' border='1' cellpadding='0' cellspacing='0' align='center'>";
			$StudentInfoTable .= "<tr>";
			
			$count = 0;
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				$SettingIDChi = $SettingID."Ch";
				$SettingIDEng = $SettingID."En";
				
				// set width for student info width
				if($isExtraReport){
					if($count%$StudentInfoTableCol==0)	$title_width = 45;
					if($count%$StudentInfoTableCol==1)	$title_width = 10;
					if($count%$StudentInfoTableCol==2)	$title_width = 17;
					if($count%$StudentInfoTableCol==3)	$title_width = 14;
					if($count%$StudentInfoTableCol==4)	$title_width = 14;
				}
				else{
					if($count%$StudentInfoTableCol==0)	$title_width = 42;
					if($count%$StudentInfoTableCol==1)	$title_width = 6.5;
					if($count%$StudentInfoTableCol==2)	$title_width = 5.5;
					if($count%$StudentInfoTableCol==3)	$title_width = 10;
					if($count%$StudentInfoTableCol==4)	$title_width = 12;
					if($count%$StudentInfoTableCol==5)	$title_width = 9;
					if($count%$StudentInfoTableCol==6)	$title_width = 15;
				}
				$count++;
				
				$Title = $eReportCard['Template']['StudentInfo'][$SettingIDChi]."<br>".$eReportCard['Template']['StudentInfo'][$SettingIDEng];
				$StudentInfoTable .= "<td class='tabletext' width='{$title_width}%' height='{$LineHeight}' valign='middle'>$Title</td>";
			}
			$StudentInfoTable .= "</tr>";
			$StudentInfoTable .= "<tr>";
			
			$count = 0;
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				$studentdata = $data[$SettingID] ? $data[$SettingID] : $defaultVal;
				
				$td_style = "";
				if($SettingID=="Name"){
					$td_style = " style='text-align: left'";
				}
				if($SettingID=="DateOfIssue" && $studentdata!=""){
					$studentdata = date("d/m/Y", strtotime($studentdata));	
				}
				
				$StudentInfoTable .= "<td class='tabletext' $td_style>".$studentdata."</td>";
			}
			$StudentInfoTable .= "</tr>";
			$StudentInfoTable .= "</table>";
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='', $PrintTemplateType='', $PageNum='') {
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$isExtraReport = !$isMainReport;
		$SemID = $ReportSetting['Semester'];
		$term_seq = $this->Get_Semester_Seq_Number($SemID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight = $ReportSetting['LineHeight'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectComponent = $ReportSetting['ShowSubjectComponent'];
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# Retrieve Table Header
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;
		
		# Retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# Retrieve SubjectCode-SubjectID Mapping - for subject code checking
		if($isExtraReport){
			$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		}
		
		# Retrieve Subject Columns		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# Retrieve Marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, '', 0, 1);
		
		# Retrieve Marks HTML
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# Retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		// Font Size (11pt) for Term Test Report
		$font_style = $isExtraReport? "table_13pt_font" : "";
		$content_class = $isExtraReport? "_test" : "";
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='2' class='report_content$content_class $font_style'>";
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		$displaySubjCount = 0;
		for($i=0; $i<$sizeofSubjectCol; $i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof((array)$MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach((array)$MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			
			// component subject
			if(in_array($thisSubjectID, (array)$MainSubjectIDArray)!=true){
				$isSub=1;
				$parentSubjectID = $this->GET_PARENT_SUBJECT_ID($thisSubjectID);
			}
			
			// component subject display checking
			// hide component - template settings
			if ($ShowSubjectComponent==0 && $isSub)
				continue;
			// hide component - template flag - HideComponentSubject
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			// hide component - Term Test Report and Parent subjects in ExcludeSubjCompAry
			if($isExtraReport && in_array($SubjectCodeMappping[$parentSubjectID], (array)$eRCTemplateSetting['Report']['ReportGeneration']['ExcludeSubjCompAry']) && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				$DetailsTable .= "</tr>";
				
				$isFirst = 0;
				$displaySubjCount++;
			}
		}

		// add empty row to subject table
		if($isMainReport && $displaySubjCount < 22)
		{
			$SubjEmptyColumn = $ColNum - 1;
			
			for($i=0; $i<(22-$displaySubjCount); $i++)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<td class='border_left' colspan='3'>&nbsp;</td>";
//				$DetailsTable .= "<td>&nbsp;</td>";
//				$DetailsTable .= "<td>&nbsp;</td>";
				for($j=0; $j<$SubjEmptyColumn; $j++)
				{
					$border_style = "";
					if($j==($SubjEmptyColumn-1)){
						$border_style = "border_right";
					}
					$DetailsTable .= "<td class='border_left $border_style'>&nbsp;</td>";
				}
				$DetailsTable .= "</tr>";
			}
		}
		else if($isExtraReport && $displaySubjCount < 11)
		{
			for($i=0; $i<(11-$displaySubjCount); $i++)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<td class='border_left' colspan='2'>&nbsp;</td>";
				$DetailsTable .= "<td class='border_left'>&nbsp;</td>";
				$DetailsTable .= "<td class='border_left'>&nbsp;</td>";
				$DetailsTable .= "</tr>";
			}
		}
		
		# MS Table Footer
		$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		
		# Merit and ECA Table
		//$DetailsTable .= $this->getMeritAndECATable($ReportID, $StudentID);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID) {
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
			
		$RankTitle = $eReportCard['Template']['MSTable']['FormRankCh']."<br>".$eReportCard['Template']['MSTable']['FormRankEn'];
		if($FormNumber > 3)	
		{
			$RankTitle = $eReportCard['Template']['MSTable']['ClassRankCh']."<br>".$eReportCard['Template']['MSTable']['ClassRankEn'];
		}
				
		$n = 0;
		$e = 0;	# column within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		
		$colwidth = 8;
		$fullmark_width = 8;
		$semester_width = 0;
		
		#########################################################
		# Marks START
		#########################################################
		
		$x = "";
		$row2 = "";
		if($ReportType=="T")	# Term Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
//			$semester_name = $this->returnSemesters($SemID, "en");
//			
//			$ColumnID = array();
//			$ColumnTitle = array();
//			if (sizeof($ColoumnTitle) > 0)
//				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			
			$e = 0;
			if($isExtraReport)
			{
				$subject_width = 56;
				$subject_colspan = 2;
				
				$row1 .= "<td class='border_left border_bottom' align='center' width='19%'>".$eReportCard['Template']['MarkCh']."<br><span class='font_12pt'>".$eReportCard['Template']['MarkEn']."</span></td>";
				$row1 .= "<td class='border_left border_bottom' align='center' width='24%'>".$eReportCard['Template']['FormPositionCh']."<br><span class='font_12pt'>".$eReportCard['Template']['FormPositionEn']."</span></td>";
				$n = $n+2;
				$e = $e+2;
			}
			else
			{
//				if($FormNumber != 6)
//				{
//					$row1 .= "<td class='border_left' align='center' $td_width>".$eReportCard['Template']['MSTable']['TermTestCh']."</td>";
////					$row1 .= "<td align='center' $td_width>".$semester_name."</td>";
//					$row1 .= "<td align='center' $td_width>".$eReportCard['Template']['MSTable']['TermTestEn']."</td>";
//					$row2 .= "<td class='border_top border_left border_bottom' align='center' $td_width>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
//					$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
//					$n++;
//	 				$e++;
//				}
				$subject_width = 52;
				$subject_colspan = 3;
 				$td_width = 12;
 				
 				$border_style = "border_top";
 				
				//for($i=0;$i<sizeof($ColoumnTitle);$i++)
				//{
				$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['TermMarkCh']."<br>".$eReportCard['Template']['TermMarkEn']."</td>";
				$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['ExamMarkCh']."<br>".$eReportCard['Template']['ExamMarkEn']."</td>";
				$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['TermAvgCh']."<br>".$eReportCard['Template']['TermAvgEn']."</td>";
				$row1 .= "<td class='border_left border_right border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['FormPositionCh']."<br><span class='font_12pt'>".$eReportCard['Template']['FormPositionEn']."</span></td>";
				$n = $n + 4;
 				$e = $e + 4;
				//}
				
				$x = "<tr><td class='tabletext' colspan='".($n+3)."'>".$eReportCard['Template']['AcademicResultCh']." ".$eReportCard['Template']['AcademicResultEn']."</td></tr>";				
			}
			
//			$semester_width = $colwidth * 2 * $n;
//			if($isMainReport && $this->Get_Semester_Seq_Number($SemID) == 2)
//			{
//				$semester_width += $colwidth * 2;
//			}
		}
		else	# Whole Year Report Type
		{
//			if($FormNumber == 6)
//			{
//				$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
////				$semester_name = $this->returnSemesters($termReport[0]["Semester"], "en");
//				$ColoumnTitle = $this->returnReportTemplateColumnData($termReport[0]["ReportID"]);
//				
////				$ColumnID = array();
////				$ColumnTitle = array();
////				if (sizeof($ColoumnTitle) > 0)
////					foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
//				
//				$td_width = "width='$colwidth%'";
//				
//				$e = 0;
//				for($i=0;$i<sizeof($ColoumnTitle);$i++)
//				{
//					$row2 .= "<td class='border_top border_left border_bottom' align='center' $td_width>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
//					$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
//					$n++;
//	 				$e++;
//				}
//			}
//			else
//			{
//				$ColumnData = $this->returnReportTemplateColumnData($ReportID);debug_pr($ColumnData);
//	 			$needRowspan=0;
//				for($i=0;$i<sizeof($ColumnData);$i++)
//				{
//					$td_width = "width='$colwidth%'";
//					$colspan = "colspan='2'";
//					
//					$SemNameB5 = $this->returnSemesters($ColumnData[$i]['SemesterNum'], "b5");
//					$SemNameEN = $this->returnSemesters($ColumnData[$i]['SemesterNum'], "en");
//					
////					$row1 .= "<td {$colspan} width='".($colwidth * 2)."%' height='{$LineHeight}' class='border_left' align='center'>".$SemName."</td>";
//					$row1 .= "<td class='border_left' align='center' $td_width>". $SemNameB5 ."</td>";
//					$row1 .= "<td align='center' $td_width>". $SemNameEN ."</td>";
//					$row2 .= "<td class='border_top border_left border_bottom' align='center' $td_width>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
//					$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
//					$n++;
//					$e++;
//				}
				
				$subject_width = 45;
				$subject_colspan = 3;
 				$td_width = 11;
 				
 				$border_style = "border_top";
 				
				//for($i=0;$i<sizeof($ColoumnTitle);$i++)
				//{
				$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['TermMarkCh']."<br>".$eReportCard['Template']['TermMarkEn']."</td>";
				$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['ExamMarkCh']."<br>".$eReportCard['Template']['ExamMarkEn']."</td>";
				$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['TermAvgCh']."<br>".$eReportCard['Template']['TermAvgEn']."</td>";
				if($FormNumber!=6){
					$row1 .= "<td class='border_left border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['FormPositionCh']."<br><span class='font_12pt'>".$eReportCard['Template']['FormPositionEn']."</span></td>";
					$row1 .= "<td class='border_left border_right border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['YearAvgCh']."<br><span class='font_12pt'>".$eReportCard['Template']['YearAvgEn']."</span></td>";
					$n = $n + 5;
 					$e = $e + 5;
				}
				else{
					$row1 .= "<td class='border_left border_right border_top border_bottom' align='center' width='$td_width%'>".$eReportCard['Template']['FormPositionCh']."<br><span class='font_12pt'>".$eReportCard['Template']['FormPositionEn']."</span></td>";
					$n = $n + 4;
 					$e = $e + 4;
				}
//			}
			$semester_width = $colwidth * 2 * ($n + 1);
		}
		#########################################################
		# Marks END
		#########################################################

		if(!$needRowspan)
		{
			$row1 = str_replace("rowspan='2'", "", $row1);
		}
		
		$x .= "<tr>";
		
		# Subject
//		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
//		for($i=0; $i<sizeof($SubjectColAry); $i++)
//		{
//			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
//			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
//			$SubjectTitle = $SubjectColAry[$i];
//			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
//			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
		$x .= "<td colspan='$subject_colspan' valign='middle' class='border_left border_bottom $border_style' height='{$LineHeight}' width='$subject_width%' align='center'>".$eReportCard['Template']['SubjectChn']."<br><span class='font_12pt'>".$eReportCard['Template']['SubjectEng']."</span></td>";
		$n++;
//		}
//		
		# Full Mark
//		$x .= "<td {$Rowspan} valign='middle' class='border_left border_bottom' align='center' width='$fullmark_width%'>". $eReportCard['Template']['MSTable']['FullMarksCh'] ."<br>". $eReportCard['Template']['MSTable']['FullMarksEn'] ."</td>";
//		$e++;
		
		# Marks
		$x .= $row1;
		
//		# Subject Overall 
//		if($ShowRightestColumn)
//		{
//			$x .= "<td valign='middle' class='border_left' align='center' $td_width>". $eReportCard['Template']['MSTable']['AnnualCh'] ."</td>";
//			$x .= "<td valign='middle' align='center' $td_width>". $eReportCard['Template']['MSTable']['AnnualEn'] ."</td>";
//			$row2 .= "<td class='border_top border_left border_bottom' align='center' $td_width>".$eReportCard['Template']['MSTable']['ResultCh']."<br>".$eReportCard['Template']['MSTable']['ResultEn']."</td>";
//			$row2 .= "<td class='border_top border_bottom' align='center' $td_width>".$RankTitle."</td>";
//			$n++;
//		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID) {
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$isExtraReport = !$isMainReport;
		$ReportType = $ReportSetting['Semester'];
		$LineHeight = $ReportSetting['LineHeight'];
		
		# Retrieve SubjectID Array
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
		
		if($isMainReport){
			# Loading Report Template Data
			$column_data = $this->returnReportTemplateColumnData($ReportID); 		// Column Data
			$ColumnSize = sizeof($column_data);
			
			# Load Settings - Calculation
			// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
			$SettingCategory = 'Calculation';
			$categorySettings = $this->LOAD_SETTING($SettingCategory);
			$TermCalculationType = $categorySettings['OrderTerm'];
			$FullYearCalculationType = $categorySettings['OrderFullYear'];
			
			# Get Existing Report Calculation Method
			$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
			
			$col_1_width = 32;
			$col_2_width = 6;
			$col_3_width = 14;
			if($ReportType == "F"){
				$col_1_width = 27;
				$col_2_width = 6;
				$col_3_width = 12;
			}
		}
		
		$isFirst = 1;
 		$x = array();
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
				# Get Component Subject Percentage
				$WeightAry = array();
				$WeightAry[0] = 0;
	 			if($isMainReport && count($Ary)>1){
					if($CalculationType == 2){
						if(count($column_data) > 0){
					    	//for($i=0 ; $i<sizeof($column_data) ; $i++){
				           	foreach($Ary as $SubSubjectID => $Subjs){
				           		if($SubSubjectID==0){
				           			continue;
				           		}
		            			$OtherCondition = "SubjectID = '$SubSubjectID' AND ReportColumnID = ".$column_data[0]['ReportColumnID'];
								$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
								$weight_data = $weight_data[0]['Weight']? $weight_data[0]['Weight'] : 0;
					            $WeightAry[$SubSubjectID] = $weight_data;
					            $WeightAry[0] += $weight_data;
					        }
							//}
						}
					}
					else {
			           	foreach($Ary as $SubSubjectID => $Subjs){
			           		if($SubSubjectID==0){
			           			continue;
			           		}
			            	$OtherCondition = "SubjectID = '$SubSubjectID' AND ReportColumnID IS NULL";
							$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
							$weight_data = $weight_data[0]['Weight']? $weight_data[0]['Weight'] : 0;
				            $WeightAry[$SubSubjectID] = $weight_data;
				            $WeightAry[0] += $weight_data;
			           	}
					}
	 			}
				
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$SbjSpan = 1;
			 		// Indentation for component subject 
			 		$Prefix = "　　";
			 		
			 		$isParentSubject = false;
			 		$isCompSubject = true;
			 		if($SubSubjectID==0)		# Main Subject
			 		{
			 			// remove Indentation for main subject
				 		$Prefix = "";
				 		$mainSbjSpan = " colspan='3'";
				 		
				 		$SubSubjectID=$SubjectID;
				 		$isCompSubject = false;
			 			$isParentSubject = count($Ary)>1;
			 			if($isParentSubject){
				 			$mainSbjSpan = "";
			 			}
			 		}

//			 		foreach($SubjectDisplay as $k=>$v)
//			 		{
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				
		 				if($isExtraReport){
				 			$t .= "<td class='tabletext border_left {$css_border_top}' height='{$LineHeight}' valign='middle' width='20%'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
								$t .= "<tr><td height='{$LineHeight}'class='tabletext'>$SubjectChn</td></tr>";
							$t .= "</table>";
							$t .= "</td>";
			 				
				 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
								$t .= "<tr><td height='{$LineHeight}'class='tabletext'>$SubjectEng</td></tr>";
							$t .= "</table>";
							$t .= "</td>";
		 				}
		 				else{
				 			$t .= "<td class='tabletext border_left {$css_border_top}' height='{$LineHeight}' valign='middle' width='$col_1_width%' $mainSbjSpan>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
								$t .= "<tr><td height='{$LineHeight}'class='tabletext'>$Prefix$SubjectChn $SubjectEng</td></tr>";
							$t .= "</table>";
							$t .= "</td>";
			 				
			 				if($isParentSubject){
					 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle' width='$col_2_width%'>&nbsp;</td>";
					 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle' width='$col_3_width%'>".$eReportCard['Template']['TotalMarksCh']." ".$eReportCard['Template']['TotalMarksEn']."</td>";
			 				}
			 				else if($isCompSubject){
			 					$cmpSubjectPerCentage = ($WeightAry[0]>0)? intval($WeightAry[$SubSubjectID] / $WeightAry[0] * 100) : $this->EmptySymbol;
			 					
					 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle' width='$col_2_width%' align='right'>($cmpSubjectPerCentage%)</td>";
					 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle' width='$col_3_width%'>&nbsp;</td>";
				 			}
		 				}
//			 		}
					$x[] = $t;
					$isFirst = 0;
				}
		 	}
		}
 		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array()) {
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark 		= $ReportSetting['ShowSubjectFullMark'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$ShowSubjectOverall 		= $ShowRightestColumn;
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		# Retrieve Display Settings
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		# Retrieve SubjectID Array
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		
		# Retrieve SubjectCode-SubjectID Mapping - for subject code checking
		$SubjectCodeMappping = $this->GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		$isFirst = 1;
		
		$n = 0;
		$x = array();
		if($ReportType=="T")	# Term Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			# Retrieve Column Info
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0){
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			}
							
			if($isMainReport && $FormNumber!=6)
			{
				# Retrieve Extra Report Infomation
				$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
				$extraReportID = $extraReportInfo["ReportID"];
				$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
			
				$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
				if(sizeof($extraColoumnTitle) > 0){
					foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
						break;
					}
				}
			}
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				// italic for component subject
				$prefix = "<i>";
				$postfix = "</i>";
				$CmpSubjectArr = array();
				$isParentSubject = 0;
				// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					// remove italic for main subject
					$prefix = "";
					$postfix = "";
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				// check if Subject need to display order of Class
				$displayClassOrder = in_array($SubjectCodeMappping[$SubjectID], (array)$eRCTemplateSetting['Report']['ReportGeneration']['NotSubjGroupOrderList']);
		
				# Retrieve Subject Scheme Settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				$isAllNA = true;
				
				# Retrieve Full Mark
	  			$thisFullMark = $SubjectFullMarkAry[$SubjectID];
//	  			$SpFullMarkTmp = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, 0);
//	  			$SpFullMark = ($SpFullMarkTmp) ? $SpFullMarkTmp[0] : "";
	  			$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID IS NULL and SubjectID = '$SubjectID' ");
				$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
				
				$FullMark = $ScaleDisplay=="M"? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
				
//				$fullmark_align = "right";
//				if($isExtraReport)
//				{
//					$fullmark_align = "center";
//				}
//				$x[$SubjectID] .= "<td class='border_left' align='$fullmark_align'>". $FullMark .$postfix."&nbsp;</td>";
//				$x[$SubjectID] .= "<td class='border_left' align='center'>".$prefix.$FullMark.$postfix."</td>";
				
				# Display Marks 
				for($i=0;$i<sizeof($ColumnID);$i++)
				{
					if($i==2)	break;
					
					$thisColumnID = $ColumnID[$i];
					$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
					$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
					$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
					
					$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);

					if ($isSub && $columnSubjectWeightTemp == 0)
					{
						$thisMarkDisplay = $this->EmptySymbol;
					} 
					else
					{
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID='".$ColumnID[$i] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
						
						$thisMSGrade = $MarksAry[$SubjectID][$ColumnID[$i]]['Grade'];
						$thisMSMark = $MarksAry[$SubjectID][$ColumnID[$i]]['Mark'];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						# Retrieve Order
//						if($FormNumber > 3)
//						{
//							if($displayClassOrder){
//								$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritClass'];
//								$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['ClassNoOfStudent'];
//							}
//							else{
//								$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritSubjectGroup'];
//								$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['SubjectGroupNoOfStudent'];
//							}
//							$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//							if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//							{
//								$orderDisplay = $this->EmptySymbol;
//							}
//						} 
//						else 
//						{
//							$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritForm'];
//							$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['FormNoOfStudent'];
//							$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//							if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
//							{
//								$orderDisplay = $this->EmptySymbol;
//							}
//						}
						$thisOrder = $MarksAry[$SubjectID][$ColumnID[$i]]['OrderMeritForm'];
						$orderTotalNumber = $MarksAry[$SubjectID][$ColumnID[$i]]['FormNoOfStudent'];
						//$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
						$orderDisplay = $this->Get_Ranking_Display($thisOrder, $orderTotalNumber, 1, 1);
						
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						# store Subject ID if absent
						if(in_array($thisMark, array("+", "-", "abs")))
						{
							$this->absentSubjectAry[] = $SubjectID;
						}
						
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID[$i]]['Grade']);
						
						if($needStyle)
						{
							if ($ScaleDisplay=="M" && $thisSubjectWeight>0)
							{
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							}
							else
							{
								$thisMarkTemp = $thisMark;
							}
							
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
						{
							$thisMarkDisplay = $thisMark;
						}
						
					//	$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					//	if($ScaleDisplay=="M" && $ScaleDisplay=="G"$SchemeInfo['TopPercentage'] == 0)
					//	$thisMarkDisplay = ($ScaleDisplay=="M" && strlen($thisMark)) ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID) : $thisMark; 
					}
					$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix;
  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
  					
  					if($isExtraReport){
  						$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$orderDisplay</td>";
  					}
					//$x[$SubjectID] .= "</td>";
					
					// only display first column if is Term Test Report
					if($isExtraReport) break;
				}
				
				if($isMainReport)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# Retrieve Order
//					if($FormNumber > 3)
//					{
//						if($displayClassOrder){
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritClass'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
//						}
//						else{
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['SubjectGroupNoOfStudent'];
//						}
//						$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//						{
//							$orderDisplay = $this->EmptySymbol;
//						}
//					} 
//					else 
//					{
//						$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
//						$orderTotalNumber = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
//						$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
//						{
//							$orderDisplay = $this->EmptySymbol;
//						}
//					}
					$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$orderTotalNumber = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
					//$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
					$orderDisplay = $this->Get_Ranking_Display($thisOrder, $orderTotalNumber, 1, 1);
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
					
					if(in_array($thisMark, array("+", "-", "abs")))
					{
						$this->absentSubjectAry[] = $SubjectID;
					}
						
					# check special case
//					if ($CalculationMethod==2 && $isSub)
//					{
//						$thisMarkDisplay = $this->EmptySymbol;
//					}
//					else
//					{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
						}
						else
							$thisMarkDisplay = $thisMark;
//					}
					$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
					
  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
  					$x[$SubjectID] .= "<td class='border_left border_right tabletext' align='center'>$orderDisplay</td>";
				} 
//				else
//				{
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}			
					
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
				$isFirst = 0;
			}
		} 		
		# Term Report Type [End]
		
		# Whole Year Report Type
		else
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Terms
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			# See if any term reports available
			$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$ColumnData[(sizeof($ColumnData)-1)]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
			$thisReportID = $thisReport['ReportID'];
			$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
				
			$thisMarksAry = $this->getMarks($thisReportID, $StudentID,"","",1);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				// italic for component subject
				$prefix = "<i>";
				$postfix = "</i>";
				$CmpSubjectArr = array();
				$isParentSubject = 0;		
				// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					// remove italic for component subject
					$prefix = "";
					$postfix = "";
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				// check if Subject need to display order of Class
				$displayClassOrder = in_array($SubjectCodeMappping[$SubjectID], (array)$eRCTemplateSetting['Report']['ReportGeneration']['NotSubjGroupOrderList']);
				
				$isAllNA = true;
				
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $thisReportID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				# Retrieve Full Mark
	  			$thisFullMark = $SubjectFullMarkAry[$SubjectID];
	  			$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID IS NULL and SubjectID = '$SubjectID' ");
				$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
				//$FullMark = $ScaleDisplay=="M"? ($UseWeightedMark ? $thisFullMark * $thisSubjectWeight : $thisFullMark) : $thisFullMark;
				
				//$x[$SubjectID] .= "<td class='border_left' align='center'>".$prefix.$FullMark.$postfix."</td>";
				
				# Terms's Assesment / Terms Result
//				for($i=0; $i<sizeof($ColumnData); $i++)
//				{						
						$ColumnID = array();
						$ColumnTitle = array();
						foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
						
						for($j=0;$j<sizeof($ColumnID);$j++)
						{
							if($j==2)	break;
							
							$thisColumnID = $ColumnID[$j];
							$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
							$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
							$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
							
							$isAllCmpZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $SubjectID);
							$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
		
							if ($isSub && $columnSubjectWeightTemp == 0)
							{
								$thisMarkDisplay = $this->EmptySymbol;
							} 
							else
							{
								$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnID[$j] ."' and SubjectID = '$SubjectID' ");
								$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
								
								$thisMSGrade = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade'];
								$thisMSMark = $thisMarksAry[$SubjectID][$ColumnID[$j]]['Mark'];
								
								$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
								$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";

								$thisOrder = $thisMarksAry[$SubjectID][$ColumnID[$j]]['OrderMeritForm'];
								$orderTotalNumber = $thisMarksAry[$SubjectID][$ColumnID[$j]]['FormNoOfStudent'];
								//$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
								$orderDisplay = $this->Get_Ranking_Display($thisOrder, $orderTotalNumber, 1, 1);
								
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
									$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
								}
								
								$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
								if ($thisMark != "N.A.")
								{
									$isAllNA = false;
								}
								
								# store Subject ID if absent
								if(in_array($thisMark, array("+", "-", "abs")))
								{
									$this->absentSubjectAry[] = $SubjectID;
								}
								
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][$ColumnID[$j]]['Grade']);
								
								if($needStyle)
								{
									if ($ScaleDisplay=="M" && $thisSubjectWeight>0)
									{
										$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
									}
									else
									{
										$thisMarkTemp = $thisMark;
									}
									
									$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
								}
								else
								{
									$thisMarkDisplay = $thisMark;
								} 
							}
							
							$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix;
		  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
						}
				
						$thisMSGrade = $thisMarksAry[$SubjectID][0]['Grade'];
						$thisMSMark = $thisMarksAry[$SubjectID][0]['Mark'];
	
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
						
						$thisOrder = $thisMarksAry[$SubjectID][0]['OrderMeritForm'];
						$orderTotalNumber = $thisMarksAry[$SubjectID][0]['FormNoOfStudent'];
						//$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
						$orderDisplay = $this->Get_Ranking_Display($thisOrder, $orderTotalNumber, 1, 1);
					
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
											
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
						
						if(in_array($thisMark, array("+", "-", "abs")))
						{
							$this->absentSubjectAry[] = $SubjectID;
						}
							
						# check special case
	//					if ($CalculationMethod==2 && $isSub)
	//					{
	//						$thisMarkDisplay = $this->EmptySymbol;
	//					}
	//					else
	//					{
							list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisMarksAry[$SubjectID][0]['Grade']);
							if($needStyle)
							{
		 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
									$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
								else
									$thisMarkTemp = $thisMark;
									
								$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
							}
							else
								$thisMarkDisplay = $thisMark;
	//					}
						$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
						
						$orderDisplay_style = $FormNumber==6? "border_right" : "";
						
	  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
	  					$x[$SubjectID] .= "<td class='border_left $orderDisplay_style tabletext' align='center'>$orderDisplay</td>";
//					}
//					else 
//					{
//						# if no term reports, the term mark should be entered directly
//						if (empty($thisReport))
//						{
//							$thisReportID = $ReportID;
//							$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
//							$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
//							
//							$thisMSGrade = $thisMarksAry[$SubjectID][$thisReportColumnID]["Grade"];
//							$thisMSMark = $thisMarksAry[$SubjectID][$thisReportColumnID]["Mark"];
//							
//							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//							
//							# Retrieve Order
//							if($FormNumber > 3)
//							{
//								if($displayClassOrder){
//									$thisOrder = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritClass'];
//									$orderTotalNumber = $MarksAry[$SubjectID][$thisReportColumnID]['ClassNoOfStudent'];
//								}
//								else{
//									$thisOrder = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritSubjectGroup'];
//									$orderTotalNumber = $MarksAry[$SubjectID][$thisReportColumnID]['SubjectGroupNoOfStudent'];
//								}
//								$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//								if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass))
//								{ 
//									$orderDisplay = $this->EmptySymbol;
//								}
//							} 
//							else 
//							{
//								$thisOrder = $MarksAry[$SubjectID][$thisReportColumnID]['OrderMeritForm'];
//								$orderTotalNumber = $MarksAry[$SubjectID][$thisReportColumnID]['FormNoOfStudent'];
//								$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//								if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm  > 0 && $thisOrder > $OverallPositionRangeForm))
//								{
//									$orderDisplay = $this->EmptySymbol;
//								}
//							}
//							
//							$reportcolumn_cond = "ReportColumnID='$thisReportColumnID'";
//						} 
//						else 
//						{
//							$thisReportID = $thisReport['ReportID'];
//							$thisReportColumnID = 0;
//							$thisMarksAry = $this->getMarks($thisReportID, $StudentID, "", "", 1);
//							
//							$thisMSGrade = $thisMarksAry[$SubjectID][$thisReportColumnID]["Grade"];
//							$thisMSMark = $thisMarksAry[$SubjectID][$thisReportColumnID]["Mark"];
//							
//							$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
//							$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
//						
//							# Retrieve Order
//							if($FormNumber > 3)
//							{
//								if($displayClassOrder){
//									$thisOrder = $thisMarksAry[$SubjectID][$thisReportColumnID]['OrderMeritClass'];
//									$orderTotalNumber = $thisMarksAry[$SubjectID][$thisReportColumnID]['ClassNoOfStudent'];
//								}
//								else{
//									$thisOrder = $thisMarksAry[$SubjectID][$thisReportColumnID]['OrderMeritSubjectGroup'];
//									$orderTotalNumber = $thisMarksAry[$SubjectID][$thisReportColumnID]['SubjectGroupNoOfStudent'];
//								}
//								$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//								if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass)) 
//								{
//									$orderDisplay = $this->EmptySymbol;
//								}
//							} 
//							else 
//							{
//								$thisOrder = $thisMarksAry[$SubjectID][$thisReportColumnID]['OrderMeritForm'];
//								$orderTotalNumber = $thisMarksAry[$SubjectID][$thisReportColumnID]['FormNoOfStudent'];
//								$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//								if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm  > 0 && $thisOrder > $OverallPositionRangeForm))
//								{
//									$orderDisplay = $this->EmptySymbol;
//								}
//							}
//							
//							$reportcolumn_cond = "ReportColumnID IS NULL";
//						}
//	
//						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "$reportcolumn_cond and SubjectID = '$SubjectID' ");
//						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
//													
//						# for preview purpose
//						if(!$StudentID)
//						{
//							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
//							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
//						}
//						
//						# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
//						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
//						
//						if ($thisMark != "N.A.")
//						{
//							$isAllNA = false;
//						}
//						
//						if(in_array($thisMark, array("+", "-", "abs")))
//						{
//							$this->absentSubjectAry[] = $SubjectID;
//						}
//					
//						# check special case
//						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
//						
//						if($needStyle)
//						{
//							$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
//						}
//						else
//						{
//							$thisMarkDisplay = $thisMark;
//						}
//						$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
//	
//	  					$x[$SubjectID] .= "<td class='border_left tabletext' align='center'>$thisMarkDisplay</td>";
//			  			$x[$SubjectID] .= "<td class='tabletext' align='center'>$orderDisplay</td>";
//					}
//				}
				
				# Subject Overall
				if($ShowSubjectOverall && $FormNumber!=6)
				{
					$thisMSGrade = $MarksAry[$SubjectID][0]["Grade"];
					$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
					
//					# Retrieve Order
//					if($FormNumber > 3)
//					{
//						if($displayClassOrder){
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritClass'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['ClassNoOfStudent'];
//						}
//						else{
//							$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritSubjectGroup'];
//							$orderTotalNumber = $MarksAry[$SubjectID][0]['SubjectGroupNoOfStudent'];
//						}
//						$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
//						if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeClass > 0 && $thisOrder > $OverallPositionRangeClass)) 
//							$orderDisplay = $this->EmptySymbol;
//					}
//					else
//					{
						$thisOrder = $MarksAry[$SubjectID][0]['OrderMeritForm'];
						$orderTotalNumber = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
						//$orderDisplay = "$prefix$thisOrder$postfix / $prefix$orderTotalNumber$postfix";
						//if(!($thisOrder > 0 && $orderTotalNumber > 0) || ($OverallPositionRangeForm > 0 && $thisOrder > $OverallPositionRangeForm))
						//	$orderDisplay = $this->EmptySymbol;
						$orderDisplay = $this->Get_Ranking_Display($thisOrder, $orderTotalNumber, 1, 1);
//					}
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A.") 
					{
						$isAllNA = false;
					}
					
					if(in_array($thisMark, array("+", "-", "abs")))
					{
						$this->absentSubjectAry[] = $SubjectID;
					}

					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
					if($needStyle)
					{
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
					}
					else
					{
						$thisMarkDisplay = $thisMark;
					}
					$thisMarkDisplay = $thisMarkDisplay==$this->EmptySymbol? $thisMarkDisplay : $prefix.$thisMarkDisplay.$postfix; 
					
					$x[$SubjectID] .= "<td class='border_left tabletext border_right' align='center'>$thisMarkDisplay</td>";
//					$x[$SubjectID] .= "<td class='border_left border_right tabletext' align='center'>$orderDisplay</td>";					
					
				} 
//				else 
//				{
//					$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
//				}
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
				
				$isFirst = 0;
			}
		}	
		# Whole Year Report Type [End]
		
		return $returnArr;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array()) {
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
 		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		
		# Retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		if($isExtraReport) return "";
		
		# Retrieve Result Data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1);
		
		$OverallGrandAverage = "";
		//$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "#";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "#";
		//$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		//$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		if($ReportType=="W"){
			# Retrieve Invloved Terms
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			# See if any term reports available
			$thisReport = $this->returnReportTemplateBasicInfo("", "Semester='".$ColumnData[(sizeof($ColumnData)-1)]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
			$thisReportID = $thisReport['ReportID'];

			$term_result = $this->getReportResultScore($thisReportID, 0, $StudentID, '', 0, 1);
			
			$OverallGrandAverage = $GrandAverage;
			$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($term_result['GrandAverage'], $thisReportID, $ClassLevelID, '', '', 'GrandTotal') : "#";
	  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($term_result['OrderMeritForm'], $thisReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
	  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($term_result['FormNoOfStudent'], $thisReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		}
		
		$thisTitleEn = $eReportCard['Template']['WeightAvgEn'];
		$thisTitleCh = $eReportCard['Template']['WeightAvgCh'];
		$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, array($GrandAverage, $FormPosition, $FormNumOfStudent, $OverallGrandAverage), "", 1);
		
//		if($isMainReport && $FormNumber!=6)
//		{	
//			# Retrieve Extra Report (Test) Display Settings
//			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
//			$extraReportID = $extraReportInfo["ReportID"];
//			
//			$extraMarksAry = $this->getMarks($extraReportID, $StudentID,'', 0, 1);
//			
//			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
//			if(sizeof($extraColoumnTitle) > 0){
//				foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
//					break;
//				}
//			}
//				
//			$extraColumn = $extraColumnID[0];
//			
//			# Display Extra Report (Test) Result Column
//			$extraColumnResult = $this->getReportResultScore($extraReportID, $extraColumn, $StudentID, '', 1, 1);
//			
//			$columnTotal[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['GrandTotal'], $extraReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
//			$columnAverage[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['GrandAverage'], $extraReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
//			
//			$columnClassPos[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['OrderMeritClass'], $extraReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
//			$columnFormPos[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['OrderMeritForm'], $extraReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
//			$columnClassNumOfStudent[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['ClassNoOfStudent'], $extraReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
//			$columnFormNumOfStudent[$extraColumn] = $StudentID ? $this->Get_Score_Display_HTML($extraColumnResult['FormNoOfStudent'], $extraReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";	
//		}
		
//		if($ReportType == "W" && $FormNumber == 6)
//		{
//			$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
//			$termReportID = $termReport[0]["ReportID"];
//			$ColumnTitle = $this->returnReportColoumnTitle($termReportID);
//			
//			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName) 
//			{
//				$columnResult = $this->getReportResultScore($termReportID, $ColumnID, $StudentID, '', 1, 1);
//				
//				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $termReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
//				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $termReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
//				
//				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $termReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
//				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $termReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
//				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $termReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
//				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $termReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
//			}
//		}
//		else 
//		{
//			foreach ((array)$ColumnTitle as $ColumnID => $ColumnName) {
//				
//				$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 1,1);
//				
//				$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
//				$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
//				
//				$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
//				$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
//				$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
//				$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
//			
//				// only display first column if is term test report
//				if($isExtraReport) break;
//			}
//		}
//
//		$first = 1;
//		
//		# Overall Result
//		if($ShowGrandTotal)
//		{
//			$thisTitleEn = $eReportCard['Template']['MSTable']['OverallResultEn'];
//			$thisTitleCh = $eReportCard['Template']['MSTable']['OverallResultCh'];
//			$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnTotal, $GrandTotal, $first);
//			
//			$first = 0;
//		}
//		
//		if ($eRCTemplateSetting['HideCSVInfo'] != true)
//		{
//			# Average Mark 
//			if($ShowGrandAvg)
//			{
//				$thisTitleEn = $eReportCard['Template']['MSTable']['AvgMarkEn'];
//				$thisTitleCh = $eReportCard['Template']['MSTable']['AvgMarkCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnAverage, $GrandAverage, $first);
//				
//				$first = 0;
//			}
//			
//			# Position in Class - Senior
////			if($ShowOverallPositionClass && $FormNumber > 3)
//			if($ShowOverallPositionClass)
//			{
//				$thisTitleEn = $eReportCard['Template']['MSTable']['ClassPositionEn'];
//				$thisTitleCh = $eReportCard['Template']['MSTable']['ClassPositionCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassPos, $ClassPosition, $first, $columnClassNumOfStudent, $ClassNumOfStudent);
//				
//				$first = 0;
//			}
//			
////			# Number of Students in Class 
////			if($ShowNumOfStudentClass)
////			{
////				$thisTitleEn = $eReportCard['Template']['MSTable']['ClassNumOfStudentEn'];
////				$thisTitleCh = $eReportCard['Template']['MSTable']['ClassNumOfStudentCh'];
////				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnClassNumOfStudent, $ClassNumOfStudent, $first);
////				
////				$first = 0;
////			}
//			
//			# Position in Form - Junior
////			if($ShowOverallPositionForm && $FormNumber && $FormNumber < 4)
//			if($ShowOverallPositionForm)
//			{
//				$thisTitleEn = $eReportCard['Template']['MSTable']['FormPositionEn'];
//				$thisTitleCh = $eReportCard['Template']['MSTable']['FormPositionCh'];
//				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormPos, $FormPosition, $first, $columnFormNumOfStudent, $FormNumOfStudent);
//				
//				$first = 0;
//			}
//			
//			# Number of Students in Form 
////			if($ShowNumOfStudentForm)
////			{
////				$thisTitleEn = $eReportCard['Template']['MSTable']['FormNumOfStudentEn'];
////				$thisTitleCh = $eReportCard['Template']['MSTable']['FormNumOfStudentCh'];
////				$x .= $this->Generate_Footer_Info_Row($ReportID, $StudentID, $ColNum2, $NumOfAssessment, $thisTitleEn, $thisTitleCh, $columnFormNumOfStudent, $FormNumOfStudent, $first);
////				
////				$first = 0;
////			}
//			
//			##################################################################################
//			# CSV related
//			##################################################################################
//			
//			# build data array
//			$ary = array();
//			
//			# Get Other Info from CSV
//			if ($StudentID != '')
//			{
//				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID,$StudentID);
//				$ary = $OtherInfoDataAry[$StudentID];
//			}
//			
//			if($SemID=="F")
//			{
//				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
//				
//				# calculate sems/assesment column
//				$ColNum2Ary = array();
//				foreach($ColumnData as $k1=>$d1)
//				{
//					$TermID = $d1['SemesterNum'];
////					if($d1['IsDetails']==1)
////					{
////						# check sems/assesment col
////						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$TermID ." and ClassLevelID=".$ClassLevelID);
////						$thisReportID = $thisReport['ReportID'];
////						
////						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
////						$ColNum2Ary[$TermID] = sizeof($ColumnTitleAry)-1;
////					}
////					else
////					{
//						$ColNum2Ary[$TermID] = 0;
////					}
//				}
//			}
//
//			$border_top = $first ? "border_top" : "";
//			
//			# Days Absent 
//			$thisTitleEn = $eReportCard['Template']['MSTable']['DaysAbsentEn'];
//			$thisTitleCh = $eReportCard['Template']['MSTable']['DaysAbsentCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Days Absent", $ary);
//			
//			# Times Late
//			$thisTitleEn = $eReportCard['Template']['MSTable']['TimesLateEn'];
//			$thisTitleCh = $eReportCard['Template']['MSTable']['TimesLateCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Times Late", $ary);
//			
//			# Conduct - Semester Report
//			if($isMainReport)
//			{
//				$thisTitleEn = $eReportCard['Template']['MSTable']['ConductEn'];
//				$thisTitleCh = $eReportCard['Template']['MSTable']['ConductCh'];
//				$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Conduct", $ary);
//			}
//			
//			# Demerits
//			$thisTitleEn = $eReportCard['Template']['MSTable']['DemeritEn'];
//			$thisTitleCh = $eReportCard['Template']['MSTable']['DemeritCh'];
//			$x .= $this->Generate_CSV_Info_Row($ReportID, $StudentID, $ColNum2Ary, $ColNum2, $thisTitleEn, $thisTitleCh, "Demerits", $ary);
//		}
		
		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='', $PrintTemplateType=''){
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
 		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
		$term_num 					= $this->Get_Semester_Seq_Number($SemID);
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
		
		$x = "";
		$y = "";
		
		// Term Test Report
		if($isExtraReport){
			# Full Mark and Pass Mark
			$x .= "<table class='FullMarkInfo' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr>";
				$x .= "<td width='2%'>* </td>";
				$x .= "<td width='10%'>".$eReportCard['Template']['FullMarksCh'].":</td>";
				$x .= "<td width='8%'>100</td>";
				$x .= "<td width='10%'>".$eReportCard['Template']['PassMarksCh'].":</td>";
				$x .= "<td width='5%'>50</td>";
				$x .= "<td>&nbsp;</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td width='2%'>&nbsp;</td>";
				$x .= "<td width='10%'>".$eReportCard['Template']['FullMarksEn'].":</td>";
				$x .= "<td width='8%'>100</td>";
				$x .= "<td width='10%'>".$eReportCard['Template']['PassMarksEn'].":</td>";
				$x .= "<td width='5%'>50</td>";
				$x .= "<td>&nbsp;</td>";
			$x .= "</tr>";
			$x .= "</table>";
			
			return $x;
		}
		// Semester / Consolidate Report
		else{
			include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
			$libaward = new libreportcard_award();
			
			# Get Other Info from CSV
			$OtherInfoDataAry = array();
			if ($StudentID != '') 
			{
				$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID); 
				$ary_content = $OtherInfoDataAry[$StudentID];
			}
			
			# Get Term Reports
			if ($ReportType == 'W') {
				$TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
				$LastTermReportID = $TermReportInfoArr[count($TermReportInfoArr)-1]["ReportID"];
				$LastTermSemester = $TermReportInfoArr[count($TermReportInfoArr)-1]["Semester"];
			}
			$SemIDData = $ReportType=='W'? $LastTermSemester : $SemID;
			
			// Get Student Info
			// (eAttendance & eDiscipline)
			$studentData = $this->Get_Student_Profile_Data($ReportID, $StudentID, true, true, true, "", true);
			$studentAttendanceData = $studentData['AttendanceAry'][$StudentID];
			// (eEnrolment)
			$EnrolData = $this->Get_eEnrolment_Data($StudentID, '', $includeActivity=true, $includeEnrolMerit=false);
			// (iPortfolio)
			$iPoData = $this->returnOLEStudentRecords($StudentID, ($SemID!="F"? $SemID : ""));
			
			// Promotion Status
			if($ReportType=="W" && $FormNumber!=6){
				$PromotionStatusAry = $this->GetPromotionStatusList("", "", $StudentID, $ReportID);
				$PromotionStatus = $PromotionStatusAry[$StudentID];
				$promoted = $PromotionStatus["Promotion"]==1 || $PromotionStatus["FinalStatus"]==$eReportCard["PromotionStatus"][1];
				$repeat = $PromotionStatus["Promotion"]==3 && $PromotionStatus["FinalStatus"]==$eReportCard["PromotionStatus"][3];
				$conditional = !$promoted && !$repeat;
				
				$pro_pre = !$promoted? "<s>" : "";
				$pro_post = !$promoted? "</s>" : "";
				$cond_pre = !$conditional? "<s>" : "";
				$cond_post = !$conditional? "</s>" : "";
				$repeat_pre = !$repeat? "<s>" : "";
				$repeat_post = !$repeat? "</s>" : "";
			}
			
			// Get Received Award
			if ($ReportType == 'W') {
				//$awardData = $libaward->Get_Award_Student_Record(array($LastTermReportID, $ReportID), $StudentID, 0);
				//$lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=0, $AwardNameWithSubject=1, $AwardID, $SubjectID);
				$awardData = $libaward->Get_Award_Generated_Student_Record($ReportID, $StudentID, 0, 1);
				$lastawardData = $libaward->Get_Award_Generated_Student_Record($LastTermReportID, $StudentID, 0, 1);
				$awardData = array_merge($lastawardData, $awardData);
			}
			else{
				$awardData = $libaward->Get_Award_Generated_Student_Record($ReportID, $StudentID, 0, 1);
			}
			
			// Get Personal Characteristics
			$pcData = $this->getPersonalCharacteristicsProcessedData($StudentID, ($ReportType=='W'?$LastTermReportID:$ReportID), 0);
			
			// Get Teacher Comment
			$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
			
			# English as a Study Language
			if($FormNumber<=3){
				$x .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr>";
					$x .= "<td colspan='2'>".$eReportCard['Template']['ESLCh']." ".$eReportCard['Template']['ESLEn']."</td>";
				$x .= "</tr>";
				$x .= "<tr>";
					$x .= "<td class='border_top border_left border_right border_bottom' width='75%' align='center'>";
						$x .= $eReportCard['Template']['DistinctCh']." ".$eReportCard['Template']['DistinctEn']." / ";
						$x .= $eReportCard['Template']['AverageCh']." ".$eReportCard['Template']['AverageEn']." / ";
						$x .= $eReportCard['Template']['BelowAvgCh']." ".$eReportCard['Template']['BelowAvgEn'];
					$x .= "</td>";
					$x .= "<td class='border_right border_top border_bottom' width='25%' align='center'>".$eReportCard['Template']['TermAvgCh']."<br>".($ary_content[$SemIDData]["Result"]?$ary_content[$SemIDData]["Result"]:$this->EmptySymbol)."</td>";
				$x .= "</tr>";
				$x .= "</table>";
			}
			
			# Attendance Records & Merits and Demerits
			$x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' style='margin-top: 6px;'>";
			$x .= "<tr><td width='38%' style='font-size:11pt'>".$eReportCard['Template']['AttendanceCh']." ".$eReportCard['Template']['AttendanceEn']."</td>";
			$x .= "<td width='2%'>&nbsp;</td>";
			$x .= "<td width='60%' style='font-size:11pt'>".$eReportCard['Template']['MeritsDemeritsCh']." ".$eReportCard['Template']['MeritsDemeritsEn']."</td></tr>";
			$x .= "<tr><td valign='bottom'>";
					$x .= "<table class='MiscInfo2' width='100%' border='0' cellpadding='0' cellspacing='0'>";
					$x .= "<tr>";
						$x .= "<td width='25%'>&nbsp;</td>";
						$x .= "<td class='border_left border_top' colspan='4' align='center'>".$eReportCard['Template']['AbsentCh']." ".$eReportCard['Template']['AbsentEn']."</td>";
						$x .= "<td class='border_left border_right border_top' width='25%' align='center'>".$eReportCard['Template']['LateCh']." ".$eReportCard['Template']['LateEn']."</td>";
					$x .= "</tr>";
					$x .= "<tr>";
						$x .= "<td class='border_left border_top border_bottom' align='center'>".$eReportCard['Template']['TermTotalCh']."<br>".$eReportCard['Template']['TermTotalEn']."</td>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='12.5%'>".$eReportCard['Template']['DaysCh']."<br>".$eReportCard['Template']['DaysEn']."</td>";
						$x .= "<td class='border_top border_bottom' align='center' width='12.5%'>".($studentAttendanceData['Days Absent']>0? $studentAttendanceData['Days Absent'] : 0)."</td>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='12.5%'>".$eReportCard['Template']['PeriodCh']."<br>".$eReportCard['Template']['PeriodEn']."</td>";
						$x .= "<td class='border_top border_bottom' align='center' width='12.5%'>".($ary_content[$SemIDData]["Absent (Period)"]>0? $ary_content[$SemIDData]["Absent (Period)"] : 0)."</td>";
						$x .= "<td class='border_top border_left border_right border_bottom' align='center'>".($studentAttendanceData['Time Late']>0? $studentAttendanceData['Time Late'] : 0)."</td>";
					$x .= "</tr>";
//					$x .= "<tr>";
//						$x .= "<td class='border_left border_bottom' align='center'>".$eReportCard['Template']['TermTotalEn']."</td>";
//						$x .= "<td class='border_left border_bottom' align='center' width='12.5%'>".$eReportCard['Template']['DaysEn']."</td>";
//						$x .= "<td class='border_left border_bottom' align='center' width='12.5%'>".$eReportCard['Template']['PeriodEn']."</td>";
//					$x .= "</tr>";
					$x .= "</table>";
			$x .= "</td>";
			$x .= "<td>&nbsp;</td>";
			$x .= "<td valign='bottom'>";
					$x .= "<table class='MiscInfo2' width='100%' border='0' cellpadding='0' cellspacing='0'>";
					$x .= "<tr>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='16%'>".$eReportCard['Template']['MeritsCh']."<br>".$eReportCard['Template']['MeritsEn']."</td>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='16%'>".$eReportCard['Template']['MinorMeritCh']."<br>".$eReportCard['Template']['MinorMeritEn']."</td>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='16%'>".$eReportCard['Template']['MajorMeritCh']."<br>".$eReportCard['Template']['MajorMeritEn']."</td>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='16%'>".$eReportCard['Template']['DemeritsCh']."<br>".$eReportCard['Template']['DemeritsEn']."</td>";
						$x .= "<td class='border_left border_top border_bottom' align='center' width='16%'>".$eReportCard['Template']['MinorDemeritCh']."<br>".$eReportCard['Template']['MinorDemeritEn']."</td>";
						$x .= "<td class='border_top border_left border_right border_bottom' align='center' width='16%'>".$eReportCard['Template']['MajorDemeritCh']."<br>".$eReportCard['Template']['MajorDemeritEn']."</td>";
					$x .= "</tr>";
					$x .= "<tr>";
						$x .= "<td class='border_left border_bottom' align='center'>".(intval($studentData[1])>0? intval($studentData[1]) : 0)."</td>";
						$x .= "<td class='border_left border_bottom' align='center'>".(intval($studentData[2])>0? intval($studentData[2]) : 0)."</td>";
						$x .= "<td class='border_left border_bottom' align='center'>".(intval($studentData[3])>0? intval($studentData[3]) : 0)."</td>";
						$x .= "<td class='border_left border_bottom' align='center'>".(intval($studentData[-1])>0? intval($studentData[-1]) : 0)."</td>";
						$x .= "<td class='border_left border_bottom' align='center'>".(intval($studentData[-2])>0? intval($studentData[-2]) : 0)."</td>";
						$x .= "<td class='border_left border_bottom border_right' align='center'>".(intval($studentData[-3])>0? intval($studentData[-3]) : 0)."</td>";
					$x .= "</tr>";
					$x .= "</table>";
			$x .= "</td></tr>";
			$x .= "</table>";
			
			# Awards in Academic Performance and Conduct
			$x .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr>";
				$x .= "<td colspan='2'>".$eReportCard['Template']['AwardsCh']." ".$eReportCard['Template']['AwardsEn']."</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td class='border_left border_top' width='50%' align='center'>".($awardData[0]["AwardName"]?$awardData[0]["AwardName"]:"&nbsp;")."</td>";
				$x .= "<td class='border_left border_right border_top' width='50%' align='center'>".($awardData[1]["AwardName"]?$awardData[1]["AwardName"]:"&nbsp;")."</td>";
			$x .= "</tr>";
			$awardCount = round(count($awardData)/2)-1;
			$awardCount = $awardCount>3? $awardCount : 3;
			for($i=1; $i<$awardCount; $i++){
				$x .= "<tr>";
					$x .= "<td class='border_left' align='center'>".($awardData[($i*2)]["AwardName"]?$awardData[($i*2)]["AwardName"]:"&nbsp;")."</td>";
					$x .= "<td class='border_left border_right' align='center'>".($awardData[($i*2)+1]["AwardName"]?$awardData[($i*2)+1]["AwardName"]:"&nbsp;")."</td>";
				$x .= "</tr>";
			}
			$x .= "<tr>";
				$x .= "<td class='border_left border_bottom' align='center'>".($awardData[$awardCount*2]["AwardName"]?$awardData[$awardCount*2]["AwardName"]:"&nbsp;")."</td>";
				$x .= "<td class='border_left border_right border_bottom' align='center'>".($awardData[($awardCount*2)+1]["AwardName"]?$awardData[($awardCount*2)+1]["AwardName"]:"&nbsp;")."</td>";
			$x .= "</tr>";
			$x .= "</table>";
			
			# Notes
			$notesContent = $ary_content[$SemIDData]["Remark"];
			$notesCount = round(count($notesContent)/2);
			$notesCount = $notesCount>2? $notesCount : 2;
			$x .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr>";
				$x .= "<td colspan='2'>".$eReportCard['Template']['NotesCh']." ".$eReportCard['Template']['NotesEn']."</td>";
			$x .= "</tr>";
			for($i=0; $i<$notesCount; $i++){
				$border_css = ($i==$notesCount-1)? "border_bottom" : "";
				$border_css .= ($i==0)? "border_top" : "";
				$x .= "<tr>";
					$x .= "<td class='border_left $border_css' width='50%' align='left'>".($notesContent[$i*2]?$notesContent[$i*2]:"&nbsp;")."</td>";
					$x .= "<td class='border_left border_right $border_css' width='50%' align='left'>".($notesContent[($i*2)+1]?$notesContent[($i*2)+1]:"&nbsp;")."</td>";
				$x .= "</tr>";
			}
			$x .= "</table>";
			
//			// Page Break
//			$x .= "</td></tr>";
//			$x .= "</table>";
//			
//			$x .= "</td></tr>";
//			$x .= "</table>";
//			
//			$x .= "</td></tr>";
//			$x .= "</table>";
//			$x .= "</div>";
//			
//			$x .= "<div id='container' style='page-break-after:always;'>";
//			$x .= "<table width='100%' border='0' cellpadding='02' cellspacing='0' valign='top'>";
//			
//			$x .= "<tr><td>";
//			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
//			$x .= "<tr height='899px' valign='top'><td>";
//			
//			$x .= "<table class='$class_name' width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
//			$x .= "<tr><td>";
//
//			# Student Info
//			$x .= $this->getReportStudentInfo($ReportID, $StudentID);
			
			# Activities / Services
			$y .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$y .= "<tr><td width='100%' style='padding:0;'>".$eReportCard['Template']['ActivitynServiceCh']." ".$eReportCard['Template']['ActivitynServiceEn']."</td></tr>";
			$y .= "<tr><td style='padding:0;'>";
				$y .= "<table class='MiscInfo2' width='100%' border='0' cellpadding='0' cellspacing='0' style='border: 1px solid #000000'>";
				$y .= "<tr>";
					$y .= "<td class='border_bottom' align='center' width='53%'>".$eReportCard['Template']['ProgrammeCh']."<br>".$eReportCard['Template']['ProgrammeEn']."</td>";
					$y .= "<td class='border_left border_bottom' align='center' width='17%'>".$eReportCard['Template']['PostCh']."<br>".$eReportCard['Template']['PostEn']."</td>";
					$y .= "<td class='border_left border_bottom' align='center' width='30%'>".$eReportCard['Template']['PerformanceCh']."<br>".$eReportCard['Template']['PerformanceEn']."</td>";
				$y .= "</tr>";
				for($i=0; $i<count($iPoData); $i++){
					$currentiPo = $iPoData[$i];
					$y .= "<tr>";
						$y .= "<td>".$currentiPo["Title"]."</td>";
						$y .= "<td class='border_left' align='center'>".$currentiPo["Role"]."</td>";
						$y .= "<td class='border_left' align='center'>".$currentiPo["Achievement"]."</td>";
					$y .= "</tr>";
				}
				for($i=0; $i<count($EnrolData); $i++){
					$currentAct = $EnrolData[$i];
					$y .= "<tr>";
						$y .= "<td>".$currentAct["ClubTitle"]."</td>";
						$y .= "<td class='border_left' align='center'>".$currentAct["RoleTitle"]."</td>";
						$y .= "<td class='border_left' align='center'>".$currentAct["Performance"]."</td>";
					$y .= "</tr>";
				}
				$emptyCol = $ReportType=="W"? 10 : 13;
				if(count($iPoData)+count($EnrolData) < $emptyCol){
					$emptyCol = $emptyCol - count($iPoData) - count($EnrolData);
					for($i=0; $i<$emptyCol; $i++){
						$y .= "<tr>";
							$y .= "<td>&nbsp;</td>";
							$y .= "<td class='border_left' align='center'>&nbsp;</td>";
							$y .= "<td class='border_left' align='center'>&nbsp;</td>";
						$y .= "</tr>";
					}
				}
				$y .= "</table>";
			$y .= "</td>";
			$y .= "</tr>";
			$y .= "</table>";
			
			# Readers' Academy
			if($FormNumber<=3 && ($term_num==2 || $SemID=="W")){
				$y .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0' style='border-bottom:1px solid #000000'>";
				$y .= "<tr>";
					$y .= "<td class='border_top border_left border_right' colspan='8'>".$eReportCard['Template']['RRACh']." ".$eReportCard['Template']['RRAEn'].": ".$ary_content[$SemIDData]["Performance"]."</td>";
				$y .= "</tr>";
				$y .= "<tr>";
					$y .= "<td class='border_top border_left border_right' colspan='8'>".$eReportCard['Template']['RAsCh']." ".$eReportCard['Template']['RAsEn']." ".$eReportCard['Template']['BookNumCh']." ".$eReportCard['Template']['BookNumEn']."</td>";
				$y .= "</tr>";
				$y .= "<tr>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['ChiCh']."<br>".$eReportCard['Template']['ChiEn']."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['EngCh']."<br>".$eReportCard['Template']['EngEn']."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['MathCh']."<br>".$eReportCard['Template']['MathEn']."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['SciCh']."<br>".$eReportCard['Template']['SciEn']."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['STCh']."<br>".$eReportCard['Template']['STEn']."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['PSHCh']."<br>".$eReportCard['Template']['PSHEn']."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".$eReportCard['Template']['ArtsCh']."<br>".$eReportCard['Template']['ArtsEn']."</td>";
					$y .= "<td class='border_top border_left border_right' width='12%' align='center'>".$eReportCard['Template']['SportCh']."<br>".$eReportCard['Template']['SportEn']."</td>";
				$y .= "</tr>";
				$y .= "<tr>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["Chinese Language"]>0? $ary_content[$SemIDData]["Chinese Language"] : 0)."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["English Language"]>0? $ary_content[$SemIDData]["English Language"] : 0)."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["Mathematics"]>0? $ary_content[$SemIDData]["Mathematics"] : 0)."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["Science"]>0? $ary_content[$SemIDData]["Science"] : 0)."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["Technology"]>0? $ary_content[$SemIDData]["Technology"] : 0)."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["Personal, Social & Humanities"]>0? $ary_content[$SemIDData]["Personal, Social & Humanities"] : 0)."</td>";
					$y .= "<td class='border_top border_left' width='12%' align='center'>".($ary_content[$SemIDData]["Arts"]>0? $ary_content[$SemIDData]["Arts"] : 0)."</td>";
					$y .= "<td class='border_top border_left border_right' width='12%' align='center'>".($ary_content[$SemIDData]["Sports"]>0? $ary_content[$SemIDData]["Sports"] : 0)."</td>";
				$y .= "</tr>";
				$y .= "</table>";
			}

			# Personal Qualities
			$y .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0' style='border-bottom:1px solid #000000'>";
			$y .= "<tr>";
				$y .= "<td colspan='6'>".$eReportCard['Template']['PQCh']." ".$eReportCard['Template']['PQEn']."</td>";
			$y .= "</tr>";
			$y .= "<tr>";
				$y .= "<td class='border_top border_left' align='center' colspan='2'>".$eReportCard['Template']['AutonomyCh']." ".$eReportCard['Template']['AutonomyEn']."</td>";
				$y .= "<td class='border_top border_left' align='center' colspan='2'>".$eReportCard['Template']['CompetenceCh']." ".$eReportCard['Template']['CompetenceEn']."</td>";
				$y .= "<td class='border_top border_left border_right' align='center' colspan='2'>".$eReportCard['Template']['RelatednessCh']." ".$eReportCard['Template']['RelatednessEn']."</td>";
			$y .= "</tr>";
			$y .= "<tr>";
				$y .= "<td class='border_top border_left' width='16%' align='center'>".$eReportCard['Template']['SelfDisCh']."<br>".$eReportCard['Template']['SelfDisEn']."</td>";
				$y .= "<td class='border_top border_left' width='16%' align='center'>".$eReportCard['Template']['LearningAttCh']."<br>".$eReportCard['Template']['LearningAttEn']."</td>";
				$y .= "<td class='border_top border_left' width='16%' align='center'>".$eReportCard['Template']['WillingnessCh']."<br>".$eReportCard['Template']['WillingnessEn']."</td>";
				$y .= "<td class='border_top border_left' width='16%' align='center'>".$eReportCard['Template']['SelfIntiCh']."<br>".$eReportCard['Template']['SelfIntiEn']."</td>";
				$y .= "<td class='border_top border_left' width='16%' align='center'>".$eReportCard['Template']['InterpersonCh']."<br>".$eReportCard['Template']['InterpersonEn']."</td>";
				$y .= "<td class='border_top border_left border_right' width='16%' align='center'>".$eReportCard['Template']['CourtesyCh']."<br>".$eReportCard['Template']['CourtesyEn']."</td>";
			$y .= "</tr>";
			$y .= "<tr>";
				$y .= "<td class='border_top border_left' align='center'>".($pcData[$this->returnPersonalCharTitleByID(1)]? $pcData[$this->returnPersonalCharTitleByID(1)] : $this->EmptySymbol)."</td>";
				$y .= "<td class='border_top border_left' align='center'>".($pcData[$this->returnPersonalCharTitleByID(2)]? $pcData[$this->returnPersonalCharTitleByID(2)] : $this->EmptySymbol)."</td>";
				$y .= "<td class='border_top border_left' align='center'>".($pcData[$this->returnPersonalCharTitleByID(3)]? $pcData[$this->returnPersonalCharTitleByID(3)] : $this->EmptySymbol)."</td>";
				$y .= "<td class='border_top border_left' align='center'>".($pcData[$this->returnPersonalCharTitleByID(4)]? $pcData[$this->returnPersonalCharTitleByID(4)] : $this->EmptySymbol)."</td>";
				$y .= "<td class='border_top border_left' align='center'>".($pcData[$this->returnPersonalCharTitleByID(5)]? $pcData[$this->returnPersonalCharTitleByID(5)] : $this->EmptySymbol)."</td>";
				$y .= "<td class='border_top border_left border_right' align='center'>".($pcData[$this->returnPersonalCharTitleByID(6)]? $pcData[$this->returnPersonalCharTitleByID(6)] : $this->EmptySymbol)."</td>";
			$y .= "</tr>";
			$y .= "</table>";
			
			# Remarks
			$y .= "<table class='MiscInfo' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			$y .= "<tr>";
				$y .= "<td colspan='2'>".$eReportCard['Template']['RemarkCh']." ".$eReportCard['Template']['RemarkEn']."</td>";
			$y .= "</tr>";
			$y .= "<tr>";
			if($ReportType=="W" && $FormNumber!=6){
				$y .= "<td class='border_left border_right border_top border_bottom' width='58%'>".nl2br($CommentAry[0])."</td>";
				$y .= "<td width='2%'>&nbsp;</td>";
				$y .= "<td class='border_left border_right border_top border_bottom' width='40%'>";
					$y .= $pro_pre.$eReportCard['Template']['PromoteCh']." ".$eReportCard['Template']['PromoteEn'].$pro_post."<br />";
					$y .= $cond_pre.$eReportCard['Template']['CondPromoteCh']." ".$eReportCard['Template']['CondPromoteEn'].$cond_post."<br />";
					$y .= $repeat_pre.$eReportCard['Template']['RepeatCh']." ".$eReportCard['Template']['RepeatEn'].$repeat_post."<br />";
				$y .= "</td>";
			}
			else{
				$y .= "<td class='border_left border_right border_top border_bottom' width='100%' style='height:60px;'>".nl2br($CommentAry[0])."</td>";
			}
			$y .= "</tr>";
			$y .= "</table>";
			
			// Return Page 1 and Page 2 in A3
			return array($x, $y);
		}
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $PrintTemplateType='') {
 		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport				= !$isMainReport;
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight					= $ReportSetting['LineHeight'];
		$AllowClassTeacherComment 	= $ReportSetting['AllowClassTeacherComment'];
 		
		$SignatureTable = "";
		
		// Term Test Report
 		if($isExtraReport){
 			$SignatureTable .= "<table class='teacher_comment_td' width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			$SignatureTable .= "<tr>";
				$SignatureTable .= "<td valign='top' align='left' width='54%'>&nbsp;</td>";
				$SignatureTable .= "<td valign='top' align='left' width='20%'>".$eReportCard['Template']['ParentSignCh']."</td>";
				$SignatureTable .= "<td valign='top' align='left'>&nbsp;</td>";
			$SignatureTable .= "</tr>";
			$SignatureTable .= "<tr>";
				$SignatureTable .= "<td valign='top' align='left' width='54%'>&nbsp;</td>";
				$SignatureTable .= "<td valign='top' align='left' width='20%'>".$eReportCard['Template']['ParentSignEn']."</td>";
				$SignatureTable .= "<td valign='top' align='left'>__________________________</td>";
			$SignatureTable .= "</tr>";
			$SignatureTable .= "</table>\n";
			
			return $SignatureTable;
 		}
		
		// Semester / Consoldate Report
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];
		
		$SignatureTable .= "<table class='signature' width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$SignatureTable .= "<tr>";
			$SignatureTable .= "<td class='sign_footer' valign='bottom' align='left' colspan='3'>".$eReportCard['Template']['SignatureTable']['SignatureCh']." ".$eReportCard['Template']['SignatureTable']['SignatureEn']."</td>";
		$SignatureTable .= "</tr>";
		$SignatureTable .= "<tr>";
		
		$border_left_style = "border_left";
		for($k=1; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$TitleEn = $eReportCard["Template"]['SignatureTable'][$SettingID."En"];
			$TitleCh = $eReportCard["Template"]['SignatureTable'][$SettingID."Ch"];
			
			$SignatureTable .= "<td class='border_top $border_left_style border_right border_bottom' valign='bottom' align='center' width='33%'>";
			$SignatureTable .= "<table class='sign_footer' cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' height='56px' valign='bottom'>&nbsp;</td></tr>";
			$SignatureTable .= "<tr><td $td_class align='center' valign='bottom'>$TitleCh $TitleEn</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
			
			$border_left_style = "";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	########### END Template Related ##############
	
	function Generate_CSV_Info_Row($ReportID, $StudentID="", $ColNum2Ary=array(), $ColNum2, $TitleEn, $TitleCh, $InfoKey, $ValueArr)
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Report Display Settings
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		// Test - Get Start Date and End Date to get AP records
		$ReportStartDate = $ReportSetting['TermStartDate'];
		$ReportEndDate = $ReportSetting['TermEndDate'];
		if($isMainReport || ($isExtraReport && ($ReportStartDate=="0000-00-00" || $ReportEndDate=="0000-00-00"))){
			$ReportStartDate = "";
			$ReportEndDate = "";
		}
		
		# initialization
		$border_top = "";
		$x = "";
		
		return $x;
	}
	
	function Generate_Footer_Info_Row($ReportID, $StudentID="", $ColNum2, $NumOfAssessment, $TitleEn, $TitleCh, $ValueArr, $OverallValue, $isFirst=0, $OtherArr=array(), $otherOverall="")
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$isMainReport 				= $ReportSetting['isMainReport'];
		$isExtraReport 				= !$isMainReport;
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$FormNumber 				= $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ColumnTitle 				= $this->returnReportColoumnTitle($ReportID);
		$CalOrder 					= $this->Get_Calculation_Setting($ReportID);
		$ShowRightestColumn 		= $this->Is_Show_Rightest_Column($ReportID);
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
//		if($isMainReport && $FormNumber!=6 && $ReportType!="W")
//		{
//			# Retrieve Extra Report (Test) Display Settings
//			$extraReportInfo = $this->returnReportTemplateBasicInfo("", "", $ClassLevelID, $SemID, 0);
//			$extraReportID = $extraReportInfo["ReportID"];
//			
//			$extraColoumnTitle = $this->returnReportColoumnTitle($extraReportID);
//			if(sizeof($extraColoumnTitle) > 0){
//				foreach ($extraColoumnTitle as $extraColumnID[] => $extraColumnTitle[]){
//					break;
//				}
//			}
//			
//			$extraColumn = $extraColumnID[0];
//		}
//		
//		if($ReportType == "W" && $FormNumber == 6)
//		{
//			$termReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
//			$ColumnTitle = $this->returnReportColoumnTitle($termReport[0]["ReportID"]);
//		}

		list($currentAverage, $currentOrder, $FormTotalNumber) = $ValueArr;
		
		$border_top = $isFirst ? "border_top" : "";
		
		$x .= "<tr>";
		$x .= "<td class='tabletext {$border_top}' align='center' height='{$LineHeight}' colspan='3'>&nbsp;</td>";
		$x .= "<td class='tabletext {$border_top} border_left border_bottom' align='center' height='{$LineHeight}' colspan='2'>".$TitleCh." ".$TitleEn."</td>";
		$x .= "<td class='tabletext {$border_top} border_left border_bottom' align='center' height='{$LineHeight}'>".$currentAverage."</td>";
		
		if($ReportType!="W" || $FormNumber==6){
			$x .= "<td class='tabletext {$border_top} border_left border_bottom border_right' align='center' height='{$LineHeight}'>".$this->Get_Ranking_Display($currentOrder, $FormTotalNumber, 1, 1)."</td>";
		}
		else{
			$x .= "<td class='tabletext {$border_top} border_left border_bottom' align='center' height='{$LineHeight}'>".$this->Get_Ranking_Display($currentOrder, $FormTotalNumber, 1, 1)."</td>";
			$x .= "<td class='tabletext {$border_top} border_left border_bottom border_right' align='center' height='{$LineHeight}'>".$ValueArr[3]."</td>";
		}
		
		$curColumn = 0;
//		foreach ($ColumnTitle as $ColumnID => $ColumnName) 
//		{
//			$current_count = $NumOfAssessment[$curColumn] - 1;
//			
//			if($curColumn==0) 
//			{
//				$current_count = $NumOfAssessment[$curColumn]? $NumOfAssessment[$curColumn] : 1;
//			}
//
//			for ($i=0; $i<$current_count; $i++)
//			{
//				$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}'>".$this->EmptySymbol."</td>";
//			}
//
//			$thisValue = $ValueArr[$ColumnID];
//			if($TitleEn == $eReportCard['Template']['MSTable']['ClassPositionEn'] && count($OtherArr)>0)
//			{
//				$thisInvalid = !($thisValue > 0 && $OtherArr[$ColumnID] > 0) || ($OverallPositionRangeClass > 0 && $thisValue > $OverallPositionRangeClass);
//				$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $OtherArr[$ColumnID], 1, $ShowNumOfStudentClass);
//			}
//			else if($TitleEn == $eReportCard['Template']['MSTable']['FormPositionEn'] && count($OtherArr)>0)
//			{
//				$thisInvalid = !($thisValue > 0 && $OtherArr[$ColumnID] > 0) || ($OverallPositionRangeForm > 0 && $thisValue > $OverallPositionRangeForm);
//				$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $OtherArr[$ColumnID], 1, $ShowNumOfStudentForm);
//			}
//			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>".$thisValue."</td>";
//			
//			$curColumn++;
//			
//			// only display first column if is term test report
//			if($ReportType=="T" && $isExtraReport) break;
//		}
//		
//		if ($ShowRightestColumn)
//		{
//			$thisValue = $OverallValue;
//			if($TitleEn == $eReportCard['Template']['MSTable']['ClassPositionEn'] && $otherOverall)
//			{
//				$thisInvalid = !($thisValue > 0 && $otherOverall > 0) || ($OverallPositionRangeClass > 0 && $thisValue > $OverallPositionRangeClass);
//				$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $otherOverall, 1, $ShowNumOfStudentClass);
//			}
//			else if($TitleEn == $eReportCard['Template']['MSTable']['FormPositionEn'] && $otherOverall)
//			{
//				$thisInvalid = !($thisValue > 0 && $otherOverall > 0) || ($OverallPositionRangeForm > 0 && $thisValue > $OverallPositionRangeForm);
//				$thisValue = $thisInvalid? $this->EmptySymbol : $this->Get_Ranking_Display($thisValue, $otherOverall, 1, $ShowNumOfStudentForm);
//			}
//			$x .= "<td class='tabletext {$border_top} border_left' align='center' height='{$LineHeight}' colspan='2'>". $thisValue ."&nbsp;</td>";
//		}
		
		$x .= "</tr>";
		
		return $x;
	}
	
	function Generate_Info_Title_td($TitleEn, $TitleCh, $hasBorderTop=0)
	{
		$x = "";
		$border_top = ($hasBorderTop)? "border_top" : "";
				
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>$Prefix</td><td height='{$LineHeight}'class='tabletext'>". $TitleCh ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		$x .= "<td class='tabletext {$border_top}' height='{$LineHeight}'>";
			$x .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$x .= "<tr><td>$Prefix</td><td height='{$LineHeight}'class='tabletext'>". $TitleEn ."</td></tr>";
			$x .= "</table>";
		$x .= "</td>";
		
		return $x;
	}
	
	function Is_Show_Rightest_Column($ReportID)
	{
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport 				= $ReportSetting['isMainReport'];
		$SemID 						= $ReportSetting['Semester'];
		$term_num 					= $SemID == "F" ? "W" : $this->Get_Semester_Seq_Number($SemID);
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
//		$ShowRightestColumn = $isMainReport && $term_num!=1 && $ShowSubjectOverall;
		$ShowRightestColumn = $isMainReport && $term_num=="W" && $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		# Display Demerit Records
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	// $border, e.g. "left,right,top"
	function Get_Double_Line_Td($border="",$width=1)
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;

		$style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		
		return $td;
	}		
	
	function Get_Ranking_Display($Rank, $NumOfStudent, $DisplayRank, $DisplayNumOfStudent)
	{
		$Display = '';
		$DisplayRank = ($Rank != "-1" && $Rank != $this->EmptySymbol && $DisplayRank == true);
		
		if ($DisplayRank==true && $DisplayNumOfStudent==true)
		{
			$Display = $Rank.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==true && $DisplayNumOfStudent==false)
		{
			$Display = $Rank;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==true)
		{
			$Display = $this->EmptySymbol.' / '.$NumOfStudent;
		}
		else if ($DisplayRank==false && $DisplayNumOfStudent==false)
		{
			$Display = $this->EmptySymbol;
		}
		
		return $Display;
	}
	
	function Get_Subject_Col_Num($ReportID)
	{
		
	}
	
	// Customized Logic for Award Generation
	public function Generate_Report_Award_Customized($ReportID) {
		global $PATH_WRT_ROOT, $eRCTemplateSetting, $lreportcard_award;
		
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		$scm = new subject_class_mapping();
		$fcm = new form_class_manage();
		
		### Get Report Info
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semester = $ReportInfoArr['Semester'];
		$SemesterSequence = $this->Get_Semester_Seq_Number($Semester);
		$ReportType = $this->Get_Award_Generate_Report_Type($ReportID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, true);
		
		### Get Report Column Info
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID, $other="");
		$numOfReportColumn = count($ReportColumnInfoArr);
				
		### Get Form Students
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0);
		$StudentIDArr = Get_Array_By_Key($StudentInfoArr,"UserID");
		$numOfStudent = count($StudentInfoArr);
		
		### Get Class Info
		$ClassInfo = $fcm->Get_Class_List($this->GET_ACTIVE_YEAR_ID() ,$ClassLevelID);
		$ClassInfo = BuildMultiKeyAssoc((array)$ClassInfo, array("YearClassID"));
		
		### Get Subjects
		$MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=true, $ReportID);
		$SubjectIDArr = array_keys($MainSubjectArr);
		$SubjectIDArr[] = 0;
		$numOfSubject = count($SubjectIDArr);
		
		## Get Subject Type
		$FormSubjectType = $this->Get_Form_Main_Subject($ClassLevelID, 1);
		$FormSubjectType = BuildMultiKeyAssoc((array)$FormSubjectType, array('SubjectID'));
		
		### Get Subject Group Info
		$SubjectGroupContent = $scm->Get_Subject_Group_Stat($Semester, array($ClassLevelID));
		$SubjectGroupType = $this->Report_SubjectGroup_Checking_for_Award_Generation($SubjectGroupContent[0], $MainSubjectArr, $ClassInfo, $ClassLevelID);
		
		### Get Subject ID Code mapping
		$SubjectIdCodeMappingAry = $this->GET_SUBJECTS_CODEID_MAP();
		
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		
		### Get Subject Subject Marks
		$MarksArr = $this->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0);
		
		### Get Report Result Marks
		$TotalMarksArr = $this->getReportResultScore($ReportID, $ReportColumnID=0, $StudentID="", $other_conds='', $debug=0, $includeAdjustedMarks=1, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
		
		### Get Report Result Status
		$PassSubjectCount = $this->RETURN_FAIL_SUBJECT_COUNT($ReportID, $ClassLevelID, $StudentIDArr);

		### Get Subject Personal Characteristics
		$SubjectPersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID, 0, $GroupByCharID=0);
		$SubjectPersonalCharAssoArr = BuildMultiKeyAssoc($SubjectPersonalCharInfoArr, array('SubjectID', 'CharID'));
		$SubjectPersonalCharAssoArr = $SubjectPersonalCharAssoArr[0];
		
		### Get Related Award Config
		//$PersonalCharConfigArr = $this->Get_Award_Generate_Personal_Char_Grade_Config();
		
		### Get exclude ordering student
		$excludeRankingStudentIdAry = $this->GET_EXCLUDE_ORDER_STUDENTS($ReportID);
		
		### Check Award Criteria
		$AwardStudentInfoArr = array();
		// Term Report
		if ($ReportType == 'T') {
			$TermReportMapping = array();
			$TermReportMapping[$Semester] = $ReportID;
			
			### Get Student Personal Characteristics
			$StudentPersonalCharInfoArr = $this->returnPCRecordExcellentStatus($TermReportMapping, $StudentIDArr);
			
			// loop Students
			for ($i=0; $i<$numOfStudent; $i++) {
				$thisStudentID = $StudentInfoArr[$i]['UserID'];
				$thisClassID = $StudentInfoArr[$i]['YearClassID'];
				
				$thisSubject_ExcellentinClass = array();
				$thisSubject_BestInClass = array();
				$thisSubject_ExcellentInForm = array();
				$thisSubject_BestInForm = array();
				//$thisSubject_ExcellentInSG = array();
				//$thisSubject_BestInSG = array();
				$thisSubject_FrontInSubject = array();
				$thisGradeSubject_Result = array();
				
				$ExcellentSbjAwardID = $this->Get_AwardID_By_AwardCode("excellence_in_subject");
				$BestSbjID = $this->Get_AwardID_By_AwardCode("best_in_subject");
				foreach ((array)$MainSubjectArr as $thisSubjectID => $thisSubjectInfoArr)
				{
					// Student Subject Result
					$thisSubjectResult = $MarksArr[$thisStudentID][$thisSubjectID][0];
					$thisSubjectClassOrder = $thisSubjectResult["OrderMeritClassBySD"];
					$thisSubjectFormOrder = $thisSubjectResult["OrderMeritFormBySD"];
					//$thisSubjectGroupOrder = $thisSubjectResult["OrderMeritSubjectGroup"];
					
					// Subject Order
					$thisSubject_ExcellentInClass[$thisSubjectID] = is_numeric($thisSubjectClassOrder) && $thisSubjectClassOrder>0 && $thisSubjectClassOrder<=3;
					$thisSubject_BestInClass[$thisSubjectID] = is_numeric($thisSubjectClassOrder) && $thisSubjectClassOrder==1;
					$thisSubject_ExcellentInForm[$thisSubjectID] = is_numeric($thisSubjectFormOrder) && $thisSubjectFormOrder>0 && $thisSubjectFormOrder<=3;
					$thisSubject_BestInForm[$thisSubjectID] = is_numeric($thisSubjectFormOrder) && $thisSubjectFormOrder==1;
					//$thisSubject_ExcellentInSG[$thisSubjectID] = $thisSubjectGroupOrder>0 && $thisSubjectGroupOrder<=3;
					//$thisSubject_BestInSG[$thisSubjectID] = $thisSubjectGroupOrder==1;
					
					// Subject Order (Front)
					$FrontCalculation = $SubjectGroupType[$thisSubjectID];
					if(isset($FrontCalculation[0]) || isset($FrontCalculation[$thisClassID])){
						if(is_numeric($thisSubjectFormOrder) && $thisSubjectFormOrder>0 && $thisSubjectFormOrder<=5){
							$thisSubject_FrontInSubject[] = $thisSubjectID;
						}
					}
					else{
						if(is_numeric($thisSubjectFormOrder) && $thisSubjectFormOrder>0 && $thisSubjectFormOrder<=3){
							$thisSubject_FrontInSubject[] = $thisSubjectID;
						}
					}
					
					// Technical Subject - Grade Checking
					if($FormSubjectType[$thisSubjectID]["DisplayStatus"]==2){
						$thisSubjectGrade = $MarksArr[$thisStudentID][$thisSubjectID][0]["Grade"];
						if($thisSubjectGrade=="A" || $thisSubjectGrade=="B" || $thisSubjectGrade=="C")
							$thisGradeSubject_Result["Good"][] = $thisSubjectID;
						else
							$thisGradeSubject_Result["NotGoodEnough"][] = $thisSubjectID;
					}
					
					// Award 2 - 各科成績優異獎（上、下學期）
					if($thisSubject_ExcellentInClass[$thisSubjectID]){
						$AwardStudentInfoArr[$ExcellentSbjAwardID][$thisSubjectID][$thisStudentID]['RankField'] = "OrderMeritClass";
						$AwardStudentInfoArr[$ExcellentSbjAwardID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisSubjectClassOrder;
					}
					// Award 6 - 學科優秀表現獎（上、下學期）
					if($thisSubject_BestInForm[$thisSubjectID]){
						$AwardStudentInfoArr[$BestSbjID][$thisSubjectID][$thisStudentID]['RankField'] = "OrderMeritForm";
						$AwardStudentInfoArr[$BestSbjID][$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisSubjectFormOrder;
					}
				}
				
				// Student Overall Result
				$thisStudentOverallResult = $TotalMarksArr[$thisStudentID][0];
				$thisOverallClassOrder = $thisStudentOverallResult["OrderMeritClassBySD"];
				$thisOverallFormOrder = $thisStudentOverallResult["OrderMeritFormBySD"];
				
				// Overall Order
				$thisOverall_ExcellentinClass = is_numeric($thisOverallClassOrder) && $thisOverallClassOrder>0 && $thisOverallClassOrder<=3;
				$thisOverall_BestInClass = is_numeric($thisOverallClassOrder) && $thisOverallClassOrder==1;
				$thisOverall_ExcellentInForm = is_numeric($thisOverallFormOrder) && $thisOverallFormOrder>0 && $thisOverallFormOrder<=3;
				$thisOverall_BestInForm = is_numeric($thisOverallFormOrder) && $thisOverallFormOrder==1;
				$thisOverall_FrontInSubject = is_numeric($thisOverallFormOrder) && $thisOverallFormOrder>0 && $thisOverallFormOrder<=5;
				
				// Fail Subject
				$thisSubject_FailCount = $PassSubjectCount[$thisStudentID];
				$thisStudent_NoFailSubject = $thisSubject_FailCount[0]==0 && $thisSubject_FailCount[1]==0;
				
				// Excellence in Personal Characteristics
				$thisStudent_ExcellentInPC = $StudentPersonalCharInfoArr[$SemesterSequence][$thisStudentID]["Excellent"]==1;
				
				// Award 7 - 各科成績優異獎（上、下學期）(用標準分計算)
				$thisAwardID = $this->Get_AwardID_By_AwardCode("outstanding_award_class");
				if($thisOverall_ExcellentinClass && $thisStudent_NoFailSubject && $thisStudent_ExcellentInPC && !(($FormNumber==4 || $FormNumber== 5) && $SemesterSequence==2) && $FormNumber!=6){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['RankField'] = "OrderMeritClass";
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = $thisOverallClassOrder;
				}
				// Award 8 - 中一至中五級（上、下學期）全級成績前列獎(用標準分計算)
				$thisAwardID = $this->Get_AwardID_By_AwardCode("outstanding_award_form");
				if($thisOverall_FrontInSubject && $thisStudent_NoFailSubject && $thisStudent_ExcellentInPC && $FormNumber!=6){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['RankField'] = "OrderMeritForm";
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = $thisOverallFormOrder;
				}
				// Award 10 - 學業嘉許獎（上、下學期）
				$thisAwardID = $this->Get_AwardID_By_AwardCode("academic_award_merit");
				if($thisStudent_NoFailSubject && count($thisSubject_FrontInSubject)==2 && count($thisGradeSubject_Result["NotGoodEnough"])==0 && $thisStudent_ExcellentInPC){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = count($thisSubject_FrontInSubject);
				}
				// Award 11 - 學業良好獎（上、下學期）
				$thisAwardID = $this->Get_AwardID_By_AwardCode("academic_award_good");
				if($thisStudent_NoFailSubject && count($thisSubject_FrontInSubject)==3 && count($thisGradeSubject_Result["NotGoodEnough"])==0 && $thisStudent_ExcellentInPC){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = count($thisSubject_FrontInSubject);
				}
				// Award 12 - 學業優異獎（上、下學期）
				$thisAwardID = $this->Get_AwardID_By_AwardCode("academic_award_excellent");
				if($thisStudent_NoFailSubject && count($thisSubject_FrontInSubject)>=4 && count($thisGradeSubject_Result["NotGoodEnough"])==0 && $thisStudent_ExcellentInPC){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = count($thisSubject_FrontInSubject);
				}
			}
		}
		// Consolidate Report
		else if ($ReportType == 'W') {
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			
			### Get Term Reports
			$TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
			$TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
			$TermReportMapping = BuildMultiKeyAssoc((array)$TermReportInfoArr,array("Semester"));
			$numOfTermReport = count($TermReportInfoArr);
			
			### Get Last Term Report
			$LastTermReportID = $TermReportInfoArr[count($TermReportInfoArr)-1]["ReportID"];
			$LastTermID = $TermReportInfoArr[count($TermReportInfoArr)-1]["Semester"];
			$LateSemesterSequence = $this->Get_Semester_Seq_Number($LastTermID);
			
			### Get Student Personal Characteristics
			$StudentPersonalCharInfoArr = $this->returnPCRecordExcellentStatus($TermReportMapping, $StudentIDArr);
			$StudentPersonalCharPassArr = $this->returnPCRecordFailStatus($TermReportMapping, $StudentIDArr);
			$StudentPersonalCharPassArr = $StudentPersonalCharPassArr[0];
			
			### Get Student Improvement
			$FirstTermReportID = $TermReportInfoArr[0]["ReportID"];
			$FromReportID = $FirstTermReportID;
			$FromReportColumnID = 0;
			$FromMustBePass = 0;
			$ToReportID = $ReportID;
			$ToReportColumnID = 0;
			$thisSubjectID = 0;
			$ImprovementField = 'GrandAverage';
			
			$SatisfiedStudentInfoArr = array();
			
			// Get last year consolidate report
			if($FormNumber==6){
				### Generate Result
				// Get Mark Info
				$FromMarkArr = array();
				$ToMarkArr = $this->getReportResultScore($ToReportID, $ToReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
				
				// Get Mark Nature Info
				$FromMarkNatureArr = array();
				$ToMarkNatureArr = $this->Get_Student_Grand_Mark_Nature_Arr($ToReportID, $ClassLevelID, $ToMarkArr);
				
				// Convert Array Format
				$FromMarkArr = array();
				$ToMarkArr = $this->Add_SubjectID_In_GrandMarkArr($ToMarkArr);
				$FromMarkNatureArr = array();
				$ToMarkNatureArr = $this->Add_SubjectID_In_GrandMarkArr($ToMarkNatureArr);

				$LastYearID = $this->Get_Previous_YearID_By_Active_Year();
				if($LastYearID!=""){
					$lastYearRC = new libreportcard($LastYearID);
					$lastYearRC->Load_Batch_Student_Class_ClassLevel_Info();
					$StudentInfo = $lastYearRC->Student_Class_ClassLevel_Info;
					
					// get ClassLevelID of students
					$LastYearClassLevels = array();
					foreach((array)$StudentInfo as $currentStudentID => $StudentContent){
						if(in_array($currentStudentID, $StudentIDArr)){
							$LastYearClassLevels[] = $StudentContent["ClassLevelID"];
						}
					}
					
					// Build comparing data for related classlevels
					$LastYearReport = array();
					$LastYearClassLevels = array_unique($LastYearClassLevels);
					for($i=0; $i<count($LastYearClassLevels); $i++){
						$currentClassLevel = $LastYearClassLevels[$i];
						$LastYearClassLevelReport = $lastYearRC->returnReportTemplateBasicInfo('', '', $currentClassLevel, 'F', 1);
						$LastYearReport[$currentClassLevel] = $LastYearClassLevelReport[0];
						
						$FirstTermReportID = $LastYearClassLevelReport[0]["ReportID"];
						$FromReportID = $FirstTermReportID;				
				
						### Generate Result
						// Get Mark Info
						$FromMarkArr[$currentClassLevel] = $lastYearRC->getReportResultScore($FromReportID, $FromReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
						
						// Get Mark Nature Info
						$FromMarkNatureArr[$currentClassLevel] = $lastYearRC->Get_Student_Grand_Mark_Nature_Arr($FromReportID, $ClassLevelID, $FromMarkArr[$currentClassLevel]);
						
						// Convert Array Format
						$FromMarkArr[$currentClassLevel] = $lastYearRC->Add_SubjectID_In_GrandMarkArr($FromMarkArr[$currentClassLevel]);
						$FromMarkNatureArr[$currentClassLevel] = $lastYearRC->Add_SubjectID_In_GrandMarkArr($FromMarkNatureArr[$currentClassLevel]);
					}
				}
			}
			else{
				### Generate Result
				// Get Mark Info
				$FromMarkArr = $this->getReportResultScore($FromReportID, $FromReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
				$ToMarkArr = $this->getReportResultScore($ToReportID, $ToReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
				
				// Get Mark Nature Info
				$FromMarkNatureArr = $this->Get_Student_Grand_Mark_Nature_Arr($FromReportID, $ClassLevelID, $FromMarkArr);
				$ToMarkNatureArr = $this->Get_Student_Grand_Mark_Nature_Arr($ToReportID, $ClassLevelID, $ToMarkArr);
				
				// Convert Array Format
				$FromMarkArr = $this->Add_SubjectID_In_GrandMarkArr($FromMarkArr);
				$ToMarkArr = $this->Add_SubjectID_In_GrandMarkArr($ToMarkArr);
				$FromMarkNatureArr = $this->Add_SubjectID_In_GrandMarkArr($FromMarkNatureArr);
				$ToMarkNatureArr = $this->Add_SubjectID_In_GrandMarkArr($ToMarkNatureArr);
			}			
			
			// loop student
			for ($i=0; $i<$numOfStudent; $i++) {
				$thisStudentID = $StudentInfoArr[$i]['UserID'];
				//$thisClassID = $StudentInfoArr[$i]['YearClassID'];
				
				// Student Overall Result
				$thisStudentOverallResult = $TotalMarksArr[$thisStudentID][0];
				$thisOverallClassOrder = $thisStudentOverallResult["OrderMeritClass"];
				$thisOverallFormOrder = $thisStudentOverallResult["OrderMeritForm"];
				
				// Overall Order
				$thisOverall_ExcellentinClass = is_numeric($thisOverallClassOrder) && $thisOverallClassOrder>0 && $thisOverallClassOrder<=3;
				$thisOverall_BestInClass = is_numeric($thisOverallClassOrder) && $thisOverallClassOrder==1;
				$thisOverall_ExcellentInForm = is_numeric($thisOverallFormOrder) && $thisOverallFormOrder>0 && $thisOverallFormOrder<=3;
				$thisOverall_BestInForm = is_numeric($thisOverallFormOrder) && $thisOverallFormOrder==1;
				$thisOverall_FrontInSubject = is_numeric($thisOverallFormOrder) && $thisOverallFormOrder>0 && $thisOverallFormOrder<=5;
			
				// Fail Subject Count
				$thisSubject_FailCount = $PassSubjectCount[$thisStudentID];
				$thisStudent_NoFailSubject = $thisSubject_FailCount[0]==0 && $thisSubject_FailCount[1]==0;
				
				// Excellence in Personal Characteristics
				$thisStudent_ExcellentInPC = $StudentPersonalCharInfoArr[$LateSemesterSequence][$thisStudentID]["Excellent"]==1;
				
				// Excellence in Enrolment
				$excellentPerformanceInEnrol = 0;
				$excellentEnrolDataCount = 0;
				$thisStudentEnrolData = $this->Get_eEnrolment_Data($thisStudentID, '', true, true);
				for($edata=0; $edata<count($thisStudentEnrolData); $edata++){
					$currentEnrolData = $thisStudentEnrolData[$edata];
					$excellentEnrolDataCount += ($currentEnrolData["merit"] * 1) + ($currentEnrolData["minorMerit"] * 3) + ($currentEnrolData["majorMerit"] * 9);
					if((($currentEnrolData["merit"] * 1) + ($currentEnrolData["minorMerit"] * 3) + ($currentEnrolData["majorMerit"] * 9)) > 0){
						$excellentPerformanceInEnrol++;
					}
				}
				
				// Award 7 - 各科成績優異獎（上、下學期）(用標準分計算)
				// 中四至中五級（下學期）
				$thisAwardID = $this->Get_AwardID_By_AwardCode("outstanding_award_class");
				if($thisOverall_BestInClass && $thisStudent_NoFailSubject && $thisStudent_ExcellentInPC && ($FormNumber==4 || $FormNumber== 5)){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['RankField'] = "OrderMeritClass";
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = $thisOverallClassOrder;
				}
				// Award 9 - 全班全年成績前列獎﹝獎學金﹞(用標準分計算)
				$thisAwardID = $this->Get_AwardID_By_AwardCode("scholarship_outstanding_award_class");
				if($thisOverall_BestInClass && $thisStudent_NoFailSubject && $thisStudent_ExcellentInPC && !(($FormNumber==4 || $FormNumber== 5) && $SemesterSequence==2) && $FormNumber!=6){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['RankField'] = "OrderMeritClass";
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = $thisOverallClassOrder;
				}
				// Award 26 - 服務獎
				$thisAwardID = $this->Get_AwardID_By_AwardCode("service_award");
				if($excellentPerformanceInEnrol>=3 && $excellentEnrolDataCount>=7 && $thisStudent_ExcellentInPC){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID]['DetermineValue'] = $excellentEnrolDataCount;
				}
				
				// Get value and nature for comparing
				if($FormNumber==6 && isset($StudentInfo[$thisStudentID])){
					$currentClassLevel = $StudentInfo[$thisStudentID]["ClassLevelID"];
					
					// Check Special Case
					$thisFromGrade = $FromMarkArr[$currentClassLevel][$thisStudentID][$thisSubjectID][$FromReportColumnID]['Grade'];
					$thisToGrade = $ToMarkArr[$thisStudentID][$thisSubjectID][$ToReportColumnID]['Grade'];
					if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisFromGrade) || $lreportcard->Check_If_Grade_Is_SpecialCase($thisToGrade)) {
						continue;
					}
					
					// Get the From and To Value
					$thisFromValue = $FromMarkArr[$currentClassLevel][$thisStudentID][$thisSubjectID][$FromReportColumnID][$ImprovementField];
					$thisToValue = $ToMarkArr[$thisStudentID][$thisSubjectID][$ToReportColumnID][$ImprovementField];
					
					$thisFromMarkNature = $FromMarkNatureArr[$currentClassLevel][$thisStudentID][$thisSubjectID][$FromReportColumnID];
					$thisToMarkNature = $ToMarkNatureArr[$thisStudentID][$thisSubjectID][$ToReportColumnID];
				}
				else {
					// Check Special Case
					$thisFromGrade = $FromMarkArr[$thisStudentID][$thisSubjectID][$FromReportColumnID]['Grade'];
					$thisToGrade = $ToMarkArr[$thisStudentID][$thisSubjectID][$ToReportColumnID]['Grade'];
					if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisFromGrade) || $lreportcard->Check_If_Grade_Is_SpecialCase($thisToGrade)) {
						continue;
					}
					
					// Get the From and To Value
					$thisFromValue = $FromMarkArr[$thisStudentID][$thisSubjectID][$FromReportColumnID][$ImprovementField];
					$thisToValue = $ToMarkArr[$thisStudentID][$thisSubjectID][$ToReportColumnID][$ImprovementField];
					
					$thisFromMarkNature = $FromMarkNatureArr[$thisStudentID][$thisSubjectID][$FromReportColumnID];
					$thisToMarkNature = $ToMarkNatureArr[$thisStudentID][$thisSubjectID][$ToReportColumnID];
				}
				
				// no allow checking if fail
				$thisToPass = true;
				if (!$this->Check_MarkNature_Is_Pass($thisToMarkNature)) {
					$thisToPass = false;
				}				
				
				// record satisfied student
				if (is_numeric($thisFromValue) && is_numeric($thisToValue) && $thisToPass) {
					$thisImprovement = $thisToValue - $thisFromValue; 
					if ($thisImprovement >= 0) {
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisImprovement;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ImprovementField'] = $ImprovementField;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['FromValue'] = $thisFromValue;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['FromNature'] = $thisFromMarkNature;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ToValue'] = $thisToValue;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ToNature'] = $thisToMarkNature;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ImprovementValue'] = $thisImprovement;
					}
				}	
			}
		
			### Sort and determine the improvement ranking
			$SatisfiedStudentInfoArr = $lreportcard_award->Sort_And_Add_AwardRank_In_InfoArr($SatisfiedStudentInfoArr, 'DetermineValue', 'desc');
			
			for ($i=0; $i<$numOfStudent; $i++) {
				$thisStudentID = $StudentInfoArr[$i]['UserID'];
				
				// skip if student not satisfy the requirement
				if(!isset($SatisfiedStudentInfoArr[0][$thisStudentID]))
					continue;
				
				### Get Student Info
				$studentData = $this->Get_Student_Profile_Data($ReportID, $thisStudentID, true, true, true);
				$withDemerit = $studentData[-1]>0 || $studentData[-2]>0 || $studentData[-3]>0 || $studentData[-4]>0;
				
				// Award 4 - 學業成績進步獎
				$thisAwardID = $this->Get_AwardID_By_AwardCode("improvement_in_subject");
				$ImprovementInfoArr = $SatisfiedStudentInfoArr[0][$thisStudentID];
				if($ImprovementInfoArr["AwardRank"]>0 && $ImprovementInfoArr["AwardRank"]<5 && $StudentPersonalCharPassArr[$thisStudentID]!="Y" && !$withDemerit){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID] = $ImprovementInfoArr;
				}
				// Award 5 - 學業成績最進步獎
				$thisAwardID = $this->Get_AwardID_By_AwardCode("best_in_academic_improvement");
				$ImprovementInfoArr = $SatisfiedStudentInfoArr[0][$thisStudentID];
				if($ImprovementInfoArr["AwardRank"]==1 && $StudentPersonalCharPassArr[$thisStudentID]!="Y" && !$withDemerit){
					$AwardStudentInfoArr[$thisAwardID][0][$thisStudentID] = $ImprovementInfoArr;
				}
			}
		}	// End else if ($ReportType == 'W')
		
		### Convert to Update function format and Save to DB
		$SuccessArr['Delete_Old_Report_Award'] = $this->Delete_Award_Generated_Student_Record($ReportID);
		foreach ((array)$AwardStudentInfoArr as $thisAwardID => $thisAwardStudentInfoArr) {
			$thisAwardStudentInfoArr = $this->Sort_And_Add_AwardRank_In_InfoArr($thisAwardStudentInfoArr, 'DetermineValue', 'desc');
			
			$AwardUpdateDBInfoArr = array();
			foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
				foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
					$thisUpdateDBInfoArr = array();
					$thisUpdateDBInfoArr = $thisAwardSubjectStudentInfoArr;
					$thisUpdateDBInfoArr['SubjectID'] = $thisSubjectID;
					$thisUpdateDBInfoArr['StudentID'] = $thisStudentID;
					$AwardUpdateDBInfoArr[] = $thisUpdateDBInfoArr;
					
					unset($thisUpdateDBInfoArr);
				}
			}
			
			if (count((array)$AwardUpdateDBInfoArr) > 0) {
				$SuccessArr['Update_Award_Generated_Student_Record'][$thisAwardID] = $this->Update_Award_Generated_Student_Record($thisAwardID, $ReportID, $AwardUpdateDBInfoArr);
			}
			unset($AwardUpdateDBInfoArr);
		}
		
		### Save the Award Text
		$SuccessArr['Delete_Old_Report_Award_Text'] = $this->Delete_Award_Student_Record($ReportID);
		//$SuccessArr['Generate_Report_Award_Text'] = $this->Generate_Report_Award_Text($ReportID);
		
		### Update Last Generated Date 
		$SuccessArr['Update_LastGeneratedAward_Date'] = $this->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGeneratedAward');
		
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	// Customized Logic for Promotion Status Generation
	function getPromotionStatusCustomized($ReportID, $StudentID, $OtherInfoAry=array(), $LastestTermID=0){
		global $eRCTemplateSetting;
		$PromotionStatus = "";
		
		# Get ClassLevelID
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$currentFromNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		
		# Related Reports
		// Term Reports
		$TermReports = BuildMultiKeyAssoc((array)$this->Get_Related_TermReport_Of_Consolidated_Report($ReportID),array("Semester"));
		foreach((array)$TermReports as $TermID => $ReportInfo){
			$TermReportID = $ReportInfo["ReportID"];
		}
		
		# Personal Characteristics
		$pcData = $this->getPersonalCharacteristicsData($StudentID, $TermReportID, 0);
		$pcData = $pcData[0]["CharData"];
		if(trim($pcData)!=""){
			$count_poor = 0;
			$count_average = 0;
		
			$pcData = explode(",", $pcData);
			for($i=0; $i<count($pcData); $i++){
				$data = explode(":", $pcData[$i]);
				$data = $data[1];
				
				if($data==4){
					$count_average++;
				}
				else if($data==5){
					$count_poor++;
				}
			}
			
			// Poor >= 2  OR  (Poor = 1 AND Average = 4)
			if($count_poor>=2 || ($count_poor==1 && $count_average>=4)){
				$PromotionStatus = "2";
			}
			else{
				$PromotionStatus = "1";
			}
		}
		
		# Average Mark
		$OverallResult = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1, 0, 1, '');
		$OverallAverage = $OverallResult["GrandAverage"];
		if($OverallAverage!="" && $PromotionStatus!="2"){
			// Average Mark < 50
			if($OverallAverage < 50){
				$PromotionStatus = "2";
			}
			else{
				$PromotionStatus = "1";
			}
		}

		# Subject Marks
		$Marks = $this->getMarks($ReportID, $StudentID, '', 1, 1, '', '', 0, 0, '', true);
		$totalSubject = count($Marks);
		if($totalSubject>0 && $PromotionStatus!="2"){
			// initiate
			$MainSubjectCount = 0;
			$NormalSubjectCount = 0;
			//$TotalSubjectCount = 0;
			//$passSubject = 0;
			$failMainSubject = 0;
			$failNormalSubject = 0;
			$failSubjectTotal = 0;
			$specialScoreArr = array();
			$withSpecialSubject = false;
			
			// Get Main Subject
			$MainSubjectArr = $this->Get_Form_Main_Subject($ClassLevelID, 0);
			$MainSubjectArr = array_keys($MainSubjectArr);
			$MainSubjectCount = count($MainSubjectArr);
		
			// Get Type of All Subjects
			$SubjectClassInfoArr = $this->Get_Form_Main_Subject($ClassLevelID, 1);
			$SubjectClassInfoArr = BuildMultiKeyAssoc((array)$SubjectClassInfoArr, array("SubjectID"));
			$SubjectCodeMapping = $currentFromNumber && $currentFromNumber<4? $this->GET_SUBJECTS_CODEID_MAP(0, 0) : "";
			
			// loop Marks
			foreach($Marks as $curSubjectID => $SubjectMark){
					
				// Get Subject Code
				$thisSubjectCode = $SubjectCodeMapping[$curSubjectID];
				
				// Get Subject Type
				$SubjectType = $SubjectClassInfoArr[$curSubjectID]["DisplayStatus"];
				
				$isSpecialSubject = $currentFromNumber>0 && $currentFromNumber<4 && in_array($thisSubjectCode, $this->SpecialSubjectList);
				
				if(!in_array($curSubjectID, $MainSubjectArr) && (!$withSpecialSubject || !$isSpecialSubject)){
					//$TotalSubjectCount++;
					if($SubjectType==1)
						$NormalSubjectCount++;
						
					if(!$withSpecialSubject)
						$withSpecialSubject = $isSpecialSubject;
				}
				
				if(empty($SubjectMark)){
					continue;
				}
				
				// Get Score Display Settings
				$subjectSetting = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $curSubjectID, 1, 0, $ReportID);
				$ScaleDisplay = $subjectSetting['ScaleDisplay'];
				$SubjectMark = $SubjectMark[0];
				
				// Skip if special case (except "+")
				if($this->Check_If_Grade_Is_SpecialCase($SubjectMark['Grade']) && $SubjectMark['Grade']!="+")
					continue;
				
				// Get Mark Nature
				$SubjectMark = $ScaleDisplay=="M"? $SubjectMark["Mark"] : $SubjectMark["Grade"];  
				$SubjectMarkNature = $this->returnMarkNature($ClassLevelID, $curSubjectID, $SubjectMark, 0, '', $ReportID);
				// for "+" - absent (zero mark)
				if($SubjectMark['Grade']=="+"){
					$SubjectMarkNature = "Fail";
					$SubjectMark = 0;
				}
				
				// Handling for F1 - F3 - Phy, Chem, Bio
				if($currentFromNumber>0 && $currentFromNumber<4 && in_array($thisSubjectCode, $this->SpecialSubjectList)){
					$specialScoreArr[] = $SubjectMark;
				}
				// Other Subject
				else{
					// Counting for Subject
//					if(!in_array($curSubjectID, $MainSubjectArr)){
//						//$TotalSubjectCount++;
//						if($SubjectType==1)
//							$NormalSubjectCount++;
//					}
					
					// Fail Count
					if($SubjectMarkNature=="Fail"){
						// Main Subject
						if(in_array($curSubjectID, $MainSubjectArr)){
							$failMainSubject++;
						}
						// Other Subject
						else{
							$failSubjectTotal++;
							if($SubjectType==1)
								$failNormalSubject++;
						}
					}
					// Pass
//					else{
//						//$passSubject++;
//					}
				}
			}
				
			// Handling for F1 - F3 - Phy, Chem, Bio
			// check if combined subject fail
			if(count($specialScoreArr) > 0){
				$isSubjectFail = (array_sum((array)$specialScoreArr) / count($specialScoreArr)) < 50;
				if($isSubjectFail){
					$failNormalSubject++;
					$failSubjectTotal++;
				}
				//$NormalSubjectCount++;
				//$TotalSubjectCount++;
			}
			
			// F1 - F3
			if($currentFromNumber<4){
				$failTotal = $failMainSubject + $failSubjectTotal;
				
				// calculate fail limit
				$failNormalLimit = intval(round($NormalSubjectCount/2));
				$failTotalLimit = intval(round($totalSubject/2));
				
				if($failMainSubject==$MainSubjectCount || $failNormalSubject>=$failNormalLimit || $failTotal>=$failTotalLimit){
					$PromotionStatus = "2";
				}
				else{
					$PromotionStatus = "1";
				}
			}
			// F4 - F5
			else{
				if($failMainSubject==($MainSubjectCount-1) || $failSubjectTotal>=2){
					$PromotionStatus = "2";
				}
				else{
					$PromotionStatus = "1";
				}
			}
		}
		return $PromotionStatus;
	}
	
	// Promotion Meeting Report
	function Get_Pooi_To_Promotion_Meetting_Record($type, $ClassLevelID){
		global $PATH_WRT_ROOT, $intranet_session_language, $Lang, $eReportCard;
		global $linterface;
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		
		# Report Settings
		//$maxRow = 48.5;
		//$HeaderRow = 4;
		
		# Year
		$SchoolYearName = $this->GET_ACTIVE_YEAR_NAME();
		$SchoolYearID = $this->schoolYearID;
		
		# Form
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		$FormName = $this->RETURN_CHINESE_NUM($FormNumber);
		
		# Related Reports
		// Consolidate Report
     	$FianlReportInfo = $this->returnReportTemplateBasicInfo(''," ClassLevelID='$ClassLevelID' AND Semester='F' ");
		$FinalReportID = $FianlReportInfo["ReportID"];
		// Term Report
		$TermReports = BuildMultiKeyAssoc((array)$this->Get_Related_TermReport_Of_Consolidated_Report($FinalReportID),array("Semester"));

		# Get Promotion Status
		$StudentPromotionList = $this->GetPromotionStatusList($ClassLevelID, "", "", $FinalReportID);
		
		# Get Report Result
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql = "
			Select
				result.StudentID,
				result.GrandAverage,
				/*IF((result.OrderMeritForm IS NULL OR result.OrderMeritForm = -1), 999, result.OrderMeritForm) as OrderMeritForm,*/
				IF((result.OrderMeritFormBySD IS NULL OR result.OrderMeritFormBySD = -1), 999, result.OrderMeritFormBySD) as OrderMeritForm,
				IF(result.Promotion=2, 'Y', 'N') as PromotionStatus,
				iu.ChineseName AS StudentName,
				yc.ClassTitleB5 AS ClassName,
				ycu.ClassNumber,
				iu.WebSAMSRegNo
			From
				$table AS result
				Inner Join YEAR_CLASS_USER as ycu On result.StudentID = ycu.UserID
				Inner Join YEAR_CLASS as yc On ycu.YearClassID = yc.YearClassID
				Inner Join INTRANET_USER AS iu On ycu.UserID = iu.UserID
			Where
				result.ReportID = '$FinalReportID'
				And (result.ReportColumnID = 0 OR result.ReportColumnID IS NULL)
				And yc.AcademicYearID = '$SchoolYearID'
			Order By
				OrderMeritForm desc
		";
		$StudentPromotionArr = $this->returnArray($sql);
		
		# Student List
		$StudentIDArr = Get_Array_By_Key((array)$StudentPromotionArr,"StudentID");
		$student_count = count($StudentIDArr);
		
		# Get Fail Subject Count
		$StudentFailCount = $this->RETURN_FAIL_SUBJECT_COUNT($FinalReportID, $ClassLevelID, $StudentIDArr);
		
		# Get Term Average
		$TermResultArr = array();
		foreach((array)$TermReports as $TermID => $ReportInfo){
			$TermReportID = $ReportInfo["ReportID"];
			$TermResultArr[$this->Get_Semester_Seq_Number($TermID)] = $this->getReportResultScore($TermReportID);
		}
		
		# Get Personal Characteristics Status
		$PCResultArr = $this->returnPCRecordFailStatus($TermReports, $StudentIDArr);
		
		$EmptySymbol = $Lang['General']['EmptySymbol'];
//		$StylePrefix = '<span class="tabletextrequire">';
//		$StyleSuffix = '</span>';
//		if($format == 'csv'){
//			$StylePrefix = '';
//			$StyleSuffix = '';
//		}
		
		$x = '';
		$rows = array();
		if($type != 'csv'){
			$x .= '<table width="100%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
					</tr>
				</table>';
		}
			
		# Report Header and Info
		$reportHeader = '<div><div class="main_container">

					<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
						<tr class="report_sch_header">
							<td width="25%">&nbsp;</td>
							<td width="42%" align="center">香港培道中學 升留班會議資料</td>
							<td width="33%">&nbsp;</td>
						</tr>
						<tr class="report_sub_header">
							<td>'.$SchoolYearName.' 學年中'.$FormName.'級</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>

					<table class="report_table" cellspacing="0" cellpadding="0" border="1" width="100%" align="center" style="border-collapse: collapse;">
						<tr class="report_title_header">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td colspan="2">學年不<br/>合格數<br/>目</td>
							<td>上<br/>學<br/>期</td>
							<td>下<br/>學<br/>期</td>
							<td>學<br/>年</td>
							<td colspan="2">個人素質<br/>(二欠佳或<br/>四個尚可+<br/>欠佳)</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr class="report_title_header">
							<td width="7%">排名<br/>(標準分)</td>
							<td width="7%">註冊編<br/>號</td>
							<td width="6%">班<br/>別</td>
							<td width="6%">學<br/>號</td>
							<td width="13%">中文<br/>姓名</td>
							<td width="5%">主<br/>科</td>
							<td width="5%">學<br/>科</td>
							<td width="7%">平<br/>均<br/>分</td>
							<td width="7%">平<br/>均<br/>分</td>
							<td width="7%">平<br/>均<br/>分</td>
							<td width="5%">上<br/>學<br/>期</td>
							<td width="5%">下<br/>學<br/>期</td>
							<td width="6%">曾留<br/>級別</td>
							<td width="6%">試升</td>
							<td width="8%">備註</td>
						</tr>';
		$x .= $reportHeader;
		$rows[] = array($SchoolYearName.' 學年中'.$FormName.'級');
		$rows[] = array('', '', '', '', '', '學年不合格數目', '', '上學期', '下學期', '學年', '個人素質(二欠佳或四個尚可+欠佳)');
		$rows[] = array('排名(標準分)', '註冊編號', '班別', '學號', '中文姓名', '主科', '學科', '平均分', '平均分', '平均分', '上學期', '下學期', '曾留級別', '試升', '備註');
		
		# Loop Students
		for($i=0; $i<$student_count; $i++)
		{
			//$currentPage = 1;
			//$currentRow = $maxRow - $pageOneHeader;

			# Student Info
			$student_id = $StudentPromotionArr[$i]['StudentID'];
			$student_name = $StudentPromotionArr[$i]['StudentName'];
			$class_name = $StudentPromotionArr[$i]['ClassName'];
			$class_number = $StudentPromotionArr[$i]['ClassNumber'];
			$websams_no = $StudentPromotionArr[$i]['WebSAMSRegNo'];
			$form_order = $StudentPromotionArr[$i]['OrderMeritForm']==999? "--" : $StudentPromotionArr[$i]['OrderMeritForm'];
			$grand_avg = $StudentPromotionArr[$i]['GrandAverage']>=0? $StudentPromotionArr[$i]['GrandAverage'] : "--";
			$grand_avg = trim($grand_avg)==""? "--" : $grand_avg;
			$promote_status = $StudentPromotionArr[$i]['PromotionStatus'];
			$imported_promote_status = $StudentPromotionList[$student_id];
			if(isset($imported_promote_status) && $imported_promote_status["Promotion"]==3){
				$promote_status = ($imported_promote_status["FinalStatus"]==$eReportCard["PromotionStatus"][2])? "Y" : "N"; 
			}
			
			# Term Info
			$failMainSbj = $StudentFailCount[$student_id][0];
			$failSbj = $StudentFailCount[$student_id][1];
			$first_term_avg = $TermResultArr[1][$student_id][0]["GrandAverage"]>=0? $TermResultArr[1][$student_id][0]["GrandAverage"] : "--";
			$first_term_avg = trim($first_term_avg)==""? "--" : $first_term_avg;
			$sec_term_avg = $TermResultArr[2][$student_id][0]["GrandAverage"]>=0? $TermResultArr[2][$student_id][0]["GrandAverage"] : "--";
			$sec_term_avg = trim($sec_term_avg)==""? "--" : $sec_term_avg;
			$first_term_pc = $PCResultArr[1][$student_id];
			$first_term_pc = trim($first_term_pc)==""? "--" : $first_term_pc;
			$sec_term_pc = $PCResultArr[2][$student_id];
			$sec_term_pc = trim($sec_term_pc)==""? "--" : $sec_term_pc;
			
			# Repeat Info
			$StudentLevels = array();
			$StudentHistory = $lclass->returnStudentClassHistory($student_id);
			for($sch=0; $sch<count($StudentHistory); $sch++){
				$StudentLevels[] = preg_replace("/[^0-9]/", '', $StudentHistory[$sch]["ClassName"]);
			}
			$StudentRepeat = array_count_values($StudentLevels);
			$StudentLevels = array();
			foreach((array)$StudentRepeat as $formNumber => $inFormNum){
				if($inFormNum > 1){
					$StudentLevels[] = $formNumber;
				}
			}
			$repeatDisplay = "--";
			if(count($StudentLevels) > 0){
				sort($StudentLevels);
				$repeatDisplay = "S".implode(", S",$StudentLevels);
			}
			
			$x .= '	<tr>
						<td>'.$form_order.'</td>
						<td>'.$websams_no.'</td>
						<td>'.$class_name.'</td>
						<td>'.$class_number.'</td>
						<td>'.$student_name.'</td>
						<td>'.$failMainSbj.'</td>
						<td>'.$failSbj.'</td>
						<td>'.$first_term_avg.'</td>
						<td>'.$sec_term_avg.'</td>
						<td>'.$grand_avg.'</td>
						<td>'.$first_term_pc.'</td>
						<td>'.$sec_term_pc.'</td>
						<td>'.$repeatDisplay.'</td>
						<td>'.$promote_status.'</td>
						<td>--</td>
					</tr>';
			$rows[] = array($form_order, $websams_no, $class_name, $class_number, $student_name, $failMainSbj, $failSbj, $first_term_avg, $sec_term_avg, $grand_avg, $first_term_pc, $sec_term_pc, $repeatDisplay, $promote_status, '--');
					
			if($i!=0 && $i%30==0 && $student_count>($i+1)){
				$x .= '</table></div>';
				$x .= '</div>';
				$x .= '<div style="page-break-after:always">&nbsp;</div>';
				$x .= $reportHeader;
			}
		}
		
		// No record
		if($student_count==0){
			$x .= '	<tr>
						<td colspan="15">暫時係任何記錄</td>
					</tr>';
			$rows[] = array('暫時係任何記錄');
		}
		$x .= '</table>';
		
		if($type != 'csv'){
			return $x;
		}
		else{
			return $rows;
		}
	}
	
	// Return if position required for receiving award 
	function Report_SubjectGroup_Checking_for_Award_Generation($SGStatArr, $TargetSubjectArr, $ClassInfoArr, $FormID){
		$returnArr = array();
		
		// loop Subject
		foreach ((array)$TargetSubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
			$existingSG = $SGStatArr[$thisSubjectID];
			
			// More than 1 Subject Group
			if(count($existingSG)>1)
			{
				$returnArr[$thisSubjectID][0] = 5;
				continue;
			}
			else
			{
				foreach ((array)$existingSG as $thisSubjectGroupID => $thisSubjectGroupInfoArr) {
					$thisSubjectGroupInfoArr = $thisSubjectGroupInfoArr[$FormID];
					
					$totalCount = 0;
					foreach((array)$thisSubjectGroupInfoArr as $thisClassID => $thisClassInfoArr){
						$ClassTotal = $thisClassInfoArr["ClassTotal"];
						$totalCount += $ClassTotal;
						
						// Whole Class in Subject Group
						if($ClassTotal == $ClassInfoArr[$thisClassID]["NumberOfStudent"]){
							$returnArr[$thisSubjectID][$thisClassID] = 5;
						}
					}
					
					// Subject Group more than 30 people
					if($totalCount > 30){
						$returnArr[$thisSubjectID][0] = 5;
					}
					break;
				}
			}
		}
		return $returnArr;
	}
	
	// Return Fail Subject Count
	// [0] => Fail Count of Main Subject, [1] => Fail Count of Subject
	function RETURN_FAIL_SUBJECT_COUNT($ReportID, $ClassLevelID, $StudentIDArr, $skipTechnicalSubject=false){
		$returnArr = array();
		
		# Form
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID, 1);
		
		# Subject Related Info
		$SubjectClassInfoArr = $this->Get_Form_Main_Subject($ClassLevelID, 1);
		$SubjectClassInfoArr = BuildMultiKeyAssoc((array)$SubjectClassInfoArr, array("SubjectID"));
		$SubjectCodeMapping = $FormNumber && $FormNumber<4? $this->GET_SUBJECTS_CODEID_MAP(0, 0) : "";
		
		// loop Student
		for($i=0; $i<count($StudentIDArr); $i++){
			$StudentID = $StudentIDArr[$i];
			$failMainSubject = 0;
			$failSubject = 0;
			$specialScoreArr = array();
			
			// Get Marks
			$Marks = $this->getMarks($ReportID, $StudentID, '', 1, 1, '', '', 0, 0, '', true);
			
			// loop Subject
			$totalSubject = count($Marks);
			if($totalSubject > 0){
				foreach($Marks as $curSubjectID => $SubjectMark){
					if(empty($SubjectMark)){
						continue;
					}
					
					// Get Subject Code
					$thisSubjectCode = $SubjectCodeMapping[$curSubjectID];
					
					// Get Subject Marks
					$SubjectMark = $SubjectMark[0];
					
					// Get Subject Type
					$SubjectType = $SubjectClassInfoArr[$curSubjectID]["DisplayStatus"];
					
					// Get Score Display Settings
					$subjectSetting = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $curSubjectID, 1, 0, $ReportID);
					$ScaleDisplay = $subjectSetting['ScaleDisplay'];
					
					// Skip if Special Case Or Technical Subject
					if(($this->Check_If_Grade_Is_SpecialCase($SubjectMark['Grade']) && $SubjectMark['Grade']!="+") || ($skipTechnicalSubject && $SubjectType==2))
						continue;
					
					// Get Mark Nature
					$SubjectMark = ($SubjectType==1 || $ScaleDisplay=="M")? $SubjectMark["Mark"] : $SubjectMark["Grade"];
					$SubjectMarkNature = $this->returnMarkNature($ClassLevelID, $curSubjectID, $SubjectMark, 0, '', $ReportID);
					// for "+" - absent (zero mark)
					if($SubjectMark['Grade']=="+"){
						$SubjectMarkNature = "Fail";
						$SubjectMark = 0;
					}
					
					// F1-F3 - Phy, Chem, Bio
					if($FormNumber>0 && $FormNumber<4 && in_array($thisSubjectCode, $this->SpecialSubjectList)){
						$specialScoreArr[] = $SubjectMark;
					}
					// Other Subjects
					// Count Fail Number
					else if($SubjectMarkNature=="Fail"){
						// Main Subject
						if($SubjectType==0){
							$failMainSubject++;
						}
						// Other Subject
						else{
							$failSubject++;
						}
					}
				}
				
				// F1-F3 - Phy, Chem, Bio
				// Check if fail (Average < 50)
				if(count($specialScoreArr) > 0){
					$isSubjectFail = (array_sum((array)$specialScoreArr) / count($specialScoreArr)) < 50;
					if($isSubjectFail)
						$failSubject++;
				}
			}
			$returnArr[$StudentID] = array($failMainSubject, $failSubject);
		}
		return $returnArr;
	}
	
	// Return Personal Characterisitics Status
	// ["AllGood"] => All PC at least Good?, ["Excellent"] => Reach Excellent Level?
	function returnPCRecordExcellentStatus($TermReportAry, $StudentIDAry){
		$returnAry = array();
		
		// loop Term Report
		foreach((array)$TermReportAry as $TermID => $ReportInfo){
			$TermReportID = $ReportInfo["ReportID"];
			$TermNum = $this->Get_Semester_Seq_Number($TermID);
			
			// loop Student
			for($i=0; $i<count($StudentIDAry); $i++){
				$StudentID = $StudentIDAry[$i];
				$ReturnExcellent = false;
				$ReturnAllGood = false;
				
				// Get data
				$pcData = $this->getPersonalCharacteristicsData($StudentID, $TermReportID, 0);
				$pcData = $pcData[0]["CharData"];
				if(trim($pcData)!=""){
					$count_excellent = 0;
					$count_good = 0;
					$count_poor = 0;
				
					$pcData = explode(",", $pcData);
					for($j=0; $j<count($pcData); $j++){
						$data = explode(":", $pcData[$j]);
						$data = $data[1];
						
						if($data==1 || $data==2){
							$count_excellent++;
						}
						else if($data==3){
							$count_good++;
						}
						else{
							$count_poor++;
						}
					}
					
					// Poor = 0
					if($count_poor==0){
						$ReturnAllGood = true;
					}
					// Excellent >= 4 And Good = 0-2
					if($count_poor==0 && $count_excellent>=4){
						$ReturnExcellent = true;
					}
				}
				$returnArr[$TermNum][$StudentID]["AllGood"] = $ReturnAllGood;
				$returnArr[$TermNum][$StudentID]["Excellent"] = $ReturnExcellent;
			}
		}
		return $returnArr;
	}
	
	// Return Personal Characterisitics Fail? - "Y" or "N"
	function returnPCRecordFailStatus($TermReportAry, $StudentIDAry){
		$returnAry = array();
		
		// loop Term Report
		foreach((array)$TermReportAry as $TermID => $ReportInfo){
			$TermReportID = $ReportInfo["ReportID"];
			$TermNum = $this->Get_Semester_Seq_Number($TermID);
			
			// loop Student
			for($i=0; $i<count($StudentIDAry); $i++){
				$StudentID = $StudentIDAry[$i];
				$PCRecordStatus = "N";
				
				// Get PC data
				$pcData = $this->getPersonalCharacteristicsData($StudentID, $TermReportID, 0);
				$pcData = $pcData[0]["CharData"];
				if(trim($pcData)!=""){
					$count_poor = 0;
					$count_average = 0;
				
					$pcData = explode(",", $pcData);
					for($j=0; $j<count($pcData); $j++){
						$data = explode(":", $pcData[$j]);
						$data = $data[1];
						
						if($data==4){
							$count_average++;
						}
						else if($data==5){
							$count_poor++;
						}
					}
					
					// Poor >= 2  OR  (Poor = 1 AND Average = 4)
					if($count_poor>=2 || ($count_poor==1 && $count_average>=4)){
						$PCRecordStatus = "Y";
					}
				}
				$returnArr[$TermNum][$StudentID] = $PCRecordStatus;
			}
		}
		return $returnArr;
	}
	
	// Return OLE Records that decided to display in ReportCard
	function returnOLEStudentRecords($StudentID, $TermID=""){
		global $eclass_db;
		
		$term_cond = trim($TermID!="")? " AND p.YearTermID = '$TermID'" : "";
		
		$sql = "Select 
					os.Title, os.Role, os.Achievement 
				From ".$eclass_db.".OLE_STUDENT os
					Inner Join ".$eclass_db.".OLE_PROGRAM op ON (os.ProgramID = op.ProgramID) 
				Where
					os.UserID = '$StudentID' AND os.displayIneRC = 1 
					AND op.AcademicYearID = '".$this->GET_ACTIVE_YEAR_ID()."' $term_cond
				Order By
					os.StartDate";
		$OLEArr = $this->returnArray($sql);
		
		return $OLEArr;
	}

	# Map Subject Code and Subject ID
	function GET_SUBJECT_SUBJECTCODE_MAPPING(){
		
		$sql = "SELECT 
					CODEID,
					RecordID
				FROM
					ASSESSMENT_SUBJECT 
				WHERE
					EN_DES IS NOT NULL
					AND RecordStatus = 1
					AND (CMP_CODEID IS NULL || CMP_CODEID = '')
				ORDER BY
					DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 2);
		
		$ReturnArr = array();
		if (sizeof($SubjectArr) > 0) {
			for($i=0; $i<sizeof($SubjectArr); $i++) {
				$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
			}
		}
		return $ReturnArr;
	}
	
	// Return Chinese Number
	function RETURN_CHINESE_NUM($num){
		global $eReportCard;
		return $eReportCard['Template']['MSTable']['MeritNumChi'][$num];
	}
	
	// Return AwardID
	private function Get_AwardID_By_AwardCode($AwardCode) {
		global $lreportcard_award;
		$targetAward = $lreportcard_award->Get_Award_Info_By_Code($AwardCode);
		return $targetAward['AwardID'];
	}
}
?>