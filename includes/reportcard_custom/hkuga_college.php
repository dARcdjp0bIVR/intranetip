<?php
# Editing by 
/*
 * 2020-03-05 Bill: Special Handling for Drama Education    [2020-0203-1139-58206]
 *
 * 2019-01-16 Bill: added Case Subject handling         [2019-0111-1652-08289]
 * 
 * 2017-06-13 Bill:	Update 2 Elective Display Format	[2017-0602-0941-40164]
 * 
 * 2017-02-10 Villa: Update Wording for the latest change of v16
 * 				     Change Titile from HardCoding in en to dynamic lang depanding on LangDisplay
 * 
 * 2016-12-28 Villa: Update the page design from v7 to v10
 * 
 * 2016-12-05 Villa: Modified all function: redesign for the new term report	
 * 
 * 2012-02-23 Ivan [2012-0223-1308-08073 - 港大同學會書院 - Fail to display some subjects]
 * 	- Hide page checking: added to hide the subject if the student are not in the subject group of the subject
 * 
 * 2012-02-23 Ivan [2012-0223-1308-08073 - 港大同學會書院 - Fail to display some subjects]
 * 	- Display the subject even if the subject grades are all N.A. 
 * 
 */

include_once($intranet_root."/lang/reportcard_custom/hkuga_college.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("award", "eca", "attendance", "transcript");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		// Moved to config file
		//$this->textAreaMaxChar = 509;
		//$this->textAreaMaxChar_SubjectTeacherComment = 480;
		
		$this->EmptySymbol = "&nbsp;";
		$this->Spacing = "&nbsp;&nbsp;";
		//$this->PersonalCharGradeArr[Index] = LangKey
		$this->DefaultPersonalCharGrade = 60;
		$this->PersonalCharGradeArr['60'] = '6';
		$this->PersonalCharGradeArr['50'] = '5';
		$this->PersonalCharGradeArr['40'] = '4';
		$this->PersonalCharGradeArr['30'] = '3';
		$this->PersonalCharGradeArr['20'] = '2';
		$this->PersonalCharGradeArr['10'] = '1';
		
		$this->SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP();
		
//		080		中國語文 (26)
//		165		English Language (28)
//		280		Mathematics (85)
// 		280C	Mathematics Compulsory Part (51)
// 		281		Mathematics Extended Part Module 1 (40)
// 		282		Mathematics Extended Part Module 2 (42)
// 		310		Physical Education (41)
// 		265		Liberal Studies (36)
		$this->NonElectiveSujectCode = array('080','165','280','280C','281','282','310','265');
		
//		075		Chinese History (34)
//		090		Chinese Literature (10)
		$this->ElectiveDisplayNLSubjectCode = array('075', '090');
		
		$this->PersonalCharAdminOnlyGradeArr = array('10', '20');
	}
		
	########## START Template Related ##############
	function GetLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID, $SubjectID='', $SubjectSectionOnly='')
	{
		$count = 0; //count how many page printed
		$pageNumber = 2;
		$x = '';
		
		$x .= "<tr><td valign='top'>";
		
		if ($SubjectSectionOnly) {
			$x .= $this->SubjectPage($ReportID, $StudentID, array($SubjectID), $isLastPage=true, $FromMarksheet=true,$pageNumber);
			$pageNumber++;
		}
		else {
			### Student Cover Page ###
			$x .= $this->CoverPage($ReportID,$StudentID);
			$count++;
			### Student Info Page ###
			$x .= $this->FirstPage($ReportID, $StudentID);
			$count++;
			
			### Subejct Pages
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
// 			$isForm6 = $this->isFrom6($ClassLevelID);
			$Semester = $ReportSetting['Semester'];
			$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, 0, '', '', 'Desc', 0, $ReportID);
			$numOfMainSubject = count($MainSubjectArray);
			
			$SubjectCounter = 0;
			$tmpSubjectIDArr = array();
			foreach((array)$MainSubjectArray as $SubjectID => $SubjectData)
			{
				$SubjectCounter++;
				
				# Hide the subject if all columns are exempted
				if ($ReportID && $StudentID)
				{
					//2012-0223-1308-08073 
					if (!$this->Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID, $Semester)) {
						continue;
					}
					
					//2012-0223-1308-08073
					//$exemptArr = array("abs", "+", "-", "N.A.");
					$exemptArr = array("abs", "+", "-");
					
					$SubjectNameDisplay = strtoupper($this->GET_SUBJECT_NAME_LANG($SubjectID, 'EN'));
					// check if modular study subject (only applicable to S3 templates)
					if ($FormNumber==3 || $FormNumber==0) {
					    $isModularStudySubject = $this->isModularStudySubject($SubjectID);
						if ($isModularStudySubject) {
							$exemptArr[] = 'N.A.';
						}
					}
					
					// [2019-0111-1652-08289]
					// check if case subject (only applicable to S1 & S2 templates)
					if ($FormNumber==1 || $FormNumber==2 || $FormNumber==0) {
					    $isCaseSubject = $this->isCaseSubject($SubjectID);
    					if ($isCaseSubject) {
    					    $exemptArr[] = 'N.A.';
    					}
					}
					
					$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
					
					$MarksAry = $this->getMarks($ReportID, $StudentID, '', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID, '', $CheckPositionDisplay=0, $SubOrderArr='');
					
					$skipFlag = true;
					foreach ($ColumnTitle as $thisColumnID => $thisTitle)
					{
						$thisMark = $MarksAry[$SubjectID][$thisColumnID]["Mark"];
						$thisGrade = $MarksAry[$SubjectID][$thisColumnID]["Grade"];
						
						if ( (is_numeric($thisMark) && $thisMark!=-1) )
						{
							if ($thisMark != 0)
							{
								// Mark
								$skipFlag = false;
								break;
							}
							else
							{
								// Grade
								if (in_array($thisGrade, $exemptArr) == false)
								{
									$skipFlag = false;
									break;
								}
							}
						}
					}
					
					if ($skipFlag)
						continue;
				}
	
				$tmpSubjectIDArr[] = $SubjectID;
				$thisNumOfTmpSubject = count($tmpSubjectIDArr);
				
				if ($SubjectCounter == $numOfMainSubject)
					$isLastPage = true;
				else
					$isLastPage = false;
					
				if ($thisNumOfTmpSubject == 2 || $SubjectCounter == $numOfMainSubject)
				{
					$x .= $this->SubjectPage($ReportID, $StudentID, $tmpSubjectIDArr, $isLastPage,false,$pageNumber);
					$tmpSubjectIDArr = array();
					$count++;
					$pageNumber++;
				}
			}
			
			// Last Page with Single Subject (if any)
			if (count($tmpSubjectIDArr) > 0)
			{
				$x .= $this->SubjectPage($ReportID, $StudentID, $tmpSubjectIDArr, true, false, $pageNumber);
				$tmpSubjectIDArr = array();
				$count++;
				$pageNumber++;
			}
		}
		### Subejct END ###
		
		### fill Blank Page START###
		if(($count+1)%4>0){ //+1 for last Page
			$numOfBlankPage = 4-($count+1)%4;
		}else{
			$numOfBlankPage=0;
		}
		for($i=0;$i<$numOfBlankPage;$i++){
			$x .= $this->BlankPage($pageNumber);
			$pageNumber++;
		}
		### fill Blank Page END ###
		
		### Print Last Page START###
		$x .= $this->LastPage();
		### Print Last Page END###
		
		$x .= "</td></tr>";
		
 		return $x; 
	}
	function CoverPage($ReportID='',$StudentID=''){
		global $eReportCard, $PATH_WRT_ROOT;
		if($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
				
			$data['NameEn'] = $lu->EnglishName;
			$data['NameCh'] = $lu->ChineseName;
// 			$data['STRN'] = ($lu->STRN=='')? '&nbsp;' : $lu->STRN;
				
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$data['Class'] = $StudentInfoArr[0]['ClassName'];
			$data['ClassNo'] = $StudentInfoArr[0]['ClassNumber'];
		}else{
			$data['NameEn'] = 'XXX';
			$data['NameCh'] = 'XXX';
			$data['Class'] = 'XXX';
			$data['ClassNo'] = 'XXX';
		}
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle =  str_replace(":_:", "<br>", $ReportSetting['ReportTitle']);
		$SemID =  $ReportSetting['Semester'];
		$Semester = $this->returnSemesters($SemID);
		$AcademicYear =  $this->GetAcademicYear($SemID);
		
		$x = "";
		$x .= "<table width='100%' align='right'>";
			$x .= "<tr>";
				$x .= "<td height='250px'></td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td align ='right' class='text20pt spacing_right_title'>";
					$x .= $AcademicYear."&nbsp;".$Semester."&nbsp;".$eReportCard['ReportCard'];
					$x .= "</br>";
					$x .= "</br>";
				$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td align ='right' class='text20pt spacing_right_title'>";
					$x .= Get_Lang_Selection($data['NameCh'], $data['NameEn']);
					$x .= "</br>";
				$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td align ='right' class='text20pt spacing_right_title'>";
					$x .= $data['Class']."(".$data['ClassNo'].")";
					$x .= "</br>";
				$x .= "</td>";
			$x .= "</tr>";
		$x .= "</table>";
		# Build the whole page
		$r = "
			<table height=\"1050px\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"page-break-after:always\">
			<tr valign='top'><td>". $x ."</td><tr>
			</table>
		";
		return $r;
	}
	function FirstPage($ReportID, $StudentID='')
	{
		$x = "";
		$x .= '<br />'."\n";
		
		# Header
		$x .= $this->ReportHeader($ReportID, $StudentID, 1);
// 		$x .= '<br />'."\n";
		
		$x .= $this->Get_Achivement_Attitude_Remarks_Table();
// 		$x .= '<br />'."\n";
		
		$x .= $this->Comment_Attendance_ECA_Table($ReportID, $StudentID);
// 		$x .= '<br />'."\n";
		
		# Signature Table
		$x .= $this->SignatureTable($ReportID, $StudentID);
		
		#PageNumber
		$pageNumberDisplay = "<tr valign='bottom' align='left'>";
			$pageNumberDisplay .= "<td>";
				$pageNumberDisplay .= '1';
			$pageNumberDisplay .= "</td>";
		$pageNumberDisplay .= "</tr>";
		# Build the whole page
		$r = "
			<table width=\"100%\" height=\"1050px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"page-break-after:always\">
			<tr><td height=\"950px\" valign=\"top\" >". $x ."</td><tr>
			$pageNumberDisplay
			</table>
		";
		
		return $r;
	}
	
	function SubjectPage($ReportID='', $StudentID='', $SubjectIDArr='', $isLastPage=false, $FromMarksheet=false, $pageNumber='')
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$x = '';
		$x .= '<br />'."\n";
		
		# Header
		$x .= $this->ReportHeader($ReportID, $StudentID);
		$x .= '<br />'."\n";
		
		# Subject Result
		$numOfSubject = ($SubjectIDArr == '')? 0 : count($SubjectIDArr);
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$x .= $this->SubjectResultInfo($ReportID, $StudentID, $SubjectIDArr[$i], $FromMarksheet);
			$x .= '<br />'."\n";
			$x .= '<br />'."\n";
// 			$x .= '<br />'."\n";
// 			$x .= '<br />'."\n";
		}
		
		# Build the whole page
		if ($isLastPage == true){
			$page_break = 'style="page-break-after:always"';
// 			$page_break = '';
		}else{
			$page_break = 'style="page-break-after:always"';
		}
		if($pageNumber%2==0){
			$pageNumAlign = 'right';
		}else{
			$pageNumAlign = 'left';
		}
		$pageNumberDisplay = "<tr valign='bottom' align='".$pageNumAlign."'>";
			$pageNumberDisplay .= "<td>";
			$pageNumberDisplay .= $pageNumber;
			$pageNumberDisplay .= "</td>";
		$pageNumberDisplay .= "</tr>";
		
		$r = "
			<table width=\"100%\" height=\"1050px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ".$page_break.">
			<tr><td valign=\"top\" height=\"950px\">". $x ."</td><tr>
					$pageNumberDisplay
			</table>
		";
		
		return $r;
	}
	
	function ReportHeader($ReportID, $StudentID='', $ForFirstPage=0)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		### Academic Year & Term info
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle =  str_replace(":_:", "<br>", $ReportSetting['ReportTitle']);
		$SemID =  $ReportSetting['Semester'];
		$Semester = $this->returnSemesters($SemID);
		
		$ReportTitle =  $ReportSetting['ReportTitle'];
		$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
		$AcademicYear = $this->GET_ACTIVE_YEAR('-');
		//$SchoolLogo = GET_SCHOOL_BADGE();
		$SchoolLogo = $image_path."/file/reportcard2008/templates/st_stephen_logo.jpg";
		
		
		### Student info
		# retrieve required variables
		if ($ForFirstPage)
			$data['DateOfIssue'] = $this->FormatDate($ReportSetting['Issued'], 'D/M/Y');
		else
		{
			$data['DateOfIssue'] = date('F Y', strtotime($ReportSetting['Issued']));
			$data['DateOfIssue'] = strtoupper($data['DateOfIssue']);
		}
		
		$defaultVal = "XXX";
		if($StudentID)		# retrieve Student Info
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			
			$data['NameEn'] = $lu->EnglishName;
			$data['NameCh'] = $lu->ChineseName;
			$data['STRN'] = ($lu->STRN=='')? '&nbsp;' : $lu->STRN;
			
			$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
			$data['Class'] = $StudentInfoArr[0]['ClassName'];
			$data['ClassNo'] = $StudentInfoArr[0]['ClassNumber'];
		}
		else
		{
			$data['Class'] = $defaultVal;
			$data['ClassNo'] = $defaultVal;
			$data['NameCh'] = $defaultVal;
			$data['NameEn'] = $defaultVal;
			$data['STRN'] = $defaultVal;
		}
		
		$x = '';
		$x .= "<table class='report_border lite_grey_bg' width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>";
			# School Name
			$x .= "<tr>";
				$x .= "<td class='text11pt' width='120'>&nbsp;</td>";
				$x .= "<td class='text11pt' align='center'><b>".$eReportCard['Template']['SchoolName']."</b></td>";
				$thisValue = ($ForFirstPage==0)? $data['DateOfIssue'] : '&nbsp;';
				$x .= "<td class='text11pt' width='120' align='right'>".$thisValue."</td>";
			$x .= "</tr>";
			
			# Report title
			if ($ForFirstPage)
			{
				$x .= "<tr>";
					$x .= "<td class='text11pt' align='center' colspan='3'><b>".$ReportTitle."</b></td>";
				$x .= "</tr>";
			}
		$x .= "</table>";
		
// 		$x .= "<br />";
		
		$x .= "<table class='border_right border_bottom border_left' width='100%' border='0' cellpadding='5' cellspacing='0' align='center' >";
			$x .= "<tr>";
				# Name
				$thisWidth = ($ForFirstPage == 1)? '55%' : '55%';
				$x .= "<td class='text11pt' width='$thisWidth'>";
					$x .= "<table cellspacing='0' cellpadding='0' width='100%'>";
						$x .= "<tr>";
							$x .= "<td class='text11pt'><b>".$eReportCard['Template']['StudentInfo']['NameEn']."</b></td>";
							$x .= "<td class='text11pt' width='5%'> : </td>";
							$thisWidth = ($ForFirstPage == 1)? '85%' : '85%';
							$x .= "<td class='text11pt' width='$thisWidth'>".$data['NameEn']."&nbsp;".$data['NameCh']."</td>";
// 							$thisWidth = ($ForFirstPage == 1)? '80%' : '80%';
// 							$x .= "<td class='text11pt' width='$thisWidth'>".$data['NameCh']."</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
				
				# Class (Class Number)
				$thisWidth = ($ForFirstPage == 1)? '45%' : '45%';
				$thisTitle = $eReportCard['Template']['StudentInfo']['ClassEn'].' ('.$eReportCard['Template']['StudentInfo']['ClassNoEn'].')';
				$x .= "<td class='text11pt border_left' width='$thisWidth'>";
					$x .= "<table cellspacing='0' cellpadding='0' width='100%'>";
						$x .= "<tr>";
							$x .= "<td class='text11pt'><b>".$thisTitle."</b></td>";
							$x .= "<td class='text11pt' width='5%'> : </td>";
							$thisWidth = ($ForFirstPage == 1)? '20%' : '20%';
							$x .= "<td class='text11pt' width='$thisWidth'>".$data['Class']."</td>";
							$thisWidth = ($ForFirstPage == 1)? '20%' : '20%';
							$x .= "<td class='text11pt' width='$thisWidth'>(".$data['ClassNo'].")</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			
			if ($ForFirstPage == 1)
			{
				$x .= "<tr>";
					# Student Number
					$x .= "<td class='text11pt border_top' width='$thisWidth'>";
						$x .= "<table cellspacing='0' cellpadding='0' width='100%'>";
							$x .= "<tr>";
								$x .= "<td class='text11pt'><b>".$eReportCard['Template']['StudentInfo']['StudentNoEn']."</b></td>";
								$x .= "<td class='text11pt' width='5%'> : </td>";
								$x .= "<td class='text11pt' width='65%'>".$data['STRN']."</td>";
							$x .= "</tr>";
						$x .= "</table>";
					$x .= "</td>";
				
					# Date of Issue
					$x .= "<td class='text11pt border_top border_left' width='$thisWidth'>";
						$x .= "<table cellspacing='0' cellpadding='0' width='100%'>";
							$x .= "<tr>";
								$x .= "<td class='text11pt'><b>".$eReportCard['Template']['IssueDateEn']."</b></td>";
								$x .= "<td class='text11pt' width='5%'> : </td>";
								$x .= "<td class='text11pt' width='40%'>".$data['DateOfIssue']."</td>";
							$x .= "</tr>";
						$x .= "</table>";
					$x .= "</td>";
				$x .= "</tr>";
			}
		$x .= "</table>";
		
		
		return $x;
	}
	
	function Get_Achivement_Attitude_Remarks_Table()
	{
		global $eReportCard, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$emptyImagePath = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/10x10.gif";
		$arrowImagePath = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/ereportcard/double_arrow.png";
		
		$x = '';
		$x .= "<table class='border_right border_bottom border_left' width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>";
			$x .= "<col style='width:30' />";
			$x .= "<col style='width:50' />";
			$x .= "<col style='width:20' />";
			$x .= "<col />";
			
			# Intro
			
			$x .= "<tr>";
				$x .= "<td colspan='4'>";
					$x .= "<div class='text11pt spacing_2_both' style='text-align:justify;'>".$eReportCard['Template']['AchievementRemark']['Intro']."</div>";
				$x .= "</td>";
			$x .= "</tr>";
			
			# Achievement Title
			$x .= "<tr>";
				$x .= "<td colspan='4' class='lite_grey_bg border_top text11pt spacing' >";
// 					$x .= "<b>".$eReportCard['Template']['AchievementEn'].":-".$eReportCard['Template']['GradeCriteriaEn']."</b>";
					$x .= "<b>".$eReportCard['Template']['AchievementEn'].": ".$eReportCard['Template']['LevelDescriptionEn']."</b>";
				$x .= "</td>";
			$x .= "</tr>";
			
			# Achievement Remarks
// 			debug_pr($eReportCard['Template']['AchievementGradeRemark']);
			foreach((array)$eReportCard['Template']['AchievementGradeRemark'] as $key => $remarks)
			{
					
				$x .= "<tr height='35' align='middle'>";
					$x .= "<td class='border_top text11pt' width='30' align='center'><b>".$key."</b></td>";
					$x .= "<td class='border_top border_left text11px spacing_2_both' colspan='3'>";
						$x .= "<div style='text-align:justify;' class='text11pt '>".$remarks."</div>";
					$x .= "</td>";
				$x .= "</tr>";
			}
			
		$x .= "</table>";
		
		return $x;
	}
	
	function Comment_Attendance_ECA_Table($ReportID, $StudentID='')
	{
		global $eReportCard;
		
		# Get Class Teacher Comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$ClassTeacherComment = $CommentAry[0];
		
		# Get CSV Info
		$lastSemID = $this->Get_Last_Semester_Of_Report($ReportID);
		$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
		$CsvInfoArr = $OtherInfoDataAry[$StudentID][$lastSemID];
		$NumOfAbsent =  ($CsvInfoArr['Number of days absent']=='')? $this->EmptySymbol : $CsvInfoArr['Number of days absent'];
		$NumOfLate =  ($CsvInfoArr['Number of times late']=='')? $this->EmptySymbol : $CsvInfoArr['Number of times late'];
		
// 		$Activities =  ($CsvInfoArr['Activities']=='')? $this->EmptySymbol : $CsvInfoArr['Activities'];
// 		$Responsibilities =  ($CsvInfoArr ['Responsibilities/Services']=='')? $this->EmptySymbol : $CsvInfoArr['Responsibilities/Services'];
// 		$Awards = ($CsvInfoArr['Awards']=='')? $this->EmptySymbol : $CsvInfoArr['Awards'];
		$Activities_CSV = $CsvInfoArr['Activities'];
		$Responsibilities_CSV =  $CsvInfoArr['Responsibilities/Services'];
		$Awards_CSV =  $CsvInfoArr['Awards'];
		//villa
		
		### check if column break
		$hasColumnBreak = false;
		if(strpos($Activities_CSV,'####') ){
			$hasColumnBreak = true;
		}
		if(strpos($Responsibilities_CSV,'####') ){
			$hasColumnBreak = true;
		}
		if(strpos($Awards_CSV,'####') ){
			$hasColumnBreak = true;
		}
		
		$Activities_CSV = str_replace("####", "##ColumnBreak##", $Activities_CSV);
		$Responsibilities_CSV = str_replace("####", "##ColumnBreak##", $Responsibilities_CSV);
		$Awards_CSV = str_replace("####", "##ColumnBreak##", $Awards_CSV);
		
		##explode the line break
		if(!is_array($Activities_CSV)&&!empty($Activities_CSV)){
			$Activities = explode('##',$Activities_CSV);
		}else{
			$Activities = $Activities_CSV;
		}
		if(!is_array($Responsibilities_CSV)&&!empty($Responsibilities_CSV)){
			$Responsibilities = explode('##',$Responsibilities_CSV);
		}else{
			$Responsibilities = $Responsibilities_CSV;
		}
		if(!is_array($Awards_CSV)&&!empty($Awards_CSV)){
			$Awards = explode('##',$Awards_CSV);
		}else{
			$Awards = $Awards_CSV;
		}
			
		$x = '';
		$x .= "<table class='border_bottom border_left border_right' border='0' width='100%' cellpadding='2' cellspacing='0' align='center' style='border-collapse:collapse'>";
			# Tutor's Comments
			$x .= "<tr>";
				$x .= "<td width='75%' class='lite_grey_bg text11pt border_right spacing'>";
					$x .= "<b>".$eReportCard['Template']['ClassTeacherCommentsEn'].":</b>";
				$x .= "</td>";
				$x .= "<td width='25%'  class='lite_grey_bg text11pt' align='center'>";
				$x .= "<b>".$eReportCard['Template']['DaysAbsentEn']."</b>";
				$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr >";
				$x .= "<td  rowspan='3' class='border_top border_right spacing_2_both'>";
					$x .= "<table width='99%' border='0'>";
						$x .= "<tr><td class='text11pt spacing_2_both'>".Get_String_Display(nl2br($ClassTeacherComment))."</td></tr>";
					$x .= "</table>";
				$x .= "</td>";
				$x .= "<td  align='center' class='border_top text11pt'>";
					$x .= $NumOfAbsent;
				$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td class='lite_grey_bg text11pt border_top' align='center'><b>";
				$x .= $eReportCard['Template']['TimesLateEn'];
				$x .= "</b></td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td align='center' class='text11pt border_top '>";
				$x .= $NumOfLate;
				$x .= "</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		#Repsonsiblities/Services Awards Tiltle Data Start
		//method 1
		if($hasColumnBreak){
		
			$Column = 0;
			$start = '';
			
			$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['ResponsibilityServicesEn'] ."</b></u></div>";
			if(!empty($Responsibilities)){
// 				$Data[$Column][] = "<u><b>".$eReportCard['Template']['AttendanceEn']."</b></u>";
				foreach ((array)$Responsibilities as $_Responsibilities){
					if($_Responsibilities == "ColumnBreak"){
						$Column++;
						$Data[$Column][] = "<div class='spacing_height text11pt'><u><b>".$eReportCard['Template']['ResponsibilityServicesEn'] ."(con't)"."</b></u></div>";
						continue; //if ColumnBreak Skip Printing
					}
					if($_Responsibilities!=$this->EmptySymbol){
// 						$temp = "&diams; ".$_Responsibilities;
						$temp = "<table style='height: 100%;'>";
						$temp .= "<tr style='line-height:14px;'>";
						$temp .= "<td class='text11pt' valign='top'>&diams;</td>";
						$temp =  "<td class='text11pt' valign='top'>".$_Responsibilities."</td>";
						$temp .= "</tr>";
						$temp .= "</table>";
					}else{
						$temp = '--';
					}
					$Data[$Column][] = $temp;
				}
				$start = "<br>";
			}else{
				$Data[$Column][] = '--';
			}
			
			$Data[$Column][] = "<div class='spacing_height text11pt'><u><b>".$eReportCard['Template']['ActivitiesEn']."</b></u></div>";
			if(!empty($Activities)){
// 				if(!empty($start)){
// 					$Data[$Column][] = $start;
// 				}
// 				$Data[$Column][] = "<u><b>".$eReportCard['Template']['ActivitiesEn']."</b></u>";
				foreach ((array)$Activities as $Activity){
					if($Activity == "ColumnBreak"){
						$Column++;
						$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['ActivitiesEn']."(con't)"."</b></u></div>";
						continue; //if ColumnBreak Skip Printing
					}
					if($Activity!=$this->EmptySymbol){
// 						$temp = "&diams; ".$Activity;
						$temp = "<table style='height: 100%;'>";
						$temp .= "<tr style='line-height:14px;'>";
						$temp .= "<td class='text11pt' valign='top'>&diams;</td>";
						$temp =  "<td class='text11pt' valign='top'>".$Activity."</td>";
						$temp .= "</tr>";
						$temp .= "</table>";
					}else{
						$temp = '--';
					}
					$Data[$Column][] = $temp;
				}
			}else{
				$Data[$Column] = "--";
			}
			
			$Data[$Column][] = "<div class='spacing_height text11pt'><u><b>".$eReportCard['Template']['AwardsEn']."</b></u></div>";
			if(!empty($Awards)){
// 				if(!empty($start)){
// 					$Data[$Column][] = $start;
// 				}
// 				$Data[$Column][] = "<u><b>".$eReportCard['Template']['AwardsEn']."</b></u>";
				foreach ((array)$Awards as $_Awards){
					if($_Awards == "ColumnBreak"){
						$Column++;
						$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['AwardsEn']."(con't)"."</b></u></div>";
						continue; //if ColumnBreak Skip Printing
					}
					if($_Awards!=$this->EmptySymbol){
						$temp = "<table style='height: 100%;'>";
						$temp .= "<tr style='line-height:14px;'>";
						$temp .= "<td class='text11pt' valign='top'>&diams;</td>";
						$temp =  "<td class='text11pt' valign='top'>".$_Awards."</td>";
						$temp .= "</tr>";
						$temp .= "</table>";
					}else{
						$temp = '--';
					}
					$Data[$Column][] = $temp;
				}
			}else{
				$Data[$Column][] = '--';
			}
			
		}else{ // method 2 column break if line > 10
			$Column = 0;
			$start = '';
			
			$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['ResponsibilityServicesEn'] ."</b></u></div>";
			if(!empty($Responsibilities)){
// 				$Data[$Column][] = "<u><b>".$eReportCard['Template']['AttendanceEn']."</b></u>";
				foreach ((array)$Responsibilities as $_Responsibilities){
					if($_Responsibilities!=$this->EmptySymbol){
						$temp = "<table style='height: 100%;'>";
						$temp .= "<tr style='line-height:10px;'>";
						$temp .= "<td class='text11pt spacing_left_only' valign='top'>&diams;</td>";
						$temp .= "<td class='text11pt' valign='top'>".$_Responsibilities."</td>";
						$temp .= "</tr>";
						$temp .= "</table>";
								
					}else{
							$temp = "<div class='spacing_left_only'>--</div>";
					}
					$Data[$Column][] = $temp;
					if(sizeof($Data[$Column])>7){
						$Column++;
						$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['ResponsibilityServicesEn'] ."(con't)"."</b></u></div>";
					}
				}
				$start = "<br>";
			}else{
				$Data[$Column][] =  "<div class='spacing_left_only'>--</div>";
			}
			
			$Data[$Column][] = "<div class='spacing_height text11pt'><u><b>".$eReportCard['Template']['ActivitiesEn']."</b></u></div>";
			if(!empty($Activities)){
// 				if(!empty($start)){
// 					$Data[$Column][] = $start;
// 				}
// 				$Data[$Column][] = "<u><b>".$eReportCard['Template']['ActivitiesEn']."</b></u>";
				foreach ((array)$Activities as $_key=>$Activity){
					if($Activity!=$this->EmptySymbol){
						$temp = "<table style='height: 100%;'>";
						$temp .= "<tr  style='line-height:10px;'>";
						$temp .= "<td class='text11pt spacing_left_only' valign='top'>&diams;</td>";
						$temp .= "<td class='text11pt' valign='top'>".$Activity."</td>";
						$temp .= "</tr>";
						$temp .= "</table>";
					}else{
							$temp = "<div class='spacing_left_only'>--</div>";
					}
					$Data[$Column][] = $temp;
					if(sizeof($Data[$Column])>7&&$Activities[$_key+1]!=''){
						$Column++;
						$ColumnAdd = true;
						$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['ActivitiesEn']."(con't)"."</b></u></div>";
					}
				}
			}else{
				$Data[$Column][] =  "<div class='spacing_left_only'>--</div>";
			}
			
			$Data[$Column][] = "<div class='spacing_height text11pt'><u><b>".$eReportCard['Template']['AwardsEn']."</b></u></div>";
			if(!empty($Awards)){
// 				if(!empty($start)){
// 					$Data[$Column][] = $start;
// 				}
// 				$Data[$Column][] = "<u><b>".$eReportCard['Template']['AwardsEn']."</b></u>";
				foreach ((array)$Awards as $_key=>$_Awards){
					if($_Awards!=$this->EmptySymbol){
						$temp = "<table style='height: 100%;'>";
						$temp .= "<tr  style='line-height:14px;'>";
						$temp .= "<td class='text11pt spacing_left_only' valign='top'>&diams;</td>";
						$temp .= "<td class='text11pt' valign='top'>".$_Awards."</td>";
						$temp .= "</tr>";
						$temp .= "</table>";
					}else{
						$temp = "<div class='spacing_left_only'>--</div>";
					}
					$Data[$Column][] = $temp;
					if(sizeof($Data[$Column])>7&&$Awards[$_key+1]!=''){
						$Column++;
						$Data[$Column][] = "<div class='spacing_height2 text11pt'><u><b>".$eReportCard['Template']['AwardsEn']."(con't)"."</b></u></div>";
					}
				}
			}else{
				$Data[$Column][] =  "<div class='spacing_left_only'>--</div>";
			}
		}
		$numOfColumn = count($Data);
		if($numOfColumn>0){
			$width = 100/$numOfColumn;
		}
		$x .= "<table class='border_bottom border_left border_right' border='0' width='100%' cellpadding='2' cellspacing='0' align='center' style='border-collapse:collapse'>";
		$x .= "<tr>";
			$x .= "<td class='lite_grey_bg text11pt border_bottom spacing' width='100%' colspan='$numOfColumn'>";
				$x .= "<b>".$eReportCard['Template']['NonAcademicAchievements']."</b>";
			$x .= "</td>";
		$x .= "</tr>";
		$line = sizeof($Data[0]);
		if($line=='0'){ //prevent no data in non academicAchievemnts 
			$x .= "<tr>";
				$x .= "<td valign='top' height='50px' class='spacing_left_only'>";
					$x .= "--";
				$x .= "</td>";
			$x .= "</tr>";
		}else{
// 			for($i=0; $i<$line; $i++){
// 				$x .= "<tr>";
// 				for($j=0; $j<$numOfColumn; $j++){
					
// 					$x .= "<td valign='top' class='text11pt' width='$width%'>";
// 						$x .= $Data[$j][$i];
// 					$x .= "</td>";
// 				}
// 				$x .= "</tr>";
// 			}
			$x .= "<tr>";
			for($j=0; $j<$numOfColumn; $j++){
				$x .= "<td valign = 'top'>";
					$x .= "<table cellspacing='0' cellpadding='0'>";
						for($i=0; $i<$line; $i++){
							$x .= "<tr>";
								$x .= "<td>";
									$x .=  $Data[$j][$i];
								$x .= "</td>";
							$x .= "</tr>";
						}
					$x .= "</table>";
				$x .= "</td>";
			}
			$x .= "</tr>";
		}
		$x .= "</table>";
		
		return $x;
	}
	
	function SignatureTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		if ($StudentID == '')
		{
			$TutorNameDisplay = 'Mr. xxx';
			$numOfTutor = 1;
		}
		else
		{
			include_once($PATH_WRT_ROOT."includes/libclass.php");
			$lclass = new libclass();
			$TutorNameArr =  $lclass->returnClassTeacher($this->Get_Student_ClassName($StudentID), $this->schoolYearID);
			$TutorNameDisplay = implode("<br />", Get_Array_By_Key($TutorNameArr, "CTeacher"));
			$numOfTutor = count($TutorNameArr);
		}
		
		$TutorSignatureTitle = ($numOfTutor > 1)? $eReportCard['Template']['SignatureEn'] : $eReportCard['Template']['SignatureEn'];
		
		$x = '';
		# Tutor's Name
		$x .= "<table class='border_bottom border_left border_right' width='100%' border='0' cellpadding='2' cellspacing='0' align='center' style='border-collapse:collapse'>";
			$x .= "<tr>";
				$x .= "<td class='text11pt spacing' width='15%' height='43'><b>".$eReportCard['Template']['NamesOfClassTeacher']."</b></td>";
				$x .= "<td class='text11pt' width='1'>:</td>";
				$x .= "<td class='text11pt' width='25%'>".$TutorNameDisplay."</td>";
				$x .= "<td class='text11pt border_left spacing' width='12%'><b>".$TutorSignatureTitle."</b></td>";
				$x .= "<td class='text11pt'>:</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		
		# Principal's Name
		$x .= "<table class='border_bottom border_left border_right' width='100%' border='0' cellpadding='2' cellspacing='0' align='center' style='border-collapse:collapse'>";
			$x .= "<tr>";
				$x .= "<td class='text11pt border_left spacing' width='15%' height='43'><b>".$eReportCard['Template']['PrincipalNameEn']."</b></td>";
				$x .= "<td class='text11pt' width='1'>:</td>";
				$x .= "<td class='text11pt' width='25%'>".$eReportCard['Template']['PrincipalName']."</td>";
				$x .= "<td class='text11pt border_left spacing' width='12%'><b> ".$eReportCard['Template']['SignatureEn']." </b></td>";
				$x .= "<td class='text11pt border_right' >:</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	
	function SubjectResultInfo($ReportID, $StudentID='', $SubjectID='', $FromMarksheet=false)
	{
		global $image_path, $LAYOUT_SKIN, $eReportCard, $PATH_WRT_ROOT, $i_general_Teacher;
		
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
		$lreportcard_ui = new libreportcard_ui();
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		$tickImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/ereportcard/icon_present_bw.gif' width='10'>";
		$titleHeight = 18;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $ReportSetting['ClassLevelID'];
		$SemID =  $ReportSetting['Semester'];
		$Semester = $this->returnSemesters($SemID);
		$ISSemester = $this->Get_Semester_Seq_Number($SemID);
		$latestSem = $this->Get_Last_Semester_Of_Report($ReportID);
		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
// 		$isForm6 = $this->isFrom6($ClassLevelID);
		
		### Get Subject Data
		if ($SubjectID != '')
		{
			$SubjectNameDisplay = strtoupper($this->GET_SUBJECT_NAME_LANG($SubjectID, 'EN'));
			$SubjectNameDisplayCh = strtoupper($this->GET_SUBJECT_NAME_LANG($SubjectID, 'CH'));
			$SubjectGroupID = $this->Get_Student_Studying_Subject_Group($latestSem, $StudentID, $SubjectID);
			
			// check if modular study subject (only applicable to S3 templates)
			$achievementGrade = '';
			if ($FormNumber == 3 || $FormNumber == 0)
			{
			    $isModularStudySubject = $this->isModularStudySubject($SubjectID);
				if ($isModularStudySubject)
				{
					$ParentSubjectMarkArr = array();
					if ($FromMarksheet) {
						// Build the getMarks array from MS
						$ParentSubjectMarkArr = $this->getMarksFromMarksheet($ReportID, $SubjectID);
						$ParentSubjectMarkArr = $ParentSubjectMarkArr[$StudentID];
					}
					else {
						$ParentSubjectMarkArr = $this->getMarks($ReportID, $StudentID, '', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID, 0, $CheckPositionDisplay=0, $SubOrderArr='');
					}
					
					$achievementGrade = $ParentSubjectMarkArr[$SubjectID][0]['Grade'];
				}
			}
			else
            {
				$isModularStudySubject = false;
			}
			
			// [2019-0111-1652-08289]
			// check if case subject (only applicable to S1 & S2 templates)
			if ($FormNumber == 1 || $FormNumber == 2 || $FormNumber == 0)
			{
    			$isCaseSubject = $this->isCaseSubject($SubjectID);
    			if ($isCaseSubject)
    			{
    		        $ParentSubjectMarkArr = array();
    		        if ($FromMarksheet) {
    		            // Build the getMarks array from MS
    		            $ParentSubjectMarkArr = $this->getMarksFromMarksheet($ReportID, $SubjectID);
    		            $ParentSubjectMarkArr = $ParentSubjectMarkArr[$StudentID];
    		        }
    		        else {
    		            $ParentSubjectMarkArr = $this->getMarks($ReportID, $StudentID, '', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID, 0, $CheckPositionDisplay=0, $SubOrderArr='');
    		        }
    		        
    		        $achievementGrade = $ParentSubjectMarkArr[$SubjectID][0]['Grade'];
    			}
			}
			else
            {
			    $isCaseSubject = false;
			}
			
			# Subject Weight Info
			$SubjectColumnWeightArr = array();
			$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID);
			$ReportWeightInfo = BuildMultiKeyAssoc($ReportWeightInfo, array('SubjectID', 'ReportColumnID'));
			foreach ((array)$ReportWeightInfo as $thisSubjectID => $thisSubjectWeightInfoArr) {
				foreach ((array)$thisSubjectWeightInfoArr as $thisReportColumnID => $thisReportColumnWeightInfoArr) {
					if ($thisReportColumnID == '') {
						$ReportWeightInfo[$thisSubjectID][0] = $thisReportColumnWeightInfoArr;
					}
				}
			}
			
			# Get Subject Teacher
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$ObjSubjectGroup = new subject_term_class($SubjectGroupID, true);
			$SubjectTeacherArr = $ObjSubjectGroup->ClassTeacherList;
			$SubjectTeacherNameArr = Get_Array_By_Key($SubjectTeacherArr, 'TeacherName');
			$SubjectTeacherNameDisplay = implode('<br />', $SubjectTeacherNameArr);
			$SubjectTeacherNameArrCh = Get_Array_By_Key($SubjectTeacherArr, 'ChineseName');
			$SubjectTeacherNameDisplayCh = implode('<br />', $SubjectTeacherNameArrCh);

			# Get Subject Display Language
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
			$LangDisplay = $SubjectFormGradingSettings['LangDisplay'];

			# Get Scheme Grade
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
			$SchemeGradeArr = Get_Array_By_Key($SchemeRangeInfo, 'Grade');
			$numOfSchemeGrade = count($SchemeGradeArr);
			
			# Get Subject Teacher Comment
			$SubjectTeacherCommentArr = $this->returnSubjectTeacherComment($ReportID, $StudentID);
			$thisSubjectTeacherComment = $SubjectTeacherCommentArr[$SubjectID];
			if ($thisSubjectTeacherComment == '') {
				$thisSubjectTeacherComment = '&nbsp;';
			}
			
			# Get Subject Curriculum Expectation
			$SubjectCurriculumExpectationArr = $this->returnCurriculumExpectation($ClassLevelID, $SubjectID, $SemID);
			$thisSubjectCurriculumExpectation = $SubjectCurriculumExpectationArr[$SubjectID];
			if ($thisSubjectCurriculumExpectation == '') {
			    $thisSubjectCurriculumExpectation = '&nbsp;';
			}
			
			$colspan = $numOfSchemeGrade + 1;
		}
		else
		{
			$SchemeGradeArr = array('5**', '5*', '5', '4', '3', '2','1');
			$numOfSchemeGrade = count($SchemeGradeArr);
			
			$thisSubjectTeacherComment = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
			$thisSubjectCurriculumExpectation = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
		}
        // [2020-0203-1139-58206]
        $subjectAlwaysDisplayYearFormat = false;
		
		### Get Attitude Data
		# $pcData = array("CharID", "Title_EN", "Title_CH"...)
		$pcData = $this->returnPersonalCharSettingData($ClassLevelID, $SubjectID);
		$numOfCriteria = count($pcData);
		
		# $Scale
		// $ScalePointArr = $this->PersonalCharGradeArr;
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
		$lreportcard_pc = new libreportcard_pc();
		$scaleInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale();
		foreach($scaleInfoArr as $_scaleInfoArr){
			$ScalePointArr[$_scaleInfoArr['ScaleID']]['NameEn'] = $_scaleInfoArr['NameEn'];
			$ScalePointArr[$_scaleInfoArr['ScaleID']]['NameCh'] = $_scaleInfoArr['NameCh'];
		}
// 		$ScalePointArr = $scaleInfoArr;
// 		$TitleLang = Get_Lang_Selection("NameCh","NameEn");
// 		$ScalePointArrEn = BuildMultiKeyAssoc($scaleInfoArr, "ScaleID", "NameEn", 1); 
// 		$ScalePointArrCh = BuildMultiKeyAssoc($scaleInfoArr, "ScaleID", "NameCh", 1);

		# $TempResult = array("CharData")
		$TempResult = $this->getPersonalCharacteristicsData($StudentID, $ReportID, $SubjectID);

		# Build data array
		$result = $TempResult[0][0];
		$resultRow = explode(",", $result);
		# $resultAry[#CharID] = #OptionPoint

		$PersonalCharArr = array();
		foreach($resultRow as $key=>$data)
		{
			list($PCID, $PCPoint) = explode(":", $data);
			$PersonalCharArr[$PCID] = $PCPoint;
		}

        # Subject Code Mapping
		$SubjectCodeMapping = $this->SubjectCodeMapping;

		### Build Table
		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_left border_top border_right'>";
			# Subject Name
			$x .= "<tr class='lite_grey_bg'><td colspan='".$colspan."'><b><img src='".$emptyImagePath."' height='1' align='center'></b></td></tr>";
			$x .= "<tr class='lite_grey_bg '>";
				$x .= "<td colspan='".$colspan."'>";
					$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' >";
						$x .= "<tr>";
							# Subject Name
							$ElectiveLabelEn = '';
							$ElectiveLabelCh = '';
							if(($FormNumber == '4' || $FormNumber == '5' || $FormNumber == '6') && !in_array($SubjectCodeMapping[$SubjectID], $this->NonElectiveSujectCode))
							{
								if ($LangDisplay == 'en' || $LangDisplay == ''){
									$ElectiveLabelEn = $eReportCard['Template']['ElectiveEn'].' - ';
								}
								else if ($LangDisplay == 'ch'){
									$ElectiveLabelCh = $eReportCard['Template']['ElectiveCh'].' - ';
								}
								else{
									$ElectiveLabelEn = $eReportCard['Template']['ElectiveEn'].' - ';
									$ElectiveLabelCh = $eReportCard['Template']['ElectiveCh'].' - ';
								}
							}
							if ($LangDisplay == 'en' || $LangDisplay == ''){
								$SubjecName = $ElectiveLabelEn.$SubjectNameDisplay;
							}
							else if ($LangDisplay == 'ch'){
								$SubjecName = $ElectiveLabelCh.$SubjectNameDisplayCh;
							}
							else{
								$Seperator = (in_array($SubjectCodeMapping[$SubjectID], $this->ElectiveDisplayNLSubjectCode))? "<br/>" : "&nbsp";
								$SubjecName = $ElectiveLabelEn.$SubjectNameDisplay.$Seperator.$ElectiveLabelCh.$SubjectNameDisplayCh;
							}
							$x .= "<td class='text13pt spacing' width='55%'><b>".$SubjecName."</b></td>";
							
							# Teacher Title
							if ($LangDisplay == 'en' || $LangDisplay == ''){
								$TeacherTitle = $eReportCard['Template']['TeacherNameEn'];
							}
							else if ($LangDisplay == 'ch'){
								$TeacherTitle = $eReportCard['Template']['TeacherNameCh'];
							}
							else{
								$TeacherTitle = $eReportCard['Template']['TeacherNameEn'].'&nbsp'.$eReportCard['Template']['TeacherNameCh'];
							}
							if ($LangDisplay == 'en' || $LangDisplay == ''){
								$TeacherNameDisplay = $SubjectTeacherNameDisplay;
							}
							else if ($LangDisplay == 'ch'){
								$TeacherNameDisplay = $SubjectTeacherNameDisplayCh;
							}
							else{
								$TeacherNameDisplay = $SubjectTeacherNameDisplay.'&nbsp'.$SubjectTeacherNameDisplayCh;
							}
							$x .= "<td class='text11pt' width='15%'><b>".$TeacherTitle.":</b></td>";
							$x .= "<td class='text11pt' width='30%'>".$TeacherNameDisplay."</td>";
						$x .= "</tr>";
					$x .= "</table>";
				$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td  class='border_top' colspan='$colspan' height='8'>";
					$x .= " ";
				$x .= "</td>";
			$x .= "</tr>";
// 			$x .= "<tr class='lite_grey_bg'><td colspan='".$colspan."'><b><img src='".$emptyImagePath."' height='1' align='center'></b></td></tr>";

		if (!$isModularStudySubject && !$isCaseSubject)
		{
			### Achievement Title
			if ($LangDisplay == 'en' || $LangDisplay == ''){
				$AttitudeTitle = $eReportCard['Template']['AttitudeEn'];
			}
			else if ($LangDisplay == 'ch'){
				$AttitudeTitle = $eReportCard['Template']['AttitudeCh'];
			}
			else{
				$AttitudeTitle = $eReportCard['Template']['AttitudeEn'].'&nbsp'.$eReportCard['Template']['AttitudeCh'];
			}
			
			$x .= "<tr class='lite_grey_bg' height='".$titleHeight."'>";
			if ($LangDisplay == 'en' || $LangDisplay == ''){
				$AchievementTitle = $eReportCard['Template']['AchievementEn'];
			}
			else if ($LangDisplay == 'ch'){
				$AchievementTitle = $eReportCard['Template']['AchievementCh'];
			}
			else{
				$AchievementTitle = $eReportCard['Template']['AchievementEn'].'&nbsp'.$eReportCard['Template']['AchievementCh'];
			}
			$x .= "<td class='text11pt border_top spacing' width='65%'><b>".$AchievementTitle." (%)"."</b></td>";

			# Scheme Grade Title
			if($numOfSchemeGrade > 0){
				$widthOfGrade = 35/$numOfSchemeGrade;
				$widthOfGrade = $widthOfGrade.'%';
			}
			for ($i=0; $i<$numOfSchemeGrade; $i++) {
                $x .= "<td class='text11pt border_left border_top'  width = '$widthOfGrade' align='center'><b>".$SchemeGradeArr[$i]."</b></td>";
            }
            $x .= "</tr>";

            # Component Subject Name and Grade Image
            $ComponentSubjectEnArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, 'en', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
            $ComponentSubjectChArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, 'ch', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
            $numOfComponent = count($ComponentSubjectEnArr);

            $MarkArr = array();
            if ($FromMarksheet) {
                // Build the getMarks array from MS
                $MarkArr = $this->getMarksFromMarksheet($ReportID, $SubjectID);
                $MarkArr = $MarkArr[$StudentID];
            }
            else {
                $MarkArr = $this->getMarks($ReportID, $StudentID, '', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', '', 0, $CheckPositionDisplay=0, $SubOrderArr='');
            }

            # EG / YG / CA Checking
            $TermGrade = '';
            $ExaminationGrade = '';
            $YearGrade = '';
            $numOfEGCGYG = 0;
            for ($i=0; $i<$numOfComponent; $i++)
            {
                $thisCmpSubjectNameEn = $ComponentSubjectEnArr[$i]['SubjectName'];
                if($thisCmpSubjectNameEn=='Examination' ){
                    $numOfEGCGYG++;
                }
                if($thisCmpSubjectNameEn=='Continuous Assessment'){
                    $numOfEGCGYG++;
                }
                if( $thisCmpSubjectNameEn=='Year Grade'){
                    $numOfEGCGYG++;
                }
            }
            for ($i=0; $i<$numOfComponent; $i++)
            {
                $thisCmpSubjectID = $ComponentSubjectEnArr[$i]['SubjectID'];
                $thisCmpSubjectNameEn = $ComponentSubjectEnArr[$i]['SubjectName'];
                $thisCmpSubjectNameCh = $ComponentSubjectChArr[$i]['SubjectName'];
                $thisNameSpanEn = '<span class="en_font">'.$thisCmpSubjectNameEn.'</span>';
                $thisNameSpanCh = '<span class="ch_font">'.$thisCmpSubjectNameCh.'</span>';

                if ($LangDisplay == 'en' || $LangDisplay == ''){
                    $thisCmpNameDisplay = $thisNameSpanEn;
                }
                else if ($LangDisplay == 'ch'){
                    $thisCmpNameDisplay = $thisNameSpanCh;
                }
                else{
                    $thisCmpNameDisplay = $thisNameSpanEn.'&nbsp;'.$thisNameSpanCh;
                    // $thisCmpNameDisplay = $thisNameSpanCh.' ('.$thisNameSpanEn.')';
                }

                $thisSubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisCmpSubjectID,0,0,$ReportID);
                $thisSchemeID = $thisSubjectFormGradingSettings['SchemeID'];
                $SubjectOverallWeight = $ReportWeightInfo[$thisCmpSubjectID][0]['Weight'];
                // debug_pr($SubjectOverallWeight);

                # Get Student Grade
                $thisGrade = $MarkArr[$thisCmpSubjectID][0]['Grade'];
                if ($thisGrade == '')
                {
                    // no grade => convert from mark
                    $thisMark = $MarkArr[$thisCmpSubjectID][0]["RawMark"];
                    $thisGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark, $ReportID, $StudentID, $thisCmpSubjectID, $ClassLevelID, 0);
                }

                // 				if ($i >= $numOfComponent - 3)
                    // 				{
                    // 					list($thisGradeDisplay, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisGrade, $thisGrade, $checkManualAdjust=1);
                    // 					// Last 3 components are Term Grade, Examination Grade, and Year Grade
                    // 					### 20100818 eRC - 港大同學會書院 change wordings - swap position of Term Grade and Exam Grade
                    // 					if ($i == $numOfComponent - 2)
                        // 					{
                        // 						//$TermGrade = $thisGrade;
                        // 						$CAGrade = $thisGradeDisplay;
                        // 					}
                    // 					else if ($i == $numOfComponent - 3)
                        // 					{
                        // 						$ExaminationGrade = $thisGradeDisplay;
                        // 					}
                    // 					else if ($i == $numOfComponent - 1)
                        // 					{
                        // 						$YearGrade = $thisGradeDisplay;
                        // 					}
                    // 				}

                if ($i >= $numOfComponent - $numOfEGCGYG)
                {
                    list($thisGradeDisplay, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisGrade, $thisGrade, $checkManualAdjust=1);

                    // Last 3 components are Term Grade, Examination Grade, and Year Grade
                    ### 20100818 eRC - 港大同學會書院 change wordings - swap position of Term Grade and Exam Grade
                    if($thisCmpSubjectNameEn == 'Continuous Assessment') {
                        $CAGrade = $thisGradeDisplay;
                    }
                    if($thisCmpSubjectNameEn == 'Examination') {
                        $ExaminationGrade = $thisGradeDisplay;
                    }
                    if($thisCmpSubjectNameEn == 'Year Grade') {
                        $YearGrade = $thisGradeDisplay;
                    }
                }
                else
                {
                    // Subject Component
                    //if ($thisGrade == 'N.A.' || $thisGrade == '') {
                    if ($SubjectOverallWeight == 0 || $thisGrade == '') {
                        continue;
                    }

                    // $css_border_top = ($i==0)? 'border_top' : '';
                    $css_border_top = 'border_top';
                    $weightDisplay = $SubjectOverallWeight*100;

                    # Display Cmp Name
                    $x .= "<tr>";
                    $x .= "<td class='text11pt spacing_2 ".$css_border_top."'>".$thisCmpNameDisplay." "."(".$weightDisplay."%)"."</td>";

                    # Scheme Grade Title
                    for ($j=0; $j<$numOfSchemeGrade; $j++)
                    {
                        $thisColumnGrade = $SchemeGradeArr[$j];
                        // $thisGradeDisplay = ($thisGrade == $thisColumnGrade)? $tickImg : "&nbsp;";
                        $thisGradeDisplay = ($thisGrade == $thisColumnGrade)? "<span style=\"font-family:'Wingdings 2'\">&#80;</span>" : "&nbsp;";
                        $x .= "<td class='text14px border_left border_top' align='center' valign='middle'>".$thisGradeDisplay."</td>";
                    }
                    $x .= "</tr>";
                }
            }
            $x .= "<tr height='8'>";
                $x .= "<td class='border_top' colspan=$colspan>";
                $x .= " ";
                $x .= "</td>";
            $x .= "</tr>";
            $x .= "</table>";

            # Skip Attitude Subjects from display
            // [2020-0203-1139-58206]
            // (ori. request) S1 2019-2020 Drama Education
            // (updated request) apply to all Drama Education
            $skipAttitudeDisplay = false;
            //if($SubjectID != '' && $SubjectCodeMapping[$SubjectID] == '128' && $this->schoolYearID == 11 && $FormNumber == '1') {
            if($SubjectID != '' && $SubjectCodeMapping[$SubjectID] == '128') {
                $skipAttitudeDisplay = true;
                $subjectAlwaysDisplayYearFormat = true;
            }

            ### Attitude Title
            // Term 1 S1-5
            // [2020-0203-1139-58206] excluded Drama Education
            //if($ISSemester == '1' && !($FormNumber == '6')
            if($ISSemester == '1' && !($FormNumber == '6') && !$skipAttitudeDisplay)
            {
                # Attitude Title
                if ($LangDisplay == 'en' || $LangDisplay == ''){
                    $AttitudeTitle = $eReportCard['Template']['AttitudeEn'];
                }
                else if ($LangDisplay == 'ch'){
                    $AttitudeTitle = $eReportCard['Template']['AttitudeCh'];
                }
                else{
                    $AttitudeTitle = $eReportCard['Template']['AttitudeEn'].'&nbsp'.$eReportCard['Template']['AttitudeCh'];
                }

                $x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_left  border_right'>";
                $x .= "<tr class='lite_grey_bg'  height='".$titleHeight."'>";
                $x .= "<td class='text15px border_top spacing' width='65%'><b>".$AttitudeTitle."</b></td>"; // 65% -> client site shift

                # Scheme Grade Title
                $widthScale = 35/count($ScalePointArr);
                foreach ($ScalePointArr as $point => $title)
                {
                    if ($LangDisplay == 'en' || $LangDisplay == ''){
                        $ScalePointTitle = $title['NameEn'];
                    }
                    else if ($LangDisplay == 'ch'){
                        $ScalePointTitle =  $title['NameCh'];
                    }
                    else{
                        $ScalePointTitle =  $title['NameEn'].'</br>'. $title['NameCh'];
                    }
                    $x .= "<td class='text11px border_left border_top' align='center' width='$widthScale"."%"."'><b>".$ScalePointTitle."</b></td>";
                    // $x .= "<td class='text11px border_left border_top' align='center' width='$widthScale"."%"."'><b>".$title."</b></td>";
                }
                $x .= "</tr>";

                # Display Personal Char Result
                for ($i=0; $i<$numOfCriteria; $i++)
                {
                    $thisCharID = $pcData[$i]['CharID'];
                    $thisPersonalChar = $pcData[$i]['Title_EN'];
                    $thisPersonalChar_CH = $pcData[$i]['Title_CH'];
                    if ($LangDisplay == 'en' || $LangDisplay == ''){
                        $thisPersonalChar_Display = $thisPersonalChar;
                    }
                    else if ($LangDisplay == 'ch'){
                        $thisPersonalChar_Display =  $thisPersonalChar_CH;
                    }
                    else{
                        $thisPersonalChar_Display =  $thisPersonalChar.'&nbsp;'. $thisPersonalChar_CH;
                    }

                    // $this_border_top = ($i==0)? 'border_top' : '';
                    $this_border_top = 'border_top';

                    $x .= "<tr>";
                    $x .= "<td class='text14px spacing_2 ".$this_border_top."'>"."<span style='font-family:Wingdings'>&#118;</span>"."&nbsp;".$thisPersonalChar_Display."</td>";

                    # Scheme Grade Title
                    foreach ($ScalePointArr as $optionPoint => $optionTitle)
                    {
                        // $thisData = ($PersonalCharArr[$thisCharID] == $optionPoint)? $tickImg : "&nbsp;";
                        $thisData = ($PersonalCharArr[$thisCharID] == $optionPoint)? "<span style=\"font-family:'Wingdings 2'\">&#80;</span>" : "&nbsp;";
                        $x .= "<td class='text14px border_left border_top' align='center' valign='middle'>".$thisData."</td>";
                    }
                    $x .= "</tr>";
                }
                $x .= "</table>";
            }
		}

		### Comments, Term Grade, Examination Grade, Year Grade
		//if($ISSemester=='2'||$FormNumber=='6'){
		if($isCaseSubject)
		{
		    # Course Description Title
		    if ($LangDisplay == 'en' || $LangDisplay == '') {
		        $CourseDescriptionTitle = $eReportCard['Template']['CourseDescriptionEn'];
		    }
		    else if ($LangDisplay == 'ch') {
		        $CourseDescriptionTitle = $eReportCard['Template']['CourseDescriptionCh'];
		    }
		    else {
		        $CourseDescriptionTitle = $eReportCard['Template']['CourseDescriptionEn'].'&nbsp'.$eReportCard['Template']['CourseDescriptionCh'];
		    }
		    
		    $x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_top border_left border_right'>";
    		    $x .= "<tr class='lite_grey_bg text15px spacing' height='".$titleHeight."'>";
        		    $x .= "<td class='spacing'><b>".$CourseDescriptionTitle.":</b></td>";
                $x .= "</tr>";
                
                ### Course Description
                $x .= "<tr class='text15px subject_teacher_comment' height='95'>";
                    $x .= "<td class='border_top spacing_subjectComment'>".$thisSubjectCurriculumExpectation."</td>";
                $x .= "</tr>";
            $x .= "</table>";
		}
        // [2020-0203-1139-58206] Drama Education always show comment box
        //else if($ISSemester == '2' || $FormNumber == '6' || $isModularStudySubject)
		else if($ISSemester == '2' || $FormNumber == '6' || $isModularStudySubject || $subjectAlwaysDisplayYearFormat)
		{
			$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='border_top border_left border_right'>";
				$x .= "<tr class='lite_grey_bg text15px spacing' height='".$titleHeight."'>";

				# Subject Teacher Comment Title
                if ($LangDisplay == 'en' || $LangDisplay == ''){
                    $SubjectTeacherCommentTitle = $eReportCard['Template']['SubjectTeacherCommentsEn'];
                }
                else if ($LangDisplay == 'ch'){
                    $SubjectTeacherCommentTitle = $eReportCard['Template']['SubjectTeacherCommentsCh'];
                }
                else{
                    $SubjectTeacherCommentTitle = $eReportCard['Template']['SubjectTeacherCommentsEn'].'&nbsp'.$eReportCard['Template']['SubjectTeacherCommentsCh'];
                }
                $x .= "<td class='spacing'><b>".$SubjectTeacherCommentTitle.":</b></td>";
				$x .= "</tr>";

				### Subject Teacher Comment
				$x .= "<tr class='text15px subject_teacher_comment' height='95'>";
					$x .= "<td  class='border_top spacing_subjectComment'>".$thisSubjectTeacherComment."</td>";
				$x .= "</tr>";

			$x .= "</table>";
		}
		
		### CA Grade, Year Grade, Examination Grade START###
		### Term 1 ###
        // [2020-0203-1139-58206] excluded Drama Education
        //if($ISSemester == '1'&& !($FormNumber == '6'))
		if($ISSemester == '1'&& !($FormNumber == '6') && !$subjectAlwaysDisplayYearFormat)
		{
			$x .= "<table class='report_border' width='100%' border='0' cellpadding='2' cellspacing='0' >";
				$x .= '<tr>';
					$printGrade = false;
					$border_left = '';

					## CA Grade
					if($CAGrade!='N.A.' && !empty($CAGrade))
					{
						$x .= '<td class="text15px lite_grey_bg spacing_right '.$border_left.'" width="35%" align="right">';
							if($CAGrade == 'ABS'){
								$CAGrade = 'N.A.';
							}

							# CA Title
							if ($LangDisplay == 'en' || $LangDisplay == ''){
								$CATitle = $eReportCard['Template']['CAGradeEn'];
							}
							else if ($LangDisplay == 'ch'){
								$CATitle = $eReportCard['Template']['CAGradeCh'];
							}
							else{
								$CATitle = $eReportCard['Template']['CAGradeEn'].'&nbsp'.$eReportCard['Template']['CAGradeCh'];
							}
							$x .= '<b>'.$CATitle.': '.$CAGrade.'</b>';
						$x .= '</td>';
						$border_left = "border_left";
						$printGrade = true;
					}

					## Examination Grade
					if($ExaminationGrade!="N.A."&&!empty($ExaminationGrade)){
						$x .='<td class="text15px lite_grey_bg spacing_right '.$border_left.'" width="35%" align="right">';
						if($ExaminationGrade == 'ABS'){
							$ExaminationGrade = 'N.A.';
						}

						# Examination Title
						if ($LangDisplay == 'en' || $LangDisplay == ''){
							$ExmainationTitle = $eReportCard['Template']['ExaminationGradeEn'];
						}
						else if ($LangDisplay == 'ch'){
							$ExmainationTitle = $eReportCard['Template']['ExaminationGradeCh'];
						}
						else{
							$ExmainationTitle = $eReportCard['Template']['ExaminationGradeEn'].'&nbsp'.$eReportCard['Template']['ExaminationGradeCh'];
						}
						$x .= '<b>'.$ExmainationTitle.': '.$ExaminationGrade.'</b>';

						$x .='</td>';
						$printGrade = true;
					}
					else if ($isModularStudySubject || $isCaseSubject)
					{
						$x .='<td class="text15px lite_grey_bg spacing_right '.$border_left.'" width="35%" align="right">';
						if($achievementGrade == 'ABS') {
							$achievementGrade = 'N.A.';
						}

						# Achievement Title
						if ($LangDisplay == 'en' || $LangDisplay == '') {
							$achievementTitle = $eReportCard['Template']['AchievementShortEn'];
						} else if ($LangDisplay == 'ch') {
							$achievementTitle = $eReportCard['Template']['AchievementShortCh'];
						} else {
							$achievementTitle = $eReportCard['Template']['AchievementShortEn'].'&nbsp'.$eReportCard['Template']['AchievementShortCh'];
						}
						$x .= '<b>'.$achievementTitle.': '.$achievementGrade.'</b>';
						$x .='</td>';
						$printGrade = true;
					}
					else
                    {
						if(!$printGrade)
						{
							$x .='<td class="text15px lite_grey_bg spacing_right '.$border_left.'" width="35%" align="right">';
							$x .= "&nbsp;";
							$x .='</td>';
						}
					}

				$x .= '</tr>';
			$x .= '</table>';
		}

		### Term 2 ###
        // [2020-0203-1139-58206] Drama Education always show CA Grade, Year Grade, Examination Grade
        //if($ISSemester == '2' || $FormNumber == '6')
		if($ISSemester == '2' || $FormNumber == '6' || $subjectAlwaysDisplayYearFormat)
		{
			$x .= "<table class='report_border' width='100%' border='0' cellpadding='2' cellspacing='0'  >";
				$x .= '<tr>';

					# CA Grade
					$printGrade = false;
					$border_left = '';
					if($CAGrade != 'N.A.' && !empty($CAGrade))
					{
						$x .= '<td class="text11pt lite_grey_bg spacing_right '.$border_left.'" align="right">';
						if($CAGrade == 'ABS'){
							$CAGrade = 'N.A.';
						}

						# CA Title
						if ($LangDisplay == 'en' || $LangDisplay == ''){
							$CATitle = $eReportCard['Template']['CAGradeEn'];
						}
						else if ($LangDisplay == 'ch'){
							$CATitle = $eReportCard['Template']['CAGradeCh'];
						}
						else{
							$CATitle = $eReportCard['Template']['CAGradeEn'].'&nbsp'.$eReportCard['Template']['CAGradeCh'];
						}
						$x .= '<b>'.$CATitle.': '.$CAGrade.'</b>';
						$x .= '</td>';
						$border_left = "border_left";
						$printGrade = true;
					}
					
					# Examination Grade
					if($ExaminationGrade != 'N.A.' && !empty($ExaminationGrade))
					{
						$x .='<td class="text11pt lite_grey_bg spacing_right '.$border_left.'"  width="28%" align="right">';
						if($ExaminationGrade == 'ABS'){
							$ExaminationGrade = 'N.A.';
						}

						if ($LangDisplay == 'en' || $LangDisplay == ''){
							$ExmainationTitle = $eReportCard['Template']['ExaminationGradeEn'];
						}
						else if ($LangDisplay == 'ch'){
							$ExmainationTitle = $eReportCard['Template']['ExaminationGradeCh'];
						}
						else{
							$ExmainationTitle = $eReportCard['Template']['ExaminationGradeEn'].'&nbsp'.$eReportCard['Template']['ExaminationGradeCh'];
						}
						$x .= '<b>'.$ExmainationTitle.': '.$ExaminationGrade.'</b>';
						$x .='</td>';
						$border_left = "border_left";
						$printGrade = true;
					}
					
					# Year Grade
					if($YearGrade != 'N.A.'&& !empty($YearGrade))
					{
						$x .='<td class="text11pt lite_grey_bg spacing_right '.$border_left.'" width="28%" align="right">';
						if($YearGrade == 'ABS'){
							$YearGrade = 'N.A.';
						}

						if ($LangDisplay == 'en' || $LangDisplay == ''){
							$YearGradeTitle = $eReportCard['Template']['YearGradeEn'];
						}
						else if ($LangDisplay == 'ch'){
							$YearGradeTitle = $eReportCard['Template']['YearGradeCh'];
						}
						else{
							$YearGradeTitle = $eReportCard['Template']['YearGradeEn'].'&nbsp'.$eReportCard['Template']['YearGradeCh'];
						}
						$x .= '<b>'.$YearGradeTitle.': '.$YearGrade.'</b>';		
						$x .='</td>';
						$printGrade = true;
					}
					else if ($isModularStudySubject || $isCaseSubject)
					{
						$x .='<td class="text15px lite_grey_bg spacing_right '.$border_left.'" width="35%" align="right">';
						if($achievementGrade == 'ABS') {
							$achievementGrade = 'N.A.';
						}

						# Achievement Title
						if ($LangDisplay == 'en' || $LangDisplay == '') {
							$achievementTitle = $eReportCard['Template']['AchievementShortEn'];
						}
						else if ($LangDisplay == 'ch') {
							$achievementTitle = $eReportCard['Template']['AchievementShortCh'];
						}
						else {
							$achievementTitle = $eReportCard['Template']['AchievementShortEn'].'&nbsp'.$eReportCard['Template']['AchievementShortCh'];
						}
						$x .= '<b>'.$achievementTitle.': '.$achievementGrade.'</b>';
						$x .='</td>';
						$printGrade = true;
					}
					else
                    {
						if(!$printGrade)
						{
							$x .='<td class="text15px lite_grey_bg spacing_right '.$border_left.'" width="35%" align="right">';
							$x .= "&nbsp;";
							$x .='</td>';
						}
					}
				$x .= '</tr>';
			$x .= '</table>';
		}

		# Remarks
        // [2020-0203-1139-58206] Drama Education hide remarks
		//if($ISSemester=='1'&&!($FormNumber=='6')){
        //if(!$isModularStudySubject && !$isCaseSubject && $ISSemester == '1' && !($FormNumber == '6'))
		if(!$isModularStudySubject && !$isCaseSubject && $ISSemester == '1' && !($FormNumber == '6') && !$subjectAlwaysDisplayYearFormat)
		{
			if($thisSubjectTeacherComment != '&nbsp;')
			{
				$x .= "<table>";
				$x .= "<tr class='text13px spacing' >";
				$x .= "<td class=''>"."<b>Remarks:"."&nbsp;".$thisSubjectTeacherComment."</b></td>";
				$x .= "</tr>";
				$x .= "</table>";
			}
		}
		### CA Grade, Year Grade, Examination Grade END###
		
// 		$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='report_border'>";
// 			$x .= "<tr class='lite_grey_bg text15px' height='".$titleHeight."'>";
// 				# Comment Title
// 				$x .= "<td width='22%'><b>".$eReportCard['Template']['CommentsEn'].":</b></td>";
				
// 				# Examination Grade
// 				$x .= "<td class='border_left'>";
// 					$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
// 						$x .= "<tr class='text15px'>";
// 							$x .= "<td width='65%'><b>".$eReportCard['Template']['ExaminationGradeEn'].":</b></td>";
// 							$x .= "<td align='center' width='35%'><b>".$ExaminationGrade."</b></td>";
// 						$x .= "</tr>";
// 					$x .= "</table>";
// 				$x .= "</td>";
				
// 				# Term Grade
// 				$x .= "<td class='border_left' width='22%'>";
// 					$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
// 						$x .= "<tr class='text15px'>";
// 							$x .= "<td width='65%'><b>".$eReportCard['Template']['CAGradeEn'].":</b></td>";
// 							$x .= "<td align='center' width='35%'><b>".$CAGrade."</b></td>";
// 						$x .= "</tr>";
// 					$x .= "</table>";
// 				$x .= "</td>";
				
// 				# Year Grade
// 				$x .= "<td class='border_left' width='22%'>";
// 					$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
// 						$x .= "<tr class='text15px'>";
// 							$x .= "<td width='65%'><b>".$eReportCard['Template']['YearGradeEn'].":</b></td>";
// 							$x .= "<td align='center' width='35%'><b>".$YearGrade."</b></td>";
// 						$x .= "</tr>";
// 					$x .= "</table>";
// 				$x .= "</td>";
// 			$x .= "</tr>";
			
// 			### Subject Teacher Comment
// 			$x .= "<tr class='text15px subject_teacher_comment' height='95'>";
// 				$x .= "<td colspan='4' class='border_top'>".$thisSubjectTeacherComment."</td>";
// 			$x .= "</tr>";
// 		$x .= "</table>";
		
		return $x;
	}
	
	function BlankPage($pageNumber=''){
		$x = '';
		$x .= '<table width="100%"  height="1000px">';
			$x .= '<tr>';
				$x .= '<td width="100%"  height="100%" align="center" style="font-size: 35px;">';
				$x .= '<b>'.'This page is blank'.'</b>';
				$x .= '</td>';
			$x .= '</tr>';
		$x .= '</table>';
		if($pageNumber%2==0){
			$pageNumAlign = 'right';
		}else{
			$pageNumAlign = 'left';
		}
		if($pageNumber){
			$pageNumberDisplay = "<tr valign='bottom' align='".$pageNumAlign."'>";
				$pageNumberDisplay .= "<td>";
					$pageNumberDisplay .= $pageNumber;
				$pageNumberDisplay .= "</td>";
			$pageNumberDisplay .= "</tr>";
		}
		$r = "
			<table width=\"100%\" height=\"1050px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"page-break-after:always\">
			<tr><td>". $x ."</td><tr>
					$pageNumberDisplay
			</table>
		"; 
		
		return $r;
	
	}
	
	function LastPage($pageNumber=''){
		$x = '';
		$x .= '<table class="text15px" width="100%"  height="900px" >';
			$x .= '<tr>';
				$x .= '<td height="970" align="left" valign="bottom" class="font_10pt">';
				$x .= '<i>Our school report card has been revamped in the 2016 – 2017 academic year. The assessment results have been changed from grades to levels. For details, please refer to the level descriptors on page 1 of the report card.</i>';
				$x .= '</td>';
			$x .= '</tr>';
		$x .= '</table>';
		if($pageNumber%2==0){
			$pageNumAlign = 'right';
		}else{
			$pageNumAlign = 'left';
		}
		if($pageNumber){
			$pageNumberDisplay = "<tr valign='bottom' align='".$pageNumAlign."'>";
				$pageNumberDisplay .= "<td>";
					$pageNumberDisplay .= $pageNumber;
				$pageNumberDisplay .= "</td>";
			$pageNumberDisplay .= "</tr>";
		}
		$r = "
			<table width=\"100%\" height=\"1050px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"page-break-after:always\">
				<tr><td>". $x ."</td><tr>
				$pageNumberDisplay
			</table>
		";
		
		return $r;
		
	}
	
	function isModularStudySubject($parSubjectId) {
	    global $PATH_WRT_ROOT;
	    
	    include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	    $subjectObj = new subject($parSubjectId);
	    
	    return (strpos(strtolower($subjectObj->CODEID), 's3mdu') === false)? false : true;
	}
	
	function isCaseSubject($parSubjectId) {
	    global $PATH_WRT_ROOT;
	    
	    include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	    $subjectObj = new subject($parSubjectId);
	    
	    return (strpos(strtolower($subjectObj->CODEID), 'case') === false)? false : true;
	}
}
?>