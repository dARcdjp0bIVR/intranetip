<?php
# Editing by 

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

$ReportCardCustomSchoolName = ($ReportCardCustomSchoolName == '')? 'general' : $ReportCardCustomSchoolName;
include_once($intranet_root."/lang/reportcard_custom/".$ReportCardCustomSchoolName.".".$intranet_session_language.".php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "attendance", "merit", "remark", "eca");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "---";
		
		$this->ColorBlue = '#0023fa';
	}
	
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow) {
		global $eReportCard;
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
//		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$x = "";
		$x .= "<tr><td>";
			//2014-1117-1107-37207
			$x .= "<br>";
			$x .= "<table width='718px' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				//2014-1117-1107-37207
				//$x .= "<tr height='850px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr height='885px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID)
	{
		global $eReportCard, $intranet_root, $title1, $title2, $title3;
		
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
            
			$ReportTitle =  $title1."<br>".'<span style="color:#000000">'.$title2.'</span>'."<br>".$title3;
            
			# get school badge
			//$SchoolLogo = GET_SCHOOL_BADGE();
			
			//2011-0221-1556-42073
			//$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
			//$SchoolLogo = ($imgfile != "") ? "<img src=\"/file/{$imgfile}\" width=90>\n" : "";
			//$imgfile = "/file/reportcard2008/templates/sha_tin_methodhist.gif";
			//$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" width=90>\n" : "";
			$imgfile = "/file/reportcard2008/templates/sha_tin_methodhist.png";
			$SchoolLogo = ($imgfile != "") ? "<img src=\"{$imgfile}\" height=91.344>\n" : "";
			
			# get school name
			//$SchoolName = GET_SCHOOL_NAME();
//			$SchoolName = '<b>'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</b><br />'.$eReportCard['Template']['SchoolNameCh'];
			$SchoolName = '';
			$SchoolName .= '<b>'.strtoupper($eReportCard['Template']['SchoolNameEn']).'</b>';
			$SchoolName .= $this->Get_Empty_Image_Div('4px');
			$SchoolName .= $eReportCard['Template']['SchoolNameCh'];
            
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	        
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center' height='95px'>".$TempLogo."</td>";
			
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName)) {
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' style='color:".$this->ColorBlue."'>".$SchoolName."</td></tr>\n";
					}
					if(!empty($ReportTitle)) {
						$TitleTable .= "<tr><td nowrap='nowrap' class='font_16px' align='center' valign='top' style='color:".$this->ColorBlue."'>".$ReportTitle."</td></tr>\n";
					}
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal = "XXX";
			$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			$data['DateOfIssue'] = $ReportSetting['Issued'];
			$data['DateOfIssue'] = date('d/n/Y', strtotime($data['DateOfIssue']));
			
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				//$data['Name'] = $lu->UserName2Lang('en', 2);
				$thisStudentNameEn = $StudentInfoArr[0]['EnglishName'];
				$thisStudentNameCh = $StudentInfoArr[0]['ChineseName'];
				
				// 2011-0222-1604-48096
				//$data['Name'] = $lu->UserName2Lang('en', 2);
				if (strlen($thisStudentNameEn) >= 26) {
					$data['Name'] = $thisStudentNameEn.'<br /><span class="font_chi">'.$thisStudentNameCh.'</span>';
				} else {
					$data['Name'] = $thisStudentNameEn.' <span class="font_chi">'.$thisStudentNameCh.'</span>';
				}
				
				//$data['ClassNo'] = $lu->ClassNumber;
				$data['ClassNo'] = $thisClassNumber;
				$data['StudentNo'] = $data['ClassNo'];
				//$data['Class'] = $lu->ClassName;
				$data['Class'] = $thisClassName;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = $lu->STRN;
				$data['RegNo'] = str_replace("#", "", $lu->WebSamsRegNo);
			}
			
			# hardcode the 1st six items
			$defaultInfoArray = array("Name", "Class", "RegNo", "STRN", "ClassNo", "DateOfIssue");
						
			$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' >";
			if(!empty($defaultInfoArray))
			{
				$count = 0;
				for($i=0; $i<sizeof($defaultInfoArray); $i++)
				{
					$SettingID = trim($defaultInfoArray[$i]);
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
						$dataWidth = '320px';
					} else {
						$dataWidth = '80px';
					}
					
					if($count%$StudentInfoTableCol==0) {
						$titleWidth = '65';       // first column
					} else if(($count+1)%$StudentInfoTableCol==0) {
						$titleWidth = '110';      // last column
					} else {
						$titleWidth = '70';
					}
					
					$StudentInfoTable .= "<td class='tabletext2' valign='top' height='{$LineHeight}'>";
					
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
								              <tr>
								              	<td nowrap class='font_16px' width='".$titleWidth."' style='color:".$this->ColorBlue."'>".$Title."</td>
								                <td width='2' style='color:".$this->ColorBlue."'>";
					if ($Title != "") $StudentInfoTable .= ":&nbsp;";
					$StudentInfoTable .= "</td><td width='$dataWidth' class='font_16px' nowrap>";
					$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal;
					$StudentInfoTable .= "</td></tr></table>\n";
					
					$StudentInfoTable .= "</td>";
					
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					}
					$count++;
				}
			}
			
			# Display student info selected by the user other than the 1st six items
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true && (!in_array($SettingID, $defaultInfoArray))===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					    
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						if(($count+1)%$StudentInfoTableCol==0) {
							$titleWidth = '90';      // last column
						} else {
							$titleWidth = '60';
						}
						
						$StudentInfoTable .= "<td class='tabletext2' width='33%' valign='top' height='{$LineHeight}'>";
						
						$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
									              <tr>
									              	<td nowrap class='font_16px' width='".$titleWidth."'>".$Title."</td>
									                <td width='2'>";
						if ($Title != "") $StudentInfoTable .= ":&nbsp;";
						$StudentInfoTable .= "</td><td nowrap class='font_16px' width='180'>";
						$StudentInfoTable .= $data[$SettingID] ? $data[$SettingID] : $defaultVal;
						$StudentInfoTable .= "</td></tr></table>\n";
						
						$StudentInfoTable .= "</td>";
						
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						}
						$count++;
					}
				}
			}
			
			$StudentInfoTable .= "</table>";
			$StudentInfoTable .= "<div class='StudentInfoTableSpacer'></div>";
		}
		
		return $StudentInfoTable;
	}
	
	/*
	 *	$ReportColumnIDArr[$SubjectID] = $ReportColumnID
	 *	$FullMarkArr[$SubjectID] = $FullMark
	 *	$DisplayArr[$SubjectID] = on
	 */
	function getAssessmentMSTable($ReportID, $ReportColumnIDArr, $DisplayArr, $StudentID='' )
	{
		global $eRCTemplateSetting, $eReportCard;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Subject List to be displayed
		$SubjectIDArr = (array)$DisplayArr;
		$numOfSubject = count($SubjectIDArr);
		
		### Get Subject Column
		$SubjectCol_HTML_Arr = $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		
		### Get Subject SubMS Mark
		# $MarkArr[$SubjectID][$ColumnID] = Mark
		
		$BatchMarkArr = $this->getMarks($ReportID, '', '', 0, 1, '', '', '', 1);
		$MarkArr = $BatchMarkArr[$StudentID];
		
		$html = '';
		$html .= "<table width='100%' border='0' cellspacing='0' cellpadding='4' class='report_border'>";
			$html .= "<tr>";
				$html .= "<td align='left' class='font_18px padding_1 border_bottom' style='color:".$this->ColorBlue."' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Subject']."'><b>".$eReportCard['Template']['SubjectEn']."</b></td>";
				$html .= $this->Get_Double_Line_Td("left",1,$this->ColorBlue);
				$html .= "<td align='center' class='font_16px border_left border_bottom ColHeader' style='color:".$this->ColorBlue."' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Grade']."'>".$eReportCard['Template']['Grade']."</td>";
				$html .= "<td align='center' class='font_16px border_left border_bottom ColHeader' style='color:".$this->ColorBlue."' width='".$eRCTemplateSetting['AssessmentReport']['ColumnWidth']['Position']."'>".$eReportCard['Template']['Position']."</td>";
			$html .= "</tr>";
			
			$maxRow = count($SubjectIDArr);
			for ($i=0, $printed=0; $i<$maxRow || $printed<10; $i++)
			{
				$thisSubjectID = $SubjectIDArr[$i];
				if(!empty($thisSubjectID))
				{
					$thisReportColumnID = $ReportColumnIDArr[$thisSubjectID];
					
					# Retrieve Subject Scheme ID & settings
					$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID,0 ,0, $ReportID );
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
					
					### Get the corresponding mark of the subject
					$thisMSGrade = $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
					$thisMSMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
					
					if($thisMSGrade=="N.A.") {
						continue;
					}
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					# check special case
					list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisGrade);
					
					// [2018-1121-1204-10235] Display '*' if absence
					if($thisMark == 'N.A.' && $thisMSGrade == '-') {
					    $thisMark = '*';
					}
					
					if($needStyle) {
						$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID);
					} else {
						$thisMarkDisplay = $thisMark;
					}
					$thisMarkDisplay = ($thisMarkDisplay=='')? $this->EmptySymbol : $thisMarkDisplay;
					
					# position
					$thisPosition = $MarkArr[$thisSubjectID][$thisReportColumnID]['OrderMeritForm'];
					$thisNumOfStudent = $MarkArr[$thisSubjectID][$thisReportColumnID]['FormNoOfStudent'];
					if(trim($thisPosition)>0 && $thisPosition<=$thisNumOfStudent*0.75) {
						$thisPositionDisplay = $thisPosition.'/'.$thisNumOfStudent;
					} else {
						$thisPositionDisplay = '---';
					}
				}
				else
				{
					$thisMarkDisplay = "&nbsp;";
					$thisPositionDisplay = "&nbsp;"; 
				}
				
//				$css_border_top = ($i==0)? "border_top" : "";
				$html .= "<tr>";
					$html .= $SubjectCol_HTML_Arr[$thisSubjectID];
					$html .= $this->Get_Double_Line_Td("left",1,$this->ColorBlue);
					$html .= "<td class='tabletext $css_border_top border_left font_16px padding_2' align='center'>&nbsp;".$thisMarkDisplay."</td>";
					$html .= "<td class='tabletext $css_border_top border_left font_16px padding_2' align='center'>&nbsp;".$thisPositionDisplay."</td>";
				$html .= "</tr>";
				
				$printed++;
			}
		
		$html .= "</table>";
		
		return $html; 
	} 
	
	function getAssessmentRemarkTable()
	{
		$x .= '<table border=0 cellpadding=0 cellspacing=0 class="border_left border_right border_bottom" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_Grading_Scheme_Info_Table();
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array();
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID = $SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		//$css_border_top = ($Prefix)? "" : "border_top";
			 		//$css_border_top = ($isFirst)? "border_top" : "";
			 		
			 		foreach($SubjectDisplay as $k=>$v)
			 		{
				 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
		 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
		 				
		 				$v = str_replace("SubjectEng", $SubjectEng, $v);
		 				$v = str_replace("SubjectChn", $SubjectChn, $v);
		 				
			 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
						$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$t .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext font_18px padding_2'>$v</td>";
						$t .= "</tr></table>";
						$t .= "</td>";
			 		}
					$x[$SubSubjectID] = $t;
					$isFirst = 0;
				}
				
				$empty = "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
				$empty .= "<table border='0' cellpadding='0' cellspacing='0'>";
				$empty .= "<tr><td>{$Prefix}</td><td height='{$LineHeight}'class='tabletext font_18px padding_2'>&nbsp;</td>";
				$empty .= "</tr></table>";
				$empty .= "</td>";
				$x[""] = $empty;
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='', $StudentID='', $IssueDate='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
		$SignatureTitleArray = $eRCTemplateSetting['AssessmentReport']['Signature'];
		$SignatureTable = "";
		//2014-1014-1144-21164
		//$SignatureTable = "<table width='90%' border='0' cellpadding='4' cellspacing='0' align='center' class='SignatureTable'>";
		$SignatureTable = "<table width='90%' border='0' cellpadding='4' cellspacing='0' align='center' class='SignatureTable' style='height:120px;'>";
		$SignatureTable .= "<tr>";
		
		$td_width = round(100/sizeof($SignatureTitleArray))."%";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			if($k==0) {
				$align = "left";
			} else if($k==sizeof($SignatureTitleArray)-1) {
				$align = "right";
			} else {
				$align = "center";
			}
			
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			
//			$IssueDateDisplay = ($SettingID == "IssueDate" && $IssueDate)? "<u>".$IssueDate."</u>" : "__________";
			$IssueDateDisplay = ($SettingID == "IssueDate" && $IssueDate)?$IssueDate : "&nbsp;";
			$SignatureTable .= "<td valign='bottom' align='$align' width='$td_width'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0' width='75%'>";
			//2014-1014-1144-21164
			$SignatureTable .= "<tr><td align='center' class='small_title border_bottom' height='100' valign='bottom'>$IssueDateDisplay</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='report_title' valign='bottom' style='color:".$this->ColorBlue."'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}
        
		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function Get_Address_Table()
	{
		global $eReportCard;
		
		if (is_array($eReportCard['Template']['AddressDisplay']) || $eReportCard['Template']['AddressDisplay'] != '')
		{
			$AddressArr = array();
			if (is_array($eReportCard['Template']['AddressDisplay']) == false) {
				$AddressArr[] = $eReportCard['Template']['AddressDisplay'];
			} else {
				$AddressArr = $eReportCard['Template']['AddressDisplay'];
			}
		}
		
		$numOfRow = count($AddressArr);
		$x = '';
		$x .= '<table cellspacing="0" cellpadding="2" border="0">';
			for ($i=0; $i<$numOfRow; $i++)
			{
				$x .= '<tr><td class="reportcard_text">'.$AddressArr[$i].'</td></tr>';
			}
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Grading_Scheme_Info_Table()
	{
		$x = '';
		
		$numOfCol = 13;
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" style="color:'.$this->ColorBlue.'" class="">'."\n";
        
			$x .= '<tr class="grading_scheme_text">';
				$x .= '<td colspan="'.$numOfCol.'">&nbsp;&nbsp;Academic Performance</td>';
			$x .= '</tr>';
			
//			$x .= '<tr class="grading_scheme_text">';
//				$x .= '<td width="20">&nbsp;&nbsp;A</td>';
//				$x .= '<td width="85">Top 5%</td>';
//				$x .= '<td width="25">B</td>';
//				$x .= '<td width="85">Next 15%</td>';
//				$x .= '<td width="25">C</td>';
//				$x .= '<td width="85">Next 20%</td>';
//				$x .= '<td width="25">D</td>';
//				$x .= '<td width="85">Next 30%</td>';
//				$x .= '<td width="25">E</td>';
//				$x .= '<td width="100">Next 15% - 30%</td>';
//				$x .= '<td width="25">F</td>';
//				$x .= '<td width="100">Next 0% - 15%</td>';
//			$x .= '</tr>';
			
			//2014-1014-1144-21164
//			$x .= '<tr class="grading_scheme_text" style="padding-top:12px; line-height: 20px;">';
//				$x .= '<td width="75">&nbsp;&nbsp;F.1 - F.3</td>';
//				$x .= '<td width="30">A</td>';
//				$x .= '<td width="70">Top 10%</td>';
//				$x .= '<td width="30">B</td>';
//				$x .= '<td width="70">Next 15%</td>';
//				$x .= '<td width="30">C</td>';
//				$x .= '<td width="70">Next 20%</td>';
//				$x .= '<td width="30">D</td>';
//				$x .= '<td width="85">Next 25%</td>';
//				$x .= '<td width="30">E</td>';
//				$x .= '<td width="100">Next 15% - 30%</td>';
//				$x .= '<td width="30">F</td>';
//				$x .= '<td width="100">Next 0% - 15%</td>';
//			$x .= '</tr>';
//			$x .= '<tr class="grading_scheme_text" style="padding-top:6px; line-height: 20px;">';
//				$x .= '<td width="75">&nbsp;&nbsp;F.4 - F.6</td>';
//				$x .= '<td width="30">A</td>';
//				$x .= '<td width="70">Top 5%</td>';
//				$x .= '<td width="30">B</td>';
//				$x .= '<td width="70">Next 15%</td>';
//				$x .= '<td width="30">C</td>';
//				$x .= '<td width="70">Next 20%</td>';
//				$x .= '<td width="30">D</td>';
//				$x .= '<td width="85">Next 30%</td>';
//				$x .= '<td width="30">E</td>';
//				$x .= '<td width="100">Next 15% - 30%</td>';
//				$x .= '<td width="30">F</td>';
//				$x .= '<td width="100">Next 0% - 15%</td>';
//			$x .= '</tr>';
			$x .= '<tr class="grading_scheme_text" >';
				$x .= '<td width="20" style="padding-top:5px;">&nbsp;&nbsp;A</td>';
				$x .= '<td width="85" style="padding-top:5px;">Top 10%</td>';
				$x .= '<td width="25" style="padding-top:5px;">B</td>';
				$x .= '<td width="85" style="padding-top:5px;">Next 15%</td>';
				$x .= '<td width="25" style="padding-top:5px;">C</td>';
				$x .= '<td width="85" style="padding-top:5px;">Next 25%</td>';
				$x .= '<td width="25" style="padding-top:5px;">D</td>';
				$x .= '<td width="85" style="padding-top:5px;">Next 20%</td>';
				$x .= '<td width="25" style="padding-top:5px;">E</td>';
				$x .= '<td width="100" style="padding-top:5px;">Next 15% - 30%</td>';
				$x .= '<td width="25" style="padding-top:5px;">F</td>';
				$x .= '<td width="100" style="padding-top:5px;">Next 0% - 15%</td>';
			$x .= '</tr>';
            
			//2014-1014-1144-21164
			$x .= '<tr class="grading_scheme_text">';
				$x .= '<td style="padding-bottom:5px;" colspan="13">&nbsp;&nbsp;(Minor adjustment of the above percentages for subjects with less than 10 students)</td>';
			$x .= '</tr>';
			
			$x .= '<tr class="grading_scheme_text" style="padding-top:12px;">';
//				$x .= '<td>&nbsp;&nbsp;A-E</td>';
//				$x .= '<td>Pass</td>';
//				$x .= '<td>F</td>';
//				$x .= '<td>Fail</td>';
				$x .= '<td colspan="5">&nbsp;&nbsp;'.'A-E'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Pass'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'F'.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Fail'.'</td>';
//				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>*</td>';
				$x .= '<td colspan="5">Absence in test</td>';
			$x .= '</tr>';
			
			$x .= '<tr class="grading_scheme_text" style="padding-top:12px;">';
				$x .= '<td colspan="2">&nbsp;&nbsp;Position</td>';
				$x .= '<td colspan="10">Shown for top 75% students only</td>';
			$x .= '</tr>';
			
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Double_Line_Td($border="",$width=1, $color='#000')
	{
		global $image_path,$LAYOUT_SKIN;
		
		$borderArr = explode(",",$border);
		$left = $right = $top = $bottom = 0;
		
		if(in_array("left",$borderArr)) $left = $width;
		if(in_array("right",$borderArr)) $right = $width;
		if(in_array("top",$borderArr)) $top = $width;
		if(in_array("bottom",$borderArr)) $bottom = $width;
        
		$style = " style='padding:0; border-style:solid; border-color:{$color}; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";
		
		$td = "<td width='0%' height='0%' $style><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='$width' height='$width'/></td>";
		return $td;
	}
}
?>