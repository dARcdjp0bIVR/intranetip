<?php
# Editing by ivan

####################################################
# General library for Product Testing & Completion
# This library should be able to handle different settings and calculation methods
####################################################

include_once($intranet_root."/lang/reportcard_custom/pooi_tun_secondary.$intranet_session_language.php");

class libreportcardcustom extends libreportcard {

	function libreportcardcustom() {
		$this->libreportcard();
		$this->configFilesType = array("summary", "merit", "remark", "attendance");
		
		// Temp control variables to enable/disaable features
		$this->IsEnableSubjectTeacherComment = 1;
		$this->IsEnableMarksheetFeedback = 1;
		$this->IsEnableMarksheetExtraInfo = 0;
		$this->IsEnableManualAdjustmentPosition = 0;
		
		$this->EmptySymbol = "- -";
		$this->MaxSubjectRow = 20;
		$this->MaxSubjectRowExtraReport = 17;
		
		$this->ExtraReportMinScoreArr[1] = array(35, 40);
		$this->ExtraReportMinScoreArr[2] = array(35, 40);
		$this->ExtraReportMinScoreArr[3] = array(35, 40);
		$this->ExtraReportMinScoreArr[4] = array(30, 35);
		$this->ExtraReportMinScoreArr[5] = array();
		$this->ExtraReportMinScoreArr[6] = array();
		$this->ExtraReportMinScoreArr[7] = array();
	}
		
	########## START Template Related ##############
	function getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID) {
		global $eReportCard, $PATH_WRT_ROOT;
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$isMainReport =  $ReportSetting['isMainReport'];
		// added: 2015-02-04
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$TableTop = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' >";
		$TableTop .= "<tr><td><img src='".$emptyImagePath."' height='30'>&nbsp;</td></tr>";
		$TableTop .= "<tr><td>".$TitleTable."</td></tr>";
		$TableTop .= "<tr><td>".$StudentInfoTable."</td></tr>";
		$TableTop .= "<tr><td>".$MSTable."</td></tr>";
		$TableTop .= "<tr><td>".$MiscTable."</td></tr>";
		$TableTop .= "</table>";
		
		$TableBottom = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='bottom'>";
		$TableBottom .= "<tr valign='bottom'><td>".$SignatureTable."</td></tr>";
		$TableBottom .= $FooterRow;
		$TableBottom .= "</table>";
		
		$Height = ($isMainReport)? 1022 : 915;
		
		// as F3 with more subject, add height to ensure enough space	[2015-0204-1253-02073]
		if($this->GET_FORM_NUMBER($ClassLevelID) == 3 && $isMainReport){
			$Height = 1038;
		}
		
		$x = "";
		$x .= "<tr><td>";
			$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>";
				//$x .= "<tr height='995px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr height='".$Height."px' valign='top'><td>".$TableTop."</td></tr>";
				$x .= "<tr valign='bottom'><td>".$TableBottom."</td></tr>";
			$x .= "</table>";
		$x .= "</td></tr>";
		
		return $x;
	}
	
	function getReportHeader($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		$TitleTable = "";
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$isMainReport =  $ReportSetting['isMainReport'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
			
			if ($isMainReport)
			{
				# get school badge
				$SchoolLogo = GET_SCHOOL_BADGE();
					
				# get school name
				$SchoolName = GET_SCHOOL_NAME();
				$SchoolInfo = $eReportCard['Template']['SchoolInfo']['Email'].' '.$eReportCard['Template']['SchoolInfo']['Telephone'].' '.$eReportCard['Template']['SchoolInfo']['Fax'];
				
				$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
				if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
				
				include_once($PATH_WRT_ROOT.'includes/libuser.php');
				$libuser = new libuser();
				$StudentPhotoInfoArr = $libuser->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
				$PhotoImgPath = $StudentPhotoInfoArr[1];
// 				$PhotoImg = '<img src="'.$PhotoImgPath.'" width="100px"/>';
				// 2020-01-22 (Philips) [2020-0110-1410-53066] - Move Student Photo Next to STRN
				$PhotoImg = '<img src="'.$PhotoImgPath.'" width="100px" style="transform: translateY(27px)" />';
		
				$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr>";
						$TitleTable .= "<td align='center' valign='bottom' class='text_9px' width='150px'>&nbsp;</td>";
						$TitleTable .= "<td align='center' valign='bottom' class='text_9px'>".$SchoolInfo."</td>";
						$TitleTable .= "<td align='right' valign='bottom' class='text_9px' width='150px'>".$PhotoImg."</td>";
					$TitleTable .= "</tr>";
				
					/*
				$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
				
				if(!empty($ReportTitle) || !empty($SchoolName))
				{
					$TitleTable .= "<td>";
					if ($HeaderHeight == -1) {
						$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
						if(!empty($SchoolName))
							$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
						if(!empty($ReportTitle))
							$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
						$TitleTable .= "</table>\n";
					} else {
						for ($i = 0; $i < $HeaderHeight; $i++) {
							$TitleTable .= "<br/>";
						}
					}
					$TitleTable .= "</td>";
				}
				$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
				*/
				
				$TitleTable .= "</table>";
			}
			else
			{
				$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td><img src='".$emptyImagePath."' height='100'>&nbsp;</td></tr>";
					$TitleTable .= "<tr>";
						$TitleTable .= "<td align='center' class='text_24px'><b>".$ReportTitle."</b></td>";
					$TitleTable .= "</tr>";
					$TitleTable .= "<tr><td>&nbsp;</td></tr>";
					$TitleTable .= "<tr><td>&nbsp;</td></tr>";
				$TitleTable .= "</table>";
			}
		}
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			$ReportTitle = str_replace(":_:", " ", $ReportTitle);
			$isMainReport = $ReportSetting['isMainReport'];
			
			# retrieve required variables
			$defaultVal = ($StudentID=='')? "XXX" : '';
			//$data['AcademicYear'] = $this->GET_ACTIVE_YEAR();
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$ObjYear = new academic_year($this->schoolYearID);
			$data['AcademicYear'] = $ObjYear->Get_Academic_Year_Name();
			
			if ($isMainReport)
				$data['DateOfIssue'] = str_replace('-', '/', $ReportSetting['Issued']);
			else
				$data['DateOfIssue'] = date('d-m-Y', strtotime($ReportSetting['Issued']));
				
				
			$data['ReportTitle'] = $ReportTitle;
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] = $lu->UserName2Lang('ch', 2);
				//$data['ClassNo'] = $lu->ClassNumber;
				//$data['Class'] = $lu->ClassName;
				
				$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $StudentInfoArr[0]['ClassName'];
				$thisClassNumber = $StudentInfoArr[0]['ClassNumber'];
				
				$data['ClassNo'] = $thisClassNumber;
				$data['Class'] = $thisClassName;
				
				$data['StudentNo'] = $data['STRN'];
				$data['DateOfBirth'] = str_replace('-', '/', $lu->DateOfBirth);
				$data['Gender'] = $lu->Gender;
				$data['STRN'] = $lu->STRN;
				$data['StudentAdmNo'] = $data['STRN'];
				
				$ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $this->schoolYearID);
				// [2020-0611-1034-49207] sort by class teacher english name
                $ClassTeacherAry = sortByColumn($ClassTeacherAry,'EnglishName');
				foreach($ClassTeacherAry as $key => $val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = !empty($CTeacher) ? implode(" / ", $CTeacher) : "--";
			}
			
			if ($isMainReport)
			{
				$SettingStudentInfo = array("ReportTitle", '', "ClassNo", "Class", '', "STRN", "Name", "ClassTeacher", "DateOfBirth", "Gender", "DateOfIssue"); 
				if(!empty($SettingStudentInfo))
				{
					$count = 0;
					$StudentInfoTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
					for($i=0; $i<sizeof($SettingStudentInfo); $i++)
					{
						$SettingID = trim($SettingStudentInfo[$i]);
						if(in_array($SettingID, $SettingStudentInfo)===true)
						{
							$Title = $eReportCard['Template']['StudentInfo'][$SettingID.'En'];
							if ($Title != '')
								$Title .= ' ('.$eReportCard['Template']['StudentInfo'][$SettingID.'Ch'].')';
							
							if($count%$StudentInfoTableCol==0) {
								$StudentInfoTable .= "<tr>";
							}
							
							//$colspan = $SettingID=="Name" ? " colspan='10' " : "";
							if ($SettingID == 'ReportTitle')
							{
								$StudentInfoTable .= "<td class='tabletext' $colspan width='10%' valign='top' height='{$LineHeight}'>";
								$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							}
							else
							{
								if ($Title == '')
									$tmp = ' ';
								else
									$tmp = ' : ';
									
								// narrower for the middle column
								if($count%$StudentInfoTableCol==1)
									$width = '5%';
								else if($count%$StudentInfoTableCol==0)
									$width = '10%';
								else
									$width = '13%';
									
								$colspan = '';
								if ($SettingID == 'Name') {
									$colspan = "colspan='2'";
									$count++;
								}
									
								$StudentInfoTable .= "<td class='tabletext' $colspan width='".$width."' valign='top' height='{$LineHeight}' ".$colspan.">".$Title.$tmp;
								$StudentInfoTable .= ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							}
							
							if(($count+1)%$StudentInfoTableCol==0) {
								$StudentInfoTable .= "</tr>";
							}
							$count++;
						}
						else if ($SettingID == '')
						{
							$StudentInfoTable .= "<td>&nbsp;</td>";
							$count++;
						}
						
					}
					$StudentInfoTable .= "</table>";
				}
			}
			else
			{
				$StudentInfoTable .= "<table width='85%' border='0' cellpadding='3' cellspacing='0' align='center'>";
					# Name
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='text_16px'>";
							$thisDisplay = ($StudentID)? $lu->EnglishName.' ('.$lu->ChineseName.')' : $defaultVal;
							$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['NameCh'].': '.$thisDisplay;
						$StudentInfoTable .= "</td>";
					$StudentInfoTable .= "</tr>";
					
					# Class
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='text_16px'>";
							$thisDisplay = ($StudentID)? $data['Class'] : $defaultVal;
							$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['ClassCh'].': '.$thisDisplay;
						$StudentInfoTable .= "</td>";
					$StudentInfoTable .= "</tr>";
					
					# Class Number
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='text_16px'>";
							$thisDisplay = ($StudentID)? $data['ClassNo'] : $defaultVal;
							$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['ClassNoCh'].': '.$thisDisplay;
						$StudentInfoTable .= "</td>";
					$StudentInfoTable .= "</tr>";
					
					# Issue Date
					$StudentInfoTable .= "<tr>";
						$StudentInfoTable .= "<td class='text_16px'>";
							$thisDisplay = ($data['DateOfIssue'])? $data['DateOfIssue'] : $defaultVal;
							$StudentInfoTable .= $eReportCard['Template']['StudentInfo']['DateOfIssueCh'].': '.$thisDisplay;
						$StudentInfoTable .= "</td>";
					$StudentInfoTable .= "</tr>";
					
					$StudentInfoTable .= "<tr><td>&nbsp;</td></tr>";
				$StudentInfoTable .= "</table>";
			}
			
		}
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$isMainReport = $ReportSetting['isMainReport'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum, $ColNum2, $NumOfAssessmentArr) = $ColHeaderAry;		
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if (sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		if (sizeof($SubjectArray) > 0)
			foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		# retrieve marks
		$MarksAry = $this->getMarks($ReportID, $StudentID, '', 0, 1, '', '', '', 1);
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID, $NumOfAssessmentArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		# retrieve Subject Teacher's Comment
		$SubjectTeacherCommentAry = $this->returnSubjectTeacherComment($ReportID,$StudentID);
		
		##########################################
		# Start Generate Table
		##########################################
		$thisClass = ($isMainReport)? 'report_border' : '';
		$thisPadding = ($isMainReport)? 0 : 2;
		$thisWidth = ($isMainReport)? "100%" : "70%";
		$DetailsTable = "<table width='$thisWidth' align='center' border='0' cellspacing='0' cellpadding='$thisPadding' class='$thisClass'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		$displayedCounter = 0;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			# If all the marks is "*", then don't display
			if (sizeof($MarksAry[$thisSubjectID]) > 0) 
			{
				$Droped = 1;
				foreach($MarksAry[$thisSubjectID] as $cid=>$da)
					if($da['Grade']!="*")	$Droped=0;
			}
			if($Droped)	continue;
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			
			if ($isMainReport == false && $isSub)
				continue;
			
			if ($eRCTemplateSetting['HideComponentSubject'] && $isSub)
				continue;
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				# Subject Teacher Comment
				if ($AllowSubjectTeacherComment) {
					$css_border_top = $isFirst? "border_top":"";
					if (isset($SubjectTeacherCommentAry[$thisSubjectID]) && $SubjectTeacherCommentAry[$thisSubjectID] !== "") {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>".$SubjectTeacherCommentAry[$thisSubjectID];
					} else {
						$DetailsTable .= "<td class='tabletext border_left $css_border_top' align='center'>";
						$DetailsTable .= "<span style='padding-left:4px;padding-right:4px;'>-";
					}
					$DetailsTable .= "</span></td>";
				}
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
				$displayedCounter++;
			}
		}
		
		if ($isMainReport)
		{
			$DetailsTable .= $this->Get_Empty_Row($withStar=1);
		
			$numOfEmptyRow = $this->MaxSubjectRow - $displayedCounter;
			for ($i=0; $i<$numOfEmptyRow; $i++)
			{
				$DetailsTable .= $this->Get_Empty_Row($withStar=0);
			}
			
			# MS Table Footer
			$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
		}
		else
		{
			# MS Table Footer
			$DetailsTable .= $this->genMSTableFooter($ReportID, $StudentID, $ColNum2, $NumOfAssessmentArr);
			$DetailsTable .= $this->Get_Empty_Row($withStar=1, $isMainReport);
			
			$numOfEmptyRow = $this->MaxSubjectRowExtraReport - $displayedCounter - 1;
			for ($i=0; $i<$numOfEmptyRow; $i++)
				$DetailsTable .= $this->Get_Empty_Row($withStar=0, $isMainReport);
		}
		
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$LineHeight = $ReportSetting['LineHeight'];
		$isMainReport = $ReportSetting['isMainReport'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		/*
		$n = 0;
		$e = 0;	# column# within term/assesment
		$t = array(); # number of assessments of each term (for consolidated report only)
		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Terms Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if (sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
				
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				//$ColumnTitleDisplay = convert2unicode($ColumnTitle[$i], 1, 2);
				$ColumnTitleDisplay = $ColumnTitle[$i];
				$row1 .= "<td valign='middle' height='{$LineHeight}' class='border_left tabletext' align='center'><b>". $ColumnTitleDisplay . "</b></td>";
				$n++;
 				$e++;
			}
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			$needRowspan=0;
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						//$ColumnTitleDisplay =  (convert2unicode($ColumnTitle[$j], 1, 2));
						$ColumnTitleDisplay =  $ColumnTitle[$j];
						$row2 .= "<td class='border_top border_left reportcard_text' align='center' height='{$LineHeight}'>". $ColumnTitleDisplay . "</td>";
						$n++;
						$e++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
					$needRowspan++;
					$t[$i] = sizeof($ColumnTitle);
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
					$e++;
				}
				$row1 .= "<td {$Rowspan} {$colspan} height='{$LineHeight}' class='border_left small_title' align='center'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		$Rowspan = $row2 ? "rowspan='2'" : "";

		if(!$needRowspan)
			$row1 = str_replace("rowspan='2'", "", $row1);
		
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectEng'];
			$SubjectChn = "&nbsp;&nbsp;".$eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan}  valign='middle' class='small_title' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowRightestColumn)
		{
			$x .= "<td {$Rowspan}  valign='middle' width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		else
		{
			//$e--;	
		}
		
		# Subject Teacher Comment
		if ($AllowSubjectTeacherComment) {
			$x .= "<td {$Rowspan} valign='middle' class='border_left small_title' align='center' width='10%'>".$eReportCard['TeachersComment']."</td>";
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		*/
		
		
		$x = '';
		
		if ($isMainReport)
		{
			### Subject
			$thisTitle = $eReportCard['Template']['SubjectEn'].'<br />'.$eReportCard['Template']['SubjectCh'];
			$x .= "<td colspan='2' valign='middle' align='center' class='tabletext border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $thisTitle . "</td>";
				
			### Full Mark
			$thisTitle = $eReportCard['Template']['FullMarkEn'].'<br />'.$eReportCard['Template']['FullMarkCh'];
			$x .= "<td valign='middle' align='center' class='tabletext border_left border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['FullMark'] ."'>". $thisTitle . "</td>";
			
			### 1st Term
			$thisTitle = $eReportCard['Template']['Term1En'].'<br />'.$eReportCard['Template']['Term1Ch'];
			$x .= "<td valign='middle' align='center' class='tabletext border_left border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $thisTitle . "</td>";
			
			### 2nd Term
			//if ($ReportType == 'W')
			//	$thisTitle = $eReportCard['Template']['Term2En'].'<br />'.$eReportCard['Template']['Term2Ch'];
			//else
			//	$thisTitle = '&nbsp;';
			$thisTitle = $eReportCard['Template']['Term2En'].'<br />'.$eReportCard['Template']['Term2Ch'];
				
			$x .= "<td valign='middle' align='center' class='tabletext border_left border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $thisTitle . "</td>";
				
			### Average
			$thisTitle = $eReportCard['Template']['AverageEn'].'<br />'.$eReportCard['Template']['AverageCh'];
			$x .= "<td valign='middle' align='center' class='tabletext border_left border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Average'] ."'>". $thisTitle . "</td>";
			
			### Rank
			$thisTitle = '('.$eReportCard['Template']['RankEn'].')<br />('.$eReportCard['Template']['RankCh'].')';
			$x .= "<td valign='middle' align='center' class='tabletext border_bottom' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Rank'] ."'>". $thisTitle . "</td>";
		}
		else
		{
			### Subject
			$thisTitle = $eReportCard['Template']['SubjectCh'];
			$x .= "<td valign='middle' class='text_16px' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'><b>". $thisTitle . "</b></td>";
			
			### Mark
			$thisTitle = $eReportCard['Template']['MarkCh'];
			$x .= "<td valign='middle' align='center' class='text_16px' height='{$LineHeight}' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'><b>". $thisTitle . "</b></td>";
		}
		
		
		return array($x, $n, $e, $t);
	}
	
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$isMainReport = $ReportSetting['isMainReport'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
		if (sizeof($SubjectArray) > 0) {
	 		foreach($SubjectArray as $SubjectID=>$Ary)
	 		{
		 		foreach($Ary as $SubSubjectID=>$Subjs)
		 		{
			 		$t = "";
			 		$Prefix = "&nbsp;&nbsp;&nbsp;";
			 		if($SubSubjectID==0)		# Main Subject
			 		{
				 		$SubSubjectID=$SubjectID;
				 		$Prefix = "";
			 		}
			 		
			 		if ($isMainReport)
			 		{
			 			//$css_border_top = ($isFirst)? "border_top" : "";
				 		foreach($SubjectDisplay as $k=>$v)
				 		{
					 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
			 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
			 				
			 				$v = str_replace("SubjectEng", $SubjectEng, $v);
			 				$v = str_replace("SubjectChn", $SubjectChn, $v);
			 				
				 			$t .= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>";
							$t .= "<table border='0' cellpadding='0' cellspacing='0'>";
							$t .= "<tr><td>&nbsp;&nbsp;{$Prefix}</td><td height='{$LineHeight}'class='tabletext'>$v</td>";
							$t .= "</tr></table>";
							$t .= "</td>";
				 		}
						$x[] = $t;
						$isFirst = 0;
			 		}
			 		else
			 		{
			 			foreach($SubjectDisplay as $k=>$v)
				 		{
					 		$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
			 				
			 				$t = "<td class='text_16px'>$SubjectChn</td>";
				 		}
						$x[] = $t;
			 		}
			 		
				}
		 	}
		}
		
 		return $x;
	}
	
	function getSignatureTable($ReportID='')
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
 		if($ReportID)
 		{
 			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Issued = $ReportSetting['Issued'];
			$isMainReport = $ReportSetting['isMainReport'];
			$ClassLevelID = $ReportSetting['ClassLevelID'];
		}
 		
 		$SignatureTable = "";
 		if ($isMainReport)
 		{
 			$SignatureTitleArray = $eRCTemplateSetting['Signature'];
			
			$SignatureTable .= "<table width='85%' border='0' cellpadding='4' cellspacing='0' align='left'>";
			$SignatureTable .= "<tr>";
			for($k=0; $k<sizeof($SignatureTitleArray); $k++)
			{
				$SettingID = trim($SignatureTitleArray[$k]);
				$Title = $eReportCard['Template'][$SettingID];
				$IssueDate = ($SettingID == "IssueDate" && $Issued)	? "<u>".$Issued."</u>" : "__________";
				$SignatureTable .= "<td valign='bottom' align='left'>";
				$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
				$SignatureTable .= "<tr><td align='center' class='tabletext' valign='bottom'>____________". $IssueDate ."____________</td></tr>";
				$SignatureTable .= "<tr><td align='center' class='tabletext' valign='bottom'>".$Title."</td></tr>";
				$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
			}
	
			$SignatureTable .= "</tr>";
			$SignatureTable .= "</table>";
 		}
 		else
 		{
 			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
 			$thisRemark = '';
 			
 			//2013-1101-1559-16071
// 			if ($FormNumber < 6)
// 				$thisRemark = $eReportCard['ExtraTermReportPassScoreRemark'][0];
// 			else
// 				$thisRemark = $eReportCard['ExtraTermReportPassScoreRemark'][1];
			if ($FormNumber == 6) {
				$thisRemark = $eReportCard['ExtraTermReportPassScoreRemark'][1];	// pass mark = 40
			}
			else if ($FormNumber == 4 || $FormNumber == 5) {
				$thisRemark = $eReportCard['ExtraTermReportPassScoreRemark'][2];	// pass mark = 45
			}
			else {
				$thisRemark = $eReportCard['ExtraTermReportPassScoreRemark'][0];	// pass mark = 50
			}
 			
 			$SignatureTable .= "<table width='75%' border='0' cellpadding='4' cellspacing='0' align='left' valign='bottom'>";
 				$SignatureTable .= "<tr><td colspan='4' height='100'>&nbsp</td>";
 				$SignatureTable .= "<tr>";
 					$SignatureTable .= "<td width='10%'>&nbsp;</td>";
 					
 					$SignatureTable .= "<td align='center' class='text_16px border_top' valign='bottom'>";
						$SignatureTable .= $eReportCard['Template']['PrincipalSignatureCh'];
					$SignatureTable .= "</td>";
					
					$SignatureTable .= "<td width='15%'>&nbsp;</td>";
					
					$SignatureTable .= "<td align='center' class='text_16px border_top' valign='bottom'>";
						$SignatureTable .= $eReportCard['Template']['ParentSignatureCh'];
					$SignatureTable .= "</td>";
				$SignatureTable .= "</tr>";
				$SignatureTable .= "<tr><td>&nbsp</td><td colspan='3' class='text_12px'>".$thisRemark."</td></tr>";
			$SignatureTable .= "</table>";
 		}
		
		return $SignatureTable;
	}
	
	function genMSTableFooter($ReportID, $StudentID='', $ColNum2=1, $NumOfAssessment=array())
	{
		global $eReportCard, $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$LineHeight 				= $ReportSetting['LineHeight'];
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$ClassLevelID 				= $ReportSetting['ClassLevelID'];
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
		$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
		$isMainReport 				= $ReportSetting['isMainReport'];
		
		$ShowRightestColumn = $this->Is_Show_Rightest_Column($ReportID);
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		$ColumnTitle = $this->returnReportColoumnTitle($ReportID);
		$ColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		
		# retrieve Student Class
		if($StudentID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$lu = new libuser($StudentID);
			$ClassName 		= $this->Get_Student_ClassName($StudentID);
			$WebSamsRegNo 	= $lu->WebSamsRegNo;
		}
		
		# retrieve result data
		$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1, 1);
		//$GrandTotal = $StudentID ? $this->Get_Score_Display_HTML($result['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
		$GrandAverage = $StudentID ? $this->Get_Score_Display_HTML($result['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
		
		$ClassPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "#";
  		$FormPosition = $StudentID ? $this->Get_Score_Display_HTML($result['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "#";
  		$ClassNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "#";
  		$FormNumOfStudent = $StudentID ? $this->Get_Score_Display_HTML($result['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "#";
		
		
		if ($isMainReport)
		{
			//if ($CalOrder == 2) {
				foreach ($ColumnTitle as $ColumnID => $ColumnName) {
					
					$columnResult = $this->getReportResultScore($ReportID, $ColumnID, $StudentID, '', 0,1,1);
					
					//$columnTotal[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandTotal'], $ReportID, $ClassLevelID, '', '', 'GrandTotal') : "S";
					$columnAverage[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['GrandAverage'], $ReportID, $ClassLevelID, '', '', 'GrandAverage') : "S";
					
					$columnClassPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritClass'], $ReportID, $ClassLevelID, '', '', 'ClassPosition') : "S";
					$columnFormPos[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['OrderMeritForm'], $ReportID, $ClassLevelID, '', '', 'FormPosition') : "S";
					$columnClassNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['ClassNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'ClassNoOfStudent') : "S";
					$columnFormNumOfStudent[$ColumnID] = $StudentID ? $this->Get_Score_Display_HTML($columnResult['FormNoOfStudent'], $ReportID, $ClassLevelID, '', '', 'FormNoOfStudent') : "S";
				}
			//}
		
			### Get csv data
			$OtherInfoDataAry = $this->getReportOtherInfoData($ReportID, $StudentID);
			$CsvArr = $OtherInfoDataAry[$StudentID];
			$lastestTerm = $this->Get_Last_Semester_Of_Report($ReportID);
			
			### Generate Footer Data Cells
			$GrandSubjectID = '-1';
			$PositionDisplaySettingArr = $this->Get_Position_Display_Setting($ReportID, $GrandSubjectID);
			$ShowClassPosition = $PositionDisplaySettingArr[$GrandSubjectID]['Class']['ShowPosition'];
			$ShowFormPosition = $PositionDisplaySettingArr[$GrandSubjectID]['Form']['ShowPosition'];
			$FooterDataTdArr = array();
			
			# Grand Average
			$FooterDataTdArr[] = $this->Get_Grand_Info_Cells($eReportCard['Template']['AvgMarkEn'], $eReportCard['Template']['AvgMarkCh'], 
													100, $GrandAverage, $columnAverage, $ReportType, 1);
													
			# Position in From
			if ($ShowFormPosition)
			{
				$FooterDataTdArr[] = $this->Get_Grand_Info_Cells($eReportCard['Template']['FormPositionEn'], $eReportCard['Template']['FormPositionCh'], 
														$this->EmptySymbol, $FormPosition, $columnFormPos, $ReportType);
			}

			# Position in Class
			if ($ShowClassPosition)
			{
				$FooterDataTdArr[] = $this->Get_Grand_Info_Cells($eReportCard['Template']['ClassPositionEn'], $eReportCard['Template']['ClassPositionCh'], 
														$this->EmptySymbol, $ClassPosition, $columnClassPos, $ReportType);
			}

			# Absence
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['DaysAbsentEn'], $eReportCard['Template']['DaysAbsentCh'], 
													'Days Absent', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Late
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['TimesLateEn'], $eReportCard['Template']['TimesLateCh'], 
													'Time Late', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Conduct
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['ConductEn'], $eReportCard['Template']['ConductCh'], 
													'Conduct', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Major Merit
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['MajorMeritEn'], $eReportCard['Template']['MajorMeritCh'], 
													'MajorMerit', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Minor Merit
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['MinorMeritEn'], $eReportCard['Template']['MinorMeritCh'], 
													'MinorMerit', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Good Point
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['GoodPointEn'], $eReportCard['Template']['GoodPointCh'], 
													'GoodPoint', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Major Demerit
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['MajorDemeritEn'], $eReportCard['Template']['MajorDemeritCh'], 
													'MajorDemerit', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Minor Demerit
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['MinorDemeritEn'], $eReportCard['Template']['MinorDemeritCh'], 
													'MinorDemerit', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);

			# Bad Point
			$FooterDataTdArr[] = $this->Get_CSV_Info_Cells($eReportCard['Template']['BadPointEn'], $eReportCard['Template']['BadPointCh'], 
													'BadPoint', $CsvArr, $ReportType, $SemID, $ColumnInfoArr);
			
			
			$x = '';
			$FooterCellCounter = 0;
			
			### Passing Mark & Grand Average
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '-1', 1,0,$ReportID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$PassMark = $SchemeInfo['PassMark'];
			
			$thisTitle = $eReportCard['Template']['PassingMarkEn'].' ('.$eReportCard['Template']['PassingMarkCh'].')';
			$x .= '<tr>';
				# Passing Mark
				$x .= '<td class="border_top">';
					$x .= '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
						$x .= '<tr>';
							$x .= '<td class="tabletext">'.$thisTitle.'</td>';
							$x .= '<td class="tabletext" align="right">'.$PassMark.'</td>';
						$x .= '</tr>';
					$x .= '</table>';
				$x .= '</td>';
				
				# Grand Average
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Grading Table
				$x .= '<td class="border_top" rowspan="8" valign="top">';
					$x .= $this->Get_Grading_Table();
				$x .= '</td>';
				
				# Position in From
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Position in Class
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Absence
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Late
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Conduct
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Major Merit
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Minor Merit
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Good Point
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Failed Not Applicable Table
				$x .= '<td class="border_top" rowspan="3">';
					$x .= $this->Get_Fail_NotApplicable_Table();
				$x .= '</td>';
			
				# Major Demerit
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Minor Demerit
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Bad Point
				$x .= $this->Display_Footer_Data_Cell($FooterDataTdArr, $FooterCellCounter++, $ReportType, $ColumnInfoArr);
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Promotion
				$x .= '<td class="border_top tabletext">';
					$x .= '&nbsp;'.$eReportCard['Template']['PromotedToEn'].' ('.$eReportCard['Template']['PromotedToCh'].') : ';
					$x .= $CsvArr[$lastestTerm]['PromotedTo'];
				$x .= '</td>';
				$x .= '<td class="border_top tabletext" colspan="2">';
					$x .= ' / &nbsp;&nbsp;&nbsp;&nbsp;'.$eReportCard['Template']['DetainedInEn'].' ('.$eReportCard['Template']['DetainedInCh'].') : ';
					$x .= $CsvArr[$lastestTerm]['RetainedIn'];
				$x .= '</td>';
				
				# Term Class Teacher Comment Title
				$x .= '<td class="border_top border_left tabletext" align="center">';
					$x .= $eReportCard['Template']['Term1RemarksCh'].'<br />'.$eReportCard['Template']['Term1RemarksEn'];
				$x .= '</td>';
				$x .= '<td class="border_top border_left tabletext" align="center">';
					if ($ReportType == 'T')
						$x .= '&nbsp;';
					else
						$x .= $eReportCard['Template']['Term2RemarksCh'].'<br />'.$eReportCard['Template']['Term2RemarksEn'];
				$x .= '</td>';
				$x .= '<td class="border_top border_left tabletext" colspan="2">&nbsp;</td>';
			$x .= '</tr>';
			
			$x .= '<tr>';
				# Remark
				$x .= '<td class="border_top tabletext" colspan="3" valign="top">';
					$x .= '&nbsp;'.$eReportCard['Template']['RemarksEn'].' ('.$eReportCard['Template']['RemarksCh'].')';
					$x .= '<br />';
					$x .= '<table border="0" cellpadding="2" cellspacing="0" width="95%" align="center">';
						$x .= '<tr><td class="tabletext">';
							$x .= $CsvArr[$lastestTerm]['Remark'];
						$x .= '</td></tr>';
					$x .= '</table>';
				$x .= '</td>';
				
				# Term Class Teacher Comment
				$ReportIDArr = array();
				if ($ReportType == 'T')
				{
					$ReportIDArr[] = $ReportID;
				}
				else
				{
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					$numOfColumn = count($ColumnData);
					for ($i=0; $i<$numOfColumn; $i++)
					{
						$thisReportColumnID = $ColumnData[$i]['ReportColumnID'];
						$ReportIDArr[] = $this->getCorespondingTermReportID($ReportID, $thisReportColumnID, $MainReportOnly=1);
					}
				}
				
				$numOfReport = count($ReportIDArr);
				for ($i=0; $i<$numOfReport; $i++)
				{
					$thisReportID = $ReportIDArr[$i];
					$thisChineseName = "";
					if($StudentID) {
						$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
						$thisChineseName = $StudentInfoArr[0]["ChineseName"];
					}
					$thisComment = $this->returnSubjectTeacherComment($thisReportID, $StudentID);
					$thisComment = trim($thisComment[0]);
					//$thisComment = nl2br($thisComment[0]);
					//$thisComment = str_replace("<br>", "，", $thisComment);
					
					$thisCommentStr = "";
					if(!empty($thisComment))
					{
						$thisComment = explode("\n", $thisComment);
						$thisComment = Array_Trim($thisComment);
						$thisComment = array_filter((array)$thisComment);
						$thisComment = implode("，", (array)$thisComment);
						
						//$thisCommentStr = "[".$thisChineseName."]".$eReportCard['Template']['CommentStudent']." ".$thisComment."。";
						$thisCommentStr = $thisChineseName.$eReportCard['Template']['CommentStudent'].$thisComment."。";
					}
					
					$x .= '<td class="border_top border_left tabletext" valign="top">';
						$x .= '<table border="0" cellpadding="2" cellspacing="0" width="95%" align="center">';
							$x .= '<tr><td class="text_10px">'.$thisCommentStr.'</td></tr>';
							//$x .= '<tr><td class="tabletext">****</td></tr>';
						$x .= '</table>';
					$x .= '</td>';
				}
				
				if ($ReportType == 'T')
					$x .= '<td class="border_top border_left tabletext">&nbsp;</td>';
				
				$x .= '<td class="border_top border_left tabletext" colspan="2">&nbsp;</td>';
				
			$x .= '</tr>';
		}
		else
		{
			# Form Position
			if ($ShowOverallPositionForm)
			{
				if ($FormPosition != $this->EmptySymbol)
				{
					$thisDisplay = $this->Get_Mark_Display_Td($FormPosition.'/'.$FormNumOfStudent, '', $ReportID);
					$x .= '<tr>';
						$x .= '<td class="text_16px">'.$eReportCard['Template']['FormPositionCh'].'</td>';
						$x .= '<td class="text_16px">'.$thisDisplay.'</td>';
					$x .= '</tr>';
				}
			}
			
			# Class Position
			if ($ShowOverallPositionClass)
			{
				if ($ClassPosition != $this->EmptySymbol)
				{
					$thisDisplay = $this->Get_Mark_Display_Td($ClassPosition.'/'.$ClassNumOfStudent, '', $ReportID);
					$x .= '<tr>';
						$x .= '<td class="text_16px">'.$eReportCard['Template']['ClassPositionCh'].'</td>';
						$x .= '<td class="text_16px">'.$thisDisplay.'</td>';
					$x .= '</tr>';
				}
			}
			
			
			# Grand Average Display
			$SubjectID = '-1';
			$thisMark = $result['GrandAverage'];
			
			$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
			
			if ($thisNature == 'Fail')
			{
				$thisDisplay = $this->Get_Mark_Display_Td($thisMark, 'Fail', $ReportID);
			}
			else
			{
				$thisMarkTmp = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID);
				$thisDisplay = $this->Get_Mark_Display_Td($thisMarkTmp, '', $ReportID);
			}
			
			$x .= '<tr>';
				# Grand Average
				$x .= '<td class="text_16px">'.$eReportCard['Template']['AvgMarkCh'].'</td>';
				$x .= '<td class="text_16px">'.$thisDisplay.'</td>';
			$x .= '</tr>';
		}
		
		
		return $x;
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='', $NumOfAssessment=array())
	{
		global $eReportCard;				
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID 				= $ReportSetting['Semester'];
 		$ReportType 		= $SemID == "F" ? "W" : "T";		
 		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$isMainReport 				= $ReportSetting['isMainReport'];
		
		# updated on 08 Dec 2008 by Ivan
		# if subject overall column is not shown, grand total, grand average... also cannot be shown
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		# Retrieve Calculation Settings
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
		
		$StorageSetting = $this->LOAD_SETTING("Storage&Display");
		$SubjectDecimal = $StorageSetting["SubjectScore"];
		$SubjectTotalDecimal = $StorageSetting["SubjectTotal"];
		
		$SubjectArray 		= $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		
		$ReportMarksAry[$ReportID] = $MarksAry;
				
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
		if($ReportType=="T")	# Temrs Report Type
		{
			$CalculationOrder = $CalSetting["OrderTerm"];
			
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			if(sizeof($ColoumnTitle) > 0)
				foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
							
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				# define css
				//$css_border_top = ($isFirst)? "border_top" : "";
				$css_border_top = '';
		
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				
				### Full Mark
				if ($isMainReport)
				{
					$thisFullMark = $SubjectFullMarkAry[$SubjectID];
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$thisFullMark.'</td>';
				}
				
				$isAllNA = true;
				# Subject Overall (disable when calculation method is Vertical-Horizontal)
				//if($ShowSubjectOverall)
				//{
					$thisMSGrade = $MarksAry[$SubjectID][0]['Grade'];
					$thisMSMark = $MarksAry[$SubjectID][0]['Mark'];

					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					#$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? ($StudentID ? my_round($thisMark,2) : $thisMark) : $thisGrade;
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
										
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					# check special case
					//if ($CalculationMethod==2 && $isSub)
					//{
					//	$thisMarkDisplay = $this->EmptySymbol;
					//}
					//else
					//{
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
						
						if($needStyle)
						{
	 						if($thisSubjectWeight > 0 && $ScaleDisplay=="M")
								$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
							else
								$thisMarkTemp = $thisMark;
								
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMarkTemp,'','',$ReportID);
							
							if ($thisNature == 'Fail')
							{
								$thisAssessmentMarkDisplay = $this->Get_Mark_Display_Td(my_round($thisMarkTemp, $SubjectDecimal), 'Fail', $ReportID);
								$thisTermOverallMarkDisplay = $this->Get_Mark_Display_Td(my_round($thisMarkTemp, $SubjectTotalDecimal), 'Fail', $ReportID);
							}
							else
							{
								$thisAssesmentMark = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp, '', '', '', '', 0, $SubjectDecimal);
								$thisTermOverallMark = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp, '', '', '', '', 0, $SubjectTotalDecimal);
								
								$thisAssessmentMarkDisplay = $this->Get_Mark_Display_Td($thisAssesmentMark, '', $ReportID);
								$thisTermOverallMarkDisplay = $this->Get_Mark_Display_Td($thisTermOverallMark, '', $ReportID);
							}
						}
						else
						{
							if (is_numeric($thisMark))
							{
								$thisAssesmentMark = my_round($thisMark, $SubjectDecimal);
								$thisTermOverallMark = my_round($thisMark, $SubjectTotalDecimal);
							}
							else
							{
								$thisAssesmentMark = $thisMark;
								$thisTermOverallMark = $thisMark;
							}
							
							$thisAssessmentMarkDisplay = $this->Get_Mark_Display_Td($thisAssesmentMark, '', $ReportID);
							$thisTermOverallMarkDisplay = $this->Get_Mark_Display_Td($thisTermOverallMark, '', $ReportID);
						}
					//}
					
					if ($isMainReport)
					{
						### Term 1
						$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$thisAssessmentMarkDisplay.'</td>';
						
						### Term 2
						$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$this->EmptySymbol.'</td>';
						
						### Average
						$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$thisTermOverallMarkDisplay.'</td>';
						
						### Rank
						$thisFormPosition = $MarksAry[$SubjectID][0]['OrderMeritForm'];
						$thisFormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
						
						if ($thisFormPosition != '')
							$thisRankDisplay = '('.$thisFormPosition.'/'.$thisFormNumOfStudent.')';
						else
							$thisRankDisplay = '&nbsp;';
							
						$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>".$thisRankDisplay.'</td>';
					}
					else
					{
						//$x[$SubjectID] .= "<td class='text_14px' align='center'>".$thisMarkDisplay.'</td>';
						$x[$SubjectID] .= "<td class='text_16px' align='center'>".$thisTermOverallMarkDisplay.'</td>';
					}
					
				//}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		} # End if($ReportType=="T")
		else					# Whole Year Report Type
		{
			$CalculationOrder = $CalSetting["OrderFullYear"];
			
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# Retrieve Subject Scheme ID & settings
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
							
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				if (!$isSub) {
					$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				}
				
				#$css_border_top = ($isFirst or $isSub)? "" : "border_top";
				//$css_border_top = $isFirst? "border_top" : "";
				
				### Full Mark
				$thisFullMark = $SubjectFullMarkAry[$SubjectID];
				$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$thisFullMark.'</td>';
				
				
				$isAllNA = true;
				
				# Terms's Assesment / Terms Result
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					//if ($isParentSubject && $CalculationOrder == 1) {
					//	$thisMarkDisplay = $this->EmptySymbol;
					//} else {
						# See if any term reports available
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester='".$ColumnData[$i]['SemesterNum']."' and ClassLevelID='".$ClassLevelID."'");
						
						# if no term reports, the term mark should be entered directly
						
						if (empty($thisReport)) {
							$thisReportID = $ReportID;
							$thisReportColumnID = $ColumnData[$i]["ReportColumnID"];
						} else {
							$thisReportID = $thisReport['ReportID'];
							$thisReportColumnID = 0;
						}
						
						if (!isset($ReportMarksAry[$thisReportID]))
							$ReportMarksAry[$thisReportID] = $this->getMarks($thisReportID, $StudentID, '', 0, 1, '', '', '', 0);
							
						$thisMSGrade = $ReportMarksAry[$thisReportID][$SubjectID][$thisReportColumnID]["Grade"];
						$thisMSMark = $ReportMarksAry[$thisReportID][$SubjectID][$thisReportColumnID]["Mark"];
						
						$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
						$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
 						
						$thisSubjectWeightData = $this->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID='".$ColumnData[$i]['ReportColumnID'] ."' and SubjectID = '$SubjectID' ");
						$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
													
						# for preview purpose
						if(!$StudentID)
						{
							$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
							$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
						}
						# If it is special case, the symbol is saved in $thisGrade, so need to check if it is empty or not
						$thisMark = ($ScaleDisplay=="M" && strlen($thisMark) && $thisGrade == "") ? $thisMark : $thisGrade;
						
						if ($thisMark != "N.A.")
						{
							$isAllNA = false;
						}
					
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($thisReportID, $SubjectID, $thisMark, $thisGrade);
						if($needStyle)
						{
							if (is_numeric($thisMark))
								$thisMark = my_round($thisMark, $SubjectTotalDecimal);
								
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$thisReportID);
								
							if ($thisNature == 'Fail')
							{
								$thisAssessmentMarkDisplay = $this->Get_Mark_Display_Td(my_round($thisMark, $SubjectDecimal), 'Fail', $ReportID);
							}
							else
							{
								$thisAssesmentMark = $this->Get_Score_Display_HTML($thisMark, $thisReportID, $ClassLevelID, $SubjectID, '', '', '', '', '', 0, $SubjectDecimal);
								$thisAssessmentMarkDisplay = $this->Get_Mark_Display_Td($thisAssesmentMark, '', $ReportID);
							}
						}
						else
						{
							if (is_numeric($thisMark))
							{
								$thisAssesmentMark = my_round($thisMark, $SubjectDecimal);
							}
							else
							{
								$thisAssesmentMark = $thisMark;
							}
							
							$thisAssessmentMarkDisplay = $this->Get_Mark_Display_Td($thisAssesmentMark, '', $ReportID);
						}
					//}
						
					
					### Term 1 & Term 2
					if ($thisAssessmentMarkDisplay == '')
						$thisAssessmentMarkDisplay = $this->EmptySymbol;
						
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$thisAssessmentMarkDisplay.'</td>';
				}
				
				# Subject Overall
				//if($ShowSubjectOverall)
				//{
					//if (!isset($ReportMarksAry[$ReportID]))
					//	$ReportMarksAry[$ReportID] = $this->getMarks($ReportID, $StudentID, "", 0, 1, '', '', '', 1);		
					
					$thisMSGrade = $ReportMarksAry[$ReportID][$SubjectID][0]["Grade"];
					$thisMSMark = $ReportMarksAry[$ReportID][$SubjectID][0]["Mark"];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisMSGrade!='' )? $thisMSGrade : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMSMark : "";
					
					# for preview purpose
					if(!$StudentID)
					{
						$thisMark 	= $ScaleDisplay=="M" ? "S" : "";
						$thisGrade 	= $ScaleDisplay=="G" ? "G" : "";
					}
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if ($thisMark != "N.A.")
					{
						$isAllNA = false;
					}
						
					//if ($CalculationMethod==2 && $isSub)
					//{
					//	$thisMarkDisplay = $this->EmptySymbol;
					//}
					//else
					//{
						# check special case
						list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $ReportMarksAry[$ReportID][$SubjectID][0]['Grade']);
						if($needStyle)
						{
							if (is_numeric($thisMark))
								$thisMark = my_round($thisMark, $SubjectTotalDecimal);
								
							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
								
							if ($thisNature == 'Fail')
							{
								$thisMarkDisplay = $this->Get_Mark_Display_Td(my_round($thisMark, $SubjectTotalDecimal), 'Fail', $ReportID);
							}
							else
							{
								$thisOverallMark = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, '', '', '', '', '', 0, $SubjectTotalDecimal);
								$thisMarkDisplay = $this->Get_Mark_Display_Td($thisOverallMark, '', $ReportID);
							}
						}
						else
						{
							if (is_numeric($thisMark))
							{
								$thisOverallMark = my_round($thisMark, $SubjectTotalDecimal);
							}
							else
							{
								$thisOverallMark = $thisMark;
							}
							
							$thisMarkDisplay = $this->Get_Mark_Display_Td($thisOverallMark, '', $ReportID);
						}
					//}
						
					### Average
					$x[$SubjectID] .= "<td class='border_left {$css_border_top} tabletext' align='center'>".$thisMarkDisplay.'</td>';
					
					### Rank
					$thisFormPosition = $MarksAry[$SubjectID][0]['OrderMeritForm'];
					$thisFormNumOfStudent = $MarksAry[$SubjectID][0]['FormNoOfStudent'];
					
					if ($thisFormPosition != '')
						$thisRankDisplay = '('.$thisFormPosition.'/'.$thisFormNumOfStudent.')';
					else
						$thisRankDisplay = '&nbsp;';
						
					$x[$SubjectID] .= "<td class='{$css_border_top} tabletext' align='center'>".$thisRankDisplay.'</td>';
					
					
				//} else if ($ShowRightestColumn) {
				//	$x[$SubjectID] .= "<td align='center' class='border_left {$css_border_top}'>".$this->EmptySymbol."</td>";
				//}
				
				$isFirst = 0;
				
				# construct an array to return
				$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
				$returnArr['isAllNA'][$SubjectID] = $isAllNA;
			}
		}	# End Whole Year Report Type
		
		return $returnArr;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;
		
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID); 
		$isMainReport 	= $ReportSetting['isMainReport'];
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		
		if ($isMainReport)
		{
			return '';
		}
		else
		{
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			$LimitArr = $this->ExtraReportMinScoreArr[$FormNumber];
			$numOfLimit = count($LimitArr);
			
			$thisRemark = '';
			if ($numOfLimit > 0)
			{
				$result = $this->getReportResultScore($ReportID, 0, $StudentID, '', 0, 1, 1);
				$GrandAverage = $result['GrandAverage'];
				
				for ($i=0; $i<$numOfLimit; $i++)
				{
					$thisLimit = $LimitArr[$i];
					
					if ($GrandAverage < $thisLimit)
					{
						$thisRemark = $eReportCard['ExtraTermReportComment'][$FormNumber][$i];
						break;
					}
				}
			}
			
			if ($thisRemark != '')
			{
				$x .= '<table border="0" cellpadding="2" cellspacing="0" width="75%">';
					$x .= '<tr><td width="10%">&nbsp;</td><td colspan="2" class="border_top">&nbsp;</td></tr>';
					
					$x .= '<tr>';
						$x .= '<td>&nbsp;</td>';
						$x .= '<td class="tabletext text_16px" width="10%" valign="top">'.$eReportCard['Template']['CommentCh'].' :</td>';
						$x .= '<td class="tabletext text_16px">'.$thisRemark.'</td>';
					$x .= '</tr>';
					
					$x .= '<tr><td>&nbsp;</td><td colspan="2" class="border_bottom">&nbsp;</td></tr>';
				$x .= '</table>';
			}
		}
		
		return $x;
	}
	
	########### END Template Related

	
	function Is_Show_Rightest_Column($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$ShowSubjectOverall 		= $ReportSetting['ShowSubjectOverall'];
		$ShowOverallPositionClass 	= $ReportSetting['ShowOverallPositionClass'];
		$ShowNumOfStudentClass 		= $ReportSetting['ShowNumOfStudentClass'];
		$ShowOverallPositionForm 	= $ReportSetting['ShowOverallPositionForm'];
		$ShowNumOfStudentForm 		= $ReportSetting['ShowNumOfStudentForm'];
		$ShowGrandTotal 			= $ReportSetting['ShowGrandTotal'];
		$ShowGrandAvg 				= $ReportSetting['ShowGrandAvg'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		//$ShowRightestColumn = $ShowSubjectOverall || $ShowOverallPositionClass || $ShowOverallPositionForm || $ShowGrandTotal || $ShowGrandAvg;
		$ShowRightestColumn = $ShowSubjectOverall;
		
		return $ShowRightestColumn;
	}
	
	function Get_Calculation_Setting($ReportID)
	{
		$ReportSetting 				= $this->returnReportTemplateBasicInfo($ReportID); 
		$SemID 						= $ReportSetting['Semester'];
 		$ReportType 				= $SemID == "F" ? "W" : "T";
 		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
		
		return $CalOrder;
	}
	
	function Get_Empty_Row($withStar=0, $isMainReport=1)
	{
		$x = '';
		
		$x .= '<tr>';
		
			if ($isMainReport)
			{
				### Subject
				$thisText = ($withStar==0)? '&nbsp;' : '&nbsp;&nbsp;************************';
				$x .= "<td class='tabletext'>".$thisText."</td>";
				$thisText = ($withStar==0)? '&nbsp;' : '****************';
				$x .= "<td class='tabletext'>".$thisText."</td>";
				
				### Full Mark
				$thisText = ($withStar==0)? '&nbsp;' : '****';
				$x .= "<td class='tabletext border_left' align='center'>".$thisText."</td>";
				
				### 1st Term
				$thisText = ($withStar==0)? '&nbsp;' : '****';
				$x .= "<td class='tabletext border_left' align='center'>".$thisText."</td>";
				
				### 2nd Term
				$thisText = ($withStar==0)? '&nbsp;' : '****';
				$x .= "<td class='tabletext border_left' align='center'>".$thisText."</td>";
				
				### Average
				$thisText = ($withStar==0)? '&nbsp;' : '****';
				$x .= "<td class='tabletext border_left' align='center'>".$thisText."</td>";
				
				### Rank
				$thisText = '&nbsp;';
				$x .= "<td class='tabletext'>".$thisText."</td>";
			}
			else
			{
				$thisText = ($withStar==0)? '&nbsp;' : '*********';
				$x .= '<tr>';
					$x .= '<td class="text_14px">'.$thisText.'</td>';
					$x .= '<td class="text_14px">&nbsp;</td>';
				$x .= '</tr>';
			}
			
			
		$x .= '</tr>';
		
		return $x;
	}
	
	function Get_Grand_Info_Cells($TitleEn, $TitleCh, $FullMark, $OverallValue, $ValurArr, $ReportType, $Border_Top=0)
	{
		global $eReportCard;
		
		$css_border_top = ($Border_Top==0)? '' : 'border_top';
		
		$x = '';
		$x .= '<td class="border_left '.$css_border_top.'">';
				$x .= '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
					$x .= '<tr>';
						$x .= '<td class="tabletext" nowrap>'.$TitleEn.'</td>';
						$x .= '<td class="tabletext" align="right" nowrap>'.$TitleCh.'</td>';
					$x .= '</tr>';
				$x .= '</table>';
			$x .= '</td>';
			
		$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$FullMark.'</td>';
		
		if ($ReportType == 'T')
		{
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$OverallValue.'</td>';
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$this->EmptySymbol.'</td>';
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center" colspan="2">'.$OverallValue.'</td>';
		}
		else
		{
			foreach ($ValurArr as $ColumnID => $ColumnValue)
			{
				if ($ColumnValue == '')
					$ColumnValue = '&nbsp;';
				$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$ColumnValue.'</td>';
			}
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center" colspan="2">'.$OverallValue.'</td>';
		}
		
		return $x;
	}
	
	function Get_CSV_Info_Cells($TitleEn, $TitleCh, $InfoKey, $ValurArr, $ReportType, $Semester, $ColumnInfoArr)
	{
		global $eReportCard;
		
		$TitleCh = str_replace(' ', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $TitleCh);
		
		$x = '';
		$x .= '<td class="border_left '.$css_border_top.'">';
				$x .= '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
					$x .= '<tr>';
						$x .= '<td class="tabletext" nowrap>'.$TitleEn.'</td>';
						$x .= '<td class="tabletext" align="right" nowrap>'.$TitleCh.'</td>';
					$x .= '</tr>';
				$x .= '</table>';
			$x .= '</td>';
			
		$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$this->EmptySymbol.'</td>';
		
		if ($ReportType == 'T')
		{
			$thisValue = $ValurArr[$Semester][$InfoKey];
			$thisValue = ($thisValue=='')? $this->EmptySymbol : $thisValue;
			
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$thisValue.'</td>';
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$this->EmptySymbol.'</td>';
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center" colspan="2">'.$this->EmptySymbol.'</td>';
		}
		else
		{
			$numOfColumn = count($ColumnInfoArr);
			for ($i=0; $i<$numOfColumn; $i++)
			//foreach($ColumnInfoArr as $thisSemester => $thisDataArr)
			{
				$thisSemester = $ColumnInfoArr[$i]['SemesterNum'];
				
				if ($thisSemester == 0)
					continue;
				
				$thisValue = $ValurArr[$thisSemester][$InfoKey];
				$thisValue = ($thisValue=='')? $this->EmptySymbol : $thisValue;
				$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center">'.$thisValue.'</td>';
			}
			
			$x .= '<td class="border_left '.$css_border_top.' tabletext" align="center" colspan="2">'.$this->EmptySymbol.'</td>';
		}
		
		return $x;
	}
	
	function Get_Grading_Table()
	{
		global $eReportCard;
		
		$x = '';
		
		$x .= '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
			$x .= '<tr>';
				$x .= '<td class="tabletext"><u>'.$eReportCard['Template']['GradingEn'].'</u></td>';
				$x .= '<td class="tabletext"><u>'.$eReportCard['Template']['GradingCh'].'</u></td>';
			$x .= '</tr>';
			
			$GradeArr = $eReportCard['Template']['GradingArr'];
			foreach ($GradeArr as $thisGrade => $thisTitleArr)
			{
				$x .= '<tr>';
					$x .= '<td class="tabletext">'.$thisGrade.' - '.$thisTitleArr['En'].'</td>';
					$x .= '<td class="tabletext">'.$thisTitleArr['Ch'].'</td>';
				$x .= '</tr>';
			}
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Fail_NotApplicable_Table()
	{
		global $eReportCard;
		
		$x = '';
		
		$x .= '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
			$x .= '<tr>';
				$x .= '<td class="tabletext" align="center" width="25">*</td>';
				$x .= '<td class="tabletext" align="left">'.$eReportCard['Template']['FailedEn'].' ('.$eReportCard['Template']['FailedCh'].')</td>';
			$x .= '</tr>';
			$x .= '<tr>';
				$x .= '<td class="tabletext" align="center">'.$this->EmptySymbol.'</td>';
				$x .= '<td class="tabletext" align="left">'.$eReportCard['Template']['NotApplicableEn'].' ('.$eReportCard['Template']['NotApplicableCh'].')</td>';
			$x .= '</tr>';
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Mark_Display_Td($Mark, $Nature='', $ReportID='')
	{
		if ($Nature=='Fail') {
			//2012-0215-1040-38132
			//$Symbol = '*';
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID); 
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
			
			$Symbol = ($FormNumber==6)? '&nbsp;' : '*';
		}
		else {
			$Symbol = '&nbsp;';
		}
		
		$MarkDisplay = '';
		$MarkDisplay .= '<div style="width:100%" align="center">';
			$MarkDisplay .= '<div style="width:60%;text-align:right;float:left">'.$Mark.'</div>';
			$MarkDisplay .= '<div style="width:40%;text-align:left;float:left">'.$Symbol.'</div>';
		$MarkDisplay .= '</div>';
		
		return $MarkDisplay;
	}
	
	function Display_Footer_Data_Cell($DataArr, $Index, $ReportType, $ColumnInfoArr)
	{
		$x = '';
		if (isset($DataArr[$Index]))
		{
			$x = $DataArr[$Index];
		}
		else
		{
			$x .= '<td class="border_left">';
					$x .= '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
						$x .= '<tr>';
							$x .= '<td class="tabletext" nowrap>&nbsp;</td>';
							$x .= '<td class="tabletext" align="right" nowrap>&nbsp;</td>';
						$x .= '</tr>';
					$x .= '</table>';
				$x .= '</td>';
				
			$x .= '<td class="border_left tabletext" align="center">&nbsp;</td>';
			
			if ($ReportType == 'T')
			{
				$x .= '<td class="border_left tabletext" align="center">&nbsp;</td>';
				$x .= '<td class="border_left tabletext" align="center">&nbsp;</td>';
				$x .= '<td class="border_left tabletext" align="center" colspan="2">&nbsp;</td>';
			}
			else
			{
				$numOfColumn = count($ColumnInfoArr);
				for ($i=0; $i<$numOfColumn; $i++)
				//foreach($ColumnInfoArr as $thisSemester => $thisDataArr)
				{
					$thisSemester = $ColumnInfoArr[$i]['SemesterNum'];
					
					if ($thisSemester == 0)
						continue;
					
					$x .= '<td class="border_left tabletext" align="center">&nbsp;</td>';
				}
				
				$x .= '<td class="border_left tabletext" align="center" colspan="2">&nbsp;</td>';
			}
		}
		
		return $x;
	}
}
?>