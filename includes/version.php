<?php

# version log

######################################## array ######################################
################# Format: version #, time of deployment, remark #####################
################# DECENDING order, show first entry $versions[0] ####################
#####################################################################################
//$versions[] = array("ip.2.5.0.9.2", "2009-09-01 17:31", ""); 

$versions[] = array("ip.3.0.1.11.1.0", "2020-11-25 16:28", "full version");
$versions[] = array("ip.3.0.1.8.1.6", "2020-10-21 14:00", "full version - v8.1 CD6");
$versions[] = array("ip.3.0.1.8.1.5", "2020-09-24 17:45", "full version - v8.1 CD5");
$versions[] = array("ip.3.0.1.8.1.4", "2020-09-14 15:00", "full version - v8.1 CD4");
$versions[] = array("ip.3.0.1.8.1.3", "2020-08-28 10:45", "full version - v8.1 CD3");
$versions[] = array("ip.3.0.1.8.1.2", "2020-08-14 14:31", "full version - v8.1 CD2");
$versions[] = array("ip.3.0.1.8.1.1", "2020-08-12 14:35", "full version - v8.1 CD1");
$versions[] = array("ip.3.0.1.8.1.0", "2020-08-07 14:06", "full version");


if (!$JustWantVersionData)
{
	if (isset($blcheck) && $blcheck)
	{
	  # display to BL staff when requests
		echo "Version: <b>" . $versions[0][0] ."</b><br />";
		echo "DateTime: <b>" . $versions[0][1] ."</b><br />";
		echo "Remark: <b>" . $versions[0][2] ."</b><br />";
	} else
	{
	   # display to end user
		echo "Version: " . $versions[0][0] ."<br />";
	}
}

?>
