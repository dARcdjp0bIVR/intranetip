<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
class libies_survey_email extends libies {
	public $canSendMail = false;

	public $GAMMA_MAIL_SETTINGS = array("ACCESS_RIGHT"=>false,"INTERNAL"=>false,"EXTERNAL"=>false);
	public $IMAIL_SETTINGS = array("ACCESS_RIGHT"=>false,"INTERNAL"=>false,"EXTERNAL"=>false);

	public $campusMailList;	// Send by U[userid] / X[someid] e.g. U1934, S3
	private $pureCampusMailList;
	public $webMailList;

	public $rawCampusMailList;	// a string: ""SMAX (5A-1)" <Uxxx>;"SMAY (5A-2)" <Uxxx>;"
	public $rawWebMailList;	// a string: ""SMAX (5A-1)" <email@email.com1>;"SMAY (5A-2)" <email@email.com1>;"

	private $imap_gamma;
	private $libwebmail;
	private $libcampusmail;

	public $surveyID;
	public $surveyEmailID;
	public $mailTitle;
	public $mailContent;

	const IS_COMPOSE_PUREMAILLIST = true;
	const IS_SET_RAW = true;
	public function libies_survey_email() {
		global $UserID;
		$this->libies();
		$this->libwebmail = new libwebmail();

		if ($this->isGammaMail()) {
			$this->imap_gamma = new imap_gamma();
			$this->GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"] = true;
			$this->GAMMA_MAIL_SETTINGS["INTERNAL"] = true;
			$this->GAMMA_MAIL_SETTINGS["EXTERNAL"] = $this->imap_gamma->AccessInternetMail();
		} else if ($this->isIMail()) {
			$this->IMAIL_SETTINGS["ACCESS_RIGHT"] = true;
			$this->libcampusmail = new libcampusmail();
			if ($this->libwebmail->has_webmail && $this->libwebmail->hasWebmailAccess($UserID) && $this->libwebmail->type == 3)
			{
			    $noWebmail = false;
			}
			else
			{
			    $noWebmail = true;
			}
			$this->IMAIL_SETTINGS["INTERNAL"] = !$this->libcampusmail->usage_internal_disabled;
			$this->IMAIL_SETTINGS["EXTERNAL"] = !($noWebmail || $this->libcampusmail->usage_external_disabled);
		}

		$this->canSendMail = ( auth_sendmail()
								&& ($this->GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"]
									|| ($this->IMAIL_SETTINGS["ACCESS_RIGHT"]
										&&($this->IMAIL_SETTINGS["INTERNAL"] || $this->IMAIL_SETTINGS["EXTERNAL"])
										)
									)
								);
		$this->campusMailList = array();
		$this->webMailList = array();
	}


	public function getEmailInBlanket($ParEmailDisplay) {
		return trim($this->getEmailFromEmailDisplay($ParEmailDisplay));
	}
	public function getUIDInBlanket($ParEmailDisplay) {
		return trim($this->getEmailFromEmailDisplay($ParEmailDisplay,false));
	}

	public function isGammaMail() {
		global $plugin,$special_feature;
		return $plugin['imail_gamma']
				&& !empty($_SESSION['SSV_EMAIL_LOGIN'])
				&& !empty($_SESSION['SSV_LOGIN_EMAIL'])
				&& (
					$special_feature['forCharles']
					|| $special_feature['imail']!==true
					|| $plugin['imail_gamma']
					);
	}

	public function isIMail() {
		global $plugin,$access2webmail,$access2campusmail;
		return (
					($_SESSION["SSV_PRIVILEGE"]["special_feature"]['imail'])
					|| ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"])
					|| ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"])
					|| ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"])
				)
				&& $plugin['imail_gamma']!==true;
	}

	/**
	 * Save the email content to DB
	 * To save email need the following in this object to be set
	 * [optional] rawCampusMailList
	 * [optional] rawWebMailList
	 * surveyID
	 * [optional] mailTitle
	 * [optional] mailContent
	 */
	public function saveEmail($ParSendStatus) {
		global $ies_cfg;

		if (!empty($this->rawCampusMailList)) {
			$idInBlanket = array_map(array($this,"getUIDInBlanket"),array_filter(explode(";",$this->rawCampusMailList),array($this,"filterEmpty")));
			$UserInfoArray = $this->libcampusmail->returnRecipientUserIDArrayWithQuota(implode(",",$idInBlanket));
			if (is_array($UserInfoArray)) {
				foreach($UserInfoArray as $key => $element) {
					$userId = $element[0];
					$userDisplayName = $element[1];
					$tempResult = $this->newSurveyEmailContents($this->surveyID,($userDisplayName." <".$userId.">"),$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']);	// TODO: still called as respondentnature, but here is used to distinguish by userid or by mail, how to refine it?

					if($tempResult) {
						$this->updateEmailStatus($this->surveyID,$userId,$ParSendStatus,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']);
					}
					$updateResult["INTERNAL"][] = $tempResult;
				}
			}
		}

		if (!empty($this->rawWebMailList)) {
			$fullEmailWithDisplayNameArray = array_filter(explode(";",$this->rawWebMailList),array($this,"filterEmpty"));
			$fullEmailWithDisplayNameArray = array_map("trim",$fullEmailWithDisplayNameArray);
			if (is_array($fullEmailWithDisplayNameArray)) {
				foreach($fullEmailWithDisplayNameArray as $key => $fullEmailWithDisplayName) {
					$email = $this->getEmailInBlanket($fullEmailWithDisplayName);
//					debug_r($email);
					$tempResult = $this->newSurveyEmailContents($this->surveyID,$fullEmailWithDisplayName,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);	// TODO: still called as respondentnature, but here is used to distinguish by userid or by mail, how to refine it?

					if($tempResult) {
//						DEBUG("HERE");


						$this->updateEmailStatus($this->surveyID,$email,$ParSendStatus,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);
					}
					$updateResult["EXTERNAL"][] = $tempResult;
				}

			}
		}
		$updateResult["EMAIL_CONTENT"] = $this->updateSurveryEmailContent($this->surveyID, $this->mailTitle, $this->mailContent);
		return $updateResult;
	}


	/**
	 * @param String $ParCampusMailListRaw - "SMAX <Uxxx>; SMAX1 <Uyyy>"
	 * @param boolean $ParIsSetRaw - necessary to set for function saveEmail to get the raw list
	 * 			Use constant IS_SET_RAW
	 * @param boolean $ParIsComposePureMailList - true to set pureCampusMailList like array(xxx,yyy)
	 * 			Use constant IS_COMPOSE_PUREMAILLIST
	 */
	public function setCampusMailList($ParCampusMailListRaw, $ParIsSetRaw = true, $ParIsComposePureMailList = false) {
		$ParCampusMailListRaw = str_replace(",",";",$ParCampusMailListRaw);
		$campusMailList = array_map(array($this,"getUIDInBlanket"),array_filter(explode(";",$ParCampusMailListRaw)));
		$this->campusMailList = $campusMailList;
		if ($ParIsComposePureMailList) {
			$this->pureCampusMailList = $this->extractIMailUserID($this->campusMailList);
		}
		if ($ParIsSetRaw) {
			$this->rawCampusMailList = $ParCampusMailListRaw;
		}
	}
	function filterEmpty($ParString) {
		$str = trim($ParString);
		return(!empty($str));
	}
	/**
	 * @param String $ParCampusMailListRaw - ""SMAX" <max@xx.com>; "SMAX1 <max@yy.com>"
	 * @param boolean $ParIsSetRaw - necessary to set for function saveEmail to get the raw list
	 */
	public function setWebMailList($ParWebMailListRaw, $ParIsSetRaw = true) {
		$webMailList = $this->returnPureMailListFromRaw($ParWebMailListRaw);
		$this->webMailList = $webMailList;
		if ($ParIsSetRaw) {
			$this->rawWebMailList = $ParWebMailListRaw;
		}
	}

	/**
	 * @param String $ParCampusMailListRaw - ""SMAX" <max@xx.com>; "SMAX1 <max@yy.com>"
	 * @return array - a list of email
	 */
	public function returnPureMailListFromRaw($ParWebMailListRaw) {
		$ParWebMailListRaw = str_replace(",",";",$ParWebMailListRaw);
		$webMailList = array_map(array($this,"getEmailInBlanket"),array_filter(explode(";",$ParWebMailListRaw),array($this,"filterEmpty")));
		return $webMailList;
	}
	/**
	 * Send email out including for iMail / Gamma Mail
	 */
	public function sendEmail() {
		if ($this->GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"]) {
				$this->sendEmailByEmailAddresses($this->webMailList, $this->mailTitle, $this->mailContent);
		} else if ($this->IMAIL_SETTINGS["ACCESS_RIGHT"]) {
			if ($this->IMAIL_SETTINGS["INTERNAL"]) {
				$this->sendEmailByUserIds($this->pureCampusMailList, $this->mailTitle, $this->mailContent);
			}
			if ($this->IMAIL_SETTINGS["EXTERNAL"]) {
				$this->sendEmailByEmailAddresses($this->webMailList, $this->mailTitle, $this->mailContent);
			}
		}
	}
	/**
	 * Send Email by userids by using libwebmail->sendModuleMail
	 */
	public function sendEmailByUserIds($ParRecipient, $ParTitle, $ParContent) {
		global $ies_cfg;
		if (is_array($ParRecipient)) {
			foreach($ParRecipient as $key => $userId) {
				$contentToSend = $this->returnCustomizedContent($userId,$ParContent,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']);
				$emailElement[] = $ToArray = array($userId);
				$emailElement[] = $Subject = $ParTitle;
				$emailElement[] = $Message = $contentToSend;
				$emailElement[] = $UseAdminEmailAsSender = 0;
				$sendResult[] = $this->libwebmail->sendModuleMail($ToArray,$Subject,$Message,$UseAdminEmailAsSender);
			}
		}
		return $sendResult;
	}

	public function returnSender() {
		global $iMail_Alias_Domain,$webmail_info,$UserID;

		$libuser = new libuser($UserID);

		# get the sender name
		$sql = "SELECT ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE UserID = '$UserID'";
		$StudentClassInfo = $this->returnVector($sql);

		if(sizeof($StudentClassInfo)>0){
	 		$sender_name = $StudentClassInfo[0];
	 	}else{
	 		$sender_name = $libuser->UserName();
		}

		if ($this->GAMMA_MAIL_SETTINGS["ACCESS_RIGHT"]) {
			$sender = "\"".$sender_name."\" <".$libuser->ImapUserEmail.">";
		} else if ($this->IMAIL_SETTINGS["ACCESS_RIGHT"]) {
	         # for undelivery
	         $email_return_path = $libuser->UserLogin."@".($iMail_Alias_Domain ? $webmail_info['aliasdomain'] : $this->libwebmail->mailaddr_domain);

	         if ($sender_name != "")
	         {

	             $sender = "\"".$sender_name."\" <".$email_return_path.">";
	         }
	         else
	         {
	             $sender = $email_return_path;
	         }
		}
		return $sender;
	}
	public function sendEmailByEmailAddresses($ParRecipient, $ParTitle, $ParContent) {
		global $ies_cfg;
		if (is_array($ParRecipient)) {
			$ParRecipient = array_unique($ParRecipient);
			foreach($ParRecipient as $key => $email) {
				$contentToSend = $this->returnCustomizedContent($email,$ParContent,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);
				$emailElement[] = $From = $this->returnSender();
			  	$emailElement[] = $To = array($email);
			  	$emailElement[] = $CC = null;
			  	$emailElement[] = $BCC = null;
			  	$emailElement[] = $Subject = $ParTitle;
			  	$emailElement[] = $Message = $contentToSend;
			  	$emailElement[] = $Attachment = null;
			  	$emailElement[] = $Important = false;
			  	$emailElement[] = $MailReturnPath = "";
			  	$emailElement[] = $ReplyAddress = "";
			  	$emailElement[] = $MultipartMessage = true;
			  	$sendResult[] = $this->libwebmail->SendMail($Subject,$Message,$From,$To,$CC,$BCC,$Attachment,$Important,$MailReturnPath,$MailReturnPath,$ReplyAddress,$MultipartMessage);
			}
		}

		return $sendResult;
	}
	private function returnCustomizedContent($ParEmailOrUserID,$ParContent,$ParRespondentNature="") {
		global $ies_cfg;
		# preset the respondentNature to send external (send by email address)
		if (empty($ParRespondentNature)) {
			$ParRespondentNature = $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL'];
		}
		$key = base64_encode("SurveyID={$this->surveyID}&email=$ParEmailOrUserID&respondentNature=$ParRespondentNature");	// TODO: what should be the respondentNature finally? still necesssary?
		$customizedContent = str_replace("{{{ Q_KEY }}}", $key, stripslashes($ParContent));
//		debug_r(array($ParEmailOrUserID,$ParContent,$ParRespondentNature));
//		debug_r(htmlspecialchars($customizedContent));
		return $customizedContent;
	}
	function extractIMailUserID($ParRecipients) {
		$UserIDArray = array();
		if (is_array($ParRecipients)) {
			$UserInfoArray = $this->libcampusmail->returnRecipientUserIDArrayWithQuota(implode(",",array_filter($ParRecipients)));
			$UserIDArray = array_map(array($this,"extractPureUserIDArray"),$UserInfoArray);
		}
		return $UserIDArray;
	}
	function extractPureUserIDArray($ParUserInfoArray) {
		return $ParUserInfoArray[0];
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/compose_email.php
	 * to get the html of the gamma mail internal selection box
	 * @param imap_gamma $imap_gamma
	 * @return String - HTML
	 */
	function getGammaMailInternalHtml($imap_gamma) {
		$EXTERNAL = "EXTERNAL";
		$emailHtmlShell = $this->getEmailHtmlShell();
		$temp_internet_mail_access = $imap_gamma->internet_mail_access;	// for restore
		$TOTYPE = "";

		#### IES Wordins Handling, override intranet words tempararily
		#
		#
		global $Lang;
		$Lang['Gamma']['SelectFromInternalRecipient'] = $Lang['IES']['Email']['Gamma']['SelectFromInternalRecipient'];
		$Lang['Gamma']['SelectFromExternalRecipient'] = $Lang['IES']['Email']['Gamma']['SelectFromExternalRecipient'];
		$Lang['Gamma']['SelectFromExternalRecipientGroup'] = $Lang['IES']['Email']['Gamma']['SelectFromExternalRecipientGroup'];
		$Lang['Gamma']['SelectFromInternalRecipientGroup'] = $Lang['IES']['Email']['Gamma']['SelectFromInternalRecipientGroup'];
		#
		#
		#### IES Wordins Handling, override intranet words tempararily
		$gammaMailSelectionBoxInternal = $imap_gamma->Get_AddressBook_Select_Toolbar("ToAddress_$EXTERNAL","",$EXTERNAL);


		$returnString = str_replace(array("{{{ HTML_EMAIL_ADD }}}","{{{ int/ext }}}","{{{ HTML_EMAIL_TO }}}"), array($gammaMailSelectionBoxInternal,$EXTERNAL,$TOTYPE), $emailHtmlShell);
		return $returnString;
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/compose_email.php
	 * to get the html of the iMail internal selection box
	 * @return String - HTML
	 */
	function getIMailInternalHtml() {
		global $i_frontpage_campusmail_choose,$i_CampusMail_New_SelectFromAlias,$image_path,$LAYOUT_SKIN,$PATH_WRT_ROOT,$i_frontpage_campusmail_remove,$Lang;
		$INTERNAL = "INTERNAL";
		$emailHtmlShell = $this->getEmailHtmlShell();
		$TOTYPE = "(".$Lang['IES']['Email']['IntranetRecipient(s)'].")";

		$htmlInternalEmailAdd = <<<HTMLEND
			<br/>
			<textarea id="ToAddress_$INTERNAL" name="ToAddress_$INTERNAL" style="display:none"></textarea>
			<div id="internalToTextDiv">
				<select name="Recipient[]" size="5" style="width: 50%" multiple>
				</select>
			</div>
			<br/>
			<div id="internalToRemoveBtnDiv">
				<a class="iMailsubject" href="javascript:newWindow('{$PATH_WRT_ROOT}/home/imail/choose/alias_in.php?fieldname=Recipient[]',9)">
				<img width="20" height="20" align="absMiddle" alt="Select From Alias Group" src="/images/2009a/iMail/btn_alias_group.gif" border="0" complete="complete"/>
				{$Lang['IES']['Email']['IMail']['i_CampusMail_New_SelectFromAlias']}
				</a>
				<a href="javascript:newWindow('{$PATH_WRT_ROOT}/home/imail/choose/index.php?fieldname=Recipient[]',9)" class='iMailsubject' >
				<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_address_book.gif" alt="{$Lang['IES']['Email']['IMail']['i_frontpage_campusmail_choose']}" width="20" height="20" border="0" align="absmiddle" />
				{$Lang['IES']['Email']['IMail']['i_frontpage_campusmail_choose']}
				</a>
				<a href="javascript:removeInHiddenToAddress(document.form1.elements['Recipient[]']);checkOptionRemove(document.form1.elements['Recipient[]']);" class='iMailsubject' >
				<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_delete_selected.gif" alt="{$Lang['IES']['Email']['IMail']['i_frontpage_campusmail_remove']}" width="20" height="20" border="0" align="absmiddle" />
				{$Lang['IES']['Email']['IMail']['i_frontpage_campusmail_remove']}
				</a>
			</div>
HTMLEND;
		$returnString = str_replace(array("{{{ HTML_EMAIL_ADD }}}","{{{ int/ext }}}","{{{ HTML_EMAIL_TO }}}"), array($htmlInternalEmailAdd,$INTERNAL,$TOTYPE), $emailHtmlShell);
		return $returnString;
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/compose_email.php
	 * to get the html of the external selection box
	 * @return String - HTML
	 */
	function getExternalHtml() {
		global $i_frontpage_campusmail_choose,$i_CampusMail_New_SelectFromAlias,$image_path,$LAYOUT_SKIN,$PATH_WRT_ROOT,$i_frontpage_campusmail_remove,$Lang;
		$EXTERNAL = "EXTERNAL";
		$emailHtmlShell = $this->getEmailHtmlShell();
		$TOTYPE = "(".$Lang['IES']['Email']['ExternalRecipient(s)'].")";

		$htmlExternalEmailAdd = <<<HTMLEND
			<br/>
			<div id="externalToTextDiv">
			<textarea id="ExternalTo" ROWS=4 name="ExternalTo"></textarea>
			<textarea id="ToAddress_$EXTERNAL" ROWS=4 name="ToAddress_$EXTERNAL" style="display:none"></textarea>
			</div>
			<div id="externalToRemoveBtnDiv">
			<a href="javascript:newWindow('{$PATH_WRT_ROOT}/home/imail/choose_ex/alias_ex.php?fieldname=ExternalTo',9)" class='iMailsubject' >
			<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_alias_group.gif" alt="{$Lang['IES']['Email']['IMail']['i_CampusMail_New_SelectFromAlias']}" width="20" height="20" border="0" />
			{$Lang['IES']['Email']['IMail']['i_CampusMail_New_SelectFromAlias']}
			</a>
			<img src="{$image_path}/{$LAYOUT_SKIN}/10x10.gif" />
			<a href="javascript:newWindow('{$PATH_WRT_ROOT}/home/imail/choose_ex/index.php?fieldname=ExternalTo',9)" class='iMailsubject' >
			<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_address_book.gif" alt="{$Lang['IES']['Email']['IMail']['FieldTitle']['ChooseFromExternalRecipient']}" width="20" height="20" border="0" />
			{$Lang['IES']['Email']['IMail']['FieldTitle']['ChooseFromExternalRecipient']}
			</a><img src="{$image_path}/{$LAYOUT_SKIN}/10x10.gif" />
			<a href="javascript:newWindow('{$PATH_WRT_ROOT}/home/imail/choose_ex/internal_recipient.php?fieldname=ExternalTo',9)" class='iMailsubject' >
			<img src="{$image_path}/{$LAYOUT_SKIN}/iMail/btn_address_book.gif" alt="{$Lang['IES']['Email']['IMail']['FieldTitle']['ChooseFromInternalRecipient']}" width="20" height="20" border="0" />
			{$Lang['IES']['Email']['IMail']['FieldTitle']['ChooseFromInternalRecipient']}
			</a>
			</div>
HTMLEND;
		$returnString = str_replace(array("{{{ HTML_EMAIL_ADD }}}","{{{ int/ext }}}","{{{ HTML_EMAIL_TO }}}"), array($htmlExternalEmailAdd,$EXTERNAL,$TOTYPE), $emailHtmlShell);
		return $returnString;
	}
	/**
	 * The function is used by /home/web/eclass40/intranetIP25/home/eLearning/ies/survey/mail/compose_email.php
	 * to get the html of the email selection box
	 * @return String - HTML
	 */
	function getEmailHtmlShell() {
		global $Lang;
		$emailHtmlShell = <<<HTMLEND
		<table border=0 class="form_table" style="width:100%;font-size:15px; padding-top:0; margin-top:-8px; *margin-top:-14px">
			<col class="field_title" />
			<tr>
				<td style="width:100%">
			{$Lang['IES']['Email']['AddEmail(s)Here']} <span style="color:grey">{{{ HTML_EMAIL_TO }}}</span>
					<br/>
					{{{ HTML_EMAIL_ADD }}}
				</td>
			</tr>
			</table>
HTMLEND;
		return $emailHtmlShell;
	}
}
?>