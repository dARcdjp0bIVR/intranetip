<?php
/** [Modification Log] Modifying By:fai
 * *******************************************
 * 2011-01-25	Thomas	(201101251730)
 * -	Modified function genTableWithStyle(), Display Question title input box if focus question is checked
 *
 * 2011-01-11	Thomas	(201101111418)
 * -	Add var $display_survey_mapping_option
 * -	Add function set_display_survey_mapping_option(), get_display_survey_mapping_option()
 * *******************************************
 */
class libies_survey extends libies {

	var $display_survey_mapping_option = true;

	public function analyzeXandY($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		global $ies_cfg;
		$xType = $ParQuestionElements[$ParXQuestionNumber]["TYPE"];
		$yType = $ParQuestionElements[$ParYQuestionNumber]["TYPE"];
		if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMC_vs_MC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMC_vs_MultiMC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MultiMC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMultiMC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeLC_vs_MC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeLC_vs_MultiMC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeLC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		}
		return $returnArray;
	}

	public function analyzeXandY2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		global $ies_cfg;
		$x_questionArray = explode(".",$ParXQuestionNumber);
		$x_question_question = $x_questionArray[0];
		$y_questionArray = explode(".",$ParYQuestionNumber);
		$y_question_question = $y_questionArray[0];



		$xType = $ParQuestionElements[$x_question_question]["TYPE"];
		$yType = $ParQuestionElements[$y_question_question]["TYPE"];
//		////debug_r($xType);
//		////debug_r($yType);
		if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMC_vs_MC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMC_vs_MultiMC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMC_vs_LC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeMultiMC_vs_MultiMC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeMultiMC_vs_LC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			}
		} else if ($xType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
			if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]) {
				$returnArray = $this->analyzeLC_vs_MC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]) {
				$returnArray = $this->analyzeLC_vs_MultiMC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
			} else if ($yType == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
				$returnArray = $this->analyzeLC_vs_LC2($ParTableName,$x_question_question,$y_question_question,$ParQuestionElements);
//				$returnArray = $this->analyzeLC_vs_LC($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
			}
		}
		return $returnArray;
	}

	public function analyzeXorY2($ParTableName,$ParQuestionNumber,$ParQuestionElements) {
  		global $ies_cfg;
//  		////debug_r($ParQuestionElements);
//  		echo "over here";
		switch((int)$ParQuestionElements[$ParQuestionNumber]["TYPE"]) {
			case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
				$returnArray = $this->analyzeMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);

				break;
			case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
				$returnArray = $this->analyzeMultiMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);

				break;
			# this case wont happen since the LS will be tuned as MC to process
//			case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
//				$returnArray = $this->analyzeLC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);
//				break;
		}


  		return $returnArray;
	}

	public function analyzeMC2($ParTableName,$ParXQuestionNumber,$ParQuestionElements) {
//		$options = $ParQuestionElements[$ParQuestionNumber]["OPTIONS"];
////		////debug_r($options);
//		if (is_array($options)) {
//			$resultArray = array();
//			foreach($options as $optionsKey => $optionsElement) {
//				$sql = "SELECT '[".$optionsElement."]' AS OPT_POS,COUNT(*) AS VALUE FROM $ParTableName WHERE INSTR(Q_$ParQuestionNumber,'[".$optionsElement."]')";
////				////debug_r($sql);
//				$resultArray = array_merge($resultArray,$this->returnArray($sql));
//			}
//		}
//		$returnArray = $resultArray;
//		return $returnArray;
////debug_r($ParQuestionElements[$ParXQuestionNumber]["OPTIONS"]);
		$X_options = $ParQuestionElements[$ParXQuestionNumber]["OPTIONS"];

		if (is_array($X_options)) {
			$resultArray = array();
			foreach($X_options as $X_optionsKey => $X_optionsElement) {
				$X_optionsElement = addslashes($X_optionsElement);
				$sql = "SELECT '[$X_optionsElement]' AS OPT_POS_1,COUNT(*) AS VALUE FROM $ParTableName WHERE INSTR(Q_1,'[$X_optionsElement]')";
				$resultArray = array_merge($resultArray,$this->returnArray($sql));
			}
		}
		$returnArray = $resultArray;
		return $returnArray;
	}
	public function analyzeMultiMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC2($ParTableName,$ParQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC2($ParTableName,$ParQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		$X_options = $ParQuestionElements[$ParXQuestionNumber]["OPTIONS"];
		$Y_options = $ParQuestionElements[$ParYQuestionNumber]["OPTIONS"];

		if (is_array($X_options)) {
			$resultArray = array();
			foreach($X_options as $X_optionsKey => $X_optionsElement) {
				$X_optionsElement = addslashes($X_optionsElement);
				if (is_array($Y_options)) {
					foreach($Y_options as $Y_optionsKey => $Y_optionsElement) {
						$Y_optionsElement = addslashes($Y_optionsElement);
						$sql = "SELECT '[$X_optionsElement]' AS OPT_POS_1,'[$Y_optionsElement]'OPT_POS_2,COUNT(*) AS VALUE FROM $ParTableName WHERE ( INSTR(Q_1,'[$X_optionsElement]') AND INSTR(Q_2,'[$Y_optionsElement]') ) ";
						$resultArray = array_merge($resultArray,$this->returnArray($sql));
					}
				}
			}
		}
		$returnArray = $resultArray;
		return $returnArray;
	}
	public function analyzeMC_vs_MultiMC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMC_vs_LC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMultiMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMultiMC_vs_MultiMC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeMultiMC_vs_LC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC_vs_MultiMC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function analyzeLC_vs_LC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements) {
		return $this->analyzeMC_vs_MC2($ParTableName,$ParXQuestionNumber,$ParYQuestionNumber,$ParQuestionElements);
	}
	public function generateTableXandY($ParResultArray) {
		if (is_array($ParResultArray)) {
			foreach($ParResultArray as $ParResultArrayKey => $ParResultArrayElement) {
				$header[$ParResultArrayElement["OPT_POS_1"]] = $this->trimOptions($ParResultArrayElement["OPT_POS_1"]);;
				if (isset($tableContent[$ParResultArrayElement["OPT_POS_2"]])) {
					$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
				} else {
					$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $this->trimOptions($ParResultArrayElement["OPT_POS_2"]);
					$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
				}

			}
		}
		$content = "";
		if (is_array($tableContent)) {
			foreach($tableContent as $tableContentKey => $tableContentElement) {
				$sizeOfTableContentElement = count($tableContentElement);
				$content .= "<tr>";
				for($i=0;$i<$sizeOfTableContentElement;$i++) {
					if ($i>0) {
						$content .= "<td>".$tableContentElement[$i]."</td>";
					} else {
						$content .= "<th class='side_hd'>".$tableContentElement[$i]."</th>";
					}
				}
				$content .= "</tr>";
			}
		}
		$html .= "
			<table class='common_table_list'><tr><th><!--TITLE--></th><th class='top_hd'>".(is_array($header)?implode("</th><th class='top_hd'>",$header):"")."</th></tr>".$content."</table>
		";
		return $html;
	}
	public function generateTableXorY($ParResultArray) {
		////debug_r($ParResultArray);
			if (is_array($ParResultArray)) {
				$tableContent = array();
				foreach($ParResultArray as $key => $element) {
					$header[] = $this->trimOptions($element["OPT_POS"]);
					$tableContent[] = $element["VALUE"];
				}
			}
			$html .= "
				<table border=1><tr><th>".implode("</th><th>",$header)."</th></tr><tr><td>".implode("</td><td>",$tableContent)."</td></tr></table>
			";
		return $html;
	}
	public function trimOptions($ParOption) {
		////debug_r($ParOption);
		return substr($ParOption,strpos($ParOption,"[")+1,strpos($ParOption,"]")-strpos($ParOption,"[")-1);
	}
	public function returnTableHeaderArray($ParAxisNum,$ParQuestionTitle,$ParQuestionOptions=null) {
		if (count($ParQuestionOptions[$ParAxisNum])>0) {
			$headerArray = $ParQuestionOptions[$ParAxisNum];
		} else {
			$headerArray = $ParQuestionTitle[$ParAxisNum];
		}
		return $headerArray;
	}
	public function getSurveyQuestion($ParSurveyID) {

		global $ies_cfg,$intranet_db;
		$sql = "SELECT QUESTION FROM {$intranet_db}.IES_SURVEY WHERE SURVEYID = '$ParSurveyID' AND SURVEYTYPE = ".$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0];
		$returnResult = current($this->returnVector($sql));
		return $returnResult;
	}
	public function getSurveyAnswer($ParSurveyID,$ParIsValid=null) {
		global $ies_cfg,$intranet_db;
		if ($ParIsValid === true) {
			$checkValidCond = " AND ISA.STATUS = ".$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Valid'];
		} else if ($ParIsValid === false) {
			$checkValidCond = " AND ISA.STATUS = ".$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Invalid'];
		} else {
			$checkValidCond = "";
		}
		$sql = "SELECT ISA.ANSWER FROM {$intranet_db}.IES_SURVEY_ANSWER ISA
				INNER JOIN {$intranet_db}.IES_SURVEY ISU
					ON ISA.SURVEYID = ISU.SURVEYID
				WHERE ISU.SURVEYID = '$ParSurveyID'
					$checkValidCond";

		$returnResult = $this->returnVector($sql);
		return $returnResult;
	}
	public function getQuestionnaireTags($ParCurrentTag,$ParIsIESStudent) {

		$currentTag1 = $currentTag2 = $currentTag3 = "";
		${"currentTag".$ParCurrentTag} = " class=\"current\" ";
		if ($ParIsIESStudent) {
			$groupingTag = "<li $currentTag2><a href=\"#\" onClick=\"goGrouping()\">Grouping</a></li>";
		} else {
			// do nothing
		}
		$html = <<<HTMLEND
		    <ul class="q_tab">
		      <li $currentTag1><a href="#" onClick="goDisplay()">Overall</a></li>
		      {$groupingTag}
		      <li $currentTag3><a href="#" onClick="goGroupingList()">Grouping List</a></li>
		    </ul>
HTMLEND;
		return $html;
	}
	public function getSelectQuestionBox($ParSurveyID,$ParName="",$ParId="",$ParSelected=null,$ParShowNoCombine=false,$ParOnChangeAction="",$ParXAxis=null) {
		global $Lang,$ies_cfg;
//		error_log("getSelectQuestionBox -->".$ParSurveyID."\n", 3, "/tmp/aaa.txt");

		################################################
		##	Get the question elements
		$question = $this->getSurveyQuestion($ParSurveyID);	// This is the raw question
		$questionElements = $this->breakQuestionsString($question);
		################################################

		################################################
		##	Cannot do self mapping
		$ParSkipQuestionArray = array();
		$ParSkipQuestionArray[] = $ParXAxis;
		################################################

		################################################
		##	Construct options
		if (is_array($questionElements)) {
			$options = array();
			if ($ParShowNoCombine) {
				$options[] = "<option value=''>".$Lang['IES']['NoCombination']."</option>";
			}
			foreach($questionElements as $questionElementsKey => $questionElementsElement) {
				# Handle cannot map item
				#	- 1. those already put in $ParSkipQuestionArray
				#	- 2. question type that not available for analyze
				if (in_array($questionElementsKey,$ParSkipQuestionArray)
					|| !$this->isTypeForAnalyze($questionElementsElement["TYPE"])) {
					$options[] = "<optgroup label='".$questionElementsKey." ".$questionElementsElement["TITLE"]."' style='color:grey;'></optgroup>";
				} else {
					if ($questionElementsElement["TYPE"]==$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
						$subquestion = $questionElementsElement["QUESTION_QUESTIONS"];
						$options[] = "<optgroup label='".$questionElementsKey." ".$questionElementsElement["TITLE"]."'>";
						if (is_array($subquestion)) {
							foreach($subquestion as $subquestionKey => $subquestionElement) {
								# Handle the selected item
								$subquestionNumber = $questionElementsKey.".".($subquestionKey+1);
								$selectedStr = "";
								if ($ParSelected) {
									if ($subquestionNumber==$ParSelected) {
										$selectedStr = " selected='selected' ";
									}
								}
								if (in_array($subquestionNumber,$ParSkipQuestionArray)) {
									$options[] = "<optgroup label='".$subquestionNumber." ".$subquestionElement."'></optgroup>";
								} else {
									$options[] = "<option value='".$subquestionNumber."' $selectedStr>".$subquestionNumber." ".$subquestionElement."</option>";
								}

							}
						}
						$options[] = "</optgroup>";
					} else {
						# Handle the selected item
						$selectedStr = "";
						if ($ParSelected) {
							if (($questionElementsKey)==$ParSelected) {
								$selectedStr = " selected='selected' ";
							}
						}
						$options[] = "<option value='".$questionElementsKey."' $selectedStr>".$questionElementsKey." ".$questionElementsElement["TITLE"]."</option>";
					}

				}

			}
		}
		################################################

		################################################
		##	Last padding for selection
		if (count($options)>0) {
			$returnStr = "<select name='$ParName' id='$ParId' onChange='".$ParOnChangeAction."'>".implode("",$options)."</select>";
		} else {
			$returnStr = $Lang['IES']['NoRecordAtThisMoment'];
		}
		################################################
		return $returnStr;
	}

	public function getSurveyQuestions($ParSurveyID, $ParIsFilterQues=false) {
		global $ies_cfg;
		$surveyDetails = $this->getSurveyDetails($ParSurveyID);
		$surveyDetails = current($surveyDetails);
		$questions = array();
		if (is_array($surveyDetails)) {
			$questionElements = $this->breakQuestionsString($surveyDetails["Question"]);
			$sizeOfQuestionElements = count($questionElements);
			for($i=1;$i<=$sizeOfQuestionElements;$i++) {
				$questionElement = $questionElements[$i];
//				if ($ParIsFilterQues) {
//					if (!$this->isTypeForAnalyze($questionElement["TYPE"])) {
//						continue;
//					}
//				}
				$questions[$i] = $i.". ".$questionElement["TITLE"];
			}

		}
		return $questions;
	}

	function saveSurveyMapping($ParXAxis,$ParYAxis,$ParMappingTitle,$ParXTitle=null,$ParYTitle=null,$ParSurveyID,$ParUserID) {
		global $intranet_db;

		if (!empty($ParXAxis)) {
			$mappingCondX .= " XMAPPING=$ParXAxis ";
		} else {
			$mappingCondX .= " XMAPPING=NULL ";
		}

		if (!empty($ParYAxis)) {
			$mappingCondY .= " YMAPPING=$ParYAxis ";
		} else {
			$mappingCondY .= " YMAPPING=NULL ";
		}

		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID='$ParSurveyID' AND USERID='$ParUserID' AND ".str_replace("=NULL"," IS NULL ",$mappingCondX)." AND ".str_replace("=NULL"," IS NULL ",$mappingCondY);
//		////debug_r($sql);

		$surveyMappingID = current($this->returnVector($sql));

		$setCond .= " MAPPINGTITLE='$ParMappingTitle' ";
		if (!empty($ParXTitle)) {
			$setCond .= ",XTITLE='$ParXTitle' ";
		}
		if (!empty($ParYTitle)) {
			$setCond .= ",YTITLE='$ParYTitle' ";
		}
		if (!empty($surveyMappingID)) {
			$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING SET $setCond WHERE SURVEYMAPPINGID = '$surveyMappingID'";
		} else {
			$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_MAPPING SET $setCond, $mappingCondX,$mappingCondY,SURVEYID='$ParSurveyID',USERID='$ParUserID',DATEINPUT = NOW() ";

		}
//		////debug_r($sql);
		$result = $this->db_db_query($sql);
		if ($result) {
			if (!empty($surveyMappingID)) {
				$result = $surveyMappingID;
			} else {
				$result = mysql_insert_id();
			}
		}
		return $result;
	}

	public function loadSurveyMapping($ParSurveyID,$ParUserID,$ParSurveyMappingID=null) {
		global $intranet_db;
		if (!empty($ParSurveyMappingID)) {
			$cond = " SURVEYMAPPINGID = $ParSurveyMappingID ";
		} else {
			$cond = " SURVEYID = $ParSurveyID AND USERID = $ParUserID ";
		}


		$sql = "SELECT SURVEYMAPPINGID,SURVEYID,USERID,XMAPPING,YMAPPING,XTITLE,YTITLE,DATEINPUT,DATEMODIFIED,MAPPINGTITLE,COMMENT,SEQUENCE
				FROM {$intranet_db}.IES_SURVEY_MAPPING
				WHERE $cond ORDER BY SEQUENCE";
//error_log($sql."\n", 3, "/tmp/aaa.txt");

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function breakQuestionsString($ParQuestionsString) {
		global $ies_cfg;
		$questionsArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question"],$ParQuestionsString);
		array_shift($questionsArray);
		if (is_array($questionsArray)) {
			foreach($questionsArray as $questionsArrayKey => $questionsArrayElement) {
				$questionNumber = $questionsArrayKey+1;
				$questionElements[$questionNumber] = libies::extractQuestionElement($questionsArrayElement);
			}
		}
		return $questionElements;
	}

	public function isTypeForAnalyze($ParType) {
		global $ies_cfg;
		return in_array($ParType,$ies_cfg["Questionnaire"]["ValidTypeForAnalyze"]);
	}


	public function getAnalysisResults2($ParQuestionElements,$ParBrokenAnswerArray, $ParXAxis,$ParYAxis="") {
		//debug_r($ParXAxis);
		//debug_r($ParYAxis);

		if (!empty($ParXAxis)||!empty($ParYAxis)) {
			$tableName = "temp_survey_answers";
			$answerTableResult = $this->createAnswerTempTable3($tableName,$ParQuestionElements,$ParBrokenAnswerArray, $ParXAxis, $ParYAxis);
		} else {
			return false;
		}
		$x_result = array();
		$y_result = array();
		$twod_result = array();
		if ($answerTableResult) {
			if (!empty($ParXAxis)) {
				$x_result = $this->analyzeXorY2($tableName,$ParXAxis,$ParQuestionElements);
			}
			if (!empty($ParYAxis)) {
				$y_result = $this->analyzeXorY2($tableName,$ParYAxis,$ParQuestionElements);
			}
			if (!empty($ParXAxis)&&!empty($ParYAxis)) {
				$twod_result = $this->analyzeXandY2($tableName,$ParXAxis,$ParYAxis,$ParQuestionElements);
			}
		}
		$returnArray = array("X_RESULT"=>$x_result,"Y_RESULT"=>$y_result,"2D_RESULT"=>$twod_result);
		return $returnArray;

	}


	/* duplicated in libies for this moment */
	public function createAnswerTempTable3($tableName,$questionsArray,$answer,$ParXAxis,$ParYAxis) {
		global $ies_cfg;
		if (is_array($questionsArray)) {
			$sizeOfQuestionArray = count($questionsArray);
			$fieldStringArray = array();
			$fieldStringArray[] = "Q_1 mediumtext NULL";
			$fieldStringArray[] = "Q_2 mediumtext NULL";
			$fieldString = implode(",",$fieldStringArray);
		}

		$sql = <<<SQLEND
			create temporary table if not exists {$tableName}
			(
			{$fieldString}
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
SQLEND;
		$createResult = $this->db_db_query($sql);
//		////debug_r($sql);

		$sql = <<<SQLEND
		delete from $tableName
SQLEND;
		$this->db_db_query($sql);

		$question_1 = explode(".",$ParXAxis);
		$question_1_question = $question_1[0];
		$question_1_subquestion = $question_1[1];
//		//debug($question_1_subquestion);
		$question_2 = explode(".",$ParYAxis);
		$question_2_question = $question_2[0];
		$question_2_subquestion = $question_2[1];

		if (empty($question_1_subquestion)) {
			$question_1_subquestion = 1;
		}
		if (empty($question_2_subquestion)) {
			$question_2_subquestion = 1;
		}

		$questionElementOptions1 = $questionsArray[$question_1_question]["OPTIONS"];
		$questionElementOptions2 = $questionsArray[$question_2_question]["OPTIONS"];
//		////debug_r($questionsArray);
		if ($createResult) {
//			////debug_r($answer);
			foreach($answer as $answerKey=>$answerElement) {
				$sql_answer1 = "";
				$sql_answer2 = "";
				if($questionsArray[$question_1_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]
					|| $questionsArray[$question_1_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]){
					$sql_answer1 = "[".$questionElementOptions1[$answerElement[$question_1_question][$question_1_subquestion-1]]."]";
				}
				if($questionsArray[$question_1_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]){
//					////debug_r($questionElementOptions1);
					for($num = 0; $num < sizeof($answerElement[$question_1_question]); $num++){
//						////debug_r($questionElementOptions2);
						$currentAnswer = $answerElement[$question_1_question][$num];
						if ($currentAnswer) {
							$sql_answer1 .= "[".$questionElementOptions1[$num]."]";
						}
					}
				}
				if($questionsArray[$question_2_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MC"]
					|| $questionsArray[$question_2_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]){
					$sql_answer2 = "[".$questionElementOptions2[$answerElement[$question_2_question][$question_2_subquestion-1]]."]";
				}
				if($questionsArray[$question_2_question]["TYPE"] == $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]){
					for($num = 0; $num < sizeof($answerElement[$question_2_question]); $num++){
//						////debug_r($questionElementOptions2);
						$currentAnswer = $answerElement[$question_2_question][$num];
						if ($currentAnswer) {
							$sql_answer2 .= "[".$questionElementOptions2[$num]."]";
						}

					}
				}
//				$sql_answer1 = $questionElementOptions1[$answerElement[$question_1_question][$question_1_subquestion-1]];
//				$sql_answer2 = $questionElementOptions2[$answerElement[$question_2_question][$question_2_subquestion-1]];
				$sql_answer1 = addslashes($sql_answer1);
				$sql_answer2 = addslashes($sql_answer2);
				$insertValueArray[] = "('$sql_answer1','$sql_answer2')";
			}
			if (is_array($insertValueArray) && count($insertValueArray)>0) {
				$sql = "INSERT INTO $tableName VALUES".implode(",",$insertValueArray);
				$insertResult = $this->db_db_query($sql);
			}

		}
		return $createResult;
	}

	public function generateXYSelection($ParXAxis,$ParYAxis,$ParQuestionsElement) {
		if (is_array($ParQuestionsElement)) {
			foreach($ParQuestionsElement as $questionsElementKey => $questionsElementElement) {
				$type = $questionsElementElement["TYPE"];
				$questionTitle = $questionsElementElement["TITLE"];
				$isTypeForAnalyze = $this->isTypeForAnalyze($type);
				if ($isTypeForAnalyze) {
					$x_checked = "";
					if ($ParXAxis==$questionsElementKey) {
						$x_checked = "checked = 'checked' ";
					}
					$x_axis_radio_selection_array[] = "<input type=\"radio\" class=\"radio\" name=\"x_axis\" $x_checked id=\"x_axis_$questionsElementKey\" value=\"$questionsElementKey\" />
													<label for=\"x_axis_$questionsElementKey\">$questionTitle</label>
													<input type=\"hidden\" name=\"x_axis_{$questionsElementKey}_type\" value=\"".$questionElements["TYPE"]."\" />";
					$y_checked = "";
					if ($ParYAxis==$questionsElementKey) {
						$y_checked = "checked = 'checked' ";
					}
					$y_axis_radio_selection_array[] = "<input type=\"radio\" class=\"radio\" name=\"y_axis\" $y_checked id=\"y_axis_$questionsElementKey\" value=\"$questionsElementKey\" />
													<label for=\"y_axis_$questionsElementKey\">$questionTitle</label>
													<input type=\"hidden\" name=\"y_axis_{$questionsElementKey}_type\" value=\"".$questionElements["TYPE"]."\" />";

				}
			}
		}



		if (is_array($x_axis_radio_selection_array) && is_array($y_axis_radio_selection_array)) {
			$X_Selection = implode("<br/>",$x_axis_radio_selection_array);
			$Y_Selection = implode("<br/>",$y_axis_radio_selection_array);;
		}

		$returnArray = array("X_SELECTION"=>$X_Selection,"Y_SELECTION"=>$Y_Selection);
		return $returnArray;
	}

	/*
	* $displayMode = 1  , normal display --> first version
	* $displayMode = 2  , display without a) question selection box
	*/
	public function generateCombineBox($ParSurveyID,$ParCombineNameUse,$ParCombineBoxContent,$ParXAxis,$ParYAxis=null,$ParMappingTitle="",$ParXTitle="",$ParYTitle="", $ParShowNoCombination=false,$displayMode = 1) {
		global $Lang, $linterface;

		if (empty($ParYAxis)) {
			$hiddenAxisTitle = " style='display:none' ";
		}

		##################################################
		##	select_question_combine
		$selectedQuestion = $ParYAxis;
		$showNoCombine = $ParShowNoCombination;
		$name = $ParCombineNameUse."[QuestionNum]";
		$id = $name;
		$onChangeAction="loadCombineBoxContent(this.id)";
		$select_question_combine = $this->getSelectQuestionBox($ParSurveyID,$name,$id,$selectedQuestion,$showNoCombine,$onChangeAction,$ParXAxis);
		##################################################
		$selectHtml = '';
		$removeButtonHtml = '';
if($displayMode == 1){
		$selectHtml = <<<HTMLSELECT
	        <tr><td>{$Lang['IES']['PairUpWith']}</td><td>:</td><td>{$select_question_combine}</td></tr>
HTMLSELECT;
		$removeButtonHtml = <<<HTMLREMOVEBUTTON
		<div class="btn">
		<input name="submit2" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" {$Lang['IES']['Delete']} " type="button" onClick="removeBox('{$ParCombineNameUse}')">
        </div>
HTMLREMOVEBUTTON;
}
		$axisTitleBox .= <<<HTMLEND
		<table class="form_table" name="{$ParCombineNameUse}[AxisTitle]" id="{$ParCombineNameUse}[AxisTitle]" {$hiddenAxisTitle}>
            <col class="field_title">
			<col class="field_c">
            <tbody><tr><td><div style="border: 2px solid rgb(204, 204, 204); width: 30px; height: 16px; background-color: rgb(194, 236, 236);"></div></td><td>:</td><td><input name="{$ParCombineNameUse}[XTitle]" id="{$ParCombineNameUse}[XTitle]" type="text" value="{$ParXTitle}"></td></tr>
            <tr><td><div style="border: 2px solid rgb(204, 204, 204); width: 30px; height: 16px; background-color: rgb(223, 249, 182);"></div></td><td>:</td><td><input name="{$ParCombineNameUse}[YTitle]" id="{$ParCombineNameUse}[YTitle]" type="text" value="{$ParYTitle}"></td></tr>
            </tbody>
        </table>
HTMLEND;
		$questionNum = "";
		$html = <<<HTMLEND
	<div class="ies_q_box q_group" id="{$ParCombineNameUse}">
        <div class="q_title">
	        <table class="form_table">
	        <col class="field_title">
			<col class="field_c">
	        <tbody><tr><td>{$Lang['IES']['PairupGroupItem']}</td><td>:</td><td><input name="{$ParCombineNameUse}[Title]" id="{$ParCombineNameUse}_Title" value="{$ParMappingTitle}" class="textbox combinationInfoTitle" type="text"></td></tr>
			{$selectHtml}
	        </tbody></table>
	        <br />
	    </div>


        <div  name="{$ParCombineNameUse}[Content]" id="{$ParCombineNameUse}[Content]">
        	<div class="q_option">
	        	{$ParCombineBoxContent}
			</div>
		</div>
		{$axisTitleBox}

			<span style="text-align:center;"><input type = "button" name="cancelSelect" value="{$Lang['IES']['SurveyTable']['string26']}" onClick="resetCombineButton()"></span>
        {$removeButtonHtml}


	</div>
HTMLEND;
		return $html;
	}
	public function getCombineBoxContent($ParSurveyID,$ParXAxis,$ParYAxis="",$ParCombineNameUse="") {
		global $Lang;

//error_log("-->getCombineBoxContent ParXAxis -- ".$ParXAxis." ParYAxis -- ".$ParYAxis."\n", 3, "/tmp/aaa.txt");

		$question = $this->getSurveyQuestion($ParSurveyID);
		$questionsElement = $this->breakQuestionsString($question);
		$og_questionsElement = $questionsElement;
		$IS_ANSWER_VALID = true;

		$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
		$broken_answer = $this->splitAnswer($answer);
		$refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsElement,$answer,$ParXAxis,$ParYAxis);
		$questionsElement = $refinedQuestionsAnswer["QUESTIONS_ARRAY"];
		$answer = $refinedQuestionsAnswer["ANSWER"];

		if (empty($refinedQuestionsAnswer["Y_AXIS"])) {
			$resultTable = $this->getPlainTable($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"]);
		} else {
			$analysisResults = $this->getAnalysisResults2($og_questionsElement,$broken_answer, $ParXAxis,$ParYAxis);

			# with subquestions (e.g.likert scale)
			if (isset($og_questionsElement[$refinedQuestionsAnswer["X_AXIS"]]["QUESTION_NUM"])) {
				$x_title = $og_questionsElement[$refinedQuestionsAnswer["X_AXIS"]]["QUESTION_QUESTIONS"][$refinedQuestionsAnswer["X_SUBQUESTION_NUMBER"]-1];

			# without subquestions (e.g.MC,MMC)
			} else {
				$x_title = $og_questionsElement[$refinedQuestionsAnswer["X_AXIS"]]["TITLE"];
			}
			# with subquestions (e.g.likert scale)
			if (isset($og_questionsElement[$refinedQuestionsAnswer["Y_AXIS"]]["QUESTION_NUM"])) {
				$y_title = $og_questionsElement[$refinedQuestionsAnswer["Y_AXIS"]]["QUESTION_QUESTIONS"][$refinedQuestionsAnswer["Y_SUBQUESTION_NUMBER"]-1];

			# without subquestions (e.g.MC,MMC)
			} else {
				$y_title = $og_questionsElement[$refinedQuestionsAnswer["Y_AXIS"]]["TITLE"];
			}


			$resultTable .= "<div>".$Lang['IES']['TableBelowShowResultOfQuestion'].":</div>";
		    $resultTable .= "<br style=\"clear:both;\" />";

			$title = "<font color=\"black\">{$y_title}/{$x_title}</font>";
			$resultTable .= str_replace("<!--TITLE-->" , $title , $this->generateTableXandY($analysisResults["2D_RESULT"]))."<br />";
		}

		return $resultTable;
	}

	function AnswerStringParse($ParXAxis, $ParYAxis, $ParAnswer, $ParQuestion){
		global $ies_cfg;
		$returnArray = array();
		////debug("IN");
		//////debug_r($ParQuestion);
		for($i = 0; $i < $ParQuestion[$ParXAxis]['QUESTION_NUM']; $i++){
				if($ParYAxis == -1){
					$AnswerArray = array();
					for($m = 0; $m < sizeof($ParAnswer); $m++){
						$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswer[$m]);
						//////debug_r($answerArray);
						if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
						{
							$allanswer = split(",", $answerArray[$ParXAxis]);
							$answerArray[$ParXAxis] = $allanswer[$i];
						}

						$AnsString = "";
						for($k = 1; $k < sizeof($answerArray); $k++){
							$AnsString .= $ies_cfg["Questionnaire"]["Delimiter"]["Answer"].$answerArray[$k];
						}
						$AnswerArray[] = $AnsString;
					}
					$returnArray[$i][0] = $AnswerArray;
				}
				else{
					for($j = 0; $j < $ParQuestion[$ParYAxis]['QUESTION_NUM']; $j++){
						$AnswerArray = array();
						for($m = 0; $m < sizeof($ParAnswer); $m++){
							$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswer[$m]);
							//////debug_r($answerArray);
							if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
							{
								$allanswer = split(",", $answerArray[$ParXAxis]);
								$answerArray[$ParXAxis] = $allanswer[$i];
							}
							if($ParQuestion[$ParYAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
							{
								$allanswer = split(",", $answerArray[$ParYAxis]);
								$answerArray[$ParYAxis] = $allanswer[$j];
							}
							$AnsString = "";
							for($k = 1; $k < sizeof($answerArray); $k++){
								$AnsString .= $ies_cfg["Questionnaire"]["Delimiter"]["Answer"].$answerArray[$k];
							}
							$AnswerArray[] = $AnsString;
						}
						$returnArray[$i][$j] = $AnswerArray;
						//echo "i:$i;j:$j";
					}
				}
		}
		//////debug_r($returnArray);
		return $returnArray;
	}
	function QuestionStringParse($ParXAxis, $ParYAxis, $ParQuestion){
		global $ies_cfg;
		$returnArray = array();
		//////debug_r($ParQuestion);
		for($i = 0; $i < $ParQuestion[$ParXAxis]['QUESTION_NUM']; $i++){
			if($ParYAxis == -1){
				$temp_ques = $ParQuestion;
				if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
				{
					$temp_ques[$ParXAxis]["TITLE"] = $ParQuestion[$ParXAxis]["QUESTION_QUESTIONS"][$i];
					$temp_ques[$ParXAxis]["TYPE"] = $ies_cfg["Questionnaire"]["QuestionType"]["MC"];

				}

				$returnArray[$i][0] = $temp_ques;
			}
			else{
				for($j = 0; $j < $ParQuestion[$ParYAxis]['QUESTION_NUM']; $j++){
					$temp_ques = $ParQuestion;
					if($ParQuestion[$ParXAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
					{
						$temp_ques[$ParXAxis]["TITLE"] = $ParQuestion[$ParXAxis]["QUESTION_QUESTIONS"][$i];
						$temp_ques[$ParXAxis]["TYPE"] = $ies_cfg["Questionnaire"]["QuestionType"]["MC"];

					}
					if($ParQuestion[$ParYAxis]['TYPE'] == $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])
					{
						$temp_ques[$ParYAxis]["TITLE"] = $ParQuestion[$ParYAxis]["QUESTION_QUESTIONS"][$j];
						$temp_ques[$ParYAxis]["TYPE"] = $ies_cfg["Questionnaire"]["QuestionType"]["MC"];
					}
					$returnArray[$i][$j] = $temp_ques;
					//echo "i:$i;j:$j";
				}
			}
		}
		//////debug_r($returnArray);
		return $returnArray;
	}


	public function generateMappingTables($ParSurveyID, $ParStudentID,$ParDefaultXAxis) {
		$SurveyMapping = $this->loadSurveyMapping($ParSurveyID,$ParStudentID);
		if (is_array($SurveyMapping)) {
			$question = $this->getSurveyQuestion($ParSurveyID);
			$IS_ANSWER_VALID = true;
			$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
			$questionsElement = $this->breakQuestionsString($question);


			foreach($SurveyMapping as $SurveyMappingKey => $SurveyMappingElement) {
				if ($SurveyMappingElement["XMAPPING"]==$ParDefaultXAxis) {
					$x_axis = $SurveyMappingElement["XMAPPING"];
					$y_axis = $SurveyMappingElement["YMAPPING"];
					if (!empty($x_axis) && empty($y_axis)) continue;
					$title = $SurveyMappingElement["MAPPINGTITLE"];
					$x_title = $SurveyMappingElement["XTITLE"];
					$y_title = $SurveyMappingElement["YTITLE"];
					$isNewTable = false;
					$combineNameUse = $this->getCombineNameUse($isNewTable,$SurveyMappingElement["SURVEYMAPPINGID"]);
					$combineBoxContent = $this->getCombineBoxContent($ParSurveyID,$x_axis,$y_axis,$combineNameUse);
//					error_log(" --> x_axis -->".$x_axis." y_axis --> ".$y_axis." title --> ".$title." x_title --> ".$x_title." y_title --> ".$y_title."\n", 3, "/tmp/aaa.txt");

					$html .= $this->generateCombineBox($ParSurveyID,$combineNameUse,$combineBoxContent,$x_axis,$y_axis,$title,$x_title,$y_title);
				}
			}
		}
		return $html;
	}

	public function getCombineNameUse($ParIsNewTable,$ParQuestionID) {
		if ($ParIsNewTable) {
			$type = "new";
		} else {
			$type = "old";
		}
		return "combinationInfo[$type][$ParQuestionID]";
	}
	public function saveMappings($ParXAxis,$ParCombinationInfo,$ParSurveyID,$ParStudentID,$ParDeleteMode = 1) {

		$oldCombinations = $ParCombinationInfo["old"];
		$newCombinations = $ParCombinationInfo["new"];
		$CurrentUpdateMappingIds = array();
		if (is_array($oldCombinations)) {
			foreach($oldCombinations as $SurveyMappingID => $oldCombinationsElement) {
				$CurrentUpdateMappingIds[] = (int)$SurveyMappingID;
				$y_axis = $oldCombinationsElement["QuestionNum"];
				$x_title = $oldCombinationsElement["XTitle"];
				$y_title = $oldCombinationsElement["YTitle"];
				$title = $oldCombinationsElement["Title"];
				$result[] = $this->updateSurveyMapping($SurveyMappingID,$ParXAxis,$y_axis,$x_title,$y_title,$title);
			}
		}

		$SurveyMappingIds = array();
		$SurveyMappingIds = $this->getSurveyMappingIdsWithSameX($ParSurveyID,$ParStudentID,$ParXAxis);
		$RecordToRemove = array_diff($SurveyMappingIds,$CurrentUpdateMappingIds);

		if($ParDeleteMode){
			if (count($RecordToRemove)>0) {
				$result[] = $this->removeSurveyMapping($RecordToRemove);
			}
		}

		if (is_array($newCombinations)) {

			foreach($newCombinations as $newCombinationsKey => $newCombinationsElement) {
				$y_axis = $newCombinationsElement["QuestionNum"];
				$x_title = $newCombinationsElement["XTitle"];
				$y_title = $newCombinationsElement["YTitle"];
				$title = $newCombinationsElement["Title"];
				$result[] = $this->newSurveyMapping($ParSurveyID,$ParStudentID,$ParXAxis,$y_axis,$x_title,$y_title,$title);
			}
		}

		return $result;
	}

	public function IsSurveyMappingExist($ParSurveyID,$ParStudentID,$ParXAxis,$ParYAxis) {
		global $intranet_db;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		if (empty($ParYAxis)) {
			$ParYAxis = "NULL";
		}
		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID = '$ParSurveyID' AND USERID = '$ParStudentID' AND XMAPPING = '$XAxis' AND YMAPPING = '$YAxis'";
		$returnArray = $this->returnArray($sql);
		if (count($returnArray)>0) {
			return true;
		} else {
			return false;
		}
	}

	public function removeSurveyMapping($ParSurveyMappingIds) {
		global $intranet_db;
		$sql = "DELETE FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYMAPPINGID IN (".implode(",",$ParSurveyMappingIds).")";

//error_log($sql." \nlibies_survey\n", 3, "/tmp/aaa.txt");
		$result = $this->db_db_query($sql);
		return $result;
	}

	public function getSurveyMappingIdsWithSameX($ParSurveyID,$ParStudentID,$ParXAxis) {
		global $intranet_db,$ies_cfg;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID = '$ParSurveyID' AND USERID = '$ParStudentID' AND XMAPPING = '$ParXAxis'";
		$returnVector = $this->returnVector($sql);
		return $returnVector;
	}

	public function newSurveyMapping($ParSurveyID,$ParStudentID,$ParXAxis,$ParYAxis,$ParXTitle,$ParYTitle,$ParTitle,$ParComment='') {
		global $intranet_db;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		else {
			$ParXAxis = "'$ParXAxis'";
		}
		if (empty($ParYAxis)) {
			$ParYAxis = "NULL";
		}
		else {
			$ParYAxis = "'$ParYAxis'";
		}

		if ($ParComment != '')
			$CommentInsert = ", COMMENT = '$ParComment' ";

		$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_MAPPING
				SET SURVEYID = '$ParSurveyID',
				USERID = '$ParStudentID',
				XMAPPING = $ParXAxis,
				YMAPPING = $ParYAxis,
				XTITLE = '$ParXTitle',
				YTITLE = '$ParYTitle',
				MAPPINGTITLE = '$ParTitle',
				DATEINPUT = NOW(),
				DATEMODIFIED = NOW()
				$CommentInsert
				";
//error_log($sql." \nlibies_survey\n", 3, "/tmp/aaa.txt");
		$result = $this->db_db_query($sql);

		/*
		if($result == false){
			// do nothing
		}else{
			//return the last insert id
			$_lastInsertID = $this->db_insert_id();
			$result = $_lastInsertID;
		}
		*/
		return $result;
	}
	public function updateSurveyMapping($ParSurveyMappingID,$ParXAxis,$ParYAxis,$ParXTitle,$ParYTitle,$ParTitle,$ParComment='') {
		global $intranet_db;
		if (empty($ParXAxis)) {
			$ParXAxis = "NULL";
		}
		else {
			$ParXAxis = "'$ParXAxis'";
		}
		if (empty($ParYAxis)) {
			$ParYAxis = "NULL";
		}
		else {
			$ParYAxis = "'$ParYAxis'";
		}

		if ($ParComment != '')
			$CommentUpdate = ", COMMENT = '$ParComment' ";

		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING
				SET
				XMAPPING = $ParXAxis,
				YMAPPING = $ParYAxis,
				XTITLE = '$ParXTitle',
				YTITLE = '$ParYTitle',
				MAPPINGTITLE = '$ParTitle',
				DATEMODIFIED = NOW()
				$CommentUpdate
				WHERE SURVEYMAPPINGID = '$ParSurveyMappingID'";
//error_log($sql." \nlibies_survey\n", 3, "/tmp/aaa.txt");

		$result = $this->db_db_query($sql);
		return $result;
	}

	public function updateSurveyComment($ParSurveyMappingID,$ParComment="") {
		global $intranet_db;
		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING
				SET COMMENT = '$ParComment'
				WHERE SURVEYMAPPINGID = '$ParSurveyMappingID'";
		$result = $this->db_db_query($sql);
		return $result;
	}

	/**
	* Update table IES_SURVEY_MAPPING
	* @owner : Fai (201000922)
	* @param : integer $ParSurveyMappingID ID of the SurveyMappingID that need to update
	* @param : Array  $ParData Data that need to update
	* @return : Boolean $result Result of the update
	*
	*/
	public function updateSurvey($ParSurveyMappingID,$ParData) {
		global $intranet_db;
		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING
				set XTitle = '{$ParData['xTitle']}',
				YTitle = '{$ParData['yTitle']}',
				MappingTitle = '{$ParData['title']}'
				WHERE SURVEYMAPPINGID = '$ParSurveyMappingID'";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
		$result = $this->db_db_query($sql);
		return $result;
	}


	public function getSurveyListingTable($ParSurveyID,$ParStudentID,$ParIsCheckFromIESStudent,$ParCommentEdit=false,$ForSurveyDiscovery=0) {

		global $Lang;
		$SurveyMapping = $this->loadSurveyMapping($ParSurveyID,$ParStudentID);

		$EditPhp = ($ForSurveyDiscovery == 1)? 'discovery_info_layer.php' : 'questionnaireShowTable.php';
		$ItemTitle = ($ForSurveyDiscovery == 1)? $Lang['IES']['DiscoveryName'] : $Lang['IES']['CombinationName'];

		if (is_array($SurveyMapping) && count($SurveyMapping)>0) {
			if ($ParCommentEdit) {
				$URIEdit = "IsEdit=1";
			} else {
				$URIEdit = "IsEdit=0";
			}
			$count=0;
			foreach($SurveyMapping as $key => $element) {
				$surveyMappingID = $element["SURVEYMAPPINGID"];
				$encodedMappingID = base64_encode($surveyMappingID);
				$dateModified = $element["DATEMODIFIED"];
				$mappingTitle = $element["MAPPINGTITLE"];
				++$count;

				if ($ParIsCheckFromIESStudent) {
					$deleteButton = '';
					if ($ForSurveyDiscovery == 0)
						$deleteButton .= "<div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"移動\"></a></div>";
					$deleteButton .= "<div class=\"table_row_tool\"><a title=\"Delete\" class=\"delete_dim\" href=\"#\" onClick=\"removeMapping(this,'{$encodedMappingID}')\"></a></div>";
				} else {
					$deleteButton = "&nbsp;";
				}
				$content .= <<<HTMLEND
		    <tr id="{$surveyMappingID}">
		      <td>{$count}</td>
		      <td><a class="IES_file_upload thickbox" href="{$EditPhp}?SurveyMappingID={$surveyMappingID}&$URIEdit&KeepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=550">$mappingTitle</a></td>
		      <td>{$dateModified}</td>
		      <td class="sub_row Dragable">{$deleteButton}</td>
		    </tr>
HTMLEND;
			}
		} else {
			$content .= "<tr><td colspan='4' style='text-align:center'>{$Lang['IES']['NoRecordAtThisMoment']}</td></tr>";
		}
		$html = <<<HTMLEND
		<table class="common_table_list">
		  <thead>
		  	  <tr>
		      <th class="num_check">#</th>
		      <th>{$ItemTitle}</th>
		      <th>{$Lang['IES']['CreationDate']}</th>
		      <th>&nbsp;</th>
		    </tr>
		  </thead>
		  <tbody class="ClassDragAndDrop" id="SurveyMappingIDInSeq">
		  	{$content}
		  </tbody>
		</table>
HTMLEND;
		return $html;
	}

	function getNoCombineSurveyMapping($ParSurveyID,$ParQuestionNo,$ParStudentID) {
		global $intranet_db;
		$sql = "SELECT SURVEYMAPPINGID FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SURVEYID = {$ParSurveyID} AND XMAPPING = '{$ParQuestionNo}' AND (YMAPPING IS NULL OR YMAPPING = '') AND USERID = '{$ParStudentID}'";
//error_log(" getNoCombineSurveyMapping --->".$sql."\n", 3, "/tmp/aaa.txt");
		return current($this->returnVector($sql));
	}

	#===ADD
	/**
	 * GetIndividualAnswerDisplay
	 * @param INT $ParSurveyID - the Survey ID
	 *
	 * @param INT $ParQuestionNo - The question number start from 1
	 *
	 * @param INT $ParDisplayCreateNewQuestion - Option for Control the UI for question setting
	 *
	 * @return HTML code
	 */
	function GetIndividualAnswerDisplay($ParSurveyID, $ParQuestionNo, $ParIsAddButton=false, $ParStudentID=null,$ParDisplayCreateNewQuestion = "1"){
		global  $ies_cfg;

		################################################
		##	Prepare resources
		$question = $this->getSurveyQuestion($ParSurveyID);
		$IS_ANSWER_VALID = true;
		$answer = $this->getSurveyAnswer($ParSurveyID,$IS_ANSWER_VALID);
		$questionsArray = $this->breakQuestionsString($question);

		# refined the LS element
		$refinedQuestionsAnswer = $this->getRefinedQuestionsAndAnswerForLS($questionsArray,$answer,$ParQuestionNo);
//		////debug_r($refinedQuestionsAnswer);
		################################################

		$table = "";
		## Coz the Qestion Number Start From 1## so <=
		if ($refinedQuestionsAnswer["X_QUESTION_NUMBER"] <= sizeof($questionsArray)) {
			$PlainTable = $this->getPlainTable($refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["ANSWER"], $refinedQuestionsAnswer["X_AXIS"],$refinedQuestionsAnswer["X_SUBQUESTION_NUMBER"]);
		}
		if(!empty($PlainTable)){
			if (!empty($ParStudentID)) {
				$currentSurveyMappingID = $this->getNoCombineSurveyMapping($ParSurveyID,$ParQuestionNo,$ParStudentID);
			}
			if ($ParIsAddButton && count($questionsArray)>1) {	// only one question do not show the add combination button
				$IsAddButton = true;
			} else {
				$IsAddButton = false;
			}
//			$ParDisplayCreateNewQuestion = 0;
			$table = $this->genTableWithStyle($PlainTable, $refinedQuestionsAnswer["QUESTIONS_ARRAY"], $refinedQuestionsAnswer["X_AXIS"],$IsAddButton,$currentSurveyMappingID,$ParDisplayCreateNewQuestion,$ParSurveyID,$ParStudentID);
		}
		return $table;
	}

	/**
	 * Handling the likert scale for specified subquestion required
	 */
	public function getRefinedQuestionsAndAnswerForLS($ParQuestionsArray,$ParAnswer,$ParQuestionNumberX,$ParQuestionNumberY=null) {
		$returnArray = array();
		# handling for x question, subquestion number
		$questionSubquestionNumX = explode(".",$ParQuestionNumberX);	// it is a decimal format x.yyy
		$returnArray["X_AXIS"] = $questionSubquestionNumX[0];
		$returnArray["X_SUBQUESTION_NUMBER"] = (int)$questionSubquestionNumX[1];

		# handling for y question, subquestion number
		if (!empty($ParQuestionNumberY)) {
			$questionSubquestionNumY = explode(".",$ParQuestionNumberY);	// it is a decimal format x.yyy
			$returnArray["Y_AXIS"] = $questionSubquestionNumY[0];
			$returnArray["Y_SUBQUESTION_NUMBER"] = (int)$questionSubquestionNumY[1];
		}

		# handling for x subquestion, answer extraction
		if (!empty($returnArray["X_SUBQUESTION_NUMBER"])) {	// the subquestion number i.e. yyy
			$returnArray["QUESTIONS_ARRAY"] = $this->extractWithSubquestion($ParQuestionsArray,$returnArray["X_AXIS"],$returnArray["X_SUBQUESTION_NUMBER"]);
			$returnArray["ANSWER"] = $this->filterUnwantedAnswer($ParAnswer,$returnArray["X_AXIS"],$returnArray["X_SUBQUESTION_NUMBER"]);
		} else {
			$returnArray["QUESTIONS_ARRAY"] = $ParQuestionsArray;
			$returnArray["ANSWER"] = $ParAnswer;
		}

		# handling for y subquestion, answer extraction
		if (isset($returnArray["Y_SUBQUESTION_NUMBER"])) {	// the subquestion number i.e. yyy
			$returnArray["QUESTIONS_ARRAY"] = $this->extractWithSubquestion($returnArray["QUESTIONS_ARRAY"],$returnArray["Y_AXIS"],$returnArray["Y_SUBQUESTION_NUMBER"]);
			$returnArray["ANSWER"] = $this->filterUnwantedAnswer($returnArray["ANSWER"],$returnArray["Y_AXIS"],$returnArray["Y_SUBQUESTION_NUMBER"]);
		} else {
			// do nothing, it will then equal to the return array set during handling for x subquestion, answer extraction
		}
		return $returnArray;
	}

	public function filterUnwantedAnswer($ParAnswerStringArray, $ParQuestionNumber, $ParSubquestionRequired) {
		global $ies_cfg;
		if (is_array($ParAnswerStringArray)) {
			$subquestionIndex = $ParSubquestionRequired-1;
			foreach($ParAnswerStringArray as $ParAnswerStringArrayKey => &$ParAnswerStringArrayElement) {
				# break to answers
				$explodedAnswer = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswerStringArrayElement);	// reminded that the 1st one of the array is empty
				# break options for specific answer
				$targetSubquestionAnswerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$explodedAnswer[$ParQuestionNumber]);
				# assign the only required answer back
				$explodedAnswer[$ParQuestionNumber] = $targetSubquestionAnswerArray[$subquestionIndex];
				# construct back to answer
				$ParAnswerStringArrayElement = implode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$explodedAnswer);
			}
		}
		return $ParAnswerStringArray;
	}

	/**
	 * Get the question element with specified question extracted required subquestion
	 * @param Array $ParQuestionElement - array generated by function breakQuestionsString()
	 * @param INT paramname - question number
	 * @param INT $ParSubquestionRequired - subquestion number
	 * @return Array
	 */
	public function extractWithSubquestion($ParQuestionElement,$ParQuestionNumber, $ParSubquestionRequired) {
		$targetSubquestions = $ParQuestionElement[$ParQuestionNumber]["QUESTION_QUESTIONS"];
		if (is_array($targetSubquestions)) {
			foreach($targetSubquestions as $targetSubquestionsKey => $targetSubquestionsElement) {
				$subquestionIndex = $ParSubquestionRequired-1;
				if ($targetSubquestionsKey==$subquestionIndex) {
					$newSetOfSubquestions[] = $targetSubquestionsElement;
				} else {
					// do nothing
				}
			}
		}
		$ParQuestionElement[$ParQuestionNumber]["QUESTION_QUESTIONS"] = $newSetOfSubquestions;
		$ParQuestionElement[$ParQuestionNumber]["QUESTION_NUM"] = count($newSetOfSubquestions);
		return $ParQuestionElement;
	}

	function getPlainTable($ParQuestionsArray, $ParAnswer, $ParQuestionNo, $ParSubquestionNo=0){
		global $ies_cfg;
		$resultArray = $this->getAnalysisResultArray($ParQuestionsArray, $ParAnswer, $ParQuestionNo);
		switch ($ParQuestionsArray[$ParQuestionNo]["TYPE"]){
				case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:

					return $this->genPlainTableForMC($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:

					return $this->genPlainTableForMC($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
					return $this->genPlainTableForText($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
					return $this->genPlainTableForText($resultArray);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
					return $this->genPlainTableForLS($resultArray, $ParQuestionNo, $ParQuestionsArray,$ParSubquestionNo);
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

					break;

			}
	}

	function getAnalysisResultArray($ParQuestionsArray, $ParAnswer, $ParQuestionNo){
		global $ies_cfg;

		switch ($ParQuestionsArray[$ParQuestionNo]["TYPE"]){
				case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
					$answer = $this->splitAnswer($ParAnswer);
					$array = $this->getAnalysisResults2($ParQuestionsArray,$answer, $ParQuestionNo, null);
					return $array["X_RESULT"];
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
					$answer = $this->splitAnswer($ParAnswer);
					$array = $this->getAnalysisResults2($ParQuestionsArray,$answer, $ParQuestionNo, null);
					return $array["X_RESULT"];

					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
					return $this->getAnsTextResultArray($ParQuestionNo, $ParQuestionsArray, $ParAnswer);

					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
					return $this->getAnsTextResultArray($ParQuestionNo, $ParQuestionsArray, $ParAnswer);

					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
					$array = array();
					## Gen New Answer and Question
					$NewAns = $this->AnswerStringParse($ParQuestionNo, -1, $ParAnswer, $ParQuestionsArray);

					#!!!! The type of the question will be tuned as MC here for process
					$NewQuestion = $this->QuestionStringParse($ParQuestionNo, -1, $ParQuestionsArray);

					for($i = 0; $i < $ParQuestionsArray[$ParQuestionNo]["QUESTION_NUM"]; $i++){
						$answer = $this->splitAnswer($NewAns[$i][0]);
						$ra = $this->getAnalysisResults2($NewQuestion[$i][0],$answer, $ParQuestionNo, null);
						$array[] = $ra["X_RESULT"];
					}
					return $array;
					break;

				case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

					break;

			}
	}


	//		$ParDisplayCreateNewQuestion is NEW, 20110831
	//	    $ParDisplayCreateNewQuestion = 0 , don't display the set question input
	//		$ParDisplayCreateNewQuestion = 1 (original version), display the set question input
	//		$ParDisplayCreateNewQuestion = 2 , display the question setting in a new place
	function genTableWithStyle($ParPlainTable, $ParQuestionArray, $ParQuestionNo, $ParIsAddButton=false, $ParMappingID="",$ParDisplayCreateNewQuestion = '1',$ParSurveyID,$ParStudentID){
		global $Lang;

		$normalQuestionFlag = true;
		$specialQuestionHTML = '';
		$specialQuestionHTMLDetails = '';
		$displayFlag  = false;

		$subQuestionArray = array(); // store the sub question for LikertScale question

		$html .= "<div class=\"ies_q_box\">";
		$html .= "<div class=\"q_title\" id=\"title_".$ParQuestionNo."\">";
		$html .= "\n<ol start=\"{$ParQuestionNo}\">";
//		$html .= $ParQuestionArray[$ParQuestionNo]["TITLE"];

		if($ParDisplayCreateNewQuestion == 2){
		$uncheckAllSubQuestionJS = '';
			if(is_array($ParQuestionArray[$ParQuestionNo]["QUESTION_QUESTIONS"])){
				//special handle LikertScale question,
				$normalQuestionFlag = false;
				$displayWholeSetQuestionFlag = false;		//control the display of the whole set question input

				for($i = 0,$i_max = count($ParQuestionArray[$ParQuestionNo]["QUESTION_QUESTIONS"]);$i< $i_max; $i++){
					$_mTitle = '';
					$h_checked = '';

					$individaulSurveyMappingDetails = array();
					$currentSurveyMappingID = '';

					$_q_title = $ParQuestionArray[$ParQuestionNo]["QUESTION_QUESTIONS"][$i];	//$_q_title ==> question title
					$_tmp_no = $i+1;
					$_q_no	= $ParQuestionNo.'.'.$_tmp_no; //$_q_no ==> question Number
					$subQuestionArray[] = $_q_no; // store the sub question to $subQuestionArray

					$currentSurveyMappingID = $this->getNoCombineSurveyMapping($ParSurveyID,$_q_no,$ParStudentID);
					if(is_numeric($currentSurveyMappingID)){
						$individaulSurveyMappingDetails = current($this->loadSurveyMapping($ParSurveyID,$ParStudentID,$currentSurveyMappingID));
						$_mTitle  = $individaulSurveyMappingDetails['MAPPINGTITLE'];	//$_mTitle ==> mapping title
						$displayWholeSetQuestionFlag = true;
						$h_checked = ' checked ';

					}

					//hidden form field to store the DB SurveyMappingID
					$specialQuestionHTMLDetails .= '<tr><td width= "100">';
					$specialQuestionHTMLDetails .= '<input type="hidden" name="FocusQuestionSurveyMappingID['.$_q_no.']" id="FocusQuestionSurveyMappingID_'.$_q_no.'" value="'.$currentSurveyMappingID.'" />';

					//checkbox
					$specialQuestionHTMLDetails .= '<input type="checkbox" name="FocusQuestionSelected['.$_q_no.']" id="chkFocusQuestion_'.$_q_no.'" '.$h_checked.' /><label for="chkFocusQuestion_'.$_q_no.'">'.$_q_title.'</label>&nbsp;&nbsp;';

					$specialQuestionHTMLDetails .= '</td>';
					$specialQuestionHTMLDetails .= '<td>';

					$specialQuestionHTMLDetails .= '<input type="text" name="FocusQuestionTitle['.$_q_no.']" id="currentTitle_'.$_q_no.'" value="'.intranet_htmlspecialchars($_mTitle).'" onclick="selectQuestionChkBox(\''.$_q_no.'\')"/>';

					$specialQuestionHTMLDetails .= '</td></tr>';

				}

				$h_displayStyle = ($displayWholeSetQuestionFlag === true)? '': ' style="display:none" ';
//				$specialQuestionHTML .= '<span id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.'>';
				$specialQuestionHTML .= '<table id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.' border = "0">';
				$specialQuestionHTML .= $specialQuestionHTMLDetails;
//				$specialQuestionHTML .= '</span>';
				$specialQuestionHTML .= '</table>';

				$specialQuestionHTMLDetails = ''; // reset the variable
				//end of special handle LikertScale question,

			}else{
				//normal question
				//do nothing
			}

//			$currentSurveyMappingID = array();

			$h_checked = '';
			$h_displayStyle = '';
			$currentSurveyMappingID = '';
			$h_spanCSS = ' check_group ';
			if($normalQuestionFlag == true){
				$currentSurveyMappingID = $this->getNoCombineSurveyMapping($ParSurveyID,$ParQuestionNo,$ParStudentID);

				$h_displayStyle = ' style="display:none" ';
				$h_checked = '';
				$h_spanCSS = ' check_group ';
				if(is_numeric($currentSurveyMappingID)){
					$individaulSurveyMappingDetails = current($this->loadSurveyMapping($ParSurveyID,$ParStudentID,$currentSurveyMappingID));
					$_mTitle = $individaulSurveyMappingDetails['MAPPINGTITLE'];//$_mTitle ==> mapping title
					$h_displayStyle = '';  // reset the display style , set it to visible
					$h_checked = ' checked ';
					$h_spanCSS = 'check_group_checked';
					$is_existingQuestionFlag = 1;
				}

//				$htmlInputTitle  = '<span id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.' >'.$Lang['IES']['CombinationName'];
				$htmlInputTitle  = '<table id="currentTitleDiv_'.$ParQuestionNo.'" '.$h_displayStyle.' ><tr><td>'.$Lang['IES']['CombinationName'].' ';

				//hidden form field to store the DB SurveyMappingID
				$htmlInputTitle  .= '<input type="hidden" name="FocusQuestionSurveyMappingID['.$ParQuestionNo.']" id="FocusQuestionSurveyMappingID_'.$ParQuestionNo.'" value="'.$currentSurveyMappingID.'" />';

				$htmlInputTitle .= '<input type="text" name="FocusQuestionTitle['.$ParQuestionNo.']" id="currentTitle_'.$ParQuestionNo.'" value="'.intranet_htmlspecialchars($_mTitle).'" onclick="selectQuestionChkBox(\''.$ParQuestionNo.'\')"/>';

//				$htmlInputTitle .= '</span>'; //close 'currentTitleDiv_'
				$htmlInputTitle .= '</td></tr></table>'; //close 'currentTitleDiv_'
			}


//			$html .= '<span class="check_group_checked">'.$htmlInputTitle;

			if($normalQuestionFlag === true){
				$chkName = 'FocusQuestionSelected['.$ParQuestionNo.']';
			}else{
				$chkName = 'SpecialFocusQuestionSelected['.$ParQuestionNo.']';
				if($displayWholeSetQuestionFlag === true){
					//this is a LikertScale question and is selected
					$h_checked = ' checked ';
					$h_spanCSS = 'check_group_checked';
					$jsSubQuestionNoStr = implode('##',$subQuestionArray);
					$uncheckAllSubQuestionJS = ' unCheckSubQuestion(\''.$jsSubQuestionNoStr.'\'); ';
				}
			}

			$html .= '<span class="'.$h_spanCSS.'" id="setQuestionControl_'.$ParQuestionNo.'">';
			$html .= '<input name="'.$chkName.'" type="checkbox" ';
			$html .= ' onClick="showHideCurrentTitle('.$ParQuestionNo.');'.$uncheckAllSubQuestionJS.'" id="chkFocusQuestion_'.$ParQuestionNo.'" '.$h_checked.'/>';
			$html .= '<label for="chkFocusQuestion_'.$ParQuestionNo.'">'.$Lang['IES']['IsFocusQuestion'].'</label>';
			$html .= '<br/>'.$htmlInputTitle.$specialQuestionHTML.'</span>';

		}

		$html .= '<li>'.$ParQuestionArray[$ParQuestionNo]["TITLE"].'<a href="javascript:void(0);" onclick="return displayStatistics('.$ParQuestionNo.')" class="hidedata" id="displayStatistics_'.$ParQuestionNo.'">'.$Lang['IES']['HideRecord'].'</a></li>';
		$html .= "</ol>\n";
		$html .= '<br class="clear" /><p class="spacer"></p><!-- html layout handling , don\'t remove  -->';
		$html .= "</div>";
		$html .= "<div class=\"q_option\" id=\"q_option_".$ParQuestionNo."\">".$ParPlainTable;
		$html .= "</div>";

		/*
		if (empty($ParMappingID)) {
			$ParMappingID = -1;
		} else {
			$h_Checked = " checked='checked' ";
			$mappingDetailArray = current($this->loadSurveyMapping("","",$ParMappingID));
		}

		if($this->get_display_survey_mapping_option()){
			$h_hide = $h_Checked? "" : "display:none";

			if($ParDisplayCreateNewQuestion == 1){
				$html .= <<<HTMLEND

					<div style="text-align:left">

					<input type="checkbox" name="checkCurrent" id="checkCurrent" value="{$ParMappingID}" $h_Checked onClick="showHideCurrentTitle()"/>
						<label for="checkCurrent">{$Lang['IES']['IsFocusQuestion']}</label>

						<div id="currentTitleDiv" style="$h_hide">
						<br/>
						{$Lang['IES']['CombinationName']} :
						<input type="text" name="currentTitle" id="currentTitle" value="{$mappingDetailArray["MAPPINGTITLE"]}"/>
						</div>
					</div>
HTMLEND;
			}

		}

		if($ParDisplayCreateNewQuestion == 1){
			if ($ParIsAddButton) {
				$html .=<<<HTMLEND
				<div class="btn">
				<input type="button" value="{$Lang['IES']['newgrouping']}" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" class="formbutton" name="submit2" onClick="addCombineBox()">
				</div>
HTMLEND;
			}
		}
		*/
		$html .= "</div>";

		return $html;
	}



	function genPlainTableForMC($ParResultArray){
		$return = "<table class=\"common_table_list\">";

		if (is_array($ParResultArray)) {
			$total = 0;
			foreach($ParResultArray as $key => $element) {
				$total += $element["VALUE"];
			}

			foreach($ParResultArray as $key => $element) {
				if($total != 0){
				$elementValue = $element['VALUE'] / $total;
				$elementValue *= 100;
				$elementValue = round($elementValue, 0);
				}
				else{
					$elementValue = 0;
				}
//					////debug_r($this->trimOptions($element["OPT_POS"]));
				$return .= "<tr>";
				$return .= "<td>".$this->trimOptions($element["OPT_POS_1"])."</td>";
				$return .= "</td><td class=\"tool_files_col\" nowrap=\"nowrap\">".$element['VALUE']."({$elementValue}%)</td>";
				$return .= "</tr>";

			}
		}
		$return .= "</table>";
		return $return;
	}

	function genPlainTableForText($ParResultArray){

		$return = "<table class=\"common_table_list\">";
		//////debug_r($ParResultArray);
		if (is_array($ParResultArray)) {


			foreach($ParResultArray["ANSWER"] as $key => $element) {

				$return .= "<tr>";
				$return .= "<td>".nl2br($element)."</td>";
				$return .= "</tr>";
			}
		}
		$return .= "</table>";
		return $return;
	}

	function genPlainTableForLS($ParResultArray, $ParQuestionNumber,$ParQuestionsArray,$ParSubquestionNumber=0){
		//debug_r($ParResultArray);
		$return = "<table class=\"common_table_list\">";
		$return .= "<tr>";
		$return .= "<th class=\"num_check\">#</th><th>問題</th>";
		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["OPTION_NUM"]; $i++){
			$return .= "<th class=\"sub_row_top\">".$this->trimOptions($ParResultArray[0][$i]["OPT_POS_1"])."</th>";
		}
		$return .= "</tr>";
		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["QUESTION_NUM"]; $i++){
			if (is_array($ParResultArray[$i])) {
				if (!empty($ParSubquestionNumber)) {
					$q_num = $ParSubquestionNumber;
				} else {
					$q_num = $i + 1;
				}
				$return .= "<tr>";
				$return .= "<td>{$q_num}</td>";
				$return .= "<td>".$ParQuestionsArray[$ParQuestionNumber]["QUESTION_QUESTIONS"][$i]."</td>";
				foreach($ParResultArray[$i] as $key => $element) {
					$return .= "<td class=\"tool_files_col\" nowrap=\"nowrap\">".$element['VALUE']."</td>";
				}
				$return .= "</tr>";
			}
		}
		$return .= "</table>";
		return $return;
	}

	/**
	 * Get the result of analysis
	 * @param INT $ParQuestionNo - the question number, start from 1
	 *
	 * @param Array $ParQuestionElements - array element generated by function breakQuestionsString()
	 * @param  String $ParAnswer - answer generated by function getSurveyAnswer()
	 * @return Array	- answer Format => Array["TITLE"] = title, Array["ANSWER"]=> ARRAY()
	 */
	function getAnsTextResultArray($ParQuestionNo,$ParQuestionElements,$ParAnswer){
		global $ies_cfg;
		$Answer = array();
		$title = $ParQuestionElements[$ParQuestionNo]["TITLE"];
		$Answer["TITLE"] = $title;
		$Answer["ANSWER"] = array();
		foreach($ParAnswer as $answerKey=>$answerElement) {
			$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$answerElement);
			$Answer["ANSWER"][] = $answerArray[$ParQuestionNo];
		}
		return $Answer;
	}

	static public function breakEncodedURI($ParKey) {
		$decodedKey = base64_decode($ParKey);
		$parameters = array();
		foreach(explode("&",$decodedKey) as $pKey => $parameter) {
			$paraNameValuePair = explode("=",$parameter);
			$paraName = $paraNameValuePair[0];
			$parVar = $paraNameValuePair[1];
			$parameters[$paraName] = $parVar;
		}
		return $parameters;
	}

	public function getFormattedSurveyTitle($ParSurveyType,$ParSurveyTypeName,$ParTitle,$ParOriginalKey,$ParIsIESStudent) {
		if (!$ParIsIESStudent) {
			global $intranet_session_language,$Lang;
			$html_studentSelectionBox = $this->getStudentSurveySelectionBox($ParOriginalKey,$ParSurveyType);
		}
		$returnString = "<span>$ParSurveyTypeName</span>".$html_studentSelectionBox."<br/>".$ParTitle;
		return $returnString;
	}

	public function getSurveyAnsweredRespondentList($ParSurveyID){
		global $intranet_db, $ies_cfg;

		$sql = "SELECT isa.SurveyID, isa.AnswerID, isa.Respondent, date_format(isa.DateInput, '%e-%c-%Y') as InputDate, isa.Type, isa.Status, isa.RespondentNature, ".getNameFieldWithClassNumberByLang ("iu.")." AS StudentName, isa.UserID FROM {$intranet_db}.IES_SURVEY_ANSWER isa " .
				"LEFT JOIN {$intranet_db}.INTRANET_USER iu ON isa.UserID = iu.UserID WHERE isa.SurveyID = '$ParSurveyID' ORDER BY isa.DateInput DESC";
//		DEBUG($sql);
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	public function DeleteSurveyAnswer($ParAnswerID){
		global $intranet_db;

		$sql = "DELETE FROM IES_SURVEY_ANSWER WHERE AnswerID = $ParAnswerID";

		$result = $this->db_db_query($sql);
		return $result;
	}

	public function UpdateSurveyAnswerStatus($ParSurveyID, $ParRespondent, $ParStatus, $ParRespondentNature){
		global $intranet_db, $ies_cfg;
		if($ParRespondentNature != $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
			$sql = "Update IES_SURVEY_ANSWER SET status = '{$ParStatus}' WHERE SurveyID = '{$ParSurveyID}' AND Respondent = '{$ParRespondent}' AND RespondentNature = $ParRespondentNature";
		}
		else{
			$sql = "Update IES_SURVEY_ANSWER SET status = '{$ParStatus}' WHERE SurveyID = '{$ParSurveyID}' AND UserID = '{$ParRespondent}' AND RespondentNature = '$ParRespondentNature'";
		}
		$result = $this->db_db_query($sql);
		return $result;
	}

	public function splitAnswer($ParAnswerArray) {
		global $ies_cfg;
		$returnArray = array();
		if(is_array($ParAnswerArray)){
			foreach($ParAnswerArray as $key=>$ParAnswer){
				$answerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswer);
				if (is_array($answerArray)) {
					foreach($answerArray as $answerArrayKey => $answerArrayElement) {
						$eachAnswerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answerArrayElement);
						if ($answerArrayKey == 0) {
							// do nothing
						} else {
							$tempArray[$answerArrayKey] = $eachAnswerArray;
						}

					}
				}
				$returnArray[] = $tempArray;
			}

		}
		return $returnArray;
	}

	/**
	 * @param INT $ParTaskID - the taskid
	 * @param String $ParKey - the original key, URL
	 * @param String $ParSelectedValue - the selected option of the selection box
	 * @return String - HTML of a selection box
	 */
	public function getStudentSurveySelectionBox($ParKey,$ParSurveyType) {
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libuser.php");

		$brokenKey = $this->breakEncodedURI($ParKey);
		$TaskID = $brokenKey["TaskID"];
		$students = $this->getTaskStudent($TaskID);
		$parentIds = current($this->getParentsIds("TASK",$TaskID));


		$keyStudentNamePairs = array();
		if (is_array($students)) {
			foreach($students as $studentsKey => $studendId) {
				$studentStageSurvey = $this->getStudentStageSurvey($parentIds["STAGEID"],$studendId);
				$surveyId = $studentStageSurvey[$ParSurveyType];

				# replace new values into the key array
				$tempKey = $brokenKey;
				$tempKey["SurveyID"] = $surveyId;
				$tempKey["StudentID"] = $studendId;
				$newKeyArray = array();
				foreach($tempKey as $key=>$element) {
					$newKeyArray[] = $key."=".$element;
				}
				$newKey = base64_encode(implode("&",$newKeyArray));
				$libuser = new libuser($studendId);
				$keyStudentNamePairs[] = array($surveyId,$newKey,$libuser->UserNameLang());
			}
		}

		//-- Start: SELECTION BOX --//
		if (is_array($keyStudentNamePairs) && count($keyStudentNamePairs)>0) {
			$html = "<select name='student_survey_selection' onChange='goOtherStudent(this.value);'>";
			foreach($keyStudentNamePairs as $key => $element) {
				$surveyId = $element[0];
				$value = $element[1];
				$name = $element[2];
				$style  = "";
				if (empty($surveyId)) {
					$optionType = "optgroup";
					$style = " style='color: #CCCCCC'";
				} else {
					$optionType = "option";
				}
				if ($ParKey == $value) {
					$selected = "selected='selected'";
				} else {
					$selected = "";
				}
				$html .= "<$optionType $style $selected value='$value' label='$name'>";
				$html .= $name;
				$html .= "</$optionType>";
			}
			$html .= "</select>";
		} else {
			$html = "";
		}
		//-- End: SELECTION BOX --//
		return $html;
	}

	/**
	 * @author maxwong
	 * @param INT $ParSurveyID - SurveyID
	 * @param INT $ParStudentID - StudentID
	 * @param Array $ParSurveyMappingIDInSeq - array of survey mapping id in sequence
	 */
	public function updateMappingSequence($ParSurveyID,$ParStudentID,$ParSurveyMappingIDInSeq) {
		global $intranet_db;
		# Clear old sequence
		$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING SET SEQUENCE = NULL WHERE SURVEYID = '{$ParSurveyID}' AND USERID = '{$ParStudentID}'";
		$result["clear"] = $this->db_db_query($sql);

		if ($result["clear"]) {
			if (is_array($ParSurveyMappingIDInSeq)) {
				$i = 1;
				foreach($ParSurveyMappingIDInSeq as $key => $SurveyMappingID) {
					$sql = "UPDATE {$intranet_db}.IES_SURVEY_MAPPING SET SEQUENCE = '{$i}' WHERE SURVEYMAPPINGID = '{$SurveyMappingID}'";
					$result["updateSeq"][] = $this->db_db_query($sql);
					$i++;
				}
			}
		}
		return $result;
	}

	function set_display_survey_mapping_option($enable){
		# set to false when obj created
		$this->display_survey_mapping_option = $enable;
	}

	function get_display_survey_mapping_option(){
		return $this->display_survey_mapping_option;
	}

	/**
	* Return Default Question by the Survey Type
	* @owner : Fai (201000922)
	* @param : Integer $parSurveyType, the survey type that need to return
    * @param : Integer $parSurveyLang, the survey lang that need to return
	* @return : String , A concat value of the question type
	*
	*/

	function getDefaultQuestion($parSurveyType,$parSurveyLang = ''){
		global $ies_cfg;

		$parSurveyLang = ($parSurveyLang == '')?$ies_cfg['DB_IES_DEFAULT_SURVEY_QUESTION']['B5']:$parSurveyLang;
		$returnStr = '';
		if(is_numeric($parSurveyType)){
			$sql  = ' select GROUP_CONCAT(QuestionStr SEPARATOR \'\') as `question`';
			$sql .= ' from '.$intranet_db.'.IES_DEFAULT_SURVEY_QUESTION ';
			$sql .= ' where ';
			$sql .= ' DefaultQuestion = '.$ies_cfg['Questionnaire']['isDefaultQuestion'].' and SurveyType = \''.$parSurveyType.'\' ';
			$sql .= ' and LangType = \''.$parSurveyLang.'\' ';
			$sql .= ' order by DisplayOrder';
//debug_r($sql);


			$result = $this->returnArray($sql,'',1);
			if(is_array($result) && count($result) ==1){
				$returnStr = $result[0]['question'];
			}
		}

		return $returnStr;
	}
}




?>