<?php
class ImportDefaultCommentBank {
	public function import() {
		global $intranet_root,$intranet_db, $UserID;
		########### Start : include default comment
		include_once($intranet_root."/home/eLearning/ies/admin/settings/default_comments.php");
		$temp = $temp;
		$array = explode("\n",$temp);
		$array = array_map("trim",$array);
		$array = array_filter($array);
		
		$_currentLang = "B5";
		$catCount = -1;
		foreach($array as $key => $element) {
			$delimiter = substr($element,0,2);
			switch($delimiter) {
				case "##":	//	Category
				$catCount++;
				$_tmpCategory = explode($delimiter,$element);
				$categories[$catCount][TITLE] = array("B5"=>$_tmpCategory[1],"EN"=>$_tmpCategory[2]);
				
				break;
				case ":-":	//	Comment Lang
				$_currentLang = explode($delimiter,$element);
				$currentLang = strtoupper($_currentLang[1]);
				break;
				case "--":	// Comment
				$categories[$catCount][COMMENTS][$currentLang][] = substr($element,2);
				break;
			}
		}
		########### End : include default comment
		
		
		$libdb = new libdb();
		$libdb->Start_Trans();
		
		foreach($categories as $key => $category) {
			$titles = $category[TITLE];
			$commentsB5 = $category[COMMENTS][B5];
			$commentsEN = $category[COMMENTS][EN];
			$titles = array_map(array("libdb","Get_Safe_Sql_Query"),$titles);
			$commentsB5 = array_map(array("libdb","Get_Safe_Sql_Query"),$commentsB5);
			$commentsEN = array_map(array("libdb","Get_Safe_Sql_Query"),$commentsEN);
			$sizeOfComments = null;
			if (count($commentsB5) != count($commentsEN)) {
				$result[] = false;
				$this->writeLog("Different comments size of B5 and EN in [{$titles[B5]}]/[{$titles[EN]}]");
				break;
			} else {
				$sizeOfComments = count($commentsB5);
			}
			$this->writeLog("<hr/><font style='color:green;'><b>Create Category</b></font>");
			$sql = "INSERT INTO {$intranet_db}.IES_COMMENT_CATEGORY SET TITLE = '{$titles[B5]}', TITLEEN = '{$titles[EN]}', DATEINPUT = NOW(), INPUTBY = '$UserID'";
				$this->writeLog($sql);
			$_tmpResult = $libdb->db_db_query($sql);
			$newCategoryID = null;
			if ($_tmpResult) {
				$newCategoryID = mysql_insert_id();
				$result[] = "Category: ".$newCategoryID;
				$this->writeLog("New Category [{$newCategoryID}] is created...");
			} else {
				$result[] = false;
				$this->writeLog(mysql_error());
				break;
			}
			$this->writeLog("");
			if ($newCategoryID) {
				for($i=0;$i<$sizeOfComments;$i++) {
					$this->writeLog("<font style='color:tan;'>Create Comment</font>");
					$sql = "INSERT INTO {$intranet_db}.IES_COMMENT SET CATEGORYID = $newCategoryID, COMMENT = '{$commentsB5[$i]}', COMMENTEN = '{$commentsEN[$i]}', DATEINPUT = NOW(), INPUTBY = '$UserID'";
					$this->writeLog($sql);
					$_tmpResult = $libdb->db_db_query($sql);
					if ($_tmpResult) {
						$newCommentID  = mysql_insert_id();
						$result[] = "Comment: ".$newCommentID;
						$this->writeLog("New Comment [{$newCommentID}] is created...");
					} else {
						$result[] = false;
						$this->writeLog(mysql_error());
						break 2;
					}
				}
				
			}
			
		}
//		debug_r($result);
		$this->writeLog("");
		$this->writeLog("");
		if (in_array(false,$result)) {
			$this->writeLog("Import Failed and Rollback");
			$libdb->RollBack_Trans();
		} else {
			$this->writeLog("Import Success");
			$libdb->Commit_Trans();
			$finalResult = true;
		}
		$this->writeLog("######################################################################");
		return $finalResult;
	}
	function writeLog($ParContent, $ParShowToScreen=false) {
		global $intranet_root;
		$ParContent .= "\n";
		$today = date("Ymd");
		if ($ParShowToScreen) {
			echo $ParContent."<br/>";
		}
		
		$logPath = $intranet_root."/file/ies/log";
		if(!file_exists($logPath))
		{
		  mkdir($logPath, 0777, true);
		}
		$logFile = "$logPath/import_default_comments_$today.log";
		error_log($ParContent, 3, $logFile);
	}
}

?>