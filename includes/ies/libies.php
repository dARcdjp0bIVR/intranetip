<?php
#  Editing by :
// error_log($sql, 3, "/home/web/eclass40/intranetIP25/file/aa");
######################################
# -	Table handling function is used to handle specified DB table,
# 	Correspoding assistance functions should be implemented to avoid direct use of the core handling function
#	You can use the Bookmark: TABLE_HANDLING to search the position for those functions
######################################

/*****************************************************
 * Modification Log
 *
 * Date		:	2011-01-14 Thomas
 * Detail	:	Modified function getStudentSurveyMappingInfoBySchemeID(), add Survey Title in returnArray
 *
 * Date		:	2011-01-12 Ivan
 * Detail	:	Added function getStudentSurveyMappingInfo(), getStudentSurveyMappingInfoBySchemeID()
 *
 * Date		:	2011-01-10 Thomas (201101101121)
 * Detail	:	Added functions GET_SECTION_ARR(), GET_SECTION_TASK_ARR()
 *
 * Date		:	2010-12-13 Ivan (201012131223)
 * Detail	:	modified functions GET_MODULE_OBJ_ARR(), added "?clearCoo=1" in the link to "Mgmt > Student Progress"
 *
 * Date		:	2010-08-19 Max (201008171012)
 * Detail	:	Added functions refineSurveyEmailCsvDataToRequiredFormat(),newSurveyEmailList(),updateSurveyEmailList(),getSurveyEmailList(),combineOldNewSurveyEmailList(),getSurveyEmailCsvData(),checkSurveyEmailCsvData(),checkSurveyEmailCsvHeader(),getSurveyEmailErrorMessage(),getSurveyEmailDetails(),updateSurveyEmailContents(),newSurveyEmailContents(),generateSurveryEmailCheckBoxList(),getSurveyEmailListPureEmail(),sendEmailForSurveyEmail(),updateEmailListField(),getSurveyDetails()
 *
 * Date		:	2010-08-04 Max (201008041036)
 * Detail	:	Updated GET_SCHEME_DETAIL to get also the scheme version
 *
 * Date		:	20100625 (Henry Chow)
 * Detail	:	added isIES_Teacher() & isIES_Student, for access right checking
 *
 * Date		:	20100617 (Henry Chow)
 * Detail	:	added getFAQData(), retrieve FAQ data
 *
 * Date		:	20100615 (Henry Chow)
 * Detail	:	modified GET_MODULE_OBJ_ARR(), add menu option "FAQ"
 *
/*****************************************************/
//include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
include_once($intranet_root."/lang/ies_lang.".(empty($_SESSION['IES_CURRENT_LANG'])?$intranet_session_language:$_SESSION['IES_CURRENT_LANG']).".php");
include_once($intranet_root."/includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

class libies extends libaccessright {

	public function libies(){
		//do nothing now
		$this->libdb();
	}
	/**
	 * Get MODULE_OBJ array for generation of left menu
	 *
	 * @return array Array contains the required info for generating left menu
	 */
	public function GET_MODULE_OBJ_ARR(){

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPage, $CurrentPageArr;
		global $top_menu_mode,$ies_lang, $Lang;
		global $plugin, $UserID;

		// Logo link
		$MODULE_OBJ['root_path'] = "/home/eLearning/ies/admin/management/marking";

		// Current Page Information init
		$PageManagement = 0;
		$PageSettings = 0;

		switch ($CurrentPage) {
			case "StudentProgress":
				$PageManagement = 1;
				$PageManagement_StudentProgress = 1;
				break;
			case "FAQ":
				$PageManagement = 1;
				$PageManagement_FAQ = 1;
				break;
			case "Worksheet":
				$PageManagement = 1;
				$PageManagement_Worksheet = 1;
				break;
			case "SchemeSettings":
				$PageSettings = 1;
				$PageSettings_SchemeSettings = 1;
				break;
			case "DefaultSurveyQuestion":
        $PageSettings = 1;
				$PageSettings_DefaultSurveyQuestion = 1;
				break;
			case "CommentBank":
				$PageSettings = 1;
				$PageSettings_CommentBank = 1;
				break;
			case "UploadSettings":
				$PageSettings = 1;
				$PageSettings_UploadSettings = 1;
				break;
			case "RubricSettings":
				$PageSettings = 1;
				$PageSettings_RubricSettings = 1;
				break;
		}

		// Menu information
		//$MenuArr["Management"] = array($ies_lang['Management'], $PATH_WRT_ROOT."home/eLearning/ies/index.php", $PageStatistics);

		$MenuArr["Management"] = array($ies_lang['Management'], "", $PageManagement);
//		$MenuArr["Management"]["Child"]["StudentProgress"] = array($ies_lang['StudentProgress'], $PATH_WRT_ROOT."home/eLearning/ies/admin/index.php", $PageManagement_StudentProgress);


		if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
			$MenuArr["Management"]["Child"]["StudentProgress"] = array($ies_lang['StudentProgress'], $PATH_WRT_ROOT."home/eLearning/ies/admin/management/marking/index.php?clearCoo=1", $PageManagement_StudentProgress);
			$MenuArr["Management"]["Child"]["Worksheet"] = array($Lang['IES']['Worksheet'], $PATH_WRT_ROOT."home/eLearning/ies/admin/management/worksheet/", $PageManagement_Worksheet);
			$MenuArr["Management"]["Child"]["FAQ"] = array($Lang['IES']['FAQ'], $PATH_WRT_ROOT."home/eLearning/ies/admin/management/faq/", $PageManagement_FAQ);

		}

		if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"]) {
			$MenuArr["Settings"] = array($ies_lang['Settings'], "", $PageSettings);
			$MenuArr["Settings"]["Child"]["SchemeSettings"] = array($Lang['IES']['Scheme'], $PATH_WRT_ROOT."home/eLearning/ies/admin/settings/scheme_index.php", $PageSettings_SchemeSettings);
			$MenuArr["Settings"]["Child"]["StudentLicense"] = array($ies_lang['StudentLicense'], "javascript:newWindow('/home/moduleLicense/module_license.php?ModuleCode=ies','31')", $PageManagement_StudentLicense);
			$MenuArr["Settings"]["Child"]["CommentBank"] = array($Lang['IES']['CommentBank'], $PATH_WRT_ROOT."home/eLearning/ies/admin/management/commentbank/", $PageSettings_CommentBank);
			$MenuArr["Settings"]["Child"]["UploadSettings"] = array($Lang['IES']['UploadSettings'], "javascript: var uploadSettingWin = newWindow('".$PATH_WRT_ROOT."ipfiles/iportal_file_settings.php?moduleCode=ies','6');", $PageSettings_UploadSettings);

			//for tempraory "DEV" 20100910
//
        $MenuArr["Settings"]["Child"]["DefaultSurveyQuestion"] = array($Lang['IES']['DefaultSurveyQuestion'], $PATH_WRT_ROOT."home/eLearning/ies/admin/settings/default_question/", $PageSettings_DefaultSurveyQuestion);
//		if(strtoupper($plugin['IES_ENV']) == "DEV"){
				$MenuArr["Settings"]["Child"]["Rubric"] = array($Lang['IES']['RubricSetting'], "javascript: var uploadSettingWin = newWindow('/home/eLearning/ies/LoginEclass.php','31');", $PageSettings_RubricSettings);
//			}
		}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass LSIES";'."\n";
        $js.= '</script>'."\n";
        $MODULE_OBJ['title'] .= $js;

		// module information
		$MODULE_OBJ['title'] = $ies_lang['IES_TITLE'].$js;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_IES_setting.png";
		$MODULE_OBJ['menu'] = $MenuArr;

		return $MODULE_OBJ;
	}

  /**
   * Load paper frame info
   * @param INT $StageSeq - stage sequence
   * @return Array
   */
	public function GET_PAGEFRAME_INFO($StageSeq=1)
	{
    switch($StageSeq)
    {
      case 1:
      default:
        $templateInfo["background_class"] = "student_stage1";
        break;
      case 2:
        $templateInfo["background_class"] = "student_stage2";
        break;
      case 3:
        $templateInfo["background_class"] = "student_stage3";
        break;
    }
    return $templateInfo;
  }

	public function GET_CURRENT_STEP_ANSWER($StepID, $StudentID, $QuestionCode=""){
    global $intranet_db;

    $conds = ($QuestionCode=="") ? "" : " AND QuestionCode = '{$QuestionCode}'";

    $sql =  "
              SELECT
                AnswerID,
                QuestionCode,
                QuestionType,
                Answer,
                AnswerActualValue,
                AnswerExtra,
                DATE_format(DateModified, '%Y-%m-%d') as \"DateModified\"
              FROM
                {$intranet_db}.IES_QUESTION_HANDIN
              WHERE
                StepID = '{$StepID}' AND
                UserID = '{$StudentID}'
                {$conds}
            ";
    $returnArr = $this->returnArray($sql);
    return $returnArr;
  }

  public function GET_SCHEME_ARR(){
    global $intranet_db,$ies_cfg;
	global $UserID;

	//handle a teacher login , show his / her scheme only
	$teacherSQLQuery = "";
	$teacherCriteria = "";
	$distinct = "";
	if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"]){
		//check not a IES Admin login
		//do nothing
	}else{
		$teacherSQLQuery = " LEFT JOIN {$intranet_db}.IES_SCHEME_TEACHER ies_st ON ies_st.SchemeID = IES_SCHEME.SchemeID ";
		$teacherCriteria = " and ies_st.UserID = '{$UserID}' ";  // the teacher id is equal to this teacher login
		$distinct = " distinct "; // to avoid duplicate scheme display while a teacher is both a teaching teacher and marking teacher (left join) IES_SCHEME_TEACHER
	}

    $sql =  "
              SELECT
		  	    {$distinct}
                IES_SCHEME.SchemeID as 'SchemeID',
                Title
              FROM
                {$intranet_db}.IES_SCHEME
			    {$teacherSQLQuery}
			  where 1
			    and IES_SCHEME.SchemeType = '".$ies_cfg["moduleCode"]."'
		  	    {$teacherCriteria}
			  ORDER BY Title
            ";

    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }

  public function GET_SCHEME_DETAIL($SchemeID){
    global $intranet_db;

    $sql =  "
							SELECT
								Title,
								SchemeID,
								DateInput,
								InputBy,
								DateModified,
								ModifyBy,
								Version,
								Language,
								SchemeType,
								MaxScore
              FROM
                {$intranet_db}.IES_SCHEME
              WHERE
                SchemeID = '{$SchemeID}'
            ";
    $returnArr = $this->returnArray($sql);

    return current($returnArr);
  }

  public function GET_STAGE_ARR($SchemeID){
    global $intranet_db;

    $sql =  "
              SELECT
                StageID,
                Title,
								Sequence,
								DATE_format(Deadline, '%Y-%m-%d') AS Deadline,
								Weight,
								MaxScore
              FROM
                {$intranet_db}.IES_STAGE
              WHERE
                SchemeID = '{$SchemeID}'
              ORDER BY
                Sequence
            ";
    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }

  public function GET_STAGE_DETAIL($StageID){
    global $intranet_db;

    $sql =  "
              SELECT
                Title,
                Sequence,
                DATE_format(Deadline, '%Y-%m-%d') AS Deadline,
                SchemeID,
								Description,
								Weight,
								MaxScore
              FROM
                {$intranet_db}.IES_STAGE
              WHERE
                StageID = '{$StageID}'
            ";
    $returnArr = $this->returnArray($sql);

    return current($returnArr);
  }

  public function GET_TASK_ARR($StageID){
    global $intranet_db;

    $sql =  "
              SELECT
                TaskID,
                Title,
                Sequence
              FROM
                {$intranet_db}.IES_TASK
              WHERE
                StageID = '{$StageID}'
              ORDER BY
                Sequence
            ";
    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }

  public function GET_TASK_DETAIL($TaskID){
	  global $intranet_db;

    $sql =  "
              SELECT
                TaskID,
                Title,
                Description,
                InstantEdit,
				        StageID
              FROM
                {$intranet_db}.IES_TASK
              WHERE
                TaskID = '{$TaskID}'
            ";
	  $taskArr = current($this->returnArray($sql));

	  return $taskArr;
  }

	public function getTaskStepArr($parTaskID)
	{
	  global $intranet_db;


	//  $sql = "SELECT StepID, StepNo, Title, Sequence FROM {$intranet_db}.IES_STEP WHERE TaskID = {$parTaskID} ORDER BY Sequence";
	  $sql = "SELECT
				step.StepID, step.StepNo, step.Title, step.Sequence as step_seq,
				task.Sequence as task_seq,
				stage.Sequence as stage_seq
				FROM
					{$intranet_db}.IES_STEP as step
				inner join
					{$intranet_db}.IES_TASK as task on step.taskid = task.taskid
				inner join
					{$intranet_db}.IES_STAGE as stage on task.StageID = stage.StageID
				WHERE
					step.TaskID = '{$parTaskID}'
				ORDER BY step.Sequence";
	  $taskStepArr = $this->returnArray($sql);

	  return $taskStepArr;
	}

	public function generateHandinTaskCommentPartHtml($comment_info) {
		$html_comment_list = "";
		if (is_array($comment_info)) {
			foreach($comment_info as $cKey => $cElement) {
			$html_comment_list .= <<<HTMLEND
		      {$cElement["COMMENT"]}
		      <br /><span class="update_record">({$cElement["DATEINPUT"]} 由{$cElement["COMMENTTEACHER"]})</span><br />
HTMLEND;
			}
		}
		return $html_comment_list;
	}

	public function getAnswerArea($ParTaskId, $ParStepId, $ParUserId, $ParTemplate, $ParDisplayAllStep="stick_to_scheme_model",$ParIsShowDate=false) {
		global $intranet_root;
		//INCLUDE THE SCHEME MODEL / DATA / CONFIG FILE FOR THIS STEP , ALL THE VARIABLE IN THIS FILE SHOULD BE STARTED WITH $schemeCfg_
		$schemeModelFile = "$intranet_root/home/eLearning/ies/coursework/templates/default/scheme_model.php";
		include_once($schemeModelFile);
		$step_arr = $this->GET_STEP_ARR($ParTaskId);
		$task_step_arr = $this->getTaskStepArr($ParTaskId);
		$stepDetails = $this->getStepDetail($ParStepId);

		list($stage_id, $stage_seq, $stage_title, $task_id, $task_title, $step_title, $step_no, $step_seq) = $stepDetails;
		$step_raw_ans_forDisplayArr = $this->getAllStepAnswerForDisplay($ParTaskId,$ParUserId);

		$paddedQuestions = $this->formatQuestions($ParTaskId,$schemeCfg_TaskQuestion,$step_arr);
		if ($ParDisplayAllStep == "stick_to_scheme_model") {
			$ParDisplayAllStep = $schemeCfg_AccumulateToDisplayAllStepAns;
		} else {
			// do nothing
		}
		$ParDisplayAllStep = false;
		$answer_display = //$this->getHTMLAnswerArea($step_raw_ans_forDisplayArr,$task_step_arr,$stepCfg_skip_displayStepSeq,$paddedQuestions,$step_seq,$schemeCfg_AccumulateToDisplayAllStepAns,$ParTemplate,$ParIsShowDate);
		$this->getHTMLAnswerArea($step_raw_ans_forDisplayArr,$task_step_arr,$stepCfg_skip_displayStepSeq,$paddedQuestions,$step_seq,$ParDisplayAllStep,$ParTemplate,$ParIsShowDate);
		return $answer_display;
	}
	/**
	* GET THE STUDENT INPUT ANSWER FROM A RAW DATA
	*
	* @param : INT $stageSeq ,					require stage sequence
	* @param : INT $taskSeq ,					require task sequence
	* @param : INT $stepSeq,					require step sequence
	* @param : INT $questionNo,				require question sequence
	* @param : Array $studentRawAnswerArray,	student input raw data
	* @return : STRING  , Answer of the request question
	*/
	public function getQuestionAnswer($stageSeq,$taskSeq,$stepSeq,$questionNo,$studentRawAnswerArray,$ParStepId,$ParIsShowDate)
	{
		global $ies_cfg,$intranet_root;

		for($i = 0;$i < sizeof($studentRawAnswerArray);$i++)
		{
	//		$returnAnswer = "--";
			$_QuestionType = $studentRawAnswerArray[$i]["QuestionType"];
			$_QuestionCode = $studentRawAnswerArray[$i]["QuestionCode"];

			$_Answer = htmlspecialchars($studentRawAnswerArray[$i]["Answer"]);
			$_AnswerActualValue = htmlspecialchars($studentRawAnswerArray[$i]["AnswerActualValue"]);
			$_AnswerExtra = htmlspecialchars($studentRawAnswerArray[$i]["AnswerExtra"]);
			$_s_StepID = $studentRawAnswerArray[$i]["s_StepID"];
			$_StepSeq	= $studentRawAnswerArray[$i]["StepSeq"];
			$_TaskID	= $studentRawAnswerArray[$i]["TaskID"];
			$_TaskSeq	= $studentRawAnswerArray[$i]["TaskSeq"];
			$_StageID	= $studentRawAnswerArray[$i]["StageID"];
			$_StageSeq	= $studentRawAnswerArray[$i]["StageSeq"];
			$_DateModified = $studentRawAnswerArray[$i]["DateModified"];

			if($_StageSeq == $stageSeq && $_TaskSeq==$taskSeq && $stepSeq==$_StepSeq && $questionNo == $_QuestionCode)
			{
				switch(strtoupper($_QuestionType))
				{
					case "TEXT" :// SAME AS TEXTAREA
					case "TEXTAREA":
						$returnAnswer = nl2br($_Answer);
						break;
					//case "CHECKBOX":
					case "RADIO":
						$returnAnswer = "";

						$tempAnswer = explode($ies_cfg["question_answer_sperator"],$_AnswerActualValue);

						if(is_array($tempAnswer))
						{	//student has answer
							$_mark = $tempAnswer[0];
							$_comment = $tempAnswer[1];
							$returnAnswer = $_comment;
						}
						break;
					case "SELECT_MOD":
/*
						//replace the delimiter with ,
						$tempAnswer = $_AnswerActualValue.$ies_cfg["question_answer_sperator"].$_AnswerExtra;
						$tempAnswer = str_replace($ies_cfg["question_answer_sperator"],",",$tempAnswer);

						//REMOVE THE LAST OCCURRENCE OF THE SPERATOR
				        $tempAnswer = substr($tempAnswer,0, 0 - intval(strlen(",")));
*/
            $tempAnswer[] = str_replace($ies_cfg["question_answer_sperator"],",",$_AnswerActualValue);
            $tempAnswer[] = str_replace($ies_cfg["question_answer_sperator"],",",$_AnswerExtra);

						$returnAnswer = implode(",",array_filter($tempAnswer));
						break;
					case "TABLE":
						$returnAnswer = $this->getQuestionAnswer_Table($_Answer,$ParStepId);
//						$stepDetails = $this->getStepDetail($ParStepId);
//						list($stage_id, $stage_seq, $stage_title, $task_id, $task_title, $step_title, $step_no, $step_seq) = $stepDetails;
//						$stepModelFile = "$intranet_root/home/eLearning/ies/coursework/templates/default/{$stage_seq}/{$step_no}/model.php";
//
//						global $Lang;
//						include($stepModelFile);
//						$HTML = "
//						<table style='empty-cells:show'>
//							<tr><th>";
//
//						//variable $_question get from include file
//						$HTML .= implode("</th><th>",$_question[1]);
//						$HTML .= "</th></tr>
//							{{{ ROWS }}}
//						</table>";
//						$rowAnswer = explode($ies_cfg["new_set_question_answer_sperator"],$_Answer);
//						if (count($rowAnswer)>0) {
//							foreach($rowAnswer as $key => $element) {
//								$tdAnswer = explode($ies_cfg["question_answer_sperator"],$element);
//								$rows .= "<tr>";
//								foreach($tdAnswer as $tdKey => $tdValue) {
//									$rows .= "<td>".($tdValue?$tdValue:"--")."</td>";
//								}
//								$rows .= "</tr>";
//							}
//							$returnAnswer = str_replace("{{{ ROWS }}}",$rows,$HTML);
//						}
						break;
					default:
						$returnAnswer = "";
						break;

				}
				break;
			}
		}

		# handle empty return answer and handle pad date
		if (empty($returnAnswer)) {
			$returnAnswer = "--";
		}
		if ($ParIsShowDate && $returnAnswer != "--" && $returnAnswer != "") {
			$returnAnswer .= "<br /><span class=\"update_record\">($_DateModified)</span><br/>";
		}
		return $returnAnswer;


	}

	/**
	 * Format the answer for table, this is a subfunction of getQuestionAnswer
	 */
	public function getQuestionAnswer_Table($ParAnswer, $ParStepId) {
		global $ies_cfg,$intranet_root;
		$stepDetails = $this->getStepDetail($ParStepId);
		list($stage_id, $stage_seq, $stage_title, $task_id, $task_title, $step_title, $step_no, $step_seq) = $stepDetails;
		$stepModelFile = "$intranet_root/home/eLearning/ies/coursework/templates/default/{$stage_seq}/{$step_no}/model.php";

		global $Lang;
		include($stepModelFile);
		$HTML = "
		<table style='empty-cells:show'>
			<tr><th>";

		//variable $_question get from include file
		$HTML .= implode("</th><th>",$_question[1]);
		$HTML .= "</th></tr>
			{{{ ROWS }}}
		</table>";
		$rowAnswer = explode($ies_cfg["new_set_question_answer_sperator"],$ParAnswer);
		if (count($rowAnswer)>0) {
			foreach($rowAnswer as $key => $element) {
				$tdAnswer = explode($ies_cfg["question_answer_sperator"],$element);
				$rows .= "<tr>";
				foreach($tdAnswer as $tdKey => $tdValue) {
					$rows .= "<td>".($tdValue?$tdValue:"--")."</td>";
				}
				$rows .= "</tr>";
			}
			$returnAnswer = str_replace("{{{ ROWS }}}",$rows,$HTML);
		}
		return $returnAnswer;
	}

	/**
	* GENERATE HTML for displaying student input answer for every step
	*
	* @param : ARRAY $studentRawAnswerArray , student answer in the storage
	* @param : ARRAY $stepDetails , value return by function getStepDetail
	* @param : ARRAY $skipDisplayStep , Step Seq in this array will not be display
	* @return : STRING HTML DISPLAY
	*/
	public function getHTMLAnswerArea($studentRawAnswerArray,$task_step_arr,$skipDisplayStep,$thisTaskQuestion,$current_step_seq,$displayAllStep = true,$ParTemplate=null,$ParIsShowDate=false) {


		if($skipDisplayStep == null)
		{
			$skipDisplayStep = array();
		}
		//format $thisTaskQuestion["stageNo"]["taskNo"]["stepNo"][(question / subquestion)], this have a correct ordering]
		$html_answerArea = "";
		//analysis the thisTaskQuestion

		foreach($thisTaskQuestion as $stageNo => $taskDetails)
		{
			// BASE ON $thisTaskQuestion , GO INTO STAGE LEVEL
			$_stageSeq = $stageNo;
			$_taskArray = $taskDetails;
			foreach($_taskArray as $taskNo => $stepDetails)
			{
				// BASE ON $thisTaskQuestion , GO INTO TASK LEVEL
				$_taskSeq = $taskNo;
				$_stepArray = $stepDetails;
				foreach($_stepArray as $stepNo => $questionDetails)
				{
					// BASE ON $thisTaskQuestion , GO INTO STEP LEVEL
					$_stepSeq  = $stepNo;
					$_questionArray = $questionDetails;

					if($current_step_seq == $_stepSeq)
					{
						break;
					}

					//CHECK AND SKIP THE  DISPLAY OF ANY STEP ANSWER IN ARRAY $skipDisplayStep
					if(in_array($_stepSeq,$skipDisplayStep)){
						//do nothing
					}
					else{
						for($j = 0; $j < sizeof($_questionArray);$j++)
						{
							$_questionNo = $j+1;
							$_questionTitle = $_questionArray[$j];

							$stepId = $this->getStepIdByStageSeqTaskSeqStepSeq($_stageSeq,$_taskSeq,$_stepSeq);
							$_studentAnswer = $this->getQuestionAnswer($_stageSeq,$_taskSeq,$_stepSeq,$_questionNo,$studentRawAnswerArray,$stepId,$ParIsShowDate);

							//$html_answerArea .= "<tr><td>{$_stepSeq} <font size=\"-1\">) ".$_questionTitle."</font></td><td class=\"student_ans\">".$_studentAnswer."&nbsp;</td></tr>";
							//$html_answerArea .= "<tr><td>{$_stepSeq} ) ".$_questionTitle."</td><td class=\"student_ans\">".$_studentAnswer."&nbsp;</td></tr>";
							$html_answerArea .= "<tr><td>{$_stepSeq} ) ".$_questionTitle."</td><td class=\"text_stu\">".$_studentAnswer."&nbsp;</td></tr>";
						}
					}

					//USER DON'T WANT TO DISPLAY ALL STEP ANS IN THE ANSWER AREA , ACCUMULATE DISPLAY ANWER TO CURRENT ACCESS STEP
					if($displayAllStep != true)
					{
						//FOR THIS SCHEME , IT NO NEED TO DISPLAY THE ANSWER ACCUMULATLY, BREAK THE LOOP IF $current_step_seq == $_stepSeq
						if($current_step_seq == $_stepSeq)
						{
							break;
						}
					}
				}
			}

		}
		if (isset($ParTemplate)) {
			// do nothing
		} else {
			$ParTemplate = $this->template_AnswerArea01();
		}
		if($html_answerArea == "")
		{
			//do nothing
		}
		else
		{
			$HTML = str_replace("{{{ HTML_ANSWER_AREA }}}",$html_answerArea,$ParTemplate);
		}


		return $HTML;
	}
	/**
	* GET STUDENT ANSWER FOR DISPLAY AREA
	*
	* @param : INT $task_id TASK ID
	* @param : INT $student_id STUDENT USER ID
	* @return : ARRAY array of student all step answer for a task ($task_id)
	*
	*/
	public function getAllStepAnswerForDisplay($task_id,$student_id)
	{
		global $intranet_db;

		  $sql =  "
					SELECT
					  handin.StepID,
					  handin.QuestionType,
					  handin.QuestionCode,
					  handin.Answer,
					  handin.AnswerActualValue,
					  handin.AnswerExtra,
					  handin.DateModified,
					  step.StepID as \"s_StepID\" /*should be same as handin.StepID*/,
					  step.Sequence as StepSeq,
					  task.TaskID,
					  task.Sequence as TaskSeq,
					  stage.StageID,
					  stage.Sequence as StageSeq
					FROM
					  {$intranet_db}.IES_QUESTION_HANDIN handin
					INNER JOIN {$intranet_db}.IES_STEP step
					  ON handin.StepID = step.StepID
					INNER JOIN {$intranet_db}.IES_TASK task
					  ON step.TaskID = task.TaskID
					INNER JOIN {$intranet_db}.IES_STAGE stage
					  ON task.StageID = stage.StageID
					WHERE
					  step.TaskID = '{$task_id}' AND
					  handin.UserID = '{$student_id}'
				  ";

		  $_taskAnswerArr = $this->returnArray($sql);
		return $_taskAnswerArr;


	}
	public function formatQuestions($ParTaskId,$ParInQuestions,$ParStepArr) {

		global $intranet_db;

		$parentsIdsSeqs = current($this->getParentsIds("TASK",$ParTaskId));
		$stageSeq = $parentsIdsSeqs["STAGE_SEQ"];
		$taskSeq = $parentsIdsSeqs["TASK_SEQ"];

		foreach($ParStepArr as $key => $STEP_INFO) {
			$temp = &$ParInQuestions[$stageSeq][$taskSeq][$STEP_INFO["Sequence"]];
			if (isset($temp)) {
				foreach($temp as $questionCode => &$question) {
					if (empty($question)) {
						$question = $STEP_INFO["Title"];
					}
				}
			} else {
				$temp[] = $STEP_INFO["Title"];
			}
		}
		$returnQuestion[$stageSeq][$taskSeq] = $ParInQuestions[$stageSeq][$taskSeq];
		ksort($returnQuestion[$stageSeq][$taskSeq]);
		return $returnQuestion;
	}

  public function GET_STEP_ARR($TaskID){
    global $intranet_db;

    $sql =  "
              SELECT
                StepID,
                Title,
                StepNo,
                Sequence
              FROM
                {$intranet_db}.IES_STEP
              WHERE
                TaskID = '{$TaskID}'
              ORDER BY
                Sequence
            ";
    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }

  public function GET_IES_STUDENT_ARR($YearClassID="", $ExcludeStudentID=array()){
	  global $intranet_db,$intranet_root,$ies_cfg;


		// get the module id of IES
	  include_once($intranet_root."/includes/libIntranetModule.php");
	  $objModuleLicense = new libintranetmodule();
	  $iesModuleID = $objModuleLicense->getModuleID($ies_cfg["moduleCode"]);


	  $conds = ($YearClassID == "") ? "" : " AND ycu.YearClassID = ".$YearClassID;
	  $conds .= empty($ExcludeStudentID) ? "" : " AND iu.UserID NOT IN (".implode(",", $ExcludeStudentID).")";


	  // GET THE STUDENT LIST FROM TABLE : INTRANET_MODULE_USER (authorized student to use IES)

	  $sql =  "
				SELECT
				  iu.UserID,
				  ".getNameFieldWithClassNumberByLang ("iu.")." AS StudentName
				FROM
				  {$intranet_db}.INTRANET_USER iu
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
				  ON iu.UserID = ycu.UserID
				INNER JOIN {$intranet_db}.YEAR_CLASS yc
				  ON yc.YearClassID = ycu.YearClassID
				INNER JOIN {$intranet_db}.INTRANET_MODULE_USER m_user
				  ON iu.UserID = m_user.UserID and m_user.ModuleID = '{$iesModuleID}'
				WHERE
				  1
				  $conds
				ORDER BY ".Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN')." ,
				  ycu.ClassNumber
			  ";
//debug_r($sql);


	  $returnArr = $this->returnArray($sql);

	  return $returnArr;
	}

	public function handleTeacherToScheme($schemeID,$selectTeacherList, $teacherType, $existingTeacherList,$isNewScheme,$creatorID)
	{
		global $intranet_db;

		$objDB = new libdb();
		$removeTeacherSql = "";

		if(!$isNewScheme){
			//NOT A NEW SCHEME
			//REMOVE TEACHER FROM IES_SCHEME_TEACHER WHO DOES NOT IN THE HTML SELECT LIST

			$_selectTeacherIDList = "";

			$_selectTeacherIDList = (sizeof($selectTeacherList) > 0)	?	implode(',',$selectTeacherList):	"";


			if(trim($_selectTeacherIDList) != ""){
				$keepTeaherList = " AND UserID not in (".$_selectTeacherIDList.")";
			}

			//case 1) HTML LIST IS EMPTY. USER REMOVE ALL EXISTING TEACHER FROM THE HTML LIST , THEN $keepTeaherList WILL BECOME ""
			//		  IT WILL REMOVE ALL THE EXISTING TEACHER IN IES_SCHEME_TEACHER WITH THE SPECIFIC TYPE
			//case 2) HTML LIST NOT EMPTY. USER MAY KEEP SOME EXISTING TEACHER , THEN $keepTeaherList WILL HAVE VALUE
			$removeTeacherSql = "delete
										from {$intranet_db}.IES_SCHEME_TEACHER
									where
										SchemeID = '{$schemeID}' and
										TeacherType = '{$teacherType}'
										{$keepTeaherList}
								";

			if($removeTeacherSql != ""){

				$res[] = $objDB->db_db_query($removeTeacherSql);
				$removeTeacherSql = "";
			}

		}
	    for($i=0; $i<count($selectTeacherList); $i++)
		{
			$_teacherIsExistBefore = false;

				//CHECK WHETHER STUDENT EXIST IN THE SCHEME BEFORE
				for($j = 0;$j < sizeof($existingTeacherList);$j++)
				{
					$_existingTeacherID = $existingTeacherList[$j]["UserID"];
					$_existingTeacherType = $existingTeacherList[$j]["TeacherType"];
					if($selectTeacherList[$i] == $_existingTeacherID &&
							$_existingTeacherType == $teacherType
					){
						//studented student exist in the scheme before
						$_teacherIsExistBefore = true;
						break;
					}
				}

				if($_teacherIsExistBefore == false){
					$values[] = "(".$schemeID.", ".$selectTeacherList[$i].", ".$teacherType.",".$creatorID.")";
				}

	    }

		if(sizeof($values) >0)
		{
			$sql = "INSERT INTO {$intranet_db}.IES_SCHEME_TEACHER (SchemeID, UserID, TeacherType, InputBy) VALUES ";

			//case for all teacher is existing this scheme , sizeof($value) = 0
			$sql .= implode(", ", $values);

			$res[] = $objDB->db_db_query($sql);
		}
		unset($values);
		if ($res==NULL) {	// if this is NULL, no update/insert process is being performed
			$res = true;
		}
		return $res;
	}

	/**
	* Generate HTML FUNCTION TAB
	*
	* @param : STRING $currentTagFunction CURRENT USING FUNCTION, for display active tag
	* @param : ARRAY $tabArray ARRAY TO STORE ALL THE TAB NEED TO DISPLAY
	* @return : String HTML FUNCITON TAB
	*
	*/
	public function GET_TAB_MENU($currentTagFunction, $tabArray)
	{

		$x = "";
		$x .= "<div class=\"shadetabs\">";
		$x .= "<ul>";

		if(is_array($tabArray) > 0)
		{
			foreach ($tabArray as $thisTabFunction => $thisDetails)
			{

				$ClassStyle = ($thisTabFunction == $currentTagFunction) ? "selected" : "";

				$Link = $thisDetails[0];   // DATA STRUCTURE the first element is the Link
				$Title = $thisDetails[1];  // DATA STRUCTURE the second element is the Module title

				$x .= "<li class=\"".$ClassStyle."\"><a href=\"".$Link."\"><strong>".$Title."</strong></a></li>";
			}
		}
		$x .= "</ul>";
		$x .= "</div>";

		return $x;
	}

	/**
	* Generate Teaching teacher and AccessTeacher list to HTML
	*
	* @param : String $schemeID

	* @return : array that is the record retrieved from db
	*
	*/

	public function get_teacher($schemeID){

		$NameField = getNameFieldByLang("b.");
		$sql = "SELECT a.Userid AS 'UserID', a.TeacherType AS 'TeacherType', $NameField AS 'TeacherName'" .
				"FROM IES_SCHEME_TEACHER AS a " .
				"INNER JOIN INTRANET_USER AS b on a.Userid = b.Userid " .
				"WHERE SchemeID = '$schemeID'";

		$objDB = new libdb();
		$rs = $objDB->returnArray($sql);
		return $rs;
	}

	/**
	 * Return a lang to corresponding status
	 * @param	INT	$ParConfigNum
	 * @return String Language for corresponding status
	 */
	public function getBatchStatusLangByConfigNum($ParConfigNum) {
		global $ies_cfg,$Lang;
		switch($ParConfigNum) {
			case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"]:
			case $ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitting"]:
				$statusLang = $Lang['IES']['Submitting'];
			break;
			case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]:
			case $ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitted"]:
				$statusLang = $Lang['IES']['Submitted'];
			break;
			case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["approved"]:
			case $ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["approved"]:
				$statusLang = $Lang['IES']['Approved'];
			break;
			case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["teacher_read"]:
			case $ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["teacher_read"]:
				$statusLang = $Lang['IES']['TeacherRead'];
			break;
			case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]:
			case $ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["redo"]:
				$statusLang = $Lang['IES']['Redo'];
			break;
			default:
				$statusLang = "";
			break;
		}
		return $statusLang;
	}


	public function generateQuestionAnswerHtml($ParUserDisplayName, $ParStepQuestionAnswer) {
		global $Lang;
		# Setting html template
		$HTML = <<<HTMLEND
<div class="IES_thickbox_list">
	<table class="form_table" style="width:100%">


		<col class="field_title" />
		<col  class="field_c" />

		<tr>
		<td>{$Lang['Identity']['Student']}</td>
		<td>:</td>
		<td>{$ParUserDisplayName}</td>
		</tr>
	</table>
	<table class="common_table_list">
		<col width="150px"/>
		<col />
		<thead>
		<tr>
		<th>{$Lang['IES']['Step']}</th>

		<th>{$Lang['IES']['StudentInputData']}</th>

		</tr>
		</thead>
		{{{ TBODY }}}
	</table>
</div>
HTMLEND;

		# Contruct the tbody part
		$tbody = "<tbody>";
		foreach($ParStepQuestionAnswer as $key => $element) {
			$tbody .= $this->generateStepQuestionAnswerHtml($element);
		}
		$tbody .= "</tbody>";

		# Finally replace the tbody in html
		$HTML = str_replace("{{{ TBODY }}}", $tbody, $HTML);
		return $HTML;
	}

	public function getStepIdByStageSeqTaskSeqStepSeq($ParStageSeq,$ParTaskSeq,$ParStepSeq) {
		$sql = "SELECT IST.STEPID FROM $intranet_db.IES_STEP IST
				INNER JOIN IES_TASK IT ON IST.TASKID = IT.TASKID
				INNER JOIN IES_STAGE STG ON IT.STAGEID = STG.STAGEID
				WHERE
					STG.SEQUENCE = '$ParStageSeq'
					AND IT.SEQUENCE= '$ParTaskSeq'
					AND IST.SEQUENCE = '$ParStepSeq'";
		return current($this->returnVector($sql));
	}


//	private function generateStepQuestionAnswerHtml($ParStepQuestionAnswer) {
//		$rowHtml = <<<HTMLEND
//        <tr>
//          <td>{{{ TITLE }}}</td>
//          <td class="student_ans">{{{ QUESTION_ANSWER_AREA }}}</td>
//        </tr>
//HTMLEND;
//
//		$stepId = $ParStepQuestionAnswer["STEPID"];
//		$title = $ParStepQuestionAnswer["TITLE"];
//		$questionAnswerPair = $ParStepQuestionAnswer["QUESTION_ANSWER_PAIR"];
//		$sizeOfQAPair = count($questionAnswerPair);
//		for($stepSeq = 1; $stepSeq <= $sizeOfQAPair; $stepSeq++) {
//			$currentElement = $questionAnswerPair[$stepSeq];
//
//			$question = ($currentElement["QUESTION"]?$currentElement["QUESTION"]:$title);	// if no specified question, use the title as question
//			$questionType = $currentElement["QUESTIONTYPE"];
//			$answer = $currentElement["ANSWER"];
//			$answerActualValue = $currentElement["ANSWERACTUALVALUE"];
//			$answerExtra = $currentElement["ANSWEREXTRA"];
//			$dateModified = $currentElement["DATEMODIFIED"];
//
//			$questionAnswerPairHtml .= "Question: $question<br/><br/>Answer:<br>";
//			$questionAnswerPairHtml .= $this->convertAnswerToDisplayFormat($questionType,$answer,$answerActualValue,$answerExtra,$stepId);
//			if ($answer.$answerActualValue.$answerExtra != "") {
//				$questionAnswerPairHtml .= "<br /><span class=\"update_record\">$dateModified</span><br/><br/>";
//			}
//		}
//		$HTML = str_replace(array("{{{ TITLE }}}","{{{ QUESTION_ANSWER_AREA }}}"),array($title,$questionAnswerPairHtml),$rowHtml);
//		return $HTML;
//
//	}

//	function convertAnswerToDisplayFormat($ParType,$ParAnswer,$ParAnswerActualValue,$ParAnswerExtra,$ParStepId) {
//
//		global $ies_cfg,$intranet_root;
//		$ParAnswer = htmlspecialchars($ParAnswer);
//		$ParAnswerActualValue = htmlspecialchars($ParAnswerActualValue);
//		$ParAnswerExtra = htmlspecialchars($ParAnswerExtra);
//
//		switch(strtoupper($ParType))
//		{
//			case "TEXT" :// SAME AS TEXTAREA
//			case "TEXTAREA":
//				$returnAnswer = $ParAnswer;
//				break;
//			//case "CHECKBOX":
//			case "SELECT_MOD":
//				//replace the delimiter with ,
//				$tempAnswer = $ParAnswerActualValue.$ies_cfg["question_answer_sperator"].$ParAnswerExtra;
//				$tempAnswer = str_replace($ies_cfg["question_answer_sperator"],",",$tempAnswer);
//
//				//REMOVE THE LAST OCCURRENCE OF THE SPERATOR
//		        $tempAnswer = substr($tempAnswer,0, 0 - intval(strlen(",")));
//				$returnAnswer = $tempAnswer;
//				break;
//			case "TABLE":
//				$stepDetails = $this->getStepDetail($ParStepId);
//				list($stage_id, $stage_seq, $stage_title, $task_id, $task_title, $step_title, $step_no, $step_seq) = $stepDetails;
//				$stepModelFile = "$intranet_root/home/eLearning/ies/coursework/templates/default/{$stage_seq}/{$step_no}/model.php";
//
//				global $Lang;
//				include($stepModelFile);
//				$HTML = "
//				<table style='empty-cells:show'>
//					<tr><th>";
//				$HTML .= implode("</th><th>",$_question[1]);
//				$HTML .= "</th></tr>
//					{{{ ROWS }}}
//				</table>";
//				$rowAnswer = explode($ies_cfg["new_set_question_answer_sperator"],$ParAnswer);
//				if (count($rowAnswer)>0) {
//					foreach($rowAnswer as $key => $element) {
//						$tdAnswer = explode($ies_cfg["question_answer_sperator"],$element);
//						$rows .= "<tr>";
//						foreach($tdAnswer as $tdKey => $tdValue) {
//							$rows .= "<td>".($tdValue?$tdValue:"&nbsp;")."</td>";
//						}
//						$rows .= "</tr>";
//					}
//					$returnAnswer = str_replace("{{{ ROWS }}}",$rows,$HTML);
//				}
//				break;
//			default:
//				$returnAnswer = $ParAnswer;
//				break;
//		}
//		return $returnAnswer;
//	}
	public function getQuestionAnswerPairs($ParUserId,$ParTaskId) {
		global $intranet_db;
		$sql = "SELECT
					IST.TASKID,IST.STEPID,IST.TITLE,IST.STEPNO,IST.STATUS,
					IQH.ANSWERID,IQH.USERID,IQH.QUESTIONTYPE,IQH.QUESTIONCODE,IQH.ANSWER,
					IQH.ANSWERACTUALVALUE,IQH.ANSWEREXTRA,IQH.DATEINPUT,IQH.INPUTBY,
					IQH.DATEMODIFIED,IQH.MODIFYBY
				FROM $intranet_db.IES_STEP IST
					LEFT JOIN $intranet_db.IES_QUESTION_HANDIN IQH ON IQH.STEPID = IST.STEPID
					LEFT JOIN $intranet_db.IES_TASK IT ON IST.TASKID = IT.TASKID
				WHERE
					IQH.USERID = '$ParUserId'
					AND IT.TASKID = '$ParTaskId'
				ORDER BY
					IST.SEQUENCE ASC, IQH.QUESTIONCODE ASC";
		$returnArray = array();
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
//	function formatQuestionAnswerAndPadCustomQuestions($ParQuestionAnswer) {
//		global $intranet_root;
//		include_once("$intranet_root/home/eLearning/ies/coursework/templates/default/scheme_model.php");
//		$stepArray = array();
//		foreach($ParQuestionAnswer as $key => $element) {
//			$stepId = $element["STEPID"];
//			$title = $element["TITLE"];
//
//			$answerId = $element["ANSWERID"];
//			$questionType = $element["QUESTIONTYPE"];
//			$questionCode = $element["QUESTIONCODE"];
//			$answer = $element["ANSWER"];
//			$answerActualValue = $element["ANSWERACTUALVALUE"];
//			$answerExtra = $element["ANSWEREXTRA"];
//			$dateInput = $element["DATEINPUT"];
//			$inputBy = $element["INPUTBY"];
//			$dateModified = $element["DATEMODIFIED"];
//			$modifiedBy = $element["MODIFYBY"];
//
//			$parentsIdSeqPairs = current($this->getParentsIds("STEP",$stepId));
//			$stageSequence = $parentsIdSeqPairs["STAGE_SEQ"];
//			$taskSequence = $parentsIdSeqPairs["TASK_SEQ"];
//			$stepSequence = $parentsIdSeqPairs["STEP_SEQ"];
//
//
//			$questionAnswerMappingArray = array();
//			$questionAnswerMappingArray["ANSWERID"] = $answerId;
//			$questionAnswerMappingArray["QUESTIONTYPE"] = $questionType;
//			$questionAnswerMappingArray["QUESTIONCODE"] = $questionCode;
//			$questionAnswerMappingArray["QUESTION"] = $schemeCfg_TaskQuestion[$stageSequence][$taskSequence][$stepSequence][$questionCode-1];
//			$questionAnswerMappingArray["ANSWER"] = $answer;
//			$questionAnswerMappingArray["ANSWERACTUALVALUE"] = $answerActualValue;
//			$questionAnswerMappingArray["ANSWEREXTRA"] = $answerExtra;
//			$questionAnswerMappingArray["DATEINPUT"] = $dateInput;
//			$questionAnswerMappingArray["INPUTBY"] = $inputBy;
//			$questionAnswerMappingArray["DATEMODIFIED"] = $dateModified;
//			$questionAnswerMappingArray["MODIFYBY"] = $modifiedBy;
//
//			$stepArray[$stepSequence]["STEPID"] = $stepId;
//			$stepArray[$stepSequence]["STEP_SEQ"] = $stepSequence;
//			$stepArray[$stepSequence]["TITLE"] = $title;
//			$stepArray[$stepSequence]["QUESTION_ANSWER_PAIR"][$questionCode] = $questionAnswerMappingArray;
////			echo "In task SEQ: $taskSequence --> stepArray[stageSequence:$stageSequence][questionCode:$questionCode]<br/>";
//		}
//		return $stepArray;
//	}





	/**
	 * Get the required elements for constructing a table field
	 * @param	INT $ParBatchId
	 * @return	DB Array - ["COMMENT"]["DATE"]["INPUTBY"]
	 */
	public function getBatchCommentRequiredElement($ParBatchId) {
		global $intranet_db;
		$sql = "SELECT
					COMMENT.COMMENT,
					DATE_FORMAT(COMMENT.DATEINPUT, '%d-%m-%Y') AS DATEINPUT,
					".getNameFieldByLang("IU.")." AS COMMENTTEACHER
				FROM {$intranet_db}.IES_STAGE_HANDIN_COMMENT AS COMMENT
					INNER JOIN {$intranet_db}.INTRANET_USER IU ON IU.USERID = COMMENT.INPUTBY
				WHERE COMMENT.BATCHID = '{$ParBatchId}'";

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}



	/**
	* APPEND PARAMETER TO THE END OF A LINK
	*
	* @param : STRING $appendParameter Parameter that need to append to the end of the URL
	* @param : ARRAY $tabArray ARRAY TO STORE ALL THE TAB NEED TO DISPLAY
	* @return : ARRAY Return array same as $tabArray but append a $appendParameter to each first element of the $tabArray
	*
	*/
	public function appendParameterToTab($tabAppendParameter,$tabArray)
	{
		//append the parameter for each tab link
		$newTab = array();
		foreach ($tabArray as $tabName => $tabDetails)
		{
			$link = $tabDetails[0].$tabAppendParameter;
			$caption = $tabDetails[1];
			$newTab[$tabName] = array($link,$caption);
		}
		return $newTab;
	}


	public function getStageTaskArr($parStageID)
	{
		global $intranet_db;

		$ldb = new libdb();

		$sql = "SELECT task.TaskID, step.StepID, task.Title,task.Sequence FROM {$intranet_db}.IES_TASK task LEFT JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID WHERE task.StageID = '{$parStageID}' GROUP BY task.TaskID HAVING MIN(step.Sequence) ORDER BY task.Sequence, step.Sequence";
		$stageTaskArr = $ldb->returnArray($sql);

		return $stageTaskArr;
	}


	public function gettaskAnswer($task_id, $ParUserId){
		$li = new libdb();
		$sql = "SELECT Answer FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = '{$task_id}' AND UserID = '{$ParUserId}'";
		$task_answer = current($li->returnVector($sql));
		return $task_answer;

	}

	public function gettaskAnswer_withQuestionType($task_id, $ParUserId){
		$li = new libdb();
		$sql = "SELECT Answer,QuestionType FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = '{$task_id}' AND UserID = '{$ParUserId}'";
		$task_answer = current($li->returnArray($sql));
		return $task_answer;

	}

	/**
	* Generate Teaching teacher and AccessTeacher list to HTML
	* @param : int $id, which is the IES_FAQ.QuestionID
	* @return : array that is the record retrieved from db,
	*/
	public function getFAQData($id="")
	{
		if($id!='') {
			$sql = "SELECT * FROM IES_FAQ WHERE QuestionID='$id'";
			$result = $this->returnArray($sql);
			return $result[0];
		}
	}

	/**
	* STUDNET submit a stage handin
	* @param : int $studentID , STUDENT ID TO SUBMIT THIS HANDIN
	* @param : int $batchID , STUDENT SELECTED WITH BACTH FOR THIS STAGE HANDIN
	* @param : int $stageID , STUDENT SUBMIT THIS HANDIN FOR WHICH STAGE
	* @return : null
	*/
	public function studentSubmitStageHandIn($studentID , $batchID, $stageID)
	{
		global $ies_cfg;

		if($studentID == "" || $batchID =="" || $stageID =="")
		{
			//for safe , update the status only when Student ID , Batch ID , Stage ID must has value
			//do nothing
		}
		else
		{
			$updateHandInStatus = $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"];

			//for the where BatchStatus = {$updateHandInStatus}. For safty, update the batch to "submited" where the original status is "submitting" (or submitted , redo ...)
			$sql = "update
						IES_STAGE_HANDIN_BATCH
					set
						BatchStatus = ".$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]."
					where
						UserID = '{$studentID}' and
						StageID = '{$stageID}' and
						BatchID = '{$batchID}' and
						BatchStatus = '{$updateHandInStatus}'
				   ";
			$this->db_db_query($sql);

		}
		return ;
	}

	/**
	 * Convert Associative array to js array
	 */
	public function ConvertToJSArrayNew($ParInArray, $ParArrayName) {
//		$returnArrayStr = "<script>\n";
		$returnArrayStr .= "var $ParArrayName = new Array();\n";
		if (count($ParInArray)>0){
			foreach($ParInArray as $key => $element) {
				$returnArrayStr .= $ParArrayName."[$key] = new Array();\n";

				foreach($element as $ekey => $eElement) {
					$returnArrayStr .= $ParArrayName."[$key][\"$ekey\"] = \"$eElement\";\n";
				}
			}
		}
//		$returnArrayStr .= "</script>\n";
		return $returnArrayStr;
	}

	/**
	 * Update the batch status
	 * @param	INT	$ParBatchId	- The batch id of batch to be updated
	 * @param	INT	$ParStatus	- The status to be changed to, refer to iesConfig.inc.php
	 * @return	bool	The update result.
	 */
	public function changeBatchStatus($ParBatchId, $ParStatus) {
		global $intranet_db;
		$sql = "UPDATE $intranet_db.IES_STAGE_HANDIN_BATCH SET BATCHSTATUS = '$ParStatus' WHERE BATCHID = '$ParBatchId'";
		$q_result = $this->db_db_query($sql);
		return $q_result;
	}
	public function updateBatchSubmissionDate($ParBatchId) {
		$sql = "UPDATE $intranet_db.IES_STAGE_HANDIN_BATCH SET SUBMISSIONDATE = NOW() WHERE BATCHID = '$ParBatchId'";
		$q_result = $this->db_db_query($sql);
		return $q_result;
	}


	public function getStepDetail($parStepID)
	{
	  global $intranet_db;


	  //PLEASE DON'T CHANGE THE SEQUENCE FOR THE SELECTED FIELD , PLESAE APPEND ALL THE ADDED FIELD TO END
	  $sql =  "SELECT
	              stage.StageID,
	              stage.Sequence,
	              stage.Title,
	              task.TaskID,
	              task.Title,
	              step.Title,
	              step.StepNo,
	              step.Sequence
	            FROM
	              {$intranet_db}.IES_STEP step
	            INNER JOIN {$intranet_db}.IES_TASK task
	              ON step.TaskID = task.TaskID
	            INNER JOIN {$intranet_db}.IES_STAGE stage
	              ON task.StageID = stage.StageID
	            WHERE
	              step.StepID = '{$parStepID}'";
	  $stepDetailArr = $this->returnArray($sql);

	  return $stepDetailArr[0];
	}


	/**
	 * Get parents tables' ids SCHEME > STAGE > TASK > STEP > ANSWER
	 * @param	$ParIdType	1. SCHEME 2. STAGE 3. TASK 4. STEP 5. ANSWER
	 * @param	$ParId
	 * @return	Array	Parents Ids, Parents Sequences
	 */
	public function getParentsIds($ParIdType, $ParId) {
		$type = strtoupper($ParIdType);
		$typeSequence = array("SCHEME","STAGE","TASK","STEP","ANSWER");
		$finalElement = false;
		if (in_array($type, $typeSequence)) {
			$preElement = "";
			$preId = "";
			foreach($typeSequence as $key => $element) {
				if ($finalElement) {
					break;
				} else {

					$selection[] = $element.".".$element."ID";
					if ($element != "SCHEME") {
						$selection[] = $element.".SEQUENCE AS ".$element."_SEQ";
					}

					if ($element == "TASK") {
						$selection[] = $element.".CODE AS ".$element."_CODE";
					}

					$selectedId = $element.".".$preElement."ID";
					$preSelectId = $preElement.".".$preElement."ID";
					switch($element) {
						case "SCHEME":
							$from[] = " FROM IES_$element $element ";
							break;
						case "STAGE":
							$from[] = " INNER JOIN IES_$element $element ON $selectedId = '$preSelectId' ";
							break;
						case "TASK":
							$from[] = " INNER JOIN IES_$element $element ON $selectedId = '$preSelectId' ";
							break;
						case "STEP":
							$from[] = " INNER JOIN IES_$element $element ON $selectedId = '$preSelectId' ";
							break;
						case "ANSWER":
							$from[] = " INNER JOIN IES_QUESTION_HANDIN $element ON $selectedId = '$preSelectId' ";
							break;
					}
				}
				$preElement = $element;

				if ($type==$element){$finalElement = true;}
			}
			$where = " WHERE ".$type.".".$type."ID = '$ParId' ";

			$sql = "SELECT ".implode(",", $selection).implode(" ",$from).$where;

		}
		$returnArray = array();
		if ($sql) {
			$returnArray = $this->returnArray($sql);
		}
		return $returnArray;
	}

	public function isHandinRedo($stage_id,$UserID) {
		global $ies_cfg,$intranet_db;
		$returnResult = false;
		$sql = "SELECT BATCHSTATUS FROM $intranet_db.IES_STAGE_HANDIN_BATCH
				where
					StageID = '{$stage_id}' and
					UserID = '{$UserID}'

		ORDER BY BATCHID DESC LIMIT 1";

		if (current($this->returnVector($sql)) == $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]) {
			$returnResult = true;
		} else {
			$returnResult = false;
		}
		return $returnResult;
	}

	/**
	 * Get Current Batch ID by using stage id and user id, if no current, create a new one
	 * @param	INT	$ParStageId	- stage id
	 * @param	INT	$ParUserId - user id
	 * @param	bool	$ParIsCreateIfNotFound - create a new batch
	 * @return	INT	batch id, if record not found and does not specified for create new one, return -1
	 */
	public function getCurrentBatchId($ParStageId,$ParUserId, $ParIsCreateIfNotFound = false) {
		global $ies_cfg,$intranet_db;
		$returnResult = false;
		$sql = "SELECT BATCHID FROM $intranet_db.IES_STAGE_HANDIN_BATCH
				WHERE STAGEID = '$ParStageId'
					AND USERID = '$ParUserId'
					AND BATCHSTATUS = {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"]}";

		$batchIdResult = $this->returnVector($sql);
		if (count($batchIdResult)==0 && $ParIsCreateIfNotFound) {
			$sql = "INSERT INTO $intranet_db.IES_STAGE_HANDIN_BATCH
						SET
							STAGEID = '$ParStageId',
							USERID = '$ParUserId',
							BATCHSTATUS = {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"]},
							DATEINPUT = NOW(),
							INPUTBY = '$ParUserId',
							MODIFYBY = '$ParUserId'";
			$q_result = $this->db_db_query($sql);
			$returnResult = mysql_insert_id();
		} else {
			$returnResult = current($batchIdResult);
		}
		return $returnResult;
	}

	public function updateStudentStageBatchHandinStatus($ParStageID, $ParStudentID, $ParTargetStatus, $ParOrigStatus="") {
		global $ies_cfg, $intranet_db, $UserID;

		if($ParOrigStatus == "")
		{
  		$sql = "INSERT INTO {$intranet_db}.IES_STAGE_STUDENT ";
      $sql .= "(StageID, UserID, BatchHandinStatus, DateInput, InputBy, ModifyBy) ";
      $sql .= "VALUES ('{$ParStageID}', '{$ParStudentID}', '{$ParTargetStatus}', NOW(), '{$UserID}', '{$UserID}') ";
      $sql .= "ON DUPLICATE KEY UPDATE BatchHandinStatus = VALUES(BatchHandinStatus), ModifyBy = VALUES(ModifyBy)";

  		$q_result = $this->db_db_query($sql);
  		$returnResult = mysql_insert_id();
    }
    else
    {
      $sql = "UPDATE {$intranet_db}.IES_STAGE_STUDENT ";
      $sql .= "SET BatchHandinStatus = '{$ParTargetStatus}' ";
      $sql .= "WHERE StageID = '{$ParStageID}' AND UserID = '{$ParStudentID}' AND BatchHandinStatus = '{$ParOrigStatus}'";
      $q_result = $this->db_db_query($sql);
    }

		return $returnResult;
	}

	/**
	* RESET STUDENT STAGE HANDIN SCORE
	* @param : INT $stageId Stage ID
	* @param : INT $studentID student ID
	* @return : NIL
	*/
	public function updateStudentStageScore($stageId, $studentID,$mark = null)
	{

		$sql = "Update IES_STAGE_STUDENT set Score = '{$mark}' where StageID = '{$stageId}' and UserID = '{$studentID}'";

		$q_result = $this->db_db_query($sql);
	}

	/**
	* STUDENT HANDIN FILE TO STAGE
	* @param : STRING $fileName , STUDENT HANDIN FILE NAME (eg homework1.doc)
	* @param : STRING $filePath , SERVER LOCATION TO STORE FILE (will not include  root path /home/web/intranetIP/files/ )
	* @param : STRING $FileHashName , HASH FILE NAME FOR THE STUDENT SUBMIT FILE
	* @param : INT $handInBy , THIS FILE IS UPLOADED BY WHOM
    * @param : INT $batchID , THIS FILE BELONG TO WHICH BATCH UPLOD
	* @return : INT INSERT FILE ID
	*/
	public function handInFileToStage($fileName,$filePath,$hashFileName,$handInBy,$batchId)
	{

		$sql = "insert into
					IES_STAGE_HANDIN_FILE
				(FileName,FolderPath,FileHashName,InputBy,BatchID,DateInput)
					value
				('{$fileName}','{$filePath}','{$hashFileName}','{$handInBy}','{$batchId}',now())";
		$this->db_db_query($sql);
		$fileId = mysql_insert_id();

		return $fileId;
	}

	public function removeHandInFilePhysical($ParFileId) {
		$libfilesystem = new libfilesystem();
		$infoArray = current($this->getStageHandinFileInfo("WHERE FILEID = ".$ParFileId));
		$FilePath = $infoArray["FOLDERPATH"];
		$FileHashName = $infoArray["FILEHASHNAME"];
		$FullFileName = $infoArray["FOLDERPATH"].$infoArray["FILEHASHNAME"];
		$removeResult = false;
		if (file_exists($FullFileName)) {
			$removeResult = $libfilesystem->file_remove($FullFileName);
		} else {
			$removeResult = true;
		}
		return $removeResult;
	}

	/**
	* REMOVE STUDENT HANDIN FILE
	* @param : INT $fileId  , FILE ID in IES_STAGE_HANDIN_FILE that need to remvoe
	* @return : INT Result of the remove
	*/
	public function removeHandInFileInStage($fileId)
	{
		$sql = "delete from IES_STAGE_HANDIN_FILE where FileID = '{$fileId}'";
		$returnResult = $this->db_db_query($sql);
		return $returnResult;
	}

	/**
	* get TaskSnap shot Answer
	* @param : INT $TaskID
	* @param : INT $StudentID
	* @return : ARRAY
	*/
	public function getTaskSnapshotAnswer($TaskID, $StudentID){
	  global $intranet_db;

	  $ldb = new libdb();

	  $sql = "SELECT SnapshotAnswerID, TaskID, UserID, Answer, DATE_FORMAT(AnswerTime, '%d-%m-%Y') AS AnswerDate FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT WHERE TaskID = '{$TaskID}' AND UserID = '{$StudentID}' ORDER BY AnswerTime DESC";



	  $taskSnapshotAnswerArr = $ldb->returnArray($sql);

	  return $taskSnapshotAnswerArr;
	}
	public function getCommentArr($SnapshotID){
	  global $intranet_db;

	  $ldb = new libdb();

	  $sql = "SELECT comment.Comment, DATE_FORMAT(comment.DateInput, '%d-%m-%Y') AS DateInput, ".getNameFieldByLang("iu.")." AS CommentTeacher, comment.Status AS Status FROM {$intranet_db}.IES_TASK_HANDIN_COMMENT AS comment INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserID = comment.InputBy WHERE comment.SnapshotAnswerID = '{$SnapshotID}'";
	  $snapshotCommentArr = $ldb->returnArray($sql);

	  return $snapshotCommentArr;
	}

	public function getTaskHandInScore($SnapshotID){
		global $intranet_db;

		$snapShortScoreArr = null;

		$ldb = new libdb();

		if(is_numeric($SnapshotID)){

			$sql = 'select s.Score as Score ,DATE_FORMAT(s.DateInput, \'%d-%m-%Y\') AS DateInput, '.getNameFieldByLang("iu.").' AS ScoreTeacher from '.$intranet_db.'.IES_TASK_HANDIN_SCORE as s left join '.$intranet_db.'.INTRANET_USER as iu on iu.UserID = s.InputBy where  s.SnapshotAnswerID=\''.$SnapshotID."'";
			$snapShortScoreArr = $ldb->returnArray($sql);
		}

		return $snapShortScoreArr;

	}

	public function updateTeacherCommentStatus($ParTaskSnapshotAnsweIDArray, $ParStatus){
		global $intranet_db;
	  	$String = "";
	  	if(sizeof($ParTaskSnapshotAnsweIDArray) == 0){
	  		return 0;
	  	}
	  	else{
	  		$String .= "(";
		  	for($i =0; $i < sizeof($ParTaskSnapshotAnsweIDArray); $i++){
		  		if($i > 0){
		  			$String .= ",".$ParTaskSnapshotAnsweIDArray[$i];
		  		}
		  		else{
		  			$String .= $ParTaskSnapshotAnsweIDArray[$i];
		  		}
		  	}
		  	$String .= ")";
	  	}

		$sql = "UPDATE {$intranet_db}.IES_TASK_HANDIN_COMMENT SET Status = '$ParStatus' WHERE SnapshotAnswerID IN $String" ;

		$result = $this->db_db_query($sql);

		return $result;
	}

	public function getTeacherCommentLatestTime($TaskID, $StudentID){
		  global $intranet_db;

		  $ldb = new libdb();

		  $sql ="SELECT thc.DateInput AS DateInput FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT ths INNER JOIN {$intranet_db}.IES_TASK_HANDIN_COMMENT thc ON ths.SnapshotAnswerID = thc.SnapshotAnswerID WHERE ths.TaskID = '{$TaskID}' AND ths.UserID = '{$StudentID}' ORDER BY thc.DateInput DESC;";
		  //$sql = "SELECT SnapshotAnswerID, TaskID, UserID, Answer, DATE_FORMAT(AnswerTime, '%d-%m-%Y') AS AnswerDate FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT WHERE TaskID = {$TaskID} AND UserID = {$StudentID} ORDER BY AnswerTime DESC";

		  $rs = $ldb->returnArray($sql);

		  return $rs[0];

	}

	public function getTeacherTaskScoreLatestTime($TaskID, $StudentID){
		  global $intranet_db;

		  $ldb = new libdb();

		  $sql ="SELECT th_score.DateInput AS DateInput FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT ths INNER JOIN {$intranet_db}.IES_TASK_HANDIN_SCORE th_score ON ths.SnapshotAnswerID = th_score.SnapshotAnswerID WHERE ths.TaskID = {$TaskID} AND ths.UserID = '{$StudentID}' ORDER BY th_score.DateInput DESC;";


		  $rs = $ldb->returnArray($sql);

		  return $rs[0];

	}


	/**
	* Check if there is a teacher comment or not
	* @param : INT $TaskID
	* @param : INT $StudentID
	* @return : TRUE / FALSE
	*/
	public function IsTeacherComment($TaskID, $StudentID){
		  global $intranet_db;

		  $ldb = new libdb();

		  $sql ="SELECT IF(COUNT(*) > 0, 'TRUE', 'FALSE') as Status FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT WHERE TaskID = '{$TaskID}' AND UserID = '{$StudentID}';";
		  //$sql = "SELECT SnapshotAnswerID, TaskID, UserID, Answer, DATE_FORMAT(AnswerTime, '%d-%m-%Y') AS AnswerDate FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT WHERE TaskID = {$TaskID} AND UserID = {$StudentID} ORDER BY AnswerTime DESC";
		  $rs = $ldb->returnVector($sql);
		  return $rs[0];

	}

		/**
	* Get the description detail of stage
	* @param : String file path
	*
	* @return : Array , the description detail
	*/
	public function GET_DESC_DETAIL($path){
	  	global $Lang;
	  	if(is_file($path))
	  	{
	  		include($path);
	  		$return_arry["FolderDesc"] = stripslashes($Lang_cus['IES']['FolderDesc']);
	  		$return_arry["TextDesc"] = stripslashes($Lang_cus['IES']['TextDesc']);
	  		$return_arry["NoteDesc"] = stripslashes($Lang_cus['IES']['NoteDesc']);
	  		$return_arry["WorksheetDesc"] = stripslashes($Lang_cus['IES']['WorksheetDesc']);
	  		$return_arry["FAQDesc"] = stripslashes($Lang_cus['IES']['FAQDesc']);
	  	}
	  	else{
			$return_arry["FolderDesc"] = $Lang['IES']['FolderDesc_org'];
			$return_arry["TextDesc"] = $Lang['IES']['TextDesc_org'];
			$return_arry["NoteDesc"] = $Lang['IES']['NoteDesc_org'];
			$return_arry["WorksheetDesc"] = $Lang['IES']['WorksheetDesc_org'];
			$return_arry["FAQDesc"] = $Lang['IES']['FAQDesc_org'];
	  	}
	  	return $return_arry;
	}


	public function getTaskArr($parTaskID)
	{
	  global $intranet_db;

	  $sql = "SELECT task.TaskID, step.StepID, task.Title, task.Description FROM {$intranet_db}.IES_TASK task LEFT JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID WHERE task.TaskID = '{$parTaskID}' GROUP BY task.TaskID HAVING MIN(step.Sequence) ORDER BY task.Sequence, step.Sequence";
	  $taskArr = current($this->returnArray($sql));

	  return $taskArr;
	}

	public function getTaskIDByTaskInfo($parSchemeID, $parStageSeq, $parTaskCode)
	{
    global $intranet_db;

    $sql =  "SELECT task.TaskID ";
    $sql .= "FROM {$intranet_db}.IES_TASK task ";
    $sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ";
    $sql .= "ON task.StageID = stage.StageID ";
    $sql .= "WHERE stage.SchemeID = '{$parSchemeID}' AND stage.Sequence = '{$parStageSeq}' AND task.Code = '{$parTaskCode}' ";

    $taskID = current($this->returnVector($sql));

    return $taskID;
  }


/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ xml generating functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/
	/**
	 * generate the output xml
	 * @param	array	ParElementsArr	the child elements for the xml output, string [0] - child tag name, string [1] - child content
	 * @return	xml	the xml to output
	 */
	public function generateXML($ParElementsArr, $ParCData=false, $ParSpcChar=false) {
		$xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		$numofElements = count($ParElementsArr);
		for ($i=0;$i<$numofElements;$i++) {
			list($TAG_NAME, $CONTENT) = $ParElementsArr[$i];
			$x .= $this->sxe($TAG_NAME, $CONTENT, $ParCData, $ParSpcChar);
		}
		$xml .= $this->sxe("elements", $x);
		return $xml;
	}


	/**
	 * sxe - simple xml embed with format <xx> yy </xx>
	 * @param	string	ParTag	the tag name of an xml element
	 * @param	string	ParValue	corresponding content for an element
	 * @param	boolean	ParCData	quote the content with <![CDATA[ content ]]>	// already html format string
	 * @param	boolean	ParSpcChar	escape special characters (single quote and double quotes)	// for strings to put into html
	 * @return	string	xml tag
	 */
	public function sxe($ParTag="element", $ParValue="content", $ParCData=false, $ParSpcChar=false) {
		return "<".$ParTag.">".($ParCData?"<![CDATA[":"").($ParSpcChar?intranet_htmlspecialchars($ParValue):$ParValue).($ParCData?"]]>":"")."</".$ParTag.">";
	}

/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ template setting functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/
	###############Bookmark: TEMPLATE_#######################
	##	Start:	functions starts with template_ used to set html template for display
	#########################################################
	public function template_AnswerArea02($ParUserDisplayName) {
		global $Lang;
				$HTML = <<<HTMLEND
<div class="IES_thickbox_list">
	<table class="form_table" style="width:100%">


		<col class="field_title" />
		<col  class="field_c" />

		<tr>
		<td>{$Lang['Identity']['Student']}</td>
		<td>:</td>
		<td>{$ParUserDisplayName}</td>
		</tr>
	</table>
	<table class="common_table_list">
		<col width="150px"/>
		<col />
		<thead>
		<tr>
		<th>{$Lang['IES']['Step']}</th>

		<th>{$Lang['IES']['StudentInputData']}</th>

		</tr>
		</thead>
		{{{ HTML_ANSWER_AREA }}}
	</table>
</div>
HTMLEND;
		return $HTML;
	}

	public function template_AnswerArea01() {
		global $Lang;
		$var = <<<HTML
  <div class="answer_area">
    <h4 class="stu_edit">{$Lang['IES']['TheInformationYouHaveEntered']}</h4>
    <table class="answer common_table_list">
      <col width="200" />
      <col />
      {{{ HTML_ANSWER_AREA }}}
    </table>
  </div>
HTML;
		return $var;
	}
	###############Bookmark: TEMPLATE_#######################
	##	End:	functions starts with template_ used to set html template for display
	#########################################################


/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ table handling functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/
	###############Bookmark: TABLE_HANDLING##################
	##	Start:	function getStageHandinBatchInfo() - handling table IES_STAGE_HANDIN_BATCH
	##			and corresponding assistance functions
	#########################################################
	/**
	 * Get information from database table IES_STAGE_HANDIN_BATCH
	 * @param	String $ParCond	- The Sql condition, *Please input with WHERE
	 * @return	Array the associative array for result get from IES_STAGE_HANDIN_BATCH
	 */
	private function getStageHandinBatchInfo($ParCond="") {
		global $intranet_db;

		$sql = "SELECT BATCHID,STAGEID,USERID,BATCHSTATUS,DATEINPUT,INPUTBY,DATEMODIFIED,MODIFYBY
				FROM $intranet_db.IES_STAGE_HANDIN_BATCH $ParCond";
		$returnArray = array();
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function isExistBatchRecord($ParStageId,$ParUserId) {
		if (count($this->getStageHandinBatchInfo("WHERE STAGEID = '$ParStageId' AND USERID = '$ParUserId'"))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get Last Submitted Batch ID by using stage id and user id
	 * @param	INT	$ParStageId	- stage id
	 * @param	INT	$ParUserId - user id
	 * @param	bool	$ParIsCreateIfNotFound - create a new batch
	 * @return	INT	batch id, if record not found and does not specified for create new one, return -1
	 */
	public function getLastSubmittedBatchId_and_BatchStatus($ParStageId,$ParUserId) {
		return current($this->getStageHandinBatchInfo("WHERE STAGEID = '$ParStageId' AND USERID = '$ParUserId' ORDER BY DATEMODIFIED DESC LIMIT 1"));
	}

	public function getStageHandinBatchInfo_notSubmitting_orderByDateModifiedDesc($ParStageId,$UserId) {
		global $ies_cfg;
		return $this->getStageHandinBatchInfo("WHERE BATCHSTATUS <> {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"]} AND STAGEID = '$ParStageId' AND USERID = '$UserId' ORDER BY DATEMODIFIED DESC");
	}
	###############Bookmark: TABLE_HANDLING##################
	##	End:	function getStageHandinBatchInfo() - handling table IES_STAGE_HANDIN_BATCH
	##			and corresponding assistance functions
	#########################################################

	###############Bookmark: TABLE_HANDLING##################
	##	Start:	function getStageHandinFileInfo() - handling table IES_STAGE_HANDIN_FILE
	##			and corresponding assistance functions
	#########################################################
	/**
	 * Get information from database table IES_STAGE_HANDIN_FILE
	 * @param	String $ParCond	- The Sql condition, *Please input with WHERE
	 * @return	Array the associative array for result get from IES_STAGE_HANDIN_FILE
	 */
	private function getStageHandinFileInfo($ParCond="") {
		global $intranet_db;

		$sql = "SELECT FILEID,FILENAME,FOLDERPATH,FILEHASHNAME,BATCHID,DATEINPUT,DATEMODIFIED,MODIFYBY
				FROM $intranet_db.IES_STAGE_HANDIN_FILE $ParCond";

		$returnArray = array();
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function getStageHandinFileInfo_byFileId($ParFileId) {
		return $this->getStageHandinFileInfo("WHERE FILEID = '{$ParFileId}'");
	}

	public function getStageHandinFileInfo_byBatchId_orderByDateModifiedDesc($ParBatchId) {

		return $this->getStageHandinFileInfo("WHERE BATCHID = '{$ParBatchId}' ORDER BY DATEMODIFIED DESC");
	}
	###############Bookmark: TABLE_HANDLING##################
	##	End:	function getStageHandinFileInfo() - handling table IES_STAGE_HANDIN_FILE
	##			and corresponding assistance functions
	#########################################################
	###############Bookmark: TABLE_HANDLING##################
	##	Start:	function getStageHandinCommentInfo() - handling table IES_STAGE_HANDIN_COMMENT
	##			and corresponding assistance functions
	#########################################################
	/**
	 * Get information from database table IES_STAGE_HANDIN_COMMENT
	 * @param	String $ParCond	- The Sql condition, *Please input with WHERE
	 * @return	Array the associative array for result get from IES_STAGE_HANDIN_COMMENT
	 */
	private function getStageHandinCommentInfo($ParCond="") {
		global $intranet_db;

		$sql = "SELECT BATCHCOMMENTID,BATCHID,COMMENT,DATEINPUT,INPUTBY
				FROM $intranet_db.IES_STAGE_HANDIN_COMMENT $ParCond";
		$returnArray = array();
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	###############Bookmark: TABLE_HANDLING##################
	##	End:	function getStageHandinCommentInfo() - handling table IES_STAGE_HANDIN_COMMENT
	##			and corresponding assistance functions
	#########################################################


	/**
	 * Check whether the user (Teacher) already assigned to any one of the scheme
	 * for access right checking
	 * @return INT, 1 : true; 0 : false
	 */
	public function isIES_Teacher($user_id)
	{
		$sql = "SELECT COUNT(*) FROM IES_SCHEME_TEACHER as teacher inner join IES_SCHEME as scheme on scheme.SchemeId = teacher.schemeid  WHERE teacher.UserID='$user_id' and scheme.schemetype = 'ies'";

		$result = $this->returnVector($sql);
		$returnValue = ($result[0]==0) ? 0 : 1;
		return $returnValue;
	}

	/**
	 * Check whether the user (Student) already assigned to any one of the scheme
	 * for access right checking
	 * @return INT, 1 : true; 0 : false
	 */
	public function isIES_Student($user_id)
	{
		global $ies_cfg;
		$sql = "SELECT COUNT(*) FROM IES_SCHEME_STUDENT as student inner join IES_SCHEME as scheme on scheme.SchemeID = student.SchemeID WHERE student.UserID='$user_id' and scheme.schemetype = 'ies'";

		$result = $this->returnVector($sql);
		$returnValue = ($result[0]==0) ? 0 : 1;
		return $returnValue;
	}

   /**
	* GIVE A scheme id , return the task that related to that scheme
	*
	* @param : INT $parSchemeID request Scheme ID details
	* @return : DB result for the scheme
	*
	*/

	public function getSchemeStage($parSchemeID){
	  global $intranet_db;

	  $ldb = new libdb();

	  $sql = "SELECT StageID, Deadline, Title FROM {$intranet_db}.IES_STAGE WHERE SchemeID = '{$parSchemeID}'";
	  $schemeStageArr = $ldb->returnArray($sql);

	  return $schemeStageArr;
	}

	/**
	 * get the stage description by give a stage seq, suppose all scheme with the same stage should have the equal description
	 *
	 * @param : INT $stageSeq   Stage SEQ
	 * @return : Stage Seq Description with HTML Format
	 *
	 */
	public function getStageDescription($stageSeq)
	{
		global $Lang;
		$htmlDesc = $Lang['IES']['StageDesc_seqDefault'];

		switch ($stageSeq)
		{
		   case 1:
				$htmlDesc = $Lang['IES']['StageDesc_seq1'];
				break;
			case 2:
				$htmlDesc = $Lang['IES']['StageDesc_seq2'];
				break;
			case 3:
				$htmlDesc = $Lang['IES']['StageDesc_seq3'];
				break;
		}

		return $htmlDesc;
	}

	/**
	 * Retrieve Note Id by the stageid and the userid
	 * @param	int	$ParUserId - the user id
	 * @param	int	$ParStageId - the stage id
	 * @return	A note id that belong to a student in a scheme
	 */
	public function retrieveNoteIdByStageIdAndUserId($ParUserId,$ParStageId) {
		global $intranet_db;
		$ldb = new libdb();
		$sql = "SELECT IRN.NOTEID
				FROM $intranet_db.IES_REFLECT_NOTE IRN
				INNER JOIN IES_SCHEME_STUDENT ISCHS
					ON IRN.SCHEMESTUDENTID = ISCHS.SCHEMESTUDENTID
				INNER JOIN IES_STAGE ISTA
					ON ISCHS.SCHEMEID = ISTA.SCHEMEID
				WHERE ISTA.STAGEID = '$ParStageId'
					AND ISCHS.USERID = '$ParUserId'
				";

		$returnArray = $ldb->returnArray($sql);
		$noteid = $returnArray[0]["NOTEID"];
		return $noteid;
	}

	/**
	 * get the selection menu of Scheme
	 * @param	int	$selectedSchemeID - the selected scheme id
	 * @return	selection menu
	 */
	public function getSchemeSelection($selectedSchemeID="",$skipSchemeID = '')
	{
		global $i_alert_pleaseselect,$ies_cfg;

		$sql  = "SELECT SchemeID, Title FROM IES_SCHEME where 1 and SchemeType = '".$ies_cfg["moduleCode"]."'";

		if($skipSchemeID!='')
		{
			$sql .= " and SchemeID!='{$skipSchemeID}' ";
		}

		$sql .= "ORDER BY Title";

		$schemeData = $this->returnArray($sql,2);

		$blank_selected = ($selectedSchemeID=="") ? " selected" : "";
		$schemeMenu = "<SELECT NAME='schemeID' id='schemeID'>";
		$schemeMenu .= "<OPTION VALUE='' {$blank_selected}>--- ".$i_alert_pleaseselect." ---</OPTION>";
		for($i=0; $i<sizeof($schemeData); $i++) {
			$selected = ($schemeData[$i][0]==$selectedSchemeID) ? " selected" : "";
			$schemeMenu .= "<OPTION value='".$schemeData[$i][0]."' {$selected}>".$schemeData[$i][1]."</OPTION>";
		}
		$schemeMenu .= "</SELECT>";

		return $schemeMenu;
	}

	/**
	 * Retrieve file list in the file path
	 * @param	String $path - file path
	 * @return	file list in list area
	 */
	public function getAttachmentArray($path)
	{
		global $file_path, $image_path, $intranet_httppath;

		$a = new libfiletable("", $path, 0, 0, "");
		$files = $a->files;

		$ct = 0;

		while (list($key, $value) = each($files))
		{

			$returnArr[$ct][0] = $files[$key][0];
			$returnArr[$ct][1] = $files[$key][0];
			$ct++;
		}

		return $returnArr;
	}

	/**
	* store WORKSHEET HANDIN FILE (By teacher / student)
	* @param	String $dbtable, table name which is going to insert the File info
	* @param	Array $dataAry, the corresponding data which are waiting to insert in db
	* @return : INT INSERT FILE ID
	*/
	public function storeHandInFile($dbtable, $dataAry=array())
	{
		global $UserID;

		foreach($dataAry as $fields[]=>$values[]);

		$sql = "INSERT INTO ".$dbtable." (";
		foreach($fields as $field) $sql .= $field.", ";
		$sql .= " DateInput, InputBy, DateModified, ModifyBy) VALUES (";
		foreach($values as $value) $sql .= "'".$value."', ";
		$sql .= " NOW(), '$UserID', NOW(), '$UserID')";

		$this->db_db_query($sql);
		$fileId = mysql_insert_id();

		return $fileId;
	}

	/**
	* retrieve WORKSHEET HANDIN FILE (By teacher / student)
	* @param	String $dbtable, table name which is going to insert the File info
	* @param	String $ParcCond, condition of sql statement
	* @return : Array $returnArray, return the data in array
	*/
	public function getHandinFileInfo($table, $ParCond="") {
		global $intranet_db;

		$sql = "SELECT WorksheetFileID, FileName, FolderPath, FileHashName, DateInput, DateModified, ModifyBy FROM $table $ParCond";


		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	/**
	* remove WORKSHEET HANDIN FILE Physically (By teacher / student)
	* @param	String $dbtable, get the file info from $table
	* @param	String $ParFileId, WorksheetFileID of the table
	* @return : Boolean $removeResult, true : success; false : fail
	*/
	public function removeWorksheetFilePhysical($table, $ParFileId) {

		$libfilesystem = new libfilesystem();
		$infoArray = current($this->getHandinFileInfo($table, " WHERE WorksheetFileID = '".$ParFileId."'"));
		$FilePath = $infoArray["FolderPath"];
		$FileHashName = $infoArray["FileHashName"];
		$FullFileName = $infoArray["FolderPath"].$infoArray["FileHashName"];
		$removeResult = false;
		if (file_exists($FullFileName)) {


			$removeResult = $libfilesystem->file_remove($FullFileName);
		} else {
			$removeResult = true;
		}
		return $removeResult;
	}

	/**
	* remove WORKSHEET HANDIN FILE record in table (By teacher / student)
	* @param	String $table, table record will be deleted
	* @param	String $fileId, WorksheetFileID of the table
	* @return : Boolean $returnResult, true : success; false : fail
	*/
	public function removeHandInFile($table, $fileId)
	{
		$sql = "delete from $table where WorksheetFileID = '{$fileId}'";

		$returnResult = $this->db_db_query($sql);
		return $returnResult;
	}

	/**
	* create WORKSHEET
	* @param	Array $dataAry, data to be insert into table "IES_WORKSHEET"
	* @return : int $insert_id, WorksheetID of new worksheet
	*/
	public function insertWorksheet($dataAry=array())
	{
		global $intranet_root, $UserID, $ies_cfg, $lfs;
		foreach($dataAry as $fields[]=>$values[]);

		# insert into table
		$sql = "INSERT INTO IES_WORKSHEET (";
		foreach($fields as $field) $sql .= $field.", ";
		$sql .= "RecordStatus, DateInput, InputBy, DateModified, ModifyBy) VALUES ('";
		foreach($values as $value) $sql .= $value."', '";
		$sql .= $ies_cfg["recordStatus_approved"]."', NOW(), '$UserID', NOW(), '$UserID')";




		$this->db_db_query($sql);
		$insert_id = $this->db_insert_id();

		# move file from tmp folder to destination
		$schemeID = $dataAry['schemeID'];
		$path = $intranet_root."/file/ies/scheme/scheme_s".$schemeID."/worksheet/";
		if(!is_dir($path))
		{
			$lfs->folder_new($path);
		}

		$insertPath = "/file/ies/scheme/scheme_s".$schemeID."/worksheet/";

		$sql = "SELECT WorksheetFileID, FileHashName, FolderPath FROM IES_WORKSHEET_TEACHER_FILE WHERE WorksheetID=0 AND InputBy='$UserID'";

		$temp = $this->returnArray($sql, 3);


		for($i=0; $i<sizeof($temp); $i++) {
			list($id, $hashName, $folder) = $temp[$i];

			$sourceFilePath = $intranet_root.$folder.$hashName;
			if(file_exists($sourceFilePath)) {
				$sucess = $lfs->file_copy($sourceFilePath, $path);			# copy to destination
				$sucess = $lfs->file_remove($sourceFilePath);  	# remove the original
			}

		}

		# assign WorksheetID to FILE table
		$sql = "UPDATE IES_WORKSHEET_TEACHER_FILE SET WorksheetID='$insert_id', FolderPath='$insertPath' WHERE WorksheetID=0 AND InputBy='$UserID'";
		$this->db_db_query($sql);

		return $insert_id;
	}

	/**
	* Remove any "Orphan" File with worksheet id = 0 in IES_WORKSHEET_TEACHER_FILE that is created by the user
	*
	* For creating a new worksheet, user may upload / attach a file to a worksheet before a worksheet is created (WorksheetID = 0),
	* User may upload file but give up the worksheet create, such that it may leave many orphan files with WorksheetID = 0
	* This function is remove those file that is created by the user before NEW any worksheet for the user
	*
	* @owner : Fai (20110817)
	* @param : Int $UserID , the file that is upload / created by whom
	* @return : Nil
	*
	*/
	public function removeTemporaryWorkSheet($parUserID){
		if(is_numeric($parUserID)){
			$sql = 'delete from IES_WORKSHEET_TEACHER_FILE where worksheetID = 0 and InputBy = \''.$parUserID."'";

			$this->db_db_query($sql);
		}
	}
	/**
	* update WORKSHEET
	* @param	Array $dataAry, data to be insert into table "IES_WORKSHEET"
	* @param	int $worksheetID, worksheet ID of the worksheet
	*/
	public function updateWorksheet($dataAry=array(), $worksheetID)
	{
		global $intranet_root, $UserID, $ies_cfg, $lfs;

		$sql = "SELECT SchemeID FROM IES_WORKSHEET WHERE WorksheetID='$worksheetID'";
		$result = $this->returnVector($sql);
		$originalSchemeID = $result[0];

		# update Worksheet Info
		$sql = "UPDATE IES_WORKSHEET SET ";
		foreach($dataAry as $field=>$value) $sql .= $field."='$value', ";
		$sql .= " ModifyBy=$UserID, DateModified=NOW() WHERE WorksheetID='$worksheetID'";
		$this->db_db_query($sql);

		# check whether folder is existed, create the destination folder if does not exist
		$schemeID = $dataAry['schemeID'];
		$dbStorePath = "/file/ies/scheme/scheme_s".$schemeID."/";
		$path = $intranet_root.$dbStorePath;

		if(!is_dir($path)) {
			$lfs->folder_new($path);
		}

		$dbStorePath .= "worksheet/";
		$path .= "worksheet/";

		if(!is_dir($path)) {
			$lfs->folder_new($path);
		}
		$sql = "UPDATE IES_WORKSHEET_TEACHER_FILE SET WorksheetID='$worksheetID', DateModified=NOW() WHERE WorksheetID=0 AND InputBy='$UserID'";
		$this->db_db_query($sql);


		# move file from original to destination if SCHEME is changed (old file)
		//if($originalSchemeID != $schemeID) {
			$sql = "SELECT WorksheetFileID, FileHashName, FolderPath FROM IES_WORKSHEET_TEACHER_FILE WHERE WorksheetID='$worksheetID'";
			$temp = $this->returnArray($sql, 3);
			for($i=0; $i<sizeof($temp); $i++) {
				list($id, $hashName, $folder) = $temp[$i];
				$srcPath = $intranet_root.$folder.$hashName;

				if(file_exists($srcPath)) {

					$lfs->file_copy($srcPath, $path);			# copy to destination

					if($originalSchemeID != $schemeID) {
						$lfs->file_remove($folder.$hashName);				# remove the original
					}
				}
			}
		//}
		# assign WorksheetID to FILE table
		$sql = "UPDATE IES_WORKSHEET_TEACHER_FILE SET FolderPath='$dbStorePath', DateModified=NOW() WHERE WorksheetID='$worksheetID'";
		$this->db_db_query($sql);

	}

	/**
	* remove WORKSHEET
	* @param	int $worksheetID, worksheet ID of the worksheet
	*/
	public function removeWorksheet($worksheetID)
	{
		global $lfs;

		# remove teacher file (if any)
		$this->removeTeacherHandinFile($worksheetID);

		# remove student file (if any)
		$this->removeStudentHandinFile($worksheetID);

		# remove record from DB
		$sql = "DELETE FROM IES_WORKSHEET WHERE WorksheetID='$worksheetID'";
		$this->db_db_query($sql);
	}

	/**
	* remove Worksheet FILE (uploaded by teacher)
	* @param	int $worksheetID, worksheet ID of the worksheet
	* @param	int $user_id, Input User ID
	*/
	public function removeTeacherHandinFile($worksheetID, $user_id="")
	{
		global $lfs;
		if($user_id!="") $conds = " AND InputBy='$user_id'";

		$sql = "SELECT FileHashName, FolderPath FROM IES_WORKSHEET_TEACHER_FILE WHERE WorksheetID='$worksheetID' $conds";
		$result = $this->returnArray($sql,2);

		for($i=0; $i<sizeof($result); $i++) {
			$targetFile = $result[$i][1].$result[$i][0];
			if(file_exists($targetFile)) {
				$flag = $lfs->file_remove($targetFile);
			}
		}
		$sql = "DELETE FROM IES_WORKSHEET_TEACHER_FILE WHERE WorksheetID='$worksheetID' $conds";
		$this->db_db_query($sql);
	}

	/**
	* remove Worksheet FILE (uploaded by student)
	* @param	int $worksheetID, worksheet ID of the worksheet
	*/
	public function removeStudentHandinFile($worksheetID)
	{
		global $lfs;
		$sql = "SELECT FileHashName, FolderPath FROM IES_WORKSHEET_HANDIN_FILE WHERE WorksheetID='$worksheetID'";
		$result = $this->returnArray($sql,2);
		for($i=0; $i<sizeof($result); $i++) {
			$targetFile = $result[$i][1].$result[$i][0];
			if(file_exists($targetFile)) {
				$flag = $lfs->file_remove($targetFile);
			}
		}
		$sql = "DELETE FROM IES_WORKSHEET_HANDIN_FILE WHERE WorksheetID='$worksheetID'";
		$this->db_db_query($sql);
	}

	/**
	* get Worksheet information
	* @param	int $worksheetID, worksheet ID of the worksheet
	* @return	Array $result, worksheet information array
	*/
	public function getWorksheetDetail($worksheetID)
	{
		if($worksheetID!="") {
			$sql = "SELECT * FROM IES_WORKSHEET WHERE WorksheetID='$worksheetID'";
			$result = $this->returnArray($sql);
			return $result[0];
		}
	}

	/**
	* check any student submission in worksheet (control display of scheme menu selection
	* @param	int $worksheetID, worksheet ID of the worksheet
	* @return	Boolean, 1:true; 0:false
	*/
	public function checkStudentSubmitWorksheetHandin($worksheetID)
	{
		$sql = "SELECT COUNT(*) FROM IES_WORKSHEET_HANDIN_FILE WHERE WorksheetID='$worksheetID'";
		$result = $this->returnVector($sql);

		return ($result[0]>0)?1:0;
	}

	/**
	* get Worksheet submitted File information
	* @param	String $table, table name to be retrieved
	* @param	int $worksheetID, worksheet ID of the worksheet
	* @param	int $conds, condition of SQL
	* @return	Array, submitted file information
	*/
	public function getWorksheetFileData($table, $worksheetID, $conds="")
	{
		$sql = "SELECT WorksheetFileID, FileName, FolderPath, FileHashName, LEFT(DateModified,10) FROM $table WHERE WorksheetID='$worksheetID' $conds";

		return $this->returnArray($sql, 4);
	}

	/**
	* get Student list in scheme by WorksheetID
	* @param	int $worksheetID, worksheet ID of the worksheet
	* @return	Array, student information
	*/
	public function getWorksheetStudent($worksheetID)
	{
		$name_field = getNameFieldByLang("USR.");
		$teacherName_field = getNameFieldByLang("Teacher.");
		//GROUP BY --> there is many IES_WORKSHEET_HANDIN_FILE for IES_WORKSHEET
//		$sql = "SELECT USR.UserID, CONCAT(USR.ClassName,'-',USR.ClassNumber), $name_field, f.WorksheetFileID, c.Comment from IES_SCHEME_STUDENT s LEFT OUTER JOIN IES_WORKSHEET ws on (ws.SchemeID=s.SchemeID) LEFT OUTER JOIN IES_WORKSHEET_HANDIN_COMMENT c ON (c.WorksheetID=ws.WorksheetID AND c.StudentID=s.UserID) LEFT OUTER JOIN IES_WORKSHEET_HANDIN_FILE f ON (f.WorksheetID=ws.WorksheetID) LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=s.UserID) WHERE ws.WorksheetID=$worksheetID GROUP BY s.UserID ORDER BY USR.ClassName, USR.ClassNumber";

		$sql = "SELECT USR.UserID, CONCAT(USR.ClassName,'-',USR.ClassNumber), $name_field, f.WorksheetFileID, c.Comment ,{$teacherName_field} as TeacherName , DATE_format(c.DateModified , '%Y-%m-%d') as DateModified from IES_SCHEME_STUDENT s LEFT OUTER JOIN IES_WORKSHEET ws on (ws.SchemeID=s.SchemeID) LEFT OUTER JOIN IES_WORKSHEET_HANDIN_COMMENT c ON (c.WorksheetID=ws.WorksheetID AND c.StudentID=s.UserID) LEFT OUTER JOIN IES_WORKSHEET_HANDIN_FILE f ON (f.WorksheetID=ws.WorksheetID) LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=s.UserID) left join INTRANET_USER as Teacher on Teacher.UserID = c.InputBy WHERE ws.WorksheetID='$worksheetID' GROUP BY s.UserID ORDER BY USR.ClassName, USR.ClassNumber";




		return $this->returnArray($sql,5);
	}

	/**
	* load resources for file upload (ajax script)
	* @param	int $schemeID, scheme ID of the scheme
	* @return : String $returnValue, script of ajax
	*/
	//handle upload file (involve part , worksheet)
	public function returnFileUploadResources($schemeID, $userType=1, $newEditFlag, $worksheetID="",$studentID = '')
	{


		global $ies_cfg, $Lang;
		switch($userType) {
			case $ies_cfg['HandinFileByTeacher']	:
							$table = "IES_WORKSHEET_TEACHER_FILE";
							$type = $ies_cfg['HandinFileByTeacher'];
			break;
			case $ies_cfg['HandinFileByStudent']	:
							$table = "IES_WORKSHEET_HANDIN_FILE";
							$type = $ies_cfg['HandinFileByStudent'];
			break;
			default 								:
				return 'userType not find.';
			break;
		}

		$load_file_list = "";
		if($newEditFlag==$ies_cfg['WorksheetFlag_Edit']) {	# edit worksheet->load previous upload
			$sqlCond = '';
			if($userType == $ies_cfg['HandinFileByStudent']){
				$sqlCond = ' and StudentID ='.$studentID.' ';
			}
			$fileRecord = $this->getWorksheetFileData($table, $worksheetID,$sqlCond);
			for($i=0; $i<sizeof($fileRecord); $i++) {
				list($file_id, $file_name, $folder_path, $file_hashname, $modifiedDate) = $fileRecord[$i];
				$load_file_list .= "addFileDisplay('$file_id', '$file_name', '$file_hashname', '$modifiedDate', true);";


			}
		}
		$returnValue = '
			<script language="javascript">
			var fileAdded = 0;
			$(document).ready(function () {
				loadHandinBlock("current");
				'.$load_file_list.'
			});
			function loadHandinBlock(ParType) {
						$.ajax({
							url:      "/home/eLearning/ies/ajax/ajax_load_upload_file.php",
							type:     "POST",
							data:     "loadType="+ParType+"&schemeID='.$schemeID.'&userType='.$type.'&sheetID='.$worksheetID.'&studentID='.$studentID.'",
							async:	  false,
							error:    function(xhr, ajaxOptions, thrownError){
										alert(xhr.responseText);
									  },
							success:  function(xml){
										  result = $(xml).find("result").text();
										  allowJsRemoveOn = $(xml).find("js_allow_remove_on").text();

										  if (result) {
											$("div[id=handin_block]").html(result);
											loadFilesDisplay(allowJsRemoveOn);
											initializeSubmissionButton();

										  }
										  else {
											alert("init block failed");
										  }
									  }
						});
			}
			function loadFilesDisplay(ParAllowJsRemove) {
				if (window.jsFileArray) {
					$.each(jsFileArray, function(key, element) {
						addFileDisplay(element["WorksheetFileID"], element["FileName"], element["FileHashName"], element["DateModified"],ParAllowJsRemove);
					});
				}
			}

			function initializeSubmissionButton() {

				if (window.jsFileArray && jsFileArray.length) {
						fileAdded = jsFileArray.length;
				}
			}

			function startUpload(){

				  if (!$("input[name=submitFile]").val()) {
					alert("Please Select A File");
					return false;
				  }
				  $("#f1_upload_process").css("display", "");
				  $("div[class=submission_upload]").css("display", "none");
				  return true;
			}

			function stopUpload(success, file_id, file_name, file_hash_name, date_modified){
				  $("#f1_upload_process").css("display", "none");
				  $("div[class=submission_upload]").css("display", "");


				  if (success) {

					addFileDisplay(file_id, file_name, file_hash_name, date_modified, true);

					if (!fileAdded++) {
						initializeSubmissionButton();
					}
				  }
				  $("form[id=uploadForm]")[0].reset();
				  return true;
			}

			function addFileDisplay(file_id, file_name, file_hash_name, date_modified, is_removable) {
				var removeBtn = "";
				if (Number(is_removable)) {
					removeBtn = "<a href=\'javascript: void(0);\' id=\'remove_"+file_id+"\' class=\'deselect\' title=\''.$Lang['IES']['Delete'].'\'onClick=\'javascript: removeFile(this);\'>&nbsp;<\/a>";
				}
				var displayFileHtml = "<div class=\'IES_file_list\'><span><a href=\'/home/eLearning/ies/getfile.php?type='.$userType.'&fhn="+file_hash_name+"\'>"+file_name+"</a><br \/><span class=\'update_record\'>("+date_modified+")<\/span><\/span>"+removeBtn+"<div class=\'clear\'><\/div>	<\/div>";
				$(displayFileHtml).insertBefore($("div[id=submission_upload]"));
			}

			function removeFile(obj) {

				var confirmDelete = confirm("'.$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile'].$Lang['_symbol']["questionMark"].'");
				if (confirmDelete) {

					remove_id = $(obj).attr("id");

					$target = $(obj).parent();
					$target.hide();

					file_id = (remove_id.split("_"))[1];


					$.ajax({
						url:      "/home/eLearning/ies/ajax/ajax_remove_files.php",
						type:     "POST",
						data:     "file_id="+file_id+"&userType="+'.$userType.',
						async:	  false,
						error:    function(xhr, ajaxOptions, thrownError){
									alert(xhr.responseText);
								  },
						success:  function(xml){
									  result = $(xml).find("result").text();
									  if (result) {
										$target.remove();
										fileAdded--;
										if (fileAdded == 0) {
											$("#studentSubmission").hide();
										}
									  } else {
										$target.show();
										alert("remove failed");
									  }
								  }
					});
				}
			}

			</script>
			<div id="handin_block"></div>
		';

		return $returnValue;
	}

	/**
	* update Worksheet Handin Comment by teacher
	* @param	int $worksheetID, worksheet ID of the worksheet
	* @param	int $studentID, student ID in worksheet
	* @param	int $comment, handin comment
	* @return : Boolean, 1: success; 0:fail
	*/
	public function updateWorksheetHandinComment($worksheetID, $studentID, $comment="")
	{
		global $UserID;
		/*
		# remove existing comment
		$sql = "DELETE FROM IES_WORKSHEET_HANDIN_COMMENT WHERE WorksheetID=$worksheetID AND StudentID=$studentID";
		$this->db_db_query($sql);

		# add new comment if any
		//echo $worksheetID.'/'.$studentID.'/'.$comment;
		if($comment!="") {
			$sql = "INSERT INTO IES_WORKSHEET_HANDIN_COMMENT (WorksheetID, StudentID, Comment, DateInput, InputBy, DateModified, ModifyBy) VALUES ('$worksheetID', '$studentID', '$comment', NOW(), '$UserID', NOW(), '$UserID')";
			return $this->db_db_query($sql);
		}
		*/

		$sql = "INSERT INTO IES_WORKSHEET_HANDIN_COMMENT
					(WorksheetID, StudentID, Comment, DateInput, InputBy, DateModified, ModifyBy)
				VALUES
					('$worksheetID', '$studentID', '$comment', NOW(), '$UserID', NOW(), '$UserID')
				on duplicate key update comment=values(comment),ModifyBy=values(ModifyBy),DateModified=now()
			   ";



		$result = $this->db_db_query($sql);
		if($result == false){
			return false;
		}else{
			$insertID = $this->db_insert_id();
			return $insertID;
		}

	}

		/**
	* Get the task ans type
	* Eric Yip (20100903):
	* - Since task type is the same though different student answer the task
	* - UserID is not neccessary for checking task type
	*
	* @return Type(String)
	*/
	public function CheckTaskAnsType($task_id, $ParUserId=""){
		$li = new libdb();
		//$sql = "SELECT QuestionType FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = {$task_id} AND UserID = {$ParUserId}";
		$sql = "SELECT QuestionType FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = '{$task_id}'";
		$sql .= ($ParUserId == "") ? "" : " AND UserID = '{$ParUserId}'";
		$task_type = current($li->returnVector($sql));
		return $task_type;

	}
	public function GetTableContent($st){
		global $ies_cfg, $Lang;
		$table = "<tr><th>#</th><th>".$Lang['IES']['Date']."</th><th>".$Lang['IES']['WorkItem']."/".$Lang['IES']['Content']."</th>";
		$table .= "<th>".$Lang['IES']['WorkPurpose']."</th></tr>";

		$col_number = 3;
		$row_number = 0;
		$tr = strpos($st, $ies_cfg["new_set_question_answer_sperator"]);
		$subst = "";

		while(strlen($st) != 0){
			$tr = strpos($st, $ies_cfg["new_set_question_answer_sperator"]);

			$row_number++;
			$table .= "<tr>";
			$table .= "<td>".$row_number."</td>";
			$i = 0;


			if($tr != false){
				$subst = substr($st, 0 ,$tr);
				$st = substr($st, $tr + strlen($ies_cfg["new_set_question_answer_sperator"]));

			}
			else{
				$subst = $st;
				$st = "";
			}

			$td = 1;
			while($i < $col_number){
				if($i < $col_number - 1){
					$td = strpos($subst, $ies_cfg["question_answer_sperator"]);
					$table .= "<td>".substr($subst, 0, $td)."</td>";

					$subst = substr($subst, $td + strlen($ies_cfg["question_answer_sperator"]));
				}
				else{
					$table .= "<td>".$subst."</td>";

				}

				$i++;

			}
			$table .= "</tr>";


		}

		return $table;
	}

	/**
	* update Task handin by step handin
	* @param	int $taskID, ID of task
	* @param	text	$taskContent, task content
	* @param	text	$taskType, handin type of task
	* @param	int $studentID, student ID in task
	* @return : Boolean, 1: success; 0:fail
	*/
	public function UPDATE_TASK_CONTENT($taskID, $taskContent, $taskType, $studentID)
	{
    global $intranet_db;

    $sql = "INSERT INTO {$intranet_db}.IES_TASK_HANDIN ";
    $sql .= "(TaskID, UserID, Answer, QuestionType, DateInput, InputBy, ModifyBy) VALUES ";
    $sql .= "({$taskID}, {$studentID}, '".mysql_real_escape_string(htmlentities(stripslashes($taskContent), ENT_QUOTES, "UTF-8"))."', '{$taskType}', now(), {$studentID}, {$studentID}) ";
    $sql .= "ON DUPLICATE KEY UPDATE Answer = VALUES(Answer), QuestionType = VALUES(QuestionType),ModifyBy = VALUES(ModifyBy)";
    $res = $this->db_db_query($sql);

    return $res;
  }

  public function get_student_stage_details($stageID,$studentId)
	{
	      global $intranet_db;
	  $sql = "select StageID,UserID,BatchHandinStatus,Score from IES_STAGE_STUDENT where StageID = '{$stageID}' and UserID = '{$studentId}'";


	  $res = current($this->returnArray($sql));
	  return $res;

	}
	/**
	* get scheme by user ID
	* @param	int $parUserID, ID of User
	* @return : ARRAY
	*/
  public function GET_USER_SCHEME_ARR($parUserID)
  {
    $scheme_arr = array();

    if($this->IS_ADMIN_USER($parUserID))
    {
      $scheme_arr = $this->GET_SCHEME_ARR();
    }
    else if($this->isIES_Teacher($parUserID))
    {
      $scheme_arr = $this->GET_TEACHER_SCHEME_ARR($parUserID);
    }
    else if($this->isIES_Student($parUserID))
    {
      $scheme_arr = $this->GET_STUDENT_SCHEME_ARR($parUserID);
    }

    return $scheme_arr;
  }

  public function IS_ADMIN_USER($ParUserID='')
  {
  	if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"])
	{
		return true;
	}
	else{
		return $_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"];
	}

  }

  /**
	* Check the access right
	* @param	String: function name (PS: All function must be in CAPITAL LETTER)
	* @return : Boolean
  */
//  public function CHECK_ACCESS($access_function="")
//  {
//  	global $UserID;
//	$uid = $uid ? $uid : $UserID;
//
//	if($this->IS_ADMIN_USER($uid))
//	{
//		return true;
//	}
//	else{
//		return false;
//	}
//  }

//  public function NO_ACCESS_RIGHT_REDIRECT($msg="", $redirect_link="")
//  {
//		echo "<script language='javascript'>";
//		echo "window.location='/'";
//		echo "</script>";
//  }



  private function GET_TEACHER_SCHEME_ARR($teacherID)
  {
    global $intranet_db,$ies_cfg;

    $sql =  "
              SELECT DISTINCT
                scheme.SchemeID,
                scheme.Title
              FROM
                {$intranet_db}.IES_SCHEME scheme
              INNER JOIN {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher
                ON scheme.SchemeID = scheme_teacher.SchemeID
              WHERE
                scheme_student.UserID = '{$studentID}'
				and scheme.SchemeType = '".$ies_cfg["moduleCode"]."'
              ORDER BY
                scheme.Title
            ";
    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }

  private function GET_STUDENT_SCHEME_ARR($studentID)
  {
    global $intranet_db,$ies_cfg;

    $sql =  "
              SELECT DISTINCT
                scheme.SchemeID,
                scheme.Title
              FROM
                {$intranet_db}.IES_SCHEME scheme
              INNER JOIN {$intranet_db}.IES_SCHEME_STUDENT scheme_student
                ON scheme.SchemeID = scheme_student.SchemeID
              WHERE
                scheme_student.UserID = '{$studentID}'
				and scheme.SchemeType = '".$ies_cfg["moduleCode"]."'
              ORDER BY
                scheme.Title
            ";
    $returnArr = $this->returnArray($sql);

    return $returnArr;
  }

  /**
	* Get array of data for loading task block in student view
	* @param	int $taskID, ID of task
	* @param	int $studentID, ID of student
	* @return : Array
  */
  public function GET_TASK_BLOCK_INFO($taskID, $studentID)
  {
    global $ies_cfg;

    // Get task info
    $task_arr = $this->GET_TASK_DETAIL($taskID);

	$returnArr["studentID"] = $studentID;
    $returnArr["task_id"] = $task_arr['TaskID'];
    $returnArr["title"] = $task_arr['Title'];
    $returnArr["description"] = $task_arr['Description'];
    $returnArr["instant_edit"] = $task_arr['InstantEdit'];
	$returnArr["stage_id"] = $task_arr['StageID'];


    // Get steps in task
    $step_arr = $this->GET_STEP_ARR($taskID);
    $returnArr["step_arr"] = $step_arr;
    $returnArr["first_step_id"] = $step_arr[0]["StepID"];

    // Check if task has been done
    $sql = "SELECT Answer,DateInput,DateModified,QuestionType FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = '{$taskID}' AND UserID = '{$studentID}'";
    $task_element = current($this->returnArray($sql));
//debug_r($sql);
    if(!empty($task_element))
    {
      switch(strtoupper($task_element["QuestionType"]))
      {
        default:
        ## stringfilter to filter out html special open (in config)
          $returnArr["task_answer"] = $this->stringfilter($task_element["Answer"]);
          break;
        case "SURVEY:EDIT":

          $returnArr["task_answer"] = $this->parseSurveyMapString($task_element["Answer"]);

          break;
        case "SURVEY:COLLECT":
        	$TaskDetail = $this->GET_TASK_DETAIL($taskID);
        	$StageID = $TaskDetail["StageID"];
           	$returnArr["surveydetailarray"] = $this->getTastAllSurveyDetail($StageID, $studentID, $taskID);
        break;
        case "SURVEY:ANALYSIS":
        	$TaskDetail = $this->GET_TASK_DETAIL($taskID);
        	$StageID = $TaskDetail["StageID"];
           	$returnArr["surveydetailarray"] = $this->getTastAllSurveyDetail($StageID, $studentID, $taskID);
          break;
      }
      $returnArr["task_question_type"] = $task_element["QuestionType"];
      $returnArr["task_input_date"] = date("d-m-Y", strtotime($task_element["DateInput"]));
      $returnArr["task_modified_date"] = date("d-m-Y", strtotime($task_element["DateModified"]));
    }

    // Check if any step in task has been done
    $sql = "SELECT count(*) FROM {$intranet_db}.IES_QUESTION_HANDIN handin INNER JOIN {$intranet_db}.IES_STEP step ON handin.StepID = step.StepID WHERE step.TaskID = '{$taskID}' AND handin.UserID = '{$studentID}'";
    $step_count = intval(current($this->returnVector($sql)));

    // Check if the task/any step in task has been done
    $returnArr["is_task_start"] = !($task_element === false && $step_count === 0);

    // Check for the task enable status
    $returnArr["is_task_enable"] = $this->isTaskEnable($taskID);

    // Check for the stage status status
    $returnArr["is_task_open"] = $this->isTaskOpen($taskID, $studentID);

    // check if there are any comments from teachers
    $returnArr["is_teacher_comment"] = $this->getTeacherCommentLatestTime($taskID, $studentID);
	$returnArr["is_teacher_score_on_task"] = $this->  getTeacherTaskScoreLatestTime($taskID, $studentID);

    return $returnArr;
  }

  public function isTaskEnable($parTaskID)
  {
    global $intranet_db, $ies_cfg;

  	$sql = "SELECT Enable FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$parTaskID}'";
  	$taskEnabled = (current($this->returnVector($sql)) == 1);

    return $taskEnabled;
  }

  private function isTaskOpen($parTaskID, $parUserID)
  {
    global $intranet_db, $ies_cfg;

  	$sql = "SELECT Approved FROM {$intranet_db}.IES_TASK_STUDENT WHERE TaskID = '{$parTaskID}' AND UserID = '{$parUserID}'";
  	$taskOpened = (current($this->returnVector($sql)) == $ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"]);

  	if(!$taskOpened)
  	{
      $sql = "SELECT Approval FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$parTaskID}'";
      $taskOpened = (current($this->returnVector($sql)) == 0);
    }

    return $taskOpened;
  }

  public function getCommentBank($ParCategoryID="",$SchemeType='')
  {
    global $intranet_db;
    $cond = " WHERE 1 ";
    if (empty($ParCategoryID)) {
    	// do nothing
    } else {
    	$cond .= " AND CATEGORYID = $ParCategoryID ";
    }
    if(!empty($SchemeType)){
    	$cond .= " AND SchemeType = '$SchemeType' ";
    }

    $ldb = new libdb();
    $sql = "SELECT Comment, CommentID, CommentEN FROM IES_COMMENT $cond ORDER BY Comment ASC";
    $returnArr = $ldb->returnArray($sql);

    return $returnArr;
  }

  /**
   * @return Array - All scheme version
   */
  static public function GetAllSchemesVersion() {
	global $intranet_db;
	global $ies_cfg;
  	$libdb = new libdb();
  	$sql = "SELECT SCHEMEID, TITLE, VERSION, LANGUAGE FROM {$intranet_db}.IES_SCHEME where SchemeType = '{$ies_cfg["moduleCode"]}' ";
  	$returnArray = $libdb->returnArray($sql);
  	return $returnArray;
  }

  public function getStudentStageSurvey($parStageID, $parUserID) {
    global $intranet_db, $ies_cfg;

    $sql =  "SELECT handin.Answer ";
    $sql .= "FROM {$intranet_db}.IES_TASK_HANDIN handin ";
    $sql .= "INNER JOIN {$intranet_db}.IES_TASK task ";
    $sql .= "ON handin.TaskID = task.TaskID ";
    $sql .= "WHERE ";
    $sql .= "task.StageID = '{$parStageID}' AND ";
    $sql .= "handin.UserID = '{$parUserID}' AND ";
    $sql .= "handin.QuestionType = 'survey:edit'";

    $survey_answer = current($this->returnVector($sql));

    $survey_map_arr = $this->parseSurveyMapString($survey_answer);

    return $survey_map_arr;
  }

  public function getStudentTaskSurvey($parTaskID, $parUserID) {
    global $intranet_db, $ies_cfg;

    $sql =  "SELECT Answer ";
    $sql .= "FROM {$intranet_db}.IES_TASK_HANDIN ";
    $sql .= "WHERE ";
    $sql .= "TaskID = '{$parTaskID}' AND ";
    $sql .= "UserID = '{$parUserID}' AND ";
    $sql .= "QuestionType = 'survey:edit'";

    $survey_answer = current($this->returnVector($sql));

    $survey_map_arr = $this->parseSurveyMapString($survey_answer);

    return $survey_map_arr;
  }

  public function parseSurveyMapString($parSurveyMapStr)
  {
    global $ies_cfg;

    // Init each question type pointing to null
    foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $survey_type)
    {
      list($_survey_code, $_survey_name) = $survey_type;

      $returnArray[$_survey_code] = null;
    }

    $survey_arr = explode($ies_cfg["new_set_question_answer_sperator"], $parSurveyMapStr);
    for($i=0, $i_max=count($survey_arr); $i<$i_max; $i++)
    {
      if(empty($survey_arr[$i])) continue;

      list($_survey_code, $_survey_id) = explode($ies_cfg["question_answer_sperator"], $survey_arr[$i]);
      $returnArray[$_survey_code] = explode($ies_cfg["answer_within_question_sperator"],$_survey_id);
    }
    return $returnArray;
  }

  /**
   * Turn the Survey Email Csv data to required format
   * @return Array - The EmailList required array format
   */
  public function refineSurveyEmailCsvDataToRequiredFormat($ParCsvObtainedData) {
  	global $ies_cfg;
  	$returnArray = array();
  	if (is_array($ParCsvObtainedData)) {
  		foreach($ParCsvObtainedData as $key => $emailInfo) {
  			$emailAddress = $emailInfo[0];

  			# default the send status as not sent
  			$returnArray[$emailAddress][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"]]
  				= $ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"]]["NOT_SENT"];
  		}
  	}
  	return $returnArray;
  }
  /**
   * Insert a new IES_SURVEY_EMAIL record
   * @param INT $ParSurveyID - SurveyID
   * @param String/Array $ParEmailList - Specific format of EmailList(Serialized)/Array
   * @return INT/boolean - SurveyEmailID if insert success, else false
   */
  public function newSurveyEmailList($ParSurveyID, $ParEmailList) {
  	global $intranet_db;
  	if (is_array($ParEmailList)) {
  		$ParEmailList = serialize($ParEmailList);
  	}
  	$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_EMAIL SET DATEINPUT = NOW(), SURVEYID = '$ParSurveyID', EMAILLIST = '$ParEmailList'";

  	$result = $this->db_db_query($sql);
  	if ($result) {
  		$result = mysql_insert_id();
  	}
  	return $result;
  }
  /**
   * Update the EmailList of IES_SURVEY_EMAIL
   * @param INT $ParEmailSurveyID - SurveyEmailID
   * @param String/Array $ParEmailList - Specific format of EmailList(Serialized)/Array
   * @return INT/boolean - SurveyEmailID if updated success, else false
   */
  public function updateSurveyEmailList($ParSurveyEmailID, $ParEmailList) {
  	global $intranet_db;
  	if (is_array($ParEmailList)) {
  		$ParEmailList = serialize($ParEmailList);
  	}
	$sql = "UPDATE {$intranet_db}.IES_SURVEY_EMAIL SET EMAILLIST = '$ParEmailList' WHERE SURVEYEMAILID = '$ParSurveyEmailID'" ;

	$result = $this->db_db_query($sql);
	if ($result) {
		$result = $ParSurveyEmailID;
	}
	return $result;
  }

  /**
   * Get the EmailList from DB IES_SURVEY_EMAIL
   * @param INT $ParSurveyEmailID - SurveyEmailID
   * @return Array - Survey EmailList
   */
  public function getSurveyEmailList($ParSurveyEmailID) {
  	global $intranet_db;
  	$sql = "SELECT EMAILLIST FROM {$intranet_db}.IES_SURVEY_EMAIL WHERE SURVEYEMAILID = '$ParSurveyEmailID'";
  	$emailList = current($this->returnVector($sql));
  	$returnResult = unserialize($emailList);
  	return $returnResult;
  }

  /**
   * Combine the old and new Survey EmailList data
   * @return Array - combined EmailList result
   */
  public function combineOldNewSurveyEmailList($ParOldData, $ParNewData) {
  	# checking before combine
  	if (!is_array($ParOldData) || count($ParOldData)==0) {
  		return $ParNewData;
  	}
  	if (!is_array($ParNewData) || count($ParNewData)==0) {
  		return $ParOldData;
  	}

  	$ParCombinedEmail = $ParOldData;
  	foreach($ParNewData as $email => $newEmailInfo) {
  		if (isset($ParOldData[$email])) {	# The new one exist in the old record
  			$ParCombinedEmail[$email] = $ParOldData[$email];	# keep the old record
  		} else {
  			$ParCombinedEmail[$email] = $newEmailInfo;	# add the new email to the old one
  		}
  	}

  	return $ParCombinedEmail;
  }


  /**
   * Get the SurveyEmail Csv data
   * @return Array/boolean - success return data array, else false
   */
  public function getSurveyEmailCsvData() {
  		global $ies_cfg;
		$userfile = $_FILES["userfile"];
		$filepath = $userfile["tmp_name"];
		$filename = $userfile["name"];

		$libimporttext = new libimporttext();
		if (!empty($filepath) && $libimporttext->CHECK_FILE_EXT($filename)) {
			$data = $libimporttext->GET_IMPORT_TXT($filepath);
			$result = $data;
		} else {
			$result = false;
		}
		return $result;
  }

  /**
   * Check the survey email csv data
   */
  public function checkSurveyEmailCsvData($ParRawCsvData) {
  		global $ies_cfg;
  		$headerResult = $this->checkSurveyEmailCsvHeader(array_shift($ParRawCsvData),$ies_cfg["SurveyEmailCSVHeader"]);
  		return $headerResult;
  }

  /**
   * Check the csv header format
   * @param String $ParCsvHeaderArray - Csv Header
   * @param Array $ParControlHeaderArray - Control Header format
   * @return boolean/Array	- true if success, error message array
   */
  public function checkSurveyEmailCsvHeader($ParCsvHeader, $ParControlHeader) {
  		global $ies_cfg;
  		$CsvHeader = explode($ies_cfg["CSV_Delimiter"],$ParCsvHeader);

  		$error_message = array();
  		if (count($ParCsvHeader) != count($ParControlHeader)) {
			$error_message[] = $this->getSurveyEmailErrorMessage(ies_cfg_ERR_MSG_TYPE_DIFF_FIELD_NUM);
  		}
  		if (is_array($ParControlHeader)) {
  			foreach($ParControlHeader as $key => $field) {
  				if (!in_array($field,$ParCsvHeader)) {
					$error_message[] = $this->getSurveyEmailErrorMessage(ies_cfg_ERR_MSG_TYPE_FIELD_MISS, array($field));
  				}
  			}
  		}
  		if (count($error_message)>0) {
  			$result = $error_message;
  		} else {
  			$result = true;
  		}
  		return $result;
  }

  public function getSurveyEmailErrorMessage($ParMessageType, $ParFillMessageArray=null) {
  		global $ies_cfg;

  		$mapping = array();
  		if (is_array($ParFillMessageArray) && count($ParFillMessageArray)>0) {
  			$sizeOfFills = count($ParFillMessageArray);
  			for($i=0;$i<$sizeOfFills;$i++){
  				$mapping[] = "{{{ $i }}}";
  			}
  		}
  		$msg = str_replace($mapping,$ParFillMessageArray,$ies_cfg["CSV_ErrorMessage"][$ParMessageType]);
  		return $msg;
  }

  public function updateSurveryEmailContent($ParSurveyID, $ParTitle, $ParContent){
  	global $intranet_db;
  	$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_EMAILCONTENT SET DateInput = NOW(), Content = '$ParContent', Title = '$ParTitle', SurveyID = '$ParSurveyID', DateModified = NOW() ON DUPLICATE KEY UPDATE Title ='$ParTitle', Content ='$ParContent', DateModified = NOW()";
  //	DEBUG($sql);

  	$result = $this->db_db_query($sql);

  	return $result;
  }

  public function getSurveyEmailContent($ParSurveyID){
  	global $intranet_db;

  	$sql = "SELECT *
			FROM {$intranet_db}.IES_SURVEY_EMAILCONTENT
			WHERE SURVEYID = '$ParSurveyID'";
	$returnArray = current($this->returnArray($sql));
	return $returnArray;
  }

  /**
   * Get the Details from DB Table IES_SURVEY_EMAIL
   * @param INT $ParSurveyEmailID - the SurveyEmailID
   * @param STRING $ParEmail - the email address
   * @return Array - Details of SurveyEmail
   */
  public function getSurveyEmailDetails($ParSurveyID, $ParEmail = '', $ParRESPONDENTNATURE= '') {
  	global $intranet_db, $ies_cfg;
  	if($ParRESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']){
  		$cond .= "AND ISE.EMAIL='$ParEmail' ";
  	}
  	else if($ParRESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
  		$cond .= "AND ISE.USERID='$ParEmail' ";
  	}
  	/* This is the approach to get Class title and class number from Year Class
  	$sql = "SELECT ISA.ANSWERID,ISE.SURVEYEMAILID,ISE.SURVEYID,ISE.DATEINPUT,ISE.DATEMODIFIED,ISE.EMAIL,ISE.EmailDisplayName,ISE.STATUS,ISE.RESPONDENTNATURE,ISE.USERID,
			IU.ENGLISHNAME, IU.IMAPUSEREMAIL, IU.CLASSNUMBER, YC.CLASSTITLEEN, YC.CLASSTITLEB5
			FROM {$intranet_db}.IES_SURVEY_EMAIL ISE
			LEFT JOIN {$intranet_db}.INTRANET_USER IU ON ISE.USERID = IU.USERID
			LEFT JOIN {$intranet_db}.YEAR_CLASS_USER YCU ON YCU.USERID = IU.USERID
			LEFT JOIN {$intranet_db}.YEAR_CLASS YC ON YCU.YEARCLASSID = YC.YEARCLASSID
			LEFT JOIN {$intranet_db}.IES_SURVEY_ANSWER ISA ON ISE.RESPONDENTNATURE = ISA.RESPONDENTNATURE AND ISE.SURVEYID = ISA.SURVEYID
				AND IF(ISE.RESPONDENTNATURE=".$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL'].",ISE.USERID,ISE.EMAIL)
				=IF(ISE.RESPONDENTNATURE=".$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL'].",ISA.USERID,ISA.RESPONDENT)
			WHERE ISE.SURVEYID = $ParSurveyID $cond ORDER BY ISE.STATUS DESC, ISE.EmailDisplayName
				AND YC.ACADEMICYEARID = ".Get_Current_Academic_Year_ID();
				*/
  	$sql = "SELECT ISA.ANSWERID,ISE.SURVEYEMAILID,ISE.SURVEYID,DATE_format(ISE.DATEINPUT, '%Y-%m-%d %k:%i:%S') as  DATEINPUT,ISE.DATEMODIFIED,ISE.EMAIL,ISE.EmailDisplayName,ISE.STATUS,ISE.RESPONDENTNATURE,ISE.USERID,
		IU.IMAPUSEREMAIL, ".getNameFieldWithClassNumberByLang("IU.")." AS USERNAME
		FROM {$intranet_db}.IES_SURVEY_EMAIL ISE
		LEFT JOIN {$intranet_db}.INTRANET_USER IU ON ISE.USERID = IU.USERID
		LEFT JOIN {$intranet_db}.IES_SURVEY_ANSWER ISA ON ISE.RESPONDENTNATURE = ISA.RESPONDENTNATURE AND ISE.SURVEYID = ISA.SURVEYID
			AND IF(ISE.RESPONDENTNATURE=".$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL'].",ISE.USERID,ISE.EMAIL)
			=IF(ISE.RESPONDENTNATURE=".$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL'].",ISA.USERID,ISA.RESPONDENT)
		WHERE ISE.SURVEYID = '$ParSurveyID' $cond ORDER BY ISE.STATUS DESC, ISE.EmailDisplayName";
//		echo $sql."<br/>";
	$returnArray = $this->returnArray($sql);
	return $returnArray;
  }

  public function getEmailFromEmailDisplay($ParEmailDisplay,$ParStringToLower=true){
  	$find = strpos($ParEmailDisplay, "<");

	$sub = substr($ParEmailDisplay, $find);
  	$pattern = array("<", ">");
  	$sub = str_replace($pattern, "", $sub);
  	if ($ParStringToLower) {
  		$returnString = strtolower($sub);
  	} else {
  		$returnString = $sub;
  	}
  	return $returnString;

  }

   public function getDisplayNameFromEmailDisplay($ParEmailDisplay){
  	$find = strpos($ParEmailDisplay, "<");

	$sub = substr($ParEmailDisplay, 0,$find);
  	return $sub;

  }

  /**
   * Insert new record with only email title and content
   * @param INT $ParSurveyID - the SurveyID
   * @param String $ParTitle - The title want to be insert
   * @param String $ParContent - The content want to be insert
   * @return INT/boolean - recordid when success inserted, else false
   */
  public function newSurveyEmailContents($ParSurveyID, $ParEmailDisplay, $ParRespondentNature="") {
	global $intranet_db,$ies_cfg;
	if (empty($ParRespondentNature)) {
		$ParRespondentNature = $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL'];
	}

	$EmailOrUserID = $this->getEmailFromEmailDisplay($ParEmailDisplay);
	if ($ParRespondentNature == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']) {
		$updateField = " USERID = '$EmailOrUserID' ";
		$updateField .= " , EMAIL = '' ";
	} else {
		$updateField = " EMAIL = '$EmailOrUserID' ";
		$updateField .= " ,USERID = 0 ";
	}
	$DisplayName = $this->getDisplayNameFromEmailDisplay($ParEmailDisplay);
  	$sql = "INSERT INTO {$intranet_db}.IES_SURVEY_EMAIL
			SET SURVEYID = '$ParSurveyID', DATEINPUT = NOW(), $updateField, EmailDisplayName = '$DisplayName', Status = 0, RespondentNature='{$ParRespondentNature}'
ON DUPLICATE KEY UPDATE EmailDisplayName ='$DisplayName'";
	$result = $this->db_db_query($sql);
	if ($result) {
		$result = mysql_insert_id();
	}
	return $result;
  }

  /**
   * Generate email check box for EmailList in IES_SURVEY_EMAIL
   * @param String/Array $ParEmailList - if String, the serialized string of the corresponding array
   * @return String - check box div of email list
   */
  public function generateSurveryEmailCheckBoxList($ParEmailList) {
  	global $ies_cfg;
  	if (!is_array($ParEmailList)) {
  		$ParEmailList = unserialize($ParEmailList);
  	}
  	$html = "";
  	if (is_array($ParEmailList) && count($ParEmailList)>0) {
  		$sent_address = array();
  		$checkBoxesField = array();
	  	foreach($ParEmailList as $email=>$element) {
	  		$checkBoxesField[] = "<input type='checkbox' name='emails[]' value='$email' id='$email' /><label for='$email'>$email</label>";
	  		if (
	  			$element[$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"]]
	  			== $ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"]]["SENT"]
	  			) {
	  				$sent_address[] = "\"".$email."\"";
	  			}
	  	}
	  	$js_sent_emails = "var sent_address = [".implode(",",$sent_address)."];\n";
	  	$js = <<<JSEND
	  <script>
	  {$js_sent_emails}
	  function emailCheckAll() {
	  	$.each($("input[name=emails\\[\\]]"), function() {
	  		$(this).attr("checked","checked");
	  	});
	  }
	  function emailCheckNotSent() {
	  	$.each($("input[name=emails\\[\\]]"), function() {
	  		var currentEmail = $(this).val();
	  		if (checkSent(currentEmail)) {
	  			$(this).removeAttr("checked");
	  		} else {
	  			$(this).attr("checked","checked");
	  		}
	  	});
	  }
	  function emailCheckSent() {
	  	$.each($("input[name=emails\\[\\]]"), function() {
	  		var currentEmail = $(this).val();
	  		if (checkSent(currentEmail)) {
	  			$(this).attr("checked","checked");
	  		} else {
	  			$(this).removeAttr("checked");
	  		}
	  	});
	  }
	  function checkSent(ParEmailAddress) {
	  	var sent = false;
	  	for(i=0;i<sent_address.length;i++) {
	  		if (sent_address[i] == ParEmailAddress) {
	  			sent = true;
	  			break;
	  		}
	  	}
	  	return sent;
	  }
	  $(document).ready(function(){
	  	emailCheckNotSent();
	  });
	  </script>
JSEND;
	  	$html .= "<input type='button' value='Check All' onClick='emailCheckAll();'/>
				<input type='button' value='Check Not Sent' onClick='emailCheckNotSent();'/>
				<input type='button' value='Check Sent' onClick='emailCheckSent();'/><br/>";
		$html .= implode("<br/>",$checkBoxesField);
  	}
  	$html = "<div>".$js.$html."</div>";
  	return $html;

  }

  /**
   * Get the Pure Email List from EmailList format from IES_SURVEY_EMAIL
   * @param String/Array $ParEmailList - if String, the serialized string of the corresponding array
   * @return Array - Pure email list
   */
  public function getSurveyEmailListPureEmail($ParEmailList) {
  	if (!is_array($ParEmailList)) {
  		$ParEmailList = unserialize($ParEmailList);
  	}
  	$pureEmailList = array();
  	if (is_array($ParEmailList)) {

	  	foreach($ParEmailList as $email=>$element) {
	  		$pureEmailList[] = $email;
	  	}
  	}
  	return $pureEmailList;
  }
  /**
   * Update the Email status
   * @param $ParSurveyEmailID - The SurveyEmailID
   * @param $ParStatus - Status (1 - sent/ 0 - not yet sent)
   *
   * @return boolean - true if sent successfully, vice versa
   */
  public function updateEmailStatus($ParSurveyID, $ParEmailORID, $ParStatus, $ParRESPONDENTNATURE){
  	global $intranet_db, $ies_cfg;
  	if($ParRESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']){
  		$cond .= " AND Email = '$ParEmailORID'";
  	}
  	else{
  		$cond .= " AND UserID = '$ParEmailORID'";
  	}
  	$sql = "UPDATE {$intranet_db}.IES_SURVEY_EMAIL SET Status = '$ParStatus' WHERE SURVEYID = '$ParSurveyID' $cond" ;
//  	echo $sql;
	$result = $this->db_db_query($sql);
	return $result;
  }

  public function deleteSurveyEmailContents($ParSurveyID, $ParEmailDisplay, $ParRespondentNature=""){
	global $intranet_db,$ies_cfg;

	if (empty($ParRespondentNature)) {
		$ParRespondentNature = $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL'];
	}

	$EmailOrUserID = $this->getEmailFromEmailDisplay($ParEmailDisplay);
	if ($ParRespondentNature == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']) {
		$whereField = " USERID = '$EmailOrUserID' ";
	} else {
		$whereField = " EMAIL = '$EmailOrUserID' ";
	}

//	$Email = $this->getEmailFromEmailDisplay($ParEmailDisplay);

	$sql = "DELETE FROM {$intranet_db}.IES_SURVEY_EMAIL WHERE SURVEYID = '$ParSurveyID' AND $whereField";

	$result = $this->db_db_query($sql);
	if ($result) {
		$result = mysql_insert_id();
	}
	return $result;
  }

  /**
   * Send email to users for survey email
   * @param Array $ParEmailArray - the email list to send email
   * @param String $ParTitle - The Email Subject
   * @param String $ParContent - The Email Message
   * @return boolean - true if sent successfully, vice versa
   */
  public function sendEmailForSurveyEmail($ParEmailArray,$ParTitle,$ParContent,$ParMailType="IMAIL") {
  	global $ies_cfg,$UserID,$iMail_Alias_Domain,$webmail_info;
  	if (!is_array($ParEmailArray)) {
  		$ParEmailArray = array($ParEmailArray);
  	}
  	$libwebmail = new libwebmail();
	$libuser = new libuser($UserID);

	$sql = "SELECT ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE UserID = '$UserID'";
	$StudentClassInfo = $this->returnVector($sql);

	if(sizeof($StudentClassInfo)>0){
 		$sender_name = $StudentClassInfo[0];
 	}else{
 		$sender_name = $libuser->UserName();
	}
	switch (strtoupper($ParMailType)) {
		default:
		case "IMAIL":

	         # for undelivery
	         $email_return_path = $libuser->UserLogin."@".($iMail_Alias_Domain ? $webmail_info['aliasdomain'] : $libwebmail->mailaddr_domain);

	         if ($sender_name != "")
	         {

	             $emailElement[] = $From = "\"".$sender_name."\" <".$email_return_path.">";
	         }
	         else
	         {
	             $emailElement[] = $From = $email_return_path; #$luser->UserLogin."@".$lwebmail->mailaddr_domain;
	         }
			break;
		case "GAMMAMAIL":
			$emailElement[] = $From = "\"".$sender_name."\" <".$libuser->ImapUserEmail.">";
			break;
	}
  	$emailElement[] = $To = $ParEmailArray;
  	$emailElement[] = $CC = null;
  	$emailElement[] = $BCC = null;
  	$emailElement[] = $Subject = $ParTitle;
  	$emailElement[] = $Message = $ParContent;
  	$emailElement[] = $Attachment = null;
  	$emailElement[] = $Important = false;
  	$emailElement[] = $MailReturnPath = "";
  	$emailElement[] = $ReplyAddress = "";
  	$emailElement[] = $MultipartMessage = true;
  	$result = $libwebmail->SendMail($Subject,$Message,$From,$To,$CC,$BCC,$Attachment,$Important,$MailReturnPath,$MailReturnPath,$ReplyAddress,$MultipartMessage);
	return $result;
  }

  /**
   * Update Fields in EmailList of IES_SURVEY_EMAIL
   * @param INT $ParSurveyEmailID - the SurveyEmailID
   * @param Array $ParEmailArray - A list of email in the specified SurveyEmailID that need to change status
   * @param String $ParFieldName description
   * @param INT $Status - the status, refer to /includes/ies/iesConfig.inc.php
   * 				$ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["_FIELD_NAME_"]]["_STATUS_"]
   */
  public function updateEmailListField($ParSurveyEmailID, $ParEmailArray, $ParFieldName, $ParStatus) {
  	global $ies_cfg;
  	$emailList = $this->getSurveyEmailList($ParSurveyEmailID);
  	if (is_array($emailList)) {
  		foreach($emailList as $email=>&$element) {
  			if (in_array($email,$ParEmailArray)) {
  				$element[$ParFieldName] = $ParStatus;
  			}
  		}
  	}
  	$result = $this->updateSurveyEmailList($ParSurveyEmailID,$emailList);
  	return $result;
  }


  /**
   * Get the survey details
   * @param INT $ParSurveyID - The SurveyID
   * @param INT $orderByField - Order by what field
   * @param INT $ordering -     1 -> DESC or 2 -> ASC
   * @return Array - the information of a survey
   */
  public function getSurveyDetails($ParSurveyID,$orderByField = -1,$ordering = 0) {
  	global $intranet_db, $ies_cfg;

  	$returnArray = array();

	//PLEASE APPEND ANY NEW ITEM TO THE END OF THE ARRAY.
	//DON'T UPDATE THIS ARRAY ORDER UNLESS MAKE SURE THE CHANGE
	$orderFieldArray = array('SurveyID','Title','DateInput','DateModified');

	if(!is_array($ParSurveyID )){
		$ParSurveyID = array($ParSurveyID);
	}

	//remove any empty element in $ParSurveyID
	$ParSurveyID = array_filter($ParSurveyID);

	$surveyIDstr = implode(',',$ParSurveyID);

	if(trim($surveyIDstr) == ''){
		return $returnArray;
	}else{
		$sqlOrderField = '';
		$sqlOrder  = '';
		$sqlOrderBYStr = '';


		if($orderByField > -1){
			$sqlOrderField = $orderFieldArray[$orderByField];
		}

		if($ordering > 0){
			$sqlOrder = ($ordering == 1)? ' DESC ': ' ASC ' ;
		}

		if($sqlOrderField != '' && $sqlOrder != ''){
			$sqlOrderBYStr  = ' order by '.$sqlOrderField.' '.$sqlOrder;
		}

		$sql = " SELECT Title, Description, Question, DateStart, DateEnd, SurveyID, SurveyType, ";
		$sql .= " CASE SurveyType ";
		foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $surveyTypeArr)
		{
		  $_surveyTypeCode = $surveyTypeArr[0];
		  $_surveyTypeName = $surveyTypeArr[1];

		  $sql .= " WHEN {$_surveyTypeCode} THEN '{$_surveyTypeName}' ";
		}
		$sql .= " END AS SurveyTypeName, ";
		$sql .= " InputBy, DATE_format(DateModified, '%Y-%m-%d') as `DateModified` , DATE_format(DateInput, '%Y-%m-%d') as `DateInput`, ";
		$sql .= " Remark1,Remark2,Remark3,Remark4 ";
		$sql .= " FROM {$intranet_db}.IES_SURVEY ";
		$sql .= " WHERE SurveyID in ({$surveyIDstr}) ";
		$sql .= " {$sqlOrderBYStr}";
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
  }

  /**
   * Get the survey ANSWER details
   * @param INT $ParSurveyID - The SurveyID
   * @return Array - the information of a survey ANSWER
   */
  public function getSurveyAnsDetails($ParSurveyID, $ParRespondent, $ParRespondentNature) {
  	global $intranet_db, $ies_cfg;
  	if($ParRespondentNature != $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
		$sql = "SELECT Respondent, Answer, date_format(DateInput, '%e-%c-%Y') as DateInput, DateModified, Type, Status, RespondentNature ";
		$sql .= "FROM {$intranet_db}.IES_SURVEY_ANSWER ";
		$sql .= "WHERE SurveyID = '{$ParSurveyID}' AND Respondent = '{$ParRespondent}'";
  	}
	else if($ParRespondentNature == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
  		$sql = "SELECT isa.Respondent, isa.Answer, date_format(isa.DateInput, '%e-%c-%Y') as DateInput, isa.DateModified, isa.Type, isa.Status, isa.RespondentNature, ".getNameFieldWithClassNumberByLang ("iu.")." AS StudentName ";
		$sql .= "FROM {$intranet_db}.IES_SURVEY_ANSWER isa ";
		$sql .= "LEFT JOIN {$intranet_db}.INTRANET_USER iu ON isa.UserID = iu.UserID ";
		$sql .= "WHERE isa.SurveyID = '{$ParSurveyID}' AND isa.UserID = '{$ParRespondent}'";
  	}

	$returnArray = current($this->returnArray($sql));
	return $returnArray;
  }

  public function getSurveyAnsDetailsByAnswerID($ParAnswerID) {
  	global $intranet_db;
	$sql = "SELECT Respondent, Answer, DateInput, DateModified, Type, RespondentNature ";
	$sql .= "FROM {$intranet_db}.IES_SURVEY_ANSWER ";
	$sql .= "WHERE AnswerID = '{$ParAnswerID}'";
	$returnArray = current($this->returnArray($sql));
	return $returnArray;
  }


  /**
   * Split Questionnaire Answers into format array[0] - array("Question1",Answer);
   * @param String $ParQuestionString - Question in format #QUE#[TYPE],[OPTIONS_NUM]||[TITLE]||#OPT#[OPTION_TITLE]#OPT#[OPTION_TITLE]
   * @param String $ParAnswerString - Answer in format #ANS#[ANSWER_A,ANSWER_B]
   * @return Array - splited Answers
   */
  static public function splitQuestionnaireAnswers($ParQuestionString, $ParAnswerString) {
  		global $ies_cfg;
  		$QuestionArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question"],$ParQuestionString);
  		$AnswerArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer"],$ParAnswerString);

  		$sizeOfQuestion = count($QuestionArray);
  		$sizeOfAnswer = count($AnswerArray);
  		if (is_array($QuestionArray) && is_array($AnswerArray) && $sizeOfQuestion==$sizeOfAnswer) {
  			$resultArray = array();
  			for($i=1;$i<$sizeOfQuestion;$i++) {
  				$element = $QuestionArray[$i];
  				$questionElements = self::extractQuestionElement($element);
  				$resultArray[] = array("QUESTION"=>$questionElements,"ANSWER"=>$AnswerArray[$i]);
  			}
  			$result = $resultArray;
  		} else {
  			$result = false;
  		}
  		return $result;
  }

  /**
   * Break the questions element
   * @param Stinr $ParAQuestion - a question string, in format [TYPE],[OPTIONS_NUM]||[TITLE]||#OPT#[OPTION_TITLE]#OPT#[OPTION_TITLE]
   * @return Array array("TITLE"=>$questionTitle,"OPTIONS"=>$questionOptions), in which $questionOptions is an array of options
   */
  static public function extractQuestionElement($ParAQuestion) {
		global $ies_cfg;

		$ParAQuestion_ary = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question_Title"],$ParAQuestion);
		list($type_OptionNum, $questionTitle, $Option) = $ParAQuestion_ary;

		$type_OptionNum_ary = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$type_OptionNum);
		$type = $type_OptionNum_ary[0];

		if($type==$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]){
			$questionNum = $type_OptionNum_ary[1];
			$optionNum	 = $type_OptionNum_ary[2];
		}
		elseif($type==$ies_cfg["Questionnaire"]["QuestionType"]["MC"] || $type==$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]){
			$optionNum	 = $type_OptionNum_ary[1];
		}
		else{
			$optionNum	 = 0;
		}

		$tempOptionsArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question_Option"],$Option);
		array_shift($tempOptionsArray);

		if(is_array($tempOptionsArray)&&($type==$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])){
			$sizeOfOptions = count($tempOptionsArray);
			for($i=0;$i<$sizeOfOptions;$i++) {
				if ($i < $questionNum) {
					$questionQuestions[] = $tempOptionsArray[$i];
				} else {
					$questionOptions[] = $tempOptionsArray[$i];
				}
			}
		}
		else {
			$questionOptions=$tempOptionsArray;
		}

		/*
		$questionTitleDelimiterLength = strlen($ies_cfg["Questionnaire"]["Delimiter"]["Question_Title"]);
		$startPos = strpos($ParAQuestion,$ies_cfg["Questionnaire"]["Delimiter"]["Question_Title"])+$questionTitleDelimiterLength;
		$length = strrpos($ParAQuestion,$ies_cfg["Questionnaire"]["Delimiter"]["Question_Title"])-$questionTitleDelimiterLength-strpos($ParAQuestion,$ies_cfg["Questionnaire"]["Delimiter"]["Question_Title"]);
		$questionTitle = substr($ParAQuestion,$startPos,$length);

		$tempOptionsArray = explode($ies_cfg["Questionnaire"]["Delimiter"]["Question_Option"],$ParAQuestion);
		$type_OptionNum_Title = array_shift($tempOptionsArray);
		$type = substr($type_OptionNum_Title,0,1);
		if ($type==$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]) {
			$OptionNumArr = explode("||", $type_OptionNum_Title);
			$commaPosArr = explode(",", $OptionNumArr[0]);
			$questionNum = isset($commaPosArr[1]) ? $commaPosArr[1] : 0;
			$optionNum = isset($commaPosArr[2]) ? $commaPosArr[2] : 0;

			$commaPos = strpos($type_OptionNum_Title,",");
			if ($commaPos) {
				$questionNum = substr($type_OptionNum_Title,$commaPos+1,1);
			} else {
				$questionNum = 0;
			}
			$commaPos2 = strpos($type_OptionNum_Title,",",2);
			if ($commaPos2) {
				$optionNum = substr($type_OptionNum_Title,$commaPos2+1,1);
			} else {
				$optionNum = 0;
			}

		} else {
			$commaPos = strpos($type_OptionNum_Title,",");
			if ($commaPos) {
				$optionNum = substr($type_OptionNum_Title,$commaPos+1,1);
			} else {
				$optionNum = 0;
			}
		}

		if (is_array($tempOptionsArray)&&($type==$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"])) {
			$sizeOfOptions = count($tempOptionsArray);
			for($i=0;$i<$sizeOfOptions;$i++) {
				if ($i < $questionNum) {
					$questionQuestions[] = $tempOptionsArray[$i];
				} else {
					$questionOptions[] = $tempOptionsArray[$i];
				}
			}
		} else {
			$questionOptions=$tempOptionsArray;
		}*/

		return array("TITLE"=>$questionTitle,"TYPE"=>$type,"QUESTION_NUM"=>$questionNum,"OPTION_NUM"=>$optionNum,"QUESTION_QUESTIONS"=>$questionQuestions,"OPTIONS"=>$questionOptions);
  }


  public function getValidSurveyCountAndLastModifiedDate($ParSurveyID) {
  	global $intranet_db,$ies_cfg;
  	$sql = "SELECT COUNT(*) AS COUNT, MAX(DATEMODIFIED) AS DATE FROM {$intranet_db}.IES_SURVEY_ANSWER WHERE SURVEYID = '$ParSurveyID' AND STATUS = ".$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Valid'];
  	$returnArray = current($this->returnArray($sql));
  	return $returnArray;
  }

	public function getTastAllSurveyDetail($ParstageID, $ParUserID, $TaskID){

		global $ies_cfg;
		$ReturnArray = array();
		$suvery_detail = $this->getStudentStageSurvey($ParstageID, $ParUserID);

		$SurveyID = $suvery_detail[$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]];
		$InterviewID =  $suvery_detail[$ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]];
		$ObservationID =  $suvery_detail[$ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]];



		if(is_array($SurveyID) && count($SurveyID) > 0){
//			$ReturnArray["Survey"]= $this->getSurveyDetails($SurveyID);
			$ReturnArray[$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][2]]= $this->getSurveyDetails($SurveyID);
		}

		if(is_array($InterviewID) && count($InterviewID) > 0){
//			$ReturnArray["Interview"] = $this->getSurveyDetails($InterviewID);
			$ReturnArray[$ies_cfg["Questionnaire"]["SurveyType"]["Interview"][2]]= $this->getSurveyDetails($InterviewID);
		}

		if(is_array($ObservationID) && count($ObservationID) > 0){
//			$ReturnArray["Observation"] = $this->getSurveyDetails($ObservationID);
			$ReturnArray[$ies_cfg["Questionnaire"]["SurveyType"]["Observe"][2]]= $this->getSurveyDetails($ObservationID);

		}

		$ReturnArray["TaskID"] = $TaskID;
		$ReturnArray["UserID"] = $ParUserID;
		return $ReturnArray;

	}
	public function Get_WARNING_MSG($ParMsg) {

		global $i_con_msg_add, $i_con_msg_update, $i_con_msg_delete, $i_con_msg_email, $i_con_msg_duplicate_item;
		global $i_con_msg_password1, $i_con_msg_password2, $i_con_msg_password3;
		global $i_con_msg_email1, $i_con_msg_email2, $i_con_msg_archive;
		global $i_con_msg_import_success, $i_con_msg_date_wrong, $i_con_msg_admin_modified;
		global $i_con_msg_suspend, $i_con_msg_cannot_edit_poll, $i_con_msg_photo_delete;
		global $i_con_msg_import_failed, $i_con_msg_import_failed2, $i_con_msg_import_invalid_login, $i_con_msg_user_add_failed;
		global $i_con_msg_date_startend_wrong_alert, $i_con_msg_date_startend_wrong;
		global $i_con_msg_add_form_failed, $i_con_msg_delete_part, $i_con_msg_enrollment_next;
		global $i_con_msg_enrollment_confirm, $i_con_msg_enrollment_lottery_completed, $i_con_msg_subject_leader_updated, $i_con_msg_quota_exceeded;
		global $i_con_msg_add_failed, $i_con_msg_update_failed, $i_con_msg_delete_failed, $i_con_msg_sports_house_group_existed;
		global $i_con_msg_notify, $i_con_msg_notify_failed, $i_con_msg_hide_notify, $i_con_msg_hide_notify_failed, $i_con_msg_report_generate, $i_con_msg_report_generate_failed, $i_con_msg_data_transit, $i_con_msg_data_transit_failed;
		global $PATH_WRT_ROOT;
		global $i_con_msg_voted, $i_con_msg_upload, $i_con_msg_upload_failed, $i_con_msg_wrong_csv_header;
		global $i_con_msg_delete_following_failed, $i_con_msg_copy, $i_con_msg_copy_failed_dup_name, $i_con_msg_copy_failed, $ec_iPortfolio, $i_con_msg_activate;
		global $i_con_msg_add_overlapped, $i_con_msg_update_overlapped, $i_con_msg_add_past, $i_con_msg_update_past, $i_con_msg_add_past_overlapped, $i_con_msg_update_incompatible_form, $i_con_msg_update_incompatible_vacancy, $i_con_msg_session_not_available, $i_con_msg_session_past, $i_con_msg_session_full, $i_con_msg_session_assigned_before, $i_con_msg_update_inapplicable_form, $i_con_msg_update_already_in_session, $i_con_msg_update_not_enough_session, $i_con_msg_cancel, $i_con_msg_cancel_failed, $i_con_msg_cancel_past;
		global $i_con_msg_approve, $i_con_msg_reject, $i_con_msg_release, $i_con_msg_unrelease, $i_con_msg_waive, $i_con_msg_unwaive, $i_con_msg_no_delete_acc_record, $i_con_msg_no_student_select, $i_con_msg_duplicate_students, $i_con_msg_not_all_approve, $i_con_msg_not_all_reject, $i_con_msg_not_all_release, $i_con_msg_no_record_release, $i_con_msg_not_all_approve, $i_con_msg_no_record_approve, $i_con_msg_not_all_unrelease, $i_con_msg_no_record_unrelease, $i_con_msg_no_record_reject;
		global $i_con_msg_redeem_success, $i_con_msg_redeem_failed, $i_con_msg_cancel_redeem_success, $i_con_msg_cancel_redeem_failed,$i_con_msg_import_header_failed,$i_con_msg_grade_generated,$i_con_msg_wrong_header, $i_con_msg_sports_ranking_generated;
		global $i_con_msg_import_no_record, $i_con_msg_report_archive, $i_con_msg_report_archive_failed;
		global $i_con_msg_survey_add, $i_con_msg_survey_update, $i_con_msg_survey_delete, $i_con_msg_homework_clear;
		global $i_con_msg_New_Record_Delete;
		global $i_con_msg_promotion_generate, $i_con_msg_promotion_generate_failed, $i_con_msg_GroupLogo_PhotoWarning;
		global $Lang;


		$String_pop = ${"i_con_msg_".$ParMsg};
		$ReturnStr = "<div class=\"alert_message\"><span class=\"fail\">{$String_pop}</span></div> ";

		return $ReturnStr;
	}

  public function get_task_type($ParTaskID){
  	global $intranet_db;
  	$sql = "SELECT QuestionType FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = '$ParTaskID'";
  	$returnArray = current($this->returnArray($sql));
  	return $returnArray;
  }

  public function getMaxStepNoSaveToTask($ParTaskID) {
  	global $intranet_db;
  	$sql = "SELECT MAX(STEPNO) FROM {$intranet_db}.IES_STEP WHERE TASKID = '$ParTaskID' AND SAVETOTASK = 1";
  	$returnStepNo = current($this->returnVector($sql));
  	return $returnStepNo;
  }

  public function getTipsClass($ParArray){
  	return (sizeof($ParArray) == 3)? "three_example" : "two_row";
  }

  public function getTipsAnswerDisplay($ParArray){
    if(is_array($ParArray)){
			$html_answer = "";
			foreach($ParArray as $key => $value){
				$html_answer .= '<div class="narrow_sample">';
				$html_answer .= "<a href=\"javascript:void(0);\" id=\"{$key}\" class='sw_click'><span style='font-size:15px;'>".htmlspecialchars($value[0])."</span></a>";
				$html_answer .= '</div>';
			}
			return $html_answer;
		}
  }

  public function getTipsAnswerDisplayWithArrow($ParArray, $ParArrowPath){
    if(is_array($ParArray)){
			$html_answer = "";
			$arrow = "";
			foreach($ParArray as $key => $value){
				$html_answer .= $arrow;
				$html_answer .= '<div class="narrow_sample">';
				$html_answer .= "<a href=\"javascript:void(0);\" id=\"{$key}\" class='sw_click'><span style='font-size:15px;'>".htmlspecialchars($value[0])."</span></a>";
				$html_answer .= '</div>';
				$arrow = "<img src=\"{$ParArrowPath}\" style=\"float:left\"/>";
			}
			return $html_answer;
		}
  }

  public function getTaskStudent($taskID){
	  global $intranet_db;

	  $sql = "SELECT DISTINCT iu.UserID ";
	  $sql .= "FROM {$intranet_db}.IES_SCHEME_STUDENT schemestudent ";
	  $sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ON schemestudent.SchemeID = stage.SchemeID ";
	  $sql .= "INNER JOIN {$intranet_db}.IES_TASK task ON stage.StageID = task.StageID ";
	  $sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON schemestudent.UserID = iu.UserID ";
	  $sql .= "WHERE task.TaskID = '{$taskID}' ";
	  $sql .= "ORDER BY iu.ClassName, iu.ClassNumber";
	  $taskStudentArr = $this->returnVector($sql);

	  return $taskStudentArr;
	}

	public function stringfilter($s){
		global $ies_cfg;
		$order = array($ies_cfg["html_open"], $ies_cfg["html_close"]);
		$replace =  array("<", ">");
		return str_replace($order, $replace, $s);
	}

	/**
	* OPEN / GET XML CONTENT
	* @owner : Fai (20101013)
	* @param : STRING $file File name for the XML with full path e.g /home/web/eclass40/intranetIP25/home/eLearning/ies/admin/settings/scheme.default.xml
	* @return : $XML OBJECT
	*
	*/
	public function openXMLContent($file)
	{
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');
		$xml = null;

		if(file_exists($file)){
			$xmlstr = get_file_content("scheme.default.xml");
			$xml = new SimpleXMLElement($xmlstr);
		}
		return $xml;

	}

   /**
	* CLOSE XML ZEND MODE
	* @owner : Fai (20101013)
	* @param : $mode a variable init by "ini_get('zend.ze1_compatibility_mode')"
	* @return : --
	*
	*/
	public function closeXMLContent($mode)
	{
		ini_set('zend.ze1_compatibility_mode', $mode);
	}

	static public function getThisSchemeLang()
	{
		global $ies_cfg;

		$returnLang = "b5";
		$_sessionLang = $_SESSION["IES_CURRENT_LANG"];
		if(in_array($_sessionLang,$ies_cfg["IESSchemeSupportLang"])){  //if the scheme session lang is supported in IES scheme lang "$ies_cfg["IESSchemeSupportLang"]"
				$returnLang = $_SESSION["IES_CURRENT_LANG"];
		}


		return $returnLang;
	}
	static public function setSchemeLang($lang)
	{
		global $ies_cfg,$_IES_DEFAULT_LANG;

		$returnLang = "b5";
		if(in_array($lang,$ies_cfg["IESSchemeSupportLang"])){  //if the scheme session lang is supported in IES scheme lang "$ies_cfg["IESSchemeSupportLang"]"
				$returnLang = $_SESSION["IES_CURRENT_LANG"] = $lang;
		}


		return $returnLang;
	}
	static public function getPurchaseLang() {
		global $plugin;
		$defaultLangToUse = "b5";
		if (!is_array($plugin['IES_Lang_Purchased']) || !isset($plugin['IES_Lang_Purchased'])) {
			$purchasedLang = array($defaultLangToUse);
		} else {
			$purchasedLang = $plugin['IES_Lang_Purchased'];
		}
		return $purchasedLang;
	}

	/**
	 * @author maxwong
	 * @param INT $ParStageID - StageID
	 * @return INT - file count in this stage
	 */
	public function getFileCountByStageID($ParStageID) {
		global $intranet_db,$intranet_root;
		$sql = "SELECT COUNT(*)
FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ISHB
INNER JOIN {$intranet_db}.IES_STAGE_HANDIN_FILE ISHF
	ON ISHB.BATCHID = ISHF.BATCHID
WHERE ISHB.STAGEID = '".$ParStageID."'";
		return current($this->returnVector($sql));
	}

	public function getFileDetailsByStageID($ParStageID) {
		global $intranet_db,$intranet_root;
		$parentsDetails = current($this->getParentsIds("stage",$ParStageID));
		$sql = "SELECT
IF (YC.CLASSTITLEEN IS NULL, YC.CLASSTITLEB5, YC.CLASSTITLEEN) AS CLASSTITLE,
IF (IU.ENGLISHNAME IS NULL, IU.USERLOGIN, IU.ENGLISHNAME) AS STUDENTNAME,
YCU.CLASSNUMBER,
ISHF.FILEID,ISHF.FILENAME,ISHF.FILEHASHNAME,CONCAT('$intranet_root', FolderPath, FileHashName) as FILEABSPATH
FROM {$intranet_db}.IES_SCHEME_STUDENT ISS
INNER JOIN {$intranet_db}.INTRANET_USER IU
	ON IU.USERID = ISS.USERID
	AND ISS.SCHEMEID = '".$parentsDetails["SCHEMEID"]."'
INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
	ON YCU.USERID = IU.USERID
INNER JOIN {$intranet_db}.YEAR_CLASS YC
	ON YC.YEARCLASSID = YCU.YEARCLASSID AND YC.ACADEMICYEARID = ".Get_Current_Academic_Year_ID()."
LEFT JOIN {$intranet_db}.IES_STAGE_HANDIN_BATCH ISHB
	ON ISHB.USERID = ISS.USERID
LEFT JOIN {$intranet_db}.IES_STAGE_HANDIN_FILE ISHF
	ON ISHB.BATCHID = ISHF.BATCHID
WHERE ISHB.STAGEID = '".$ParStageID."'
ORDER BY CLASSTITLE,STUDENTNAME,CLASSNUMBER";

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	/**
	 * @param INT $ParStageID get doc section details
	 * @return Array - Detail of Doc Section
	 */
	public function getDocSectionDetails($ParStageID) {
		global $intranet_db;
		$sql = "SELECT ISDS.TITLE AS SECT_TITLE, ISDS.SEQUENCE AS SECT_SEQ, ISDST.SEQUENCE AS SECT_TASK_SEQ,
IT.TITLE AS TASK_TITLE, ITH.ANSWER AS TASK_ANS
FROM {$intranet_db}.IES_STAGE_DOC_SECTION ISDS
	LEFT JOIN {$intranet_db}.IES_STAGE_DOC_SECTION_TASK ISDST
		ON ISDST.SECTIONID = ISDS.SECTIONID
	LEFT JOIN {$intranet_db}.IES_TASK IT
		ON ISDST.TASKID = IT.TASKID
	LEFT JOIN {$intranet_db}.IES_TASK_HANDIN ITH
		ON ITH.TASKID = IT.TASKID
WHERE ISDS.STAGEID = '{$ParStageID}'
ORDER BY SECT_SEQ,SECT_TASK_SEQ";
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function getDocSectionDetailsRefined($ParStageID) {
		$detailsArray = $this->getDocSectionDetails($ParStageID);
		$returnArray = array();
		if (is_array($detailsArray)) {
			foreach($detailsArray as $key => $element) {
				$returnArray[$element["SECT_SEQ"]]["SECT_TITLE"] = $element["SECT_TITLE"];
				$returnArray[$element["SECT_SEQ"]]["TASK_DETAIL"][$element["SECT_TASK_SEQ"]]
								= array("TASK_TITLE"=>$element["TASK_TITLE"],
										"TASK_ANS"=>$element["TASK_ANS"]);
			}
		}
		return $returnArray;
	}

	public function addDocSectionFromDefault($parSchemeID, $selectedLang, $parUserID, $parStageSeq="", $xmlSecDoc="scheme_doc_section.default.xml") {
		global $intranet_db;

	  $mode = ini_get('zend.ze1_compatibility_mode');
	  ini_set('zend.ze1_compatibility_mode', '0');

	  $xml_docsec_str = get_file_content($xmlSecDoc);
	  $xml_docsec = new SimpleXMLElement($xml_docsec_str);

	  for($i=0; $i<count($xml_docsec->stage); $i++)
	  {
  		$t_stage_seq = (int) $xml_docsec->stage[$i][sequence];
  		$t_sections = $xml_docsec->stage[$i]->sections;

  		// Skip if stage is specified and is not specified stage
  		if($parStageSeq != "" && $parStageSeq != $t_stage_seq) continue;

  		$sql = "SELECT StageID FROM {$intranet_db}.IES_STAGE WHERE SchemeID = '{$parSchemeID}' AND Sequence = '{$t_stage_seq}'";
  		$t_stageID = current($this->returnVector($sql));

  		for($j=0; $j<count($t_sections->section); $j++)
  		{
  			$t_section_title = (string) $t_sections->section[$j]->title->$selectedLang;
  			$t_section_tasks = $t_sections->section[$j]->tasks;

  			$sql = "INSERT INTO {$intranet_db}.IES_STAGE_DOC_SECTION (StageID, Sequence, Title, DateInput, InputBy, ModifyBy) ";
  			$sql .= "VALUES ('{$t_stageID}', ".($j+1).", '{$t_section_title}', NOW(), '{$parUserID}', '{$parUserID}')";
  			$res[] = $this->db_db_query($sql);

  			$t_section_id = $this->db_insert_id();

				for($k=0, $k_max=count($t_section_tasks->task); $k<$k_max; $k++)
  		  {
    			$t_task_code = (string) $t_section_tasks->task[$k]->code;

    			$sql = "INSERT INTO {$intranet_db}.IES_STAGE_DOC_SECTION_TASK ";
					$sql .= "(SectionID, TaskID, Sequence, DateInput, InputBy, ModifyBy) ";
					$sql .= "SELECT {$t_section_id}, TaskID, ".($k+1).", NOW(), {$parUserID}, {$parUserID} ";
					$sql .= "FROM {$intranet_db}.IES_TASK ";
					$sql .= "WHERE StageID = '{$t_stageID}' AND Code = '{$t_task_code}'";
    			$res[] = $this->db_db_query($sql);
  		  }
  		}
  	}
	  ini_set('zend.ze1_compatibility_mode', $mode);

	  return !in_array(false,$res);
	}

	/**
	 * @author maxwong
	 * @param INT $ParQuestionID - FAQ QuestionID
	 * @param String $ParFolderPath - folder path
	 * @param String $ParFileName - File Name
	 * @param INT $ParInputBy - Creator UserID
	 * @return INT/false update result, - success fileid, else false
	 */
	public function addFAQAttachment($ParQuestionID,$ParFolderPath,$ParFileName,$ParFileHashName,$ParInputBy) {
		global $intranet_db;
		$sql = "INSERT INTO {$intranet_db}.IES_FAQ_FILE
SET
	QUESTIONID = '$ParQuestionID',
	FOLDERPATH = '$ParFolderPath',
	FILENAME = '$ParFileName',
	FILEHASHNAME = '$ParFileHashName',
	DATEINPUT = NOW(),
	INPUTBY = '$ParInputBy'";

		$result = $this->db_db_query($sql);
		if ($result) {
			$result = mysql_insert_id();
		}
		return $result;
	}

	/**
	 * @author maxwong
	 * @param INT $ParQuestionID - FAQ QuestionID
	 */
	public function getFAQAttachmentDetails($ParQuestionID) {
		global $intranet_db;
		$sql = "SELECT FILEID,FILENAME,FOLDERPATH,FILEHASHNAME,QUESTIONID,DATEINPUT,INPUTBY,DATEMODIFIED,MODIFYBY
FROM {$intranet_db}.IES_FAQ_FILE WHERE QUESTIONID = '$ParQuestionID'";

		return $this->returnArray($sql);
	}

	/**
	 * @author maxwong
	 * expect only on record
	 * @param INT $ParFileHashName - File Hashed Name
	 */
	public function getFAQAttachmentDetails_byFileHashName($ParFileHashName) {
		global $intranet_db;
		$sql = "SELECT FILEID,FILENAME,FOLDERPATH,FILEHASHNAME,QUESTIONID,DATEINPUT,INPUTBY,DATEMODIFIED,MODIFYBY
FROM {$intranet_db}.IES_FAQ_FILE WHERE FILEHASHNAME = '$ParFileHashName'";

		return $this->returnArray($sql);
	}


	public function removeFAQAttachmentRecord($ParFileHashName) {
		global $intranet_db;
		$sql = "DELETE
FROM {$intranet_db}.IES_FAQ_FILE WHERE FILEHASHNAME = '$ParFileHashName'";
		return $this->db_db_query($sql);
	}

	static public function getSchemeLangRadio($ParFromPage="new",$ParLanguage="") {
		global $Lang;
		//HANDLE THE SCHEME XML LANG
		$schemeConfigXML = libies::openXMLContent("scheme.default.xml");
		$schemeSupportLang = $schemeConfigXML[lang];
		$h_schemeTitle = $schemeConfigXML[title];

		//split the lang to a array
		$supportLangAry = explode("/",$schemeSupportLang);

		$h_RadioLang = "";
		$checked = "";

		//	Handle the default lang purchased
		$purchasedLang = self::getPurchaseLang();

		// Construct the html for selection of scheme lang
		for($i = 0,$i_max = sizeof($supportLangAry); $i  < $i_max;$i++)
		{
			$_tmpLang = $supportLangAry[$i];
			$_tmpID = "iesLang_".$_tmpLang;
			$_tmpLangCaption = $Lang['IES']['SchemeLang'][$_tmpLang];

			switch(strtoupper($ParFromPage)) {
				default:
				case "NEW":
					$checked = ($i == 0 )?" checked ":""; // set default checked the first value
					break;
				case "EDIT":
					if ($_tmpLang==$ParLanguage){
						$checked = " checked ";
					} else {
						$checked = "";
					}
					break;
			}
			if (in_array($_tmpLang,$purchasedLang)) {
				// Enabled
				$inputElementName = "selectedLang";
				$inputDisabled = "";
				$titleDisplay = "";
			} else {
				// Disabled
				$inputElementName = "";
				$inputDisabled = "disabled=\"disabled\"";
				$checked = "";
				$titleDisplay = "title='{$Lang['IES']['WantToUseThisLangContactUs']}'";
			}

			$h_RadioLang .= "<input type=\"radio\"  name=\"$inputElementName\" value=\"{$_tmpLang}\" id=\"{$_tmpID}\" {$checked} $inputDisabled ><label for=\"{$_tmpID}\" $titleDisplay>{$_tmpLangCaption}</label>&nbsp;\n";

		}
		return $h_RadioLang;
	}

	/**
	 * @author maxwong
	 * @param INT $ParCategoryID - CategoryID
	 * get comment category details only categoryid and title
	 * @return db->returnArray - comment category details
	 */
	public function getCommentCategoryDetails_Lite($ParCategoryID="",$SchemeType='') {
		global $intranet_db;
		$cond = " WHERE 1 ";
		if (!empty($ParCategoryID)) {
			$cond .= " AND ICC.CATEGORYID = '{$ParCategoryID}' ";
		}
		if(!empty($SchemeType)) {
			$cond .= " AND ICC.SchemeType = '$SchemeType'";
		}
		$sql = "SELECT ICC.CATEGORYID,ICC.TITLE,ICC.TITLEEN,COUNT(*) AS NUM_OF_COMMENT
		FROM {$intranet_db}.IES_COMMENT_CATEGORY ICC
		LEFT JOIN {$intranet_db}.IES_COMMENT IC
		ON ICC.CATEGORYID = IC.CATEGORYID
		$cond
		GROUP BY ICC.CATEGORYID";

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	/**
	 * @author maxwong
	 * get comment category details
	 * @return db->returnArray - comment category details
	 */
	public function getCommentCategoryDetails($SchemeType='') {
		global $intranet_db;
		$cond = " WHERE 1 ";
		if(!empty($SchemeType)) {
			$cond .= " AND ICC.SchemeType = '$SchemeType' ";
		}
		$sql = "SELECT ICC.CATEGORYID,ICC.TITLE,ICC.TITLEEN,ICC.DATEINPUT,ICC.INPUTBY,ICC.DATEMODIFIED,ICC.MODIFYBY,IF (IC.COMMENTID IS NULL,0,COUNT(*)) AS NUM_OF_COMMENT
		FROM {$intranet_db}.IES_COMMENT_CATEGORY ICC
		LEFT JOIN {$intranet_db}.IES_COMMENT IC
		ON ICC.CATEGORYID = IC.CATEGORYID
		$cond
		GROUP BY ICC.CATEGORYID";
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	/**
	 * @author maxwong
	 * Add a new comment category
	 * @param Array $ParTitle - array([B5],[EN])
	 * @param INT $ParInputBy - Input By (UserID)
	 * @return INT/false - success->CategoryID, else false
	 */
	public function addCommentCategory($ParTitle,$ParInputBy="") {

		global $intranet_db;
		if (empty($ParInputBy)) {
			global $UserID;
			$inputBy = $UserID;
		}

		$sql = "INSERT INTO {$intranet_db}.IES_COMMENT_CATEGORY SET TITLE = '$ParTitle[B5]', TITLEEN='$ParTitle[EN]', DATEINPUT = NOW(), INPUTBY = '$inputBy'";
		$result = $this->db_db_query($sql);
		if ($result) {
			$result = mysql_insert_id();
		}
		return $result;
	}
	/**
	 * @author maxwong
	 * Update a comment category
	 * @param INT $ParCategoryID - CategoryID
	 * @param Array $ParTitle - array([B5],[EN])
	 * @param INT $ParModifyBy - Modify By (UserID)
	 * @return INT/false - success->CategoryID, else false
	 */
	public function editCommentCategory($ParCategoryID,$ParTitle,$ParModifyBy="") {
		global $intranet_db;
		if (empty($ParModifyBy)) {
			global $UserID;
			$modifyBy = $UserID;
		}

		$sql = "UPDATE {$intranet_db}.IES_COMMENT_CATEGORY SET TITLE = '$ParTitle[B5]',TITLEEN = '$ParTitle[EN]', MODIFYBY = '$modifyBy' WHERE CATEGORYID = '$ParCategoryID'";

		$result = $this->db_db_query($sql);
		if ($result) {
			$result = $ParCategoryID;
		}
		return $result;
	}
	/**
	 * @author maxwong
	 * Remove a comment category
	 * @param INT $ParCategoryID - CategoryID
	 * @return INT/false - success->CategoryID, else false
	 */
	public function removeCommentCategory($ParCategoryID) {
		global $intranet_db;

		$sql = "DELETE FROM {$intranet_db}.IES_COMMENT_CATEGORY WHERE CATEGORYID = '$ParCategoryID'";
		$result[] = $this->db_db_query($sql);
		$sql = "DELETE FROM {$intranet_db}.IES_COMMENT WHERE CATEGORYID = '$ParCategoryID'";
		$result[] = $this->db_db_query($sql);
		if (!in_array(false,$result)) {
			$result = $ParCategoryID;
		}
		return $result;
	}

	public function GET_SECTION_ARR($_stage_id){
		global $intranet_db;

		$sql  = "SELECT SectionID, Title ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION ";
		$sql .= "WHERE StageID = '{$_stage_id}' ";
		$sql .= "ORDER BY Sequence";

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function GET_SECTION_TASK_ARR($_sectionID){
		global $intranet_db;

		$sql  = "SELECT dst.TaskID, t.Title ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION_TASK dst ";
		$sql .= "INNER JOIN {$intranet_db}.IES_TASK t ";
		$sql .= "ON dst.TaskID = t.TaskID ";
		$sql .= "WHERE SectionID = '{$_sectionID}' ";
		$sql .= "ORDER BY dst.Sequence";

		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	/*
	function getThisSchemeLang()
	{
		return $_SESSION["IES_CURRENT_LANG"];
	}*/

	public function getStudentSurveyMappingInfoBySchemeID($ParSchemeID, $ParStudentID) {
		global $intranet_db, $ies_cfg;

		$requiredStageSeq = 2;
		$requiredTaskCode = "datarelation";
		$requiredTaskID = $this->getTaskIDByTaskInfo($ParSchemeID, $requiredStageSeq, $requiredTaskCode);
		$requiredTaskParentsInfo = current($this->getParentsIds("task",$requiredTaskID));

		$SurveyDetailArray = $this->getTastAllSurveyDetail($requiredTaskParentsInfo["STAGEID"], $_SESSION['UserID'], $requiredTaskID);

		$returnArray = array();

		foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $qTypeDetail){

			$_qTypeCodeNo = $qTypeDetail[0];
			$_qTypeName = $qTypeDetail[1];
			$_qTypeCode = $qTypeDetail[2];

			$_Details = $SurveyDetailArray[$_qTypeCode];

			for($d = 0,$d_max = sizeof($_Details);$d< $d_max;$d++){
				$tmpArray = array();
				$thisSurveyType  = $_Details[$d];
				$thisSurveyTitle = $_Details[$d]["Title"];
				$thisSurveyID 	 = $_Details[$d]['SurveyID'];

				$thisSurveyMappingInfo = $this->getStudentSurveyMappingInfo($thisSurveyID, $ParStudentID);

				$tmpArray['Title'] = $thisSurveyTitle;
				$tmpArray['Mapping'] = $thisSurveyMappingInfo;


				$returnArray[$_qTypeCode][]  = $tmpArray;
			}
		}

		return $returnArray;

	}

	public function getStudentSurveyMappingInfo($ParSurveyID, $ParStudentID) {
		global $intranet_db, $ies_cfg;

		$sql = "SELECT MappingTitle, Comment FROM {$intranet_db}.IES_SURVEY_MAPPING WHERE SurveyID = '".$ParSurveyID."' And UserID = '".$ParStudentID."' Order By Sequence";
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}

	public function handleSurveyURLDetails($parSurveyDetail,$isStage3 = false){

		global $ies_cfg, $Lang;


		$taskID = $parSurveyDetail['TaskID'];
		$student_UserID = $parSurveyDetail['UserID'];

		$URLStage3Num = ($isStage3)? 1 : 0 ;

		foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $qTypeDetail){

			$_qTypeCodeNo = $qTypeDetail[0];
			$_qTypeName = $qTypeDetail[1];
			$_qTypeCode = $qTypeDetail[2];

			$_Details = $parSurveyDetail[$_qTypeCode];
			$parSurveyDetail[$_qTypeCode] = array(); // RESET THE SURVERY DETAILS ,IT WILL BE REASSIGN LATTER


			$_qTypeCount = count($_Details);

			for($i = 0; $i< $_qTypeCount; $i++){

				// $_s --> STAND FOR SURVEY
				$_sCollectManagementURL = '';
				$_sAnalysisManagementURL = '';

				$_sSurveyID = $_Details[$i]['SurveyID'];
				$_sTitle	= $_Details[$i]['Title'];
				$_sDateModified	= $_Details[$i]['DateModified'];
				$_sDateInput		= $_Details[$i]['DateInput'];
				$_sCode_key = "SurveyID={$_sSurveyID}&task_id=$taskID";
				$_skey = base64_encode($_sCode_key);

				$_sViewURL = '/home/eLearning/ies/survey/viewQue.php?key='.base64_encode('SurveyID='.$_sSurveyID);
				$_sEditURL = '/home/eLearning/ies/survey/createForm.php?SurveyID='.base64_encode($_sSurveyID);
				$_sSurveyPreviewURL = '/home/eLearning/ies/survey/SurveyPreview.php?SurveyID='.base64_encode($_sSurveyID);
				if($_qTypeCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
					$_sMailURL  = '/home/eLearning/ies/survey/mail/compose_email.php?KeepThis=true&key='.$_skey.'&amp;TB_iframe=true&amp;height=450&amp;width=730';
				}

				if($isStage3){
					$_sCollectManagementFile = '/home/eLearning/ies/survey/questionnaire_discovery_list.php';

					if($_qTypeCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
						$_sCollectManagementFile  = '/home/eLearning/ies/survey/questionnaireGroupingList.php';
					}
				}else{
					$_sCollectManagementFile = '/home/eLearning/ies/survey/management.php';
				}


				if($_qTypeCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
					$_sCollectManagementParmeter = base64_encode("SurveyID={$_sSurveyID}&StudentID={$student_UserID}&TaskID={$taskID}&IsStage3={$URLStage3Num}&Stage3TaskID={$ParStage3TaskID}&FromPage=Collect:Survey");
				}elseif($_qTypeCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){

					$_sCollectManagementParmeter = base64_encode("SurveyID={$_sSurveyID}&FromPage=Collect:Interview&IsStage3={$URLStage3Num}");

				}elseif($_qTypeCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){

					$_sCollectManagementParmeter = base64_encode("SurveyID={$_sSurveyID}&FromPage=Collect:Observe&IsStage3={$URLStage3Num}");
				}

				$_sCollectManagementURL = $_sCollectManagementFile.'?key='.$_sCollectManagementParmeter;

				if($_qTypeCodeNo == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
					$_sAnalysisManagementFile = '/home/eLearning/ies/survey/questionnaireGroupingList.php';
					$_sAnalysisManagementParmeter = base64_encode("SurveyID=".$_sSurveyID."&StudentID=".$student_UserID."&TaskID=".$taskID."&FromPage=Analyze:Survey");
					$_sAnalysisManagementURL = $_sAnalysisManagementFile.'?key='.$_sAnalysisManagementParmeter;
				}

				$_Details[$i]['ViewURL'] = $_sViewURL;
				$_Details[$i]['EditURL'] = $_sEditURL;
				$_Details[$i]['MailURL'] = $_sMailURL;
				$_Details[$i]['CollectManagementURL'] = $_sCollectManagementURL;
				$_Details[$i]['AnalysisManagementURL'] = $_sAnalysisManagementURL;
				$_Details[$i]['SurveyPreviewURL'] = $_sSurveyPreviewURL;

				$parSurveyDetail[$_qTypeCode] = $_Details;

			}

		}

		return $parSurveyDetail;
	}

	/**

        * Generate Vector list

        * @owner : Connie (201100902)

        * @param : String $title (value of the textBox)

        * @objective : To check whether the input title exists in the IES_SCHEME.

        * @return : Vector list (1 row). If 'number'>0, it means that the title exists. Duplicated title.

        *

        */
	public function isDuplicateTitle($title){
		global $intranet_db;


		$sql = "select count(*) as number from {$intranet_db}.IES_SCHEME where title = '".$this->Get_Safe_Sql_Query($title)."' ";
		$result = $this->returnVector($sql);
		return $result;


	}
	/**

        * Get Table details for copy / clone scheme

        * @owner : Connie (20110906)

        * @param : String $tableName eg.IES_SCHEME

        * @param : String $tableID eg.SchemeID

        * @param : String $requestID eg.414

        * @return : Array(s) of table record (there may be more than one record)

        *

        */
	function getTableInfo($tableName, $tableID,$requestID){
		global $intranet_db;
		$sql =  "SELECT * FROM {$intranet_db}.{$tableName} WHERE {$tableID}= '{$requestID}'";
//debug_r($sql);
		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}

	/**

        * Insert IES_SCHEME Record

        * @owner : Connie (20110906)

        * @param : $ParSchemeArr -- It is a Scheme Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return the inserted SchemeID ($returnResult)
        *
        *

        */

	function insertScheme($ParSchemeArr){
	  global $intranet_db;

	  if ($ParSchemeArr['MaxScore'] == '') {
	  	$ParSchemeArr['MaxScore'] = 'null';
	  }

	  if ($ParSchemeArr['Version'] == '') {
	  	$ParSchemeArr['Version'] = 'null';
	  }

	  $sql = "INSERT INTO {$intranet_db}.IES_SCHEME (Title,MaxScore,DateInput,InputBy,ModifyBy,Version,Language,SchemeType) ";
      $sql .= "VALUES ('{$ParSchemeArr['Title']}',{$ParSchemeArr['MaxScore']},{$ParSchemeArr['DateInput']},'{$ParSchemeArr['InputBy']}','{$ParSchemeArr['ModifyBy']}',{$ParSchemeArr['Version']},'{$ParSchemeArr['Language']}','{$ParSchemeArr['SchemeType']}'); ";

      $ForDebug=$this->db_db_query($sql);

      if($ForDebug==false)
      {
      	return false;
      }

      $returnResult = $this->db_insert_id();
      return $returnResult;

	}




	/**

        * Insert IES_STAGE Record

        * @owner : Connie (20110906)

        * @param : $ParStageArr -- It is a Stage Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return the inserted StageID ($returnResult)
        *
        *

        */
	function insertStage($ParStageArr){
	  global $intranet_db;

	  if ($ParStageArr['Sequence'] == '') {
	  	$ParStageArr['Sequence'] = 'null';
	  }

	  if ($ParStageArr['MaxScore'] == '') {
	  	$ParStageArr['MaxScore'] = 'null';
	  }

	  if ($ParStageArr['Weight'] == '') {
	  	$ParStageArr['Weight'] = 'null';
	  }

	  $sql = "INSERT INTO {$intranet_db}.IES_STAGE (SchemeID,Title,Description,Sequence,Deadline,MaxScore,Weight,DateInput,InputBy,ModifyBy) ";
      $sql .= "VALUES ('{$ParStageArr['SchemeID']}','{$ParStageArr['Title']}', '{$ParStageArr['Description']}',{$ParStageArr['Sequence']},'{$ParStageArr['Deadline']}',{$ParStageArr['MaxScore']},{$ParStageArr['Weight']},{$ParStageArr['DateInput']},'{$ParStageArr['InputBy']}','{$ParStageArr['ModifyBy']}') ";

      $ForDebug=$this->db_db_query($sql);

      if($ForDebug==false)
      {
      	return false;
      }

      $returnResult = $this->db_insert_id();
      return $returnResult;

	}

	/**

        * Insert IES_TASK Record

        * @owner : Connie (20110906)

        * @param : $ParTaskArr -- It is a Task Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return the inserted TaskID ($returnResult)
        *
        *

        */
	function insertTask($ParTaskArr){
	  global $intranet_db;

	  if ($ParTaskArr['Sequence'] == '') {
	  	$ParTaskArr['Sequence'] = 'null';
	  }

	  if ($ParTaskArr['Approval'] == '') {
	  	$ParTaskArr['Approval'] = 'null';
	  }

	  if ($ParTaskArr['Enable'] == '') {
	  	$ParTaskArr['Enable'] = 'null';
	  }

	  if ($ParTaskArr['InstantEdit'] == '') {
	  	$ParTaskArr['InstantEdit'] = 'null';
	  }

	  $sql = "INSERT INTO {$intranet_db}.IES_TASK (StageID,Title,Code,Sequence,Approval,Enable,Description,InstantEdit,DateInput,InputBy,ModifyBy) ";
      $sql .= "VALUES ('{$ParTaskArr['StageID']}', '{$ParTaskArr['Title']}','{$ParTaskArr['Code']}',{$ParTaskArr['Sequence']},{$ParTaskArr['Approval']},{$ParTaskArr['Enable']},'{$ParTaskArr['Description']}',{$ParTaskArr['InstantEdit']},{$ParTaskArr['DateInput']},'{$ParTaskArr['InputBy']}','{$ParTaskArr['ModifyBy']}') ";


      $ForDebug=$this->db_db_query($sql);

      if($ForDebug==false)
      {
      	return false;
      }
      $returnResult = $this->db_insert_id();
      return $returnResult;

	}

	/**

        * Insert IES_STEP Record

        * @owner : Connie (20110906)

        * @param : $ParStepArr -- It is a Step Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return the inserted StepID ($returnResult)
        *
        *

        */
	function insertStep($ParStepArr){
	  global $intranet_db;

	  if ($ParStepArr['StepNo'] == '') {
	  	$ParStepArr['StepNo'] = 'null';
	  }

	  if ($ParStepArr['Status'] == '') {
	  	$ParStepArr['Status'] = 'null';
	  }

	  if ($ParStepArr['Sequence'] == '') {
	  	$ParStepArr['Sequence'] = 'null';
	  }

	  if ($ParStepArr['SaveToTask'] == '') {
	  	$ParStepArr['SaveToTask'] = 'null';
	  }

	  $sql = "INSERT INTO {$intranet_db}.IES_STEP (TaskID,Title,StepNo,Status,Sequence,SaveToTask,DateInput,InputBy,ModifyBy) ";
      $sql .= "VALUES ('{$ParStepArr['TaskID']}','{$ParStepArr['Title']}',{$ParStepArr['StepNo']},{$ParStepArr['Status']},{$ParStepArr['Sequence']},{$ParStepArr['SaveToTask']},{$ParStepArr['DateInput']},'{$ParStepArr['InputBy']}', '{$ParStepArr['ModifyBy']}')";

      $ForDebug=$this->db_db_query($sql);

      if($ForDebug==false)
      {
      	return false;
      }

      $returnResult = $this->db_insert_id();
      return $returnResult;

	}
	/**

        * Insert IES_STAGE_MARKING_CRITERIA Record

        * @owner : Connie (20110907)

        * @param : $ParMarkingArr -- It is a IES_STAGE_MARKING_CRITERIA Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return the inserted StageMarkCriteriaID ($returnResult)

        */
	/*
	 * insertStageMarkingCriteria() here is Depreciated, moved to libSba
	 */
	function insertStageMarkingCriteria($ParMarkingArr){
	  global $intranet_db;

	  if ($ParMarkingArr['task_rubric_id'] == '') {
	  	$ParMarkingArr['task_rubric_id'] = 'null';
	  }
	  if ($ParMarkingArr['MaxScore'] == '') {
	  	$ParMarkingArr['MaxScore'] = 'null';
	  }
	  if ($ParMarkingArr['Weight'] == '') {
	  	$ParMarkingArr['Weight'] = 'null';
	  }


	  $sql = "INSERT INTO {$intranet_db}.IES_STAGE_MARKING_CRITERIA (StageID,MarkCriteriaID,task_rubric_id,MaxScore,Weight,DateInput,InputBy,ModifyBy) ";
      $sql .= "VALUES ('{$ParMarkingArr['StageID']}','{$ParMarkingArr['MarkCriteriaID']}',{$ParMarkingArr['task_rubric_id']},{$ParMarkingArr['MaxScore']},{$ParMarkingArr['Weight']},{$ParMarkingArr['DateInput']},'{$ParMarkingArr['InputBy']}','{$ParMarkingArr['ModifyBy']}'); ";

      $ForDebug=$this->db_db_query($sql);


      if($ForDebug==false)
      {
      	return false;
      }

      $returnResult = $this->db_insert_id();
      return $returnResult;

	}


	/**

        * Insert IES_STAGE_DOC_SECTION Record

        * @owner : Connie (20110907)

        * @param : $ParSectionArr -- It is a IES_STAGE_DOC_SECTION Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return the inserted SectionID ($returnResult)

        */

	function insertStageDocSection($ParSectionArr){
	  global $intranet_db;

	  if ($ParSectionArr['StageID'] == '') {
	  	$ParSectionArr['StageID'] = 'null';
	  }
	  if ($ParSectionArr['Sequence'] == '') {
	  	$ParSectionArr['Sequence'] = 'null';
	  }


	  $sql = "INSERT INTO {$intranet_db}.IES_STAGE_DOC_SECTION (StageID,Sequence,Title,DateInput,InputBy,ModifyBy) ";
      $sql .= "VALUES ({$ParSectionArr['StageID']},{$ParSectionArr['Sequence']},'{$ParSectionArr['Title']}','{$ParSectionArr['DateInput']}','{$ParSectionArr['InputBy']}','{$ParSectionArr['ModifyBy']}'); ";

      $ForDebug=$this->db_db_query($sql);


      if($ForDebug==false)
      {
      	return false;
      }

      $returnResult = $this->db_insert_id();
      return $returnResult;

	}

	/**

        * Insert IES_STAGE_DOC_SECTION_TASK Record

        * @owner : Connie (20110907)

        * @param : $ParSectionTaskArr -- It is a IES_STAGE_DOC_SECTION_TASK Record Array

        * @return : If insertion fails, it returns false. Otherwise, it return true.

        */

	function insertStageDocSectionTask($ParSectionTaskArr){
	  global $intranet_db;

	  if ($ParSectionTaskArr['SectionID'] == '') {
	  	$ParSectionTaskArr['SectionID'] = 'null';
	  }
	  if ($ParSectionTaskArr['TaskID'] == '') {
	  	$ParSectionTaskArr['TaskID'] = 'null';
	  }
	  if ($ParSectionTaskArr['Sequence'] == '') {
	  	$ParSectionTaskArr['Sequence'] = 'null';
	  }

	  $sql = "INSERT INTO {$intranet_db}.IES_STAGE_DOC_SECTION_TASK (SectionID,TaskID,Sequence,DateInput,InputBy,ModifyBy) ";
      $sql .= "VALUES ('{$ParSectionTaskArr['SectionID']}',{$ParSectionTaskArr['TaskID']},{$ParSectionTaskArr['Sequence']},{$ParSectionTaskArr['DateInput']},'{$ParSectionTaskArr['InputBy']}','{$ParSectionTaskArr['ModifyBy']}'); ";

      $ForDebug=$this->db_db_query($sql);

      if($ForDebug==false)
      {
      	return false;
      }

      $returnResult = $ForDebug;

      return $returnResult;

	}

	 /** For update the Table ID (based on the oldID)

        * @owner : Connie (20110909)

        * @param : $tableName eg.IES_STAGE_HANDIN_BATCH
        * @param : $tableID eg.StageID
        * @param : $ParOldID eg. old stageid
        * @param : $ParNewID eg. new stageid
        * @return : true or false

        */
	  function UpdateTableID($tableName,$tableID,$ParOldID,$ParNewID)
	  {
	    global $intranet_db;
	  	$sql = "Update {$intranet_db}.{$tableName} set ".$tableID."=".$ParNewID." where ".$tableID."='".$ParOldID."'";
	  	$returnResult=$this->db_db_query($sql);
	//debug_r($sql);
	  	return $returnResult;
	  }
	 /**

        * For update the Table ID (based on the oldID and UserID)

        * @owner : Connie (20110909)

        * @param : $tableName eg.IES_STAGE_HANDIN_BATCH
        * @param : $tableID eg.StageID
        * @param : $ParOldID eg. old stageid
        * @param : $ParNewID eg. new stageid
        * @param : $ParUserID eg. UserID
        * @return : true or false

        **/
	  function UpdateTableID_userID($tableName,$tableID,$ParOldID,$ParNewID,$ParUserID)
	  {
	    global $intranet_db;
	  	$sql = "Update {$intranet_db}.{$tableName} set ".$tableID."=".$ParNewID." where ".$tableID."='".$ParOldID."' and UserID='".$ParUserID."'";
	  	$returnResult=$this->db_db_query($sql);

	  	return $returnResult;
	  }

	  /**

        * Get Table details for copy / clone scheme (Order by Field)

        * @owner : Connie (20110906)

        * @param : String $tableName eg.IES_SCHEME

        * @param : String $tableID eg.SchemeID

        * @param : String $requestID eg.414
        *
        * @param : String $orderBy eg.Sequence

        * @return : Array(s) of table record (there may be more than one record)

        *

        */
		function getTableInfoOrderBy($tableName, $tableID,$requestID,$orderBy){
		global $intranet_db;
		$sql =  "SELECT * FROM {$intranet_db}.{$tableName} WHERE {$tableID}= '{$requestID}' order by {$orderBy}";

		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}

		/** Get Student Details by SchemeID (default '') from IES_SCHEME_STUDENT

        * @owner : Connie (20110915)

        * @param : String $ParSchemeID eg.SchemeID
        *
        * @param : Array $skipStudentIDArr

        * @return : Array(s) of <Option> (there may be more than one record)

        *

        */
		function getStudentBySchemeID($ParSchemeID, $skipStudentIDArr=""){
			global $intranet_db;

			$NameField = getNameFieldWithClassNumberByLang('user.');

			$conds_SchemeID = " And iss.SchemeID ='".$ParSchemeID."' ";


			if ($skipStudentIDArr !== '') {
				$conds_StudentID = " And iss.UserID Not In ('".implode("','", (array)$skipStudentIDArr)."') ";
			}

			$sql = "Select
							iss.SchemeID,

							iss.UserID,
							{$NameField} as StudentName
					From
							IES_SCHEME_STUDENT as iss
							Inner Join INTRANET_USER as user On (iss.UserID = user.UserID)

					Where
							1
							$conds_SchemeID
							$conds_StudentID
					";


			$studentData = $this->returnArray($sql,2);
		//	debug_r($studentData);


			$SchemeArr = current($this-> getTableInfo('IES_SCHEME', 'SchemeID',$ParSchemeID));
			$SchemeTitle = $SchemeArr["Title"];

			for($i=0; $i<sizeof($studentData); $i++) {

				$_StudentName = $studentData[$i]['StudentName'];
				$_studentInfo = $_StudentName." , ".$SchemeTitle;

				$schemeMenu .= "<OPTION value='".$studentData[$i]['UserID']."_".$studentData[$i]['SchemeID']."'>".$_studentInfo."</OPTION>";

			}

			return $schemeMenu;
		}



}
?>
