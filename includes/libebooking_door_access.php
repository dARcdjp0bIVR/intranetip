<?php
// using: ivan
if (!defined("LIBEBOOKING_DOOR_ACCESS_DEFINED"))                  // Preprocessor directives
{
	class libebooking_door_access extends libebooking{
		
		function libebooking_door_access() {
			$this->libebooking();
		}
		
		function canLocationApplyDoorAccess($LocationId) {
			global $plugin, $PATH_WRT_ROOT, $locationConfigAry;
			
			$canApply = false;
			if ($plugin['door_access']) {
				include_once($PATH_WRT_ROOT.'includes/liblocation_cardReader.php');
				$lcardreader = new liblocation_cardReader();
				
				$cardReaderInfoAry = $lcardreader->getCardReaderInfoByLocationId($LocationId, $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['enabled']);
				$numOfCardReader = count($cardReaderInfoAry);
				
				if ($numOfCardReader > 0) {
					$canApply = true;
				}
			}
			return $canApply;
		}
		
		function updateBookingDoorAccessRecord($ParentBookingId) {
			global $PATH_WRT_ROOT, $locationConfigAry, $door_access;
			
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			include_once($PATH_WRT_ROOT.'includes/liblocation_cardReader.php');
			include_once($PATH_WRT_ROOT.'plugins/door_access_conf.php');
			
			if ($door_access['provider'] == 'aci2') {
				// update txt file content with future 30 days bookings
				return $this->saveDoorAccessRecordToApiForAci2($ParentBookingId);
			}
			else {
				// one booking record one door access record
				
				### Get all booking records related to this booking
				$allBookingInfoAry = $this->Get_Related_Booking_Record_By_BookingID($ParentBookingId, $BookingStatus='', $RecordStatusAry='');
				$allBookingIdAry = Get_Array_By_Key($allBookingInfoAry, 'BookingID');
				$numOfBooking = count($allBookingIdAry);
				
				### Get booking details data of all booking records
				$bookingDetailsInfoAry = $this->Get_Booking_Details_By_BookingID($allBookingIdAry, LIBEBOOKING_FACILITY_TYPE_ROOM);
				$locationIdAry = Get_Array_By_Key($bookingDetailsInfoAry, 'FacilityID');
				$bookingDetailsAssoAry = BuildMultiKeyAssoc($bookingDetailsInfoAry, 'BookingID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
				$numOfBookingDetails = count($bookingDetailsInfoAry);
				
				### Get door access data of all booking records
				$doorAccessRecordInfoAry = $this->getDoorAccessRecord($allBookingIdAry);
				$doorAccessRecordAssoAry = BuildMultiKeyAssoc($doorAccessRecordInfoAry, array('BookingID', 'BookingDetailsID', 'UserID'));
				
				
				### Get the booking user and responsible user info
				$requestedByUserIdAry = Get_Array_By_Key($allBookingInfoAry, 'RequestedBy');
				$responsibleUserIdAry = Get_Array_By_Key($allBookingInfoAry, 'ResponsibleUID');
				$allUserIdAry = array_values(array_unique(array_merge($requestedByUserIdAry, $responsibleUserIdAry)));
				$userObj = new libuser('', '', $allUserIdAry);
				
				### Get the card reader info of all locations
				$liblocation_cardReader = new liblocation_cardReader();
				$cardReaderInfoAry = $liblocation_cardReader->getCardReaderInfoByLocationId($locationIdAry, $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['enabled']);
				$numOfCardReader = count($cardReaderInfoAry);
				
				$successAry = array();
				for ($i=0; $i<$numOfBooking; $i++) {
					$_bookingId = $allBookingInfoAry[$i]['BookingID'];
					$_date = $allBookingInfoAry[$i]['Date'];
					$_startTime = $allBookingInfoAry[$i]['StartTime'];
					$_endTime = $allBookingInfoAry[$i]['EndTime'];
					$_requestedBy = $allBookingInfoAry[$i]['RequestedBy'];
					$_responsibleUid = $allBookingInfoAry[$i]['ResponsibleUID'];
					$_applyDoorAccess = $allBookingInfoAry[$i]['ApplyDoorAccess'];
					$_recordStatus = $allBookingInfoAry[$i]['RecordStatus'];
					$_bookingDetailsInfoAry = $bookingDetailsAssoAry[$_bookingId];
					$_numOfBookingDetails = count($_bookingDetailsInfoAry);
					
					
					if (!is_array($_bookingDetailsInfoAry) || $_numOfBookingDetails == 0 || $numOfCardReader == 0) {
						continue;
					}
					
					if ($_applyDoorAccess) {
						for ($j=0; $j<$_numOfBookingDetails; $j++) {
							$__bookingDetailsId = $_bookingDetailsInfoAry[$j]['RecordID'];
							$__bookingStatus = $_bookingDetailsInfoAry[$j]['BookingStatus'];
							
							if ($_recordStatus == 1 && $__bookingStatus == LIBEBOOKING_BOOKING_STATUS_APPROVED) {
								### Handle aprroved booking => insert / update door access records
								$userObj->LoadUserData($_requestedBy);
								$__doorAccessRecordId = $doorAccessRecordAssoAry[$_bookingId][$__bookingDetailsId][$_requestedBy]['DoorAccessRecordID'];
								$successAry['saveDoorAccessRecord'][$_bookingId][$__bookingDetailsId][$_requestedBy] = $this->saveUserDoorAccessRecord($_bookingId, $__bookingDetailsId, $__doorAccessRecordId, $_requestedBy, $userObj->CardID, $cardReaderInfoAry, $_date, $_startTime, $_endTime);
								
								if ($_responsibleUid != $_requestedBy) {
									$userObj->LoadUserData($_responsibleUid);
									$__doorAccessRecordId = $doorAccessRecordAssoAry[$_bookingId][$__bookingDetailsId][$_responsibleUid]['DoorAccessRecordID'];
									$successAry['saveDoorAccessRecord'][$_bookingId][$__bookingDetailsId][$_responsibleUid] = $this->saveUserDoorAccessRecord($_bookingId, $__bookingDetailsId, $__doorAccessRecordId, $_responsibleUid, $userObj->CardID, $cardReaderInfoAry, $_date, $_startTime, $_endTime);
								}
							}
							else {
								### Handle deleted / removed / cancelled booking => delete door access records
								$successAry['deleteDoorAccessRecordByBookingDetailsId'][$__bookingDetailsId] = $this->deleteDoorAccessRecord($_bookingId, $__bookingDetailsId);
							}
						}
					}
					else {
						### Handle not apply door access => remove all door access records of this booking
						$successAry['deleteDoorAccessRecordByBookingId'][$_bookingId] = $this->deleteDoorAccessRecord($_bookingId);
					}
				}
				return (in_multi_array(false, $successAry))? false : true;
			}
		}
		
		function getDoorAccessRecord($BookingIdAry, $BookingDetailsIdAry='', $UserIdAry='', $UserSmartCardIdAry='', $CardReaderIdAry='', $DoorAccessRecordIdAry='') {
			if ($BookingDetailsIdAry !== '') {
				$condsBookingDetailsId = " And BookingDetailsID In ('".implode("','", (array)$BookingDetailsIdAry)."') ";
			}
			if ($UserIdAry !== '') {
				$condsUserId = " And UserID In ('".implode("','", (array)$UserIdAry)."') ";
			}
			if ($UserSmartCardIdAry !== '') {
				$condsUserSmartCardId = " And CardID In ('".implode("','", (array)$UserSmartCardIdAry)."') ";
			}
			if ($CardReaderIdAry !== '') {
				$condsCardReaderId = " And CardReaderID In ('".implode("','", (array)$CardReaderIdAry)."') ";
			}
			if ($DoorAccessRecordIdAry !== '') {
				$condsDoorAccessRecordId = " And DoorAccessRecordID In ('".implode("','", (array)$DoorAccessRecordIdAry)."') ";
			}
			
			$sql = "Select
							RecordID, BookingID, BookingDetailsID, DoorAccessRecordID, CardReaderID, UserID
					From
							INTRANET_EBOOKING_DOOR_ACCESS_RECORD
					Where
							BookingID In ('".implode("','", (array)$BookingIdAry)."')
							$condsBookingDetailsId
							$condsUserId
							$condsUserSmartCardId
							$condsCardReaderId
							$condsDoorAccessRecordId
					";
			return $this->returnResultSet($sql);
		}
		
		function saveUserDoorAccessRecord($BookingId, $BookingDetailsId, $DoorAccessRecordId, $TargetUserId, $UserSmartCardId, $CardReaderInfoAry, $Date, $StartTime, $EndTime) {
			$numOfCardReader = count($CardReaderInfoAry);
			
			if ($UserSmartCardId=='' || $numOfCardReader==0) {
				return false;
			}
			
			$cardReaderCodeText = implode('_', Get_Array_By_Key($CardReaderInfoAry, 'Code'));
			
			$startDateTime = $Date.' '.$StartTime;
			$endDateTime = $Date.' '.$EndTime;
			$startTimeText = date('YmdHis', strtotime($startDateTime));
			$endTimeText = date('YmdHis', strtotime($endDateTime));
			
			
			### Add / Update door access record in the API
			$doorAccessRecordId = $this->saveDoorAccessRecordToApi($DoorAccessRecordId, $UserSmartCardId, $cardReaderCodeText, $startTimeText, $endTimeText);
			
						
			### Add / Update door access record in the intranet db for reference 
			if ($doorAccessRecordId) {
				$successAry = array();
				for ($i=0; $i<$numOfCardReader; $i++) {
					$_cardReaderId = $CardReaderInfoAry[$i]['ReaderID'];
					
					$_doorAccessRecordInfoAry = $this->getDoorAccessRecord($BookingId, $BookingDetailsId, $TargetUserId, $UserSmartCardId, $_cardReaderId, $doorAccessRecordId);
					$_numOfRecord = count($_doorAccessRecordInfoAry);
					
					if ($_numOfRecord == 0) {
						// no record yet => insert
						$sql = "Insert Into INTRANET_EBOOKING_DOOR_ACCESS_RECORD
									(BookingID,  BookingDetailsID, UserID, CardID, CardReaderID, DoorAccessRecordID, StartTime, EndTime, InputBy, DateInput, ModifiedBy, DateModified)
								Values
									('".$BookingId."', '".$BookingDetailsId."', '".$TargetUserId."', '".$UserSmartCardId."', '".$_cardReaderId."', '".$doorAccessRecordId."',
									 '".$startDateTime."', '".$endDateTime."', '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."', now())
								";
						$successAry['insertRecord'][] = $this->db_db_query($sql);
					}
					else {
						// have record already => update
						$_recordId = $_doorAccessRecordInfoAry[0]['RecordID'];
						$sql = "Update INTRANET_EBOOKING_DOOR_ACCESS_RECORD
								Set
										BookingID = '".$BookingId."',
										BookingDetailsID = '".$BookingDetailsId."',
										UserID = '".$TargetUserId."',
										CardID = '".$UserSmartCardId."',
										CardReaderID = '".$_cardReaderId."',
										DoorAccessRecordID = '".$doorAccessRecordId."',
										StartTime = '".$startDateTime."',
										EndTime = '".$endDateTime."',
										ModifiedBy = '".$_SESSION['UserID']."',
										DateModified = now()
								Where
										RecordID = '".$_recordId."'
								";
						$successAry['updateRecord'][$_recordId] = $this->db_db_query($sql);
					}
				}
				
				return (in_multi_array(false, $successAry))? false : true;
			}
			else {
				return false;
			}
		}
		
		
		function deleteDoorAccessRecord($BookingId, $BookingDetailsId='') {
			global $PATH_WRT_ROOT, $door_access;
			
			include_once($PATH_WRT_ROOT.'plugins/door_access_conf.php');
			
			if ($door_access['provider'] == 'aci2') {
				// update txt file content with future 30 days bookings
				return $this->saveDoorAccessRecordToApiForAci2($BookingId);
			}
			else {
				$doorAccessRecordInfoAry = $this->getDoorAccessRecord($BookingId, $BookingDetailsId);
				$doorAccessRecordIdAry = Get_Array_By_Key($doorAccessRecordInfoAry, 'DoorAccessRecordID');
				$numOfRecord = count($doorAccessRecordIdAry);
				
				$successAry = array();
				for ($i=0; $i<$numOfRecord; $i++) {
					$_doorAccessRecordId = $doorAccessRecordIdAry[$i];
					$successAry[$_doorAccessRecordId]['deleteApiRecord'] = $this->deleteDoorAccessRecordFromApi($_doorAccessRecordId);
					
					if ($successAry[$_doorAccessRecordId]) {
						$sql = "Delete From INTRANET_EBOOKING_DOOR_ACCESS_RECORD Where DoorAccessRecordID = '".$_doorAccessRecordId."'";
						$successAry[$_doorAccessRecordId]['deleteDbRecord'] = $this->db_db_query($sql);
					}
				}
			}
			
			return in_multi_array(false, $successAry)? false : true;
		}
		
		
		
		function saveDoorAccessRecordToApi($DoorAccessRecordId, $SmartCardId, $ReaderCode, $StartTime, $EndTime) {
			if ($DoorAccessRecordId == '') {
				$newDoorAccessRecordId = $this->newDoorAccessRecordToApi($SmartCardId, $ReaderCode, $StartTime, $EndTime);
			}
			else {
				$success = $this->editDoorAccessRecordToApi($DoorAccessRecordId, $SmartCardId, $ReaderCode, $StartTime, $EndTime);
				$newDoorAccessRecordId = ($success)? $DoorAccessRecordId : false;
			}
			
			return $newDoorAccessRecordId;
		}
		
		function newDoorAccessRecordToApi($SmartCardId, $ReaderCode, $StartTime, $EndTime) {
			global $PATH_WRT_ROOT, $door_access;
			include_once($PATH_WRT_ROOT.'includes/libdooraccess_factory.php');
			
			$errorAry = array();
			
			$objDoorAccess = libdooraccess_factory::createObject();
			if ($objDoorAccess === null) {
				$errorAry[] = 'Failed to initialize object.';
			}
			
			$doorAccessRecordId = $objDoorAccess->actionCreate($SmartCardId, $ReaderCode, $StartTime, $EndTime);
			if ($doorAccessRecordId <= 0) {
				$errorAry[] = 'Failed to create door access record.';
			}
			
			return $doorAccessRecordId;
		}
		
		function editDoorAccessRecordToApi($DoorAccessRecordId, $SmartCardId, $ReaderCode, $StartTime, $EndTime) {
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/libdooraccess_factory.php');
			
			$errorAry = array();
			
			$objDoorAccess = libdooraccess_factory::createObject();
			if ($objDoorAccess === null) {
				$errorAry[] = 'Failed to initialize object.';
			}
			
			$success = $objDoorAccess->actionUpdate($DoorAccessRecordId, $SmartCardId, $ReaderCode, $StartTime, $EndTime);
			if (!$success) {
				$errorAry[] = 'Failed to edit door access record.';
			}
			
			return $DoorAccessRecordId;
		}
		
		function deleteDoorAccessRecordFromApi($DoorAccessRecordId) {
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/libdooraccess_factory.php');
			
			$errorAry = array();
			
			$objDoorAccess = libdooraccess_factory::createObject();
			if ($objDoorAccess === null) {
				$errorAry[] = 'Failed to initialize object.';
			}
			
			$success = $objDoorAccess->actionDelete($DoorAccessRecordId);
			if (!$success) {
				$errorAry[] = 'Failed to delete door access record.';
			}
						
			return $success;
		}
		
		function saveDoorAccessRecordToApiForAci2($parentBookingId='', $startDate='') {
			global $PATH_WRT_ROOT, $door_access, $locationConfigAry;
			
			$startDateLimitTimestamp = time();
			$startDateLimitString = date('Y-m-d', $startDateLimitTimestamp);
			$endDateLimitTimestamp = strtotime("+30 days", $startDateLimitTimestamp);
			$endDateLimitString = date('Y-m-d', $endDateLimitTimestamp);
			
			$dateRangeAry = '';
			if ($startDate != '') {
				$dateRangeAry = array();
				$dateRangeAry['StartDate'] = $startDate;
				$dateRangeAry['EndDate'] = date('Y-m-d', strtotime("+30 days", $startDateLimitTimestamp));
			}
			
			$allBookingInfoAry = $this->Get_Related_Booking_Record_By_BookingID($parentBookingId, $BookingStatus='', $RecordStatusAry='', $dateRangeAry);
			$needUpdateTxt = false;
			$firstBookingDate = $allBookingInfoAry[0]['Date']; 
			$lastBookingDate = $allBookingInfoAry[count($allBookingInfoAry)-1]['Date'];
			
			if (!is_date_empty($firstBookingDate) && !is_date_empty($lastBookingDate)) {
				if ($startDateLimitString <= $firstBookingDate && $lastBookingDate <= $endDateLimitString) {
					$needUpdateTxt = true;
				}
			}
			
			if ($needUpdateTxt) {
				$doorAccessInfoDataAry = array();
				
				$sql = "SELECT
							br.BookingID,
							br.Date,
							br.StartTime,
							br.EndTime,
							br.RequestedBy,
							br.ResponsibleUID,
							br.ApplyDoorAccess,
							br.RecordStatus
						FROM
							INTRANET_EBOOKING_RECORD as br
						WHERE
							'".$startDateLimitString."' <= br.Date And br.Date <= '".$endDateLimitString."'
						ORDER BY
							br.Date, br.StartTime
					";
				$allBookingInfoAry = $this->returnResultSet($sql);
				$numOfBooking = count($allBookingInfoAry);
				$allBookingIdAry = Get_Array_By_Key($allBookingInfoAry, 'BookingID');
				$requestedByUserIdAry = Get_Array_By_Key($allBookingInfoAry, 'RequestedBy');
				$responsibleUserIdAry = Get_Array_By_Key($allBookingInfoAry, 'ResponsibleUID');
				$relatedUserIdAry = array_values(array_unique(array_merge((array)$requestedByUserIdAry, (array)$responsibleUserIdAry)));
				
				// order by smartcard id as requested by the aci API
				$sql = "Select
							UserID, LPAD(HEX(CAST(CardID AS SIGNED)), 16, '0') as FormattedCardID
						From
							INTRANET_USER
						Where
							UserID IN ('".implode("','", (array)$relatedUserIdAry)."')
						Order By
							LPAD(HEX(CAST(CardID AS SIGNED)), 16, '0')
						";
				$relatedUserInfoAry = $this->returnResultSet($sql);
				$numOfRelatedUser = count($relatedUserInfoAry);
				
				$apiDataAry = array();
				$cardIdMappingAry = array();
				for ($i=0; $i<$numOfRelatedUser; $i++) {
					$_userId = $relatedUserInfoAry[$i]['UserID'];
					$_formattedCardId = $relatedUserInfoAry[$i]['FormattedCardID'];
					
					if ($_formattedCardId != '') {
						$cardIdMappingAry[$_userId] = $_formattedCardId;
						
						// initialize here to maintain the cardid ordering
						$apiDataAry[$_formattedCardId] = array();
					}
				}
					
				### Get booking details data of all booking records
				$bookingDetailsInfoAry = $this->Get_Booking_Details_By_BookingID($allBookingIdAry, LIBEBOOKING_FACILITY_TYPE_ROOM);
				$locationIdAry = Get_Array_By_Key($bookingDetailsInfoAry, 'FacilityID');
				$bookingDetailsAssoAry = BuildMultiKeyAssoc($bookingDetailsInfoAry, 'BookingID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
				$numOfBookingDetails = count($bookingDetailsInfoAry);
				
				### Get the card reader info of all locations
				$liblocation_cardReader = new liblocation_cardReader();
				$cardReaderInfoAry = $liblocation_cardReader->getCardReaderInfoByLocationId($locationIdAry, $locationConfigAry['INVENTORY_LOCATION_CARD_READER']['RecordStatus']['enabled']);
				$cardReaderAssoAry = BuildMultiKeyAssoc($cardReaderInfoAry, 'LocationID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
				$numOfCardReader = count($cardReaderInfoAry);
				
				for ($i=0; $i<$numOfBooking; $i++) {
					$_bookingId = $allBookingInfoAry[$i]['BookingID'];
					$_date = $allBookingInfoAry[$i]['Date'];
					$_startTime = $allBookingInfoAry[$i]['StartTime'];
					$_endTime = $allBookingInfoAry[$i]['EndTime'];
					$_requestedBy = $allBookingInfoAry[$i]['RequestedBy'];
					$_responsibleUid = $allBookingInfoAry[$i]['ResponsibleUID'];
					$_applyDoorAccess = $allBookingInfoAry[$i]['ApplyDoorAccess'];
					$_recordStatus = $allBookingInfoAry[$i]['RecordStatus'];
					$_bookingDetailsInfoAry = $bookingDetailsAssoAry[$_bookingId];
					$_numOfBookingDetails = count($_bookingDetailsInfoAry);
					
					$_dateApi = date('dmY', strtotime($_date));
					$_startTimeApi = date('His', strtotime($_startTime));
					$_endTimeApi = date('His', strtotime($_endTime));
					
					
					if (!is_array($_bookingDetailsInfoAry) || $_numOfBookingDetails == 0 || $numOfCardReader == 0) {
						continue;
					}
					
					if ($_applyDoorAccess) {
						for ($j=0; $j<$_numOfBookingDetails; $j++) {
							$__bookingDetailsId = $_bookingDetailsInfoAry[$j]['RecordID'];
							$__roomId = $_bookingDetailsInfoAry[$j]['FacilityID'];
							$__bookingStatus = $_bookingDetailsInfoAry[$j]['BookingStatus'];
							$__roomCardReaderAry = $cardReaderAssoAry[$__roomId];
							$__numOfCardReader = count((array)$__roomCardReaderAry);
							
							if ($__numOfCardReader == 0) {
								continue;
							}
							
							if ($_recordStatus == 1 && $__bookingStatus == LIBEBOOKING_BOOKING_STATUS_APPROVED) {
								$__requestedByUserCardId = $cardIdMappingAry[$_requestedBy];
								
								for ($k=0; $k<$__numOfCardReader; $k++) {
									$___roomReaderCode = $__roomCardReaderAry[$k]['Code'];
									
									$apiDataAry[$__requestedByUserCardId][] = array(
																			'card_id' => $__requestedByUserCardId,
																			'door_id' => $___roomReaderCode,
																			'date' => $_dateApi,
																			'start_time' => $_startTimeApi,
																			'end_time' => $_endTimeApi
																		);
								}
								
								
								if ($_responsibleUid != $_requestedBy) {
									$__responsibleUserCardId = $cardIdMappingAry[$_responsibleUid];
								
									for ($k=0; $k<$__numOfCardReader; $k++) {
										$___roomReaderCode = $__roomCardReaderAry[$k]['Code'];
										
										$apiDataAry[$__responsibleUserCardId][] = array(
																				'card_id' => $__responsibleUserCardId,
																				'door_id' => $___roomReaderCode,
																				'date' => $_dateApi,
																				'start_time' => $_startTimeApi,
																				'end_time' => $_endTimeApi
																			);
									}
								}
							}
						}
					}
				}	// end loop all booking
				
				
				$consolidatedApiDataAry = array();
				foreach ((array)$apiDataAry as $_cardId => $_userBookingAry) {
					$consolidatedApiDataAry = array_merge((array)$consolidatedApiDataAry, (array)$_userBookingAry);
				}
				
				include_once($PATH_WRT_ROOT.'includes/libdooraccess_aci2.php');
				$libdooraccess_aci2 = new libdooraccess_aci2();
				$libdooraccess_aci2->actionCreate($consolidatedApiDataAry);
				
				$success = $libdooraccess_aci2->actionUpload($door_access['host'],$door_access['username'],$door_access['password']);
				return $success;
			}
		}
	}
}
		
?>