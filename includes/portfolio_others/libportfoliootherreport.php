<?php
/*
 * 	Description: This class aims to manage other portfolio like importing primary school portfolio to secondary school
 * 
 * 	Date:	2015-11-12 [Cameron] create this file
 * 
 */
class libportfoliootherreport {
	private $db;	
	private $OtherReportID;
	private $ReportTitle;
	private $Description;
	private $DateInput;
	private $InputBy;
	private $DateLastModified;
	private $LastModifiedBy;

	
	public function getdb(){return $this->db;}
	public function setdb($x){$this->db = $x;}
	
	public function getOtherReportID(){return $this->OtherReportID;}
	public function setOtherReportID($x){$this->OtherReportID = $x;}
	
	public function getReportTitle(){return $this->ReportTitle;}
	public function setReportTitle($x){$this->ReportTitle = $x;}
	
	public function getDescription(){return $this->Description;}
	public function setDescription($x){$this->Description = $x;}
	
	public function getDateInput(){return $this->DateInput;}
	public function setDateInput($x){$this->DateInput = $x;}
	
	public function getInputBy(){return $this->InputBy;}
	public function setInputBy($x){$this->InputBy = $x;}
	
	public function getDateLastModified(){return $this->DateLastModified;}
	public function setDateLastModified($x){$this->DateLastModified = $x;}
	
	public function getLastModifiedBy(){return $this->LastModifiedBy;}
	public function setLastModifiedBy($x){$this->LastModifiedBy = $x;}

	public function libportfoliootherreport($OtherReportID='') {
		$this->db = new libdb();	
		if (!empty($OtherReportID)) {
			$this->setOtherReportID($OtherReportID);
			$this->loadField();			
		}
	}
	
	private function loadField() {
		global $eclass_db;
		$sql = "SELECT 	* 
				FROM 	$eclass_db.PORTFOLIO_OTHER_REPORT 
				WHERE  	OtherReportID='". IntegerSafe($this->OtherReportID,$separator='') ."'";
		$rs = $this->db->returnArray($sql);
		if (count($rs) == 1)
		{
			$rs = current($rs);
			$this->OtherReportID = $rs['OtherReportID'];
			$this->ReportTitle = $rs['ReportTitle'];
			$this->Description = $rs['Description'];
			$this->DateInput = $rs['DateInput'];
			$this->InputBy = $rs['InputBy'];
			$this->DateLastModified = $rs['DateLastModified'];
			$this->LastModifiedBy = $rs['LastModifiedBy'];
		}						
	}

	public function save(){
		if(empty($this->OtherReportID)){
//error_log("\n\n before insert-->".print_r('',true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
			
			$ret = $this->insertRecord();			
		}else{
//error_log("\n\n before update-->".print_r('',true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
			
			$ret = $this->updateRecord();	
		}
		return $ret;
	}

	private function updateRecord(){
		global $eclass_db;		
		if(!empty($this->OtherReportID)){ 			
			$DataArr = array();
			$DataArr['ReportTitle'] = $this->db->pack_value($this->getReportTitle(),'str');
			$DataArr['Description'] = $this->db->pack_value($this->getDescription(),'str');
			$DataArr['DateLastModified'] = $this->db->pack_value('now()','date');
			$DataArr['LastModifiedBy'] = $this->db->pack_value($_SESSION['UserID'],'str');

			$updateDetails = '';
			foreach ($DataArr as $fieldName => $data)
			{
				$updateDetails .= $fieldName."=".$data.",";
			}

			// Remvoe last occurrence of ",";
			$updateDetails = substr($updateDetails,0,-1);

			$sql = "UPDATE ".$eclass_db.".PORTFOLIO_OTHER_REPORT SET ".$updateDetails." WHERE OtherReportID = '".$this->OtherReportID."'";
			$ret = $this->db->db_db_query($sql);
			if($ret != 1){
				return false;
			}
			else {
				return $ret;
			}
		}
		else {
			return false;
		}
	}
	
	private function insertRecord(){
		global $eclass_db;
		$DataArr = array();
		$DataArr['ReportTitle'] = $this->db->pack_value($this->getReportTitle(),'str');
		$DataArr['Description'] = $this->db->pack_value($this->getDescription(),'str');
		$DataArr['DateInput'] = $this->db->pack_value('now()','date');
		$DataArr['InputBy'] = $this->db->pack_value($_SESSION['UserID'],'str');
		$DataArr['DateLastModified'] = $this->db->pack_value('now()','date');
		$DataArr['LastModifiedBy'] = $this->db->pack_value($_SESSION['UserID'],'str');
		
		$sql = 'INSERT INTO '.$eclass_db.'.PORTFOLIO_OTHER_REPORT ('.implode(',',array_keys($DataArr)).') VALUES ('.implode(',',array_values($DataArr)).')';
//error_log("\n\n -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

		$ret = $this->db->db_db_query($sql);
		if($ret != 1){
			return false;
		}
		else {
			$this->OtherReportID = $this->db->db_insert_id();
			return $ret;
		}
	}
			
	public function deleteRecord(){
		global $eclass_db;		

		if(!empty($this->OtherReportID)){
			$sql = "DELETE FROM ".$eclass_db.".PORTFOLIO_OTHER_REPORT WHERE OtherReportID = '".$this->OtherReportID."'";
			$ret = $this->db->db_db_query($sql);
			if($ret == 0){
				return false;
			}
			else {
				return $ret;
			}
		}
		else {
			return false;
		}
	}

	// return true if item exist and false otherwise
	public function isReportItemExist() {
		global $eclass_db;
		$sql = "SELECT 	COUNT(ReportItemID) AS NumberOfItem
 				FROM 	$eclass_db.PORTFOLIO_OTHER_REPORT_ITEM 
				WHERE 	OtherReportID = '".$this->OtherReportID."'";
				
		$rs = $this->db->returnArray($sql);
		if (count($rs) > 0) {
			$rs = current($rs);
			$num = $rs['NumberOfItem'];
		}
		else {
			$num = 0;
		}		
		return $num ? true : false; 
	}
	
}	// End class libportfoliootherreport

?>