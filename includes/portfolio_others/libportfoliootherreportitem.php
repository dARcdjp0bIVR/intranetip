<?php
/*
 * 	Description: This class aims to manage other portfolio items like importing primary school portfolio to secondary school
 * 
 * 	Date:	2015-11-13 [Cameron] create this file
 * 
 */
class libportfoliootherreportitem {
	private $db;	
	private $ReportItemID;
	private $OtherReportID;
	private $StudentID;
	private $ReportFile;
	private $IsDir;
	private $DateInput;
	private $InputBy;

	
	public function getdb(){return $this->db;}
	public function setdb($x){$this->db = $x;}
	
	public function getReportItemID(){return $this->ReportItemID;}
	public function setReportItemID($x){$this->ReportItemID = $x;}
	
	public function getOtherReportID(){return $this->OtherReportID;}
	public function setOtherReportID($x){$this->OtherReportID = $x;}
	
	public function getStudentID(){return $this->StudentID;}
	public function setStudentID($x){$this->StudentID = $x;}
	
	public function getReportFile(){return $this->ReportFile;}
	public function setReportFile($x){$this->ReportFile = $x;}
	
	public function getIsDir(){return $this->IsDir;}
	public function setIsDir($x){$this->IsDir = $x;}
	
	public function getDateInput(){return $this->DateInput;}
	public function setDateInput($x){$this->DateInput = $x;}
	
	public function getInputBy(){return $this->InputBy;}
	public function setInputBy($x){$this->InputBy = $x;}


	public function libportfoliootherreportitem($ReportItemID='') {
		$this->db = new libdb();	
		if (!empty($ReportItemID)) {
			$this->setReportItemID($ReportItemID);
			$this->loadField();			
		}
	}
	
	private function loadField() {
		global $eclass_db;
		$sql = "SELECT 	* 
				FROM 	$eclass_db.PORTFOLIO_OTHER_REPORT_ITEM 
				WHERE  	ReportItemID='". IntegerSafe($this->ReportItemID,$separator='') ."'";
		$rs = $this->db->returnArray($sql);
		if (count($rs) == 1)
		{
			$rs = current($rs);
			$this->ReportItemID = $rs['ReportItemID'];
			$this->OtherReportID = $rs['OtherReportID'];
			$this->StudentID = $rs['StudentID'];
			$this->ReportFile = $rs['ReportFile'];
			$this->IsDir = $rs['IsDir'];
			$this->DateInput = $rs['DateInput'];
			$this->InputBy = $rs['InputBy'];
		}						
	}

	public function save(){
		if(empty($this->ReportItemID)){
//error_log("\n\n before insert-->".print_r('',true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
			
			$ret = $this->insertRecord();			
		}else{
//error_log("\n\n before update-->".print_r('',true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
			
			$ret = $this->updateRecord();	
		}
		return $ret;
	}

	private function updateRecord(){
		global $eclass_db;		
		if(!empty($this->ReportItemID)){ 			
			$DataArr = array();
			$DataArr['ReportFile'] = $this->db->pack_value($this->getReportFile(),'str');
			$DataArr['IsDir'] = $this->db->pack_value($this->getIsDir(),'str');

			$updateDetails = '';
			foreach ($DataArr as $fieldName => $data)
			{
				$updateDetails .= $fieldName."=".$data.",";
			}

			// Remvoe last occurrence of ",";
			$updateDetails = substr($updateDetails,0,-1);

			$sql = "UPDATE ".$eclass_db.".PORTFOLIO_OTHER_REPORT_ITEM SET ".$updateDetails." WHERE ReportItemID = '".$this->ReportItemID."'";
			$ret = $this->db->db_db_query($sql);
			if($ret != 1){
				return false;
			}
			else {
				return $ret;
			}
		}
		else {
			return false;
		}
	}
	
	private function insertRecord(){
		global $eclass_db;
		$DataArr = array();
		$DataArr['OtherReportID'] = $this->db->pack_value($this->getOtherReportID(),'str');
		$DataArr['StudentID'] = $this->db->pack_value($this->getStudentID(),'str');
		$DataArr['ReportFile'] = $this->db->pack_value($this->getReportFile(),'str');
		$DataArr['IsDir'] = $this->db->pack_value($this->getIsDir(),'str');
		$DataArr['DateInput'] = $this->db->pack_value('now()','date');
		$DataArr['InputBy'] = $this->db->pack_value($_SESSION['UserID'],'str');
		
		$sql = 'INSERT INTO '.$eclass_db.'.PORTFOLIO_OTHER_REPORT_ITEM ('.implode(',',array_keys($DataArr)).') VALUES ('.implode(',',array_values($DataArr)).')';
//error_log("\n\n -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

		$ret = $this->db->db_db_query($sql);
		if($ret != 1){
			return false;
		}
		else {
			$this->ReportItemID = $this->db->db_insert_id();
			return $ret;
		}
	}
			
	public function deleteRecord(){
		global $eclass_db;		

		if(!empty($this->ReportItemID)){
			$sql = "DELETE FROM ".$eclass_db.".PORTFOLIO_OTHER_REPORT_ITEM WHERE ReportItemID = '".$this->ReportItemID."'";
//error_log("\n\n -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
			
			$ret = $this->db->db_db_query($sql);
			if($ret == 0){
				return false;
			}
			else {
				return $ret;
			}
		}
		else {
			return false;
		}
	}
	
	/*
	 * 	get the item owner (student) with class and class number
	 */
	public function getItemOwner() {
		global $intranet_session_language,$eclass_db,$intranet_db;
		if ($intranet_session_language == "en") {
			$sql_UserName = "u.EnglishName";
			$sql_ClassName = "yc.ClassTitleEN";
			
		}
		else {
			$sql_UserName = "u.ChineseName";
			$sql_ClassName = "yc.ClassTitleB5";
		}

		$currentAcademicYear = Get_Current_Academic_Year_ID();
		
		$sql = "SELECT 	r.ReportTitle,
						$sql_UserName AS StudentName,
						$sql_ClassName AS ClassName,
						ycu.ClassNumber				
				FROM    {$eclass_db}.PORTFOLIO_OTHER_REPORT_ITEM i
				INNER JOIN {$eclass_db}.PORTFOLIO_OTHER_REPORT r
					ON r.OtherReportID=i.OtherReportID
				INNER JOIN {$intranet_db}.INTRANET_USER u
					ON  u.UserID=i.StudentID
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu 
					ON  ycu.UserID=u.UserID
				INNER JOIN {$intranet_db}.YEAR_CLASS yc
					ON 	yc.YearClassID=ycu.YearClassID
					AND yc.AcademicYearID='$currentAcademicYear' 
				WHERE 	i.ReportItemID = '".$this->ReportItemID."'";
		$rs = $this->db->returnArray($sql);
		return (count($rs) == 1) ? current($rs) : false;			
	}
	
}	// End class libportfoliootherreportitem

?>