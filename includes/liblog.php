<?php
// Modifying by : 

### Change Log [Start] ###
/*
* Date		:	2011-02-08 (Kenneth Chung)
* Detail	:	modified INSERT_LOG(), add Get_Safe_SQL_Query to field value that may include single quote
*
* Date		:	2010-07-08 (Henry Chow)
* Detail	:	modified INSERT_LOG(), add quotation marks in SQL statement
*
*/
### Change Log [Start] ###

class liblog extends libdb 
{
	function liblog()
	{
		$this->libdb();
	}
	    
	function BUILD_DETAIL($ary=array())
	{
		$str = "";
		$br = "";
		if(!empty($ary))
		{
			foreach($ary as $k=>$d)
			{
				$str .= $br . str_replace("_", " ", $k) . ": ".$d;
				$br = "<br>";
			}
		}
		return $str;
	}
	
	function INSERT_LOG($Module='', $Section='', $RecordDetail='', $TableName='', $RecodID='')
	{
		if($Module)
		{
			if(empty($RecordDetail))
				$RecordDetail = "";
			else if(is_array($RecordDetail))
				$RecordDetail = $this->BUILD_DETAIL($RecordDetail);
				
			$RecodID = $RecodID ? $RecodID : NULL;
			$sql = "insert into MODULE_RECORD_DELETE_LOG 
					(Module, Section, RecordDetail, DelTableName, DelRecordID, LogDate, LogBy)
					values
					('$Module', '$Section', '".$this->Get_Safe_Sql_Query($RecordDetail)."', '$TableName', '$RecodID', now(), '".$_SESSION['UserID']."')
					";
			return $this->db_db_query($sql) or die(mysql_error());
		}
	}    
	
	function DELETE_LOG($Module='', $StartDate='', $EndDate='')
	{
		if($Module && $StartDate && $EndDate)
		{
			$sql = "delete from 
						MODULE_RECORD_DELETE_LOG 
					where
						Module = '". $Module ."' and 
						left(LogDate,10) >= '". $StartDate ."' and 
						left(LogDate,10) <= '". $EndDate ."'
					";	
			return $this->db_db_query($sql) or die(mysql_error());
		}
	}
	
	
	    
}
?>