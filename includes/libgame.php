<?php

class game extends libdb{

	function game()
	{
		global $lu;
		global $UserID;

		$this->libdb();

		$this->checkGameUserInfo($UserID);

//		list($class_name,$class_number) = explode("-",$lu->class_number);
//		$class_name	= trim($class_name);
		$class_name	= $lu->ClassName;

		$form = (int)ereg_replace("[^0-9]", "", $class_name);
		if(!in_array($form,array(1,2,3,4,5,6,7)))
			$form = 1;

		$this->form = $form;
		$this->class_name = $class_name;

		$this->air_type = 1;
		$this->h2o_type = 2;
		$this->tree_type = 3;
	}

//	function retrieve_game_report($cpid){
//		global $ck_user_id, $ck_wws_subject,$Lang;
//
//
//		$ability_arr = $Lang['LessonPlanToAbility']["template_".$ck_wws_subject];
//		$score_arr = $Lang['LessonPlanToAbility']["template_".$ck_wws_subject];
//
//		if($cpid != ''){
//			$sql = " SELECT * FROM lesson_plan_section WHERE plan_id = '$cpid' ";
//			$lps_obj = $this->returnArray($sql);
//
//
//			foreach($lps_obj as $key=>$value){
//				$to_ability = $value['to_ability'];
//
//				$serializedArr = unserialize($value['functionSerializedObj']);
//				if($serializedArr["Note"]){
//					$current_note_no = key($serializedArr["Note"]);
//
//					$sql = " Select * From notes_section WHERE notes_id = '$current_note_no' ";
//
//					$ns_obj = current($this->returnArray($sql));
//					if($ns_obj["notes_section_type"] == 15){
//						$content_Arr = explode("|=|", $ns_obj["content"]);
//						if($content_Arr[0] == 0){
//							$q_count = count(explode("\n",$content_Arr[2]));
//						}else{
//							$q_count = count(explode("|=@=|",$content_Arr[2]));
//						}
//
//						$full_mark = $q_count * $content_Arr[4];
//						$ability_arr[$to_ability] += $full_mark;
//
//						$notes_section_id = $ns_obj["notes_section_id"];
//
//						$sql = " SELECT score FROM minigame_record WHERE notes_section_id = '$notes_section_id' " .
//								" AND plan_id = '$cpid' AND user_id = '$ck_user_id' " .
//								" ORDER BY score DESC LIMIT 1 ";
//
//						$score = current($this->returnVector($sql));
//						$score_arr[$to_ability] += $score;
//					}
//				}
//			}
//
//			foreach($score_arr as $key=>$value){
//				$score_arr[$key] = $ability_arr[$key] == 0 ? 0 : (100*$score_arr[$key]/ $ability_arr[$key]);
//			}
//		}
//
//		return $score_arr;
//	}

	function genGameUserDataXML($teacher_level='')
	{
		global $lu;
		$xml = $this->genXMLHead();

		$form = ($teacher_level!='')?$teacher_level:$this->form;
		if(!in_array($form,array(1,2,3,4,5,6)))
			$form = 1;
		else{
			$form = ($this->form -1) % 3 + 1;
		}

		$xml.='<data><xHeader><xSpan type="Debug">false</xSpan><xSpan type="Primary">'.$form.'</xSpan></xHeader>';
		$xml.= $this->genXMLIslandData();
		$xml.= $this->genXMLPlayerData(true);
		$xml.= $this->genXMLOtherPlayerData($teacher_level);
		$xml.= $this->genXMLBuildingData($teacher_level);
		$xml.= $this->genXMLEventData($teacher_level);
		$xml.='</data>';

		$xml = ereg_replace("[\r\t\n]","",$xml);


		return $xml;
	}

	function genXMLIslandData()
	{
		$today = date("Y-m-d");
		$days_in_month = $this->getDaysInMonth();

		$sql = "SELECT Level,RiseType,ScoreSpent,if(datediff('".$today."',timeinput)<=$days_in_month,ScoreSpent,0) as MonthlySpent FROM W2_GAME_PARTICIPATE ";
		$score_obj = $this->returnArray($sql);

		for($a=0;$a<sizeof($score_obj);$a++){
			$tscore_arr[$score_obj[$a]['Level']][$score_obj[$a]['RiseType']]['total'] += $score_obj[$a]['ScoreSpent'];
			$tscore_arr[$score_obj[$a]['Level']][$score_obj[$a]['RiseType']]['month'] += $score_obj[$a]['MonthlySpent'];
		}

		$score_index = ($this->form -1) % 3 + 1;
		$score_arr[$score_index] = $tscore_arr[$this->form];

		for($i =1; $i <= $score_index; $i ++){
			if(sizeof($score_arr[$i]) == 0){
	            $score_arr[$i][$this->air_type]['total']=0;
	            $score_arr[$i][$this->h2o_type]['total']=0;
	            $score_arr[$i][$this->tree_type]['total']=0;
	            $score_arr[$i][$this->air_type]['month']=0;
	            $score_arr[$i][$this->h2o_type]['month']=0;
	            $score_arr[$i][$this->tree_type]['month']=0;
			}
		}


		$xml = '<IslandData primary="1" totalAirNeed="76000" totalWaterNeed="84500" totalTreeNeed="85750" airSpend="'.$score_arr[1][$this->air_type]['total'].'" waterSpend="'.$score_arr[1][$this->h2o_type]['total'].'" treeSpend="'.$score_arr[1][$this->tree_type]['total'].'" airMonthSpend="'.$score_arr[1][$this->air_type]['month'].'" waterMonthSpend="'.$score_arr[1][$this->h2o_type]['month'].'" treeMonthSpend="'.$score_arr[1][$this->tree_type]['month'].'" />
				<IslandData primary="2" totalAirNeed="76000" totalWaterNeed="84500" totalTreeNeed="85750" airSpend="'.$score_arr[2][$this->air_type]['total'].'" waterSpend="'.$score_arr[2][$this->h2o_type]['total'].'" treeSpend="'.$score_arr[2][$this->tree_type]['total'].'" airMonthSpend="'.$score_arr[2][$this->air_type]['month'].'" waterMonthSpend="'.$score_arr[2][$this->h2o_type]['month'].'" treeMonthSpend="'.$score_arr[2][$this->tree_type]['month'].'" />
				<IslandData primary="3" totalAirNeed="76000" totalWaterNeed="84500" totalTreeNeed="85750" airSpend="'.$score_arr[3][$this->air_type]['total'].'" waterSpend="'.$score_arr[3][$this->h2o_type]['total'].'" treeSpend="'.$score_arr[3][$this->tree_type]['total'].'" airMonthSpend="'.$score_arr[3][$this->air_type]['month'].'" waterMonthSpend="'.$score_arr[3][$this->h2o_type]['month'].'" treeMonthSpend="'.$score_arr[3][$this->tree_type]['month'].'" />
				<IslandData primary="4" totalAirNeed="76000" totalWaterNeed="84500" totalTreeNeed="85750" airSpend="'.$score_arr[4][$this->air_type]['total'].'" waterSpend="'.$score_arr[4][$this->h2o_type]['total'].'" treeSpend="'.$score_arr[4][$this->tree_type]['total'].'" airMonthSpend="'.$score_arr[4][$this->air_type]['month'].'" waterMonthSpend="'.$score_arr[4][$this->h2o_type]['month'].'" treeMonthSpend="'.$score_arr[4][$this->tree_type]['month'].'" />
				<IslandData primary="5" totalAirNeed="76000" totalWaterNeed="84500" totalTreeNeed="85750" airSpend="'.$score_arr[5][$this->air_type]['total'].'" waterSpend="'.$score_arr[5][$this->h2o_type]['total'].'" treeSpend="'.$score_arr[5][$this->tree_type]['total'].'" airMonthSpend="'.$score_arr[5][$this->air_type]['month'].'" waterMonthSpend="'.$score_arr[5][$this->h2o_type]['month'].'" treeMonthSpend="'.$score_arr[5][$this->tree_type]['month'].'" />
				<IslandData primary="6" totalAirNeed="76000" totalWaterNeed="84500" totalTreeNeed="85750" airSpend="'.$score_arr[6][$this->air_type]['total'].'" waterSpend="'.$score_arr[6][$this->h2o_type]['total'].'" treeSpend="'.$score_arr[6][$this->tree_type]['total'].'" airMonthSpend="'.$score_arr[6][$this->air_type]['month'].'" waterMonthSpend="'.$score_arr[6][$this->h2o_type]['month'].'" treeMonthSpend="'.$score_arr[6][$this->tree_type]['month'].'" />';
		return $xml;
	}



	function getCharacterData($id='')
	{
		$sql = "SELECT InfoXML FROM W2_GAME_USER_INFO WHERE UserID = '".$id."'";
		$info_obj = $this->returnVector($sql);

		$xml = stripslashes($info_obj[0]);
		return $xml;
	}

	function genXMLPlayerData($is_self=false,$peers_id='')
	{
		global $ck_course_id;
		$ck_memberType = $_SESSION["UserType"];

		if($is_self)
		{
			global $UserID,$lu;

			$gender			= $lu->gender;
//			list($class_name,$class_number) = explode("-",$lu->class_number);
			$class_name		= $lu->ClassName;
			$english_name	= $lu->EnglishName;
			$user_type 		= $ck_memberType?'S':'T';

			$is_mine = 1;
			if($user_type=='S'){
				$info_obj = $this->getUserGameScoreInfo();

				$score_left 	= $info_obj['ScoreLeft'];
				$score_spent	= $info_obj['ScoreSpent'];
			}
			else{

			}

			$id = $UserID;
		}
		else
		{
			if($peers_id!=''){

				$lu2 = new libuser($peers_id);

				$gender			= $lu2->Gender;
				$class_name		= $lu2->ClassName;
				$english_name	= $lu2->EnglishName;
				$user_type 		= 'S';

				$info_obj = $this->getUserGameScoreInfo($peers_id);

				$score_left 	= $info_obj['ScoreLeft'];
				$score_spent	= $info_obj['ScoreSpent'];
				$id = $peers_id;
			}
		}

		$xml = '<PlayerData id="'.$id.'" name="'.$english_name.'" class="'.$class_name.'" sex="'.$gender.'" usertype="'.$user_type.'" isMine="'.$is_mine.'" score="'.$score_left.'" totalSpend="'.$score_spent.'" >';
		$xml.= $this->getCharacterData($id);
		$xml.='</PlayerData>';
		return $xml;
	}

	function checkGameUserInfo($id=''){
		global $UserID;
		$id = $id?$id:$UserID;
		$sql = " SELECT AirUsed,WaterUsed,TreeUsed,GameScoreLeft,LastUsed FROM W2_GAME_USER_INFO WHERE UserID = '$id' " ;
		$obj = $this->returnArray($sql);

		if(sizeof($obj)==0){
			$sql = " INSERT INTO W2_GAME_USER_INFO (UserID, LastUsed) Values ('$UserID', NOW()) ";
			$this->db_db_query($sql);

			$last_time_sql = " InputDate < NOW() ";
		}else{
			$info = current($obj);

			$previous_last_time = $info["LastUsed"];

			if($previous_last_time != ''){
				$last_time_sql = " InputDate > '$previous_last_time' AND InputDate < NOW() ";
			}else{
				$last_time_sql = " InputDate < NOW() ";
			}
		}

		$score_new = 0;

//		$sql = " SELECT  w.WRITING_ID, MARK, TEACHER_WEIGHT, PEER_WEIGHT FROM W2_STEP_HANDIN h
//				 INNER JOIN W2_STEP s ON h.STEP_ID = s.STEP_ID
//				INNER JOIN W2_WRITING w ON s.WRITING_ID= w.WRITING_ID
//				WHERE h.USER_ID = '$id' AND ((s.SEQUENCE = 5 AND w.CONTENT_CODE != 'sci') OR (s.SEQUENCE = 6 AND w.CONTENT_CODE = 'sci'))
//				AND $last_time_sql
//				ORDER BY h.DATE_INPUT DESC ";
//
//		$result = $this->returnArray($sql);
//		$writing_id_arr = array();
//
//		$handin_arr = array();
//
//		foreach($result as $key => $handin){
//			array_push($writing_id_arr,$handin["WRITING_ID"] );
//			$handin_arr["WRITING_ID"] = $handin;
//			$handin_arr["WRITING_ID"]["PEER_MARK"] = 0;
//		}
//
//		$sql = " Select * From W2_PEER_MARKING_DATA WHERE TARGET_USER_ID = '$id' AND WRITING_ID IN (".implode(",", $writing_id_arr).") ";
//
//		$result2 = $this->returnArray($sql);
//
//		foreach($result2 as $key => $peer_mark){
//			$handin_arr[$peer_mark["WRITING_ID"]]["PEER_MARK"] += $peer_mark["MARK"];
//		}
//
//		foreach($handin_arr as $key => $handin){
//			$score_new += (int)(($handin["MARK"] * $handin["TEACHER_WEIGHT"] + $handin["PEER_MARK"] * $handin["PEER_WEIGHT"])/100) ;
//		}
//
//		if($score_new > 0)
//			$this->updateStudentGameScore($id,$score_new);

		$sql = " SELECT * FROM WS_GAME_SCORE_LOG
				WHERE UserID = '$id' AND $last_time_sql ";
		$result2 = $this->returnArray($sql);

		$version_ratio_arr = array(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);

		foreach($result2 as $key => $value){
			$score_new += $value["Score"] ; // * $version_ratio_arr[$value["version"]]
		}

		if($score_new > 0)
			$this->updateStudentGameScore($id,$score_new);

		$sql = " UPDATE W2_GAME_USER_INFO SET LastUsed = NOW() WHERE UserID = '$id'";
		$this->db_db_query($sql);
	}

	function getUserGameScoreInfo($id='')
	{
		global $UserID;
		$id = $id?$id:$UserID;
		$sql = "SELECT AirUsed,WaterUsed,TreeUsed,GameScoreLeft FROM W2_GAME_USER_INFO WHERE UserID = '{$id}'";
		$obj = $this->returnArray($sql);

		if(sizeof($obj)==0){
			$AirUsed	= 0;
			$WaterUsed	= 0;
			$TreeUsed	= 0;
			$ScoreLeft	= 0;
			$score_spent = 0;
		}
		else{
			$AirUsed	= $obj[0]['AirUsed'];
			$WaterUsed	= $obj[0]['WaterUsed'];
			$TreeUsed	= $obj[0]['TreeUsed'];
			$ScoreLeft	= $obj[0]['GameScoreLeft'];
			$score_spent = $obj[0]['AirUsed']+$obj[0]['WaterUsed']+$obj[0]['TreeUsed'];
		}

		return array('AirUsed'=>$AirUsed,'WaterUsed'=>$WaterUsed,'TreeUsed'=>$TreeUsed,'ScoreLeft'=>$ScoreLeft,'ScoreSpent'=>$score_spent);
	}

	function genXMLOtherPlayerData($teacher_level='')
	{
		global $lu,$UserID;

		//$form = ($teacher_level!='')?$teacher_level:$this->form;
		//list($class) = explode("-",$lu->class_number);
		$class_name = $lu->ClassName;
		$sql = "SELECT
					UserID
				FROM
					INTRANET_USER
				WHERE
					ClassName like '".$class_name."%'
				AND
					RecordType='2'
				AND
					UserID != '".$UserID."' ";
		$student_obj = $this->returnVector($sql);

		$xml = '';
		if(sizeof($student_obj)>0){
			for($a=0;$a<sizeof($student_obj);$a++)
				$xml .= $this->genXMLPlayerData(false,$student_obj[$a]);
		}

		return $xml;
	}



	function genXMLBuildingData($teacher_level='')
	{
		global $lu,$ck_school_code, $UserID;

		if($teacher_level!='')
			$form = $teacher_level;
		else
			$form = $this->form;
		$sql = "SELECT
					g.Area,g.Item,g.UserID as UserID
				FROM
					W2_GAME_PARTICIPATE as g
				INNER JOIN
					W2_GAME_USER_INFO as u
				ON
					g.UserID = u.UserID
				WHERE
					g.Level = '".$form."' AND g.UserID = '".$UserID."' AND g.Item not like '99%' ";
		$build_obj = $this->returnArray($sql);

		$xml = '';
		for($a=0;$a<sizeof($build_obj);$a++){
			$xml .= '<BuildRec area="'.$build_obj[$a]['Area'].'" item="'.$build_obj[$a]['Item'].'" sid="'.$build_obj[$a]['UserID'].'" />';
		}

		return $xml;
	}

	function getDaysInMonth(){

		$month = date("m");
		$year = date("Y");

		return cal_days_in_month(CAL_GREGORIAN, $month, $year);
	}

	function genXMLEventData($teacher_level='')
	{
		global $lu,$ck_school_code,$UserID;

		$days_in_month = $this->getDaysInMonth();
		$today = date("Y-m-d");
		$form = ($teacher_level!='')?$teacher_level:$this->form;
		$sql = "SELECT Item,ManNeeded,UserID," .
				" count(if(datediff('".$today."',timeinput)<=$days_in_month,1,null)) as Power," .
				" count(if(UserID=".$UserID." and datediff('".$today."',timeinput)<=$days_in_month,1,null)) as RecentJoin " .
				" FROM W2_GAME_PARTICIPATE WHERE Level = ".$form." AND Item like '99%' GROUP BY Item";
		$event_obj = $this->returnArray($sql);

		$xml = '';
		for($a=0;$a<sizeof($event_obj);$a++){
			$xml .= '<Event item="'.$event_obj[$a]['Item'].'" power="'.$event_obj[$a]['Power'].'" recentJoin="'.$event_obj[$a]['RecentJoin'].'" />';
		}

		return $xml;
	}


	function saveAvatarInfo($xml='')
	{

			if($xml!='')
				$this->updateUserAvatar($xml);
	}

	function updateUserAvatar($xml)
	{
		global $UserID;
		$xml = addslashes($xml);
		$sql = "UPDATE
					W2_GAME_USER_INFO
				SET
					InfoXML = '".$xml."'
				WHERE
					UserID = '".$UserID."'";

		return $this->db_db_query($sql);

	}


	function canParticipate($area,$item,$man_needed){

		$level = $this->form;
		$count = $this->countParticipant($area,$item);

		return $count<$man_needed;
	}

	function deductStudentGameScore($scoreSpend,$buildType,$riseType){

		global $UserID;

		switch($riseType)
		{
			case 1:
				$field = 'Air';
			break;
			case 2:
				$field = 'Water';
			break;
			case 3:
				$field = 'Tree';
			break;

		}
		$field_2_update = $field.'Used = '.$field.'Used + '.$scoreSpend;
		$sql = "UPDATE
					W2_GAME_USER_INFO
				SET
					GameScoreLeft = GameScoreLeft - ".$scoreSpend.",
					".$field_2_update."

				WHERE
					UserID = '".$UserID."'";

		return $this->db_db_query($sql);

	}

	function countParticipant($area,$item){

		global $UserID;

		if(substr($item,0,2)=='99'){
			$days_in_month = $this->getDaysInMonth();
			$today = date("Y-m-d");
			$cond =" and datediff('".$today."',timeinput)<=$days_in_month";
		}

		$sql = "SELECT count(*) FROM W2_GAME_PARTICIPATE WHERE Area = '".$area."' AND Item = '".$item."' AND Level = '".$this->form."' AND UserID = '".$UserID."' ".$cond;

		$obj = $this->returnVector($sql);echo mysql_error();
		return $obj[0];
	}

	function insertGameParticipateRecord($area,$item,$man_needed,$scoreSpend,$buildType,$riseType,$level){
		global $UserID;

		$level = $this->form;

		$sql = "INSERT INTO W2_GAME_PARTICIPATE
				(UserID,Area,Item,ManNeeded,BuildType,RiseType,Level,ScoreSpent,Timeinput,Timemodified)
				VALUES('".$UserID."','".$area."','".$item."','".$man_needed."','''".$buildType."','".$riseType."','".$level."','".$scoreSpend."',now(),now())";

		return $this->db_db_query($sql);
	}

	function updateStudentGameScore($student_id,$score){
		$sql = "UPDATE W2_GAME_USER_INFO SET GameScoreLeft = GameScoreLeft+".$score." WHERE UserID ='".$student_id."'";
		return $this->db_db_query($sql);
	}

	function getBuildXML($area,$item,$level){

		$level = $this->form;

		if(substr($item,0,2)=='99'){
			$days_in_month = $this->getDaysInMonth();
			$today = date("Y-m-d");
			$cond =" and datediff('".$today."',timeinput)<=$days_in_month";
		}
		$sql = "SELECT UserID FROM W2_GAME_PARTICIPATE WHERE Area = '".$area."' AND Item = '".$item."' AND Level = '".$level."' ".$cond;
		$user_obj = $this->returnVector($sql);

		$xml = $this->genXMLHead().'<data>';
		for($a=0;$a<sizeof($user_obj);$a++)
			$xml.= '<BuildRec area="'.$area.'" item="'.$item.'" sid="'.$user_obj[$a].'" />';
		$xml.='</data>';
		return $xml;
	}

	function genXMLHead()
	{
		return '<?xml version="1.0" encoding="utf-8" ?>';
	}

}
?>