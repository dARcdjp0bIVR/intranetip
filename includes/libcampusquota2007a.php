<?php
// Editing by 
/*
 * 2017-08-25 (Carlos): Modified displayiMailPercentageBar(), added onclick handler to quota bar to recalculate used quota.
 */
class libcampusquota2007 extends libcampusquota
{


	function libcampusquota2007($UserID="")
	{	   
		$this->libcampusquota($UserID);   
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// For new Interface (IP 2.0)
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function displayiMailInformation()
	{
		global $i_Campusquota_max,$i_Campusquota_left,$i_Campusquota_used;
		
		$total = $this->returnQuota() * 1024;
		$totalMB = $this->returnQuota(); 
		$used = $this->returnUsedQuota();
		$left = $total - $used;
		if ($left < 0) $left = 0;
		$usedMB = round($used / 1024);
	
		//$x  = "$i_Campusquota_used: {$usedMB} / {$totalMB}MB";
		$x  = "$i_Campusquota_used: ".number_format($used,0)." / ".number_format($total,0)." KB";

		return $x;
	}
	
	function displayiMailPercentageBar()
	{
		global $i_CampusMail_New_Quota1, $i_CampusMail_New_Quota2, $image_path, $LAYOUT_SKIN;
		
		$total = $this->returnQuota() * 1024;
		$used = $this->returnUsedQuota();
		if ($total != 0)
		   $pused = round($used/$total * 100);
		else $pused = 100;
		$left = $total - $used;
		if ($left < 0) $left = 0;
		if ($pused > 100) $pused = 100;		

		$storage   = "<style type='text/css'> \n";
		$storage  .= "<!--";
		$storage  .= "#UsageLayer {";
		$storage  .= "	position:absolute;";
		$storage  .= "	width:150px;";
		$storage  .= "	height:14x;";
		$storage  .= "	z-index:2;";
		$storage  .= "}";
		$storage  .= "-->";
		$storage  .= "</style>";
		
		$storage .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' class='imailusage' > \n";
		$storage .= "<tr > \n";			
		$storage .= "<td style='vertical-align:bottom'  > \n";
		$storage .= "<div id='UsageLayer' > \n";                                    
		$storage .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' > \n";                                      
		$storage .= "<tr> \n";
		$storage .= "<td align='center' class='imailusagetext'>{$pused}%</td> \n";                                          
		$storage .= "</tr> \n";                                            
		$storage .= "</table> \n";
		$storage .= "</div> \n";                                              
		$storage .= "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/usage_bar.gif' width='$pused%' height='14' align='absmiddle'></td> \n";
		$storage .= "</tr> \n";                                            
		$storage .= "</table> \n";
        
        $storage .= '<script type="text/javascript" language="javascript">'."\n";
		$storage .= '$(document).ready(function(){'."\n";
		$storage .= '$("#UsageLayer").click(function(){$.get("/home/imail/recalculate_quota.php",function(returnData){window.location.reload();});});'."\n";
		$storage .= '});'."\n";
		$storage .= '</script>'."\n";
                                      
		return $storage;
	}	
	
	function displayiMailFullBar()
	{
		$x  = "";
		$x .= "<table border='0' cellspacing='0' cellpadding='0'  >";
		$x .= "<tr>";
		$x .= "<td class='tabletext'  >".$this->displayiMailInformation()."&nbsp;</td>";                              
		$x .= "<td  >".$this->displayiMailPercentageBar()."</td>";                              
		$x .= "</tr> \n";                                            
		$x .= "</table> \n";
		
		return $x;
	}	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
            
}
?>