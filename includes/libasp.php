<?php

# being modified by: Siuwan


/********************** Change Log ************************
*   Date	:	2016-02-05 [Siuwan]
*	Details	:	added one project PowerVoice
* 
*  	Date	:	2015-09-23 [Paul]
*	Details	:	added classlevel handling specifies for the case happening in GrammarTouch1 & GrammarTouch2
*				Replacing originally using ereg to preg as PHP suggested the function has been deprecated
*
*   Date	:	2015-05-29 [Siuwan]
*	Details	:	added one project GrammarTouch2 in Class 
*
*  	Date	:	2010-10-31 [Siuwan]
*	Details	:	added one project LS in Class 
*
*  	Date	:	2010-07-12 [Kelvin]
*	Details	:	added UserEmail as one of the field to pass to ASP
*
* 	Date	:	2009-12-11 [Yuen]
*	Details	:	improved the classlevel detection by removing all letters to keep numbers only
*
* 	Date	:	2009-12-09 [Yuen]
*	Details	:	introduce ASP api version no for backward compitability
*               (e.g. if ASP server change but a school still use previous API version)
*
/******************* End Of Change Log *******************/


class libasp extends libdb{


		var $ASP_Server;
		var $ASP_Server_Login;
		var $KeySalt;
		var $Delimiter;
		var $SchoolCode;
		var $YearlyLicenseKey;
		var $KeyEncrpyt;
		var $TimeThreshold = 20;		//minutes
		var $SchoolYearArr;
		var $UserNameEng;
		var $UserNameChi;
		var $ClassLevel;
		var $ClassName;
		var $ClassNumber;
		var $IsServer;
		var $SysLang;
		var $eClassUserID;
		var $eClassUserType;
		var $isAdminUser;
		var $APIVersion;
		var $ASPCourseID;
		var $project;
		var $CentralServiceServerPath;
		
        function libasp($project, $is_server=false)
        {
			global $plugin, $intranet_db,$intranet_root,$lslp_httppath;

			$this->db = $intranet_db;
			$this->IsServer = $is_server;
			$this->project = $project;
			# please assign using datetime and desc order (larger version should be first)
			# will compare the version value and apply corrsponding logic changes
			$this->APIVersion[] = array(200912091722, "add version no. and use rawURLencode on data");
			$this->CentralServiceServerPath = "https://service.broadlearning.com"; //for powervoice 
			
	        switch($project)
	        {
	        	case "PowerVoice":
	        		$powerVoiceLength = "";
					if (isset($plugin['power_voice']))
					{
						$this->ASP_Server = $this->CentralServiceServerPath;
						$this->ASP_Server_Login = $this->CentralServiceServerPath."/ecAsp/auth.php";
					}
					
					$this->KeySalt = "T8E694iQz64C2";
					$this->KeyEncrpyt = "pVoicezSgo5";
					
	        	break;
	        	case "FlipChan":
					$this->isAdminUser = $plugin['FlippedChannels'] && $_SESSION["SSV_USER_ACCESS"]["eLearning-FlippedChannels"];
					if (isset($plugin['FlippedChannels']) && $plugin['FlippedChannels'])
					{
						//$this->ASP_Server = "http://videolib.eclass.com.hk";
						$this->ASP_Server = $_SESSION["FlippedChannelsServerPath"];
					}
					else 
					{
						//$this->ASP_Server = "http://videolib-dev.eclass.com.hk";
					}
					
					$this->KeySalt = "yUed768G119KK";
					$this->KeyEncrpyt = "flipChanwoB6";
					
	        	break;
	        	case "GrammarTouch1":

		     		# check if user is admin user
		     		include_once($intranet_root."/includes/iTextbook/libitextbook.php");
		     		$libitextbook = new libitextbook();
		     		$course_code = "grammartouch1";
		     		$module_id = $libitextbook->get_iTextbook_ModuleID($course_code);
		     		//$this->isAdminUser = $plugin['iTextbook'] && $_SESSION["SSV_USER_ACCESS"]["eLearning-iTextbook"] && $libitextbook->is_iTextbook_enabled($module_id);

					
					if (isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev'])
					{
						$this->ASP_Server = "http://gt-dev.eclass.com.hk/grammartouch1";
					}else if (isset($plugin['iTextbook']) && $plugin['iTextbook']){
						$this->ASP_Server = "http://gt.eclass.com.hk/grammartouch1";
					}

					$this->KeySalt = "yUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     	break;	
	        	case "GrammarTouch2":

		     		# check if user is admin user
		     		include_once($intranet_root."/includes/iTextbook/libitextbook.php");
		     		$libitextbook = new libitextbook();
		     		$course_code = "grammartouch2";
		     		$module_id = $libitextbook->get_iTextbook_ModuleID($course_code);
		     		//$this->isAdminUser = $plugin['iTextbook'] && $_SESSION["SSV_USER_ACCESS"]["eLearning-iTextbook"] && $libitextbook->is_iTextbook_enabled($module_id);

					
					if (isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev'])
					{
						$this->ASP_Server = "http://gt-dev.eclass.com.hk/grammartouch2";
					}else if (isset($plugin['iTextbook']) && $plugin['iTextbook']){
						$this->ASP_Server = "http://gt.eclass.com.hk/grammartouch2";
					}

					$this->KeySalt = "yUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     	break;	
	        	case "GrammarTouch3":

		     		# check if user is admin user
		     		include_once($intranet_root."/includes/iTextbook/libitextbook.php");
		     		$libitextbook = new libitextbook();
		     		$course_code = "grammartouch3";
		     		$module_id = $libitextbook->get_iTextbook_ModuleID($course_code);
		     		//$this->isAdminUser = $plugin['iTextbook'] && $_SESSION["SSV_USER_ACCESS"]["eLearning-iTextbook"] && $libitextbook->is_iTextbook_enabled($module_id);

					
					if (isset($plugin['iTextbook_dev']) && $plugin['iTextbook_dev'])
					{
						$this->ASP_Server = "http://gt-dev.eclass.com.hk/grammartouch3";
					}else if (isset($plugin['iTextbook']) && $plugin['iTextbook']){
						$this->ASP_Server = "http://gt.eclass.com.hk/grammartouch3";
					}

					$this->KeySalt = "yUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     	break;        	
	        	case "LS":

		     		# check if user is admin user
		     		include_once($intranet_root."/includes/libls.php");
		     		$ls = new ls();
		     		$this->isAdminUser = $ls->isAdminUser();

					if (isset($plugin['ls']) && $plugin['ls'])
					{
						$this->ASP_Server = "http://caswcls.eclass.com.hk";
					}
					else if (isset($plugin['ls_test']) && $plugin['ls_test'])
					{
						$this->ASP_Server = "http://eclass-spec-22.eclass.com.hk";
					}
					else
					{
						$this->ASP_Server = $lslp_httppath;
					}

					$this->KeySalt = "yUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     	break;
		     		
		     	case "LSLP":

		     		# check if user is admin user
		     		include_once($intranet_root."/includes/liblslp.php");
		     		$ls = new lslp();
		     		$this->isAdminUser = $ls->isAdminUser();

					if (isset($plugin['lslp_dev']) && $plugin['lslp_dev'])
					{
						$this->ASP_Server = "http://project7.broadlearning.com";
					}
					else if (isset($plugin['lslp_test']) && $plugin['lslp_test'])
					{
						$this->ASP_Server = "http://lslp-alpha.broadlearning.com";
					}
					else
					{
						$this->ASP_Server = "http://lslp.eclass.com.hk";
					}

					$this->KeySalt = "yUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     		break;

		     	case"P2PW":
					$this->KeySalt = "yUwo284GTS11AB7";
					$this->KeyEncrpyt = "tsaslpBbyuwoB2";
		     	   	break;
		     	 
	     	 	case "PHONICS":

		     		# check if user is admin user
		     		//include_once($intranet_root."/includes/libphonics.php");
		     		//$lp = new phonics();
		     		//$this->isAdminUser = $lp->isAdminUser();
	
					if (isset($plugin['phonics_dev']) && $plugin['phonics_dev'])
					{
						$this->ASP_Server = "http://project8.broadlearning.com";
					}
					else if (isset($plugin['phonics_test']) && $plugin['phonics_test'])
					{
						//$this->ASP_Server = "http://lslp-alpha.broadlearning.com";
					}
					else
					{
						//$this->ASP_Server = $lslp_httppath;
					}
	
					$this->KeySalt = "yUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     	break;
		     	case"LER":
		     	
					$this->KeySalt = "yUwo284GTS11AB7";
					$this->KeyEncrpyt = "lerslpBbyuwoB2";
					$this->ASPCourseID = 1;
					if (isset($plugin['ler_dev']) && $plugin['ler_dev'])
					{
						$this->ASP_Server = "http://ler-dev.broadlearning.com";
					}
					else if (isset($plugin['ler_test']) && $plugin['ler_test'])
					{
						$this->ASP_Server = "http://ler-testing.broadlearning.com";
					}
					else if (isset($plugin['ler_trial']) && $plugin['ler_trial'])
					{
						$this->ASP_Server = "http://ler-trial1.broadlearning.com";
					}
					else if(isset($plugin['ler_production']) && $plugin['ler_production'])
					{
						$this->ASP_Server = "http://ler.broadlearning.com";	
					}
		     	break;  	
		     	case "KSK":

		     		# check if user is admin user
		     		include_once($intranet_root."/includes/libksk.php");
		     		$ksk = new ksk();
		     		$this->isAdminUser = $ksk->isAdminUser();

					if (isset($plugin['kskgsmath_dev']) && $plugin['kskgsmath_dev'])
					{
						$this->ASP_Server = "http://eclass-spec-24.eclass.com.hk";
					}
					else if (isset($plugin['kskgsmath_test']) && $plugin['kskgsmath_test'])
					{
						$this->ASP_Server = "http://eclassgame.twghsksk.edu.hk";
					}
					else
					{
						//$this->ASP_Server = '';
					}

					$this->KeySalt = "kskgsmathyUwo284G119BB";
					$this->KeyEncrpyt = "lslpBbyuwoB6";
		     		break;
		     	   	
	        }
			//$this->$ASP_Server_Login = $this->ASP_Server . "/direct_login.php";
			if(!isset($this->ASP_Server_Login)){
				$this->ASP_Server_Login = $this->ASP_Server . "/direct_login.php";
			}

			if (!$this->IsServer)
			{
				# @ eClass local
				global $intranet_session_language, $intranet_root, $UserID, $US_Intranet_IDType;

				$this->SET_SCHOOL_CODE_KEY();

				$this->eClassUserID = $UserID;

				include_once($intranet_root."/includes/libuser.php");
				$lib_user = new libuser($this->eClassUserID);
				$UserArr['SysLang'] = $intranet_session_language;
				$UserArr['eClassUserID'] = $this->eClassUserID;
				$UserArr['UserNameChi'] = $lib_user->ChineseName;
				$UserArr['UserNameEng'] = $lib_user->EnglishName;
				$UserArr['UserNickname'] = $lib_user->NickName;
				$UserArr['ClassName'] = $lib_user->ClassName;
				$UserArr['ClassNumber'] = $lib_user->ClassNumber;
				if(strpos(strtolower($this->project),'grammartouch') !== false){
					$userForm = $lib_user->Get_User_Studying_Form($this->eClassUserID, '');
					$classlevel = preg_replace("/[^0-9]+/","",$userForm[0]['FormWebSAMSCode']);
					if ($userForm[0]['FormWebSAMSCode'] != "" && $userForm[0]['FormWebSAMSCode'] != 'NA'){
						$classlevel = preg_replace("/[^0-9]+/","",$userForm[0]['FormWebSAMSCode']);
						$UserArr['ClassLevel'] = $classlevel;
					}else if (preg_match('#[0-9]#',$userForm[0]['YearName'])){
						$UserArr['ClassLevel'] = preg_replace("/[^0-9]+/","",$userForm[0]['YearName']);
					}else{
						$UserArr['ClassLevel'] = $lib_user->ClassLevel; /* Required furthur exception handling if needed */
					}
				}else{
					$UserArr['ClassLevel'] = $lib_user->ClassLevel;
				}
				$UserArr['UserEmail'] = $lib_user->UserEmail; # added by Kelvin 2010-07-12				

				# change user type to 4 if user is lslp admin
				if($this->isAdminUser)
				{				
					if($project=="FlipChan"||(strpos(strtolower($project),'grammartouch') !== false)){
						$UserArr['eClassUserType'] = $lib_user->RecordType + 10; //
					}else{
						$UserArr['eClassUserType'] = 4;
					}
				}
				else
				{
					$UserArr['eClassUserType'] = $lib_user->RecordType;
				}
				$this->SET_USER_BASIC_INFO($UserArr);
			}

			$this->Delimiter = "###";
			#$this->SchoolYearArr = array('start'=>"09-01", 'end'=>"08-31");

			#debug($this->UserNameChi, $this->UserNameEng, $this->UserNickname, $this->ClassName, $this->ClassNumber);
	    }


		function SET_USER_BASIC_INFO($ParArr)
		{
			$this->SysLang = $ParArr['SysLang'];//$ParArr['eClassUserType'];
			$this->eClassUserID = $ParArr['eClassUserID'];
			$this->eClassUserType = $ParArr['eClassUserType'];
			$this->UserNameChi = $ParArr['UserNameChi'];
			$this->UserNameEng = $ParArr['UserNameEng'];
			$this->UserNickname = $ParArr['UserNickname'];
			$this->UserEmail = $ParArr['UserEmail'];

			# detect form level
			# 2009-12-11
			# Collect multi-digit number in class level
			# 2015-09-23
			$FormInt = (int)preg_replace("/[^0-9]+/", "", $ParArr['ClassLevel']);
			if ($FormInt==0 || $FormInt<1)
			{
				$FormInt = (int)preg_replace("/[^0-9]+/", "", $ParArr['ClassName']);
			}

			$this->ClassLevel = $FormInt;
			$this->ClassName = $ParArr['ClassName'];
			$this->ClassNumber = $ParArr['ClassNumber'];
		}


		function SET_SCHOOL_CODE_KEY($SchoolCode="") {
			global $config_school_code;

			$this->SchoolCode = ($SchoolCode!="") ? $SchoolCode : $config_school_code;
		}


	    function SET_KEY_STRING_ARRAY($ParArr)
        {
	        if(is_array($ParArr))
	        {
		     	foreach($ParArr as $Key=>$Value)
		     	{
			     	//$this->$Key = $Value;
			     	$this->Key = $Value;
		     	}
	        }

        }


	    function CHECK_ASP_ACCESS($ParArr="",$returnFormData=true)
		{

			//$ReturnURL = $this->$ASP_Server_Login;
			$ReturnURL = $this->ASP_Server_Login;
			if ($returnFormData){
				$ReturnX = $this->GENERATE_KEY_STRING_FORM();
			}
			else{
				$ReturnX = $this->GENERATE_KEY_STRING_ARRAY();
			}

			return array($ReturnURL, $ReturnX);
		}


		function GENERATE_KEY_STRING_FORM(){

			global $eclass40_httppath,$lib_user;

			$TimeStamp = date("YmdHi");		//YYYYMMDDHHmm

			$ParArray['SysLang'] = $this->SysLang;
			$ParArray['SchoolCode'] = $this->SchoolCode;
			$ParArray['eClassUserType'] = $this->eClassUserType;
			$ParArray['eClassUserID'] = $this->eClassUserID;
			$ParArray['UserNameChi'] = $this->UserNameChi;
			$ParArray['UserNameEng'] = $this->UserNameEng;
			$ParArray['UserNickname'] = $this->UserNickname;
			$ParArray['ClassLevel'] = $this->ClassLevel;
			$ParArray['ClassName'] = $this->ClassName;
			$ParArray['ClassNumber'] = $this->ClassNumber;
			
			
			if($this->project=='LER') 
			{
				global $BroadlearningClientName;
				$ParArray['UserEmail'] = $this->UserEmail;
				$ParArray['ASPCourseID'] = $this->ASPCourseID;
				$ParArray['SchoolName'] = $BroadlearningClientName;
				
			}else if(strpos(strtolower($this->project),'grammartouch') !== false){
				$ParArray['UserEmail'] = $this->UserEmail;
				$ParArray['AcademicYearID'] = $_SESSION['CurrentSchoolYearID'];
			}else if($this->project=='FlipChan'){
				$ParArray['UserEmail'] = $this->UserEmail;
			}else if($this->project=='PowerVoice'){
				$ParArray['UserEmail'] = $this->UserEmail;
				$ParArray['Service'] = $this->project;
				$ParArray['IsFromAdmin'] = (!empty($_SERVER['PHP_AUTH_USER'])&&$_SERVER['PHP_AUTH_USER']=="admin")?1:0;
				$pvoice_target_file = "$intranet_root/file/powervoice.txt";
				$pvoice_data = get_file_content($pvoice_target_file);
				$pvoice_array = unserialize($pvoice_data);	
				$ParArray['PowerVoiceLength'] = $pvoice_array['length']*60;
				$ParArray['PowerVoiceLength'] = ($ParArray['PowerVoiceLength'] == "")?60*60:$ParArray['PowerVoiceLength']; 
			}				
			$ParArray['APIVersion'] = $this->APIVersion[0][0];
			$ParArray['TimeStamp'] = $TimeStamp;

			# added by kelvin (for ebook content)
			$ParArray['SchoolAddress'] = $eclass40_httppath;
			

			$ReturnX = $this->GET_STRING_VALUE_FORM($ParArray);

			return $ReturnX;
		}
		
		
		function GENERATE_KEY_STRING_ARRAY($ParArray=array()){
			global $eclass40_httppath;

			$TimeStamp = date("YmdHi");		//YYYYMMDDHHmm

			$ParArray['SysLang'] = $this->SysLang;
			$ParArray['SchoolCode'] = $this->SchoolCode;
			$ParArray['eClassUserType'] = $this->eClassUserType;
			$ParArray['eClassUserID'] = $this->eClassUserID;
			$ParArray['UserNameChi'] = $this->UserNameChi;
			$ParArray['UserNameEng'] = $this->UserNameEng;
			$ParArray['UserNickname'] = $this->UserNickname;
			$ParArray['ClassLevel'] = $this->ClassLevel;
			$ParArray['ClassName'] = $this->ClassName;
			$ParArray['ClassNumber'] = $this->ClassNumber;
			if($this->project=='LER')
			{
				global $BroadlearningClientName;
				$ParArray['UserEmail'] = $this->UserEmail;
				$ParArray['ASPCourseID'] = $this->ASPCourseID;
				$ParArray['SchoolName'] = $BroadlearningClientName;
				
			}
			$ParArray['APIVersion'] = $this->APIVersion[0][0];
			$ParArray['TimeStamp'] = $TimeStamp;

			# added by kelvin (for ebook content)
			$ParArray['SchoolAddress'] = $eclass40_httppath;
			$ParmValueAppend  = '';
			foreach($ParArray as $ParmName => $ParmValue){
				if($ParmName!='SchoolAddress'){
					$ParmValueAppend .= $this->Delimiter . $ParmValue;
				}
			}

			$ParArray['Key'] = md5($this->KeySalt.$this->Delimiter.$ParmValueAppend);
			
			return $ParArray;
		}


		function GET_STRING_VALUE_FORM($ParArray){
			global $intranet_version;
			if (sizeof($ParArray)>0)
			{
				foreach($ParArray as $ParmName => $ParmValue)
				{
					//$ReturnX .= "<input type='hidden' name=\"".$ParmName."\" value=\"".str_replace('"', "&quot;", $ParmValue)."\" />\n";
					$ReturnX .= "<input type='hidden' name=\"".$ParmName."\" value=\"".rawurlencode($ParmValue)."\" />\n";
					if($ParmName!='SchoolAddress')
					{
						$ParmValueAppend .= $this->Delimiter . $ParmValue;
					}
				}
			}
			if($this->project=='KSK')
			{				
				$lib_user = new libuser($this->eClassUserID);
				$ksk = new ksk();
				if($lib_user->RecordType==1)//teacher
					$SubjectAccess = $ksk->getUserKSKSubject();
				
				$ReturnX .= "<input type='hidden' name=\"UserSubjectAccess\" value=\"".$SubjectAccess."\" />\n";	
				$ReturnX .= "<input type='hidden' name=\"UserEmail\" value=\"".rawurlencode($lib_user->UserEmail)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserTitle\" value=\"".rawurlencode($lib_user->Title)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserFirstName\" value=\"".rawurlencode($lib_user->FirstName)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserLastName\" value=\"".rawurlencode($lib_user->LastName)."\" />\n";
				
				$ReturnX .= "<input type='hidden' name=\"UserGender\" value=\"".rawurlencode($lib_user->Gender)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserICQNo\" value=\"".rawurlencode($lib_user->ICQNo)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserGender\" value=\"".rawurlencode($lib_user->HomeTelNo)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserFaxNo\" value=\"".rawurlencode($lib_user->FaxNo)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserDateOfBirth\" value=\"".rawurlencode($lib_user->DateOfBirth)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserAddress\" value=\"".rawurlencode($lib_user->Address)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserCountry\" value=\"".rawurlencode($lib_user->Country)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserURL\" value=\"".rawurlencode($lib_user->URL)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserInfo\" value=\"".rawurlencode($lib_user->Info)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserTitleChinese\" value=\"".rawurlencode($lib_user->TitleChinese)."\" />\n";
				$ReturnX .= "<input type='hidden' name=\"UserTitleEnglish\" value=\"".rawurlencode($lib_user->TitleEnglish)."\" />\n";
				//debug_R($lib_user);die;
			}
			$ReturnX .= "<input type='hidden' name=\"Key\" value=\"".md5($this->KeySalt.$this->Delimiter.$ParmValueAppend)."\" />\n";

			return $ReturnX;
		}


		# determine whether the school is subscribing LSLP
		function GET_CURRENT_LICENSE($ParSchoolCode=""){
			/*
			$ParSchoolCode = ($ParSchoolCode=="") ? $this->SchoolCode : $ParSchoolCode;
			$YearsDecrypted = trim(str_replace($this->Delimiter, "", str_replace($ParSchoolCode, "", $this->DecryptKey($this->YearlyLicenseKey))));

			# get the start and end dates licensed
			list($LicenseYearStart, $LicenseYearEnd) = split("\-", $YearsDecrypted);
			$LicenseYearStart .= "-".$this->SchoolYearArr['start'];
			$LicenseYearEnd .= "-".$this->SchoolYearArr['end'];
			$Today = date("Y-m-d");
			return ($LicenseYearStart<=$Today && $Today<=$LicenseYearEnd);
			*/

			# to be controlled later when license setting is ready
			return true;
		}


		function GET_STRING_VALUE($ParArray){

			if (sizeof($ParArray)>0)
			{
				foreach($ParArray as $ParmName => $ParmValue)
				{
					if (strstr(strtoupper($ParmName), "NAME"))
					{
						$ParmStringAppend .= "&".$ParmName."=".rawurlencode($ParmValue);
					} else
					{
						$ParmStringAppend .= "&".$ParmName."=".$ParmValue;
					}
					$ParmValueAppend .= $this->Delimiter . $ParmValue;
				}
			}
			$ParmStringAppend = ltrim($ParmStringAppend, "\&");
			$ParmValueAppend = $this->KeySalt . $this->Delimiter . $ParmValueAppend;

			return array($ParmStringAppend, $ParmValueAppend);
		}



		function AUTHENTICATE($ParArray, $ParKey){

			$ReturnVal = false;

			list($ParmStringAppend, $ParmValueAppend) = $this->GET_STRING_VALUE($ParArray);

			$KeyCandidate = md5($ParmValueAppend);

			if ($ParKey==$KeyCandidate)
			{
				$TimeStampNow = date("YmdHi");		//YYYYMMDDHHmm
				$TimeStampDifference = abs($TimeStampNow - $ParArray['TimeStamp']);

				// validate by checking the hash of parameters V.S. $Key
				if ($TimeStampDifference<=$this->TimeThreshold)
				{
					$ReturnVal = true;
				} else
				{
					echo "Key is expired for " . $TimeStampDifference . " minute(s)!";
				}
			}

			return $ReturnVal;
		}





		# Encrypt Password
		function EncryptKey($my_key){
			$cut_size = 5;

			$my_key = "{".$my_key."}";
			$encrypt1 = base64_encode($my_key);
			$shifted = "";
			for ($i=0; $i<strlen($encrypt1); $i++)
			{
				$shifted .= chr(ord(substr($encrypt1, $i, 1)) + 3);
			}
			$encrypt2 = base64_encode($shifted);

			$cut_total = ceil(strlen($encrypt2)/$cut_size);
			$Key = "";
			for ($i=0; $i<$cut_total; $i++)
			{
				$Key .= (($Key!="")?"-":"").substr($encrypt2, $i*$cut_size, $cut_size);
			}

			return $Key;
		}


		# Decrypt Password
		function DecryptKey($my_key){
			$cut_size = 5;

			$cut_total = ceil(strlen($my_key)/($cut_size+1));
			$Key = "";
			for ($i=0; $i<$cut_total; $i++)
			{
				$Key .= substr($my_key, $i*($cut_size+1), $cut_size);
			}

			$decrypt1 = base64_decode($Key);
			$shifted = "";
			for ($i=0; $i<strlen($decrypt1); $i++)
			{
				$shifted .= chr(ord(substr($decrypt1, $i, 1)) - 3);
			}
			$decrypt2 = base64_decode($shifted);

			return substr($decrypt2, 1, strlen($decrypt2)-2);
		}

}
?>