<?
abstract class importData{
	public $objImportDefinition = null;
	public $dataArray = null;

	protected $objExtraImportData = null; // a object of importData
	protected $importErrorAry = null;
	protected $invalidRecord = array();
	protected $validRecord = array();

	public function importData(){

	}

	public function setExtraImportData($objExtraImportData){
		$this->objExtraImportData = $objExtraImportData;
	}

	public function getDataDefinition(){

		if($this->objExtraImportData != null){
			$returnDefinition = array_merge($this->objImportDefinition->getDataDefinition(),$this->objExtraImportData->objImportDefinition->getDataDefinition());

		}else{

			$returnDefinition = $this->objImportDefinition->getDataDefinition();
		}

		return $returnDefinition;
	}
	public function getDataDefaultCsvHeaderArr(){

		$returnDefaultCsvHeaderArr = array();
		if($this->objExtraImportData != null){

			$returnDefaultCsvHeaderArr = array_merge($this->objImportDefinition->getDataDefaultCsvHeaderArr(),$this->objExtraImportData->objImportDefinition->getDataDefaultCsvHeaderArr());

		}else{
			$returnDefaultCsvHeaderArr = $this->objImportDefinition->getDataDefaultCsvHeaderArr();
		}
		
		return $returnDefaultCsvHeaderArr;

	}
	public function getDataColumnPropertyArr(){
		$returnDataColumnPropertyArr = array();
		if($this->objExtraImportData != null){

			$returnDataColumnPropertyArr = array_merge($this->objImportDefinition->getDataColumnPropertyArr(),$this->objExtraImportData->objImportDefinition->getDataColumnPropertyArr());

		}else{
			$returnDataColumnPropertyArr = $this->objImportDefinition->getDataColumnPropertyArr();
		}


		return $returnDataColumnPropertyArr;
	}
	public function setImportDataSource($dataArray){
		if($this->objExtraImportData != null){
			$this->objExtraImportData->setImportDataSource($dataArray);
		}
		$this->dataArray = $dataArray;
	}

	public function getImportDataSource(){
		return $this->dataArray;
	}
	public function checkImportData(){
		$returnError = array();
		$returnErrorExtra = array();
		
		$this->checkImportDataDetails();
		$returnError = $this->getImportError();

		if($this->objExtraImportData != null) {
			$this->objExtraImportData->checkImportDataDetails();
			$returnErrorExtra = $this->objExtraImportData->getImportError();
		}

		if(count($returnErrorExtra) > 0){
			$combineErrorAry = array_merge(array($returnError) , array($returnErrorExtra));

			$returnArray = array();

			//LOOP EACH IMPORT ONE BY ONE
			foreach ($combineErrorAry as $_errorForEachImport => $_lineNoErrorDetails){
				//LOOP EACH LINE ERROR FOR EACH IMPORT
				foreach($_lineNoErrorDetails as $_lineNumber=> $_lineError){
					$previousError = array(); //error return from first round checking
					$combineError = array();

					if($returnArray[$_lineNumber] == null){
						$returnArray[$_lineNumber] = $_lineError;
					}else{
						$previousError = $returnArray[$_lineNumber]['ERROR'];
						$thisError = $_lineError['ERROR'];
						$combineError = array_merge($previousError,$thisError);
						$returnArray[$_lineNumber]['ERROR'] = $combineError;
					}
				}
			}
			$this->importErrorAry = $returnArray;
		}else{
			$this->importErrorAry = $returnError;
		}

//debug_r($this->importErrorAry);

		// ARRAY ($this->importErrorAry) MUST HAS VALUE EVEN THERE IS NO ERROR AFTER THE CHECKING
		foreach($this->importErrorAry as $_lineNo => $_lineNoError){
				$tmp = explode("_", $_lineNo);  
				//the second element of $tmp is line number with integer
				$lineNumber = intval($tmp[1]);
				if(count($_lineNoError['ERROR']) > 0) {
					$this->invalidRecord[] = $lineNumber;
				}else{
					$this->validRecord[] = $lineNumber;
				}

		}
	}
	abstract function checkImportDataDetails();

	public function processImportData(){
		
		$this->processImportDataDetails();
		if($this->objExtraImportData != null) {
			$this->objExtraImportData->processImportData();
		}
	}
	abstract function processImportDataDetails();

	protected function registerError($errorCode,$errorMsg ,&$error){
		$error[$errorCode] = $errorMsg;
	}

	protected function pushToError($line , $errorItems){

		$this->importErrorAry['line_'.$line]['ERROR'] = $errorItems;

/*
		if(count($errorItems)>0){
			$this->invalidRecord[] = $line;
		}else{
			$this->validRecord[] = $line;
		}
*/		
	}
	public function getImportError(){
//		debug_r($this->importErrorAry);
		return $this->importErrorAry;
	}
	public function getNoOfValidRecord(){
		return count($this->validRecord);
	}
	public function getNoOfInvalidRecord(){
		return count($this->invalidRecord);
	}

	public function dataHandle($dataArray , $DefaultCsvHeaderArr,$ColumnPropertyArr){
		$returnRecord = array();

		foreach ($dataArray as $imported_record) {

				// Ensure that the number of elements in the record is the same as the number of fields, as required by array_combine().
				$imported_record = array_pad($imported_record, count($DefaultCsvHeaderArr), null);
				array_splice($imported_record, count($DefaultCsvHeaderArr));

				// Change the record format from array(value1, value2, ...) to array(field1 => value1, field2 => value2, ...)
				$returnRecord[] = array_combine($DefaultCsvHeaderArr, $imported_record);
		}
		
		$tmp = array_shift($returnRecord);	//remove the first line (CSV TITLE) of the data ary
		return $returnRecord;
}

}
?>