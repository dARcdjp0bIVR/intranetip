<?
abstract class importDataDefinition{

	//definiation field
	protected $field = null;

	//please don't change this value unless you know how to config
	protected $cfgFieldTypeValue = array(
		'is_mandatory' => 1,
		'is_reference' => 2,
		'is_optional' => 3,
	);
	
	/**
	 * Return the definitions of the fields.
	 */	
	public function getDataDefinition() {
		return $this->fields;
	}

	public function getDataDefaultCsvHeaderArr(){
		$DefaultCsvHeaderArr = array();
		$thisFields = $this->getDataDefinition();
		foreach ($thisFields as $fieldName => $fieldDetails){
			$DefaultCsvHeaderArr[] = $fieldName;
		}
		return $DefaultCsvHeaderArr;
	}

	public function getDataColumnPropertyArr(){
		$ColumnPropertyArr = array();
		$thisFields = $this->getDataDefinition();
		foreach ($thisFields as $fieldName => $fieldDetails){
			$data_mandatory = $fieldDetails['data_mandatory'];			
			$ColumnPropertyArr[] = ($data_mandatory == $this->cfgFieldTypeValue['is_reference']) ? 0 : 1 ;
		}
		return $ColumnPropertyArr;
	}

}
?>