<?php
// Editing by 

$eClassAPIConfig = array();
$eClassAPIConfig['moduleCode'] = "eClassAPI";
$eClassAPIConfig['settings'] = array("eClassAPIOpenUse","eClassAPIPassword");

$eClassAPIConfig['MaxNumberOfRecord'] = 1000;

$eClassAPIConfig['errorSettings'] = array();
$eClassAPIConfig['errorSettings'][1] = "Unauthorized Access.";
$eClassAPIConfig['errorSettings'][2] = "Incorrect API Password.";
$eClassAPIConfig['errorSettings'][3] = "API Service Closed.";
$eClassAPIConfig['errorSettings'][4] = "Group(s) have not been found. ";
$eClassAPIConfig['errorSettings'][5] = "Subject Group(s) have not been found. ";
$eClassAPIConfig['errorSettings'][6] = "Subject(s) have not been found. ";
$eClassAPIConfig['errorSettings'][7] = "eClass Course(s) have not been found. ";
$eClassAPIConfig['errorSettings'][8] = "Assessment(s) have not been found. ";
$eClassAPIConfig['errorSettings'][9] = "Year Class(s) have not been found. ";

$eClassAPIConfig['AnnouncementType']['School'] = 'S';
$eClassAPIConfig['AnnouncementType']['Group'] = 'G';

$eClassAPIConfig['EventType']['SchoolEvent'] ='0';
$eClassAPIConfig['EventType']['AcademicEvent'] ='1';
$eClassAPIConfig['EventType']['GroupEvent'] ='2';
$eClassAPIConfig['EventType']['PublicHoliday'] ='3';
$eClassAPIConfig['EventType']['SchoolHoliday'] ='4';



?>