<?php
// Editing by Pun
/****************************************
 * 2020-03-05 Ray
 * - add getAnnouncement(), getAnnouncementXml() website code
 * 2019-08-19 Pun
 * - Modified getDigitalChannelsAlbum(), getDigitalChannelsXML(), added $albumTarget, $limit, $page
 * 2019-08-19 Pun
 * - Modified getAnnouncement(), fixed cannot get file name for some chinese character
 * 2019-08-05 Pun
 * - Modified getAnnouncement(), fixed does not order by title if same date
 * 2019-05-30 Pun
 * - Modified getAnnouncement(), getAnnouncementXML(), added $announcementIds, $page and $countResult
 * 2019-03-29 (Anna)
 * - Modified getAnnouncement(), added $sys_custom['SchoolNews']['DisplayEngTitleAndContent']
 * 2019-03-01 (Pun)
 * - Added getSubjectGroupXML(), getSubjectGroup()
 * 2019-02-11 (Pun)
 * - modified getAnnouncement(), getEvent(), getDigitalChannelsAlbum(), getDigitalChannelsXML(), getSchoolInfo(), getChildSchoolInfoList(), added SFOC cust
 * 2019-01-28 (Anna)
 * - modified getMedalListRecord(), added WebsiteCode
 * 2019-01-09 (Cameron)
 *  - add parameter $albumCode to getDigitalChannelsXML(), getDigitalChannelsCategory(), getDigitalChannelsAlbum()
 * 2018-12-11 (Pun) [ip.2.5.10.1.1]
 *  - Modified getAnnouncementXml(), getAnnouncement(), added keyword
 * 2018-10-23 (Isaac)
 *  - added utf8_for_xml() to tackle the invalid Char value issue and applied to all xml output
 * 2018-07-24 (Pun) [ip.2.5.9.10.1]
 *   - Added getYearClassXML(), getYearClass()
 * 2018-06-14 (Pun) [ip.2.5.9.7.1]
 *  - Modified getAnnouncementXml(), getAnnouncement(), added advanced date control
 * 2018-06-05 (Pun): Modified getAnnouncement(), fixed does not return Attachments field while only one attachment
 * 2018-03-28 (Anna): Modified getDigitalChannelsXML(), getDigitalChannelsAlbum(), added SFOC cust
 * 2018-03-09 (Anna): Added getSchoolInfoXml(), getSchoolInfo()
 * 2017-10-23 (Pun): Modified getAnnouncement(), added Attachments tag for result
 * 2017-10-23 (Pun): Fixed PHP5.4 split() problem
 * 2017-10-16 (Carlos): Modified getAnnouncement(), added api_key parameter to view multiple attachments url /home/view_files.php to allow public access.
 * 2017-04-05 (Pun):
 *   Modified getDigitalChannelsAlbum(), getDigitalChannelsXML(), added return categoryCode for album
 *   Modified getDigitalChannelsAlbum(), getDigitalChannelsXML(), added return sequence
 * 2017-03-23 (Pun):
 *   Modified getDigitalChannelsCategory(), added return album target, fixed coverphoto does not initialize
 *   Modified getDigitalChannelsXML(), added return category display order
 * 2017-03-09 (Pun):
 *   Added getAcademicYearTermXML(), getAcademicYearTerm()
 *   Modified getDigitalChannelsCategory() and getDigitalChannelsAlbum(), fixed year filter read wrong field
 * 2017-03-02 (Pun): Modified getDigitalChannelsAlbum(), getDigitalChannelsXML() added SharedSince, SharedUntil, Target field for return xml
 * 2017-03-01 (Pun): Modified getDigitalChannelsAlbum(), getDigitalChannelsXML() added Date field for return xml
 * 2016-11-18 (Ronald): Add function getDigitalChannelsCategory(),getDigitalChannelsAlbum(), and getDigitalChannelsXML() for digital Channels API
 * 2015-09-01 (Carlos): Modified getNotice() and getNoticeXml(), added advance date filter paramters.
 * 2014-10-29 (Carlos): Modified getAnnouncement() and getNotice() sorting order follow admin page default sorting order
 * 2014-09-16 (Carlos): Modified getNotice() and getAnnouncement() date filter conditions
 * 2014-06-19 (Carlos): Modified getIntranetHomeworkBySubjectGroup() and getAssessment() date conditions
 * 2014-04-28 (Carlos): Added getNotice() and getNoticeXml()
 * 2013-03-04 (Carlos): Created
 * 2013-03-07 (Rita)  : Added intranet events api
 * 2013-03-18 (Carlos): Revised for providing both English and Chinese Name/Group Name
 * 						intranet homework added yearClassId
 ******************************************/
if (!defined("LIBECLASS_API_DEFINED")) {
	define("LIBECLASS_API_DEFINED", true);

	$JustWantVersionData = true;

	include_once("eClassAPIConfig.inc.php");
	include_once("$intranet_root/includes/libdb.php");
	include_once("$intranet_root/includes/libgeneralsettings.php");
	include_once("$intranet_root/includes/version.php");
	include_once("$intranet_root/includes/libfilesystem.php");
	include_once("$intranet_root/includes/libxml.php");

	class libeclass_api extends libgeneralsettings
	{
		var $eclassApiSettings = array();
		var $libxml = false;
		var $libfilesystem = false;
		var $errorAry = array();

		function libeclass_api()
		{
			global $eClassAPIConfig;

			$this->libgeneralsettings();

			$settingValuesAry = array();
			for($i=0;$i<count($eClassAPIConfig['settings']);$i++) {
				$settingValuesAry[] = "'".$eClassAPIConfig['settings'][$i]."'";
			}

			$this->eclassApiSettings = $this->Get_General_Setting($eClassAPIConfig['moduleCode'],$settingValuesAry);
		}

		// get setting by name
		function geteClassApiSettings($settingName)
		{
			return $this->eclassApiSettings[$settingName];
		}

		// get libxml() instance
		function getInstanceLibXml()
		{
			if($this->libxml === false) {
				$this->libxml = new libxml();
			}

			return $this->libxml;
		}

		// get libfilesystem() instance
		function getInstanceLibFilesystem()
		{
			if($this->libfilesystem === false) {
				$this->libfilesystem = new libfilesystem();
			}

			return $this->libfilesystem;
		}

		function isEJ()
		{
			global $versions, $JustWantVersionData;

			$is_ej = false;
			$ver = $versions[0][0];
			if(strpos($ver,"ej") !== false) {
				$is_ej = true;
			}
			return $is_ej;
		}

		// is eClass API open for public use
		function isOpenForUse()
		{
			global $eClassAPIConfig;

			return $this->eclassApiSettings[$eClassAPIConfig['settings'][0]]=='1';
		}

		// check API password
		function authenticatePassword($password)
		{
			global $eClassAPIConfig;

			$password = trim($password);
			return ($password != '' && $this->eclassApiSettings[$eClassAPIConfig['settings'][1]]==$password);
		}

		// check access right and output error xml if anything wrong
		function authErrorExit($params)
		{
			global $plugin;

			$api_password = trim($params['api_password']);

			if(!$this->isOpenForUse()) {
				$this->addError(3);
				$xml = $this->getErrorXml();
				echo $xml;
				intranet_closedb();
				exit;
			}

			if($api_password == '' || !$plugin['eClassAPI']){
				$this->addError(1);
				$xml = $this->getErrorXml();
				echo $xml;
				intranet_closedb();
				exit;
			}

			if(!$this->authenticatePassword($api_password)) {
				$this->addError(2);
				$xml = $this->getErrorXml();
				echo $xml;
				intranet_closedb();
				exit;
			}
		}

		// append error to buffer array
		function addError($errorCode, $errorDetail="")
		{
			global $eClassAPIConfig;

			if(isset($eClassAPIConfig['errorSettings'][$errorCode])) {
				$this->errorAry[] = array('Error' => array('ErrorCode'=>$errorCode,
															'ErrorDetail'=>$eClassAPIConfig['errorSettings'][$errorCode].($errorDetail != ""? $errorDetail:""))
										 );
			}
		}

		// clear error buffer array
		function removeAllError()
		{
			$this->errorAry = array();
		}

		// count number of error in buffer array
		function errorCount()
		{
			return count($this->errorAry);
		}

		/*
		 * <eClassResponse>
		 *		<Errors>
		 *			<Error>
		 *				<ErrorCode></ErrorCode>
		 *				<ErrorDetail></ErrorDetail>
		 *			</Error>
		 *		</Errors>
		 *	</eClassResponse>
		 */
		function getErrorXml()
		{
			$libxml = $this->getInstanceLibXml();

			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			$xml.= $libxml->XML_OpenElement('Errors');
			$error_count = count($this->errorAry);
			for($i=0;$i<$error_count;$i++) {
				$xml .= $libxml->Array_To_XML($this->errorAry[$i], true);
			}
			$xml.= $libxml->XML_CloseElement('Errors');
			$xml.= $this->getXmlRootCloseNode();

			return $xml;
		}

		function outputHttpXmlHeader($charset="")
		{
			if($charset == "") {
				$charset = returnCharset();
			}

			header("Content-type: text/xml;charset=$charset");
		}

		function getXmlHeader($charset="")
		{
			if($charset == "") {
				$charset = returnCharset();
			}
			$charset = strtoupper($charset);

			$x = '<?xml version="1.0" encoding="'.$charset.'"?>';

			return $x;
		}

		function getXmlRootOpenNode()
		{
			return '<eClassResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
		}

		function getXmlRootCloseNode()
		{
			return '</eClassResponse>';
		}

		/*
		 * [Get announcement data]
		 * @param string $announcementType : 'S' - School announcement, 'G' - Group announcement, '' - all type of announcement
		 * @param array|int|string $groupId : only useful for group announcement, i.e. when $announcementType is 'G'
		 * @param string $startDate : start date boundary
		 * @param string $endDate : end date boundary
		 * @param string|int $order : '1' or '' sort in descending order, '0' sort in ascending order
		 * @param string|int $noOfRecord : maximum number of records to get. If not set, get as many as possible
		 * @return array with number index
		 */
		function getAnnouncement($announcementType="",$groupId="",$startDate="",$endDate="",$order="1",$noOfRecord="", $advanceDateControl=0,$startBefore="",$startAfter="",$endBefore="",$endAfter="", $keyword="", $page='', $countResult="", $announcementIds="",$api_website_code="")
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language, $sys_custom;

			$announcementIds = (array)$announcementIds;

			$lfs = $this->getInstanceLibFilesystem();

			//$is_EJ = $this->isEJ();

			// school domain for attachment url
			$school_url = explode("\n",get_file_content("$intranet_root/file/email_website.txt"));
			$school_domain = $school_url[0];

			if(substr($school_domain,-1)=="/") {
				$school_domain = substr($school_domain,0,-1);
			}

			$returnAry = array();
			//$name_field = getNameFieldWithClassNumberByLang("u."); // announcement creator name
			$conds = "";
			// date period
			if($advanceDateControl == 1)
			{
				if($startBefore != ''){
					if(strlen($startBefore) <= 10 ){
					    $startBefore = "{$startBefore} 23:59:59";
					}
					$conds .= " AND a.AnnouncementDate <= '$startBefore' ";
				}
				if($startAfter != ''){
					if(strlen($startAfter) <= 10 ){
					    $startAfter = "{$startAfter} 00:00:00";
					}
					$conds .= " AND a.AnnouncementDate >= '$startAfter' ";
				}
				if($endBefore != ''){
					if(strlen($endBefore) <= 10 ){
					    $endBefore = "{$endBefore} 23:59:59";
					}
					$conds .= " AND a.EndDate <= '$endBefore' ";
				}
				if($endAfter != ''){
					if(strlen($endAfter) <= 10 ){
					    $endAfter = "{$endAfter} 00:00:00";
					}
					$conds .= " AND a.EndDate >= '$endAfter' ";
				}
			}else{
    			if($startDate!="" && $endDate!=""){
    				$startDate = date("Y-m-d", strtotime($startDate));
    				$endDate = date("Y-m-d", strtotime($endDate));

    				$conds .= " AND (";
    				$conds .= " (a.AnnouncementDate>='$startDate 00:00:00' AND a.AnnouncementDate<='$endDate 23:59:59') ";
    				$conds .= " OR (a.EndDate>='$startDate 00:00:00' AND a.EndDate<='$endDate 23:59:59') ";
    				$conds .= ") ";
    			}else if($startDate!="" || $endDate!="") {
    				if($startDate != "") {
    				 	$startDatetime = date("Y-m-d",strtotime($startDate));
    				 	$conds .= " AND DATE_FORMAT(a.AnnouncementDate,'%Y-%m-%d')<='$startDatetime' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$startDatetime' ";
    				}
    				if($endDate != "") {
    					$endDatetime = date("Y-m-d",strtotime($endDate));
    					$conds .= " AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')<='$endDatetime' ";
    				}
                	//$conds .= " AND (DATE_FORMAT(a.AnnouncementDate,'%Y-%m-%d') BETWEEN '$startDatetime' AND '$endDatetime')";
                } else {
    	            $conds .= " AND (a.EndDate is null OR CURDATE()>=DATE_FORMAT(a.AnnouncementDate,'%Y-%m-%d')) ";
                }
            }
            if($keyword){
                if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
                    $conds .= " AND (a.Title LIKE '%{$keyword}%' OR a.TitleEng LIKE '%{$keyword}%' OR a.Description LIKE '%{$keyword}%' OR a.DescriptionEng LIKE '%{$keyword}%')";
                }else{
                    $conds .= " AND (a.Title LIKE '%{$keyword}%' OR a.Description LIKE '%{$keyword}%')";
                }
            }

			if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
				if($api_website_code != '') {
					$conds .= " AND (a.WebsiteCode='$api_website_code')";
				}
			}

            // sort order
			$cond_order = $order=="1" || $order=="" ? "DESC" : "ASC";
			// max number of record
			$cond_limit = "";
			if($noOfRecord != "" && $noOfRecord > 0) {
				$cond_limit = " LIMIT $noOfRecord";

				if($page != "" && $page > 0) {
				    $offset = $noOfRecord * $page;
    				$cond_limit .= " OFFSET $offset";
    			}
			}

			if ($plugin['power_voice'])
            {
            	$extra_field = ", a.VoiceFile";
        	}
        	else
        	{
            	$extra_field = "";
        	}

			if($announcementType == $eClassAPIConfig['AnnouncementType']['School']) {
				// only want school announcement, exclude group announcement
				$sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
            	$AnnouncementIDs = $this->returnVector($sql);
            	if(count($AnnouncementIDs) > 0){
            		$conds = " AND a.AnnouncementID NOT IN (".implode(",",$AnnouncementIDs).") ".$conds;
            	}
            	$type_field = "'".$eClassAPIConfig['AnnouncementType']['School']."'";
			}
			if($announcementType == "") {
				$join_announcement_group_table = "LEFT JOIN INTRANET_GROUPANNOUNCEMENT as ga ON ga.AnnouncementID=a.AnnouncementID
													LEFT JOIN INTRANET_GROUP as ig ON ig.GroupID=ga.GroupID ";
				$type_field = "IF(ga.AnnouncementID IS NULL,'".$eClassAPIConfig['AnnouncementType']['School']."','".$eClassAPIConfig['AnnouncementType']['Group']."')";
				$extra_field .= ", GROUP_CONCAT(ga.GroupID) as GroupID, GROUP_CONCAT(ig.Title) as GroupTitleEN, GROUP_CONCAT(ig.TitleChinese) as GroupTitleCH ";
			}
			if($announcementType == $eClassAPIConfig['AnnouncementType']['Group']) {
				$join_announcement_group_table = "INNER JOIN INTRANET_GROUPANNOUNCEMENT as ga ON ga.AnnouncementID=a.AnnouncementID
													LEFT JOIN INTRANET_GROUP as ig ON ig.GroupID=ga.GroupID ";
				$type_field = "'".$eClassAPIConfig['AnnouncementType']['Group']."'";
				if($groupId != "") {
					$groupIdAry = array();
					if(is_array($groupId) && count($groupId)>0) {
						$conds .= " AND ga.GroupID IN (".implode(",",$groupId).") ";
						$groupIdAry = array_values(array_unique($groupId));
					}else if(is_string($groupId) && strstr($groupId,",")!== false ) { // groupId in csv format
						$conds .= " AND ga.GroupID IN (".$groupId.") ";
						$tmpGroupIdAry = explode(",",$groupId);
						for($i=0;$i<count($tmpGroupIdAry);$i++) {
							$tmpGroupIdAry[$i] = trim($tmpGroupIdAry[$i]);
						}
						$groupIdAry = array_values(array_unique($tmpGroupIdAry));
					}else {
						$conds .= " AND ga.GroupID='$groupId' ";
						$groupIdAry[] = $groupId;
					}
					// checking valid groupIDs
					if(count($groupIdAry) > 0) {
						$sql = "SELECT DISTINCT GroupID FROM INTRANET_GROUP WHERE GroupID IN (".implode(",",$groupIdAry).")";
						$foundGroupIdAry = $this->returnVector($sql);

						if(count($foundGroupIdAry) != count($groupIdAry)) {
							$this->addError(4,"Invalid GroupID: ".implode(", ",array_diff($groupIdAry,$foundGroupIdAry)));
						}
					}
				}

				$extra_field .= ", GROUP_CONCAT(ga.GroupID) as GroupID, GROUP_CONCAT(ig.Title) as GroupTitleEN,GROUP_CONCAT(ig.TitleChinese) as GroupTitleCH ";
			}
			if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
			    $extra_field .= ", a.TitleEng AS TitleEn, a.DescriptionEng AS DescriptionEn";
			}

			#### Get AnnouncementID order START ####
			$sql = "SELECT
						a.AnnouncementID
					FROM INTRANET_ANNOUNCEMENT as a $join_announcement_group_table
					LEFT JOIN INTRANET_GROUP as g ON a.OwnerGroupID=g.GroupID
					WHERE a.RecordStatus='1' $conds
					ORDER BY a.AnnouncementDate $cond_order";
			$announcementIdArr = $this->returnVector($sql);
			#### Get AnnouncementID order END ####

			$sql = "SELECT
						a.AnnouncementID,
						a.Title,
						a.Description,
						DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d') as AnnouncementDate,
						DATE_FORMAT(a.EndDate, '%Y-%m-%d') as AnnouncementEndDate,
						$type_field as AnnouncementType,
						u.EnglishName as PosterNameEN,
						u.ChineseName as PosterNameCH,
						g.Title as OwnerGroupTitleEN,
						g.TitleChinese as OwnerGroupTitleCH,
						a.Attachment
						$extra_field
					FROM INTRANET_ANNOUNCEMENT as a $join_announcement_group_table
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.UserID
					LEFT JOIN INTRANET_GROUP as g ON a.OwnerGroupID=g.GroupID
					WHERE a.RecordStatus='1' $conds
					GROUP BY a.AnnouncementID
					ORDER BY a.AnnouncementDate $cond_order, a.Title $cond_order $cond_limit";
			$dataAry = $this->returnResultSet($sql);
			$data_count = count($dataAry);

			if($countResult){
			    return $data_count;
			}

			for($i=0;$i<$data_count;$i++) {
				$announcement_id = $dataAry[$i]['AnnouncementID'];
				if($announcementIds && !in_array($announcement_id, $announcementIds)){
				    continue;
				}

				$attachment_folder = $dataAry[$i]['Attachment'];
				$voice_file = $dataAry[$i]['VoiceFile'];

				$poster_name_en = trim($dataAry[$i]['PosterNameEN']);
				$poster_name_ch = trim($dataAry[$i]['PosterNameCH']);
				$owner_group_title_en = trim($dataAry[$i]['OwnerGroupTitleEN']);
				$owner_group_title_ch = trim($dataAry[$i]['OwnerGroupTitleCH']);
				$group_id = trim($dataAry[$i]['GroupID']); // group ID in csv format
				$group_title_en = trim($dataAry[$i]['GroupTitleEN']); // group names in csv format
				$group_title_ch = trim($dataAry[$i]['GroupTitleCH']);

				$description = $dataAry[$i]['Description'];
				$descriptionEn = $dataAry[$i]['DescriptionEn'];
				//$description = str_replace("&nbsp;", " ", strip_tags(intranet_undo_htmlspecialchars($dataAry[$i]['Description']))); // IP25

				#### Prev/Next START ####
				$index = array_search($announcement_id, $announcementIdArr);
				$prevAnnouncementId = (int)($announcementIdArr[$index-1]);
				$nextAnnouncementId = (int)($announcementIdArr[$index+1]);
				#### Prev/Next END ####

				$api_key = getEncryptedText($announcement_id);

				$Attachments = array();
				$AttachmentURL = " ";
				if (trim($attachment_folder)!="")
				{
					 $attachment_path = "$file_path/file/announcement/".$attachment_folder.$announcement_id;
					 if (file_exists($attachment_path))
					 {
					 	$attachment_array = $lfs->return_files($attachment_path);
					 	if (sizeof($attachment_array)==1)
					 	{
					 		$AttachmentURL = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($attachment_array[0]);
					 	} elseif (sizeof($attachment_array)>1)
					 	{
					 		# list files for user to view one by one
					 		$Module = "Announcement";
					 		$AttachmentURL = $school_domain."/home/view_files.php?key=".EncryptModuleFilePars($Module, "", "", $announcement_id)."&api_key=".$api_key;
					 	}

				 		foreach((array)$attachment_array as $attachment){
				 		    $Attachments[] = array(
                                get_file_basename($attachment),
				 		        $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($attachment)
				 		    );
				 		}
					 }
				}

				if ($plugin['power_voice'] && $voice_file!="")
				{
					if ($AttachmentURL!="")
					{
						if (sizeof($attachment_array)==1)
						{
					 		# list files for user to view one by one
					 		$Module = "Announcement";
					 		$AttachmentURL = $school_domain."/home/view_files.php?key=".EncryptModuleFilePars($Module, "", "", $announcement_id)."&api_key=".$api_key;
						}
					} else
					{
					 	$AttachmentURL = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($voice_file);
					}
				}

				$returnAry[] = array(
									"AnnouncementID" => $announcement_id,
									"Title" => $dataAry[$i]['Title'],
									"TitleEn" => $dataAry[$i]['TitleEn'],
									"Description" => $description,
									"DescriptionEn" => $descriptionEn,
									"AnnouncementDate" => $dataAry[$i]['AnnouncementDate'],
									"AnnouncementEndDate" => $dataAry[$i]['AnnouncementEndDate'],
									"PosterNameEN" => $poster_name_en,
									"PosterNameCH" => $poster_name_ch,
									"OwnerGroupTitleEN" => $owner_group_title_en,
									"OwnerGroupTitleCH" => $owner_group_title_ch,
									"AnnouncementType" => $dataAry[$i]['AnnouncementType'],
									"GroupID" => $group_id,
									"GroupTitleEN" => $group_title_en,
									"GroupTitleCH" => $group_title_ch,
									"AttachmentURL" => $AttachmentURL,
				                    "Attachments" => $Attachments,
				                    "PrevAnnouncementID" => $prevAnnouncementId,
				                    "NextAnnouncementID" => $nextAnnouncementId,
								);
			}

			return $returnAry;
		}

		/*
		 * 	<eClassResponse>
		 *		<Announcements>
		 *			<Announcement>
		 *				<AnnouncementID></AnnouncementID>
		 *				<Title></Title>
		 *				<Description></Description>
		 *				<AnnouncementDate>YYYY-MM-DD</AnnouncementDate>
		 *				<PosterNameEN></PosterNameEN>
		 *				<PosterNameCH></PosterNameCH>
		 *				<OwnerGroupTitleEN></OwnerGroupTitleEN>
		 *				<OwnerGroupTitleCH></OwnerGroupTitleCH>
		 *				<AnnouncementType>S: School announcement | G: Group announcement</AnnouncementType>
		 *				<GroupID>If have multiple groups, GroupID are separated by comma</GroupID>
		 *				<GroupTitleEN>If have multiple groups, Group Title are separated by comma</GroupTitleEN>
		 *				<GroupTitleCH>If have multiple groups, Group Title are separated by comma</GroupTitleCH>
		 *				<AttachmentURL></AttachmentURL>
		 *			</Announcement>
		 *			...
		 *		</Announcements>
		 *	</eClassResponse>
		 */
		function getAnnouncementXml($announcementType="",$groupId="",$startDate="",$endDate="",$order="1",$noOfRecord="", $advanceDateControl=0,$startBefore="",$startAfter="",$endBefore="",$endAfter="",$keyword="", $page="", $_countResult="", $announcement_id="", $api_website_code="")
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();
			$dataAry = $this->getAnnouncement($announcementType,$groupId,$startDate,$endDate,$order,$noOfRecord, $advanceDateControl,$startBefore,$startAfter,$endBefore,$endAfter,$keyword,$page, $_countResult, $announcement_id, $api_website_code);

			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			// construct announcement xml
			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			$xml.= $libxml->XML_OpenElement('Announcements');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('Announcement');
				//$xml .= $libxml->Array_To_XML($dataAry[$i], true);

				foreach($dataAry[$i] as $key => $value) {
				    $xml .= $libxml->XML_OpenElement($key);
				    if(is_array($value)) {
				        if($key == 'Attachments') {
				            foreach($value as $key2 => $value2) {
				                $xml .= $libxml->XML_OpenElement('Attachment');
				                $xml .= $libxml->XML_OpenElement('FileName');
				                $xml .= $libxml->HandledCharacterForXML($value2[0]);
				                $xml .= $libxml->XML_CloseElement('FileName');
				                $xml .= $libxml->XML_OpenElement('FileURL');
				                $xml .= $libxml->HandledCharacterForXML($value2[1]);
				                $xml .= $libxml->XML_CloseElement('FileURL');
				                $xml .= $libxml->XML_CloseElement('Attachment');
				            }
				        }else {
				            foreach($value as $key2 => $value2) {
				                $xml .= $libxml->XML_OpenElement($key2);
				                $xml .= $libxml->Parse_Array($value2);
				                $xml .= $libxml->XML_CloseElement($key2);
				            }
				        }
				    }else{
				        $xml .= $libxml->HandledCharacterForXML($value);
				    }
				    $xml .= $libxml->XML_CloseElement($key);
				}

				$xml .= $libxml->XML_CloseElement('Announcement');
			}
			$xml.= $libxml->XML_CloseElement('Announcements');
			$xml.= $this->getXmlRootCloseNode();
			$xml = $this->utf8_for_xml($xml);

			return $xml;
		}

		/*
		 * [Get intranet event data]
		 * @param string $eventType : '0' - School Event, '1' - Academic Event, '2' - Group Event, '3' - Public Holiday, '4' - School Holiday, '' - all type of event
		 * @param array|int|string $groupId : only useful for group event, i.e. when $eventType is '2'
		 * @param string $startDate : start date boundary
		 * @param string $endDate : end date boundary
		 * @param string|int $order : '1' or '' sort in descending order, '0' sort in ascending order
		 * @param string|int $noOfRecord : maximum number of records to get. If not set, get as many as possible
		 * @return array with number index
		 */
		function getEvent($eventType='', $groupId='', $startDate='', $endDate='', $order='1', $noOfRecord='')
		{
		    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language,$sys_custom;

			$conds = "";
			$extra_fields = ",'' as GroupID,'' as GroupTitleEN,'' as GroupTitleCH ";

			# Event Type
			if($eventType !=''){
				$conds .= "  AND ie.RecordType IN ($eventType) " ;
			}

			# Event ID
			$groupJoinCond='';
			//if($eventType == $eClassAPIConfig['EventType']['GroupEvent']) {
				if($groupId!=''){
					$groupIdAry = array();
					if(is_array($groupId) && count($groupId)>0) {
						$conds .= " AND e.GroupID IN (".implode(",",$groupId).") ";
						$groupIdAry = array_values(array_unique($groupId));
					}else if(is_string($groupId) && strstr($groupId,",")!== false ) { // groupId in csv format
						$conds .= " AND e.GroupID IN (".$groupId.") ";
						$tmpGroupIdAry = explode(",",$groupId);
						for($i=0;$i<count($tmpGroupIdAry);$i++) {
							$tmpGroupIdAry[$i] = trim($tmpGroupIdAry[$i]);
						}
						$groupIdAry = array_values(array_unique($tmpGroupIdAry));
					}else {
						$conds .= " AND e.GroupID='$groupId' ";
						$groupIdAry[] = $groupId;
					}
					// checking valid groupIDs
					if(count($groupIdAry) > 0) {
						$sql = "SELECT DISTINCT GroupID FROM INTRANET_GROUP WHERE GroupID IN (".implode(",",$groupIdAry).")";
						$foundGroupIdAry = $this->returnVector($sql);

						if(count($foundGroupIdAry) != count($groupIdAry)) {
							$this->addError(4,"Invalid GroupID: ".implode(", ",array_diff($groupIdAry,$foundGroupIdAry)));
						}
					}
				}
				$groupJoinCond = " LEFT JOIN INTRANET_GROUPEVENT as e ON e.EventID=ie.EventID
										LEFT JOIN INTRANET_GROUP as ig ON e.GroupID=ig.GroupID ";
				$extra_fields = ",GROUP_CONCAT(ig.GroupID) as GroupID,GROUP_CONCAT(ig.Title) as GroupTitleEN, GROUP_CONCAT(ig.TitleChinese) as GroupTitleCH ";
			//}

			# Start Date
			if($startDate!="" || $endDate!="") {
				if($startDate!=''){
					$startDatetime = date("Y-m-d",strtotime($startDate));

					$conds .= " AND DATE_FORMAT(ie.EventDate,'%Y-%m-%d')>='$startDatetime' ";
				}
				if($endDate!=''){
					$endDatetime = date("Y-m-d",strtotime($endDate));
					$conds .= " AND DATE_FORMAT(ie.EventDate,'%Y-%m-%d')<='$endDatetime' ";
				}
			}

			# Order By
			$cond_order = $order=="1" || $order=="" ? "DESC" : "ASC";

			# Num Of Max Record
			$cond_limit = "";
			if($noOfRecord != "" && $noOfRecord > 0) {
				$cond_limit = " LIMIT $noOfRecord";
			}
			if($sys_custom['eClassApp']['SFOC']){

			    $SFOCFields = ", ie.EventAssociation , ie.EventStartTime,ie.EventEndTime,ie.Email,ie.ContactNo,ie.Website, ie.EventStatus, sst.SportChiName, sst.SportEngName,CONCAT(IconPath,'.gif') as IconPath,
			        ie.TitleEng AS TitleEn, ie.EventAssociationEng AS EventAssociationEn, ie.EventVenueEng AS EventVenueEn, ie.EventNatureEng AS EventNatureEn, ie.DescriptionEng AS DescriptionEn ";
			    $SFOCTable = "LEFT JOIN SFOC_SPORTS_TYPE AS sst ON sst.SportID = ie.SportType  ";
			}

			//$creator_namefield = getNameFieldByLang("iu.");

			$sql = " SELECT
							ie.EventID,
							ie.Title,
							ie.Description,
							iu.EnglishName as CreatorNameEN,
							iu.ChineseName as CreatorNameCH,
							ie.RecordType as EventType,
							DATE_FORMAT(ie.EventDate,'%Y-%m-%d') as EventDate,
							ie.EventVenue,
							ie.EventNature,
							ie.isSkipCycle
							$extra_fields
                            $SFOCFields
					 FROM INTRANET_EVENT AS ie
					 LEFT JOIN INTRANET_USER as iu ON iu.UserID=ie.InputBy
					 $groupJoinCond
					 $SFOCTable
					 WHERE
						ie.RecordStatus='1'
						$conds
					 GROUP BY
						ie.EventID
					 ORDER BY
						ie.EventDate $cond_order,ie.Title
						$cond_order
						$cond_limit ";
			$dataAry = $this->returnResultSet($sql);
			/*
			$data_count = count($dataAry);

			for($i=0;$i<$data_count;$i++) {
				$thisEventId = $dataAry[$i]['EventID'];
				$thisEventTitle = $dataAry[$i]['Title'];
				$thisDescription = $dataAry[$i]['Description'];
				$thisCreatorName = $dataAry[$i]['CreatorName'];
				$thisEventType = $dataAry[$i]['RecordType'];
				$thisEventDate = $dataAry[$i]['EventDate'];
				$thisEventVenue = $dataAry[$i]['EventVenue'];
				$thisEventNature = $dataAry[$i]['EventNature'];
				$thisisSkipCycle = $dataAry[$i]['isSkipCycle'];
				$thisisSkipSAT = $dataAry[$i]['isSkipSAT'];
				$thisisSkipSUN = $dataAry[$i]['isSkipSUN'];
				$thisGroupId = $dataAry[$i]['GroupID'];
				$thisGroupTitle = $dataAry[$i]['GroupTitle'];
				$returnAry[] = array(
									"EventID" => $thisEventId,
									"Title" => $thisEventTitle,
									"Description" => $thisDescription,
									"EventDate" => $thisEventDate,
									"CreatorName" => $thisCreatorName,
									"EventType" => $thisEventType,
									"EventNature" => $thisEventNature,
									"isSkipCycle" => $thisisSkipCycle,
									"isSkipSAT" => $thisisSkipSAT,
									"isSkipSUN" => $thisisSkipSUN,
									"GroupID" => $thisGroupId,
									"GroupTitle" => $thisGroupTitle
								);
			}
			*/
			return $dataAry;
		}


		/*
		 * 	<eClassResponse>
		 *		<Events>
		 *			<Event>
		 *				<EventID></EventID>
		 *				<Title></Title>
		 *				<Description></Description>
		 *				<CreatorNameEN></CreatorNameEN>
		 *				<CreatorNameCH></CreatorNameCH>
		 *				<EventType>0: School Event | 1: Academic Event | 2: Group Event | 3: Public Holiday | 4: School Holiday</EventType>
		 *				<EventDate>YYYY-MM-DD</EventDate>
		 *				<EventVenue></EventVenue>
		 *				<EventNature></EventNature>
		 *				<isSkipCycle>1:Yes | 0:No</isSkipCycle>
		 *				<GroupID>If have multiple groups, GroupID are separated by comma</GroupID>
		 *				<GroupTitleEN>If have multiple groups, Group Title are separated by comma</GroupTitleEN>
		 *				<GroupTitleCH>If have multiple groups, Group Title are separated by comma</GroupTitleCH>
		 *			</Event>
		 *			...
		 *		</Events>
		 *	 </eClassResponse>
		 */
		function getEventXml($eventType="",$groupId="",$startDate="",$endDate="",$order="1",$noOfRecord="")
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();
			$dataAry = $this->getEvent($eventType,$groupId,$startDate,$endDate,$order,$noOfRecord);

			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			// construct announcement xml
			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			$xml.= $libxml->XML_OpenElement('Events');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('Event');
				$xml .= $libxml->Array_To_XML($dataAry[$i], true);
				$xml .= $libxml->XML_CloseElement('Event');
			}
			$xml.= $libxml->XML_CloseElement('Events');
			$xml.= $this->getXmlRootCloseNode();
			$xml = $this->utf8_for_xml($xml);

			return $xml;
		}

		/*
		 * [Get intranet homework data i.e. eHomework]
		 * @param array|int|string $subjectGroupId : number or number in csv or array of number
		 * @param string $startDate : start date boundary
		 * @param string $dueDate : end date boundary
		 * @param string|int $order : '1' or '' sort in descending order, '0' sort in ascending order
		 * @param string|int $noOfRecord : maximum number of records to get. If not set, get as many as possible
		 * @return array with number index
		 */
		function getIntranetHomeworkBySubjectGroup($yearClassId="",$subjectId="",$subjectGroupId="",$startDate="",$dueDate="",$order="1",$noOfRecord="")
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

			$lfs = $this->getInstanceLibFilesystem();

			// school domain for attachment url
			$school_url = explode("\n",get_file_content("$intranet_root/file/email_website.txt"));
			$school_domain = $school_url[0];

			if(substr($school_domain,-1)=="/") {
				$school_domain = substr($school_domain,0,-1);
			}

			$conds = "";

			$today = date('Y-m-d');
			$yearID = Get_Current_Academic_Year_ID();

			# Current Year Term
			$currentYearTerm = getAcademicYearAndYearTermByDate($today);
			$yearTermID = $currentYearTerm[0];
			if($yearTermID=="")
				$yearTermID = 0;

			$yearClassCond = "";
			$year_class_fields = " '' as YearClassID, '' as YearClassTitleEN, '' as YearClassTitleCH, ";
			if($yearClassId != "") {
				$yearClassIdAry = array();
				if(is_array($yearClassId) && count($yearClassId)>0) {
					$conds .= " AND ycu.YearClassID IN (".implode(",",$yearClassId).") ";
					$yearClassCond = " AND ycu.YearClassID IN (".implode(",",$yearClassId).") ";
					$yearClassIdAry = array_values(array_unique($yearClassId));
				}else if(is_string($yearClassId) && strstr($yearClassId,",")!== false) {
					$conds .= " AND ycu.YearClassID IN (".$yearClassId.") ";
					$yearClassCond = " AND ycu.YearClassID IN (".$yearClassId.") ";
					$tmpYearClassIdAry = explode(",",$yearClassId);
					for($i=0;$i<count($tmpYearClassIdAry);$i++) {
						$tmpYearClassIdAry[$i] = trim($tmpYearClassIdAry[$i]);
					}
					$yearClassIdAry = array_values(array_unique($tmpYearClassIdAry));
				}else {
					$conds .= " AND ycu.YearClassID='$yearClassId' ";
					$yearClassCond = " AND ycu.YearClassID='$yearClassId' ";
					$yearClassIdAry[] = $yearClassId;
				}
				if(count($yearClassIdAry) > 0) {
					$sql = "SELECT DISTINCT YearClassID FROM YEAR_CLASS WHERE YearClassID IN (".implode(",",$yearClassIdAry).")";
					$foundYearClassIdAry = $this->returnVector($sql);

					if(count($foundYearClassIdAry) != count($yearClassIdAry)) {
						$this->addError(9,"Invalid YearClassID: ".implode(", ",array_diff($yearClassIdAry,$foundYearClassIdAry)));
					}
				}
				$year_class_fields = " yc.YearClassID,
										yc.ClassTitleEN as YearClassTitleEN,
										yc.ClassTitleB5 as YearClassTitleCH, ";
				$year_class_join_table = " INNER JOIN SUBJECT_TERM_CLASS_USER as tcu ON tcu.SubjectGroupID=h.ClassGroupID
											INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=tcu.UserID $yearClassCond
											INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID ";
			}
		/*	else {
				$year_class_fields = " GROUP_CONCAT(yc.YearClassID) as YearClassID,
										GROUP_CONCAT(yc.ClassTitleEN) as YearClassTitleEN,
										GROUP_CONCAT(yc.ClassTitleB5) as YearClassTitleCH, ";
				$year_class_join_table = " LEFT JOIN SUBJECT_TERM_CLASS_USER as tcu ON tcu.SubjectGroupID=h.ClassGroupID
											LEFT JOIN YEAR_CLASS_USER ycu ON ycu.UserID=tcu.UserID $yearClassCond
											LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID ";
			}
		*/
			if($subjectId != "") {
				$subjectIdAry = array();
				if(is_array($subjectId) && count($subjectId)>0) {
					$conds .= " AND s.RecordID IN (".implode(",",$subjectId).") ";
					$subjectIdAry = array_values(array_unique($subjectId));
				}else if(is_string($subjectId) && strstr($subjectId,",")!== false) {
					$conds .= " AND s.RecordID In (".$subjectId.") ";
					$tmpSubjectIdAry = explode(",",$subjectId);
					for($i=0;$i<count($tmpSubjectIdAry);$i++) {
						$tmpSubjectIdAry[$i] = trim($tmpSubjectIdAry[$i]);
					}
					$subjectIdAry = array_values(array_unique($tmpSubjectIdAry));
				}else {
					$conds .= " AND s.RecordID='$subjectId' ";
					$subjectIdAry[] = $subjectId;
				}
				if(count($subjectIdAry) > 0) {
					$sql = "SELECT DISTINCT RecordID FROM ASSESSMENT_SUBJECT WHERE RecordID IN (".implode(",",$subjectIdAry).")";
					$foundSubjectIdAry = $this->returnVector($sql);

					if(count($foundSubjectIdAry) != count($subjectIdAry)) {
						$this->addError(6,"Invalid SubjectID: ".implode(", ",array_diff($subjectIdAry,$foundSubjectIdAry)));
					}
				}
			}

			if($subjectGroupId != "") {
				$subjectGroupIdAry = array();
				if(is_array($subjectGroupId) && count($subjectGroupId)>0) {
					$conds .= " AND c.SubjectGroupID IN (".implode(",",$subjectGroupId).") ";
					$subjectGroupIdAry = array_values(array_unique($subjectGroupId));
				}else if(is_string($subjectGroupId) && strstr($subjectGroupId,",")!== false ) { // SubjectGroupId in csv format
					$conds .= " AND c.SubjectGroupID IN (".$subjectGroupId.") ";
					$tmpSubjectGroupIdAry = explode(",",$subjectGroupId);
					for($i=0;$i<count($tmpSubjectGroupIdAry);$i++) {
						$tmpSubjectGroupIdAry[$i] = trim($tmpSubjectGroupIdAry[$i]);
					}
					$subjectGroupIdAry = array_values(array_unique($tmpSubjectGroupIdAry));
				}else {
					$conds .= " AND c.SubjectGroupID='$subjectGroupId' ";
					$subjectGroupIdAry[] = $subjectGroupId;
				}
				// checking valid subjectGroupIDs
				if(count($subjectGroupIdAry) > 0) {
					$sql = "SELECT DISTINCT SubjectGroupID FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID IN (".implode(",",$subjectGroupIdAry).")";
					$foundSubjectGroupIdAry = $this->returnVector($sql);

					if(count($foundSubjectGroupIdAry) != count($subjectGroupIdAry)) {
						$this->addError(5,"Invalid SubjectGroupID: ".implode(", ",array_diff($subjectGroupIdAry,$foundSubjectGroupIdAry)));
					}
				}
			}

			if($startDate != "" && $dueDate != ""){
				// if a date range is given, get those homework that start date or due date fall into the given date range
				$startDate = date("Y-m-d", strtotime($startDate));
				$dueDate = date("Y-m-d", strtotime($dueDate));

				$conds .= " AND (";
				$conds .= " (h.StartDate>='$startDate 00:00:00' AND h.StartDate<='$dueDate 23:59:59') ";
				$conds .= " OR (h.DueDate>='$startDate 00:00:00' AND h.DueDate<='$dueDate 23:59:59') ";
				$conds .= ") ";
			}else{
				// get those homework that have started before or on the given start date, but haven't end yet before or on the given start date
				if($startDate != "") {
					$startDate = date("Y-m-d", strtotime($startDate));

					$conds .= " AND (h.StartDate<='$startDate 23:59:59' AND h.DueDate>='$startDate 00:00:00') ";
				}
				// get those homework that have ended before or on the given end date
				if($dueDate != "") {
					$dueDate = date("Y-m-d", strtotime($dueDate));

					$conds .= " AND (h.DueDate<='$dueDate 23:59:59') ";
				}
			}

			$conds .= " AND h.AcademicYearID = '$yearID' ";
			$conds .= " AND h.YearTermID = '$yearTermID' ";

			# Order By
			$cond_order = $order=="1" || $order=="" ? "DESC" : "ASC";

			# Num Of Max Record
			$cond_limit = "";
			if($noOfRecord != "" && $noOfRecord > 0) {
				$cond_limit = " LIMIT $noOfRecord";
			}

			//$namefield = getNameFieldByLang("u.");

			$sql = "SELECT
						h.HomeworkID,
						$year_class_fields
						h.ClassGroupID,
						h.SubjectID,
						u.EnglishName as PosterNameEN,
						u.ChineseName as PosterNameCH,
						DATE_FORMAT(h.StartDate,'%Y-%m-%d') as StartDate,
						DATE_FORMAT(h.DueDate,'%Y-%m-%d') as DueDate,
						h.Title,
						h.Description,
						h.AttachmentPath,
						h.HandinRequired,
						s.EN_DES as SubjectTitleEN,
						s.CH_DES as SubjectTitleCH,
						c.ClassTitleEN,
						c.ClassTitleB5 as ClassTitleCH
					FROM INTRANET_HOMEWORK as h
					LEFT JOIN ASSESSMENT_SUBJECT as s ON s.RecordID=h.SubjectID
					LEFT JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID=h.ClassGroupID
					$year_class_join_table
					LEFT JOIN INTRANET_USER as u ON u.UserID=h.PosterUserID
					WHERE 1 $conds
					GROUP BY h.HomeworkID
					ORDER BY h.StartDate $cond_order,h.Title
					$cond_limit";

			$dataAry = $this->returnResultSet($sql);

			$numOfRecord = sizeof($dataAry);
			$returnAry = array();
			for($i=0;$i<$numOfRecord;$i++) {
				$homework_id = $dataAry[$i]['HomeworkID'];
				$year_class_id = $dataAry[$i]['YearClassID'];
				$year_class_title_en = $dataAry[$i]['YearClassTitleEN'];
				$year_class_title_ch = $dataAry[$i]['YearClassTitleCH'];
				$class_group_id = $dataAry[$i]['ClassGroupID'];
				$subject_id = $dataAry[$i]['SubjectID'];
				$poster_name_en = trim($dataAry[$i]['PosterNameEN']);
				$poster_name_ch = trim($dataAry[$i]['PosterNameCH']);
				$start_date = $dataAry[$i]['StartDate'];
				$due_date = $dataAry[$i]['DueDate'];
				$title = trim($dataAry[$i]['Title']);
				$description = trim($dataAry[$i]['Description']);
				$handin_required = $dataAry[$i]['HandinRequired'];
				//$type_name = trim($dataAry[$i]['TypeName']);
				$subject_title_en = trim($dataAry[$i]['SubjectTitleEN']);
				$subject_title_ch = trim($dataAry[$i]['SubjectTitleCH']);
				$class_title_en = trim($dataAry[$i]['ClassTitleEN']);
				$class_title_ch = trim($dataAry[$i]['ClassTitleCH']);

				$attachment_path = trim($dataAry[$i]['AttachmentPath']);
				$attachment_url_ary = array();
				$attachment_fullpath = "$file_path/file/homework/".$attachment_path.$homework_id;
				 if (file_exists($attachment_fullpath))
				 {
				 	$attachment_ary = $lfs->return_files($attachment_fullpath);
				 	for($j=0;$j<count($attachment_ary);$j++) {
				 		$attachment_url = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($attachment_ary[$j]);
				 		$attachment_url_ary[] = $attachment_url;
				 	}
				 }

				 $returnAry[] = array(
				 					"HomeworkID" => $homework_id,
				 					"YearClassID" => $year_class_id,
				 					"YearClassTitleEN" => $year_class_title_en,
				 					"YearClassTitleCH" => $year_class_title_ch,
				 					"ClassGroupID" => $class_group_id,
				 					"SubjectID" => $subject_id,
				 					"PosterNameEN" => $poster_name_en,
				 					"PosterNameCH" => $poster_name_ch,
				 					"StartDate" => $start_date,
				 					"DueDate" => $due_date,
				 					"Title" => $title,
				 					"Description" => $description,
				 					"Attachments" => $attachment_url_ary,
				 					"HandinRequired" => $handin_required,
				 					"SubjectTitleEN" => $subject_title_en,
				 					"SubjectTitleCH" => $subject_title_ch,
				 					"ClassTitleEN" => $class_title_en,
				 					"ClassTitleCH" => $class_title_ch
				 				);
			}

			return $returnAry;
		}

		/*
		 * 	<eClassResponse>
		 * 		<IntranetHomeworks>
		 *			<IntranetHomework>
		 *				<HomeworkID></HomeworkID>
		 *				<YearClassID></YearClassID>
		 *				<YearClassTitleEN></YearClassTitleEN>
		 *				<YearClassTitleCH></YearClassTitleCH>
		 *				<ClassGroupID></ClassGroupID>
		 *				<SubjectID></SubjectID>
		 *				<PosterNameEN></PosterNameEN>
		 *				<PosterNameCH></PosterNameCH>
		 *				<StartDate>YYYY-MM-DD</StartDate>
		 *				<DueDate>YYYY-MM-DD</DueDate>
		 *				<Title></Title>
		 *				<Description></Description>
		 *				<Attachments>
		 *					<Attachment></Attachment>
		 *					...
		 *				</Attachments>
		 *				<HandinRequired>0:No | 1:Yes</HandinRequired>
		 *				<SubjectTitleEN></SubjectTitleEN>
		 *				<SubjectTitleCH></SubjectTitleCH>
		 *				<ClassTitleEN></ClassTitleEN>
		 *				<ClassTitleCH></ClassTitleCH>
		 *			</IntranetHomework>
		 *			...
		 *		</IntranetHomeworks>
		 *	</eClassResponse>
		 */
		function getIntranetHomeworkBySubjectGroupXml($yearClassId="",$subjectId="",$subjectGroupId="",$startDate="",$dueDate="",$order="1",$noOfRecord="")
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();
			$dataAry = $this->getIntranetHomeworkBySubjectGroup($yearClassId,$subjectId,$subjectGroupId,$startDate,$dueDate,$order,$noOfRecord);

			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			// construct announcement xml
			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			$xml.= $libxml->XML_OpenElement('IntranetHomeworks');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('IntranetHomework');
				foreach($dataAry[$i] as $key => $value) {
					$xml .= $libxml->XML_OpenElement($key);
					if(is_array($value)) {
						if($key == 'Attachments') {
							foreach($value as $key2 => $value2) {
								$xml .= $libxml->XML_OpenElement('Attachment');
								$xml .= $libxml->HandledCharacterForXML($value2);
								$xml .= $libxml->XML_CloseElement('Attachment');
							}
						}else {
							foreach($value as $key2 => $value2) {
								$xml .= $libxml->XML_OpenElement($key2);
								$xml .= $libxml->Parse_Array($value2);
								$xml .= $libxml->XML_CloseElement($key2);
							}
						}
					}else{
						$xml .= $libxml->HandledCharacterForXML($value);
					}
					$xml .= $libxml->XML_CloseElement($key);
				}
				$xml .= $libxml->XML_CloseElement('IntranetHomework');
			}
			$xml.= $libxml->XML_CloseElement('IntranetHomeworks');
			$xml.= $this->getXmlRootCloseNode();

			return $xml;

		}

		/* [eClass Course List]
		 *
		 */
		function getCourse($courseId='', $roomType='', $order='1', $numOfRecord='')
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;
			global $eclass_db, $eclass_filepath, $intranet_db;

			$conds = "";
			if($courseId != "") {
				$courseIdAry = array();
				if(is_array($courseId) && count($courseId)>0) {
					$conds .= " AND a.course_id IN (".implode(",",$courseId).") ";
					$courseIdAry = array_values(array_unique($courseId));
				}else if(is_string($courseId) && strstr($courseId,",")!== false ) {
					$conds .= " AND a.course_id IN (".$courseId.") ";
					$tmpCourseIdAry = explode(",",$courseId);
					for($i=0;$i<count($tmpCourseIdAry);$i++) {
						$tmpCourseIdAry[$i] = trim($tmpCourseIdAry[$i]);
					}
					$courseIdAry = array_values(array_unique($tmpCourseIdAry));
				}else {
					$conds .= " AND a.course_id='$courseId' ";
					$courseIdAry[] = $courseId;
				}
				if(count($courseIdAry)>0) {
					$sql = "SELECT DISTINCT course_id FROM {$eclass_db}.course WHERE course_id IN (".implode(",",$courseIdAry).")";
					$foundCourseIdAry = $this->returnVector($sql);

					if(count($foundCourseIdAry) != count($courseIdAry)) {
						$this->addError(7,"Invalid CourseID: ".implode(", ",array_diff($courseIdAry,$foundCourseIdAry)));
					}
				}
			}

			if($roomType != '') {
				if(is_array($roomType) && count($roomType)>0) {
					$conds .= " AND a.RoomType IN (".implode(",",$roomType).") ";
				}else if(is_string($roomType) && strstr($roomType,",")!== false) {
					$conds .= " AND a.RoomType IN (".$roomType.") ";
				}else {
					$conds .= " AND a.RoomType='".$roomType."' ";
				}
			}

			# Order By
			$cond_order = $order=="1" || $order=="" ? "DESC" : "ASC";

			# Num Of Max Record
			$cond_limit = "";
			if($numOfRecord != "" && $numOfRecord > 0) {
				$cond_limit = " LIMIT $numOfRecord";
			}

			$fieldname  = "a.course_id, a.course_code, a.course_name ";
		    $sql = "SELECT $fieldname
					FROM {$eclass_db}.course AS a
					WHERE 1 $conds
					GROUP BY a.course_id
					ORDER BY a.course_code $cond_order, a.course_name $cond_limit";

		    $dataAry = $this->returnResultSet($sql);
			$data_count = count($dataAry);

			$returnAry = array();
			for($i=0;$i<$data_count;$i++) {
				$returnAry[] = array('CourseID'=>$dataAry[$i]['course_id'],
										'CourseCode'=>$dataAry[$i]['course_code'],
										'CourseName'=>$dataAry[$i]['course_name']);
			}

			return $returnAry;
		}

		/*
		 * <eClassResponse>
		 *		<Courses>
		 *			<Course>
		 *				<CourseID></CourseID>
		 *				<CourseCode></CourseCode>
		 *				<CourseName></CourseName>
		 *			</Course>
		 *			...
		 *		</Courses>
		 *	</eClassResponse>
		 */
		function getCourseXml($courseId='', $roomType='', $order='1', $numOfRecord='')
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;
			global $eclass_db, $eclass_filepath, $intranet_db;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();
			$dataAry = $this->getCourse($courseId, $roomType, $order, $numOfRecord);

			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			// construct announcement xml
			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			$xml.= $libxml->XML_OpenElement('Courses');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('Course');
				$xml .= $libxml->Array_To_XML($dataAry[$i], true);
				$xml .= $libxml->XML_CloseElement('Course');
			}
			$xml.= $libxml->XML_CloseElement('Courses');
			$xml.= $this->getXmlRootCloseNode();
			$xml = $this->utf8_for_xml($xml);

			return $xml;
		}

		function getAssessment($courseId, $assessmentId='', $startDate='', $endDate='', $order='1', $numOfRecord='')
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language, $eclass40_httppath, $eclass40_filepath;
			global $eclass_db, $eclass_filepath, $intranet_db, $eclass_prefix;

			include_once("$eclass_filepath/system/settings/settings.php");
			//include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
			$course_db = $eclass_prefix."c".$courseId;

			$sql = "show databases like '$course_db'";
			$dataRow = $this->returnVector($sql);

			$returnAry = array();
			if(count($dataRow) > 0) {

				// school domain for attachment url
				$school_url = explode("\n",get_file_content("$intranet_root/file/email_website.txt"));
				$school_domain = $school_url[0];

				if(substr($school_domain,-1)=="/") {
					$school_domain = substr($school_domain,0,-1);
				}

		    	$sql = "SELECT a.course_id, a.course_code, a.course_name
						FROM {$eclass_db}.course AS a
						WHERE a.course_id='$courseId'";
				$courseAry = $this->returnResultSet($sql);
				$course_id = $courseAry[0]['course_id'];
				$course_code = $courseAry[0]['course_code'];
				$course_name = $courseAry[0]['course_name'];

				//$lfm = new fileManager($course_id, 3, 0);

				$conds = "";

				if($assessmentId != "") {
					$assessmentIdAry = array();
					if(is_array($assessmentId) && count($assessmentId)>0) {
						$conds .= " AND a.assessment_id IN (".implode(",",$assessmentId).") ";
						$assessmentIdAry = array_values(array_unique($assessmentId));
					}else if(is_string($assessmentId) && strstr($assessmentId,",")!== false ) {
						$conds .= " AND a.assessment_id IN (".$assessmentId.") ";
						$tmpAssessmentIdAry = explode(",",$assessmentId);
						for($i=0;$i<count($tmpAssessmentIdAry);$i++) {
							$tmpAssessmentIdAry[$i] = trim($tmpAssessmentIdAry[$i]);
						}
						$assessmentIdAry = array_values(array_unique($tmpAssessmentIdAry));
					}else {
						$conds .= " AND a.assessment_id='$assessmentId' ";
						$assessmentIdAry[] = $assessmentId;
					}

					if(count($assessmentIdAry) > 0) {
						$sql = "SELECT DISTINCT assessment_id FROM {$course_db}.assessment WHERE assessment_id IN (".implode(",",$assessmentIdAry).")";
						$foundAssessmentIdAry = $this->returnVector($sql);

						if(count($foundAssessmentIdAry) != count($assessmentIdAry)) {
							$this->addError(8,"Invalid AssessmentID: ".implode(", ",array_diff($assessmentIdAry,$foundAssessmentIdAry)));
						}
					}
				}

				if($startDate != "" && $endDate != ""){
					// if a date range is given, get those homework that start date or deadline fall into the given date range
					$startDate = date("Y-m-d", strtotime($startDate));
					$endDate = date("Y-m-d", strtotime($endDate));

					$conds .= " AND (";
					$conds .= " (a.startdate>='$startDate 00:00:00' AND a.startdate<='$endDate 23:59:59') ";
					$conds .= " OR (a.enddate>='$startDate 00:00:00' AND a.enddate<='$endDate 23:59:59') ";
					$conds .= ") ";
				}else{
					// get those homework that have started before or on the given start date, but haven't end yet before or on the given start date
					if($startDate != "") {
						$startDate = date("Y-m-d", strtotime($startDate));

						$conds .= " AND (a.startdate<='$startDate 23:59:59' AND a.enddate>='$startDate 00:00:00') ";
					}
					// get those homework that have ended before or on the given end date
					if($endDate != "") {
						$endDate = date("Y-m-d", strtotime($endDate));

						$conds .= " AND (a.enddate<='$endDate 23:59:59') ";
					}
				}

				# Order By
				$cond_order = $order=="1" || $order=="" ? "DESC" : "ASC";

				# Num Of Max Record
				$cond_limit = "";
				if($numOfRecord != "" && $numOfRecord > 0) {
					$cond_limit = " LIMIT $numOfRecord";
				}

				$sql = "SELECT
							a.assessment_id,
							a.category,
							a.handinby,
							a.title,
							a.instruction,
							DATE_FORMAT(a.startdate,'%Y-%m-%d') as startdate,
							DATE_FORMAT(a.enddate,'%Y-%m-%d') as enddate,
							a.attachment,
							a.phasetype,
							DATE_FORMAT(a.publisheddate,'%Y-%m-%d') as publisheddate
						FROM {$course_db}.assessment as a
						WHERE 1 $conds
						ORDER BY a.startdate $cond_order,a.title $cond_limit";

				//debug_r($sql);
				$dataAry = $this->returnResultSet($sql);
				$data_count = count($dataAry);
				$returnAry = array();
				for($i=0;$i<$data_count;$i++) {
					$assessment_id = $dataAry[$i]['assessment_id'];
					$category = $dataAry[$i]['category'];
					$handinby = $dataAry[$i]['handinby']; // 0: individual, 1:group
					$title = $dataAry[$i]['title'];
					$instruction = $dataAry[$i]['instruction'];
					$start_date = $dataAry[$i]['startdate'];
					$end_date = $dataAry[$i]['enddate'];
					$attachment = trim($dataAry[$i]['attachment']);
					//$notification = $dataAry[$i]['notification'];
					//$status = $dataAry[$i]['status']; // 1:draft, 2:published, 3:finished
					$phase_type = $dataAry[$i]['phasetype']; // 0:no phase, 1: multi phase
					$published_date = $dataAry[$i]['publisheddate'];

					//$attachments_ary = $lfm->manipulateAttachmentFile($attachment,$category);
					//debug_r($attachments_ary);
					$attachment_ary = $this->getAssessmentAttachmentFilePath($courseId,$attachment);

					$returnAry[] = array(
										'CourseID' => $course_id,
										'CourseCode' => $course_code,
										'CourseName' => $course_name,
										'AssessmentID' => $assessment_id,
										'Category' => $category,
										'Handinby' => $handinby,
										'Title' => $title,
										'Instruction' => $instruction,
										'StartDate' => $start_date,
										'EndDate' => $end_date,
										'Attachments' => $attachment_ary,
										'PhaseType' => $phase_type,
										'PublishedDate' => $published_date
									);
				}

			} else {
				$this->addError(7,"Invalid CourseID: $courseId");
			}

			return $returnAry;
		}

		function getAssessmentAttachmentFilePath($courseId,$attachmentStr)
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language, $eclass40_httppath, $eclass40_filepath;
			global $eclass_db, $eclass_filepath, $intranet_db, $eclass_prefix;

			$attachmentUrlAry = array();

			if($attachmentStr == '') {
				return $attachmentUrlAry;
			}

			$files = explode(":",$attachmentStr);

			$course_db = $eclass_prefix."c".$courseId;
			$course_folder = $eclass40_filepath ."/files/" . $eclass_prefix."c".$courseId . "/";
			$assignment_folder = $course_folder."assignment/";
			//debug_r($files);
			for ($i=0; $i<count($files); $i++)
			{
				if (preg_match("/^Upload\?\?/",trim($files[$i]))){
					$file_path = 'fileupload/'.trim(str_replace('Upload??', '', $files[$i]));

					$file_full_path = $course_folder . $file_path;

					$attachment_url = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($file_full_path);
					//$attachment_url = $school_domain."/home/download_attachment.php?target=".$file_full_path;
					$attachmentUrlAry[] = $attachment_url;
				} else {
					$Title 	 = get_file_basename($files[$i]);		// php5 + utf8
					$sLen 	 = strlen($files[$i])- strlen($Title);
					$sLen 	 = ($sLen > 0) ? $sLen - 1 : $sLen;			// minus 1 is used for handling chinese file name in utf8
					$VirPath = addslashes(trim(substr($files[$i], 0, $sLen)));
					$VirPath .= ($VirPath == '' || $VirPath{strlen($VirPath)-1} == '/') ? '' : '/';			// add a slashes to the end of the path according to the 'minus 1' action

					$Title 	 = addslashes(trim($Title));
					$Title   = str_replace("&amp;", "&", $Title);

					if ($VirPath!="" && $VirPath!="/"){
						$VirPath = "VirPath='".$VirPath."'";
					} else {
						$VirPath = "VirPath is NULL";
					}
					$sql = "SELECT FileID, Size, Location FROM {$course_db}.eclass_file WHERE $VirPath AND Title='$Title' AND IsDir<>1 ";
					$row = $this->returnArray($sql, 3);

					if (sizeof($row)>0)
					{
						$FileID   = $row[0][0];
						$Location = $row[0][2];
						$filename = trim($files[$i]);

						$file_full_path = $assignment_folder. $Location . $filename;

						$attachment_url = $school_domain."/home/download_attachment.php?target_e=".getEncryptedText($file_full_path);
						//$attachment_url = $school_domain."/home/download_attachment.php?target=".$file_full_path;
						$attachmentUrlAry[] = $attachment_url;
					}

				}
			}

			return $attachmentUrlAry;
		}

		/*
		 * <eClassResponse>
		 *		<Assessments>
		 *			<Assessment>
		 *				<CourseID></CourseID>
		 *				<CourseCode></CourseCode>
		 *				<CourseName></CourseName>
		 *				<AssessmentID></AssessmentID>
		 *				<Category></Category>
		 *				<Handinby>0: individual | 1:group</Handinby>
		 *				<Title></Title>
		 *				<Instruction></Instruction>
		 *				<StartDate>YYYY-MM-DD</StartDate>
		 *				<EndDate>YYYY-MM-DD</EndDate>
		 *				<Attachments>
		 *					<Attachment></Attachment>
		 *					...
		 *				</Attachments>
		 *				<PhaseType>0:no phase | 1: multi phase</PhaseType>
		 *				<PublishedDate></PublishedDate>
		 *			</Assessment>
		 *			...
		 *		</Assessments>
		 *	</eClassResponse>
		 */
		function getAssessmentXml($courseId, $assessmentId='', $startDate='', $endDate='', $order='1', $numOfRecord='')
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;
			global $eclass_db, $eclass_filepath, $WV_path, $intranet_db;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();
			$dataAry = $this->getAssessment($courseId, $assessmentId, $startDate, $endDate, $order, $numOfRecord);

			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			// construct announcement xml
			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();
			/*
			$xml.= $libxml->XML_OpenElement('Assessments');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('Assessment');
				$xml .= $libxml->Array_To_XML($dataAry[$i], true);
				$xml .= $libxml->XML_CloseElement('Assessment');
			}
			$xml.= $libxml->XML_CloseElement('Assessments');
			$xml.= $this->getXmlRootCloseNode();
			*/
			$xml.= $libxml->XML_OpenElement('Assessments');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('Assessment');
				foreach($dataAry[$i] as $key => $value) {
					$xml .= $libxml->XML_OpenElement($key);
					if(is_array($value)) {
						if($key == 'Attachments') {
							foreach($value as $key2 => $value2) {
								$xml .= $libxml->XML_OpenElement('Attachment');
								$xml .= $libxml->HandledCharacterForXML($value2);
								$xml .= $libxml->XML_CloseElement('Attachment');
							}
						}else {
							foreach($value as $key2 => $value2) {
								$xml .= $libxml->XML_OpenElement($key2);
								$xml .= $libxml->Parse_Array($value2);
								$xml .= $libxml->XML_CloseElement($key2);
							}
						}
					}else{
						$xml .= $libxml->HandledCharacterForXML($value);
					}
					$xml .= $libxml->XML_CloseElement($key);
				}
				$xml .= $libxml->XML_CloseElement('Assessment');
			}
			$xml.= $libxml->XML_CloseElement('Assessments');
			$xml.= $this->getXmlRootCloseNode();
			$xml = $this->utf8_for_xml($xml);

			return $xml;
		}

		function getNotice($targetType="",$startDate="",$endDate="",$order="1",$noOfRecord="", $advanceDateControl=0,$startBefore="",$startAfter="",$endBefore="",$endAfter="")
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;
			global $i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,$i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual;

			$lfs = $this->getInstanceLibFilesystem();

			//$is_EJ = $this->isEJ();

			// school domain for attachment url
			$school_url = explode("\n",get_file_content("$intranet_root/file/email_website.txt"));
			$school_domain = $school_url[0];

			if(substr($school_domain,-1)=="/") {
				$school_domain = substr($school_domain,0,-1);
			}

			$targetTypeAry = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);

			$returnAry = array();
			$conds = "";

			if($targetType != ""){
			    $targetTypes = explode(',', $targetType);

			    $recordType = '';
			    foreach($targetTypes as $type){
			        if(in_array($type,array(1,2,3,4))){
			            $recordType .= "{$type},";
			        }
			    }
			    $recordType = trim($recordType, ',');
				$conds .= " AND a.RecordType IN ({$recordType}) ";
			}

			// date period
			if($advanceDateControl == 1)
			{
				if($startBefore != ''){
					if(strlen($startBefore) <= 10 ){
					    $startBefore = "{$startBefore} 23:59:59";
					}
					$conds .= " AND a.DateStart <= '$startBefore' ";
				}
				if($startAfter != ''){
					if(strlen($startAfter) <= 10 ){
					    $startAfter = "{$startAfter} 00:00:00";
					}
					$conds .= " AND a.DateStart >= '$startAfter' ";
				}
				if($endBefore != ''){
					if(strlen($endBefore) <= 10 ){
					    $endBefore = "{$endBefore} 23:59:59";
					}
					$conds .= " AND a.DateEnd <= '$endBefore' ";
				}
				if($endAfter != ''){
					if(strlen($endAfter) <= 10 ){
					    $endAfter = "{$endAfter} 00:00:00";
					}
					$conds .= " AND a.DateEnd >= '$endAfter' ";
				}
			}else{
				if($startDate != "" && $endDate != ""){
					// if a date range is given, get those homework that start date or deadline fall into the given date range
					if(strlen($startDate) <= 10 ){
					    $startDate = "{$startDate} 00:00:00";
					}
					if(strlen($endDate) <= 10 ){
					    $endDate = "{$endDate} 23:59:59";
					}

					$conds .= " AND (";
					$conds .= " (a.DateStart >= '$startDate' AND a.DateStart <= '$endDate') ";
					$conds .= " OR (a.DateEnd >= '$startDate' AND a.DateEnd <= '$endDate') ";
					$conds .= ") ";
				}else if($startDate!="" || $endDate!="") {
					if($startDate != "") {
    					if(strlen($startDate) <= 10 ){
    					    $startDate = "{$startDate} 23:59:59";
    					}

						$conds .= " AND a.DateStart <= '$startDate' AND a.DateEnd >= CURRENT_TIMESTAMP() ";
					}
					if($endDate != "") {
    					if(strlen($endDate) <= 10 ){
    					    $endDate = "{$endDate} 23:59:59";
    					}

						$conds .= " AND a.DateEnd <= '$endDate' ";
					}
	            } else {
					$conds .= " AND CURRENT_TIMESTAMP() <= a.DateEnd "; // Default get not expired notice
		            //$conds .= " AND (a.DateEnd is null OR CURDATE()<=DATE_FORMAT(a.DateEnd,'%Y-%m-%d') OR CURDATE()>=DATE_FORMAT(a.DateStart,'%Y-%m-%d')) "; // get non-expired notices or announced notices
	            }
			}
            /*
            if($startDate != "" && $endDate != ""){
            	$startDate = date("Y-m-d", strtotime($startDate));
				$endDate = date("Y-m-d", strtotime($endDate));
				$conds .= " AND a.DateStart >= '$startDate 00:00:00' AND a.DateEnd <= '$endDate 23:59:59' ";
            }else if($startDate != ""){
            	$startDate = date("Y-m-d", strtotime($startDate));
				$conds .= " AND a.DateStart >= '$startDate 00:00:00' AND a.DateEnd <= CONCAT(CURDATE(),' 23:59:59') ";
            }else if($endDate != ""){
            	$endDate = date("Y-m-d", strtotime($endDate));
            	$conds .= " AND a.DateEnd <= '$endDate 23:59:59' ";
            }
            */
            // sort order
			$cond_order = $order=="1" || $order=="" ? "DESC" : "ASC";
			// max number of record
			$cond_limit = "";
			if($noOfRecord != "" && $noOfRecord > 0) {
				$cond_limit = " LIMIT $noOfRecord";
			}
			$name_field = getNameFieldByLang("u.");
			$sql = "SELECT
						a.NoticeID,
						a.NoticeNumber,
						a.Title,
						a.Description,
						DATE_FORMAT(a.DateStart,'%Y-%m-%d') as DateStart,
						DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as DateEnd,
            			DATE_FORMAT(a.DateStart,'%H:%i:%s') as TimeStart,
            			DATE_FORMAT(a.DateEnd,'%H:%i:%s') as TimeEnd,
						IF(u.UserID IS NULL,a.IssueUserName,$name_field) as IssueUserName,
						a.Attachment,
						a.RecordType
					FROM INTRANET_NOTICE as a
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.IssueUserID
					WHERE a.RecordStatus='1' AND a.isDeleted=0 AND a.IsModule=0 AND (a.Module IS NULL OR a.Module='') $conds
					ORDER BY a.DateStart $cond_order,a.NoticeID desc $cond_limit";

			$dataAry = $this->returnResultSet($sql);
			$data_count = count($dataAry);

			for($i=0;$i<$data_count;$i++) {

				$notice_id = $dataAry[$i]['NoticeID'];
				$notice_number = $dataAry[$i]['NoticeNumber'];
				$title = $dataAry[$i]['Title'];
				$description = $dataAry[$i]['Description'];
				$date_start = $dataAry[$i]['DateStart'];
				$date_end = $dataAry[$i]['DateEnd'];
				$time_start = $dataAry[$i]['TimeStart'];
				$time_end = $dataAry[$i]['TimeEnd'];
				$issue_user_name = $dataAry[$i]['IssueUserName'];
				$attachment_folder = $dataAry[$i]['Attachment'];
				$record_type = $dataAry[$i]['RecordType'];
				$target_type = $targetTypeAry[$dataAry[$i]['RecordType']];

				$attachment_ary = array();
				if (trim($attachment_folder)!="")
				{
					 $attachment_path = "$file_path/file/notice/".$attachment_folder;
					 if (file_exists($attachment_path))
					 {
					 	$attachment_array = $lfs->return_files($attachment_path);
					 	for($j=0;$j<count($attachment_array);$j++)
					 	{
					 		$file_name = get_file_basename($attachment_array[$j]);
					 		$attachment_ary[] = array($file_name,$school_domain."/home/download_attachment.php?target_e=".getEncryptedText($attachment_array[$j]));
					 	}
					 }
				}

				$returnAry[] = array(
									"NoticeID" => $notice_id,
									"NoticeNumber" => $notice_number,
									"Title" => $title,
									"Description" => $description,
									"DateStart" => $date_start,
									"DateEnd" => $date_end,
            						"TimeStart" => $time_start,
            						"TimeEnd" => $time_end,
									"IssueUserName" => $issue_user_name,
									"Attachments" => $attachment_ary,
									"TargetType"=> $target_type,
									"RecordType"=> $record_type
								);
			}

			return $returnAry;
		}

		/*
		 * <eClassResponse>
		 *		<Notices>
		 *			<Notice>
		 *				<NoticeID></NoticeID>
		 *				<NoticeNumber></NoticeNumber>
		 *				<Title></Title>
		 *				<Description></Description>
		 *				<DateStart></DateStart>
		 *				<DateEnd></DateEnd>
		 *				<IssueUserName></IssueUserName>
		 *				<Attachments>
		 *					<Attachment>
		 *						<FileName></FileName>
		 *						<FileURL></FileURL>
		 *					</Attachment>
		 *					...
		 *				</Attachments>
		 *				<TargetType></TargetType>
		 *			</Notice>
		 *			...
		 *		</Notices>
		 *	</eClassResponse>
		 */
		function getNoticeXml($targetType='', $startDate='', $endDate='', $order='1', $numOfRecord='', $advanceDateControl=0, $startBefore='', $startAfter='', $endBefore='', $endAfter='')
		{
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;
			global $eclass_db, $eclass_filepath, $WV_path, $intranet_db;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();
			$dataAry = $this->getNotice($targetType, $startDate, $endDate, $order, $numOfRecord, $advanceDateControl, $startBefore, $startAfter, $endBefore, $endAfter);

			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			// construct xml
			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			$xml.= $libxml->XML_OpenElement('Notices');
			$data_count = count($dataAry);
			for($i=0;$i<$data_count;$i++) {
				$xml .= $libxml->XML_OpenElement('Notice');
				foreach($dataAry[$i] as $key => $value) {
					$xml .= $libxml->XML_OpenElement($key);
					if(is_array($value)) {
						if($key == 'Attachments') {
							foreach($value as $key2 => $value2) {
								$xml .= $libxml->XML_OpenElement('Attachment');
									$xml .= $libxml->XML_OpenElement('FileName');
										$xml .= $libxml->HandledCharacterForXML($value2[0]);
									$xml .= $libxml->XML_CloseElement('FileName');
									$xml .= $libxml->XML_OpenElement('FileURL');
										$xml .= $libxml->HandledCharacterForXML($value2[1]);
									$xml .= $libxml->XML_CloseElement('FileURL');
								$xml .= $libxml->XML_CloseElement('Attachment');
							}
						}else {
							foreach($value as $key2 => $value2) {
								$xml .= $libxml->XML_OpenElement($key2);
								$xml .= $libxml->Parse_Array($value2);
								$xml .= $libxml->XML_CloseElement($key2);
							}
						}
					}else{
						$xml .= $libxml->HandledCharacterForXML($value);
					}
					$xml .= $libxml->XML_CloseElement($key);
				}
				$xml .= $libxml->XML_CloseElement('Notice');
			}
			$xml.= $libxml->XML_CloseElement('Notices');
			$xml.= $this->getXmlRootCloseNode();
			$xml = $this->utf8_for_xml($xml);

			return $xml;
		}

		function getDigitalChannelsCategory($categoryId='',$year='',$albumCode=''){
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language,$eclass_httppath;
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			//$videoFormat = array('3GP','WMV','MP4','MOV','RM','FLV','AVI');
			$conds = "";
			$returnAry = array();


			$ldc = new libdigitalchannels();

			$categoryIdAry = array();
			$cond = '';
			if($year !=""){
				$yearAry = explode("-",$year);
				if(empty($yearAry[1])){
				    $yearAry[1] = $yearAry[0] + 1;
				}
				$cond .= ' AND DateTaken >= "'.$yearAry[0].'-09-01 00:00:00" AND DateTaken <= "'.$yearAry[1].'-08-31 00:00:00"';
			}
			if ($albumCode != '') {
			    if (is_array($albumCode)) {
			        $cond .= ' AND AlbumCode IN ("'.implode('","',$albumCode).'"';
			    }
			    else {
			        $cond .= ' AND AlbumCode="'.$albumCode.'"';
			    }
			}

			//			else{
			//				$fcm = new form_class_manage();
			//				$SchoolYearList = $fcm->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
			//				$cond = ' AND DateInput >= "'.$SchoolYearList[0]['AcademicYearStart'].'" AND DateInput <= "'.$SchoolYearList[0]['AcademicYearEnd'].'"';
			//			}

			if($categoryId != "") {
				if(is_array($categoryId) && count($categoryId)>0) {
					$categoryIdAry = array_values(array_unique($categoryId));
				}else if(is_string($categoryId) && strstr($categoryId,",")!== false ) {
					$tmpCategoryIdAry = explode(",",$categoryId);
					for($i=0;$i<count($tmpCategoryIdAry);$i++) {
						$tmpCategoryIdAry[$i] = trim($tmpCategoryIdAry[$i]);
					}
					$categoryIdAry = array_values(array_unique($tmpCategoryIdAry));
				}else {
					$categoryIdAry[] = $categoryId;
				}
				if(count($categoryIdAry)>0) {
					$sql = "SELECT DISTINCT CategoryCode FROM INTRANET_DC_CATEGORY WHERE CategoryCode IN ('".implode("','",$categoryIdAry)."')";
					$foundCategoryIdAry = $this->returnVector($sql);

					if(count($foundCategoryIdAry) != count($categoryIdAry)) {
						$this->addError(7,"Invalid CategoryCode: ".implode(", ",array_diff($categoryIdAry,$foundCategoryIdAry)));
					}
				}
			}else{
				$sql = "SELECT DISTINCT CategoryCode FROM INTRANET_DC_CATEGORY";
				$categoryIdAry = $this->returnVector($sql);
			}

			foreach($categoryIdAry as $categoryKey =>$categoryCode){
				$categoryInfo = $ldc->Get_Category($categoryCode);
				$returnAry[] = $categoryInfo[0];
				$sql = "Select CategoryCode, CoverPhotoID, AlbumID from INTRANET_DC_ALBUM where CategoryCode='$categoryCode' $cond";
				$AlbumToal = $this->returnArray($sql);
				$returnAry[$categoryKey]['AlbumTotal'] = count($AlbumToal);
				foreach($AlbumToal as $AlbumKey => $AlbumDetail){
				    $AlbumID = $AlbumDetail['AlbumID'];
					$AlbumDetail = $ldc->GetAlbum($AlbumID);

					$album_user_group = $ldc->getAlbumUsersGroups($AlbumID);
					if(is_array($album_user_group) && $album_user_group[0] == 1){
					    $album_status = 'ALL';
					}else if(is_array($album_user_group) && $album_user_group[0] == 2){
					    $album_status = 'GROUP';
					}else{
					    $album_status = 'PRIVATE';
					}

					if($AlbumDetail['cover_photo_id'] ==''){
						$sql ="Select AlbumPhotoID from INTRANET_DC_ALBUM_PHOTO where AlbumID='".$AlbumDetail['id']."' and Sequence='0'";
						$coverPhotoID = $this->returnVector($sql);
						$AlbumDetail['cover_photo_id'] = $coverPhotoID[0];
					}

					$photoDetail = $ldc->getAlbumPhoto($AlbumDetail['cover_photo_id']);
					$url = '';
					if($photoDetail && $AlbumDetail){
						$url = $_SERVER["HTTP_HOST"].$ldc ->getPhotoFileName('thumbnail',$AlbumDetail,$photoDetail);
					}

					$AlbumDetail['since'] = gmdate("Y-m-d H:i:s", $AlbumDetail['since']);
					$AlbumDetail['until'] = gmdate("Y-m-d H:i:s", $AlbumDetail['until']);
					$AlbumDetail['date'] = gmdate("Y-m-d H:i:s", $AlbumDetail['date']);
					$AlbumDetail['input_date'] = gmdate("Y-m-d H:i:s", $AlbumDetail['input_date']);
					$AlbumDetail['coverphoto'] = $url;
					$AlbumDetail['Target'] = $album_status;

					$sql = "Select Count(AlbumPhotoID) From INTRANET_DC_ALBUM_PHOTO where AlbumID ='".$AlbumDetail['id']."'";
					$totalPhoto = $this->returnVector($sql);
					$AlbumDetail['totalPhoto'] = $totalPhoto[0];

					$returnAry[$categoryKey]['Album'][]=$AlbumDetail;
				}
			}


			return $returnAry;

		}

		function getDigitalChannelsAlbum($targetType='',$albumId='',$year='',$order='',$albumCode='', $albumTarget='', $limit='',$page=''){
			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language,$eclass_httppath, $sys_custom;
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$videoFormat = array('3GP','WMV','MP4','MOV','RM','FLV','AVI');
			$conds = "";
            $limitSql = "";
			$returnAry = array();


			$ldc = new libdigitalchannels();
			$albumIdAry = array();
			$cond = '';
			if($year !=""){
				$year = explode("-",$year);
				if(empty($year[1])){
				    $year[1] = $year[0] + 1;
				}
				if($targetType == 1){ //normal album
    				$cond = ' AND IDA.DateTaken >= "'.$year[0].'-09-01 00:00:00" AND IDA.DateTaken <= "'.$year[1].'-08-31 00:00:00"';
				}else if($targetType == 2){ //highlight album
    				$cond = ' AND r.DateInput >= "'.$year[0].'-09-01 00:00:00" AND r.DateInput <= "'.$year[1].'-08-31 00:00:00"';
				}
			}

			$normalAlbumCond = $cond;
			$highlightAlbumCond = $cond;

			if ($albumCode != '') {
			    if (is_array($albumCode)) {
			        $normalAlbumCond .= ' AND IDA.AlbumCode IN ("'.implode('","',$albumCode).'"';
			        $highlightAlbumCond .= ' AND a.AlbumCode IN ("'.implode('","',$albumCode).'"';
			    }
			    else {
			        $normalAlbumCond .= ' AND IDA.AlbumCode="'.$albumCode.'"';
			        $highlightAlbumCond .= ' AND a.AlbumCode="'.$albumCode.'"';
			    }
			}

            if($albumTarget=='ALL'){
                $normalAlbumCond .= ' AND IDAU.RecordType="all"';
            }elseif($albumTarget=='GROUP'){
                $normalAlbumCond .= ' AND IDAU.RecordType="group"';
            }elseif($albumTarget=='USER'){
                $normalAlbumCond .= ' AND IDAU.RecordType="user"';
            }elseif($albumTarget=='PRIVATE'){
                $normalAlbumCond .= ' AND IDAU.RecordType IS NULL';
            }

            $normalOrderBy = ($order == 1) ? 'IDA.DateTaken ASC' : 'IDA.DateTaken DESC';
            $highlightOrderBy = ($order == 1) ? 'r.SharedSince ASC' : 'r.SharedSince DESC';

            if ($limit) {
                $limitSql = " LIMIT {$limit}";
                if ($page) {
                    $offset = $limit * ($page - 1);
                    $limitSql .= " OFFSET {$offset}";
                }
            }

			//			else{
			//				$fcm = new form_class_manage();
			//				$SchoolYearList = $fcm->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
			//				$cond = ' AND DateInput >= "'.$SchoolYearList[0]['AcademicYearStart'].'" AND DateInput <= "'.$SchoolYearList[0]['AcademicYearEnd'].'"';
			//			}

			if($albumId != "") {
				if(is_array($albumId) && count($albumId)>0) {
					$albumIdAry = array_values(array_unique($albumId));
				}else if(is_string($albumId) && strstr($albumId,",")!== false ) {
					$tmpAlbumIdAry = explode(",",$albumId);
					for($i=0;$i<count($tmpAlbumIdAry);$i++) {
						$tmpAlbumIdAry[$i] = trim($tmpAlbumIdAry[$i]);
					}
					$albumIdAry = array_values(array_unique($tmpAlbumIdAry));
				}else {
					$albumIdAry[] = $albumId;
				}
				if(count($albumIdAry)>0) {
					if($targetType == 1){ //normal album
						$sql = "SELECT DISTINCT 
                            IDA.AlbumID 
                        FROM 
                            INTRANET_DC_ALBUM IDA 
                        WHERE 
                            IDA.AlbumID IN (".implode(",",$albumIdAry).")
                        ORDER BY 
                            {$normalOrderBy}
                        {$limitSql}
                        ";
					}else if($targetType == 2){ //highlight album
						$sql = "SELECT DISTINCT 
                            r.RecommendID 
                        FROM 
                            INTRANET_DC_RECOMMEND r
                        WHERE
                            r.RecommendID IN (".implode(",",$albumIdAry).")
                        ORDER BY 
                            {$highlightOrderBy}
                        {$limitSql}
                        ";
					}
					$foundAlbumIdAry = $this->returnVector($sql);

					if(count($foundAlbumIdAry) != count($albumIdAry)) {

						$this->addError(7,"Invalid AlbumID: ".implode(", ",array_diff($albumIdAry,$foundAlbumIdAry)));
					}
				}
			}else{
				if($targetType == 1){ //normal album
				    $sql = "SELECT DISTINCT 
                        IDA.AlbumID 
                    FROM 
                        INTRANET_DC_ALBUM IDA
                    LEFT JOIN 
                        INTRANET_DC_ALBUM_USER IDAU 
                    ON 
                        IDA.AlbumID = IDAU.AlbumID
                    WHERE 
                        1 
                        $normalAlbumCond
                    ORDER BY 
                        {$normalOrderBy}
                    {$limitSql}
                    ";
				}else if($targetType == 2){ //highlight album
					$sql = "SELECT
                                DISTINCT r.RecommendID
                            FROM
                                INTRANET_DC_RECOMMEND r
					        INNER JOIN
                                INTRANET_DC_RECOMMEND_PHOTO rp ON rp.RecommendID=r.RecommendID
                            INNER JOIN
                                INTRANET_DC_ALBUM_PHOTO ap ON ap.AlbumPhotoID=rp.AlbumPhotoID
                            INNER JOIN
                                INTRANET_DC_ALBUM a ON a.AlbumID=ap.AlbumID
                            WHERE 
                                1 
                                $highlightAlbumCond
                            ORDER BY 
                                {$highlightOrderBy}
                            {$limitSql}
                            ";
				}
				$albumIdAry = $this->returnVector($sql);
			}

			foreach($albumIdAry as $albumKey => $selectAlbumId){

				if($targetType == 1){
					$album_detail = $ldc->GetAlbum($selectAlbumId);

					$album_user_group = $ldc->getAlbumUsersGroups($selectAlbumId);
					if(is_array($album_user_group) && $album_user_group[0] == 1){
					    $album_status = 'ALL';
					}else if(is_array($album_user_group) && $album_user_group[0] == 2){
					    $album_status = 'GROUP';
					}else{
					    $album_status = 'PRIVATE';
					}

					$PhotoAry = $ldc->Get_Album_Photo('',$selectAlbumId); // can added date range as conditions
					$returnAry[$albumKey]['AlbumID'] = $album_detail['id'];
					if($album_detail['title']){
						$returnAry[$albumKey]['Title'] = $album_detail['title'];
						$returnAry[$albumKey]['TitleZh'] = $album_detail['titleZh'];
					}else{
						$returnAry[$albumKey]['Title'] = "(Untitled Album)";
						$returnAry[$albumKey]['TitleZh'] = "(未命名相簿)";
					}
					$returnAry[$albumKey]['Date'] = $album_detail['date'];
					$returnAry[$albumKey]['SharedSince'] = $album_detail['since'];
					$returnAry[$albumKey]['SharedUntil'] = $album_detail['until'];
					$returnAry[$albumKey]['Description'] = $album_detail['description'];
					$returnAry[$albumKey]['CategoryCode'] = $album_detail['CategoryCode'];
					$returnAry[$albumKey]['TotalItem'] = sizeof($PhotoAry);
					$returnAry[$albumKey]['Target'] = $album_status;
				}else if($targetType == 2){
					$album_detail = $ldc->Get_Recommend_Album($selectAlbumId);
					$PhotoAry = $ldc->Get_Recommend_Album_Photo($selectAlbumId,'');
					$returnAry[$albumKey]['AlbumID'] = $album_detail[0]['RecommendID'];
					if($album_detail[0]['Title']){
						$returnAry[$albumKey]['Title'] = $album_detail[0]['Title'];
						$returnAry[$albumKey]['TitleZh'] = $album_detail[0]['TitleZh'];
					}else{
						$returnAry[$albumKey]['Title'] = "(Untitled highlight Album)";
						$returnAry[$albumKey]['TitleZh'] = "(未命名精華相簿)";
					}
					$returnAry[$albumKey]['Date'] = $album_detail[0]['SharedSince'];
					$returnAry[$albumKey]['SharedSince'] = $album_detail[0]['SharedSince'];
					$returnAry[$albumKey]['SharedUntil'] = $album_detail[0]['SharedUntil'];
					$returnAry[$albumKey]['Description'] = $album_detail[0]['Description'];
					$returnAry[$albumKey]['TotalItem'] = sizeof($PhotoAry);
				}

				if($sys_custom['eClassApp']['SFOC']){
				    if($album_detail['titleZh']){
				        $returnAry[$albumKey]['TitleZh'] = $album_detail['titleZh'];
				    }else{
				        $returnAry[$albumKey]['TitleZh'] = "(未命名相簿)";
				    }
				    $returnAry[$albumKey]['DescriptionZh'] = $album_detail['descriptionZh'];
				}

				foreach($PhotoAry as $photoKey => $selectPhoto){

					$sql = "SELECT DateModified FROM INTRANET_DC_ALBUM_PHOTO WHERE AlbumPhotoID={$selectPhoto['id']}";
					$dateModify = $this->returnVector($sql);
					$dateModify = explode(" ",$dateModify[0]);
					$dateModify = $dateModify[0];

					$returnAry[$albumKey]['item'][$photoKey]['PhotoID'] = $selectPhoto['id'];
					$returnAry[$albumKey]['item'][$photoKey]['title'] = $selectPhoto['title'];
					$returnAry[$albumKey]['item'][$photoKey]['description'] = $selectPhoto['description'];
					$returnAry[$albumKey]['item'][$photoKey]['dateModify'] = $dateModify;
					$returnAry[$albumKey]['item'][$photoKey]['sequence'] = $selectPhoto['sequence'];
					if($targetType == 1){ //normal album
						$returnAry[$albumKey]['item'][$photoKey]['coverUrl'] = $_SERVER["HTTP_HOST"].$ldc ->getPhotoFileName('thumbnail',$album_detail,$selectPhoto);
						$returnAry[$albumKey]['item'][$photoKey]['url'] = $_SERVER["HTTP_HOST"].$ldc ->getPhotoFileName('photo',$album_detail,$selectPhoto);
					}else if($targetType == 2){ //highlight album
						$photo_album = $ldc->GetAlbum($selectPhoto['album_id']);
						$returnAry[$albumKey]['item'][$photoKey]['coverUrl'] = $_SERVER["HTTP_HOST"].$ldc ->getPhotoFileName('thumbnail',$photo_album,$selectPhoto);
						$returnAry[$albumKey]['item'][$photoKey]['url'] = $_SERVER["HTTP_HOST"].$ldc ->getPhotoFileName('photo',$photo_album,$selectPhoto);
					}

					if ($album_detail ['cover_photo_id'] == '') {
						$returnAry [$albumKey] ['CoverUrl'] = $returnAry [$albumKey] ['item'] [0] ['coverUrl'];
					} else {
						if($selectPhoto['id'] == $album_detail ['cover_photo_id']){
							$returnAry [$albumKey] ['CoverUrl'] = $returnAry [$albumKey] ['item'] [$photoKey] ['coverUrl'];
						}
					}

					$o_ext = pathinfo($selectPhoto['title'], PATHINFO_EXTENSION);
					if(in_array(strtoupper($o_ext),$videoFormat)){
						$returnAry[$albumKey]['item'][$photoKey]['type'] = 'video';
					}else{
						$returnAry[$albumKey]['item'][$photoKey]['type'] = 'photo';
					}


					if($sys_custom['eClassApp']['SFOC']){
					    $returnAry[$albumKey]['item'][$photoKey]['EventTitle'] = $selectPhoto['event_title'];
					    $returnAry[$albumKey]['item'][$photoKey]['EventTitleZh'] = $selectPhoto['chi_event_title'];
					    $returnAry[$albumKey]['item'][$photoKey]['descriptionZh'] = $selectPhoto['chi_description'];
					    $returnAry[$albumKey]['item'][$photoKey]['EventDate'] = $selectPhoto['event_date'];
					}
				}

			}


			// 			if($targetType == 1){ //normal album

			// 			}else if($targetType == 2){ //highlight album

			// 			}

			return $returnAry;

		}

		/*
		 *	Category
		 * 	<eClassResponse>
		 *	   <Categories>
		 *	      <Category>
		 *	         <CategoryCode> </CategoryCode>
		 *	         <CategoryName_EN> </CategoryName_EN>
		 *	         <CategoryName_CHI> </CategoryNameCHI>
		 *	         <CategoryDateModified> </CategoryDateModified>
		 *	         <CategoryTotalAlbum> </CategoryTotalAlbum>
		 *	         <Album>
		 *	            <AlbumID></AlbumID>
		 *	            <AlbumTitle></AlbumTitle>
		 *	            <AlbumCoverUrl></AlbumCoverUrl>
		 *	            <AlbumTotalPhoto></AlbumTotalPhoto>
		 *	            <AlbumInputDate></AlbumInputDate>
		 *	         </Album>
		 *	         ...
		 *	      </Category>
		 *	      ...
		 *	   </Categories>
		 *	</eClassResponse>
		 *
		 *	Album or Highlight Album
		 *	<eClassResponse>
		 *	   <Albums>
		 *	      <Album>
		 *	         <AlbumID> </AlbumID>
		 *	         <AlbumTitle> </AlbumTitle>
		 *	         <AlbumDescription> </AlbumDescription>
		 *	         <AlbumTotalItem> </AlbumTotalItem>
		 *	         <AlbumCoverUrl> </AlbumCoverUrl>
		 *	         <Photo>
		 *	            <PhotoID></PhotoID>
		 *	            <PhotoTitle></PhotoTitle>
		 *	            <PhotoDescription></PhotoDescription>
		 *	            <PhotoCoverURL></PhotoCoverURL>
		 *	            <PhotoURL></PhotoURL>
		 *	         </Photo>
		 *	         ...
		 *	         <Video>
		 *	            <VideoID></VideoID>
		 *	            <VideoTitle></VideoTitle>
		 *	            <VideoDescription></VideoDescription>
		 *	            <VideoCoverURL></VideoCoverURL>
		 *	            <VideoURL></VideoURL>
		 *	         </Video>
		 *	         ...
		 *	      </Album>
		 *	      ...
		 *	   </Albums>
		 *	</eClassResponse>
		 */

		function getDigitalChannelsXML($targetType,$inputId='',$year='',$order='', $albumCode='', $albumTarget='', $limit='',$page=''){
			//for category
			//getDigitalChannelsAlbumXML('0',$categoryCode,$year);
			//for album
			//getDigitalChannelsAlbumXML('1',$albumId,$order,);
			//for highlight
			//getDigitalChannelsAlbumXML('2',$recommendId,$order);

			global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;
			global $eclass_db, $eclass_filepath, $WV_path, $intranet_db, $sys_custom;

			$this->removeAllError(); // clear errors

			$libxml = $this->getInstanceLibXml();


            if ($targetType == 1) { //category
                $categoryAry = $this->getDigitalChannelsCategory($inputId, $year, $albumCode);
            } else if ($targetType == 2) { //album
                $AlbumAry = $this->getDigitalChannelsAlbum(1, $inputId, $year, $order, $albumCode, $albumTarget, $limit, $page);
            } else if ($targetType == 3) { //highlight
                $AlbumAry = $this->getDigitalChannelsAlbum(2, $inputId, $year, $order, $albumCode, '', $limit, $page);
            }
            //else if ($targetType == 4){ //photo
			// for select photo only
			//}
			else{
				$this->addError(7,"Invalid target type: $targetType");
			}



			// if have error, inform user
			if($this->errorCount() > 0) {
				return $this->getErrorXml();
			}

			$xml = $this->getXmlHeader();
			$xml.= $this->getXmlRootOpenNode();

			if($categoryAry){
				$xml.= $libxml->XML_OpenElement('Categories');
				foreach($categoryAry as $key => $category){
					$xml.= $libxml->XML_OpenElement('Category');

					$xml.= $libxml->XML_OpenElement('CategoryCode');
					$xml.= $libxml->HandledCharacterForXML($category['CategoryCode']);
					$xml.= $libxml->XML_CloseElement('CategoryCode');

					$xml.= $libxml->XML_OpenElement('CategoryName_EN');
					$xml.= $libxml->HandledCharacterForXML($category['DescriptionEn']);
					$xml.= $libxml->XML_CloseElement('CategoryName_EN');

					$xml.= $libxml->XML_OpenElement('CategoryName_CHI');
					$xml.= $libxml->HandledCharacterForXML($category['DescriptionChi']);
					$xml.= $libxml->XML_CloseElement('CategoryName_CHI');

					$xml.= $libxml->XML_OpenElement('CategoryDateModified');
					$xml.= $libxml->HandledCharacterForXML($category['DateModified']);
					$xml.= $libxml->XML_CloseElement('CategoryDateModified');

					$xml.= $libxml->XML_OpenElement('CategoryDisplayOrder');
					$xml.= $libxml->HandledCharacterForXML($category['DisplayOrder']);
					$xml.= $libxml->XML_CloseElement('CategoryDisplayOrder');

					$xml.= $libxml->XML_OpenElement('CategoryTotalAlbum');
					$xml.= $libxml->HandledCharacterForXML($category['AlbumTotal']);
					$xml.= $libxml->XML_CloseElement('CategoryTotalAlbum');

					if($category['Album']){
						foreach($category['Album'] as $albumKey => $album){

							$xml.= $libxml->XML_OpenElement('Album');

							$xml.= $libxml->XML_OpenElement('AlbumID');
							$xml.= $libxml->HandledCharacterForXML($album['id']);
							$xml.= $libxml->XML_CloseElement('AlbumID');

							$xml.= $libxml->XML_OpenElement('AlbumTitle');
							$xml.= $libxml->HandledCharacterForXML($album['title']);
							$xml.= $libxml->XML_CloseElement('AlbumTitle');

							$xml.= $libxml->XML_OpenElement('AlbumCoverUrl');
							$xml.= $libxml->HandledCharacterForXML($album['coverphoto']);
							$xml.= $libxml->XML_CloseElement('AlbumCoverUrl');

							$xml.= $libxml->XML_OpenElement('AlbumTotalPhoto');
							$xml.= $libxml->HandledCharacterForXML($album['totalPhoto']);
							$xml.= $libxml->XML_CloseElement('AlbumTotalPhoto');

							$xml.= $libxml->XML_OpenElement('AlbumInputDate');
							$xml.= $libxml->HandledCharacterForXML($album['date']);
							$xml.= $libxml->XML_CloseElement('AlbumInputDate');

							$xml.= $libxml->XML_OpenElement('Target');
							$xml.= $libxml->HandledCharacterForXML($album['Target']);
							$xml.= $libxml->XML_CloseElement('Target');

							$xml.= $libxml->XML_CloseElement('Album');
						}
					}
					$xml.= $libxml->XML_CloseElement('Category');
				}
				$xml.= $libxml->XML_CloseElement('Categories');
			}

			if($AlbumAry){
				$xml.= $libxml->XML_OpenElement('Albums');
				foreach($AlbumAry as $key => $album){

					$xml.= $libxml->XML_OpenElement('Album');
					$xml.= $libxml->XML_OpenElement('AlbumID');
					$xml.= $libxml->HandledCharacterForXML($album['AlbumID']);
					$xml.= $libxml->XML_CloseElement('AlbumID');

					$xml.= $libxml->XML_OpenElement('AlbumTitle');
					$xml.= $libxml->HandledCharacterForXML($album['Title']);
					$xml.= $libxml->XML_CloseElement('AlbumTitle');

					$xml.= $libxml->XML_OpenElement('AlbumDescription');
					$xml.= $libxml->HandledCharacterForXML($album['Description']);
					$xml.= $libxml->XML_CloseElement('AlbumDescription');

					if($sys_custom['eClassApp']['SFOC']){
					    $xml.= $libxml->XML_OpenElement('AlbumTitleZh');
					    $xml.= $libxml->HandledCharacterForXML($album['TitleZh']);
					    $xml.= $libxml->XML_CloseElement('AlbumTitleZh');
					    $xml.= $libxml->XML_OpenElement('AlbumDescriptionZh');
					    $xml.= $libxml->HandledCharacterForXML($album['DescriptionZh']);
					    $xml.= $libxml->XML_CloseElement('AlbumDescriptionZh');
					}

					$xml.= $libxml->XML_OpenElement('AlbumCoverUrl');
					$xml.= $libxml->HandledCharacterForXML($album['CoverUrl']);
					$xml.= $libxml->XML_CloseElement('AlbumCoverUrl');

					$xml.= $libxml->XML_OpenElement('AlbumTotalItem');
					$xml.= $libxml->HandledCharacterForXML($album['TotalItem']);
					$xml.= $libxml->XML_CloseElement('AlbumTotalItem');

					$xml.= $libxml->XML_OpenElement('Date');
					$xml.= $libxml->HandledCharacterForXML($album['Date']);
					$xml.= $libxml->XML_CloseElement('Date');

					if ($targetType == 2){
    					$xml.= $libxml->XML_OpenElement('Target');
    					$xml.= $libxml->HandledCharacterForXML($album['Target']); //album target: ALL, GROUP, PRIVATE
    					$xml.= $libxml->XML_CloseElement('Target');

    					$xml.= $libxml->XML_OpenElement('CategoryCode');
    					$xml.= $libxml->HandledCharacterForXML($album['CategoryCode']);
    					$xml.= $libxml->XML_CloseElement('CategoryCode');
					}

					$xml.= $libxml->XML_OpenElement('SharedSince');
					$xml.= $libxml->HandledCharacterForXML($album['SharedSince']);
					$xml.= $libxml->XML_CloseElement('SharedSince');

					$xml.= $libxml->XML_OpenElement('SharedUntil');
					$xml.= $libxml->HandledCharacterForXML($album['SharedUntil']);
					$xml.= $libxml->XML_CloseElement('SharedUntil');

					if($album['item']){
						foreach($album['item'] as $photoKey => $photo){
							if($photo['type'] == 'video'){
								$xml.= $libxml->XML_OpenElement('Video');
								$xml.= $libxml->XML_OpenElement('VideoID');
								$xml.= $libxml->HandledCharacterForXML($photo['PhotoID']);
								$xml.= $libxml->XML_CloseElement('VideoID');

								$xml.= $libxml->XML_OpenElement('VideoTitle');
								$xml.= $libxml->HandledCharacterForXML($photo['title']);
								$xml.= $libxml->XML_CloseElement('VideoTitle');

								$xml.= $libxml->XML_OpenElement('VideoDescription');
								$xml.= $libxml->HandledCharacterForXML($photo['description']);
								$xml.= $libxml->XML_CloseElement('VideoDescription');

								$xml.= $libxml->XML_OpenElement('VideoCoverURL');
								$xml.= $libxml->HandledCharacterForXML($photo['coverUrl']);
								$xml.= $libxml->XML_CloseElement('VideoCoverURL');

								$xml.= $libxml->XML_OpenElement('VideoDateModified');
								$xml.= $libxml->HandledCharacterForXML($photo['dateModify']);
								$xml.= $libxml->XML_CloseElement('VideoDateModified');

								$xml.= $libxml->XML_OpenElement('VideoURL');
								$xml.= $libxml->HandledCharacterForXML($photo['url']);
								$xml.= $libxml->XML_CloseElement('VideoURL');

								$xml.= $libxml->XML_OpenElement('Sequence');
								$xml.= $libxml->HandledCharacterForXML($photo['sequence']);
								$xml.= $libxml->XML_CloseElement('Sequence');

								if($sys_custom['eClassApp']['SFOC']){
								    $xml.= $libxml->XML_OpenElement('EventTitle');
								    $xml.= $libxml->HandledCharacterForXML($photo['EventTitle']);
								    $xml.= $libxml->XML_CloseElement('EventTitle');

								    $xml.= $libxml->XML_OpenElement('EventTitleZh');
								    $xml.= $libxml->HandledCharacterForXML($photo['EventTitleZh']);
								    $xml.= $libxml->XML_CloseElement('EventTitleZh');

								    $xml.= $libxml->XML_OpenElement('EventDate');
								    $xml.= $libxml->HandledCharacterForXML($photo['EventDate']);
								    $xml.= $libxml->XML_CloseElement('EventDate');

								    $xml.= $libxml->XML_OpenElement('VideoDescriptionZh');
								    $xml.= $libxml->HandledCharacterForXML($photo['descriptionZh']);
								    $xml.= $libxml->XML_CloseElement('VideoDescriptionZh');
								}
								$xml.= $libxml->XML_CloseElement('Video');
							}else{
								$xml.= $libxml->XML_OpenElement('Photo');
								$xml.= $libxml->XML_OpenElement('PhotoID');
								$xml.= $libxml->HandledCharacterForXML($photo['PhotoID']);
								$xml.= $libxml->XML_CloseElement('PhotoID');

								$xml.= $libxml->XML_OpenElement('PhotoTitle');
								$xml.= $libxml->HandledCharacterForXML($photo['title']);
								$xml.= $libxml->XML_CloseElement('PhotoTitle');

								$xml.= $libxml->XML_OpenElement('PhotoDescription');
								$xml.= $libxml->HandledCharacterForXML($photo['description']);
								$xml.= $libxml->XML_CloseElement('PhotoDescription');

								$xml.= $libxml->XML_OpenElement('PhotoCoverURL');
								$xml.= $libxml->HandledCharacterForXML($photo['coverUrl']);
								$xml.= $libxml->XML_CloseElement('PhotoCoverURL');

								$xml.= $libxml->XML_OpenElement('PhotoDateModified');
								$xml.= $libxml->HandledCharacterForXML($photo['dateModify']);
								$xml.= $libxml->XML_CloseElement('PhotoDateModified');

								$xml.= $libxml->XML_OpenElement('PhotoURL');
								$xml.= $libxml->HandledCharacterForXML($photo['url']);
								$xml.= $libxml->XML_CloseElement('PhotoURL');

								$xml.= $libxml->XML_OpenElement('Sequence');
								$xml.= $libxml->HandledCharacterForXML($photo['sequence']);
								$xml.= $libxml->XML_CloseElement('Sequence');

								if($sys_custom['eClassApp']['SFOC']){
								    $xml.= $libxml->XML_OpenElement('EventTitle');
								    $xml.= $libxml->HandledCharacterForXML($photo['EventTitle']);
								    $xml.= $libxml->XML_CloseElement('EventTitle');

								    $xml.= $libxml->XML_OpenElement('EventTitleZh');
								    $xml.= $libxml->HandledCharacterForXML($photo['EventTitleZh']);
								    $xml.= $libxml->XML_CloseElement('EventTitleZh');

								    $xml.= $libxml->XML_OpenElement('EventDate');
								    $xml.= $libxml->HandledCharacterForXML($photo['EventDate']);
								    $xml.= $libxml->XML_CloseElement('EventDate');

								    $xml.= $libxml->XML_OpenElement('PhotoDescriptionZh');
								    $xml.= $libxml->HandledCharacterForXML($photo['descriptionZh']);
								    $xml.= $libxml->XML_CloseElement('PhotoDescriptionZh');
								}

								$xml.= $libxml->XML_CloseElement('Photo');
							}
						}
					}

					$xml.= $libxml->XML_CloseElement('Album');

				}
				$xml.= $libxml->XML_CloseElement('Albums');
			}

			$xml.= $this->getXmlRootCloseNode();

			$xml = $this->utf8_for_xml($xml);

			return $xml;
		}


		function getAcademicYearTerm(){

		    $sql = "SELECT * FROM ACADEMIC_YEAR ORDER BY Sequence";
		    $academicYears = $this->returnResultSet($sql);

		    $sql = "SELECT * FROM ACADEMIC_YEAR_TERM ORDER BY TermStart";
		    $academicYearTerms = $this->returnResultSet($sql);

		    $currentDate = date('Y-m-d H:i:s');

		    $returnAry = array();
		    foreach($academicYears as $academicYear){
		        $terms = array();
		        $isCurrentYear = false;
		        $yearStart = '9999-99-99 99:99:99';
		        $yearEnd = '0000-00-00 00:00:00';
		        foreach($academicYearTerms as $academicYearTerm){
		            if($academicYearTerm['AcademicYearID'] != $academicYear['AcademicYearID']){
		                continue;
		            }

		            if( ($academicYearTerm['TermStart'] <= $currentDate) && ($academicYearTerm['TermEnd'] >= $currentDate) ){
		                $isCurrentYear = true;
		            }

		            if($academicYearTerm['TermStart'] < $yearStart){
		                $yearStart = $academicYearTerm['TermStart'];
		            }

		            if($academicYearTerm['TermEnd'] > $yearEnd){
		                $yearEnd = $academicYearTerm['TermEnd'];
		            }

		            $terms[] = array(
		                'YearTermID' => $academicYearTerm['YearTermID'],
		                'YearTermName_EN' => $academicYearTerm['YearTermNameEN'],
		                'YearTermName_CHI' => $academicYearTerm['YearTermNameB5'],
		                'TermStart' => $academicYearTerm['TermStart'],
		                'TermEnd' => $academicYearTerm['TermEnd'],
		            );
		        }

		        $returnAry[] = array(
		            'AcademicYearID' => $academicYear['AcademicYearID'],
		            'YearName_EN' => $academicYear['YearNameEN'],
		            'YearName_CHI' => $academicYear['YearNameB5'],
		            'YearStart' => $yearStart,
		            'YearEnd' => $yearEnd,
		            'Sequence' => $academicYear['Sequence'],
		            'isCurrentYear' => $isCurrentYear,
		            'Terms' => $terms
		        );
		    }

		    return $returnAry;

		}

    	/*
    	 * 	<eClassResponse>
    	 *	   <AcademicYears>
    	 *	      <AcademicYear>
    	 *	         <AcademicYearID> </AcademicYearID>
    	 *	         <YearName_EN> </YearName_EN>
    	 *	         <YearName_CHI> </YearName_CHI>
    	 *	         <Sequence> </Sequence>
    	 *	         <AcademicYearTerm>
    	 *	            <YearTermID> </YearTermID>
    	 *	            <YearTermName_EN> </YearTermName_EN>
    	 *	            <YearTermName_CHI> </YearTermName_CHI>
    	 *	            <TermStart> </TermStart>
    	 *	            <TermEnd> </TermEnd>
    	 *	         </AcademicYearTerm>
    	 *	         ...
    	 *	      </AcademicYear>
    	 *	      ...
    	 *	   </AcademicYears>
    	 *	</eClassResponse>
    	 */
    	function getAcademicYearTermXML(){
    		$this->removeAllError(); // clear errors

    		$libxml = $this->getInstanceLibXml();
    		$dataAry = $this->getAcademicYearTerm();

    		// if have error, inform user
    		if($this->errorCount() > 0) {
    			return $this->getErrorXml();
    		}

    		// construct announcement xml
    		$xml = $this->getXmlHeader();
    		$xml.= $this->getXmlRootOpenNode();

    		$xml.= $libxml->XML_OpenElement('AcademicYears');
    		foreach($dataAry as $data) {
    			$xml .= $libxml->XML_OpenElement('AcademicYear');

    			$fields = array('AcademicYearID', 'YearName_EN', 'YearName_CHI', 'YearStart', 'YearEnd', 'Sequence', 'isCurrentYear');
    			foreach($fields as $field){
        			$xml.= $libxml->XML_OpenElement($field);
        			$xml.= $libxml->HandledCharacterForXML( $data[$field] );
        			$xml.= $libxml->XML_CloseElement($field);
    			}

    			foreach($data['Terms'] as $terms){
    			    $xml.= $libxml->XML_OpenElement('AcademicYearTerm');

    			    $fields = array('YearTermID', 'YearTermName_EN', 'YearTermName_CHI', 'TermStart', 'TermEnd');
    			    foreach($fields as $field){
    			        $xml.= $libxml->XML_OpenElement($field);
    			        $xml.= $libxml->HandledCharacterForXML( $terms[$field] );
    			        $xml.= $libxml->XML_CloseElement($field);
    			    }

        			$xml.= $libxml->XML_CloseElement('AcademicYearTerm');
    			}

    			$xml .= $libxml->XML_CloseElement('AcademicYear');
    		}
    		$xml.= $libxml->XML_CloseElement('AcademicYears');
    		$xml.= $this->getXmlRootCloseNode();
    		$xml = $this->utf8_for_xml($xml);

    		return $xml;
    	}

    	function getSchoolInfo($AssociationName=''){
    	    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language,$sys_custom;

    	    $conds = "";

    	    if($AssociationName !=''){
    	        $conds .= "AND (TitleChi = '{$AssociationName}' OR TitleEng = '{$AssociationName}')";
    	    }

    	    $sql = " SELECT
                        MenuID,
                        TitleChi,
                        TitleEng,
                        Description,
                        Icon,
                        IsItem,
                        Url,
                        DescriptionEng
                    FROM SCHOOL_INFO
                    WHERE 1 AND RecordStatus != 0 $conds
                    ORDER BY DisplayOrder

    	     ";
    	    $dataAry = $this->returnResultSet($sql);

    	    return $dataAry;
    	}

    	function getSchoolInfoXml($AssociationName='')
    	{
    	    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

    	    $this->removeAllError(); // clear errors

    	    $libxml = $this->getInstanceLibXml();
    	    $dataAry = $this->getSchoolInfo($AssociationName);

    	    // if have error, inform user
    	    if($this->errorCount() > 0) {
    	        return $this->getErrorXml();
    	    }

    	    // construct announcement xml
    	    $xml = $this->getXmlHeader();
    	    $xml.= $this->getXmlRootOpenNode();

    	    $xml.= $libxml->XML_OpenElement('SchoolInfos');
    	    $data_count = count($dataAry);
    	    for($i=0;$i<$data_count;$i++) {
    	        $xml .= $libxml->XML_OpenElement('SchoolInfo');
    	        $xml .= $libxml->Array_To_XML($dataAry[$i], true);
    	        $xml .= $libxml->XML_CloseElement('SchoolInfo');
    	    }
    	    $xml.= $libxml->XML_CloseElement('SchoolInfos');
    	    $xml.= $this->getXmlRootCloseNode();
    	    $xml = $this->utf8_for_xml($xml);

    	    return $xml;
    	}

    	function getChildSchoolInfoList($ParentMeunID=''){
    	    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language,$sys_custom;

    	    $conds = "";

    	    if($ParentMeunID!=''){
    	        $conds .= "AND ParentMenuID='$ParentMeunID'";
    	    }

    	    $sql = " SELECT
                	    MenuID,
                	    TitleChi,
                	    TitleEng,
                	    Description,
                	    Icon,
                	    IsItem,
                	    Url,
                	    DescriptionEng
            	    FROM SCHOOL_INFO
            	    WHERE 1 AND RecordStatus != 0 $conds
            	    ORDER BY DisplayOrder

    	    ";
    	    $dataAry = $this->returnResultSet($sql);

    	    return $dataAry;
    	}

    	function getChildSchoolInfoListXml($ParentMeunID='')
    	{
    	    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

    	    $this->removeAllError(); // clear errors

    	    $libxml = $this->getInstanceLibXml();
    	    $dataAry = $this->getChildSchoolInfoList($ParentMeunID);

    	    // if have error, inform user
    	    if($this->errorCount() > 0) {
    	        return $this->getErrorXml();
    	    }

    	    // construct announcement xml
    	    $xml = $this->getXmlHeader();
    	    $xml.= $this->getXmlRootOpenNode();

    	    $xml.= $libxml->XML_OpenElement('ChildSchoolInfos');
    	    $data_count = count($dataAry);
    	    for($i=0;$i<$data_count;$i++) {
    	        $xml .= $libxml->XML_OpenElement('ChildSchoolInfo');
    	        $xml .= $libxml->Array_To_XML($dataAry[$i], true);
    	        $xml .= $libxml->XML_CloseElement('ChildSchoolInfo');
    	    }
    	    $xml.= $libxml->XML_CloseElement('ChildSchoolInfos');
    	    $xml.= $this->getXmlRootCloseNode();

    	    return $xml;
    	}

    	function getMedalListXml($SortBy='',$Year='',$LocationName='',$GameName='',$EventName='',$SportName='',$AthleteName='',$Medal=''){
    	    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language;

    	    $this->removeAllError(); // clear errors

    	    $libxml = $this->getInstanceLibXml();
    	    $dataAry = $this->getMedalListRecord($SortBy,$Year,$LocationName,$GameName,$EventName,$SportName,$AthleteName,$Medal);
//     	    $dataAry = array($dataAry[0]);

    	    // if have error, inform user
    	    if($this->errorCount() > 0) {
    	        return $this->getErrorXml();
    	    }

    	    // construct announcement xml
    	    $xml = $this->getXmlHeader();
    	    $xml.= $this->getXmlRootOpenNode();

    	    $xml.= $libxml->XML_OpenElement('MedalLists');
    	    $data_count = count($dataAry);
    	    for($i=0;$i<$data_count;$i++) {
    	        $xml .= $libxml->XML_OpenElement('MedalList');
    	        $xml .= $libxml->Array_To_XML($dataAry[$i], true);
    	        $xml .= $libxml->XML_CloseElement('MedalList');
    	    }
    	    $xml.= $libxml->XML_CloseElement('MedalLists');
    	    $xml.= $this->getXmlRootCloseNode();

    	    return $xml;
    	}
    	function getMedalListRecord($SortBy='',$Year='',$LocationName='',$GameName='',$EventName='',$SportName='',$AthleteName='',$Medal=''){
    	    global $intranet_root, $file_path, $plugin, $school_domain, $eClassAPIConfig, $PATH_WRT_ROOT, $intranet_session_language,$sys_custom;
    	    $conds = "";
    	    if($SortBy =='Year'){
    	        $Order ="ORDER BY smlr.Year";
    	    }
    	    elseif($SortBy =='Games'){
    	        $Order ="ORDER BY sgs.GameEngName,sgs.GameChiName";
    	    }
    	    elseif($SortBy =='Medal'){
    	        $Order ="ORDER BY smlr.Medal";
    	    }
    	    elseif($SortBy =='Sport'){
    	        $Order ="ORDER BY sst.SportEngName,sst.SportChiName";
    	    }
    	    elseif($SortBy =='Event'){
    	        $Order ="ORDER BY ses.EventEngName,ses.EventChiName";
    	    }
    	    elseif($SortBy =='Nameofathlete'){
    	        $Order ="ORDER BY smlr.NameEng,smlr.NameChi";
    	    }

    	    if($Year !=''){
    	        $conds .="AND smlr.Year = '$Year' ";
    	    }

    	    if($LocationName !=''){
    	        $conds .="AND (smlr.LocationChi = '$LocationName' OR smlr.LocationEng = '$LocationName')";
    	    }

    	    if($GameName!=''){
    	        $conds .="AND (sgs.GameChiName = '$GameName' OR sgs.GameEngName = '$GameName') ";
    	    }

    	    if($EventName!=''){
    	        $conds .="AND (ses.EventChiName = '$EventName' OR ses.EventEngName = '$EventName')";
    	    }

    	    if($SportName!=''){
    	        $conds .="AND (sst.SportChiName = '$SportName' OR sst.SportEngName = '$SportName')";
    	    }

    	    if($AthleteName!=''){
    	        $conds .="AND (smlr.NameChi = '$AthleteName' OR smlr.NameEng = '$AthleteName')";
    	    }

    	    if($Medal == 'G'){
    	        $conds .="AND smlr.Medal= '1' ";
    	    }else if($Medal == 'S'){
    	        $conds .="AND smlr.Medal= '2' ";
    	    }else if($Medal == 'B'){
    	        $conds .="AND smlr.Medal= '3' ";
    	    }

    	    $sql = "SELECT
                	    smlr.RecordID,
                	    smlr.Year,
                	    smlr.LocationChi,
                	    smlr.LocationEng,
                	    sgs.GameChiName,
                	    sgs.GameEngName,
                	    ses.EventChiName,
                	    ses.EventEngName,
                	    sst.SportChiName,
                	    sst.SportEngName,
                	    smlr.NameChi,
                	    smlr.NameEng,
                	    smlr.Medal,
    	                smlr.WebsiteCode
                	FROM  SFOC_MEDAL_LIST_RECORD AS smlr
                	    INNER JOIN SFOC_GAMES_SETTING AS sgs ON (smlr.GameID = sgs.GameID)
                	    INNER JOIN SFOC_EVENTS_SETTING AS ses ON (smlr.EventID = ses.EventID)
                	    INNER JOIN SFOC_SPORTS_TYPE AS sst ON (smlr.SportID = sst.SportID)
                	WHERE
                	    1 $conds  $Order";

    	    $dataAry = $this->returnResultSet($sql);

    	    return $dataAry;
    	}

    	function getYearClass($api_academic_year=''){
    	    if($api_academic_year){
    	        $cond = " AND YC.AcademicYearID='{$api_academic_year}'";
    	    }

    	    $sql = "SELECT
    	        YC.YearClassID,
    	        YC.AcademicYearID,
    	        YC.YearID,
    	        YC.ClassTitleEN,
    	        YC.ClassTitleB5,
    	        YC.WEBSAMSCode AS YC_WEBSAMSCode,
    	        0 AS course_id,
    	        YC.Sequence AS YC_Sequence,
    	        Y.YearName,
    	        Y.WEBSAMSCode AS Y_WEBSAMSCode,
    	        Y.Sequence AS Y_Sequence
	        FROM
    	        YEAR_CLASS YC
	        INNER JOIN
    	        YEAR Y
	        ON
    	        YC.YearID = Y.YearID
	        INNER JOIN
    	        ACADEMIC_YEAR AY
	        ON
    	        YC.AcademicYearID = AY.AcademicYearID
	        WHERE
    	        1=1
    	        {$cond}
	        ORDER BY
	            AY.Sequence, Y.Sequence, YC.Sequence";
	        $rs = $this->returnResultSet($sql);

	        return $rs;
    	}

    	function getYearClassXML($api_academic_year=''){


    	    $this->removeAllError(); // clear errors

    	    $libxml = $this->getInstanceLibXml();
    	    $dataAry = $this->getYearClass($api_academic_year);

    	    // if have error, inform user
    	    if($this->errorCount() > 0) {
    	        return $this->getErrorXml();
    	    }

    	    // construct announcement xml
    	    $xml = $this->getXmlHeader();
    	    $xml.= $this->getXmlRootOpenNode();

    	    $xml.= $libxml->XML_OpenElement('YearClasses');
    	    foreach($dataAry as $d1) {
    	        $xml .= $libxml->XML_OpenElement('YearClass');

    	        foreach($d1 as $key=>$value){
    	            $xml.= $libxml->XML_OpenElement($key);
    	            $xml.= $libxml->HandledCharacterForXML($value);
    	            $xml.= $libxml->XML_CloseElement($key);
    	        }

    	        $xml .= $libxml->XML_CloseElement('YearClass');
    	    }
    	    $xml.= $libxml->XML_CloseElement('YearClasses');
    	    $xml.= $this->getXmlRootCloseNode();
    	    $xml = $this->utf8_for_xml($xml);

    	    return $xml;
    	}

    	function getSubjectGroup($api_academic_year=''){
    	    global $PATH_WRT_ROOT;
    	    include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
    	    include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

    	    if($api_academic_year == ''){
    	        $rs = getAcademicYearAndYearTermByDate(date('Y-m-d'));
    	        $YearTermID = $rs['YearTermID'];
    	    }else{
        	    $academic_year = new academic_year($api_academic_year);
        	    $rs = $academic_year->Get_Term_List();
        	    $YearTermID = array_keys($rs);
        	    $YearTermID = $YearTermID[0];
    	    }

    	    $subject = new subject();
    	    $rs = $subject->Get_All_Subjects();
    	    $allSubjectMapping = BuildMultiKeyAssoc($rs, array('RecordID'));
    	    $rs = $subject->Get_Subject_Group_List($YearTermID);
    	    $subjectGroupList = array();

    	    foreach($rs as $r){
    	        $subject = $allSubjectMapping[$r['RecordID']];
    	        $subjectGroupList[] = array(
    	            'SubjectID' => $subject['RecordID'],
    	            'SubjectTitleCH' => $subject['CH_DES'],
    	            'SubjectTitleEN' => $subject['EN_DES'],
    	            'SubjectGroupID' => $r['SubjectGroupID'],
    	            'SubjectComponentID' => $r['SubjectComponentID'],
    	            'SubjectGroupTitleCH' => $r['ClassTitleB5'],
    	            'SubjectGroupTitleEN' => $r['ClassTitleEN'],
    	        );
    	    }

	        return $subjectGroupList;
    	}

    	function getSubjectGroupXML($api_academic_year=''){


    	    $this->removeAllError(); // clear errors

    	    $libxml = $this->getInstanceLibXml();
    	    $dataAry = $this->getSubjectGroup($api_academic_year);

    	    // if have error, inform user
    	    if($this->errorCount() > 0) {
    	        return $this->getErrorXml();
    	    }

    	    // construct announcement xml
    	    $xml = $this->getXmlHeader();
    	    $xml.= $this->getXmlRootOpenNode();

    	    $xml.= $libxml->XML_OpenElement('SubjectGroups');
    	    foreach($dataAry as $d1) {
    	        $xml .= $libxml->XML_OpenElement('SubjectGroup');

    	        foreach($d1 as $key=>$value){
    	            $xml.= $libxml->XML_OpenElement($key);
    	            $xml.= $libxml->HandledCharacterForXML($value);
    	            $xml.= $libxml->XML_CloseElement($key);
    	        }

    	        $xml .= $libxml->XML_CloseElement('SubjectGroup');
    	    }
    	    $xml.= $libxml->XML_CloseElement('SubjectGroups');
    	    $xml.= $this->getXmlRootCloseNode();
    	    $xml = $this->utf8_for_xml($xml);

    	    return $xml;
    	}

    	function utf8_for_xml($string)
    	{
    	    return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
    	}
	} // End libeclass_api()
}
?>