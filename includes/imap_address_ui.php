<?php
// Modifying by:

require_once('UserInterface.php');

class imap_address_ui extends UserInterface {

	function imap_address_ui() {

		Global $Lang;
		// temp lang data, will be moved to en.php
		$Lang['email']['ImportContact'] = "Import Contact";
		$Lang['email']['ImportContacts'] = "Import External Contacts";
		$Lang["email"]["select_file"] = "Select a CSV file to import:";
		$Lang["email"]["download_sample"] = "Download Sample";
		$Lang["email"]["download_notice"] = "2. The file must be in a specified format. Please download the below sample for reference.";

		$Lang["email"]["fail_to_import_contact"] = "Fail to import contact";
		$Lang["email"]["wrong_file_format"] = "Wrong file format";
		$Lang["email"]["import_contact_finished"] = "Import contact finished";
		$Lang['email']['ExportContact'] = "Export Contact";
		$Lang["email"]["empty_record"] = "No record";
		$Lang["email"]["note"] = "Note:";
		$Lang["email"]["import_notice"] = "1. All contacts in the csv file MUST have First Name, Surname & Email Address for the import to function properly.";
		$Lang["email"]["import_outlook_notice1"] = "1. You could export the Outlook 2003 / 2007 CSV (Dos) version and import into TG.";
		$Lang["email"]["import_outlook_notice2"] = "2. All contacts in the csv file MUST have First Name, Last Name & Email Address for the import to function properly.";
		$Lang["email"]["import_outlook_notice3"] = "3. The file must be in a specified format. Please download the below sample for reference.";

		parent::UserInterface();
	}

	// Get Read email interface
	/*function Get_Read_Mail($Mail) {
		Global $Lang, $SYS_CONFIG;
		Global $Folder, $MessageID;

		// Subject/ Receive Date
		$x  = $this->Get_Div_Open("commontabs_board",'CLASS="imail_mail_content"');
		$x .= $this->Get_Span_Open("read_mail_subject").$Mail[0]["subject"].$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("read_mail_daytime").$Mail[0]["dateReceive"].$this->Get_Span_Close();
		$x .= "<br>";

		//from
		$fromList = $Mail[0]["fromDetail"];
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['From'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_content");
		for ($i=0; $i< sizeof($fromList); $i++) {
			if (!isset($fromList[$i]->personal)) $DisplayName = $fromList[$i]->mailbox."@".$fromList[$i]->host;
			else $DisplayName = $fromList[$i]->personal;

			$Address = $fromList[$i]->mailbox."@".$fromList[$i]->host;

			$x .= $this->Get_HyperLink_Open($Address,'CLASS="imail_entry_unread_link"');
			$x .= $DisplayName;
			$x .= $this->Get_HyperLink_Close();
			$x .= ";";
		}
		$x .= $this->Get_Span_Close();
		$x .= "<br>";

		//to
		$toList = $Mail[0]["toDetail"];
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['To'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_content");
		$LoopCount = (sizeof($toList) < 5)? sizeof($toList):5;
		for ($i=0; $i< $LoopCount; $i++) {
			if (!isset($toList[$i]->personal)) $DisplayName = $toList[$i]->mailbox."@".$toList[$i]->host;
			else $DisplayName = $toList[$i]->personal;

			$Address = $toList[$i]->mailbox."@".$toList[$i]->host;

			$x .= $this->Get_HyperLink_Open($Address,'CLASS="imail_entry_read_link"');
			$x .= $DisplayName;
			$x .= $this->Get_HyperLink_Close();
			$x .= "; ";
		}

		if (sizeof($toList) > 5) {
			$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
			$x .= " ...";
			$x .= $this->Get_HyperLink_Close();
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Open("mail_form_label_button");
			$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
			$x .= $Lang['email']['ShowAll'];
			$x .= $this->Get_HyperLink_Close();
			$x .= $this->Get_Span_Close();
		}
		else {
			$x .= $this->Get_Span_Close();
		}
		$x .= '<br>';
		//end of to

		//CC
		$ccList = $Mail[0]["ccDetail"];
		if (sizeof($ccList) > 0) {
			$x .= $this->Get_Span_Open("mail_form_label");
			$x .= $Lang['email']['Cc'];
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Open("mail_form_label_content");
			$LoopCount = (sizeof($ccList) < 5)? sizeof($ccList):5;
			for ($i=0; $i< $LoopCount; $i++) {
				if (!isset($ccList[$i]->personal)) $DisplayName = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				else $DisplayName = $ccList[$i]->personal;

				$Address = $ccList[$i]->mailbox."@".$ccList[$i]->host;

				$x .= $this->Get_HyperLink_Open($Address,'CLASS="imail_entry_read_link"');
				$x .= $DisplayName;
				$x .= $this->Get_HyperLink_Close();
				$x .= "; ";
			}
			if (sizeof($ccList) > 5) {
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
				$x .= " ...";
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Open("mail_form_label_button");
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
				$x .= $Lang['email']['ShowAll'];
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();
			}
			else {
				$x .= $this->Get_Span_Close();
			}
		}
		$x .= '<br>';
		//end of CC

		//attachment
		$Attachment = $Mail[0]["attach_parts"];
		if (!(is_null($Attachment) || $Attachment == "")) {
			$LoopCount = (sizeof($Attachment) > 5)? 5:sizeof($Attachment);
			$x .= $this->Get_Span_Open("mail_form_attach");
			$x .= $this->Get_Span_Open("mail_form_label_content_line");
			for ($i=0; $i< $LoopCount; $i++) {
				$x .= $this->Get_HyperLink_Open("view_attachment.php?AttachNum=".$i."&Folder=".$Folder."&MessageID=".$MessageID,'CLASS="imail_entry_read_link"');
				$x .= $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'imail/icon_attachment.gif',12,18,'BORDER="0" ALIGN="absmiddle"');
				$x .= $Attachment[$i]['filename'];
				$x .= $this->Get_HyperLink_Close();
				$x .= "; ";
			}
			$x .= $this->Get_Span_Close();
			if (sizeof($Attachment) > 5) {
				$x .= $this->Get_Span_Open("mail_form_label_button");
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
				$x .= $Lang['email']['DownloadAll'];
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();

			}
			$x .= $this->Get_Span_Close();
		}
		$x .= $this->Get_Div_Close();

		// Mail Body
		$x .= $this->Get_Div_Open("mail_form_content_board");
		$x .= $this->Get_IFrame("0","MailBody","MailBody","auto","100%","100%","view_body.php?Folder=".$Folder."&MessageID=".$MessageID,"0","0");
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();

		return $x;
	}*/

	// get left menu of mail subsystem
	/*function Get_Left_Sub_Mail_Menu($CurMenu="",$CurTag="",$IMap=NULL,$Folder="") {
		global $Lang, $PathRelative, $CurPage, $SYS_CONFIG;

		// Ajax form
		$x = '
					<!-- YUI Panel -->
					<link rel="stylesheet" type="text/css" href="'.$PathRelative.'src/include/js/ajax/build/container/assets/skins/sam/container.css">
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/yahoo-dom-event/yahoo-dom-event.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/dragdrop/dragdrop-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/container/container-min.js"></script>
				 ';

		$x .= $this->Get_Div_Open("NewFolderPanel");
		$x .= $this->Get_Form_Open("NewFolderForm","POST","new_folder_update.php");
		$x .= $this->Get_Div();
		$x .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03"').$this->Get_Span_Open("",'CLASS="bubble_board_04"');
		$x .= $Lang['email']['NewFolderPanelHeader']."<br><br>";
		$x .= $this->Get_Div_Open("",'style="width:90%"');
		$x .= $this->Get_Input_Text("FolderName","",'class="textbox"')."<br>";
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_save'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myPanel.hide()" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Input_Hidden("CurFolder","CurFolder",$Folder);
		$x .= $this->Get_Input_Hidden("CurTag","CurTag",$CurTag);
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		$x .= '
					<script>
						var myPanel = new YAHOO.widget.Panel("NewFolderPanel", { width:"240px", visible:false, constraintoviewport:true });

						myPanel.render(document.body);

					function showNewFolderPanel(obj) {
						var pos_left = getPostion(obj,"offsetLeft");
						var pos_top  = getPostion(obj,"offsetTop");

						myPanel.cfg.setProperty("x", pos_left);
						myPanel.cfg.setProperty("y", pos_top);

						myPanel.show();
					}
					</script>
					<!-- End of YUI Panel -->
				 ';
		// end of Ajax form

		// top button
		$x .= $this->Get_Div_Open("imail_left");
		$x .= $this->Get_Div_Open("mail_list", 'class="imail_left_function"');
		$x .= $this->Get_Div_Open("imail_left_function_item");
		$x .= $this->Get_Span_Open("", 'style="text-align:center"');
		$x .= $this->Get_Input_Button("button", "check_mail", $Lang['email']['CheckMail'], 'onclick="window.location.reload();" class="button_main_act button_imail_menu" ').'&nbsp;';
		$x .= $this->Get_Input_Button("button", "compose_mail", $Lang['email']['ComposeMail'], 'onclick="window.location = \'compose_email.php\'" class="button_main_act button_imail_menu"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif', 150, 1);
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();

		// mail box list
		$x .= $this->Get_Div_Open("mail_list", ($CurMenu == '1')? 'class="imail_left_function_selected"':'class="imail_left_function_item"');
		$x .= $this->Get_Div_Open("imail_search");
		$x .= $this->Get_Div_Open("",'style="width:75px;float:left;padding:2px; height: 22px;"');
		$x .= $this->Get_Input_Text("Keywords", "", 'class="textbox"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("",'style="width:30px;float:left;padding-bottom:5px; height: 22px; padding-top:3px"');
		$x .= $this->Get_Input_Button("Go", "button", $Lang['email']['Go'], 'class="button button_go"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("",'style="width:70px;float:left;padding:2px;padding-left:2px; padding-top:7px; height: 22px;"');
		$x .= $this->Get_Span_Open("",'CLASS="imail_menu_adv_search"')."|";
		$x .= $this->Get_HyperLink_Open("imail_advanced_search.htm");
		$x .= $Lang['email']['AdvSearch'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("imail_left_function_item");
		$x .= $this->Get_List_Open();
		$x .= $this->Get_List_Member($this->Get_Span_Open().$Lang['email']['MailFolder'].$this->Get_Span_Close(),  'ID="subtitle"');
		// get short info number of unseen message in folder
		$DisplayUnseen = $IMap->Get_Unseen_Mail_Number("INBOX");
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=1", 'class="imail_inbox"').$this->Get_Span_Open().$Lang['email']['Inbox']." ".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == '1')? 'ID="current"':'');
		// get short info number of unseen message in folder
		$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Sent");
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=2&Folder=".$IMap->RootPrefix."Sent",'CLASS="imail_sent"').$this->Get_Span_Open().$Lang['email']['Sent']." ".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == '2')? 'ID="current"':'');
		// get short info number of unseen message in folder
		$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Draft");
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=3&Folder=".$IMap->RootPrefix."Draft",'CLASS="imail_draft"').$this->Get_Span_Open().$Lang['email']['Draft']." ".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == '3')? 'ID="current"':'');
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=4",'CLASS="imail_spam"').$this->Get_Span_Open().$Lang['email']['Spam']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->Get_Input_Button("Go","button",'['.$Lang['email']['Clear'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_clear"').$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == '4')? 'ID="current"':'');
		// get short info number of unseen message in folder
		$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Trash");
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=5&Folder=".$IMap->RootPrefix."Trash",'CLASS="imail_trash"').$this->Get_Span_Open().$Lang['email']['Trash']." ".$DisplayUnseen."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->Get_Input_Button("Go","button",'['.$Lang['email']['Clear'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_clear"').$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == '5')? 'ID="current"':'');
		$x .= $this->Get_List_Member($this->Get_Span_Open().$Lang['email']['PersonalFolder']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->Get_Input_Button("Go","button",'['.$Lang['email']['FolderNew'], 'OnClick="showNewFolderPanel(this)" class="imail_menu_sub_btn imail_menu_sub_btn_new"')."|".$this->Get_Input_Button("Go","button",$Lang['email']['FolderManage'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_manage" onClick="MM_goToURL(\'parent\',\'imail_myfolder.php\');return document.MM_returnValue"').$this->Get_Span_Close(), 'id="subtitle"');
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=6",'class="imail_folder"').$this->Get_Span_Open().'...'.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && ($CurTag == 6 || $CurTag == ""))? 'id="current"':'');

		$TabSpacing = 12;
		if ($IMap!="") {
			$FolderList = $IMap->Get_Folder_List_In_Path($Folder);
			if ($Folder != "") {
				$FolderPath = explode(".", $Folder);
			}

			// folder path
			for ($i=0; $i< sizeof($FolderPath); $i++) {
				// get folder full path for back navigation
				if ($i == 0) {
					$FolderFullPath .= $FolderPath[$i];
				}
				else {
					$FolderFullPath .= ".".$FolderPath[$i];
				}

				// get short info number of unseen message in folder
				$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($FolderFullPath);

				$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=".$FolderPath[$i]."&Folder=".$FolderFullPath,'class="imail_folder" style="background-position:'.$TabSpacing.'px 2px"').$this->Get_Span_Open("",'style="padding-left:'.($TabSpacing+15).'px"').$FolderPath[$i]."..".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $FolderPath[$i])? 'ID="current"':'');

				$TabSpacing = $TabSpacing + 10;
			}

			// folder list
			for ($i=0; $i< sizeof($FolderList); $i++) {
				$FolderDisplayName = $IMap->getMailDisplayFolder($FolderList[$i]);

				// get short info number of unseen message in folder
				$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($FolderList[$i]);

				$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_inbox.php?CurTag=".$FolderDisplayName."&Folder=".$FolderList[$i],'class="imail_folder" style="background-position:'.$TabSpacing.'px 2px"').$this->Get_Span_Open("",'style="padding-left:'.($TabSpacing+15).'px"').$FolderDisplayName." ".$DisplayUnseen.$this->Get_Span_Close.$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $FolderDisplayName)? 'ID="current"':'');
			}
		}
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif',150,1);
		$x .= $this->Get_Div_Close();

		//address book
		$x .= $this->Get_Div_Open("addressbook", ($CurMenu == '2')? 'class="imail_left_function_selected"':'class="imail_left_function_item"');
		$x .= $this->Get_Div_Open("imail_left_function_item");
		$x .= $this->Get_List_Open();
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_address_book.php",'class="imail_address_book"').$this->Get_Span_Open().$Lang['email']['AddressBook'].$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '2' && $CurTag == '1')? 'ID="current"':'');
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif',150,1);
		$x .= $this->Get_Div_Close();

		// perference
		$x .= $this->Get_Div_Open("preference",($CurMenu == '3')? 'class="imail_left_function_selected"':'class="imail_left_function_item"');
		$x .= $this->Get_Div_Open("imail_left_function_item");
		$x .= $this->Get_List_Open();
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open("imail_preference.php",'class="imail_preference"').$this->Get_Span_Open().$Lang['email']['Preference'].$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '3' && $CurTag == '1')? 'ID="current"':'');
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif',150,1);
		$x .= $this->Get_Div_Close();
		$x .= "&nbsp;";
		$x .= $this->Get_Div_Close();

		return $x;
	}*/

	// get tool button bar of Mail system
	/*function Get_Tool_Bar($LeftMenu=array(),$RightMenu=array(),$MainID="",$LeftID="",$RightID="") {
		$MainID = ($MainID=="")? "imail_toolbar":$MainID;
		$LeftID = ($LeftID=="")? "imail_toolbar_right":$LeftID;
		$RightID = ($RightID=="")? "imail_toolbar_left":$RightID;
		$x .= $this->Get_Div_Open($MainID);
		$x .= $this->Get_Div_Open($LeftID);
		for ($i=0; $i< sizeof($LeftMenu); $i++) {
			$x .= $LeftMenu[$i]." ";
		}
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open($RightID);
		for ($i=0; $i< sizeof($RightMenu); $i++) {
			$x .= $RightMenu[$i]." ";
		}
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();

		return $x;
	}*/

	/*function Get_Compose_Mail_Form($Mail="") {
		global $SYS_CONFIG, $Lang;

		$toList = $Mail[0]['toDetail'];
		for ($i=0; $i< sizeof($toList); $i++) {
			$toAddress .= $toList[$i]->mailbox."@".$toList[$i]->host."; ";
		}

		$ccList = $Mail[0]['ccDetail'];
		for ($i=0; $i< sizeof($ccList); $i++) {
			$ccAddress .= $ccList[$i]->mailbox."@".$ccList[$i]->host."; ";
		}

		$bccList = $Mail[0]['bccDetail'];
		for ($i=0; $i< sizeof($bccList); $i++) {
			$bccAddress .= $bccList[$i]->mailbox."@".$bccList[$i]->host."; ";
		}

		$Subject = $Mail[0]['subject'];
		$Body = $Mail[0]['message'];
		$Priority = ($Mail[0]['Priority'] == 1)? true:false;

		$x = $this->Get_Div_Open("commontabs_board",'CLASS="imail_mail_content"');
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['To'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		$x .= $this->Get_Input_Text("ToAddress",$toAddress,'CLASS="textbox"');
		$x .= $this->Get_Span_Open("mail_form_label_content_line");
		$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
		$x .= $Lang['email']['AddCc'];
		$x .= $this->Get_HyperLink_Close();
		$x .= " | ";
		$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
		$x .= $Lang['email']['AddBcc'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_button");
		$x .= $this->Get_HyperLink_Open("#");
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'BORDER="0"');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= "<br>";
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['Subject'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		$x .= $this->Get_Input_Text("Subject",$Subject,'CLASS="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= "<br>";

		// hidden Iframe to update the attachment list periodically
		$x .= $this->Get_IFrame("0","","","no","0","0","attach_list_refresh.php?uid=".$Mail[0]['uid'],"0","0",'style="visibility:visible;"');

		// attachment part
		$x .= $this->Get_Span_Open("mail_form_attach",'style="visibility:hidden"');
		$x .= $this->Get_Span_Open("mail_form_label_content_whole2");
		$x .= $this->Get_Div_Open("added_item_attachment");
		$x .= $this->Get_Span_Open("AttachmentList");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= '<br style="clear:left">';
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="window.open(\'attach.php\')"');
		$x .= $Lang['email']['AttachAnotherFile'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();

		$x .= $this->Get_Span_Open("mail_form_label_content_whole",'style="visibility:visible"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_attachment.gif",12,18,'BORDER="0" ALIGN="absmiddle"');
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="window.open(\'attach.php\')"');
		$x .= $Lang['email']['AttachFile'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();

		$x .= "<br>";

		$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_event.gif",16,20,'BORDER="0" ALIGN="absmiddle"');
		$x .= $this->Get_HyperLink_Open("imail_compose_mail_wrote.htm",'class="imail_entry_read_link"');
		$x .= $Lang['email']['AddEventInvitation'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		$x .= $this->Get_Div_Open("mail_form_content_board");
		//$x .= $this->Get_Div_Open("",'CLASS="mail_msg"');

		//text editor of email body
		$x .= $this->Get_Word_Editor("MailBody","MailBody",$Body);

		//$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= "<br>";
		$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		$x .= $this->Get_Input_CheckBox("ImportantFlag","ImportantFlag",1,$Priority);
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_important.gif",9,16,'align="absmiddle"');
		$x .= $Lang['email']['Important'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();

		return $x;
	}*/


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Newly added items
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function Get_Import_Layer($AddType="user", $CacheFormat=false)
	{
		Global $Lang, $PathRelative, $CurPage, $SYS_CONFIG;

		if ($CacheFormat)
			$ActionPage = "cache_import_contact_process.php?AddType=".$AddType;
		else
			$ActionPage = "import_contact_process.php?AddType=".$AddType;

		// Ajax form
		$x = '
					<!-- YUI Panel -->
					<link rel="stylesheet" type="text/css" href="'.$PathRelative.'src/include/js/ajax/build/container/assets/skins/sam/container.css">
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/yahoo-dom-event/yahoo-dom-event.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/dragdrop/dragdrop-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/container/container-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/jquery/jquery.js"></script>
				 ';
		// target="upload_target"
		$x .= $this->Get_Div_Open("ImportPanel");
		$x .= $this->Get_Form_Open("ImportForm","POST",$ActionPage, 'target="upload_target" enctype="multipart/form-data" onSubmit="return jImportContactForm(this);"');
		$x .= $this->Get_Div_Open();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03"').$this->Get_Span_Open("",'CLASS="bubble_board_04"');
		$x .= $Lang["email"]["select_file"];
		$x .= $this->Get_Br();
		$x .= $this->Get_Br();
		$x .= $this->Get_Div_Open("Import_Warning");
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Br();
		$x .= $this->Get_Div_Open("FileDiv");
		$x .= $this->Get_Input_File("ImportFile","ImportFile","");
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Br();
		$x .= $Lang["email"]["note"];
		$x .= $this->Get_Br();
		$x .= $Lang["email"]["import_notice"];
		$x .= $this->Get_Br();
		$x .= $this->Get_Br();
		$x .= $Lang["email"]["download_notice"];
		$x .= $this->Get_Br();

		$x .= $this->Get_Div_Open("btn_link");
		$x .= $this->Get_HyperLink_Open("view_attachment.php");
		$x .= $this->Get_Image($SYS_CONFIG["Image"]["Abs"]."xls.gif","","",'BORDER="0" ALIGN="absmiddle"');
		$x .= $Lang["email"]["download_sample"];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Br('style="clear:both"');
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_submit'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myImportPanel.hide();" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();

		$x .= '<iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>';

		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		$x .= '
					<script>
						var myImportPanel = new YAHOO.widget.Panel("ImportPanel", { width:"250px", visible:false, constraintoviewport:true });

						myImportPanel.render(document.body);

					function jShowImportPanel(obj)
					{
						var pos_left = getPostion(obj,"offsetLeft")+110;
						var pos_top  = getPostion(obj,"offsetTop")-20;

						myImportPanel.cfg.setProperty("x", pos_left);
						myImportPanel.cfg.setProperty("y", pos_top);


						$("#Import_Warning").html("");
						$("#FileDiv").html("<input type=\"file\" id=\"ImportFile\" name=\"ImportFile\" value=\"\" />");

						myImportPanel.show();
					}

					function jImportContactForm(obj)
					{
						var inputObj = document.getElementById("ImportFile");

						if(inputObj.value == null || inputObj.value == "")
						{
							$("#Import_Warning").html("<b>Please select a file</b>");
							return false;
						}

						$("#Import_Warning").html("<b>Uploading...</b>");
						return true;
					}

					// ajax callback
					function stopUpload(result, addType, userIDArr)
					{
						if(result == 0)
						{
							$("#Import_Warning").html("<b>'.$Lang["email"]["fail_to_import_contact"].'</b>");
						}
						else if(result == -1)
						{
							$("#Import_Warning").html("<b>'.$Lang["email"]["wrong_file_format"].'</b>");
						}
						else if(result == -2)
						{
							$("#Import_Warning").html("<b>'.$Lang["email"]["empty_record"].'</b>");
						}
						else if(result == -3)
						{
							$("#Import_Warning").html("<b>'.$Lang["email"]["fail_to_import_contact"].'</b>");
						}

						else
						{
							$("#Import_Warning").html("<b>'.$Lang["email"]["import_contact_finished"].'</b>");

							var isAdd = 0;
							var actionPage = "";';
		if ($CacheFormat) {
			$x .= '	if(addType == "add_group")
							{
								actionPage = "cache_address_add_group.php";
								isAdd = 1;
							}
							else if(addType == "edit_group")
							{
								actionPage = "cache_address_edit_group.php";
								isAdd = 1;
							}
							else if(addType == "add_school_group")
							{
								actionPage = "cache_address_add_group_school.php";
								isAdd = 1;
							}
							else if(addType == "edit_school_group")
							{
								actionPage = "cache_address_edit_group_school.php";
								isAdd = 1;
							}

							if(isAdd == 1)
							{
								var objForm =document.getElementById("NewUserForm");
								document.getElementById("TmpChooseUserIDs").value = userIDArr;
								document.getElementById("TempAction").value = "add";
								objForm.action = actionPage;
								objForm.submit();
							}
							else
							{
								window.location.href = "cache_imail_address_book.php";
								window.location.reload(true);
							}
						}';
		}
		else {
			$x .= '	if(addType == "add_group")
							{
								actionPage = "address_add_group.php";
								isAdd = 1;
							}
							else if(addType == "edit_group")
							{
								actionPage = "address_edit_group.php";
								isAdd = 1;
							}
							else if(addType == "add_school_group")
							{
								actionPage = "address_add_group_school.php";
								isAdd = 1;
							}
							else if(addType == "edit_school_group")
							{
								actionPage = "address_edit_group_school.php";
								isAdd = 1;
							}

							if(isAdd == 1)
							{
								var objForm =document.getElementById("NewUserForm");
								document.getElementById("TmpChooseUserIDs").value = userIDArr;
								document.getElementById("TempAction").value = "add";
								objForm.action = actionPage;
								objForm.submit();
							}
							else
							{
								window.location.href = "imail_address_book.php";
								window.location.reload(true);
							}
						}';
		}

		$x .= '	$("#FileDiv").html("<input type=\"file\" id=\"ImportFile\" name=\"ImportFile\" value=\"\" />");

					} // end stop upload
					</script>
					<!-- End of YUI Panel -->
				 ';

		// end of Ajax form
		$ReturnValue = $x;

		return $ReturnValue;
	} // end function get import layer

		
	function Get_Import_Outlook_Layer($AddType="user", $CacheFormat=false)
	{
		Global $Lang, $PathRelative, $CurPage, $SYS_CONFIG;

		if ($CacheFormat)
			$ActionPage = "cache_import_outlook_contact_process.php?AddType=".$AddType;
		else
			$ActionPage = "import_outlook_contact_process.php?AddType=".$AddType;

		// Ajax form
		$x = '
					<!-- YUI Panel -->
					<link rel="stylesheet" type="text/css" href="'.$PathRelative.'src/include/js/ajax/build/container/assets/skins/sam/container.css">
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/yahoo-dom-event/yahoo-dom-event.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/dragdrop/dragdrop-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/container/container-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/jquery/jquery.js"></script>
				 ';
		// target="upload_target"
		$x .= $this->Get_Div_Open("ImportOutlookPanel");
		$x .= $this->Get_Form_Open("ImportOutlookForm","POST",$ActionPage, 'target="upload_target" enctype="multipart/form-data" onSubmit="return jImportOutlookContactForm(this);"');
		$x .= $this->Get_Div_Open();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03"').$this->Get_Span_Open("",'CLASS="bubble_board_04"');
		$x .= $Lang["email"]["select_file"];
		$x .= $this->Get_Br();
		$x .= $this->Get_Br();
		$x .= $this->Get_Div_Open("Import_Outlook_Warning");
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Br();
		$x .= $this->Get_Div_Open("OutlookFileDiv");
		$x .= $this->Get_Input_File("ImportOutlookFile","ImportOutlookFile","");
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Br();
		$x .= $Lang["email"]["note"];
		$x .= $this->Get_Br();
		$x .= $Lang["email"]["import_outlook_notice1"];		
		$x .= $this->Get_Br();
		$x .= $this->Get_Br();
		$x .= $Lang["email"]["import_outlook_notice2"];		
		$x .= $this->Get_Br();
		$x .= $this->Get_Br();
		$x .= $Lang["email"]["import_outlook_notice3"];
		$x .= $this->Get_Br();

		$x .= $this->Get_Div_Open("btn_link");
		$x .= $this->Get_HyperLink_Open("view_outlook_attachment.php");
		$x .= $this->Get_Image($SYS_CONFIG["Image"]["Abs"]."xls.gif","","",'BORDER="0" ALIGN="absmiddle"');
		$x .= $Lang["email"]["download_sample"];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Br('style="clear:both"');
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_submit'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myImportOutlookPanel.hide();" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();

		$x .= '<iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>';

		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		$x .= '
					<script>
						var myImportOutlookPanel = new YAHOO.widget.Panel("ImportOutlookPanel", { width:"250px", visible:false, constraintoviewport:true });

						myImportOutlookPanel.render(document.body);

					function jShowOutlookImportPanel(obj)
					{
						var pos_left = getPostion(obj,"offsetLeft")+110;
						var pos_top  = getPostion(obj,"offsetTop")-20;

						myImportOutlookPanel.cfg.setProperty("x", pos_left);
						myImportOutlookPanel.cfg.setProperty("y", pos_top);


						$("#Import_Outlook_Warning").html("");
						$("#OutlookFileDiv").html("<input type=\"file\" id=\"ImportOutlookFile\" name=\"ImportOutlookFile\" value=\"\" />");

						myImportOutlookPanel.show();
					}

					function jImportOutlookContactForm(obj)
					{
						var inputObj = document.getElementById("ImportOutlookFile");

						if(inputObj.value == null || inputObj.value == "")
						{
							$("#Import_Outlook_Warning").html("<b>Please select a file</b>");
							return false;
						}

						$("#Import_Outlook_Warning").html("<b>Uploading...</b>");
						return true;
					}

					// ajax callback
					function stopOutlookUpload(result, addType, userIDArr)
					{
						if(result == 0)
						{
							$("#Import_Outlook_Warning").html("<b>'.$Lang["email"]["fail_to_import_contact"].'</b>");
						}
						else if(result == -1)
						{
							$("#Import_Outlook_Warning").html("<b>'.$Lang["email"]["wrong_file_format"].'</b>");
						}
						else if(result == -2)
						{
							$("#Import_Outlook_Warning").html("<b>'.$Lang["email"]["empty_record"].'</b>");
						}
						else if(result == -3)
						{
							$("#Import_Outlook_Warning").html("<b>'.$Lang["email"]["fail_to_import_contact"].'</b>");
						}

						else
						{
							$("#Import_Outlook_Warning").html("<b>'.$Lang["email"]["import_contact_finished"].'</b>");

							var isAdd = 0;
							var actionPage = "";';
		if ($CacheFormat) {
			
			$x .= '	if(addType == "add_group")
							{
								actionPage = "cache_address_add_group.php";
								isAdd = 1;
							}
							else if(addType == "edit_group")
							{
								actionPage = "cache_address_edit_group.php";
								isAdd = 1;
							}
							else if(addType == "add_school_group")
							{
								actionPage = "cache_address_add_group_school.php";
								isAdd = 1;
							}
							else if(addType == "edit_school_group")
							{
								actionPage = "cache_address_edit_group_school.php";
								isAdd = 1;
							}

							if(isAdd == 1)
							{
								var objForm =document.getElementById("NewUserForm");
								document.getElementById("TmpChooseUserIDs").value = userIDArr;
								document.getElementById("TempAction").value = "add";
								objForm.action = actionPage;
								objForm.submit();
							}
							else
							{
								window.location.href = "cache_imail_address_book.php";
								window.location.reload(true);
							}
						}';
		}
		else {
			$x .= '	if(addType == "add_group")
							{
								actionPage = "address_add_group.php";
								isAdd = 1;
							}
							else if(addType == "edit_group")
							{
								actionPage = "address_edit_group.php";
								isAdd = 1;
							}
							else if(addType == "add_school_group")
							{
								actionPage = "address_add_group_school.php";
								isAdd = 1;
							}
							else if(addType == "edit_school_group")
							{
								actionPage = "address_edit_group_school.php";
								isAdd = 1;
							}

							if(isAdd == 1)
							{
								var objForm =document.getElementById("NewUserForm");
								document.getElementById("TmpChooseUserIDs").value = userIDArr;
								document.getElementById("TempAction").value = "add";
								objForm.action = actionPage;
								objForm.submit();
							}
							else
							{
								window.location.href = "imail_address_book.php";
								window.location.reload(true);
							}
						}';
		}

		$x .= '	$("#OutlookFileDiv").html("<input type=\"file\" id=\"ImportOutlookFile\" name=\"ImportOutlookFile\" value=\"\" />");

					} // end stop upload
					</script>
					<!-- End of YUI Panel -->
				 ';

		// end of Ajax form
		$ReturnValue = $x;

		return $ReturnValue;
	} // end function get import layer	
		
	
	/*
	*  ParType: 1 - School, 2 - User
	*/
	function Get_Address_List($ParType="2", $ParKeyword="",$ParSchoolType="1",$ParPersonalType="2", $OutputList="", $CacheFormat=false)
	{
		global $Lang, $SYS_CONFIG;

		if ($CacheFormat) {
			$x = $this->Get_Div_Open("imail_iframe_right");
		}
		else {
			$x = $this->Get_Div_Open("imail_right");
		}


		$x .= $this->Get_Div_Open("listcommontabs");
		if ($CacheFormat)
			$SchoolLink .=  $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=1");
		else
			$SchoolLink .=  $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
		$SchoolLink .=  $this->Get_Span_Open();
		$SchoolLink .=  $Lang['email']['MySchool'];
		$SchoolLink .=  $this->Get_Span_Close();
		$SchoolLink .=  $this->Get_HyperLink_Close();

		if ($CacheFormat)
			$PersonalLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2");
		else
			$PersonalLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
		$PersonalLink .= $this->Get_Span_Open();
		$PersonalLink .= $Lang['email']['Personal'];
		$PersonalLink .= $this->Get_Span_Close();
		$PersonalLink .= $this->Get_HyperLink_Close();

		$x .= $this->Get_List_Open();


		if(Auth(array("Communication-Mail-MaintainSharedAddress")))
		{
		if ($ParType=="1")
			$x .= $this->Get_List_Member($SchoolLink, "id='current'");
		else
			$x .= $this->Get_List_Member($SchoolLink);
		}


		if (($ParType=="2") || ($ParType==""))
			$x .= $this->Get_List_Member($PersonalLink, "id='current'");
		else
			$x .= $this->Get_List_Member($PersonalLink);
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();
		$x .= "<br style=\"clear:both;\" />";

		$AddImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_add.gif',"","",'BORDER="0" ALIGN="absmiddle"');

		$ImportImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_import.gif',"","",'BORDER="0" ALIGN="absmiddle"');

		$ExportImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_export.gif',"","",'BORDER="0" ALIGN="absmiddle"');

		if ($ParType!="1")
		{
			//$AddContactLink .= $this->Get_HyperLink_Open("address_add_user.php");
			$AddContactLink .= $this->Get_HyperLink_Open("javascript:jAdd_Contact()");
			$AddContactLink .= $AddImage.$Lang['email']['NewContact'];
			$AddContactLink .= $this->Get_HyperLink_Close();

			$ImportContactLink .= $this->Get_Span_Open("ImportDiv");
			$ImportContactLink .= $this->Get_HyperLink_Open("javascript:jImport_Contact('ImportDiv')");
			$ImportContactLink .= $ImportImage.$Lang['email']['ImportContact'];
			$ImportContactLink .= $this->Get_HyperLink_Close();
			$ImportContactLink .= $this->Get_Span_Close();
			
			$ImportOutlookContactLink .= $this->Get_Span_Open("ImportOutlookDiv");
			$ImportOutlookContactLink .= $this->Get_HyperLink_Open("javascript:jImport_Outlook_Contact('ImportOutlookDiv')");
			$ImportOutlookContactLink .= $ImportImage.$Lang['email']['ImportOutlookContact'];
			$ImportOutlookContactLink .= $this->Get_HyperLink_Close();
			$ImportOutlookContactLink .= $this->Get_Span_Close();

			$ExportContactLink .= $this->Get_Span_Open("ImportDiv");
			$ExportContactLink .= $this->Get_HyperLink_Open("javascript:jExport_Contact()");
			$ExportContactLink .= $ExportImage.$Lang['email']['ExportContact'];
			$ExportContactLink .= $this->Get_HyperLink_Close();
			$ExportContactLink .= $this->Get_Span_Close();

		}

/*
		if ($ParType=="1")
			$AddGroupLink .= $this->Get_HyperLink_Open("address_add_group_school.php");
		else
			$AddGroupLink .= $this->Get_HyperLink_Open("address_add_group.php");
*/
		if ($ParType=="1")
			$AddGroupLink .= $this->Get_HyperLink_Open("javascript:jAdd_Group('school')");
		else
			$AddGroupLink .= $this->Get_HyperLink_Open("javascript:jAdd_Group('personal')");

		$AddGroupLink .= $AddImage.$Lang['email']['NewGroup'];
		$AddGroupLink .= $this->Get_HyperLink_Close();

		$x .= $this->Get_Div_Open("imail_toolbar");

		$x .= $this->Get_Div_Open("btn_link");
		$x .= $AddContactLink.$ImportContactLink.$ImportOutlookContactLink.$ExportContactLink.$AddGroupLink;
		//$x .= $AddContactLink;


		$x .= $this->Get_Div_Close();
		$x .= "<br style=\"clear:both;\" />";

		$x .= $this->Get_Div_Open("imail_toolbar_right");
/*
		$x .= "<div id=\"sub_layer_imail_import_file\">";
		$x .= "<span class=\"bubble_board_01\">";
		$x .= "<span class=\"bubble_board_02\"></span>";
		$x .= "</span>";
		$x .= "<span class=\"bubble_board_03\">";
		$x .= "<span class=\"bubble_board_04\">";
		$x .= "<span class=\"sub_layer_close\"><a href=\"#\">X</a></span>";
		$x .= "Select File";
		$x .= "<br /><br />";
		$x .= "<div style=\"width:90%\">";
		$x .= "<label>";
		$x .= "<input type=\"file\" name=\"fileField\" id=\"fileField\">";
		$x .= "</label>";
		$x .= "<div id=\"btn_link\"><a href=\"#\"><img src=\"".$SYS_CONFIG["Image"]["Abs"]."/xls.gif\" border=\"0\" align=\"absmiddle\"> Download Sample</a></div>";
		$x .= "<a href=\"#\" class=\"cal_calendar_07\"></a><br style=\"clear:both\">";
		$x .= "<div id=\"form_btn\">";
		$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_submit'],'class="button" onclick="" ');
		$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="" ');
		$x .= "</div>";
		$x .= "</div>";
		$x .= "</span>";
		$x .= "</span>";
		$x .= "<span class=\"bubble_board_05\"><span class=\"bubble_board_06\"></span>";
		$x .= "</span>";
		$x .= "</div>";
*/
		//$x .= $this->Get_Input_Button("btn_compose","button",$Lang['email']['compose'],'class="button"  ');
		if ($CacheFormat)
			$x .= $this->Get_Input_Button("btn_delete","button",$Lang['email']['Delete'],'class="button" onclick="Confirm_Submit_Page(\''.$Lang['Warning']['RecordDeleteWarn'].'\',\'AddressForm\',\'cache_address_remove_process.php\');" ')." | ";
		else
			$x .= $this->Get_Input_Button("btn_delete","button",$Lang['email']['Delete'],'class="button" onclick="Confirm_Submit_Page(\''.$Lang['Warning']['RecordDeleteWarn'].'\',\'AddressForm\',\'address_remove_process.php\');" ')." | ";
		$x .= $this->Get_Input_Text("Keyword",$ParKeyword,'class="textbox textbox_search"');
		$x .= $this->Get_Input_Button("btn_go","button",$Lang['email']['Go'],'class="button button_go" onclick="Check_Submit(this.form,0)" ');
		$x .= $this->Get_Div_Close();

		if ($ParPersonalType == "1")
		{
			$AllLink .= $this->Get_Span_Open();
			$AllLink .= $Lang['email']['All'];
			$AllLink .= $this->Get_Span_Close();
		}
		else
		{
			if ($CacheFormat)
				$AllLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2&PersonalType=1");
			else
				$AllLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2&PersonalType=1");
			$AllLink .= $Lang['email']['All'];
			$AllLink .= $this->Get_HyperLink_Close();
		}

		if (($ParPersonalType == "2") || ($ParPersonalType == ""))
		{
			$IndividualLink .= $this->Get_Span_Open();
			$IndividualLink .= $Lang['email']['Individual'];
			$IndividualLink .= $this->Get_Span_Close();
		}
		else
		{
			if ($CacheFormat)
				$IndividualLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2&PersonalType=2");
			else
				$IndividualLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2&PersonalType=2");
			$IndividualLink .= $Lang['email']['Individual'];
			$IndividualLink .= $this->Get_HyperLink_Close();
		}

		if ($ParPersonalType == "3")
		{
			$GroupLink .= $this->Get_Span_Open();
			$GroupLink .= $Lang['email']['Groups'];
			$GroupLink .= $this->Get_Span_Close();
		}
		else
		{
			if ($CacheFormat)
				$GroupLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2&PersonalType=3");
			else
				$GroupLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2&PersonalType=3");
			$GroupLink .= $Lang['email']['Groups'];
			$GroupLink .= $this->Get_HyperLink_Close();
		}

		$x .= $this->Get_Div_Open(""," class=\"list_option\" ");
		if ($ParType!="1")
		{
			$x .= $AllLink." | ".$IndividualLink." | ".$GroupLink;
		}
		//$x .= $IndividualLink;
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Open("imail_mail_list");
		$x .= $OutputList;
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		return $x;
	}


	function Get_User_Form($ParGroupArr=array(),$ParChooseUserID="",$ParUserArr=array(),$ListTable ="",$IncludeTabs=true,$CacheFormat=false)
	{
		global $SYS_CONFIG, $Lang;
		global $ChooseType, $PersonalType;

		if ($CacheFormat)
			$x = $this->Get_Div_Open("imail_iframe_right");
		else
			$x = $this->Get_Div_Open("imail_right");

		if ($IncludeTabs) {
			$x .= $this->Get_Div_Open("listcommontabs");

			if ($CacheFormat)
				$SchoolLink .=  $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=1");
			else
				$SchoolLink .=  $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
			$SchoolLink .=  $this->Get_Span_Open();
			$SchoolLink .=  $Lang['email']['MySchool'];
			$SchoolLink .=  $this->Get_Span_Close();
			$SchoolLink .=  $this->Get_HyperLink_Close();

			if ($CacheFormat)
				$PersonalLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2");
			else
				$PersonalLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
			$PersonalLink .= $this->Get_Span_Open();
			$PersonalLink .= $Lang['email']['Personal'];
			$PersonalLink .= $this->Get_Span_Close();
			$PersonalLink .= $this->Get_HyperLink_Close();

			$x .= $this->Get_List_Open();


			if(Auth(array("Communication-Mail-MaintainSharedAddress")))
				$x .= $this->Get_List_Member($SchoolLink);

			$x .= $this->Get_List_Member($PersonalLink, "id='current'");

			$x .= $this->Get_List_Close();
			$x .= $this->Get_Div_Close();
		}

		$x .= $this->Get_Div_Open("sub_page_title");
		$AddImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_add.gif',"","",'BORDER="0" ALIGN="absmiddle"');
		$x .= $AddImage;
		if ($ParChooseUserID != "")
			$x .= $Lang['email']['EditContact'];
		else
			$x .= $Lang['email']['NewContact'];
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Open("commontabs_board","class=\"imail_mail_content\"");
		if ($ParChooseUserID != "") {
			if ($CacheFormat)
				$x .= $this->Get_Form_Open("NewUserForm","POST","cache_address_edit_user_process.php","class=\"cssform\"");
			else
				$x .= $this->Get_Form_Open("NewUserForm","POST","address_edit_user_process.php","class=\"cssform\"");
		}
		else	{
			if ($CacheFormat)
				$x .= $this->Get_Form_Open("NewUserForm","POST","cache_address_add_user_process.php","class=\"cssform\"");
			else
				$x .= $this->Get_Form_Open("NewUserForm","POST","address_add_user_process.php","class=\"cssform\"");
		}

		$x .= $this->Get_Div_Open("MsgDiv","class=\"warning\"");
		$x .= $this->Get_Div_Close();


		///////////////////////////////////////////////////////////////////////////////////////////////////
		// First name + Surname
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class='cssform'");
		$x .= "<label for='Firstname'>".$Lang['email']['FirstName']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice", " style=\"padding-top:0px\" ");

		$x .= $this->Get_Table_Open("","100%","",0,5);
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("Firstname",$ParUserArr["Firstname"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"5%\" nowrap=\"nowrap\" style=\"vertical-align: top\" ") ;
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= " *";
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"15%\" nowrap=\"nowrap\" align=\"right\" style=\"vertical-align: top\" ") ;
		$x .= "<span style='font-size: 11px;'>".$Lang['email']['Surname']."</span>";
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("Surname",$ParUserArr["Surname"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();

		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= " *";
		$x .= $this->Get_Span_Close();

		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Title
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='Title'>".$Lang['email']['Title']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("Title",$ParUserArr["Title"],'class="textbox"');
		$x .= $this->Get_Span_Close();		
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Email Address
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='UserEmail'>".$Lang['email']['EmailAddress']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("UserEmail",$ParUserArr["UserEmail"],'class="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= " *";
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Email Address 2
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='UserEmail2'>".$Lang['email']['EmailAddress2']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("UserEmail2",$ParUserArr["UserEmail2"],'class="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= " *";
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Job Title
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='UserEmail'>".$Lang['email']['JobTitle']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("JobTitle",$ParUserArr["JobTitle"],'class="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Department
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='Department'>".$Lang['email']['Department']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("Department",$ParUserArr["Department"],'class="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Company
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='UserEmail'>".$Lang['email']['Company']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("Company",$ParUserArr["Company"],'class="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Office Address
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='UserEmail'>".$Lang['email']['OfficeAddress']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		//$x .= $this->Get_Input_Text("OfficeAddress",$ParUserArr["OfficeAddress"],'class="textbox"');
		$x .= $this->Get_Textarea_Open("OfficeAddress",' style="width:100%;" cols="70" rows="4" ');
		$x .= $ParUserArr["OfficeAddress"];
		$x .= $this->Get_Textarea_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Address 2
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='Address2'>".$Lang['email']['Address2']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		//$x .= $this->Get_Input_Text("Address2",$ParUserArr["Address2"],'class="textbox"');
		$x .= $this->Get_Textarea_Open("Address2",' style="width:100%;" cols="70" rows="4" ');
		$x .= $ParUserArr["Address2"];
		$x .= $this->Get_Textarea_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Office phone + Mobile Phone
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class='cssform'");
		$x .= "<label for='OfficePhone'>".$Lang['email']['OfficePhone']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice", " style=\"padding-top:0px\" ");

		$x .= $this->Get_Table_Open("","100%","",0,5);
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("OfficePhone",$ParUserArr["OfficePhone"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"5%\" nowrap=\"nowrap\" style=\"vertical-align: top\" ") ;
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"15%\" nowrap=\"nowrap\" align=\"right\" style=\"vertical-align: top\" ") ;
		$x .= "<span style='font-size: 11px;'>".$Lang['email']['MobilePhone']."</span>";
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("Mobile",$ParUserArr["Mobile"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();

		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();

		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////

		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Home phone + Other Phone
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class='cssform'");
		$x .= "<label for='HomePhone'>".$Lang['email']['HomePhone']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice", " style=\"padding-top:0px\" ");

		$x .= $this->Get_Table_Open("","100%","",0,5);
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("HomePhone",$ParUserArr["HomePhone"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"5%\" nowrap=\"nowrap\" style=\"vertical-align: top\" ") ;
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"15%\" nowrap=\"nowrap\" align=\"right\" style=\"vertical-align: top\" ") ;
		$x .= "<span style='font-size: 11px;'>".$Lang['email']['OtherPhone']."</span>";
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("OtherPhone",$ParUserArr["OtherPhone"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();

		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();

		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////
				
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Office Fax
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='OfficeFax'>".$Lang['email']['OfficeFax']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");

		$x .= $this->Get_Table_Open("","100%","",0,5);
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("OfficeFax",$ParUserArr["OfficeFax"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open(""," width=\"60%\" nowrap=\"nowrap\" ") ;
		$x .= "&nbsp;";
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();

		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Webpage
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='Webpage'>".$Lang['email']['Webpage']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");

		$x .= $this->Get_Table_Open("","100%","",0,5);
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"40%\" nowrap=\"nowrap\" ") ;
		$x .= $this->Get_Input_Text("Webpage",$ParUserArr["Webpage"],'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open(""," width=\"60%\" nowrap=\"nowrap\" ") ;
		$x .= "&nbsp;";
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();

		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Notes
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='UserEmail'>".$Lang['email']['Notes']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Textarea_Open("Notes",' style="width:100%;" cols="70" rows="10" ');
		$x .= $ParUserArr["Notes"];
		$x .= $this->Get_Textarea_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("","class='indicator'");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";
		///////////////////////////////////////////////////////////////////////////////////////////////////



		if ($ParChooseUserID != "")
		{
			$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
			$x .= "<label for='None'>".ucfirst($Lang['email']['MemberofGroups'])."</label>";
			$x .= $this->Get_Span_Open("imail_form_indside_choice");

			$x .= $this->Get_Table_Open("","100%");
			$x .= $this->Get_Table_Row_Open("");
			$x .= $this->Get_Table_Cell_Open(""," width=\"100%\" nowrap=\"nowrap\" ") ;
			$x .= $ListTable;
			$x .= $this->Get_Table_Cell_Close();
			$x .= $this->Get_Table_Row_Close();
			$x .= $this->Get_Table_Close();
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Paragraph_Close();
			$x .= "<br style=\"clear:both;\" />";
		}


		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='None'>".$Lang['email']['AddToGroup']."</label>";
		//$ClickAction1 = " onclick=\"if (this.checked) this.form.NewGroup.checked=false;\" ";
/*
		if (count($ParUserArr["Groups"]) > 0)
			$x .= $this->Get_Input_CheckBox("ExistGroup","ExistGroup","1",true);
		else
			$x .= $this->Get_Input_CheckBox("ExistGroup","ExistGroup","1",false);
*/
		$x .= $this->Get_Input_CheckBox("ExistGroup","ExistGroup","1",false);
		$x .= $Lang['email']['Existing'];
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$ClickAction2 = " onclick=\"this.form.ExistGroup.checked=true;\" ";
		$x .= $this->Get_Input_Select_Multi("ChooseGroupID[]","ChooseGroupID[]",$ParGroupArr,$ParUserArr["Groups"],"size=\"10\" ".$ClickAction2." ");
		//$x .= $this->Get_Input_Select_Multi("ChooseGroupID[]","ChooseGroupID[]",$ParGroupArr,"","size=\"10\" ".$ClickAction2." ");
		$x .= $this->Get_Span_Close();
		$x .= "<br style=\"clear:both;\" />";

		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		//$ClickAction2 = " onclick=\"if (this.checked) this.form.ExistGroup.checked=false;\" ";
		$x .= $this->Get_Input_CheckBox("NewGroup","NewGroup","1",false);
		$x .= $Lang['email']['AddToGroupIntro1'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$ClickAction2 = " onclick=\"this.form.NewGroup.checked=true;\" onfocus=\"this.form.NewGroup.checked=true;\" ";
		$x .= $this->Get_Input_Text("GroupName","",'class="textbox" '.$ClickAction2.' ');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";

		$x .= $this->Get_Paragraph_Open("class=\"dotline_p\"");
		$x .= $Lang['email']['RequiredField'];
		$x .= $this->Get_Paragraph_Close();

		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_save'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form)" ');
		$x .= "&nbsp;";
		if ($CacheFormat)
			$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="Parent_To_Top(); self.location=\'cache_imail_address_book.php?ChooseType='.$ChooseType.'&PersonalType='.$PersonalType.'\'" ');
		else
			$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="self.location=\'imail_address_book.php?ChooseType='.$ChooseType.'&PersonalType='.$PersonalType.'\'" ');
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Input_Hidden("ChooseUserID","ChooseUserID",$ParChooseUserID);
		if (is_array($ParUserArr["Groups"]) && count($ParUserArr["Groups"]) >0)
		{
			$TempGroupIDs = implode(",",$ParUserArr["Groups"]);
		}
		$x .= $this->Get_Input_Hidden("TempGroupIDs","TempGroupIDs",$TempGroupIDs);
		$x .= $this->Get_Input_Hidden("TempDeleteGroupIDs","TempDeleteGroupIDs");
		$x .= $this->Get_Input_Hidden("TempAction","TempAction");
		$x .= $this->Get_Input_Hidden("IsEditing","IsEditing",1);
		$x .= $this->Get_Input_Hidden("ChooseType","ChooseType",$ChooseType);
		$x .= $this->Get_Input_Hidden("PersonalType","PersonalType",$PersonalType);
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		return $x;
	}


	function Get_Group_Form($ParUserArr=array(),$ParChooseGroupID="",$ParGroupArr=array(),$ListTable ="",$CacheFormat=false)
	{
		global $SYS_CONFIG, $Lang, $order, $field;
		global $ChooseType, $PersonalType;

		if ($CacheFormat)
			$x = $this->Get_Div_Open("imail_iframe_right");
		else
			$x = $this->Get_Div_Open("imail_right");

		$x .= $this->Get_Div_Open("listcommontabs");
		if ($CacheFormat)
			$SchoolLink .=  $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=1");
		else
			$SchoolLink .=  $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
		$SchoolLink .=  $this->Get_Span_Open();
		$SchoolLink .=  $Lang['email']['MySchool'];
		$SchoolLink .=  $this->Get_Span_Close();
		$SchoolLink .=  $this->Get_HyperLink_Close();

		if ($CacheFormat)
			$PersonalLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2");
		else
			$PersonalLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
		$PersonalLink .= $this->Get_Span_Open();
		$PersonalLink .= $Lang['email']['Personal'];
		$PersonalLink .= $this->Get_Span_Close();
		$PersonalLink .= $this->Get_HyperLink_Close();

		$AddImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_add.gif',"","",'BORDER="0" ALIGN="absmiddle"');
		$ImportImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_import.gif',"","",'BORDER="0" ALIGN="absmiddle"');

		$ImportContactLink .= $this->Get_Span_Open("ImportDiv");
		$ImportContactLink .= $this->Get_HyperLink_Open("javascript:jImport_Contact('ImportDiv')");
		$ImportContactLink .= $ImportImage.$Lang['email']['ImportContacts'];
		$ImportContactLink .= $this->Get_HyperLink_Close();
		$ImportContactLink .= $this->Get_Span_Close();
		
		$ImportOutlookContactLink .= $this->Get_Span_Open("ImportOutlookDiv");
		$ImportOutlookContactLink .= $this->Get_HyperLink_Open("javascript:jImport_Outlook_Contact('ImportOutlookDiv')");
		$ImportOutlookContactLink .= $ImportImage.$Lang['email']['ImportOutlookContacts'];
		$ImportOutlookContactLink .= $this->Get_HyperLink_Close();
		$ImportOutlookContactLink .= $this->Get_Span_Close();

		$x .= $this->Get_List_Open();

		if(Auth(array("Communication-Mail-MaintainSharedAddress")))
		$x .= $this->Get_List_Member($SchoolLink);

		$x .= $this->Get_List_Member($PersonalLink, "id='current'");
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Open("sub_page_title");
		$x .= $AddImage;
		if ($ParChooseGroupID != "")
			$x .= $Lang['email']['EditGroup'];
		else
			$x .= $Lang['email']['NewGroup'];
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Open("commontabs_board","class=\"imail_mail_content\"");
		if ($ParChooseGroupID != "")
			if ($CacheFormat)
				$x .= $this->Get_Form_Open("NewUserForm","POST","cache_address_edit_group.php","class=\"cssform\"");
			else
				$x .= $this->Get_Form_Open("NewUserForm","POST","address_edit_group.php","class=\"cssform\"");
		else
			if ($CacheFormat)
				$x .= $this->Get_Form_Open("NewUserForm","POST","cache_address_add_group.php","class=\"cssform\"");
			else
				$x .= $this->Get_Form_Open("NewUserForm","POST","address_add_group.php","class=\"cssform\"");

		$x .= $this->Get_Div_Open("MsgDiv","class=\"warning\"");
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Paragraph_Open("class='cssform'");
		$x .= "<label for='UserName'>".$Lang['email']['GroupsName']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("GroupName",$ParGroupArr["GroupName"],'class="textbox" ');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= $this->Get_Br("style=\"clear:both;\"");

		$x .= $this->Get_Paragraph_Open("class=\"dotline_p\"");

		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='None'>".ucfirst($Lang['email']['Contacts'])."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");

		$x .= $this->Get_Table_Open("","100%");
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"100%\" nowrap=\"nowrap\" ") ;
		$x .= $ListTable;
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		$x .= $this->Get_Br();

		$x .= $AddImage.$Lang['email']['AddContacts'];
		$x .= "<br>";
		$x .= $ImportContactLink;
		$x .= "&nbsp;&nbsp;&nbsp;";
		$x .= $ImportOutlookContactLink;
		$x .= "&nbsp;&nbsp;&nbsp;";
		$x .= $this->Get_Span_Open("ImportDiv");
		$x .= $this->Get_HyperLink_Open("#", 'onclick="window.open(\'../select_recipient.php?fromSrc=ToUserAddress&srcForm=NewUserForm\',\'\',\'scrollbars=yes,width=350,height=350\');return false"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0" style="vertical-align: top" ');
		$x .= $Lang['email']['AddSchoolContacts'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();		
		
		$x .= $this->Get_Table_Open("","100%");
		$x .= $this->Get_Table_Row_Open("","bgcolor='#F7F7F7'");
		$x .= $this->Get_Table_Cell_Open(""," width=\"90%\" nowrap=\"nowrap\" class=\"imail_entry_top\" ") ;
		$x .= $this->Get_Input_Select_Multi("ChooseUserID[]","ChooseUserID[]",$ParUserArr,$ParGroupArr["Users"],"size=\"10\" style=\"width: 99%\" ");
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"10%\" nowrap=\"nowrap\" class=\"imail_entry_top\" valign=\"bottom\" style=\"padding: 0px\" ") ;
		if ($ParChooseGroupID != "")
		{
			$x .= $this->Get_Input_Button("btn_new","button",$Lang['btn_new'],'class="button" onclick="Show_Contact_Setup(this)" ');
			$x .= $this->Get_Br();
			$x .= $this->Get_Br();
			$x .= $this->Get_Input_Button("btn_add","button",$Lang['btn_add'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,3)" ');
		}
		else
		{
			$x .= $this->Get_Input_Button("btn_new","button",$Lang['btn_new'],'class="button" onclick="Show_Contact_Setup(this)" ');
			$x .= $this->Get_Br();
			$x .= $this->Get_Br();
			$x .= $this->Get_Input_Button("btn_add","button",$Lang['btn_add'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,3)" ');
		}

		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Cell_Open(""," width=\"100%\" nowrap=\"nowrap\" class=\"imail_entry_top\" colspan=\"2\" ") ;
		$x .= "(".$Lang['email']['SelectMultipleWarning'].")";
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Open("","bgcolor='#F7F7F7'");

		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		
		$x .= $this->Get_Br();
		
		/*    S013           *******************************************************************/
		/*	
		$x .= $this->Get_Table_Open("","100%");
		$x .= $this->Get_Table_Row_Open(""," bgcolor='#F7F7F7' ");
		$x .= $this->Get_Table_Cell_Open(""," width=\"90%\" nowrap=\"nowrap\" class=\"imail_entry_top\" style=\"vertical-align: top\" ") ;

		$x .= $this->Get_Textarea_Open("ToAddress",'class="textarea_autoexpand" style="width:96%;" onblur="HideDiv(\'to_list\')" onkeydown="return NavigateAddress(\'ToEmailList\', event)" onkeyup="return AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php?UserType=S\',\'\', \'ToEmailList\');"');
		$x .= $this->Get_Textarea_Close();

		$x .= $this->Get_HyperLink_Open("#", 'onclick="window.open(\'../select_recipient.php?fromSrc=ToUserAddress&srcForm=NewUserForm\',\'\',\'scrollbars=yes,width=350,height=350\');return false"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0" style="vertical-align: top" ');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Br('style="clear: both"');

		$x .= $this->Get_Div_Open("to_list", 'class="added_item_ppl_list_board" onmouseover="EmailListFocus=true" onmouseout="EmailListFocus=false" style="display:none;"');
		$x .= $this->Get_Div_Open("added_item_ppl_list", '');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Cell_Open(""," width=\"10%\"  bgcolor='#F7F7F7'  style=\"vertical-align: top\" class=\"imail_entry_top\"  ") ;
		//$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_add'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,4)" ');
		$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_add'],'class="button"');
		$x .= $this->Get_Table_Cell_Close();

		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();		
		*/
		/*  S013 end  *********************************************************************/

			
			
			
			
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";

		$x .= $this->Get_Paragraph_Open("class=\"dotline_p\"");
		$x .= $this->Get_Paragraph_Close();

		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_save'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,1)" ');
		$x .= "&nbsp;";
		if ($CacheFormat)
			$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="Parent_To_Top(); self.location=\'cache_imail_address_book.php?ChooseType='.$ChooseType.'&PersonalType='.$PersonalType.'\'" ');
		else
			$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="self.location=\'imail_address_book.php?ChooseType='.$ChooseType.'&PersonalType='.$PersonalType.'\'" ');
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Input_Hidden("ChooseGroupID","ChooseGroupID",$ParChooseGroupID);
		$x .= $this->Get_Input_Hidden("order","order",$order);
		$x .= $this->Get_Input_Hidden("field","field",$field);
		if (is_array($ParGroupArr["Users"]) && count($ParGroupArr["Users"]) >0)
		{
			$TempUserIDs = implode(",",$ParGroupArr["Users"]);
		}
		// if(is_array($ParGroupArr["Users2"]) && count($ParGroupArr["Users2"]) > 0){
			// $TempUsers = implode(";", $ParGroupArr["Users2"]);	
		// }
		
		$x .= $this->Get_Input_Hidden("TempUserIDs","TempUserIDs",$TempUserIDs);
		// $x .= $this->Get_Input_Hidden("TempUsers", "TempUsers", $TempUsers);
		$x .= $this->Get_Input_Hidden("TempDeleteUserIDs","TempDeleteUserIDs");
		$x .= $this->Get_Input_Hidden("TempAction","TempAction");
		$x .= $this->Get_Input_Hidden("IsEditing","IsEditing",1);
		$x .= $this->Get_Input_Hidden("ChooseType","ChooseType",$ChooseType);
		$x .= $this->Get_Input_Hidden("PersonalType","PersonalType",$PersonalType);
		$x .= $this->Get_Input_Hidden("TmpChooseUserIDs","TmpChooseUserIDs");
		$x .= $this->Get_Input_Textarea("ToUserAddress", $ToUserAddress, 70, 5, "", "style='display: none' ");
		
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		return $x;
	}


	function Get_School_Group_Form($ParUserArr=array(),$ParChooseGroupID="",$ParGroupArr=array(),$ListTable ="",$CacheFormat=false)
	{
		global $SYS_CONFIG, $Lang, $order, $field, $PathRelative;
		global $ChooseType, $PersonalType;
		
		if ($CacheFormat)
			$x = $this->Get_Div_Open("imail_iframe_right");
		else
			$x = $this->Get_Div_Open("imail_right");

		$x .= $this->Get_Div_Open("listcommontabs");
		if ($CacheFormat)
			$SchoolLink .=  $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=1");
		else
			$SchoolLink .=  $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
		$SchoolLink .=  $this->Get_Span_Open();
		$SchoolLink .=  $Lang['email']['MySchool'];
		$SchoolLink .=  $this->Get_Span_Close();
		$SchoolLink .=  $this->Get_HyperLink_Close();

		if ($CacheFormat)
			$PersonalLink .= $this->Get_HyperLink_Open("cache_imail_address_book.php?ChooseType=2");
		else
			$PersonalLink .= $this->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
		$PersonalLink .= $this->Get_Span_Open();
		$PersonalLink .= $Lang['email']['Personal'];
		$PersonalLink .= $this->Get_Span_Close();
		$PersonalLink .= $this->Get_HyperLink_Close();

		$ImportImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_import.gif',"","",'BORDER="0" ALIGN="absmiddle"');
		$ImportContactLink .= $this->Get_Span_Open("ImportDiv");
		$ImportContactLink .= $this->Get_HyperLink_Open("javascript:jImport_Contact('ImportDiv')");
		$ImportContactLink .= $ImportImage.$Lang['email']['ImportContacts'];
		$ImportContactLink .= $this->Get_HyperLink_Close();
		$ImportContactLink .= $this->Get_Span_Close();
		
		$ImportOutlookContactLink .= $this->Get_Span_Open("ImportOutlookDiv");
		$ImportOutlookContactLink .= $this->Get_HyperLink_Open("javascript:jImport_Outlook_Contact('ImportOutlookDiv')");
		$ImportOutlookContactLink .= $ImportImage.$Lang['email']['ImportOutlookContacts'];
		$ImportOutlookContactLink .= $this->Get_HyperLink_Close();
		$ImportOutlookContactLink .= $this->Get_Span_Close();
		
		$x .= $this->Get_List_Open();

		if(Auth(array("Communication-Mail-MaintainSharedAddress")))
		$x .= $this->Get_List_Member($SchoolLink, "id='current'");

		$x .= $this->Get_List_Member($PersonalLink);
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Open("sub_page_title");
		$AddImage = $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'icon_add.gif',"","",'BORDER="0" ALIGN="absmiddle"');
		$x .= $AddImage;
		if ($ParChooseGroupID != "")
			$x .= $Lang['email']['EditGroup'];
		else
			$x .= $Lang['email']['NewGroup'];
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Open("commontabs_board","class=\"imail_mail_content\"");
		if ($ParChooseGroupID != "")
			if ($CacheFormat)
				$x .= $this->Get_Form_Open("NewUserForm","POST","cache_address_edit_group_school.php","class=\"cssform\"");
			else
				$x .= $this->Get_Form_Open("NewUserForm","POST","address_edit_group_school.php","class=\"cssform\"");
		else
			if ($CacheFormat)
				$x .= $this->Get_Form_Open("NewUserForm","POST","cache_address_add_group_school.php","class=\"cssform\"");
			else
				$x .= $this->Get_Form_Open("NewUserForm","POST","address_add_group_school.php","class=\"cssform\"");

		$x .= $this->Get_Div_Open("MsgDiv","class=\"warning\"");
		$x .= $ParGroupArr["Msg"];
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Paragraph_Open("class='cssform'");
		$x .= "<label for='UserName'>".$Lang['email']['GroupsName']."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");
		$x .= $this->Get_Input_Text("GroupName",$ParGroupArr["GroupName"],'class="textbox"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();
		
		$x .= "<br style=\"clear:both;\" />";
		
		if($SYS_CONFIG['Mail']['ServerType'] == "imap") {
			$x .= $this->Get_Paragraph_Open("class='cssform'");
			$x .= "<label for='UserEmail'>".$Lang['email']['GroupsEmail']."</label>";
			$x .= $this->Get_Span_Open("imail_form_indside_choice");
			$x .= $this->Get_Input_Text("GroupEmail",$ParGroupArr["GroupEmail"],'class="textbox_date" maxlength="30"');
			$x .= ' @'.$SYS_CONFIG['Mail']['UserNameSubfix'];
			$x .= $this->Get_Span_Close();
			$x .= "<br style=\"clear:both;\" />";
			$x .= $this->Get_Span_Open("imail_form_indside_choice", "style=\"color:#A6A6A6\"");
			$x .= $Lang['Warning']['InvalidEmail_SpecialCharacters'];
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Paragraph_Close();
			
			$x .= "<br style=\"clear:both;\" />";
		}
		
		$x .= $this->Get_Paragraph_Open("class=\"dotline_p\"");

		$x .= $this->Get_Paragraph_Open("class=\"cssform\"");
		$x .= "<label for='None'>".ucfirst($Lang['email']['Contacts'])."</label>";
		$x .= $this->Get_Span_Open("imail_form_indside_choice");

		$x .= $this->Get_Table_Open("","100%");
		$x .= $this->Get_Table_Row_Open("");
		$x .= $this->Get_Table_Cell_Open(""," width=\"100%\" nowrap=\"nowrap\" ") ;
		$x .= $ListTable;
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		$x .= $this->Get_Br();

		$x .= $AddImage.$Lang['email']['AddContacts'];
		$x .= "<br>";
		$x .= $ImportContactLink;
		$x .= "&nbsp;&nbsp;&nbsp;";
		$x .= $ImportOutlookContactLink;
		$x .= "&nbsp;&nbsp;&nbsp;";
		$x .= $this->Get_HyperLink_Open("#", 'onclick="window.open(\'../select_recipient.php?fromSrc=ToSchoolAddress&srcForm=NewUserForm\',\'\',\'scrollbars=yes,width=350,height=350\');return false"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0" style="vertical-align: top" ');
		$x .= $Lang['email']['AddSchoolContacts'];
		$x .= $this->Get_HyperLink_Close();		

		$x .= $this->Get_Table_Open("","100%");
		$x .= $this->Get_Table_Row_Open(""," bgcolor='#F7F7F7' ");
		
		/*
		$x .= $this->Get_Table_Cell_Open(""," nowrap=\"nowrap\" width=\"100%\" class=\"imail_entry_top\" style=\"vertical-align: top\" ") ;
		$x .= $this->Get_Textarea_Open("ToAddress",'rows="7" class="textarea_autoexpand" style="width:100%;" onblur="HideDiv(\'to_list\')" onkeydown="return NavigateAddress(\'ToEmailList\', event)" onkeyup="return AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php?UserType=S\',\'\', \'ToEmailList\');"');
		$x .= $this->Get_Textarea_Close();
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Div_Open("to_list", 'class="added_item_ppl_list_board" onmouseover="EmailListFocus=true" onmouseout="EmailListFocus=false" style="display:none;"');
		$x .= $this->Get_Div_Open("added_item_ppl_list", '');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Table_Cell_Close();
		*/
		
		/*
		$x .= $this->Get_Table_Cell_Open(""," width=\"10%\"  bgcolor='#F7F7F7'  style=\"vertical-align: bottom\" class=\"imail_entry_top\"  ") ;
		$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_add'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,3)" ');
		$x .= $this->Get_Table_Cell_Close();
		*/	
			
		$x .= $this->Get_Table_Cell_Open(""," width=\"90%\" nowrap=\"nowrap\" class=\"imail_entry_top\" ") ;
		$x .= $this->Get_Input_Select_Multi("ChooseUserID[]","ChooseUserID[]",$ParUserArr,$ParGroupArr["Users"],"size=\"10\" style=\"width: 99%\" ");
		$x .= $this->Get_Table_Cell_Close();
		
		$x .= $this->Get_Table_Cell_Open(""," width=\"10%\" nowrap=\"nowrap\" class=\"imail_entry_top\" valign=\"bottom\" style=\"padding: 0px\" ") ;
		$x .= $this->Get_Input_Button("btn_new","button",$Lang['btn_new'],'class="button" onclick="Show_Contact_Setup(this)" ');
		$x .= $this->Get_Br();
		$x .= $this->Get_Br();
		$x .= $this->Get_Input_Button("btn_add","button",$Lang['btn_add'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,3)" ');
		$x .= $this->Get_Table_Cell_Close();		
			
		$x .= $this->Get_Table_Row_Close();
		
		$x .= $this->Get_Table_Row_Open("","bgcolor='#F7F7F7'");
		$x .= $this->Get_Table_Cell_Open(""," width=\"100%\" nowrap=\"nowrap\" class=\"imail_entry_top\" colspan=\"2\" ") ;
		$x .= "(".$Lang['email']['SelectMultipleWarning'].")";
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();		
		
		$x .= $this->Get_Table_Close();

		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Paragraph_Close();

		$x .= $this->Get_Paragraph_Close();
		$x .= "<br style=\"clear:both;\" />";

		$x .= $this->Get_Paragraph_Open("class=\"dotline_p\"");
		$x .= $this->Get_Paragraph_Close();
		
		if($SYS_CONFIG['Mail']['ServerType'] == "imap") {
			$x .= $Lang['email']['ChangeInGroupMsg'];
		}
		
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Button("btn_submit","button",$Lang['btn_save'],'class="button" onclick="Parent_To_Top(); Check_Submit(this.form,1)" ');
		$x .= "&nbsp;";
		if ($CacheFormat)
			$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="Parent_To_Top(); self.location=\'cache_imail_address_book.php?ChooseType='.$ChooseType.'&PersonalType='.$PersonalType.'\'" ');
		else
			$x .= $this->Get_Input_Button("btn_cancel","button",$Lang['btn_cancel'],'class="button" onclick="self.location=\'imail_address_book.php?ChooseType='.$ChooseType.'&PersonalType='.$PersonalType.'\'" ');
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Input_Hidden("ChooseGroupID","ChooseGroupID",$ParChooseGroupID);
		$x .= $this->Get_Input_Hidden("order","order",$order);
		$x .= $this->Get_Input_Hidden("field","field",$field);
		$x .= $this->Get_Input_Hidden("fromSchool","fromSchool","1");
		$x .= $this->Get_Input_Hidden("ChooseType","ChooseType",$ChooseType);
		$x .= $this->Get_Input_Hidden("PersonalType","PersonalType",$PersonalType);
		$x .= $this->Get_Input_Hidden("TmpChooseUserIDs","TmpChooseUserIDs");

		if (is_array($ParGroupArr["Users"]) && count($ParGroupArr["Users"]) >0)
		{
			$TempUserIDs = implode(",",$ParGroupArr["Users"]);
		}
		$x .= $this->Get_Input_Textarea("TempUserIDs", $TempUserIDs,70,5,"","style='display: none' ");
		$x .= $this->Get_Input_Textarea("TempDeleteUserIDs", $TempDeleteUserIDs,70,5,"","style='display: none' ");
		$x .= $this->Get_Input_Textarea("TempDeleteUsers", $TempDeleteUsers, 10, 5, "", "style='display: none' ");
		$x .= $this->Get_Input_Hidden("TempAction","TempAction");
		$x .= $this->Get_Input_Hidden("IsEditing","IsEditing",1);
		$x .= $this->Get_Input_Textarea("ToSchoolAddress", $ToSchoolAddress, 70, 5, "", "style='display: none' ");
		
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();

		$x .= $this->Get_Div_Close();	//imail_right

		return $x;
	}

	function Get_Contact_Already_exist($MailAddress) {
		global $SYS_CONFIG, $Lang, $PathRelative;

		$x .= $this->Get_Div_Open("commontabs_board",'class="bulletin_board"');
		$x .= $this->Get_Div_Open("bulletin_recordtop");
		$x .= $this->Get_Div_Open("btn_link");
		$x .= $this->Get_HyperLink_Open("#");
		$x .= $this->Get_Span_Open("",'class="content_bold"');
		$x .= $Lang['Warning']['ContactExistWarning'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("",'class="sub_layer_link"  style="float:right; font-size:9px; padding-top:5px;"');
		$x .= $this->Get_HyperLink_Open("#",'onclick="document.getElementById(\'EditMemberForm\').reset();" class="sub_layer_link"');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();

		return $x;
	}

	function Get_Simple_Add_Contact($MailAddress) {
		global $SYS_CONFIG, $Lang, $PathRelative;

	}

	function Get_New_Contact($ParUserArr=array(), $CacheFormat=false, $ParAction="add_group", $ParGroupID="")
	{
		Global $Lang, $SYS_CONFIG, $PathRelative;

		if($CacheFormat == false){ 
			if($ParAction == "add_group"){
				$form_link = 'address_add_group_new_user_process.php';
			}elseif($ParAction == "edit_group") {
				$form_link = 'address_edit_group_new_user_process.php';
			}elseif($ParAction == "add_group_school") {
				$form_link = 'address_add_group_school_new_user_process.php';
			}elseif($ParAction == "edit_group_school") {	
				$form_link = 'address_edit_group_school_new_user_process.php';
			}
		} else {
			if($ParAction == "add_group"){
				$form_link = 'cache_address_add_group_new_user_process.php';
			}elseif($ParAction == "edit_group") {
				$form_link = 'cache_address_edit_group_new_user_process.php';
			}elseif($ParAction == "add_group_school") {
				$form_link = 'cache_address_add_group_school_new_user_process.php';
			}elseif($ParAction == "edit_group_school") {	
				$form_link = 'cache_address_edit_group_school_new_user_process.php';
			}
		}

		// Layer for New Contact Setup
		$SetupContactLayer = '';
		$SetupContactLayer .= '
					<!-- YUI Panel -->
					<link rel="stylesheet" type="text/css" href="'.$PathRelative.'src/include/js/ajax/build/container/assets/skins/sam/container.css">
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/yahoo-dom-event/yahoo-dom-event.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/dragdrop/dragdrop-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/container/container-min.js"></script>
				 ';

		$SetupContactLayer .= $this->Get_Div_Open("SetupContactLayer");
		$SetupContactLayer .= $this->Get_Form_Open("SetupContactForm", "POST", $form_link);
		$SetupContactLayer .= $this->Get_Div();
		$SetupContactLayer .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$SetupContactLayer .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$SetupContactLayer .= $this->Get_Span_Open("",'CLASS="bubble_board_03_b"').$this->Get_Span_Open("",'CLASS="bubble_board_04_b"');
		$SetupContactLayer .= $this->Get_Div_Open("",'style="width:90%"');
		$SetupContactLayer .= $this->Get_Table_Open("", "100%", "", "0", "0", "0", "");
		$SetupContactLayer .= $this->Get_Table_Row_Open();
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','colspan="3"');
		$SetupContactLayer .= $this->Get_Span_Open('WarningMsgLayer','class="warning"');
		$SetupContactLayer .= $this->Get_Span_Close();
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Close();
		// Name
		$SetupContactLayer .= $this->Get_Table_Row_Open('NameRow');
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="48%"');
		$SetupContactLayer .= $Lang['email']['FirstName'];
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="4%"');
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="48%"');
		$SetupContactLayer .= $Lang['email']['Surname'];
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Open('NameDataRow');
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="48%"');
		$SetupContactLayer .= $this->Get_Input_Text("Firstname","",'class="textbox"');
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="4%"');
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="48%"');
		$SetupContactLayer .= $this->Get_Input_Text("Surname","",'class="textbox"');
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Close();
		// Email
		$SetupContactLayer .= $this->Get_Table_Row_Open('EmailRow');
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="100%" colspan="3"');
		$SetupContactLayer .= $Lang['email']['EmailAddress'];
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Open('EmailDataRow');
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','width="100%" colspan="3"');
		$SetupContactLayer .= $this->Get_Input_Text("UserEmail","",'class="textbox"');
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Open();
		$SetupContactLayer .= $this->Get_Table_Cell_Open('','colspan="3"');
		$SetupContactLayer .= $this->Get_Div_Open("form_btn");
		$SetupContactLayer .= $this->Get_Input_Button("Submit","Submit",$Lang['btn_confirm'],'onclick="Check_Setup_Contact_Submit()" class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="HidePanel();" CLASS="button_main_act"');
		$SetupContactLayer .= $this->Get_Div_Close();
		$SetupContactLayer .= $this->Get_Table_Cell_Close();
		$SetupContactLayer .= $this->Get_Table_Row_Close();
		$SetupContactLayer .= $this->Get_Table_Close();
		$SetupContactLayer .= $this->Get_Div_Close();
		$SetupContactLayer .= $this->Get_Span_Close();
		$SetupContactLayer .= $this->Get_Span_Close();
		$SetupContactLayer .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$SetupContactLayer .= $this->Get_Div_Close();
		$SetupContactLayer .= $this->Get_Div_Close();
		if (is_array($ParUserArr) && count($ParUserArr) >0)
		{
			$TempUserIDs = implode(",",$ParUserArr);
		}
		$ChooseGroupID = $ParGroupID;
		$SetupContactLayer .= $this->Get_Input_Hidden("TempUserIDs","TempUserIDs",$TempUserIDs);
		$SetupContactLayer .= $this->Get_Input_Hidden("ChooseGroupID","ChooseGroupID",$ChooseGroupID);
		$SetupContactLayer .= $this->Get_Input_Hidden("TempGroupName","TempGroupName","");	// TempGroupName
		$SetupContactLayer .= $this->Get_Input_Hidden("TempGroupEmail","TempGroupEmail","");	// TempGroupEmail
		$SetupContactLayer .= $this->Get_Form_Close();
		$SetupContactLayer .= $this->Get_Div_Close();

		$SetupContactLayer .= '
			<script>
				var myPanel = new YAHOO.widget.Panel("SetupContactLayer", { width:"240px", visible:false, constraintoviewport:true });

				function Show_Contact_Setup(obj) {
					document.getElementById(\'Firstname\').value = \'\';
					document.getElementById(\'Surname\').value = \'\';
					document.getElementById(\'UserEmail\').value = \'\';
					document.getElementById(\'WarningMsgLayer\').innerHTML = \'\';
					// commented by kenneth chung on 20090114, no need to set visible for YUI panel
					/*document.getElementById(\'NameRow\').style.visibility = \'visible\';
					document.getElementById(\'NameRow\').style.display = \'table-row\';
					document.getElementById(\'NameDataRow\').style.visibility = \'visible\';
					document.getElementById(\'NameDataRow\').style.display = \'table-row\';
					document.getElementById(\'EmailRow\').style.visibility = \'visible\';
					document.getElementById(\'EmailRow\').style.display = \'table-row\';
					document.getElementById(\'EmailDataRow\').style.visibility = \'visible\';
					document.getElementById(\'EmailDataRow\').style.display = \'table-row\';*/

					var pos_left = getPostion(obj,"offsetLeft")-230;
					var pos_top  = getPostion(obj,"offsetTop")-10;

					myPanel.cfg.setProperty("x", pos_left);
					myPanel.cfg.setProperty("y", pos_top);

					myPanel.show();
				}

				function Check_Setup_Contact_Submit() {
					if (document.getElementById(\'Firstname\').value.Trim() == ""){
						document.getElementById(\'WarningMsgLayer\').innerHTML = "'.$Lang['Warning']['PleaseInputFirstName'].'";
					} else if (document.getElementById(\'Surname\').value.Trim() == ""){
						document.getElementById(\'WarningMsgLayer\').innerHTML = "'.$Lang['Warning']['PleaseInputSurname'].'";
					} else if (document.getElementById(\'UserEmail\').value.Trim() == ""){
						document.getElementById(\'WarningMsgLayer\').innerHTML = "'.$Lang['Warning']['PleaseInputEmail'].'";
					} else if (!Validate_Email(document.getElementById(\'UserEmail\').value.Trim())){
						document.getElementById(\'WarningMsgLayer\').innerHTML = "'.$Lang['Warning']['InvalidEmail'].'";
					} else{
						var GpName = document.getElementById(\'GroupName\').value;
						document.getElementById(\'TempGroupName\').value = GpName;

						if(document.getElementById(\'GroupEmail\')) {
							var GpEmail = document.getElementById(\'GroupEmail\').value;
							document.getElementById(\'TempGroupEmail\').value = GpEmail;
						}
						document.getElementById(\'SetupContactForm\').submit();
					}
				}

				function HidePanel() {
					// commented by kenneth chung on 20090114, no need to set visible for YUI panel
					/*document.getElementById(\'NameRow\').style.visibility = \'hidden\';
					document.getElementById(\'NameRow\').style.display = \'none\';
					document.getElementById(\'NameDataRow\').style.visibility = \'hidden\';
					document.getElementById(\'NameDataRow\').style.display = \'none\';
					document.getElementById(\'EmailRow\').style.visibility = \'hidden\';
					document.getElementById(\'EmailRow\').style.display = \'none\';
					document.getElementById(\'EmailDataRow\').style.visibility = \'hidden\';
					document.getElementById(\'EmailDataRow\').style.display = \'none\';*/
					myPanel.hide();
				}
			</script>';
		// ------ end Layer for New Contact Setup ------

		$ReturnStr = "";
		$ReturnStr .= $SetupContactLayer;

		return $ReturnStr;
	}
}