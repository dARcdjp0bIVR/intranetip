<?php
# using: 

############## Change Log Start ###############
#
#   Date:   2019-09-25 Tommy
#           modified displayGroupPage and displayGroupPage2 for choosing available club group 
#
# Date:	2010-12-13 YatWoon
#		update returnOtherGroups(), add "TitleChinese" and update order by
#
#	Date:	2010-04-15 YatWoon
#			update displayGroupPage(), update display order of icon list, remove some useless icon (i.e. events)
#	
#	Date:	2010-01-27	YatWoon
#			update displayGroupPage2(), user should not access the group in "Other Groups"
#
#	Date:	20100114 Marcus
#			add return field DisplayInCommunity in function returnOtherGroups
#
############## Change Log End ###############

	class libuser2007 extends libuser
	{
		function libuser2007($uid="")
		{
			$this->libuser($uid);
		}
		
        # /home/eCommunity/index.php
        function displayGroupPage($ParAcademicYearID='', $showDisplayIneCommOnly=0)
        {
             global $image_path, $LAYOUT_SKIN;
             global $i_frontpage_schoolinfo_groupinfo_group_timetable
             ,$i_frontpage_schoolinfo_groupinfo_group_chat
             ,$i_frontpage_schoolinfo_groupinfo_group_bulletin
             ,$i_frontpage_schoolinfo_groupinfo_group_links
             ,$i_frontpage_schoolinfo_groupinfo_group_files
             ,$i_adminmenu_plugin_qb
             ,$i_frontpage_schoolinfo_groupinfo_group_settings
             ,$i_no_record_exists_msg
             ;
             include_once("libclubsenrol.php");
             $libenroll = new libclubsenrol();
             
             $groups = $this->returnGroupsWithFunctionAdmin($ParAcademicYearID,$showDisplayIneCommOnly);
             $groupList = "";
             $delimiter = "";
             for ($i=0; $i<sizeof($groups); $i++)
             {
                  $groupList .= $delimiter.$groups[$i][0];
                  $delimiter = ",";
             }
             $available = returnGroupAvailableFunctions();
             $announce = $this->returnGroupsNewAnnounce($groupList);
             $bulletin = $this->returnGroupsNewBulletin($groupList);
             $links = $this->returnGroupsNewLink($groupList);
             $files = $this->returnGroupsNewFile($groupList);

			 $x = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"3\" >\n";
			 					 	 
			for ($i=0; $i<sizeof($groups); $i++)
			{
				list ($id, $name, $function, $admin, $type) = $groups[$i];
				//$grouplink = "<a href=\"javascript:openGroupPage($id)\" class=\"indextabclasslist\" >$name</a>";
				$grouplink = "<a href=\"{$PATH_WRT_ROOT}eCommunity/group/index.php?GroupID={$id}\" class=\"indextabclasslist\" >$name</a>";
				$countBull = $bulletin[$id];
				$countLink = $links[$id];
				$countFile = $files[$id];
				$countBull = ($countBull == "" || $countBull == 0? "0":"($countBull)");
				$countLink = ($countLink == "" || $countLink == 0? "0":"($countLink)");
				$countFile = ($countFile == "" || $countFile == 0? "0":"($countFile)");
				# Field : FunctionAccess
				#         - Bit-1 (LSB) : timetable
				#         - Bit-2       : chat
				#         - Bit-3       : bulletin
				#         - Bit-4       : shared links
				#         - Bit-5       : shared files
				#         - Bit-6       : question bank
				#         - Bit-7       : Photo Album
				#         - Bit-8 (MSB) : Survey
				if ($function == "ALL")
				{
					$isTimetable = ($available[0]!="");
					$isChat = ($available[1]!="");
					$isBulletin = ($available[2]!="");
					$isLink = ($available[3]!="");
					$isFile = ($available[4]!="");
					$isQB = $type == 2 && ($available[5]!="");
					$isPhoto = ($available[6]!="");
					$isSurvey = ($available[7]!="");
				}
				else
				{
					$isTimetable = (substr($function,0,1)=='1' && $available[0]!="");
					$isChat = (substr($function,1,1)=='1' && $available[1]!="");
					$isBulletin = (substr($function,2,1)=='1' && $available[2]!="");
					$isLink = (substr($function,3,1)=='1' && $available[3]!="");
					$isFile = (substr($function,4,1)=='1' && $available[4]!="");
					$isQB = $type == 2 && (substr($function,5,1)=='1' && $available[5]!="");
					$isPhoto = (substr($function,6,1)=='1' && $available[6]!="");
					$isSurvey = (substr($function,7,1)=='1' && $available[7]!="");
				}
				$functionIcon = "";
				if ($isFile || $isPhoto)
				{
					# 20081020 - display the link even the count is 0
					$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}eCommunity/files/index.php?GroupID=$id\" onMouseOver=\"MM_swapImage('eclassdoc{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" class=\"indextabclassicon\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc.gif\" name=\"eclassdoc{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_files'><br />$countFile</a></td>\n";				
					
// 					if ($countFile == "0")
// 					{
// 						$functionIcon .= "<td align='center' ><span class=\"indextabclassiconoff\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_off.gif\" name=\"eclassdoc\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc\" alt='$i_frontpage_schoolinfo_groupinfo_group_files'>$countFile</span></td>\n";
// 					} else {
// 						//$functionIcon .= "<td align='center' ><a href=\"javascript:fe_view_files($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassdoc{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc.gif\" name=\"eclassdoc{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_files'><br />$countFile</a></td>\n";				
// 						$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}eCommunity/files/index.php?GroupID=$id\" onMouseOver=\"MM_swapImage('eclassdoc{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" class=\"indextabclassicon\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc.gif\" name=\"eclassdoc{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_files'><br />$countFile</a></td>\n";				
// 					}
				}					
				if ($isBulletin)
				{
					# 20081020 - display the link even the count is 0
					$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}eCommunity/bulletin/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassforum{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" name=\"eclassforum{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassforum{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_bulletin'> <br />{$countBull}</a></td>\n";				
					
// 					if ($countBull == "0")
// 					{
// 						$functionIcon .= "<td align='center' ><span class=\"indextabclassiconoff\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_off.gif\" name=\"eclassforum\" border=\"0\" align=\"absmiddle\" id=\"eclassforum\" alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'>{$countBull}</span></td>\n";						
// 					} else {
// 						//$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_bulletin($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassforum{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" name=\"eclassforum{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassforum{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'> <br />{$countBull}</a></td>\n";				
// 						$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}eCommunity/bulletin/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassforum{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" name=\"eclassforum{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassforum{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'> <br />{$countBull}</a></td>\n";				
// 					}
				}				
				if ($isLink)
				{
					# 20081020 - display the link even the count is 0
					//$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_links($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclasslink{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</a></td>\n";
					$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}eCommunity/files/index2.php?GroupID=$id&FileType=W\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclasslink{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</a></td>\n";
					
// 					if ($countLink == "0")
// 					{
// 						$functionIcon .= "<td align='center' ><span class=\"indextabclassiconoff\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_off.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</span></td>\n";						
// 					} else {
// 						$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_links($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclasslink{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</a></td>\n";
// 					}															
				}
				if ($isChat)
				{
					//$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_chat($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('comchat{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_chatroom_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"/images/2007a/index/group_tab/icon_list/icon_chatroom.gif\" name=\"comchat{$i}\" border=\"0\" align=\"absmiddle\" id=\"comchat{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_chat' /></a></td>\n";					
					$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}eCommunity/chat/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('comchat{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_chatroom_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_chatroom.gif\" name=\"comchat{$i}\" border=\"0\" align=\"absmiddle\" id=\"comchat{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_chat'></a></td>\n";				
				}				
				if ($isQB)
					$functionIcon .= "<td align='center' ><a href=javascript:fe_view_qb($id)><img src=/images/icon_schoolbasedqb.gif border=0 title='$i_adminmenu_plugin_qb'></a></td>\n";
				if ($isPhoto)
					$functionIcon .= ""; # Not yet implemented
				if ($isSurvey)
					$functionIcon .= ""; # Not yet implemented
				if ($admin == "A")
					//$functionIcon .= "<td align='center' ><a href=\"javascript:fe_view_settings($id)\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_groupsetting.gif\" name=\"com_setting{$i}\" border=\"0\" id=\"com_setting{$i}\" onMouseOver=\"MM_swapImage('com_setting{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_groupsetting_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" title=\"$i_frontpage_schoolinfo_groupinfo_group_settings\" ></a></td>\n";
					$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}eCommunity/group/settings.php?GroupID=$id\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_groupsetting.gif\" name=\"com_setting{$i}\" border=\"0\" id=\"com_setting{$i}\" onMouseOver=\"MM_swapImage('com_setting{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_groupsetting_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" title=\"$i_frontpage_schoolinfo_groupinfo_group_settings\" ></a></td>\n";
					/*
				if ($isTimetable)
					$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_timetable($id)\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_timetable.gif\" name=\"com_timet{$i}\" border=\"0\" id=\"com_timet{$i}\" onMouseOver=\"MM_swapImage('com_timet{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_timetable_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" alt='$i_frontpage_schoolinfo_groupinfo_group_timetable' ></a></td>\n";
					*/
															
				if ($i%2==0)
              	{
                  	$RowStyle= " class=\"indextabwhiterow\" ";
              	} else {
                  	$RowStyle= " class=\"indextaborangerow\" ";
              	}
              	
              	$outputResultDateTime = $libenroll->AnnounceEnrolmentResultDate." ".$libenroll->AnnounceEnrolmentResultHour.':'.$libenroll->AnnounceEnrolmentResultMin;
              	$currentDateTime = date('Y-m-d H:i');
              	$userinEnrolGroup = $this->approvedUserinEnrol($this->UserID);
              	$alreadyExist = $this->groupRecordExist($this->UserID, $id);
              	if($type == 5 && $currentDateTime < $outputResultDateTime && sizeof($userinEnrolGroup) > 0 && sizeof($alreadyExist) == 0){
              	
			    }else{
              	$x .= 	"
							<tr>
								<td {$RowStyle}>
								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								<tr>
									<td width=\"19\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_communites.gif\" width=\"20\" height=\"20\"></td>
									<td>{$grouplink}</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>																				
									<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
									{$functionIcon}
									</table>
									
									</td>
								</tr>
								</table>
								</td>
							</tr>              
              				";
              	}
                      
			}
                 
			if(sizeof($groups)==0)
			{
				$x .= "<tr>
							<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
							<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
							<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
						</tr>
						";			
			}
			$x .= "</table>\n";
                 
			return $x;

        }
	
		# modified by marcus 10/09/2009 - added parameter $ParAcademicYearID in order to get groups in specific Year
        function displayGroupPage2($non=0, $ParAcademicYearID='')
        {
             global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
             global $i_frontpage_schoolinfo_groupinfo_group_timetable
             ,$i_frontpage_schoolinfo_groupinfo_group_chat
             ,$i_frontpage_schoolinfo_groupinfo_group_bulletin
             ,$i_frontpage_schoolinfo_groupinfo_group_links
             ,$i_frontpage_schoolinfo_groupinfo_group_files
             ,$i_frontpage_schoolinfo_groupinfo_announcement
             ,$i_adminmenu_plugin_qb
             ,$i_frontpage_schoolinfo_groupinfo_group_settings
             ,$i_no_record_exists_msg
             ;
             include_once("libclubsenrol.php");
             $libenroll = new libclubsenrol();
             
             if(!$non)
             	$groups = $this->returnGroupsWithFunctionAdmin($ParAcademicYearID,1);
             else
             	$groups = $this->returnOtherGroups($ParAcademicYearID,1);
             	
             $groupList = "";
             $delimiter = "";
             for ($i=0; $i<sizeof($groups); $i++)
             {
                  $groupList .= $delimiter.$groups[$i][0];
                  $delimiter = ",";
             }
             $available = returnGroupAvailableFunctions();
             if(!$non)
             {
	             $announce = $this->returnGroupsNewAnnounce($groupList);
	             $bulletin = $this->returnGroupsNewBulletin($groupList);
	             $links = $this->returnGroupsNewLink($groupList);
	             $files = $this->returnGroupsNewFile($groupList);
             }
             
			 $x = "<table width=\"100%\" border=\"0\" cellpadding=\"8\" cellspacing=\"0\" >\n";
			$x .= "<tr>";

			if(!$non)
	         {
	         	include_once("libannounce.php");
				$la = new libannounce();
	         	$GroupIDArr = Get_Array_By_Key($groups,"GroupID");
				$unread_announcement_arr = $la->returnUnreadGroupAnnouncementArr($GroupIDArr);
				
	         }

			$tr_i = 0;					 	 
			for ($i=0; $i<sizeof($groups); $i++)
			{ 
				list ($id, $name, $function, $admin, $type, $publicstatus, $DisplayInCommunity) = $groups[$i];
				if(!$DisplayInCommunity) continue;
				
				//$grouplink = "<a href=\"javascript:openGroupPage($id)\" class=\"tabletoplink\" >$name</a>";
				if($non)
					$grouplink = "<span class='tabletopnolink'>$name</span>";
				else
					$grouplink = "<a href=\"{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$id}\" class=\"tabletoplink\" >$name</a>";
				if(!$publicstatus)
				{
					if($this->isInGroup($id))
						$grouplink .= " <img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\" align=\"absmiddle\">";
					else
						continue;
				}
					
				
				//$this->isInGroup($id);
				//echo $publicstatus;
								
				$countBull = $bulletin[$id];
				$countLink = $links[$id];
				$countFile = $files[$id];
				$countBull = ($countBull == "" || $countBull == 0? "0":"$countBull");
				$countLink = ($countLink == "" || $countLink == 0? "0":"$countLink");
				$countFile = ($countFile == "" || $countFile == 0? "0":"$countFile");
				# Field : FunctionAccess
				#         - Bit-1 (LSB) : timetable
				#         - Bit-2       : chat
				#         - Bit-3       : bulletin
				#         - Bit-4       : shared links
				#         - Bit-5       : shared files
				#         - Bit-6       : question bank
				#         - Bit-7       : Photo Album
				#         - Bit-8 (MSB) : Survey
				if ($function == "ALL")
				{
					$isTimetable = ($available[0]!="");
					$isChat = ($available[1]!="");
					$isBulletin = ($available[2]!="");
					$isLink = ($available[3]!="");
					$isFile = ($available[4]!="");
					$isQB = $type == 2 && ($available[5]!="");
					$isPhoto = ($available[6]!="");
					$isSurvey = ($available[7]!="");
				}
				else
				{
					$isTimetable = (substr($function,0,1)=='1' && $available[0]!="");
					$isChat = (substr($function,1,1)=='1' && $available[1]!="");
					$isBulletin = (substr($function,2,1)=='1' && $available[2]!="");
					$isLink = (substr($function,3,1)=='1' && $available[3]!="");
					$isFile = (substr($function,4,1)=='1' && $available[4]!="");
					$isQB = $type == 2 && (substr($function,5,1)=='1' && $available[5]!="");
					$isPhoto = (substr($function,6,1)=='1' && $available[6]!="");
					$isSurvey = (substr($function,7,1)=='1' && $available[7]!="");
				}
				$functionIcon = "";
				
				# Announcement
//	             if(!$non)
//    	         {
//					include_once("libannounce.php");
//					$la = new libannounce();
//					$unread_announcement = $la->returnGroupAnnouncement_unread($id);
					$unread_announcement = $unread_announcement_arr[$id]>0?$unread_announcement_arr[$id]:0;
//    	         }
				$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassannouncement{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_annou_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_annou.gif\" name=\"eclassannouncement{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassannouncement{$i}\" title='$i_frontpage_schoolinfo_groupinfo_announcement'> $unread_announcement</a></td>\n";				
				
				if ($isFile||$isPhoto)
				{
					$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}home/eCommunity/files/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassdoc{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc.gif\" name=\"eclassdoc{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_files'> $countFile</a></td>\n";				
					
					# 20081020 - display the link even the count is 0
// 					if ($countFile == "0")
// 					{
// 						$functionIcon .= "<td align='center' ><span class=\"indextabclassiconoff\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_off.gif\" name=\"eclassdoc\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc\" alt='$i_frontpage_schoolinfo_groupinfo_group_files'>$countFile</span></td>\n";
// 					} else {
// 						//$functionIcon .= "<td align='center' ><a href=\"javascript:fe_view_files($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassdoc{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc.gif\" name=\"eclassdoc{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_files'> $countFile</a></td>\n";				
// 						$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}home/eCommunity/files/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassdoc{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_doc.gif\" name=\"eclassdoc{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassdoc{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_files'> $countFile</a></td>\n";				
// 					}
				}		
				
				if ($isLink)
				{
					# 20081020 - display the link even the count is 0
					$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}home/eCommunity/files/index2.php?GroupID=$id&FileType=W\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclasslink{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</a></td>\n";
					
// 					if ($countLink == "0")
// 					{
// 						$functionIcon .= "<td align='center' ><span class=\"indextabclassiconoff\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_off.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</span></td>\n";						
// 					} else {
// 						$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}home/eCommunity/files/index2.php?GroupID=$id&FileType=W\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclasslink{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link.gif\" name=\"eclasslink{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclasslink{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_links' />{$countLink}</a></td>\n";
// 					}															
				}							
				if ($isBulletin)
				{
					# 20081020 - display the link even the count is 0
					$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}home/eCommunity/bulletin/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassforum{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" name=\"eclassforum{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassforum{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_bulletin'> {$countBull}</a></td>\n";				
					
// 					if ($countBull == "0")
// 					{
// 						$functionIcon .= "<td align='center' ><span class=\"indextabclassiconoff\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_off.gif\" name=\"eclassforum\" border=\"0\" align=\"absmiddle\" id=\"eclassforum\" alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'>{$countBull}</span></td>\n";						
// 					} else {
// 						//$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_bulletin($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassforum{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" name=\"eclassforum{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassforum{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'> {$countBull}</a></td>\n";				
// 						$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}home/eCommunity/bulletin/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('eclassforum{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" name=\"eclassforum{$i}\" border=\"0\" align=\"absmiddle\" id=\"eclassforum{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'> {$countBull}</a></td>\n";				
// 					}
				}				
				
				if ($isChat)
				{
					//$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_chat($id)\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('comchat{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_chatroom_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"/images/2007a/index/group_tab/icon_list/icon_chatroom.gif\" name=\"comchat{$i}\" border=\"0\" align=\"absmiddle\" id=\"comchat{$i}\" alt='$i_frontpage_schoolinfo_groupinfo_group_chat' /></a></td>\n";					
					$functionIcon .= "<td align='center'><a href=\"{$PATH_WRT_ROOT}home/eCommunity/chat/index.php?GroupID=$id\" class=\"indextabclassicon\" onMouseOver=\"MM_swapImage('comchat{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_chatroom_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_chatroom.gif\" name=\"comchat{$i}\" border=\"0\" align=\"absmiddle\" id=\"comchat{$i}\" title='$i_frontpage_schoolinfo_groupinfo_group_chat'></a></td>\n";				
				}				
				if ($isQB)
					$functionIcon .= "<td align='center' ><a href=javascript:fe_view_qb($id)><img src=/images/icon_schoolbasedqb.gif border=0 title='$i_adminmenu_plugin_qb'></a></td>\n";
				if ($isPhoto)
					$functionIcon .= ""; # Not yet implemented
				if ($isSurvey)
					$functionIcon .= ""; # Not yet implemented
				if ($admin == "A")
					$functionIcon .= "<td align='center' ><a href=\"{$PATH_WRT_ROOT}home/eCommunity/group/settings.php?GroupID=$id\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_groupsetting.gif\" name=\"com_setting{$i}\" border=\"0\" id=\"com_setting{$i}\" onMouseOver=\"MM_swapImage('com_setting{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_groupsetting_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" title=\"$i_frontpage_schoolinfo_groupinfo_group_settings\" ></a></td>\n";
				/*
				if ($isTimetable)
					$functionIcon .= "<td align='center'><a href=\"javascript:fe_view_timetable($id)\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_timetable.gif\" name=\"com_timet{$i}\" border=\"0\" id=\"com_timet{$i}\" onMouseOver=\"MM_swapImage('com_timet{$i}','','{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_timetable_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" alt='$i_frontpage_schoolinfo_groupinfo_group_timetable' ></a></td>\n";
					*/
															
				if ($i%2==0)
              	{
                  	$RowStyle= " class=\"indextabwhiterow\" ";
              	} else {
                  	$RowStyle= " class=\"indextaborangerow\" ";
              	}
                
              	$outputResultDateTime = $libenroll->AnnounceEnrolmentResultDate." ".$libenroll->AnnounceEnrolmentResultHour.':'.$libenroll->AnnounceEnrolmentResultMin;
              	$currentDateTime = date('Y-m-d H:i');
              	$userinEnrolGroup = $this->approvedUserinEnrol($this->UserID);
              	$alreadyExist = $this->groupRecordExist($this->UserID, $id);
              	if($type == 5 && $currentDateTime < $outputResultDateTime && sizeof($userinEnrolGroup) > 0 && sizeof($alreadyExist) == 0){
              	    
              	}else{
                $x .="
                	<td width='50%' valign='top'>
                        	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                <td width='10' background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>
                                <td background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>
                                <tr>
                                <td width='19'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup.gif' width='20' height='20'></td>
                                <td>{$grouplink}</td>
                                </tr>
                                </table></td>
                                <td width='12' background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>
                                </tr>
                                <tr>
                                <td width='10' background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board04.gif' width='10' height='11'></td>
                                <td background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>
                                <tr>
                                <td>";
                                
                                if(!$non)
                                {
	                                $x .= "
	                                <table border='0' cellspacing='0' cellpadding='6'>
	                                <tr>
	                                {$functionIcon}
	                                </tr>
	                                </table>";
                            	}
                                
                               $x .="</td>
                                </tr>
                                </table></td>
                                <td width='12' background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board06.gif' width='12' height='12'></td>
                                </tr>
                                <tr>
                                <td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board07.gif' width='10' height='10'></td>
                                <td height='10' background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board08.gif' width='10' height='10'></td>
                                <td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/group_board09.gif' width='12' height='10'></td>
                                </tr>
                                </table>
                        </td>
		";
              	}
              	
                if(sizeof($groups)==1)
                {
                	$x .= "<td width='50%'>&nbsp;</td>"; 
                }
                
                $x .= ($tr_i%2==1) ? "</tr>\n<tr>\n" : "";
                $tr_i++;
		}
                 
			if(sizeof($groups)==0)
			{
				$x .= "<tr>
							<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br><br><br><br><br>$i_no_record_exists_msg</td>
						</tr>
						";			
			}
			$x .= "</table>\n";
                 
			return $x;

        }		
        
        function OtherGroupStr()
        {
			$mygroup_str = ":";
	        
	        $groups = $this->returnGroupsWithFunctionAdmin();
	        for($i=0;$i<sizeof($groups);$i++)
	        {
		     	list ($id, $name, $function, $admin, $type) = $groups[$i];
		     	$mygroup_str .= $id .":";
	        }
	        
	        return $mygroup_str;
        }
        
		# modified by marcus 10/09/2009 - added parameter $ParAcademicYearID in order to get groups in specific Year
        function returnOtherGroups($ParAcademicYearID='', $showDisplayIneCommOnly=0)
        {
	        global $intranet_session_language;
	        
	        $mygroup_str = $this->OtherGroupStr();

            if(!empty($ParAcademicYearID))
               	$YearFilter = " AND a.AcademicYearID = '$ParAcademicYearID'";
                if($showDisplayIneCommOnly == 1)
                	$cond_DisplayInCommunity = " AND a.DisplayInCommunity = '1' ";
                	
                $title_field = $intranet_session_language == "en" ? "a.Title":"a.TitleChinese";	
                $sql = "SELECT 
							a.GroupID, 
							". $title_field .", 
							IF(a.FunctionAccess IS NULL,'ALL',REVERSE(BIN(a.FunctionAccess))),
							'',
							'',
							a.PublicStatus,
							a.DisplayInCommunity
						FROM 
							INTRANET_GROUP AS a 
						WHERE 
							'$mygroup_str' not like concat('%:',a.GroupID,':%')
							$YearFilter
							$cond_DisplayInCommunity
						ORDER BY 
							a.RecordType+0, " . $title_field;
                
                $others = $this->returnArray($sql,6);
                return $others;
        }
        
        function isInGroup($GroupID)
        {
	        $myGroupIDs = $this->returnGroupIDs();
	        return (in_array($GroupID, $myGroupIDs));
        }
	}
?>