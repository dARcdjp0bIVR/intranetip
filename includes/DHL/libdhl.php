<?php
// Editing by

/*
 * Change Log
 * 2020-01-17 (Bill):   Modified upsertUserRecord(), handle import update logic for 2 types - Movement & Leaver [2020-0117-1616-21226]
 * 2019-08-02 (Bill):   Modified deleteDepartmentUserRecord(), support delete records in INTRANET_USERGROUP when $departmentId is empty [2019-0729-1711-21226]
 * 2018-04-25 (Carlos): Modified getDepartmentUserRecords($filterMap), add parameter UserStatus to filter INTRANET_USER.RecordStatus
 * 2018-02-22 (Carlos): Fixed auotActivateSuspendUserAccounts() move users to new department but failed to delete from old department intranet group bug.
 * 2017-08-17 (Carlos): Fixed convertTargetSelectionToUserAry($targetAry) parse division id logic.
 * 2017-08-02 (Ivan):	Modified deleteDepartmentRecord(), deleteDivisionRecord(), deleteCompanyRecord() to add delete log
 * 2017-07-03 (Carlos): Modified getIntranetGroupSelection() added optional parameter $display_prefix_code_in_group_name=true;
 * 						Modified getDepartmentSelection() added optional parameter $fetchIntranetGroup=false
 */
global $intranet_root;
include_once($intranet_root."/includes/libdb.php");

class libdhl extends libdb 
{	
	var $is_magic_quotes_active = false;
	
	function libdhl()
	{
		$this->libdb();
		
		$this->is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
	}
	
	function isModuleOn()
	{
		global $sys_custom;
		
		return $sys_custom['DHL'];
	}
	
	function isDev()
	{
		$is_dev = false;
		if(preg_match('/^192\..+$/', $_SERVER['SERVER_NAME'])){
			$is_dev = true;
		}
		return $is_dev;
	}
	
	function log($log_row)
	{
		global $intranet_root, $file_path, $sys_custom;
		
		$num_line_to_keep = isset($sys_custom['DHLLogMaxLine'])? $sys_custom['DHLLogMaxLine'] : 100000;
		$log_file_path = $file_path."/file/dhl_log";
		$log_file_path_tmp = $file_path."/file/dhl_log_tmp";
		// assume $log_row does not contains single quote that would break the shell command statement
// 		shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
		shell_exec("echo '$log_row' >> ".OsCommandSafe($log_file_path)); 
		$line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
		if($line_count > $num_line_to_keep){
		    $log_file_path = OsCommandSafe($log_file_path);
		    $log_file_path_tmp = OsCommandSafe($log_file_path_tmp);
		    $num_line_to_keep = OsCommandSafe($num_line_to_keep);
			shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
			shell_exec("rm '$log_file_path'");
			shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
		}
	}
	
	function getTagsObjArr($currentTag)
	{
		global $Lang;
		
		$TAGS_OBJ = array();
		$TAGS_OBJ[] = array($Lang['DHL']['Company'], $currentTag == 'Company'? "index.php?clearCoo=1" : "../company/", $currentTag == 'Company');
		$TAGS_OBJ[] = array($Lang['DHL']['Division'], $currentTag == 'Division'? "index.php?clearCoo=1" : "../division/", $currentTag == 'Division');
		$TAGS_OBJ[] = array($Lang['DHL']['Department'], $currentTag == 'Department'? "index.php?clearCoo=1" : "../department/", $currentTag == 'Department');
		
		return $TAGS_OBJ;
	}
	
	function cleanData($value)
	{
		if($this->is_magic_quotes_active)
		{
			$value = stripslashes($value);
		}
		return trim($value);
	}
	
	function cleanTargetSelectionData($ary, $charToRemove)
	{
		if(!is_array($ary)){
			return str_replace($charToRemove,'',$ary);
		}
		for($i=0;$i<count($ary);$i++){
			$ary[$i] = str_replace($charToRemove,'',$ary[$i]);
		}
		return $ary;
	}
	
	function formatOrganizationDisplayName($companyName,$divisionName,$departmentName)
	{
		return $companyName.' > '.$divisionName.' > '.$departmentName;
	}
	
	function formatDBOriganizationDisplayName($company_db_prefix, $division_db_prefix, $department_db_prefix, $noTooltip=false)
	{
		global $Lang, $intranet_session_language;
			
		$company_name_field = Get_Lang_Selection($company_db_prefix."CompanyNameChi",$company_db_prefix."CompanyNameEng");
		$division_name_field = Get_Lang_Selection($division_db_prefix."DivisionNameChi",$division_db_prefix."DivisionNameEng");
		$department_name_field = Get_Lang_Selection($department_db_prefix."DepartmentNameChi",$department_db_prefix."DepartmentNameEng");
		$x = "CONCAT(";
		if(!$noTooltip) $x.= "'<span title=\"',CONCAT(GROUP_CONCAT($company_name_field SEPARATOR ','),' > ',$division_name_field,' > ',$department_name_field),'\">',";
		$x.= "GROUP_CONCAT(".$company_db_prefix."CompanyCode SEPARATOR ','),' > ',".$division_db_prefix."DivisionCode,' > ',".$department_db_prefix."DepartmentCode";
		if(!$noTooltip) $x.= ",'</span>'";
		$x.= ")";
		return $x;
	}
	
	function formatDBCompanyDisplayName($company_db_prefix, $noTooltip=false)
	{
		global $Lang, $intranet_session_language;
			
		$company_name_field = Get_Lang_Selection($company_db_prefix."CompanyNameChi",$company_db_prefix."CompanyNameEng");
		$x = "CONCAT(";
		if(!$noTooltip) $x.= "'<span title=\"',GROUP_CONCAT($company_name_field SEPARATOR ','),'\">',";
		$x.= "GROUP_CONCAT(".$company_db_prefix."CompanyCode SEPARATOR ',')";
		if(!$noTooltip) $x.= ",'</span>'";
		$x.= ")";
		return $x;
	}
	
	function formatDBDivisionDisplayName($division_db_prefix, $noTooltip=false)
	{
		global $Lang, $intranet_session_language;
			
		$division_name_field = Get_Lang_Selection($division_db_prefix."DivisionNameChi",$division_db_prefix."DivisionNameEng");
		$x = "CONCAT(";
		if(!$noTooltip) $x.= "'<span title=\"',$division_name_field,'\">',";
		$x.= $division_db_prefix."DivisionCode";
		if(!$noTooltip) $x.= ",'</span>'";
		$x.= ")";
		return $x;
	}
	
	function formatDBDepartmentDisplayName($department_db_prefix, $noTooltip=false)
	{
		global $Lang, $intranet_session_language;
			
		$department_name_field = Get_Lang_Selection($department_db_prefix."DepartmentNameChi",$department_db_prefix."DepartmentNameEng");
		$x = "CONCAT(";
		if(!$noTooltip) $x.= "'<span title=\"',$department_name_field,'\">',";
		$x.= $department_db_prefix."DepartmentCode";
		if(!$noTooltip) $x.= ",'</span>'";
		$x.= ")";
		return $x;
	}
	
	function getCompanyRecords($filterMap=array())
	{
		global $Lang, $intranet_session_language;
		$filters = array('CompanyID','Keyword','CheckCompany');
		$name_field = Get_Lang_Selection("c.CompanyNameChi","c.CompanyNameEng");
		$cond = "";
		if(isset($filterMap['CompanyID'])){
			$recordId = IntegerSafe($filterMap['CompanyID']);
			if(is_array($recordId)){
				$cond .= " AND c.CompanyID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND c.CompanyID='$recordId' ";
			}
		}
		if(isset($filterMap['CheckCompany']) && $filterMap['CheckCompany'])
		{
			if(isset($filterMap['ExcludeCompanyID'])){
				$cond .= " AND c.CompanyID <> '".IntegerSafe($filterMap['ExcludeCompanyID'])."' ";
			}
			if(isset($filterMap['CheckCompanyNameChi'])){
				$cond .= " AND c.CompanyNameChi='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckCompanyNameChi']))."' ";
			}
			if(isset($filterMap['CheckCompanyNameEng'])){
				$cond .= " AND c.CompanyNameEng='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckCompanyNameEng']))."' ";
			}
			if(isset($filterMap['CheckCompanyCode'])){
				$cond .= " AND c.CompanyCode='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckCompanyCode']))."' ";
			}
		}
		
		if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
			$keyword = $this->Get_Safe_Sql_Like_Query($this->cleanData($filterMap['Keyword']));
			$cond .= " AND ($name_field LIKE '%$keyword%' OR CompanyCode LIKE '%$keyword%') ";
		}
		
		$picView = $filterMap['picView'];
		if($picView){
		    $PICID = $_SESSION['UserID'];
		    $cond .=" AND dpi.UserID = '$PICID'";
		    $joinTable = "INNER JOIN DHL_PIC_INFO AS dpi ON dpi.ItemID = c.CompanyID AND  dpi.ItemType = 'Company'";
		}
		
		if($filterMap['GetDBQuery']){
			$libdbtable = $filterMap['libdbtable'];	
			
			$modifier_name_field = getNameFieldByLang2("u.");
			$archived_modifier_name_field = getNameFieldByLang2("au.");
			$field_array = array("CompanyName","c.CompanyCode","c.Description","c.DateModified","ModifiedBy");
			$sql = "SELECT 
						CONCAT('<span style=\"display:none\">',$name_field,'</span><a href=\"edit.php?CompanyID=',c.CompanyID,'\">',$name_field,'</a>') as CompanyName,
						c.CompanyCode,
						c.Description,
						c.DateModified,
						IF(u.UserID IS NOT NULL,$modifier_name_field,CONCAT('<span class=\"red\">*</span>',$archived_modifier_name_field)) as ModifiedBy,
						CONCAT('<input type=\"checkbox\" name=\"CompanyID[]\" value=\"',c.CompanyID,'\" />') as Checkbox  
					FROM DHL_COMPANY as c 
					LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifiedBy 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=c.ModifiedBy 
                    $joinTable
					WHERE 1 $cond ";
					
			// TABLE COLUMN
			$pos = 0;
			$column_list = array();
			$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
			$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['CompanyName'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Code'])."</th>\n";
			$column_list[] = "<th width='30%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Description'])."</th>\n";
			$column_list[] = "<th width='17%'>".$libdbtable->column_IP25($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
			$column_list[] = "<th width='17%'>".$libdbtable->column_IP25($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
			$column_list[] = "<th width='1'>".$libdbtable->check("CompanyID[]")."</th>\n";
			
			$column_array = array(0,22,18,22,0);
			$wrap_array = array_fill(0,count($field_array),0);
			
			return array($field_array,$sql,$column_list,$column_array,$wrap_array);
		}
		
		$sql = "SELECT 
					c.* 
				FROM DHL_COMPANY as c 
                $joinTable
				WHERE 1 $cond 
				ORDER BY ".$name_field;
		
		$records = $this->returnResultSet($sql);
		
		return $records;
	}
	
	function upsertCompanyRecord($map)
	{
		$fields = array('CompanyNameChi','CompanyNameEng','CompanyCode','Description');
		$user_id = $_SESSION['UserID'];
		if(count($map) == 0) return false;
		
		$success = true;
		if(isset($map['CompanyID']) && trim($map['CompanyID'])!='' && $map['CompanyID'] > 0)
		{
			$record_id = intval($map['CompanyID']);
			$sql = "UPDATE DHL_COMPANY SET ";
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				
				$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($this->cleanData($val))."'";
				$sep = ",";
			}
			$sql.= " ,DateModified=NOW(),ModifiedBy='".$user_id."' WHERE CompanyID='$record_id'";
			
			$success = $this->db_db_query($sql);
			
			return $success;
		}else{
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				$keys .= $sep."$key";
				$values .= $sep."'".$this->Get_Safe_Sql_Query($this->cleanData($val))."'";
				$sep = ",";
			}
			$keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
			$values .= ",NOW(),NOW(),'$user_id','$user_id'";
			
			$sql = "INSERT INTO DHL_COMPANY ($keys) VALUES ($values)";
			
			$success = $this->db_db_query($sql);
			
			if($success){
				$record_id = $this->db_insert_id();
			}
		}
		
		return $record_id;
	}
	
	function deleteCompanyRecord($recordIdAry)
	{
		if(count($recordIdAry) == 0) return false; 
		
		$recordIdAry = IntegerSafe($recordIdAry);
		
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/liblog.php');
		$liblog = new liblog();
		$sql = "SELECT * FROM DHL_COMPANY WHERE CompanyID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$dataAry = $this->returnResultSet($sql);
		$success = $liblog->INSERT_LOG('Organization', 'DeleteCompany', serialize($dataAry), 'DHL_COMPANY');
		
		$sql = "SELECT DISTINCT DivisionID FROM DHL_DIVISION WHERE CompanyID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$divisionIdAry = $this->returnVector($sql);
		$delete_division_success = $this->deleteDivisionRecord($divisionIdAry);
		
		$sql = "DELETE FROM DHL_COMPANY WHERE CompanyID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function getDivisionRecords($filterMap=array())
	{
		global $Lang, $intranet_session_language;
		$filters = array('Keyword','CheckDivision');
		$company_name_field = Get_Lang_Selection("c.CompanyNameChi","c.CompanyNameEng");
		$division_name_field = Get_Lang_Selection("v.DivisionNameChi","v.DivisionNameEng");
		$cond = "";
		if(isset($filterMap['DivisionID'])){
			$recordId = IntegerSafe($filterMap['DivisionID']);
			if(is_array($recordId)){
				$cond .= " AND v.DivisionID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND v.DivisionID='$recordId' ";
			}
		}
		if(isset($filterMap['CompanyID'])){
			$recordId = IntegerSafe($filterMap['CompanyID']);
			if(is_array($recordId)){
				$cond .= " AND c.CompanyID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND c.CompanyID='$recordId' ";
			}
		}
		if(isset($filterMap['CheckDivision']) && $filterMap['CheckDivision'])
		{
			if(isset($filterMap['ExcludeDivisionID'])){
				$cond .= " AND v.DivisionID <> '".IntegerSafe($filterMap['ExcludeDivisionID'])."' ";
			}
			if(isset($filterMap['CheckDivisionNameChi'])){
				$cond .= " AND v.DivisionNameChi='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckDivisionNameChi']))."' ";
			}
			if(isset($filterMap['CheckDivisionNameEng'])){
				$cond .= " AND v.DivisionNameEng='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckDivisionNameEng']))."' ";
			}
			if(isset($filterMap['CheckDivisionCode'])){
				$cond .= " AND v.DivisionCode='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckDivisionCode']))."' ";
			}
		}
		
		if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
			$keyword = $this->Get_Safe_Sql_Like_Query($this->cleanData($filterMap['Keyword']));
			$cond .= " AND ($name_field LIKE '%$keyword%' OR v.DivisionCode LIKE '%$keyword%') ";
		}
		
		if($filterMap['GetDBQuery']){
			$libdbtable = $filterMap['libdbtable'];
			
			$picView = $filterMap['picView'];
			if($picView){
			    $PICID = $_SESSION['UserID'];
			    $cond .=" AND dpi.UserID = '$PICID'";
			    $joinTable = "INNER JOIN DHL_PIC_INFO AS dpi ON dpi.ItemID = v.DivisionID AND  dpi.ItemType = 'Division'";
			}
			
			$modifier_name_field = getNameFieldByLang2("u.");
			$archived_modifier_name_field = getNameFieldByLang2("au.");
			$field_array = array("DivisionName","v.DivisionCode","CompanyName","v.Description","v.DateModified","ModifiedBy");
			$sql = "SELECT 
						CONCAT('<span style=\"display:none\">',$division_name_field,'</span><a href=\"edit.php?DivisionID=',v.DivisionID,'\">',$division_name_field,'</a>') as DivisionName,
						v.DivisionCode,
						GROUP_CONCAT($company_name_field SEPARATOR ', ') as CompanyName,
						v.Description,
						v.DateModified,
						IF(u.UserID IS NOT NULL,$modifier_name_field,CONCAT('<span class=\"red\">*</span>',$archived_modifier_name_field)) as ModifiedBy,
						CONCAT('<input type=\"checkbox\" name=\"DivisionID[]\" value=\"',v.DivisionID,'\" />') as Checkbox  
					FROM DHL_DIVISION as v 
					INNER JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=v.DivisionID 
					INNER JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID 
					LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifiedBy 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=c.ModifiedBy 
                    $joinTable
					WHERE 1 $cond 
					GROUP BY v.DivisionID ";
					
			// TABLE COLUMN
			$pos = 0;
			$column_list = array();
			$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['DivisionName'])."</th>\n";
			$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Code'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['RelatedCompany'])."</th>\n";
			$column_list[] = "<th width='30%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Description'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
			$column_list[] = "<th width='1'>".$libdbtable->check("DivisionID[]")."</th>\n";
			
			$column_array = array(0,22,22,18,22,0);
			$wrap_array = array_fill(0,count($field_array),0);
			
			return array($field_array,$sql,$column_list,$column_array,$wrap_array);
		}
		
		$sql = "SELECT 
					v.*,
					c.CompanyID,
					c.CompanyNameChi,
					c.CompanyNameEng,
					c.CompanyCode 
				FROM DHL_DIVISION as v 
				INNER JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=v.DivisionID 
				INNER JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID 
				WHERE 1 $cond ";
			
		if(isset($filterMap['OrderByCompanyFirst']) && $filterMap['OrderByCompanyFirst']){
			$sql .= " ORDER BY ".$company_name_field.",".$division_name_field;
		}else{
			$sql.=" ORDER BY ".$division_name_field;
		}

		$records = $this->returnResultSet($sql);

		return $records;
	}
	
	function upsertDivisionRecord($map)
	{
		$fields = array('DivisionNameChi','DivisionNameEng','DivisionCode','Description');
		$user_id = $_SESSION['UserID'];
		if(count($map) == 0) return false;
		
		$success = true;
		if(isset($map['DivisionID']) && trim($map['DivisionID'])!='' && $map['DivisionID'] > 0)
		{
			$record_id = intval($map['DivisionID']);
			$sql = "UPDATE DHL_DIVISION SET ";
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				
				$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($this->cleanData($val))."'";
				$sep = ",";
			}
			$sql.= " ,DateModified=NOW(),ModifiedBy='".$user_id."' WHERE DivisionID='$record_id'";
			
			$success = $this->db_db_query($sql);
			
			return $success;
		}else{
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				$keys .= $sep."$key";
				$values .= $sep."'".$this->Get_Safe_Sql_Query($this->cleanData($val))."'";
				$sep = ",";
			}
			$keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
			$values .= ",NOW(),NOW(),'$user_id','$user_id'";
			
			$sql = "INSERT INTO DHL_DIVISION ($keys) VALUES ($values)";
			
			$success = $this->db_db_query($sql);
			
			if($success){
				$record_id = $this->db_insert_id();
			}
		}
		
		return $record_id;
	}
	
	function deleteDivisionRecord($recordIdAry)
	{
		if(count($recordIdAry) == 0) return false;
		
		$recordIdAry = IntegerSafe($recordIdAry);
		
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/liblog.php');
		$liblog = new liblog();
		$sql = "SELECT * FROM DHL_DIVISION WHERE DivisionID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$dataAry = $this->returnResultSet($sql);
		$success = $liblog->INSERT_LOG('Organization', 'DeleteDivision', serialize($dataAry), 'DHL_DIVISION');
		
		$sql = "SELECT DISTINCT DepartmentID FROM DHL_DEPARTMENT WHERE DivisionID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$departmentIdAry = $this->returnVector($sql);
		$delete_department_success = count($departmentIdAry)>0? $this->deleteDepartmentRecord($departmentIdAry) : true;
		
		$delete_company_relation = $this->deleteDivisionCompanyRecord($recordIdAry);
		
		$sql = "DELETE FROM DHL_DIVISION WHERE DivisionID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$success = $this->db_db_query($sql);
		
		return $success && $delete_company_relation && $delete_department_success;
	}
	
	function insertDivisionCompanyRecord($divisionId,$companyIdAry)
	{
		if( !($divisionId > 0) || count($companyIdAry)==0) return false;
		
		$sql = "INSERT INTO DHL_COMPANY_DIVISION (CompanyID,DivisionID) VALUES ";
		$sep = "";
		$chunks = array_chunk($companyIdAry,300);
		$resultAry = array();
		foreach($chunks as $chunk)
		{
			$insert_sql = $sql;
			foreach($chunk as $cid){
				$insert_sql .= $sep."('$cid','$divisionId')";
				$sep = ",";
			}
			$resultAry[] = $this->db_db_query($insert_sql);
		}
		
		return !in_array(false,$resultAry);
	}
	
	function deleteDivisionCompanyRecord($divisionId,$companyIdAry=array())
	{
		if(!( (is_array($divisionId) && count($divisionId)>0) || (!is_array($divisionId) && $divisionId > 0))) return false;
		
		$divisionId = IntegerSafe($divisionId);
		$sql = "DELETE FROM DHL_COMPANY_DIVISION WHERE DivisionID ".(is_array($divisionId)?" IN (".implode(",",$divisionId).") ":"='$divisionId'")." ";
		if(count($companyIdAry)>0){
			$companyIdAry = IntegerSafe($companyIdAry);
			$sql .= " AND CompanyID IN (".implode(",",$companyIdAry).")";
		}
		
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function getDivisionCompanyRecords($filterMap)
	{
		global $Lang, $intranet_session_language;
		
		$company_name_field = Get_Lang_Selection("c.CompanyNameChi","c.CompanyNameEng");
		
		$cond = "";
		if(isset($filterMap['DivisionID'])){
			$recordId = IntegerSafe($filterMap['DivisionID']);
			if(is_array($recordId)){
				$cond .= " AND d.DivisionID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND d.DivisionID='$recordId' ";
			}
		}
		if(isset($filterMap['CompanyID'])){
			$recordId = IntegerSafe($filterMap['CompanyID']);
			if(is_array($recordId)){
				$cond .= " AND c.CompanyID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND c.CompanyID='$recordId' ";
			}
		}
		
		$sql = "SELECT 
					c.CompanyID,
					$company_name_field as CompanyName,
					c.CompanyNameChi,
					c.CompanyNameEng,
					c.CompanyCode,
					d.DivisionID,
					d.DivisionNameChi,
					d.DivisionNameEng,
					d.DivisionCode  
				FROM DHL_DIVISION as d 
				INNER JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=d.DivisionID 
				INNER JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID 
				WHERE 1 $cond 
				ORDER BY CompanyName ";
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	function getDepartmentRecords($filterMap=array())
	{
		global $Lang, $intranet_session_language;
		$filters = array('DepartmentID','DivisionID','CompanyID','Keyword','CheckDepartment');
		$department_name_field = Get_Lang_Selection("d.DepartmentNameChi","d.DepartmentNameEng");
		$company_name_field = Get_Lang_Selection("c.CompanyNameChi","c.CompanyNameEng");
		$division_name_field = Get_Lang_Selection("v.DivisionNameChi","v.DivisionNameEng");
		$cond = "";
		if(isset($filterMap['DepartmentID'])){
			$recordId = IntegerSafe($filterMap['DepartmentID']);
			if(is_array($recordId)){
				$cond .= " AND d.DepartmentID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND d.DepartmentID='$recordId' ";
			}
		}
		if(isset($filterMap['DivisionID'])){
			$recordId = IntegerSafe($filterMap['DivisionID']);
			if(is_array($recordId)){
				$cond .= " AND v.DivisionID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND v.DivisionID='$recordId' ";
			}
		}
		if(isset($filterMap['CompanyID'])){
			$recordId = IntegerSafe($filterMap['CompanyID']);
			if(is_array($recordId)){
				$cond .= " AND c.CompanyID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND c.CompanyID='$recordId' ";
			}
		}
		if(isset($filterMap['CheckDepartment']) && $filterMap['CheckDepartment'])
		{
			if(isset($filterMap['ExcludeDepartmentID'])){
				$cond .= " AND d.DepartmentID <> '".IntegerSafe($filterMap['ExcludeDepartmentID'])."' ";
			}
			if(isset($filterMap['CheckDepartmentNameChi'])){
				$cond .= " AND d.DepartmentNameChi='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckDepartmentNameChi']))."' ";
			}
			if(isset($filterMap['CheckDepartmentNameEng'])){
				$cond .= " AND d.DepartmentNameEng='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckDepartmentNameEng']))."' ";
			}
			if(isset($filterMap['CheckDepartmentCode'])){
				$cond .= " AND d.DepartmentCode='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['CheckDepartmentCode']))."' ";
			}
		}
		
		if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
			$keyword = $this->Get_Safe_Sql_Like_Query($this->cleanData($filterMap['Keyword']));
			$cond .= " AND ($name_field LIKE '%$keyword%' OR d.DepartmentCode LIKE '%$keyword%') ";
		}
		
		if($filterMap['GetDBQuery']){
			$libdbtable = $filterMap['libdbtable'];
			$picView = $filterMap['picView'];	
			if($picView){
			    $PICID = $_SESSION['UserID'];
			    $cond .=" AND dpi.UserID = '$PICID'";			    		 
		        $joinTable = "INNER JOIN DHL_PIC_INFO AS dpi ON dpi.ItemID = d.DepartmentID AND  dpi.ItemType = 'Department'";		        					        		
			}
			$modifier_name_field = getNameFieldByLang2("u.");
			$archived_modifier_name_field = getNameFieldByLang2("au.");
			$field_array = array("DepartmentName","d.DepartmentCode","CompanyName","DivisionName","d.Description","UserCount","d.DateModified","ModifiedBy");
			$sql = "SELECT 
						CONCAT('<span style=\"display:none;\">',$department_name_field,'</span><a href=\"edit.php?DepartmentID=',d.DepartmentID,'\">',$department_name_field,'</a>') as DepartmentName,
						d.DepartmentCode,
						GROUP_CONCAT(DISTINCT $company_name_field SEPARATOR ', ') as CompanyName,
						GROUP_CONCAT(DISTINCT $division_name_field SEPARATOR ', ') as DivisionName,
						d.Description,
						CONCAT(COUNT(DISTINCT du.UserID),'<a href=\"javascript:void(0);\" onclick=\"showStaffList(this,',d.DepartmentID,');\" title=\"".$Lang['DHL']['ViewStaffList']."\"><img src=\"/images/2009a/icon_view.gif\" border=\"0\" valign=\"middle\"></a>') as UserCount,
						d.DateModified,
						IF(u.UserID IS NOT NULL,$modifier_name_field,CONCAT('<span class=\"red\">*</span>',$archived_modifier_name_field)) as ModifiedBy,
						CONCAT('<input type=\"checkbox\" name=\"DepartmentID[]\" value=\"',d.DepartmentID,'\" />') as Checkbox  
					FROM DHL_DEPARTMENT as d 
					INNER JOIN DHL_DIVISION as v ON v.DivisionID=d.DivisionID 
					INNER JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=v.DivisionID 
					INNER JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID AND d.CompanyID=c.CompanyID 
					LEFT JOIN DHL_DEPARTMENT_USER as du ON du.DepartmentID=d.DepartmentID 
					LEFT JOIN INTRANET_USER as u ON u.UserID=d.ModifiedBy 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=d.ModifiedBy 
                    $joinTable
					WHERE 1 $cond 
					GROUP BY d.DepartmentID ";

			// TABLE COLUMN
			$pos = 0;
			$column_list = array();
			$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
			$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['DepartmentName'])."</th>\n";
			$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Code'])."</th>\n";
			$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['RelatedCompany'])."</th>\n";
			$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['RelatedDivision'])."</th>\n";
			$column_list[] = "<th width='25%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Description'])."</th>\n";
			$column_list[] = "<th width='5%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['NoOfStaff'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
			$column_list[] = "<th width='1'>".$libdbtable->check("DepartmentID[]")."</th>\n";
			
			$column_array = array(0,22,22,22,18,0,22,0);
			$wrap_array = array_fill(0,count($field_array),0);
			
			return array($field_array,$sql,$column_list,$column_array,$wrap_array);
		}
		
		$sql = "SELECT 
					d.*,";
		if(isset($filterMap['OrderByDivisionFirst']) && $filterMap['OrderByDivisionFirst'] && !$filterMap['DoNotGroup']){
			$sql .=	" GROUP_CONCAT(c.CompanyID) as CompanyID,
					GROUP_CONCAT(c.CompanyNameChi) as CompanyNameChi,
					GROUP_CONCAT(c.CompanyNameEng) as CompanyNameEng,
					GROUP_CONCAT(c.CompanyCode) as CompanyCode,";
		}else{
			$sql .=	" c.CompanyID,
					c.CompanyNameChi,
					c.CompanyNameEng,
					c.CompanyCode,";
		}
		$sql .= " $division_name_field as DivisionName,
					v.DivisionNameChi,
					v.DivisionNameEng,
					v.DivisionCode 
				FROM DHL_DEPARTMENT as d 
				INNER JOIN DHL_DIVISION as v ON v.DivisionID=d.DivisionID 
				INNER JOIN DHL_COMPANY_DIVISION as cd On cd.DivisionID=v.DivisionID 
				INNER JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID AND d.CompanyID=c.CompanyID 
				WHERE 1 $cond ";
		
		if(isset($filterMap['OrderByDivisionFirst']) && $filterMap['OrderByDivisionFirst']){
			if($filterMap['DoNotGroup'] != 1) $sql.= " GROUP BY d.DepartmentID ";
			$sql.= " ORDER BY ".$company_name_field.",".$division_name_field.",".$department_name_field;
		}else{
			$sql.=" ORDER BY ".$department_name_field;
		}
		$records = $this->returnResultSet($sql);
		
		return $records;
	}
	
	function upsertDepartmentRecord($map)
	{
		$fields = array('CompanyID','DivisionID','DepartmentNameChi','DepartmentNameEng','DepartmentCode','Description');
		$user_id = $_SESSION['UserID'];
		if(count($map) == 0) return false;
		
		// Division selection value is composed by [CompanyID,DivisionID]
		if(isset($map['DivisionID']) && strpos($map['DivisionID'],',')!==false )
		{
			$id_ary =  explode(',',$map['DivisionID']);
			$map['CompanyID'] = $id_ary[0];
			$map['DivisionID'] = $id_ary[1];
		}
		
		$success = true;
		if(isset($map['DepartmentID']) && trim($map['DepartmentID'])!='' && $map['DepartmentID'] > 0)
		{
			$record_id = intval($map['DepartmentID']);
			$old_record = $this->getDepartmentRecords(array('DepartmentID'=>$map['DepartmentID']));
			
			$sql = "UPDATE DHL_DEPARTMENT SET ";
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				
				$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($this->cleanData($val))."'";
				$sep = ",";
			}
			$sql.= " ,DateModified=NOW(),ModifiedBy='".$user_id."' WHERE DepartmentID='$record_id'";
			
			$success = $this->db_db_query($sql);
			
			if($success)
			{
				// sync department to intrante group
				$sql = "UPDATE INTRANET_GROUP SET Title='".$this->Get_Safe_Sql_Query($this->cleanData($map['DepartmentNameEng']))."',TitleChinese='".$this->Get_Safe_Sql_Query($this->cleanData($map['DepartmentNameChi']))."',RecordType=1,AcademicYearID=1,DateModified=NOW() WHERE Title='".$old_record[0]['DepartmentNameEng']."'";
				$this->db_db_query($sql);
				
				$affected_row = $this->db_affected_rows();
				if($affected_row == 0){
					$sql = "INSERT INTO INTRANET_GROUP (Title,TitleChinese,RecordType,AcademicYearID,DateInput,DateModified) VALUES ('".$this->Get_Safe_Sql_Query($this->cleanData($map['DepartmentNameEng']))."','".$this->Get_Safe_Sql_Query($this->cleanData($map['DepartmentNameChi']))."',1,1,NOW(),NOW())";
					$this->db_db_query($sql);
				}
			}
			return $success;
		}else{
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				$keys .= $sep."$key";
				$values .= $sep."'".$this->Get_Safe_Sql_Query($this->cleanData($val))."'";
				$sep = ",";
			}
			$keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
			$values .= ",NOW(),NOW(),'$user_id','$user_id'";
			
			$sql = "INSERT INTO DHL_DEPARTMENT ($keys) VALUES ($values)";
			
			$success = $this->db_db_query($sql);
			
			if($success){
				$record_id = $this->db_insert_id();
			}
			
			$sql = "INSERT INTO INTRANET_GROUP (Title,TitleChinese,RecordType,AcademicYearID,DateInput,DateModified) VALUES ('".$this->Get_Safe_Sql_Query($this->cleanData($map['DepartmentNameEng']))."','".$this->Get_Safe_Sql_Query($this->cleanData($map['DepartmentNameChi']))."',1,1,NOW(),NOW())";
			$this->db_db_query($sql);
		}
		
		return $record_id;
	}
	
	function deleteDepartmentRecord($recordIdAry)
	{
		if(count($recordIdAry) == 0) return false;
		
		$recordIdAry = IntegerSafe($recordIdAry);
		
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/liblog.php');
		$liblog = new liblog();
		$sql = "SELECT * FROM DHL_DEPARTMENT WHERE DepartmentID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$dataAry = $this->returnResultSet($sql);
		$success = $liblog->INSERT_LOG('Organization', 'DeleteDepartment', serialize($dataAry), 'DHL_DEPARTMENT');
		
		$old_records = $this->getDepartmentRecords(array('DepartmentID'=>$recordIdAry));
		
		$delete_user_success = $this->deleteDepartmentUserRecord($recordIdAry);
		
		$sql = "DELETE FROM DHL_DEPARTMENT WHERE DepartmentID ".(is_array($recordIdAry)?" IN (".implode(",",$recordIdAry).") ":"='".$recordIdAry."'");
		$success = $this->db_db_query($sql);
		
		// delete intrante group by department names
		$department_names = Get_Array_By_Key($old_records,'DepartmentNameEng');
		if(count($department_names)>0)
		{
			$sql = "SELECT 
						g.GroupID 
					FROM INTRANET_GROUP as g 
					WHERE g.Title IN ('".implode("','",$department_names)."')";
			$groupIdAry = $this->returnVector($sql);
			$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID IN (".implode(",",$groupIdAry).")";
			$this->db_db_query($sql);
			$sql = "DELETE FROM INTRANET_GROUP WHERE GroupID IN (".implode(",",$groupIdAry).")";
			$this->db_db_query($sql);
		}
		return $success;
	}
	
	function insertDepartmentUserRecord($departmentId,$userIdAry)
	{
		if( !($departmentId > 0) || count($userIdAry)==0) return false;
		
		$sql = "INSERT INTO DHL_DEPARTMENT_USER (DepartmentID,UserID) VALUES ";
		$sep = "";
		$chunks = array_chunk($userIdAry,300);
		
		$resultAry = array();
		foreach($chunks as $chunk)
		{
			$insert_sql = $sql;
			foreach($chunk as $uid){
				$insert_sql .= $sep."('$departmentId','$uid')";
				$sep = ",";
			}
		
			$resultAry[] = $this->db_db_query($insert_sql);
		}
		
		$records = $this->getDepartmentRecords(array('DepartmentID'=>$departmentId));
		for($i=0;$i<count($records);$i++)
		{
			$groupIdAry = $this->returnVector("SELECT GroupID FROM INTRANET_GROUP WHERE Title='".$this->Get_Safe_Sql_Query($records[$i]['DepartmentNameEng'])."'");
			if(count($groupIdAry)>0)
			{
				$group_id = $groupIdAry[0];
				
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID,UserID) VALUES ";
				$sep = "";
				foreach($chunks as $chunk)
				{
					$insert_sql = $sql;
					foreach($chunk as $uid){
						$insert_sql .= $sep."('$group_id','$uid')";
						$sep = ",";
					}
					$this->db_db_query($insert_sql);
				}
			}
		}
		
		return !in_array(false,$resultAry);
	}
	
	function deleteDepartmentUserRecord($departmentId=array(),$user_id=array())
	{
		$is_departmentid_empty = !( (is_array($departmentId) && count($departmentId)>0) || (!is_array($departmentId) && $departmentId !='' && $departmentId > 0));
		$is_userid_empty = !( (is_array($user_id) && count($user_id)>0) || (!is_array($user_id) && $user_id !='' && $user_id > 0 ) );
		if($is_departmentid_empty && $is_userid_empty) return false;

		/*
		if(!$is_departmentid_empty){
			$departments = $this->getDepartmentRecords(array('DepartmentID'=>$departmentId));
			$department_names = Get_Array_By_Key($departments,'DepartmentNameEng');
			$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE Title IN ('".implode("','",$department_names)."')";
			$groupIdAry = $this->returnVector($sql);
			$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID IN (".implode(",",$groupIdAry).") ";
			if(!$is_userid_empty){
				$user_id = IntegerSafe($user_id);
				$sql.= " AND UserID ".(is_array($user_id)?" IN (".implode(",",$user_id).") " : " ='$user_id' ");
			}
			$this->db_db_query($sql);
		}
		*/

		$cond = '';
        if(!$is_departmentid_empty){
            $departments = $this->getDepartmentRecords(array('DepartmentID'=>$departmentId));
            $department_names = Get_Array_By_Key($departments,'DepartmentNameEng');

            $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE Title IN ('".implode("','",$department_names)."')";
            $groupIdAry = $this->returnVector($sql);
            $cond .= " GroupID IN (".implode(",",$groupIdAry).") ";
        }
        // [2019-0729-1711-21226]
        if(!$is_userid_empty){
            $user_id = IntegerSafe($user_id);
            $cond .= $cond? ' AND ' : '';
            $cond .= " UserID ".(is_array($user_id)?" IN (".implode(",",$user_id).") " : " = '$user_id' ");
        }
        if($cond != '') {
            $sql = "DELETE FROM INTRANET_USERGROUP WHERE $cond ";
            $this->db_db_query($sql);
        }

		$sql = "DELETE FROM DHL_DEPARTMENT_USER WHERE ";
		if(!$is_departmentid_empty){
			$departmentId = IntegerSafe($departmentId);
			$sql.=" DepartmentID ".(is_array($departmentId)?" IN (".implode(",",$departmentId).") ":" ='$departmentId' ")." ";
		}
		if(!$is_userid_empty){
			$user_id = IntegerSafe($user_id);
			$sql.= (!$is_departmentid_empty?" AND ":"")." UserID ".(is_array($user_id)?" IN (".implode(",",$user_id).") " : " ='$user_id' ");
		}
		
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function getDepartmentUserRecords($filterMap)
	{
		global $Lang, $intranet_session_language;
		
		$name_field = getNameFieldByLang2("u.");
		$department_name_field = Get_Lang_Selection("DepartmentNameChi","DepartmentNameEng");
		
		$cond = "";
		if(isset($filterMap['DepartmentID'])){
			$recordId = IntegerSafe($filterMap['DepartmentID']);
			if(is_array($recordId)){
				$cond .= " AND d.DepartmentID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND d.DepartmentID='$recordId' ";
			}
		}
		if(isset($filterMap['DepartmentUserID'])){
			$recordId = IntegerSafe($filterMap['DepartmentUserID']);
			if(is_array($recordId)){
				$cond .= " AND du.UserID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND du.UserID='$recordId' ";
			}
		}
		if(isset($filterMap['UserStatus'])){
			$user_status = $filterMap['UserStatus'];
			if(is_array($user_status)){
				$cond .= " AND u.RecordStatus IN ('".implode("','",$user_status)."') ";
			}else{
				$cond .= " AND u.RecordStatus='$user_status' ";
			}
		}
		
		$sort_field = " UserName ";
		if(isset($filterMap['SortByDepartmentFirst']) && $filterMap['SortByDepartmentFirst']){
			$sort_field = " $department_name_field,UserName ";
		}
		
		$sql = "SELECT 
					u.UserID,
					$name_field as UserName,
					d.DepartmentID,
					d.DepartmentNameChi,
					d.DepartmentNameEng,
					d.DepartmentCode,
                    u.UserLogin
				FROM DHL_DEPARTMENT as d 
				INNER JOIN DHL_DEPARTMENT_USER as du ON du.DepartmentID=d.DepartmentID 
				INNER JOIN INTRANET_USER as u ON u.UserID=du.UserID 
				WHERE 1 $cond 
				ORDER BY ".$sort_field;
		$records = $this->returnResultSet($sql);

		return $records;
	}
	function getPICRecords($itemType,$itemID){
	    
	    if($itemType){
	        $cond = " AND ItemType = '$itemType' ";
	    }  
// 	    if($itemID){
	        $cond .= " AND ItemID  = '$itemID' ";
// 	    }  
	    $name_field = getNameFieldByLang2("u.");
	       
	    $sql = "SELECT
        	       dpi.UserID,
                   $name_field as UserName,
                   UserLogin 				
        	    FROM DHL_PIC_INFO AS dpi
                INNER JOIN INTRANET_USER as u ON u.UserID = dpi.UserID    
        	    WHERE 1 $cond ";	    
	    
	    $records = $this->returnResultSet($sql);
	
	    return $records;
	    
	}
	
	function insertPICRecord($itemType,$itemId,$picIDAry)
	{
// 	    if( !($departmentId > 0) || count($picIDAry)==0) return false;
	    
	    $sql = "INSERT INTO DHL_PIC_INFO (ItemType,ItemID,UserID,DateInput,DateModified,InputBy,ModifiedBy) VALUES ";
	    $sep = "";
	    $chunks = array_chunk($picIDAry,300);
	    
	    $insertby = $_SESSION['UserID'];
	    $resultAry = array();
	    foreach($chunks as $chunk)
	    {
	        $insert_sql = $sql;
	        foreach($chunk as $uid){
	            $insert_sql .= $sep."('$itemType','$itemId','$uid',now(),now(),'$insertby','$insertby')";
	            $sep = ",";
	        }
	        
	        $resultAry[] = $this->db_db_query($insert_sql);
	    }

  
	    return !in_array(false,$resultAry);
	}
	
	function deletePICRecord($itemType,$itemId){
	    
	    $sql = "DELETE FROM DHL_PIC_INFO 
             WHERE ItemType = '$itemType'
             AND ItemID = '$itemId'";
	    
	    $success = $this->db_db_query($sql);
	    return $success;
    
	}
	function getPICSelection($itemType,$itemId,$id,$name, $selectedValue, $isMultiple=false, $hasFirst=false, $firstText='', $tags='', $valuePrefix='', $hasOptgroupLabel=false)
	{
	    global $Lang, $intranet_session_language;
	    
	    $records = $this->getPICRecords($itemType,$itemId);
	    
	    $x = '<select id="'.$id.'" name="'.$name.'" '.($isMultiple?'multiple="multiple"':'').' '.$tags.'>'."\n";
	    if($hasFirst){
	        if(is_array($selectedValue)){
	            $selected = count($selectedValue)==0;
	        }else{
	            $selected = $selectedValue == '';
	        }
	        $x .= '<option value=""'.($selected?' selected="selected"':'').'>---- '.($firstText!=''?$firstText:$Lang['Btn']['Select']).' ----</option>'."\n";
	    }
	    for($i=0;$i<count($records);$i++)
	    {
	        if($hasOptgroupLabel && $records[$i]['DepartmentID'] != $records[$i-1]['DepartmentID']){
	            $x .= '<optgroup label="'.$records[$i][$department_name_field].'">'."\n";
	        }
	        
	        $val = $records[$i]['UserID'];
	        $text = $records[$i]['UserName'].' ('.$records[$i]['UserLogin'].')';
	        if(is_array($selectedValue)){
	            $selected = in_array($val, $selectedValue);
	        }else{
	            $selected = $selectedValue == $val;
	        }
	        $x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').' >'.intranet_htmlspecialchars($text).'</option>';
	        
	        if($hasOptgroupLabel && $records[$i]['DepartmentID'] != $records[$i+1]['DepartmentID']){
	            $x .= '</optgroup>'."\n";
	        }
	    }
	    $x .= '</select>'."\n";
	    
	    return $x;
	}
	function isPIC($itemType='',$itemId=''){
	    $checkUserID = $_SESSION['UserID'];
	    
	    if($itemId){
	        $conds .=" AND ItemID = '$itemId' ";
	    }
	    
	    if($itemType){
	        $conds .=" AND ItemType = '$itemType' ";
	    }
	   
	    $sql = "SELECT PICID FROM DHL_PIC_INFO  
                WHERE 1
                AND UserID = '$checkUserID' 
                $conds
               ";
	    $isPIC = $this->returnVector($sql);
	
	    if(empty($isPIC)){
	        return false;
	    }else{
	        return true;
	    }
	    
	}
	
	function getPICResponsInfo(){
	    
	    $checkUserID = $_SESSION['UserID'];
	    $DepartmenrIDAry = array();
	    $sql = "SELECT 
                    dpi.ItemID as DepartmentID,
                    dd.DivisionID,
                    dd.CompanyID
                FROM 
                    DHL_PIC_INFO as dpi
                    INNER JOIN DHL_DEPARTMENT AS dd ON dd.DepartmentID = dpi.ItemID
                WHERE 
                    ItemType  = 'Department' 
                    AND UserID = '$checkUserID'";
	    $DepartmentInfoAry = $this->returnResultSet($sql);
	    
	    $DepartmenrIDAry = Get_Array_By_Key($DepartmentInfoAry,'DepartmentID');
	    $DepartmenrDivisionIDAry = Get_Array_By_Key($DepartmentInfoAry,'DivisionID');
	    $DepartmenrCompanyIDAry =Get_Array_By_Key($DepartmentInfoAry,'CompanyID');
	    
// 	    debug_pr($CompanyIDAry);
	    $sql = "SELECT
        	       dpi.ItemID as DivisionID,
                   dcd.CompanyID,
                   dd.DepartmentID
        	    FROM
        	       DHL_PIC_INFO AS dpi
                   INNER JOIN DHL_COMPANY_DIVISION  AS dcd ON ( dpi.ItemID = dcd.DivisionID)
                   LEFT JOIN DHL_DEPARTMENT AS dd ON (dd.DivisionID = dcd.DivisionID)
        	    WHERE
        	       dpi.ItemType  = 'Division'
        	       AND dpi.UserID = '$checkUserID'";
	    $DivisionInfoAry = $this->returnResultSet($sql);
	    
	    $DivisionDepartmenrIDAry= array_unique(array_merge($DepartmenrIDAry, Get_Array_By_Key($DivisionInfoAry,'DepartmentID')));	    
	    $DivisionIDAry = array_unique(array_merge($DepartmenrDivisionIDAry, Get_Array_By_Key($DivisionInfoAry,'DivisionID')));	    
	    $DivisionCompanyIDAry = array_merge($DepartmenrCompanyIDAry,Get_Array_By_Key($DivisionInfoAry,'CompanyID'));
	   
	    
	    $sql = "SELECT
            	    dpi.ItemID as CompanyID,
                    dcd.DivisionID,
                    dd.DepartmentID
            	FROM
            	    DHL_PIC_INFO AS dpi
                    LEFT JOIN DHL_COMPANY_DIVISION AS dcd ON ( dpi.ItemID = dcd.CompanyID)
                    LEFT JOIN DHL_DEPARTMENT  AS dd ON (dcd.DivisionID = dd.DivisionID)
            	WHERE
            	    dpi.ItemType  = 'Company'
            	    AND dpi.UserID = '$checkUserID'";
	    $CompanyIDInfoAry = $this->returnResultSet($sql);
	    
	    $CompanyDepartmenrIDAry = array_unique(array_merge($DivisionDepartmenrIDAry, Get_Array_By_Key($CompanyIDInfoAry,'DepartmentID')));
	    $CompanyDivisionIDAry = array_unique(array_merge($DivisionIDAry, Get_Array_By_Key($CompanyIDInfoAry,'DivisionID')));	    
	    $CompanyIDAry = array_unique(array_merge($DivisionCompanyIDAry,Get_Array_By_Key($CompanyIDInfoAry,'CompanyID')));
	    
	  
	    
	    $returnAry['Department'] = $CompanyDepartmenrIDAry;
	    $returnAry['Division'] = $CompanyDivisionIDAry;
	    $returnAry['Company'] = $CompanyIDAry;

	    return $returnAry;   
	}
	
	
	
	function getCompanySelection($id, $name, $selectedValue, $isMultiple=false, $hasFirst=false, $firstText='', $tags='')
	{
		global $Lang, $intranet_session_language;
		
		$name_field = Get_Lang_Selection("CompanyNameChi","CompanyNameEng");
		$records = $this->getCompanyRecords();
		
		$x = '<select id="'.$id.'" name="'.$name.'" '.($isMultiple?'multiple="multiple"':'').' '.$tags.'>'."\n";
		if($hasFirst){
			if(is_array($selectedValue)){
				$selected = count($selectedValue)==0;
			}else{
				$selected = $selectedValue == '';
			}
			$x .= '<option value=""'.($selected?' selected="selected"':'').'>---- '.($firstText!=''?$firstText:$Lang['Btn']['Select']).' ----</option>'."\n";
		}
		for($i=0;$i<count($records);$i++)
		{
			$val = $records[$i]['CompanyID'];
			$text = $records[$i][$name_field];
			if(is_array($selectedValue)){
				$selected = in_array($val, $selectedValue);
			}else{
				$selected = $selectedValue == $val;
			}
			$x .= '<option value="'.$val.'"'.($selected?' selected="selected"':'').' '.($records[$i]['Description']!=''?' title="'.intranet_htmlspecialchars($records[$i]['Description']).'" ':'').'>'.intranet_htmlspecialchars($text).'</option>';
		}
		$x .= '</select>'."\n";
		
		return $x;
	}
	
	function getDivisionSelection($id, $name, $selectedValue, $isMultiple=false, $hasFirst=false, $firstText='', $tags='', $noOptgroupLabel=false, $companyIdAry='', $valuePrefix='',$filterPIC = '')
	{
		global $Lang, $intranet_session_language;
		
		$name_field = Get_Lang_Selection("DivisionNameChi","DivisionNameEng");
		$company_name_field = Get_Lang_Selection("CompanyNameChi","CompanyNameEng");
		$params = array('OrderByCompanyFirst'=>1);
		if($companyIdAry != '' || (is_array($companyIdAry) && count($companyIdAry)>0)){	
			$params['CompanyID'] = $companyIdAry;
		}
		$records = $this->getDivisionRecords($params);

		$x = '<select id="'.$id.'" name="'.$name.'" '.($isMultiple?'multiple="multiple"':'').' '.$tags.'>'."\n";
		if($hasFirst){
			if(is_array($selectedValue)){
				$selected = count($selectedValue)==0;
			}else{
				$selected = $selectedValue == '';
			}
			$x .= '<option value=""'.($selected?' selected="selected"':'').'>---- '.($firstText!=''?$firstText:$Lang['Btn']['Select']).' ----</option>'."\n";
		}
		for($i=0;$i<count($records);$i++)
		{
			if(!$noOptgroupLabel && $records[$i]['CompanyID'] != $records[$i-1]['CompanyID']){
				$x .= '<optgroup label="'.$records[$i][$company_name_field].'">'."\n";
			}
			$val = $records[$i]['CompanyID'].','.$records[$i]['DivisionID'];
			$text = $records[$i][$name_field];
			if(is_array($selectedValue)){
				$selected = in_array($val, $selectedValue);
			}else{
				$selected = $selectedValue == $val;
			}
			if($filterPIC){
			    if(in_array($records[$i]['DivisionID'],$selectedValue)){
			        $x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').' '.($records[$i]['Description']!=''?' title="'.intranet_htmlspecialchars($records[$i]['Description']).'" ':'').'>'.intranet_htmlspecialchars($text).'</option>';
			        if(!$noOptgroupLabel && $records[$i]['CompanyID'] != $records[$i+1]['CompanyID']){
			            $x .= '</optgroup>'."\n";
			        }
			    }
			}else{
			    $x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').' '.($records[$i]['Description']!=''?' title="'.intranet_htmlspecialchars($records[$i]['Description']).'" ':'').'>'.intranet_htmlspecialchars($text).'</option>';
			    if(!$noOptgroupLabel && $records[$i]['CompanyID'] != $records[$i+1]['CompanyID']){
			        $x .= '</optgroup>'."\n";
			    }
			}
			
		}
		$x .= '</select>'."\n";		
		return $x;
	}
	
	function getDepartmentSelection($id, $name, $selectedValue, $isMultiple=false, $hasFirst=false, $firstText='', $tags='', $noOptgroupLabel=false, $divisionIdAry='', $valuePrefix='', $fetchIntranetGroup=false, $filterPIC='0')
	{
		global $Lang, $intranet_session_language;
		
		$name_field = Get_Lang_Selection("DepartmentNameChi","DepartmentNameEng");
		$division_name_field = Get_Lang_Selection("DivisionNameChi","DivisionNameEng");
		$company_name_field = Get_Lang_Selection("CompanyNameChi","CompanyNameEng");
		$department_id_field = 'DepartmentID';
		$params = array('OrderByDivisionFirst'=>1);
		if($divisionIdAry != '' || (is_array($divisionIdAry) && count($divisionIdAry)>0)){
			if( strpos($divisionIdAry[0],',')!==false ){
				$company_id_ary = array();
				$division_id_ary  = array();
				for($i=0;$i<count($divisionIdAry);$i++)
				{
					$id_ary = explode(',',$divisionIdAry[$i]);
					$company_id_ary[] = $id_ary[0];
					$division_id_ary[] = $id_ary[1];
				}
				$params['CompanyID'] = $company_id_ary;
				$params['DivisionID'] = $division_id_ary;
			}else{
				$params['DivisionID'] = $divisionIdAry;
			}
		}
		if($fetchIntranetGroup)
		{
			$name_field = Get_Lang_Selection("TitleChinese","Title");
			$department_id_field = 'GroupID';
			$records = $this->getIntranetGroupRecords($params);
		}else{
			$records = $this->getDepartmentRecords($params);
		}
		
		$x = '<select id="'.$id.'" name="'.$name.'" '.($isMultiple?'multiple="multiple"':'').' '.$tags.'>'."\n";
		if($hasFirst){
			if(is_array($selectedValue)){
				$selected = count($selectedValue)==0;
			}else{
				$selected = $selectedValue == '';
			}
			$x .= '<option value=""'.($selected?' selected="selected"':'').'>---- '.($firstText!=''?$firstText:$Lang['Btn']['Select']).' ----</option>'."\n";
		}
		for($i=0;$i<count($records);$i++)
		{
			if(!$noOptgroupLabel && $records[$i]['DivisionID'] != $records[$i-1]['DivisionID']){
				$x .= '<optgroup label="'.$records[$i][$company_name_field].' &gt; '.$records[$i][$division_name_field].'">'."\n";
			}
			$val = $records[$i][$department_id_field];
			$text = $records[$i][$name_field];
			if(is_array($selectedValue)){
				$selected = in_array($val, $selectedValue);
			}else{
				$selected = $selectedValue == $val;
			}
			if($filterPIC){
			  
			    if(in_array($records[$i][$department_id_field],$selectedValue)){
			        $x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').' '.($records[$i]['Description']!=''?' title="'.intranet_htmlspecialchars($records[$i]['Description']).'" ':'').'>'.intranet_htmlspecialchars($text).'</option>'."\n";
			        if(!$noOptgroupLabel && $records[$i]['DivisionID'] != $records[$i+1]['DivisionID']){
			            $x .= '</optgroup>'."\n";
			        }
			    }
			}else{
			    $x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').' '.($records[$i]['Description']!=''?' title="'.intranet_htmlspecialchars($records[$i]['Description']).'" ':'').'>'.intranet_htmlspecialchars($text).'</option>'."\n";
			    if(!$noOptgroupLabel && $records[$i]['DivisionID'] != $records[$i+1]['DivisionID']){
			        $x .= '</optgroup>'."\n";
			    }
			}
			
		}
		$x .= '</select>'."\n";
		
		return $x;
	}
	
	// INTRANET_GROUP.RecordType=1 is Admin Category
	function getIntranetGroupRecords($filterMap)
	{
		global $Lang, $intranet_session_language;
		
		$cond = "";
		if(isset($filterMap['GroupID'])){
			if(is_array($filterMap['GroupID'])){
				$cond .= " AND g.GroupID IN (".implode(",",$filterMap['GroupID']).") ";
			}else{
				$cond .= " AND g.GroupID='".$filterMap['GroupID']."' ";
			}
		}
		if(isset($filterMap['ExcludeGroupID'])){
			if(is_array($filterMap['ExcludeGroupID'])){
				$cond .= " AND g.GroupID NOT IN (".implode(",",$filterMap['ExcludeGroupID']).") ";
			}else{
				$cond .= " AND g.GroupID<>'".$filterMap['ExcludeGroupID']."' ";
			}
		}
		
		if(isset($filterMap['DivisionID'])){
			$recordId = IntegerSafe($filterMap['DivisionID']);
			if(is_array($recordId)){
				$cond .= " AND v.DivisionID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND v.DivisionID='$recordId' ";
			}
		}
		if(isset($filterMap['CompanyID'])){
			$recordId = IntegerSafe($filterMap['CompanyID']);
			if(is_array($recordId)){
				$cond .= " AND c.CompanyID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND c.CompanyID='$recordId' ";
			}
		}
		
		$sql = "SELECT g.*,d.CompanyID,c.CompanyNameChi,c.CompanyNameEng,c.CompanyCode,d.DivisionID,v.DivisionNameChi,v.DivisionNameEng,v.DivisionCode ,d.DepartmentID 
				FROM INTRANET_GROUP as g 
				INNER JOIN DHL_DEPARTMENT as d ON d.DepartmentNameEng=g.Title 
				INNER JOIN DHL_DIVISION as v ON v.DivisionID=d.DivisionID 
				INNER JOIN DHL_COMPANY_DIVISION as cd On cd.DivisionID=v.DivisionID 
				INNER JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID AND d.CompanyID=c.CompanyID 
				WHERE g.RecordType=1 AND g.Title IS NOT NULL AND g.Title<>'' $cond 
				ORDER BY c.CompanyNameEng,v.DivisionNameEng,g.Title ";
		$records = $this->returnResultSet($sql);
	
		if(isset($filterMap['GroupIDToDisplayTitle']) && $filterMap['GroupIDToDisplayTitle'])
		{
			$name_field = Get_Lang_Selection("TitleChinese","Title");
			$assocAry = array();
			for($i=0;$i<count($records);$i++)
			{
				$assocAry[$records[$i]['GroupID']] = '('.$records[$i]['CompanyCode'].' > '.$records[$i]['DivisionCode'].') '.$records[$i][$name_field];
			}
			return $assocAry;
		}
		
		return $records;
	}
	
	function getIntranetGroupSelection($id, $name, $selectedValue, $isMultiple=false, $hasFirst=false, $firstText='', $tags='', $valuePrefix='', $display_prefix_code_in_group_name=true,$fromSchoolInfo=false, $fromSchoolNews=false)
	{
		global $Lang, $intranet_session_language;
		
		$name_field = Get_Lang_Selection("TitleChinese","Title");
		$division_name_field = Get_Lang_Selection("DivisionNameChi","DivisionNameEng");
		$company_name_field = Get_Lang_Selection("CompanyNameChi","CompanyNameEng");
		//$sql = "SELECT GroupID,$name_field as Title FROM INTRANET_GROUP WHERE RecordType IS NULL AND Title IS NOT NULL AND Title<>''";
		//$records = $this->returnResultSet($sql);
		$params = array();
		if(is_array($selectedValue) && count($selectedValue)>0){
			$params['GroupID'] = $selectedValue;
		}
		if($fromSchoolInfo){		   
		    $params['GroupID'] = $this->getMappingIntranetGroupID($selectedValue);		    
		}
		
// 		if($fromSchoolNews){
// 		$selectedList = implode(',',$selectedValue);
// 		$params['GroupID'] = $this->getMappingDepartmentGroupID($selectedList);		
// 		}
		$records = $this->getIntranetGroupRecords($params);
	
		$x = '<select id="'.$id.'" name="'.$name.'" '.($isMultiple?'multiple="multiple"':'').' '.$tags.'>'."\n";
		if($hasFirst){
			if(is_array($selectedValue)){
				$selected = count($selectedValue)==0;
			}else{
				$selected = $selectedValue == '';
			}
			$x .= '<option value=""'.($selected?' selected="selected"':'').'>---- '.($firstText!=''?$firstText:$Lang['Btn']['Select']).' ----</option>'."\n";
		}
		for($i=0;$i<count($records);$i++)
		{
			$tooltip = $display_prefix_code_in_group_name? '('.$records[$i]['CompanyCode'].' > '.$records[$i]['DivisionCode'].') ' : '';
			$val = $records[$i]['GroupID'];
			if($fromSchoolInfo){
			    $val = $records[$i]['DepartmentID'];
			}
			
			if($fromSchoolNews){
			    $val = $records[$i]['DepartmentID'];
			}
			$text = $records[$i][$name_field];
			if(is_array($selectedValue)){
				$selected = in_array($val, $selectedValue);
			}else{
				$selected = $selectedValue == $val;
			}
			$x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').'>'.intranet_htmlspecialchars($tooltip.$text).'</option>'."\n";
		}
		$x .= '</select>'."\n";
		
		return $x;
	}
	
	function getDepartmentUserSelection($departmentId, $id, $name, $selectedValue, $isMultiple=false, $hasFirst=false, $firstText='', $tags='', $valuePrefix='', $hasOptgroupLabel=false)
	{
		global $Lang, $intranet_session_language;
		
		$department_name_field = Get_Lang_Selection("DepartmentNameChi","DepartmentNameEng");
		$params = array('DepartmentID'=>$departmentId,'UserStatus'=>1);
		if($hasOptgroupLabel){
			$params['SortByDepartmentFirst'] = 1;
		}
	
		
		$records = $this->getDepartmentUserRecords($params);
		
		$x = '<select id="'.$id.'" name="'.$name.'" '.($isMultiple?'multiple="multiple"':'').' '.$tags.'>'."\n";
		if($hasFirst){
			if(is_array($selectedValue)){
				$selected = count($selectedValue)==0;
			}else{
				$selected = $selectedValue == '';
			}
			$x .= '<option value=""'.($selected?' selected="selected"':'').'>---- '.($firstText!=''?$firstText:$Lang['Btn']['Select']).' ----</option>'."\n";
		}
		for($i=0;$i<count($records);$i++)
		{
			if($hasOptgroupLabel && $records[$i]['DepartmentID'] != $records[$i-1]['DepartmentID']){
				$x .= '<optgroup label="'.$records[$i][$department_name_field].'">'."\n";
			}
			
			$val = $records[$i]['UserID'];
			$text = $records[$i]['UserName'].' ('.$records[$i]['UserLogin'].')';
			if(is_array($selectedValue)){
				$selected = in_array($val, $selectedValue);
			}else{
				$selected = $selectedValue == $val;
			}
			$x .= '<option value="'.$valuePrefix.$val.'"'.($selected?' selected="selected"':'').' >'.intranet_htmlspecialchars($text).'</option>';
			
			if($hasOptgroupLabel && $records[$i]['DepartmentID'] != $records[$i+1]['DepartmentID']){
				$x .= '</optgroup>'."\n";
			}
		}
		$x .= '</select>'."\n";
		
		return $x;
	}
	
	function getUserRecords($filterMap)
	{
		global $Lang, $intranet_session_language, $i_UserEnglishName, $i_UserChineseName, $i_UserLogin, $i_frontpage_eclass_lastlogin, $i_UserRecordStatus;
		//$filters = array('User_ID','Keyword','RecordStatus');
		$name_field = Get_Lang_Selection("u.ChineseName","u.EnglishName");
		$cond = "";
		$join = "LEFT";
		if(isset($filterMap['user_id'])){
			$recordId = IntegerSafe($filterMap['user_id']);
			if(is_array($recordId)){
				$cond .= " AND u.UserID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND u.UserID='$recordId' ";
			}
		}
		if(isset($filterMap['RecordStatus']) && $filterMap['RecordStatus']!='' && in_array($filterMap['RecordStatus'],array(1,0))){
			$cond .= " AND u.RecordStatus='".$filterMap['RecordStatus']."' ";
		}
		
		if(isset($filterMap['CheckUserLogin']) && $filterMap['CheckUserLogin'])
		{
			if(isset($filterMap['ExcludeUserID'])){
				$cond .= " AND u.UserID <> '".IntegerSafe($filterMap['ExcludeUserID'])."' ";
			}
			if(isset($filterMap['UserLogin'])){
				$cond .= " AND u.UserLogin='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['UserLogin']))."' ";
			}
		}
		
		if(isset($filterMap['CheckUserEmail']) && $filterMap['CheckUserEmail'])
		{
			if(isset($filterMap['ExcludeUserID'])){
				$cond .= " AND u.UserID <> '".IntegerSafe($filterMap['ExcludeUserID'])."' ";
			}
			if(isset($filterMap['UserEmail'])){
				$cond .= " AND u.UserEmail='".$this->Get_Safe_Sql_Query($this->cleanData($filterMap['UserEmail']))."' ";
			}
		}
		
		if(isset($filterMap['GetUsersUnderCompanyID']) && $filterMap['GetUsersUnderCompanyID']){
			$recordId = IntegerSafe($filterMap['GetUsersUnderCompanyID']);
			if(is_array($recordId)){
				$cond .= " AND c.CompanyID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND c.CompanyID='$recordId' ";
			}
		}
		if(isset($filterMap['GetUsersUnderDivisionID']) && $filterMap['GetUsersUnderDivisionID']){
			$recordId = IntegerSafe($filterMap['GetUsersUnderDivisionID']);
			if(is_array($recordId)){
				$cond .= " AND v.DivisionID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND v.DivisionID='$recordId' ";
			}
		}
		if(isset($filterMap['GetUsersUnderDepartmentID']) && $filterMap['GetUsersUnderDepartmentID']){
			$recordId = IntegerSafe($filterMap['GetUsersUnderDepartmentID']);
			if(is_array($recordId)){
				$cond .= " AND d.DepartmentID IN (".implode(",",$recordId).") ";
			}else{
				$cond .= " AND d.DepartmentID='$recordId' ";
			}
		}
		
		if(isset($filterMap['GetUsersUnderCompanyDivisionIDGroup']) && $filterMap['GetUsersUnderCompanyDivisionIDGroup']){
			$join = "INNER";
			$id_groups = $filterMap['GetUsersUnderCompanyDivisionIDGroup'];
			$cond .= " AND (";
			$or = "";
			for($i=0;$i<count($id_groups);$i++){
				$cond .= $or." (cd.CompanyID='".$id_groups[$i]['CompanyID']."' AND cd.DivisionID='".$id_groups[$i]['DivisionID']."') ";
				$or = " OR ";
			}
			$cond .= " ) ";
		}
		
		if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
			$keyword = $this->Get_Safe_Sql_Like_Query($this->cleanData($filterMap['Keyword']));
			$cond .= " AND (u.EnglishName LIKE '%$keyword%' OR u.ChineseName LIKE '%$keyword%' OR u.UserLogin LIKE '%$keyword%') ";
		}
		
		if(isset($filterMap['ExcludeUserIdAry'])){
		    if(is_array($filterMap['ExcludeUserIdAry']) && !empty($filterMap['ExcludeUserIdAry'])){
		        $cond .= " AND u.UserID NOT IN (".implode(",",$filterMap['ExcludeUserIdAry']).") ";
		    }else{
		        $cond .= " AND u.UserID != '".$filterMap['ExcludeUserIdAry']."' ";
		    }
		}
		
		if($filterMap['GetDBQuery']){
			//$company_name_field = Get_Lang_Selection("c.CompanyNameChi","c.CompanyNameEng");
			//$division_name_field = Get_Lang_Selection("v.DivisionNameChi","v.DivisionNameEng");
			//$department_name_field = Get_Lang_Selection("d.DepartmentNameChi","d.DepartmentNameEng");
			$organization_field = $this->formatDBOriganizationDisplayName("c.","v.","d.");
			$libdbtable = $filterMap['libdbtable'];
			$field_array = array("u.UserLogin","u.EnglishName","DepartmentName","u.JoinDate","u.TerminatedDate","u.EffectiveDate","RecordStatus","u.LastUsed");
			$sql = "SELECT 
						CONCAT('<span style=\"display:none\">',u.UserLogin,'</span><a href=\"javascript:goEdit(',u.UserID,');\" >',u.UserLogin,'</a>') as UserLogin,
						u.EnglishName,
						IF(du.UserID IS NOT NULL,$organization_field,'-') as DepartmentName,
						u.JoinDate,
						u.TerminatedDate,
						u.EffectiveDate,
						CASE u.RecordStatus 
						WHEN '1' THEN '".$Lang['Status']['Active']."' 
						WHEN '0' THEN '".$Lang['Status']['Suspended']."' 
						END as RecordStatus,
						u.LastUsed,
						CONCAT('<input type=\"checkbox\" name=\"user_id[]\" value=\"',u.UserID,'\" />') as Checkbox  
					FROM INTRANET_USER as u 
					LEFT JOIN DHL_DEPARTMENT_USER as du ON du.UserID=u.UserID 
					LEFT JOIN DHL_DEPARTMENT as d ON d.DepartmentID=du.DepartmentID 
					LEFT JOIN DHL_DIVISION as v ON v.DivisionID=d.DivisionID 
					LEFT JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=v.DivisionID 
					LEFT JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID AND c.CompanyID=d.CompanyID 
					WHERE u.RecordType='".USERTYPE_STAFF."' $cond  GROUP BY u.UserID ";
					
			// TABLE COLUMN
			$pos = 0;
			$column_list = array();
			$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $i_UserLogin)."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['StaffName'])."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['Department'])."</th>\n";
			$column_list[] = "<th width='11%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['JoinDate'])."</th>\n";
			$column_list[] = "<th width='11%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['TerminatedDate'])."</th>\n";
			$column_list[] = "<th width='11%'>".$libdbtable->column_IP25($pos++, $Lang['DHL']['EffectiveDate'])."</th>\n";
			$column_list[] = "<th width='6%'>".$libdbtable->column_IP25($pos++, $i_UserRecordStatus)."</th>\n";
			$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
			$column_list[] = "<th width='1'>".$libdbtable->check("user_id[]")."</th>\n";
			
			$column_array = array(0,22,0,22,22,22,22,22);
			$wrap_array = array_fill(0,count($field_array),0);
			
			return array($field_array,$sql,$column_list,$column_array,$wrap_array);
		}
		
		$sql = "SELECT 
					u.* ";
		if($filterMap['GetOrganizationInfo'] || $filterMap['GetUsersUnderCompanyID'] || $filterMap['GetUsersUnderDivisionID'] || $filterMap['GetUsersUnderDepartmentID'] || $filterMap['GetUsersUnderCompanyDivisionIDGroup']){
			$sql.= ",GROUP_CONCAT(c.CompanyID SEPARATOR ',') as CompanyID,GROUP_CONCAT(c.CompanyNameChi SEPARATOR ',') as CompanyNameChi,GROUP_CONCAT(c.CompanyNameEng SEPARATOR ',') as CompanyNameEng,GROUP_CONCAT(c.CompanyCode SEPARATOR ',') as CompanyCode,v.DivisionID,v.DivisionNameChi,v.DivisionNameEng,v.DivisionCode,d.DepartmentID,d.DepartmentNameChi,d.DepartmentNameEng,d.DepartmentCode ";
			if($filterMap['GetOrganizationField']){
				$sql .= ",".$this->formatDBOriganizationDisplayName("c.","v.","d.",$filterMap['GetOrganizationFieldNoTooltip'])." as Organization ";
			}
		}
		$sql.=" FROM INTRANET_USER as u ";
		if($filterMap['GetOrganizationInfo'] || $filterMap['GetUsersUnderCompanyID'] || $filterMap['GetUsersUnderDivisionID'] || $filterMap['GetUsersUnderDepartmentID'] || $filterMap['GetUsersUnderCompanyDivisionIDGroup']){
			$sql .= "$join JOIN DHL_DEPARTMENT_USER as du ON du.UserID=u.UserID 
					 $join JOIN DHL_DEPARTMENT as d ON d.DepartmentID=du.DepartmentID 
					 $join JOIN DHL_DIVISION as v ON v.DivisionID=d.DivisionID 
					 $join JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=v.DivisionID 
					 $join JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID AND c.CompanyID=d.CompanyID 
					";
		}
		$sql.=" WHERE u.RecordType='".USERTYPE_STAFF."' $cond ";
		if($filterMap['GetOrganizationInfo'] || $filterMap['GetUsersUnderCompanyID'] || $filterMap['GetUsersUnderDivisionID'] || $filterMap['GetUsersUnderDepartmentID'] || $filterMap['GetUsersUnderCompanyDivisionIDGroup']){
			$sql.=" GROUP BY u.UserID ";
		}
		$sql.=" ORDER BY ".$name_field;
		
		$records = $this->returnResultSet($sql);
// 		debug_pr($sql);
		return $records;
	}
	
	function upsertUserRecord($map, $import_type='')
	{
		global $intranet_authentication_method, $intranet_password_salt;
		
		$fields = array('UserLogin', 'ChineseName', 'EnglishName', 'UserEmail', 'JoinDate', 'EffectiveDate', 'TerminatedDate', 'RecordStatus', 'MoveToDepartmentID');

		// [2020-0117-1616-21226] define fields for 2 import types - Movement & Leaver
		if($import_type == 'Movement') {
            $fields = array('UserLogin', 'ChineseName', 'EnglishName', 'UserEmail', 'EffectiveDate', 'RecordStatus', 'MoveToDepartmentID');
        }
        else if($import_type == 'Leaver') {
            $fields = array('UserLogin', 'ChineseName', 'EnglishName', 'UserEmail', 'JoinDate', 'TerminatedDate', 'RecordStatus', 'MoveToDepartmentID');
        }

        $user_id = $_SESSION['UserID'];
		if(count($map) == 0) return false;
		
		$success = true;

		if(isset($map['EnglishName']))
		{
			$map['ChineseName'] = $map['EnglishName'];      // copy EnglishName to ChineseName
		}

		if(isset($map['UserEmail']) && $map['UserEmail'] == '' && $this->isDev())
		{
			$map['UserEmail'] = $map['UserLogin'].'@'.$_SERVER['SERVER_NAME'];
		}

		if(isset($map['user_id']) && trim($map['user_id']) != '' && $map['user_id'] > 0)
		{
			$record_id = intval($map['user_id']);

			$sql = "UPDATE INTRANET_USER SET ";
			$sep = "";
			foreach($map as $key => $val)
			{
				if(!in_array($key, $fields) || $key == 'UserLogin') continue;

                // [2020-0117-1616-21226] skip email update if empty for 2 import types - Movement & Leaver
                if(($import_type == 'Movement' || $import_type == 'Leaver') && $key == 'UserEmail' && trim($val) == '') {
                    continue;
                }

				$val = $this->cleanData($val);
				$sql .= $sep."$key=".($val == '' ? "NULL" : "'".$this->Get_Safe_Sql_Query($val)."'");
				$sep = ",";
			}
			$sql.= " , RecordType = '".USERTYPE_STAFF."', Teaching = '1', DateModified = NOW(), ModifyBy = '".$user_id."' WHERE UserID = '$record_id'";
			
			$success = $this->db_db_query($sql);
		}
		else
        {
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val)
			{
				if(!in_array($key, $fields)) continue;

				$keys .= $sep."$key";
				$val = $this->cleanData($val);
				$values .= $sep.($val == '' ? "NULL" : "'".$this->Get_Safe_Sql_Query($val)."'");
				$sep = ",";
			}
			$keys .= " , RecordType, Teaching, DateInput, DateModified, InputBy, ModifyBy ";
			$values .= " ,'".USERTYPE_STAFF."', '1', NOW(), NOW(), '$user_id', '$user_id' ";
			
			$sql = "INSERT INTO INTRANET_USER ($keys) VALUES ($values)";
			$success = $this->db_db_query($sql);
			
			if($success){
				$record_id = $this->db_insert_id();
			}
			else{
				$record_id = false;
			}
		}
		//debug_pr($sql);
		//debug_pr($record_id);
		
		if($record_id > 0 && isset($map['UserPassword']))
		{
			$userlogin = $this->cleanData($map['UserLogin']);
			$user_password = $this->cleanData($map['UserPassword']);

			if ($intranet_authentication_method=="HASH") {
			    $sql = "UPDATE INTRANET_USER SET HashedPass=MD5('".$this->Get_Safe_Sql_Query($userlogin).$this->Get_Safe_Sql_Query($user_password)."$intranet_password_salt') WHERE UserID='$record_id'";
			} else {
			    $sql = "UPDATE INTRANET_USER SET UserPassword='".$this->Get_Safe_Sql_Query($user_password)."' WHERE UserID='$record_id'";
			}
			$update_pwd_success = $this->db_db_query($sql);
			//debug_pr($sql);
			//debug_pr($update_pwd_success);
		}
		
		return $record_id;
	}
	
	function getImportUserHeaderColumns($type="")
	{
		if($type == 'NewJoiner'){
			$columns = array("UserLogin","Password","UserEmail","EnglishName","Company","Division","Department","JoinDate","TerminatedDate","Status");
		}else if($type == 'Movement'){
			$columns = array("UserLogin","Password","UserEmail","EnglishName","Company","Division","Department","EffectiveDate","Status");
		}else if($type == 'Leaver'){
			$columns = array("UserLogin","Password","UserEmail","EnglishName","Company","Division","Department","JoinDate","TerminatedDate","Status");
		}else{
			$columns = array("UserLogin","Password","UserEmail","EnglishName","Company","Division","Department","JoinDate","EffectiveDate","TerminatedDate","Status");
		}
		return $columns;
	}
	
	function convertTargetSelectionToUserAry($targetAry)
	{
		$target_ary = (array)$targetAry;
		$target_size = count($target_ary);

		$user_ary = array();
		$user_id_ary = array();
		$company_id_ary = array();
		$division_id_ary = array();
		$department_id_ary = array();
		$company_division_id_groups = array();
		for($i=0;$i<$target_size;$i++)
		{
			if(preg_match('/^COM\d+$/', $target_ary[$i])){
				$company_id_ary[] = str_replace('COM','', $target_ary[$i]);
			}else if(preg_match('/^DIV.+$/', $target_ary[$i])){
				$company_division_id = str_replace('DIV','', $target_ary[$i]);
				if(strpos($company_division_id,',')!==false){
					$id_ary = explode(',',$company_division_id);
					//$company_id_ary[] = $id_ary[0];
					//$division_id_ary[] = $id_ary[1];
					$company_division_id_groups[] = array('CompanyID'=>$id_ary[0],'DivisionID'=>$id_ary[1]);
				}else{
					$division_id_ary[] = $company_division_id;
				}
			}else if(preg_match('/^DEP\d+$/', $target_ary[$i])){
				$department_id_ary[] = str_replace('DEP','', $target_ary[$i]);
			}else if(preg_match('/^U\d+$/', $target_ary[$i])){
				$user_id_ary[] = str_replace('U','', $target_ary[$i]);
			}else if(preg_match('/^G\d+$/', $target_ary[$i])){
			    $GroupIDAry[] = str_replace('G','', $target_ary[$i]);
			}
		}
		if(empty($department_id_ary)){
		    $department_id_ary  = $this->getMappingDepartmentGroupID(implode(',',$GroupIDAry));
		}else{
		    $department_id_ary = array_merge($department_id_ary,$this->getMappingDepartmentGroupID(implode(',',$GroupIDAry)));
		}
		
		if(count($company_id_ary)>0){
			$users = $this->getUserRecords(array('GetUsersUnderCompanyID'=>$company_id_ary,'GetOrganizationInfo'=>1,'GetOrganizationField'=>1,'GetOrganizationFieldNoTooltip'=>1));
			$user_id_ary = array_merge($user_id_ary, Get_Array_By_Key($users,'UserID'));
		}
		
		if(count($division_id_ary)>0){
			$users = $this->getUserRecords(array('GetUsersUnderDivisionID'=>$division_id_ary,'GetOrganizationInfo'=>1,'GetOrganizationField'=>1,'GetOrganizationFieldNoTooltip'=>1));
			$user_id_ary = array_merge($user_id_ary, Get_Array_By_Key($users,'UserID'));
		}
		
		if(count($department_id_ary)>0){
			$users = $this->getUserRecords(array('GetUsersUnderDepartmentID'=>$department_id_ary,'GetOrganizationInfo'=>1,'GetOrganizationField'=>1,'GetOrganizationFieldNoTooltip'=>1));
			$user_id_ary = array_merge($user_id_ary, Get_Array_By_Key($users,'UserID'));
		}
		
		if(count($company_division_id_groups)>0){
			$users = $this->getUserRecords(array('GetUsersUnderCompanyDivisionIDGroup'=>$company_division_id_groups,'GetOrganizationInfo'=>1,'GetOrganizationField'=>1,'GetOrganizationFieldNoTooltip'=>1));
			$user_id_ary = array_merge($user_id_ary, Get_Array_By_Key($users,'UserID'));
		}
		
		$user_id_ary = array_values(array_unique($user_id_ary));
		$users = $this->getUserRecords(array('user_id'=>$user_id_ary,'GetOrganizationInfo'=>1,'GetOrganizationField'=>1,'GetOrganizationFieldNoTooltip'=>1));
		$user_size = count($users);
		$name_field = Get_Lang_Selection('ChineseName','EnglishName');
		
		for($i=0;$i<$user_size;$i++){
			$user_name = $users[$i][$name_field];
			if($users[$i]['Organization'] != ''){
				$user_name .= '('.$users[$i]['Organization'].')';
			}
			$user_ary[] = array($users[$i]['UserID'],$user_name);
		}
		
		return $user_ary;
	}
	
	function auotActivateSuspendUserAccounts()
	{
	    global $PATH_WRT_ROOT,$intranet_root;
	    
		$today = date("Y-m-d");
		$users = $this->getUserRecords(array());
		$user_size = count($users);
		
		$log = date("Y-m-d H:i:s").' ';
		for($i=0;$i<$user_size;$i++){
			
			$user_id = $users[$i]['UserID'];
			$status = $users[$i]['RecordStatus'];
			$join_date = $users[$i]['JoinDate'];
			$terminated_date = $users[$i]['TerminatedDate'];
			$effective_date = $users[$i]['EffectiveDate'];
			$move_to_department_id = $users[$i]['MoveToDepartmentID'];
			
			if($status == '0' && $join_date !='' && $join_date <= $today && ($terminated_date=='' || ($terminated_date!='' && $terminated_date > $today))){
				$sql = "UPDATE INTRANET_USER SET RecordStatus='1' WHERE UserID='$user_id'";
				$success['Activated_'.$user_id] = $this->db_db_query($sql);
				$log .= '[Activated '.$user_id.' '.$users[$i]['UserLogin'].' '.$users[$i]['EnglishName'].' '.$join_date.' '.$terminated_date.' '.($success['Activated_'.$user_id]?'SUCCESS':'FAIL').']';
			}
			
			if($status == '1' && $terminated_date != '' && $terminated_date <= $today){
			    include_once($PATH_WRT_ROOT."includes/libuser.php");
			    include_once($PATH_WRT_ROOT."includes/libclass.php");
			    include_once($PATH_WRT_ROOT."includes/libwebmail.php");
			    include_once($PATH_WRT_ROOT."includes/libftp.php");
			    include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
			    
			    
			    $lsp = new libstudentpromotion();
			    $lu = new libuser();
// 				$sql = "UPDATE INTRANET_USER SET RecordStatus='0' WHERE UserID='$user_id'";

			    $lsp->archiveIntranetUser($user_id);
			    $lu->prepareUserRemoval($user_id);
			    $lu->removeUsers($user_id);
			    
			    # delete calendar viewer
			    $sql = "delete from CALENDAR_CALENDAR_VIEWER where UserID in ($user_id)";
			    $lu->db_db_query($sql);
			    
				$success['Suspended_'.$user_id] = $this->db_db_query($sql);
				$log .= '[Suspended '.$user_id.' '.$users[$i]['UserLogin'].' '.$users[$i]['EnglishName'].' '.$join_date.' '.$terminated_date.' '.($success['Suspended_'.$user_id]?'SUCCESS':'FAIL').']';
			}
			
			if($move_to_department_id != '' && $effective_date != '' && $effective_date <= $today){
				$department_record = $this->getDepartmentRecords(array('DepartmentID'=>$move_to_department_id));
				if(count($department_record)>0){
					$old_department_users = $this->getDepartmentUserRecords(array('DepartmentUserID'=>$user_id));
					$old_department_ids = Get_Array_By_Key($old_department_users,'DepartmentID');
					$this->deleteDepartmentUserRecord($old_department_ids,$user_id);
					$success['MoveDepartment_'.$user_id] = $this->insertDepartmentUserRecord($move_to_department_id,array($user_id));
					if($success['MoveDepartment_'.$user_id]){
						$this->upsertUserRecord(array('user_id'=>$user_id,'MoveToDepartmentID'=>''));
					}
					$log .= '[MoveDepartment '.$user_id.' '.$users[$i]['UserLogin'].' '.$users[$i]['EnglishName'].' '.$effective_date.' ('.$move_to_department_id.' '.$department_record[0]['DepartmentCode'].') '.($success['MoveDepartment_'.$user_id]?'SUCCESS':'FAIL').']';
				}
			}
		}
		$this->log($log);
	}
	
	function SetSchoolNewsGroupInfo($Target_user,$MenuID,$GroupIDList){
	    $sql = "SELECT * FROM SCHOOL_INFO_USERGROUP  WHERE MenuID = '$MenuID'";
	
	    $Record = $this->returnArray($sql);
	   
	    
	    if(count($Record)>0){
	        $sql = "UPDATE 
                        SCHOOL_INFO_USERGROUP
                    SET 
                        GroupType = '".$Target_user."',
                        GroupIDList   = '$GroupIDList',
                        DateModified  = now(),
                        ModifyBy = '".$_SESSION['UserID']."' 
                    WHERE 
                        MenuID = '$MenuID'
                    ";
	        $success = $this->db_db_query($sql);
	        
	    }else{
	        
	        $sql = "INSERT INTO SCHOOL_INFO_USERGROUP
                    (MenuID,GroupType,GroupIDList ,DateInput,InputBy,DateModified,ModifyBy)
                    VALUES
                    ('$MenuID','$Target_user', '$GroupIDList' ,now(),'".$_SESSION['UserID']."',now(),'".$_SESSION['UserID']."')";
	        $success = $this->db_db_query($sql);
	    }
	 
	    
	    return $success;
	}
	
	function getSchoolNewsGroupList($MenuID='0'){
	    
	    if($MenuID !='0'){
	        $conds ="AND MenuID = '$MenuID'";
	    }	
	    $sql = "SELECT GroupIDList FROM SCHOOL_INFO_USERGROUP WHERE 1 $conds";
	    
	    $Record = $this->returnVector($sql);
	    return $Record[0];
	}
	
	function getSchoolNewsTargetGroup($MenuID='0'){
	    
	    if($MenuID !='0'){
	        $conds ="AND MenuID = '$MenuID'";
	    }
	    $sql = "SELECT GroupType  FROM SCHOOL_INFO_USERGROUP WHERE 1 $conds";
	    
	    $Record = $this->returnVector($sql);
	    return $Record[0];
	}
	
	
	
	function getSchoolNewsDepartmentList($MenuID='0'){
	    
	    $GroupIDList = $this->getSchoolNewsGroupList($MenuID);
	  
	    $sql = "SELECT dd.DepartmentID 
                FROM INTRANET_GROUP  AS ig
                INNER JOIN DHL_DEPARTMENT  AS dd ON dd.DepartmentNameEng = ig.Title 
                WHERE GroupID in ($GroupIDList)";
	
	    $Record = $this->returnVector($sql);
	 
	    return $Record;
	}
	
	
	function getUserSchoolNewsList($MenuID='0'){
	    
	    $UserID = $_SESSION['UserID'];
// 	    $UserID = '27025';
	    $sql = "SELECT 
                    ddu.DepartmentID,
                    ig.GroupID
                FROM 
                    DHL_DEPARTMENT_USER AS ddu
                INNER JOIN DHL_DEPARTMENT  AS dd ON dd.DepartmentID = ddu.DepartmentID
                INNER JOIN INTRANET_GROUP AS ig  ON dd.DepartmentNameEng = ig.Title 
                WHERE 
                        ddu.UserID = '$UserID'
                        AND ig.RecordType=1 
                        AND ig.Title IS NOT NULL 
                        AND ig.Title<>''
                ";
    
	    $Record = $this->returnArray($sql);
	    
// 	    $sql = "SELECT
//             	    dpi.ItemID,
//             	    ig.GroupID
//             	FROM
//             	    DHL_PIC_INFO AS dpi
//             	    INNER JOIN DHL_DEPARTMENT  AS dd ON dd.DepartmentID = ddu.DepartmentID
//             	    INNER JOIN INTRANET_GROUP AS ig  ON dd.DepartmentNameEng = ig.Title
//             	WHERE
//             	    ddu.UserID = '$UserID'
//             	    AND ig.RecordType=1
//             	    AND ig.Title IS NOT NULL
//             	    AND ig.Title<>''
// 	    ";
	    
// 	    debug_Pr($Record);
	    return $Record;
	}
	
	function getMappingIntranetGroupID($GroupID){
	    
	    $GroupIDAry = is_array($GroupID)?$GroupID:(array)$GroupID;
	    $sql = "SELECT ig.GroupID
	    FROM INTRANET_GROUP as ig
	    INNER JOIN DHL_DEPARTMENT as d ON d.DepartmentNameEng = ig.Title 
        WHERE  d.DepartmentID IN ('".implode('\',\'',$GroupIDAry)."')";
	    $Records = $this->returnVector($sql);
	    return $Records;	    
	}
	function getMappingDepartmentGroupID($GroupIDList){
	   
	    $sql = "SELECT d.DepartmentID
	    FROM INTRANET_GROUP as ig
	    INNER JOIN DHL_DEPARTMENT as d ON d.DepartmentNameEng = ig.Title
        WHERE  ig.GroupID IN ($GroupIDList)";
	    $Records = $this->returnVector($sql);

	    return $Records;	 
	    
	}
	
}

?>