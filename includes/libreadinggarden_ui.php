<?php
// editing by  

/***************************** Modification Log *************************************
 * 2016-05-16 (Cameron) :
 * 		- replace if(trim($array)!='') by if($array) in: Get_Student_Recommend_Book_List_Index()
 * 2016-05-13 (Cameron) : 
 * 		- fix bug in Get_Settings_Edit_Assign_Reading(): do not use basename to retrieve attachment name as it does not support Chinese
 * 		- fix bug: show description with line break in Get_Settings_State_Setting_Table()
 * 2016-05-12 (Cameron) : modify Get_Settings_Assign_Reading_Setting_Table(), use if($YearClassID) to replace if(trim($YearClassID)!='') 
 * 2015-09-10 (Ivan): modified Get_Reading_Garden_Index_Title() to add flag $sys_custom['ReadingGarden']['TeacherCanViewAllClassesRecords'] that teacher can view all classes reading records
 * 2013-03-12 (Rita) :modified Get_Book_Detail_Entry() cater different type of book
 * 2013-03-11 (Rita) :modified Get_Settings_Assign_Reading_Select_Book_Layer() add z-index: -1; 
 * 2013-02-27 (Rita) :modified Get_My_Record_New_My_Reading_Book_Layer_Step1_External_Book() for customization [#2013-0225-1415-16054]
 * 2013-02-20 (Rita) :add Get_Poppular_Award_Scheme_Display_Table() for customization [#2012-1009-1402-14054] 
 * 2013-02-19 (Rita) :modified Get_Recommend_Book_Reading_Record_Edit_Form() customization [#2012-1009-1402-14054] 
 * 2013-02-06 (Rita) :modified Get_My_Record_New_My_Reading_Book_Layer_Step1_External_Book() for customization [#2012-1009-1402-14054] 					  
 * 2012-11-08 (Yuen) :updated to support eBook open using new reader (HTML5)
 * 2012-11-06 (Rita) :modified Get_Teacher_View_Student_Reading_Record_UI() add Academic Year Selection
 * 								Get_Teacher_View_Student_Reading_Record()
 * 								Get_My_Record_My_Reading_Book_UI()
 * 								Get_My_Record_My_Reading_Book_DBTable()
 * 2012-09-27 (Rita) : modified Get_Book_Edit_Form() remove mandatory * for Call number field
 * 2012-09-24 (Rita) : modified Get_Popular_Award_Scheme_Form_Table add import award btn
 * 2012-09-19 (Rita) :modified Get_Book_Index() add import btn
 * 2012-07-17 (Carlos): modified download_attachment.php target to target_e (encrypted file path)
 * 2012-04-27 (Marcus) : 
 *	- modified Get_Student_Book_Report_Edit_Form, cater online writing, file upload instruction. 
 *	- Added Get_Book_Report_Setting_Index_UI.
 *	- Modified Get_Settings_Edit_Assign_Reading, cater Online Writing Instruction
 *	- modified GET_MODULE_OBJ_ARR add Book Report Setting 
 * 2011-09-08 (Yuen) : set value to fckeditor in order to load contents even when using iPad
 * 2011-05-27 (Carlos) : add comment bank and sticekr to report marking page
 * 2011-05-26 (Carlos) : add sticker settings
 * 2011-05-25 (Carlos) : add comment bank settings
 * 2011-05-13 (Carlos) : add award level to award setting
 * 2011-05-04 (Carlos) : modified places where involves answer sheet
 * 2011-03-24 (Carlos) : change UI of book report marking frameset page
 * 2011-03-23 (Carlos) : added Get_Book_Detail_Entry()
 ************************************************************************************/
include_once("libinterface.php");
include_once($intranet_root."/lang/readinggarden_lang.$intranet_session_language.php");

class libreadinggarden_ui extends interface_html
{
	function libreadinggarden_ui($template='')
	{
		if(trim($template)!='')
			parent::interface_html($template);
		else
			parent::interface_html();
			
		$this->MaxLength = 255;
		$this->MaxCodeLength = 20;
		$this->MaxNumLength = 11;
		
		$this->TBHeight = "450";
		$this->TBWidth = "600";
		$this->TBReadingRecordHeight = "400";
		$this->TBReadingRecordWidth = "500";
		$this->TBBookReportFileHeight = "200";
		$this->TBBookReportFileWidth = "500";	
		$this->TBAnswerSheetHeight = "500";
		$this->TBAnswerSheetWidth = "700";
		$this->TBOnlineWritingHeight = "350";
		$this->TBOnlineWritingWidth = "600";		
				
	}
	
	function GET_MODULE_OBJ_ARR($PortalPage='')
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $Lang, $sys_custom, $plugin, $customLeftMenu;
		
		# Menu information
    	$ReadingGardenPath = $PATH_WRT_ROOT."home/eLearning/reading_garden/";
//		$ReadingGardenPath = $PATH_WRT_ROOT."home/eLearning/reading_garden/books/";
		
		$MenuArr=array();

	    $MODULE_OBJ['title'] = $Lang['ReadingGarden']['ReadingGarden'];

	    if($PortalPage!=1)
		{
			
			// Current Page Information init
			$PageManagement = 0;
			$PageManagement_Announcement = 0;
			$PageManagement_Forum = 0;
			$PageManagement_PopularAwardScheme = 0;
			
			$PageReports = 0;
			
			$PageStatistic = 0;
			
			$PageSettings = 0;
			$PageSettings_BookSettings = 0;
			$PageSettings_BookCategorySettings = 0;
			$PageSettings_AssignReadingSettings = 0 ;
			$PageSettings_BookSourceSettings = 0;
			$PageSettings_BookReportSettings = 0;
			$PageSettings_RecommendBookSettings = 0;
			$PageSettings_ReadingTargetSettings = 0;
			$PageSettings_AwardScheme = 0;
			$PageSettings_PopularAwardScheme = 0;
			$PageSettings_AnswerSheetSettings = 0;
			$PageSettings_StateSettings = 0;
			$PageSettings_CommentBankSettings = 0;
			$PageSettings_StickerSettings = 0;
			
			if($CurrentPage)
			{
				list($Section,$Page) = explode("_",$CurrentPage);
				${"Page".$Section} = 1;
				${"Page".$CurrentPage} = 1;
			}
			
			/*
			switch ($CurrentPage) {
				case "Management_Schedule":
					$PageManagement = 1;
					$PageManagement_Schedule = 1;
					break;
				case "Management_MarksheetRevision":
					$PageManagement = 1;
					$PageManagement_MarksheetRevision = 1;
					break;
			}
			*/

			# Mgmt
			$MenuArr["Management"] = array($Lang['ReadingGarden']['ManagementArr']['MenuTitle'], "", $PageManagement);
	
				# Management > Announcement
				$MenuArr["Management"]["Child"]["AnnouncementMgmt"] = array($Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/management/announcement/announcement.php?clearCoo=1", $PageManagement_Announcement);
				
				# Management > Forum
				$MenuArr["Management"]["Child"]["ForumMgmt"] = array($Lang['ReadingGarden']['ForumMgmt']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/management/forum/forum.php", $PageManagement_Forum);

				if($sys_custom['ReadingGarden']['PopularAwardScheme'])
				{
					# Management > Popular Award Scheme 
					$MenuArr["Management"]["Child"]["PopularAwardSchemeMgmt"] = array($Lang['ReadingGarden']['PopularAwardSchemeMgmt']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/management/popular_award_scheme/index.php", $PageManagement_PopularAwardScheme);
				}

			# Reports
//			$MenuArr["Reports"] = array($Lang['ReadingGarden']['ReportsArr']['MenuTitle'], "", $PageReports);

			# Reports
			$MenuArr["Statistic"] = array($Lang['ReadingGarden']['StatisticArr']['MenuTitle'],  $PATH_WRT_ROOT."home/eLearning/reading_garden/statistic/", $PageStatistic);
		
			# Settings
			$MenuArr["Settings"] = array($Lang['ReadingGarden']['SettingsArr']['MenuTitle'], "", $PageSettings);
			
				# Settings > Book
				$MenuArr["Settings"]["Child"]["BookSettings"] = array($Lang['ReadingGarden']['BookSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/books/?clearCoo=1", $PageSettings_BookSettings);

				# Settings > BookCategory
				$MenuArr["Settings"]["Child"]["BookCategorySettings"] = array($Lang['ReadingGarden']['BookCategorySettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/book_category/book_category.php", $PageSettings_BookCategorySettings);

				# Settings > AssignReadingSettings
				$MenuArr["Settings"]["Child"]["AssignReadingSettings"] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/assign_reading/assign_reading.php?clearCoo=1", $PageSettings_AssignReadingSettings);

				# Settings > Reading Target
				$MenuArr["Settings"]["Child"]["ReadingTargetSettings"] = array($Lang['ReadingGarden']['ReadingTarget'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/reading_target/reading_target.php", $PageSettings_ReadingTargetSettings);

				# Settings > AwardScheme
				$MenuArr["Settings"]["Child"]["AwardScheme"] = array($Lang['ReadingGarden']['AwardScheme'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/award_scheme/award_scheme.php", $PageSettings_AwardScheme);

				if($sys_custom['ReadingGarden']['PopularAwardScheme'])
				{
					# Settings > PopularAwardScheme
					$MenuArr["Settings"]["Child"]["PopularAwardScheme"] = array($Lang['ReadingGarden']['PopularAwardSchemeSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/popular_award_scheme/index.php", $PageSettings_PopularAwardScheme);
				}

				# Settings > Answer Sheet Settings
				$MenuArr["Settings"]["Child"]["AnswerSheetSettings"] = array($Lang['ReadingGarden']['AnswerSheetTemplateSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/answer_sheet/index.php?clearCoo=1", $PageSettings_AnswerSheetSettings);

				# Settings > BookSourceSettings
				$MenuArr["Settings"]["Child"]["BookSourceSettings"] = array($Lang['ReadingGarden']['BookSourceSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/form_book_source_settings/form_book_source_settings.php", $PageSettings_BookSourceSettings);

				# Settings > BookReportSettings
				$MenuArr["Settings"]["Child"]["BookReportSettings"] = array($Lang['ReadingGarden']['BookReportSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/book_report/settings.php", $PageSettings_BookReportSettings);
			
				# Settings > StateSettings & ScoreRuleSettings
				$MenuArr["Settings"]["Child"]["StateSettings"] = array($Lang['ReadingGarden']['StateSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/state/state.php", $PageSettings_StateSettings);
				
				$MenuArr["Settings"]["Child"]["CommentBankSettings"] = array($Lang['ReadingGarden']['CommentBank'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/comment/index.php", $PageSettings_CommentBankSettings);
				
				$MenuArr["Settings"]["Child"]["StickerSettings"] = array($Lang['ReadingGarden']['Sticker'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/sticker/index.php", $PageSettings_StickerSettings);

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Reading Scheme";'."\n";
            $js.= '</script>'."\n";
            $MODULE_OBJ['title'] .= $js;

			# module information
			$MODULE_OBJ['title_css'] = "menu_opened";
		    $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reading_scheme.png";
		    $MODULE_OBJ['root_path'] = $ReadingGardenPath."?clearCoo=1";
		    $MODULE_OBJ['menu'] = $MenuArr;
		}
		else
		{
            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Reading Scheme";'."\n";
            $js.= '</script>'."\n";
            $MODULE_OBJ['title'] .= $js;

			parent::interface_html("reading_scheme_default.html");
			
//			$MODULE_OBJ['title_css'] = "menu_small";
//		    $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reading_scheme.png";
//		    $MODULE_OBJ['root_path'] = $ReadingGardenPath;
//		    $MODULE_OBJ['CustomLogo'] = $this->printLeftMenu();
		}
		
		return $MODULE_OBJ;
	}

	function printLeftMenu() 
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$img = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reading_scheme.png";
		$x .= '<span><div id="Reading_Scheme_logo">';
            $x .= "<a href='".$PATH_WRT_ROOT."home/eLearning/reading_garden/?clearCoo=1'><img border=\"0\" width=\"110\" height=\"100\" src=\"$img\"/></a>";
		$x .= '</div></span>';

		return $x;
	}
	
	function Include_Reading_Garden_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$x = '
			<script>
				var js_PATH_WRT_ROOT = "'.$PATH_WRT_ROOT.'";
				var js_ConfirmDeleteReadingRecord = "'.$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteReadingRecord'].'";
				var js_InvalidDateFormat = "'.$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat'].'";
				var js_InvalidDateRange = "'.$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange'].'";
				var js_SubmitAnswerSheetSuccess = "'.$Lang['ReadingGarden']['ReturnMsg']['SubmitAnswerSheetSuccess'].'";
				var js_SubmitAnswerSheetFail = "'.$Lang['ReadingGarden']['ReturnMsg']['SubmitAnswerSheetFail'].'";
				var js_ConfirmDeleteAnswerSheet = "'.$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteAnswerSheet'].'";
				var js_DeleteAnswerSheetSuccess = "'.$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetSuccess'].'";
				var js_DeleteAnswerSheetFail = "'.$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetFail'].'";
				var js_Display_All_Cover = "'.$Lang['ReadingGarden']['DisplayAllCover'].'";
				var js_Hide_All_Cover = "'.$Lang['ReadingGarden']['HideAllCover'].'";
				var js_StartDateGreaterThanEndDate = "'.$Lang['ReadingGarden']['WarningMsg']['RequestInputStartDateEarlierThanEndDate'].'";
				var js_StartDateGreaterThanToday = "'.$Lang['ReadingGarden']['WarningMsg']['StartDateGreaterThanToday'].'";
				var js_EndDateGreaterThanToday = "'.$Lang['ReadingGarden']['WarningMsg']['EndDateGreaterThanToday'].'";
				var js_ConfirmDeleteOnlineWriting = "'.$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteOnlineWriting'].'";
				var js_SubmitOnlineWritingSuccess = "'.$Lang['ReadingGarden']['ReturnMsg']['SubmitOnlineWritingSuccess'].'";
				var js_SubmitOnlineWritingFail = "'.$Lang['ReadingGarden']['ReturnMsg']['SubmitOnlineWritingFail'].'";
				var js_DeleteOnlineWritingSuccess = "'.$Lang['ReadingGarden']['ReturnMsg']['DeleteOnlineWritingSuccess'].'";
				var js_DeleteOnlineWritingFail = "'.$Lang['ReadingGarden']['ReturnMsg']['DeleteOnlineWritingFail'].'";
				var js_ConfirmDeleteBookReport = "'.$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBookReport'].'";
				var js_ConfirmYouCanOnlyAnswerOneAnswerSheet = "'.$Lang['ReadingGarden']['ConfirmArr']['YouCanOnlyAnswerOneAnswerSheet'].'";

			</script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'home/eLearning/reading_garden/reading_scheme.js"></script>
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/reading_scheme.css" type="text/css" />
		'."\n";
		
		return $x;	
	}

	function  Include_Reading_Garden_Answer_Sheet_CSS()
	{
		$x .= '
			<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>
			<script language="javascript" src="/templates/forms/layer.js"></script>
		';
		
		return $x;
	}
		
	function Get_Reading_Garden_Index_Title($ClassID='', $hideClassSelect=0)
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib,$image_path ,$LAYOUT_SKIN, $sys_custom;
		
		if(!$ReadingGardenLib->isStudent && $hideClassSelect != 1)
		{
			include_once("libclass.php");
			$lclass = new libclass();
			
			$ClassSelection = '<form>';
				if(!$ReadingGardenLib->isAdmin) // Admin can access all class, Teacher can access teaching classes only
				{	
					//get class teacher
					include_once("form_class_manage.php");
					$YearObj = new Year();
					
					if ($sys_custom['ReadingGarden']['TeacherCanViewAllClassesRecords']) {
						$checkClassTeacher = 0;
					}
					else {
						$checkClassTeacher = 1;
					}
					$ClassTeacherClass = $YearObj->Get_All_Classes($checkClassTeacher);
					
					$ClassTeacherClassID = Get_Array_By_Key($ClassTeacherClass,"YearClassID");
					
					// get teaching class
					include_once("subject_class_mapping.php");
					$scm = new subject_class_mapping();
					$TeachingClassList = $scm->returnSubjectTeacherClass($_SESSION['UserID']);
					$TeachingClassID = Get_Array_By_Key($TeachingClassList,"ClassID");
					
					$ClassIDArr = array_union($ClassTeacherClassID, $TeachingClassID);
				}
				$ClassSelection .= $lclass->getSelectClassID(" id='ClassID' name='ClassID' onchange='this.form.submit()' style='float:left;' ",$ClassID,2,'','',$ClassIDArr);
			$ClassSelection .= '</form>';
		}
		else
		{
			$ClassSelection = '<input type="hidden" id="ClassID" name="ClassID" value="'.$ClassID.'">';
//			$Score = $ReadingGardenLib->Get_Student_Score($_SESSION['UserID']);
//			$ClassSelection .= '<span id="DebugScore">'.$Score[$_SESSION['UserID']].'</span>';
		}
		
		$x .= '<table width="100%"><tr>'."\n";
			$x .= '<td class="contenttitle">'.$ClassSelection.'</td>'."\n";
			if($ReadingGardenLib->isAdmin) // implement access right checking here
				$x .= '<td align="right"><a class="GroupTitleLink" href="'.$ReadingGardenLib->ModulePath.'/books/"><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/icon_manage.gif" border=0 align="absmiddle">'.$Lang['General']['Settings'].'</a></td>'."\n";
		$x .= '</tr></table>'."\n";
		
		return $x;
	}
	
	function Get_Reading_Garden_Index($ClassID='')
	{
		global $ReadingGardenLib;
		
		if($ReadingGardenLib->isStudent)
			$MyRecordArea = $this->Get_Reading_Garden_Index_MyRecord_Area($ClassID);
		else
			$MyRecordArea = $this->Get_Reading_Garden_Index_Teacher_Record_Area($ClassID);
		$AnnouncementArea = $this->Get_Reading_Garden_Index_Announcement_Area();
		$ForumArea = $this->Get_Reading_Garden_Index_Forum_Area($ClassID);
		$RecommendArea = $this->Get_Reading_Garden_Index_Recommend_Area($ClassID);
		
		$x .= '<br />'."\n";
		$x .= '<div style="text-align:left;"> '."\n";
		$x .= '<div class="reading_portal_top">'."\n";
			$x .= '<div class="reading_top_left">'."\n";
				$x .= $MyRecordArea;
			$x .= '</div>'; 
			$x .= '<div class="reading_top_right">'."\n";
				$x .= $AnnouncementArea;
			$x .= '</div>'; 
		$x .= '</div>';
		$x.= $this->Spacer()."\n";
		$x .= '<div class="reading_portal">'."\n";
			$x .= '<div class="reading_left">'."\n";
				$x .= $ForumArea;
			$x .= '</div>'; 
			$x .= '<div class="reading_right">'."\n";
				$x .= $RecommendArea;
			$x .= '</div>'; 
		$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Reading_Garden_Index_MyRecord_Area($ClassID)
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT, $sys_custom;
		
		# Get State Info
		$StateInfoArr = $ReadingGardenLib->Get_Student_State($_SESSION['UserID']);
		$StateInfo = $StateInfoArr[$_SESSION['UserID']];
		
		# Get Reading Target
//		$ClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($_SESSION['UserID']);
//		$ClassID = $ClassInfo[0]['YearClassID'];
		$ReadingTarget = $ReadingGardenLib->Get_Class_Reading_Target_Mapping('',$ClassID,1);
		$ReadingTargetChart = $this->Get_Student_Reading_Target_Achievement_Chart($_SESSION['UserID'],$ClassID,$ReadingTarget[0]['ReadingTargetID']);
		
		$x.= '<div id="reading_myrecord" class="reading_board">'."\n";
			$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
				$x.= '<h1>'.$Lang['ReadingGarden']['MyRecord'].'</h1>'."\n";
			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_left">'."\n";
				$x.= '<div class="reading_board_right">'."\n";
					$x.= '<div class="reading_myrecord_conent">'."\n";
					
						$x.= '<div class="reading_mystate">'."\n";
						
							# State
							$x.= '<div class="myrecord_state">'."\n";
//								$x.= '<h2>'.$Lang['ReadingGarden']['State'].'</h2>'."\n";
								$x.= '<p class="spacer">&nbsp;</p>'."\n";
								if(!$StateInfo)
								{
									$x .= $Lang['General']['NoRecordAtThisMoment'];
								}
								else
								{
									$x.= '<span>'."\n";
										$x.= '<a href="javascript:void(0);" onclick="js_View_Level();" class="reading_more" title="'.$Lang['ReadingGarden']['ViewState'].'">'."\n";
											$x.= '<img border=0 width="100px" height="120px"  src="'.$PATH_WRT_ROOT.$ReadingGardenLib->StateLogoPath."/".$StateInfo['ImagePath'].'">'."\n";
										$x.= '</a>'."\n";
										$x.= '<br><em>'.$StateInfo['StateName'].'</em>'."\n";
									$x.= '</span>'."\n";
								}
								$x.= '<p class="spacer">&nbsp;</p>'."\n";
							$x.= '</div>'."\n";
							$x.= '<p class="spacer">&nbsp;</p>'."\n";
							$x.= '<p class="spacer">&nbsp;</p>'."\n";
							
							# Target
							if(!empty($ReadingTarget))
							{
								$x.= '<br>'."\n";
								$x.= '<div class="myrecord_target">'."\n";
									$x.= '<h2><span><a class="reading_more" href="student_record/index.php?CurrentTab=ReadingTarget">'.$Lang['ReadingGarden']['ReadingTarget'].'</a></span><!--<a href="student_record/index.php?CurrentTab=ReadingTarget" class="reading_more">'.$Lang['ReadingGarden']['More'].'</a>--> <p class="spacer"></p></h2>'."\n";
									$x.= '<p class="spacer">&nbsp;</p>'."\n";
	//								$x.= '<em>'.$ReadingTarget[0]['TargetName'].'</em>'."\n";
									$x.= $ReadingTarget[0]['TargetName']."\n";
									$x .= '<div class="tb_hidden_object">';
										$x.= $ReadingTargetChart."\n";
									$x.= '</div>'."\n";
								$x.= '</div>'."\n";
							}
							$x.= '&nbsp;'."\n";
						$x.= '</div>'."\n";
						
						$x.= '<div class="reading_mybook">'."\n";
							$x.= '<div id="reading_mybook_record" class="reading_board">'."\n";
								$x.= '<div class="reading_board_top_left">'."\n";
									$x.= '<div class="reading_board_top_right">'."\n";
										$x.= '<div class="mybook_record_tab">'."\n";
											$x.= '<ul>'."\n";
												$x.= '<li id="AssignedReadingTab"><a href="javascript:void(0);" onclick="js_Load_MyRecord_Area(\'AssignedReading\')">'.$Lang['ReadingGarden']['AssignedBook'].'</a></li>'."\n";
												$x.= '<li id="ReadingRecordTab"><a href="javascript:void(0);" onclick="js_Load_MyRecord_Area(\'ReadingRecord\')">'.$Lang['ReadingGarden']['MyReadingBook'].'</a></li>'."\n";
												if($sys_custom['ReadingGarden']['PopularAwardScheme'])
													$x.= '<li id="PopularAwardSchemeTab"><a href="javascript:void(0);" onclick="js_Load_MyRecord_Area(\'PopularAwardScheme\')">'.$Lang['ReadingGarden']['PopularAwardScheme'].'</a></li>'."\n";
											$x.= '</ul>'."\n";
										$x.= '</div>'."\n";
										$x.= '<a class="reading_more" href="javascript:void(0);" onclick="js_Go_To_Student_Record();">'.$Lang['ReadingGarden']['More'].'</a>'."\n";
										$x.= '<p class="spacer"></p>'."\n";
									$x.= '</div>'."\n";
								$x.= '</div>'."\n";
								
								# Assign Reading List / Reading Record
								$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
									$x .= '<div id="MyRecord_Area"></div>'."\n";
								$x.= '</div></div>'."\n";
								
								$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
								$x.= '<p class="spacer"></p>'."\n";
							$x.= '</div>'."\n";
						$x.= '</div>'."\n";
						$x.= '<p class="spacer">&nbsp;</p>'."\n";
						
					$x.= '</div>'."\n";
					$x.= '<p class="spacer">&nbsp;</p>'."\n";
				$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
		$x.= '</div>'."\n";
		$x .= '<input type="hidden" id="CurrentTab" value="">';								
								
		return $x;
	}

	function Get_Student_Reading_Target_Achievement_Chart($ParUserID, $ClassID, $ReadingTargetID)
	{
		global $PATH_WRT_ROOT, $ReadingGardenLib;
		
		$Achievement = $ReadingGardenLib->Get_Student_Reading_Target_Achievement_Percentage($ParUserID,$ClassID,$ReadingTargetID);
		
		include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
		
		$Percent = $Achievement[$ReadingTargetID][$ParUserID]['OverallAchievePercent'];
		$Required = $Achievement[$ReadingTargetID][$ParUserID]['OverallRequired'];
		$Count = $Achievement[$ReadingTargetID][$ParUserID]['OverallCount'];
		$Achieve = $Count;
		$Unachieve = $Required-$Count;
		
		$title = new title( ROUND(100*$Percent)."%" );
		$title->set_style( "{font-size: 12px; font-family: _sans; color: #333333; text-align:center;}" );
		
//		$x_legend = new x_legend( 'class' );
//		$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
		
//		$x = new x_axis();
//		$x->set_stroke( 2 );
//		$x->set_tick_height( 2 );
//		$x->set_colour( '#999999' );
//		$x->set_grid_colour( '#CCCCCC' );
//		$x->set_labels_from_array( array('1A','1B','1C','1D','1E','1F') );
//		
//		$y_legend = new y_legend( 'Quantity' );
//		$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
//		
//		$y = new y_axis();
//		$y->set_stroke( 2 );
//		$y->set_tick_length( 2 );
//		$y->set_colour( '#999999' );
//		$y->set_grid_colour( '#CCCCCC' );
//		$y->set_range( 0, 20, 5 );
//		$y->set_offset(true);
		
//		$pie0 = new pie_value(64,'記大����);
//		$pie0->set_colour( '#72a9db' );
		//$pie0->set_label('記大����, '#72a9db', '11');
//		$pie0->set_tooltip( '記大����#val#' );
		

		$label = " ";
		$color = "#8AD51C";
		$ctr= 0;
		$pie_section[$ctr] = new pie_value($Achieve,$hover);
		$pie_section[$ctr]->set_colour( $color );
		$pie_section[$ctr]->set_label($label, $color, '1');
//		$pie_section[$ctr]->set_tooltip( '#percent#' );

		$label = " ";
		$color = "#F6F0EA";
		$ctr++;
		$pie_section[$ctr] = new pie_value($Unachieve,$hover);
		$pie_section[$ctr]->set_colour( $color );
		$pie_section[$ctr]->set_label($label, $color, '1');
//		$pie_section[$ctr]->set_tooltip( $label.':#val# #total# #percent# ' );
		
		$pie = new pie();
		$pie->set_alpha(0.6);
		$pie->set_start_angle(-90);
//		$pie->set_animate( true );
//		$pie->set_tooltip( '#val# of #total#<br>#percent# of 100%' );
//		$pie->set_colours( array('#87CCIB','#FFFFFF') );
		//$pie->set_values( array(12,33,84,new pie_value(64,'記大����#val#')) );
		$pie->set_values( $pie_section );
		
		$tooltip = new tooltip();
//		$tooltip->set_hover();
		//$tooltip->set_stroke( 2 );
		//$tooltip->set_colour( "#000000" );
		//$tooltip->set_background_colour( "#ffffff" ); 
		
		# show/hide checkbox panel
		//$key = new key_legend();
		//$key->set_visible(true);		
		
		$chart = new open_flash_chart();
		$chart->set_bg_colour( '#EFE6DB' );
		$chart->set_title( $title );
//		$chart->x_legend = null;
//		$chart->x_axis = null;
//		$chart->y_legend = null;
//		$chart->y_axis = null;
		$chart->add_element( $pie );
//		$chart->set_tooltip( $tooltip );
		
		$x .= '
			<div id="my_chart">no flash?</div>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'includes/flashchart_basic/js/json/json2.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'includes/flashchart_basic/js/swfobject.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'home/eLearning/reading_garden/reading_scheme_flash_chart.js"></script>	
			<script type="text/javascript">
				var data = '.$chart->toPrettyString() .';
			</script>
			<script type="text/javascript">
				swfobject.embedSWF("'.$PATH_WRT_ROOT.'includes/flashchart_basic/open-flash-chart.swf", "my_chart", "100", "100", "9.0.0");
			</script>
		';
		
		return $x;
	}

	function Get_Reading_Garden_Index_MyRecord_Area_Table($TagSelected)
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
		if(in_array($TagSelected, array("AssignedReading", "ReadingRecord")))
		{
			$Table = $this->Get_Reading_Garden_Index_MyRecord_Area_Book_List($TagSelected);
		}
		else
		{
			$Table = $this->Get_Reading_Garden_Index_MyRecord_Area_Popular_Award_Scheme();	
		}
			
		return $Table;
	}

	function Get_Reading_Garden_Index_MyRecord_Area_Book_List($TagSelected)
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
		if($TagSelected == "AssignedReading")
		{
			$AssignedReadingList = $ReadingGardenLib->Get_Index_Assigned_Reading_List();
		}
		else
		{
			$AssignedReadingList = $ReadingGardenLib->Get_Index_My_Reading_Record_List();
			$NewBtn = '<div class="Conntent_tool">'."\n";
				$NewBtn .= $this->Get_Content_Tool_v30('new','./student_record/new_reading_record.php',$Lang['ReadingGarden']['AddReadingRecord']);
			$NewBtn .= '</div>'."\n";
		}

		$x .= $NewBtn;
		$x.= '<table class="mybook_record_table">'."\n";
			$x .= '<col style="width:60%;">';
			$x .= '<col>';
			$x .= '<col>';
		
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['Book'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['ReadingStatus'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['BookReport'].'</th>'."\n";
				$x.= '</tr>'."\n";
				
				if(sizeof($AssignedReadingList)>0)
				{
					$numRec = min(5,sizeof($AssignedReadingList));

					for($i=0; $i<$numRec; $i++)
					{
						$thisInfo  = $AssignedReadingList[$i];
						
						$img = $ReadingGardenLib->Get_Book_Cover_Image($thisInfo['BookCoverImage'], 45, 54);
						
						$CallBack = "js_Load_MyRecord_Area(\'".$TagSelected."\')";				
						if($thisInfo['RecordStatus'])
						{
							$SubmitReport = '';
							$ViewReport = '';
							$Answer = '';
							$Writing = '';
							$Report = '';
							$trclass = "row_read_book";
							$ReadStatus = $Lang['ReadingGarden']['Read'];
							
							$ReadingStatusLink = '<div onmouseover="View_Reading_Record(this,\''.$thisInfo['ReadingRecordID'].'\')" onmouseout="Remove_Reading_Record_Layer()">'.$this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_read", $Lang['ReadingGarden']['EditReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisInfo['BookID'].",0,'".$thisInfo['ReadingRecordID']."','$CallBack');", "FakeLayer", $ReadStatus, "").'</div>';
							
							if($thisInfo['Attachment'] || $thisInfo['Answer']|| $thisInfo['OnlineWriting'])
							{
								$ViewReport = '<a class="book_report_done" href="" onclick="js_View_Mark_Report('.$thisInfo['ReadingRecordID'].');">'.$Lang['ReadingGarden']['ViewBookReport'].'</a>';
							}
							
							if(!$thisInfo['Attachment'] && $thisInfo['BookReportRequired'])
							{
								$SubmitReport = $this->Get_Thickbox_Link($this->TBBookReportFileHeight, $this->TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'".$thisInfo['ReadingRecordID']."','".$thisInfo['BookReportID']."','".$thisInfo['AssignedReadingID']."','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
							}
							
							if(!$thisInfo['Answer'] && $thisInfo['AnswerSheet'])
							{
								$Answer = $this->Get_Thickbox_Link($this->TBAnswerSheetHeight, $this->TBAnswerSheetWidth, "book_report_edit", $Lang['ReadingGarden']['DoAnswerSheet'], "Get_Book_Report_Edit_Form(1,1,'".$thisInfo['ReadingRecordID']."','".$thisInfo['BookReportID']."','".$thisInfo['AssignedReadingID']."','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['DoAnswerSheet'], "");
							}
							
							if(!$thisInfo['OnlineWriting'] && ($thisInfo['WritingLimit'] && $thisInfo['WritingLimit']!=-1 || $thisInfo['LowerLimit'] && $thisInfo['LowerLimit']!=-1 ))
							{
								$Writing = $this->Get_Thickbox_Link($this->TBOnlineWritingHeight, $this->TBOnlineWritingWidth, "book_report_edit", $Lang['ReadingGarden']['OnlineWriting'], "Get_Book_Report_Edit_Form(2,1,'".$thisInfo['ReadingRecordID']."','".$thisInfo['BookReportID']."','".$thisInfo['AssignedReadingID']."','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['OnlineWriting'], "");
							}
														
							$Report = $ViewReport.$SubmitReport.$Answer.$Writing;
							if(trim($Report)=='')
								$Report = $Lang['General']['EmptySymbol'];
							
						}
						else
						{
							$trclass = "row_unread_book";
							$Report = $Lang['General']['EmptySymbol'];
							$ReadStatus = $Lang['ReadingGarden']['Unread'];
							$ReadingStatusLink = $this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_unread", $Lang['ReadingGarden']['AddReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisInfo['BookID'].",1,'','$CallBack');", "FakeLayer", $ReadStatus, "");
						}
						$FullBookTitle = $this->Format_Book_Display($thisInfo);
						
//						if(mb_strlen($FullBookTitle)>50)
//							$BookTitle = mb_substr($FullBookTitle,0,50)."...";
//						else
							$BookTitle = $FullBookTitle;
						
						$x.= '<tr class="'.$trclass.'" >'."\n";
						$x.= '<td>'."\n";
							$x.= '<div class="book_list_item reading_recommend_book_list">'."\n";
								$x.= '<div class="recommend_book_cover">'."\n";
									$x.= '<a href="javascript:js_View_Book_Detail('.$thisInfo['BookID'].');">'.$img.'</a>'."\n";
								$x.= '</div>'."\n";
								$x.= '<div class="recommend_book_detail">'."\n";
									$x.= '<h2>'.$BookTitle.'</h2>'."\n";
									$x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK, $thisInfo['BookID'], ' ');
	//								$x.= '<p class="spacer"></p>'."\n";
	//								$x.= '<p class="spacer"></p>'."\n";
								$x.= '</div>'."\n";
								$x.= '<p class="spacer"></p>'."\n";
							$x.= '</div>'."\n";
						$x.= '</td>'."\n";
							$x.= '<td>'.$ReadingStatusLink.'</td>'."\n";
							$x.= '<td>'.$Report.' </td>'."\n";
						$x.= '</tr>'."\n";
					}
				}
				else
				{
					$x .= '<tr class="row_unread_book"><td align="center" colspan="3"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
				}
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
			
		return $x;
	}

	function Get_Reading_Garden_Index_MyRecord_Area_Popular_Award_Scheme()
	{
		global $Lang, $ReadingGardenLib;
		
		$ScoreArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Student_Score($_SESSION['UserID']);
		$ScoreArr = BuildMultiKeyAssoc($ScoreArr, array("CategoryID"), "Score", 1);

		$StudentAward = $ReadingGardenLib->Get_Popular_Award_Scheme_Student_Award($_SESSION['UserID']);
		$AwardNameLang = Get_Lang_Selection("AwardNameCh", "AwardNameEn");
		
		$CategoryArr = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info();
		
//		$ThickBoxHeight = 350;
//		$ThickBoxWidth = 550;
		$NewBtn = '<div class="Conntent_tool">'."\n";
//			$NewBtn .= $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord'], "Get_Popular_Award_Scheme_Record_Add_Layer()", $InlineID="FakeLayer", $Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord']);
			$NewBtn .= $this->Get_Content_Tool_v30('new',"javascript:load_dyn_size_thickbox_ip('".$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord']."', 'Get_Popular_Award_Scheme_Record_Add_Layer(\'js_Load_MyRecord_Area(\\\\\'PopularAwardScheme\\\\\');\');');", $Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord'], "");
		$NewBtn .= '</div>'."\n";
		
		$x .= $NewBtn;
		
		$x.= '<table class="mybook_record_table">'."\n";
			$x .= '<col>';
			$x .= '<col>';
		
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Score'].'</th>'."\n";
				$x.= '</tr>'."\n";
				
				$SizeOfCategory = sizeof($CategoryArr);
				$CatNameLang = Get_Lang_Selection("CategoryNameCh", "CategoryNameEn");
				$TotalScore = 0;
				for($i=0; $i<$SizeOfCategory; $i++)
				{
					$thisScore = $ScoreArr[$CategoryArr[$i]['CategoryID']]?$ScoreArr[$CategoryArr[$i]['CategoryID']]:0;
					$x.= '<tr>'."\n";
						$x.= '<td>'.Get_String_Display($CategoryArr[$i][$CatNameLang]).'</td>'."\n";
						$x.= '<td>'.$thisScore.'</td>'."\n";
					$x.= '</tr>'."\n";
					
					$TotalScore += $thisScore;
				}
				if($StudentAward[$_SESSION['UserID']])
				{
					$AwardTitle= " (".$Lang['ReadingGarden']['PopularAwardSchemeArr']['Award'].")";
					$AwardDisplay = " (".$StudentAward[$_SESSION['UserID']][$AwardNameLang].")";
				}
				$x.= '<tr>'."\n";
					$x.= '<td>'.$Lang['ReadingGarden']['Total'].$AwardTitle.'</td>'."\n";
					$x.= '<td>'.$TotalScore.$AwardDisplay.'</td>'."\n";
				$x.= '</tr>'."\n";
				
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		
		return $x;
	}

	function Get_Reading_Garden_Index_Announcement_Area()
	{
		global $PATH_WRT_ROOT,$image_path, $LAYOUT_SKIN, $ReadingGardenLib, $Lang;
		
		$x.= '<div class="reading_board" id="reading_award_anncoucement">'."\n";
			# Announcement title
			$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
				$x.= '<h1><span>'.$Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'].'</span></h1>'."\n";
				$x.= '<a href="./announcement/index.php" class="reading_more">'.$Lang['ReadingGarden']['More'].'</a>'."\n";
			$x.= '</div></div>'."\n";
			
			$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
				
				# Announcement Tab
				$x.= '<div class="thumb_list_tab"><a id="AnnouncementTab1" href="javascript:void(0);" onclick="js_Select_Announcement_Type(1);" title="Announcement"><span><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/reading_scheme/icon_anncouce_arrow.gif" border=0;></span></a><em>|</em><a id="AnnouncementTab2" href="javascript:void(0);" onclick="js_Select_Announcement_Type(2);" title="Award"><span><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/reading_scheme/icon_anncouce_award.gif" border=0;></span></a><em>|</em><a id="AnnouncementTab" href="javascript:void(0);" onclick="js_Select_Announcement_Type(\'\');" class="thumb_list_tab_on"><span>'.$Lang['ReadingGarden']['All'].'</span></a></div>'."\n";
				$x.= '<input type="hidden" id="AnnouncementType" value="">'."\n";
				$x.= '<p class="spacer"></p>'."\n";
				
				# Announcement List
				$list_class =  $ReadingGardenLib->isStudent?"reading_annoucement_list":"reading_annoucement_list_teacher";
				$x.= '<div id="reading_annoucement_list" class="'.$list_class.'">'."\n";
				$x.= '</div>'."\n";
				
				$x.= '<p class="spacer">&nbsp;</p>'."\n";
			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
		$x.= '</div>'."\n";

		
		return $x;
				
	}
	
	function Get_Reading_Garden_Index_Announcement_List($ClassID='',$AnnouncementType='')
	{
		global $image_path, $LAYOUT_SKIN, $ReadingGardenLib, $Lang;
		
		if(trim($ClassID)=='')
		{
			$ClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($_SESSION['UserID']);
			$ClassID = $ClassInfo[0]['YearClassID'];
		}
		$AnnouncementList = $ReadingGardenLib->Get_Class_Announcement($ClassID,'',1, $AnnouncementType);
		
		$maxNoOfDisplay = $ReadingGardenLib->isStudent?5:10;
		$SizeOfAnnouncement = min(sizeof($AnnouncementList),$maxNoOfDisplay);
		if($SizeOfAnnouncement==0)
		{
			$x .= $Lang['General']['NoRecordAtThisMoment'];
		}
		else
		{
			for($i=0; $i<$SizeOfAnnouncement; $i++)
			{
				$thisAnnouncement = $AnnouncementList[$i];
				
				$FileName = '';
				$AttList = $ReadingGardenLib->Get_Announcement_Attachment($thisAnnouncement['Attachment'],1);
				$FileName = $AttList[0].(count($AttList)>1?"...":"");
				
				$DaysAgo = Convert_Date_To_DaysAgo($thisAnnouncement['StartDate']);
				if($thisAnnouncement['AwardSchemeID']) // is award
				{
					$AnnounceClass = "aw";
					$OnClick = 'js_View_Award('.$thisAnnouncement['AwardSchemeID'].')';	
				}
				else
				{
					$AnnounceClass = "an";
					$OnClick = 'js_View_Announcement('.$thisAnnouncement['AnnouncementID'].')';	
				}
				
				$x.= '<div class="announce_list_'.$AnnounceClass.'">'."\n";
					$x.= '<a href="javascript:void(0);" onclick="'.$OnClick.'" class="announce_list_'.$AnnounceClass.'_title">'.$thisAnnouncement['Title'].'<p class="spacer"></p>'."\n";
					if($FileName)
						$x.= '<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/icon_attachment.gif" align="absmiddle" border="0">'.$FileName."\n";
					$x.= '<span class="date_time">'.$DaysAgo.' '.$Lang['ReadingGarden']['DaysAgo'].'</span> </a>'."\n";
				$x.= '</div>'."\n";
			}
		}
		
		return $x;
	}
	
	function Get_Announcement_Layer($AnnouncementID)
	{
		global $ReadingGardenLib, $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $intranet_root;
		$AnnouncementInfo = $ReadingGardenLib->Get_Announcement_Info($AnnouncementID);
		$thisAnnouncementInfo = $AnnouncementInfo[0];
		
		$AttList = $ReadingGardenLib->Get_Announcement_Attachment($thisAnnouncementInfo['Attachment']);
		global $fs;
		$AttachPath = $intranet_root.$ReadingGardenLib->AnouncementAttachmentPath."/".$thisAnnouncementInfo["Attachment"];
		foreach((array)$AttList as $thisFile)
		{
			$AttName = $fs->get_file_basename($thisFile);
			$target_e = getEncryptedText($AttachPath.'/'.$AttName);
			//$AttLink[] = '<a href="http://'.$_SERVER['HTTP_HOST'].'/home/download_attachment.php?target='.urlencode($AttachPath.'/'.$AttName).'" target="_blank"><img border="0" align="absmiddle" src="'."{$image_path}/{$LAYOUT_SKIN}".'/icon_attachment.gif">'.$AttName.'</a>';
			$AttLink[] = '<a href="http://'.$_SERVER['HTTP_HOST'].'/home/download_attachment.php?target_e='.$target_e.'" target="_blank"><img border="0" align="absmiddle" src="'."{$image_path}/{$LAYOUT_SKIN}".'/icon_attachment.gif">'.$AttName.'</a>';
		}
		$thisAttachment = implode(", ",(array)$AttLink);
		
		$x.= '<div class="edit_pop_board award_pop_board">'."\n";
			$x.= '<div class="edit_pop_board_write award_pop_board_write">'."\n";
				$x.= '<div class="award_detail_title"><span>'.$thisAnnouncementInfo['Title'].' </span></div>'."\n";
				$x.= '<p class="spacer"></p>'."\n";
				$x.= '<div class="announce_detail_info">'."\n";
					$x.= '<table class="form_table_v30 form_table_thickbox">'."\n";
						$x.= '<tbody>'."\n";
							$x.= '<tr>'."\n";
								$x.= '<td class="field_title">'.$Lang['ReadingGarden']['StartDate'].'</td>'."\n";
								$x.= '<td>'.$thisAnnouncementInfo['StartDate'].'</td>'."\n";
							$x.= '</tr>'."\n";
							$x.= '<tr>'."\n";
								$x.= '<td class="field_title">'.$Lang['ReadingGarden']['PostedBy'].'</td>'."\n";
								$x.= '<td>'.$thisAnnouncementInfo['PostedBy'].'</td>'."\n";
							$x.= '</tr>'."\n";
							if(count($AttList)>0)
							{
								$x.= '<tr>'."\n";
									$x.= '<td class="field_title">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
									$x.= '<td>'.$thisAttachment.'</td>'."\n";
								$x.= '</tr>'."\n";
							}
						$x.= '</tbody>'."\n";
					$x.= '</table>'."\n";
					$x .= '<div style="line-height:normal">';
						$x .= $thisAnnouncementInfo['Message'];
					$x .= '</div>';
				$x.= '</div>'."\n";
			$x.= '</div>'."\n";
		$x.= '</div>'."\n";

		

		return $x;
	}
	
	function Get_Award_Layer($AwardSchemeID)
	{
		global $ReadingGardenLib, $Lang, $image_path, $LAYOUT_SKIN;
		$AwardList = $ReadingGardenLib->Get_Award_Scheme_List($AwardSchemeID);
		$AwardSchemeRequirement = $ReadingGardenLib->Get_Award_Scheme_Requirement('',false,$AwardSchemeID);
		$AwardSchemeRequirement = BuildMultiKeyAssoc($AwardSchemeRequirement, "AwardLevelID", '',0,1);
		
		
		foreach((array)$AwardSchemeRequirement as $thisLevelID => $LevelCatRequirement)
		{
			$RequirementList = '';
			$LevelRequirement = $LevelCatRequirement[0]; 
			$LevelMinBookRead = $LevelRequirement['LevelMinBookRead'];
			$LevelMinReportSubmit = $LevelRequirement['MinReportSubmit'];
			$LevelAwardType = $LevelRequirement['AwardType'];
			
			if($LevelRequirement['RequirementID'])
			{
				$RequirementList .= $Lang['ReadingGarden']['MinBookRequiredInSpecificCategory'].'<br>'."\n";
				foreach($LevelCatRequirement as $ReqInfo)
				{
					$RequirementList .= $ReqInfo['CategoryCode'].' '.$ReqInfo['CategoryName'].': '.$ReqInfo['MinBookRead'].'<br>';
				}
			}

			$ImgSrc = $LevelAwardType==AWARD_TYPE_GENERAL?($image_path.'/'.$LAYOUT_SKIN.'/reading_scheme/icon_anncouce_award.gif'):($image_path.'/'.$LAYOUT_SKIN.'/ediscipline/icon_merit.gif');
			$lv .= '<div><img align="absmiddle" src="'.$ImgSrc.'">'.$LevelRequirement['LevelName'].' [<a href="javascript:void(0);" onclick="js_View_Award_Level_Layer('.$thisLevelID.');">'.$Lang['ReadingGarden']['Detail'].'</a>]</div>';
			$lv .= '<div id="Layer'.$thisLevelID.'" class="LevelLayer" style="display:none; border:1px solid #EFEFEF;">';
			$lv .= $Lang['ReadingGarden']['MinBookRequired'].': '.($LevelMinBookRead?$LevelMinBookRead:$Lang['General']['EmptySymbol']).'<br>'."\n";
			$lv .= $Lang['ReadingGarden']['MinReportRequired'].': '.($LevelMinReportSubmit?$LevelMinReportSubmit:$Lang['General']['EmptySymbol']).'<br>'."\n";
			$lv .= $RequirementList."\n";
			$lv .= '</div>';
			
			
		}
//		foreach((array)$AwardSchemeRequirement as $EachCat)
//		{
//			$RequirementList .= $EachCat['CategoryCode'].' '.$EachCat['CategoryName'].': '.$EachCat['MinBookRead'].'<br>';
//		}
		
		$AwardType = $AwardList[0]['AwardType'];
		
		$x .= '<div class="edit_pop_board award_pop_board">'."\n";
			$x .= '<div class="edit_pop_board_write award_pop_board_write">'."\n";
				$x .= '<div class="award_detail_title"><span>'.$AwardList[0]['AwardName'].'</span></div>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= '<div class="award_detail_info">'."\n";
					$x .= '<table class="form_table_v30 form_table_thickbox">'."\n";
						$x .= '<tbody>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Type'].'</td>'."\n";
								$x .= '<td>'.$this->Get_String_Display($Lang['ReadingGarden']['AwardTypeName'][$AwardType]).'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Target'].'</td>'."\n";
								$x .= '<td>'.$this->Get_String_Display($AwardList[0]['Target'])."\n";
								$x .= '<p class="spacer"></p></td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Period'].'</td>'."\n";
								$x .= '<td> '.$AwardList[0]['StartDate'].' '.$Lang['ReadingGarden']['To'].' '.$AwardList[0]['EndDate']."\n";
								$x .= '<p class="spacer"></p></td>'."\n";
							$x .= '</tr>'."\n";
							if(trim($AwardList[0]['Description'])!='')
							{
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>'."\n";
									$x .= '<td> '.$AwardList[0]['Description']."\n";
									$x .= '<p class="spacer"></p></td>'."\n";
								$x .= '</tr>'."\n";
							}
							if(trim($AwardList[0]['Attachment'])!='')
							{
								$FileList = Get_FileList_By_FolderPath($AwardList[0]['Attachment']);
								foreach($FileList as $thisFile)
								{
									$DownloadURL = Get_Attachment_Download_Link($AwardList[0]['Attachment'], $thisFile[0]);
									$linkArr []= '<a href="'.$DownloadURL.'">'.$this->Get_Attachment_Icon().$thisFile[0].'</a>';
								}
								$link = implode(", ", (array)$linkArr);
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
									$x .= '<td> '.$link."\n";
									$x .= '<p class="spacer"></p></td>'."\n";
								$x .= '</tr>'."\n";
							}

							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AwardLevel'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $lv;
									$x .= '<p class="spacer"></p>';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td class="field_title">評審�������td>'."\n";
//								$x .= '<td>'."\n";
//									$x .= '��總����������������������\n";
//								$x .= '</td>'."\n";
//							$x .= '</tr>'."\n";
						$x .= '</tbody>'."\n";
					$x .= '</table>'."\n";
				$x .= '</div>'."\n";
				
				#Prize
				$ResultPublishDate = $AwardList[0]['ResultPublishDate'];
				if(time()>=strtotime($ResultPublishDate))
				{
					$WinnerList = $ReadingGardenLib->Get_Award_Scheme_Winner_List($AwardList[0]['AwardSchemeID']);
					$WinnerIDArr = Get_Array_By_Key($WinnerList, "WinnerID");
					$WinnerAwardList = BuildMultiKeyAssoc($WinnerList,"WinnerID",array("LevelName", "AwardType"),1,1);
					
					if(count($WinnerIDArr)>0)
					{
						$ReadingReportCount = $ReadingGardenLib->Get_Reading_Report_Count_In_Award_Scheme_Period($AwardList[0]['AwardSchemeID']);
//						sortByColumn2($ReadingReportCount,"RecordCount",1,1);
						
						if($AwardType == AWARD_SCHEME_TYPE_INDIVIDUAL)
						{
							$StudentNameLang = Get_Lang_Selection("ChineseName","EnglishName");
							$ClassNameLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
							
							$StudentClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($WinnerIDArr);
							$StudentClassInfoArr = BuildMultiKeyAssoc($StudentClassInfo, "UserID");
	
							$WinnerTable .= '<table class="normal_table_list">'."\n";
								$WinnerTable .= '<col width="35%">'."\n";
								$WinnerTable .= '<col width="5%">'."\n";
								$WinnerTable .= '<col width="5%">'."\n";
								$WinnerTable .= '<col>'."\n";
								$WinnerTable .= '<tbody>'."\n";
								
									$ReadIcon = '<a  class="book_status_read thickbox" title="'.$Lang['ReadingGarden']['BookRead'].'">&nbsp;</a>';
									$ReportIcon = '<a  class="book_report_done thickbox" title="'.$Lang['ReadingGarden']['ReportSubmit'].'">&nbsp;</a>';
									$WinnerTable .= '<tr>'."\n";
										$WinnerTable .= '<td>'.$Lang['ReadingGarden']['Student'].'</td>'."\n";
										$WinnerTable .= '<td>'.$ReadIcon.'</td>'."\n";
										$WinnerTable .= '<td>'.$ReportIcon.'</td>'."\n";
										$WinnerTable .= '<td>'.$Lang['ReadingGarden']['AwardLevel'].'</td>'."\n";
									$WinnerTable .= '</tr>'."\n";						
									foreach((array)$StudentClassInfoArr as $thisStudentID => $thisClassInfo)
									{
										$WinnerTable .= '<tr valign="center">'."\n";
											$WinnerTable .= '<td>'.$thisClassInfo[$StudentNameLang].' ('.$thisClassInfo[$ClassNameLang].' - '.$thisClassInfo['ClassNumber'].')'.'</td>'."\n";
											$WinnerTable .= '<td>'.$ReadingReportCount[$thisStudentID]['RecordCount'].'</td>'."\n";
											$WinnerTable .= '<td>'.$ReadingReportCount[$thisStudentID]['BookReportCount'].'</td>'."\n";
//											<div><img align="absmiddle" src="/images/2009a/reading_scheme/icon_anncouce_award.gif">easy job [<a onclick="js_View_Award_Level_Layer(27);" href="javascript:void(0);">詳細資���</a>]</div>
											$WinnerTable .= '<td>'."\n";  
												foreach($WinnerAwardList[$thisStudentID] as $thisPrize)
												{
													$AwardImgSrc = $thisPrize['AwardType']==AWARD_TYPE_GENERAL?($image_path.'/'.$LAYOUT_SKIN.'/reading_scheme/icon_anncouce_award.gif'):($image_path.'/'.$LAYOUT_SKIN.'/ediscipline/icon_merit.gif');
													$WinnerTable .= '<div><img align="absmiddle" src="'.$AwardImgSrc.'">'.$thisPrize['LevelName'].'</div>'."\n";
												}
											$WinnerTable .= '</td>'."\n";
										$WinnerTable .= '</tr>'."\n";
									}
								$WinnerTable .= '</tbody>'."\n";
							$WinnerTable .= '</table>'."\n";	
						}
						else
						{
							$ClassNameLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
							
							$WinnerTable .= '<table class="normal_table_list">'."\n";
								$WinnerTable .= '<tbody>'."\n";
									$WinnerTable .= '<tr>'."\n";
										$WinnerTable .= '<td>'.$Lang['ReadingGarden']['Class'].'</td>'."\n";
										$WinnerTable .= '<td>'.$Lang['ReadingGarden']['BookRead'].'</td>'."\n";
										$WinnerTable .= '<td>'.$Lang['ReadingGarden']['ReportSubmit'].'</td>'."\n";
										$WinnerTable .= '<td>'.$Lang['ReadingGarden']['LevelName'].'</td>'."\n";
									$WinnerTable .= '</tr>'."\n";						
									foreach((array)$WinnerIDArr as $thisClassID)
									{
										include_once("form_class_manage.php");
										$year_class = new year_class($thisClassID);
										$ClassName = Get_Lang_Selection($year_class->ClassTitleB5,$year_class->ClassTitleEN);
										
										$WinnerTable .= '<tr>'."\n";
											$WinnerTable .= '<td>'.$ClassName.'</td>'."\n";
											$WinnerTable .= '<td>'.$ReadingReportCount[$thisClassID]['RecordCount'].'</td>'."\n";
											$WinnerTable .= '<td>'.$ReadingReportCount[$thisClassID]['BookReportCount'].'</td>'."\n";
											$WinnerTable .= '<td>'.$WinnerAwardList[$thisClassID].'</td>'."\n";
										$WinnerTable .= '</tr>'."\n";
									}
								$WinnerTable .= '</tbody>'."\n";
							$WinnerTable .= '</table>'."\n";
						}
					}
				}
				
				if(empty($WinnerTable))
				{
					$WinnerTable = $Lang['ReadingGarden']['AwardWinnerNotPublish'];
				}
				$x .= '<div class="award_detail_prize">'."\n";
					$x .= '<div class="award_detail_prize_board">'."\n";
						$x .= '<h1><img align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/reading_scheme/icon_anncouce_award.gif"> '.$Lang['ReadingGarden']['WinnerList'].'</h1>'."\n";
							
//							$x .= '<table class="normal_table_list">'."\n";
//								$x .= '<tbody>'."\n";
//									$x .= '<tr>'."\n";
//										$x .= '<td>'.$Lang['ReadingGarden']['Student'].'</td>'."\n";
//										$x .= '<td>'.$Lang['ReadingGarden']['BookRead'].'</td>'."\n";
//										$x .= '<td>'.$Lang['ReadingGarden']['ReportSubmit'].'</td>'."\n";
//									$x .= '</tr>'."\n";
									$x .= $WinnerTable;
//									$x .= '<tr>'."\n";
//										$x .= '<td>Chan Siu Ming</td>'."\n";
//										$x .= '<td>50</td>'."\n";
//										$x .= '<td>100</td>'."\n";
//									$x .= '</tr>'."\n";
//									$x .= '<tr>'."\n";
//										$x .= '<td>Chan Siu Ming</td>'."\n";
//										$x .= '<td>50</td>'."\n";
//										$x .= '<td>100</td>'."\n";
//									$x .= '</tr>'."\n";
//									$x .= '<tr>'."\n";
//										$x .= '<td>Chan Siu Ming</td>'."\n";
//										$x .= '<td>50</td>'."\n";
//										$x .= '<td>100</td>'."\n";
//									$x .= '</tr>'."\n";
//								$x .= '</tbody>'."\n";
//							$x .= '</table>'."\n";
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
			$x .= '</div>'."\n";
		$x .= '</div>'."\n";
		
		return $x;

	}
	
	function Get_Reading_Garden_Index_Forum_Area($ClassID='')
	{
		global $ReadingGardenLib, $Lang;
		
		$TopicSummary = $ReadingGardenLib->Get_Forum_Topic(NULL,NULL,NULL, $ClassID, 1, 1);	

		$x.= '<div class="reading_board" id="reading_forum">'."\n";
			$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
				$x.= '<h1><span>'.$Lang['ReadingGarden']['Forum'].'</span></h1>'."\n";
				$x.= '<a class="reading_more" href="./forum/forum.php">'.$Lang['ReadingGarden']['More'].'</a>'."\n";
			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
//				$x.= '<div class="table_row_tool"><a href="#" class="add" title="Add Annoucement"></a></div>'."\n";
				$x.= '<p class="spacer"></p>'."\n";
					$x.= '<table class="reading_forum_table">'."\n";
						$x.= '<tbody>'."\n";
						if(count($TopicSummary)>0)
						{
							$NumOfTopic = min(count($TopicSummary),10);
							for($i=0; $i<$NumOfTopic; $i++)
							{
								$x.= '<tr>'."\n";
									$x.= '<td><a href="./forum/post.php?ForumID='.$TopicSummary[$i]['ForumID'].'&ForumTopicID='.$TopicSummary[$i]['ForumTopicID'].'">'.$TopicSummary[$i]['TopicSubject'].'</a></td>'."\n";
									$x.= '<td>'.$TopicSummary[$i]['PostCount'].'</td>'."\n";
								$x.= '</tr>'."\n";
							}
						}
						else
						{
								$x.= '<tr>'."\n";
									$x.= '<td><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td>'."\n";
								$x.= '</tr>'."\n";
						}
						$x.= '</tbody>'."\n";
					$x.= '</table>'."\n";
			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
		$x.= '</div>'."\n";
	
		return $x;	
	}

	function Get_Reading_Garden_Index_Recommend_Area($ClassID='')
	{
		global $ReadingGardenLib, $Lang;

		$ReadingGardenLib->Cache_Book_Info();
		$ReadingGardenLib->Cache_Function_Like_Record();
		
//		if(trim($ClassID)=='')
//		{
//			$ClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($_SESSION['UserID']);
//			$Par['YearID'] = $ClassInfo[0]['YearID'];
//		}
//		else
//		{
			include_once("form_class_manage.php");
			$year_class = new year_class($ClassID);
			$Par['YearID'] = $year_class->YearID;
//		}
		
		$RecommendBookList = $ReadingGardenLib->Get_Recommend_Book_Mappings($Par);
		$ReccomendBookIDArr = Get_Array_By_Key($RecommendBookList,"BookID");
		
		$RecommendReportArea = $this->Get_Index_Recommend_Book_Report_List();
		$RecommendBookArea = $this->Get_Index_Recommend_Book_List($ReccomendBookIDArr,$ClassID);
		$NewRecommendBook = $this->Get_Index_Recommend_Book_New_Book($ReccomendBookIDArr);
		
		$x.= '<div class="reading_board" id="reading_recommend">'."\n";
			$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
				$x.= '<h1><span>'.$Lang['ReadingGarden']['Recommend'].'</span></h1>'."\n";
			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
				$x.= '<div class="reading_recommend_content">'."\n";
				
					$x.= '<div id="reading_recommend_report" class="reading_recommend_report">'."\n";
						$x .= $RecommendReportArea;
					$x .= '</div>';
					$x.= '<div class="reading_recommend_book">'."\n";
						$x .= $RecommendBookArea;
					$x .= '</div>';
					$x.= '<div class="reading_recommend_new">'."\n";
						$x .= $NewRecommendBook;
					$x .= '</div>';

					$x.= '<p class="spacer"></p>'."\n";
				$x.= '</div>'."\n";

			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
		$x.= '</div>'."\n";

		return $x;
				
	}
	
	function Get_Index_Recommend_Book_Report_List()
	{
		global $Lang, $ReadingGardenLib;
		
		$ReportList = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade('','',1);
		
			$x.= '<h1> <span><b>'.$Lang['ReadingGarden']['BookReport'].'</b></span>'."\n";
				$x.= '<a href="./recommend_report/student_recommend_book_report.php?clearCoo=1" class="reading_more">'.$Lang['ReadingGarden']['More'].'</a>'."\n";
				$x.= '<p class="spacer"></p>'."\n";
			$x.= '</h1>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
			
			$UserIDArr = Get_Array_By_Key($ReportList,"UserID");
			$StudentInfo = $ReadingGardenLib->Get_Student_ClassInfo($UserIDArr);
			$StudentInfoAssoc = BuildMultiKeyAssoc($StudentInfo,"UserID");
			
			if(count($ReportList)>0)
			{
				$NoOfReport = min(count($ReportList),2);
				for($i=0; $i<$NoOfReport; $i++)
				{
					$thisReportInfo = $ReportList[$i];
					
					$ReadingRecordID =  $thisReportInfo['ReadingRecordID'];
					$StudentInfo = $StudentInfoAssoc[$thisReportInfo['UserID']];
					
					$StdNameLang = Get_Lang_Selection("ChineseName","EnglishName");
					
					$x.= '<div class="reading_recommend_report_list">'."\n";
						$x.= '<h2>'.$this->Format_Book_Display($thisReportInfo).'</h2>'."\n";
						$x.= '<div class="recommend_report_detail">'."\n";
							$x.= '<h3><a href="javascript:void(0);" onclick="js_View_Mark_Report('.$ReadingRecordID.',1)">'.$Lang['ReadingGarden']['BookReport'].'</a></h3>'."\n";
							$x.= '<p class="spacer">&nbsp;</p>'."\n";
							$x.= '<em>'.$StudentInfo[$StdNameLang].'</em>'."\n";
							$x.= '<p class="spacer">&nbsp;</p>'."\n";
							$x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK_REPORT,$thisReportInfo['BookReportID']);
							$x.= '<p class="spacer"></p>'."\n";
						$x.= '</div>'."\n";
						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</div>'."\n";				
				}
			}
			else
			{
				$x .= $Lang['General']['NoRecordAtThisMoment']; 
			}

			$x.= '<p class="spacer"></p>'."\n";
		
		return $x;
	}
	
	function Get_Index_Recommend_Book_List($ReccomendBookIDArr, $ClassID='')
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
			$x.= '<h1> <span><b>'.$Lang['ReadingGarden']['Book'].'</b></span>'."\n";
			$x.= '<a href="recommend_book/student_recommend_book.php?clearCoo=1&ClassID='.$ClassID.'" class="reading_more">'.$Lang['ReadingGarden']['More'].'</a>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
			$x.= '</h1>'."\n";
			
			if(count($ReccomendBookIDArr)>1)
			{
				for($i=1; $i<4 && $i<count($ReccomendBookIDArr); $i++)
				{
					$thisBookID = $ReccomendBookIDArr[$i];
					$thisBookInfo = $ReadingGardenLib->Cache_Book_Info_Arr[$thisBookID];
	//				if($thisBookInfo['BookCoverImage'])
	//					$thisImg = '<img src="'.$ReadingGardenLib->BookCoverImagePath.'/'.$thisBookInfo['BookCoverImage'].'" width="45px" height="65px">'."\n";
					$thisImg= $ReadingGardenLib->Get_Book_Cover_Image($thisBookInfo['BookCoverImage'],45,65);
					$BookName = $this->Format_Book_Display($thisBookInfo);
					
					$x.= '<div class="reading_recommend_book_list">'."\n";
						$x.= '<div class="recommend_book_cover"><a href="javascript:js_View_Book_Detail('.$thisBookID.');">'.$thisImg.'</a></div>'."\n";
						$x.= '<div class="recommend_book_detail">'."\n";
							$x.= '<h2>'.$BookName.'</h2>'."\n";
							$x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK,$thisBookID);
							$x.= '<div>'."\n";
								$x.= '<p class="spacer"></p>'."\n";
							$x.= '</div>'."\n";
							$x.= '<p class="spacer"></p>'."\n";
						$x.= '</div>'."\n";
						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</div>'."\n";
					$x.= '<p class="spacer"></p>'."\n";
				}
			}
			else
			{
				$x.= '<div class="reading_recommend_book_list">'."\n";
//					$x.= '<div class="recommend_book_cover"><a href="#">'.$thisImg.'</a></div>'."\n";
					$x.= '<div class="recommend_book_detail">'."\n";
						$x .= $Lang['General']['NoRecordAtThisMoment'];
						$x.= '<div>'."\n";
							$x.= '<p class="spacer"></p>'."\n";
						$x.= '</div>'."\n";
						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</div>'."\n";
					$x.= '<p class="spacer"></p>'."\n";
				$x.= '</div>'."\n";
				$x.= '<p class="spacer"></p>'."\n";
				
			}
		
		return $x;
	}
	
	function Format_Book_Display($thisBookInfo, $nolink=0)
	{
		$BookName = intranet_htmlspecialchars($thisBookInfo['BookName']);
		if($thisBookInfo['Author'])
			$BookName .= ' ['.intranet_htmlspecialchars($thisBookInfo['Author']).']';
		
		if($nolink==1)
			return $BookName;
		else
		{
			$x .= '<a href="javascript:js_View_Book_Detail('.$thisBookInfo['BookID'].');" title="'.$BookName.'">';
				$x .= $BookName;
			$x .= '</a>';	
				
			return $x;
		}
	}
	
	function Get_Index_Recommend_Book_New_Book($ReccomendBookIDArr)
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
		if(count($ReccomendBookIDArr)>0)
		{
			$thisBookID = $ReccomendBookIDArr[0];
			$thisBookInfo = $ReadingGardenLib->Cache_Book_Info_Arr[$thisBookID];
			
			$BookName = $this->Format_Book_Display($thisBookInfo);
	//		if($thisBookInfo['BookCoverImage'])
	//			$thisImg = '<img src="'.$ReadingGardenLib->BookCoverImagePath.'/'.$thisBookInfo['BookCoverImage'].'" width="120px" height="183px" style="border:0px">'."\n";
			$thisImg= $ReadingGardenLib->Get_Book_Cover_Image($thisBookInfo['BookCoverImage'],120,183);
			
				$x.= '<div class="reading_recommend_new_content">'."\n";
					$x.= '<div class="new_book_detail">'."\n";
						$x.= '<a href="javascript:js_View_Book_Detail('.$thisBookID.');">'.$thisImg.'</a>'."\n";
						$x.= '<h2>'.$BookName.'</h2>'."\n";
//						$x.= '</a>'."\n";
					$x.= '</div>'."\n";
					$x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK,$thisBookID);
				$x.= '</div>'."\n";
			
		}
		else
		{
				$x.= '<div class="reading_recommend_new_content">'."\n";
					$x.= '<div class="new_book_detail">'."\n";
						$x .= $Lang['General']['NoRecordAtThisMoment'];
					$x.= '</div>'."\n";
				$x.= '</div>'."\n";
			
		}
		
		return $x;
	}
	
	function Get_Like_Comment_Div($FunctionName, $FunctionID, $Other='')
	{
		
		$x.= '<div class="like_comment '.$FunctionName.'_'.$FunctionID.'_like_div" '.$Other.'>'."\n";
			$x .= $this->Get_Like_Comment_Div_Content($FunctionName, $FunctionID);
		$x.= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Like_Comment_Div_Content($FunctionName, $FunctionID)
	{
		global $Lang, $ReadingGardenLib;
		
		if($FunctionName==FUNCTION_NAME_BOOK_REPORT)
		{
			$NoOfComment = count($ReadingGardenLib->Get_Book_Report_Comment_List($FunctionID));
			if($NoOfComment>0)
				$NoOfComment = $NoOfComment.' ';
			else
				$NoOfComment = '';
			$comment = " | ".'<a href="javascript:void(0)" onclick="js_Add_Comment(\''.$FunctionName.'\',\''.$FunctionID.'\')">'.$NoOfComment.' '.$Lang['ReadingGarden']['Comment'].'</a>';
		}
		
		$LikeRecord = $ReadingGardenLib->Get_Function_Like_Record($FunctionName);
		$BookLikeAssoc = BuildMultiKeyAssoc($LikeRecord,array("FunctionID","UserID"));
		
		$Num = count($BookLikeAssoc[$FunctionID]);
		if($BookLikeAssoc[$FunctionID][$_SESSION['UserID']])
		{
			$Like = '<a href="javascript:void(0)" onclick="js_Unlike(\''.$FunctionName.'\',\''.$FunctionID.'\')">'.$Lang['ReadingGarden']['Unlike'].'</a>';
			$YouAnd = $Lang['ReadingGarden']['You'] ;
			$NumDisplay= $Num-1;
			if($NumDisplay>0)
				$Other = ' &amp; '.'<a href="javascript:void(0);" onmouseover="js_Open_Like_List(this,\''.$FunctionName.'\',\''.$FunctionID.'\')" onmouseout="js_Hide_Like_List()">'.$NumDisplay.'</a> ';
		}
		else
		{
			$Like = '<a href="javascript:void(0)" onclick="js_Like(\''.$FunctionName.'\',\''.$FunctionID.'\')">'.$Lang['ReadingGarden']['Like'].'</a>';
			$NumDisplay = $Num;
			if($NumDisplay>0)
				$Other = '<a href="javascript:void(0);" onmouseover="js_Open_Like_List(this,\''.$FunctionName.'\',\''.$FunctionID.'\')" onmouseout="js_Hide_Like_List()">'.$NumDisplay.'</a> ';
		}
		
		$x.= $Like.$comment."\n";
		$x.= '<p class="spacer"></p>'."\n";
		if($Num>0)
			$x.= '<span class="like_num">'.$YouAnd.$Other.' '.$Lang['ReadingGarden']['likes'] .'</span>'."\n";
		
		return $x;
	}
	
	function Get_Like_List_Layer($FunctionName, $FunctionID)
	{
		global $ReadingGardenLib, $PATH_WRT_ROOT, $Lang;
		
		$LikeRecord = $ReadingGardenLib->Get_Function_Like_Record($FunctionName);
		$BookLikeAssoc = BuildMultiKeyAssoc($LikeRecord,array("FunctionID","UserID"));
		
		$ParUserID = array_keys((array)$BookLikeAssoc[$FunctionID]);
		$ParUserID = array_diff($ParUserID,(array)$_SESSION['UserID']);
		
		include_once($PATH_WRT_ROOT."/includes/libuser.php");
		$luser = new libuser('','',$ParUserID);
		
		foreach((array)$ParUserID as $thisUserID)
		{
			$luser->LoadUserData($thisUserID);
			$UserName[] = $luser->StandardDisplayName;
		}
		$UserNameStr = implode(', ',$UserName);
		$x .= $UserNameStr.' '.$Lang['ReadingGarden']['AlsoLikeThis'];
		
		return $x;
	}
	
	function Get_Student_Record_UI($CurrentTab='',$field='', $order='', $pageNo='', $numPerPage='')
	{
		global $Lang, $sys_custom;
		
		$Navigation[] = array($Lang['ReadingGarden']['Home'],"../");
		$Navigation[] = array($Lang['ReadingGarden']['MyRecord'],"");
		
		$x .= '<form name="form1" id="form1" method="post">';
			$x.= $this->GET_NAVIGATION_IP25($Navigation,"navigation_2")."\n";
			$x.= '<p class="spacer"></p><p class="spacer"></p>'."\n";
			$x.= '<div id="reading_myrecord" class="reading_board">'."\n";
				$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
					$x.= '<h1>'.$Lang['ReadingGarden']['MyRecord'].'</h1>'."\n";
				$x.= '</div></div>'."\n";
				$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
						$x.= '<div class="reading_myrecord_conent_detail">'."\n";
							$x.= '<div class="reading_mybook_detail">'."\n";
								$x.= '<div id="reading_mybook_record" class="reading_board">'."\n";
									$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
										$x.= '<div class="mybook_record_tab">'."\n";
											$x.= '<ul>'."\n";
//												$x.= '<li id=""><a href="reading_scheme_student_myrecord_state.htm">My State</a></li>'."\n";
												$x.= '<li id="ReadingTargetTab"><a href="javascript:void(0);" onclick="js_Reload_Student_Record_Tab(\'ReadingTarget\');">'.$Lang['ReadingGarden']['ReadingTarget'].'</a></li>'."\n";
												$x.= '<li id="AssignedReadingTab"><a href="javascript:void(0);" onclick="js_Reload_Student_Record_Tab(\'AssignedReading\');">'.$Lang['ReadingGarden']['AssignedBook'].'</a></li>'."\n";
												$x.= '<li id="ReadingRecordTab"><a href="javascript:void(0);" onclick="js_Reload_Student_Record_Tab(\'ReadingRecord\');">'.$Lang['ReadingGarden']['MyReadingBook'].'</a></li>'."\n";
												if($sys_custom['ReadingGarden']['PopularAwardScheme']){
													$x.= '<li id="PopularAwardSchemeTab"><a href="javascript:void(0);" onclick="js_Reload_Student_Record_Tab(\'PopularAwardScheme\');">'.$Lang['ReadingGarden']['PopularAwardScheme'].'</a></li>'."\n";
												}
												
											$x.= '</ul>'."\n";
										$x.= '</div>'."\n";
										$x.= '<p class="spacer"></p>'."\n";
									$x.= '</div></div>'."\n";
									$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
										$x .= '<div id="MyRecordDiv"></div>';
									$x.= '</div></div>'."\n";
									$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
									$x.= '<p class="spacer"></p>'."\n";
								$x.= '</div>'."\n";
							$x.= '</div>'."\n";
							$x.= '<p class="spacer">&nbsp;</p>'."\n";
						$x.= '</div>'."\n";
						$x.= '<p class="spacer">&nbsp;</p>'."\n";
					$x.= '</div></div>'."\n";
				$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
				$x.= '<p class="spacer"></p>'."\n";
			$x.= '</div>'."\n";
			$x .= '<input type="hidden" name="CurrentTab" id="CurrentTab" value="'.$CurrentTab.'">';
			$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
			$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$numPerPage.'" />';	
			$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" id="order" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" id="field" name="field" value="'.$field.'" />';
		$x .= '</form>';
		
		
		
		return $x;
	}
	
	function Get_My_Record_Assigned_Reading_UI($ReadingStatusArr='')
	{
		$x.= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<td valign="bottom">'."\n";
						$x .= $this->Get_Reading_Status_Filter('js_Apply_Reading_Status()',$ReadingStatusArr);
						
						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</td>'."\n";
					$x.= '<td valign="bottom">'."\n";
					$x.= '</td>'."\n";
				$x.= '</tr>'."\n";
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		
		$x .= $this->Get_My_Record_Assigned_Reading_Table($ReadingStatusArr);
		
		return $x;
	}
	
	function Get_My_Record_Assigned_Reading_Table($ReadingStatusArr='')
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
		$AssignedReadingList = $ReadingGardenLib->Get_Index_Assigned_Reading_List('',$ReadingStatusArr,0);

		$x.= '<table class="mybook_record_table">'."\n";
			$x .= '<col style="width:3px;">';
			$x .= '<col style="width:65%;">';
			$x .= '<col>';
			$x .= '<col>';
		
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<th>#</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['Book'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['ReadingStatus'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['BookReport'].'</th>'."\n";
				$x.= '</tr>'."\n";
				
				if(sizeof($AssignedReadingList)>0)
				{
					$numRec = sizeof($AssignedReadingList);

					$TBReadingRecordHeight = "400";
					$TBReadingRecordWidth = "500";
					$TBBookReportFileHeight = "200";
					$TBBookReportFileWidth = "500";	
					
					for($i=0; $i<$numRec; $i++)
					{
						$thisInfo  = $AssignedReadingList[$i];
						
//						$img = '<img style="float:left;" width="45px" height="54px" src="'.$PATH_WRT_ROOT.$ReadingGardenLib->BookCoverImagePath."/".$thisInfo['BookCoverImage'].'" >';
						$img= $ReadingGardenLib->Get_Book_Cover_Image($thisInfo['BookCoverImage'],45,54);
						
						$CallBack = "js_Reload_Student_Record_Tab()";
						if($thisInfo['RecordStatus'])
						{
							$SubmitReport = '';
							$ViewReport = '';
							$Answer = '';
							$Writing = '';
							$Report = '';
							$trclass = "row_read_book";
							$ReadStatus = $Lang['ReadingGarden']['Read'];
							$ReadingStatusLink = '<div onmouseover="View_Reading_Record(this,\''.$thisInfo['ReadingRecordID'].'\')" onmouseout="Remove_Reading_Record_Layer()">'.$this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_read", $Lang['ReadingGarden']['EditReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisInfo['BookID'].",0,'".$thisInfo['ReadingRecordID']."','$CallBack');", "FakeLayer", $ReadStatus, "").'</div>';
							
							if($thisInfo['Attachment'] || $thisInfo['Answer'] || $thisInfo['OnlineWriting'])
							{
								$ViewReport = '<a class="book_report_done" href="javascript:void(0);" onclick="js_View_Mark_Report('.$thisInfo['ReadingRecordID'].');">'.$Lang['ReadingGarden']['ViewBookReport'].'</a>';
							}
							
							if(!$thisInfo['Attachment'] && $thisInfo['BookReportRequired'])
							{
								$SubmitReport = $this->Get_Thickbox_Link($this->TBBookReportFileHeight, $this->TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'".$thisInfo['ReadingRecordID']."','".$thisInfo['BookReportID']."','".$thisInfo['AssignedReadingID']."','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
							}
							
							if(!$thisInfo['Answer'] && $thisInfo['AnswerSheet'])
							{
								$Answer = $this->Get_Thickbox_Link($this->TBAnswerSheetHeight, $this->TBAnswerSheetWidth, "book_report_edit", $Lang['ReadingGarden']['DoAnswerSheet'], "Get_Book_Report_Edit_Form(1,1,'".$thisInfo['ReadingRecordID']."','".$thisInfo['BookReportID']."','".$thisInfo['AssignedReadingID']."','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['DoAnswerSheet'], "");
							}
							
							if(!$thisInfo['OnlineWriting'] && $thisInfo['WritingLimit']!=-1)
							{
								$Writing = $this->Get_Thickbox_Link($this->TBOnlineWritingHeight, $this->TBOnlineWritingWidth, "book_report_edit", $Lang['ReadingGarden']['OnlineWriting'], "Get_Book_Report_Edit_Form(2,1,'".$thisInfo['ReadingRecordID']."','".$thisInfo['BookReportID']."','".$thisInfo['AssignedReadingID']."','$CallBack');", "FakeLayer", $Lang['ReadingGarden']['OnlineWriting'], "");
							}
							
							$Report = $ViewReport.$SubmitReport.$Answer.$Writing;
							if(trim($Report)=='')
								$Report = $Lang['General']['EmptySymbol'];
							
						}
						else
						{
							$trclass = "row_unread_book";
							$Report = $Lang['General']['EmptySymbol'];
							$ReadStatus = $Lang['ReadingGarden']['Unread'];
							$ReadingStatusLink = $this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_unread", $Lang['ReadingGarden']['AddReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisInfo['BookID'].",1,'','$CallBack');", "FakeLayer", $ReadStatus, "");
						}
						
						$FullBookTitle = $this->Format_Book_Display($thisInfo);
						$BookTitle = $FullBookTitle;
						
						$x.= '<tr class="'.$trclass.'" >'."\n";
							$x.= '<td>'.($i+1).'</td>'."\n";
							$x.= '<td>'."\n";
								$x.= '<div class="book_list_item reading_recommend_book_list">'."\n";
									$x.= '<div class="recommend_book_cover " >'."\n";
										$x.= '<a href="javascript:js_View_Book_Detail('.$thisInfo['BookID'].');">'.$img.'</a>'."\n";
									$x.= '</div>'."\n";
									$x.= '<div class="recommend_book_detail">'."\n";
										$x.= '<h2>'.$FullBookTitle.'</h2>'."\n";
										$x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK, $thisInfo['BookID'], ' ');
									$x.= '</div>'."\n";
									$x.= '<p class="spacer"></p>'."\n";
								$x.= '</div>'."\n";
							$x.= '</td>'."\n";
							$x.= '<td>'.$ReadingStatusLink.' </td>'."\n";
							$x.= '<td>'.$Report.' </td>'."\n";
						$x.= '</tr>'."\n";
					}
				}
				else
				{
					$x .= '<tr class="row_unread_book"><td align="center" colspan="4"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
				}
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
			
		return $x;
	}

	function Get_My_Record_My_Reading_Book_UI($field='', $order='', $pageNo='', $numPerPage='',$ReadingStatusArr='', $AcademicYearID='')
	{
		global $Lang, $ReadingGardenLib;
	
		$ThickBoxHeight = 505;
		$ThickBoxWidth = 750;
		# Acadermic Year Selection  
		    
		if (!isset($AcademicYearID) || $AcademicYearID==""){
			$AcademicYearID = Get_Current_Academic_Year_ID();
		}	
		$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="js_Reload_Student_Record_Tab();"', 1, 0, $AcademicYearID, 1);
 
		
		# Add Btn
		$thisOnclick="js_Add_Reading_Record()";
//		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['AddReadingRecord'], $thisOnclick, $InlineID="FakeLayer", $Lang['Btn']['New']);
		$NewBtn = $this->Get_Content_Tool_v30('new','new_reading_record.php',$Lang['ReadingGarden']['AddReadingRecord']);

		$x.= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<td valign="bottom">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .= $NewBtn."\n";
						$x .= '</div>'."\n";
						$x.= '<p class="spacer"></p>'."\n";
				
						# Academic Year Filter Display										
						$x .= '<div class="table_filter">'."\n";						
							$x .= $yearFilter . "\n";
				        $x .= '</div>'."\n";
				        						
//						$x .= $this->Get_Reading_Status_Filter('js_Apply_Reading_Status()',$ReadingStatusArr);
//						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</td>'."\n";
					$x.= '<td valign="bottom">'."\n";
					$x.= '</td>'."\n";
				$x.= '</tr>'."\n";
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		
		$x .= $this->Get_My_Record_My_Reading_Book_DBTable($field, $order, $pageNo, $numPerPage,$ReadingStatusArr, $AcademicYearID);
		
		return $x;
	}
	
	function Get_My_Record_My_Reading_Book_DBTable($field='', $order='', $pageNo='', $numPerPage='',$ReadingStatusArr='', $AcademicYearID='')
	{
		global $Lang, $page_size, $ck_my_record_reading_record_page_size, $ReadingGardenLib;
		
		$ReadingGardenUI = $this;
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 5 : $field;
		$order = ($order==='')? 0 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;

		if (isset($ck_my_record_reading_record_page_size) && $ck_my_record_reading_record_page_size != "") $page_size = $ck_my_record_reading_record_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_My_Record_My_Reading_Book_DBTable_Sql($AcademicYearID);
		
		$li->sql = $sql;
		$li->IsColOff = "ReadingScheme_MyReadingBook";
			
		$li->field_array = array("b.BookName","b.Author","b.Publisher","bc.CategoryName","rr.StartDate","rr.FinishedDate","rr.NumberOfHours","br.BookReportID");
		$li->column_array = array(18,18,18,18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Book'])."</th>\n";
//		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['ReadingStatus'])."</td>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Author'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Publisher'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Category'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['StartDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['FinishedDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['ReadingHours'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Report'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("BookIDArr[]")."</td>\n";

		$li->no_col = $pos+1;
	
		$x .= '<div class="common_table_tool">'."\n";
	        $x .= '<a class="tool_edit" href="javascript:checkEditMultiple(document.form1,\'BookIDArr[]\',\'../recommend_book/student_recommend_book.php?clearCoo=1\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
			$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'BookIDArr[]\',\'js_Delete_Reading_Record();\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
		$x .= '</div>'."\n";		
	
		$x .= $li->display();
	
		return $x;		
	}
	
	function Get_Reading_Status_Filter($CallBack='', $CheckedArr='')
	{
		global $Lang;
		
//		$x.= '<!--# Filter layer start #-->'."\n";
//		$x.= '<div class="table_filter">'."\n";
//			$x.= '<div class="selectbox_group selectbox_group_filter"> <a onclick="MM_showHideLayers(\'status_option\',\'\',\'show\')" href="javascript:void(0);">'.$Lang['ReadingGarden']['SelectTitleArr']['Status'].'</a> </div>'."\n";
//			$x.= '<p class="spacer"></p>'."\n";
//			$x.= '<div class="selectbox_layer selectbox_group_layer" id="status_option">'."\n";
//				foreach(array(READING_STATUS_READ,READING_STATUS_UNREAD) as $ReadingStatus)
//				{
//					$Checked = (trim($CheckedArr)==''||in_array($ReadingStatus, (array)$CheckedArr))?"checked":"";
//					$x.= '<input id="ReadingStatus'.$ReadingStatus.'" type="checkbox" '.$Checked.' class="ReadingStatus" name="ReadingStatus[]" value='.$ReadingStatus.'>'."\n";
//					$x.= '<label for="ReadingStatus'.$ReadingStatus.'">'.$Lang['ReadingGarden']['ReadingStatusArr'][$ReadingStatus].'</label><br>'."\n";
//				}
//				$x.= '<p class="spacer"></p>'."\n";
//				$x.= '<div class="edit_bottom">'."\n"; # js_Check_Reading_Status_Filter placed in reading_garden/reading_scheme.js
//					$x.= $this->Get_Small_Btn($Lang['Btn']['Apply'],"button","js_Check_Reading_Status_Filter('$CallBack'); ")."&nbsp;";
//					$x.= $this->Get_Small_Btn($Lang['Btn']['Cancel'],"button","MM_showHideLayers('status_option','','hide')");
//				$x.= '</div>'."\n";
//			$x.= '</div>'."\n";
//		$x.= '</div>'."\n";
//		$x .= '<script>var jsWarnSelectReadingStatus = "'.$Lang['ReadingGarden']['WarningMsg']['PleaseSelectReadingStatus'].'"</script>';
//		$x.= '<!--# Filter layer end #-->'."\n";
		 
		$oprionArr[] = array(READING_STATUS_READ,$Lang['ReadingGarden']['ReadingStatusArr'][READING_STATUS_READ]);
		$oprionArr[] = array(READING_STATUS_UNREAD,$Lang['ReadingGarden']['ReadingStatusArr'][READING_STATUS_UNREAD]); 
		$x = getSelectByArray($oprionArr, " id='ReadingStatusSelect' name='ReadingStatus[]' onchange='$CallBack' ",$CheckedArr,1,0,$Lang['ReadingGarden']['SelectTitleArr']['AllReadingStatus']);
		 
		return $x;
	}
	
	function Get_My_Record_Reading_Target_DBTable($field='', $order='', $pageNo='', $numPerPage='')
	{
		global $Lang, $page_size, $ck_my_record_reading_target_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 0 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;

		if (isset($ck_my_record_reading_target_page_size) && $ck_my_record_reading_target_page_size != "") $page_size = $ck_my_record_reading_target_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_My_Record_Reading_Target_DBTable_Sql();
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
			
		$li->field_array = array("rt.StartDate, rt.EndDate","ReadingEn","ReportEn","ReadingCh","ReportCh","Target");
		$li->column_array = array(18,18,18,18);
		$li->wrap_array = array(0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Period'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['EnglishReadingTarget'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['EnglishReportTarget'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['ChineseReadingTarget'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['ChineseReportTarget'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['OtherReadingTarget'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['AccomplishRate'])."</th>\n";

		$li->no_col = $pos+1;
		$li->no_navigation_row = true;
		
//		$IndicationArr[] = array("normal",$Lang['ReadingGarden']['Indication']['Normal']);
		$IndicationArr[] = array("avaliable",$Lang['ReadingGarden']['Indication']['InProgress']); 
		$x .= $this->GET_RECORD_INDICATION_IP25($IndicationArr);
		$x .= $li->display("","common_table_list_v30");

//		debug_pr($li->built_sql());
	
		return $x;		
	}

	function Get_My_Record_Popular_Award_Scheme_UI($field='', $order='', $pageNo='', $numPerPage='')
	{
		global $Lang, $ReadingGardenLib;
	
		$ThickBoxHeight = 505;
		$ThickBoxWidth = 750;
		
		$NewBtn = '<div class="Conntent_tool">'."\n";
			$NewBtn .= $this->Get_Content_Tool_v30('new',"javascript:load_dyn_size_thickbox_ip('".$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord']."', 'Get_Popular_Award_Scheme_Record_Add_Layer(\'js_Reload_Student_Record_Tab();\');');", $Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord'], "");
		$NewBtn .= '</div>'."\n";

		$x.= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<td valign="bottom">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .= $NewBtn."\n";
						$x .= '</div>'."\n";
						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</td>'."\n";
					$x.= '<td valign="bottom">'."\n";
					$x.= '</td>'."\n";
				$x.= '</tr>'."\n";
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		
		$x .= $this->Get_My_Record_Popular_Award_Scheme_DBTable($field, $order, $pageNo, $numPerPage);
		
		return $x;
	}

	function Get_My_Record_Popular_Award_Scheme_DBTable($field='', $order='', $pageNo='', $numPerPage='')
	{
		global $Lang, $page_size, $ck_my_record_popular_award_scheme_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 3 : $field;
		$order = ($order==='')? 0 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;

		if (isset($ck_my_record_popular_award_scheme_page_size) && $ck_my_record_popular_award_scheme_page_size != "") $page_size = $ck_my_record_popular_award_scheme_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_My_Record_Popular_Award_Scheme_DBTable_Sql();
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$EnCh = Get_Lang_Selection("Ch", "En");
		$NameLang = getNameFieldByLang("iu.");
			
		$li->field_array = array("i.ItemDesc$EnCh", "ic.CategoryName$EnCh", "$NameLang", "sim.DateInput");
		$li->column_array = array(18,18,18,18);
		$li->wrap_array = array(0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='40%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['PopularAwardSchemeArr']['Item'])."</th>\n";
		$li->column_list .= "<th width=''>".$li->column_IP25($pos++, $Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['General']['InputBy'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['General']['DateInput'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("StudentItemID[]")."</th>\n";

		$li->no_col = $pos+2;
		
		
		$x .= '<div class="common_table_tool">'."\n";
			$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'StudentItemID[]\',\'js_Delete_Popular_Award_Scheme_Record();\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
		$x .= '</div>'."\n";		
		
		$x .= $li->display("","common_table_list_v30");

//		debug_pr($li->built_sql());
	
		return $x;		
	}
	
	function Get_Reading_Garden_Index_Teacher_Record_Area($ClassID='')
	{
		global $Lang, $ReadingGardenLib;
		
		$x.= '<div id="reading_myrecord" class="reading_board">'."\n";
			$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
				$x.= '<h1>'.$Lang['ReadingGarden']['StudentRecord'].'</h1>'."\n";
			$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_left">'."\n";
				$x.= '<div class="reading_board_right_teacher">'."\n";
					
					$x.= '<div id="reading_mybook_record" class="reading_board">'."\n";
							
							# 2012Sept: show reading target period
							$CurrentTargetObj  = $ReadingGardenLib->Get_Class_Reading_Target_Mapping("",$ClassID, true);
							if (sizeof($CurrentTargetObj)>0)
							{
								$ReadingTarget_Period = " (".$CurrentTargetObj[0]["StartDate"]." ~ ".$CurrentTargetObj[0]["EndDate"].")";
							}

							#Tab
							$x.= '<div class="reading_board_top_left"><div class="reading_board_top_right">'."\n";
								$x.= '<div class="mybook_record_tab">'."\n";
									$x.= '<ul>'."\n";
									$x.= '<li id="ReadingTargetTab"><a href="javascript:void(0);" onclick="js_Load_Teacher_View_Area(\'ReadingTarget\')">'.$Lang['ReadingGarden']['ReadingTarget'].' '.$ReadingTarget_Period.'</a></li>'."\n";
									$x.= '<li id="StudentStateTab"><a href="javascript:void(0);" onclick="js_Load_Teacher_View_Area(\'StudentState\')">'.$Lang['ReadingGarden']['StudentState'].'</a></li>'."\n";
//									$x.= '<li><a href="#">'.$Lang['ReadingGarden']['MyLike'].'</a></li>'."\n";
									$x.= '</ul>'."\n";
								$x.= '</div>'."\n";
								$x.= '<span id="moreBtn"><a class="reading_more" href="student_reading_record_list.php">'.$Lang['ReadingGarden']['More'].'</a></span>'."\n";
								$x.= '<span id="classExportBtn"><a onclick="document.form_class.submit()" style="padding-right:20px" class="reading_more" href="javascript:void(0)">'.$Lang['ReadingGarden']['ClassExport'].'</a></span>'."\n";
								$x.= '<span id="schExportBtn"><a onclick="document.school_class.submit()" style="padding-right:20px" class="reading_more" href="javascript:void(0)">'.$Lang['ReadingGarden']['SchoolExport'].'</a></span>'."\n";
								$x.= '<p class="spacer"></p>'."\n";
							$x.= '</div></div>'."\n";
							
							# Content
							$x.= '<div class="reading_board_left"><div class="reading_board_right">'."\n";
								$x .= '<div id="MyRecord_Area"></div>'."\n";
							$x.= '</div></div>'."\n";
							
						$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
						$x.= '<p class="spacer"></p>'."\n";
					$x.= '</div>'."\n";

					$x.= '<p class="spacer">&nbsp;</p>'."\n";
				$x.= '</div></div>'."\n";
			$x.= '<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
		$x.= '</div>'."\n";
		$x .= '<input type="hidden" id="CurrentTab" value="">';	
		
		return $x;
	}
	
	function Get_Reading_Garden_Index_Teacher_View_Student_State($ClassID='')
	{
		global $ReadingGardenLib, $Lang;
		
		include_once("form_class_manage.php");
		$year_class = new year_class($ClassID,true,false,true);
		$StudentIDArr = Get_Array_By_Key($year_class->ClassStudentList, "UserID");
		
		$StudentInfoArr = $ReadingGardenLib->Get_Student_ClassInfo($StudentIDArr);
		$StudentAssoc = BuildMultiKeyAssoc($StudentInfoArr, "UserID");
		$StudentNameLang = Get_Lang_Selection("ChineseName","EnglishName");
						
		if(count($ReadingGardenLib->Settings)>0)
			$StudentStateAssoc = $ReadingGardenLib->Get_Student_State($StudentIDArr);
		
		$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
			$x .= '<col width="15%">'."\n";
			$x .= '<col width="35%">'."\n";
			$x .= '<col>'."\n";
			$x .= '<col>'."\n";
		
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
				$x .= '<th>'.$Lang['Identity']['Student'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['State'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['Score'].'</th>'."\n";
			$x .= '</tr>'."\n";
			if(count($StudentStateAssoc)==0)
			{
				$x .= '<tr><td colspan=4 align="center"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
			}
			else
			{
				foreach((array)$StudentStateAssoc as $thisStudentID => $StateInfo)
				{
					$thisStudentInfo = $StudentAssoc[$thisStudentID];
					if(!$thisStudentInfo) continue; // skip students who is deleted/ archived
					
					$x .= '<tr>'."\n";
						$x .= '<td>'.$thisStudentInfo['ClassNumber'].'</td>'."\n";
						$x .= '<td>'.$thisStudentInfo[$StudentNameLang].'</td>'."\n";
						$x .= '<td>'.$StateInfo['StateName'].'</td>'."\n";
						$x .= '<td>'.$StateInfo['Score'].'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			}
		$x .= '</table>'."\n";
		
		
		return $x;
	}
	
	function Get_Reading_Garden_Index_Teacher_View_Reading_Target_Import($ClassID,&$have_grade=true)
	{
		global $Lang, $ReadingGardenLib;
		
		include_once("form_class_manage.php");
		$year_class = new year_class($ClassID,true,false,true);
		$StudentIDArr = Get_Array_By_Key($year_class->ClassStudentList, "UserID");
		$StudentInfoArr = $ReadingGardenLib->Get_Student_ClassInfo($StudentIDArr);
		$StudentAssoc = BuildMultiKeyAssoc($StudentInfoArr, "UserID");
		$StudentNameLang = Get_Lang_Selection("ChineseName","EnglishName");
		$StudentNameLang2 = Get_Lang_Selection("EnglishName","ChineseName");
		$ClassTitleLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
		$ClassTitleLang2 = Get_Lang_Selection("ClassTitleEN","ClassTitleB5");
		
		$CurrentTargetObj  = current($ReadingGardenLib->Get_Class_Reading_Target_Mapping("",$ClassID, true));
				
		$StudentTargetArr = $ReadingGardenLib->Get_Student_Target_Reading_Count($ClassID,$CurrentTargetObj["ReadingTargetID"],1,'',$have_grade);
		$StudentTargetAssoc = BuildMultiKeyAssoc($StudentTargetArr, "UserID");		
		
		$result_arr = array();
			
		if(count($StudentTargetAssoc)>0)
		{	
			foreach((array)$StudentTargetAssoc as $thisStudentID => $TargetInfo)
			{
				$thisStudentInfo = $StudentAssoc[$thisStudentID];
				if(!$thisStudentInfo) continue; // skip students who is deleted/ archived
				
				$result_arr[] = array();
				$current_i = sizeof($result_arr) - 1;
				
				$result_arr[$current_i][0] = $thisStudentInfo[$ClassTitleLang] == "" ? $thisStudentInfo[$ClassTitleLang2] : $thisStudentInfo[$ClassTitleLang];
				$result_arr[$current_i][1] = $thisStudentInfo['ClassNumber'];
				$result_arr[$current_i][2] = $thisStudentInfo[$StudentNameLang] == "" ? $thisStudentInfo[$StudentNameLang2] : $thisStudentInfo[$StudentNameLang];
				$result_arr[$current_i][3] = $TargetInfo['Progress']."%";
				$result_arr[$current_i][4] = $TargetInfo['TotalReading'];
				$result_arr[$current_i][5] = $TargetInfo['TotalReportCh']+$TargetInfo['TotalReportEn'];
				if($have_grade)
					$result_arr[$current_i][6] = $TargetInfo['Grade'];
				else
					$result_arr[$current_i][6] = "--";
			}
		}
		
		return $result_arr;
	}
		
	function Get_Reading_Garden_Index_Teacher_View_Reading_Target($ClassID)
	{
		global $Lang, $ReadingGardenLib;
		
		include_once("form_class_manage.php");
		$year_class = new year_class($ClassID,true,false,true);
		$StudentIDArr = Get_Array_By_Key($year_class->ClassStudentList, "UserID");
		$StudentInfoArr = $ReadingGardenLib->Get_Student_ClassInfo($StudentIDArr);
		$StudentAssoc = BuildMultiKeyAssoc($StudentInfoArr, "UserID");
		$StudentNameLang = Get_Lang_Selection("ChineseName","EnglishName");
		
		$CurrentTargetObj  = current($ReadingGardenLib->Get_Class_Reading_Target_Mapping("",$ClassID, true));
		
		$StudentTargetArr = $ReadingGardenLib->Get_Student_Target_Reading_Count($ClassID,$CurrentTargetObj["ReadingTargetID"],1,'',$have_grade);
		$StudentTargetAssoc = BuildMultiKeyAssoc($StudentTargetArr, "UserID");		
		
		$x .= '<form action="export.php" id="form_class" name="form_class" style="display:none"><input value="'.$ClassID.'" name="ClassID"></form>';
		$x .= '<form action="export.php" id="school_class" name="school_class" style="display:none"></form>';
		
		$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
			$x .= '<col width="5%">'."\n";
			$x .= '<col width="25%">'."\n";
			$x .= '<col width="20%">'."\n";
			$x .= '<col width="20%">'."\n";
			$x .= '<col width="20%">'."\n";
			$x .= '<col width="20%">'."\n";
		
			$x .= '<tr>'."\n";
			$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
			$x .= '<th>'.$Lang['Identity']['Student'].'</th>'."\n";
			$x .= '<th><center>'.$Lang['ReadingGarden']['Progress'].'</center></th>'."\n";
			$x .= '<th><center>'.$Lang['ReadingGarden']['ReadingRecord'].'</center></th>'."\n";
			$x .= '<th><center>'.$Lang['ReadingGarden']['BookReport'].'</center></th>'."\n";
			if($have_grade)
				$x .= '<th><center>'.$Lang['ReadingGarden']['Score'].'</center></th>'."\n";
			$x .= '</tr>'."\n";
			
		if(count($StudentTargetAssoc)>0)
		{	
			foreach((array)$StudentTargetAssoc as $thisStudentID => $TargetInfo)
			{
				$thisStudentInfo = $StudentAssoc[$thisStudentID];
				if(!$thisStudentInfo) continue; // skip students who is deleted/ archived
				
//				$ReadStatus = $this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_read", $Lang['ReadingGarden']['EditReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisBookInfo['BookID'].",0,'".$thisReadingRecordInfo['ReadingRecordID']."','js_Search_Book();');", "FakeLayer", $ReadStatus, "");
					
				$x .= '<tr>'."\n";
					$x .= '<td>'.$thisStudentInfo['ClassNumber'].'</td>'."\n";
					$x .= '<td><a href="student_reading_record_list.php?StudentID='.$thisStudentID.'">'.$thisStudentInfo[$StudentNameLang].'</td>'."\n";
					$x .= '<td align="center">'.$TargetInfo['Progress'].'%</td>'."\n";
					$x .= '<td align="center">'.$TargetInfo['TotalReading'].'</td>'."\n";
					$x .= '<td align="center">'.($TargetInfo['TotalReportCh']+$TargetInfo['TotalReportEn']).'</td>'."\n";
					if($have_grade)
						$x .= '<td align="center">'.($TargetInfo['Grade']).'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		}
		else
		{
			$x .= '<tr>'."\n";
				$x .= '<td colspan="6" align="center"><br>'.$Lang['ReadingGarden']['NoCurrentReadingTarget'].'<br><br></td>'."\n";
			$x .= '</tr>'."\n";
		}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Teacher_View_Student_Reading_Record_UI($ClassID='', $StudentID='', $AcademicYearID='')
	{
		global $Lang;
		
		# Get Navigation
		$navigation_arr[] = array($Lang['Header']['Menu']['Home'],"./");
		$navigation_arr[] = array($Lang['ReadingGarden']['ReadingRecord'],"");
		
		# Student Selection
		include_once("form_class_manage_ui.php");
		$fcm_ui = new form_class_manage_ui();
		$StudentSelect = $fcm_ui->Get_Student_Selection("StudentID",$ClassID,$StudentID,"js_Reload_Student_Reading_Record()",1);
		
		# Acadermic Year Selection        
		if (!isset($AcademicYearID) || $AcademicYearID==""){
			$AcademicYearID = Get_Current_Academic_Year_ID();
		}	
		$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="js_Reload_Student_Reading_Record();"', 1, 0, $AcademicYearID, 1);
    
     	
		$x .= $this->GET_NAVIGATION_IP25($navigation_arr,"navigation_2");
		$x .= '<div class="table_filter">'."\n";
			$x .= $StudentSelect."\n";
			$x .= $yearFilter . "\n";
        $x .= '</div>'."\n";
		$x .= '<div class="table_board" id="ReadingRecordListDiv">'."\n";
			$x .= $this->Get_Ajax_Loading_Image();
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Teacher_View_Student_Reading_Record($StudentID, $ClassID='', $AcademicYearID='') 
	{
		global $Lang, $ReadingGardenLib, $image_path, $LAYOUT_SKIN;
		
		$ReadingRecordList = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade($StudentID,'','','','','','rr.FinishedDate DESC', $AcademicYearID);
		
		$CurrentReadingTarget = $ReadingGardenLib->Get_Class_Reading_Target_Mapping('',$ClassID,1);
		$TargetStartDate = $CurrentReadingTarget[0]['StartDate'];
		$TargetEndDate = $CurrentReadingTarget[0]['EndDate'];

		$IndicationArr[] = array("avaliable",$Lang['ReadingGarden']['InCurrentTarget']);

		$x .= '<br />';
		$x .= $this->GET_RECORD_INDICATION_IP25($IndicationArr);
		$x .= '<table class="common_table_list_v30">'."\n";
			$x .= '<col width="5%">'."\n";
			$x .= '<col width="30%">'."\n";
			$x .= '<col width="10%">'."\n";
			$x .= '<col width="10%">'."\n";
			$x .= '<col width="5%">'."\n";
			$x .= '<col width="5%">'."\n";
			$x .= '<col width="5%">'."\n";
			$x .= '<col width="25%">'."\n";
			$x .= '<col width="5%">'."\n";
		
			$x .= '<tr>'."\n";
				$x .= '<th class="num_check">#</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['Book'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['StartDate'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['FinishedDate'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['ReadingHours'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['Report'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['Grade'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['TeacherComment'].'</th>'."\n";
				$x .= '<th>'.$Lang['ReadingGarden']['Recommend'].'</th>'."\n";
			$x .= '</tr>'."\n";
		if(count($ReadingRecordList)>0)
		{	
			$TickImg = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_star_on.gif\"  border=\"0\" align=\"absmiddle\" />";
			foreach((array)$ReadingRecordList as $key => $ReadingRecordInfo)
			{
				$row_css = strtotime($TargetStartDate)<strtotime($ReadingRecordInfo['FinishedDate']) && strtotime($ReadingRecordInfo['FinishedDate']) <strtotime($TargetEndDate)?"row_avaliable":"";
				
				$ReportMarkingLink = $ReadingRecordInfo['BookReportID']?$this->GET_ACTION_LNK('javascript:void(0)',$Lang['ReadingGarden']['MarkReport'],"newWindow('book_report_marking.php?ReadingRecordID=".$ReadingRecordInfo['ReadingRecordID']."',31)","book_report_edit"):$Lang['General']['EmptySymbol'];
				
				$BookName = $this->Format_Book_Display($ReadingRecordInfo);
				$x .= '<tr class="'.$row_css.'">'."\n";
					$x .= '<td>'.($key+1).'</td>'."\n";
					$x .= '<td>'.$BookName.'</td>'."\n";
					$x .= '<td>'.$ReadingRecordInfo['StartDate'].'</td>'."\n";
					$x .= '<td>'.$ReadingRecordInfo['FinishedDate'].'</td>'."\n";
					$x .= '<td>'.$ReadingRecordInfo['NumberOfHours'].'</td>'."\n";
					$x .= '<td>'.$ReportMarkingLink.'</td>'."\n";
					$GradeEmptyNew = ($ReportMarkingLink!=$Lang['General']['EmptySymbol'] && trim($ReadingRecordInfo['Grade'])=="") ? "<img src=\"/images/2009a/ies/alert_new2.gif\" border='0' align='absmiddle' />" : $Lang['General']['EmptySymbol'];
					$x .= '<td>'.($ReadingRecordInfo['Grade']?$ReadingRecordInfo['Grade']:$GradeEmptyNew).'</td>'."\n";
					$x .= '<td>'.($ReadingRecordInfo['TeacherComment']?htmlspecialchars($ReadingRecordInfo['TeacherComment']):$Lang['General']['EmptySymbol']).'</td>'."\n";
					$x .= '<td>'.($ReadingRecordInfo['Recommend']?$TickImg:$Lang['General']['EmptySymbol']).'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		}
		else
		{
			$x .= '<tr>'."\n";
				$x .= '<td colspan="9" align="center"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td>'."\n";
			$x .= '</tr>'."\n";
		}
		$x .= '</table>'."\n";
		
		return $x;
	}
		
	function Get_My_Record_New_My_Reading_Book_Layer()
	{
		global $Lang, $ReadingGardenLib;
			
		# Step
		$StepsObj[] = array($Lang['ReadingGarden']['BookInfo'],1);
		$StepsObj[] = array($Lang['ReadingGarden']['ReadingStatus'],0);
		
		# Get Navigation
		$navigation_arr = array(
			array($Lang['Header']['Menu']['Home'],"../index.php"),
			array($Lang['ReadingGarden']['MyReadingBook'],"../student_record/index.php?CurrentTab=ReadingRecord"),
			array($Lang['ReadingGarden']['AddReadingRecord'],"")
		);
		
		# Get Book Source Settings
		$ClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($_SESSION['UserID']);
		$BookSourceSetting = $ReadingGardenLib->Settings_Get_Class_Book_Source($ClassInfo[0]['YearClassID']);
		$BookSource = $BookSourceSetting[0]['BookSource'];
		
		$x .= '';
		$x .= '<table width="98%" cellpadding=0px cellspacing=0px align="center">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td valign="top">'."\n";
					$x .= $this->GET_NAVIGATION_IP25($navigation_arr, "navigation_2");	
					$x .= $this->GET_STEPS_IP25($StepsObj);
					
					$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
						$x .= '<col class="field_title">';
						$x .= '<col class="field_c">';
						# Book Source
						$ExternalBookDisable = $BookSource==BOOK_REPORT_SOURCE_ANY_BOOK?"":"1";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['ReadingGarden']['BookSource'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->Get_Radio_Button("ExistingBook", "BookSource", 1, $isChecked=1, $Class='BookSource', $Lang['ReadingGarden']['ExistingBook'], "js_Change_Book_Source()").'&nbsp;'."\n";
								$x .= $this->Get_Radio_Button("ExternalBook", "BookSource", 2, $isChecked=0, $Class='BookSource', $Lang['ReadingGarden']['ExternalBook'], "js_Change_Book_Source()",$ExternalBookDisable)."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";

					$x .= '<form id="SearchResultForm" name="SearchResultForm">'."\n";
						$x .= '<div id="SearchResultDiv"></div>'."\n";
					$x .= '</form>'."\n";
					
					$x .= '<form id="ExistingBookForm" name="ExistingBookForm">'."\n";
						$x .= '<div id="ExistingBookDiv" >'."\n";
							$x .= $this->Get_My_Record_New_My_Reading_Book_Layer_Step1_Existing_Book();
						$x .= '</div>'."\n";
						$x .= '<input type="hidden" name="BookSource" value="'.$BookSource.'">';
					$x .= '</form>'."\n";
					if($BookSource==BOOK_REPORT_SOURCE_ANY_BOOK)
					{
						$x .= '<form id="ExternalBookForm" name="ExternalBookForm">'."\n";
							$x .= '<div id="ExternalBookDiv" style="display:none;">'."\n";
								$x .= $this->Get_My_Record_New_My_Reading_Book_Layer_Step1_External_Book();
							$x .= '</div>'."\n";
						$x .= '</form>'."\n";
					}
				
				
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		# hidden field
		
		$x .= '<iframe id="FileUploadFrame" name="FileUploadFrame" style="display:none;"></iframe>'."\n";
			
		return $x;
	}
	
	//20111013
	function Get_My_Record_New_My_Reading_Book_Layer_Step1_External_Book()
	{
		global $Lang, $ReadingGardenLib;

		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Save'],"button","js_Submit_TB_Form()");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'],"button","js_Back_To_Search()","BackToSearchBtn");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Back_To_My_Record()");
		
		$x .= $this->Get_Form_Sub_Title_v30($Lang['ReadingGarden']['BookInfo']);
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
				
				# Book Name
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['BookName'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="BookName" id="BookName" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankBookName",$Lang['ReadingGarden']['WarningMsg']['RequestInputBookName'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				
				if($ReadingGardenLib->IsMadatory("CallNumber")){
					$CallNumbermadatoryFieldSpan = '<span class="tabletextrequire">*</span>';
					$CallNumbermadatoryClass = 'Mandatory';
				}
			
				# Call Number
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$CallNumbermadatoryFieldSpan.$Lang['ReadingGarden']['CallNumber'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="CallNumber" id="CallNumber" class="textbox_name '.$CallNumbermadatoryClass.'" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankCallNumber",$Lang['ReadingGarden']['WarningMsg']['RequestInputBookCallNumber'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";

				
				# ISBN
				$madatoryFieldSpan = '';
				$madatoryClass = '';
				$ISBNWarningDiv = '';
				if($ReadingGardenLib->enableReadingSchemeEditISBNByStudentRight() || $ReadingGardenLib->IsMadatory("ISBN")){
					$madatoryFieldSpan = '<span class="tabletextrequire">*</span>';
					$madatoryClass = 'Mandatory';
					$ISBNWarningDiv = $this->Get_Thickbox_Warning_Msg_Div("WarnBlankISBN",$Lang['ReadingGarden']['WarningMsg']['RequestInputISBN'], "WarnMsgDiv");
				}
				
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">' . $madatoryFieldSpan .$Lang['ReadingGarden']['ISBN'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="ISBN" id="ISBN" type="text" class="textbox_name '.$madatoryClass.'" maxlength="'.$this->MaxCodeLength.'" />'."\n";
						$x .= $ISBNWarningDiv;
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				
				if($ReadingGardenLib->IsMadatory("Author")){
					$AuthormadatoryFieldSpan = '<span class="tabletextrequire">*</span>';
					$AuthormadatoryClass = 'Mandatory';
				}

				# Author
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$AuthormadatoryFieldSpan.$Lang['ReadingGarden']['Author'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="Author" id="Author" class="textbox_name '.$AuthormadatoryClass.'" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankAuthor",$Lang['ReadingGarden']['WarningMsg']['PleaseInputAuthor'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				if($ReadingGardenLib->IsMadatory("Publisher")){
					$PublishermadatoryFieldSpan = '<span class="tabletextrequire">*</span>';
					$PublishermadatoryClass = 'Mandatory';
				}
				
				# Publisher
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$PublishermadatoryFieldSpan.$Lang['ReadingGarden']['Publisher'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="Publisher" id="Publisher" class="textbox_name '.$PublishermadatoryClass.'" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankPublisher",$Lang['ReadingGarden']['WarningMsg']['PleaseInputPublisher'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Language
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Language'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Language_Selection("External_Book_Language",'',0,1,"","js_Reload_Category_Selection(); js_Reload_Award_Scheme_Selection();")."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Category
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Category'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span id="CategorySelectSpan">'.$this->Get_Ajax_Loading_Image().'</span>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Description
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_TEXTAREA("Description", $thisValue);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['CoverImage'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input type="file" name="BookCoverImage" id="BookCoverImage" class="textbox_name" value="'.$thisValue.'" >'."\n";
						$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankBookCoverImage",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBookCover'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
		$x .= '</table>'."\n";
		
		$x .= $this->Get_Form_Sub_Title_v30($Lang['ReadingGarden']['ReadingRecord']);
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			
			# Start Date
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StartDate'].'</td>'."\n";
				$x .= '<td>'.$this->GET_DATE_PICKER("StartDate",'','','','','','','','',1).''."\n";
					$x .= '<div id="StartDateWarningDiv" style="color:red;display:none"></div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# End Date
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['FinishedDate'].'</td>'."\n";
				$x .= '<td>'.$this->GET_DATE_PICKER("FinishedDate",'','','','','','','','',1).''."\n";
					$x .= '<div id="FinishedDateWarningDiv" style="color:red;display:none"></div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Reading Hours
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['ReadingHours'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<input name="ReadingHours" id="ReadingHours" type="text" class="textboxnum" maxlength="10" size="10" /> '.$Lang['ReadingGarden']['Hour(s)'].''."\n";
					$x .= '<div id="ReadingHoursWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumber'].'</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";

			# Award Scheme Selection
			if($ReadingGardenLib->Settings['RequireSelectAwardScheme'])
			{
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AwardScheme'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<span id="AwardSchemeSelectSpan">'.$this->Get_Ajax_Loading_Image().'</span>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>';
			}
				
		$x .= '</table>'."\n";

		# Customization [#2012-1009-1402-14054]
		if($ReadingGardenLib->enableReadingSchemeAddPopularDataTogetherRight())
		{		
			$x .= $this->Get_Poppular_Award_Scheme_Display_Table($StudentSubmit=1);
		}

		$x .= $this->MandatoryField();
			
		# Btns
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $SubmitBtn;
			$x .= "&nbsp;".$BackBtn;
			$x .= "&nbsp;".$CloseBtn;
		$x .= '</div>'."\n";		
		
		return $x;
	}
	
	function Get_My_Record_New_My_Reading_Book_Layer_Step1_Existing_Book()
	{
		global $Lang;
		
		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Search'],"button","js_Submit_TB_Form()");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'],"button","js_Back_To_Search()","BackToSearchBtn");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Back_To_My_Record()");

		$x .= $this->Get_Form_Sub_Title_v30($Lang['ReadingGarden']['BookInfo']);
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			
				# Language
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Language'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Language_Selection("Language",'',1,0)."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Category
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Category'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Category_Selection("CategoryID", "CategoryID","",$Lang['ReadingGarden']['SelectTitleArr']['AllCategory']," ");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Book Name
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['BookName'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="BookName" id="BookName" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Author
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Author'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="Author" id="Author" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Publisher
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Publisher'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="Publisher" id="Publisher" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Call Number
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['CallNumber'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="CallNumber" id="CallNumber" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
		$x .= '</table>'."\n";
		# Btns
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $SubmitBtn;
			$x .= "&nbsp;".$BackBtn;
			$x .= "&nbsp;".$CloseBtn;
		$x .= '</div>'."\n";	
		
		return $x;
	}	
	
	function Get_My_Record_New_My_Reading_Book_Search_Result($BookSource, $BookInfo='', $CategoryID='', $Language='')
	{
		global $ReadingGardenLib, $Lang;
		
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Select'],"button","js_Submit_TB_Form()");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'],"button","js_Back_To_Search()","BackToSearchBtn");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Back_To_My_Record()");
		
		$ValidBookList = $ReadingGardenLib->Get_New_My_Reading_Book_Valid_Book_List($BookSource, $BookInfo, $CategoryID, $Language);
		$StudentReadingRecord = $ReadingGardenLib->Get_Student_Reading_Record();
		$ReadingRecordInfo = BuildMultiKeyAssoc($StudentReadingRecord,"BookID");
		$ReadBookID = Get_Array_By_Key($StudentReadingRecord,"BookID");
		
//		$x .= '<div style="text-align:right;"><a href="javascript:void(0);" onclick="js_Back_To_Search()"> &lt;- '.$Lang['Btn']['Back'].'</a></div>';
		$x .= $this->Get_Form_Sub_Title_v30($Lang['ReadingGarden']['BookSearchResult']);
		$x.= '<table class="mybook_record_table">'."\n";
			$x.= '<col style="">'."\n";
			$x.= '<col style="width:40%">'."\n";
			$x.= '<col style="">'."\n";
			$x.= '<col style="">'."\n";
			$x.= '<col style="">'."\n";
			$x.= '<col style="">'."\n";
			$x.= '<thead>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<th>#</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['Book'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['Publisher'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['Category'].'</th>'."\n";
					$x.= '<th>'.$Lang['ReadingGarden']['ReadingStatus'].'</th>'."\n";
//					$x.= '<th>&nbsp;</th>'."\n";
					$x.= '<th class="num_check"><input type="checkbox" id="CheckAllBook" onclick="js_Check_All_Book(this.checked)"></th>'."\n";
				$x.= '</tr>'."\n";
			$x.= '</thead>'."\n";
			$x.= '<tbody>'."\n";
				$NumOfBook = sizeof($ValidBookList);
				if($NumOfBook>0)
				{
					for($i=0;$i<$NumOfBook; $i++)
					{
						$thisBookInfo = $ValidBookList[$i];

						if(in_array($thisBookInfo['BookID'],(array)$ReadBookID))
						{
//							$disabled = "disabled";
							$thisReadingRecordInfo = $ReadingRecordInfo[$thisBookInfo['BookID']];
							
							$ReadingStatusClass = "row_read_book";
							$ReadStatus = '<a class="book_status_read" >'.$Lang['ReadingGarden']['Read'].'</a>';
//							$ReadStatus = $this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_read", $Lang['ReadingGarden']['EditReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisBookInfo['BookID'].",0,'".$thisReadingRecordInfo['ReadingRecordID']."','js_Search_Book();');", "FakeLayer", $ReadStatus, "");
							
							$ReadingStatusLink = '<div onmouseover="View_Reading_Record(this,\''.$thisReadingRecordInfo['ReadingRecordID'].'\')" onmouseout="Remove_Reading_Record_Layer()">'.$ReadStatus.'</div>';
							if($thisBookInfo['Attachment'])
							{
								$Report = '<a class="book_report_done" target="_blank" href="'.$ReadingGardenLib->BookReportPath.$thisBookInfo['Attachment'].'">'.$Lang['ReadingGarden']['ViewBookReport'].'</a>';
							}
							else if($thisBookInfo['BookReportRequired'])
							{
//								$Report = '<a class="book_report_done" href="'.$ReadingGardenLib->BookReportPath.$thisInfo['Attachment'].'">'.basename($thisInfo['Attachment']).'</a>';
								$Report = $this->Get_Thickbox_Link($this->TBBookReportFileHeight, $this->TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'".$thisBookInfo['ReadingRecordID']."','".$thisBookInfo['BookReportID']."','".$thisBookInfo['AssignedReadingID']."','js_Search_Book()');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
							}
							else
								$Report = '-';
//							$ReadingStatus = '<a class="book_status_read" >'.$Lang['ReadingGarden']['Read'].'</a>';
						}
						else
						{
//							$disabled = "";
							$ReadingStatusClass = "row_unread_book";
								$Report = "-";
//							$ReadStatus = $Lang['ReadingGarden']['AddReadingRecord'];
							$ReadingStatusLink = '<a class="book_status_unread" >'.$Lang['ReadingGarden']['Unread'].'</a>';
//							$ReadingStatusLink = $this->Get_Thickbox_Link($this->TBReadingRecordHeight, $this->TBReadingRecordWidth, "book_status_unread", $Lang['ReadingGarden']['AddReadingRecord'], "Get_Reading_Record_Edit_Form(".$thisBookInfo['BookID'].",1,'','js_Search_Book()');", "FakeLayer", $ReadStatus, "");
//							$ReadingStatus = '<a class="book_status_unread" >'.$Lang['ReadingGarden']['Unread'].'</a>';
						}
						
						$BookName = $this->Format_Book_Display($thisBookInfo);
						$x.= '<tr class="'.$ReadingStatusClass.'">'."\n";
							$x.= '<td>'.($i+1).'</td>'."\n";
							$x.= '<td>'.$BookName.'</td>'."\n";
							$x.= '<td>'.($thisBookInfo['Publisher']?$thisBookInfo['Publisher']:$Lang['General']['EmptySymbol']).'</td>'."\n";
							$x.= '<td>'.($thisBookInfo['CategoryName']?$thisBookInfo['CategoryName']:$Lang['ReadingGarden']['UnclassifiedCategory']).'</td>'."\n";
							$x.= '<td>'.$ReadingStatusLink.'</td>'."\n";
							$x.= '<td><input type="checkbox" name="BookIDArr[]" value="'.$thisBookInfo['BookID'].'" '.$disabled.'></td>'."\n";
						$x.= '</tr>'."\n";
					}
				}
				else
				{
					$x .= '<tr><td align="center" colspan="6"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
				}

			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $SubmitBtn;
			$x .= "&nbsp;".$BackBtn;
			$x .= "&nbsp;".$CloseBtn;
		$x .= '</div>'."\n";	
		
		return $x;
	}
	
	/*function Get_My_Record_New_My_Reading_Book_Cover_View($BookIDArr)
	{
		global $ReadingGardenLib, $Lang,$image_path,$LAYOUT_SKIN ;
		$BookInfoArr = $ReadingGardenLib->Get_Book_List($BookIDArr);
		
		$x = '';
		$x .= "<table class='common_table_list_v30 $othercss1'>\n";
		$x .= "<thead>\n";
		$x .= "</thead>\n";
//		$x .= $this->displayColumn(); 
		
		$TBReadingRecordHeight = "400";
		$TBReadingRecordWidth = "500";
		$TBBookReportFileHeight = "200";
		$TBBookReportFileWidth = "500";
		$TBAnswerSheetHeight = "600";
		$TBAnswerSheetWidth = "800";
		
			foreach($BookInfoArr as $thisBookInfo) 
			{
				
				$i++;
				$css = ($i%2) ? "" : "2";
				
				# Book Info
				$book_id = $thisBookInfo['BookID'];
				$book_name = htmlspecialchars($thisBookInfo['BookName'], ENT_QUOTES);
				$author = htmlspecialchars($thisBookInfo['Author'],ENT_QUOTES);
				$call_number = htmlspecialchars($thisBookInfo['CallNumber'],ENT_QUOTES);
				$publisher = htmlspecialchars($thisBookInfo['Publisher'],ENT_QUOTES);
				$description = htmlspecialchars($thisBookInfo['Description'],ENT_QUOTES);
				$cover_image_path = trim($thisBookInfo['BookCoverImage'])==''?($image_path."/".$LAYOUT_SKIN."/10x10.gif"):($ReadingGardenLib->BookCoverImagePath."/".$thisBookInfo['BookCoverImage']);
				$category_name = $thisBookInfo['CategoryName'];
				$category_code = $thisBookInfo['CategoryCode'];
				$category = $category_name;
				if($category_code!='') $category .= " (".$category_code.")";
				$category = htmlspecialchars($category,ENT_QUOTES);
				
				# Reading Record
				$reading_record_id = trim($thisBookInfo['ReadingRecordID']);
				$book_report_id = trim($thisBookInfo['BookReportID']);
				$student_id = trim($thisBookInfo['StudentID']);
				
				# Assigned Reading 
				$assigned_reading_id = trim($thisBookInfo['AssignedReadingID']);
				// need assigned target > 0
				$assigned_reading_target_cnt = $thisBookInfo['NumOfTarget'];
				// if have assigned attachment, need to upload book report
				$assigned_attachment = trim($thisBookInfo['AssignedAttachment']);
				// if have assigned answer sheet, need to do answer sheet 
				$assigned_answer_sheet = trim($thisBookInfo['AssignedAnswerSheet']);
				
				
				# Answer sheet / Report
				// Book default answer sheet
				$default_answer_sheet = trim($thisBookInfo['DefaultAnswerSheet']);
				// book report answer sheet answer
				$report_answer_sheet = trim($thisBookInfo['AnswerSheet']);
				// uploaded book report file
				$book_report_attachment = trim($thisBookInfo['Attachment']);
				
				$TBAddReadingRecordButton = '';
				$ReadingRecordReaded = '';
				$TBEditReadingRecordButton = '';
				$RemoveReadingRecordButton = '';
				$HandinBookReportButton = '';
				$DoAnswerSheetButton = '';
				$EditAnswerSheetButton = '';
				$ViewAnswerSheetButton = '';
				$DeleteAnswerSheetButton = '';
				$BookReportFileLink = '';
				$RemoveBookReportFileButton = '';
				
				if($reading_record_id == ''){// Reading record not submitted yet
					$TBAddReadingRecordButton = $this->Get_Thickbox_Link($TBReadingRecordHeight, $TBReadingRecordWidth, "book_status_unread", $Lang['ReadingGarden']['AddReadingRecord'], "Get_Reading_Record_Edit_Form($book_id,1,'');", "FakeLayer", $Lang['ReadingGarden']['AddReadingRecord'], "");
				}else{// Submitted reading record
					$ReadingRecordReaded = "<a class=\"book_status_read\" href=\"javascript:void(0);\" onmouseover=\"View_Reading_Record(this, '$reading_record_id');\" onmouseout=\"Remove_Reading_Record_Layer();\">".$Lang['ReadingGarden']['Readed']."</a>";
					$TBEditReadingRecordButton = $this->Get_Thickbox_Link($TBReadingRecordHeight, $TBReadingRecordWidth, "", $Lang['ReadingGarden']['EditReadingRecord'], "Get_Reading_Record_Edit_Form($book_id,0,$reading_record_id);", "FakeLayer", " [".$Lang['Btn']['Edit']."] ", "");
					$RemoveReadingRecordButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteReadingRecord']."\" onclick=\"Delete_Reading_Record('$reading_record_id');return false;\">[".$Lang['Btn']['Delete']."]</a>";
					if($book_report_id!=''){
						if($book_report_attachment!=''){
							//$file_link = $ReadingGardenLib->ModuleFilePath."/book_report/u".$student_id."/r".$book_report_id."/".$book_report_attachment;
							$file_link = $ReadingGardenLib->ModuleFilePath."/book_report".$book_report_attachment;
							$BookReportFileLink = "<a href=\"".$file_link."\" target=\"_blank\" class=\"book_report_edit\" title=\"".htmlspecialchars(basename($book_report_attachment),ENT_QUOTES)."\">".$Lang['ReadingGarden']['ViewBookReport']."</a>";
							$RemoveBookReportFileButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteBookReport']."\" onclick=\"Delete_Book_Report_File('$book_report_id');return false;\">[".$Lang['Btn']['Delete']."]</a>";
						}else{
							$HandinBookReportButton = $this->Get_Thickbox_Link($TBBookReportFileHeight, $TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'$reading_record_id','$book_report_id','$assigned_reading_id');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
						}
						
						if($report_answer_sheet!=''){
							// show answer
							$EditAnswerSheetButton = $this->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['EditAnswerSheet'], "Get_Book_Report_Edit_Form(1,0,'$reading_record_id','$book_report_id','$assigned_reading_id');", "FakeLayer", $Lang['ReadingGarden']['EditAnswerSheet'], "");
							$ViewAnswerSheetButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['ViewAnswerSheet']."\" onclick=\"newWindow('answer_sheet.php?reading_record_id=".$reading_record_id."&book_report_id=".$book_report_id."&assigned_reading_id=".$assigned_reading_id."',10);\">[".$Lang['Btn']['View']."]</a>";
							$DeleteAnswerSheetButton = "<a href=\"javascript:void(0);\" title=\"".$Lang['ReadingGarden']['DeleteAnswerSheet']."\" onclick=\"Delete_Answer_Sheet('$book_report_id');return false;\">[".$Lang['Btn']['Delete']."]</a>";
						}else if(($assigned_reading_target_cnt>0 && $assigned_answer_sheet!='') || $default_answer_sheet!=''){
							// if either have assigned answer sheet or book default answer sheet, do it
							$DoAnswerSheetButton = $this->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['DoAnswerSheet'], "Get_Book_Report_Edit_Form(1,1,'$reading_record_id','$book_report_id','$assigned_reading_id');", "FakeLayer", $Lang['ReadingGarden']['DoAnswerSheet'], "");
						}
					}else{
						$HandinBookReportButton = $this->Get_Thickbox_Link($TBBookReportFileHeight, $TBBookReportFileWidth, "book_report_edit", $Lang['ReadingGarden']['HandInBookReport'], "Get_Book_Report_Edit_Form(0,1,'$reading_record_id','$book_report_id','$assigned_reading_id');", "FakeLayer", $Lang['ReadingGarden']['HandInBookReport'], "");
						if(($assigned_reading_target_cnt>0 && $assigned_answer_sheet!='') || $default_answer_sheet!=''){
							// if either have assigned answer sheet or book default answer sheet, do it
							$DoAnswerSheetButton = $this->Get_Thickbox_Link($TBAnswerSheetHeight, $TBAnswerSheetWidth, "book_report_done", $Lang['ReadingGarden']['DoAnswerSheet'], "Get_Book_Report_Edit_Form(1,1,'$reading_record_id','$book_report_id','$assigned_reading_id');", "FakeLayer", $Lang['ReadingGarden']['DoAnswerSheet'], "");
						}
					}
				}
				
				$x .= "<tr>\n<td>\n";
				$x .= "<div class=\"book_detail_w_cover\">
                      	<div class=\"book_detail_cover\">
                      		<img src=\"$cover_image_path\" width=\"100px\" height=\"120px\">
                       	</div>";
				$x .= " <div class=\"book_detail\">
                    		<div class=\"book_detail_info\">
                        		<h1>$book_name</h1>
                        		<h2 class=\"book_author\"><em>".$Lang['ReadingGarden']['Author'].":</em><span>$author</span></h2>
                        		<h2 class=\"book_publisher\"><em>".$Lang['ReadingGarden']['Publisher'].":</em><span>$publisher</span></h2>
                        		<h2 class=\"book_ISBN\"><em>".$Lang['ReadingGarden']['CallNumber'].":</em><span>$call_number</span></h2>
                         		<p class=\"spacer\"></p>
                        	</div>
                         	<!--<h2 class=\"book_level\"><em>level:</em><span>P6</span></h2>-->
                        	<h2 class=\"book_cat\"><em>".$Lang['ReadingGarden']['Category'].":</em><span>".$category."</span></h2>
                         	<p class=\"spacer\"></p>
                      		<h2 class=\"book_desc\"><em>".$Lang['ReadingGarden']['Description'].":</em><span>".nl2br($description)."</span></h2>
                        	<p class=\"spacer\"></p>";
                # Like Function
                $x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK, $book_id);
                
                $x .= '<p class=\"spacer\"></p>';     		
                $x .= "<div>
                     		<h2 class=\"book_level\"><em> ".$Lang['ReadingGarden']['MyRecord']." :</em>
							<span>";
				$x .= $TBAddReadingRecordButton;
				$x .= $ReadingRecordReaded;
				$x .= $TBEditReadingRecordButton;
				$x .= $RemoveReadingRecordButton;
				$x .= $HandinBookReportButton;
				$x .= $BookReportFileLink;
				$x .= $RemoveBookReportFileButton;
				$x .= $DoAnswerSheetButton;
				$x .= $EditAnswerSheetButton;
				$x .= $ViewAnswerSheetButton;
				$x .= $DeleteAnswerSheetButton;
				
				$x .= "		</span>
							</h2>
                    	</div>";
                
				$x .= " </div>
		                <p class=\"spacer\"></p>
		             </div>";
				$x .= "</td>\n</tr>\n";
			}
		
		$x .= "</thead></table>";
		
		return $x;
	}*/
		
	function Get_Language_Selection($ID_Name='', $SelectedLangauge='', $all=0, $noFirst=0, $FirstTitle="", $OnChange="")
	{
		global $Lang;
		
		$language_arr[CATEGORY_LANGUAGE_CHINESE]=$Lang['ReadingGarden']['Chinese'];
		$language_arr[CATEGORY_LANGUAGE_ENGLISH]=$Lang['ReadingGarden']['English'];
		
		if(trim($OnChange)!='')
			$other = " onchange='$OnChange' ";
		
		$LanguageSelection = getSelectByAssoArray($language_arr, " id='$ID_Name' name='$ID_Name' $other ",$SelectedLangauge, $all, $noFirst, $FirstTitle);
		
		return $LanguageSelection;
			
	}
	
	function Get_Return_Message($Key)
	{
		global $Lang;
		
		if($Lang['ReadingGarden']['ReturnMessage'][$Key])
			return $Lang['ReadingGarden']['ReturnMessage'][$Key];
		else if($Lang['ReadingGarden']['ReturnMsg'][$Key])
			return $Lang['ReadingGarden']['ReturnMsg'][$Key];
		else if($Lang['General']['ReturnMessage'][$Key])
			return $Lang['General']['ReturnMessage'][$Key];
		else
			return $Key;
	}
	
	function Get_Category_Selection($ID, $Name, $SelectedCategory='', $SelectTitle='', $onChange='', $class='', $Language='', $ShowUnclassified=true, $isMultiple=0)
	{
		global $ReadingGardenLib, $Lang;
		$CateList = $ReadingGardenLib->Get_Category_List('',$Language);
		
		if($ShowUnclassified) $optionArr[] = array("0",$Lang['ReadingGarden']['UnclassifiedCategory']);
		foreach((array)$CateList as $thisCat)
		{
			$LanguageDisplay = $Lang['ReadingGarden']['BookCategorySettings']['LangArr'][$thisCat['Language']]; 
			$optionArr[] = array($thisCat['CategoryID'],$thisCat['CategoryCode']." ".$thisCat['CategoryName'],$LanguageDisplay);
		}
		
		$Tags = " id='$ID' name='$Name' ";
		if(trim($onChange)!='')
			$Tags .= " onchange='$onChange' ";	

		if(trim($class)!='')
			$Tags .= " class='$class' ";	

		if($isMultiple==1)
			$Tags .= " multiple size=10 ";

		$Selection = $this->GET_SELECTION_BOX_WITH_OPTGROUP($optionArr, $Tags, $SelectTitle, $SelectedCategory);
		return $Selection;
	}

	/************************************ Marcus Part Start *************************************/
	# updated date: 20110103
	function Get_Settings_Scoring_Rule_Setting_UI()
	{
		global $Lang, $ReadingGardenLib;
		
//		$x .= '<br><br>'."\n";
//		$x .= $this->GET_NAVIGATION2_IP25($Lang['ReadingGarden']['ScoringSettings']['ScoringRule'])."\n";
		$x .= '<form name="form1" id="form1" action="scoring_rule_settings_update.php" method="post" onsubmit="return CheckForm()">'."\n";
			$x .= '<div class="table_board">'."\n";
			 	$x .= $this->Get_Warning_Message_Box($Lang['General']['Instruction'],$Lang['ReadingGarden']['ScoringSettings']['InputZeroToDisableRule']);
				$x .= '<table class="form_table_v30">'."\n";
					$x .= '<tbody>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['PressLikeButton'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= ' + '.$this->GET_TEXTBOX_NUMBER('PressLikeButton','ScoringSettings['.ACTION_SCORE_LIKE.']',$ReadingGardenLib->Settings[ACTION_SCORE_LIKE], "NumericField")."\n";
								$x .= ' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['Every'].' '.$this->GET_TEXTBOX_NUMBER('TimesOfLike','ScoringSettings['.ACTION_SETTING_TIMES_OF_LIKE.']',$ReadingGardenLib->Settings[ACTION_SETTING_TIMES_OF_LIKE], "NumericField").' '.$Lang['ReadingGarden']['ScoringSettings']['Times']."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"PressLikeButtonWarnMsg TimesOfLikeWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['WriteComment'].'</td>'."\n";
							$x .= ' <td> + '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('WriteComment','ScoringSettings['.ACTION_SCORE_COMMENT.']',$ReadingGardenLib->Settings[ACTION_SCORE_COMMENT], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"WriteCommentWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['StartAThreadInForum'].'</td>'."\n";
							$x .= ' <td> + '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('StartAThreadInForum','ScoringSettings['.ACTION_SCORE_START_THREAD.']',$ReadingGardenLib->Settings[ACTION_SCORE_START_THREAD], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"StartAThreadInForumWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['FollowAThreadInForum'].'</td>'."\n";
							$x .= ' <td> + '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('FollowAThreadInForum','ScoringSettings['.ACTION_SCORE_FOLLOW_THREAD.']',$ReadingGardenLib->Settings[ACTION_SCORE_FOLLOW_THREAD], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"FollowAThreadInForumWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['AddReadingRecord'].'</td>'."\n";
							$x .= ' <td> + '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('AddReadingRecord','ScoringSettings['.ACTION_SCORE_ADD_READING_RECORD.']',$ReadingGardenLib->Settings[ACTION_SCORE_ADD_READING_RECORD], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"AddReadingRecordWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['WriteBookReport'].'</td>'."\n";
							$x .= ' <td> + '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('WriteBookReport','ScoringSettings['.ACTION_SCORE_ADD_BOOK_REPORT.']',$ReadingGardenLib->Settings[ACTION_SCORE_ADD_BOOK_REPORT], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"WriteBookReportWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['TeacherRecommendReport'].'</td>'."\n";
							$x .= ' <td> + '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('TeacherRecommendReport','ScoringSettings['.ACTION_SCORE_RECOMMEND_REPORT.']',$ReadingGardenLib->Settings[ACTION_SCORE_RECOMMEND_REPORT], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"TeacherRecommendReportWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
//						$x .= '<tr>'."\n";
//							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['CommentLength'].'</td>'."\n";
//							$x .= '<td>'."\n";
//								$x .= ' + '.$this->GET_TEXTBOX_NUMBER('CommentLengthScore','ScoringSettings['.ACTION_SCORE_COMMENT_LENGTH.']',$ReadingGardenLib->Settings[ACTION_SCORE_COMMENT_LENGTH], "NumericField")."\n";
//								$x .= ' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['Every'].' '.$this->GET_TEXTBOX_NUMBER('CommentMoreThanEvery','ScoringSettings['.ACTION_SETTING_COMMENT_LENGTH.']',$ReadingGardenLib->Settings[ACTION_SETTING_COMMENT_LENGTH], "NumericField").' '.$Lang['ReadingGarden']['ScoringSettings']['Words'].' '."\n";
//								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"CommentLengthScoreWarnMsg CommentMoreThanEveryWarnMsg WarnDiv");
//							$x .= '</td>'."\n";	
//						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['Login'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= ' + '.$this->GET_TEXTBOX_NUMBER('LoginScore','ScoringSettings['.ACTION_SCORE_LOGIN.']',$ReadingGardenLib->Settings[ACTION_SCORE_LOGIN], "NumericField")."\n";
								$x .= ' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['MoreThan'].' '.$this->GET_TEXTBOX_NUMBER('LoginMoreThan','ScoringSettings['.ACTION_SETTING_LOGIN_MORE_THAN.']',$ReadingGardenLib->Settings[ACTION_SETTING_LOGIN_MORE_THAN], "NumericField").' '.$Lang['ReadingGarden']['ScoringSettings']['MinsWithin24Hours']."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"LoginMoreThanWarnMsg LoginScoreWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDaysWithoutLogin'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= ' - '.$this->GET_TEXTBOX_NUMBER('WithoutLoginPenalty','ScoringSettings['.ACTION_PENALTY_WITHOUT_LOGIN.']',$ReadingGardenLib->Settings[ACTION_PENALTY_WITHOUT_LOGIN], "NumericField")."\n";
								$x .= ' '.' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['Every'].' '.$this->GET_TEXTBOX_NUMBER('SuccessiveDaysWithoutLogin','ScoringSettings['.ACTION_SETTING_DAYS_WITHOUT_LOGIN.']',$ReadingGardenLib->Settings[ACTION_SETTING_DAYS_WITHOUT_LOGIN], "NumericField").' '.' '.$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDays']."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"SuccessiveDaysWithoutLoginWarnMsg WithoutLoginPenaltyWarnMsg");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['CommentDeletedByAdmin'].'</td>'."\n";
							$x .= ' <td> - '."\n";	
								$x .= $this->GET_TEXTBOX_NUMBER('CommentDeletedByAdmin','ScoringSettings['.ACTION_PENALTY_COMMENT_DELETED.']',$ReadingGardenLib->Settings[ACTION_PENALTY_COMMENT_DELETED], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"CommentDeletedByAdminWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['ForumThreadDeletedByAdmin'].'</td>'."\n";
							$x .= ' <td> - '."\n";	
								$x .=  $this->GET_TEXTBOX_NUMBER('ForumThreadDeletedByAdmin','ScoringSettings['.ACTION_PENALTY_THREAD_DELETED.']',$ReadingGardenLib->Settings[ACTION_PENALTY_THREAD_DELETED], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"ForumThreadDeletedByAdminWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['ForumPostDeletedByAdmin'].'</td>'."\n";
							$x .= ' <td> - '."\n";	
								$x .=  $this->GET_TEXTBOX_NUMBER('ForumPostDeletedByAdmin','ScoringSettings['.ACTION_PENALTY_POST_DELETED.']',$ReadingGardenLib->Settings[ACTION_PENALTY_POST_DELETED], "NumericField")."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"ForumPostDeletedByAdminWarnMsg WarnDiv");
							$x .= '</td>'."\n";	
						$x .= '</tr>'."\n";
					$x .= '</tbody>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'],"submit");
			$x .= '</div>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br><br>'."\n";
	
		return $x;
	}
	
	function Get_Settings_Form_Book_Source_Setting_UI()
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
		# Get Class List
		include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
		$YearLib = new year();
		$YearClassList =  $YearLib->Get_All_Classes($CheckClassTeacher=0, $AcademicYearID='');
		$ClassTitleLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
		$YearClassAssoc = BuildMultiKeyAssoc($YearClassList,array("YearName","YearClassID"),$ClassTitleLang,1);
		
		# Get Class Book Source
		$ClassBookSourceArr = $ReadingGardenLib->Settings_Get_Class_Book_Source();
		$ClassBookSourceAssoc = BuildMultiKeyAssoc($ClassBookSourceArr,"ClassID","BookSource",1);
		
		# Selection Oprion Arr
		# get Set All Scheme Selection
		$optionArr[BOOK_REPORT_SOURCE_BOOK_LIST] = $Lang['ReadingGarden']['BookSourceSettings']['BookSource'][BOOK_REPORT_SOURCE_BOOK_LIST];
		$optionArr[BOOK_REPORT_SOURCE_EXISTING_BOOK] = $Lang['ReadingGarden']['BookSourceSettings']['BookSource'][BOOK_REPORT_SOURCE_EXISTING_BOOK];
		$optionArr[BOOK_REPORT_SOURCE_ANY_BOOK] = $Lang['ReadingGarden']['BookSourceSettings']['BookSource'][BOOK_REPORT_SOURCE_ANY_BOOK];
		$SetAllSchemeSelection = '<span class="table_filter">'.getSelectByAssoArray($optionArr," id='SetAllSourceSelection' ",'',0,0).'</span>';
		$SetAllSchemeSelection .= '<div class="table_row_tool"><a href="javascript:void(0);" onclick="SetAllSelection();" class="icon_batch_assign" title="'.$Lang['Btn']['ApplyToAll'].'"></a></div>';	

//		$x .= '<br><br>'."\n";
//		$x .= $this->GET_NAVIGATION2_IP25($Lang['ReadingGarden']['ScoringSettings']['ScoringRule'])."\n";
		$x .= '<form name="form1" id="form1" action="form_book_source_settings_update.php" method="post" onsubmit="return CheckForm();">'."\n";
			$x .= '<div class="table_board">'."\n";
			 	$x .= '<table class="common_table_list_v30">'."\n";

	 				# col group
					$x .= '<col align="left" style="width: 30%;">'."\n";
					$x .= '<col align="left" style="width: 30%;">'."\n";
					$x .= '<col align="left">'."\n";

					# table head
					$x .= '<thead>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<th>'.$Lang['ReadingGarden']['Form'].'</th>'."\n";
							$x .= '<th class="sub_row_top">'.$Lang['ReadingGarden']['Class'].'</th>'."\n";
							$x .= '<th class="sub_row_top">'.$Lang['ReadingGarden']['BookSourceSettings']['PossibleBookSource'].'</th>'."\n";	
						$x .= '</tr>'."\n";
						$x .= '<tr class="sub_row">'."\n";
							$x .= '<td class="sub_row_top">&nbsp;</td>'."\n";
							$x .= '<td class="sub_row_top">&nbsp;</td>'."\n";
							$x .= '<td class="sub_row_top">'.$SetAllSchemeSelection.'</td>'."\n";
						$x .= '</tr>'."\n";		 	
					$x .= '</thead>'."\n";
					
					#table body
					$x .= '<tbody>'."\n";
					
						# loop Form
						foreach((array)$YearClassAssoc as $YearName => $ClassNameArr)
						{
							$NumOfClass = count($ClassNameArr) + 1;
					
							$x .= '<tr>'."\n";
								$x .= '<td rowspan="'.$NumOfClass.'">'.$YearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							foreach((array)$ClassNameArr as $ClassID => $ClassName)
							{
								$SelectID = "ClassBookSource_$ClassID";
								$SelectedVal = $ClassBookSourceAssoc[$ClassID];
								
								$x .= '<tr class="sub_row">'."\n";
									$x .= '<td>'.$ClassName.'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= getSelectByAssoArray($optionArr," id='$SelectID' name='ClassBookSourceArr[$ClassID]' class='SourceSelect' ",$SelectedVal)."\n";
										$x .= $this->Get_Form_Warning_Msg($SelectID."WarnMsg",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBookSource'],"WarnMsgDiv")."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							}
								
						}
						
					$x .= '</tbody>'."\n";

				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'],"submit");
			$x .= '</div>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Book_Category_Setting_UI()
	{
		global $Lang;
		
//		$x .= '<br><br>'."\n";
		$x .= '<form name="form1" id="form1" action="form_book_source_settings_update.php" method="post" onsubmit="return CheckForm();">'."\n";
			$x .= '<div class="table_board" id="CategoryTableDiv">'."\n";
				$x .= $this->Get_Ajax_Loading_Image();
			$x .= '</div>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br><br>'."\n";
		return $x;
	}
	
	function Get_Settings_Book_Category_Setting_Table()
	{
		global $Lang, $ReadingGardenLib;
	
		$ThickBoxHeight = 450;
		$ThickBoxWidth = 750;
		
		$BookArr = $ReadingGardenLib->Get_Book_List("","","","","",0);
		$CatBookAssoc =  BuildMultiKeyAssoc($BookArr, array("CategoryID","BookID"));
		$CategoryArr = $ReadingGardenLib->Settings_Get_Category_Info();
		$LangCategoryAssoc =  BuildMultiKeyAssoc($CategoryArr, array("Language","CategoryID"));
		 
		$BookListLink = "../../books/index.php";
		
	 	$x .= '<table class="common_table_list_v30" id="CategoryTable">'."\n";
	
			# col group
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left" style="width: 30px">'."\n";
			$x .= '<col align="left" style="width: 100px">'."\n";
	
			# table head
			$ColSpan = 5;
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['BookCategorySettings']['Language'].'</th>'."\n";
					$x .= '<th colspan="'.$ColSpan.'">'.$Lang['ReadingGarden']['BookCategorySettings']['Category'].'</th>'."\n";
						
				$x .= '</tr>'."\n";
				$x .= '<tr class="sub_row">'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['BookCategorySettings']['Language'].'</td>'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['BookCategorySettings']['CategoryCode'].'</td>'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['BookCategorySettings']['CategoryName'].'</td>'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['BookCategorySettings']['NumOfBooks'].'</td>'."\n";
					$x .= '<td class="sub_row_top">&nbsp;</td>'."\n";
					$x .= '<td class="sub_row_top">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";		 	
			$x .= '</thead>'."\n";

			# Unclassified Category
			$NumOfBook = count($CatBookAssoc['']);
			$x .= '<tbody>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'.$Lang['ReadingGarden']['UnclassifiedCategory'].'</td>'."\n";
					$x .= '<td>'.$Lang['General']['EmptySymbol'].'</td>'."\n";
					$x .= '<td>'.$Lang['General']['EmptySymbol'].'</td>'."\n";
					$x .= '<td><a href="'.$BookListLink.'?CategoryID=0">'.$NumOfBook.'</a></td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";	
			$x .= '</tbody>'."\n";

			// Loop Language (Chinese, English)
			foreach(array(CATEGORY_LANGUAGE_CHINESE,CATEGORY_LANGUAGE_ENGLISH, CATEGORY_LANGUAGE_OTHERS) as $thisLang)
			{
				#table body
				$x .= '<tbody>'."\n";
					$CateogryInfoArr = 	$LangCategoryAssoc[$thisLang];
					
					$NumOfCategory = count($CateogryInfoArr)+2;
					$x .= '<tr class="nodrop">'."\n";
						$x .= '<td rowspan="'.$NumOfCategory.'">'.$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][$thisLang].'</td>'."\n";
					$x .= '</tr>'."\n";
	
					foreach((array)$CateogryInfoArr as $CategoryInfo)
					{
						$NumOfBook = count($CatBookAssoc[$CategoryInfo['CategoryID']]);
						$ContainBook = $NumOfBook>0?1:0;
						$thisTitle = $Lang['ReadingGarden']['BookCategorySettings']['EditCategory'];
						$thisOnclick="js_Category_Setting('$thisLang','".$CategoryInfo['CategoryID']."')";
						$DeleteLink = '<a href="javascript:void(0)" onclick="js_Delete_Category(\''.$CategoryInfo['CategoryID'].'\','.$ContainBook.')" class="delete_dim" title="'.$Lang['ReadingGarden']['BookCategorySettings']['DeleteCategory'].'"></a>';
						$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['ReadingGarden']['BookCategorySettings']['MoveCategory'].'"></a>';
						$ThickBoxEditLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.htmlspecialchars($CategoryInfo["CategoryCode"]).'</td>'."\n";
							$x .= '<td>'.htmlspecialchars($CategoryInfo["CategoryName"]).'</td>'."\n";
							$x .= '<td><a href="'.$BookListLink.'?CategoryID='.$CategoryInfo['CategoryID'].'">'.$NumOfBook.'</a></td>'."\n";
							$x .= '<td class="Draggable"><span class="table_row_tool">'.$MoveLink.'</span></td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<div class="table_row_tool row_content_tool">'."\n";
									$x .= $ThickBoxEditLink;
									$x .= $DeleteLink;
								$x .= '</div>'."\n";
								$x .= '<input type="hidden" name="RowCategoryID[]" class="RowCategoryID" value="'.$CategoryInfo['CategoryID'].'">'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					}
					
					# Add Record Row
					$thisTitle = $Lang['ReadingGarden']['BookCategorySettings']['AddCategory'];
					$thisOnclick="js_Add_Category($thisLang)";
					$ThickBoxLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
					
					$x .= '<tr class="nodrop">'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>&nbsp;</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<div class="table_row_tool row_content_tool">'."\n";
								$x .= $ThickBoxLink;
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</tbody>'."\n";
			}
			
		$x .= '</table>'."\n";
		
		return $x;		
	}
	
	
	function Get_Settings_Book_Category_Edit_Layer($Language, $CategoryID = '')
	{
		global $Lang, $ReadingGardenLib;
		
		if(trim($CategoryID)!='')
			$CategoryArr = $ReadingGardenLib->Settings_Get_Category_Info($CategoryID, $Language);
		
		
		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit");
		$SubmitAndContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'],"Button"," CheckTBForm(1);");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
		
		# Get Max Length Info
//		$LengthInfo = $eRC_Rubrics_ConfigArr['MaxLength'];
		
		$x .= '';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form id="TBForm" name="TBForm" onsubmit=" CheckTBForm(); return false;">'."\n";
			$x .= '<table width="98%" cellpadding=10px cellspacing=10px align="center" height="290px">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							
							# Code
							$thisValue = intranet_htmlspecialchars($CategoryArr[0]["CategoryCode"]);
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['BookCategorySettings']['CategoryCode'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="CategoryCode" id="CategoryCode" class="textbox_name Mandatory" onkeyup="CheckCode(this)" value="'.$thisValue.'" maxlength="'.$this->MaxCodeLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankCategoryCode",$Lang['ReadingGarden']['WarningMsg']['PleaseInputCategoryName'], "WarnMsgDiv");
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnExistCategoryCode",$Lang['ReadingGarden']['WarningMsg']['CategoryCodeExist']);
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Name
							$thisValue = intranet_htmlspecialchars($CategoryArr[0]["CategoryName"]);
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['BookCategorySettings']['CategoryName'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="CategoryName" id="CategoryName" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
									$x .= $this->Spacer();
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankCategoryName",$Lang['ReadingGarden']['WarningMsg']['PleaseInputCategoryCode'], "WarnMsgDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
			if(trim($CategoryID)=='') $x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
		# hidden field
		$x .= '<input type="hidden" name="CategoryID" id="CategoryID" value="'.$CategoryID.'">'."\n";
		$x .= '<input type="hidden" name="Language" id="Language" value="'.$Language.'">'."\n";
		
		$x .= '</form>';		
		$x .= $this->FOCUS_ON_LOAD("TBForm.CategoryCode");
		
		return $x;
	}
	
	function Get_Settings_Recommend_Book_List_UI($YearID='', $field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $LAYOUT_SKIN, $image_path;
		
		$ThickBoxHeight = 600;
		$ThickBoxWidth = 750;
		
		# Add Btn
//		$thisOnclick="js_Add_Book()";
//		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['RecommendBookSettings']['AddRecommendBook'], $thisOnclick, $InlineID="FakeLayer", $Lang['Btn']['New']);

		# Form Seletion 
		include_once("form_class_manage_ui.php");
		$fcm_ui = new form_class_manage_ui();
		$FormSelection = $fcm_ui->Get_Form_Selection("YearID", $YearID, " js_Relaod_Book_List(); ", 1, 0, 0);
		
		# Category Selection
		$SelectTitle = $Lang['ReadingGarden']['SelectTitleArr']['AllCategory'];
		$CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID","",$SelectTitle," js_Relaod_Book_List(); ");
		
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="recommend_book_list.php" onsubmit="js_Relaod_Book_List(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						#content_top_tool 
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $ThickBoxNewLink."\n";
							$x .= '</div>'."\n";
							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n"; 
						# table_filter / common_table_tool  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="table_filter">'."\n";
										$x .= $FormSelection."\n";
										$x .= $CategorySelection."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_photo" href="javascript:Toggle_Cover();">'."\n";
											$x .= '<span id=DisplayCoverText>'.$Lang['ReadingGarden']['DisplayAllCover'].'</span>'."\n";
										$x .= '</a>'."\n";
								       $x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'RecommendBookID[]\',\'delete_reccommend_book.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						# Main Table	
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="BookListDBTable">'."\n";
										$x .= $this->Get_Ajax_Loading_Image()."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Recommend_Book_List_DBTable($YearID='', $field='', $order='', $pageNo='', $numPerPage='', $Keyword='', $CategoryID='')
	{
		global $Lang, $page_size, $ck_settings_recommend_book_list_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;

		if (isset($ck_settings_recommend_book_list_page_size) && $ck_settings_recommend_book_list_page_size != "") $page_size = $ck_settings_recommend_book_list_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Recommend_Book_List_DBTable_Sql($YearID, $Keyword, $CategoryID);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
			
		$li->field_array = array("b.BookName", "CallNumber",  "Author", "Publisher", "CategoryName", "DateInput");
		$li->column_array = array(18,18,18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1'  class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['BookName'])."</th>\n";
//		$li->column_list .= "<th width='100px' >".$Lang['ReadingGarden']['CoverImage']."</td>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['CallNumber'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Author'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Publisher'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Category'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['CreateDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("RecommendBookID[]")."</th>\n";
		$li->no_col = $pos+2;
	
		$x .= $li->display();
	
		return $x;		
	}
	
	function Get_Settings_Recommend_Book_List_New_Layer($YearID)
	{
		global $Lang, $ReadingGardenLib;
		
		$Showcase = $this->Get_Settings_Recommend_Book_List_Book_Showcase('','',$YearID);
		
			$SelectTitle = $Lang['ReadingGarden']['SelectTitleArr']['AllCategory'];
			$CategorySelection = $this->Get_Category_Selection("AssignCategoryID", "AssignCategoryID","",$SelectTitle," LoadBook(); ");

			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<div>'."\n";
				$x .= '<div class="table_filter">'."\n";
					$x .= $CategorySelection."\n";
				$x .= '</div>'."\n";
				$x .= '<div class="Conntent_search"><form onsubmit="LoadBook(); return false;"><input type="text" id="BookKeyword" name="BookKeyword"></form></div>'."\n";
			$x .= '</div>'."\n";
			$x.= '<div id="ItemShowcase" style="text-align:center; padding: 0px; width: 100%; height: 450px; overflow: auto;">'."\n";
				$x .= $Showcase."\n";
			$x.= '</div>'."\n";
			$x.= '<div class="edit_bottom_v30">'."\n";
				$x.= $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "tb_remove(); if(isMappingUdapte) {js_Relaod_Book_List();}");
			$x.= '</div>'."\n";
		
		return $x;
				
	}
	
	function Get_Settings_Recommend_Book_List_Book_Showcase($CategoryID='', $Keyword='', $YearID='')
	{
		global $ReadingGardenLib;
		
		$BookList = $ReadingGardenLib->Get_Book_List('',$Keyword,$CategoryID,'','',0);
		
		if(trim($YearID)!='')
		{
			$Par['YearID'] = $YearID;
			$ReccomendBookList = $ReadingGardenLib->Get_Recommend_Book_Mappings($Par);
			$ReccomendBookIDArr = Get_Array_By_Key($ReccomendBookList,"BookID");
		}
		$x.= '<table cellspacing="0" cellpadding="3" border="0" width="97%">'."\n";
			$x.= '<tbody>'."\n";
			
				$NumOfColumn = 5;
				$NumOfRow = ceil(count($BookList)/$NumOfColumn); 
				for($i=0; $i<$NumOfRow; $i++)
				{
					$x.= '<tr class="tablegreenrow2">'."\n";

					for($j=0; $j<$NumOfColumn; $j++)
					{
						$key = $NumOfColumn*$i + $j;
						$thisImg = '';
						$BookDisplay = '';	
						$thisSelectedClass = '';							
						
						if($thisBookInfo = $BookList[$key])
						{
							$thisClassName = "Book_".$thisBookInfo['BookID'];
							$BookDisplay = $this->Format_Book_Display($thisBookInfo,1);
							$thisSelectedClass = in_array($thisBookInfo['BookID'],(array)$ReccomendBookIDArr)?"booking_table_item_select":"booking_table_item";
							$thisOnclick = in_array($thisBookInfo['BookID'],(array)$ReccomendBookIDArr)?'':' onclick="js_Select_Book('.$thisBookInfo['BookID'].')"';
//							if($thisBookInfo['BookCoverImage'])
//								$thisImg = '<img src="'.$ReadingGardenLib->BookCoverImagePath.'/'.$thisBookInfo['BookCoverImage'].'" width="100px" height="120px">'."\n";
							$thisImg= $ReadingGardenLib->Get_Book_Cover_Image($thisBookInfo['BookCoverImage'],100,120);
						}	 						
						$x.= '<td align="center" width="20%">'."\n";
							$x.= '<a class="'.$thisSelectedClass.' tablelink '.$thisClassName.'" style="width:95%" '.$thisOnclick.' href="javascript:void(0);" id="'.$thisBookInfo['BookID'].'">'.$thisImg.'<br>'.$BookDisplay.'<br></a>'."\n";
						$x.= '</td>'."\n";
					}
						
					$x.= '</tr>'."\n";		
				}
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Assign_Reading_UI($YearClassID='', $Keyword='')
	{
		global $Lang;
		
		$ThickBoxHeight = 600;
		$ThickBoxWidth = 750;
		
		# Add Btn
		$thisOnclick="js_Create_Assign_Reading()";
		$NewReadingBtn = $this->Get_Content_Tool_v30("new", "new_assign_reading.php", $Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading'] );
//		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading'], $thisOnclick, $InlineID="FakeLayer", $Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading']);
		$thisOnclick="js_Assign_Reading_Go_To_Step1()";
		$AssignReadingBtn = $this->Get_Content_Tool_v30("new", "javascript:void(0);", $Lang['ReadingGarden']['AssignReadingToClass'],'',' onclick="'.$thisOnclick.'" ' );

		$WarnRemoveMsg=  $Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReading'];
		
		include_once("libclass.php");
		$lclass = new libclass();
		$ClassSelection = $lclass->getSelectClassWithWholeForm(" id='YearClassID' name='YearClassID' onchange='js_Reload_Assign_Reading_Table()'",$YearClassID,$Lang['ReadingGarden']['SelectTitleArr']['AllAssignReading']);
		
			$x .= '<div class="content_top_tool">'."\n";
				$x .= '<div class="Conntent_tool">'."\n";
					$x .= $NewReadingBtn."\n";
					$x .= $AssignReadingBtn."\n";
				$x .= '</div>'."\n";
				$x .= '<div class="Conntent_search"><form name="searchform" onsubmit="js_Reload_Assign_Reading_Table(); return false;"><input type="text" id="Keyword" name="Keyword" value="'.$Keyword.'"></form></div>'."\n";
				$x .= '<br style="clear: both;">'."\n";
			$x .= '</div>'."\n";
			$x .= '<form name="form1" id="form1" action="" method="post" onsubmit="return CheckForm();">'."\n";
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td valign="bottom">'."\n";
							$x .= '<div class="table_filter">'."\n";
								$x .= $ClassSelection."\n";
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td valign="bottom">'."\n";
							$x .= '<div class="common_table_tool">'."\n";
							    $x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'AssignedReadingID[]\',\'new_assign_reading.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
							    $x .= '<a class="tool_delete" href="javascript:js_Unassign_Class();">'.$Lang['ReadingGarden']['UnassignClass'].'</a>'."\n";
								$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'AssignedReadingID[]\',\'js_Delete_Assign_Reading()\',\''.$WarnRemoveMsg.'\')">'.$Lang['ReadingGarden']['DeleteReadingAssignment'].'</a>'."\n";
							$x .= '</div>'."\n";	
						$x .= '</td>'."\n";		
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				$x .= '<div class="table_board" id="AssignReadingTableDiv">'."\n";
					$x .= $this->Get_Ajax_Loading_Image();
				$x .= '</div>'."\n";
			$x .= '</form>'."\n";
		$x .= '<br><br>'."\n";
		return $x;
	}
	
	function Get_Settings_Assign_Reading_Setting_Table($YearClassID='',$Keyword='')
	{
		global $Lang, $ReadingGardenLib,$image_path, $LAYOUT_SKIN ;
	
//		if(trim($YearClassID)!='')
		if($YearClassID)
		{
			if(!is_array($YearClassID))
				$showMoveLink = 1;
			$AssignedReadingList = $ReadingGardenLib->Get_Class_Assign_Reading_Target($YearClassID);
			
			$AssignedReadingIDArr = Get_Array_By_Key($AssignedReadingList,"AssignedReadingID");
			$AssignedReadingIDArr = array_unique($AssignedReadingIDArr);
			
			$AssignedReadingInfoArr = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingIDArr, '','',$Keyword);
		}
		else
		{
			$AssignedReadingInfoArr = $ReadingGardenLib->Get_Assign_Reading_Info('', '','',$Keyword); // get all assigned reading
			$AssignedReadingIDArr = Get_Array_By_Key($AssignedReadingInfoArr,"AssignedReadingID");
		}
		
		$AssignedReadingAssoc = BuildMultiKeyAssoc($AssignedReadingInfoArr, "AssignedReadingID");
		
		$x .= '<table class="common_table_list_v30" id="AssignReadingTable">'."\n";
	
			# col group
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 100px">'."\n";
			$x .= '<col align="left" style="width: 100px">'."\n";
			$x .= '<col align="left" style="width: 100px">'."\n";
			$x .= '<col align="left">'."\n";
			if($showMoveLink)
			$x .= '<col align="left" style="width: 30px; ">'."\n";
			$x .= '<col align="left" style="width: 30px">'."\n";
	
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['BookName'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['Description'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['AnswerSheet'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['FileUpload'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['OnlineWriting'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['AssignedClass'].'</th>'."\n";
					if($showMoveLink)
					$x .= '<th>&nbsp;</th>'."\n";
					$x .= '<th>'.$this->Get_Checkbox($ID="CheckAllAssignReading", "", "", 0, '', '', 'js_Check_All_Assign_Reading(this)').'</th>'."\n";
				$x .= '</tr>'."\n";		 	
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				foreach((array)$AssignedReadingIDArr as $AssignedReadingID)
				{
					if(!$AssignedReadingInfo = $AssignedReadingAssoc[$AssignedReadingID])
						continue;
					
					$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['ReadingGarden']['AssignReadingSettings']['MoveReadingAssignment'].'"></a>';
					
					$ViewAnswerSheet = '';
					$ReportRequired = '';
					$OnlineWriting = '';
					if($AssignedReadingInfo['AnswerSheet'])
					{
						$ViewIcon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
//						$ViewAnswerSheet = "<a href=\"javascript:void(0);\" onclick=\"js_Preview_AnswerSheet('".$AssignedReadingInfo['AnswerSheet']."')\">".$ViewIcon."</a>";
						$ViewAnswerSheet = "<a href=\"javascript:void(0);\" onclick=\"js_Preview_AnswerSheet('',".$AssignedReadingInfo['AssignedReadingID'].")\">".$ViewIcon."</a>";
					}
					else
					{
						$ViewAnswerSheet = $Lang['General']['EmptySymbol'];
					}
					
					if($AssignedReadingInfo['BookReportRequired'])
					{
						$ReportRequired = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/attendance/icon_present_s.png\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
					}
					else
					{
						$ReportRequired = $Lang['General']['EmptySymbol'];	
					}
					
					
//					if($AssignedReadingInfo['WritingLimit']!=-1 || $AssignedReadingInfo['WritingLimit']!= -1)
//					{
//						$OnlineWriting = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/attendance/icon_present_s.png\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
//						$OnlineWriting .= " (".$this->Get_Online_Writing_Word_Limit_Display($AssignedReadingInfo['LowerLimit'], $AssignedReadingInfo['WritingLimit']).")";
						$OnlineWriting = $this->Get_Online_Writing_Word_Limit_Display($AssignedReadingInfo['LowerLimit'], $AssignedReadingInfo['WritingLimit']);
//					}
					
					$EditLink = '<a href="new_assign_reading.php?AssignedReadingID[]='.$AssignedReadingID.'">'.$AssignedReadingInfo['BookName'].'</a>';
					$EditLink .= '&nbsp;<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/reading_scheme/icon_book.gif" width="14px" height="14px" align="absmiddle" valign="2" onclick="js_View_Book_Detail('.$AssignedReadingInfo['BookID'].');" title="'.$Lang['ReadingGarden']['ViewBookDetail'].'" />';
					$x .= '<tr>'."\n";
						$x .= '<td>'.$EditLink.'&nbsp;</td>'."\n";
						$x .= '<td>'.($AssignedReadingInfo['Description']?htmlspecialchars($AssignedReadingInfo['Description']):$Lang['General']['EmptySymbol']).'</td>'."\n";
						$x .= '<td>'.$ViewAnswerSheet.'&nbsp;</td>'."\n";
						$x .= '<td>'.$ReportRequired.'&nbsp;</td>'."\n";
						$x .= '<td>'.$OnlineWriting.'&nbsp;</td>'."\n";
						$x .= '<td>'.($AssignedReadingInfo['AssignedClass']?$AssignedReadingInfo['AssignedClass']:$Lang['General']['EmptySymbol']).'&nbsp;</td>'."\n";
						if($showMoveLink)
						$x .= '<td class="Draggable"><span class="table_row_tool">'.$MoveLink.'</span></td>'."\n";
						$x .= '<td>'."\n";
							$x .= $this->Get_Checkbox("", "AssignedReadingID[]", $AssignedReadingInfo['AssignedReadingID'], 0, 'AssignedReadingID', '', '','')."\n";
							$x .= '<input type="hidden" class="ClassAssignedReadingID" name="ClassAssignedReadingID[]" value="'.$AssignedReadingInfo['AssignedReadingID'].'">'."\n";
						$x .= '</td>'."\n";
						
					$x .= '</tr>'."\n";
				
					$trctr++;	
				}
				
				if($trctr==0)
				{
					$Colspan = $showMoveLink?7:6;
					$x .= '<tr><td align="center" colspan="'.$Colspan.'"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
				}
				
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<input type="hidden" id="AnswerSheet" name="AnswerSheet" value="">'."\n";
		
		return $x;		
	}

	function Get_Settings_Assign_Reading_To_Class_Step1_Layer($YearClassID='')
	{
		global $Lang, $ReadingGardenLib,$image_path, $LAYOUT_SKIN ;
		
		# Get Btns
		$NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Next'],"Submit","");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Back_To_Main();");
		
		# Form Seletion 
		include_once("libclass.php");
		$lclass = new libclass();
		$YearClassSelection = $lclass->getSelectClassID(" id='ClassID[]' name='ClassID[]' onchange='js_Reload_Assign_Reading_Table()' multiple size=10 ",$YearClassID);
		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);");
		
		# Step
		$StepsObj[] = array($Lang['ReadingGarden']['SelectClass'],1);
		$StepsObj[] = array($Lang['ReadingGarden']['SelectReadingAssignment'],0);
		$StepsObj[] = array($Lang['ReadingGarden']['Confirmation'],0);
		
		$x .= '<form id="form1" name="form1" onsubmit=" return CheckForm(); " action="assign_reading_select_assignment.php">'."\n";
		$x .= '<table width="100%" cellpadding=10px cellspacing=10px align="center">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td valign="top">'."\n";
					$x .= $this->GET_STEPS_IP25($StepsObj);	
					$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
						$x .= '<col class="field_title">';
						$x .= '<col class="field_c">';
						# Class
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Class'].'</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $YearClassSelection."\n";
								$x .= $YearClassSelectAllBtn."\n";
								$x .= $this->Spacer()."\n";
								$x .= $this->MultiSelectionRemark()."\n";
								$x .= $this->Get_Form_Warning_Msg("WarnBlankClassID",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectClass'], "WarnMsgDiv");
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= $this->MandatoryField();		
		
		# Btns
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $NextBtn;
			$x .= "&nbsp;".$CloseBtn;
		$x .= '</div>'."\n";
			
		$x .= '</form>';	
		return $x;		
	}

	function Get_Settings_Assign_Reading_To_Class_Step2_Layer($YearClassIDArr='')
	{
		global $Lang, $ReadingGardenLib,$image_path, $LAYOUT_SKIN ;
		
		# Get Btns
		$NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"Submit","");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'],"Button","window.history.back()");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Back_To_Main();");
		
		# Class List 
		include_once("form_class_manage.php");
		$fcm = new form_class_manage();
		$ClassList = $fcm->Get_All_Year_Class();
		$ClassNameAssoc = BuildMultiKeyAssoc($ClassList,"YearClassID",1,1);
		foreach((array)$YearClassIDArr as $ClassID)
		{
			$ClassNameArr[] = $ClassNameAssoc[$ClassID];
			$hidden .= '<input type="hidden" name="ClassID[]" id="ClassID" value="'.$ClassID.'">'."\n";
		}
		$ClassNameDisplay = implode(", ",$ClassNameArr); 
		
		# Available Assign Reading
		$AssignedReadingList = $ReadingGardenLib->Settings_Get_Class_Available_Assign_Reading($YearClassIDArr);
		$AssignedReadingIDArr = Get_Array_By_Key($AssignedReadingList,"AssignedReadingID");
		$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingIDArr);
		
		# Step 
		$StepsObj[] = array($Lang['ReadingGarden']['SelectClass'],0);
		$StepsObj[] = array($Lang['ReadingGarden']['SelectReadingAssignment'],1);
		$StepsObj[] = array($Lang['ReadingGarden']['Confirmation'],0);
		
		# Instruction
//		$Instruction = $this->Get_Warning_Message_Box('',$Lang['ReadingGarden']['EachBookAssignOnce']);
						
			$x .= '<form id="form1" name="form1" onsubmit="return CheckForm(); " action="assign_reading_confirm.php" >'."\n";
			$x .= '<table width="100%" cellpadding=10px cellspacing=10px align="center">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";
						$x .= $this->GET_STEPS_IP25($StepsObj);	
						$x.= $Instruction;
						$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							# Class
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['SelectedClass'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $ClassNameDisplay."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
//						$x .= $this->Get_Form_Sub_Title_v30($Lang['ReadingGarden']['AvailableReadingAssignement']);
						
						$x .= '<table id="AssignedReadingTable" width="100%" cellpadding="2" class="common_table_list_v30">'."\n";
							$x .= '<col>';
							$x .= '<col>';
							$x .= '<col style="width:12%">';
							$x .= '<col style="width:12%">';
							$x .= '<col style="width:30px">';
							
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th>'.$Lang['ReadingGarden']['BookName'].'</th>'."\n";
									$x .= '<th>'.$Lang['ReadingGarden']['Description'].'</th>'."\n";
									$x .= '<th>'.$Lang['ReadingGarden']['AnswerSheet'].'</th>'."\n";
									$x .= '<th>'.$Lang['ReadingGarden']['BookReportRequired'].'</th>'."\n";
//									$x .= '<th>'.$this->Get_Checkbox("CheckAllAssignedReading", "", "", 0, 'AssignedReadingID', '', 'js_Check_All_Assign_Reading(this)','').'</th>'."\n";
									$x .= '<th>&nbsp;</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
						
							$x .= '<tbody>'."\n";
							if(count($AssignedReadingInfo)==0)
							{
								$x .= '<tr><td align="center" colspan="4"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
							}
							else
							{
								foreach((array)$AssignedReadingInfo as $thisInfo)
								{
									$ViewAnswerSheet = '&nbsp;';
									$ReportRequired = '&nbsp;';
									if($thisInfo['AnswerSheet'])
									{
										$ViewIcon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
										$ViewAnswerSheet = "<a href=\"javascript:void(0);\" onclick=\"js_Preview_AnswerSheet('".$thisInfo['AnswerSheet']."',".$thisInfo['AssignedReadingID'].")\">".$ViewIcon."</a>";
									}
									if($thisInfo['BookReportRequired'])
									{
										$ReportRequired = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/attendance/icon_present_s.png\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
									}
									
									$Checkbox_Class = "Book_".$thisInfo['BookID'];
									$Checkbox_Click = " Check_Book(this,".$thisInfo['BookID'].")";
									$x .= '<tr>'."\n";
										$x .= '<td>'.htmlspecialchars($thisInfo['BookName']).'</td>'."\n";
										$x .= '<td>'.($thisInfo['Description']?htmlspecialchars($thisInfo['Description']):$Lang['General']['EmptySymbol']).'</td>'."\n";
										$x .= '<td>'.$ViewAnswerSheet.'</td>'."\n";
										$x .= '<td>'.$ReportRequired.'</td>'."\n";
										$x .= '<td>'. $this->Get_Checkbox("", "AssignedReadingID[]", $thisInfo['AssignedReadingID'], 0, 'AssignedReadingID '.$Checkbox_Class, '', $Checkbox_Click,'').'</td>'."\n";
									$x .= '</tr>'."\n";
								}
							}
							$x .= '</tbody>'."\n";
								
						$x .= '</table>'."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankAssignedReadingID",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectReadingAssignment'], "WarnMsgDiv");
						
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $NextBtn;
				$x .= "&nbsp;".$BackBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
			# hidden field
			foreach((array)$YearClassIDArr as $ClassID)
			$x .= '<input type="hidden" name="ClassID[]" id="ClassID" value="'.$ClassID.'">'."\n";
			$x .= '<input type="hidden" name="AnswerSheet" id="AnswerSheet" value="">'."\n";
		
		$x .= '</form>';	
		return $x;		
	}

	function Settings_Assign_Reading_To_Class_Comfirm_Page($ClassIDArr, $AssignedReadingIDArr)
	{
		global $ReadingGardenLib, $PATH_WRT_ROOT, $Lang;

		# Get Btns
		$NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'],"Submit","");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'],"Button","window.history.back()");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Back_To_Main();");

		# Steps
		$StepsObj[] = array($Lang['ReadingGarden']['SelectClass'],0);
		$StepsObj[] = array($Lang['ReadingGarden']['SelectReadingAssignment'],0);
		$StepsObj[] = array($Lang['ReadingGarden']['Confirmation'],1);
				
		$ClassAssignReading = $ReadingGardenLib->Get_Class_Assign_Reading_Target($ClassIDArr);
		$ClassAssignBookID = BuildMultiKeyAssoc($ClassAssignReading,array("ClassID","BookID"));	
		$ClassAssignReadingID = BuildMultiKeyAssoc($ClassAssignReading,array("ClassID","AssignedReadingID"));
		
		$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingIDArr);
		$AssignedReadingBookAssoc = BuildMultiKeyAssoc($AssignedReadingInfo,array("AssignedReadingID"));

		# Get Class List
		include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
		$YearLib = new year();
		$YearClassList =  $YearLib->Get_All_Classes();
		$ClassTitleLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
		$YearClassAssoc = BuildMultiKeyAssoc($YearClassList,array("YearClassID"),$ClassTitleLang,1);

		
		$x .= '<form name="form1" action="assign_reading_save_mapping.php" method="POST">';
		$x .= '<table width="100%" cellpadding=10px cellspacing=10px align="center">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td valign="top">'."\n";
					$x .= $this->GET_STEPS_IP25($StepsObj);
					$x .= '<table class="common_table_list_v30">'."\n";
						# col group
						$x .= '<col align="left" style="width: 10%;">'."\n";
						$x .= '<col align="left" style="width: 25%;">'."\n";
						$x .= '<col align="left" style="width: 30%;">'."\n";
						$x .= '<col align="left">'."\n";
			
						# table head
						$x .= '<thead>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['ReadingGarden']['Class'].'</th>'."\n";
								$x .= '<th class="sub_row_top">'.$Lang['ReadingGarden']['Book'].'</th>'."\n";
								$x .= '<th class="sub_row_top">'.$Lang['ReadingGarden']['Description'].'</th>'."\n";
								$x .= '<th class="sub_row_top">'.$Lang['General']['Remark'].'</th>'."\n";	
							$x .= '</tr>'."\n";
						$x .= '</thead>'."\n";
						
						$NewAssignReadingList = array();
						foreach((array)$ClassIDArr as $ClassID)
						{
							$rowspan= count($AssignedReadingIDArr)+1;
							$x .= '<tr>'."\n";
								$x .= '<td rowspan="'.$rowspan.'">'.$YearClassAssoc[$ClassID].'</td>'."\n";
							$x .= '</tr>'."\n";
							foreach((array)$AssignedReadingIDArr as $AssignedReadingID)
							{
								$thisInfo = $AssignedReadingBookAssoc[$AssignedReadingID];
								$err = '';
								if($ClassAssignReadingID[$ClassID][$AssignedReadingID])
									$err = "AssignmentAssigned";
								else if($ClassAssignBookID[$ClassID][$thisInfo['BookID']])
									$err = "BookAssigned";
								else // assign reading list 
									$NewAssignReadingList[$ClassID][] = $AssignedReadingID;
								
								$err = $err?'<span class="tabletextrequire">* '.$Lang['ReadingGarden']['WarningMsg'][$err].'</span>':$Lang['General']['EmptySymbol'];	
								$x .= '<tr>'."\n";
									$x .= '<td>'.($thisInfo['BookName']?$thisInfo['BookName']:$Lang['General']['EmptySymbol']).'</td>'."\n";
									$x .= '<td>'.($thisInfo['Description']?$thisInfo['Description']:$Lang['General']['EmptySymbol']).'</td>'."\n";
									$x .= '<td>'.$err.'</td>'."\n";
								$x .= '</tr>'."\n";
							}	
						}
					$x .= '</table>'."\n";
					$x .= '<span class="tabletextremark">'.$Lang['ReadingGarden']['AssignReadingRemarks'].'</span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					
					# hidden field 
					$x .= '<input type="hidden" name="AssignReadingData" value="'.urlencode(serialize($NewAssignReadingList)).'">';
					
					$x .= '<div class="edit_bottom_v30">'."\n";
						if(count($NewAssignReadingList)>0)
						$x .= $NextBtn.'&nbsp;'."\n";
						$x .= $BackBtn.'&nbsp;'."\n";
						$x .= $CloseBtn."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";			
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '</form>';
		
		return $x;
		
	}
	
//	function Get_Settings_Assign_Reading_Edit_Layer($AssignedReadingID='')
//	{
//		global $PATH_WRT_ROOT, $Lang, $ReadingGardenLib;
//		
//		if(trim($AssignedReadingID)!='')
//		{
//			$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingID);
//			$isEdit = 1;
//			
//			# Prepare Book Info
//			$BookInfo = $ReadingGardenLib->Get_Book_List($AssignedReadingInfo[0]["BookID"]);
//			
//			# Prepare Class Selection
//			$AssignedReadingTarget = $ReadingGardenLib->Get_Class_Assign_Reading_Target('','','',$AssignedReadingInfo[0]["BookID"]);
//			$AssignedReadingTargetAssoc = BuildMultiKeyAssoc($AssignedReadingTarget,"ClassID","AssignedReadingID",1);
//
//			# Parpare available class list
//			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//			$fcm = new form_class_manage();
//
//			$ParArr['BookID'] = $AssignedReadingInfo[0]["BookID"];
//			$RecommendBookMapping = $ReadingGardenLib->Get_Recommend_Book_Mappings($ParArr);
//			$AvailableYear = Get_Array_By_Key($RecommendBookMapping, "YearID");
//			
//			$AcademicYearID = Get_Current_Academic_Year_ID();
//			$classArr = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $AvailableYear, 0);
//			$AvailableClass = Get_Array_By_Key($classArr,"YearClassID");
//
//			$ClassOptionArr = array();
//			foreach((array)$AvailableClass as $thisClassID )
//			{
//				$ClassAssignedReadingID = $AssignedReadingTargetAssoc[$thisClassID];
//				if($ClassAssignedReadingID == $AssignedReadingID) // if the reading assignment assigned to the class is the assignment current editing, highlight the option in class selection
//				{ 
//					$ClassOptionArr[] = $thisClassID;
//					$SelectedArr[] = $thisClassID;
//				}
//				else if(!$ClassAssignedReadingID)// if the book has not been assigned to this class, this assignment can be assigned to this class
//					$ClassOptionArr[] = $thisClassID;
//			}
//		}
//
//		# Class Selection
//		include_once("libclass.php");
//		$lclass = new libclass();
//		$ClassSelection = $lclass->getSelectClassID(" id='YearClassIDArr[]' name='YearClassIDArr[]' multiple size=10 ",$SelectedArr,2,'','',$ClassOptionArr);
//		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All_Year_Class();");
//		
//		# Get Btns
//		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit");
//		$SubmitAndContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'],"Button"," CheckTBForm(1);");
////		$RemoveBtn = $this->GET_ACTION_BTN($Lang['Btn']['Remove'],"Button"," js_Delete_Assign_Reading(".$AssignedReadingID.");","","","","formbutton_alert");
//		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
//		
//		$x .= '';
//		$x .= '<form id="TBForm" name="TBForm" onsubmit="CheckTBForm(0); return false;">'."\n";
//			$x .= '<div id="AssignReadingLayerDiv">'."\n";
//				$x .= $this->Get_Thickbox_Return_Message_Layer();
//				$x .= '<div style="overflow:auto; height:450px;">'."\n";
//				$x .= '<table width="97%" cellpadding="10px" cellspacing="10px" align="center">'."\n";
//					$x .= '<tr>'."\n";
//						$x .= '<td valign="top">'."\n";	
//							$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
//								$x .= '<col class="field_title">';
//								$x .= '<col class="field_c">';
//								
////								# AssignReadingName
////								$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["AssignedReadingName"]);
////								$x .= '<tr>'."\n";
////									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['AssignReadingSettings']['AssignReadingName'].'</td>'."\n";
////									$x .= '<td>'."\n";
////										$x .= '<input name="AssignedReadingName" id="AssignedReadingName" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
////										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankAssignedReadingName",$Lang['ReadingGarden']['WarningMsg']['PleaseInputAssignReadingName'], "WarnMsgDiv");
////									$x .= '</td>'."\n";
////								$x .= '</tr>'."\n";
//								
//								# Select book
//								$PreviewDiv = $this->Get_Settings_Assign_Reading_Book_Preview('',$BookInfo);
//								$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["BookID"]);
//								$x .= '<tr>'."\n";
//									$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Book'].'</td>'."\n";
//									$x .= '<td>'."\n";
//										if(!$AssignedReadingInfo[0])
//											$x .= '[<a href="javascript:void(0);" onclick="js_Open_Select_Book_Layer();">'.$Lang['Btn']['Select'].'</a>]'."\n";
//										$x .= '<input type="hidden" name="BookID" id="BookID" class="textbox_name Mandatory" value="'.$thisValue.'">'."\n";
//										$x .= '<div id="BookInfoDiv">'.$PreviewDiv.'</div>';
//										$x .= $this->Spacer();
//										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankBookID",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBook'], "WarnMsgDiv");
//									$x .= '</td>'."\n";
//								$x .= '</tr>'."\n";
//								
//								# Description
//								$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["Description"]);
//								$x .= '<tr>'."\n";
//									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>'."\n";
//									$x .= '<td>'."\n";
//										$x .= $this->GET_TEXTAREA("Description", $thisValue);
////										$x .= '<textarea name="Description" id="Description" class="textbox" value="'.$thisValue.'" ></textarea>'."\n";
//									$x .= '</td>'."\n";
//								$x .= '</tr>'."\n";
//								
////								# Attachment
////								$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["Attachment"]);
////								if($thisValue)
////								{
////									$AttList = $ReadingGardenLib->Get_Assign_Reading_Attachment($thisValue);
////									$RowSpan = sizeof($AttList)+1;
////									for($i=0; $i<sizeof($AttList);$i++)
////									{
////										$AttName = basename($AttList[$i]);
////										$x .= '<tr class="AttRow'.$i.'">'."\n";
////											if($i==0)
////												$x .= '<td class="field_title" rowspan="'.$RowSpan.'" id="AttachmentTitleTD">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
////											$x .= '<td class="AttachmentFileTD">'."\n";
////													$x .= $AttName."\n";
////													$x .= '<input type="hidden" name="old_Attachment[]" class="Attachment" value="'.$AttName.'">'."\n";
////												$x .= '[<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row('.$i.')">'.$Lang['Btn']['Delete'].'</a>]'."\n";
////											$x .= '</td>'."\n";
////										$x .= '</tr>'."\n";
////									}
////								}
////								else
////								{
////									$x .= '<tr class="AttRow0">'."\n";
////										$x .= '<td class="field_title" rowspan="2" id="AttachmentTitleTD">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
////										$x .= '<td class="AttachmentFileTD">'."\n";
////												$x .= '<input type="file" name="Attachment[]" class="Attachment">'."\n";
////											$x .= '[<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row(0)">'.$Lang['Btn']['Delete'].'</a>]'."\n";
////										$x .= '</td>'."\n";
////									$x .= '</tr>'."\n";
////								}
////								$x .= '<tr id="AddAttachmentRow">'."\n";
////									$x .= '<td>'."\n";
////										$x .= '[<a href="javascript:void(0);" onclick="js_Add_Attachment();">'.$Lang['Btn']['AddMore'].'</a>]'."\n";
////									$x .= '</td>'."\n";
////								$x .= '</tr>'."\n";
//								
//								
//								# worksheet
//								$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["AnswerSheet"]);
//								if($AssignedReadingInfo[0] && trim($thisValue)=='' || !$BookInfo)
//								{
//									$isChecked = 0;
//									$thisValue = $BookInfo[0]['DefaultAnswerSheet'];
//								}
//								else
//									$isChecked = 1;
//
//								$x .= '<tr>'."\n";
//									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AnswerSheet'].'</td>'."\n";
//									$x .= '<td>'."\n";
//										$x .= $this->Get_Checkbox("UseAnswerSheet", "UseAnswerSheet", 1, $isChecked, $Class='', $Lang['ReadingGarden']['AssignReadingSettings']['UseAnswerSheet'], "js_Enable_AnswerSheet()").'<br/>'."\n";
//										$x .= '<input type="hidden" id="AnswerSheet" name="AnswerSheet" value="'.$thisValue.'">'."\n";
//										$x .= '<span id="AnswerSheetBtn">'."\n";
//											$x .= '[<a href="javascript:void(0);" onclick="js_Edit_AnswerSheet();">'.$Lang['Btn']['Edit'].'</a>]'."\n";
//											$x .= '[<a href="javascript:void(0);" onclick="js_View_AnswerSheet();">'.$Lang['Btn']['Preview'].'</a>]'."\n";
//										$x .= '</span>'."\n";
//										$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankAnswerSheet",$Lang['ReadingGarden']['WarningMsg']['PleaseComposeAsnwerSheet'], "WarnMsgDiv");
//									$x .= '</td>'."\n";
//								$x .= '</tr>'."\n";
//								
//								# Book Report Required
//								$x .= '<tr>'."\n";
//									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['BookReport'].'</td>'."\n";
//									$x .= '<td>'."\n";
//										$x .= $this->Get_Checkbox("BookReportRequired", "BookReportRequired", 1, $AssignedReadingInfo[0]["BookReportRequired"], $Class='', $Lang['ReadingGarden']['AssignReadingSettings']['UseBookReport'], "").'<br/>'."\n";
//									$x .= '</td>'."\n";
//								$x .= '</tr>'."\n";
//																
//								# Class Selection
//								$x .= '<tr>'."\n";
//									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AssignToClass'].'</td>'."\n";
//									$x .= '<td>'."\n";
//										$x .= '<span id="AssignedClassSelectionSpan">'.$ClassSelection.'</span>'."\n";
//										$x .= $SelectAllClassBtn."\n";
//										$x .= '<br />'."\n";
//										$x .= $this->MultiSelectionRemark()."\n";
//									$x .= '</td>'."\n";
//								$x .= '</tr>'."\n";
//																					
//							$x .= '</table>'."\n";
//						$x .= '</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</table>'."\n";
//				$x .= '</div>'."\n";
//				$x .= $this->MandatoryField();		
//				
//				# Btns
//				$x .= '<div class="edit_bottom_v30">'."\n";
//					$x .= $SubmitBtn;
//				if(trim($AssignedReadingID)=='') 
//					$x .= "&nbsp;".$SubmitAndContinueBtn;
////					if($AssignedReadingID)
////						$x .= "&nbsp;".$RemoveBtn;
//					$x .= "&nbsp;".$CloseBtn;
//				$x .= '</div>'."\n";
//		
//			$x .= '</div>'."\n";
//			
//			$x .= '<div id="SelectBookLayer" style="height:450px; display:none;"></div>'."\n";
//			$x .= '<iframe id="FileUploadFrame" name="FileUploadFrame" style="display:none;"></iframe>'."\n";
//			
//		# hidden field
//		$x .= '<input type="hidden" name="AssignedReadingID" id="AssignedReadingID" value="'.$AssignedReadingID.'">'."\n";
////		$TempPath = $AssignedReadingInfo[0]["Attachment"]?$AssignedReadingInfo[0]["Attachment"]:time();
////		$x .= '<input type="hidden" name="TempPath" id="TempPath" value="'.$TempPath.'">'."\n";
//		$x .= '<input type="hidden" name="isEdit" id="isEdit" value="'.$isEdit.'">'."\n";
//		$x .= '<input type="hidden" name="isPreview" id="isPreview" value="">'."\n";
//		
//		$x .= '</form>';
//
//		return $x;
//	}

	function Get_Settings_Edit_Assign_Reading($AssignedReadingID='')
	{
		global $PATH_WRT_ROOT, $Lang, $ReadingGardenLib;
		
		if(trim($AssignedReadingID)!='')
		{
			$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingID);
			$isEdit = 1;
			
			# Prepare Book Info
			$BookInfo = $ReadingGardenLib->Get_Book_List($AssignedReadingInfo[0]["BookID"]);
			
			# Prepare Class Selection
			$AssignedReadingTarget = $ReadingGardenLib->Get_Class_Assign_Reading_Target('','','',$AssignedReadingInfo[0]["BookID"]);
			$AssignedReadingTargetAssoc = BuildMultiKeyAssoc($AssignedReadingTarget,"ClassID","AssignedReadingID",1);

			# Parpare available class list
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$fcm = new form_class_manage();

			$ParArr['BookID'] = $AssignedReadingInfo[0]["BookID"];
			$RecommendBookMapping = $ReadingGardenLib->Get_Recommend_Book_Mappings($ParArr);
			$AvailableYear = Get_Array_By_Key($RecommendBookMapping, "YearID");
			
			$AcademicYearID = Get_Current_Academic_Year_ID();
			$classArr = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $AvailableYear, 0);
			$AvailableClass = Get_Array_By_Key($classArr,"YearClassID");

			$ClassOptionArr = array();
			foreach((array)$AvailableClass as $thisClassID )
			{
				$ClassAssignedReadingID = $AssignedReadingTargetAssoc[$thisClassID];
				
				if($ClassAssignedReadingID == $AssignedReadingID) // if the reading assignment assigned to the class is the assignment current editing, highlight the option in class selection
				{ 
					$ClassOptionArr[] = $thisClassID;
					$SelectedArr[] = $thisClassID;
				}
				else if(!$ClassAssignedReadingID)// if the book has not been assigned to this class, this assignment can be assigned to this class
					$ClassOptionArr[] = $thisClassID;
			}
			
			$NavigationTitle = $Lang['ReadingGarden']['AssignReadingSettings']['EditAssignReading'];
		}
		else
			$NavigationTitle = $Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading'];
		
		# Navigation
		$NavigationObj[] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'],"./assign_reading.php");
		$NavigationObj[] = array($NavigationTitle);
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj);

		# Class Selection
		include_once("libclass.php");
		$lclass = new libclass();
		$ClassSelection = $lclass->getSelectClassID(" id='YearClassIDArr[]' name='YearClassIDArr[]' multiple size=10 ",$SelectedArr,2,'','',$ClassOptionArr);
		$SelectAllClassBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All_Year_Class();");
		
		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","window.location='./assign_reading.php' ");
		
		$x .= '';
		$x .= '<form id="form1" name="form1" onsubmit="return CheckTBForm(0);" action="assign_reading_update.php" method="POST" enctype="multipart/form-data" >'."\n";
			$x .= $Navigation;
			$x .= '<div class="table_board">'."\n";
				$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
					$x .= '<col class="field_title">';
					$x .= '<col class="field_c">';
					
					# Select book
					$PreviewDiv = $this->Get_Settings_Assign_Reading_Book_Preview('',$BookInfo);
					$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["BookID"]);
					$ThickBoxNewLink = $this->Get_Thickbox_Link($this->TBHeight, $this->TBWidth,  'new', $Lang['Btn']['Select'], "js_Open_Select_Book_Layer()", $InlineID="FakeLayer", $Lang['Btn']['Select']);
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Book'].'</td>'."\n";
						$x .= '<td>'."\n";
							if(!$AssignedReadingInfo[0])
								$x .= '['.$ThickBoxNewLink.']'."\n";
							$x .= '<input type="hidden" name="BookID" id="BookID" class="textbox_name Mandatory" value="'.$thisValue.'">'."\n";
							$x .= '<div id="BookInfoDiv">'.$PreviewDiv.'</div>';
							$x .= $this->Spacer();
							$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankBookID",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBook'], "WarnMsgDiv");
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					# Description
					$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["Description"]);
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= $this->GET_TEXTAREA("Description", $thisValue);
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					# Book Report 
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['BookReport'].'</td>'."\n";
						$x .= '<td>'."\n";
						
							# Answer sheet
							$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["AnswerSheet"]);
							$DefaultAnswerSheetID = $BookInfo[0]['DefaultAnswerSheetID'];
							if($AssignedReadingInfo[0] && trim($thisValue)=='' || !$BookInfo)
							{
								$isChecked = 0;
//								$thisValue = $BookInfo[0]['DefaultAnswerSheet'];
							}
							else
								$isChecked = 1;
							$x .= $this->Get_Checkbox("UseAnswerSheet", "UseAnswerSheet", 1, $isChecked, $Class='', $Lang['ReadingGarden']['AnswerSheet'], "js_Enable_AnswerSheet()").'<br/>'."\n";
							$x .= '<input type="hidden" id="AnswerSheet" name="AnswerSheet" value="'.$thisValue.'">'."\n";
							$x .= '<div id="AnswerSheetBtn">'."\n";
								$x .= '<div id="SelectAnswerSheet">'."\n";
									$x .= $this->Get_Answer_Sheet_Template_Selection("AnswerSheetID", $DefaultAnswerSheetID, $all=0, $noFirst=0, $FirstTitle="", $OnChange="", $CategoryIDArr='');
								$x .= '</div>'."\n";
								$x .= '[<a href="javascript:void(0);" onclick="js_Edit_AnswerSheet();">'.$Lang['Btn']['Edit'].'</a>]'."\n";
								$x .= '[<a href="javascript:void(0);" onclick="js_View_AnswerSheet();">'.$Lang['Btn']['Preview'].'</a>]'."\n";
								$x .= '<span id="DeleteAnswerSheetBtn">'."\n";
									$x .= '[<a href="javascript:void(0);" onclick="js_Delete_AnswerSheet();">'.$Lang['Btn']['Delete'].'</a>]'."\n";
								$x .= '</span>'."\n";		
							$x .= '</div>'."\n";
							$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankAnswerSheet",$Lang['ReadingGarden']['WarningMsg']['PleaseComposeAsnwerSheet'], "WarnMsgDiv");
						
							# Upload File
							$x .= $this->Get_Checkbox("BookReportRequired", "BookReportRequired", 1, $AssignedReadingInfo[0]["BookReportRequired"], $Class='', $Lang['ReadingGarden']['AssignReadingSettings']['FileUpload'], "js_Toggle_Attachment()").'<br/>'."\n";
							
							# Attachment
							$x .= '<div id="AttachmentDiv">';
								$thisValue = $AssignedReadingInfo[0]["Attachment"];
								if($thisValue)
								{
									global $intranet_root;
									include_once("libfilesystem.php");
									$fs = new libfilesystem();
									$AttList = $ReadingGardenLib->Get_Assign_Reading_Attachment($thisValue);
									$AttachPath = $ReadingGardenLib->AssignReadingAttachmentPath."/".$AssignedReadingInfo[0]["Attachment"];
								}
								
								if(!empty($AttList))
								{
									include_once("libfilesystem.php");
									$fs = new libfilesystem();
//									$AttName = basename($AttList[0]);
									$pos = strrpos($AttList[0],"/");
									$AttName = substr($AttList[0],$pos+1);
									$Url = Get_Attachment_Download_Link($AttachPath, $AttName);
									$AttLink = '<a href="'.$Url.'" target="_blank">'.$AttName.'</a>';
	
									$x .= '<span id="AttachmentSpan">'.$AttLink.'<input type="hidden" name="old_Attachment[]" class="Attachment" value="'.$AttName.'"></span>'."\n";
								}
								else
								{
									$x .= '<span id="AttachmentSpan"><input type="file" name="Attachment[]" class="Attachment"></span>'."\n";
								}
								$x .= '<a href="javascript:void(0);" onclick="js_Remove_Attachment()">['.$Lang['Btn']['Delete'].']</a>'."\n";
								$TempPath = $thisValue?$thisValue:session_id().time();
								$x .= '<input type="hidden" name="TempPath" id="TempPath" value="'.$TempPath.'">'."\n";
								
								$x .= '<br>'."\n";
							$x .= '</div>'."\n";
						
							# Online Writing
							$LowerLimit = intranet_htmlspecialchars($AssignedReadingInfo[0]["LowerLimit"]);
							$UpperLimit = intranet_htmlspecialchars($AssignedReadingInfo[0]["WritingLimit"]);
							
							if($AssignedReadingInfo[0] && ($LowerLimit>=0 || $UpperLimit>=0))
							{
								$isChecked = 1;
								if($UpperLimit==-1)
									$UpperLimit = '';
								if($LowerLimit==-1)
									$LowerLimit = '';
								$OnlineWritingDesc = $AssignedReadingInfo[0]["OnlineWritingDesc"]; 	
							}
							else
							{
								$isChecked = 0;
								$LowerLimit = '';
								$UpperLimit = '';
							}
							$x .= $this->Get_Checkbox("OnlineWriting", "OnlineWriting", 1, $isChecked, $Class='', $Lang['ReadingGarden']['AssignReadingSettings']['OnlineWriting'], "js_Toggle_WordLimit()").'<br/>'."\n";
							$x .= '<div id="WordLimitDiv">'."\n";
								$x .= $Lang['ReadingGarden']['AssignReadingSettings']['WordLimit'].":";
								$x .= '<input name="LowerLimit" id="LowerLimit" class="textbox_num" value="'.$LowerLimit.'" maxlength="'.$this->MaxNumLength.'">'."\n";
								$x .= ' ~ '."\n";
								$x .= '<input name="WritingLimit" id="WritingLimit" class="textbox_num" value="'.$UpperLimit.'" maxlength="'.$this->MaxNumLength.'">'."\n";
								$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnNonIntegerWritingLimit",$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'], "WarnMsgDiv");
								$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnInvalidRange",$Lang['ReadingGarden']['WarningMsg']['InvalidRangeOfWordLimit'], "WarnMsgDiv");
								$x .= '<br>';
								$x .= '<div>'.$Lang['ReadingGarden']['OnlineWritingDesc'].':</div>';
								$x .= $this->GET_TEXTAREA("OnlineWritingDesc", $OnlineWritingDesc);
								
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
													
					# Class Selection
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AssignToClass'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<span id="AssignedClassSelectionSpan">'.$ClassSelection.'</span>'."\n";
							$x .= $SelectAllClassBtn."\n";
							$x .= '<br />'."\n";
							$x .= $this->MultiSelectionRemark()."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
																		
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
					$x .= $SubmitBtn;
					$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
			
		# hidden field
		$x .= '<input type="hidden" name="AssignedReadingID" id="AssignedReadingID" value="'.$AssignedReadingID.'">'."\n";
		$x .= '<input type="hidden" name="isEdit" id="isEdit" value="'.$isEdit.'">'."\n";
		$x .= '<input type="hidden" name="isPreview" id="isPreview" value="">'."\n";
		
		$x .= '</form>';

		return $x;
	}
	
	function Get_Settings_Assign_Reading_Select_Book_Showcase($CategoryID='',$Keyword='',$YearID='', $SelectedBookID='')
	{
		
		global $ReadingGardenLib;
		
		if(trim($YearID)!='')
		{
			$Par['YearID'] = $YearID;
			$ReccomendBookList = $ReadingGardenLib->Get_Recommend_Book_Mappings($Par);
			$ReccomendBookIDArr = Get_Array_By_Key($ReccomendBookList,"BookID");
		}

		$BookList = $ReadingGardenLib->Get_Book_List($ReccomendBookIDArr,$Keyword,$CategoryID,'','',0);
		$BookInfoAssoc = BuildMultiKeyAssoc($BookList, "BookID");

//		$x .= '<div style="overflow:auto; width:95%; ">'."\n";
		$x.= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\n";
			$x.= '<tbody>'."\n";
			
				$NumOfColumn = 4;
				$NumOfRow = ceil(min(count($ReccomendBookIDArr),count($BookInfoAssoc))/$NumOfColumn); 
				for($i=0; $i<$NumOfRow; $i++)
				{
					$x.= '<tr class="tablegreenrow2">'."\n";

					for($j=0; $j<$NumOfColumn; $j++)
					{
						$key = $NumOfColumn*$i + $j;
						$thisImg = '';
						$BookDisplay = '';	
						$thisSelectedClass = '';							
						
						$thisBookInfo = $BookList[$key];
						
						if($thisBookInfo)
						{
							$thisClassName = "Book_".$thisBookInfo['BookID'];
							$BookDisplay = $this->Format_Book_Display($thisBookInfo,1);
							$thisSelectedClass = $thisBookInfo['BookID']==$SelectedBookID?"booking_table_item_select":"booking_table_item";
						
//							if($thisBookInfo['BookCoverImage'])
//								$thisImg = '<img src="'.$ReadingGardenLib->BookCoverImagePath.'/'.$thisBookInfo['BookCoverImage'].'" width="100px" height="120px">'."\n";
							$thisImg= $ReadingGardenLib->Get_Book_Cover_Image($thisBookInfo['BookCoverImage'],100,120);
						}	 						
						$x.= '<td align="center" width="20%" >'."\n";
							$x.= '<a class="'.$thisSelectedClass.' tablelink '.$thisClassName.'" style="width:95%" onclick="js_Select_Book('.$thisBookInfo['BookID'].')" href="javascript:void(0);" id="'.$thisBookInfo['BookID'].'">'.$thisImg.'<br>'.$BookDisplay.'<br></a>'."\n";
						$x.= '</td>'."\n";
					}
						
					$x.= '</tr>'."\n";		
				}
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
//		$x.= '</div>'."\n";		
		
		return $x;		
	}
	
	function Get_Settings_Assign_Reading_Select_Book_Layer($CategoryID='',$Keyword='',$YearID='')
	{
		global $Lang, $ReadingGardenLib;
		
		# Category Seletion
		$SelectTitle = $Lang['ReadingGarden']['SelectTitleArr']['AllCategory'];
		$CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID","",$SelectTitle," js_Reload_Book_Showcase(); ");

		# Form Seletion 
		include_once("form_class_manage_ui.php");
		$fcm_ui = new form_class_manage_ui();
		$FormSelection = $fcm_ui->Get_Form_Selection("BookYearID", $YearID, " js_Reload_Book_Showcase(); ", 1, 0, 0);

		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<div>'."\n";
			$x .= '<div class="table_filter">'."\n";
				$x .= $FormSelection."\n";
				$x .= $CategorySelection."\n";
			$x .= '</div>'."\n";
			$x .= '<div class="Conntent_search"><form onsubmit="js_Reload_Book_Showcase(); return false;"><input type="text" id="BookKeyword" name="BookKeyword"></form></div>'."\n";
		$x .= '</div>'."\n";
		$x.= '<div id="ItemShowcase" style="text-align:center; padding: 8px; width: 99%; z-index: -1;">'."\n";
			$x .= $Showcase."\n";
		$x.= '</div>'."\n";
		$x.= '<div class="edit_bottom_v30">'."\n";
			$x.= $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "tb_remove();");
		$x.= '</div>'."\n";
			
		return $x;
	}
	
	function Get_Settings_Assign_Reading_Book_Preview($BookID='', $BookInfo='')
	{
		global $Lang, $ReadingGardenLib;
		
//		if(trim($BookInfo)=='' && trim($BookID)=='')
		if(!$BookInfo && !$BookID)
			return false;
		
//		if(trim($BookInfo)=='')
		if(!$BookInfo)
			$BookInfo = $ReadingGardenLib->Get_Book_List($BookID);
			
		
//		if($BookInfo[0]['BookCoverImage'])
//		{
//			$thisImg = '<img border=2 src="'.$ReadingGardenLib->BookCoverImagePath.'/'.$BookInfo[0]['BookCoverImage'].'" width="100px" height="120px" style="margin:2px;">'."\n";
			$thisImg= $ReadingGardenLib->Get_Book_Cover_Image($BookInfo[0]['BookCoverImage'],100,120,2);
			$x .= '<div style="float:left">'.$thisImg.'</div>'."\n";
//		}
		$x .= '<div>'.$BookInfo[0]['BookName'].'</div>'."\n";
		if($BookInfo[0]['Author'])
		{
			$x .= '<div>['.$BookInfo[0]['Author'].']</div>'."\n";
		}
		
		return $x ;
	}
	
	function Get_Answer_Sheet_Preview_Assigned_Reading_Info_Table($AssignedReadingID)
	{
		global $ReadingGardenLib,$Lang;
		$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingID);
		
		$x .= '<table style="width:100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			
			# book
			$thisValue = intranet_htmlspecialchars($AssignedReadingInfo[0]["BookName"]);
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Book'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $thisValue;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";

			# description
			$thisValue = $AssignedReadingInfo[0]["Description"]?intranet_htmlspecialchars($AssignedReadingInfo[0]["Description"]):$Lang['General']['EmptySymbol'];
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $thisValue;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";

			# Assigned Class
			$thisValue = $AssignedReadingInfo[0]["AssignedClass"]?intranet_htmlspecialchars($AssignedReadingInfo[0]["AssignedClass"]):$Lang['General']['EmptySymbol'];
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AssignedClass'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $thisValue;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";

																
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Answer_Sheet_Preview_UI($AnswerSheet,$ViewOnly)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		include_once($PATH_WRT_ROOT."includes/libform.php");
		
		$lform = new libform();
		
		$x.= '<script language="javascript" src="/templates/forms/layer.js"></script>'."\n";
		$x.= '<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>'."\n";
		$x.= '<script language="Javascript">'."\n";
		$x.= 'var replyslip = \''.$Lang['ReadingGarden']['AnswerSheet'].'\';'."\n";
		$x.= '</script>'."\n";
		$x.= ''."\n";
		$x.= '<br />'."\n";
		$x.= '<p>'."\n";
		$x.= '<table width="100%" align="center" border="0">'."\n";
		$x.= '<form name="ansForm" method="post" action="">'."\n";
		$x.= '<input type="hidden" id="qStr" name="qStr" value="'.htmlspecialchars(stripslashes($AnswerSheet),ENT_QUOTES).'">'."\n";
		$x.= '<input type="hidden" name="aStr" value="">'."\n";
		$x.= '<input type="hidden" name="isPreview" value="">'."\n";
		$x.= '</form>'."\n";
		$x.= ''."\n";
		$x.= '<tr>'."\n";
			$x.= '<td align="left">'."\n";
				$x.= '<div id="AnswerSheetDiv"></div>'."\n";
			$x.= '<script language="Javascript">'."\n";
			$x.= ''.$lform->getWordsInJS().''."\n";
			$x .= 'var MarksTxt = \''.$Lang['ReadingGarden']['Mark(s)'].'\';';
			$x .= 'var AnswerTxt = \''.$Lang['ReadingGarden']['Answer'].'\';';
			$x.= 'var s = $(\'input#qStr\').val();'."\n";
				$x.= 's = s.replace(/"/g, \'&quot;\');'."\n";
			$x.= 'var sheet= new Answersheet();'."\n";
			$x.= '// attention: MUST replace \'"\' to \'&quot;\''."\n";
			$x.= 'sheet.qString=s;'."\n";
			//$x.= 'sheet.aString="";'."\n";
			$x.= '//edit submitted application'."\n";
			$x.= 'sheet.mode=1;'."\n";
			$x.= 'sheet.answer=sheet.sheetArr();'."\n";
			$x.= ''."\n";
			$x.= 'Part3 = \'\';'."\n";
			$x.= '//document.write(editPanel());'."\n";
			$x.= '$(document).ready(function(){'."\n";
					$x.= '$(\'div#AnswerSheetDiv\').html(editPanel());'."\n";
					$x.= '$(\'div#blockInput\').html(sheet.writeSheet());'."\n";
				$x.= '});'."\n";
			$x.= '</script>'."\n";
			$x.= '</td>'."\n";
		$x.= '</tr>'."\n";
		$x.= ''."\n";
		$x.= '<tr>'."\n";
			$x.= '<td class="dotline"><img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" /></td>'."\n";
		$x.= '</tr>'."\n";
		$x.= ''."\n";
		$x.= '<tr>'."\n";
			$x.= '<td align="center">'."\n";
				if(!$ViewOnly)
				$x.= $this->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "js_Edit(); return false;","editbtn") ."\n";
				$x.= $this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "window.close(); return false;","cancelbtn") ."\n";
			$x.= '</td>'."\n";
		$x.= '</tr>'."\n";
		$x.= '</table>'."\n";

		return $x;
	}
	
	function Get_Answer_Sheet_Edit_UI($AnswerSheet)
	{
		global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libform.php");
		
		$lform = new libform();

		$x.= '<script language="javascript" src="/templates/forms/layer.js"></script>'."\n";
		$x.= '<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>'."\n";
		$x.= '<script language="Javascript">'."\n";
		$x.= 'var replyslip = \''.$Lang['ReadingGarden']['AnswerSheet'].'\';'."\n";
		$x.= '</script>'."\n";
		$x.= ''."\n";
		$x.= '<br />'."\n";
		$x .= '<form name="ansForm" action="" method="post">'."\n";
				$x .= '<input type="hidden" id="qStr" name="qStr" value="'.htmlspecialchars(stripslashes($AnswerSheet),ENT_QUOTES).'">'."\n";
				$x .= '<input type="hidden" id="aStr" name="aStr" value="">'."\n";
				$x .= '<input type="hidden" name="isPreview" value="">'."\n";
		$x .= '</form>'."\n";
		$x .= '<div id="TemplateEditDiv">'."\n";
				$x .= '</div>'."\n";
		$x .= '<div class="edit_bottom_v30">'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					$x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","if(finish()){js_Save();}","submit_btn")."\n";
					$x .= $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.close();","cancel_btn")."\n";
					$x .= '<p class="spacer"></p>'."\n";
			   $x .= '</div>'."\n";
		
		$x .= '<script>'."\n";
		$x .= $lform->getWordsInJS()."\n";
		$x .= 'var answer_sheet	= \'Content\';'."\n";
		$x .= 'var space10 	= \'<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" />\';'."\n";
		$x .= 'var add_btn 	= \''.str_replace("'", "\'",$this->GET_BTN($Lang['Btn']['Add'], 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2')).'\''."\n";
		//$x .= 'var order_name	= \''.$i_Sports_Order.'\';'."\n";

		$x .= 'var replyslip = \''.$Lang['ReadingGarden']['AnswerSheet'].'\';'."\n";
		$x .= 'var MoveUpBtn = \''.$Lang['Button']['MoveUp'].'\';'."\n";
		$x .= 'var MoveDownBtn = \''.$Lang['Button']['MoveDown'].'\';'."\n";
		$x .= 'var DeleteBtn = \''.$Lang['Button']['Delete'].'\';'."\n";
		$x .= 'var EditBtn = \''.$Lang['Button']['Edit'].'\';'."\n";
		$x .= 'var MarksTxt = \''.$Lang['ReadingGarden']['Mark(s)'].'\';';
		$x .= 'var AnswerTxt = \''.$Lang['ReadingGarden']['Answer'].'\';';
		$x .= 'var need2checkform = true;';
		$x .= 'var pls_complete_all_fields = \''.$Lang['ReadingGarden']['WarningMsg']['RequestCompleteAnswerSheetAllAnswerScore'].'\';';
		$x .= 'var s = $(\'input#qStr\').val();
				s = s.replace(/"/g, \'&quot;\');
				$(\'input#qStr\').val(s);
				var form_templates = new Array(); 
			   	var sheet= new Answersheet();
				sheet.qString = s;
				sheet.mode=0; // 0:edit 1:fill in application
				sheet.answer=sheet.sheetArr();
				sheet.templates=form_templates;
				$(document).ready(function(){
					$(\'div#TemplateEditDiv\').html(editPanel());
					$(\'div#blockInput\').html(sheet.writeSheet());
				});
			   </script>';
		
		return $x;
	}
	
//	function Get_Settings_Assign_Reading_Management_UI($YearID='',$field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
//	{
//		global $Lang;
//		
//		$ThickBoxHeight = 600;
//		$ThickBoxWidth = 750;
//		
//		# Add Btn
//		$thisOnclick="js_Create_Assign_Reading()";
//		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading'], $thisOnclick, $InlineID="FakeLayer", $Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading']);
//
//		# Page Btn
//		$pages_arr[] = array($Lang['ReadingGarden']['AssignReadingSettings']['ManageAssignment'],"assign_reading_manage.php","",1);
//		$pages_arr[] = array($Lang['ReadingGarden']['AssignReadingSettings']['AssignReading'],"assign_reading.php","");
//
////		# Form Seletion 
////		include_once("form_class_manage_ui.php");
////		$fcm_ui = new form_class_manage_ui();
////		$FormSelection = $fcm_ui->Get_Form_Selection("YearID", $YearID, " js_Reload_Class_Selection(); ", 1, 1, 0);
//		
//		# Category Selection
////		$SelectTitle = $Lang['ReadingGarden']['SelectTitleArr']['AllCategory'];
////		$CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID","",$SelectTitle," js_Relaod_Book_List(); ");
//		
////		$x .= '<br><br>'."\n";
//		$x .= '<form id="form1" name="form1" method="post" action="assign_reading_manage.php" onsubmit="js_Reload_Assign_Reading_Table(); return false;">'."\n";
//			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
//				$x .= '<tr>'."\n";
//					$x .= '<td>'."\n";
//					
//						# Main Table  
//						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td>'."\n";
//									$x .= '<div class="content_top_tool">'."\n";
//										$x .= '<div class="Conntent_tool">'."\n";
//											$x .= $ThickBoxNewLink."\n";
//										$x .= '</div>'."\n";
//										$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
//										$x .= '<br style="clear: both;">'."\n";
//									$x .= '</div>'."\n";
//									$x .= $this->GET_CONTENT_TOP_BTN($pages_arr);
////									$x .= '<div class="table_filter">'."\n";
////										$x .= $FormSelection."\n";
////										$x .= '<span id="ClassFilterSpan">'.$this->Get_Ajax_Loading_Image().'</span>'."\n";
////									$x .= '</div>'."\n";
//									$x .= '<div class="common_table_tool">'."\n";
////								        $x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'ReportID[]\',\'edit.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
//										$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'AssignedReadingID[]\',\'js_Delete_Assign_Reading()\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
//									$x .= '</div>'."\n";		
//								$x .= '</td>'."\n";		
//							$x .= '</tr>'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td>'."\n";
//									$x .= '<div id="AssignReadingDBTable">'."\n";
//										$x .= $this->Get_Ajax_Loading_Image()."\n";
//									$x .= '</div>'."\n";
//								$x .= '</td>'."\n";		
//							$x .= '</tr>'."\n";
//						$x .= '</table>'."\n";
//						  
//					$x .= '</td>'."\n";		
//				$x .= '</tr>'."\n";
//			$x .= '</table>'."\n";
//			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
//			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
//			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
//			$x .= '<input type="hidden" name="page_size_change" value="" />';
//			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
//			$x .= '<input type="hidden" name="AnswerSheet" value="" />';	
//			$x .= '<input type="hidden" name="ViewOnly" value="1" />';
//		$x .= '</form>'."\n";		
//		$x .= '<br><br>'."\n";
//		
//		return $x;
//	}
	

		
	function Get_Settings_State_Setting_UI()
	{
		global $Lang;
		
		$ThickBoxHeight = 450;
		$ThickBoxWidth = 750;
		
		# Add Btn
		$thisOnclick="js_Create_State()";
		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['StateSettings']['NewState'], $thisOnclick, $InlineID="FakeLayer", $Lang['ReadingGarden']['StateSettings']['NewState']);
		
		$StateSettingTable = $this->Get_Settings_State_Setting_Table();
		
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="state.php" onsubmit="return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $ThickBoxNewLink."\n";
							$x .= '</div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'StateID[]\',\'js_Edit_State()\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'StateID[]\',\'js_Remove_State()\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span id="StateSettingTable">'.$StateSettingTable.'</span>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Settings_State_Setting_Table()
	{
		global $Lang, $ReadingGardenLib;
		
		$ThickBoxHeight = 550;
		$ThickBoxWidth = 750;
		
		$StateArr = $ReadingGardenLib->Get_State_Info();
		
		$x .= '<table class="common_table_list">'."\n";
			$x .= '<col style="width:10px">'."\n";
			$x .= '<col style="width:120px">'."\n";
			$x .= '<col style="width:20%">'."\n";
			$x .= '<col style="width:30%">'."\n";
			$x .= '<col style="width:10%">'."\n";
			$x .= '<col>'."\n";
			$x .= '<col style="width:10px">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>#</th>'."\n";	
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['Logo'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['StateName'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['Description'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['ScoreRange'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['LastModifiedDate'].'</th>'."\n";
					$x .= '<th>'.$this->Get_Checkbox($ID="CheckAllState", "", "", 0, '', '', 'js_Check_All_State(this.checked)').'</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
				if($SizeOfstate = sizeof($StateArr))
				{
					$LastStateScore = '';
					for($i=0; $i<$SizeOfstate; $i++)
					{
						$thisStateInfo = $StateArr[$i];
						$Img = $thisStateInfo['ImagePath']?'<img src="'.$ReadingGardenLib->StateLogoPath.'/'.$thisStateInfo['ImagePath'].'" width="100" height="120">':'';
						$thisOnclick="js_Edit_State(".$thisStateInfo['StateID'].")";
						$ThickBoxEditLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  '', $Lang['ReadingGarden']['StateSettings']['EditState'], $thisOnclick, $InlineID="FakeLayer",$thisStateInfo['StateName']);
						
						if($i==0)
							$ScoreRange = $thisStateInfo['ScoreRequired']."+";
						else if($thisStateInfo['ScoreRequired'] == ($LastStateScore-1))
							$ScoreRange = $thisStateInfo['ScoreRequired'];
						else
							$ScoreRange = $thisStateInfo['ScoreRequired']." ~ ".($LastStateScore-1);
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.($i+1).'</td>'."\n";
							$x .= '<td>'.$Img.'</td>'."\n";
							$x .= '<td>'.$ThickBoxEditLink.'</td>'."\n";
							$x .= '<td>'.($thisStateInfo['Description']?nl2br(htmlspecialchars($thisStateInfo['Description'])):$Lang['General']['EmptySymbol']).'</td>'."\n";
							$x .= '<td>'.$ScoreRange.'</td>'."\n";
							$x .= '<td>'.$thisStateInfo['DateModified'].'</td>'."\n";
							$x .= '<td>'.$this->Get_Checkbox("", "StateID[]", $thisStateInfo['StateID'], 0, 'StateIDChk', '', '').'</td>'."\n";		
						$x .= '</tr>'."\n";
						
						$LastStateScore = $thisStateInfo['ScoreRequired'];
					}				
				}
				else
				{
					$x .= '<tr><td align="center" colspan="7"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Settings_State_Setting_Edit_Layer($StateID='')
	{
		global $Lang, $ReadingGardenLib;
		if(trim($StateID) != '')
			$StateInfo = $ReadingGardenLib->Get_State_Info($StateID);
			
		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","isAddMore=0;");
		$SubmitAndContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'],"Button","isAddMore=1; CheckTBForm();");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");
		
		$x .= '';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form id="TBForm" name="TBForm" onsubmit=" CheckTBForm(); return false;">'."\n";
			$x .= '<table width="98%" cellpadding=10px cellspacing=10px align="center" height="290px">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							
							# Name
							$thisValue = intranet_htmlspecialchars($StateInfo[0]["StateName"]);
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StateSettings']['StateName'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="StateName" id="StateName" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankStateName",$Lang['ReadingGarden']['WarningMsg']['PleaseInputStateName'], "WarnMsgDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";

							# Logo
							if($thisValue = $StateInfo[0]["ImagePath"])
							{
								$thisImg = '<img src="'.$ReadingGardenLib->StateLogoPath.'/'.$StateInfo[0]['ImagePath'].'" width="100" height="120">';
							}
							else
							{
								$thisImg .= '<input type="file" name="ImagePath" id="ImagePath" class="Mandatory"  >'."\n";
							}
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StateSettings']['Logo'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span id="ImageUpload">'.$thisImg.'</span>'."\n";
									$x .= '[<a href="javascript:void(0);" onclick="js_Delete_Logo()">'.$Lang['Btn']['Delete'].'</a>]'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankImagePath",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectAnImage'], "WarnMsgDiv");
									$x .= '<input type="hidden" name="OldImagePath" value="'.$thisValue.'">';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";

							# Description
							$thisValue = intranet_htmlspecialchars($StateInfo[0]["Description"]);
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['StateSettings']['Description'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $this->GET_TEXTAREA("Description", $thisValue)."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Score Required
							$thisValue = intranet_htmlspecialchars($StateInfo[0]["ScoreRequired"]);
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StateSettings']['ScoreRequired'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="ScoreRequired" id="ScoreRequired" class="textboxnum Mandatory Numeric" value="'.$thisValue.'" maxlength="'.$this->MaxNumLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnBlankScoreRequired",$Lang['ReadingGarden']['WarningMsg']['PleaseInputScoreRequired'], "WarnMsgDiv");
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnNonIntegerScoreRequired",$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'], "WarnMsgDiv");
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnExistScoreRequired",$Lang['ReadingGarden']['WarningMsg']['ScoreRequiredExist'], "WarnMsgDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";

						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
			if(!($StateID > 0))
				$x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
			# hidden field
			$x .= '<input type="hidden" name="StateID" id="StateID" value="'.$StateID.'">'."\n";
		
		$x .= '</form>';	
		$x .= '<iframe id="FileUploadFrame" name="FileUploadFrame" style="display:none;"></iframe>'."\n";
			
		$x .= $this->FOCUS_ON_LOAD("TBForm.StateName");
		
		return $x;
	}

	function Get_State_Student_View()
	{
		global $Lang, $ReadingGardenLib;
		
		$StateArr = $ReadingGardenLib->Get_State_Info();
		
		$x .= '<table class="common_table_list">'."\n";
			$x .= '<col style="width:10px">'."\n";
			$x .= '<col style="width:120px">'."\n";
			$x .= '<col>'."\n";
			$x .= '<col>'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>#</th>'."\n";	
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['Logo'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['StateName'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['StateSettings']['ScoreRange'].'</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
				if($SizeOfstate = sizeof($StateArr))
				{
					$LastStateScore = '';
					for($i=0; $i<$SizeOfstate; $i++)
					{
						$thisStateInfo = $StateArr[$i];
						$Img = $thisStateInfo['ImagePath']?'<img src="'.$ReadingGardenLib->StateLogoPath.'/'.$thisStateInfo['ImagePath'].'" width="100" height="120">':'';
						
						if($i==0)
							$ScoreRange = $thisStateInfo['ScoreRequired']."+";
						else if($thisStateInfo['ScoreRequired'] == ($LastStateScore-1))
							$ScoreRange = $thisStateInfo['ScoreRequired'];
						else
							$ScoreRange = $thisStateInfo['ScoreRequired']." ~ ".($LastStateScore-1);
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.($i+1).'</td>'."\n";
							$x .= '<td>'.$Img.'</td>'."\n";
							$x .= '<td>'.$thisStateInfo['StateName'].'</td>'."\n";
							$x .= '<td>'.$ScoreRange.'</td>'."\n";
						$x .= '</tr>'."\n";
						
						$LastStateScore = $thisStateInfo['ScoreRequired'];
					}				
				}
				else
				{
					$x .= '<tr><td align="center" colspan="7"><br>'.$Lang['General']['NoRecordAtThisMoment'].'<br><br></td></tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Scoring_Rule_Student_View()
	{
		global $Lang, $ReadingGardenLib;
		
		$x .= '<div class="table_board">'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tbody>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['PressLikeButton'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= ' + '.$ReadingGardenLib->Settings[ACTION_SCORE_LIKE]."\n";
							$x .= ' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['Every'].' '.' '.Get_Plural_Display($ReadingGardenLib->Settings[ACTION_SETTING_TIMES_OF_LIKE],$Lang['ReadingGarden']['ScoringSettings']['Time'])."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['WriteComment'].'</td>'."\n";
						$x .= ' <td> + '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_SCORE_COMMENT]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['StartAThreadInForum'].'</td>'."\n";
						$x .= ' <td> + '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_SCORE_START_THREAD]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['FollowAThreadInForum'].'</td>'."\n";
						$x .= ' <td> + '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_SCORE_FOLLOW_THREAD]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['AddReadingRecord'].'</td>'."\n";
						$x .= ' <td> + '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_SCORE_ADD_READING_RECORD]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['WriteBookReport'].'</td>'."\n";
						$x .= ' <td> + '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_SCORE_ADD_BOOK_REPORT]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['TeacherRecommendReport'].'</td>'."\n";
						$x .= ' <td> + '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_SCORE_RECOMMEND_REPORT]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
//						$x .= '<tr>'."\n";
//							$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['CommentLength'].'</td>'."\n";
//							$x .= '<td>'."\n";
//								$x .= ' + '.$this->GET_TEXTBOX_NUMBER('CommentLengthScore','ScoringSettings['.ACTION_SCORE_COMMENT_LENGTH.']',$ReadingGardenLib->Settings[ACTION_SCORE_COMMENT_LENGTH], "NumericField")."\n";
//								$x .= ' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['Every'].' '.$this->GET_TEXTBOX_NUMBER('CommentMoreThanEvery','ScoringSettings['.ACTION_SETTING_COMMENT_LENGTH.']',$ReadingGardenLib->Settings[ACTION_SETTING_COMMENT_LENGTH], "NumericField").' '.$Lang['ReadingGarden']['ScoringSettings']['Words'].' '."\n";
//								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'],"CommentLengthScoreWarnMsg CommentMoreThanEveryWarnMsg WarnDiv");
//							$x .= '</td>'."\n";	
//						$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['Login'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= ' + '.$ReadingGardenLib->Settings[ACTION_SCORE_LOGIN]."\n";
							$x .= ' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['MoreThan'].' '.$ReadingGardenLib->Settings[ACTION_SETTING_LOGIN_MORE_THAN].' '.$Lang['ReadingGarden']['ScoringSettings']['MinsWithin24Hours']."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDaysWithoutLogin'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= ' - '.$ReadingGardenLib->Settings[ACTION_PENALTY_WITHOUT_LOGIN]."\n";
							$x .= ' '.' '.$Lang['ReadingGarden']['ScoringSettings']['For'].' '.$Lang['ReadingGarden']['ScoringSettings']['Every'].' '.$ReadingGardenLib->Settings[ACTION_SETTING_DAYS_WITHOUT_LOGIN].' '.' '.$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDays']."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['CommentDeletedByAdmin'].'</td>'."\n";
						$x .= ' <td> - '."\n";	
							$x .= $ReadingGardenLib->Settings[ACTION_PENALTY_COMMENT_DELETED]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['ForumThreadDeletedByAdmin'].'</td>'."\n";
						$x .= ' <td> - '."\n";	
							$x .=  $ReadingGardenLib->Settings[ACTION_PENALTY_THREAD_DELETED]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= ' <td class="field_title">'.$Lang['ReadingGarden']['ScoringSettings']['ForumPostDeletedByAdmin'].'</td>'."\n";
						$x .= ' <td> - '."\n";	
							$x .=  $ReadingGardenLib->Settings[ACTION_PENALTY_POST_DELETED]."\n";
						$x .= '</td>'."\n";	
					$x .= '</tr>'."\n";
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Book_Cover_Image($Path, $Width, $Height)
	{
		global $ReadingGardenLib,$image_path,$LAYOUT_SKIN;
		
		if(trim($Path)=='')
			$cover_image_path = $image_path."/".$LAYOUT_SKIN.'/reading_scheme/temp_book_cover.gif';
		else
			$cover_image_path = $ReadingGardenLib->BookCoverImagePath.'/'.$Path;
		$x = '<img src="'.$cover_image_path.'" width="'.$Width.'px" height="'.$Height.'px"  border=0/>';
		
		return $x;
	}
	
	function Get_Management_Announcement_UI($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang;
		
		# Add Btn
		$NewBtn = $this->Get_Content_Tool_v30('new','edit_announcement.php',$Lang['Btn']['New']);
		
		$AnnouncementDBTable = $this->Get_Management_Announcement_DBTable($field, $order, $pageNo, $numPerPage, $Keyword);
		
		$x .= '<form id="form1" name="form1" method="post" action="announcement.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $NewBtn."\n";
							$x .= '</div>'."\n";
							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="'.htmlspecialchars($Keyword).'"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'AnnouncementID[]\',\'edit_announcement.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'AnnouncementID[]\',\'delete_announcement.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span id="StateSettingTable">'.$AnnouncementDBTable.'</span>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Management_Announcement_DBTable($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_settings_announcement_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		//$ReadingGardenLib = new libreadinggarden();
		
		$field = ($field=='')? 1 : $field;
		$order = ($order=='')? 1 : $order;
		$pageNo = ($pageNo=='')? 1 : $pageNo;
		
		if (isset($ck_settings_announcement_page_size) && $ck_settings_announcement_page_size != "") $page_size = $ck_settings_announcement_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Management_Announcement_DBTable_Sql($Keyword);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		$li->count_mode= 1;
		
		$li->field_array = array("Title", "StartDate", "EndDate", "TargetClass", "LastModifiedBy", "DateModified");
		$li->column_array = array(18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Title'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['StartDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['EndDate'])."</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['TargetClass'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedBy'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("AnnouncementID[]")."</th>\n";
		$li->no_col = $pos+2;
	
		$x .= $li->display();
	
		return $x;	
	}
	
	function Get_Management_Announcement_Edit_UI($AnnouncementID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib,$intranet_root;
		
		if(trim($AnnouncementID)!='')
		{
			$AddEditNav = $Lang['Btn']['Edit'];
			$AnnouncementArr = $ReadingGardenLib->Get_Class_Announcement('',$AnnouncementID);
			$AnnouncementInfo = $AnnouncementArr[0];
			$ClassIDArr = Get_Array_By_Key($AnnouncementArr,"ClassID");
		}
		else
			$AddEditNav = $Lang['Btn']['New'];
		
		# Navigation
		$NavigationObj[] = array($Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'],"./announcement.php");
		$NavigationObj[] = array($AddEditNav);
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj);
		
		#Class Selection
		include_once("libclass.php");
		$lclass = new libclass();
		$YearClassSelection = $lclass->getSelectClassID(" id='ClassID' name='ClassID[]' multiple size=10 class='Mandatory' ",$ClassIDArr,2);
		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID',true);");
		
		$x .= $Navigation;
		$x .= '<form id="form1" name="form1" action="announcement_update.php" method="POST" enctype="multipart/form-data" onsubmit="return js_Check_Form();">'."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
				$x .= '<col class="field_title">';
				$x .= '<col class="field_c">';
	
				# Title
				$thisValue = htmlspecialchars($AnnouncementInfo['Title']);
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Title'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="Title" id="Title" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankTitle",$Lang['ReadingGarden']['WarningMsg']['PleaseInputAnnouncementTitle'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
	
				# Date
				$StartDate = $AnnouncementInfo['StartDate'];
				$EndDate = $AnnouncementInfo['EndDate'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Period'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_DATE_PICKER("StartDate",$StartDate).'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$this->GET_DATE_PICKER("EndDate",$EndDate);
						$x .= $this->Get_Form_Warning_Msg("WarnInvalidDateFormat",$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat'], "WarnMsgDiv");
						$x .= $this->Get_Form_Warning_Msg("WarnInvalidDateRange",$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Message
				//$Message = $AnnouncementInfo['Message'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Message'].'</td>'."\n";
					$x .= '<td>'."\n";
						# Components size
						$msg_box_width = "100%";
						$msg_box_height = 350;
						$obj_name = "Message";
						$editor_width = $msg_box_width;
						$editor_height = $msg_box_height;
						
						include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
						$oFCKeditor = new FCKeditor('Message',$editor_width,$editor_height);
						$oFCKeditor->Value = $AnnouncementInfo['Message'];
						
						$x .= $oFCKeditor->Create2();
						$x .= $this->Get_Form_Warning_Msg("WarnBlankMessage",$Lang['ReadingGarden']['WarningMsg']['PleaseInputMessage'], "WarnMsgDiv");
										
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Attachment
				$thisValue = intranet_htmlspecialchars($AnnouncementInfo["Attachment"]);
				$AttachPath = $intranet_root.$ReadingGardenLib->AnouncementAttachmentPath."/".$AnnouncementInfo["Attachment"];
				if($thisValue)
				{
					include_once("libfilesystem.php");
					$fs = new libfilesystem();
					$AttList = $ReadingGardenLib->Get_Announcement_Attachment($thisValue);
				}
				
				if(sizeof($AttList))
				{
					$RowSpan = sizeof($AttList)+1;
					for($i=0; $i<sizeof($AttList);$i++)
					{
						$AttName = $fs->get_file_basename($AttList[$i]);
						$target_e = getEncryptedText($AttachPath.'/'.$AttName);
						//$AttLink = '<a href="'.$PATH_WRT_ROOT.'home/download_attachment.php?target='.urlencode($AttachPath.'/'.$AttName).'" target="_blank">'.$AttName.'</a>';
						$AttLink = '<a href="'.$PATH_WRT_ROOT.'home/download_attachment.php?target_e='.$target_e.'" target="_blank">'.$AttName.'</a>';
						$x .= '<tr class="AttRow'.$i.'">'."\n";
							if($i==0)
								$x .= '<td class="field_title" rowspan="'.$RowSpan.'" id="AttachmentTitleTD">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
							$x .= '<td class="AttachmentFileTD">'."\n";
									$x .= $AttLink."\n";
									$x .= '<input type="hidden" name="old_Attachment[]" class="Attachment" value="'.$AttName.'">'."\n";
								$x .= '<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row('.$i.')">['.$Lang['Btn']['Delete'].']</a>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					}
				}
				else
				{
					$x .= '<tr class="AttRow0">'."\n";
						$x .= '<td class="field_title" rowspan="2" id="AttachmentTitleTD">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
						$x .= '<td class="AttachmentFileTD">'."\n";
								$x .= '<input type="file" name="Attachment[]" class="Attachment">'."\n";
							$x .= '<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row(0)">['.$Lang['Btn']['Delete'].']</a>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				}
				$x .= '<tr id="AddAttachmentRow">'."\n";
					$x .= '<td>'."\n";
						$x .= '<a href="javascript:void(0);" onclick="js_Add_Attachment();">['.$Lang['Btn']['AddMore'].']</a>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Target Class
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Class'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $YearClassSelection."\n";
						$x .= $YearClassSelectAllBtn."\n";
						$x .= $this->Spacer()."\n";
						$x .= $this->MultiSelectionRemark()."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankClassID",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectClass'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
			$x .= '</table>'."\n";
			$TempPath = $AnnouncementInfo["Attachment"]?$AnnouncementInfo["Attachment"]:time();
			$x .= '<input type="hidden" name="TempPath" id="TempPath" value="'.$TempPath.'">'."\n";
			$x .= '<input type="hidden" name="AnnouncementID" id="AnnouncementID" value="'.$AnnouncementID.'">'."\n";
			
			# edit bottom
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit");
			$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./announcement.php'");
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$CancelBtn;
			$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;	
	}
	
	function Get_Book_Report_Comment_List_UI($BookReportID='',$pageNo='',$numPerPage='')
	{
		global $ReadingGardenLib, $Lang, $page_size; 
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		//$ReadingGardenLib = new libreadinggarden();
		
		$field = 1;
		$order = 0;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		$page_size = $numPerPage?$numPerPage:50;
		
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Book_Report_Comment_DBTable_Sql($BookReportID);
		
		$li->sql = $sql;
		$li->IsColOff = "ReadingSchemeReportComment";
		
		$li->field_array = array("brc.DateInput");
//		$li->column_array = array(18,18,18,18,18,18);
//		$li->wrap_array = array(0,0,0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['BookName'])."</th>\n";
//		$li->column_list .= "<th width='100px' >".$Lang['ReadingGarden']['CoverImage']."</td>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['CallNumber'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Author'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Publisher'])."</th>\n";
		//$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Description'])."</td>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Category'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("BookID[]")."</td>\n";
		$li->no_col = $pos+2;
	
		$x .= '<form name="form1" id="form1" method="POST">';
			$x .= $li->display();
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>';
		
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Close'],"button","window.close()");
		
		$x .= '<form name="CommentForm" id="CommentForm" method="POST" action="update_comment.php?Action=add">';	
		$x .= '<table class="form_table_v30">';
			$x .= '<col class="field_title">';
			$x .= '<col class="field_c">';
			$x .= '<tbody>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Comment'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_TEXTAREA('Comment','');
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $SubmitBtn;
			$x .= "&nbsp;".$CloseBtn;
		$x .= '</div>'."\n";
		$x .= '<input type="hidden" name="BookReportID" value="'.$BookReportID.'">';
		$x .= '</form>';

		return $x;		
	}
	
	function Get_Book_Report_Comment_Bubble($BookReportInfo)
	{
		global $image_path, $LAYOUT_SKIN, $Lang, $ReadingGardenLib;
		
		$DateInput = $BookReportInfo['DateInput'];
		$Comment = nl2br(htmlspecialchars($BookReportInfo['Comment']));
		$PhotoLink = $BookReportInfo['PersonalPhotoLink']?$BookReportInfo['PersonalPhotoLink']:"{$image_path}/myaccount_personalinfo/samplephoto.gif";
		$Name = $BookReportInfo['NameLang'];
		
		$x .= '<tr>'."\n";
			$x .= '<td width="120">'."\n";
				$x .= '<table cellspacing="0" cellpadding="3" border="0"><tbody>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td align="center" width="120">'."\n";
							$x .= '<table cellspacing="0" cellpadding="3" border="0" width="50"><tbody>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td>'."\n";
										$x .= '<div id="div">'."\n";
											$x .= '<span>'."\n";
												$x .= '<img height="60" border="0" width="45" src="'.$PhotoLink.'">'."\n";
											$x .= '</span>'."\n";
										$x .= '</div>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</tbody></table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td align="center" width="120" class="tabletext">'.$Name.'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</tbody></table>'."\n";
			$x .= '</td>'."\n";
			$x .= '<td align="left" valign="top">'."\n";
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td height="7" width="25"><img height="7" width="25" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_01.gif"></td>'."\n";
						$x .= '<td height="7" background="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_02.gif"><img height="7" width="12" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_02.gif"></td>'."\n";
						$x .= '<td height="7" width="9"><img height="7" width="9" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_03.gif"></td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td background="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_04.gif" width="25" valign="top"><img height="40" width="25" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_04b.gif"></td>'."\n";
						$x .= '<td bgcolor="#ffffff" valign="top" class="tabletext">'."\n";
							$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%"><tbody>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td align="right" class="tabletext forumtablerow">'."\n";
										$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%"><tbody>'."\n";
											$x .= '<tr>'."\n";
												$x .= '<td align="left">'.$Lang['Identity']['Teacher'].'</td>'."\n";
												$x .= '<td align="right" class="tabletext">'.$DateInput.'</td>'."\n";
											$x .= '</tr>'."\n";
										$x .= '</tbody></table>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="tabletext">'.$Comment.'</td>'."\n";
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td height="10" class="tabletext"><img height="10" width="10" src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif"></td>'."\n";
								$x .= '</tr>'."\n";
								if($ReadingGardenLib->isAdmin)
								{
									$x .= '<tr>'."\n";
										$x .= '<td height="5" class="dotline"><img height="5" width="10" src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif"></td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td height="10" class="tabletext">'."\n";
											$x .= '<table cellspacing="0" cellpadding="2" border="0" width="100%"><tbody>'."\n";
												$x .= '<tr>'."\n";
													$x .= '<td>&nbsp;</td>';
													$x .= '<td width="15px" align="right">'.$this->GET_LNK_DELETE("javascript:void(0)","","js_Delete_Report_Comment(".$BookReportInfo['BookReportCommentID'].",".$BookReportInfo['RecordType'].",".$BookReportInfo['UserID'].");").'</td>'."\n";
												$x .= '</tr>'."\n";
											$x .= '</tbody></table>'."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
								}
							$x .= '</tbody></table>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td background="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_06.gif" width="9">&nbsp;</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td height="10" width="25"><img height="10" width="25" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_07.gif"></td>'."\n";
						$x .= '<td height="10" background="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_08.gif"><img height="10" width="9" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_08.gif"></td>'."\n";
						$x .= '<td height="10" width="9"><img height="10" width="9" src="'."{$image_path}/{$LAYOUT_SKIN}".'/ecomm/forum_bubble_re_09.gif"></td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</tbody></table>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		return $x;
		
	}
	
	function Get_Announcement_List_DBTable_UI($ClassID='',$AnnouncementType='',$pageNo='',$order='',$field='',$numPerPage='', $Keyword='')
	{
		global $Lang, $image_path, $LAYOUT_SKIN;
//		if($this->isStudent)
//		{
//			$ClassInfo = $this->Get_Student_ClassInfo($_SESSION['UserID']);
//			$ClassID = $ClassInfo[0]['YearClassID'];
//		}
		
		$Navigation[] = array($Lang['ReadingGarden']['Home'],"../");
		$Navigation[] = array($Lang['ReadingGarden']['Announcement'],"");
		
		$x .= $this->GET_NAVIGATION_IP25($Navigation,"navigation_2")."\n";
		
		$x .= '<form name="search_form" id="search_form" onsubmit="js_Reload_Announcement_List(); return false;">'."\n";
			$x.= '<div class="content_top_tool">'."\n";
			$x .= $this->Get_Search_Box_Div("Keyword",$Keyword);
			$x.= '</div>'."\n";
		$x .= '</form>'."\n";
		$x .= '<form name="form1" id="form1">'."\n";
			$x.= '<div class="content_top_tool">'."\n";
				$x.= '<div class="thumb_list_tab">'."\n";
					$x.= '<a id="AnnouncementTab1" title="Announcement" href="javascript:void(0);" onclick="js_Select_Announcement_Type(\'1\')"><span><img align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/reading_scheme/icon_anncouce_arrow.gif" border=0>'.$Lang['ReadingGarden']['Announcement'].'</span></a>'."\n";
					$x.= '<em>|</em><a id="AnnouncementTab2" title="Award" href="javascript:void(0);" onclick="js_Select_Announcement_Type(\'2\')"><span><img align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/reading_scheme/icon_anncouce_award.gif" border=0>'.$Lang['ReadingGarden']['AwardScheme'].'</span></a>'."\n";
					$x.= '<em>|</em><a id="AnnouncementTab" class="thumb_list_tab_on" href="javascript:void(0);" onclick="js_Select_Announcement_Type(\'\')"><span>'.$Lang['ReadingGarden']['All'].'</span></a></div>'."\n";
				$x.= '<p class="spacer"></p>'."\n";
				$x.= '<input type="hidden" id="AnnouncementType" name="AnnouncementType" value="">'."\n";
			$x.= '</div>'."\n";
			$x .= '<div class="table_board">'."\n";
				$x .= '<div id="AnnoucementListDiv"></div>'."\n";
			$x .= '</div>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>';
		
		return $x;
	}
	
	function Get_Announcement_List_DBTable($ClassID, $AnnouncementType='',$field='', $order='', $pageNo='',$numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_announcement_list_page_size, $ReadingGardenLib;
		
		if($ReadingGardenLib->isStudent)
		{
			$ClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($_SESSION['UserID']);
			$ClassID = $ClassInfo[0]['YearClassID'];
		}
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		//$ReadingGardenLib = new libreadinggarden();
		
		$field = ($field==='')? 2 : $field;
		$order = ($order==='')? 0 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_announcement_list_page_size) && $ck_announcement_list_page_size != "") $page_size = $ck_announcement_list_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$AnnouncementListDBSql = $ReadingGardenLib->Get_Announcement_List_DBTable_Sql($ClassID, $AnnouncementType, $Keyword);
		
		$li->sql = $AnnouncementListDBSql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("a.Title",  "NameField", "DATE(a.StartDate)");
		$li->column_array = array(18,18,18);
		$li->wrap_array = array(0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Title'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['PostedBy'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['StartDate'])."</th>\n";
		$li->no_col = $pos+1;
	
		$x .= $li->display("","award_table_list");
	
		return $x;	
	}
	
	// 20110511
	function Settings_Award_Scheme_Generate_Winner_UI($AwardSchemeID, $WinnerStatus, $field='', $order='', $pageNo='', $numPerPage='', $AwardLevelID='')
	{
		global $Lang, $ReadingGardenLib;
		
		$AwardInfo = $ReadingGardenLib->Get_Award_Scheme_List($AwardSchemeID);
		$DBTable = $this->Settings_Award_Scheme_Generate_Winner_DBTable($AwardSchemeID, $WinnerStatus, $field, $order, $pageNo, $numPerPage, $AwardLevelID);
		
		$NavigationArr[] = array($Lang['ReadingGarden']['AwardScheme'],"./award_scheme.php");
		$NavigationArr[] = array($Lang['ReadingGarden']['GenerateWinner'],"");
		
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationArr);
		
		$ContentToolArr[] = array("assign_to_student",'javascript:checkEditMultiple(document.form1,\'WinnerID[]\',\'winner_update.php?action=assign\',\'test\')',$Lang['ReadingGarden']['AssignWinner']);
		$ContentToolArr[] = array("unassign_to_student",'javascript:checkRemove(document.form1,\'WinnerID[]\',\'winner_update.php?action=unassign\',\''.$Lang['ReadingGarden']['ConfirmArr']['UnassgnWinner'].'\')',$Lang['ReadingGarden']['UnassignWinner']);
		$ContentTool = $this->Get_DBTable_Action_Button_IP25($ContentToolArr);
		
		# filter
//		$AwardLevelSelection = Get_Award_Scheme_Level_Selection($ID_Name, $Selected);
		$option =  array();
		$option[1] = $Lang['ReadingGarden']['InWinnerList'];
		$option[2] = $Lang['ReadingGarden']['NotInWinnerList'];
		$WinnerFilter = getSelectByAssoArray($option, " id='WinnerStatus' name='WinnerStatus' onchange='this.form.submit();' ",$WinnerStatus,1, 0);

		$option =  array();
		$AwardLevelList = $ReadingGardenLib->Get_Award_Scheme_Level($AwardSchemeID);
		foreach($AwardLevelList as $thisLevel)
		{
			$option[$thisLevel['AwardLevelID']] = $thisLevel['LevelName'];
		}
		$AwardLevelFilter = getSelectByAssoArray($option, " id='AwardLevelID' name='AwardLevelID' onchange='this.form.submit();' ",$AwardLevelID, 1, 0, $Lang['ReadingGarden']['AllAwardLevel']);
		
		$x .= $Navigation;
		$x .= '<form name="form1" id="form1" method="POST" action="generate_winner.php">'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AwardScheme'].'</td>'."\n";
					$x .= '<td>'.$AwardInfo[0]['AwardName'].'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br/>'."\n";
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
				$x .= '<tr>'."\n";
					$x .='<td valign="bottom">'."\n";
						$x .= '<div class="table_filter">'.$WinnerFilter.$AwardLevelFilter.'</div>';
					$x .='</td>'."\n";
					$x .='<td valign="bottom">'."\n";
						$x .= $ContentTool;		
					$x .='</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="table_board">'."\n";
				$x .= $DBTable."\n";
			$x .= '</div>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';
			$x .= '<input type="hidden" name="AwardSchemeID" value="'.$AwardSchemeID.'" />';		
		$x .= '</form>';
		
		return $x;
	}
	
	//201105011
	function Settings_Award_Scheme_Generate_Winner_DBTable($AwardSchemeID, $WinnerStatus='', $field='', $order='', $pageNo='', $numPerPage='', $AwardLevelID='')
	{
		
		global $Lang, $page_size, $ck_settings_generate_winner_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$DBTablePar = $ReadingGardenLib->Settings_Get_Award_Scheme_Generate_Winner_DBTable_Param($AwardSchemeID, $WinnerStatus, $AwardLevelID);
		$AwardType =  $DBTablePar['AwardType'];
		$CategoryInfoArr = $ReadingGardenLib->Settings_Get_Category_Info($DBTablePar['CategoryArr']);
		$CategoryInfoArr = BuildMultiKeyAssoc($CategoryInfoArr, "CategoryID");

//		$page_size  = $numPerPage?$numPerPage:$ck_settings_generate_winner_page_number;
		if (isset($ck_settings_generate_winner_page_size) && $ck_settings_generate_winner_page_size != "") $page_size = $ck_settings_generate_winner_page_size;

		$posOfLevel = ($AwardType == AWARD_SCHEME_TYPE_GROUP?3:5)+count($CategoryInfoArr);
		$field = ($field=='')? $posOfLevel : $field; // Book Read
		$order = ($order=='')? 1 : $order;
		$pageNo = ($pageNo=='')? 1 : $pageNo;
		
		$li = new libdbtable2007($field,$order,$pageNo);
			
		$li->sql = $DBTablePar['sql'];
		
		$li->IsColOff = "IP25_table";
		
		if($AwardType == AWARD_SCHEME_TYPE_GROUP)
		{
			$li->field_array = array("Class","BookRead","ReportSubmit");
			$li->column_array = array(18,18,18);
			$li->wrap_array = array(0,0,0);
			$pos = 0;
			
			$li->column_list .= "<th width='1'  class='num_check'>#</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Class'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['BookRead'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['ReportSubmit'])."</th>\n";
			
			foreach((array)$DBTablePar['CategoryArr'] as $thisCategoryID)
			{
				$thisCategory = $CategoryInfoArr[$thisCategoryID];
				$li->field_array[]= "C".$thisCategory['CategoryID']; 
				$li->column_array[] = 18;
				$li->wrap_array[] = 0; 
				$li->column_list .= "<th>".$li->column_IP25($pos++, $thisCategory['CategoryName'])."</th>\n";
			}
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['AwardLevel'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['IsWinner'])."</th>\n";
			$li->column_list .= "<th width='1'>".$li->check("WinnerID[]")."</td>\n";
			
			$li->field_array[] = "l.level";  // GenerateLevel
			$li->column_array[] = 18; // GenerateLevel
			$li->wrap_array[] = 0;  // GenerateLevel

			$li->field_array[] = "IsWinner";  // isWinner
			$li->column_array[] = 18; // isWinner
			$li->wrap_array[] = 0;  // isWinner
			
			$li->no_col = $pos+2;
			$li->no_msg = $Lang['ReadingGarden']['NoClassesMeetTheRequirement'];
		}
		else
		{
			$li->field_array = array("Class", "ClassNumber", "StudentName", "BookRead", "ReportSubmit");
			$li->column_array = array(18,18,18,18,18);
			$li->wrap_array = array(0,0,0,0,0);
			
			$pos = 0;
			$li->column_list .= "<th width='1' class='num_check'>#</td>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Class'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['General']['ClassNumber'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Student'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['BookRead'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['ReportSubmit'])."</th>\n";

			foreach((array)$DBTablePar['CategoryArr'] as $thisCategoryID)
			{
				$thisCategory = $CategoryInfoArr[$thisCategoryID];
				$li->field_array[]= "C".$thisCategory['CategoryID']; 
				$li->column_array[] = 18;
				$li->wrap_array[] = 0; 
				$li->column_list .= "<th>".$li->column_IP25($pos++, $thisCategory['CategoryName'])."</th>\n";
			}
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['AwardLevel'])."</th>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['IsWinner'])."</th>\n";
			$li->column_list .= "<th width='1'>".$li->check("WinnerID[]")."</th>\n";
			
			$li->field_array[] = "l.level";  // GenerateLevel
			$li->column_array[] = 18; // GenerateLevel
			$li->wrap_array[] = 0;  // GenerateLevel

			$li->field_array[] = "IsWinner";  // isWinner
			$li->column_array[] = 18; // isWinner
			$li->wrap_array[] = 0;  // isWinner
			
			$li->no_col = $pos+2;
			$li->no_msg = $Lang['ReadingGarden']['NoStudentsMeetTheRequirement'];
		}
		
		$x .= $li->display("","award_table_list");
		$x .= '<input type="hidden" name="AwardList" value="'.serialize($DBTablePar['AwardList']).'">';
	
		return $x;	
	}
	
	/************************************ Marcus Part End *************************************/
	
	/************************************ Carlos Part Start *************************************/
	function Get_Book_Index($field='', $order='', $pageNo='', $numPerPage='', $Keyword='', $CategoryID='', $BookSourceArr= '')
	{
		global $Lang, $plugin, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		
		# Book Source Filter
		if(empty($BookSourceArr))
		{
			$BookSourceArr = array(BOOK_SOURCE_INPUT_BY_ADMIN,BOOK_SOURCE_INPUT_FROM_ELIB); // default hide books input by students
		}
		
		$filter.= '<!--# Filter layer start #-->'."\n";
		$filter.= '<div class="table_filter">'."\n";
			$filter.= '<div class="selectbox_group_v30 selectbox_group_filter"> <a onclick="MM_showHideLayers(\'status_option\',\'\',\'show\')" href="javascript:void(0);">'.$Lang['ReadingGarden']['SelectTitleArr']['BookSource'].'</a> </div>'."\n";
			$filter.= '<p class="spacer"></p>'."\n";
			$filter.= '<div class="selectbox_layer selectbox_group_layer" id="status_option" style="z-index:1">'."\n";
				foreach(array(BOOK_SOURCE_INPUT_BY_ADMIN,BOOK_SOURCE_INPUT_FROM_ELIB,BOOK_SOURCE_INPUT_BY_STUDENT) as $BookSource)
				{
					$Checked = (in_array($BookSource, (array)$BookSourceArr))?"checked":"";
					$filter.= $this->Get_Checkbox("BookSource_".$BookSource, "BookSource[]", $BookSource, $Checked, 'BookSource', $Lang['ReadingGarden']['BookSourceInput'][$BookSource]).'<br>';
				}
				$filter.= '<p class="spacer"></p>'."\n";
				$filter.= '<div class="edit_bottom">'."\n"; 
					$filter.= $this->Get_Small_Btn($Lang['Btn']['Apply'],"button","js_Check_Book_Source_Filter(); ")."&nbsp;";
					$filter.= $this->Get_Small_Btn($Lang['Btn']['Cancel'],"button","MM_showHideLayers('status_option','','hide')");
				$filter.= '</div>'."\n";
			$filter.= '</div>'."\n";
		$filter.= '</div>'."\n";
		$filter.= '<!--# Filter layer end #-->'."\n";
		
		# Category Selection
		$SelectTitle = $Lang['ReadingGarden']['SelectTitleArr']['AllCategory'];
		$CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID",$CategoryID,$SelectTitle," Get_Book_Table(); ");
		$ThickBoxHeight = 550;
		$ThickBoxWidth = 750;
		
		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['AddBook'], "Get_Book_Edit_Form(1,'')", $InlineID="FakeLayer", $Lang['Btn']['New']);
		if($plugin['eLib']==true && $plugin['eLib_license']>0){
			$ThickBoxFromELIBLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth, 'sub_btn', $Lang['ReadingGarden']['FromELIBRARY'], "Get_ELIB_Book_Layer()","FakeLayer",$Lang['ReadingGarden']['FromELIBRARY']); 
		}
		
		$ImportBookFromCSV =  "<a href= \"import_book_step1.php\" class=\"sub_btn\"> ".$Lang['ReadingGarden']['Book']."</a>";
			
		# import menu
		$menuBar .= '
			<div class="btn_option" id="ImportDiv">
				<a onclick="js_Clicked_Option_Layer(\'import_option\', \'btn_import\');" id="btn_import" class="import option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Import'].'</a>
				<br style="clear: both;">
				<div onclick="js_Clicked_Option_Layer_Button(\'import_option\', \'btn_import\');" id="import_option" class="btn_option_layer" style="visibility: hidden;">
				'.$ImportBookFromCSV.'
				'.$ThickBoxFromELIBLink.'				
				</div>
			</div>';
				
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="index.php" onsubmit="Get_Book_Table(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $ThickBoxNewLink."\n";									
											$x .= $menuBar."\n";
										$x .= '</div>'."\n";
										$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td valign="bottom">'."\n";
											$x .= $filter."\n";
											$x .= '<div class="table_filter">'."\n";
												$x .= $CategorySelection."\n";
											$x .= '</div>'."\n";
											$x .= $this->Spacer();
										$x .= '</td>'."\n";
										$x .= '<td valign="bottom">'."\n";
											$x .= '<div class="common_table_tool">'."\n";
												$x .= '<a class="tool_photo" href="javascript:Toggle_Cover();">'."\n";
													$x .= '<span id=DisplayCoverText>'.$Lang['ReadingGarden']['DisplayAllCover'].'</span>'."\n";
												$x .= '</a>'."\n";
												$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'BookID[]\',\'Get_Book_Edit_Form(0, false);\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
												$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'BookID[]\',\'remove_book.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
											$x .= '</div>'."\n";
										$x .= '</td>'."\n";		
									$x .= '</tr>'."\n";
									$x .= '</table>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DivBookTable">'."\n";
										//$x .= $this->Get_Book_DBTable($field, $order, $pageNo, $numPerPage, $Keyword, $CategoryID)."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Book_DBTable($field='', $order='', $pageNo='', $numPerPage='', $Keyword='', $CategoryID='', $BookSourceArr='')
	{
		global $Lang, $page_size, $ck_book_list_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		//$ReadingGardenLib = new libreadinggarden();
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_book_list_page_size) && $ck_book_list_page_size != "") $page_size = $ck_book_list_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Book_List_DBTable_Sql($Keyword,$CategoryID,$BookSourceArr);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("b.BookName", "CallNumber", "Author", "Publisher", "CategoryName", "b.DateModified");
		$li->column_array = array(18,18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['BookName'])."</th>\n";
//		$li->column_list .= "<th width='100px' >".$Lang['ReadingGarden']['CoverImage']."</td>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['CallNumber'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Author'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Publisher'])."</th>\n";
		//$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Description'])."</td>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Category'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("BookID[]")."</th>\n";
		$li->no_col = $pos+2;
	
		$x .= $li->display();
	
		return $x;	
	}
	
	function Get_ELIB_Book_Layer()
	{
		global $Lang;
		
		//$x .= '<div style="color:green">*'.$Lang['ReadingGarden']['WarningMsg']['BooksHaveImportedFromeLibrary'].'</div>'; 
		$x .= '<br />';
		$x .= '<form id="ELIBBookForm" name="ELIBBookForm" method="post" action="" onsubmit="Get_ELIB_Book_Table(); return false;">';
		$x .= '<div id="DivELIBBookTable" style="height:500px; overflow:auto;">';
		$x .= $this->Get_ELIB_Book_Table();
		$x .= '</div>';
		//$x .= '<div style="color:green">*'.$Lang['ReadingGarden']['WarningMsg']['BooksHaveImportedFromeLibrary'].'</div>';
		$x .= '</form>';
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Add'],"submit","Import_ELIB_Book();","submit_btn").'
					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
		
		return $x;
	}
	
	function Get_ELIB_Book_Table()
	{
		global $PATH_WRT_ROOT,$Lang,$intranet_root, $ReadingGardenLib;
		
		//$ReadingGardenLib = new libreadinggarden();
		
		$book_list = $ReadingGardenLib->Get_ELIB_Book_List();
		$size_book_list = sizeof($book_list);
		
		$x .= '<table class="common_table_list_v30">
				<thead>
					<tr>
					<th>#</th>
					<th>'.$Lang['ReadingGarden']['BookName'].'</th>
					<th>'.$Lang['ReadingGarden']['Author'].'</th>
					<th>'.$Lang['ReadingGarden']['Category'].'</th>
					<th>'.$Lang['ReadingGarden']['SubCategory'].'</th>
					<th>'.$Lang['ReadingGarden']['Publisher'].'</th>
					<th>'.$Lang['ReadingGarden']['Source'].'</th>
					<th width="1"><input type="checkbox" onclick="(this.checked)?setChecked(1,document.getElementById(\'ELIBBookForm\'),\'BookID[]\'):setChecked(0,document.getElementById(\'ELIBBookForm\'),\'BookID[]\')" name="checkmaster" /></th>
					</tr>
				</thead>
				<tbody>';
		if($size_book_list==0){
			$x .= '<tr><td colspan="10" align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
		}
		for($i=0;$i<$size_book_list;$i++){
			list($elib_book_id,$book_name,$author,$category,$subcategory,$publisher,$source,$book_id,$preface,$language,$isbn,$ebook_id)=$book_list[$i];
			$ebook_id = trim($ebook_id);
			$book_name = trim($book_name)==''?$Lang['General']['EmptySymbol']:$book_name;
			$author = trim($author)==''?$Lang['General']['EmptySymbol']:$author;
			$category = trim($category)==''?$Lang['General']['EmptySymbol']:$category;
			$subcategory = trim($subcategory)==''?$Lang['General']['EmptySymbol']:$subcategory;
			$publisher = trim($publisher)==''?$Lang['General']['EmptySymbol']:$publisher;
			$source = trim($source)==''?$Lang['General']['EmptySymbol']:$source;
			$x .= '<tr>';
			$x .= '<td>'.($i+1).'</td>';
			//$marker = (trim($book_id)!='')?'<span style="color:green">*</span>':'';
			$x .= '<td>'.htmlspecialchars($book_name).'</td>';
			$x .= '<td>'.htmlspecialchars($author).'</td>';
			$x .= '<td>'.htmlspecialchars($category).'</td>';
			$x .= '<td>'.htmlspecialchars($subcategory).'</td>';
			$x .= '<td>'.htmlspecialchars($publisher).'</td>';
			$x .= '<td>'.htmlspecialchars($source).'</td>';
			$x .= '<td>';
			if($ebook_id=='')
				$x .= '<input type="checkbox" name="BookID[]" value="'.$elib_book_id.'" />';
			else
				$x .= $Lang['General']['EmptySymbol'];
			$x .= '</td>';
			$x .= '</tr>';
		}
		
		$x .= '	</tbody>
			  </table>
			<br />';
		
		return $x;
	}
	/*
	function Get_Book_Table($Keyword="")
	{
		global $PATH_WRT_ROOT,$Lang,$intranet_root;
		
		$ReadingGardenLib = new libreadinggarden();
		
		$book_list = $ReadingGardenLib->Get_Book_List("",$Keyword);
		$size_book_list = sizeof($book_list);
		
		$x .= '<table class="common_table_list_v30">
				<thead>
					<tr>
					<th>#</th>
					<th>'.$Lang['ReadingGarden']['CoverImage'].'</th>
					<th>'.$Lang['ReadingGarden']['CallNumber'].'</th>
					<th>'.$Lang['ReadingGarden']['BookName'].'</th>
					<th>'.$Lang['ReadingGarden']['Author'].'</th>
					<th>'.$Lang['ReadingGarden']['Publisher'].'</th>
					<th>'.$Lang['ReadingGarden']['Description'].'</th>
					<th>'.$Lang['ReadingGarden']['Category'].'</th>
					<th>'.$Lang['ReadingGarden']['LastModifiedDate'].'</th>
					<th>
						<div class="table_row_tool row_content_tool">
							<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Book_Edit_Form(1,\'\');" title="'.$Lang['ReadingGarden']['AddBook'].'" class="add_dim thickbox" ></a>
						</div></th>
					</tr>
				</thead>
				<tbody>';
		if($size_book_list==0){
			$x .= '<tr><td colspan="10" align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
		}
		for($i=0;$i<$size_book_list;$i++){
			list($book_id,$call_number,$book_name,$author,$publisher,$description,$cover_image,$ebook_id,$language,$category_id,$category_code,$category_name,$display_order,$date_modified,$last_modified_by)=$book_list[$i];
			$x .= '<tr>';
			$x .= '<td>'.($i+1).'</td>';
			if(trim($cover_image)!=""){
				$cover_image_path = $ReadingGardenLib->BookCoverImagePath.'/'.$cover_image;
				$x .= '<td><a href="'.$cover_image_path.'" target="_blank" title="'.$Lang['ReadingGarden']['ViewCoverImage'].'"><img src="'.$cover_image_path.'" width="100px" height="120px" /></td>';
			}else
				$x .= '<td>&nbsp;</td>';
			$x .= '<td>'.$call_number.'</td>';
			$x .= '<td>'.htmlspecialchars($book_name).'</td>';
			$x .= '<td>'.htmlspecialchars($author).'</td>';
			$x .= '<td>'.htmlspecialchars($publisher).'</td>';
			$x .= '<td>'.htmlspecialchars($description).'</td>';
			$x .= '<td>'.htmlspecialchars($category_name).'</td>';
			$x .= '<td>'.$date_modified.'</td>';
			$x .= '<td>
					<div class="table_row_tool">
						<a title="'.$Lang['Btn']['Edit'].'" href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Book_Edit_Form(0,\''.$book_id.'\')" class="edit_dim thickbox" ></a>
						<a title="'.$Lang['Btn']['Delete'].'" class="delete_dim" href="#" onclick="Delete_Book('.$book_id.');return false;"></a>
					</div>
				   </td>';
			$x .= '</tr>';
		}
		
		$x .= '	</tbody>
			  </table>
			<br />';
		
		return $x;
	}
	*/
	function Get_Book_Edit_Form($is_new,$book_id="")
	{
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $image_path, $LAYOUT_SKIN, $ReadingGardenLib;
		
		include_once("form_class_manage_ui.php");
		//$fcm_ui = new form_class_manage_ui();
		$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
		
		# Book Detail
		$BookInfo=array();
		$BookFormID = array();
		if($is_new!=1){
			$BookInfo = $ReadingGardenLib->Get_Book_List($book_id);
			$FormParArr['BookID'] = $BookInfo[0]['BookID'];
			$BookFormArr = $ReadingGardenLib->Get_Recommend_Book_Mappings($FormParArr);
			for($i=0;$i<sizeof($BookFormArr);$i++){
				$BookFormID[] = $BookFormArr[$i]['YearID'];
			}
		}
		# Category
		/*
		$category_list = $ReadingGardenLib->Get_Category_List();
		$CategorySelection = '<select id="SelCategoryID" name="SelCategoryID">
								<option value="">-- '.$Lang['Btn']['Select'].' --</option>';
		for($i=0;$i<sizeof($category_list);$i++){
			$selected = ($BookInfo[0]['CategoryID']==$category_list[$i]['CategoryID'])?'selected="selected"':'';
			$CategorySelection .= '<option value="'.$category_list[$i]['CategoryID'].'" '.$selected.'>'.htmlspecialchars($category_list[$i]['CategoryName'],ENT_QUOTES).'</option>';
		}
		$CategorySelection .= '</select>';
		*/
		
		# Language
		/*
		$language_arr[0]='-- '.$Lang['Btn']['Select'].' --';
		$language_arr[CATEGORY_LANGUAGE_CHINESE]=$Lang['ReadingGarden']['Chinese'];
		$language_arr[CATEGORY_LANGUAGE_ENGLISH]=$Lang['ReadingGarden']['English'];
		$LanguageSelection = '<select id="Language" name="Language">';
		for($i=0;$i<sizeof($language_arr);$i++){
			$selected = ($BookInfo[0]['Language']==$i)?'selected="selected"':'';
			$LanguageSelection .= '<option value="'.$i.'" '.$selected.'>'.$language_arr[$i].'</option>';
		}
		$LanguageSelection .= '</select>';
		*/
		# Language
		$BookInfo[0]['Language'] = trim($BookInfo[0]['Language']);
		$DefaultLang = $BookInfo[0]['Language']==''?CATEGORY_LANGUAGE_CHINESE:$BookInfo[0]['Language'];
		$LanguageSelection = $this->Get_Radio_Button("LanguageChi", "Language", CATEGORY_LANGUAGE_CHINESE, ($DefaultLang==CATEGORY_LANGUAGE_CHINESE), "",$Lang['ReadingGarden']['Chinese'], "Load_Category_Selection(this);",0);
		$LanguageSelection .= '&nbsp;'.$this->Get_Radio_Button("LanguageEng", "Language", CATEGORY_LANGUAGE_ENGLISH, ($DefaultLang==CATEGORY_LANGUAGE_ENGLISH), "",$Lang['ReadingGarden']['English'], "Load_Category_Selection(this)",0);
		
		# Category
		$CategorySelection = $this->Get_Category_Selection("SelCategoryID", "SelCategoryID", $BookInfo[0]['CategoryID'], $SelectTitle='', $onChange='', $class='', $DefaultLang);
		
		#Assigned Forms
		$formSelection = '<select id="YearID" name="YearID[]" multiple size="10">';
		for($i=0;$i<sizeof($FormArr);$i++){
			$selected = in_array($FormArr[$i]['YearID'],$BookFormID) || $is_new?'selected="selected"':'';
			$formSelection .= '<option value="'.$FormArr[$i]['YearID'].'" '.$selected.'>'.$FormArr[$i]['YearName'].'</option>';
		}
		$formSelection .= '</select>';
		$formSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('YearID',true);");
		
		#Answer Sheet
		$HaveAnswerSheet = trim($BookInfo[0]['DefaultAnswerSheet'])==''?false:true;
//		if($HaveAnswerSheet){
//			$AnswerSheetSelectionSpanStyle = 'style="display:none" ';
//			$AnswerSheetManageSpanStyle = 'style="display:block" ';
//		}else{
//			$AnswerSheetSelectionSpanStyle = 'style="display:block" ';
//			$AnswerSheetManageSpanStyle = 'style="display:none" ';
//		}
		$AnswerSheets = $ReadingGardenLib->Get_Answer_Sheet_Template();
		$AnswerSheetSelection = $this->Get_Answer_Sheet_Template_Selection("AnswerSheet", $BookInfo[0]['DefaultAnswerSheetID'], $all=0, $noFirst=0, $FirstTitle="", $OnChange="", $CategoryIDArr='');
//		$AnswerSheetSelection = '<select id="AnswerSheet" name="AnswerSheet">
//									<option value="">-- '.$Lang['Btn']['Select'].' --</option>';
//		for($i=0;$i<sizeof($AnswerSheets);$i++){
//			$AnswerSheetSelection .= '<option value="'.$AnswerSheets[$i]['TemplateID'].'">'.htmlspecialchars($AnswerSheets[$i]['TemplateName']).'</option>';
//		}
//		$AnswerSheetSelection .= '</select>';
		
		$x = $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form name="BookEditForm" id="BookEditForm" onsubmit="return false;">
			  	<div id="BookEditDiv" style="height:430px; overflow-x:hidden; overflow-y:auto;">
				  <table class="form_table_v30">
					<tbody>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['BookName'].'</td>
							<td>
								<input name="BookName" id="BookName" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($BookInfo[0]['BookName'],ENT_QUOTES).'" />
								<div id="BookNameWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputBookName'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['CallNumber'].'</td>
							<td>
								<input name="CallNumber" id="CallNumber" type="text" class="textbox_name" maxlength="20" value="'.htmlspecialchars($BookInfo[0]['CallNumber'],ENT_QUOTES).'" />							
							</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['ISBN'].'</td>
							<td><input name="ISBN" id="ISBN" type="text" class="textbox_name" maxlength="20" value="'.htmlspecialchars($BookInfo[0]['ISBN'],ENT_QUOTES).'" /></td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['Author'].'</td>
							<td><input name="Author" id="Author" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($BookInfo[0]['Author'],ENT_QUOTES).'" /></td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['Publisher'].'</td>
							<td><input name="Publisher" id="Publisher" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($BookInfo[0]['Publisher'],ENT_QUOTES).'" /></td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Language'].'</td>
							<td>'.$LanguageSelection.'</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Category'].'</td>
							<td id="TdCategoryLayer">'.$CategorySelection.'</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>
							<td><textarea name="Description" id="Description" class="textbox_name" cols="20" rows="10">'.htmlspecialchars(preg_replace('/\<br(\s*)?\/?\>/i',"\n",$BookInfo[0]['Description']),ENT_QUOTES).'</textarea></td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['CoverImage'].'</td>
							<td>';
		if(trim($BookInfo[0]['BookCoverImage'])!=""){
						$x .= '<div id="DivCoverImage">';
						
						$x .= '<span style="float:left;">'.$ReadingGardenLib->Get_Book_Cover_Image($BookInfo[0]['BookCoverImage'], 200, 240).'</span>';
//						$x .= '<span style="float:left;"><img src="'.$ReadingGardenLib->BookCoverImagePath.'/'.$BookInfo[0]['BookCoverImage'].'" width=\'200\' height=\'240\' /></span>';
						$x .= '<span class="table_row_tool row_content_tool" style="vertical-align:bottom;"><a title="'.$Lang['Btn']['Delete'].'" class="delete_dim" href="#" onclick="Delete_Book_Cover_Image('.$book_id.');return false;"></a></span>';
						$x .= '</div>';
						$x .= '<input name="CoverImage" id="CoverImage" type="file" onchange="Check_Cover_Image();" style="display:none"/>';
		}else{
						$x .= '<input name="CoverImage" id="CoverImage" type="file" onchange="Check_Cover_Image(); "/>';
		}
							$x.='<div id="FileUploadWarningDiv" style="display:none;color:red;">'.$Lang['ReadingGarden']['WarningMsg']['InvalidCoverImage'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['AssignToForm'].'</td>
							<td>
								'.$formSelection.'&nbsp;'.$formSelectAllBtn.'<br>
								'. $this->MultiSelectionRemark().'
							</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['AnswerSheet'].'</td>
							<td><!--<span id="AnswerSheetSelectionSpan" '.$AnswerSheetSelectionSpanStyle.'>'.$AnswerSheetSelection.'</span>
								<span id="AnswerSheetManageSpan" '.$AnswerSheetManageSpanStyle.'>
									<span><a href="javascript:newWindow(\'../answer_sheet/preview.php?bid='.$book_id.'\',10)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" border="0" title="'.$Lang['Btn']['View'].'" alt="'.$Lang['Btn']['View'].'" /></a></span>
									<span class="table_row_tool row_content_tool" ><a title="'.$Lang['Btn']['Delete'].'" class="delete_dim" href="#" onclick="Remove_Book_Answer_Sheet('.$book_id.');return false;"></a></span>
								<span>-->
								'.$AnswerSheetSelection.'
								<a href="javascript:js_Preview_Answer_Sheet();"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" border="0" title="'.$Lang['Btn']['View'].'" alt="'.$Lang['Btn']['View'].'" align="absmiddle"/></a>
							</td>
						</tr>
					</tbody>
				  </table>
				  <iframe style="display: none;" name="CheckFileUploadIframe" id="CheckFileUploadIframe"></iframe>
				  <iframe style="display: none;" name="FileUploadIframe" id="FileUploadIframe"></iframe>
			  	</div>
				<input type="hidden" id="IsNew" name="IsNew" value="'.$is_new.'" />
				<input type="hidden" id="BookID" name="BookID" value="'.$BookInfo[0]['BookID'].'" />
				<input type="hidden" id="eBookID" name="eBookID" value="'.$BookInfo[0]['eBookID'].'" />
				<input type="hidden" id="ByStudent" name="ByStudent" value="'.$BookInfo[0]['ByStudent'].'" />
				<input type="hidden" id="IsCoverImageValid" name="IsCoverImageValid" value="0" />
			  </form>';
		$x .= $this->MandatoryField();
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>';
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Check_Book(1,0);","submit_btn")."&nbsp;";
		if($is_new==1) $x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'],"Button"," Check_Book(1,1);")."&nbsp;";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn")."&nbsp;";
			$x .= '<p class="spacer"></p>
			   </div>';
		return $x;
	}
	
	function Get_Book_Detail_Entry($BookID)
	{
		global $PATH_WRT_ROOT, $image_path, $intranet_root, $LAYOUT_SKIN, $Lang, $ReadingGardenLib, $objInstall, $lelib;
		
		if (!isset($objInstall))
		{
			include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
			$objInstall 	= new elibrary_install();
		}	
		
		$BookInfo = $ReadingGardenLib->Get_Book_List($BookID);
		
		$eLibBookID = trim($BookInfo[0]['eBookID']);
		$cover_image = $this->Get_Book_Cover_Image($BookInfo[0]['BookCoverImage'], "100", "120");
		if($eLibBookID != '' && $objInstall->check_book_is_readable($eLibBookID, $_SESSION['UserID'])){
			/*
			$eLibBookInfo = $lelib->getBookInformation(array("BookID"=>$eLibBookID));
			if($eLibBookInfo[0]['IsTLF']==1)
				$eLibFolder = "elibrary_dev";
			else
				$eLibFolder = "elibrary";
			$cover_image = "<a onclick=\"MM_openBrWindowFull('".$PATH_WRT_ROOT."/home/eLearning/".$eLibFolder."/tool/index.php?BookID=$eLibBookID','booktool','scrollbars=yes')\" title=\"".$Lang['ReadingGarden']['ViewBook']."\" href=\"Javascript:void(0);\">$cover_image<br>[".$Lang['ReadingGarden']['VieweBook']."]</a>";
			*/
			$this_book_info = $lelib->getBookInformation(array("BookID"=>$eLibBookID));
			$this_book_info = $this_book_info[0];
			$read_link = $objInstall->get_book_reader($eLibBookID, $this_book_info["BookFormat"], $this_book_info["IsTLF"], $_SESSION['UserID']);
			//debug($eLibBookID, $_SESSION['UserID']);
			//$cover_image = "<a onclick=\"{$read_link}\" title=\"".$Lang['ReadingGarden']['ViewBook']."\" href=\"Javascript:void(0);\">$cover_image<br>[".$Lang['ReadingGarden']['VieweBook']."]</a>";
			
			$cover_image = "<a onclick=\"". $objInstall->get_book_reader_link($eLibBookID, $this_book_info["BookFormat"], $this_book_info["IsTLF"], $_SESSION['UserID'], "COVER")."\" href=\"Javascript:void(0);\">$cover_image<br>[".$Lang['ReadingGarden']['VieweBook']."]</a>";
		
		}
		$book_name = htmlspecialchars($BookInfo[0]['BookName'], ENT_QUOTES);
		$author = $BookInfo[0]['Author']?htmlspecialchars($BookInfo[0]['Author'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
		$call_number = $BookInfo[0]['CallNumber']?htmlspecialchars($BookInfo[0]['CallNumber'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
		$publisher = $BookInfo[0]['Publisher']?htmlspecialchars($BookInfo[0]['Publisher'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
		$description = $BookInfo[0]['Description']?htmlspecialchars($BookInfo[0]['Description'],ENT_QUOTES):$Lang['General']['EmptySymbol'];
		if( $BookInfo[0]['CategoryName'])
		{
			$category_name = htmlspecialchars($BookInfo[0]['CategoryName'],ENT_QUOTES);
			$category_code = htmlspecialchars($BookInfo[0]['CategoryCode'],ENT_QUOTES);
			$category = $category_code."&nbsp;".$category_name;
		}
		else
			$category = $Lang['ReadingGarden']['UnclassifiedCategory'];
		
		$x .= "<table class='common_table_list_v30'>\n<tbody>\n";
		$x .= "<tr>\n<td style=\"border-right:none\" align=\"center\">\n";
		//$x .= "<div class=\"book_detail_w_cover\">
        //      	<div class=\"book_detail_cover\">";
        $x .= 	$cover_image;
        //$x .= "</div>";
        $x .= "</td>\n";////
        $x .= "<td style=\"border-left:none\">\n";/////
		$x .= " <div class=\"book_detail\">
            		<div class=\"book_detail_info\">
                		<h1>$book_name</h1>
                		<h2 class=\"book_author\"><em>".$Lang['ReadingGarden']['Author'].":</em><span>$author</span></h2>
                		<h2 class=\"book_publisher\"><em>".$Lang['ReadingGarden']['Publisher'].":</em><span>$publisher</span></h2>
                		<h2 class=\"book_ISBN\"><em>".$Lang['ReadingGarden']['CallNumber'].":</em><span>$call_number</span></h2>
                 		<p class=\"spacer\"></p>
                	</div>
                	<h2 class=\"book_cat\"><em>".$Lang['ReadingGarden']['Category'].":</em><span>".$category."</span></h2>
                 	<p class=\"spacer\"></p>
              		<h2 class=\"book_desc\"><em>".$Lang['ReadingGarden']['Description'].":</em><span>".nl2br($description)."</span></h2>
                	<p class=\"spacer\"></p>";
        # Like Function
        $x .= $this->Get_Like_Comment_Div(FUNCTION_NAME_BOOK, $BookID);
        
        $x .= "<p class=\"spacer\"></p>";
		$x .= "</div>\n";
		$x .= "<p class=\"spacer\"></p>";
		$x .= "</td>\n";////
		
        $x .= "</div>\n";
		$x .= "</td>\n</tr>\n";
			
		$x .= "</tbody></table>";
		
		return $x;
	}
	
	function Get_Answer_Sheet_Template_Index($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_answersheet_template_page_size, $ReadingGardenLib, $libform;
		
		$ThickBoxHeight = 550;
		$ThickBoxWidth = 750;
		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['ReadingGarden']['NewAnswerSheetTemplate'], "Get_Answersheet_Template_Form(1,'')", $InlineID="FakeLayer", $Lang['Btn']['New']);
		
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="index.php" onsubmit="Get_Answersheet_Template_Table(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $ThickBoxNewLink."\n";
										$x .= '</div>'."\n";
										$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<div class="table_filter">'."\n";
										
									$x .= '</div>'."\n";
									$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td valign="bottom">'."\n";
											$x .= '<div class="common_table_tool">'."\n";
								        	$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'TemplateID[]\',\'Get_Answersheet_Template_Form(0, false);\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
											$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'TemplateID[]\',\'remove_template.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
											$x .= '</div>'."\n";
										$x .= '</td>'."\n";		
									$x .= '</tr>'."\n";
									$x .= '</table>'."\n"; 		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DivTemplateTable">'."\n";
										//$x .= $this->Get_Answer_Sheet_Template_Table($field, $order, $pageNo, $numPerPage, $Keyword)."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Answer_Sheet_Template_Table($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_answersheet_template_page_size, $ReadingGardenLib, $libform;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_answersheet_template_page_size) && $ck_answersheet_template_page_size != "") $page_size = $ck_answersheet_template_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Answer_Sheet_Template_DBTable_Sql($Keyword);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("TemplateName", "DateModified", "LastModifiedBy");
		$li->column_array = array(18,18,18,18);
		$li->wrap_array = array(0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='10'>".$Lang['Btn']['Preview']."</th>";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['TemplateName'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedBy'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("TemplateID[]")."</th>\n";
		$li->no_col = $pos+3;
		
		$x .= $li->display();
		
		return $x;
	}
	
	function Get_Answer_Sheet_Template_Edit_Form($is_new, $template_id="")
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		include_once($PATH_WRT_ROOT."includes/libform.php");
		
		$libform = new libform();
		
		if($is_new!=1 & $template_id!=""){
			$TemplateInfo = $ReadingGardenLib->Get_Answer_Sheet_Template($template_id);
			$CategoryIDArr = $ReadingGardenLib->Get_Category_Answer_Sheet_Mapping('', $template_id);
			$CategoryIDArr = Get_Array_By_Key($CategoryIDArr, "CategoryID");
		}
		
		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('CategoryID',true);");
		$CategoryIDSelection = $this->Get_Category_Selection("CategoryID", "CategoryID[]", $CategoryIDArr, '', '', '', '', true, 1);
		$CategorySelection = $this->Get_MultipleSelection_And_SelectAll_Div($CategoryIDSelection, $YearClassSelectAllBtn);
		
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form name="ansForm" action="">
				<table class="form_table_v30">
					<tbody>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['TemplateName'].'</td>
							<td>
								<input name="TemplateName" id="TemplateName" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($TemplateInfo[0]['TemplateName'],ENT_QUOTES).'" />
								<div id="TemplateNameWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['ApplicableCategory'].'</td>
							<td>
								'.$CategorySelection.'
							</td>
						</tr>
					</tbody>
				</table>';
		$x .= $this->MandatoryField();
		$x .= ' <input type="hidden" id="IsNew" name="IsNew" value="'.$is_new.'" />
				<input type="hidden" id="TemplateID" name="TemplateID" value="'.$template_id.'" />
				<input type="hidden" id="OldTemplateName" name="OldTemplateName" value="'.htmlspecialchars($TemplateInfo[0]['TemplateName'],ENT_QUOTES).'" />
				<input type="hidden" id="qStr" name="qStr" value="'.htmlspecialchars($TemplateInfo[0]['AnswerSheet'],ENT_QUOTES).'">
				<input type="hidden" id="aStr" name="aStr" value="">';
		$x .= '</form>';
		$x .= '<div id="TemplateEditDiv">
				</div>';
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","if(finish()){Check_Template_Name(1);}","submit_btn").'
					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
		
		$x .= '<script>';
		$x .= $libform->getWordsInJS();
		$x .= 'var answer_sheet	= \'Content\';';
		$x .= 'var space10 	= \'<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" />\';';
		$x .= 'var add_btn 	= \''.str_replace("'", "\'",$this->GET_BTN($Lang['Btn']['Add'], 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2')).'\';';
		//$x .= 'var order_name	= \''.$i_Sports_Order.'\';';
		
		$x .= 'var replyslip = \''.$Lang['ReadingGarden']['AnswerSheet'].'\';';
		$x .= 'var MoveUpBtn = \''.$Lang['Button']['MoveUp'].'\';';
		$x .= 'var MoveDownBtn = \''.$Lang['Button']['MoveDown'].'\';';
		$x .= 'var DeleteBtn = \''.$Lang['Button']['Delete'].'\';';
		$x .= 'var EditBtn = \''.$Lang['Button']['Edit'].'\';';
		$x .= 'var MarksTxt = \''.$Lang['ReadingGarden']['Mark(s)'].'\';';
		$x .= 'var AnswerTxt = \''.$Lang['ReadingGarden']['Answer'].'\';';
		$x .= 'var need2checkform = true;';
		$x .= 'var pls_complete_all_fields = \''.$Lang['ReadingGarden']['WarningMsg']['RequestCompleteAnswerSheetAllAnswerScore'].'\';';
		$x .= 'var s = $(\'input#qStr\').val();
				s = s.replace(/"/g, \'&quot;\');
				$(\'input#qStr\').val(s);
				var form_templates = new Array(); 
			   	var sheet= new Answersheet();
				sheet.qString = s;
				sheet.mode=0; // 0:edit 1:fill in application
				sheet.answer=sheet.sheetArr();
				sheet.templates=form_templates;
				$(document).ready(function(){
					$(\'div#TemplateEditDiv\').html(editPanel());
					$(\'div#blockInput\').html(sheet.writeSheet());
				});
			   </script>';
		
		return $x;
	}
	
	function Get_Answer_Sheet_Template_Preview_Form($TemplateID)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		include_once($PATH_WRT_ROOT."includes/libform.php");
		
		$libform = new libform();
		$TemplateInfo = $ReadingGardenLib->Get_Answer_Sheet_Template($TemplateID);
		
		$x .= '<form name="ansForm" action="">
				<input type="hidden" id="TemplateID" name="TemplateID" value="'.$TemplateID.'" />
				<input type="hidden" id="qStr" name="qStr" value="'.htmlspecialchars($TemplateInfo[0]['AnswerSheet'],ENT_QUOTES).'">
				<input type="hidden" id="aStr" name="aStr" value="">';
		$x .= '</form>';
		$x .= '<div id="TemplateEditDiv">
				</div>';
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Close'],"button","window.top.tb_remove();","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
		
		$x .= '<script>';
		$x .= $libform->getWordsInJS();
		$x .= 'var answer_sheet	= \'Content\';';
		$x .= 'var space10 	= \'<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" />\';';
		$x .= 'var add_btn 	= \''.str_replace("'", "\'",$this->GET_BTN($Lang['Btn']['Add'], 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2')).'\';';
		//$x .= 'var order_name	= \''.$i_Sports_Order.'\';';
		
		$x .= 'var replyslip = \''.htmlspecialchars($TemplateInfo[0]['TemplateName'],ENT_QUOTES).'\';';
		$x .= 'var MarksTxt = \''.$Lang['ReadingGarden']['Mark(s)'].'\';';
		$x .= 'var AnswerTxt = \''.$Lang['ReadingGarden']['Answer'].'\';';
		$x .= 'var s = $(\'input#qStr\').val();
				s = s.replace(/"/g, \'&quot;\');
				$(\'input#qStr\').val(s);
				var form_templates = new Array(); 
			   	var sheet= new Answersheet();
				sheet.qString = s;
				sheet.mode=1; // 0:edit 1:fill in application
				sheet.answer=sheet.sheetArr();
				sheet.templates=form_templates;
				$(document).ready(function(){
					$(\'div#TemplateEditDiv\').html(editPanel());
					$(\'div#blockInput\').html(sheet.writeSheet());
				});
			   </script>';
		
		return $x;
	}
	
	function Settings_Award_Scheme_Index()
	{
		global $PATH_WRT_ROOT,$Lang,$intranet_root,$linterface,$ReadingGardenLib;
		
		$ThickboxHeight = "550";
		$ThickboxWidth = "750";
		//$ThickboxAddButton = $this->Get_Thickbox_Link($ThickboxHeight, $ThickboxWidth, "add_dim", $Lang['ReadingGarden']['AddAward'], "Get_Award_Edit_Form(1,'');", "FakeLayer", $Lang['ReadingGarden']['AddAward'], "");
		//$ThickboxAddButton = $this->GET_LNK_ADD("./edit_award_scheme.php", $Lang['ReadingGarden']['AddAward'], "", "", "", 0);
		$ThickboxAddButton = $this->Get_Content_Tool_v30("new", "./edit_award_scheme.php", $Lang['ReadingGarden']['AddAward']);
		
		$AwardLanguageArr[] = array(CATEGORY_LANGUAGE_CHINESE,$Lang['ReadingGarden']['Chinese']);
		$AwardLanguageArr[] = array(CATEGORY_LANGUAGE_ENGLISH,$Lang['ReadingGarden']['English']);
		$AwardLanguageArr[] = array(CATEGORY_LANGUAGE_OTHERS,$Lang['ReadingGarden']['Either']);
		
		$AwardLanguageSelection = $this->GET_SELECTION_BOX($AwardLanguageArr, 'id="Language" name="Language" onchange="Get_Award_Scheme_Table();"', '-- '. $Lang['General']['All'].' --', '');
		
		$ContentToolArr[] = array("assign_to_student",'javascript:checkEdit(document.form1,\'AwardSchemeID[]\',\'generate_winner.php?clearCoo=1\')',$Lang['ReadingGarden']['GenerateWinner']);
		//$ContentToolArr[] = array("edit",'javascript:checkEdit2(document.form1,\'AwardSchemeID[]\',\'Get_Award_Edit_Form(0, false);\')');
		$ContentToolArr[] = array("edit",'javascript:checkEdit2(document.form1,\'AwardSchemeID[]\',\'Get_Award_Edit_Form(0, false);\')');
		$ContentToolArr[] = array("delete",'javascript:checkRemove2(document.form1,\'AwardSchemeID[]\',\'Delete_Award();\')'); 
		$ContentTool = $this->Get_DBTable_Action_Button_IP25($ContentToolArr);
		
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="award_scheme.php" onsubmit="Get_Award_Scheme_Table();return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $ThickboxAddButton."\n";
										$x .= '</div>'."\n";
										$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
										$x .= '<tr>'."\n";
											$x .= '<td valign="bottom">'."\n";
												$x .= '<div class="table_filter">'."\n";
													$x .= $AwardLanguageSelection."\n";
												$x .= '</div>'."\n";
											$x .= '</td>'."\n";	
											$x .= '<td valign="bottom">'."\n";
												$x .= $ContentTool;
//												$x .= '<div class="common_table_tool">'."\n";
//									        		$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'AwardSchemeID[]\',\'Get_Award_Edit_Form(0, false);\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
//													$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'AwardSchemeID[]\',\'Delete_Award();\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
//												$x .= '</div>'."\n";
											$x .= '</td>'."\n";	
										$x .= '</tr>'."\n";
									$x .= '</table>'."\n"; 
									//$x .= '<div class="common_table_tool">'."\n";
								        //$x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'BookID[]\',\'edit.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										//$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'BookID[]\',\'remove_book.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									//$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DivAwardSchemeTable">'."\n";
										//$x .= $this->Settings_Award_Scheme_Table();
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Settings_Award_Scheme_Table($Keyword="",$Language="")
	{
		global $PATH_WRT_ROOT,$Lang,$intranet_root,$linterface,$ReadingGardenLib;
		
		$award_list = $ReadingGardenLib->Get_Award_Scheme_List("",$Keyword,$Language);
		$size_award_list = sizeof($award_list);
		
		$ThickboxHeight = "550";
		$ThickboxWidth = "750";
		//$ThickboxAddButton = $this->Get_Thickbox_Link($ThickboxHeight, $ThickboxWidth, "add_dim", $Lang['ReadingGarden']['AddAward'], "Get_Award_Edit_Form(1,\'\');", "FakeLayer", "", "");
		
		$Type[AWARD_SCHEME_TYPE_INDIVIDUAL] = $Lang['ReadingGarden']['Individual'];
		$Type[AWARD_SCHEME_TYPE_GROUP] = $Lang['ReadingGarden']['Inter-Class'];
		
		$BookCategoryLanguage[0] = $Lang['General']['EmptySymbol'];
		$BookCategoryLanguage[CATEGORY_LANGUAGE_CHINESE] = $Lang['ReadingGarden']['Chinese'];
		$BookCategoryLanguage[CATEGORY_LANGUAGE_ENGLISH] = $Lang['ReadingGarden']['English'];
		$BookCategoryLanguage[CATEGORY_LANGUAGE_OTHERS] = $Lang['ReadingGarden']['Either'];
		
		$x .= '<table class="common_table_list_v30">
				<thead>
					<tr>
					<th width="1" rowspan="2">#</th>
					<th width="20%" rowspan="2">'.$Lang['ReadingGarden']['AwardName'].'</th>
					<th width="5%" rowspan="2">'.$Lang['ReadingGarden']['Type'].'</th>
					<th width="5%" rowspan="2">'.$Lang['ReadingGarden']['Language'].'</th>
					<th width="8%" rowspan="2">'.$Lang['ReadingGarden']['Target'].'</th>
					<th rowspan="2">'.$Lang['ReadingGarden']['Period'].'</th>
					
					<th rowspan="2">'.$Lang['ReadingGarden']['TargetClass'].'</th>
					
					<!--<th width="30%" colspan="5">'.$Lang['ReadingGarden']['AwardLevel'].'</th>-->
					<th width="10px" rowspan="2">';
						$x .= $this->Get_Checkbox("CheckMaster", "", "", 0, '', '', 'Check_All_Award(this.checked)');
			$x .= '</th>';
			$x .= '</tr>';
//			$x .= '<tr>';
//			$x .= '<th>'.$Lang['ReadingGarden']['Level'].'</th>';
//			$x .= '<th>'.$Lang['ReadingGarden']['LevelName'].'</th>';
//			$x .= '<th>'.$Lang['ReadingGarden']['MinBookRequired'].'</th>';
//			$x .= '<th>'.$Lang['ReadingGarden']['MinReportRequired'].'</th>';
//			$x .= '<th>'.$Lang['ReadingGarden']['Requirement'].'</th>';
//			$x .= '</tr>';
		$x .= '	</thead>
				<tbody>';
		if($size_award_list==0){
			$x .= '<tr><td colspan="12" align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
		}
		for($i=0;$i<$size_award_list;$i++){
			$AwardSchemeID = $award_list[$i]['AwardSchemeID'];
			$AwardName = $award_list[$i]['AwardName'];
			$AwardType = $award_list[$i]['AwardType'];
			$AwardLanguage = trim($award_list[$i]['Language'])==''?$Lang['General']['EmptySymbol']:$award_list[$i]['Language'];
			$AwardTarget = trim($award_list[$i]['Target'])==''?$Lang['General']['EmptySymbol']:$award_list[$i]['Target'];
			$StartDate = $award_list[$i]['StartDate'];
			$EndDate = $award_list[$i]['EndDate'];
			$MinBookRead = trim($award_list[$i]['MinBookRead'])==''?$Lang['General']['EmptySymbol']:$award_list[$i]['MinBookRead'];
			$MinReportSubmit = trim($award_list[$i]['MinReportSubmit'])==''?$Lang['General']['EmptySymbol']:$award_list[$i]['MinReportSubmit'];
			$AwardLevelList = $ReadingGardenLib->Get_Award_Scheme_Level($AwardSchemeID);
			
			
		//	$ThickboxEditButton = $this->Get_Thickbox_Link($ThickboxHeight, $ThickboxWidth, "", $Lang['Btn']['Edit'], "Get_Award_Edit_Form(0,".$AwardSchemeID.");", "FakeLayer", htmlspecialchars($AwardName), "");
			$ThickboxEditButton = $this->GET_LNK_EDIT("./edit_award_scheme.php?AwardSchemeID=".$AwardSchemeID, htmlspecialchars($AwardName), "", "", "", 0);
			//$ThickboxDeleteButton = $this->Get_Thickbox_Link($ThickboxHeight, $ThickboxWidth, "delete_dim", $Lang['Btn']['Delete'], "Delete_Award(".$AwardSchemeID.");return false;", "", "", "");
			//$ThickboxDeleteButton = '<a href="#" class="delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="Delete_Award('.$AwardSchemeID.');return false;"></a>';
			
			$AwardPeriod = $StartDate.'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$EndDate;
			
			$TargetClassList = $ReadingGardenLib->Get_Award_Scheme_Target_Class($AwardSchemeID);
			$TargetClass = array();
			for($j=0;$j<sizeof($TargetClassList);$j++){
				$class_name = Get_Lang_Selection($TargetClassList[$j]['ClassTitleB5'],$TargetClassList[$j]['ClassTitleEN']);
				$TargetClass[] = htmlspecialchars($class_name);
			}
			if(sizeof($TargetClass)==0) $TargetClass = $Lang['General']['EmptySymbol'];
			else $TargetClass = implode(", ", $TargetClass);
		/*	
			$AwardRequirementList = $ReadingGardenLib->Get_Award_Scheme_Requirement($AwardSchemeID);
			$AwardRequirement = '';
			for($j=0;$j<sizeof($AwardRequirementList);$j++){
				$cat_id = $AwardRequirementList[$j]['CategoryID'];
				$cat_code = $AwardRequirementList[$j]['CategoryCode'];
				$cat_name = $AwardRequirementList[$j]['CategoryName'];
				$min_book_read = $AwardRequirementList[$j]['MinBookRead'];
				$AwardRequirement .= htmlspecialchars($cat_code.' '.$cat_name.' ['.$min_book_read.']').'<br />';
			}
			if($AwardRequirement=='') $AwardRequirement = $Lang['General']['EmptySymbol'];
		*/	
		
//			$RowSpan = max(count($AwardLevelList),1);
//			$AwardLevel = '';
//			$AwardLevelFirstRow = '';
//			if(sizeof($AwardLevelList)>0){
//				$RequirementList = $ReadingGardenLib->Get_Award_Scheme_Requirement($AwardLevelList[0]['AwardLevelID']);
//				$AwardLevelFirstRow .= '<td>'.$AwardLevelList[0]['Level'].'</td>';
//				$AwardLevelFirstRow .= '<td>'.$AwardLevelList[0]['LevelName'].'</td>';
//				$AwardLevelFirstRow .= '<td>'.$AwardLevelList[0]['MinBookRead'].'</td>';
//				$AwardLevelFirstRow .= '<td>'.$AwardLevelList[0]['MinReportSubmit'].'</td>';
//				$AwardLevelFirstRow .= '<td>';
//				$tmp_requirement = array();
//				for($k=0;$k<sizeof($RequirementList);$k++){
//					$tmp_requirement[] = htmlspecialchars($RequirementList[$k]['CategoryCode'].' '.$RequirementList[$k]['CategoryName'].'('.$RequirementList[$k]['MinBookRead'].')');
//				}
//				$AwardLevelFirstRow .= sizeof($tmp_requirement)>0?implode(", ",$tmp_requirement):$Lang['General']['EmptySymbol'];
//				$AwardLevelFirstRow .= '</td>';
//			}else{
//				$AwardLevelFirstRow .= '<td colspan="5">'.$Lang['General']['EmptySymbol'].'</td>';
//			}
//			for($j=1;$j<sizeof($AwardLevelList);$j++){
//				$RequirementList = $ReadingGardenLib->Get_Award_Scheme_Requirement($AwardLevelList[$j]['AwardLevelID']);
//				$AwardLevel .= '<tr>';
//				$AwardLevel .= '<td>'.$AwardLevelList[$j]['Level'].'</td>';
//				$AwardLevel .= '<td>'.$AwardLevelList[$j]['LevelName'].'</td>';
//				$AwardLevel .= '<td>'.$AwardLevelList[$j]['MinBookRead'].'</td>';
//				$AwardLevel .= '<td>'.$AwardLevelList[$j]['MinReportSubmit'].'</td>';
//				$AwardLevel .= '<td>';
//				$tmp_requirement = array();
//				for($k=0;$k<sizeof($RequirementList);$k++){
//					$tmp_requirement[] = htmlspecialchars($RequirementList[$k]['CategoryCode'].' '.$RequirementList[$k]['CategoryName'].'('.$RequirementList[$k]['MinBookRead'].')');
//				}
//				$AwardLevel .= sizeof($tmp_requirement)>0?implode(", ",$tmp_requirement):$Lang['General']['EmptySymbol'];
//				$AwardLevel .= '</td>';
//				$AwardLevel .= '</tr>';
//			}
			
			$RowSpan = 1;
			$x .= '<tr>';
			$x .= '<td rowspan="'.$RowSpan.'">'.($i+1).'</td>';
			$x .= '<td rowspan="'.$RowSpan.'">'.$ThickboxEditButton.'</td>';
			$x .= '<td rowspan="'.$RowSpan.'">'.$Type[$AwardType].'</td>';
			$x .= '<td rowspan="'.$RowSpan.'">'.$BookCategoryLanguage[$AwardLanguage].'</td>';
			$x .= '<td rowspan="'.$RowSpan.'">'.htmlspecialchars($AwardTarget).'</td>';
			$x .= '<td rowspan="'.$RowSpan.'">'.$AwardPeriod.'</td>';
			$x .= '<td rowspan="'.$RowSpan.'">'.$TargetClass.'</td>';
			$x .= $AwardLevelFirstRow;
			$x .= '<td rowspan="'.$RowSpan.'">'.$this->Get_Checkbox("", "AwardSchemeID[]", $AwardSchemeID, 0, '', '', '').'</td>';
			$x .= '</tr>';
			$x .= $AwardLevel;
		}
		
		$x .= '	</tbody>
			  </table>
			<br />';
		
		return $x;
	}
	
	function Settings_Award_Scheme_Edit_Form($IsNew,$AwardSchemeID="")
	{
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $image_path, $LAYOUT_SKIN, $ReadingGardenLib;
		
//		include_once("form_class_manage_ui.php");
//		$fcm_ui = new form_class_manage_ui();
		//$fcm = new form_class_manage();
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		//$ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
		
		$EntryNum = 0;
		$AwardLevelEntries = '';
		if($IsNew!='1' && $AwardSchemeID > 0){
			$AwardInfo = $ReadingGardenLib->Get_Award_Scheme_List($AwardSchemeID);
			
		//	$AwardRequirementList = $ReadingGardenLib->Get_Award_Scheme_Requirement($AwardSchemeID);
			$AwardTargetClass = $ReadingGardenLib->Get_Award_Scheme_Target_Class($AwardSchemeID,$AcademicYearID);
			
			# check if winner list was generated
			$WinnerList = $ReadingGardenLib->Get_Award_Scheme_Winner_List($AwardSchemeID);
			$WinnerGenerated = count($WinnerList)>0?1:0;
			
			$AwardLevelList = $ReadingGardenLib->Get_Award_Scheme_Level($AwardSchemeID);
			for($i=0;$i<sizeof($AwardLevelList);$i++){
				$AwardLevelEntries[$AwardLevelList[$i]['AwardType']] .= $this->Settings_Get_Award_Level_Entry($EntryNum++,$AwardLevelList[$i]['AwardLevelID'], $AwardLevelList[$i]['AwardType']);
			}
			//if(sizeof($AwardLevelList)==0){
			//	$AwardLevelEntries .= $this->Settings_Get_Award_Level_Entry($EntryNum++,'');
			//}
		}else{
			$AwardLevelEntries[AWARD_TYPE_GENERAL] .= $this->Settings_Get_Award_Level_Entry($EntryNum++,'', AWARD_TYPE_GENERAL);
		}
		
		$StartDate = trim($AwardInfo[0]['StartDate'])!=""?$AwardInfo[0]['StartDate']:date("Y-m-d",strtotime(getStartDateOfAcademicYear($AcademicYearID)));
		$EndDate = trim($AwardInfo[0]['EndDate'])!=""?$AwardInfo[0]['EndDate']:date("Y-m-d",strtotime(getEndDateOfAcademicYear($AcademicYearID)));
		$ResultPublishDate = trim($AwardInfo[0]['ResultPublishDate'])!=""?$AwardInfo[0]['ResultPublishDate']:date("Y-m-d",strtotime(getEndDateOfAcademicYear($AcademicYearID)));
		$Description = htmlspecialchars($AwardInfo[0]['Description']);
		$Attachment = $AwardInfo[0]['Attachment'];
		
		# Type 
		$type_arr[AWARD_SCHEME_TYPE_INDIVIDUAL] = $Lang['ReadingGarden']['Individual'];
		$type_arr[AWARD_SCHEME_TYPE_GROUP] = $Lang['ReadingGarden']['Inter-Class'];
		$AwardTypeSelection = '<select id="AwardType" name="AwardType">';
		foreach((array)$type_arr as $i=>$v){
			$selected = ($AwardInfo[0]['AwardType']==$i)?'selected="selected"':'';
			$AwardTypeSelection .= '<option value="'.$i.'" '.$selected.'>'.$v.'</option>';
		}
		$AwardTypeSelection .= '</select>';
		
		# Language
		//$language_arr[0]='-- '.$Lang['Btn']['Select'].' --';
		$language_arr[CATEGORY_LANGUAGE_CHINESE]=$Lang['ReadingGarden']['Chinese'];
		$language_arr[CATEGORY_LANGUAGE_ENGLISH]=$Lang['ReadingGarden']['English'];
		$language_arr[CATEGORY_LANGUAGE_OTHERS]=$Lang['ReadingGarden']['Either'];
		$DefaultAwardLanguage = $AwardInfo[0]['Language']>0?$AwardInfo[0]['Language']:CATEGORY_LANGUAGE_CHINESE;
		$LanguageSelection = $this->Get_Radio_Button("AwardLanguageChi", "AwardLanguage", CATEGORY_LANGUAGE_CHINESE, $DefaultAwardLanguage==CATEGORY_LANGUAGE_CHINESE,"",$Lang['ReadingGarden']['Chinese']);
		$LanguageSelection .= '&nbsp;'.$this->Get_Radio_Button("AwardLanguageEng", "AwardLanguage", CATEGORY_LANGUAGE_ENGLISH, $DefaultAwardLanguage==CATEGORY_LANGUAGE_ENGLISH,"",$Lang['ReadingGarden']['English']);
		$LanguageSelection .= '&nbsp;'.$this->Get_Radio_Button("AwardLanguageOthers", "AwardLanguage", CATEGORY_LANGUAGE_OTHERS, $DefaultAwardLanguage==CATEGORY_LANGUAGE_OTHERS,"",$Lang['ReadingGarden']['Either']);
		/*$LanguageSelection = '<select id="AwardLanguage" name="AwardLanguage">';
		foreach($language_arr as $k=>$v){
			$selected = ($AwardInfo[0]['Language']==$k)?'selected="selected"':'';
			$LanguageSelection .= '<option value="'.$k.'" '.$selected.'>'.$v.'</option>';
		}
		$LanguageSelection .= '</select>';
		*/
		
		
		# Target Class Edition Layer
		$TargetClassList = array();
		for($i=0;$i<sizeof($AwardTargetClass);$i++){
			$TargetClassList[] = $AwardTargetClass[$i]['ClassID'];
		}
//		$YearClassSelection = $fcm_ui->Get_Class_Selection($AcademicYearID, '', 'ClassID[]', $TargetClassList, $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
//		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);");
		# Class Selection
		include_once("libclass.php");
		$lclass = new libclass();
		$YearClassSelection = $lclass->getSelectClassID(" id='ClassID[]' name='ClassID[]' multiple size=10 ",$TargetClassList,2);
		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);");

		/*
		$TargetClassLayer .= $YearClassSelection;
		
		$TargetClassLayer .= $this->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "Add_TargetClass_Item();", $ParName="", $ParOtherAttribute="").'&nbsp;';
		$TargetClassLayer .= $this->GET_SMALL_BTN($Lang['Btn']['DeleteAll'], "button", "$('div#DivTargetClass').html('');", $ParName="", $ParOtherAttribute="");
		$TargetClassLayer .= '<div id="TargetClassWarningLayer" style="color:red;display:none;"></div>';
		$TargetClassLayer .= '<div id="DivTargetClass">';
		for($i=0;$i<sizeof($AwardTargetClass);$i++){
			$class_name = Get_Lang_Selection($AwardTargetClass[$i]['ClassTitleB5'],$AwardTargetClass[$i]['ClassTitleEN']);
			$class_id = $AwardTargetClass[$i]['ClassID'];
			$TargetClassLayer .= '<div>'.htmlspecialchars($class_name).'<input type="hidden" name="TargetClassItem[]" value="'.$class_id.'"/>&nbsp;';
			$TargetClassLayer .= '<a href="javascript:void(0);" onclick="$(this).parent().remove();"><img border="0" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_delete.gif" title="'.$Lang['Btn']['Delete'].'" /></a>&nbsp;</div>';
		}
		$TargetClassLayer .= '</div>';
		*/
		
		/*
		# Requirement Edition Layer
		$RequirementLayer = '';
		$CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID",'', '', '', '', '', false);
		$RequirementLayer .= $CategorySelection;
		$RequirementLayer .= $this->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "Add_Requirement_Item();", $ParName="", $ParOtherAttribute="").'&nbsp;';
		$RequirementLayer .= $this->GET_SMALL_BTN($Lang['Btn']['DeleteAll'], "button", "$('div#DivRequirement').html('');", $ParName="", $ParOtherAttribute="");
		$RequirementLayer .= '<div id="CategoryWarningLayer" style="color:red;display:none;"></div>';
		$RequirementLayer .= '<div id="DivRequirement">';
		for($i=0;$i<sizeof($AwardRequirementList);$i++){
			$cat_id = $AwardRequirementList[$i]['CategoryID'];
			$cat_name = $AwardRequirementList[$i]['CategoryName'];
			$min_book_read = $AwardRequirementList[$i]['MinBookRead'];
			$RequirementLayer .= '<div>'.htmlspecialchars($cat_name).'<input type="hidden" name="RequirementItemCategory[]" value="'.$cat_id.'"/>&nbsp;';
			$RequirementLayer .= '<input name="RequirementItemQuantity[]" type="text" maxlength="10" size="10" value="'.$min_book_read.'"/>&nbsp;';
			$RequirementLayer .= '<a href="javascript:void(0);" onclick="$(this).parent().remove();"><img border="0" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_delete.gif" title="'.$Lang['Btn']['Delete'].'" /></a>&nbsp;</div>';
		}
		$RequirementLayer .= '</div>';
		*/
		
		
		$AddAwardLevelBtn = '[<a href="javascript:void(0);" onclick="Add_Award_Level_Entry(\'\',1);">'.$Lang['Btn']['AddMore'].'</a>]';
		$AddAdditionalAwardBtn = '[<a href="javascript:void(0);" onclick="Add_Award_Level_Entry(\'\',2);">'.$Lang['Btn']['AddMore'].'</a>]';
		
		
		# Navigation
		$NavigationObj[] = array($Lang['ReadingGarden']['AwardScheme'],"./award_scheme.php");
		$NavigationObj[] = array($AwardSchemeID==""?$Lang['Btn']['New']:$Lang['Btn']['Edit']);
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj);
		
		//$x = $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form name="AwardEditForm" id="AwardEditForm" method="POST" action="edit_award_scheme_update.php" onsubmit="return false;">';
		$x .= $Navigation;
		$x .= '	<div id="AwardEditDiv">
				  <table class="form_table_v30">
					<tbody>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['AwardName'].'</td>
							<td>
								<input name="AwardName" id="AwardName" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($AwardInfo[0]['AwardName'],ENT_QUOTES).'" />
								<div id="AwardNameWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['TargetDescription'].'</td>
							<td>
								<input name="AwardTarget" id="AwardTarget" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($AwardInfo[0]['Target'],ENT_QUOTES).'" />
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['TargetClass'].'</td>
							<td>'.$YearClassSelection.'&nbsp;'.$YearClassSelectAllBtn.'<br />'.$this->MultiSelectionRemark().'
								<div id="TargetClassWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Language'].'</td>
							<td>
								'.$LanguageSelection.'
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Type'].'</td>
							<td>
								'.$AwardTypeSelection.'
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Period'].'</td>
							<td>'.$this->GET_DATE_PICKER("StartDate",$StartDate).'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$this->GET_DATE_PICKER("EndDate",$EndDate).'
								<div id="DatePeriodWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['ResultPublishDate'].'</td>
							<td>'.$this->GET_DATE_PICKER("ResultPublishDate",$ResultPublishDate).'
								<div id="ResultPublishDateWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>';
					$x .= '<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>
							<td>'.$this->GET_TEXTAREA("Description",$Description).'
							</td>
						</tr>';
					if(!$Attachment)
					{
						$foldername = session_id().time();
						$Attachment = $ReadingGardenLib->AwardSchemeAttachmentPath."/".$foldername;
					}
					$x .= '<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['Attachment'].'</td>
							<td>'.$this->Get_Upload_Attachment_UI("AwardEditForm", "AttachmentArr[]", $Attachment).'
							</td>
						</tr>';
					$x .= '<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['AwardLevel'].' ('.$Lang['ReadingGarden']['DescendingOrder'].')'.'</td>
							<td>
								<div id="AwardLevelDiv">'.$AwardLevelEntries[AWARD_TYPE_GENERAL].'</div>
								<div id="EmptyAwardLevelWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneAwardLevel'].'</div>
								'.$AddAwardLevelBtn.'
							</td>
						</tr>';
					$x .= '<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['AdditionalAward'].'</td>
							<td>
								<div id="AdditionalAwardDiv">'.$AwardLevelEntries[AWARD_TYPE_ADDITIONAL].'</div>'.$AddAdditionalAwardBtn.'
							</td>
						</tr>';
				$x .= '</tbody>
				  </table>';
					
			  	$x .= '</div>
				<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
				<input type="hidden" id="AwardSchemeID" name="AwardSchemeID" value="'.$AwardSchemeID.'" />
				<input type="hidden" id="OldAwardName" name="OldAwardName" value="'.htmlspecialchars($AwardInfo[0]['AwardName'],ENT_QUOTES).'" />
				<input type="hidden" id="WinnerGenerated" name="WinnerGenerated" value="'.$WinnerGenerated.'">
				<input type="hidden" id="EntryNumber" name="EntryNumber" value="'.$EntryNum.'" />
			  </form>';
		$x .= $this->MandatoryField();
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Check_Award_Name(1);","submit_btn").'
					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./award_scheme.php';","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
		return $x;
	}
	
	function Settings_Get_Award_Level_Entry($EntryNumber,$AwardLevelID="", $AwarType="")
	{
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $image_path, $LAYOUT_SKIN, $ReadingGardenLib;
		
		if($AwardLevelID != '')
			$AwardLevelRequirement = $ReadingGardenLib->Get_Award_Scheme_Requirement($AwardLevelID,false);

		$LevelTable .= '<table class="form_table_v30" cellpadding=0 cellspacing=0  width="100%">'."\n";

		$NameField = $AwarType==AWARD_TYPE_GENERAL?$Lang['ReadingGarden']['LevelName']:$Lang['ReadingGarden']['PrizeName'];
		$LevelTable .= '<tr>';
		$LevelTable .= 	'<td class="field_title">'.$NameField.'</td>';
		$LevelTable .= 	'<td>
					<input class="LevelName" type="text" maxlength="255" size="20" id="LevelName'.$EntryNumber.'" name="LevelName'.$EntryNumber.'" value="'.$AwardLevelRequirement[0]['LevelName'].'" />
					<div class="LevelNameWarningDiv" id="LevelNameWarningDiv'.$EntryNumber.'" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputLevelName'].'</div>
				</td>';
		$LevelTable .= '</tr>';
		$LevelTable .= '<tr>';
		$LevelTable .= 	'<td class="field_title">'.$Lang['ReadingGarden']['MinBookRequired'].'</td>';
		$LevelTable .= 	'<td>
					<input class="MinBookRead" id="MinBookRead'.$EntryNumber.'" name="MinBookRead'.$EntryNumber.'" type="text" class="textboxnum" maxlength="10" size="10" value="'.$AwardLevelRequirement[0]['LevelMinBookRead'].'" />
					<div class="MinBookWarningDiv" id="MinBookWarningDiv'.$EntryNumber.'" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>';
		$LevelTable .= 	'</td>';
		$LevelTable .= '</tr>';
		$LevelTable .= '<tr>';
		$LevelTable .= '<td class="field_title">'.$Lang['ReadingGarden']['MinReportRequired'].'</td>';
		$LevelTable .= 	'<td>
					<input class="MinReportSubmit" id="MinReportSubmit'.$EntryNumber.'" name="MinReportSubmit'.$EntryNumber.'" type="text" class="textboxnum" maxlength="10" size="10" value="'.$AwardLevelRequirement[0]['MinReportSubmit'].'" />
					<div class="MinReportWarningDiv" id="MinReportWarningDiv'.$EntryNumber.'" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>';
		$LevelTable .= 	'</td>';
		$LevelTable .= '</tr>';
		
		# Requirement Edition Layer
		$RequirementLayer = '';
		//if(isset($this->CachedCategorySelection)){
		//	$CategorySelection = $this->CachedCategorySelection;
		//}else{
			$CategorySelection = $this->Get_Category_Selection("CategoryID".$EntryNumber, "CategoryID".$EntryNumber,'', '', '', '', '', false);
		//	$this->CachedCategorySelection = $CategorySelection;
		//}
		$RequirementLayer .= $CategorySelection;
		$RequirementLayer .= $this->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "Add_Requirement_Item($EntryNumber);", $ParName="", $ParOtherAttribute="").'&nbsp;';
		$RequirementLayer .= $this->GET_SMALL_BTN($Lang['Btn']['DeleteAll'], "button", "$('div#DivRequirement".$EntryNumber."').html('');", $ParName="", $ParOtherAttribute="");
		$RequirementLayer .= '<div class="CategoryWarningLayer" id="CategoryWarningLayer'.$EntryNumber.'" style="color:red;display:none;"></div>';
		$RequirementLayer .= '<div id="DivRequirement'.$EntryNumber.'" >';
		for($i=0;$i<sizeof($AwardLevelRequirement);$i++){
			if(trim($AwardLevelRequirement[$i]['RequirementID'])=='') continue;
			$cat_id = $AwardLevelRequirement[$i]['CategoryID'];
			$cat_code = $AwardLevelRequirement[$i]['CategoryCode'];
			$cat_name = $AwardLevelRequirement[$i]['CategoryName'];
			$min_book_read = $AwardLevelRequirement[$i]['MinBookRead'];
			$RequirementLayer .= '<div>'.htmlspecialchars($cat_code." ".$cat_name).'<input class="RequirementItemCategory" type="hidden" name="RequirementItemCategory'.$EntryNumber.'[]" value="'.$cat_id.'"/>&nbsp;';
			//$RequirementLayer .= '<input type="hidden" name="RequirementID'.$EntryNumber.'[]" value="'.$AwardLevelRequirement[0]['RequirementID'].'" />';
			$RequirementLayer .= '<input class="RequirementItemQuantity" name="RequirementItemQuantity'.$EntryNumber.'[]" type="text" maxlength="10" size="10" value="'.$min_book_read.'"/>&nbsp;';
			$RequirementLayer .= '<a href="javascript:void(0);" onclick="$(this).parent().remove();"><img border="0" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_delete.gif" title="'.$Lang['Btn']['Delete'].'" /></a>&nbsp;</div>';
		}
		$RequirementLayer .= '</div>';
		
		$LevelTable .= '<tr>
					<td class="field_title">'.$Lang['ReadingGarden']['Requirement'].'</td>
					<td>'.$RequirementLayer.'</td>
				</tr>';
		$LevelTable .= '<tr>';
		$LevelTable .= '<td colspan="2" style="text-align:center;">[<a href="javascript:void(0);" onclick="$(this).closest(\'.AwardLevelEntryClass\').remove();">'.$Lang['Btn']['Delete'].'</a>]</td>';
		$LevelTable .= '</tr>';
		$LevelTable .= '</table>'."\n";
		
		$x .= '<div class="AwardLevelEntryClass" id="AwardLevelEntry'.$EntryNumber.'" >';
		if($AwarType==AWARD_TYPE_ADDITIONAL)
		{
			$x .= $LevelTable;
		}
		else
		{
			$x .= '<table cellspacing="0" cellpadding="0" border="0" style="background-color:#fff" width="100%">'."\n";
				$x .= '<tr>';
					$x .= '<td>'.$this->GET_LNK_MOVE("javascript:void(0);","",1).'</td>';
					$x .= '<td>';
						$x .= $LevelTable;
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</table>'."\n";
		}
		$x .= '<input type="hidden" name="AwardLevelID'.$EntryNumber.'" value="'.$AwardLevelID.'" />'."\n";
		$x .= '<input type="hidden" name="AwardType'.$EntryNumber.'" value="'.($AwarType==AWARD_TYPE_ADDITIONAL?AWARD_TYPE_ADDITIONAL:AWARD_TYPE_GENERAL).'" />';
		$x .= '<input type="hidden" name="AwardLevelEntryNumber[]" value="'.$EntryNumber.'" />'."\n";
		$x .= '<br />'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}

	function Settings_Award_Scheme_Extra_Setting()
	{
		global $Lang, $ReadingGardenLib;
		
		$RequireSelectAwardScheme = $ReadingGardenLib->Settings['RequireSelectAwardScheme'];
		
		$x .= '<form id="form1" name="form1" action="extra_setting_update.php" method="POST">'."\n";
			$x .= '<div style="text-align:right; height:20px;">'.$this->Get_Small_Btn($Lang['Btn']['Edit'], "button", "js_Edit_View()", "", "", "DisplayView").'</div>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['RequireStudentToSelectAwardScheme'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="EditView" style="display:none">'."\n";
							$RequireSelectAwardScheme == 1?$EnableChecked=1:$DisableChecked=1;
							$x .= $this->Get_Radio_Button("Enable", "RequireSelectAwardScheme", 1, $EnableChecked, $Class="", $Lang['ReadingGarden']['Enable'])."\n";
							$x .= $this->Get_Radio_Button("Disable", "RequireSelectAwardScheme", 0, $DisableChecked, $Class="", $Lang['ReadingGarden']['Disable'])."\n";
						$x .= '</div>'."\n";
						$x .= '<div class="DisplayView">'."\n";
							$LangKey = $RequireSelectAwardScheme == 1?"Enable":"Disable";
							$x .= $Lang['ReadingGarden'][$LangKey]."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";	
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<div class="EditView" style="display:none">'."\n";
					$x .= $this->Get_Action_Btn($Lang['Btn']['Submit'],"submit")."\n";
					$x .= $this->Get_Action_Btn($Lang['Btn']['Cancel'],"button","js_Display_View()")."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;
	}
	
	function Settings_Student_Reading_Target_Index()
	{
		global $PATH_WRT_ROOT,$Lang,$intranet_root,$linterface,$ReadingGardenLib;
		
		$ThickboxHeight = "550";
		$ThickboxWidth = "750";
		$ThickboxAddButton = $this->Get_Thickbox_Link($ThickboxHeight, $ThickboxWidth, "add_dim", $Lang['ReadingGarden']['NewReadingTarget'], "Get_Reading_Target_Edit_Form(1,'');", "FakeLayer", $Lang['ReadingGarden']['NewReadingTarget'], "");
		
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="reading_target.php" onsubmit="Get_Reading_Target_Table();return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $ThickboxAddButton."\n";
										$x .= '</div>'."\n";
										$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
										$x .= '<tr>'."\n";
											$x .= '<td valign="bottom">'."\n";
												$x .= '<div class="table_filter">'."\n";
													
												$x .= '</div>'."\n";
											$x .= '</td>'."\n";	
											$x .= '<td valign="bottom">'."\n";
												$x .= '<div class="common_table_tool">'."\n";
									        		$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'ReadingTargetID[]\',\'Get_Reading_Target_Edit_Form(0, false);\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
													$x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'ReadingTargetID[]\',\'Delete_Reading_Target();\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
												$x .= '</div>'."\n";
											$x .= '</td>'."\n";	
										$x .= '</tr>'."\n";
									$x .= '</table>'."\n"; 
											
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DivReadingTargetTable">'."\n";
										//$x .= $this->Settings_Student_Reading_Target_Table();
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Settings_Student_Reading_Target_Table($Keyword="")
	{
		global $PATH_WRT_ROOT,$Lang,$intranet_root,$linterface,$ReadingGardenLib;
		
		$reading_target_list = $ReadingGardenLib->Get_Student_Reading_Target_List("",$Keyword);
		$size_reading_target_list = sizeof($reading_target_list);
		
		$ThickboxHeight = "600";
		$ThickboxWidth = "750";
		
		$x = '<table class="common_table_list_v30">
				<thead>
					<tr>
					<th width="1" rowspan="2">#</th>
					<th width="20%" rowspan="2">'.$Lang['ReadingGarden']['ReadingTargetName'].'</th>
					<th width="15%" rowspan="2">'.$Lang['ReadingGarden']['Period'].'</th>
					<th width="25%" colspan="3">'.$Lang['ReadingGarden']['NumberOfReadingRequired'].'</th>
					<th width="25%" colspan="2">'.$Lang['ReadingGarden']['NumberOfBookReportRequired'].'</th>
					<th width="20%" rowspan="2">'.$Lang['ReadingGarden']['TargetClass'].'</th>
					<th width="10px" rowspan="2">
						'.$this->Get_Checkbox("CheckMaster", "", "", 0, '', '', 'Check_All_Reading_Target(this.checked)').'
					</th>
					</tr>
					<tr>
					<th>'.$Lang['ReadingGarden']['Chinese'].'</th>
					<th>'.$Lang['ReadingGarden']['English'].'</th>
					<th>'.$Lang['ReadingGarden']['Either'].'</th>
					<th>'.$Lang['ReadingGarden']['Chinese'].'</th>
					<th>'.$Lang['ReadingGarden']['English'].'</th>
					</tr>
				</thead>
				<tbody>';
		if($size_reading_target_list==0){
			$x .= '<tr><td colspan="10" align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
		}
		for($i=0;$i<$size_reading_target_list;$i++){
			$ReadingTargetID = $reading_target_list[$i]['ReadingTargetID'];
			$TargetName = $reading_target_list[$i]['TargetName'];
			$StartDate = $reading_target_list[$i]['StartDate'];
			$EndDate = $reading_target_list[$i]['EndDate'];
			$ReadingRequiredCh = $reading_target_list[$i]['ReadingRequiredCh'];
			$BookReportRequiredCh = $reading_target_list[$i]['BookReportRequiredCh'];
			$ReadingRequiredEn = $reading_target_list[$i]['ReadingRequiredEn'];
			$BookReportRequiredEn = $reading_target_list[$i]['BookReportRequiredEn'];
			$ReadingRequiredOthers = $reading_target_list[$i]['ReadingRequiredOthers'];
			
			$TargetClassList = $ReadingGardenLib->Get_Student_Reading_Target_Class($ReadingTargetID,true);
			
			$TargetClass = array();
			for($j=0;$j<sizeof($TargetClassList);$j++){
				$class_name = Get_Lang_Selection($TargetClassList[$j]['ClassTitleB5'],$TargetClassList[$j]['ClassTitleEN']);
				$TargetClass[] = htmlspecialchars($class_name);
			}
			if(sizeof($TargetClass)==0) $TargetClass = $Lang['General']['EmptySymbol'];
			else $TargetClass = implode(", ",$TargetClass);
			
			$ThickboxEditButton = $this->Get_Thickbox_Link($ThickboxHeight, $ThickboxWidth, "", $Lang['ReadingGarden']['EditReadingTarget'], "Get_Reading_Target_Edit_Form(0,".$ReadingTargetID.");", "FakeLayer", htmlspecialchars($TargetName,ENT_QUOTES), "");
			//$ThickboxDeleteButton = '<a href="#" class="delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="Delete_Reading_Target('.$ReadingTargetID.');return false;"></a>';
			
			$ReadingTargetPeriod = $StartDate.'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$EndDate;
			
			$x .= '<tr>';
			$x .= '<td>'.($i+1).'</td>';
			$x .= '<td>'.$ThickboxEditButton.'</td>';
			$x .= '<td>'.$ReadingTargetPeriod.'</td>';
			$x .= '<td>'.$ReadingRequiredCh.'</td>';
			$x .= '<td>'.$ReadingRequiredEn.'</td>';
			$x .= '<td>'.$ReadingRequiredOthers.'</td>';
			$x .= '<td>'.$BookReportRequiredCh.'</td>';
			$x .= '<td>'.$BookReportRequiredEn.'</td>';
			$x .= '<td>'.$TargetClass.'</td>';
			$x .= '<td>'.$this->Get_Checkbox("", "ReadingTargetID[]", $ReadingTargetID, 0, '', '', '').'</td>';
			$x .= '</tr>';
		}
		
		$x .= '	</tbody>
			  </table>
			<br />';
		
		return $x;
	}
	
	function Settings_Student_Reading_Target_Edit_Form($IsNew,$ReadingTargetID='')
	{
		global $PATH_WRT_ROOT, $Lang, $intranet_root, $image_path, $LAYOUT_SKIN, $ReadingGardenLib;
		
//		include_once("form_class_manage_ui.php");
//		$fcm_ui = new form_class_manage_ui();
//		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$ReadingTargetClassList = array();
		if(trim($ReadingTargetID)!=''){
			$ReadingTargetInfo = $ReadingGardenLib->Get_Student_Reading_Target_List($ReadingTargetID);
			$ReadingTargetClassMappings = $ReadingGardenLib->Get_Student_Reading_Target_Class($ReadingTargetID,true);
			for($i=0;$i<sizeof($ReadingTargetClassMappings);$i++){
				$ReadingTargetClassList[] = $ReadingTargetClassMappings[$i]['ClassID'];
			}
		}
		
		//$StartDate = trim($ReadingTargetInfo[0]['StartDate'])!=""?$ReadingTargetInfo[0]['StartDate']:date("Y-m-d",strtotime(getStartDateOfAcademicYear($AcademicYearID)));
		//$EndDate = trim($ReadingTargetInfo[0]['EndDate'])!=""?$ReadingTargetInfo[0]['EndDate']:date("Y-m-d",strtotime(getEndDateOfAcademicYear($AcademicYearID)));
		$StartDate = trim($ReadingTargetInfo[0]['StartDate'])!=""?$ReadingTargetInfo[0]['StartDate']:date("Y-m-d");
		$EndDate = trim($ReadingTargetInfo[0]['EndDate'])!=""?$ReadingTargetInfo[0]['EndDate']:date("Y-m-d");
		
//		$YearClassSelection = $fcm_ui->Get_Class_Selection($AcademicYearID, '', 'ClassID[]', $ReadingTargetClassList, $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
//		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);");
		
		# Class Selection
		include_once("libclass.php");
		$lclass = new libclass();
		$YearClassSelection = $lclass->getSelectClassID(" id='ClassID[]' name='ClassID[]' multiple size=10 ",$ReadingTargetClassList,2);
		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);");
		
		
		$x = $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form name="ReadingTargetEditForm" id="ReadingTargetEditForm" onsubmit="return false;">
			  	<div id="ReadingTargetEditDiv" style="height:430px; overflow:auto;">
				  <table class="form_table_v30" style="width:700px;">
					<tbody>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['ReadingTargetName'].'</td>
							<td>
								<input name="TargetName" id="TargetName" type="text" class="textbox_name" maxlength="255" value="'.htmlspecialchars($ReadingTargetInfo[0]['TargetName'],ENT_QUOTES).'" />
								<div id="TargetNameWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Period'].'</td>
							<td>'.$this->GET_DATE_PICKER("StartDate",$StartDate).'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$this->GET_DATE_PICKER("EndDate",$EndDate).'
								<div id="DatePeriodWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['NumberOfReadingRequired'].'('.$Lang['ReadingGarden']['Chinese'].')'.'</td>
							<td>
								<input name="ReadingRequiredCh" id="ReadingRequiredCh" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingTargetInfo[0]['ReadingRequiredCh'].'" />
								<div id="ReadingRequiredChWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['NumberOfReadingRequired'].'('.$Lang['ReadingGarden']['English'].')'.'</td>
							<td>
								<input name="ReadingRequiredEn" id="ReadingRequiredEn" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingTargetInfo[0]['ReadingRequiredEn'].'" />
								<div id="ReadingRequiredEnWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['NumberOfReadingRequired'].'('.$Lang['ReadingGarden']['Either'].')'.'</td>
							<td>
								<input name="ReadingRequiredOthers" id="ReadingRequiredOthers" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingTargetInfo[0]['ReadingRequiredOthers'].'" />
								<div id="ReadingRequiredOthersWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['NumberOfBookReportRequired'].'('.$Lang['ReadingGarden']['Chinese'].')'.'</td>
							<td>
								<input name="BookReportRequiredCh" id="BookReportRequiredCh" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingTargetInfo[0]['BookReportRequiredCh'].'" />
								<div id="BookReportRequiredChWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['NumberOfBookReportRequired'].'('.$Lang['ReadingGarden']['English'].')'.'</td>
							<td>
								<input name="BookReportRequiredEn" id="BookReportRequiredEn" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingTargetInfo[0]['BookReportRequiredEn'].'" />
								<div id="BookReportRequiredEnWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'].'</div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['TargetClass'].'</td>
							<td>'.$YearClassSelection.'&nbsp;'.$YearClassSelectAllBtn.'<br />'.$this->MultiSelectionRemark().'
								<div id="TargetClassWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses'].'</div>
							</td>
						</tr>
					</tbody>
				  </table>
			  	</div>
				<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
				<input type="hidden" id="ReadingTargetID" name="ReadingTargetID" value="'.$ReadingTargetID.'" />
				<input type="hidden" id="OldTargetName" name="OldTargetName" value="'.htmlspecialchars($ReadingTargetInfo[0]['TargetName'],ENT_QUOTES).'" />
				<input type="hidden" id="OverlapPeriod" name="OverlapPeriod" value="0" />
			  </form>';
		$x .= $this->MandatoryField();
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Check_Reading_Target_Name(1);","submit_btn").'
					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
		return $x;
	}
	
	function Get_Student_Recommend_Book_List_Index($field="",$order="",$pageNo="",$numPerPage="",$CategoryID="", $BookIDArr="",$Keyword="")
	{
		global $Lang;
		
		if($BookIDArr)
		{
			# Step
			$StepsObj[] = array($Lang['ReadingGarden']['BookInfo'],0);
			$StepsObj[] = array($Lang['ReadingGarden']['ReadingStatus'],1);
			
			# Get Navigation
			$navigation_arr = array(
				array($Lang['Header']['Menu']['Home'],"../index.php"),
				array($Lang['ReadingGarden']['MyReadingBook'],"../student_record/index.php?CurrentTab=ReadingRecord"),
				array($Lang['ReadingGarden']['AddReadingRecord'],"")
			);
			
			$Steps = $this->GET_STEPS_IP25($StepsObj);
	
			$BackToBookSearch = $this->GET_ACTION_BTN($Lang['ReadingGarden']['AddOtherReadingRecord'],"button","window.location='../student_record/new_reading_record.php'");
			$BackToMyRecord = $this->GET_ACTION_BTN($Lang['ReadingGarden']['BackToMyReadingRecord'],"button","window.location='../student_record/index.php?CurrentTab=ReadingRecord'");
			
			$edit_bottom = '<div class="edit_bottom_v30">'.$BackToBookSearch.'&nbsp;'.$BackToMyRecord.'</div>';
		}
		else
		{
			$SubTagsObj[] = array($Lang['ReadingGarden']['RecommendBook'],"",1);
			$SubTagsObj[] = array($Lang['ReadingGarden']['RecommendBookReport'],"../recommend_report/student_recommend_book_report.php");

   			$navigation_arr = array(
				array($Lang['Header']['Menu']['Home'],"../index.php"),
				array($Lang['ReadingGarden']['RecommendBookList'],"")
			);
   
		}
//		$x .= '<br /><br />';
		$x.= $this->GET_NAVIGATION_IP25($navigation_arr,"navigation_2");
		$x .= $Steps;
		
		$x .= $this->GET_SUBTAGS($SubTagsObj);
		$x .= '<form id="form1" name="form1" action="student_recommend_book.php" method="post" onsubmit="Get_Recommend_Book_CoverView_Table();return false;">';
		
		//$x .= '<div class="reading_book_board">';
		
		# Category List
		$SelectTitle = $Lang['ReadingGarden']['SelectTitleArr']['AllCategory'];
        $CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID",$CategoryID,$SelectTitle," Get_Recommend_Book_CoverView_Table(); ");
        
        $sort_fields = array(
        					array("0",$Lang['ReadingGarden']['BookName']),
        					array("1",$Lang['ReadingGarden']['CallNumber']),
        					array("2",$Lang['ReadingGarden']['Author']),
        					array("3",$Lang['ReadingGarden']['Publisher'])
        				);
        $SortbySelection = getSelectByArray($sort_fields, "id='sortby' name='sortby' onchange=\"Sort_Table('sortby', 'field');\" ", $field, 0, 1, "", 1);
        
		# Book List
		$x .= '<div class="reading_book_list">';
		$x .= '		<div class="content_top_tool">
						<div class="table_filter">
							'.$CategorySelection.' | '.$Lang['ReadingGarden']['SortBy'].': '.$SortbySelection.'
						</div>';
						$x .= $this->Get_Search_Box_Div('Keyword',$Keyword);
		//'
        //                <div class="thumb_list_tab">
		//					<!--<a href="#"><span>'.$Lang['ReadingGarden']['ListView'].'</span></a><em>|</em>-->
		//					<a class="thumb_list_tab_on" href="javascript:void(0);" onclick="Get_Recommend_Book_CoverView_Table();"><span>'.$Lang['ReadingGarden']['BookCoverView'].'</span></a>
		//				</div>';
		$x .= '		</div>
					<p class="spacer"></p>';
		$x .= '		<div id="DivBookTable">';
		//$x .= $this->Get_Student_Recommend_Book_List_DBTable();
		$x .= '		</div>';
		$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$pageNo.'" />';
		$x .= '<input type="hidden" id="order" name="order" value="'.$order.'" />';
		$x .= '<input type="hidden" id="field" name="field" value="'.$field.'" />';
		$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
		$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$numPerPage.'" />';
		foreach((array)$BookIDArr as $PostBookID)
			$x .= '<input type="hidden" name="BookIDArr[]" value="'.$PostBookID.'" />';	
		
		$x .= '</form>';
		
		$x .= '</div>';// end of reading_book_list
		$x .= $edit_bottom;
		
		//$x .= '</div>';
		$x .= '<br><br>';
		
		return $x;
	}
	
	function Get_Student_Recommend_Book_List_DBTable($field="",$order="",$pageNo="",$numPerPage="",$CategoryID="",$Keyword="", $BookIDArr="", $ClassID="")
	{
		global $Lang, $page_size, $ck_recommend_book_list_page_size, $ReadingGardenLib;

		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_recommend_book_list_page_size) && $ck_recommend_book_list_page_size != "") $page_size = $ck_recommend_book_list_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Student_Recommend_Book_List_DBTable_Sql($CategoryID, $Keyword, $BookIDArr, $ClassID);
		
		$li->count_mode = 1;
		$li->sql = $sql;
		$li->IsColOff = "ReadingScheme_RecommendBookCoverView";
		
		$li->field_array = array("BookName","CallNumber","Author","Publisher");
		$li->column_array = array(18,18,18,18);
		$li->wrap_array = array(0,0,0,0);
		
		$x .= $li->display();
		
		return $x;
	}
	
//	function Get_Recommend_Book_Reading_Record_Edit_Form($BookID, $IsNew=1, $ReadingRecordID='')
//	{
//		global $Lang, $ReadingGardenLib;
//		
//		if($IsNew!=1){
//			$ReadingRecordInfo = $ReadingGardenLib->Get_Student_Reading_Record($ReadingRecordID);
//		}
//		$BookInfo = $ReadingGardenLib->Get_Book_List($BookID);
//		
//		$x = $this->Get_Thickbox_Return_Message_Layer();
//		$x .= '<form name="ReadingRecordEditForm" id="ReadingRecordEditForm" onsubmit="return false;">
//			  	<div id="ReadingRecordEditDiv">
//				  <table class="form_table_v30">
//					<tbody>';
//		$x .= '
//						<tr>
//							<td class="field_title">'.$Lang['ReadingGarden']['BookName'].'</td>
//							<td>'.htmlspecialchars($BookInfo[0]['BookName']).'</td>
//						</tr>
//						<tr>
//							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StartDate'].'</td>
//							<td>'.$this->GET_DATE_PICKER("StartDate",$ReadingRecordInfo[0]['StartDate']).'
//								<div id="StartDateWarningDiv" style="color:red;display:none"></div>
//							</td>
//						</tr>
//						<tr>
//							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['FinishedDate'].'</td>
//							<td>'.$this->GET_DATE_PICKER("FinishedDate",$ReadingRecordInfo[0]['FinishedDate']).'
//								<div id="FinishedDateWarningDiv" style="color:red;display:none"></div>
//							</td>
//						</tr>
//						<tr>
//							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['ReadingHours'].'</td>
//							<td>
//								<input name="ReadingHours" id="ReadingHours" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingRecordInfo[0]['NumberOfHours'].'" /> '.$Lang['ReadingGarden']['Hour(s)'].'
//								<div id="ReadingHoursWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumber'].'</div>
//							</td>
//						</tr>
//						<tr>
//							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Progress'].'</td>
//							<td>
//								<input name="Progress" id="Progress" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingRecordInfo[0]['Progress'].'" /> %
//								<div id="ProgressWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumberWithin100'].'</div>
//							</td>
//						</tr>
//					</tbody>
//				  </table>
//			  	</div>
//				<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
//				<input type="hidden" id="BookID" name="BookID" value="'.$BookID.'" />
//				<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />
//			  </form>';
//		
//		$x .= '<div class="edit_bottom_v30">
//					<p class="spacer"></p>
//					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Check_Reading_Record(1);","submit_btn").'
//					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
//					<p class="spacer"></p>
//			   </div>';
//		return $x;
//	}
	
	function Get_Student_Reading_Record_Layer($ReadingRecordID)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		$ReadingRecordInfo = $ReadingGardenLib->Get_Student_Reading_Record($ReadingRecordID);
		
		$x = '<b>'.$Lang['ReadingGarden']['ReadingRecord'].'</b><br />';
		$x .= $Lang['ReadingGarden']['StartDate'].': '.$ReadingRecordInfo[0]['StartDate'].'<br />';
		$x .= $Lang['ReadingGarden']['FinishedDate'].': '.$ReadingRecordInfo[0]['FinishedDate'].'<br />';
		$x .= $Lang['ReadingGarden']['ReadingHours'].': '.$ReadingRecordInfo[0]['NumberOfHours'].'<br />';
//		$x .= $Lang['ReadingGarden']['Progress'].': '.$ReadingRecordInfo[0]['Progress'].' %<br />';
		
		return $x;
	}
	
	#############################################################################
	# If you modify this function, you may also modify Get_My_Record_New_My_Reading_Book_Layer_Step1_External_Book
	# Please take a look of this function  
	#############################################################################
	//20111013
	function Get_Recommend_Book_Reading_Record_Edit_Form($BookID, $IsNew=1, $ReadingRecordID='',$CallBack='')
	{
		global $Lang, $ReadingGardenLib, $PATH_WRT_ROOT;
		
		if($IsNew!=1){
			$ReadingRecordInfo = $ReadingGardenLib->Get_Student_Reading_Record($ReadingRecordID);
		}
		$BookInfo = $ReadingGardenLib->Get_Book_List($BookID);
		
		$x = $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form name="ReadingRecordEditForm" id="ReadingRecordEditForm" onsubmit="return false;">
			  	<div id="ReadingRecordEditDiv">
				  <table class="form_table_v30">
					<tbody>';
		$x .= '
						<tr>
							<td class="field_title">'.$Lang['ReadingGarden']['BookName'].'</td>
							<td>'.htmlspecialchars($BookInfo[0]['BookName']).'</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StartDate'].'</td>
							<td>'.$this->GET_DATE_PICKER("StartDate",$ReadingRecordInfo[0]['StartDate'],'','','','','','','',1).'
								<div id="StartDateWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['FinishedDate'].'</td>
							<td>'.$this->GET_DATE_PICKER("FinishedDate",$ReadingRecordInfo[0]['FinishedDate'],'','','','','','','',1).'
								<div id="FinishedDateWarningDiv" style="color:red;display:none"></div>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['ReadingHours'].'</td>
							<td>
								<input name="ReadingHours" id="ReadingHours" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingRecordInfo[0]['NumberOfHours'].'" /> '.$Lang['ReadingGarden']['Hour(s)'].'
								<div id="ReadingHoursWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumber'].'</div>
							</td>
						</tr>';
						
						$AwardSchemeID = $ReadingRecordInfo[0]['AwardSchemeID'];
						if($ReadingGardenLib->Settings['RequireSelectAwardScheme'])
						{
							$ForStudentSelect = 1;
							if($AwardSchemeID>0)
							{
								$AwardSchemeInfo = $ReadingGardenLib->Get_Award_Scheme_List($AwardSchemeID);
								$EndDate = $AwardSchemeInfo[0]['EndDate'];
							}
							
							if($AwardSchemeID>0 && strtotime($EndDate)<time())
							{
								$AwardSchemeSelection .= $AwardSchemeInfo[0]['AwardName']."\n";
								$AwardSchemeSelection .= '<input type="hidden" id="AwardSchemeID" name="AwardSchemeID" value="'.$AwardSchemeID.'">'."\n";
							}
							else
							{
								$Language = array($BookInfo[0]['Language'], CATEGORY_LANGUAGE_OTHERS);
								$AwardSchemeSelection = $this->Get_Award_Scheme_Selection("AwardSchemeID", $AwardSchemeID, $Language, $ForStudentSelect);
							}
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AwardScheme'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $AwardSchemeSelection."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>';
						}
						else
						{
							$x .= '<input type="hidden" id="AwardSchemeID" name="AwardSchemeID" value="'.$AwardSchemeID.'">'."\n";
						}
		
//						$x .= '
//						<tr>
//							<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Progress'].'</td>
//							<td>
//								<input name="Progress" id="Progress" type="text" class="textboxnum" maxlength="10" size="10" value="'.$ReadingRecordInfo[0]['Progress'].'" /> %
//								<div id="ProgressWarningDiv" style="color:red;display:none">'.$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumberWithin100'].'</div>
//							</td>
//						</tr>';
		
		
			$x .= '</tbody>
				</table>
			</div>';

			# Customization [#2012-1009-1402-14054]	
			if($IsNew==1 && $ReadingGardenLib->enableReadingSchemeAddPopularDataTogetherRight()){				
				$x .= $this->Get_Poppular_Award_Scheme_Display_Table($StudentSubmit=1);		
			}

			
		$x .= '<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
			<input type="hidden" id="BookID" name="BookID" value="'.$BookID.'" />
			<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />
		  </form>';
		
		
		$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Check_Reading_Record(1,'$CallBack');","submit_btn").'
					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
			   
			   
		# Customization [#2012-1009-1402-14054] - JS for display category items
		if($IsNew==1 && $ReadingGardenLib->enableReadingSchemeAddPopularDataTogetherRight()){
			$x .= '<script>'."\n";
			$x .= '$(document).ready(function(){
					js_Reload_Popular_Award_Scheme_Add_Layer_Category(1);
					});';
			$x .= '</script>'."\n";
		}
		
		
		return $x;
	}
	
	function Get_Student_Book_Report_Edit_Form($Type=0, $IsNew=0, $ReadingRecordID, $BookReportID='', $AssignedReadingID='',$CallBack='', $CategoryID='')
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		/* Type: 
		 * 	0 - Upload Attachment 
		 *  1 - Answer Sheet
		 *  2 - Online Writing
		 */
		$x = '';
		if($Type==0)
		{
			if(trim($AssignedReadingID)!='')
			{
				$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingID);
				$thisValue = $AssignedReadingInfo[0]["Attachment"];
				if($thisValue)
				{
					global $intranet_root;
					include_once("libfilesystem.php");
					$fs = new libfilesystem();
					$AttList = $ReadingGardenLib->Get_Assign_Reading_Attachment($thisValue);
					$AttachPath = $ReadingGardenLib->AssignReadingAttachmentPath."/".$AssignedReadingInfo[0]["Attachment"];
				}
				
				if(!empty($AttList))
				{
					include_once("libfilesystem.php");
					$fs = new libfilesystem();
					
					$AttName = basename($AttList[0]);
					$Url = Get_Attachment_Download_Link($AttachPath, $AttName);
					$AttLink = '<a href="'.$Url.'">'.$AttName.'</a>';
				}			
			}
			else
			{
				$FileUploadDesc = $ReadingGardenLib->Settings['FileUploadDesc'];
			}
			
			$x = $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<form name="BookReportEditForm" id="BookReportEditForm" onsubmit="return false;">
				  	<div id="BookReportEditDiv">
					  <table class="form_table_v30">
						<tbody>';
			if($FileUploadDesc)
			{
				$x .= '<tr>';
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['FileUploadDesc'].'</td>';
					$x .= '<td>';
						$x .= Get_String_Display($FileUploadDesc);
					$x .= '</td>';
				$x .= '</tr>';
			}
			$x .= '		  <tr>
							<td class="field_title">'.$Lang['ReadingGarden']['BookReport'].'</td>
							<td>
								<input name="BookReportFile" id="BookReportFile" type="file" onchange="$(\'div#FileUploadWarningDiv\').hide();" />
								<div id="FileUploadWarningDiv" style="display:none;color:red;"></div>
							</td>
						  </tr>';
			if($AttLink)
			{
			$x .= '		  <tr>
							<td class="field_title">'.$Lang['ReadingGarden']['Attachment'].'</td>
							<td>
								'.$AttLink.'
							</td>
						  </tr>';
			}						  			
			$x .= '		</tbody>
				  	  </table>
			  		</div>
					<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
					<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />
					<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />
					<iframe style="display: none;" name="FileUploadIframe" id="FileUploadIframe"></iframe>
			  	  </form>';
			$x .= '<div class="edit_bottom_v30">
					<p class="spacer"></p>
					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Submit_Book_Report('$CallBack');","submit_btn").'&nbsp;
					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
					<p class="spacer"></p>
			   </div>';
		}else if($Type==1)
		{
			include_once($PATH_WRT_ROOT."includes/libform.php");
			$libform = new libform();
			
			if($IsNew!=1){
				$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report($BookReportID);
				$StudentSelectedAnswerSheetID = $BookReportInfo[0]['StudentSelectedAnswerSheetID'];
				$AnswerSheetAns = $BookReportInfo[0]['AnswerSheet'];
				$Grade = trim($BookReportInfo[0]['Grade']);
			}

			$AnswerSheetQue = $ReadingGardenLib->Get_Answer_Sheet_From_Assigned_Reading_Or_ReadingRecord($AssignedReadingID,$ReadingRecordID);
			if(!$AnswerSheetQue)
			{
				if(trim($StudentSelectedAnswerSheetID)!='')
				{
					$TemplateInfo = $ReadingGardenLib->Get_Answer_Sheet_Template($StudentSelectedAnswerSheetID);
					$AnswerSheetQue = $TemplateInfo[0]['AnswerSheet'];
				}
				
				if($Grade=='')
				{
					$AnswerSheetSelection = $this->Get_Answer_Sheet_Template_Selection("AnswerSheetID", $StudentSelectedAnswerSheetID, 0, 0, "", "js_Refresh_Answer_Sheet()", $CategoryID);
					if(!$StudentSelectedAnswerSheetID)
						$hideSubmit = " style='display:none;' ";
				}
			}
			
			$x .= $this->Get_Thickbox_Return_Message_Layer();
			$x .= '<form name="ansForm" action="">
					<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
					<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />
					<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />
					<input type="hidden" id="AssignedReadingID" name="AssignedReadingID" value="'.$AssignedReadingID.'" />
					<input type="hidden" id="qStr" name="qStr" value="'.htmlspecialchars($AnswerSheetQue,ENT_QUOTES).'">
					<input type="hidden" id="aStr" name="aStr" value="'.htmlspecialchars($AnswerSheetAns,ENT_QUOTES).'">';
					$x .= $AnswerSheetSelection;
					$x .= '<input type="hidden" id="StudentSelectedAnswerSheetID" name="StudentSelectedAnswerSheetID" value="'.$StudentSelectedAnswerSheetID.'" />';
					$x .= '<input type="hidden" id="OldAnswer" name="OldAnswer" value="'.htmlspecialchars($AnswerSheetAns,ENT_QUOTES).'" />';
			$x .= '</form>';
			$x .= '<div id="AnswerSheetEditDiv">
					</div>';
			$x .= '<div class="edit_bottom_v30">
						<p class="spacer"></p>';
			if($Grade==""){ // not marked by teacher, student can edit
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","finish();Submit_Answer_Sheet('$CallBack');","submit_btn", $hideSubmit).'&nbsp;';
			}
			$x .= $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
						<p class="spacer"></p>
				   </div>';
			
			$x .= '<script>';
			$x .= $libform->getWordsInJS();
			$x .= 'var answer_sheet	= \'Content\';';
			$x .= 'var space10 	= \'<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" />\';';
			$x .= 'var add_btn 	= \''.str_replace("'", "\'",$this->GET_BTN($Lang['Btn']['Add'], 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2')).'\';';
			
			$x .= 'var replyslip = \''.$Lang['ReadingGarden']['AnswerSheet'].'\';';
			$x .= 'var MoveUpBtn = \''.$Lang['Button']['MoveUp'].'\';';
			$x .= 'var MoveDownBtn = \''.$Lang['Button']['MoveDown'].'\';';
			$x .= 'var DeleteBtn = \''.$Lang['Button']['Delete'].'\';';
			$x .= 'var EditBtn = \''.$Lang['Button']['Edit'].'\';';
			$x .= 'var s = $(\'input#qStr\').val();
					s = s.replace(/"/g, \'&quot;\');
					$(\'input#qStr\').val(s);
					var form_templates = new Array(); 
				   	var sheet= new Answersheet();
					sheet.qString = s;
					s = $(\'input#aStr\').val();
					s = s.replace(/"/g, \'&quot;\');
					$(\'input#aStr\').val(s);
					sheet.aString = s;';
			if($Grade != ""){
				$x .= 'sheet.autoMark = true;';
			}
			$x .= '	sheet.mode=1; // 0:edit 1:fill in application
					sheet.answer=sheet.sheetArr();
					//sheet.templates=form_templates;
					$(document).ready(function(){
						$(\'div#AnswerSheetEditDiv\').html(editPanel());
						$(\'div#blockInput\').html(sheet.writeSheet());
					});
				   </script>';
		}
		else if($Type==2) // online writing
		{
			if($AssignedReadingID)
			{
				$AssignedReadingInfo = $ReadingGardenLib->Get_Assign_Reading_Info($AssignedReadingID);
				$WritingLimit = $AssignedReadingInfo[0]['WritingLimit']>0?$AssignedReadingInfo[0]['WritingLimit']:0;
				$LowerLimit = $AssignedReadingInfo[0]['LowerLimit']>0?$AssignedReadingInfo[0]['LowerLimit']:0;
				$LimitDisplay = $this->Get_Online_Writing_Word_Limit_Display($LowerLimit, $WritingLimit);
				
				$OnlineWritingDesc = $AssignedReadingInfo[0]["OnlineWritingDesc"];
			}			
			else
			{
				$OnlineWritingDesc = $ReadingGardenLib->Settings['OnlineWritingDesc'];
			}

			if($IsNew!=1){
				$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report($BookReportID);
				$OnlineWriting = $BookReportInfo[0]['OnlineWriting'];
			}
			
			$x .= '<form name="BookReportEditForm" id="BookReportEditForm" onsubmit=" return false;">';
				$x .= '<div id="BookReportEditDiv">';
					$x .= '<table class="form_table_v30">';
						$x .= '<tbody>';
							if($OnlineWritingDesc)
							{
								$x .= '<tr>';
									$x .= '<td class="field_title">'.$Lang['ReadingGarden']['OnlineWritingDesc'].'</td>';
									$x .= '<td>';
										$x .= Get_String_Display($OnlineWritingDesc);
									$x .= '</td>';
								$x .= '</tr>';
							}
							$x .= '<tr>';
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['OnlineWriting'].'</td>';
								$x .= '<td>';
									$x .= $this->GET_TEXTAREA("OnlineWriting", $OnlineWriting, $taCols=70, $taRows=5, $OnFocus = "", $readonly = "0", $other=' onkeyup="js_Check_Word_Count();" ', $class='');
									$x .= $this->Get_Form_Warning_Msg("WarnEmptyOnlineWriting",$Lang['ReadingGarden']['WarningMsg']['PleaseInputWriting'], "WarnMsgDiv");
									$x .= $this->Get_Form_Warning_Msg("WarnExceedWritingLimit",$Lang['ReadingGarden']['WarningMsg']['WordCountExceedLimit'], "WarnMsgDiv");
								$x .= '</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['WordCount'].'</td>';
								$x .= '<td>';
									$x .= '<span id="WordCountSpan">0</span>&nbsp;';
									$x .= '('.$Lang['ReadingGarden']['WordLimit'].': '.$LimitDisplay.')';
								$x .= '</td>';
							$x .= '</tr>';
						$x .= '</tbody>';
					$x .= '</table>';
				$x .= '</div>';
				$x .= '<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />';
				$x .= '<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />';
				$x .= '<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />';
				$x .= '<input type="hidden" id="WritingLimit" name="WritingLimit" value="'.$WritingLimit.'" />';
				$x .= '<input type="hidden" id="LowerLimit" name="LowerLimit" value="'.$LowerLimit.'" />';
			$x .= '</form>';
			$x .= '<div class="edit_bottom_v30">';
				$x .= '<p class="spacer"></p>';
					$x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","js_Check_Online_Writing('$CallBack');","submit_btn").'&nbsp;';
					$x .= $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn");
				$x .= '<p class="spacer"></p>';
			$x .= '</div>';
		}
		return $x;
	}
	
//	function Get_Student_Book_Report_Edit_Form($Type=0, $IsNew=0, $ReadingRecordID, $BookReportID='', $AssignedReadingID='')
//	{
//		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
//		/* Type: 
//		 * 	0 - Upload Attachment 
//		 *  1 - Answer Sheet
//		 */
//		$x = '';
//		if($Type==0)
//		{
//			$x = $this->Get_Thickbox_Return_Message_Layer();
//			$x .= '<form name="BookReportEditForm" id="BookReportEditForm" onsubmit="return false;">
//				  	<div id="BookReportEditDiv">
//					  <table class="form_table_v30">
//						<tbody>';
//			$x .= '		  <tr>
//							<td class="field_title">'.$Lang['ReadingGarden']['BookReport'].'</td>
//							<td>
//								<input name="BookReportFile" id="BookReportFile" type="file" onchange="$(\'div#FileUploadWarningDiv\').hide();" />
//								<div id="FileUploadWarningDiv" style="display:none;color:red;"></div>
//							</td>
//						  </tr>';			
//			$x .= '		</tbody>
//				  	  </table>
//			  		</div>
//					<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
//					<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />
//					<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />
//					<iframe style="display: none;" name="FileUploadIframe" id="FileUploadIframe"></iframe>
//			  	  </form>';
//			$x .= '<div class="edit_bottom_v30">
//					<p class="spacer"></p>
//					'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","Submit_Book_Report();","submit_btn").'
//					'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
//					<p class="spacer"></p>
//			   </div>';
//		}else if($Type==1)
//		{
//			include_once($PATH_WRT_ROOT."includes/libform.php");
//			$libform = new libform();
//			
//			$AnswerSheetQue = $ReadingGardenLib->Get_Answer_Sheet_From_Assigned_Reading_Or_ReadingRecord($AssignedReadingID,$ReadingRecordID);
//			
//			if($IsNew!=1){
//				$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report($BookReportID);
//				$AnswerSheetAns = $BookReportInfo[0]['AnswerSheet'];
//			}
//			
//			$x .= $this->Get_Thickbox_Return_Message_Layer();
//			$x .= '<form name="ansForm" action="">
//					<input type="hidden" id="IsNew" name="IsNew" value="'.$IsNew.'" />
//					<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />
//					<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />
//					<input type="hidden" id="AssignedReadingID" name="AssignedReadingID" value="'.$AssignedReadingID.'" />
//					<input type="hidden" id="qStr" name="qStr" value="'.htmlspecialchars($AnswerSheetQue,ENT_QUOTES).'">
//					<input type="hidden" id="aStr" name="aStr" value="'.htmlspecialchars($AnswerSheetAns,ENT_QUOTES).'">';
//			$x .= '</form>';
//			$x .= '<div id="AnswerSheetEditDiv">
//					</div>';
//			$x .= '<div class="edit_bottom_v30">
//						<p class="spacer"></p>
//						'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","finish();Submit_Answer_Sheet();","submit_btn").'
//						'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.top.tb_remove();","cancel_btn").'
//						<p class="spacer"></p>
//				   </div>';
//			
//			$x .= '<script>';
//			$x .= $libform->getWordsInJS();
//			$x .= 'var answer_sheet	= \'Content\';';
//			$x .= 'var space10 	= \'<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/10x10.gif" width="10" height="1" />\';';
//			$x .= 'var add_btn 	= \''.str_replace("'", "\'",$this->GET_BTN($Lang['Btn']['Add'], 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2')).'\';';
//			
//			$x .= 'var replyslip = \''.$Lang['ReadingGarden']['AnswerSheet'].'\';';
//			$x .= 'var MoveUpBtn = \''.$Lang['Button']['MoveUp'].'\';';
//			$x .= 'var MoveDownBtn = \''.$Lang['Button']['MoveDown'].'\';';
//			$x .= 'var DeleteBtn = \''.$Lang['Button']['Delete'].'\';';
//			$x .= 'var EditBtn = \''.$Lang['Button']['Edit'].'\';';
//			$x .= 'var s = $(\'input#qStr\').val();
//					s = s.replace(/"/g, \'&quot;\');
//					$(\'input#qStr\').val(s);
//					var form_templates = new Array(); 
//				   	var sheet= new Answersheet();
//					sheet.qString = s;
//					s = $(\'input#aStr\').val();
//					s = s.replace(/"/g, \'&quot;\');
//					$(\'input#aStr\').val(s);
//					sheet.aString = s;
//					sheet.mode=1; // 0:edit 1:fill in application
//					sheet.answer=sheet.sheetArr();
//					//sheet.templates=form_templates;
//					$(document).ready(function(){
//						$(\'div#AnswerSheetEditDiv\').html(editPanel());
//						$(\'div#blockInput\').html(sheet.writeSheet());
//					});
//				   </script>';
//		}
//		return $x;
//	}

	## Book Report Marking Page 
	function Get_Book_Report_Marking_Page($ReadingRecordID, $RecommendOnly=0, $PublicPage=0)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		
		$x .= '<frameset id="HorzMainFrameset" border="0" cols="*,250" rows="*">'; 
			$x .= '<frameset id="LeftFrameset" cols="*" rows="70, *">'; //Left Part - Top student info & Bottom attached file
				$x .= '<frame id="TopFrame" name="TopFrame" scrolling="no" src="'.$PATH_WRT_ROOT.'home/eLearning/reading_garden/book_report_marking.php?Part=top&ReadingRecordID='.$ReadingRecordID.'&RecommendOnly='.$RecommendOnly.'&PublicPage='.($PublicPage?1:0).'">';// Top student info
				$x .= '</frame>';
				// Answer sheet view area
			//	$x .= '<frame id="AnswerSheetFrame" name="AnswerSheetFrame" src="">';
			//	$x .= '</frame>';
				// Bottom attachment file view area
				$x .= '<frame id="ViewFrame" name="ViewFrame">';
				$x .= '</frame>';

			$x .= '</frameset>';
			
			$x .= '<frame id="EditFrame" name="EditFrame" scrolling="no" src="'.$PATH_WRT_ROOT.'home/eLearning/reading_garden/book_report_marking.php?Part=right&ReadingRecordID='.$ReadingRecordID.'&RecommendOnly='.$RecommendOnly.'&PublicPage='.($PublicPage?1:0).'">';// Right Part - Mark & Comment area
			
			$x .= '</frame>';
		$x .= '</frameset>';
		
		return $x;
	}
	
	function Get_Book_Report_Marking_Top_Info_Area($ReadingRecordID, $RecommendOnly=0, $PublicPage=0)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		
		$RecordInfo = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade('', $ReadingRecordID);
		$StudentID = $RecordInfo[0]['UserID'];
		$BookReportID = $RecordInfo[0]['BookReportID'];
		
		$ReadingRecordArr = $ReadingGardenLib->Get_Single_Student_Reading_Record_Book_Report_List($StudentID,'',$RecommendOnly);
		
		$StudentName = $ReadingRecordArr[0]['UserName'];
		$BookName = $ReadingRecordArr[0]['BookName'];
		
		if($PublicPage)
		{
			$ReadingRecordArr = BuildMultiKeyAssoc($ReadingRecordArr, "ReadingRecordID");
			
			$ReadingRecordSelection .= $Lang['ReadingGarden']['BookReport'].":";
			$ReadingRecordSelection .= '<input type="hidden" id="ReadingRecordList" name="ReadingRecordList" value="'.$ReadingRecordID.'">';
			$ReadingRecordSelection .= $ReadingRecordArr[$ReadingRecordID]['BookName'];
			
		}
		else
		{
			$ReadingRecordList = array();
			for($i=0;$i<sizeof($ReadingRecordArr);$i++){
				$ReadingRecordList[] = array($ReadingRecordArr[$i]['ReadingRecordID'],$ReadingRecordArr[$i]['BookName']);
			}
			
			$ReadingRecordSelection .= '<div class="handin_list">'."\n";
				$ReadingRecordSelection .= '<span class="field_title">';
				$ReadingRecordSelection .= $Lang['ReadingGarden']['BookReport'].":";
				$ReadingRecordSelection .= '</span>'."\n";
				
				$ReadingRecordSelection .= '<span>'."\n";
				$ReadingRecordSelection .= getSelectByArray($ReadingRecordList, "id='ReadingRecordList' name='ReadingRecordList' onchange='js_Refresh_View_Buttons();'", $ReadingRecordID, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
				$ReadingRecordSelection .= '</span>'."\n";
				
			$ReadingRecordSelection .= '</div>'."\n";			
		}
		
		$x = '<div class="marking_body">'."\n";
		//$x .= '<form id="FormInfoArea" name="FormInfoArea" method="post" action="">'."\n";
		
		
		$x .= '<div class="marking_top">'."\n";
			$x .= '<div class="marking_title">'."\n";
				$x .= '<h1>'.$StudentName.'</h1>';
				$x .= '	<br style="clear: both;">'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="markting_top_detail">'."\n";
			
//				$x .= '<div class="handin_list">'."\n";
//					$x .= '<span class="field_title">';
//					$x .= $Lang['ReadingGarden']['BookReport'].":";
//					$x .= '</span>'."\n";
					
					$x .= $ReadingRecordSelection;
					
//				$x .= '</div>'."\n";

				$x .= '<div class="work_list" id="ViewButtonsLayer">'."\n";
				$x .= '</div>'."\n";
			
			$x .= '</div>'."\n";
			
		$x .= '</div>'."\n";
		
		$x .= '<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />';
		$x .= '<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />';
		
		//$x .= '</form>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Book_Report_Marking_View_Buttons_Layer($ReadingRecordID)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		
		$RecordInfo = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade('', $ReadingRecordID);
		
		$Attachment = trim($RecordInfo[0]['Attachment']);
		$AnswerSheet = trim($RecordInfo[0]['Answersheet']);
		$OnlineWriting = trim($RecordInfo[0]['OnlineWriting']);
		
		$toggle_btns = array();
		if($OnlineWriting!=""){
			$online_tabon_css = 'class="thumb_list_tab_on"';
			$attach_tabon_css = '';
			$ans_tabon_css = '';
			$script = '<script>'."\n";
			$script .= 'js_Load_Online_Writing();'."\n";
			$script .= 'js_Load_Marking_Area();'."\n";
			$script .= '</script>'."\n";
		}elseif($Attachment!=""){
			$attach_tabon_css = 'class="thumb_list_tab_on"';
			$online_tabon_css = '';
			$ans_tabon_css = '';
			$script = '<script>'."\n";
			$script .= 'js_Load_Attachment();'."\n";
			$script .= 'js_Load_Marking_Area();'."\n";
			$script .= '</script>'."\n";
		}elseif($AnswerSheet!=""){
			$online_tabon_css = '';
			$attach_tabon_css = '';
			$ans_tabon_css = 'class="thumb_list_tab_on"';
			$script = '<script>'."\n";
			$script .= 'js_Load_Answersheet();'."\n";
			$script .= 'js_Load_Marking_Area();'."\n";
			$script .= '</script>'."\n";
		}
		
		if($OnlineWriting!=""){
			$toggle_btns[] = '<a id="OnlineWritingBtn" '.$online_tabon_css.' href="javascript:js_Load_Online_Writing();" title="'.$Lang['ReadingGarden']['OnlineWriting'].'"><span>'.$Lang['ReadingGarden']['OnlineWriting'].'</span></a>';
		}
		if($AnswerSheet!=""){
			$toggle_btns[] = '<a id="AnswerSheetBtn" '.$ans_tabon_css.' href="javascript:js_Load_Answersheet();" title="'.$Lang['ReadingGarden']['AnswerSheet'].'"><span>'.$Lang['ReadingGarden']['AnswerSheet'].'</span></a>';
		}
		if($Attachment!=""){
			$toggle_btns[] = '<a id="AttachmentBtn" '.$attach_tabon_css.' href="javascript:js_Load_Attachment();" title="'.$Lang['ReadingGarden']['BookReport'].'"><span>'.$Lang['ReadingGarden']['BookReport'].'</span></a>';
		}
		
		if(sizeof($toggle_btns)>1){
			$toggle_btns_link = implode("<em> | </em>",$toggle_btns);
		}
		$toggle_btns_div = '<div class="thumb_list_tab">'.$toggle_btns_link.'</div>'."\n";
		$toggle_btns_div .= $script;
		return $toggle_btns_div;
	}
	
	function Get_Book_Report_Marking_Edit_Area($ReadingRecordID,$RecommendOnly='', $PublicPage=0)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib, $intranet_session_language;
	
		$identity_type = (!$PublicPage) ? $_SESSION['UserType'] : 0;
		
		$BookReportInfo = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade('', $ReadingRecordID);
		$BookReportID = $BookReportInfo[0]['BookReportID'];
		$BookReportGrade = $BookReportInfo[0]['Grade'];
		$BookReportMark = $BookReportInfo[0]['Mark'];
		$BookReportTeacherComment = trim($BookReportInfo[0]['TeacherComment']);
		$BookReportRecommend = $BookReportInfo[0]['Recommend'];
		$StudentID = $BookReportInfo[0]['UserID'];
		$sticker_display = $Lang['General']['EmptySymbol'];
		$sticker_inner_display = '';
		$sticker_display_height = 0;
		$sticker_display_scrolly = 'auto';
		$existing_sticker = array();
		if($BookReportID > 0){
			$sticker_list = $ReadingGardenLib->Get_Report_Sticker('',$BookReportID,true);
			$sticker_display_height = min(360,sizeof($sticker_list)*120);
			if(sizeof($sticker_list)>3) $sticker_display_scrolly = 'scroll';
			if($identity_type==1){// Teacher
				$sticker_display = '<a href="javascript:void(0);" onclick="js_Get_Sticker(this, jsStickerList, \'DivStickerLayer\', \'DivDisplayStickers\')">[ '.$Lang['ReadingGarden']['GiveSticker'].' ]</a>';
				for($i=0;$i<sizeof($sticker_list);$i++){
					$image_path = $ReadingGardenLib->StickerPath."/".$sticker_list[$i]['ImagePath'];
					$sticker_inner_display .= '<a href="javascript:void(0)" onclick="js_Update_Sticker('.$sticker_list[$i]['StickerID'].',\'0\',\'DivDisplayStickers\');$(this).remove();" title="'.$Lang['ReadingGarden']['ClickToRemoveSticker'].'"><img src="'.$image_path.'" width="120px" height="120px" /><input type="hidden" name="StickerID[]" value="'.$sticker_list[$i]['StickerID'].'" /></a>';
					$existing_sticker[] = $sticker_list[$i]['StickerID'];
				}
			}else{ 
				if(sizeof($sticker_list)>0) $sticker_display = '';
				for($i=0;$i<sizeof($sticker_list);$i++){
					$image_path = $ReadingGardenLib->StickerPath."/".$sticker_list[$i]['ImagePath'];
					$sticker_inner_display .= '<img src="'.$image_path.'" width="120px" height="120px" />';
				}
			}
		}
		$sticker_display .= '<div id="DivDisplayStickers" style="overflow-x: hidden; overflow-y: '.$sticker_display_scrolly.'; height: '.$sticker_display_height.'px; width: 150px;">'.$sticker_inner_display.'</div>';
		
		$x .= '<div id="DivMarkingArea" style="width:250px;">'."\n";
			$x .= '<div id="marking_grade" class="marking_body">'."\n";
				$x .= '<div class="marking_area">'."\n";
				
					$x .= '<div class="marking_top_left">'."\n";
						$x .= '<div class="marking_top_right">'."\n";
							$x .= '<h1>'.$Lang['ReadingGarden']['MarkingArea'].'</h1>';
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
					
					$x .= '<div class="marking_left">'."\n";
						$x .= '<div class="marking_right">'."\n";
							//$x .= '<div class="content_top_tool"><br style="clear: both;"></div>'."\n";
							if($identity_type==1) $x .= $this->Get_Thickbox_Return_Message_Layer();
							else $x .= '<p class="spacer"></p>'."\n";
							$x .= '<form id="FormMarkingArea" name="FormMarkingArea" method="post" action="">'."\n";
							
							$x .= '<table class="form_table">'."\n";
							$x .= '<tbody>'."\n";
							# Grade
							$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Grade'].': </td>'."\n";
							//$x .= '</tr>';
							//$x .= '<tr>';
							$x .= '<td>'.($identity_type==1?$this->GET_TEXTBOX_NUMBER("Grade", "Grade", $BookReportGrade, ""):$BookReportGrade).'</td>'."\n";
							$x .= '</tr>'."\n";

//							# Mark
//							$x .= '<tr>'."\n";
//							$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Mark'].': </td>'."\n";
//							$x .= '<td>'.($identity_type==1?$this->GET_TEXTBOX_NUMBER("Mark", "Mark", $BookReportMark, ""):$BookReportMark).'</td>'."\n";
//							$x .= '</tr>'."\n";
							
							# Teacher comment
							$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['ReadingGarden']['TeacherComment'].':</td>'."\n";
							//$x .= '</tr>';
							//$x .= '<tr>';
							$x .= '<td>'.($identity_type==1?$this->GET_TEXTAREA("TeacherComment", $BookReportTeacherComment, $taCols=20, $taRows=5, $OnFocus = "", $readonly = "0", $other='', $class='textbox'):nl2br(htmlspecialchars($BookReportTeacherComment))).'</td>'."\n";
							$x .= '</tr>'."\n";
							
							if($identity_type==1){
								# Add comment from comment bank button
								$x .= '<tr>'."\n";
								$x .= '<td>&nbsp;</td>'."\n";
								$x .= '<td><a href="javascript:void(0);" onclick="js_Get_Comment(this, jsCommentList, \'DivCommentLayer\', \'TeacherComment\')">[ '.$Lang['ReadingGarden']['AddComment'].' ]</a></td>'."\n";
								$x .= '</tr>'."\n";
							}
							
							# Stickers
							$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Sticker'].':</td>'."\n";
							$x .= '<td>'.$sticker_display.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							if($identity_type==1){
								# Recommend checkbox
								$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'.$this->Get_Checkbox("Recommend", "Recommend", "1", $BookReportRecommend==1, "", $Lang['ReadingGarden']['RecommendThisBookReport'], $Onclick='', $Disabled='').'</td>'."\n";
								$x .= '</tr>'."\n";
							}else{
								$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Recommended'].": ".'</td>'."\n";
								//$x .= '</tr>';
								//$x .= '<tr>';
								$x .= '<td>'."\n";
								$x .= (($BookReportRecommend==1)?$Lang['General']['Yes']:$Lang['General']['No']);
								$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							}
							
							$x .= ' </tbody>
								   </table>'."\n";
							$x .= '<p class="spacer"></p>'."\n";	   
							$x .= '<input type="hidden" id="ReadingRecordID" name="ReadingRecordID" value="'.$ReadingRecordID.'" />';
							$x .= '<input type="hidden" id="BookReportID" name="BookReportID" value="'.$BookReportID.'" />';
							$x .= '<input type="hidden" id="StudentID" name="StudentID" value="'.$StudentID.'" />';
							$x .= '<input type="hidden" id="OldRecommend" name="OldRecommend" value="'.$BookReportRecommend.'" />';
							//$x .= '<input type="hidden" id="ExistingStickerID" name="ExistingStickerID" value="'.implode(",",$existing_sticker).'" />';
							$x .= '<p class="spacer"></p>'."\n";
							
							# Public link
							$PublicLinkKey = "UserID=$StudentID&ReadingRecordID=$ReadingRecordID&RecommendOnly=$RecommendOnly&intranet_session_language=$intranet_session_language";
							$checksum = strlen($PublicLinkKey);
							$PublicLinkKey .= "&checksum=$checksum";
							$EncodedKey = base64_encode($PublicLinkKey);
							$PublicLink= "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/home/eLearning/reading_garden/book_report_marking.php?key=$EncodedKey";
							$x .= $Lang['ReadingGarden']['PublicLink'].':<br />'.$this->GET_TEXTAREA("PublicLink", $PublicLink, $taCols=32, $taRows=3, $OnFocus = "", $readonly = "1", $other='', $class='textbox');
							$x .= '<p class="spacer"></p>'."\n";
							
							if($identity_type==1){
								$x .= '<br /><div class="edit_bottom">
										<p class="spacer"></p>'."\n";
									$x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","js_Save_Marking(0);","submit_btn");
									$x .= $this->GET_ACTION_BTN($Lang['Btn']['Submit'].' &amp; '.$Lang['Btn']['Close'],"button","js_Save_Marking(1);","submit_close_btn");
									$x .= $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.parent.close();","close_btn");
								$x .= '<p class="spacer"></p>
									   </div>'."\n";
							}
							
							$x .= '</form>'."\n";
							$x .= '<br /><br /><p class="spacer"></p>'."\n";
							
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
					
					$x .= '<div class="marking_bottom_left">'."\n";
						$x .= '<div class="marking_bottom_right">'."\n";
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
		
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
			
			# Comment bank js list & sticker js list
			if($identity_type==1){
				$comment_list = $ReadingGardenLib->Get_Comment_Bank_Comments();
				$sticker_list = $ReadingGardenLib->Get_Report_Sticker('','',false);
				$x .= "<SCRIPT LANGUAGE=\"JavaScript\">\n";
				$x .= "var js_preset_no_record = '".$Lang['General']['NoRecordAtThisMoment']."';\n";
				$x .= "var jsCommentList = new Array();\n";
				$x .= "var jsStickerList = new Array();\n";
				for ($i = 0; $i < sizeof($comment_list); $i++)
				{
					$x .= "jsCommentList.push('".htmlspecialchars($comment_list[$i]['Comment'],ENT_QUOTES)."');\n";
				}
				for($i = 0; $i < sizeof($sticker_list); $i++)
				{
					$image_path = $ReadingGardenLib->StickerPath."/".$sticker_list[$i]['ImagePath'];
					$is_added = in_array($sticker_list[$i]['StickerID'],$existing_sticker)?'1':'0';
					$x .= "jsStickerList.push({StickerID:".$sticker_list[$i]['StickerID'].",ImagePath:'".$image_path."',IsAdded:".$is_added."});";
				}
				$x .= "</SCRIPT>\n";
				$x .= "<div id='DivCommentLayer' style='display:none'></div>";
				$x .= "<div id='DivStickerLayer' style='display:none'></div>";
			}
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Student_Recommend_Book_Report_List_Index($field="",$order="",$pageNo="",$numPerPage="", $Keyword="")
	{
		global $Lang;
		
		$tabs .= '<div class="shadetabs">
	                 <ul>
					   <li><a href="../recommend_book/student_recommend_book.php">'.$Lang['ReadingGarden']['RecommendBook'].'</a></li>
	                   <li class="selected"><a href="student_recommend_book_report.php">'.$Lang['ReadingGarden']['RecommendBookReport'].'</a></li>
	                 </ul>
	               </div>
				   <p class="spacer"></p>
				   <p class="spacer"></p>';
			   
		$navigation_arr = array(
			array($Lang['Header']['Menu']['Home'],"../index.php"),
			array($Lang['ReadingGarden']['RecommendBookReport'],"")
		);
		
		$x.= $this->GET_NAVIGATION_IP25($navigation_arr,"navigation_2");
		
		$x .= $tabs;
		$x .= '<form id="form1" name="form1" action="student_recommend_book_report.php" method="post" onsubmit="js_Get_Recommend_Book_Report_Table();return false;">';
			$x .= '<div class="content_top_tool">'."\n";
				$x .= $this->Get_Search_Box_Div('Keyword',$Keyword);
			$x .= '</div>';
			$x .= '<div id="DivBookReportTable" class="table_board">';
			//$x .= $this->Get_Student_Recommend_Book_Report_List_DBTable($field,$order,$pageNo,$numPerPage,$Keyword);
			$x .= '</div>';
			
			$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" id="order" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" id="field" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
			$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$numPerPage.'" />';
		$x .= '</form>';
		$x .= '<br><br>';

		
		return $x;
	}
	
	function Get_Student_Recommend_Book_Report_List_DBTable($field="",$order="",$pageNo="",$numPerPage="",$Keyword="")
	{
		global $Lang, $page_size, $ck_recommend_book_report_list_page_size, $ReadingGardenLib;

		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 0 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_recommend_book_report_list_page_size) && $ck_recommend_book_report_list_page_size != "") $page_size = $ck_recommend_book_report_list_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Student_Recommend_Book_Report_DBTable_Sql($Keyword);
		
		$li->count_mode = 1;
		$li->sql = $sql;
		$li->IsColOff = "ReadingScheme_RecommendBookReport";
		
		$li->field_array = array("BookInfo","StudentName");
		$li->column_array = array(18,18);
		$li->wrap_array = array(0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Book'])."</th>\n";
		$li->column_list .= "<th width='25%'>".$Lang['ReadingGarden']['BookReport']."</th>\n";
		$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['Identity']['Student'])."</th>\n";
		$li->column_list .= "<th width='25%'>&nbsp;</th>";
		
		$li->no_col = $pos+3;
		
		$x .= $li->display("","common_table_list_v30 view_table_list_v30","view_table_bottom_v30");
		
		return $x;
	}
	/*************************************** Carlos Part End *******************************************/
	
	/**************************************** Start of Forum UI ***************************************************/
	function Get_Management_Forum_UI($Keyword='')
	{
		global $Lang;
		
		# Add Btn
		$NewBtn = $this->Get_Content_Tool_v30('new','edit_forum.php',$Lang['Btn']['New']);
		
		$x .= '<form id="form1" name="form1" method="post" action="forum.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $NewBtn."\n";
							$x .= '</div>'."\n";
							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="'.htmlspecialchars($Keyword).'"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'ForumID[]\',\'edit_forum.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'ForumID[]\',\'delete_forum.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="ForumTableDiv"></div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Management_Forum_Table($Keyword="")
	{
		global $Lang, $ReadingGardenLib;
		
		$ForumList = $ReadingGardenLib->Get_Management_Forum_List('',$Keyword);
		
		$x .= '<table class="common_table_list_v30" id="ForumTable">'."\n";
		# col group
			$x .= '<col align="left" style="width: 10px">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left" style="width: 5%;">'."\n";
			$x .= '<col align="left" style="width: 5%;">'."\n";
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 30px">'."\n";
			$x .= '<col align="left" style="width: 30px">'."\n";
	
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>#</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['ForumName'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['StudentCreateTopicRequireApprove'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['AllowAttachment'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['TargetClass'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['LastModifiedDate'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['LastModifiedBy'].'</th>'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
					$x .= '<th>'.$this->Get_Checkbox("CheckMaster", "", "", 0, '', '', 'Check_All_Forum(this.checked)').'</th>';
				$x .= '</tr>'."\n";	
			$x .= '</thead>'."\n";
		
			$x .= '<tbody>'."\n";
			if(sizeof($ForumList)==0){
				$x .= '<tr><td colspan="9" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			for($i=0;$i<sizeof($ForumList);$i++)
			{
				$ForumID = $ForumList[$i]['ForumID'];
				$ForumName = htmlspecialchars($ForumList[$i]['ForumName']);
				$AllowAttachment = $ForumList[$i]['AllowAttachment']==1?$Lang['General']['Yes']:$Lang['General']['No'];;
				$TargetClass = htmlspecialchars($ForumList[$i]['TargetClass']);
				$DisplayOrder = $ForumList[$i]['DisplayOrder'];
				$TopicNeedApprove = $ForumList[$i]['TopicNeedApprove']==1?$Lang['General']['Yes']:$Lang['General']['No'];
				$DateModified = $ForumList[$i]['DateModified'];
				$LastModifiedBy = $ForumList[$i]['LastModifiedBy'];
				$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['ReadingGarden']['MoveForum'].'"></a>';
				
				$x .= '<tr>'."\n";
				$x .= '<td>'.($i+1).'</td>'."\n";
				$x .= '<td><a href="./edit_forum.php?ForumID='.$ForumID.'" title="'.$Lang['Btn']['Edit'].'">'.$ForumName.'</a></td>'."\n";
				$x .= '<td>'.$TopicNeedApprove.'</td>'."\n";
				$x .= '<td>'.$AllowAttachment.'</td>'."\n";
				$x .= '<td>'.$TargetClass.'</td>'."\n";
				$x .= '<td>'.$DateModified.'</td>'."\n";
				$x .= '<td>'.$LastModifiedBy.'</td>'."\n";
				$x .= '<td class="Draggable">'."\n";
					$x .= '<span class="table_row_tool" style="width: 30px;">'.$MoveLink.'</span>'."\n";
					$x .= '<input type="hidden" name="RowForumID[]" class="RowForumID" value="'.$ForumID.'">'."\n";
				$x .= '</td>'."\n";
				$x .= '<td>'.$this->Get_Checkbox("", "ForumID[]", $ForumID, 0, '', '', '').'</td>'."\n";
				$x .= '</tr>'."\n";
			}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br />';
		
		return $x;	
	}
	
	function Get_Management_Forum_Edit_UI($ForumID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
		
		if(trim($ForumID)!='')
		{
			$AddEditNav = $Lang['Btn']['Edit'];
			$ForumArr = $ReadingGardenLib->Get_Class_Forum('',$ForumID);
			
			$ForumInfo = $ForumArr[0];
			$ClassIDArr = Get_Array_By_Key($ForumArr,"ClassID");
		}
		else
			$AddEditNav = $Lang['Btn']['New'];
		
		# Navigation
		$NavigationObj[] = array($Lang['ReadingGarden']['ForumMgmt']['MenuTitle'],"./forum.php");
		$NavigationObj[] = array($AddEditNav);
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj);
		
		#Class Selection
		include_once("libclass.php");
		$lclass = new libclass();
		$YearClassSelection = $lclass->getSelectClassID(" id='ClassID' name='ClassID[]' multiple size=10 class='Mandatory' ",$ClassIDArr,2);
		$YearClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID',true);");
		
		$x .= $Navigation;
		$x .= '<form id="form1" name="form1" action="forum_update.php" method="POST">'."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
				$x .= '<col class="field_title">';
				$x .= '<col class="field_c">';
		
		# Forum Name
				$ForumName = htmlspecialchars($ForumInfo['ForumName'],ENT_QUOTES);
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['ForumName'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="ForumName" id="ForumName" class="textbox_name Mandatory" value="'.$ForumName.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= '<div id="WarnForumName" class="WarnMsgDiv" style="display:none;">'."\n";
							$x .= '<span class="tabletextrequire">* </span><span class="tabletextrequire" id="WarnMsgForumName"></span>'."\n";
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
		
		# Description
				$Description = $ForumInfo['Description'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Description'].'</td>'."\n";
					$x .= '<td>'."\n";
					$x .= $this->GET_TEXTAREA("Description", $Description);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
		
		# New topic by student require approval
				$TopicNeedApprove = $ForumInfo['TopicNeedApprove'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['StudentCreateTopicRequireApprove'].'</td>'."\n";
					$x .= '<td>'."\n";
					$x .= $this->Get_Radio_Button("TopicNeedApproveYes", "TopicNeedApprove", 1, $TopicNeedApprove==1,"",$Lang['General']['Yes']);
					$x .= '&nbsp;'.$this->Get_Radio_Button("TopicNeedApproveNo", "TopicNeedApprove", 0, $TopicNeedApprove==0,"",$Lang['General']['No']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
		
		# Allow Attachment
				$AllowAttachment = $ForumInfo['AllowAttachment'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['AllowAttachment'].'</td>'."\n";
					$x .= '<td>'."\n";
					$x .= $this->Get_Radio_Button("AllowAttachmentYes", "AllowAttachment", 1, $AllowAttachment==1,"",$Lang['General']['Yes']);
					$x .= '&nbsp;'.$this->Get_Radio_Button("AllowAttachmentNo", "AllowAttachment", 0, $AllowAttachment==0,"",$Lang['General']['No']);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
		
		# Target Class
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Class'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $YearClassSelection."\n";
						$x .= $YearClassSelectAllBtn."\n";
						$x .= $this->Spacer()."\n";
						$x .= $this->MultiSelectionRemark()."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankClassID",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectClass'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="ForumID" id="ForumID" value="'.$ForumID.'">'."\n";
			$x .= '<input type="hidden" name="FormValid" id="FormValid" value="1">'."\n";
			
			# edit bottom
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","js_Check_ForumName();");
			$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./forum.php'");
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$CancelBtn;
			$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;	
		
	}
	
	function Get_Forum_UI($Keyword='',$ClassIDArr='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
		
		# Navigation
		$NavigationObj[] = array($Lang['Header']['Menu']['Home'],"../index.php");
		$NavigationObj[] = array($Lang['ReadingGarden']['Forum'],"");
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		$x .= $Navigation;
		$x .= '<form id="form1" name="form1" method="post" action="forum.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								
							$x .= '</div>'."\n";
							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="'.htmlspecialchars($Keyword,ENT_QUOTES).'"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="ForumTableDiv">'.$this->Get_Forum_Table_UI($Keyword,$ClassIDArr).'</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
						$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Forum_Table_UI($Keyword='',$ClassIDArr='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib,$image_path,$LAYOUT_SKIN, $clearCoo;
		
		if($ReadingGardenLib->isStudent){
			$StudentInfo = $ReadingGardenLib->Get_Student_ClassInfo($_SESSION['UserID']);
			$ClassIDArr = $StudentInfo[0]['YearClassID'];
		}
		
		$ForumList = $ReadingGardenLib->Get_Forum_Summary($Keyword,'',$ClassIDArr);
		$ForumListSize = sizeof($ForumList);
		
		$x .= '<table class="common_table_list_v30">'."\n";
		# table head
		$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th><img height="20" align="absmiddle" width="25" src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_forum.gif" />'.$Lang['ReadingGarden']['Forum'].'</th>'."\n";
			$x .= '</tr>'."\n";	
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
		if($ForumListSize==0){
			$x .= '<tr><td colspan="8" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
		}
		for($i=0;$i<$ForumListSize;$i++)
		{
			$ForumID = $ForumList[$i]['ForumID'];
			$ForumName = htmlspecialchars($ForumList[$i]['ForumName']);
			$Description = htmlspecialchars($ForumList[$i]['Description']);
			$TotalTopic = $ForumList[$i]['TotalTopic'];
			$TotalPost = $ForumList[$i]['TotalPost'];
			$LastTopicTime = trim($ForumList[$i]['LastTopicTime']);
			$LastPostTime = trim($ForumList[$i]['LastPostTime']);
			if($LastTopicTime=='') $LastTopicTime = $Lang['General']['EmptySymbol'];
			if($LastPostTime=='') $LastPostTime = $Lang['General']['EmptySymbol'];
			
			$x .= '<tr><td>'."\n";
			$x .= '<table class="common_table_list_v30" margin="10px" padding="10px">'."\n";
			$x .= '<tbody>'."\n";
				$x .= '<tr><td>'."\n";
					$x .= '<div><a href="./topic.php?ForumID='.$ForumID.'&clearCoo='.$clearCoo.'">'.$ForumName.'</a></div>'."\n";
					$x .= '<div margin="10px" padding="10px">'.$Description.'</div>';
					$x .= '<div>'."\n";
						$x .= '<span style="float:left">'."\n";
						$x .= '<img height="17" align="absmiddle" width="17" src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_chat.gif" /> '.$TotalTopic.' '.$Lang['ReadingGarden']['Topic(s)'].' '."\n";
						$x .= '<img height="17" align="absmiddle" width="17" src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_chat.gif" /> '.$TotalPost.' '.$Lang['ReadingGarden']['Post(s)'].' '."\n";
						$x .= '</span>'."\n";
						$x .= '<span style="float:right">'."\n";
						$x .= $Lang['ReadingGarden']['LastTopicOn'].': '.$LastTopicTime." | \n";
						$x .= $Lang['ReadingGarden']['LastPostOn'].': '.$LastPostTime."\n";
						$x .= '</span>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td></tr>'."\n";
			$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '</td></tr>'."\n";
		}
		$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Forum_Selection($ID_Name='', $SelectedForum='', $all=0, $noFirst=0, $FirstTitle="", $Other="", $ClassID ="")
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
		
		$ForumArr = $ReadingGardenLib->Get_Forum_Selection($ClassID);
		$ForumSelection = getSelectByArray($ForumArr, " id='$ID_Name' name='$ID_Name' $Other ",$SelectedForum, $all, $noFirst, $FirstTitle);
		
		return $ForumSelection;
	}
	
	function Get_Forum_Topic_Table_Tool_Layer($RecordStatus, $FieldName, $WithOnTopBtn=1)
	{
		global $Lang, $ReadingGardenLib;
		$ActionButtons = '<div class="common_table_tool">'."\n";
		if($ReadingGardenLib->isAdmin==1){
			
			if($RecordStatus==RECORD_STATUS_ACTIVE){
				// Mark as on top btn
				// Remove on top btn
				// Delete btn
				if($WithOnTopBtn==1){
					$ActionButtons .= '<a class="tool_totop" href="javascript:void(0);" onclick="js_Perform_Action(\'markontop\',\''.$FieldName.'\');">'.$Lang['ReadingGarden']['MarkAsOnTop'].'</a>'."\n";
					$ActionButtons .= '<a class="tool_undo" href="javascript:void(0);" onclick="js_Perform_Action(\'removeontop\',\''.$FieldName.'\');">'.$Lang['ReadingGarden']['RemoveOnTop'].'</a>'."\n";
				}
				$ActionButtons .= '<a class="tool_reject" href="javascript:void(0);" onclick="js_Perform_Action(\'reject\',\''.$FieldName.'\');">'.$Lang['ReadingGarden']['Reject'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_edit" href="javascript:void(0);" onclick="js_Perform_Action(\'edit\',\''.$FieldName.'\');">'.$Lang['Btn']['Edit'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_delete" href="javascript:void(0);" onclick="js_Perform_Action(\'delete\',\''.$FieldName.'\');">'.$Lang['Btn']['Delete'].'</a>'."\n";
			}else if($RecordStatus==RECORD_STATUS_WAITING_APPROVED){
				// Approve btn
				// Reject btn
				// Delete btn
				$ActionButtons .= '<a class="tool_approve" href="javascript:void(0);" onclick="js_Perform_Action(\'approve\',\''.$FieldName.'\');">'.$Lang['ReadingGarden']['Approve'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_reject" href="javascript:void(0);" onclick="js_Perform_Action(\'reject\',\''.$FieldName.'\');">'.$Lang['ReadingGarden']['Reject'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_edit" href="javascript:void(0);" onclick="js_Perform_Action(\'edit\',\''.$FieldName.'\');">'.$Lang['Btn']['Edit'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_delete" href="javascript:void(0);" onclick="js_Perform_Action(\'delete\',\''.$FieldName.'\');">'.$Lang['Btn']['Delete'].'</a>'."\n";
			}else if($RecordStatus==RECORD_STATUS_REJECTED){
				// Approve btn
				// Delete btn
				$ActionButtons .= '<a class="tool_approve" href="javascript:void(0);" onclick="js_Perform_Action(\'approve\',\''.$FieldName.'\');">'.$Lang['ReadingGarden']['Approve'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_edit" href="javascript:void(0);" onclick="js_Perform_Action(\'edit\',\''.$FieldName.'\');">'.$Lang['Btn']['Edit'].'</a>'."\n";
				$ActionButtons .= '<a class="tool_delete" href="javascript:void(0);" onclick="js_Perform_Action(\'delete\',\''.$FieldName.'\');">'.$Lang['Btn']['Delete'].'</a>'."\n";
			}
		}else{
			$ActionButtons .= '<a class="tool_edit" href="javascript:void(0);" onclick="js_Perform_Action(\'edit\',\''.$FieldName.'\');">'.$Lang['Btn']['Edit'].'</a>'."\n";
		}
		$ActionButtons .= '</div>'."\n";
		return $ActionButtons;
	}
	
	function Get_Forum_Topic_UI($ForumID, $Keyword='',$field='', $order='', $pageNo='', $numPerPage='',$RecordStatus='', $ClassID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
		
		$NewBtn = $this->Get_Content_Tool_v30('new','edit_topic.php?ForumID='.$ForumID,$Lang['ReadingGarden']['NewTopic']);
		
		$ForumSelection = $this->Get_Forum_Selection('ForumID', $ForumID,0,1,'',' onchange="js_Reload_Forum_Topic_Table();" ',$ClassID);
		
		# Navigation
		$NavigationObj[] = array($Lang['Header']['Menu']['Home'],"../index.php");
		$NavigationObj[] = array($Lang['ReadingGarden']['Forum'],"./forum.php");
		$NavigationObj[] = array($ForumSelection,"");
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		if($RecordStatus=='') $RecordStatus = RECORD_STATUS_ACTIVE; // Default show Active topics
		
		$RecordStatusArr=array();
		if($ReadingGardenLib->isAdmin==1){
			$RecordStatusArr[] = array(RECORD_STATUS_ACTIVE,$Lang['ReadingGarden']['Approved']);
			$RecordStatusArr[] = array(RECORD_STATUS_WAITING_APPROVED,$Lang['ReadingGarden']['WaitingForApproval']);
			$RecordStatusArr[] = array(RECORD_STATUS_REJECTED,$Lang['ReadingGarden']['Rejected']);
			$RecordStatusSelection = getSelectByArray($RecordStatusArr, " id='RecordStatus' name='RecordStatus' onchange='js_Reload_Forum_Topic_Table();' ",$RecordStatus, 0, 1);
			$RecordStatusFilter = $Lang['ReadingGarden']['SelectTitleArr']['Status'].":&nbsp;".$RecordStatusSelection;
		}else{
			$RecordStatusArr[] = array(RECORD_STATUS_ACTIVE,$Lang['ReadingGarden']['Approved']);
			$RecordStatusSelection = getSelectByArray($RecordStatusArr, " id='RecordStatus' name='RecordStatus' style='display:none' ",$RecordStatus, 0, 1);
			$RecordStatusFilter = $RecordStatusSelection;
		}
		
		
		$x .= '<form id="form1" name="form1" method="post" action="topic.php" onsubmit="js_Reload_Forum_Topic_Table();return false;">'."\n";
			$x .= $Navigation;
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $NewBtn."\n";
							$x .= '</div>'."\n";
							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="'.htmlspecialchars($Keyword,ENT_QUOTES).'"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
										$x .= '<div class="table_filter">'."\n";
//											$x .= $Lang['ReadingGarden']['Forum'].':&nbsp;'.$ForumSelection."\n";
											$x .= $RecordStatusFilter."\n";
										$x .= '</div>'."\n";
								$x .= '</td>'."\n";
								
								$x .= '<td valign="bottom" id="TableToolTd">'."\n";
									// action buttons here	
									$x .= $this->Get_Forum_Topic_Table_Tool_Layer($RecordStatus, "ForumTopicID[]", ($ReadingGardenLib->isAdmin==1?1:0));
								$x .= '</td>'."\n";
								
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="ForumTopicTableDiv"></div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Forum_Topic_DBTable($ForumID, $Keyword='',$field='', $order='', $pageNo='', $numPerPage='',$RecordStatus='')
	{
		global $Lang, $page_size, $ck_forum_topic_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_forum_topic_page_size) && $ck_forum_topic_page_size != "") $page_size = $ck_forum_topic_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$x = '';
		$sql = $ReadingGardenLib->Get_Forum_Topic_DBTable_Sql($ForumID,$Keyword,$RecordStatus);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->count_mode=1;
		
		$li->field_array = array("TopicSubject", "DateInput", "NumOfReply", "CreatedBy", "LastReplyBy", "LastPostTime");
		$li->column_array = array(18,18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0,0);
		
		$pos = 0;
		$extraCols = 1;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Topic'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['CreateDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['CreatedBy'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['NumberOfReply'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastReplyBy'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastPostTime'])."</th>\n";
		//if($ReadingGardenLib->isAdmin==1){
			$li->column_list .= "<th width='1'>".$li->check("ForumTopicID[]")."</td>\n";
			$extraCols++;
		//}
		$li->no_col = $pos+$extraCols;
		
		//$x .= $li->display();
		
		$sql = $li->built_sql();
		$sql = str_ireplace("ORDER BY","ORDER BY t.OnTop DESC,",$sql);
		$i = 0;
		$li->rs = $li->db_db_query($sql);
		$width = ($width!="") ? $width : 560;
		$n_start = $li->n_start;
		$x = '';
		$x .= "<table class='common_table_list_v30'>\n<thead>\n";
		$x .= $li->displayColumn(); 
		
		if ($li->db_num_rows()==0){
				$x .= "<tr><td class=tableContent align=center colspan=".$li->no_col."><br>".$li->no_msg."<br><br></td></tr>\n";
		} else {
			while($row = $li->db_fetch_array()) {
				$i++;
				$css = ($i%2) ? "" : "2";
				if($i>$li->page_size) break;
				
				$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
											
				$x .= "<tr ".$trClass.">\n";
				$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
				for($j=0; $j<$li->no_col-1; $j++)
				$x .= $li->displayCell($j,$row[$j], $css);
				$x .= "</tr>\n";
			}
		}
		$x .= "</thead></table>";
		if($li->db_num_rows()<>0 && $li->no_navigation_row!=true) $x .= $li->navigation_IP25("","");
		$li->db_free_result();
		
		return $x;
	}
	
	function Get_Forum_Topic_Edit_UI($ForumID,$ForumTopicID,$FromPost=0)
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
		
		$ForumInfo = $ReadingGardenLib->Get_Class_Forum('', $ForumID);
		if(trim($ForumTopicID)!=''){
			$AddEditNav = $Lang['Btn']['Edit'];
			$TopicArr = $ReadingGardenLib->Get_Forum_Topic($ForumID,$ForumTopicID);
			$ForumTopicInfo = $TopicArr[0];
		}else{
			$AddEditNav = $Lang['Btn']['New'];
			if($_SESSION['UserType']==2){// Student
				if($ForumInfo[0]['TopicNeedApprove']==1)
					$ApproveRemindText = '<span>'.$Lang['ReadingGarden']['WarningMsg']['NewTopicNeedApprove'].'</span><p class="spacer"></p>';
			}
		}
		
		$AllowAttachment = $ForumInfo[0]['AllowAttachment'];
		
		# Navigation
		$NavigationObj[] = array($Lang['Header']['Menu']['Home'],"../index.php");
		$NavigationObj[] = array($Lang['ReadingGarden']['Forum'],"./forum.php");
		$NavigationObj[] = array($Lang['ReadingGarden']['Topic'],"./topic.php?ForumID=$ForumID");
		$NavigationObj[] = array($AddEditNav);
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		$x .= $Navigation;
		$x .= '<form id="form1" name="form1" action="topic_update.php" method="POST" onsubmit="js_Check_Form();return false">'."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
				$x .= '<col class="field_title">';
				$x .= '<col class="field_c">';
				
				# Topic Subject
				$TopicSubject = htmlspecialchars($ForumTopicInfo['TopicSubject'],ENT_QUOTES);
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Topic'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="TopicSubject" id="TopicSubject" class="textbox_name Mandatory" value="'.$TopicSubject.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnTopicSubject",$Lang['ReadingGarden']['WarningMsg']['PleaseInputTopic'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Message Content
				$Message = $ForumTopicInfo['Message'];
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Content'].'</td>'."\n";
					$x .= '<td>'."\n";
						# Components size
						$msg_box_width = "100%";
						$msg_box_height = 350;
						$obj_name = "Message";
						$editor_width = $msg_box_width;
						$editor_height = $msg_box_height;
						
						include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
						$oFCKeditor = new FCKeditor('Message',$editor_width,$editor_height);
						//debug($Message);
						$oFCKeditor->Value = $Message;
						$x .= $oFCKeditor->Create2();
						$x .= $this->Get_Form_Warning_Msg("WarnMessage",$Lang['ReadingGarden']['WarningMsg']['PleaseInputContent'], "WarnMsgDiv");
									
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";

				# Attachment
				if($_SESSION['UserType']!=2 || $AllowAttachment)
				{
					$Attachment = $ForumTopicInfo['Attachment'];
					if(!$Attachment)
					{
						$foldername = session_id().time();
						$Attachment = $ReadingGardenLib->ForumAttachmentPath."/".$foldername;
					}
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= $this->Get_Upload_Attachment_UI("form1", array("AttachmentArr","AttachmentArr[]"), $Attachment)."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";	
				}
				
				$x .= '</table>'."\n";
				
				$x .= '<input type="hidden" id="ForumID" name="ForumID" value="'.$ForumID.'" />'."\n";
				$x .= '<input type="hidden" id="ForumTopicID" name="ForumTopicID" value="'.$ForumTopicID.'" />'."\n";
				$x .= '<input type="hidden" id="FromPost" name="FromPost" value="'.$FromPost.'" />'."\n";
				$x .= '<input type="hidden" id="Attachment" name="Attachment" value="'.$Attachment.'" />'."\n";
				# edit bottom
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","");
			if($FromPost==1 && $ForumTopicID != '')
				$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID'");
			else
				$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./topic.php?ForumID=$ForumID'");
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $ApproveRemindText;
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$CancelBtn;
			$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;	
	}
	
	function Get_Forum_Post_UI($ForumID,$ForumTopicID,$field='',$order='',$pageNo='',$numPerPage='',$Keyword='',$RecordStatus='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib, $ck_forum_topic_page_order, $ck_forum_topic_page_field;
		
		$NewBtn = $this->Get_Content_Tool_v30('new','edit_post.php?ForumID='.$ForumID.'&ForumTopicID='.$ForumTopicID,$Lang['ReadingGarden']['NewPost']);
		
		$TopicInfo = $ReadingGardenLib->Get_Forum_Topic($ForumID,$ForumTopicID,true);
		$ForumSummary = $ReadingGardenLib->Get_Forum_Summary('',$ForumID);
		
		# Navigation
		$NavigationObj[] = array($Lang['Header']['Menu']['Home'],"../index.php");
		$NavigationObj[] = array($Lang['ReadingGarden']['Forum'],"./forum.php");
		$NavigationObj[] = array($ForumSummary[0]['ForumName'],"./topic.php?ForumID=$ForumID");
		$NavigationObj[] = array($TopicInfo[0]['TopicSubject'],"");
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		if($RecordStatus=='') $RecordStatus = RECORD_STATUS_ACTIVE; // Default show Active posts
		/*
		$RecordStatusArr=array();
		if($ReadingGardenLib->isAdmin==1){
			$RecordStatusArr[] = array(RECORD_STATUS_ACTIVE,$Lang['ReadingGarden']['Approved']);
			$RecordStatusArr[] = array(RECORD_STATUS_WAITING_APPROVED,$Lang['ReadingGarden']['WaitingForApproval']);
			$RecordStatusArr[] = array(RECORD_STATUS_REJECTED,$Lang['ReadingGarden']['Rejected']);
			$RecordStatusSelection = getSelectByArray($RecordStatusArr, " id='RecordStatus' name='RecordStatus' onchange='js_Reload_Forum_Post_Table();' ",$RecordStatus, 0, 1);
			$RecordStatusFilter = '<div class="table_filter">'."\n";
			$RecordStatusFilter .= $Lang['ReadingGarden']['SelectTitleArr']['Status'].":&nbsp;".$RecordStatusSelection."\n";
			$RecordStatusFilter .= '</div>'."\n";
		}else{
			$RecordStatusArr[] = array(RECORD_STATUS_ACTIVE,$Lang['ReadingGarden']['Approved']);
			$RecordStatusSelection = getSelectByArray($RecordStatusArr, " id='RecordStatus' name='RecordStatus' style='display:none' ",$RecordStatus, 0, 1);
			$RecordStatusFilter = $RecordStatusSelection;
		}
		*/
		# Get Previous & Next Topic ID
		$ConditionArr['topic_field']=$ck_forum_topic_page_field;
		$ConditionArr['topic_order']=$ck_forum_topic_page_order;
		$PrevNextTopicID = $ReadingGardenLib->Get_Forum_Prev_Next_Topic($ForumID,$ForumTopicID,$ConditionArr);
		$PrevNextBtns = array();
		if(trim($PrevNextTopicID['PrevTopicID'])!=''){
			$PrevTopicBtn = $this->GET_ACTION_BTN($Lang['ReadingGarden']['PreviousTopic'],"button","window.location='./post.php?ForumID=$ForumID&ForumTopicID=".$PrevNextTopicID['PrevTopicID']."'");
			$PrevNextBtns[] = $PrevTopicBtn;
		}
		if(trim($PrevNextTopicID['NextTopicID'])!=''){
			$NextTopicBtn = $this->GET_ACTION_BTN($Lang['ReadingGarden']['NextTopic'],"button","window.location='./post.php?ForumID=$ForumID&ForumTopicID=".$PrevNextTopicID['NextTopicID']."'");
			$PrevNextBtns[] = $NextTopicBtn;
		}
		
		$x .= $Navigation;
		$x .= '<form id="form1" name="form1" method="post" action="post.php" onsubmit="js_Reload_Forum_Post_Table();return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $NewBtn."\n";
							$x .= '</div>'."\n";
							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="'.htmlspecialchars($Keyword,ENT_QUOTES).'"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						
						# Topic 
						$x .= '<div id="ForumTopicTableDiv">'."\n";
						$x .= '</div>'."\n";
						
						$x .= '<br />';
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
										//$x .= '<div class="table_filter">'."\n";
											//$x .= $Lang['ReadingGarden']['Forum'].':&nbsp;'.$ForumSelection."\n";
										//	$x .= $RecordStatusFilter."\n";
										//$x .= '</div>'."\n";
								$x .= '</td>'."\n";
								
								$x .= '<td valign="bottom" id="TableToolTd">'."\n";
									// action buttons here
								if($ReadingGardenLib->isAdmin==1){
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_delete" href="javascript:void(0);" onclick="js_Perform_Action(\'delete\',\'ForumPostID[]\');">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>';
								}
								$x .= '</td>'."\n";
								
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="ForumPostTableDiv"></div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';
			$x .= '<input type="hidden" id="ForumID" name="ForumID" value="'.$ForumID.'" />';
			$x .= '<input type="hidden" id="ForumTopicID" name="ForumTopicID" value="'.$ForumTopicID.'" />';
			$x .= '<input type="hidden" id="FromPost" name="FromPost" value="1" />';
		$x .= '<div class="edit_bottom_v30">'."\n";
		if(sizeof($PrevNextBtns)>0)
			$x .= implode('&nbsp;',$PrevNextBtns);
		$x .= '</div>'."\n";
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Forum_Post_Entry($DataArr)
	{
		global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $ReadingGardenLib;
		
		$PhotoLink = $DataArr['PhotoLink'];
		$CreatorName = $DataArr['CreatorName'];
		$Subject = $DataArr['Subject'];
		$Message = $DataArr['Message'];
		$Attachment = $DataArr['Attachment'];
		$CreateTime = $DataArr['CreateTime'];
		$ModifyTime = $DataArr['ModifyTime'];
		$LastModifyName = $DataArr['LastModifyName'];
		$DisplayNumber = $DataArr['DisplayNumber'];
		$CheckBtn = $DataArr['CheckBtn'];
		if($DataArr['ForumTopicID'])
		{
			$FuncID = $DataArr['ForumTopicID'];
			$FuncName = FUNCTION_NAME_TOPIC;
		}
		else
		{
			$FuncID = $DataArr['ForumPostID'];
			$FuncName = FUNCTION_NAME_POST;
		}
		if($DisplayNumber!='') $DisplayNumber = '<strong>#'.$DisplayNumber.'</strong>&nbsp;'.$CheckBtn;
		
		$Btns=array();
		$Btns[] = $DataArr['EditBtn'];
		$Btns[] = $DataArr['DeleteBtn'];
		$Btns[] = $DataArr['QuoteBtn'];
		$Btns[] = $DataArr['ReplyBtn'];
		$AllBtns = implode("&nbsp;",$Btns);
		
		if(trim($PhotoLink)==''){
			$PhotoPath = $image_path.'/myaccount_personalinfo/samplephoto.gif';
		}else{
			$PhotoPath = $PhotoLink;
		}
		
		if($ModifyTime != $CreateTime)
			$LastModifyInfo = $Lang['ReadingGarden']['LastModifiedDate'].': '.$ModifyTime.' by '.$LastModifyName;
		
		if($Attachment)
		{
			$AttachmentList = Get_FileList_By_FolderPath($Attachment);
			for($i=0; $i<sizeof($AttachmentList); $i++)
			{
				list($FileName, $FileSize) = $AttachmentList[$i]; 
				$FileLink = Get_Attachment_Download_Link($Attachment, $FileName);
				$FileLinkArr[] = '<a href="'.$FileLink.'">'.$this->Get_Attachment_Icon().$FileName.'</a>'; 
			}
			$FileLinkStr = '<div>'.$Lang['ReadingGarden']['Attachment'].': '.implode(", ",(array)$FileLinkArr).'</div>'."\n";
		}
		
		$x .= '<table class="table_plain" cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
		$x .= '<tbody>'."\n";
			$x .= '<tr>'."\n";
			
				$x .= '<td width="10%" valign="top">
							<div id="photo_border_nolink" style="vertical-align:top;align:center;">
								<span><img height="130" border="0" width="100" src="'.$PhotoPath.'"></span>
							</div>
							<span class="share_file_title" style="text-align:center;"><strong>'.$CreatorName.'</strong></span>
						</td>';
				$x .= '<td width="90%">'."\n";
				
					$x .= '<table class="common_table_list_v30" cellspacing="0" cellpadding="3" border="0" width="100%" style="height:100%;">
		             		<tbody>
								<tr>
		                			<td align="center" style="border-bottom:1px solid #CCCCCC">
										<span style="float:left;">'.$Subject.'</span>
										<span style="float:right;">'.$Lang['ReadingGarden']['PostedOn'].': '.$CreateTime.'&nbsp;'.$DisplayNumber.'</span>
									</td>
		              			</tr>
		              			<tr>
		                			<td valign="top">';
		                			# message
		                			$x .= '<table class="table_plain" cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr><td>';
										$x .= '<div class="DivPostMessage" style="min-height:150px">'.$Message.'</div>';
							 		$x .= '</td></tr></tbody></table>';
							 		#Attachment
							 		$x .= $FileLinkStr;
							 		
							 		#Like
							 		$x .= $this->Get_Like_Comment_Div($FuncName, $FuncID, ' ');
							 $x .= '</td>
		              			</tr>
								<tr>
		                			<td align="center" valign="bottom" style="border-top:1px solid #CCCCCC">
										<span style="float:left;">'.$LastModifyInfo.'</span>
										<span style="float:right;">'.$AllBtns.' <a href="javascript:Scroll_To_Top();" title="'.$Lang['ReadingGarden']['GoToTop'].'">TOP</a></span>
									</td>
		              			</tr>
		          			</tbody>
							</table>'."\n";
							
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Forum_Topic_Entry($ForumID,$ForumTopicID)
	{
		global $Lang, $ReadingGardenLib;
		$TopicInfo = $ReadingGardenLib->Get_Forum_Topic($ForumID,$ForumTopicID,true);
		$DataArr['PhotoLink'] = $TopicInfo[0]['PersonalPhotoLink'];
		$DataArr['CreatorName'] = $TopicInfo[0]['CreatorName'];
		$DataArr['Message'] = $TopicInfo[0]['Message'];
		$DataArr['Attachment'] = $TopicInfo[0]['Attachment'];
		$DataArr['Subject'] = $TopicInfo[0]['TopicSubject'];
		$DataArr['CreateTime'] = $TopicInfo[0]['DateInput'];
		$DataArr['ModifyTime'] = $TopicInfo[0]['DateModified'];
		$DataArr['LastModifyName'] = $TopicInfo[0]['LastModifyName'];
		$DataArr['RecordStatus'] = $TopicInfo[0]['RecordStatus'];
		$DataArr['ForumTopicID'] = $ForumTopicID;
		if($ReadingGardenLib->isAdmin==1 || $_SESSION['UserID']==$TopicInfo['UserID']){
			$DataArr['EditBtn'] = "<span><a href='./edit_topic.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID&FromPost=1'>".$Lang['Btn']['Edit']."</a></span>";
		}
		if($ReadingGardenLib->isAdmin==1){
			$DataArr['DeleteBtn'] = "<span><a href='javascript:js_Delete_Topic(\"".$ForumTopicID."\");'>".$Lang['Btn']['Delete']."</a></span>";
		}
		$DataArr['ReplyBtn'] = "<span><a href='./edit_post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID&ReplyTopicID=$ForumTopicID'>".$Lang['ReadingGarden']['Reply']."</a></span>";
	
		$x = '<table class="common_table_list_v30" cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th><span style="float:left;">'.$Lang['ReadingGarden']['Topic'].'</span>';
					if($ReadingGardenLib->isAdmin==1){
						if($DataArr['RecordStatus']==RECORD_STATUS_ACTIVE){
							$TopicStatus = $Lang['ReadingGarden']['Approved'];
							$ActionButtons .= '<a href="javascript:void(0);" onclick="js_Change_Topic_Status(\'reject\',\''.$TopicInfo[0]['ForumTopicID'].'\');">['.$Lang['ReadingGarden']['Reject'].']</a>'."\n";
						}else if($DataArr['RecordStatus']==RECORD_STATUS_WAITING_APPROVED){
							$TopicStatus = $Lang['ReadingGarden']['WaitingForApproval'];
							$ActionButtons = '<a href="javascript:void(0);" onclick="js_Change_Topic_Status(\'approve\',\''.$TopicInfo[0]['ForumTopicID'].'\');">['.$Lang['ReadingGarden']['Approve'].']</a>'."\n";
							$ActionButtons .= '<a href="javascript:void(0);" onclick="js_Change_Topic_Status(\'reject\',\''.$TopicInfo[0]['ForumTopicID'].'\');">['.$Lang['ReadingGarden']['Reject'].']</a>'."\n";
						}else if($DataArr['RecordStatus']==RECORD_STATUS_REJECTED){
							$TopicStatus = $Lang['ReadingGarden']['Rejected'];
							$ActionButtons = '<a href="javascript:void(0);" onclick="js_Change_Topic_Status(\'approve\',\''.$TopicInfo[0]['ForumTopicID'].'\');">['.$Lang['ReadingGarden']['Approve'].']</a>'."\n";
						}
						$x .= '<span style="float:right;">'.$TopicStatus.'&nbsp;'.$ActionButtons.'</span>';
					}
					$x .= '</th>'."\n";
				$x .= '</tr>'."\n";	
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_Forum_Post_Entry($DataArr);
				$x .= '</td>'."\n";		
			$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Forum_Post_DBTable($ForumID,$ForumTopicID,$field='',$order='',$pageNo='',$numPerPage='',$Keyword='',$RecordStatus='')
	{
		global $Lang, $page_size, $ck_forum_post_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_forum_post_page_size) && $ck_forum_post_page_size != "") $page_size = $ck_forum_post_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Forum_Topic_Post_DBTable_Sql($ForumTopicID,$Keyword,$RecordStatus);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("p.DateInput");
		$li->column_array = array(18,18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0,0);
		
		$i = 0;
		$li->rs = $li->db_db_query($li->built_sql());
		$width = ($width!="") ? $width : 560;
		$n_start = $li->n_start;
		$x = '';
		$x .= "<table class='common_table_list_v30'>\n<thead>\n";
		# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th><span style="float:left;">'.$Lang['ReadingGarden']['Post'].'</span>';
					if($ReadingGardenLib->isAdmin==1)
						$x .= '<span style="float:right">'.$this->Get_Checkbox("CheckMaster", "", "", 0, '', '', '(this.checked)?setChecked(1,this.form,\'ForumPostID[]\'):setChecked(0,this.form,\'ForumPostID[]\')').'</span>';
					$x .= '</th>'."\n";
				$x .= '</tr>'."\n";	
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
		
		if($li->db_num_rows()<>0 && $li->no_navigation_row!=true) $x .= $li->navigation_IP25("","");
		if ($li->db_num_rows()==0){
				$x .= "<tr><td class=tableContent align=center colspan=".$li->no_col."><br>".$Lang['ReadingGarden']['ThereIsNoReplyAtTheMoment']."<br><br></td></tr>\n";
		} else {
			while($row = $li->db_fetch_array()) {
				$i++;
				$css = ($i%2) ? "" : "2";
				if($i>$li->page_size) break;
				
				$trClass = (isset($row['trCustClass']) && $row['trCustClass'] != '') ? 'class="'.$row['trCustClass'].'"' : '';
				
				$DataArr = array();
				$DataArr['PhotoLink'] = $row['PersonalPhotoLink'];
				$DataArr['CreatorName'] = $row['CreatorName'];
				$DataArr['Subject'] = $row['PostSubject'];
				$DataArr['Message'] = $row['Message'];
				$DataArr['Attachment'] = $row['Attachment'];
				$DataArr['CreateTime'] = $row['DateInput'];
				$DataArr['ModifyTime'] = $row['DateModified'];
				$DataArr['LastModifyName'] = $row['LastModifiedName'];
				$DataArr['DisplayNumber'] = $i;
				$DataArr['ForumPostID'] = $row['ForumPostID'];
				
				if($ReadingGardenLib->isAdmin==1 || $_SESSION['UserID']==$row['UserID']){
					$DataArr['EditBtn'] = "<span><a href='./edit_post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID&ForumPostID=".$row['ForumPostID']."'>".$Lang['Btn']['Edit']."</a></span>";
				}
				if($ReadingGardenLib->isAdmin==1){
					$DataArr['DeleteBtn'] = "<span><a href='javascript:js_Delete_Post(\"".$row['ForumPostID']."\");'>".$Lang['Btn']['Delete']."</a></span>";
					$DataArr['CheckBtn'] = $this->Get_Checkbox("", "ForumPostID[]", $row['ForumPostID'], 0, '', '', '');
				}
				$DataArr['ReplyBtn'] = "<span><a href='./edit_post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID&ReplyPostID=".$row['ForumPostID']."'>".$Lang['ReadingGarden']['Reply']."</a></span>";
				
				$x .= "<tr ".$trClass.">\n";
				$x .= "<td>";
				$x .= $this->Get_Forum_Post_Entry($DataArr);
				$x .= "</td>";
				$x .= "</tr>\n";
			}
		}
		$x .= "</tbody></table>";
		if($li->db_num_rows()<>0 && $li->no_navigation_row!=true) $x .= $li->navigation_IP25("","");
		$li->db_free_result();
		
		return $x;
	}
	
	function Get_Forum_Post_Edit_UI($ForumID,$ForumTopicID,$ForumPostID='',$ReplyPostID='',$ReplyTopicID='')
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib, $userBrowser;
		
		if($ForumPostID!=''){
			$AddEditNav = $Lang['Btn']['Edit'];
			$PostArr = $ReadingGardenLib->Get_Forum_Topic_Post($ForumTopicID,$ForumPostID);
			$PostSubject = htmlspecialchars($PostArr[0]['PostSubject']);
			$Message = $PostArr[0]['Message'];
			$Attachment = $PostArr[0]['Attachment'];

		}else if($ReplyPostID!=''){
			$AddEditNav = $Lang['Btn']['New'];
			$PostArr = $ReadingGardenLib->Get_Forum_Topic_Post($ForumTopicID,$ReplyPostID);
			$PostSubject = $Lang['ReadingGarden']['Reply:'].htmlspecialchars($PostArr[0]['PostSubject']);
			$Attachment = $PostArr[0]['Attachment'];
			//if($_SESSION['UserType']==2)
			//	$ApproveRemindText = '<span>'.$Lang['ReadingGarden']['WarningMsg']['NewPostNeedApprove'].'</span><p class="spacer"></p>';
		}else if($ReplyTopicID!=''){
			$AddEditNav = $Lang['Btn']['New'];
			$PostArr = $ReadingGardenLib->Get_Forum_Topic($ForumID,$ForumTopicID);
			$Message = $PostArr[0]['Message'];
			$PostSubject = $Lang['ReadingGarden']['Reply:'].htmlspecialchars($PostArr[0]['TopicSubject']);
			$Attachment = $PostArr[0]['Attachment'];
			//if($_SESSION['UserType']==2)
			//	$ApproveRemindText = '<span>'.$Lang['ReadingGarden']['WarningMsg']['NewPostNeedApprove'].'</span><p class="spacer"></p>';
		}else{
			$AddEditNav = $Lang['Btn']['New'];
			//if($_SESSION['UserType']==2)
			//	$ApproveRemindText = '<span>'.$Lang['ReadingGarden']['WarningMsg']['NewPostNeedApprove'].'</span><p class="spacer"></p>';
		}

		if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
		{
			if ($Message!=strip_tags($Message))
			{
				$Message = strip_tags(convert_line_breaks($Message));
			}
			$Message = "\n\n\n> ".str_replace("\n", "\n> ", trim($Message));
		}

		$TopicInfo = $ReadingGardenLib->Get_Forum_Topic($ForumID,$ForumTopicID,true);
		$ForumSummary = $ReadingGardenLib->Get_Forum_Summary('',$ForumID);
		
		# allow attachment
		$AllowAttachment = $ForumSummary[0]['AllowAttachment'];
		
		# Navigation
		$NavigationObj[] = array($Lang['Header']['Menu']['Home'],"../index.php");
		$NavigationObj[] = array($Lang['ReadingGarden']['Forum'],"./forum.php");
		$NavigationObj[] = array($ForumSummary[0]['ForumName'],"./topic.php?ForumID=$ForumID");
		$NavigationObj[] = array($TopicInfo[0]['TopicSubject'],"./post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID");
		$NavigationObj[] = array($AddEditNav);
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		$x .= $Navigation;
		$x .= '<form id="form1" name="form1" action="post_update.php" method="POST" onsubmit="js_Check_Form();return false;">'."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30 form_table_thickbox">'."\n";
				$x .= '<col class="field_title">';
				$x .= '<col class="field_c">';
				
				# Post Subject
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['PostSubject'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<input name="PostSubject" id="PostSubject" class="textbox_name Mandatory" value="'.$PostSubject.'" maxlength="'.$this->MaxLength.'">'."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnPostSubject",$Lang['ReadingGarden']['WarningMsg']['PleaseInputTopic'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Message Content
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['Content'].'</td>'."\n";
					$x .= '<td>'."\n";
						# Components size
						$msg_box_width = "100%";
						$msg_box_height = 350;
						$obj_name = "Message";
						$editor_width = $msg_box_width;
						$editor_height = $msg_box_height;
						
						include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
						$oFCKeditor = new FCKeditor('Message',$editor_width,$editor_height);
						$oFCKeditor->Value = $Message;
						$x .= $oFCKeditor->Create2();
						$x .= $this->Get_Form_Warning_Msg("WarnMessage",$Lang['ReadingGarden']['WarningMsg']['PleaseInputContent'], "WarnMsgDiv");
									
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				# Attachment
				if($_SESSION['UserType']!=2 || $AllowAttachment)
				{
					if(!$Attachment)
					{
						$foldername = session_id().time();
						$Attachment = $ReadingGardenLib->ForumAttachmentPath."/".$foldername;
					}
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Attachment'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= $this->Get_Upload_Attachment_UI("form1", array("AttachmentArr","AttachmentArr[]"), $Attachment)."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";	
				}
				$x .= '</table>'."\n";
				
				$x .= '<input type="hidden" id="ForumID" name="ForumID" value="'.$ForumID.'" />'."\n";
				$x .= '<input type="hidden" id="ForumTopicID" name="ForumTopicID" value="'.$ForumTopicID.'" />'."\n";
				$x .= '<input type="hidden" id="ForumPostID" name="ForumPostID" value="'.$ForumPostID.'" />'."\n";
				$x .= '<input type="hidden" id="ReplyPostID" name="ReplyPostID" value="'.$ReplyPostID.'" />'."\n";
				$x .= '<input type="hidden" id="ReplyTopicID" name="ReplyTopicID" value="'.$ReplyTopicID.'" />'."\n";
				$x .= '<input type="hidden" id="Attachment" name="Attachment" value="'.$Attachment.'" />'."\n";
				# edit bottom
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","");
			$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='./post.php?ForumID=$ForumID&ForumTopicID=$ForumTopicID'");
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $ApproveRemindText;
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$CancelBtn;
			$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;	
	}
	/*********************************************** End of Forum UI ***********************************************/

	function Get_Settings_Report_Comment_Bank_UI($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_comment_bank_page_size, $ReadingGardenLib;
		
		$ThickBoxHeight = 350;
		$ThickBoxWidth = 550;
		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['Btn']['New'], "Get_Comment_Bank_Form('','')", $InlineID="FakeLayer", $Lang['Btn']['New']);
		
		
		$x .= '<form id="form1" name="form1" method="post" action="index.php" onsubmit="Get_Comment_Bank_Table(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $ThickBoxNewLink."\n";
										$x .= '</div>'."\n";
										$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<div class="table_filter">'."\n";
										
									$x .= '</div>'."\n";
									$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td valign="bottom">'."\n";
											$x .= '<div class="common_table_tool">'."\n";
								        	$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'CommentID[]\',\'Get_Comment_Bank_Form(-1, false);\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
											$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'CommentID[]\',\'remove_comment.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
											$x .= '</div>'."\n";
										$x .= '</td>'."\n";		
									$x .= '</tr>'."\n";
									$x .= '</table>'."\n"; 		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DivCommentBankTable">'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Report_Comment_Bank_DBTable($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_comment_bank_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_comment_bank_page_size) && $ck_comment_bank_page_size != "") $page_size = $ck_comment_bank_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Settings_Get_Comment_Bank_DBTable_Sql($Keyword);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("Comment", "DateModified", "LastModifiedBy");
		$li->column_array = array(18,18,18,18);
		$li->wrap_array = array(0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['TeacherComment'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedBy'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("CommentID[]")."</td>\n";
		$li->no_col = $pos+2;
		
		$x .= $li->display();
		
		return $x;
	}
	
	function Get_Settings_Report_Comment_Bank_Edit_Form($CommentID='')
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		
		if($CommentID!=''){
			$CommentInfo = $ReadingGardenLib->Get_Comment_Bank_Comments($CommentID);
		}
		
		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","isAddMore=0;");
		$SubmitAndContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'],"Button","isAddMore=1; CheckTBForm();");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");
		
		$x .= '';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form id="TBForm" name="TBForm" onsubmit="CheckTBForm(); return false;">'."\n";
			$x .= '<table width="98%" cellpadding=10px cellspacing=10px align="center" height="150px">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							
							# Comment
							$thisValue = intranet_htmlspecialchars($CommentInfo[0]["Comment"]);
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ReadingGarden']['TeacherComment'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="Comment" id="Comment" class="textbox_name Mandatory" value="'.$thisValue.'" maxlength="'.$this->MaxLength.'">'."\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnComment",$Lang['ReadingGarden']['WarningMsg']['RequestInputComment'], "WarnMsgDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";

						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
			if(!($CommentID > 0))
				$x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
			# hidden field
			$x .= '<input type="hidden" name="CommentID" id="CommentID" value="'.$CommentID.'">'."\n";
		
		$x .= '</form>';	
			
		$x .= $this->FOCUS_ON_LOAD("TBForm.Comment");
		
		return $x;
	}
	
	function Get_Settings_Report_Sticker_UI($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_sticker_page_size, $ReadingGardenLib;
		
		$ThickBoxHeight = 400;
		$ThickBoxWidth = 550;
		$ThickBoxNewLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'new', $Lang['Btn']['New'], "Get_Sticker_Form('','')", $InlineID="FakeLayer", $Lang['Btn']['New']);
		
		$x .= '<form id="form1" name="form1" method="post" action="index.php" onsubmit="Get_Sticker_Table(); return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						# Main Table  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div class="content_top_tool">'."\n";
										$x .= '<div class="Conntent_tool">'."\n";
											$x .= $ThickBoxNewLink."\n";
										$x .= '</div>'."\n";
										//$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
										$x .= '<br style="clear: both;">'."\n";
									$x .= '</div>'."\n";
									$x .= '<div class="table_filter">'."\n";
									
									$x .= '</div>'."\n";
									$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td valign="bottom">'."\n";
											$x .= '<div class="common_table_tool">'."\n";
								        	$x .= '<a class="tool_edit" href="javascript:checkEdit2(document.form1,\'StickerID[]\',\'Get_Sticker_Form(-1, false);\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
											$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'StickerID[]\',\'remove_sticker.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
											$x .= '</div>'."\n";
										$x .= '</td>'."\n";		
									$x .= '</tr>'."\n";
									$x .= '</table>'."\n"; 		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DivStickerTable">'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';	
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Settings_Report_Sticker_DBTable($field='', $order='', $pageNo='', $numPerPage='', $Keyword='')
	{
		global $Lang, $page_size, $ck_sticker_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 1 : $field;
		$order = ($order==='')? 1 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;
		
		if (isset($ck_sticker_page_size) && $ck_sticker_page_size != "") $page_size = $ck_sticker_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Settings_Get_Report_Sticker_DBTable_Sql($Keyword);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$li->field_array = array("ImagePath", "DateModified", "LastModifiedBy");
		$li->column_array = array(18,18,18,18);
		$li->wrap_array = array(0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['Sticker'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedDate'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['ReadingGarden']['LastModifiedBy'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("StickerID[]")."</th>\n";
		$li->no_col = $pos+2;
		
		$x .= $li->display();
		
		return $x;
	}
	
	function Get_Settings_Report_Sticker_Edit_Form($StickerID='')
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $ReadingGardenLib;
		
		if($StickerID!=''){
			$StickerInfo = $ReadingGardenLib->Get_Report_Sticker($StickerID);
		}
		
		# Get Btns
		$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"Button","isAddMore=0; CheckTBForm();");
		$SubmitAndContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'],"Button","isAddMore=1; CheckTBForm();");
		$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");
		
		$x .= '';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<form id="TBForm" name="TBForm" >'."\n";
			$x .= '<table width="98%" cellpadding=10px cellspacing=10px align="center" height="150px">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td valign="top">'."\n";	
						$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
							$x .= '<col class="field_title">';
							$x .= '<col class="field_c">';
							
							$FieldUploadImage = $Lang['ReadingGarden']['UploadStickerImage'];
							# Upload Image
							$imagePath = trim($StickerInfo[0]['ImagePath']);
							if($imagePath!=''){
								$fullImagePath = $ReadingGardenLib->StickerPath."/".$imagePath;
								$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['ReadingGarden']['CurrentStickerImage'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<img src="'.$fullImagePath.'" width="120px" height="120px">'."\n";
								$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								$FieldUploadImage = $Lang['ReadingGarden']['UploadNewStickerImage'];
							}
							
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$FieldUploadImage.'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input name="UploadImage" id="UploadImage" type="file" class="Mandatory"><br /><span style="color:grey">('.$Lang['ReadingGarden']['DescribeStickerDimension'].")</span>\n";
									$x .= $this->Get_Thickbox_Warning_Msg_Div("WarnStickerImage",$Lang['ReadingGarden']['WarningMsg']['RequestInputStickerImage'], "WarnMsgDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";

						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= $this->MandatoryField();		
			
			# Btns
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
			if(!($StickerID > 0))
				$x .= "&nbsp;".$SubmitAndContinueBtn;
				$x .= "&nbsp;".$CloseBtn;
			$x .= '</div>'."\n";
		
			# hidden field
			$x .= '<input type="hidden" name="StickerID" id="StickerID" value="'.$StickerID.'">'."\n";
		
		$x .= '</form>';	
		$x .= '<iframe id="FileUploadFrame" name="FileUploadFrame" style="display:none;"></iframe>'."\n";
		
		$x .= $this->FOCUS_ON_LOAD("TBForm.UploadImage");
		
		return $x;
	}
	
	// 20111013
	function Get_Statistic_Index()
	{
		global $Lang, $ReadingGardenLib;
		
		# Academic Year Select
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$AcademicYearSelect = getSelectAcademicYear("AcademicYearID"," onchange='js_Reload_Date_Range();' ", 1, 0, $AcademicYearID);
		
		# Date Picker
		$FromDate = $this->GET_DATE_PICKER("FromDate"," ");
		$ToDate = $this->GET_DATE_PICKER("ToDate"," ");

		# Form Selection 
		include_once("form_class_manage_ui.php");
		$fcm_ui = new form_class_manage_ui();
		$FormSelection = $fcm_ui->Get_Form_Selection("YearID[]", $YearID, " js_Reload_Class_Selection(); ", 1, 0, 1);
		
		# Class Selection
		$ClassSelection = $this->Get_Ajax_Loading_Image();
		
		# Category Selection
		$CategorySelection = $this->Get_Category_Selection("CategoryID", "CategoryID[]","","","","","",1,1);
		
		#Select All Btn
		$FormSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('YearID[]',true,'js_Reload_Class_Selection();');");
		$ClassSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('YearClassID[]',true);");
		$CategorySelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('CategoryID',true);");
		
		# Generate Btn
		$GenerateBtn = $this->GET_ACTION_BTN($Lang['Btn']['Generate'],"button","js_Submit_Form()");
		
		$x .= '<br>'."\n";
		$x .= '<form id="form1" name="form1" action="generate.php" target="_blank">'."\n";
			$SectionTitle = $Lang['ReadingGarden']['ReportSettings'];
			$x .= $this->GET_NAVIGATION2($SectionTitle)."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
				$x .= '<col class="field_title">'."\n";
				$x .= '<col class="field_c">'."\n";
				
				# Year
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>'."\n";
					$x .= '<td>'.$AcademicYearSelect.'</td>'."\n"; 
				$x .= '</tr>'."\n"; 
				
				# Date 
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['Date'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $FromDate."\n";
						$x .= $Lang['General']['To']."\n";
						$x .= $ToDate."\n";	
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 

				# Format
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['ViewFormat'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Radio_Button("ViewFormat1", "ViewFormat", "HTML", 1, "ViewFormat", $Lang['General']['HTML'], "")."\n";
						$x .= $this->Get_Radio_Button("ViewFormat2", "ViewFormat", "CSV", 0, "ViewFormat", $Lang['General']['CSV'], "")."\n";
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n";
				
				# Award Scheme
				if($ReadingGardenLib->Settings['RequireSelectAwardScheme'])
				{
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['ReadingGarden']['AwardScheme'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= $this->Get_Award_Scheme_Selection("AwardSchemeID", $Selected, $Language="", $ForStudentSelect="", $all=1, $noFirst=0, $FirstTitle="", $OnChange="", $other="");
						$x .= '</td>'."\n"; 
					$x .= '</tr>'."\n";
				}
				
			$x .= '</table>'."\n";
			
			$SectionTitle = $Lang['ReadingGarden']['DisplayItem'].' ('.$Lang['ReadingGarden']['Row'].') '."\n";
			$x .= '<br>'."\n";
			$x .= $this->GET_NAVIGATION2($SectionTitle)."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
				$x .= '<col class="field_title">'."\n";
				$x .= '<col class="field_c">'."\n";
				
				# Display Item
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['DisplayItem'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Radio_Button("RowItem1", "RowItem", "Form", 1, "RowItem", $Lang['ReadingGarden']['Form'], "js_Reload_Row_Item_Selection()")."\n";
						$x .= $this->Get_Radio_Button("RowItem2", "RowItem", "Class", 0, "RowItem", $Lang['ReadingGarden']['Class'], "js_Reload_Row_Item_Selection()")."\n";
						$x .= $this->Get_Radio_Button("RowItem3", "RowItem", "Student", 0, "RowItem", $Lang['ReadingGarden']['Student'], "js_Reload_Row_Item_Selection()")."\n";
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 
				
				# Form Selection 
				$x .= '<tr id="FormSelectionTR">'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Form'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div($FormSelection, $FormSelectAllBtn)."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankForm",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectForm'], "WarnMsgDiv");
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 
				
				# Class Selection 
				$x .= '<tr id="ClassSelectionTR" style="display:none;">'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Class'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div($ClassSelection, $ClassSelectAllBtn, "ClassSelectionSpan")."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankClass",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectClass'], "WarnMsgDiv");
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 
				
			$x .= '</table>'."\n";			 
			
			$SectionTitle = $Lang['ReadingGarden']['DisplayItem'].' ('.$Lang['ReadingGarden']['Column'].') '."\n";
			$x .= '<br>'."\n";
			$x .= $this->GET_NAVIGATION2($SectionTitle)."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
				$x .= '<col class="field_title">'."\n";
				$x .= '<col class="field_c">'."\n";
				
				# Display Item
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['DisplayItem'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Radio_Button("ColItem0", "ColItem", "Total", 1, "ColItem", $Lang['ReadingGarden']['Total'], "js_Reload_Col_Item_Selection()")."\n";
						$x .= $this->Get_Radio_Button("ColItem1", "ColItem", "Month", 0, "ColItem", $Lang['ReadingGarden']['Month'], "js_Reload_Col_Item_Selection()")."\n";
						$x .= $this->Get_Radio_Button("ColItem2", "ColItem", "Category", 0, "ColItem", $Lang['ReadingGarden']['Category'], "js_Reload_Col_Item_Selection()")."\n";
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 
				
				# Category Selection
				$x .= '<tr id="CategorySelectionTR" style="display:none;">'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['Category'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_MultipleSelection_And_SelectAll_Div($CategorySelection, $CategorySelectAllBtn, "CategorySelectionSpan")."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnBlankCategory",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectCategory'], "WarnMsgDiv");
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 

				# Book Report 
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['DisplayBookReport'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_CheckBox("DisplayBookReport", "DisplayBookReport", 1)."\n";
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 

				# Book Report 
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['DisplayNoOfMarkedReport'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_CheckBox("DisplayMarkedReport", "DisplayMarkedReport", 1)."\n";
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 

				# Group by lang
				$x .= '<tr id="MonthSelectionTR" style="display:none;">'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['SeparateByLang'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_CheckBox("SeparateByLang", "SeparateByLang", 1)."\n";
					$x .= '</td>'."\n"; 
				$x .= '</tr>'."\n"; 
				
			$x .= '</table>'."\n";	
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $GenerateBtn."\n";
			$x .= '</div>'."\n";
			
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Answer_Sheet_Template_Selection($ID_Name, $Selected, $all=0, $noFirst=0, $FirstTitle="", $OnChange="", $CategoryIDArr='')
	{
		global $Lang, $ReadingGardenLib;
		
		if(trim($CategoryIDArr)!='')
		{
			$TemplateList = $ReadingGardenLib->Get_Category_Answer_Sheet_Mapping($CategoryIDArr);
			$TemplateList = Get_Array_By_Key($TemplateList, "AnswerSheetID");
		}
		
		$AnswerSheets = $ReadingGardenLib->Get_Answer_Sheet_Template($TemplateList);
		
		for($i=0;$i<sizeof($AnswerSheets);$i++)
		{
			$optionArr[$AnswerSheets[$i]['TemplateID']] = htmlspecialchars($AnswerSheets[$i]['TemplateName']);
		}
		
		if(trim($OnChange)!='')
			$other = " onchange='$OnChange' ";
		
		$AnswerSheetSelection = getSelectByAssoArray($optionArr, " id='$ID_Name' name='$ID_Name' $other ",$Selected, $all, $noFirst, $FirstTitle);

		return $AnswerSheetSelection; 
	}

	//20111013
	function Get_Award_Scheme_Selection($ID_Name, $Selected, $Language="", $ForStudentSelect="", $all=0, $noFirst=0, $FirstTitle="", $OnChange="", $other="")
	{
		global $Lang, $ReadingGardenLib;
		if($ForStudentSelect==1)
		{
			$InProgressOnly = 1;
			$ClassID = $_SESSION['READING_SCHEME_SESSION']['StudentClassID'];
		}
		$AwardSchemeArr = $ReadingGardenLib->Get_Award_Scheme_List('', '', $Language, $InProgressOnly, $ClassID);
		
		for($i=0;$i<sizeof($AwardSchemeArr);$i++)
			$optionArr[$AwardSchemeArr[$i]['AwardSchemeID']] = htmlspecialchars($AwardSchemeArr[$i]['AwardName']);
		
		if(trim($OnChange)!='')
			$other .= " onchange='$OnChange' ";
		
		if(is_array($ID_Name))
			list($ID, $Name) = $ID_Name;
		else
			$ID = $Name = $ID_Name;
		
		$$AwardSchemeSelection = getSelectByAssoArray($optionArr, " id='$ID' name='$Name' $other ",$Selected, $all, $noFirst, $FirstTitle);

		return $$AwardSchemeSelection; 
	}
	
	function Get_String_Display($String, $checkEmptyOnly=0)
	{
		global $Lang;
		
		if(trim($String)=='')
			return $Lang['General']['EmptySymbol']; 
		else if($checkEmptyOnly)
			return $String;
		else
			return nl2br(htmlspecialchars($String));
	}
	
	function Get_Online_Writing_Word_Limit_Display($LowerLimit, $UpperLimit )
	{
		global $Lang;
//		$UpperLimit = $UpperLimit>0?$UpperLimit:0;
//		$LowerLimit = $LowerLimit>0?$LowerLimit:0;

		switch(true)
		{
			case $UpperLimit==-1&&$LowerLimit==-1; // disabled
				$LimitDisplay = $Lang['General']['EmptySymbol'];
			break;
			case $UpperLimit&&$LowerLimit; 
				$LimitDisplay = "$LowerLimit ~ $UpperLimit";
			break;
			case !empty($LowerLimit);
				$LimitDisplay = "> $LowerLimit";
			break;
			case !empty($UpperLimit);
				$LimitDisplay = "< $UpperLimit";
			break;
			default:
				$LimitDisplay = $Lang['ReadingGarden']['Unlimit'];
		}		
		
		return $LimitDisplay;
	}
	
	function Get_Book_Report_Setting_Index_UI()
	{
		global $ReadingGardenLib, $Lang; 
		
//		debug_pr($ReadingGardenLib->Settings);
//		die();
		
		$UseUploadFile= $ReadingGardenLib->Settings['FileUpload']!==0?$ReadingGardenLib->Settings['FileUpload']:0; // default active because of original design
		$UploadFileDesc = $ReadingGardenLib->Settings['FileUploadDesc']?$ReadingGardenLib->Settings['FileUploadDesc']:"";
		$UseOnlineWriting= $ReadingGardenLib->Settings['OnlineWriting']?$ReadingGardenLib->Settings['OnlineWriting']:0;
		$WritingDesc = $ReadingGardenLib->Settings['OnlineWritingDesc']?$ReadingGardenLib->Settings['OnlineWritingDesc']:"";
		
		$x .= '<form id="form1" name="form1" action="settings_update.php" method="POST">'."\n";
			$x .= '<div style="text-align:right; height:20px;">'.$this->Get_Small_Btn($Lang['Btn']['Edit'], "button", "js_Edit_View()", "", "", "DisplayView").'</div>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
			
				${"isCheckedUpload".$UseUploadFile} = 1; 
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['FileUpload'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="EditView" style="display:none">'."\n";
							$x .= $this->Get_Radio_Button("UploadFile_1", "FileUpload", 1, $isCheckedUpload1, "FileUpload", $Lang['ReadingGarden']['ActiveStatus'][1], "js_Disable_Desc('FileUpload');")."\n";
							$x .= $this->Get_Radio_Button("UploadFile_2", "FileUpload", 0, $isCheckedUpload0, "FileUpload", $Lang['ReadingGarden']['ActiveStatus'][0], "js_Disable_Desc('FileUpload');")."\n";
						$x .= '</div>'."\n";
						$x .= '<div class="DisplayView">'."\n";
							$x .= $Lang['ReadingGarden']['ActiveStatus'][$UseUploadFile];
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";	
				$x .= '</tr>'."\n";
				
				$x .= '<tr class="FileUploadTR">'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['FileUploadDesc'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="EditView" style="display:none">'."\n";
							$x .= $this->GET_TEXTAREA("FileUploadDesc", $UploadFileDesc);
						$x .= '</div>'."\n";
						$x .= '<div class="DisplayView">'."\n";
							$x .= Get_String_Display($UploadFileDesc);
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";	
				$x .= '</tr>'."\n";
				
				${"isCheckedWriting".$UseOnlineWriting} = 1; 
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['OnlineWriting'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="EditView" style="display:none">'."\n";
							$x .= $this->Get_Radio_Button("OnlineWriting_1", "OnlineWriting", 1, $isCheckedWriting1, "OnlineWriting", $Lang['ReadingGarden']['ActiveStatus'][1], "js_Disable_Desc('OnlineWriting');")."\n";
							$x .= $this->Get_Radio_Button("OnlineWriting_2", "OnlineWriting", 0, $isCheckedWriting0, "OnlineWriting", $Lang['ReadingGarden']['ActiveStatus'][0], "js_Disable_Desc('OnlineWriting');")."\n";
						$x .= '</div>'."\n";
						$x .= '<div class="DisplayView">'."\n";
							$x .= $Lang['ReadingGarden']['ActiveStatus'][$UseOnlineWriting];
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";	
				$x .= '</tr>'."\n";
				
				$x .= '<tr class="OnlineWritingTR">'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['OnlineWritingDesc'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div class="EditView" style="display:none">'."\n";
							$x .= $this->GET_TEXTAREA("OnlineWritingDesc", $WritingDesc);
						$x .= '</div>'."\n";
						$x .= '<div class="DisplayView">'."\n";
							$x .= Get_String_Display($WritingDesc);
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";	
				$x .= '</tr>'."\n";
				
				
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<div class="EditView" style="display:none">'."\n";
					$x .= $this->Get_Action_Btn($Lang['Btn']['Submit'],"submit")."\n";
					$x .= $this->Get_Action_Btn($Lang['Btn']['Cancel'],"button","js_Display_View()")."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;
	}
	
	function Get_Settings_Popular_Award_Scheme_Item_Setting_UI()
	{
		global $Lang;
		
//		$x .= '<br><br>'."\n";
		$x .= '<form name="form1" id="form1">'."\n";
			$x .= '<div class="table_board" id="ItemTableDiv">'."\n";
				$x .= $this->Get_Ajax_Loading_Image();
			$x .= '</div>'."\n";
		$x .= '</form>'."\n";
		$x .= '<br><br>'."\n";
		return $x;
	}
	
	function Get_Settings_Popular_Award_Scheme_Item_Setting_Table()
	{
		global $Lang, $ReadingGardenLib, $LAYOUT_SKIN, $image_path;
	
		$ThickBoxHeight = 450;
		$ThickBoxWidth = 750;
		
		$CatItemArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Item_Info();
		$CatItemAssoc = BuildMultiKeyAssoc($CatItemArr, array("CategoryID", "ItemID"), array("ItemDescEn", "ItemDescCh", "Score", "CanSubmitByStudent"));
		$CatArr = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info();
		$CatAssoc = BuildMultiKeyAssoc($CatArr, array("CategoryID"), array("CategoryCode", "CategoryNameEn", "CategoryNameCh"));
		
		
		$ChEn = Get_Lang_Selection("Ch", "En");

		# $SettingBtn
		$thisTitle = $Lang['ReadingGarden']['PopularAwardSchemeArr']['CategorySetting'];
		$thisOnclick = "js_Category_Setting();";
		$SettingLink = $this->Get_Thickbox_Link($this->TBHeight, $this->TBWidth, 'setting_row', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
		 
	 	$x .= '<table class="common_table_list_v30" id="ItemTable">'."\n";
	
			# col group
			$x .= '<col align="left" style="width: 25%;">'."\n";
			$x .= '<col align="left"">'."\n";
			$x .= '<col align="left" style="width: 10%;">'."\n";
			$x .= '<col align="left" style="width: 10%;">'."\n";
			$x .= '<col align="left" style="width: 30px">'."\n";
			$x .= '<col align="left" style="width: 100px">'."\n";
	
			# table head
			$ColSpan = 5;
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'."\n";
						$x .= '<span style="float:left">';
							$x .= $Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'];
						$x .= '</span>';
						$x .= '<span class="table_row_tool" style="float:right">';
							$x .= $SettingLink;
						$x .= '</span>';
					$x .= '</th>'."\n";
					$x .= '<th colspan="'.$ColSpan.'">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Item'].'</th>'."\n";
						
				$x .= '</tr>'."\n";
				$x .= '<tr class="sub_row">'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryName'].'</td>'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDesc'].'</td>'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Score'].'</td>'."\n";
					$x .= '<td class="sub_row_top">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['SubmitByStudent'].'</td>'."\n";
					$x .= '<td class="sub_row_top">&nbsp;</td>'."\n";
					$x .= '<td class="sub_row_top">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";		 	
			$x .= '</thead>'."\n";

			#table body
			$TickImg = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/attendance/icon_present_s.png\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
			
			if(count($CatAssoc))
			{
				foreach((array)$CatAssoc as $CategoryID => $thisCategoryInfo)
				{
					$CateogryName = Get_String_Display($thisCategoryInfo["CategoryName".$ChEn]);
					$thisItemArr = $CatItemAssoc[$CategoryID];
					
						$NumOfItem = count($thisItemArr)+2;

						$x .= '<tbody>'."\n";
						$x .= '<tr class="nodrop">'."\n";
							$x .= '<td rowspan="'.$NumOfItem.'">'.$CateogryName.'</td>'."\n";
						$x .= '</tr>'."\n";
		
						if($thisItemArr)
						{
							foreach((array)$thisItemArr as $ItemID => $thisItemInfo)
							{
		
								$DeleteLink = '<a href="javascript:void(0)" onclick="js_Delete_Item('.$ItemID.')" class="delete_dim" title="'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['DeleteItem'].'"></a>';
								$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['MoveItem'].'"></a>';
								$thisTitle = $Lang['ReadingGarden']['PopularAwardSchemeArr']['EditItem'];
								$thisOnclick="js_Edit_Item(".$CategoryID.", ".$ItemID.")";
								$ThickBoxEditLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth,  'edit_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
								
								$x .= '<tr>'."\n";
									$x .= '<td>'.Get_String_Display($thisItemInfo['ItemDesc'.$ChEn]).'</td>'."\n";
									$x .= '<td>'.$thisItemInfo['Score'].'</td>'."\n";
									$x .= '<td>'.($thisItemInfo['CanSubmitByStudent']?$TickImg:$Lang['General']['EmptySymbol']).'</td>'."\n";
									$x .= '<td class="Draggable"><span class="table_row_tool">'.$MoveLink.'</span></td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<div class="table_row_tool row_content_tool">'."\n";
											$x .= $ThickBoxEditLink;
											$x .= $DeleteLink;
										$x .= '</div>'."\n";
										$x .= '<input type="hidden" name="RowItemID[]" class="RowItemID" value="'.$ItemID.'">'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							}
						}
					
						# Add Record Row
						$thisTitle = $Lang['ReadingGarden']['PopularAwardSchemeArr']['AddItem'];
						$thisOnclick="js_Edit_Item($CategoryID)";
						$ThickBoxLink = $this->Get_Thickbox_Link($ThickBoxHeight, $ThickBoxWidth, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
						
						$x .= '<tr class="nodrop">'."\n";
							$x .= '<td colspan="999">'."\n";
								$x .= '<div class="table_row_tool row_content_tool">'."\n";
									$x .= $ThickBoxLink;
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";

						$x .= '</tbody>'."\n";

				}
				

			}
			else
			{
				$x .= '<tr class="nodrop">'."\n";
					$x .= '<td colspan="999">'."\n";
						$x .= '<div style="text-align:center; padding:20px;">'."\n";
							$x .= $Lang['General']['NoRecordAtThisMoment'];
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<script>$(function(){js_Show_ThickBox("'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategorySetting'].'", "'.$ReadingGardenUI->TBHeight.'", "'.$ReadingGardenUI->TBWidth.'"); js_Category_Setting();})</script>';
			}
				
		$x .= '</table>'."\n";
		
		return $x;		
	}

	function Get_Setting_Popular_Award_Scheme_Category_Edit_Layer()
	{
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
		
		$CategoryArr = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info();
		
		$x .= '<br>';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<div style="height:300px; overflow:auto;">'."\n";
		$x .= '<table id="CategoryTable" width="100%" border="0" cellspacing="0" cellpadding="2" class="common_table_list_v30">'."\n";
			$x .= '<col>';
			$x .= '<col width="30%">';
			$x .= '<col width="30%">';
			$x .= '<col width="30px">';
			$x .= '<col width="80px">';

			$x .= '<thead>'."\n";
				$x .= '<tr class="nodrag nodrop">'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryCode'].'</th>';
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryNameEn'].'</th>';
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryNameCh'].'</th>';
					$x .= '<th>&nbsp;</th>';
					$x .= '<th>&nbsp;</th>';
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
			
				# Category Row
				$onMouseEvent = ' onmouseover="Show_Edit_Background(this)" onmouseout="Hide_Edit_Background(this)" ';
				$MoveLink = $this->GET_LNK_MOVE("javascript:void(0);", $Lang['Btn']['Move']);
				
				$MaxDisplayOrder = 0;
				foreach((array)$CategoryArr as $thisCat)
				{
					$thisCatID = $thisCat['CategoryID'];
					$EditableCodeID = 'CategoryCode_'.$thisCatID;
					$EditableNameEnID = 'CategoryNameEn_'.$thisCatID;
					$EditableNameChID = 'CategoryNameCh_'.$thisCatID;
					
					$SubCatExist = $thisCat['NumOfItem']==0?0:1;
					if(!$SubCatExist)
					$DeleteLink = '<a href="javascript:void(0)" onclick="js_Delete_Category(\''.$thisCatID.'\')" class="delete_dim" title="'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['DeleteCategory'].'"></a>';
						
					$EditLink = '<a href="javascript:void(0)" onclick="js_Edit_Category(\''.$thisCatID.'\')" class="edit_dim" title="'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['EditCategory'].'"></a>';	
					
					$x .= '<tr id="row_'.$thisCatID.'">'."\n";
						$x .= '<td>'."\n";
							$x .= '<div class="DisplayDiv" >'.Get_String_Display($thisCat['CategoryCode']).'</div>'."\n";
							$x .= '<div class="InputDiv" style="display:none;">'."\n";
							$x .= '<input class="textboxtext required CategoryCode" onkeyup="js_Delay_Action(\'js_Check_Code()\')" type="text" id="CategoryCode" value="'.intranet_htmlspecialchars($thisCat['CategoryCode']).'"/>'."\n";
								$x .= $this->Get_Form_Warning_Msg("", $Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryCode'], "WarnMsg WarnEmptyCategoryCode");
								$x .= $this->Get_Form_Warning_Msg("WarnUnavailableCategoryCode", $Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CodeUnavailable'], "WarnUnavailableCategoryCode");
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<div class="DisplayDiv" >'.Get_String_Display($thisCat['CategoryNameEn']).'</div>'."\n";
							$x .= '<div class="InputDiv" style="display:none;">'."\n";
								$x .= '<input class="textboxtext required CategoryNameEn" type="text" id="CategoryNameEn" value="'.intranet_htmlspecialchars($thisCat['CategoryNameEn']).'"/>'."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryName'], "WarnMsg WarnEmptyCategoryNameEn");
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<div class="DisplayDiv" >'.Get_String_Display($thisCat['CategoryNameCh']).'</div>'."\n";
							$x .= '<div class="InputDiv" style="display:none;">'."\n";
								$x .= '<input class="textboxtext required CategoryNameCh" type="text" id="CategoryNameCh" value="'.intranet_htmlspecialchars($thisCat['CategoryNameCh']).'"/>'."\n";
								$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryName'], "WarnMsg WarnEmptyCategoryNameCh");
							$x .= '</div>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td class="Draggable">'.$MoveLink.'</td>'."\n";
						$x .= '<td>';
							$x .= '<div class="table_row_tool row_content_tool DisplayDiv">';
								$x .= $EditLink;
								$x .= $DeleteLink;
							$x .= '</div>';
							$x .= '<div style="display:none;" class="InputDiv">';
								$x .= $this->GET_SMALL_BTN($Lang['Btn']['Save'],"button","js_Validate_Category($thisCatID)");;
								$x .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'],"button","js_Cancel_Edit_Category($thisCatID)");
							$x .= '</div>';
							# hidden field
							$x .= '<input type="hidden" class="CategoryID" name="CategoryID[]" value="'.$thisCatID.'"> ';
						$x .= '</td>'."\n";		
					$x .= '</tr>'."\n";
					
					$MaxDisplayOrder = max($thisCat['DisplayOrder'], $MaxDisplayOrder);
				}			

				$SaveBtn = $this->GET_SMALL_BTN($Lang['Btn']['Save'],"button","js_Validate_Category()");
				$CancelBtn = $this->GET_SMALL_BTN($Lang['Btn']['Cancel'],"button","js_Cancel_Add_Category()");
				$x .= '<tr id="row_new" style="display:none;">';
					$x .= '<td>';
						$x .= '<input id="CategoryCode" name="CategoryCode" class="textboxtext required CategoryCode" onkeyup="js_Delay_Action(\'js_Check_Code()\')" maxlength="'.$this->MaxCodeLength.'">';
						$x .= $this->Get_Form_Warning_Msg("", $Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryCode'], "WarnMsg WarnEmptyCategoryCode");
						$x .= $this->Get_Form_Warning_Msg("WarnUnavailableCategoryCode", $Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CodeUnavailable'], "WarnUnavailableCategoryCode");
					$x .= '</td>';
					$x .= '<td>';
						$x .= '<input id="CategoryNameEn" name="CategoryNameEn" class="textboxtext required CategoryNameEn" maxlength="'.$this->MaxLength.'">';
						$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryName'], "WarnMsg WarnEmptyCategoryNameEn");
					$x .= '</td>';
					$x .= '<td>';
						$x .= '<input id="CategoryNameCh" name="CategoryNameCh" class="textboxtext required CategoryNameCh" maxlength="'.$this->MaxLength.'">';
						$x .= $this->Get_Form_Warning_Msg("",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryName'], "WarnMsg WarnEmptyCategoryNameCh");
					$x .= '</td>';
					$x .= '<td>';
						$x .= '&nbsp;';
					$x .= '</td>';
					$x .= '<td>';
						$x .= $SaveBtn;
						$x .= $CancelBtn;
					$x .= '</td>';
				$x .= '</tr>';
				
				# Add Cat Row
				$OnClick = "js_Add_Category();";
				$x .= '<tr id="add_row" class="nodrag nodrop">'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>';
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= '<a class="add_dim" href="javascript:void(0);" onclick="'.$OnClick.'"></a>';
						$x .= '</div>';
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";	
		$x .= '</div>';
		
		# Btns
		$CloseBtn = $this->Get_Action_Btn($Lang['Btn']['Close'],"Button","js_Hide_ThickBox()");
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $CloseBtn;
		$x .= '</div>'."\n";
		
		if(count($CategoryArr)==0)
			$x .= '<script>$(function(){js_Add_Category();})</script>';
		
		return $x;
		
	}
	
	function Get_Popular_Award_Scheme_Item_Edit_Layer($CategoryID, $ItemID='')
	{
		global $Lang, $ReadingGardenLib;
		
		$CategoryInfo = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info($CategoryID);
		$CatNameLang = Get_Lang_Selection("CategoryNameCh", "CategoryNameEn");
		
		if(trim($ItemID)!='')
		{
			$ItemInfo = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Info($ItemID);
			$ItemInfo[0]['CanSubmitByStudent']?$SubmitByStudentYes=1:$SubmitByStudentNo=1;	
		}
		else
		{
			$SubmitByStudentYes = 1;
		}
		
		$x .= '<form name="TBForm" id="TBForm">'."\n";
		$x .= '<br>';
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<div style="height:300px; overflow:auto;">'."\n";
			
			$x.= '<table class="form_table_v30 form_table_thickbox">'."\n";
				$x.= '<tbody>'."\n";
					
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x .= Get_String_Display($CategoryInfo[0][$CatNameLang])."\n";
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemCode'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x .= '<input name="ItemCode" id="ItemCode" class="textbox_name Mandatory" value="'.htmlspecialchars($ItemInfo[0]['ItemCode']).'" maxlength="'.$this->MaxCodeLength.'" onkeyup="js_Delay_Action(\'js_Check_ItemCode()\')">'."\n";
							$x .= $this->Get_Form_Warning_Msg("WarnEmptyItemCode",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemCode'], "WarnMsgDiv");
							$x .= $this->Get_Form_Warning_Msg("WarnItemCodeExist",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['ItemCodeExist'], "WarnMsgDiv");
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDescEn'].'</td>'."\n";
						$x.= '<td>'."\n";
//							$x .= '<input name="ItemDescEn" id="ItemDescEn" class="textbox_name Mandatory" value="'.$ItemInfo[0]['ItemDescEn'].'">'."\n";
							$x .= $this->GET_TEXTAREA("ItemDescEn", $ItemInfo[0]['ItemDescEn'], 70, 5, NULL, NULL, NULL, "Mandatory")."\n";
							$x .= $this->Get_Form_Warning_Msg("WarnEmptyItemDescEn",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemName'], "WarnMsgDiv");
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDescCh'].'</td>'."\n";
						$x.= '<td>'."\n";
//							$x .= '<input name="ItemDescCh" id="ItemDescCh" class="textbox_name Mandatory" value="'.$ItemInfo[0]['ItemDescCh'].'">'."\n";
							$x .= $this->GET_TEXTAREA("ItemDescCh", $ItemInfo[0]['ItemDescCh'], 70, 5, NULL, NULL, NULL, "Mandatory")."\n";
							$x .= $this->Get_Form_Warning_Msg("WarnEmptyItemDescCh",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemName'], "WarnMsgDiv");
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['Score'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x .= '<input name="Score" id="Score" class="textbox_num Mandatory" value="'.$ItemInfo[0]['Score'].'" maxlength="'.$this->MaxCodeLength.'">'."\n";
							$x .= $this->Get_Form_Warning_Msg("WarnEmptyScore",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputScore'], "WarnMsgDiv");
							$x .= $this->Get_Form_Warning_Msg("WarnNonNumbericScore",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputNumber'], "WarnMsgDiv");
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['SubmitByStudent'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x.= $this->Get_Radio_Button("SubmitByStudentYes", "CanSubmitByStudent", 1, $SubmitByStudentYes, $Class="CanSubmitByStudent", $Display=$Lang['General']['Yes'], $Onclick="",$isDisabled=0)."\n";
							$x.= $this->Get_Radio_Button("SubmitByStudentNo", "CanSubmitByStudent", 0, $SubmitByStudentNo, $Class="CanSubmitByStudent", $Display=$Lang['General']['No'], $Onclick="",$isDisabled=0)."\n";
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
				$x.= '</tbody>'."\n";
			$x.= '</table>'."\n";
			$x .= '<input type="hidden" id="CategoryID" name="CategoryID" value="'.$CategoryID.'">'."\n";
			$x .= '<input type="hidden" id="ItemID" name="ItemID" value="'.$ItemID.'">'."\n";			
		$x .= '</div>';
		$x .= $this->MandatoryField();
		
		# Btns
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'],"Button","js_Validate_Item()");
		$SubmitAndAddMoreBtn = $this->Get_Action_Btn($Lang['Btn']['Submit&AddMore'],"Button","js_Validate_Item(1)");
		$CancelBtn = $this->Get_Action_Btn($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $SubmitBtn."\n";
			$x .= $SubmitAndAddMoreBtn."\n";
			$x .= $CancelBtn."\n";
		$x .= '</div>'."\n";
		$x .= '</form>';
		
		return $x;
	}
	
	function Get_Popular_Award_Scheme_Add_Record_Layer($Callback='', $StudentID='')
	{
		global $Lang;
	
		$StudentSubmit = $StudentID?0:1; // Only teacher submit will pass StudentID
		
		$x .= '<br/>'."\n";
		$x .= '<form id="TBForm" name="TBForm" >'."\n";
			$x .= '<table class="form_table_v30 form_table_thickbox">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'].'</td>'."\n";
					$x .= '<td>'."\n";	
						$x .= $this->Get_Popular_Award_Scheme_Categoty_Selection("PopularAwardSchemeAddLayerCategoryID", "", 0, 1, "", "js_Reload_Popular_Award_Scheme_Add_Layer_Category($StudentSubmit)", 1, $StudentSubmit)."\n"; //  js_Reload_Popular_Award_Scheme_Add_Layer_Category handled in reading_scheme.js
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<div id="PopularAwardSchemeItemSelection">'."\n";
			$x .= '</div>'."\n";

			$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'],"Button","js_Check_Submit_Popular_Award_Scheme_Add_Record('".addslashes($Callback)."', '$StudentID')");
			$CancelBtn = $this->Get_Action_Btn($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>'."\n";
			
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Popular_Award_Scheme_Item_Select_Form($CategoryID, $StudentSubmit)
	{
		global $ReadingGardenLib, $Lang;
		
		$ItemArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Item_Info("", "", $CategoryID, $StudentSubmit);
		$ItemDescLang = Get_Lang_Selection("ItemDescCh", "ItemDescEn");
		
		for($i=1; $i<=10; $i++)
			$options[$i] = $i;
		
		$TimesSelection = getSelectByAssoArray($options, " id='times' name='times' ",1, 0, 1);	
		
		$x .= '<div style="overflow:auto;">'."\n";
		
			$x .= '<table cellpadding="0" cellspacing="0" border="0" class="common_table_list_v30" width="100%" style="border-color:#aaa;">'."\n";
				$x .= '<col width="5px">'."\n";
				$x .= '<col>'."\n";
			
			$SizeOfItem = sizeof($ItemArr);
			for($i=0; $i<$SizeOfItem; $i++)
			{
				$thisID = "PopularAwardSchemeItem_".$ItemArr[$i]['ItemID'];
				$isChecked = false;
						
				
				$x .= '<tr>'."\n";
					$x .= '<td>'.$this->Get_Radio_Button($thisID, "PopularAwardSchemeItem", $ItemArr[$i]['ItemID'], $isChecked, $Class='PopularAwardSchemeItem', "", "").'</td>'."\n";
					$x .= '<td><label for="'.$thisID.'"><div>'. Get_String_Display($ItemArr[$i][$ItemDescLang]).'</div></label></td>'."\n";
				$x .= '</tr>'."\n"; 
			}
			$x .= '</table>'."\n";
			$x .= $this->Get_Form_Warning_Msg("WarnEmptyPopularAwardSchemeItem",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectItem'],"WarnMsg");
			
			$x .= '<table class="form_table_v30 form_table_thickbox">'."\n";
				$x .= '<col class="field_title">';
				$x .= '<col class="field_c">';
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Times'].'</td>'."\n";
					$x .= '<td>'.$TimesSelection.'</td>'."\n";
				$x .= '</tr>'."\n"; 
			$x .= '</table>'."\n";
			
		$x .= '</div>'."\n";

		return $x;
	}
	
	function Get_Popular_Award_Scheme_Categoty_Selection($ID_Name, $SelectedCategory='', $all=0, $noFirst=0, $FirstTitle="", $OnChange="", $SkipEmptyCategory=0, $ForStudentSelect=0)
	{
		global $ReadingGardenLib;	
		
		$Category = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info('', '', $SkipEmptyCategory, $ForStudentSelect);
		
		$CatNameLang = Get_Lang_Selection("CategoryNameCh", "CategoryNameEn");
		$SizeOfCategory = sizeof($Category);
		for($i=0; $i<$SizeOfCategory; $i++)
		{
			$Option[$Category[$i]['CategoryID']] = htmlspecialchars($Category[$i][$CatNameLang]);
		}

		if(trim($OnChange)!='')
			$other = " onchange='$OnChange' ";
		
		$Selection = getSelectByAssoArray($Option, " id='$ID_Name' name='$ID_Name' $other ",$SelectedCategory, $all, $noFirst, $FirstTitle);
		
		return $Selection;
	}
	
	function Get_Popular_Award_Scheme_Form_Table() 
	{
		global $Lang, $ReadingGardenLib;
		
		$ClassInputDate = $ReadingGardenLib->Get_Popular_Award_Scheme_Record_Last_Input_Date();
		$ClassInputDate = BuildMultiKeyAssoc($ClassInputDate, "YearClassID", array("InputBy", "DateInput"));
		
		include_once("form_class_manage.php");
		$year = new year();
		$YearClassList = $year->Get_All_Classes();
		$YearClassAssoc = BuildMultiKeyAssoc($YearClassList, array("YearID","YearClassID"), array("ClassTitleEN", "ClassTitleB5"));
		$YearAssoc = BuildMultiKeyAssoc($YearClassList, array("YearID"), array("YearName"), 1);
		
		$ClassTitleLang = Get_Lang_Selection("ClassTitleB5", "ClassTitleEN");
			
		$ImportBtn = '<div class="Conntent_tool">'."\n";
			$ImportBtn .= $this->Get_Content_Tool_v30('import','./import_award_record_step1.php',$Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord']);
		$ImportBtn .= '</div>'."\n";	
			
		$x .= $ImportBtn;
		$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
			# col group
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left">'."\n";
	
			# table head
			$ColSpan = "4";
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['ReadingGarden']['Form'].'</th>';
					$x .= '<th colspan="'.$ColSpan.'" class="sub_row_top">'.$Lang['ReadingGarden']['Class'].'</th>';	
				$x .= '</tr>';
				$x .= '<tr>';
					$x .= '<th >'.$Lang['ReadingGarden']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['ReadingGarden']['Name'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['General']['InputBy'].'</th>';
					$x .= '<th class="sub_row_top">'.$Lang['General']['DateInput'].'</th>';
					$x .= '<th class="sub_row_top">&nbsp;</th>';
				$x .= '</tr>';
				
			$x .= '</thead>';		
			
			# table body
			$x .= '<tbody>';
			
				# loop Year
				foreach((array)$YearAssoc as $YearID => $YearName)
				{
				
					$ClassInfoArr = $YearClassAssoc[$YearID];
					$NumOfClass = count($ClassInfoArr) + 2;
					
					$x .= '<tr>';
						$x .= '<td rowspan="'.$NumOfClass.'">'.$YearName.'</td>';
					$x .= '</tr>';
					
					# loop Class
					foreach((array)$ClassInfoArr as $thisClassID => $thisClassInfo)
					{

						$InputDate = $ClassInputDate[$thisClassID]['DateInput'];
						$InputBy = $ClassInputDate[$thisClassID]['InputBy'];

						$x .= '<tr class="sub_row">';
							$x .= '<td>'.$thisClassInfo[$ClassTitleLang].'</td>';
							$x .= '<td>'.Get_String_Display($InputDate).'</td>';	
							$x .= '<td>'.Get_String_Display($InputBy).'</td>';
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool"><a href="student_score_list.php?ClassID='.$thisClassID.'" class="edit_dim"></a></div>';
							$x .= '</td>';
						$x .= '</tr>';
					} // end loop class

					$x .= '<tr>';
						$x .= '<td colspan="3">&nbsp;</td>';
						$x .= '<td>';
							$x .= '<div class="table_row_tool row_content_tool"><a href="student_score_list.php?YearID='.$YearID.'" class="edit_dim"></a></div>';
						$x .= '</td>';
					$x .= '</tr>';
				}// end loop form

				
			$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Popular_Award_Scheme_Student_Score_Table($ClassIDArr, $YearID)
	{
		global $Lang, $ReadingGardenLib;
		
		include_once("form_class_manage.php");
		if(trim($YearID)!='')
		{
			$year = new year($YearID);
			$ClassIDArr = Get_Array_By_Key($year->Get_All_Classes(), "YearClassID");
			$YearName = $year->YearName;
		}
		
		foreach((array)$ClassIDArr as $ClassID)
		{
			$year_class = new year_class($ClassID,true,false,true);
			$StudentArr = array_merge((array)$StudentArr, $year_class->ClassStudentList);
		}
		
		$ClassTitleLang = Get_Lang_Selection("ClassTitleB5", "ClassTitleEN");
		$YearClassName = $YearName?$YearName:$year_class->{$ClassTitleLang};
		
		$StudentIDArr= Get_Array_By_Key($StudentArr, "UserID");
		$StudentInfoArr = BuildMultiKeyAssoc($StudentArr, "UserID");
		
		$CategoryArr = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info('', '', $SkipEmptyCategory=1);
		$CatNameLang = Get_Lang_Selection("CategoryNameCh", "CategoryNameEn") ;	

		$StudentScore = $ReadingGardenLib->Get_Popular_Award_Scheme_Student_Score();
		$StudentScore = BuildMultiKeyAssoc($StudentScore, array("StudentID", "CategoryID"), "Score", 1);
		
		if(sizeof($StudentArr))
		{
			foreach($StudentArr as $thisStudentInfo)
			{
				if($StudentScore[$thisStudentInfo['UserID']])
					$StudentTotalArr[$thisStudentInfo['UserID']] = array_sum($StudentScore[$thisStudentInfo['UserID']]);
				else
					$StudentTotalArr[$thisStudentInfo['UserID']] = 0;
			}
			arsort($StudentTotalArr); // sort the student assoc in desc order
		}
		
		$StudentAward = $ReadingGardenLib->Get_Popular_Award_Scheme_Student_Award($StudentIDArr);
		
		$AwardNameLang = Get_Lang_Selection("AwardNameCh", "AwardNameEn");
		
		$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardScheme'], "index.php");
		$PAGE_NAVIGATION[] = array($YearClassName, "");
		$PageNavigation = $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
		
		$x .= $PageNavigation;
		
		$x .= '<table class="common_table_list_v30 edit_table_list_v30" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['General']['Class'].'</th>'."\n";
					$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['Student'].'</th>'."\n";
					foreach($CategoryArr as $thisCat)
					{
						$x .= '<th>'.$thisCat[$CatNameLang].'</th>'."\n";	
					}
					$x .= '<th>'.$Lang['ReadingGarden']['Total'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Award'].'</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n"
			;
			$x .= '<tbody>'."\n";
				foreach((array)$StudentTotalArr as $thisUserID => $thisTotalScore)
				{
					$thisStudentInfo = $StudentInfoArr[$thisUserID];
					 
					$x .= '<tr>'."\n";
						$x .= '<td>'.$thisStudentInfo['ClassName'].'</td>'."\n";
						$x .= '<td>'.$thisStudentInfo['ClassNumber'].'</td>'."\n";
						$x .= '<td>'.$thisStudentInfo['StudentName'].'</td>'."\n";
						foreach($CategoryArr as $thisCat)
						{
							$Score = $StudentScore[$thisStudentInfo['UserID']][$thisCat['CategoryID']];
							$Score = $Score?$Score:0;
							$x .= '<td><a href="student_record.php?clearCoo=1&StudentID='.$thisStudentInfo['UserID'].'&CategoryID='.$thisCat['CategoryID'].'">'.$Score.'</a></td>'."\n";
						}
						$x .= '<td><a href="student_record.php?clearCoo=1&StudentID='.$thisStudentInfo['UserID'].'">'.$thisTotalScore.'</a></td>'."\n";
						$x .= '<td>'.Get_String_Display($StudentAward[$thisStudentInfo['UserID']][$AwardNameLang]).'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";	
				
		return $x;	
	}
	
	function Get_Popular_Award_Scheme_Form_Student_Score_Record_UI($StudentID, $field='', $order='', $pageNo='', $numPerPage='', $CategoryID='')
	{
		
		global $Lang, $PATH_WRT_ROOT, $ReadingGardenLib;
	
		$ClassInfo = $ReadingGardenLib->Get_Student_ClassInfo($StudentID);
		$NameLang = Get_Lang_Selection("ChineseName", "EnglishName");
		$ClassNameLang = Get_Lang_Selection("ClassTitleB5", "ClassTitleEN");
	
		$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardScheme'], "index.php");
		$PAGE_NAVIGATION[] = array($ClassInfo[0][$ClassNameLang], "student_score_list.php?ClassID=".$ClassInfo[0]['YearClassID']);
		$PAGE_NAVIGATION[] = array($ClassInfo[0][$NameLang], "");
		$PageNavigation = $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

		$CategorySelection = $this->Get_Popular_Award_Scheme_Categoty_Selection("CategoryID", $CategoryID, 1, 0, Get_Selection_First_Title($Lang['General']['All']), "js_Reload_Popular_Award_Scheme_Record_Table()", 1)."\n"; //  js_Reload_Popular_Award_Scheme_Add_Layer_Category handled in reading_scheme.js
		$ThickBoxNewLink = $this->Get_Content_Tool_v30('new',"javascript:load_dyn_size_thickbox_ip('".$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord']."', 'Get_Popular_Award_Scheme_Record_Add_Layer(\'js_Reload_Popular_Award_Scheme_Record_Table();\', $StudentID);');", $Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord'], "");
		
		$x .= '<form id="form1" name="form1" method="post" >'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= $PageNavigation."\n";
					
						#content_top_tool 
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $ThickBoxNewLink."\n";
							$x .= '</div>'."\n";
//							$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n"; 
						# table_filter / common_table_tool  
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="table_filter">'."\n";
										$x .= $CategorySelection."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
								       $x .= '<a class="tool_delete" href="javascript:checkRemove2(document.form1,\'StudentItemID[]\',\'js_Delete_Popular_Award_Scheme_Record();\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";

						# Main Table	
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="StudentRecordDBTable">'."\n";
										$x .= $this->Get_Ajax_Loading_Image()."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						  
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="pageNo" value="'.$pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$numPerPage.'" />';
			$x .= '<input type="hidden" name="StudentID" value="'.$StudentID.'" />';	
		
		$x .= '</form>'."\n";
		
		return $x;
	}

	function Get_Popular_Award_Scheme_Form_Student_Score_Record_DBTable($StudentID, $field='', $order='', $pageNo='', $numPerPage='', $CategoryID='')
	{
		global $Lang, $page_size, $ck_setting_popular_award_scheme_student_record_page_size, $ReadingGardenLib;
		
		include_once("libdbtable.php");
		include_once("libdbtable2007a.php");
		
		$field = ($field==='')? 4 : $field;
		$order = ($order==='')? 0 : $order;
		$pageNo = ($pageNo==='')? 1 : $pageNo;

		if (isset($ck_setting_popular_award_scheme_student_record_page_size) && $ck_setting_popular_award_scheme_student_record_page_size != "") $page_size = $ck_setting_popular_award_scheme_student_record_page_size;
		$li = new libdbtable2007($field,$order,$pageNo);
		
		$sql = $ReadingGardenLib->Get_Management_Popular_Award_Scheme_DBTable_Sql($StudentID, $CategoryID);
		
		$li->sql = $sql;
		$li->IsColOff = "IP25_table";
		
		$EnCh = Get_Lang_Selection("Ch", "En");
		$NameLang = getNameFieldByLang("iu.");
			
		$li->field_array = array("i.ItemCode", "i.ItemDesc$EnCh", "ic.CategoryName$EnCh", "$NameLang", "sim.DateInput");
		$li->column_array = array(18,18,18,18,18);
		$li->wrap_array = array(0,0,0,0,0);
		
		$pos = 0;
		$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
		$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemCode'])."</th>\n";
		$li->column_list .= "<th width='40%'>".$li->column_IP25($pos++, $Lang['ReadingGarden']['PopularAwardSchemeArr']['Item'])."</th>\n";
		$li->column_list .= "<th width=''>".$li->column_IP25($pos++, $Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['General']['InputBy'])."</th>\n";
		$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['General']['DateInput'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("StudentItemID[]")."</th>\n";

		$li->no_col = $pos+2;
		
		$x .= $li->display("","common_table_list_v30");

//		debug_pr($li->built_sql());
	
		return $x;		
	}
	
	function Get_Settings_Popular_Award_Scheme_Award_UI()
	{
		global $Lang;
		
		# Add Btn
		$NewBtn = '<div class="Conntent_tool">'."\n";
			$NewBtn .= $this->Get_Content_Tool_v30('new','./edit_award.php',$Lang['ReadingGarden']['PopularAwardSchemeArr']['NewAward']);
		$NewBtn .= '</div>'."\n";

		
		$AwardTable = $this->Get_Settings_Popular_Award_Scheme_Award_Table();
		
//		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="post" action="state.php" onsubmit="return false;">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $NewBtn."\n";					
							$x .= '</div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'AwardID[]\',\'edit_award.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'AwardID[]\',\'delete_award.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span id="AwardTable">'.$AwardTable.'</span>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;		
	}
	
	function Get_Settings_Popular_Award_Scheme_Award_Table()
	{
		global $Lang, $ReadingGardenLib;
		
		$AwardArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Info();
		$AwardAssoc = BuildMultiKeyAssoc($AwardArr, "AwardID", NULL, 1);

		$FormMapping = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Form_Mapping();
		$FormMapping = BuildMultiKeyAssoc($FormMapping, "AwardID", "YearName", 1, 1);

		$RequirementArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Requirement_Info();
		$RequirementScoreAssoc = BuildMultiKeyAssoc($RequirementArr, array("AwardID", "RequirementID"), "Score", 1);
		
		$RequirementCategoryArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Requirement_Category();
		$RequirementCategoryAssoc = BuildMultiKeyAssoc($RequirementCategoryArr, "RequirementID", "CategoryID", 1, 1);
		
		$CategoryIDArr = Get_Array_By_Key($RequirementCategoryArr, "CategoryID");
		$CategoryArr = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info($CategoryIDArr);
		$CategoryAssoc = BuildMultiKeyAssoc($CategoryArr, "CategoryID");
		
		$EnCh = Get_Lang_Selection("Ch", "En");
		
		$x .= '<table class="common_table_list">'."\n";
			$x .= '<col style="width:20%">'."\n";
			$x .= '<col style="width:10%">'."\n";
			$x .= '<col style="width:20%">'."\n";
			$x .= '<col>'."\n";
			$x .= '<col>'."\n";
			$x .= '<col>'."\n";
			$x .= '<col style="width:10px">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardName'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoreRequired'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['Form'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardRequirement'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['LastModifiedBy'].'</th>'."\n";
					$x .= '<th>'.$Lang['ReadingGarden']['LastModifiedDate'].'</th>'."\n";
					$x .= '<th>'.$this->Get_Checkbox($ID="CheckAllAward", "", "", 0, '', '', 'js_Check_All_Award(this.checked)').'</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
			if(sizeof($AwardAssoc))
			{
				foreach($AwardAssoc as $AwardID => $thisAward)
				{
					$AwardYearName = implode(", ", (array)$FormMapping[$AwardID]);
					
					$x .= '<tr>'."\n";
						$x .= '<td><a href="edit_award.php?AwardID[]='.$AwardID.'">'.Get_String_Display($thisAward['AwardName'.$EnCh]).'</a></td>'."\n";
						$x .= '<td>'.$thisAward['Score'].'</td>'."\n";
						$x .= '<td>'.Get_String_Display($AwardYearName).'</td>'."\n";
						$x .= '<td>'."\n";
							$thisAwardRequirementScoreArr = $RequirementScoreAssoc[$AwardID];
							if(sizeof($thisAwardRequirementScoreArr))
							{
								foreach($thisAwardRequirementScoreArr as $thisRequirementID => $Score)
								{
									$CatNameArr = array();
									foreach($RequirementCategoryAssoc[$thisRequirementID] as $thisCatID)
									{
										$CatNameArr[] = Get_String_Display($CategoryAssoc[$thisCatID]['CategoryName'.$EnCh]);
									}
									
									$x .= '<div>'.implode("/", (array)$CatNameArr).": ".$Score.'</div>'."\n";		
								}
							}
							else
							{
								$x .= $Lang['General']['EmptySymbol']."\n";		
							}
						$x .= '</td>'."\n";
						$x .= '<td>'.Get_String_Display($thisAward['LastModifiedBy']).'</td>'."\n";
						$x .= '<td>'.Get_String_Display($thisAward['DateModified']).'</td>'."\n";
						$x .= '<td>'.$this->Get_Checkbox($ID="AwardID", "AwardID[]", $AwardID, 0, '', '', '').'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			}
			else
			{
				$x .= '<tr>'."\n";
					$x .= '<td colspan="999">'."\n";
						$x .= '<div style="text-align:center; padding:20px;">'."\n";
							$x .= $Lang['General']['NoRecordAtThisMoment'];
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
			}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
		
	}
	
	function Get_Settings_Popular_Award_Scheme_Edit_Award_UI($AwardID='')
	{
		global $Lang, $ReadingGardenLib;
		
		if(trim($AwardID)!='')
		{
			$AwardInfo = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Info($AwardID);
			$FormMapping = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Form_Mapping($AwardID);
			$YearIDArr = Get_Array_By_Key($FormMapping, "YearID");
			$RequirementInfo = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Requirement_Info('', $AwardID);
			$AddEdit = $Lang['Btn']['Edit'];
		}
		else
		{
			$AddEdit = $Lang['Btn']['New'];
		}

		$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['PopularAwardScheme'], "index.php");
		$PAGE_NAVIGATION[] = array($AddEdit, "");
		$PageNavigation = $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

		// Btn 
		$SubmitBtn = $this->Get_Action_Btn($Lang['Btn']['Submit'],"submit", "");
		$CancelBtn = $this->Get_Action_Btn($Lang['Btn']['Cancel'],"button", "window.location='index.php';");

		$AddRequirementBtn = '[<a href="javascript:void(0);" onclick="Add_Requirement();">'.$Lang['Btn']['AddMore'].'</a>]';

		$x .= '<form id="form1" name="form1" action="edit_award_update.php" method="POST" onsubmit="return js_Check_Form();">'."\n";
			
			$x .= $PageNavigation."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x.= '<tbody>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title" rowspan="2">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardName'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x.= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>'."\n";
							$x.= '<span class="row_content_v30">'."\n";
								$x.= '<input name="AwardNameEn" id="AwardNameEn" class="textbox_name" type="text" value="'.htmlspecialchars($AwardInfo[0]['AwardNameEn']).'">'."\n";
								$x .= $this->Get_Form_Warning_Msg("WarnEmptyAwardNameEn",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputAwardName'],"WarnMsg");
							$x.= '</span>'."\n";
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td>'."\n";
							$x.= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>'."\n";
							$x.= '<span class="row_content_v30">'."\n";
								$x.= '<input name="AwardNameCh" id="AwardNameCh" class="textbox_name" type="text" value="'.htmlspecialchars($AwardInfo[0]['AwardNameCh']).'">'."\n";
								$x .= $this->Get_Form_Warning_Msg("WarnEmptyAwardNameCh",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputAwardName'],"WarnMsg");
							$x.= '</span>'."\n";
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";

					# Form Seletion 
					include_once("form_class_manage_ui.php");
					$fcm_ui = new form_class_manage_ui();
					$FormSelection = $fcm_ui->Get_Form_Selection("YearID[]", $YearIDArr, "", 1, 0, 1);
					$YearSelectAllBtn = $this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('YearID[]',true);");
					$YearSelection = $this->Get_MultipleSelection_And_SelectAll_Div($FormSelection, $YearSelectAllBtn);

					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['Form'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x .= $YearSelection."\n";
							$x .= $this->Get_Form_Warning_Msg("WarnEmptyForm",$Lang['ReadingGarden']['WarningMsg']['PleaseSelectForm'],"WarnMsg");
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoreRequired'].'</td>'."\n";
						$x.= '<td>'."\n";
							$x .= '<input id="TotalScore" name="TotalScore" class="textbox_num" value="'.$AwardInfo[0]['Score'].'">'."\n";
							$x .= $this->Get_Form_Warning_Msg("WarnEmptyScoreRequired_Total",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputScore'],"WarnMsg");
							$x .= $this->Get_Form_Warning_Msg("WarnNumberScoreRequired_Total",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputNumber'],"WarnMsg");
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";

					// Requirement
					$x.= '<tr>'."\n";
						$x.= '<td class="field_title" rowspan="2">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardRequirement'].'</td>'."\n";
						$x.= '<td id="RequirementEntryTD">'."\n";
							$SizeOfRequirement = sizeof($RequirementInfo); 
							if($SizeOfRequirement > 0)
							{
								for($i=0; $i<$SizeOfRequirement; $i++)
									$x .= $this->Get_Popular_Award_Scheme_Requirement_Entry($i, $RequirementInfo[$i]['RequirementID']);
							}
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
					$x.= '<tr>'."\n";
						$x.= '<td>'."\n";
							$x.= $AddRequirementBtn."\n";
						$x.= '</td>'."\n";
					$x.= '</tr>'."\n";
				$x.= '</tbody>'."\n";
			$x .= '</table>'."\n";
			$x .= '<input type="hidden" name="AwardID" value="'.$AwardID.'">'."\n";
			
			$x .= $this->MandatoryField()."\n";
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>'."\n";

		$x .= '</form>'."\n";
		
		return $x;
				
	}
	
	function Get_Popular_Award_Scheme_Requirement_Entry($EntryIdx, $RequirementID='')
	{
		global $Lang, $ReadingGardenLib;
		
		if(trim($RequirementID)!='')
		{
			$RequirementInfo = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Requirement_Info($RequirementID);
			$RequirementCat = $ReadingGardenLib->Get_Popular_Award_Scheme_Award_Requirement_Category($RequirementID);
			$CategoryIDArr = Get_Array_By_Key($RequirementCat, "CategoryID");
		}
		
		$CategoryArr = $ReadingGardenLib->Get_Popular_Award_scheme_Item_Category_Info('', '', 1);
		
		$DeleteRequirementBtn = '[<a href="javascript:void(0);" onclick="Delete_Requirement('.$EntryIdx.');">'.$Lang['Btn']['Delete'].'</a>]';
		
		$x .= '<div id="EntryDiv_'.$EntryIdx.'" class="EntryDiv" style="border:1px solid #ccc; margin:2px;">'."\n";
		$x .= '<table class="form_table_v30">'."\n";
			$x.= '<tbody>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoreRequired'].'</td>'."\n";
					$x.= '<td>'."\n";
						$x .= '<input id="Score_'.$EntryIdx.'" name="Score['.$EntryIdx.']" class="textbox_num" value="'.$RequirementInfo[0]['Score'].'">'."\n";
						$x .= $this->Get_Form_Warning_Msg("WarnEmptyScoreRequired_$EntryIdx",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputScore'],"WarnMsg");
						$x .= $this->Get_Form_Warning_Msg("WarnNumberScoreRequired_$EntryIdx",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputNumber'],"WarnMsg");
					$x.= '</td>'."\n";
				$x.= '</tr>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<td class="field_title">'.$this->RequiredSymbol().$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryIncluded'].'</td>'."\n";
					$x.= '<td>'."\n";
						$CatNameLang = Get_Lang_Selection("CategoryNameEn", "CategoryNameCh");
						if(sizeof($CategoryArr))
						{
							foreach($CategoryArr as $thisCategory)
							{
								$isChecked = in_array($thisCategory['CategoryID'], (array)$CategoryIDArr);
								$x .= '<div>'.$this->Get_Checkbox("CategoryID_".$EntryIdx."_".$thisCategory['CategoryID'], "CategoryID[$EntryIdx][]", $thisCategory['CategoryID'], $isChecked, "CategorySelect_".$EntryIdx, $thisCategory[$CatNameLang], "").'</div>'."\n";
							}
						}
						else
						{
							$x .= $Lang['General']['NoRecordAtThisMoment'];
						}
						$x .= $this->Get_Form_Warning_Msg("WarnEmptyCategory_$EntryIdx",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectAtLeastOneCategory'],"WarnMsg");
						$x .= $this->Get_Form_Warning_Msg("WarnDuplicatedRequirement_$EntryIdx",$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['DuplicatedRequirement'],"WarnMsg");
					$x.= '</td>'."\n";
				$x.= '</tr>'."\n";
				$x.= '<tr>'."\n";
					$x.= '<td colspan="2" style="text-align:right;">'.$DeleteRequirementBtn.'</td>'."\n";
				$x.= '</tr>'."\n";
			$x.= '</tbody>'."\n";
		$x.= '</table>'."\n";
		$x .= '<input type="hidden" name="EntryIdx[]" class="EntryIndex" value="'.$EntryIdx.'">'."\n";
		$x.= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Poppular_Award_Scheme_Display_Table($StudentSubmit=''){
		global $Lang;
		
		# 2013-02-06 add for customization						
			$x = $this->Get_Form_Sub_Title_v30($Lang['ReadingGarden']['ReadingAwardScheme']);
			# For Student Submit
			//$StudentSubmit = 1;
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'].'</td>'."\n";
					$x .= '<td>'."\n";	
						$x .= $this->Get_Popular_Award_Scheme_Categoty_Selection("PopularAwardSchemeAddLayerCategoryID", "", 0, 1, "", "js_Reload_Popular_Award_Scheme_Add_Layer_Category($StudentSubmit)", 1, $StudentSubmit)."\n"; //  js_Reload_Popular_Award_Scheme_Add_Layer_Category handled in reading_scheme.js
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<div id="PopularAwardSchemeItemSelection">'."\n";
			$x .= '</div>'."\n";	
		return $x;
	}
		
}

?>