<?php

/* this is a base class */

class bar_base
{
	function bar_base(){}

	function set_key( $text, $size )
	{
		$this->text = $text;
		$tmp = 'font-size';
		$this->$tmp = $size;
	}

	function set_values( $v )
	{
		$this->values = $v;		
	}
	
	function append_value( $v )
	{
		$this->values[] = $v;		
	}
	
	function set_colour( $colour )
	{
		$this->colour = $colour;	
	}

	function set_alpha( $alpha )
	{
		$this->alpha = $alpha;	
	}
	
	function set_tooltip( $tip )
	{
		$this->tip = $tip;	
	}
	
	# added by adam 2009-03-03 
	function set_visible( $visible )
	{
		$this->visible = $visible?1:0;	
	}	
	
	function set_id( $id )
	{
		$this->id = $id;	
	}
	
	//whether the bar can be click to expand to sub bar chart 
	function set_on_click( $text )
	{
		$tmp = 'on-click';
		$this->$tmp = $text;
	}	
	
	function set_expandable( $e )
	{
		$this->expandable = $e;
	}			
}

