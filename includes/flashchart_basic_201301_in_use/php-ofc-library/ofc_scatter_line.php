<?php

class scatter_line
{
	function scatter_line( $colour, $dot_size )
	{
		$this->type      = "scatter_line";
		$this->set_colour( $colour );
		$this->set_dot_size( $dot_size );
	}
	
	function set_colour( $colour )
	{
		$this->colour = $colour;
	}

	function set_dot_size( $dot_size )
	{
		$tmp = 'dot-size';
		$this->$tmp = $dot_size;
	}
	
	function set_values( $values )
	{
		$this->values = $values;
	}
	
	# added by adam 2009-03-03 
	function set_visible( $visible )
	{
		$this->visible = $visible?1:0;
	}	
	
	function set_id( $id )
	{
		$this->id = $id;	
	}	
	
	function set_on_click( $text )
	{
		$tmp = 'on-click';
		$this->$tmp = $text;
	}	
}