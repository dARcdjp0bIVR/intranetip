<?php
## Using by : 

################### Change Log
#
#   Date:   2020-11-02 Henry
#           - modified returnAvailableItems(), fix the access right checking [Case#L200379]
#
#   Date:   2020-09-07 Tommy
#           - modified returnAvailableItems(), fix getting null item from INTRANET_GROUPRESOURCE
#
#   Date:   2019-05-13 Cameron
#           - fix potential sql injection problem by enclosing var with apostrophe
#
#	Date:	2017-01-10	Villa
#			modified manipulateFile fix php 5.4 split
#
#	Date:	2012-01-06	YatWoon
#			update Archive_Booking_Record(), fixed cannot remove archived record in INTRANET_BOOKING_RECORD [Case#2012-0106-0914-40132]
#
#	Date:	2011-12-14	YatWoon
#			update returnDayRecords(), fixed filte problem [Case#2011-1214-0937-18066]
#
#	Date:	2011-03-18	YatWoon
#			add function NeedUpdatePeriodicRecordStatus()
#			update function displayPeriodicTable()
#
#	Date:	2011-03-10	YatWoon
#			update displayMySingleRecords(), incorrect query syntax
#
#	Date:	2011-03-09	Yuen
#			improved returnReservedRecords() for better performance in standard mode
#
#	Date:	2011-03-04	Kenneth Chung
#			improve sql performance in displayMySingleRecords() 
#
#	Date:	2011-02-10	YatWoon
#			update returnReservedRecords(), fixed: incorrect mysql sql $cond ($ts+...)
#
##################################

if (!defined("LIBRB_DEFINED"))                     // Preprocessor directive
{
define("LIBRB_DEFINED", true);

$rb_image_path = "$image_path/resourcesbooking";
class librb extends libdb {

     var $UserID;
     var $ResourceID;
     var $AppliedUserID;
     var $BookingDate;
     var $PeriodicID;
     var $Remark;
     var $TimeSlot;
     var $RecordStatus;
     var $TimeApplied;
     var $LastAction;
     var $no_msg;

     ###############################################################################

     function librb(){
          global $UserID,$i_no_record_exists_msg;
          $this->libdb();
          $this->UserID = $UserID;
          $this->no_msg = $i_no_record_exists_msg;
     }

     function pageTitle(){
         # change page web title
         $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
         $js.= 'document.title="eClass Resources Booking";'."\n";
         $js.= '</script>'."\n";

         echo $js;
     }

     function returnSingleEditDeleteLink ($bid)
     {
              global $image_path;
              return "<A HREF=/home/eService/ResourcesBooking/edit_single.php?bid=$bid><img src=\"$image_path/icon_edit.gif\" border=0></A> <a HREF=/home/eService/ResourcesBooking/remove_single.php?bid=$bid onClick=\"if(confirm(globalAlertMsg3)){return true;}else{return false;}\"><img border=0 src=\"$image_path/icon_erase.gif\"></a>";
     }

     function returnPeriodicEditDeleteLink ($pid)
     {
              global $image_path;
              return "<A HREF=/home/eService/ResourcesBooking/edit_periodic.php?pid=$pid><img src=\"$image_path/icon_edit.gif\" border=0></A> <a HREF=/home/eService/ResourcesBooking/remove_periodic.php?pid=$pid onClick=\"if(confirm(globalAlertMsg3)){return true;}else{return false;}\"><img border=0 src=\"$image_path/icon_erase.gif\"></a>";
     }

     function returnSlotsString($slots, $batchID,$delimiter="")
     {
              $sql = "SELECT Title FROM INTRANET_SLOT WHERE BatchID = '$batchID' AND SlotSeq IN ($slots) ORDER BY SlotSeq ASC";
              $result = $this->returnVector($sql);
              if ($delimiter=="") $delimiter =" <br>";
              return implode($delimiter,$result);
     }

     function returnTypeString ($recordType , $typeValues)
     {
              global $i_BookingType_EveryDay,$i_BookingType_EverySeparate,$i_BookingType_Days,$i_BookingType_Every,$i_Booking_WeekDays;
              global $intranet_root,$i_DayType0,$i_DayType1,$i_DayType2,$i_DayType3;
              $result = "";
              switch ($recordType)
              {
                      case 0:
                           $result = "$i_BookingType_EveryDay";
                           break;
                      case 1:
                           $result = "$i_BookingType_EverySeparate $typeValues $i_BookingType_Days";
                           break;
                      case 2:
                           $result = "$i_BookingType_Every ".$i_DayType0[$typeValues];
                           break;
                      case 3:
                      /*
                           $cid = get_file_content("$intranet_root/file/cycle.txt");
                           if ($cid == "" || $cid == 0)
                               return "Error occurs";
                           if ($cid <= 3 || $cid == 10)
                               $target = ${"i_DayType1"};
                           else if ($cid <= 6 || $cid == 11)
                               $target = ${"i_DayType2"};
                           else
                               $target = ${"i_DayType3"};
                           $result = "$i_BookingType_Every ".$target[$typeValues];
                           */
                           break;
              }
              return $result;
     }

     function returnGroupIDs(){
          $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$this->UserID."'";
          return $this->db_sub_select($sql);
     }

     function retrieveBooking($bid)
     {
              $sql = "SELECT ResourceID, UserID, BookingDate, PeriodicBookingID, Remark, TimeSlot, RecordStatus, TimeApplied, LastAction FROM INTRANET_BOOKING_RECORD WHERE BookingID = '$bid'";
              $result = $this->returnArray($sql,9);
              if (sizeof($result)!=0)
              {
                  $this->ResourceID = $result[0][0];
                  $this->AppliedUserID = $result[0][1];
                  $this->BookingDate = $result[0][2];
                  $this->PeriodicID = $result[0][3];
                  $this->Remark = $result[0][4];
                  $this->TimeSlot = $result[0][5];
                  $this->RecordStatus = $result[0][6];
                  $this->TimeApplied = $result[0][7];
                  $this->LastAction = $result[0][8];
              }
     }

     function retrieveBookingBatch()
     {
              $bdate = $this->BookingDate;
              $rid = $this->ResourceID;
              $status = $this->RecordStatus;
              $applied = $this->TimeApplied;
              $u_applied = $this->AppliedUserID;
              $subquery = "SELECT TimeSlot FROM INTRANET_BOOKING_RECORD WHERE UserID = '$u_applied' AND BookingDate = '$bdate' AND ResourceID = '$rid' AND RecordStatus = '$status' AND TimeApplied = '$applied' order by TimeSlot";
              $sub_result = $this->returnVector($subquery);
              return $sub_result;
     }

     function retrieveBookingBatchIDs()
     {
              $bdate = $this->BookingDate;
              $rid = $this->ResourceID;
              $status = $this->RecordStatus;
              $applied = $this->TimeApplied;
              $u_applied = $this->AppliedUserID;
              $subquery = "SELECT BookingID FROM INTRANET_BOOKING_RECORD WHERE UserID = '$u_applied' AND BookingDate = '$bdate' AND ResourceID = '$rid' AND RecordStatus = '$status' AND TimeApplied = '$applied' order by TimeSlot";
              $sub_result = $this->returnVector($subquery);
              return $sub_result;
     }

     function returnAvailableItems(){
           $sql = "SELECT a.ResourceID, a.ResourceCode, a.ResourceCategory, a.title,a.Attachment
           FROM INTRANET_RESOURCE AS a LEFT OUTER JOIN INTRANET_GROUPRESOURCE AS b ON a.ResourceID = b.ResourceID WHERE a.RecordStatus = '1'
           AND (b.GroupID IN (".$this->returnGroupIDs().") OR  b.GroupID IS NULL)
           GROUP BY a.ResourceID
           ORDER BY a.ResourceCategory, a.ResourceCode";
//          $sql = "SELECT a.ResourceID, a.ResourceCode, a.ResourceCategory, a.title,a.Attachment
//          FROM INTRANET_RESOURCE AS a 
//          LEFT OUTER JOIN INTRANET_GROUPRESOURCE AS b ON (a.ResourceID = b.ResourceID AND (b.GroupID IN (".$this->returnGroupIDs().") OR  b.GroupID IS NULL))
//          WHERE a.RecordStatus = '1'
//          GROUP BY a.ResourceID
//          ORDER BY a.ResourceCategory, a.ResourceCode";
          return $this->returnArray($sql,5);
     }


     function getLargeTitleBar ($twords,$awords="",$toolbar="")
     {
              global $rb_image_path;
              $arrow = "<img src='$rb_image_path/arrow.gif' width='9' height='15'>";
              if ($toolbar == "")
              {
                  $tool_width = "100";
                  $main_width = "569";
              }
              else
              {
                  $tool_width = "400";
                  $main_width = "269";
              }
              $x = "
                    <table width=729 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/framebg.gif'>
                    <tr>
                    <td colspan=4><img src='$rb_image_path/frametop.gif'></td>
                    </tr>
                    <tr>
                    <td width=30>
                    <p><br>
                    </p>
                    </td>
                    <td width=$main_width class=h1 align=left valign=top><img src='$rb_image_path/sq_darkblue.gif'>
                    $twords";
               if ($awords != "")
               {
                   $x .= " $arrow $awords";
               }
/*
*/

               $x .= "</td>
                        <td width=$tool_width align=right class=functionlink valign=top>$toolbar</td>
                      <td width=30>&nbsp;</td>
                      </tr>
                      </table>
                      ";
               return $x;
     }

     function getIndexTitleBar ($twords,$awords="",$toolbar="")
     {
              global $rb_image_path;
              global $i_Booking_indexTitle;
              $arrow = "<img src='$rb_image_path/arrow.gif' width='9' height='15'>";
              $x = "
                    <table width=517 border=0 cellspacing=0 cellpadding=0 background='/images/index/resources_tb2.gif'>
                    <tr>
                    <td colspan=4><a href=resource/>$i_Booking_indexTitle</a></td>
                    </tr>
                    <tr>
                    <td width=15>
                    <p><br><br>
                    </p>
                    </td>
                    <td class=title_head valign=top><img src='$rb_image_path/sq_darkblue.gif'>
                    $twords";
               if ($awords != "")
               {
                   $x .= " $arrow $awords";
               }
               $x .= "</td>
                      <td class=td_right_middle valign=top>$toolbar</td>
                      <td width=15>&nbsp;</td>
                      </tr>
                      </table>
                      ";
               return $x;
     }

     function getTopPaper()
     {
              global $rb_image_path;
              $x = "<table width=729 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/framebg.gif'>
                     <tr>
                     <td width=30>&nbsp;</td>
                     <td align=left style=\"vertical-align:bottom\" valign=bottom><img src='$rb_image_path/framepapertop.gif'></td>
                     </tr>
                     </table>";
              return $x;
     }

     function getLargeEndPaper()
     {
              global $rb_image_path;
              $x = "<table width=729 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/framebg.gif'>
                     <tr>
                     <td>
                     <p><img src='$rb_image_path/framepaperend.gif'></p>
                     <p>&nbsp;</p>
                     </td>
                     </tr>
                     </table>";
              return $x;
     }

     function getLargeEndBoard()
     {
              global $rb_image_path;
              $x = "<table width=709 border=0 cellspacing=0 cellpadding=0>
                     <tr>
                     <td><img src='$rb_image_path/framebottom.gif'></td>
                     </tr>
                     </table>";
              return $x;
     }

     function getTab($text)
     {
              global $rb_image_path;
              $x = "<table width=729 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/framebg.gif'>
                     <tr>
                     <td width=30>&nbsp;</td>
                     <td width=5><img src='$rb_image_path/tablh.gif' width=5 height=30></td>
                     <td width=150 align=center valign=middle style=\"background-image: url('$rb_image_path/tabcell.gif')\" class=h1>$text</td>
                     <td width=5><img src='$rb_image_path/tabrh.gif' width=5 height=30></td>
                     <td style=\"vertical-align:bottom\" align=left width=539><img src='$rb_image_path/framepaper.gif'></td>
                     </tr>
                     </table>
                     ";
              return $x;

     }

     function getSmallTab($text)
     {
              $x = "<table width=517 border=0 cellspacing=0 cellpadding=0 background='/images/index/resources_tb2.gif'>
                     <tr>
                     <td width=6>&nbsp;</td>
                     <td width=5><img src='/images/index/resources_tablh.gif' width=5></td>
                     <td width=150 align=center bgcolor=#E8FBFC class=title_head>$text</td>
                     <td width=5><img src='/images/index/resources_tabrh.gif' width=5></td>
                     <td width=351 style=\"vertical-align:bottom\" align=left></td>
                     </tr>
                     </table>
                     ";
              return $x;

     }

     function displayIndex()
     {
              global $i_BookingMyRecords,$i_BookingNew;
              if ($this->hasRightToReserve($UserID))
              {
                  $toolbar = "<a href=resource/add.php><img border=0 src=/images/icon_new.gif><span class=functionlink>$i_BookingNew</span></a>";
              }
              else
              {
                  $toolbar = "<img border=0 src=/images/icon_new.gif><span STYLE=\"text-decoration: line-through\">$i_BookingNew</span>";
              }
              $x .= $this->getIndexTitleBar($i_BookingMyRecords,"",$toolbar);
              $x .= $this->displayIndexSingle();
              $x .= $this->displayIndexPeriodic();
              return $x;
     }

     function displayIndexSingle()
     {
              global $rb_image_path, $i_BookingSingle;
              $x = $this->getSmallTab("<img src='$rb_image_path/icon_single.gif'><font color=#000099>$i_BookingSingle</font>");
              $x .= $this->displayIndexSingleRecords();
              $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0>
                       <tr>
                         <td><img src=/images/index/resources_bottom2.gif></td>
                       </tr>
                     </table>\n";
              return $x;
     }

     function displayIndexPeriodic()
     {
              global $rb_image_path, $i_BookingPeriodic;
              $x = $this->getSmallTab("<img src='$rb_image_path/icon_periodic.gif'><font color=#000099>$i_BookingPeriodic</font>");
              $x .= $this->displayIndexPeriodicRecords();
              $x .= "<table width=517 border=0 cellspacing=0 cellpadding=0>
              <tr>
                <td><img src=/images/index/resources_bottom.gif></td>
              </tr>
            </table>\n";
              return $x;
     }

     function displayIndexPeriodicRecords()
     {
              global $rb_image_path;
              global $i_BookingItem,$i_BookingStartDate,$i_BookingEndDate,$i_BookingTimeSlots,$i_BookingPeriodicType,$i_BookingApplied,$i_BookingRemark;
              global $i_Booking_Period;
              $field_names = "a.PeriodicBookingID, b.ResourceID, b.Title, CONCAT(a.BookingStartDate, ' - ',a.BookingEndDate), a.TimeSlots, a.TimeApplied,a.RecordStatus, b.TimeSlotBatchID, a.RecordType, a.TypeValues, a.Remark, a.BookingStartDate, a.BookingEndDate";
              $dbtables = "INTRANET_PERIODIC_BOOKING as a, INTRANET_RESOURCE as b";
              $join_conds = "a.ResourceID = b.ResourceID";
              $conds = "a.BookingEndDate >= CURDATE() AND a.RecordStatus IN (2,3,4) AND a.UserID = '".$this->UserID."'";
              $order_str = "a.BookingStartDate DESC, b.Title, a.RecordStatus ASC";
              $sql = "SELECT $field_names FROM $dbtables WHERE $join_conds AND $conds ORDER BY $order_str";

              $result = $this->returnArray($sql,13);
              $x .= "<table width=517 border=0 cellspacing=0 cellpadding=5 background='/images/index/resources_tb.gif' class=h1>
                     <tr>
                     <td width=5>&nbsp;</td>
                     <td width=25 align=center valign=middle>#</td>
                     <td width=107>$i_BookingItem</td>
                     <td width=5><img src=/images/spacer.gif width=5 height=5></td>
                     <td width=90>$i_Booking_Period</td>
                     <td width=75>$i_BookingPeriodicType</td>
                     <td width=5><img src=/images/spacer.gif width=5 height=5></td>
                     <td width=90>$i_BookingTimeSlots</td>
                     <td width=90>$i_BookingApplied</td>
                     <td width=25>&nbsp;</td>
                     </tr>
                     <tr align=center>
                     <td colspan=10><img src='/images/index/line.gif'></td>
                     </tr></table>";
              $x .= "<table width=517 border=0 cellspacing=0 cellpadding=5 background='/images/index/resources_tb.gif' class=body>\n";

                       $count = 0;
                       $slots_str = "";
                  for ($i=0; $i<sizeof($result); $i++)
                  {
                       list($pid,$rid,$item,$period,$slots,$applied,$status,$batchID,$recordType,$typeValues,$remark, $booking_start, $booking_end) = $result[$i];
                       if ($status == 0 || $status == 4)
                       {
                           $link = $this->returnPeriodicEditDeleteLink($pid);
                       }
                       else
                       {
                           $link = "";
                       }
                       /*
                       switch ($status)
                       {
                               case 0: $flag = ""; break;
                               case 2:
                               case 3:
                               case 4: $flag = "<font color=#000099> *</font>"; break;
                               case 5: $flag = "<font color=#FF0000> *</font>"; break;
                       }
                       */
                       $remark = str_replace("\n","<br>",$remark);
                       $remark = str_replace("\r","",$remark);

                       //$item = intranet_wordwrap ($item,20,"\n",1);
                       $item = $item;

//                       $toolTipShown = ($remark!=""? "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip','<table cellpadding=1><tr><td class=tipborder><table><tr><td class=tipbg>Remark: $remark</td></tr></table></td></tr></table>')\"":"");
                       $toolTipShown = ($remark!=""? "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip',makeTip('$i_BookingRemark: $remark'))\"":"");
                       $item = "<a $toolTipShown style='font-family: \"Verdana\", \"Arial\", \"Helvetica\", \"sans-serif\", \"�s�ө���\"; font-size: 13px; color: #006600; text-decoration: underline' href=javascript:newWindow('/home/resource/view_periodic.php?pid=$pid',1)>$item</a> $flag";

                       if ($recordType != 3)
                           $type_str = $this->returnTypeString($recordType,$typeValues);
                       else
                       {
                           $a_lcycleperiods = new libcycleperiods();
                           $type_str = $a_lcycleperiods->returnCriteriaString($typeValues,$booking_start,$booking_end);
                       }
                       //$slots_str = intranet_wordwrap($this->returnSlotsString ($slots, $batchID),14,"\n",1);
                       $slots_str = $this->returnSlotsString ($slots, $batchID);

                       $count = $i+1;
                       $x .= "<tr><td width=5>&nbsp;</td>
                               <td width=25 align=center valign=top>$count</td>
                               <td width=107 align=left valign=top><span class=bodylink>$item</span><br>
                               $link</td>
                               <td width=5><img src=/images/spacer.gif width=5 height=5></td>
                               <td width=90 align=left valign=top>$period</td>
                               <td width=75 align=left valign=top>$type_str</td>
                               <td width=5><img src=/images/spacer.gif width=5 height=5></td>
                               <td width=90 align=left valign=top>$slots_str</td>
                               <td width=90 align=left valign=top>$applied</td>
                               <td width=25>&nbsp;</td>
                               </tr>
                               <tr align=center>
                               <td colspan=10><img src='/images/index/line.gif'></td>
                               </tr>\n";
                  }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=10 align=center>".$this->no_msg."</td></tr>\n";
                  $x .= "<tr align=center>
                               <td colspan=10><img src='/images/index/line.gif'></td>
                               </tr>\n";
              }
              $x .= "</table>";
              return $x;
     }

     function displayIndexSingleRecords()
     {
              global $rb_image_path;
              global $i_BookingNewSingle,$i_BookingItem,$i_BookingDate,$i_BookingTimeSlots,$i_BookingApplied,$i_BookingRemark;

              $field_names = "a.BookingID, b.ResourceID, b.Title, a.BookingDate, c.Title, a.TimeApplied,a.RecordStatus,a.Remark";
//              $dbtables = "INTRANET_BOOKING_RECORD as a, INTRANET_RESOURCE as b, INTRANET_SLOT as c, INTRANET_SLOT_BATCH as d";
//              $join_conds = "a.ResourceID = b.ResourceID AND b.TimeSlotBatchID = d.BatchID AND c.BatchID = d.BatchID";
              $dbtables = "INTRANET_BOOKING_RECORD as a, INTRANET_RESOURCE as b, INTRANET_SLOT as c";
              $join_conds = "a.ResourceID = b.ResourceID AND b.TimeSlotBatchID = c.BatchID";
              $conds = "a.PeriodicBookingID is null AND a.BookingDate >= CURDATE() AND c.SlotSeq = a.TimeSlot AND a.RecordStatus IN (2,3,4) AND a.UserID = '".$this->UserID."'";
              $order_str = "a.BookingDate DESC, b.Title, a.RecordStatus, a.TimeSlot ASC";
              $sql = "SELECT $field_names FROM $dbtables WHERE $join_conds AND $conds ORDER BY $order_str";
              $result = $this->returnArray($sql,8);
              $x = "<table width=517 border=0 cellspacing=0 cellpadding=5 background='/images/index/resources_tb.gif' class=h1>
                     <tr>
                     <td width=5 height=30>&nbsp;</td>
                     <td width=25 align=center valign=middle>#</td>
                     <td width=192>$i_BookingItem</td>
                     <td width=90>$i_BookingDate</td>
                     <td width=90>$i_BookingTimeSlots</td>
                     <td width=90>$i_BookingApplied</td>
                     <td width=25>&nbsp;</td>
                     </tr>
                     <tr align=center>
                     <td colspan=7><img src='/images/index/line.gif'></td>
                     </tr></table>";
              $x .= "<table width=517 border=0 cellspacing=0 cellpadding=5 background='/images/index/resources_tb.gif' class=body>\n";

                       $count = 0;
                       $slots_str = "";
                  for ($i=0; $i<sizeof($result); $i++)
                  {
                       list($bid,$rid,$item,$date,$slots,$applied,$status,$remark) = $result[$i];
                       if ($date == $result[$i+1][3] && $rid == $result[$i+1][1] && $applied == $result[$i+1][5] && $status == $result[$i+1][6])
                       {
                           $slots_str .= "$slots<br>";
                           continue;
                       }
                       $slots_str .= "$slots<br>";
                       //$slots_str = intranet_wordwrap ($slots_str,14,"\n",1);
                       $slots_str = $slots_str;
                       $count++;
                       if ($status == 0 || $status == 4)
                       {
                           $link = $this->returnSingleEditDeleteLink($bid);
                       }
                       else
                       {
                           $link = "";
                       }/*
                       switch ($status)
                       {
                               case 0: $flag = ""; break;
                               case 2:
                               case 3:
                               case 4: $flag = "<font color=#000099> *</font>"; break;
                               case 5: $flag = "<font color=#FF0000> *</font>"; break;
                       }*/
                       //$remark = convert2jsvariable ($remark);
                       $remark = str_replace("\n","<br>",$remark);
                       $remark = str_replace("\r","",$remark);

                       //$item = intranet_wordwrap ($item,30,"\n",1);
                       $item = $item;

                       $toolTipShown = ($remark!=""? "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip',makeTip('$i_BookingRemark: $remark'))\"":"");
//                       $toolTipShown = ($remark!=""? "onMouseOver=\"showMenu('ToolMenu','<table cellpadding=1><tr><td class=tipborder><table><tr><td class=tipbg>Remark: $remark</td></tr></table></td></tr></table>')\" onMouseOut=\"hideMenu('ToolMenu')\"":"");
                       $item = "<a $toolTipShown style='font-family: \"Verdana\", \"Arial\", \"Helvetica\", \"sans-serif\", \"�s�ө���\"; font-size: 13px; color: #006600; text-decoration: underline' href=javascript:newWindow('/home/resource/view_single.php?bid=$bid',1)>$item</a> $flag";

                       $x .= "<tr><td width=5 height=30>&nbsp;</td>
                               <td width=25 align=center valign=top>$count</td>
                               <td width=192 align=left valign=top><span class=bodylink>$item</span><br>
                               $link</td>
                               <td width=90 align=left valign=top>$date</td>
                               <td width=90 align=left valign=top>$slots_str</td>
                               <td width=90 align=left valign=top>$applied</td>
                               <td width=25>&nbsp;</td>
                               </tr>
                               <tr align=center>
                               <td colspan=7><img src='/images/index/line.gif'></td>
                               </tr>\n";
                       $slots_str = "";
                  }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=7 align=center>".$this->no_msg."</td></tr>\n";
                  $x .= "<tr align=center>
                           <td colspan=7><img src='/images/index/line.gif'></td>
                         </tr>\n";
              }
              $x .= "</table>";
              //              $x .= $this->getLargeEndPaper();
              return $x;
     }

     function displaySingleBookings()
     {
              global $rb_image_path, $i_BookingSingle;
              $x = $this->getTab("<img src='$rb_image_path/icon_single.gif'><font color=#000099>$i_BookingSingle</font>");
              $x .= $this->displayMySingleRecords();
              return $x;
     }

     function displayMySingleRecords()
     {
     		$sql = "select
     						a.BookingID, b.ResourceID, b.Title, a.BookingDate, c.Title, a.TimeApplied,a.RecordStatus,a.Remark
     					from 
     						INTRANET_BOOKING_RECORD as a 
     						INNER JOIN 
     						INTRANET_RESOURCE as b 
     						on (
     							a.UserID = '".$this->UserID."' 
     							AND a.PeriodicBookingID is null 
     							AND a.RecordStatus IN (0,2,3,4,5) 
     							AND  a.BookingDate >= CURDATE() 
     							AND a.UserCleared IS NULL 
     							AND a.ResourceID = b.ResourceID) 
     						INNER JOIN 
     						INTRANET_SLOT as c 
     						ON (a.TimeSlot = c.SlotSeq AND b.TimeSlotBatchID = c.BatchID)
              order by 
              	a.BookingDate DESC, b.Title, a.RecordStatus, a.TimeSlot ASC";

       return $this->displaySingleTable($sql);
     }

     function displaySingleTable($sql)
     {
              global $rb_image_path;
              global $i_BookingNewSingle,$i_BookingItem,$i_BookingDate,$i_BookingTimeSlots,$i_BookingApplied,$i_BookingRecordStatus,$i_BookingStatusImageArray,$i_BookingRemark;

              $result = $this->returnArray($sql,8);
              $x = "<table width=729 border=0 cellspacing=0 cellpadding=5 background='$rb_image_path/framebglightblue.gif' class=h1>
                     <tr>
                     <td width=30 height=30>&nbsp;</td>
                     <td width=30 align=center valign=middle>#</td>
                     <td width=364>$i_BookingItem</td>
                     <td width=90>$i_BookingDate</td>
                     <td width=90>$i_BookingTimeSlots</td>
                     <td width=90>$i_BookingRecordStatus</td>
                     <td width=35>&nbsp;</td>
                     </tr>
                     <tr align=center>
                     <td colspan=7><img src='/images/line.gif' width=616 height=1></td>
                     </tr></table>";
              $x .= "<table width=729 border=0 cellspacing=0 cellpadding=5 background='$rb_image_path/framebglightblue.gif' class=body>\n";

                       $count = 0;
                       $slots_str = "";
                  for ($i=0; $i<sizeof($result); $i++)
                  {
                       list($bid,$rid,$item,$date,$slots,$applied,$status,$remark) = $result[$i];
                       //$item = intranet_wordwrap ($item,30,"\n",1);
                       $item = $item;
                       if ($date == $result[$i+1][3] && $rid == $result[$i+1][1] && $applied == $result[$i+1][5] && $status == $result[$i+1][6])
                       {
                           //intranet_wordwrap ($slots,10,"\n",1);
                           $slots_str .= "$slots<br>";
                           continue;
                       }
                       $slots_str .= "$slots<br>";
                       $count++;
                       if ($status == 0 || $status == 4)
                       {
                           $link = $this->returnSingleEditDeleteLink($bid);
                       }
                       else
                       {
                           $link = "";
                       }
                       /*
                       switch ($status)
                       {
                               case 0: $flag = ""; break;
                               case 2:
                               case 3:
                               case 4: $flag = "<font color=#000099> *</font>"; break;
                               case 5: $flag = "<font color=#FF0000> *</font>"; break;
                       }
                       */
                       //$remark = convert2jsvariable ($remark);
                       $remark = str_replace("\n","<br>",$remark);
                       $remark = str_replace("\r","",$remark);
                       $remark = addslashes($remark);

                       $toolTipShown = ($remark!=""? "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip',makeTip('$i_BookingRemark: $remark'))\"":"");
//                       $toolTipShown = ($remark!=""? "onMouseOver=\"showMenu('ToolMenu','<table cellpadding=1><tr><td class=tipborder><table><tr><td class=tipbg>Remark: $remark</td></tr></table></td></tr></table>')\" onMouseOut=\"hideMenu('ToolMenu')\"":"");
                       $item = "<a $toolTipShown style='font-family: \"Verdana\", \"Arial\", \"Helvetica\", \"sans-serif\", \"�s�ө���\"; font-size: 13px; color: #006600; text-decoration: underline' href=javascript:newWindow('/home/eService/ResourcesBooking/view_single.php?bid=$bid',1)>$item</a>";
                       $image_status = $i_BookingStatusImageArray[$status];

                       $x .= "<tr><td width=30 height=30>&nbsp;</td>
                               <td width=30 align=center valign=top>$count</td>
                               <td width=364 align=left valign=top><span class=bodylink>$item</span><br>
                               $link</td>
                               <td width=90 align=left valign=top>$date</td>
                               <td width=90 align=left valign=top>$slots_str</td>
                               <td width=90 align=left valign=top>$image_status</td>
                               <td width=35>&nbsp;</td>
                               </tr>
                               <tr align=center>
                               <td colspan=7><img src='/images/line.gif' width=616 height=1></td>
                               </tr>\n";
                       $slots_str = "";
                  }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=7 align=center>".$this->no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              $x .= $this->getLargeEndPaper();
              return $x;
     }

     function displayPeriodicBookings()
     {
              global $rb_image_path, $i_BookingPeriodic;
              $x = $this->getTab("<img src='$rb_image_path/icon_periodic.gif'><font color=#000099>$i_BookingPeriodic</font>");
              $x .= $this->displayMyPeriodicRecords();
              return $x;
     }

     function displayMyPeriodicRecords()
     {
              $field_names = "a.PeriodicBookingID, b.ResourceID, b.Title, a.BookingStartDate, a.BookingEndDate, a.TimeSlots, a.TimeApplied,a.RecordStatus, b.TimeSlotBatchID, a.RecordType, a.TypeValues, a.Remark";
              $dbtables = "INTRANET_PERIODIC_BOOKING as a, INTRANET_RESOURCE as b";
              $join_conds = "a.ResourceID = b.ResourceID";
              $conds = "a.BookingEndDate >= CURDATE() AND a.RecordStatus IN (0,2,3,4,5) AND a.UserCleared IS NULL AND a.UserID = '".$this->UserID."'";
              $order_str = "a.BookingStartDate DESC, b.Title, a.RecordStatus ASC";
              $sql = "SELECT $field_names FROM $dbtables WHERE $join_conds AND $conds ORDER BY $order_str";
              
              return $this->displayPeriodicTable($sql);
     }

     function displayPeriodicTable($sql)
     {
              global $rb_image_path;
              global $i_BookingItem,$i_BookingStartDate,$i_BookingEndDate,$i_BookingTimeSlots,$i_BookingPeriodicType,$i_BookingApplied,$i_BookingRecordStatus,$i_BookingStatusImageArray,$i_BookingRemark;

              $result = $this->returnArray($sql,12);
              $x .= "<table width=729 border=0 cellspacing=0 cellpadding=5 background='$rb_image_path/framebglightblue.gif' class=h1>
                     <tr>
                     <td width=30 height=30>&nbsp;</td>
                     <td width=30 align=center valign=middle>#</td>
                     <td width=194>$i_BookingItem</td>
                     <td width=90>$i_BookingStartDate</td>
                     <td width=90>$i_BookingEndDate</td>
                     <td width=80>$i_BookingPeriodicType</td>
                     <td width=90>$i_BookingTimeSlots</td>
                     <td width=90>$i_BookingRecordStatus</td>
                     <td width=35>&nbsp;</td>
                     </tr>
                     <tr align=center>
                     <td colspan=9><img src='/images/line.gif' width=616 height=1></td>
                     </tr></table>";
              $x .= "<table width=729 border=0 cellspacing=0 cellpadding=5 background='$rb_image_path/framebglightblue.gif' class=body>\n";

                       $count = 0;
                       $slots_str = "";
                  for ($i=0; $i<sizeof($result); $i++)
                  {
                       list($pid,$rid,$item,$start,$end,$slots,$applied,$status,$batchID,$recordType,$typeValues,$remark) = $result[$i];
                       //$item = intranet_wordwrap($item,15,"\n",1);
                       
                       #### Check need to change status to "Approved" or not (if no pending record)
                       if ($status==0)
                       {
	                       $new_status = $this->NeedUpdatePeriodicRecordStatus($pid);
	                       $status = $new_status ? $new_status : $status;
                       }
                       
                       $item = $item;
                       if ($status == 0 || $status == 4)
                       {
                           $link = $this->returnPeriodicEditDeleteLink($pid);
                       }
                       else
                       {
                           $link = "";
                       }
                       
                       $remark = str_replace("\n","<br>",$remark);
                       $remark = str_replace("\r","",$remark);
                       $remark = addslashes($remark);

//                       $toolTipShown = ($remark!=""? "onMouseOver=\"showMenu('ToolMenu','<table cellpadding=1><tr><td class=tipborder><table><tr><td class=tipbg>Remark: $remark</td></tr></table></td></tr></table>')\" onMouseOut=\"hideMenu('ToolMenu')\"":"");
                       $toolTipShown = ($remark!=""? "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip',makeTip('$i_BookingRemark: $remark'))\"":"");
                       $item = "<a $toolTipShown style='font-family: \"Verdana\", \"Arial\", \"Helvetica\", \"sans-serif\", \"�s�ө���\"; font-size: 13px; color: #006600; text-decoration: underline' href=javascript:newWindow('/home/eService/ResourcesBooking/view_periodic.php?pid=$pid',1)>$item</a>";
                       $image_status = $i_BookingStatusImageArray[$status];

                       if ($recordType != 3)
                           $type_str = $this->returnTypeString($recordType,$typeValues);
                       else
                       {
                           $a_lcycleperiods = new libcycleperiods();
                           $type_str = $a_lcycleperiods->returnCriteriaString($typeValues,$start,$end);
                       }
                       //$slots_str = intranet_wordwrap($this->returnSlotsString ($slots, $batchID),10,"\n",1);
                       $slots_str = $this->returnSlotsString ($slots, $batchID);

                       $count = $i+1;
                       $x .= "<tr><td width=30 height=30>&nbsp;</td>
                               <td width=30 align=center valign=top>$count</td>
                               <td width=194 align=left valign=top><span class=bodylink>$item</span><br>
                               $link</td>
                               <td width=90 align=left valign=top>$start</td>
                               <td width=90 align=left valign=top>$end</td>
                               <td width=80 align=left valign=top>$type_str</td>
                               <td width=90 align=left valign=top>$slots_str</td>
                               <td width=90 align=left valign=top>$image_status</td>
                               <td width=35>&nbsp;</td>
                               </tr>
                               <tr align=center>
                               <td colspan=9><img src='/images/line.gif' width=616 height=1></td>
                               </tr>\n";
                  }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=9 align=center>".$this->no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              $x .= $this->getLargeEndPaper();
              return $x;
     }
     
     function NeedUpdatePeriodicRecordStatus($pid)
     {
		$sql_tmp = "select RecordStatus from INTRANET_BOOKING_RECORD where PeriodicBookingID='$pid'";
		$result_tmp = $this->returnVector($sql_tmp);
		
		$hv_pending = 0;
		$hv_approve = 0;
		foreach($result_tmp as $k=>$d)
		{
			if($d==0) $hv_pending = 1;
			if($d==4) $hv_approve = 1;
		}
		
		if(!$hv_pending)
		{
			if($hv_approve)
				$new_status = 4;	# includes reserved => Approved
			else
				$new_status = 5;	# all rejected => Rejected
			
			# update periodic booking records status
			$sql = "update INTRANET_PERIODIC_BOOKING set RecordStatus='$new_status' where PeriodicBookingID='$pid'";
			$this->db_db_query($sql);
			
			return $new_status;
		}   
     }

     function undo_htmlspecialchars($string)
     {
              $string = str_replace("&amp;", "&", $string);
              $string = str_replace("&quot;", "\"", $string);
              $string = str_replace("&#039;", "'", $string);
              $string = str_replace("&lt;", "<", $string);
              $string = str_replace("&gt;", ">", $string);
              $string = str_replace("<form>", "&lt;form&gt;", $string);
              $string = str_replace("</form>", "&lt;/form&gt;", $string);
              $string = str_replace("<input", "&lt;input", $string);
              $string = str_replace("<textarea>", "&lt;textarea&gt;", $string);
              $string = str_replace("</textarea>", "&lt;/textarea&gt;", $string);
              return $string;
     }


     function displayAvailableItems()
     {
              global $rb_image_path;
              global $image_single_a,$image_single_b,$image_periodic_a,$image_periodic_b;
              global $i_BookingAddStep;
              $items = $this->returnAvailableItems();
              $steps_icon = getStepIcons(3,2,$i_BookingAddStep);
              $x .= "
                     <table width=729 border=0 cellspacing=0 cellpadding=10 background='$rb_image_path/framebglightblue.gif' class=h1>
                     <tr>
                     <td width=50>&nbsp;</td>
                     <td width=639>
                     ";
              $count = 0;
              for ($i=0; $i<sizeof($items); $i++)
              {
                   $num1 = $i*2;
                   $num2 = $num1 + 1;

                   list($rid,$rcode,$rcat,$title,$attach) = $items[$i];
                   //$rcat = addslashes(intranet_wordwrap($rcat,60,"\n",1));
                   //$rcode = addslashes(intranet_wordwrap($rcode, 8," ",1));
                   //$title = addslashes(intranet_wordwrap($title,15," ",1));
                   $rcat = addslashes($rcat);
                   $rcode = addslashes($rcode);
                   $title = addslashes($title);
                   $attach_str = ($attach==""? "":$this->manipulateFile($attach));
                   $layer[$count] .= "<tr><td width=80>$rcode</td><td width=158>$title $attach_str</td>";
                   $layer[$count] .= "<td width=90><a href=add_single.php?rid=$rid onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage(\\'Image$num1\\',\\'\\',\\'$rb_image_path/$image_single_b\\',1)\">";
                   $layer[$count] .= "<img name=\"Image$num1\" border=\"0\" src=\"$rb_image_path/$image_single_a\"></a></td>";
                   $layer[$count] .= "<td width=120><a href=add_periodic.php?rid=$rid onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage(\\'Image$num2\\',\\'\\',\\'$rb_image_path/$image_periodic_b\\',1)\">";
                   $layer[$count] .= "<img name=\"Image$num2\" border=\"0\" src=\"$rb_image_path/$image_periodic_a\"></a></td></tr>";
                   if ($items[$i+1][2] == $rcat)
                   {
                       continue;
                   }
                   else
                   {
                       $x .= "<p><img src=$rb_image_path/bullet.gif><a href=\"javascript:showMenu('ToolMenu',table_head+layer$count+table_tail, 260)\" onMouseMove='overhere()'> $rcat </a></p>";

                       $count++;
                       $layer[$count] = "";
                   }
              }
              $x .= "<SCRIPT LANGUAGE=JAVASCRIPT>\n";
              $x .= "var table_head = '<table width=448 border=0 cellspacing=0 cellpadding=0><tr><td><img src=$rb_image_path/itemtopbar.gif height=15></td></tr></table>';\n";
              $x .= "table_head += '<table width=448 border=0 cellspacing=0 cellpadding=0 background=$rb_image_path/itembg.gif><tr><td style=\"vertical-align:bottom\" width=429>$steps_icon</td></tr></table>';\n";
              $x .= "var table_tail = '<table width=448 border=0 cellspacing=0 cellpadding=0 background=$rb_image_path/itembg.gif><tr><td style=\"vertical-align:bottom\" width=429><img src=$rb_image_path/itembottom.gif></td><td width=19><a href=javascript:hideMenu(\"ToolMenu\")><img border=0 src=$rb_image_path/itemclose.gif></a></td></tr></table>';\n";
              for ($i=0; $i<sizeof($layer); $i++)
              {
                   $x .= "var layer$i = '<table width=448 border=0 cellspacing=0 cellpadding=5 class=body background=$rb_image_path/itembg.gif>".$layer[$i]."</table>';\r\n";
              }
              $x .= "</SCRIPT>";
              $x .= "</td><td width=50>&nbsp;</td></tr></table>";
              return $x;
     }

     function listTimeSlots($rid, $bdate)
     {
              global $rb_image_path, $i_BookingTimeSlots, $i_BookingStatus,$i_BookingAvailable,$i_BookingReserved,$i_label_SelectAll;
              global $intranet_session_language;
              $fields_name = "a.SlotID, a.Title, a.TimeRange, a.SlotSeq";
              $dbtables = "INTRANET_SLOT as a, INTRANET_RESOURCE as b";
              $conds = "a.BatchID = b.TimeSlotBatchID AND b.ResourceID = $rid";
              $sql = "SELECT $fields_name FROM $dbtables WHERE $conds ORDER BY a.SlotSeq ASC";
              $slots = $this->returnArray($sql,4);
/*
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldByLang("b.");

              $fields_name = "a.TimeSlot,CONCAT($username_field,IF (b.ClassNumber IS NOT NULL,CONCAT(' (',b.ClassName,'-',b.ClassNumber,')'),'')) ";
              $dbtables = "INTRANET_BOOKING_RECORD as a , INTRANET_USER as b";
              $join_conds = "a.UserID = b.UserID";
              $conds = "a.BookingDate = '$bdate' AND a.ResourceID = '$rid' AND a.RecordStatus IN (2,3,4)";
              $sql = "SELECT $fields_name FROM $dbtables WHERE $join_conds AND $conds ORDER BY a.TimeSlot ASC";
              $reserved = $this->returnArray($sql,2);

              $x .= "<table width=408 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/timeslotsbg2.gif' class=body>
                <tr>
                  <td align=center width=134 class=h1 style=\"background-image: url('$rb_image_path/timeslotstop1a.gif'); vertical-align: middle\">$i_BookingTimeSlots</td>
                  <td align=center width=200 style=\"background-image: url('$rb_image_path/timeslotstop1c.gif'); vertical-align: middle\" class=h1>$i_BookingStatus</td>
                  <td align=center width=74 class=h1 style=\"background-image: url('$rb_image_path/timeslotstop1b.gif'); vertical-align: middle\" class=h1>
                     $i_label_SelectAll<input type=checkbox onClick=(this.checked)?setChecked(1,document.add,'slots[]'):setChecked(0,document.add,'slots[]')>
                  </td>
                </tr>
                <tr valign=top>
                  <td align=center width=134>&nbsp;</td>
                  <td align=center width=200>&nbsp;</td>
                  <td align=center width=74>&nbsp;</td>
                </tr>";
              for ($i=0; $i<sizeof($slots); $i++)
              {
                   list ($sid, $title, $timerange,$seq) = $slots[$i];
                   //$title = intranet_wordwrap($title,12," ",1);
                   //$timerange = intranet_wordwrap($timerange,12," ",1);
                   $title = $title;
                   $timerange = $timerange;
                   $pos = $this->in_arrayField($seq, $reserved,0);

                   if ($pos != -1)
                   {
                       $color_str = "#FF0000";
                       //$name = intranet_wordwrap ($reserved[$pos][1],20," ",1);
                       $name = $reserved[$pos][1];
                       $text = "$i_BookingReserved $name";
                       $checkbox_str = "";
                   }
                   else
                   {
                       $color_str = "#000099";
                       $text = $i_BookingAvailable;
                       $checkbox_str = "<input type=checkbox name=slots[] value=$seq>";
                   }

                   $x .= "<tr valign=top>
                  <td align=center width=134>$title<br>$timerange</td>
                  <td align=center width=200><font color=\"$color_str\">$text</font><br>
                  </td>
                  <td align=center width=74>$checkbox_str</td>
                </tr>
                <tr valign=top>
                  <td align=center width=134>&nbsp;</td>
                  <td align=center width=200>&nbsp;</td>
                  <td align=center width=74>&nbsp;</td>
                </tr>";
              }
              $x .= "<tr valign=top>
                  <td align=center colspan=3><img src='$rb_image_path/timeslotsbottom2.gif' width=408 height=6>
                  </td></tr></table>\n";
              return $x;
     }

     function listEditTimeSlots($bid="")
     {
              global $rb_image_path, $i_BookingTimeSlots, $i_BookingStatus,$i_BookingAvailable,$i_BookingReserved,$i_BookingAppliedByU,$i_BookingStatusArray,$i_label_SelectAll;
              global $intranet_session_language;

              if ($bid != "") $this->retrieveBooking($bid);
              $bdate = $this->BookingDate;
              $rid = $this->ResourceID;
              $status = $this->RecordStatus;
              $applied = $this->TimeApplied;

              $fields_name = "a.SlotID, a.Title, a.TimeRange, a.SlotSeq";
              $dbtables = "INTRANET_SLOT as a, INTRANET_RESOURCE as b";
              $conds = "a.BatchID = b.TimeSlotBatchID AND b.ResourceID = '$rid' ";
              if ($status == 4)
              {
              /*
                  $subquery = "SELECT TimeSlot FROM INTRANET_BOOKING_RECORD WHERE BookingDate = '$bdate' AND ResourceID = $rid AND RecordStatus = $status AND TimeApplied = '$applied'";
                  $sub_result = $this->returnVector($subquery);
                  */
                  $sub_result = $this->retrieveBookingBatch();
                  $sub_list = implode(",",$sub_result);
                  $conds .= " AND (a.SlotSeq IN ($sub_list))";
              }
              else if ($status != 0)
              {
                   $conds .= " AND false";
              }
              //$conds2 = "a.RecordStatus = 0 OR (a.RecordStatus = 4 AND a.SlotSeq IN (".$this->db_sub_select($subquery).") )";
              $sql = "SELECT $fields_name FROM $dbtables WHERE $conds ORDER BY a.SlotSeq ASC";
              $slots = $this->returnArray($sql,4);
/*
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldByLang("b.");

              $fields_name = "a.TimeSlot, CONCAT($username_field,IF (b.ClassNumber IS NOT NULL,CONCAT(' (',b.ClassName,'-',b.ClassNumber,')'),'')), a.UserID, IF ((a.BookingDate = '$bdate' AND a.ResourceID = '$rid' AND a.RecordStatus = '$status' AND a.TimeApplied = '$applied'),1,2)";
              $dbtables = "INTRANET_BOOKING_RECORD as a , INTRANET_USER as b";
              $join_conds = "a.UserID = b.UserID";
              $conds = "a.BookingDate = '$bdate' AND a.ResourceID = '$rid' AND (a.RecordStatus IN (2,3,4) OR (a.RecordStatus=0 AND a.UserID = '".$this->UserID."') )";
              $sql = "SELECT $fields_name FROM $dbtables WHERE $join_conds AND $conds ORDER BY a.TimeSlot ASC";
              $reserved = $this->returnArray($sql,4);

              $x .= "<table width=408 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/timeslotsbg2.gif' class=body>
                <tr>
                  <td align=center width=134 class=h1 style=\"background-image: url('$rb_image_path/timeslotstop1a.gif'); vertical-align: middle\">$i_BookingTimeSlots</td>
                  <td align=center width=200 style=\"background-image: url('$rb_image_path/timeslotstop1c.gif'); vertical-align: middle\" class=h1>$i_BookingStatus</td>
                  <td align=center width=74 class=h1 style=\"background-image: url('$rb_image_path/timeslotstop1b.gif'); vertical-align: middle\">
                    $i_label_SelectAll<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'slots[]'):setChecked(0,document.form1,'slots[]')>
                  </td>
                </tr>
                <tr valign=top>
                  <td align=center width=134>&nbsp;</td>
                  <td align=center width=200>&nbsp;</td>
                  <td align=center width=74>&nbsp;</td>
                </tr>";
              for ($i=0; $i<sizeof($slots); $i++)
              {
                   list ($sid, $title, $timerange,$seq) = $slots[$i];
                   //$title = intranet_wordwrap ($title,12,"\n",1);
                   //$timerange = intranet_wordwrap($timerange,12,"\n",1);
                   $title = $title;
                   $timerange = $timerange;
                   $pos = $this->in_arrayField($seq, $reserved,0);

                   if ($pos != -1)
                   {
                       $color_str = "#FF0000";
                       if ($reserved[$pos][2] == $this->UserID && $reserved[$pos][3]==1)
                       {
                           $text = "<font color=red>".$i_BookingStatusArray[$status]."</font>";
                           $checkbox_str = "<input type=checkbox name=slots[] value=$seq CHECKED>";
                       }
                       else
                       {
                           //$name = intranet_wordwrap ($reserved[$pos][1],20," ",1);
                           $name = $reserved[$pos][1];
                           $text = "$i_BookingReserved $name";
//                           $text = "$i_BookingReserved ".$reserved[$pos][1];
                           $checkbox_str = "";
                       }
                   }
                   else
                   {
                       $color_str = "#000099";
                       $text = $i_BookingAvailable;
                       $checkbox_str = "<input type=checkbox name=slots[] value=$seq>";
                   }

                   $x .= "<tr valign=top>
                  <td align=center width=134>$title<br>$timerange</td>
                  <td align=center width=200><font color=\"$color_str\">$text</font><br>
                  </td>
                  <td align=center width=74>$checkbox_str</td>
                </tr>
                <tr valign=top>
                  <td align=center width=134>&nbsp;</td>
                  <td align=center width=200>&nbsp;</td>
                  <td align=center width=74>&nbsp;</td>
                </tr>";
              }
              $x .= "<tr valign=top>
                  <td align=center colspan=3><img src='$rb_image_path/timeslotsbottom2.gif' width=408 height=6>
                  </td></tr></table>\n";
              return $x;
     }

     function listPeriodicTimeSlots($rid)
     {
              global $rb_image_path, $i_BookingTimeSlots,$i_label_SelectAll;
              $sql = "SELECT b.SlotSeq, b.Title, b.TimeRange FROM INTRANET_RESOURCE as a, INTRANET_SLOT as b WHERE a.TimeSlotBatchID = b.BatchID AND a.ResourceID = '$rid' ORDER BY b.SlotSeq";
              $slots = $this->returnArray($sql,3);

              $x .= "<table width=408 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/timeslotsbg3.gif' class=body>
                <tr>
                  <td width=6><img src=/images/resourcesbooking/timeslotstopleft.gif width=6 height=20></td>
                  <td align=center width=223 class=h1 style=\"background-image: url('$rb_image_path/timeslotstopcellbg.gif'); vertical-align: middle\">$i_BookingTimeSlots</td>
                  <td width=12><img src=/images/resourcesbooking/timeslotstopmiddle.gif width=12 height=20></td>
                  <td align=center width=161 class=h1 style=\"background-image: url('$rb_image_path/timeslotstopcellbg.gif'); vertical-align: middle\">
                    $i_label_SelectAll<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'slots[]'):setChecked(0,document.form1,'slots[]')>
                  </td>
                  <td width=6><img src=/images/resourcesbooking/timeslotstopright.gif width=6 height=20></td>
                </tr>
                <tr valign=top>
                  <td colspan=5>&nbsp;</td>
                </tr>";
              for ($i=0; $i<sizeof($slots); $i++)
              {
                   list ($seq, $title, $timerange) = $slots[$i];
                   //$title = intranet_wordwrap ($title, 20,"\n",1);
                   //$timerange = intranet_wordwrap($timerange,20,"\n",1);
                   $title = $title;
                   $timerange = $timerange;

                       $color_str = "#000099";
                       $checkbox_str = "<input type=checkbox name=slots[] value=$seq>";

                   $x .= "<tr valign=top>
                  <td>&nbsp;</td>
                  <td align=center>$title $timerange</td>
                  <td>&nbsp;</td>
                  <td align=center>$checkbox_str</td>
                  <td>&nbsp;</td>
                </tr>
                <tr valign=top>
                  <td colspan=5>&nbsp;</td>
                </tr>";
              }
              $x .= "<tr valign=top>
                  <td align=center colspan=5><img src='$rb_image_path/timeslotsbottom3.gif'  width=408 height=5>
                  </td></tr></table>\n";
              return $x;
     }

     function returnReservedRecords($ts,$cat,$item, $standard=true,$schoolBatch)
     {
              global $intranet_session_language;
              if ($schoolBatch == "")
              {
                  $sql = "SELECT BatchID FROM INTRANET_SLOT_BATCH WHERE RecordStatus = 1";
                  $res = $this->returnVector($sql);
                  $schoolBatch = $res[0];
              }
/*
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldByLang("b.");
              
              
              if ($cat != "")
              {
                  $cond2 = "AND d.ResourceCategory = '$cat'";
              }
              if ($item != "")
              {
                  $cond2 .= " AND d.ResourceID = $item";
              }

              if ($standard)
              {
                  $cond1 = "d.TimeSlotBatchID = $schoolBatch";
                  $order_str = "a.TimeSlot, a.BookingDate, d.Title";
                  $field2 = "";
                  $num = 4;
                  # improve performance as there is no need to join table INTRANET_SLOT as c in this case
                  $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_RESOURCE as d";
              		$join_conds = "a.UserID = b.UserID AND a.ResourceID = d.ResourceID";
              }
              else
              {
                  $cond1 = "d.TimeSlotBatchID != $schoolBatch";
                  $order_str = "a.BookingDate, d.Title, a.TimeSlot";
                  $field2 = ",c.Title SlotTitle";
                  $num = 5;
                  
                  $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d";
              		$join_conds = "a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq";
              }

              $field_names = "a.BookingDate, a.TimeSlot, d.Title, CONCAT($username_field, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ) NameField $field2";

              $conds = "$cond1 $cond2 AND UNIX_TIMESTAMP(BookingDate) BETWEEN ". $ts ." AND ". ($ts+604799) ." AND a.RecordStatus IN (2,3,4)";
              $sql = "SELECT $field_names FROM $db_tables WHERE $join_conds AND $conds ORDER BY $order_str";

              return $this->returnArray($sql,$num);
     }
     
     function returnReservedRecords_new($ts,$cat,$item, $standard=true,$schoolBatch)
     {
              global $intranet_session_language;
              if ($schoolBatch == "")
              {
                  $sql = "SELECT BatchID FROM INTRANET_SLOT_BATCH WHERE RecordStatus = 1";
                  $res = $this->returnVector($sql);
                  $schoolBatch = $res[0];
              }
/*
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldByLang("b.");
              
              
              if ($cat != "")
              {
                  $cond2 = "AND d.ResourceCategory = '$cat'";
              }
              if ($item != "")
              {
                  $cond2 .= " AND d.ResourceID = '$item'";
              }

              if ($standard)
              {
                  $cond1 = "d.TimeSlotBatchID = '$schoolBatch'";
                  $order_str = "a.TimeSlot, a.BookingDate, d.Title";
                  $field2 = "";
                  $num = 4;
                  $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_RESOURCE as d";
              		$join_conds = "a.ResourceID = d.ResourceID";

              }
              else
              {
                  $cond1 = "d.TimeSlotBatchID != '$schoolBatch'";
                  $order_str = "a.BookingDate, d.Title, a.TimeSlot";
                  $field2 = ",c.Title SlotTitle";
                  $num = 5;
                  
                  $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_SLOT as c, INTRANET_RESOURCE as d";
              		$join_conds = "c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq";
              }

              $field_names = "a.BookingDate, a.TimeSlot, d.Title $field2, a.UserID";

              $conds = "$cond1 $cond2 AND UNIX_TIMESTAMP(BookingDate) BETWEEN ". $ts ." AND ". ($ts+604799) ." AND a.RecordStatus IN (2,3,4)";
              $sql = "SELECT $field_names FROM $db_tables WHERE $join_conds AND $conds ORDER BY $order_str";

			$tempResult = $this->returnResultSet($sql);

			//Cache UserNameField
			$UserIDArr = Get_Array_By_Key($tempResult, "UserID");
			$username_field = getNameFieldByLang();
			$sql = " SELECT UserID, $username_field NameField FROM INTRANET_USER WHERE UserID IN (".implode(",",(array)$UserIDArr).")";
			$UserArr = $this->returnResultSet($sql);
			$UserArr = BuildMultiKeyAssoc($UserArr, "UserID", "NameField", 1);
			
			foreach($tempResult as $thisResult)
			{
				$thisResult["NameField"] =  $UserArr[$thisResult['UserID']];
				$Result[] = $thisResult;
			}
//              return $this->returnArray($sql,$num);
			return $Result;
     }     
     
     
     function returnItemBatchID($itemID)
     {
              if ($itemID == "")
              {
                  return "";
              }
              $sql = "SELECT TimeSlotBatchID FROM INTRANET_RESOURCE WHERE ResourceID = '$itemID'";
              $result = $this->returnVector($sql);
              return $result[0];
     }

     function displayReservedStandard($ts="",$cat="",$item="")
     {
              global $i_Booking_Weekday,$i_DayType0,$i_DayType1,$i_DayType2,$i_DayType3,$intranet_session_language,$i_Booking_Year;
              $wrap_length = 10;
              if ($ts == "")
              {
                  $ts = time();
              }
              $ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
              $dates = array();
              for ($i=0; $i<sizeof($i_DayType0); $i++)
              {
                   $dates[$i] = date('Y-m-d',$ts+$i*86400);
              }
              $lc = new libcycleperiods();
              #$cycles = $lc->getCycleDaysFromDates($dates);
              $cycles = $lc->getCycleInfoByDateArray($dates);
              $field_pos = ($intranet_session_language=="en"?0:1);

              #$i_DayType = ${"i_DayType".$lc->CycleType};

              $x = "<table width=735 border=0 cellspacing=0 cellpadding=0><tr><td width=105>&nbsp;</td>\n";
              $x .= "<td width=35>".$i_Booking_Weekday[0]."</td>
                  <td width=56 class=holiday_tab>&nbsp;</td>\n";

              for ($i=1; $i<sizeof($i_Booking_Weekday); $i++)
              {
                   #$cycleNum = ($cycles[$i]!=-1)? $i_DayType[$cycles[$i]] :"";
                   $targetDate = $dates[$i];
                   $cycleNum = (is_array($cycles[$targetDate])? $cycles[$targetDate][$field_pos]:"&nbsp;");
                   $x .= "<td width=34>".$i_Booking_Weekday[$i]."</td>
                  <td width=56 class=weekday_tab><font color=#00129D>$cycleNum</font></td>\n";
              }
              $yearNum = date('Y',$ts);
              $year = ($intranet_session_language == "en"? "$i_Booking_Year $yearNum": "$yearNum $i_Booking_Year");

              $x .= "<tr><td width=105 align=center class=h1><font color=#000099>$year</font></td>
                  <td colspan=2 class=holiday><font color=#FF0000><a class=daytext onMouseOver=\"window.status='Day View';return true;\" onMouseOut=\"window.status='';return true;\" href=javascript:fe_view_day_rb($ts)>".date('d/m',strtotime($dates[0]))."</a></font></td>\n";
              for ($i=1; $i<sizeof($i_Booking_Weekday); $i++)
              {
                   $currDate = date('d/m',strtotime($dates[$i]));
                   $x .= "<td colspan=2 class=weekday><font color=#000099><a class=daytext onMouseOver=\"window.status='Day View';return true;\" onMouseOut=\"window.status='';return true;\" href=javascript:fe_view_day_rb(".($ts+$i*86400).")>$currDate</a></font></td>\n";
              }
              $x .= "</tr></table>\n";
              if ($item=="")
              {
                  $lb = new libbatch();
              }
              else
              {
                  $lb = new libbatch($this->returnItemBatchID($item));
              }
              
              $slots = $lb->slots;
/* testing only, will deleted              
//$runtime =10;
//
//for($runtime_ctr=0; $runtime_ctr<$runtime; $runtime_ctr++)
//{
//	StartTimer();
//	
//	$this->returnReservedRecords($ts,$cat,$item,true,$lb->BatchID);
//	
//	$time['sql1'] += StopTimer();
//	$time['sql1arr'][] = StopTimer();
//}
//
//for($runtime_ctr=0; $runtime_ctr<$runtime; $runtime_ctr++)
//{
//	StartTimer();
//
//	$this->returnReservedRecords_new($ts,$cat,$item,true,$lb->BatchID);
//
//	$time['sql2'] += StopTimer();
//	$time['sql2arr'][] = StopTimer();
//}
//              
//hdebug_pr($time);    
 * */          
              $result = $this->returnReservedRecords($ts,$cat,$item,true,$lb->BatchID);
              $ptr = 0;
              $x .= "<table width=735 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>\n";
              for ($i=0; $i<sizeof ($slots); $i++)
              {
                   //$lesson = intranet_wordwrap($slots[$i][1],$wrap_length," ",1);
                   //$stime = intranet_wordwrap($slots[$i][2],$wrap_length," ",1);
                   $lesson = $slots[$i][1];
                   $stime = $slots[$i][2];
                   $slotseq = $slots[$i][3];
                   $x .= "<tr>
                         <td width=98 class=td_center_middle bgcolor=#C0FBEC>$lesson<br>$stime</td>\n";
                   for ($j=0; $j<sizeof($i_Booking_Weekday); $j++)
                   {
                        $bg = ($j==0?"#E8FCEA" :"#E8FBFC");
                        $x .= "<td width=91 class=td_center_middle bgcolor=$bg>\n";
                        while ($ptr < sizeof($result))
                        {
//                               list ($bdate,$timeslot,$item,$username) = $result[$ptr];
							$bdate = $result[$ptr]['BookingDate'];
							$timeslot = $result[$ptr]['TimeSlot'];
							$item = $result[$ptr]['Title'];
							$username = $result[$ptr]['NameField'];

                               //$item = intranet_wordwrap ($item, $wrap_length," ",1);
                               //$username = intranet_wordwrap($username ,$wrap_length," ",1);
                               if ($bdate == $dates[$j] && $timeslot == $slotseq)
                               {
                                   $x .= "<span class=h1>$item</span><br>$username<br>\n";
                                   $ptr++;
                               }
                               else
                               {
                                   break;
                               }
                        }
                        $x .= "</td>\n";
                   }
                   $x .= "</tr>\n";
              }
              $x .= "</table>\n";
              return $x;

     }

     function displayReservedOther($ts="",$cat="",$item="")
     {
              global $i_Booking_Weekday,$i_DayType0,$i_DayType1,$i_DayType2,$i_DayType3,$intranet_session_language,$i_Booking_Year,$i_Booking_BookingRecords;
              $wrap_length = 10;
              if ($ts == "")
              {
                  $ts = time();
              }
              $ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
              $dates = array();
              for ($i=0; $i<sizeof($i_DayType0); $i++)
              {
                   $dates[$i] = date('Y-m-d',$ts+$i*86400);
              }
              #$lc = new libcycle();
              $lc = new libcycleperiods();
              #$cycles = $lc->getCycleDaysFromDates($dates);
              $cycles = $lc->getCycleInfoByDateArray($dates);
              $field_pos = ($intranet_session_language=="en"?0:1);
              #$i_DayType = ${"i_DayType".$lc->CycleType};

              $x = "<table width=735 border=0 cellspacing=0 cellpadding=0><tr><td width=105>&nbsp;</td>\n";
              $x .= "<td width=35>".$i_Booking_Weekday[0]."</td>
                  <td width=56 class=holiday_tab>&nbsp;</td>\n";

              for ($i=1; $i<sizeof($i_Booking_Weekday); $i++)
              {
                   #$cycleNum = ($cycles[$i]!=-1)? $i_DayType[$cycles[$i]] :"";
                   $targetDate = $dates[$i];
                   $cycleNum = (is_array($cycles[$targetDate])? $cycles[$targetDate][$field_pos]:"&nbsp;");
                   $x .= "<td width=34>".$i_Booking_Weekday[$i]."</td>
                  <td width=56 class=weekday_tab><font color=#00129D>$cycleNum</font></td>\n";
              }
              $yearNum = date('Y',$ts);
              $year = ($intranet_session_language == "en"? "$i_Booking_Year $yearNum": "$yearNum $i_Booking_Year");

              $x .= "<tr><td width=105 align=center class=h1><font color=#000099>$year</font></td>
                  <td colspan=2 class=holiday><font color=#FF0000><a class=daytext onMouseOver=\"window.status='Day View';return true;\" onMouseOut=\"window.status='';return true;\" href=javascript:fe_view_day_rb($ts)>".date('d/m',strtotime($dates[0]))."</a></font></td>\n";
              for ($i=1; $i<sizeof($i_Booking_Weekday); $i++)
              {
                   $currDate = date('d/m',strtotime($dates[$i]));
                   $x .= "<td colspan=2 class=weekday><font color=#000099><a class=daytext onMouseOver=\"window.status='Day View';return true;\" onMouseOut=\"window.status='';return true;\" href=javascript:fe_view_day_rb(".($ts+$i*86400).")>$currDate</a></font></td>\n";
              }
              $x .= "</tr></table>\n";
              $lb = new libbatch();
              $slots = $lb->slots;
              $result = $this->returnReservedRecords($ts,$cat,$item,false,$lb->BatchID);
              $ptr = 0;
              $x .= "<table width=735 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>\n";
              $x .= "<tr>
                       <td height=100 width=98 class=td_center_middle bgcolor=#C0FBEC>$i_Booking_BookingRecords</td>\n";
              for ($j=0; $j<sizeof($i_Booking_Weekday); $j++)
              {
                   $bg = ($j==0?"#E8FCEA" :"#E8FBFC");
                   $x .= "<td width=91 class=td_center_middle bgcolor=$bg>\n";
                   while ($ptr < sizeof($result))
                   {
//                          list ($bdate,$timeslot,$item,$username,$slotname) = $result[$ptr];
						$bdate = $result[$ptr]['BookingDate'];
						$timeslot = $result[$ptr]['TimeSlot'];
						$item = $result[$ptr]['Title'];
						$username = $result[$ptr]['NameField'];
						$slotname = $result[$ptr]['SlotTitle'];
							
                          //$item = intranet_wordwrap ($item, $wrap_length," ",1);
                          //$timeslot = intranet_wordwrap ($timeslot,$wrap_length," ",1);
                          //$slotname = intranet_wordwrap($slotname,$wrap_length," ",1);
                          //$username = intranet_wordwrap($username ,$wrap_length," ",1);
                          if ($bdate == $dates[$j])
                          {
                              $x .= "<span class=h1>$item</span> ($slotname)<br>$username\n";
                              $ptr++;
                          }
                          else
                          {
                              break;
                          }
                   }
                   $x .= "</td>\n";
              }
              $x .= "</tr>\n";
              $x .= "</table>\n";
              return $x;

     }

     function returnDayRecords($ts,$cat,$item)
     {
              global $intranet_session_language;
              
              if ($cat != "")
              {
                  $cond2 = "AND d.ResourceCategory = '$cat'";
              }
//               else
//               {
                  if ($item != "")
                  {
                      $cond2 .= "AND d.ResourceID = '$item'";
                  }
                  else
                  {
                      $cond2 .= "";
                  }
//               }

              $ts = mktime(0,0,0,date('m',$ts),date('d',$ts),date('Y',$ts));
              /*
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldByLang("b.");

              $field_names = " d.Title, CONCAT($username_field, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ) , c.Title, c.TimeRange, a.Remark, d.ResourceCode";
              $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d";
              $join_conds = "a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq";
              $conds = "UNIX_TIMESTAMP(BookingDate) = $ts AND a.RecordStatus IN (2,3,4) $cond2";
              $order_str = "d.Title, d.ResourceID, a.TimeSlot";
              $sql = "SELECT $field_names FROM $db_tables WHERE $join_conds AND $conds ORDER BY $order_str";
              
              return $this->returnArray($sql,6);
     }

     function displayDayView($ts,$cat,$item)
     {
              global $i_BookingItem,$i_Booking_Time,$i_Booking_ReservedBy,$i_BookingNotes;
              $result = $this->returnDayRecords($ts,$cat,$item);
              
//              $x .= "<table width=585 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
              $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#E8FBFC bordercolordark=#3BC0C6 class=body>
        <tr>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_BookingItem</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Booking_Time</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_Booking_ReservedBy</td>
          <td class=td_center_middle_h1 bgcolor=#E8FCEA>$i_BookingNotes</td>
        </tr>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list ($item,$username,$slotname,$slottime,$remark,$rcode) = $result[$i];
                   $remark = nl2br($this->convertAllLinks2($remark));
                   $x .= "<tr>
          <td class=td_center_middle bgcolor=#E8FBFC>$item ($rcode)</td>
          <td class=td_center_middle bgcolor=#E8FBFC>$slotname <em>$slottime</em></td>
          <td class=td_center_middle bgcolor=#E8FBFC>$username</td>
          <td class=td_center_middle bgcolor=#E8FBFC>&nbsp;$remark</td>
        </tr>";
              }
              if (sizeof($result)==0)
              {
                  $x .= "<tr><td colspan=4 class=td_center_middle bgcolor=#E8FBFC>".$this->no_msg."</td></tr>\n";
              }
              $x .= "</table>";
              return $x;
     }

     function in_arrayField ($needle, $array, $atField)
     {
              for ($i=0; $i<sizeof($array); $i++)
              {
                   if ($needle == $array[$i][$atField])
                       return $i;
              }
              return -1;
     }
     function manipulateFile($str){
          $files = explode(":", stripslashes($str));
          for($i=0; $i<count($files); $i++){
               $x .= $this->processAttachmentString(trim($files[$i]));
          }
          return $x;
     }

     function processAttachmentString($filename){
          global $intranet_httppath;
          $path = "/file/resource/";
          $url = $path.str_replace(" ","%20",$filename);
          $ext = substr($filename,strrpos($filename,"."));
//          $x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status=\\'$filename\\';return true;\" onMouseOut=\"window.status=\\'\\';return true;\"><img border=0 src=/images/icon_attachment.gif></a>";
          $x .= "<a href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status=\\'$filename\\';return true;\" onMouseOut=\"window.status=\\'\\';return true;\"><img border=0 src=/images/icon_attachment.gif></a>";
          return $x;
     }

     function hasRightToReserve($uid="")
     {
              global $intranet_root;
              if ($uid=="")
              {
                  global $UserID;
                  $uid = $UserID;
              }
              if ($uid=="") return false;

              $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '$uid'";
              $temp = $this->returnVector($sql);
              $userlogin = $temp[0];
              if ($userlogin == "") return false;
              $list = get_file_content("$intranet_root/file/resourcebooking_banlist.txt");
              $banusers = explode("\n",$list);
              for ($i=0; $i<sizeof($banusers); $i++)
              {
                   $banusers[$i] = trim($banusers[$i]);
              }
              if (in_array($userlogin,$banusers))
              {
                  return false;
              }
              else return true;
     }
     
	function Archive_Booking_Record($ResourceID='', $BatchID='')
	{
		if(trim($BatchID)!='')
		{
			$sql = "SELECT ResourceID FROM INTRANET_RESOURCE WHERE TimeSlotBatchID = '$BatchID'";
			$items = $this->returnVector($sql);
			
			if(empty($items))
				return true;
			
		}
		else if(trim($ResourceID)!='')
		{
			$items = (array)$ResourceID;
		}
		else
		{
			$cond2 = " AND a.BookingDate < CURDATE() ";
			$cond2_del = "AND BookingDate < CURDATE() ";
		}
	
		if(isset($items))
		{
			$conds = " AND a.ResourceID IN (".implode(",",(array)$items).") ";
			$conds_del = "AND ResourceID IN (".implode(",",(array)$items).") ";
		}
		
		$sql = "
			INSERT INTO INTRANET_BOOKING_ARCHIVE
				(BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
			SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
				CONCAT(b.EnglishName, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ),
				d.ResourceCategory, d.Title, d.ResourceCode,
				a.RecordStatus, a.TimeApplied, a.LastAction
			FROM 
				INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d
			WHERE 
				a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq
			AND a.BookingDate < CURDATE() $conds
		";
		$Result["Insert"] = $this->db_db_query($sql);
	
		# Remove records
		//$sql = "DELETE FROM INTRANET_BOOKING_RECORD a WHERE 1 $conds $cond2";
		$sql = "DELETE FROM INTRANET_BOOKING_RECORD where 1 $conds_del $cond2_del";

		$Result["Delete"] = $this->db_db_query($sql);
		
		return !in_array(false,(array)$Result);
	}     

}


}        // End of directive

?>