<?php
if (!defined("LIBFAMILY_DEFINED"))         // Preprocessor directives
{

 define("LIBFAMILY_DEFINED",true);

 class libfamily extends libdb{

       function libfamily()
       {
                $this->libdb();
       }
       function retrieveRecord($id)
       {
                return $result;
       }
       function returnChildrens($parentID)
       {
                $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$parentID'";
                return $this->returnVector($sql);
       }
       function returnFirstChild($parentID)
       {
                $sql = "SELECT a.StudentID
                        FROM INTRANET_PARENTRELATION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                        WHERE a.ParentID = '$parentID'
                        ORDER BY b.ClassName, b.ClassNumber";
                $result = $this->returnVector($sql);
                return $result[0];
       }
       function returnChildrensList($parentID)
       {
                $name_field = getNameFieldWithClassNumberByLang("b.");
                $sql = "SELECT a.StudentID, $name_field
                        FROM INTRANET_PARENTRELATION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                        WHERE a.ParentID = '$parentID'
                        ORDER BY b.ClassName, b.ClassNumber";
                return $this->returnArray($sql,2);
       }
       function getSelectChildren($parentID,$tags,$selected="")
       {
                $data = $this->returnChildrensList($parentID);
                if (sizeof($data)==0) return "";
                else return getSelectByArray($data,$tags,$selected);
       }
       function displayChildrenList($parentID)
       {
                global $i_UserParent_LinkedChildren;
                $bordercolordark = "#909EC7";
                $bordercolorlight = "#DCE5FF";
                $bgcolortag = "bgcolor=#CBE9FB";
                $table_attr = "$bgcolortag bordercolordark=$bordercolordark bordercolorlight=$bordercolorlight";
                $children = $this->returnChildrensList($parentID);
                $namefield = getNameFieldWithLoginByLang("");
                $sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID = '$parentID'";
                $temp = $this->returnVector($sql);
                $parent_name = $temp[0];
                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 class=body $table_attr>\n";
                $x .= "<tr class=parent_popup_title><td colspan=2><STRONG>$parent_name$i_UserParent_LinkedChildren</STRONG></td></tr>\n";
                for ($i=0; $i<sizeof($children); $i++)
                {
                     list($StudentID, $name) = $children[$i];
                     $x .= "<tr><td width=5% align=right >&nbsp;</td><td >$name</td></tr>\n";
                }
                if (sizeof($children)==0)
                {
                    global $i_UserParentWithNoLink;
                    $x .= "<tr><td >$i_UserParentWithNoLink</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       
       function displayChildrenList2007a($parentID)
       {
                global $i_UserParent_LinkedChildren;
                
                $children = $this->returnChildrensList($parentID);
                $namefield = getNameFieldWithLoginByLang("");
                $sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID = '$parentID'";
                $temp = $this->returnVector($sql);
                $parent_name = $temp[0];
                
                $x = "";
                $x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' class='attendancestudentphoto'>\n";
                $x .= "<tr class='tabletext'><td colspan='2'><STRONG>$parent_name$i_UserParent_LinkedChildren</STRONG></td></tr>\n";
                for ($i=0; $i<sizeof($children); $i++)
                {
                     list($StudentID, $name) = $children[$i];
                     $x .= "<tr><td width='5%'>&nbsp;</td><td class='tabletext'>$name</td></tr>\n";
                }
                if (sizeof($children)==0)
                {
                    global $i_UserParentWithNoLink;
                    $x .= "<tr><td class='tabletext'>$i_UserParentWithNoLink</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

 }


} // End of directives
?>