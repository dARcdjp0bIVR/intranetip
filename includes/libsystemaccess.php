<?php
# modifying : henry chow
if (!defined("LIBSYSTEMACCESS_DEFINED"))                     // Preprocessor directive
{
define("LIBSYSTEMACCESS_DEFINED", true);

class libsystemaccess extends libdb {
        var $UserID;
        var $ACL;

        function libsystemaccess ($uid="")
        {
                 $this->libdb();
                 if ($uid!="")
                 {
                     $this->UserID = $uid;
                     $this->ACL = $this->returnACLString($uid);
                 }
        }

        function returnACLString($uid)
        {
        		global $_SESSION, $UserID;
        		
        		if ($UserID==$uid && isset($_SESSION['system_access_reversed']) && $_SESSION['system_access_reversed']!="")
        		{
        			return ($_SESSION['system_access_reversed']=="NULL_RIGHT") ? "" : $_SESSION['system_access_reversed'];
        		} else
        		{
        		    $sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '".IntegerSafe($uid)."' ";
	                 $result = $this->returnVector($sql);
	                 
	                 if ($UserID==$uid)
	                 	$_SESSION['system_access_reversed'] = ($result[0]!="") ? $result[0] : "NULL_RIGHT";
	                 return $result[0];
        		}
        }

        function hasFilesRight()
        {
                 return ($this->ACL[1]==1);
        }
        function hasMailRight()
        {
                 return ($this->ACL[0]==1);
        }

}


}        // End of directive
?>