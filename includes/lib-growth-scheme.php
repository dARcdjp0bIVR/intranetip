<?php

# Modifing by : Anna
/**
 * 2019-02-12 (Anna)
 * -modified getSchemeUsers(), added user recordstatus [#J155926]
 * 
 * 2016-07-15 (Henry HM)
 * - Replaced all split() to explode()
 * 
 * 2012-03-27 (Henry Chow)
 * - modified returnExportQuestionContent(), revise the display of lang parameter with another lang parameter
 * 
 * 2012-03-15 (Henry Chow)
 * - modified getHandedinUserForExport(), revise display "Name" (exclude "First Name" & "Last Name")
 *   
 * 2009-12-03: Eric Yip
 * - Distiguish between sbs and weblog in handin 
 */

class growth_scheme extends libdb
{
	
	var $ClassName;

	function growth_scheme(){
		
		global $eclass_db;
		global $ck_course_id;
		
		$this->db = $this->classNamingDB($ck_course_id);
	}
	
	function classNamingDB($course_id="")
	{
		global $ck_course_id, $eclass_prefix;
		$x = $eclass_prefix."c".$ck_course_id;
		
		return $x;
	}

	function getSchemeInfo($my_assignment_id){
		$sql = "SELECT
					assignment_id, 
					title, 
					instruction, 
					status, 
					starttime,
					deadline, 
					parent_id, 
					answer, 
					marking_scheme AS person_response, 
					answersheet, 
					sheettype, 
					teacher_subject_id,
					answer_display_mode,
					modified
				FROM
					assignment
				WHERE
					assignment_id='$my_assignment_id' ";
		$ra = $this->returnArray($sql);
		
		return $ra;
	}

	function getPhaseInfo($my_assignment_id){
		return $this->getSchemeInfo($my_assignment_id);
	}

	/*
	*	Modified by Key (2008-07-23) - Get all relevant phase query
	*/
	function GET_ALL_RELEVANT_PHASE_QUERY($my_assignment_id="", $my_phase_id="")
	{
		$sql = "SELECT
					assignment_id, title, marking_scheme
				FROM
					assignment
				WHERE
					parent_id='$my_assignment_id' AND assignment_id<>'$my_phase_id'
				ORDER BY
					starttime, deadline ";
		
		return $sql;
	}
	
	/*
	*	Modified by Key (2008-07-23) - Get all relevant phase return value
	*/
	function GET_ALL_RELEVANT_PHASE($my_assignment_id="", $my_phase_id="")
	{
		$sql = $this->GET_ALL_RELEVANT_PHASE_QUERY($my_assignment_id, $my_phase_id);
		
		$row = $this->returnArray($sql);
		
		return $row;
	}
	
	/*
	*	Modified by Key (2008-07-23) - Get selected relevant phase return value
	*/
	function GET_SELECTED_RELEVANT_PHASE($my_assignment_id="", $my_phase_id="", $my_relevant_id="", $ReturnType="array")
	{
		# selected previously
		$current_selected = "";
		$ReturnArr = array();
		
		$row = $this->GET_ALL_RELEVANT_PHASE($my_assignment_id, $my_phase_id);
		//hdebug_r($row);
		if (trim($my_relevant_id)!="")
		{
			$my_relevant_arr = explode(",", $my_relevant_id);
			for ($i=0; $i<sizeof($my_relevant_arr); $i++)
			{
				if ($current_id=trim($my_relevant_arr[$i]))
				{
					$current_title = "";
					for ($j=0; $j<sizeof($row); $j++)
					{
						if ($row[$j]["assignment_id"]==$current_id)
						{
							$current_title = $row[$j]["title"];
							$current_marking_scheme = $row[$j]["marking_scheme"];
							break;
						}
					}
					if ($current_title!="")
					{
						$current_selected .= "<option value='$current_id' >".$current_title."</option>";
						$ReturnArr[] = array($current_id, $current_title, $current_marking_scheme);
					}
				}
			}
		}
		
		if($ReturnType == "option")
		return $current_selected;
		else
		return $ReturnArr;
	} //end function
	
	/*
	*	Modified by Key (2008-07-28) - Get non selected relevant phase return value
	*/
	function GET_NON_SELECTED_RELEVANT_PHASE($my_assignment_id="", $my_phase_id="", $my_relevant_id="", $ReturnType="array")
	{
		# available and not selected previously
		$source_available = "";
		$ReturnArr = array();
		
		$row = $this->GET_ALL_RELEVANT_PHASE($my_assignment_id, $my_phase_id);
		
		if (trim($my_relevant_id)!="")
		{
			$my_relevant_arr = explode(",", $my_relevant_id);
		}
		
		for ($i=0; $i<sizeof($row); $i++)
		{
			$is_selected = false;
			for ($j=0; $j<sizeof($my_relevant_arr); $j++)
			{
				if (trim($my_relevant_arr[$j])==$row[$i]["assignment_id"])
				{
					$is_selected = true;
					break;
				}
			}
			if (!$is_selected)
			{
				$source_available .= "<option value='".$row[$i]["assignment_id"]."' >".$row[$i]["title"]."</option>";
				$ReturnArr[] = array($row[$i]["assignment_id"], $row[$i]["title"]);
			}
		}
		
		if($ReturnType == "option")
		return $source_available;
		else
		return $ReturnArr;
	} // end function
	

	function getRelevantPhase($my_assignment_id, $my_phase_id, $my_relevant_id){
		global $button_add, $button_remove, $sr_image_path;

		$sql = "SELECT
					assignment_id, title
				FROM
					assignment
				WHERE
					parent_id='$my_assignment_id' AND assignment_id<>'$my_phase_id'
				ORDER BY
					starttime, deadline ";
		$row = $this->returnArray($sql);

		# selected previously
		if (trim($my_relevant_id)!="")
		{
			$my_relevant_arr = explode(",", $my_relevant_id);
			for ($i=0; $i<sizeof($my_relevant_arr); $i++)
			{
				if ($current_id=trim($my_relevant_arr[$i]))
				{
					$current_title = "";
					for ($j=0; $j<sizeof($row); $j++)
					{
						if ($row[$j]["assignment_id"]==$current_id)
						{
							$current_title = $row[$j]["title"];
							break;
						}
					}
					if ($current_title!="")
					{
						$current_selected .= "<option value='$current_id' >".$current_title."</option>";
					}
				}
			}
		}

		# available and not selected previously
		for ($i=0; $i<sizeof($row); $i++)
		{
			$is_selected = false;
			for ($j=0; $j<sizeof($my_relevant_arr); $j++)
		{
				if (trim($my_relevant_arr[$j])==$row[$i]["assignment_id"])
				{
					$is_selected = true;
					break;
				}
			}
			if (!$is_selected)
			{
				$source_available .= "<option value='".$row[$i]["assignment_id"]."' >".$row[$i]["title"]."</option>";
			}
		}

		if (sizeof($row)>0)
		{
			$rx = "<table><tr><td width='10%'>\n";
			$rx .= "<select name='relevant_phase[]' size=5 multiple>". $current_selected . "</select>\n";
			$rx .= "</td><td nowrap>\n";
			$rx .= "<p><a href=\"javascript:check(document.form1.elements['src_phase[]'],document.form1.elements['relevant_phase[]'])\" onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('imgAddItem','','$sr_image_path/Buttons/btn_Increase_U.gif',1)\"><img src='$sr_image_path/Buttons/btn_Increase_D.gif' name='imgAddItem' border='0' align='absmiddle'></a></p>\n";
			$rx .= "<p><a href=\"javascript:check(document.form1.elements['relevant_phase[]'],document.form1.elements['src_phase[]'])\" onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('imgRemoveItem','','$sr_image_path/Buttons/btn_Decreas_U.gif',1)\"><img src='$sr_image_path/Buttons/btn_Decreas_D.gif' name='imgRemoveItem' border='0' align='absmiddle'></a></p>\n";
			$rx .= "</td><td width='90%'>\n";
			$rx .= "<select name='src_phase[]' size=5 multiple>" . $source_available . "</select>\n";
			$rx .= "</td></tr></table>";
		} else
		{
			$rx = "--";
		}

		return $rx;
	}

	function getRelevantPhaseText($my_relevant_id){
		
		if (trim($my_relevant_id)!="")
		{
			$sql = "SELECT
						title
					FROM
						assignment
					WHERE
						assignment_id IN ($my_relevant_id)
					ORDER BY
						starttime, deadline ";
			$row = $this->returnVector($sql);

			if (sizeof($row)>0)
			{
				$rx = implode(", ", $row);
			}
		} else
		{
			$rx = "--";
		}

		return $rx;
	}
	
	function returnExportQuestionContent($phase_obj)
	{	
		global $ec_iPortfolio;
		global $g_encoding_unicode;
		
		$delimiter = "\t";
		$valueQuote = "";

		$QuestionArr = explode("#QUE#", $phase_obj["answersheet"]);
		$content .= $ec_iPortfolio['WebSAMSRegNo'].$delimiter;
		$content .= $ec_iPortfolio['class'].$delimiter;
		$content .= $ec_iPortfolio['number'].$delimiter;
		$content .= $ec_iPortfolio['export']['Student'];
		$content .= (sizeof($QuestionArr) > 1)? $delimiter:"\r\n";
		
		for($j=1; $j<sizeof($QuestionArr); $j++)
		{
			$CurrentQuestion = explode("||", $QuestionArr[$j]);
			list($QuestionType, $QuestionContent, $Options) = $CurrentQuestion;

			$QuestionContent = str_replace("\"", "\"\"", strip_tags(html_entity_decode($QuestionContent)));
			$content .= (($j+1)!=sizeof($QuestionArr)) ? $valueQuote.$QuestionContent.$valueQuote.$delimiter : $valueQuote.$QuestionContent.$valueQuote."\r\n";
			
			$QuestionInfoArr[$j]["type"] = substr($QuestionType,0,1);
			if($Options!="")
			{
				$OptionArr = explode("#OPT#", $Options);
				$QuestionInfoArr[$j]["option"] = $OptionArr;
			}
		}

		return array($content, $QuestionInfoArr);
	}

	function returnExportHandinContent($handin_obj, $QuestionInfoArr)
	{	
		global $g_encoding_unicode;
		
		$delimiter = "\t";
		$valueQuote = "";
		
		for($m=0; $m<sizeof($handin_obj); $m++)
		{
			if(sizeof($handin_obj[$m])!=0)
			{
				$content .= "\"".$handin_obj[$m]["WebSAMSRegNo"]."\"".$delimiter;
				$content .= "\"".$handin_obj[$m]["ClassName"]."\"".$delimiter;
				$content .= "\"".$handin_obj[$m]["ClassNumber"]."\"".$delimiter;
				$content .= "\"".$handin_obj[$m]["user_name"]."\"";

				$answer = str_replace("\"", "\"\"", strip_tags(html_entity_decode($handin_obj[$m]["answer"])));

				//Remove the new line , for generate the answer to CSV
				$answer = str_replace(array("\r", "\r\n", "\n"),'',$answer);

				$AnswerArr = explode("#ANS#", $answer);
		        $content .= (sizeof($AnswerArr) > 1)? $delimiter:"\r\n";
        
				for($k=1; $k<sizeof($AnswerArr); $k++)
				{
					$ans = $AnswerArr[$k];

					if($QuestionInfoArr[$k]["type"]==1 || $QuestionInfoArr[$k]["type"]==2 || $QuestionInfoArr[$k]["type"]==3)
					{
						if($ans=="" || $ans=="NULL" || $ans=="null")
						{
							$content .= $valueQuote.$valueQuote;
						}
						else if($QuestionInfoArr[$k]["type"]=="3")
						{
							$ans_arr = explode(",", $ans);
							$content .= $valueQuote;
							for($n=0; $n<sizeof($ans_arr); $n++)
							{
								$content .= (($n+1)!=sizeof($ans_arr)) ? $QuestionInfoArr[$k]["option"][$ans_arr[$n]+1].$delimiter : $QuestionInfoArr[$k]["option"][$ans_arr[$n]+1];
							}
							$content .= $valueQuote;
						}
						else
							$content .= $valueQuote.$QuestionInfoArr[$k]["option"][$ans+1].$valueQuote;
					}
					else
					{
						$content .= $valueQuote.$ans.$valueQuote;
					}
					$content .= (($k+1)!=sizeof($AnswerArr)) ? $delimiter : "\n";
				}
			}
		}
		return $content;
	}


	function getRelevantPhaseView($my_relevant_id, $text_only=0){
		global $ec_iPortfolio;

		if (trim($my_relevant_id)!="")
		{
			$field = ($text_only==1) ? "title" : "CONCAT('<a class=\"contenttool\" href=\"javascript:newWindow(\'view_phase_popup.php?phase_id=', assignment_id, '\', 27)\" >', title, '</a>')";
			$sql = "SELECT
						{$field}
					FROM
						assignment
					WHERE
						assignment_id IN ($my_relevant_id)
					ORDER BY
						starttime, deadline ";
			$row = $this->returnVector($sql);
			
			if (sizeof($row)>0)
			{
				//$rx = implode("<br>", $row);
				$rx .= "<table border=0 cellpadding=6 cellspacing=6>";
				for($i=0; $i<sizeof($row); $i++)
				{
					$rx .= "<tr><td align='center' style='border:thin dashed grey'>".$row[$i]."</td></tr>";
				}
				$rx .= "</table>";
			}
		} else
		{
			$rx = "--";
		}

		return $rx;
	}

	function getRelevantPhaseViewAnswer($my_relevant_id, $my_user_id, $text_only=0){
		global $ec_iPortfolio;

		if (trim($my_relevant_id)!="")
		{
			$field = ($text_only==1) ? "title" : "CONCAT('<a class=\"contenttool\" href=\'javascript:viewPhaseAnswer(', assignment_id, ', $my_user_id)\' >', title, '</a>')";
			$sql = "SELECT
						{$field}
					FROM
						assignment
					WHERE
						assignment_id IN ($my_relevant_id)
					ORDER BY
						starttime, deadline ";
			$row = $this->returnVector($sql);
			if (sizeof($row)>0)
			{
				for($i=0; $i<sizeof($row); $i++)
				{
					$RelevantTable .= "<tr><td align='center' style='border:thin dashed grey'>".$row[$i]."</td></tr>";
				}
			}

		} else
		{
			$rx = "";
		}
		if ($RelevantTable!="")
		{
			$rx = "<table border=0 cellspacing=6 cellpadding=6>";
			$rx .= "<tr><td>".$ec_iPortfolio['growth_relevant_phase'].":</td></tr>";
			$rx .= $RelevantTable;
			$rx .= "</table>";
		}

		return $rx;
	}


	# display phases for working on
	function getSchemePhases($my_assignment_id, $user_id="", $studentID=""){
		global $ck_memberType, $ck_user_id, $user_id_par, $ck_is_alumni;
		global $ec_iPortfolio, $profiles_to, $usertype_ct, $usertype_p, $usertype_s, $usertype_st, $no_record_msg, $assignments_datesubmit;
		global $button_redo;

		$user_id = ($user_id!="") ? $user_id : $ck_user_id;
		$fieldname = "a.title, ";
		$fieldname .= "DATE_FORMAT(a.starttime, '%Y-%m-%d %k:%i'), ";
		$fieldname .= "DATE_FORMAT(a.deadline, '%Y-%m-%d %k:%i'), ";
		$fieldname .= "CASE a.marking_scheme WHEN 'CT' THEN '$usertype_ct' WHEN 'P' THEN '$usertype_p' WHEN 'ST' THEN '$usertype_st' ELSE '$usertype_s' END, ";
		$fieldname .= "ifnull(h.inputdate, '--'), ";
		$fieldname .= "a.starttime, ";
		$fieldname .= "a.deadline, ";
		$fieldname .= "a.assignment_id, ";
		$fieldname .= "a.marking_scheme AS person_response, ";
		$fieldname .= "a.teacher_subject_id, ";
		$fieldname .= "a.sheettype, ";
		$fieldname .= "h.correctiondate, ";
		$fieldname .= "h.status ";
		$sql  = "
					SELECT
							$fieldname
					FROM
							assignment AS a
							LEFT JOIN handin AS h ON (h.assignment_id=a.assignment_id AND h.user_id='{$user_id}' AND h.type = 'sbs')
					WHERE
							a.parent_id='$my_assignment_id'
					GROUP BY
							a.assignment_id
					ORDER BY
							a.starttime, a.deadline 
				";
		$phases_arr = $this->returnArray($sql);

		$rx = "<table width='100%' border='0' cellpadding='5' cellspacing='0'>\n";
		$rx .= "<tr><td height='35' width='5%' align='center' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>#</td>\n";
		$rx.= "<td width='40%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;' nowrap='nowrap'>".$ec_iPortfolio['growth_phase_title']."</td>\n";
		$rx .= "<td width='35%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;' nowrap='nowrap'>".$ec_iPortfolio['deadline']."</td>\n";
		$rx .= "<td width='10%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;' nowrap='nowrap'>".$ec_iPortfolio['growth_target_person']."</td>\n";
		$rx .= "<td width='20%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;' nowrap='nowrap'>".$ec_iPortfolio['status']."</td>\n";
		$rx .= "<td bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;' nowrap='nowrap'>&nbsp;</td>\n";
		$rx .= ($this->getClassTeacherRight() && !$ck_is_alumni) ? "<td bgcolor='#D6EDFA' class='link_chi' width='20' style='border-bottom:2px solid #7BC4EA;'><input type=checkbox name='checkmaster' onClick=(this.checked)?setChecked(1,document.form1,'phase_id[]'):setChecked(0,document.form1,'phase_id[]')></td></tr>\n" : "<td bgcolor='#D6EDFA' class='link_chi' width='15' style='border-bottom:2px solid #7BC4EA;'>&nbsp;</td>";

		for ($i=0; $i<sizeof($phases_arr); $i++)
		{
			$phase_obj = $phases_arr[$i];
			$person_right = $this->getPhaseRight($ck_memberType, $phase_obj);

			$now_style0 = "";
			$now_style1 = "";
			$allow_redo = 0;
			if ($person_right=="DO" || $person_right=="REDO")
			{
				$do_wording = ($phase_obj["sheettype"]==2) ? $ec_iPortfolio['growth_phase_view_only'] : $ec_iPortfolio['growth_phase_fill'];

				$operator = "<a href=\"javascript:fillNow(".$phase_obj["assignment_id"].")\">".$do_wording."</a>";
				$now_style0 = "<b>";
				$now_style1 = "</b>";

			} elseif ($person_right=="VIEW" || $person_right=="VIEW_DOING")
			{			
				# check wether
				$allow_redo = ($this->getClassTeacherRight() && !$ck_is_alumni && $phase_obj["person_response"]=="S" && $phase_obj["sheettype"]!=2 && $person_right!="VIEW_DOING") ? 1 : 0;

				$view_wording = ($phase_obj["sheettype"]==2) ? $ec_iPortfolio['growth_phase_view_only'] : $ec_iPortfolio['growth_phase_view'];
				$operator = "<a href=\"javascript:viewFilled(".$phase_obj["assignment_id"].")\">".$view_wording."</a>";
			} else
			{
				$operator = "<a href=\"phase_view.php?".$user_id_par."phase_id=".$phase_obj["assignment_id"]."\">".$ec_iPortfolio['preview']."</a>";
			}
			# check the status
			$submitted_word = ($phase_obj["sheettype"]==2) ? $ec_iPortfolio['growth_phase_viewed'] : $ec_iPortfolio['submitted'];
			$not_submitted_word = ($phase_obj["sheettype"]==2) ? $ec_iPortfolio['growth_phase_not_viewed'] : $ec_iPortfolio['not_submitted']; 

			$time_now = date("Y-m-d H:i:s");
			if($phase_obj["starttime"]>$time_now)
			{
				$status = $ec_iPortfolio['not_start'];
			}
			else if($phase_obj["starttime"]<$time_now && $phase_obj["deadline"]>$time_now)
			{
				$status = ($phase_obj[4]!="--" && $person_right=="DO") ? $submitted_word."<br />".$phase_obj[4] : $ec_iPortfolio['processing'];
			}
			else
			{
				if($phase_obj["status"]=="R")
				{
					$status = ($time_now<=$phase_obj["correctiondate"]) ? "<font color=red>$button_redo</font>" : "<font color=red>$button_redo (".$ec_iPortfolio['not_submitted'].")</font>";
				}	
				else if($phase_obj["status"]=="LR")
				{
					$status = "<font color=red>$button_redo (".$ec_iPortfolio['submitted'].")<br />".$phase_obj[4]."</font>";
				}
				else
					$status = ($phase_obj[4]=="--") ? $not_submitted_word : $submitted_word."<br />".$phase_obj[4];
			}

			$bg_color = ($i%2==0) ? "#FFFFFF" : "#EEEEEE";
			$rx .= "<tr><td bgcolor='$bg_color' height='60' align='center'>$now_style0".($i+1)."$now_style1</td>\n";
			$rx .= "<td bgcolor='$bg_color'>$now_style0".$phase_obj[0]."$now_style1</td>\n";
			$rx .= "<td bgcolor='$bg_color' onMouseMove='overhere();' onMouseOver=\"showTipsHelp('".$phase_obj[1]."&nbsp;".$profiles_to."&nbsp;".$phase_obj[2]."')\" onMouseOut='hideTip()'>$now_style0".$phase_obj[2]."$now_style1</td>\n";
			//$now_style0".$phase_obj[2]."$now_style1
			$rx .= "<td bgcolor='$bg_color' nowrap='nowrap'>$now_style0".$phase_obj[3]."$now_style1</td>\n";
			$rx .= ($phase_obj["status"]=="R" || $phase_obj["status"]=="LR") ? "<td bgcolor='$bg_color' nowrap='nowrap' onMouseMove='overhere();' onMouseOver=\"showTipsHelp('".$ec_iPortfolio['deadline'].": ".$phase_obj["correctiondate"]."')\" onMouseOut='hideTip()'>$now_style0".$status."$now_style1</td>\n" : "<td bgcolor='$bg_color' nowrap='nowrap'>$now_style0".$status."$now_style1</td>\n";
			$rx .= "<td bgcolor='$bg_color' nowrap='nowrap'>$now_style0".$operator."$now_style1</td>\n";
			$rx .= ($allow_redo==1) ? "<td bgcolor='$bg_color'><input onClick='this.form.checkmaster.checked=false' type=checkbox name='phase_id[]' value='".$phase_obj["assignment_id"]."'></td></tr>\n" : "<td bgcolor='$bg_color'>&nbsp;</td>";
		}
		if (sizeof($phases_arr)<=0)
		{
			$rx .= "<tr><td colspan='6' bgcolor='#FBFCFA' align='center' height='100'>".$no_record_msg."</td></tr>\n";
		}

		$rx .= "</table>";

		return $rx;
	}


	function getPhaseRight($memberType, $phase_obj, $ignore_classname="", $ignore_subject="")
	{
		$period_result = $this->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);
	
		if ($phase_obj["person_response"]=="CT" && $memberType=="T")
		{
			if($ignore_classname)
			$is_right_person = true;
			else
			$is_right_person = $this->getClassTeacherRight();

		} elseif ($phase_obj["person_response"]=="P" && $memberType=="P")
		{
			$is_right_person = true;
		} elseif ($phase_obj["person_response"]=="S" && $memberType=="S")
		{
			$is_right_person = true;
		} elseif ($phase_obj["person_response"]=="ST" && $memberType=="T")
		{
			if($ignore_subject)
			$is_right_person = true;
			else
			$is_right_person = $this->getSubjectTeacherRight($phase_obj["teacher_subject_id"]);

		} else
		{
			$is_right_person = false;
		}
		
		if ($period_result=="IN" && ($is_right_person || $memberType=="T"))
		{
			$rx = ($memberType=="T" && !$is_right_person) ? "VIEW_DOING" : "DO";
		} elseif ($period_result=="PAST")
		{
			$time_now = date("Y-m-d H:i:s");
			$rx = (($phase_obj["status"]=="R" || $phase_obj["status"]=="LR") && $is_right_person && $time_now<=$phase_obj["correctiondate"]) ? "REDO" : "VIEW";
		} else
		{
			$rx = "NO";
		}

		return $rx;
	}
	
	function getStudentPhaseHandin($my_user_id, $phase_obj){
		global $ck_user_id;

		$sql = "SELECT
					handin_id, 
					answer,
					correctiondate,
					status,
					inputdate
				FROM
					handin
				WHERE
					assignment_id='".$phase_obj["assignment_id"]."' 
					AND user_id='".$my_user_id."'
					AND type='sbs'
				ORDER BY 
					handin_id DESC
				LIMIT 1
			";
		$handins_arr = $this->returnArray($sql);
		return $handins_arr[0];
	}
	
	function getTeacherPhaseHandin($phase_obj){
	global $intranet_db;
		
		$sql = "SELECT
					a.handin_id, 
					a.answer,
					a.correctiondate,
					a.status,
					a.inputdate,
					a.user_id,
					iu.UserID
				FROM
					handin as a
				INNER JOIN usermaster as um ON a.user_id = um.user_id
				INNER JOIN {$intranet_db}.INTRANET_USER as iu ON um.user_email = iu.UserEmail
				WHERE
					a.assignment_id='".$phase_obj["assignment_id"]."' 
				  AND a.user_id = um.user_id
				  AND a.type = 'sbs'
				ORDER BY 
					a.inputdate DESC, a.handin_id DESC
				LIMIT 1
			";
		$handins_arr = $this->returnArray($sql);
		
		return $handins_arr[0];
	}

	function returnClassOfStudent($my_user_id){
		$sql = "SELECT
					class_number
				FROM
					usermaster
				WHERE
					user_id='$my_user_id' ";
		$row = $this->returnVector($sql);
		list($class_name, $class_number) = explode("-", $row[0]);

		return trim($class_name);
	}


	function returnClasses(){
		global $ck_user_id, $intranet_db, $ck_course_id;

		$ra = array();
		if ($ck_user_id!="")
		{
			$course_db = $this->classNamingDB($ck_course_id);
			$sql = "SELECT
						ic.ClassName AS class_name, ic.ClassName
					FROM
						{$intranet_db}.INTRANET_CLASS AS ic,
						{$intranet_db}.INTRANET_CLASSTEACHER AS ict,
						{$intranet_db}.INTRANET_USER AS iu,
						{$course_db}.usermaster AS um
					WHERE
						um.user_id='$ck_user_id' 
						AND iu.UserEmail=um.User_Email 
						AND ict.UserID=iu.UserID
						AND ict.ClassID=ic.ClassID
						AND (um.status IS NULL OR um.status NOT IN ('deleted')) ";

			$ra = $this->returnArray($sql);
		}

		return $ra;
	}


	function returnStudents($class_name){
		global $ck_user_id, $intranet_db, $ck_course_id;

		$ra = array();
		if ($class_name!="")
		{
			$sql = "SELECT
						user_id, if(class_number IS NULL OR class_number='', CONCAT(ifnull(lastname,''), ' ', ifnull(firstname, '')), CONCAT(ifnull(lastname,''), ' ', ifnull(firstname, ''), ' &lt;', class_number, '&gt; ')) AS user_name
					FROM
						usermaster
					WHERE
						class_number LIKE '$class_name %'
					ORDER BY
						class_number, lastname, firstname ";
			$ra = $this->returnArray($sql);
		}

		return $ra;
	}


	function returnChildren(){
		global $ck_children_ids;

		$ra = array();
		if ($ck_children_ids!="")
		{
			$sql = "SELECT
						user_id, if(class_number IS NULL OR class_number='', CONCAT(ifnull(lastname,''), ' ', ifnull(firstname, '')), CONCAT(ifnull(lastname,''), ' ', ifnull(firstname, ''), ' &lt;', class_number, '&gt; ')) AS user_name
					FROM
						usermaster
					WHERE
						user_id IN ($ck_children_ids)
					ORDER BY
						class_number, lastname, firstname ";
			$ra = $this->returnArray($sql);
		}

		return $ra;
	}


	function getChildrenSelection(){
		global $user_id;

		$childrens_arr = $this->returnChildren();

		if ($user_id=="" || $user_id==0)
		{
			$user_id = $childrens_arr[0]["user_id"];
		}
		$rx = getSelectByArrayTitle($childrens_arr, "name='user_id' onChange='this.form.submit()'", "", $user_id);

		return $rx;
	}


	function getClassSelection($class_name){
		$classes_arr = $this->returnClasses();

		if ($user_id!="" && $user_id>0 && $class_name=="")
		{
			$class_name = $this->returnClassOfStudent($user_id);
		}
		if ($class_name=="")
		{
			$class_name = $classes_arr[0]["class_name"];
		}
		$rx = getSelectByArrayTitle($classes_arr, "name='class_name' onChange='this.form.submit()'", "", $class_name);

		return Array($rx, $class_name);
	}


	function getStudentSelection($class_name){
		global $user_id;

		if ($class_name!="")
		{
			$students_arr = $this->returnStudents($class_name);

			if ($user_id=="" || $user_id==0)
			{
				$user_id = $students_arr[0]["user_id"];
			}
			$rx = getSelectByArrayTitle($students_arr, "name='user_id' onChange='this.form.submit()'", "", $user_id);
		}

		return $rx;
	}


	function getIsParentAllowed(){
		global $eclass_db, $ck_course_id;

		$sql = "SELECT course_id
				FROM {$eclass_db}.course
				WHERE course_id='$ck_course_id' AND is_parent='1' ";
		$row = $this->returnVector($sql);

		return (sizeof($row)>0);
	}
	
########## Function added for Teacher Subject in School-based Schme ###############
#################### Start ####################
	function getSubjectSelection($ParSubjectID)
	{
		global $intranet_db;

		$subject_desc_field = $_SESSION['intranet_session_language'] == "b5" ? "CH_DES" : "EN_DES";
		$sql = "SELECT
					RecordID, $subject_desc_field
				FROM
					{$intranet_db}.ASSESSMENT_SUBJECT
				WHERE
					RecordStatus = '1'
				";
		$SubjectArr = $this->returnArray($sql);
		$ReturnStr = getSelectByArrayTitle($SubjectArr, "name='SubjectID'", "", $ParSubjectID);

		return $ReturnStr;
	}

	function getSubjectName($ParSubjectID)
	{
		global $intranet_db;

		$subject_desc_field = $_SESSION['intranet_session_language'] == "b5" ? "CH_DES" : "EN_DES";
		$sql = "SELECT
					$subject_desc_field
				FROM
					{$intranet_db}.ASSESSMENT_SUBJECT
				WHERE
					RecordStatus = '1'
					AND RecordID = '$ParSubjectID'
				LIMIT 1
				";
		$ReturnValue = $this->returnVector($sql);

		return $ReturnValue[0];
	}
		
	function getClassTeacherRight()
	{
		global $intranet_db, $ck_intranet_user_id, $ClassName;
		global $lpf;

		$TargetClass = ($ClassName=="") ? $this->ClassName : $ClassName;
		
		return in_array($TargetClass, $lpf->GET_CLASS_TEACHER_CLASS());
// INTRANET_CLASS is not used now, just use GET_CLASS_TEACHER_CLASS provided by libportfolio.
//		$sql = "SELECT
//					ct.ClassTeacherID
//				FROM
//					{$intranet_db}.INTRANET_CLASSTEACHER AS ct,
//					{$intranet_db}.INTRANET_CLASS AS c
//				WHERE
//					c.ClassName='$TargetClass' 
//					AND c.ClassID=ct.ClassID
//					AND ct.UserID='$ck_intranet_user_id'
//				";
//		$row = $this->returnVector($sql);
//		
//		return ($row[0]!="") ? true : false;
	}
	
	function getSubjectTeacherRight($ParSubjectID)
	{
		global $intranet_db, $ck_intranet_user_id, $ClassName;
		global $lpf;
		
		$TargetClass = ($ClassName=="") ? $this->ClassName : $ClassName;
		
		return in_array($TargetClass, $lpf->GET_SUBJECT_TEACHER_CLASS($ParSubjectID));
// INTRANET_CLASS is not used now, just use GET_SUBJECT_TEACHER_CLASS provided by libportfolio.
//		$sql = "SELECT 
//					st.SubjectTeacherID
//				FROM 
//					{$intranet_db}.INTRANET_SUBJECT_TEACHER AS st,
//					{$intranet_db}.INTRANET_CLASS AS c
//				WHERE 
//					c.ClassID=st.ClassID
//					AND c.ClassName='$TargetClass' 
//					AND st.SubjectID = '".$ParSubjectID."' 
//					AND st.UserID = '".$ck_intranet_user_id."'
//				";
//		$row = $this->returnVector($sql);
//
//		return ($row[0]!="") ? true : false;
	}

	function getAllSubjectTeacher($ParSubjectID)
	{
		global $intranet_db, $eclass_db, $ck_intranet_user_id;
		
		$sql = "SELECT DISTINCT
					ic.ClassName,
					CONCAT(um.lastname, ' ', um.firstname),
					um.user_email
				FROM 
					{$intranet_db}.INTRANET_SUBJECT_TEACHER AS st
					LEFT JOIN {$intranet_db}.INTRANET_CLASS AS ic ON ic.ClassID=st.ClassID
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON st.UserID = iu.UserID
					LEFT JOIN usermaster as um ON um.user_email = iu.UserEmail
				WHERE 
					st.SubjectID = '".$ParSubjectID."'
					AND um.user_id IS NOT NULL
				";
		$row = $this->returnArray($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
			list($class_name, $teacher, $user_email) = $row[$i];
			$ReturnArray["Name"][$class_name][] = $teacher;
			$ReturnArray["Email"][$class_name][] = $user_email;
		}

		return $ReturnArray;
	}

	function getAllClassTeacher()
	{
		global $intranet_db, $eclass_db, $ck_intranet_user_id;

		$sql = "SELECT DISTINCT
					ic.ClassName,
					CONCAT(um.lastname, ' ', um.firstname),
					um.user_email
				FROM 
					{$intranet_db}.INTRANET_CLASSTEACHER AS ct
					LEFT JOIN {$intranet_db}.INTRANET_CLASS AS ic ON ic.ClassID=ct.ClassID
					LEFT JOIN {$intranet_db}.INTRANET_USER as iu ON ct.UserID = iu.UserID
					LEFT JOIN usermaster as um ON um.user_email = iu.UserEmail
				WHERE 
					um.user_id IS NOT NULL
				";
		$row = $this->returnArray($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
			list($class_name, $teacher, $user_email) = $row[$i];
			$ReturnArray["Name"][$class_name][] = $teacher;
			$ReturnArray["Email"][$class_name][] = $user_email;
		}

		return $ReturnArray;
	}
	
	function getAllParent($ParStudentArr)
	{
		global $intranet_db, $eclass_db, $ck_intranet_user_id;
		
		$sql = "SELECT DISTINCT
					sum.user_id,
					CONCAT(pum.lastname, ' ', pum.firstname),
					pum.user_email
				FROM 
					{$intranet_db}.INTRANET_PARENTRELATION AS pr
					LEFT JOIN {$intranet_db}.INTRANET_USER as piu ON pr.ParentID = piu.UserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as siu ON pr.StudentID = siu.UserID 
					LEFT JOIN usermaster as pum ON pum.user_email = piu.UserEmail
					LEFT JOIN usermaster as sum ON sum.user_email = siu.UserEmail
				WHERE 
					pum.user_id IS NOT NULL
				";
		$row = $this->returnArray($sql);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($student_id, $parent, $user_email) = $row[$i];
			$ReturnArray["Name"][$student_id][] = $parent;
			$ReturnArray["Email"][$student_id][] = $user_email;
		}

		return $ReturnArray;
	}
#################### End ####################
	
######################### Function new added for showing scheme info #########################
	
	function returnPhasesBySchemeID($SchemeArr)
	{
		$SchemeIDStr = implode(",", $SchemeArr);
		$sql = "SELECT
					a.assignment_id,
					a.parent_id,
					if(a.starttime='0000-00-00 00:00:00','',a.starttime) as starttime,
					if(a.deadline='0000-00-00 00:00:00','',a.deadline) as deadline,
					a.marking_scheme AS person_response,
					a.teacher_subject_id
				FROM 
					assignment as a
				WHERE 
					a.parent_id IN ({$SchemeIDStr}) 	
					 AND (a.marking_scheme = 'CT' OR a.marking_scheme = 'ST')
				ORDER BY
					a.assignment_id
				";
		$ReturnArray = $this->returnArray($sql);

		return $ReturnArray;
	}
	
	# return detail info table or simple info info
	function getUserInfoTable($user_id, $NoPhoto=0)
	{
		global $intranet_db, $eclass_db, $ec_student_word, $ec_iPortfolio;
		
		//$ClassNumField = getClassNumberField("a.");
		$sql = "SELECT
					a.EnglishName, 
					a.ChineseName,
					a.ClassName,
					a.ClassNumber
				FROM 
					{$intranet_db}.INTRANET_USER as a
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.UserID = b.UserID
				WHERE 
					b.CourseUserID = '{$user_id}'
				ORDER BY
					a.ClassName,
					a.ClassNumber
				";
		$row = $this->returnArray($sql);
		
		if($NoPhoto==0)
		{
			$ReturnTable = "<TABLE border='0' width='220' height='130' cellpadding='0' cellspacing='6' style='border:thin dashed grey'>";
			$ReturnTable .= "<tr height=20><td width=70 nowrap='nowrap'>".$ec_student_word['name_english']."</td><td width=20>:</td><td nowrap='nowrap'>".$row[0][0]."</td></tr>";
			$ReturnTable .= "<tr height=20><td width=70 nowrap='nowrap'>".$ec_student_word['name_chinese']."</td><td width=20>:</td><td nowrap='nowrap'>".$row[0][1]."</td></tr>";
			$ReturnTable .= "<tr height=20><td width=70 nowrap='nowrap'>".$ec_iPortfolio['class']."</td><td width=20>:</td><td nowrap='nowrap'>".$row[0][2]."</td></tr>";
			$ReturnTable .= "<tr height=20><td width=70 nowrap='nowrap'>".$ec_iPortfolio['number']."</td><td width=20>:</td><td nowrap='nowrap'>".$row[0][3]."</td></tr>";
			$ReturnTable .= "</TABLE>";
		}
		else
		{
			$ReturnTable = "<TABLE border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$ReturnTable .= "<tr><td nowrap='nowrap'>".$ec_iPortfolio['growth_student_name'].": ".$row[0][0]." (".$row[0][1].")</td><td align='right' nowrap='nowrap'>".$ec_iPortfolio['class'].": ".$row[0][2]."</td><td align='right' nowrap='nowrap'>".$ec_iPortfolio['number'].": ".$row[0][3]."</td></tr>";
			$ReturnTable .= "</TABLE>";
		}
		
		return $ReturnTable;
	}

  // Eric Yip (20101004): retrieve Chinese Name for display
	function getUserInfo($ParUsers)
	{
		global $lpf, $eclass_db, $intranet_db;
		
		$UserList = (is_array($ParUsers)) ? implode(",", $ParUsers) : $ParUsers;
		
		if($UserList == "") $UserList = "''";
		
		$sql = "SELECT
					um.user_id,
					".getNameFieldByLang2("iu.")." AS firstname,
					um.lastname,
					CONCAT(iu.ClassName, ' - ', iu.ClassNumber) AS class_number,
					um.user_email
				FROM 
					{$lpf->course_db}.usermaster um
				INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps
				  ON um.user_id = ps.CourseUserID
				INNER JOIN {$intranet_db}.INTRANET_USER iu
				  ON ps.UserID = iu.UserID
				WHERE 
					um.user_id IN ($UserList)
					AND (um.status IS NULL OR um.status NOT IN ('deleted'))
				ORDER BY
					iu.ClassNumber
				";
		$row = $this->returnArray($sql);

		return $row;
	}
	
	/*
	// not used
	function getResponsePersonEmail($ParStudentArr, $ParResponse, $ParSubjectID=0)
	{
		global $intranet_db, $eclass_db;

		$StudentList = implode(",", $ParStudentArr);
		if($ParResponse=="CT")
		{
			$sql = "SELECT DISTINCT 
						e.UserEmail
					FROM 
						usermaster as a 
						LEFT JOIN {$intranet_db}.INTRANET_USER as b ON a.user_email = b.UserEmail
						LEFT JOIN {$intranet_db}.INTRANET_CLASS as c ON b.ClassName = c.ClassName
						LEFT JOIN {$intranet_db}.INTRANET_CLASSTEACHER as d ON c.ClassID = d.ClassID
						LEFT JOIN {$intranet_db}.INTRANET_USER as e ON d.UserID = e.UserID
					WHERE
						a.user_id IN ({$StudentList})
						AND e.UserEmail IS NOT NULL
					";
		}
		else if($ParResponse=="ST")
		{
			$sql = "SELECT DISTINCT 
						e.UserEmail
					FROM 
						usermaster as a 
						LEFT JOIN {$intranet_db}.INTRANET_USER as b ON a.user_email = b.UserEmail
						LEFT JOIN {$intranet_db}.INTRANET_CLASS as c ON b.ClassName = c.ClassName
						LEFT JOIN {$intranet_db}.INTRANET_SUBJECT_TEACHER as d ON c.ClassID = d.ClassID AND SubjectID = '{$ParSubjectID}'
						LEFT JOIN {$intranet_db}.INTRANET_USER as e ON d.UserID = e.UserID
					WHERE
						a.user_id IN ({$StudentList})
						AND e.UserEmail IS NOT NULL
					";
		}
		else if($ParResponse=="P")
		{
			$sql = "SELECT DISTINCT 
						c.UserEmail
					FROM 
						{$eclass_db}.PORTFOLIO_STUDENT as a 
						LEFT JOIN {$intranet_db}.INTRANET_PARENTRELATION as b ON a.UserID = b.StudentID
						LEFT JOIN {$intranet_db}.INTRANET_USER as c ON b.ParentID = c.UserID
					WHERE
						a.CourseUserID IN ({$StudentList})
						AND c.UserEmail IS NOT NULL
					";
		}
		else
		{
			$sql = "SELECT DISTINCT 
						user_email 
					FROM 
						usermaster 
					WHERE 
						user_id IN ({$StudentList}) 
						AND user_email IS NOT NULL
					";
		}
		$ReturnEmailList = $this->returnVector($sql);

		return $ReturnEmailList;
	}
	*/

	function getPhasePeriod($starttime, $deadline)
	{
		$time_now = date("Y-m-d H:i:s");
		$period_result = "FUTURE";
		if ($time_now>=$starttime)
		{
			$period_result = ($time_now<=$deadline) ? "IN" : "PAST";
		}
		
		return $period_result;
	}	
		
	# function for retrieve student photo 
	function getStudentPhoto($user_id)
	{
		global $eclass_db, $intranet_root, $intranet_rel_path, $ec_iPortfolio, $luser;

		$sql = "SELECT 
					ps.WebSAMSRegNo 
				FROM 
					{$eclass_db}.PORTFOLIO_STUDENT as ps
				WHERE 
					ps.CourseUserID = '{$user_id}'
				";
		$row = $this->returnVector($sql);
		$regno = $row[0];

		// get the official photo of the student by the RegNo
		$tmp_regno = trim(str_replace("#", "", $regno));
		$photolink = "/file/official_photo/".$tmp_regno.".jpg";
		$photo_filepath = $intranet_root.$photolink;
		$photo_url = $intranet_rel_path.$photolink;
		
		//$StudentPhoto = (is_file($photo_filepath)) ? "<img src=".$photo_url." border='1' width='100' height='130'>" : $luser->user_photo($user_id);
		$StudentPhoto = (is_file($photo_filepath)) ? "<img src=".$photo_url." border='1' width='100' height='130'>" : "";
	
		return 	$StudentPhoto;
	}
	
############################################# End #######################################################

####################### School-based Scheme Phase Fill Count Related Function ########################### 
	function getSchemeUsers($ParAssignmentID, $ParGroupID="", $ParClassTaught="")
	{
		global $eclass_db, $intranet_db;
		
		$conds = ($ParGroupID!="") ? " AND b.group_id = '".$ParGroupID."'" : "";
		$conds .= ($ParClassTaught!="") ? " AND d.ClassName IN ('".implode("','", $ParClassTaught)."')" : "";
		
		# get total number of user in group(s)
		$sql = "SELECT DISTINCT 
					b.user_id
				FROM 
					grouping_function as a 
					LEFT JOIN user_group as b ON a.group_id = b.group_id 
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as c ON b.user_id = c.CourseUserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as d ON c.UserID = d.UserID
				WHERE 
					a.function_id = '{$ParAssignmentID}'
					AND a.function_type = 'A'
					AND b.user_id IS NOT NULL
					AND c.WebSAMSRegNo IS NOT NULL 
					AND c.UserKey IS NOT NULL 
					AND c.IsSuspend = '0'
					AND d.RecordType = '2'
                    AND d.RecordStatus = '1'
					$conds
				ORDER BY
					d.ClassName asc, d.ClassNumber asc
			";
			
		$row = $this->returnVector($sql);
		
		# if no group assigned, get total student number in iPortfolio
		if(sizeof($row)==0)
		{
			if($ParGroupID!="")
			{
				$conds = " AND c.group_id = '".$ParGroupID."'";
				$join = " LEFT JOIN user_group as c ON a.CourseUserID = c.user_id";
			}
			else
			{
				$conds = "";
				$join = "";
			}
			
			$conds .= ($ParClassTaught!="") ? " AND b.ClassName IN ('".implode("','", $ParClassTaught)."')" : "";
			$sql = "SELECT DISTINCT
						a.CourseUserID 
					FROM 
						{$eclass_db}.PORTFOLIO_STUDENT as a 
						LEFT JOIN {$intranet_db}.INTRANET_USER as b ON a.UserID = b.UserID
						$join
					WHERE
						a.WebSAMSRegNo IS NOT NULL
						AND a.CourseUserID IS NOT NULL
						AND a.UserKey IS NOT NULL 
						AND a.IsSuspend = '0'
						AND b.RecordType = '2'
                        AND b.RecordStatus = '1'
						$conds
					ORDER BY
						b.ClassName asc, b.ClassNumber asc
					";
			$row = $this->returnVector($sql);
		}
		
		return $row;
	}
	
	function getAllPhasesWithPeriod($assignment_id, $UserList)
	{
		$sql = "SELECT DISTINCT
					a.assignment_id,
					a.starttime,
					a.deadline
				FROM 
					assignment AS a 
					LEFT JOIN handin as b ON a.assignment_id = b.assignment_id AND b.user_id IN ({$UserList}) AND b.type='sbs' 
				WHERE 
					a.parent_id='$assignment_id'
					AND a.worktype='7'
				";
		$row = $this->returnArray($sql);
		
		$FutureStr = ",";
		$InStr = ",";
		for($i=0; $i<sizeof($row); $i++)
		{
			list($aid, $starttime, $deadline) = $row[$i];
			$period = $this->getPhasePeriod($starttime, $deadline);
			if($period=="FUTURE")
				$FutureStr .= $aid.",";
			else if($period=="IN")
				$InStr .= $aid.",";
		}

		return array($InStr, $FutureStr);
	}
	
	function getHandedinUser($ParPhaseID, $ParGroupID="", $ParClassTaught="")
	{
		global $eclass_db, $intranet_db;

		if($ParGroupID!="")
		{
			$join = " LEFT JOIN user_group AS d ON a.user_id = d.user_id";
			$cond = " AND d.group_id = '{$ParGroupID}'";
		}
		$conds .= ($ParClassTaught!="") ? " AND c.ClassName IN ('".implode("','", $ParClassTaught)."')" : "";
		$sql = "SELECT DISTINCT
					a.user_id
				FROM 
					handin AS a
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.user_id = b.CourseUserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as c ON b.UserID = c.UserID
					$join
				WHERE 
					a.assignment_id = '{$ParPhaseID}' 
					AND b.WebSAMSRegNo IS NOT NULL
			    AND b.UserKey IS NOT NULL
					AND b.IsSuspend = '0'
					AND c.RecordType = '2'
					AND a.type = 'sbs'
					$cond
				ORDER BY 
					c.ClassName, 
					c.ClassNumber
				";
		$row = $this->returnVector($sql);

		return $row;
	}
	
	function getHandedinUserSubmittedDate($ParPhaseID, $ParGroupID="", $ParUserID)
	{
		global $eclass_db, $intranet_db;

		if($ParGroupID!="")
		{
			$join = " LEFT JOIN user_group AS d ON a.user_id = d.user_id";
			$cond = " AND d.group_id = '{$ParGroupID}'";
		}
		$sql = "SELECT
				*
				FROM 
					handin AS a
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.user_id = b.CourseUserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as c ON b.UserID = c.UserID
					$join
				WHERE 
					a.assignment_id = '{$ParPhaseID}' 
					AND b.WebSAMSRegNo IS NOT NULL
			    AND b.UserKey IS NOT NULL
					AND b.IsSuspend = '0'
					AND c.RecordType = '2'
					AND a.user_id = '{$ParUserID}'
					AND a.type = 'sbs'
					$cond
				ORDER BY 
					c.ClassName, 
					c.ClassNumber
				";
				
		$row = $this->returnArray($sql);

		return $row;
	}
	
	function getHandedinUserForExport($phase_obj){
		global $ck_memberType, $ck_user_id, $intranet_db, $eclass_db;
		/*
		$sql = "SELECT 
		      c.UserID,
					c.WebSAMSRegNo, 
					c.ClassName, 
					c.ClassNumber, 
					IF(STRCMP(CONCAT(ifnull(c.LastName,''), ' ', ifnull(c.FirstName, '')), ' '), CONCAT(c.LastName, ' ', c.FirstName), CONCAT(c.EnglishName, ' ', c.ChineseName)) AS user_name,
					a.handin_id, 
					a.answer
				FROM
					handin as a 
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.user_id = b.CourseUserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as c ON c.UserID = b.UserID
				WHERE
					a.assignment_id='".$phase_obj["assignment_id"]."' 
					AND b.WebSAMSRegNo IS NOT NULL
					AND b.UserKey IS NOT NULL
					AND b.IsSuspend = 0
					AND c.RecordType = '2'
					AND a.type = 'sbs'
				ORDER BY
					c.ClassName, 
					c.ClassNumber
				";
		*/
		$sql = "SELECT 
		      c.UserID,
					c.WebSAMSRegNo, 
					c.ClassName, 
					c.ClassNumber, 
					".Get_Lang_Selection("c.ChineseName","c.EnglishName")." AS user_name,
					a.handin_id, 
					a.answer
				FROM
					handin as a 
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.user_id = b.CourseUserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as c ON c.UserID = b.UserID
				WHERE
					a.assignment_id='".$phase_obj["assignment_id"]."' 
					AND b.WebSAMSRegNo IS NOT NULL
					AND b.UserKey IS NOT NULL
					AND b.IsSuspend = 0
					AND c.RecordType = '2'
					AND a.type = 'sbs'
				ORDER BY
					c.ClassName, 
					c.ClassNumber
				";
		$handins_arr = $this->returnArray($sql);

		return $handins_arr;
	}
  
  function getHandedinUserInfo($UserId){
  
    global $ck_memberType, $ck_user_id, $intranet_db, $eclass_db;
    
    $sql = "SELECT 
		      c.UserID,
					c.WebSAMSRegNo, 
					c.ClassName, 
					c.ClassNumber, 
					IF(STRCMP(CONCAT(ifnull(c.LastName,''), ' ', ifnull(c.FirstName, '')), ' '), CONCAT(c.LastName, ' ', c.FirstName), CONCAT(c.EnglishName, ' ', c.ChineseName)) AS user_name
				FROM
          {$intranet_db}.INTRANET_USER as c 
				WHERE
					c.UserID = $UserId
					AND c.RecordType = '2'
				ORDER BY
					c.ClassName, 
					c.ClassNumber
				";
				
		$userInfo_arr = $this->returnArray($sql);
		
		return $userInfo_arr;
    
  }
  
	function getSchemeProcessingCount()
	{
		$SchemeArray = $this->getPublishedSchemeWithGrouping();
		for($i=0; $i<sizeof($SchemeArray); $i++)
		{
			list($aid, $group_id) = $SchemeArray[$i];
			if($group_id=="")
				$ReturnArray["all"][] = $aid;
			else
				$ReturnArray[$group_id][] = $aid;
		}
	
		return $ReturnArray;
	}
	
	function getPublishedScheme($ParGroupID="")
	{
		$conds = ($ParGroupID!="") ? " AND (b.grouping_function_id IS NULL OR b.group_id = '".$ParGroupID."')" : "";
		# get all schemes
		$sql = "SELECT DISTINCT
					a.assignment_id
				FROM 
					assignment as a
					LEFT JOIN grouping_function as b ON a.assignment_id = b.function_id AND b.function_type = 'A'
				WHERE
					a.worktype='6' 
					AND a.status='1'
					AND a.parent_id IS NULL
					$conds
				ORDER BY
					a.assignment_id
				";
		$ReturnArray = $this->returnVector($sql);

		return $ReturnArray;
	}
	
	function getPublishedSchemeWithGrouping()
	{
		# get all schemes with grouping
		$sql = "SELECT
					a.assignment_id,
					b.group_id
				FROM 
					assignment as a
					LEFT JOIN grouping_function as b ON a.assignment_id = b.function_id AND b.function_type = 'A'
				WHERE 
					a.worktype='6' 
					AND a.status='1'
					AND a.parent_id IS NULL
				ORDER BY
					a.assignment_id,
					b.group_id
				";
		$ReturnArray = $this->returnArray($sql, 2);

		return $ReturnArray;
	}
	
	function getTeacherPhaseFillCountByClass($ClassGroupArr, $SchemeProcessingArr)
	{
		global $ck_memberType;
		
		$ReturnArray = array();
		$PublishedScheme = $this->getPublishedScheme();
		if(sizeof($PublishedScheme)!=0)
		{
			$PhaseArray = $this->returnPhasesBySchemeID($PublishedScheme);
			for($k=0; $k<sizeof($ClassGroupArr); $k++)
			{
				$this->ClassName = trim($ClassGroupArr[$k]["ClassName"]);
				$GroupID = trim($ClassGroupArr[$k]["GroupID"]);				
				for($i=0; $i<sizeof($PhaseArray); $i++)
				{
					$phase_obj = $PhaseArray[$i];
					if((!empty($SchemeProcessingArr[$GroupID]) && (in_array($phase_obj["parent_id"], $SchemeProcessingArr[$GroupID])) || (!empty($SchemeProcessingArr["all"]) && in_array($phase_obj["parent_id"], $SchemeProcessingArr["all"]))))
					{
						$person_right = $this->getPhaseRight($ck_memberType, $phase_obj);
						if($person_right=="DO")
						{
							$SchemeUserArr = $this->getGroupMembers($GroupID);
							$HandedinUser = $this->getHandedinUser($phase_obj["assignment_id"]);
							if(sizeof($SchemeUserArr)-sizeof($HandedinUser)>0)
								$ReturnArray[$this->ClassName] = $ReturnArray[$this->ClassName] + 1;
						}
					}
				}
			}
		}		

		return $ReturnArray;
	}
	
	function getTeacherPhaseNotFillCountByStudent($ParGroupID){
		global $ck_memberType;
		
		$ReturnUserArr = array();
		$SchemeUserArr = $this->getGroupMembers($ParGroupID);
		$SchemeArr = $this->getPublishedScheme($ParGroupID);
		if(sizeof($SchemeArr)!=0)
		{
			$PhaseArray = $this->returnPhasesBySchemeID($SchemeArr);
			for($i=0; $i<sizeof($PhaseArray); $i++)
			{
				$phase_obj = $PhaseArray[$i];
				$person_right = $this->getPhaseRight($ck_memberType, $phase_obj);
				if($person_right=="DO")
				{	
					$HandedinUser = $this->getHandedinUser($phase_obj["assignment_id"]);
					for($j=0; $j<sizeof($SchemeUserArr); $j++)
					{
						$uid = trim($SchemeUserArr[$j]);					
						if(sizeof($HandedinUser)==0 || !in_array($uid, $HandedinUser))
						{
							$ReturnUserArr[$uid] = $ReturnUserArr[$uid] + 1;
						}
					}
				}
			}
		}	

		return $ReturnUserArr;
	}
	
	function getAllSchemeUsers($ParGroupID="")
	{
		global $eclass_db, $intranet_db;
		
		$SchemeArr = $this->getPublishedScheme();
		for($i=0; $i<sizeof($SchemeArr); $i++)
		{
			$aid = trim($SchemeArr[$i]);
			$UserArr = $this->getSchemeUsers($aid, $ParGroupID);
			$SchemeUserArr[$aid] = $UserArr;
		}
		
		return $SchemeUserArr;
	}

	function getGroupIDByClassID($ParClassID)
	{
		global $intranet_db, $eclass_db;
		
		$ClassIDStr = is_array($ParClassID) ? implode(",", $ParClassID) : $ParClassID;
		$sql = "SELECT DISTINCT
					a.group_id
				FROM 
					user_group as a 
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.user_id = b.CourseUserID
					INNER JOIN {$intranet_db}.INTRANET_USER as c ON b.UserID = c.UserID
					INNER JOIN {$intranet_db}.INTRANET_CLASS as d ON c.ClassName = d.ClassName
				WHERE
					d.ClassID IN ({$ClassIDStr})
				";
		$row = $this->returnVector($sql);

		return $row;
	}
	
	function getParentPhases($ParChildrenArr)
	{
		global $eclass_db;
		
		$ChildrenStr = (is_array($ParChildrenArr)) ? implode(",", $ParChildrenArr) : $ParChildrenArr;
		$sql = "SELECT DISTINCT
					a.starttime,
					a.deadline,
					a.assignment_id,
					a.parent_id
				FROM
					assignment as a
					LEFT JOIN grouping_function as b ON b.function_id = a.parent_id AND b.function_type = 'A'
					LEFT JOIN user_group as c ON c.group_id = b.group_id
				WHERE
					(b.grouping_function_id IS NULL OR c.user_id IN ({$ChildrenStr}))
					AND a.worktype = '7'
					AND a.marking_scheme = 'P'
				ORDER BY
					a.assignment_id
				";
		$row = $this->returnArray($sql, 4);

		return $row;
	}

	function getStudentPhases()
	{
		global $ck_course_id, $ck_user_id;
			
		$lo = new libgroups($this->classNamingDB($ck_course_id));
		$ExcludeList = $lo->returnFunctionIDs("A",$ck_user_id);
		# get all Phases
		$sql = "SELECT DISTINCT
					a.starttime,
					a.deadline,
					a.assignment_id,
					a.parent_id,
					a.teacher_subject_id,
					c.handin_id,
					c.status,
					c.correctiondate
				FROM 
					assignment AS a 
					LEFT JOIN assignment AS b ON a.parent_id = b.assignment_id
					LEFT JOIN handin as c ON a.assignment_id = c.assignment_id AND c.user_id = '{$ck_user_id}' AND c.type = 'sbs'
				WHERE 
					b.worktype=6
					AND b.status='1' 
					AND b.assignment_id NOT IN ({$ExcludeList})
					AND a.worktype=7
					AND a.marking_scheme = 'S'
				";
		$row = $this->returnArray($sql, 8);

		return $row;
	}

	function getTeacherPhases($ParGroupID)
	{
		$GroupIDStr = (is_array($ParGroupID)) ? implode(",", $ParGroupID) : $ParGroupID;
		$sql = "SELECT DISTINCT
					a.starttime,
					a.deadline,
					a.assignment_id,
					a.parent_id,
					a.teacher_subject_id
				FROM 
					assignment as a
					LEFT JOIN grouping_function as b ON a.parent_id = b.function_id AND b.function_type = 'A'
				WHERE 
					a.worktype='7' 
					AND a.parent_id IS NOT NULL
					AND (b.grouping_function_id IS NULL OR b.group_id IN ({$GroupIDStr}))
					AND (a.marking_scheme = 'CT' OR a.marking_scheme = 'ST')
				ORDER BY
					a.assignment_id
				";
		$row = $this->returnArray($sql, 5);

		return $row;
	}
	
	function checkHandedin($ParPhaseID, $ParUserID)
	{
		$sql = "SELECT 
					COUNT(handin_id) 
				FROM 
					handin 
				WHERE 
					assignment_id = '".$ParPhaseID."' 
					AND user_id = '".$ParUserID."'
					AND status <> 'R'
					AND type = 'sbs'
			";
		$row = $this->returnVector($sql);

		return ($row[0]>0) ? 1 : 0;
	}

	# get not filled phase
	function getNotFillScheme($ParGroupID, $ParUserID)
	{
		global $ck_memberType;

		$ReturnSchemeArr = array();
		if($ck_memberType=="T")
			$PhaseObjArr = $this->getTeacherPhases($ParGroupID);
		else if($ck_memberType=="S")
			$PhaseObjArr = $this->getStudentPhases();
		else if($ck_memberType=="P")
		{
			# get student course user id
			$ChildrenIntranetIDArr = $this->getChildrenCourseUserID();
			$PhaseObjArr = $this->getParentPhases($ChildrenIntranetIDArr);
		}

		for($i=0; $i<sizeof($PhaseObjArr); $i++)
		{
			$phase_obj = $PhaseObjArr[$i];
			$InPeriod = $this->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);
			if($InPeriod=="IN")
			{
				if($ck_memberType=="S")
				{
					$IsHandedin = ($phase_obj["handin_id"]!="" && $phase_obj["status"]!="R") ? 1 : 0;
				}
				else 
				{
					$IsHandedin = $this->checkHandedin($phase_obj["assignment_id"], $ParUserID);
				}
				if($IsHandedin==0 && !in_array($phase_obj["parent_id"], $ReturnSchemeArr))
					$ReturnSchemeArr[] = $phase_obj["parent_id"];
			}
		}

		return $ReturnSchemeArr;
	}
	
	function getSchemeChildren($ParAssignmentID)
	{
		global $eclass_db, $ck_children_intranet_ids;
		
		$sql = "SELECT
					b.user_id
				FROM 
					grouping_function as a 
					LEFT JOIN user_group as b ON a.group_id = b.group_id
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as c ON b.user_id = c.CourseUserID
				WHERE
					c.UserID IN ({$ck_children_intranet_ids})
					AND a.function_type = 'A'
					AND a.function_id = '".$ParAssignmentID."'
				";
		$row = $this->returnVector($sql);
		
		return $row;
	}
	
	function getChildrenCourseUserID()
	{
		global $eclass_db, $ck_children_intranet_ids;

		$sql = "SELECT CourseUserID FROM {$eclass_db}.PORTFOLIO_STUDENT WHERE UserID IN ({$ck_children_intranet_ids})";
		$row = $this->returnVector($sql);

		return $row;
	}

	function getParentNotFillPhase($frIntranet=0)
	{
		global $ck_children_intranet_ids;
		
		# get student course user id
		$ChildrenIntranetIDArr = $this->getChildrenCourseUserID();

		# all parent phases
		$PhaseObjArr = $this->getParentPhases($ChildrenIntranetIDArr);
		for($i=0; $i<sizeof($PhaseObjArr); $i++)
		{
			$SchemeUsers = array();
			if($frIntranet==1)
			{
				list($starttime, $deadline, $assignment_id, $parent_id) = $PhaseObjArr[$i];
				$phase_obj["starttime"] = $starttime;
				$phase_obj["deadline"] = $deadline;
				$phase_obj["assignment_id"] = $assignment_id;
				$phase_obj["parent_id"] = $parent_id;
			}
			else
				$phase_obj = $PhaseObjArr[$i];
			
			$InPeriod = $this->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);
			if($InPeriod=="IN")
			{
				$AllUsers = $this->getSchemeUsers($phase_obj["parent_id"]);
				for($j=0; $j<sizeof($ChildrenIntranetIDArr); $j++)
				{
					$uid = $ChildrenIntranetIDArr[$j];
					if(in_array($uid, $AllUsers) && !in_array($uid, $SchemeUsers))
						$SchemeUsers[] = $uid;
				}
				$TotalUser = sizeof($SchemeUsers);
				$HandinedUser = sizeof($this->getHandedinUser($phase_obj["assignment_id"]));
				if($TotalUser-$HandinedUser>0)
				{
					$ReturnPhaseArr[] = $phase_obj;
				}
			}
		}

		return $ReturnPhaseArr;
	}

	# get student not filled phase
	function getStudentNotFillPhase($frIntranet=0)
	{
		$PhaseObjArr = $this->getStudentPhases();
		for($i=0; $i<sizeof($PhaseObjArr); $i++)
		{
			if($frIntranet==1)
			{
				list($starttime, $deadline, $assignment_id, $parent_id, $teacher_subject_id, $handin_id, $status, $correctiondate) = $PhaseObjArr[$i];
				$phase_obj["starttime"] = $starttime;
				$phase_obj["deadline"] = $deadline;
				$phase_obj["assignment_id"] = $assignment_id;
				$phase_obj["parent_id"] = $parent_id;
				$phase_obj["teacher_subject_id"] = $teacher_subject_id;
				$phase_obj["handin_id"] = $handin_id;
				$phase_obj["status"] = $status;
				$phase_obj["correctiondate"] = $correctiondate;
			}
			else
				$phase_obj = $PhaseObjArr[$i];
		
			$InPeriod = $this->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);
			if($InPeriod=="IN")
			{
				if($phase_obj["handin_id"]=="" || ($phase_obj["handin_id"]!="" && $phase_obj["status"]=="R"))
					$ReturnPhaseArr[] = $phase_obj;					
			}
		}

		return $ReturnPhaseArr;
	}
	
	# get teacher not filled phase
	function getTeacherNotFillPhase($frIntranet=0)
	{
		global $intranet_db, $eclass_db;
		global $ck_course_id, $ck_user_id, $ck_memberType, $ck_intranet_user_id;
		
		# get all class of class/subject teacher
		$sql = "SELECT DISTINCT 
					a.ClassID
				FROM 
					{$intranet_db}.INTRANET_CLASS as a
					LEFT JOIN {$intranet_db}.INTRANET_CLASSTEACHER as b ON a.ClassID = b.ClassID
					LEFT JOIN {$intranet_db}.INTRANET_SUBJECT_TEACHER AS c ON a.ClassID = c.ClassID
				WHERE 
					a.ClassID IS NOT NULL
					AND b.ClassTeacherID IS NOT NULL
					AND c.SubjectTeacherID IS NOT NULL
					AND (b.UserID = '".$ck_intranet_user_id."' OR c.UserID = '".$ck_intranet_user_id."')
				ORDER BY 
					a.ClassName
				";
		$ClassIDArr = $this->returnVector($sql);

		if(sizeof($ClassIDArr)!=0)
		{
			$GroupIDArr = $this->getGroupIDByClassID($ClassIDArr);
			if(sizeof($GroupIDArr)!=0)
			{
				$PhaseObjArr = $this->getTeacherPhases($GroupIDArr);				
				$SchemeUserArr = $this->getAllSchemeUsers();
				for($i=0; $i<sizeof($PhaseObjArr); $i++)
				{
					if($frIntranet==1)
					{
						list($starttime, $deadline, $assignment_id, $parent_id, $teacher_subject_id) = $PhaseObjArr[$i];
						$phase_obj["starttime"] = $starttime;
						$phase_obj["deadline"] = $deadline;
						$phase_obj["assignment_id"] = $assignment_id;
						$phase_obj["parent_id"] = $parent_id;
						$phase_obj["teacher_subject_id"] = $teacher_subject_id;
					}
					else
						$phase_obj = $PhaseObjArr[$i];
					
					$InPeriod = $this->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);
					if($InPeriod=="IN")
					{
						$TotalUser = sizeof($SchemeUserArr[$phase_obj["parent_id"]]);
						$HandinedUser = sizeof($this->getHandedinUser($phase_obj["assignment_id"]));
						if($TotalUser-$HandinedUser>0)
						{
							$ReturnPhaseArr[] = $phase_obj;
						}
					}
				}
			}
		}
		return $ReturnPhaseArr;
	}

	// get not filled phase
	function getNotFillPhase($ParCourseID="", $ParCourseUserID="", $ParMemberType="", $ParChildrenIDs="", $ParUserID="", $frIntranet=0)
	{
		global $ck_course_id, $ck_user_id, $ck_memberType, $ck_intranet_user_id, $ck_children_intranet_ids;			
		
		$ck_course_id = ($ParCourseID=="") ? $ck_course_id : $ParCourseID;
		$ck_user_id = ($ParCourseUserID=="") ? $ck_user_id : $ParCourseUserID;
		$ck_memberType = ($ParMemberType=="") ? $ck_memberType : $ParMemberType;
		$ck_intranet_user_id = ($ParUserID=="") ? $ck_intranet_user_id : $ParUserID;
		
		$this->db = $this->classNamingDB($ck_course_id);
		if($ck_memberType=="T")
			$ReturnPhaseArr = $this->getTeacherNotFillPhase($frIntranet);
		else if($ck_memberType=="S")
			$ReturnPhaseArr = $this->getStudentNotFillPhase($frIntranet);
		else if($ck_memberType=="P")
		{	
			$ck_children_intranet_ids = ($ParChildrenIDs=="") ? $ck_children_intranet_ids : $ParChildrenIDs;
			$ReturnPhaseArr = $this->getParentNotFillPhase($frIntranet);
		}

		return $ReturnPhaseArr;
	}

	function getGroupMembers($ParGroupID)
	{
		$sql = "SELECT DISTINCT
					user_id
				FROM 
					user_group
				WHERE 
					group_id = '".$ParGroupID."'
				";
		$row = $this->returnVector($sql);

		return $row;
	}


############################################# End #######################################################
}
?>
