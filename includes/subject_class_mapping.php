<?
# Using: 

/********************** Change Log ***********************/
#   Date    :   2019-06-19 [Philips]
#               Modified Get_Subject_Group_Student_List() - add $showClassID to get YearClassID
#
#   Date 	:	2018-09-10 [Isaac]
#               Modified Get_Subject_Groups_By_SubjectStudent() - added ORDER BY stc.ClassTitleEN to sql statement.
#   Date 	:	2017-11-30 [Isaac]
#               Modified Get_Subject_Group_Student_List() - added u.WebSAMSRegNo in select for $sql
#
#	Date 	:	2017-03-15 [Villa]	(Bill undo) update 2017-4-5 fix the problem
#				Modified Get_Subject_Group_Student_List() - fix Subject Group only has student without class display problem
#
#	Date	:	2017-01-11 [Villa]
#				Modified Get_Subject_Group_Student_List() - support displaying student in the Subject Group but not in Class
#
#	Date	:	2016-06-27 [Omas] [ip.2.5.7.8.1]
#				Modified Get_Subject_Code_ID_Mapping_Arr() - add parm $isCaseSensitive
#
#	Date	: 	2016-03-30 [Ivan] [ip.2.5.7.4.1]
#				Modified Create_Form_Subject() to correct the sequence of fields
#
#	Date	: 	2016-03-11 [Pun] [ip.2.5.7.4.1] (93360)
#				Modified Get_Subject_Group_Student_List(), cannot load records if student is deleted in INTRANET_USER
#
#	Date	: 	2016-02-25 [Pun] [ip.2.5.7.4.1]
#				Modified Get_Subject_List_With_Component(), added return parent subject name
#
#	Date	: 	2015-12-18 [Pun] [ip.2.5.7.1.1] [84012]
#				Modified Get_Subject_Directory(), Get_Related_CourseID(), Get_Unclassified_CourseID()
#
#	Date	: 	2015-11-30 [Paul] [ip.2.5.7.1.1]
#				Modified Add_User_To_eClassCourse_And_iCalendar(), removing the checking stristr($classNumber, $className)
#
#	Date	:	2015-08-14 [Pun]
#				Modify Create_Subject_Group() added sync subject group to eClass
#
#	Date	:	2015-08-14 [Pun]
#				sorting for $subject_class_mapping->Get_Subject_Group_Student_List()
#
#	Date	:	2015-08-13 [Pun]
#				Added Get_Subject_Group_Teacher_ListArr()
#
#	Date	:	2015-03-09 [Pun]
#				Fix Get_Class_Group_Teachers() array no init
#
#	Date	:	2015-02-03 [Omas]
#				improve Check_Timetable_Conflict_Student() to avoid find record from a deleted timetable
#
#	Date	:	2015-01-15 [Bill]
#				modifed Batch_Check_Create_Subject_Group(), to display alert message when out of Form-Subject relation
#
#	Date	:	2013-04-11 [Rita]
#				modified Get_User_Accessible_Subject_Group() add subjectID
#
#	Date	:	2012-08-23 [YatWoon]
#				update Get_Number_Of_Subject_Taken_With_FormRelationChecking(), add parameter for YearTermID [Case#2012-0823-1126-34071]
#
#	Date	:	2011-11-09 [Thomas]
#	Details :	modified Get_Subject_Directory, add c.course_id to select field in $sql
#
#	Date	:	2011-10-12 [Marcus]
#	Details	:	modified Get_Subject_Group_List(), added return value YearID
#
#	Date	:	2011-08-04 [Ivan]
#	Details	:	modified Create_Subject_Group(), for copy Subject Group, if the Subject Group is linked to a Classroom, the students will not set status to "deleted" now
#
#	Date	: 	2011-07-21 [Henry Chow]
#	Details	:	modified Get_Number_Of_Subject_Taken_With_FormRelationChecking(), display the "No. of Students Taken" of current semester
#
#	Date	: 	2011-07-20 [Henry Chow]
#	Details	:	added Get_Subject_Group_List_By_YearID(), return subject group info by YearID
#
#	Date	: 	2011-06-30 [Henry Chow]
#	Details	:	added getSubjectGroupIDByYearClassID(), for "Class > Class Detail"
#
#	Date	: 	2011-06-20 [Marcus]
#	Details	:	modified Check_Class_Code
#				add param $YearTermID,  cater using same subject group code in different term.  
#				modified constructor subject_term_class(), add param $YearTermID, $YearTermID is necessary if use SubjectGroupCode to retrieve SubjectGruop
#
#	Date	: 	2011-05-06 [Henry Chow]
#	Deatils	:	modified Get_Subject_Groups_By_SubjectStudent() & Get_Subject_Groups_By_SubjectID()
#				add "YearTermID" , there can subject group data of previous year / term  
#
#	Date	: 	2011-02-22 [Marcus]
#	Deatils	:	added returnSubjectTeacherClass (copy and modified from eRC), get all teaching class of a teacher 
#
#	Date	: 	2011-01-18 [Henry Chow]
#	Deatils	:	added Get_Subject_Groups_By_SubjectStudent(), for "Class > Subject Enrolment Detail"
#
#	Date	: 	2011-01-18 [Henry Chow]
#	Deatils	:	added getAllSubjectFormRelation(), retrieve all "Form Subject relation"
#
#	Date	: 	2011-01-17 [Henry Chow]
#	Deatils	:	added Get_Form_By_Subject(), get the linked "Form" by "Subject" from according to "Form-Subject Relation"
#
#	Date	: 	2011-01-12 [Henry Chow]
#	Deatils	:	added Get_Number_Of_Subject_Taken_With_FormRelationChecking(), get no. of subject taken in "Subject enrolment"
#
#	Date	: 	2010-12-29 [Henry Chow]
#	Deatils	:	added Delete_Subject_Year_Relation(), Create_Form_Subject(), delete & create the relation of "Subject" & "Form"
#
#	Date	: 	2010-12-23 [Henry Chow]
#	Deatils	:	added Get_All_Subjects(), Update_Form_Subject(), to display & update "Form-Subject" relation
#
#	Date	: 	2010-12-15 [Ivan]
#	Deatils	:	added Get_User_Accessible_Subject_Group() to get the Teacher teaching subject group info
#
#	Date	:	2010-12-14 [Henry Chow]
#	Details	:	added Get_Subject_By_Form(), get "Subject list" by "Form"
#
#	Date	:	2010-12-08 [Marcus]
#	Details	:	Modified Get_Subject_Group_Student_List, add return value YearID
#
#	Date	:	2010-11-22 [Ivan]
#	Details	:	Modified Create_Subject_Group(), add condition $sys_custom['SubjectGroup']['LockLogic']
#
#	Date	:	2010-11-22 [Henry Chow]
#	Details	:	add displaySubjectGroupSelection(), display Subject Group selection in "Student Mgmt"
#
#	Date	:	2010-11-09 [Marcus]
#	Details	:	modified Get_Subject_Group_List, add return field SubjectID
#
#	Date	:	2010-11-05 [Ivan]
#	Details	:	modified Batch_Create_Subject_Group(), Create_Subject_Group() to support Subject Group Lock Logic
#
#	Date	: 	2010-11-03 [Ivan]
#	Details	:	modified Get_Avaliable_Year_Class_User() to support getting the whole Form student
#
#	Date	:	2010-10-18 [Henry Chow]
#	Details	:	modified Edit_Subject_Group() - remove from Subject Leader list if user does not in new member list
#
#	Date	:	2010-10-07 [Ivan]
#	Details	:	modified Delete_Subject_Term_Class() - added the delete log logic
#
#	Date	:	2010-08-30 [Ivan]
#	Details	:	modified Batch_Create_Subject_Group() to edit the default Subject Group Name
#				past - AcademicYearName YearTermName SubjectName for ClassName
#				now	 - ClassName SubjectName
#
# 	Date	:	2010-08-03 [Ivan]
# 	Details	:	modified function Create_Subject_Group(), Edit_Subject_Group(), Get_Avaliable_Year_Class_User(), Copy_Subject_Group()
#				to support mapping of Subject Component to Subject Group
#
# 	Date	:	2010-07-13 [Ivan]
#	Details	:	modified Delete_Subject_Term_Class(). Delete subject leader and related homework list as well.
#
# 	Date	:	2010-04-16 [Yuen]
#	Details	:	add default max course storage setting
#				will be adopted when create new courses
#
#	20091222 Marcus:
#		- modified ClassTeacherList sql (Add NickName Field)
#	20091216 Ivan:
#			- Create_Subject_Group (Add $CopyFromSubjectGroupID for copy subject group function)
/******************* End Of Change Log *******************/

include_once('libdb.php');
include_once('libuser.php');
include_once('lib.php');
// include_once('libeclass40.php'); 
include_once('icalendar.php');


class subject_term_class extends libdb {
	var $SubjectGroupID;
	var $ClassCode;
	var $ClassTitleEN;
	var $ClassTitleB5;
	var $InternalClassCode;
	var $DateInput;
	var $InputBy;
	var $DateModified;
	var $ModifyBy;
	var $InputByName;
	var $ModifiedByName;
	var $course_id;
	
	var $SubjectID;
	
	var $YearTermID = array();
	var $YearID = array();
	var $ClassTeacherList = array();
	var $ClassStudentList = array();
	function subject_term_class($SubjectGroupID='', $GetTeacherList=false, $GetStudentList=false, $SubjectGroupCode='', $YearTermID='') {
		parent::libdb();
		if ($SubjectGroupID != '' || $SubjectGroupCode != '') {
			$ModifiedByNamefield = getNameFieldByLang('u.');
			$InputByNameField = getNameFieldByLang('u1.');
			
			$cond_SubjectGroupID = '';
			if ($SubjectGroupID != '')
				$cond_SubjectGroupID = ' AND stc.SubjectGroupID = \''.$SubjectGroupID.'\' ';
				
			$cond_SubjectGroupCode = $JoinSubjectTerm = '';
			if ($SubjectGroupCode != '' && $YearTermID != '')
			{
				$JoinSubjectTerm = ' INNER JOIN SUBJECT_TERM st ON st.SubjectGroupID = stc.SubjectGroupID AND st.YearTermID = \''.$YearTermID.'\' ';
				$cond_SubjectGroupCode = ' AND stc.ClassCode = \''.$this->Get_Safe_Sql_Query($SubjectGroupCode).'\' ';
			}
			
			$sql = 'Select 
								stc.SubjectGroupID, 
								stc.ClassCode, 
								stc.InternalClassCode,
								stc.ClassTitleEN, 
								stc.ClassTitleB5, 
								stc.DateInput, 
								stc.InputBy, 
								stc.DateModified, 
								stc.ModifyBy, 
								'.$InputByNameField.' as InputByName, 
								'.$ModifiedByNamefield.' as ModifiedByName,
								stc.course_id,
								stc.LockedYearClassID
							From 
								SUBJECT_TERM_CLASS stc
								'.$JoinSubjectTerm.'
								LEFT JOIN 
								INTRANET_USER u 
								on stc.ModifyBy = u.UserID 
								LEFT JOIN 
								INTRANET_USER u1 
								on stc.InputBy = u1.UserID 
							Where 
								1
								'.$cond_SubjectGroupID.'
								'.$cond_SubjectGroupCode.'
					';
// 					debug_pr($sql);
			$Result = $this->returnArray($sql);
			
			$this->SubjectGroupID = $Result[0]['SubjectGroupID'];
			
			if ($this->SubjectGroupID != '')
			{
				//$this->YearID = $Result[0]['YearID'];
				$this->ClassCode = $Result[0]['ClassCode'];
				$this->InternalClassCode = $Result[0]['InternalClassCode'];
				$this->ClassTitleEN = $Result[0]['ClassTitleEN'];
				$this->ClassTitleB5 = $Result[0]['ClassTitleB5'];
				$this->DateInput = $Result[0]['DateInput'];
				$this->InputBy = $Result[0]['InputBy'];
				$this->DateModified = $Result[0]['DateModified'];
				$this->ModifyBy = $Result[0]['ModifyBy'];
				$this->InputByName = $Result[0]['InputByName'];
				$this->ModifiedByName = $Result[0]['ModifiedByName'];
				$this->course_id = $Result[0]['course_id'];
				$this->LockedYearClassID = $Result[0]['LockedYearClassID'];
				
				$sql = 'Select 
									YearID 
								From 
									SUBJECT_TERM_CLASS_YEAR_RELATION 
								Where 
									SubjectGroupID = \''.$this->SubjectGroupID.'\'';
				$this->YearID = $this->returnVector($sql);								
				
				$sql = 'Select 
									YearTermID, 
									SubjectID,
									SubjectComponentID 
								From 
									SUBJECT_TERM 
								Where 
									SubjectGroupID = \''.$this->SubjectGroupID.'\'';
				$Result = $this->returnArray($sql);
				$this->YearTermID = array();
				for ($i=0; $i< sizeof($Result); $i++) {
					$this->YearTermID[] = $Result[$i]['YearTermID'];
					$this->SubjectID = $Result[$i]['SubjectID'];
					$this->SubjectComponentID = $Result[$i]['SubjectComponentID'];
				}
				
				if ($GetTeacherList) {
					$NameField = getNameFieldByLang('u.');
					$ArchiveNameField = getNameFieldByLang2('au.');
					$sql = 'Select 
										stct.UserID, 
										CASE 
											WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
											ELSE '.$NameField.' 
										END as TeacherName,
										u.EnglishName,
										u.ChineseName,
										u.NickName,
										u.UserLogin,
										u.TitleEnglish,
										u.TitleChinese
									From 
										SUBJECT_TERM_CLASS_TEACHER as stct
										LEFT JOIN 
										INTRANET_USER as u ON (stct.UserID = u.UserID)
										LEFT JOIN 
										INTRANET_ARCHIVE_USER as au ON (stct.UserID = au.UserID) 
									Where 
										stct.SubjectGroupID = \''.$this->SubjectGroupID.'\'
									';
					$this->ClassTeacherList = $this->returnArray($sql);
				}
				
				if ($GetStudentList) {
					$NameField = getNameFieldByLang('u.');
					$ArchiveNameField = getNameFieldByLang2('au.');
					/*
					$sql = 'select 
										stcu.UserID, 
										CASE 
											WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
											ELSE '.$NameField.' 
										END as StudentName,
										u.EnglishName,
										u.ChineseName,
										u.UserLogin
									from 
										SUBJECT_TERM_CLASS_USER as stcu
										LEFT JOIN 
										INTRANET_USER as u ON (stcu.UserID = u.UserID)
										LEFT JOIN 
										INTRANET_ARCHIVE_USER as au ON (stcu.UserID = au.UserID) 
									Where 
										stcu.SubjectGroupID = \''.$this->SubjectGroupID.'\'
									Order By
										u.ClassName ASC, u.ClassNumber ASC
								';
					*/
					//echo '<pre>'.$sql.'</pre>'; 
					//$this->ClassStudentList = $this->returnArray($sql);
					/*echo '<pre>';
					var_dump($this->ClassStudentList);
					echo '</pre>';*/
					
					$this->ClassStudentList = $this->Get_Subject_Group_Student_List();
				}
			}
		}
	}
	
	function Get_Class_Title() {
		return Get_Lang_Selection($this->ClassTitleB5,$this->ClassTitleEN);
	}
	
	function Get_All_Class_Teacher_List()
	{
		$sql = 'Select 
						DISTINCT(UserID)
				From 
						SUBJECT_TERM_CLASS_TEACHER ';
		$returnArr = $this->returnVector($sql);
		
		return $returnArr;
	}
	
	function Get_All_Class_Student_List()
	{
		$sql = 'Select 
						DISTINCT(UserID)
				From 
						SUBJECT_TERM_CLASS_USER ';
		$returnArr = $this->returnVector($sql);
		
		return $returnArr;
	}
	
	function Has_eclass(){ 
		global $eclass_db;
		$sql = "select s.course_id from SUBJECT_TERM_CLASS as s
				inner join {$eclass_db}.course as c on
				s.course_id = c.course_id and 
				s.course_id is not null
				where 
				s.SubjectGroupID = ".$this->SubjectGroupID;

		return (count($this->returnVector($sql))>0);
	}


	function Is_eClassCourse_Created_From_SubjectGroup()
	{
		global $eclass_db;
		$sql = "Select 
						c.IsFromSubjectGroup
				From 
						SUBJECT_TERM_CLASS as s 
						Inner Join 
						{$eclass_db}.course as c 
						On s.course_id = c.course_id
				Where
						s.SubjectGroupID = '".$this->SubjectGroupID."' 
						And 
						s.course_id is not null
				";
		$groupDetail = $this->returnArray($sql);
		$isFromSubjectGroup = ($groupDetail[0]["IsFromSubjectGroup"]==1);
		
		return $isFromSubjectGroup;
	}
	
	function Get_eClassCourse_CalendarID()
	{
		global $eclass_db;
		$sql = "Select 
						c.CalID 
				From 
						SUBJECT_TERM_CLASS as s 
						Inner Join 
						{$eclass_db}.course as c 
						On s.course_id = c.course_id
				Where 
						s.SubjectGroupID = '".$this->SubjectGroupID."' 
						And 
						s.course_id is not null
				";
		$groupDetail = $this->returnArray($sql);
		$calID = $groupDetail[0]["CalID"];
		
		return $calID;
	}
	
	function Get_Applicable_Form_Name_List()
	{
		$sql = 'Select 
						y.YearName 
				From 
						SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr
						INNER JOIN
						YEAR as y
						ON (stcyr.YearID = y.YearID)
				Where 
						stcyr.SubjectGroupID = \''.$this->SubjectGroupID.'\'';
		$ApplicableFormArr = $this->returnVector($sql);
		
		return $ApplicableFormArr;
	}
	
	function Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = false, $showYearName = false)
	{
		if ($SortingOrder == '')
			$SortingOrder = 'asc';
		
		# Order By Class Name and Class Number By Default
		if ($OrderByStudentName == 0)
			$OrderBy_SQL = 'y.Sequence '.$SortingOrder.', yc.Sequence '.$SortingOrder.', ytcu.ClassNumber '.$SortingOrder;
		else
			$OrderBy_SQL = 'u.EnglishName '.$SortingOrder;
			
		$StylePrefix = '';
		$StyleSuffix = '';
		if ($WithStyle==1)
		{
			$StylePrefix = '<font style="color:red;">';
			$StyleSuffix = '</font>';
		}
			
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		### Select Student in Class Start ###
		if($showClassID){
		    $classIDField = ',yc.YearClassID';
		}
		if($showYearName){
		    $yearNameField = ',y.YearName';
		}
		$sql = 'select 
					stcu.UserID, 
					CASE 
						WHEN au.UserID IS NOT NULL then CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\','.$ArchiveNameField.') 
						WHEN u.RecordStatus = 3  THEN CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\','.$NameField.') 
						ELSE '.$NameField.' 
					END as StudentName,
					CASE 
						WHEN au.UserID IS NOT NULL then CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',au.EnglishName) 
						WHEN u.RecordStatus = 3  THEN CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',u.EnglishName) 
						ELSE u.EnglishName
					END as StudentNameEn,
					CASE 
						WHEN au.UserID IS NOT NULL then CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',au.ChineseName) 
						WHEN u.RecordStatus = 3  THEN CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',u.ChineseName) 
						ELSE u.ChineseName
					END as StudentNameCh,
					u.EnglishName,
					u.ChineseName,
					u.UserLogin,
                    u.WebSAMSRegNo,
                    u.Gender,
					yc.ClassTitleEN,
					yc.ClassTitleB5,
                    
					ytcu.ClassNumber,
					y.YearID,
					stcu.SubjectGroupID
                    '.$classIDField.'
                    '.$yearNameField.'
                    
				from 
					SUBJECT_TERM_CLASS_USER as stcu
					Inner Join
					SUBJECT_TERM as st ON (stcu.SubjectGroupID = st.SubjectGroupID)
					Left Join
					INTRANET_USER as u ON (stcu.UserID = u.UserID)
					Left Join
					INTRANET_ARCHIVE_USER as au ON (stcu.UserID = au.UserID) 
					Left Join
					YEAR_CLASS_USER as ytcu ON (stcu.UserID = ytcu.UserID)
					Inner Join
					YEAR_CLASS as yc ON (ytcu.YearClassID = yc.YearClassID)
					Inner Join
					YEAR as y ON (yc.YearID = y.YearID)
					Inner Join
					ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID)
					Inner Join
					ACADEMIC_YEAR as ay ON (ayt.AcademicYearID = ay.AcademicYearID And yc.AcademicYearID = ay.AcademicYearID)
				Where 
					stcu.SubjectGroupID = \''.$this->SubjectGroupID.'\'
				Order By
					'.$OrderBy_SQL.'
				';
// 		debug_pr($sql);
		$temp =  $this->returnArray($sql);
//  		debug_pr($temp);
		### Select Student in Class END ###
		//echo '<pre>'.$sql.'</pre>'; 
		### Select Student Not in Class START###
		$student = Get_Array_By_Key($temp, "UserID");
		$student_sql = implode("','",$student);
		if(!empty($student_sql)){
			$cond = " AND
			stcu.UserID NOT in ('$student_sql')";
		}
		$sql = 'select 
					stcu.UserID, 
					CASE 
						WHEN au.UserID IS NOT NULL then CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\','.$ArchiveNameField.') 
						WHEN u.RecordStatus = 3  THEN CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\','.$NameField.') 
						ELSE '.$NameField.' 
					END as StudentName,
					CASE 
						WHEN au.UserID IS NOT NULL then CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',au.EnglishName) 
						WHEN u.RecordStatus = 3  THEN CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',u.EnglishName) 
						ELSE u.EnglishName
					END as StudentNameEn,
					CASE 
						WHEN au.UserID IS NOT NULL then CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',au.ChineseName) 
						WHEN u.RecordStatus = 3  THEN CONCAT(\''.$StylePrefix.'*'.$StyleSuffix.'\',u.ChineseName) 
						ELSE u.ChineseName
					END as StudentNameCh,
					u.EnglishName,
					u.ChineseName,
					u.UserLogin,
					stcu.SubjectGroupID
                    '.$classIDField.'
				from 
					SUBJECT_TERM_CLASS_USER as stcu
				Left JOIN
					INTRANET_USER as u ON (stcu.UserID = u.UserID)
				Left Join
					INTRANET_ARCHIVE_USER as au ON (stcu.UserID = au.UserID)
				Where 
					stcu.SubjectGroupID = \''.$this->SubjectGroupID.'\'
				'.$cond;
		$temp2 =  $this->returnArray($sql);
// 		debug_pr($temp);
		### Select Student Not in Class END###
		$x = array_merge($temp,$temp2);
		
		return $x;
	}
	function Get_Existing_Teacher_List(){
		global $eclass_db;
		$sql = "select s.course_id from SUBJECT_TERM_CLASS as s
				inner join {$eclass_db}.course as c on
				s.course_id = c.course_id and 
				s.course_id is not null
				where 
				s.SubjectGroupID = ".$this->SubjectGroupID;
		$result = $this->returnVector($sql);
		$course_id = $result[0];		
		$lo = new libeclass($course_id);
		$ec_db_name = $lo->db_prefix."c$course_id";
		$sql = "SELECT user_id, LTRIM(CONCAT(lastname, ' ', firstname)) as t_name, user_email ";
		$sql .= "FROM ".$ec_db_name.".usermaster ";
		$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') ";
		$sql .= "ORDER BY t_name ";
		$row_e = $lo->returnArray($sql, 3); //echo $sql;

		$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
		$sql .= "FROM INTRANET_USER ";
		$sql .= "WHERE RecordType = 1 ";
		$sql .= "ORDER BY EnglishName ";
		$row_i = $this->returnArray($sql, 4);
		return Array('in_class'=>$row_e,'out_class'=>$row_i);
	}
	
	/*
	 *	For copy Subject Group, broken the link between the eClass and the old Subject Group
	 *	Store the course id to old_course_id for future reference
	 */
	function UnLink_eClass()
	{
		$sql = "Update
						SUBJECT_TERM_CLASS
				Set
						course_id = null,
						old_course_id = '".$this->course_id."'
				Where
						SubjectGroupID = '".$this->SubjectGroupID."'
				";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Copy_Subject_Group($ToYearTermID, $NewSubjectGroupCode, $CopyStudent, $CopyTeacher, $Create_eClass, $CopyLeader)
	{
		if ($this->SubjectGroupID == '')
			return false;
			
		# Prepare Data
		$YearIDArr 			= $this->YearID;
		$SubjectID 			= $this->SubjectID;
		$ClassTitleEN 		= $this->ClassTitleEN;
		$ClassTitleB5 		= $this->ClassTitleB5;
		$course_id 			= $this->course_id;
		$SubjectComponentID = $this->SubjectComponentID;
		
		# Get Current Date Time (keep ClassCode uniquiness)
		$CurDateTime = date('m_d_H_i_s');
		
		if ($Create_eClass == 1)
			$thisCreateEClass = true;
		else
			$thisCreateEClass = ($course_id != '' && $course_id != 0)? true : false;
		
		# Get Student Array
		$StudentArr = array();
		if ($CopyStudent)
			$StudentArr = Get_Array_By_Key($this->ClassStudentList, 'UserID');
			
		# Get Teacher Array
		$TeacherArr = array();
		if ($CopyTeacher)
			$TeacherArr = Get_Array_By_Key($this->ClassTeacherList, 'UserID');
			
		# Get Leader Array
		if ($CopyLeader){
			$sql = "select UserID from INTRANET_SUBJECT_LEADER where ClassID = {$this->SubjectGroupID} and SubjectID = {$this->SubjectID} 
			and RecordStatus = 1";
			$LeaderArr = $this->returnVector($sql);
		}
		
		# Create the subject group
		$libSCM = new subject_class_mapping();
		$SuccessArr['Create_Subject_Group'] = $libSCM->Create_Subject_Group($YearIDArr, $ToYearTermID, $SubjectID, 
												$ClassTitleEN, $ClassTitleB5, $NewSubjectGroupCode, $StudentArr, $TeacherArr, 
												'', $thisCreateEClass, $this->SubjectGroupID,$Create_eClass,'',$Create_eClass, $LeaderArr, $SubjectComponentID);
	}
	
	function displaySubjectGroupSelection($ID=0, $yearTermID='')
	{
		global $PATH_WRT_ROOT, $Lang, $i_eNews_AddTo, $i_eNews_DeleteTo;
		
		if($yearTermID=='') $yearTermID = getCurrentSemesterID();
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$laccount = new libaccountmgmt();
		$linterface = new interface_html();
		$sbj = new subject();
		$stdinfo = $laccount->getClassNameClassNoByUserID($ID, Get_Current_Academic_Year_ID());


		$classInfo = $laccount->getClassNameClassNoByUserID($ID);
		$sbjGroups = $sbj->Get_Subject_Group_List(getCurrentSemesterID(), $classInfo['YearID'],"","","",$stdinfo['YearClassID']);

		$sql = "SELECT st.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER stcu LEFT OUTER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID) WHERE stcu.UserID='$ID' AND st.YearTermID='$yearTermID'";
		$joinedGroups = $this->returnVector($sql);
		
		$x .= "<table width=100% border=0 cellpadding=5 cellspacing=0>\n";
		$x .= "<tr>\n";
		$x .= "<td class=tableContent width=50%>\n";
		
		$x .= "<select name=sbjGpID[] id=sbjGpID size=10 multiple>\n";
		
		$allGp = "";
		for($i=0; $i<sizeof($sbjGroups); $i++)
		{			
			$gpID = $sbjGroups[$i][0];
			$gpName = Get_Lang_Selection($sbjGroups[$i][2], $sbjGroups[$i][1]);
			$IsCheck = (in_array($gpID, $joinedGroups)) ? 1 : 0;
			$x .= ($IsCheck) ? "<option value=$gpID>$gpName</option>\n" : "";
			
			$allGp .= ($allGp=="") ? $gpID : ",".$gpID;
		}
		$x .= "<option>\n";
		for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
		$x .= "</option>\n";
		$x .= "</select>\n";
		$x .= "</td>\n";
		$x .= "<td class=tableContent align=center>\n";
		
		
		$x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "button", "checkOptionTransfer(this.form.elements['AvailableSbjGpID[]'],this.form.elements['sbjGpID[]']);reload_Available_SubjectGroup();return false;", "submit11") . "<br /><br />";
		$x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "button", "checkOptionTransfer(this.form.elements['sbjGpID[]'],this.form.elements['AvailableSbjGpID[]']);reload_Available_SubjectGroup();return false;", "submit12");
		
		$x .= "</td>\n";
		$x .= "<td class=tableContent width=50%>\n";
		$x .= "<div id='DivAvailableSbjGp'>";
		$x .= "<select name=AvailableSbjGpID[] id=AvailableSbjGpID size=10 multiple>\n";
		
		
		for($i=0; $i<sizeof($sbjGroups); $i++)
		{			
			$gpID = $sbjGroups[$i][0];
			$gpName = Get_Lang_Selection($sbjGroups[$i][2], $sbjGroups[$i][1]);
			$IsCheck = (in_array($gpID, $joinedGroups)) ? 1 : 0;
			$x .= ($IsCheck) ? "" : "<option value=$gpID>$gpName</option>\n";
		}
		$x .= "</select>\n";
		$x .= "</div>\n";
		$x .= "</td>\n";
		$x .= "</tr>\n";
		$x .= "</table>\n";
		
		$x .= "<input type='hidden' name='allGroups' id='allGroups' value='$allGp'>";
		
		return $x;
	}		
	
}

class Learning_Category extends libdb { 
	var $LearningCategoryID;
	var $Code;
	var $NameEng;
	var $NameChi;
	var $DisplayOrder;
	var $RecordType;
	var $RecordStatus;
	var $DateInput;
	var $DateModified;
	var $LastModifiedBy;
	
	var $TableName;
	var $ID_FieldName;
	var $RecordID;
	
	
	function Learning_Category($LearningCategoryID=''){
		parent:: libdb();
		$this->TableName = "LEARNING_CATEGORY";
		$this->ID_FieldName = "LearningCategoryID";
		
		if ($LearningCategoryID!='')
		{
			$this->RecordID = $LearningCategoryID;
			
			$sql = "";
			$sql .= " SELECT * FROM ".$this->TableName;
			$sql .= " WHERE ".$this->ID_FieldName." = '".$this->RecordID."'";
			
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			foreach ($infoArr as $key => $value)
				$this->{$key} = $value;
		}
	}
	
	function Get_All_Learning_Category($returnAsso=0) 
	{
		$sql = "SELECT * FROM ".$this->TableName." WHERE RecordStatus = '1' ORDER BY DisplayOrder";
		$resultSet = $this->returnArray($sql);
		
		if ($returnAsso == 0)
		{
			return $resultSet;
		}
		else
		{
			$numOfLC = count($resultSet);
			$returnArr = array();
			for ($i=0; $i<$numOfLC; $i++)
			{
				$thisLearningCategoryID = $resultSet[$i]['LearningCategoryID'];
				
				$returnArr[$thisLearningCategoryID]['Code'] = $resultSet[$i]['Code'];
				$returnArr[$thisLearningCategoryID]['NameEng'] = $resultSet[$i]['NameEng'];
				$returnArr[$thisLearningCategoryID]['NameChi'] = $resultSet[$i]['NameChi'];
				$returnArr[$thisLearningCategoryID]['Name'] = Get_Lang_Selection($resultSet[$i]['NameChi'], $resultSet[$i]['NameEng']);
			}
			
			return $returnArr;
		}
	}
	
	function Get_Subject_List()
	{
		$sql = "SELECT 
						RecordID 
				FROM 
						ASSESSMENT_SUBJECT 
				WHERE 
						".$this->ID_FieldName." = '".$this->RecordID."' 
						AND 
						RecordStatus = 1
				ORDER BY
						DisplayOrder
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet;
	}
	
	function Insert_Record($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
			
		# Check if the record is in-active by the "NameEng", and "NameChi"
		$RecordID = $this->Is_Record_Existed($DataArr['NameEng'], $DataArr['NameChi']);
		if ($RecordID)
		{
			# set to active
			$objectClass = get_class($this);
			$libObject = new $objectClass($RecordID);
			
			$success = $libObject->Activate_Record();
			$success = $this->Update_Record($DataArr, $RecordID);
		}
		else
		{
			## insert data
			# DisplayOrder
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			
			# Last Modified By
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DisplayOrder
			$fieldArr[] = 'DisplayOrder';
			$valueArr[] = $thisDisplayOrder;
			# Last Modified By
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $LastModifiedBy;
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			# Set RecordStatus = active
			$fieldArr[] = 'RecordStatus';
			$valueArr[] = 1;
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success = $this->db_db_query($sql);
			if ($success == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
		}
		
		return $success;
	}
	
	function Update_Record($DataArr=array(), $RecordID='')
	{
		if (count($DataArr) == 0)
			return false;
			
		if ($RecordID == '')
			$RecordID = $this->RecordID;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModified = now(), ';
		$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
		$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName.' ';
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$RecordID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Is_Record_Existed($NameEng, $NameChi)
	{
		$sql = 'SELECT 
						'.$this->ID_FieldName.'
				FROM 
						'.$this->TableName.'
				WHERE 
						NameEng = \''.$this->Get_Safe_Sql_Query($NameEng).'\'
					AND
						NameChi = \''.$this->Get_Safe_Sql_Query($NameChi).'\'
				';
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return false;
		else
			return $resultSet[0];
	}
	
	function Get_Max_Display_Order()
	{
		$sql = '';
		$sql .= 'SELECT max(DisplayOrder) FROM '.$this->TableName;
		$MaxDisplayOrder = $this->returnVector($sql);
		
		return $MaxDisplayOrder[0];
	}
	
	function Get_Update_Display_Order_Arr($OrderText, $Separator=",", $Prefix="")
	{
		if ($OrderText == "")
			return false;
			
		$orderArr = explode($Separator, $OrderText);
		$orderArr = array_remove_empty($orderArr);
		$orderArr = Array_Trim($orderArr);
		
		$numOfOrder = count($orderArr);
		# display order starts from 1
		$counter = 1;
		$newOrderArr = array();
		for ($i=0; $i<$numOfOrder; $i++)
		{
			$thisID = str_replace($Prefix, "", $orderArr[$i]);
			if (is_numeric($thisID))
			{
				$newOrderArr[$counter] = $thisID;
				$counter++;
			}
		}
		
		return $newOrderArr;
	}
	
	function Update_DisplayOrder($DisplayOrderArr=array())
	{
		if (count($DisplayOrderArr) == 0)
			return false;
			
		$this->Start_Trans();
		for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
			$thisBuildingID = $DisplayOrderArr[$i];
			
			$sql = 'UPDATE '.$this->TableName.' SET 
								DisplayOrder = \''.$i.'\' 
							WHERE 
								'.$this->ID_FieldName.' = \''.$thisBuildingID.'\'';
			$Result['ReorderResult'.$i] = $this->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) 
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Delete_Record()
	{
		$success = $this->Inactivate_Record();
		return $success;
	}
	
	function Inactivate_Record()
	{
		$DataArr['RecordStatus'] = "0";
		$success = $this->Update_Record($DataArr);
		
		return $success;
	}
	
	function Activate_Record()
	{
		$DataArr['RecordStatus'] = 1;
		
		# assign to the last display for activated records
		$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
		$DataArr['DisplayOrder'] = $thisDisplayOrder;
		
		$success = $this->Update_Record($DataArr);
		return $success;
	}
	
	function Is_Title_Existed($RecordType, $TargetTitle, $TargetTitleEn='')
	{
		if ($RecordType == "LC_TitleEn")
			$TitleField = "NameEng";
		else
			$TitleField = "NameChi";
			
		$conds = '';
		if ($this->LearningCategoryID != '')
			$conds = " AND LearningCategoryID != '".$this->LearningCategoryID."' ";
		
		$sql = "SELECT
						DISTINCT(LearningCategoryID)
				FROM
						".$this->TableName."
				WHERE
						RecordStatus = '1'
					AND
						$TitleField = '$TargetTitle'
					$conds
				";
		$resultSet = $this->returnVector($sql);
		$isExistedTitle = (count($resultSet)==0)? false : true;
		
		if ($RecordType=="Both" && $isExistedTitle==false)
		{
			# Check TitleEn as well if need to check Both title
			$isExistedTitleEn = $this->Is_Title_Existed("LC_TitleEn", $TargetTitleEn);
			
			return $isExistedTitleEn;
		}
		else
		{
			return $isExistedTitle;
		}
	}
	
	function Is_Code_Existed($TargetCode)
	{
		$conds = '';
		if ($this->LearningCategoryID != '')
			$conds = " AND LearningCategoryID != '".$this->LearningCategoryID."' ";
		
		$sql = "SELECT
						DISTINCT(LearningCategoryID)
				FROM
						".$this->TableName."
				WHERE
						RecordStatus = '1'
					AND
						Code = '$TargetCode'
					$conds
				";
		$resultSet = $this->returnVector($sql);
		$isExistedTitle = (count($resultSet)==0)? false : true;
		
		return $isExistedTitle;
	}
	
	function Get_LearningCategoryID_By_Code($Code)
	{
		$sql = "Select
						LearningCategoryID
				From
						LEARNING_CATEGORY
				Where
						Code = '".$this->Get_Safe_Sql_Query($Code)."'
						And
						RecordStatus = 1
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet[0];
	}
	
	function Get_LearningCategory_Name() {
		return Get_Lang_Selection($this->NameChi, $this->NameEng);
	}
}

class subject extends libdb {
	var $RecordID;
	var $CH_SNAME;
	var $EN_SNAME;
	var $CH_DES;
	var $EN_DES;
	var $EN_ABBR;
	var $CH_ABBR;
	var $DisplayOrder;
	var $CODEID;
	var $InputDate;
	var $ModifiedDate;
	var $LastModifiedBy;
	var $CMP_CODEID;
	var $LearningCategoryID;
	
	var $TableName;
	var $ID_FieldName;
	
	var $YearRelationDetail;
	var $YearRelationID;
	
	function subject($RecordID="",$GetFormRelation=false) {
		parent::libdb();
		
		$this->TableName = "ASSESSMENT_SUBJECT";
		$this->ID_FieldName = "RecordID";
		
		if ($RecordID != "") {
			$this->RecordID = $RecordID;
			
			$sql = 'select 
								*
					From 
						'.$this->TableName.'
					Where 
						'.$this->ID_FieldName.' = \''.$RecordID.'\'';
			$Result = $this->returnArray($sql);
			$infoArr = $Result[0];
			
			foreach ((array)$infoArr as $key => $value)
			{
				$this->{$key} = $value;
			}	
			if ($GetFormRelation) {
				$sql = "select 
									y.YearID, 
									y.YearName 
								from 
									SUBJECT_YEAR_RELATION syr 
									inner join 
									YEAR y 
									on syr.YearID = y.YearID 
										and 
										syr.SubjectID = '".$RecordID."' 
								order by 
									y.Sequence";
				
				$this->YearRelationDetail = $this->returnArray($sql);
				for ($i=0; $i< sizeof($this->YearRelationDetail); $i++) {
					$this->YearRelationID[] = $this->YearRelationDetail[$i]['YearID'];
				}
			}
		}
	}
	
	function Get_Subject_Desc() {
		return Get_Lang_Selection($this->CH_DES,$this->EN_DES);
	}
	
	function Get_Subject_Short_Form() {
		return Get_Lang_Selection($this->CH_SNAME,$this->EN_SNAME);
	}
	
	function Get_Subject_Abbr() {
		return Get_Lang_Selection($this->CH_ABBR,$this->EN_ABBR);
	}
	
	function Get_Subject_List($returnAssociate=0, $withLearningCategory=1) {
		
		$learningCategoryCond = '';
		if ($LearningCategoryID!='')
			$learningCategoryCond = " AND LearningCategoryID = '$LearningCategoryID' ";
		
		$sql = "SELECT 		
						*,
						RecordID as SubjectID
				FROM 		
						$this->TableName
				WHERE 		
						RecordStatus = '1' 
					AND
						(CMP_CODEID IS NULL OR CMP_CODEID = '')
					$learningCategoryCond
				ORDER BY 	DisplayOrder";
		$resultSet = $this->returnArray($sql);
		$numOfSubject = count($resultSet);
		
		$returnArr = array();
		if ($returnAssociate)
		{
			for ($i=0; $i<$numOfSubject; $i++)
			{
				$thisSubjectInfoArr = $resultSet[$i];
				$thisLearningCategoryID = $thisSubjectInfoArr['LearningCategoryID'];
				$thisRecordID = $thisSubjectInfoArr['RecordID'];
				$thisLearningCategoryID = ($thisLearningCategoryID=="")? 'temp' : $thisLearningCategoryID;
				
				if ($withLearningCategory == 1)
				{
					if (!isset($returnArr[$thisLearningCategoryID]))
						$returnArr[$thisLearningCategoryID] = array();
						
					# $returnArr[LearningCategoryID][RecordID] = $SubjectInfoArr[Title, Code....]
					$returnArr[$thisLearningCategoryID][$thisRecordID] = $thisSubjectInfoArr;
				}
				else
				{
					$returnArr[$thisRecordID] = $thisSubjectInfoArr;
				}
			}
		}
		else
		{
			$returnArr = $resultSet;
		}
		
		return $returnArr;
	}
	
	function Get_Subject_Component()
	{
		$CODEID_Cond = "";
		if ($this->CODEID != "")
			$CODEID_Cond = " AND CODEID = '".$this->CODEID."' ";
			
		$sql = "SELECT 
						* 
				FROM 
						".$this->TableName." 
				WHERE 
						RecordStatus = '1' 
					AND
						CMP_CODEID != ''
					$CODEID_Cond 
				ORDER BY 
						DisplayOrder";
		$resultSet = $this->returnArray($sql);
		
		return $resultSet;
	}
	
	function Is_Parent_Subject()
	{
		$componentArr = $this->Get_Subject_Component();
		
		if (count($componentArr) > 0)
			return true;
		else
			return false;
	}
	
	function Is_Component_Subject()
	{
		if ($this->CMP_CODEID == "")
			$Is_ComponentSubject = false;
		else
			$Is_ComponentSubject = true;
			
		return $Is_ComponentSubject;
	}
	
	function Get_Subject_Code()
	{
		if ($this->Is_Component_Subject())
			return $this->CMP_CODEID;
		else
			return $this->CODEID;
	}
	
	# Return the type of code existed
	function Is_Code_Existed($TargetCode, $TargetID='')
	{
		$TargetCode = trim($TargetCode);
		
		$subjectCond = " AND RecordID != '$TargetID' ";
		
		# Check Subject Code
		$sql_subject = "SELECT 
								DISTINCT(RecordID) 
						FROM 
								".$this->TableName." 
						WHERE 
								(CODEID = '".$TargetCode."' AND (CMP_CODEID = '' OR CMP_CODEID IS NULL))
								AND 
								RecordStatus = '1'
								$subjectCond
						";
		$subjectCodeArr = $this->returnVector($sql_subject);
		$isSubjectCode = (count($subjectCodeArr)==0)? false : true;
		
		if ($isSubjectCode)
			return "Subject";
		else
			return "";
	}
	
	function Insert_Record($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
			
		# Check if the record is in-active by the "CODEID"
		$RecordID = $this->Is_Record_Existed($DataArr['CODEID']);
		if ($RecordID)
		{
			# set to active
			$objectClass = get_class($this);
			$libObject = new $objectClass($RecordID);
			
			$success['Activate'] = $libObject->Activate_Record();
			$success['Update'] = $libObject->Update_Record($DataArr);
		}
		else
		{
			## insert data
			# DisplayOrder
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			
			# UniqueRecordID
			//$thisUniqueRecordID = $this->Get_Max_UniqueRecordID() + 1;
			
			# Last Modified By
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DisplayOrder
			$fieldArr[] = 'DisplayOrder';
			$valueArr[] = $thisDisplayOrder;
			# Last Modified By
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $LastModifiedBy;
			# InputDate
			$fieldArr[] = 'InputDate';
			$valueArr[] = 'now()';
			# ModifiedDate
			$fieldArr[] = 'ModifiedDate';
			$valueArr[] = 'now()';
			# Set RecordStatus = active
			$fieldArr[] = 'RecordStatus';
			$valueArr[] = 1;
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success['Insert'] = $this->db_db_query($sql);
			
			if ($success['Insert'] == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
				
		}
		
		if (in_array(false, $success))
			return false;
		else
			return true;
	}
	
	function Update_Record($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
			
		$Old_CODEID = $this->CODEID;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' ModifiedDate = now(), ';
		$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
		$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName.' ';
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->RecordID.'\' ';
		$success['update'] = $this->db_db_query($sql);
		
		if ($this->Is_Parent_Subject() && $DataArr['CODEID'] != "")
		{
			# update the subject code of the corresponding subject components
			$sql_component = '';
			$sql_component .= ' UPDATE '.$this->TableName.' ';
			$sql_component .= ' SET CODEID = \''.$DataArr['CODEID'].'\' ';
			$sql_component .= ' WHERE CODEID = \''.$Old_CODEID.'\' ';
			$success['update_component'] = $this->db_db_query($sql_component);
		}
		
		if (in_array(false, $success))
			return false;
		else
			return true;
	}
	
	function Is_Record_Existed($CODEID)
	{
		$sql = 'SELECT 
						'.$this->ID_FieldName.'
				FROM 
						'.$this->TableName.'
				WHERE 
						CODEID = \''.$this->Get_Safe_Sql_Query($CODEID).'\'
						And
						RecordStatus = 1
				';
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return false;
		else
			return $resultSet[0];
	}
	
	function Delete_Record()
	{
		$success = $this->Inactivate_Record();
		return $success;
	}
	
	function Inactivate_Record()
	{
		$DataArr['RecordStatus'] = "0";
		$success = $this->Update_Record($DataArr);
		
		return $success;
	}
	
	function Activate_Record()
	{
		$DataArr['RecordStatus'] = 1;
		
		# assign to the last display for activated records
		$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
		$DataArr['DisplayOrder'] = $thisDisplayOrder;
		
		$success = $this->Update_Record($DataArr);
		return $success;
	}
	
	function Get_Max_Display_Order()
	{
		$sql = '';
		$sql .= 'SELECT max(DisplayOrder) FROM '.$this->TableName;
		$MaxDisplayOrder = $this->returnVector($sql);
		
		return $MaxDisplayOrder[0];
	}
	
	function Get_Update_Display_Order_Arr($OrderText, $Separator=",", $Prefix="")
	{
		if ($OrderText == "")
			return false;
			
		$orderArr = explode($Separator, $OrderText);
		$orderArr = array_remove_empty($orderArr);
		$orderArr = Array_Trim($orderArr);
		
		$numOfOrder = count($orderArr);
		# display order starts from 1
		$counter = 1;
		$newOrderArr = array();
		
		foreach ($orderArr as $key => $RowID)
		{
			$thisID = str_replace($Prefix, "", $RowID);
			
			if (is_numeric($thisID))
				$newOrderArr[$counter++] = $thisID;
		}
		
		return $newOrderArr;
	}
	
	function Update_DisplayOrder($DisplayOrderArr=array())
	{
		if (count($DisplayOrderArr) == 0)
			return false;
			
		$this->Start_Trans();
		for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
			$thisRecordID = $DisplayOrderArr[$i];
			
			$sql = 'UPDATE '.$this->TableName.' SET 
								DisplayOrder = \''.$i.'\' 
							WHERE 
								'.$this->ID_FieldName.' = \''.$thisRecordID.'\'';
			$Result['ReorderResult'.$i] = $this->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) 
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	/*
	function Get_Max_UniqueRecordID()
	{
		$sql = '';
		$sql .= 'SELECT 
						MAX(GREATEST(s.UniqueRecordID, sc.UniqueRecordID))
				FROM 
						SUBJECT as s 
					INNER JOIN
						SUBJECT_COMPONENT as sc on s.CODEID = sc.SubjectCode
				';
		$MaxUniqueRecordID = $this->returnVector($sql);
		
		return $MaxUniqueRecordID[0];
	}
	*/
	
	function Get_Subject_Group_List($YearTermID='', $ClassLevelID='', $SubjectGroupID='', $TeacherID='', $returnAsso=0, $YearClassID='', $SubjectID='', $InYearClassOnly=false)
	{

		$cond_RecordID = '';
		if ($this->RecordID == '')
			$cond_RecordID = '';
		else
			$cond_RecordID = " And s.RecordID = '".$this->RecordID."' ";
		
		
		$cond_YearTermID = '';
		if ($YearTermID!='')
			$cond_YearTermID = " AND st.YearTermID IN (".implode(",",(array)$YearTermID).") ";
			
		$cond_ClassLevelID = '';
		if ($ClassLevelID!='')
			$cond_ClassLevelID = " AND stcyr.YearID = '$ClassLevelID' ";
			
		$cond_SubjectGroupID = '';
		if ($SubjectGroupID!='')
		{
			if (is_array($SubjectGroupID) == false)
			{
				$cond_SubjectGroupID = " AND st.SubjectGroupID = '$SubjectGroupID' ";
			}
			else
			{
				$SubjectGroupList = implode(',', $SubjectGroupID);
				$cond_SubjectGroupID = " AND st.SubjectGroupID In ($SubjectGroupList) ";
			}
		}
			
		$cond_Teaching = '';
		if ($TeacherID!='')
			$cond_Teaching = " AND stct.UserID = '$TeacherID' ";
		
		$table_YearClassID = '';
		$cond_YearClassID = '';
		if($YearClassID!='') {
			
			$table_YearClassID = " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID AND ycu.YearClassID='$YearClassID')";
			//$cond_YearClassID = " AND ycu.YearClassID='$YearClassID'";
			
			if ($InYearClassOnly) {
				$cond_YearClassID = " AND ycu.YearClassID='$YearClassID'";
			}
			
		}
		
		if ($SubjectID!="")
		{
			$cond_subject = " AND s.RecordID='{$SubjectID}' ";
		}
			
		$sql = "";
		$sql .= "	SELECT 
							st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5, stc.course_id, st.SubjectComponentID, stc.LockedYearClassID, s.RecordID, stcyr.YearID
					FROM 
							ASSESSMENT_SUBJECT as s 
							Inner Join
							SUBJECT_TERM as st ON (s.RecordID = st.SubjectID)
							Inner Join
							SUBJECT_TERM_CLASS as stc ON (st.SubjectGroupID = stc.SubjectGroupID)
							Left Outer Join
							SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr ON (st.SubjectGroupID = stcyr.SubjectGroupID)
							Left Outer Join
							SUBJECT_TERM_CLASS_TEACHER as stct ON (stcyr.SubjectGroupID = stct.SubjectGroupID)
							$table_YearClassID
					WHERE
							1
							$cond_RecordID
							$cond_YearTermID
							$cond_SubjectGroupID
							$cond_ClassLevelID
							$cond_Teaching
							$cond_YearClassID
							$cond_subject
					Group By
							st.SubjectGroupID
					Order by 
							stc.ClassTitleEN
				";
		
		$resultSet = $this->returnArray($sql);
		
		$returnArr = array();
		if ($returnAsso == 1)
		{
			$numOfSubjectGroup = count($resultSet);
			for ($i=0; $i<$numOfSubjectGroup; $i++)
			{
				$thisSubjectGroupID = $resultSet[$i]['SubjectGroupID'];
				$returnArr[$thisSubjectGroupID] = $resultSet[$i];
			}
		}
		else
		{
			$returnArr = $resultSet;
		}
		
		return $returnArr;
	}
	
	function getSubjectList($include_sub_subject=1)
	{
		$sql = "SELECT ".Get_Lang_Selection("CH_DES","EN_DES").", CODEID FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 ";
		if(!$include_sub_subject)
		{
			$sql .= " and (CMP_CODEID is NULL or CMP_CODEID = '') ";
		}
		$sql .= " ORDER BY DisplayOrder";
		$result = $this->returnArray($sql, 2);
		return $result;
	}
	
	function Is_Subject_Component_Code($Code)
	{
		$sql = "SELECT
						RecordID
				FROM
						".$this->TableName."
				WHERE
						CMP_CODEID = '".$this->Get_Safe_Sql_Query($Code)."'
						And
						RecordStatus = 1
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	
	function Get_Subject_By_SubjectCode($Code, $isComponent=0, $ParentCode='', $ActiveOnly=1)
	{
		if ($isComponent)
		{
			$cond_code = " (CMP_CODEID = '".$Code."' And CODEID = '".$ParentCode."') ";
		}
		else
		{
			$cond_code = " (CODEID = '".$Code."' AND (CMP_CODEID = '' OR CMP_CODEID IS NULL)) ";
		}
		
		$conds_recordstatus = '';
		if ($ActiveOnly == 1)
			$conds_recordstatus = " And RecordStatus = '1' ";
		
		$sql = "SELECT
						*
				FROM
						".$this->TableName."
				WHERE
						$cond_code
						$conds_recordstatus
				";
		$resultSet = $this->returnArray($sql);
		
		return $resultSet;
	}
	
	function Get_Subject_By_Form($YearID="")
	{
		if($YearID!="") {
			$sql = "SELECT RecordID, CH_DES, EN_DES FROM ASSESSMENT_SUBJECT SUB INNER JOIN SUBJECT_YEAR_RELATION syr ON (SUB.RecordID=syr.SubjectID) WHERE syr.YearID='$YearID' AND SUB.RecordStatus = 1 AND (CMP_CODEID is NULL or CMP_CODEID = '')";
			$result = $this->returnArray($sql, 3);
			
			return $result;
		}
	}
	
	function Get_Form_By_Subject($SubjectID="")
	{
		if($SubjectID!="") {
			$sql = "SELECT y.YearID, YearName FROM YEAR y INNER JOIN SUBJECT_YEAR_RELATION syr ON (y.YearID=syr.YearID) WHERE syr.SubjectID='$SubjectID' AND y.RecordStatus=1";
			$result = $this->returnArray($sql, 3);
			
			return $result;
		}
	}
		
	function Get_All_Subjects()
	{
		$sql = "SELECT RecordID, CH_DES, EN_DES FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 AND (CMP_CODEID is NULL or CMP_CODEID = '') ORDER BY DisplayOrder";
		return $this->returnArray($sql);	
	}
	
	function Update_Form_Subject($YearID="", $SubjectID=array())
	{
		global $UserID;
		
		if($YearID!="") {
			
			$ReturnAry = array();
			
			$sql = "DELETE FROM SUBJECT_YEAR_RELATION WHERE YearID='$YearID'";
			$ReturnAry[] = $this->db_db_query($sql);
			
			$delim = "";
			if(sizeof($SubjectID)>0) {
				
				$sql = "INSERT INTO SUBJECT_YEAR_RELATION VALUES ";
				foreach($SubjectID as $sbjID) {
					$sql .= $delim."('$sbjID','$YearID','$UserID', NOW(), '$UserID', NOW())";
					$delim = ",";	
				}
				
				$ReturnAry[] = $this->db_db_query($sql);
			}
			return (in_array(false, $ReturnAry)) ? 0 : 1;	
		}
	}
	
	function Get_Subject_Groups_By_SubjectID($SubjectID, $YearTermID="")
	{
		if($YearTermID=="") $YearTermID = GetCurrentSemesterID();
		
		$sql = "SELECT stc.SubjectGroupID, stc.ClassTitleB5, ClassTitleEN FROM SUBJECT_TERM_CLASS stc INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stc.SubjectGroupID) WHERE st.YearTermID='$YearTermID' AND st.SubjectID='$SubjectID'";
		$result = $this->returnArray($sql,3);
		//debug_pr($result);
		$tempAry = array();
		
		for($i=0; $i<sizeof($result); $i++) {
			list($subjectGroupID, $classB5, $classEN) = $result[$i];
			$tempAry[$subjectGroupID][] = $classB5;
			$tempAry[$subjectGroupID][] = $classEN;
		}
		return $tempAry;
	}
	
	
	function Get_Subject_Group_List_By_YearID($SubjectID, $YearID, $YearTermID="") 
	{
		
		$sql = "SELECT stc.SubjectGroupID, stc.ClassTitleB5, ClassTitleEN FROM SUBJECT_TERM_CLASS_YEAR_RELATION stcyr INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcyr.SubjectGroupID AND st.YearTermID='$YearTermID' AND st.SubjectID='$SubjectID') INNER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=st.SubjectGroupID) WHERE stcyr.YearID='$YearID'";
		$result = $this->returnArray($sql,3);
		$tempAry = array();
		for($i=0; $i<sizeof($result); $i++) {
			list($subjectGroupID, $classB5, $classEN) = $result[$i];
			$tempAry[$subjectGroupID][] = $classB5;
			$tempAry[$subjectGroupID][] = $classEN;
		}
		return $tempAry;
	}
	
	
	function Get_Subject_Groups_By_SubjectStudent($SubjectID, $YearClassID, $StudentID, $YearTermID="")
	{
		if($YearTermID=="") $YearTermID = getCurrentSemesterID();
		
		$sql = "SELECT AcademicYearID FROM YEAR_CLASS WHERE YearClassID='$YearClassID'";
		$temp = $this->returnVector($sql);
		$AcademicYearID = $temp[0];
// 		$allSemesters = getSemesters($AcademicYearID);
// 		if(sizeof($allSemesters) > 0) {
// 			$termID = array_keys($allSemesters);
// 			$conds = " AND st.YearTermID IN (".implode(',', $termID).")";	
// 		}
		
		$sql = "SELECT stc.SubjectGroupID, stc.ClassTitleB5, stc.ClassTitleEN FROM SUBJECT_TERM_CLASS stc INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stc.SubjectGroupID) INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=stc.SubjectGroupID) INNER JOIN YEAR_CLASS yc ON (yc.YearID=stcyr.YearID) INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) WHERE st.SubjectID='$SubjectID' AND yc.AcademicYearID='$AcademicYearID' AND ycu.UserID='$StudentID' AND yc.YearClassID='$YearClassID' AND st.YearTermID='$YearTermID' ORDER BY stc.ClassTitleEN";
		$result = $this->returnArray($sql,3);
		//echo $sql.'<br>';
		$tempAry = array();
		
		for($i=0; $i<sizeof($result); $i++) {
			list($subjectGroupID, $classB5, $classEN) = $result[$i];
			$tempAry[$i][] = $subjectGroupID;
			$tempAry[$i][] = Get_Lang_Selection($classB5, $classEN);
		}
		//debug_pr($tempAry);
		return $tempAry;
	}
	
	function Delete_Subject_Year_Relation()
	{
		$sql = "DELETE FROM SUBJECT_YEAR_RELATION";
		return $this->db_db_query($sql);	
	}
	
	function Create_Form_Subject($dataAry=array())
	{
		global $UserID;
		
		include_once("form_class_manage.php");
		$fcm = new form_class_manage();
		
		$FormList = $fcm->Get_Form_List();
		$SubjectList = $this->Get_All_Subjects();
		//debug_pr($dataAry);
		$values = "";
		$delim = "";
		
		for($i=0; $i<sizeof($SubjectList); $i++) {
			$subjectID = $SubjectList[$i]['RecordID'];
			for($j=0; $j<sizeof($FormList); $j++) {
				$yearID = $FormList[$j]['YearID'];
				$linked = $dataAry['SubjectGroup_'.$subjectID.'_'.$yearID];
				
				if($linked) {
					$values .= $delim."('$subjectID','$yearID','$UserID', NOW(), '$UserID', NOW())";
					$delim = ", ";
				}
			}	
		}
		
		//$sql = "INSERT INTO SUBJECT_YEAR_RELATION VALUES $values";
		$sql = "INSERT INTO SUBJECT_YEAR_RELATION (SubjectID, YearID, InputBy, DateInput, ModifyBy, DateModified) VALUES $values";
		return $this->db_db_query($sql);
		
	}	
	
	function getAllSubjectFormRelation()
	{
		$sql = "SELECT * FROM SUBJECT_YEAR_RELATION";
		return $this->returnArray($sql);	
	}
}


class Subject_Component extends subject {
	
	function Subject_Component($RecordID="") {
		parent::subject($RecordID);
		
	}
	
	function Get_Parent_Subject()
	{
		$sql = "SELECT
						* 
				FROM 
						".$this->TableName."
				WHERE 
						CODEID = '".$this->CODEID."'
					AND
						( CMP_CODEID IS NULL OR CMP_CODEID = '' ) and RecordStatus = 1
				";
		$resultSet = $this->returnArray($sql);
		
		return $resultSet[0];
	}
	
	### Check WebSAMS between all Parent Subjects and Component Subjects
	function Is_Code_Existed($TargetCode, $ExcludeID='', $ParentRecordID='')
	{
		$TargetCode = trim($TargetCode);
		
		$excludeSubjectID_Cond = '';
		if ($ExcludeID != '')
			$excludeSubjectID_Cond = " AND RecordID != '$ExcludeID' ";
		
		if ($ParentRecordID !='')
			$ParentSubject_Obj = new subject($ParentRecordID);
		$parentCODEID = $ParentSubject_Obj->CODEID;
		
		# Check Building Code
		$sql_subject = "SELECT
								DISTINCT(RecordID) 
						FROM 
								".$this->TableName." 
						WHERE 
								CMP_CODEID = '".$TargetCode."' 
								AND
								CODEID = '".$parentCODEID."'
								AND 
								RecordStatus = '1'
								$excludeSubjectID_Cond
						";
		$subjectCodeArr = $this->returnVector($sql_subject);
		$isSubjectComponentCode = (count($subjectCodeArr)==0)? false : true;
		
		if ($isSubjectComponentCode)
			return "SubjectComponent";
		else
			return "";
	}
	
	function Insert_Record($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
			
		# Check if the record is in-active by the "CODEID" and "CMP_CODEID"
		$RecordID = $this->Is_Record_Existed($DataArr['CODEID'], $DataArr['CMP_CODEID']);
		if ($RecordID)
		{
			# set to active
			$objectClass = get_class($this);
			$libObject = new $objectClass($RecordID);
			
			$success['Activate'] = $libObject->Activate_Record();
			$success['Update'] = $libObject->Update_Record($DataArr);
		}
		else
		{
			## insert data
			# DisplayOrder
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			
			# UniqueRecordID
			//$thisUniqueRecordID = $this->Get_Max_UniqueRecordID() + 1;
			
			# Last Modified By
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DisplayOrder
			$fieldArr[] = 'DisplayOrder';
			$valueArr[] = $thisDisplayOrder;
			# Last Modified By
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $LastModifiedBy;
			# InputDate
			$fieldArr[] = 'InputDate';
			$valueArr[] = 'now()';
			# ModifiedDate
			$fieldArr[] = 'ModifiedDate';
			$valueArr[] = 'now()';
			# Set RecordStatus = active
			$fieldArr[] = 'RecordStatus';
			$valueArr[] = 1;
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success['Insert'] = $this->db_db_query($sql);
			
			if ($success['Insert'] == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
		}
		
		if (in_array(false, $success))
			return false;
		else
			return true;
	}
	
	function Is_Record_Existed($SubjecCode, $SubjectComponentCode)
	{
		$sql = 'SELECT 
						'.$this->ID_FieldName.'
				FROM 
						'.$this->TableName.'
				WHERE 
						CODEID = \''.$this->Get_Safe_Sql_Query($SubjecCode).'\'
					AND
						CMP_CODEID = \''.$this->Get_Safe_Sql_Query($SubjectComponentCode).'\'
				';
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return false;
		else
			return $resultSet[0];
	}
	
	function Update_Record($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' ModifiedDate = now(), ';
		$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
		$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName.' ';
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->RecordID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
}



class subject_class_mapping extends libdb {
	function subject_class_mapping() {
		parent::libdb();
	}
	
	function Get_Subject_List($Order='') {
			
		$sql = 'select
						  s.DisplayOrder as Sequence,
						  s.RecordID as SubjectID,
						  s.CodeID as WEBSAMSCode,
						  s.EN_DES as SubjectDescEN,
						  s.CH_DES as SubjectDescB5,
						  s.EN_SNAME as SubjectShortNameEN,
						  s.CH_SNAME as SubjectShortNameB5
						from
						  ASSESSMENT_SUBJECT as s 
						  left outer join
						  LEARNING_CATEGORY as lc On (s.LearningCategoryID = lc.LearningCategoryID)
						where 
							s.RecordStatus = \'1\' 
							and 
							(s.CMP_CODEID is null or s.CMP_CODEID = \'\') 
						order by 
							lc.DisplayOrder '.$Order.', s.DisplayOrder '.$Order.'
						';
		//echo $sql; die;
		return $this->returnArray($sql);
	}
	
	function Get_Subject_List_With_Component($Order='') {
			
		$sql = 'select
						  s.DisplayOrder as Sequence,
						  s.RecordID as SubjectID,
						  s.CODEID as WEBSAMSCode,
						  s.CMP_CODEID as Cmp_WEBSAMSCode,
						  s.EN_DES as SubjectDescEN,
						  s.CH_DES as SubjectDescB5,
						  s.EN_SNAME as SubjectShortNameEN,
						  s.CH_SNAME as SubjectShortNameB5,
						  If (s.CMP_CODEID is null or s.CMP_CODEID = \'\', 0, 1) as IsComponent,
						  If (s.CMP_CODEID is null or s.CMP_CODEID = \'\', 1, 2) as ParentComponentDisplayOrder,
						  parent.CODEID as ParentWEBSAMSCode,
						  parent.EN_SNAME as ParentSubjectShortNameEN,
						  parent.CH_SNAME as ParentSubjectShortNameB5,
						  parent.RecordID as ParentSubjectID,
						  parent.EN_DES as ParentSubjectDescEN,
						  parent.CH_DES as ParentSubjectDescB5
						from
						  ASSESSMENT_SUBJECT as s 
						  left outer join
						  LEARNING_CATEGORY as lc On (s.LearningCategoryID = lc.LearningCategoryID)
						  inner join
						  ASSESSMENT_SUBJECT as parent On (s.CODEID = parent.CODEID)
						  left outer join
						  LEARNING_CATEGORY as parent_lc On (parent.LearningCategoryID = parent_lc.LearningCategoryID)
						where 
							s.RecordStatus = \'1\' 
							and 
							parent.RecordStatus = \'1\' 
							and 
							(parent.CMP_CODEID is null or parent.CMP_CODEID = \'\') 
						order by 
							parent_lc.DisplayOrder '.$Order.', parent.DisplayOrder '.$Order.', ParentComponentDisplayOrder Asc, s.DisplayOrder '.$Order.'
						';
		//echo $sql; die;
		return $this->returnArray($sql);
	}
	
	function Get_Subject_Code_ID_Mapping_Arr($MapByShortName=0, $isCaseSensitive=1, $reverseResult=0, $onlyReturnCmp=0)
	{
		$SubjectInfoArr = $this->Get_Subject_List_With_Component();
		$numOfSubject = count($SubjectInfoArr);
		
		$SubjectAssoArr = array();
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $SubjectInfoArr[$i]['SubjectID'];
			$thisWEBSAMSCode = trim($SubjectInfoArr[$i]['WEBSAMSCode']);
			$thisCmpWEBSAMSCode = trim($SubjectInfoArr[$i]['Cmp_WEBSAMSCode']);
			$thisSubjectShortNameEN = trim($SubjectInfoArr[$i]['SubjectShortNameEN']);
			$thisParentWEBSAMSCode = trim($SubjectInfoArr[$i]['ParentWEBSAMSCode']);
			$thisParentSubjectShortNameEN = trim($SubjectInfoArr[$i]['ParentSubjectShortNameEN']);
			
			if ($thisCmpWEBSAMSCode == '') {
				# Main Subject
				$thisKey = ($MapByShortName)? $thisSubjectShortNameEN : $thisWEBSAMSCode;
				if($onlyReturnCmp){
					continue;
				}
			}
			else {
				# Component Subject
				$thisKey = ($MapByShortName)? $thisParentSubjectShortNameEN.'###'.$thisSubjectShortNameEN : $thisParentWEBSAMSCode.'###'.$thisCmpWEBSAMSCode;
				if($onlyReturnCmp){
					$thisKey = ($MapByShortName)? $thisSubjectShortNameEN : $thisCmpWEBSAMSCode;
				}
			}
			
			if(!$isCaseSensitive){
				$thisKey = strtolower($thisKey);
			}
			
			if($reverseResult){
				$SubjectAssoArr[$thisSubjectID] = $thisKey;
			}
			else{
				$SubjectAssoArr[$thisKey] = $thisSubjectID;
			}
		}
		
		return $SubjectAssoArr;
	}
	
	function Get_Subject_Group_List($YearTermID='', $SubjectID='', $returnAssociate=1, $TeachingOnly=0, $OrderBy='') {
		$NameField = getNameFieldByLang('u.');
		$cond_YearTermID = '';
		if ($YearTermID != '')
		{
			if (is_array($YearTermID))
				$cond_YearTermID = " and st.YearTermID In (".implode(',', $YearTermID).") ";
			else
				$cond_YearTermID = ' and st.YearTermID = \''.$YearTermID.'\' ';
		}
		
		$cond_subject = '';
		if ($SubjectID != '')
		{
			if (!is_array($SubjectID))
				$SubjectID = array($SubjectID);
			$cond_subject = " and st.SubjectID in (".implode(',', $SubjectID).") "; 
		}
		
		$cond_TeachingOnly = '';
		if ($TeachingOnly == 1)
			$cond_TeachingOnly = " And stct.UserID = '".$_SESSION['UserID']."' ";
		if ($OrderBy!=''){
		}else{
			$OrderBy = ' lc.DisplayOrder, s.DisplayOrder, stc.ClassTitleEN, ayt.TermStart';
		}
		
		$sql = 'select
						  stc.SubjectGroupID,
						  stc.ClassTitleEN,
						  stc.ClassTitleB5,
						  stc.ClassCode,
						  '.$NameField.' as StaffName,
						  st.YearTermID,
						  ayt.YearTermNameEN,
						  ayt.YearTermNameB5,
						  st.SubjectID
						From
						  LEARNING_CATEGORY as lc
						  right outer join
						  ASSESSMENT_SUBJECT as s
						  on (lc.LearningCategoryID = s.LearningCategoryID)
						  inner join
						  SUBJECT_TERM as st
						  on (s.RecordID = st.SubjectID)
						  inner join
						  ACADEMIC_YEAR_TERM as ayt
						  on (st.YearTermID = ayt.YearTermID)
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on (st.SubjectGroupID = stc.SubjectGroupID '.$cond_YearTermID.' '.$cond_subject.')
						  left join
						  SUBJECT_TERM_CLASS_TEACHER as stct
						  on stc.SubjectGroupID = stct.SubjectGroupID
						  left join 
						  INTRANET_USER as u 
						  on stct.UserID = u.UserID
						Where
						  1
						  '.$cond_TeachingOnly.'
						Order by 
						  '.$OrderBy;

		//echo $sql; die;
		$SubjectGroupList = $this->returnArray($sql);
		/*echo '<pre>';
		var_dump($FormSubjectGroupList);
		echo '</pre>';
		die;*/
		if ($returnAssociate)
		{
			$SubjectGroupID = array();
			$SubjectGroupTeacherName = array('');
			$StaffCount = 0;
			for ($i=0; $i< sizeof($SubjectGroupList); $i++) {
				if (in_array($SubjectGroupList[$i]['SubjectGroupID'],$SubjectGroupID)) {
					$Temp[$SubjectGroupList[$i]['SubjectGroupID']]['StaffName'] .= '<br>'.$SubjectGroupList[$i]['StaffName'];
				}
				else {
					$Temp[$SubjectGroupList[$i]['SubjectGroupID']] = $SubjectGroupList[$i];
				}
				
				if (!in_array(trim($SubjectGroupList[$i]['StaffName']),$SubjectGroupTeacherName)) 
					$StaffCount++;
				
				$SubjectGroupID[] = $SubjectGroupList[$i]['SubjectGroupID'];
				$SubjectGroupTeacherName[] = trim($SubjectGroupList[$i]['StaffName']);
			}
			
			$ReturnArray['SubjectGroupList'] = $Temp;
			$ReturnArray['StaffCount'] = $StaffCount;
		}
		else
		{
			$ReturnArray = $SubjectGroupList;
		}
		return $ReturnArray;
	}
	
	
	// function disabled on 20090703, by kenneth chung as replaced by Get_Subject_Group_Stat
	/*function Get_Subject_Group_Stat1($YearTermID,$SubjectGroupID) {
		$sql = 'select
						  yc.YearID,
						  count(yc.YearID) as Total
						From
						  SUBJECT_TERM_CLASS_USER as stcu
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on stcu.SubjectGroupID = stc.SubjectGroupID and stcu.SubjectGroupID = \''.$SubjectGroupID.'\'
						  inner join
						  SUBJECT_TERM as st
						  on st.SubjectGroupID = stc.SubjectGroupID
						  inner join
						  ACADEMIC_YEAR_TERM as ayt
						  on ayt.YearTermID = st.YearTermID and ayt.YearTermID = \''.$YearTermID.'\'
						  inner join
						  YEAR_CLASS as yc
						  on yc.AcademicYearID = ayt.AcademicYearID
						  inner join
						  YEAR_CLASS_USER as ycu
						  on ycu.YearClassID = yc.YearClassID and stcu.UserID = ycu.UserID
						group by
						  yc.YearID';
		//echo $sql; die;
		$Result = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$ReturnArray[$Result[$i]['YearID']] = $Result[$i]['Total'];
		}
		
		return $ReturnArray;
	}*/
	function Get_Subject_Group_Index_By_SubjectID($YearTermID){
		$sql = "select 
					s.RecordID,
					stc.SubjectGroupID,
					stc.ClassTitleEN as SubjectGroupTitleEN,
					stc.ClassTitleB5 as SubjectGroupTitleB5
					From
						  ASSESSMENT_SUBJECT as s
						  Inner Join
						  SUBJECT_TERM as st
						  on s.RecordID = st.SubjectID
						  Inner Join
						  SUBJECT_TERM_CLASS as stc
							on st.SubjectGroupID = stc.SubjectGroupID
						  Inner Join
						  ACADEMIC_YEAR_TERM as ayt
						  on ayt.YearTermID = st.YearTermID
					where
						  st.YearTermID = '$YearTermID'
						  and
						  ayt.AcademicYearID is NOT NULL
					order by s.RecordID, stc.ClassTitleEN
				";
		$Result = $this->returnArray($sql); 
		$subjectGroup = Array();
		if (sizeof($Result) > 0) {
			foreach($Result as $r){
				$subjectGroup[$r['RecordID']][$r['SubjectGroupID']]=$r;
			}
		}
		return $subjectGroup;
		
	}
	
	
	function Get_Subject_Group_Stat($YearTermID,$FormID,$subjectID="") {
		$conditions = "";
		if ($subjectID != ""){
			$conditions = " and s.RecordID = $subjectID ";
		}
		$sql = 'select 
							s.RecordID,
						  s.EN_DES,
						  s.CH_DES,
						  stc.SubjectGroupID,
						  stc.ClassTitleEN as SubjectGroupTitleEN,
						  stc.ClassTitleB5 as SubjectGroupTitleB5,
						  y.YearID,
						  y.YearName,
						  yc.YearClassID,
						  yc.ClassTitleEN,
						  yc.ClassTitleB5,
						  count(ycu.YearClassID) as ClassTotal 
						From
						  ASSESSMENT_SUBJECT as s
						  inner join
						  SUBJECT_TERM as st
						  on s.RecordID = st.SubjectID
						  '.$conditions.'
						  left join
						  SUBJECT_TERM_CLASS as stc
							on st.SubjectGroupID = stc.SubjectGroupID
						  left join
						  SUBJECT_TERM_CLASS_USER as stcu
						  on stc.SubjectGroupID = stcu.SubjectGroupID
						  left join
						  ACADEMIC_YEAR_TERM as ayt
						  on ayt.YearTermID = st.YearTermID
						  left join
						  ACADEMIC_YEAR as ay
						  on ayt.AcademicYearID = ay.AcademicYearID
						  left join
						  YEAR_CLASS as yc
						  on ay.AcademicYearID = yc.AcademicYearID
						  left join
						  YEAR as y
						  on yc.YearID = y.YearID
						  left join
						  YEAR_CLASS_USER as ycu
						  on yc.YearClassID = ycu.YearClassID and ycu.UserID = stcu.UserID
						where
						  st.YearTermID = \''.$YearTermID.'\'
						  and
						  ayt.AcademicYearID is NOT NULL
						  '.$conditions.'
						group by
						  s.RecordID, st.SubjectGroupID, st.YearTermID, yc.YearClassID
						order by
						  s.DisplayOrder,stc.ClassTitleEN, y.Sequence,yc.Sequence';		  
		//echo '<pre>'.$sql.'</pre>'; die;
		$Result = $this->returnArray($sql);
		
		if (sizeof($Result) > 0) {
			if (sizeof($FormID) > 0 && $FormID!='') {
				$SubjectGroupStat = array();
				$SubjectGroupToShow = array();
				$ReturnArray = array();
				$ReturnFormStat = array();
				$ReturnClassStat = array();
				
				for ($i=0; $i < sizeof($Result); $i++) {
					if ($Result[$i]['ClassTotal'] > 0) 
						$SubjectGroupStat[$Result[$i]['SubjectGroupID']][] = $Result[$i]['YearID'];
				} 
				
				foreach ($SubjectGroupStat as $SubjectGroupID => $YearIDs) {
					$TempArray = array_intersect($YearIDs,$FormID);
					if (sizeof($TempArray) > 0) 
						$SubjectGroupToShow[] = $SubjectGroupID;
				}
				
				for ($i=0; $i < sizeof($Result); $i++) {
					if (in_array($Result[$i]['SubjectGroupID'],$SubjectGroupToShow)) {
							$ReturnArray[$Result[$i]['RecordID']][$Result[$i]['SubjectGroupID']][$Result[$i]['YearID']][$Result[$i]['YearClassID']] = $Result[$i];
						if ($Result[$i]['ClassTotal'] > 0) {
							$ReturnFormStat[$Result[$i]['YearID']] = $Result[$i]['YearID'];
							$ReturnClassStat[$Result[$i]['YearClassID']] = $Result[$i]['YearClassID'];
						}
					}
				}
			}
			else {
				for ($i=0; $i < sizeof($Result); $i++) {
						$ReturnArray[$Result[$i]['RecordID']][$Result[$i]['SubjectGroupID']][$Result[$i]['YearID']][$Result[$i]['YearClassID']] = $Result[$i];
					if ($Result[$i]['ClassTotal'] > 0) {
						$ReturnFormStat[$Result[$i]['YearID']] = $Result[$i]['YearID'];
						$ReturnClassStat[$Result[$i]['YearClassID']] = $Result[$i]['YearClassID'];
					}
				}
			}
		}
		else {
			$ReturnArray = array();
			$ReturnFormStat = array();
			$ReturnClassStat = array();
		}
		
		/*echo '<pre>';
		var_dump($ReturnArray);
		echo '</pre>';
		die;*/
		return array($ReturnArray,$ReturnFormStat,$ReturnClassStat);
	}
	
	function Get_Avaliable_Year_Class_User($YearClassID,$YearTermID,$SubjectID,$StudentSelected=array(),$SubjectGroupID='',$WithFormID=0) 
	{
		global $sys_custom;
		
		if (sizeof($StudentSelected) > 0) {
			$SelectedID = implode(',',$StudentSelected);
			$SelectedCondition = ' AND ycu.UserID not in ('.$SelectedID.') ';
		}
		
		if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']==true)
		{
			// Student can join more than one Subject Group in the same Subject 
			$joinTable_JoinedOtherSGOfTheSameSubject = '';
			$cond_JoinedOtherSGOfTheSameSubject = '';
		}
		else
		{
			// Normal flow: Student CANNOT join more than one Subject Group in the same Subject 
			$joinTable_JoinedOtherSGOfTheSameSubject = 'left join
														  (select
														    UserID
														  from
														    SUBJECT_TERM as st
														    inner join
														    SUBJECT_TERM_CLASS as stc
														    on
														    	st.SubjectGroupID = stc.SubjectGroupID 
														    	and st.YearTermID = \''.$YearTermID.'\' 
														    	and st.SubjectID = \''.$SubjectID.'\' 
														    	and stc.SubjectGroupID != \''.$SubjectGroupID.'\'
														    inner join
														    SUBJECT_TERM_CLASS_USER as stcu
														    on
														    stc.SubjectGroupID = stcu.SubjectGroupID) as stcu1
														  on ycu.UserID = stcu1.UserID';
			$cond_JoinedOtherSGOfTheSameSubject = ' And stcu1.UserID IS NULL ';
		} 
		
		$conds_YearID = '';
		$conds_YearClassID = '';
		$conds_AcademicYearID = '';
		if ($WithFormID==1)
		{
			# According to getSelectClassWithWholeForm() in libclass, YearID is numeric and YearClassID has prefix "::" before the ID
			$YearClassIDPrefix = substr($YearClassID, 0, 2);
			if ($YearClassIDPrefix == '::')
			{
				$YearClassID = substr($YearClassID, 2, strlen($YearClassID)-2);
				$conds_YearClassID = " And yc.YearClassID = '".$YearClassID."' ";
			}
			else
			{
				$YearID = $YearClassID;
				$conds_YearID = " And yc.YearID = '".$YearID."' ";
				
				global $PATH_WRT_ROOT;
				include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
				$YearTermObj = new academic_year_term($YearTermID, $GetAcademicYearInfo=false);
				$AcademicYearID = $YearTermObj->AcademicYearID;
				$conds_AcademicYearID = " And yc.AcademicYearID = '".$AcademicYearID."' ";
			}
		}
		else
		{
			$conds_YearClassID = " And yc.YearClassID = '".$YearClassID."' ";
		}
		
		$sql = 'select
						  ycu.UserID,
						  yc.ClassTitleEN,
						  yc.ClassTitleB5,
						  ycu.ClassNumber 
						From
						  YEAR_CLASS_USER as ycu 
						  inner join 
						  INTRANET_USER as u 
						  on ycu.UserID = u.UserID
						  inner join 
						  YEAR_CLASS as yc 
						  on ycu.YearClassID = yc.YearClassID '.$conds_YearClassID.' '.$conds_YearID.' '.$conds_AcademicYearID.'
						  '.$joinTable_JoinedOtherSGOfTheSameSubject.'
						where
						  1
						  '.$cond_JoinedOtherSGOfTheSameSubject.'
						  '.$SelectedCondition.' 
						order by 
							yc.Sequence, ycu.ClassNumber';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Batch_Create_Subject_Group($AcademicYearID,$YearTermID,$SubjectID,$FormList,$SelectAll,$SelectClass,$isCreateEclass) {
		$fcm = new form_class_manage();
		$YearTerm = new academic_year_term($YearTermID);
		
		$classStudentArr = array();
		$Result = Array();
		for ($i=0; $i< sizeof($SubjectID); $i++) {
			$Subject = new subject($SubjectID[$i],true);
			
			for ($j=0; $j< sizeof($FormList); $j++) {
				// skip form if form is not in form-subject relation setting
				if (in_array($FormList[$j]['YearID'],$Subject->YearRelationID)) {
				/*
//				if (in_array($FormList[$j]['YearID'],$SelectAll)) {
//					$YearClassList = $fcm->Get_Class_List($AcademicYearID,$FormList[$j]['YearID']);
//					
//					for ($k=0; $k< sizeof($YearClassList); $k++) {
//						$YearClass = new year_class($YearClassList[$k]['YearClassID'],false,false,false);
//						$AvaliableStudent = $this->Get_Avaliable_Year_Class_User($YearClassList[$k]['YearClassID'],$YearTermID,$Subject->RecordID);
//						$ClassStudent = $YearClass->Get_Active_User_List();
//						if (sizeof($AvaliableStudent) == sizeof($ClassStudent)) {
//							unset($Student);
//							for ($l=0; $l< sizeof($AvaliableStudent); $l++) {
//								$Student[] = $AvaliableStudent[$l]['UserID'];
//							}
//							$SubjectTermClassCode = $AcademicYearID.'-'.$YearTermID.'-'.$Subject->RecordID.'-'.$FormList[$j]['YearID'].'-'.$YearClassList[$k]['YearClassID'];
//							//$SubjectTermClassTitleB5 = $YearTerm->YearNameB5.' '.$YearTerm->YearTermNameB5.' '.$Subject->CH_DES.' for '.$YearClassList[$k]['ClassTitleB5'];
//							//$SubjectTermClassTitleEN = $YearTerm->YearNameEN.' '.$YearTerm->YearTermNameEN.' '.$Subject->EN_DES.' for '.$YearClassList[$k]['ClassTitleEN'];
//							$SubjectTermClassTitleB5 = $YearClassList[$k]['ClassTitleB5'].' '.$Subject->CH_DES;
//							$SubjectTermClassTitleEN = $YearClassList[$k]['ClassTitleEN'].' '.$Subject->EN_DES;
//							
//							if ($this->Check_Internal_Subject_Term_Class_Code($SubjectTermClassCode))
//								$Result['CreateSubjectGroup'.$AcademicYearID.'-'.$YearTermID.'-'.$Subject->RecordID.'-'.$FormList[$j]['YearID'].'-'.$YearClassList[$k]['YearClassID']] = $this->Create_Subject_Group(array($FormList[$j]['YearID']),$YearTermID,$Subject->RecordID,$SubjectTermClassTitleEN,$SubjectTermClassTitleB5,$SubjectTermClassCode,$Student,array(),$SubjectTermClassCode,$isCreateEclass,'',1,'',1, array(), '', $YearClassList[$k]['YearClassID']);
//																																																		
//						}
//					}
//				}
//				else {
				*/
					for ($k=0; $k< sizeof($SelectClass[$FormList[$j]['YearID']]); $k++) {
						$YearClassID = $SelectClass[$FormList[$j]['YearID']][$k];
						$YearClass = new year_class($YearClassID,false,false,false);
						$AvaliableStudent = $this->Get_Avaliable_Year_Class_User($YearClass->YearClassID,$YearTermID,$Subject->RecordID);
						$ClassStudent = $YearClass->Get_Active_User_List();
						if (sizeof($AvaliableStudent) == sizeof($ClassStudent)) {
							unset($Student);
							for ($l=0; $l< sizeof($AvaliableStudent); $l++) {
								$Student[] = $AvaliableStudent[$l]['UserID'];
							}
							$SubjectTermClassCode = $AcademicYearID.'-'.$YearTermID.'-'.$Subject->RecordID.'-'.$FormList[$j]['YearID'].'-'.$YearClass->YearClassID;
							//$SubjectTermClassTitleB5 = $YearTerm->YearNameB5.' '.$YearTerm->YearTermNameB5.' '.$Subject->CH_DES.' for '.$YearClass->ClassTitleB5;
							//$SubjectTermClassTitleEN = $YearTerm->YearNameEN.' '.$YearTerm->YearTermNameEN.' '.$Subject->EN_DES.' for '.$YearClass->ClassTitleEN;
							$SubjectTermClassTitleB5 = $YearClass->ClassTitleB5.' '.$Subject->CH_DES;
							$SubjectTermClassTitleEN = $YearClass->ClassTitleEN.' '.$Subject->EN_DES;
							
							if ($this->Check_Internal_Subject_Term_Class_Code($SubjectTermClassCode))
								$Result['CreateSubjectGroup'.$AcademicYearID.'-'.$YearTermID.'-'.$Subject->RecordID.'-'.$FormList[$j]['YearID'].'-'.$YearClass->YearClassID] = $this->Create_Subject_Group(array($FormList[$j]['YearID']),$YearTermID,$Subject->RecordID,$SubjectTermClassTitleEN,$SubjectTermClassTitleB5,$SubjectTermClassCode,$Student,array(),$SubjectTermClassCode,$isCreateEclass,'',1,'',1, array(), '', $YearClass->YearClassID);
						}
						
						
					}
				//}
				}
			}
		}
		

		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>';*/
		return !in_array(false,$Result);
	}
	
	function Batch_Check_Create_Subject_Group($AcademicYearID,$YearTermID,$SubjectID,$FormList,$SelectAll,$SelectClass) {
		global $Lang;
		
		$fcm = new form_class_manage();
		$YearTerm = new academic_year_term($YearTermID);
		
		for ($i=0; $i< sizeof($SubjectID); $i++) {
			$Subject = new subject($SubjectID[$i]);
			
			$Problem = false;
			$ProblemClass = array();
			$OKClass = array();
			// Added: 2015-01-15
			$noSubjectClass = array();
			
			for ($j=0; $j< sizeof($FormList); $j++) {
				
				// Added: check subject in selected form
				if($FormList[$j]['YearID']!="") {
					$sql = "SELECT RecordID FROM ASSESSMENT_SUBJECT SUB INNER JOIN SUBJECT_YEAR_RELATION syr 
								ON (SUB.RecordID=syr.SubjectID) WHERE syr.YearID='".$FormList[$j]['YearID']."' AND SUB.RecordStatus = 1 AND (CMP_CODEID is NULL or CMP_CODEID = '')";
					$formSubjectIDList = Get_Array_By_Key($this->returnArray($sql), 'RecordID');
					$subjectInFrom = in_array($Subject->RecordID, $formSubjectIDList);
				}
				
				// Form - Select All Classes
				if (in_array($FormList[$j]['YearID'],$SelectAll)) {
					$YearClassList = $fcm->Get_Class_List($AcademicYearID,$FormList[$j]['YearID']);
					
					for ($k=0; $k< sizeof($YearClassList); $k++) {
						$YearClass = new year_class($YearClassList[$k]['YearClassID'],false,false,false);
						$ClassStudent = $YearClass->Get_Active_User_List();
						$AvaliableStudent = $this->Get_Avaliable_Year_Class_User($YearClassList[$k]['YearClassID'],$YearTermID,$Subject->RecordID);
						$SubjectTermClassCode = $AcademicYearID.'-'.$YearTermID.'-'.$Subject->RecordID.'-'.$FormList[$j]['YearID'].'-'.$YearClassList[$k]['YearClassID'];
						
						if (sizeof($AvaliableStudent) < sizeof($ClassStudent) || !$this->Check_Internal_Subject_Term_Class_Code($SubjectTermClassCode)) {
							$ProblemClass[] = $FormList[$j]['YearName']."-".$YearClass->Get_Class_Name();
							$Problem = true;
						}
						// Added: subject not in select form - add to $noSubjectClass[]
						else if (!$subjectInFrom){
							$noSubjectClass[] = $FormList[$j]['YearName']."-".$YearClass->Get_Class_Name();
							$Problem = true;
						}
						else {
							$OKClass[] = $FormList[$j]['YearName']."-".$YearClass->Get_Class_Name();
						}
					}
				}
				// Form - Select some Classes 
				else {
					for ($k=0; $k< sizeof($SelectClass[$FormList[$j]['YearID']]); $k++) {
						$YearClassID = $SelectClass[$FormList[$j]['YearID']][$k];
						$YearClass = new year_class($YearClassID,false,false,false);
						$ClassStudent = $YearClass->Get_Active_User_List();
						$AvaliableStudent = $this->Get_Avaliable_Year_Class_User($YearClass->YearClassID,$YearTermID,$Subject->RecordID);
						$SubjectTermClassCode = $AcademicYearID.'-'.$YearTermID.'-'.$Subject->RecordID.'-'.$FormList[$j]['YearID'].'-'.$YearClass->YearClassID;
						
						if (sizeof($AvaliableStudent) < sizeof($ClassStudent) || !$this->Check_Internal_Subject_Term_Class_Code($SubjectTermClassCode)) {
							$ProblemClass[] = $FormList[$j]['YearName']."-".$YearClass->Get_Class_Name();
							$Problem = true;
						}
						// Added: subject not in select form - add to $noSubjectClass[]
						else if (!$subjectInFrom){
							$noSubjectClass[] = $FormList[$j]['YearName']."-".$YearClass->Get_Class_Name();
							$Problem = true;
						}
						else {
							$OKClass[] = $FormList[$j]['YearName']."-".$YearClass->Get_Class_Name();
						}
					}
				}
			}
			
			if ($Problem) {
				// display when problem class exist
//				if(count($ProblemClass) > 0){
				$ProblemList .= $Subject->Get_Subject_Desc().": ".implode(", ",$ProblemClass).chr(10);
//				}
				// display when out of form-subject relation
//				if(count($noSubjectClass) > 0){
				$noSubjectList .= $Subject->Get_Subject_Desc().": ".implode(", ",$noSubjectClass).chr(10);
//				}
				$OKList .= $Subject->Get_Subject_Desc().": ".implode(", ",$OKClass).chr(10);
			}
			else {
				$OKList .= $Subject->Get_Subject_Desc().": ".implode(", ",$OKClass).chr(10);
			}
		}
		
		if ($ProblemList != "") {
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning1'].chr(10).chr(10);
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning2'].chr(10);	
			$ReturnMessage .= $ProblemList.chr(10);
			
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning3'].chr(10);
			$ReturnMessage .= $OKList.chr(10);
			
//			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning4'].chr(10);
		}
		// Added: display alert message when subject not in select form
		if ($noSubjectList != "") {
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning_without_Subject'].chr(10).chr(10);
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning2'].chr(10);	
			$ReturnMessage .= $noSubjectList.chr(10);
			
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning3'].chr(10);
			$ReturnMessage .= $OKList.chr(10);
			
//			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning4'].chr(10);
		}
		
		if ($ProblemList != "" || $noSubjectList != "") {
			$ReturnMessage .= $Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning4'].chr(10);
		}
		
		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>';*/
		return $ReturnMessage;
	}
	
	function Check_Internal_Subject_Term_Class_Code($InternalClassCode) {
		$sql = 'select 
							count(1)
						from 
							SUBJECT_TERM_CLASS 
						where 
							InternalClassCode = \''.$InternalClassCode.'\'';
		$Result = $this->returnVector($sql);
		
		if ($Result[0] > 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	function Get_Unique_Subject_Term_Class_Code($ClassCode,$Subfix="", $YearTermID="") {
		
//		$sql = 'select 
//							count(1)
//						from 
//							SUBJECT_TERM_CLASS 
//						where 
//							ClassCode = \''.$this->Get_Safe_Sql_Query($ClassCode.$Subfix).'\'';
//		$ClassCodeExist = $this->returnVector($sql);
		$ClassCodeAvailable = $this->Check_Class_Code('' ,$ClassCode.$Subfix, $YearTermID);
		
		if (!$ClassCodeAvailable) {
			if ($Subfix == "") $Subfix = 1;
			else $Subfix++;
			
			return $this->Get_Unique_Subject_Term_Class_Code($ClassCode,$Subfix, $YearTermID);
		}
		else {
			return $ClassCode.$Subfix;
		}
	}
	
	function getTotalCourseUser($course_id){
		global $eclass_db;
		$sql = "select count(*) from {$eclass_db}.user_course where course_id = {$course_id} and status is null";
		$no = $this->returnVector($sql);
		return $no[0];
	}
	
	function Create_Subject_Group($YearID, $YearTermID, $SubjectID, $ClassTitleEN, $ClassTitleB5, $ClassCode,
									$StudentSelected, $SelectedClassTeacher=array(), $InternalClassCode='', $isCreateclass=false, $CopyFromSubjectGroupID='',
									$createClassNew=0,$eclass='',$addStd=0, $LeaderArr=array(), $SubjectComponentID='', $LockedYearClassID='') 
	{
		global $eclass_db, $intranet_session_language, $sys_custom, $intranet_db;
		//global $PATH_WRT_ROOT;
		
		// get Next SubjectGroupID 
		$sql = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$intranet_db."' And table_name='SUBJECT_TERM_CLASS'";
		$TmpArr = $this->returnArray($sql);
		$NextSubjectGroupID = $TmpArr[0]['AUTO_INCREMENT'];
		if ($NextSubjectGroupID == '' || $NextSubjectGroupID == 0)
		{
			// in case schema not updated to auto increment => get the max of SUBJECT_TERM_CLASS and SUBJECT_TERM
			$sql = 'select MAX(SubjectGroupID) From SUBJECT_TERM';
			$TempFromSubjectTerm = $this->returnVector($sql);
			$TempFromSubjectTerm = $TempFromSubjectTerm[0];
			$sql = 'select MAX(SubjectGroupID) From SUBJECT_TERM_CLASS';
			$TempFromSubjectTermClass = $this->returnVector($sql);
			$TempFromSubjectTermClass = $TempFromSubjectTermClass[0];
			$Temp = max($TempFromSubjectTerm, $TempFromSubjectTermClass);
			$NextSubjectGroupID = $Temp + 1;
		}
		
		
		//debug_r($SelectedClassTeacher);
		//debug_r($StudentSelected);
		// create YEAR_TERM Record 
		$sql = 'insert into SUBJECT_TERM (
							SubjectID, 
							SubjectComponentID,
							YearTermID, 
							SubjectGroupID, 
							DateInput, 
							InputBy
							)
						value (
							\''.$SubjectID.'\', 
							\''.$SubjectComponentID.'\',
							\''.$YearTermID.'\', 
							\''.$NextSubjectGroupID.'\', 
							Now(), 
							\''.$_SESSION['UserID'].'\'
							)
					';
		$Result['CreateYearTerm'] = $this->db_db_query($sql);
		
		# insert Subject Leader
		if (!empty($LeaderArr)){
			$LeaderArr = array_unique($LeaderArr);
			foreach ($LeaderArr as $lid){
				$sql = "Insert into INTRANET_SUBJECT_LEADER (UserID, ClassID, SubjectID, RecordStatus, DateInput) 
					values
					('$lid',$NextSubjectGroupID,$SubjectID,1,now())";
				$this->db_db_query($sql);
			}
		}
		
		$calID = 0;
		#create eclass
		if ($isCreateclass){
			$allUser = array_merge((array)$StudentSelected, (array)$SelectedClassTeacher);
			$numOfUser = count($allUser);
			
			$objOldSubjectGroup = new subject_term_class($CopyFromSubjectGroupID, true, true);
				
			### If it is from Copy Subject Group and the Old Subject Group has linked eClass,
			### Link the New Subject Group to the old eClass;
			### Unlink the Old eClass from the Old subject group;
			### Set the is_past status of all users to 'Y';
			if ($CopyFromSubjectGroupID != '' && $objOldSubjectGroup->course_id != '' && $objOldSubjectGroup->course_id != 0)
			{
				# Set the target eClass course id as the eClass of the old subject group
				$course_id = $objOldSubjectGroup->course_id;
				
				# Link the Old eClass to the New Subject Group
				$lo = new libeclass($course_id);
				$lo->removeSubjectGroup($CopyFromSubjectGroupID,$objOldSubjectGroup->course_id);
				$Result['Link_eClass_To_New_Subject_Group'] = $lo->eClassUpdate($course_id, $lo->course_code, $lo->course_name, 
																$lo->course_desc, $lo->max_user, $lo->max_storage, $NextSubjectGroupID, false);
				
				# Unlink the Old eClass from the Old subject group
				$Result['Unlink_eClass'] = $objOldSubjectGroup->UnLink_eClass();
				
				# Update the is_past status
				$OldTeacherArr = Get_Array_By_Key($objOldSubjectGroup->ClassTeacherList, 'UserID');
				$OldStudentArr = Get_Array_By_Key($objOldSubjectGroup->ClassStudentList, 'UserID');
				$allExistingUser = array_merge($OldTeacherArr, $OldStudentArr);
				$numExistingUser = count($allExistingUser);
				if ($numExistingUser > 0) {
					$Result['Update_eClass_User_IsPast_Status'] = $lo->eClassUserUpdateIsPastStatus('Y');
				}

				// 2011-08-04 Ivan - requested by CS, the classroom user will be kept now and the user has to delete the students in eClass			
//				# Set the removed student and teacher to "status = deleted" in eClass
//				$removeUser = array_diff($allExistingUser, $allUser);				
//				 
//				if (is_array($removeUser) && count($removeUser) > 0)
//				{
//					/*
//					$sql = "select user_id from {$eclass_db}.user_course as c
//							inner join INTRANET_USER as u on
//								c.user_email = u.UserEmail
//								where 
//								c.course_id = '$course_id' and 
//								u.UserID in ('".implode("','",$removeUser)."')
//								";
//					$user_id = $this->returnVector($sql); 
//					*/
//					$user_id = $this->Get_eClass_UserID($course_id, $removeUser);
//					
//					### Delete User From eClass
//					$lo->eClassUserDel($user_id);
//				}
			}
			else
			{
				if ($createClassNew){
					$libeclass = new libeclass();
					$class_name = "ClassTitle".strtoupper($intranet_session_language);
					$course_id = $libeclass->eClassAdd($this->Get_Safe_Sql_Query($ClassCode), $this->Get_Safe_Sql_Query($$class_name), '', 0, '', $_SESSION['UserID'], $NextSubjectGroupID, true);
					$lo = new libeclass($course_id);
					
					//include_once('icalendar.php');
					$iCal = new icalendar();
					$calID = $iCal->createSystemCalendar($$class_name,3,"P",'');
					$Result['create_Calendar_to_eclass'] = $calID;
					$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
					$Result['create_Calendar_connection_to_eclass'] = $lo->db_db_query($sql);					
				}
				else{
					if (!empty($eclass)){
						$sql = "select i.UserID from {$eclass_db}.user_course as u inner join INTRANET_USER as i on 
						u.user_email = i.UserEmail
						where u.course_id = '$eclass'";						
						$user_list = $this->returnVector($sql);
						
						$sql = "select SubjectGroupID from {$eclass_db}.course where course_id = '$eclass'";
						$r = $this->returnVector($sql);
						$r = empty($r)?array():$r[0];
						$subj_list = explode(';',$r);
						$subj_list[] = $NextSubjectGroupID;
						$subj_list = array_unique($subj_list);
						$sql = "update {$eclass_db}.course set SubjectGroupID = '".implode(";",$subj_list)."' 
							where course_id = '$eclass'";
						$this->db_db_query($sql);
						
						$user_list = empty($user_list)?array():$user_list;
						$allUser = array_diff($allUser,$user_list);
						$numOfUser = count($allUser);
						$course_id = $eclass;
						if ($addStd){
							$sql ="select CalID from {$eclass_db}.course where course_id = $course_id";
							$r = $this->returnVector($sql);
							$calID = $r[0];
						}
					}
					else{
						$numOfUser = 0;
					}
				}
				if ($numOfUser > 0 && $addStd)
				{
					### The Subject Group has not created yet. So, need to use the course_id.
					$Result['AddUserToeClassAndiCalendar'] = $this->Add_User_To_eClassCourse_And_iCalendar($NextSubjectGroupID, $allUser, $course_id,$calID);
				}
			}
			$total = $this->getTotalCourseUser($course_id);
			$sql ="update {$eclass_db}.course set max_user = if(max_user is null, NULL, if(max_user<{$total},{$total},max_user)) where course_id = {$course_id}";
			$Result['update_course_max_user'] =$this->db_db_query($sql);			
		}
		
		
		if ($sys_custom['SubjectGroup']['LockLogic']==false || $LockedYearClassID == '')
			$LockedYearClassID = 'null';
		
		// create Subject term class record 
		$sql = 'insert into SUBJECT_TERM_CLASS (
							SubjectGroupID, 
							ClassCode, 
							InternalClassCode,
							ClassTitleEN, 
							ClassTitleB5, 
							DateInput, 
							InputBy,
							course_id,
							CopyFromSubjectGroupID,
							LockedYearClassID
							)
						value (
							\''.$NextSubjectGroupID.'\',  
							\''.$this->Get_Safe_Sql_Query($this->Get_Unique_Subject_Term_Class_Code($ClassCode, '', $YearTermID)).'\', 
							\''.$this->Get_Safe_Sql_Query($InternalClassCode).'\', 
							\''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\', 
							\''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\', 
							NOW(), 
							\''.$_SESSION['UserID'].'\',
							\''.$course_id.'\',
							\''.$CopyFromSubjectGroupID.'\',
							'.$LockedYearClassID.'
							)'; 
							
		$Result['CreateYearTermClass'] = $this->db_db_query($sql);
				
		for ($i=0; $i< sizeof($YearID); $i++) {
			$sql = 'insert into SUBJECT_TERM_CLASS_YEAR_RELATION (
								SubjectGroupID, 
								YearID, 
								DateInput, 
								InputBy
								)
							value (
								\''.$NextSubjectGroupID.'\',  
								\''.$YearID[$i].'\', 
								NOW(), 
								\''.$_SESSION['UserID'].'\'
								)';
			$Result['CreatYearRelation'.$YearID[$i]] = $this->db_db_query($sql);
			//echo $sql; die;
		}
		
		// insert class Teacher
		for ($i=0; $i< sizeof($SelectedClassTeacher); $i++) {
			if (trim($SelectedClassTeacher[$i]) != "") {
				$sql = 'insert into SUBJECT_TERM_CLASS_TEACHER (
									SubjectGroupID, 
									UserID, 
									DateInput, 
									InputBy
									)
								value (
									\''.$NextSubjectGroupID.'\', 
									\''.$SelectedClassTeacher[$i].'\', 
									now(),
									\''.$_SESSION['UserID'].'\'
									)';
				$Result['InsertClassTeacher'.$SelectedClassTeacher[$i]] = $this->db_db_query($sql);
			}
		}
		
		// insert class student
		for ($i=0; $i< sizeof($StudentSelected); $i++) {
			$sql = 'insert into SUBJECT_TERM_CLASS_USER (
								SubjectGroupID, 
								UserID, 
								DateInput, 
								InputBy
								)
							value (
								\''.$NextSubjectGroupID.'\', 
								\''.$StudentSelected[$i].'\', 
								now(),
								\''.$_SESSION['UserID'].'\'
								)';
			$Result['InsertClassStudent'.$StudentSelected[$i]] = $this->db_db_query($sql);
		}
		

		######## Add subject group to eClass START ########
		if ($isCreateclass){
			$libeclass = new libeclass();
			$libeclass->createSubjectGroupForClassroom($course_id, $NextSubjectGroupID);
				
			if($addStd){
				$libeclass->syncUserToSubjectGroup($course_id);
			}
		}
		######## Add subject group to eClass END ########
		
		// echo '<pre>';
		// var_dump($Result);
		// echo '</pre>';
		return !in_array(false,$Result);
	}
	
	function Edit_Subject_Group($SubjectGroupID, $YearID, $YearTermID, $ClassTitleEN, $ClassTitleB5, $ClassCode,
								$StudentSelected, $SelectedClassTeacher, $is_user_import, $teacher_benefit,$isCreateClass=false,$createClassNew=0,$eclass='',$addStd=0, $SubjectComponentID='') {
		// Edit YEAR_TERM Record - to be added later
		
		// create Subject term class record 
		$sql= 'Update SUBJECT_TERM_CLASS set 
						ClassCode = \''.$this->Get_Safe_Sql_Query($ClassCode).'\', 
						ClassTitleEN = \''.$this->Get_Safe_Sql_Query($ClassTitleEN).'\', 
						ClassTitleB5 = \''.$this->Get_Safe_Sql_Query($ClassTitleB5).'\', 
						ModifyBy = \''.$_SESSION['UserID'].'\', 
						DateModified = NOW() 
					Where 
						SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['UpdateYearTermClass'] = $this->db_db_query($sql);
		
		// Update Subject Component Info
		$sql = "Update SUBJECT_TERM set SubjectComponentID = '$SubjectComponentID' Where SubjectGroupID = '$SubjectGroupID' And YearTermID = '$YearTermID'";
		$Result['UpdateSubjectComponentMapping'] = $this->db_db_query($sql);
	
		# check existing eclass
		global $eclass_db, $intranet_db, $intranet_session_language;
		$sql = "select s.course_id, c.IsFromSubjectGroup, c.CalID from SUBJECT_TERM_CLASS as s 
				inner join {$eclass_db}.course as c on
				s.course_id = c.course_id
				where s.SubjectGroupID = $SubjectGroupID and s.course_id is not null";
		$groupDetail = $this->returnArray($sql);
		$isFromSubjectGroup = ($groupDetail[0]["IsFromSubjectGroup"]==1);
		$course_id = $groupDetail[0]["course_id"];
		$calID = $groupDetail[0]["CalID"];
	/*	if ($isFromSubjectGroup){
			$lo = new libeclass($course_id);
			$iCal = new icalendar(); 
			# get removed teacher
			$removedTeacher = Array();					
			$sub_sql = implode(',',$SelectedClassTeacher);
			$sql = "select user_id from ".$lo->db_prefix."c$course_id".".usermaster where  (status IS NULL OR status='') AND
					user_email in (
						select u.UserEmail from SUBJECT_TERM_CLASS_TEACHER as t
						inner join INTRANET_USER as u on
						t.UserID = u.UserID
						Where t.SubjectGroupID = '$SubjectGroupID' 
						and t.UserID not in($sub_sql)
					)"; //echo $sql.'<br>';
			$removedTeacher = $this->returnVector($sql);
			if ($is_user_import){	
				if (count($removedTeacher)>0){
					$SelectedClassTeacher[] = $teacher_benefit;
				}
			}
			
			# update course title
			$sql = "update {$eclass_db}.course set course_name = '$ClassTitleEN' 
					where course_id = $course_id
					";
			$this->db_db_query($sql);
			
			# update calendar title
			$sql = "update CALENDAR_CALENDAR set Name = '$ClassTitleEN' 
					where CalID = $calID
					";
			$this->db_db_query($sql);
			
			# get existing user
			$sql = "select UserID from SUBJECT_TERM_CLASS_TEACHER
					Where SubjectGroupID = '$SubjectGroupID' 
					Union select UserID from SUBJECT_TERM_CLASS_USER 
					where SubjectGroupID = '$SubjectGroupID'
					"; 
			$existingUser = $this->returnVector($sql); 
			$allUser = array_merge($SelectedClassTeacher, $StudentSelected);
			
			$newUser = array_diff($allUser,$existingUser);
			$removeUser = array_diff($existingUser,$allUser);
			
			# remove calendar viewer
			if (count($removeUser) > 0){
				$sql = "select CalID from course where course_id = '$course_id'";
				$calID = $lo->returnVector($sql);
				$Result['remove_calendar_viewer'] = $iCal->removeCalendarViewer($calID[0], $removeUser);

				$user_id = $this->Get_eClass_UserID($course_id, $removeUser);
				
				### Delete User From eClass
				$lo->eClassUserDel($user_id);
			}
			
			if (count($newUser)>0){
				$Result['AddUserToeClassAndiCalendar'] = $this->Add_User_To_eClassCourse_And_iCalendar($SubjectGroupID, $newUser);
			}
			
			if (count($removedTeacher)>0){			
				if ($is_user_import){
					// import user to course
					//$li = new libdb();	
					// find the user_id in course
					$sql = "SELECT user_id FROM ".$lo->db_prefix."c$course_id".".usermaster WHERE user_email in(
						select UserEmail from {$intranet_db}.INTRANET_USER where UserID = $teacher_benefit
					)
					AND (status IS NULL OR status='') ";
					$row = $lo->returnVector($sql); //echo $sql.'<br>';
					$teacher_benefit = (int) $row[0];
				}
				//echo 'Teacher: '.$teacher_benefit.'<br>';
				# transfer removed teacher's files to another teacher 
				if ($teacher_benefit!=0){
					for ($i=0; $i<sizeof($removedTeacher); $i++){
						$lo->assignFileTo($removedTeacher[$i], $teacher_benefit, $course_id);
					}
				}
			}
		}
		else*/ if ($isCreateClass){
			$allUser= array_merge($SelectedClassTeacher, $StudentSelected);
			//debug_r($allUser);
			$numOfUser = count($allUser);
			if ($createClassNew){
				$libeclass = new libeclass();
				$libeclass->removeSubjectGroup($SubjectGroupID,$course_id);
				$class_name = "ClassTitle".strtoupper($intranet_session_language);
				$course_id = $libeclass->eClassAdd($this->Get_Safe_Sql_Query($ClassCode), $this->Get_Safe_Sql_Query($$class_name), '', 0, '', $_SESSION['UserID'], $SubjectGroupID, true);
				$lo = new libeclass($course_id);
				
				//include_once('icalendar.php');
				$iCal = new icalendar();
				$calID = $iCal->createSystemCalendar($$class_name,3,"P",'');
				$Result['create_Calendar_to_eclass'] = $calID;
				$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
				$Result['create_Calendar_connection_to_eclass'] = $lo->db_db_query($sql);					
			}
			else{
				if (!empty($eclass)){
					if ($course_id != $eclass){
						$libeclass = new libeclass();
						$libeclass->removeSubjectGroup($SubjectGroupID,$course_id);
					}
					$sql = "select i.UserID from {$eclass_db}.user_course as u inner join INTRANET_USER as i on 
					u.user_email = i.UserEmail
					where u.course_id = '$eclass'";
					$user_list = $this->returnVector($sql);
					
					$sql = "select SubjectGroupID from {$eclass_db}.course where course_id = '$eclass'";
					$r = $this->returnVector($sql);
					$r = empty($r)?array():$r[0];
					$subj_list = explode(';',$r);
					$subj_list[] = $SubjectGroupID;
					$subj_list = array_unique($subj_list);
					$sql = "update {$eclass_db}.course set SubjectGroupID = '".implode(";",$subj_list)."' 
						where course_id = '$eclass'";
					$this->db_db_query($sql);
										
					$user_list = empty($user_list)?array():$user_list;
					$allUser = array_diff($allUser,$user_list);
					$numOfUser = count($allUser);
					$course_id = $eclass;			
				}
				else{
					$numOfUser = 0;
				}
			}
			if ($numOfUser > 0 && $addStd)
			{
				### The Subject Group has not created yet. So, need to use the course_id.
				$Result['AddUserToeClassAndiCalendar'] = $this->Add_User_To_eClassCourse_And_iCalendar($SubjectGroupID, $allUser, $course_id, $calID);
			}
		/*
			$libeclass = new libeclass();
			$course_id = $libeclass->eClassAdd($this->Get_Safe_Sql_Query($ClassCode), $this->Get_Safe_Sql_Query($ClassTitleEN), '', 0, '', $_SESSION['UserID'], $SubjectGroupID, true);
			$lo = new libeclass($course_id);
			$iCal = new icalendar();
			$calID = $iCal->createSystemCalendar($ClassTitleEN,3,"P",'');
			$Result['create_Calendar_to_eclass'] = $calID;
			$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
			$Result['create_Calendar_connection_to_eclass'] = $lo->db_db_query($sql);
			
			if ($numOfUser > 0)
			{
				### The Subject Group has not created yet. So, need to use the course_id.
				$Result['AddUserToeClassAndiCalendar'] = $this->Add_User_To_eClassCourse_And_iCalendar($SubjectGroupID, $allUser, $course_id);
			}
		*/
			$sql = "Update SUBJECT_TERM_CLASS set course_id = $course_id  where SubjectGroupID = $SubjectGroupID";
			$Result['update_course_id'] = $this->db_db_query($sql);
			$total = $this->getTotalCourseUser($course_id);
			$sql ="update {$eclass_db}.course set max_user = if(max_user is null, max_user, if(max_user<{$total},{$total},max_user)) where course_id = {$course_id}";
			$Result['update_course_max_user'] =$this->db_db_query($sql);
		}
		else{
			$libeclass = new libeclass();
			$libeclass->removeSubjectGroup($SubjectGroupID,$course_id);
			$sql = "Update SUBJECT_TERM_CLASS set course_id = null  where SubjectGroupID = $SubjectGroupID";
			$Result['update_course_id'] = $this->db_db_query($sql);
		}
		
		// clear old year relation
		$sql = 'delete from SUBJECT_TERM_CLASS_YEAR_RELATION Where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['ClearYearRelation'] = $this->db_db_query($sql);
		
		// insert year relation
		for ($i=0; $i< sizeof($YearID); $i++) {
			$sql = 'insert into SUBJECT_TERM_CLASS_YEAR_RELATION (
								SubjectGroupID, 
								YearID, 
								DateInput, 
								InputBy
								)
							value (
								\''.$SubjectGroupID.'\',  
								\''.$YearID[$i].'\', 
								NOW(), 
								\''.$_SESSION['UserID'].'\'
								)';
			$Result['CreatYearRelation'.$YearID[$i]] = $this->db_db_query($sql);
		}
		
		
		// clear old class teacher
		$sql = 'delete from SUBJECT_TERM_CLASS_TEACHER Where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['ClearClassTeacher'] = $this->db_db_query($sql);
		
		// insert class Teacher
		for ($i=0; $i< sizeof($SelectedClassTeacher); $i++) {
			$sql = 'insert into SUBJECT_TERM_CLASS_TEACHER (
								SubjectGroupID, 
								UserID, 
								DateInput, 
								InputBy
								)
							value (
								\''.$SubjectGroupID.'\', 
								\''.$SelectedClassTeacher[$i].'\', 
								now(),
								\''.$_SESSION['UserID'].'\'
								)';
			$Result['InsertClassTeacher'.$SelectedClassTeacher[$i]] = $this->db_db_query($sql);
		}
		
		// retrieve original group member  
		$sql = "SELECT UserID FROM INTRANET_SUBJECT_LEADER WHERE ClassID = '".$SubjectGroupID."'";
		$originalClassUserID = $this->returnVector($sql);
		$userDiff = array_diff($originalClassUserID, $StudentSelected);
		
		// remove subject leader if group member does not in new group member list
		if(sizeof($userDiff)>0) {
			$sql = "DELETE FROM INTRANET_SUBJECT_LEADER WHERE ClassID='".$SubjectGroupID."' AND UserID IN (".implode(',',$userDiff).")";
			$this->db_db_query($sql);
		}		
		
		// clear old class student
		$sql = 'delete from SUBJECT_TERM_CLASS_USER Where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['ClearClassStudent'] = $this->db_db_query($sql);
		
		// insert class student
		for ($i=0; $i< sizeof($StudentSelected); $i++) {
			$sql = 'insert into SUBJECT_TERM_CLASS_USER (
								SubjectGroupID, 
								UserID, 
								DateInput, 
								InputBy
								)
							value (
								\''.$SubjectGroupID.'\', 
								\''.$StudentSelected[$i].'\', 
								now(),
								\''.$_SESSION['UserID'].'\'
								)';
			$Result['InsertClassStudent'.$StudentSelected[$i]] = $this->db_db_query($sql);
		}
		
		######## Sync subject group to eClass START ########
		if ($isCreateClass){
			$libeclass = new libeclass();
			$libeclass->createSubjectGroupForClassroom($course_id, $SubjectGroupID);
			if($addStd){
				$libeclass->syncUserToSubjectGroup($course_id);
			}
		}
		######## Sync subject group to eClass END ########
		
		/*echo '<pre>';
		debug_r($Result);
		echo '</pre>';*/
		return !in_array(false,$Result);
	}
	
	function Edit_SubjectGroup_Code_Name($SubjectGroupID, $DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModified = now(), ';
		$valueFieldText .= ' ModifyBy = \''.$_SESSION['UserID'].'\' ';
		
		$sql = '';
		$sql .= ' UPDATE SUBJECT_TERM_CLASS ';
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE SubjectGroupID = \''.$SubjectGroupID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Update_Subject_Group_Applicable_Form($SubjectGroupID, $YearIDArr)
	{
		// clear old year relation
		$sql = 'delete from SUBJECT_TERM_CLASS_YEAR_RELATION Where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['ClearYearRelation'] = $this->db_db_query($sql);
		
		// insert year relation
		$numOfYear = count($YearIDArr);
		if ($numOfYear > 0)
		{
			$ValueArr = array();
			for ($i=0; $i<$numOfYear; $i++)
			{
				$thisYearID = $YearIDArr[$i];
				$ValueArr[] = " ('$SubjectGroupID', '$thisYearID', now(), '".$_SESSION['UserID']."') ";
			}
			
			$ValueList = implode(',', $ValueArr);
			$sql = "insert into SUBJECT_TERM_CLASS_YEAR_RELATION 
						(SubjectGroupID, YearID, DateInput,	InputBy)
					values 
						$ValueList
					";
			$Result['UpdateYearRelation'] = $this->db_db_query($sql);
		}
		
		return !in_array(false, $Result);
	}
	
	function Delete_Subject_Term_Class($SubjectGroupID) {
		global $eclass_db;
		
		$this->Start_Trans();
		
		$SubjectGroupObj = new subject_term_class($SubjectGroupID);
		
		// delete YEAR_TERM Record 
		$sql = 'delete from SUBJECT_TERM where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteYearTerm'] = $this->db_db_query($sql);
		
		// delete Subject term class record 
		$sql= 'delete from SUBJECT_TERM_CLASS where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteYearTermClass'] = $this->db_db_query($sql);
		
		// delete class-year relation
		$sql = 'delete from SUBJECT_TERM_CLASS_YEAR_RELATION where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteYearRelation'] = $this->db_db_query($sql);
		
		// delete class Teacher
		$sql = 'delete from SUBJECT_TERM_CLASS_TEACHER where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteClassTeacher'] = $this->db_db_query($sql);
		
		// delete class student
		$sql = 'delete from SUBJECT_TERM_CLASS_USER where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteClassStudent'] = $this->db_db_query($sql);
		
		// delete timetable data
		$sql = 'delete from INTRANET_TIMETABLE_ROOM_ALLOCATION where SubjectGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteTimetableData'] = $this->db_db_query($sql);
		
		// delete subject leader
		$sql = 'Update INTRANET_SUBJECT_LEADER Set RecordStatus = 0 Where ClassID = \''.$SubjectGroupID.'\'';
		$Result['DeleteSubjectLeader'] = $this->db_db_query($sql);
		
		// delete subject homework list
		$sql = 'delete from INTRANET_HOMEWORK where ClassGroupID = \''.$SubjectGroupID.'\'';
		$Result['DeleteHomeworkList'] = $this->db_db_query($sql);
		
		// unlink eClass
		$sql = "select SubjectGroupID,course_id from {$eclass_db}.course where SubjectGroupID regexp '^$SubjectGroupID$|;$SubjectGroupID;|;$SubjectGroupID$' ";
		$allClass = $this->returnArray($sql);
		if (!empty($allClass)){
			foreach ($allClass as $class){
				$allSubj = empty($class['SubjectGroupID'])?array():explode(';',$class['SubjectGroupID']);
				$allSubj = array_filter($allSubj,create_function('$a','return $a != '.$SubjectGroupID.';'));
				$subj_sql = empty($allSubj)?'':implode(";",$allSubj);
				
				$sql = "update {$eclass_db}.course 
						set SubjectGroupID = '$subj_sql'
							where course_id = ".$class['course_id'];
				$Result['update_course_linkage_'.$class['course_id']] = $this->db_db_query($sql);
			}
		}
		
		// Log the deletion
		$LogTextArr = array();
		$LogTextArr[] = 'SubjectGroupCode:::'.$SubjectGroupObj->ClassCode;
		$LogTextArr[] = 'InternalClassCode:::'.$SubjectGroupObj->InternalClassCode;
		$LogTextArr[] = 'ClassTitleEN:::'.$SubjectGroupObj->ClassTitleEN;
		$LogTextArr[] = 'ClassTitleB5:::'.$SubjectGroupObj->ClassTitleB5;
		$LogTextArr[] = 'course_id:::'.$SubjectGroupObj->course_id;
		$LogText = implode('|||', $LogTextArr);
								
		$sql = "Insert Into MODULE_RECORD_DELETE_LOG
					(Module, Section, RecordDetail, DelTableName, DelRecordID, LogDate, LogBy)
				Values
					('Subject', 'Subject_Group', '".$this->Get_Safe_Sql_Query($LogText)."', 'SUBJECT_TERM_CLASS', '$SubjectGroupID', now(), '".$_SESSION['UserID']."')
				";
		$Result['AddLog'] = $this->db_db_query($sql);
		
		if (!in_array(false, $Result)) 
		{
			$this->Commit_Trans();
			return true;
		}
		else
		{
			$this->RollBack_Trans();
			return false;
		}
	}
	
	/*
	 * return: array of StudentID who cannot join the subject group
	 */
	function Check_Selected_Student($StudentSelected,$YearID,$AcademicYearID) {
		if (sizeof($StudentSelected) > 0) {
			$StudentCondition = ' and YearClassTable.UserID in ('.implode(',',$StudentSelected).') ';
			$IntranetUserStudentCondition = ' UserID in ('.implode(',',$StudentSelected).') ';
		}
		else 
			return array();
		
		if (is_array($YearID))
		{
			// no Form applicable for the subject group => always return null
			if (count($YearID) == 0)
				$YearID = array(0);
			$YearCondition = ' or YearClassTable.YearID not in ('.implode(',',$YearID).') ';
		}
			
		/*
		$sql = 'select
						  UserID
						From
						  INTRANET_USER
						where
						  '.$IntranetUserStudentCondition.'
						  and (ClassName = \'\' OR ClassName IS NULL)
						union
						select
						  ycu.UserID
						From
						  YEAR_CLASS as yc
						  inner join
						  YEAR_CLASS_USER as ycu
						  on yc.YearClassID = ycu.YearClassID 
						  and yc.AcademicYearID = \''.$AcademicYearID.'\' 
						  '.$YearCondition.' 
						  '.$StudentCondition.'
					 ';
		*/
		$sql = 'select
						  iu.UserID
						From
						  INTRANET_USER as iu
						  Left Outer Join
							(
								Select
										ycu.UserID, yc.YearClassID, ycu.ClassNumber, yc.YearID
								From	
										YEAR_CLASS_USER as ycu
										Inner Join
										YEAR_CLASS as yc
										On (ycu.YearClassID = yc.YearClassID And yc.AcademicYearID = \''.$AcademicYearID.'\')
							) as YearClassTable
							On (iu.UserID = YearClassTable.UserID)
						Where
						  ((YearClassTable.ClassNumber Is Null And YearClassTable.YearClassID Is Null) '.$YearCondition.')
						  '.$StudentCondition.'
					 ';
		//echo $sql; die;
		
		return $this->returnVector($sql);
	}
	
	function Search_Student($SearchValue,$StudentSelected,$YearTermID,$SubjectID,$SubjectGroupID,$YearID) {
		global $sys_custom;
		
		$NameField = getNameFieldByLang('u.');
		$SearchValue = $this->Get_Safe_Sql_Query($SearchValue);
		$StudentCondition = (trim($StudentSelected) == "")? "": ' AND ycu.UserID not in ('.$StudentSelected.') ';
		$YearCondition = (trim($YearID) == "")? 'AND yc.YearID = -1 ': ' AND yc.YearID in ('.$YearID.') ';
		
		if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']==true)
		{
			// Student can join more than one Subject Group in the same Subject 
			$joinTable_JoinedOtherSGOfTheSameSubject = '';
			$cond_JoinedOtherSGOfTheSameSubject = '';
		}
		else
		{
			// Normal flow: Student CANNOT join more than one Subject Group in the same Subject 
			$joinTable_JoinedOtherSGOfTheSameSubject = 'left join
														  (select
														    UserID
														  from
														    SUBJECT_TERM as st
														    inner join
														    SUBJECT_TERM_CLASS as stc
														    on
														    	st.SubjectGroupID = stc.SubjectGroupID 
														    	and st.YearTermID = \''.$YearTermID.'\' 
														    	and st.SubjectID = \''.$SubjectID.'\' 
														    	and stc.SubjectGroupID != \''.$SubjectGroupID.'\'
														    inner join
														    SUBJECT_TERM_CLASS_USER as stcu
														    on
														    stc.SubjectGroupID = stcu.SubjectGroupID) as stcu1
														  on ycu.UserID = stcu1.UserID';
			$cond_JoinedOtherSGOfTheSameSubject = ' And stcu1.UserID IS NULL ';
		} 
		
		$this->With_Nolock_Trans();
		$sql = 'select
						  ycu.UserID,
						  '.$NameField.' as Name,
						  u.UserEmail, 
						  yc.ClassTitleEN,
						  yc.ClassTitleB5,
						  ycu.ClassNumber 
						From
						  YEAR_CLASS_USER as ycu
						  inner join 
						  YEAR_CLASS as yc 
						  on 
						  	ycu.YearClassID = yc.YearClassID 
						  	and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\' 
						  	'.$YearCondition.' 
						  	'.$StudentCondition.' 
						  inner join 
						  INTRANET_USER as u 
						  on ycu.UserID = u.UserID 
						  and RecordType = \'2\' 
					  	and RecordStatus = \'1\' 
					  	and (
					  		UserLogin like \'%'.$SearchValue.'%\' 
					  		OR 
					  		EnglishName like \'%'.$SearchValue.'%\' 
					  		OR 
					  		UserEmail like \'%'.$SearchValue.'%\'
					  		OR
					  		ChineseName like \'%'.$SearchValue.'%\'
					  	)
						  '.$joinTable_JoinedOtherSGOfTheSameSubject.'
						where
						  1
						  '.$cond_JoinedOtherSGOfTheSameSubject.'
						order by 
							ycu.ClassNumber';
		//echo $sql; die;
		return $this->returnArray($sql);
	}
	
	function Check_Class_Code($SubjectGroupID,$ClassCode, $YearTermID) {
				/*$sql = 'select 
							count(1) 
						from 
							SUBJECT_TERM_CLASS 
						Where 
							ClassCode = \''.$ClassCode.'\'
							AND 
							SubjectGroupID != \''.$SubjectGroupID.'\'';*/
		
		$sql = 'select 
							count(1) 
						from 
							SUBJECT_TERM_CLASS stc
							INNER JOIN SUBJECT_TERM st ON st.SubjectGroupID = stc.SubjectGroupID
						Where 
							stc.ClassCode = \''.$ClassCode.'\'
							AND 
							stc.SubjectGroupID != \''.$SubjectGroupID.'\'
							AND 
							st.YearTermID = \''.$YearTermID.'\'
				';
							
		$Result = $this->returnVector($sql);
		
		return ($Result[0] == 0);
	}
	
	function Get_Number_Of_Subject_Taken($YearClassID,$AcademicYearID,$ReturnDetail=false) {
		$sql = 'select
						  st.SubjectID, 
						  count(st.SubjectID) as SubjectSubCount
						from
						  SUBJECT_TERM as st
						  inner join
						  ACADEMIC_YEAR_TERM as yt
						  on
						  st.YearTermID = yt.YearTermID and yt.AcademicYearID = \''.$AcademicYearID.'\'
						  inner join
							SUBJECT_TERM_CLASS as stc
						  on st.SubjectGroupID = stc.SubjectGroupID
							inner join
							SUBJECT_TERM_CLASS_USER as stcu
							on
							stc.SubjectGroupID = stcu.SubjectGroupID
						  inner join
						  YEAR_CLASS_USER as ycu
						  on
						  stcu.UserID = ycu.UserID and YearClassID = \''.$YearClassID.'\'
						Group by
						  st.SubjectID';
		$Result = $this->returnArray($sql);
		
		if ($ReturnDetail) 
			return $Result;
		else 
			return sizeof($Result);
	}
	
	function Get_Number_Of_Subject_Taken_With_FormRelationChecking($YearClassID,$AcademicYearID, $returnDetail=true, $thisYearTermID = '') 
	{
		
		$YearTermID = $thisYearTermID ? $thisYearTermID : GetCurrentSemesterID();

		$sql = "select
				  st.SubjectID, 
				  count(distinct stcu.UserID) as SubjectSubCount
				from
				  	YEAR_CLASS yc
					inner join YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND ycu.YearClassID='$YearClassID') 
					left outer join SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=ycu.UserID)
					left outer join SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID='$YearTermID')
					inner join ACADEMIC_YEAR_TERM yt ON (yt.YearTermID=st.YearTermID AND yt.AcademicYearID='$AcademicYearID')
					inner join SUBJECT_YEAR_RELATION rel ON (rel.YearID=yc.YearID AND rel.SubjectID=st.SubjectID)
				WHERE
					yc.AcademicYearID='$AcademicYearID' and yc.YearClassID='$YearClassID'
				Group by
				  st.SubjectID";
		//echo $sql;
		$Result = $this->returnArray($sql);
		
		$returnAry = array();
		for($i=0; $i<sizeof($Result); $i++) {
			list($sbjID, $count) = $Result[$i];
				$returnAry[$sbjID] = $count;
		}
		
		return $returnDetail? $returnAry : sizeof($Result);
	}
	
	function getAmountOfSubjectTakenByYearClassID($YearClassID)
	{
		$sql = "SELECT COUNT(SubjectID) FROM SUBJECT_YEAR_RELATION syr INNER JOIN YEAR_CLASS yc ON (yc.YearID=syr.YearID) WHERE yc.YearClassID='$YearClassID'";
		$temp = $this->returnVector($sql); 
		return $temp[0];
	}
	
	function Get_Subject_Group_Name_Taken($UserID,$SubjectID,$AcademicYearID) {
		$sql = 'select
						  stc.ClassTitleEN,
						  stc.ClassTitleB5,
						  yt.YearTermNameEN,
						  yt.YearTermNameB5
						from
						  SUBJECT_TERM as st
						  inner join
						  ACADEMIC_YEAR_TERM as yt
						  on
						  st.YearTermID = yt.YearTermID
						  and
						  yt.AcademicYearID = \''.$AcademicYearID.'\'
						  and
						  st.SubjectID = \''.$SubjectID.'\'
						  inner join
						  SUBJECT_TERM_CLASS as stc
							on st.SubjectGroupID = stc.SubjectGroupID
							inner join
							SUBJECT_TERM_CLASS_USER as stcu
							on
							stc.SubjectGroupID = stcu.SubjectGroupID and stcu.UserID = \''.$UserID.'\'';
		return $this->returnArray($sql);
	}
	
	function Get_SubjectGroup_Subject_Mapping($YearTermID='', $TimetableID='')
	{
		$cond_YearTermID = '';
		if ($YearTermID!='')
			$cond_YearTermID = " And st.YearTermID = '$YearTermID' ";
			
		$cond_TimetableID = '';
		if ($TimetableID!='')
		{
			$join_INTRANET_TIMETABLE_ROOM_ALLOCATION = "INNER JOIN
														INTRANET_TIMETABLE_ROOM_ALLOCATION as itra On (st.SubjectGroupID = itra.SubjectGroupID)";
			$cond_TimetableID = " And itra.TimetableID = '$TimetableID' ";
		}
			
		$sql = "";
		$sql .= "	SELECT 
							Distinct(st.SubjectGroupID), s.RecordID as SubjectID, s.EN_DES as SubjectDescEN, s.CH_DES as SubjectDescB5
					FROM 
							SUBJECT_TERM as st
						INNER JOIN
							ASSESSMENT_SUBJECT as s ON (s.RecordID = st.SubjectID)
						$join_INTRANET_TIMETABLE_ROOM_ALLOCATION
					WHERE
							1
							$cond_YearTermID
							$cond_TimetableID
				";
		$resultSet = $this->returnArray($sql);
		$numOfSubjectGroup = count($resultSet);
		
		$returnArr = array();
		for ($i=0; $i<$numOfSubjectGroup; $i++)
		{
			$thisSubjectGroupID = $resultSet[$i]['SubjectGroupID'];
			$returnArr[$thisSubjectGroupID] = $resultSet[$i];
		}
		
		return $returnArr;
	}
	
	function Get_Subject_Group_Info($AcademicYearID='', $YearTermID='', $TimetableID='', $parSubjectGroupIdAry='')
	{
		if  ($AcademicYearID=='' || $YearTermID=='')
		{
			$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
			$YearTermID = $CurrentYearTermArr['YearTermID'];
			$AcademicYearID = $CurrentYearTermArr['AcademicYearID'];
		}
		
		if ($TimetableID != '')
		{
			$timetable_join_table = " inner join INTRANET_TIMETABLE_ROOM_ALLOCATION as itra On (st.SubjectGroupID = itra.SubjectGroupID And itra.TimetableID = '$TimetableID') ";
		}
		
		if ($parSubjectGroupIdAry !== '') {
			$condsSubjectGroupId = " And stc.SubjectGroupID IN ('".implode("','", (array)$parSubjectGroupIdAry)."') ";
			$condsSubjectGroupId2 = " And stct.SubjectGroupID IN ('".implode("','", (array)$parSubjectGroupIdAry)."') ";
		}
		
		$sql = 'select
						  stc.SubjectGroupID,
						  stc.ClassTitleEN,
						  stc.ClassTitleB5,
						  stc.ClassCode as SubjectGroupCode,
						  s.RecordID as SubjectID,
						  s.EN_DES as SubjectDescEN,
						  s.CH_DES as SubjectDescB5
						From
						  SUBJECT_TERM_CLASS as stc
						  inner join
						  SUBJECT_TERM as st
						  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\'
						  inner join
						  ASSESSMENT_SUBJECT as s
						  On (st.SubjectID = s.RecordID)
						  '.$timetable_join_table.'
						Where
						  1
						  '.$condsSubjectGroupId.'
				';
		$SubjectGroupInfoArr = $this->returnArray($sql);
		$numOfSubjectGroup = count($SubjectGroupInfoArr);
		
		$sql = 'select
						  stcu.SubjectGroupID,
						  count(DISTINCT(stcu.UserID)) as Total
						From
						  SUBJECT_TERM_CLASS_USER as stcu
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on stcu.SubjectGroupID = stc.SubjectGroupID
						  inner join
						  SUBJECT_TERM as st
						  on (st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\')
						Where
						  1
						  '.$condsSubjectGroupId.'
						group by
						  stcu.SubjectGroupID;
				';
		$SubjectGroupTotalArr = $this->returnArray($sql);
		$SubjectGroupTotalAssoArr = build_assoc_array($SubjectGroupTotalArr, 'null');
		
		$NameField = getNameFieldByLang('u.');
		$sql_teacher = "SELECT
								stct.SubjectGroupID,
								$NameField as StaffName,
								u.UserLogin as StaffUserLogin
						FROM
								SUBJECT_TERM_CLASS_TEACHER as stct 
							INNER JOIN
								INTRANET_USER as u ON stct.UserID = u.UserID
						Where
						  		1
						  		$condsSubjectGroupId2
						";
		$SubjectGroupTeacherInfoArr = $this->returnArray($sql_teacher);
		$numOfTeacher = count($SubjectGroupTeacherInfoArr);
		
		$SubjectGroupTeacherArr = array();
		$SubjectGroupTeacherLoginArr = array();
		for ($i=0; $i<$numOfTeacher; $i++)
		{
			$thisSubjectGroupID = $SubjectGroupTeacherInfoArr[$i]['SubjectGroupID'];
			$thisStaffName = $SubjectGroupTeacherInfoArr[$i]['StaffName'];
			$thisStaffUserLogin = $SubjectGroupTeacherInfoArr[$i]['StaffUserLogin'];
			
			if (!isset($SubjectGroupTeacherArr[$thisSubjectGroupID]))
				$SubjectGroupTeacherArr[$thisSubjectGroupID] = array();
			if (!isset($SubjectGroupTeacherLoginArr[$thisSubjectGroupID]))
				$SubjectGroupTeacherLoginArr[$thisSubjectGroupID] = array();
				
			$SubjectGroupTeacherArr[$thisSubjectGroupID][] = $thisStaffName;
			$SubjectGroupTeacherLoginArr[$thisSubjectGroupID][] = $thisStaffUserLogin;
		}
		
		$returnArr = array();
		for ($i=0; $i<$numOfSubjectGroup; $i++)
		{
			$thisSubjectGroupID = $SubjectGroupInfoArr[$i]['SubjectGroupID'];
			$returnArr[$thisSubjectGroupID] = $SubjectGroupInfoArr[$i];
			$returnArr[$thisSubjectGroupID]['Total'] = $SubjectGroupTotalAssoArr[$thisSubjectGroupID];
			
			if (!isset($SubjectGroupTeacherArr[$thisSubjectGroupID]))
				$SubjectGroupTeacherArr[$thisSubjectGroupID] = array();
				
			$returnArr[$thisSubjectGroupID]['StaffName'] = $SubjectGroupTeacherArr[$thisSubjectGroupID];
			$returnArr[$thisSubjectGroupID]['StaffUserLogin'] = $SubjectGroupTeacherLoginArr[$thisSubjectGroupID];
		}
		return $returnArr;
	}
	
	function Get_Subject_Group_Info_By_SubjectGroupID($SubjectGroupIDArr='') 
	{
		$sql = "Select
						SubjectGroupID,
						ClassTitleEN as SubjectGroupNameEn,
						ClassTitleB5 as SubjectGroupNameCh
				From
						SUBJECT_TERM_CLASS as stc
				Where
						stc.SubjectGroupID In ('".implode("','", (array)$SubjectGroupIDArr)."')
				";
		return BuildMultiKeyAssoc($this->returnResultSet($sql), 'SubjectGroupID');
	}
	
	function Get_SubjectGroupID_Taken($YearTermID,$ViewMode,$YearClassID="", $UserID="", $LocationID="", $Identity="", $ExcludeSubjectGroupID="", $SubjectGroupIDArr=array())
	{
		$subj_user_table = "SUBJECT_TERM_CLASS_USER";
		$class_user_table = "YEAR_CLASS_USER";
		$class_user_subject_user_join = "inner join";
		
		if($ViewMode == 'Class')
			$yearClassUser_cond = ' and YearClassID = \''.$YearClassID.'\' ';
		else if ($ViewMode == 'Personal')
		{
			$subj_user_table = ($Identity=="Student")? $subj_user_table:"SUBJECT_TERM_CLASS_TEACHER";
			$class_user_table = ($Identity=="Student")? $class_user_table:"YEAR_CLASS_TEACHER";
			$class_user_subject_user_join = ($Identity=="Student")? $class_user_subject_user_join:"left outer join";
			
			$subjectClassUser_cond = ' and stcu.UserID = \''.$UserID.'\' ';
			$yearClassUser_cond = ' and ycu.UserID = \''.$UserID.'\' ';
		}  
		
		# exclude the $ExcludeSubjectGroupID if any
		$conds_ExcludeSubjectGroupID = '';
		if ($ExcludeSubjectGroupID != '')
			$conds_ExcludeSubjectGroupID = " AND st.SubjectGroupID != '$ExcludeSubjectGroupID' ";
			
		# get the specific SubjectGroupID if specified
		$conds_in_SubjectGroupID = '';
		if (is_array($SubjectGroupIDArr) && count($SubjectGroupIDArr) > 0)
		{
			$SubjectGroupIDList = implode(", ", $SubjectGroupIDArr);
			$conds_in_SubjectGroupID = " AND st.SubjectGroupID IN ($SubjectGroupIDList) ";
		}
		
		$sql = 'SELECT 
						st.SubjectGroupID 
				FROM 
						SUBJECT_TERM as st 
					inner join 
						SUBJECT_TERM_CLASS as stc 
						on 
						st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID=\''.$YearTermID.'\' ';
		if($ViewMode == 'Room')
		{
			$sql .= 'inner join 
						INTRANET_TIMETABLE_ROOM_ALLOCATION as tra 
						on 
						stc.SubjectGroupID = tra.SubjectGroupID and tra.LocationID = \''.$LocationID.'\' 
					';        
		}
		else
		{
			$sql .= 'inner join
						'.$subj_user_table.' as stcu 
						on 
						stc.SubjectGroupID = stcu.SubjectGroupID '.$subjectClassUser_cond.'
					'.$class_user_subject_user_join.'
						'.$class_user_table.' as ycu 
						on 
						stcu.UserID = ycu.UserID '.$yearClassUser_cond.' ';
		}        
		
		$sql .= " WHERE 1 ";
		$sql .= $conds_ExcludeSubjectGroupID;
		$sql .= $conds_in_SubjectGroupID;
		$sql .= 'Group by 
						st.SubjectGroupID';
		
		$Result = $this->returnVector($sql);
		return $Result;
		
	}
  
	function Get_Subject_Group_Teacher_List($SubjectGroupID) {
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		$sql = 'Select 
							u.UserID, 
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
								ELSE '.$NameField.' 
							END as TeacherName 
						From 
							SUBJECT_TERM_CLASS_TEACHER as stct 
							LEFT JOIN 
							INTRANET_USER as u 
							on stct.UserID = u.UserID 
							LEFT JOIN 
							INTRANET_ARCHIVE_USER as au 
							on stct.UserID = au.UserID 
						where 
							stct.SubjectGroupID = \''.$SubjectGroupID.'\'';
		return $this->returnArray($sql);
	}
  
	function Get_Subject_Group_Teacher_ListArr($SubjectGroupIdArr) {
		$SubjectGroupIdArr = IntegerSafe($SubjectGroupIdArr);
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		$sql = 'Select 
			stct.SubjectGroupID,
			u.UserID, 
			CASE 
				WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
				ELSE '.$NameField.' 
			END as TeacherName 
		From 
			SUBJECT_TERM_CLASS_TEACHER as stct 
			LEFT JOIN 
			INTRANET_USER as u 
			on stct.UserID = u.UserID 
			LEFT JOIN 
			INTRANET_ARCHIVE_USER as au 
			on stct.UserID = au.UserID 
		where 
			stct.SubjectGroupID IN (\''.implode("','", $SubjectGroupIdArr).'\')';
		$rs = $this->returnResultSet($sql);
		$arr = array();
		foreach($rs as $r){
			$arr[$r['SubjectGroupID']][] = array('UserID' => $r['UserID'], 'TeacherName' => $r['TeacherName']);
		}
		return $arr;
	}
	
	/*
	function Get_Class_Teachers($AcademicYearID, $SubjectID="", $YearClassID)
	{
		# get all classes in the academic year
		
		# get students of each class 
		# find corresponding subject groups
	}
	*/
	
	  function Get_Class_Group_Teachers($AcademicYearID)
	  {
	  		global $eclass_db, $intranet_session_language;
	  		
			if ($intranet_session_language == 'en')
				$targetNameField = 'ClassTitleEN';
			else
				$targetNameField = 'ClassTitleB5';
				
	  		
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		
	  		if (sizeof($TermArr)>0)
	  		{
	  			foreach ($TermArr as $TermID => $TermName)
	  			{
	  				$YearTermIDs[] = $TermID;
	  			}
	  			$sql_year_term_ids = implode(",", $YearTermIDs);
	  		} else
	  		{
	  			# no term settings done
	  		}	  		
	  		
	  		$sql =  " SELECT " .
		  				"	DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.{$targetNameField} AS SubjectClassName " .
		  				" FROM " .
		  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
		  				" WHERE " .
		  				"	st.YearTermID IN ($sql_year_term_ids) AND st.SubjectComponentID=0 " .
		  				" ORDER BY " .
		  				"	stc.ClassTitleEN ";
			$SubjectGroupArr = $this->returnResultSet($sql);
			
			//debug($sql);
			//debug_r($SubjectGroupArr);
			$SubjectGroupIDs = array();
			for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
			{
				$SubjectGroupIDs[] = $SubjectGroupArr[$i]["SubjectGroupID"];
			}
	  		$sql_subject_group_ids = implode(",", $SubjectGroupIDs);
	  		
		  	$NameField = getNameFieldByLang('u.');
			$ArchiveNameField = getNameFieldByLang2('au.');
			$sql = 'Select 
								u.UserID, 
								CASE 
									WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
									ELSE '.$NameField.' 
								END as TeacherName, 
								SubjectGroupID 
							From 
								SUBJECT_TERM_CLASS_TEACHER as stct 
								LEFT JOIN 
								INTRANET_USER as u 
								on stct.UserID = u.UserID 
								LEFT JOIN 
								INTRANET_ARCHIVE_USER as au 
								on stct.UserID = au.UserID 
							where 
								stct.SubjectGroupID in ('.$sql_subject_group_ids.')';
			$Rows = $this->returnArray($sql);
			
			for ($i=0; $i<sizeof($Rows); $i++)
			{
				$RowObj = $Rows[$i];
				$ReturnResult[$RowObj["SubjectGroupID"]][] = $RowObj["TeacherName"];
			}
			
			return $ReturnResult;
	  }
	  
	  
	
	function Get_Subject_Group_Teacher_Info($SubjectGroupIDArr='', $YearTermIDArr='') {
		if ($SubjectGroupIDArr != '') {
			$cond_SubjectGroupID = " And stc.SubjectGroupID In ('".implode("','", (array)$SubjectGroupIDArr)."') ";
		}
		if ($YearTermIDArr != '') {
			$cond_YearTermID = " And st.YearTermID In ('".implode("','", (array)$YearTermIDArr)."') ";
		}
		
		$sql = "Select
						stc.SubjectGroupID, iu.UserID
				From
						SUBJECT_TERM_CLASS as stc
						Inner Join SUBJECT_TERM as st On (stc.SubjectGroupID = st.SubjectGroupID)
						Inner Join SUBJECT_TERM_CLASS_TEACHER as stct On (stc.SubjectGroupID = stct.SubjectGroupID)
						Inner Join INTRANET_USER as iu On (stct.UserID = iu.UserID)
				Where
						1
						$cond_SubjectGroupID
						$cond_YearTermID
				";
		return $this->returnResultSet($sql);
	}
	
	function Get_Subject_Group_Student_List($SubjectGroupIDArr) {
		$NameField = getNameFieldByLang('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		$sql = 'Select 
							stct.SubjectGroupID,
							IF(u.UserID IS NULL, au.UserID, u.UserID) AS UserID, 
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
								ELSE '.$NameField.' 
							END as StudentName 
						From 
							SUBJECT_TERM_CLASS_USER as stct 
						LEFT JOIN 
							INTRANET_USER as u 
						on 
							stct.UserID = u.UserID 
						LEFT JOIN 
							INTRANET_ARCHIVE_USER as au 
						on 
							stct.UserID = au.UserID 
						LEFT JOIN
							SUBJECT_TERM st
						ON 
							stct.SubjectGroupID = st.SubjectGroupID
						LEFT JOIN
							ACADEMIC_YEAR_TERM ayt
						ON 
							st.YearTermID = ayt.YearTermID
						LEFT JOIN
							PROFILE_CLASS_HISTORY pch
						ON
							pch.UserID = u.UserID
						AND
							pch.AcademicYearID = ayt.AcademicYearID
						where 
							stct.SubjectGroupID In ('.implode(',', (array)$SubjectGroupIDArr).') 
						ORDER BY
							pch.ClassName, pch.ClassNumber
						';
//  		debug_pr($sql);
		return $this->returnArray($sql);
	}
	
	/*
	 *	return: array of student who has joined the subject group of the selected subject
	 */
	function Student_Joined_Subject_Group_Of_Subject($SubjectID, $YearTermID, $StudentIDArr=array())
	{
		$cond_StudentID = '';
		if (count($StudentIDArr) > 0)
		{
			$StudentIDList = implode(',', $StudentIDArr);
			$cond_StudentID = " AND UserID In ('".$StudentIDList."') ";
		}
		
		$sql = "Select
						UserID
				From
						SUBJECT_TERM_CLASS_USER as stcu
						INNER JOIN
						SUBJECT_TERM as st
						ON (stcu.SubjectGroupID = st.SubjectGroupID)
				Where
						st.SubjectID = '".$SubjectID."'
						AND
						st.YearTermID = '".$YearTermID."'
						$cond_StudentID
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet;
	}
	
	function Add_Student_To_Subject_Group($SubjectGroupIDArr, $StudentIDArr=array())
	{
		# If no student and no teacher to add => return false
		if (is_array($StudentIDArr) == false || count($StudentIDArr) == 0)
			return false;
			
		if (!is_array($SubjectGroupIDArr))
			$SubjectGroupIDArr = array($SubjectGroupIDArr);
			
		$numOfSubjectGroup = count($SubjectGroupIDArr);
		$numOfStudent = count($StudentIDArr);
		
		for ($i=0; $i<$numOfSubjectGroup; $i++)
		{
			$thisSubjectGroupID = $SubjectGroupIDArr[$i];
			
			# insert class student
			$valueArr = array();
			for ($j=0; $j<$numOfStudent; $j++) {
				$thisStudentID = $StudentIDArr[$j];
				$valueArr[] = " ( '$thisSubjectGroupID', '$thisStudentID', now(), '".$_SESSION['UserID']."' ) ";
			}
			
			$valueList = implode(',', $valueArr);
			$sql = "Insert Into SUBJECT_TERM_CLASS_USER 
						(SubjectGroupID, UserID, DateInput, InputBy)
					Values
						$valueList
					";
			$SuccessArr[$thisSubjectGroupID]['SubjectGroup_Student'] = $this->db_db_query($sql);
			
			
			# Add student to eClass course and iCalendar if the subject group is linked with a course
			$obj_SubjectGroup = new subject_term_class($thisSubjectGroupID);
			$isFromSubjectGroup = $obj_SubjectGroup->Is_eClassCourse_Created_From_SubjectGroup();
			if ($isFromSubjectGroup)
				$SuccessArr[$thisSubjectGroupID]['eClassCourse&iCalendar'] = $this->Add_User_To_eClassCourse_And_iCalendar($thisSubjectGroupID, $StudentIDArr);
		}
		
		if (in_multi_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Delete_Student_From_Subject_Group($SubjectGroupIDArr, $StudentIDArr)
	{
		if (!is_array($SubjectGroupIDArr))
			$SubjectGroupIDArr = array($SubjectGroupIDArr);
		$SubjectGroupIDList = implode(',', $SubjectGroupIDArr);
		
		if (!is_array($StudentIDArr))
			$StudentIDArr = array($StudentIDArr);
		$StudentIDList = implode(',', $StudentIDArr);
			
		$SuccessArr = array();
		$this->Start_Trans();
		
		// remove subject leader 
		$sql = "DELETE FROM INTRANET_SUBJECT_LEADER WHERE ClassID In ($SubjectGroupIDList) AND UserID IN ($StudentIDList)";
		$SuccessArr['DeleteFromSubjectLeader'] = $this->db_db_query($sql);

		// remove subject group student
		$sql = "delete from SUBJECT_TERM_CLASS_USER Where SubjectGroupID In ($SubjectGroupIDList) And UserID In ($StudentIDList)";
		$SuccessArr['DeleteFromSubjectGroup'] = $this->db_db_query($sql);
		
		if (in_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Add_Teacher_To_Subject_Group($SubjectGroupID, $TeacherIDArr=array())
	{
		# If no student and no teacher to add => return false
		if (is_array($TeacherIDArr) == false || count($TeacherIDArr) == 0)
			return false;
			
		// insert class student
		$valueArr = array();
		$numOfTeacher = count($TeacherIDArr);
		for ($i=0; $i<$numOfTeacher; $i++) {
			$thisTeacherID = $TeacherIDArr[$i];
			$valueArr[] = " ( '$SubjectGroupID', '$thisTeacherID', now(), '".$_SESSION['UserID']."' ) ";
		}
		
		$valueList = implode(',', $valueArr);
		$sql = "Insert Into SUBJECT_TERM_CLASS_TEACHER 
					(SubjectGroupID, UserID, DateInput, InputBy)
				Values
					$valueList
				";
		$SuccessArr['SubjectGroup_Teacher'] = $this->db_db_query($sql);
		
		
		# Add student to eClass course and iCalendar if the subject group is linked with a course
		$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
		$isFromSubjectGroup = $obj_SubjectGroup->Is_eClassCourse_Created_From_SubjectGroup();
		if ($isFromSubjectGroup)
		{
			$SuccessArr['eClassCourse&iCalendar'] = $this->Add_User_To_eClassCourse_And_iCalendar($SubjectGroupID, $TeacherIDArr);
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Add_User_To_eClassCourse_And_iCalendar($SubjectGroupID, $UserIDArr=array(), $course_id='', $calID='')
	{
		if (is_array($UserIDArr) == false || count($UserIDArr) == 0)
			return false;
			
		if ($course_id=='')
		{
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$course_id = $obj_SubjectGroup->course_id;
		}
		
		$lo = new libeclass($course_id);
		//include_once('icalendar.php');
		$iCal = new icalendar();
		
		$UserInfoArr = $this->Get_iCalendar_And_eClass_UserInfo($UserIDArr);
		$numOfUser = count($UserInfoArr);
		
		for($i=0; $i<$numOfUser; $i++)
		{
			//if($lo->max_user <> "" && $i == $lo->ticketUser()) break;
			
			$User_ID = $UserInfoArr[$i]['UserID'];
			$UserEmail = $UserInfoArr[$i]['UserEmail'];
			$UserPassword = $UserInfoArr[$i]['UserPassword'];
			$ClassNumber = $UserInfoArr[$i]['ClassNumber'];
			$FirstName = $UserInfoArr[$i]['FirstName'];
			$LastName = $UserInfoArr[$i]['LastName'];
			$ClassName = $UserInfoArr[$i]['ClassName'];
			$Title = $UserInfoArr[$i]['Title'];
			$EngName = $UserInfoArr[$i]['EnglishName'];
			$ChiName = $UserInfoArr[$i]['ChineseName'];
			$NickName = $UserInfoArr[$i]['NickName'];
			$MemberType = $UserInfoArr[$i]['MemberType'];
			
			$eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
			/*
			   if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
			     $eclassClassNumber = $ClassNumber;
			   else
			     $eclassClassNumber = $ClassName." - ".$ClassNumber;
			*/ 
			$lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info, "", $User_ID);
		}
		
		$lo->eClassUserNumber($lo->course_id);
		  
		# insert calendar viewer			
		$Result['insert_calendar_viewer'] = $iCal->insertCalendarViewer($calID, $UserIDArr, "W", $course_id, 'C');
		
		if (in_array(false, $Result))
			return false;
		else
			return true;
	}
	
	function Get_iCalendar_And_eClass_UserInfo($UserIDArr=array())
	{
		if (is_array($UserIDArr) == false || count($UserIDArr) == 0)
			return array();
			
		$sql = "SELECT 
						UserID, 
						UserEmail, 
						UserPassword, 
						ClassNumber, 
						FirstName, 
						LastName, 
						ClassName, 
						CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END as Title, 
						EnglishName, 
						ChineseName, 
						NickName, 
						Case when RecordType = 1 Then 'T' ELSE 'S' END as MemberType
				FROM 
						INTRANET_USER 
				WHERE 
						UserID IN (".implode(",", $UserIDArr).")
				"; 
		$row = $this->returnArray($sql,11);
		
		return $row;
	}
	
	function Check_Timetable_Conflict_Student($StudentSelected,$SubjectGroupID) {
		if (sizeof($StudentSelected) > 0) {
			$sql = 'select
							  stcu.UserID
							from
							  SUBJECT_TERM_CLASS_USER as stcu
							where
							  stcu.SubjectGroupID in (
							    select distinct
							      itra1.SubjectGroupID
							    From
							      INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							    inner join
									INTRANET_SCHOOL_TIMETABLE as t ON (itra.TimetableID = t.TimetableID) /*S74921*/  
								inner join
							      INTRANET_TIMETABLE_ROOM_ALLOCATION as itra1
							      on
							        itra.SubjectGroupID = \''.$SubjectGroupID.'\'
							        and itra.TimeTableID = itra1.TimeTableID
							        and itra.TimeSlotID = itra1.TimeSlotID
							        and itra.Day = itra1.Day
							        and itra1.SubjectGroupID <> \''.$SubjectGroupID.'\')
							  and
							  stcu.UserID in ('.implode(',',$StudentSelected).')';
			return $this->returnVector($sql);
		}
		else {
			return array();
		}
	}
	
	function Check_Timetable_Conflict_Teacher($TeacherSelected,$SubjectGroupID) {
		if (sizeof($TeacherSelected) > 0) {
			$sql = 'select
							  stct.UserID
							from
							  SUBJECT_TERM_CLASS_TEACHER as stct
							where
							  stct.SubjectGroupID in (
							    select distinct
							      itra1.SubjectGroupID
							    From
							      INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							      inner join
							      INTRANET_TIMETABLE_ROOM_ALLOCATION as itra1
							      on
							        itra.SubjectGroupID = \''.$SubjectGroupID.'\'
							        and itra.TimeTableID = itra1.TimeTableID
							        and itra.TimeSlotID = itra1.TimeSlotID
							        and itra.Day = itra1.Day
							        and itra1.SubjectGroupID <> \''.$SubjectGroupID.'\')
							  and
							  stct.UserID in ('.implode(',',$TeacherSelected).')';
			return $this->returnVector($sql);
		}
		else {
			return array();
		}
	}
	
	function Get_Subject_Directory(){
		global $eclass_db, $intranet_session_language, $sys_custom;
		
		$NameField = $intranet_session_language=='b5'?'Chi':'Eng';
		$Name2Field = $intranet_session_language=='b5'?'CH':'EN';
		$Name3Field = $intranet_session_language=='b5'?'B5':'EN';
		
		######## Link to Subject START ########
		if($sys_custom['eClass']['EnableLinkToSubject']){
			$sql = "SELECT 
				course_id, 
				SubjectID 
			FROM 
				{$eclass_db}.course 
			WHERE 
				SubjectID IS NOT NULL 
			AND 
				SubjectID <> 0";
			$rs = $this->returnResultSet($sql);
			
			$subjectCourseLink = array();
			foreach($rs as $r){
				$subjectCourseLink[$r['SubjectID']][] = $r['course_id'];
			}
		}
		######## Link to Subject END ########
		
		$sql = "select  lc.LearningCategoryID, lc.Name{$NameField} as catName,
						s.{$Name2Field}_DES as SubjectName, s.RecordID, c.ClassTitle{$Name3Field} as SubjectGroupName,
						c.SubjectGroupID, c.course_id
				from LEARNING_CATEGORY as lc 
				Left join ASSESSMENT_SUBJECT as s on
					s.RecordStatus = 1 AND
					(s.CMP_CODEID IS NULL OR s.CMP_CODEID = '') AND
					lc.LearningCategoryID = s.LearningCategoryID
				Left join SUBJECT_TERM as t on
					t.SubjectID = s.RecordID
				Left join SUBJECT_TERM_CLASS as c on
					c.SubjectGroupID = t.SubjectGroupID and
					c.course_id is not null and 
					c.course_id <> 0
				where
					lc.RecordStatus = 1		
				order by lc.DisplayOrder ASC, SubjectName ASC
			";
		$sqlResult = $this->returnArray($sql); 
		
		$result = Array();
		foreach($sqlResult as $sr){
			$result[$sr['LearningCategoryID']]['Category'] = $sr;
			if (!empty($sr['RecordID'])){
				$result[$sr['LearningCategoryID']]['Subject'][$sr['RecordID']]=$sr;
				
				if (!empty($sr['SubjectGroupID']))
					$result[$sr['LearningCategoryID']]['SubjectGroup'][$sr['RecordID']][$sr['SubjectGroupID']]=$sr;
				
				if ($sys_custom['eClass']['EnableLinkToSubject'] && $subjectCourseLink[ $sr['RecordID'] ]){
					$result[$sr['LearningCategoryID']]['SubjectLink'][$sr['RecordID']] = $subjectCourseLink[ $sr['RecordID'] ];
				}
			}
		}
		return $result;
	}
	
	function Get_Related_CourseID($subjectID){
		global $eclass_db, $sys_custom;
		
		$courseIdArr = array();
		
		$sql = "select c.course_id from SUBJECT_TERM as t inner join
				SUBJECT_TERM_CLASS as c on
					c.SubjectGroupID = t.SubjectGroupID
					and c.course_id is not null and 
					c.course_id <> 0
				where 
					t.SubjectID = '$subjectID'
				";
		$rs = $this->returnVector($sql);
		$courseIdArr = $rs;
		
		if($sys_custom['eClass']['EnableLinkToSubject']){
			$sql = "SELECT course_id FROM {$eclass_db}.course where SubjectID = '{$subjectID}'";
			$rs = $this->returnVector($sql);
			$courseIdArr = array_merge($courseIdArr, $rs);
		}
		return $courseIdArr;
	}
	
	function Get_Unclassified_CourseID(){
		global $eclass_db, $sys_custom;
		
		if($sys_custom['eClass']['EnableLinkToSubject']){
			$subjectIdSQL = 'AND
				(
					SubjectID is null 
				or 
					SubjectID = 0
				)';
		}
		$sql = "select 
			course_id 
		from 
			{$eclass_db}.course 
		where 
			RoomType='0' 
		AND 
			(
				(
					SubjectGroupID is null 
				or 
					SubjectGroupID = '' 
				or 
					SubjectGroupID = 0
				)
				{$subjectIdSQL}
			)
		";
		return $this->returnVector($sql);
	}
	
	function Get_Copied_SubjectGroup($SubjectGroupID, $YearTermID='')
	{
		$YearTermID_conds = '';
		if ($YearTermID != '')
			$YearTermID_conds = " And st.YearTermID = '".$YearTermID."' ";
			
		$sql = "Select
						Distinct(stc.SubjectGroupID)
				From
						SUBJECT_TERM_CLASS as stc
						Inner Join
						SUBJECT_TERM as st
						On (stc.SubjectGroupID = st.SubjectGroupID)
				Where
						stc.CopyFromSubjectGroupID = '".$SubjectGroupID."'
						$YearTermID_conds
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet;
	}
	
	function Get_Term_Copied_SubjectGroup($FromYearTermID, $ToYearTermID)
	{
		$sql = "Select
						stcFrom.SubjectGroupID as FromSubjectGroupID,
						stcTo.SubjectGroupID as ToSubjectGroupID
				From
						SUBJECT_TERM_CLASS as stcFrom
						Inner Join
						SUBJECT_TERM as stFrom
						On (stcFrom.SubjectGroupID = stFrom.SubjectGroupID)
						Inner Join
						SUBJECT_TERM_CLASS as stcTo
						On (stcFrom.SubjectGroupID = stcTo.CopyFromSubjectGroupID)
						Inner Join
						SUBJECT_TERM as stTo
						On (stcTo.SubjectGroupID = stTo.SubjectGroupID)
				Where
						stFrom.YearTermID = '".$FromYearTermID."'
						And
						stTo.YearTermID = '".$ToYearTermID."'
				";
		$SubjectGroupArr = $this->returnArray($sql);
		$numOfSubjectGroup = count($SubjectGroupArr);
		
		$returnArray = array();
		if ($numOfSubjectGroup > 0)
		{
			$returnArray = build_assoc_array($SubjectGroupArr);
		}
		
		return $returnArray;
	}
	
	function Has_SubjectGroup_Copied_To_YearTerm($SubjectGroupID, $YearTermID)
	{
		$CopiedSubjectGroupIDArr = $this->Get_Copied_SubjectGroup($SubjectGroupID, $YearTermID);
		
		if (count($CopiedSubjectGroupIDArr) > 0)
			return true;
		else
			return false;
	}
	
	function Get_eClass_UserID($course_id, $UserIDArr)
	{
		global $eclass_db;
		
		if (is_array($UserIDArr) == false)
			$UserIDArr = array();
			
		$sql = "select 
					user_id 
				from 
					{$eclass_db}.user_course as c
					inner join 
					INTRANET_USER as u 
					on
					c.user_email = u.UserEmail
				where 
					c.course_id = '$course_id' 
					and 
					u.UserID in ('".implode("','",$UserIDArr)."')
				";
		$eClassUserIDArr = $this->returnVector($sql); 
		
		return $eClassUserIDArr;
	}
	
	function Get_Subject_Group_ID_Code_Mapping($YearTermID)
	{
		$sql = "Select
						stc.ClassCode, stc.SubjectGroupID
				From
						SUBJECT_TERM_CLASS as stc
						Inner Join
						SUBJECT_TERM as st
						On (stc.SubjectGroupID = st.SubjectGroupID)
				Where
						st.YearTermID = '".$YearTermID."'
				";
		$SubjectGroupInfoArr = $this->returnArray($sql);
		$numOfSubjectGroup = count($SubjectGroupInfoArr);
		
		$ReturnArr = array();
		for ($i=0; $i<$numOfSubjectGroup; $i++)
		{
			$thisSubjectGroupID = $SubjectGroupInfoArr[$i]['SubjectGroupID'];
			$thisSubjectGroupCode = $SubjectGroupInfoArr[$i]['ClassCode'];
			
			$ReturnArr[$thisSubjectGroupCode] = $thisSubjectGroupID;
		}
		
		return $ReturnArr;
	}
	
	function Get_User_Accessible_Subject_Group($ParUserID, $AcademicYearID, $YearTermIDArr='',$subjectID='')
	{
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$libuser = new libuser($ParUserID);
		$UserType = $libuser->RecordType;
		
		$USER_TABLE = '';
		if ($UserType == USERTYPE_STAFF)
			$USER_TABLE = 'SUBJECT_TERM_CLASS_TEACHER';
		else if ($UserType == USERTYPE_STUDENT)
			$USER_TABLE = 'SUBJECT_TERM_CLASS_USER';
		
		if ($YearTermIDArr != '')
			$conds_YearTermID = " And st.YearTermID In (".implode(',', (array)$YearTermIDArr).") ";
		
		if(trim($subjectID)!=''){
			$subjectIDCond = " And s.RecordID IN (".implode("','", (array)$subjectID).") ";
			
		}
		
		$sql = "Select
						s.RecordID as SubjectID, s.EN_DES as SubjectNameEn, s.CH_DES as SubjectNameCh, 
						stc.SubjectGroupID, stc.ClassTitleEN as SubjectGroupNameEn, stc.ClassTitleB5 as SubjectGroupNameCh 
				From 
						SUBJECT_TERM_CLASS as stc
						Inner Join
						$USER_TABLE as ut On (stc.SubjectGroupID = ut.SubjectGroupID)
						Inner Join
						SUBJECT_TERM as st On (stc.SubjectGroupID = st.SubjectGroupID)
						Inner Join
						ACADEMIC_YEAR_TERM as ayt On (st.YearTermID = ayt.YearTermID)
						Inner Join
						ASSESSMENT_SUBJECT as s On (st.SubjectID = s.RecordID)
				Where
						ut.UserID = '".$ParUserID."'
						And ayt.AcademicYearID = '".$AcademicYearID."'
						And (s.CMP_CODEID Is Null Or s.CMP_CODEID = '')
						$conds_YearTermID
						$subjectIDCond
				Order By
						s.DisplayOrder, stc.ClassTitleEN
				";
// 			debug_pr($sql);
		return $this->returnArray($sql);
	}
	
	function getSubjectGroupIDByStudentID($StudentID, $YearTermID="")
	{
		if($YearTermID=="") $YearTermID = GetCurrentSemesterID();
		
		$sql = "SELECT st.SubjectID, st.SubjectGroupID FROM SUBJECT_TERM st INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) WHERE stcu.UserID='$StudentID' AND st.YearTermID='$YearTermID'";
		$result = $this->returnArray($sql);
		$returnAry = array();
		for($i=0; $i<sizeof($result); $i++) {
			list($SubjectID, $SubjectGroupID) = $result[$i];
			$returnAry[$SubjectID] = $SubjectGroupID;
		}
		return $returnAry;
	}
	
	function returnSubjectTeacherClass($ParUserID='', $ClassLevelID='', $AcademicYearID=0)
	{
		global $intranet_session_language;
		
		$ClassNameField = ($intranet_session_language=='en')? 'ClassTitleEN' : 'ClassTitleB5';
		
		if ($AcademicYearID==0)
		{
	 		$AcademicYearID = Get_Current_Academic_Year_ID();
		}
	 	
	 	$ClassLevelCond = ($ClassLevelID != "")? " and yc.YearID = '$ClassLevelID'" : "";
	 	if(trim($ParUserID)!='')
	 		$cond_UserID = " AND stct.UserID = '$ParUserID' ";
	 	
	 	$sql = "SELECT
	 					DISTINCT(yc.YearClassID) as ClassID,
	 					yc.$ClassNameField as ClassName,
	 					yc.YearID as ClassLevelID,
						yc.ClassTitleEN,
						yc.ClassTitleB5
	 			FROM
	 					SUBJECT_TERM_CLASS_TEACHER as stct
	 					INNER JOIN
	 					SUBJECT_TERM_CLASS_USER as stcu
	 					ON (stct.SubjectGroupID = stcu.SubjectGroupID)
	 					INNER JOIN
	 					YEAR_CLASS_USER as ycu
	 					ON (stcu.UserID = ycu.UserID)
	 					INNER JOIN
	 					YEAR_CLASS as yc
	 					ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$AcademicYearID."' $ClassLevelCond)
				WHERE
					1
					$cond_UserID
	 			ORDER BY
	 				yc.Sequence
	 			";
	 			
		return $this->returnArray($sql);
	}
	
	function getSubjectGroupIDByYearClassID($YearClassID, $YearTermID="")
	{
		if($YearTermID=="") $YearTermID = GetCurrentSemesterID();
		
		$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE YearTermID='$YearTermID'";
		$year = $this->returnVector($sql);
		
		//$sql = "SELECT st.SubjectID, st.SubjectGroupID, ".Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN")." as SgTitle FROM SUBJECT_TERM st INNER JOIN SUBJECT_TERM_CLASS stc ON (st.SubjectGroupID=stc.SubjectGroupID) INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.YearClassID='$YearClassID' AND yc.AcademicYearID='".$year[0]."') WHERE st.YearTermID='$YearTermID' GROUP BY st.SubjectGroupID ORDER BY st.SubjectID, st.SubjectGroupID";
		$sql = "SELECT st.SubjectID, st.SubjectGroupID, ".Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN")." as SgTitle FROM SUBJECT_TERM st INNER JOIN SUBJECT_YEAR_RELATION syr ON (st.SubjectID=syr.SubjectID AND st.YearTermID='$YearTermID') INNER JOIN YEAR_CLASS yc ON (yc.YearID=syr.YearID AND yc.YearClassID='$YearClassID' AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') INNER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=st.SubjectGroupID)";
		//echo $sql;
		$result = $this->returnArray($sql);
		//debug_pr($result);
		$returnAry = array();
		for($i=0; $i<sizeof($result); $i++) {
			list($SubjectID, $SubjectGroupID, $sgTitle) = $result[$i];
			$returnAry[$SubjectID][$SubjectGroupID] = $sgTitle;
		}
		
		return $returnAry;
	} 
}
?>