<?
// using: Siuwan
/*
 * 	Log
 *  Date:	2016-05-30 [Siuwan] [ip.2.5.7.7.1]
 * 			- modified function getSuggestedVocabForStudent(), use preg_split() instead of split() for PHP5.4
 *  Date:	2015-11-30 [Paul] [ip.2.5.7.1.1]
 * 			- modified createEclassUserAccountForWriting20(), remove stristr($classNumber, $className) checking
 *  Date: 	2015-10-06 [Siuwan] [ip.2.5.6.10.1]
 * 			- modified function getStudentStepHandinHistorySelection(), hide draft selection if current version is 1 and correction area is showing
 *  Date: 	2015-10-04 [Jason] [ip.2.5.6.10.1]
 * 			- edit createStepHandinHisoryRecord() to empty the answermarked field when redo
 * 			- edit getHandinOverallMark() to handle empty mark but show '0' instead in mark
 *
 * 	Date:	2015-02-10 [Cameron] modify getGradeSelection(), don't use Lang value in logic because different Lang have different values,
 * 				therefore, remove string checking agaiinst 'Secondary'
 * 				add function isSecondary()
 */

class libW2 {
	private $objDb;

	public function __construct() {
		$this->objDb = new libdb();
    }

	/**
	* Get the Subject Applied to the client
	*
	* @return	Array of the Internal Subject Code
	*/
	public function getApplicableContentAry() {
		global $w2_cfg, $plugin;

		$showIconArr = array();

		$showIconArr[] = $w2_cfg['contentArr']['chi']['contentCode'];
		$showIconArr[] = $w2_cfg['contentArr']['eng']['contentCode'];

//		if ($plugin['iPortfolio_env'] == 'DEV') {
			$showIconArr[] = $w2_cfg['contentArr']['ls']['contentCode'];
//		}

		$showIconArr[] = $w2_cfg['contentArr']['ls_eng']['contentCode'];

//		if ($plugin['iPortfolio_env'] == 'DEV') {
			$showIconArr[] = $w2_cfg['contentArr']['sci']['contentCode'];
//		}

		return $showIconArr;
	}


	### Updated @20140620
	### @param Boolean $w2_thisIsAdmin check if the user can see and use content input tab
	/**
	* Get the Top Tab Menu Html
	*
	* @param	Int 	$userType	user type of the user
	* @param	String 	$tabTask	task of current page to determine the current tab
	* @return	Array of the Internal Subject Code
	*/
	public function generateTabMenu($userType, $tabTask, $taskAction,$r_contentCode) {
		global $w2_cfg, $PATH_WRT_ROOT;

		### Get Tab Menu
		$tabArr = $w2_cfg["tabArr"][$userType];

		if(!in_array($r_contentCode,$w2_cfg['contentInput']['allowContentCode']) && $userType ==1)
		{
			$tabArr = array($w2_cfg["tabArr"][$userType][1]);
		}
		## Get Current Tab
		$tabIndex = 0;
		if ($taskAction == $w2_cfg["actionArr"]["marking"]) {
			$tabIndex = CFG_W2_TAB_TEACHER_MANAGEMENT;
		}
		else if ($taskAction == $w2_cfg["actionArr"]["peerMarking"]) {
			$tabIndex = CFG_W2_TAB_STUDENT_PEER_MARKING;
		}
		else if ($taskAction == $w2_cfg["actionArr"]["sharing"]) {
	    	$tabIndex = ($userType ==USERTYPE_STAFF)?
	    				CFG_W2_TAB_TEACHER_SHARE_WRITING:
	    					CFG_W2_TAB_STUDENT_SHARE_WRITING;
		}
		else {
			switch ($tabTask) {
				case "listWriting":
			        $tabIndex = CFG_W2_TAB_STUDENT_INCOMPLETE_WRITING;
			        break;
			    case "completedWriting":
			        $tabIndex = CFG_W2_TAB_STUDENT_COMPLETEED_WRITING;
			        break;
			    case "listMarkWriting":
			    case "listMarkWritingStudent":
			        $tabIndex = CFG_W2_TAB_STUDENT_PEER_MARKING;
			        break;
			    case "listSharingWriting":
			    	$tabIndex = ($userType ==USERTYPE_STAFF)?
			    				CFG_W2_TAB_TEACHER_SHARE_WRITING:
			    					CFG_W2_TAB_STUDENT_SHARE_WRITING;
			        break;
			    case "management":
			        $tabIndex = CFG_W2_TAB_TEACHER_MANAGEMENT;
			        break;
			    case "templateManagement":
			    case "newTemplateWriting":
			    case "newChiTemplateWriting":
			    case "newLsEngTemplateWriting":
			    case "newSciTemplateWriting":
			    		$tabIndex = CFG_W2_TAB_TEACHER_TEMPLATE_MGMT;
			        break;
//			    case "publishAll":
//			    case "publishSelectContent":
//			    case "publish":
//			    	$tabIndex = CFG_W2_TAB_TEACHER_PUBLISH;
//			        break;
			    case "listWritingReport":
			    case "listWritingClassReport":
			    case "viewStudentWriting":
			        $tabIndex = CFG_W2_TAB_TEACHER_REPORT;
			        break;
			    default:
			    	$tabIndex = CFG_W2_TAB_STUDENT_INCOMPLETE_WRITING;
			        break;
			}
		}


		if(in_array($r_contentCode,$w2_cfg['contentInput']['allowContentCode']))
		{
			$tabArr[$tabIndex][3] = 1;
		}

		### 20140620 Updated
		### Check if the user is admin
		//if($r_contentCode=='eng')
		//20140911 Siuwan Check admin user inside function instead
		$isAdmin = $this->isAdminUser();
		if(!$isAdmin && $userType == USERTYPE_STAFF){
			unset($tabArr[CFG_W2_TAB_TEACHER_TEMPLATE_MGMT]);
		}
		$x = '';
		$x .= '<div class="resource_tab print_hide">'."\r\n";
			$x .= '<ul>'."\r\n";
				foreach ((array)$tabArr as $_tabIndex => $_tabInfoArr) {
					$_title = $_tabInfoArr[0];
					$_link = $_tabInfoArr[1];

					$_icon = $_tabInfoArr[2];
					$_iconPath = $PATH_WRT_ROOT.$w2_cfg["imagePath"].$_icon;

					$_isCurrentTab = $_tabInfoArr[3];
					$_tabClass = ($_isCurrentTab)? 'current_r_tab current_r_tab_qb' : 'r_tab_qb';

					$x .= '<li class="'.$_tabClass.'">'."\r\n";
						$x .= '<a href="'.$_link.'">'."\r\n";
							$x .= '<span><img border="0" align="absmiddle" src="'.$_iconPath.'"> '.$_title.'</span>'."\r\n";
						$x .= '</a>'."\r\n";
					$x .= '</li>'."\r\n";
				}
			$x .= '</ul>'."\r\n";
		$x .= '</div>'."\r\n";

		return $x;
	}

	/**
	* return a HTML path for client to walk step
	*
	* @param : ARRAY $stepArr , suppose the array are all the step of a give writing, and supppose the step sequence is order by asc
	* @param : String $schemeCode , Scheme code
	* @param : INT $writingid, Writing ID
	* @param : STRING $content, the writing content (eng, chi..)
	* @return : String HTML for the walking step
	*/
//	function getWalkingStepOld($stepArr,$schemeCode,$writingid,$content,$currentStepId,$studentStepStatusArr = null){
//		global $w2_cfg;
//		//$needCheckStep = 0;
////debug_r($studentStepStatusArr);
////		if(is_array($studentStepStatusArr) && count($studentStepStatusArr) > 0){
////			$needCheckStep = 1;
////		}
////		debug_r($needCheckStep);
//		$html = '<div class="icon_step">';
//
//		for($i = 0,$i_max = count($stepArr);$i < $i_max; $i++){
//
//			$_onClick = '';
//			$strJS = htmlGoToStep($schemeCode,$stepArr[$i]['CODE'],$stepArr[$i]['STEP_ID'],$writingid,$content);
//
//			$_class  = ($stepArr[$i]['STEP_ID'] <= $currentStepId)? 'visit' : 'block' ;
//
//
////			if($needCheckStep){
//				if($i == 0){
//					//this i == 0, this is a first step, default allow click this step
//					$_onClick = ' OnClick = "'.$strJS.'"';
//				}else{
//					//check whether to allow student to click this step
//					for($j = 0,$j_max = count($studentStepStatusArr);$j<$j_max;$j++){
//						if($studentStepStatusArr[$j]['STEP_ID'] == $stepArr[$i]['STEP_ID'] &&
//							$studentStepStatusArr[$j]['STEP_STATUS'] == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]){
//							$_onClick = ' OnClick = "'.$strJS.'"';
//							break;
//						}
//					}
//				}
////			}else{
//				//no need to check step, open all step to click
////				$_onClick = ' OnClick = "'.$strJS.'"';
////			}
//
//			$html .= '<a href="#" class="'.$_class.'" "'.$_onClick.'">'.$stepArr[$i]['SEQUENCE'].'</a>';
//
//			if($i == intval($i_max-1)){
//				//this is the last step
//				//do nothing
//			}else{
//				$html .= '<span></span>';
//			}
//		}
//		$html .= '</div>';
//		return $html;
//	}
	function getTextTypeSelection($r_contentCode,$r_contentTypeSuper="",$r_contentType="",$grade=""){
		global $Lang;
		$h_select2 = '<select name="contentTypeSlt" id="id_contentTypeSlt" onchange="updateSchemeCodeSlt(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\')">';
			$h_select2 .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';


			$_typeAry = $this->getTextTypeSuperArray($r_contentCode,$r_contentTypeSuper,$grade);
			foreach((array)$_typeAry as $_type => $_typeName){
				$_selected = '';
				if($r_contentType != '' && $r_contentType == $_type){
					$_selected = ' SELECTED ';
				}
				$h_select2 .= '<option value="'.$_type.'" '.$_selected.'>'.$_type.'</option>';
			}

			$h_select2 .= '</select>';
			return $h_select2;
	}
	function getTextTypeSuperArray($r_contentCode,$r_typeSuperValue,$grade=""){
		global $Lang;
		$allTitleForThisContent = $this->getContentList($r_contentCode);
		$r_typeSuperValue = str_replace("\'", "'", $r_typeSuperValue);
		$returnAry = array();
		foreach ((array)$allTitleForThisContent as $eachSchemeCode => $schemeDetails){
			$_grade = $schemeDetails['grade'];
			$_typeSuper = $schemeDetails['type_super'];
			$_type = $schemeDetails['type'];
			$_name = $schemeDetails['name'];
			$_introduction = $schemeDetails['introduction'];

			if(!empty($grade)&&$grade==$_grade||empty($grade)||$grade==$Lang['W2']['na_without_slash']&&empty($_grade)){
					if( $_typeSuper == $r_typeSuperValue){
						$returnAry[$_type] = array('type'=>$_type);
					}
			}
		}
		asort($returnAry);
		return $returnAry;
	}
	function getTextTypeArray($r_contentCode,$r_typeSuperValue,$r_typeValue,$grade=""){
		global $Lang;
		$allTitleForThisContent = $this->getContentList($r_contentCode);
		$r_typeValue = str_replace("\'", "'", $r_typeValue);
		$r_typeSuperValue = str_replace("\'", "'", $r_typeSuperValue);
		$returnAry = array();
		foreach ((array)$allTitleForThisContent as $eachSchemeCode => $schemeDetails){
			$_grade = $schemeDetails['grade'];
			$_typeSuper = $schemeDetails['type_super'];
			$_type = $schemeDetails['type'];
			$_name = $schemeDetails['name'];

			if(!empty($grade)&&$grade==$_grade||empty($grade)||$grade==$Lang['W2']['na_without_slash']&&empty($_grade)){
				//there is two types of checking
				//1) compare ($_type  == $r_typeValue) is a must
				//2) if $r_superTypeVal has value(so far is for eng now) , add the value if $_typeSuper == $r_superTypeVal
				if( $_type== $r_typeValue){
					if($r_typeSuperValue == ''||(!empty($r_typeSuperValue)&&$_typeSuper == $r_typeSuperValue)){
						$returnAry[$eachSchemeCode] = array('name'=>$_name);
					}

				}
			}
		}
		asort($returnAry);
		return $returnAry;
	}
	function getSchemeCodeSelection($r_contentCode,$r_contentTypeSuper="",$r_contentType="",$r_schemeCode="",$grade=""){
		global $Lang;
		$h_selectSchemeCode = '<select class="requiredField" id="r_schemeCode" name="r_schemeCode" onchange="updateWritingSlt(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\')">';
		$h_selectSchemeCode .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
		$_selected = '';
		$selectSchemeTypes = $this->getTextTypeArray($r_contentCode,$r_contentTypeSuper,$r_contentType,$grade);
		foreach((array)$selectSchemeTypes as $_schemeCode => $_schemeAry){
			$_selected = ($r_schemeCode == $_schemeCode) ? ' SELECTED ': '';
			$h_selectSchemeCode .= '<option value= "'.$_schemeCode.'" '.$_selected.'>'.$_schemeAry['name'].'</option>';
		}
		$h_selectSchemeCode .= '</select>';
		return $h_selectSchemeCode;
	}
	function getWritingSelection($r_contentCode,$schemeCodeAry=array(),$r_writingId=""){
		global $Lang,$w2_thisUserID;
		$objWritingTeacher = new libWritingTeacher($w2_thisUserID);
		$accessiableOnly = $this->isAdminUser();
		$teacherWritingInfoArr = $objWritingTeacher->findTeacherWritingByContent($r_contentCode, $accessiableOnly, $schemeCodeAry);
		$WritingStudentCnt = count($teacherWritingInfoArr);
		$h_selectExercise = '<select class="requiredField" id="r_writingId" name="r_writingId" >';
		$h_selectExercise .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
		$_selected = '';
		for($i =0; $i < $WritingStudentCnt; $i++){
			$_selected = ($r_writingId == $teacherWritingInfoArr[$i]['WRITING_ID']) ? ' SELECTED ': '';
			$h_selectExercise .= '<option value= "'.$teacherWritingInfoArr[$i]['WRITING_ID'].'" '.$_selected.'>'.$teacherWritingInfoArr[$i]['TITLE'].'</option>';
		}
		$h_selectExercise .= '</select>';
		return $h_selectExercise;
	}
	function getWalkingStep($stepArr, $schemeCode, $writingId, $content, $currentStepId, $studentStepStatusArr, $isWritingCompleted, $action, $currentStepCode) {
		global $w2_cfg;


		### Get Step info for this writing
		$writingStepInfoArr = $this->getWritingStep($writingId);
		$writingStepAssoArr = BuildMultiKeyAssoc((array)$writingStepInfoArr, 'STEP_ID');
		unset($writingStepInfoArr);


		### Build associative array for student writing step info
		$studentStepAssoArr = BuildMultiKeyAssoc((array)$studentStepStatusArr, 'STEP_ID');
		if ($action == $w2_cfg["actionArr"]["marking"] || $action == $w2_cfg["actionArr"]["preview"] || $isWritingCompleted) {
			$displayMode = $w2_cfg["walkingStepModeArr"]["stepFree"];	// user can go to all steps
		}
		else {
			$displayMode = $w2_cfg["walkingStepModeArr"]["stepWise"];	// user can only go to step sequencially
		}
		$html = '';
		$html .= '<div class="icon_step">'."\r\n";
			if ($displayMode == $w2_cfg["walkingStepModeArr"]["stepWise"]) {
//				$curStudentStepSequence = $writingStepAssoArr[$currentStepId]['SEQUENCE'];
				$curStudentStepSequence = $writingStepAssoArr[$currentStepId]['STEP_SEQUENCE'];


				for($i=0, $i_max=count($stepArr); $i<$i_max; $i++){
					$_stepId = $stepArr[$i_max-$i-1]['STEP_ID'];
					$_stepCode = $stepArr[$i_max-$i-1]['STEP_CODE'];
					$_stepSequence = $stepArr[$i_max-$i-1]['STEP_SEQUENCE'];
					$_stepRequireApproval = $stepArr[$i_max-$i-1]['STEP_REQUIRE_APPROVAL'];
					$_studentThisStepApprovalStatus = $studentStepAssoArr[$_stepId]['STEP_APPROVAL_STATUS'];
					if($curStudentStepSequence == $_stepSequence){
					}
					//$_isCurrentStep = ($_stepId == $currentStepId)? true : false;
					$_isCurrentStep = ($_stepCode == $currentStepCode)? true : false;

					$_previousStepId = $stepArr[$i-1]['STEP_ID'];
					$_isPreviousStepSubmitted = ($studentStepAssoArr[$_previousStepId]['STEP_STATUS'] == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"])? true : false;

					$_previousStepRequireApproval = $stepArr[$i-1]['STEP_REQUIRE_APPROVAL'];
					$_studentPreviousStepApprovalStatus = $studentStepAssoArr[$_previousStepId]['STEP_APPROVAL_STATUS'];


					$_ballBallOnclick = '';
					$_ballBallClass = '';
					$_arrowClass = '';

					if ($studentStepAssoArr[$_stepId]['STEP_STATUS'] == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"] || $_isPreviousStepSubmitted) {
							// submitted => ball ball clickable
							$_ballBallOnclick = ' onclick="'.htmlGoToStep($schemeCode, $_stepCode, $_stepId, $writingId, $content).'" ';
							if ($_stepSequence < $curStudentStepSequence) {
								// submitted and before current step => highlight ball ball and arrow
//								$_ballBallClass = 'current';
//								$_arrowClass = 'visit';
							}
							else if ($_isCurrentStep) {
								// submitted and is current step => highlight ball ball only
								$_ballBallClass = 'current';
							}
							else {
								// submitted and after current step => not hightlighted
							}

							//further checking, for current step , if preview step require approval and preview step has not approved by teacher, this step should not clickable 	ie. $_ballBallOnclick = '';
							if($_previousStepRequireApproval == $w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["YES"] && $_studentPreviousStepApprovalStatus != $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]){
								$_ballBallOnclick = '';
							}
					}
					else if ($_isCurrentStep) {
						// not submitted but current step => highlight ball ball only and not clickable
						$_ballBallClass = 'current';
					}
					else {
						// not submitted and not current step => not highlighted and not clickable
					}

					$html .= '<a href="javascript:void(0);" id="icon_step'.$_stepSequence.'" class="'.$_ballBallClass.'" '.$_ballBallOnclick.'><span>'.$_stepSequence.'</span></a>'."\r\n";

//					if($i == intval($i_max-1)) {
//						// last step => do not show the arrow
//					}
//					else {
//						$html .= '<span class="'.$_arrowClass.'"></span>'."\r\n";
//					}
				}
			}
			else if ($displayMode == $w2_cfg["walkingStepModeArr"]["stepFree"]) {
				for($i=0, $i_max=count($stepArr); $i<$i_max; $i++){

					$_stepId = $stepArr[$i_max-$i-1]['STEP_ID'];
					$_stepCode = $stepArr[$i_max-$i-1]['STEP_CODE'];
					$_stepSequence = $stepArr[$i_max-$i-1]['STEP_SEQUENCE'];

					//$_isCurrentStep = ($_stepId == $currentStepId)? true : false;
					$_isCurrentStep = ($_stepCode == $currentStepCode)? true : false;

//					$_ballBallClass = ($_isCurrentStep)? 'visit' : 'visit_free';
					$_ballBallClass = ($_isCurrentStep)? 'current' : '';
					$_ballBallOnclick = ' onclick="'.htmlGoToStep($schemeCode, $_stepCode, $_stepId, $writingId, $content).'" ';
					$html .= '<a href="javascript:void(0);" class="'.$_ballBallClass.'" '.$_ballBallOnclick.' id="icon_step'.$_stepSequence.'"><span>'.$_stepSequence.'</span></a>'."\r\n";

//					if($i == intval($i_max-1)) {
//						// last step => do not show the arrow
//					}
//					if($i == 1) {
//						// last step => do not show the arrow
//					}
//					else {
//						$_arrowClass = '';
//						$html .= '<span class="'.$_arrowClass.'"></span>'."\r\n";
//					}
				}
			}

		$html .= '</div>'."\r\n";
		return $html;
	}

	/**
	* return related file path for content file
	*
	* @param : String $content, CONTENT CODE
	* @param : STRING $schemeCode , SCHEME CODE
	* @param : STRING $stepCode, STEP CODE
	* @return : ARRAY $fileDetails, a list of file name for a step content
	*/
	function getModelFile($content,$schemeCode,$stepCode){
		global $intranet_root;

		$_thisSchemeFolder = $this->getThisSchemeFolder($content,$schemeCode);


		$modelFile = $_thisSchemeFolder.'/'.$stepCode.'/model.php';
		$viewFile = $_thisSchemeFolder.'/'.$stepCode.'/view.php';

		$fileDetails['schemePath'] = $_thisSchemeFolder.'/';
		$fileDetails['path'] = $_thisSchemeFolder.'/'.$stepCode;
		$fileDetails['model'] = $modelFile;
		$fileDetails['view'] = $viewFile;
		return $fileDetails;
	}


	/**
	* return the scheme file path
	*
	* @param : String $content, CONTENT CODE
	* @param : STRING $schemeCode , SCHEME CODE
	* @return : STRING the scheme path
	*/
	function getThisSchemeFolder($content,$schemeCode){
			global $intranet_root;
			return $intranet_root.'/home/eLearning/w2/content/'.$content.'/'.$schemeCode;
	}

	/**
	* return a HTML BUTTON that with goToStep function
	*
	* @param : String $schemeCode , Scheme Code
	* @param : String $ stepCode, Step Code
	* @param : INT $stepID, Step ID
	* @param : INT $preNextButton
	*             		1   ---> Next Button
	*                   2   ---> Pre Button
	* @return : String HTML of the BUTTON
	*/

	function htmlStepButton($schemeCode,$stepCode,$stepID,$writingid,$content,$preNextButton = 1){


			if($preNextButton == 1){
				$buttonCaption = ' Next Step ';
				$buttonID = $buttonName = 'nextStepCmd';
			}else{
				$buttonCaption = 'Pre Step';
				$buttonID = $buttonName  = 'preStepCmd';
			}


			$js_goToStep = htmlGoToStep($schemeCode,$stepCode,$stepID,$writingid,$content);

			$htmlButton = '<input id = "'.$buttonID.'" type = "button" name="'.$buttonName.'" value ="'.$buttonCaption.'" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value ="'.$buttonCaption.'" onClick="'.$js_goToStep.'">';


			return $htmlButton;
	}


	/**
	* Generate HTML BUTTON FOR HANDLE SUTDNE SAVE HANDIN
	* @owner : Fai (20111028)
	* @param : INTEGER $goNextStepFlag
				0	->	don't go to next step AND set the step status as draft
				1	->	go to next step	AND set the step status as submit
				2	->	don't go to next step AND set the step status as submit
	* @param : STRING $buttonCaption , Can specific the caption/value for the button
	* @param : BOOLEAN $disabled , Can specific if the button is disabled by default
	* @param : INTEGER $stepStatus , update the step status with what value
	* @return : STRING HTML BUTTON FOR STUDENT TO SAVE HAND IN
	*
	*/
	function htmlSaveStudentHandInButton($goNextStepFlag,$buttonCaption = null, $disabled=false,$stepStatus=null){
			global $w2_cfg, $Lang;
			$stepStatus = ($stepStatus == null)? $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"] :$stepStatus;

			$allowGoToNextStep = 1; // default allow go to next step
			if($goNextStepFlag == 0 || $goNextStepFlag == 2){
				$allowGoToNextStep = 0; // does not allow go to next step
			}

			$js_Action = 'saveStepAns('.$allowGoToNextStep.','.$stepStatus.')';
//debug_r($goNextStepFlag);
			if(trim($buttonCaption) == ''){
				//$Lang['W2']['submitted']
//				$buttonCaption = ($goNextStepFlag == 1) ? $Lang['W2']['saveAndNextStep'] :  $Lang['W2']['saveAsDraft'];
				switch($goNextStepFlag){
					case 1:
						$buttonCaption = $Lang['W2']['saveAndNextStep'];
						break;
					case 2:
						$buttonCaption = $Lang['W2']['submitted'];
						break;
					default :
						$buttonCaption = $Lang['W2']['saveAsDraft'];
						break;
				}
			}else{
				//do nothing, display the Button name as $buttonCaption
			}

			$disabledAttr = '';
			if ($disabled) {
				$disabledAttr = 'disabled';
			}

			$htmlButton = '<input id = "saveButton" type = "button" value = "'.$buttonCaption.'" name ="saveButton" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" onClick="'.$js_Action.'" '.$disabledAttr.'>';

			return $htmlButton;
	}
	function htmlSaveStudentHandInWithAttachmentButton($goNextStepFlag,$buttonCaption = null, $disabled=false,$stepStatus=null){
			global $w2_cfg, $Lang;
			$stepStatus = ($stepStatus == null)? $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"] :$stepStatus;

			$allowGoToNextStep = 1; // default allow go to next step
			if($goNextStepFlag == 0 || $goNextStepFlag == 2){
				$allowGoToNextStep = 0; // does not allow go to next step
			}

			$js_Action = 'uploadStudentAttachment('.$allowGoToNextStep.','.$stepStatus.')';

			if(trim($buttonCaption) == ''){
				switch($goNextStepFlag){
					case 1:
						$buttonCaption = $Lang['W2']['saveAndNextStep'];
						break;
					case 2:
						$buttonCaption = $Lang['W2']['submitted'];
						break;
					default :
						$buttonCaption = $Lang['W2']['saveAsDraft'];
						break;
				}
			}else{
				//do nothing, display the Button name as $buttonCaption
			}

			$disabledAttr = '';
			if ($disabled) {
				$disabledAttr = 'disabled';
			}

			$htmlButton = '<input id = "saveButton" type = "button" value = "'.$buttonCaption.'" name ="saveButton" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" onClick="'.$js_Action.'" '.$disabledAttr.'>';

			return $htmlButton;
	}
	function htmlSubmitStudentHandInButton(){
		global $Lang;

//		$js_Action = 'saveFinalStepAns()';
		$js_Action = 'submitFinalStep()';

		$htmlButton = '<input id = "submitHandinButton" type = "button" name ="submitHandinButton" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value ="'.$Lang['Btn']['Submit'].'" onClick="'.$js_Action.'">';

		return $htmlButton;
	}

	/**
	* Get the DB Table and Pagination template for display
	*
	* @param	String 	$dbTableCode	the code to represent the position of the db table
	* @param	String 	$paginationCode	the code to represent the position of the pagination
	* @return	String	html of the template included both db table and pagination
	*/
	public function getDbTableDivTemplate($dbTableCode, $paginationCode,$title="") {
		$x = '';
		$x .= '<div class="usermgmt_table_190">'."\r\n";
			$x .= (!empty($title))?"<h1 class=\"ex_name\">".$title."</h1>":"";
        	$x .= '<div class="table_board">'."\r\n";
				$x .= '<div class="table_left"><div class="table_right">'."\r\n";
					$x .= $dbTableCode."\r\n";
				$x .= '</div>'."\r\n";
				$x .= '<div class="table_bottom_left">'."\r\n";
					$x .= '<div class="table_bottom_right">'."\r\n";
						$x .= $paginationCode."\r\n";
					$x .= '</div>'."\r\n";
				$x .= '</div>'."\r\n";
			$x .= '</div>'."\r\n";
		$x .= '</div>'."\r\n";

		return $x;
	}
//	/**
//	* Get the DB Table and Pagination template for display
//	*
//	* @param	String 	$dbTableCode	the code to represent the position of the db table
//	* @param	String 	$paginationCode	the code to represent the position of the pagination
//	* @return	String	html of the template included both db table and pagination
//	*/
//	public function getDbTableDivTemplate($dbTableCode, $paginationCode) {
//		$x = '';
//		$x .= '<div class="usermgmt_table_190">'."\r\n";
//        	$x .= '<div class="table_board">'."\r\n";
////				$x .= '<div class="table_top_left">'."\r\n";
//				$x .= '<div class="">'."\r\n";
////					$x .= '<div style="height:10px" class="table_top_right">'."\r\n";
//					$x .= '<div style="height:10px" class="">'."\r\n";
//					$x .= '</div>'."\r\n";
//				$x .= '</div>'."\r\n";
//				$x .= '<div class="table_left">'."\r\n";
//					$x .= '<div class="table_right">'."\r\n";
//						$x .= $dbTableCode."\r\n";
//					$x .= '</div>'."\r\n";
//				$x .= '</div>'."\r\n";
//				$x .= '<div class="table_bottom_left">'."\r\n";
//					$x .= '<div class="table_bottom_right">'."\r\n";
//						$x .= $paginationCode."\r\n";
//					$x .= '</div>'."\r\n";
//				$x .= '</div>'."\r\n";
//			$x .= '</div>'."\r\n";
//		$x .= '</div>'."\r\n";
//
//		return $x;
//	}

	/**
	* GET NEXT OR PRE STEP ID
	*
	* @param : Array STEP DETAIS FOR A GIVEN WRITING ID
	* @param : INT $currentStepId , CURRENT STEP ID
	* @param : INT $nextStep , indicate need next step or prev step
	* @param : INT $requestInfo , request what information should be return
	*             		1   ---> stepId
	*                   2   ---> stepCode
	* @return : INT pre / next step  CODE, Depend on $nextStep; return 0 --> no pre or next step
	*/
	function getNextPreStepInfo($stepArray,$currentStepId,$nextStep = 1 , $requestInfo = 1)
	{

		for($i = 0,$i_max = count($stepArray); $i < $i_max; $i++)
		{
			$t_step_id = $stepArray[$i]["STEP_ID"];

			if($t_step_id == $currentStepId)
			{
				$prev_step_id = $stepArray[$i-1]["STEP_ID"];
				$prev_step_code = $stepArray[$i-1]["STEP_CODE"];
				$next_step_id = $stepArray[$i+1]["STEP_ID"];
				$next_step_code = $stepArray[$i+1]["STEP_CODE"];
				break;
			}
		}

		$returnStepId = ($nextStep == 1)? $next_step_id: $prev_step_id;
		$returnStepId = is_numeric($returnStepId)? $returnStepId : 0;

		$returnStepCode = ($nextStep == 1)? $next_step_code: $prev_step_code;
		$returnStepCode = trim($returnStepCode);

		switch($requestInfo){
			case 1:
				$returnInfo = $returnStepId;
				break;
			case 2:
				$returnInfo	= $returnStepCode;
				break;
			default:
				break;
		}


		return $returnInfo;
	}
	function updateStudentWritingHandinStatus($writingId,$studentId,$setStatus){
		global $intranet_db,$w2_cfg;

		$sql = 'update '.$intranet_db.'.W2_WRITING_STUDENT set HANDIN_SUBMIT_STATUS = "'.$setStatus.'" ,HANDIN_SUBMIT_STATUS_DATE = now() where WRITING_ID = \''.$writingId.'\' and USER_ID = \''.$studentId."'";
		$this->objDb->db_db_query($sql);
	}

	public function getStepInfoByContent($content) {
		global $intranet_root;

		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');
		$xmlstr = get_file_content($intranet_root."/home/eLearning/w2/content/$content/structure.xml");
		$xml = new SimpleXMLElement($xmlstr);

		ini_set('zend.ze1_compatibility_mode', $mode);
		$stepInfoArr = array();
		for($i=0,$i_max = count($xml->step); $i<$i_max; $i++){
			$stepInfoArr[$i]['titleEng'] = (string) $xml->step[$i]->titleEng;
			$stepInfoArr[$i]['titleChi'] = (string) $xml->step[$i]->titleChi;
			$stepInfoArr[$i]['sequence'] = (string) $xml->step[$i]->sequence;
			$stepInfoArr[$i]['code'] = (string) $xml->step[$i]->code;
		}

		return $stepInfoArr;
	}

	/**
	* GET a set of writing that a student has
	* @owner : Fai (20111026)
	* @param : NIL
	* @return : ARRAY , array of a set of writing
	*
	*/
	public function findStudentWriting($studentId, $content, $writingIdAry=null){
		global $intranet_db,$w2_cfg;

		if ($writingIdAry !== null) {
			$conds_writingId = " And w.WRITING_ID In ('".implode("','", (array)$writingIdAry)."') ";
		}

		$sql = 'select
					s.WRITING_STUDENT_ID, w.WRITING_ID , w.TITLE as `TITLE`, w.CONTENT_CODE, s.HANDIN_SUBMIT_STATUS, s.OTHER_INFO
				from
					'.$intranet_db.'.W2_WRITING_STUDENT as s
				inner join
					'.$intranet_db.'.W2_WRITING as w on s.WRITING_ID = w.WRITING_ID
				where
					s.DELETE_STATUS = '.$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"].'
					and w.DELETE_STATUS = '.$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"].'
					and s.USER_ID = \''.$studentId.'\'
					and w.CONTENT_CODE = \''.$content.'\'
					'.$conds_writingId.'
				';
		$result = $this->objDb->returnResultSet($sql);
		return $result;
	}

	public function findShareWritingByStudentSQL($studentID, $contentCode){
		global $intranet_db,$w2_cfg;

		$nameField = getNameFieldByLang2("u.");
		$archiveNameField = getNameFieldByLang2("ua.");

		if($studentID != null){
			$conds = " and s.USER_ID = '{$studentID}'";
		}
		$sql = "Select hw.WRITING_ID, hw.Title as WRITING_NAME, hw.SCHEME_CODE, hs.USER_ID,
				h.DATE_INPUT as SHARING_DATE, step.STEP_ID, hs.WRITING_STUDENT_ID,
				IF(ua.UserID is not null, $archiveNameField, $nameField) as STUDENT_NAME,
				u.ClassName, u.ClassNumber
				from ".$intranet_db.".W2_WRITING_STUDENT s
				inner join ".$intranet_db.".W2_WRITING w on s.WRITING_ID = w.WRITING_ID
				inner join ".$intranet_db.".W2_SHARE_TO_STUDENT_HANDIN h on w.SCHEME_CODE = h.SCHEME_CODE
				inner join ".$intranet_db.".W2_STEP step on h.STEP_ID = step.STEP_ID
				inner join ".$intranet_db.".W2_WRITING_STUDENT hs on step.WRITING_ID = hs.WRITING_ID and hs.USER_ID = h.STUDENT_ID
				inner join ".$intranet_db.".W2_WRITING hw on hs.WRITING_ID = hw.WRITING_ID
				left join ".$intranet_db.".INTRANET_USER u on h.STUDENT_ID = u.UserID
				left join ".$intranet_db.".INTRANET_ARCHIVE_USER ua on h.STUDENT_ID = ua.UserID
				where w.CONTENT_CODE = '$contentCode'
				and hw.CONTENT_CODE = '$contentCode'
				and w.END_DATE_TIME < now()
				{$conds}
				group by hw.WRITING_ID, hw.Title, hs.USER_ID
				";
//		debug_r($sql);
		return $sql;
	}

	public function findShareWritingByStudent($studentID, $contentCode){

		$sql = $this->findShareWritingByStudentSQL($studentID, $contentCode);

		$result = $this->objDb->returnResultset($sql);
		return $result;
	}

	public function findStudentWritingByContentSql($studentIdAry, $content, $writingStatusArr=null, $withinPeriod=null, $allowPeerMarking=null, $withinPeerMarkingPeriod=null, $publicWritingOnly=false){
		global $intranet_db,$w2_cfg;

		if ($writingStatusArr !== null) {
			$condsWritingStatus = " and s.HANDIN_SUBMIT_STATUS In ('".implode("','", (array)$writingStatusArr)."') ";
		}

		if ($withinPeriod !== null) {
			if ($withinPeriod) {
				$condsSubmissionPeriod = " and (w.START_DATE_TIME <= now() and now() <= w.END_DATE_TIME) ";
			}
			else {
				$condsSubmissionPeriod = " and (now() < w.START_DATE_TIME || now() > w.END_DATE_TIME) ";
			}
		}

		if ($allowPeerMarking !== null) {
			$targetStatus = ($allowPeerMarking)? $w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING"]["allow"] : $w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING"]["notAllow"];
			$condsAllowPeerMarking = " and w.ALLOW_PEER_MARKING = '".$targetStatus."' ";
		}

		if ($withinPeerMarkingPeriod !== null) {
			if ($withinPeerMarkingPeriod) {
				$condsPeerMarkingPeriod = " and (w.PEER_MARKING_START_DATE <= now() and now() <= w.PEER_MARKING_END_DATE) ";
			}
			else {
				$condsPeerMarkingPeriod = " and (now() < w.PEER_MARKING_START_DATE || now() > w.PEER_MARKING_END_DATE) ";
			}
		}
		if($publicWritingOnly){
			$condsPublicWritingOnly = " and w.Record_Status = '".W2_STATUS_PUBLIC."'";
		}
		$str = "select w.WRITING_ID , w.TITLE as `TITLE`, w.CONTENT_CODE, w.SCHEME_CODE, s.HANDIN_SUBMIT_STATUS_DATE, w.END_DATE_TIME, s.WRITING_STUDENT_ID, s.USER_ID from ".$intranet_db.".W2_WRITING_STUDENT
				as s inner join ".$intranet_db.".W2_WRITING as w on s.WRITING_ID = w.WRITING_ID where s.DELETE_STATUS = ".$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"]." and w.DELETE_STATUS =
				".$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"]."  and s.USER_ID in ('".implode("','", (array)$studentIdAry)."') and w.CONTENT_CODE = '".$content."' $condsWritingStatus $condsSubmissionPeriod $condsAllowPeerMarking $condsPeerMarkingPeriod $condsPublicWritingOnly";

		return $str;
	}


	public function findStudentWritingByContent($studentIdAry, $content, $writingStatusArr=null, $withinPeriod=null, $allowPeerMarking=null, $withinPeerMarkingPeriod=null, $publicWritingOnly=false){
		global $intranet_db,$w2_cfg;

		$sql = $this->findStudentWritingByContentSql($studentIdAry, $content, $writingStatusArr, $withinPeriod, $allowPeerMarking, $withinPeerMarkingPeriod, $publicWritingOnly);

		$result = $this->objDb->returnResultset($sql);
		return $result;
	}

	/**
	* RETURN WHETHER STUDENT HAS ANY FOOTPRINT IN THE STEP BY A GIVEN WRITING ID
	* @owner : Fai (20111101)
	* @RETURN : ARRAY OF A RESULT THAT INDICATE WHETHER A STUDENT HAS ANY FOOTPRINT FOR A STEP ID
	*/
	public function findStudentWritingStepStatus($studentId, $writingId){
		$resultSet = array();

		if(is_numeric($studentId) && is_numeric($writingId)){
			$resultSet = $this->getStudentWritingStepStatus($studentId, $writingId);
		}

		return $resultSet;
	}

	/**
	* RETURN WHETHER STUDENT HAS ANY FOOTPRINT IN THE STEP BY A GIVEN WRITING ID
	* @owner : Fai (20111101)
	* @RETURN : ARRAY OF A RESULT THAT INDICATE WHETHER A STUDENT HAS ANY FOOTPRINT FOR A STEP ID
	*/
	public function getStudentWritingStepStatus($studentIdAry=null, $writingIdAry=null, $stepIdAry=null, $handinSubmitStatusAry=null, $withComment=null, $commentStatusAry=null) {
		global $intranet_db, $w2_cfg;

		$condsStudentId = '';
		if ($studentIdAry) {
			$condsStudentId = " And ss.USER_ID In ('".implode("','", (array)$studentIdAry)."') ";
		}

		$condsWritingId = '';
		if ($writingIdAry) {
			$condsWritingId = " And s.WRITING_ID In ('".implode("','", (array)$writingIdAry)."') ";
		}

		$condsStepId = '';
		if ($stepIdAry) {
			$condsStepId = " And ss.STEP_ID In ('".implode("','", (array)$stepIdAry)."') ";
		}

		$condsHandinSubmitStatus = '';
		if ($handinSubmitStatusAry) {
			$condsHandinSubmitStatus = " And ws.HANDIN_SUBMIT_STATUS In ('".implode("','", (array)$handinSubmitStatusAry)."') ";
		}

		$condsCommentId = '';
		if ($withComment === true) {
			$condsCommentId = " And (ssc.COMMENT_ID != '' And ssc.COMMENT_ID is not null) ";
		}
		else if ($withComment === false) {
			$condsCommentId = " And ssc.COMMENT_ID is null ";
		}

		$condsCommentStatus = '';
		if ($commentStatusAry) {
			$condsCommentStatus = " And ssc.COMMENT_STATUS In ('".implode("','", (array)$commentStatusAry)."') ";
		}

		$W2_STEP_STUDENT = $intranet_db.'.W2_STEP_STUDENT';
		$W2_STEP = $intranet_db.'.W2_STEP';
		$W2_WRITING_STUDENT = $intranet_db.'.W2_WRITING_STUDENT';
		$W2_STEP_STUDENT_COMMENT = $intranet_db.'.W2_STEP_STUDENT_COMMENT';
		$INTRANET_USER = $intranet_db.'.INTRANET_USER';
		$sql = "select
						ss.STEP_ID as `STEP_ID`, ss.USER_ID as `USER_ID`, s.WRITING_ID, ss.STEP_STATUS as `STEP_STATUS`, s.CODE as 'STEP_CODE', ssc.COMMENT_ID, s.SEQUENCE,
						ssc.COMMENT_STATUS,ss.APPROVAL_STATUS as `STEP_APPROVAL_STATUS`
				from
						$W2_STEP_STUDENT as ss
						inner join $W2_STEP as s on (s.STEP_ID = ss.STEP_ID)
						inner join $W2_WRITING_STUDENT as ws on (s.WRITING_ID = ws.WRITING_ID and ss.USER_ID = ws.USER_ID)
						inner join $INTRANET_USER as ui on (ui.UserID = ss.USER_ID)
						left outer join $W2_STEP_STUDENT_COMMENT as ssc on (ss.STEP_ID = ssc.STEP_ID AND ss.USER_ID = ssc.USER_ID)
				where
						ws.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"]."'
						$condsStudentId
						$condsWritingId
						$condsStepId
						$condsHandinSubmitStatus
						$condsCommentId
						$condsCommentStatus
				order by
						s.SEQUENCE
				";
		// Missing Delete Status Check
//		$sql = "select
//						ss.STEP_ID as `STEP_ID`, ss.USER_ID as `USER_ID`, s.WRITING_ID, ss.STEP_STATUS as `STEP_STATUS`, s.CODE as 'STEP_CODE', ssc.COMMENT_ID, s.SEQUENCE,
//						ssc.COMMENT_STATUS,ss.APPROVAL_STATUS as `STEP_APPROVAL_STATUS`
//				from
//						$W2_STEP_STUDENT as ss
//						inner join $W2_STEP as s on (s.STEP_ID = ss.STEP_ID)
//						inner join $W2_WRITING_STUDENT as ws on (s.WRITING_ID = ws.WRITING_ID and ss.USER_ID = ws.USER_ID)
//						left outer join $W2_STEP_STUDENT_COMMENT as ssc on (ss.STEP_ID = ssc.STEP_ID AND ss.USER_ID = ssc.USER_ID)
//				where
//						1
//						$condsStudentId
//						$condsWritingId
//						$condsStepId
//						$condsHandinSubmitStatus
//						$condsCommentId
//						$condsCommentStatus
//				order by
//						s.SEQUENCE
//				";
//	debug_r($sql);
		return $this->objDb->returnResultSet($sql);
	}

	/**
	* Return the result set of the student in each writing for a specific handin status (if specified)
	* @owner : Ivan (20111102)
	* @param : String $content, code of the content
	* @param : Int/Array $writingIdAry, include this para if retrieving specific writing only
	* @param : Int/Array $handinSubmitStatusAry, include this para if retrieving specific handin submit status only
	* @return : Array result set of the student of each writing
	*/
	public function getWritingStudentInfoAry($content, $writingIdAry=null, $handinSubmitStatusAry=null) {
		global $intranet_db, $w2_cfg;

		$condsWritingId = '';
		if ($writingIdAry !== null) {
			$condsWritingId = " And ws.WRITING_ID In ('".implode("','", (array)$writingIdAry)."') ";
		}

		$condsHandinSubmitStatus = '';
		if ($handinSubmitStatusAry !== null && $handinSubmitStatusAry !== '') {
			$condsHandinSubmitStatus = " And ws.HANDIN_SUBMIT_STATUS In ('".implode("','", (array)$handinSubmitStatusAry)."') ";
		}

		$W2_WRITING_STUDENT = $intranet_db.'.W2_WRITING_STUDENT';
		$W2_WRITING = $intranet_db.'.W2_WRITING';
		$INTRANET_USER = $intranet_db.'.INTRANET_USER';
		$sql = "Select
						ws.USER_ID,
						iu.ClassName as CLASS_NAME,
						iu.ClassNumber as CLASS_NUMBER,
						iu.EnglishName as USER_ENGLISH_NAME,
						iu.ChineseName as USER_CHINESE_NAME,
						ws.WRITING_ID,
						w.SCHEME_CODE,
						ws.HANDIN_SUBMIT_STATUS,
						ws.HANDIN_SUBMIT_STATUS_DATE,
						ws.WRITING_STUDENT_ID
				From
						$W2_WRITING_STUDENT as ws
						Inner Join $W2_WRITING as w On (ws.WRITING_ID = w.WRITING_ID)
						Inner Join $INTRANET_USER as iu On (ws.USER_ID = iu.UserID)
				Where
						ws.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"]."'
						And w.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"]."'
						And w.CONTENT_CODE = '".$content."'
						$condsWritingId
						$condsHandinSubmitStatus
				Order By
						iu.ClassName, iu.ClassNumber
				";
		$resultSet = $this->objDb->returnResultSet($sql);
//		debug_r($sql);
		return $resultSet;
	}

	/**
	* Return the students whose writing is to be marked
	* @owner : Ivan (20111128)
	* @param : Int/Array $writingIdAry, include this para if retrieving specific writing only
	* @return : Array result set of the students whose writing is to be marked
	*/
	public function getWritingToMarkStudent($writingIdAry) {
		global $w2_cfg;

		### Get Last Step of each writings
		$stepInfoAry = $this->getWritingStep($writingIdAry);
		$stepInfoAssoAry = BuildMultiKeyAssoc($stepInfoAry, 'WRITING_ID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		$lastStepIdAry = array();
		foreach ((array)$stepInfoAssoAry as $_writingId => $_writingStepInfoAry) {
			$lastStepIdAry[] = $_writingStepInfoAry[count((array)$_writingStepInfoAry) - 1]['STEP_ID'];
		}

		### Retrieve the Last Step info of each students in each writings
		// step without comment
		$lastStepStudentInfoAry = $this->getStudentWritingStepStatus($studentIdAry=null, $parWritingIdAry=null, $lastStepIdAry, $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"], $withComment=false);
		// step with student-has-read comment

		$lastStepStudentInfoAry = array_merge($lastStepStudentInfoAry, $this->getStudentWritingStepStatus($studentIdAry=null, $parWritingIdAry=null, $lastStepIdAry, $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"], $withComment=true, $w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["studentHasRead"]));

		return BuildMultiKeyAssoc($lastStepStudentInfoAry, 'WRITING_ID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
	}
	public function getStepIdByStepCode($contentCode,$writingId,$stepCode="",$template=""){
		global $w2_cfg,$intranet_db;
		if(empty($stepCode)){
			$stepCode = array_search($template, $w2_cfg['HandinArr']['WritingTemplate'][$contentCode]);
		}
		$sql = "SELECT STEP_ID FROM ".$intranet_db.".W2_STEP WHERE WRITING_ID = '".$writingId."' AND CODE = '".$stepCode."'";
		return current($this->objDb->returnVector($sql));

	}
	/**
	* Return the css class of the related status
	* @owner : Ivan (20111101)
	* @param : Int status code of the record
	*
	* @return : String the css class of complete icon
	*/
	public function getCompleteStatusIconClass($statusCode) {
		global $w2_cfg;

		$cssClass = '';
		switch ($statusCode) {
			case $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]:
				$cssClass = 'complete';
				break;
			default:
				$cssClass = 'incomplete';
		}

		return $cssClass;
	}

	/**
	* Return the parameter string of the url for the given array info
	* @owner : Ivan (20111103)
	* @param : Array $paraAssoAry, an array to contain info to be converted. The "key" is the parameter name and the "value" is the parameter value.
	*
	* @return : String, the parameter string WITHOUT the starting "?"
	*/
	public function getUrlParaByAssoAry($paraAssoAry) {
		$paraAry = array();
		foreach ((array)$paraAssoAry as $_paraName => $_paraValue) {
			$paraAry[] = $_paraName.'='.$_paraValue;
		}
		return implode('&', $paraAry);
	}

	/**
	* Return the scheme name according to the given scheme code
	* @owner : Ivan (20111103)
	* @param : String $schemeCode, the scheme code
	*
	* @return : String, the scheme name
	*/
	public function getSchemeNameBySchemeCode($schemeCode,$contendCode = 'eng') {
		global $w2_cfg,$intranet_root;

		include_once($intranet_root."/includes/w2/libW2ContentFactory.php");

		$w2_objContent = libW2ContentFactory::createContent($contendCode);
		$w2_objContent->setSchemeCode($schemeCode);

		return $w2_objContent->getSchemeName();
	}


	public function getNavigationHtml() {
		return '';
//		return <<<html
//<div class="main_top">
//	<div class="navigation"><a href="#">Home</a> > <a href="190_Writing2.0_eng_incomplete_main.htm">English exercise</a> > Writing</div>
//</div>
//html;
	}

	/**
	* Return the html of the approval action button
	* @owner : Ivan (20111103)
	* @param : Int $iconStatus, the target button status
	* @param : Int $stepApprovalStatus, the current step status (affecting the css class of the button)
	* @param : String $onclick, onclick javascript of the button
	* @return : String, the html if the action button
	*/
	public function getUpdateApprovalStatusBtn($iconStatus, $stepApprovalStatus, $onclick) {
		global $w2_cfg, $Lang;

		switch ($iconStatus) {
			case $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]:
				$class = 'btn_wait';
				$divId = 'w2_stepApproval_wait';
				$text = $Lang['Btn']['WaitingForApproval'];
				break;
			case $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]:
				$class = 'btn_approve';
				$divId = 'w2_stepApproval_approve';
				$text = $Lang['Btn']['Approve'];
				break;
			case $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]:
				$class = 'btn_redo';
				$divId = 'w2_stepApproval_redo';
				if($iconStatus==$stepApprovalStatus){
					$text = $Lang['W2']['RedoSent'];
				}else{
					$text = $Lang['Btn']['Redo'];
				}
				break;
			default:
				$class = '';
				$btnText = '';
		}

		if ($iconStatus == $stepApprovalStatus) {
			$class .= '_current';
		}

		if ($onclick != '') {
			$onclickAttr = ' onclick="'.$onclick.'" ';
		}

		return '<a id= "'.$divId.'" class="'.$class.'" '.$onclickAttr.'><span>'.$text.'</span></a>';
	}

	/**
	* Return the html of the student drop down list to navigate to different students within a step
	* @owner : Ivan (20111103)
	* @param : String $contentCode, the content of the writing (e.g. eng, chi, ...)
	* @param : Int $writingId, target writingId
	* @param : Int $stepId, target stepId
	* @param : Int $selectedStudentID, preset selected studentId
	* @param : String $onclick, onclick javascript of the selection
	* @return : String, the HTML of the student drop-down list
	*/
	public function getHandinStudentSelectionHtml($contentCode, $writingId, $stepId, $selectedStudentID, $onchange, $handinSubmitStatus, $includeStudentIdAry=null) {
		global $w2_cfg;

		$writingStudentInfoAry = $this->getWritingStudentInfoAry($contentCode, $writingId, $handinSubmitStatus);
		$writingStudentIDAry = Get_Array_By_Key($writingStudentInfoAry, 'USER_ID');
		$numOfWritingStudent = count($writingStudentIDAry);

		$stepStudentInfoAry = $this->getStudentWritingStepStatus($writingStudentIDAry, $writingId, $stepId);
		$stepStudentAssoAry = BuildMultiKeyAssoc($stepStudentInfoAry, 'USER_ID', 'STEP_STATUS', $SingleValue=1);

		$x = '';
		$x .= '<div style="float:left" class="page_no">'."\r\n";
			$x .= '<span>'."\r\n";
				$x .= '<select onchange="'.$onchange.'">'."\r\n";
					for ($i=0, $i_max=$numOfWritingStudent; $i<$i_max; $i++) {
						$_studentID = $writingStudentInfoAry[$i]['USER_ID'];

						if (is_array($includeStudentIdAry) && !in_array($_studentID, (array)$includeStudentIdAry)) {
							continue;
						}

						$_studentName = Get_Lang_Selection($writingStudentInfoAry[$i]['USER_CHINESE_NAME'], $writingStudentInfoAry[$i]['USER_ENGLISH_NAME']);
						$_className = $writingStudentInfoAry[$i]['CLASS_NAME'];
						$_classNumber = $writingStudentInfoAry[$i]['CLASS_NUMBER'];
						$_stepStatus = $stepStudentAssoAry[$_studentID];

						$_optionText = '';
						$_optionText .= $_studentName.' ('.$_className.'-'.$_classNumber.')';
						if ($_stepStatus==$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]) {
							$_optionText .= ' ['.$this->getStatusTextByCode($_stepStatus).']';
						}

						//$_disabled = ($_stepStatus==$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"])? false : true;
						$_disabled = false;
						$_selected = ($_studentID == $selectedStudentID)? true : false;

						$x .= $this->getSelectOptionHtml($_studentID, $_optionText, $_selected, $_disabled);
					}
				$x .= '</select>'."\r\n";
			$x .= '</span>'."\r\n";
		$x .= '</div>'."\r\n";

		return $x;

//		return <<<html
//<div style="float:left" class="page_no">
//	<a title="First hand-in" class="first" href="#">&nbsp;</a>
//	<a title="Previous hand-in" class="prev" href="#">&nbsp;</a>
//    <span>
//	    <select id="select" name="select">
//		      <option>4D-01 CHAN Tai Man</option>
//		      <option>4D-02 CHAN Si Kit</option>
//		      <option>4D-03 CHEUNG Chi Sum</option>
//		  </select>
//	</span>
//    <a title="Next hand-in" class="next" href="#">&nbsp;</a>
//    <a title="Last hand-in" class="last" href="#">&nbsp;</a>
//</div>
//html;
	}

	/**
	* Return the html of the option in a drop down list for different status
	* @owner : Ivan (20111103)
	* @param : Int/Sting $value, the value of the option
	* @param : String $text, the text display of the option
	* @param : Boolean $isSelected, the flag to determine if the option is selected or not
	* @param : Boolean $isDisabled, the flag to determine if the option is disabled or not
	* @return : String, the HTML of the option
	*/
	private function getSelectOptionHtml($value, $text, $isSelected, $isDisabled=false) {
		$selected = ($isSelected)? 'selected' : '';

		$x = '';
		if ($isDisabled) {
			$x .= '<optgroup style="color:#CCCCCC;" value="'.intranet_htmlspecialchars($value).'" label=" '.intranet_htmlspecialchars($text).'" '.$selected.'> '.$text.'</optgroup>'."\r\n";
		}
		else {
			$x .= '<option value="'.intranet_htmlspecialchars($value).'" label="'.intranet_htmlspecialchars($text).'" '.$selected.'>'.$text.'</option>'."\r\n";
		}

		return $x;
	}

	/**
	* Return STEP ANS by a giving step code and writing ID and student id
	*
	* @param : Array $stepCodeArr Step Handin Code
	* @param : INT $writingID , writing ID
	* @param : INT $studentID , Student User ID
	* @return : Student answer for the giving writing ID and step handin code and student id
	*/
	public function getWritingStepAnsByStepCode($stepCodeArr, $writingID, $studentID){
		global $intranet_db;

		if(!is_array($stepCodeArr)){
			//convert the $stepCode to Array if it is not an array
			$stepCodeArr = array($stepCodeArr);
		}

		$codeStrSQL = implode('\',\'',$stepCodeArr);

		$sql= 'select
						step.CODE as "CODE",
						step.SEQUENCE as "SEQUENCE",
						h.ANSWER as "ANSWER",
						step.STEP_ID as "STEPID",
						h.STEP_HANDIN_CODE as "StepHandinCode",
						h.version as "VERSION"
				from
						'.$intranet_db.'.W2_WRITING as w
						inner join '.$intranet_db.'.W2_STEP as step on step.WRITING_ID = w.Writing_id
						inner join '.$intranet_db.'.W2_STEP_HANDIN as h on h.STEP_ID = step.STEP_ID
				where
						h.USER_ID = \''.$studentID.'\'
						and w.WRITING_ID = \''.$writingID.'\'
						and h.STEP_HANDIN_CODE in(\''.$codeStrSQL.'\')
				';
//	debug_r($sql);
		//$objDB  = new libDB();

		$result = $this->objDb->returnResultSet($sql);

		return $result;
	}

	/**
	* Return Step info array
	*
	* @param : Array, $writingIdAry, writing id
	* @return : Array, step info array
	*/
	public function getWritingStep($writingIdAry) {
		global $intranet_db;

		if ($writingIdAry) {
			$condsWritingIdAry = " and w.WRITING_ID in ('".implode("','", (array)$writingIdAry)."') ";
		}

		$sql = 'select s.STEP_ID, s.TITLE_ENG as `STEP_TITLE_ENG` ,s.TITLE_CHI as `STEP_TITLE_CHI`, s.CODE as `STEP_CODE`, s.SEQUENCE as `STEP_SEQUENCE`, w.WRITING_ID ,s.REQUIRE_APPROVAL as `STEP_REQUIRE_APPROVAL` from '.$intranet_db.'.W2_WRITING as w inner join '.$intranet_db.'.W2_STEP as s on w.WRITING_ID = s.WRITING_ID where 1 '.$condsWritingIdAry.' order by s.SEQUENCE asc';
		return $this->objDb->returnResultSet($sql);
	}

	public function getThickboxHtml($header, $content, $button, $mainDivExtraPara='') {
		$html = $this->getThickboxHtmlTemplate();

		$html = str_replace('{{{mainDivExtraPara}}}', $mainDivExtraPara, $html);
		$html = str_replace('{{{header}}}', $header, $html);
		$html = str_replace('{{{content}}}', $content, $html);
		$html = str_replace('{{{button}}}', $button, $html);

		return $html;
	}

	private function getThickboxHtmlTemplate() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		return <<<html
<div id="thickbox_content_outer_div" class="content_pop_board_190" {{{mainDivExtraPara}}}>
  {{{header}}}
  	<div style="position: absolute; margin:0 0 0 0; display:none;" class="SystemReturnMessage" id="system_message_box">
		<div class="msg_board_left">
	  	<div style="text-align:center;" id="message_body" class="msg_board_right">
	  	<a onclick="$('div.SystemReturnMessage').css('visibility','hidden'); return false;" href="#">[Clear]</a>
	  	</div>
		</div>
	</div>
	<div class="content_pop_board_write_190" id="thickbox_content_div">
		<form id="thickboxForm" name="thickboxForm" enctype="multipart/form-data">
    		{{{content}}}
    	</form>
    </div>
  	<div class="edit_bottom" id="edit_content_pop">
		{{{button}}}
  	</div>
</div>
html;
	}

	public function getTeacherCommentButton() {
		global $Lang;

		return <<<html
<a onclick="MM_showHideLayers('layer_comment01','','show'); MM_showHideLayers('layer_peer_comment01','','hide');" class="btn_comment_tea" href="#">
	<span>{$Lang['W2']['teachersComment']}</span>
</a>
html;
	}

	public function getTeacherCommentLayer($isEditMode, $commentId) {
		global $Lang;

//		if(is_numeric($commentId)){
//			$objStudentComment = new libStepStudentComment($commentId);
//			$comment = $objStudentComment->getContent();
//			$commentLastUpdate = $objStudentComment->getDateModified();
//			$commentLastUpdateBy = $objStudentComment->getModifiedBy();
//			$lastModifiedDisplay = Get_Last_Modified_Remark($commentLastUpdate, '', $commentLastUpdateBy);
//		}

		$content = $this->getTeacherCommentLayerContent($isEditMode, $commentId);

		$button = '';
		if ($isEditMode) {
			$onclick = 'saveComment(\''.$Lang['W2']['returnMsgArr']['teacherCommentUpdateSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['teacherCommentUpdateFailed'].'\');';
			$button .= '<input type="button" onclick="'.$onclick.'" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'.$Lang['Btn']['Save'].'">';
		}
		$button .= '<input type="button" onclick="MM_showHideLayers(\'layer_comment01\',\'\',\'hide\')" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'.$Lang['Btn']['Close'].'">';

		$html = $this->getTeacherCommentLayerTemplate();
		$html = str_replace('{{{contentHtml}}}', $content, $html);
		$html = str_replace('{{{buttonHtml}}}', $button, $html);

		return $html;
	}
	public function getTeacherComment($stepID,$studentID,$teacherID='',$version=1){
		global $intranet_db;
		$cond = (!empty($teacherID))?" AND c.INPUT_BY = '".$teacherID."'":"";
		$sql = "
				SELECT
					".getNameFieldByLang2("u.")." as TEACHER,
					c.COMMENT_ID,
					c.STICKER,
					c.CONTENT,
					c.COMMENT_STATUS,
					c.INPUT_BY
				FROM
					".$intranet_db.".W2_STEP_STUDENT_COMMENT c
				INNER JOIN
					".$intranet_db.".INTRANET_USER u on c.INPUT_BY = u.UserID
				WHERE
					c.USER_ID = '".$studentID."'
				AND
					c.STEP_ID = '".$stepID."'
				AND
					c.VERSION = '".$version."'
				".$cond."
		";
		return $this->objDb->returnArray($sql);

	}
	public function getPeerComment($writingID,$studentID,$markerID='',$version=1,$includeNullComment=false){
		global $intranet_db;
		$cond = (!empty($markerID))?" AND d.MARKER_USER_ID = '".$markerID."'":"";
		$cond .= (!$includeNullComment)?" AND d.COMMENT IS NOT NULL":"";
		$cond .= (!empty($version))?" AND d.VERSION = '".$version."'":"";
		$sql = "
				SELECT
					".getNameFieldByLang2("u.")." as PEER,
					d.MARK,
					d.STICKER,
					d.COMMENT
				FROM
					".$intranet_db.".W2_PEER_MARKING_DATA d
				INNER JOIN
					".$intranet_db.".INTRANET_USER u on d.MARKER_USER_ID = u.UserID
				WHERE
					d.WRITING_ID = '".$writingID."'
				AND
					d.TARGET_USER_ID = '".$studentID."'
				".$cond."
				ORDER BY
					d.DATE_MODIFIED DESC
		";
		return $this->objDb->returnArray($sql);

	}
	public function getWritingLastStepId($writingId){
		$stepInfoAry = $this->getWritingStep(array($writingId));
		return $stepInfoAry[count((array)$stepInfoAry) - 1]['STEP_ID'];
	}

	public function getStudentWritingRecordByWritingId($contentCode,$writingId,$studentIdAry=array()) {
		global $w2_cfg,$intranet_db,$Lang;
		$cond = "";
		if(!empty($studentIdAry)){
			$cond .= "AND ws.USER_ID IN ('".implode("','",$studentIdAry)."')";
		}
		### Get Last Step of each writings
		$lastStepId = $this->getWritingLastStepId($writingId);
		$sql = "SELECT
						 iu.ClassName, iu.ClassNumber, ".getNameFieldByLang2("iu.")." StudentName, ws.USER_ID, sh.MARK, sh.ANSWER, sh.VERSION
					FROM
						".$intranet_db.".W2_WRITING_STUDENT ws
					INNER JOIN ".$intranet_db.".W2_STEP s ON ws.WRITING_ID = s.WRITING_ID
					INNER JOIN ".$intranet_db.".W2_STEP_HANDIN sh ON s.STEP_ID = sh.STEP_ID and ws.USER_ID = sh.USER_ID
					INNER JOIN ".$intranet_db.".INTRANET_USER iu ON ws.USER_ID = iu.UserID
				WHERE
					sh.STEP_ID = '".$lastStepId."'
				AND
					ws.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"]."'
				AND
					ws.HANDIN_SUBMIT_STATUS = '".$w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"]."'
				$cond
				ORDER BY iu.ClassName, iu.ClassNumber
			";



		$writingRecord = $this->objDb->returnArray($sql);
		$studentWritingRecord = BuildMultiKeyAssoc($writingRecord,"USER_ID");
		$studentIdAry = array_keys($studentWritingRecord);

		$peerMarkingRecord = $this->getStudentOverallPeerMarkingRecord($writingId,$studentIdAry);
		$HandinHistoryRecord = $this->getStepHandinHisotryRecord($lastStepId,$studentIdAry);

		###Peer Marking Record
		$studentPeerMarkingAry = array();
		foreach((array)$peerMarkingRecord as  $_peerMarkingAry){
			$_studentId = $_peerMarkingAry['TARGET_USER_ID'];
			$_version = $_peerMarkingAry['VERSION'];
			$studentPeerMarkingAry[$_studentId][$_version] = $_peerMarkingAry['PEER_MARK'];
		};
		###Handin History Record + Peer Marking Record
		$studentHandinMarkAry = array();
		$studentHandinAnswerAry = array();
		foreach((array)$HandinHistoryRecord as  $_handinHistoryAry){
			$_studentId = $_handinHistoryAry['USER_ID'];
			$_version = $_handinHistoryAry['VERSION'];
			$_mark = $_handinHistoryAry['MARK']?$_handinHistoryAry['MARK']:0;
			$_peerMark = $studentPeerMarkingAry[$_studentId][$_version]?$studentPeerMarkingAry[$_studentId][$_version]:0;
			$_mark = $this->returnStudentOverallWritingMark($writingId,$_mark,$_peerMark);
			$studentHandinMarkAry[$_studentId][$_version] = $_mark;
			$studentHandinAnswerAry[$_studentId][$_version] = $_handinHistoryAry['ANSWER'];
		};
		$studentWritingReportAry = array();
		foreach((array)$studentWritingRecord as  $_studentId => $_writingRecordAry){
			$_className = $_writingRecordAry['ClassName'];
			$_classNumber = $_writingRecordAry['ClassNumber'];
			$_studentName = $_writingRecordAry['StudentName'];
			$_version = $_writingRecordAry['VERSION'];
			$_mark = $_writingRecordAry['MARK']?$_writingRecordAry['MARK']:0;
			$_answer = $_writingRecordAry['ANSWER']?$_writingRecordAry['ANSWER']:"";
			$_peerMark = $studentPeerMarkingAry[$_studentId][$_version]?$studentPeerMarkingAry[$_studentId][$_version]:0;
			$studentHandinMarkAry[$_studentId][$_version] = $this->returnStudentOverallWritingMark($writingId,$_mark,$_peerMark);
			$studentHandinAnswerAry[$_studentId][$_version] = $_answer;
			$studentWritingReportAry[$_studentId]['CLASS'] = $_className;
			$studentWritingReportAry[$_studentId]['CLASS_NO'] = $_classNumber;
			$studentWritingReportAry[$_studentId]['STUDENT_NAME'] = $_studentName;
			for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){

				$studentWritingReportAry[$_studentId]['DRAFT'.$i] = $studentHandinMarkAry[$_studentId][$i]?$studentHandinMarkAry[$_studentId][$i]:'';
				if($studentWritingReportAry[$_studentId]['DRAFT'.$i]){
					$studentWritingReportAry[$_studentId]['DRAFT'.$i.'LINK'] = "<a href=\"/home/eLearning/w2/index.php?mod=report&task=viewStudentWriting&r_contentCode=".$contentCode."&r_writingId=".$writingId."&student_id=".$_studentId."&r_version=".$i."\" target=\"_BLANK\">".$studentWritingReportAry[$_studentId]['DRAFT'.$i]."</a>";
				}else{
					$studentWritingReportAry[$_studentId]['DRAFT'.$i.'LINK'] = "-";
				}
				$studentWritingReportAry[$_studentId]['DRAFT'.$i] = $studentWritingReportAry[$_studentId]['DRAFT'.$i]?$studentWritingReportAry[$_studentId]['DRAFT'.$i]:'-';
				$studentWritingReportAry[$_studentId]['ANSWER'.$i] = $studentHandinAnswerAry[$_studentId][$i]?$studentHandinAnswerAry[$_studentId][$i]:'-';
			}
			$studentWritingReportAry[$_studentId]['HIGHEST_MARK'] = max($studentHandinMarkAry[$_studentId]);
			$studentWritingReportAry[$_studentId]['TOTAL_ATTEMPT'] = $_version;
			$studentWritingReportAry[$_studentId]['EXPORT_BTN'] = '<div class="Content_tool Content_tool_report print_hide"><a class="export" href="/home/eLearning/w2/index.php?mod=report&task=exportStudentWriting&r_contentCode='.$contentCode.'&r_writingId='.$writingId.'&student_id='.$_studentId.'">'.$Lang['Btn']['Export'].'</a></div>';
		};
		return $studentWritingReportAry;

	}
	function returnStudentOverallWritingMark($writingId,$teacherMark,$peerMark){
		global $w2_cfg;
		$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $writingId);
		$allow_peer_marking = $objWriting->getAllowPeerMarking();
		$teacher_weight = $objWriting->getTeacherWeight();
		$peer_weight = $objWriting->getPeerWeight();
		if($allow_peer_marking){
			$mark = my_round(($teacherMark*$teacher_weight+$peerMark*$peer_weight)/100,0);
		}else{
			$mark = $teacherMark;
		}
		return $mark;

	}
	function getStudentOverallPeerMarkingRecord($writingId,$studentIdAry=array(),$version=1){
		$cond = "";
		if(!empty($studentIdAry)){
			$cond .= "AND TARGET_USER_ID IN ('".implode("','",$studentIdAry)."')";
		}
		$sql = "
				SELECT
					TARGET_USER_ID, VERSION, AVG(MARK) PEER_MARK
				FROM
					W2_PEER_MARKING_DATA
				WHERE
					WRITING_ID = '".$writingId."'
				AND
					MARK IS NOT NULL
				$cond
				GROUP BY
					TARGET_USER_ID,VERSION
				ORDER BY
					TARGET_USER_ID,VERSION
		";
		return $this->objDb->returnArray($sql);
	}
	public function getHandinOverallMark($stepID,$studentID,$writingID,$stepHandinCode,$version) {
		global $PATH_WRT_ROOT,$Lang,$w2_cfg;
		$studentStepHandinRecord = $this->getStudentStepHandinRecord($stepID,$studentID,$stepHandinCode);
		if($studentStepHandinRecord['VERSION']==$version){
			$teacher_mark = $studentStepHandinRecord['MARK'];
		}else{
			$lastHandinRecord = current($this->getStepHandinHisotryRecord($stepID,array($studentID),$version));
			$teacher_mark = $lastHandinRecord['MARK'];
		}

		$sql = "SELECT AVG(MARK) PEER_MARK FROM W2_PEER_MARKING_DATA WHERE WRITING_ID = '".$writingID."' AND TARGET_USER_ID = '".$studentID."' AND MARK IS NOT NULL AND VERSION = '".$version."'";
		$peer_mark = current($this->objDb->returnVector($sql));
		include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
		include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
		include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");

		$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $writingID);
		$allow_peer_marking = $objWriting->getAllowPeerMarking();
		$output = array();
		$output['fullmark'] = $objWriting->getFullMark();
		if($allow_peer_marking){
			if(($teacher_mark===NULL || $teacher_mark ==='') &&$peer_mark===NULL){
				$output['mark'] = '--';
			}else{
				$teacher_weight = $objWriting->getTeacherWeight();
				$peer_weight = $objWriting->getPeerWeight();

				$overall_mark = $teacher_mark*$teacher_weight+$peer_mark*$peer_weight;
				$output['mark'] = my_round($overall_mark/100,0);
			}
		}else{
			if($teacher_mark===NULL){
				$output['mark'] = '--';
			}else{
				$output['mark'] = $teacher_mark;
			}
		}
		return $output;


	}
	public function getHandinStickerLayerHTML($teacherCommentAry,$peerCommentAry) {
		global $Lang;

		$stickerAry = array();
		$stickerTotal = 0;
		for($i = 0,$i_max = count($teacherCommentAry);$i < $i_max ;$i++){
    		$_teacherStickerAry = explode(":",$teacherCommentAry[$i]['STICKER']);
    		for($j = 0,$j_max = count($_teacherStickerAry);$j < $j_max ;$j++){
    			$__stickerIdx = $_teacherStickerAry[$j];
    			if(!empty($__stickerIdx)){
	    			$stickerAry[$__stickerIdx]++;
	    			$stickerTotal++;
    			}
    		}
    	}
		for($i = 0,$i_max = count($peerCommentAry);$i < $i_max ;$i++){
    		$_peerStickerAry = explode(":",$peerCommentAry[$i]['STICKER']);
    		for($j = 0,$j_max = count($_peerStickerAry);$j < $j_max ;$j++){
    			$__stickerIdx = $_peerStickerAry[$j];
    			if(!empty($__stickerIdx)){
	    			$stickerAry[$__stickerIdx]++;
	    			$stickerTotal++;
    			}

    		}
    	}
    	$stickerCnt = count($stickerAry);
		$html = '<fieldset><legend>'.$Lang['W2']['stickers'].' ('.$stickerTotal.') </legend>';
	    $html .= $stickerCnt>3?'<a href="javascript:void(0);" class="btn_close" onclick="toggleStickerList();">&nbsp;</a>':'';
	    $html .= '<ul>';

	    foreach($stickerAry as $_stickerIdx => $_stickerCnt){
			$html .= '<li><div class="award_sticker"><span class="'.$_stickerIdx.'"><em>'.$_stickerCnt.'</em></span></div></li>';
	    }
	    $html .= ' </ul>';

	    $html .= $stickerCnt>3?'<a href="javascript:void(0);" class="btn_seemore" onclick="toggleStickerList();">'.$Lang['W2']['seeMore'].'</a>':'';
	    $html .= '</fieldset>';
		return $html;
	}
	public function getDisplayMarkingLayerHTML($stepID,$studentID,$writingID,$stepHandinCode,$version=1,$showOverallMark=true) {
		global $Lang;
		$teacherCommentAry = $this->getTeacherComment($stepID,$studentID,'',$version);
		$peerCommentAry = $this->getPeerComment($writingID,$studentID,'',$version,true);
		$markAry = $this->getHandinOverallMark($stepID,$studentID,$writingID,$stepHandinCode,$version);
		$overallMark = $showOverallMark?$markAry['mark']:'--';
		$fullmark = $markAry['fullmark'];
		$handinCommentHTML = $this->getHandinCommentLayerHTML($teacherCommentAry,$peerCommentAry);
		$stickerHTML = $this->getHandinStickerLayerHTML($teacherCommentAry,$peerCommentAry);


		$html = '<div id="item_sticker" class="sticker_collsape">';
        	$html .= '<div class="sticker_list">';
            	$html .= $stickerHTML;
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="item_mark">';
        	$html .= '<fieldset>';
        	$html .= '<legend>'.$Lang['W2']['marks'].'</legend>';
            $html .= '<span>'.$overallMark.'</span>/'.$fullmark.'</fieldset>';
        $html .= '</div>';
        $html .= '<div class="item_comment">';
        	$html .= $handinCommentHTML;
        $html .= '</div>';
        return $html;
	}
	public function getHandinCommentLayerHTML($teacherCommentAry,$peerCommentAry) {
		global $Lang;

		$teacherCommentCnt = sizeof($teacherCommentAry);
		$peerCommentCnt = sizeof($peerCommentAry);
		$commentTotal = 0;
		$html .= '<div id="comment_board">';
        	$html .= '<div class="comment_board_top_left"><div class="comment_board_top_right"><div class="comment_board_top_bg"></div></div></div>';
            $html .= '<div class="comment_board_left"><div class="comment_board_right"><div class="comment_board_bg">';
            	$html .= '<a href="javascript:void(0);" class="btn_close" title="'.$Lang['Btn']['Close'].'" onclick="$(\'div#comment_board\').hide();"> </a>';
                $html .= '<p class="spacer"></p>';
                $html .= '<div class="comment_list">';
                	$html .= '<ul>';
                	##Teacher Comment on top
                	for($i = 0,$i_max = count($teacherCommentAry);$i < $i_max ;$i++){
                		if(!empty($teacherCommentAry[$i]['CONTENT'])){
	                		$html .= '<li class="comment_teacher"><em>'.$teacherCommentAry[$i]['TEACHER'].'</em>'.nl2br($teacherCommentAry[$i]['CONTENT']);
	                       		$html .= '<p class="spacer"></p>';
	                        $html .= '</li>';
	                        $commentTotal++;
                		}
                	}
                	for($i = 0,$i_max = count($peerCommentAry);$i < $i_max ;$i++){
                		if(!empty($peerCommentAry[$i]['COMMENT'])){
	                		$html .= '<li><em>'.$peerCommentAry[$i]['PEER'].'</em>'.nl2br($peerCommentAry[$i]['COMMENT']);
	                       		$html .= '<p class="spacer"></p>';
	                        $html .= '</li>';
	                        $commentTotal++;
                		}
                	}
                        $html .= '<p class="spacer"></p>';
                    $html .= '</ul>';
                $html .= '</div>';
            $html .= '</div></div></div>';
            $html .= '<div class="comment_board_bottom_left"><div class="comment_board_bottom_right"><div class="comment_board_bottom_bg"></div></div></div>';
        $html .= '</div>';
	    if($commentTotal>0){
            $html = '<a href="javascript:void(0);" class="btn_comment" onclick="$(\'div#comment_board\').show();">'.$Lang['W2']['comments'].'('.$commentTotal.')</a>'.$html;
		}else{
			$html .= '<a href="javascript:void(0);" class="btn_comment">'.$Lang['W2']['comments'].'('.$commentTotal.')</a>';
		}
        return $html;
	}

	public function getTeacherCommentLayerContent($isEditMode, $commentId) {
		global $Lang;

		if(is_numeric($commentId)){
			$objStudentComment = new libStepStudentComment($commentId);
			$comment = $objStudentComment->getContent();
			$commentLastUpdate = $objStudentComment->getDateModified();
			$commentLastUpdateBy = $objStudentComment->getModifiedBy();
			$lastModifiedDisplay = Get_Last_Modified_Remark($commentLastUpdate, '', $commentLastUpdateBy);
		}

		$content = '';
		if ($isEditMode) {
			$content .= '<textarea style="width: 95%" class="textbox" rows="7" name="r_comment[0]">'.$comment.'</textarea>';


//			$onclick = 'saveComment(\''.$Lang['W2']['returnMsgArr']['teacherCommentUpdateSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['teacherCommentUpdateFailed'].'\');';
//			$button .= '<input type="button" onclick="'.$onclick.'" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'.$Lang['Btn']['Save'].'">';
		}
		else{
			$content .= nl2br($comment);
		}

		$html = $this->getTeacherCommentLayerContentTemplate();
		$html = str_replace('{{{content}}}', $content, $html);
		$html = str_replace('{{{lastModifiedInfo}}}', $lastModifiedDisplay, $html);

		return $html;
	}
	public function getDraftName($version){
		global $intranet_session_language,$Lang;
		$no = ($intranet_session_language=='en')?Get_Sequence_Number($version):$version;
		return ($intranet_session_language=='en')?$no.' '.$Lang['W2']['draft']:str_replace('<-version->',$no,$Lang['W2']['draft']);
	}
	public function getStudentStepHandinHistorySelection($currentVersion,$selectedValue='',$showCorrectionArea=false){
		$style = $currentVersion<=1&&$showCorrectionArea?' style="visibility:hidden;"':'';
		$html = '<select class="select_draft"'.$style.'>';
		for($i=1;$i<=$currentVersion;$i++){
			$draft = $this->getDraftName($i);
			$selected = ($i==$selectedValue)?' selected':'';
    		$html .= '<option value="'.$i.'"'.$selected.'>';
    		$html .= $draft;
    		$html .= '</option>';
    	}
    	$html .= '</select>';
    	return $html;
	}
	public function getStepHandinHisotryRecord($stepID,$studentIdAry=array(),$version=''){
		global $intranet_db;
		$cond = !empty($version)?" AND VERSION='".$version."'":"";
		if ($studentIdAry != '') {
			$cond .= " And USER_ID In ('".implode("','", (array)$studentIdAry)."') ";
		}
		$sql = "
			SELECT
				STEP_ID,
				USER_ID,
				ANSWER,
				ANSWER_MARKED,
				MARK,
				VERSION,
				DATE_INPUT,
				DATE_MODIFIED
			FROM
				".$intranet_db.".W2_STEP_HANDIN_HISTORY
			WHERE
					STEP_ID = '".$stepID."'
			".$cond."

		";
		return $this->objDb->returnArray($sql);
	}
	public function createStepHandinHisoryRecord($stepID,$studentID,$stepHandinCode,$isUpdateHandin=true){
		global $intranet_db,$w2_cfg;

		$stepHandinRecord = $this->getStudentStepHandinRecord($stepID,$studentID,$stepHandinCode);
		$sql = "SELECT APPROVAL_STATUS FROM ".$intranet_db.".W2_STEP_STUDENT WHERE STEP_ID = '".$stepID."' AND USER_ID = '".$studentID."'";
		$approvalStatus =  current($this->objDb->returnVector($sql));

		if($approvalStatus!=$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]){
			$ans_marked = $this->objDb->Get_Safe_Sql_Query(trim(stripslashes($stepHandinRecord['ANSWER_MARKED'])));
			$ans = $this->objDb->Get_Safe_Sql_Query(trim(stripslashes($stepHandinRecord['ANSWER'])));
			$sql = "
				INSERT INTO ".$intranet_db.".W2_STEP_HANDIN_HISTORY
				(STEP_ID,USER_ID,STEP_HANDIN_CODE,ANSWER,ANSWER_MARKED,MARK,VERSION,DATE_INPUT,DATE_MODIFIED)
				VALUES
				(
					'".$stepHandinRecord['STEP_ID']."','".$stepHandinRecord['USER_ID']."','".$stepHandinCode."','".$ans."',
					'".$ans_marked."','".$stepHandinRecord['MARK']."','".$stepHandinRecord['VERSION']."',NOW(),NOW()
				)
			";

			$result = $this->objDb->db_db_query($sql);
			if($result&&$isUpdateHandin){
				$version = $stepHandinRecord['VERSION']+1;
				$answer = $stepHandinRecord['ANSWER'];
				$answerMarked = "";		// set to null when set to redo , do not keep $stepHandinRecord['ANSWER_MARKED']
				return $this->updateStudentStepHandinRecord($stepID,$studentID,$stepHandinCode,array("VERSION"=>$version,"ANSWER"=>$answer,"ANSWER_MARKED"=>"","MARK"=>""));
			}else{
				return $result;
			}
		}
	}
	public function getStudentStepHandinLatestVersion($stepID,$studentID){
		global $intranet_db;
		$sql = "
				SELECT
					MAX(VERSION)
				FROM
					".$intranet_db.".W2_STEP_HANDIN
				WHERE
					STEP_ID = '".$stepID."'
				AND
					USER_ID = '".$studentID."'
		";
		return current($this->objDb->returnVector($sql));
	}
	public function getStudentStepHandinRecord($stepID,$studentID,$stepHandinCode){
		global $intranet_db;
		$sql = "
				SELECT
					STEP_ID,
					USER_ID,
					ANSWER,
					ANSWER_MARKED,
					MARK,
					VERSION
				FROM
					".$intranet_db.".W2_STEP_HANDIN
				WHERE
					STEP_ID = '".$stepID."'
				AND
					USER_ID = '".$studentID."'
				AND
					STEP_HANDIN_CODE = '".$stepHandinCode."'
		";
		return current($this->objDb->returnArray($sql));
	}
	public function updateStudentStepHandinRecord($stepID,$studentID,$stepHandinCode,$Data){
		global $intranet_db;
		extract($Data);
		$sql = "
				SELECT
					COUNT(*)
				FROM
					".$intranet_db.".W2_STEP_HANDIN
				WHERE
					STEP_ID = '".$stepID."'
				AND
					USER_ID = '".$studentID."'
				AND
					STEP_HANDIN_CODE = '".$stepHandinCode."'
		";

		$result = current($this->objDb->returnVector($sql));
		if($result>0){
			$sql = "
				UPDATE
					".$intranet_db.".W2_STEP_HANDIN
				SET ";
			foreach($Data as $_key => $_value){
				if($_key=='ANSWER'||$_key=='ANSWER_MARKED'){
					$_value = $this->objDb->Get_Safe_Sql_Query(trim(stripslashes($_value)));
				}
					$sql .= $_key." = '".$_value."',";
			}
			$sql .= "
					DATE_MODIFIED = NOW()
				WHERE
					STEP_ID = '".$stepID."'
				AND
					USER_ID = '".$studentID."'
				AND
					STEP_HANDIN_CODE = '".$stepHandinCode."'
			";

			return $this->objDb->db_db_query($sql);
		}else{
			return false;
		}
	}
	private function getTeacherCommentLayerTemplate() {
		return <<<html
<!-- Teacher Comment layer start -->
<div id="layer_comment01" class="layer_comment">
	<div class="top">
		<div></div>
	</div>
	<div class="mid">
		<div class="right">
			<div id="layer_comment01_content" class="content">
				{{{contentHtml}}}
			</div>
			<!-- button start-->
			<div class="layer_comment_btn">
				{{{buttonHtml}}}
			</div>
			<!-- button end-->
		</div>
	</div>
	<div style="clear: both"></div>
	<div class="bottom">
		<div></div>
	</div>
</div>
<!-- Teacher Comment layer end -->
html;
	}

	private function getTeacherCommentLayerContentTemplate() {
		return <<<html
{{{content}}}
<br><span class="date">{{{lastModifiedInfo}}}</span>
html;
	}

	public function updateStepStudentCommentLinkage($stepId, $studentId, $commentId){
		global $intranet_db;
		$W2_STEP_STUDENT = $intranet_db.'.W2_STEP_STUDENT';
		$sql = "Update
						$W2_STEP_STUDENT
				Set
						COMMENT_ID = '".$commentId."'
				Where
						STEP_ID = '".$stepId."'
						And USER_ID = '".$studentId."'
				";
		return $this->objDb->db_db_query($sql);
	}

	public function getIconCommentHtml($commentStatus='') {
		global $w2_cfg;

		$iconClass = '';
		switch ($commentStatus) {
			case $w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["teacherCommented"]:
				$iconClass = 'commented';
				break;
			case $w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["studentHasRead"]:
			default:

				$iconClass = 'comment';
				break;
		}

		return '<span class="'.$iconClass.'"></span>';
	}

	public function getIconPeerCommentHtml($floatLeft=false) {
		if ($floatLeft) {
			$floatLeftStyle = 'float:left;';
		}

		return '<span class="peer_comment" style="'.$floatLeftStyle.'"></span>';
	}


   /**
	* create eclass account for a give userid , and class room id
	* @owner : Fai (20111126)
	* @param : INTEGER $parUserId , user id that in INTRANET_USER and need to created account in eclass
	* @param : INTEGER $parClassRoomID, class room id that user need to created
	* @return : BOOLEAN true false
	*
	*/

	public function createEclassUserAccountForWriting20($parUserId,$parClassRoomID){
		global $eclass_db,$intranet_db;
		$objDb = new libdb();

		//check account in eclass
		 $sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, Title, EnglishName, ChineseName , NickName, TitleEnglish, TitleChinese, HomeTelNo, Address, FaxNo,DateOfBirth    FROM ".$intranet_db.".INTRANET_USER WHERE UserID = '{$parUserId}'";
		$row = $objDb->returnResultSet($sql);

		if(is_array($row) && count($row) == 1){
			$row = current($row);
		}else{
			//suppose there is only one and one user in the INTRANET_USER;
	//		echo 'Error in finding user';
			return false;
		}

		$UserEmail = $row['UserEmail'];
		$result = $this->findUserDetailsInW2ClassRoom($parClassRoomID, $parUserId,  $UserEmail);

		if(is_array($result) && count($result) == 1){
			//user already exist in the eclass, skip create eclass account
	//		$result = current($result);
	//		$returnID  = $result['user_course_id'];
			return true;
		}elseif(is_array($result) && count($result) > 1)
		{
			//abnormal case , there should be one user_course_id for a user in a course_id
			return false;
		}else{

			//case for count($result) == 0 , create user account in eclass

			//copy from /home/web/eclass40/intranetIP25/home/eLearning/eclass/organize/user/import_update.php
			$lo = new libeclass($parClassRoomID);

			$MemberType = 'S';
			$User_ID = $row['UserID'];
			$UserEmail = $row['UserEmail'];
			$UserPassword = $row['UserPassword'];
			$ClassNumber = $row['ClassNumber'];
			$FirstName = $row['FirstName'];
			$LastName = $row['LastName'];
			$ClassName = $row['ClassName'];
			$Title = $row['Title'];
			$EngName = $row['EnglishName'];
			$ChiName = $row['ChineseName'];
			$NickName = $row['NickName'];
			$titleEng = $row['TitleEnglish'];
			$titleChi = $row['TitleChinese'];

			$HomeTelNo = $row['HomeTelNo'];
			$Address = $row['Address'];
			$FaxNo = $row['FaxNo'];
			$DateOfBirth = $row['DateOfBirth'];
			$eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
			/*
			if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
				$eclassClassNumber = $ClassNumber;
			else
				$eclassClassNumber = $ClassName ." - ".$ClassNumber;
			*/
			$lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, $MemberType, $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info, "", $User_ID,$titleEng,$titleChi);

			return true;
		}
	}

	/**
	* GET user course id by a give intraneIP userid and class room id
	* @owner : Fai (20111126)
	* @param : INTEGER $parUserId , user id that in INTRANET_USER
	* @param : INTEGER $parClassRoomID
	* @return : INTEGER $returnUserCourseId User course ID
	*
	*/

	function getUserCourseId($parIpUserId,$classRoomId){
		global $eclass_db;

		$objDb = new libdb();

		$returnUserCourseId = 0;

		$sql = 'select user_course_id from '.$eclass_db.'.user_course where intranet_user_id = \''.$parIpUserId.'\' and course_id = \''.$classRoomId."'";

		$result = $objDb->returnResultSet($sql);

		if(is_array($result) && count($result) == 1){
			$result = current($result);
			$returnUserCourseId = $result['user_course_id'];
		}

		return $returnUserCourseId;

	}

	/**
	* GET user id in eclass by a give intraneIP userid and class room id  , but it should be combine with f:getUserCourseId
	* @owner : Fai (20120203)
	* @param : INTEGER $parUserId , user id that in INTRANET_USER
	* @param : INTEGER $parClassRoomID
	* @return : INTEGER $returnEclassUserId User course ID
	*
	*/
	function getEclassUserId($parIpUserId,$classRoomId){
		global $eclass_db;

		$objDb = new libdb();

		$returnEclassUserId = 0;

		$sql = 'select user_id from '.$eclass_db.'.user_course where intranet_user_id = '.$parIpUserId.' and course_id = \''.$classRoomId."'";

		$result = $objDb->returnResultSet($sql);

		if(is_array($result) && count($result) == 1){
			$result = current($result);
			$returnEclassUserId = $result['user_id'];
		}

		return $returnEclassUserId;

	}


	/**
	* GET the view content for editor (mainly convert the eclass image path)
	* @owner : Ivan (20111126)
	* @param : STRING $content original content from db
	* @return : STRING $content content with changed image path of eclass images
	*/
	public function getEditorViewContent($content) {
		global $eclass40_httppath;

		$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
		$content = str_replace('="/eclass40/', '="'.$HTTP_SSL.$eclass40_httppath, $content);
		return $content;
	}

	/**
	* GET the save content for editor (mainly convert the eclass image path)
	* @owner : Ivan (20111126)
	* @param : STRING $content content with changed image path of eclass images
	* @return : STRING $content content to be saved in db
	*/
	public function getEditorSaveContent($content) {
		global $eclass40_httppath;

		$content = str_replace('="http://'.$eclass40_httppath, '="/eclass40/', $content);
		$content = str_replace('="https://'.$eclass40_httppath, '="/eclass40/', $content);
		return $content;
	}

	/**
	* GET the text display for different status
	* @owner : Ivan (20111128)
	* @param : STRING $code the status constant defined in the config file
	* @return : STRING $text the readable status text to be displayed for the users
	*/
	public function getStatusTextByCode($code) {
		global $Lang;

		switch ($code) {
			case W2_STATUS_PUBLIC:
				$text = $Lang['General']['Public'];
				break;
			case W2_STATUS_PRIVATE:
				$text = $Lang['General']['Private'];
				break;
			case W2_STATUS_SUBMITTED:
				$text = $Lang['W2']['submitted'];
				break;
			default:
				$text = $Lang['General']['EmptySymbol'];
				break;
		}

		return $text;
	}

	/**
	* GET the multiple selection list with the student in the writing
	* @owner : Ivan (20111128)
	* @param : STRING $id, id of the selection
	* @param : STRING $name, name of the selection
	* @param : INT $writingId, target writingId
	* @param : INT $selected, default selected options
	* @param : STRING $onchange, onchange js function of the selection
	* @return : STRING, the html of the selection
	*/
	public function getWritingStudentSelection($id, $name, $writingId, $selected, $onchange='') {
		$objWriting = new libWriting($writingId);
		$studentInfoAry = $objWriting->findStudentInWriting();
		$studentInfoAssoAry = BuildMultiKeyAssoc($studentInfoAry, 'USER_ID', 'USER_NAME', $SingleValue=1);

		$onchangeTag = '';
		if ($onchange != "") {
			$onchangeTag = ' onchange="'.$onchange.'" ';
		}
		$selectionTags = ' id="'.$id.'" name="'.$name.'" multiple="true" size="10" '.$onchangeTag;

		return getSelectByAssoArray($studentInfoAssoAry, $selectionTags, $selected, $all=0, $noFirst=0);
	}

	/**
	* GET the float layer html
	* @owner : Ivan (20111130)
	* @param : STRING $content, the content html of the layer
	* @return : STRING $html, the html of the layer
	*/
	public function getFloatLayerHtml($content='') {
		$html = $this->getFloatLayerTemplate();
		$html = str_replace('{{{contentHtml}}}', $content, $html);

		return $html;
	}

	/**
	* GET the float layer template html
	* @owner : Ivan (20111130)
	* @return : STRING $html, the template html of the layer
	*/
	private function getFloatLayerTemplate() {
		return <<<html
<div id="layer_ex_brief" class="layer_ex_brief">
	<div class="top"><div></div></div>
	<div class="mid">
		<div class="right">
			<div id="layer_ex_brief_content_div" class="content">{{{contentHtml}}}</div>
		</div>
	</div>
	<div style="clear:both"></div>
	<div class="bottom"><div></div></div>
</div>
html;
	}

	/**
	* GET if the current login user is the admin of Writing 2.0
	* @owner : Ivan (20111205)
	* @return : BOOLEAN, true if the user is Writing 2.0 admin
	*/
	public function isAdminUser() {
		return ($_SESSION['SSV_USER_ACCESS']['eLearning-W2'])? true : false;
	}


	public function getWritingAccessStatusSelection($id, $name, $selected='', $onchange='') {
		global $Lang, $w2_cfg;

		$selectAry = array();
		$selectAry[$w2_cfg["writingAccessStatus"]["allWriting"]] = $Lang['W2']['allWriting'];
		$selectAry[$w2_cfg["writingAccessStatus"]["myWriting"]] = $Lang['W2']['myWriting'];

		$onchangeTag = "";
		if ($onchange != "") {
			$onchangeTag = ' onchange="'.$onchange.'" ';
		}

		$selectionTags = ' id="'.$id.'" name="'.$name.'" style="width:100px;" '.$onchangeTag;

		return  getSelectByAssoArray($selectAry, $selectionTags, $selected, $isAll=false, $noFirst=true);
	}

	/**
	* FIND THE USER DETAILS WITH A GIVING IP.USERID IN W2 CLASSROOM
	* @owner : FAI (20111207)
	* @param : INTEGER $parClassRoomID , THE CLASS ROOM ID THAT NEED TO CHECK , SUPPOSE IS W2 CLASSROOM ID
	* @param : INTEGER $parUserId  , IP.USERID THAT NEED TO CHECK
	* @param : (OPTIONAL) String $parUserEmail , IP.USEREMAIL THAT NEED TO CHECK
	* @return : ARRAY OF RESULT SET , SUPPOSE RETURN ONE ROW OR EMPTY ROW. A ABNORMAL CASE WITH RETURN MULTIPLE ROW
	*/
	public function findUserDetailsInW2ClassRoom($parClassRoomID , $parUserId  , $parUserEmail = ''){
			global $eclass_db;
			$objDb = new libdb();
			$query = '';

			if(trim($parUserEmail) == ''){
				//do nothing
			}else{
				$query .= ' or UserEmail = \''.$parUserEmail.'\'';
			}

			$sql = 'select user_course_id as `USER_COURSE_ID` ,
			               user_id as `CLASSROOM_USER_ID`
			            from '.$eclass_db.'.user_course
						where
						course_id = \''.$parClassRoomID.'\' and
						(intranet_user_id =\''.$parUserId.'\'
						'.$query.'
						)';



			$result  = $objDb->returnResultSet($sql);
			return $result;
	}

	/**
	* return power concept link for a given studentid
	* @owner : FAI (20111214)
	* @param : INTEGER $studentID  , IP.USERID for the student
	* @param : OBJECT $objWriting  , a object of libWriting
	* @param : INTEGER $classRoomID , classroomid for W2
	* @param : STRING  $handinCode , individual step handin code , t: W2_STEP_HANDIN.STEP_HANDIN_CODE
	* @param : STRING  $allowAction , view or edit for the Power Concept
	* @param : INTEGER $withDefaultConceptMap, with default concept map or not  suppose 0 / 1
	* @param : INTEGER $displayMode, please refer to $w2_cfg['conceptMapDisplayMode']
	* @return : String $str, JS FUNCTION for the pop up of power concept link
	*/

	function findPowerConceptLinkForStudent($studentID, $objWriting, $classRoomID,$handinCode,$allowAction,$withDefaultConceptMap , $displayMode,$conceptType=W2_CONCEPT_TYPE_POWERCONCEPT){
		global $intranet_db,$w2_cfg,$eclass_url_root;

		$writingId = $objWriting->getWritingId();

		$eclassUserCourseLoginId = $this->getUserCourseId($studentID,$classRoomID);
		$eclassUserId = $this->getEclassUserId($studentID,$classRoomID);

		//ensure there is a value for $allowAction
		$allowAction = (trim($allowAction) == '') ?$w2_cfg['studentHandInAllowAction']['edit']:$allowAction;

		//check whether it is a teacher preview mode of the concept map
		$allowAction = ($displayMode == $w2_cfg['conceptMapDisplayMode']['teacherPreivewImage']) ? $w2_cfg["actionArr"]["preview"]: $allowAction;

		$objDb = new libdb();


		if($displayMode == $w2_cfg['conceptMapDisplayMode']['teacherPreivewImage'] || $allowAction == $w2_cfg['studentHandInAllowAction']['view']){
			//this is a preview mode from teacher , a view mode from student skip this checking
			//for student skip the checking in view mode , it is because there is a case that the student has not input any concept map for this step before . Then system should load the default content to the student

			//do nothing

		}else{

			if($eclassUserCourseLoginId == 0){
				echo 'error login eclass!<br/>';
				exit();
			}
		}
		$eclassTaskId = 0;
		$strCode = 'c2'; //hard code first

		$otherCode = array();
		$otherCode[] = $w2_cfg["contentArr"]["ls"]["contentCode"];
		$otherCode[] = $w2_cfg["contentArr"]["ls_eng"]["contentCode"];
		$otherCode[] = $w2_cfg["contentArr"]["sci"]["contentCode"];

		//if($objWriting->getContentCode() == $w2_cfg["contentArr"]["ls"]["contentCode"]){
		if(in_array($objWriting->getContentCode(),$otherCode)){
			$strCode = 'c3'; //hard code first
		}

		$_stepDetails = $this->findStepDetails($writingId ,$strCode);

		$eclassTaskId = $_stepDetails['ECLASS_TASK_ID'];
		$stepId = $_stepDetails['STEP_ID'];
		$stepCode = $_stepDetails['CODE'];

		$extraVariableAry = array();
		$extraVariableAry['r_sId'] = $studentID;
		$extraVariableAry['r_esId'] = $eclassUserId;   //r_esId  --> request variable , e --> eclass , sId--> student Id (the user id in eclass)
		$extraVariableAry['r_stepId'] = $stepId;
		$extraVariableAry['r_stepC'] = $stepCode;  //r_stepC --> request variable , step --> step Code
		$extraVariableAry['r_stepHC']= $handinCode;  //r_stepHC, r--> request variable , step --> step Code , H --> Handin, C --> Code
		$extraVariableAry['r_taskId'] = $eclassTaskId;
		$extraVariableAry['r_comeFrom'] = 'w2_';
		$extraVariableAry['r_mod'] = ($conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'pb':'pc'); //power concept
		$extraVariableAry['r_task'] = $allowAction;
		$extraVariableAry['r_dv'] = $withDefaultConceptMap;  //r_dv --> request variable , dv --> default value
		$extraVariableAry['r_cc'] = $objWriting->getContentCode();  //r_dv --> request variable , cc --> content code
		$extraVariableAry['r_sc'] = $objWriting->getSchemeCode();  //r_sc --> request variable , sc --> scheme code


		$extraVariableStr = '';

		foreach ($extraVariableAry as $key => $value){
			$extraVariableStr .= $key.'='.$value.'&';
		}
//		debug_r($extraVariableStr);
//		exit;

		$extraVariableStr = base64_encode($extraVariableStr);

		//w2_pc stand for writing 2.0 Power concept
		$jumpback = ($conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'w2_pb':'w2_pc');

		$goToURL = '/home/eLearning/login.php?uc_id='.$eclassUserCourseLoginId.'&jumpback='.$jumpback.'&r_var='.$extraVariableStr;

		if($displayMode == $w2_cfg['conceptMapDisplayMode']['teacherPreivewImage']){
			$_url = "$eclass_url_root/guest.php?course_id={$classRoomID}&jumpback={$jumpback}&r_var={$extraVariableStr}";
			$goToURL = $eclass_url_root.'/mode.php?url='.urlencode($_url);
		}



		$str = 'newWindow(\''.$goToURL.'\',8);';

//		$str = 'newWindow(\'/home/eLearning/login.php?uc_id='.$eclassUserCourseLoginId.'&jumpback=w2_pc&r_var='.$extraVariableStr.'\',8);'; //<-- redirect to eclass classroom directly

		return $str;
	}
	/**
	* FIND Related eclass task id in W2_STEP by a giving writingid and step code
	* @owner : FAI (20111208)
	* @param : INTEGER $writingId  , writing ID
	* @param : String $strStepCode , STEP CODE
	* @return : INTEGER eclasstaskid , the relate eclass task id
	*/

	public function findEclassTaskId($writingId ,$strStepCode){
		global $intranet_db;
		$objDb = new libdb();

		$sql = 'select ECLASS_TASK_ID from '.$intranet_db.'.W2_STEP where WRITING_ID = \''.$writingId.'\' and CODE = \''.$strStepCode.'\'';
		$result  = $objDb->returnResultSet($sql);
		if(is_array($result) && count($result) == 1){
			//suppose there is only one ECLASS_TASK_ID for a pair for writingid and code
			$result = current($result);
			$eclassTaskId = $result['ECLASS_TASK_ID'];
			return $eclassTaskId;
		}else{
			return false;
		}

	}

	/**
	* FIND details for a W2_STEP  by a given $writingId and step Code
	* @owner : FAI (20111228)
	* @param : INTEGER $writingId  , writing ID
	* @param : String $strStepCode , STEP CODE
	* @return : ARRAY of details
	*/

	public function findStepDetails($writingId ,$strStepCode){
		global $intranet_db;
		$objDb = new libdb();

		$sql = 'select STEP_ID ,WRITING_ID ,TITLE_ENG ,TITLE_CHI ,CODE ,SEQUENCE , ECLASS_TASK_ID  from '.$intranet_db.'.W2_STEP where WRITING_ID = '.$writingId.' and CODE = \''.$strStepCode.'\'';

		$result  = $objDb->returnResultSet($sql);
		if(is_array($result) && count($result) == 1){
			//suppose there is only one record for a pair for writingid and code
			$result = current($result);

			return $result;
		}else{
			return false;
		}

	}

	/**
	* Generate the html of the cancel button in student handin pages
	* @owner : 	Ivan (20111208)
	* @param :	String 	$parContentCode, content code of the handin
	* @param :	String 	$parAction, action of the current page (e.g. marking)
	* @param :	Int 	$parWritingId, writingId of the current writing
	* @param :	BOOLEAN	$parWritingCompleted, is writing completed or not
	* @param :	Int		$parHandinSubmitStatus, handin status of the teacher come-from-page
	* @param :	BOOLEAN	$parFromToBeMarked, is the page come from to be marked page
	* @return:	String, html of the cancel button
	*/
	public function htmlCancelStudentHandInButton($parContentCode, $parAction, $parWritingId, $parWritingCompleted, $parHandinSubmitStatus, $parFromToBeMarked) {
		global $linterface, $Lang, $w2_cfg;

		$paraAssoAry = array();
		$paraAssoAry['mod'] = 'writing';
		$paraAssoAry['r_contentCode'] = $parContentCode;
		$buttonText = $Lang['Btn']['Cancel'];
		if ($parAction == $w2_cfg["actionArr"]["marking"]) {
			$paraAssoAry['task'] = 'writingStudentMgmt';
			$paraAssoAry['r_writingId'] = $parWritingId;
			$paraAssoAry['r_handinSubmitStatus'] = $parHandinSubmitStatus;
			$paraAssoAry['r_fromToBeMarked'] = $parFromToBeMarked;
		}
		else if ($parAction == $w2_cfg["actionArr"]["peerMarking"]) {
			$paraAssoAry['mod'] = 'peerMarking';
			$paraAssoAry['task'] = 'listMarkWriting';
		}
		else if ($parAction == $w2_cfg["actionArr"]["sharing"]) {
			$paraAssoAry['task'] = 'listSharingWriting';
			$buttonText = $Lang['Btn']['Back'];
		}
		else {
			$paraAssoAry['task'] = ($parWritingCompleted)? 'completedWriting' : 'listWriting';
		}
		$para = $this->getUrlParaByAssoAry($paraAssoAry);

		return $linterface->GET_ACTION_BTN($buttonText, 'button', 'window.location=\'?'.$para.'\'', 'cancelBtn');
	}

	/**
	* Generate the html of the legend info
	* @owner : 	Ivan (20111209)
	* @param :	Array / String $parLegendArr, return the included legend only
	* @return:	String, html of the legend
	*/
	public function htmlLegendInfo($parLegendArr='') {
		global $Lang, $w2_cfg, $PATH_WRT_ROOT;

		$x = '';
		$x .= '<div class="remarksinfo">'."\r\n";
			$x .= $Lang['General']['Remark'].': '."\r\n";

			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["approvalIsNeeded"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_approval.gif">'.$Lang['W2']['approvalIsNeeded'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["waitingForApproval"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_wait.gif">'.$Lang['W2']['waitingForApproval'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["approved"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_approved.gif">'.$Lang['General']['Approved'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["needToRedo"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_redo.gif">'.$Lang['W2']['needToRedo'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["incomplete"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_step_incomplete.png">'.$Lang['W2']['incomplete'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["completed"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_step_complete.png">'.$Lang['W2']['completed'].' '."\r\n";
			}

			### 20140930 Hide Waiting for Comment Start
//			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["teacherNeedProvideComment"], (array)$parLegendArr)) {
//				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_comment.gif">'.$Lang['W2']['needProvideComment'].' '."\r\n";
//			}
//			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["studentWaitingTeacherComment"], (array)$parLegendArr)) {
//				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_comment.gif">'.$Lang['W2']['waitingTeacherComment'].' '."\r\n";
//			}
			### 20140930 Hide Waiting for Comment End
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["teacherAlreadyCommented"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_commented.gif">'.$Lang['W2']['alreadyCommented'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["studentNeedCheckComment"], (array)$parLegendArr)) {
				//$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_commented.gif">'.$Lang['W2']['needCheckComment'].' '."\r\n";
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_commented.gif">'.$Lang['W2']['teacherCommented'].' '."\r\n";
			}

			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["peerCommented"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_peer_comment.gif">'.$Lang['W2']['peerCommented'].' '."\r\n";
			}

			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["shareWriting"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_writing.gif">'.$Lang['W2']['shareWriting'].' '."\r\n";
			}
			if ($parLegendArr=='' || in_array($w2_cfg["legendArr"]["shareRubrics"], (array)$parLegendArr)) {
				$x .= '<img src="'.$PATH_WRT_ROOT.$w2_cfg["imagePath"].'icon_other_rubrics.gif">'.$Lang['W2']['shareRubrics'].' '."\r\n";
			}
		$x .= '</div>'."\r\n";

		return $x;
	}

	// to be deleted
	public function _getWritingWholeStep($writingId, $orderBy=1){
		global $intranet_db;

		$orderBYstr = ' ';

		switch($orderBy){
			case 1:
				$orderBYstr = ' step.SEQUENCE asc ';
				break;
			default :
				break;

		}

		$sql = 'select
					step.STEP_ID, step.CODE,step.SEQUENCE
				from
					W2_WRITING as w
				inner join
					W2_STEP as step on step.writing_id = w.writing_id
				where
					w.writing_id = \''.$writingId.'\' order by '.$orderBYstr;
	//debug_r($sql);
		return $this->objDb->returnResultSet($sql);
	}

	public function getAnsSelected($ansValue, $objValue) {
		if ($ansValue == $objValue) {
			return 'selected';
		}
		else {
			return '';
		}
	}

	public function getAnsChecked($ansValue, $objValue) {
		global $w2_cfg;

		$ansValueArr = explode($w2_cfg['delimiter1'], $ansValue);
		if (in_array($objValue, $ansValueArr)) {
			return 'checked';
		}
		else {
			return '';
		}
	}

	public function getAnsValue($ans) {
		return $ans;
	}

	public function findStepAnsLastModified($studentId, $stepId, $stepHandInCode){
		//DATE_format(DATE_MODIFIED, `%Y-%m-%d %k:%i:%S');
		global $intranet_db;
		$sql = 'select DATE_format(DATE_MODIFIED, \'%Y-%m-%d\') as `DATE_MODIFIED` from '.$intranet_db.'.W2_STEP_HANDIN where `STEP_ID` = \''.$stepId.'\' and `USER_ID` = \''.$studentId.'\' and `STEP_HANDIN_CODE` = \''.$stepHandInCode.'\'';

		$objDb = new libdb();
		$result = $objDb->returnResultSet($sql);

		if(count($result) == 1){
			//according to the DB structure UNIQUE KEY `STEP_USER_CODE` (`STEP_ID`,`USER_ID`,`STEP_HANDIN_CODE`)
			//suppose the return array must be size of 1
			$temp = current($result);

			$dateModify = $temp['DATE_MODIFIED'];

			return $dateModify;
		}else{
			return '';
		}
	}

	/**
	* Return STEP ANS by a giving step code that $stepCodeAnsArray is generate in f:getWritingStepAnsByStepCode
	*
	* @param : String $stepHandinCode
	* @param : Array  $stepCodeAnsArray , a set for student
	* @return : Student answer for the giving writing ID and step handin code and student id
	*/
	function getWritingStepAnsByStepCodeInParsedArray($stepHandinCode,$stepCodeAnsArray){
		$returnAnswer = '';
		if(is_array($stepCodeAnsArray) && count($stepCodeAnsArray)){
			for($i = 0,$i_max = count($stepCodeAnsArray);$i<$i_max;$i++){
				$_details = $stepCodeAnsArray[$i];
				$_stepHandinCode = $_details['StepHandinCode'];

				if($_stepHandinCode == $stepHandinCode){
					$returnAnswer = $_details['ANSWER'];
					break;
				}
			}
		}
		return $returnAnswer;
	}

	/**
	* Return Suggested Vocab For Student (suppose is sciense now)
	*
	* @param : String STEP CODE for storing student inputed suggest vocab
	* @return : Return HTML Code for the Suggested Vocab for Student
	*/
	function getSuggestedVocabForStudent($stepCode_SuggestedVocabForStudent){

		$objDb = new libdb();
		global $intranet_db;

		$sql = 'select distinct ANSWER as `ANSWER` from '.$intranet_db.'.W2_STEP_HANDIN where STEP_HANDIN_CODE  = \''.$stepCode_SuggestedVocabForStudent.'\'';

		$rs = $objDb->returnResultSet($sql);


		$allSuggestedVocabAry = array();
		for($i = 0, $i_max = count($rs); $i< $i_max;$i++){
			// 1) split the answer by "\n" or "\r"
			// 2) get the unique
			$_answerArray = array_unique(preg_split("[\n|\r]", $rs[$i]['ANSWER']));
			$allSuggestedVocabAry = array_unique(array_merge($_answerArray, $allSuggestedVocabAry));
		}

		//remove any empty element in 'allSuggestedVocabAry'
		$allSuggestedVocabAry = array_filter($allSuggestedVocabAry);

		//sort the element in allSuggestedVocabAry
		sort($allSuggestedVocabAry);
		$h_vocab = implode(", ", $allSuggestedVocabAry);


	$html = <<<HTML
			<span class="subtitle">Suggested by other students</span><br />{$h_vocab}<br />
HTML;
		return $html;
	}

	/**
	* Return array of the highlight color options
	*
	* @return : Return Array of the highlight color options
	*/
	public function getHighlightOptionInfoAry() {
		$ary = array();

		$ary[0]['title'] = 'Red';
		$ary[0]['css'] = 'highlight_01';
		$ary[0]['cssSet'] = '1';
		$ary[1]['title'] = 'Orange';
		$ary[1]['css'] = 'highlight_02';
		$ary[1]['cssSet'] = '2';
		$ary[2]['title'] = 'Yellow';
		$ary[2]['css'] = 'highlight_03';
		$ary[2]['cssSet'] = '3';
		$ary[3]['title'] = 'Green';
		$ary[3]['css'] = 'highlight_04';
		$ary[3]['cssSet'] = '4';
		$ary[4]['title'] = 'Cyan';
		$ary[4]['css'] = 'highlight_05';
		$ary[4]['cssSet'] = '5';
		$ary[5]['title'] = 'Blue';
		$ary[5]['css'] = 'highlight_06';
		$ary[5]['cssSet'] = '6';
		$ary[6]['title'] = 'Purple';
		$ary[6]['css'] = 'highlight_07';
		$ary[6]['cssSet'] = '7';
		$ary[7]['title'] = 'Grey';
		$ary[7]['css'] = 'highlight_00';
		$ary[7]['cssSet'] = '0';

		return $ary;
	}

	/**
	* Return html of the highlight color options
	*
	* @return : Return html of the highlight color options
	*/
	public function getHighlightOptionHtml($selectedCssSet=null) {
		$highlightInfoAry = $this->getHighlightOptionInfoAry();

		$html = '';
		$html .= '<div class="newgrp_row">'."\r\n";
			for ($i=0, $i_max=count($highlightInfoAry); $i<$i_max; $i++) {
				$_title = $highlightInfoAry[$i]['title'];
				$_css = $highlightInfoAry[$i]['css'];
				$_cssSet = $highlightInfoAry[$i]['cssSet'];

				$_highlightSelected = '';
				if ($_cssSet == $selectedCssSet) {
					$_highlightSelected = 'highlight_select';
				}

				$html .= '<a title="'.$_title.'" class="highlight_box w2_highlight '.$_css.' '.$_highlightSelected.'" href="javascript:void(0);" onclick="updateRefCategoryCssSetHiddenField(\''.$_cssSet.'\', this);"></a>'."\r\n";
			}
		$html .= '</div>'."\r\n";

		return $html;
	}



   /**
	* GENERATE a Vocab panel for content (mainly used in science now)
	* @owner : Fai (20120223)
	* @param : Array $defaultContent , array of the content that includes the "Keyword" and "Details"
	* @param : String $unique_identifier , any string is ok, should not duplicate for those using this function
	* @return : String HTML Panel
	*/

	function generateReferencePanelStyle2($defaultContent,$unique_identifier){

		$h_str = '';

		$layoutTemplate = <<<LAYOUT
			<li id="{$unique_identifier}_meaning_[COUNTER]_li">
				<a href="javaScript:void(0)" OnClick="handleVocabMeaningDisplay('{$unique_identifier}_meaning_[COUNTER]')">
					[KEYWORD]
					<br />
					<span id="{$unique_identifier}_meaning_[COUNTER]_content" class="meaning" style="display:none;">
					[DETAILS_1]
					</span>
				</a>
			</li>
LAYOUT;

		$layoutPattern = array("[COUNTER]", "[KEYWORD]", "[DETAILS_1]");

		for($i = 0,$i_max = count($defaultContent);$i < $i_max;$i++){
			$_content = $defaultContent[$i];

			$_keyword = $_content[0];  // 0 --> store the keyword
			$_details = $_content[1];  // 1 --> store the details
			$_data   = array( ($i+1), $_keyword, $_details);

			//replace the layout with content
			$h_str .= str_replace($layoutPattern, $_data, $layoutTemplate)."\n";
		}

		return $h_str;
	}
function uploadTeacherAttachmentTemplate($cid,$teacherAttachmentAry,$can_edit=true){
	global $PATH_WRT_ROOT,$LAYOUT_SKIN,$w2_cfg,$Lang;

	$h_data = '';
	//if $allowAction is empty , default allow upload file
	$teacherAttachmentCnt = count($teacherAttachmentAry);
	$newAttachmentCnt = $teacherAttachmentCnt?0:$w2_cfg["DB_W2_CONTENT_DATA_TEACHERATTACTMENT"]["defaultDisplayNum"];
	if($can_edit){
		if(is_array($teacherAttachmentAry) && $teacherAttachmentCnt > 0){
			$i=1;
			foreach($teacherAttachmentAry as $_attachmentItem){
				$_attachmentName = !empty($_attachmentItem['teacherAttachmentItemName'])?$_attachmentItem['teacherAttachmentItemName']:'';
				$_attachmentFilePath = $_attachmentItem['teacherAttachmentItemFile'];
				$_encodedFile = safe_b64encode($_attachmentFilePath);
				$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeTeacherAttachment('.$i.')">&nbsp;</a>';

				$h_data .= '<tr id="w2_existFileId_'.$i.'">';
				$h_data .= '<td><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
				$h_data .= '<td colspan="2">';
					$h_data .= '<a href = "index.php?mod=common&task=downloadTeacherAttachmentFile&cid='.$cid.'&r_fName='.$_encodedFile.'">'.$_attachmentName.' </a>';
				$h_data .= '</td>';
				$h_data .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
				$h_data .= '</tr>';
				$i++;
			}
			$current_cnt = $teacherAttachmentCnt;
		}else{
			for($i=0;$i<$newAttachmentCnt;$i++){
				$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeAttachmentBox(\'w2_uploadFileId_'.$i.'\')">&nbsp;</a>';

				$h_data .= '<tr id="w2_uploadFileId_'.$i.'">';
					$h_data .= '<td><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
					$h_data .= '<td>';
						$h_data .= '<input class="teacherAttachment" type="text" name="r_tattname_'.$i.'" id="r_tattname_'.$i.'" style="width:250px" value=""/>';
					$h_data .= '</td>';
					$h_data .= '<td><input type="file" name="r_tatt_'.$i.'" id="r_tatt_'.$i.'" style="width:190px"/></td>';
					$h_data .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
				$h_data .= '</tr>';
			}
			$current_cnt = $newAttachmentCnt;
		}
		$h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addAttachmentBox()">'.$Lang['W2']['uploadFileMore'].'</a>';

		$html = '<table id="teacher_attachment_table" class="tea_attach">';
		$html .= '<thead>';
			$html .= '<tr>';
				$html .= '<td>&nbsp;</td>';
				$html .= '<td width="250">'.$Lang['W2']['uploadFileName'].'</td>';
				$html .= '<td width="200">'.$Lang['W2']['uploadFileSource'].'</td>';
				$html .= '<td>&nbsp;</td>';
			$html .= '</tr>';
		$html .= '</thead>';
		$html .= $h_data;
		$html .= '</table>';
		$html .= '<table>';
		$html .= '<tr>';
			$html .= '<td>&nbsp;</td>';
			$html .= '<td class="Content_tool_190">'.$h_addMoreAction.'</td>';
			$html .= '<td class="Content_tool_190">&nbsp;</td>';
			$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<input type ="hidden" name="attachmentBox_counter" id="attachmentBox_counter" value="'.$current_cnt.'">';
	}else{
		if(is_array($teacherAttachmentAry) && $teacherAttachmentCnt > 0){
			$html = '<ul class="attach_list">';
   			foreach($teacherAttachmentAry as $_attachmentItem){
				$_attachmentName = $_attachmentItem['teacherAttachmentItemName'];
				$_attachmentFilePath = $_attachmentItem['teacherAttachmentItemFile'];
				$_encodedFile = safe_b64encode($_attachmentFilePath);
					$html .= '<li>';
						$html .= '<a href="index.php?mod=common&task=downloadTeacherAttachmentFile&cid='.$cid.'&r_fName='.$_encodedFile.'">'.$_attachmentName.'</a>';
					$html .= '</li>';
			}
			$html .= '</ul>';
		}
	}
	return $html;
}
function displayHighlightWordsTemplate($highlightWordsAry,$type,$can_edit=false){
	global $PATH_WRT_ROOT,$LAYOUT_SKIN,$w2_cfg,$Lang,$linterface;
	$html = '';
	$html .= '<table style="width:100%;" class="tea_attach" id="w2_highlightword_'.$type.'_table">';
		$html .= '<thead>';
			$html .= '<tr class="vocab_group">';
				$html .= '<td>'.$Lang['W2']['wordsPhrases'].'</td>';
				$html .= '<td>&nbsp;</td>';
				$html .= '<td>'.$Lang['W2']['meaningUsage'].'</td>';
				$html .= '<td>&nbsp;</td>';
			$html .= '</tr>';
		$html .= '</thead>';
		$wordCount = count($highlightWordsAry)?count($highlightWordsAry):$w2_cfg['contentInput']['defaultInputNum'];
		for($i=0;$i<$wordCount;$i++){
			$_ary = $highlightWordsAry['item'.$i];
			$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeAttachmentBox(\'w2_highlightword_'.$type.'_'.$i.'\')">&nbsp;</a>';
			$html .= '<tr class="vocab_group" id="w2_highlightword_'.$type.'_'.$i.'">';
				$html .= '<td class="title" width="20%">';
					$html .= '<input class="highlightword_'.$type.'" type="text" name="'.$type.'_word_'.$i.'" id="'.$type.'_word_'.$i.'" style="width:150px;" value="'.stripslashes($_ary['word']).'"/>';
				$html .= '</td>';
				$html .= '<td width="1%">:</td>';
				$html .= '<td width="74%">';
					$html .= '<textarea class="highlightword_'.$type.'" name="'.$type.'_meaning_'.$i.'" id="'.$type.'_meaning_'.$i.'">'.stripslashes($_ary['meaning']).'</textarea>';
				$html .= '</td>';
				$html .= '<td class="table_row_tool" width="5%">'.$h_deleteAction.'</td>';
			$html .= '</tr>';
		}

    $html .= '</table>';
    $h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addHighlightWordRow(\''.$type.'\')">'.$Lang['W2']['uploadFileMore'].'</a>';
    $html .= '<table>';
		$html .= '<tr>';
			$html .= '<td>&nbsp;</td>';
			$html .= '<td class="Content_tool_190">'.$h_addMoreAction.'</td>';
			$html .= '<td class="Content_tool_190">&nbsp;</td>';
			$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
	$html .= '</table>';
	$current_cnt = $wordCount?$wordCount:$w2_cfg['contentInput']['defaultInputNum'];
	$html .= $linterface->Get_Form_Warning_Msg('r_'.$type.'Word_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');
	$html .= '<input type ="hidden" name="w2_highlightword_'.$type.'_counter" id="w2_highlightword_'.$type.'_counter" value="'.$current_cnt.'">';
    return $html;
}
function displayMoreVocabTemplate($step,$cid,$vocabContent){
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");
	$FCKEditor = new FCKeditor ( 'step'.$step.'Vocab' , "100%", "280", "", "", "");
	$FCKEditor->Value = $vocabContent;
	$stepVocabEditor = trim( $FCKEditor->CreateHtml() );
	return $stepVocabEditor;
}
function displayGrammarAnalysisTemplate($step,$cid,$grammarContent){
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");
	$FCKEditor = new FCKeditor ( 'step'.$step.'Grammar' , "100%", "280", "", "", "");
	$FCKEditor->Value = $grammarContent;
	$stepGrammar = trim( $FCKEditor->CreateHtml() );
	return $stepGrammar;
}
function viewUploadFileTemplate($fileUploadResult,$allowAction){
	global $PATH_WRT_ROOTimages,$LAYOUT_SKIN,$w2_cfg,$Lang;

	$h_data = '';

	//if $allowAction is empty , default allow upload file
	$allowAction = (trim($allowAction)=='') ? $w2_cfg['uploadFileActionMode']['allowUpload']:$allowAction;

	if(is_array($fileUploadResult) && count($fileUploadResult) > 0){
		for($i = 0, $i_max = count($fileUploadResult) ; $i < $i_max; $i++){
			$_fileSupplementName = $fileUploadResult[$i]['FILE_SUPPLEMENTARY_NAME'];
			$_fileOriName = $fileUploadResult[$i]['FILE_ORI_NAME'];
			$_fileHashName = $fileUploadResult[$i]["FILE_HASH_NAME"];
			$_fileSupplementName = ($_fileSupplementName == '')?'--':$_fileSupplementName;

			$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeAttachment(\''.$_fileHashName.'\','.$i.')">&nbsp;</a>';

			if($allowAction == $w2_cfg['uploadFileActionMode']['doNotAllowUpload']){
				$h_deleteAction = '&nbsp;';
			}

			$h_data .= '<tr id="w2_uploadFileId_'.$i.'">';
			$h_data .= '<td><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
			//$h_data .= '<td>'.$_fileSupplementName.'</td>';
			$h_data .= '<td><a href = "index.php?mod=common&task=downloadFile&r_fName='.$_fileHashName.'">'.$_fileOriName.' </a></td>';
			$h_data .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
			$h_data .= '</tr>';

		}
	}

	$h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addAttachmentBox()">'.$Lang['W2']['uploadFileMore'].'</a>';
	if($allowAction == $w2_cfg['uploadFileActionMode']['doNotAllowUpload']){
		$h_addMoreAction = '&nbsp;';
	}

	$html = <<<HTML2
			<div class="studentattach">
				{$Lang['W2']['uploadFileCaption']}:<br />

						<table id="w2_div_attachmentBox">
							<tbody>
								<thead class="attach">
									<!--tr>
										<td>&nbsp;</td>
											<td>&nbsp;{$Lang['W2']['uploadFileName']}</td>
											<td>&nbsp;{$Lang['W2']['uploadFileSource']}</td>
										<td>&nbsp;</td>
									</tr-->
								</thead>
								{$h_data}
							</tbody>
						</table>

					<input type ="hidden" name="attachmentBox_counter" id="div_attachmentBox_counter" value="0">
					<table class = "sub" id="">
						<tr><td class="Content_tool_190">{$h_addMoreAction}</td></tr>
					</table>
					<iframe id="uploadFileIFrame" name="uploadFileIFrame" style="width:100%;height:300px;display:none;"></iframe>
			</div>
HTML2;
	return $html;

}
function uploadStudentAttachmentTemplate($fileUploadResult,$allowAction){
	global $PATH_WRT_ROOT,$LAYOUT_SKIN,$w2_cfg,$Lang;

	$h_data = '';

	//if $allowAction is empty , default allow upload file
	$allowAction = (trim($allowAction)=='') ? $w2_cfg['uploadFileActionMode']['allowUpload']:$allowAction;

	if(is_array($fileUploadResult) && count($fileUploadResult) > 0){
		for($i = 0, $i_max = count($fileUploadResult) ; $i < $i_max; $i++){
			$_fileSupplementName = $fileUploadResult[$i]['FILE_SUPPLEMENTARY_NAME'];
			$_fileOriName = $fileUploadResult[$i]['FILE_ORI_NAME'];
			$_fileHashName = $fileUploadResult[$i]["FILE_HASH_NAME"];
			$_fileSupplementName = ($_fileSupplementName == '')?'--':$_fileSupplementName;

			$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeStudentAttachmentFile(\''.$_fileHashName.'\','.$i.')">&nbsp;</a>';

			if($allowAction == $w2_cfg['uploadFileActionMode']['doNotAllowUpload']){
				$h_deleteAction = '&nbsp;';
			}

			$h_data .= '<tr id="w2_uploadFileId_'.$i.'">';
			$h_data .= '<td><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
			$h_data .= '<td>'.$_fileSupplementName.'</td>';
			$h_data .= '<td><a href = "index.php?mod=common&task=downloadFile&r_fName='.$_fileHashName.'">'.$_fileOriName.' </a></td>';
			$h_data .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
			$h_data .= '</tr>';

		}
	}else{
		$defaultAttachmentCnt = $w2_cfg["DB_W2_STEP_STUDENT_FILE_STUDENTATTACHMENT"]["defaultDisplayNum"];
		for($i=0;$i<$defaultAttachmentCnt;$i++){
				$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeAttachmentBox(\'div_attachmentBox_'.$i.'\')">&nbsp;</a>';

				$h_data .= '<tr id="div_attachmentBox_'.$i.'">';
					$h_data .= '<td><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
					$h_data .= '<td>';
						$h_data .= '<input type="text" name="r_attachmentBox_supplementName[]" id="div_attachmentBox_supplementName_'.$i.'" size="40" value=""/>';
					$h_data .= '</td>';
					$h_data .= '<td><input type="file" name="r_attachmentBox[]" id="div_attachmentBox_'.$i.'" size="40"/></td>';
					$h_data .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
				$h_data .= '</tr>';
		}
	}

	$h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addStudentAttachmentBox()">'.$Lang['W2']['uploadFileMore'].'</a>';
	if($allowAction == $w2_cfg['uploadFileActionMode']['doNotAllowUpload']){
		$h_addMoreAction = '&nbsp;';
	}
	$html = <<<HTML2
			<div class="studentattach" style="border:none">
				{$Lang['W2']['uploadFileCaption']}:<br />

						<table id="w2_div_attachmentBox" class="sub">
								<thead class="attach">
									<tr>
										<td width="10">&nbsp;</td>
											<td width="250">&nbsp;{$Lang['W2']['uploadFileName']}</td>
											<td width="200">&nbsp;{$Lang['W2']['uploadFileSource']}</td>
										<td>&nbsp;</td>
									</tr>
								</thead>
								<tbody>
								{$h_data}
								</tbody>
						</table>

					<input type ="hidden" name="attachmentBox_counter" id="div_attachmentBox_counter" value="0">
					<table class = "sub" id="">
						<tr><td class="Content_tool_190">{$h_addMoreAction}</td></tr>
					</table>
					<iframe id="uploadFileIFrame" name="uploadFileIFrame" style="width:100%;height:300px;display:none;"></iframe>
			</div>
HTML2;
	return $html;

}
	function displayViewStudentUploadFile($studentID,$stepId,$allowAction = '',$isContentInputStep2=false){
		global $intranet_db,$w2_cfg;

		$objDb  = new libdb();

		$sql = 'select RECORD_ID , USER_ID,STEP_ID,FILE_ORI_NAME,FILE_HASH_NAME,FILE_SUPPLEMENTARY_NAME,FILE_PATH from '.$intranet_db.'.W2_STEP_STUDENT_FILE where USER_ID = \''.$studentID.'\' and STEP_ID = \''.$stepId.'\' order by FILE_ORI_NAME';
		$fileUploadResult = $objDb->returnResultSet($sql);
		$h_fileUpload  = '';
		$h_fileUpload = $isContentInputStep2?$this->uploadStudentAttachmentTemplate($fileUploadResult,$allowAction):$this->viewUploadFileTemplate($fileUploadResult,$allowAction);
		return $h_fileUpload;
	}

	function getContentList($contentCode){
		global $w2_cfg_contentSetting;

		return $w2_cfg_contentSetting[$contentCode];


	}
	function getContentAttributes($contentCode , $schemeCode){
		global $w2_cfg_contentSetting;
		return $w2_cfg_contentSetting[$contentCode][$schemeCode];

	}
	function getContentGroupByType($contentCode){
		$allTitleForThisContent = $this->getContentList($contentCode);

		$defaultContentType = array();

		foreach ($allTitleForThisContent as $eachSchemeCode => $schemeDetails){
			$_type = $schemeDetails['type'];
			$schemeDetails['schemeCode'] = $eachSchemeCode;
			$defaultContentType[$_type][] = $schemeDetails;
		}
		return $defaultContentType;
	}

	function getContentGroupByTypeSuper($contentCode){
		$allTitleForThisContent = $this->getContentList($contentCode);

		foreach ($allTitleForThisContent as $eachSchemeCode => $schemeDetails){
			$_typeSuper = $schemeDetails['type_super'];
			$schemeDetails['schemeCode'] = $eachSchemeCode;
			$defaultContentType[$_typeSuper][] = $schemeDetails;
		}
		return $defaultContentType;

	}
	# Generate Random Peer Marking Grouping
	# format: studentArr[$user_id]['other_info_key']
	function genRandomPeerMarkingGrouping($studentArr, $group_num){
		global $Lang;
		$str = '';

		if($group_num > 0){
			$groupArr = array();
			if(!empty($studentArr)){
				$stdIDArr = array_keys($studentArr);

				$group_idx = 0;
				while(count($stdIDArr) > 0){
					$rand_index = array_rand($stdIDArr, 1);			# get the random selected index
					$rand_id = $stdIDArr[$rand_index];				# get the random selected user_id
					unset($stdIDArr[$rand_index]);					# truncate the extraced user_id

					$groupArr[$group_idx][] = $rand_id;				# assign to specific group one by one
					$group_idx = ($group_idx == $group_num-1) ? 0 : $group_idx + 1;
				}

				$str .= $this->getPeerMarkingGroupingDistribution($groupArr, $studentArr);

			} else {
				$str .= $Lang['Assessment']['NoStudentIsAssignedToAssessment'];
			}
		} else {
			$str .= $Lang['Assessment']['InvalidGroupNumber'];
		}
		return $str;
	}

	# Variable Format :
	# 	groupArr[i][j] - [i] index indicates group ; [j] - index indicates student ; [i][j] - indicates student user_id in group i
	# 	studentArr[$user_id]['xxx'] - xxx : other student information
	function getPeerMarkingGroupingDistribution($groupArr, $studentArr){
		global $Lang;

		$within_group_column = count($groupArr);

		$within_group_min_user = 0;
		if ($within_group_column > 0) {
			$within_group_min_user = floor(count($studentArr) / $within_group_column);
		}

		$str = '';
		$str .= '
			<table class="peer_within_group_board">
              <tr>';
				$str .= '<input type="hidden" id="within_group_column" name="within_group_column" value="'.$within_group_column.'" />';
				$str .= '<input type="hidden" id="within_group_min_user" name="within_group_min_user" value="'.$within_group_min_user.'" />';
				for($i=0 ; $i<count($groupArr) ; $i++){
					$str .= '<td width="50%">';
					$str .= '
					  <div class="within_group_title">
						<span class="within_group_title_name">'.$Lang['Header']['Menu']['Group'].($i+1).'</span>
						<!--<span class="within_group_title_no">'.count($groupArr[$i]).'</span>-->
						<input type="hidden" id="group_user_serialize_'.$i.'" name="group_user_serialize_'.$i.'" value="" />
						<p class="spacer"></p>
					  </div>';
				    $str .= '<div id="group_column_'.$i.'" class="within_column" style="padding-bottom:10px;">';
					for($j=0 ; $j<count($groupArr[$i]) ; $j++){
						$user_id   = $groupArr[$i][$j];

						if (!isset($studentArr[$user_id])) {
							continue;
						}

						$class_name = $studentArr[$user_id]['className'];
						$class_no = $studentArr[$user_id]['classNumber'];
						$student_name = $studentArr[$user_id]['studentName'];

						$name_display = $class_name.' - '.$class_no.' '.$student_name;

						$str .= '<div class="within_group_student" id="group_user_id_'.$user_id.'">';
							$str .= '<a href="javascript:void(0)">'.$name_display.'</a>';
						$str .= '</div>';
					}
					$str .= '</div>';
					$str .= '</td>';
					$str .= ($i % 2 == 1) ? '</tr></tr>' : '';
				}
				$str .= '
              </tr>
            </table>';
		return $str;
	}

	public function isMatchedSavedPeerMarkingGrouping($writingId, $newPeerMarkingGroupingAry) {
		$groupingAssoAry = $this->getPeerMarkingGrouping($writingId);

		$isMatch = true;
		for ($i=0, $i_max=count($newPeerMarkingGroupingAry); $i<$i_max; $i++) {
			$_newGroupStudentIdList = $newPeerMarkingGroupingAry[$i];
			$_newGroupStudentIdAry = explode(',', $_newGroupStudentIdList);

			$_savedGroupStudentIdAry = (array)$groupingAssoAry[$i];

			$_diffGroupStudentIdAry = array_diff($_newGroupStudentIdAry, $_savedGroupStudentIdAry);
			$_isSameGrouping = (count($_diffGroupStudentIdAry) == 0)? true : false;

			if ($_isSameGrouping) {
				// matched => check next group
				continue;
			}
			else {
				$isMatch = false;
				break;
			}
		}

		return $isMatch;
	}

	public function getPeerMarkingGrouping($writingId) {
		global $intranet_db, $w2_cfg;

		$W2_PEER_MARKING_GROUPING = $intranet_db.'.W2_PEER_MARKING_GROUPING';
		$INTRANET_USER = $intranet_db.'.INTRANET_USER';
		$sql = "Select
						pmg.GROUP_NUM, pmg.USER_ID
				From
						$W2_PEER_MARKING_GROUPING as pmg
						Inner Join $INTRANET_USER as iu On (pmg.USER_ID = iu.UserID)
				Where
						WRITING_ID = \''".$writingId."'\' And DELETE_STATUS = '".$w2_cfg["DB_W2_PEER_MARKING_GROUPING_DELETE_STATUS"]["active"]."'
				Order By
						iu.ClassName, iu.ClassNumber
				";
		$groupingInfoAry = $this->objDb->returnResultSet($sql);

		return BuildMultiKeyAssoc($groupingInfoAry, array('GROUP_NUM'), array('USER_ID'), $SingleValue=1, $BuildNumericArray=1);
	}

	public function deletePeerMarkingGrouping($writingId, $groupNumAry='', $studentIdAry='') {
		global $intranet_db, $w2_cfg;

		if ($groupNumAry != '') {
			$condsGroupNum = " And GROUP_NUM In ('".implode("','", (array)$groupNumAry)."') ";
		}
		if ($studentIdAry != '') {
			$condsStudentId = " And USER_ID In ('".implode("','", (array)$studentIdAry)."') ";
		}

		$W2_PEER_MARKING_GROUPING = $intranet_db.'.W2_PEER_MARKING_GROUPING';
		$sql = "Update $W2_PEER_MARKING_GROUPING
				Set DELETE_STATUS = '".$w2_cfg["DB_W2_PEER_MARKING_GROUPING_DELETE_STATUS"]["isDeleted"]."', DELETED_BY = \''".$_SESSION['UserID']."'\'
				Where WRITING_ID = \''".$writingId."'\' $condsGroupNum $condsStudentId
				";
		return $this->objDb->db_db_query($sql);
	}

	public function insertPeerMarkingGrouping($writingId, $groupNum, $studentIdAry) {
		global $intranet_db;

		$success = false;

		$numOfStudent = count((array)$studentIdAry);
		if ($numOfStudent > 0) {
			$insertAry = array();
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $studentIdAry[$i];

				$insertAry[] = "('".$writingId."', '".$groupNum."', '".$_studentId."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')";
			}
			$insertValue = implode(',', (array)$insertAry);

			$W2_PEER_MARKING_GROUPING = $intranet_db.'.W2_PEER_MARKING_GROUPING';
			$sql = "Insert Into $W2_PEER_MARKING_GROUPING
						(WRITING_ID, GROUP_NUM, USER_ID, DATE_INPUT, INPUT_BY, DATE_MODIFIED, MODIFIED_BY)
					Values
						$insertValue
					";
			$success = $this->objDb->db_db_query($sql);
		}

		return $success;
	}

	/*
	 * 	based on $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList'], get the key of this list
	 * 	to check if the level is secondary or not
	 */
	public function isSecondary($key) {
		$level_key = array();
		$level_key[] = 'S1';
		$level_key[] = 'S2';
		$level_key[] = 'S3';
		$level_key[] = 'S4';
		$level_key[] = 'S5';
		$level_key[] = 'S6';
		$level_key[] = 'S7';
		$level_key[] = 'SJ';
		$level_key[] = 'SS';
		$level_key[] = 'F1';
		$level_key[] = 'F2';
		$level_key[] = 'F3';
		$level_key[] = 'F4';
		$level_key[] = 'F5';
		$level_key[] = 'F6';
		$level_key[] = '7';
		$level_key[] = '8';
		$level_key[] = '9';
		$level_key[] = '10';
		$level_key[] = '11';
		$level_key[] = '12';
		$level_key[] = 'ERG';
		$level_key[] = 'ERC';
		return (in_array($key,$level_key) ? true : false);
	}

	public function getGradeSelection($grade='',$otherParameters='',$showAll=false){
		global $Lang;
		$selection = '';
		$selection .= '<select name="grade" id="grade" '.$otherParameters.'>';
		$selection .= $showAll?'<option value="">'.$Lang['W2']['allGrades'].'</option>':'';
		$selection .= '<option value="'.$Lang['W2']['na_without_slash'].'">'.$Lang['W2']['na_without_slash'].'</option>';
		foreach ($Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList'] as $key=>$var) {
//			if(strpos($var, 'Secondary') !== false){
			if($this->isSecondary($key)){
				$selected = !empty($grade)&&$grade==$key?' selected':'';
				$selection .= '<option value="'.$key.'"'.$selected.'>'.$var.'</option>';
			}
		}
		$selection .= '</select>';
		return $selection;

	}
	/*Use in View Writing Step 3*/
	public function getEssayRemarkHTML($remarkAry){
		global $w2_cfg,$Lang;

		$html = '<ul>';
		for($i=1;$i<=$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
			$_remarkAry = $remarkAry['remarkItem'.$i];

			$_hasInput = count($_remarkAry)>0?true:false;
			if($_hasInput){
				$_title = !empty($_remarkAry['remarkTitle'])?stripslashes($_remarkAry['remarkTitle']):'';
				$_content = !empty($_remarkAry['remarkContent'])?stripslashes($_remarkAry['remarkContent']):'';
				$_remarkClass = 'remark_highlight_icon_big_'.str_pad($i, 2, 0, STR_PAD_LEFT);
				$html .= '<li>';
					$html .= '<span id="remark_highlight_icon_big" class="'.$_remarkClass.'">';
						$html .= '<h2>'.$_title.'</h2><u style="display:none;">'.$_content.'</u>';
					$html .= '</span>';

				$html .= '</li>';
			}
		}
		$html .= '</ul>';
		return $html;
	}
	/*Use in Content Input Step 3 Sample Writing*/
	public function getStep3RemarkHTML($remarkAry=array(),$step=''){
		global $w2_cfg,$Lang;
		##initialize

		$remarkTitleName = !empty($step)?'r_step'.$step.'RemarkTitle':'remarkTitle';
		$remarkContentName = !empty($step)?'r_step'.$step.'RemarkContent':'remarkContent';
		$html = '<ul>';
		for($i=1;$i<=$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
			$_remarkAry = $remarkAry['remarkItem'.$i];
			$_title = !empty($_remarkAry['remarkTitle'])?stripslashes($_remarkAry['remarkTitle']):'';
			$_content = !empty($_remarkAry['remarkContent'])?stripslashes($_remarkAry['remarkContent']):'';
			$_hasInput = !empty($_title)||!empty($_content)?true:false;
			$_remarkClass = 'remark_highlight_icon_big_'.str_pad($i, 2, 0, STR_PAD_LEFT);
			$_remarkClass .= $_hasInput?'':' empty';
			$html .= '<li>';
				$html .= '<span id="remark_highlight_icon_big" class="'.$_remarkClass.'">';
				if($_hasInput){
					$html .= '<input type="text" name="'.$remarkTitleName.'_'.$i.'" id="'.$remarkTitleName.'_'.$i.'" value="'.$_title.'"/>';
   					$html .= '<textarea name="'.$remarkContentName.'_'.$i.'" id="'.$remarkContentName.'_'.$i.'">'.$_content.'</textarea>';
				}else{
					$html .= '<a href="javascript:void(0);" id="a_'.$i.'">'.$Lang['W2']['enterText'].'</a>';
				}
				$html .= '</span>';
			$html .= '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	/*For View Writing*/
	public function getEssayBubbleHTML($paragraphAry){
		global $w2_cfg;
		#init
		$i=1;
		$html = '';
		foreach( (array)$paragraphAry as $_paragraphItem){
			$bubbleType = ($i>$w2_cfg['contentInput']['SampleWritingLeftBubbleNum'])?$i%$w2_cfg['contentInput']['SampleWritingLeftBubbleNum']:$i;
			$bubble_class = "bubble_".str_pad($bubbleType, 2, 0, STR_PAD_LEFT);
			$_title = !empty($_paragraphItem['leftBubbleTitle'])?stripslashes($_paragraphItem['leftBubbleTitle']):'';
			$_content = !empty($_paragraphItem['leftBubbleContent'])?stripslashes($_paragraphItem['leftBubbleContent']):'';
			$_paragraph = $_paragraphItem['paragraph'];
			$_display = !(empty($_title)&&empty($_content))?'':'style="display:none;"';
			#get html
			$html .= '<div class="content_main_grp">';
				$html .= '<div class="bubble" '.$_display.'>';
					$_title .= !empty($_title)?':':'';
					$html .= '<div id="bubble_left" class="'.$bubble_class.'"><span>'.$_title.'</span> '.$_content.'</div>';
				$html .= '</div>';
				$html .= '<div class="essay">'.$_paragraph.'</div>';
			$html .= '</div>';
			$i++;
		}
		return $html;
	}
	public function getStep3MyOutlineHTML($idx,$outlineAry=array(),$action=''){
		global $w2_cfg,$Lang,$PATH_WRT_ROOT,$LAYOUT_SKIN,$linterface;
		$action = !empty($action)?$action:$w2_cfg['contentInput']['SampleWritingAction']['new'];
		$outline = !empty($outlineAry['outline'])?$outlineAry['outline']:'';
		$section = !empty($outlineAry['section'])?$outlineAry['section']:'';
		$html = '';
		$html .= '<div class="content_input_main_grp" id="div_outline_'.$idx.'">';
//			$html .= '<div class="content_input_main_tag">';
//				$html .= '<input type="text" name="section_'.$idx.'" id="section_'.$idx.'" value="'.$section.'" />';
//				 $html .= $linterface->Get_Form_Warning_Msg('r_Section_warningDiv'.$idx, $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
//            $html .= '</div>';
            $html .= '<div>';
            	$html .= '<div style="width:70%;float:left;"><textarea name="outline_'.$idx.'" id="outline_'.$idx.'" rows="2">'.intranet_undo_htmlspecialchars($outline).'</textarea></div>';
            	$html .= '<span class="table_row_tool"><a href=javascript:void(0);" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeStepItemBox(\'div_outline_'.$idx.'\');">&nbsp;</a></span>';
            $html .= '</div>';
            $html .= '<br style="clear:both" />';
            $html .= $linterface->Get_Form_Warning_Msg('r_Outline_warningDiv'.$idx, $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
       	$html .= '</div>';


    	return $html;
	}
	/*For Content Input Engine*/
	public function getParagraphBubbleHTML($paragraphId,$paragraphAry=array(),$action='NEW',$step='',$hasHighlight=true){
		global $w2_cfg,$Lang,$PATH_WRT_ROOT,$LAYOUT_SKIN,$linterface;
		$idx = $paragraphId%$w2_cfg['contentInput']['SampleWritingLeftBubbleNum'];
		$bubbleType = ($idx>0)?$idx:$w2_cfg['contentInput']['SampleWritingLeftBubbleNum'];
		$bubble_class = "bubble_".str_pad($bubbleType, 2, 0, STR_PAD_LEFT);
		##initialize
		$editorName = !empty($step)?'r_step'.$step.'editorParagraph':'editorParagraph';
		$bubbleTitleName = !empty($step)?'r_step'.$step.'BubbleTitle':'leftBubbleTitle';
		$bubbleContentName = !empty($step)?'r_step'.$step.'BubbleContent':'leftBubbleContent';
		$paragraphWarningName = !empty($step)?'Step'.$step.'Paragraph':'Paragraph';

		$FCKEditor = new FCKeditor ( $editorName.$paragraphId , "100%", "300", "", "", "");
		$action = count($paragraphAry)==0?'NEW':$action;
		if($action=='EDIT'){
			$leftBubbleTitle = !empty($paragraphAry['leftBubbleTitle'])?stripslashes($paragraphAry['leftBubbleTitle']):'';
			$leftBubbleContent = !empty($paragraphAry['leftBubbleContent'])?stripslashes($paragraphAry['leftBubbleContent']):'';
			$FCKEditor->Value = !empty($paragraphAry['paragraph'])?intranet_undo_htmlspecialchars($paragraphAry['paragraph']):'';
		}else{
			$leftBubbleContent = '';
			$leftBubbleTitle = '';
			$FCKEditor->Value = '';
		}
		if($hasHighlight){
			$FCKEditor->ToolbarSet = "W2_SampleWriting";
		}
		$FCKEditor->Config['EditorAreaCSS'] = $PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/css/w2/fckeditor_highlight.css";
		$editor = trim( $FCKEditor->CreateHtml());
		$html = '';
		$html .= '<div id="div_paragraph_'.(!empty($step)?$step.'_':'').$paragraphId.'" class="content_input_main_grp step'.$step.'WritingSample">';
			$html .= '<div class="content_input_main_tag">';
				$html .= '<div class="bubble_left '.$bubble_class.'">';
					$html .= '<input type="text" name="'.$bubbleTitleName.'_'.$paragraphId.'" id="'.$bubbleTitleName.'_'.$paragraphId.'" value="'.$leftBubbleTitle.'" />';
					$html .= '<textarea name="'.$bubbleContentName.'_'.$paragraphId.'" id="'.$bubbleContentName.'_'.$paragraphId.'">'.$leftBubbleContent.'</textarea></div>';
            $html .= '</div>';
            $html .= '<div class="content_input_main">';
            	$html .= '<div style="width:94%;float:left;">'.$editor.'</div>';
            	$html .= '<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeParagraph(\''.(!empty($step)?$step.'_':'').$paragraphId.'\');">&nbsp;</a></span>';
            $html .= '</div>';
            $html .= $linterface->Get_Form_Warning_Msg('r_'.$paragraphWarningName.'_warningDiv'.$paragraphId, $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');
            $html .= '<br style="clear:both" />';
       $html .= '</div>';
       return $html;
	}
	function getStep3ThickboxSampleWritngHTML($parseData=array(),$can_edit=true){
		global $linterface,$PATH_WRT_ROOT,$Lang,$LAYOUT_SKIN;
		$step3Writing = !empty($parseData['step3Writing'])?intranet_undo_htmlspecialchars($parseData['step3Writing']):'';
		if($can_edit){
			include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");
			$FCKEditor = new FCKeditor ( 'step3Writing' , "100%", "500", "", "", "");
			$FCKEditor->Value = $step3Writing;
			$FCKEditor->ToolbarSet = "W2_SampleWriting";
			$FCKEditor->Config['EditorAreaCSS'] = $PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/css/w2/fckeditor_highlight.css";
			$step3ParagraphEditor = trim( $FCKEditor->CreateHtml() );
			$html = '';
			$html .= $linterface->Get_Form_Warning_Msg('r_Step3Writing_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
			$html .= '<div class="content_input_sample" style="padding-left:0px">';
	        	$html .= '<div class="content_input_sample_left">';
	                  $html .= '<div>';
		        		$html .= $step3ParagraphEditor;
		       		 $html .= '</div>';

	            $html .= '</div>';
				$html .= '<div class="content_input_sample_right">';
			        $html .= '<div class="content_input_main_remark">';
			        	$html .= $this->getStep3RemarkHTML($parseData['step3RemarkAry']);
			        $html .= '</div>';
		        $html .= '</div>';
			$html .= '</div>';
		}else{
			$html .= '<div class="sample">';
      			$html .= '<div class="essay">'.$step3Writing.'</div>';
      			$html .= '<div class="remarks">';
					$html .= $this->getEssayRemarkHTML($parseData['step3RemarkAry']);
      			$html .= '</div>';
      			$html .= '<br style="clear:both" />';
   			 $html .= '</div>';
		}
		return $html;
	}
	// modified from /home/web/eclass40/eclass40/src/includes/php/lib-assessment.php function Get_Specific_Task_Peer_List()
	## Get Specific Task Peer List Array from the orignal peer list
	# Generate by recursive way
	# Logic :
	# Current/self target : C
	# Peer list : A, B, D, E
	#   case 1 - the number of peers that C can mark is 2, C can mark D, E
	#   case 2 - the number of peers that C can mark is 3, C can mark D, E, A
	#   case 3 - the number of peers that C can mark is 5, C can mark D, E, A, B
	public function getStudentPeerMarkingTargetList($peerInfo, $own_id, $target_num, $newPeerInfo, $noChecking=false){

		$returnArr = (count($newPeerInfo) > 0) ? $newPeerInfo : array();
		$target_num = ($target_num < count($peerInfo)) ? $target_num : count($peerInfo);

		if(count($peerInfo) > 0){
			for($i=0 ; $i<count($peerInfo) ;$i++){
				if($noChecking){
					if($own_id < $peerInfo[$i]){ break; }
					# start getting target_id from the beginning
					$returnArr[] = $peerInfo[$i];
				} else {
					if($own_id < $peerInfo[$i]){
						$returnArr[] = $peerInfo[$i];
					}
				}

				if(count($returnArr) < $target_num){
					# case : when looping until the end of the array, the peers number does not match with the expected peer target num
					if($i == count($peerInfo)-1){
						$returnArr = $this->getStudentPeerMarkingTargetList($peerInfo, $own_id, $target_num, $returnArr, true);
					}
				} else if(count($returnArr) == $target_num){
					break;
				}
			}
		}
		return $returnArr;
	}

	public function deleteStudentPeerMarkingTargetData($writingId) {
		global $intranet_db, $w2_cfg;

		$W2_PEER_MARKING_DATA = $intranet_db.'.W2_PEER_MARKING_DATA';
		$sql = "Update $W2_PEER_MARKING_DATA
				Set DELETE_STATUS = '".$w2_cfg["DB_W2_PEER_MARKING_DATA_DELETE_STATUS"]["isDeleted"]."', DELETED_BY = '".$_SESSION['UserID']."'
				Where WRITING_ID = '".$writingId."' And DELETE_STATUS = '".$w2_cfg["DB_W2_PEER_MARKING_DATA_DELETE_STATUS"]["active"]."'
				";
		$success = $this->objDb->db_db_query($sql);
	}

	public function insertStudentPeerMarkingTargetData($writingId, $peerMarkingTargetAssoAry) {
		global $intranet_db;

		$W2_PEER_MARKING_DATA = $intranet_db.'.W2_PEER_MARKING_DATA';
		$insertAry = array();
		foreach ((array)$peerMarkingTargetAssoAry as $_markerStudentId => $_targetStudentIdAry) {
			for ($i=0, $i_max=count((array)$_targetStudentIdAry); $i<$i_max; $i++) {
				$_targetStudentId = $_targetStudentIdAry[$i];

				$insertAry[] = "('".$writingId."', '".$_markerStudentId."', '".$_targetStudentId."', now(), '".$_SESSION['UserID']."')";
			}
		}

		$success = false;
		if (count($insertAry) > 0) {
			$sql = "Insert Into $W2_PEER_MARKING_DATA
						(WRITING_ID, MARKER_USER_ID, TARGET_USER_ID, DATE_INPUT, INPUT_BY)
					Values
						".implode(',', (array)$insertAry)."
					";
			$success = $this->objDb->db_db_query($sql);
		}

		return $success;
	}

	public function updatePeerMarkingViewStatus($writingId, $viewStatus, $markerUserIdAry=null, $targetUserIdAry=null){
		global $intranet_db;

		if ($markerUserIdAry !== null) {
			$condsMarkerUserId = " And MARKER_USER_ID In ('".implode("','", (array)$markerUserIdAry)."') ";
		}

		if ($targetUserIdAry !== null) {
			$condsTargetUserId = " And TARGET_USER_ID In ('".implode("','", (array)$targetUserIdAry)."') ";
		}

		$W2_PEER_MARKING_DATA = $intranet_db.'.W2_PEER_MARKING_DATA';
		$sql = "Update $W2_PEER_MARKING_DATA Set VIEW_STATUS = '".$viewStatus."' Where WRITING_ID = '".$writingId."' $condsMarkerUserId $condsTargetUserId";
		return $this->objDb->db_db_query($sql);
	}

	public function getStudentPeerMarkingTargetData($markerUserIdAry=null, $writingIdAry=null, $markedOnly=null, $targetUserIdAry=null, $version=1) {
		global $intranet_db, $w2_cfg;

		if ($markerUserIdAry !== null) {
			$condsMarkerUserId = " And MARKER_USER_ID In ('".implode("','", (array)$markerUserIdAry)."') ";
		}

		if ($writingIdAry !== null) {
			$condsWritingId = " And WRITING_ID In ('".implode("','", (array)$writingIdAry)."') ";
		}

		if ($markedOnly !== null) {
			if ($markedOnly) {
				$condsMarkingStatus = " And COMMENT is not null ";
			}
			else {
				$condsMarkingStatus = " And COMMENT is null ";
			}
		}

		if ($targetUserIdAry !== null) {
			$condsTargetUserId = " And TARGET_USER_ID In ('".implode("','", (array)$targetUserIdAry)."') ";
		}
		$condsVersion = " AND VERSION = '".$version."'";
		$W2_PEER_MARKING_DATA = $intranet_db.'.W2_PEER_MARKING_DATA';
		$sql = "Select RECORD_ID, WRITING_ID, MARKER_USER_ID, TARGET_USER_ID, COMMENT, VIEW_STATUS, DATE_MODIFIED, MODIFIED_BY From $W2_PEER_MARKING_DATA Where DELETE_STATUS = '".$w2_cfg["DB_W2_PEER_MARKING_DATA_DELETE_STATUS"]["active"]."' $condsMarkerUserId $condsWritingId $condsMarkingStatus $condsTargetUserId $condsVersion";
		return $this->objDb->returnResultSet($sql);
	}

	public function getShareButton($_setShareAction, $contentCode, $schemeCode, $stepId, $w2_m_ansCode, $w2_thisStudentID){
		global $Lang;
		if($_setShareAction==1){
			$_shareCaption = $Lang['W2']['shareThisWriting'];
//			$_class = 'btn_certificate_light btn_share_new';
			$_class = 'btn_share_new';
		}else{
			$_shareCaption = $Lang['W2']['shareWritingShared'];
//			$_class = 'btn_certificate_dark btn_share_new';
			$_class = 'btn_share_new shared';
			$_mouseover = "changeShareText(1)";
			$_mouseout = "changeShareText(0)";
		}
		return <<<html
<a share_action="{$_setShareAction}" onmouseover="{$_mouseover}" onmouseout="{$_mouseout}" id="w2_shareToStudent" href="javascript:void(0);" class="{$_class}" onclick="shareToStudent('{$contentCode}','{$schemeCode}','{$stepId}','{$w2_m_ansCode[0]}','{$w2_thisStudentID}');">
	<span>{$_shareCaption}</span>
</a>
html;
	}

	public function getPeerCommentButton() {
		global $Lang;

		return <<<html
<a onclick="MM_showHideLayers('layer_peer_comment01','','show'); MM_showHideLayers('layer_comment01','','hide');" class="btn_comment_tea" href="#">
	<span>{$Lang['W2']['peerComment']}</span>
</a>
html;
	}

	public function getPeerCommentLayer($isEditMode, $recordId, $isViewSelfPeerComment, $writingId, $targetStudentId) {
		global $Lang;

		$content = $this->getPeerCommentLayerContent($isEditMode, $recordId, $isViewSelfPeerComment, $writingId, $targetStudentId);

		$button = '';
		if ($isEditMode) {
			$onclick = 'savePeerComment(\''.$Lang['W2']['returnMsgArr']['peerCommentUpdateSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['peerCommentUpdateFailed'].'\');';
			$button .= '<input type="button" onclick="'.$onclick.'" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'.$Lang['Btn']['Save'].'">';
		}
		$button .= '<input type="button" onclick="MM_showHideLayers(\'layer_peer_comment01\',\'\',\'hide\')" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" value="'.$Lang['Btn']['Close'].'">';

		$html = $this->getPeerCommentLayerTemplate();
		$html = str_replace('{{{contentHtml}}}', $content, $html);
		$html = str_replace('{{{buttonHtml}}}', $button, $html);

		return $html;
	}

	public function getPeerCommentLayerContent($isEditMode, $recordId, $isViewSelfPeerComment=false, $writingId=null, $targetStudentId=null) {
		global $Lang;

		$comment = '';
		if (is_numeric($recordId)){
			$objPeerMarkingData = new libW2PeerMarkingData($recordId);
			$comment = $objPeerMarkingData->getComment();
			$commentLastUpdate = $objPeerMarkingData->getDateModified();
			$commentLastUpdateBy = $objPeerMarkingData->getModifiedBy();
			$lastModifiedDisplay = Get_Last_Modified_Remark($commentLastUpdate, '', $commentLastUpdateBy);
		}
		else if ($isViewSelfPeerComment) {
			$peerMarkingDataInfoAry = $this->getStudentPeerMarkingTargetData($markerUserId=null, $writingId, $markedOnly=null, $targetStudentId);
			$markerUserIdAry = Get_Array_By_Key($peerMarkingDataInfoAry, 'MARKER_USER_ID');

			$libuser = new libuser();
			$markerNameAssoAry = $libuser->getNameWithClassNumber($markerUserIdAry);

			for ($i=0, $i_max=count($peerMarkingDataInfoAry); $i<$i_max; $i++) {
				$_markerUserId = $peerMarkingDataInfoAry[$i]['MARKER_USER_ID'];
				$_comment = trim($peerMarkingDataInfoAry[$i]['COMMENT']);
				$_dateModified = $peerMarkingDataInfoAry[$i]['DATE_MODIFIED'];

				if ($_comment == '') {
					continue;
				}

				$_markerName = $markerNameAssoAry[$_markerUserId];

				$comment .= $_markerName.':';
				$comment .= '<br />';
				$comment .= $_comment;
				$comment .= '<br />';
				$comment .= '<span class="date">'.$Lang['W2']['lastUpdateDate'].': '.$_dateModified.'</span>';
				$comment .= '<br />';
				$comment .= '<br />';
			}

			if ($comment == '') {
				$comment = $Lang['General']['EmptySymbol'];
			}
		}

		$content = '';
		if ($isEditMode) {
			$content .= '<textarea style="width: 95%" class="textbox" rows="7" name="r_peerComment[0]">'.$comment.'</textarea>';
		}
		else{
			$content .= nl2br($comment);
		}

		$html = $this->getPeerCommentLayerContentTemplate();
		$html = str_replace('{{{content}}}', $content, $html);
		$html = str_replace('{{{lastModifiedInfo}}}', $lastModifiedDisplay, $html);

		return $html;
	}

	private function getPeerCommentLayerTemplate() {
		return <<<html
<!-- Teacher Comment layer start -->
<div id="layer_peer_comment01" class="layer_comment">
	<div class="top">
		<div></div>
	</div>
	<div class="mid">
		<div class="right">
			<div id="layer_peer_comment01_content" class="content">
				{{{contentHtml}}}
			</div>
			<!-- button start-->
			<div class="layer_comment_btn">
				{{{buttonHtml}}}
			</div>
			<!-- button end-->
		</div>
	</div>
	<div style="clear: both"></div>
	<div class="bottom">
		<div></div>
	</div>
</div>
<!-- Teacher Comment layer end -->
html;
	}

	private function getPeerCommentLayerContentTemplate() {
		return <<<html
{{{content}}}
<br><span class="date">{{{lastModifiedInfo}}}</span>
html;
	}

	public function getIconNew() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;

		return '<img align="texttop" width="28" height="15" border="0" src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/new.gif">';
	}
	public function updateStepStudentApprovalStatus($stepID , $studentID, $approvalStatus){
		global $intranet_db,$w2_cfg;
		if($approvalStatus==$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]){
			$sql = 'update '.$intranet_db.'.W2_STEP_STUDENT set APPROVAL_STATUS = \''.$approvalStatus.'\',STEP_STATUS = '.$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["draft"].' where STEP_ID = \''.$stepID.'\' and USER_ID = \''.$studentID."'";
		}else{
			$sql = 'update '.$intranet_db.'.W2_STEP_STUDENT set APPROVAL_STATUS = \''.$approvalStatus.'\' where STEP_ID = \''.$stepID.'\' and USER_ID = \''.$studentID."'";
		}
		$objDB = new libdb();
		return $objDB->db_db_query($sql);
	}
	public function getMarkDisplay($fullMark=W2_MARKING_FULL,$passMark=W2_MARKING_PASS,$lowestMark=W2_MARKING_LOWEST,$teacherWeight=W2_MARKING_TEACHER_WEIGHT,$selfWeight=W2_MARKING_SELF_WEIGHT,$peerWeight=W2_MARKING_PEER_WEIGHT){
		global $Lang;
		$str = $Lang['W2']['markUsing']." : ".$Lang['W2']['mark'];
		$str .= "(".$Lang['W2']['passmark'].": ".$passMark.", ".$Lang['W2']['fullmark'].": ".$fullMark.", ".$Lang['W2']['lowestmark'].": ".$lowestMark.")";
		$str .= "<br />";
		$str .= $Lang['W2']['marker']." : ";
		$str .= $Lang['W2']['teacher']." (".(is_null($teacherWeight)?$Lang['W2']['na']:$teacherWeight)."), ".$Lang['W2']['peer']." (".(is_null($peerWeight)?$Lang['W2']['na']:$peerWeight).")";
		return $str;
	}
	/**
	* Show if the user has enter to W2 today
	*
	* @return	A boolean value
	*/
	public function isFirstVisitW2Today($userID){
		if($_SESSION['visitedW2Today']){
			return false;
		}

		$sql = "
			Select
				count(*) As visitTime
			From
				INTRANET_LOGIN_SESSION
			Where
				DATE(StartTime) = CURDATE()
			And
				UserID='{$userID}'
			";
		$rs = $this->objDb->returnResultSet($sql);

		$_SESSION['visitedW2Today'] = true;
		$firstVisitW2 = ($rs[0]['visitTime'] <=1);

		return $firstVisitW2;
	}
	public function getStep2ReferenceHTML($cid,$referenceAry,$can_edit=true){
		global $Lang,$w2_cfg,$PATH_WRT_ROOT,$LAYOUT_SKIN,$linterface;
		$referenceType = !empty($referenceAry['referenceType'])?$referenceAry['referenceType']:'normal';
		$referenceTopic = !empty($referenceAry['referenceTopic'])?stripslashes($referenceAry['referenceTopic']):'';
		$referenceImagePath = $PATH_WRT_ROOT.'file/w2/reference/cid_'.$cid.'/';
		$html = '';
		if($can_edit){
			$html .= '<div class="content_input">'.$Lang['W2']['referenceTitle'].': <input type="text" name="topic" id="topic" size="80" maxlength="50" value="'.$referenceTopic.'" /></div>';
			$html .= $linterface->Get_Form_Warning_Msg('r_ReferenceTopic_warningDiv', $Lang['W2']['jsWarningAry']['topicCannotBeBlank'], 'warningDiv_2');
			$html .= '<div class="content_input">'.$Lang['W2']['referenceType'].': ';
			foreach($w2_cfg['contentInput']['step2ReferenceType'] as $_type){
				$html .= '<input type="radio" name="type" id="type_'.$_type.'" value="'.$_type.'" onclick="switchReferenceType(\''.$_type.'\')" '.($referenceType==$_type?'checked':'').'/>';
				$html .= '<label for="type_'.$_type.'">&nbsp;'.$Lang['W2']['step2ReferenceTypeAry'][$_type].'</label>';
				$html .= '&nbsp;';
			}
			$html .= '</div>';
			$html .= $linterface->Get_Form_Warning_Msg('r_ReferenceType_warningDiv', $Lang['W2']['jsWarningAry']['pleaseSelectReferenceType'], 'warningDiv_2');
			##Normal Start##
			$h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addReference(\'normal\')">'.$Lang['W2']['uploadFileMore'].'</a>';
			$html .= '<div class="div_referenceType" id="div_referenceType_normal" '.($referenceType=='normal'?'':'style="display:none;"').'>';
					$html .= '<table id="w2_step2ReferenceTable_normal" class="thumbnail" style="width:700px;">';
						$html .= '<tr>';
							$html .= '<td>&nbsp;</td>';
							$html .= '<td width="250">&nbsp;'.$Lang['W2']['imageFile'].'</td>';
							$html .= '<td width="200">&nbsp;'.$Lang['W2']['imageCaption'].'</td>';
							$html .= '<td>&nbsp;</td>';
						$html .= '</tr>';

				$referenceData = $referenceAry['referenceData']['normalData1'];
				$dataCnt = count($referenceData);
				if($dataCnt>0){
					$i=0;
					foreach($referenceData["itemAry"] as $_dataAry){
						$_imagePath = $referenceImagePath.$_dataAry['referencePath'];
						$_caption = !empty($_dataAry['referenceCaption'])?stripslashes($_dataAry['referenceCaption']):'';
						$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeReferenceImage(\'normal_'.$i.'\')">&nbsp;</a>';
						$html .= '<tr id="div_normalImage_'.$i.'">';
							$html .= '<td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
							$html .= '<td class="photo">';
							$html .= '<img src="'.$_imagePath.'">';
							$html .= '</td>';
							$html .= '<td>';
							$html .= $_caption;
							//	$html .= '<input type="text" name="r_normalImage_Caption_'.$i.'" id="r_normalImage_Caption_'.$i.'" style="width:250px" value="'.$_caption.'"/>';
							$html .= '</td>';
							$html .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
						$html .= '</tr>';
						$html .= '<tr><td colspan="4" height="5"></td></tr>';
						$i++;

					}
				}else{
					$defaultAttachmentCnt = $w2_cfg["DB_W2_STEP_STUDENT_FILE_STUDENTATTACHMENT"]["defaultDisplayNum"];
					for($i=0;$i<$defaultAttachmentCnt;$i++){
							$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeStepItemBox(\'div_normalImage_'.$i.'\')">&nbsp;</a>';

							$html .= '<tr id="div_normalImage_'.$i.'">';
								$html .= '<td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
								$html .= '<td><input type="file" class="normalImage" name="r_normalImage_'.$i.'" id="r_normalImage_'.$i.'" style="width:190px"/></td>';
								$html .= '<td>';
									$html .= '<input type="text" name="r_normalImage_Caption_'.$i.'" id="r_normalImage_Caption_'.$i.'" style="width:250px" value=""/>';
								$html .= '</td>';
								$html .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
							$html .= '</tr>';
					}
				}

				$html .= '<tr id="tr_normal_add"><td colspan="4">';
				$html .= '<div style="text-align:left;">'.$linterface->Get_Form_Warning_Msg('r_ReferenceNormalImage1_warningDiv', $Lang['W2']['jsWarningAry']['wrongImageFormat'], 'warningDiv_2').'</div>';
				$html .= '<div class="Content_tool_190">'.$h_addMoreAction.'</div>';
				$html .= '</td></tr>';
				$html .= '</table>';
				$html .= '<input type ="hidden" name="reference_normal_counter" id="reference_normal_counter" value="'.$i.'">';

			$html .= '</div>';
			##Normal End##
			##Compare Start##
			$html .= '<div class="div_referenceType" id="div_referenceType_compare" '.($referenceType=='compare'?'':'style="display:none;"').'>';
				###Compare 1 Start###
					$referenceData = $referenceAry['referenceData']['compareData1'];
					$_title = !empty($referenceData['itemTitle'])?stripslashes($referenceData['itemTitle']):'';
					$dataCnt = count($referenceData);
					$h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addReference(\'compare1\')">'.$Lang['W2']['uploadFileMore'].'</a>';
					$html .= '<div style="overflow-x:hidden;">';
					$html .= '<table id="w2_step2ReferenceTable_compare1" class="thumbnail" style="width:100%">';
						$html .= '<tr>';
							$html .= '<td colspan="4" align="left">&nbsp;'.$Lang['W2']['data1Title'].': <input type="text" name="compare1_title" id="compare1_title" size="30" maxlength="50" value="'.$_title.'" /></td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>&nbsp;</td>';
							$html .= '<td width="200">&nbsp;'.$Lang['W2']['imageFile'].'</td>';
							$html .= '<td width="200">&nbsp;'.$Lang['W2']['imageCaption'].'</td>';
							$html .= '<td>&nbsp;</td>';
						$html .= '</tr>';

				if($dataCnt>0){
							$i=0;
							foreach($referenceData["itemAry"] as $_value){
								$_imagePath = $referenceImagePath.$_value['referencePath'];
								$_caption = !empty($_value['referenceCaption'])?stripslashes($_value['referenceCaption']):'';
								$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeReferenceImage(\'compare1_'.$i.'\')">&nbsp;</a>';
								$html .= '<tr id="div_compare1Image_'.$i.'">';
									$html .= '<td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
									$html .= '<td class="photo">';
									$html .= '<img src="'.$_imagePath.'">';
									$html .= '</td>';
									$html .= '<td>';
									$html .= $_caption;
									//	$html .= '<input type="text" name="r_compare1Image_Caption_'.$i.'" id="r_compare1Image_Caption_'.$i.'" style="width:250px" value="'.$_caption.'"/>';
									$html .= '</td>';
									$html .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
								$html .= '</tr>';
								$html .= '<tr><td colspan="4" height="5"></td></tr>';
								$i++;
							}
						}else{
							$defaultAttachmentCnt = $w2_cfg["DB_W2_STEP_STUDENT_FILE_STUDENTATTACHMENT"]["defaultDisplayNum"];
							for($i=0;$i<$defaultAttachmentCnt;$i++){
									$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeStepItemBox(\'div_compare1Image_'.$i.'\')">&nbsp;</a>';

									$html .= '<tr id="div_compare1Image_'.$i.'">';
										$html .= '<td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
										$html .= '<td><input type="file" class="compareImage" name="r_compare1Image_'.$i.'" id="r_compare1Image_'.$i.'" style="width:190px"/></td>';
										$html .= '<td>';
											$html .= '<input type="text" name="r_compare1Image_Caption_'.$i.'" id="r_compare1Image_Caption_'.$i.'" style="width:250px" value=""/>';
										$html .= '</td>';
										$html .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
									$html .= '</tr>';
							}
						}
						$html .= '<tr id="tr_compare1_add"><td colspan="4">';
						$html .= '<div style="text-align:left;">'.$linterface->Get_Form_Warning_Msg('r_ReferenceCompareImage1_warningDiv', $Lang['W2']['jsWarningAry']['wrongImageFormat'], 'warningDiv_2').'</div>';
						$html .= '<div class="Content_tool_190">'.$h_addMoreAction.'</div>';
						$html .= '</td></tr>';
					$html .= '</table>';
					$html .= '<input type ="hidden" name="reference_compare1_counter" id="reference_compare1_counter" value="'.$i.'">';
					$html .= '</div>';
				###Compare 1 End###
				$html .= '<br style="clear:both;">';
				###Compare 2 Start###
					$referenceData = $referenceAry['referenceData']['compareData2'];
					$_title = !empty($referenceData['itemTitle'])?stripslashes($referenceData['itemTitle']):'';
					$dataCnt = count($referenceData);
					$h_addMoreAction = '<a href="javaScript:void(0)" class="new" OnClick="addReference(\'compare2\')">'.$Lang['W2']['uploadFileMore'].'</a>';
					$html .= '<div style="overflow-x:hidden;">';
					$html .= '<table id="w2_step2ReferenceTable_compare2" class="thumbnail" style="width:100%">';
						$html .= '<tr>';
							$html .= '<td colspan="4" align="left">&nbsp;'.$Lang['W2']['data2Title'].': <input type="text" name="compare2_title" id="compare2_title" size="30" maxlength="50" value="'.$_title.'" /></td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>&nbsp;</td>';
							$html .= '<td width="200" >&nbsp;'.$Lang['W2']['imageFile'].'</td>';
							$html .= '<td width="200">&nbsp;'.$Lang['W2']['imageCaption'].'</td>';
							$html .= '<td>&nbsp;</td>';
						$html .= '</tr>';

				if($dataCnt>0){
							$i=0;
							foreach($referenceData["itemAry"] as $_value){
								$_imagePath = $referenceImagePath.$_value['referencePath'];
								$_caption = !empty($_value['referenceCaption'])?stripslashes($_value['referenceCaption']):'';
								$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeReferenceImage(\'compare2_'.$i.'\')">&nbsp;</a>';
								$html .= '<tr id="div_compare2Image_'.$i.'">';
									$html .= '<td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
									$html .= '<td class="photo">';
									$html .= '<img src="'.$_imagePath.'">';
									$html .= '</td>';
									$html .= '<td>';
									$html .= $_caption;
									//	$html .= '<input type="text" name="r_compare2Image_Caption_'.$i.'" id="r_compare2Image_Caption_'.$i.'" style="width:250px" value="'.$_caption.'"/>';
									$html .= '</td>';
									$html .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
								$html .= '</tr>';
								$html .= '<tr><td colspan="4" height="5"></td></tr>';
								$i++;
							}
						}else{
							$defaultAttachmentCnt = $w2_cfg["DB_W2_STEP_STUDENT_FILE_STUDENTATTACHMENT"]["defaultDisplayNum"];
							for($i=0;$i<$defaultAttachmentCnt;$i++){
									$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeStepItemBox(\'div_compare2Image_'.$i.'\')">&nbsp;</a>';

									$html .= '<tr id="div_compare2Image_'.$i.'">';
										$html .= '<td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
										$html .= '<td><input type="file" class="compareImage" name="r_compare2Image_'.$i.'" id="r_compare2Image_'.$i.'" style="width:190px"/></td>';
										$html .= '<td>';
											$html .= '<input type="text" name="r_compare2Image_Caption_'.$i.'" id="r_compare2Image_Caption_'.$i.'" style="width:250px" value=""/>';
										$html .= '</td>';
										$html .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
									$html .= '</tr>';
							}
						}
						$html .= '<tr id="tr_compare2_add"><td colspan="4">';
						$html .= '<div style="text-align:left;">'.$linterface->Get_Form_Warning_Msg('r_ReferenceCompareImage2_warningDiv', $Lang['W2']['jsWarningAry']['wrongImageFormat'], 'warningDiv_2').'</div>';
						$html .= '<div class="Content_tool_190">'.$h_addMoreAction.'</div>';
						$html .= '</td></tr>';
					$html .= '</table>';
					$html .= '<input type ="hidden" name="reference_compare2_counter" id="reference_compare2_counter" value="'.$i.'">';
					$html .= '</div>';
				###Compare 2 End###
			$html .= '</div>';
			##Compare End##
		}else{//view only
			$html .= '<table cellspacing="0" cellpadding="0">';
				$html .= '<tr><td align="center">';
					$html .= '<table class="thumbnail" align="center" style="width:648px">';
					if($referenceType=='compare'){
						### Prepare Data
						$data1 = $referenceAry['referenceData']['compareData1'];
						$data1Title = $data1['itemTitle'];
						$data1Cnt = count($data1);
						$data2 = $referenceAry['referenceData']['compareData2'];
						$data2Cnt = count($data2);
						$data2Title = $data1['itemTitle'];
						$row = max($data1Cnt,$data2Cnt);

						$html .= '<tr>';
							$html .= '<td style="text-align:center; width:300px">'.stripslashes($data1Title).'</td>';
							$html .= '<td style="width:48px">&nbsp;</td>';
							$html .= '<td style="text-align:center; width:300px">'.stripslashes($data2Title).'</td>';
						$html .= '</tr>';
						$html .= '<tr><td colspan="3">&nbsp;</td></tr>';

						for($i=0;$i<$row;$i++){
								$_data1Ary = $data1["itemAry"]["referenceItem".$i];
								$_data2Ary = $data2["itemAry"]["referenceItem".$i];
								$_image1Path = !empty($_data1Ary['referencePath'])?$referenceImagePath.$_data1Ary['referencePath']:'';
								$_image2Path = !empty($_data2Ary['referencePath'])?$referenceImagePath.$_data2Ary['referencePath']:'';
								$_image1Caption = !empty($_data1Ary['referencePath'])&&!empty($_data1Ary['referenceCaption'])?$_data1Ary['referenceCaption']:'';
								$_image2Caption = !empty($_data2Ary['referencePath'])&&!empty($_data2Ary['referenceCaption'])?$_data2Ary['referenceCaption']:'';
								$html .= '<tr>';
									$html .= '<td class="photo">'.(!empty($_image1Path)?'<img src="'.$_image1Path.'">':'&nbsp;').'</td>';
									$html .= '<td>&nbsp;</td>';
									$html .= (!empty($_image2Path)?'<td class="photo"><img src="'.$_image2Path.'">':'<td>&nbsp;').'</td>';
								$html .= '</tr>';
								$html .= '<tr>';
									$html .= '<td>'.stripslashes($_image1Caption).'</td>';
									$html .= '<td>&nbsp;</td>';
									$html .= '<td>'.stripslashes($_image2Caption).'</td>';
								$html .= '</tr>';
						}
					}elseif($referenceType=='normal'){
						$referenceData = $referenceAry['referenceData']['normalData1'];
						$dataCnt = count($referenceData);
						$i=0;

						foreach((array)$referenceData["itemAry"] as $_key => $_data){
							if($i%2==0){
								$_data1Ary = $_data;
								$_data2Ary = $referenceData["itemAry"]["referenceItem".($i+1)];
								$_image1Path = !empty($_data1Ary['referencePath'])?$referenceImagePath.$_data1Ary['referencePath']:'';
								$_image2Path = !empty($_data2Ary['referencePath'])?$referenceImagePath.$_data2Ary['referencePath']:'';
								$_image1Caption = !empty($_data1Ary['referencePath'])&&!empty($_data1Ary['referenceCaption'])?$_data1Ary['referenceCaption']:'';
								$_image2Caption = !empty($_data2Ary['referencePath'])&&!empty($_data2Ary['referenceCaption'])?$_data2Ary['referenceCaption']:'';
								$html .= '<tr>';
									$html .= '<td class="photo">'.(!empty($_image1Path)?'<img src="'.$_image1Path.'">':'&nbsp;').'</td>';
									$html .= '<td>&nbsp;</td>';
									$html .= (!empty($_image2Path)?'<td class="photo"><img src="'.$_image2Path.'">':'<td>&nbsp;').'</td>';
								$html .= '</tr>';
								$html .= '<tr>';
									$html .= '<td>'.stripslashes($_image1Caption).'</td>';
									$html .= '<td>&nbsp;</td>';
									$html .= '<td>'.stripslashes($_image2Caption).'</td>';
								$html .= '</tr>';
							}
							$i++;
						}
					}

	$html .= '</table>';
$html .= '</td></tr>';
$html .= '</table>';
		}

		return $html;
	}
	function getSciencePresetCategoryVocabulary($cid,$infoboxCodeAry,$myKeywordId){
		global $w2_cfg,$Lang;
		$refPresetAry = array();
		for($i=0;$i<count($infoboxCodeAry);$i++){
			$_infoboxCode = $infoboxCodeAry[$i];
			$refPresetAry[$_infoboxCode]['categoryAry'][0] = array(
						'categoryTitle'=>$Lang['W2']['myKeywords'],
						'categoryCode'=>$_infoboxCode.'_cat1',
						'categoryCssSet'=>4,
						'categoryType'=>'stepAns',
						'categoryRefAnsCodeAry'=>array($myKeywordId)
				);
			$inputCode = 'infobox_'.$cid.'_'.$i;
			$sql = "
				SELECT
					REF_CATEGORY_CODE,
					INFOBOX_CODE,
					TITLE,
					CSS_SET
				FROM
					W2_CONTENT_REF_CATEGORY
				WHERE
					cid = '".$cid."'
				AND
					INFOBOX_CODE = '".$inputCode."'
				AND
					DELETE_STATUS = '".$w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["active"]."'
			";
			$result = $this->objDb->returnArray($sql);
			$cnt = count($result);
			for($j=1;$j<=$cnt;$j++){
				list($cat_code,$infobox_code,$title,$css) = $result[$j-1];
				$_catCode = $_infoboxCode.'_cat'.($j+1);
				$refPresetAry[$_infoboxCode]['categoryAry'][$j]['categoryTitle'] = $title;
				$refPresetAry[$_infoboxCode]['categoryAry'][$j]['categoryCode'] = $_catCode;
				$refPresetAry[$_infoboxCode]['categoryAry'][$j]['categoryCssSet'] = $css;
				$refPresetAry[$_infoboxCode]['categoryAry'][$j]['categoryType'] = 'default';
				$refPresetAry[$_infoboxCode]['categoryAry'][$j]['itemAry']	= array();
				$sql = "
					SELECT
						TITLE
					FROM
						W2_CONTENT_REF_ITEM
					WHERE
						cid = '".$cid."'
					AND
						REF_CATEGORY_CODE = '".$cat_code."'
					AND
						INFOBOX_CODE = '".$inputCode."'
					AND
						DELETE_STATUS = '".$w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["active"]."'
				";
				$item_result = $this->objDb->returnArray($sql);
				$iCnt = count($item_result);
				for($k=0;$k<$iCnt;$k++){
					$__title = $item_result[$k]['TITLE'];
					$__itemCode = 'item'.str_pad($k+1,  2, "0", STR_PAD_LEFT);
					$refPresetAry[$_infoboxCode]['categoryAry'][$j]['itemAry'][] = array('itemTitle'=>$__title,'itemType'=>'default','itemCode'=>$__itemCode);
				}
			}
		}




		return $refPresetAry;
	}

	public function insertMarkingScoreToGSGame($passScoreToGSRecord, $r_stepId,$r_studentId,$step_handin_code,$version, $r_mark){
		global $Lang,$w2_thisUserID;
		if(!$passScoreToGSRecord) return false;

		$sql = " SELECT * FROM WS_GAME_SCORE_LOG WHERE FunctionType = 'marking' " .
				" AND ScoreType = '$step_handin_code' " .
				" AND Version = '$version' AND UserID = '$r_studentId' ";
		$result = $this->objDb->returnArray($sql);

		if(sizeof($result)){
			return false;
		}

		if($r_mark == (int) $r_mark){
			$sql = " INSERT INTO WS_GAME_SCORE_LOG (FunctionType, ScoreType, Version, UserID, InputBy, Score, InputDate)" .
					" VALUES ('marking','$step_handin_code', '$version', '$r_studentId', '$w2_thisUserID', '$r_mark', NOW() ) ";
			return $this->objDb->db_db_query($sql);
		}
		return false;
	}

	public function insertSharingScoreToGSGame($r_stepId,$r_studentId,$step_handin_code){
		global $Lang,$w2_thisUserID;

		$sql = " SELECT * FROM W2_STEP_HANDIN WHERE STEP_ID = '$r_stepId' AND USER_ID = '$r_studentId' ";
		$result = current($this->objDb->returnArray($sql));
		$version = $result["VERSION"];
		$mark = $result["MARK"];

		$sql = " SELECT * FROM WS_GAME_SCORE_LOG WHERE FunctionType = 'sharing' " .
				" AND ScoreType = '$step_handin_code' " .
				" AND Version = '$version' AND UserID = '$r_studentId' ";
		$result = $this->objDb->returnArray($sql);

		if(sizeof($result)){
			return false;
		}

		if($mark == (int) $mark){
			$sql = " INSERT INTO WS_GAME_SCORE_LOG (FunctionType, ScoreType, Version, UserID, InputBy, Score, InputDate)" .
					" VALUES ('sharing','$step_handin_code', '$version', '$r_studentId', '$w2_thisUserID', '$mark', NOW() ) ";
			return $this->objDb->db_db_query($sql);
		}
		return false;
	}
}
?>