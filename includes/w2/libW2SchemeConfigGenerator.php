<?php
include_once('w2ShareContentConfig.inc.php');
class libW2SchemeConfigGenerator
{
	function generateConfigFile($filePath,$schoolCode)
	{
		GLOBAL $w2_cfg;
		try
		{

			$fh = fopen($filePath, 'w') or die("can't open file");
			$stringData = $this->getConfigContent($schoolCode);

			fwrite($fh, $stringData);
			fclose($fh);
		}
		catch (Exception $e) 
		{
		    echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	function getConfigContent($schoolCode)
	{
		GLOBAL $w2_cfg;
		$r_contentCodeList = $w2_cfg["contentCodeList"];
		
		$returnStr ="<?php"."\r\n";
		foreach( (array)$r_contentCodeList as $r_contentCode)
		{
			$schemeDetailList =$this->getConfigUpdateData($r_contentCode);
			
			$returnStr.= '$w2_cfg_contentSetting[\''.$schoolCode."_".$r_contentCode."'] = array("."\r\n";
			foreach( (array)$schemeDetailList as $schemeDetail)
			{
				

				$returnStr .="'". $schoolCode."_scheme${schemeDetail[schemeNum]}' => array("."\r\n";
				
				// Every english scheme has an extra field type_super, which indicates passge difficulty
				if($r_contentCode=='eng')
				{
					$returnStr .= " 'type_super' => '".htmlspecialchars($schemeDetail['level'],ENT_QUOTES)."' ,"."\n\r";
				}
				$returnStr .=	" 'type' =>'".htmlspecialchars($schemeDetail['category'],ENT_QUOTES)."', "."\n\r";
				$returnStr .=	" 'name' =>'".htmlspecialchars($schemeDetail['topicName'],ENT_QUOTES)."', "."\n\r";
				$returnStr .=	" 'introduction' =>'".htmlspecialchars($schemeDetail['topicIntro'],ENT_QUOTES)."', "."\n\r";
				$returnStr .=	" 'dateModified' =>'".htmlspecialchars($schemeDetail['dateModified'],ENT_QUOTES)."', "."\n\r";
				$returnStr .=	" 'fullDateModified' =>'".htmlspecialchars($schemeDetail['fullDateModified'],ENT_QUOTES)."', "."\n\r";
				$returnStr .=	" 'schoolCode' =>'".htmlspecialchars($schemeDetail['schoolCode'],ENT_QUOTES)."', "."\n\r";
				$returnStr .=	"), "."\n\r";
			}
			$returnStr .=");		"."\r\n";	
		}
		$returnStr .="?>"."\r\n";
		
		return $returnStr;
	}

	function getConfigUpdateData($r_contentCode) 
	{	
		$objDB = new libdb();
		$sql = "Select
			level, category, topicName, topicIntro,
			DATE_FORMAT(dateModified, '%Y-%m-%d') as dateModified,
			dateModified as fullDateModified,
			schemeNum, schoolCode
			From W2_CONTENT_DATA 
			Where r_contentCode = '$r_contentCode' And status=".W2_GENERATED.";"; 
		return $objDB->returnResultSet($sql);
	}
	
	
}

?>