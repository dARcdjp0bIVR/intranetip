<?php
abstract class libW2RefCategory {
	
	private $isEditable;
	private $infoboxCode;
	
	private $refCategoryCode;
	private $title;
	private $cssSet;
	private $sourceType;
	private $displayMode;
	
	protected $objRefItemAry;
	
	protected $objDb;
	
	public function __construct() {
		$this->objDb = new libdb();
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setIsEditable($bool) {
    	$this->isEditable = $bool;
    }
    protected function getIsEditable() {
    	return $this->isEditable;
    }
    
    public function setInfoboxCode($str) {
    	$this->infoboxCode = $str;
    }
    protected function getInfoboxCode() {
    	return $this->infoboxCode;
    }
    
    public function setRefCategoryCode($str) {
    	$this->refCategoryCode = $str;
    }
    protected function getRefCategoryCode() {
    	return $this->refCategoryCode;
    }
    
    public function setTitle($str) {
    	$this->title = $str;
    }
    public function getTitle() {
    	return $this->title;
    }
    
    public function setCssSet($int) {
    	$this->cssSet = $int;
    }
    public function getCssSet() {
    	return $this->cssSet;
    }
    
    protected function setSourceType($str) {
    	$this->sourceType = $str;
    }
    protected function getSourceType() {
    	return $this->sourceType;
    }
    
    private function setObjRefItemAry($arr) {
    	$this->objRefItemAry = $arr;
    }
    public function getObjRefItemAry() {
    	return $this->objRefItemAry;
    }
    
    public function setDisplayMode($str) {
    	$this->displayMode = $str;
    }
    protected function getDisplayMode() {
    	return $this->displayMode;
    }
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    /**
	* Load the Reference Item to protected element $objRefItemAry
	* @owner	Ivan (20120216)
	* @return	null
	*/
	abstract public function loadObjRefItemAry();
	
	/**
	* Return the html of the category display for manage mode
	* @owner	Ivan (20120216)
	* @return	String of html of the category display
	*/
	abstract public function returnDisplayHtmlManageMode();
	
	/**
	* Add Reference Item Object to protected element $objRefItemAry
	* @owner	Ivan (20120216)
	* @return	null
	*/  
    protected function addObjRefItem($objRefItem) {
    	$this->objRefItemAry[] = $objRefItem;
    }
    
    /**
	* Return the html of the display
	* @owner	Ivan (20120216)
	* @return	String of html of the category display
	*/
    public function returnDisplayHtml() {
    	global $w2_cfg;
    	
    	switch ($this->getDisplayMode()) {
    		case $w2_cfg["refDisplayMode"]["input"]:
    		case $w2_cfg["refDisplayMode"]["manage"]:
    			$html = $this->returnDisplayHtmlManageMode();
    			break;
    		case $w2_cfg["refDisplayMode"]["view"]:
    			$html = $this->returnDisplayHtmlViewMode();
    			break;
    	}
    	
    	return $html;
    }
    
    /**
	* Return the highlight css of this Reference Category
	* @owner	Ivan (20120216)
	* @return	String of the name of the css
	*/
    public function returnHighlightCss() {
    	return 'highlight_'.str_pad($this->getCssSet(), 2, "0", STR_PAD_LEFT);
    }
    
    /**
	* Return the html of the category display template of manage mode
	* @owner	Ivan (20120216)
	* @return	String of the html of the category display template of manage mode
	*/
    protected function returnDivHtmlTemplate() {
    	return <<<html
<div onmouseout="MM_showHideLayers('{{{toolbarDivId}}}','','hide');this.className='gloss_item_heading {{{highlightCss}}}'" onmouseover="MM_showHideLayers('{{{toolbarDivId}}}','','show');this.className='gloss_item_heading_over {{{highlightCss}}}'" class="gloss_item_heading {{{highlightCss}}}">
    <h1>{{{categoryTitle}}}</h1>
    <div id="{{{toolbarDivId}}}" class="gloss_tool">
        {{{categoryToolbarButtonHtml}}}
    </div>
    <p class="spacer"></p>
</div>
{{{refItemHtml}}}
{{{addItemButtonHtml}}}
<br />
html;
    }
    
    /**
	* Return the html of the category display template of view mode
	* @owner	Ivan (20120216)
	* @return	String of the html of the category display template of view mode
	*/
    protected function returnViewModeHtmlTemplate() {
    	return <<<html
<span class="subtitle {{{highlightCss}}} use_title">{{{categoryTitle}}}</span><ul>{{{refItemHtml}}}</ul>
html;
    }
    
    /**
	* Return the html of the category display of view mode
	* @owner	Ivan (20120216)
	* @return	String of the html of the category display of view mode
	*/
    protected function returnDisplayHtmlViewMode() {
    	$html = $this->returnViewModeHtmlTemplate();
    	$html = str_replace('{{{highlightCss}}}', $this->returnHighlightCss(), $html);
    	$html = str_replace('{{{categoryTitle}}}', $this->getTitle(), $html);
    	$html = str_replace('{{{refItemHtml}}}', $this->returnItemDisplayHtml(), $html);
    	
    	return $html;
    }
    
    /**
	* Return the unique toolbar div id
	* @owner	Ivan (20120216)
	* @return	String of the div id
	*/
    protected function returnToolbarDivID() {
    	return 'gloss_tool_'.$this->getInfoboxCode().'_category_'.$this->getRefCategoryCode();
    }
    
    /**
	* Return the html display for the reference item(s) of this category
	* @owner	Ivan (20120216)
	* @return	String of html display for the reference item(s) of this category
	*/
    protected function returnItemDisplayHtml() {
    	$objRefItemAry = $this->getObjRefItemAry();

    	$html = '';
    	for ($i=0, $i_max=count($objRefItemAry); $i<$i_max; $i++) {
    		$_objRefItem = $objRefItemAry[$i];
    		
    		$_objRefItem->setDisplayMode($this->getDisplayMode());
    		$html .= $_objRefItem->returnDisplayHtml();
    	}
    	
    	return $html;
    }
    
    /**
	* Return the id of the reference item of this category
	* @owner	Ivan (20120216)
	* @return	Array of id of reference item
	*/
    protected function returnRefItemIdAry() {
    	global $intranet_db, $w2_cfg;
    	if($this->getSourceType()==$w2_cfg["refItemObjectSource"]["contentInput"]){
 	    	$W2_REF_ITEM = $intranet_db.'.W2_CONTENT_REF_ITEM';	
 	    	$sql = "Select
						REF_ITEM_ID
				From
						$W2_REF_ITEM
				Where
						REF_CATEGORY_CODE = '".$this->getRefCategoryCode()."'
						And DELETE_STATUS = '".$w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["active"]."'
				Order By
						TITLE
				";
    	}else{
    		$W2_REF_ITEM = $intranet_db.'.W2_REF_ITEM';
	    	$sql = "Select
							REF_ITEM_ID
					From
							$W2_REF_ITEM
					Where
							REF_CATEGORY_TYPE = '".$this->getSourceType()."'
							And REF_CATEGORY_CODE = '".$this->getRefCategoryCode()."'
							And DELETE_STATUS = '".$w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["active"]."'
					Order By
							TITLE
					";    		
    	}
		return Get_Array_By_Key($this->objDb->returnResultSet($sql), 'REF_ITEM_ID');
    }
}
?>