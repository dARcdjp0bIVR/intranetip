<?
// using: 

include_once($intranet_root."/includes/w2/libW2RefItem.php");

class libW2RefItemFactory {
	public function __construct() {
		
    }
   
    public static function createObject($sourceType, $extraParam=null) {
    	global $w2_cfg, $intranet_root;
	    	
		$sourceType = ucfirst($sourceType) ;

		$objectClass = 'libW2RefItem'.$sourceType;
		$libFile = $intranet_root.'/includes/w2/'.$objectClass.'.php';
		if(file_exists($libFile)){
			include_once($libFile);
			return new $objectClass($extraParam);
		}else{
			return null;
		}
    }
}
?>