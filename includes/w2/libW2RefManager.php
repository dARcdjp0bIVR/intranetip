<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2RefCategoryFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2RefItemFactory.php");

class libW2RefManager {
	
	private $isEditable;
	private $infoboxCode;
	private $writingStudentId;
	private $presetCategoryAry;
	private $stepAnsAry;
	private $displayMode;
	private $objRefCategoryAry;
	
	private $objDb;
	private $objW2;
	
	public function __construct() {
		$this->objDb = new libdb();
		$this->objW2 = new libw2();
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setIsEditable($bool) {
    	$this->isEditable = $bool;
    }
    protected function getIsEditable() {
    	return $this->isEditable;
    }
    
    public function setInfoboxCode($str) {
    	$this->infoboxCode = $str;
    }
    protected function getInfoboxCode() {
    	return $this->infoboxCode;
    }
    
    public function setWritingStudentId($int) {
    	$this->writingStudentId = $int;
    }
    private function getWritingStudentId() {
    	return $this->writingStudentId;
    }
    
    public function setPresetCategoryAry($arr) {
    	$this->presetCategoryAry = $arr;
    }
    private function getPresetCategoryAry() {
    	return $this->presetCategoryAry;
    }
    
    public function setStepAnsAry($arr) {
    	$this->stepAnsAry = $arr;
    }
    private function getStepAnsAry() {
    	return $this->stepAnsAry;
    }
    
    public function setDisplayMode($str) {
    	$this->displayMode = $str;
    }
    private function getDisplayMode() {
    	return $this->displayMode;
    }
    
    private function setObjRefCategoryAry($arr) {
    	$this->objRefCategoryAry = $arr;
    }
    private function getObjRefCategoryAry() {
    	return $this->objRefCategoryAry;
    }
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    /**
	* Load the Reference Category and the corresponding Reference Item for this infobox
	* @owner	Ivan (20120216)
	*/
    private function loadAllObjCategoryWithObjItem() {
    	global $w2_cfg;
    	if($this->getDisplayMode()==$w2_cfg["refDisplayMode"]["input"]){
 	    	$objRefCategoryAry = $this->returnAllObjCategoryContentInput();   		
    	}else{
	    	$objCategoryStepAnsAry = $this->returnAllObjCategoryStepAns();
			$objCategoryDefaultAry = $this->returnAllObjCategoryDefault();
			$objCategorySelfAddAry = $this->returnAllObjCategorySelfAdd();
			
			$objRefCategoryAry = array_merge($objCategoryStepAnsAry, $objCategoryDefaultAry, $objCategorySelfAddAry);    		
    	}

    	
		for ($i=0, $i_max=count($objRefCategoryAry); $i<$i_max; $i++) {
			$objRefCategoryAry[$i]->loadObjRefItemAry();
		}
	
		$this->setObjRefCategoryAry($objRefCategoryAry);
    }
    
    /**
	* Return the array of object Reference Category which is set in the module file by default and is retrieve Reference Item from answer of other Steps
	* @owner	Ivan (20120216)
	* @return	Array of object of Reference Category
	*/
    private function returnAllObjCategoryStepAns() {
    	global $w2_cfg;
    	
    	$presetCategoryAry = $this->returnInfoboxPresetCategoryAry();
    	
    	$returnObjAry = array();
		for ($i=0, $i_max=count($presetCategoryAry); $i<$i_max; $i++) {
			$_categoryTitle 		= $presetCategoryAry[$i]['categoryTitle'];
			$_categoryCode 			= $presetCategoryAry[$i]['categoryCode'];
			$_categoryCssSet 		= $presetCategoryAry[$i]['categoryCssSet'];
			$_categoryType 			= $presetCategoryAry[$i]['categoryType'];
			$_categoryRefAnsCodeAry = $presetCategoryAry[$i]['categoryRefAnsCodeAry'];
			
			if ($_categoryType != $w2_cfg["refCategoryObjectSource"]["stepAns"]) {
				continue;
			}
			
			$_objW2RefCategoryStepAns = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["stepAns"]);
			$_objW2RefCategoryStepAns->setInfoboxCode($this->getInfoboxCode());
			$_objW2RefCategoryStepAns->setTitle($_categoryTitle);
			$_objW2RefCategoryStepAns->setRefCategoryCode($_categoryCode);
			$_objW2RefCategoryStepAns->setCssSet($_categoryCssSet);
			$_objW2RefCategoryStepAns->setRefAnsCodeAry($_categoryRefAnsCodeAry);
			$_objW2RefCategoryStepAns->setStepAnsAry($this->getStepAnsAry());
					
			$returnObjAry[] = $_objW2RefCategoryStepAns;
		}
	
		return $returnObjAry;
    }
    
    /**
	* Return the array of object Reference Category which is set in the module file by default
	* @owner	Ivan (20120216)
	* @return	Array of object of Reference Category
	*/
    private function returnAllObjCategoryDefault() {
    	global $w2_cfg;
    	
    	$presetCategoryAry = $this->returnInfoboxPresetCategoryAry();
    	
    	$returnObjAry = array();
		for ($i=0, $i_max=count($presetCategoryAry); $i<$i_max; $i++) {
			$_categoryTitle 		= $presetCategoryAry[$i]['categoryTitle'];
			$_categoryCode	 		= $presetCategoryAry[$i]['categoryCode'];
			$_categoryCssSet 		= $presetCategoryAry[$i]['categoryCssSet'];
			$_categoryType 			= $presetCategoryAry[$i]['categoryType'];
			$_itemAry				= $presetCategoryAry[$i]['itemAry'];
			
			if ($_categoryType != $w2_cfg["refCategoryObjectSource"]["default"]) {
				continue;
			}
			
			$_objW2RefCategoryDefault = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["default"]);
			$_objW2RefCategoryDefault->setIsEditable($this->getIsEditable());
			$_objW2RefCategoryDefault->setInfoboxCode($this->getInfoboxCode());
			$_objW2RefCategoryDefault->setTitle($_categoryTitle);
			$_objW2RefCategoryDefault->setRefCategoryCode($_categoryCode);
			$_objW2RefCategoryDefault->setCssSet($_categoryCssSet);
			$_objW2RefCategoryDefault->setDefaultItemAry($_itemAry);
			
			$returnObjAry[] = $_objW2RefCategoryDefault;
		}
		
		return $returnObjAry;
    }
    private function returnAllObjCategoryContentInput() {
    	global $w2_cfg;
    	
    	$contentInputRefCategoryIdAry = $this->returnContentInputRefCategoryIdAry();
    	$returnObjAry = array();
    	for ($i=0, $i_max=count($contentInputRefCategoryIdAry); $i<$i_max; $i++) {
    		$_contentInputRefCategoryId = $contentInputRefCategoryIdAry[$i];
    		
    		$_objW2RefCategoryContentInput = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["contentInput"], $_contentInputRefCategoryId);
    		$_objW2RefCategoryContentInput->setIsEditable($this->getIsEditable());
    		
    		$returnObjAry[] = $_objW2RefCategoryContentInput;
    	}
    	
		return $returnObjAry;
    }    
    /**
	* Return the array of object Reference Category which is set by the user
	* @owner	Ivan (20120216)
	* @return	Array of object of Reference Category
	*/
    private function returnAllObjCategorySelfAdd() {
    	global $w2_cfg;
    	
    	$selfAddRefCategoryIdAry = $this->returnSelfAddRefCategoryIdAry();
    	$returnObjAry = array();
    	for ($i=0, $i_max=count($selfAddRefCategoryIdAry); $i<$i_max; $i++) {
    		$_selfAddRefCategoryId = $selfAddRefCategoryIdAry[$i];
    		
    		$_objW2RefCategorySeldAdd = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["selfAdd"], $_selfAddRefCategoryId);
    		$_objW2RefCategorySeldAdd->setIsEditable($this->getIsEditable());
    		
    		$returnObjAry[] = $_objW2RefCategorySeldAdd;
    	}
    	
		return $returnObjAry;
    }
    private function returnContentInputRefCategoryIdAry() {
    	global $intranet_db, $w2_cfg;
    	
    	$W2_REF_CATEGORY = $intranet_db.'.W2_CONTENT_REF_CATEGORY';
    	$sql = "Select
					REF_CATEGORY_ID
				From
					$W2_REF_CATEGORY
				Where
					DELETE_STATUS = '".$w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["active"]."'
					And INFOBOX_CODE = '".$this->getInfoboxCode()."'
				Order By
					TITLE
				";
		return Get_Array_By_Key($this->objDb->returnResultSet($sql), 'REF_CATEGORY_ID');
    }    
    /**
	* Return the IDs of the Reference Category from the DB of this infobox of this student
	* @owner	Ivan (20120216)
	* @return	Array of IDs of Reference Category
	*/
    private function returnSelfAddRefCategoryIdAry() {
    	global $intranet_db, $w2_cfg;
    	
    	$W2_REF_CATEGORY = $intranet_db.'.W2_REF_CATEGORY';
    	$sql = "Select
					REF_CATEGORY_ID
				From
					$W2_REF_CATEGORY
				Where
					DELETE_STATUS = '".$w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["active"]."'
					And INFOBOX_CODE = '".$this->getInfoboxCode()."'
					And WRITING_STUDENT_ID = '".$this->getWritingStudentId()."'
				Order By
					TITLE
				";
		return Get_Array_By_Key($this->objDb->returnResultSet($sql), 'REF_CATEGORY_ID');
    }
    
    /**
	* Return the Reference Category Array of this infobox from the preset array defined in module.php
	* @owner	Ivan (20120216)
	* @return	Array of info of Reference Category
	*/
    private function returnInfoboxPresetCategoryAry() {
    	$presetCategoryAry = $this->getPresetCategoryAry();
    	$infoboxCode = $this->getInfoboxCode();
    	
    	return $presetCategoryAry[$infoboxCode]['categoryAry'];
    }
    
    /**
	* Return the html code for the reference of this infobox
	* @owner	Ivan (20120216)
	* @return	String of html of the reference display
	*/
    public function display() {
    	global $w2_cfg;
    	
    	$this->loadAllObjCategoryWithObjItem();
    	
    	switch ($this->getDisplayMode()) {
      		case $w2_cfg["refDisplayMode"]["input"]:  	
    		case $w2_cfg["refDisplayMode"]["manage"]:
    			$html = $this->returnDisplayHtmlInManageMode();
    			break;
    		case $w2_cfg["refDisplayMode"]["view"]:
    			$html = $this->returnDisplayHtmlInViewMode();
    			break;		
    	}
    	
    	return $html;
    }
    
    /**
	* Return the html code for the reference of this infobox for manage mode
	* @owner	Ivan (20120216)
	* @return	String of html of the reference display
	*/
    private function returnDisplayHtmlInManageMode() {
    	$objRefCategoryAry = $this->getObjRefCategoryAry();
    
    	$html = '';
    	$html .= $this->returnRefHeader();
    	for ($i=0, $i_max=count($objRefCategoryAry); $i<$i_max; $i++) {
    		$_objRefCategory = $objRefCategoryAry[$i];
    		$_objRefCategory->setDisplayMode($this->getDisplayMode());
    		
    		$html .= $_objRefCategory->returnDisplayHtml();
    	}
    	$html .= $this->returnRefFooter();
    	
    	return $html;
    }
    
    /**
	* Return the html code for the reference of this infobox for view mode
	* @owner	Ivan (20120216)
	* @return	String of html of the reference display
	*/
    private function returnDisplayHtmlInViewMode() {
    	$objRefCategoryAry = $this->getObjRefCategoryAry();
    	
    	$html = '';
    	$html .= '<span class="title">Useful points / information:</span>'."\r\n";
    	$html .= '<br>'."\r\n";
    	for ($i=0, $i_max=count($objRefCategoryAry); $i<$i_max; $i++) {
    		$_objRefCategory = $objRefCategoryAry[$i];
    		$_objRefCategory->setDisplayMode($this->getDisplayMode());
    		
    		$html .= $_objRefCategory->returnDisplayHtml();
    	}
    	
    	return $html;
    }
    
    /**
	* Return the html template of the reference header for manage mode
	* @owner	Ivan (20120216)
	* @return	String of html of the header
	*/
    private function returnRefHeader() {
    	global $w2_cfg;
    	
    	$h_addCategoryLink = '';
    	if ($this->getIsEditable()) {
    		$h_addCategoryLink = $this->returnAddCategoryLinkHtml();
    	}
    	
    	return <<<html
<div id="refDiv_{$this->getInfoboxCode()}">
	<div class="ref ref_top">
		<span class="title">Useful points / information:</span><br>
		{$h_addCategoryLink}
	</div>
	<div class="ref">
html;
    }
    
    private function returnAddCategoryLinkHtml() {
    	global $w2_cfg;
    	$libw2 = new libw2();
    	if($this->getDisplayMode()==$w2_cfg["refDisplayMode"]["input"]){
    		$jsFunction = 'loadInputRefCategoryByThickbox';
    	}else{
    		$jsFunction = 'loadEditRefCategoryByThickbox';    		
    	}
    	$paraAssoAry = array();
    	$paraAssoAry['infoboxCode'] = $this->getInfoboxCode();
    	$extraParam = base64_encode($libw2->getUrlParaByAssoAry($paraAssoAry));
    	
    	return <<<html
<span class="table_row_tool_text"><a class="thickbox tool_add" href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=600" onclick="{$jsFunction}('<h1 class=ref>Add new group of useful points</h1>', '{$extraParam}');"><em>&nbsp;</em><span>Add new group</span></a></span><br>
html;
    }
    
    /**
	* Return the html template of the reference footer for manage mode
	* @owner	Ivan (20120216)
	* @return	String of html of the footer
	*/
    private function returnRefFooter() {
    	return <<<html
	</div>
</div>
html;
    }
    
    /**
	* Return the array for JS to highlight the vocab in the essay
	* @owner	Ivan (20120216)
	* @return	Asso array of css and the corresponding vocab
	*/
    public function returnHighlightInfoAry() {
    	$this->loadAllObjCategoryWithObjItem();
    	$objRefCategoryAry = $this->getObjRefCategoryAry();
    	
    	$highlighInfoAry = array();
    	for ($i=0, $i_max=count($objRefCategoryAry); $i<$i_max; $i++) {
    		$_objRefCategory = $objRefCategoryAry[$i];
    		$_css = $_objRefCategory->returnHighlightCss();
    		
    		$_objRefItemAry = $_objRefCategory->getObjRefItemAry();
    		for ($j=0, $j_max=count($_objRefItemAry); $j<$j_max; $j++) {
    			$highlighInfoAry[$_css][] = $_objRefItemAry[$j]->getTitle();
    		}
    	}
    	
    	// for unhighlight just deleted color
    	$allHighlightOptionAry = $this->objW2->getHighlightOptionInfoAry();
    	for ($i=0, $i_max=count($allHighlightOptionAry); $i<$i_max; $i++) {
    		$_css = $allHighlightOptionAry[$i]['css'];
    		
    		if (!isset($highlighInfoAry[$_css])) {
    			$highlighInfoAry[$_css] = array();
    		}
    	}
    	
    	return $highlighInfoAry;
    }
}
?>