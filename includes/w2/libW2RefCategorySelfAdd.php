<?
// using: Siuwan

class libW2RefCategorySelfAdd extends libW2RefCategory {
	
	private $refCategoryId;
	private $writingStudentId;
	private $dateInput;
	private $inputBy;
	private $dateModified;
	private $modifiedBy;
	private $deleteStatus;
	private $deletedBy;
	
	private $objW2;
	
	public function __construct($id=null) {
		global $w2_cfg;
		
		parent::__construct();
		$this->setSourceType($w2_cfg["refCategoryObjectSource"]["selfAdd"]);
		
		$this->objW2 = new libw2();
		
		if(is_numeric($id)){
			$this->setRefCategoryId($id);
			$this->loadRecordFromStorage();
		}
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setRefCategoryId($int) {
    	$this->refCategoryId = $int;
    }
    private function getRefCategoryId() {
    	return $this->refCategoryId;
    }
    
    public function setWritingStudentId($int) {
    	$this->writingStudentId = $int;
    }
    private function getWritingStudentId() {
    	return $this->writingStudentId;
    }
    
    public function setDateInput($str){
		$this->dateInput = $str;
	}
	private function getDateInput(){
		return $this->dateInput;
	}
    
    public function setInputBy($id){
		$this->inputBy = $id;
	}
	private function getInputBy(){
		return $this->inputBy;
	}
	
	public function setDateModified($str){
		$this->dateModified = $str;
	}
	private function getDateModified(){
		return $this->dateModified;
	}
	
	public function setModifiedBy($id){
		$this->modifiedBy = $id;
	}
	private function getModifiedBy(){
		return $this->modifiedBy;
	}
	
	public function setDeleteStatus($val){
		$this->deleteStatus = $val;
	}
	private function getDeleteStatus(){
		return $this->deleteStatus;
	}
	
	public function setDeletedBy($id){
		$this->deletedBy = $id;
	}
	private function getDeletedBy(){
		return $this->deletedBy;
	}
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    private function loadRecordFromStorage(){
		global $intranet_db, $w2_cfg;
		
		$W2_REF_CATEGORY = $intranet_db.'.W2_REF_CATEGORY';
		$sql = "Select
						REF_CATEGORY_ID,
						REF_CATEGORY_CODE,
						WRITING_STUDENT_ID,
						INFOBOX_CODE,
						TITLE,
						CSS_SET,
						DATE_INPUT,
						INPUT_BY,
						DATE_MODIFIED,
						MODIFIED_BY,
						DELETE_STATUS,
						DELETED_BY
				From
						$W2_REF_CATEGORY
				Where
						REF_CATEGORY_ID = '".$this->getRefCategoryId()."'
				";
		$resultAry = $this->objDb->returnResultSet($sql);
		
		$this->setRefCategoryId($resultAry[0]['REF_CATEGORY_ID']);
		$this->setRefCategoryCode($resultAry[0]['REF_CATEGORY_CODE']);
		$this->setWritingStudentId($resultAry[0]['WRITING_STUDENT_ID']);
		$this->setInfoboxCode($resultAry[0]['INFOBOX_CODE']);
		$this->setTitle($resultAry[0]['TITLE']);
		$this->setCssSet($resultAry[0]['CSS_SET']);
		$this->setDateInput($resultAry[0]['DATE_INPUT']);
		$this->setInputBy($resultAry[0]['INPUT_BY']);
		$this->setDateModified($resultAry[0]['DATE_MODIFIED']);
		$this->setModifiedBy($resultAry[0]['MODIFIED_BY']);
		$this->setDeleteStatus($resultAry[0]['DELETE_STATUS']);
		$this->setDeletedBy($resultAry[0]['DELETED_BY']);
	}
	
	public function save(){
		$refCatgoryId = $this->getRefCategoryId();

		$resultID = null;
		if(is_numeric($refCatgoryId) && $refCatgoryId > 0) {
			$refCatgoryId = $this->updateDb();
		}else{
			$refCatgoryId = $this->insertDb();
		}

		$this->setRefCategoryId($refCatgoryId);
		$this->loadRecordFromStorage();

		return $refCatgoryId;
	}
	
	private function insertDb(){
		global $w2_cfg, $intranet_db;

		$this->setDateInput('now()');
		$this->setDateModified('now()');
		$this->setDeleteStatus($w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["active"]);

		$dataAry = array();
		$dataAry["WRITING_STUDENT_ID"]	= $this->objDb->pack_value($this->getWritingStudentId(), "int");
		$dataAry["INFOBOX_CODE"]		= $this->objDb->pack_value($this->getInfoboxCode(), "str");
		$dataAry["TITLE"]				= $this->objDb->pack_value($this->getTitle(), "str");
		$dataAry["CSS_SET"]				= $this->objDb->pack_value($this->getCssSet(), "int");
		$dataAry["DATE_INPUT"]			= $this->objDb->pack_value($this->getDateInput(), "date");
		$dataAry["INPUT_BY"]			= $this->objDb->pack_value($this->getInputBy(), "int");
		$dataAry["DATE_MODIFIED"]		= $this->objDb->pack_value($this->getDateModified(), "date");
		$dataAry["MODIFIED_BY"]			= $this->objDb->pack_value($this->getModifiedBy(), "int");	
		$dataAry["DELETE_STATUS"]		= $this->objDb->pack_value($this->getDeleteStatus(), "date");
		$dataAry["DELETED_BY"]			= $this->objDb->pack_value($this->getDeletedBy(), "int");
		
		$sqlStrAry = $this->objDb->concatFieldValueToSqlStr($dataAry);
		$fieldStr = $sqlStrAry['sqlField'];
		$valueStr = $sqlStrAry['sqlValue'];
			
		# Insert Record
		$successAry = array();
		$W2_REF_CATEGORY = $intranet_db.'.W2_REF_CATEGORY';
		$sql = "Insert Into $W2_REF_CATEGORY ($fieldStr) Values ($valueStr)";
		$successAry['insertRecord'] = $this->objDb->db_db_query($sql);
						
		$recordId = $this->objDb->db_insert_id();
		$this->setRefCategoryId($recordId);
		
		$sql = "Update $W2_REF_CATEGORY set REF_CATEGORY_CODE = REF_CATEGORY_ID Where REF_CATEGORY_ID = '".$this->getRefCategoryId()."'";
		$successAry['updateRefCategoryCode'] = $this->objDb->db_db_query($sql);
			
		$this->loadRecordFromStorage();
		
		return $this->getRefCategoryId();		
	}
	
	private function updateDb(){
		global $intranet_db;
		
		if(is_numeric($this->getRefCategoryId()) && $this->getRefCategoryId() > 0){
			// it is a valid writing id
			//do nothing 
		}else{
			return null;
		}

		$dataAry = array();
		$dataAry["WRITING_STUDENT_ID"]	= $this->objDb->pack_value($this->getWritingStudentId(), "int");
		$dataAry["INFOBOX_CODE"]		= $this->objDb->pack_value($this->getInfoboxCode(), "str");
		$dataAry["TITLE"]				= $this->objDb->pack_value($this->getTitle(), "str");
		$dataAry["CSS_SET"]				= $this->objDb->pack_value($this->getCssSet(), "int");
		$dataAry["DATE_MODIFIED"]		= $this->objDb->pack_value($this->getDateModified(), "date");
		$dataAry["MODIFIED_BY"]			= $this->objDb->pack_value($this->getModifiedBy(), "int");	
		$dataAry["DELETE_STATUS"]		= $this->objDb->pack_value($this->getDeleteStatus(), "date");
		$dataAry["DELETED_BY"]			= $this->objDb->pack_value($this->getDeletedBy(), "int");

		$valueFieldTextAry = array();
		foreach ($dataAry as $field => $value) {
			$valueFieldTextAry[] = $field." = ".$value;
		}
		$valueFieldText = implode(',', $valueFieldTextAry);
		
		$W2_REF_CATEGORY = $intranet_db.'.W2_REF_CATEGORY';
		$sql = "Update $W2_REF_CATEGORY Set ".$valueFieldText." Where REF_CATEGORY_ID = ".$this->getRefCategoryId();
		$this->objDb->db_db_query($sql);
		
		return $this->getRefCategoryId();
	}
	
	public function delete() {
		global $w2_cfg;
		
		$this->setDeleteStatus($w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["isDeleted"]);
		$tempId = $this->updateDB();
		
		// delete reference items of the category
		$refItemIdAry = $this->returnRefItemIdAry();
		$deleteRefItemSuccessAry = array();
		for ($i=0, $i_max=count($refItemIdAry); $i<$i_max; $i++) {
			$_refItemId = $refItemIdAry[$i];
			
			$_objRefItemSelfAdd = libW2RefItemFactory::createObject($w2_cfg["refItemObjectSource"]["selfAdd"], $_refItemId);
			$_tempRefItemId = $_objRefItemSelfAdd->delete();
			
			$deleteRefItemSuccessAry[$_refItemId] = ($_tempRefItemId > 0)? true : false;
		}
		
		return ($tempId > 0 && !in_array(false, $deleteRefItemSuccessAry))? true : false;
	}
    
    public function loadObjRefItemAry(){
    	global $w2_cfg;
    	
		$refItemIdAry = $this->returnRefItemIdAry();
		for ($i=0, $i_max=count($refItemIdAry); $i<$i_max; $i++) {
			$_refItemId = $refItemIdAry[$i];
			
			$_objW2RefItemSelfAdd = libW2RefItemFactory::createObject($w2_cfg["refItemObjectSource"]["selfAdd"], $_refItemId);
			$_objW2RefItemSelfAdd->setIsEditable($this->getIsEditable());
			$this->addObjRefItem($_objW2RefItemSelfAdd);
		}
    }
    
    public function returnDisplayHtmlManageMode() {
    	$toolbarButtonHtml = '';
    	if ($this->getIsEditable()) {
    		$toolbarButtonHtml .= $this->returnEditButtonHtml();
    		$toolbarButtonHtml .= $this->returnDeleteButtonHtml();
    	}
    	
    	$html = $this->returnDivHtmlTemplate();
    	$html = str_replace('{{{toolbarDivId}}}', $this->returnToolbarDivID(), $html);
    	$html = str_replace('{{{highlightCss}}}', $this->returnHighlightCss(), $html);
    	$html = str_replace('{{{categoryTitle}}}', $this->getTitle(), $html);
    	$html = str_replace('{{{categoryToolbarButtonHtml}}}', $toolbarButtonHtml, $html);
    	$html = str_replace('{{{refItemHtml}}}', $this->returnItemDisplayHtml(), $html);
    	
    	$h_addItemButton = '';
    	if ($this->getIsEditable()) {
    		$h_addItemButton = $this->returnAddRefItemDivHtml().$this->returnAddRefItemButtonHtml();
    	}
    	$html = str_replace('{{{addItemButtonHtml}}}', $h_addItemButton, $html);
    	
    	return $html;
    }
    
    private function returnDeleteButtonHtml() {
    	global $Lang;
    	
    	return '<a title="'.$Lang['Btn']['Delete'].'" class="tool_delete" href="javascript:void(0);" onclick="deleteRefCategory(\''.$Lang['W2']['jsWarningAry']['deleteVocabGroup'].'\', \''.$Lang['W2']['returnMsgArr']['vocabGroupDeleteSuccess'].'\', \''.$Lang['W2']['returnMsgArr']['vocabGroupDeleteFailed'].'\', \''.$this->getRefCategoryId().'\', \''.$this->getInfoboxCode().'\');"></a>';
    }
    
    private function returnEditButtonHtml() {
    	global $Lang;
    	
    	$paraAssoAry = array();
    	$paraAssoAry['infoboxCode'] = $this->getInfoboxCode();
    	$paraAssoAry['refCategoryId'] = $this->getRefCategoryId();
    	$extraParam = base64_encode($this->objW2->getUrlParaByAssoAry($paraAssoAry));
    	
    	return '<a title="'.$Lang['Btn']['Edit'].'" class="thickbox tool_edit" class="thickbox tool_add" href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=600" onclick="loadEditRefCategoryByThickbox(\'<h1 class=ref>Edit Group</h1>\', \''.$extraParam.'\');"></a>';
    }
    
    private function returnAddRefItemDivHtml() {
    	global $Lang;
    	
    	$divId = 'addRefItemDiv_'.$this->getRefCategoryCode();
    	$textboxId = 'newRefItemTb_'.$this->getRefCategoryCode();
    	$addOnclickTag = 'onclick="saveRefItem(\''.$divId.'\', \''.$textboxId.'\', \''.$this->getSourceType().'\', \''.$this->getRefCategoryCode().'\', \''.$this->getInfoboxCode().'\');"';
    	$cancelOnclickTag = 'onclick="triggerAddItemDiv(\''.$this->getRefCategoryCode().'\');"';
    	
    	$html = '';
    	$html .= '<div id="'.$divId.'" style="display:none;">'."\r\n";
    		$html .= '<input type="text" size="30" id="'.$textboxId.'" value="" />'."\r\n";
    		$html .= '<br />'."\r\n";
    		$html .= '<input type="button" class="formsmallbutton" value="'.$Lang['Btn']['Add'].'" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" '.$addOnclickTag.' />'."\r\n";
    		$html .= '<input type="button" class="formsmallbutton" value="'.$Lang['Btn']['Cancel'].'" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" '.$cancelOnclickTag.' />'."\r\n";
    	$html .= '</div>'."\r\n";
    	
    	return $html;
    }
    
    private function returnAddRefItemButtonHtml() {
    	return '<span class="table_row_tool_text"><a class="tool_add_small" href="javascript:void(0);" onclick="triggerAddItemDiv(\''.$this->getRefCategoryCode().'\');"><em>&nbsp;</em><span>Add new word</span></a></span><br />';
    }
}
?>