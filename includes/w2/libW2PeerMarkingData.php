<?
// using: ivan
/*
 * Modification Log:
 *
 */

class libW2PeerMarkingData {
	private $objDb;
	private $tableName;
	private $primaryKeyName;
	
	private $recordId;
	private $writingId;
	private $markerUserId;
	private $targetUserId;
	private $comment;
	private $viewStatus;
	private $dateInput;
	private $inputBy;
	private $dateModified;
	private $modifiedBy;
	private $deleteStatus;
	private $deletedBy;
	private $mark;
	private $sticker;
	private $version;
	
	
	public function __construct($primaryKeyVal = NULL) {
		global $w2_cfg,$intranet_db;

		$this->objDb = new libdb();
		
		$this->setTableName('W2_PEER_MARKING_DATA');
		$this->setPrimaryKeyName('RECORD_ID');

		if(is_numeric($primaryKeyVal)){
			$this->setRecordId($primaryKeyVal);
			$this->loadRecordFromStorage();
		}
    }
    
   
////START GET SET///
	private function setTableName($str) {
		$this->tableName = $str;
	}
	private function getTableName() {
		return $this->tableName;
	}
	
	private function setPrimaryKeyName($str) {
		$this->primaryKeyName = $str;
	}
	private function getPrimaryKeyName() {
		return $this->primaryKeyName;
	}
	
	private function getPrimaryKeyValue() {
		return $this->recordId;
	}
	
	public function setRecordId($val) {
		$this->recordId = $val;
	}
	public function getRecordId() {
		return $this->recordId;
	}
	
	public function setWritingId($val) {
		$this->writingId = $val;
	}
	public function getWritingId() {
		return $this->writingId;
	}
	
	public function setMarkerUserId($val) {
		$this->markerUserId = $val;
	}
	public function getMarkerUserId() {
		return $this->markerUserId;
	}
	
	public function setTargetUserId($val) {
		$this->targetUserId = $val;
	}
	public function getTargetUserId() {
		return $this->targetUserId;
	}
	
	public function setComment($val) {
		$this->comment = $val;
	}
	public function getComment() {
		return $this->comment;
	}
	
	public function setViewStatus($val) {
		$this->viewStatus = $val;
	}
	public function getViewStatus() {
		return $this->viewStatus;
	}
	
	public function setDateInput($val) {
		$this->dateInput = $val;
	}
	public function getDateInput() {
		return $this->dateInput;
	}
	
	public function setInputBy($val) {
		$this->inputBy = $val;
	}
	public function getInputBy() {
		return $this->inputBy;
	}
	
	public function setDateModified($val) {
		$this->dateModified = $val;
	}
	public function getDateModified() {
		return $this->dateModified;
	}
	
	public function setModifiedBy($val) {
		$this->modifiedBy = $val;
	}
	public function getModifiedBy() {
		return $this->modifiedBy;
	}
	
	public function setDeleteStatus($val) {
		$this->deleteStatus = $val;
	}
	public function getDeleteStatus() {
		return $this->deleteStatus;
	}
	
	public function setDeletedBy($val) {
		$this->deletedBy = $val;
	}
	public function getDeletedBy() {
		return $this->deletedBy;
	}	
	public function setMark($val) {
		$this->mark = $val;
	}
	public function getMark() {
		return $this->mark;
	}
	
	public function setSticker($val) {
		$this->sticker = $val;
	}
	public function getSticker() {
		return $this->sticker;
	}
	
	public function setVersion($val) {
		$this->version = $val;
	}
	public function getVersion() {
		return $this->version;
	}	
////END GET SET///

	
	/**
	* LOAD peer marking data
	* @owner : Ivan (20120405)
    * @input : NIL
	* @return : NIL
	*/
	private function loadRecordFromStorage(){
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		
		$sql = "select * from $tableName where $primaryKeyName = $primaryKeyValue";
		$result = current($this->objDb->returnResultSet($sql));
		
		$this->setRecordId($result['RECORD_ID']);
		$this->setWritingId($result['WRITING_ID']);
		$this->setMarkerUserId($result['MARKER_USER_ID']);
		$this->setTargetUserId($result['TARGET_USER_ID']);
		$this->setComment($result['COMMENT']);
		$this->setViewStatus($result['VIEW_STATUS']);
		$this->setDateInput($result['DATE_INPUT']);
		$this->setInputBy($result['INPUT_BY']);
		$this->setDateModified($result['DATE_MODIFIED']);
		$this->setModifiedBy($result['MODIFIED_BY']);
		$this->setDeleteStatus($result['DELETE_STATUS']);
		$this->setDeletedBy($result['DELETED_BY']);
	}
	
	public function save() {
		$primaryKey = $this->getPrimaryKeyValue();
		
		if (is_numeric($primaryKey)) {
			$newPrimaryKey = $this->updateDb();
		}
		else {
			$newPrimaryKey = $this->insertDb();
		}
		
		return $newPrimaryKey;
	}
	
	private function updateDb() {
		# Prepare value for SQL update
		$DataArr = array();
		$DataArr["COMMENT"]			= $this->objDb->pack_value($this->getComment(), "str");
		$DataArr["STICKER"]			= $this->objDb->pack_value($this->getSticker(), "str");
		$DataArr["MARK"]			= $this->objDb->pack_value($this->getMark(), "str");
		$DataArr["VIEW_STATUS"]		= $this->objDb->pack_value($this->getViewStatus(), "int");
		$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value($this->getDateModified(), "date");
		$DataArr["MODIFIED_BY"]		= $this->objDb->pack_value($this->getModifiedBy(), "int");
		
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $_field => $_value) {
			$valueFieldArr[] = " $_field = $_value ";
		}
		$valueFieldText .= implode(',', $valueFieldArr);
		
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		$sql = "Update $tableName Set $valueFieldText Where $primaryKeyName = '$primaryKeyValue'";
		$success = $this->objDb->db_db_query($sql);
		
		$this->loadRecordFromStorage();
		return $success;
	}
	
	private function insertDb() {
		$this->setDateInput('now()');
		$this->setInputBy($_SESSION['UserID']);
		
		$DataArr = array();
		$DataArr["WRITING_ID"]		= $this->objDb->pack_value($this->getWritingId(), "int");
		$DataArr["MARKER_USER_ID"]	= $this->objDb->pack_value($this->getMarkerUserId(), "int");
		$DataArr["TARGET_USER_ID"]	= $this->objDb->pack_value($this->getTargetUserId(), "int");
		$DataArr["COMMENT"]			= $this->objDb->pack_value($this->getComment(), "str");
		$DataArr["STICKER"]			= $this->objDb->pack_value($this->getSticker(), "str");
		$DataArr["MARK"]			= $this->objDb->pack_value($this->getMark(), "str");
		$DataArr["VERSION"]			= $this->objDb->pack_value($this->getVersion(), "int");
		$DataArr["VIEW_STATUS"]		= $this->objDb->pack_value($this->getViewStatus(), "int");
		$DataArr["DATE_INPUT"]		= $this->objDb->pack_value($this->getDateInput(), "date");
		$DataArr["INPUT_BY"]		= $this->objDb->pack_value($this->getInputBy(), "int");
		$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value($this->getDateModified(), "date");
		$DataArr["MODIFIED_BY"]		= $this->objDb->pack_value($this->getModifiedBy(), "int");

		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value){
			$fieldArr[] = $field;
			$valueArr[] = $value;
		}
			
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
			
		# Insert Record
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		$sql = "Insert Into $tableName ($fieldText) Values ($valueText)";
		$success = $this->objDb->db_db_query($sql);		

		$primaryKeyVal = $this->objDb->db_insert_id();
		$this->setRecordId($primaryKeyVal);
		$this->loadRecordFromStorage();
		
		return $success;
	}
}
?>