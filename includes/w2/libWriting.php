<?
// using: Siuwan
/*
 * 	Log
 *  Date:	2016-06-02 [Siuwan] [ip.2.5.7.7.1]
 * 			- modified function handleStudentToWriting(),replace $inactiveUserIdAry with $inactiveUserInfoAry as $inactiveUserIdAry is just a simple 1-D array

 */



abstract class libWriting {

	private	$writingId;
	private	$title;
	private	$introduction;
	private	$contentCode;
	private $schemeCode;
	private $startDateTime;
	private $endDateTime;
	private $recordStatus;
	private	$inputBy;
	private	$dateInput;
	private	$dateModified;
	private $deleteStatus;
	private $findStepSql;
	private $withDefaultConceptMap;
	private $allowPeerMarking;
	private $peerMarkingStartDate;
	private $peerMarkingEndDate;
	private $peerMarkingTargetNum;
	private $showPeerName;
	private $allowSelfMarking;
	private $fullMark;
	private $passMark;
	private $lowestMark;
	private $teacherWeight;
	private $selfWeight;
	private $peerWeight;
	
	// Date: 08-01-2014
	// Stylish Template
	private $templateID;
	private $clipArtID;

	// Date: 19-03-2014
	// Exercise date of deletion
	private $dateDeleted;
	
	protected $objDb;
	protected $objW2;
	protected $inputByName;

	public function __construct($writingId = NULL) {
		global $w2_cfg,$intranet_db;

		$this->objDb = new libdb();
		$this->objW2 = new libW2();
		
		if(is_numeric($writingId)){
			$this->setWritingId($writingId);
			$this->loadRecordFromStorage();
		}
    }
    
    /**
	* LOAD writing details from storage with a writing ID
	* @owner : Fai (201000922)
    * @input : NIL
	* @return : NIL
	* 
	*/
	abstract public function loadRecordFromStorage();
	
	/**
	* FIND all the step that related to this writingid
	* @owner : Fai (20111027)
	* @param : NIL
	* @return : ARRAY $result , step details for the writingid 
	* 
	*/
	abstract public function findStep();
	
	abstract public function save();
	
	abstract public function delete();
	

////START GET SET///

	public function setWritingId($id){
		$this->writingId = $id;
	}

	public function getWritingId(){
		return $this->writingId;
	}

	public function setTitle($title){
		$this->title = $title;
	}
	public function getTitle(){
		return $this->title;
	}

	public function setIntroduction($str){
		$this->introduction = $str;
	}
	public function getIntroduction(){
		return $this->introduction;
	}




	public function setContentCode($contentCode){
		$this->contentCode = $contentCode;
	}
	public function getContentCode(){
		return $this->contentCode;
	}

	public function setSchemeCode($schemeCode){
		$this->schemeCode = $schemeCode;
	}
	public function getSchemeCode(){
		return $this->schemeCode;
	}
	
	public function setStartDateTime($str){
		$this->startDateTime = $str;
	}
	public function getStartDateTime(){
		return $this->startDateTime;
	}
	public function getStartDate() {
		return getDateByDateTime($this->getStartDateTime());
	}
	public function getStartHour() {
		return getHourByDateTime($this->getStartDateTime());
	}
	public function getStartMin() {
		return getMinuteByDateTime($this->getStartDateTime());
	}
	
	public function setEndDateTime($str){
		$this->endDateTime = $str;
	}
	public function getEndDateTime(){
		return $this->endDateTime;
	}
	public function getEndDate() {
		return getDateByDateTime($this->getEndDateTime());
	}
	public function getEndHour() {
		return getHourByDateTime($this->getEndDateTime());
	}
	public function getEndMin() {
		return getMinuteByDateTime($this->getEndDateTime());
	}
	
	public function setRecordStatus($str){
		$this->recordStatus = $str;
	}
	public function getRecordStatus(){
		return $this->recordStatus;
	}
	
	
	public function setInputBy($id){
		$this->inputBy = $id;
	}
	public function getInputBy(){
		return $this->inputBy;
	}
	public function setDateInput($str){
		$this->dateInput = $str;
	}
	public function getDateInput(){
		return $this->dateInput;
	}
	public function setDateModified($str){
		$this->dateModified = $str;
	}
	public function getDateModified(){
		return $this->dateModified;
	}
	public function setDeleteStatus($val){
		$this->deleteStatus = $val;
	}
	public function getDeleteStatus(){
		return $this->deleteStatus;
	}
	public function setWithDefaultConceptMap($val){
		$this->withDefaultConceptMap = $val;
	}
	public function getWithDefaultConceptMap(){
		return $this->withDefaultConceptMap;
	}
	
	public function setAllowPeerMarking($val) {
		global $w2_cfg;
		
		if ($val == '') {
			$val = $w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING_default"];
		}
		
		$this->allowPeerMarking = $val;
	}
	public function getAllowPeerMarking() {
		return $this->allowPeerMarking;
	}
	
	public function setPeerMarkingStartDate($val) {
		$this->peerMarkingStartDate = $val;
	}
	public function getPeerMarkingStartDate() {
		return $this->peerMarkingStartDate;
	}
	
	public function setPeerMarkingEndDate($val) {
		$this->peerMarkingEndDate = $val;
	}
	public function getPeerMarkingEndDate() {
		return $this->peerMarkingEndDate;
	}
	
	public function setPeerMarkingTargetNum($val) {
		global $w2_cfg;
		
		if ($val == '') {
			$val = $w2_cfg["DB_W2_WRITING_PEER_MARKING_TARGET_NUM_default"];
		}
		
		$this->peerMarkingTargetNum = $val;
	}
	public function getPeerMarkingTargetNum() {
		return $this->peerMarkingTargetNum;
	}
	
	public function setShowPeerName($val) {
		global $w2_cfg;
		
		if ($val == '') {
			$val = $w2_cfg["DB_W2_WRITING_SHOW_PEER_NAME_default"];
		}
		
		$this->showPeerName = $val;
	}
	public function getShowPeerName() {
		return $this->showPeerName;
	}

	//inputByName should be without set method
	public function getInputByName(){
		return $this->inputByName;
	}
	##Siuwan 20131204 Marking Method
	function setAllowSelfMarking($val) {
		$this->allowSelfMarking = $val;
	}
	function getAllowSelfMarking() {
		return $this->allowSelfMarking;
	}
	
	function setFullMark($val) {
		$this->fullMark = $val;
	}
	function getFullMark() {
		return $this->fullMark;
	}
	
	function setPassMark($val) {
		$this->passMark = $val;
	}
	function getPassMark() {
		return $this->passMark;
	}
	
	function setLowestMark($val) {
		$this->lowestMark = $val;
	}
	function getLowestMark() {
		return $this->lowestMark;
	}
	
	function setTeacherWeight($val) {
		$this->teacherWeight = $val;
	}
	function getTeacherWeight() {
		return $this->teacherWeight;
	}
	
	function setSelfWeight($val) {
		$this->selfWeight = $val;
	}
	function getSelfWeight() {
		return $this->selfWeight;
	}
	
	function setPeerWeight($val) {
		$this->peerWeight = $val;
	}
	function getPeerWeight() {
		return $this->peerWeight;
	}
	
	public function setTemplateID($val){
		$this->templateID = $val;
	}
	public function getTemplateID(){
		return $this->templateID;
	}	

	public function setClipArtID($val){
		$this->clipArtID = $val;
	}
	public function getClipArtID(){
		return $this->clipArtID;
	}	
	public function setDateDeleted($val){
		$this->dateDeleted = $val;
	}
	public function getDateDeleted(){
		return $this->dateDeleted;
	}		
////END GET SET///


	/**
	* LOAD writing details from storage with a writing ID
	* @owner : Fai (201000922)
    * @input : NIL
	* @return : NIL
	* 
	*/
//	private function loadRecordFromStorage() {
//		global $intranet_db,$w2_cfg;
////error_log("in loadRecordFromStorage get writindid ".$this->getWritingId()."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
//		$sql = 'select 
//					WRITING_ID , w.TITLE as `TITLE`, w.INTRODUCTION as `INTRODUCTION`,w.WITH_DEFAULT_CONCEPTMAP as `WITH_DEFAULT_CONCEPTMAP`, SCHEME_CODE,CONTENT_CODE,INPUT_BY as `INPUT_BY`, '.getNameFieldByLang('i.').' as `INPUT_BY_NAME`,DATE_INPUT,DATE_MODIFIED ,w.DELETE_STATUS as `DELETE_STATUS`, START_DATE_TIME, END_DATE_TIME, RECORD_STATUS
//				from '.$intranet_db.'.W2_WRITING as w 
//					left join '.$intranet_db.'.INTRANET_USER as i on w.INPUT_BY = i.UserID 
//				where w.WRITING_ID ='.$this->getWritingId().' and w.DELETE_STATUS='.$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"];
////error_log("load Record ".$sql."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
//
//		$result = current($this->objDb->returnResultSet($sql));
//
//
//		$this->writingId = $result['WRITING_ID'];
//		$this->title = $result['TITLE'];
//		$this->introduction = $result['INTRODUCTION'];
//		$this->contentCode = $result['CONTENT_CODE'];
//		$this->schemeCode = $result['SCHEME_CODE'];
//		$this->startDateTime = $result['START_DATE_TIME'];
//		$this->endDateTime = $result['END_DATE_TIME'];
//		$this->recordStatus = $result['RECORD_STATUS'];
//		$this->inputBy = $result['INPUT_BY'];
//		$this->inputByName = $result['INPUT_BY_NAME'];
//		$this->dateInput = $result['DATE_INPUT'];
//		$this->withDefaultConceptMap = $result['WITH_DEFAULT_CONCEPTMAP'];
//		$this->dateModified  = $result['DATE_MODIFIED'];
//		$this->deleteStatus = $result['DELETE_STATUS'];
//	}
	
	
	
   /**
	* INSERT STUDENT INTO a writing by this writing ID
	* @owner : Fai (20111026)
	* @param : INTEGER / INTEGER ARRAY $studentIDAry  integer or array of integer , should be a valid userid in ip.INTRANET_USER
	* @param : Int $creatorID  Integer , creator id for the record
	* @return : ARRAY $result , Associative array  of a key --> userid with the value --> new created id in the "W2_WRITING_STUDENT"
	* 
	*/
	public function handleStudentToWriting($studentIDAry,$creatorID){
		global $w2_cfg,$intranet_db,$w2_classRoomID;
		
		$SuccessArr = array();
		
		### Handle the deletion of students first (soft delete)
		$activeUserInfoAry = $this->findStudentInWriting($w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"]);
		$activeUserIdAry = Get_Array_By_Key($activeUserInfoAry, 'USER_ID');
		unset($activeUserInfoAry);
		$deletedUserIdAry = array_values(array_diff((array)$activeUserIdAry, (array)$studentIDAry));
		$SuccessArr['softDeleteUser'] = $this->deleteStudentFromWriting($deletedUserIdAry, $creatorID);
		
		
		### Get the inactive students of the writing (activate the student if the student is added to the writing again)
		$inactiveUserInfoAry = $this->findStudentInWriting($w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["isDeleted"]);
		$inactiveUserIdAry = Get_Array_By_Key($inactiveUserInfoAry, 'USER_ID');
		$inactiveUserAssoAry = BuildMultiKeyAssoc($inactiveUserInfoAry, 'USER_ID');
		unset($inactiveUserInfoAry);


		$result = array();
		$fieldArr = array();
		$valueArr = array();
//error_log("inside inside --> ".$studentIDAry."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
		if($studentIDAry == ''){
			return ;
		}
		$studentIDAry = is_array($studentIDAry)? $studentIDAry: array($studentIDAry);
//error_log("count --> ".count($studentIDAry)."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
		for($i = 0,$i_max = count($studentIDAry);$i < $i_max ;$i++){

			$DataArr = $fieldArr = $valueArr  = array();

			$_userID = $studentIDAry[$i];
//error_log("uid uid -->".$_userID."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
			if(is_numeric($_userID) && $_userID > 0){
				// do nothing
			}else{
				//since $_userID is not a integer , skip the create step
				continue; 
			}
			
			
			if (in_array($_userID, (array)$inactiveUserIdAry)) {
				// The user is deleted before => re-acitvate the user record again
				$DataArr["DELETE_STATUS"]	= $this->objDb->pack_value($w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"], "int");
				$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value('now()', 'date');
				$_setText = $this->objDb->Get_Update_SQL_Set_String($DataArr);
				
				$sql = 'Update '.$intranet_db.'.W2_WRITING_STUDENT Set '.$_setText.' Where WRITING_ID = \''.$this->getWritingId().'\' and USER_ID = \''.$_userID.'\'';
				$SuccessArr['reactivateUser'][$_userID] = $this->objDb->db_db_query($sql);
				
				$lastInsertId = $inactiveUserAssoAry[$_userID]['WRITING_STUDENT_ID'];
			}
			else {
				// new user
				$DataArr["WRITING_ID"]		= $this->objDb->pack_value($this->getWritingId(), "int");
				$DataArr["USER_ID"]			= $this->objDb->pack_value($_userID, "int");
				$DataArr["INPUT_BY"]		= $this->objDb->pack_value($creatorID, "int");
				$DataArr["DELETE_STATUS"]	= $this->objDb->pack_value($w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"], "int");
				$DataArr["DATE_INPUT"]		= $this->objDb->pack_value('now()', "date");
				$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value('now()', 'date');
	
				# set field and value string
				foreach ($DataArr as $field => $value){
					$fieldArr[] = $field;
					$valueArr[] = $value;
				}
	
				$fieldText = implode(", ", $fieldArr);
				$valueText = implode(", ", $valueArr);
	
				$sql = 'Insert Into '.$intranet_db.'.W2_WRITING_STUDENT ('.$fieldText.') Values ('.$valueText.')';
	//error_log($sql."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
				$success = $this->objDb->db_db_query($sql);
				$lastInsertId = $this->objDb->db_insert_id();
			}
			

			$result[$_userID] = $lastInsertId;

			$objW2 = new libW2();
			$objW2->createEclassUserAccountForWriting20($_userID, $w2_classRoomID);
		}
		return $result[$_userID];
	}

   /**
	* INSERT STUDENT INTO a writing by this writing ID
	* @owner : Fai (20111102)
	* @param : INTEGER / INTEGER ARRAY $teacherId a integer or an array of integer , should be a valid userid in ip.INTRANET_USER
	* @param : Int $creatorID  Integer , creator id for the record
	* @return : ARRAY $result , Associative array  of a key --> userid with the value --> new created id in the "W2_WRITING_STUDENT"
	* 
	*/
	public function handleTeacherToWriting($teacherId,$creatorId){
		global $w2_cfg,$intranet_db;
		
		$SuccessArr = array();
		
		### Handle the deletion of teachers first (soft delete)
		$activeUserInfoAry = $this->findTeacherInWriting($w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"]);
		$activeUserIdAry = Get_Array_By_Key($activeUserInfoAry, 'USER_ID');
		unset($activeUserInfoAry);
		$deletedUserIdAry = array_values(array_diff((array)$activeUserIdAry, (array)$teacherId));
		$SuccessArr['softDeleteUser'] = $this->deleteTeacherFromWriting($deletedUserIdAry, $creatorId);
		
		
		### Get the inactive teachers of the writing (activate the teacher if the teacher is added to the writing again)
		$inactiveUserInfoAry = $this->findTeacherInWriting($w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["isDeleted"]);
		$inactiveUserIdAry = Get_Array_By_Key($inactiveUserInfoAry, 'USER_ID');
		$inactiveUserAssoAry = BuildMultiKeyAssoc($inactiveUserIdAry, 'USER_ID');
		unset($inactiveUserInfoAry);
		

		$result = array();
		$fieldArr = array();
		$valueArr = array();

		if($teacherId == ''){
			return;
		}
		$teacherIdAry  = is_array($teacherId)? $teacherId : array($teacherId);

		for($i = 0,$i_max = count($teacherIdAry);$i < $i_max ;$i++){

			$DataArr = $fieldArr = $valueArr  = array();

			$_userID = $teacherIdAry[$i];
			if(is_numeric($_userID) && $_userID > 0){
				// do thing 
			}else{
				//since $_userID is not a integer , skip the create step
				continue; 
			}
			
			if (in_array($_userID, (array)$inactiveUserIdAry)) {
				// The user is deleted before => re-acitvate the user record again
				$DataArr["DELETE_STATUS"]	= $this->objDb->pack_value($w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"], "int");
				$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value('now()', 'date');
				$_setText = $this->objDb->Get_Update_SQL_Set_String($DataArr);
				
				$sql = 'Update '.$intranet_db.'.W2_WRITING_TEACHER Set '.$_setText.' Where WRITING_ID = \''.$this->getWritingId().'\' and USER_ID = \''.$_userID.'\'';
				$SuccessArr['reactivateUser'][$_userID] = $this->objDb->db_db_query($sql);
				
				$lastInsertId = $inactiveUserAssoAry[$_userID]['WRITING_TEACHER_ID'];
			}
			else {
				$DataArr["WRITING_ID"]		= $this->objDb->pack_value($this->getWritingId(), "int");
				$DataArr["USER_ID"]			= $this->objDb->pack_value($_userID, "int");
				$DataArr["INPUT_BY"]		= $this->objDb->pack_value($creatorId, "int");
				$DataArr["DELETE_STATUS"]	= $this->objDb->pack_value($w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"], "str");
				$DataArr["DATE_INPUT"]		= $this->objDb->pack_value('now()', "date");
				$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value('now()', 'date');
	
				# set field and value string
				foreach ($DataArr as $field => $value){
					$fieldArr[] = $field;
					$valueArr[] = $value;
				}
	
				$fieldText = implode(", ", $fieldArr);
				$valueText = implode(", ", $valueArr);
	
				$sql = 'Insert Into '.$intranet_db.'.W2_WRITING_TEACHER ('.$fieldText.') Values ('.$valueText.')';
	//error_log($sql."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
				$success = $this->objDb->db_db_query($sql);
				$lastInsertId = $this->objDb->db_insert_id();
				$result[$_userID] = $lastInsertId;
			}
			
		}
		return $result[$_userID];
	}

	/**
	* FIND all teacher that is active in the writing
	* @owner : Fai (20111126)
	* @param : ARRAY $deleteStatusAry, the target status of the teacher, default return active teacher only
	* @return : ARRAY $result , teacher details
	* 
	*/
	public function findTeacherInWriting($deleteStatusAry=null){
		global $intranet_db , $w2_cfg;
		
		if ($deleteStatusAry == null) {
			$condsDeleteStatus = " and t.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"]."' ";
		}
		else {
			$condsDeleteStatus = " and t.DELETE_STATUS in ('".implode("','", (array)$deleteStatusAry)."') ";
		}

		$dbNameField = getNameFieldWithClassNumberByLang("i.");
		$sql = 'select i.userid as USER_ID , '.$dbNameField.' as USER_NAME, t.WRITING_TEACHER_ID
					from '.$intranet_db.'.W2_WRITING_TEACHER as t
					inner join '.$intranet_db.'.INTRANET_USER as i
					on t.USER_ID = i.UserID
				where 
					WRITING_ID ='.$this->getWritingId().' 
					'.$condsDeleteStatus.'
				';

		$result = $this->objDb->returnResultSet($sql);
		return $result;
	}

	/**
	* FIND all student that is active in the writing
	* @owner : Fai (20111128)
	* @param : ARRAY $deleteStatusAry, the target status of the student, default return active student only
	* @return : ARRAY $result , student details
	* 
	*/
	public function findStudentInWriting($deleteStatusAry=null){
		global $intranet_db , $w2_cfg;
		
		if ($deleteStatusAry == null) {
			$condsDeleteStatus = " and s.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"]."' ";
		}
		else {
			$condsDeleteStatus = " and s.DELETE_STATUS in ('".implode("','", (array)$deleteStatusAry)."') ";
		}

		$dbNameField = getNameFieldWithClassNumberByLang("i.");
		$sql = 'select i.userid as USER_ID , '.$dbNameField.' as USER_NAME, s.WRITING_STUDENT_ID
					from '.$intranet_db.'.W2_WRITING_STUDENT as s
					inner join '.$intranet_db.'.INTRANET_USER as i
					on s.USER_ID = i.UserID
				where 
					s.WRITING_ID ='.$this->getWritingId().'
					'.$condsDeleteStatus.'
				order by
					i.ClassName, i.ClassNumber
				';

		$result = $this->objDb->returnResultSet($sql);
		return $result;
	}

	/**
	* FIND all the step that related to this writingid
	* @owner : Fai (20111027)
	* @param : NIL
	* @return : ARRAY $result , step details for the writingid 
	* 
	*/
//	public function findStep(){
//		global $w2_cfg,$intranet_db;
//
//		//$sql = 'select s.STEP_ID, s.TITLE_ENG as `STEP_TITLE_ENG` ,s.TITLE_CHI as `STEP_TITLE_CHI`, s.CODE as `STEP_CODE`, s.SEQUENCE as `STEP_SEQUENCE` from '.$intranet_db.'.W2_WRITING as w inner join '.$intranet_db.'.W2_STEP as s on w.WRITING_ID = s.WRITING_ID where w.WRITING_ID = '.$this->getWritingId().' order by s.SEQUENCE asc';
//
//		//$result = $this->objDb->returnResultSet($sql);
//		//return $result;
//		
//		return $this->objW2->getWritingStep($this->getWritingId());
//	}
	
	/**
	* DELETE the student from the writing (soft delete)
	* @owner : Ivan (20111129)
	* @param : ARRAY $userIdAry, the target userId to be removed 
	* @return : BOOLEAN, true if the delete process success 
	*/
	private function deleteStudentFromWriting($userIdAry, $deletedByUserId) {
		global $w2_cfg, $intranet_db;
		
		$DataArr = array();
		$DataArr["DELETE_STATUS"]	= $this->objDb->pack_value($w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["isDeleted"], 'int');
		$DataArr["DELETED_BY"]	= $this->objDb->pack_value($deletedByUserId, 'int');
		$DataArr["DATE_DELETE"]	= $this->objDb->pack_value('now()', 'date');
		$setText = $this->objDb->Get_Update_SQL_Set_String($DataArr);
		
		$W2_WRITING_STUDENT = $intranet_db.'.W2_WRITING_STUDENT';
		$sql = "Update $W2_WRITING_STUDENT Set $setText Where WRITING_ID = '".$this->getWritingId()."' And USER_ID In ('".implode("','", (array)$userIdAry)."')";
		return $this->objDb->db_db_query($sql);
	}
	
	/**
	* DELETE the teacher from the writing (soft delete)
	* @owner : Ivan (20111129)
	* @param : ARRAY $userIdAry, the target userId to be removed 
	* @return : BOOLEAN, true if the delete process success 
	*/
	private function deleteTeacherFromWriting($userIdAry, $deletedByUserId) {
		global $w2_cfg, $intranet_db;
		
		$DataArr = array();
		$DataArr["DELETE_STATUS"]	= $this->objDb->pack_value($w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["isDeleted"], 'int');
		$DataArr["DELETED_BY"]	= $this->objDb->pack_value($deletedByUserId, 'int');
		$DataArr["DATE_DELETE"]	= $this->objDb->pack_value('now()', 'date');
		$setText = $this->objDb->Get_Update_SQL_Set_String($DataArr);
		
		$W2_WRITING_TEACHER = $intranet_db.'.W2_WRITING_TEACHER';
		$sql = "Update $W2_WRITING_TEACHER Set $setText Where WRITING_ID = '".$this->getWritingId()."' And USER_ID In ('".implode("','", (array)$userIdAry)."')";
		return $this->objDb->db_db_query($sql);
	}
	
	public function isCompleted() {
		if (strtotime(date('Y-m-d H:i:s')) > strtotime($this->getEndDateTime())) {
			return true;
		}
		else {
			return false;
		}
	}

	public function isCurrentMarkingRecordSaveAsScore($r_stepId, $p_studentId){
		$sql = " SELECT * FROM W2_STEP_HANDIN WHERE STEP_ID = '$r_stepId' AND USER_ID = '$p_studentId' "; 		
		$result = current($this->objDb->returnArray($sql));
		$step_handin_code = $result["STEP_HANDIN_CODE"]; 
		$version = $result["VERSION"]; 
		
		$sql = " SELECT * FROM WS_GAME_SCORE_LOG WHERE FunctionType = 'marking' " .
				" AND ScoreType = '$step_handin_code' " .
				" AND Version = '$version' AND UserID = '$p_studentId' ";
		$result = $this->objDb->returnArray($sql);
		
		if(sizeof($result)){
			return true;
		}
		return false;
	}
}
?>