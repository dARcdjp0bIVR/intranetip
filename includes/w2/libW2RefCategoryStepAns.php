<?
// using: 

class libW2RefCategoryStepAns extends libW2RefCategory {
	
	private $refAnsCodeAry;
	private $stepAnsAry;
	
	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setSourceType($w2_cfg["refCategoryObjectSource"]["stepAns"]);
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setRefAnsCodeAry($arr) {
    	$this->refAnsCodeAry = $arr;
    }
    private function getRefAnsCodeAry() {
    	return $this->refAnsCodeAry;
    }
    
    public function setStepAnsAry($arr) {
    	$this->stepAnsAry = $arr;
    }
    private function getStepAnsAry() {
    	return $this->stepAnsAry;
    }
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    public function loadObjRefItemAry(){
    	global $w2_cfg;
    	
		$libW2 = new libW2();
		
		$stepAnsAry = $this->getStepAnsAry();
		$refAnsCodeAry = $this->getRefAnsCodeAry();
		for ($i=0, $i_max=count($refAnsCodeAry); $i<$i_max; $i++) {
			$_refAnsCode = $refAnsCodeAry[$i];
			
			$_refAns = $libW2->getWritingStepAnsByStepCodeInParsedArray($_refAnsCode, $stepAnsAry);
			$_refAnsAry = explode("\n", $_refAns);	// the item is "enter" separated
			for ($j=0, $j_max=count($_refAnsAry); $j<$j_max; $j++) {
				$__refVocab = trim($_refAnsAry[$j]);
				
				$_objW2RefItemStepAns = libW2RefItemFactory::createObject($w2_cfg["refItemObjectSource"]["stepAns"]);
				$_objW2RefItemStepAns->setInfoboxCode($this->getInfoboxCode());
				$_objW2RefItemStepAns->setRefCategoryType($this->getSourceType());
				$_objW2RefItemStepAns->setRefCategoryCode($this->getRefCategoryCode());
				$_objW2RefItemStepAns->setTitle($__refVocab);
				$_objW2RefItemStepAns->setCode($_refAnsCode.'_'.$j);
				$this->addObjRefItem($_objW2RefItemStepAns);
			}
		}
    }
    
    public function returnDisplayHtmlManageMode() {
    	$html = $this->returnDivHtmlTemplate();
    	
    	$html = str_replace('{{{toolbarDivId}}}', $this->returnToolbarDivID(), $html);
    	$html = str_replace('{{{highlightCss}}}', $this->returnHighlightCss(), $html);
    	$html = str_replace('{{{categoryTitle}}}', $this->getTitle(), $html);
    	$html = str_replace('{{{categoryToolbarButtonHtml}}}', '', $html);	// cannot edit and delete this type of category
    	$html = str_replace('{{{refItemHtml}}}', $this->returnItemDisplayHtml(), $html);
    	$html = str_replace('{{{addItemButtonHtml}}}', '', $html);			// cannot add new item to this type of category
    	
    	return $html;
    }
}
?>