<?
// using: Adam

class libWritingDb extends libWriting {

	public function __construct($writingId = NULL) {
		parent::__construct();
		
		if(is_numeric($writingId)){
			$this->setWritingId($writingId);
			$this->loadRecordFromStorage();
		}
    }
    
	# 20140623
	# By Adam
	# A variable date delete is added
    public function loadRecordFromStorage(){
		global $intranet_db,$w2_cfg;
//error_log("in loadRecordFromStorage get writindid ".$this->getWritingId()."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
		$sql = 'select 
					WRITING_ID , 
					w.TITLE as `TITLE`, 
					w.INTRODUCTION as `INTRODUCTION`,
					w.WITH_DEFAULT_CONCEPTMAP as `WITH_DEFAULT_CONCEPTMAP`, 
					SCHEME_CODE,
					CONTENT_CODE,
					INPUT_BY as `INPUT_BY`, 
					'.getNameFieldByLang('i.').' as `INPUT_BY_NAME`,
					DATE_INPUT,DATE_MODIFIED , DATE_DELETED ,
					w.DELETE_STATUS as `DELETE_STATUS`, 
					w.START_DATE_TIME as START_DATE_TIME, 
					w.END_DATE_TIME as END_DATE_TIME, 
					w.RECORD_STATUS as RECORD_STATUS, 
					w.ALLOW_PEER_MARKING as ALLOW_PEER_MARKING, 
					w.PEER_MARKING_START_DATE as PEER_MARKING_START_DATE, 
					w.PEER_MARKING_END_DATE as PEER_MARKING_END_DATE, 
					w.PEER_MARKING_TARGET_NUM as PEER_MARKING_TARGET_NUM, 
					w.TEMPLATE_ID as TEMPLATE_ID, 
					w.CLIPART_ID as CLIPART_ID, 
					w.SHOW_PEER_NAME as SHOW_PEER_NAME, 
					w.ALLOW_SELF_MARKING, 
					w.FULLMARK, 
					w.PASSMARK, 
					w.LOWESTMARK, 
					w.TEACHER_WEIGHT, 
					w.SELF_WEIGHT,
					w.PEER_WEIGHT
				from '.$intranet_db.'.W2_WRITING as w 
					left join '.$intranet_db.'.INTRANET_USER as i on w.INPUT_BY = i.UserID 
				where w.WRITING_ID ='.$this->getWritingId().' and w.DELETE_STATUS='.$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"];

		$result = current($this->objDb->returnResultSet($sql));

		$this->setWritingId($result['WRITING_ID']);
		$this->setTitle($result['TITLE']);
		$this->setIntroduction($result['INTRODUCTION']);
		$this->setContentCode($result['CONTENT_CODE']);
		$this->setSchemeCode($result['SCHEME_CODE']);
		$this->setStartDateTime($result['START_DATE_TIME']);
		$this->setEndDateTime($result['END_DATE_TIME']);
		$this->setRecordStatus($result['RECORD_STATUS']);
		$this->setAllowSelfMarking($result['ALLOW_SELF_MARKING']);
		$this->setAllowPeerMarking($result['ALLOW_PEER_MARKING']);
		$this->setPeerMarkingStartDate($result['PEER_MARKING_START_DATE']);
		$this->setPeerMarkingEndDate($result['PEER_MARKING_END_DATE']);
		$this->setPeerMarkingTargetNum($result['PEER_MARKING_TARGET_NUM']);
		$this->setShowPeerName($result['SHOW_PEER_NAME']);
		$this->setTemplateID($result['TEMPLATE_ID']);// By Adam
		$this->setClipArtID($result['CLIPART_ID']);// By Adam
		
		$this->setInputBy($result['INPUT_BY']);
		$this->setDateInput($result['DATE_INPUT']);
		$this->setWithDefaultConceptMap($result['WITH_DEFAULT_CONCEPTMAP']);
		$this->setDateModified ($result['DATE_MODIFIED']);
		$this->setDeleteStatus($result['DELETE_STATUS']);
		
		$this->inputByName = $result['INPUT_BY_NAME'];
		
		
		
		$result['FULLMARK'] = $result['FULLMARK']?$result['FULLMARK']:W2_MARKING_FULL;
		$result['PASSMARK'] = $result['PASSMARK']?$result['PASSMARK']:W2_MARKING_PASS;
		$result['LOWESTMARK'] = $result['LOWESTMARK']?$result['LOWESTMARK']:W2_MARKING_LOWEST;
		
		$this->setFullMark($result['FULLMARK']);
		$this->setPassMark($result['PASSMARK']);
		$this->setLowestMark($result['LOWESTMARK']);
		
		if(empty($result['TEACHER_WEIGHT'])&&!$result['ALLOW_PEER_MARKING']){
			$result['TEACHER_WEIGHT'] = W2_MARKING_TEACHER_WEIGHT;
			$result['SELF_WEIGHT'] = W2_MARKING_SELF_WEIGHT;
			$result['PEER_WEIGHT'] = W2_MARKING_PEER_WEIGHT;
		}
		
		$this->setTeacherWeight($result['TEACHER_WEIGHT']);
		$this->setSelfWeight($result['SELF_WEIGHT']);
		$this->setPeerWeight($result['PEER_WEIGHT']);
	}
	
	public function save(){
		$writingId = $this->getWritingId();

		$resultID = null;
		if(is_numeric($writingId) && $writingId > 0) {
			$writingId = $this->updateDB();
		}else{
			$writingId = $this->insertDB();
		}

		$this->setWritingId($writingId);
		$this->loadRecordFromStorage();

		return $writingId;
	}
	

	// Date: 03-19-2014
	// Add date of deletion	
	public function delete() {
		global $w2_cfg;
		$this->setDateDeleted('now()');
		$this->setDeleteStatus($w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["isDeleted"]);
		$tempId = $this->updateDB();
		return ($tempId > 0)? true : false;
	}
	
	private function insertDB(){
		global $w2_cfg,$intranet_db;

		$this->setDateInput('now()');
		$this->setDateModified('now()');
		$this->setDeleteStatus($w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"]);

		$DataArr = array();

		$DataArr["TITLE"]						= $this->objDb->pack_value($this->getTitle(), "str");
		$DataArr["INTRODUCTION"]				= $this->objDb->pack_value($this->getIntroduction(), "str");
		$DataArr["SCHEME_CODE"]					= $this->objDb->pack_value($this->getSchemeCode(), "str");
		$DataArr["CONTENT_CODE"]				= $this->objDb->pack_value($this->getContentCode(), "str");
		$DataArr["START_DATE_TIME"]				= $this->objDb->pack_value($this->getStartDateTime(), "date");
		$DataArr["END_DATE_TIME"]				= $this->objDb->pack_value($this->getEndDateTime(), "date");
		$DataArr["RECORD_STATUS"]				= $this->objDb->pack_value($this->getRecordStatus(), "int");
		
		$DataArr["ALLOW_PEER_MARKING"]			= $this->objDb->pack_value($this->getAllowPeerMarking(), "int");
		$DataArr["PEER_MARKING_START_DATE"]		= $this->objDb->pack_value($this->getPeerMarkingStartDate(), "date");
		$DataArr["PEER_MARKING_END_DATE"]		= $this->objDb->pack_value($this->getPeerMarkingEndDate(), "date");
		$DataArr["PEER_MARKING_TARGET_NUM"]		= $this->objDb->pack_value($this->getPeerMarkingTargetNum(), "int");
		$DataArr["SHOW_PEER_NAME"]				= $this->objDb->pack_value($this->getShowPeerName(), "int");

		$DataArr["ALLOW_SELF_MARKING"]			= $this->objDb->pack_value($this->getAllowSelfMarking(), "int");
		$DataArr["FULLMARK"]					= $this->objDb->pack_value($this->getFullMark(), "float");
		$DataArr["PASSMARK"]					= $this->objDb->pack_value($this->getPassMark(), "float");
		$DataArr["LOWESTMARK"]					= $this->objDb->pack_value($this->getLowestMark(), "float");
		$DataArr["TEACHER_WEIGHT"]				= $this->objDb->pack_value($this->getTeacherWeight(), "float");
		$DataArr["SELF_WEIGHT"]					= $this->objDb->pack_value($this->getSelfWeight(), "float");
		$DataArr["PEER_WEIGHT"]					= $this->objDb->pack_value($this->getPeerWeight(), "float");	

		$DataArr["TEMPLATE_ID"]					= $this->objDb->pack_value($this->getTemplateID(), "int");	
		$DataArr["CLIPART_ID"]					= $this->objDb->pack_value($this->getClipArtID(), "int");	
				
		$DataArr["WITH_DEFAULT_CONCEPTMAP"]		= $this->objDb->pack_value($this->getWithDefaultConceptMap(), "int");	
		$DataArr["INPUT_BY"]					= $this->objDb->pack_value($this->getInputBy(), "int");
		$DataArr["DATE_INPUT"]					= $this->objDb->pack_value($this->getDateInput(), "date");
		$DataArr["DATE_MODIFIED"]				= $this->objDb->pack_value($this->getDateModified(), "date");
		$DataArr["DELETE_STATUS"]				= $this->objDb->pack_value($this->getDeleteStatus(), "int");
		
		$sqlStrAry = $this->objDb->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];
			
		# Insert Record
		$sql = 'Insert Into '.$intranet_db.'.W2_WRITING ('.$fieldStr.') Values ('.$valueStr.')';
//error_log("insert --->".$sql."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
		$success = $this->objDb->db_db_query($sql);
						
		$RecordID = $this->objDb->db_insert_id();
		$this->recordID = $RecordID;
			
		$this->loadRecordFromStorage();
		return $RecordID;		
	}
	
	private function updateDB(){
		global $intranet_db;
		if(is_numeric($this->getWritingId()) && $this->getWritingId() > 0){
			// it is a valid writing id
			//do nothing 
		}else{
			return null;
		}

		$DataArr = array();

		$DataArr["TITLE"]						= $this->objDb->pack_value($this->getTitle(), "str");
		$DataArr["INTRODUCTION"]				= $this->objDb->pack_value($this->getIntroduction(), "str");
		$DataArr["SCHEME_CODE"]					= $this->objDb->pack_value($this->getSchemeCode(), "str");
		$DataArr["START_DATE_TIME"]				= $this->objDb->pack_value($this->getStartDateTime(), "date");
		$DataArr["END_DATE_TIME"]				= $this->objDb->pack_value($this->getEndDateTime(), "date");
		$DataArr["RECORD_STATUS"]				= $this->objDb->pack_value($this->getRecordStatus(), "int");
		$DataArr["WITH_DEFAULT_CONCEPTMAP"]		= $this->objDb->pack_value($this->getWithDefaultConceptMap(), "int");
		
		$DataArr["ALLOW_PEER_MARKING"]			= $this->objDb->pack_value($this->getAllowPeerMarking(), "int");
		$DataArr["PEER_MARKING_START_DATE"]		= $this->objDb->pack_value($this->getPeerMarkingStartDate(), "date");
		$DataArr["PEER_MARKING_END_DATE"]		= $this->objDb->pack_value($this->getPeerMarkingEndDate(), "date");
		$DataArr["PEER_MARKING_TARGET_NUM"]		= $this->objDb->pack_value($this->getPeerMarkingTargetNum(), "int");
		$DataArr["SHOW_PEER_NAME"]				= $this->objDb->pack_value($this->getShowPeerName(), "int");

		$DataArr["ALLOW_SELF_MARKING"]			= $this->objDb->pack_value($this->getAllowSelfMarking(), "int");
		$DataArr["FULLMARK"]					= $this->objDb->pack_value($this->getFullMark(), "int");
		$DataArr["PASSMARK"]					= $this->objDb->pack_value($this->getPassMark(), "int");
		$DataArr["LOWESTMARK"]					= $this->objDb->pack_value($this->getLowestMark(), "int");
		$DataArr["TEACHER_WEIGHT"]				= $this->objDb->pack_value($this->getTeacherWeight(), "int");
		$DataArr["SELF_WEIGHT"]					= $this->objDb->pack_value($this->getSelfWeight(), "int");
		$DataArr["PEER_WEIGHT"]					= $this->objDb->pack_value($this->getPeerWeight(), "int");												

		$DataArr["TEMPLATE_ID"]					= $this->objDb->pack_value($this->getTemplateID(), "int");	
		$DataArr["CLIPART_ID"]					= $this->objDb->pack_value($this->getClipArtID(), "int");	
		
		$DataArr["CONTENT_CODE"]				= $this->objDb->pack_value($this->getContentCode(), "str");
		$DataArr["DATE_MODIFIED"]				= $this->objDb->pack_value('now()', "date");
		$DataArr["DELETE_STATUS"]				= $this->objDb->pack_value($this->getDeleteStatus(), "int");

		$updateDetails = "";
		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);

		$sql = "update ".$intranet_db.".W2_WRITING  set ".$updateDetails." where WRITING_ID = ".$this->getWritingId();
//	debug_r($sql);exit;
//error_log("update --->".$sql."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
		$this->objDb->db_db_query($sql);
		
		return $this->getWritingId();
	}
	
	public function findStep(){
		global $w2_cfg,$intranet_db;

		return $this->objW2->getWritingStep($this->getWritingId(), 1);
	}
	
	public function returnWritingAryForJs() {
		$infoAry = array();
		
		$infoAry['r_allowPeerMarking'] = $this->getAllowPeerMarking();
		$infoAry['r_peerMarkingStartDate'] = (is_date_empty($this->getPeerMarkingStartDate()))? '' : $this->getPeerMarkingStartDate();
		$infoAry['r_peerMarkingEndDate'] = (is_date_empty($this->getPeerMarkingEndDate()))? '' : $this->getPeerMarkingEndDate();
		$infoAry['r_peerMarkingTargetNum'] = $this->getPeerMarkingTargetNum();
		$infoAry['r_showPeerName'] = $this->getShowPeerName();
		$infoAry['r_allowSelfMarking'] = $this->getAllowSelfMarking();
		$infoAry['r_fullMark'] = $this->getFullMark();
		$infoAry['r_passMark'] = $this->getPassMark();
		$infoAry['r_lowestMark'] = $this->getLowestMark();
		$infoAry['r_teacherWeight'] = $this->getTeacherWeight();
		$infoAry['r_selfWeight'] = $this->getSelfWeight();
		$infoAry['r_peerWeight'] = $this->getPeerWeight();
		$infoAry['r_templateID'] = $this->getTemplateID();
		$infoAry['r_clipArtID'] = $this->getClipArtID();
		
		$peerMarkingGroupingAry = $this->objW2->getPeerMarkingGrouping($this->getWritingId());
		for ($i=0, $i_max=count($peerMarkingGroupingAry); $i<$i_max; $i++) {
			$infoAry['r_peerMarkingGroupingAry'][$i] = implode(',', (array)$peerMarkingGroupingAry[$i]);
		}
		
		return $infoAry;
	}
}
?>