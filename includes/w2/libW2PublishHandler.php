<?
/**
 * Change Log:
 * 2019-05-02 Pun
 *  - Prevent Command-injection
 */
class PublishHanlder
{

	// logic controller
	function start_publish($aryContent, $arySchoolList,$contentCode){
		if(!is_array($aryContent)){
			$aryContent = array($aryContent);
		}

		if(!is_array($arySchoolList)){
			$arySchoolList = array($arySchoolList);
		}

		for($i = 0,$iMax = count($aryContent);$i < $iMax; $i++){
			$_schemeCode = $aryContent[$i];

			$tarFile = tarFile($_schemeCode,$contentCode);
			if($tarFile !== false){
				for($s = 0,$sMax = count($arySchoolList);$s < $sMax; $s++){
					$result = ftpFileToSchools($tarFile,$arySchoolList[$s]);

					if($result){
						$result = getFtpResult($tarFile,$arySchoolList[$s]);
						debug_r($result);
					}
				}
			}
		}
	}

	// get response through FTP
	function getFtpResult($tarFile, $schoolCode){
		//f:curl_init copy from http://www.lornajane.net/posts/2011/posting-json-data-with-php-curl
		//	$data_string = json_encode($data);
			$data_string = '';

		$arySchoolList[$s];


		 $url = 'http://192.168.0.149/test/test_publish.php';
		$ch = curl_init('http://192.168.0.149/test/test_publish.php');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);

		$result = curl_exec($ch);

		$objJson = new JSON_obj();

		$jsonStr = $objJson->decode($result);
		return $jsonStr;
	}


	// send files to school(s)
	function ftpFileToSchools($tarFile , $arySchoolList){
		if(!is_array($arySchoolList)){
			$arySchoolList = array($arySchoolList);
		}

		for($i = 0,$iMax = count($arySchoolList);$i < $iMax; $i++){
			ftpFile($tarFile ,$arySchoolList[$i]);
		}
		return true;
	}

	// ftp logic
	function ftpFile($aryFile,$schoolCode){
		global $w2_cfg, $schoolConfig;
		if(!is_array($aryFile)){
			$aryFile = array($aryFile);
		}

		$ftp_server = 	$schoolConfig[$schoolCode]['server'];

		$conn_id = ftp_connect($ftp_server);

		if($conn_id === false){
			$err = 'FTP connect fail. Server Id '.$ftp_server;
			bl_error_log($w2_cfg['moduleCode'],$err);
			return false;
		}

		$ftp_user_name = 	$schoolConfig[$schoolCode]['login'];
		$ftp_user_pass = 	$schoolConfig[$schoolCode]['pwd'];;

		$login_result = ftp_login($conn_id,$ftp_user_name,$ftp_user_pass);

		if($login_result !== true){
			bl_error_log($w2_cfg['moduleCode'],'Cannot login to ftp server ['.$ftp_server.']!!');
			return false;
		}

		for($i = 0,$iMax = count($aryFile); $i < $iMax; $i++){
				$_file = $aryFile[$i];
				if(!file_exists($_file)){
					bl_error_log($w2_cfg['moduleCode'],'Try to ftp a file '.$_file.' but the src file does not exist');
				}else{
					if(!is_readable($_file)){
						bl_error_log($w2_cfg['moduleCode'],'Try to ftp a file '.$_file.' but src file does not readable by server');
					}else{
						//$fp = fopen($_file, 'r');

						// try to upload $file

						//if (ftp_fput($conn_id, $_file, $fp, FTP_ASCII)) {

						$remote_file = get_file_basename($_file);

						if (ftp_put($conn_id, $remote_file, $_file, FTP_BINARY)) {
							bl_error_log($w2_cfg['moduleCode'],'Ftp a file '.$_file.' to ['.$ftp_server.'] successfully.');
						} else {
							bl_error_log($w2_cfg['moduleCode'],'Ftp a file '.$_file.' to ['.$ftp_server.'] but failed.');
						}
						//fclose($fp);
					}
				}
		}

		ftp_close($conn_id);
		return true;
	}

	// zip file
	 function tarFile($schemeCode,$contentCode){
		 global $intranet_root,$w2_cfg;

		$srcDir = $intranet_root.'/home/eLearning/w2/content/'.$contentCode.'/'.$schemeCode;
		$tarFile = '/tmp/'.$schemeCode.'.tar';

		if(!file_exists($srcDir)){
			echo ' file does not exist ['.$srcDir.']!';
			bl_error_log($w2_cfg['moduleCode'],'Request tar file but src dir / file does not eixst ['.$srcDir.']');
			return false;
		}


		//If it already exist , check if it writable
		if(file_exists($tarFile)){
			if(!is_writable($tarFile)){
			    $tarFile = OsCommandSafe($tarFile);
			    $cmd = 'ls  -ltr '.$tarFile;
				exec($cmd,$output);
				bl_error_log($w2_cfg['moduleCode'],'Try to overwrite a file ['.$tarFile.'] but it is unwritable by server!!');
				//write to log
				return false;
			}
		}

		$tarFile = OsCommandSafe($tarFile);
		$srcDir = OsCommandSafe($srcDir);
		$cmd = 'tar -cvf '.$tarFile.' '.$srcDir;

		exec ( $cmd ,$output ,$return_var);

		if(!file_exists($tarFile)){
			$cmd = 'df -k';
			exec($cmd);
			//write to log
			bl_error_log($w2_cfg['moduleCode'],'Suppose a tar file must exist ['.$tarFile.'],  but it doe not');
			return false;
		}

		return $tarFile;

	 }

	//error log
	function bl_error_log($projectName='',$errorMsg='',$errorType=''){
		$mail = 'mfkhoe@g2.broadlearning.com';
		//mail($mail,"Project {$projectName} - ".date("F j, Y, g:i a"),$errorMsg);
		return true;
	}
}
?>