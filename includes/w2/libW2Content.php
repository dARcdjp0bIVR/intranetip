<?
abstract class libW2Content {

	protected $objDb;
	
	private $contentCode;
	private $schemeCode;
	//private $unitCode;

	public function __construct($writingId = NULL) {
		global $w2_cfg,$intranet_db;

		$this->objDb = new libdb();	
    }

	abstract public function getConceptMapStepCode();

	abstract public function getConceptMapTitle();

	abstract public function getConceptMapInstruction();
	
	abstract public function getUnitTitle();
	
	//abstract public function getUnitName();
	
	abstract public function getSchemeTitle();
	
	//abstract public function getSchemeName();
	
	abstract public function getWritingTaskStepCode();
	
	
	////START GET SET///
	public function setContentCode($str){
		$this->contentCode = $str;
	}
	public function getContentCode(){
		return $this->contentCode;
	}
	
	public function setSchemeCode($str){
		$this->schemeCode = $str;
	}
	public function getSchemeCode(){
		return $this->schemeCode;
	}
	
//	public function setUnitCode($str){
//		$this->unitCode = $str;
//	}
//	public function getUnitCode(){
//		return $this->unitCode;
//	}
	////END GET SET///
	
	public function getUnitName() {
		global $w2_cfg_contentSetting;
		
		$contentCode = $this->getContentCode();
		$schemeCode = $this->getSchemeCode();
		
		return $w2_cfg_contentSetting[$contentCode][$schemeCode]['type'];
	}
	public function getTypeSuperName(){
		global $w2_cfg_contentSetting;
		
		$contentCode = $this->getContentCode();
		$schemeCode = $this->getSchemeCode();

		return $w2_cfg_contentSetting[$contentCode][$schemeCode]['type_super'];

	}
	public function getSchemeName() {
		global $w2_cfg_contentSetting;
		
		$contentCode = $this->getContentCode();
		$schemeCode = $this->getSchemeCode();
		
		return $w2_cfg_contentSetting[$contentCode][$schemeCode]['name'];
	}
}

?>