<?
// using: 

class libW2RefCategoryDefault extends libW2RefCategory {
	
	private $defaultItemAry;
	
	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setSourceType($w2_cfg["refCategoryObjectSource"]["default"]);
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setDefaultItemAry($arr) {
    	$this->defaultItemAry = $arr;
    }
    private function getDefaultItemAry() {
    	return $this->defaultItemAry;
    }
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    public function loadObjRefItemAry(){
    	global $w2_cfg;
    	
    	// load default item
		$defaultItemAry = $this->getDefaultItemAry();
		for ($i=0, $i_max=count($defaultItemAry); $i<$i_max; $i++) {
			$_title = $defaultItemAry[$i]['itemTitle'];
			$_type = $defaultItemAry[$i]['itemType'];
			$_code = $defaultItemAry[$i]['itemCode'];
			
			$_objW2RefItemDefault = libW2RefItemFactory::createObject($_type);
			$_objW2RefItemDefault->setInfoboxCode($this->getInfoboxCode());
			$_objW2RefItemDefault->setRefCategoryType($this->getSourceType());
			$_objW2RefItemDefault->setRefCategoryCode($this->getRefCategoryCode());
			$_objW2RefItemDefault->setTitle($_title);
			$_objW2RefItemDefault->setCode($_code);
			$this->addObjRefItem($_objW2RefItemDefault);
		}
		
		// load self-add item
		if ($this->getIsEditable()) {
			$refItemIdAry = $this->returnRefItemIdAry();
			for ($i=0, $i_max=count($refItemIdAry); $i<$i_max; $i++) {
				$_refItemId = $refItemIdAry[$i];
				
				$_objW2RefItemSelfAdd = libW2RefItemFactory::createObject($w2_cfg["refItemObjectSource"]["selfAdd"], $_refItemId);
				$_objW2RefItemSelfAdd->setIsEditable($this->getIsEditable());
				$_objW2RefItemSelfAdd->setDisplayMode($this->getDisplayMode());
				$this->addObjRefItem($_objW2RefItemSelfAdd);
			}
		}
    }
    
    public function returnDisplayHtmlManageMode() {
    	global $w2_cfg;
    	
    	$html = $this->returnDivHtmlTemplate();
    	
    	$html = str_replace('{{{toolbarDivId}}}', $this->returnToolbarDivID(), $html);
    	$html = str_replace('{{{highlightCss}}}', $this->returnHighlightCss(), $html);
    	$html = str_replace('{{{categoryTitle}}}', $this->getTitle(), $html);
    	$html = str_replace('{{{categoryToolbarButtonHtml}}}', '', $html);
    	$html = str_replace('{{{refItemHtml}}}', $this->returnItemDisplayHtml(), $html);
    	
    	$h_addItemButton = '';
    	if ($this->getIsEditable()) {
    		$h_addItemButton = $this->returnAddRefItemDivHtml().$this->returnAddRefItemButtonHtml();
    	}
    	$html = str_replace('{{{addItemButtonHtml}}}', $h_addItemButton, $html);
    	
    	return $html;
    }
    
    private function returnAddRefItemDivHtml() {
    	global $Lang;
    	
    	$divId = 'addRefItemDiv_'.$this->getRefCategoryCode();
    	$textboxId = 'newRefItemTb_'.$this->getRefCategoryCode();
    	$addOnclickTag = 'onclick="saveRefItem(\''.$divId.'\', \''.$textboxId.'\', \''.$this->getSourceType().'\', \''.$this->getRefCategoryCode().'\', \''.$this->getInfoboxCode().'\');"';
    	$cancelOnclickTag = 'onclick="triggerAddItemDiv(\''.$this->getRefCategoryCode().'\');"';
    	
    	$html = '';
    	$html .= '<div id="'.$divId.'" style="display:none;">'."\r\n";
    		$html .= '<input type="text" size="30" id="'.$textboxId.'" value="" />'."\r\n";
    		$html .= '<br />'."\r\n";
    		$html .= '<input type="button" class="formsmallbutton" value="'.$Lang['Btn']['Add'].'" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" '.$addOnclickTag.' />'."\r\n";
    		$html .= '<input type="button" class="formsmallbutton" value="'.$Lang['Btn']['Cancel'].'" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" '.$cancelOnclickTag.' />'."\r\n";
    	$html .= '</div>'."\r\n";
    	
    	return $html;
    }
    
    private function returnAddRefItemButtonHtml() {
    	return '<span class="table_row_tool_text"><a class="tool_add_small" href="javascript:void(0);" onclick="triggerAddItemDiv(\''.$this->getRefCategoryCode().'\');"><em>&nbsp;</em><span>Add new word</span></a></span><br />';
    }
}
?>