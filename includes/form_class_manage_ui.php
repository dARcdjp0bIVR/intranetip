<?
// Using:

/**
 * *******************************************
 * modification:
 * 2020-07-22 (Philips) [#L188901]
 * modified Get_Student_Selection(), add $ActiveOnly parameter for $sys_custom['SDAS']['StudentPerformanceTracking']['ShowActiveOnly']
 * 
 * 2018-11-26 (Cameron)
 * pass $HasFloorOnly=0 to Get_Building_Selection() in Get_Class_Form() so that it can retrieve buildings event not input floor info (e.g. kentville)
 * - display Building in Get_Class_Info() for eSchoolBus
 * 
 * 2018-11-16 (Cameron)
 * add Building selection for eSchoolBus in Get_Class_Form()
 * 
 * 2018-08-29 (Bill)    [2017-1207-0956-53277]
 * Modified Get_Class_Info(), Get_Class_Form(), Get_Form_Class_Table(), to display curriculum settings
 * 
 * 2018-03-28 (Cameron)
 * add parameter style to Get_Class_Selection()
 *
 * 2018-01-31 (Carlos)
 * Modified Get_Student_Without_Class_Selection(), added parameter $IncludeYearClassDeselectedStudents and $YearClassID
 * to find out the students that are removed from the class in UI but not updated DB yet. They are also counted as students without class.
 *
 * 20170609 (Icarus)
 * - Modified Get_Form_List_Form(), added the the action button > closebtn
 * - Modified Last Updated Info Div, added the the action button > closebtn into Last Updated Info Div &
 * move it to the bottom of the thickbox,
 * details:
 * Moved Last Updated Info Div of Get_Class_Group_Info_Layer_Table() to Get_Class_Group_Info_Layer()
 *
 * 20170608 Icarus
 * - remove the rowspan of table
 *
 * 20161207 Villa
 * - Add Get_Specific_Form_Selection() get form selection box with input form id
 * - Add Get_Specific_Class_Selection() get class selection box with input class id
 * 20161114 Villa
 * - modified Get_Class_Selection() add param firsttitle
 *
 * 20160503 Ivan [ip.2.5.7.7.1.0] [A95524]
 * - modified Get_Form_Class_Table() to hide "subject taken" column in class list table for KIS
 * 20160111 Carlos
 * - modified Get_Class_Info() and Get_Form_Class_Management_List(), enable edit and import for admin.
 *
 * 20151231 Carlos
 * - $sys_custom['KIS_ClassGroupAttendanceType'] modified Get_Class_Group_Info_Layer_Table(), added AttendanceType field for KIS attendance.
 *
 * 20151023 Pun
 * - modified Get_Subject_Taken_Detail() added added checkbox to sync subject group to eClass
 *
 * 20150323 Henry
 * - added flag $sys_custom['KIS_Admission']['HideDeleteFormBtnInSchSetting'] to control the visibility of delete form button for KIS
 *
 * 20150317 Cameron
 * - modify Get_Form_Class_Table(), hide column "number of subject taken" if $special_feature['form_class_hide_no_subject_taken'] is set
 * (used by Kowloon City Baptist Church)
 * 20130813 Carlos:
 * - modified Get_Form_List_Table(), Get_Class_Form(), hide WEBSAMS code for KIS
 * 20130308 Rita:
 * - modified Get_Form_List_Table(), change to check active year only -- !$fcm->Check_Class_AcademicYear_RecordStatus($thisYearID)
 * 20130124 Ivan:
 * - modified Get_Edit_Class_SubjectGroup_Table() to cater customization $sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']
 * 20130123 Rita:
 * - modified Get_Edit_Class_SubjectGroup_Table();
 * 20120823 YatWoon:
 * - updated Get_Class_Info(), fixed cannot display next year "Subject Enrolment" data. [Case#2012-0823-1126-34071]
 *
 * 20111012 Ivan:
 * - updated Get_Student_Selection added para $ExcludedStudentIDArr
 *
 * 20110907 Marcus:
 * - update Get_Class_Selection, add optgroup if multiple year
 *
 * 20110720 (Henry Chow):
 * - updated Get_Subject_Taken_Detail(), default select current semester
 *
 * 20110720 (Henry Chow):
 * - updated Get_Edit_Class_SubjectGroup_Table(), add "Apply all - subject group menu"
 *
 * 20110505 (Henry Chow):
 * - updated Get_Class_Info(), allow to edit class info in previous year if
 *
 * 20110307 (Henry Chow):
 * - updated Get_Form_Class_Table(), allow to create class in previous academic year (if $special_feature['CanEditPreviousYearInClass'] is on)
 *
 * 20110211 (Ivan):
 * - updated Get_Form_Class_Management_List() to add the Form Stage Customization (2011-0117-1020-27128 Point 2)
 *
 * 20110118 (Henry Chow):
 * - updated Get_Edit_Class_SubjectGroup_Table(), only display applicable subject groups in "Class > Subject Enrolment Detail"
 *
 * 20110112 (Henry Chow):
 * - updated Get_Class_Info() & Get_Form_Class_Table(), synchronize "Subject Enrolment" with "Subject Detail" page
 *
 * 20110110 (Henry Chow):
 * - updated Get_Edit_Class_SubjectGroup_Table(), display warning message if setting not yet completed
 *
 * 20101203 (Marcus):
 * - updated function Get_Form_Selection, add $isMultiple
 *
 * 20101102 (Ivan):
 * - updated Get_Form_List_Table(), disabled the active/inactive status settings until CS confirm the logic
 *
 * 20101026 (Ivan):
 * - updated Get_Form_List_Table(), added active/inactive status for the Form Settings
 *
 * 20100906 (YatWoon):
 * - update Get_Class_Form(), add 2 options for classnumber ordering
 *
 * 20100813 (Ivan):
 * - modified Get_Subject_Taken_Detail() to support one Student can join more than one Subject Group in the same term of the same subject
 *
 * 20100810 (Henry Chow):
 * - modified Get_Form_Class_Management_List(), display "export" & "copy" in all academic year
 *
 * 20100708 (Henry Chow):
 * - modified Get_Form_Class_Management_List(), add links to export
 *
 * 20100629 (Henry Chow):
 * - modified Get_Form_Class_Management_List(), revise the width of layer for IMPORt & EXPORT
 *
 * 20100629 (Henry Chow):
 * - modified Get_Form_Class_Management_List(), display export & copy function
 *
 * 20100622 (Henry Chow):
 * - modified Get_Form_Class_Management_List(), hidden export & copy function
 *
 * 20100617 (Henry Chow):
 * - modified Get_Form_Class_Management_List(), 1) Grouped the options of import in layer; 2) add export & copy options
 *
 * 20100330 Ivan:
 * - modified Get_Class_Form, add option "Add from Student without Class" in the student selection part
 *
 * 20100302 Marcus:
 * - modify Get_Class_Selection, change $SelectedBuildingID to $SelectYearClassID
 * ******************************************
 */
include_once ('form_class_manage.php');
include_once ('subject_class_mapping.php');
include_once ('libuser.php');
include_once ('libinterface.php');

class form_class_manage_ui
{

    function form_class_manage_ui()
    {
        $this->thickBoxWidth = 750;
        $this->thickBoxHeight = 450;
        $this->toolCellWidth = "120px";
        $this->Code_Maxlength = "10";
        $this->WithStudentInClass = 0;
        $this->WithFormSubjectSetting = 0;
    }

    function Get_Class_Info($YearClass)
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom, $special_feature, $UserID, $plugin;
        
        $AcademicYear = new academic_year($YearClass->AcademicYearID);
        $fcm = new form_class_manage();
        $scm = new subject_class_mapping();
        
        if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] || (isset($special_feature['CanEditPreviousYearInClass']) && in_array($UserID, $special_feature['CanEditPreviousYearInClass'])))
            $showAddClassButton = true;
        
        // jquery include
        $x = '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/' . $LAYOUT_SKIN . '/js/form_class_management.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.js"></script>
					
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.css" type="text/css" />
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />';
        
        // start of ui
        $x .= '
				<div id="ClassInfoLayer">
				<form name="ClassNumberForm" id="ClassNumberForm">
			  <!-- ****** Content Board Start ****** -->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
            	<td class="main_content">
					  	<div class="navigation">
					  		<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
					  		<a href="index.php?AcademicYearID=' . $YearClass->AcademicYearID . '">' . $AcademicYear->Get_Academic_Year_Name() . ' ' . $Lang['SysMgr']['FormClassMapping']['ClassPageSubTitle'] . '</a>
					  		<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
					  		' . $YearClass->Get_Class_Name() . ' ' . $Lang['SysMgr']['FormClassMapping']['ClassInfo'] . '
					  		<br />
					  		<br />
					  	</div>
			  			<div class="content_top_tool">
								<div class="Conntent_tool">
									<!--<a href="#" class="export"> ' . $Lang['SysMgr']['FormClassMapping']['Export'] . '</a>-->
									<!--<a href="#" class="print"> ' . $Lang['SysMgr']['FormClassMapping']['Print'] . '</a>-->
								</div>
								<div class="Conntent_search">
                	<!--<input name="SearchValue" id="SearchValue" type="text"/>-->
                </div>
                <br style="clear:both" />
							</div>
              <div class="table_board">
                <table class="form_table">
                <tr>
              	  <td colspan="3" class="form_sub_title">
              	  	<em>' . $Lang['SysMgr']['FormClassMapping']['BasicClassInfo'] . '</em>
              	  	<p class="spacer"></p>
              	  	<span class="row_content tabletextremark"> 
              	  		' . Get_Last_Modified_Remark($YearClass->DateModified, $fcm->Get_UserName_By_UserID($YearClass->ModifyBy)) . '
              	  	</span>';
        if ($AcademicYear->IsPrevious != "1" || $showAddClassButton) {
            $x .= '        <div class="table_row_tool row_content_tool">
			                  <input alt="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Show_Class_Form(\'' . $YearClass->YearClassID . '\',\'' . $YearClass->YearID . '\',\'' . $YearClass->AcademicYearID . '\');" name="Edit" id="Edit" type="button" class="thickbox formsmallbutton" onmouseover="this.className=\'thickbox formsmallbuttonon\'" onmouseout="this.className=\'thickbox formsmallbutton\'" value="' . $Lang['SysMgr']['FormClassMapping']['EditThisClass'] . '" />
			                  <input onclick="Remove_Class(\'' . $YearClass->YearClassID . '\',\'' . $YearClass->AcademicYearID . '\');" name="RemoveClassBtn" id="RemoveClassBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="' . $Lang['SysMgr']['FormClassMapping']['RemoveThisClass'] . '" />
		                  </div>';
        }
        $x .= '
                  </td>
                </tr>
                <col class="field_title" />
                <col  class="field_c" />';
        $ClassList = $fcm->Get_Class_List_By_Academic_Year($YearClass->AcademicYearID);
        $x .= '     <tr>
	                <td>' . $Lang['SysMgr']['FormClassMapping']['ClassTitle'] . '</td>
	                <td>:</td>
	                <td ><span class="table_filter">
	                  <select name="YearClassSelectShortCut" id="YearClassSelectShortCut" class="formtextbox" onchange="Get_Class_Info(this.value);">';
        for ($i = 0; $i < sizeof($ClassList); $i ++) {
            if ($ClassList[$i]['YearID'] != $ClassList[$i - 1]['YearID']) {
                if ($i == 0)
                    $x .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
                else {
                    $x .= '</optgroup>';
                    $x .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
                }
            }
            
            unset($Selected);
            $Selected = ($ClassList[$i]['YearClassID'] == $YearClass->YearClassID) ? 'selected' : '';
            $x .= '<option value="' . $ClassList[$i]['YearClassID'] . '" ' . $Selected . '>' . Get_Lang_Selection($ClassList[$i]['ClassTitleB5'], $ClassList[$i]['ClassTitleEN']) . '</option>';
            
            if ($i == (sizeof($ClassList) - 1))
                $x .= '</optgroup>';
        }
        $x .= '
                    </select>
                    </span></td>
                </tr>';
        if ($sys_custom['Class']['ClassGroupSettings']) {
            $ClassGroupInfo = $YearClass->Get_Class_Group_Info();
            $ClassGroupTitle = Get_Lang_Selection($ClassGroupInfo[0]["TitleCh"], $ClassGroupInfo[0]["TitleEn"]);
            $x .= '
				<tr>
                  <td>' . $Lang['SysMgr']['FormClassMapping']['ClassGroup'] . '</td>
                  <td>:</td>
                  <td>
                  	' . $ClassGroupTitle . '
                	</td>
                </tr>';
        }

        if ($plugin['eSchoolBus']) {
            $classBuildingInfo = $YearClass->Get_Class_Building_Info();
            $buildingName = $classBuildingInfo[0]['BuildingName'];
            $x .= '
				<tr>
                  <td>' . $Lang['SysMgr']['Location']['Building'] . '</td>
                  <td>:</td>
                  <td>
                  	' . $buildingName. '
                    </td>
                </tr>';
        }
        
        
        if ($sys_custom['iPf']['CurriculumsSettings']) {
            global $intranet_root;
            include_once ($PATH_WRT_ROOT.'includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php');
            $lpf_dbs = new libpf_dbs_transcript();
            $CurriculumArr = $lpf_dbs->getCurriculumSetting($YearClass->CurriculumID);
            if($YearClass->CurriculumID > 0 && !empty($CurriculumArr)) {
                $CurriculumTitle = Get_Lang_Selection($CurriculumArr[0]['NameCh'], $CurriculumArr[0]['NameEn']);
            }
            $x .= '
				<tr>
                  <td>' . $Lang['SysMgr']['FormClassMapping']['Curriculum']. '</td>
                  <td>:</td>
                  <td>
                  	' . $CurriculumTitle. '
                	</td>
                </tr>';
        }
        $x .= '	<tr>
                  <td>' . $Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] . '</td>
                  <td>:</td>
                  <td>
                  	' . $YearClass->ClassWEBSAMSCode . '
                	</td>
                </tr>
                <tr>
                  <td>' . $Lang['SysMgr']['FormClassMapping']['ClassTeacher'] . '</td>
                  <td>:</td>
                  <td>';
        for ($i = 0; $i < sizeof($YearClass->ClassTeacherList); $i ++) {
            $TeacherName = $YearClass->ClassTeacherList[$i]['TeacherName'];
            $TeacherRemark = ($YearClass->ClassTeacherList[$i]['DeletedUser']) ? '<font style="color:red;">*</font>' : '';
            if ($i != 0)
                $x .= ', <br>';
            $x .= $TeacherRemark . $TeacherName;
        }
        $x .= '
                  </td>
                </tr>
                <tr>
                  <td>' . $Lang['SysMgr']['FormClassMapping']['ClassStudent'] . '</td>
                  <td>:</td>
                  <td>';
        if (($AcademicYear->IsPrevious != "1" || $showAddClassButton) && sizeof($YearClass->ClassStudentList) > 0) {
            $x .= '     	<div class="table_row_tool row_content_tool">
			              	<input onclick="$(\'span.WarningImage\').show(); $(\'input#EditClassNumberSubmit\').show(); $(\'input#EditClassNumberCancel\').show(); this.style.display = \'none\'; $(\'span.ClassNumberLayer\').hide(); $(\'span.ClassNumberInput\').show();" name="EditClassNumber" id="EditClassNumber" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="' . $Lang['SysMgr']['FormClassMapping']['EditClassNumber'] . '" />
			              	<input onclick="Reset_Class_Number();" name="EditClassNumberSubmit" id="EditClassNumberSubmit" style="display:none;" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="' . $Lang['Btn']['Submit'] . '" />
			              	<input onclick="$(\'span.WarningImage\').hide(); $(\'input#EditClassNumber\').show(); $(\'input#EditClassNumberSubmit\').hide(); $(\'input#EditClassNumberCancel\').hide(); this.style.display = \'none\'; $(\'span.ClassNumberLayer\').show(); $(\'span.ClassNumberInput\').hide();" name="EditClassNumberCancel" id="EditClassNumberCancel" style="display:none;" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
		                </div>';
        }
        $x .= '         <table class="common_table_list">
                    <thead>
                      <tr>
                        <th width="10%">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>
                        <th width="40%">' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</th>
                        <th class="dummy_row">&nbsp;</th>
                        <th width="10%">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>
                        <th width="40%">' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</th>
                      </tr>
                    </thead>
                    <tbody>';
        for ($i = 0; $i < ((sizeof($YearClass->ClassStudentList) / 2)); $i ++) {
            $FirstIndex = $i;
            $SecondIndex = round(sizeof($YearClass->ClassStudentList) / 2) + $i;
            $UserName1 = $YearClass->ClassStudentList[$FirstIndex]['StudentName'];
            $UserName2 = $YearClass->ClassStudentList[$SecondIndex]['StudentName'];
            $ClassNumber1 = (trim($YearClass->ClassStudentList[$FirstIndex]['ClassNumber']) == "") ? $Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet'] : $YearClass->ClassStudentList[$FirstIndex]['ClassNumber'];
            if (trim($YearClass->ClassStudentList[$SecondIndex]['UserID']) != "")
                $ClassNumber2 = (trim($YearClass->ClassStudentList[$SecondIndex]['ClassNumber']) == "") ? $Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet'] : $YearClass->ClassStudentList[$SecondIndex]['ClassNumber'];
            else
                $ClassNumber2 = '&nbsp;';
            $x .= '         <tr>
                        <td width="10%">
                        	<span class="ClassNumberLayer">' . $ClassNumber1 . '</span>';
            if ($YearClass->ClassStudentList[$FirstIndex]['ArchiveUser'] == '0') {
                $x .= '           <span class="ClassNumberInput" style="display:none;">
                        		<input type="hidden" name="UserID[]" value="' . $YearClass->ClassStudentList[$FirstIndex]['UserID'] . '">
                        		<input name="ClassNumber[]" value="' . $ClassNumber1 . '" style="width:50px;" onkeyup="Check_Class_Number(' . $YearClass->ClassStudentList[$FirstIndex]['UserID'] . ',this.value);">
                        	</span>
                        	<span class="WarningImage" style="color:red;" id="' . $YearClass->ClassStudentList[$FirstIndex]['UserID'] . 'Warning" name="ClassNumberWarning[]">
                        	</span>';
            }
            $x .= '           </td>
                        <td width="40%">' . $UserName1 . '&nbsp;</td>
                        <td class="dummy_row">&nbsp;</td>
                        <td width="10%">
                        	<span class="ClassNumberLayer">' . $ClassNumber2 . '</span>';
            if ($YearClass->ClassStudentList[$SecondIndex]['UserID'] != "" && $YearClass->ClassStudentList[$SecondIndex]['ArchiveUser'] == '0') {
                $x .= '           <span class="ClassNumberInput" style="display:none;">
                        		<input type="hidden" name="UserID[]" value="' . $YearClass->ClassStudentList[$SecondIndex]['UserID'] . '">
                        		<input name="ClassNumber[]" value="' . $ClassNumber2 . '" style="width:50px;" maxlength="9" onkeyup="Check_Class_Number(' . $YearClass->ClassStudentList[$SecondIndex]['UserID'] . ',this.value);">
                        	</span>
                        	<span class="WarningImage" style="color:red;" id="' . $YearClass->ClassStudentList[$SecondIndex]['UserID'] . 'Warning" name="ClassNumberWarning[]">
                        	</span>';
            }
            $x .= '           </td>
                        <td width="40%">' . $UserName2 . '&nbsp;</td>
                      </tr>';
        }
        $x .= '						</tbody>
                  </table>  
                  ' . $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . '
                  <br />
                  <br />
                <a href="#"></a> 
                </td>
              </tr>
              
              <tr>
                <td colspan="3" class="form_sub_title"><em>' . $Lang['SysMgr']['FormClassMapping']['ReferenceInfo'] . '</em>
                  <div class="table_row_tool row_content_tool">
	                  <input name="submit" type="button" class="formsmallbutton" onclick="Go_Subject_Detail(\'' . $YearClass->YearClassID . '\'); return false;" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="' . $Lang['SysMgr']['FormClassMapping']['ViewDetails'] . '" />
                  </div>
                </td>
              </tr>
              <tr>
                <td> ' . $Lang['SysMgr']['FormClassMapping']['RelatedSubjects'] . '</td>
                <td>:</td>
                <td>';
        
        // retrieve term's data
        $ay = new academic_year($YearClass->AcademicYearID);
        $this_term_ary = $ay->Get_Term_List();
        
        foreach ($this_term_ary as $this_term_id => $this_term_name) {
            $x .= $this_term_name;
            $x .= '<table class="common_table_list">
					          <tr>
					            <th>' . $Lang['SysMgr']['FormClassMapping']['Subject'] . '</th>
					            <th>' . $Lang['SysMgr']['FormClassMapping']['NoOfStudentsTaken'] . '</th>
					          </tr>';
            
            $SubjectTaken = $scm->Get_Number_Of_Subject_Taken_With_FormRelationChecking($YearClass->YearClassID, $YearClass->AcademicYearID, $returnDetail = true, $this_term_id);
            $sql = "SELECT SubjectID FROM SUBJECT_YEAR_RELATION rel INNER JOIN YEAR_CLASS yc ON (yc.YearID=rel.YearID) WHERE yc.YearClassID='" . $YearClass->YearClassID . "'";
            $sbj = $scm->returnVector($sql);
            
            for ($i = 0; $i < sizeof($sbj); $i ++) {
                $Subject = new subject($sbj[$i]);
                $x .= '<tr>
					      <td>' . $Subject->Get_Subject_Desc() . '</td>
					      <td>' . ($SubjectTaken[$sbj[$i]] ? $SubjectTaken[$sbj[$i]] : 0) . '</td>
					    </tr>';
            }
            
            if (sizeof($sbj) == 0) {
                $x .= '<tr><td colspan="2" height="40"><div align="center">' . $Lang['SysMgr']['FormClassMapping']['FormSubjectSettingNotComplete'] . '</div></td></tr>';
            }
            
            $x .= '      </table><br>';
        }
        
        $x .= '</td>
                </tr>
              </table>
        		<p class="spacer"></p>
						</div></td>
                    </tr>
                  </table>
			  <!-- ****** Content Board End  ****** -->
			  </form>
			  </div>
				';
        
        // layer to fake thick box script
        $x .= '<div class="FakeLayer"></div>';
        
        return $x;
    }

    function Get_Form_Class_Management_List($AcademicYearID = "")
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $linterface, $sys_custom, $button_cancel, $special_feature, $UserID;
        
        $NoCheckCurrent = (trim($AcademicYearID) == "") ? false : true;
        
        if ($AcademicYearID != '') {
            $objAcademicYear = new academic_year($AcademicYearID);
            $showImport = ($objAcademicYear->Has_Academic_Year_Past()) ? false : true;
        } else {
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $showImport = true;
        }
        if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] || (isset($special_feature['CanEditPreviousYearInClass']) && in_array($UserID, $special_feature['CanEditPreviousYearInClass'])))
            $showImport = true;
        
        $fcm = new form_class_manage();
        
        $x = '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/' . $LAYOUT_SKIN . '/js/form_class_management.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.tablednd_0_5.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
					
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.css" type="text/css" />
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />';
        
        $x .= '<!-- ****** Content Board Start ****** -->
					<form id="form1" name="form1" method="post">
					<div id="FormClassListLayer">
					<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					  <td class="main_content"><div class="content_top_tool">
						<div class="Conntent_tool">
							<div id="import_button_div">';
        // import Form/Class
        if ($showImport) {
            // Grouped 3 import functions into layer, by Henry Chow on 20100617
            /*
             * $x .= $linterface->GET_LNK_IMPORT("javascript:importFormClass();", $Lang['SysMgr']['FormClassMapping']['ImportFormClass']);
             * $x .= $linterface->GET_LNK_IMPORT("javascript:importClassStudent();", $Lang['SysMgr']['FormClassMapping']['ImportClassStudent']);
             * $x .= $linterface->GET_LNK_IMPORT("javascript:importClassTeacher();", $Lang['SysMgr']['FormClassMapping']['ImportClassTeacher']);
             */
            $x .= '<div class="Conntent_tool"><a href="javascript:;"   class="import" onClick="checkArrowLayer(\'importImgID\',\'importArrow\',\'import_option\')" onMouseOver="swapImage(\'importImgID\',\'importArrow\')" onMouseOut="swapImage(\'importImgID\',\'importArrow\')">' . $Lang['SysMgr']['FormClassMapping']['Import'] . '<img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/selectbox_arrow_open.gif" align="absmiddle" border="0" id="importArrow"></a><div class="print_option_layer2" id="import_option" style="visibility:hidden"><table><col class="field_title" /><col class="field_c" /><tr><td><a href="javascript:importFormClass()" class="blank_content"><img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/btn_sub.gif" align="absmiddle" border="0">' . $Lang['SysMgr']['FormClassMapping']['ImportFormClass'] . '</a></td></tr><tr><td><a href="javascript:importClassStudent()" class="blank_content"><img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/btn_sub.gif" align="absmiddle" border="0">' . $Lang['SysMgr']['FormClassMapping']['ImportClassStudent'] . '</a></td></tr><tr><td><a href="javascript:importClassTeacher()" class="blank_content"><img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/btn_sub.gif" align="absmiddle" border="0">' . $Lang['SysMgr']['FormClassMapping']['ImportClassTeacher'] . '</a></td></tr></table></div></div>';
            $x .= '<input type="hidden" name="importImgID" id="importImgID" value="0">';
        }
        $x .= "</div>";
        
        // Export
        $x .= '<div class="Conntent_tool"><a href="javascript:;"  onClick="checkArrowLayer(\'exportImgID\',\'exportArrow\',\'export_option\')" class="export" onMouseOver="swapImage(\'exportImgID\',\'exportArrow\')" onMouseOut="swapImage(\'exportImgID\',\'exportArrow\')" onClick="checkExportLayer(\'exportArrow\')">' . $Lang['SysMgr']['FormClassMapping']['Export'] . '<img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/selectbox_arrow_open.gif" align="absmiddle" border="0" id="exportArrow"></a><div class="print_option_layer2" id="export_option" style="visibility:hidden"><table><tr><td><a href="javascript:exportClassStudent()" class="blank_content"><img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/btn_sub.gif" align="absmiddle" border="0">' . $Lang['SysMgr']['FormClassMapping']['ExportClassStudent'] . '</a></td></tr><tr><td><a href="javascript:exportClassTeacher()" class="blank_content"><img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/btn_sub.gif" align="absmiddle" border="0">' . $Lang['SysMgr']['FormClassMapping']['ExportClassTeacher'] . '</a></td></tr></table></div></div>';
        
        $x .= '<input type="hidden" name="exportImgID" id="exportImgID" value="0">';
        
        // Copy from other academic year
        $x .= '<div class="Conntent_tool"><div><a href="javascript:copyFromOtherYear()" class="copy">' . $Lang['SysMgr']['FormClassMapping']['CopyFromOtherAcademicYear'] . '</a></div></div>';
        
        $x .= '				
							<!--<a href="#TB_inline?height=400&width=800&inlineId=FakeLayer" onclick="Show_Form_List(); return false;" class="thickbox new"> ' . $Lang['SysMgr']['FormClassMapping']['NewForm'] . '</a>-->
							<!--<a href="#" class="import"> ' . $Lang['SysMgr']['FormClassMapping']['Import'] . '</a>-->
							<!--<a href="#" class="export"> ' . $Lang['SysMgr']['FormClassMapping']['Export'] . '</a>-->
							<!-- dimed on 20090423 as feature considered useless-->
							<!--<a href="#TB_inline?height=400&width=800&inlineId=FakeLayer" onclick="Show_Name_Of_Class(); return false;" class="thickbox new"> ' . $Lang['SysMgr']['FormClassMapping']['NameOfClass'] . '</a>-->
						</div>
						<div class="Conntent_search">';
        
        if ($sys_custom['Form']['StageSettings'] == true) {
            // Form Stage Settings
            $x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "tablelink", $Lang['SysMgr']['FormClassMapping']['FormStageSettings'], "js_Show_Form_Stage_Settings_Layer(); return false;", "FakeLayer", $Lang['SysMgr']['FormClassMapping']['FormStageSettings']);
        }
        
        if ($sys_custom['Class']['ClassGroupSettings'] == true) {
            // Class Group Settings
            $x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "tablelink", $Lang['SysMgr']['FormClassMapping']['ClassGroupSettings'], "js_Show_Class_Group_Settings_Layer(); return false;", "FakeLayer", $Lang['SysMgr']['FormClassMapping']['ClassGroupSettings']);
        }
        
        $x .= '				<!--<input name="SearchKeyword" id="SearchKeyword" type="text"/>-->
						</div>
					  <br style="clear:both" />
						<div class="table_board">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="bottom">
									<div class="table_filter">';
        
        // get school year selection
        $SchoolYearList = $fcm->Get_Academic_Year_List();
        $x .= '				<select name="SchoolYear" id="SchoolYear" onchange="Get_Form_Class_List();">';
        $x .= '					<option value="">' . $Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'] . '</option>';
        for ($i = 0; $i < sizeof($SchoolYearList); $i ++) {
            $SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'], $SchoolYearList[$i]['YearNameEN']);
            
            if (! $NoCheckCurrent) {
                if ($SchoolYearList[$i]['CurrentSchoolYear'] == '1')
                    $AcademicYearID = $SchoolYearList[$i]['AcademicYearID'];
            }
            
            unset($Selected);
            $Selected = ($AcademicYearID == $SchoolYearList[$i]['AcademicYearID']) ? 'selected' : '';
            $x .= '<option value="' . $SchoolYearList[$i]['AcademicYearID'] . '" ' . $Selected . '>' . $SchoolYearName . '</option>';
        }
        $x .= '				</select>';
        
        $x .= '
									</div>
								</td>
								<td valign="bottom">&nbsp;</td>
							</tr>
						</table>';
        
        // db table start
        $x .= '<div id="DetailLayer">';
        $x .= $this->Get_Form_Class_Table($AcademicYearID);
        $x .= '</div>';
        // db table end
        
        $x .= '
					 	</td>
					</tr>
					</table>
					</div>
					</form>
			';
        
        // layer to fake thick box script
        $x .= '<div class="FakeLayer"></div>';
        
        return $x;
    }

    function Get_Teacher_Ownership_Box($teacherList)
    {
        global $i_ec_file_exist_teacher, $i_ec_file_not_exist_teacher, $i_ec_file_no_transfer;
        $row_e = $teacherList['in_class'];
        $row_i = $teacherList['out_class']; // debug_r($row_e);
        
        $teacher_exist_list = "<select id='teacher_benefit' name='teacher_benefit'>\n";
        $teacher_exist_list .= "<option value='0'>" . $i_ec_file_no_transfer . "</option>\n";
        $teacher_exist_list .= "<option value=''>____________________</option>";
        $teacher_exist_list .= "<option value=''>&nbsp;-- " . $i_ec_file_exist_teacher . " --&nbsp;</option>\n";
        $cours_teacher_size = sizeof($row_e);
        $exclus_email = array();
        for ($i = 0; $i < $cours_teacher_size; $i ++) {
            $teacher_exist_list .= "<option value='" . $row_e[$i][0] . "'>" . $row_e[$i][1] . "</option>\n";
            $exclus_email[] = trim($row_e[$i][2]);
        }
        $teacher_exist_list .= "<option value=''>____________________</option>";
        $teacher_exist_list .= "<option value=''>&nbsp;-- " . $i_ec_file_not_exist_teacher . " --&nbsp;</option>\n";
        for ($i = 0; $i < sizeof($row_i); $i ++) {
            if (! in_array($row_i[$i][3], $exclus_email)) {
                $chiname = ($row_i[$i][2] == "" ? "" : "(" . $row_i[$i][2] . ")");
                $teacher_exist_list .= "<option value='" . $row_i[$i][0] . "'>" . $row_i[$i][1] . " $chiname</option>\n";
            }
        }
        
        $teacher_exist_list .= "</select>\n";
        $teacher_exist_list .= "<input type='hidden' id='noClasseTeacher' name='noClasseTeacher' value='" . $cours_teacher_size . "'>";
        return $teacher_exist_list;
    }

    function Get_Class_Form($YearClassID = '', $YearID = '', $AcademicYearID = '')
    {
        global $Lang, $i_transfer_file_ownership, $sys_custom, $PATH_WRT_ROOT, $plugin;
        global $i_StudentPromotion_Sorting_BoysName, $i_StudentPromotion_Sorting_GirlsName, $i_StudentPromotion_Sorting_Name;
        
        $isKIS = $_SESSION["platform"] == "KIS";
        
        include_once ('libclass.php');
        
        $Year = new Year($YearID);
        $YearClass = new year_class($YearClassID, false, true, true);
        // $has_eclass = $YearClass->has_eclass();
        $fcm = new form_class_manage();
        $PreviousAcademicYearID = $fcm->Get_Previous_Academic_Year($AcademicYearID);
        $x = '<div class="edit_pop_board" style="height:395px;">
					<form name="ClassForm" id="ClassForm" onsubmit="return false;">
					<input type=hidden name="AcademicYearID" id="AcademicYearID" value="' . $AcademicYearID . '">
					<input type=hidden name="YearID" id="YearID" value="' . $YearID . '">
					<input type=hidden name="YearClassID" id="YearClassID" value="' . $YearClassID . '">
					<!--<h1> ' . $Lang['SysMgr']['FormClassMapping']['Class'] . ' &gt; <span>' . $Lang['SysMgr']['FormClassMapping']['NameOfClass'] . '</span> </h1>-->
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:365px">
						<table class="form_table">
							<col class="field_title" />
							<col  class="field_c" />
							<tr>
								<td>' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>
								<td>:</td>
								<td>' . $Year->YearName . '</td>
								<td ></td>
							</tr>
							<tr>
								<td>' . $Lang['SysMgr']['FormClassMapping']['ClassTitleEN'] . '</td>
								<td>:</td>
								<td ><input name="ClassTitleEN" type="text" id="ClassTitleEN" value="' . htmlspecialchars($YearClass->ClassTitleEN, ENT_QUOTES) . '" class="textbox" onkeyup="Check_Name_Duplicate(\'EN\',this.value,\'' . $YearClassID . '\',\'' . $AcademicYearID . '\');"/></td>
							</tr>
							<tr id="ClassTitleENWarningRow" name="ClassTitleENWarningRow" style="display:none;">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>
								<span id="ClassTitleENWarningLayer" name="ClassTitleENWarningLayer" style="color:red;"></span>
								</td>
							</tr>
							<tr>
								<td>' . $Lang['SysMgr']['FormClassMapping']['ClassTitleB5'] . '</td>
								<td>:</td>
								<td ><input name="ClassTitleB5" type="text" id="ClassTitleB5" value="' . htmlspecialchars($YearClass->ClassTitleB5, ENT_QUOTES) . '" class="textbox" onkeyup="Check_Name_Duplicate(\'B5\',this.value,\'' . $YearClassID . '\',\'' . $AcademicYearID . '\');"/></td>
							</tr>
							<tr id="ClassTitleB5WarningRow" name="ClassTitleB5WarningRow" style="display:none;">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>
								<span id="ClassTitleB5WarningLayer" name="ClassTitleB5WarningLayer" style="color:red;"></span>
								</td>
							</tr>
							<tr ' . ($isKIS ? 'style="display:none;"' : '') . '>
								<td>' . $Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] . '</td>
								<td>:</td>
								<td ><input name="WEBSAMSCode" type="text" id="WEBSAMSCode" maxlength="5" value="' . htmlspecialchars($YearClass->ClassWEBSAMSCode, ENT_QUOTES) . '" class="textbox"/></td>
							</tr>';
        
        if ($sys_custom['Class']['ClassGroupSettings'] == true) {
            include_once ($PATH_WRT_ROOT . 'includes/libclassgroup.php');
            $libClassGroup = new Class_Group();
            $ClassGroupSelection = $libClassGroup->Get_Class_Group_Selection('ClassGroupID', $YearClass->ClassGroupID, $Onchange = '', $noFirst = 0);
            
            $x .= '<tr>';
            $x .= '<td>' . $Lang['SysMgr']['FormClassMapping']['ClassGroup'] . '</td>';
            $x .= '<td>:</td>';
            $x .= '<td >' . $ClassGroupSelection . '</td>';
            $x .= '</tr>';
        }
        
        // [2017-1207-0956-53277]
        if($sys_custom['iPf']['CurriculumsSettings'] == true) {
            global $intranet_root;
            include_once ($PATH_WRT_ROOT.'includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php');
            $lpf_dbs = new libpf_dbs_transcript();
            
            include_once ($PATH_WRT_ROOT.'includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php');
            $lpf_dbs_ui = new libpf_dbs_transcript_ui();
            $lpf_dbs_ui->setObjFunction($lpf_dbs);
            $CurriculumSelection = $lpf_dbs_ui->getCurriculumSelection('CurriculumID', $YearClass->CurriculumID);
            
            $x .= '<tr>';
                $x .= '<td>' . $Lang['SysMgr']['FormClassMapping']['Curriculum'] . '</td>';
                $x .= '<td>:</td>';
                $x .= '<td >' . $CurriculumSelection. '</td>';
            $x .= '</tr>';
        }

        if ($plugin['eSchoolBus']) {
            include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");
            $llocation_ui = new liblocation_ui();
            $locationBuilding = $llocation_ui->Get_Building_Selection($YearClass->BuildingID, "BuildingID", $ParOnchange='', $noFirst=0, $WithOthersOpt=0, $ParFirstTitle='', $HasFloorOnly=0);
            $x .= '<tr>';
                $x .= '<td>' . $Lang['SysMgr']['Location']['Building']. '</td>';
                $x .= '<td>:</td>';
                $x .= '<td >' . $locationBuilding. '</td>';
            $x .= '</tr>';
        }
        
        $x .= '	<tr>
								<td>' . $Lang['SysMgr']['FormClassMapping']['ClassTeacher'] . '</td>
								<td>:</td>
								<td>';
        
        // get teacher list
        $TeacherList = $fcm->Get_Teaching_Staff_List();
        $x .= '<select name="TeacherList" id="TeacherList" onchange="Add_Teaching_Teacher();">
						<option value="" selected="selected">' . $Lang['SysMgr']['FormClassMapping']['AddTeacher'] . '</option>';
        for ($i = 0; $i < sizeof($TeacherList); $i ++) {
            $Hide = false;
            for ($j = 0; $j < sizeof($YearClass->ClassTeacherList); $j ++) {
                if ($TeacherList[$i]['UserID'] == $YearClass->ClassTeacherList[$j]['UserID']) {
                    $Hide = true;
                    break;
                }
            }
            if (! $Hide)
                $x .= '<option value="' . $TeacherList[$i]['UserID'] . '">' . $TeacherList[$i]['Name'] . '</option>';
        }
        $x .= '</select>';
        
        $x .= '			<p class="spacer"></p>';
        // tempory selection box to store class teacher
        $x .= '     <select name="SelectedClassTeacher[]" size="3" id="SelectedClassTeacher[]" style="width:50%" multiple="true">';
        for ($i = 0; $i < sizeof($YearClass->ClassTeacherList); $i ++) {
            $x .= '<option value="' . $YearClass->ClassTeacherList[$i]['UserID'] . '">' . $YearClass->ClassTeacherList[$i]['TeacherName'] . '</option>';
        }
        $x .= '			</select>';
        
        $x .= '			<p class="spacer"></p>
								<input onclick="Remove_Teaching_Teacher();" name="submit2" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="' . $Lang['Btn']['RemoveSelected'] . '"/></td></tr>';
        /*
         * if($has_eclass){
         * $teacher_List = $YearClass->Get_Existing_Teacher_List();
         * $listBox = $this->Get_Teacher_Ownership_Box($teacher_List);
         * $x .= '<tr>
         * <td>'.$i_transfer_file_ownership.'</td><td>:</td>
         * <td>
         * <div id="transferOwnership" style="display:block;">
         * <input type="hidden" name="is_user_import" id="is_user_import" value="0">
         * '.$listBox.'
         * </div>
         * </td>
         * </tr>
         * ';
         * }
         */
        $x .= '
							<tr>
								<td>' . $Lang['SysMgr']['FormClassMapping']['ClassStudent'] . '</td>
								<td>:</td>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td width="50%" bgcolor="#EEEEEE">';
        
        // get class list
        /*
         * $YearClassList = $fcm->Get_Class_List_By_Academic_Year($PreviousAcademicYearID);
         *
         * $x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();">';
         * $x .= '<option value="">'.$Lang['SysMgr']['FormClassMapping']['SelectPreviousYearClass'].'</option>';
         * for ($i=0; $i< sizeof($YearClassList); $i++) {
         * if ($YearClassList[$i]['YearID'] != $YearClassList[$i-1]['YearID']) {
         * if ($i == 0)
         * $x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
         * else {
         * $x .= '</optgroup>';
         * $x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
         * }
         * }
         *
         * $x .= '<option value="'.$YearClassList[$i]['YearClassID'].'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
         *
         * if ($i == (sizeof($YearClassList)-1))
         * $x .= '</optgroup>';
         * }
         * $x .= '</select>';
         */
        
        // ## Student source selection (Add from student without class / Add from class of previous year)
        $x .= '<select id="StudentSourceSelect" class="formtextbox" onchange="js_Change_Student_Source(this.value);">';
        $x .= '<option value="1">' . $Lang['SysMgr']['FormClassMapping']['SelectStudentWithoutClass'] . '</option>';
        $x .= '<option value="2">' . $Lang['SysMgr']['FormClassMapping']['SelectPreviousYearClass'] . '</option>';
        $x .= '</select>';
        
        $x .= '
												</td>
											<td width="40"></td>
											<td width="50%" bgcolor="#EFFEE2" class="steptitletext">' . $Lang['SysMgr']['FormClassMapping']['StudentSelected'] . ' </td>
										</tr>
										<tr>
											<td bgcolor="#EEEEEE" align="center">';
        
        // class student list of selected class
        // $x .= ' <div id="ClassStudentList">
        // <select name="ClassStudent" id="ClassStudent" size="10" style="width:99%" multiple="true">';
        // $x .= ' </select>
        // </div>';
        
        // previous year class selection
        $lclass = new libclass();
        $thisAttr = ' id="YearClassSelect" name="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();" ';
        $PreviousYearClassSelection = $lclass->getSelectClassID($thisAttr, $selected = "", $DisplaySelect = 1, $PreviousAcademicYearID);
        $x .= '<div id="PreviousYearClassSelectionDiv" style="display:none;">';
        $x .= $PreviousYearClassSelection;
        $x .= '</div>';
        
        $x .= '<div id="ClassStudentList">';
        $x .= $this->Get_Student_Without_Class_Selection('ClassStudent', '', $AcademicYearID);
        $x .= '</div>';
        
        $x .= '
												<span class="tabletextremark">' . $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'] . ' </span>
											</td>
											<td><input name="AddAll" onclick="Add_All_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="' . $Lang['Btn']['AddAll'] . '"/>
												<br />
												<input name="Add" onclick="Add_Selected_Class_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="' . $Lang['Btn']['AddSelected'] . '"/>
												<br /><br />
												<input name="Remove" onclick="Remove_Selected_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="' . $Lang['Btn']['RemoveSelected'] . '"/>
												<br />
												<input name="RemoveAll" onclick="Remove_All_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="' . $Lang['Btn']['RemoveAll'] . '"/>
											</td>
											<td bgcolor="#EFFEE2">';
        // selected student list
        $x .= '<select name="StudentSelected[]" id="StudentSelected[]" size="10" style="width:99%" multiple="true">';
        for ($i = 0; $i < sizeof($YearClass->ClassStudentList); $i ++) {
            // $TempUser = new libuser($YearClass->ClassStudentList[$i]['UserID']);
            $StudentName = $YearClass->ClassStudentList[$i]['StudentName'];
            $x .= '<option value="' . $YearClass->ClassStudentList[$i]['UserID'] . '">' . $StudentName . '</option>';
        }
        $x .= '</select>';
        $x .= '						</td>
										</tr>
										<tr>
											<td width="50%" bgcolor="#EEEEEE">';
        $x .= $Lang['SysMgr']['FormClassMapping']['Or'] . '<Br>';
        $x .= $Lang['SysMgr']['FormClassMapping']['SearchStudent'];
        
        $x .= '
												<!--<div class="Conntent_search" style="float:left;">-->
												<div style="float:left;">
													<input type="text" id="StudentSearch" name="StudentSearch" value=""/>
												</div>
											</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</table>
									<p class="spacer"></p>
									</td>
							</tr>';
        if ($YearClassID != "") {
            $x .= '		<tr>
									<td>' . $Lang['SysMgr']['FormClassMapping']['StudentClassNumber'] . '</td>
									<td>:</td>
									<td> 
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod1" value="ByName"/>
										<label for="ClassNumberMethod1">' . $Lang['SysMgr']['FormClassMapping']['ClassNumFollowName'] . '</label>
										<br/>
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod3" value="ByBoysName"/>
										<label for="ClassNumberMethod3">' . $i_StudentPromotion_Sorting_BoysName . '</label>
										<br/>
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod4" value="ByGirlsName"/>
										<label for="ClassNumberMethod4">' . $i_StudentPromotion_Sorting_GirlsName . '</label>
										<br/>
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod2" value="ByTime" checked="checked"/>
										<label for="ClassNumberMethod2">' . $Lang['SysMgr']['FormClassMapping']['ClassNumFollowTime'] . '</label>
										
									</td>
								</tr>
								<tr id="ClassNumberMethodWarningRow" name="ClassNumberMethodWarningRow" style="display:none;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>
									<span id="ClassNumberMethodWarningLayer" name="ClassNumberMethodWarningLayer" style="color:red;"></span>
									</td>
								</tr>';
        } else {
            $x .= '		<tr>
									<td>' . $Lang['SysMgr']['FormClassMapping']['StudentClassNumber'] . '</td>
									<td>:</td>
									<td> 
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod1" value="ByName"  checked="checked" />
										<label for="ClassNumberMethod1">' . $i_StudentPromotion_Sorting_Name . '</label>
										<br/>
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod3" value="ByBoysName" />
										<label for="ClassNumberMethod3">' . $i_StudentPromotion_Sorting_BoysName . '</label>
										<br/>
										<input type="radio" name="ClassNumberMethod" id="ClassNumberMethod4" value="ByGirlsName""/>
										<label for="ClassNumberMethod4">' . $i_StudentPromotion_Sorting_GirlsName . '</label>
										
									</td>
								</tr>
								<tr id="ClassNumberMethodWarningRow" name="ClassNumberMethodWarningRow" style="display:none;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>
									<span id="ClassNumberMethodWarningLayer" name="ClassNumberMethodWarningLayer" style="color:red;"></span>
									</td>
								</tr>';
        }
        $x .= '	</table>
						<br />
					</form>
					</div>
					<div class="edit_bottom">';
        
        if ($YearClassID != "") {
            // last updated date information
            $x .= '<span>' . Get_Last_Modified_Remark($YearClass->DateModified, $fcm->Get_UserName_By_UserID($YearClass->ModifyBy)) . '</span>';
        }
        
        $x .= '
					<p class="spacer"></p>';
        
        if ($YearClassID == "")
            $x .= '	<input onclick="Create_Class();" name="ClassSubmit" id="ClassSubmit" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Submit'] . '" />';
        else
            $x .= '	<input onclick="Edit_Class();" name="ClassSubmit" id="ClassSubmit" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Submit'] . '" />';
        
        $x .= '	<input onclick="window.top.tb_remove();" name="ClassCancel" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
						<!--<input name="submit2" type="button" class="formbutton" onclick="return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Remove this class" />-->
						<p class="spacer"></p>
					</div>
				</div>';
        
        return $x;
    }

    function Get_Form_Class_Table($AcademicYearID, $SearchValue = '')
    {
        global $Lang, $sys_custom, $special_feature, $UserID;
        
        if ($_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] || (isset($special_feature['CanEditPreviousYearInClass']) && in_array($UserID, $special_feature['CanEditPreviousYearInClass']))) {
            $showAddClassButton = true;
        }
        
        if ($AcademicYearID != "") {
            $fcm = new form_class_manage();
            $scm = new subject_class_mapping();
            $AcademicYear = new academic_year($AcademicYearID);
            
            $isKIS = ($_SESSION["platform"] == 'KIS') ? true : false;
            
            $x .= '<table class="common_table_list ClassDragAndDrop">
								<tr>
									<th style="width:25%;">&nbsp;</th>
	      			    <th class="sub_row_top" style="width:18%;">' . $Lang['SysMgr']['FormClassMapping']['Title'] . '</th>
	      			    <th class="sub_row_top" style="width:12%;">' . $Lang['SysMgr']['FormClassMapping']['ClassTeacher'] . '</th>
	      			    <th class="sub_row_top" style="width:25%;">' . $Lang['SysMgr']['FormClassMapping']['NumberOfStudent'] . '</th>';
            if (! $special_feature['form_class_hide_no_subject_taken'] && ! $isKIS) {
                $x .= ' <th class="sub_row_top" style="width:15%;">' . $Lang['SysMgr']['FormClassMapping']['SubjectTaken'] . '</th>';
            }
            // [2017-1207-0956-53277]
            if ($sys_custom['iPf']['CurriculumsSettings']) {
                $x .= ' <th class="sub_row_top" style="width:15%;">' . $Lang['SysMgr']['FormClassMapping']['Curriculum'] . '</th>';
            }
            
            $colspan = ($special_feature['form_class_hide_no_subject_taken'] || $isKIS) ? 4 : 5;
            
            // [2017-1207-0956-53277]
            if($sys_custom['iPf']['CurriculumsSettings']) {
                $colspan++;
                
                global $PATH_WRT_ROOT, $intranet_root;
                include_once ($PATH_WRT_ROOT.'includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php');
                $lpf_dbs = new libpf_dbs_transcript();
            }
            
            $x .= '<th class="sub_row_top" style="width:5%;">&nbsp;</th>
	   			      </tr>
	   			      <thead>
								<tr>
								<th style="width:25%"><span class="row_content">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</span>
								   <span class="table_row_tool row_content_tool" style="float:right;">
								   	<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Show_Form_List(); return false;" class="setting_row thickbox" title="' . $Lang['SysMgr']['FormClassMapping']['EditForm'] . '">
								   	</a>
								   </span>
								</th>
								<th colspan="' . $colspan. '" class="sub_row_top">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>
								</tr>
								</thead>';
            // $x .= ' </table>';
            // Form list start
            $FormList = $fcm->Get_Form_List();
            for ($i = 0; $i < sizeof($FormList); $i ++) {
                $ClassList = $fcm->Get_Class_List($AcademicYearID, $FormList[$i]['YearID'], $SearchValue);
                
                $RowSpan = (sizeof($ClassList) > 0) ? 'rowspan="' . (sizeof($ClassList) + 2) . '"' : '';
                $FirstDisplay = (sizeof($ClassList) > 0) ? 'display:none;' : '';
                // $x .= '<table class="common_table_list ClassDragAndDrop" id="ClassListTable'.$i.'">';
                $x .= '		<tbody>';
                $x .= '			<tr class="nodrag nodrop">
											<td width="25%" ' . $RowSpan . '>' . $FormList[$i]['YearName'] . '</td>
											<td width="18%" style="' . $FirstDisplay . '">&nbsp;</td>
											<td width="12%" style="' . $FirstDisplay . '">&nbsp;</td>
											<td width="25%" style="' . $FirstDisplay . '">&nbsp;</td>';
                if (! $special_feature['form_class_hide_no_subject_taken'] && ! $isKIS) {
                    $x .= ' <td width="15%" style="' . $FirstDisplay . '">&nbsp;</td>';
                }
                // [2017-1207-0956-53277]
                if ($sys_custom['iPf']['CurriculumsSettings']) {
                    $x .= ' <td width="15%" style="' . $FirstDisplay . '">&nbsp;</td>';
                }
                $x .= '	<td width="5%" style="' . $FirstDisplay . '">';
                if ($showAddClassButton || $AcademicYear->IsPrevious != '1') {
                    $x .= '<div class="table_row_tool row_content_tool">
											<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox add_dim" title="' . $Lang['SysMgr']['FormClassMapping']['AddClass'] . '" onclick="Show_Class_Form(\'\',\'' . $FormList[$i]['YearID'] . '\',\'' . $AcademicYearID . '\'); return false;"></a>
											</div>';
                } else {
                    $x .= '&nbsp;';
                }
                $x .= '				</td>
										</tr>';
                for ($j = 0; $j < sizeof($ClassList); $j ++) {
                    $x .= '<tr class="sub_row" id="' . $ClassList[$j]['YearClassID'] . '">';
                    $x .= '	 	<td><a href="class_info.php?YearClassID=' . $ClassList[$j]['YearClassID'] . '">' . Get_Lang_Selection($ClassList[$j]['ClassTitleB5'], $ClassList[$j]['ClassTitleEN']) . '&nbsp;</a></td>';
                    $x .= '	 	<td>' . $ClassList[$j]['TeacherName'] . '&nbsp;</td>
										<td>' . $ClassList[$j]['NumberOfStudent'] . '</td>';
                    // $NumberOfSubjectTaken = $scm->Get_Number_Of_Subject_Taken($ClassList[$j]['YearClassID'],$AcademicYearID);
                    // $NumberOfSubjectTaken = $scm->Get_Number_Of_Subject_Taken_With_FormRelationChecking($ClassList[$j]['YearClassID'],$AcademicYearID,$returnDetail=false);
                    $NumberOfSubjectTaken = $scm->getAmountOfSubjectTakenByYearClassID($ClassList[$j]['YearClassID']);
                    if (! $special_feature['form_class_hide_no_subject_taken'] && ! $isKIS) {
                        $x .= '		<td><a href="subject_detail.php?YearClassID=' . $ClassList[$j]['YearClassID'] . '">' . $NumberOfSubjectTaken . '</a></td>';
                    }
                    // [2017-1207-0956-53277]
                    if ($sys_custom['iPf']['CurriculumsSettings']) {
                        $CurriculumDisplay = '&nbsp;';
                        $ClassCurriculum = $lpf_dbs->getCurriculumSetting($ClassList[$j]['CurriculumID']);
                        if($ClassList[$j]['CurriculumID'] && !empty($ClassCurriculum)) {
                            $CurriculumDisplay = $ClassCurriculum[0]['Code'];
                        }
                        $x .= '     <td nowrap>' . $CurriculumDisplay. '</td>';
                    }
                    $x .= '		<td class="Dragable"><div class="table_row_tool"><a href="#" class="move_order_dim" title="' . $Lang['SysMgr']['FormClassMapping']['Move'] . '"></a></div></td>
								</tr>';
                }
                
                if (sizeof($ClassList) > 0) {
                    $x .= '<tr class="nodrag nodrop">
												<td width="18%">&nbsp;</td>
												<td width="12%">&nbsp;</td>
												<td width="25%">&nbsp;</td>';
                    if (! $special_feature['form_class_hide_no_subject_taken'] && ! $isKIS) {
                        $x .= '					<td width="15%">&nbsp;</td>';
                    }
                    // [2017-1207-0956-53277]
                    if ($sys_custom['iPf']['CurriculumsSettings']) {
                        $x .= '                 <td width="15%">&nbsp;</td>';
                    }
                    $x .= '						<td width="5%">';
                    if ($showAddClassButton || $AcademicYear->IsPrevious != '1') {
                        $x .= '				<div class="table_row_tool row_content_tool">
													<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox add_dim" title="' . $Lang['SysMgr']['FormClassMapping']['AddClass'] . '" onclick="Show_Class_Form(\'\',\'' . $FormList[$i]['YearID'] . '\',\'' . $AcademicYearID . '\'); return false;"></a>
													</div>';
                    } else {
                        $x .= '&nbsp;';
                    }
                    $x .= '				</td>
											</tr>';
                }
                $x .= '		</tbody>';
                // $x .= ' </table>';
            }
            
            $x .= '</table>
						' . $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . '';
        } else {
            $x = '';
        }
        
        return $x;
    }

    // dimed on 20090423, as feature considered useless
    function Get_Name_Of_Class_Form()
    {
        /*
         * global $Lang;
         *
         * $fcm = new form_class_manage();
         *
         * $NameOfClassList = $fcm->Get_Name_Of_Class_List();
         * $x = '<div class="edit_pop_board">
         * <h1> '.$Lang['SysMgr']['FormClassMapping']['Class'].' &gt; <span>'.$Lang['SysMgr']['FormClassMapping']['NameOfClass'].'</span> </h1>
         * <div class="edit_pop_board_write">
         * <div id="NameOfClassTableLayer">
         * <table id="NameOfClassTable" class="common_table_list">
         * <thead>
         * <tr>
         * <th>#</th>
         * <th>'.$Lang['SysMgr']['FormClassMapping']['NameOfClassEng'].'</th>
         * <th>'.$Lang['SysMgr']['FormClassMapping']['NameOfClassChi'].'</th>
         * <th>&nbsp;</th>
         * </tr>
         * </thead>
         * <tbody>';
         *
         * if (sizeof($NameOfClassList) == 0) {
         * $x .= '<tr id="AddNameOfClassRow" style="display:none; visibility:none;">
         * <td colspan=4>&nbsp;</td>
         * </tr>';
         * }
         * else {
         * for ($i=0; $i< sizeof($NameOfClassList); $i++) {
         * $x .= '<tr '.(($i == (sizeof($NameOfClassList)-1))? 'id="AddNameOfClassRow"':'').'>';
         * $x .= '<td>';
         * $x .= $NameOfClassList[$i]['Sequence'].'&nbsp;';
         * $x .= '</td>';
         * $x .= '<td>';
         * $x .= '<div id="RenameEnNameOfClassDiv'.$NameOfClassList[$i]['ClassID'].'" style="visibility:hidden; display:none;">';
         * $x .= '<input type="hidden" id="DefaultEnNameOfClass'.$NameOfClassList[$i]['ClassID'].'" value="'.$NameOfClassList[$i]['ClassNameEN'].'"/>';
         * $x .= '<input type="text" id="NewEnNameOfClass'.$NameOfClassList[$i]['ClassID'].'" class="textbox" value="" onBlur="Go_Rename_Field(\''.$NameOfClassList[$i]['ClassID'].'\',\'EnNameOfClass\');" />';
         * $x .= '</div>';
         * $x .= '<div id="EnNameOfClassDiv'.$NameOfClassList[$i]['ClassID'].'" onclick="Rename_Field(\''.$NameOfClassList[$i]['ClassID'].'\',\'EnNameOfClass\');">';
         * $x .= $NameOfClassList[$i]['ClassNameEN'].'&nbsp;';
         * $x .= '</div>';
         * $x .= '</td>';
         * $x .= '<td>';
         * $x .= '<div id="RenameB5NameOfClassDiv'.$NameOfClassList[$i]['ClassID'].'" style="visibility:hidden; display:none;">';
         * $x .= '<input type="hidden" id="DefaultB5NameOfClass'.$NameOfClassList[$i]['ClassID'].'" value="'.$NameOfClassList[$i]['ClassNameB5'].'"/>';
         * $x .= '<input type="text" id="NewB5NameOfClass'.$NameOfClassList[$i]['ClassID'].'" class="textbox" value="" onBlur="Go_Rename_Field(\''.$NameOfClassList[$i]['ClassID'].'\',\'B5NameOfClass\');" />';
         * $x .= '</div>';
         * $x .= '<div id="B5NameOfClassDiv'.$NameOfClassList[$i]['ClassID'].'" onclick="Rename_Field(\''.$NameOfClassList[$i]['ClassID'].'\',\'B5NameOfClass\');">';
         * $x .= $NameOfClassList[$i]['ClassNameB5'].'&nbsp;';
         * $x .= '</div>';
         * $x .= '</td>';
         * $x .= '<td>';
         * if ($NameOfClassList[$i]['MappedClass'] == 0) {
         * $x .= '<div class="table_row_tool row_content_tool">';
         * $x .= '<a href="#" class="delete_dim" title="Delete" onclick="Delete_Name_Of_Class(\''.$NameOfClassList[$i]['ClassID'].'\'); return false;"></a>';
         * $x .= '</div>';
         * //$x .= $Lang['SysMgr']['FormClassMapping']['Delete'];
         * //$x .= '</a>';
         * }
         * else
         * $x .= '&nbsp;';
         * $x .= ' </td>
         * </tr>';
         * }
         * }
         * $x .= ' </tbody>
         * </table>
         * </div>';
         *
         * // new Name of Class Layer
         * $x .= '<div id="NewNameOfClassList" class="Conntent_tool">';
         * $x .= '<a href="#" onclick="Add_Name_Of_Class_Row(); return false;" class="new">';
         * $x .= $Lang['SysMgr']['FormClassMapping']['NameOfClass'];
         * $x .= '</a>';
         * $x .= '</div>';
         * $x .= '</div>';
         * $x .= '</div>';
         *
         *
         * return $x;
         */
    }

    function Get_Form_List_Form()
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
        include_once ("libinterface.php");
        $libinterface = new interface_html();
        
        $lui = new interface_html();
        
        $x .= '<div class="edit_pop_board" style="height:380px;">';
        $x .= $lui->Get_Thickbox_Return_Message_Layer();
        // $x .= '<h1> '.$Lang['SysMgr']['FormClassMapping']['Class'].' &gt; <span>x'.$Lang['SysMgr']['FormClassMapping']['FormSetting'].'</span> </h1>
        $x .= '<div class="edit_pop_board_write" style="height:330px;">
						<div id="FormListTableLayer">';
        $x .= $this->Get_Form_List_Table();
        $x .= '	</div>';
        
        $x .= '</div>';
        $x .= '</div>';
        
        // the action button > close
        $htmlAry['closeBtn'] = $libinterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn");
        
        $x .= '<div class="edit_bottom_v30">';
        $x .= '<span>' . $LastModifiedDisplay . '</span>';
        $x .= '<p class="spacer"></p>';
        $x .= $htmlAry['closeBtn'];
        $x .= '<p class="spacer"></p>';
        $x .= '</div>';
        
        return $x;
    }

    function Get_Form_List_Table()
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom;
        
        $isKIS = $_SESSION["platform"] == "KIS";
        
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        $libinterface = new interface_html();
        
        if ($sys_custom['Form']['StageSettings'] == true) {
            include_once ($PATH_WRT_ROOT . "includes/libformstage.php");
            $libFormStage = new Form_Stage();
        }
        
        $fcm = new form_class_manage();
        $FormList = $fcm->Get_Form_List(true, 0);
        
        $x = '';
        $x .= '<table id="FormListTable" class="common_table_list" style="width:97%">';
        $x .= '<thead>';
        $x .= '<tr>';
        $x .= '<th width="40%">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</th>';
        $x .= '<th width="30%" ' . ($isKIS ? 'style="display:none;"' : '') . '>' . $Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] . '</th>';
        if ($sys_custom['Form']['StageSettings'] == true) {
            $x .= '<th width="20%">' . $Lang['SysMgr']['FormClassMapping']['FormStage'] . '</th>';
        }
        $x .= '<th width="17%">&nbsp;</th>';
        $x .= '</tr>';
        $x .= '</thead>';
        $x .= '<tbody>';
        
        if (sizeof($FormList) == 0) {
            /*
             * $x .= '<tr id="AddFormRow" style="display:none; visibility:none;">
             * <td colspan=4>&nbsp;</td>
             * </tr>';
             */
        } else {
            for ($i = 0; $i < sizeof($FormList); $i ++) {
                $thisYearID = $FormList[$i]['YearID'];
                $thisWEBSAMSCode = $FormList[$i]['WEBSAMSCode'];
                $thisRecordStatus = $FormList[$i]['RecordStatus'];
                $thisStatusDisplay = ($thisRecordStatus == 1) ? $Lang['SysMgr']['FormClassMapping']['InUse'] : $Lang['SysMgr']['FormClassMapping']['NotInUse'];
                
                $x .= '<tr id="' . $thisYearID . '">';
                $x .= '<td>';
                $x .= '<div id="' . $thisYearID . '" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);" class="jEditInput">';
                $x .= $FormList[$i]['YearName'];
                $x .= '</div>';
                $x .= '<div id="' . $thisYearID . 'WarningLayer" style="display:none; color:red;">';
                $x .= '</div>';
                $x .= '</td>';
                $x .= '<td ' . ($isKIS ? 'style="display:none;"' : '') . '>';
                $x .= '<div id="' . $thisYearID . '_WebSAMSCode" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);" class="jEditSelect_WebSAMSCode">';
                $x .= $thisWEBSAMSCode;
                $x .= '</div>';
                $x .= '</td>';
                if ($sys_custom['Form']['StageSettings'] == true) {
                    $thisFormStageID = $FormList[$i]['FormStageID'];
                    if ($thisFormStageID == '') {
                        $thisFormStageName = 'NA';
                    } else {
                        $thisFormStageArr = $libFormStage->Get_Name_By_ID($thisFormStageID);
                        $thisFormStageName = Get_Lang_Selection($thisFormStageArr[0]['TitleCh'], $thisFormStageArr[0]['TitleEn']);
                    }
                    
                    $x .= '<td>';
                    $x .= '<div id="' . $thisYearID . '_FormStageID" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);" class="jEditSelect_FormStage">';
                    $x .= $thisFormStageName;
                    $x .= '</div>';
                    $x .= '</td>';
                }
                $x .= '<td class="Dragable" align="left">';
                $x .= '<div class="table_row_tool">' . "\n";
                // Enable / Disable Icon
                // $thisID = 'ActiveStatus_'.$thisYearID;
                // $thisJS = 'javascript:js_Update_Form_Info('.$thisYearID.')';
                // $x .= $libinterface->GET_LNK_ACTIVE($thisJS, $thisRecordStatus, $thisStatusDisplay, 0, $thisID)."\n";
                //
                // $x .= '<span> | </span>'."\n";
                
                $x .= '<span class="table_row_tool"><a href="#" class="move_order_dim" title="' . $Lang['SysMgr']['FormClassMapping']['Move'] . '"></a></span>';
                // if ($FormList[$i]['MappedClass'] == 0) {
                if (! $fcm->Check_Class_AcademicYear_RecordStatus($thisYearID) && ! ($isKIS && $sys_custom['KIS_Admission']['HideDeleteFormBtnInSchSetting'])) {
                    $x .= '<span class="table_row_tool row_content_tool">';
                    $x .= '<a href="#" class="delete_dim" title="Delete" onclick="Delete_Form(\'' . $thisYearID . '\'); return false;"></a>';
                    $x .= '</span>';
                    // $x .= $Lang['SysMgr']['FormClassMapping']['Delete'];
                    // $x .= '</a>';
                } else {
                    $x .= '&nbsp;';
                }
                $x .= '</div>' . "\n";
                $x .= '				</td>
										</tr>';
            }
        }
        
        $x .= '<tr id="AddFormRow">';
        $x .= '<td>&nbsp;</td>';
        $x .= '<td ' . ($isKIS ? 'style="display:none;"' : '') . '>&nbsp;</td>';
        if ($sys_custom['Form']['StageSettings'] == true) {
            $x .= '<td>&nbsp;</td>';
        }
        $x .= '<td>';
        $x .= '<div class="table_row_tool row_content_tool">';
        $x .= '<a href="#" onclick="Add_Form_Row(); return false;" class="add_dim" title="' . $Lang['SysMgr']['FormClassMapping']['NewForm'] . '"></a>';
        $x .= '</div>';
        $x .= '</td>';
        $x .= '</tr>';
        $x .= '		</tbody>
						</table>
					<input type="hidden" id="RecordOrder" name="RecordOrder" value="">';
        return $x;
    }

    function Get_Class_Student_Selection($YearClassID, $PrevYearClassID, $SelectedStudentList, $AcademicYearID)
    {
        $fcm = new form_class_manage();
        
        $UserList = $fcm->Get_Avaliable_Year_Class_User($YearClassID, $PrevYearClassID, $SelectedStudentList, $AcademicYearID);
        $x .= '<select name="ClassStudent" id="ClassStudent" size="10" style="width:99%" multiple="true">';
        for ($i = 0; $i < sizeof($UserList); $i ++) {
            $TempUser = new libuser($UserList[$i]['UserID']);
            $x .= '<option value="' . $UserList[$i]['UserID'] . '">';
            $x .= $TempUser->UserNameLang();
            $x .= '</option>';
        }
        $x .= '</select>';
        
        return $x;
    }

    function Get_Subject_Taken_Detail($YearClass, $YearTermID = "")
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom, $UserID;
        
        $fcm = new form_class_manage();
        $scm = new subject_class_mapping();
        $linterface = new interface_html();
        
        $AcademicYear = new academic_year($YearClass->AcademicYearID);
        $allSemesters = getSemesters($YearClass->AcademicYearID);
        $allSemesterKey = array_keys($allSemesters);
        for ($i = 0; $i < sizeof($allSemesters); $i ++) {
            $semesterAry[$i][] = $allSemesterKey[$i];
            $semesterAry[$i][] = $allSemesters[$allSemesterKey[$i]];
        }
        
        $thisSemester = ($YearTermID == "") ? GetCurrentSemesterID() : $YearTermID;
        
        // allow to edit previous subject group if
        // i) Current semester; ii) future date; iii) with flag
        if ($thisSemester == GetCurrentSemesterID() || (is_array($sys_custom['SubjectGroup']['CanEditPastSubjectGroupUserIDArr']) && in_array($UserID, $sys_custom['SubjectGroup']['CanEditPastSubjectGroupUserIDArr']))) {
            $allowEdit = 1;
        } else {
            $startdate = getStartDateOfAcademicYear($YearClass->AcademicYearID, $thisSemester);
            if (date('Y-m-d') <= $startdate) {
                $allowEdit = 1;
            }
        }
        
        $x = '<div id="SubjectTakenDetailLayer">';
        $x .= '		<form name="Form_Class_Subject" id="Form_Class_Subject" method="post" action="subject_group_update.php">
					<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					<td class="main_content">
				  <div class="navigation">
				  	<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	<a href="index.php?AcademicYearID=' . $YearClass->AcademicYearID . '">' . $AcademicYear->Get_Academic_Year_Name() . ' ' . $Lang['SysMgr']['FormClassMapping']['ClassPageSubTitle'] . '</a>
				  	<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	<a href="class_info.php?YearClassID=' . $YearClass->YearClassID . '">' . $YearClass->Get_Class_Name() . ' ' . $Lang['SysMgr']['FormClassMapping']['ClassInfo'] . '</a>
				  	<img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	' . $Lang['SysMgr']['FormClassMapping']['SubjectTakenPageTitle'] . '
				  	<br />
				  </div>
					<div class="content_top_tool">
						<!--<div class="Conntent_tool">
							<a href="#" class="export"> ' . $Lang['SysMgr']['FormClassMapping']['Export'] . '</a>
							<a href="#" class="print"> ' . $Lang['SysMgr']['FormClassMapping']['Print'] . '</a>
						</div>
			            <div class="Conntent_search">
			              <input name="SearchValue" type="text"/>
			            </div>-->
            <br style="clear:both" />
					</div>
          <div class="table_board">
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        $ClassList = $fcm->Get_Class_List_By_Academic_Year($YearClass->AcademicYearID);
        $x .= '     <tr>
	                <td valign="bottom"><div class="table_filter">
	                	' . $Lang['SysMgr']['FormClassMapping']['ClassTitle'] . '
	                	:
	                  <select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_Subject_Taken_Detail();">';
        for ($i = 0; $i < sizeof($ClassList); $i ++) {
            if ($ClassList[$i]['YearID'] != $ClassList[$i - 1]['YearID']) {
                if ($i == 0)
                    $x .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
                else {
                    $x .= '</optgroup>';
                    $x .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
                }
            }
            
            unset($Selected);
            $Selected = ($ClassList[$i]['YearClassID'] == $YearClass->YearClassID) ? 'selected' : '';
            $x .= '<option value="' . $ClassList[$i]['YearClassID'] . '" ' . $Selected . '>' . Get_Lang_Selection($ClassList[$i]['ClassTitleB5'], $ClassList[$i]['ClassTitleEN']) . '</option>';
            
            if ($i == (sizeof($ClassList) - 1))
                $x .= '</optgroup>';
        }
        
        $SubjectTaken = $scm->Get_Number_Of_Subject_Taken($YearClass->YearClassID, $YearClass->AcademicYearID, true);
        $x .= '
                    </select> ';
        $x .= getSelectByArray($semesterAry, ' name="YearTermID" id="YearTermID" onchange="Get_Subject_Taken_Detail();"', $thisSemester, 0, 1);
        $x .= '              </div></td>
                </tr>
							</table>';
        /*
         * $x .= ' <table class="common_table_list">
         * <thead>
         * <tr>
         * <th rowspan="2" class="num_check" width="1%">#</th>
         * <th rowspan="2" width="6%"><a href="#">'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</a></th>
         * <th rowspan="2"><a href="#">'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</a></th>
         * <th colspan="'.(sizeof($SubjectTaken)+1).'">'.$Lang['SysMgr']['FormClassMapping']['SubjectsApplied'].'</th>
         * </tr>';
         *
         * $x .= ' <tr>';
         * for ($i=0;$i< sizeof($SubjectTaken); $i++) {
         * $Subject = new subject($SubjectTaken[$i]['SubjectID']);
         * $x .= ' <th>'.$Subject->Get_Subject_Short_Form().'</th>';
         * }
         * $x .= ' <th>'.$Lang['SysMgr']['FormClassMapping']['Total'].'<br /></th>
         * </tr>
         * </thead>
         * <tbody>';
         *
         * $UserList = $YearClass->ClassStudentList;
         * for ($i=0; $i< sizeof($UserList); $i++) {
         * $UserName = $UserList[$i]['StudentName'];
         * $x .= '<tr>
         * <td>'.($i+1).'</td>
         * <td>'.$UserList[$i]['ClassNumber'].'&nbsp;</td>
         * <td width="127">'.$UserName.'</td>';
         * $SubjectTakenTotal = 0;
         * for ($j=0; $j< sizeof($SubjectTaken); $j++) {
         * $TakenGroup = $scm->Get_Subject_Group_Name_Taken($UserList[$i]['UserID'],$SubjectTaken[$j]['SubjectID'],$YearClass->AcademicYearID);
         * $x .= ' <td>';
         * if (count($TakenGroup) == 0)
         * $x .= '&nbsp;';
         * else
         * {
         * $numOfTakenGroup = count($TakenGroup);
         * $TmpNameArr = array();
         * for ($k=0; $k<$numOfTakenGroup; $k++)
         * {
         * $thisSubjectGroupName = Get_Lang_Selection($TakenGroup[$k]['ClassTitleB5'],$TakenGroup[$k]['ClassTitleEN']);
         * $thisTermName = Get_Lang_Selection($TakenGroup[$k]['YearTermNameB5'],$TakenGroup[$k]['YearTermNameEN']);
         * $TmpNameArr[] = $thisSubjectGroupName.' ('.$thisTermName.')';
         * }
         * $x .= implode('<br />', $TmpNameArr);
         * }
         * $x .= '</td>';
         *
         * //if (Trim(Get_Lang_Selection($TakenGroup[0]['ClassTitleB5'],$TakenGroup[0]['ClassTitleEN'])) != "")
         * $SubjectTakenTotal += count($TakenGroup);
         * }
         *
         * $x .= ' <td>'.$SubjectTakenTotal.'</td>
         * </tr>';
         * }
         *
         *
         * $x .= '
         * </tbody>
         * </table>';
         */
        $x .= $this->Get_Edit_Class_SubjectGroup_Table($YearClass, $thisSemester);
        
        if ($sys_custom['SyncSubjectGroupToClassroom']) {
            $checked = ($sys_custom['SubjectGroup']['DefaultSyncStudentToClassroom']) ? 'checked="checked"' : '';
            $x .= "<input type='checkbox' value='1' name='addStd' id='addStd' {$checked}> <label for=\"addStd\">{$Lang['SysMgr']['SubjectClassMapping']['newStd2eclass']}</label>";
        }
        $x .= '<br />';
        $x .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
        $x .= '</div>';
        
        if ($this->WithStudentInClass && $this->WithFormSubjectSetting) {
            $x .= '<div class="edit_bottom_v30">';
            // $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", "", "", "formbutton_v30")."&nbsp;";
            if ($allowEdit) {
                $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "Update_Class_Subject_Detail()", "", "", "", "formbutton_v30") . "&nbsp;";
            }
            $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location.href='class_info.php?YearClassID=" . $YearClass->YearClassID . "'", "", "", "", "formbutton_v30");
            $x .= '</div>';
        }
        $x .= '</form>';
        $x .= '</div>';
        
        $x .= '		
					</td>
					</tr>
					</table>';
        
        return $x;
    }

    /*
     * 20100211 Ivan: Please use function getSelectClass() in libclass.php for future development
     */
    function Get_YearClass_Selection($ID_Name, $SelectedYearClassID = '', $Onchange = '', $AcademicYearID = '', $noFirst = 0)
    {
        global $Lang;
        
        // Use current Academic year if not specified
        if ($AcademicYearID == '') {
            $AcademicYearArr = getCurrentAcademicYearAndYearTerm();
            $AcademicYearID = $AcademicYearArr['AcademicYearID'];
        }
        
        $onchange = '';
        if ($Onchange != '')
            $onchange = 'onchange="' . $Onchange . '"';
        
        $fcm = new form_class_manage();
        $ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
        
        $selection = '';
        $selection = '<select name="' . $ID_Name . '" id="' . $ID_Name . '" class="formtextbox" ' . $onchange . '>';
        
        if (! $noFirst) {
            $selection .= '<option value="">' . Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['AllClass']) . '</option>';
        }
        
        for ($i = 0; $i < sizeof($ClassList); $i ++) {
            if ($ClassList[$i]['YearID'] != $ClassList[$i - 1]['YearID']) {
                if ($i == 0)
                    $selection .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
                else {
                    $selection .= '</optgroup>';
                    $selection .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
                }
            }
            
            unset($Selected);
            $Selected = ($ClassList[$i]['YearClassID'] == $SelectedYearClassID) ? 'selected' : '';
            $selection .= '<option value="' . $ClassList[$i]['YearClassID'] . '" ' . $Selected . '>' . Get_Lang_Selection($ClassList[$i]['ClassTitleB5'], $ClassList[$i]['ClassTitleEN']) . '</option>';
            
            if ($i == (sizeof($ClassList) - 1))
                $selection .= '</optgroup>';
        }
        
        $selection .= '</select>';
        
        return $selection;
    }

    function Get_Form_Selection($ID_Name, $SelectedYearID = '', $Onchange = '', $noFirst = 0, $isAll = 0, $isMultiple = 0)
    {
        global $Lang;
        $libYear = new Year();
        $FormArr = $libYear->Get_All_Year_List();
        $numOfForm = count($FormArr);
        
        $selectArr = array();
        
        if ($isAll)
            $selectArr['-1'] = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['All']['Form']);
        
        for ($i = 0; $i < $numOfForm; $i ++) {
            $thisYearID = $FormArr[$i]['YearID'];
            $thisYearName = $FormArr[$i]['YearName'];
            
            $selectArr[$thisYearID] = $thisYearName;
        }
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';
        
        if ($isMultiple == 1)
            $multiple = ' multiple size="10" ';
        
        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $multiple;
        $firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Form']);
        
        $formSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearID, $all = 0, $noFirst, $firstTitle);
        
        return $formSelection;
    }

    function Get_Class_Selection($AcademicYearID, $YearID = '', $ID_Name = '', $SelectedYearClassID = '', $Onchange = '', $noFirst = 0, $isMultiple = 0, $isAll = 0, $TeachingOnly = 0, $firstTitle = '', $style = '')
    {
        global $Lang;
        
        $fcm = new form_class_manage();
        $IsMultiForm = count($YearID) > 1;
        $classArr = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $YearID, $TeachingOnly);
        $numOfClass = count($classArr);
        $selectArr = array();
        for ($i = 0; $i < $numOfClass; $i ++) {
            $thisYearID = $classArr[$i]['YearID'];
            
            // Display classes of the specific Form only (Do in Get_Class_List_By_Academic_Year() now)
            // if ($thisYearID != $YearID)
            // continue;
            
            $thisYearClassID = $classArr[$i]['YearClassID'];
            $thisClassTitleEN = $classArr[$i]['ClassTitleEN'];
            $thisClassTitleB5 = $classArr[$i]['ClassTitleB5'];
            $thisClassTitleDisplay = Get_Lang_Selection($thisClassTitleB5, $thisClassTitleEN);
            
            if ($IsMultiForm)
                $selectArr[$classArr[$i]['YearName']][$thisYearClassID] = $thisClassTitleDisplay;
            else
                $selectArr[$thisYearClassID] = $thisClassTitleDisplay;
        }
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';
        
        if ($isMultiple)
            $style .= ' multiple="true" size="10"';
        
        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . $style;
        if ($firstTitle == '') {
            $firstTitle = ($isAll) ? $Lang['SysMgr']['FormClassMapping']['All']['Class'] : $Lang['SysMgr']['FormClassMapping']['Select']['Class'];
            $firstTitle = Get_Selection_First_Title($firstTitle);
        }
        // $classSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedBuildingID, $isAll, $noFirst, $firstTitle);
        $classSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearClassID, $isAll, $noFirst, $firstTitle);
        
        return $classSelection;
    }

    function Get_Staff_Selection($ID_Name, $isTeachingStaff = 1, $SelectedUserID = '', $Onchange = '', $noFirst = 0, $isMultiple = 0, $isAll = 0, $ParID = '', $ParClass = '')
    {
        global $Lang;
        
        $fcm = new form_class_manage();
        $staffArr = ($isTeachingStaff) ? $fcm->Get_Teaching_Staff_List() : $fcm->Get_Non_Teaching_Staff_List();
        $numOfStaff = count($staffArr);
        
        $selectArr = array();
        for ($i = 0; $i < $numOfStaff; $i ++) {
            $thisUserID = $staffArr[$i]['UserID'];
            $thisName = $staffArr[$i]['Name'];
            
            $selectArr[$thisUserID] = $thisName;
        }
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';
        
        $style = '';
        if ($isMultiple)
            $style = ' multiple="true" size="10"';
        
        if ($ParClass != '') {
            $class = ' class="' . $ParClass . '" ';
        }
        
        $SelectionID = ($ParID == '') ? $ID_Name : $ParID;
        $selectionTags = ' id="' . $SelectionID . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $style . ' ' . $class;
        
        if ($isTeachingStaff)
            $firstTitle = ($isAll) ? $Lang['SysMgr']['FormClassMapping']['All']['TeachingStaff'] : $Lang['SysMgr']['FormClassMapping']['Select']['TeachingStaff'];
        else
            $firstTitle = ($isAll) ? $Lang['SysMgr']['FormClassMapping']['All']['NonTeachingStaff'] : $Lang['SysMgr']['FormClassMapping']['Select']['NonTeachingStaff'];
        
        $firstTitle = Get_Selection_First_Title($firstTitle);
        
        $staffSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedUserID, $isAll, $noFirst, $firstTitle);
        
        return $staffSelection;
    }

    function Get_Student_Selection($ID_Name, $YearClassIDArr, $SelectedStudentID = '', $Onchange = '', $noFirst = 0, $isMultiple = 0, $isAll = 0, $ExcludedStudentIDArr = '', $IncludeStudentIDArr = '', $activeOnly = false)
    {
        global $Lang;
        
        /*
         * $objClass = new year_class($YearClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
         * $StudentArr = $objClass->ClassStudentList;
         * $numOfStudent = count($StudentArr);
         */
        
        $libFCM = new form_class_manage();
        
        $StudentInfoArr = $libFCM->Get_Student_By_Class($YearClassIDArr,$activeOnly);
        
        $numOfStudent = count($StudentInfoArr);
        
        $selectArr = array();
        for ($i = 0; $i < $numOfStudent; $i ++) {
            $thisUserID = $StudentInfoArr[$i]['UserID'];
            
            if ($ExcludedStudentIDArr != '' && in_array($thisUserID, (array) $ExcludedStudentIDArr)) {
                continue;
            }
            
            if ($IncludeStudentIDArr != '' && ! in_array($thisUserID, (array) $IncludeStudentIDArr)) {
                continue;
            }
            
            $thisClassName = Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleB5'], $StudentInfoArr[$i]['ClassTitleEN']);
            $thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
            
            $thisDisplay = $StudentInfoArr[$i]['StudentName'] . ' (' . $thisClassName . '-' . $thisClassNumber . ')';
            $selectArr[$thisUserID] = $thisDisplay;
        }
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';
        
        $style = '';
        if ($isMultiple)
            $style = ' multiple="true" size="10"';
        
        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . $style;
        
        // $firstTitle = ($isAll)? $Lang['SysMgr']['FormClassMapping']['All']['NonTeachingStaff'] : $Lang['SysMgr']['FormClassMapping']['Select']['NonTeachingStaff'];
        // $firstTitle = Get_Selection_First_Title($firstTitle);
        
        return getSelectByAssoArray($selectArr, $selectionTags, $SelectedStudentID, $isAll, $noFirst, $firstTitle);
    }

    function Get_Class_Group_Info_Layer()
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        include_once ($PATH_WRT_ROOT . "includes/libclassgroup.php");
        
        $libinterface = new interface_html();
        $libClassGroup = new Class_Group();
        
        $x = "";
        $x .= '<div id="debugArea"></div>';
        
        $x .= '<div class="edit_pop_board" style="height:360px;">';
        $x .= $libinterface->Get_Thickbox_Return_Message_Layer();
        // $x .= '<h1> '.$Lang['SysMgr']['Location']['Location'].' &gt; <span>'.$Lang['SysMgr']['Location']['Setting']['Building'].'</span></h1>';
        $x .= '<div id="ClassGroupInfoSettingLayer" class="edit_pop_board_write" style="height:330px;">';
        $x .= $this->Get_Class_Group_Info_Layer_Table();
        $x .= '</div>';
        $x .= '</div>';
        
        // the action button > close
        $htmlAry['closeBtn'] = $libinterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn");
        
        // Last Updated Info Div
        $LastModifiedInfoArr = $libClassGroup->Get_Last_Modified_Info();
        if ($LastModifiedInfoArr != NULL) {
            $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
            $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
            
            $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
            $x .= '<div class="edit_bottom_v30">';
            $x .= '<span>' . $LastModifiedDisplay . '</span>';
            $x .= '<p class="spacer"></p>';
            $x .= $htmlAry['closeBtn'];
            $x .= '<p class="spacer"></p>';
            $x .= '</div>';
        }
        
        return $x;
    }

    function Get_Class_Group_Info_Layer_Table()
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom;
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        include_once ($PATH_WRT_ROOT . "includes/libclassgroup.php");
        
        $libinterface = new interface_html();
        $libClassGroup = new Class_Group();
        $classGroupInfoArr = $libClassGroup->Get_All_Class_Group();
        $numOfClassGroup = count($classGroupInfoArr);
        
        $col_percent = array(
            20,
            30,
            30
        );
        if ($sys_custom['KIS_ClassGroupAttendanceType']) {
            $col_percent = array(
                20,
                20,
                20,
                20
            );
            
            $attendance_type_ary = array();
            foreach ($sys_custom['KIS_ClassGroupAttendanceType_Options'] as $key => $val) {
                $attendance_type_ary[] = array(
                    $key,
                    $val
                );
            }
        }
        
        $x .= '<table id="ClassGroupInfoTable" class="common_table_list" style="width:95%">';
        $x .= '<thead>';
        $x .= '<tr>';
        $x .= '<th style="width:' . $col_percent[0] . '%">' . $Lang['SysMgr']['Location']['Code'] . '</th>';
        $x .= '<th style="width:' . $col_percent[1] . '%">' . $Lang['SysMgr']['Location']['TitleEn'] . '</th>';
        $x .= '<th style="width:' . $col_percent[2] . '%">' . $Lang['SysMgr']['Location']['TitleCh'] . '</th>';
        if ($sys_custom['KIS_ClassGroupAttendanceType']) {
            $x .= '<th style="width:' . $col_percent[3] . '%">' . $Lang['StudentAttendance']['AttendanceType'] . '</th>';
        }
        $x .= '<th style="width:' . $this->toolCellWidth . '">&nbsp;</th>';
        $x .= '</tr>';
        $x .= '</thead>';
        
        $x .= '<tbody>';
        
        // Display Building Info
        if ($numOfClassGroup > 0) {
            $moveToolTips = $Lang['SysMgr']['FormClassMapping']['Reorder']['ClassGroup'];
            $deleteToolTips = $Lang['SysMgr']['FormClassMapping']['DeleteTips']['ClassGroup'];
            
            for ($i = 0; $i < $numOfClassGroup; $i ++) {
                $thisClassGroupID = $classGroupInfoArr[$i]['ClassGroupID'];
                $thisCode = $classGroupInfoArr[$i]['Code'];
                $thisTitleEn = $classGroupInfoArr[$i]['TitleEn'];
                $thisTitleCh = $classGroupInfoArr[$i]['TitleCh'];
                $thisIsActive = $classGroupInfoArr[$i]['RecordStatus'];
                $thisActiveIconTitle = ($thisIsActive == 1) ? $Lang['SysMgr']['FormClassMapping']['InUse'] : $Lang['SysMgr']['FormClassMapping']['NotInUse'];
                
                // show delete icon if the building has no linked floor
                $thisObjClassGroup = new Class_Group($thisClassGroupID);
                $subjectInfoArr = $thisObjClassGroup->Get_Class_List();
                $numOfSubject = count($subjectInfoArr);
                $showDeleteIcon = ($numOfSubject == 0) ? true : false;
                
                $x .= '<tr id="ClassGroupRow_' . $thisClassGroupID . '">';
                // Code
                $x .= '<td>';
                $x .= $libinterface->Get_Thickbox_Edit_Div("ClassGroupCode_" . $thisClassGroupID, "ClassGroupCode_" . $thisClassGroupID, "jEditCode", $thisCode);
                $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_" . $thisClassGroupID);
                $x .= '</td>';
                // Title (English)
                $x .= '<td>';
                $x .= $libinterface->Get_Thickbox_Edit_Div("ClassGroupTitleEn_" . $thisClassGroupID, "ClassGroupTitleEn_" . $thisClassGroupID, "jEditTitleEn", $thisTitleEn);
                $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEnWarningDiv_" . $thisClassGroupID);
                $x .= '</td>';
                // Title (Chinese)
                $x .= '<td>';
                $x .= $libinterface->Get_Thickbox_Edit_Div("ClassGroupTitleCh_" . $thisClassGroupID, "ClassGroupTitleCh_" . $thisClassGroupID, "jEditTitleCh", $thisTitleCh);
                $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleChWarningDiv_" . $thisClassGroupID);
                $x .= '</td>';
                
                if ($sys_custom['KIS_ClassGroupAttendanceType']) {
                    $x .= '<td>';
                    $x .= $libinterface->GET_SELECTION_BOX($attendance_type_ary, ' name="ClassGroupAttendanceType_' . $thisClassGroupID . '" id="ClassGroupAttendanceType_' . $thisClassGroupID . '" onchange="js_Update_Class_Group(\'AttendanceTypeWarningDiv_' . $thisClassGroupID . '\', \'AttendanceType\', this.options[this.selectedIndex].value);" ', $Lang['General']['NA'], $classGroupInfoArr[$i]['AttendanceType']);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("AttendanceTypeWarningDiv_" . $thisClassGroupID);
                    $x .= '</td>';
                }
                
                // Functional icons
                $x .= '<td class="Dragable" align="left">';
                $x .= '<div class="table_row_tool">';
                // Active / Inactive Icon Btn
                $thisID = 'ActiveStatus_' . $thisClassGroupID;
                $thisJS = "javascript:js_Change_Active_Status('" . $thisID . "', 'ClassGroup')";
                
                $x .= $libinterface->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);
                $x .= '<span> | </span>';
                
                // Move
                $x .= $libinterface->GET_LNK_MOVE("#", $moveToolTips, 0);
                
                // Delete
                if ($showDeleteIcon) {
                    $x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Class_Group('$thisClassGroupID'); return false;", '', 0);
                }
                $x .= '</div>';
                $x .= '</td>';
                $x .= '</tr>';
            }
        }
        
        // Add ClassGroup Button
        $x .= '<tr id="AddClassGroupRow" class="nodrop nodrag">';
        $x .= '<td>&nbsp;</td>';
        $x .= '<td>&nbsp;</td>';
        $x .= '<td>&nbsp;</td>';
        if ($sys_custom['KIS_ClassGroupAttendanceType']) {
            $x .= '<td>&nbsp;</td>';
        }
        $x .= '<td>';
        $x .= '<div class="table_row_tool row_content_tool">';
        $x .= '<a href="#" onclick="js_Add_Class_Group_Row(); return false;" class="add_dim" title="' . $Lang['SysMgr']['FormClassMapping']['Add']['ClassGroup'] . '"></a>';
        $x .= '</div>';
        $x .= '</td>';
        $x .= '</tr>';
        $x .= '</tbody>';
        $x .= '</table>';
        
        $x .= '<br />';
        
        /**
         * ****************** ORIGINAL CODE >>> now moved to [Get_Class_Group_Info_Layer()]
         * # Last Updated Info Div
         * $LastModifiedInfoArr = $libClassGroup->Get_Last_Modified_Info();
         * if ($LastModifiedInfoArr != NULL)
         * {
         * $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
         * $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
         *
         * $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
         * $x .= '<div class="edit_bottom">';
         * $x .= '<span>'.$LastModifiedDisplay.'</span>';
         * $x .= '<p class="spacer"></p>';
         * $x .= '</div>';
         * }
         * **********************************************************************
         */
        
        return $x;
    }

    function Get_Form_Stage_Info_Layer()
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        
        $libinterface = new interface_html();
        
        $x = "";
        $x .= '<div id="debugArea"></div>';
        
        $x .= '<div class="edit_pop_board" style="height:410px;">';
        $x .= $libinterface->Get_Thickbox_Return_Message_Layer();
        // $x .= '<h1> '.$Lang['SysMgr']['Location']['Location'].' &gt; <span>'.$Lang['SysMgr']['Location']['Setting']['Building'].'</span></h1>';
        $x .= '<div id="FormStageInfoSettingLayer" class="edit_pop_board_write" style="height:370px;">';
        $x .= $this->Get_Form_Stage_Info_Layer_Table();
        $x .= '</div>';
        $x .= '</div>';
        
        return $x;
    }

    function Get_Form_Stage_Info_Layer_Table()
    {
        global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        include_once ($PATH_WRT_ROOT . "includes/libformstage.php");
        
        $libinterface = new interface_html();
        $libFormStage = new Form_Stage();
        $FormStageInfoArr = $libFormStage->Get_All_Form_Stage();
        $numOfFormStage = count($FormStageInfoArr);
        
        $x .= '<table id="FormStageInfoTable" class="common_table_list" style="width:95%">';
        $x .= '<thead>';
        $x .= '<tr>';
        $x .= '<th style="width:20%">' . $Lang['SysMgr']['Location']['Code'] . '</th>';
        $x .= '<th style="width:30%">' . $Lang['SysMgr']['Location']['TitleEn'] . '</th>';
        $x .= '<th style="width:30%">' . $Lang['SysMgr']['Location']['TitleCh'] . '</th>';
        $x .= '<th style="width:' . $this->toolCellWidth . '">&nbsp;</th>';
        $x .= '</tr>';
        $x .= '</thead>';
        
        $x .= '<tbody>';
        
        // Display Form Stage Info
        if ($numOfFormStage > 0) {
            $moveToolTips = $Lang['SysMgr']['FormClassMapping']['Reorder']['FormStage'];
            $deleteToolTips = $Lang['SysMgr']['FormClassMapping']['DeleteTips']['FormStage'];
            
            for ($i = 0; $i < $numOfFormStage; $i ++) {
                $thisFormStageID = $FormStageInfoArr[$i]['FormStageID'];
                $thisCode = $FormStageInfoArr[$i]['Code'];
                $thisTitleEn = $FormStageInfoArr[$i]['TitleEn'];
                $thisTitleCh = $FormStageInfoArr[$i]['TitleCh'];
                $thisIsActive = $FormStageInfoArr[$i]['RecordStatus'];
                $thisActiveIconTitle = ($thisIsActive == 1) ? $Lang['SysMgr']['FormClassMapping']['InUse'] : $Lang['SysMgr']['FormClassMapping']['NotInUse'];
                
                // show delete icon if the building has no linked floor
                $thisObjFormStage = new Form_Stage($thisFormStageID);
                $subjectInfoArr = $thisObjFormStage->Get_Form_List();
                $numOfSubject = count($subjectInfoArr);
                $showDeleteIcon = ($numOfSubject == 0) ? true : false;
                
                $x .= '<tr id="FormStageRow_' . $thisFormStageID . '">';
                // Code
                $x .= '<td>';
                $x .= $libinterface->Get_Thickbox_Edit_Div("FormStageCode_" . $thisFormStageID, "FormStageCode_" . $thisFormStageID, "jEditCode", $thisCode);
                $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_" . $thisFormStageID);
                $x .= '</td>';
                // Title (English)
                $x .= '<td>';
                $x .= $libinterface->Get_Thickbox_Edit_Div("FormStageTitleEn_" . $thisFormStageID, "FormStageTitleEn_" . $thisFormStageID, "jEditTitleEn", $thisTitleEn);
                $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEnWarningDiv_" . $thisFormStageID);
                $x .= '</td>';
                // Title (Chinese)
                $x .= '<td>';
                $x .= $libinterface->Get_Thickbox_Edit_Div("FormStageTitleCh_" . $thisFormStageID, "FormStageTitleCh_" . $thisFormStageID, "jEditTitleCh", $thisTitleCh);
                $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleChWarningDiv_" . $thisFormStageID);
                $x .= '</td>';
                
                // Functional icons
                $x .= '<td class="Dragable" align="left">';
                $x .= '<div class="table_row_tool">';
                // Active / Inactive Icon Btn
                $thisID = 'ActiveStatus_' . $thisFormStageID;
                $thisJS = "javascript:js_Change_Active_Status('" . $thisID . "', 'FormStage')";
                
                $x .= $libinterface->GET_LNK_ACTIVE($thisJS, $thisIsActive, $thisActiveIconTitle, 0, $thisID);
                $x .= '<span> | </span>';
                
                // Move
                $x .= $libinterface->GET_LNK_MOVE("#", $moveToolTips, 0);
                
                // Delete
                if ($showDeleteIcon) {
                    $x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Form_Stage('$thisFormStageID'); return false;", '', 0);
                }
                $x .= '</div>';
                $x .= '</td>';
                $x .= '</tr>';
            }
        }
        
        // Add FormStage Button
        $x .= '<tr id="AddFormStageRow" class="nodrop nodrag">';
        $x .= '<td>&nbsp;</td>';
        $x .= '<td>&nbsp;</td>';
        $x .= '<td>&nbsp;</td>';
        $x .= '<td>';
        $x .= '<div class="table_row_tool row_content_tool">';
        $x .= '<a href="#" onclick="js_Add_Form_Stage_Row(); return false;" class="add_dim" title="' . $Lang['SysMgr']['FormClassMapping']['Add']['FormStage'] . '"></a>';
        $x .= '</div>';
        $x .= '</td>';
        $x .= '</tr>';
        $x .= '</tbody>';
        $x .= '</table>';
        
        $x .= '<br />';
        
        // Last Updated Info Div
        $LastModifiedInfoArr = $libFormStage->Get_Last_Modified_Info();
        if ($LastModifiedInfoArr != NULL) {
            $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
            $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
            
            $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, '', $lastModifiedBy);
            $x .= '<div class="edit_bottom">';
            $x .= '<span>' . $LastModifiedDisplay . '</span>';
            $x .= '<p class="spacer"></p>';
            $x .= '</div>';
        }
        
        return $x;
    }

    function Get_Student_Without_Class_Selection($ID_Name, $ExcludeStudentIDArr = array(), $AcademicYearID = '', $IncludeYearClassDeselectedStudents = false, $YearClassID = '')
    {
        $x = '';
        
        if ($AcademicYearID == '')
            $AcademicYearID = Get_Current_Academic_Year_ID();
        $fcm = new form_class_manage();
        $StudentArr = $fcm->Get_Student_Without_Class($AcademicYearID, $ExcludeStudentIDArr);
        if ($IncludeYearClassDeselectedStudents && (count($ExcludeStudentIDArr) > 0 || $YearClassID != '')) {
            // first find out all class of the excluded students
            if (count($ExcludeStudentIDArr) > 0) {
                $class_infos = $fcm->Get_Student_Class_Info_In_AcademicYear($ExcludeStudentIDArr, $AcademicYearID);
                $year_class_ids = array_values(array_unique(Get_Array_By_Key($class_infos, 'YearClassID')));
            } else if ($YearClassID != '') {
                $year_class_ids = (array) $YearClassID;
            }
            if (count($year_class_ids) > 0) {
                // then get the students in the classes but exclude the excluded students, the rest students are the just deleted from the classes but not updated DB yet
                $students = $fcm->Get_Student_By_Class($year_class_ids);
                for ($i = 0; $i < count($students); $i ++) {
                    if (! in_array($students[$i]['UserID'], $ExcludeStudentIDArr)) {
                        $StudentArr[] = $students[$i];
                    }
                }
            }
        }
        $numOfStudent = count($StudentArr);
        $DataArr = array();
        for ($i = 0; $i < $numOfStudent; $i ++) {
            $thisUserID = $StudentArr[$i]['UserID'];
            $thisStudentName = $StudentArr[$i]['StudentName'];
            $thisUserLogin = $StudentArr[$i]['UserLogin'];
            
            $DataArr[$thisUserID] = $thisStudentName . ' (' . $thisUserLogin . ')';
        }
        
        $tags = 'name="' . $ID_Name . '" id="' . $ID_Name . '" size="10" style="width:99%" multiple="true"';
        $x .= getSelectByAssoArray($DataArr, $tags, $selected = "", $all = 0, $noFirst = 1, $FirstTitle = "", $ParQuoteValue = 1);
        
        return $x;
    }

    function Get_Edit_Class_SubjectGroup_Table($YearClass, $YearTermID = "")
    {
        global $Lang, $i_general_na, $i_no_record_exists_msg, $sys_custom, $eComm, $i_alert_pleaseselect;
        
        $fcm = new form_class_manage();
        $sbj = new subject();
        $scm = new subject_class_mapping();
        
        $YearClassID = $YearClass->YearClassID;
        $allSemesters = getSemesters($YearClass->AcademicYearID);
        
        $sql = "SELECT YearID FROM YEAR_CLASS WHERE YearClassID='$YearClassID'";
        $temp = $fcm->returnVector($sql);
        $YearID = $temp[0];
        
        $result = $sbj->Get_Subject_By_Form($YearID);
        // debug_pr($result);
        if (sizeof($result) == 0) {
            // $x = $Lang['SysMgr']['FormClassMapping']['FormSubjectSettingNotComplete'];
            $x = showWarningMsg($eComm['Warning'], $Lang['SysMgr']['FormClassMapping']['FormSubjectSettingNotComplete']);
        }
        
        $x .= '<table id="StudentStudySubjectGroupTable" class="common_table_list">
						<thead>
							<tr>
								<th width="6%">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>
								<th>' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</th>';
        
        // $sgClassAry = $scm->getSubjectGroupIDByYearClassID($YearClassID, $YearTermID);
        
        $UserList = $YearClass->ClassStudentList;
        
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($SubjectID, $CH_Name, $EN_Name) = $result[$i];
            
            // $subjectGroups[$SubjectID] = $sbj->Get_Subject_Groups_By_SubjectID($SubjectID, $YearTermID);
            $subjectGroups[$SubjectID] = $sbj->Get_Subject_Group_List_By_YearID($SubjectID, $YearID, $YearTermID);
            
            // $selectMenu = "<select onChange=\"if(this.value!='#') {applyToAll($SubjectID, this.value, '".sizeof($UserList)."');}\">\n";
            $thisClass = "GlobalSubjectGroupIDChk_" . $SubjectID;
            $thisOnchange = "js_Apply_To_All_SuperTable('" . $SubjectID . "')";
            $selectMenu = "<select onchange=\"" . $thisOnchange . "\" class=\"" . $thisClass . "\">\n";
            $selectMenu .= "<option value=\"#\">-- $i_alert_pleaseselect --</option>\n";
            $selectMenu .= "<option>$i_general_na</option>\n";
            
            if (sizeof($subjectGroups[$SubjectID]) > 0) {
                
                foreach ($subjectGroups[$SubjectID] as $_sgid => $_sgname) {
                    $selectMenu .= "<option value='$_sgid'>" . Get_Lang_Selection($_sgname[0], $_sgname[1]) . "</option>\n";
                }
            }
            $selectMenu .= "</select>\n";
            
            $x .= '<th>' . Get_Lang_Selection($CH_Name, $EN_Name) . '<br>' . $selectMenu . '</th>';
            
            // $subjectGroups[$SubjectID] = $sbj->Get_Subject_Groups_By_SubjectID($SubjectID, $YearTermID);
            /*
             * $j = 0;
             * foreach($subjectGroups[$SubjectID] as $sgid=>$nameAry) {
             * $sgAry[$SubjectID][$j][] = $sgid;
             * $sgAry[$SubjectID][$j][] = Get_Lang_Selection($nameAry[0], $nameAry[1]);
             * $j++;
             * }
             */
        }
        // debug_pr($sgAry);
        $x .= '					</tr>
						</thead>';
        
        // debug_pr($UserList);
        // $SubjectTaken = $scm->Get_Number_Of_Subject_Taken($YearClass->YearClassID,$YearClass->AcademicYearID,true);
        
        if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']) {
            $addition = " multiple";
        }
        
        $x .= '<tbody>' . "\n";
        for ($i = 0; $i < sizeof($UserList); $i ++) {
            
            $studentSubjectGroup = $scm->getSubjectGroupIDByStudentID($UserList[$i]['UserID'], $YearTermID);
            
            // debug_pr($studentSubjectGroup);
            $UserName = $UserList[$i]['StudentName'];
            $userID = $UserList[$i]['UserID'];
            $x .= '<tr>
						<td valign="top">' . $UserList[$i]['ClassNumber'] . '&nbsp;</td>
						<td valign="top" width="127">' . $UserName . '</td>';
            
            for ($j = 0; $j < sizeof($result); $j ++) {
                list ($SubjectID, $CH_Name, $EN_Name) = $result[$j];
                $sgAryTemp = $sbj->Get_Subject_Groups_By_SubjectStudent($SubjectID, $YearClassID, $UserList[$i]['UserID'], $YearTermID);
                // debug_pr($sgAryTemp);
                
                if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent']) {
                    $selectedValue = array();
                    $selectedValue = Get_Array_By_Key($sgAryTemp, '0'); // '0' means SubjectGroupID
                } else {
                    $selectedValue = $studentSubjectGroup[$SubjectID];
                }
                
                $thisClass = 'SubjectGroupIDChk_' . $SubjectID;
                $selection = getSelectByArray($sgAryTemp, ' class="' . $thisClass . '" name="SubjectGroup_' . $SubjectID . '_' . $userID . '[]" id="SubjectGroup_' . $SubjectID . '_' . $i . '"' . $addition, $selectedValue, 0, 0, $i_general_na);
                
                $x .= '<td valign="top">' . $selection . '</td>';
            }
            
            $x .= '</tr>';
        }
        // no student in class
        if (sizeof($UserList) == 0) {
            $colspan = sizeof($result) + 2;
            $x .= '<tr><td colspan="' . $colspan . '" align="center" valign="middle" height="40">' . $i_no_record_exists_msg . '<td></tr>';
        } else
            $this->WithStudentInClass = 1;
        
        if (sizeof($result) != 0)
            $this->WithFormSubjectSetting = 1;
        
        $x .= '</tbody>' . "\n";
        $x .= '</table>';
        return $x;
    }

    function Get_Specific_Form_Selection($ID_Name, $YearIDArr = '', $Onchange = '', $noFirst = 0, $isAll = 0, $isMultiple = 0)
    {
        global $Lang;
        $libYear = new Year();
        $FormArr = $libYear->Get_All_Year_List();
        
        // ## Form filtering ###
        foreach ((array) $FormArr as $FormArr_key => $_FormArr) {
            if (! in_array($_FormArr['YearID'], (array) $YearIDArr)) {
                unset($FormArr[$FormArr_key]);
            }
        }
        
        $selectArr = array();
        
        if ($isAll) {
            $selectArr['-1'] = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['All']['Form']);
        }
        foreach ($FormArr as $_FormArr) {
            $thisYearID = $_FormArr['YearID'];
            $thisYearName = $_FormArr['YearName'];
            $selectArr[$thisYearID] = $thisYearName;
        }
        
        $onchange = '';
        if ($Onchange != "") {
            $onchange = 'onchange="' . $Onchange . '"';
        }
        if ($isMultiple == 1) {
            $multiple = ' multiple size="10" ';
        }
        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $multiple;
        $firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Form']);
        
        $formSelection = getSelectByAssoArray($selectArr, $selectionTags, '', $all = 0, 1);
        
        return $formSelection;
    }

    function Get_Specific_Class_Selection($AcademicYearID, $YearID = '', $ID_Name = '', $ClassIDArr = '', $SelectedYearClassID = '', $Onchange = '', $noFirst = 0, $isMultiple = 0, $isAll = 0, $TeachingOnly = 0, $firstTitle = '')
    {
        global $Lang;
        $fcm = new form_class_manage();
        $IsMultiForm = count($YearID) > 1;
        $classArr = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $YearID, $TeachingOnly);
        $numOfClass = count($classArr);
        $selectArr = array();
        for ($i = 0; $i < $numOfClass; $i ++) {
            $thisYearID = $classArr[$i]['YearID'];
            
            // Display classes of the specific Form only (Do in Get_Class_List_By_Academic_Year() now)
            // if ($thisYearID != $YearID)
            // continue;
            
            $thisYearClassID = $classArr[$i]['YearClassID'];
            $thisClassTitleEN = $classArr[$i]['ClassTitleEN'];
            $thisClassTitleB5 = $classArr[$i]['ClassTitleB5'];
            $thisClassTitleDisplay = Get_Lang_Selection($thisClassTitleB5, $thisClassTitleEN);
            if (in_array($thisYearClassID, (array) $ClassIDArr)) {
                if ($IsMultiForm) {
                    $selectArr[$classArr[$i]['YearName']][$thisYearClassID] = $thisClassTitleDisplay;
                } else {
                    $selectArr[$thisYearClassID] = $thisClassTitleDisplay;
                }
            }
        }
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';
        
        $style = '';
        if ($isMultiple)
            $style = ' multiple="true" size="10"';
        
        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . $style;
        // if($firstTitle==''){
        // $firstTitle = ($isAll)? $Lang['SysMgr']['FormClassMapping']['All']['Class'] : $Lang['SysMgr']['FormClassMapping']['Select']['Class'];
        // $firstTitle = Get_Selection_First_Title($firstTitle);
        // }
        // $classSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedBuildingID, $isAll, $noFirst, $firstTitle);
        $classSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearClassID, 0, 1);
        
        return $classSelection;
    }
}
?>