<?php
# Setting for Big Newspaper
$cfg_ePost['BigNewspaper']['courseware']['courseware'] 	   = "1";
$cfg_ePost['BigNewspaper']['courseware']['non_courseware'] = "2";

# Folder Config
$cfg_ePost['BigNewspaper']['Folder_filter']['All'] = 0;
$cfg_ePost['BigNewspaper']['Folder_filter']['FolderWithNewspaper'] = 1;
$cfg_ePost['BigNewspaper']['Folder_filter']['EmptyFolder'] = 2;

$cfg_ePost['BigNewspaper']['Folder_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['Folder_status']['deleted'] = 1;

$cfg_ePost['BigNewspaper']['DefaultFolderTitle'] = "Public";

# Newspaper Config
$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['Newspaper_status']['deleted'] = 1;

$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']   	  = 0;
$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['WaitApproval'] = 1;
$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published'] 	  = 2;

$cfg_ePost['BigNewspaper']['Newspaper_OpenToPublic']['No']  = 0;
$cfg_ePost['BigNewspaper']['Newspaper_OpenToPublic']['Yes'] = 1;

$cfg_ePost['BigNewspaper']['Newspaper_filter']['All'] 		   = 0;
$cfg_ePost['BigNewspaper']['Newspaper_filter']['Draft'] 	   = 1;
$cfg_ePost['BigNewspaper']['Newspaper_filter']['WaitApproval'] = 2;
$cfg_ePost['BigNewspaper']['Newspaper_filter']['Published']    = 3;

$cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']   = 'SchoolLogo';
$cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']  = 'CoverBanner';
$cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner'] = 'InsideBanner';

# Newspaper Page Config
$cfg_ePost['BigNewspaper']['Page_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['Page_status']['deleted'] = 1;

# Newspaper Page Article Config
$cfg_ePost['BigNewspaper']['Article_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['Article_status']['deleted'] = 1;

$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['No']  = 0;
$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes'] = 1;

$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['ArticleShelf_status']['deleted'] = 1;

$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['No']  = 0;
$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['Yes'] = 1;

$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['deleted'] = 1;

$cfg_ePost['sql_config']['NonCoursewareRequest_TargetLevel_delimiter'] = '|=|';

# Student Editor Config
$cfg_ePost['BigNewspaper']['StudentEditor_filter']['All'] = 0;
$cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithRequestManaging'] = 1;
$cfg_ePost['BigNewspaper']['StudentEditor_filter']['StudentwithIssueManaging'] = 2;

$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['StudentEditor_status']['deleted'] = 1;

$cfg_ePost['BigNewspaper']['PublushManager_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['PublushManager_status']['deleted'] = 1;

# Student Editor Managing Config]
$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']   = 'REQ';
$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper'] = 'ISS';

$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']   = 0;
$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['deleted'] = 1;

$cfg_ePost['BigNewspaper']['PageType']['Cover']  = "C";
$cfg_ePost['BigNewspaper']['PageType']['Inside'] = "I";
$cfg_ePost['BigNewspaper']['PageType']['Left']   = "L";
$cfg_ePost['BigNewspaper']['PageType']['Right']  = "R";
$cfg_ePost['BigNewspaper']['PageType']['Back'] 	 = "B";

$cfg_ePost['BigNewspaper']['Alignment']['Hide']  = 0;
$cfg_ePost['BigNewspaper']['Alignment']['Left']  = 1;
$cfg_ePost['BigNewspaper']['Alignment']['Right'] = 2;

$cfg_ePost['comics_m']['max_width']  = 221;
$cfg_ePost['comics_m']['max_height'] = 166;

$cfg_ePost['comics_s']['max_width']  = 133;
$cfg_ePost['comics_s']['max_height'] = 100;

$cfg_ePost['flv_player']['width_m']  = 250; // 320
$cfg_ePost['flv_player']['height_m'] = 199; // 255

$cfg_ePost['flv_player']['width_s']  = 200;
$cfg_ePost['flv_player']['height_s'] = 150;

# Newspaper Skin (Only apply for ePost)
$cfg_ePost['BigNewspaper']['skins']['skin_001']['thumbnail'] 			 = 'skin_01.gif';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['isDefault'] 			 = true;
$cfg_ePost['BigNewspaper']['skins']['skin_001']['page_class_suffix'] 	 = '';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['page_big_class_suffix'] = '';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['body_class'] 			 = 'bg3_green_body';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['top_left_class'] 		 = 'bg3_green_top_left';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['top_right_class'] 		 = 'bg3_green_top_right';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['top_bg_class'] 		 = 'bg3_green_top_bg';
$cfg_ePost['BigNewspaper']['skins']['skin_001']['bg_class'] 			 = 'bg3_green';

$cfg_ePost['BigNewspaper']['skins']['skin_002']['thumbnail']			 = 'skin_02.gif';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['isDefault'] 			 = false;
$cfg_ePost['BigNewspaper']['skins']['skin_002']['page_class_suffix'] 	 = '00';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['page_big_class_suffix'] = '00';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['body_class'] 			 = 'bg4_wood';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['top_left_class'] 		 = '';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['top_right_class'] 		 = '';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['top_bg_class'] 		 = '';
$cfg_ePost['BigNewspaper']['skins']['skin_002']['bg_class'] 			 = '';

# Article Type
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['id']   = 1;
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['name'] = $Lang['ePost']['Filter']['Courseware'];

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_1']['name'] = "Posters";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_1']['search_attr'] = array("level"=>"P3", "content"=>"module_1");

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_2']['name'] = "Stories";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_2']['search_attr'] = array("level"=>"P3", "content"=>"module_2");

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_3']['name'] = "Poems";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_3']['search_attr'] = array("level"=>"P3", "content"=>"module_3");

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_4']['name'] = "Comics";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_4']['search_attr'] = array("level"=>"P3", "content"=>"module_4");

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['name'] = "News";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['search_attr'] = array("level"=>"P4");

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_1']['name'] = "Serious News";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_1']['search_attr'] = array("level"=>"P4", "content"=>"module_1");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_2']['name'] = "Entertainment & Features";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_2']['search_attr'] = array("level"=>"P4", "content"=>"module_2");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_3']['name'] = "Sports News";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_3']['search_attr'] = array("level"=>"P4", "content"=>"module_3");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_4']['name'] = "Weather Report";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_5']['sub_types']['sub_type_4']['search_attr'] = array("level"=>"P4", "content"=>"module_4");
 
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['name'] = "Video";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['search_attr'] = array("level"=>"P5");

$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_1']['name'] = "Weather Report";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_1']['search_attr'] = array("level"=>"P5", "content"=>"module_1");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_2']['name'] = "Event Report";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_2']['search_attr'] = array("level"=>"P5", "content"=>"module_2");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_3']['name'] = "Crime & Hearing Report";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_3']['search_attr'] = array("level"=>"P5", "content"=>"module_3");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_4']['name'] = "Accident Report";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_4']['search_attr'] = array("level"=>"P5", "content"=>"module_4");
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_5']['name'] = "Sports Event Report";
$cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types']['type_6']['sub_types']['sub_type_5']['search_attr'] = array("level"=>"P5", "content"=>"module_5");

$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['id']	= 2;
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['name'] = $Lang['ePost']['Filter']['NonCourseware'];

$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_1']['name'] = $Lang['ePost']['TextImage'];
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_1']['search_attr'] = array("level"=>"NC", "content"=>"module_1");

$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_2']['name'] = $Lang['ePost']['TextVideo'];
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_2']['search_attr'] = array("level"=>"NC", "content"=>"module_2");

/*
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_3']['name'] = "Poems";
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_3']['search_attr'] = array("level"=>"NC", "content"=>"module_3");

$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_4']['name'] = "Comics";
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_4']['search_attr'] = array("level"=>"NC", "content"=>"module_4");

$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_5']['name'] = "News";
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_5']['search_attr'] = array("level"=>"NC", "content"=>"module_5");

$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_6']['name'] = "Video";
$cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types']['type_6']['search_attr'] = array("level"=>"NC", "content"=>"module_6");
*/

# Page Type and Layout
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['img'] = "cover_01.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['class']       	= "log_full height_medium";
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['flv_needspacer'] 	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['class']       	= "height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['flv_needspacer'] 	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C2'][1]['articles'][2]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['img'] = "cover_02.gif"; 
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['class']       	= "log_full height_medium";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['flv_needspacer']  = false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][2]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][1]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['img'] = "cover_03.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['class']       	= "log_half_left height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][1]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][2]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['C3'][2]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['img'] = "inside_01.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['class']       	= "log_full height_short no_margin_top";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['flv_needspacer']  = false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['class']       	= "log_full height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['flv_needspacer'] 	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][2]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['class']       	= "log_full height_short no_margin_bottom";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['flv_needspacer']  = false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][1]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['img'] = "inside_03.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['class']       	= "log_full height_medium";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['flv_needspacer'] 	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][2]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I3'][2]['articles'][3]['restriction'] 	= array();

$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['img'] = "inside_02.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['class']       	= "log_full height_medium";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['flv_needspacer']  = false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['class']       	= "log_half_left height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][2]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][1]['articles'][4]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['img'] = "inside_04.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['class']       	= "log_half_left height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][1]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['needspacer'] 	 	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][2]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['class']       	= "log_half_right height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][2]['articles'][4]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['img'] = "inside_07.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['needspacer'] 		= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['class']       	= "log_half_right height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][2]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['needspacer']  	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['class']       	= "height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['flv_needspacer']  = false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['image_size']		= array("Large"=>array("W"=>320, "H"=>240), "Medium"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['video_size']		= array("Large"=>array("W"=>320, "H"=>255), "Medium"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['image_size_s']	= array("Large"=>array("W"=>174, "H"=>119), "Medium"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['video_size_s']	= array("Large"=>array("W"=>174, "H"=>130), "Medium"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I4'][3]['articles'][4]['restriction'] 	= array("type_4");

$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['img'] = "inside_05.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['class']       	= "log_half_left height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][1]['restriction']	 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][2]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['needspacer']  	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][4]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][1]['articles'][5]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['img'] = "inside_06.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['class']       	= "log_half_right height_long";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][2]['restriction'] 	= array();
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['needspacer']  	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][4]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I5'][2]['articles'][5]['restriction'] 	= array("type_4"); // comics not allowed

$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['img'] = "inside_08.gif";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][1]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][2]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][3]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['flv_needspacer']  = true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][4]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['class']       	= "log_half_left height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][5]['restriction'] 	= array("type_4"); // comics not allowed
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['class']       	= "log_half_right height_short";
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['needspacer']  	= false;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['flv_needspacer'] 	= true;
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['image_size']		= array("Large"=>array("W"=>260, "H"=>190), "Small"=>array("W"=>155, "H"=>113));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['video_size']		= array("Large"=>array("W"=>250, "H"=>199), "Small"=>array("W"=>200, "H"=>150));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['image_size_s']	= array("Large"=>array("W"=>142, "H"=>97),  "Small"=>array("W"=>70,  "H"=>48));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['video_size_s']	= array("Large"=>array("W"=>142, "H"=>106), "Small"=>array("W"=>70,  "H"=>52));
$cfg_ePost['BigNewspaper']['TypeLayout']['I6'][1]['articles'][6]['restriction'] 	= array("type_4"); // comics not allowed

# Page Theme
$cfg_ePost['BigNewspaper']['Theme'][1]['class']   	= "page_cat_01";
$cfg_ePost['BigNewspaper']['Theme'][1]['big_img']   = "icon_page_cat_big_01.png";
$cfg_ePost['BigNewspaper']['Theme'][1]['small_img'] = "icon_page_cat_small_01.png";

$cfg_ePost['BigNewspaper']['Theme'][2]['class']   	= "page_cat_02";
$cfg_ePost['BigNewspaper']['Theme'][2]['big_img']   = "icon_page_cat_big_02.png";
$cfg_ePost['BigNewspaper']['Theme'][2]['small_img'] = "icon_page_cat_small_02.png";

$cfg_ePost['BigNewspaper']['Theme'][3]['class']   	= "page_cat_03";
$cfg_ePost['BigNewspaper']['Theme'][3]['big_img']   = "icon_page_cat_big_03.png";
$cfg_ePost['BigNewspaper']['Theme'][3]['small_img'] = "icon_page_cat_small_03.png";

$cfg_ePost['BigNewspaper']['Theme'][4]['class']   	= "page_cat_04";
$cfg_ePost['BigNewspaper']['Theme'][4]['big_img']   = "icon_page_cat_big_04.png";
$cfg_ePost['BigNewspaper']['Theme'][4]['small_img'] = "icon_page_cat_small_04.png";

$cfg_ePost['BigNewspaper']['Theme'][5]['class']   	= "page_cat_05";
$cfg_ePost['BigNewspaper']['Theme'][5]['big_img']   = "icon_page_cat_big_05.png";
$cfg_ePost['BigNewspaper']['Theme'][5]['small_img'] = "icon_page_cat_small_05.png";

$cfg_ePost['BigNewspaper']['Theme'][6]['class']   	= "page_cat_06";
$cfg_ePost['BigNewspaper']['Theme'][6]['big_img']   = "icon_page_cat_big_06.png";
$cfg_ePost['BigNewspaper']['Theme'][6]['small_img'] = "icon_page_cat_small_06.png";

$cfg_ePost['BigNewspaper']['Theme'][7]['class']   	= "page_cat_07";
$cfg_ePost['BigNewspaper']['Theme'][7]['big_img']   = "icon_page_cat_big_07.png";
$cfg_ePost['BigNewspaper']['Theme'][7]['small_img'] = "icon_page_cat_small_07.png";

$cfg_ePost['BigNewspaper']['Theme'][8]['class']	= "page_cat_08";
$cfg_ePost['BigNewspaper']['Theme'][8]['big_img'] = "icon_page_cat_big_08.png";	

$cfg_ePost['BigNewspaper']['Theme'][9]['class']	= "page_cat_09";
$cfg_ePost['BigNewspaper']['Theme'][9]['big_img'] = "icon_page_cat_big_09.png";	

$cfg_ePost['BigNewspaper']['Theme'][10]['class'] = "page_cat_10";
$cfg_ePost['BigNewspaper']['Theme'][10]['big_img'] = "icon_page_cat_big_10.png";
	
$cfg_ePost['BigNewspaper']['Theme'][11]['class']	= "page_cat_11";
$cfg_ePost['BigNewspaper']['Theme'][11]['big_img'] = "icon_page_cat_big_11.png";
	
$cfg_ePost['BigNewspaper']['Theme'][12]['class']	= "page_cat_12";
$cfg_ePost['BigNewspaper']['Theme'][12]['big_img'] = "icon_page_cat_big_12.png";	

$cfg_ePost['BigNewspaper']['Theme'][13]['class']	= "page_cat_13";
$cfg_ePost['BigNewspaper']['Theme'][13]['big_img'] = "icon_page_cat_big_13.png";	

$cfg_ePost['BigNewspaper']['Theme'][14]['class'] = "page_cat_j01";
$cfg_ePost['BigNewspaper']['Theme'][14]['big_img'] = "icon_page_cat_big_j01.png"; 

$cfg_ePost['BigNewspaper']['Theme'][15]['class'] = "page_cat_j02";
$cfg_ePost['BigNewspaper']['Theme'][15]['big_img'] = "icon_page_cat_big_j02.png"; 

$cfg_ePost['BigNewspaper']['Theme'][16]['class'] = "page_cat_j03";
$cfg_ePost['BigNewspaper']['Theme'][16]['big_img'] = "icon_page_cat_big_j03.png"; 

$cfg_ePost['BigNewspaper']['Theme'][17]['class'] = "page_cat_j04";
$cfg_ePost['BigNewspaper']['Theme'][17]['big_img'] = "icon_page_cat_big_j04.png";
 
$cfg_ePost['BigNewspaper']['Theme'][18]['class'] = "page_cat_j05";
$cfg_ePost['BigNewspaper']['Theme'][18]['big_img'] = "icon_page_cat_big_j05.png"; 

$cfg_ePost['BigNewspaper']['Theme'][19]['class'] = "page_cat_j06";
$cfg_ePost['BigNewspaper']['Theme'][19]['big_img'] = "icon_page_cat_big_j06.png"; 

$cfg_ePost['BigNewspaper']['Theme'][20]['class'] = "page_cat_j07";
$cfg_ePost['BigNewspaper']['Theme'][20]['big_img'] = "icon_page_cat_big_j07.png"; 


# Article Theme
/* Type 01 */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['name'] = "Type 01";

/* Type 01 - Basketball */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['basketball']['name'] = "Basketball";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['basketball']['Color']['orange']['name'] = "Orange";

/* Type 01 - Building*/
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['building']['name'] = "Building";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['building']['Color']['blue']['name'] = "Blue";

/* Type 01 - Cloud */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['cloub']['name'] = "Cloud";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['cloub']['Color']['pink']['name'] = "Pink";

/* Type 01 - Phone*/
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['phone']['name'] = "Phone";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['phone']['Color']['green']['name'] = "Green";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['phone']['Color']['blue']['name']  = "Blue";

/* Type 01 - Telephone */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['telephone']['name'] = "Telephone";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['telephone']['Color']['blue']['name'] = "Blue";

/* Type 01 - Tower */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['tower']['name'] = "Tower";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['tower']['Color']['blue']['name']   = "Blue";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['tower']['Color']['yellow']['name'] = "Yellow";

/* Type 01 - Magnifier */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['magnifier']['name'] = "Magnifier";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['01']['Themes']['magnifier']['Color']['purple']['name'] = "Purple";

/* Type 02 */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['name'] = "Type 02";

/* Type 02 - Flag */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['flag']['name'] = "Flag";

/* Type 02 - Color */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['color']['name'] = "Color"; 
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['color']['Color']['blue']['name']   = "Blue";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['color']['Color']['yellow']['name'] = "Yellow";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['color']['Color']['pink']['name']   = "Pink";

/* Type 02 - Pin */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['pin']['name'] = "Pin";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['pin']['Color']['red']['name'] 	  = "Red"; 
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['pin']['Color']['orange']['name'] = "Orange";

/* Type 02 - Red Phone */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['redphone']['name'] = "Red Phone";

/* Type 02 - Green Phone */
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['02']['Themes']['greenphone']['name'] = "Green Phone";

$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['name'] = "Type 03";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['football']['name'] = "Football";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['pencil']['name'] 	= "Pencil";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['comic']['name'] 	= "Comic";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['kite']['name'] 	= "Kite";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['writing']['name'] 	= "Writing";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['travel']['name'] 	= "Travel";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['03']['Themes']['travel2']['name'] 	= "Travel 2";

$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['name'] = "Type 04";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['tram']['name'] 	 = "Tram (Red)";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['tram2']['name'] 	 = "Tram (Purple)";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['taxi']['name'] 	 = "Taxi";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['baseball']['name']  = "Baseball";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['color']['name'] 	 = "Color";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['colorpen']['name']  = "Color Pen";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['milk']['name'] 	 = "Milk";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['ilovehk']['name'] 	 = "I love HK (Green)";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['ilovehk2']['name']  = "I love HK (Yellow)";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['hongkong']['name']  = "Hong Kong";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['spaceship']['name'] = "Spaceship";
$cfg_ePost['BigNewspaper']['ArticleTheme']['Types']['04']['Themes']['ufo']['name'] 		 = "UFO";
?>