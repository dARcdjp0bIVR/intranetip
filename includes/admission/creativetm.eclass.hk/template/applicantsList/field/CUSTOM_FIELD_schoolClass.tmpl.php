<?php
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));

list($start, $end) = explode(' - ', $value);
?>

<span class="itemInput" name="field_<?=$field['FieldID'] ?>" data-field="<?=$extraArr['field']?>">
    <div class="itemInput-complex" style="margin-bottom:0;">
    	<div style="display: inline-block;">
    		<?=$kis_lang['Admission']['from'] ?>
    		<input
        		type="text"
        		id="field_<?=$field['FieldID'] ?>_start"
        		name="field_<?=$field['FieldID'] ?>_start"
        		value="<?=$start ?>"
    		>
    	</div>
    	<div style="display: inline-block;">
    		<?=$kis_lang['Admission']['to'] ?>
    		<input
        		type="text"
        		id="field_<?=$field['FieldID'] ?>_end"
        		name="field_<?=$field['FieldID'] ?>_end"
        		value="<?=$end ?>"
    		>
    	</div>
    </div>

    <div style="">
    	<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
    	<div class="remark remark-warn number hide" style="clear:both;">必須是數字 Must be number</div>
    	<div class="remark remark-warn integer hide" style="clear:both;">必須是整數 Must be integer</div>
    	<div class="remark remark-warn postive_number hide" style="clear:both;">必須是正整數 Must be postive integer</div>
    	<div class="remark remark-warn number_in_range hide" style="clear:both;">範圍必須由 {min} - {max} Must between {min} - {max}</div>
    	<div class="remark remark-warn chinese hide" style="clear:both;">必須是中文字 Must be Chinese character</div>
    	<div class="remark remark-warn english hide" style="clear:both;">必須是英文字 Must be English character</div>
    	<div class="remark remark-warn date hide" style="clear:both;">日期錯誤 Invalid date</div>
    	<div class="remark remark-warn hkid email hide" style="clear:both;">格式錯誤 Invalid format</div>
    	<div class="remark remark-warn equal hide" style="clear:both;">資料不相符 Data must be same</div>
    	<div class="remark remark-warn length hide" style="clear:both;">長度必需為 {length} Length must be {length}</div>
    	<div class="remark remark-warn custom hide" style="clear:both;"></div>
    </div>
</span>