<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(屯門)';
$admission_cfg['SchoolName']['en'] .= ' (Tuen Mun)';
$admission_cfg['SchoolCode'] = 'TM';
$admission_cfg['SchoolPhone'] = '2469 2288';
$admission_cfg['SchoolAddress']['b5'] = '新界屯門大興花園第二期河興街5A';
$admission_cfg['SchoolAddress']['en'] = '5A Ho Hing Circuit, Phase II, Tai Hing Gardens, Tuen Mun, New Territories';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'cktm.photo@gmail.com';
}
/* for email [end] */


/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = 'WMSEVZD7R4CXQ';
} else {
    $admission_cfg['paypal_signature'] = 'IFLgvhI0PT8PXCFjuc76D7_UNQhJQOGC06JMpo-xk4vukhbzr2Cz7jB4eAy';
    $admission_cfg['hosted_button_id'] = 'NU3BE8TN3GWU6';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Tuen Mun)';
}
/* for paypal [End] */