<?php
// Editing by
/*
 * Change Log:
 * 2015-10-07 (Henry)	added function getStaticIndexContent()
 *
 * Common functions, i.e. non-customized functions can implement in this base class
 * customized functions implement in inherited class, functions can be overwritten there
 */

class admission_ui_cust_base extends interface_html{

	public function __construct(){

	}

	function admission_ui_cust_base(){


	}

	public $langOrder = array('b5','en','gb');
	public function getLangStr($langArr, $seperater=' '){
		$strArr = array();
		foreach($this->langOrder as $order){
			if(isset($langArr[$order])) {
				$strArr[] = $langArr[ $order ];
			}
		}

		return implode($seperater, $strArr);
	}

	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac;

		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1>'.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}

		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$star.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
				  </tr>';
		}

//		$x .='<tr>
//					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
//					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
//			</tr>';
//		$x .='<tr>
//					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
//					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
//			</tr>';

		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');

		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
				
			</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}

	function getStaticIndexContent($Instruction, $FilePath){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			//$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage']; //Henry 20131107
		}

//		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token']){
//			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'</center></h2>';
//		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<!--<h1 class="notice_title">'.$Lang['Admission']['applicationtitle'].'</h1>-->
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';

//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}

					$x .='<p class="spacer"></p>
                    	</div>';
//                  $x .='<div class="edit_bottom">
//						'.$this->GET_ACTION_BTN($Lang['Admission']['downloadApplicationForm'], "button", "window.open('".$FilePath."');", "SubmitBtn", "", 0, "formbutton")
//						.'</div>';
					$x .='<div class="edit_bottom">
						<h3>下載申請表已完結</h3>
						</div>';
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';

    	return $x;
	}

	function getPDFContentForEmail($recordID, $applicationIDAry, $subject = '', $message = '', $templateId = ''){
		global $PATH_WRT_ROOT,$lac,$Lang,$admission_cfg, $setting_path_ip_rel, $libkis_admission;

		######## Init PDF START ########
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		$margin_top = '7';
		$mpdf = new mPDF('','A4',0,'',5,5,10,5);
		$mpdf->mirrorMargins = 1;
		######## Init PDF END ########

		######## Load header to PDF START ########
		$pageHeader = '
				<style type="text/css">
					body{color: #000;  font-family:  msjh !important; font-size:0.9em; line-height:0.9em; margin:0; padding:0; -webkit-print-color-adjust: exact; font-stretch: condensed; /*font-size-adjust: 0.5;*/}
					@media all {
						.page-break {display: none;}
					}
					@media print {
						.page-break {display: block; page-break-after: always;}
					}
				</style>';

		$mpdf->WriteHTML($pageHeader);
		######## Load header to PDF END ########

		$receiverAry = $libkis_admission->getEmailReceivers((array)$applicationIDAry,$recordID);
		$emailDetail = $libkis_admission->getEmailRecordDetail($recordID);
		if($templateId > 0){
			$emailTemplateDetail = $libkis_admission->getEmailTemplateRecords('',$templateId);
			$emailDetail['Subject'] = $emailTemplateDetail[0]['Title'];
			$emailDetail['Message'] = $emailTemplateDetail[0]['Content'];
		}
		$subject = $subject?$subject:$emailDetail['Subject'];
		$message = $message?$message:$emailDetail['Message'];

		######## Load data to PDF START ########
		$count = 0;
		foreach((array)$receiverAry as $receiver){
			$replaced_text = '<h3>'.$subject.'</h3>';
			$replaced_text .= $libkis_admission->replaceEmailVariables($message, $receiver['ChineseName'], $receiver['EnglishName'], $receiver['ApplicationNo'], $receiver['Status'], $receiver['InterviewDate'], $receiver['InterviewLocation'],$receiver['InterviewSettingID'], $receiver['InterviewSettingID2'], $receiver['InterviewSettingID3'], $receiver['BriefingDate'], $receiver['BriefingInfo'], $receiver['LangSpokenAtHome'], $libkis_admission->getPrintLink($recordID, $receiver['UserID'], $templateId));
			if($count + 1 < count($receiverAry)){
				$replaced_text .= '<div class="page-break"></div>';
			}
			//debug_pr($replaced_text);die();
			$mpdf->WriteHTML($replaced_text);
			$count++;
		}
		######## Load data to PDF END ########

// 				echo $pageHeader;
// 				echo $page1;
		$mpdf->Output();
	}
}

?>
