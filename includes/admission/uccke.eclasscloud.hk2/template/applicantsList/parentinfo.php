<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

#### Religion START ####
$regligion = $applicationCustInfo['Parent_Religion'][0]['Value'];
#### Religion END ####

#### Fax START ####
/*$fax = $applicationCustInfo['Parent_Fax'][0]['Value'];
#### Fax END ####

#### Lang Spoken START ####
$langStr = '';
$Lang_Spoken = (array)$applicationCustInfo['Parent_Lang_Spoken'];
foreach($Lang_Spoken as $lang){
	if($lang['Value'] == 'Cantonese'){
		$langStr .= $kis_lang['Admission']['Languages']['Cantonese'];
	}else if($lang['Value'] == 'Putonghua'){
		$langStr .= $kis_lang['Admission']['Languages']['Putonghua'];
	}else if($lang['Value'] == 'English'){
		$langStr .= $kis_lang['Admission']['Languages']['English'];
	}else if($lang['Value'] == 'Others'){
		$langStr .= $applicationCustInfo['Parent_Lang_Spoken_Other'][0]['Value'];
	}
	$langStr .= '<br />';
}
// $langStr = substr($langStr, 0, strlen($langStr)-6); // Trim br
$langSpoken = $langStr;
#### Lang Spoken END ####

#### Level of education START ####
if($applicationInfo['G']['levelofeducation'] == 'Tertiary'){
	$levelofeducation = $kis_lang['Admission']['UCCKE']['EducationType']['Tertiary'];
}else if($applicationInfo['G']['levelofeducation'] == 'Secondary'){
	$levelofeducation = $kis_lang['Admission']['UCCKE']['EducationType']['Secondary'];
}else{
	$levelofeducation = $applicationInfo['G']['levelofeducation'];
}*/
#### Level of education END ####
######## Get Parent Cust Info END ########
?>
<table class="form_table">
	<colgroup>
		<col width="30%">
	</colgroup>
	<tbody>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['NameOfParentGuardian'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['EnglishName']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['relationship'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['Relationship']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['religion'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($regligion) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['email'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['Email']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['EngAddress'] ?> (<?=$kis_lang['Admission']['UCCKE']['DifferentAddress'] ?>)
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['OfficeAddress']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['phome'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['OfficeTelNo']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['mobile'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['Mobile']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['occupation'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($applicationInfo['G']['JobTitle']) ?>
			</td>
		</tr>
		
	</tbody>
</table>