<?php

global $libkis_admission;


#### Get Student Cust Info START ####
$applicationInfo['studentCustInfo'] = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


#### Get Relatives Info START ####
$applicationInfo['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
#### Get Relatives Info END ####


?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<input type="hidden" name="RelativesID" value="<?=$applicationInfo['studentApplicationRelativesInfo'][0]['RecordID']?>" />
<table class="form_table">
	<colgroup>
		<col width="30%">
		<col width="20%">
		<col width="30%">
		<col width="20%">
	</colgroup>
	<tbody>     
<!-------------- Siblings START -------------->
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['RelativeAttending']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('RelativeStudent', 'RelativeStudent', $applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeStudiedName'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['Relationship']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('RelativeRelationship', 'RelativeRelationship', $applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeRelationship'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['ClassAttending']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('RelativeClassAttend', 'RelativeClassAttend', $applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeClassPosition'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
<!-------------- Siblings END -------------->


<!-------------- Referee START -------------->
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['KTLMSKG']['introducedName']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('Referee_Name', 'Referee_Name', $applicationInfo['studentCustInfo']['Referee_Name'][0]['Value'], $OtherClass='', $OtherPar=array())?>
			</td>
		</tr>
	</tbody>
</table>
<!-------------- Referee END -------------->


<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){

	return true;
}
</script>