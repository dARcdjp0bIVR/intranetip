<section id="otherForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['otherInfo'] ?> <?=$LangEn['Admission']['otherInfo'] ?>
	</div>
	<div class="sheet">
		
		<div class="item" id="divOtherFormClassLevel">
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['applyLevel'] ?> <?=$LangEn['Admission']['applyLevel'] ?></div>
				<div class="dataValue"><?=$classLevel ?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="RelativeStudent" id="RelativeStudent" value="<?=$relativeName ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['RelativeAttending'] ?> <?=$LangEn['Admission']['UCCKE']['RelativeAttending'] ?></div>
					<div class="remark remark-warn hide" id="errRelativeStudent"></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['RelativeAttending'] ?> <?=$LangEn['Admission']['UCCKE']['RelativeAttending'] ?></div>
				<div class="dataValue <?=$relativeName ? '' : 'dataValue-empty'?>"><?=$relativeName ? $relativeName : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="RelativeRelationship" id="RelativeRelationship" value="<?=$relativeRelationship ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['Relationship'] ?> <?=$LangEn['Admission']['UCCKE']['Relationship'] ?></div>
					<div class="remark remark-warn hide" id="errRelativeRelationship"></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['Relationship'] ?> <?=$LangEn['Admission']['UCCKE']['Relationship'] ?></div>
				<div class="dataValue <?=$relativeRelationship ? '' : 'dataValue-empty'?>"><?=$relativeRelationship ? $relativeRelationship : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="RelativeClassAttend" id="RelativeClassAttend" value="<?=$relativeClass ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['ClassAttending'] ?> <?=$LangEn['Admission']['UCCKE']['ClassAttending'] ?></div>
					<div class="remark remark-warn hide" id="errRelativeClassAttend"></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['ClassAttending'] ?> <?=$LangEn['Admission']['UCCKE']['ClassAttending'] ?></div>
				<div class="dataValue <?=$relativeClass ? '' : 'dataValue-empty'?>"><?=$relativeClass ? $relativeClass : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="Referee_Name" id="Referee_Name" value="<?=$referee ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['Recommended'] ?> <?=$LangEn['Admission']['UCCKE']['Recommended'] ?></div>
					<div class="remark remark-warn hide"></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['Recommended'] ?> <?=$LangEn['Admission']['UCCKE']['Recommended'] ?></div>
				<div class="dataValue <?=$referee ? '' : 'dataValue-empty'?>"><?=$referee ? $referee : '－－' ?></div>
			</span>
		</div>
	</div>
</section>	
<?if($lac->isInternalUse($_GET['token']) && !$IsUpdate){?>
<section id="feeForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['applicationfee'] ?> <?=$LangEn['Admission']['applicationfee'] ?>
	</div>
	<div class="sheet">

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="BankName" id="BankName" value="<?=$StatusInfo[0]['FeeBankName'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['KTLMSKG']['bankName'] ?> <?=$LangEn['Admission']['KTLMSKG']['bankName'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['KTLMSKG']['bankName'] ?> <?=$LangEn['Admission']['KTLMSKG']['bankName'] ?></div>
				<div class="dataValue <?=$StatusInfo[0]['FeeBankName'] ? '' : 'dataValue-empty'?>"><?=$StatusInfo[0]['FeeBankName'] ? $StatusInfo[0]['FeeBankName'] : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ChequeNo" id="ChequeNo" value="<?=$StatusInfo[0]['FeeChequeNo'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['KTLMSKG']['chequeNum'] ?> <?=$LangEn['Admission']['KTLMSKG']['chequeNum'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['KTLMSKG']['chequeNum'] ?> <?=$LangEn['Admission']['KTLMSKG']['chequeNum'] ?></div>
				<div class="dataValue <?=$StatusInfo[0]['FeeChequeNo'] ? '' : 'dataValue-empty'?>"><?=$StatusInfo[0]['FeeChequeNo'] ? $StatusInfo[0]['FeeBankName'] : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel"><?=$LangB5['Admission']['KTLMSKG']['Payed'] ?> <?=$LangEn['Admission']['KTLMSKG']['Payed'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="Payed_1" name="Payed" value="1" <?=$feestatus?'checked':'' ?>>
					<label for="Payed_1"><?=$LangB5['Admission']['yes'] ?> <?=$LangEn['Admission']['yes'] ?></label>
				</span>
				<span>
					<input type="radio" id="Payed_0" name="Payed" value="0" <?=!$feestatus?'checked':'' ?>>
					<label for="Payed_0"><?=$LangB5['Admission']['no'] ?> <?=$LangEn['Admission']['no'] ?></label>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue"><?=$feestatus ?$LangB5['Admission']['yes'].' '.$LangEn['Admission']['yes']:$LangB5['Admission']['no'].' '.$LangEn['Admission']['no']?></div>
			</div>
		</div>
		
	</div>
</section>
<?}?>

<script>
$(function(){
	'use strict';
	
	$('#otherForm .itemInput, #feeForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html($(this).val());
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html($(this).next('label').html());
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
			
		$inputDateSelect.on('change', function(e){
			$dataValue.html($inputDateYearSelect.val()+'-'+$inputDateMonthSelect.val()+'-'+$inputDateDaySelect.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
		
		
	});
	
	window.checkOtherForm = (function(lifecycle){
		var isValid = true;
		var $otherForm = $('#otherForm');

		/******** Basic init START ********/
		//for mm/yy validation
		var re = /^(0[123456789]|10|11|12)\/\d{2}$/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($otherForm);
		
		if($('#RelativeStudent').val().trim()!=''){
			if($('#RelativeRelationship').val().trim()==''){
				if($('#RelativeRelationship').parent().find('.remark-warn').hasClass('hide')){
					$('#errRelativeRelationship').html('<?=$LangB5['Admission']['msg']['enterrelationship']?> <?=$LangEn['Admission']['msg']['enterrelationship']?>');
					$('#errRelativeRelationship').removeClass('hide');
					focusElement = $('#RelativeRelationship');
				}	
		    	isValid = false;
			}
			if($('#RelativeClassAttend').val().trim()==''){
				if($('#RelativeClassAttend').parent().find('.remark-warn').hasClass('hide')){
					$('#errRelativeClassAttend').html('<?=$LangB5['Admission']['UCCKE']['msg']['enterClassAttend']?> <?=$LangEn['Admission']['UCCKE']['msg']['enterClassAttend']?>');
					$('#errRelativeClassAttend').removeClass('hide');
					focusElement = $('#RelativeClassAttend');
				}	
		    	isValid = false;
			}
		}
		
		if(($('#RelativeRelationship').val().trim()!='' || $('#RelativeClassAttend').val().trim()!='') && $('#RelativeStudent').val().trim()=='' ){
			if($('#RelativeStudent').parent().find('.remark-warn').hasClass('hide')){
				$('#errRelativeStudent').html("<?=$LangB5['Admission']['UCCKE']['msg']['enterRelativeName']?> <?=$LangEn['Admission']['UCCKE']['msg']['enterRelativeName']?>");
				$('#errRelativeStudent').removeClass('hide');
				focusElement = $('#RelativeStudent');
			}	
	    	isValid = false;
		}
		
		/**** Check required END ****/

		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkOtherForm);
});
</script>