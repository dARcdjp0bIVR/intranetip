<h1>
    <?=$Lang['Admission']['PICLC']['FamilyLanguageProfile']?>
</h1>

<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['Student']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['Mother']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['Father']?></td>
	</tr>
	<tr>
		<td><?=$Lang['Admission']['PICLC']['FirstLanguage']?> </td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentFirstLanguage']?>
		<?php }else{ ?>
			<input name="StudentFirstLanguage" type="text" id="StudentFirstLanguage" class="textboxtext" value="<?=$allCustInfo['StudentFirstLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['MotherFirstLanguage']?>
		<?php }else{ ?>
			<input name="MotherFirstLanguage" type="text" id="MotherFirstLanguage" class="textboxtext" value="<?=$allCustInfo['MotherFirstLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['FatherFirstLanguage']?>
		<?php }else{ ?>
			<input name="FatherFirstLanguage" type="text" id="FatherFirstLanguage" class="textboxtext" value="<?=$allCustInfo['FatherFirstLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td><?=$Lang['Admission']['PICLC']['SecondLanguage']?> </td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentSecondLanguage']?>
		<?php }else{ ?>
			<input name="StudentSecondLanguage" type="text" id="StudentSecondLanguage" class="textboxtext" value="<?=$allCustInfo['StudentSecondLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['MotherSecondLanguage']?>
		<?php }else{ ?>
			<input name="MotherSecondLanguage" type="text" id="MotherSecondLanguage" class="textboxtext" value="<?=$allCustInfo['MotherSecondLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['FatherSecondLanguage']?>
		<?php }else{ ?>
			<input name="FatherSecondLanguage" type="text" id="FatherSecondLanguage" class="textboxtext" value="<?=$allCustInfo['FatherSecondLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td><?=$Lang['Admission']['PICLC']['ThirdLanguage']?> </td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentThirdLanguage']?>
		<?php }else{ ?>
			<input name="StudentThirdLanguage" type="text" id="StudentThirdLanguage" class="textboxtext" value="<?=$allCustInfo['StudentThirdLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['MotherThirdLanguage']?>
		<?php }else{ ?>
			<input name="MotherThirdLanguage" type="text" id="MotherThirdLanguage" class="textboxtext" value="<?=$allCustInfo['MotherThirdLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
		<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<?=$formData['FatherThirdLanguage']?>
		<?php }else{ ?>
			<input name="FatherThirdLanguage" type="text" id="FatherThirdLanguage" class="textboxtext" value="<?=$allCustInfo['FatherThirdLanguage'][0]['Value']?>"/>
		<?php } ?>
		</td>
	</tr>
</table>

<!--<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['LanguageSpokenAtHome']?> 
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['LangSpokenAtHome']?>
		<?php }else{ ?>
			<input name="LangSpokenAtHome" type="text" id="LangSpokenAtHome" class="textboxtext" value="<?=$StudentInfo['LangSpokenAtHome']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['StudentFirstLanguageIsEnglish']?> 
	</td>
	<td colspan="3">
		<?php 
		if($IsConfirm){ 
		    if($formData['StudentFirstLanguageIsEnglish'] == 'Y'){
		        echo "{$Lang['Admission']['yes']} {$LangEn['Admission']['yes']}<br />";
		    }else if($formData['StudentFirstLanguageIsEnglish'] == 'N'){
		        echo "{$Lang['Admission']['no']} {$LangEn['Admission']['no']}";
		    }else{
		        echo ' -- ';
		    }
		}else{ 
		    $checkedY = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'Y')? 'checked' : '';
		    $checkedN = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N')? 'checked' : '';
		?>
			<div>
    			<input type="radio" value="Y" id="StudentFirstLanguageIsEnglish_Y" name="StudentFirstLanguageIsEnglish" <?=$checkedY ?>>
    			<label for="StudentFirstLanguageIsEnglish_Y"><?=$Lang['Admission']['yes'] ?></label>
    			&nbsp;
    			&nbsp;
    			<input type="radio" value="N" id="StudentFirstLanguageIsEnglish_N" name="StudentFirstLanguageIsEnglish" <?=$checkedN ?>>
    			<label for="StudentFirstLanguageIsEnglish_N"><?=$Lang['Admission']['no'] ?></label>
			</div>
		<?php 
		} 

		if($IsConfirm){
		    $display = ($formData['StudentFirstLanguageIsEnglish'] == 'N')? 'display: block;' : 'display: none;';
		}else{
		    $display = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N')? 'display: block;' : 'display: none;';
		}
		?>
		<div id="StudentFirstLanguageIsEnglishDiv" style="<?=$display ?>">
			
            <table class="form_table" style="font-size: 13px">
                <colgroup>
                    <col style="width:200px;">
                    <col style="width:200px;">
                    <col style="width:200px;">
                    <col style="width:200px;">
                    <col style="width:200px;">
                    <col style="width:200px;">
                </colgroup>
            	<tr>
            		<td class="form_guardian_head" rowspan="2"><center><?=$Lang['Admission']['PICLC']['SkillAreas']?></center></td>
            		<td class="form_guardian_head" colspan="5"><center><?=$Lang['Admission']['PICLC']['EnglishAssessmentOfProficiency']?></center></td>
            	</tr>
            	<tr>
            		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$Lang['Admission']['PICLC']['Advanced']?></center></td>
            		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$Lang['Admission']['PICLC']['UpperIntermediate']?></center></td>
            		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$Lang['Admission']['PICLC']['Intermediate']?></center></td>
            		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$Lang['Admission']['PICLC']['PreIntermediate']?></center></td>
            		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$Lang['Admission']['PICLC']['Beginner']?></center></td>
            	</tr>
            	
            	<?php
            	$areas = array('Speaking', 'Listening', 'Reading', 'Writing');
            	foreach($areas as $area){
            	?>
                	<tr>
                		<td class="field_title"><?=$Lang['Admission']['PICLC'][$area] ?></td>
                		<?php
                		for($i=5;$i>=1;$i--){
                		    if($IsConfirm){
                		        $checked = ($formData["StudentEnglishLevel_{$area}"] == $i)?'checked':'';
                		        $disabled = ($formData["StudentEnglishLevel_{$area}"] == $i)?'':'disabled';
                		    }else{
                		        $checked = ($allCustInfo["StudentEnglishLevel_{$area}"][0]['Value'] == $i)?'checked':'';
                		        $disabled = '';
                		    }
                		?>
                    		<td style="background:#fcfcfc">
                        		<label style="width:100%;height:100%;">
                        			<center>
                        				<?php if($IsConfirm){ ?>
                            				<?php if($formData['StudentFirstLanguageIsEnglish'] == 'Y' && $checked){ ?>
                                				<input 
                                    				type="hidden" 
                                    				value="<?=$i ?>" 
                                    				id="StudentEnglishLevel_<?=$area.$i ?>" 
                                    				name="StudentEnglishLevel_<?=$area ?>" 
                                    				<?=$checked ?>
                                				/>
                            				<?php } ?>
                            				<input type="radio" <?=$checked ?> onclick="return false;" <?=$disabled ?> />
                        				<?php }else{ ?>
                            				<input 
                                				type="radio" 
                                				value="<?=$i ?>" 
                                				id="StudentEnglishLevel_<?=$area.$i ?>" 
                                				name="StudentEnglishLevel_<?=$area ?>" 
                                				<?=$checked ?>
                            				/>
                        				<?php } ?>
                        			</center>
                    			</label>
                    		</td>
                		<?php
                		}
                		?>
                	</tr>
            	<?php 
            	}
            	?>
            </table>
            				
		</div>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['StudentHowLongLearnEnglish']?> 
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentHowLongLearnEnglish']?>
		<?php }else{ ?>
			<input name="StudentHowLongLearnEnglish" type="text" id="StudentHowLongLearnEnglish" class="textboxtext" value="<?=$allCustInfo['StudentHowLongLearnEnglish'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['StudentForeignLanguage']?> 
	</td>
	<td colspan="3">
		<?php 
		if($IsConfirm){ 
		    if(empty($formData['StudentForeignLanguage'])){
		        echo ' -- ';
		    }else{
		        foreach($formData['StudentForeignLanguage'] as $studentForeignLanguageId){
		            $studentForeignLanguage = $admission_cfg['StudentForeignLanguage'][$studentForeignLanguageId];
		            $details = $formData["StudentForeignLanguage_{$studentForeignLanguage}Details"];
        ?>
	            	<div>
	            		<span style="width: 80px;display: inline-block;">
	            			<?=$Lang['Admission']['PICLC']['StudentForeignLanguageType'][$studentForeignLanguage] ?>
	            		</span>
	            		
	            		<?php if($details){ ?>
	            			<span>( <?=$details ?> )</span>
	            		<?php } ?>
	            	</div>
        <?php
		        }
		    }
		}else{ 
	        $studentForeignLanguageStr = $allCustInfo["StudentForeignLanguage"][0]['Value'];
	        $studentForeignLanguageArr = explode(',', $studentForeignLanguageStr);
		    foreach($admission_cfg['StudentForeignLanguage'] as $value => $studentForeignLanguage){ 
		        $checked = (in_array($value, $studentForeignLanguageArr))?'checked':'';
	    ?>
    			<div class="studentForeignLanguageOptionDiv">
        			<input type="checkbox" id="StudentForeignLanguage_<?=$studentForeignLanguage ?>" name="StudentForeignLanguage[]" value="<?=$value ?>" <?=$checked ?> />
        			<label for="StudentForeignLanguage_<?=$studentForeignLanguage ?>" style="margin-bottom: 5px;">
            			<?=$Lang['Admission']['PICLC']['StudentForeignLanguageType'][$studentForeignLanguage] ?>
                    </label>
                    
                    <?php if($studentForeignLanguage == 'others'){ ?>
                        (
                            <label for="StudentForeignLanguage_<?=$studentForeignLanguage ?>Details">
                            	<?=$Lang['Admission']['PICLC']['PleaseSpecify'] ?>:
                        	</label>
                        	<input id="StudentForeignLanguage_<?=$studentForeignLanguage ?>Details" name="StudentForeignLanguage_<?=$studentForeignLanguage ?>Details" value="<?=$allCustInfo["StudentForeignLanguage_{$studentForeignLanguage}Details"][0]['Value'] ?>" />
                        )
                    <?php } ?>
                </div>
        <?php 
		    }
		} 
		?>
	</td>
</tr>
</table>-->

<script>
$(function(){
	'use strict';
	$('#StudentFirstLanguageIsEnglish_N').click(function(){
		$('#StudentFirstLanguageIsEnglishDiv').slideDown();
	});
	$('#StudentFirstLanguageIsEnglish_Y').click(function(){
		$('#StudentFirstLanguageIsEnglishDiv').slideUp();
	});
});
</script>
