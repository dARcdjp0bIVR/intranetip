<?php if($IsConfirm || $IsUpdate){ ?>
	<h1><?=$Lang['Admission']['docsUpload'] ?></h1>
<?php } ?>

<table class="form_table" style="font-size: 15px">
    <?php if(!$IsConfirm){ ?>
        <tr>
    		<td colspan="2">
    			<span class="date_time">
        			<?=$Lang['Admission']['document'] ?>
    				(
        				<?=$Lang['Admission']['msg']['birthCertFormat'] ?>
        				<?= ($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1') ?> MB
    				)
    			</span>
    		</td>
    	</tr>
    <?php } ?>
    <tr>
		<td class="field_title">
			<?=$Lang['Admission']['personalPhoto'] ?>
		</td>
		<td>
			<?php if($IsConfirm){ ?>
				<?=stripslashes($fileData['StudentPersonalPhoto']) ?>
			<?php }else{ ?>
				<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" value="Add" class="" accept=".gif, .jpeg, .jpg, .png" data-is-optional="1" data-teacher-is-optional="1" />
			<?php } ?>
			<?=$viewFilePath ?>
		</td>
	</tr>
	
	<?php
	for($i=0;$i<$attachment_settings_count;$i++) {
	    $attachment_name = $attachment_settings[$i]['AttachmentName'];
	    
	    $tempAttachmentName = '';
	    if($admission_cfg['BirthCertType']){
		    foreach($admission_cfg['BirthCertType'] as $type){
	    	    if($Lang['Admission']['PICLC']['documentUpload'][$type][$i]){
	    	        $tempAttachmentName .= "<span id=\"docUploadRemarks\" class=\"docUpload docUpload_{$type}\" style=\"display:none;\">{$Lang['Admission']['PICLC']['documentUpload'][$type][$i]}</span>";
	    	    }
		    }
	    }
	    $attachment_name = ($tempAttachmentName)?$tempAttachmentName:$attachment_name;
	    
	    $isOptional = $attachment_settings[$i]['IsOptional'];
	    
	    $accept = '.gif, .jpeg, .jpg, .png, .pdf';
	?>
		<tr>
			<td class="field_title">
				<?= (!$isOptional)? $star : '' ?>
				<?=$attachment_name ?>
			</td>
			<td>
				<?php if($IsConfirm){ ?>
					<?=stripslashes($fileData['OtherFile'.$i]) ?>
				<?php }else{ ?>
					<input type="file" name="OtherFile<?=$i?>" id="OtherFile<?=$i?>" value="Add" class="OtherFile" accept="<?=$accept ?>" data-is-optional="<?=$isOptional ?>" data-teacher-is-optional="1" />
				<?php } ?>
				
				<?php
				if(
				    is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]) && 
				    ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['OtherFile'.$i])
		        ){
				?>
					<a href="download_attachment.php?type=<?=$attachment_settings[$i]['AttachmentName'] ?>" target="_blank" ><?=$Lang['Admission']['viewSubmittedFile'] ?></a>
			    <?php 
				}
				?>
			    
		    </td>
	    </tr>
	<?php 
	}
	if(!$IsConfirm && $IsUpdate){
	?>
    	<tr>
    		<td colspan="2">
    			<?=$star ?>
				<?=$Lang['Admission']['leaveBlankIfNoUpdate'] ?>
			</td>
    	</tr>
	<?php 
	}
	?>
</table>