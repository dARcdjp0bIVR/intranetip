<?php

global $libkis_admission;

$StudentInfo = current($libkis_admission->getApplicationStudentInfo($schoolYearID,'',$applicationInfo['applicationID']));
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>

<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Student']?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Mother']?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Father']?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['PICLC']['FirstLanguage']?> </td>
		<td class="form_guardian_field">
		<input name="StudentFirstLanguage" type="text" id="StudentFirstLanguage" class="textboxtext" value="<?=$allCustInfo['StudentFirstLanguage'][0]['Value']?>"/>
		</td>
		<td class="form_guardian_field">
		<input name="MotherFirstLanguage" type="text" id="MotherFirstLanguage" class="textboxtext" value="<?=$allCustInfo['MotherFirstLanguage'][0]['Value']?>"/>
		</td>
		<td class="form_guardian_field">
		<input name="FatherFirstLanguage" type="text" id="FatherFirstLanguage" class="textboxtext" value="<?=$allCustInfo['FatherFirstLanguage'][0]['Value']?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['PICLC']['SecondLanguage']?> </td>
		<td class="form_guardian_field">
		<input name="StudentSecondLanguage" type="text" id="StudentSecondLanguage" class="textboxtext" value="<?=$allCustInfo['StudentSecondLanguage'][0]['Value']?>"/>
		</td>
		<td class="form_guardian_field">
		<input name="MotherSecondLanguage" type="text" id="MotherSecondLanguage" class="textboxtext" value="<?=$allCustInfo['MotherSecondLanguage'][0]['Value']?>"/>
		</td>
		<td class="form_guardian_field">
		<input name="FatherSecondLanguage" type="text" id="FatherSecondLanguage" class="textboxtext" value="<?=$allCustInfo['FatherSecondLanguage'][0]['Value']?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['PICLC']['ThirdLanguage']?> </td>
		<td class="form_guardian_field">
		<input name="StudentThirdLanguage" type="text" id="StudentThirdLanguage" class="textboxtext" value="<?=$allCustInfo['StudentThirdLanguage'][0]['Value']?>"/>
		</td>
		<td class="form_guardian_field">
		<input name="MotherThirdLanguage" type="text" id="MotherThirdLanguage" class="textboxtext" value="<?=$allCustInfo['MotherThirdLanguage'][0]['Value']?>"/>
		</td>
		<td class="form_guardian_field">
		<input name="FatherThirdLanguage" type="text" id="FatherThirdLanguage" class="textboxtext" value="<?=$allCustInfo['FatherThirdLanguage'][0]['Value']?>"/>
		</td>
	</tr>
</table>

<!--<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="20%">
    	<col width="30%">
    	<col width="20%">
    </colgroup>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['LanguageSpokenAtHome']?> 
    	</td>
    	<td colspan="3">
			<input name="LangSpokenAtHome" type="text" id="LangSpokenAtHome" class="textboxtext" value="<?=$StudentInfo['LangSpokenAtHome']?>"/>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['StudentFirstLanguageIsEnglish']?> 
    	</td>
    	<td colspan="3">
    		<?php
    		    $checkedY = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'Y')? 'checked' : '';
    		    $checkedN = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N')? 'checked' : '';
    		    $displayStyle = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N')? 'display: block;' : 'display: none;';
    		?>
			<div>
    			<input type="radio" value="Y" id="StudentFirstLanguageIsEnglish_Y" name="StudentFirstLanguageIsEnglish" <?=$checkedY ?>>
    			<label for="StudentFirstLanguageIsEnglish_Y"><?=$kis_lang['Admission']['yes'] ?> <?=$kis_langEn['Admission']['yes'] ?></label>
    			&nbsp;
    			&nbsp;
    			<input type="radio" value="N" id="StudentFirstLanguageIsEnglish_N" name="StudentFirstLanguageIsEnglish" <?=$checkedN ?>>
    			<label for="StudentFirstLanguageIsEnglish_N"><?=$kis_lang['Admission']['no'] ?> <?=$kis_langEn['Admission']['no'] ?></label>
			</div>

    		<div id="StudentFirstLanguageIsEnglishDiv" style="<?=$displayStyle ?>">
    			
                <table class="form_table" style="font-size: 13px">
                    <colgroup>
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                    </colgroup>
                	<tr>
                		<td class="form_guardian_head" rowspan="2"><center><?=$kis_lang['Admission']['PICLC']['SkillAreas'] ?></center></td>
                		<td class="form_guardian_head" colspan="5"><center><?=$kis_lang['Admission']['PICLC']['EnglishAssessmentOfProficiency'] ?></center></td>
                	</tr>
                	<tr>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['Advanced'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['UpperIntermediate'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['Intermediate'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['PreIntermediate'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['Beginner'] ?></center></td>
                	</tr>
                	
                	<?php
                	$areas = array('Speaking', 'Listening', 'Reading', 'Writing');
                	foreach($areas as $area){
                	?>
                    	<tr>
                    		<td class="field_title"><?=$kis_lang['Admission']['PICLC'][$area] ?></td>
                    		<?php
                    		for($i=5;$i>=1;$i--){
                		        $checked = ($allCustInfo["StudentEnglishLevel_{$area}"][0]['Value'] == $i)?'checked':'';
                    		?>
                        		<td style="background:#fcfcfc">
                            		<label style="width:100%;height:100%;">
                            			<center>
                            				<input 
                                				type="radio" 
                                				value="<?=$i ?>" 
                                				id="StudentEnglishLevel_<?=$area.$i ?>" 
                                				name="StudentEnglishLevel_<?=$area ?>" 
                                				<?=$checked ?>
                            				/>
                            			</center>
                        			</label>
                        		</td>
                    		<?php
                    		}
                    		?>
                    	</tr>
                	<?php 
                	}
                	?>
                </table>
                				
    		</div>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['StudentHowLongLearnEnglish']?> 
    	</td>
    	<td colspan="3">
			<input name="StudentHowLongLearnEnglish" type="text" id="StudentHowLongLearnEnglish" class="textboxtext" value="<?=$allCustInfo['StudentHowLongLearnEnglish'][0]['Value']?>"/>
    	</td>
    </tr>
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['StudentForeignLanguage']?> 
    	</td>
    	<td colspan="3">
			<?php
    	        $studentForeignLanguageStr = $allCustInfo["StudentForeignLanguage"][0]['Value'];
    	        $studentForeignLanguageArr = explode(',', $studentForeignLanguageStr);
    		    foreach($admission_cfg['StudentForeignLanguage'] as $value => $studentForeignLanguage){ 
    		        $checked = (in_array($value, $studentForeignLanguageArr))?'checked':'';
    	    ?>
        			<div class="studentForeignLanguageOptionDiv">
            			<input type="checkbox" id="StudentForeignLanguage_<?=$studentForeignLanguage ?>" name="StudentForeignLanguage[]" value="<?=$value ?>" <?=$checked ?> />
            			<label for="StudentForeignLanguage_<?=$studentForeignLanguage ?>" style="margin-bottom: 5px;">
                			<?=$kis_lang['Admission']['PICLC']['StudentForeignLanguageType'][$studentForeignLanguage] ?>
                        </label>
                        
                        <?php if($studentForeignLanguage == 'others'){ ?>
                            (
                                <label for="StudentForeignLanguage_<?=$studentForeignLanguage ?>Details">
                                	<?=$kis_lang['Admission']['PICLC']['PleaseSpecify'] ?>:
                            	</label>
                            	<input id="StudentForeignLanguage_<?=$studentForeignLanguage ?>Details" name="StudentForeignLanguage_<?=$studentForeignLanguage ?>Details" value="<?=$allCustInfo["StudentForeignLanguage_{$studentForeignLanguage}Details"][0]['Value'] ?>" />
                            )
                        <?php } ?>
                    </div>
            <?php 
    		    }
    	    ?>
    	</td>
    </tr>
</table>-->






<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();

	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

$('#StudentFirstLanguageIsEnglish_N').click(function(){
	$('#StudentFirstLanguageIsEnglishDiv').slideDown();
});
$('#StudentFirstLanguageIsEnglish_Y').click(function(){
	$('#StudentFirstLanguageIsEnglishDiv').slideUp();
});

function checkValidForm(){
	return true;
}

</script>