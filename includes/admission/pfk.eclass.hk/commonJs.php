<!-- Using: Pun -->
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">

// autocomplete for inputselect fields
$('.inputselect').each(function(){
	var this_id = $(this).attr('id');
	if($(this).length > 0){
		$(this).autocomplete(
	      "ajax_get_suggestions.php",
	      {
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			extraParams: {'field':this_id},
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px'
	  		}
	    );
	}
});


var isUpdatePeriod =<?=($allowToUpdate)? 'true' : 'false'; ?>;

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
var preventBackTimeout = setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>");
		window.onbeforeunload = '';
		form1.submit();
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	var _alert = window.alert;
	window.alert = function(a){ /*console.log(a);*/ };
	
	var isValid = check_input_info(form1);
	
	window.alert = _alert;
	return isValid;
}
function check_docs_upload2(form1) {
	var _alert = window.alert;
	window.alert = function(a){ /*console.log(a);*/ };
	
	var isValid = check_docs_upload(form1);
	
	window.alert = _alert;
	return isValid;
}

/*
    hkid format:  A123456(7)
    A1234567
    AB123456(7)
    AB1234567
*/
function check_hkid(hkid) {
    // hkid = $.trim(hkid);
    // hkid = hkid.replace(/\s/g, '');
    // hkid = hkid.toUpperCase();
    // $(":input[name='id_no']").val(hkid);
    
    var re = /^([A-Z]{1})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
    var ra = re.exec(hkid);
    
    if (ra != null) {
    	var p1 = ra[1];
    	var p2 = ra[2];
    	var p3 = ra[4];
    	var check_sum = 0;
    	if (p1.length == 2) {
    		check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
    	}else if (p1.length == 1){
    		check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
    	}
    
    	check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
    	var check_digit = 11 - (check_sum % 11);
    	if(check_digit == '11'){
    		check_digit = 0;
    	}else if(check_digit == '10'){
    		check_digit = 'A';
    	}
    	
    	return (check_digit == p3 );
    }
	return false;
}

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}
function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}
function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function goto(current,page){
	var isTeacherInput =<?= ($lac->isInternalUse($_GET['token']))?1:0 ?>;
	var isValid = true;
	if(page == 'step_instruction'){
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}else if(page == 'step_docs_upload'){
		isValid = check_input_info($("form")[0]);
	}else if(page == 'step_payment' || (isTeacherInput && page == 'step_confirm')){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
	   /* Clear result div*/
		var chk_ary = $('input#Agree');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['msg']['tickAgreeTermCondition']?>");
				isValid = false;
			}
		}
	}
	
	// Henry added [20151013]
	if(current == 'step_index' && page == 'step_update_input_form'){
		if(form1.InputApplicationID.value==''){
			alert("<?=$Lang['Admission']['msg']['enterApplicationNumber']?>");	
			form1.InputApplicationID.focus();
			return false;
		}else if(form1.InputStudentBirthCertNo.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>");	
			form1.InputStudentBirthCertNo.focus();
			return false;
		}else if(!form1.InputStudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
			if(form1.InputStudentDateOfBirth.value!=''){
				alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>");
			}else{
				alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>");	
			}
			
			form1.InputStudentDateOfBirth.focus();
			return false;
		}
		
<?if($lac->IsAfterUpdatePeriod()){?>
		clearTimeout(preventBackTimeout);
		window.onbeforeunload = '';
<?}?>
	
	   /* Get some values from elements on the page: */
	   var values = $("#form1").serialize();
		
	   /* Send the data using post and put the results in a div */
	   $.ajax({
	       url: "ajax_valid_for_update_form.php",
	       type: "post",
	       data: values,
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           $("#InputApplicationID").val('');
	           $("#InputStudentBirthCertNo").val('');
	           $("#InputStudentDateOfBirth").val('');
	           if(data==0){
	           		alert("<?=$Lang['Admission']['msg']['incorrectApplicationNumBirthCertNum']?>");	
					form1.InputStudentBirthCertNo.focus();
					isValid = false;
					return false;
	           }
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   
	   /* Send the data using post and put the results in a div */
	   $.ajax({
	       url: "ajax_get_whole_form.php",
	       type: "post",
	       data: values,
	       success: function(data){
	           $("#formContainer").html(data);
	           kis.datepicker('.datepicker');

	       },
	       error:function(){
	           $("#result").html('There is error while submit');
	       }
	   });
	}
	
	if(current == 'step_update_input_form' && page == 'step_confirm'){
		/* get the birthday range of the form level */
		var values = $("#form1").serialize();
	   $.ajax({
	       url: "ajax_get_bday_range.php",
	       type: "post",
	       data: values,
	       async: false,
	       success: function(data){
	           dOBRange = data.split(",");
	       },
	       error:function(){
	           $("#result").html('There is error while submit');
	       }
	   });
		isValid1 = true;//check_choose_class($("form")[0]);
		isValid2 = check_input_info($("form")[0]);
		isValid3 = check_docs_upload($("form")[0]);
		
		if(isValid1 && isValid2 && isValid3){
		/* Clear result div*/
		   $("#step_confirm .admission_board").html('');
		
		   	var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_update_confirm.php?encoded=1",
		       type: "post",
		       data: values,
		       async: false,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           if(data == ''){
		           	isValid = false;
		           	alert("<?=$Lang['Admission']['msg']['systemBusy']?>");
		           }
		           var decoded = $("#step_confirm .admission_board").html(data).text();
		           $("#step_confirm .admission_board").html(decoded);

					readFileURL($("#StudentPersonalPhoto").get(0));
					for(var i=0;i<file_count;i++)
					{
						var file_element = file_ary.get(i);
						readFileURL(file_element);
					}
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		           isValid = false;
		           alert("<?=$Lang['Admission']['msg']['systemBusy']?>");
		       }
		   });
		   }
		   else{
		   		return false;
		   }
	}
	
	if(isValid){
		$('#'+current).hide();
		$('#'+page).show();
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		if(isValid){
		   /* Get some values from elements on the page: */
		   $('#formContainer').html('');
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_whole_form.php",
		       type: "post",
		       data: values,
		       success: function(data){
		    	   $('#formContainer').html(data);
		    	   
		    	   // autocomplete for inputselect fields
		    	   $('.inputselect').each(function(){
		    	   	var this_id = $(this).attr('id');
		    	   	if($(this).length > 0){
		    	   		$(this).autocomplete(
		    	   	      "ajax_get_suggestions.php",
		    	   	      {
		    	   	  			delay:3,
		    	   	  			minChars:1,
		    	   	  			matchContains:1,
		    	   	  			extraParams: {'field':this_id},
		    	   	  			autoFill:false,
		    	   	  			overflow_y: 'auto',
		    	   	  			overflow_x: 'hidden',
		    	   	  			maxHeight: '200px'
		    	   	  		}
		    	   	    );
		    	   	}
		    	   });
		    	   kis.datepicker('.datepicker');	
		    	   $('.datepicker').click(function(){
		    	   		$(this).datepicker( "show" );
		    	   });
		       },
		       error:function(e){
		           //alert("failure");
		           $("#formContainer").html('There is error while submit');
		           try{
		           		console.err(e);
		           }catch(err){}
		       }
		   });
		}
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();	 
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
			   window.scrollTo(0,0);
	}
	
	if(current != 'step_update_input_form' && page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm .admission_board").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php?encoded=1",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           //$("#step_confirm").html(data);

		           var decoded = $("#step_confirm .admission_board").html(data).text();
		           $("#step_confirm .admission_board").html(decoded);
					readFileURL($("#StudentPersonalPhoto").get(0));
					for(var i=0;i<file_count;i++)
					{
						var file_element = file_ary.get(i);
						readFileURL(file_element);
					}
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){
	document.getElementById('form1').target = '';
	if(!isUpdatePeriod)
		document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	if(isUpdatePeriod)
		return confirm('<?=$Lang['Admission']['msg']['suresubmit']?>');	
		
	if(check_input_info($("form")[0]) && check_docs_upload($("form")[0])){	
		return confirm('<?=$Lang['Admission']['msg']['checkInfoCarefully']?>');
	}
	else{
		return false;
	}
}

function check_choose_class(form1) {
	var chk_ary = $('input#Agree');
	var chk_count = chk_ary.length;
	for(var i=0;i<chk_count;i++)
	{
		var chk_element = chk_ary.get(i);
		if(chk_element.checked == false){
			alert("<?=$Lang['Admission']['msg']['tickAgreeTermCondition']?>");
			return false;
		}
	}
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	//For debugging only
// 	return true;
	
	return (
    	check_student_form() &&
    	check_parent_form() &&
    	check_other_form()
	);
}

function check_student_form(){
	//For debugging only
// 	return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	dOBRange = dOBRange || ['',''];
	/******** Basic init END ********/
	
	/**** School START ****/
	var firstChoice = $('#schoolFirstChoice').val();
	var secondChoice = $('#schoolSecondChoice').val();
	var thirdChoice = $('#schoolThirdChoice').val();
	if(
		firstChoice == '' &&
		secondChoice == '' &&
		thirdChoice == '' 
	){
		alert("<?=$Lang['Admission']['PFK']['msg']['selectSchool']?>");	
		$('#schoolFirstChoice').focus();
		return false;
	}
	
	if(
		(firstChoice != '' && secondChoice != '' && firstChoice == secondChoice) ||
		(firstChoice != '' && thirdChoice != '' && firstChoice == thirdChoice) ||
		(secondChoice != '' && thirdChoice != '' && secondChoice == thirdChoice)
	){
		alert("<?=$Lang['Admission']['PFK']['msg']['selectDifferentSchool']?>");	
		$('#schoolFirstChoice').focus();
		return false;
	}
	/**** School END ****/

	/**** Name START ****/
	if(
		!checkNaNull($('#StudentChineseSurname').val()) && 
		!checkIsChineseCharacter($('#StudentChineseSurname').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");	
		$('#StudentChineseSurname').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentChineseFirstName').val()) && 
		!checkIsChineseCharacter($('#StudentChineseFirstName').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");	
		$('#StudentChineseFirstName').focus();
		return false;
	}
	
	
	if($('#StudentEnglishSurname').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>");	
		$('#StudentEnglishSurname').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishSurname').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishSurname').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>");	
		$('#StudentEnglishSurname').focus();
		return false;
	}
	
	
	if($('#StudentEnglishFirstName').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>");	
		$('#StudentEnglishFirstName').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishFirstName').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishFirstName').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>");	
		$('#StudentEnglishFirstName').focus();
		return false;
	}
	/**** Name END ****/

	/**** DOB START ****/
	if( !$('#StudentDateOfBirth').val().match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/) ){
		if($('#StudentDateOfBirth').val()!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>");
		}else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>");	
		}
		
		$('#StudentDateOfBirth').focus();
		return false;
	} 
	if(
		(
			dOBRange[0] !='' && 
			$('#StudentDateOfBirth').val() < dOBRange[0] 
		) || (
			dOBRange[1] !='' && 
			$('#StudentDateOfBirth').val() > dOBRange[1]
		)
	){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>");
		$('#StudentDateOfBirth').focus();
		return false;
	} 
	/**** DOB END ****/

	/**** Gender START ****/
//	if($('input[name=StudentGender]:checked').length == 0){
//		alert("<?=$Lang['Admission']['msg']['selectgender']?>");	
//		$('input[name=StudentGender]:first').focus();
//		return false;
//	}
	/**** Gender END ****/
	
	/**** Personal Identification START ****/
	if($('#StudentBirthCertNo').val().trim()==''){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterPersonalIdentificationNo']?>");	
		$('#StudentBirthCertNo').focus();
		return false;
	}
// 	if(!check_hkid(form1.StudentBirthCertNo.value)){
//    	alert("<?=$Lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>");
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }

    if(!isUpdatePeriod && checkBirthCertNo() > 0){
    	alert("<?=$Lang['Admission']['PICLC']['msg']['duplicateBirthCertNo']?>");	
    	form1.StudentBirthCertNo.focus();
    	return false;
    }
	/**** Personal Identification END ****/


	/**** Nationality START ****/
//	if($('#StudentCounty').val().trim()==''){
//		alert("<?=$Lang['Admission']['msg']['enternationality']?>");	
//		$('#StudentCounty').focus();
//		return false;
//	}
	/**** Nationality END ****/

	/**** Address START ****/
	if($('#StudentAddress').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterhomeaddress']?>");	
		$('#StudentAddress').focus();
		return false;
	}
	/**** Address END ****/

	/**** Registed Residence START ****/
//	if($('#StudentRegistedResidence').val().trim()==''){
//		alert("<?=$Lang['Admission']['PICLC']['msg']['enterRegistedResidence']?>");	
//		$('#StudentRegistedResidence').focus();
//		return false;
//	}
	/**** Registed Residence END ****/

	/**** Native Place START ****/
//	if($('#StudentNativePlace').val().trim()==''){
//		alert("<?=$Lang['Admission']['PICLC']['msg']['enterNativePlace']?>");	
//		$('#StudentNativePlace').focus();
//		return false;
//	}
	/**** Native Place END ****/

	/**** Email START ****/
	if($('#StudentEmail').val().trim()==''){
		alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	if($('#StudentEmail').val().trim()!='' && !re.test($('#StudentEmail').val().trim())){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	/**** Email END ****/
	
	return true;
}

function check_parent_form(){
	//For debugging only
	//return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	
	var isValid = true;
	/******** Basic init END ********/
	
	
	/**** All empty START ****/
	var parentHasInfoArr = [];
	var _text = '';
	$('.parentInfo input').each(function(){
		//if($(this).val().trim() != ''){
			parentHasInfoArr.push($(this).data('parent'));
		//}
	});
	
	/*if(parentHasInfoArr.length == 0){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterAtLeastOneParent']?>");
		$('#G1EnglishSurname').focus();
		return false;
	}*/
	/**** All empty END ****/
	

	/**** Name START ****/	
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(EnglishSurname|EnglishFirstName)/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
    		this.focus();
    		return isValid = false;
    	}
    	if(
    		!checkNaNull(this.value) &&
    		!checkIsEnglishCharacter(this.value)
    	){
    		alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(ChineseSurname|ChineseFirstName)/);
    }).each(function(){
    	if(
			$.trim(this.value)!='' &&
    		!checkNaNull(this.value) &&
    		!checkIsChineseCharacter(this.value)
    	){
    		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name END ****/
	
	/**** Nationality START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dNationality/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['msg']['enternationality']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Nationality START ****/
	
	/**** Native Place START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dNativePlace/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['PICLC']['msg']['enterNativePlace']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Native Place START ****/
	
	/**** Employer START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dCompany/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['PICLC']['msg']['enterEmployer']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Employer START ****/
	
	/**** Position START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dJobPosition/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['PICLC']['msg']['enterPosition']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Position START ****/
	
	/**** Business Address START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dOfficeAddress/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['PICLC']['msg']['enterBusinessAddress']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Business Address START ****/
	
	/**** Business Phone START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dOfficeTelNo/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['PICLC']['msg']['enterBusinessPhone']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Business Phone START ****/
	
	/**** Mobile START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dMobile/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['msg']['entermobilephoneno']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Mobile START ****/
	
	/**** Email START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dEmail/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//
//    	if(!re.test($.trim(this.value))){
//    		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Email START ****/

	/**** WeChat ID START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dWeChatID/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$Lang['Admission']['PICLC']['msg']['enterWeChatId']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** WeChat ID START ****/
	
	return true;
}

function check_other_form(){
	//For debugging only
// 	return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	/******** Basic init END ********/


	/**** ChildPlacedOutStandardGroup START ****/
	if($('#ChildPlacedOutStandardGroup_Y').prop('checked') && $('#ChildPlacedOutStandardGroup_Details').val().trim()==''){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterDetails']?>");	
		$('#ChildPlacedOutStandardGroup_Details').focus();
		return false;
	}
	if($('#ChildPlacedOutStandardGroup_N').prop('checked')){
		$('#ChildPlacedOutStandardGroup_Details').val('');
	}
	/**** ChildPlacedOutStandardGroup END ****/

	/**** ChildSpecialClassTalent START ****/
	if($('#ChildSpecialClassTalent_Y').prop('checked') && $('#ChildSpecialClassTalent_Details').val().trim()==''){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterDetails']?>");	
		$('#ChildSpecialClassTalent_Details').focus();
		return false;
	}
	if($('#ChildSpecialClassTalent_N').prop('checked')){
		$('#ChildSpecialClassTalent_Details').val('');
	}
	/**** ChildSpecialClassTalent END ****/

	/**** ChildEducationalPsychologist START ****/
	if($('#ChildEducationalPsychologist_Y').prop('checked') && $('#ChildEducationalPsychologist_Details').val().trim()==''){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterDetails']?>");	
		$('#ChildEducationalPsychologist_Details').focus();
		return false;
	}
	if($('#ChildEducationalPsychologist_N').prop('checked')){
		$('#ChildEducationalPsychologist_Details').val('');
	}
	/**** ChildEducationalPsychologist END ****/

	/**** StudentFirstLanguage START ****/
//	if($('#StudentFirstLanguage').val().trim()==''){
//		alert("<?=$Lang['Admission']['PICLC']['msg']['enterFirstLanguage']?>");	
//		$('#StudentFirstLanguage').focus();
//		return false;
//	}
	/**** StudentFirstLanguage END ****/
	
	
	/**** MotherFirstLanguage START ****/
//	if($('#PG_Type_M').prop('checked') && $('#MotherFirstLanguage').val().trim()==''){
//		alert("<?=$Lang['Admission']['PICLC']['msg']['enterFirstLanguage']?>");	
//		$('#MotherFirstLanguage').focus();
//		return false;
//	}
	/**** MotherFirstLanguage END ****/
	
	/**** FatherFirstLanguage START ****/
//	if($('#PG_Type_F').prop('checked') && $('#FatherFirstLanguage').val().trim()==''){
//		alert("<?=$Lang['Admission']['PICLC']['msg']['enterFirstLanguage']?>");	
//		$('#FatherFirstLanguage').focus();
//		return false;
//	}
	/**** FatherFirstLanguage END ****/

	/**** StudentFirstLanguageIsEnglish START ****/
	if($('#StudentFirstLanguageIsEnglish_N').prop('checked')){
		if($('input[name="StudentEnglishLevel_Speaking"]:checked').length == 0){
			alert("<?=$Lang['Admission']['PICLC']['msg']['selectAssessmentOfProficiencyInEnglish']?>");	
			$('#StudentEnglishLevel_Speaking5').focus();
			return false;
		}
		
		if($('input[name="StudentEnglishLevel_Listening"]:checked').length == 0){
			alert("<?=$Lang['Admission']['PICLC']['msg']['selectAssessmentOfProficiencyInEnglish']?>");	
			$('#StudentEnglishLevel_Listening5').focus();
			return false;
		}
		
		if($('input[name="StudentEnglishLevel_Reading"]:checked').length == 0){
			alert("<?=$Lang['Admission']['PICLC']['msg']['selectAssessmentOfProficiencyInEnglish']?>");	
			$('#StudentEnglishLevel_Reading5').focus();
			return false;
		}
		
		if($('input[name="StudentEnglishLevel_Writing"]:checked').length == 0){
			alert("<?=$Lang['Admission']['PICLC']['msg']['selectAssessmentOfProficiencyInEnglish']?>");	
			$('#StudentEnglishLevel_Writing5').focus();
			return false;
		}
	}
	/**** ChildEducationalPsychologist END ****/
	
	return true;
}

function check_docs_upload(form1) {
	var isTeacherInput =<?= ($lac->isInternalUse($_GET['token']))?1:0 ?>;
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}

	var maxFileSize = 1 * 1024 * 1024;
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		maxFileSize =<?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}?>
	
	var $files = $('#StudentPersonalPhoto, input[type=file][name*=OtherFile]');

	var isValid = true;
	$files.each(function(){
		var teacherIsOptional = !!$(this).data('teacher-is-optional');
		var isOptional = !!$(this).data('is-optional');

		if(isTeacherInput && teacherIsOptional){
			isOptional = true;
		}

		/**** Empty START ****/
		if(!isUpdatePeriod && !isOptional && $(this).val()==''){
			alert("<?=$Lang['Admission']['PICLC']['msg']['uploadFile']?>");
			$(this).focus();
			return isValid = false;
		}
		/**** Empty END ****/

		/**** File size START ****/
		var filesize = ($(this).val()=='')? 0 : $(this)[0].files[0].size;
		if(!isOldBrowser && filesize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");
			$(this).focus();
			return isValid = false;
		}
		/**** File size END ****/

		/**** File format START ****/
		var format = $(this).val().split('.').pop().toLowerCase();
		var acceptArr = $(this).attr('accept').split(',');

		acceptArr = $.map(acceptArr, function(accept){
			return $.trim(accept).substr(1).toLowerCase();
		});
		if(format && $.inArray(format, acceptArr) == -1){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
			$(this).focus();
			return isValid = false;
		}
		/**** File format END ****/
	});

	return isValid;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

function readFileURL(input) {
	if(navigator.appVersion.indexOf("MSIE 1")==-1 && !(navigator.appName == 'Netscape')){
		$('#href'+input.name).hide();
		$('#div'+input.name).hide();
		return;
	}		
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        	var isOldBrowser = 0;
			if(navigator.appName.indexOf("Internet Explorer")!=-1 || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null))){
				isOldBrowser = 1;
			}
			
        	if(isOldBrowser){
            	$('#href'+input.name).hide();
        	}
        	else{
        		$('#href'+input.name).attr('href', e.target.result);
        	}
        	
            if(input.value.split('.').pop().toUpperCase() == "PDF"){
            	$('#div'+input.name).hide();
            }
            else{
            	$('#img'+input.name).attr('src', e.target.result);
            }
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}
</script>