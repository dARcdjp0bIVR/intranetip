<?php
//using: Pun
/*
 * This page is for admission only. For general KIS config : kis/config.php
 */

  ######## Basic config START ########
$admission_cfg['Status'] = array(); //Please also define lang in admission_lang
$admission_cfg['Status']['pending']	= 1;
$admission_cfg['Status']['waitingforassessment'] = 2;
$admission_cfg['Status']['waitingforinterview']	= 3;
$admission_cfg['Status']['waitingforpayment']	= 4;
//	$admission_cfg['Status']['PICLC_waitingforinterview1']	= 3;
//	$admission_cfg['Status']['PICLC_waitingforinterview2']	= 6;
//	$admission_cfg['Status']['PICLC_waitingforinterview3']	= 7;
$admission_cfg['Status']['confirmed']	= 5;
$admission_cfg['Status']['PICLC_reserve']	= 6;
$admission_cfg['Status']['PICLC_rejected']	= 7;
$admission_cfg['Status']['cancelled']	= 8;

$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
$admission_cfg['PaymentStatus']['OtherPayment']	= 2;

$admission_cfg['DefaultLang'] = "en";
// $admission_cfg['Lang'][0] = 'en';
// $admission_cfg['Lang'][1] = 'gb';
// $admission_cfg['Lang'][2] = 'b5';

$admission_cfg['interview_arrangment']['interview_group_type'] = 'Room';
$admission_cfg['interview_arrangment']['interview_group_name'] = array('FT01', 'FT02', 'FT03', 'FT04', 'FT05', 'FT06', 'FT07', 'FT08', 'FT09',
																		'LG01', 'LG02', 'LG03', 'LG04', 'LG05', 'LG06', 'LG07', 'LG08', 'LG09',
																		'LH01', 'LH02', 'LH03', 'LH04', 'LH05', 'LH06', 'LH07', 'LH08', 'LH09',
																		'YT01', 'YT02', 'YT03', 'YT04', 'YT05', 'YT06', 'YT07', 'YT08', 'YT09',
																		'BA01', 'BA02', 'BA03', 'BA04', 'BA05', 'BA06', 'BA07', 'BA08', 'BA09');
  ######## Basic config END ########



######## Cust config START ########
$admission_cfg['SchoolType'][1] = 'pik';
$admission_cfg['SchoolType'][2] = 'pfik';
$admission_cfg['SchoolType'][3] = 'piik';
$admission_cfg['SchoolType'][4] = 'pstik';
$admission_cfg['SchoolType'][5] = 'paik';
//	$admission_cfg['BirthCertType'][1] = 'foreigner';
//	$admission_cfg['BirthCertType'][2] = 'hkmctw';
//	$admission_cfg['BirthCertType'][3] = 'chinese';

$admission_cfg['AnnualHouseholdIncome'][1] = '0-1M';
$admission_cfg['AnnualHouseholdIncome'][2] = '1M-2M';
$admission_cfg['AnnualHouseholdIncome'][3] = '2M-3M';
$admission_cfg['AnnualHouseholdIncome'][4] = '3M+';

$admission_cfg['StudentForeignLanguage'][1] = 'chi'; // ISO639-2
$admission_cfg['StudentForeignLanguage'][2] = 'fra';
$admission_cfg['StudentForeignLanguage'][3] = 'spa';
$admission_cfg['StudentForeignLanguage'][4] = 'rus';
$admission_cfg['StudentForeignLanguage'][5] = 'ara';
$admission_cfg['StudentForeignLanguage'][6] = 'others';

$admission_cfg['WhereToKnowPis'][1] = 'Friend';
$admission_cfg['WhereToKnowPis'][2] = 'Parents';
$admission_cfg['WhereToKnowPis'][3] = 'Staff';
$admission_cfg['WhereToKnowPis'][4] = 'Website';
$admission_cfg['WhereToKnowPis'][5] = 'Media';
$admission_cfg['WhereToKnowPis'][6] = 'WeChat';
$admission_cfg['WhereToKnowPis'][7] = 'Magazine';
$admission_cfg['WhereToKnowPis'][8] = 'Event';
$admission_cfg['WhereToKnowPis'][9] = 'Other';
$admission_cfg['WhereToKnowPisDetails'] = array('Magazine', 'Event', 'Other');
######## Cust config END ########



######## Fixed config START ########
$admission_cfg['PrintByPDF'] = 1;
$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
$admission_cfg['personal_photo_width'] = 200;
$admission_cfg['personal_photo_height'] = 260;

if($plugin['eAdmission_devMode']){
    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
}else{
    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
}
######## Fixed config END ########