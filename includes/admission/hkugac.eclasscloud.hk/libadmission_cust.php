<?php
// modifying by:
/**
 * ******************
 * Change Log :
 * Date 2018-09-28 [Henry]
 * File Created
 *
 * ******************
 */
include_once("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");

class admission_cust extends \AdmissionSystem\AdmissionCustBase
{

    const STUDENT_ACADEMIC_SCHOOL_COUNT = 3;

    const RELATIVES_COUNT = 3;

    const LEARNING_EXPERIENCE_COUNT = 5;

    public function __construct()
    {
        global $plugin;

        if ($plugin['eAdmission_devMode']) {
            error_reporting(E_ALL & ~E_NOTICE);
            ini_set('display_errors', 1);
        }

        parent::__construct();
        $this->init();
    }

    private function init()
    {
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function ($emailTitle) {
            global $Lang;
            return $Lang['Admission']['HKUGAC']['email']['admissionNotification'];
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummySchoolInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummyRelativesInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_UPDATE_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateStudentInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateParentInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateStudentAcademicBackgroundInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateOtherInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateRelativesInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_INFO END #### */

        /* #### ACTION_APPLICANT_UPDATE_*_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INFO, array(
            $this,
            'updateApplicantInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_*_INFO END #### */
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID)
    {
        $result = true;

        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i++) {
            $sql = "INSERT INTO 
                ADMISSION_STU_PREV_SCHOOL_INFO 
            (
                ApplicationID, 
                SchoolOrder,
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";

            $result = $result && $this->db_db_query($sql);
        }

        if (!$result) {
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummyRelativesInfo($ApplicationID)
    {
        $result = true;

        for ($i = 0; $i < self::RELATIVES_COUNT; $i++) {
            $sql = "INSERT INTO 
                ADMISSION_RELATIVES_AT_SCH_INFO 
            (
                ApplicationID,
				Type, 
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
				'{$i}',
                NOW(),
                '{$this->uid}'
            )";

            $result = $result && $this->db_db_query($sql);
        }

        if (!$result) {
            throw new \Exception('Cannot insert dummy relatives info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentInfo($Data, $ApplicationID)
    {
        // #### Basic info START ####
        $fieldArr = array(
            'ChineseName' => 'StudentChineseName',
            'EnglishName' => 'StudentEnglishName',
            'Gender' => 'StudentGender',
            'Age' => 'StudentAge',
            'Nationality' => 'StudentNationality',
            'BirthCertNo' => 'StudentBirthCertNo',
            'DOB' => 'StudentDateOfBirth',
            'PlaceOfBirth' => 'StudentPlaceOfBirth',
            'AddressChi' => 'StudentAddressChi',
            'Address' => 'StudentAddress',
            'ContactAddressChi' => 'StudentContactAddressChi',
            'ContactAddress' => 'StudentContactAddress',
            'HomeTelNo' => 'StudentHomePhone',
            'Fax' => 'StudentFax'
        );

        $updateSql = '';
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $updateSql .= "$dbField = '{$Data[$dataField]}',";
            }
        }

        // ### Concat name START ####
        if ($Data['StudentDateOfBirthYear'] && $Data['StudentDateOfBirthMonth'] && $Data['StudentDateOfBirthDay']) {
            $updateSql .= "DOB = '{$Data['StudentDateOfBirthYear']}-{$Data['StudentDateOfBirthMonth']}-{$Data['StudentDateOfBirthDay']}',";
        }
        // ### Concat name END ####

        $sql = "UPDATE 
            ADMISSION_STU_INFO
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'";
        $result = $this->db_db_query($sql);
        // #### Basic info END ####

        // #### Cust START ####
        $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => "StudentSTRN",
                'Value' => $Data["StudentSTRN"]
            ), $insertIfNotExists = true);
        // #### Cust END ####

        if (!$result) {
            throw new \Exception('Cannot update student info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateParentInfo($Data, $ApplicationID)
    {
        $result = true;

        $pgType = array(1 => 'F', 2 => 'M', 3 => 'G');

        /**** Update parent basic info START ****/
        $fieldArr = array(
            'EnglishName',
            'ChineseName',
            'JobTitle',
            'Company',
            'OfficeTelNo',
            'Mobile',
            'Email'
        );

        foreach ($pgType as $id => $type) {
            $updateSQL = '';
            foreach ($fieldArr as $field) {
                if (isset($Data["G{$id}{$field}"])) {
                	if($field == 'Email'){
                		$Data["G{$id}{$field}"] = trim($Data["G{$id}{$field}"]);
                	}
                    $updateSQL .= "{$field} = '{$Data["G{$id}{$field}"]}',";
                }
            }

            $sql = "UPDATE 
                ADMISSION_PG_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                PG_TYPE = '{$type}'";
            $result = $result && $this->db_db_query($sql);
        }
        /**** Update parent basic info END ****/

        /**** Update parent extra info START ****/
        foreach ($pgType as $id => $type) {
            if (isset($Data["G{$id}NightTelephone"])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                        'filterApplicationId' => $ApplicationID,
                        'filterCode' => "G{$id}NightTelephone",
                        'Value' => $Data["G{$id}NightTelephone"],
                    ), $insertIfNotExists = true);
            }
        }
        /**** Update parent extra info END ****/

        /**** Update Cust info START ****/
        $custFieldArr = array(
            'ContactPerson',
            'IsTwinsApplied',
            'TwinsAppliedName',
            'TwinsAppliedSTRN',
        );
        foreach ($custFieldArr as $custField) {
            if (isset($Data[$custField])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                        'filterApplicationId' => $ApplicationID,
                        'filterCode' => $custField,
                        'Value' => $Data[$custField],
                    ), $insertIfNotExists = true);
            }
        }
        /**** Update Cust info END ****/

        if (!$result) {
            throw new \Exception('Cannot update parent info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentAcademicBackgroundInfo($Data, $ApplicationID)
    {
        global $admission_cfg;
        $result = true;

        /**** Update school basic info START ****/
        $fieldArr = array(
            'NameOfSchool' => 'Name',
            'Class' => 'Class',
            'Year' => 'Year'
        );

        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i++) {
            $updateSQL = '';
            foreach ($fieldArr as $dbField => $inputField) {
                if (isset($Data["StudentSchool{$i}_{$inputField}"])) {
                    $updateSQL .= "{$dbField} = '{$Data["StudentSchool{$i}_{$inputField}"]}',";
                }
            }

            $sql = "UPDATE
                ADMISSION_STU_PREV_SCHOOL_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                SchoolOrder = '{$i}'";
            $result = $result && $this->db_db_query($sql);
        }
        /**** Update school basic info END ****/

        /**** Update Cust info START ****/
        foreach ($admission_cfg['Subjects'] as $subject) {
            $custField = "F5_Subject_{$subject}";
            if (isset($Data[$custField])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                        'filterApplicationId' => $ApplicationID,
                        'filterCode' => $custField,
                        'Value' => $Data[$custField],
                    ), $insertIfNotExists = true);
            }
            $custField = "F6_Subject_{$subject}";
            if (isset($Data[$custField])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                        'filterApplicationId' => $ApplicationID,
                        'filterCode' => $custField,
                        'Value' => $Data[$custField],
                    ), $insertIfNotExists = true);
            }
        }
        /**** Update Cust info END ****/

        if (!$result) {
            throw new \Exception('Cannot update student academic background info');
        }
        return true;
    }

    /**
     * Admission Form - update Relatives
     */
    protected function updateRelativesInfo($Data, $ApplicationID)
    {
        $result = true;

        /* #### Update relatives info START #### */
        $fieldArr = array(
            'Name' => 'Name',
            'Year' => 'Year',
            'ClassPosition' => 'Class',
            'Relationship' => 'Relationship'
        );

        for ($i = 0; $i < self::RELATIVES_COUNT; $i++) {
            $updateSQL = '';
            foreach ($fieldArr as $dbField => $inputField) {
                if (isset($Data["Relative{$i}_{$inputField}"])) {
                    $updateSQL .= "{$dbField} = '{$Data["Relative{$i}_{$inputField}"]}',";
                }
            }

            $sql = "UPDATE
                ADMISSION_RELATIVES_AT_SCH_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                Type = '{$i}'";
            $result = $result && $this->db_db_query($sql);
        }
        /* #### Update relatives info END #### */

        if (!$result) {
            throw new \Exception('Cannot relatives info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateOtherInfo($Data, $ApplicationID)
    {
        global $admission_cfg;

        $result = true;

        $fieldArr = array(
            'Year' => 'Year',
            'Name' => 'Name'
        );

        for ($i = 0; $i < self::LEARNING_EXPERIENCE_COUNT; $i++) {
            foreach ($fieldArr as $dbField => $dataField) {
                if (isset($Data["StudentExperience{$i}_{$dataField}"])) {
                    $result = $result && $this->updateApplicationCustInfo(array(
                            'filterApplicationId' => $ApplicationID,
                            'filterCode' => "StudentExperience{$i}_{$dbField}",
                            'Value' => $Data["StudentExperience{$i}_{$dataField}"]
                        ), $insertIfNotExists = true);
                }
            }
        }

        // #### Cust info START ####
        $fieldArr = array(
            'WhyChooseSchool' => 'WhyChooseSchool',
            'ApplicantStrengthsWeaknesses' => 'ApplicantStrengthsWeaknesses',
            'IsParentsHKUGAMember' => 'IsParentsHKUGAMember',
            'ParentsHKUGAMemberName' => 'ParentsHKUGAMemberName',
            'ParentsHKUGAMemberNumber' => 'ParentsHKUGAMemberNumber',
            'IsParentsHKUGAEFMember' => 'IsParentsHKUGAEFMember',
            'ParentsHKUGAEFMemberName' => 'ParentsHKUGAEFMemberName',
            'ParentsHKUGAEFMemberNumber' => 'ParentsHKUGAEFMemberNumber'
        );
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                        'filterApplicationId' => $ApplicationID,
                        'filterCode' => $dbField,
                        'Value' => $Data[$dataField]
                    ), $insertIfNotExists = true);
            }
        }
        // #### Cust info END ####

        if (!$result) {
            throw new \Exception('Cannot update other info');
        }
        return true;
    }

    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID)
    {
        $result = true;

        if ($type == 'studentinfo') {
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);
            $result = $result && $this->updateStudentAcademicBackgroundInfo($Data, $ApplicationID);

        } elseif ($type == 'parentinfo') {
            $result = $result && $this->updateParentInfo($Data, $ApplicationID);
        } elseif ($type == 'otherinfo') {
            $result = $result && $this->updateOtherInfo($Data, $ApplicationID);
            $result = $result && $this->updateRelativesInfo($Data, $ApplicationID);
        } elseif ($type == 'remarks') {
            $result = $result && $this->updateApplicationStatus($Data, $ApplicationID);
        } elseif ($type == 'hkugac_academic_performance') {
            $result = $result && $this->updateStudentAcademicBackgroundInfo($Data, $ApplicationID);
        } else {
            $result = false;
        }

        if (!$result) {
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }

    public function getExportHeaderCustom($schoolYearID = '', $classLevelID = '', $type = '')
    {
        global $kis_lang;

        $headerArray = array();
        if ($type == '') {
            $headerArray = $this->getExportHeader($schoolYearID, $classLevelID);
        } elseif ($type == 'status') {
            $headerArray[0] = array(
                $kis_lang['Admission']['applicationno'],
                $kis_lang['Admission']['name'],
                $kis_lang['Admission']['applicationstatus'],
            );
        } elseif ($type == 'interviewTime') {
            $headerArray[0] = array(
                '',
                '',
                '',
                '',
                '',
                $kis_lang['Admission']['HKUGAC']['Inteview1'],
                '',
                '',
                '',
                $kis_lang['Admission']['HKUGAC']['Inteview2'],
                '',
                '',
                '',
                $kis_lang['Admission']['HKUGAC']['Inteview3'],
            );
            $headerArray[1] = array(
                $kis_lang['Admission']['applicationno'],
                $kis_lang['Admission']['name'],
                $kis_lang['Admission']['HKUGAC']['strn'],
                $kis_lang['Admission']['gender'],
                $kis_lang['Admission']['HKUGAC']['CurrentSchoolAttend'],
                $kis_lang['Admission']['interviewdate'],
                $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')',
                $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')',
                $kis_lang['sessiongroup'],
                $kis_lang['Admission']['interviewdate'],
                $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')',
                $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')',
                $kis_lang['sessiongroup'],
                $kis_lang['Admission']['interviewdate'],
                $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')',
                $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')',
                $kis_lang['sessiongroup'],
            );
        }
        return $headerArray;
    }

    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '', $classLevelID = '')
    {
        global $kis_lang, $admission_cfg;

        $headerArray = array();

        $headerArray[] = $kis_lang['Admission']['admissiondate'];
        $headerArray[] = $kis_lang['applicationno'];
        $headerArray[] = $kis_lang['Admission']['applyLevel'];

        // for student info
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['strn'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['chinesename'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['englishname'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['Age'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['nationality'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['birthcertno'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['address'] . '(' . $kis_lang['Admission']['HKUGAC']['Chinese'] . ')';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['address'] . '(' . $kis_lang['Admission']['HKUGAC']['English'] . ')';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['contactaddress'] . '(' . $kis_lang['Admission']['HKUGAC']['Chinese'] . ')';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['contactaddress'] . '(' . $kis_lang['Admission']['HKUGAC']['English'] . ')';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['HomeTelephone'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['Fax'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['year'] . ')(1)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['HKUGAC']['Class'] . ')(1)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['HKUGAC']['School'] . ')(1)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['year'] . ')(2)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['HKUGAC']['Class'] . ')(2)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['HKUGAC']['School'] . ')(2)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['year'] . ')(3)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['HKUGAC']['Class'] . ')(3)';
        $headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAC']['SchoolAttend'] . '(' . $kis_lang['Admission']['HKUGAC']['School'] . ')(3)';
        // for parent info
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAC']['englishname'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAPS']['chinesename'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAC']['Occupation'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAC']['Company'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAC']['Telephone'] . '(' . $kis_lang['Admission']['HKUGAC']['Day'] . ')';
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAC']['Telephone'] . '(' . $kis_lang['Admission']['HKUGAC']['Night'] . ')';
        $headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAC']['Telephone'] . '(' . $kis_lang['Admission']['HKUGAC']['Mobile'] . ')';
        $headerArray['parentInfo2'][] = $kis_lang['Admission']['contactperson'];
        $headerArray['parentInfo2'][] = $kis_lang['Admission']['HKUGAC']['EmailAddress'];

        // for sibling info
        $headerArray['siblingInfo'][] = $kis_lang['Admission']['name'];
        $headerArray['siblingInfo'][] = $kis_lang['Admission']['HKUGAC']['strn'];

        // for other info
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['year'] . ')(1)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] . ')(1)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['year'] . ')(2)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] . ')(2)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['year'] . ')(3)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] . ')(3)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['year'] . ')(4)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] . ')(4)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['year'] . ')(5)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['LearningExperience'] . '(' . $kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] . ')(5)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['WhyChooseSchool'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['ApplicantStrengthsWeaknesses'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['IsParentsHKUGAMember'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['MemberName'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['MembershipNo'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['IsParentsHKUGAEFMember'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['MemberName'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['MembershipNo'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['name'] . ')(1)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['year'] . ')(1)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['HKUGAC']['Class'] . ')(1)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['HKUGAC']['Relationship'] . ')(1)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['name'] . ')(2)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['year'] . ')(2)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['HKUGAC']['Class'] . ')(2)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['HKUGAC']['Relationship'] . ')(2)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['name'] . ')(3)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['year'] . ')(3)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['HKUGAC']['Class'] . ')(3)';
        $headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAC']['SiblingInformation'] . '(' . $kis_lang['Admission']['HKUGAC']['Relationship'] . ')(3)';

        // for academic performance
        foreach ($admission_cfg['Subjects'] as $subjectCode) {
            $headerArray['academicPerformanceInfo'][] = $kis_lang['Admission']['HKUGAC']['Subjects'][$subjectCode];
        }
        foreach ($admission_cfg['Subjects'] as $subjectCode) {
            $headerArray['academicPerformanceInfo'][] = $kis_lang['Admission']['HKUGAC']['Subjects'][$subjectCode];
        }

        // for official use
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod']; // Omas
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['KTLMSKG']['bankName'] . ")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['KTLMSKG']['chequeNum'] . ")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['receiptcode'] . ")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['date'] . ")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['handler'] . ")";
//		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'];
//		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (1)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (2)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (3)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];

        // ####### Header START ########
        // ### Admission Info START ####
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        // ### Admission Info END ####

        // ### Student Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['childDetails'];
        for ($i = 0; $i < count($headerArray['studentInfo']) - 1; $i++) {
            $exportColumn[0][] = "";
        }
        // ### Student Info END ####

        // ### Parent Info START ####
        $exportColumn[0][] = " {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['F']})";
        for ($i = 0; $i < count($headerArray['parentInfo']) - 1; $i++) {
            $exportColumn[0][] = "";
        }

        $exportColumn[0][] = " {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['M']})";
        for ($i = 0; $i < count($headerArray['parentInfo']) - 1; $i++) {
            $exportColumn[0][] = "";
        }

        $exportColumn[0][] = " {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['G']})";
        for ($i = 0; $i < count($headerArray['parentInfo']) - 1; $i++) {
            $exportColumn[0][] = "";
        }

        for ($i = 0; $i < count($headerArray['parentInfo2']); $i++) {
            $exportColumn[0][] = "";
        }
        // ### Parent Info END ####

        // ### Sibling Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['HKUGAC']['IsTwinsApplied'];
        $exportColumn[0][] = "";
        // ### Sibling Info END ####

        // ### Other Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
        for ($i = 0; $i < count($headerArray['otherInfo']) - 1; $i++) {
            $exportColumn[0][] = "";
        }
        // ### Other Info END ####

        // ### Academic Performance Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['HKUGAC']['P5FirstResult'].' '.$kis_lang['Admission']['HKUGAC']['P6FinalResult'].' '.$kis_lang['Admission']['HKUGAC']['S1FinalResult'].' '.$kis_lang['Admission']['HKUGAC']['S2FinalResult'];
        for ($i = 0; $i < count($admission_cfg['Subjects']) - 1; $i++) {
            $exportColumn[0][] = "";
        }
        $exportColumn[0][] = $kis_lang['Admission']['HKUGAC']['P5FinalResult'].' '.$kis_lang['Admission']['HKUGAC']['S1FirstResult'].' '.$kis_lang['Admission']['HKUGAC']['S2FirstResult'].' '.$kis_lang['Admission']['HKUGAC']['S3FirstResult'];
        for ($i = 0; $i < count($admission_cfg['Subjects']) - 1; $i++) {
            $exportColumn[0][] = "";
        }
        // ### Academic Performance Info END ####

        // ### Offical Use START ####
        $exportColumn[0][] = $kis_lang['remarks'];
        for ($i = 0; $i < count($headerArray['officialUse']) - 1; $i++) {
            $exportColumn[0][] = "";
        }
        // ### Offical Use END ####

        // sub header
        $exportColumn[1] = array_merge(array(
            $headerArray[0],
            $headerArray[1],
            $headerArray[2]
        ), $headerArray['studentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo2'],
            $headerArray['siblingInfo'],
            $headerArray['otherInfo'],
            $headerArray['academicPerformanceInfo'],
            $headerArray['officialUse']
        );

        return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportDataCustom($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $type = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        $dataArray = array();
        if ($type == '') {
            $dataArray = $this->getExportData($schoolYearID, $classLevelID, $applicationID, $recordID);
        } elseif ($type == 'status') {
            // ######## Get data START ########
            $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
            $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
            // ######## Get data END ########

            $dataArray = array(
                $StudentInfo['applicationID'],
                Get_Lang_Selection($StudentInfo['student_name_b5'], $StudentInfo['student_name_en']),
                $kis_lang['Admission']['Status'][$status['status']]
            );
        } elseif ($type == 'interviewTime') {
            // ######## Get data START ########
            $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
            $allCustInfo = $this->getAllApplicationCustInfo($StudentInfo['applicationID']);
            // ######## Get data END ########

            // ######## Get last school name START ########
            $rs = $this->getApplicationPrevSchoolInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
            $StudentPrevSchoolInfo = array();
            foreach ($rs as $r) {
                $StudentPrevSchoolInfo["{$r['Year']}_{$r['SchoolOrder']}"] = $r;
            }
            ksort($StudentPrevSchoolInfo);
            $StudentPrevSchoolInfo = array_values(array_reverse($StudentPrevSchoolInfo));
            $lastSchoolName = $StudentPrevSchoolInfo[0]['NameOfSchool'];
            // ######## Get last school name END ########

            // ######## Get interview START ########
            $interviewInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
            $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '');
            $interviewSettings = BuildMultiKeyAssoc($rs, 'RecordID');

            $interview1 = $interviewSettings[$interviewInfo['InterviewSettingID']];
            $interview2 = $interviewSettings[$interviewInfo['InterviewSettingID2']];
            $interview3 = $interviewSettings[$interviewInfo['InterviewSettingID3']];
            // ######## Get interview END ########

            $dataArray = array(
                $StudentInfo['applicationID'],
                Get_Lang_Selection($StudentInfo['student_name_b5'], $StudentInfo['student_name_en']),
                $allCustInfo['StudentSTRN'][0]['Value'],
                $kis_lang['Admission']['genderType'][$StudentInfo['Gender']],
                $lastSchoolName,
                $interview1['Date'],
                substr($interview1['StartTime'], 0, 5),
                substr($interview1['EndTime'], 0, 5),
                $interview1['GroupName'],
                $interview2['Date'],
                substr($interview2['StartTime'], 0, 5),
                substr($interview2['EndTime'], 0, 5),
                $interview2['GroupName'],
                $interview3['Date'],
                substr($interview3['StartTime'], 0, 5),
                substr($interview3['EndTime'], 0, 5),
                $interview3['GroupName'],
            );
        }

        return $dataArray;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        // ######## Get data START ########
        // ### Get student Info START ####
        $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        // ### Get student Info END ####

        // ### Get parent Info START ####
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
        $parentInfoArr = array();
        foreach ($parentInfo as $parent) {
            $ParentInfo[$parent['type']] = $parent;
        }
        // ### Get parent Info END ####

        // ### Get academic background START ####
        $StudentPrevSchoolInfo = $this->getApplicationPrevSchoolInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
        foreach ($StudentPrevSchoolInfo as $index => $school) {
            if ($school['StartDate'] == '0000-00-00') {
                $StudentPrevSchoolInfo[$index]['StartDate'] = '';
            } else {
                $StudentPrevSchoolInfo[$index]['StartDate'] = date("m/Y", strtotime($school['StartDate']));
            }
            if ($school['EndDate'] == '0000-00-00') {
                $StudentPrevSchoolInfo[$index]['EndDate'] = '';
            } else {
                $StudentPrevSchoolInfo[$index]['EndDate'] = date("m/Y", strtotime($school['EndDate']));
            }
        }
        // ### Get academic background END ####

        // ### Get other info START ####
        $RelativeInfo = $this->getApplicationRelativesInfo($schoolYearID, $classLevelID, $applicationID, $recordID);

        $allCustInfo = $this->getAllApplicationCustInfo($StudentInfo['applicationID']);

        $allClassLevel = $this->getClassLevel();
        $classLevel = $this->getClassLevel();
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
        // ### Get other info END ####
        // ######## Get data END ########

        $dataArray = array();

        // ######## Export data START ########
        // ### Basic Info START ####
        $dataArray[] = substr($otherInfo['DateInput'], 0, -9);
        $dataArray[] = $StudentInfo['applicationID'];
        $dataArray[] = $classLevel[$otherInfo['classLevelID']];
        // ### Basic Info END ####

        // ### Student Info START ####
        $dataArray['studentInfo'][] = $allCustInfo['StudentSTRN'][0]['Value'];
        $dataArray['studentInfo'][] = $StudentInfo['ChineseName'];
        $dataArray['studentInfo'][] = $StudentInfo['EnglishName'];
        $dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$StudentInfo['Gender']];
        $dataArray['studentInfo'][] = $StudentInfo['Age'];
        $dataArray['studentInfo'][] = $StudentInfo['Nationality'];
        $dataArray['studentInfo'][] = $StudentInfo['BirthCertNo'];
        $dataArray['studentInfo'][] = ($StudentInfo['DOB'] == '0000-00-00') ? '' : $StudentInfo['DOB'];
        $dataArray['studentInfo'][] = $StudentInfo['PlaceOfBirth'];
        $dataArray['studentInfo'][] = $StudentInfo['AddressChi'];
        $dataArray['studentInfo'][] = $StudentInfo['Address'];
        $dataArray['studentInfo'][] = $StudentInfo['ContactAddressChi'];
        $dataArray['studentInfo'][] = $StudentInfo['ContactAddress'];
        $dataArray['studentInfo'][] = $StudentInfo['HomeTelNo'];
        $dataArray['studentInfo'][] = $StudentInfo['Fax'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[0]['Year'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[0]['Class'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[0]['NameOfSchool'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[1]['Year'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[1]['Class'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[1]['NameOfSchool'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[2]['Year'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[2]['Class'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[2]['NameOfSchool'];
        // ### Student Info END ####

        // ### Parent Info START ####
        $types = array(
            1 => 'F',
            2 => 'M',
            3 => 'G'
        );
        foreach ($types as $index => $type) {
            $dataArray['parentInfo'][] = $ParentInfo[$type]['EnglishName'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['ChineseName'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['JobTitle'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Company'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['OfficeTelNo'];
            $dataArray['parentInfo'][] = $allCustInfo["G{$index}NightTelephone"][0]['Value'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Mobile'];
        }
        $dataArray['parentInfo'][] = $allCustInfo['ContactPerson'][0]['Value'] == "Father" ? $Lang['Admission']['HKUGAC']['father'] : ($allCustInfo['ContactPerson'][0]['Value'] == "Mother" ? $Lang['Admission']['HKUGAC']['mother'] : ($allCustInfo['ContactPerson'][0]['Value'] == "Guardian" ? $Lang['Admission']['HKUGAC']['guardian'] : '－－'));
        $dataArray['parentInfo'][] = $ParentInfo['F']['Email'];
        // ### Parent Info END ####

        // ### Sibling Info START ####
        $dataArray['siblingInfo'][] = $allCustInfo['TwinsAppliedName'][0]['Value'];
        $dataArray['siblingInfo'][] = $allCustInfo['TwinsAppliedSTRN'][0]['Value'];
        // ### Sibling Info END ####

        // ### Other Info START ####
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience0_Year'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience0_Name'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience1_Year'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience1_Name'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience2_Year'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience2_Name'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience3_Year'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience3_Name'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience4_Year'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['StudentExperience4_Name'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['WhyChooseSchool'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['IsParentsHKUGAMember'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['ParentsHKUGAMemberName'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['IsParentsHKUGAEFMember'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['ParentsHKUGAEFMemberName'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['ParentsHKUGAEFMemberNumber'][0]['Value'];

        $dataArray['otherInfo'][] = $RelativeInfo[0]['Name'];
        $dataArray['otherInfo'][] = $RelativeInfo[0]['Year'];
        $dataArray['otherInfo'][] = $RelativeInfo[0]['ClassPosition'];
        $dataArray['otherInfo'][] = $RelativeInfo[0]['Relationship'];
        $dataArray['otherInfo'][] = $RelativeInfo[1]['Name'];
        $dataArray['otherInfo'][] = $RelativeInfo[1]['Year'];
        $dataArray['otherInfo'][] = $RelativeInfo[1]['ClassPosition'];
        $dataArray['otherInfo'][] = $RelativeInfo[1]['Relationship'];
        $dataArray['otherInfo'][] = $RelativeInfo[2]['Name'];
        $dataArray['otherInfo'][] = $RelativeInfo[2]['Year'];
        $dataArray['otherInfo'][] = $RelativeInfo[2]['ClassPosition'];
        $dataArray['otherInfo'][] = $RelativeInfo[2]['Relationship'];
        // ### Other Info END ####

        // ### Academic Performance Info START ####
        foreach ($admission_cfg['Subjects'] as $subjectCode) {
            $dataArray['academicPerformanceInfo'][] = $allCustInfo["F5_Subject_{$subjectCode}"][0]['Value'];
        }
        foreach ($admission_cfg['Subjects'] as $subjectCode) {
            $dataArray['academicPerformanceInfo'][] = $allCustInfo["F6_Subject_{$subjectCode}"][0]['Value'];
        }
        // ### Academic Performance Info END ####

        // ### Official Use START ####
        $dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
        $dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
        $dataArray['officialUse'][] = $status['FeeBankName'];
        $dataArray['officialUse'][] = $status['FeeChequeNo'];
        $dataArray['officialUse'][] = $status['receiptID'];
        $dataArray['officialUse'][] = $status['receiptdate'];
        $dataArray['officialUse'][] = $status['handler'];
//        $dataArray['officialUse'][] = $status['interviewdate'];
//		$dataArray['officialUse'][] = $status['interviewlocation'];
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, -3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, -3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, -3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, -3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, -3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, -3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
        $dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
        $dataArray['officialUse'][] = $status['remark'];
        // ### Official Use END ####

        $ExportArr = array_merge(array(
            $dataArray[0],
            $dataArray[1],
            $dataArray[2]
        ),
            $dataArray['studentInfo'],
            $dataArray['parentInfo'],
            $dataArray['siblingInfo'],
            $dataArray['otherInfo'],
            $dataArray['academicPerformanceInfo'],
            $dataArray['officialUse']
        );
        // ######## Export data END ########

        return $ExportArr;
    }

    function getExportDataForImportAccount($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $tabID = '')
    {
        global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;

        $studentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
        $custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);

        $dataArray = array();
        if ($tabID == 2) {
            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
            $studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
            $dataArray[0] = array();
            $dataArray[0][] = ''; //UserLogin
            $dataArray[0][] = ''; //Password
            $dataArray[0][] = ''; //UserEmail
            $dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
            $dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
            $dataArray[0][] = ''; //NickName
            $dataArray[0][] = $studentInfo['gender']; //Gender
            $dataArray[0][] = ''; //Mobile
            $dataArray[0][] = ''; //Fax
            $dataArray[0][] = ''; //Barcode
            $dataArray[0][] = ''; //Remarks
            $dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
            $dataArray[0][] = (is_numeric($studentInfo['homeaddress']) ? $Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']] : $studentInfo['homeaddress']); //Address
            if ((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment'])) {
                $dataArray[0][] = ''; //CardID
                if ($sys_custom['SupplementarySmartCard']) {
                    $dataArray[0][] = ''; //CardID2
                    $dataArray[0][] = ''; //CardID3
                }
            }
            if ($special_feature['ava_hkid'])
                $dataArray[0][] = $studentInfo['birthcertno']; //HKID
            if ($special_feature['ava_strn'])
                $dataArray[0][] = $custInfo['BD_Ref_Num'][0]['Value']; //STRN
            if ($plugin['medical'])
                $dataArray[0][] = ''; //StayOverNight
            $dataArray[0][] = $studentInfo['county']; //Nationality
            $dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
            $dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
        } else if ($tabID == 3) {
            $hasParent = false;
            $dataCount = array();
            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
            for ($i = 0; $i < count($parentInfo); $i++) {
                if ($parentInfo[$i]['type'] == 'G' && !$hasParent) {
                    $dataArray[0] = array();
                    $dataArray[0][] = ''; //UserLogin
                    $dataArray[0][] = ''; //Password
                    $dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
                    $dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
                    $dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
                    $dataArray[0][] = ''; //Gender
                    $dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
                    $dataArray[0][] = $custInfo['Parent_Fax'][0]['Value']; //Fax
                    $dataArray[0][] = ''; //Barcode
                    $dataArray[0][] = ''; //Remarks
                    if ($special_feature['ava_hkid'])
                        $dataArray[0][] = ''; //HKID
                    $dataArray[0][] = ''; //StudentLogin1
                    $dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
                    $dataArray[0][] = ''; //StudentLogin2
                    $dataArray[0][] = ''; //StudentEngName2
                    $dataArray[0][] = ''; //StudentLogin3
                    $dataArray[0][] = ''; //StudentEngName3
                    //$hasParent = true;
                    $dataCount[0] = ($parentInfo[$i]['email'] ? 1 : 0) + ($parentInfo[$i]['parent_name_en'] ? 1 : 0) + ($parentInfo[$i]['parent_name_b5'] ? 1 : 0) + ($parentInfo[$i]['mobile'] ? 1 : 0);
                }
            }
            if ($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]) {
                $tempDataArray = $dataArray[0];
            } else if ($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]) {
                $tempDataArray = $dataArray[1];
            } else if ($dataCount[2] > 0) {
                $tempDataArray = $dataArray[2];
            }
            $dataArray = array();
            $dataArray[0] = $tempDataArray;
        }
        $ExportArr = $dataArray;

        return $ExportArr;
    }

    function checkImportDataForImportAdmissionHeaderCust($data, $lang = '')
    {
        //$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
        $file_format = $this->getExportHeaderCustom($schoolYearID = '', $classLevelID = '', $type = 'status');
        $file_format = $file_format[0];
        $csv_header = $data[0];
        # check csv header
        $format_wrong = false;

        for ($i = 0; $i < sizeof($file_format); $i++) {
            if ($csv_header[$i] != $file_format[$i]) {
                $format_wrong = true;
                break;
            }
        }

        return $format_wrong;
    }

    function checkImportDataForImportAdmissionCust($data)
    {
        global $kis_lang, $admission_cfg;
        $resultArr = array();
        $i = 0;
        array_shift($data);

        #### Get current application id START ####
        $sql = "SELECT ApplicationID FROM ADMISSION_APPLICATION_STATUS WHERE SchoolYearID='{$this->schoolYearID}'";
        $currentApplicationIdArr = $this->returnVector($sql);
        #### Get current application id END ####

        #### Get valid status START ####
        $validStatusArr = array();
        foreach ($admission_cfg['Status'] as $key => $val) {
            $validStatusArr[] = $kis_lang['Admission']['Status'][$key];
        }
        #### Get valid status END ####

        foreach ($data as $aData) {
            //valid ApplicationID
            if ($aData[0]) {
                $resultArr[$i]['validApplicationID'] = in_array($aData[0], $currentApplicationIdArr);
            } else {
                $resultArr[$i]['validApplicationID'] = false;
            }

            //valid Status
            if ($aData[2]) {
                $resultArr[$i]['validStatus'] = in_array($aData[2], $validStatusArr);
            } else {
                $resultArr[$i]['validStatus'] = false;
            }

            $resultArr[$i]['validData'] = $aData[0];
            $resultArr[$i]['validDataName'] = $aData[1];
            $resultArr[$i]['validDataStatus'] = $aData[2];
            $i++;
        }
        $result = $resultArr;

        //for printing the error message
        $errCount = 0;
        $x = '';

        $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['applicationno'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['name'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['applicationstatus'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
        $i = 1;
        foreach ($result as $aResult) {
            //developing
            $hasError = false;
            $errorMag = '';
            if (!$aResult['validApplicationID']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
            } elseif (!$aResult['validStatus']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['HKUGAC']['msg']['importApplicationStatusIncorrect'];
            }

            //print the error msg to the client
            if ($hasError) {
                $errCount++;
                $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validData'] . '</td>
					<td>' . $aResult['validDataName'] . '</td>
					<td>' . $aResult['validDataStatus'] . '</td>
					<td><font color="red">';
                $x .= $errorMag;
                $x .= '</font></td></tr>';
            }

            $i++;
        }
        $x .= '</tbody></table>';
        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
    }

    function importDataForImportAdmission($data)
    {
        global $kis_lang, $admission_cfg;
        $resultArr = array();
        array_shift($data);

        #### Get applicationId START ####
        $sql = "SELECT ApplicationID, RecordID FROM ADMISSION_OTHERS_INFO";
        $rs = $this->returnResultSet($sql);

        $applicationIdMapping = BuildMultiKeyAssoc($rs, 'ApplicationID', 'RecordID', true);
        #### Get applicationId END ####

        #### Get status mapping START ####
        $validStatusMapping = array();
        foreach ($admission_cfg['Status'] as $key => $val) {
            $validStatusMapping[$kis_lang['Admission']['Status'][$key]] = $key;
        }
        #### Get status mapping END ####

        foreach ($data as $aData) {
            global $UserID;

            $applicationNumber = $aData[0];
            $status = $admission_cfg['Status'][$validStatusMapping[$aData[2]]];

            $result = $this->updateApplicationStatusByIds($applicationIdMapping[$applicationNumber], $status);
            $resultArr[] = $result;
            //debug_pr($aData);
        }
        return $resultArr;
    }

    function checkImportDataForImportInterviewApplicantHeader($data, $schoolYearID, $round = 1)
    {
        global $kis_lang;

        #### Get correct header START ####
        $_ = array_map('strip_tags', $kis_lang['Admission']['HKUGAC']['importInterviewField']);
        $correctHeader = array();
        foreach ($_ as $header) {
            $correctHeader[] = trim($header, '*');
        }
        #### Get correct header END ####

        array_shift($data);
        $csv_header = array_shift($data);
        $csv_header = array_map('trim', $csv_header);

        return (count(array_diff($correctHeader, $csv_header)) == 0);
    }

    function checkImportDataForImportInterviewApplicant($data, $schoolYearID, $round = 1)
    {
        global $kis_lang, $admission_cfg;
        $resultArr = array();
        $roundShift = ($round - 1) * 4;
        $i = 0;

        array_shift($data);
        array_shift($data);

        $sql = "SELECT
	        ApplicationID,
	        ApplyLevel
        FROM
	        ADMISSION_OTHERS_INFO O
        WHERE
	        ApplyYear = '{$schoolYearID}'";
        $rs = $this->returnResultSet($sql);
        $applicationLevelMapping = BuildMultiKeyAssoc($rs, array('ApplicationID'), array('ApplyLevel'), $SingleValue = 1);
        $validApplicationIdArr = array_keys($applicationLevelMapping);


        // call libkis_admission.php => getInterviewSettingAry()
        $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '', $round);

        $interviewSettings = array();
        foreach ($rs as $r) {
            $startTime = substr($r['StartTime'], 0, 5);
            $endTime = substr($r['EndTime'], 0, 5);
            $interviewSettings[$r['ClassLevelID']][$r['Date']][$startTime][$endTime][] = $r['GroupName'];
        }
        $applicantIds = array();
        foreach ($data as $aData) {
            $aData = array_map('trim', $aData);

            $isEmptyRecord = false;
            if ($aData[5 + $roundShift] . $aData[6 + $roundShift] . $aData[7 + $roundShift] . $aData[8 + $roundShift] == '') {
                $isEmptyRecord = true;
            }

            $aData[5 + $roundShift] = getDefaultDateFormat($aData[5 + $roundShift]);
            $aData[6 + $roundShift] = substr("0{$aData[6+$roundShift]}", -5);
            $aData[7 + $roundShift] = substr("0{$aData[7+$roundShift]}", -5);

            // check applicant
            $resultArr[$i]['validInterviewApplicant'] = true;
            $resultArr[$i]['validInterviewApplicantDuplicate'] = true;
            if (($aData[0] == '') || (!in_array($aData[0], $validApplicationIdArr))) {
                $resultArr[$i]['validInterviewApplicant'] = false;
            } elseif (in_array($aData[0], $applicantIds)) {
                $resultArr[$i]['validInterviewApplicantDuplicate'] = false;
            }
            $applicantIds[] = $aData[0];

            // check form
            $resultArr[$i]['validClassLevel'] = false;
            foreach ($this->classLevelAry as $_classLevelId => $_classLevelName) {
                if ($applicationLevelMapping[$aData[0]] == $_classLevelId) {
                    $resultArr[$i]['validClassLevel'] = true;
                    break;
                }
            }

            if ($isEmptyRecord) {
                $resultArr[$i]['isEmpty'] = true;
                $resultArr[$i]['validInterviewDate'] = true;
                $resultArr[$i]['validInterviewTimeStart'] = true;
                $resultArr[$i]['validInterviewTimeEnd'] = true;
                $resultArr[$i]['validInterviewRoomSession'] = true;
            } else {
                // check date
                $resultArr[$i]['validInterviewDate'] = true;
                if (
                    ($aData[5 + $roundShift] == '') ||
                    !isset($interviewSettings[$r['ClassLevelID']][$aData[5 + $roundShift]])
                ) {
                    $resultArr[$i]['validInterviewDate'] = false;
                }

                // check time start
                $resultArr[$i]['validInterviewTimeStart'] = true;
                if (
                    ($aData[6 + $roundShift] == '') ||
                    !isset($interviewSettings[$r['ClassLevelID']][$aData[5 + $roundShift]][$aData[6 + $roundShift]])
                ) {
                    $resultArr[$i]['validInterviewTimeStart'] = false;
                }

                // check time end
                $resultArr[$i]['validInterviewTimeEnd'] = true;
                if (
                    ($aData[7 + $roundShift] == '') ||
                    !isset($interviewSettings[$r['ClassLevelID']][$aData[5 + $roundShift]][$aData[6 + $roundShift]][$aData[7 + $roundShift]])
                ) {
                    $resultArr[$i]['validInterviewTimeEnd'] = false;
                }

                // check room/session
                $resultArr[$i]['validInterviewRoomSession'] = true;
                if (
                    ($aData[8 + $roundShift] == '') ||
                    (
                        isset($interviewSettings[$r['ClassLevelID']][$aData[5 + $roundShift]][$aData[6 + $roundShift]][$aData[7 + $roundShift]]) &&
                        !in_array($aData[8 + $roundShift], $interviewSettings[$r['ClassLevelID']][$aData[5 + $roundShift]][$aData[6 + $roundShift]][$aData[7 + $roundShift]])
                    )
                ) {
                    $resultArr[$i]['validInterviewRoomSession'] = false;
                }
            }

            if ($resultArr[$i]['isEmpty']) {
                $resultArr[$i]['validDataApplicant'] = $aData[0];
                $resultArr[$i]['validDataInterviewDate'] = '';
            } else {
                $resultArr[$i]['validDataApplicant'] = $aData[0];
                $resultArr[$i]['validDataInterviewDate'] = $aData[5 + $roundShift] . ' (' . $aData[6 + $roundShift] . ' ~ ' . $aData[7 + $roundShift] . ')&nbsp;&nbsp;';
                $resultArr[$i]['validDataInterviewDate'] .= ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room') ? "{$kis_lang['interviewroom']}: {$aData[8+$roundShift]}" : "{$kis_lang['sessiongroup']}: {$aData[8+$roundShift]}";
            }
            $i++;
        }
        // return $resultArr;
        $result = $resultArr;

        // for printing the error message
        $errCount = 0;

        $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['applicationno'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['interviewdate'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
        $i = 1;
        foreach ($result as $aResult) {
            // developing
            $hasError = false;
            $errorMag = '';
            if (!$aResult['validInterviewApplicant']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importInvalidApplicant'];
            } elseif (!$aResult['validInterviewApplicantDuplicate']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importDuplicateApplicant'];
            } elseif (!$aResult['validClassLevel']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importClassLevelNotFound'];
            } elseif (!$aResult['validInterviewDate']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importInvalidInterviewDate'];
            } elseif (!$aResult['validInterviewTimeStart']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importInvalidInterviewTimeStart'];
            } elseif (!$aResult['validInterviewTimeEnd']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importInvalidInterviewTimeEnd'];
            } elseif (!$aResult['validInterviewRoomSession']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importInvalidInterviewSettingSession'];
            }

            // print the error msg to the client
            if ($hasError) {
                $errCount++;
                $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validDataApplicant'] . '</td>
					<td>' . $aResult['validDataInterviewDate'] . '</td>
					<td><font color="red">';
                $x .= $errorMag;
                $x .= '</font></td></tr>';
            }
            $i++;
        }
        $x .= '</tbody></table>';
        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
    }

    function importDataForImportInterviewApplicant($schoolYearID, $data, $round = 1)
    {
        $resultArr = array();
        $roundShift = ($round - 1) * 4;
        array_shift($data);
        array_shift($data);

        // ### Get all applicationId START ####
        $allApplicationId = array();
        foreach ($data as $d) {
            $allApplicationId[] = trim($d[0]);
        }
        // ### Get all applicationId END ####

        // ### Remove all inteview record for that school year and round START ####
        $allApplicationIdSql = implode("','", $allApplicationId);
        $field = 'InterviewSettingID' . ($round > 1 ? $round : '');
        $sql = "UPDATE
	        ADMISSION_OTHERS_INFO
        SET
	        {$field} = '0'
        WHERE
            ApplyYear='{$schoolYearID}'
        AND
            ApplicationID IN ('{$allApplicationIdSql}')
        ";
        $result = $this->db_db_query($sql);
        $resultArr[] = $result;
        // ### Remove all inteview record for that school year and round END ####

        // ### Get all class level START ####
        $sql = "SELECT
            ApplicationID,
            ApplyLevel
        FROM
            ADMISSION_OTHERS_INFO O
        WHERE
            ApplyYear = '{$schoolYearID}'";
        $rs = $this->returnResultSet($sql);
        $applicationLevelMapping = BuildMultiKeyAssoc($rs, array(
            'ApplicationID'
        ), array(
            'ApplyLevel'
        ), $SingleValue = 1);
        // ### Get all class level START ####

        // ### Get all interview settings START ####
        // call libkis_admission.php => getInterviewSettingAry()
        $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '', $round);
        $interviewSettings = array();
        $interviewQuota = array();
        foreach ($rs as $r) {
            $interviewSettings[strtoupper($r['ClassLevelID'])][$r['Date']][$r['StartTime']][$r['EndTime']][$r['GroupName']][] = $r;

            // $remainsQuota = $r['Quota'] - $r['Applied'];
            $remainsQuota = $r['Quota']; // Remove all data before import, no need to count old data
            $interviewQuota[$r['RecordID']] = $remainsQuota;
        }
        // ### Get all interview settings END ####

        // ### Update inteview record START ####
        foreach ($data as $aData) {
            $aData = array_map('trim', $aData);

            $isEmptyRecord = false;
            if ($aData[5 + $roundShift] . $aData[6 + $roundShift] . $aData[7 + $roundShift] . $aData[8 + $roundShift] == '') {
                $isEmptyRecord = true;
            }

            $aData[5 + $roundShift] = getDefaultDateFormat($aData[5 + $roundShift]);

            $field = 'InterviewSettingID' . ($round > 1 ? $round : '');
            $classLevelId = strtoupper($applicationLevelMapping[$aData[0]]);
            $date = $aData[5 + $roundShift];
            $startTime = date('H:i:00', strtotime($aData[6 + $roundShift]));
            $endTime = date('H:i:59', strtotime($aData[7 + $roundShift]));
            $roomSession = $aData[8 + $roundShift];

            $settings = $interviewSettings[$classLevelId][$date][$startTime][$endTime][$roomSession];
            $settingId = 0;
            if (!$isEmptyRecord) {
                for ($i = 0, $iMax = count($settings); $i < $iMax; $i++) {
                    $_settingId = $settings[$i]['RecordID'];
                    if ($interviewQuota[$_settingId] > 0 || $i == $iMax - 1) {
                        $settingId = $_settingId;
                        $interviewQuota[$_settingId]--;
                        break;
                    }
                }
            }

            $sql = "UPDATE
                ADMISSION_OTHERS_INFO
            SET
                {$field} = '{$settingId}'
            WHERE
                ApplyYear = '{$schoolYearID}'
            AND
                ApplicationID = '{$aData[0]}'
            ";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;
        }
        // ### Update inteview record END ####

        return $resultArr;
    }

    function getApplicantEmail($applicationId)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;

        $sql = "SELECT
					a.Email as Email
				FROM ADMISSION_PG_INFO as a
				WHERE a.ApplicationID = '" . trim($applicationId) . "'
				AND
					a.PG_TYPE = 'F'";
        $records = current($this->returnArray($sql));

        return $records['Email'];
    }

    /*
     * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
     */
    public function sendMailToNewApplicant($applicationId, $subject, $message, $isEdit=false)
    {
        global $intranet_root, $kis_lang, $admission_cfg;

        if(!$this->AdmissionFormSendEmail){
            return true;
        }

        include_once($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new libwebmail();

        $from = $libwebmail->GetWebmasterMailAddress();
        $inputby = $_SESSION['UserID'];
        $result = array();

        $bccArr = array();
        if($isEdit && $this->IsUpdateExtraAttachmentPeriod()) {
            $bccArr = $admission_cfg['extra_attachment_period_edit_email'];
        }

        $sql = "SELECT 
					a.Email as Email
				FROM ADMISSION_PG_INFO as a 
				WHERE a.ApplicationID = '" . trim($applicationId) . "'
				AND a.PG_TYPE='F'";
        $records = current($this->returnArray($sql));
        //debug_pr($applicationId);
        $to_email = trim($records['Email']);
        if ($subject == '') {
            $email_subject = "港大同學會書院入學申請通知 HKUGA College Admission Notification";
        } else {
            $email_subject = $subject;
        }
        $email_message = $message;
        $sent_ok = true;
        if ($to_email != '' && intranet_validateEmail($to_email)) {
            $sent_ok = $libwebmail->sendMail($email_subject, $email_message, $from, array($to_email), array(), $bccArr, "", $IsImportant = "", $mail_return_path = get_webmaster(), $reply_address = "", $isMulti = null, $nl2br = 0);
        } else {
            $sent_ok = false;
        }

        return $sent_ok;
    }

    public function sendMailToNewApplicantWithReceiver($applicationId, $subject, $message, $to_email)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
        include_once($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new libwebmail();

        $from = $libwebmail->GetWebmasterMailAddress();

        if ($subject == '') {
            $email_subject = "匯基書院(東九龍)入學申請通知 United Christian College (Kowloon East) Admission Notification";
        } else {
            $email_subject = $subject;
        }
        $email_message = $message;
        $sent_ok = true;
        $to_email = trim($to_email);
        if ($to_email != '' && intranet_validateEmail($to_email)) {
            $sent_ok = $libwebmail->sendMail($email_subject, $email_message, $from, array($to_email), array(), array(), "", $IsImportant = "", $mail_return_path = get_webmaster(), $reply_address = "", $isMulti = null, $nl2br = 0);
        } else {
            $sent_ok = false;
        }

        return $sent_ok;
    }

    function checkImportDataForImportInterview($data)
    {
        $resultArr = array();
        $i = 0;
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);
            //check date
            if ($aData[4] == '' && $aData[5] == '') {
                $validDate = true;
            } else if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4])) {
                list($year, $month, $day) = explode('-', $aData[4]);
                $validDate = checkdate($month, $day, $year);
            } else {
                $validDate = false;
            }

            //check time
            if ($aData[4] == '' && $aData[5] == '') {
                $validTime = true;
            } else if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5])) {
                $validTime = true;
            } else {
                $validTime = false;
            }
            $sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '" . trim($aData[0]) . "' AND trim(s.birthCertNo) = '" . trim($aData[3]) . "'
	    	";
            $result = $this->returnVector($sql);
            if ($result[0] == 0) {
                $resultArr[$i]['validData'] = $aData[0];
            } else
                $resultArr[$i]['validData'] = false;
            $resultArr[$i]['validDate'] = $validDate;
            $resultArr[$i]['validTime'] = $validTime;
            if (!$validDate || !$validTime)
                $resultArr[$i]['validData'] = $aData[0];
            $i++;
        }
        return $resultArr;
    }

    function importDataForImportInterview($data)
    {
        $resultArr = array();
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);

            $sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '" . $aData[4] . " " . $aData[5] . "',
				InterviewLocation = '" . $aData[6] . "',
				DateModified = NOW(),
		   		ModifiedBy = '" . $this->uid . "'
   				WHERE ApplicationID = '" . $aData[0] . "'
	    	";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;

        }
        return $resultArr;
    }

    function getExportDataForImportInterview($recordID, $schoolYearID = '', $selectStatus = '', $classLevelID = '')
    {
        global $admission_cfg;
        $cond = !empty($schoolYearID) ? " AND a.SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= !empty($selectStatus) ? " AND a.Status='" . $selectStatus . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				" . $cond . "
    	";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1)
    {
        $status_cond = '';
        if (sizeof($selectStatusArr) > 0) {
            $status_cond .= " AND st.status in ('" . implode("','", $selectStatusArr) . "') ";
        }

        $round_cond = " AND Round = '" . $round . "' ";

        $sql = 'Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "' . $selectSchoolYearID . '" ' . $status_cond . ' order by y.YearName desc';

        $result = $this->returnArray($sql);

        $allApplicant = array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $allApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
        }

//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);

        $result = array();

        $TwinsAssignedApplicant = array();

//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
        $sql = "SELECT RecordID, Quota FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '" . $selectSchoolYearID . "' $round_cond AND GroupName IS NOT NULL ORDER BY StartTime, GroupName";
        $interviewRecordIDArr = $this->returnArray($sql);

        for ($j = 0; $j < sizeof($interviewRecordIDArr); $j++) {

            if (!$allApplicant) {
                break;
            }

            $previousClassLevel = $allApplicant[0]['ClassLevel'];
            $sql = "UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '" . $previousClassLevel . "' WHERE RecordID = '" . $interviewRecordIDArr[$j]['RecordID'] . "' ";
            $result[] = $this->db_db_query($sql);

            for ($k = 0; $k < $interviewRecordIDArr[$j]['Quota']; $k++) {
                $sql = "UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID" . ($round > 1 ? $round : '') . " = '" . $interviewRecordIDArr[$j]['RecordID'] . "' Where ApplicationID = '" . $allApplicant[0]['ApplicationID'] . "' ";
                $result[] = $this->db_db_query($sql);
                array_shift($allApplicant);
                if ($previousClassLevel != $allApplicant[0]['ClassLevel']) {
                    break;
                }
            }

        }
//		}

        //handling twins swraping [start]
        $sql = 'Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID' . ($round > 1 ? $round : '') . ' as InterviewSettingID From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				where o.ApplyYear = "' . $selectSchoolYearID . '" AND s.IsTwinsApplied = "Y" order by y.YearName desc';

        $twinsResult = $this->returnArray($sql);

        $twinsArray = array();
        $assignedTwins = array();

        for ($i = 0; $i < sizeof($twinsResult); $i++) {
            for ($j = 0; $j < sizeof($twinsResult); $j++) {
                if ($twinsResult[$i]['TwinsApplicationID'] == $twinsResult[$j]['BirthCertNo'] && $twinsResult[$i]['InterviewSettingID'] == $twinsResult[$j]['InterviewSettingID'] && !in_array($twinsResult[$j]['ApplicationID'], $assignedTwins)) {

                    $sql = 'Select RecordID, ClassLevelID, Date, GroupName From ADMISSION_INTERVIEW_SETTING where RecordID = "' . $twinsResult[$i]['InterviewSettingID'] . '" ';
                    $originalSession = current($this->returnArray($sql));

                    $sql = 'Select RecordID From ADMISSION_INTERVIEW_SETTING where ClassLevelID = "' . $originalSession['ClassLevelID'] . '" AND Date = "' . $originalSession['Date'] . '" ' . $round_cond . ' AND GroupName <> "' . $originalSession['GroupName'] . '" ';
                    $newSession = current($this->returnArray($sql));

                    if ($newSession) {
                        $sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID' . ($round > 1 ? $round : '') . ' = "' . $newSession['RecordID'] . '" AND s.IsTwinsApplied <> "Y" ';
                        $swapApplicant = current($this->returnArray($sql));
                        if ($swapApplicant) {
                            $sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID' . ($round > 1 ? $round : '') . ' = "' . $originalSession['RecordID'] . '" where ApplicationID = "' . $swapApplicant['ApplicationID'] . '" ';
                            $updateResult = $this->db_db_query($sql);
                            $sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID' . ($round > 1 ? $round : '') . ' = "' . $newSession['RecordID'] . '" where ApplicationID = "' . $twinsResult[$j]['ApplicationID'] . '" ';
                            $updateResult = $this->db_db_query($sql);
                            $assignedTwins[] = $twinsResult[$j]['ApplicationID'];
                        }
                    }
                }
            }
        }

        //handling twins swraping [end]

        return !in_array(false, $result);
    }

    function hasBirthCertNumber($birthCertNo, $applyLevel)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '" . $this->getNextSchoolYearID() . "'";
        return current($this->returnVector($sql));
    }

    function hasToken($token)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '" . $token . "' ";
        return current($this->returnVector($sql));
    }

    function getApplicationStudentInfoCust($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = !empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID applicationID,					
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName 
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'	
			" . $cond . " 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

        return $this->returnArray($sql);
    }

    function getApplicationRelativesInfoCust($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = !empty($applicationID) ? " AND r.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year OthersRelativeStudiedYear,
				r.Name OthersRelativeStudiedName,		    					
     			r.ClassPosition OthersRelativeClassPosition,
				r.Relationship OthersRelativeRelationship 
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'	
			" . $cond . " 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

        return $this->returnArray($sql);
    }

    function uploadAttachment($type, $file, $destination, $randomFileName = "")
    {//The $randomFileName does not contain file extension
        global $admission_cfg, $intranet_root, $libkis;
        include_once($intranet_root . "/includes/libimage.php");
        $uploadSuccess = false;
        $ext = strtolower(getFileExtention($file['name']));

// 		if($type == "personal_photo"){
// 			return true;
// 		}
        if (!empty($file['image_data'])) {
            list($type, $data) = explode(';', $file['image_data']);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);

            $tmpfname = tempnam("/tmp", "admission_file_");
            $handle = fopen($tmpfname, "w");
            fwrite($handle, $data);
            fclose($handle);

            $file['tmp_name'] = $tmpfname;
        }
        if (!empty($file['tmp_name'])) {
            require_once($intranet_root . "/includes/admission/class.upload.php");
            $handle = new Upload($file['tmp_name']);
            if ($handle->uploaded) {
                $handle->Process($destination);
                if ($handle->processed) {
                    $uploadSuccess = true;
                    if ($type == "personal_photo") {
                        $image_obj = new SimpleImage();
                        $image_obj->load($handle->file_dst_pathname);
                        if ($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
                            $image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
                        else
                            $image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
                        //rename the file and then save

                        $image_obj->save($destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext, $image_obj->image_type);
                        unlink($handle->file_dst_pathname);
                    } else {
                        rename($handle->file_dst_pathname, $destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext);
                    }
                    //$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
                } else {
                    // one error occured
                    $uploadSuccess = false;
                }
                // we delete the temporary files
                $handle->Clean();
            }
        } else {
            if ($this->isInternalUse($_REQUEST['token'])) {
                return true;
            }
        }

        if (file_exists($file['tmp_name'])) {
            @unlink($file['tmp_name']);
        }
        return $uploadSuccess;
        //}
        //return true;
    }

    function preReplaceEmailVariables($text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1 = '', $interviewDateTime2 = '', $interviewDateTime3 = '', $briefingdate = '', $briefinginfo = '', $langSpokenAtHome = '', $printEmailLink = '')
    {
        global $PATH_WRT_ROOT, $setting_path_ip_rel;
        $replaced_text = $text;

        $interviewInfo = current($this->getInterviewListAry($interviewDateTime1, '', '', '', '', '', '', 1));
        $interviewInfo2 = current($this->getInterviewListAry($interviewDateTime2, '', '', '', '', '', '', 2));
        $interviewInfo3 = current($this->getInterviewListAry($interviewDateTime3, '', '', '', '', '', '', 3));

        $replaced_text = str_replace("[=FirstRoundInterviewSession=]", $interviewInfo['GroupName'], $replaced_text);
        $replaced_text = str_replace("[=SecondRoundInterviewSession=]", $interviewInfo2['GroupName'], $replaced_text);
        $replaced_text = str_replace("[=ThirdRoundInterviewSession=]", $interviewInfo3['GroupName'], $replaced_text);

        $replaced_text = str_replace("[=FirstRoundInterviewDate=]", $interviewInfo['Date'], $replaced_text);
        $replaced_text = str_replace("[=SecondRoundInterviewDate=]", $interviewInfo2['Date'], $replaced_text);
        $replaced_text = str_replace("[=ThirdRoundInterviewDate=]", $interviewInfo3['Date'], $replaced_text);

        $replaced_text = str_replace("[=FirstRoundInterviewStartTime=]", substr($interviewInfo['StartTime'], 0, -3), $replaced_text);
        $replaced_text = str_replace("[=SecondRoundInterviewStartTime=]", substr($interviewInfo2['StartTime'], 0, -3), $replaced_text);
        $replaced_text = str_replace("[=ThirdRoundInterviewStartTime=]", substr($interviewInfo3['StartTime'], 0, -3), $replaced_text);

        $replaced_text = str_replace("[=FirstRoundInterviewEndTime=]", substr($interviewInfo['EndTime'], 0, -3), $replaced_text);
        $replaced_text = str_replace("[=SecondRoundInterviewEndTime=]", substr($interviewInfo2['EndTime'], 0, -3), $replaced_text);
        $replaced_text = str_replace("[=ThirdRoundInterviewEndTime=]", substr($interviewInfo3['EndTime'], 0, -3), $replaced_text);

        return $replaced_text;
    }
} // End Class
