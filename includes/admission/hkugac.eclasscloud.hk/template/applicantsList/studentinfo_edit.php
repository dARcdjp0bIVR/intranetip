<?php
global $libkis_admission;

$star = $mustfillinsymbol;
$StudentInfo = $applicationInfo;
$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,'',$applicationInfo['applicationID'],'');
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

$applySchoolId = $allCustInfo['ApplySchool'][0]['Value'];
$applySchoolType = $admission_cfg['SchoolType'][$applySchoolId];
?>
<style>
select:disabled {
	color: #ccc;
}

.col2div {
	display: inline-block;
	width: 48%;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%;" />
		<col style="width: 40%;" />
		<col style="width: 30%;" />
	</colgroup>
	<tbody>


		<!-- ######## Basic Information START ######## -->
        <tr>
            <td class="field_title">
                <?=$star?><?=$kis_lang['Admission']['HKUGAC']['strn']?>
            </td>
            <td>
                <input name="StudentSTRN" type="text"
                       id="StudentSTRN" class="textboxtext"
                       value="<?=$allCustInfo['StudentSTRN'][0]['Value']?>" />
            </td>
        </tr>
		<tr>
			<td width="30%" class="field_title">
				<?=$star?><?= $kis_lang['Admission']['HKUGAPS']['chinesename']?>
			</td>
			<td width="40%">
				<input name="StudentChineseName" type="text"
						id="StudentChineseName" class="textboxtext"
						value="<?=$StudentInfo['ChineseName']?>" />
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin: 0px;">
					<img
						src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>" />
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id="btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align: center;">
						<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?=$kis_lang['Admission']['HKUGAC']['englishname']?>
			</td>
			<td>
				<input name="StudentEnglishName" type="text"
					id="StudentEnglishName" class="textboxtext"
					value="<?=$StudentInfo['EnglishName']?>" />
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['gender']?>
			</td>
			<td>
    			<?=$libinterface->Get_Radio_Button('StudentGender_M', 'StudentGender', 'M', ($StudentInfo['Gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
    			<?=$libinterface->Get_Radio_Button('StudentGender_F', 'StudentGender', 'F', ($StudentInfo['Gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
    		</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['Age']?>
			</td>
			<td><input name="StudentAge" id="StudentAge"
				value="<?=$StudentInfo['Age']?>" />
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['nationality']?>
			</td>
			<td><input id="StudentNationality" name="StudentNationality"
				class="textboxtext" value="<?=$StudentInfo['Nationality'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?=$kis_lang['Admission']['HKUGAC']['birthcertno']?>
			</td>
			<td nowrap="" colspan="2"><input name="StudentBirthCertNo"
				id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30"
				style="width: 150px" value="<?=$StudentInfo['BirthCertNo'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['dateofbirth']?>
			</td>
			<td><input name="StudentDateOfBirth" id="StudentDateOfBirth"
				value="<?=$StudentInfo['dateofbirth']?>">&nbsp; <span
				class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['placeofbirth']?>
			</td>
			<td><input id="StudentPlaceOfBirth"
				name="StudentPlaceOfBirth" class="textboxtext"
				value="<?=$StudentInfo['PlaceOfBirth'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['HKUGAPS']['address']?>(<?=$kis_lang['Admission']['HKUGAC']['Chinese']?>)
			</td>
			<td><input id="StudentAddressChi"
				name="StudentAddressChi" class="textboxtext"
				value="<?=$StudentInfo['AddressChi'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['HKUGAPS']['address']?>(<?=$kis_lang['Admission']['HKUGAC']['English']?>)
			</td>
			<td><input id="StudentAddress"
				name="StudentAddress" class="textboxtext"
				value="<?=$StudentInfo['Address'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['HKUGAC']['contactaddress']?>(<?=$kis_lang['Admission']['HKUGAC']['Chinese']?>)
			</td>
			<td><input id="StudentContactAddressChi"
				name="StudentContactAddressChi" class="textboxtext"
				value="<?=$StudentInfo['ContactAddressChi'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['HKUGAC']['contactaddress']?>(<?=$kis_lang['Admission']['HKUGAC']['English']?>)
			</td>
			<td><input id="StudentContactAddress"
				name="StudentContactAddress" class="textboxtext"
				value="<?=$StudentInfo['ContactAddress'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['HKUGAC']['HomeTelephone']?>
			</td>
			<td><input id="StudentHomePhone"
				name="StudentHomePhone" class="textboxtext"
				value="<?=$StudentInfo['HomeTelNo'] ?>" /></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['HKUGAC']['Fax']?>
			</td>
			<td><input id="StudentFax"
				name="StudentFax" class="textboxtext"
				value="<?=$StudentInfo['Fax'] ?>" /></td>
		</tr>
		<!-- ######## Basic Information END ######## -->
		</tbody>
		</table>
		
		<table class="form_table">
		    <colgroup>
		        <col style="width:30%">
		        <col style="width:23%">
		        <col style="width:23%">
		        <col style="width:24%">
		    </colgroup>
		    <tr>
				<td>&nbsp;</td>
				<td class="form_guardian_head"><?=$kis_lang['Admission']['year']?> (YYYY-YYYY)</td>
				<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['Class'] ?></td>
				<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['School'] ?></td>
			</tr>
			<?php for($i=0;$i<1;$i++){ ?>
			<tr>
				<td class="field_title">
					<?=$i==0?$star:''?>
					<?=$kis_lang['Admission']['HKUGAC']['CurrentSchoolAttend'] ?>
				</td>
				<td>
					<input id="StudentSchool<?=$i?>_Year" name="StudentSchool<?=$i?>_Year"
						class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['Year'] ?>" />
				</td>
				<td>
					<input id="StudentSchool<?=$i?>_Class" name="StudentSchool<?=$i?>_Class"
						class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['Class'] ?>" />
				</td>
				<td>
					<input id="StudentSchool<?=$i?>_Name" name="StudentSchool<?=$i?>_Name"
						class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['NameOfSchool'] ?>" />
				</td>
			</tr>
			<? }?>
			<?php for($i=1;$i<$libkis_admission::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){ ?>
			<tr>
				<td class="field_title">
					<?=$i==0?$star:''?>
					<?=$kis_lang['Admission']['HKUGAC']['LastSchoolAttend'] ?> (<?=$i?>)
				</td>
				<td>
					<input id="StudentSchool<?=$i?>_Year" name="StudentSchool<?=$i?>_Year"
						class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['Year'] ?>" />
				</td>
				<td>
					<input id="StudentSchool<?=$i?>_Class" name="StudentSchool<?=$i?>_Class"
						class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['Class'] ?>" />
				</td>
				<td>
					<input id="StudentSchool<?=$i?>_Name" name="StudentSchool<?=$i?>_Name"
						class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['NameOfSchool'] ?>" />
				</td>
			</tr>
			<? }?>
			</tr>
		</table>
		<table class="form_table">
			<colgroup>
				<col style="width: 30%;" />
				<col style="width: 40%;" />
				<col style="width: 30%;" />
			</colgroup>
			<tbody>
		<!-- ######## Document START ######## --> 
    	<?php
    for ($i = 0; $i < sizeof($attachmentSettings); $i ++) {
        $isExtra = $attachmentSettings[$i]['IsExtra'];
        
        // #### Check ClassLevel START ####
        if ($attachmentSettings[$i]['ClassLevelStr']) {
            $classLevelArr = explode(',', $attachmentSettings[$i]['ClassLevelStr']);
            
            if (! in_array($StudentInfo['classLevelID'], $classLevelArr)) {
                continue;
            }
        }
        // #### Check ClassLevel END ####
        
        // #### Get attachment name START #### //
        $attachment_name = $attachmentSettings[$i]['AttachmentName'];
        $attachmentNameDisplay = $attachment_name;
        
        // ## Multi-lang START ## //
        if ($admission_cfg['MultipleLang']) {
            foreach ($admission_cfg['Lang'] as $index => $lang) {
                if ($lang == $intranet_session_language) {
                    $attachmentNameDisplay = $attachmentSettings[$i]["AttachmentName{$index}"];
                }
            }
        }
        // ## Multi-lang END ## //
        
        // ## Cust-lang START ## //
        $custAttachmentName = '';
        $attachmentNameDisplay = ($custAttachmentName) ? $custAttachmentName : $attachmentNameDisplay;
        // ## Cust-lang END ## //
        // #### Get attachment name END #### //
        
        $_filePath = $attachmentList[$attachment_name]['link'];
        if ($_filePath) {
            $_spanDisplay = '';
            $_buttonDisplay = ' style="display:none;"';
        } else {
            $_filePath = '';
            $_spanDisplay = ' style="display:none;"';
            $_buttonDisplay = '';
        }

        if($isExtra) {
            $_filePath = '/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($attachment_name);
        }
        
        $_attachment = "<a href ='" . $_filePath . "' class='file_attachment' target='_blank'>" . $kis_lang['view'] . "</a>";
        $_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="' . $kis_lang['delete'] . '"></a></div>';
        ?>
        
    		<tr>
			<td class="field_title">
    				<?=$attachmentNameDisplay?>
    			</td>
			<td id="<?=$attachment_name?>"><span class="view_attachment"
				<?=$_spanDisplay?>><?=$_attachment?></span> <input type="button"
				class="attachment_upload_btn formsmallbutton"
				value="<?=$kis_lang['Upload']?>" id="uploader-<?=$attachment_name?>"
				<?=$_buttonDisplay?> /></td>
		</tr>	
    	<?php
    }
    ?>
		<!-- ######## Document END ######## -->
	</tbody>
</table>

<script>
//// UI Releated START ////
$('#StudentBirthCertType').change(function(){
	$('.docUpload').hide();
	$('.docUpload_' + $(this).val()).show();
}).change();
////UI Releated END ////

var dOBRange;
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});

function checkIsMMYY(str){
	return str.match(/^(0[123456789]|10|11|12)\/\d{2}$/);
}

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	return check_student_form();
}


function check_student_form(){
	return true;
	//For debugging only
	if(
		$('#StudentSchool0_DatesAttendedFrom').val().trim()!='' &&
		!checkIsMMYY($('#StudentSchool0_DatesAttendedFrom').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#StudentSchool0_DatesAttendedFrom').focus();
		return false;
	}
	if(
		$('#StudentSchool0_DatesAttendedTo').val().trim()!='' &&
		!checkIsMMYY($('#StudentSchool0_DatesAttendedTo').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#StudentSchool0_DatesAttendedTo').focus();
		return false;
	}
	if(
		$('#StudentSchool1_DatesAttendedFrom').val().trim()!='' &&
		!checkIsMMYY($('#StudentSchool1_DatesAttendedFrom').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#StudentSchool1_DatesAttendedFrom').focus();
		return false;
	}
	if(
		$('#StudentSchool1_DatesAttendedTo').val().trim()!='' &&
		!checkIsMMYY($('#StudentSchool1_DatesAttendedTo').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#StudentSchool1_DatesAttendedTo').focus();
		return false;
	}
 	return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;

	var form1 = applicant_form;
	dOBRange = dOBRange || ['',''];
	/******** Basic init END ********/
	

	/**** Name START ****/
	if(
		!checkNaNull($('#StudentChineseSurname').val()) && 
		!checkIsChineseCharacter($('#StudentChineseSurname').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		$('#StudentChineseSurname').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentChineseFirstName').val()) && 
		!checkIsChineseCharacter($('#StudentChineseFirstName').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		$('#StudentChineseFirstName').focus();
		return false;
	}
	
	
	if($('#StudentEnglishSurname').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishname']?>");	
		$('#StudentEnglishSurname').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishSurname').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishSurname').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		$('#StudentEnglishSurname').focus();
		return false;
	}
	
	
	if($('#StudentEnglishFirstName').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishname']?>");	
		$('#StudentEnglishFirstName').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishFirstName').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishFirstName').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		$('#StudentEnglishFirstName').focus();
		return false;
	}
	/**** Name END ****/

	/**** DOB START ****/
	if( !$('#StudentDateOfBirth').val().match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/) ){
		if($('#StudentDateOfBirth').val()!=''){
			alert(" <?=$kis_lang['Admission']['msg']['invaliddateformat']?>");
		}else{
			alert(" <?=$kis_lang['Admission']['msg']['enterdateofbirth']?>");	
		}
		
		$('#StudentDateOfBirth').focus();
		return false;
	} 
	if(
		(
			dOBRange[0] !='' && 
			$('#StudentDateOfBirth').val() < dOBRange[0] 
		) || (
			dOBRange[1] !='' && 
			$('#StudentDateOfBirth').val() > dOBRange[1]
		)
	){
		alert(" <?=$kis_lang['Admission']['msg']['invalidbdaydateformat']?>");
		$('#StudentDateOfBirth').focus();
		return false;
	} 
	/**** DOB END ****/

	/**** Gender START ****/
	if($('input[name=StudentGender]:checked').length == 0){
		alert(" <?=$kis_lang['Admission']['msg']['selectgender']?>");	
		$('input[name=StudentGender]:first').focus();
		return false;
	}
	/**** Gender END ****/
	
	/**** Personal Identification START ****/
	if($('#StudentBirthCertNo').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterPersonalIdentificationNo']?>");	
		$('#StudentBirthCertNo').focus();
		return false;
	}
// 	if(!check_hkid(form1.StudentBirthCertNo.value)){
//    	alert(" <?=$kis_lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>\n Invalid Birth Cert No.");
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }
    
//     if(!isUpdatePeriod && checkBirthCertNo() > 0){
//    	alert(" <?=$kis_lang['Admission']['SHCK']['msg']['duplicateBirthCertNo']?>\n The Birth Cert No. is used for admission! Please enter another Birth Cert No.");	
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }
	/**** Personal Identification END ****/


	/**** Nationality START ****/
	if($('#StudentCounty').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enternationality']?>");	
		$('#StudentCounty').focus();
		return false;
	}
	/**** Nationality END ****/

	/**** Address START ****/
	if($('#StudentAddress').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterhomeaddress']?>");	
		$('#StudentAddress').focus();
		return false;
	}
	/**** Address END ****/

	/**** Registed Residence START ****/
	if($('#StudentRegistedResidence').val().trim()==''){
		alert("<?=$kis_lang['Admission']['PICLC']['msg']['enterRegistedResidence']?>");	
		$('#StudentRegistedResidence').focus();
		return false;
	}
	/**** Registed Residence END ****/

	/**** Native Place START ****/
	if($('#StudentNativePlace').val().trim()==''){
		alert("<?=$kis_lang['Admission']['PICLC']['msg']['enterNativePlace']?>");	
		$('#StudentNativePlace').focus();
		return false;
	}
	/**** Native Place END ****/
	

	/**** Email START ****/
	if($('#StudentEmail').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	if($('#StudentEmail').val().trim()!='' && !re.test($('#StudentEmail').val().trim())){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	/**** Email END ****/
	
	return true;
}
</script>