<?php
global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$parentTypeArr = array(
    1 => 'F',
    2 => 'M',
    3 => 'G'
);

?>
<style>
    .form_guardian_head, .form_guardian_field {
        text-align: center !important;
    }
</style>
<table class="form_table">
    <colgroup>
        <col style="width: 30%">
        <col style="width: 20%">
        <col style="width: 20%">
        <col style="width: 20%">
    </colgroup>
    <tr>
        <td>&nbsp;</td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_head">
                <center><?= $kis_lang['Admission']['HKUGAC']['ParentInformation'][$parentType] ?></center>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['englishname'] ?>
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($ParentInfo[$parentType]['EnglishName']) ?>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['chinesename'] ?>
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($ParentInfo[$parentType]['ChineseName']) ?>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['Occupation'] ?>
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($ParentInfo[$parentType]['JobTitle']) ?>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['Company'] ?>
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($ParentInfo[$parentType]['Company']) ?>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['Telephone'] ?>(<?= $kis_lang['Admission']['HKUGAC']['Day'] ?>)
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($ParentInfo[$parentType]['OfficeTelNo']) ?>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['Telephone'] ?>(<?= $kis_lang['Admission']['HKUGAC']['Night'] ?>)
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($allCustInfo["G{$index}NightTelephone"][0]['Value']) ?>
            </td>
        <?php } ?>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['Telephone'] ?>(<?= $kis_lang['Admission']['HKUGAC']['Mobile'] ?>)
        </td>
        <?php foreach ($parentTypeArr as $index => $parentType) { ?>
            <td class="form_guardian_field">
                <?= kis_ui::displayTableField($ParentInfo[$parentType]['Mobile']) ?>
            </td>
        <?php } ?>
    </tr>
</table>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col width="30%">
        <col width="70%">
    </colgroup>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['contactperson'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($allCustInfo['ContactPerson'][0]['Value'] == "Father" ? $kis_lang['Admission']['HKUGAC']['father'] : ($allCustInfo['ContactPerson'][0]['Value'] == "Mother" ? $kis_lang['Admission']['HKUGAC']['mother'] : ($allCustInfo['ContactPerson'][0]['Value'] == "Guardian" ? $kis_lang['Admission']['HKUGAC']['guardian'] : ''))) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['EmailAddress'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($ParentInfo['F']['email']) ?>
        </td>
    </tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['IsTwinsApplied'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($allCustInfo['IsTwinsApplied'][0]['Value'] == 'Y' ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsTwinsApplied'][0]['Value'] == 'N' ? $kis_lang['Admission']['no'] : '')) ?>
            <?php if ($allCustInfo['IsTwinsApplied'][0]['Value'] == 'Y'): ?>
                <br/><?= $kis_lang['Admission']['name'] ?>: <?= kis_ui::displayTableField($allCustInfo['TwinsAppliedName'][0]['Value']) ?>
                <br/><?= $kis_lang['Admission']['HKUGAC']['strn'] ?>: <?= kis_ui::displayTableField($allCustInfo['TwinsAppliedSTRN'][0]['Value']) ?>
            <?php endif; ?>
        </td>
    </tr>
</table>