<?php
global $libkis_admission;

$RelativeInfo = $libkis_admission->getApplicationRelativesInfo($schoolYearID, '', $applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>
<style>
.whereToKnowOptionDiv {
	margin-bottom: 5px;
}

.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>

<table class="form_table">
    <colgroup>
        <col style="width:40%">
        <col style="width:30%">
        <col style="width:30%">
    </colgroup>
    <tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['year'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] ?></td>
	</tr>
	<?php for($i=0;$i<$libkis_admission::LEARNING_EXPERIENCE_COUNT;$i++){ ?>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['HKUGAC']['LearningExperience'] ?> (<?=$i+1?>)
		</td>
		<td>
			<?= kis_ui::displayTableField($allCustInfo['StudentExperience'.$i.'_Year'][0]['Value'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($allCustInfo['StudentExperience'.$i.'_Name'][0]['Value'])?>
		</td>
	</tr>
	<? }?>
</table>

<table class="form_table" style="font-size: 13px">
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAC']['WhyChooseSchool']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( nl2br($allCustInfo['WhyChooseSchool'][0]['Value']) )?>
    	</td>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAC']['ApplicantStrengthsWeaknesses']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( nl2br($allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value']) )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
			<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['IsParentsHKUGAMember']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['IsParentsHKUGAMember'][0]['Value'] == 'Y' ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsParentsHKUGAMember'][0]['Value'] == 'N' ? $kis_lang['Admission']['no'] : ''))?>
    		<br/><?=$kis_lang['Admission']['HKUGAC']['MemberName']?>: <?= kis_ui::displayTableField( $allCustInfo['ParentsHKUGAMemberName'][0]['Value'] )?>
    		<br/><?=$kis_lang['Admission']['HKUGAC']['MembershipNo']?>: <?= kis_ui::displayTableField( $allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
			<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['IsParentsHKUGAEFMember']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] == 'Y' ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] == 'N' ? $kis_lang['Admission']['no'] : ''))?>
    		<br/><?=$kis_lang['Admission']['HKUGAC']['MemberName']?>: <?= kis_ui::displayTableField( $allCustInfo['ParentsHKUGAEFMemberName'][0]['Value'] )?>
    		<br/><?=$kis_lang['Admission']['HKUGAC']['MembershipNo']?>: <?= kis_ui::displayTableField( $allCustInfo['ParentsHKUGAEFMemberNumber'][0]['Value'] )?>
    	</td>
	</tr>
</table>	
<table class="form_table">
    <colgroup>
        <col style="width:20%">
        <col style="width:20%">
        <col style="width:20%">
        <col style="width:20%">
        <col style="width:20%">
    </colgroup>
    <tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['name'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['year'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['Class'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['Relationship'] ?></td>
	</tr>
	<?php for($i=0;$i<$libkis_admission::RELATIVES_COUNT;$i++){ ?>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['HKUGAC']['SiblingInformation'] ?> (<?=$i+1?>)
		</td>
		<td>
			<?= kis_ui::displayTableField($RelativeInfo[$i]['Name'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($RelativeInfo[$i]['Year'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($RelativeInfo[$i]['ClassPosition'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($RelativeInfo[$i]['Relationship'])?>
		</td>
	</tr>
	<? }?>
</table>