<?php
global $libkis_admission;

$StudentInfo = $applicationInfo;
$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,'',$applicationInfo['applicationID'],'');
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

$applySchoolId = $allCustInfo['ApplySchool'][0]['Value'];
$applySchoolType = $admission_cfg['SchoolType'][$applySchoolId];
?>
<table class="form_table">
	<tbody>
    <tr>
        <td class="field_title">
            <?=$kis_lang['Admission']['HKUGAC']['strn']?>
        </td>
        <td>
            <?=kis_ui::displayTableField($allCustInfo['StudentSTRN'][0]['Value'])?>
        </td>
    </tr>
		<tr>
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['chinesename']?>
			</td>
			<td width="40%">
				<?=kis_ui::displayTableField($StudentInfo['ChineseName'])?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin: 0px;">
					<img
						src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>" />
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['HKUGAC']['englishname']?>
			</td>
			<td>
				<?=kis_ui::displayTableField($StudentInfo['EnglishName'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][ $StudentInfo['Gender'] ])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['Age']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Age'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['nationality']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Nationality'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAC']['birthcertno']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['BirthCertNo'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['dateofbirth'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['PlaceOfBirth'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['address']?>(<?=$kis_lang['Admission']['HKUGAC']['Chinese']?>)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['AddressChi'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAPS']['address']?>(<?=$kis_lang['Admission']['HKUGAC']['English']?>)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Address'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAC']['contactaddress']?>(<?=$kis_lang['Admission']['HKUGAC']['Chinese']?>)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['ContactAddressChi'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAC']['contactaddress']?>(<?=$kis_lang['Admission']['HKUGAC']['English']?>)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['ContactAddress'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAC']['HomeTelephone']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['HomeTelNo'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['HKUGAC']['Fax']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Fax'])?>
			</td>
		</tr>
	</tbody>
</table>
<table class="form_table">
    <colgroup>
        <col style="width:30%">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:24%">
    </colgroup>
    <tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['year'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['Class'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['School'] ?></td>
	</tr>
	<?php for($i=0;$i<1;$i++){ ?>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['HKUGAC']['CurrentSchoolAttend'] ?>
		</td>
		<td>
			<?= kis_ui::displayTableField($StudentPrevSchoolInfo[$i]['Year'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($StudentPrevSchoolInfo[$i]['Class'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($StudentPrevSchoolInfo[$i]['NameOfSchool'])?>
		</td>
	</tr>
	<? }?>
	<?php for($i=1;$i<$libkis_admission::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){ ?>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['HKUGAC']['LastSchoolAttend'] ?> (<?=$i?>)
		</td>
		<td>
			<?= kis_ui::displayTableField($StudentPrevSchoolInfo[$i]['Year'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($StudentPrevSchoolInfo[$i]['Class'])?>
		</td>
		<td>
			<?= kis_ui::displayTableField($StudentPrevSchoolInfo[$i]['NameOfSchool'])?>
		</td>
	</tr>
	<? }?>
</table>
<table class="form_table">
	<tbody>
		<tr>
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['document']?>
			</td>
			<td width="70%" colspan="2">
				<?php
    $attachmentAry = array();
    for ($i = 0; $i < sizeof($attachmentSettings); $i ++) {
        
        // #### Get attachment name START #### //
        $isExtra = $attachmentSettings[$i]['IsExtra'];
        $attachment_name = $attachmentSettings[$i]['AttachmentName'];
        $_filePath = $attachmentList[$attachment_name]['link'];

        // ## Multi-lang START ## //
        $attachmentNameDisplay = $attachment_name;
        if ($admission_cfg['MultipleLang']) {
            foreach ($admission_cfg['Lang'] as $index => $kis_lang) {
                if ($kis_lang == $intranet_session_language) {
                    $attachmentNameDisplay = $attachmentSettings[$i]["AttachmentName{$index}"];
                }
            }
        }
        // ## Multi-lang END ## //

        // ## Cust-lang START ## //
        if ($kis_lang['Admission']['PICLC']['documentUpload'][$StudentInfo['BirthCertTypeOther']][$i]) {
            $attachmentNameDisplay = $kis_lang['Admission']['PICLC']['documentUpload'][$StudentInfo['BirthCertTypeOther']][$i];
        }
        // ## Cust-lang END ## //

        if($isExtra) {
            $attachmentNameDisplay = $attachmentList[$attachment_name]['originalName'][0];
            $_filePath = '/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$schoolYearID.'&id='.$applicationInfo['applicationID'].'&type='.urlencode($attachment_name);
        }
        // #### Get attachment name END #### //

        if ($_filePath) {
            // $attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
            $attachmentAry[] = '<a href="' . $_filePath . '" target="_blank">' . $attachmentNameDisplay . '</a>';
        }
    }
    ?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>
	</tbody>
</table>