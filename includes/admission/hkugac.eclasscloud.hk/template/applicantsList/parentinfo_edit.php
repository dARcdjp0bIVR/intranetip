<?php
global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$star = $mustfillinsymbol;
$parentTypeArr = array(
    1 => 'F',
    2 => 'M',
    3 => 'G'
);

?>
<style>
.col2div {
	display: inline-block;
	width: 48%;
}

.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>

<table class="form_table parentInfo">
	<colgroup>
		<col style="width: 30%">
		<col style="width: 20%">
		<col style="width: 20%">
		<col style="width: 20%">
	</colgroup>
	<tr>
		<td>&nbsp;</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAC']['ParentInformation'][$parentType] ?></center></td>
		<?php }?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['englishname']?>
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>EnglishName" type="text"
					id="G<?=$index?>EnglishName" class="textboxtext"
					value="<?=$ParentInfo[$parentType]['EnglishName'] ?>" />
			</td>
		<?php }?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAPS']['chinesename']?>
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>ChineseName" type="text"
					id="G<?=$index?>ChineseName" class="textboxtext"
					value="<?=$ParentInfo[$parentType]['ChineseName'] ?>" />
			</td>
		<?php }?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['Occupation']?>
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>JobTitle" type="text"
					id="G<?=$index?>JobTitle" class="textboxtext"
					value="<?=$ParentInfo[$parentType]['JobTitle'] ?>" />
			</td>
		<?php }?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['Company']?>
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>Company" type="text"
					id="G<?=$index?>Company" class="textboxtext"
					value="<?=$ParentInfo[$parentType]['Company'] ?>" />
			</td>
		<?php }?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['Telephone']?>(<?=$kis_lang['Admission']['HKUGAC']['Day']?>)
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>OfficeTelNo" type="text"
					id="G<?=$index?>OfficeTelNo" class="textboxtext"
					value="<?=$ParentInfo[$parentType]['OfficeTelNo'] ?>" />
			</td>
		<?php }?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['Telephone']?>(<?=$kis_lang['Admission']['HKUGAC']['Night']?>)
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>NightTelephone" type="text"
					id="G<?=$index?>NightTelephone" class="textboxtext"
					value="<?=$allCustInfo["G{$index}NightTelephone"][0]['Value'] ?>" />
			</td>
		<?php }?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['Telephone']?>(<?=$kis_lang['Admission']['HKUGAC']['Mobile']?>)
		</td>
		<?php foreach ($parentTypeArr as $index => $parentType) {?>
    		<td class="form_guardian_field">
				<input name="G<?=$index?>Mobile" type="text"
					id="G<?=$index?>Mobile" class="textboxtext"
					value="<?=$ParentInfo[$parentType]['Mobile'] ?>" />
			</td>
		<?php }?>
	</tr>
</table>

<table class="form_table" style="font-size: 13px">
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tr>
		<td class="field_title">
			<?=$star?>
    		<?=$kis_lang['Admission']['contactperson']?>
    	</td>
		<td>
			<?=$libinterface->Get_Radio_Button('BothParentResidingHK_Father', 'ContactPerson', 'Father', ($allCustInfo['ContactPerson'][0]['Value'] == "Father"), '', $kis_lang['Admission']['HKUGAC']['father'])?>
    		<?=$libinterface->Get_Radio_Button('BothParentResidingHK_Mother', 'ContactPerson', 'Mother', ($allCustInfo['ContactPerson'][0]['Value'] == "Mother"), '', $kis_lang['Admission']['HKUGAC']['mother'])?>
    		<?=$libinterface->Get_Radio_Button('BothParentResidingHK_Guardian', 'ContactPerson', 'Guardian', ($allCustInfo['ContactPerson'][0]['Value'] == "Guardian"), '', $kis_lang['Admission']['HKUGAC']['guardian'])?>
    	</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$star?>
    		<?=$kis_lang['Admission']['HKUGAC']['EmailAddress']?>
    	</td>
		<td>
			<input name="G1Email" type="text"
					id="Email" class="textboxtext"
					value="<?=$ParentInfo['F']['email'] ?>" />
    	</td>
	</tr>

    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAC']['IsTwinsApplied'] ?>
        </td>
        <td>
            <?=$libinterface->Get_Radio_Button('IsTwinsApplied_Y', 'IsTwinsApplied', 'Y', ($allCustInfo['IsTwinsApplied'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
            <?=$libinterface->Get_Radio_Button('IsTwinsApplied_N', 'IsTwinsApplied', 'N', ($allCustInfo['IsTwinsApplied'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
            <br/><?=$kis_lang['Admission']['name']?>: <input name="TwinsAppliedName" type="text"
                                                                             id="TwinsAppliedName" class="textboxtext"
                                                                             value="<?=$allCustInfo['TwinsAppliedName'][0]['Value']?>" />
            <br/><?=$kis_lang['Admission']['HKUGAC']['strn']?>: <input name="TwinsAppliedSTRN" type="text"
                                                                               id="TwinsAppliedSTRN" class="textboxtext"
                                                                               value="<?=$allCustInfo['TwinsAppliedSTRN'][0]['Value']?>" />
        </td>
    </tr>
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
    if($('#IsTwinsApplied_N').prop("checked")){
        $('#TwinsAppliedName, #TwinsAppliedSTRN').val('').change();
    }
    
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	return check_parent_form();
}

function check_parent_form(){
	//For debugging only
	return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;

	var form1 = applicant_form;
	var isValid = true;
	/******** Basic init END ********/
	
	
	/**** All empty START ****/
	var parentHasInfoArr = [];
	var _text = '';
	$('.parentInfo input').each(function(){
		if($(this).val().trim() != ''){
			parentHasInfoArr.push($(this).data('parent'));
		}
	});
	
	if(parentHasInfoArr.length == 0){
		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterAtLeastOneParent']?>");
		$('#G1EnglishSurname').focus();
		return false;
	}
	/**** All empty END ****/
	

	/**** Name START ****/	
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(EnglishSurname|EnglishFirstName)/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
    		this.focus();
    		return isValid = false;
    	}
    	if(
    		!checkNaNull(this.value) &&
    		!checkIsEnglishCharacter(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(ChineseSurname|ChineseFirstName)/);
    }).each(function(){
    	if(
			$.trim(this.value)!='' &&
    		!checkNaNull(this.value) &&
    		!checkIsChineseCharacter(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name END ****/
	
	/**** Nationality START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dNationality/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['msg']['enternationality']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Nationality START ****/
	
	/**** Native Place START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dNativePlace/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$kis_lang['Admission']['PICLC']['msg']['enterNativePlace']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Native Place START ****/
	
	/**** Employer START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dCompany/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterEmployer']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Employer START ****/
	
	/**** Position START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dJobPosition/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterPosition']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Position START ****/
	
	/**** Business Address START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dOfficeAddress/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterBusinessAddress']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Business Address START ****/
	
	/**** Business Phone START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dOfficeTelNo/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterBusinessPhone']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Business Phone START ****/
	
	/**** Mobile START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dMobile/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['msg']['entermobilephoneno']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Mobile START ****/
	
	/**** Email START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dEmail/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
    		this.focus();
    		return isValid = false;
    	}

    	if(!re.test($.trim(this.value))){
    		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Email START ****/

	return true;
}
</script>