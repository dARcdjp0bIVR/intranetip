<section id="otherForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation" style="display: none">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['otherInfo'] ?> <?=$LangEn['Admission']['otherInfo'] ?>
	</div>
	<div class="sheet">
		
		<div class="item">
			<div class="itemLabel"><?=$LangB5['Admission']['HKUGAC']['LearningExperience'] ?><br/><?=$LangEn['Admission']['HKUGAC']['LearningExperience'] ?></div>
			<?php for($i=0;$i<$lac::LEARNING_EXPERIENCE_COUNT;$i++){ ?>
			<span class="itemInput">
				<div class="itemInput-complex-order"><?=$i+1?></div>
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-half">
						<input type="text" name="StudentExperience<?=$i?>_Year" id="StudentExperience<?=$i?>_Year" value="<?=$allCustInfo['StudentExperience'.$i.'_Year'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['year'] ?> <?=$LangEn['Admission']['year'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentExperience<?=$i?>_Year"></div>
						<div class="remark">(YYYY-YYYY)</div>
					</div>
					<div class="textbox-floatlabel textbox-half">
						<input type="text" name="StudentExperience<?=$i?>_Name" id="StudentExperience<?=$i?>_Name" value="<?=$allCustInfo['StudentExperience'.$i.'_Name'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['OtherLearningExperience'] ?> <?=$LangEn['Admission']['HKUGAC']['OtherLearningExperience'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-half">
						<div class="dataLabel"><?=$LangB5['Admission']['year'] ?> <?=$LangEn['Admission']['year'] ?></div>
						<div class="dataValue <?=$allCustInfo['StudentExperience'.$i.'_Year'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['StudentExperience'.$i.'_Year'][0]['Value']? $allCustInfo['StudentExperience'.$i.'_Year'][0]['Value'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-half">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['HKUGAC']['OtherLearningExperience'] ?> <?=$LangEn['Admission']['HKUGAC']['HKUGAC']['OtherLearningExperience'] ?></div>
						<div class="dataValue <?=$allCustInfo['StudentExperience'.$i.'_Name'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['StudentExperience'.$i.'_Name'][0]['Value'] ? $allCustInfo['StudentExperience'.$i.'_Name'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			<?php }?>
		</div>
		
		<div class="item">
			<div class="itemLabel"><?=$LangB5['Admission']['HKUGAC']['WhyChooseSchool'] ?><br/><?=$LangEn['Admission']['HKUGAC']['WhyChooseSchool'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<textarea rows="10" name="WhyChooseSchool" id="WhyChooseSchool"><?=$allCustInfo['WhyChooseSchool'][0]['Value'] ?></textarea>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['WhyChooseSchool'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['WhyChooseSchool'][0]['Value'] ? nl2br($allCustInfo['WhyChooseSchool'][0]['Value']) : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel"><?=$LangB5['Admission']['HKUGAC']['ApplicantStrengthsWeaknesses'] ?><br/><?=$LangEn['Admission']['HKUGAC']['ApplicantStrengthsWeaknesses'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<textarea rows="10" name="ApplicantStrengthsWeaknesses" id="ApplicantStrengthsWeaknesses"><?=$allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value'] ?></textarea>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value'] ? nl2br($allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value']) : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['IsParentsHKUGAMember'] ?><br/><?=$LangEn['Admission']['HKUGAC']['IsParentsHKUGAMember'] ?></div>
			<span class="itemInput">
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsParentsHKUGAMember_Y" name="IsParentsHKUGAMember" value="Y" required <?=$allCustInfo['IsParentsHKUGAMember'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsParentsHKUGAMember_Y"><?=$LangB5['Admission']['yes'] ?> <?=$LangEn['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsParentsHKUGAMember_N" name="IsParentsHKUGAMember" value="N" required <?=!$allCustInfo['IsParentsHKUGAMember'][0]['Value'] || $allCustInfo['IsParentsHKUGAMember'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsParentsHKUGAMember_N"><?=$LangB5['Admission']['no'] ?> <?=$LangEn['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="ParentsHKUGAMemberName" id="ParentsHKUGAMemberName" value="<?=$allCustInfo['ParentsHKUGAMemberName'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['MemberName'] ?> <?=$LangEn['Admission']['HKUGAC']['MemberName'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errParentsHKUGAMemberName"></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="ParentsHKUGAMemberNumber" id="ParentsHKUGAMemberNumber" value="<?=$allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['MembershipNo'] ?> <?=$LangEn['Admission']['HKUGAC']['MembershipNo'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['MemberName'] ?> <?=$LangEn['Admission']['HKUGAC']['MemberName'] ?></div>
						<div class="dataValue <?=$allCustInfo['ParentsHKUGAMemberName'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ParentsHKUGAMemberName'][0]['Value'] ? $allCustInfo['ParentsHKUGAMemberName'][0]['Value'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['MembershipNo'] ?> <?=$LangEn['Admission']['HKUGAC']['MembershipNo'] ?></div>
						<div class="dataValue <?=$allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] ? $allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['IsParentsHKUGAEFMember'] ?><br/><?=$LangEn['Admission']['HKUGAC']['IsParentsHKUGAEFMember'] ?></div>
			<span class="itemInput">
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsParentsHKUGAEFMember_Y" name="IsParentsHKUGAEFMember" value="Y" required <?=$allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsParentsHKUGAEFMember_Y"><?=$LangB5['Admission']['yes'] ?> <?=$LangEn['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsParentsHKUGAEFMember_N" name="IsParentsHKUGAEFMember" value="N" required <?=!$allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] || $allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsParentsHKUGAEFMember_N"><?=$LangB5['Admission']['no'] ?> <?=$LangEn['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="ParentsHKUGAEFMemberName" id="ParentsHKUGAEFMemberName" value="<?=$allCustInfo['ParentsHKUGAEFMemberName'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['MemberName'] ?> <?=$LangEn['Admission']['HKUGAC']['MemberName'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errParentsHKUGAEFMemberName"></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="ParentsHKUGAEFMemberNumber" id="ParentsHKUGAEFMemberNumber" value="<?=$allCustInfo['ParentsHKUGAEFMemberNumber'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['MembershipNo'] ?> <?=$LangEn['Admission']['HKUGAC']['MembershipNo'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['MemberName'] ?> <?=$LangEn['Admission']['HKUGAC']['MemberName'] ?></div>
						<div class="dataValue <?=$allCustInfo['ParentsHKUGAMemberName'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ParentsHKUGAMemberName'][0]['Value'] ? $allCustInfo['ParentsHKUGAMemberName'][0]['Value'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['MembershipNo'] ?> <?=$LangEn['Admission']['HKUGAC']['MembershipNo'] ?></div>
						<div class="dataValue <?=$allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] ? $allCustInfo['ParentsHKUGAMemberNumber'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel"><?=$LangB5['Admission']['HKUGAC']['SiblingInformation'] ?><br/><?=$LangEn['Admission']['HKUGAC']['SiblingInformation'] ?></div>
			<?php for($i=0;$i<$lac::RELATIVES_COUNT;$i++){ ?>
			<span class="itemInput">
				<div class="itemInput-complex-order"><?=$i+1?></div>
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In4">
						<input type="text" name="Relative<?=$i?>_Name" id="Relative<?=$i?>_Name" value="<?=$RelativeInfo[$i]['Name'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['name'] ?> <?=$LangEn['Admission']['name'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errRelative<?=$i?>_Name"></div>
					</div>
					<div class="textbox-floatlabel textbox-In4">
						<input type="text" name="Relative<?=$i?>_Year" id="Relative<?=$i?>_Year" value="<?=$RelativeInfo[$i]['Year'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['year'] ?> <?=$LangEn['Admission']['year'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errRelative<?=$i?>_Year"></div>
						<div class="remark">(YYYY-YYYY)</div>
					</div>
					<div class="textbox-floatlabel textbox-In4">
						<input type="text" name="Relative<?=$i?>_Class" id="Relative<?=$i?>_Class" value="<?=$RelativeInfo[$i]['ClassPosition'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['Class'] ?> <?=$LangEn['Admission']['HKUGAC']['Class'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In4">
						<input type="text" name="Relative<?=$i?>_Relationship" id="Relative<?=$i?>_Relationship" value="<?=$RelativeInfo[$i]['Relationship'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['Relationship'] ?> <?=$LangEn['Admission']['HKUGAC']['Relationship'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In4">
						<div class="dataLabel"><?=$LangB5['Admission']['name'] ?> <?=$LangEn['Admission']['name'] ?></div>
						<div class="dataValue <?=$RelativeInfo[$i]['Name'] ? '' : 'dataValue-empty'?>"><?=$RelativeInfo[$i]['Name'] ? $RelativeInfo[$i]['Name'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In4">
						<div class="dataLabel"><?=$LangB5['Admission']['year'] ?> <?=$LangEn['Admission']['year'] ?></div>
						<div class="dataValue <?=$RelativeInfo[$i]['Year'] ? '' : 'dataValue-empty'?>"><?=$RelativeInfo[$i]['Year'] ? $RelativeInfo[$i]['Year'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In4">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Class'] ?> <?=$LangEn['Admission']['HKUGAC']['Class'] ?></div>
						<div class="dataValue <?=$RelativeInfo[$i]['ClassPosition'] ? '' : 'dataValue-empty'?>"><?=$RelativeInfo[$i]['ClassPosition'] ? $RelativeInfo[$i]['ClassPosition'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In4">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Relationship'] ?> <?=$LangEn['Admission']['HKUGAC']['Relationship'] ?></div>
						<div class="dataValue <?=$RelativeInfo[$i]['Relationship'] ? '' : 'dataValue-empty'?>"><?=$RelativeInfo[$i]['Relationship'] ? $RelativeInfo[$i]['Relationship'] : '－－' ?></div>
					</div>
				</div>
			</span>
			<? } ?>
		</div>
			
		<div class="item" id="divOtherFormClassLevel">
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['applyLevel'] ?> <?=$LangEn['Admission']['applyLevel'] ?></div>
				<div class="dataValue"><?=$classLevel ?></div>
			</span>
		</div>
		
		<div class="remark">* <?=$LangB5['Admission']['requiredFields'] ?> <?=$LangEn['Admission']['requiredFields'] ?></div>
	</div>
</section>	

<script>
$(function(){
	'use strict';
	
	$('#otherForm .itemInput, #feeForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$textarea = $itemInput.find('textarea'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html($(this).val());
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
		
		$textarea.on('change', function(e){
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html($(this).val().replace(/\n/g,'<br/>'));
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html($(this).next('label').html());
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
			
		$inputDateSelect.on('change', function(e){
			$dataValue.html($inputDateYearSelect.val()+'-'+$inputDateMonthSelect.val()+'-'+$inputDateDaySelect.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
		
		
	});
	
	window.checkOtherForm = (function(lifecycle){
		var isValid = true;
		var $otherForm = $('#otherForm');

		/******** Basic init START ********/
		//for YYYY validation
		var re = /^\d{4}-\d{4}$/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($otherForm);
		
		if($('#IsParentsHKUGAMember_Y').prop("checked") && $('#ParentsHKUGAMemberName').val() == ''){
			$('#ParentsHKUGAMemberName').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#ParentsHKUGAMemberName');
			isValid = false;
		}
		
		if($('#IsParentsHKUGAMember_Y').prop("checked") && $('#ParentsHKUGAMemberNumber').val() == ''){
			$('#ParentsHKUGAMemberNumber').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#ParentsHKUGAMemberNumber');
			isValid = false;
		}
		
		if($('#IsParentsHKUGAEFMember_Y').prop("checked") && $('#ParentsHKUGAEFMemberName').val() == ''){
			$('#ParentsHKUGAEFMemberName').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#ParentsHKUGAEFMemberName');
			isValid = false;
		}
		
		if($('#IsParentsHKUGAEFMember_Y').prop("checked") && $('#ParentsHKUGAEFMemberNumber').val() == ''){
			$('#ParentsHKUGAEFMemberNumber').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#ParentsHKUGAEFMemberNumber');
			isValid = false;
		}
		
		if($('#StudentExperience0_Year').val().trim()!='' && !re.test($('#StudentExperience0_Year').val().trim())){
			if($('#StudentExperience0_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentExperience0_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errStudentExperience0_Year').removeClass('hide');
				focusElement = $('#StudentExperience0_Year');
			}	
	    	isValid = false;
		}
		
		if($('#StudentExperience1_Year').val().trim()!='' && !re.test($('#StudentExperience1_Year').val().trim())){
			if($('#StudentExperience1_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentExperience1_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errStudentExperience1_Year').removeClass('hide');
				focusElement = $('#StudentExperience1_Year');
			}	
	    	isValid = false;
		}
		
		if($('#StudentExperience2_Year').val().trim()!='' && !re.test($('#StudentExperience2_Year').val().trim())){
			if($('#StudentExperience2_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentExperience2_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errStudentExperience2_Year').removeClass('hide');
				focusElement = $('#StudentExperience2_Year');
			}	
	    	isValid = false;
		}
		
		if($('#StudentExperience3_Year').val().trim()!='' && !re.test($('#StudentExperience3_Year').val().trim())){
			if($('#StudentExperience3_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentExperience3_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errStudentExperience3_Year').removeClass('hide');
				focusElement = $('#StudentExperience3_Year');
			}	
	    	isValid = false;
		}
		
		if($('#StudentExperience4_Year').val().trim()!='' && !re.test($('#StudentExperience4_Year').val().trim())){
			if($('#StudentExperience4_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentExperience4_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errStudentExperience4_Year').removeClass('hide');
				focusElement = $('#StudentExperience4_Year');
			}	
	    	isValid = false;
		}
		
		if($('#Relative0_Year').val().trim()!='' && !re.test($('#Relative0_Year').val().trim())){
			if($('#Relative0_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errRelative0_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errRelative0_Year').removeClass('hide');
				focusElement = $('#Relative0_Year');
			}	
	    	isValid = false;
		}
		
		if($('#Relative1_Year').val().trim()!='' && !re.test($('#Relative1_Year').val().trim())){
			if($('#Relative1_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errRelative1_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errRelative1_Year').removeClass('hide');
				focusElement = $('#Relative1_Year');
			}	
	    	isValid = false;
		}
		
		if($('#Relative2_Year').val().trim()!='' && !re.test($('#Relative2_Year').val().trim())){
			if($('#Relative2_Year').parent().find('.remark-warn').hasClass('hide')){
				$('#errRelative2_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
				$('#errRelative2_Year').removeClass('hide');
				focusElement = $('#Relative2_Year');
			}	
	    	isValid = false;
		}
		
		/**** Check required END ****/

		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkOtherForm);
});
</script>