<section id="academicPerformanceForm"
         class="form displaySection display_pageAcademicPerformanceInfo display_pageConfirmation" style="display: none">
    <div class="form-header marginB10">
        <?= $LangB5['Admission']['HKUGAC_academicPerformance'] ?>
        <?= $LangEn['Admission']['HKUGAC_academicPerformance'] ?>
    </div>
    <div class="sheet">

        <div class="item item-half">
            <div class="text-inst">
                <?= $LangB5['Admission']['HKUGAC']['P5FirstResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['P5FirstResult'] ?><br/>
				<?= $LangB5['Admission']['HKUGAC']['P6FinalResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['P6FinalResult'] ?><br/>
				<?= $LangB5['Admission']['HKUGAC']['S1FinalResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['S1FinalResult'] ?><br/>
				<?= $LangB5['Admission']['HKUGAC']['S2FinalResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['S2FinalResult'] ?>
            </div>
            <?php foreach ($admission_cfg['Subjects'] as $subjectCode): ?>
                <span class="itemInput">
                    <div class="textbox-floatlabel">
                        <input type="text" name="F5_Subject_<?= $subjectCode ?>" id="F5_Subject_<?= $subjectCode ?>"
                               value="<?= $allCustInfo["F5_Subject_{$subjectCode}"][0]['Value'] ?>">
                        <div class="textboxLabel ">
                            <?= $LangB5['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                            <?= $LangEn['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                        </div>
                    </div>
                </span><span class="itemData">
                    <div class="dataLabel">
                            <?= $LangB5['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                            <?= $LangEn['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                    </div>
                    <div class="dataValue <?= $allCustInfo["F5_Subject_{$subjectCode}"][0]['Value'] ? '' : 'dataValue-empty' ?>">
                        <?= $allCustInfo["F5_Subject_{$subjectCode}"][0]['Value'] ? $allCustInfo["F5_Subject_{$subjectCode}"][0]['Value'] : '－－' ?>
                    </div>
                </span>
            <?php endforeach; ?>
        </div>
        <div class="item item-half">
            <div class="text-inst">
                <?= $LangB5['Admission']['HKUGAC']['P5FinalResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['P5FinalResult'] ?><br/>
				<?= $LangB5['Admission']['HKUGAC']['S1FirstResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['S1FirstResult'] ?><br/>
				<?= $LangB5['Admission']['HKUGAC']['S2FirstResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['S2FirstResult'] ?><br/>
				<?= $LangB5['Admission']['HKUGAC']['S3FirstResult'] ?><br/>
				<?= $LangEn['Admission']['HKUGAC']['S3FirstResult'] ?>
            </div>
            <?php foreach ($admission_cfg['Subjects'] as $subjectCode): ?>
                <span class="itemInput">
                    <div class="textbox-floatlabel">
                        <input type="text" name="F6_Subject_<?= $subjectCode ?>" id="F6_Subject_<?= $subjectCode ?>"
                               value="<?= $allCustInfo["F6_Subject_{$subjectCode}"][0]['Value'] ?>">
                        <div class="textboxLabel ">
                            <?= $LangB5['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                            <?= $LangEn['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                        </div>
                    </div>
                </span><span class="itemData">
                    <div class="dataLabel">
                            <?= $LangB5['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                            <?= $LangEn['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
                    </div>
                    <div class="dataValue <?= $allCustInfo["F6_Subject_{$subjectCode}"][0]['Value'] ? '' : 'dataValue-empty' ?>">
                        <?= $allCustInfo["F6_Subject_{$subjectCode}"][0]['Value'] ? $allCustInfo["F6_Subject_{$subjectCode}"][0]['Value'] : '－－' ?>
                    </div>
                </span>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<script>
    $(function () {
        'use strict';

        $('#academicPerformanceForm .itemInput').each(function () {
            var $itemInput = $(this),
                $inputText = $itemInput.find('input[type="text"]'),
                $inputRadio = $itemInput.find('input[type="radio"]'),
                $inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
                $inputDateYearSelect = $itemInput.find('select.dateYear'),
                $inputDateMonthSelect = $itemInput.find('select.dateMonth'),
                $inputDateDaySelect = $itemInput.find('select.dateDay'),
                $dataValue = $itemInput.next('.itemData').find('.dataValue');

            $inputText.on('change', function (e) {
                $($dataValue.get($inputText.index($(this)))).html($(this).val());
                $($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
                if (!$($dataValue.get($inputText.index($(this)))).html()) {
                    $($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
                    $($dataValue.get($inputText.index($(this)))).html('－－');
                }
            });

            $inputRadio.on('change', function (e) {
                $dataValue.html($(this).next('label').html());
                $dataValue.removeClass('dataValue-empty');
                if (!$dataValue.html()) {
                    $dataValue.html('－－');
                    $dataValue.addClass('dataValue-empty');
                }
            });

            $inputDateSelect.on('change', function (e) {
                $dataValue.html($inputDateYearSelect.val() + '-' + $inputDateMonthSelect.val() + '-' + $inputDateDaySelect.val());
                $dataValue.removeClass('dataValue-empty');
                if (!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()) {
                    $dataValue.html('－－');
                    $dataValue.addClass('dataValue-empty');
                }
            });


        });

        // autocomplete for inputselect fields
        $('.inputselect').each(function () {
            var this_id = $(this).attr('id');
            if ($(this).length > 0) {
                $(this).autocomplete(
                    "../admission_form/ajax_get_suggestions.php",
                    {
                        delay: 3,
                        minChars: 1,
                        matchContains: 1,
                        extraParams: {'field': this_id},
                        autoFill: false,
                        overflow_y: 'auto',
                        overflow_x: 'hidden',
                        maxHeight: '200px'
                    }
                );
            }
        });

        window.checkAcademicPerformanceForm = (function (lifecycle) {
            var isValid = true;
            var $academicPerformanceForm = $('#academicPerformanceForm');

            /******** Basic init START ********/
            /******** Basic init END ********/

            /**** Check required START ****/
            isValid = isValid && checkInputRequired($academicPerformanceForm);


            /**** Check required END ****/

            if (focusElement) {
                focusElement.focus();
            }

            return isValid;
        });

        window.validateFunc['pagePersonalInfo'].push(checkAcademicPerformanceForm);
    });
</script>