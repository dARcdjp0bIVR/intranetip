<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/thickbox.css">
<script type="text/javascript" src="/templates/jquery/thickbox-compressed.js"></script>
<script src="/templates/jquery/jquery.cropit.js" type="text/javascript" charset="utf-8"></script>

<section id="docsForm"
         class="form <?= (!$IsConfirm) ? 'displaySection display_pageDocsUpload display_pageConfirmation' : '' ?>" style="display: none;">
    <div class="form-header marginB10">
        <?= $LangB5['Admission']['document'] ?> <?= $LangEn['Admission']['document'] ?>
    </div>
    <div class="sheet">
        <div class="text-inst">
            <?= $LangB5['Admission']['UCCKE']['msg']['birthCertFormat'] ?> <?= $LangEn['Admission']['UCCKE']['msg']['birthCertFormat'] ?>
            <?= ($admission_cfg['maxUploadSize'] ? $admission_cfg['maxUploadSize'] : '1') ?> MB
        </div>
        <div class="item">
            <div class="itemLabel <?= $lac->isInternalUse($_GET['token']) ? '' : 'requiredLabel' ?>"><?= $LangB5['Admission']['personalPhoto'] ?> <?= $LangEn['Admission']['personalPhoto'] ?></div>
            <div class="uploadedFiles">
                <?= $viewFilePath ?>
                <div class="uploadedFile" style="display:none">
                    <span class="link"></span><span class="icon icon-button fa-trash"></span>
                </div>
                <?php
                $display = ($IsConfirm && $_FILES['CropStudentPersonalPhoto']) ? '' : 'display:none';
                ?>
                <img id="CropPreviewStudentPersonalPhoto" src="<?= $_FILES['CropStudentPersonalPhoto'] ?>"
                     style="margin-bottom:5px;width: 40mm;height: 50mm;<?= $display ?>"/>
                <br id="CropPreviewNewLineStudentPersonalPhoto" style="<?= $display ?>"/>
                <input type="button" class="button button-secondary formbutton cropImage"
                       id="CropEditStudentPersonalPhoto" data-target="StudentPersonalPhoto"
                       value="<?= $LangB5['Admission']['mgf']['cropPhoto'] ?> <?= $LangEn['Admission']['mgf']['cropPhoto'] ?>"
                       style="margin-left: 5px;display:none;">
                <input type="hidden" id="CropStudentPersonalPhoto" name="CropStudentPersonalPhoto"/>
            </div>
            <? if (!$IsConfirm) { ?>
                <div class="itemInput itemInput-file">
                    <input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto"
                           data-multiple-caption="<?= $LangB5['Admission']['fileSelected'] ?> <?= $LangEn['Admission']['fileSelected'] ?>" <?= $viewFilePath || $lac->isInternalUse($_GET['token']) ? '' : 'required' ?>
                           accept=".gif, .jpeg, .jpg, .png"/>
                    <label for="StudentPersonalPhoto">
                        <?php
                        if ($viewFilePath) {
                            echo "{$LangB5['Admission']['updateFile']} {$LangEn['Admission']['updateFile']}";
                        } else {
                            echo "{$LangB5['Admission']['selectFile']} {$LangEn['Admission']['selectFile']}";
                        }
                        ?>
                    </label>
                    <div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
                    <div class="remark remark-warn errFile hide"></div>
                </div>
            <? } ?>
        </div>
        <?php
        foreach ($attachment_settings as $index => $attachment_setting):
            $hasUploadedFile = is_file($intranet_root . "/file/admission/" . $applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$index]['AttachmentName']]['attachment_link'][0]);
            $labelClass = ($hasUploadedFile || $attachment_setting['IsOptional'] || $lac->isInternalUse($_GET['token'])) ? '' : 'requiredLabel';
            $required = ($hasUploadedFile || $attachment_setting['IsOptional'] || $lac->isInternalUse($_GET['token'])) ? '' : 'required';
            $accept = '.gif, .jpeg, .jpg, .png';
            $allowCrop = false;
            ?>
            <div class="item">
                <div class="itemLabel <?= $labelClass ?>"><?= $attachment_setting['AttachmentName'] ?></div>
                <div class="uploadedFiles">
                    <?php if ($hasUploadedFile) { ?>
                        <a href="../admission_form/download_attachment.php?type=<?= $attachment_setting['AttachmentName'] ?>"
                           target="_blank">檢視已遞交的檔案 View submitted file</a></br>
                    <?php } ?>
                    <div class="uploadedFile" style="display:none">
                        <span class="link"></span><span class="icon icon-button fa-trash"></span>
                    </div>
                    <img id="PreviewOtherFile<?= $index ?>" src="<?= $_FILES['OtherFile' . $index] ?>"
                         style="margin-bottom:5px;max-width: 400px;max-height: 400px;display:none;"/>
                    <?php
                    if ($allowCrop) {
                        $display = ($IsConfirm && $_FILES['CropOtherFile' . $i]) ? '' : 'display:none';
                        ?>
                        <img id="CropPreviewOtherFile<?= $index ?>" src="<?= $_FILES['CropOtherFile' . $index] ?>"
                             style="margin-bottom:5px;width: 322px;height: 54mm;<?= $display ?>"/>
                        <br id="CropPreviewNewLineOtherFile<?= $index ?>" style="<?= $display ?>"/>
                        <input type="button" class="button button-secondary formbutton cropImage"
                               id="CropEditOtherFile<?= $index ?>" data-target="OtherFile<?= $index ?>"
                               value="<?= $LangB5['Admission']['mgf']['cropPhoto'] ?> <?= $LangEn['Admission']['mgf']['cropPhoto'] ?>"
                               style="margin-left: 5px;display:none;">
                        <input type="hidden" id="CropOtherFile<?= $index ?>" name="CropOtherFile<?= $index ?>"/>
                    <?php } ?>
                </div>
                <?
                if (!$IsConfirm) {
                    ?>
                    <div class="itemInput itemInput-file">
                        <input type="file" name="OtherFile<?= $index ?>" id="OtherFile<?= $index ?>" class="OtherFile"
                               data-multiple-caption="<?= $LangB5['Admission']['fileSelected'] ?> <?= $LangEn['Admission']['fileSelected'] ?>" <?= $required ?>
                               accept="<?= $accept ?>"/>
                        <label for="OtherFile<?= $index ?>">
                            <?php
                            if ($hasUploadedFile) {
                                echo "{$LangB5['Admission']['updateFile']} {$LangEn['Admission']['updateFile']}";
                            } else {
                                echo "{$LangB5['Admission']['selectFile']} {$LangEn['Admission']['selectFile']}";
                            }
                            ?>
                        </label>
                        <div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
                        <div class="remark remark-warn errFile hide"></div>
                    </div>
                <?
                } ?>
            </div>
        <?php
        endforeach;
        ?>

        <div class="remark">
            * <?= $LangB5['Admission']['requiredFields'] ?> <?= $LangEn['Admission']['requiredFields'] ?></div>
    </div>
</section>

<style>
    /**** Image Cropper START ****/
    .cropit-preview {
        width: 322px;
        height: 54mm;
        margin: 25px auto;
    }

    .cropit-preview-container {
        border: 1px solid lightgrey;
    }

    .cropit-preview-image-container {
        cursor: move;
        border: 1px solid red;
    }

    .cropit-preview-image-container img {
        max-width: none !important;
    }

    input.cropit-image-input {
        display: none;
    }

    input.cropit-image-zoom-input {
        position: relative;
    }

    input.cropit-image-zoom-input[disabled] {
        cursor: not-allowed;
    }

    #image-cropper {
        overflow: hidden;
    }

    .image-control {
        text-align: center;
        padding-bottom: 5px;
    }

    .cropit-preview-background {
        opacity: .2;
        max-width: none !important;
    }

    .rotateBtn {
        font-size: 2em;
        color: #737373;
        cursor: pointer;
    }

    .select-image-btn {
        margin-top: 5px;
    }

    /**** Image Cropper END ****/

    /**** Save START ****/
    #saveControlBtn {
        margin-top: 10px;
        padding: 10px;
        text-align: center;
    }

    /**** Save END ****/
</style>
<div id="cropPhotoDiv" class="dialog-container hide">
	<span class="dialog dialog-yesno" id="blkCancel">
		<!-- This wraps the whole cropper -->
		<div id="image-cropper">
			<div class="cropit-preview-container">
	    		<!-- This is where the preview image is displayed -->
				<div class="cropit-preview"></div>
			</div>

			<div class="image-control">
	    		<!-- This range input controls zoom -->
                <!-- You can add additional elements here, e.g. the image icons -->
	    		<input type="range" class="cropit-image-zoom-input"/>

                <!-- This is where user selects new image -->
	    		<span class="rotateBtn rotate-ccw-btn"><i class="fa fa-rotate-left" aria-hidden="true"></i></span>
	    		<span class="rotateBtn rotate-cw-btn"><i class="fa fa-rotate-right" aria-hidden="true"></i></span>
	    		<input type="file" class="cropit-image-input"/>
                <!--button
	    			type="button"
	    			class="formsubbutton select-image-btn"
	    		>
	    			<?= $Lang['Admission']['mgf']['uploadNewImage'] ?>
	    		</button-->
			</div>
		</div>
		<div class="dialog-buttons">
			<span id="saveCropImage"
                  class="closeDialog-button button "><?= $LangB5['Btn']['Confirm'] ?> <?= $LangEn['Btn']['Confirm'] ?></span>
			<span class="closeDialog-button button button-secondary"><?= $LangB5['Btn']['Cancel'] ?> <?= $LangEn['Btn']['Cancel'] ?></span>
		</div>
	</span>
</div>

<script>
    $(function () {
        'use strict';

        $('.OtherFile').change(function (ev) {
            var otherFile = $(this);
            var id = $(this).attr('id');
            setTimeout(function () {
                if (ev.target.files.length == 1) {
                    var fr = new FileReader();
                    fr.onload = function () {
                        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                        if ($.inArray(ev.target.files[0]["type"], ValidImageTypes) > -1) {
                            $('#Preview' + id).attr('src', fr.result).show();
                        }
                    }
                    fr.readAsDataURL(ev.target.files[0]);
                }

                $('#Preview' + id).hide();
            }, 500);
        });
        $('.OtherFile, #StudentPersonalPhoto').change(function (ev) {
            var otherFile = $(this);
            var id = $(this).attr('id');

            setTimeout(function () {
                if ($('#CropEdit' + id).length == 0) {
                    return;
                }

                if (ev.target.files.length == 1) {
                    var fr = new FileReader();
                    fr.onload = function () {
                        $('#CropPreview' + id).attr('src', fr.result).show();
                        $('#Crop' + id).val(fr.result);
                        $('#CropPreviewNewLine' + id).show();
                    }
                    fr.readAsDataURL(ev.target.files[0]);
                }

                $('#Crop' + id).val('');

                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if (otherFile.val() == '' || $.inArray(ev.target.files[0]["type"], ValidImageTypes) == -1) {
                    $('#CropEdit' + id).hide();
                } else {
                    $('#CropEdit' + id).val('<?=$LangB5['Admission']['mgf']['cropPhoto'] ?> <?=$LangEn['Admission']['mgf']['cropPhoto'] ?>');
                    $('#CropEdit' + id).show();
                }
                $('#CropPreview' + id).hide();
                $('#CropPreviewNewLine' + id).hide();
            }, 500);
        });


        var currentCropId = '';

        var $imageCropper = $('#image-cropper');
        $imageCropper.cropit({
            imageBackground: true,
            imageBackgroundBorderWidth: 25,
            smallImage: 'stretch',
            maxZoom: 1.5
        });

        $('.rotate-cw-btn').click(function () {
            $('#image-cropper').cropit('rotateCW');
        });
        $('.rotate-ccw-btn').click(function () {
            $('#image-cropper').cropit('rotateCCW');
        });


        $('.cropImage').click(function (e) {
            currentCropId = $(this).data('target');
            if ($(this).attr('id') == 'CropEditStudentPersonalPhoto') {
                $(".cropit-preview").css("width", "40mm");
                $(".cropit-preview").css("height", "50mm");
                $('#image-cropper').cropit('previewSize', {width: '151', height: '189'});
            } else {
                $(".cropit-preview").css("width", "322px");
                $(".cropit-preview").css("height", "54mm");
                $('#image-cropper').cropit('previewSize', {width: '322', height: '204'});
            }
            if ($('#Crop' + currentCropId).val()) {
                $imageCropper.cropit('imageSrc', $('#Crop' + currentCropId).val());
                $('#cropPhotoDiv').removeClass("hide").show();
            }
            return false; // Prevent open default upload behaviour
        });
        $('#saveCropImage').click(function () {
            var newPhotoString = $imageCropper.cropit('export', {
                type: 'image/jpeg',
                quality: 1,
                originalSize: true
            });
            $('#Crop' + currentCropId).val(newPhotoString);
            $('#CropPreview' + currentCropId).attr('src', newPhotoString).show();
            $('#CropPreviewNewLine' + currentCropId).show();
        });

        $('.uploadedFile .fa-trash').each(function () {
            var $span = $(this),
                $input = $span.parent().parent().next('.itemInput-file').children('input');

            $span.on('click', function (e) {
                $('#CropPreview' + $input.get(0).name).removeAttr('src').hide();
                $('#CropPreviewNewLine' + $input.get(0).name).hide();
                $input.val("");
                $input.change();
            });
        });

        var isOldBrowser = 0;
        if (navigator.appName.indexOf("Internet Explorer") != -1 && navigator.appVersion.indexOf("MSIE 1") == -1) {
            isOldBrowser = 1;
        }

        var maxFileSize = 1 * 1024 * 1024;
        <?if($admission_cfg['maxUploadSize'] > 0){?>
        maxFileSize =<?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
        <?}?>

        $('.itemInput-file input[type="file"]').each(function () {
            var $input = $(this),
                $uploadedFileDiv = $input.parent().prev('.uploadedFiles').children('.uploadedFile'),
                $label = $input.next('label'),
                $link = $uploadedFileDiv.children('.link'),
                linkVal = $link.html();

            $input.on('change', function (e) {
                var fileName = '';

                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else if (e.target.value)
                    fileName = e.target.value.split('\\').pop();

                if (fileName) {
                    $input.parent().find('.remark-warn').addClass('hide');
                    /**** File size START ****/
                    var filesize = ($(this).val() == '') ? 0 : $(this)[0].files[0].size;
                    if (!isOldBrowser && filesize > maxFileSize) {
                        if ($input.parent().find('.remark-warn.errFile').hasClass('hide')) {
                            $input.parent().find('.remark-warn.errFile').html('<?=$LangB5['Admission']['msg']['FileSizeExceedLimit']?> <?=$LangEn['Admission']['msg']['FileSizeExceedLimit']?>');
                            $input.parent().find('.remark-warn.errFile').removeClass('hide');
                            focusElement = $input;
                        }
                        $input.val("");
                        return false;
                    }
                    /**** File size END ****/

                    /**** File format START ****/
                    var _acceptFormatArr = $(this).attr('accept').split(',');
                    var acceptFormatArr = [];
                    $.each(_acceptFormatArr, function () {
                        acceptFormatArr.push($.trim(this).substr(1));
                    });

                    var ext = $(this).val().split('.').pop().toLowerCase();
                    if ($.inArray(ext, acceptFormatArr) == -1) {
                        if ($input.parent().find('.remark-warn.errFile').hasClass('hide')) {
                            $input.parent().find('.remark-warn.errFile').html('<?=$LangB5['Admission']['msg']['invalidfileformat']?> <?=$LangEn['Admission']['msg']['invalidfileformat']?>');
                            $input.parent().find('.remark-warn.errFile').removeClass('hide');
                            focusElement = $input;
                        }
                        $input.val("");
                        return false;
                    }
                    /**** File format END ****/

                    $input.parent('.itemInput-file').hide();
                    $uploadedFileDiv.show();
                    $uploadedFileDiv.parent().find('a, br').hide();
                    $link.html(fileName);
                } else {
                    $input.parent('.itemInput-file').show();
                    $uploadedFileDiv.hide();
                    $uploadedFileDiv.parent().find('a, br').show();
                    $link.html(linkVal);
                }
            });
        });

        <?if(!$IsConfirm){?>
        window.checkDocsForm = (function (lifecycle) {
            var isValid = true;
            var $docsForm = $('#docsForm');

            /**** Check required START ****/
            isValid = isValid && checkInputRequired($docsForm);

            var isUpdatePeriod =<?=($allowToUpdate) ? 'true' : 'false'; ?>;
            var isTeacherInput =<?= ($lac->isInternalUse($_GET['token'])) ? 1 : 0 ?>;
            var isOldBrowser = 0;
            if (navigator.appName.indexOf("Internet Explorer") != -1 && navigator.appVersion.indexOf("MSIE 1") == -1) {
                isOldBrowser = 1;
            }

            var maxFileSize = 1 * 1024 * 1024;
            <?if($admission_cfg['maxUploadSize'] > 0){?>
            maxFileSize =<?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
            <?}?>

            var $files = $('#StudentPersonalPhoto, input[type=file][name*=OtherFile]');

            $files.each(function () {
                var teacherIsOptional = !!$(this).data('teacher-is-optional');
                var isOptional = !!$(this).data('is-optional');

                if (isTeacherInput && teacherIsOptional) {
                    isOptional = true;
                }

                if (!$(this).parent().find('.remark-warn.errFile').hasClass('hide')) {
                    $(this).parent().find('.remark-warn.errFile').addClass('hide');
                }

                /**** File size START **** /
                 var filesize = ($(this).val()=='')? 0 : $(this)[0].files[0].size;
                 if(!isOldBrowser && filesize > maxFileSize){
				if($(this).parent().find('.remark-warn .errFile').hasClass('hide')){
					$(this).parent().find('.remark-warn .errFile').html('<?=$LangB5['Admission']['msg']['FileSizeExceedLimit']?> <?=$LangEn['Admission']['msg']['FileSizeExceedLimit']?>');
					$(this).parent().find('.remark-warn .errFile').removeClass('hide');
					focusElement = $(this);
				}
		    	isValid = false;
			}
                 /**** File size END ****/

                /**** File format START **** /
                 var _acceptFormatArr = $(this).attr('accept').split(',');
                 var acceptFormatArr = [];
                 $.each(_acceptFormatArr, function(){
				acceptFormatArr.push( $.trim(this).substr(1) );
			});

                 var ext = $(this).val().split('.').pop().toLowerCase();
                 if($.inArray(ext, acceptFormatArr) == -1){
				if($(this).parent().parent().find('.remark-warn.errFile').hasClass('hide')){
					$(this).parent().parent().find('.remark-warn.errFile').html('<?=$LangB5['Admission']['msg']['invalidfileformat']?> <?=$LangEn['Admission']['msg']['invalidfileformat']?>');
					$(this).parent().parent().find('.remark-warn.errFile').removeClass('hide');
					focusElement = $(this);
				}
		    	isValid = false;
			}
                 /**** File format END ****/
            });

            return isValid;
        });

        window.validateFunc['pageDocsUpload'].push(checkDocsForm);
        <?}?>
    });
</script>