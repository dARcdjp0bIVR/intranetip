<?php
global $libkis_admission;
$recordAry = $libkis_admission->getApplicationParentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
$recordCount = count($recordAry);
$kis_data['applicationInfo'] = array ();
for ($i = 0; $i < $recordCount; $i++) {
    $_type = $recordAry[$i]['type'];
    $kis_data['applicationInfo']['applicationID'] = $recordAry[$i]['applicationID'];
    $kis_data['applicationInfo']['classLevelID'] = $kis_data['applicationInfo']['classLevelID'] ? $kis_data['applicationInfo']['classLevelID'] : $recordAry[$i]['classLevelID'];
    $kis_data['applicationInfo'][$_type] = $recordAry[$i];
}

$ParentInfo = $kis_data['applicationInfo'];
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$parentTypeArr = array('F','M','G');
$totalField = count($fields) / 3;
?>
<?=$this->generateValidateJs($fields); ?>

<style>
.hide{
    display:none;
}
.itemInput .remark {
    margin-top: 5px;
}
.itemInput .remark.remark-warn {
    color: #dd2c00;
}
.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%">
		<?php foreach($parentTypeArr as $parentType): ?>
			<col style="">
		<?php endforeach; ?>
	</colgroup>
	<tr>
		<td>&nbsp;</td>
		<?php foreach($parentTypeArr as $parentType): ?>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type'][$parentType] ?></center></td>
		<?php endforeach; ?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['chinesename']?>
		</td>
		<?php
		foreach($parentTypeArr as $index => $parentType):
    		$field = $fields[($totalField * $index) + 1];
    		$value = $ParentInfo[$parentType]['ChineseName'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['englishname']?>
		</td>
		<?php
		foreach($parentTypeArr as $index => $parentType):
    		$field = $fields[($totalField * $index) + 2];
    		$value = $ParentInfo[$parentType]['EnglishName'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['occupation']?>
		</td>
		<?php
		foreach($parentTypeArr as $index => $parentType):
    		$field = $fields[($totalField * $index) + 3];
    		$value = $ParentInfo[$parentType]['Company'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['companyaddress']?>
		</td>
		<?php
		foreach($parentTypeArr as $index => $parentType):
    		$field = $fields[($totalField * $index) + 4];
    		$value = $ParentInfo[$parentType]['OfficeAddress'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['OfficeTel']?>
		</td>
		<?php
		foreach($parentTypeArr as $index => $parentType):
    		$field = $fields[($totalField * $index) + 5];
    		$value = $ParentInfo[$parentType]['OfficeTelNo'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['MobileTel']?>
		</td>
		<?php
		foreach($parentTypeArr as $index => $parentType):
    		$field = $fields[($totalField * $index) + 6];
    		$value = $ParentInfo[$parentType]['Mobile'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();

	window.scrollTo(0,0);
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
  var data = $(this).serialize()
  check_input_info().then(function(){
            $.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function(success){
                $.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
            });
      });

	return false;
});
</script>
