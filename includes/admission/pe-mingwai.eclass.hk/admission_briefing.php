<?php

class admission_briefing extends admission_briefing_base{
    function insertBriefingApplicant($schoolYearID, $Data){
        global $sys_custom;
        extract($Data);
         
        #### Get ApplicantID START ####
        $ApplicantID = $ApplicationID;
        #### Get ApplicantID END ####
    
        #### Insert Record START ####
		$HTTP_USER_AGENT = addslashes($_SERVER['HTTP_USER_AGENT']);
		$REMOTE_ADDR = addslashes($_SERVER['REMOTE_ADDR']);
		$HTTP_X_FORWARDED_FOR = addslashes($_SERVER['HTTP_X_FORWARDED_FOR']);
        $sql = "INSERT INTO
        ADMISSION_BRIEFING_APPLICANT_INFO
        (
            BriefingID,
            Email,
            ApplicantID,
            BirthCertNo,
            ParentName,
            StudentName,
            StudentGender,
            Kindergarten,
            PhoneNo,
            SeatRequest,
            DateInput,
			HTTP_USER_AGENT,
			REMOTE_ADDR,
			HTTP_X_FORWARDED_FOR
        ) VALUES (
            '{$BriefingID}',
            '{$Email}',
            '{$ApplicantID}',
            '{$BirthCertNo}',
            '{$ParentName}',
            '{$StudentName}',
            '{$StudentGender}',
            '{$Kindergarten}',
            '{$PhoneNo}',
            '{$SeatRequest}',
            now(),
			'{$HTTP_USER_AGENT}',
			'{$REMOTE_ADDR}',
			'{$HTTP_X_FORWARDED_FOR}'
        )";
        
        $result = $this->db_db_query($sql);
        #### Insert Record END ####
    
        #### Update Delete Pass Key START ####
        $Briefing_ApplicantID = $this->db_insert_id();
    
        $sql = "SELECT DeleteRecordPassKey FROM ADMISSION_BRIEFING_APPLICANT_INFO";
        $DeleteRecordPassKeyArr = $this->returnVector($sql);
        $i = 0;
        do{
            $DeleteRecordPassKey = md5($Briefing_ApplicantID . $ApplicantID . $Email . $i++);
        }while(in_array($DeleteRecordPassKey, $DeleteRecordPassKeyArr));
    
        $sql = "UPDATE
            ADMISSION_BRIEFING_APPLICANT_INFO
        SET
            DeleteRecordPassKey='{$DeleteRecordPassKey}'
        WHERE
            Briefing_ApplicantID='{$Briefing_ApplicantID}'";
        $result = $result && $this->db_db_query($sql);
        #### Update Delete Pass Key END ####
    
    
        #### Delete record if insert not success START ####
        if(!$result){
            $this->deleteBriefingApplicant($Briefing_ApplicantID, true);
            return 0;
        }
        #### Delete record if insert not success END ####
    
        return $Briefing_ApplicantID;
    }
    
	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = ''){
		if($Briefing_ApplicantID == 0){
			return parent::sendMailToNewApplicant($Briefing_ApplicantID, $subject, $message, $email);
		}
		
		$applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
		$ApplicantID = $applicant['ApplicantID'];
		$deleteRecordLink = $this->getApplicantDeleteLink($Briefing_ApplicantID, $applicant['DeleteRecordPassKey']);
		$BriefingID = $applicant['BriefingID'];
		$briefingSession = $this->getBriefingSession($BriefingID);
		
		#### Email Date START ####
		$_starttime = strtotime($briefingSession['BriefingStartDate']);
		$_endtime = strtotime($briefingSession['BriefingEndDate']);
		$_endDay = date('h:ia', $_endtime);
		## Chinese START ##
		$weekDayArr = array('日','一','二','三','四','五','六');
		$meridiemArr = array('AM' => '上午', 'PM' => '下午');
		
		$_weekDay = $weekDayArr[ date('w', $_starttime) ];
		$BriefingDateChi = date('Y年n月j日(星期%1)', $_starttime);
		$BriefingDateChi = str_replace('%1', $_weekDay, $BriefingDateChi);
		$BriefingTimeChi = date('%1 g時i分', $_starttime);
		$meridiem = $meridiemArr[date('A', $_starttime)];
		$BriefingTimeChi = str_replace('%1', $meridiem, $BriefingTimeChi);
		## Chinese END ##
		
		## English START ##
		$BriefingDateEng = date('j F, Y(l)', $_starttime);
		$BriefingTimeEng = date('g:ia', $_starttime);
		
		## English END ##
		#### Email Date END ####
		
		@ob_start();
?>
<pre>
<font color="green">申請完成。請記錄你的申請編號 <font size="5"><u><?=$ApplicantID?></u></font></font>

多謝家長報名參加本園簡介會，請注意下列事項
<ol>
<li>申請人姓名: <?=$applicant['StudentName']?></li>
<li>申請人之出生証明文件編號: <?=$applicant['BirthCertNo']?></li>
<li>日期: <?=$BriefingDateChi?></li>
<li>時間: <?=$BriefingTimeChi?> (時間不設修改)</li>
<li>出席人數: <?=$applicant['SeatRequest']?></li>
<li>地點: 九龍運動場道15號，京華大廈地下3-9號地下及一樓</li>
</ol>
請家長自行列印此表，並於入場前出示。<br /><br />
簡介會申請一經刪除，不可作出修改或從新申請，將會以放棄參加簡介會論。<br /><br />
如需要刪除申請，請按此連結： <?=$deleteRecordLink?> 
</pre>
<br />
<pre>
<font color="green">Application is Completed.Your application number is <font size="5"><u><?=$ApplicantID?></u></font></font>

Thank you for enrolling our briefing sessions, please note:
<ol>
<li>Name of applicant: <?=$applicant['StudentName']?></li>
<li>Applicant's Birth Certificate No: <?=$applicant['BirthCertNo']?></li>
<li>Date: <?=$BriefingDateEng?></li>
<li>Time: <?=$BriefingTimeEng?> (Time slot cannot be changed)</li>
<li>Number of attendants: <?=$applicant['SeatRequest']?></li>
<li>Venue: Shop 3-9, G/F, & 1/F, Kenwood Mansion, 15 Playing Field Road, Kowloon</li>
</ol>
Parents please print out this confirmation and present it upon arrival.<br /><br />
Application cannot be modified or re-submitted once cancellation is done and it will be abandoned.<br /><br />
If you want to cancel the application, click this link: <?=$deleteRecordLink?>
</pre>
<?php	
		$content = ob_get_clean();
		return parent::sendMailToNewApplicant($Briefing_ApplicantID, $subject, $message = $content, $email);
	}
	
} // End Class