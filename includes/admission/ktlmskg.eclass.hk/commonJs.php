<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">

kis.datepicker('#StudentDateOfBirth');	

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>\nThe time is up! Please apply again!');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>/nThe time is up!\nThe admission form will auto submit after pressing \'OK\'!");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	//For debugging only
	//return true;
	
	// borower version checking if browser is IE9 or below or not
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	//for email validation
	var re = /\S+@\S+\.\S+/;

	//allow file size checking if the  
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.studentssurname_b5.value==''){
		return false;
	} else if(form1.studentsfirstname_b5.value==''){
		return false;
	} else if(form1.studentssurname_en.value==''){
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		return false;
	} else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		form1.StudentDateOfBirth.focus();
		return false;
	} else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		return false;
	} 
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		return false;
	} else if(form1.StudentPlaceOfBirth.value==''){
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		return false;
	} else if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
		return false;
	} else if(checkBirthCertNo() > 0){
		return false;
	} else if(form1.StudentHomePhoneNo.value==''){
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.StudentHomePhoneNo.value)){
		return false;
	} else if(form1.StudentHomeAddress.value==''){
		return false;
	} else if(form1.StudentEmail.value==''){
		return false;
	} else if(!re.test(form1.StudentEmail.value)){
		return false;
	} else if(form1.G1ChineseName.value=='' && form1.G2ChineseName.value==''){ //for parent info
			return false;
	}
	if(form1.G1ChineseName.value!=''){
		if(form1.G1MobileNo.value==''){
			return false;
		} else if(!/^[0-9]{8}$/.test(form1.G1MobileNo.value)){
			return false;
		} else if(form1.G1Email.value!='' && !re.test(form1.G1Email.value)){
			return false;
		}
	}
	if(form1.G2ChineseName.value!=''){
		if(form1.G2MobileNo.value==''){
			return false;
		} else if(!/^[0-9]{8}$/.test(form1.G2MobileNo.value)){
			return false;
		} else if(form1.G2Email.value!='' && !re.test(form1.G2Email.value)){
			return false;
		}
	}
	
	//for other information
//	if(form1.OthersApplyDayType2 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value ==""){
//		//else if(form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value){
//		return false; 
//	}
//	else 
	if($('input:radio[name=OthersIsConsiderAlternative]:checked').val() == null){
		return false;
	}
	else if(form1.OthersPrevSchYear1.value==''){
		return false;
	}
	else if(form1.OthersPrevSchClass1.value==''){
		return false;
	}
	else if(form1.OthersPrevSchName1.value==''){
		return false;
	}
	return true;
}
function check_docs_upload2(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	//allow file size checking if the  
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	
	if(form1.StudentPersonalPhoto.value==''){
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		return false;
	}
	var temp_count = (file_count > 1?1:file_count);
	for(var i=0;i<temp_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			return false;
		}
	}
	if(file_count > 1){
	for(var i=1;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(otherFileVal!=''){
			if(!isOldBrowser){
				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
			}
			if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
				return false;
			} else if(!isOldBrowser && otherFileSize > maxFileSize){
				return false;
			}
		}
	}
	}
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
//		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
//	        alert("表格需以 Google Chrome、Firefox 或 Internet Explorer 10 或以上瀏覽器填寫。");
//	        return;
//	    }
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		//alert($("form").serialize());
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
			var chk_ary = $('#step_instruction input[type=checkbox]');
			var chk_count = chk_ary.length;
			for(var i=0;i<chk_count;i++)
			{
				var chk_element = chk_ary.get(i);
				if(chk_element.checked == false){
					alert("Please tick I have read and understand the statement. I am ready to proceed application online.\n請剔選 我已閱讀並理解以上資料，已準備繼續進行網上報名程序。");
					isValid = false;
				}
			}
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
			   clearTimeout(timer);
			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();	 
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
		   
	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){
//	if(checkInterviewQuotaLeft() <= 0 ){
//		alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
//		goto('step_confirm','step_input_form');
//		form1.InterviewSettingID.focus();
//		return false;
//	}
	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	return confirm('<?=$Lang['Admission']['munsang']['msg']['suresubmit']?>\nPlease check the information given again, it cannot edit after submitted.');
}

function check_choose_class(form1) {

	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>");
				return false;
			}
		}
	<?}?>
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>\nPlease select Class.");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {

	//for email validation
	var re = /\S+@\S+\.\S+/;

	//////// Student Information ////////
	if(form1.studentssurname_b5.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentssurname_b5']?>\nPlease enter Surname.");	
		form1.studentssurname_b5.focus();
		return false;
	} else if(form1.studentsfirstname_b5.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentsfirstname_b5']?>\nPlease enter First Name.");
		form1.studentsfirstname_b5.focus();
		return false;
	} else if(form1.studentssurname_en.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentssurname_en']?>\nPlease enter Surname.");	
		form1.studentssurname_en.focus();
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentsfirstname_en']?>\nPlease enter First Name.");
		form1.studentsfirstname_en.focus();
		return false;
	} else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.StudentDateOfBirth.value!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>\nInvalid Birthday Range of Student");
		form1.StudentDateOfBirth.focus();
		return false;
	} 
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>\nPlease select Gender.");	
		form1.StudentGender[0].focus();
		return false;
	} else if(form1.StudentPlaceOfBirth.value==''){
		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>\nPlease enter Place of Birth.");	
		form1.StudentPlaceOfBirth.focus();
		return false;
	} else if(form1.Nationality.value==''){
		alert("<?=$Lang['Admission']['icms']['msg']['enternationality']?>\nPlease enter Nationality.");	
		form1.Nationality.focus();
		return false;
	} else if($('#BirthCertType2').attr('checked') == 'checked' && $('#BirthCertTypeOther').val() == ''){
		alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterBirthCertTypeOther']?>\nPlease enter Type of Identity Document.");	
		$('#BirthCertTypeOther').focus();
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterIdNum']?>\nPlease enter Birth Certificate Number.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(
		$('#BirthCertType1').attr('checked') == 'checked' && 
		!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)
	){
		alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['munsang']['msg']['duplicatebirthcertificatenumber']?>\nThe Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(form1.StudentHomeAddress.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>\nPlease enter Address");	
		form1.StudentHomeAddress.focus();
		return false;
	} else if(form1.StudentHomePhoneNo.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterphone']?>\nPlease enter Telephone.");	
		form1.StudentHomePhoneNo.focus();
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.StudentHomePhoneNo.value)){
		alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
		form1.StudentHomePhoneNo.focus();
		return false;
	} else if(form1.StudentEmail.value==''){
		alert("<?=$Lang['Admission']['msg']['enterstudentemail']?>\nPlease enter Contact Email.");	
		form1.StudentEmail.focus();
		return false;
	} else if(!re.test(form1.StudentEmail.value)){
		alert("<?=$Lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress']?>\nInvalid Contact Email.");	
		form1.StudentEmail.focus();
		return false;
	} else if( $('#twinsY').attr('checked') == 'checked' && $('#twinsapplicationid').val() == ''){
		alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterTwinsIdNum']?>\nPlease enter Twins' Number of Identity Document.");	
		form1.twinsapplicationid.focus();
		return false;
	} else if(form1.G1ChineseName.value=='' && form1.G2ChineseName.value==''){ //for parent info
			alert("<?=$Lang['Admission']['msg']['enteratleastoneparent']?>\nPlease enter at least one parent information.");
			form1.G1EnglishName.focus();
			return false;
	}
	
	//////// Parent Information ////////
	if(form1.G1ChineseName.value!=''){
		if(form1.G1EnglishName.value==''){
			alert("<?=$Lang['Admission']['msg']['enterenglishname'] ?>\nPlease enter English Name.");
			form1.G1EnglishName.focus();
			return false;
		}else if(form1.G1EducationLevel.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enterlevelofeducation'] ?>\nPlease enter Education Level.");
			form1.G1EducationLevel.focus();
			return false;
		}else if(form1.G1Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation']?>\nPlease enter Occupation.");
			form1.G1Occupation.focus();
			return false;
		}else if(form1.G1CompanyAddress.value==''){
			alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterCompanyAddress']?>\nPlease enter Company Address.");
			form1.G1CompanyAddress.focus();
			return false;
		}else if(form1.G1ContactNumber.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile'] ?>\nPlease enter Mobile Phone No.");
			form1.G1ContactNumber.focus();
			return false;
		}else if(!/^[0-9]{8}$/.test(form1.G1ContactNumber.value)){
			alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
			form1.G1ContactNumber.focus();
			return false;
		}
	}
	if(form1.G2ChineseName.value!=''){
		if(form1.G2EnglishName.value==''){
			alert("<?=$Lang['Admission']['msg']['enterenglishname'] ?>\nPlease enter English Name.");
			form1.G2EnglishName.focus();
			return false;
		}else if(form1.G2EducationLevel.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enterlevelofeducation'] ?>\nPlease enter Education Level.");
			form1.G2EducationLevel.focus();
			return false;
		}else if(form1.G2Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation']?>\nPlease enter Occupation.");
			form1.G2Occupation.focus();
			return false;
		}else if(form1.G2CompanyAddress.value==''){
			alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterCompanyAddress']?>\nPlease enter Company Address.");
			form1.G2CompanyAddress.focus();
			return false;
		}else if(form1.G2ContactNumber.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile'] ?>\nPlease enter Mobile Phone No.");
			form1.G2ContactNumber.focus();
			return false;
		}else if(!/^[0-9]{8}$/.test(form1.G2ContactNumber.value)){
			alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
			form1.G2ContactNumber.focus();
			return false;
		}
	}
	
	//////// Other Information ////////
	if($('#CurrentStudySchool').val() == ''){
		alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterPrevSchool']?>\nPlease enter Kindergarten Attending.");
		form1.CurrentStudySchool.focus();
		return false;
	}
	if($('#CurrentStudyClass').val() == ''){
		alert("<?=$Lang['Admission']['KTLMSKG']['msg']['enterPrevClass']?>\nPlease enter Class Attending.");
		form1.CurrentStudyClass.focus();
		return false;
	}
	
	if( $('#MeritClass').val() != '' && parseInt($('#MeritClass').val()) != $('#MeritClass').val() ){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidPlaceAttained']?>\nInvalid Place Attained. ');
		form1.MeritClass.focus();
		return false;
	}
	if( $('#MeritClassTotal').val() != '' && parseInt($('#MeritClassTotal').val()) != $('#MeritClassTotal').val() ){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidPlaceAttained']?>\nInvalid Place Attained. ');
		form1.MeritClassTotal.focus();
		return false;
	}
	if( $('#MeritForm').val() != '' && parseInt($('#MeritForm').val()) != $('#MeritForm').val() ){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidPlaceAttained']?>\nInvalid Place Attained. ');
		form1.MeritForm.focus();
		return false;
	}
	if( $('#MeritFormTotal').val() != '' && parseInt($('#MeritFormTotal').val()) != $('#MeritFormTotal').val() ){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidPlaceAttained']?>\nInvalid Place Attained. ');
		form1.MeritFormTotal.focus();
		return false;
	}
	if( 
		$('#fluency1').val() == $('#fluency2').val() ||
		$('#fluency2').val() == $('#fluency3').val() ||
		$('#fluency1').val() == $('#fluency3').val()
	){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidFluency']?>\nFluency in Languages must not be same.');
		form1.fluency1.focus();
		return false;
	}
	if($('input[name="MotherTongue"]:checked').length == 0){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterMotherTongue']?>\nPlease select a Mother Tongue.');
		$('#MotherTongue1').focus();
		return false;
	}
	
	//////// Application Fee ////////
	if($('#BankName').val() == ''){
		alert("<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterBankName']?>\nPlease enter Bank Name.");
		form1.BankName.focus();
		return false;
	}
	if($('#ChequeNo').val() == ''){
		alert("<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterChequeNo']?>\nPlease enter Cheque No.");
		form1.ChequeNo.focus();
		return false;
	}
	
	
	return true;	
}
function check_docs_upload(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	//allow file size checking if the  
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	
	if(form1.StudentPersonalPhoto.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>\nPlease upload a personal photo.");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
		form1.StudentPersonalPhoto.focus();
		return false;
	}
	var temp_count = file_count;
	for(var i=0;i<temp_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			alert("<?=$Lang['Admission']['msg']['uploadfile']?>\nPlease upload file.");
			file_element.focus();
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
			file_element.focus();
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
			file_element.focus();
			return false;
		}
	}
	if(file_count > 1){
	for(var i=1;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		var otherFileVal = file_element.value;
		if(otherFileVal!=''){
			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
			if(!isOldBrowser){
				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
			}
			if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
				alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
				file_element.focus();
				return false;
			} else if(!isOldBrowser && otherFileSize > maxFileSize){
				alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
				file_element.focus();
				return false;
			}
		}
	}
	}
//	if(checkInterviewQuotaLeft() <= 0 ){
//		alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
//		goto('step_docs_upload','step_input_form');
//		form1.InterviewSettingID.focus();
//		return false;
//	}
	return true;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

//function checkInterviewQuotaLeft(){
//	var values = $("#form1").serialize();
//	var res = null;
//	/* Check the quota of interview timeslot */
//   $.ajax({
//       url: "ajax_check_num_of_interview_quota.php",
//       type: "post",
//       data: values,
//       async: false,
//       success: function(data){
//           //alert("debugging: The classlevel is updated!"+values);
//           //$("#step_confirm").html(data);
////           if(data <= 0){
////		           	alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
////		           	return false;
//					res = data;
////		           }
//		       },
//		       error:function(){
//		           //alert("failure");
//           //$("#result").html('There is error while submit');
//       }
//   });
//   return res;	
//}

function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount > 4){
			document.getElementById('btn_addRow1').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow1').style.display = '';
		}
		 
	}
	else{
		if(rowCount > 4){
			document.getElementById('btn_addRow2').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow2').style.display = '';
		}
	}
	
	if(rowCount <= 5){                            // limit the user from creating fields more than your limits
		var row = table.insertRow(rowCount);
		var newcell = row.insertCell(0);
		newcell.className = "field_title";
		newcell.style.textAlign ="right";
		newcell.innerHTML = '('+rowCount+')';
		if(tableID == 'dataTable1'){
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchYear'+rowCount+'" type="text" id="OthersPrevSchYear'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchClass'+rowCount+'" type="text" id="OthersPrevSchClass'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchName'+rowCount+'" type="text" id="OthersPrevSchName'+rowCount+'" class="textboxtext" />';
		}
		else{
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedYear'+rowCount+'" type="text" id="OthersRelativeStudiedYear'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedName'+rowCount+'" type="text" id="OthersRelativeStudiedName'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeClassPosition'+rowCount+'" type="text" id="OthersRelativeClassPosition'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(4);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeRelationship'+rowCount+'" type="text" id="OthersRelativeRelationship'+rowCount+'" class="textboxtext" />';
		}
	}else{
		 //alert("Maximum Passenger per ticket is 5");
			   
	}
}

function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount < 4){
			document.getElementById('btn_deleteRow1').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow1').style.display = '';
		}
	}
	else{
		if(rowCount < 4){
			document.getElementById('btn_deleteRow2').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow2').style.display = '';
		}
	}
	
	if(rowCount <= 2) {               // limit the user from removing all the fields
		//alert("Cannot Remove all the Passenger.");				
	}
	else{
		table.deleteRow(rowCount - 1);	
	}
}

//function startUpload(){
//      document.getElementById('f1_upload_process').style.visibility = 'visible';
//      document.getElementById('f1_upload_form').style.visibility = 'hidden';
//      return true;
//}
//Henry modifying 20131028
//function stopUpload(temp_folder_name){
//		document.getElementById('tempFolderName').value = temp_folder_name;
//		
//		 var values = $("#form1").serialize();
//			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
//			var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
//			var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
//			values+=studentPersonalPhoto;
//			values+=otherFile;
//			values+=otherFile1;
//		$.ajax({
//		       url: "ajax_get_confirm.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!"+values);
//		           $("#step_confirm").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });     
//      return true;  
//}
</script>