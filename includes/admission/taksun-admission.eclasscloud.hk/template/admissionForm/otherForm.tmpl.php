<h1>
    <?=$Lang['Admission']['otherInfo']?>
</h1>

<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="70%">
</colgroup>

<?php if($isFirstYear): ?>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['TssExperienceDay']?> 
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		    if($formData['TssExperienceDay'] == 'Y'){
		?>
				<?=$Lang['Admission']['yes'] ?>
				<br />
				<?=$Lang['Admission']['TSS']['ExperienceDayNo'] ?>:
				<?=$formData['TssExperienceDayNo'] ?>
		<?php   
		    }else{
		        echo $Lang['Admission']['no'];
		    }
		}else{ 
		?>
			<div>
				<?php $checked = ($allCustInfo['TssExperienceDay'][0]['Value'] != 'Y')? 'checked' : '' ?>
    			<input type="radio" id="TssExperienceDay_N" name="TssExperienceDay" value="N" <?=$checked ?>/>
    			<label for="TssExperienceDay_N"><?=$Lang['Admission']['no'] ?></label>
			</div>
			<div>
				<?php $checked = ($allCustInfo['TssExperienceDay'][0]['Value'] == 'Y')? 'checked' : '' ?>
    			<input type="radio" id="TssExperienceDay_Y" name="TssExperienceDay" value="Y" <?=$checked ?>/>
    			<label for="TssExperienceDay_Y"><?=$Lang['Admission']['yes'] ?></label>
    			(
    				<label for="TssExperienceDayNo"><?=$Lang['Admission']['TSS']['ExperienceDayNo'] ?>:</label> 
    				<input name="TssExperienceDayNo" type="text" id="TssExperienceDayNo" value="<?=$allCustInfo['TssExperienceDayNo'][0]['Value']?>"/>
    			)
			</div>
		<?php } ?>
	</td>
</tr>
<?php endif; ?>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['EnterAcceptRemarks']?> 
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['AcceptRemarks']?>
		<?php }else{ ?>
			<input name="AcceptRemarks" type="text" id="AcceptRemarks" class="textboxtext" value="<?=$allCustInfo['AcceptRemarks'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

</table>

<script>
function check_other_form(){

	/**** TssExperienceDayNo START ****/
	if($('#TssExperienceDay_Y:checked').length == 1 && $('#TssExperienceDayNo').val().trim() == ''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterExperienceDayNo']?>");	
		$('#TssExperienceDayNo').focus();
		return false;
	}else if($('[name="TssExperienceDay"]:checked').length == 0){
		$('#TssExperienceDay_N').click();
	}
	/**** TssExperienceDayNo END ****/
	
	return true;
}
</script>