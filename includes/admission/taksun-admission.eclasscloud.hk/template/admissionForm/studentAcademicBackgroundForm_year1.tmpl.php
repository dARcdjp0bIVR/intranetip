<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}
.col2div{
    display:inline-block;
    width:48%;
}
@media (min-width: 768px) and (max-width: 991.98px) {
    .col2div{
        width:100%;
    }
}

.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>

<h1>
    <?=$Lang['Admission']['TSS']['AcademicRecord']?>
</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['EdbApplicationNo']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['EdbApplicationNo']?>
		<?php }else{ ?>
    		<input name="EdbApplicationNo" type="text" id="EdbApplicationNo" class="textboxtext"  value="<?=$allCustInfo['EdbApplicationNo'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['KindergartenName']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?= ($formData['CurrentSchoolName'] != ' -- ')? $formData['CurrentSchoolName'] : "{$LangB5['Admission']['TSS']['TakSunKindergarten']} {$LangEn['Admission']['TSS']['TakSunKindergarten']}" ?>
		<?php }else{ ?>
    		<input type="radio" id="KindergartenName_tsk" name="KindergartenName_type" value="tsk" <?= ($StudentPrevSchoolInfo[0]['NameOfSchool'])? '' : 'checked' ?> />
    		<label for="KindergartenName_tsk"><?=$LangB5['Admission']['TSS']['TakSunKindergarten'] ?> <?=$LangEn['Admission']['TSS']['TakSunKindergarten'] ?></label>
    		<br />

    		<input type="radio" id="KindergartenName_other" name="KindergartenName_type" value="other" <?= ($StudentPrevSchoolInfo[0]['NameOfSchool'])? 'checked' : '' ?> />
    		<label for="KindergartenName_other"><?=$Lang['Admission']['TSS']['Others'] ?></label>
    		<label for="CurrentSchoolName">
    			(
    			<?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>:
				<input name="CurrentSchoolName" type="text" id="CurrentSchoolName" class="textboxtext" style="width: 400px;" value="<?=$StudentPrevSchoolInfo[0]['NameOfSchool']?>"/>
    			)
    		</label>
		<?php } ?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['district']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['TSS']['Districts'][ $formData['CurrentSchoolAddress'] ];?>
		<?php }else{ ?>
			<span id="CurrentSchoolAddress_default" style="display:none;"><?=$Lang['Admission']['TSS']['Districts'][4] ?></span>
			<select name="CurrentSchoolAddress" id="CurrentSchoolAddress" style="display:none;">
				<?php
				foreach($Lang['Admission']['TSS']['Districts'] as $districtId => $lang){
				    $selected = ($StudentPrevSchoolInfo[0]['SchoolAddress'] == $districtId)? ' selected' : '';
				?>
					<option value="<?=$districtId ?>" <?=$selected ?>><?=$lang ?></option>
				<?php
				}
				?>
			</select>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['OriginalSecondarySchoolName']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['OriginalSecondarySchoolName']?>
		<?php }else{ ?>
    		<input name="OriginalSecondarySchoolName" type="text" id="OriginalSecondarySchoolName" class="textboxtext"  value="<?=$allCustInfo['OriginalSecondarySchoolName'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['SchoolNetForAddress']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
		    if($formData['SchoolNet'] == $admission_cfg['SchoolNet']['Default']){
		        echo $Lang['Admission']['TSS']['SchoolNetForAddressDefault'];
		    }else{
		        echo $Lang['Admission']['TSS']['Districts'][ $formData['SchoolNetOtherDetails_District'] ];
		        echo $formData['SchoolNetOtherDetails_Net'];
		        echo $Lang['Admission']['TSS']['Net'];
		    }
		}else{ ?>
			<div>
				<?php $checked = ($allCustInfo['SchoolNet'][0]['Value'] == $admission_cfg['SchoolNet']['Default'])? 'checked' : '' ?>
    			<input type="radio" id="SchoolNet_Default" name="SchoolNet" value="<?=$admission_cfg['SchoolNet']['Default'] ?>" <?=$checked ?> />
    			<label for="SchoolNet_Default"><?=$Lang['Admission']['TSS']['SchoolNetForAddressDefault'] ?></label>
			</div>
			<div style="margin-top: 5px;">
				<?php $checked = ($allCustInfo['SchoolNet'][0]['Value'] != $admission_cfg['SchoolNet']['Default'])? 'checked' : '' ?>
    			<input type="radio" id="SchoolNet_Other" name="SchoolNet" value="<?=$admission_cfg['SchoolNet']['Other'] ?>" <?=$checked ?>/>
    			<label for="SchoolNet_Other"><?=$Lang['Admission']['TSS']['Others'] ?></label>

    			(
    				<?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>:
    				<select name="SchoolNetOtherDetails_District">
    					<?php
    					foreach($Lang['Admission']['TSS']['Districts'] as $districtId => $lang){
    					    $selected = ($allCustInfo['SchoolNetOtherDetails_District'][0]['Value'] == $districtId)? ' selected' : '';
    					?>
    						<option value="<?=$districtId ?>" <?=$selected ?>><?=$lang ?></option>
    					<?php
    					}
    					?>
    				</select>

    				<input
        				name="SchoolNetOtherDetails_Net"
        				id="SchoolNetOtherDetails_Net"
            				value="<?=$allCustInfo['SchoolNetOtherDetails_Net'][0]['Value']?>"
        				size="3"
        				maxlength="2"
        				style="margin-left: 5px;"
    				/>
    				<span><?=$Lang['Admission']['TSS']['Net'] ?></span>
    			)
			</div>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['ApplyOtherSchoolAtSameTime']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<div>
				1)
				<?=$formData['ApplyOtherSchoolAtSameTime1'] ?>
			</div>
			<div style="margin-top: 5px;">
				2)
				<?=$formData['ApplyOtherSchoolAtSameTime2'] ?>
			</div>
		<?php }else{ ?>
			<div>
				1)
				<input name="ApplyOtherSchoolAtSameTime1" id="ApplyOtherSchoolAtSameTime1" style="width: 500px"  value="<?=$allCustInfo['ApplyOtherSchoolAtSameTime1'][0]['Value']?>"/>
			</div>
			<div style="margin-top: 5px;">
				2)
				<input name="ApplyOtherSchoolAtSameTime2" id="ApplyOtherSchoolAtSameTime2" style="width: 500px"  value="<?=$allCustInfo['ApplyOtherSchoolAtSameTime2'][0]['Value']?>"/>
			</div>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['SelfApplySchool']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?= ($formData['SelfApplySchool'] != ' -- ')? $formData['SelfApplySchool'] : $Lang['Admission']['TSS']['SelfApplySchool_taksun'] ?>
		<?php }else{ ?>
    		<input type="radio" id="SelfApplySchool_taksun" name="SelfApplySchool_type" <?= ($allCustInfo['SelfApplySchool'][0]['Value'])? '' : 'checked' ?> />
    		<label for="SelfApplySchool_taksun"><?=$Lang['Admission']['TSS']['SelfApplySchool_taksun'] ?></label>
    		<br />

    		<input type="radio" id="SelfApplySchool_other" name="SelfApplySchool_type" <?= ($allCustInfo['SelfApplySchool'][0]['Value'])? 'checked' : '' ?> />
    		<label for="SelfApplySchool_other"><?=$Lang['Admission']['TSS']['Others'] ?></label>
    		<label for="SelfApplySchool">
    			(
    			<?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>:
    			<input name="SelfApplySchool" type="text" id="SelfApplySchool" class="textboxtext" style="width: 400px"  value="<?=$allCustInfo['SelfApplySchool'][0]['Value']?>"/>
    			)
    		</label>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['Wish']?> (<?=$Lang['Admission']['TSS']['PartA']?>)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?= ($formData['WishPartA'])? $Lang['Admission']['TSS']['WishOption'][ $formData['WishPartA'] ] : ''?>
		<?php }else{ ?>
			<select id="WishPartA" name="WishPartA">
				<option value="" <?= ($allCustInfo['WishPartA'][0]['Value'] == '')? 'selected' : '' ?> >- <?=$Lang['Admission']['pleaseSelect'] ?> -</option>
				<option value="FirstWish" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'FirstWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['FirstWish'] ?></option>
				<option value="SecondWish" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'SecondWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['SecondWish'] ?></option>
				<option value="ThirdWish" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'ThirdWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['ThirdWish'] ?></option>
				<option value="NoSelection" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'NoSelection')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NoSelection'] ?></option>
				<option value="NotJoinCentralAllocation" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'NotJoinCentralAllocation')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NotJoinCentralAllocation'] ?></option>
			</select>
		<?php } ?>
	</td>

	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['Wish']?> (<?=$Lang['Admission']['TSS']['PartB']?>)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?= ($formData['WishPartB'])? $Lang['Admission']['TSS']['WishOption'][ $formData['WishPartB'] ] : ''?>
		<?php }else{ ?>
			<select id="WishPartB" name="WishPartB">
				<option value="" <?= ($allCustInfo['WishPartB'][0]['Value'] == '')? 'selected' : '' ?> >- <?=$Lang['Admission']['pleaseSelect'] ?> -</option>
				<option value="FirstWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'FirstWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['FirstWish'] ?></option>
				<option value="SecondWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'SecondWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['SecondWish'] ?></option>
				<option value="ThirdWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'ThirdWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['ThirdWish'] ?></option>
				<option value="OtherWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'OtherWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['OtherWish'] ?></option>
				<option value="DifferentDistrict" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'DifferentDistrict')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['DifferentDistrict'] ?></option>
				<option value="NoSelection" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'NoSelection')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NoSelection'] ?></option>
				<option value="NotJoinCentralAllocation" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'NotJoinCentralAllocation')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NotJoinCentralAllocation'] ?></option>
			</select>
		<?php } ?>
	</td>
</tr>
</table>

<script>
$('[name="KindergartenName_type"]').change(function(){
	if($(this).val() == 'tsk'){
		$('#CurrentSchoolAddress').hide().val(4);
		$('#CurrentSchoolAddress_default').show();
	}else{
		$('#CurrentSchoolAddress').show();
		$('#CurrentSchoolAddress_default').hide();
	}
});
$('[name="KindergartenName_type"]:checked').change();

function check_academic_background_form(){
	/**** EdbApplicationNo START ****/
	if($('#EdbApplicationNo').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterEdbApplicationNo']?>");
		$('#EdbApplicationNo').focus();
		return false;
	}
	/**** EdbApplicationNo END ****/

	/**** CurrentSchoolName START ****/
	if($('#KindergartenName_tsk').prop('checked')){
		$('#CurrentSchoolName').val('');
	}else if($('#KindergartenName_other').prop('checked') && $('#CurrentSchoolName').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolName']?>");
		$('#CurrentSchoolName').focus();
		return false;
	}
	/**** CurrentSchoolName END ****/

	/**** CurrentSchoolAddress START ****/
	if($('#CurrentSchoolAddress').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolAddress']?>");
		$('#CurrentSchoolAddress').focus();
		return false;
	}
	/**** CurrentSchoolAddress END ****/

	/**** SchoolNet START ****/
	if($('[name="SchoolNet"]:checked').length == 0){
		alert("<?=$Lang['Admission']['TSS']['msg']['selectSchoolNet']?>");
		$('#SchoolNet_Default').focus();
		return false;
	}
	if($('#SchoolNet_Other:checked').length == 1 && $('#SchoolNetOtherDetails_Net').val().trim() == ''){
		alert("<?=$Lang['Admission']['TSS']['msg']['selectSchoolNet']?>");
		$('#SchoolNetOtherDetails_Net').focus();
		return false;
	}
	/**** SchoolNet END ****/

	/**** SelfApplySchool START ****/
	if($('#SelfApplySchool_taksun').prop('checked')){
		$('#SelfApplySchool').val('');
	}else{
    	if($('#SelfApplySchool').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterSelfApplySchool']?>");
    		$('#SelfApplySchool').focus();
    		return false;
    	}
	}
	/**** SelfApplySchool END ****/

	/**** WishPartA START ****/
	if($('#WishPartA').val().trim()==''){
    	alert("<?=$Lang['Admission']['TSS']['msg']['selectWishPartA']?>");
    	$('#WishPartA').focus();
    	return false;
    }
    /**** WishPartA END ****/

	/**** WishPartB START ****/
	if($('#WishPartB').val().trim()==''){
    	alert("<?=$Lang['Admission']['TSS']['msg']['selectWishPartB']?>");
    	$('#WishPartB').focus();
    	return false;
    }
    /**** WishPartA END ****/

	return true;
}
</script>