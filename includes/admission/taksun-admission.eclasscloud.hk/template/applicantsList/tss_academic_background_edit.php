<?php

global $libkis_admission;


$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,$classLevelID='',$applicationID=$applicationInfo['applicationID'],'');
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$Lang = $kis_lang;
$star = $mustfillinsymbol;

#### Get Year START ####
$allClassLevel = $libkis_admission->getClassLevel();
$isFirstYear = (filter_var($allClassLevel[$applicationInfo['classLevelID']], FILTER_SANITIZE_NUMBER_INT) == 1);
#### Get Year END ####


if($isFirstYear):
////////////////////////// First Year Form START //////////////////////////
?>
    <table class="form_table" style="font-size: 13px">
        <colgroup>
        	<col width="30%">
        	<col width="70%">
        </colgroup>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['EdbApplicationNo']?>
        	</td>
        	<td>
        		<input name="EdbApplicationNo" type="text" id="EdbApplicationNo" class="textboxtext"  value="<?=$allCustInfo['EdbApplicationNo'][0]['Value']?>"/>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['KindergartenName']?>
        	</td>

        	<td>
        		<input type="radio" id="KindergartenName_tsk" name="KindergartenName_type" value="tsk" <?= ($StudentPrevSchoolInfo[0]['NameOfSchool'])? '' : 'checked' ?> />
        		<label for="KindergartenName_tsk"><?=$Lang['Admission']['TSS']['TakSunKindergarten'] ?></label>
        		<br />

        		<input type="radio" id="KindergartenName_other" name="KindergartenName_type" value="other" <?= ($StudentPrevSchoolInfo[0]['NameOfSchool'])? 'checked' : '' ?> />
        		<label for="KindergartenName_other"><?=$Lang['Admission']['TSS']['Others'] ?></label>
        		<label for="CurrentSchoolName">
        			(
        			<?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>:
    				<input name="CurrentSchoolName" type="text" id="CurrentSchoolName" class="textboxtext" style="width: 400px;" value="<?=$StudentPrevSchoolInfo[0]['NameOfSchool']?>"/>
        			)
        		</label>

        		<br />
        		(
            		<?=$Lang['Admission']['district']?>:
        			<span id="CurrentSchoolAddress_default" style="display:none;"><?=$Lang['Admission']['TSS']['Districts'][4] ?></span>
        			<select name="CurrentSchoolAddress" id="CurrentSchoolAddress" style="margin-top: 5px;display:none;">
        				<?php
        				foreach($Lang['Admission']['TSS']['Districts'] as $districtId => $lang){
        				    $selected = ($StudentPrevSchoolInfo[0]['SchoolAddress'] == $districtId)? ' selected' : '';
        				?>
        					<option value="<?=$districtId ?>" <?=$selected ?>><?=$lang ?></option>
        				<?php
        				}
        				?>
        			</select>
        		)
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$Lang['Admission']['TSS']['OriginalSecondarySchoolName']?>
        	</td>
        	<td>
        		<input name="OriginalSecondarySchoolName" type="text" id="OriginalSecondarySchoolName" class="textboxtext"  value="<?=$allCustInfo['OriginalSecondarySchoolName'][0]['Value']?>"/>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['SchoolNetForAddress']?>
        	</td>
        	<td>
    			<div>
    				<?php $checked = ($allCustInfo['SchoolNet'][0]['Value'] == $admission_cfg['SchoolNet']['Default'])? 'checked' : '' ?>
        			<input type="radio" id="SchoolNet_Default" name="SchoolNet" value="<?=$admission_cfg['SchoolNet']['Default'] ?>" <?=$checked ?> />
        			<label for="SchoolNet_Default"><?=$Lang['Admission']['TSS']['SchoolNetForAddressDefault'] ?></label>
    			</div>
    			<div style="margin-top: 5px;">
    				<?php $checked = ($allCustInfo['SchoolNet'][0]['Value'] != $admission_cfg['SchoolNet']['Default'])? 'checked' : '' ?>
        			<input type="radio" id="SchoolNet_Other" name="SchoolNet" value="<?=$admission_cfg['SchoolNet']['Other'] ?>" <?=$checked ?> />
        			<label for="SchoolNet_Other"><?=$Lang['Admission']['TSS']['Others'] ?></label>

        			(
        				<?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>:
        				<select name="SchoolNetOtherDetails_District">
        					<?php
        					foreach($Lang['Admission']['TSS']['Districts'] as $districtId => $lang){
        					    $selected = ($allCustInfo['SchoolNetOtherDetails_District'][0]['Value'] == $districtId)? ' selected' : '';
        					?>
        						<option value="<?=$districtId ?>" <?=$selected ?>><?=$lang ?></option>
        					<?php
        					}
        					?>
        				</select>

        				<input
            				name="SchoolNetOtherDetails_Net"
            				id="SchoolNetOtherDetails_Net"
            				value="<?=$allCustInfo['SchoolNetOtherDetails_Net'][0]['Value']?>"
            				size="3"
            				maxlength="2"
            				style="margin-left: 5px;"
        				/>
        				<span><?=$Lang['Admission']['TSS']['Net'] ?></span>
        			)
    			</div>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$Lang['Admission']['TSS']['ApplyOtherSchoolAtSameTime']?>
        	</td>
        	<td>
        		<div>
        			1)
        			<input name="ApplyOtherSchoolAtSameTime1" id="ApplyOtherSchoolAtSameTime1" style="width: 500px"  value="<?=$allCustInfo['ApplyOtherSchoolAtSameTime1'][0]['Value']?>"/>
        		</div>
        		<div style="margin-top: 5px;">
        			2)
        			<input name="ApplyOtherSchoolAtSameTime2" id="ApplyOtherSchoolAtSameTime2" style="width: 500px"  value="<?=$allCustInfo['ApplyOtherSchoolAtSameTime2'][0]['Value']?>"/>
        		</div>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['SelfApplySchool']?>
        	</td>
        	<td>
        		<input type="radio" id="SelfApplySchool_taksun" name="SelfApplySchool_type" <?= ($allCustInfo['SelfApplySchool'][0]['Value'])? '' : 'checked' ?> />
        		<label for="SelfApplySchool_taksun"><?=$Lang['Admission']['TSS']['SelfApplySchool_taksun'] ?></label>
        		<br />

        		<input type="radio" id="SelfApplySchool_other" name="SelfApplySchool_type" <?= ($allCustInfo['SelfApplySchool'][0]['Value'])? 'checked' : '' ?> />
        		<label for="SelfApplySchool_other"><?=$Lang['Admission']['TSS']['Others'] ?></label>
        		<label for="SelfApplySchool">
        			(
        			<?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>:
        			<input name="SelfApplySchool" type="text" id="SelfApplySchool" class="textboxtext" style="width: 400px"  value="<?=$allCustInfo['SelfApplySchool'][0]['Value']?>"/>
        			)
        		</label>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['Wish']?> (<?=$Lang['Admission']['TSS']['PartA']?>)
        	</td>
        	<td>
        		<?php if($IsConfirm){ ?>
        			<?= ($formData['WishPartA'])? $Lang['Admission']['TSS']['WishOption'][ $formData['WishPartA'] ] : ''?>
        		<?php }else{ ?>
        			<select id="WishPartA" name="WishPartA">
        				<option value="" <?= ($allCustInfo['WishPartA'][0]['Value'] == '')? 'selected' : '' ?> >- <?=$Lang['Admission']['pleaseSelect'] ?> -</option>
        				<option value="FirstWish" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'FirstWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['FirstWish'] ?></option>
        				<option value="SecondWish" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'SecondWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['SecondWish'] ?></option>
        				<option value="ThirdWish" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'ThirdWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['ThirdWish'] ?></option>
						<option value="NoSelection" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'NoSelection')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NoSelection'] ?></option>
						<option value="NotJoinCentralAllocation" <?= ($allCustInfo['WishPartA'][0]['Value'] == 'NotJoinCentralAllocation')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NotJoinCentralAllocation'] ?></option>
        			</select>
        		<?php } ?>
        	</td>
    	</tr>

    	<tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['Wish']?> (<?=$Lang['Admission']['TSS']['PartB']?>)
        	</td>
        	<td>
        		<?php if($IsConfirm){ ?>
        			<?= ($formData['WishPartB'])? $Lang['Admission']['TSS']['WishOption'][ $formData['WishPartB'] ] : ''?>
        		<?php }else{ ?>
        			<select id="WishPartB" name="WishPartB">
        				<option value="" <?= ($allCustInfo['WishPartB'][0]['Value'] == '')? 'selected' : '' ?> >- <?=$Lang['Admission']['pleaseSelect'] ?> -</option>
        				<option value="FirstWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'FirstWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['FirstWish'] ?></option>
        				<option value="SecondWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'SecondWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['SecondWish'] ?></option>
        				<option value="ThirdWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'ThirdWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['ThirdWish'] ?></option>
        				<option value="OtherWish" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'OtherWish')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['OtherWish'] ?></option>
        				<option value="DifferentDistrict" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'DifferentDistrict')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['DifferentDistrict'] ?></option>
						<option value="NoSelection" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'NoSelection')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NoSelection'] ?></option>
						<option value="NotJoinCentralAllocation" <?= ($allCustInfo['WishPartB'][0]['Value'] == 'NotJoinCentralAllocation')? 'selected' : '' ?> ><?=$Lang['Admission']['TSS']['WishOption']['NotJoinCentralAllocation'] ?></option>
        			</select>
        		<?php } ?>
        	</td>
        </tr>
    </table>
    <script>
        $('[name="KindergartenName_type"]').change(function(){
        	if($(this).val() == 'tsk'){
        		$('#CurrentSchoolAddress').hide().val(4);
        		$('#CurrentSchoolAddress_default').show();
        	}else{
        		$('#CurrentSchoolAddress').show();
        		$('#CurrentSchoolAddress_default').hide();
        	}
        });
        $('[name="KindergartenName_type"]:checked').change();

        function check_academic_background_form(){
        	/**** EdbApplicationNo START ****/
        	if($('#EdbApplicationNo').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterEdbApplicationNo']?>");
        		$('#EdbApplicationNo').focus();
        		return false;
        	}
        	/**** EdbApplicationNo END ****/

        	/**** CurrentSchoolName START ****/
        	if($('#KindergartenName_tsk').prop('checked')){
        		$('#CurrentSchoolName').val('');
        	}else if($('#KindergartenName_other').prop('checked') && $('#CurrentSchoolName').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolName']?>");
        		$('#CurrentSchoolName').focus();
        		return false;
        	}
        	/**** CurrentSchoolName END ****/

        	/**** CurrentSchoolAddress START ****/
        	if($('#CurrentSchoolAddress').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolAddress']?>");
        		$('#CurrentSchoolAddress').focus();
        		return false;
        	}
        	/**** CurrentSchoolAddress END ****/

        	/**** SchoolNet START ****/
        	if($('[name="SchoolNet"]:checked').length == 0){
        		alert("<?=$Lang['Admission']['TSS']['msg']['selectSchoolNet']?>");
        		$('#SchoolNet_Default').focus();
        		return false;
        	}
        	if($('#SchoolNet_Other:checked').length == 1 && $('#SchoolNetOtherDetails_Net').val().trim() == ''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['selectSchoolNet']?>");
        		$('#SchoolNetOtherDetails_Net').focus();
        		return false;
        	}
        	/**** SchoolNet END ****/

        	/**** SelfApplySchool START ****/
        	if($('#SelfApplySchool_taksun').prop('checked')){
        		$('#SelfApplySchool').val('');
        	}else{
            	if($('#SelfApplySchool').val().trim()==''){
            		alert("<?=$Lang['Admission']['TSS']['msg']['enterSelfApplySchool']?>");
            		$('#SelfApplySchool').focus();
            		return false;
            	}
        	}
        	/**** SelfApplySchool END ****/

        	/**** WishPartA START ****/
        	if($('#WishPartA').val().trim()==''){
            	alert("<?=$Lang['Admission']['TSS']['msg']['selectWishPartA']?>");
            	$('#WishPartA').focus();
            	return false;
            }
            /**** WishPartA END ****/

        	/**** WishPartB START ****/
        	if($('#WishPartB').val().trim()==''){
            	alert("<?=$Lang['Admission']['TSS']['msg']['selectWishPartB']?>");
            	$('#WishPartB').focus();
            	return false;
            }
            /**** WishPartA END ****/

        	return true;
        }
    </script>
<?php
////////////////////////// First Year Form END //////////////////////////
else:
////////////////////////// Other Year Form START //////////////////////////
?>
    <table class="form_table" style="font-size: 13px">
        <colgroup>
        	<col width="30%">
        	<col width="70%">
        </colgroup>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['CurrentSchoolName']?>
        	</td>
        	<td>
        		<input name="CurrentSchoolName" type="text" id="CurrentSchoolName" class="textboxtext"  value="<?=$StudentPrevSchoolInfo[0]['NameOfSchool']?>"/>
        		<br />
        		(
            		<?=$Lang['Admission']['district']?>:
            		<input name="CurrentSchoolAddress" type="text" id="CurrentSchoolAddress" style="width: calc(100% - 80px);margin-top:5px;" value="<?=$StudentPrevSchoolInfo[0]['SchoolAddress']?>"/>
        		)
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['ApplyDate']?>
        	</td>
        	<td>
    			<input name="ApplyYear" type="text" id="ApplyYear" size="5" maxlength="4" value="<?=$allCustInfo['ApplyYear'][0]['Value']?>"/>
        		<?=$Lang['Admission']['TSS']['Year'] ?>
    			<input name="ApplyMonth" type="text" id="ApplyMonth" size="3" maxlength="2" value="<?=$allCustInfo['ApplyMonth'][0]['Value']?>"/>
        		<?=$Lang['Admission']['TSS']['Month'] ?>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['LastExamAttend']?>
        	</td>
        	<td>
        		<?=$Lang['Admission']['TSS']['overallYear'] ?>:
        		<input name="LastExamAttend_Year" type="text" id="LastExamAttend_Year" size="4" maxlength="3" value="<?=$allCustInfo['LastExamAttend_Year'][0]['Value']?>"/>
        		(
        			<?=$Lang['Admission']['TSS']['TotalAttend']?>:
        			<input name="LastExamTotalAttend_Year" type="text" id="LastExamTotalAttend_Year" size="4" maxlength="3" value="<?=$allCustInfo['LastExamTotalAttend_Year'][0]['Value']?>"/>
        		)
        		<br/ >
        		<?=$Lang['Admission']['TSS']['overallClass'] ?>:
        		<input name="LastExamAttend_Class" type="text" id="LastExamAttend_Class" size="4" maxlength="3" style="margin-top: 5px;" value="<?=$allCustInfo['LastExamAttend_Class'][0]['Value']?>"/>
    			(
            		<?=$Lang['Admission']['TSS']['TotalAttend'] ?>:
        			<input name="LastExamTotalAttend_Class" type="text" id="LastExamTotalAttend_Class" size="4" maxlength="3" style="margin-top: 5px;" value="<?=$allCustInfo['LastExamTotalAttend_Class'][0]['Value']?>"/>
    			)
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['AllSubjectScore']?>
        	</td>
        	<td>
    			<?=$Lang['Admission']['TSS']['Subjects']['Chi'] ?>:
    			<input name="AllSubjectScore_Chi" type="text" id="AllSubjectScore_Chi" size="5" value="<?=$allCustInfo['AllSubjectScore_Chi'][0]['Value']?>"/>
    			<br />
    			<?=$Lang['Admission']['TSS']['Subjects']['Eng'] ?>:
    			<input name="AllSubjectScore_Eng" type="text" id="AllSubjectScore_Eng" size="5" style="margin-top: 5px;" value="<?=$allCustInfo['AllSubjectScore_Eng'][0]['Value']?>"/>
    			<br />
    			<?=$Lang['Admission']['TSS']['Subjects']['Math'] ?>:
    			<input name="AllSubjectScore_Math" type="text" id="AllSubjectScore_Math" size="5" style="margin-top: 5px;" value="<?=$allCustInfo['AllSubjectScore_Math'][0]['Value']?>"/>
    			<br />
    			<?=$Lang['Admission']['TSS']['Subjects']['GeneralStudies'] ?>:
    			<input name="AllSubjectScore_Gs" type="text" id="AllSubjectScore_Gs" size="5" style="margin-top: 5px;" value="<?=$allCustInfo['AllSubjectScore_Gs'][0]['Value']?>"/>
        	</td>
    	</tr>
    	<tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['Conduct']?>
        	</td>
        	<td>
				<input name="Conduct" type="text" id="Conduct" class="textboxtext" value="<?=$allCustInfo['Conduct'][0]['Value']?>"/>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$star ?>
        		<?=$Lang['Admission']['TSS']['Strengths']?>
        	</td>
        	<td>
				<input name="Strengths" type="text" id="Strengths" class="textboxtext" value="<?=$allCustInfo['Strengths'][0]['Value']?>"/>
        	</td>
        </tr>
    </table>
    <script>
        function check_academic_background_form(){

        	/**** CurrentSchoolName START ****/
        	if($('#CurrentSchoolName').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterEdbApplicationNo']?>");
        		$('#CurrentSchoolName').focus();
        		return false;
        	}
        	/**** CurrentSchoolName END ****/

        	/**** CurrentSchoolAddress START ****/
        	if($('#CurrentSchoolAddress').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolAddress']?>");
        		$('#CurrentSchoolAddress').focus();
        		return false;
        	}
        	/**** CurrentSchoolAddress END ****/

        	/**** ApplyDate START ****/
        	if($('#ApplyYear').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterApplyDate']?>");
        		$('#ApplyYear').focus();
        		return false;
        	}
        	if($('#ApplyMonth').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterApplyDate']?>");
        		$('#ApplyMonth').focus();
        		return false;
        	}
        	/**** ApplyDate END ****/

        	/**** LastExamAttend START ****/
        	if($('#LastExamAttend_Year').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamAttend']?>");
        		$('#LastExamAttend_Year').focus();
        		return false;
        	}
        	if($('#LastExamAttend_Class').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamAttend']?>");
        		$('#LastExamAttend_Class').focus();
        		return false;
        	}
        	/**** LastExamAttend END ****/

        	/**** LastExamTotalAttend START ****/
        	if($('#LastExamTotalAttend_Year').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamTotalAttend']?>");
        		$('#LastExamTotalAttend_Year').focus();
        		return false;
        	}
        	if($('#LastExamTotalAttend_Class').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamTotalAttend']?>");
        		$('#LastExamTotalAttend_Class').focus();
        		return false;
        	}
        	/**** LastExamTotalAttend END ****/

        	/**** AllSubjectScore START ****/
        	if($('#AllSubjectScore_Chi').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");
        		$('#AllSubjectScore_Chi').focus();
        		return false;
        	}
        	if($('#AllSubjectScore_Eng').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");
        		$('#AllSubjectScore_Eng').focus();
        		return false;
        	}
        	if($('#AllSubjectScore_Math').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");
        		$('#AllSubjectScore_Math').focus();
        		return false;
        	}
        	if($('#AllSubjectScore_Gs').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");
        		$('#AllSubjectScore_Gs').focus();
        		return false;
        	}
        	/**** AllSubjectScore END ****/

        	/**** Conduct START ****/
        	if($('#Conduct').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterConduct']?>");
        		$('#Conduct').focus();
        		return false;
        	}
        	/**** Conduct END ****/

        	/**** Strengths START ****/
        	if($('#Strengths').val().trim()==''){
        		alert("<?=$Lang['Admission']['TSS']['msg']['enterStrengths']?>");
        		$('#Strengths').focus();
        		return false;
        	}
        	/**** Strengths END ****/

        	return true;
        }
    </script>
<?php
////////////////////////// Other Year Form END //////////////////////////
endif;
?>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();

	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(check_academic_background_form()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});
</script>