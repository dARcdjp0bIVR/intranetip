<?php

global $libkis_admission;


$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,$classLevelID='',$applicationID=$applicationInfo['applicationID'],$recordID='');
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$Lang = $kis_lang;

#### Get Year START ####
$isFirstYear = $libkis_admission->getIsFirstYear($applicationInfo['classLevelID']);
#### Get Year END ####


if($isFirstYear):
////////////////////////// First Year Form START //////////////////////////
?>
    <table class="form_table" style="font-size: 13px">
        <colgroup>
        	<col width="30%">
        	<col width="70%">
        </colgroup>

        <tr>
        	<td class="field_title">
        		<?=$Lang['Admission']['TSS']['EdbApplicationNo']?>
        	</td>
        	<td>
        		<?= kis_ui::displayTableField($allCustInfo['EdbApplicationNo'][0]['Value']) ?>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$Lang['Admission']['TSS']['KindergartenName']?>
        	</td>
        	<td>
        		<?= ($StudentPrevSchoolInfo[0]['NameOfSchool'])?$StudentPrevSchoolInfo[0]['NameOfSchool']: $Lang['Admission']['TSS']['TakSunKindergarten']?>
        		(
        		<?=$Lang['Admission']['district']?>:
        		<?= kis_ui::displayTableField($Lang['Admission']['TSS']['Districts'][$StudentPrevSchoolInfo[0]['SchoolAddress']]) ?>
        		)
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$Lang['Admission']['TSS']['OriginalSecondarySchoolName']?>
        	</td>
        	<td>
        		<?= kis_ui::displayTableField($allCustInfo['OriginalSecondarySchoolName'][0]['Value']) ?>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">

        		<?=$Lang['Admission']['TSS']['SchoolNetForAddress']?>
        	</td>
        	<td>
        		<?php
        	    if($allCustInfo['SchoolNet'][0]['Value'] == $admission_cfg['SchoolNet']['Default']){
        	        echo $Lang['Admission']['TSS']['SchoolNetForAddressDefault'];
        	    }else{
        	        echo $Lang['Admission']['TSS']['Districts'][ $allCustInfo['SchoolNetOtherDetails_District'][0]['Value'] ];
        	        echo $allCustInfo['SchoolNetOtherDetails_Net'][0]['Value'];
        	        echo $Lang['Admission']['TSS']['Net'];
        	    }
        	    ?>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">
        		<?=$Lang['Admission']['TSS']['ApplyOtherSchoolAtSameTime']?>
        	</td>
        	<td>
        		<div>
        			1)
        			<?= kis_ui::displayTableField($allCustInfo['ApplyOtherSchoolAtSameTime1'][0]['Value']) ?>
        		</div>
        		<div style="margin-top: 5px;">
        			2)
        			<?= kis_ui::displayTableField($allCustInfo['ApplyOtherSchoolAtSameTime2'][0]['Value']) ?>
        		</div>
        	</td>
        </tr>

        <tr>
        	<td class="field_title">

        		<?=$Lang['Admission']['TSS']['SelfApplySchool']?>
        	</td>
        	<td>
        		<?= ($allCustInfo['SelfApplySchool'][0]['Value'])? $allCustInfo['SelfApplySchool'][0]['Value'] : $Lang['Admission']['TSS']['SelfApplySchool_taksun'] ?>
        	</td>
        </tr>
        <!--tr>
        	<td class="field_title">

        		<?=$Lang['Admission']['TSS']['ApplySchoolPartB']?>
        	</td>
        	<td>
        		<?= kis_ui::displayTableField($allCustInfo['ApplySchoolPartB'][0]['Value']) ?>
        	</td>
        </tr-->

        <tr>
        	<td class="field_title">

        		<?=$Lang['Admission']['TSS']['Wish']?> (<?=$Lang['Admission']['TSS']['PartA']?>)
        	</td>
        	<td>
        		<?= kis_ui::displayTableField($Lang['Admission']['TSS']['WishOption'][ $allCustInfo['WishPartA'][0]['Value'] ]) ?>
        	</td>
        </tr>
        <tr>
        	<td class="field_title">

        		<?=$Lang['Admission']['TSS']['Wish']?> (<?=$Lang['Admission']['TSS']['PartB']?>)
        	</td>
        	<td>
        		<?= kis_ui::displayTableField($Lang['Admission']['TSS']['WishOption'][ $allCustInfo['WishPartB'][0]['Value'] ]) ?>
        	</td>
        </tr>
    </table>
<?php
////////////////////////// First Year Form END //////////////////////////
else:
////////////////////////// Other Year Form START //////////////////////////
?>
    <table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="70%">
    </colgroup>

    <tr>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['TSS']['CurrentSchoolName']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField($StudentPrevSchoolInfo[0]['NameOfSchool']) ?>
    		(
    		<?=$Lang['Admission']['district']?>:
    		<?= kis_ui::displayTableField($StudentPrevSchoolInfo[0]['SchoolAddress']) ?>
    		)
    	</td>
    </tr>

    <tr>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['TSS']['ApplyDate']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField($allCustInfo['ApplyYear'][0]['Value']) ?>
    		<?=$Lang['Admission']['TSS']['Year'] ?>
    		<?= kis_ui::displayTableField($allCustInfo['ApplyMonth'][0]['Value']) ?>
    		<?=$Lang['Admission']['TSS']['Month'] ?>
    	</td>
    </tr>

    <tr>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['TSS']['LastExamAttend']?>
    	</td>
    	<td>
    		<?=$Lang['Admission']['TSS']['overallYear'] ?>:
    		<?= kis_ui::displayTableField($allCustInfo['LastExamAttend_Year'][0]['Value']) ?>
    		(
    			<?=$Lang['Admission']['TSS']['TotalAttend']?>:
        		<?= kis_ui::displayTableField($allCustInfo['LastExamTotalAttend_Year'][0]['Value']) ?>
    		)
    		<br/ >
    		<?=$Lang['Admission']['TSS']['overallClass'] ?>:
    		<?= kis_ui::displayTableField($allCustInfo['LastExamAttend_Class'][0]['Value']) ?>
			(
        		<?=$Lang['Admission']['TSS']['TotalAttend'] ?>:
        		<?= kis_ui::displayTableField($allCustInfo['LastExamTotalAttend_Class'][0]['Value']) ?>
			)
    	</td>
    </tr>

    <tr>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['TSS']['AllSubjectScore']?>
    	</td>
    	<td>
			<?=$Lang['Admission']['TSS']['Subjects']['Chi'] ?>:
    		<?= kis_ui::displayTableField($allCustInfo['AllSubjectScore_Chi'][0]['Value']) ?>
			<br />
			<?=$Lang['Admission']['TSS']['Subjects']['Eng'] ?>:
    		<?= kis_ui::displayTableField($allCustInfo['AllSubjectScore_Eng'][0]['Value']) ?>
			<br />
			<?=$Lang['Admission']['TSS']['Subjects']['Math'] ?>:
    		<?= kis_ui::displayTableField($allCustInfo['AllSubjectScore_Math'][0]['Value']) ?>
			<br />
			<?=$Lang['Admission']['TSS']['Subjects']['GeneralStudies'] ?>:
    		<?= kis_ui::displayTableField($allCustInfo['AllSubjectScore_Gs'][0]['Value']) ?>
    	</td>
	</tr>
	<tr>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['TSS']['Conduct']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField($allCustInfo['Conduct'][0]['Value']) ?>
    	</td>
    </tr>

    <tr>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['TSS']['Strengths']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField($allCustInfo['Strengths'][0]['Value']) ?>
    	</td>
    </tr>
    </table>
<?php
////////////////////////// Other Year Form END //////////////////////////
endif;