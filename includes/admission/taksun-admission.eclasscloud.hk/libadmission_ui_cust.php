<?php
# modifying by:
/**
 * Change Log:
 * 2018-01-24 Pun
 *  - File Created
 */

include_once("{$intranet_root}/includes/admission/libadmission_ui_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionUiCustBase.class.php");

class admission_ui_cust extends \AdmissionSystem\AdmissionUiCustBase{

	public function __construct(){
        global $plugin;

        if($plugin['eAdmission_devMode']){
            error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);
        }

	    parent::__construct();
		$this->init();
	}

	private function init(){
	    global $LangEn;

	    $LangEn = array(); // This admission no need english

	    /**** FILTER_ADMISSION_FORM_WIZARD_STEPS START **** /
		$this->addFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS, (function($defaultArr){
		    global $Lang;

		    $additionalStep = array();
		    $additionalStep[] = array(
		        'id' => 'step_payment',
		        'title' => $Lang['Admission']['payment'],
		    );

		    array_splice($defaultArr, 3, 0, $additionalStep);

		    return $defaultArr;
		}));
	    /**** FILTER_ADMISSION_FORM_WIZARD_STEPS END ****/

		/**** FILTER_ADMISSION_FORM_STEP_PAGE_HTML START **** /
		$this->addFilter(self::FILTER_ADMISSION_FORM_STEP_PAGE_HTML, (function($defaultArr){
		    $additionalPage = array();
		    $additionalPage[] = array(
                'id' => 'step_payment',
                'func' => 'getPaymentPage',
                'args' => array(),
		    );

		    array_splice($defaultArr, 4, 0, $additionalPage);

		    return $defaultArr;
		}));
		/**** FILTER_ADMISSION_FORM_STEP_PAGE_HTML START ****/

	    /**** FILTER_ADMISSION_FORM_INSTRUCTION_HTML START ****/
        $this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml = str_replace('class="instructionSuggestBrowser_Eng"', 'class="instructionSuggestBrowser_Eng" style="display:none;"', $formHtml);
            return $formHtml;
        }));
		$this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    $formHtml = str_replace('class="instructionLang"', 'class="instructionLang" style="display:none;"', $formHtml);
		    return $formHtml;
		}));
	    /**** FILTER_ADMISSION_FORM_INSTRUCTION_HTML END ****/

		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getStudentAcademicBackgroundForm($IsConfirm, $IsUpdate, $YearID);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getParentForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getOtherForm($IsConfirm, $IsUpdate, $YearID);
		}));
		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML END ****/

		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate, $YearID){
		    return $formHtml . $this->getStudentForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate, $YearID){
		    return $formHtml . $this->getStudentAcademicBackgroundForm($IsConfirm=1, $IsUpdate, $YearID);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate, $YearID){
		    return $formHtml . $this->getParentForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate, $YearID){
		    return $formHtml . $this->getOtherForm($IsConfirm=1, $IsUpdate, $YearID);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate, $YearID){
		    return $formHtml . $this->getDocUploadForm($IsConfirm=1, $IsUpdate);
		}));
		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML END ****/

		/**** FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getStudentAcademicBackgroundForm($IsConfirm, $IsUpdate, $YearID);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getParentForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getOtherForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate, $YearID){
		    return $formHtml . $this->getDocUploadForm($IsConfirm, $IsUpdate);
		}));
		/**** FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML END ****/

		/**** FILTER_ADMISSION_TOP_MENU_TAB START ****/
		$this->addFilter(self::FILTER_ADMISSION_TOP_MENU_TAB, (function($defaultTab){
		    $newTab = array('tss_academic_background');
		    array_splice($defaultTab, 1, 0, $newTab);
		    return $defaultTab;
		}));
		/**** FILTER_ADMISSION_TOP_MENU_TAB END ****/

	}

	/**
	 * Admission Form - create/edit form student part
	 */
	protected function getStudentForm($IsConfirm=0, $BirthCertNo = "", $IsUpdate=0){
		global $formData, $Lang, $lac, $admission_cfg;

		$allClassLevel = $lac->getClassLevel();

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);

			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

			if(count($application_details) > 0){
				$StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
			}

			## Apply Year START ##
			$classLevelID = $StudentInfo['classLevelID'];
			$classLevel = $allClassLevel[$classLevelID];
			## Apply Year END ##
		}

		@ob_start();
		include(__DIR__ . "/template/admissionForm/studentForm.tmpl.php");
		$x = ob_get_clean();

		return $x;
	}

	/**
	 * Admission Form - create/edit form parent part
	 */
	protected function getParentForm($IsConfirm=0, $IsUpdate=0){
		global $formData, $Lang, $lac, $admission_cfg;

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

			$tmpParentInfoArr = $lac->getApplicationParentInfo($application_details['ApplyYear'],'',$_SESSION['KIS_ApplicationID']);
			$parentInfoArr = array();
			foreach($tmpParentInfoArr as $parent){
				foreach($parent as $para=>$info){
					$ParentInfo[$parent['type']][$para] = $info;
				}
			}
			#### Parent Info END ####
		}
		$parentTypeArr = array(1=>'F',2=>'M',3=>'G');

		@ob_start();
		include("template/admissionForm/parentForm.tmpl.php");
		$x = ob_get_clean();

		return $x;
	}

	/**
	 * Admission Form - create/edit form student academic background part
	 */
	protected function getStudentAcademicBackgroundForm($IsConfirm=0, $IsUpdate=0, $YearID=0){
	    global $formData, $Lang, $lac, $admission_cfg;

	    #### Lang START ####
	    $langArr = $this->getBilingualArr();
	    $LangB5 = $langArr['Lang']['b5'];
	    $LangEn = $langArr['Lang']['en'];
	    $kis_lang_B5 = $langArr['kis_lang']['b5'];
	    $kis_lang_En = $langArr['kis_lang']['en'];
	    #### Lang END ####

	    $allClassLevel = $lac->getClassLevel();

	    $star = $IsConfirm?'':'<font style="color:red;">*</font>';

	    if($IsUpdate){
	        $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);

	        $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
	        if(count($application_details) > 0){
	            $YearID = $application_details['ApplyLevel'];
	            $StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));

	            $StudentPrevSchoolInfo = $lac->getApplicationPrevSchoolInfo($schoolYearID=$application_details['ApplyYear'],$classLevelID='',$applicationID=$application_details['ApplicationID'],$recordID='');
	        }
	    }

	    $isFirstYear = $lac->getIsFirstYear($YearID);

	    @ob_start();
	    if($isFirstYear){
	        include(__DIR__ . "/template/admissionForm/studentAcademicBackgroundForm_year1.tmpl.php");
	    }else{
	        include(__DIR__ . "/template/admissionForm/studentAcademicBackgroundForm_year2.tmpl.php");
	    }
	    $x = ob_get_clean();

	    return $x;
	}

	/**
	 * Admission Form - create/edit form other part
	 */
	protected function getOtherForm($IsConfirm=0, $IsUpdate=0, $YearID=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang, $kis_lang_b5, $kis_lang_en;
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		$allClassLevel = $lac->getClassLevel();

		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            $YearID = $application_details['ApplyLevel'];
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			$SiblingInfo = $lac->getApplicationSibling($schoolYearID=$application_details['ApplyYear'],$classLevelID='',$applicationID=$application_details['ApplicationID'],$recordID='');
		}

		$isFirstYear = $lac->getIsFirstYear($YearID);

		@ob_start();
		include("template/admissionForm/otherForm.tmpl.php");
		$x = ob_get_clean();

		return $x;
	}


	/**
	 * Admission Form - create/edit form payment page
	 * /
	protected function getPaymentPage(){
		global $Lang;
		global $tempFolderPath, $fileData, $admission_cfg, $lac, $sys_custom, $intranet_root;

        $x = $this->getWizardStepsUI(4);

		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			$StatusInfo = $lac->getApplicationStatus($application_details['ApplyYear'],'',$application_details['ApplicationID']);
		}

		@ob_start();
		include("template/admissionForm/paymentForm.tmpl.php");
		$x .= ob_get_clean();


		/**** Step button START **** /
		$lastStepId = 'step_docs_upload';
		$currentStepId = 'step_payment';
		$nextStepId = 'step_confirm';

	    $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId);
		/**** Step button END **** /

		return $x;
	}/* */































	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){

			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'
//				</p>';
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'
////				</p>';
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['admissionQuotaFull'].'</h1>';
				$LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['interviewQuotaFull'].'</h1>';
				$LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}


	function getPayPalButton($ApplicationID){
		global $admission_cfg, $lac;
		$ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
		return '<form action="'.$admission_cfg['paypal_url'].'" <!--onsubmit="checkPayment(\''.$ApplicationID.'\');return false;"--> method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="'.$admission_cfg['hosted_button_id'].'">
				<input type="hidden" name="return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php" />
				    <input type="hidden" name="cancel_return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php?cm='.$ApplicationID.'" />
				    <input type="hidden" name="custom" value="'.$ApplicationID.'" />
					<input type="hidden" name="notify_url" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_ipn.php" />
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();

						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
	}

	function getAfterUpdateFinishPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';

		$x .=' <div class="admission_complete_msg"><h1>申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/>';
		$x .='Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></h1></div>';

		$x .= '<div class="admission_board">';
		$x .= $this->getDocsUploadForm(1, 1);

		$applicationStatus = current($lac->getApplicationStatus($lac->schoolYearID,'',$ApplicationID));

		if($applicationStatus['interviewdate'] && $applicationStatus['interviewdate'] != '0000-00-00 00:00:00'){
			$interviewDateTime = explode(" ", $applicationStatus['interviewdate']);
			$x .='<h1 style="font-size: 15px">面試資料 Interview Information</h1>';
			$x .= '<table class="form_table" style="font-size: 15px">';
			$x .= '<tr>';
			$x .= '<td class="field_title">面試日期 Interview Date</td>';
			$x .= '<td>'.$interviewDateTime[0].'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試時間 Interview Time</td>';
			$x .= '<td>'.substr($interviewDateTime[1], 0, -3).'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試地點 Interview Location</td>';
			$x .= '<td>'.$applicationStatus['interviewlocation'].'</td>';
			$x .= '</tr>';
			$x .= '</table>';
		}
		$x .= '</div>';

		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent;
		$x .= '</div>';

			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';


		return $x;
	}

}
?>