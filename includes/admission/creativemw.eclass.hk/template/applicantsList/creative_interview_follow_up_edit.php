<?php
global $libkis_admission;

$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

//// InterviewStatus START ////
$interviewStatus = $applicationCustInfo['InterviewStatus'][0]['Value'];
$interviewStatus = ($interviewStatus) ? $interviewStatus : $admission_cfg['InterviewStatus']['NotUpdated'];
$interviewStatusArr = array();
foreach ($admission_cfg['InterviewStatus'] as $key => $value) {
    $interviewStatusArr[] = array(
        $value, $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$key]
    );
}
$interviewStatusSelect = getSelectByArray($interviewStatusArr, ' name="applicantInterviewStatus"', $interviewStatus, 0, 1);
//// InterviewStatus END ////

//// RegisterEmailDate START ////
list($RegisterEmailDate_year, $RegisterEmailDate_month, $RegisterEmailDate_day) = explode('-', $applicationCustInfo['RegisterEmailDate'][0]['Value']);
//// RegisterEmailDate END ////

//// AdmitStatus START ////
$admitStatus = $applicationCustInfo['AdmitStatus'][0]['Value'];
$admitStatus = ($admitStatus) ? $admitStatus : $admission_cfg['AdmitStatus']['NotUpdated'];
$admitStatusArr = array();
foreach ($admission_cfg['AdmitStatus'] as $key => $value) {
    $admitStatusArr[] = array(
        $value, $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$key]
    );
}
$admitStatusSelect = getSelectByArray($admitStatusArr, ' name="AdmitStatus"', $admitStatus, 0, 1);
//// AdmitStatus END ////

//// RatingLevel START ////
$ratingLevelSelect = getSelectByValueDiffName(
    array_keys($libkis_admission->classLevelAry),
    array_values($libkis_admission->classLevelAry),
    ' name="RatingLevel"',
    $applicationCustInfo['RatingLevel'][0]['Value'],
    1,
    0,
    "- {$kis_lang['Admission']['pleaseSelect']} -"
);
//// RatingLevel END ////

//// RatingClass START ////
$ratingClassSelect = getSelectByValueDiffName(
    array_keys($kis_lang['Admission']['TimeSlot']),
    array_values($kis_lang['Admission']['TimeSlot']),
    ' name="RatingClass"',
    $applicationCustInfo['RatingClass'][0]['Value'],
    1,
    0,
    "- {$kis_lang['Admission']['pleaseSelect']} -"
);
//// RatingClass END ////

//// PaidReservedFee START ////
$paidReservedFeeSelect = getSelectByValueDiffName(
    array_keys($kis_lang['Admission']['CREATIVE']['Paid']),
    array_values($kis_lang['Admission']['CREATIVE']['Paid']),
    ' name="PaidReservedFee"',
    $applicationCustInfo['PaidReservedFee'][0]['Value'],
    1,
    1
);
//// PaidReservedFee END ////

//// PaidRegistrationCertificate START ////
$paidRegistrationCertificateSelect = getSelectByValueDiffName(
    array_keys($kis_lang['Admission']['CREATIVE']['Paid']),
    array_values($kis_lang['Admission']['CREATIVE']['Paid']),
    ' name="PaidRegistrationCertificate"',
    $applicationCustInfo['PaidRegistrationCertificate'][0]['Value'],
    1,
    1
);
//// PaidRegistrationCertificate END ////

//// BookFeeEmailDate START ////
list($BookFeeEmailDate_year, $BookFeeEmailDate_month, $BookFeeEmailDate_day) = explode('-', $applicationCustInfo['BookFeeEmailDate'][0]['Value']);
//// BookFeeEmailDate END ////

//// PaidBookFee START ////
$paidBookFeeSelect = getSelectByValueDiffName(
    array_keys($kis_lang['Admission']['CREATIVE']['Paid']),
    array_values($kis_lang['Admission']['CREATIVE']['Paid']),
    ' name="PaidBookFee"',
    $applicationCustInfo['PaidBookFee'][0]['Value'],
    1,
    1
);
//// PaidBookFee END ////

//// OpeningNoticeEmailDate START ////
list($OpeningNoticeEmailDate_year, $OpeningNoticeEmailDate_month, $OpeningNoticeEmailDate_day) = explode('-', $applicationCustInfo['OpeningNoticeEmailDate'][0]['Value']);
//// OpeningNoticeEmailDate END ////

//// PaidTuitionFee START ////
$paidTuitionFeeSelect = getSelectByValueDiffName(
    array_keys($kis_lang['Admission']['CREATIVE']['Paid']),
    array_values($kis_lang['Admission']['CREATIVE']['Paid']),
    ' name="PaidTuitionFee"',
    $applicationCustInfo['PaidTuitionFee'][0]['Value'],
    1,
    1
);
//// PaidTuitionFee END ////
?>
<style>
    select[disabled] {
        color: lightgrey;
    }
</style>
<table class="form_table">
    <tbody>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'] ?>
        </td>
        <td>
            <?= $interviewStatusSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'] ?>
        </td>
        <td>
            <input type="hidden" name="RegisterEmail" value="0"/>
            <input
                    type="checkbox"
                    id="RegisterEmail"
                    name="RegisterEmail"
                    value="1"
                <?= ($applicationCustInfo['RegisterEmail'][0]['Value']) ? 'checked' : '' ?>
            />
            <label for="RegisterEmail"><?= $kis_lang['Admission']['yes'] ?></label>
            &nbsp;
            &nbsp;
            (
            <label for="RegisterEmailDate"><?= $kis_lang['Admission']['date'] ?></label>:
            <span class="itemInput itemInput-selector">
                <span class="selector">
                    <select
                            id="RegisterEmailDate_year"
                            name="RegisterEmailDate_year"
                    >
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = date('Y') - 3; $i <= date('Y'); $i++): ?>
                            <option value="<?= $i ?>" <?= ($RegisterEmailDate_year == $i) ? 'selected' : '' ?>>
                                <?= $i ?>
                            </option>
                        <?php endfor; ?>
                    </select>
                </span> - <span class="selector">
                    <select
                            id="RegisterEmailDate_month"
                            name="RegisterEmailDate_month"
                    >
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                            <option value="<?= $i ?>" <?= ($RegisterEmailDate_month == $i) ? 'selected' : '' ?>>
                                <?= str_pad($i, 2, '0', STR_PAD_LEFT); ?>
                            </option>
                        <?php endfor; ?>
                    </select>
                </span> - <span class="selector">
                    <select
                            id="RegisterEmailDate_day"
                            name="RegisterEmailDate_day"
                    >
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = 1; $i <= 31; $i++): ?>
                            <option value="<?= $i ?>" <?= ($RegisterEmailDate_day == $i) ? 'selected' : '' ?>>
                                <?= str_pad($i, 2, '0', STR_PAD_LEFT); ?>
                            </option>
                        <?php endfor; ?>
                    </select>
                </span>
            </span>
            )
            <div class="remark remark-warn date" style="clear:both;display:none;color:red;">日期錯誤 Invalid date</div>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'] ?>
        </td>
        <td>
            <?= $admitStatusSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?>
        </td>
        <td>
            <?= $ratingClassSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'] ?>
        </td>
        <td>
            <?= $paidReservedFeeSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'] ?>
        </td>
        <td>
            <?= $paidRegistrationCertificateSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'] ?>
        </td>
        <td>
            <input type="hidden" name="BookFeeEmail" value="0"/>
            <input
                    type="checkbox"
                    id="BookFeeEmail"
                    name="BookFeeEmail"
                    value="1"
                <?= ($applicationCustInfo['BookFeeEmail'][0]['Value']) ? 'checked' : '' ?>
            />
            <label for="BookFeeEmail"><?= $kis_lang['Admission']['yes'] ?></label>
            &nbsp;
            &nbsp;
            (
            <label for="BookFeeEmailDate"><?= $kis_lang['Admission']['date'] ?></label>:
            <span class="itemInput itemInput-selector">
                <span class="selector">
                    <select id="BookFeeEmailDate_year" name="BookFeeEmailDate_year">
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = date('Y') - 3; $i <= date('Y'); $i++): ?>
                            <option value="<?= $i ?>" <?= ($BookFeeEmailDate_year == $i) ? 'selected' : '' ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </span> - <span class="selector">
                    <select id="BookFeeEmailDate_month" name="BookFeeEmailDate_month">
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                            <option value="<?= $i ?>" <?= ($BookFeeEmailDate_month == $i) ? 'selected' : '' ?>><?= str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
                        <?php endfor; ?>
                    </select>
                </span> - <span class="selector">
                    <select id="BookFeeEmailDate_day" name="BookFeeEmailDate_day">
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = 1; $i <= 31; $i++): ?>
                            <option value="<?= $i ?>" <?= ($BookFeeEmailDate_day == $i) ? 'selected' : '' ?>><?= str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
                        <?php endfor; ?>
                    </select>
                </span>
            </span>
            )
            <div class="remark remark-warn date" style="clear:both;display:none;color:red;">日期錯誤 Invalid date</div>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'] ?>
        </td>
        <td>
            <?= $paidBookFeeSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'] ?>
        </td>
        <td>
            <input type="hidden" name="OpeningNoticeEmail" value="0"/>
            <input
                    type="checkbox"
                    id="OpeningNoticeEmail"
                    name="OpeningNoticeEmail"
                    value="1"
                <?= ($applicationCustInfo['OpeningNoticeEmail'][0]['Value']) ? 'checked' : '' ?>
            />
            <label for="OpeningNoticeEmail"><?= $kis_lang['Admission']['yes'] ?></label>
            &nbsp;
            &nbsp;
            (
            <label for="OpeningNoticeEmailDate"><?= $kis_lang['Admission']['date'] ?></label>:
            <span class="itemInput itemInput-selector">
                <span class="selector">
                    <select id="OpeningNoticeEmailDate_year" name="OpeningNoticeEmailDate_year">
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = date('Y') - 3; $i <= date('Y'); $i++): ?>
                            <option value="<?= $i ?>" <?= ($OpeningNoticeEmailDate_year == $i) ? 'selected' : '' ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </span> - <span class="selector">
                    <select id="OpeningNoticeEmailDate_month" name="OpeningNoticeEmailDate_month">
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                            <option value="<?= $i ?>" <?= ($OpeningNoticeEmailDate_month == $i) ? 'selected' : '' ?>><?= str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
                        <?php endfor; ?>
                    </select>
                </span> - <span class="selector">
                    <select id="OpeningNoticeEmailDate_day" name="OpeningNoticeEmailDate_day">
                        <option value=''>- <?= $kis_lang['Admission']['pleaseSelect'] ?> -</option>
                        <?php for ($i = 1; $i <= 31; $i++): ?>
                            <option value="<?= $i ?>" <?= ($OpeningNoticeEmailDate_day == $i) ? 'selected' : '' ?>><?= str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
                        <?php endfor; ?>
                    </select>
                </span>
            </span>
            )
            <div class="remark remark-warn date" style="clear:both;display:none;color:red;">日期錯誤 Invalid date</div>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'] ?>
        </td>
        <td>
            <?= $paidTuitionFeeSelect ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'] ?>
        </td>
        <td>
            <input type="hidden" name="GiveUpOffer" value="0"/>
            <input
                    type="checkbox"
                    id="GiveUpOffer"
                    name="GiveUpOffer"
                    value="1"
                <?= ($applicationCustInfo['GiveUpOffer'][0]['Value']) ? 'checked' : '' ?>>
            <label for="GiveUpOffer"><?= $kis_lang['Admission']['yes'] ?></label>
        </td>
    </tr>
    </tbody>
</table>


<script>
    function updateUi(id) {
        $('select[name^="' + id + '"]').prop('disabled', !$('#' + id).prop('checked'));
    }

    updateUi('RegisterEmail');
    updateUi('BookFeeEmail');
    updateUi('OpeningNoticeEmail');
    $('#RegisterEmail, #BookFeeEmail, #OpeningNoticeEmail').change(function () {
        updateUi($(this).attr('id'));
    });


    function checkDate(fieldNameYear, fieldNameMonth, fieldNameDay) {
        var $fieldYear = $('[name="' + fieldNameYear + '"]');
        var $fieldMonth = $('[name="' + fieldNameMonth + '"]');
        var $fieldDay = $('[name="' + fieldNameDay + '"]');
        if ($fieldYear.length == 0 || $fieldMonth.length == 0 || $fieldDay.length == 0) {
            return true;
        }
        if ($fieldYear.val() == '' && $fieldMonth.val() == '' && $fieldDay.val() == '') {
            return true;
        }

        //// Check format START ////
        var year = parseInt($fieldYear.val());
        var month = parseInt($fieldMonth.val());
        var day = parseInt($fieldDay.val());
        var isValid = true;
        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            isValid = false;
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        isValid = isValid && day > 0 && day <= monthLength[month - 1];
        //// Check format END ////

        if (!isValid) {
            $fieldYear.closest('td').addClass('itemInput-warn');
            $fieldYear.closest('td').find('.remark-warn.date').removeClass('hide').show();
        }

        return isValid;
    }

    function check_input_info() {
        var isValid = true;

        $('.remark-warn.date').hide();
        isValid = checkDate('RegisterEmailDate_year', 'RegisterEmailDate_month', 'RegisterEmailDate_day') && isValid;
        isValid = checkDate('BookFeeEmailDate_year', 'BookFeeEmailDate_month', 'BookFeeEmailDate_day') && isValid;
        isValid = checkDate('OpeningNoticeEmailDate_year', 'OpeningNoticeEmailDate_month', 'OpeningNoticeEmailDate_day') && isValid;

        return isValid;
    }

    $('#applicant_form').unbind('submit').submit(function (e) {
        e.preventDefault();

        window.scrollTo(0, 0);
        var schoolYearId = $('#schoolYearId').val();
        var recordID = $('#recordID').val();
        var display = $('#display').val();
        var timeSlot = lang.timeslot.split(',');
        if (check_input_info()) {
            $.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function (success) {
                $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
            });
        }

        return false;
    });
</script>

