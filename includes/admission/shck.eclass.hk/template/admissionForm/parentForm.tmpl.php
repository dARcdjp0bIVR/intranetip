<h1>Parent Information <?=$Lang['Admission']['PGInfo']?></h1>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
    <tr>
    	<td>
    		<span>If no information, e.g. single parent family, fill in '<font style="color:red;">Nil</font>'</span>
    		<br/>
    		<span>如沒有資料，例如單親家庭，請輸入 '<font style="color:red;">沒有</font>'</span>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center>Father <?=$Lang['Admission']['HKUGAPS']['father'] ?></center></td>
		<td class="form_guardian_head"><center>Mother <?=$Lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		Name in Chinese
    		<?=$Lang['Admission']['chinesename']?> 
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1ChineseName'];
    		}else{?>
    			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$parentInfoArr['F']['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2ChineseName'];
    		}else{?>
    			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$parentInfoArr['M']['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		Name in English
    		<?=$Lang['Admission']['englishname']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1EnglishName'];
    		}else{?>
    			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$parentInfoArr['F']['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2EnglishName'];
    		}else{?>
    			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$parentInfoArr['M']['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	

	<tr>
		<td class="field_title">
    		Religion
    		<?=$Lang['Admission']['religion']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Religion'];
    		}else{?>
    			<input name="G1Religion" type="text" id="G1Religion" class="textboxtext" value="<?=$parentInfoArr['F']['Company'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Religion'];
    		}else{?>
    			<input name="G2Religion" type="text" id="G2Religion" class="textboxtext" value="<?=$parentInfoArr['M']['Company'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
			Occupation
    		<?=$Lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Occupation'];
    		}else{?>
    			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$parentInfoArr['F']['JobTitle'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Occupation'];
    		}else{?>
    			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$parentInfoArr['M']['JobTitle'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
			Office Address
    		<?=$Lang['Admission']['SHCK']['OfficeAddress']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo nl2br($formData['G1Address']);
    		}else{?>
    			<textarea name="G1Address" type="text" id="G1Address" class="textboxtext" style="height: 100px; resize: vertical;"><?=$parentInfoArr['F']['OfficeAddress'] ?></textarea>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo nl2br($formData['G2Address']);
    		}else{?>
    			<textarea name="G2Address" type="text" id="G2Address" class="textboxtext" style="height: 100px; resize: vertical;"><?=$parentInfoArr['M']['OfficeAddress'] ?></textarea>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
			Office Tel.
    		<?=$Lang['Admission']['SHCK']['OfficeTel']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1OfficeTel'];
    		}else{?>
    			<input name="G1OfficeTel" type="text" id="G1OfficeTel" class="textboxtext" value="<?=$parentInfoArr['F']['OfficeTelNo'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2OfficeTel'];
    		}else{?>
    			<input name="G2OfficeTel" type="text" id="G2OfficeTel" class="textboxtext" value="<?=$parentInfoArr['M']['OfficeTelNo'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		Mobile Tel.
    		<?=$Lang['Admission']['SHCK']['MobileTel']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1MobileNo'];
    		}else{?>
    			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$parentInfoArr['F']['Mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2MobileNo'];
    		}else{?>
    			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$parentInfoArr['M']['Mobile'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
</table>
