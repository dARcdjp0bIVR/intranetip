<style>
select:disabled{
    color: #ccc;
}
</style>

<h1>Student Information <?=$Lang['Admission']['studentInfo']?></h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star ?>
		Name in Chinese
		<?=$Lang['Admission']['chinesename']?> 
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_b5']?>
		<?php }else{ ?>
			<input name="studentsname_b5" type="text" id="studentsname_b5" class="textboxtext" value="<?=$applicationStudentInfo['student_name_b5']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		Name in English (same as on HKID Card)
		<?=$Lang['Admission']['englishname']?> (<?=$Lang['Admission']['UCCKE']['SameAsHKID']?>) <br />
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_en']?>
		<?php }else{ ?>
			<input name="studentsname_en" type="text" id="studentsname_en" class="textboxtext"  value="<?=$applicationStudentInfo['student_name_en']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		Date of Birth
		<?=$Lang['Admission']['dateofbirth']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15" value="<?=$applicationStudentInfo['dateofbirth']?>"/>(YYYY-MM-DD)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		Gender
		<?=$Lang['Admission']['gender']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['StudentGender'])? ($Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']) : ' -- ' ?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" <?=$applicationStudentInfo['gender'] == "M"?'checked':'' ?>>
			<label for="StudentGender1">M <?=$Lang['Admission']['genderType']['M']?></label>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender" <?=$applicationStudentInfo['gender'] == "F"?'checked':'' ?>>
			<label for="StudentGender2">F <?=$Lang['Admission']['genderType']['F']?></label>
		<?php } ?>
	</td>
</tr>


<tr>

	<td class="field_title">
		<?=$star ?>
		Birth Cert No. <font color="blue">(Cannot be changed after submission)</font><br />
		<?=$Lang['Admission']['SHCK']['birthCertNo']?> <font color="blue">(<?=$Lang['Admission']['SHCK']['birthCertNoHint'] ?>)</font>
	</td>
	<td colspan="3">
		<?php 
		if($IsConfirm){
		    echo $formData['StudentBirthCertNo'];
		}else{ 
		?>
			<table>
				<tr>
        			<?php if($BirthCertNo){ ?>
        				<?php if($IsUpdate){ ?>
        					<input name="StudentBirthCertNo" value="<?=$BirthCertNo ?>" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none;width: 250px;"/>
        				<?php }else{ ?>
        					<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$BirthCertNo?>"/>
	            			<br/>
	            			<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
	            			 <br/>(eg：A123456(7)，please enter "A1234567")
        				<?php } ?>	
        			<?php }else{ ?>
            			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationStudentInfo['birthcertno'] ?>"/>
            			<br/>
            			<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
            			 <br/>(eg：A123456(7)，please enter "A1234567")
        			<?php } ?>
        			</td>
				</tr>
			</table>

		<?php 
		} 
		?>
	</td>
	
</tr>


<tr>	
   	<td class="field_title">
		<?=$star?>
		Ethnicity
		<?=$Lang['Admission']['SHCK']['ethnicity']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $admission_cfg['Ethnicity'][ $formData['Nationality'] ];
		}else{
		?>
			<select id="Nationality" name="Nationality">
				<?php 
				foreach($admission_cfg['Ethnicity'] as $index=>$nationality){
			        $selected = ($index == $applicationStudentInfo['Nationality'])? 'selected':'';
				?>
					<option value="<?=$index ?>" <?=$selected ?>><?=$nationality ?></option>
				<?php 
				}
				?>
			</select>
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
		<?=$star ?>
		Place of Birth
		<?=$Lang['Admission']['placeofbirth']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $admission_cfg['PlaceOfBirth'][ $formData['StudentPlaceOfBirth'] ];
		}else{
		?>
			<select id="StudentPlaceOfBirth" name="StudentPlaceOfBirth">
				<?php 
				foreach($admission_cfg['PlaceOfBirth'] as $index=>$place){
				    if($applicationStudentInfo['PlaceOfBirth'] !== null){
				        $selected = ($index == $applicationStudentInfo['PlaceOfBirth'])? 'selected':'';
				    }else{
				        $selected = ($index == 101)? 'selected':''; // HK
				    }
				?>
					<option value="<?=$index ?>" <?=$selected ?>><?=$place ?></option>
				<?php 
				}
				?>
			</select>
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
		<?=$star ?>
		Spoken Language at Home
		<?=$Lang['Admission']['SHCK']['langspokenathome']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $admission_cfg['Language'][ $formData['LangSpokenAtHome'] ];
		}else{
		?>
			<select id="LangSpokenAtHome" name="LangSpokenAtHome">
				<?php 
				foreach($admission_cfg['Language'] as $index=>$lang){
			        $selected = ($index == $applicationStudentInfo['LangSpokenAtHome'])? 'selected':'';
				?>
					<option value="<?=$index ?>" <?=$selected ?>><?=$lang ?></option>
				<?php 
				}
				?>
			</select>
		<?php
		}
		?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star ?>
		Address
		<?=$Lang['Admission']['address']?> 
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo nl2br($formData['Address']);
		}else{
		?>
			<textarea name="Address" type="text" id="Address" class="textboxtext" style="height: 100px; resize: vertical;"><?=$applicationStudentInfo['Address'] ?></textarea>
		<?php
		}
		?>
	</td>
</tr>


<tr>
   	<td class="field_title">
		<?=$star ?>
		District
		<?=$Lang['Admission']['SHCK']['district']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $admission_cfg['District'][ $formData['District'] ];
		}else{
		?>
			<select id="District" name="District">
				<?php 
				foreach($admission_cfg['District'] as $index=>$district){
			        $selected = ($index == $applicationStudentInfo['District'])? 'selected':'';
				?>
					<option value="<?=$index ?>" <?=$selected ?>><?=$district ?></option>
				<?php 
				}
				?>
			</select>
		<?php
		}
		?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		Contact No.
		<?=$Lang['Admission']['SHCK']['ContactNo']?> 
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['HomeTelNo'];
		}else{
		?>
			<input name="HomeTelNo" type="text" id="HomeTelNo" class="textboxtext" value="<?=$applicationStudentInfo['HomeTelNo'] ?>"/>
		<?php
		}
		?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star ?>
		Email
		<?=$Lang['Admission']['email']?> 
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Email'];
		}else{
		?>
			<input name="Email" type="text" id="Email" class="textboxtext" value="<?=$applicationStudentInfo['Email'] ?>"/>
		<?php
		}
		?>
	</td>
	
	<?php
	if($IsConfirm){
	}else{
	?>
    	<td class="field_title">
    		<?=$star ?>
    		Re-enter Email
    		<?=$Lang['Admission']['SHCK']['ConfirmEmail']?> 
    	</td>
    	<td>
    			<input name="EmailConfirm" type="text" id="EmailConfirm" class="textboxtext" value="<?=$applicationStudentInfo['Email'] ?>"/>
    	</td>
	<?php
	}
	?>
</tr>


<tr>
   	<td class="field_title">
		Former School
		<?=$Lang['Admission']['SHCK']['FormerSchool']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['LastSchool'];
		}else{
		?>
			<input name="LastSchool" type="text" id="LastSchool" class="textboxtext" value="<?=$applicationStudentInfo['LastSchool'] ?>"/>
		<?php
		}
		?>
	</td>
	
	<td class="field_title">
		Class last attended
		<?=$Lang['Admission']['SHCK']['ClassLastAttended']?> 
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['LastSchoolLevel'];
		}else{
		?>
			<input name="LastSchoolLevel" type="text" id="LastSchoolLevel" class="textboxtext" value="<?=$applicationStudentInfo['LastSchoolLevel'] ?>"/>
		<?php
		}
		?>
	</td>
</tr>


<tr>
   	<td class="field_title">
		Religion
		<?=$Lang['Admission']['religion']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['ReligionOther'];
		}else{
		?>
			<input name="ReligionOther" type="text" id="ReligionOther" class="textboxtext" value="<?=$applicationStudentInfo['ReligionOther'] ?>"/>
		<?php
		}
		?>
	</td>
	
	
   	<td class="field_title">
		Baptism Cert. No. (if applicable)
		<?=$Lang['Admission']['SHCK']['BaptismCertNo']?> <?=$Lang['Admission']['ifapplicable'] ?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['BaptismCertNo'];
		}else{
		?>
			<input name="BaptismCertNo" type="text" id="BaptismCertNo" class="textboxtext" value="<?=$allCustInfo['BaptismCertNo'][0]['Value'] ?>"/>
		<?php
		}
		?>
	</td>
</tr>


<tr>
   	<td class="field_title">
		Church (if applicable)
		<?=$Lang['Admission']['SHCK']['Church']?> <?=$Lang['Admission']['ifapplicable'] ?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Church'];
		}else{
		?>
			<input name="Church" type="text" id="Church" class="textboxtext" value="<?=$applicationStudentInfo['Church'] ?>"/>
		<?php
		}
		?>
	</td>
</tr>


<tr>
   	<td class="field_title">
		Siblings
		<?=$Lang['Admission']['SHCK']['Siblings']?>
	</td>
	<td colspan="3">
    	<div style="margin-bottom: 5px;">
    		<label for="ElderBrother" style="width: 130px;display: inline-block;">
    			Elder Brother
    			<?=$Lang['Admission']['SHCK']['ElderBrother'] ?>
    		</label>
    		<?php
    		if($IsConfirm){
                echo ($formData['ElderBrother'])?$formData['ElderBrother']:'N/A';
    		}else{
    		?>
    			<select id="ElderBrother" name="ElderBrother">
    				<?php
    				    for($i=0;$i<=10;$i++){
    				        $selected = ($allCustInfo['ElderBrother'][0]['Value'] == $i)? 'selected' : '';
                            $text = ($i == 0)? 'N/A' : $i;
    				?>
    					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
    				<?php
    				    }
    				?>
    			</select>
    		<?php
    		}
    		?>
		</div>
	
    	<div style="margin-bottom: 5px;">
    		<label for="ElderSister" style="width: 130px;display: inline-block;">
    			Elder Sister
    			<?=$Lang['Admission']['SHCK']['ElderSister'] ?>
    		</label>
    		<?php
    		if($IsConfirm){
                echo ($formData['ElderSister'])?$formData['ElderSister']:'N/A';
    		}else{
    		?>
    			<select id="ElderSister" name="ElderSister">
    				<?php
    				    for($i=0;$i<=10;$i++){
    				        $selected = ($allCustInfo['ElderSister'][0]['Value'] == $i)? 'selected' : '';
                            $text = ($i == 0)? 'N/A' : $i;
    				?>
    					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
    				<?php
    				    }
    				?>
    			</select>
    		<?php
    		}
    		?>
		</div>
	
    	<div style="margin-bottom: 5px;">
    		<label for="YoungerBrother" style="width: 130px;display: inline-block;">
    			Younger Brother
    			<?=$Lang['Admission']['SHCK']['YoungerBrother'] ?>
    		</label>
    		<?php
    		if($IsConfirm){
                echo ($formData['YoungerBrother'])?$formData['YoungerBrother']:'N/A';
    		}else{
    		?>
    			<select id="YoungerBrother" name="YoungerBrother">
    				<?php
    				    for($i=0;$i<=10;$i++){
    				        $selected = ($allCustInfo['YoungerBrother'][0]['Value'] == $i)? 'selected' : '';
                            $text = ($i == 0)? 'N/A' : $i;
    				?>
    					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
    				<?php
    				    }
    				?>
    			</select>
    		<?php
    		}
    		?>
		</div>
	
    	<div style="">
    		<label for="YoungerSister" style="width: 130px;display: inline-block;">
    			Younger Sister
    			<?=$Lang['Admission']['SHCK']['YoungerSister'] ?>
    		</label>
    		<?php
    		if($IsConfirm){
                echo ($formData['YoungerSister'])?$formData['YoungerSister']:'N/A';
    		}else{
    		?>
    			<select id="YoungerSister" name="YoungerSister">
    				<?php
    				    for($i=0;$i<=10;$i++){
    				        $selected = ($allCustInfo['YoungerSister'][0]['Value'] == $i)? 'selected' : '';
                            $text = ($i == 0)? 'N/A' : $i;
    				?>
    					<option value="<?=$i ?>" <?=$selected ?>><?=$text ?></option>
    				<?php
    				    }
    				?>
    			</select>
    		<?php
    		}
    		?>
		</div>
	
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star ?>
		Choice of Language for interview
		<?=$Lang['Admission']['SHCK']['LangForInterview']?>
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		?>
			<?=$admission_cfg['LanguageForInterview'][ $formData['LangForInterview'] ]?>
		<?php 
		}else{ 
		    foreach($admission_cfg['LanguageForInterview'] as $key => $lang){
	            $checked = (isset($allCustInfo['LangForInterview']) && $key == $allCustInfo['LangForInterview'][0]['Value'])? 'checked' : '';
		?>
			<input type="radio" id="langForInterview<?=$key ?>" name="LangForInterview" value="<?=$key ?>" <?=$checked ?>/>
			<label for="langForInterview<?=$key ?>"><?=$lang ?></label>
			<br />
		<?php 
		    }
		} 
		?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		Session Choice
		<font color="blue">(No further change may be considered.)</font>
		<br />
		<?=$Lang['Admission']['SHCK']['SessionChoice']?>
		<font color="blue">(<?=$Lang['Admission']['SHCK']['SessionChoiceHint'] ?>)</font>
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		?>
			<?=$admission_cfg['SessionChoice'][ $formData['SessionChoice'] ]?>
		<?php 
		}else{ 
		    foreach($admission_cfg['SessionChoice'] as $key => $choice){
	            $checked = (isset($allCustInfo['SessionChoice']) && $key == $allCustInfo['SessionChoice'][0]['Value'])? 'checked' : '';
		?>
			<input type="radio" id="sessionChoice<?=$key ?>" name="SessionChoice" value="<?=$key ?>" <?=$checked ?>/>
			<label for="sessionChoice<?=$key ?>"><?=$choice ?></label>
			<br />
		<?php 
		    }
		} 
		?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$star ?>
		Application from Twins
		<?=$Lang['Admission']['KTLMSKG']['twins']?>
	</td>
	<td colspan="3">
		<?php 
		if($IsConfirm){ 
		?>
			<?=($formData['IsTwinsApplied']=="Y"?'Yes '.$Lang['Admission']['yes'].' (Twins\' Number of Identity Document '.$Lang['Admission']['KTLMSKG']['twinsID'].': '.strtolower($formData['TwinsBirthCertNo']).')':'No '.$Lang['Admission']['no'])?>
		<?php 
		}else{ 
		    ?>
			<?=$this->Get_Radio_Button('IsTwinsApplied1', 'IsTwinsApplied', 'Y', $applicationStudentInfo['IsTwinsApplied']=='Y','','Yes '.$Lang['Admission']['yes'].' (Twins\' Number of Identity Document '.$Lang['Admission']['KTLMSKG']['twinsID'].': <input name="TwinsBirthCertNo" type="text" value="'.$applicationStudentInfo['TwinsBirthCertNo'].'" id="TwinsBirthCertNo" class="textboxtext" maxlength="10" size="10" style="width:100px" disabled />) ',"document.getElementById('TwinsBirthCertNo').disabled = false;",0)?>
			<?=$this->Get_Radio_Button('IsTwinsApplied2', 'IsTwinsApplied', 'N', $applicationStudentInfo['IsTwinsApplied']=='N','','No '.$Lang['Admission']['no'],"document.getElementById('TwinsBirthCertNo').disabled = true;document.getElementById('TwinsBirthCertNo').value = '';",0)?>
		<?php 
		} 
		?>
	</td>
</tr>
</table>
