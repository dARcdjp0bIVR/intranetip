<?php
# modifying by: Pun

/********************
 * Log :
 * Date		2017-08-24 [Pun]
 * 			Modified getUpdateIndexContent(), fixed lang
 * Date		2017-08-15 [Pun]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step, $ApplicationID=''){
		global $Lang, $sys_custom,$lac, $validForAdmission, $kis_lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token'] && $Step!=7){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'<br/>This is preview form. Any information will not be submit.</center></h2>';
		}
		if($lac->isInternalUse($_REQUEST['token']) && ($Step!=7 || $lac->getTokenByApplicationNumber($ApplicationID)=='')){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}
				
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].' Instruction</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].' Personal Info</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].' Docs Upload</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>'.$Lang['Admission']['confirmation'].' Confirmation</a></div>
					<!--<div class="'.$active_step7.'"><a><span>5</span>'.$Lang['Admission']['submission'].' Submission</a></div>-->
					<!--<div><a><span>6</span>'.$Lang['Admission']['payment'].' Payment</a></div>-->
					<div class="'.$active_step7.' last_step"><span style="width:90px">5 '.$Lang['Admission']['finish'].' Finish</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].'<br/>Welcome to our online application page!'; //Henry 20131107
		}
		
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token']){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'<br/>This is preview form. Any information will not be submit.</center></h2>';
		}
		if($lac->isInternalUse($_GET['token'])){
			$previewnote .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm($ClassLevel);
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac, $sys_custom;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'].'<br/>Welcome to our online application page!'; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
		$HKUGAPS_cust = '<br /><br /><table style="font-size:small;">
						<tr>
							<td width="5">&nbsp;</td>
							<td align="center" width="1%" rowspan="2"><input type="checkbox" name="Agree" id="Agree" value="1" style="transform: scale(2);  -webkit-transform: scale(2);"/></td>
							<td width="5">&nbsp;</td>
							<td><label for="Agree">本人同意上述有關條款及細則。</label></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><label for="Agree">I agree the terms and conditions as stated above.</label></td>
						</tr>
					</table>';
					
         $x .= $Instruction.$HKUGAPS_cust;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_instruction','step_index')", "backToIndex", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN('開始填寫表格 Begin Application', "button", "goto('step_instruction','step_input_form')", "goToForm", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->';								
							if(!$lac->isInternalUse($_REQUEST['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
							}
							else{
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
							}
						$x .= '</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm($ClassLevel = ''){
		global $libkis_admission, $Lang, $lac, $sys_custom;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		/*
		//disable to choose class when using central server
		$disable = '';
		if($ClassLevel)
			$disable = 'return false';
		*/
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			//$class_level_selection .= '<p>英語、粵語及普通話&nbsp;&nbsp;&nbsp;English, Cantonese and Mandarin</p>';
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])){
					$hasClassLevelApply = 1;
					$selected = '';
					/*
					if($key == $ClassLevel)
						$selected = "checked='checked'";
						*/
					
					//Henry added [20140808]
					$numOFQuotaLeft = $lac->NumOfQuotaLeft($key,$value['SchoolYearID']);
					$isFullApply = false;
					if($numOFQuotaLeft <= 0){
						$isFullApply = true;
					}
					$disable = '';
					if($isFullApply){
						$selected = 'disabled';
					}
					
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="'.$disable.'" '.$selected.'  />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].($isFullApply?' <span style="color:red">('.$Lang['Admission']['IsFull'].' Full)</span>':'').'<!--(Quota Left:'.$numOFQuotaLeft.')-->'.'</label> ';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'<br/>Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'<br/>Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025
		//$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .='<fieldset ><legend>'.$Lang['Admission']['level'].' Level</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_index','step_instruction')", "goToInstruction", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
		$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm($BirthCertNo = ""){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission, $sys_custom, $admission_cfg, $lac;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm(0,$BirthCertNo);
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span>'.$Lang['Admission']['munsang']['mandatoryfield'].'</span>';
		$x .='<br/>「<span class="tabletextrequire">*</span>」are mandatory but if not applicable please fill in N.A.';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_input_form','step_instruction')", "backToInstruction", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_input_form','step_docs_upload')", "goToDocs", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->';
				if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0, $BirthCertNo = "", $IsUpdate=0){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg;
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			
			//debug_pr($formData);
			//debug_pr($application_details);
			if(count($application_details) > 0){
				$applicationStudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
				//debug_pr($applicationStudentInfo);
// 				$stuNameArr_en = explode(',',$applicationStudentInfo['student_name_en']);
// 				$stuNameArr_b5 = explode(',',$applicationStudentInfo['student_name_b5']);
			}
			## Current School Info START ##
			$studentCustInfo = $lac-> getApplicationStudentInfoCust($application_details['ApplyYear'],$classLevelID='',$application_details['ApplicationID'],$recordID='');
			## Current School Info END ##
		}
		
		
		$isBirthCertNo = 0;
		if(preg_match('/^[a-zA-Z][0-9]{6}[aA0-9]$/', $BirthCertNo)){
			$isBirthCertNo = 1;
		}
		
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		@ob_start();
		include("template/admissionForm/studentForm.tmpl.php");
		$x = ob_get_clean();
		
		if($IsUpdate){
			## Apply Year START ##
			$classLevelID = $applicationStudentInfo['classLevelID'];
			$allClassLevel = $lac->getClassLevel();
			$classLevel = $allClassLevel[$classLevelID];
			## Apply Year END ##
			
			$x .= '<h1>'.$Lang['Admission']['applyLevel'].' Student Information</h1>';
			$x .= '<table class="form_table" style="font-size: 13px"><tbody><tr>
				<td class="field_title">'.$Lang['Admission']['applyLevel'].' Student Information</td><td>'.$classLevel.'
				<input name="sus_status" type="text" id="sus_status" class="textboxtext" value="'.$classLevelID.'" hidden />		
				</td>
						
			</tr></tbody></table>';
			
		}
		return $x;
	}
	
	function getParentForm($IsConfirm=0, $IsUpdate=0){
		global $fileData, $formData, $Lang, $lac;
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

			$tmpParentInfoArr = $lac->getApplicationParentInfo($application_details['ApplyYear'],'',$_SESSION['KIS_ApplicationID']);
			$parentInfoArr = array();
			foreach($tmpParentInfoArr as $parent){
				foreach($parent as $para=>$info){
					$parentInfoArr[$parent['type']][$para] = $info;
				}
			}
			#### Parent Info END ####
		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		@ob_start();
		include("template/admissionForm/parentForm.tmpl.php");
		$x = ob_get_clean();
			
		return $x;
	}
	function getOthersForm($IsConfirm=0, $IsUpdate=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		$admission_year_start = substr($admission_year, 0, 4);
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
//		else{
//			foreach($dayTypeArr as $aDayType){
//	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
//	//			$dayTypeOption .=" ";
//				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
//			}
//			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
//		}
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			$StatusInfo = $lac->getApplicationStatus($application_details['ApplyYear'],'',$application_details['ApplicationID']);
			
			#### Siblings Info START ####
			$siblingsInfo = $lac->getApplicationRelativesInfoCust($application_details['ApplyYear'],'',$_SESSION['KIS_ApplicationID']);
			$siblingsInfoArr = array();
			foreach($siblingsInfo as $siblings){
				foreach($siblings as $para=>$info){
					$siblingsInfoArr[$siblings['type']][$para] = $info;
				}
			}
			
			#### Siblings Info END ####
			
			#### Referee Info START ####
			$referee = $allCustInfo['Referee_Name'][0]['Value'];
			#### Referee Info END ####
		}
		
		@ob_start();
		include("template/admissionForm/otherForm.tmpl.php");
		$x = ob_get_clean();
		
		return $x;
	}
	
	function getDocsUploadForm($IsConfirm=0, $IsUpdate=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac, $sys_custom, $formData,$intranet_root, $kis_lang;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			//debug_pr($application_details);
			if(count($application_details) > 0){
				$applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'],array('applicationID'=>$application_details['ApplicationID']));
				//debug_pr($applicationAttachmentInfo);
			}
			## Photo START ##
			$viewFilePath = (is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']]['personal_photo']['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['StudentPersonalPhoto'])?' <a href="download_attachment.php?type=personal_photo'./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]*/'" target="_blank" >檢視已遞交的檔案 View submitted file</a>':'');
			
			## Photo END ##
			
		}
		if(!$lac->isInternalUse($_GET['token'])){
			$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		}else{
			$star = '';
		}
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm && !$IsUpdate){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1 style="font-size: 15px">'.$Lang['Admission']['docsUpload'].' Documents Upload</h1>';
		}
		$x .='<table class="form_table" style="font-size: 15px">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)<br/>Document (image in JPEG/GIF/PNG/PDF format, file size less than : '.($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
		$x .='<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].' Personal Photo</td>
				<td>';

		if($IsConfirm){
			if($fileData['StudentPersonalPhoto']){
			    $x .= '<div id="StudentPersonalPhotoDiv">
    						<img src="#" style="max-width:600px" id="imgStudentPersonalPhoto"/>
    						<br/>
    					</div>';
		        $x .= stripslashes($fileData['StudentPersonalPhoto']);
		        $x .= '<a href="#" target="_blank" id="hrefStudentPersonalPhoto"> ['.$kis_lang['preview'].']</a>';
			}
		}else{
		    $x .= '<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png" />';
		}
		$x .= $viewFilePath;
		/*$x .='<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].' Personal Photo</td>
				<td>'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png"/>').$viewFilePath.'</td></tr>';
		
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').(is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['OtherFile'.$i])?' <a href="download_attachment.php?type='.$attachment_settings[$i]['AttachmentName']./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]* /'" target="_blank" >檢視已遞交的檔案 View submitted file</a>':'').'</td>
				  </tr>';
		}*/
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
    		$x .= '<tr>
    			<td class="field_title">'.($i<1?$star:'').$attachment_name.'</td>
    			<td>';
    		if($IsConfirm){
				if($fileData['OtherFile'.$i]){
    				$x .= '<div id="divOtherFile'.$i.'">
    						<img src="#" style="max-width:600px" id="imgOtherFile'.$i.'"/>
    						<br/>
    					</div>';
					$x .= stripslashes($fileData['OtherFile'.$i]);
					$x .= '<a href="#" target="_blank" id="hrefOtherFile'.$i.'"> ['.$kis_lang['preview'].']</a>';
				}
    		}else{
    				$x .= '<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />';
    		}
    				$x .= (is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['OtherFile'.$i])?' <a href="download_attachment.php?type='.$attachment_settings[$i]['AttachmentName'].'" target="_blank" >檢視已遞交的檔案 View submitted file</a>':'').'<br/>
    			</td>
    		</tr>';
		}
			
		
		if(!$IsConfirm && $IsUpdate){
			$x .='<tr>
					<td colspan="2">'.$star.'如不需更改文件檔案，則不用選擇檔案 if you no need to update document file, please make it blank</span></td>
				</tr>';
		}	
		/*$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';*/
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->';
			$x .= '</table>';
		if(!$IsConfirm && !$IsUpdate){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_docs_upload','step_input_form')", "backToForm", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_docs_upload','step_confirm')", "goToConfirm", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->';
				if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				
			$x.='</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	
	function stripslashesRecursive($formData){
		if($formData){
			foreach ($formData as $key=>$value) {
				if(is_array($value)){
					$formData[$key] = $this->stripslashesRecursive($value);
				}else{
					$formData[$key] = stripslashes($value);
					if($formData[$key] == ""){
						$formData[$key] =" -- ";
					}
				}
			}
		}
		return $formData;
	}
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;

		//remove the slashes of the special character
// 		if($formData){
// 			foreach ($formData as $key=>$value) {
// 				$formData[$key] = stripslashes($value);
// 				if($formData[$key] == ""){
// 					$formData[$key] =" -- ";
// 				}
// 			}
// 		}
		$formData = $this->stripslashesRecursive($formData);
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back']." Back", "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN("提交 Submit", "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
				if(!$lac->isInternalUse($_REQUEST['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
			$x.='</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7, $ApplicationID);
		if($ApplicationID){
			$x .=' <div class="admission_complete_msg"><h1><span style="display:inline">報名表已遞交，申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;</span><br/>';
			$x .='<span style="display:inline">Your application form has been successfully submitted.Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';

//			$x .='<br/><h1>現在請進行報名費付款 ，以完成報名程序。<br/>';            
//			$x .='To finish the online application, please pay now.<br/><br><input type="button" value="繳付 HK$100 報名費 Pay application fee HK$100" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'Payment Status\');"></input></h1>';            
//			
			$x .='<br/><h1><span>另外，閣下將會收到確認電郵，請檢查閣下在申請表填寫的電郵為 '.$lac->getApplicantEmail($ApplicationID).'。<br/>如未收到該電郵，請致電與本校聯絡。<br/>';            
			$x .='An acknowledgement will be sent to you via email service. Please ensure that the email address entered on the form is '.$lac->getApplicantEmail($ApplicationID).'. <br/>Please contact our school in case you fail to receive it.</span></h1>';            
			
//			$x .='<br/><h1><span>謝謝您使用網上報名服務！<br/>';            
//			$x .='Thank you for using our online application!</span></h1>';            
			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
//			
//			//add english version here...
//			$x .='<h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			
		
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>未能成功遞交申請。<span>請重新嘗試申請或致電與本校聯絡。</span></h1>';
            $x .='<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
            
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent || !$ApplicationID){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		if((!$lac->isInternalUse($_GET['token']) || $lac->getTokenByApplicationNumber($ApplicationID)!='') && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{				
			$x .= '<div class="edit_bottom">';
				//$x .= '<input id="finish_page_payment_button" type="button" class="formsubbutton" value="繳付 HK$100 報名費 Pay application fee HK$100" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'_blank\');"></input>';
				$x .= '<input id="finish_page_finish_button" style="" type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />';
			$x .= '</div>';
			$x .= '<p class="spacer"></p></div>';
		}
		return $x;
	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID='', $paymentEmail=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");

//		if($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID)==''){
//		    if($ApplicationID){
//		        $schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
//		        $stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
//		        
//		        $x .='閣下的小一郵遞報名申請表已收到。學童姓名為 '.$stuedentInfo[0]['student_name_b5'].'，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';
//		        $x .='Your application form has been received, name of child is '.$stuedentInfo[0]['student_name_en'].' and the application number is <u>'.$ApplicationID.'</u>.';
//		
//		        $x .='<br/><br/><br/>學校將透過電郵通知閣下有關面見安排，相關資訊可於學校網頁瀏覽。<br/>';
//		        $x .='You will be informed of arrangements for interview(s) via email. Please visit the school website for related information.';
//		
//		        $x .='<br/><br/><br/>謝謝您的申請！<br/>';
//		        $x .='Thanks for your application!';
//		
//		        if(!$LastContent){
//		            $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		        }
//		        $x .= '<br/><br/>'.$LastContent;
//		    }
//		}else{
    		if($ApplicationID){
    			$x .='報名表已遞交，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';         
    			$x .='Your application form has been successfully submitted and the application number is <u>'.$ApplicationID.'</u>.<br/>';
    			
    			$x .='<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
    			$x .='Click this hyperlink to view your application form.';
    			$x .="<br/><a target='_blank' href='http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."'>http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."</a>";
    			
    			$x .='<br/><br/><br/>如需要更改已遞交的申請資料，可於報名截止前按以下連結。<br/>';
    			$x .='Click this hyperlink to amend your application form before the application deadline.';
    			$x .="<br/><a target='_blank' href='http://{$_SERVER['HTTP_HOST']}/kis/admission_form/index_edit.php'>http://{$_SERVER['HTTP_HOST']}/kis/admission_form/index_edit.php</a>";
    			
    			$x .='<br/><br/><br/>謝謝您使用網上報名服務！<br/>';            
    			$x .='Thanks for lodging your application online!';
    			
    			$x .= '<br/><br/>'.$LastContent;
    		}else{
    			$x .='未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
    			$x .='<br/><br/>';
    			$x .='Your application is rejected. Please try to apply again or contact our school.';
            }
//        }
        
		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';            
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['admissionQuotaFull'].'</h1>';
				$LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['interviewQuotaFull'].'</h1>';
				$LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}

	function getPDFContent($schoolYearID,$applicationIDAry,$type=''){
		global $PATH_WRT_ROOT,$lac,$Lang,$admission_cfg, $setting_path_ip_rel, $intranet_session_language;
	
		$Lang['General']['EmptySymbol'] = '---';
		$yearStart = date('Y',getStartOfAcademicYear('',$schoolYearID));
		$yearEnd = (((int)$yearStart)+1);
		$settings = $lac->getApplicationSetting($schoolYearID);

		$selectedSchoolYear = current(kis_utility::getAcademicYears(array('AcademicYearID'=>$schoolYearID)));
		$selectedSchoolYearHTML = $selectedSchoolYear['academic_year_name_'.$intranet_session_language];
		
		######## Load Image Path to Variable START ########
		$imageNames = array(
			'checkbox','checked', 'photo'
		);
		foreach($imageNames as $names){
			$$names = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}/images/{$names}.png";
		}
		$logo = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}/images/logo.png";
		######## Load Image Path to Variable END ########
	
		######## Init PDF START ########
		$templateHeaderPath = $PATH_WRT_ROOT.'file/customization/'.$setting_path_ip_rel.'/pdf/header.php';
		$templatePath = $PATH_WRT_ROOT.'file/customization/'.$setting_path_ip_rel.'/pdf/application_form.php';
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		$margin_top = '7';
		$mpdf = new mPDF('','A4',0,'',5,5,10,5);
		$mpdf->mirrorMargins = 1;
		######## Init PDF END ########
	
		######## Load header to PDF START ########
		ob_start();
		include($templateHeaderPath);
		$pageHeader = ob_get_clean();
		
		$mpdf->WriteHTML($pageHeader);
		######## Load header to PDF END ########
		
		
		######## Load data to PDF START ########
		foreach((array)$applicationIDAry as $applicationID){
			
			#### All Cust Info START ####
			$allCustInfo = $lac->getAllApplicationCustInfo($applicationID);
			#### All Cust Info END ####
				
			## Current School Info START ##
			$studentCustInfo = $lac-> getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID,$recordID='');
			foreach($studentCustInfo as $key=>$school){
				$studentCustInfo[$key]['Class'] = explode(' - ', $school['Class']);
				$studentCustInfo[$key]['Year'] = explode(' - ', $school['Year']);
			}
			## Current School Info END ##
			
			#### Student Info START ####
			$StuInfoArr = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));

			## Photo START ##
			$attachmentArr = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
			$photoLink = $attachmentArr['personal_photo']['link'];
			$photoLink = ($photoLink)?$photoLink:$photo;
			## Photo END ##
			
			## Age Range START ##
			$ageRangeStart = $settings[ $StuInfoArr['classLevelID'] ]['DOBStart'];
			$ageRangeStart = explode('-',$ageRangeStart);
			$ageRangeStart = array_reverse($ageRangeStart);
			$ageRangeStart = implode('/',$ageRangeStart);
			
			$ageRangeEnd = $settings[ $StuInfoArr['classLevelID'] ]['DOBEnd'];
			$ageRangeEnd = explode('-',$ageRangeEnd);
			$ageRangeEnd = array_reverse($ageRangeEnd);
			$ageRangeEnd = implode('/',$ageRangeEnd);
			
			$ageRangeHTML = "{$ageRangeStart} - {$ageRangeEnd}";
			## Age Range END ##
			
			$StuInfoArr['dateofbirth'] = explode('-',$StuInfoArr['dateofbirth']);
			$StuInfoArr['dateofbirth'] = array_reverse($StuInfoArr['dateofbirth']);
			$StuInfoArr['dateofbirth'] = implode(' / ',$StuInfoArr['dateofbirth']);
			#### Parent Info START ####
			$tmpParentInfoArr = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
			$parentInfoArr = array();
			foreach($tmpParentInfoArr as $parent){
				foreach($parent as $para=>$info){
					$parentInfoArr[$parent['type']][$para] = $info;
				}
			}
			#### Parent Info END ####

				
			#### Load Template START ####
			ob_start();
			include($templatePath);
			$page1 = ob_get_clean();
			#### Load Template END ####
	
			$mpdf->WriteHTML($page1);
		}
		######## Load data to PDF END ########
	
// 				echo $pageHeader;
// 				echo $page1;
		$mpdf->Output();
	}
	/*
	function getPrintPageContent($schoolYearID,$applicationID, $type=""){ //using $type="teacher" if the form is print from teacher
		global $PATH_WRT_ROOT,$Lang,$kis_lang, $admission_cfg;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		$lac = new admission_cust();
		if($applicationID != ""){
		//get student information
		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
		foreach($parentInfo as $aParent){
			if($aParent['type'] == 'F'){
				$fatherInfo = $aParent;
			}
			else if($aParent['type'] == 'M'){
				$motherInfo = $aParent;
			}
			else if($aParent['type'] == 'G'){
				$guardianInfo = $aParent;
			}
		}
		
		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
		
		//for the 2 new table
		$studentInfoCust = $lac->getApplicationStudentInfoCust($schoolYearID,'',$applicationID);
		$relativesInfoCust = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID);
		
		if($_SESSION['UserType']==USERTYPE_STAFF){
			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
			if(!is_date_empty($remarkInfo['interviewdate'])){
				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
				list($y,$m,$d) = explode('-',$date);
				if($hour>12){
					$period = '下午';
					$hour -= 12;
				}elseif($hour<12){
					$period = '上午';
				}else{
					$period = '中午';
				}
				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
			}else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
			}
		}
		else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
		}
		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
		$personalPhotoPath = $attachmentList['personal_photo']['link'];
		$classLevel = $lac->getClassLevel();
		
//		debug_pr($studentInfo);
//		debug_pr($fatherInfo);
//		debug_pr($motherInfo);
//		debug_pr($guardianInfo);
//		debug_pr($othersInfo);
		
		for($i=1; $i<=3; $i++){
				if($othersInfo['ApplyDayType'.$i] != 0){
					//$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
					$dayTypeOption .= "(選擇 ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
				}
			}
		
		foreach($admission_cfg['BirthCertType'] as $_key => $_type){
			if($studentInfo['birthcerttype'] == $_type){
				$birth_cert_type_selection .= ' (';
				$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
				$birth_cert_type_selection .= ') ';
				break;
			}
		}
		
		$stuNameArr_en = explode(',',$studentInfo['student_name_en']);
		$stuNameArr_b5 = explode(',',$studentInfo['student_name_b5']);
		
		//Header of the page		
		$x ='<div id="content">
			<div class="top">
			<div class="logo"><img src="/includes/admission/eclassk.munsang.edu.hk/images/logo01.png" width="115px"/></div>  
			<span class="heading">
			<em>
			民生書院幼稚園<br />
			Munsang College Kindergarten<br />
			'.date('Y',getStartOfAcademicYear('',$othersInfo['schoolYearId'])).'-'.
					date('Y',getEndOfAcademicYear('',$othersInfo['schoolYearId'])).' 年度<br />
			</em>
			報名表<br />
			Application Form
			</span>
			<span class="logo02"><img src="/includes/admission/eclassk.munsang.edu.hk/images/logo02.png" width="70px"/></span>
			<div class="application_form_no_block">
			<div class="chinese">報名表編號:</div>
			<div class="eng">Application Form No.</div>
			<div class="eng"><span class="underline" style="width:50px;">'.$othersInfo['applicationID'].'</span></div>
			<div class="chinese">出生証明文件:</div>
			<div class="eng">Birth Cert:</div>
			<div class="eng"><span class="underline" style="width:100px;"></span></div>
			<div class="chinese">備註:</div>
			<div class="eng">Remark:</div>
			<div class="eng"><span class="underline" style="width:110px;"></span></div>
			<div class="school">(只供校方填寫)<br />School use only</div>
			</div>
			</div> <!--end_top-->
			
			<div style="clear:both"></div>
			<div class="application_form_block">';
			
			if(date('Y',getEndOfAcademicYear('',$othersInfo['schoolYearId'])) <= 2016){
				$x.='<table class="application_form_table_class">
				  <tr>
				    <td width="65" rowspan="3" style="border:none; line-height:16px;">申請班級&nbsp;*<br/> Class<br/>Applied For</td>
				    <td width="119" rowspan="3" class="title">幼兒班<br />Nursery Class</td>
				    <td width="242"> 
				    <span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Cantonese A.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
					&nbsp;&nbsp;上午英粵班&nbsp;&nbsp; Eng / Cantonese A.M.    </td>
				    <td width="329" rowspan="3"  style="border:none; border-left:1px solid #000000;">如申請的學習時段額滿，是&nbsp; 
				    
				    <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> / &nbsp;否&nbsp;  <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span><br />願意由學校重新編配。<br />If the session I have applied for is full,<br /> 
				    I will &nbsp;
				       <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>  / &nbsp;will not &nbsp; <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
				      consider the alternative session.</td>
				    </tr>
				  <tr>
				    <td >
					<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Cantonese P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
				         &nbsp;&nbsp;下午英粵班&nbsp;&nbsp; Eng / Cantonese P.M.    </td>
				    </tr>
				  <tr>
				    <td >
					<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Mandarin P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>       
				        &nbsp;&nbsp;下午英普班&nbsp;&nbsp; Eng / Mandarin P.M.    </td>
				    </tr>
				</table>';
			}
			else{
				$x.='<table class="application_form_table_class">
				  <tr>
				    <td width="160" rowspan="3" style="border-right:1px solid #666; line-height:16px;">申請班級&nbsp;*<br/> Class Applied For</td>
				    <!--<td width="119" rowspan="3" class="title">幼兒班<br />Nursery Class</td>-->
				    <td width="242" style="border-bottom:1px solid #666;text-align:center;background-color: #e2e2e2;"> 
				   	 幼兒班 Kindergarten One</td>
				    <td width="353" rowspan="3"  style="border:none; border-left:1px solid #000000;">如申請的學習時段額滿，是&nbsp; 
				    
				    <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> / &nbsp;否&nbsp;  <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span><br />願意由學校重新編配。<br />If the session I have applied for is full,<br /> 
				    I will &nbsp;
				       <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>  / &nbsp;will not &nbsp; <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
				      consider the alternative session.</td>
				    </tr>
				  <tr>
				    <td style="text-align:center">
					<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'A.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
				         &nbsp;&nbsp;上午&nbsp;&nbsp;A.M.    </td>
				    </tr>
				  <tr>
				    <td style="text-align:center">
					<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>       
				        &nbsp;&nbsp;下午&nbsp;&nbsp;P.M.    </td>
				    </tr>
				</table>';
			}
			
			$x.='<table class="application_form_table_information"> 
			  <tr>
			    <td width="84" rowspan="2" class="border_t_l_r_none" style="text-align:center;">姓名&nbsp;*<br />
			      Name</td>
			    <td width="58" class="border_t_r_none">中文<br />
			      Chinese</td>
			    <td colspan="3" class="border_t_l_r_none">'.$stuNameArr_b5[0].' '.$stuNameArr_b5[1].'</td>
			    <td width="180" rowspan="6" class="border_t_r_b_none" ><div class="photo">';
			    if($personalPhotoPath)
			   		$x .='<img src="'.$personalPhotoPath.'" style="height:100%;max-width: 4cm;max-height: 4.5cm" />';
			    else
			    	$x .='<span class="photo_txt" style="width:auto">近照<br />Recent Photo<br/><font style="font-size:10px">40mm (w) X 50mm (h)</font></span>';
			    $x.='</div></td>
			  </tr>
			  <tr>
			    <td class="border_t_r_none">英文<br />
			      English</td>
			    <td colspan="3" class="border_t_l_r_none">'.$stuNameArr_en[0].' '.$stuNameArr_en[1].'</td>
			    </tr>
			  <tr>
			    <td colspan="2" class="border_t_l_r_none">出生日期&nbsp;*<br />Date of Birth</td>
			    <td class="border_t_l_r_none">'.substr($studentInfo['dateofbirth'], 0, 4).' 年 '.substr($studentInfo['dateofbirth'], 5, 2).' 月 '.substr($studentInfo['dateofbirth'], 8, 2).' 日</td>
			    <td width="68" colspan="-2"  class="border_t_r_none">性別&nbsp;*<br />
			      Sex</td>
			    <td width="140"  class="border_t_l_r_none">'.$Lang['Admission']['genderType'][$studentInfo['gender']].'<br/>'.$studentInfo['gender'].'</td>
			  </tr>
			  <tr>
			    <td colspan="2" class="border_t_l_r_none">出生地點&nbsp;*<br />Place of Birth</td>
			    <td class="border_t_l_r_none">'.$studentInfo['placeofbirth'].'</td>
			    <td colspan="-2"  class="border_t_r_none">宗教<br />Religion</td>
			    <td  class="border_t_l_r_none">'.$studentInfo['religion'].'</td>
			  </tr>
			  <tr>
			    <td colspan="2" class="border_t_l_r_none">出生證明書號碼&nbsp;*<br />Birth Certificate Number</td>
			    <td width="217" class="border_t_l_r_none">'.$studentInfo['birthcertno'].'</td>
			    <td colspan="-2"  class="border_t_r_none">電話&nbsp;*<br />Telephone</td>
			    <td  class="border_t_l_r_none">'.$studentInfo['homephoneno'].'</td>
			  </tr>
			  <tr>
			    <td class="border_none">地址&nbsp;*<br />Address</td>
			    <td colspan="4" class="border_none">'.$studentInfo['homeaddress'].'</td>
			    </tr>
			</table>
			
			
			<table class="application_form_table_parents">
			  <tr>
			    <td width="145"  class="border_t_l_r_none">&nbsp;</td>
			    <th width="272"  class="border_t_r_none">父親 Father</th>
			    <th width="276"  class="border_t_r_none">母親 Mother</th>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">姓名&nbsp;*<br />Name
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['parent_name_b5'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['parent_name_b5'].'&nbsp;</center></td>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">公司<br />Company
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['companyname'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['companyname'].'&nbsp;</center></td>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">地址<br />Address
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['companyaddress'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['companyaddress'].'&nbsp;</center></td>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">職業<br />Occupation
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['occupation'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['occupation'].'&nbsp;</center></td>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">手提電話&nbsp;*<br />Mobile Phone No.
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['mobile'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['mobile'].'&nbsp;</center></td>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">電郵地址<br />E-mail Address
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['email'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['email'].'&nbsp;</center></td>
			  </tr>
			  <tr>
			    <td class="title border_t_l_r_none">教育程度<br />Level of Education
			</td>
			    <td class="border_t_r_none"><center>'.$fatherInfo['levelofeducation'].'&nbsp;</center></td>
			    <td class="border_t_r_none"><center>'.$motherInfo['levelofeducation'].'&nbsp;</center></td>
			  </tr>';
			if(date('Y',getEndOfAcademicYear('',$othersInfo['schoolYearId'])) <= 2016){
			  $x.='<tr>
			    <td class="title border_none">學校名稱<br />Name of School
			</td>
			    <td class="border_t_r_b_none"><center>'.$fatherInfo['lastschool'].'&nbsp;</center></td>
			    <td  class="border_t_r_b_none"><center>'.$motherInfo['lastschool'].'&nbsp;</center></td>
			  </tr>';
			}
			$x.='</table>
			
			<table  class="application_form_table_record">
			  <tr>
			    <td colspan="3" class="title border_t_l_r_none">幼兒過去入學記錄 Record of Previous Schooling of the Child*</td>
			    </tr>
			  <tr>
			    <th width="159"  class="border_t_l_r_none">年份 Year</th>
			    <th width="160" class="border_t_r_none">級別 Class</th>
			    <th width="440"  class="border_t_r_none">學校名稱 Name of School</th>
			    </tr>';
			    
			  	foreach($studentInfoCust as $aStudentInfoCust){
					if(trim($aStudentInfoCust['OthersPrevSchYear']) != '' || trim($aStudentInfoCust['OthersPrevSchClass']) != '' || trim($aStudentInfoCust['OthersPrevSchName']) != '')
					$x .= '<tr>
							<td class="border_t_l_r_none"><center>'.$aStudentInfoCust['OthersPrevSchYear'].'&nbsp;</center></td>
							<td class="border_t_r_none"><center>'.$aStudentInfoCust['OthersPrevSchClass'].'&nbsp;</center></td>
							<td class="border_t_r_none"><center>'.$aStudentInfoCust['OthersPrevSchName'].'&nbsp;</center></td>
						</tr>';
				}
				
			$x.='</table>
			
			<table  class="application_form_table_record">
			  <tr>
			    <td colspan="4" class="title border_t_l_r_none" style="text-align:left;">請列出曾經或現正在本校就讀/工作的親屬資料 (如有)<br />
			Please list all the relatives who have studying / working or having studied /worked at our College (if applicable)
			</td>
			    </tr>
			  <tr>
			    <th width="131" class="border_t_l_r_none">年份 Year</th>
			    <th width="133"  class="border_t_r_none">姓名 Name</th>
			    <th width="191"  class="border_t_r_none">就讀班別 / 職位<br />
			      Class / Position
			</th>
			    <th width="300"  class="border_t_r_none">與申請人的關係<br />
			      Relationship with the Applicant
			</th>
			  </tr>';
			  
				$hasRelativesInfoCust = 0;
				foreach($relativesInfoCust as $aRelativesInfoCust){
					if(trim($aRelativesInfoCust['OthersRelativeStudiedYear']) != '' || trim($aRelativesInfoCust['OthersRelativeStudiedName']) != '' || trim($aRelativesInfoCust['OthersRelativeClassPosition']) != '' || trim($aRelativesInfoCust['OthersRelativeRelationship']) != ''){
						$x .= '<tr>
							<td class="border_t_l_r_none"><center>'.$aRelativesInfoCust['OthersRelativeStudiedYear'].'</center></td>
							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeStudiedName'].'</center></td>
							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeClassPosition'].'</center></td>
							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeRelationship'].'</center></td>
						</tr>';
						$hasRelativesInfoCust++;
					}
				}
				if($hasRelativesInfoCust < 2){
					if($hasRelativesInfoCust == 0){
						$x .= '<tr>
							    <td  class="border_t_l_r_none">&nbsp;</td>
							    <td  class="border_t_r_none">&nbsp;</td>
							    <td  class="border_t_r_none">&nbsp;</td>
							    <td  class="border_t_r_none">&nbsp;</td>
							  </tr>';
					}
					$x .= '<tr>
						    <td  class="border_none">&nbsp;</td>
						    <td  class="border_t_r_b_none">&nbsp;</td>
						    <td  class="border_t_r_b_none">&nbsp;</td>
						    <td  class="border_t_r_b_none">&nbsp;</td>
						  </tr>';
				}
				 
			$x.='</table>
			
			<div class="footer">
			<span>
			根據個人資料(私穩)條例，以上個人資料只用於報讀幼稚園，完成報名程序後，所有資料將會註銷。<br />
			According to the Personal Data (Privacy) Ordinance, the above Personal data will be used for Kindergarten application only. All data will be written off after application procedure completed.</span>
			
			<span>
			備註Note:
			<ul>
			<li>必須提供標示*的資料，否則申請將不能進行，並視作放棄論。<br />
			   The * marked information are compulsory and must be provided in order for this application to be processed. Failure to provide such data will be treated as withdrawn.
			</li>
			<li>沒有標示*的資料為非必要資料，家長/申請人可自行決定提供與否。<br />
			   Information not marked with * are optional. Parent/Applicant can decide whether or not to provide the information.
			</li>
			</ul>
			</span>
			</div>
			</div><!--end_application_form_block-->
			</div>';	
			return $x;
		}
		else
			return false;
	}
	
	function getPrintPageCss(){
		return '<style type="text/css">
			body { padding:0; margin:0; }
			body{	font-family: "Times New Roman", Times, serif,"新細明體" "細明體", "微軟正黑體";color: #000000;}
			#content{ width:21cm; height:29.7cm; background-color:#fff;  margin:0 auto;}
			.top{ padding:0; float:left; width:21cm;}
			.logo{width:115px; height:120px; float:left; margin-right:1.8cm; }
			.logo02{width:80px; height:95px; float:left; }
			/*--top--* /
			.top .heading{float:left; margin-right:1.2cm; text-align:center; line-height:23px;font-size:1.2em;}
			.top .heading em{font-size:1.1em; line-height:28px; font-style:normal}
			/*--application_form_no_block--* /
			.application_form_no_block{ float:right; padding:5px 10px 3px 10px; border:2px dotted #000000;}
			.application_form_no_block .chinese{font-size:0.75em; clear:both; display:block;}
			.application_form_no_block .eng{font-size:0.7em; float:left; margin-bottom:3px; line-height:15px;}
			.application_form_no_block .school{font-size:0.65em; clear:both; display:block; text-align:center;}
			.underline { border-bottom:1px solid #666;color:#000; float:left; line-height:15px; font-size:1.3em; padding:0px 5px 2px 5px; display:block; height:12px;}
			.application_form_block{clear:both; border:1px solid #000; width:100%; margin-top:5px;}
			/* -----form_table_class----* /
			table.application_form_table_class{  border-collapse:collapse; border:1px solid #666; border-bottom:none; font-size:0.8em; margin:10px auto 0px auto; width:20.5cm; height:20px;  }
			.application_form_table_class td {font-weight: normal;color: #000;text-align:left; line-height:15px;padding:2px 5px 3px 5px; height:15px;}
			.application_form_table_class td.title {font-weight:bold; color: #000;text-align:center;}
			.photo{border:1px dotted #000; width:4cm; height:4.5cm; display:block; margin:auto; text-align:center;}
			.photo_txt{margin:60px auto; display: inline-block; width:50px; text-align:center; line-height:22px; font-size:1.2em;}
			
			/* -----form_table_information----* /
			table.application_form_table_information{ border-collapse:collapse; border:1px solid #666; font-size:0.8em;  margin:0px auto 0px auto; width:20.5cm;}
			.application_form_table_information th {background-color: #f5c567; font-weight: bold; color: #000;text-align:left;  padding:10px; border:1px solid #666; }
			.application_form_table_information td {font-weight: normal;color: #000;text-align:left; line-height:15px; border:1px solid #666; padding:3px 5px 2px 5px;}
			.application_form_table_information td.title {font-weight: normal; color: #000;text-align:center;}
			
			/*-----form_table_parents----* /
			table.application_form_table_parents{ border-collapse:collapse; border:1px solid #666; font-size:0.8em;  margin:10px auto 0px auto; width:20.5cm;}
			.application_form_table_parents th {background-color: #f5c567; font-weight: normal; color: #000;text-align:center;  padding:10px 5px 10px 5px; border:1px solid #666; background-color:#e2e2e2;}
			.application_form_table_parents td {font-weight: normal;color: #000;text-align:left; line-height:15px; border:1px solid #666;  padding:3px 5px 2px 5px;}
			.application_form_table_parents td.title {text-align:center; padding:3px 5px 2px 5px;}
			
			/*-----form_table_record----* /
			table.application_form_table_record{ border-collapse:collapse; border:1px solid #666; font-size:0.9em;  margin:10px auto 5px auto; width:20.5cm;}
			.application_form_table_record th {background-color: #e2e2e2; font-weight:normal; color: #000;text-align:center;  padding:2px 5px 2px 5px; border:1px solid #666;  line-height:15px; font-size:0.9em;}
			.application_form_table_record td {font-weight: normal;color: #000;text-align:left; line-height:13px; border:1px solid #666;  padding:3px 5px 3px 5px; font-size:0.9em; height:20px;}
			.application_form_table_record td.title {text-align:center; padding:3px 5px 2px 5px;  line-height:13px;}
			
			/*------check_box-----* /
			.check_box{ width:11px; height:11px; border:1px solid #1f1f1f; display:inline-block; vertical-align:middle; }			

			/*----footer---* /
			.footer{font-weight: normal;color: #000;text-align:left; line-height:12px; width:20.5cm; font-size:0.7em;  margin:7px auto 7px auto;}
			.footer span{ margin-bottom:7px; display:block; line-height:15px;}
			.footer ul{ margin:0; padding-left:15px;}
			.footer li{list-style:decimal; }
			/*---border----* /
			td.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
			th.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
			td.border_t_r_none{border-top:none;border-right:none;}
			th.border_t_r_none{border-top:none;border-right:none;}
			td.border_t_r_b_none{border-top:none;border-right:none;border-bottom:none;}
			td.border_none{border:none;}
			@media print
			{    
			    .print_hide, .print_hide *
			    {
			        display: none !important;
			    }
			}
			</style>';
	}*/
	
	function getPayPalButton($ApplicationID){
		global $admission_cfg, $lac;
		$ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
		return '<form action="'.$admission_cfg['paypal_url'].'" <!--onsubmit="checkPayment(\''.$ApplicationID.'\');return false;"--> method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="'.$admission_cfg['hosted_button_id'].'">
				<input type="hidden" name="return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php" /> 
				    <input type="hidden" name="cancel_return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php?cm='.$ApplicationID.'" />
				    <input type="hidden" name="custom" value="'.$ApplicationID.'" />
					<input type="hidden" name="notify_url" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_ipn.php" /> 
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();
				        
						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
	}
	
	function getUpdateWizardStepsUI($Step, $ApplicationID=''){
		global $Lang, $sys_custom,$lac, $validForAdmission, $kis_lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$active_step8 ="";
		$active_step9 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		$href_step8 ="";
		$href_step9 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_update_input_form\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		    case 8:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="completed-step";
		        $active_step8 ="active-step";
		        break;
		    case 9:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="completed-step";
		        $active_step8 ="completed-step";
		        $active_step9 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultupdatepagemessage'] .'<br/>This page is for update application information.</center></h2>';
		if($lac->isInternalUse($_REQUEST['token']) && ($Step!=7 || $lac->getTokenByApplicationNumber($ApplicationID)=='')){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}		
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<!--<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].' Instruction</a></div>-->
					<!--<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].' Personal Info</a></div>-->
					<div class="'.$active_step5.' first_step"><a '.$href_step5.'><span>1</span>'.'資料更改 Information Update</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>2</span>'.$Lang['Admission']['confirmation'].' Confirmation</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:90px">3 '.$Lang['Admission']['finish'].' Finish</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getUpdateIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].'<br/>Welcome to our online application page!'; //Henry 20131107
		}
		
		if($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && $lac->IsAfterUpdatePeriod()){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultviewpagemessage'] .'<br/>This page is for view application information.</center></h2>';
		}
		else{
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultupdatepagemessage'] .'<br/>This page is for update application information.</center></h2>';
		}
		if($lac->isInternalUse($_GET['token'])){
			$previewnote .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && $lac->IsUpdatePeriod()){
							$star = '<font style="color:red;">*</font>';
							
							//$x .='<form id="form1" name="form1">';
							$x .= '<table class="form_table" style="font-size: 13px">';
							$x .= '<tr>';
							$x .= '<td class="field_title">'.$star.'輸入申請編號 Application Number</td>';
							$x .= '<td><input style="width:200px" name="InputApplicationID" type="text" id="InputApplicationID" class="textboxtext" maxlength="16" size="8"/></td>';
							$x .= '</tr>';
							$x .= '<tr>';
							$x .= '<td class="field_title">'.$star.'輸入出生證明書號碼 Birth Certificate Number</td>';
							$x .= '<td><input style="width:200px" name="InputStudentBirthCertNo" type="text" id="InputStudentBirthCertNo" class="textboxtext" maxlength="64" size="8"/><br />不需要輸入括號，例如：A123456(7)，請輸入 "A1234567"。<br/>No need to enter the brackets. Eg. A123456(7), please enter "A1234567".</td>';
							$x .= '</tr>';
							$x .= '<tr>';
							$x .= '<td class="field_title">'.$star.'輸入出生日期 Date of Birth</td>';
							$x .= '<td><input style="width:200px" name="InputStudentDateOfBirth" type="text" id="InputStudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)</td>';
							$x .= '</tr>';
							$x .= '</table>';
							//$x .='</form>';
							
							$x .= '<div class="edit_bottom">
									'.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_index','step_update_input_form')", "goToForm", "", 0, "formbutton").'
								</div>
								<p class="spacer"></p>';
								
							$x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
							$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}
	
	function getInputFormPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		if($lac->IsUpdatePeriod()){
			//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
			//x .= '<div class="admission_board">';
			$x .= $this->getUpdateWizardStepsUI(5);
			
			$x .=$this->getStudentForm(0, $fileData['InputStudentBirthCertNo'], 1);
			$x .= $this->getParentForm(0,1);
			$x .= $this->getOthersForm(0,1);
			$x .= $this->getDocsUploadForm(0,1);
			$x .= '</div>
				<div class="edit_bottom">
					'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_update_input_form','step_index')", "backToIndex", "", 0, "formbutton").' '
					.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_update_input_form','step_confirm')", "goToConfirm", "", 0, "formbutton")
					.'
					<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
					<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
					$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index_edit.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				$x.='</div>
				<p class="spacer"></p>';
				//$x .='</form></div>';
		}
		else{
			$applicationSetting = $lac->getApplicationSetting($lac->schoolYearID);
			$applicationOthersInfo = current($lac->getApplicationOthersInfo($lac->schoolYearID,'',$fileData['InputApplicationID']));
			$lastContent = $applicationSetting[$applicationOthersInfo['classLevelID']]['LastPageContent'];
			$x .= $this->getAfterUpdateFinishPageContent($fileData['InputApplicationID'], $lastContent);
		}
		return $x;
	}
	
	function getUpdateConfirmPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;
		
		//remove the slashes of the special character
		$formData = $this->stripslashesRecursive($formData);
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getUpdateWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1,1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back']." Back", "button", "goto('step_confirm','step_update_input_form')", "SubmitBtn", "id='backToForm'", 0, "formbutton").' '
				.$this->GET_ACTION_BTN("提交 Submit", "submit", "", "SubmitBtn", "id='goToSubmit'", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
				$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index_edit.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
			$x.='</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getUpdateFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		// finish page	
		$x .= $this->getUpdateWizardStepsUI(7);
		if($ApplicationID){
			$x .=' <div class="admission_complete_msg"><h1>報名表資料已更改，申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/>';
			$x .='Your application form has been updated. Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></h1>';

//			$x .='<br/><h1>現在請進行報名費付款 ，以完成報名程序。<br/>';            
//			$x .='To finish the online application, please pay now.<br/><br><input type="button" value="繳付 HK$40 報名費 Pay application fee HK$40" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'Payment Status\');"></input></h1>';            
//			
			$x .='<br/><h1><span>另外，閣下將會收到確認電郵，請檢查閣下在申請表填寫的電郵為 '.$lac->getApplicantEmail($ApplicationID).'。<br/>如未收到該電郵，請致電與本校聯絡。<br/>';            
			$x .='An acknowledgement will be sent to you via email service. Please ensure that the email address entered on the form is '.$lac->getApplicantEmail($ApplicationID).'. <br/>Please contact our school in case you fail to receive it.</span></h1>';            
			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
//			
//			//add english version here...
//			$x .='<h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			
		
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>未能成功更改資料。<span>請重新嘗試申請或致電與本校聯絡。</span></h1>';
            $x .='<h1>Information cannot be updated.<span>Please try to apply again or contact our school.</span></h1>';
            
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent || !$ApplicationID){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		
		
		return $x;
	}
	
	function getAfterUpdateFinishPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';

		$x .=' <div class="admission_complete_msg"><h1>申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/>';
		$x .='Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></h1></div>';
		
		$x .= '<div class="admission_board">';
		$x .= $this->getDocsUploadForm(1, 1);
		
		$applicationStatus = current($lac->getApplicationStatus($lac->schoolYearID,'',$ApplicationID));
		
		if($applicationStatus['interviewdate'] && $applicationStatus['interviewdate'] != '0000-00-00 00:00:00'){
			$interviewDateTime = explode(" ", $applicationStatus['interviewdate']);
			$x .='<h1 style="font-size: 15px">面試資料 Interview Information</h1>';
			$x .= '<table class="form_table" style="font-size: 15px">';
			$x .= '<tr>';
			$x .= '<td class="field_title">面試日期 Interview Date</td>';
			$x .= '<td>'.$interviewDateTime[0].'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試時間 Interview Time</td>';
			$x .= '<td>'.substr($interviewDateTime[1], 0, -3).'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試地點 Interview Location</td>';
			$x .= '<td>'.$applicationStatus['interviewlocation'].'</td>';
			$x .= '</tr>';
			$x .= '</table>';
		}
		$x .= '</div>';
		
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent;
		$x .= '</div>';
		
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		
		
		return $x;
	}
	
	function getUpdateFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		if($ApplicationID){
			$x .='報名表資料已更改，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';         
			$x .='Your application form has been updated and the application number is <u>'.$ApplicationID.'</u>.<br/>';
			
//			$x .='<br/><br/>注意：所有申請必須支付報名費才有效；如未繳付或想檢查繳付情況，可按以下連結：<br/>';
//			$x .='Attention: Application is valid only if the application fee is paid. To pay or check payment status, click this hyperlink:';            
//			$x .='<br/>http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID);            
			
			$x .='<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
			$x .='Click this hyperlink to view your application form.';
			$x .="<br/><a target='_blank' href='http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."'>http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."</a>";
			
			$x .='<br/><br/><br/>如需要更改已遞交的申請資料，可於報名截止前按以下連結。<br/>';
			$x .='Click this hyperlink to amend your application form before the application deadline.';
			$x .="<br/><a target='_blank' href='http://{$_SERVER['HTTP_HOST']}/kis/admission_form/index_edit.php'>http://{$_SERVER['HTTP_HOST']}/kis/admission_form/index_edit.php</a>";
			
			$x .='<br/><br/><br/>謝謝您使用網上報名服務！<br/>';            
			$x .='Thanks for lodging your application online!';
			
//			if(!$LastContent){
//				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//			}
			$x .= '<br/><br/>'.$LastContent;
			
		}
		else{
			$x .='未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
			$x .='<br/><br/>';
			$x .='Your application is rejected. Please try to apply again or contact our school.';
        }
//		if($ApplicationID){
//			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);         
//			$x .='<br/><br/>';
//			$x .='Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>Application form preview<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);
//			if(!$LastContent){
//				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//			}
//			$x .= '<br/>'.$LastContent;
//		}
//		else{
//			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
//			$x .='<br/><br/>';
//			$x .='Admission is Not Completed. Please try to apply again!';
//        }
		
		return $x;
	}
}
?>