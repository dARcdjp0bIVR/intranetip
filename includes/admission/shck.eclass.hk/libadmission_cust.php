<?php
# modifying by: 

/********************
 * 
 * Log :
 * Date		2017-08-15 [Pun]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_cust_base.php");
 
class admission_cust extends admission_cust_base{
	function admission_cust(){
		global $kis_lang, $UserID; //switch $lang for IP/EJ/KIS
		$this->libdb();
		$this->filepath = '/file/admission/';
		$this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
		$this->schoolYearID = $this->getNextSchoolYearID();
		$this->uid = $UserID;
		$this->classLevelAry = $this->getClassLevel();
	}
	
	function testing(){
		return "test";
	}
	function encrypt_attachment($file){
		list($filename,$ext) = explode('.',$file);
	    $timestamp = date("YmdHis");
	    return base64_encode($filename.'_'.$timestamp).'.'.$ext;
	}
	function getNextSchoolYearID(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		$SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField=array('AcademicYearID'),1);
		krsort($SchoolYearIDArr);
		
		$SchoolYearIDArr = array_values($SchoolYearIDArr);
		$currentSchoolYear = Get_Current_Academic_Year_ID();
		$key = array_search($currentSchoolYear, $SchoolYearIDArr);
		if($key>0){
    		return $SchoolYearIDArr[$key-1];
    	}else{
    		return false;
    	}
	}
	function insertApplicationAllInfo($libkis_admission, $Data, $ApplicationID, $isUpdate=0){
		global $admission_cfg;
		extract($Data);
		
		$Success = array();
		if($ApplicationID != ""){
			
			if(!$isUpdate)
				$Success[] = $this->insertApplicationStatus($libkis_admission, $ApplicationID);

			if($isUpdate){
			    $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '".$ApplicationID."'";
			    $success[] = $this->db_db_query($sql);
			}
			
			######## Student START ########
			$Success[] = $this->insertApplicationStudentInfo($Data, $ApplicationID, $isUpdate);
			$Success[] = $this->insertApplicationStudentInfoCust($Data, $ApplicationID, $isUpdate);
			######## Student END ########
			
			######## Parent START ########
			$Success[] = $this->insertApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G1ChineseName,
				'EnglishName' => $G1EnglishName,
				'Company' => $G1Religion,
				'JobTitle' => $G1Occupation,
				'OfficeAddress' => $G1Address,
				'OfficeTelNo' => $G1OfficeTel,
				'Mobile' => $G1MobileNo,
				'PG_TYPE' => 'F'
			), $isUpdate);
			$Success[] = $this->insertApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G2ChineseName,
				'EnglishName' => $G2EnglishName,
				'Company' => $G2Religion,
				'JobTitle' => $G2Occupation,
				'OfficeAddress' => $G2Address,
				'OfficeTelNo' => $G2OfficeTel,
				'Mobile' => $G2MobileNo,
				'PG_TYPE' => 'M'
			), $isUpdate);
			######## Parent END ########
			
			
			######## Sibling START ########
			$success[] = $this->insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate);
			######## Sibling END ########
			
			######## Update School Year Record START ########
			$Success[] = $this->insertApplicationOthersInfo($Data, $ApplicationID);
			######## Update School Year Record END ########
			
			if(in_array(false,$Success)){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	function insertApplicationStatus($libkis_admission, $ApplicationID){
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){

//   		}
//   		else{
   			//Henry: not yet finish
   			$sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
     					SchoolYearID,
     					ApplicationID,
					    Status,
					    DateInput)
					VALUES (
						'".$this->schoolYearID."',
						'".$ApplicationID."',
						'1',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	function updateApplicationStatus($data){
		global $UserID;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
				s.ReceiptID = '".$receiptID."',
     			s.ReceiptDate = '".$receiptdate."',
      			s.Handler = '".$handler."', 
      			s.InterviewDate = '".$interviewdate."',
				s.InterviewLocation = '".$interviewlocation."',
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."',
				o.InterviewSettingID =  '".$InterviewSettingID."',
				o.InterviewSettingID2 =  '".$InterviewSettingID2."',
				o.InterviewSettingID3 =  '".$InterviewSettingID3."',
				s.FeeBankName = '{$FeeBankName}',
				s.FeeChequeNo = '{$FeeChequeNo}'
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}  
	function updateApplicationStatusByIds($applicationIds,$status){
		global $UserID;
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.RecordID IN (".$applicationIds.")
    	";
    	return $this->db_db_query($sql);
	}	  	
   	function insertApplicationStudentInfo($Data, $ApplicationID, $isUpdate=0){
   	    global $admission_cfg;
   		extract($Data);
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			if($isUpdate){
   				$sql = "UPDATE 
					ADMISSION_STU_INFO 
				SET
					 ChineseName = '{$studentsname_b5}',
					 EnglishName = '{$studentsname_en}',
					 Gender = '{$StudentGender}',
					 DOB = '{$StudentDateOfBirth}',
					 BirthCertNo = '{$StudentBirthCertNo}',
					 Nationality = '{$Nationality}',
					 PlaceOfBirth = '{$StudentPlaceOfBirth}',
					 LangSpokenAtHome = '{$LangSpokenAtHome}',
					 Address = '{$Address}',
					 AddressDistrict = '{$District}',
					 HomeTelNo = '{$HomeTelNo}',
					 Email = '{$Email}',
					 LastSchool = '{$LastSchool}',
					 LastSchoolLevel = '{$LastSchoolLevel}',
					 ReligionOther = '{$ReligionOther}',
					 Church = '{$Church}',
					 IsTwinsApplied = '".$IsTwinsApplied."',
					 TwinsApplicationID = '".strtolower($TwinsBirthCertNo)."',
				     DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'";
   			}
   			else{
	   			$sql = "INSERT INTO ADMISSION_STU_INFO (
							ApplicationID,
			   				 ChineseName,
			  				 EnglishName,   
			   				 Gender,
			  				 DOB,
			  				 
        					 BirthCertNo,
        					 Nationality,
        					 PlaceOfBirth,
        					 LangSpokenAtHome,
        					 Address,
        					 AddressDistrict,
        					 HomeTelNo,
        					 Email,
        					 LastSchool,
        					 LastSchoolLevel,
        					 ReligionOther,
        					 Church,
							 IsTwinsApplied,
							 TwinsApplicationID,
	   			             DateInput
	   			        ) VALUES (
							'{$ApplicationID}',
							'{$studentsname_b5}',
							'{$studentsname_en}',
							'{$StudentGender}',
							'{$StudentDateOfBirth}',
							
                            '{$StudentBirthCertNo}',
                            '{$Nationality}',
                            '{$StudentPlaceOfBirth}',
                            '{$LangSpokenAtHome}',
                            '{$Address}',
                            '{$District}',
                            '{$HomeTelNo}',
                            '{$Email}',
                            '{$LastSchool}',
                            '{$LastSchoolLevel}',
                            '{$ReligionOther}',
                            '{$Church}',
							'".$IsTwinsApplied."',
							'".strtolower($TwinsBirthCertNo)."',
                            now()
						)
				";
   			}
			return $this->db_db_query($sql);
   		}
   		return false;
    }
    
    function insertApplicationParentInfo($Data, $isUpdate=0){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
		
		if($isUpdate){
			foreach($Data as $_fieldname => $_fieldvalue){
				if($_fieldname == "ApplicationID"){
					$ApplicationID = $_fieldvalue;
				}
				else if($_fieldname == "PG_TYPE"){
					$PG_TYPE = $_fieldvalue;
				}
				else{
					$updateStr .= $_fieldname." = '".$_fieldvalue."',";
				}
			}
			//Henry
			$sql = "UPDATE 
					ADMISSION_PG_INFO 
				SET
					".$updateStr."
				    DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'
				AND
					PG_TYPE = '".$PG_TYPE."'";
					
			if(($result = $this->db_db_query($sql)) && $this->db_affected_rows() == 0){
				$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
			}
			else{
				return $result;
			}
		}
		else{	
			$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
		}
		return $this->db_db_query($sql);
    }

     function updateApplicationParentInfo($Data,$type){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			if($_fieldname != "ApplicationID"){
				$fieldname .= "{$_fieldname} = '{$_fieldvalue}',";
			}
		}
		
		$sql = "UPDATE ADMISSION_PG_INFO SET
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					ApplicationID = '".$Data['ApplicationID']."'
				AND
					PG_TYPE = '{$type}';
				";
		return $this->db_db_query($sql);
    }
    
       
    function insertApplicationOthersInfo($Data, $ApplicationID, $isUpdate=0){ 
    	global $admission_cfg, $sys_custom;
   		extract($Data);
   		$success = array();
   		#Check exist application
   		if($ApplicationID == ""){
   			return false;
   		}
    	
    	if(!$this->isInternalUse($_REQUEST['token']) && $token=='' && !$sys_custom['KIS_Admission']['IntegratedCentralServer']){
    		$token = 1;
    	}
   		
		$sql = "UPDATE 
				ADMISSION_OTHERS_INFO 
			SET
				 ApplyYear = '".$this->schoolYearID."',
				 ApplyLevel = '".$sus_status."',
				 Token = '".$token."',
			     DateInput = NOW(),
				 HTTP_USER_AGENT = '".addslashes($_SERVER['HTTP_USER_AGENT'])."'
			WHERE
				ApplicationID = '".$ApplicationID."'";
				
		if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
		
   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
						ApplicationID,
					     ApplyYear,
					     ApplyLevel,
						 Token,
					     DateInput,
   						 HTTP_USER_AGENT
					     )
					VALUES (
						'".$ApplicationID."',
						'".$this->schoolYearID."',
						'".$sus_status."',
						'".$token."',
						now(),
						'".addslashes($_SERVER['HTTP_USER_AGENT'])."')
			";
			$success[] = $this->db_db_query($sql);
		}			
			
		return !in_array(false, $success);
    }
    
    function insertApplicationStudentInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			
   			######## Update Old School Record START ########
   			$success = array();
   			if($isUpdate){
   				$sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '".$ApplicationID."' AND Code in (
   				    'BaptismCertNo', 
   				    'ElderBrother',
   				    'ElderSister',
   				    'YoungerBrother',
   				    'YoungerSister',
   				    'LangForInterview',
   				    'SessionChoice'
			    )";
   				$success[] = $this->db_db_query($sql);
   			}
   			######## Update Old School Record END ########

   			######## Student Cust START ########
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'BaptismCertNo',
   				'Value' => $BaptismCertNo
   			), $ApplicationID);
   			####  END ####
   			
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'ElderBrother',
   				'Value' => $ElderBrother
   			), $ApplicationID);
   			####  END ####
   			
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'ElderSister',
   				'Value' => $ElderSister
   			), $ApplicationID);
   			####  END ####
   			
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'YoungerBrother',
   				'Value' => $YoungerBrother
   			), $ApplicationID);
   			####  END ####
   			
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'YoungerSister',
   				'Value' => $YoungerSister
   			), $ApplicationID);
   			####  END ####
   			
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'LangForInterview',
   				'Value' => $LangForInterview
   			), $ApplicationID);
   			####  END ####
   			
   			####  START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'SessionChoice',
   				'Value' => $SessionChoice
   			), $ApplicationID);
   			####  END ####
   			
   			######## Student Cust END ########
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }

    function updateApplicationStudentInfoCust($Data){
    	global $UserID;
    	extract($Data);
    	
    	return $this->insertApplicationStudentInfoCust($Data, $applicationID,1);
    }
    
    function getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID,					
     			s.Class,
				s.Year,
				s.NameOfSchool 
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

    	return $this->returnArray($sql);
	}	
    
    function insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		
   		#Check exist application
   		if($ApplicationID != ""){

   		    ######## Update Old School Record START ########
   		    $success = array();
   		    if($isUpdate){
   		        $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '".$ApplicationID."' AND Code in (
   				    'SibilingsStudyingInThisSchool',
   				    'SibilingsStudyingInThisSchool_Name',
   				    'SibilingsStudyingInThisSchool_Relationship',
   				    'SibilingsStudyingInThisSchool_Class',
   				    'FormerStudentInThisSchool',
   				    'FormerStudentInThisSchool_Name',
   				    'FormerStudentInThisSchool_Relationship',
   				    'FormerStudentInThisSchool_YearOfGraduation',
   				    'SibilingsStudyingInOtherCanossianSchool',
   				    'SibilingsStudyingInOtherCanossianSchool_Name',
   				    'SibilingsStudyingInOtherCanossianSchool_Relationship',
   				    'SibilingsStudyingInOtherCanossianSchool_Class',
   				    'SibilingsStudyingInOtherCanossianSchool_NameOfSchool',
   				    'FormerStudentInOtherCanossianSchool',
   				    'FormerStudentInOtherCanossianSchool_Name',
   				    'FormerStudentInOtherCanossianSchool_Relationship',
   				    'FormerStudentInOtherCanossianSchool_YearOfGraduation',
   				    'FormerStudentInOtherCanossianSchool_NameOfSchool',
   				    'EmployeeOfCanossianInstitutions',
   				    'EmployeeOfCanossianInstitutions_Name',
   				    'EmployeeOfCanossianInstitutions_Relationship',
   				    'EmployeeOfCanossianInstitutions_NameOfOurInstitution'
			    )";
   		        $success[] = $this->db_db_query($sql);
   		    }
   		    ######## Update Old School Record END ########
   		    
   		    ######## Other Info Cust START ########
   		    ####  START ####
   		    $success[] = $this->insertApplicationCustInfo(array(
       		    'Code' => 'SibilingsStudyingInThisSchool',
       		    'Value' => $SibilingsStudyingInThisSchool
   		    ), $ApplicationID);
   		    
   		    for($i=0;$i<3;$i++){
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInThisSchool_Name',
           		    'Value' => $SibilingsStudyingInThisSchool_Name[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInThisSchool_Relationship',
           		    'Value' => $SibilingsStudyingInThisSchool_Relationship[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInThisSchool_Class',
           		    'Value' => $SibilingsStudyingInThisSchool_Class[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
   		    }
   		    ####  END ####
   		    
   		    ####  START ####
   		    $success[] = $this->insertApplicationCustInfo(array(
       		    'Code' => 'FormerStudentInThisSchool',
       		    'Value' => $FormerStudentInThisSchool
   		    ), $ApplicationID);
   		    
   		    for($i=0;$i<3;$i++){
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInThisSchool_Name',
           		    'Value' => $FormerStudentInThisSchool_Name[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInThisSchool_Relationship',
           		    'Value' => $FormerStudentInThisSchool_Relationship[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInThisSchool_YearOfGraduation',
           		    'Value' => $FormerStudentInThisSchool_YearOfGraduation[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
   		    }
   		    ####  END ####
   		    
   		    ####  START ####
   		    $success[] = $this->insertApplicationCustInfo(array(
       		    'Code' => 'SibilingsStudyingInOtherCanossianSchool',
       		    'Value' => $SibilingsStudyingInOtherCanossianSchool
   		    ), $ApplicationID);
   		    
   		    for($i=0;$i<3;$i++){
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInOtherCanossianSchool_Name',
           		    'Value' => $SibilingsStudyingInOtherCanossianSchool_Name[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInOtherCanossianSchool_Relationship',
           		    'Value' => $SibilingsStudyingInOtherCanossianSchool_Relationship[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInOtherCanossianSchool_Class',
           		    'Value' => $SibilingsStudyingInOtherCanossianSchool_Class[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'SibilingsStudyingInOtherCanossianSchool_NameOfSchool',
           		    'Value' => $SibilingsStudyingInOtherCanossianSchool_NameOfSchool[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
   		    }
   		    ####  END ####
   		    
   		    ####  START ####
   		    $success[] = $this->insertApplicationCustInfo(array(
       		    'Code' => 'FormerStudentInOtherCanossianSchool',
       		    'Value' => $FormerStudentInOtherCanossianSchool
   		    ), $ApplicationID);
   		    
   		    for($i=0;$i<3;$i++){
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInOtherCanossianSchool_Name',
           		    'Value' => $FormerStudentInOtherCanossianSchool_Name[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInOtherCanossianSchool_Relationship',
           		    'Value' => $FormerStudentInOtherCanossianSchool_Relationship[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInOtherCanossianSchool_YearOfGraduation',
           		    'Value' => $FormerStudentInOtherCanossianSchool_YearOfGraduation[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'FormerStudentInOtherCanossianSchool_NameOfSchool',
           		    'Value' => $FormerStudentInOtherCanossianSchool_NameOfSchool[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
   		    }
   		    ####  END ####
   		    
   		    ####  START ####
   		    $success[] = $this->insertApplicationCustInfo(array(
       		    'Code' => 'EmployeeOfCanossianInstitutions',
       		    'Value' => $EmployeeOfCanossianInstitutions
   		    ), $ApplicationID);
   		    
   		    for($i=0;$i<3;$i++){
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'EmployeeOfCanossianInstitutions_Name',
           		    'Value' => $EmployeeOfCanossianInstitutions_Name[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'EmployeeOfCanossianInstitutions_Relationship',
           		    'Value' => $EmployeeOfCanossianInstitutions_Relationship[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
       		    $success[] = $this->insertApplicationCustInfo(array(
           		    'Code' => 'EmployeeOfCanossianInstitutions_NameOfOurInstitution',
           		    'Value' => $EmployeeOfCanossianInstitutions_NameOfOurInstitution[$i],
           		    'Position' => $i
       		    ), $ApplicationID);
   		    }
   		    ####  END ####
   		    ######## Other Info Cust END ########
   		    
   			return !in_array(false, $success);
   		}
   		return false;
    }

    function updateApplicationRelativesInfoCust($Data){
    	global $UserID;
    	extract($Data);
    
    	$sql = "UPDATE ADMISSION_RELATIVES_AT_SCH_INFO
				SET
					ApplicationID = '{$ApplicationID}',
					Name = '{$RelativeStudent}',
					ClassPosition = '{$RelativeClassAttend}',
					Relationship = '{$RelativeRelationship}',
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
    	$success[] = $this->db_db_query($sql);
    
    	if($RelativeStudent == '' && $RelativeClassAttend =='' && $RelativeRelationship ==''){
    		$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE RecordID = '".$Data['RecordID']."'";
    		$success[] = $this->db_db_query($sql);
    	}
    	return !in_array(false, $success);
    }
    
    function getApplicationRelativesInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year year,
				r.Name name,		    					
     			r.ClassPosition classposition,
				r.EnglishName englishName,
				r.IsTwins istwins,
				r.DOB dob,
				r.SchoolName schoolName,
				r.Type type
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

    	return $this->returnArray($sql);
	}	
    
	function updateApplicationOtherInfo($data){    	
		global $UserID,$admission_cfg,$sys_custom;
   		extract($data);
   		
		####  START ####
		$Success[] = $this->updateApplicationCustInfo(array(
		    'filterApplicationId' => $ApplicationID,
			'filterCode' => 'SibilingsStudyingInThisSchool',
			'Value' => $SibilingsStudyingInThisSchool
		));
		if($SibilingsStudyingInThisSchool =="1"){
		    for($i=0;$i<3;$i++){
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInThisSchool_Name',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInThisSchool_Name[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInThisSchool_Relationship',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInThisSchool_Relationship[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInThisSchool_Class',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInThisSchool_Class[$i]
    			));
		    }
		}
		####  END ####
		
		####  START ####
		$Success[] = $this->updateApplicationCustInfo(array(
		    'filterApplicationId' => $ApplicationID,
			'filterCode' => 'FormerStudentInThisSchool',
			'Value' => $FormerStudentInThisSchool
		));
		if($FormerStudentInThisSchool =="1"){
		    for($i=0;$i<3;$i++){
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInThisSchool_Name',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInThisSchool_Name[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInThisSchool_Relationship',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInThisSchool_Relationship[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInThisSchool_YearOfGraduation',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInThisSchool_YearOfGraduation[$i]
    			));
		    }
		}
		####  END ####
		
		####  START ####
		$Success[] = $this->updateApplicationCustInfo(array(
		    'filterApplicationId' => $ApplicationID,
			'filterCode' => 'SibilingsStudyingInOtherCanossianSchool',
			'Value' => $SibilingsStudyingInOtherCanossianSchool
		));
		if($SibilingsStudyingInOtherCanossianSchool =="1"){
		    for($i=0;$i<3;$i++){
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInOtherCanossianSchool_Name',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInOtherCanossianSchool_Name[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInOtherCanossianSchool_Relationship',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInOtherCanossianSchool_Relationship[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInOtherCanossianSchool_Class',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInOtherCanossianSchool_Class[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'SibilingsStudyingInOtherCanossianSchool_NameOfSchool',
    			    'filterPosition' => $i,
    				'Value' => $SibilingsStudyingInOtherCanossianSchool_NameOfSchool[$i]
    			));
		    }
		}
		####  END ####
		
		####  START ####
		$Success[] = $this->updateApplicationCustInfo(array(
		    'filterApplicationId' => $ApplicationID,
			'filterCode' => 'FormerStudentInOtherCanossianSchool',
			'Value' => $FormerStudentInOtherCanossianSchool
		));
		if($FormerStudentInOtherCanossianSchool =="1"){
		    for($i=0;$i<3;$i++){
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInOtherCanossianSchool_Name',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInOtherCanossianSchool_Name[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInOtherCanossianSchool_Relationship',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInOtherCanossianSchool_Relationship[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInOtherCanossianSchool_YearOfGraduation',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInOtherCanossianSchool_YearOfGraduation[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'FormerStudentInOtherCanossianSchool_NameOfSchool',
    			    'filterPosition' => $i,
    				'Value' => $FormerStudentInOtherCanossianSchool_NameOfSchool[$i]
    			));
		    }
		}
		####  END ####
		
		####  START ####
		$Success[] = $this->updateApplicationCustInfo(array(
		    'filterApplicationId' => $ApplicationID,
			'filterCode' => 'EmployeeOfCanossianInstitutions',
			'Value' => $EmployeeOfCanossianInstitutions
		));
		if($EmployeeOfCanossianInstitutions =="1"){
		    for($i=0;$i<3;$i++){
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'EmployeeOfCanossianInstitutions_Name',
    			    'filterPosition' => $i,
    				'Value' => $EmployeeOfCanossianInstitutions_Name[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'EmployeeOfCanossianInstitutions_Relationship',
    			    'filterPosition' => $i,
    				'Value' => $EmployeeOfCanossianInstitutions_Relationship[$i]
    			));
    			$Success[] = $this->updateApplicationCustInfo(array(
    			    'filterApplicationId' => $ApplicationID,
    				'filterCode' => 'EmployeeOfCanossianInstitutions_NameOfOurInstitution',
    			    'filterPosition' => $i,
    				'Value' => $EmployeeOfCanossianInstitutions_NameOfOurInstitution[$i]
    			));
		    }
		}
		####  END ####
		
		
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }    
    function removeApplicationAttachment($data){
    	global $file_path;
    	extract($data);
    	if(empty($recordID)) return;
 		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";
    	$sql = "SELECT 
    				r.RecordID attachment_id,
    				r.AttachmentName attachment_name,
    				r.AttachmentType attachment_type
    			FROM 
    				ADMISSION_ATTACHMENT_RECORD r
    			INNER JOIN 
    				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
    			WHERE 
    				o.RecordID = '".$recordID."' 
    				".$cond."
    			";
    	$applicantAry = $this->returnArray($sql);
    
    	$Success = array();
    	for($i=0;$i<count($applicantAry);$i++){
    		$_attachmentName = $applicantAry[$i]['attachment_name'];
    		$_attachmentType = $applicantAry[$i]['attachment_type']=='personal_photo'? $applicantAry[$i]['attachment_type']:'other_files';
    		$_attachmentId = $applicantAry[$i]['attachment_id'];
    		$image_url = $this->filepath.$recordID.'/'.$_attachmentType.'/'.$_attachmentName;	
    		
    		if(file_exists($file_path.$image_url)){
    			unlink($file_path.$image_url);
    			$sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '".$_attachmentId."'";
    			
    			$Success[] = $this->db_db_query($sql);
    		}
    	}
    	if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }
    function saveApplicationAttachment($data){
    	global $UserID;
    	extract($data);
 		if(empty($recordID)) return;
    	$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '".$recordID."'";
    	$applicationID = current($this->returnVector($sql));
    	
    	if(!empty($applicationID)&&!empty($attachment_name)&&!empty($attachment_type)){
    		$result = $this->removeApplicationAttachment($data);
    		if($result){
	    		$sql = "INSERT INTO 
	    					ADMISSION_ATTACHMENT_RECORD 
	    						(ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy)
	    					VALUES
	    						('".$applicationID."','".$attachment_type."','".$attachment_name."',NOW(),'".$UserID."')
	    				";
	    		return $this->db_db_query($sql);
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
 	function updateApplicationStudentInfo($data, $isAdminUpdate = false){
 		global $kis_lang, $Lang;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
 		return $this->insertApplicationStudentInfo($data, $applicationID, 1) &&
 		    $this->updateApplicationStudentInfoCust($data);
	}    
 	
	function getApplicationAttachmentRecord($schoolYearID,$data=array()){
		extract($data);
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";		
		$sql = "
			SELECT
				 o.RecordID folder_id,
     			 r.RecordID,
			     r.ApplicationID applicationID,
			     r.AttachmentType attachment_type,
			     r.AttachmentName attachment_name,
			     r.DateInput dateinput,
			     r.InputBy inputby
			FROM
				ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID						
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			ORDER BY r.AttachmentType, r.RecordID desc
    	";
    	$result = $this->returnArray($sql);
    	$attachmentAry = array();
    	for($i=0;$i<count($result);$i++){
    		$_attachType = $result[$i]['attachment_type'];
     		$_attachName = $result[$i]['attachment_name'];   
     		$_applicationId = $result[$i]['applicationID'];  
     		$_folderId = $result[$i]['folder_id'];
     		$attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;		
     		$attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId.'/'.($_attachType=='personal_photo'?$_attachType:'other_files').'/'.$_attachName;	
     		
    	}
		return $attachmentAry;
	}
	function saveApplicationParentInfo($data){
		extract($data);
		$Success[] = $this->updateApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G1ChineseName,
				'EnglishName' => $G1EnglishName,
				'Company' => $G1Religion,
				'JobTitle' => $G1Occupation,
				'OfficeAddress' => $G1Address,
				'OfficeTelNo' => $G1OfficeTel,
				'Mobile' => $G1MobileNo,
				'PG_TYPE' => 'F'
		),'F');
		$Success[] = $this->updateApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G2ChineseName,
				'EnglishName' => $G2EnglishName,
				'Company' => $G2Religion,
				'JobTitle' => $G2Occupation,
				'OfficeAddress' => $G2Address,
				'OfficeTelNo' => $G2OfficeTel,
				'Mobile' => $G2MobileNo,
				'PG_TYPE' => 'M'
		),'M');
		######## Parent Info END ########
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationStudentInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
 		for($i=0; $i<count($OthersPrevSchYear); $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_cust_id[$i]=='new'){
				$parentInfoAry['OthersPrevSchYear'.($i+1)] = $OthersPrevSchYear[$i];
				$parentInfoAry['OthersPrevSchClass'.($i+1)] = $OthersPrevSchClass[$i];
				$parentInfoAry['OthersPrevSchName'.($i+1)] = $OthersPrevSchName[$i];
				$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry);
			}else{
				$parentInfoAry['Year'] = $OthersPrevSchYear[$i];
				$parentInfoAry['Class'] = $OthersPrevSchClass[$i];
				$parentInfoAry['NameOfSchool'] = $OthersPrevSchName[$i];
				$parentInfoAry['RecordID'] = $student_cust_id[$i];	
				$Success[] = $this->updateApplicationStudentInfoCust($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationRelativesInfoCust($data){
		extract($data);
		
		######## Relative Info START ########
 		$relativesInfoAry = array();
		$relativesInfoAry['ApplicationID'] = $ApplicationID;
		$relativesInfoAry['RelativeStudent'] = $RelativeStudent;
		$relativesInfoAry['RelativeClassAttend'] = $RelativeClassAttend;
		$relativesInfoAry['RelativeRelationship'] = $RelativeRelationship;
		$relativesInfoAry['RecordID'] = $RelativesID;	
		if($RelativesID == ''){
			$Success[] = $this->insertApplicationRelativesInfoCust($relativesInfoAry, $ApplicationID);
		}else{
			$Success[] = $this->updateApplicationRelativesInfoCust($relativesInfoAry);
		}
		######## Relative Info END ########

		######## Cust Info START ########
		$success[] = $this->updateApplicationCustInfo(array(
			'filterApplicationId' => $ApplicationID,
			'filterCode' => 'Referee_Name',
			'Value' => $Referee_Name
		));
		######## Cust Info END ########
		
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	   
	function getApplicationOthersInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND RecordID='".$recordID."'":"";

		$sql = "
			SELECT 
		        *,
				ApplicationID applicationID,
				ApplyYear schoolYearId,
				ApplyLevel classLevelID,
				IsConsiderAlternative OthersIsConsiderAlternative
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	return $this->returnArray($sql);
	}		 
	function getApplicationParentInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND pg.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				pg.*,
     			pg.ApplicationID applicationID,
     			pg.EnglishName parent_name_en,
     			pg.ChineseName parent_name_b5,
     			pg.PG_TYPE type,
				pg.Company companyname,
				pg.OfficeAddress companyaddress,
     			pg.JobTitle occupation,
     			pg.Mobile mobile,
				pg.Email email,
				pg.LevelOfEducation levelofeducation,
				pg.LastSchool lastschool,
				pg.Relationship relationship,
     			o.ApplyLevel classLevelID
			FROM
				ADMISSION_PG_INFO pg
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";

    	return $this->returnArray($sql);
	}	
	function getApplicationStatus($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			s.ApplicationID applicationID,
     			s.ReceiptID receiptID, 
			    IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
			    IF(s.Handler,".getNameFieldByLang2("iu.").",'') AS handler,
			    s.Handler handler_id,
				s.InterviewDate As interviewdate,
				s.InterviewLocation As interviewlocation,
			    s.Remark remark,CASE ";
		foreach($admission_cfg['Status'] as $_key => $_value){
				$sql .= "WHEN s.Status = ".$_value." THEN '".strtolower($_key)."' ";
		}	    
		$sql .= " ELSE s.Status END status,";
		$sql .= "
			    CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
			    s.DateInput dateinput,
			    s.InputBy inputby,
			    s.DateModified datemodified,
			    s.ModifiedBy modifiedby,
			    o.ApplyLevel classLevelID,
				s.FeeBankName,
				s.FeeChequeNo,
				IF(pi.ApplicationID IS NOT NULL , 'OnlinePayment' , IF(s.Status=2, 'OtherPayment' , 'N/A')  ) as OnlinePayment
			FROM
				ADMISSION_APPLICATION_STATUS s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			LEFT JOIN
				INTRANET_USER iu ON s.Handler = iu.UserID
			LEFT JOIN
				ADMISSION_PAYMENT_INFO pi ON o.ApplicationID = pi.ApplicationID and pi.payment_status = 'Completed'
			WHERE
				s.SchoolYearID = '".$schoolYearID."'	
			".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

// 		if($type == "personal_photo"){
// 			return true;
// 		}
			if (!empty($file['tmp_name'])) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							if($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
								$image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
							else
								$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}else{
				if($this->isInternalUse($_REQUEST['token'])){
					return true;
				}
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	function moveUploadedAttachment($tempFolderPath, $destFolderPath){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/personal_photo", $destFolderPath);
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/other_files", $destFolderPath);
		$uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);
		
		if(in_array(false, $uploadSuccess))
			return false;
		else
			return true;
	}
	//Siuwan 20131018 Copy from libstudentregistry.php, e.g.$this->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	public function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
		$result = $this->returnArray($sql);
		return getSelectByArray($result, "name=".$selection_name, $code_selected,0,1);
	}
	//Henry 20131018 Copy from libstudentregistry.php
	public function returnPresetCodeName($code_type="", $selected_code="")
	{
		$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
		$result = $this->returnVector($sql);
		return $result[0];
	}
	function displayWarningMsg($warning){
		global $kis_lang;
		$x = '
		 <fieldset class="warning_box">
			<legend>'.$kis_lang['warning'].'</legend>
			<ul>
				<li>'.$kis_lang['msg'][$warning].'</li>
			</ul>
		</fieldset>
		
		';
		return $x;
	}
		
	function insertAttachmentRecord($AttachmentType,$AttachmentName, $ApplicationID){
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
		   				 ApplicationID,
		  				 AttachmentType,   
		   				 AttachmentName,
		  				 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$AttachmentType."',
						'".$AttachmentName."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	//old method
	function newApplicationNumber($schoolYearID=""){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$defaultNo = "PL".$year."-a0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%' order by ApplicationID desc";
		$result = $this->returnArray($sql);
		
		if($result){			
			$num = substr($result[0]['ApplicationID'], -4);
			$num++;
			$num = sprintf("%04s", $num);
		}
		
		$newNo = $prefix.$num;
		
		return $newNo;
	}
	
	function newApplicationNumber2($schoolYearID="",$classLevelID=""){
		global $Lang;
		$yearStart = date('y',getStartOfAcademicYear('',$this->schoolYearID));
		
		$defaultNo = $yearStart."0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
		
		if($this->returnArray($sql)){
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( SUBSTRING( ApplicationID, '".(strlen($prefix)+1)."' ) , UNSIGNED ) ) +1, 4, '0')) 
					FROM ADMISSION_OTHERS_INFO 
					WHERE ApplicationID like '".$prefix."%'";
		}
		else{
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					Values ('".$defaultNo."')";
		}
			$result = $this->db_db_query($sql);
					
			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
			$newNo = $this->returnArray($sql);
			return $newNo[0]['ApplicationID'];
	}
	function getAttachmentByApplicationID($schoolYearID,$applicationID){
		global $file_path;
		$attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID,array("applicationID"=>$applicationID));
		$attachment = array();
		foreach((array)$attachmentAry[$applicationID] as $_type => $_attachmentAry){
			$_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
			if(!empty($_thisAttachment)){
				$_thisAttachment = $this->filepath.$_thisAttachment; 
				if(file_exists($file_path.$_thisAttachment)){
					$attachment[$_type]['link'] = $_thisAttachment;
				}else{
					$attachment[$_type]['link'] = false;
				}
			}else{
				$attachment[$_type]['link'] = false;
			}
			
		}
		return $attachment;
	}

	public function getApplicationNumber($RecordIDAry){
		$sql = 'SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID IN ("'.implode('","',$RecordIDAry).'")';
		$result = $this->returnVector($sql);
		return $result;
	}
	
	public function getPrintLink($schoolYearID="", $applicationID , $type=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?id='.$id;
	}
 	function getClassLevel($ClassLevel=''){
 		global $intranet_root;
    	include_once($intranet_root."/includes/form_class_manage.php");
    	$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
    	$numOfForm = count($FormArr);
    	$classLevelName = array();
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisClassLevelID = $FormArr[$i]['YearID'];
			$thisLevelName = $FormArr[$i]['YearName'];
			$classLevelName[$thisClassLevelID] = $thisLevelName;
		}
		return $classLevelName;
    }	
	function getBasicSettings($schoolYearID='',$SettingNameAry=array()){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
    	$sql = "
			SELECT
			    SettingName,
			    SettingValue
			FROM
				ADMISSION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'	
    	";
    	if (sizeof($SettingNameAry) > 0)  {
			$sql .= " AND 
						SettingName in ('".implode("','",$SettingNameAry)."')";
		}
    	$setting = $this->returnArray($sql);
		for ($i=0; $i< sizeof($setting); $i++) {
			$Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
		}
		return $Return;    	
    }
   	function saveBasicSettings($schoolYearID='',$SettingNameValueAry=array()){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$this->Start_Trans();
		
		$SettingsNameAry = array_keys($SettingNameValueAry);
		
		$result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID,$SettingsNameAry);
		$result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID,$SettingNameValueAry);
		
		if(in_array(false, $result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	 
	function removeBasicSettings($schoolYearID,$SettingsNameAry){
		if(count($SettingsNameAry)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameAry)."'";
		
		$sql = "
			DELETE FROM
				ADMISSION_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
			AND SchoolYearID = '".$schoolYearID."'
		";
		
		return $this->db_db_query($sql);
	}
	
	function insertBasicSettings($schoolYearID,$SettingNameValueAry){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		
		foreach((array)$SettingNameValueAry as $_settingName => $_settingValue)
		{
			$InsertSqlArr[] = "('".$schoolYearID."','".$_settingName."','".$_settingValue."', '".$this->uid."', NOW(), '".$this->uid."', NOW())";
		}
		
		if(count($InsertSqlArr)>0)
		{	
			$InsertSql = implode(',',$InsertSqlArr);	
			
			$sql = "
				INSERT INTO	ADMISSION_SETTING
					(SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)	 
				VALUES
					$InsertSql
			";
			
			return $this->db_db_query($sql);
			
		}
		else
			return false;
		
			
	}
	
	
    function getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$recordID=''){
		$cond = !empty($applicationID)?" AND stu.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($status)?" AND s.status='".$status."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			stu.ApplicationID applicationID,
     			o.ApplyLevel classLevelID,
     			o.ApplyYear schoolYearID,
		        stu.*,
				stu.TwinsApplicationID SiblingAppliedID,
     			stu.ChineseName student_name_b5,
      			stu.EnglishName student_name_en, 
      			stu.Gender gender,
				stu.BirthCertType birthcerttype,
      			IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,	
      			stu.BirthCertNo birthcertno,
      			stu.Email email,
     			".getNameFieldByLang2("stu.")." AS student_name
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID							
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	$studentInfoAry = $this->returnArray($sql);
		return $studentInfoAry;
	}
	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
	
		if($paymentStatus==$admission_cfg['PaymentStatus']['OnlinePayment']){
			$paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NOT NULL  ";
		}
		else if($paymentStatus==$admission_cfg['PaymentStatus']['OtherPayment']){
			$paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NULL  ";
		}
		
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'
					OR pg.Mobile LIKE '%".$keyword."%'
					OR stu.BirthCertNo LIKE '%".$keyword."%'
					OR stu.HomeTelNo LIKE '%".$keyword."%'
					OR stu.LangSpokenAtHome LIKE '%".$keyword."%'				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			$paymentStatus_cond	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
    	
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				stu.LangSpokenAtHome AS langspokenathome, 
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}
	
	function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1){
		global $admission_cfg;
		
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		
		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sort = $sortby? "$sortby $order":"application_id";
		
//		if(!empty($keyword)){
//			$cond .= "
//				AND ( 
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'					
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'				
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')."
			INNER JOIN 
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				1 	
			".$cond." 
			order by ".$sort."
    	";
    	$sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			stu.ChineseName AS student_name_b5,
				stu.EnglishName AS student_name_en,
				".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status, stu.BirthCertNo, stu.TwinsApplicationID, stu.LangSpokenAtHome ".$from_table." ";
     	//debug_r($sql);
    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}
	   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function checkImportDataForImportAdmissionHeader($csv_header, $lang = ''){
		//$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
		$file_format = $this->getExportHeader();
		$file_format = $file_format[1];
		# check csv header
		$format_wrong = false;
		
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($csv_header[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		
		return $format_wrong;
	}
	
	public function returnPresetCodeAndNameArr($code_type="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function checkIsChinese($str){
	    return true;
	    //return preg_match('/[\x{4e00}-\x{9fa5}]+/u', $str);
	}
	function checkIsEnglish($str){
	    return true;
	    //return preg_match('/^[A-Za-z ]*$/', $str);
	}
	function checkIsEmpty($str){
	    return (
        	(strtolower(trim($str))=='沒有') || 
        	(strtolower(trim($str))=='nil') ||
        	(strtolower(trim($str))=='n.a.')
    	);
	}
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$allAdmissionNumber = array();

		$admission_year = getAcademicYearByAcademicYearID($this->getNextSchoolYearID());
		$admission_year_start = substr($admission_year, 0, 4);
		
		//// Get all Birth Cert No START ////
		$sql = "SELECT asi.ApplicationID, asi.BirthCertNo FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID JOIN ADMISSION_APPLICATION_STATUS as aas ON asi.ApplicationID = aas.ApplicationID WHERE aoi.ApplyYear = '".$this->getNextSchoolYearID()."' AND aas.Status = 2";
		$rs = $this->returnResultSet($sql);
		$allBirthCertNo = BuildMultiKeyAssoc($rs, array('ApplicationID') , array('BirthCertNo'), $SingleValue=1);
		//// Get all Birth Cert No END ////
		
		$i=0;
		foreach($data as $aData){
			
			//valid Admission Date
			if($aData[0]){
    			$aData[0] = getDefaultDateFormat($aData[0]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[0]) ) {
    				list($year , $month , $day) = explode('-',$aData[0]);
    		       	$resultArr[$i]['validAdmissionDate'] = checkdate($month , $day , $year);
    			}
    			else{
    				$resultArr[$i]['validAdmissionDate'] = false;
    			}
			}else{
				$resultArr[$i]['validAdmissionDate'] = true;
			}
			
			//valid Admission number
			$resultArr[$i]['validAdmissionNo'] = !in_array($aData[1], $allAdmissionNumber);
			
			//valid Chinese Name
			if($aData[3] == ''){
			    $resultArr[$i]['validChineseNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validChineseNameEmpty'] = true;
			    
			    if($this->checkIsChinese($aData[3]) || $this->checkIsEmpty($aData[3])){
			        $resultArr[$i]['validChineseName'] = true;
			    }else{
			        $resultArr[$i]['validChineseName'] = false;
			    }
			}
			
			//valid English Name
			if($aData[4] == ''){
			    $resultArr[$i]['validEnglishNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validEnglishNameEmpty'] = true;
			    
			    if($this->checkIsEnglish($aData[4]) || $this->checkIsEmpty($aData[4])){
			        $resultArr[$i]['validEnglishName'] = true;
			    }else{
			        $resultArr[$i]['validEnglishName'] = false;
			    }
			}
			
			//valid Date of birth
			if($aData[5] == ''){
			    $resultArr[$i]['validDateOfBirthEmpty'] = false;
			}else{
			    $resultArr[$i]['validDateOfBirthEmpty'] = true;
    			$aData[5] = getDefaultDateFormat($aData[5]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[5]) ) {
    				list($year , $month , $day) = explode('-',$aData[5]);
    		       	$resultArr[$i]['validDateOfBirth'] = checkdate($month , $day , $year);
    			}
    			else{
    				$resultArr[$i]['validDateOfBirth'] = false;
    			}
			}
			
			//valid Gender
			if($aData[6]){
    			if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
    				$aData[6] = 'M';
    				$data[$i][6] = 'M';
    			}
    			else if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
    				$aData[6] = 'F';
    				$data[$i][6] = 'F';
    			}
    			if ( strtoupper($aData[6]) == 'M' || strtoupper($aData[6]) == 'F') {
    		       	$resultArr[$i]['validGender'] = true;
    			}
    			else{
    				$resultArr[$i]['validGender'] = false;
    			}
			}else{
			    $resultArr[$i]['validGender'] = true;
			}
			
			//valid Place of birth
			if($aData[7] == ''){
				$resultArr[$i]['validPlaceOfBirthEmpty'] = false;
			}else{
			    $resultArr[$i]['validPlaceOfBirthEmpty'] = true;
			}
			
			//valid birth cert
    		$checkBirthCertType = array(
    		    strtoupper($kis_lang['Admission']['BirthCertType']['hk2']), 
    		    strtoupper($kis_lang['Admission']['HKUGAPS']['birthcertno'])
    		);
    		if(in_array( strtoupper($aData[8]), $checkBirthCertType )){
    			if (
    			    preg_match('/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/',$aData[9]) &&
    			    $this->checkBirthCertNo($aData[9])
			    ) {
    				$resultArr[$i]['validBirthCertNoType'] = true;
    			}else{
    				$resultArr[$i]['validBirthCertNoType'] = false;
    			}
    			
    			if (
    			    $allBirthCertNo[$aData[1]] != $aData[9] && 
    			    $this->hasBirthCertNumber($aData[9], $_REQUEST['classLevelID'], $this->schoolYearID)
			    ) {
    				$resultArr[$i]['validBirthCertNo'] = false;
    			}else{
    				$resultArr[$i]['validBirthCertNo'] = true;
    			}
    		}else{
				$resultArr[$i]['validBirthCertNoType'] = true;
				$resultArr[$i]['validBirthCertNo'] = true;
    		}
			
			
			//valid SpokenLanguageForInterview
			$resultArr[$i]['validSpokenLanguageForInterview'] = false;
			foreach($kis_lang['Admission']['Languages'] as $lang => $langStr){
				if($aData[10] == $langStr){
					$resultArr[$i]['validSpokenLanguageForInterview'] = true;
					break;
				}
			}
			
			//valid AddressStreet
			if($aData[16] == ''){
			    $resultArr[$i]['validAddressStreetEmpty'] = false;
			}else{
			    $resultArr[$i]['validAddressStreetEmpty'] = true;
			}
			
			//valid AddressDistrict
			if($aData[17] == ''){
			    $resultArr[$i]['validAddressDistrictEmpty'] = false;
			}else{
			    $resultArr[$i]['validAddressDistrictEmpty'] = true;
    			$resultArr[$i]['validAddressDistrict'] = false;
    			foreach($admission_cfg['AddressDistrict'] as $_key => $addrStr){
    				if($aData[17] == $addrStr){
    					$resultArr[$i]['validAddressDistrict'] = true;
    					break;
    				}
    			}
			}

			//valid AddressArea
			if($aData[18] == ''){
			    $resultArr[$i]['validAddressAreaEmpty'] = false;
			}else{
			    $resultArr[$i]['validAddressAreaEmpty'] = true;
			}
				
			//valid Tel Home
			if($aData[19] !== ''){
    			if (preg_match('/^[0-9]*$/',$aData[19])) {
    				$resultArr[$i]['validTelHome'] = true;
    			}
    			else{
    				$resultArr[$i]['validTelHome'] = false;
    			}
			}else{
			    $resultArr[$i]['validTelHome'] = true;
			}
			
			//valid contact email
			if($aData[20]){
    			if (preg_match('/\S+@\S+\.\S+/',$aData[20])) {
    				$resultArr[$i]['validEmail'] = true;
    			}
    			else{
    				$resultArr[$i]['validEmail'] = false;
    			}
			}else{
			    $resultArr[$i]['validEmail'] = true;
			}
			
			if($aData[21] !== ''){
    			//valid NoBrotherSister
    			if (preg_match('/\d*/',$aData[21])) {
    				$resultArr[$i]['validNoBrotherSister'] = true;
    			}
    			else{
    				$resultArr[$i]['validNoBrotherSister'] = false;
    			}
    			
    			//valid BrotherSisterRank
    			if (preg_match('/\d*/',$aData[22])) {
    				$resultArr[$i]['validBrotherSisterRank'] = true;
    			}
    			else{
    				$resultArr[$i]['validBrotherSisterRank'] = false;
    			}
			}else{
				$resultArr[$i]['validNoBrotherSister'] = true;
				$resultArr[$i]['validBrotherSisterRank'] = true;
			}
			
			//valid ContactPerson
		    $resultArr[$i]['validContactPerson'] = true;
			if(!empty($aData[23])){
    			$validContactPerson = array(
    			    $kis_lang['Admission']['HKUGAPS']['father'],
    			    $kis_lang['Admission']['HKUGAPS']['mother'],
    			    $kis_lang['Admission']['HKUGAPS']['guardian']
    			);
    			$contactPersion = explode(',', $aData[23]);
    			foreach((array)$contactPersion as $persion){
    			    $persion = trim($persion);
    			    if(!in_array($persion, $validContactPerson)){
    			        $resultArr[$i]['validContactPerson'] = false;
    			        break;
    			    }
    			}
			}
			
			if($aData[24]){
//			    //valid CurrentAttend1 Grade
//    			if (preg_match('/.+-.+/',$aData[25])) {
//    				$resultArr[$i]['validCurrentAttendGrade1'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendGrade1'] = false;
//    			}
//    			
//    			//valid CurrentAttend1 Year
//    			if (preg_match('/\d{4} ?- ?\d{4}/',$aData[26])) {
//    				$resultArr[$i]['validCurrentAttendYear1'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendYear1'] = false;
//    			}
			    //valid CurrentAttend1 Grade
    			if($aData[25] && $aData[26]){
    				$resultArr[$i]['validCurrentAttendGrade1'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendGrade1'] = false;
    			}
    			//valid CurrentAttend1 Year
    			if (preg_match('/\d{4}/',$aData[27]) && preg_match('/\d{4}/',$aData[28])){
    				$resultArr[$i]['validCurrentAttendYear1'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendYear1'] = false;
    			}
			}else{
				$resultArr[$i]['validCurrentAttendGrade1'] = true;
				$resultArr[$i]['validCurrentAttendYear1'] = true;
			}
			
			if($aData[29] !== ''){
//    			//valid CurrentAttend2 Grade
//    			if (preg_match('/.+-.+/',$aData[28])) {
//    				$resultArr[$i]['validCurrentAttendGrade2'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendGrade2'] = false;
//    			}
//    			
//    			//valid CurrentAttend2 Year
//    			if (preg_match('/\d{4} ?- ?\d{4}/',$aData[29])) {
//    				$resultArr[$i]['validCurrentAttendYear2'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendYear2'] = false;
//    			}
				//valid CurrentAttend1 Grade
    			if ($aData[30] && $aData[31]){
    				$resultArr[$i]['validCurrentAttendGrade2'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendGrade2'] = false;
    			}
    			//valid CurrentAttend1 Year
    			if (preg_match('/\d{4}/',$aData[32]) && preg_match('/\d{4}/',$aData[33])){
    				$resultArr[$i]['validCurrentAttendYear2'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendYear2'] = false;
    			}
			}else{
				$resultArr[$i]['validCurrentAttendGrade2'] = true;
				$resultArr[$i]['validCurrentAttendYear2'] = true;
			}
			
			//valid twins
			if($aData[34]){
    			if(strtoupper($aData[34]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[34]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validTwins'] = true;
    			}
    			else{
    				$resultArr[$i]['validTwins'] = false;
    			}
			}else{
				$resultArr[$i]['validTwins'] = true;
			}
			
			//// Parent ////
			//valid G1ChineseName
			if($aData[36] == ''){
				$resultArr[$i]['validG1ChineseNameEmpty'] = false;
			}else{
				$resultArr[$i]['validG1ChineseNameEmpty'] = true;
			    
			    if($this->checkIsChinese($aData[36]) || $this->checkIsEmpty($aData[36])){
			        $resultArr[$i]['validG1ChineseName'] = true;
			    }else{
			        $resultArr[$i]['validG1ChineseName'] = false;
			    }
			}
			//valid G1EnglishName
			if($aData[37] == ''){
				$resultArr[$i]['validG1EnglishNameEmpty'] = false;
			}else{
				$resultArr[$i]['validG1EnglishNameEmpty'] = true;
			    
			    if($this->checkIsEnglish($aData[37]) || $this->checkIsEmpty($aData[37])){
			        $resultArr[$i]['validG1EnglishName'] = true;
			    }else{
			        $resultArr[$i]['validG1EnglishName'] = false;
			    }
			}
			
			if($aData[36]){
			    //valid G1MobileNo
    			if (
    			    preg_match('/^[0-9]*$/',$aData[38]) ||
    			    $this->checkIsEmpty($aData[38]) || 
    			    $aData[38] == ''
			    ) {
    				$resultArr[$i]['validG1MobileNo'] = true;
    			}
    			else{
    				$resultArr[$i]['validG1MobileNo'] = false;
    			}
    			
    			//valid G1Email
    			if (
    			    preg_match('/\S+@\S+\.\S+/',$aData[39]) ||
    			    $this->checkIsEmpty($aData[39]) || 
    			    $aData[39] == ''
			    ) {
    				$resultArr[$i]['validG1Email'] = true;
    			}
    			else{
    				$resultArr[$i]['validG1Email'] = false;
    			}
			}else{
				$resultArr[$i]['validG1MobileNo'] = true;
				$resultArr[$i]['validG1Email'] = true;
			}

			//valid G2ChineseName
			if($aData[42] == ''){
			    $resultArr[$i]['validG2ChineseNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validG2ChineseNameEmpty'] = true;
			    
			    if($this->checkIsChinese($aData[42]) || $this->checkIsEmpty($aData[42])){
			        $resultArr[$i]['validG2ChineseName'] = true;
			    }else{
			        $resultArr[$i]['validG2ChineseName'] = false;
			    }
			}
			//valid G2EnglishName
			if($aData[43] == ''){
			    $resultArr[$i]['validG2EnglishNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validG2EnglishNameEmpty'] = true;
			    
			    if($this->checkIsEnglish($aData[43]) || $this->checkIsEmpty($aData[43])){
			        $resultArr[$i]['validG2EnglishName'] = true;
			    }else{
			        $resultArr[$i]['validG2EnglishName'] = false;
			    }
			}
			
			if($aData[42]){
    			//valid G2MobileNo
    			if (
    			    preg_match('/^[0-9]*$/',$aData[44]) ||
    			    $this->checkIsEmpty($aData[44]) || 
    			    $aData[44] == ''
			    ) {
    				$resultArr[$i]['validG2MobileNo'] = true;
    			}
    			else{
    				$resultArr[$i]['validG2MobileNo'] = false;
    			}
    			
    			//valid G2Email
    			if (
    			    preg_match('/\S+@\S+\.\S+/',$aData[45]) ||
    			    $this->checkIsEmpty($aData[45]) || 
    			    $aData[45] == ''
			    ) {
    				$resultArr[$i]['validG2Email'] = true;
    			}
    			else{
    				$resultArr[$i]['validG2Email'] = false;
    			}
			}else{
				$resultArr[$i]['validG2MobileNo'] = true;
				$resultArr[$i]['validG2Email'] = true;
			}
			
			//// Other ////
			//valid memberOfHKUGA
			if($aData[50]){
    			if(strtoupper($aData[50]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[50]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validMemberOfHKUGA'] = true;
    			}
    			else{
    				$resultArr[$i]['validMemberOfHKUGA'] = false;
    			}
			}else{
				$resultArr[$i]['validMemberOfHKUGA'] = true;
			}
			
			//valid memberOfHKUGAFoundation
			if($aData[53]){
    			if(strtoupper($aData[53]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[53]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validMemberOfHKUGAFoundation'] = true;
    			}
    			else{
    				$resultArr[$i]['validMemberOfHKUGAFoundation'] = false;
    			}
			}else{
				$resultArr[$i]['validMemberOfHKUGAFoundation'] = true;
			}
			
			//// Sibling ////
			if($aData[55] !== ''){
    			//valid current attempt sibling class1
    			if($aData[57] !== ''){
        			if (preg_match('/[1-6][a-dA-D]/',$aData[57])) {
        				$resultArr[$i]['validOthersRelativeClassPosition1'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassPosition1'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassPosition1'] = true;
    			}
    			
    			//valid current attempt sibling year1
    			if($aData[58] !== ''){
        			if (
        			    $aData[58] >= 2006 &&
        			    $aData[58] <= ($admission_year_start-1)
        		    ) {
        				$resultArr[$i]['validOthersRelativeClassYear1'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassYear1'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassYear1'] = true;
    			}
			}else{
				$resultArr[$i]['validOthersRelativeClassPosition1'] = true;
				$resultArr[$i]['validOthersRelativeClassYear1'] = true;
			}

			if($aData[59] !== ''){
    			//valid current attempt sibling class2
    			if($aData[61] !== ''){
        			if (preg_match('/[1-6][a-dA-D]/',$aData[61])) {
        				$resultArr[$i]['validOthersRelativeClassPosition2'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassPosition2'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassPosition2'] = true;
    			}
    			
    			//valid current attempt sibling year2
    			if($aData[62] !== ''){
        			if (
        			    $aData[62] >= 2006 &&
        			    $aData[62] <= ($admission_year_start-1)
        		    ) {
        				$resultArr[$i]['validOthersRelativeClassYear2'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassYear2'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassYear2'] = true;
    			}
			}else{
				$resultArr[$i]['validOthersRelativeClassPosition2'] = true;
				$resultArr[$i]['validOthersRelativeClassYear2'] = true;
			}
			
			if($aData[63] !== ''){
    			//valid other sibling DOB1
    			$aData[65] = getDefaultDateFormat($aData[65]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[65]) ) {
    			    list($year , $month , $day) = explode('-',$aData[65]);
    			    $resultArr[$i]['validOthersRelativeBirth1'] = checkdate($month , $day , $year);
    			}
    			else{
    			    $resultArr[$i]['validOthersRelativeBirth1'] = false;
    			}
    			
    			//valid other sibling twins1
    			if($aData[66] == $kis_lang['Admission']['yes'] || $aData[66] == $kis_lang['Admission']['no']){
    				$resultArr[$i]['validOthersRelativeTwin1'] = true;
    			}
    			else{
    				$resultArr[$i]['validOthersRelativeTwin1'] = false;
    			}
			}else{
			    $resultArr[$i]['validOthersRelativeBirth1'] = true;
				$resultArr[$i]['validOthersRelativeTwin1'] = true;
			}

			if($aData[70] !== ''){
    			//valid other sibling DOB2
    			$aData[71] = getDefaultDateFormat($aData[71]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[71]) ) {
    			    list($year , $month , $day) = explode('-',$aData[71]);
    			    $resultArr[$i]['validOthersRelativeBirth2'] = checkdate($month , $day , $year);
    			}
    			else{
    			    $resultArr[$i]['validOthersRelativeBirth2'] = false;
    			}
    			
    			//valid other sibling twins2
    			if($aData[72] == $kis_lang['Admission']['yes'] || $aData[72] == $kis_lang['Admission']['no']){
    				$resultArr[$i]['validOthersRelativeTwin2'] = true;
    			}
    			else{
    				$resultArr[$i]['validOthersRelativeTwin2'] = false;
    			}
			}else{
			    $resultArr[$i]['validOthersRelativeBirth2'] = true;
				$resultArr[$i]['validOthersRelativeTwin2'] = true;
			}
			

			$resultArr[$i]['validData'] = $aData[1];
			$i++;
		} // End foreach
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
			if(!$aResult['validAdmissionDate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidAdmissionDate'];
			}
			if(!$aResult['validAdmissionNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidApplicationNo'];
			}
			else if(!$aResult['validChineseNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyChineseName'];
			}
			else if(!$aResult['validChineseName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidChineseName'];
			}
			else if(!$aResult['validEnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyEnglishName'];
			}
			else if(!$aResult['validEnglishName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidEnglishName'];
			}
			else if(!$aResult['validDateOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyDateOfBirth'];
			}
			else if(!$aResult['validDateOfBirth']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidDateOfBirth'];
			}
			else if(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfGender'];
			}
			else if(!$aResult['validPlaceOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyPlaceOfBirth'];
			}
			else if(!$aResult['validBirthCertNoType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidFormatOfBirthCertNo'];
			}
			else if(!$aResult['validBirthCertNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidBirthCertNo'];
			}
			else if(!$aResult['validSpokenLanguageForInterview']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfSpokenLanguageForInterview'] ;
			}
			else if(!$aResult['validAddressStreetEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyAddressStreet'] ;
			}
			else if(!$aResult['validAddressDistrictEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyAddressDistrict'] ;
			}
			else if(!$aResult['validAddressDistrict']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfAddressDistrict'] ;
			}
			else if(!$aResult['validAddressAreaEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyAddressArea'] ;
			}
			else if(!$aResult['validTelHome']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidTelHome'] ;
			}
			else if(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidContactEmail'] ;
			}
			else if(!$aResult['validNoBrotherSister']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidNoBrotherSister'] ;
			}
			else if(!$aResult['validBrotherSisterRank']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidNoBrotherSisterRank'] ;
			}
			else if(!$aResult['validContactPerson']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfContactPerson'] ;
			}
			else if(!$aResult['validCurrentAttendGrade1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendGrade'] ;
			}
			else if(!$aResult['validCurrentAttendYear1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendYear'] ;
			}
			else if(!$aResult['validCurrentAttendGrade2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendGrade'] ;
			}
			else if(!$aResult['validCurrentAttendYear2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendYear'] ;
			}
			else if(!$aResult['validTwins']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfTwins'] ;
			}
			else if(!$aResult['validPlaceOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyPlaceOfBirth'];
			}
			else if(!$aResult['validG1ChineseNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGChineseName'] ;
			}
			else if(!$aResult['validG1ChineseName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGChineseName'] ;
			}
			else if(!$aResult['validG1EnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGEnglishName'] ;
			}
			else if(!$aResult['validG1EnglishName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEnglishName'] ;
			}
			else if(!$aResult['validG1MobileNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGMobileNo'] ;
			}
			else if(!$aResult['validG1Email']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEmail'] ;
			}
			else if(!$aResult['validG2ChineseNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGChineseName'] ;
			}
			else if(!$aResult['validG2ChineseName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGChineseName'] ;
			}
			else if(!$aResult['validG2EnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGEnglishName'] ;
			}
			else if(!$aResult['validG2EnglishName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEnglishName'] ;
			}
			else if(!$aResult['validG2MobileNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGMobileNo'] ;
			}
			else if(!$aResult['validG2Email']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEmail'] ;
			}
			else if(!$aResult['validMemberOfHKUGA']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfMemberOfHKUGA'] ;
			}
			else if(!$aResult['validMemberOfHKUGAFoundation']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfMemberOfHKUGAFoundation'] ;
			}
			else if(!$aResult['validOthersRelativeClassPosition1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeClassPosition'] ;
			}
			else if(!$aResult['validOthersRelativeClassYear1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeYear'] ;
			}
			else if(!$aResult['validOthersRelativeClassPosition2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeClassPosition'] ;
			}
			else if(!$aResult['validOthersRelativeClassYear2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeYear'] ;
			}
			else if(!$aResult['validOthersRelativeBirth1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeBirth'] ;
			}
			else if(!$aResult['validOthersRelativeTwin1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfOthersRelativeTwin'] ;
			}
			else if(!$aResult['validOthersRelativeBirth2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeBirth'] ;
			}
			else if(!$aResult['validOthersRelativeTwin2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfOthersRelativeTwin'] ;
			}
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.$i.'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	function importDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		
		//// Get all ApplicationID START ////
		$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO";
		$allApplicationID = $this->returnVector($sql);
		//// Get all ApplicationID END ////
		
		//// Get string code START ////
		$allPlaceOfBirth = array();
		foreach($admission_cfg['PlaceOfBirth'] as $id=>$place){
		    if($id == 0){
		        continue;
		    }
		    $allPlaceOfBirth[ strtoupper($place) ] = $id;
		}
		
		$allBirthCertType = array(
		    strtoupper($kis_lang['Admission']['BirthCertType']['hk2']) => $admission_cfg['BirthCertType']['birthCert'], 
		    strtoupper($kis_lang['Admission']['HKUGAPS']['birthcertno']) => $admission_cfg['BirthCertType']['hkid']
		);
		
		$allLangSpoken = array(
		    strtoupper($kis_lang['Admission']['Languages']['Cantonese']) => 'Cantonese', 
		    strtoupper($kis_lang['Admission']['Languages']['English']) => 'English', 
		    strtoupper($kis_lang['Admission']['Languages']['Putonghua']) => 'Putonghua'
		);
		
		$allAddressDistrict = array();
		foreach($admission_cfg['AddressDistrict'] as $id=>$district){
		    $allAddressDistrict[ strtoupper($district) ] = $id;
		}
		
		$allAddressArea = array();
		foreach($admission_cfg['AddressArea'] as $id=>$area){
		    if($id == 3){
		        continue;
		    }
		    $allAddressArea[ strtoupper($area) ] = $id;
		}
		
		$allContactPerson = array(
		    strtoupper($kis_lang['Admission']['HKUGAPS']['father']) => 'Father', 
		    strtoupper($kis_lang['Admission']['HKUGAPS']['mother']) => 'Mother', 
		    strtoupper($kis_lang['Admission']['HKUGAPS']['guardian']) => 'Guardian'
		);
		//// Get string code END ////
		
		$resultArr = array();
		array_shift($data);
		array_shift($data);
		foreach($data as $aData){
			global $UserID;
			
			$isUpdate = (in_array(trim($aData[1]), $allApplicationID));
			
			//--- convert the text to key code [start]
		    if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[6] = 'M';
			}
			else if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[6] = 'F';
			}
			
			// Place Of Birth
			$_key = strtoupper($aData[7]);
			if(in_array( $_key, array_keys($allPlaceOfBirth) )){
			    $aData[7] = $allPlaceOfBirth[$_key];
			}
			
			// Birth cert type for non-other-type
			$_key = strtoupper($aData[8]);
			if(in_array( $_key, array_keys($allBirthCertType) )){
			    $birthCertType = $allBirthCertType[$_key];
			    $birthCertTypeOther = '';
			}else{
			    $birthCertType = $admission_cfg['BirthCertType']['others'];
			    $birthCertTypeOther = $aData[8];
			}
			
			// Lang for interview
			$_key = strtoupper($aData[10]);
			if(in_array( $_key, array_keys($allLangSpoken) )){
			    $aData[10] = $allLangSpoken[$_key];
			}
			
			
			// Address District
			$_key = strtoupper($aData[17]);
			if(in_array( $_key, array_keys($allAddressDistrict) )){
			    $aData[17] = $allAddressDistrict[$_key];
			}
			
			// Address Area
			$_key = strtoupper($aData[18]);
			if(in_array( $_key, array_keys($allAddressArea) )){
			    $aData[18] = $allAddressArea[$_key];
			}

			// Contact Persion
			$_contactPersons = explode(',', $aData[23]);
			$contactPersons = array();
			foreach((array)$_contactPersons as $person){
    			$_key = strtoupper(trim($person));
    			if(in_array( $_key, array_keys($allContactPerson) )){
    			    $contactPersons[] = $allContactPerson[$_key];
    			}
			}
			
			// Twins
			if(strtoupper($aData[34]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[34] = '1';
			}
			else if(strtoupper($aData[34]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[34] = '0';
			}
			
			// HKUGA member
			if(strtoupper($aData[50]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[50] = '1';
			}
			else if(strtoupper($aData[50]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[50] = '0';
			}
			
			// HKUGA Foundation member
			if(strtoupper($aData[53]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[53] = '1';
			}
			else if(strtoupper($aData[53]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[53] = '0';
			}

			// Sibling is Twins
			if(strtoupper($aData[66]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[66] = '1';
			}
			else if(strtoupper($aData[66]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[66] = '0';
			}
			if(strtoupper($aData[72]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[72] = '1';
			}
			else if(strtoupper($aData[72]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[72] = '0';
			}
			
			//valid birth cert type
			/*foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$aData[8] =$_type;
					break;
				}
			}*/
			
		    $result = array();

		    if($isUpdate){
		        $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '{$aData[1]}'";
		        $result[] = $this->db_db_query($sql);
		    }else{
		    	if(!$aData[1]){
		    		$aData[1] = $this->newApplicationNumber2($_REQUEST['classLevelID']);
		    	}
		        $result[] = $this->insertApplicationStatus($this, $aData[1]);
		        $sql = "INSERT INTO ADMISSION_STU_INFO (ApplicationID) VALUES ('{$aData[1]}')";
		        $result[] = $this->db_db_query($sql);
		    }
		    
		    $sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				SET
	     			stu.ChineseName = '".$aData[3]."',
	      			stu.EnglishName = '".$aData[4]."', 
	      			stu.DOB = '".getDefaultDateFormat($aData[5])." 00:00:00',	
	      			stu.Gender = '".$aData[6]."',
	      			stu.PlaceOfBirth = '".$aData[7]."',
					stu.BirthCertType = '{$birthCertType}' ,
					stu.BirthCertTypeOther = '{$birthCertTypeOther}' ,
	      			stu.BirthCertNo = '".$aData[9]."' ,
      			    stu.LangSpokenAtHome = '{$aData[10]}',
                    stu.AddressRoom = '".mysql_real_escape_string($aData[11])."',
                    stu.AddressFloor = '".mysql_real_escape_string($aData[12])."',
                    stu.AddressBlock = '".mysql_real_escape_string($aData[13])."',
                    stu.AddressBldg = '".mysql_real_escape_string($aData[14])."',
                    stu.AddressEstate = '".mysql_real_escape_string($aData[15])."',
                    stu.AddressStreet = '".mysql_real_escape_string($aData[16])."',
                    stu.AddressDistrict = '".mysql_real_escape_string($aData[17])."',
                    stu.Address = '".mysql_real_escape_string($aData[18])."',
                    stu.HomeTelNo = '{$aData[19]}',
                    stu.Email = '{$aData[20]}',
                    stu.IsTwinsApplied = '{$aData[34]}',	
                    stu.TwinsApplicationID  = '{$aData[35]}',
					stu.DateModified = NOW(),
				    stu.ModifiedBy = '".$UserID."'
				WHERE 
					stu.ApplicationID = '".$aData[1]."'
	    	";
		    
		    $result[] = $this->db_db_query($sql);
		    ######## Student Cust START ########
		    $data = array();
		    
		    $data['NoBrotherSister'] = $aData[21];
		    $data['BrotherSisterRank'] = $aData[22];
		    $data['ContactPerson'] = $contactPersons;
		    
		    $data['CurrentAttend1'] = $aData[24];
		    
//		    $grades1 = explode('-', $aData[25]);
		    $data['Grades1Start'] = trim($aData[25]);
		    $data['Grades1End'] = trim($aData[26]);
		    
//		    $year1 = explode('-', $aData[26]);
		    $data['Year1Start'] = trim($aData[27]);
		    $data['Year1End'] = trim($aData[28]);
		    
		    $data['CurrentAttend2'] = $aData[29];
		    
//		    $grades2 = explode('-', $aData[28]);
		    $data['Grades2Start'] = trim($aData[30]);
		    $data['Grades2End'] = trim($aData[31]);
		    
//		    $year2 = explode('-', $aData[29]);
		    $data['Year2Start'] = trim($aData[32]);
		    $data['Year2End'] = trim($aData[33]);
		    
		    $result[] = $this->insertApplicationStudentInfoCust($data, $aData[1], $isUpdate=1);
		    ######## Student Cust END ########
		    
		    
		    ######## Parent START ########
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[36],
		        'EnglishName' => $aData[37],
		        'Mobile' => $aData[38],
		        'Email' => $aData[39],
		        'JobTitle' => $aData[40],
		        'Company' => $aData[41],
		        'PG_TYPE' => 'F'
		    ), $isUpdate);
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[42],
		        'EnglishName' => $aData[43],
		        'Mobile' => $aData[44],
		        'Email' => $aData[45],
		        'JobTitle' => $aData[46],
		        'Company' => $aData[47],
		        'PG_TYPE' => 'M'
		    ), $isUpdate);
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[48],
		        'Relationship' => $aData[49],
		        'PG_TYPE' => 'G'
		    ), $isUpdate);

		    #### HKUGA Member START ####
		    $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '{$aData[1]}' AND Code in ('HKUGA_Member', 'HKUGA_Member_Name', 'HKUGA_Member_No', 'HKUGA_Foundation_Member', 'HKUGA_Foundation_Member_Name')";
		    $result[] = $this->db_db_query($sql);
		    
		    $result[] = $this->insertApplicationCustInfo(array(
    		    'Code' => 'HKUGA_Member',
    		    'Value' => $aData[50]
		    ), $aData[1]);
		    if($aData[50]){
		        $result[] = $this->insertApplicationCustInfo(array(
		            'Code' => 'HKUGA_Member_Name',
		            'Value' => $aData[51]
		        ), $aData[1]);
		         
		        $result[] = $this->insertApplicationCustInfo(array(
		            'Code' => 'HKUGA_Member_No',
		            'Value' => $aData[52]
		        ), $aData[1]);
		    }
		    #### HKUGA Member END ####
		    
		    #### HKUGA Education Foundation START ####
		    $result[] = $this->insertApplicationCustInfo(array(
    		    'Code' => 'HKUGA_Foundation_Member',
    		    'Value' => $aData[53]
		    ), $aData[1]);
		    if($aData[53]){
		        $result[] = $this->insertApplicationCustInfo(array(
		            'Code' => 'HKUGA_Foundation_Member_Name',
		            'Value' => $aData[54]
		        ), $aData[1]);
		    }
		    #### HKUGA Education Foundation END ####
		    ######## Parent END ########
		    
		    
		    ######## Siblings START ########
		    $data = array();
            $data['OthersRelativeStudiedNameChi1'] = $aData[55];
            $data['OthersRelativeStudiedNameEng1'] = $aData[56];
            $data['OthersRelativeClassPosition1'] = $aData[57];
            $data['OthersRelativeGraduationYear1'] = $aData[58];
            
            $data['OthersRelativeStudiedNameChi2'] = $aData[59];
            $data['OthersRelativeStudiedNameEng2'] = $aData[60];
            $data['OthersRelativeClassPosition2'] = $aData[61];
            $data['OthersRelativeGraduationYear2'] = $aData[62];
            
            $data['OthersRelativeNameChi1'] = $aData[63];
            $data['OthersRelativeNameEng1'] = $aData[64];
            $data['OthersRelativeBirth1'] = $aData[65];
            $data['OthersRelativeTwin1'] = $aData[66];
            $data['OthersRelativeSchool1'] = $aData[67];
            $data['OthersRelativeGrade1'] = $aData[68];
            
            $data['OthersRelativeNameChi2'] = $aData[69];
            $data['OthersRelativeNameEng2'] = $aData[70];
            $data['OthersRelativeBirth2'] = $aData[71];
            $data['OthersRelativeTwin2'] = $aData[72];
            $data['OthersRelativeSchool2'] = $aData[73];
            $data['OthersRelativeGrade2'] = $aData[74];

            $result[] = $this->insertApplicationRelativesInfoCust($data, $aData[1], $isUpdate);
		    ######## Siblings END ########

            ######## Update School Year Record START ########
            $data = array();
            $data['sus_status'] = $_REQUEST['classLevelID']; // ApplyLevel
            $data['token'] = '';
            $result[] = $this->insertApplicationOthersInfo($data, $aData[1]);
            ######## Update School Year Record END ########
		    
//		    $sql = "
//				UPDATE 
//					ADMISSION_APPLICATION_STATUS
//		    	SET
//	     			Status = '".$aData[41]."',
//					ReceiptID = '".$aData[42]."',
//					ReceiptDate = '".$aData[43]." 00:00:00',
//					Handler = '".$aData[44]."',
//					InterviewDate = '".$aData[45]."',
//					isNotified = '".$aData[46]."',
//					DateModified = NOW(),
//					ModifiedBy = '".$UserID."'
//				WHERE 
//					ApplicationID = '".$aData[1]."'
//		   	";
//		   
//		    $result[] = $this->db_db_query($sql);
		     
			$resultArr[] = !in_array(false, $result);
			//debug_pr($aData);
		}
		return $resultArr;
	}
	
	function getExportHeader($lang = ''){
		global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$headerArray = array();
		
		if($lang == 'en'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}else if($lang == 'b5'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}
		
		//for student info
		$headerArray[] = $kis_lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray[] = $kis_lang['Admission']['applyLevel'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['chinesename'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['englishname'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['birthCertNo'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['ethnicity'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['langspokenathome'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['address'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['district'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['ContactNo'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['email'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['FormerSchool'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['ClassLastAttended'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['religion'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['BaptismCertNo'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['Church'];
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['SHCK']['Siblings']} ({$kis_lang['Admission']['SHCK']['ElderBrother']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['SHCK']['Siblings']} ({$kis_lang['Admission']['SHCK']['ElderSister']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['SHCK']['Siblings']} ({$kis_lang['Admission']['SHCK']['YoungerBrother']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['SHCK']['Siblings']} ({$kis_lang['Admission']['SHCK']['YoungerSister']})";
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['LangForInterview'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['SHCK']['SessionChoice'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['KTLMSKG']['twins'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['KTLMSKG']['twinsID'];
		
		//for parent info
		$headerArray['parentInfo'][] = $kis_lang['Admission']['chinesename'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['englishname'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['religion'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['occupation'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['SHCK']['OfficeAddress'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['SHCK']['OfficeTel'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['SHCK']['MobileTel'];
		
		
		//for other info
		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['SibilingsStudyingInThisSchool'];
		for($i=0;$i<3;$i++){
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['name'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['Relationship'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['class'].($i+1);
		}
		
		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['FormerStudentInThisSchool'];
		for($i=0;$i<3;$i++){
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['name'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['Relationship'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['YearOfGraduation'].($i+1);
		}
		
		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['SibilingsStudyingInOtherCanossianSchool'];
		for($i=0;$i<3;$i++){
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['name'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['Relationship'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['class'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['NameOfSchool'].($i+1);
		}
		
		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['FormerStudentInOtherCanossianSchool'];
		for($i=0;$i<3;$i++){
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['name'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['Relationship'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['YearOfGraduation'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['NameOfSchool'].($i+1);
		}
		
		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['EmployeeOfCanossianInstitutions'];
		for($i=0;$i<3;$i++){
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['name'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['Relationship'].($i+1);
    		$headerArray['otherInfo'][] = $kis_lang['Admission']['SHCK']['NameOfOurInstitution'].($i+1);
		}
		
		
		
		//for official use
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (1)";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (2)";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (3)";
		$headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
		
		
		######## Header START ########
		#### Admission Info START ####
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		#### Admission Info END ####
		
		#### Student Info START ####
		$exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
		for($i=0; $i < count($headerArray['studentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		#### Student Info END ####
		
		#### Parent Info START ####
		$exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['HKUGAPS']['father']})";
		for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		$exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['HKUGAPS']['mother']})";
		for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		#### Parent Info END ####
		

		#### Other Info START ####
		$exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
		for($i=0; $i < count($headerArray['otherInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		#### Other Info END ####

		#### Offical Use START ####
		$exportColumn[0][] = $kis_lang['remarks'];
		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
			$exportColumn[0][] = "";
		}
		#### Offical Use END ####
		
		//sub header
		$exportColumn[1] = array_merge(
			array($headerArray[0],$headerArray[1],$headerArray[2]), 
			$headerArray['studentInfo'],
			$headerArray['parentInfo'],
			$headerArray['parentInfo'],
			$headerArray['otherInfo'],
			$headerArray['officialUse']
		);
		//if($lang)
		//include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
		return $exportColumn;
	}
	
	function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg, $Lang, $kis_lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$parentInfoArr = array();
		foreach($parentInfo as $parent){
			foreach($parent as $para=>$info){
				$parentInfoArr[$parent['type']][$para] = $info;
			}
		}
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
		
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		
		$dataArray = array();		
		
		#### Student Info START ####
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$dataArray[] = $classLevel[$otherInfo['classLevelID']];
		$dataArray['studentInfo'][] = $studentInfo['student_name_b5'];
		$dataArray['studentInfo'][] = $studentInfo['student_name_en'];
		$dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
		$dataArray['studentInfo'][] = $studentInfo['BirthCertNo'];
		$dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
		$dataArray['studentInfo'][] = $admission_cfg['Ethnicity'][ $studentInfo['Nationality'] ];
		$dataArray['studentInfo'][] = $admission_cfg['PlaceOfBirth'][ $studentInfo['PlaceOfBirth'] ];
		$dataArray['studentInfo'][] = $admission_cfg['Language'][ $studentInfo['LangSpokenAtHome'] ];
		$dataArray['studentInfo'][] = $studentInfo['Address'];
		$dataArray['studentInfo'][] = $admission_cfg['District'][ $studentInfo['AddressDistrict'] ];
		$dataArray['studentInfo'][] = $studentInfo['HomeTelNo'];
		$dataArray['studentInfo'][] = $studentInfo['Email'];
		$dataArray['studentInfo'][] = $studentInfo['LastSchool'];
		$dataArray['studentInfo'][] = $studentInfo['LastSchoolLevel'];
		$dataArray['studentInfo'][] = $studentInfo['ReligionOther'];
		$dataArray['studentInfo'][] = $custInfo['BaptismCertNo'][0]['Value'];
		$dataArray['studentInfo'][] = $studentInfo['Church'];
		$dataArray['studentInfo'][] = $custInfo['ElderBrother'][0]['Value'];
		$dataArray['studentInfo'][] = $custInfo['ElderSister'][0]['Value'];
		$dataArray['studentInfo'][] = $custInfo['YoungerBrother'][0]['Value'];
		$dataArray['studentInfo'][] = $custInfo['YoungerSister'][0]['Value'];
		$dataArray['studentInfo'][] = $admission_cfg['LanguageForInterview'][ $custInfo['LangForInterview'][0]['Value'] ];
		$dataArray['studentInfo'][] = $admission_cfg['SessionChoice'][ $custInfo['SessionChoice'][0]['Value'] ];
		$dataArray['studentInfo'][] = $studentInfo['IsTwinsApplied']=='Y'?$Lang['Admission']['yes']:$Lang['Admission']['no'];
		$dataArray['studentInfo'][] = $studentInfo['TwinsApplicationID'];
		#### Student Info END ####

		#### Parent Info START ####
		foreach(array('F', 'M') as $type){
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['ChineseName'];
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['EnglishName'];
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['Company'];
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['JobTitle'];
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['OfficeAddress'];
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['OfficeTelNo'];
    		$dataArray['parentInfo'][] = $parentInfoArr[$type]['Mobile'];
		}
		#### Parent Info END ####

		#### Other Info START ####
		$SibilingsStudyingInThisSchool = ($custInfo['SibilingsStudyingInThisSchool'][0]['Value'] == "1");
		$dataArray['otherInfo'][] = ($SibilingsStudyingInThisSchool?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']);
		for($i=0;$i<3;$i++){
		    if($SibilingsStudyingInThisSchool){
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInThisSchool_Name'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInThisSchool_Relationship'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInThisSchool_Class'][$i]['Value'];
		    }else{
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		    }
		}
		
		$FormerStudentInThisSchool = ($custInfo['FormerStudentInThisSchool'][0]['Value'] == "1");
		$dataArray['otherInfo'][] = ($FormerStudentInThisSchool?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']);
		for($i=0;$i<3;$i++){
		    if($FormerStudentInThisSchool){
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInThisSchool_Name'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInThisSchool_Relationship'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInThisSchool_YearOfGraduation'][$i]['Value'];
		    }else{
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		    }
		}
		
		$SibilingsStudyingInOtherCanossianSchool = ($custInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value'] == "1");
		$dataArray['otherInfo'][] = ($SibilingsStudyingInOtherCanossianSchool?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']);
		for($i=0;$i<3;$i++){
		    if($SibilingsStudyingInOtherCanossianSchool){
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInOtherCanossianSchool_Name'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInOtherCanossianSchool_Relationship'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInOtherCanossianSchool_Class'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['SibilingsStudyingInOtherCanossianSchool_NameOfSchool'][$i]['Value'];
		    }else{
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		    }
		}
		
		$FormerStudentInOtherCanossianSchool = ($custInfo['FormerStudentInOtherCanossianSchool'][0]['Value'] == "1");
		$dataArray['otherInfo'][] = ($FormerStudentInOtherCanossianSchool?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']);
		for($i=0;$i<3;$i++){
		    if($FormerStudentInOtherCanossianSchool){
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInOtherCanossianSchool_Name'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInOtherCanossianSchool_Relationship'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInOtherCanossianSchool_YearOfGraduation'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['FormerStudentInOtherCanossianSchool_NameOfSchool'][$i]['Value'];
		    }else{
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		    }
		}
		
		$EmployeeOfCanossianInstitutions = ($custInfo['EmployeeOfCanossianInstitutions'][0]['Value'] == "1");
		$dataArray['otherInfo'][] = ($EmployeeOfCanossianInstitutions?$kis_lang['Admission']['yes']:$kis_lang['Admission']['no']);
		for($i=0;$i<3;$i++){
		    if($EmployeeOfCanossianInstitutions){
    		    $dataArray['otherInfo'][] = $custInfo['EmployeeOfCanossianInstitutions_Name'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['EmployeeOfCanossianInstitutions_Relationship'][$i]['Value'];
    		    $dataArray['otherInfo'][] = $custInfo['EmployeeOfCanossianInstitutions_NameOfOurInstitution'][$i]['Value'];
		    }else{
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		        $dataArray['otherInfo'][] = '';
		    }
		}
		#### Other Info END ####
		

		#### Official Use START ####
		$dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
		$dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
		$dataArray['officialUse'][] = $status['receiptID'];
		$dataArray['officialUse'][] = $status['receiptdate'];
		$dataArray['officialUse'][] = $status['handler'];
		$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
		$dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
		$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
		$dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
		$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
		$dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
		$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
		$dataArray['officialUse'][] = $status['remark'];
		#### Official Use END ####
		
		$ExportArr = array_merge(
			array($dataArray[0],$dataArray[1],$dataArray[2]),
			$dataArray['studentInfo'],
			$dataArray['parentInfo'],
			$dataArray['otherInfo'], 
			$dataArray['officialUse']
		);
		
		return $ExportArr;
	}
	function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$dataArray = array();
		
		if($tabID == 2){
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			$studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = $studentInfo['homephoneno']; //Mobile
			$dataArray[0][] = ''; //Fax
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = ''; //Remarks
			$dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
			$dataArray[0][] = (is_numeric($studentInfo['homeaddress'])?$Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']]:$studentInfo['homeaddress']); //Address
			if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
			{
				$dataArray[0][] = ''; //CardID
				if($sys_custom['SupplementarySmartCard']){
					$dataArray[0][] = ''; //CardID2
					$dataArray[0][] = ''; //CardID3
				}
			}
			if($special_feature['ava_hkid'])
				$dataArray[0][] = $studentInfo['birthcertno']; //HKID
			if($special_feature['ava_strn'])
				$dataArray[0][] = ''; //STRN
			if($plugin['medical'])
				$dataArray[0][] = ''; //StayOverNight
			$dataArray[0][] = $admission_cfg['Ethnicity'][ $studentInfo['Nationality'] ]; //Nationality
			$dataArray[0][] = $admission_cfg['PlaceOfBirth'][ $studentInfo['PlaceOfBirth'] ]; //PlaceOfBirth
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
		}
		else if($tabID == 3){
			$hasParent = false;
			$dataCount = array();
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			for($i=0;$i<count($parentInfo);$i++){
				if($parentInfo[$i]['type'] == 'F' && !$hasParent){
					$dataArray[0] = array();
					$dataArray[0][] = ''; //UserLogin
					$dataArray[0][] = ''; //Password
					$dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[0][] = 'M'; //Gender
					$dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[0][] = ''; //Fax
					$dataArray[0][] = ''; //Barcode
					$dataArray[0][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[0][] = ''; //HKID
					$dataArray[0][] = ''; //StudentLogin1
					$dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[0][] = ''; //StudentLogin2
					$dataArray[0][] = ''; //StudentEngName2
					$dataArray[0][] = ''; //StudentLogin3
					$dataArray[0][] = ''; //StudentEngName3
					//$hasParent = true;
					
					$dataCount[0] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'M' && !$hasParent){
					$dataArray[1] = array();
					$dataArray[1][] = ''; //UserLogin
					$dataArray[1][] = ''; //Password
					$dataArray[1][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[1][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[1][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[1][] = 'F'; //Gender
					$dataArray[1][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[1][] = ''; //Fax
					$dataArray[1][] = ''; //Barcode
					$dataArray[1][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[1][] = ''; //HKID
					$dataArray[1][] = ''; //StudentLogin1
					$dataArray[1][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[1][] = ''; //StudentLogin2
					$dataArray[1][] = ''; //StudentEngName2
					$dataArray[1][] = ''; //StudentLogin3
					$dataArray[1][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[1] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'G' && !$hasParent){
					$dataArray[2] = array();
					$dataArray[2][] = ''; //UserLogin
					$dataArray[2][] = ''; //Password
					$dataArray[2][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[2][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[2][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[2][] = ''; //Gender
					$dataArray[2][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[2][] = ''; //Fax
					$dataArray[2][] = ''; //Barcode
					$dataArray[2][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[2][] = ''; //HKID
					$dataArray[2][] = ''; //StudentLogin1
					$dataArray[2][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[2][] = ''; //StudentLogin2
					$dataArray[2][] = ''; //StudentEngName2
					$dataArray[2][] = ''; //StudentLogin3
					$dataArray[2][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[2] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
			}
			if($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]){
				$tempDataArray = $dataArray[0];
			}
			else if($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]){
				$tempDataArray = $dataArray[1];
			}
			else if($dataCount[2] > 0){
				$tempDataArray = $dataArray[2];
			}
			$dataArray = array();
			$dataArray[0] = $tempDataArray;
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE aoi.ApplyLevel = '{$applyLevel}' AND TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."'";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();
		
		$sql = "SELECT 
					a.Email as Email
				FROM ADMISSION_STU_INFO as a 
				WHERE a.ApplicationID = '".trim($applicationId)."'";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		if($subject == ''){
			$email_subject = "嘉諾撒聖心幼稚園入學申請通知 Sacred Heart Canossian Kindergarten Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	public function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();

		if($subject == ''){
			$email_subject = "嘉諾撒聖心幼稚園入學申請通知 Sacred Heart Canossian Kindergarten Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
		$sql = "SELECT
					a.Email as Email
				FROM ADMISSION_STU_INFO as a
				WHERE a.ApplicationID = '".trim($applicationId)."'";
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	
	function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
			//check date
			if ($aData[4] =='' && $aData[5] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
		       list($year , $month , $day) = explode('-',$aData[4]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[4] =='' && $aData[5] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."' AND trim(s.birthCertNo) = '".trim($aData[3])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[4] = getDefaultDateFormat($aData[4]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[4]." ".$aData[5]."',
				InterviewLocation = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1){
		$status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND st.status in ('".implode("','",$selectStatusArr)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		
		$sql='Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied, s.LangSpokenAtHome, c.Value as LangForInterview From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel 
				LEFT JOIN ADMISSION_CUST_INFO as c ON c.ApplicationID = o.ApplicationID AND c.Code = "LangForInterview"
				where o.ApplyYear = "'.$selectSchoolYearID.'" '.$status_cond.' order by y.YearName desc, o.ApplicationID';
				
		$result = $this->returnArray($sql);
		
		$allApplicant = array();
		$englishApplicant = array();
		$putonghuaApplicant = array();
		$cantoneseApplicant = array();
		$otherApplicant = array();
		
		for($i=0; $i<sizeof($result); $i++){
			$applicant = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel'], 'LangForInterview' => $result[$i]['LangForInterview']);
//			$allApplicant[] = $applicant;
			if($result[$i]['LangForInterview'] == '2'){
				$englishApplicant[] = $applicant;
			}
			else if($result[$i]['LangForInterview'] == '1'){
				$putonghuaApplicant[] = $applicant;
			}
			else if($result[$i]['LangForInterview'] == '0'){
				$cantoneseApplicant[] = $applicant;
			}
			else {
				$otherApplicant[] = $applicant;
			}
		}
//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);
		
		$result = array();
		
		$TwinsAssignedApplicant = array();
		
//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
				
				$sql = "SELECT RecordID, Quota, GroupName, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY Date, StartTime, GroupName";

				$interviewRecordIDArr = $this->returnArray($sql);

//				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
//	        	
//			        if(!$englishApplicant && !$putonghuaApplicant){
//			            break;
//			        }
//			        
//			        if($interviewRecordIDArr[$j]['GroupName'] == '101' && $englishApplicant){
//				        $previousClassLevel = $englishApplicant[0]['ClassLevel'];
//				        $sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
//				        $result[] = $this->db_db_query($sql);
//				        	
//				        for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
//				            $sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$englishApplicant[0]['ApplicationID']."' ";
//				            $result[] = $this->db_db_query($sql);
//				            array_shift($englishApplicant);
//				            if($previousClassLevel != $englishApplicant[0]['ClassLevel']){
//				                break;
//				            }
//				        }
//				        $interviewRecordIDArr[$j]['Quota'] -= $k;
//			        }
//			        else if($interviewRecordIDArr[$j]['GroupName'] == '102' && $putonghuaApplicant){
//				        $previousClassLevel = $putonghuaApplicant[0]['ClassLevel'];
//				        $sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
//				        $result[] = $this->db_db_query($sql);
//				        	
//				        for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
//				            $sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$putonghuaApplicant[0]['ApplicationID']."' ";
//				            $result[] = $this->db_db_query($sql);
//				            array_shift($putonghuaApplicant);
//				            if($previousClassLevel != $putonghuaApplicant[0]['ClassLevel']){
//				                break;
//				            }
//				        }
//				        $interviewRecordIDArr[$j]['Quota'] -= $k;
//			        }
//			    }

				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
	        	
			        if(!$englishApplicant){
			            break;
			        }
			        
			        if($interviewRecordIDArr[$j]['GroupName'] == '101' && $englishApplicant){
				        $previousClassLevel = $englishApplicant[0]['ClassLevel'];
				        $sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
				        $result[] = $this->db_db_query($sql);
				        	
				        for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				            $sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$englishApplicant[0]['ApplicationID']."' ";
				            $result[] = $this->db_db_query($sql);
				            array_shift($englishApplicant);
				            if($previousClassLevel != $englishApplicant[0]['ClassLevel']){
				            	$k++;
				                break;
				            }
				        }
				        $interviewRecordIDArr[$j]['Quota'] -= $k;
			        }
			    }
			    
			    for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
	        	
			        if(!$englishApplicant){
			            break;
			        }
			        
			        if($interviewRecordIDArr[$j]['GroupName'] == '102' && $englishApplicant){
				        $previousClassLevel = $englishApplicant[0]['ClassLevel'];
				        $sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
				        $result[] = $this->db_db_query($sql);
				        	
				        for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				            $sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$englishApplicant[0]['ApplicationID']."' ";
				            $result[] = $this->db_db_query($sql);
				            array_shift($englishApplicant);
				            if($previousClassLevel != $englishApplicant[0]['ClassLevel']){
				            	$k++;
				                break;
				            }
				        }
				        $interviewRecordIDArr[$j]['Quota'] -= $k;
			        }
			    }

			    for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
	        	
			        if(!$putonghuaApplicant){
			            break;
			        }
			        
			        if($interviewRecordIDArr[$j]['GroupName'] == '102' && $putonghuaApplicant){
				        $previousClassLevel = $putonghuaApplicant[0]['ClassLevel'];
				        $sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
				        $result[] = $this->db_db_query($sql);
				        	
				        for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				            $sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$putonghuaApplicant[0]['ApplicationID']."' ";
				            $result[] = $this->db_db_query($sql);
				            array_shift($putonghuaApplicant);
				            if($previousClassLevel != $putonghuaApplicant[0]['ClassLevel']){
				            	$k++;
				                break;
				            }
				        }
				        $interviewRecordIDArr[$j]['Quota'] -= $k;
			        }
			    }
				
				$previousClassLevel = '';
				$allApplicant = array_merge($englishApplicant, $putonghuaApplicant, $cantoneseApplicant, $otherApplicant);
				
				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
	        	
			        if(!$allApplicant){
			            break;
			        }
			        
			        if($interviewRecordIDArr[$j]['ClassLevelID'] == 0){
			        	$previousClassLevel = $allApplicant[0]['ClassLevel'];
				        $sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
				        $result[] = $this->db_db_query($sql);
			        }
			        else if($interviewRecordIDArr[$j]['ClassLevelID'] != $allApplicant[0]['ClassLevel']){
			        	continue;
			        }
			        	
			        for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
			            $sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
			            $result[] = $this->db_db_query($sql);
			            array_shift($allApplicant);
			            if($previousClassLevel != $allApplicant[0]['ClassLevel']){
			                break;
			            }
			        }
			
			    }
//		}
		
		//handling twins swraping [start]
		$sql='Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID'.($round>1?$round:'').' as InterviewSettingID, s.LangSpokenAtHome, c.Value as LangForInterview From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN ADMISSION_CUST_INFO as c ON c.ApplicationID = o.ApplicationID AND c.Code = "LangForInterview"
				where o.ApplyYear = "'.$selectSchoolYearID.'" AND s.IsTwinsApplied = "Y" '.$status_cond.' order by c.Value desc, o.ApplicationID';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
			for($j=0; $j<sizeof($twinsResult); $j++){
				if(strtolower(trim($twinsResult[$i]['TwinsApplicationID'])) == strtolower(trim($twinsResult[$j]['BirthCertNo'])) && !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)){
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName, StartTime From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" ';
					$originalSession = current($this->returnArray($sql));
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName, StartTime From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$j]['InterviewSettingID'].'" ';
					$twinsOriginalSession = current($this->returnArray($sql));
					
					$sql = 'Select i.RecordID From ADMISSION_INTERVIEW_SETTING as i LEFT JOIN ADMISSION_OTHERS_INFO as o ON o.InterviewSettingID'.($round>1?$round:'').' = i.RecordID LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID LEFT JOIN ADMISSION_CUST_INFO as c ON c.ApplicationID = o.ApplicationID AND c.Code = "LangForInterview" where i.ClassLevelID = "'.$originalSession['ClassLevelID'].'" AND i.Date = "'.$originalSession['Date'].'" AND i.StartTime = "'.$originalSession['StartTime'].'" AND i.Round = "'.$round.'" AND i.GroupName <> "'.$originalSession['GroupName'].'" AND i.RecordID <> "'.$twinsResult[$i]['InterviewSettingID'].'" AND i.RecordID <> "'.$twinsResult[$j]['InterviewSettingID'].'"  AND c.Value = "'.$twinsResult[$j]['LangForInterview'].'"';
					$newSession = current($this->returnArray($sql));
					
					if($newSession){
						$sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" AND s.IsTwinsApplied <> "Y"';
						$swapApplicant = current($this->returnArray($sql));
						if($swapApplicant){
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$twinsOriginalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
							//debug_pr($sql);
							$updateResult = $this->db_db_query($sql);
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$j]['ApplicationID'].'" ';
							//debug_pr($sql);
							$updateResult = $this->db_db_query($sql);
							$assignedTwins[] = $twinsResult[$j]['ApplicationID'];
							$assignedTwins[] = $twinsResult[$i]['ApplicationID'];
						}
					}
				}
			}
		}
		
		//handling twins swraping [end]
		
		return !in_array(false,$result);
	}
	
	function getInterviewTimeslotName($recordId, $round){
		$interviewTimeslotName = '';
		
		$sql = 'Select Date, StartTime, SchoolYearID, Round From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$recordId.'"';
		$result = current($this->returnArray($sql));
		
		$sql = 'Select distinct Date From ADMISSION_INTERVIEW_SETTING where SchoolYearID = "'.$result['SchoolYearID'].'" AND Round = "'.$result['Round'].'" order by Date';
		$result2 = $this->returnArray($sql);
		
		$sql = 'Select distinct StartTime From ADMISSION_INTERVIEW_SETTING where SchoolYearID = "'.$result['SchoolYearID'].'" AND Round = "'.$result['Round'].'" AND Date = "'.$result['Date'].'" order by StartTime';
		$result3 = $this->returnArray($sql);
		
		for($i=0; $i<count($result2); $i++){
			if($result2[$i]['Date'] == $result['Date']){
				$interviewTimeslotName .= chr(($i+65));
				break;
			}
		}

		for($i=0; $i<count($result3); $i++){
			if($result3[$i]['StartTime'] == $result['StartTime']){
				$interviewTimeslotName .= str_pad($i+1, 2, '0', STR_PAD_LEFT);
				break;
			}
		}

		return $interviewTimeslotName;
	}
	
	function getInterviewResult($StudentDateOfBirth, $StudentBirthCertNo, $SchoolYearID='', $round=1, $ApplicationID=''){
		
		if($SchoolYearID){
			$cond = " AND o.ApplyYear = '".$SchoolYearID."' ";
		}
		if($ApplicationID){
			$cond .= " AND o.ApplicationID = '".$ApplicationID."' ";
		}
		$sql = "SELECT o.ApplicationID, s.ChineseName, s.EnglishName, i.Date, i.StartTime, i.EndTime, i.GroupName, st.InterviewDate, st.InterviewLocation, o.ApplyLevel, i.RecordID 
				FROM ADMISSION_STU_INFO as s 
				JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID 
				JOIN ADMISSION_APPLICATION_STATUS as st ON st.ApplicationID = o.ApplicationID 
				LEFT JOIN ADMISSION_INTERVIEW_SETTING as i ON o.InterviewSettingID".($round>1?$round:'')." = i.RecordID
				WHERE st.Status <> 5 AND s.DOB = '".$StudentDateOfBirth."' AND s.DOB <> '' AND (s.BirthCertNo = '".strtoupper($StudentBirthCertNo)."' OR replace(replace(replace(s.BirthCertNo,')',''),'(',''),' ','') = '".strtoupper($StudentBirthCertNo)."') AND s.BirthCertNo <> '' ".$cond." ORDER BY o.ApplicationID desc";
		
		$result = current($this->returnArray($sql));
		
		if(!$result['ApplicationID']){
			return 0;
		}
		
//		if(!$result['Date']){
//			return 'NotAssigned';	
//		}
		
		return $result;
		
	}
}
?>