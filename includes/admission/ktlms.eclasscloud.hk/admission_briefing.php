<?php

class admission_briefing extends admission_briefing_base{
	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = ''){
		if($Briefing_ApplicantID == 0){
			return parent::sendMailToNewApplicant($Briefing_ApplicantID, $subject, $message, $email);
		}
		
		$applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
		$ApplicantID = $applicant['ApplicantID'];
		$deleteRecordLink = $this->getApplicantDeleteLink($Briefing_ApplicantID, $applicant['DeleteRecordPassKey']);
		$BriefingID = $applicant['BriefingID'];
		$briefingSession = $this->getBriefingSession($BriefingID);
		$yearStart = date('Y',getStartOfAcademicYear('',$briefingSession['SchoolYearID']));
		$briefingYear = $yearStart . '-' . (((int)$yearStart)+1);
		
		#### Email Date START ####
		$_starttime = strtotime($briefingSession['BriefingStartDate']);
		$_endtime = strtotime($briefingSession['BriefingEndDate']);
		$_endDay = date('g:i a', $_endtime);
		## Chinese START ##
		$weekDayArr = array('日','一','二','三','四','五','六');
		$_weekDay = $weekDayArr[ date('w', $_starttime) ];
		
		$BriefingDateChi = date('Y年m月d日(星期%1)', $_starttime);
		$BriefingDateChi = str_replace('%1', $_weekDay, $BriefingDateChi);

		$BriefingTimeChi = date('g:i a - %2', $_starttime);
		$BriefingTimeChi = str_replace('%2', $_endDay, $BriefingTimeChi);
		## Chinese END ##
		
		## English START ##
		$BriefingDateEng = date('j F Y (l)', $_starttime);
		$BriefingDateEng = str_replace('%2', $_endDay, $BriefingDateEng);
		
		## English END ##
		#### Email Date END ####
		
		@ob_start();
?>
<p>Dear Parents,</p>

<p>Thank you for your interested in Kowloon True Light School (Primary Section) Primary One Admission Talk <?=$briefingYear?>. We are pleased to inform that your application has been confirmed (Application #: <?=$ApplicantID?>). The date and time as below:</p>

<p>Date: <?=$BriefingDateEng?></p>

<p>Time: <?=$BriefingTimeChi?></p>

<p>Session title: <?=$briefingSession['Title']?></p>

<p>Please arrive 10 minutes before on that day to do the registration.</p>

<p>Thank you for your great cooperation.</p>

<p>If you want to cancel the application, click this link:</p>

<p><u><?=$deleteRecordLink?></u></p>

<p>&nbsp;</p>

<p>致家長:</p>

<p>感謝閣下報名九龍真光中學(小學) 小一入學簡介會 <?=$briefingYear?>。您已成功報名是次活動  (申請編號：<?=$ApplicantID?>)，日期及時間如下：</p>

<p>日期：<?=$BriefingDateChi?></p>

<p>時間：<?=$BriefingTimeChi?></p>

<p>簡介會標題：<?=$briefingSession['Title']?></p>

<p>請於當日提前10分鐘到達以便進行登記。感謝您的合作！</p>

<p>如需要取消申請，請按此連結：</p>

<p><u><?=$deleteRecordLink?></u></p>
<?php	
		$content = ob_get_clean();
		return parent::sendMailToNewApplicant($Briefing_ApplicantID, $subject, $message = $content, $email);
	}
	
	public function getAllBriefingEmailBySchoolYearID($schoolYearID){
		$sql = "SELECT
			ABAI.Email
		FROM
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		INNER JOIN
			ADMISSION_BRIEFING_SETTING ABS
		ON
			ABAI.BriefingID = ABS.BriefingID
		AND
			ABS.SchoolYearID = '{$schoolYearID}'";
		$rs = $this->returnVector($sql);
		
		return $rs;
	}
	
	public function insertBriefingApplicant($schoolYearID, $Data){
   		extract($Data);
   		
   		$emailArr = $this->getAllBriefingEmailBySchoolYearID($schoolYearID);
   		if(in_array($Email, $emailArr)){
   			return 0;
   		}
   		
   		return parent::insertBriefingApplicant($schoolYearID, $Data);
	}
} // End Class