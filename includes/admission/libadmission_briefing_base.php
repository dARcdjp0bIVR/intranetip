<?php
// Using: Pun
/**
 * Change Log:
 * 2016-04-06 Pun
 *  - Fixed encrypt file cannot run
 */
 
// This file will include $PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/admission_briefing.php"
class admission_briefing_base extends libdb{
	function admission_briefing_base(){
		global $kis_lang; //switch $lang for IP/EJ/KIS
		parent::__construct();
	}
	
######################## Briefing Session START ########################
	function insertBriefingSession($schoolYearID, $Data){
    	global $UserID;
   		extract($Data);
   		
   		$sql = "INSERT INTO 
			ADMISSION_BRIEFING_SETTING
		(
			SchoolYearID,
			BriefingStartDate,
			BriefingEndDate,
			Title,
			TotalQuota,
			QuotaPerApplicant,
			EnrolmentStartDate,
			EnrolmentEndDate,
			Instruction,
			DateInput,
			InputBy,
			DateModified,
			ModifiedBy
		) VALUES (
			'$schoolYearID',
			'$BriefingStartDate',
			'$BriefingEndDate',
			'$Title',
			'$TotalQuota',
			'$QuotaPerApplicant',
			'$EnrolmentStartDate',
			'$EnrolmentEndDate',
			'$Instruction',
			NOW(),
			'$UserID',
			NOW(),
			'$UserID'
		)";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function updateBriefingSession($BriefingID, $Data){
    	global $UserID;
   		extract($Data);
   		
   		if(!$BriefingID){ // Insert the data if new record
   			return $this->insertBriefingSession($schoolYearID, $Data);
   		}

   		$sql = "UPDATE 
			ADMISSION_BRIEFING_SETTING
		SET 
			SchoolYearID = '{$schoolYearID}', 
			BriefingStartDate = '{$BriefingStartDate}', 
			BriefingEndDate = '{$BriefingEndDate}', 
			Title = '{$Title}', 
			TotalQuota = '{$TotalQuota}', 
			QuotaPerApplicant = '{$QuotaPerApplicant}', 
			EnrolmentStartDate = '{$EnrolmentStartDate}', 
			EnrolmentEndDate = '{$EnrolmentEndDate}', 
			Instruction = '{$Instruction}',
			DateModified = NOW(),
			ModifiedBy = '{$UserID}'
		WHERE
			BriefingID = '{$BriefingID}'";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function getAllBriefingSession($schoolYearID ='', $includeDeleted = false){
		$notDeletedFilter = '';
		if($schoolYearID){
			$schoolYearIdFilter = " AND SchoolYearID = '{$schoolYearID}'";
		}
		if(!$includeDeleted){
			$notDeletedFilter = ' AND DateDeleted IS NULL';
		}
		
		$sql = "SELECT 
			* 
		FROM
			ADMISSION_BRIEFING_SETTING
		WHERE
			1=1
			{$schoolYearIdFilter}
			{$notDeletedFilter}
		ORDER BY 
			BriefingStartDate";
		$rs = $this->returnResultSet($sql);
		
		return $rs;
	}
	
	function getActiveBriefingSession($schoolYearID){
		$activeSession = array();
		$rs = $this->getAllBriefingSession($schoolYearID);
		
		$curDate = date('Y-m-d H:i:s');
		foreach($rs as $r){
			if($r['EnrolmentStartDate'] <= $curDate && $r['EnrolmentEndDate'] >= $curDate){
				$activeSession[] = $r;
			}
		}
		return $activeSession;
	}
	
	function isBriefingSessionActive($BriefingID){
		$rs = $this->getBriefingSession($BriefingID);
		
		$curDate = date('Y-m-d H:i:s');
		return (
			$rs['EnrolmentStartDate'] <= $curDate && 
			$rs['EnrolmentEndDate'] >= $curDate &&
			$rs['DateDeleted'] == ''
		);
	}
	
	function getBriefingSession($BriefingID){
		$rs = $this->getBriefingSessionArr($BriefingID);
		if(count($rs)){
			return $rs[0];
		}
		return array();
	}
	function getBriefingSessionArr($BriefingID){
		$briefingIdSql = implode("','", (array)$BriefingID);
		
		$sql = "SELECT 
			* 
		FROM
			ADMISSION_BRIEFING_SETTING
		WHERE
			BriefingID IN ('{$briefingIdSql}')
		";
		$rs = $this->returnResultSet($sql);
		return $rs;
	}
	
	/**
	 * Get all used quota (not include deleted applicant)
	 * @return array Used quota
	 */
	function getAllBriefingSessionUsedQuota($schoolYearID = ''){
		if($schoolYearID){
			$schoolYearIdFilter = " AND ABS.SchoolYearID = '{$schoolYearID}'";
		}
		
		$sql = "SELECT 
			ABAI.BriefingID,
			SUM(ABAI.SeatRequest) AS TOTAL_USED_QUOTA
		FROM
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		WHERE
			1=1
			{$schoolYearIdFilter}
		AND
			ABAI.DateDeleted IS NULL
		GROUP BY
			BriefingID
		";
		$rs = $this->returnResultSet($sql);
		
		$usedQuotas = array();
		foreach($rs as $r){
			$usedQuotas[ $r['BriefingID'] ] = $r['TOTAL_USED_QUOTA'];
		}
		
		return $usedQuotas;
	}
	
	/**
	 * Get used quota (not include deleted applicant)
	 * @return int Used quota
	 */
	function getBriefingSessionUsedQuota($BriefingID){
		$sql = "SELECT 
			SUM(SeatRequest) 
		FROM
			ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
			BriefingID = '{$BriefingID}'
		AND
			DateDeleted IS NULL
		";
		$rs = $this->returnVector($sql);
		$usedQuota = $rs[0];

		return $usedQuota;
	}
	
	
	/**
	 * Get All remainning quota (not include deleted applicant)
	 * @return array Remains quotas
	 */
	function getAllBriefingSessionRemainsQuota($schoolYearID){
		$sessionSetting = $this->getAllBriefingSession($schoolYearID);
		$usedQuota = $this->getAllBriefingSessionUsedQuota($schoolYearID);
		
		$remainsQuotaArr = array();
		foreach($sessionSetting as $session){
			$BriefingID = $session['BriefingID'];
			$remainsQuotaArr[ $BriefingID ] = $session['TotalQuota'] - $usedQuota[ $BriefingID ];
		}
		return $remainsQuotaArr;
	}
	
	/**
	 * Get remainning quota (not include deleted applicant)
	 * @return int Remains quota
	 */
	function getBriefingSessionRemainsQuota($BriefingID){
		$sessionSetting = $this->getBriefingSession($BriefingID);
		$usedQuota = $this->getBriefingSessionUsedQuota($BriefingID);
		
		$remainsQuota = $sessionSetting['TotalQuota'] - $usedQuota;
		return $remainsQuota;
	}
	
	function getAllBriefingSessionApplicantCount($schoolYearID = ''){
		if($schoolYearID){
			$schoolYearIdFilter = " AND ABS.SchoolYearID = '{$schoolYearID}'";
		}
		
		$sql = "SELECT 
			ABAI.BriefingID,
			COUNT(*) AS COUNT_APPLICANT
		FROM
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		WHERE
			1=1
			{$schoolYearIdFilter}
		AND
			ABAI.DateDeleted IS NULL
		GROUP BY
			ABAI.BriefingID";
		$rs = $this->returnResultSet($sql);
		
		$countArr = array();
		foreach($rs as $r){
			$countArr[ $r['BriefingID'] ] = $r['COUNT_APPLICANT'];
		}
		
		return $countArr;
	}
	
	function getBriefingSessionApplicantCount($BriefingID, $keyword='', $schoolYearID='', $filter=''){
		#### Filter Deleted START ####
		$notDeletedFilter = '';
		if(isset($filter['filterDeleted'])){
    		if($filter['filterDeleted']){
    			$notDeletedFilter = ' AND ABAI.DateDeleted IS NOT NULL';
    		}else{
    			$notDeletedFilter = ' AND ABAI.DateDeleted IS NULL';
    		}
		}
		#### Filter Deleted END ####
		
		#### Filter Status START ####
		$statusFilter = '';
		if(isset($filter['status']) && $filter['status']){
		    $statusFilter = " AND ABAI.Status = '{$filter['status']}'";
		}
		#### Filter Status END ####
		
		#### Filter Keyword START ####
		$keywordFilter = '';
		if($keyword != ''){
			$keyword = addslashes($keyword);
			$keywordFilter = " AND (
				ABAI.Email LIKE '%$keyword%'
			OR
				ABAI.Email_BAK LIKE '%$keyword%'
			OR
				ABAI.ApplicantID LIKE '%$keyword%'
			OR
				ABAI.BirthCertNo LIKE '%$keyword%'
			OR
				ABAI.ParentName LIKE '%$keyword%'
			OR
				ABAI.PhoneNo LIKE '%$keyword%'
			OR
				ABAI.StudentName LIKE '%$keyword%'
			OR
				ABAI.Kindergarten LIKE '%$keyword%'
			)";
		}
		#### Filter Keyword END ####
		
		if($BriefingID > 0){
			$briefingSessionCond = " AND ABS.BriefingID = '".$BriefingID."' ";
		}
		
		if($schoolYearID > 0){
			$schoolYearCond = " AND ABS.SchoolYearID = '".$schoolYearID."' ";
		}
		
		$sql = "SELECT 
			COUNT(*) AS COUNT_APPLICANT
		FROM
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
			$briefingSessionCond
		WHERE
			1=1
			{$keywordFilter}
			{$notDeletedFilter}
			{$statusFilter}
			{$schoolYearCond}";
		$rs = $this->returnVector($sql);
		
		return $rs[0];
	}
	
	
	function deleteBriefingSession($BriefingID){
		global $UserID;
		$BriefingID = str_replace(',', "','", $BriefingID); 
		
		$sql = "UPDATE 
			ADMISSION_BRIEFING_SETTING
		SET
			DateDeleted=NOW(),
			DeletedBy='{$UserID}'
		WHERE
			BriefingID IN ('{$BriefingID}')";
		$result = $this->db_db_query($sql);
		
		return $result;
	}
######################## Briefing Session END ########################
	
	
######################## Briefing Applicant START ########################
	function checkBriefingDuplicate($schoolYearID, $Data){
		$sql = "SELECT * FROM ADMISSION_BRIEFING_APPLICANT_INFO WHERE BriefingID='{$Data['BriefingID']}' AND Email='{$Data['Email']}'";
		if($this->returnVector($sql)){
			return true;
		}
		return false;
	}
	function insertBriefingApplicant($schoolYearID, $Data){
		global $sys_custom;
   		extract($Data);
   		
   		#### Get ApplicantID START ####
   		$ApplicantID = $this->generateApplicantID($schoolYearID);
   		#### Get ApplicantID END ####
		
		if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){
			$sql = "Select * 
					From 
						ADMISSION_BRIEFING_APPLICANT_INFO i 
					JOIN 
						ADMISSION_BRIEFING_SETTING s 
					ON 
						i.BriefingID = s.BriefingID 
					where 
						s.SchoolYearID = '".$schoolYearID."' AND i.Email = '".$Email."'";
			if($this->returnVector($sql)){
				return 0;
			}
		}

		#### Check duplicate entry START ####
		if($this->checkBriefingDuplicate($schoolYearID, $Data)){
		    return 0;
		}
		#### Check duplicate entry END ####
		
   		#### Insert Record START ####
		$HTTP_USER_AGENT = addslashes($_SERVER['HTTP_USER_AGENT']);
		$REMOTE_ADDR = addslashes($_SERVER['REMOTE_ADDR']);
		$HTTP_X_FORWARDED_FOR = addslashes($_SERVER['HTTP_X_FORWARDED_FOR']);
		$sql = "INSERT INTO
			ADMISSION_BRIEFING_APPLICANT_INFO
		(
			BriefingID,
			Email,
			ApplicantID,
			BirthCertNo,
			ParentName,
			StudentName,
			StudentGender,
			Kindergarten,
			PhoneNo,
			SeatRequest,
			DateInput,
			HTTP_USER_AGENT,
			REMOTE_ADDR,
			HTTP_X_FORWARDED_FOR
		) VALUES (
			'{$BriefingID}',
			'{$Email}',
			'{$ApplicantID}',
			'{$BirthCertNo}',
			'{$ParentName}',
			'{$StudentName}',
			'{$StudentGender}',
			'{$Kindergarten}',
			'{$PhoneNo}',
			'{$SeatRequest}',
			now(),
			'{$HTTP_USER_AGENT}',
			'{$REMOTE_ADDR}',
			'{$HTTP_X_FORWARDED_FOR}'
		)";
		
		$result = $this->db_db_query($sql);
   		#### Insert Record END ####
		
   		#### Update Delete Pass Key START ####
		$Briefing_ApplicantID = $this->db_insert_id();
		
   		$sql = "SELECT DeleteRecordPassKey FROM ADMISSION_BRIEFING_APPLICANT_INFO";
   		$DeleteRecordPassKeyArr = $this->returnVector($sql);
   		$i = 0;
		do{
			$DeleteRecordPassKey = md5($Briefing_ApplicantID . $ApplicantID . $Email . $i++);
		}while(in_array($DeleteRecordPassKey, $DeleteRecordPassKeyArr));
		
		$sql = "UPDATE 
			ADMISSION_BRIEFING_APPLICANT_INFO 
		SET 
			DeleteRecordPassKey='{$DeleteRecordPassKey}'
		WHERE
			Briefing_ApplicantID='{$Briefing_ApplicantID}'";
		$result = $result && $this->db_db_query($sql);
   		#### Update Delete Pass Key END ####
		
		
		#### Delete record if insert not success START #### 
		if(!$result){
			$this->deleteBriefingApplicant($Briefing_ApplicantID, true);
			return 0;
		}
		#### Delete record if insert not success END #### 
		
		return $Briefing_ApplicantID;
	}
	
	function getAllBriefingApplicant($BriefingID, $includeDeleted = false){
		$BriefingIdSql = implode("','", (array)$BriefingID);
		
		$notDeletedFilter = '';
		if(!$includeDeleted){
			$notDeletedFilter = ' AND DateDeleted IS NULL';
		}
		
		$sql = "SELECT 
			* 
		FROM
			ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
			BriefingID IN ('{$BriefingIdSql}')
			{$notDeletedFilter}
		";
		$rs = $this->returnResultSet($sql);
		
		return $rs;
	}

	function getStatusSelection($status='',$name="selectStatus",$auto_submit=true,$isAll=true, $isMultiSelect=false){
	    global $admission_cfg,$kis_lang;
	    $x = '<select name="'.$name.'" id="'.$name.'" '.($auto_submit?'class="auto_submit"':'').' '.($isMultiSelect?'multiple':'').'>';
	    $x .= ($isAll)?'<option value=""'.($status==''?' selected="selected"':'').'>'.$kis_lang['allstatus'].'</option>':'';
	    foreach($admission_cfg['BriefingStatus'] as $_key => $_status){
	        $x .= '<option value="'.$_status.'"'.($status==$_status?' selected="selected"':'').'>';
	        $x .= $kis_lang['Admission']['Status'][$_key];
	        $x .= '</option>';
	    }
	    $x .= '</select>';
	    return $x;
	}

	function updateBriefingApplicationStatusByIds($briefingApplicationIds,$status){
	    $sql = "UPDATE
			ADMISSION_BRIEFING_APPLICANT_INFO
		SET
  			Status = '{$status}'
 		WHERE
			Briefing_ApplicantID IN ({$briefingApplicationIds})
    	";
	    return $this->db_db_query($sql);
	}

	function updateBriefingApplicationSessionByIds($briefingApplicationIds,$BriefingID){
	    $sql = "UPDATE
			ADMISSION_BRIEFING_APPLICANT_INFO
		SET
  			BriefingID = '{$BriefingID}'
 		WHERE
			Briefing_ApplicantID IN ({$briefingApplicationIds})
    	";
	    return $this->db_db_query($sql);
	}
	
	function briefingApplicantLottery($schoolYearID, $data = ''){
	    throw new Exception('Not implemented');
	}
	
	/**
	 * Get all Applicant using paging, searching and ordering.
	 * @param $filter array(
	 * 		'keyword' => ...,
	 * 		'page' => ...,
	 * 		'amount' => ...,
	 * 		'sortby' => ...,
	 * 		'order' => ...,
	 * 		'filterDeleted' => ...,
	 * )
	 */
	function getFilterBriefingApplicant($BriefingID, $filter){
		if($BriefingID){
			$BriefingIdSql = " AND ABAI.BriefingID IN ('".implode("','", (array)$BriefingID)."') ";
		}
		
		#### Filter Deleted START ####
		$notDeletedFilter = '';
		if(isset($filter['filterDeleted'])){
    		if($filter['filterDeleted']){
    			$notDeletedFilter = ' AND ABAI.DateDeleted IS NOT NULL';
    		}else{
    			$notDeletedFilter = ' AND ABAI.DateDeleted IS NULL';
    		}
		}
		#### Filter Deleted END ####
		
		#### Filter Status START ####
		$statusFilter = '';
		if(isset($filter['status']) && $filter['status']){
		    $statusFilter = " AND ABAI.Status = '{$filter['status']}'";
		}
		#### Filter Status END ####
		
		#### Filter Keyword START ####
		$keywordFilter = '';
		if($filter['keyword'] != ''){
			$keyword = addslashes($filter['keyword']);
			$keywordFilter = " AND (
				ABAI.ApplicantID LIKE '%$keyword%'
			OR
				ABAI.Email LIKE '%$keyword%'
			OR
				ABAI.Email_BAK LIKE '%$keyword%'
			OR
				ABAI.ApplicantID LIKE '%$keyword%'
			OR
				ABAI.BirthCertNo LIKE '%$keyword%'
			OR
				ABAI.ParentName LIKE '%$keyword%'
			OR
				ABAI.PhoneNo LIKE '%$keyword%'
			OR
				ABAI.StudentName LIKE '%$keyword%'
			OR
				ABAI.Kindergarten LIKE '%$keyword%'
			)";
		}
		#### Filter Keyword END ####
		
		#### Paging START ####
		$pagingSQL = '';
		if($filter['amount'] != ''){
			$page = $filter['page'];
			$amount = $filter['amount'];
			
			if($page>0){
				$page = $page - 1; // Page number is start from 1
			}
			$start = $page * $amount;
			$pagingSQL = " LIMIT {$start}, {$amount}";
		}
		#### Paging END ####
		
		if($filter['schoolYearID'] > 0){
			$schoolYearFilter = " AND ABS.SchoolYearID = '".$filter['schoolYearID']."' ";
		}
		
		#### Ordering START ####
		$orderSQL = ' ORDER BY ABAI.ApplicantID';
		#### Ordering END ####
		
		$sql = "SELECT 
			*, 
			DATE_FORMAT(BriefingStartDate,'%Y-%m-%d') as BriefingDate, 
			CONCAT(DATE_FORMAT(BriefingStartDate,'%H:%i'), ' ~ ', DATE_FORMAT(BriefingEndDate,'%H:%i')) as BriefingTimeSlot
		FROM
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		WHERE 1
			{$BriefingIdSql}
			{$notDeletedFilter}
			{$statusFilter}
			{$keywordFilter}
			{$schoolYearFilter}
			{$orderSQL}
			{$pagingSQL}
		";
		$rs = $this->returnResultSet($sql);
		
		return $rs;
	}

	function getBriefingApplicant($Briefing_ApplicantID){
		$sql = "SELECT
		*
		FROM
		ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
		Briefing_ApplicantID = '{$Briefing_ApplicantID}'
		";
		$rs = $this->returnResultSet($sql);
	
		if(count($rs)){
			return $rs[0];
		}
		return array();
	}

	function getBriefingApplicantByApplicantID($ApplicantID){
		$sql = "SELECT
		    *
		FROM
		    ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
		    ApplicantID = '{$ApplicantID}'
		";
		$rs = $this->returnResultSet($sql);
	
		if(count($rs)){
			return $rs[0];
		}
		return array();
	}
	
	function getBriefingApplicantByDeleteRecordPassKey($DeleteRecordPassKey){
		$sql = "SELECT 
			* 
		FROM
			ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
			DeleteRecordPassKey = '{$DeleteRecordPassKey}'
		";
		$rs = $this->returnResultSet($sql);
		
		if(count($rs)){
			return $rs[0];
		}
		return array();
	}
	
	function deleteBriefingApplicant($Briefing_ApplicantID, $hardDelete = false){
		global $sys_custom;
		if($hardDelete){
			$sql = "DELETE FROM 
				ADMISSION_BRIEFING_APPLICANT_INFO
			WHERE
				Briefing_ApplicantID = '{$Briefing_ApplicantID}'";
		}else{
			$sql = "UPDATE
				ADMISSION_BRIEFING_APPLICANT_INFO
			SET
				Email_BAK = Email,
				Email = NULL,
				DateDeleted = NOW()
			WHERE
				Briefing_ApplicantID = '{$Briefing_ApplicantID}'
			AND
				DateDeleted IS NULL";
				
			if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){
				$sql = "UPDATE
					ADMISSION_BRIEFING_APPLICANT_INFO
				SET
					Email_BAK = Email,
					DateDeleted = NOW()
				WHERE
					Briefing_ApplicantID = '{$Briefing_ApplicantID}'
				AND
					DateDeleted IS NULL";
			}
		}
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	/**
	 * Generate a new ApplicantID.
	 * Format: NextYear(2-digit) concatenate with 4-digit integer (increment) 
	 */
	function generateApplicantID($schoolYearID){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$schoolYearID)), -2);
		
		$sql = "SELECT 
			MAX(ABAI.ApplicantID) 
		FROM 
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		AND
		(
			ABS.SchoolYearID = '{$schoolYearID}'
		OR
			ABAI.ApplicantID LIKE '{$yearStart}%'
		)";
		$rs = $this->returnVector($sql);
		
		if($rs[0] == null){
			return "{$yearStart}0001";
		}
		return ((int)$rs[0]) + 1;
	}
	
	
	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = '')
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $Lang, $sys_custom;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
			$from = "no-reply <no-reply@".$_SERVER['SERVER_NAME'].">";
		}
		else{
			$from = $libwebmail->GetWebmasterMailAddress();
		}
		
		$result = array();
		
		######## Get Applicant Info START ########
		$applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
		$Email = ($email)?$email:$applicant['Email'];
		$ApplicantID = $applicant['ApplicantID'];
		$DeleteRecordPassKey = $applicant['DeleteRecordPassKey'];
		######## Get Applicant Info END ########

		######## Setup Email Content START ########
		if($subject == ''){
			$email_subject = "簡介會申請通知 Briefing Session Application Notification";
		}
		else{
			$email_subject = $subject;
		}
		
		if($message == ''){
			if($Briefing_ApplicantID == 0){
				return true;
				/* Fail case no email * /
				$email_message = <<<EMAIL
					{$Lang['Admission']['msg']['applicationnotcomplete']} 
					{$Lang['Admission']['msg']['tryagain']}
					<br /><br />
					Application is Not Completed. Please try to apply again!
EMAIL;
				/* */
			}else{
				$deleteRecordLink = $this->getApplicantDeleteLink($Briefing_ApplicantID, $DeleteRecordPassKey);
				$email_message = <<<EMAIL
					<font color="green">
						報名已遞交，申請編號為
					</font>
					<font size="5">
						<u>{$ApplicantID}</u>
					</font>
					
					<br />
					{$Lang['Admission']['briefingDeleteRecordHint']}
					<br />
					{$deleteRecordLink}
					<br /><br />
					<font color="green">
						Application is Completed.
						Your application number is 
					</font>
					<font size="5">
						<u>{$ApplicantID}</u>
					</font>
					<br />
					For further details or cancel your application, click this link:
					<br />
					{$deleteRecordLink}
EMAIL;
			}
		}else{
			$email_message = $message;
		}
		######## Setup Email Content END ########
		
		$sent_ok = true;
		if($Email != '' && intranet_validateEmail($Email)){
			$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($Email),array(),array(),"",$IsImportant="",$mail_return_path=($sys_custom['KIS_Admission']['STANDARD']['Settings']?"no-reply@".$_SERVER['SERVER_NAME']:get_webmaster()),$reply_address="",$isMulti=null,$nl2br=0);
		}else{
			$sent_ok = false;
		}
			
		return $sent_ok;
	}
	
	function getApplicantDeleteLink($Briefing_ApplicantID, $DeleteRecordPassKey){
		/* No more long url * /
		global $admission_cfg;
		$id = urlencode(getEncryptedText('Briefing_ApplicantID='.$Briefing_ApplicantID.'&DeleteRecordPassKey='.$DeleteRecordPassKey,$admission_cfg['FilePathKey']));

		return 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_briefing/deleteApplicant.php?id='.$id;
		/* */
		
		
		return 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_briefing/deleteApplicant.php?key='.urlencode($DeleteRecordPassKey);
	}
	
	function getPDFContent($Briefing_ApplicantID){
		global $PATH_WRT_ROOT,$admission_cfg, $setting_path_ip_rel, $Lang, $LangB5, $LangEn;

		
	    ######## Get briefing info START ########
	    $briefingApplicant = $this->getBriefingApplicant($Briefing_ApplicantID);
	    $briefingSession = $this->getBriefingSession($briefingApplicant['BriefingID']);
	    $allBriefingSession = $this->getAllBriefingSession($briefingSession['SchoolYearID']);
	    ######## Get briefing info END ########
	    
	    ######## Init PDF START ########
	    $templateHeaderPath = $PATH_WRT_ROOT.'file/customization/'.$setting_path_ip_rel.'/briefing/header.php';
	    require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	    $margin_top = '7';
	    $mpdf = new mPDF('','A4',0,'',5,5,10,5);
	    $mpdf->mirrorMargins = 1;
	    ######## Init PDF END ########
	    
	    ######## Load header to PDF START ########
	    ob_start();
	    include($templateHeaderPath);
	    $pageHeader = ob_get_clean();
	    
	    $mpdf->WriteHTML($pageHeader);
	    ######## Load header to PDF END ########
	    
	    
	    ######## Load data to PDF START ########
        #### Load Template START ####
        ob_start();
        $templatePath = $PATH_WRT_ROOT.'file/customization/'.$setting_path_ip_rel.'/briefing/briefing_result.php';
        include($templatePath);
        $page1 = ob_get_clean();
        #### Load Template END ####
    
        $mpdf->WriteHTML($page1);
	    ######## Load data to PDF END ########
	    
	    $mpdf->Output();
	}
######################## Briefing Applicant END ########################

} // End Class

if(file_exists($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/admission_briefing.php")){
	include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/admission_briefing.php");
}
?>