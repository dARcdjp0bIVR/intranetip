<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
html,body {
  text-size-adjust: none;
  -webkit-text-size-adjust: none;
  -moz-text-size-adjust: none;
  -ms-text-size-adjust: none;
}
</style>
<script type="text/javascript">

kis.datepicker('#StudentDateOfBirth');	

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	var english = /^[A-Za-z0-9]*$/;
	var re = /^\S+@\S+\.\S+$/;
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	var today = '<?=date('Y-m-d')?>';
	
//	if(form1.StudentPersonalPhoto.files[0]){
//	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
//	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
//	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	if(form1.studentssurname_en.value==''){
		return false;
	} else if(!english.test(form1.studentssurname_en.value.replace(/\s+/g, ''))){
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		return false;
	} else if(!english.test(form1.studentsfirstname_en.value.replace(/\s+/g, ''))){
		return false;
	} else if(form1.studentssurname_b5.value==''){
		return false;
	} else if(form1.studentsfirstname_b5.value==''){
		return false;
	} else if($('input:radio[name=StudentGender]:checked').val() == null){
		return false;
	} else if($('input:radio[name=OthersCurBSName]:checked').val() == null){
		return false;
	}else if($('input:radio[name=IsTwins]:checked').val() == null){
		return false;
	} else if($('input:radio[name=IsTwins]:checked').val() == 'Y' && $('input:radio[name=IsTwinsApplied]:checked').val() == null){
		return false;
//	} else if($('input:radio[name=IsTwinsApplied]:checked').val() == 'Y' && form1.TwinsApplicationID.value==''){
//		return false;
	} else if($('input:radio[name=IsTwinsApplied]:checked').val() == 'Y' && form1.TwinsApplicationID.value!='' && checkApplicationNo() <= 0){
		return false;
	//} else if(form1.StudentDateOfBirth.value==''  || studentDateOfBirth >= today){
	} else if(!check_date_without_return_msg(form1.StudentDateOfBirth)){
		return false;
	} <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>
	else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		return false;
	} <?}?>
//	else if(form1.StudentPlaceOfBirth.value==''){
//		return false;
//	} 
	else if(form1.StudentBirthCertNo.value==''){
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.StudentBirthCertNo.value)){
		return false;
	} else if(checkBirthCertNo() > 0){
		return false;
//	} else if(form1.StudentHomePhoneNo.value==''){
//		return false;
	} else if($('input:radio[name=StudentHomeAddress]:checked').val() == null){
		return false;
	} else if($('input:radio[name=StudentHomeAddress]:checked').val() == '<?=count($Lang['Admission']['csm']['AddressLocation'])?>' && form1.StudentHomeAddressOthers.value==''){
		return false;
//	} else if(form1.StudentPersonalPhoto.value==''){
//		return false;
//	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
//		return false;
//	} else if(studentPhotoSize > maxFileSize){
//		return false;
	} else if(!form1.OthersApplyDayType1){
		return false;
	} else if(form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value == ''){
		return false;
	} else if(form1.OthersApplyDayType2 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value ==""){
		//else if(form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value){
		return false;
	} else if($('input:radio[name=StudentRelationship]:checked').val() == null){
		return false;
	}
	else if($('input:radio[name=StudentRelationship]:checked').val() == 'F'){
		if(form1.G1EnglishName.value==''){
			return false;
		}
		else if(!english.test(form1.G1EnglishName.value.replace(/\s+/g, ''))){
			return false;
		}
		else if(form1.G1ChineseName.value==''){
			return false;
		}
		else if($('input:checkbox[name=G1LiveWithChild]:checked').val() == null){
			return false;
		}
		else if($('input:checkbox[name=G1LiveWithChild]:checked').val() != 'Y' && $('input:checkbox[name=G2LiveWithChild]:checked').val() != 'N' && $('#StudentRelationship3').is(':disabled')){
			return false;
		}
//		else if(form1.G1Email.value==''){
//			return false;
//		} else if(!re.test(form1.G1Email.value)){
//			return false;
//		}
		else if(form1.G1Mobile.value==''){
			return false;
		}		
	}
	else if($('input:radio[name=StudentRelationship]:checked').val() == 'M'){
		if(form1.G2EnglishName.value==''){
			return false;
		}
		else if(!english.test(form1.G2EnglishName.value.replace(/\s+/g, ''))){
			return false;
		}
		else if(form1.G2ChineseName.value==''){
			return false;
		}
		else if($('input:checkbox[name=G2LiveWithChild]:checked').val() == null){
			return false;
		} else if($('input:checkbox[name=G2LiveWithChild]:checked').val() != 'Y' && $('input:checkbox[name=G1LiveWithChild]:checked').val() != 'N' && $('#StudentRelationship3').is(':disabled')){
			return false;
//		}
//		else if(form1.G2Email.value==''){
//			return false;
//		} else if(!re.test(form1.G2Email.value)){
//			return false;
		} else if(form1.G2Mobile.value==''){
			return false;
		}
	}
	if($('input:radio[name=StudentRelationship]:checked').val() == 'G' || ($('input:checkbox[name=G1LiveWithChild]:checked').val() == 'N' && $('input:checkbox[name=G2LiveWithChild]:checked').val() == 'N')){
		if(form1.G3Relationship.value==''){
			return false;
		}
		else if(form1.G3EnglishName.value==''){
			return false;
		}
		else if(!english.test(form1.G3EnglishName.value.replace(/\s+/g, ''))){
			return false;
		}
		else if(form1.G3ChineseName.value==''){
			return false;
		}
//		else if($('input:checkbox[name=G3LiveWithChild]:checked').val() == null){
//			return false;
//		}
//		else if(form1.G3Email.value==''){
//			return false;
//		} else if(!re.test(form1.G3Email.value)){
//			return false;
//		}
		else if(form1.G3Mobile.value==''){
			return false;
		}
	}
	
	if($('input:radio[name=StudentContactPerson]:checked').val() == null){
		return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == 'F' && form1.G1Mobile.value==''){
		return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == 'M' && form1.G2Mobile.value==''){
		return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == 'G' && form1.G3Mobile.value==''){
		return false;
	}
	
	if(form1.G1Email.value==''){
		return false;
	} else if(!re.test(form1.G1Email.value)){
		return false;
	}
//	else if($('input:radio[name=IsSingleParent]:checked').val() == null){
//		return false;
//	} else if($('input:radio[name=IsSingleParent]:checked').val() == 'Y' && $('input:radio[name=IsApplyFullDayCare]:checked').val() == null){
//		return false;
//	}
//	if($('input:radio[name=IsFamilySpecialCase]:checked').val() == null){
//		return false;
//	} else if($('input:radio[name=IsFamilySpecialCase]:checked').val() == 'Y' && $('input:radio[name=IsApplyFullDayCare]:checked').val() == null){
//		return false;
//	}
	//else {
		return true;
	//}

}
/*
function check_docs_upload2(form1) {

	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		return false;
	} else if(otherFileSize > maxFileSize){
		return false;
	} else if(form1.OtherFile1.value==''){
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		return false;
	} else if(otherFileSize1 > maxFileSize){
		return false;
	} else  {
		return true;
	}
}
*/
function check_docs_upload2(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		
		if(!isOldBrowser){
			var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			return false;
		}
	}
	
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		//alert($("form").serialize());
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(isValid){
		document.body.scrollTop = 0; // For Safari
  		document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
//			   clearTimeout(timer);
//			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
		   
		    /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_class_selection.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#DayTypeOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
		   
	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			//var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			//values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		           	//Henry Added Start[20150120]
					for(var i=0;i<file_count;i++)
					{
						var file_element = file_ary.get(i);
						readFileURL(file_element);
					}
					//Henry Added End[20150120]
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
			
	}
}

function submitForm(){
	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	return confirm('<?=$Lang['Admission']['csm']['suresubmit']?>');
}

function check_choose_class(form1) {

	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>");
				return false;
			}
		}
	<?}?>
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	//For debugging only
	//return true;
	var english = /^[A-Za-z0-9]*$/;
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	var re = /^\S+@\S+\.\S+$/;
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
//	if(form1.StudentPersonalPhoto.files[0]){
//	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
//	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
//	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	//var hasPG = 1;
	
	
	if(form1.studentssurname_en.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentssurname_en']?>");	
		form1.studentssurname_en.focus();
		return false;
	} else if(!english.test(form1.studentssurname_en.value.replace(/\s+/g, ''))){
		alert("<?=$Lang['Admission']['csm']['msg']['ChildEnglishNameFormat']?>");	
		form1.studentssurname_en.focus();
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentsfirstname_en']?>");
		form1.studentsfirstname_en.focus();
		return false;
	} else if(!english.test(form1.studentsfirstname_en.value.replace(/\s+/g, ''))){
		alert("<?=$Lang['Admission']['csm']['msg']['ChildEnglishNameFormat']?>");	
		form1.studentsfirstname_en.focus();
		return false;
	} else if(form1.studentssurname_b5.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentssurname_b5']?>");	
		form1.studentssurname_b5.focus();
		return false;
	} else if(form1.studentsfirstname_b5.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentsfirstname_b5']?>");
		form1.studentsfirstname_b5.focus();
		return false;
	} else if($('input:radio[name=StudentGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>");	
		form1.StudentGender[0].focus();
		return false;
	} else if($('input:radio[name=OthersCurBSName]:checked').val() == null){
		alert("<?=$Lang['Admission']['csm']['msg']['CurBSName']?>");	
		form1.OthersCurBSName[0].focus();
		return false;
	}else if($('input:radio[name=IsTwins]:checked').val() == null){
		alert("<?=$Lang['Admission']['csm']['msg']['isTwins']?>");	
		form1.IsTwins[0].focus();
		return false;
	} else if($('input:radio[name=IsTwins]:checked').val() == 'Y' && $('input:radio[name=IsTwinsApplied]:checked').val() == null){
		alert("<?=$Lang['Admission']['csm']['msg']['isTwinsApplied']?>");	
		form1.IsTwinsApplied[0].focus();
		return false;
//	} else if($('input:radio[name=IsTwinsApplied]:checked').val() == 'Y' && form1.TwinsApplicationID.value==''){
//		alert("<?=$Lang['Admission']['csm']['msg']['inputTwinsApplicationNo']?>");	
//		form1.TwinsApplicationID.focus();
//		return false;
	} else if($('input:radio[name=IsTwinsApplied]:checked').val() == 'Y' && form1.TwinsApplicationID.value!='' && checkApplicationNo() <= 0){
		alert("<?=$Lang['Admission']['csm']['msg']['invalidTwinsApplicationNo']?>");	
		form1.TwinsApplicationID.focus();
		return false;
	//} else if(form1.StudentDateOfBirth.value==''  || studentDateOfBirth >= today){
	} else if(!check_date_without_return_msg(form1.StudentDateOfBirth)){
		if(form1.StudentDateOfBirth.value!=''){
			alert("<?=$Lang['Admission']['mgf']['msg']['importInvalidDateOfBirth']?>");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>
	else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>");
		form1.StudentDateOfBirth.focus();
		return false;
	} <?}?>
//	else if(form1.StudentPlaceOfBirth.value==''){
//		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>");	
//		form1.StudentPlaceOfBirth.focus();
//		return false;
//	} 
	else if(form1.StudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['enterbirthcertificatenumber']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.StudentBirthCertNo.value)){
		alert("<?=$Lang['Admission']['csm']['msg']['invalidbirthcertificatenumber']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['csm']['msg']['duplicatebirthcertificatenumber']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
//	} else if(form1.StudentHomePhoneNo.value==''){
//		alert("<?=$Lang['Admission']['csm']['msg']['enterstudenthomephoneno']?>");	
//		form1.StudentHomePhoneNo.focus();
//		return false;
//	} else if(form1.StudentHomePhoneNo.value.length != 8 || isNaN(form1.StudentHomePhoneNo.value)){
//		alert("<?=$Lang['Admission']['csm']['invalidhomephoneno']?>");	 
//		form1.StudentHomePhoneNo.focus();
//		return false;
	
	} else if($('input:radio[name=StudentHomeAddress]:checked').val() == null){
		alert("<?=$Lang['Admission']['csm']['msg']['enterhomeaddress']?>");	
		form1.StudentHomeAddress[0].focus();
		return false;
	} else if($('input:radio[name=StudentHomeAddress]:checked').val() == '<?=count($Lang['Admission']['csm']['AddressLocation'])?>' && form1.StudentHomeAddressOthers.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['enterhomeaddress']?>");	
		form1.StudentHomeAddressOthers.focus();
		return false;
//	} else if(form1.StudentPersonalPhoto.value==''){
//		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>");	
//		form1.StudentPersonalPhoto.focus();
//		return false;
//	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
//		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
//		form1.StudentPersonalPhoto.focus();
//		return false;
//	} else if(studentPhotoSize > maxFileSize){
//		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
//		form1.StudentPersonalPhoto.focus();
//		return false;
	} else if(!form1.OthersApplyDayType1){
		alert("<?=$Lang['Admission']['msg']['applyDayTypeHints']?>");
		return false;
	} else if(form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value == ''){
		alert("<?=$Lang['Admission']['csm']['msg']['applyDayTypeHints']?>");
		form1.OthersApplyDayType1.focus();
		return false;
	} else if(form1.OthersApplyDayType2 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value ==""){
		//else if(form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value){
		alert("<?=$Lang['Admission']['csm']['msg']['applyDayTypeHints']?>");
		form1.OthersApplyDayType1.focus();
		return false;
	<?if($_SESSION["platform"]=="KIS" && $_SESSION["UserID"]){?>
	} else if(form1.CustApplicationID.value !="" && checkApplicationNo2() > 0){
		alert("<?=$Lang['Admission']['csm']['msg']['ApplicationNo']?>");	
		form1.CustApplicationID.focus();
		return false;
	<?}?>
	} else if($('input:radio[name=StudentRelationship]:checked').val() == null){
		alert("<?=$Lang['Admission']['csm']['msg']['StudentRelationship']?>");	
		form1.StudentRelationship[0].focus();
		return false;
	}
	else if($('input:radio[name=StudentRelationship]:checked').val() == 'F'){
		if(form1.G1EnglishName.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishName']?>");	
			form1.G1EnglishName.focus();
			return false;
		}
		else if(!english.test(form1.G1EnglishName.value.replace(/\s+/g, ''))){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishNameFormat']?>");	
			form1.G1EnglishName.focus();
			return false;
		}
		else if(form1.G1ChineseName.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1ChineseName']?>");	
			form1.G1ChineseName.focus();
			return false;
		}
		else if($('input:checkbox[name=G1LiveWithChild]:checked').val() == null){
			alert("<?=$Lang['Admission']['csm']['msg']['parentlivewithchild']?>");	
			form1.G1LiveWithChild[0].focus();
			return false;
		}
		else if($('input:checkbox[name=G1LiveWithChild]:checked').val() != 'Y' && $('input:checkbox[name=G2LiveWithChild]:checked').val() != 'N' && $('#StudentRelationship3').is(':disabled')){
			alert("<?=$Lang['Admission']['csm']['msg']['applicantmustlivewithchild']?>");	
			form1.G1LiveWithChild[0].focus();
			return false;
		}
//		else if(form1.G1Email.value==''){
//			alert("<?=$Lang['Admission']['csm']['msg']['G1Email']?>");	
//			form1.G1Email.focus();
//			return false;
//		} else if(!re.test(form1.G1Email.value)){
//			alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");	
//			form1.G1Email.focus();
//			return false;
//		}
		else if(form1.G1Mobile.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile']?>");	
			form1.G1Mobile.focus();
			return false;
		}
		else if(form1.G1Mobile.value.length != 8 || isNaN(form1.G1Mobile.value)){
			alert("<?=$Lang['Admission']['csm']['invalidmobile']?>");	 
			form1.G1Mobile.focus();
			return false;	
		}	
	}
	else if($('input:radio[name=StudentRelationship]:checked').val() == 'M'){
		if(form1.G2EnglishName.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishName']?>");	
			form1.G2EnglishName.focus();
			return false;
		}
		else if(!english.test(form1.G2EnglishName.value.replace(/\s+/g, ''))){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishNameFormat']?>");	
			form1.G2EnglishName.focus();
			return false;
		}
		else if(form1.G2ChineseName.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1ChineseName']?>");	
			form1.G2ChineseName.focus();
			return false;
		}
		else if($('input:checkbox[name=G2LiveWithChild]:checked').val() == null){
			alert("<?=$Lang['Admission']['csm']['msg']['parentlivewithchild']?>");	
			form1.G2LiveWithChild[0].focus();
			return false;
		} else if($('input:checkbox[name=G2LiveWithChild]:checked').val() != 'Y' && $('input:checkbox[name=G1LiveWithChild]:checked').val() != 'N' && $('#StudentRelationship3').is(':disabled')){
			alert("<?=$Lang['Admission']['csm']['msg']['applicantmustlivewithchild']?>");	
			form1.G2LiveWithChild[0].focus();
			return false;
//		}
//		else if(form1.G2Email.value==''){
//			alert("<?=$Lang['Admission']['csm']['msg']['G1Email']?>");	
//			form1.G2Email.focus();
//			return false;
//		} else if(!re.test(form1.G2Email.value)){
//			alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");	
//			form1.G2Email.focus();
//			return false;
		} else if(form1.G2Mobile.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile']?>");	
			form1.G2Mobile.focus();
			return false;
		}
		else if(form1.G2Mobile.value.length != 8 || isNaN(form1.G2Mobile.value)){
			alert("<?=$Lang['Admission']['csm']['invalidmobile']?>");	 
			form1.G2Mobile.focus();
			return false;	
		}	
	}
	if($('input:radio[name=StudentRelationship]:checked').val() == 'G' || ($('input:checkbox[name=G1LiveWithChild]:checked').val() != 'Y' && $('input:checkbox[name=G2LiveWithChild]:checked').val() != 'Y')){
		if(form1.G3Relationship.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['StudentRelationship2']?>");	
			form1.G3Relationship.focus();
			return false;
		}
		else if(form1.G3EnglishName.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishName']?>");	
			form1.G3EnglishName.focus();
			return false;
		}
		else if(!english.test(form1.G3EnglishName.value.replace(/\s+/g, ''))){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishNameFormat']?>");	
			form1.G3EnglishName.focus();
			return false;
		}
		else if(form1.G3ChineseName.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1ChineseName']?>");	
			form1.G3ChineseName.focus();
			return false;
		}
//		else if($('input:checkbox[name=G3LiveWithChild]:checked').val() == null){
//			alert("<?=$Lang['Admission']['csm']['msg']['parentlivewithchild']?>");	
//			form1.G3LiveWithChild[0].focus();
//			return false;
//		}
//		else if(form1.G3Email.value==''){
//			alert("<?=$Lang['Admission']['csm']['msg']['G1Email']?>");	
//			form1.G3Email.focus();
//			return false;
//		} else if(!re.test(form1.G3Email.value)){
//			alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");	
//			form1.G3Email.focus();
//			return false;
//		}
		else if(form1.G3Mobile.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile']?>");	
			form1.G3Mobile.focus();
			return false;
		}
		else if(form1.G3Mobile.value.length != 8 || isNaN(form1.G3Mobile.value)){
			alert("<?=$Lang['Admission']['csm']['invalidmobile']?>");	 
			form1.G3Mobile.focus();
			return false;	
		}	
	}
	
	if((form1.G2EnglishName.value != '' || form1.G1EnglishName.value != '') && form1.G2EnglishName.value.toUpperCase() == form1.G1EnglishName.value.toUpperCase()){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishNameConflict']?>");	
			form1.G2EnglishName.focus();
			return false;
	}
	else if((form1.G2EnglishName.value != '' || form1.G3EnglishName.value != '') && form1.G2EnglishName.value.toUpperCase() == form1.G3EnglishName.value.toUpperCase()){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishNameConflict']?>");	
			form1.G2EnglishName.focus();
			return false;
	}
	else if((form1.G1EnglishName.value != '' || form1.G3EnglishName.value != '') && form1.G1EnglishName.value.toUpperCase() == form1.G3EnglishName.value.toUpperCase()){
			alert("<?=$Lang['Admission']['csm']['msg']['G1EnglishNameConflict']?>");	
			form1.G3EnglishName.focus();
			return false;
	}
	else if((form1.G2ChineseName.value != '' || form1.G1ChineseName.value != '') && form1.G2ChineseName.value.toUpperCase() == form1.G1ChineseName.value.toUpperCase()){
			alert("<?=$Lang['Admission']['csm']['msg']['G1ChineseNameConflict']?>");	
			form1.G2ChineseName.focus();
			return false;
	}
	else if((form1.G2ChineseName.value != '' || form1.G3ChineseName.value != '') && form1.G2ChineseName.value.toUpperCase() == form1.G3ChineseName.value.toUpperCase()){
			alert("<?=$Lang['Admission']['csm']['msg']['G1ChineseNameConflict']?>");	
			form1.G2ChineseName.focus();
			return false;
	}
	else if((form1.G1ChineseName.value != '' || form1.G3ChineseName.value != '') && form1.G1ChineseName.value.toUpperCase() == form1.G3ChineseName.value.toUpperCase()){
			alert("<?=$Lang['Admission']['csm']['msg']['G1ChineseNameConflict']?>");	
			form1.G3ChineseName.focus();
			return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == null){
		alert("<?=$Lang['Admission']['csm']['msg']['inputParentHint']?>");	
		form1.StudentContactPerson[0].focus();
		return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == 'F' && form1.G1Mobile.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile']?>");	
		form1.G1Mobile.focus();
		return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == 'M' && form1.G2Mobile.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile']?>");	
		form1.G2Mobile.focus();
		return false;
	}
	else if($('input:radio[name=StudentContactPerson]:checked').val() == 'G' && form1.G3Mobile.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile']?>");	
		form1.G3Mobile.focus();
		return false;
	}
	
	if(form1.G1Email.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['G1Email']?>");	
		form1.G1Email.focus();
		return false;
	} else if(!re.test(form1.G1Email.value)){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");	
		form1.G1Email.focus();
		return false;
	}
// 	else if($('input:radio[name=IsSingleParent]:checked').val() == null){
//		alert("<?=$Lang['Admission']['csm']['msg']['singleparent']?>");	
// 		form1.IsSingleParent[0].focus();
// 		return false;
// 	} else if($('input:radio[name=IsSingleParent]:checked').val() == 'Y' && $('input:radio[name=IsApplyFullDayCare]:checked').val() == null){
//		alert("<?=$Lang['Admission']['csm']['msg']['IsApplyFullDayCare']?>");	
// 		form1.IsApplyFullDayCare[0].focus();
// 		return false;
// 	}
//	if($('input:radio[name=IsFamilySpecialCase]:checked').val() == null){
//		alert("<?=$Lang['Admission']['csm']['msg']['IsFamilySpecialCase']?>");	
//		form1.IsFamilySpecialCase[0].focus();
//		return false;
//	} else if($('input:radio[name=IsFamilySpecialCase]:checked').val() == 'Y' && $('input:radio[name=IsApplyFullDayCare]:checked').val() == null){
//		alert("<?=$Lang['Admission']['csm']['msg']['IsApplyFullDayCare']?>");	
//		form1.IsApplyFullDayCare[0].focus();
//		return false;
//	}
	//else {
		return true;
	//}
}
function check_parent_live_with_child(){
	if($('#G1LiveWithChild2').prop("checked") && $('#G2LiveWithChild2').prop("checked") || $('#G1LiveWithChild2').prop("checked") && !$('#G2LiveWithChild1').prop("checked") && !$('#G2LiveWithChild2').prop("checked") ||  $('#G2LiveWithChild2').prop("checked") && !$('#G1LiveWithChild1').prop("checked") && !$('#G1LiveWithChild2').prop("checked")){
		$('#StudentRelationship3').prop( "disabled", false );
		$('#G3Relationship').prop( "disabled", false );
		$('#G3EnglishName').prop( "disabled", false ); 
		$('#G3ChineseName').prop( "disabled", false );
		$('#StudentContactPerson3').prop( "disabled", false ); 
		$('#G3Mobile').prop( "disabled", false );	
	}else{
		
		$('#StudentRelationship3').prop( "disabled", true );
		$('#StudentRelationship3').prop( "checked", false );
		$('#G3Relationship').prop( "disabled", true );
		$('#G3Relationship').val('');
		$('#G3EnglishName').prop( "disabled", true ); 
		$('#G3EnglishName').val('');
		$('#G3ChineseName').prop( "disabled", true );
		$('#G3ChineseName').val('');
		$('#StudentContactPerson3').prop( "disabled", true ); 
		$('#StudentContactPerson3').prop( "checked", false ); 
		$('#G3Mobile').prop( "disabled", true );
		$('#G3Mobile').val('');
	}
}
/*
function check_docs_upload(form1) {
	//For debugging only
	//return true;
	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadbirthcert']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(form1.OtherFile1.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadimmunisationrecord']?>");
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileSize1 > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else  {
		return true;
	}
}
*/
function check_docs_upload(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		
		if(!isOldBrowser){
			var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			alert("<?=$Lang['Admission']['msg']['uploadfile']?>");
			file_element.focus();
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
			file_element.focus();
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
			file_element.focus();
			return false;
		}
	}
	
	return true;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

function checkApplicationNo(){
	var value = $("#TwinsApplicationID").val();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_check_application_no.php",
       type: "post",
       data: { ApplicationNo : value,  StudentDateOfBirth : $("#StudentDateOfBirth").val()},
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

function checkApplicationNo2(){
	var value = $("#CustApplicationID").val();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_check_application_no.php",
       type: "post",
       data: { ApplicationNo : value },
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

//function startUpload(){
//      document.getElementById('f1_upload_process').style.visibility = 'visible';
//      document.getElementById('f1_upload_form').style.visibility = 'hidden';
//      return true;
//}
//Henry modifying 20131028
//function stopUpload(temp_folder_name){
//		document.getElementById('tempFolderName').value = temp_folder_name;
//		
//		 var values = $("#form1").serialize();
//			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
//			var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
//			var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
//			values+=studentPersonalPhoto;
//			values+=otherFile;
//			values+=otherFile1;
//		$.ajax({
//		       url: "ajax_get_confirm.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!"+values);
//		           $("#step_confirm").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });     
//      return true;  
//}

//preview image function
function readFileURL(input) {
	if(navigator.appVersion.indexOf("MSIE 1")==-1 && !(navigator.appName == 'Netscape')){
		$('#href'+input.name).hide();
		$('#div'+input.name).hide();
		return;
	}		
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        	var isOldBrowser = 0;
			if(navigator.appName.indexOf("Internet Explorer")!=-1 || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null))){
				isOldBrowser = 1;
			}
			
        	if(isOldBrowser){
            	$('#href'+input.name).hide();
        	}
        	else{
        		$('#href'+input.name).attr('href', e.target.result);
        	}
        	
            if(input.value.split('.').pop().toUpperCase() == "PDF"){
            	$('#div'+input.name).hide();
            }
            else{
            	$('#img'+input.name).attr('src', e.target.result);
            }
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

/*$("#OtherFile0").change(function(){
    readFileURL(this);
});*/
</script>