<?php
	//using: Pun
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

	$admission_cfg['PrintByPDF'] = 1;

	//Please also define lang in admission_lang
	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['gotofirstinterview']	= 3;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;
	$admission_cfg['Status']['gotosecondinterview']	= 6;
	$admission_cfg['Status']['gotothirdinterview']	= 7;
	$admission_cfg['Status']['hkugaps_admitted']	= 8;
	$admission_cfg['Status']['hkugaps_notadmitted']	= 9;
	$admission_cfg['Status']['hkugaps_interviewnotattended'] = 10;

	$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
	$admission_cfg['PaymentStatus']['OtherPayment']	= 2;

	$admission_cfg['BriefingStatus'] = array();
	$admission_cfg['BriefingStatus']['pending']	= 1;
	$admission_cfg['BriefingStatus']['confirmed']	= 2;
	$admission_cfg['BriefingStatus']['cancelled']	= 3;

	$admission_cfg['BirthCertType'] = array();
	$admission_cfg['BirthCertType']['birthCert']	= 1;
	$admission_cfg['BirthCertType']['hkid']	= 2;
	$admission_cfg['BirthCertType']['others']	= 3;

	$admission_cfg['PlaceOfBirth'] = array();
	$admission_cfg['PlaceOfBirth'][0] = 'Others 其他';
	$admission_cfg['PlaceOfBirth'][1] = 'Australia 澳洲';
	$admission_cfg['PlaceOfBirth'][2] = 'Austria 奧地利';
	$admission_cfg['PlaceOfBirth'][3] = 'Brazil 巴西';
	$admission_cfg['PlaceOfBirth'][4] = 'Britain 英國';
	$admission_cfg['PlaceOfBirth'][5] = 'Canada 加拿大';
	$admission_cfg['PlaceOfBirth'][6] = 'China 中國';
	$admission_cfg['PlaceOfBirth'][7] = 'France 法國';
	$admission_cfg['PlaceOfBirth'][8] = 'Germany 德國';
	$admission_cfg['PlaceOfBirth'][9] = 'Hong Kong 香港'; // Hard-code default selected
	$admission_cfg['PlaceOfBirth'][10] = 'India 印度';
	$admission_cfg['PlaceOfBirth'][11] = 'Japan 日本';
	$admission_cfg['PlaceOfBirth'][12] = 'Korea 韓國';
	$admission_cfg['PlaceOfBirth'][13] = 'Macau 澳門';
	$admission_cfg['PlaceOfBirth'][14] = 'Malaysia 馬來西亞';
	$admission_cfg['PlaceOfBirth'][15] = 'Netherlands 荷蘭';
	$admission_cfg['PlaceOfBirth'][16] = 'Nepal 尼泊爾';
	$admission_cfg['PlaceOfBirth'][17] = 'New Zealand 紐西蘭';
	$admission_cfg['PlaceOfBirth'][18] = 'Pakistan 巴基斯坦';
	$admission_cfg['PlaceOfBirth'][19] = 'Philippines 菲律賓';
	$admission_cfg['PlaceOfBirth'][20] = 'Portugal 葡萄牙';
	$admission_cfg['PlaceOfBirth'][21] = 'Singapore 新加坡';
	$admission_cfg['PlaceOfBirth'][22] = 'Spain 西班牙';
	$admission_cfg['PlaceOfBirth'][23] = 'Switzerland 瑞士';
	$admission_cfg['PlaceOfBirth'][24] = 'Thailand 泰國';
	$admission_cfg['PlaceOfBirth'][25] = 'Taiwan 台灣';
	$admission_cfg['PlaceOfBirth'][26] = 'United Kingdom 聯合王國';
	$admission_cfg['PlaceOfBirth'][27] = 'United States 美國';
	$admission_cfg['PlaceOfBirth'][28] = 'Vietnam 越南';

	$admission_cfg['AddressDistrict'] = array();
	$admission_cfg['AddressDistrict'][0] = 'Central & Western 中西區';
	$admission_cfg['AddressDistrict'][1] = 'Wanchai 灣仔';
	$admission_cfg['AddressDistrict'][2] = 'Eastern 東區';
	$admission_cfg['AddressDistrict'][3] = 'Southern 南區';
	$admission_cfg['AddressDistrict'][4] = 'Yau Tsim Mong 油尖旺';
	$admission_cfg['AddressDistrict'][5] = 'Shum Shui Po 深水埗';
	$admission_cfg['AddressDistrict'][6] = 'Kowloon City 九龍城';
	$admission_cfg['AddressDistrict'][7] = 'Wong Tai Sin 黃大仙';
	$admission_cfg['AddressDistrict'][8] = 'Kwun Tong 觀塘';
	$admission_cfg['AddressDistrict'][9] = 'Tsuen Wan 荃灣';
	$admission_cfg['AddressDistrict'][10] = 'Tuen Mun 屯門';
	$admission_cfg['AddressDistrict'][11] = 'Yuen Long 元朗';
	$admission_cfg['AddressDistrict'][12] = 'North 北區';
	$admission_cfg['AddressDistrict'][13] = 'Tai Po 大埔';
	$admission_cfg['AddressDistrict'][14] = 'Sai Kung 西貢';
	$admission_cfg['AddressDistrict'][15] = 'Shatin 沙田';
	$admission_cfg['AddressDistrict'][16] = 'Kwai Tsing 葵青';
	$admission_cfg['AddressDistrict'][17] = 'Islands 離島';
	$admission_cfg['AddressDistrict'][18] = 'Inapplicable 不適用';

	$admission_cfg['AddressArea'] = array();
	$admission_cfg['AddressArea'][0] = 'Hong Kong 香港';
	$admission_cfg['AddressArea'][1] = 'Kowloon 九龍';
	$admission_cfg['AddressArea'][2] = 'New Territories 新界';
	$admission_cfg['AddressArea'][3] = 'Other 其他';

	$admission_cfg['EmailTemplateType'] = array();
	$admission_cfg['EmailTemplateType']['Normal'] = 0;
	$admission_cfg['EmailTemplateType']['Round1_Interview']['b5'] = 1;
	$admission_cfg['EmailTemplateType']['Round1_Interview']['en'] = 2;
	$admission_cfg['EmailTemplateType']['Round1_Interview_CannotArrange']['b5'] = 3;
	$admission_cfg['EmailTemplateType']['Round1_Interview_CannotArrange']['en'] = 4;
	$admission_cfg['EmailTemplateType']['Round2_Interview']['b5'] = 5;
	$admission_cfg['EmailTemplateType']['Round2_Interview']['en'] = 6;
	$admission_cfg['EmailTemplateType']['Round2_Interview_CannotArrange']['b5'] = 7;
	$admission_cfg['EmailTemplateType']['Round2_Interview_CannotArrange']['en'] = 8;
	$admission_cfg['EmailTemplateType']['Round3_Interview']['b5'] = 9;
	$admission_cfg['EmailTemplateType']['Round3_Interview']['en'] = 10;
	$admission_cfg['EmailTemplateType']['Round3_Interview_CannotArrange']['b5'] = 11;
	$admission_cfg['EmailTemplateType']['Round3_Interview_CannotArrange']['en'] = 12;
	$admission_cfg['EmailTemplateType']['Admitted']['b5'] = 13;
	$admission_cfg['EmailTemplateType']['Admitted']['en'] = 14;
	$admission_cfg['EmailTemplateType']['NotAdmitted']['b5'] = 15;
	$admission_cfg['EmailTemplateType']['NotAdmitted']['en'] = 16;

//
//	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
//	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
//	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
//	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
//	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
//	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
//	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
//	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);

if($plugin['eAdmission_devMode']){
//	/* for paypal testing [start] */
	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
////	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
	$admission_cfg['paypal_signature'] = 'Mo09rY-jL09Vq4ANvg3WmHNsY9DIozIE-Fd2QwAhi6NFBq0TduqTZw7hxhO';
////	$admission_cfg['item_name'] = 'Application Fee';
////	$admission_cfg['item_number'] = 'AF2015';
////	$admission_cfg['amount'] = '40.00';
////	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = '4UWGZVH5JQ9KE';
	$admission_cfg['paypal_name'] = 'HKUGA Primary School (Testing)';
//	/* for paypal testing [End] */

}else{
	/* for real paypal use [start] */
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
	$admission_cfg['paypal_signature'] = 'HrAcz5erXkqMj6jiaJFbEjwk00ybG-NCafvzX_iHn-3IknNl1tLVQA-tory';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = 'AF2015';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = 'SBYNKA5VW4YPY';
	$admission_cfg['paypal_name'] = 'The Incorporated Management Committee of HKUGA Primary School';
	/* for real paypal use [End] */
}

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	$admission_cfg['interview_arrangment']['interview_group_type'] = 'Room';
	//$admission_cfg['interview_arrangment']['interview_group_name'] = array('000', '105', '107', '309', '101', '102', '103', '104', '201', '202', '203', '204', '301', '302', '303', '304');
	$admission_cfg['interview_arrangment']['interview_group_name'] = array('000', '001', '002', '107', '210', '309', '101', '102', '103', '104', '201', '202', '203', '204', '301', '302');

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}