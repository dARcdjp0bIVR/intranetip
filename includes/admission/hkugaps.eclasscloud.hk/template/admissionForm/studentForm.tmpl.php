<style>
select:disabled{
    color: #ccc;
}
</style>

<h1><?=$Lang['Admission']['studentInfo']?> Student Information</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['chinesename']?> Name in Chinese
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_b5']?>
		<?php }else{ ?>
			<input name="studentsname_b5" type="text" id="studentsname_b5" class="textboxtext" value="<?=$applicationStudentInfo['student_name_b5']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star.$Lang['Admission']['englishname']?> (<?=$Lang['Admission']['UCCKE']['SameAsHKID']?>) <br />
		Name in English (same as on HKID Card)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_en']?>
		<?php }else{ ?>
			<input name="studentsname_en" type="text" id="studentsname_en" class="textboxtext"  value="<?=$applicationStudentInfo['student_name_en']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['dateofbirth']?> Date of Birth
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15" value="<?=$applicationStudentInfo['dateofbirth']?>"/>(YYYY-MM-DD)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['gender']?> Gender
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['StudentGender'])? ($Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']) : ' -- ' ?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" <?=$applicationStudentInfo['gender'] == "M"?'checked':'' ?>>
			<label for="StudentGender1"> <?=$Lang['Admission']['genderType']['M']?> M</label>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender" <?=$applicationStudentInfo['gender'] == "F"?'checked':'' ?>>
			<label for="StudentGender2"> <?=$Lang['Admission']['genderType']['F']?> F</label>
		<?php } ?>
	</td>
</tr>


<tr>	
   	<td class="field_title">
		<?=$star.$Lang['Admission']['placeofbirth']?> Place of Birth
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
	        if($formData['StudentPlaceOfBirth'] == '0'){
	            echo $formData['StudentPlaceOfBirthOther'];
	        }else{
	            echo $admission_cfg['PlaceOfBirth'][ $formData['StudentPlaceOfBirth'] ];
	        }
		}else{
		?>
			<select id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" onchange="showOtherTypeTextField(this, '0', 'StudentPlaceOfBirthOther')">
				<?php 
				foreach($admission_cfg['PlaceOfBirth'] as $index=>$place){
				    if($applicationStudentInfo['placeofbirth']){
				        $selected = ($index == $applicationStudentInfo['placeofbirth'])? 'selected':'';
				    }else{
				        $selected = ($index == '9')? 'selected':'';
				    }
				?>
					<option value="<?=$index ?>" <?=$selected ?>><?=$place ?></option>
				<?php 
				}

				if(
				    is_numeric($applicationStudentInfo['placeofbirth']) ||
				    $applicationStudentInfo['placeofbirth'] == ''
				){
				    $display = 'display: none;';
				    $otherValue = '';
				}else{
				    $display = '';
				    $otherValue = $applicationStudentInfo['placeofbirth'];
				}
				?>
			</select>
			<input name="StudentPlaceOfBirthOther" type="text" id="StudentPlaceOfBirthOther" value="<?=$otherValue?>" style="width: 150px;<?=$display ?>" />
		<?php
		}
		?>
	</td>
</tr>


<tr>

	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['personalIdentification']?> Personal Identification<br/><font color="blue">(須與上載的文件相同)<br/>(Must be same as the upload documents)</font>
	</td>
	<td colspan="3">
		<?php 
		if($IsConfirm){
		    if($formData['BirthCertType'] == $admission_cfg['BirthCertType']['birthCert']){
		        echo "{$Lang['Admission']['BirthCertType']['hk2']} HK Birth Certificate No.: {$formData['StudentBirthCertNo']}";
		    }else if($formData['BirthCertType'] == $admission_cfg['BirthCertType']['hkid']){
		        echo "{$Lang['Admission']['HKUGAPS']['birthcertno']} ID No.: {$formData['StudentBirthCertNo']}";
		    }else if($formData['BirthCertType'] == $admission_cfg['BirthCertType']['others']){
		        echo "{$Lang['Admission']['BirthCertType']['others']} Others.: {$formData['BirthCertTypeOther']} ({$formData['StudentBirthCertNo']})";
		    }else{
		        echo ' -- ';
		    }
		}else{ 
		?>
			<table>
				<tr>
					<td nowrap="" colspan="3">
						<select name="BirthCertType" id="BirthCertType" onchange="showOtherTypeTextField(this, '3', 'BirthCertTypeOther')">
							<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
							<option value="1" <?=$applicationStudentInfo['birthcerttype'] == "1"?'selected':'' ?>> <?=$Lang['Admission']['BirthCertType']['hk2'] ?> HK Birth Certificate No. </option>
							<option value="2" <?=$applicationStudentInfo['birthcerttype'] == "2"?'selected':'' ?>> <?=$Lang['Admission']['HKUGAPS']['birthcertno'] ?> ID No. </option>
							<option value="3" <?=$applicationStudentInfo['birthcerttype'] == "3"?'selected':'' ?>><?=$Lang['Admission']['BirthCertType']['others'] ?> Others </option>
						</select>
						<input name="BirthCertTypeOther" type="text" id="BirthCertTypeOther" class="textboxtext" style="width:100px" value="<?=$applicationStudentInfo['birthcerttypeother']?>" <?=$applicationStudentInfo['birthcerttype'] == "3"?'':'hidden' ?>/>
        			<label for="BirthCertTypeOther"><?=$Lang['Admission']['HKUGAPS']['no'] ?> No. </label>
        			
        			<?php if($BirthCertNo){ ?>
        				<input name="StudentBirthCertNo" value="<?=$BirthCertNo ?>" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none;width: 250px;"/>
        			<?php }else{ ?>
            			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationStudentInfo['birthcertno'] ?>"/>
            			<br/>
            			<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
            			 <br/>(eg：A123456(7)，please enter "A1234567")
        			<?php } ?>
        			</td>
				</tr>
			</table>

		<?php 
		} 
		?>
	</td>
	
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<span class="isFirstYear">
			<?=$Lang['Admission']['HKUGAPS']['spokenLanguageForInterview']?> Spoken language for interview
		</span>
		<span class="isNotFirstYear">
			<?=$Lang['Admission']['HKUGAPS']['SpokenLanguage']?> Spoken Language
		</span>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){
		    if($formData['SpokenLanguageForInterview'] == 'Cantonese'){
		        echo "{$Lang['Admission']['Languages']['Cantonese']} Cantonese";
		    }else if($formData['SpokenLanguageForInterview'] == 'English'){
		        echo "{$Lang['Admission']['Languages']['English']} English";
		    }else if($formData['SpokenLanguageForInterview'] == 'Putonghua'){
		        echo "{$Lang['Admission']['Languages']['Putonghua']} Putonghua";
		    }else{
		        echo ' -- ';
		    }
		}else{ ?>
			<div style="white-space: nowrap;">
    			<input type="radio" value="Cantonese" id="SpokenLanguageForInterview1" name="SpokenLanguageForInterview" <?=($applicationStudentInfo['langspokenathome'] == "Cantonese")?'checked':'' ?>>
    			<label for="SpokenLanguageForInterview1"> <?=$Lang['Admission']['Languages']['Cantonese'] ?> Cantonese </label>
			</div>
			<div style="white-space: nowrap;">
    			<input type="radio" value="English" id="SpokenLanguageForInterview2" name="SpokenLanguageForInterview" <?=($applicationStudentInfo['langspokenathome'] == "English")?'checked':'' ?>>
    			<label for="SpokenLanguageForInterview2"> <?=$Lang['Admission']['Languages']['English'] ?> English </label>
			</div>
			<div style="white-space: nowrap;">
    			<input type="radio" value="Putonghua" id="SpokenLanguageForInterview3" name="SpokenLanguageForInterview" <?=($applicationStudentInfo['langspokenathome'] == "Putonghua")?'checked':'' ?>>
    			<label for="SpokenLanguageForInterview3"> <?=$Lang['Admission']['Languages']['Putonghua'] ?> Putonghua </label>
			</div>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['address']?> Home Address
	</td>
	<td colspan="3">
    	<table>
    		<tr>
    			<td width="250px"><?=$Lang['Admission']['HKUGAPS']['addressFlat'] ?> Flat</td>
    			<td width="250px">
    				<?php if($IsConfirm){ ?>
            			<?=$formData['HomeAddrRoom']?>
            		<?php }else{ ?>
            			<input name="HomeAddrRoom" type="text" id="HomeAddrRoom" class="textboxtext" value="<?=$applicationStudentInfo['AddressRoom']?>"/>
            		<?php } ?>
    			</td>
    			<td width="250px"><?=$Lang['Admission']['HKUGAPS']['addressFloor'] ?> Floor</td>
    			<td>
    				<?php if($IsConfirm){ ?>
            			<?=$formData['HomeAddrFloor']?>
            		<?php }else{ ?>
            			<input name="HomeAddrFloor" type="text" id="HomeAddrFloor" class="textboxtext" value="<?=$applicationStudentInfo['AddressFloor']?>"/>
            		<?php } ?>
    			</td>
    		</tr>
    		<tr>
    			<td><?=$Lang['Admission']['HKUGAPS']['addressBlock'] ?> Block</td>
    			<td>
    				<?php if($IsConfirm){ ?>
            			<?=$formData['HomeAddrBlock']?>
            		<?php }else{ ?>
            			<input name="HomeAddrBlock" type="text" id="HomeAddrBlock" class="textboxtext" value="<?=$applicationStudentInfo['AddressBlock']?>"/>
            		<?php } ?>
    			</td>
    			<td><?=$Lang['Admission']['HKUGAPS']['addressBuilding'] ?> Building</td>
    			<td>
    				<?php if($IsConfirm){ ?>
            			<?=$formData['HomeAddrBlding']?>
            		<?php }else{ ?>
            			<input name="HomeAddrBlding" type="text" id="HomeAddrBlding" class="textboxtext" value="<?=$applicationStudentInfo['AddressBldg']?>"/>
            		<?php } ?>
    			</td>
    		</tr>
    		<tr>
    			<td><?=$Lang['Admission']['HKUGAPS']['addressEstate'] ?> Estate</td>
    			<td>
    				<?php if($IsConfirm){ ?>
            			<?=$formData['HomeAddrEstate']?>
            		<?php }else{ ?>
            			<input name="HomeAddrEstate" type="text" id="HomeAddrEstate" class="textboxtext" value="<?=$applicationStudentInfo['AddressEstate']?>"/>
            		<?php } ?>
    			</td>
    			<td><?=($lac->isInternalUse($_GET['token']))? '':$star ?><?=$Lang['Admission']['HKUGAPS']['addressStreetNo'] ?> Street and Number</td>
    			<td>
    				<?php if($IsConfirm){ ?>
            			<?=$formData['HomeAddrStreet']?>
            		<?php }else{ ?>
            			<input name="HomeAddrStreet" type="text" id="HomeAddrStreet" class="textboxtext" value="<?=$applicationStudentInfo['AddressStreet']?>"/>
            		<?php } ?>
    			</td>
    		</tr>
    		<tr>
    			<td><?=($lac->isInternalUse($_GET['token']))? '':$star ?><?=$Lang['Admission']['HKUGAPS']['addressDistrict'] ?> District</td>
    			<td>
    				<?php if($IsConfirm){ ?>
            			<?= ($admission_cfg['AddressDistrict'][ $formData['HomeAddrDistrict'] ])? ($admission_cfg['AddressDistrict'][ $formData['HomeAddrDistrict'] ]) : ' -- ' ?>
            		<?php }else{ ?>
            			<select id="HomeAddrDistrict" name="HomeAddrDistrict">
            				<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
            				<?php 
            				foreach($admission_cfg['AddressDistrict'] as $index=>$district){ 
            				    if(
            				        $applicationStudentInfo['AddressDistrict'] == $index &&
            				        $applicationStudentInfo['AddressDistrict'] !== '' && 
            				        $applicationStudentInfo['AddressDistrict'] !== null
        				        ){
            				        $selected = 'selected';
            				    }else{
            				        $selected = '';
            				    }
            				?>
            					<option value="<?=$index ?>" <?=$selected ?>><?=$district ?></option>
            				<?php 
            				} 
            				?>
            			</select>
            		<?php
            		}
            		?>
    			</td>
    			
    			<td><?=($lac->isInternalUse($_GET['token']))? '':$star ?><?=$Lang['Admission']['HKUGAPS']['addressArea'] ?> Area</td>
    			<td>
            		<?php
            		$totalAddressArea = count($admission_cfg['AddressArea']);
            		if($IsConfirm){
            		    if($formData['HomeAddrArea'] == ($totalAddressArea-1)){
        		            echo $formData['HomeAddrAreaOther'];
        		        }else if($admission_cfg['AddressArea'][ $formData['HomeAddrArea'] ]){
        		            echo $admission_cfg['AddressArea'][ $formData['HomeAddrArea'] ];
        		        }else{
            		        echo ' -- ';
        		        }
            		}else{
            		?>
            			<select id="HomeAddrArea" name="HomeAddrArea" onchange="showOtherTypeTextField(this, '<?=($totalAddressArea-1) ?>', 'HomeAddrAreaOther')">
            				<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
            				<?php
            				foreach($admission_cfg['AddressArea'] as $index=>$area){ 
            				    if(
            				        (
                				        !is_numeric($applicationStudentInfo['Address']) || 
                				        $applicationStudentInfo['Address'] == $index 
            				        ) &&
            				        $applicationStudentInfo['Address'] !== '' && 
            				        $applicationStudentInfo['Address'] !== null
        				        ){
            				        $selected = 'selected';
            				    }else{
            				        $selected = '';
            				    }
            				?>
            					<option value="<?=$index ?>" <?=$selected ?>><?=$area ?></option>
            				<?php 
            				} 
            				
        				    if(
        				        is_numeric($applicationStudentInfo['Address']) || 
        				        $applicationStudentInfo['Address'] == ''
        				    ){
        				        $display = 'display: none;';
        				        $otherValue = '';
        				    }else{
        				        $display = '';
        				        $otherValue = $applicationStudentInfo['Address'];
        				    }
            				?>
            			</select>
            			<input name="HomeAddrAreaOther" type="text" id="HomeAddrAreaOther" value="<?=$otherValue?>" style="width: 200px;<?=$display ?>" />
            		<?php
            		}
            		?>
				</td>
    		</tr>
    	</table>
    	
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['HKUGAPS']['hometel']?> Tel (Home)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['HomeTelNo']?>
		<?php }else{ ?>
			<input name="HomeTelNo" type="text" id="TelHome" class="textboxtext" value="<?=$applicationStudentInfo['homephoneno']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['HKUGAPS']['contactEmail']?> (1) Parent's Contact E-mail (1)<br />
		<!--span class="">
    		<font color="blue">
        		(<?= $Lang['Admission']['HKUGAPS']['contactEmailHint_Y1']?>)<br />
        		(Must provide a valid email address; this email address cannot be changed after submitted)
    		</font>
		</span-->
		<span class="">
    		<font color="blue">
        		(<?= $Lang['Admission']['HKUGAPS']['contactEmailHint']?>)<br />
        		(Must provide a valid email address for receiving interview notification and result from our school; this email cannot be changed after submitted)
    		</font>
		</span>
	</td>
	
	<?php if($applicationStudentInfo['email']){ ?>
    	<td>
    		<input name="ContactEmail" value="<?=$applicationStudentInfo['email'] ?>" readonly type="text" id="ContactEmail" class="textboxtext" style="border: none;width: 250px;"/>
    	</td>
	<?php }else if($IsConfirm){ ?>
    	<td>
    		<?=$formData['ContactEmail']?>
    	</td>
	<?php }else{ ?>
    	<td>
    		<input name=ContactEmail type="text" id="ContactEmail" class="textboxtext" value="<?=$applicationStudentInfo['email']?>"/>
    	</td>
	<?php } ?>
	
	<td class="field_title">
		<?=$star.$Lang['Admission']['HKUGAPS']['contactEmail']?> (2) Parent's Contact E-mail (2)<br />
		<!--span class="">
    		<font color="blue">
        		(<?= $Lang['Admission']['HKUGAPS']['contactEmailHint_Y1']?>)<br />
        		(Must provide a valid email address; this email address cannot be changed after submitted)
    		</font>
		</span-->
		<span class="">
    		<font color="blue">
        		(<?= $Lang['Admission']['HKUGAPS']['contactEmailHint']?>)<br />
        		(Must provide a valid email address for receiving interview notification and result from our school; this email cannot be changed after submitted)
    		</font>
		</span>
	</td>
	
	<?php if($applicationStudentInfo['email'] || $applicationStudentInfo['email2']){ ?>
    	<td>
    		<input name="ContactEmail2" value="<?=$applicationStudentInfo['email2'] ?>" readonly type="text" id="ContactEmail2" class="textboxtext" style="border: none;width: 250px;"/>
    	</td>
	<?php }else if($IsConfirm){ ?>
    	<td>
    		<?=$formData['ContactEmail2']?>
    	</td>
	<?php }else{ ?>
    	<td>
    		<input name=ContactEmail2 type="text" id="ContactEmail2" class="textboxtext" value="<?=$applicationStudentInfo['email2']?>"/>
    	</td>
	<?php } ?>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['HKUGAPS']['noBrotherSister']?> No. of Brother(s)/Sister(s)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['NoBrotherSister']?>
		<?php }else{ ?>
			<select name="NoBrotherSister" id="NoBrotherSister" onchange="updateBrotherSisterRankDisabled(this)">
				<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
				<?php
				for($i=0;$i<11;$i++){
				    if(
				        $allCustInfo['No_Brother_Sister'][0]['Value'] == $i &&
				        $allCustInfo['No_Brother_Sister'][0]['Value'] !== '' && 
				        $allCustInfo['No_Brother_Sister'][0]['Value'] !== null
			        ){
				        $selected = 'selected';
				    }else{
				        $selected = '';
				    }
			    ?>
			    	<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
			    <?php
				}
				?>
			</select>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['HKUGAPS']['rank']?> Rank
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['BrotherSisterRank'])? $formData['BrotherSisterRank'] : ' -- ' ?>
		<?php }else{ 
		    $disabled = (empty($allCustInfo['No_Brother_Sister'][0]['Value']))? 'disabled' : '';
	    ?>
			<select name="BrotherSisterRank" id="BrotherSisterRank" <?=$disabled ?>>
				<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
				<?php
				for($i=1;$i<11;$i++){
				    if(
				        $allCustInfo['Brother_Sister_Rank'][0]['Value'] == $i &&
				        $allCustInfo['Brother_Sister_Rank'][0]['Value'] !== '' && 
				        $allCustInfo['Brother_Sister_Rank'][0]['Value'] !== null
			        ){
				        $selected = 'selected';
				    }else{
				        $selected = '';
				    }
			    ?>
			    	<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
			    <?php
				}
				?>
			</select>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['contactperson']?> Contact Person
	</td>
	<td colspan="3">
		<?php if($IsConfirm){
		    $persons = array();
		    if(in_array('Father', (array)$formData['ContactPerson'])){
		        $persons[] = "{$Lang['Admission']['HKUGAPS']['father']} Father";
		    }
		    if(in_array('Mother', (array)$formData['ContactPerson'])){
		        $persons[] = "{$Lang['Admission']['HKUGAPS']['mother']} Mother";
		    }
		    if(in_array('Guardian', (array)$formData['ContactPerson'])){
		        $persons[] = "{$Lang['Admission']['HKUGAPS']['guardian']} Legal Guardian";
		    }
		    if(count($persons)){
		        echo implode('<br />', $persons);
		    }else{
		        echo ' -- ';
		    }
		}else{ 
			if(!$persons){
				$persons = array();
			}
		?>
			<div style="white-space: nowrap;">
    			<input type="checkbox" value="Father" id="ContactPerson1" name="ContactPerson[]" <?=(in_array('Father',$persons))?'checked':'' ?>>
    			<label for="ContactPerson1"> <?=$Lang['Admission']['HKUGAPS']['father'] ?> Father </label>
			</div>
			<div style="white-space: nowrap;">
    			<input type="checkbox" value="Mother" id="ContactPerson2" name="ContactPerson[]" <?=(in_array('Mother',$persons))?'checked':'' ?>>
    			<label for="ContactPerson2"> <?=$Lang['Admission']['HKUGAPS']['mother'] ?> Mother </label>
			</div>
			<div style="white-space: nowrap;">
    			<input type="checkbox" value="Guardian" id="ContactPerson3" name="ContactPerson[]" <?=(in_array('Guardian',$persons))?'checked':'' ?>>
    			<label for="ContactPerson3"> <?=$Lang['Admission']['HKUGAPS']['guardian'] ?> Legal Guardian </label>
			</div>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<span class="isFirstYear">
    		<?=$Lang['Admission']['HKUGAPS']['currentAttend_Y1']?> <br /> Name of School / Kindergarten /Nursery Currently Attending
		</span>
		<span class="isNotFirstYear">
    		<?=$Lang['Admission']['HKUGAPS']['currentAttend']?> Name of School(s) Attended
		</span>
	</td>
	<td colspan="3">
		<table id="dataTable1" class="form_table" style="font-size: 13px">
			<tr>
				<td>&nbsp;</td>
				<td class="form_guardian_head" style="width: 400px;"><center><?=$Lang['Admission']['HKUGAPS']['name'] ?> Name</center></td>
				<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['grades'] ?> Grades</center></td>
				<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['year'] ?> Year</center></td>
			</tr>
			<tr>
				<td class="field_title" style="text-align:right;width:50px;">(1)</td>
				<td class="form_guardian_field">
					<?php 
					if($IsConfirm){
					    echo $formData['CurrentAttend1'];
            		}else{ 
            		?>
						<input name="CurrentAttend1" type="text" id="CurrentAttend1" class="textboxtext" value="<?=$studentCustInfo[0]['NameOfSchool']?>"/>
					<?php 
	                } 
	                ?>
				</td>
				<td class="form_guardian_field">
					<?php 
					if($IsConfirm){
					    echo "{$formData['Grades1Start']} - {$formData['Grades1End']}";
            		}else{ 
            		?>
						<input name="Grades1Start" type="text" id="Grades1Start" value="<?=$studentCustInfo[0]['Class'][0]?>" style="width: 40%"/>
						-
						<input name="Grades1End" type="text" id="Grades1End" value="<?=$studentCustInfo[0]['Class'][1]?>" style="width: 40%"/>
					<?php 
	                } 
	                ?>
				</td>
				<td class="form_guardian_field">
					<?php 
					if($IsConfirm){
					    echo "{$formData['Year1Start']} - {$formData['Year1End']}";
            		}else{ 
            		?>
						<input name="Year1Start" type="text" id="Year1Start" maxlength="4" value="<?=$studentCustInfo[0]['Year'][0]?>" style="width: 40%"/>
						-
						<input name="Year1End" type="text" id="Year1End" maxlength="4" value="<?=$studentCustInfo[0]['Year'][1]?>" style="width: 40%"/>
					<?php 
	                } 
	                ?>
				</td>
			</tr>
			<tr>
				<td class="field_title" style="text-align:right;width:50px;">(2)</td>
				<td class="form_guardian_field">
					<?php 
					if($IsConfirm){
					    echo $formData['CurrentAttend2'];
            		}else{ 
            		?>
						<input name="CurrentAttend2" type="text" id="CurrentAttend2" class="textboxtext" value="<?=$studentCustInfo[1]['NameOfSchool']?>"/>
					<?php 
	                } 
	                ?>
				</td>
				<td class="form_guardian_field">
					<?php 
					if($IsConfirm){
					    echo "{$formData['Grades2Start']} - {$formData['Grades2End']}";
            		}else{ 
            		?>
    					<input name="Grades2Start" type="text" id="Grades2Start" value="<?=$studentCustInfo[1]['Class'][0]?>" style="width: 40%"/>
    					-
    					<input name="Grades2End" type="text" id="Grades2End" value="<?=$studentCustInfo[1]['Class'][1]?>" style="width: 40%"/>
					<?php 
	                } 
	                ?>
				</td>
				<td class="form_guardian_field">
					<?php 
					if($IsConfirm){
					    echo "{$formData['Year2Start']} - {$formData['Year2End']}";
            		}else{ 
            		?>
    					<input name="Year2Start" type="text" id="Year2Start" maxlength="4" value="<?=$studentCustInfo[1]['Year'][0]?>" style="width: 40%"/>
    					-
    					<input name="Year2End" type="text" id="Year2End" maxlength="4" value="<?=$studentCustInfo[1]['Year'][1]?>" style="width: 40%"/>
					<?php 
	                } 
	                ?>
				</td>
			</tr>
		</table>
	</td>
</tr>

<tr class="isNotFirstYear">
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['specialEducationalNeeds']?> <br />
		Does the applicant have any special educational needs (e.g.: gifted, reading and learning difficulty, etc)
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ 
		    if($formData['specialEducationalNeeds']){
		        echo $Lang['Admission']['yes'].' Yes<br />';
		        echo $formData['specialEducationalNeeds_Details'];
		    }else{
		        echo $Lang['Admission']['no'].' No';
		    }
		}else{ ?>
			<input type="radio" value="1" id="specialEducationalNeedsY" name="specialEducationalNeeds" <?=$allCustInfo['specialEducationalNeeds'][0]['Value']=='1'?'checked':'' ?>>
			<label for="specialEducationalNeedsY"> <?=$Lang['Admission']['yes']?> Yes</label>
			(
				<label for="specialEducationalNeedsY">
					<?=$Lang['Admission']['PleaseSpecify'] ?>
					Please specify
				</label>
				<input name="specialEducationalNeeds_Details" type="text" id="specialEducationalNeeds_Details" class="textboxtext" style="width: 300px;" value="<?=$allCustInfo['specialEducationalNeeds_Details'][0]['Value']?>"/>
			)
			<br />
			<input type="radio" value="0" id="specialEducationalNeedsN" name="specialEducationalNeeds" <?=$allCustInfo['specialEducationalNeeds'][0]['Value']=='1'?'':'checked' ?>>
			<label for="specialEducationalNeedsN"> <?=$Lang['Admission']['no']?> No</label>
		<?php } ?>
	</td>
</tr>

<tr class="isNotFirstYear">
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['appliedBefore']?> <br />
		Does the applicant ever applied for admission to our school
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ 
		    if($formData['appliedBefore']){
		        echo $Lang['Admission']['yes'].' Yes<br />';
		        echo $formData['appliedBefore_Details'];
		    }else{
		        echo $Lang['Admission']['no'].' No';
		    }
		}else{ ?>
			<input type="radio" value="1" id="appliedBeforeY" name="appliedBefore" <?=$allCustInfo['appliedBefore'][0]['Value']=='1'?'checked':'' ?>>
			<label for="appliedBeforeY"> <?=$Lang['Admission']['yes']?> Yes</label>
			(
				<label for="appliedBeforeY">
					<?=$Lang['Admission']['HKUGAPS']['PleaseSpecifyYear'] ?>
					Please specify the year
				</label>
				<input name="appliedBefore_Details" type="text" id="appliedBefore_Details" class="textboxtext" style="width: 300px;" value="<?=$allCustInfo['appliedBefore_Details'][0]['Value']?>"/>
			)
			<br />
			<input type="radio" value="0" id="appliedBeforeN" name="appliedBefore" <?=$allCustInfo['appliedBefore'][0]['Value']=='1'?'':'checked' ?>>
			<label for="appliedBeforeN"> <?=$Lang['Admission']['no']?> No</label>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['twinsApp']?> Application from Twins
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['twins'])?$Lang['Admission']['yes'].' Yes':$Lang['Admission']['no'].' No'?>
		<?php }else{ ?>
			<input type="radio" value="1" id="twinsY" name="twins" <?=$applicationStudentInfo['istwinsapplied']=='1'?'checked':'' ?> onclick="document.getElementById('twinsapplicationid').disabled = false;">
			<label for="twinsY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
			<input type="radio" value="0" id="twinsN" name="twins" <?=$applicationStudentInfo['istwinsapplied']=='1'?'':'checked' ?> onclick="document.getElementById('twinsapplicationid').disabled = true;document.getElementById('twinsapplicationid').value = ''">
			<label for="twinsN"> <?=$Lang['Admission']['no']?> No</label>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['HKUGAPS']['twinsID']?> Twins' Number of Identity Document
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=($formData['twinsapplicationid'])? $formData['twinsapplicationid'] : ' -- ' ?>
		<?php }else{ 
		    $disabled = ($applicationStudentInfo['istwinsapplied']=='1')?'':'disabled';
	    ?>
			<input name="twinsapplicationid" type="text" id="twinsapplicationid" class="textboxtext" onkeypress="$('#twinsY').attr('checked', 'checked')" value="<?=$applicationStudentInfo['twinsapplicationid'] ?>" <?=$disabled ?> />
		<?php } ?>
	</td>
</tr>

</table>
