<?php
# modifying by: Pun

/********************
 * 
 * Log :
 * Date		2016-01-25 (Omas)
 * 			modified getApplicationDetails() - add paymentStatus filter , modified getExportData(), getExportHeader() , getApplicationStatus() , 
 * Date		2015-11-16 (Pun)
 * 			modified insertApplicationAllInfo(), updateApplicationStatus(), getApplicationStatus()
 * Date		2015-11-05 (Pun)
 * 			modified getExportData()
 * Date		2015-11-04 (Henry)
 * 			modified insertApplicationOthersInfo()
 * Date		2015-09-25 (Henry)
 * 			modified updateApplicationStatus()
 * Date		2015-09-23 (Henry)
 * 			modified newApplicationNumber2(), uploadAttachment(), getApplicationOthersInfo()
 * Date		2015-09-22 (Henry)
 * 			added updateApplicantArrangement()
 * 			modified insertApplicationStudentInfo(), updateApplicationStudentInfo() and getApplicationStudentInfo()
 * 			getExportHeader(), getExportData()
 * 
 * Date		2014-08-20 (Henry) 
 * 			added checkImportDataForImportAdmission() and importDataForImportAdmission()
 * 
 * Date		2014-07-25 [Henry]
 * 			Added funciton sendMailToNewApplicant
 * 
 * Date		2014-06-19 [Henry]
 * 			Modified insertApplicationOthersInfo() to insert the token value
 * 			Added funciton hasToken()
 * 
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_cust_base
 * 
 * Date		2014-01-02 [Henry]
 * 			created functions getExportHeader() and getExportData()
 * 
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_cust_base.php");
 
class admission_cust extends admission_cust_base{
	function admission_cust(){
		global $kis_lang, $UserID; //switch $lang for IP/EJ/KIS
		$this->libdb();
		$this->filepath = '/file/admission/';
		$this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
		$this->schoolYearID = $this->getNextSchoolYearID();
		$this->uid = $UserID;
		$this->classLevelAry = $this->getClassLevel();
	}
	
	function testing(){
		return "test";
	}
	function encrypt_attachment($file){
		list($filename,$ext) = explode('.',$file);
	    $timestamp = date("YmdHis");
	    return base64_encode($filename.'_'.$timestamp).'.'.$ext;
	}
	function getNextSchoolYearID(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		$SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField=array('AcademicYearID'),1);
		krsort($SchoolYearIDArr);
		
		$SchoolYearIDArr = array_values($SchoolYearIDArr);
		$currentSchoolYear = Get_Current_Academic_Year_ID();
		$key = array_search($currentSchoolYear, $SchoolYearIDArr);
		if($key>0){
    		return $SchoolYearIDArr[$key-1];
    	}else{
    		return false;
    	}
	}
	function insertApplicationAllInfo($libkis_admission, $Data, $ApplicationID, $isUpdate=0){
		global $admission_cfg;
		extract($Data);
		
		$Success = array();
		if($ApplicationID != ""){
			
			if(!$isUpdate)
				$Success[] = $this->insertApplicationStatus($libkis_admission, $ApplicationID);

			if($isUpdate){
			    $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '".$ApplicationID."'";
			    $success[] = $this->db_db_query($sql);
			}
			
			######## Student START ########
			$Success[] = $this->insertApplicationStudentInfo($Data, $ApplicationID, $isUpdate);
			$Success[] = $this->insertApplicationStudentInfoCust($Data, $ApplicationID, $isUpdate);
			######## Student END ########
			
			######## Parent START ########
			$Success[] = $this->insertApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G1ChineseName,
				'EnglishName' => $G1EnglishName,
				'Mobile' => $G1MobileNo,
				'Email' => $G1Email,
				'JobTitle' => $G1Occupation,
				'Company' => $G1Institution,
				'PG_TYPE' => 'F'
			), $isUpdate);
			$Success[] = $this->insertApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G2ChineseName,
				'EnglishName' => $G2EnglishName,
				'Mobile' => $G2MobileNo,
				'Email' => $G2Email,
				'JobTitle' => $G2Occupation,
				'Company' => $G2Institution,
				'PG_TYPE' => 'M'
			), $isUpdate);
    		$Success[] = $this->insertApplicationParentInfo(array(
    				'ApplicationID' => $ApplicationID,
    				'ChineseName' => $G3Name,
    				'Relationship' => $G3Relationship,
    				'PG_TYPE' => 'G'
    		), $isUpdate);
			
			
			#### HKUGA Member START ####
			$success[] = $this->insertApplicationCustInfo(array(
    			'Code' => 'HKUGA_Member',
    			'Value' => $memberOfHKUGA
			), $ApplicationID);
			if($memberOfHKUGA){
    			$success[] = $this->insertApplicationCustInfo(array(
        			'Code' => 'HKUGA_Member_Name',
        			'Value' => $memberOfHKUGAName
    			), $ApplicationID);
			    
    			$success[] = $this->insertApplicationCustInfo(array(
        			'Code' => 'HKUGA_Member_No',
        			'Value' => $memberOfHKUGANo
    			), $ApplicationID);
			}
			#### HKUGA Member END ####

			#### HKUGA Education Foundation START ####
			$success[] = $this->insertApplicationCustInfo(array(
    			'Code' => 'HKUGA_Foundation_Member',
    			'Value' => $memberOfHKUGAFoundation
			), $ApplicationID);
			if($memberOfHKUGAFoundation){
    			$success[] = $this->insertApplicationCustInfo(array(
        			'Code' => 'HKUGA_Foundation_Member_Name',
        			'Value' => $memberOfHKUGAFoundationName
    			), $ApplicationID);
			}
			#### HKUGA Education Foundation END ####
			######## Parent END ########
			
			
			######## Sibling START ########
			$success[] = $this->insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate);
			######## Sibling END ########
			
			######## Update School Year Record START ########
			$Success[] = $this->insertApplicationOthersInfo($Data, $ApplicationID);
			######## Update School Year Record END ########
			
			if(in_array(false,$Success)){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	function insertApplicationStatus($libkis_admission, $ApplicationID, $SchoolYearID = ''){
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){

//   		}
//   		else{
   			//Henry: not yet finish
   			$sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
     					SchoolYearID,
     					ApplicationID,
					    Status,
					    DateInput)
					VALUES (
						'".($SchoolYearID?$SchoolYearID:$this->schoolYearID)."',
						'".$ApplicationID."',
						'1',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	function updateApplicationStatus($data){
		global $UserID;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
				s.ReceiptID = '".$receiptID."',
     			s.ReceiptDate = '".$receiptdate."',
      			s.Handler = '".$handler."', 
      			s.InterviewDate = '".$interviewdate."',
				s.InterviewLocation = '".$interviewlocation."',
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."',
				o.InterviewSettingID =  '".$InterviewSettingID."',
				o.InterviewSettingID2 =  '".$InterviewSettingID2."',
				o.InterviewSettingID3 =  '".$InterviewSettingID3."',
				s.FeeBankName = '{$FeeBankName}',
				s.FeeChequeNo = '{$FeeChequeNo}'
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}  
	function updateApplicationStatusByIds($applicationIds,$status){
		global $UserID;
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.RecordID IN (".$applicationIds.")
    	";
    	return $this->db_db_query($sql);
	}	  	
   	function insertApplicationStudentInfo($Data, $ApplicationID, $isUpdate=0){
   	    global $admission_cfg;
   		extract($Data);
   		
   		if($StudentPlaceOfBirth == '0'){
   		    $StudentPlaceOfBirth = $StudentPlaceOfBirthOther;
   		}
   		if($HomeAddrArea == (count($admission_cfg['AddressArea'])-1) ){
   		    $HomeAddrArea = $HomeAddrAreaOther;
   		}
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			if($isUpdate){
   				$sql = "UPDATE 
					ADMISSION_STU_INFO 
				SET
					 ChineseName = '{$studentsname_b5}',
					 EnglishName = '{$studentsname_en}',
					 Gender = '{$StudentGender}',
					 DOB = '{$StudentDateOfBirth}',
					 PlaceOfBirth = '{$StudentPlaceOfBirth}',
					 BirthCertNo = '{$StudentBirthCertNo}',
					 BirthCertType = '{$BirthCertType}',
					 BirthCertTypeOther = '{$BirthCertTypeOther}',
					 LangSpokenAtHome = '{$SpokenLanguageForInterview}',
            		 AddressRoom = '{$HomeAddrRoom}',
            		 AddressFloor = '{$HomeAddrFloor}',
            		 AddressBlock = '{$HomeAddrBlock}',
            		 AddressBldg = '{$HomeAddrBlding}',
            		 AddressEstate = '{$HomeAddrEstate}',
            		 AddressStreet = '{$HomeAddrStreet}',
            		 AddressDistrict = '{$HomeAddrDistrict}',
					 Address = '{$HomeAddrArea}',
					 HomeTelNo = '{$HomeTelNo}',
					 Email = '{$ContactEmail}',
					 Email2 = '{$ContactEmail2}',
					 IsTwinsApplied = '{$twins}',
			    	 TwinsApplicationID = '{$twinsapplicationid}',
				     DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'";
   			}
   			else{
	   			$sql = "INSERT INTO ADMISSION_STU_INFO (
							ApplicationID,
			   				 ChineseName,
			  				 EnglishName,   
			   				 Gender,
			  				 DOB,
			   				 BirthCertNo,
	   			             BirthCertType,
	   			             BirthCertTypeOther,
			   				 PlaceOfBirth,
	   			             LangSpokenAtHome,
    						 AddressRoom,
    						 AddressFloor,
    						 AddressBlock,
    						 AddressBldg,
    						 AddressEstate,
    						 AddressStreet,
    						 AddressDistrict,
	   						 Address,
	   			             HomeTelNo,
	   						 Email,
	   						 Email2,
	   						IsTwinsApplied,
							TwinsApplicationID,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$studentsname_b5."',
							'".$studentsname_en."',
							'".$StudentGender."',
							'".$StudentDateOfBirth."',
							'".$StudentBirthCertNo."',
							'".$BirthCertType."',
							'".$BirthCertTypeOther."',
							'".$StudentPlaceOfBirth."',
							'".$SpokenLanguageForInterview."',
							'{$HomeAddrRoom}',
							'{$HomeAddrFloor}',
							'{$HomeAddrBlock}',
							'{$HomeAddrBlding}',
							'{$HomeAddrEstate}',
							'{$HomeAddrStreet}',
							'{$HomeAddrDistrict}',
							'{$HomeAddrArea}',
							'{$HomeTelNo}',
							'{$ContactEmail}',
							'{$ContactEmail2}',
							'{$twins}',
							'{$twinsapplicationid}',
							now())
				";
   			}
			return $this->db_db_query($sql);
   		}
   		return false;
    }
    
    function insertApplicationParentInfo($Data, $isUpdate=0){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
		
		if($isUpdate){
			foreach($Data as $_fieldname => $_fieldvalue){
				if($_fieldname == "ApplicationID"){
					$ApplicationID = $_fieldvalue;
				}
				else if($_fieldname == "PG_TYPE"){
					$PG_TYPE = $_fieldvalue;
				}
				else{
					$updateStr .= $_fieldname." = '".$_fieldvalue."',";
				}
			}
			//Henry
			$sql = "UPDATE 
					ADMISSION_PG_INFO 
				SET
					".$updateStr."
				    DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'
				AND
					PG_TYPE = '".$PG_TYPE."'";
					
			if(($result = $this->db_db_query($sql)) && $this->db_affected_rows() == 0){
				$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
			}
			else{
				return $result;
			}
		}
		else{	
			$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
		}
		return $this->db_db_query($sql);
    }

     function updateApplicationParentInfo($Data,$type){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			if($_fieldname != "ApplicationID"){
				$fieldname .= "{$_fieldname} = '{$_fieldvalue}',";
			}
		}
		
		$sql = "UPDATE ADMISSION_PG_INFO SET
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					ApplicationID = '".$Data['ApplicationID']."'
				AND
					PG_TYPE = '{$type}';
				";
		return $this->db_db_query($sql);
    }
    
       
    function insertApplicationOthersInfo($Data, $ApplicationID, $isUpdate=0){ 
    	global $admission_cfg, $sys_custom;
   		extract($Data);
   		$success = array();
   		#Check exist application
   		if($ApplicationID == ""){
   			return false;
   		}
    	
    	if(!$this->isInternalUse($_REQUEST['token']) && $token=='' && !$sys_custom['KIS_Admission']['IntegratedCentralServer']){
    		$token = 1;
    	}
   		
		$sql = "UPDATE 
				ADMISSION_OTHERS_INFO 
			SET
				 ApplyYear = '".($schoolYearID?$schoolYearID:$this->schoolYearID)."',
				 ApplyLevel = '".$sus_status."',
				 Token = '".$token."',
			     DateInput = NOW(),
				 HTTP_USER_AGENT = '".addslashes($_SERVER['HTTP_USER_AGENT'])."'
			WHERE
				ApplicationID = '".$ApplicationID."'";
				
		if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
		
   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
						ApplicationID,
					     ApplyYear,
					     ApplyLevel,
						 Token,
					     DateInput,
   						 HTTP_USER_AGENT
					     )
					VALUES (
						'".$ApplicationID."',
						'".($schoolYearID?$schoolYearID:$this->schoolYearID)."',
						'".$sus_status."',
						'".$token."',
						now(),
						'".addslashes($_SERVER['HTTP_USER_AGENT'])."')
			";
			$success[] = $this->db_db_query($sql);
		}			
			
		return !in_array(false, $success);
    }
    
    function insertApplicationStudentInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			
   			######## Update Old School Record START ########
   			$success = array();
   			if($isUpdate){
   				//Henry
   				$sql = "DELETE FROM ADMISSION_STU_PREV_SCHOOL_INFO WHERE ApplicationID = '".$ApplicationID."'";
   				$success[] = $this->db_db_query($sql);
   				
   				$sql = "DELETE FROM 
   				    ADMISSION_CUST_INFO 
			    WHERE 
   				    ApplicationID = '".$ApplicationID."' 
		        AND 
			        Code in (
   				        'No_Brother_Sister', 
   				        'Brother_Sister_Rank', 
   				        'Contact_Person', 
   				        'specialEducationalNeeds', 
   				        'specialEducationalNeeds_Details',
   				        'appliedBefore',
   				        'appliedBefore_Details'
			        )
		        ";
   				$success[] = $this->db_db_query($sql);
   			}
   			
   			if($CurrentAttend1){
       			$sql = "INSERT INTO ADMISSION_STU_PREV_SCHOOL_INFO (
    						ApplicationID,
    						NameOfSchool,
    						Class,
       			            Year,
    						DateInput
       					) VALUES (
    						'".$ApplicationID."',
       						'{$CurrentAttend1}',
       						'{$Grades1Start} - {$Grades1End}',
       						'{$Year1Start} - {$Year1End}',
       						now()
       					)";
       			$success[] = $this->db_db_query($sql);
   			}
   			if($CurrentAttend2){
       			$sql = "INSERT INTO ADMISSION_STU_PREV_SCHOOL_INFO (
    						ApplicationID,
    						NameOfSchool,
    						Class,
       			            Year,
    						DateInput
       					) VALUES (
    						'".$ApplicationID."',
       						'{$CurrentAttend2}',
       						'{$Grades2Start} - {$Grades2End}',
       						'{$Year2Start} - {$Year2End}',
       						now()
       					)";
       			$success[] = $this->db_db_query($sql);
   			}
   			######## Update Old School Record END ########

   			######## Student Cust START ########
   			#### No. of Brother/Sister START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'No_Brother_Sister',
   				'Value' => $NoBrotherSister
   			), $ApplicationID);
   			#### No. of Brother/Sister END ####
   			
   			#### Brother/Sister Rank START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'Brother_Sister_Rank',
   				'Value' => $BrotherSisterRank
   			), $ApplicationID);
   			#### Brother/Sister Rank END ####
   			
   			#### Contact Person START ####
			foreach((array)$ContactPerson as $i=>$person){
				$success[] = $this->insertApplicationCustInfo(array(
					'Code' => 'Contact_Person',
					'Value' => $person,
					'Position' => $i
				), $ApplicationID);
			}
   			#### Contact Person END ####
   			
   			#### SpecialEducationalNeeds START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'specialEducationalNeeds',
   				'Value' => $specialEducationalNeeds
   			), $ApplicationID);
   			#### SpecialEducationalNeeds END ####
   			
   			#### SpecialEducationalNeeds_Details START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'specialEducationalNeeds_Details',
   				'Value' => ($specialEducationalNeeds)?$specialEducationalNeeds_Details:''
   			), $ApplicationID);
   			#### SpecialEducationalNeeds_Details END ####
   			
   			#### AppliedBefore START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'appliedBefore',
   				'Value' => $appliedBefore
   			), $ApplicationID);
   			#### AppliedBefore END ####
   			
   			#### AppliedBefore_Details START ####
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'appliedBefore_Details',
   				'Value' => ($appliedBefore)?$appliedBefore_Details:''
   			), $ApplicationID);
   			#### AppliedBefore_Details END ####
   			######## Student Cust END ########
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }

    function updateApplicationStudentInfoCust($Data){
    	global $UserID;
    	extract($Data);
    	
    	return $this->insertApplicationStudentInfoCust($Data, $applicationID,1);
    }
    
    function getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID,					
     			s.Class,
				s.Year,
				s.NameOfSchool 
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

    	return $this->returnArray($sql);
	}	
    
    function insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			if($isUpdate){
   				//Henry
   				$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE ApplicationID = '".$ApplicationID."'";
   				$success[] = $this->db_db_query($sql);
   			}
			if(
			    $OthersRelativeStudiedNameChi1 != '' || 
			    $OthersRelativeStudiedNameEng1 !='' || 
			    $OthersRelativeClassPosition1 !='' || 
			    $OthersRelativeGraduationYear1 != ''
		    ){
				$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,   
							EnglishName,   
							ClassPosition,
							Year,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeStudiedNameChi1."',
							'".$OthersRelativeStudiedNameEng1."',
							'".$OthersRelativeClassPosition1."',
							'".$OthersRelativeGraduationYear1."',
							'1',
							now())
				";
				$success[] = $this->db_db_query($sql);
   			}
			if(
			    $OthersRelativeStudiedNameChi2 != '' || 
			    $OthersRelativeStudiedNameEng2 !='' || 
			    $OthersRelativeClassPosition2 !='' || 
			    $OthersRelativeGraduationYear2 != ''
		    ){
				$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,   
							EnglishName,   
							ClassPosition,
							Year,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeStudiedNameChi2."',
							'".$OthersRelativeStudiedNameEng2."',
							'".$OthersRelativeClassPosition2."',
							'".$OthersRelativeGraduationYear2."',
							'2',
							now())
				";
				$success[] = $this->db_db_query($sql);
   			}
   			
   			if(
   				$OthersRelativeNameChi1 != ''||
   				$OthersRelativeNameEng1 != ''||
   				$OthersRelativeBirth1 !=''||
   				$OthersRelativeTwin1 !=''||
   				$OthersRelativeSchool1 !=''||
   				$OthersRelativeGrade1 !=''
   			){
   				$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,
							EnglishName,
							DOB,
   							IsTwins,
							SchoolName,
   							ClassPosition,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeNameChi1."',
							'".$OthersRelativeNameEng1."',
							'".$OthersRelativeBirth1."',
							'".$OthersRelativeTwin1."',
							'".$OthersRelativeSchool1."',
							'".$OthersRelativeGrade1."',
							'3',
							now())
				";
   				$success[] = $this->db_db_query($sql);
   			}
   			
   			if(
   					$OthersRelativeNameChi2 != ''||
   					$OthersRelativeNameEng2 != ''||
   					$OthersRelativeBirth2 !=''||
   					$OthersRelativeTwin2 !=''||
   					$OthersRelativeSchool2 !=''||
   					$OthersRelativeGrade2 !=''
   					){
   						$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,
							EnglishName,
							DOB,
   							IsTwins,
							SchoolName,
   							ClassPosition,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeNameChi2."',
							'".$OthersRelativeNameEng2."',
							'".$OthersRelativeBirth2."',
							'".$OthersRelativeTwin2."',
							'".$OthersRelativeSchool2."',
							'".$OthersRelativeGrade2."',
							'4',
							now())
				";
   				$success[] = $this->db_db_query($sql);
   			}
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }

    function updateApplicationRelativesInfoCust($Data){
    	global $UserID;
    	extract($Data);
    
    	$sql = "UPDATE ADMISSION_RELATIVES_AT_SCH_INFO
				SET
					ApplicationID = '{$ApplicationID}',
					Name = '{$RelativeStudent}',
					ClassPosition = '{$RelativeClassAttend}',
					Relationship = '{$RelativeRelationship}',
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
    	$success[] = $this->db_db_query($sql);
    
    	if($RelativeStudent == '' && $RelativeClassAttend =='' && $RelativeRelationship ==''){
    		$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE RecordID = '".$Data['RecordID']."'";
    		$success[] = $this->db_db_query($sql);
    	}
    	return !in_array(false, $success);
    }
    
    function getApplicationRelativesInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year year,
				r.Name name,		    					
     			r.ClassPosition classposition,
				r.EnglishName englishName,
				r.IsTwins istwins,
				r.DOB dob,
				r.SchoolName schoolName,
				r.Type type
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

    	return $this->returnArray($sql);
	}	
    
	function updateApplicationOtherInfo($data){    	
		global $UserID,$admission_cfg,$sys_custom;
   		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
		$Success[] =  $this->insertApplicationRelativesInfoCust($data, $applicationID,1);

			$sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '".$ApplicationID."' AND Code in ('HKUGA_Member', 'HKUGA_Member_Name', 'HKUGA_Member_No', 'HKUGA_Foundation_Member', 'HKUGA_Foundation_Member_Name')";

			$Success[] = $this->db_db_query($sql);
		
		#### HKUGA Member START ####
		$Success[] = $this->insertApplicationCustInfo(array(
				'Code' => 'HKUGA_Member',
				'Value' => $memberOfHKUGA
		), $ApplicationID);
		if($memberOfHKUGA =="1"){
			$Success[] = $this->insertApplicationCustInfo(array(
					'Code' => 'HKUGA_Member_Name',
					'Value' => $memberOfHKUGAName
			), $ApplicationID);
			 
			$Success[] = $this->insertApplicationCustInfo(array(
					'Code' => 'HKUGA_Member_No',
					'Value' => $memberOfHKUGANo
			), $ApplicationID);
		}
		#### HKUGA Member END ####
		
		#### HKUGA Education Foundation START ####
		$Success[] = $this->insertApplicationCustInfo(array(
				'Code' => 'HKUGA_Foundation_Member',
				'Value' => $memberOfHKUGAFoundation
		), $ApplicationID);
		if($memberOfHKUGAFoundation =="1" ){
			$Success[] = $this->insertApplicationCustInfo(array(
					'Code' => 'HKUGA_Foundation_Member_Name',
					'Value' => $memberOfHKUGAFoundationName
			), $ApplicationID);
		}
		#### HKUGA Education Foundation END ####
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }    
    function removeApplicationAttachment($data){
    	global $file_path;
    	extract($data);
    	if(empty($recordID)) return;
 		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";
    	$sql = "SELECT 
    				r.RecordID attachment_id,
    				r.AttachmentName attachment_name,
    				r.AttachmentType attachment_type
    			FROM 
    				ADMISSION_ATTACHMENT_RECORD r
    			INNER JOIN 
    				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
    			WHERE 
    				o.RecordID = '".$recordID."' 
    				".$cond."
    			";
    	$applicantAry = $this->returnArray($sql);
    
    	$Success = array();
    	for($i=0;$i<count($applicantAry);$i++){
    		$_attachmentName = $applicantAry[$i]['attachment_name'];
    		$_attachmentType = $applicantAry[$i]['attachment_type']=='personal_photo'? $applicantAry[$i]['attachment_type']:'other_files';
    		$_attachmentId = $applicantAry[$i]['attachment_id'];
    		$image_url = $this->filepath.$recordID.'/'.$_attachmentType.'/'.$_attachmentName;	
    		
    		if(file_exists($file_path.$image_url)){
    			unlink($file_path.$image_url);
    			$sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '".$_attachmentId."'";
    			
    			$Success[] = $this->db_db_query($sql);
    		}
    	}
    	if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }
    function saveApplicationAttachment($data){
    	global $UserID;
    	extract($data);
 		if(empty($recordID)) return;
    	$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '".$recordID."'";
    	$applicationID = current($this->returnVector($sql));
    	
    	if(!empty($applicationID)&&!empty($attachment_name)&&!empty($attachment_type)){
    		$result = $this->removeApplicationAttachment($data);
    		if($result){
	    		$sql = "INSERT INTO 
	    					ADMISSION_ATTACHMENT_RECORD 
	    						(ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy)
	    					VALUES
	    						('".$applicationID."','".$attachment_type."','".$attachment_name."',NOW(),'".$UserID."')
	    				";
	    		return $this->db_db_query($sql);
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
 	function updateApplicationStudentInfo($data, $isAdminUpdate = false){
 		global $kis_lang, $Lang;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
 		return $this->insertApplicationStudentInfo($data, $applicationID, 1);
	}    
 	
	function getApplicationAttachmentRecord($schoolYearID,$data=array()){
		extract($data);
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";		
		$sql = "
			SELECT
				 o.RecordID folder_id,
     			 r.RecordID,
			     r.ApplicationID applicationID,
			     r.AttachmentType attachment_type,
			     r.AttachmentName attachment_name,
			     r.DateInput dateinput,
			     r.InputBy inputby
			FROM
				ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID						
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			ORDER BY r.AttachmentType, r.RecordID desc
    	";
    	$result = $this->returnArray($sql);
    	$attachmentAry = array();
    	for($i=0;$i<count($result);$i++){
    		$_attachType = $result[$i]['attachment_type'];
     		$_attachName = $result[$i]['attachment_name'];   
     		$_applicationId = $result[$i]['applicationID'];  
     		$_folderId = $result[$i]['folder_id'];
     		$attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;		
     		$attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId.'/'.($_attachType=='personal_photo'?$_attachType:'other_files').'/'.$_attachName;	
     		
    	}
		return $attachmentAry;
	}
	function saveApplicationParentInfo($data){
		extract($data);
		$Success[] = $this->updateApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G1ChineseName,
				'EnglishName' => $G1EnglishName,
				'Mobile' => $G1MobileNo,
				'Email' => $G1Email,
				'JobTitle' => $G1Occupation,
				'Company' => $G1Institution
		),'F');
		$Success[] = $this->updateApplicationParentInfo(array(
				'ApplicationID' => $ApplicationID,
				'ChineseName' => $G2ChineseName,
				'EnglishName' => $G2EnglishName,
				'Mobile' => $G2MobileNo,
				'Email' => $G2Email,
				'JobTitle' => $G2Occupation,
				'Company' => $G2Institution,
				'PG_TYPE' => 'M'
		),'M');
		if($G3Name){
			$Success[] = $this->updateApplicationParentInfo(array(
					'ApplicationID' => $ApplicationID,
					'ChineseName' => $G3Name,
					'Relationship' => $G3Relationship
			),'G');
		}
		######## Parent Info END ########
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationStudentInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
 		for($i=0; $i<count($OthersPrevSchYear); $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_cust_id[$i]=='new'){
				$parentInfoAry['OthersPrevSchYear'.($i+1)] = $OthersPrevSchYear[$i];
				$parentInfoAry['OthersPrevSchClass'.($i+1)] = $OthersPrevSchClass[$i];
				$parentInfoAry['OthersPrevSchName'.($i+1)] = $OthersPrevSchName[$i];
				$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry);
			}else{
				$parentInfoAry['Year'] = $OthersPrevSchYear[$i];
				$parentInfoAry['Class'] = $OthersPrevSchClass[$i];
				$parentInfoAry['NameOfSchool'] = $OthersPrevSchName[$i];
				$parentInfoAry['RecordID'] = $student_cust_id[$i];	
				$Success[] = $this->updateApplicationStudentInfoCust($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationRelativesInfoCust($data){
		extract($data);
		
		######## Relative Info START ########
 		$relativesInfoAry = array();
		$relativesInfoAry['ApplicationID'] = $ApplicationID;
		$relativesInfoAry['RelativeStudent'] = $RelativeStudent;
		$relativesInfoAry['RelativeClassAttend'] = $RelativeClassAttend;
		$relativesInfoAry['RelativeRelationship'] = $RelativeRelationship;
		$relativesInfoAry['RecordID'] = $RelativesID;	
		if($RelativesID == ''){
			$Success[] = $this->insertApplicationRelativesInfoCust($relativesInfoAry, $ApplicationID);
		}else{
			$Success[] = $this->updateApplicationRelativesInfoCust($relativesInfoAry);
		}
		######## Relative Info END ########

		######## Cust Info START ########
		$success[] = $this->updateApplicationCustInfo(array(
			'filterApplicationId' => $ApplicationID,
			'filterCode' => 'Referee_Name',
			'Value' => $Referee_Name
		));
		######## Cust Info END ########
		
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	   
	function getApplicationOthersInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND RecordID='".$recordID."'":"";

		$sql = "
			SELECT 
				RecordID,
				ApplicationID applicationID,
				ApplyYear schoolYearId,
				ApplyDayType1,
				ApplyDayType2,
				ApplyDayType3,
				ApplyLevel classLevelID,
				DateInput,
				DateModified,
				ModifiedBy,
				InterviewSettingID,
				InterviewSettingID2,
				InterviewSettingID3,
				SiblingAppliedName,
			 	ExBSName,
			 	ExBSName2,
				ExBSName3,
			 	ExBSGradYear,
			 	ExBSGradYear2,
			 	ExBSGradYear3,
				Remarks,
				IsConsiderAlternative OthersIsConsiderAlternative
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	return $this->returnArray($sql);
	}		 
	function getApplicationParentInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND pg.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				pg.RecordID,
     			pg.ApplicationID applicationID,
     			pg.ChineseName chinesename,
				pg.EnglishName englishname,
				pg.Relationship relationship,
     			pg.PG_TYPE type,
				pg.Company companyname,
     			pg.JobTitle occupation,
     			pg.Mobile mobile,
				pg.Email email,
     			o.ApplyLevel classLevelID
			FROM
				ADMISSION_PG_INFO pg
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";

    	return $this->returnArray($sql);
	}	
	function getApplicationStatus($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			s.ApplicationID applicationID,
     			s.ReceiptID receiptID, 
			    IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
			    IF(s.Handler,".getNameFieldByLang2("iu.").",'') AS handler,
			    s.Handler handler_id,
				s.InterviewDate As interviewdate,
				s.InterviewLocation As interviewlocation,
			    s.Remark remark,CASE ";
		foreach($admission_cfg['Status'] as $_key => $_value){
				$sql .= "WHEN s.Status = ".$_value." THEN '".strtolower($_key)."' ";
		}	    
		$sql .= " ELSE s.Status END status,";
		$sql .= "
			    CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
			    s.DateInput dateinput,
			    s.InputBy inputby,
			    s.DateModified datemodified,
			    s.ModifiedBy modifiedby,
			    o.ApplyLevel classLevelID,
				s.FeeBankName,
				s.FeeChequeNo,
				IF(pi.ApplicationID IS NOT NULL , 'OnlinePayment' , IF(s.Status=2, 'OtherPayment' , 'N/A')  ) as OnlinePayment
			FROM
				ADMISSION_APPLICATION_STATUS s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			LEFT JOIN
				INTRANET_USER iu ON s.Handler = iu.UserID
			LEFT JOIN
				ADMISSION_PAYMENT_INFO pi ON o.ApplicationID = pi.ApplicationID and pi.payment_status = 'Completed'
			WHERE
				s.SchoolYearID = '".$schoolYearID."'	
			".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

// 		if($type == "personal_photo"){
// 			return true;
// 		}
			if (!empty($file['tmp_name'])) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							ini_set('memory_limit','256M');
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							if($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
								$image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
							else
								$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}else{
				if($this->isInternalUse($_REQUEST['token'])){
					return true;
				}
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	function moveUploadedAttachment($tempFolderPath, $destFolderPath){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/personal_photo", $destFolderPath);
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/other_files", $destFolderPath);
		$uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);
		
		if(in_array(false, $uploadSuccess))
			return false;
		else
			return true;
	}
	//Siuwan 20131018 Copy from libstudentregistry.php, e.g.$this->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	public function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
		$result = $this->returnArray($sql);
		return getSelectByArray($result, "name=".$selection_name, $code_selected,0,1);
	}
	//Henry 20131018 Copy from libstudentregistry.php
	public function returnPresetCodeName($code_type="", $selected_code="")
	{
		$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
		$result = $this->returnVector($sql);
		return $result[0];
	}
	function displayWarningMsg($warning){
		global $kis_lang;
		$x = '
		 <fieldset class="warning_box">
			<legend>'.$kis_lang['warning'].'</legend>
			<ul>
				<li>'.$kis_lang['msg'][$warning].'</li>
			</ul>
		</fieldset>
		
		';
		return $x;
	}
		
	function insertAttachmentRecord($AttachmentType,$AttachmentName, $ApplicationID){
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
		   				 ApplicationID,
		  				 AttachmentType,   
		   				 AttachmentName,
		  				 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$AttachmentType."',
						'".$AttachmentName."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	//old method
	function newApplicationNumber($schoolYearID=""){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$defaultNo = "PL".$year."-a0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%' order by ApplicationID desc";
		$result = $this->returnArray($sql);
		
		if($result){			
			$num = substr($result[0]['ApplicationID'], -4);
			$num++;
			$num = sprintf("%04s", $num);
		}
		
		$newNo = $prefix.$num;
		
		return $newNo;
	}
	
	function newApplicationNumber2($schoolYearID="",$classLevelID="", $isImport=false){
		global $Lang;
		$yearStart = date('y',getStartOfAcademicYear('',($schoolYearID?$schoolYearID:$this->schoolYearID)));
		if($this->isFirstYear($classLevelID)){
			$defaultNo = 'N';
		}else{
			$defaultNo = 'T';
		}
		if($isImport){
			$defaultNo .= 'H';
		}else{
			$defaultNo .= 'W';
		}
		$defaultNo .= $yearStart.($yearStart+1)."0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
		
		if($this->returnArray($sql)){
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( SUBSTRING( ApplicationID, '".(strlen($prefix)+1)."' ) , UNSIGNED ) ) +1, 4, '0')) 
					FROM ADMISSION_OTHERS_INFO 
					WHERE ApplicationID like '".$prefix."%'";
		}
		else{
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					Values ('".$defaultNo."')";
		}
			$result = $this->db_db_query($sql);
					
			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
			$newNo = $this->returnArray($sql);
			return $newNo[0]['ApplicationID'];
	}
	function getAttachmentByApplicationID($schoolYearID,$applicationID){
		global $file_path;
		$attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID,array("applicationID"=>$applicationID));
		$attachment = array();
		foreach((array)$attachmentAry[$applicationID] as $_type => $_attachmentAry){
			$_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
			if(!empty($_thisAttachment)){
				$_thisAttachment = $this->filepath.$_thisAttachment; 
				if(file_exists($file_path.$_thisAttachment)){
					$attachment[$_type]['link'] = $_thisAttachment;
				}else{
					$attachment[$_type]['link'] = false;
				}
			}else{
				$attachment[$_type]['link'] = false;
			}
			
		}
		return $attachment;
	}

	public function getApplicationNumber($RecordIDAry){
		$sql = 'SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID IN ("'.implode('","',$RecordIDAry).'")';
		$result = $this->returnVector($sql);
		return $result;
	}
	
	public function getPrintLink($schoolYearID="", $applicationID , $type=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?id='.$id;
	}
 	function getClassLevel($ClassLevel=''){
 		global $intranet_root;
    	include_once($intranet_root."/includes/form_class_manage.php");
    	$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
    	$numOfForm = count($FormArr);
    	$classLevelName = array();
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisClassLevelID = $FormArr[$i]['YearID'];
			$thisLevelName = $FormArr[$i]['YearName'];
			$classLevelName[$thisClassLevelID] = $thisLevelName;
		}
		return $classLevelName;
    }	
	function getBasicSettings($schoolYearID='',$SettingNameAry=array()){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
    	$sql = "
			SELECT
			    SettingName,
			    SettingValue
			FROM
				ADMISSION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'	
    	";
    	if (sizeof($SettingNameAry) > 0)  {
			$sql .= " AND 
						SettingName in ('".implode("','",$SettingNameAry)."')";
		}
    	$setting = $this->returnArray($sql);
		for ($i=0; $i< sizeof($setting); $i++) {
			$Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
		}
		return $Return;    	
    }
   	function saveBasicSettings($schoolYearID='',$SettingNameValueAry=array()){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$this->Start_Trans();
		
		$SettingsNameAry = array_keys($SettingNameValueAry);
		
		$result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID,$SettingsNameAry);
		$result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID,$SettingNameValueAry);
		
		if(in_array(false, $result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	 
	function removeBasicSettings($schoolYearID,$SettingsNameAry){
		if(count($SettingsNameAry)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameAry)."'";
		
		$sql = "
			DELETE FROM
				ADMISSION_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
			AND SchoolYearID = '".$schoolYearID."'
		";
		
		return $this->db_db_query($sql);
	}
	
	function insertBasicSettings($schoolYearID,$SettingNameValueAry){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		
		foreach((array)$SettingNameValueAry as $_settingName => $_settingValue)
		{
			$InsertSqlArr[] = "('".$schoolYearID."','".$_settingName."','".$_settingValue."', '".$this->uid."', NOW(), '".$this->uid."', NOW())";
		}
		
		if(count($InsertSqlArr)>0)
		{	
			$InsertSql = implode(',',$InsertSqlArr);	
			
			$sql = "
				INSERT INTO	ADMISSION_SETTING
					(SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)	 
				VALUES
					$InsertSql
			";
			
			return $this->db_db_query($sql);
			
		}
		else
			return false;
		
			
	}
//    function getApplicationSetting($schoolYearID=''){
//    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
//
//    	$sql = "
//			SELECT
//     			ClassLevelID,
//			    IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d %H:%i'),'') As StartDate,
//				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d %H:%i'),'') As EndDate,
//				IF(DOBStart,DATE_FORMAT(DOBStart,'%Y-%m-%d'),'') As DOBStart,
//				IF(DOBEnd,DATE_FORMAT(DOBEnd,'%Y-%m-%d'),'') As DOBEnd,
//			    DayType,
//			    FirstPageContent,
//			    LastPageContent,
//			    DateInput,
//			    InputBy,
//			    DateModified,
//			    ModifiedBy
//			FROM
//				ADMISSION_APPLICATION_SETTING
//			WHERE
//				SchoolYearID = '".$schoolYearID."'
//    	";
//    	$setting = $this->returnArray($sql);
//    	$applicationSettingAry = BuildMultiKeyAssoc($setting, 'ClassLevelID');
//    	$applicationPeriodAry = array();
//    	foreach($this->classLevelAry as $_classLevelId => $_classLevelName){ 	
//    		$_startdate = $applicationSettingAry[$_classLevelId]['StartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['StartDate']:'';	
//    		$_enddate = $applicationSettingAry[$_classLevelId]['EndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['EndDate']:'';	
//    		
//    		$applicationPeriodAry[$_classLevelId] = array(
//    													'ClassLevelName'=>$_classLevelName,
//      													'StartDate'=>$_startdate,  	
//      													'EndDate'=>$_enddate,
//      													'DOBStart'=>$applicationSettingAry[$_classLevelId]['DOBStart'],
//      													'DOBEnd'=>$applicationSettingAry[$_classLevelId]['DOBEnd'],
//      													'DayType'=>$applicationSettingAry[$_classLevelId]['DayType'],   
//      													'FirstPageContent'=>$applicationSettingAry[$_classLevelId]['FirstPageContent'],  
//      													'LastPageContent'=>$applicationSettingAry[$_classLevelId]['LastPageContent']      													  																									
//    												);
//    	}
//    	return $applicationPeriodAry;    	
//    } 
//	public function updateApplicationSetting($data){
//   		extract($data);
//   		#Check exist setting
//   		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
//   		$cnt = current($this->returnVector($sql));
//
//   		if($cnt){//update
//   			$sql = "UPDATE ADMISSION_APPLICATION_SETTING SET 
//		   				 StartDate = '".$startDate."',
//		  				 EndDate = '".$endDate."', 
//						 DOBStart = '".$dOBStart."',
//		  				 DOBEnd = '".$dOBEnd."',  
//		   				 DayType = '".$dayType."',
//		  				 FirstPageContent = '".$firstPageContent."',   
//		   				 LastPageContent = '".$lastPageContent."',
//		   				 DateModified = NOW(),
//		   				 ModifiedBy = '".$this->uid."'
//   					WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
//   		}else{//insert
//   			$sql = "INSERT INTO ADMISSION_APPLICATION_SETTING (
//   						SchoolYearID,
//   						ClassLevelID,
//					    StartDate,
//					    EndDate,
//					    DayType,
//		  				FirstPageContent,   
//		   				LastPageContent,
//					    DateInput,
//					    InputBy,
//					    DateModified,
//					    ModifiedBy) 
//					VALUES (
//					    '".$schoolYearID."',
//					    '".$classLevelID."',
//					    '".$startDate."',
//					    '".$endDate."',	
//					    '".$dayType."',
//					    '".$firstPageContent."',
//					    '".$lastPageContent."',	
//					    NOW(),
//					    '".$this->uid."',
//					     NOW(),
//					    '".$this->uid."')
//			";
//   		}
//   		return $this->db_db_query($sql);
//    }    
    function getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$recordID=''){
		$cond = !empty($applicationID)?" AND stu.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($status)?" AND s.status='".$status."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			stu.ApplicationID applicationID,
     			o.ApplyLevel classLevelID,
     			o.ApplyYear schoolYearID,
     			stu.ChineseName student_name_b5,
      			stu.EnglishName student_name_en, 
      			stu.Gender gender,
				stu.BirthCertType birthcerttype,
      			IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,	
      			stu.PlaceOfBirth placeofbirth,
      			stu.County county,
      			stu.HomeTelNo homephoneno,
      			stu.BirthCertNo birthcertno,
				stu.BirthCertType birthcerttype,
				stu.BirthCertTypeOther birthcerttypeother,
				stu.LangSpokenAtHome langspokenathome,
      			stu.Email email,
      			stu.Email2 email2,
      			stu.LastSchool lastschool,
        		stu.AddressRoom,
        		stu.AddressFloor,
        		stu.AddressBlock,
        		stu.AddressBldg,
        		stu.AddressEstate,
        		stu.AddressStreet,
        		stu.AddressDistrict,
				stu.Address,
				stu.Age age,		
     			".getNameFieldByLang2("stu.")." AS student_name,
				stu.IsTwinsApplied istwinsapplied,	
				stu.TwinsApplicationID twinsapplicationid
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID							
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	$studentInfoAry = $this->returnArray($sql);
		return $studentInfoAry;
	}
	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
	
		if($paymentStatus==$admission_cfg['PaymentStatus']['OnlinePayment']){
			$paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NOT NULL  ";
		}
		else if($paymentStatus==$admission_cfg['PaymentStatus']['OtherPayment']){
			$paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NULL  ";
		}
		
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'
					OR pg.Mobile LIKE '%".$keyword."%'
					OR stu.BirthCertNo LIKE '%".$keyword."%'
					OR stu.HomeTelNo LIKE '%".$keyword."%'
					OR stu.LangSpokenAtHome LIKE '%".$keyword."%'				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			$paymentStatus_cond	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
    	
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				stu.LangSpokenAtHome AS langspokenathome, 
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}
	
	function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1){
		global $admission_cfg;
		
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		
		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sort = $sortby? "$sortby $order":"application_id";
		
//		if(!empty($keyword)){
//			$cond .= "
//				AND ( 
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'					
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'				
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')."
			INNER JOIN 
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				1 	
			".$cond." 
			order by ".$sort."
    	";
    	$sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			stu.ChineseName AS student_name_b5,
				stu.EnglishName AS student_name_en,
				".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status, stu.BirthCertNo, stu.TwinsApplicationID, stu.LangSpokenAtHome ".$from_table." ";
     	//debug_r($sql);
    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}
	   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function checkImportDataForImportAdmissionHeader($csv_header, $lang = ''){
		//$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
		$file_format = $this->getExportHeader();
		$file_format = $file_format[1];
		# check csv header
		$format_wrong = false;
		
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($csv_header[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		
		return $format_wrong;
	}
	
	public function returnPresetCodeAndNameArr($code_type="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function checkIsChinese($str){
	    return true;
	    //return preg_match('/[\x{4e00}-\x{9fa5}]+/u', $str);
	}
	function checkIsEnglish($str){
	    return true;
	    //return preg_match('/^[A-Za-z ]*$/', $str);
	}
	function checkIsEmpty($str){
	    return (
        	(strtolower(trim($str))=='沒有') || 
        	(strtolower(trim($str))=='nil') ||
        	(strtolower(trim($str))=='n.a.')
    	);
	}
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$allAdmissionNumber = array();

		$admission_year = getAcademicYearByAcademicYearID($this->getNextSchoolYearID());
		$admission_year_start = substr($admission_year, 0, 4);
		
		//// Get all Birth Cert No START ////
		$sql = "SELECT asi.ApplicationID, asi.BirthCertNo FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID JOIN ADMISSION_APPLICATION_STATUS as aas ON asi.ApplicationID = aas.ApplicationID WHERE aoi.ApplyYear = '".$this->getNextSchoolYearID()."' AND aas.Status = 2";
		$rs = $this->returnResultSet($sql);
		$allBirthCertNo = BuildMultiKeyAssoc($rs, array('ApplicationID') , array('BirthCertNo'), $SingleValue=1);
		//// Get all Birth Cert No END ////
		
		$i=0;
		foreach($data as $aData){
			
			//valid Admission Date
			if($aData[0]){
    			$aData[0] = getDefaultDateFormat($aData[0]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[0]) ) {
    				list($year , $month , $day) = explode('-',$aData[0]);
    		       	$resultArr[$i]['validAdmissionDate'] = checkdate($month , $day , $year);
    			}
    			else{
    				$resultArr[$i]['validAdmissionDate'] = false;
    			}
			}else{
				$resultArr[$i]['validAdmissionDate'] = true;
			}
			
			//valid Admission number
			$resultArr[$i]['validAdmissionNo'] = !in_array($aData[1], $allAdmissionNumber);
			
			//valid Chinese Name
			if($aData[3] == ''){
			    $resultArr[$i]['validChineseNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validChineseNameEmpty'] = true;
			    
			    if($this->checkIsChinese($aData[3]) || $this->checkIsEmpty($aData[3])){
			        $resultArr[$i]['validChineseName'] = true;
			    }else{
			        $resultArr[$i]['validChineseName'] = false;
			    }
			}
			
			//valid English Name
			if($aData[4] == ''){
			    $resultArr[$i]['validEnglishNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validEnglishNameEmpty'] = true;
			    
			    if($this->checkIsEnglish($aData[4]) || $this->checkIsEmpty($aData[4])){
			        $resultArr[$i]['validEnglishName'] = true;
			    }else{
			        $resultArr[$i]['validEnglishName'] = false;
			    }
			}
			
			//valid Date of birth
			if($aData[5] == ''){
			    $resultArr[$i]['validDateOfBirthEmpty'] = false;
			}else{
			    $resultArr[$i]['validDateOfBirthEmpty'] = true;
    			$aData[5] = getDefaultDateFormat($aData[5]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[5]) ) {
    				list($year , $month , $day) = explode('-',$aData[5]);
    		       	$resultArr[$i]['validDateOfBirth'] = checkdate($month , $day , $year);
    			}
    			else{
    				$resultArr[$i]['validDateOfBirth'] = false;
    			}
			}
			
			//valid Gender
			if($aData[6]){
    			if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
    				$aData[6] = 'M';
    				$data[$i][6] = 'M';
    			}
    			else if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
    				$aData[6] = 'F';
    				$data[$i][6] = 'F';
    			}
    			if ( strtoupper($aData[6]) == 'M' || strtoupper($aData[6]) == 'F') {
    		       	$resultArr[$i]['validGender'] = true;
    			}
    			else{
    				$resultArr[$i]['validGender'] = false;
    			}
			}else{
			    $resultArr[$i]['validGender'] = true;
			}
			
			//valid Place of birth
			if($aData[7] == ''){
				$resultArr[$i]['validPlaceOfBirthEmpty'] = false;
			}else{
			    $resultArr[$i]['validPlaceOfBirthEmpty'] = true;
			}
			
			//valid birth cert
    		$checkBirthCertType = array(
    		    strtoupper($kis_lang['Admission']['BirthCertType']['hk2']), 
    		    strtoupper($kis_lang['Admission']['HKUGAPS']['birthcertno'])
    		);
    		if(in_array( strtoupper($aData[8]), $checkBirthCertType )){
    			if (
    			    preg_match('/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/',$aData[9]) &&
    			    $this->checkBirthCertNo($aData[9])
			    ) {
    				$resultArr[$i]['validBirthCertNoType'] = true;
    			}else{
    				$resultArr[$i]['validBirthCertNoType'] = false;
    			}
    			
    			if (
    			    $allBirthCertNo[$aData[1]] != $aData[9] && 
    			    $this->hasBirthCertNumber($aData[9], $_REQUEST['classLevelID'], ($_REQUEST['schoolYearID']?$_REQUEST['schoolYearID']:$this->schoolYearID))
			    ) {
    				$resultArr[$i]['validBirthCertNo'] = false;
    			}else{
    				$resultArr[$i]['validBirthCertNo'] = true;
    			}
    		}else{
				$resultArr[$i]['validBirthCertNoType'] = true;
				$resultArr[$i]['validBirthCertNo'] = true;
    		}
			
			
			//valid SpokenLanguageForInterview
			$resultArr[$i]['validSpokenLanguageForInterview'] = true;
			if($aData[10]){
    			$resultArr[$i]['validSpokenLanguageForInterview'] = false;
    			foreach($kis_lang['Admission']['Languages'] as $lang => $langStr){
    				if($aData[10] == $langStr){
    					$resultArr[$i]['validSpokenLanguageForInterview'] = true;
    					break;
    				}
    			}
			}
			$resultArr[$i]['validAddressStreetEmpty'] = true;
			$resultArr[$i]['validAddressDistrictEmpty'] = true;
			$resultArr[$i]['validAddressDistrict'] = true;
		    $resultArr[$i]['validAddressAreaEmpty'] = true;
			/* 2019-04-15 Pun disable address checking request by siena
			//valid AddressStreet
			if($aData[16] == ''){
			    $resultArr[$i]['validAddressStreetEmpty'] = false;
			}else{
			    $resultArr[$i]['validAddressStreetEmpty'] = true;
			}
			
			//valid AddressDistrict
			if($aData[17] == ''){
			    $resultArr[$i]['validAddressDistrictEmpty'] = false;
			}else{
			    $resultArr[$i]['validAddressDistrictEmpty'] = true;
    			$resultArr[$i]['validAddressDistrict'] = false;
    			foreach($admission_cfg['AddressDistrict'] as $_key => $addrStr){
    				if($aData[17] == $addrStr){
    					$resultArr[$i]['validAddressDistrict'] = true;
    					break;
    				}
    			}
			}

			//valid AddressArea
			if($aData[18] == ''){
			    $resultArr[$i]['validAddressAreaEmpty'] = false;
			}else{
			    $resultArr[$i]['validAddressAreaEmpty'] = true;
			}
			*/
			
			//valid Tel Home
			if($aData[19] !== ''){
    			if (preg_match('/^[0-9]*$/',$aData[19])) {
    				$resultArr[$i]['validTelHome'] = true;
    			}
    			else{
    				$resultArr[$i]['validTelHome'] = false;
    			}
			}else{
			    $resultArr[$i]['validTelHome'] = true;
			}
			
			//valid contact email
			if($aData[20]){
    			if (preg_match('/\S+@\S+\.\S+/',$aData[20])) {
    				$resultArr[$i]['validEmail'] = true;
    			}
    			else{
    				$resultArr[$i]['validEmail'] = false;
    			}
			}else{
			    $resultArr[$i]['validEmail'] = true;
			}
			if($aData[21]){
    			if (preg_match('/\S+@\S+\.\S+/',$aData[21])) {
    				$resultArr[$i]['validEmail'] = true;
    			}
    			else{
    				$resultArr[$i]['validEmail'] = false;
    			}
			}else{
			    $resultArr[$i]['validEmail'] = true;
			}
			
			if($aData[22] !== ''){
    			//valid NoBrotherSister
    			if (preg_match('/\d*/',$aData[22])) {
    				$resultArr[$i]['validNoBrotherSister'] = true;
    			}
    			else{
    				$resultArr[$i]['validNoBrotherSister'] = false;
    			}
    			
    			//valid BrotherSisterRank
    			if (preg_match('/\d*/',$aData[23])) {
    				$resultArr[$i]['validBrotherSisterRank'] = true;
    			}
    			else{
    				$resultArr[$i]['validBrotherSisterRank'] = false;
    			}
			}else{
				$resultArr[$i]['validNoBrotherSister'] = true;
				$resultArr[$i]['validBrotherSisterRank'] = true;
			}
			
			//valid ContactPerson
		    $resultArr[$i]['validContactPerson'] = true;
			if(!empty($aData[24])){
    			$validContactPerson = array(
    			    $kis_lang['Admission']['HKUGAPS']['father'],
    			    $kis_lang['Admission']['HKUGAPS']['mother'],
    			    $kis_lang['Admission']['HKUGAPS']['guardian']
    			);
    			$contactPersion = explode(',', $aData[24]);
    			foreach((array)$contactPersion as $persion){
    			    $persion = trim($persion);
    			    if(!in_array($persion, $validContactPerson)){
    			        $resultArr[$i]['validContactPerson'] = false;
    			        break;
    			    }
    			}
			}

			//valid specialEducationalNeeds
			if($aData[25]){
			    if(strtoupper($aData[25]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[25]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validSpecialEducationalNeeds'] = true;
			    }
			    else{
    				$resultArr[$i]['validSpecialEducationalNeeds'] = false;
			    }
			}else{
			    $resultArr[$i]['validSpecialEducationalNeeds'] = true;
			}

			//valid appliedBefore
			if($aData[27]){
			    if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validAppliedBefore'] = true;
			    }
			    else{
    				$resultArr[$i]['validAppliedBefore'] = false;
			    }
			}else{
			    $resultArr[$i]['validAppliedBefore'] = true;
			}
			
			if($aData[29]){
//			    //valid CurrentAttend1 Grade
//    			if (preg_match('/.+-.+/',$aData[25])) {
//    				$resultArr[$i]['validCurrentAttendGrade1'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendGrade1'] = false;
//    			}
//    			
//    			//valid CurrentAttend1 Year
//    			if (preg_match('/\d{4} ?- ?\d{4}/',$aData[26])) {
//    				$resultArr[$i]['validCurrentAttendYear1'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendYear1'] = false;
//    			}
			    //valid CurrentAttend1 Grade
    			if($aData[30] && $aData[31]){
    				$resultArr[$i]['validCurrentAttendGrade1'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendGrade1'] = false;
    			}
    			//valid CurrentAttend1 Year
    			if (preg_match('/\d{4}/',$aData[32]) && preg_match('/\d{4}/',$aData[33])){
    				$resultArr[$i]['validCurrentAttendYear1'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendYear1'] = false;
    			}
			}else{
				$resultArr[$i]['validCurrentAttendGrade1'] = true;
				$resultArr[$i]['validCurrentAttendYear1'] = true;
			}
			
			if($aData[34] !== ''){
//    			//valid CurrentAttend2 Grade
//    			if (preg_match('/.+-.+/',$aData[28])) {
//    				$resultArr[$i]['validCurrentAttendGrade2'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendGrade2'] = false;
//    			}
//    			
//    			//valid CurrentAttend2 Year
//    			if (preg_match('/\d{4} ?- ?\d{4}/',$aData[29])) {
//    				$resultArr[$i]['validCurrentAttendYear2'] = true;
//    			}
//    			else{
//    				$resultArr[$i]['validCurrentAttendYear2'] = false;
//    			}
				//valid CurrentAttend1 Grade
    			if ($aData[35] && $aData[36]){
    				$resultArr[$i]['validCurrentAttendGrade2'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendGrade2'] = false;
    			}
    			//valid CurrentAttend1 Year
    			if (preg_match('/\d{4}/',$aData[37]) && preg_match('/\d{4}/',$aData[38])){
    				$resultArr[$i]['validCurrentAttendYear2'] = true;
    			}
    			else{
    				$resultArr[$i]['validCurrentAttendYear2'] = false;
    			}
			}else{
				$resultArr[$i]['validCurrentAttendGrade2'] = true;
				$resultArr[$i]['validCurrentAttendYear2'] = true;
			}
			
			//valid twins
			if($aData[39]){
    			if(strtoupper($aData[39]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[39]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validTwins'] = true;
    			}
    			else{
    				$resultArr[$i]['validTwins'] = false;
    			}
			}else{
				$resultArr[$i]['validTwins'] = true;
			}
			
			//// Parent ////
			//valid G1ChineseName
			if($aData[41] == ''){
				$resultArr[$i]['validG1ChineseNameEmpty'] = false;
			}else{
				$resultArr[$i]['validG1ChineseNameEmpty'] = true;
			    
			    if($this->checkIsChinese($aData[41]) || $this->checkIsEmpty($aData[41])){
			        $resultArr[$i]['validG1ChineseName'] = true;
			    }else{
			        $resultArr[$i]['validG1ChineseName'] = false;
			    }
			}
			//valid G1EnglishName
			if($aData[42] == ''){
				$resultArr[$i]['validG1EnglishNameEmpty'] = false;
			}else{
				$resultArr[$i]['validG1EnglishNameEmpty'] = true;
			    
			    if($this->checkIsEnglish($aData[42]) || $this->checkIsEmpty($aData[42])){
			        $resultArr[$i]['validG1EnglishName'] = true;
			    }else{
			        $resultArr[$i]['validG1EnglishName'] = false;
			    }
			}
			
			if($aData[41]){
			    //valid G1MobileNo
    			if (
    			    preg_match('/^[0-9]*$/',$aData[43]) ||
    			    $this->checkIsEmpty($aData[43]) || 
    			    $aData[43] == ''
			    ) {
    				$resultArr[$i]['validG1MobileNo'] = true;
    			}
    			else{
    				$resultArr[$i]['validG1MobileNo'] = false;
    			}
    			
    			//valid G1Email
    			if (
    			    preg_match('/\S+@\S+\.\S+/',$aData[44]) ||
    			    $this->checkIsEmpty($aData[44]) || 
    			    $aData[44] == ''
			    ) {
    				$resultArr[$i]['validG1Email'] = true;
    			}
    			else{
    				$resultArr[$i]['validG1Email'] = false;
    			}
			}else{
				$resultArr[$i]['validG1MobileNo'] = true;
				$resultArr[$i]['validG1Email'] = true;
			}

			//valid G2ChineseName
			if($aData[47] == ''){
			    $resultArr[$i]['validG2ChineseNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validG2ChineseNameEmpty'] = true;
			    
			    if($this->checkIsChinese($aData[47]) || $this->checkIsEmpty($aData[47])){
			        $resultArr[$i]['validG2ChineseName'] = true;
			    }else{
			        $resultArr[$i]['validG2ChineseName'] = false;
			    }
			}
			//valid G2EnglishName
			if($aData[48] == ''){
			    $resultArr[$i]['validG2EnglishNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validG2EnglishNameEmpty'] = true;
			    
			    if($this->checkIsEnglish($aData[48]) || $this->checkIsEmpty($aData[48])){
			        $resultArr[$i]['validG2EnglishName'] = true;
			    }else{
			        $resultArr[$i]['validG2EnglishName'] = false;
			    }
			}
			
			if($aData[47]){
    			//valid G2MobileNo
    			if (
    			    preg_match('/^[0-9]*$/',$aData[49]) ||
    			    $this->checkIsEmpty($aData[49]) || 
    			    $aData[49] == ''
			    ) {
    				$resultArr[$i]['validG2MobileNo'] = true;
    			}
    			else{
    				$resultArr[$i]['validG2MobileNo'] = false;
    			}
    			
    			//valid G2Email
    			if (
    			    preg_match('/\S+@\S+\.\S+/',$aData[50]) ||
    			    $this->checkIsEmpty($aData[50]) || 
    			    $aData[50] == ''
			    ) {
    				$resultArr[$i]['validG2Email'] = true;
    			}
    			else{
    				$resultArr[$i]['validG2Email'] = false;
    			}
			}else{
				$resultArr[$i]['validG2MobileNo'] = true;
				$resultArr[$i]['validG2Email'] = true;
			}
			
			//// Other ////
			//valid memberOfHKUGA
			if($aData[55]){
    			if(strtoupper($aData[55]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[55]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validMemberOfHKUGA'] = true;
    			}
    			else{
    				$resultArr[$i]['validMemberOfHKUGA'] = false;
    			}
			}else{
				$resultArr[$i]['validMemberOfHKUGA'] = true;
			}
			
			//valid memberOfHKUGAFoundation
			if($aData[58]){
    			if(strtoupper($aData[58]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[58]) == strtoupper($kis_lang['Admission']['no'])){
    				$resultArr[$i]['validMemberOfHKUGAFoundation'] = true;
    			}
    			else{
    				$resultArr[$i]['validMemberOfHKUGAFoundation'] = false;
    			}
			}else{
				$resultArr[$i]['validMemberOfHKUGAFoundation'] = true;
			}
			
			//// Sibling ////
			if($aData[60] !== ''){
    			//valid current attempt sibling class1
    			if($aData[62] !== ''){
        			if (preg_match('/[1-6][a-dA-D]/',$aData[62])) {
        				$resultArr[$i]['validOthersRelativeClassPosition1'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassPosition1'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassPosition1'] = true;
    			}
    			
    			//valid current attempt sibling year1
    			if($aData[63] !== ''){
        			if (
        			    $aData[63] >= 2006 &&
        			    $aData[63] <= ($admission_year_start-1)
        		    ) {
        				$resultArr[$i]['validOthersRelativeClassYear1'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassYear1'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassYear1'] = true;
    			}
			}else{
				$resultArr[$i]['validOthersRelativeClassPosition1'] = true;
				$resultArr[$i]['validOthersRelativeClassYear1'] = true;
			}

			if($aData[64] !== ''){
    			//valid current attempt sibling class2
    			if($aData[66] !== ''){
        			if (preg_match('/[1-6][a-dA-D]/',$aData[66])) {
        				$resultArr[$i]['validOthersRelativeClassPosition2'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassPosition2'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassPosition2'] = true;
    			}
    			
    			//valid current attempt sibling year2
    			if($aData[67] !== ''){
        			if (
        			    $aData[67] >= 2006 &&
        			    $aData[67] <= ($admission_year_start-1)
        		    ) {
        				$resultArr[$i]['validOthersRelativeClassYear2'] = true;
        			}
        			else{
        				$resultArr[$i]['validOthersRelativeClassYear2'] = false;
        			}
    			}else{
    				$resultArr[$i]['validOthersRelativeClassYear2'] = true;
    			}
			}else{
				$resultArr[$i]['validOthersRelativeClassPosition2'] = true;
				$resultArr[$i]['validOthersRelativeClassYear2'] = true;
			}
			
			if($aData[68] !== ''){
    			//valid other sibling DOB1
    			$aData[70] = getDefaultDateFormat($aData[70]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[70]) ) {
    			    list($year , $month , $day) = explode('-',$aData[70]);
    			    $resultArr[$i]['validOthersRelativeBirth1'] = checkdate($month , $day , $year);
    			}
    			else{
    			    $resultArr[$i]['validOthersRelativeBirth1'] = false;
    			}
    			
    			//valid other sibling twins1
    			if($aData[71] == $kis_lang['Admission']['yes'] || $aData[71] == $kis_lang['Admission']['no']){
    				$resultArr[$i]['validOthersRelativeTwin1'] = true;
    			}
    			else{
    				$resultArr[$i]['validOthersRelativeTwin1'] = false;
    			}
			}else{
			    $resultArr[$i]['validOthersRelativeBirth1'] = true;
				$resultArr[$i]['validOthersRelativeTwin1'] = true;
			}

			if($aData[75] !== ''){
    			//valid other sibling DOB2
    			$aData[76] = getDefaultDateFormat($aData[76]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[76]) ) {
    			    list($year , $month , $day) = explode('-',$aData[76]);
    			    $resultArr[$i]['validOthersRelativeBirth2'] = checkdate($month , $day , $year);
    			}
    			else{
    			    $resultArr[$i]['validOthersRelativeBirth2'] = false;
    			}
    			
    			//valid other sibling twins2
    			if($aData[77] == $kis_lang['Admission']['yes'] || $aData[77] == $kis_lang['Admission']['no']){
    				$resultArr[$i]['validOthersRelativeTwin2'] = true;
    			}
    			else{
    				$resultArr[$i]['validOthersRelativeTwin2'] = false;
    			}
			}else{
			    $resultArr[$i]['validOthersRelativeBirth2'] = true;
				$resultArr[$i]['validOthersRelativeTwin2'] = true;
			}
			

			$resultArr[$i]['validData'] = $aData[1];
			$i++;
		} // End foreach
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
			if(!$aResult['validAdmissionDate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidAdmissionDate'];
			}
			if(!$aResult['validAdmissionNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidApplicationNo'];
			}
			else if(!$aResult['validChineseNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyChineseName'];
			}
			else if(!$aResult['validChineseName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidChineseName'];
			}
			else if(!$aResult['validEnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyEnglishName'];
			}
			else if(!$aResult['validEnglishName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidEnglishName'];
			}
			else if(!$aResult['validDateOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyDateOfBirth'];
			}
			else if(!$aResult['validDateOfBirth']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidDateOfBirth'];
			}
			else if(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfGender'];
			}
			else if(!$aResult['validPlaceOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyPlaceOfBirth'];
			}
			else if(!$aResult['validBirthCertNoType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidFormatOfBirthCertNo'];
			}
			else if(!$aResult['validBirthCertNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidBirthCertNo'];
			}
			else if(!$aResult['validSpokenLanguageForInterview']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfSpokenLanguageForInterview'] ;
			}
			else if(!$aResult['validAddressStreetEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyAddressStreet'] ;
			}
			else if(!$aResult['validAddressDistrictEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyAddressDistrict'] ;
			}
			else if(!$aResult['validAddressDistrict']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfAddressDistrict'] ;
			}
			else if(!$aResult['validAddressAreaEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyAddressArea'] ;
			}
			else if(!$aResult['validTelHome']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidTelHome'] ;
			}
			else if(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidContactEmail'] ;
			}
			else if(!$aResult['validNoBrotherSister']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidNoBrotherSister'] ;
			}
			else if(!$aResult['validBrotherSisterRank']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidNoBrotherSisterRank'] ;
			}
			else if(!$aResult['validContactPerson']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfContactPerson'] ;
			}
        	else if(!$aResult['validSpecialEducationalNeeds']){
        		$hasError = true;
        		$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfSpecialEducationalNeeds'] ;
        	}
        	else if(!$aResult['validAppliedBefore']){
        		$hasError = true;
        		$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfAppliedBefore'] ;
        	}
			else if(!$aResult['validCurrentAttendGrade1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendGrade'] ;
			}
			else if(!$aResult['validCurrentAttendYear1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendYear'] ;
			}
			else if(!$aResult['validCurrentAttendGrade2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendGrade'] ;
			}
			else if(!$aResult['validCurrentAttendYear2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfCurrentAttendYear'] ;
			}
			else if(!$aResult['validTwins']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfTwins'] ;
			}
			else if(!$aResult['validPlaceOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyPlaceOfBirth'];
			}
			else if(!$aResult['validG1ChineseNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGChineseName'] ;
			}
			else if(!$aResult['validG1ChineseName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGChineseName'] ;
			}
			else if(!$aResult['validG1EnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGEnglishName'] ;
			}
			else if(!$aResult['validG1EnglishName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEnglishName'] ;
			}
			else if(!$aResult['validG1MobileNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGMobileNo'] ;
			}
			else if(!$aResult['validG1Email']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEmail'] ;
			}
			else if(!$aResult['validG2ChineseNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGChineseName'] ;
			}
			else if(!$aResult['validG2ChineseName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGChineseName'] ;
			}
			else if(!$aResult['validG2EnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyGEnglishName'] ;
			}
			else if(!$aResult['validG2EnglishName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEnglishName'] ;
			}
			else if(!$aResult['validG2MobileNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGMobileNo'] ;
			}
			else if(!$aResult['validG2Email']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidGEmail'] ;
			}
			else if(!$aResult['validMemberOfHKUGA']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfMemberOfHKUGA'] ;
			}
			else if(!$aResult['validMemberOfHKUGAFoundation']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfMemberOfHKUGAFoundation'] ;
			}
			else if(!$aResult['validOthersRelativeClassPosition1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeClassPosition'] ;
			}
			else if(!$aResult['validOthersRelativeClassYear1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeYear'] ;
			}
			else if(!$aResult['validOthersRelativeClassPosition2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeClassPosition'] ;
			}
			else if(!$aResult['validOthersRelativeClassYear2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeYear'] ;
			}
			else if(!$aResult['validOthersRelativeBirth1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeBirth'] ;
			}
			else if(!$aResult['validOthersRelativeTwin1']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfOthersRelativeTwin'] ;
			}
			else if(!$aResult['validOthersRelativeBirth2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidOthersRelativeBirth'] ;
			}
			else if(!$aResult['validOthersRelativeTwin2']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfOthersRelativeTwin'] ;
			}
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.($i+2).'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	
	function checkImportDataForImportAdmissionHeaderCust($data, $lang = '', $type = '')
    {
    	if ($type == 'status') {
	        //$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
	        $file_format = $this->getExportHeaderCustom($schoolYearID = '', $classLevelID = '', $type = 'status');
	        $file_format = $file_format[0];
	        $csv_header = $data[0];
	        # check csv header
	        $format_wrong = false;
	
	        for ($i = 0; $i < sizeof($file_format); $i++) {
	            if ($csv_header[$i] != $file_format[$i]) {
	                $format_wrong = true;
	                break;
	            }
	        }
    	}
    	else{
    		$format_wrong = $this->checkImportDataForImportAdmissionHeader($data, $lang);
    	}
        return $format_wrong;
    }

    function checkImportDataForImportAdmissionCust($data, $type = '')
    {
        global $kis_lang, $admission_cfg;
        
        if ($type == 'status') {
	        $resultArr = array();
	        $i = 0;
	        array_shift($data);
	
	        #### Get current application id START ####
	        $sql = "SELECT ApplicationID FROM ADMISSION_APPLICATION_STATUS WHERE SchoolYearID='{$this->schoolYearID}'";
	        $currentApplicationIdArr = $this->returnVector($sql);
	        #### Get current application id END ####
	
	        #### Get valid status START ####
	        $validStatusArr = array();
	        foreach ($admission_cfg['Status'] as $key => $val) {
	            $validStatusArr[] = $kis_lang['Admission']['Status'][$key];
	        }
	        #### Get valid status END ####
	
	        foreach ($data as $aData) {
	            //valid ApplicationID
	            if ($aData[0]) {
	                $resultArr[$i]['validApplicationID'] = in_array($aData[0], $currentApplicationIdArr);
	            } else {
	                $resultArr[$i]['validApplicationID'] = false;
	            }
	
	            //valid Status
	            if ($aData[2]) {
	                $resultArr[$i]['validStatus'] = in_array($aData[2], $validStatusArr);
	            } else {
	                $resultArr[$i]['validStatus'] = false;
	            }
	
	            $resultArr[$i]['validData'] = $aData[0];
	            $resultArr[$i]['validDataName'] = $aData[1];
	            $resultArr[$i]['validDataStatus'] = $aData[2];
	            $i++;
	        }
	        $result = $resultArr;
	
	        //for printing the error message
	        $errCount = 0;
	        $x = '';
	
	        $x .= '<table class="common_table_list"><tbody><tr class="step2">
						<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
						<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['applicationno'] . '</th>
						<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['name'] . '</th>
						<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['applicationstatus'] . '</th>
						<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
					</tr>';
	        $i = 1;
	        foreach ($result as $aResult) {
	            //developing
	            $hasError = false;
	            $errorMag = '';
	            if (!$aResult['validApplicationID']) {
	                $hasError = true;
	                $errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
	            } elseif (!$aResult['validStatus']) {
	                $hasError = true;
	                $errorMag = $kis_lang['Admission']['HKUGAC']['msg']['importApplicationStatusIncorrect'];
	            }
	
	            //print the error msg to the client
	            if ($hasError) {
	                $errCount++;
	                $x .= '<tr class="step2">
						<td>' . $i . '</td>
						<td>' . $aResult['validData'] . '</td>
						<td>' . $aResult['validDataName'] . '</td>
						<td>' . $aResult['validDataStatus'] . '</td>
						<td><font color="red">';
	                $x .= $errorMag;
	                $x .= '</font></td></tr>';
	            }
	
	            $i++;
	        }
	        $x .= '</tbody></table>';
	        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
        }
        else{
        	return $this->checkImportDataForImportAdmission($data);
        }
    }
    
	function importDataForImportAdmission($data, $type=""){
		global $kis_lang, $admission_cfg;
		
		if($type == 'status'){
			$resultArr = array();
	        array_shift($data);
	
	        #### Get applicationId START ####
	        $sql = "SELECT ApplicationID, RecordID FROM ADMISSION_OTHERS_INFO";
	        $rs = $this->returnResultSet($sql);
	
	        $applicationIdMapping = BuildMultiKeyAssoc($rs, 'ApplicationID', 'RecordID', true);
	        #### Get applicationId END ####
	
	        #### Get status mapping START ####
	        $validStatusMapping = array();
	        foreach ($admission_cfg['Status'] as $key => $val) {
	            $validStatusMapping[$kis_lang['Admission']['Status'][$key]] = $key;
	        }
	        #### Get status mapping END ####
	
	        foreach ($data as $aData) {
	            global $UserID;
	
	            $applicationNumber = $aData[0];
	            $status = $admission_cfg['Status'][$validStatusMapping[$aData[2]]];
	
	            $result = $this->updateApplicationStatusByIds($applicationIdMapping[$applicationNumber], $status);
	            $resultArr[] = $result;
	            //debug_pr($aData);
	        }
	        return $resultArr;
		}
		
		//// Get all ApplicationID START ////
		$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO";
		$allApplicationID = $this->returnVector($sql);
		//// Get all ApplicationID END ////
		
		//// Get string code START ////
		$allPlaceOfBirth = array();
		foreach($admission_cfg['PlaceOfBirth'] as $id=>$place){
		    if($id == 0){
		        continue;
		    }
		    $allPlaceOfBirth[ strtoupper($place) ] = $id;
		}
		
		$allBirthCertType = array(
		    strtoupper($kis_lang['Admission']['BirthCertType']['hk2']) => $admission_cfg['BirthCertType']['birthCert'], 
		    strtoupper($kis_lang['Admission']['HKUGAPS']['birthcertno']) => $admission_cfg['BirthCertType']['hkid']
		);
		
		$allLangSpoken = array(
		    strtoupper($kis_lang['Admission']['Languages']['Cantonese']) => 'Cantonese', 
		    strtoupper($kis_lang['Admission']['Languages']['English']) => 'English', 
		    strtoupper($kis_lang['Admission']['Languages']['Putonghua']) => 'Putonghua'
		);
		
		$allAddressDistrict = array();
		foreach($admission_cfg['AddressDistrict'] as $id=>$district){
		    $allAddressDistrict[ strtoupper($district) ] = $id;
		}
		
		$allAddressArea = array();
		foreach($admission_cfg['AddressArea'] as $id=>$area){
		    if($id == 3){
		        continue;
		    }
		    $allAddressArea[ strtoupper($area) ] = $id;
		}
		
		$allContactPerson = array(
		    strtoupper($kis_lang['Admission']['HKUGAPS']['father']) => 'Father', 
		    strtoupper($kis_lang['Admission']['HKUGAPS']['mother']) => 'Mother', 
		    strtoupper($kis_lang['Admission']['HKUGAPS']['guardian']) => 'Guardian'
		);
		//// Get string code END ////
		
		$resultArr = array();
		array_shift($data);
		array_shift($data);
		foreach($data as $aData){
			global $UserID;
			
			$isUpdate = (in_array(trim($aData[1]), $allApplicationID));
			
			//--- convert the text to key code [start]
		    if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[6] = 'M';
			}
			else if(strtoupper($aData[6]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[6] = 'F';
			}
			
			// Place Of Birth
			$_key = strtoupper($aData[7]);
			if(in_array( $_key, array_keys($allPlaceOfBirth) )){
			    $aData[7] = $allPlaceOfBirth[$_key];
			}
			
			// Birth cert type for non-other-type
			$_key = strtoupper($aData[8]);
			if(in_array( $_key, array_keys($allBirthCertType) )){
			    $birthCertType = $allBirthCertType[$_key];
			    $birthCertTypeOther = '';
			}else{
			    $birthCertType = $admission_cfg['BirthCertType']['others'];
			    $birthCertTypeOther = $aData[8];
			}
			
			// Lang for interview
			$_key = strtoupper($aData[10]);
			if(in_array( $_key, array_keys($allLangSpoken) )){
			    $aData[10] = $allLangSpoken[$_key];
			}
			
			
			// Address District
			$_key = strtoupper($aData[17]);
			if(in_array( $_key, array_keys($allAddressDistrict) )){
			    $aData[17] = $allAddressDistrict[$_key];
			}
			
			// Address Area
			$_key = strtoupper($aData[18]);
			if(in_array( $_key, array_keys($allAddressArea) )){
			    $aData[18] = $allAddressArea[$_key];
			}

			// Contact Persion
			$_contactPersons = explode(',', $aData[24]);
			$contactPersons = array();
			foreach((array)$_contactPersons as $person){
    			$_key = strtoupper(trim($person));
    			if(in_array( $_key, array_keys($allContactPerson) )){
    			    $contactPersons[] = $allContactPerson[$_key];
    			}
			}
			
			// Special need
			if(strtoupper($aData[25]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[25] = '1';
			}
			else if(strtoupper($aData[25]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[25] = '0';
			}
			if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[27] = '1';
			}
			else if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[27] = '0';
			}
			
			// Twins
			if(strtoupper($aData[39]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[39] = '1';
			}
			else if(strtoupper($aData[39]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[39] = '0';
			}
			
			// HKUGA member
			if(strtoupper($aData[55]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[55] = '1';
			}
			else if(strtoupper($aData[55]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[55] = '0';
			}
			
			// HKUGA Foundation member
			if(strtoupper($aData[58]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[58] = '1';
			}
			else if(strtoupper($aData[58]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[58] = '0';
			}

			// Sibling is Twins
			if(strtoupper($aData[71]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[71] = '1';
			}
			else if(strtoupper($aData[71]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[71] = '0';
			}
			if(strtoupper($aData[77]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[77] = '1';
			}
			else if(strtoupper($aData[77]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[77] = '0';
			}
			
			//valid birth cert type
			/*foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$aData[8] =$_type;
					break;
				}
			}*/
			
		    $result = array();

		    if($isUpdate){
		        $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '{$aData[1]}'";
		        $result[] = $this->db_db_query($sql);
		    }else{
		    	if(!$aData[1]){
		    		$aData[1] = $this->newApplicationNumber2($_REQUEST['schoolYearID'], $_REQUEST['classLevelID'], true);
		    	}
		        $result[] = $this->insertApplicationStatus($this, $aData[1],$_REQUEST['schoolYearID']);
		        $sql = "INSERT INTO ADMISSION_STU_INFO (ApplicationID) VALUES ('{$aData[1]}')";
		        $result[] = $this->db_db_query($sql);
		    }
		    
		    $sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				SET
	     			stu.ChineseName = '".$aData[3]."',
	      			stu.EnglishName = '".$aData[4]."', 
	      			stu.DOB = '".getDefaultDateFormat($aData[5])." 00:00:00',	
	      			stu.Gender = '".$aData[6]."',
	      			stu.PlaceOfBirth = '".$aData[7]."',
					stu.BirthCertType = '{$birthCertType}' ,
					stu.BirthCertTypeOther = '{$birthCertTypeOther}' ,
	      			stu.BirthCertNo = '".$aData[9]."' ,
      			    stu.LangSpokenAtHome = '{$aData[10]}',
                    stu.AddressRoom = '".mysql_real_escape_string($aData[11])."',
                    stu.AddressFloor = '".mysql_real_escape_string($aData[12])."',
                    stu.AddressBlock = '".mysql_real_escape_string($aData[13])."',
                    stu.AddressBldg = '".mysql_real_escape_string($aData[14])."',
                    stu.AddressEstate = '".mysql_real_escape_string($aData[15])."',
                    stu.AddressStreet = '".mysql_real_escape_string($aData[16])."',
                    stu.AddressDistrict = '".mysql_real_escape_string($aData[17])."',
                    stu.Address = '".mysql_real_escape_string($aData[18])."',
                    stu.HomeTelNo = '{$aData[19]}',
                    stu.Email = '{$aData[20]}',
                    stu.Email2 = '{$aData[21]}',
                    stu.IsTwinsApplied = '{$aData[39]}',	
                    stu.TwinsApplicationID  = '{$aData[40]}',
					stu.DateModified = NOW(),
				    stu.ModifiedBy = '".$UserID."'
				WHERE 
					stu.ApplicationID = '".$aData[1]."'
	    	";
		    
		    $result[] = $this->db_db_query($sql);
		    ######## Student Cust START ########
		    $data = array();
		    
		    $data['NoBrotherSister'] = $aData[22];
		    $data['BrotherSisterRank'] = $aData[23];
		    $data['ContactPerson'] = $contactPersons;
		    
		    $data['CurrentAttend1'] = $aData[29];
		    
//		    $grades1 = explode('-', $aData[25]);
		    $data['Grades1Start'] = trim($aData[30]);
		    $data['Grades1End'] = trim($aData[31]);
		    
//		    $year1 = explode('-', $aData[26]);
		    $data['Year1Start'] = trim($aData[32]);
		    $data['Year1End'] = trim($aData[33]);
		    
		    $data['CurrentAttend2'] = $aData[34];
		    
//		    $grades2 = explode('-', $aData[28]);
		    $data['Grades2Start'] = trim($aData[35]);
		    $data['Grades2End'] = trim($aData[36]);
		    
//		    $year2 = explode('-', $aData[29]);
		    $data['Year2Start'] = trim($aData[37]);
		    $data['Year2End'] = trim($aData[38]);
		    
		    $result[] = $this->insertApplicationStudentInfoCust($data, $aData[1], $isUpdate=1);
		    ######## Student Cust END ########
		    
		    
		    ######## Parent START ########
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[41],
		        'EnglishName' => $aData[42],
		        'Mobile' => $aData[43],
		        'Email' => $aData[44],
		        'JobTitle' => $aData[45],
		        'Company' => $aData[46],
		        'PG_TYPE' => 'F'
		    ), $isUpdate);
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[47],
		        'EnglishName' => $aData[48],
		        'Mobile' => $aData[49],
		        'Email' => $aData[50],
		        'JobTitle' => $aData[51],
		        'Company' => $aData[52],
		        'PG_TYPE' => 'M'
		    ), $isUpdate);
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[53],
		        'Relationship' => $aData[54],
		        'PG_TYPE' => 'G'
		    ), $isUpdate);
		    ######## Parent END ########
		    
		    
		    ######## Siblings START ########
		    $data = array();
            $data['OthersRelativeStudiedNameChi1'] = $aData[60];
            $data['OthersRelativeStudiedNameEng1'] = $aData[61];
            $data['OthersRelativeClassPosition1'] = $aData[62];
            $data['OthersRelativeGraduationYear1'] = $aData[63];
            
            $data['OthersRelativeStudiedNameChi2'] = $aData[64];
            $data['OthersRelativeStudiedNameEng2'] = $aData[65];
            $data['OthersRelativeClassPosition2'] = $aData[66];
            $data['OthersRelativeGraduationYear2'] = $aData[67];
            
            $data['OthersRelativeNameChi1'] = $aData[68];
            $data['OthersRelativeNameEng1'] = $aData[69];
            $data['OthersRelativeBirth1'] = $aData[70];
            $data['OthersRelativeTwin1'] = $aData[71];
            $data['OthersRelativeSchool1'] = $aData[72];
            $data['OthersRelativeGrade1'] = $aData[73];
            
            $data['OthersRelativeNameChi2'] = $aData[74];
            $data['OthersRelativeNameEng2'] = $aData[75];
            $data['OthersRelativeBirth2'] = $aData[76];
            $data['OthersRelativeTwin2'] = $aData[77];
            $data['OthersRelativeSchool2'] = $aData[78];
            $data['OthersRelativeGrade2'] = $aData[79];

            $result[] = $this->insertApplicationRelativesInfoCust($data, $aData[1], $isUpdate);
		    ######## Siblings END ########

            ######## Cust START ########
            $sql = "DELETE FROM
                ADMISSION_CUST_INFO
            WHERE
                ApplicationID = '{$aData[1]}'
            AND
                Code in (
                    'HKUGA_Member',
                    'HKUGA_Member_Name',
                    'HKUGA_Member_No',
                    'HKUGA_Foundation_Member',
                    'HKUGA_Foundation_Member_Name',
                    'specialEducationalNeeds',
                    'specialEducationalNeeds_Details',
                    'appliedBefore',
                    'appliedBefore_Details'
                )
            ";
            $result[] = $this->db_db_query($sql);
            
            #### Special need START ####
            $result[] = $this->insertApplicationCustInfo(array(
                'Code' => 'specialEducationalNeeds',
                'Value' => $aData[25]
            ), $aData[1]);
            if($aData[25]){
                $result[] = $this->insertApplicationCustInfo(array(
                    'Code' => 'specialEducationalNeeds_Details',
                    'Value' => $aData[26]
                ), $aData[1]);
            }
            #### Special need END ####
            
            #### Applied Before START ####
            $result[] = $this->insertApplicationCustInfo(array(
                'Code' => 'appliedBefore',
                'Value' => $aData[27]
            ), $aData[1]);
            if($aData[27]){
                $result[] = $this->insertApplicationCustInfo(array(
                    'Code' => 'appliedBefore_Details',
                    'Value' => $aData[28]
                ), $aData[1]);
            }
            #### Special need END ####
            
            #### HKUGA Member START ####
            $result[] = $this->insertApplicationCustInfo(array(
                'Code' => 'HKUGA_Member',
                'Value' => $aData[55]
            ), $aData[1]);
            if($aData[55]){
                $result[] = $this->insertApplicationCustInfo(array(
                    'Code' => 'HKUGA_Member_Name',
                    'Value' => $aData[56]
                ), $aData[1]);
                 
                $result[] = $this->insertApplicationCustInfo(array(
                    'Code' => 'HKUGA_Member_No',
                    'Value' => $aData[57]
                ), $aData[1]);
            }
            #### HKUGA Member END ####
            
            #### HKUGA Education Foundation START ####
            $result[] = $this->insertApplicationCustInfo(array(
            'Code' => 'HKUGA_Foundation_Member',
            'Value' => $aData[58]
            ), $aData[1]);
            if($aData[58]){
                $result[] = $this->insertApplicationCustInfo(array(
                    'Code' => 'HKUGA_Foundation_Member_Name',
                    'Value' => $aData[59]
                ), $aData[1]);
            }
            #### HKUGA Education Foundation END ####
            ######## Cust END ########
            
            ######## Update School Year Record START ########
            $data = array();
            $data['sus_status'] = $_REQUEST['classLevelID']; // ApplyLevel
            $data['schoolYearID'] = $_REQUEST['schoolYearID'];
            $data['token'] = '';
            $result[] = $this->insertApplicationOthersInfo($data, $aData[1]);
            ######## Update School Year Record END ########
		    
		    $sql = "
				UPDATE 
					ADMISSION_APPLICATION_STATUS
		    	SET
	     			FeeBankName = '".$aData[82]."',
	     			FeeChequeNo = '".$aData[83]."',
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE 
					ApplicationID = '".$aData[1]."'
		   	";
		   
		    $result[] = $this->db_db_query($sql);
		     
			$resultArr[] = !in_array(false, $result);
			//debug_pr($aData);
		}
		return $resultArr;
	}
	
	public function getExportHeaderCustom($schoolYearID = '', $classLevelID = '', $type = '')
    {
        global $kis_lang;

        $headerArray = array();
        if ($type == '') {
            $headerArray = $this->getExportHeader($schoolYearID, $classLevelID);
        } elseif ($type == 'status') {
            $headerArray[0] = array(
                $kis_lang['Admission']['applicationno'],
                $kis_lang['Admission']['name'],
                $kis_lang['Admission']['applicationstatus'],
            );
        } elseif ($type == 'interviewTime') {
            $headerArray[0] = array(
                '',
                '',
                '',
                '',
                '',
                $kis_lang['Admission']['HKUGAC']['Inteview1'],
                '',
                '',
                '',
                $kis_lang['Admission']['HKUGAC']['Inteview2'],
                '',
                '',
                '',
                $kis_lang['Admission']['HKUGAC']['Inteview3'],
            );
            $headerArray[1] = array(
                $kis_lang['Admission']['applicationno'],
                $kis_lang['Admission']['name'],
                $kis_lang['Admission']['HKUGAC']['strn'],
                $kis_lang['Admission']['gender'],
                $kis_lang['Admission']['HKUGAC']['CurrentSchoolAttend'],
                $kis_lang['Admission']['interviewdate'],
                $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')',
                $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')',
                $kis_lang['sessiongroup'],
                $kis_lang['Admission']['interviewdate'],
                $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')',
                $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')',
                $kis_lang['sessiongroup'],
                $kis_lang['Admission']['interviewdate'],
                $kis_lang['timeslot'] . '(' . $kis_lang['from'] . ')',
                $kis_lang['timeslot'] . '(' . $kis_lang['to'] . ')',
                $kis_lang['sessiongroup'],
            );
        }
        return $headerArray;
    }
	
	function getExportHeader($lang = ''){
		global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$headerArray = array();
		
		if($lang == 'en'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}else if($lang == 'b5'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}
		
		//for student info
		$headerArray[] = $kis_lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['applyLevel'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['chinesename'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['englishname'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['personalIdentification'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['personalIdentification'].$kis_lang['Admission']['HKUGAPS']['no'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['spokenLanguageForInterview'];
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressFlat']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressFloor']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressBlock']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressBuilding']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressEstate']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressStreetNo']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressDistrict']})";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['address']} ({$kis_lang['Admission']['HKUGAPS']['addressArea']})";
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['hometel'];
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['contactEmail']} (1)";
		$headerArray['studentInfo'][] = "{$kis_lang['Admission']['HKUGAPS']['contactEmail']} (2)";
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['noBrotherSister'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['rank'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['contactperson'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['specialEducationalNeeds'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['Details'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['appliedBefore'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['HKUGAPS']['PleaseSpecifyYear'];
		
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['name'].(1);
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['grades'].(1).'('.$kis_lang['from'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['grades'].(1).'('.$kis_lang['to'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['year'].(1).'('.$kis_lang['from'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['year'].(1).'('.$kis_lang['to'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['name'].(2);
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['grades'].(2).'('.$kis_lang['from'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['grades'].(2).'('.$kis_lang['to'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['year'].(2).'('.$kis_lang['from'].')';
		$headerArray['studentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['year'].(2).'('.$kis_lang['to'].')';
		
		$headerArray['studentInfo3'][] = $kis_lang['Admission']['HKUGAPS']['twinsApp'];
		$headerArray['studentInfo3'][] = $kis_lang['Admission']['HKUGAPS']['twinsID'];
		//for parent info
		
		$headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAPS']['nameChi'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAPS']['nameEng'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAPS']['contactNum'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['email'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['occupation'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['HKUGAPS']['nameInstitution'];
		
		$headerArray['parentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['guardianName'];
		$headerArray['parentInfo2'][] = $kis_lang['Admission']['HKUGAPS']['guardianReleation'];
		
		//for other info
		$headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAPS']['memberOfHKUGA'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAPS']['memberName'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAPS']['memberNo'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAPS']['memberOfHKUGAFoundation'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['HKUGAPS']['memberName'];
		
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['nameChi'].(1);
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['nameEng'].(1);
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['currentClass'].(1);
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['graduationYear'].(1);
		
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['nameChi'].(2);
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['nameEng'].(2);
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['currentClass'].(2);
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['HKUGAPS']['graduationYear'].(2);
		
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['nameChi'].(1);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['nameEng'].(1);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['dateofbirth'].(1);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['twins'].(1);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['school'].(1);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['grades'].(1);
		
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['nameChi'].(2);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['nameEng'].(2);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['dateofbirth'].(2);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['twins'].(2);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['school'].(2);
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['HKUGAPS']['grades'].(2);
		
		
		//for official use
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['KTLMSKG']['bankName'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['KTLMSKG']['chequeNum'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (1)";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (2)";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (3)";
		$headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
		
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//student info header
		$exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
		for($i=0; $i < count($headerArray['studentInfo'])-2; $i++){
			$exportColumn[0][] = "";
		}
		
		$exportColumn[0][] = $kis_lang['Admission']['HKUGAPS']['currentAttend'];
		for($i=0; $i < count($headerArray['studentInfo2'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//parent info header
		$exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['HKUGAPS']['father']})";
		for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		$exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['HKUGAPS']['mother']})";
		for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
		for($i=0; $i < count($headerArray['otherInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		$exportColumn[0][] = $kis_lang['Admission']['HKUGAPS']['siblingInformation'];
		for($i=0; $i < count($headerArray['otherInfo2'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		$exportColumn[0][] = $kis_lang['Admission']['HKUGAPS']['siblingOther'] ;
		for($i=0; $i < count($headerArray['otherInfo3'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//official use header
		$exportColumn[0][] = $kis_lang['remarks'];
		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//sub header
		$exportColumn[1] = array_merge(
			array($headerArray[0],$headerArray[1]), 
			$headerArray['studentInfo'],
			$headerArray['studentInfo2'],
			$headerArray['studentInfo3'],
			$headerArray['parentInfo'],
			$headerArray['parentInfo'],
			$headerArray['parentInfo2'],
			$headerArray['otherInfo'],
			$headerArray['otherInfo2'],
			$headerArray['otherInfo3'],
			$headerArray['officialUse']
		);
		//if($lang)
		//include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
		return $exportColumn;
	}
	
	/**
     * Portal - export applicant details
     */
    public function getExportDataCustom($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $type = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        $dataArray = array();
        if ($type == '') {
            $dataArray = $this->getExportData($schoolYearID, $classLevelID, $applicationID, $recordID);
        } elseif ($type == 'status') {
            // ######## Get data START ########
            $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
            $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
            // ######## Get data END ########

            $dataArray = array(
                $StudentInfo['applicationID'],
                Get_Lang_Selection($StudentInfo['student_name_b5'], $StudentInfo['student_name_en']),
                $kis_lang['Admission']['Status'][$status['status']]
            );
        } elseif ($type == 'interviewTime') {
            // ######## Get data START ########
            $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
            $allCustInfo = $this->getAllApplicationCustInfo($StudentInfo['applicationID']);
            // ######## Get data END ########

            // ######## Get last school name START ########
            $rs = $this->getApplicationPrevSchoolInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
            $StudentPrevSchoolInfo = array();
            foreach ($rs as $r) {
                $StudentPrevSchoolInfo["{$r['Year']}_{$r['SchoolOrder']}"] = $r;
            }
            ksort($StudentPrevSchoolInfo);
            $StudentPrevSchoolInfo = array_values(array_reverse($StudentPrevSchoolInfo));
            $lastSchoolName = $StudentPrevSchoolInfo[0]['NameOfSchool'];
            // ######## Get last school name END ########

            // ######## Get interview START ########
            $interviewInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
            $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '');
            $interviewSettings = BuildMultiKeyAssoc($rs, 'RecordID');

            $interview1 = $interviewSettings[$interviewInfo['InterviewSettingID']];
            $interview2 = $interviewSettings[$interviewInfo['InterviewSettingID2']];
            $interview3 = $interviewSettings[$interviewInfo['InterviewSettingID3']];
            // ######## Get interview END ########

            $dataArray = array(
                $StudentInfo['applicationID'],
                Get_Lang_Selection($StudentInfo['student_name_b5'], $StudentInfo['student_name_en']),
                $allCustInfo['StudentSTRN'][0]['Value'],
                $kis_lang['Admission']['genderType'][$StudentInfo['Gender']],
                $lastSchoolName,
                $interview1['Date'],
                substr($interview1['StartTime'], 0, 5),
                substr($interview1['EndTime'], 0, 5),
                $interview1['GroupName'],
                $interview2['Date'],
                substr($interview2['StartTime'], 0, 5),
                substr($interview2['EndTime'], 0, 5),
                $interview2['GroupName'],
                $interview3['Date'],
                substr($interview3['StartTime'], 0, 5),
                substr($interview3['EndTime'], 0, 5),
                $interview3['GroupName'],
            );
        }

        return $dataArray;
    }
	
	function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg, $Lang, $kis_lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$parentInfoArr = array();
		foreach($parentInfo as $parent){
			foreach($parent as $para=>$info){
				$parentInfoArr[$parent['type']][$para] = $info;
			}
		}
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo3 = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		
		$siblingsInfoArr = array();
		foreach($otherInfo3 as $siblings){
			foreach($siblings as $para=>$info){
				$siblingsInfoArr[$siblings['type']][$para] = $info;
			}
		}
		
		$custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
		
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		//$student_name_en = explode(',',$studentInfo['student_name_en']);
		//$student_name_b5 = explode(',',$studentInfo['student_name_b5']);
		
		$dataArray = array();


		#### Place of Birth START ####
		if(is_numeric($studentInfo['placeofbirth']) && $studentInfo['placeofbirth'] > 0){
		    $studentInfo['placeofbirth'] = $admission_cfg['PlaceOfBirth'][$studentInfo['placeofbirth']];
		}
		#### Place of Birth END ####
		
		#### Birth cert START ####
		$persionalId = "";
		if($studentInfo['birthcerttype'] == "1"){
			$persionalId = "{$kis_lang['Admission']['BirthCertType']['hk2']}";
		}else if ($studentInfo['birthcerttype'] == "2"){
			$persionalId = "{$kis_lang['Admission']['HKUGAPS']['birthcertno']}";
		}else if ($studentInfo['birthcerttype'] == "3"){
			$persionalId = "{$studentInfo['birthcerttypeother']}";
		}
		#### Birth cert END ####

		#### Lang for Interview START ####
		if($studentInfo['langspokenathome'] == 'Cantonese'){
			$langSpoken = $kis_lang['Admission']['Languages']['Cantonese'];
		}else if($studentInfo['langspokenathome'] == 'Putonghua'){
			$langSpoken = $kis_lang['Admission']['Languages']['Putonghua'];
		}else if($studentInfo['langspokenathome'] == 'English'){
			$langSpoken = $kis_lang['Admission']['Languages']['English'];
		}
		#### Lang for Interview END ####
		
		#### contact person START ####
		$persons = array();
		foreach((array)$custInfo['Contact_Person'] as $contactPerson){
			if($contactPerson['Value'] == "Father"){
				$persons[] = $kis_lang['Admission']['HKUGAPS']['father'];
			}else if($contactPerson['Value'] == "Mother"){
				$persons[] = $kis_lang['Admission']['HKUGAPS']['mother'];
			}else if($contactPerson['Value'] == "Guardian"){
				$persons[] = $kis_lang['Admission']['HKUGAPS']['guardian'];
			}
		}
		$persons = implode(', ', $persons);
		#### contact persion END ####
		
		
		//for student info
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$dataArray['studentInfo'][] = $classLevel[$otherInfo['classLevelID']];
		$dataArray['studentInfo'][] = $studentInfo['student_name_b5'];
		$dataArray['studentInfo'][] = $studentInfo['student_name_en'];
		$dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
		$dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
		$dataArray['studentInfo'][] = $studentInfo['placeofbirth'];
		$dataArray['studentInfo'][] = $persionalId;
		$dataArray['studentInfo'][] = $studentInfo['birthcertno'];
		$dataArray['studentInfo'][] = $langSpoken;
		$dataArray['studentInfo'][] = $studentInfo['AddressRoom'];
		$dataArray['studentInfo'][] = $studentInfo['AddressFloor'];
		$dataArray['studentInfo'][] = $studentInfo['AddressBlock'];
		$dataArray['studentInfo'][] = $studentInfo['AddressBldg'];
		$dataArray['studentInfo'][] = $studentInfo['AddressEstate'];
		$dataArray['studentInfo'][] = $studentInfo['AddressStreet'];
		$dataArray['studentInfo'][] = $admission_cfg['AddressDistrict'][ $studentInfo['AddressDistrict'] ];
		if(
		    is_numeric($studentInfo['Address']) &&
		    $studentInfo['Address'] !== '' &&
		    $studentInfo['Address'] !== null
		){
            $dataArray['studentInfo'][] = $admission_cfg['AddressArea'][ $studentInfo['Address'] ];
		}else{
            $dataArray['studentInfo'][] = $studentInfo['Address'];
		}
		$dataArray['studentInfo'][] = $studentInfo['homephoneno'];
		$dataArray['studentInfo'][] = $studentInfo['email'];
		$dataArray['studentInfo'][] = $studentInfo['email2'];
		$dataArray['studentInfo'][] = $custInfo['No_Brother_Sister'][0]['Value'];
		$dataArray['studentInfo'][] = $custInfo['Brother_Sister_Rank'][0]['Value'];
		$dataArray['studentInfo'][] = $persons;
		if($custInfo['specialEducationalNeeds'][0]['Value']=='1'){
		    $dataArray['studentInfo'][] = $kis_lang['Admission']['yes'];
		    $dataArray['studentInfo'][] = $custInfo['specialEducationalNeeds_Details'][0]['Value'];
		}else{
		    $dataArray['studentInfo'][] = $kis_lang['Admission']['no'];
		    $dataArray['studentInfo'][] = '';
		}
		if($custInfo['appliedBefore'][0]['Value']=='1'){
		    $dataArray['studentInfo'][] = $kis_lang['Admission']['yes'];
		    $dataArray['studentInfo'][] = $custInfo['appliedBefore_Details'][0]['Value'];
		}else{
		    $dataArray['studentInfo'][] = $kis_lang['Admission']['no'];
		    $dataArray['studentInfo'][] = '';
		}
		$dataArray['studentInfo'][] = $otherInfo2[0]['NameOfSchool'];
		$tempStduentClassArray = explode(' - ',$otherInfo2[0]['Class']);
		$dataArray['studentInfo'][] = $tempStduentClassArray[0];
		$dataArray['studentInfo'][] = $tempStduentClassArray[1];
		$tempStduentYearArray = explode(' - ',$otherInfo2[0]['Year']);
		$dataArray['studentInfo'][] = $tempStduentYearArray[0];
		$dataArray['studentInfo'][] = $tempStduentYearArray[1];
		$dataArray['studentInfo'][] = $otherInfo2[1]['NameOfSchool'];
		$tempStduentClassArray = explode(' - ',$otherInfo2[1]['Class']);
		$dataArray['studentInfo'][] = $tempStduentClassArray[0];
		$dataArray['studentInfo'][] = $tempStduentClassArray[1];
		$tempStduentYearArray = explode(' - ',$otherInfo2[1]['Year']);
		$dataArray['studentInfo'][] = $tempStduentYearArray[0];
		$dataArray['studentInfo'][] = $tempStduentYearArray[1];
		$dataArray['studentInfo'][] = ($studentInfo['istwinsapplied']=='1'?$kis_lang['Admission']['yes']:($studentInfo['istwinsapplied']=='0'?$kis_lang['Admission']['no']:''));
		$dataArray['studentInfo'][] = $studentInfo['twinsapplicationid'];
		## Church Act END ##
		
		$dataArray['parentInfo'][] = $parentInfoArr['F']['chinesename'];
		$dataArray['parentInfo'][] = $parentInfoArr['F']['englishname'];
		$dataArray['parentInfo'][] = $parentInfoArr['F']['mobile'];
		$dataArray['parentInfo'][] = $parentInfoArr['F']['email'];
		$dataArray['parentInfo'][] = $parentInfoArr['F']['occupation'];
		$dataArray['parentInfo'][] = $parentInfoArr['F']['companyname'];
		$dataArray['parentInfo'][] = $parentInfoArr['M']['chinesename'];
		$dataArray['parentInfo'][] = $parentInfoArr['M']['englishname'];
		$dataArray['parentInfo'][] = $parentInfoArr['M']['mobile'];
		$dataArray['parentInfo'][] = $parentInfoArr['M']['email'];
		$dataArray['parentInfo'][] = $parentInfoArr['M']['occupation'];
		$dataArray['parentInfo'][] = $parentInfoArr['M']['companyname'];
		$dataArray['parentInfo'][] = $parentInfoArr['G']['chinesename'];
		$dataArray['parentInfo'][] = $parentInfoArr['G']['relationship'];
		
		$dataArray['otherInfo'][] = ($custInfo['HKUGA_Member'][0]['Value'] == "1"?$kis_lang['Admission']['yes']:($custInfo['HKUGA_Member'][0]['Value'] == "0"?$kis_lang['Admission']['no']:''));
		$dataArray['otherInfo'][] = $custInfo['HKUGA_Member_Name'][0]['Value'];
		$dataArray['otherInfo'][] = $custInfo['HKUGA_Member_No'][0]['Value'];
		$dataArray['otherInfo'][] = ($custInfo['HKUGA_Foundation_Member'][0]['Value'] == "1"?$kis_lang['Admission']['yes']:($custInfo['HKUGA_Foundation_Member'][0]['Value'] == "0"?$kis_lang['Admission']['no']:''));
		$dataArray['otherInfo'][] = $custInfo['HKUGA_Foundation_Member_Name'][0]['Value'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[1]['name'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[1]['englishName'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[1]['classposition'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[1]['year'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[2]['name'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[2]['englishName'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[2]['classposition'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[2]['year'];
		
		$dataArray['otherInfo'][] = $siblingsInfoArr[3]['name'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[3]['englishName'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[3]['dob'];
		$dataArray['otherInfo'][] = ($siblingsInfoArr[3]['istwins']=="1"? $kis_lang['Admission']['yes']:($siblingsInfoArr[3]['istwins']=="0"? $kis_lang['Admission']['no']:''));
		$dataArray['otherInfo'][] = $siblingsInfoArr[3]['schoolName'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[3]['classposition'];
		
		$dataArray['otherInfo'][] = $siblingsInfoArr[4]['name'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[4]['englishName'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[4]['dob'];
		$dataArray['otherInfo'][] = ($siblingsInfoArr[4]['istwins']=="1"? $kis_lang['Admission']['yes']:($siblingsInfoArr[4]['istwins']=="0"? $kis_lang['Admission']['no']:''));
		$dataArray['otherInfo'][] = $siblingsInfoArr[4]['schoolName'];
		$dataArray['otherInfo'][] = $siblingsInfoArr[4]['classposition'];
		//for official use
		$dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
		$dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
		$dataArray['officialUse'][] = $status['FeeBankName'];
		$dataArray['officialUse'][] = $status['FeeChequeNo'];
		$dataArray['officialUse'][] = $status['receiptID'];
		$dataArray['officialUse'][] = $status['receiptdate'];
		$dataArray['officialUse'][] = $status['handler'];
		$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
		$dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
		$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
		$dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
		$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
		$dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
		$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
		$dataArray['officialUse'][] = $status['remark'];
		
		$ExportArr = array_merge(
			array($dataArray[0],$dataArray[1]),
			$dataArray['studentInfo'],
			$dataArray['parentInfo'],
			$dataArray['otherInfo'], 
			$dataArray['officialUse']
		);
		
		return $ExportArr;
	}
	function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		$custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
		$otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$dataArray = array();
		if($tabID == 2){
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //ClassName
			$dataArray[0][] = ''; //Class Number
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = ''; //STRN
			$dataArray[0][] = ''; //WebSAMSRegNo
			$dataArray[0][] = $studentInfo['birthcertno'];//HKID
			$dataArray[0][] = ''; //Smart Card ID
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
			$dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
			$dataArray[0][] = ''; //Nationality
			$dataArray[0][] = ''; //Spoken Language at Home
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
			$dataArray[0][] = ''; //Admission Class Level
			$dataArray[0][] = ($otherInfo2[1]['NameOfSchool'] !=''?$otherInfo2[0]['NameOfSchool']."/ ".$otherInfo2[1]['NameOfSchool']:$otherInfo2[0]['NameOfSchool']); //Last Kindergarten Attended
			$dataArray[0][] = ''; //Last Primary School Attended
			$dataArray[0][] = ''; //Date of Graduate
			$dataArray[0][] = ''; //Date of Left
			$dataArray[0][] = ''; //Secondary School
			$dataArray[0][] = $studentInfo['homeaddresschi']; //Address
			$dataArray[0][] = $studentInfo['homephoneno']; //Home Tel
			$dataArray[0][] = ''; //Mobile
			$dataArray[0][] = $studentInfo['email']; //Admission Class Level
			
			
		}
		else if($tabID == 3){
			$parentInfoArr = array();
			foreach($parentInfo as $parent){
				foreach($parent as $para=>$info){
					$parentInfoArr[$parent['type']][$para] = $info;
				}
			}
			$dataArray[0][] = '';
			$dataArray[0][] = '';
			$dataArray[0][] = '';
			$dataArray[0][] = $parentInfoArr['F']['englishname'];
			$dataArray[0][] = $parentInfoArr['F']['chinesename'];
			$dataArray[0][] = $parentInfoArr['F']['type'];
			$dataArray[0][] = $parentInfoArr['F']['mobile'];
			$dataArray[1][] = '';
			$dataArray[1][] = '';
			$dataArray[1][] = '';
			$dataArray[1][] = $parentInfoArr['M']['englishname'];
			$dataArray[1][] = $parentInfoArr['M']['chinesename'];
			$dataArray[1][] = $parentInfoArr['M']['type'];
			$dataArray[1][] = $parentInfoArr['M']['mobile'];
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID JOIN ADMISSION_APPLICATION_STATUS as aas ON asi.ApplicationID = aas.ApplicationID WHERE TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."' AND aas.Status = 2";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();
		
		$sql = "SELECT 
					a.Email as Email,
					a.Email2 as Email2
				FROM ADMISSION_STU_INFO as a 
				WHERE a.ApplicationID = '".trim($applicationId)."'";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		$to_email2 = $records['Email2'];
		if($subject == ''){
			$email_subject = "港大同學會小學入學申請通知 HKUGA Primary School Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			if($to_email2 != '' && intranet_validateEmail($to_email2)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email2),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	public function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();

		if($subject == ''){
			$email_subject = "匯基書院(東九龍)入學申請通知 United Christian College (Kowloon East) Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
		$sql = "SELECT
					a.Email as Email
				FROM ADMISSION_STU_INFO as a
				WHERE a.ApplicationID = '".trim($applicationId)."'";
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	
	function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
			//check date
			if ($aData[4] =='' && $aData[5] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
		       list($year , $month , $day) = explode('-',$aData[4]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[4] =='' && $aData[5] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."' AND trim(s.birthCertNo) = '".trim($aData[3])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[4] = getDefaultDateFormat($aData[4]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[4]." ".$aData[5]."',
				InterviewLocation = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds=array()){
		$status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND st.status in ('".implode("','",$selectStatusArr)."') ";
		}
		if(sizeof($classLevelIds) > 0){
			$status_cond .= " AND o.ApplyLevel in ('".implode("','",(array)$classLevelIds)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		
		$sql='Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied, s.LangSpokenAtHome From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" '.$status_cond.' order by FIELD(s.LangSpokenAtHome,"Cantonese","English","Putonghua") desc, y.YearName desc';
				
		$result = $this->returnArray($sql);
		
		$allApplicant = array();
		
		for($i=0; $i<sizeof($result); $i++){
			$allApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel'], 'LangSpokenAtHome' => $result[$i]['LangSpokenAtHome']);
		}
		
//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);
		
		$result = array();
		
		$TwinsAssignedApplicant = array();
		
//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
				$sql = "SELECT RecordID, Quota, GroupName FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND (GroupName = '101' OR GroupName = '102') ORDER BY GroupName, Date, StartTime";
				$interviewRecordIDArr = $this->returnArray($sql);

				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
					
					if(!$allApplicant){
						break;
					}
					
					$previousClassLevel = $allApplicant[0]['ClassLevel'];
					$previousInterviewLanguage = $allApplicant[0]['LangSpokenAtHome'];
					
					if($allApplicant[0]['LangSpokenAtHome'] == 'Cantonese'){
						break;
					}
					if(($allApplicant[0]['LangSpokenAtHome'] == 'English' && $interviewRecordIDArr[$j]['GroupName'] != '102' || $allApplicant[0]['LangSpokenAtHome'] == 'Cantonese' && $interviewRecordIDArr[$j]['GroupName'] == '102')){
						continue;
					}
					
					$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
					$result[] = $this->db_db_query($sql);
					
					for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
						$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
						$result[] = $this->db_db_query($sql);
						array_shift($allApplicant);
						
						if($previousClassLevel != $allApplicant[0]['ClassLevel'] || $previousInterviewLanguage != $allApplicant[0]['LangSpokenAtHome']){
							break;
						}
					}
	
				}
				
				$sql = "SELECT RecordID, Quota, GroupName, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY Date, StartTime, GroupName";
//				$sql = "SELECT RecordID, Quota, GroupName FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL AND GroupName <> '101' AND GroupName <> '102' ORDER BY Date, StartTime, GroupName";
				$interviewRecordIDArr = $this->returnArray($sql);

				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
					
					if(!$allApplicant){
						break;
					}
					
					if($interviewRecordIDArr[$j]['ClassLevelID']){
						continue;
					}
					
					$previousClassLevel = $allApplicant[0]['ClassLevel'];
					$previousInterviewLanguage = $allApplicant[0]['LangSpokenAtHome'];
					
					$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
					$result[] = $this->db_db_query($sql);
					
					for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
						$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
						$result[] = $this->db_db_query($sql);
						array_shift($allApplicant);
						if($previousClassLevel != $allApplicant[0]['ClassLevel'] || $previousInterviewLanguage != $allApplicant[0]['LangSpokenAtHome']){
							break;
						}
					}
	
				}
//		}
		
		//handling twins swraping [start]
		$sql='Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID'.($round>1?$round:'').' as InterviewSettingID, s.LangSpokenAtHome From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				where o.ApplyYear = "'.$selectSchoolYearID.'" AND s.IsTwinsApplied = "1" '.$status_cond.' order by FIELD(s.LangSpokenAtHome,"Cantonese","English","Putonghua") desc, o.ApplicationID';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
			for($j=0; $j<sizeof($twinsResult); $j++){
				if(strtolower(trim($twinsResult[$i]['TwinsApplicationID'])) == strtolower(trim($twinsResult[$j]['BirthCertNo'])) && !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)){
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName, StartTime From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" ';
					$originalSession = current($this->returnArray($sql));
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName, StartTime From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$j]['InterviewSettingID'].'" ';
					$twinsOriginalSession = current($this->returnArray($sql));
					
					$sql = 'Select i.RecordID From ADMISSION_INTERVIEW_SETTING as i LEFT JOIN ADMISSION_OTHERS_INFO as o ON o.InterviewSettingID'.($round>1?$round:'').' = i.RecordID LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where i.ClassLevelID = "'.$originalSession['ClassLevelID'].'" AND i.Date = "'.$originalSession['Date'].'" AND i.StartTime = "'.$originalSession['StartTime'].'" AND i.Round = "'.$round.'" AND i.GroupName <> "'.$originalSession['GroupName'].'" AND i.RecordID <> "'.$twinsResult[$i]['InterviewSettingID'].'" AND i.RecordID <> "'.$twinsResult[$j]['InterviewSettingID'].'"  AND s.LangSpokenAtHome = "'.$twinsResult[$j]['LangSpokenAtHome'].'"';
					$newSession = current($this->returnArray($sql));
					
					if($newSession){
						$sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" AND s.IsTwinsApplied <> "1"';
						$swapApplicant = current($this->returnArray($sql));
						if($swapApplicant){
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$twinsOriginalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
							//debug_pr($sql);
							$updateResult = $this->db_db_query($sql);
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$j]['ApplicationID'].'" ';
							//debug_pr($sql);
							$updateResult = $this->db_db_query($sql);
							$assignedTwins[] = $twinsResult[$j]['ApplicationID'];
							$assignedTwins[] = $twinsResult[$i]['ApplicationID'];
						}
					}
				}
			}
		}
		
		//handling twins swraping [end]
		
		return !in_array(false,$result);
	}
	
	function getInterviewTimeslotName($recordId, $round){
		$interviewTimeslotName = '';
		
		$sql = 'Select Date, StartTime, SchoolYearID, Round From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$recordId.'"';
		$result = current($this->returnArray($sql));
		
		$sql = 'Select distinct Date From ADMISSION_INTERVIEW_SETTING where SchoolYearID = "'.$result['SchoolYearID'].'" AND Round = "'.$result['Round'].'" order by Date';
		$result2 = $this->returnArray($sql);
		
		$sql = 'Select distinct StartTime From ADMISSION_INTERVIEW_SETTING where SchoolYearID = "'.$result['SchoolYearID'].'" AND Round = "'.$result['Round'].'" AND Date = "'.$result['Date'].'" order by StartTime';
		$result3 = $this->returnArray($sql);
		
		for($i=0; $i<count($result2); $i++){
			if($result2[$i]['Date'] == $result['Date']){
				$interviewTimeslotName .= chr(($i+65));
				break;
			}
		}

		for($i=0; $i<count($result3); $i++){
			if($result3[$i]['StartTime'] == $result['StartTime']){
				$interviewTimeslotName .= str_pad($i+1, 2, '0', STR_PAD_LEFT);
				break;
			}
		}

		return $interviewTimeslotName;
	}
	
	function getInterviewResult($StudentDateOfBirth, $StudentBirthCertNo, $SchoolYearID='', $round=1, $ApplicationID=''){
		
		if($SchoolYearID){
//			$cond = " AND o.ApplyYear = '".$SchoolYearID."' ";
		}
		if($ApplicationID){
			$cond .= " AND o.ApplicationID = '".$ApplicationID."' ";
		}
		$sql = "SELECT o.ApplicationID, s.ChineseName, s.EnglishName, i.Date, i.StartTime, i.EndTime, i.GroupName, st.InterviewDate, st.InterviewLocation, o.ApplyLevel, i.RecordID, o.RecordID as OthersInfoRecordID 
				FROM ADMISSION_STU_INFO as s 
				JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID 
				JOIN ADMISSION_APPLICATION_STATUS as st ON st.ApplicationID = o.ApplicationID 
				LEFT JOIN ADMISSION_INTERVIEW_SETTING as i ON o.InterviewSettingID".($round>1?$round:'')." = i.RecordID
				WHERE (st.Status <> 5 AND st.Status <> 10) AND s.DOB = '".$StudentDateOfBirth."' AND s.DOB <> '' AND (s.BirthCertNo = '".strtoupper($StudentBirthCertNo)."' OR replace(replace(replace(s.BirthCertNo,')',''),'(',''),' ','') = '".strtoupper($StudentBirthCertNo)."') AND s.BirthCertNo <> '' ".$cond." ORDER BY o.ApplicationID desc";
		
		$result = current($this->returnArray($sql));
		
		if(!$result['ApplicationID']){
			return 0;
		}
		
//		if(!$result['Date']){
//			return 'NotAssigned';	
//		}
		
		return $result;
		
	}
	
	function isFirstYear($classLevelID) {
	    return strpos($this->classLevelAry[$classLevelID], '1') !== false;
	}
}
?>