<?php
	//using:
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

	$admission_cfg['PrintByPDF'] = 1;

	//Please also define lang in admission_lang
	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['waitingforinterview']	= 3;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;

	$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
	$admission_cfg['PaymentStatus']['OtherPayment']	= 2;

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;
//	$admission_cfg['Status']['reservedstudent']	= 5;

//	$admission_cfg['BirthCertType'] = array();
//	$admission_cfg['BirthCertType']['hk']	= 1;
//	$admission_cfg['BirthCertType']['oversea']	= 2;
//	$admission_cfg['BirthCertType']['mainland']	= 3;
//	$admission_cfg['BirthCertType']['others']	= 4;
//
//	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
//	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
//	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
//	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
//	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
//	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
//	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
//	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);

	/* for paypal testing [start] */
//	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
////	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
//	$admission_cfg['paypal_signature'] = '3bG6NqSV1kcyFzTo-JaVohhxIdBgV0PhCYcJRBfa9riwMwVasUOirv_epIS';
////	$admission_cfg['item_name'] = 'Application Fee';
////	$admission_cfg['item_number'] = 'AF2015';
////	$admission_cfg['amount'] = '40.00';
////	$admission_cfg['currency_code'] = 'HKD';
//	$admission_cfg['hosted_button_id'] = 'B28445AUPPUFW';
//	$admission_cfg['paypal_name'] = 'BroadLearning Education (Asia) Ltd. (Testing)';
	/* for paypal testing [End] */
	
//	/* for real paypal use [start] */
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
	$admission_cfg['paypal_signature'] = 'QIxlU-tY_rF_0Mydpvz78jeb3P7gJJQeP_Swoz-mn68PbLeFkb1bBQHz0by';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = 'AF2015';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = '4PW4W3YBY4EDS';
	$admission_cfg['paypal_name'] = 'BroadLearning Education (Asia) Ltd.';
//	/* for real paypal use [End] */

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}