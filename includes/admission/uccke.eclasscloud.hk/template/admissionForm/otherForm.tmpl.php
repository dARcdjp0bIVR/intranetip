<h1><?=$Lang['Admission']['otherInfo']?> Other Information</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<?php if($IsConfirm){ ?>
	<tr>
		<td class="field_title"><?=$Lang['Admission']['applyLevel']?> Apply Level</td>
		<td><?=$classLevel?></td>
	</tr>
<?php } ?>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['RelativeAttending']?> <br />
		Relative Attending United Christian College (Kowloon East)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['RelativeStudent']?>
		<?php }else{ ?>
			<input name="RelativeStudent" type="text" id="RelativeStudent" class="textboxtext" value="<?=$relativeName ?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['Relationship']?> Relationship
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['RelativeRelationship']?>
		<?php }else{ ?>
			<input name="RelativeRelationship" type="text" id="RelativeRelationship" class="textboxtext" value="<?=$relativeRelationship ?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['ClassAttending']?> Class / Attending
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['RelativeClassAttend']?>
		<?php }else{ ?>
			<input name="RelativeClassAttend" type="text" id="RelativeClassAttend" class="textboxtext" value="<?=$relativeClass ?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['Recommended']?> Recommended by
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['Recommended']?>
		<?php }else{ ?>
			<input name="Recommended" type="text" id="Recommended" class="textboxtext" value="<?=$referee ?>"/>
		<?php } ?>
	</td>
</tr>

</table>


<!-------------- Fee START -------------->
<?if ($lac->isInternalUse($_REQUEST['token'])){?>
<h1><?=$Lang['Admission']['applicationfee']?> Application Fee</h1>
<table class="form_table" style="font-size: 13px">
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['bankName']?> Bank Name
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['BankName']?>
		<?php }else{ ?>
			<input name="BankName" type="text" id="BankName" class="textboxtext" value="<?=$StatusInfo[0]['FeeBankName']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['chequeNum']?> Cheque No.
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ChequeNo']?>
		<?php }else{ ?>
			<input name="ChequeNo" type="text" id="ChequeNo" class="textboxtext" value="<?=$StatusInfo[0]['FeeChequeNo']?>"/>
		<?php } ?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['Payed']?> Payment settled
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['Payed'])? "{$Lang['General']['Yes']} Yes" : "<span style=\"color:red\">{$Lang['General']['No']} No</span>" ?>
		<?php }else{ ?>
			<input name="Payed" type="checkbox" id="Payed" value="1" <?=($feestatus)?'checked':'' ?>/>
			
		<?php } ?>
	</td>
</tr>
</table>
<?}?>
<!-------------- Fee END -------------->