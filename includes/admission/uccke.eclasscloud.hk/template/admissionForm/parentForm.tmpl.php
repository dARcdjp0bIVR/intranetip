<h1><?=$Lang['Admission']['PGInfo']?> Parent Information</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['UCCKE']['NameOfParentGuardian']?> Name of Parent/Guardian
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentName']?>
		<?php }else{ ?>
			<input name="ParentName" type="text" id="ParentName" class="textboxtext" value="<?=$GuardianInfo['Name']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['relationship']?> Relationship
	</td>
	<td>
		<?php /*if($IsConfirm){ ?>
			<?=$Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']?>
		<?php }else{ ?>
			<input type="radio" value="F" id="RelationshipF" name="Relationship">
			<label for="RelationshipF"> <?=$Lang['Admission']['PG_Type']['F']?> Father</label><br />
			<input type="radio" value="M" id="RelationshipM" name="Relationship">
			<label for="RelationshipM"> <?=$Lang['Admission']['PG_Type']['M']?> Mother</label><br />
			
			<div style="white-space: nowrap;">
				<input type="radio" id="RelationshipOtherChk" name="Relationship">
				<label for="RelationshipOtherChk"><?=$Lang['Admission']['others'] ?> Others </label>
				<input name="RelationshipOther" type="text" id="RelationshipOther"/>
			</div>
		<?php } */?>
		
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentRelationship']?>
		<?php }else{ ?>
			<input name="ParentRelationship" type="text" id="ParentRelationship" class="textboxtext" value="<?=$GuardianInfo['Relationship']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['religion']?>(<?=$Lang['Admission']['ifAny'] ?>) Religion(if any)
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentReligion']?>
		<?php }else{ ?>
			<input name="ParentReligion" type="text" id="ParentReligion" class="textboxtext" value="<?=$GuardianInfo['Religion'] ?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		
		<?=$star.$Lang['Admission']['email']?> Email Address
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentEmail']?>
		<?php }else{ ?>
			<input name="ParentEmail" type="text" id="ParentEmail" class="textboxtext" value="<?=$GuardianInfo['Email'] ?>"/>
		<?php } ?>
	</td>
	
	<?php if(!$IsConfirm){ ?>
		<td class="field_title">
			<?=$star.$Lang['Admission']['contactEmailConfirm']?> Confirm Email Address
		</td>
		<td>
			<input type="text" id="ParentEmailConfirm" class="textboxtext" value="<?=$GuardianInfo['Email'] ?>"/>
		</td>
	<?php } ?>
</tr>


<tr>
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['EngAddress']?> (<?=$Lang['Admission']['UCCKE']['DifferentAddress'] ?>) <br />
		Address in English (if it is different from the student's)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentAddress']?>
		<?php }else{ ?>
			<input name="ParentAddress" type="text" id="ParentAddress" class="textboxtext" value="<?=$GuardianInfo['Address'] ?>"/>
		<?php } ?>
	</td>
	
	
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['phome']?> Home Phone No.
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentHomeTelNo']?>
		<?php }else{ ?>
			<input name="ParentHomeTelNo" type="text" id="ParentHomeTelNo" class="textboxtext" value="<?=$GuardianInfo['HomePhone'] ?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['mobile']?> Mobile Phone No.
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentMobileNo']?>
		<?php }else{ ?>
			<input name="ParentMobileNo" type="text" id="ParentMobileNo" class="textboxtext" value="<?=$GuardianInfo['Mobile'] ?>"/>
		<?php } ?>
	</td>

	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['occupation']?> Occupation
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ParentOccupation']?>
		<?php }else{ ?>
			<input name="ParentOccupation" type="text" id="ParentOccupation" class="textboxtext" value="<?=$GuardianInfo['Occupation'] ?>"/>
		<?php } ?>
	</td>
</tr>

</table>


<script>
$(function() {
	function focusNextInput(){
		if($(this).attr('checked') == 'checked'){
			$(this).next().next('input').focus();
		}
	}
	function checkLastCheckbox(e){
		var code = e.keyCode || e.which;
	    if (code != '9') { // Tab
	    	$(this).prev().prev('input').attr('checked', 'checked');
	    }
	}
	
	$('#ParentLangSpokenOtherChk, #ParentEducationOtherChk').click(focusNextInput);
	$('#ParentLangSpokenOther, #ParentEducationOther').keyup(checkLastCheckbox);
});
</script>