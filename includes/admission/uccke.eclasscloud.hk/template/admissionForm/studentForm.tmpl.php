<h1><?=$Lang['Admission']['studentInfo']?> Student Information</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['chinesename']?> Name in Chinese
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_b5']?>
		<?php }else{ ?>
			<input name="studentsname_b5" type="text" id="studentsname_b5" class="textboxtext" value="<?=$applicationStudentInfo['student_name_b5']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star.$Lang['Admission']['englishname']?> (<?=$Lang['Admission']['UCCKE']['SameAsHKID']?>) <br />
		Name in English (same as on HKID Card)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_en']?>
		<?php }else{ ?>
			<input name="studentsname_en" type="text" id="studentsname_en" class="textboxtext"  value="<?=$applicationStudentInfo['student_name_en']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['dateofbirth']?> Date of Birth
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15" value="<?=$applicationStudentInfo['dateofbirth']?>"/>(YYYY-MM-DD)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['gender']?> Gender
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" <?=$applicationStudentInfo['gender'] == "M"?'checked':'' ?>>
			<label for="StudentGender1"> <?=$Lang['Admission']['genderType']['M']?> M</label>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender" <?=$applicationStudentInfo['gender'] == "F"?'checked':'' ?>>
			<label for="StudentGender2"> <?=$Lang['Admission']['genderType']['F']?> F</label>
		<?php } ?>
	</td>
</tr>


<tr>	
   	<td class="field_title">
		<?=$Lang['Admission']['placeofbirth']?> Place of Birth
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentPlaceOfBirth']?>
		<?php }else{ ?>
			<input name="StudentPlaceOfBirth" type="text" id="StudentPlaceOfBirth" class="textboxtext" value="<?=$applicationStudentInfo['placeofbirth']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['TSUENWANBCKG']['nationality']?> Nationality
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['Nationality']?>
		<?php }else{ ?>
			<input name="Nationality" type="text" id="Nationality" class="textboxtext" value="<?=$applicationStudentInfo['county']?>"/>
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['HKID']?> HKID Card No.
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentBirthCertNo']?>
		<?php }else{ ?>
			<?=$IsUpdate==true?$BirthCertNo:'' ?>
			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" value="<?=$BirthCertNo ?>" <?=$IsUpdate==true?'hidden':'' ?>/>
			
			<?php if(!$IsUpdate){ ?>
    			<br/>
    			<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
    			<br/>(eg：A123456(7)，please enter "A1234567")
			<?php } ?>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['BdRefNo']?> B.D.Student Reference No.
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			STRN: <?=$formData['BdRefNo']?>
		<?php }else{ ?>
			<span style="white-space: nowrap;">
				<span>STRN:</span> 
				<input name="BdRefNo" type="text" id="BdRefNo" class="textboxtext" value="<?=$BdRefNo ?>" style="width: 75%"/>
			</span>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['CurrentStudySchool']?> School Attending
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['LastSchool']?>
		<?php }else{ ?>
			<input name="LastSchool" type="text" id="LastSchool" class="textboxtext inputselect" value="<?=$currentSchoolName?>"/>
		<?php } ?>
	</td>

	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['ClassLastAttended']?> Class Last Attended
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['LastSchoolLevel']?>
		<?php }else{ ?>
			<input name="LastSchoolLevel" type="text" id="LastSchoolLevel" class="textboxtext" value="<?=$currentSchoolClass?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['NameOfChurch']?>(<?=$Lang['Admission']['ifAny'] ?>) Name of Church Attended (if any)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['Church']?>
		<?php }else{ ?>
			<input name="Church" type="text" id="Church" class="textboxtext" value="<?=$church?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['ElementryChinese']?> Elementary Chinese Student
	</td>
	<td>
		<?php 
		if($IsConfirm){
			if($formData['Elementary']){
				echo $Lang['Admission']['yes'] . ' Yes';
			}else{
				echo $Lang['Admission']['no'] . ' No';
			}
		}else{ 
		?>
			<input type="radio" value="1" id="ElementaryY" name="Elementary" <?=$elementaryY ?>>
			<label for="ElementaryY"><?=$Lang['Admission']['yes'] ?> Yes </label><br />
			<input type="radio" value="0" id="ElementaryN" name="Elementary" <?=$elementaryN ?>>
			<label for="ElementaryN"><?=$Lang['Admission']['no'] ?> No </label><br />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['UCCKE']['EngAddress']?> Address in English
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentHomeAddress']?>
		<?php }else{ ?>
			<input name="StudentHomeAddress" type="text" id="StudentHomeAddress" class="textboxtext" value="<?=$applicationStudentInfo['homeaddress']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['UCCKE']['ChiAddress']?> Address in Chinese
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentHomeAddressChi']?>
		<?php }else{ ?>
			<input name="StudentHomeAddressChi" type="text" id="StudentHomeAddressChi" class="textboxtext" value="<?=$applicationStudentInfo['homeaddresschi']?>"/>
		<?php } ?>
	</td>
</tr>




</table>

<script>
$(function() {
	function focusNextInput(){
		if($(this).attr('checked') == 'checked'){
			$(this).next().next('input').focus();
		}
	}
	function checkLastCheckbox(e){
		var code = e.keyCode || e.which;
	    if (code != '9') { // Tab
	    	$(this).prev().prev('input').attr('checked', 'checked');
	    }
	}
	
	$('#LangSpokenOtherChk, #LangWrittenOtherChk, #ChurchActivitiesOtherChk').click(focusNextInput);
	$('#LangSpokenOther, #LangWrittenOther, #ChurchActivitiesOther').keyup(checkLastCheckbox);
});
</script>
