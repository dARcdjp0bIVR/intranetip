<?php
	//using:
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */
//	 include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");

	//Please also define lang in admission_lang
	$admission_cfg['PrintByPDF'] = 1;
	$admission_cfg['SingleClassSelection'] = 1;

	// Prev Class Config -- use in libadmission_ui_cust.php -> getPrevClassSelection()
	$admission_cfg['classSelect'][0] = 2;
	$admission_cfg['classSelect'][1] = 5;
	$admission_cfg['classSelect'][2] = 5;
	$admission_cfg['classSelect'][3] = 3;
	$admission_cfg['classSelect'][4] = 2;

	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['waitingforinterview']	= 3;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;
//	$admission_cfg['Status']['reservedstudent']	= 5;

//	$admission_cfg['BirthCertType'] = array();
//	$admission_cfg['BirthCertType']['hkBirthCert']	= 1;
//	$admission_cfg['BirthCertType']['hkIdCard']	= 2;
//	$admission_cfg['BirthCertType']['other']	= 3;

//	$admission_cfg['placeofbirth'] = array();
//	$admission_cfg['placeofbirth']['hk']	= 1;
//	$admission_cfg['placeofbirth']['macau']	= 2;
//	$admission_cfg['placeofbirth']['china']	= 3;
//	$admission_cfg['placeofbirth']['other']	= 4;

	$admission_cfg['addressdistrict'] = array();
	$admission_cfg['addressdistrict']['hk']	= 1;
	$admission_cfg['addressdistrict']['kln']	= 2;
	$admission_cfg['addressdistrict']['nt']	= 3;

	$admission_cfg['lang'] = array();
	$admission_cfg['lang']['eng']	= 1;
	$admission_cfg['lang']['cantonese']	= 2;
	$admission_cfg['lang']['pth']	= 3;
	$admission_cfg['lang']['others']	= 4;

//	$admission_cfg['religion'] = array();
//	$admission_cfg['religion']['Christian']	= 1 ;
//	$admission_cfg['religion']['Catholic']	= 2;
//	$admission_cfg['religion']['other']	= 3;
//
//	$admission_cfg['classtype']['AM'] = 1;
//	$admission_cfg['classtype']['PM'] = 2 ;
//	$admission_cfg['classtype']['EitherOne'] = 3;
//
	$admission_cfg['classtype2']['AM'] = 1 ;
	$admission_cfg['classtype2']['PM'] = 2 ;
	$admission_cfg['classtype2']['WholeDay'] = 3 ;
//
//	$admission_cfg['relation']['F'] = 1;
//	$admission_cfg['relation']['M'] = 2;
//	$admission_cfg['relation']['B'] = 3;
//	$admission_cfg['relation']['S'] = 4;

	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "SDF2672qwer3we523";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 2; //in MB
	//$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}

		/* for paypal testing [start] */
//	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
////	$admission_cfg['paypal_to_email'] = 'rmkg.paypal@kinderu.org';
//	$admission_cfg['paypal_signature'] = '1WlCyeu3yi8D5fpegWb3O5z7Q-6VlyJoH7Z4Mq0wZcuumElEYwoYSc5MM1i';
////	$admission_cfg['item_name'] = 'Application Fee';
////	$admission_cfg['item_number'] = 'AF2015';
////	$admission_cfg['amount'] = '40.00';
////	$admission_cfg['currency_code'] = 'HKD';
//	$admission_cfg['hosted_button_id'] = 'N3VX9RN7XMPXW';
//	$admission_cfg['paypal_name'] = "test facilitator's Test Store";
	/* for paypal testing [End] */
	
	/* for real paypal use [start] */
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'rmkg.paypal@kinderu.org';
	$admission_cfg['paypal_signature'] = 'GWF6P5LIuJ68--KkZAVh78P3Q0neOjVlkzWVxufs47LP-y0-bo2My0l0QAC';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = 'AF2016';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = 'ZAPCNBQEENEJA';
	$admission_cfg['paypal_name'] = 'Grand (HK) Limited';
	/* for real paypal use [End] */
?>