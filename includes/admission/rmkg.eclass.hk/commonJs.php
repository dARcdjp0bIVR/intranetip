<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<!--script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script-->
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">

kis.datepicker('#FormStudentInfo_DOB');	

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert("Time is up for completing the application form! Please try again!\n<?=$Lang['Admission']['msg']['timeup']?>");
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	//For debugging only
	//return true;
	
	// borower version checking if browser is IE9 or below or not
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	//for email validation
	var re = /\S+@\S+\.\S+/;

	//allow file size checking if the  
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	// Student Info
	if(form1.ApplyClass.value==''){
		return false;
	} 
	if($('input:radio[name=OthersIsConsiderAlternative]:checked').val() == null){
		return false;
//	} else if(form1.studentssurname_b5.value==''){
//		return false;
//	} else if(form1.studentsfirstname_b5.value==''){
//		return false;
	} else if(form1.studentssurname_en.value==''){
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		return false;
	} else if(!form1.FormStudentInfo_DOB.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		return false;
	} else if(dOBRange[0] !='' && form1.FormStudentInfo_DOB.value < dOBRange[0] || dOBRange[1] !='' && form1.FormStudentInfo_DOB.value > dOBRange[1]){
		return false;
	} 
	else if($('input:radio[name=FormStudentInfo_StuGender]:checked').val() == null){	
		return false;
	} else if(form1.FormStudentInfo_POB.value==''){	
		return false;
	} else if(form1.FormStudentInfo_Nationality.value==''){	
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		return false;
	}
	if(checkBirthCertNo() > 0){
		return false;
	} else if(form1.FormStudentInfo_HomeTel.value==''){
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.FormStudentInfo_HomeTel.value)){
		return false;
	} else if(form1.HomeAddrRoom.value==''){
		return false;
	} else if(form1.HomeAddrFloor.value==''){
		return false;
	} else if(form1.HomeAddrBlock.value==''){
		return false;
	} else if(form1.HomeAddrBlding.value==''){
		return false;
	} else if(form1.HomeAddrStreet.value==''){
		return false;
	} else if(form1.HomeAddrDistrict.value==''){
		return false;
	} else if(form1.PrimaryEmail.value==''){
		return false;
	} else if(!re.test(form1.PrimaryEmail.value)){
		return false;
	} else if(form1.G1ChineseName.value=='' && form1.G2ChineseName.value==''){ //for parent info
		return false;
	}
	if(form1.G1ChineseName.value!=''){
		if(form1.G1Nationality.value==''){
			return false;
		}
		else if(form1.G1Occupation.value==''){
			return false;
		}
		else if(form1.G1MobileNo.value==''){
			return false;
		}
		else if(!/^[0-9]{8}$/.test(form1.G1MobileNo.value)){
			return false;
		}
	}
	
	if(form1.G2ChineseName.value!=''){
		if(form1.G2Nationality.value==''){
			return false;
		}
		else if(form1.G2Occupation.value==''){
			return false;
		}
		else if(form1.G2MobileNo.value==''){
			return false;
		}
		else if(!/^[0-9]{8}$/.test(form1.G2MobileNo.value)){
			return false;
		}
	}
	
	if(form1.StuMotherTongue.value==''){
		return false;
	}
	else if(form1.IsKinderU.value==''){
		return false;
	}

	return true;
}

function check_docs_upload2(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	//allow file size checking if the  
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	
	if(form1.StudentPersonalPhoto.value==''){
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		return false;
	}
//	var temp_count = (file_count > 1?1:file_count);
	var temp_count = file_count;
	for(var i=0;i<temp_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			if(i==0){
				return false;
			}
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			return false;
		}
	}
//	if(file_count > 1){
//	for(var i=1;i<file_count;i++)
//	{
//		var file_element = file_ary.get(i);
//		var otherFileVal = file_element.value;
//		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//		if(otherFileVal!=''){
//			if(!isOldBrowser){
//				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//			}
//			if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//				return false;
//			} else if(!isOldBrowser && otherFileSize > maxFileSize){
//				return false;
//			}
//		}
//	}
//	}
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
//		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
//	        alert("表格需以 Google Chrome、Firefox 或 Internet Explorer 10 或以上瀏覽器填寫。");
//	        return;
//	    }
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		//alert($("form").serialize());
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
			var chk_ary = $('input#Agree');
			var chk_count = chk_ary.length;
			for(var i=0;i<chk_count;i++)
			{
				var chk_element = chk_ary.get(i);
				if(chk_element.checked == false){
					alert("Please tick I agree the terms and conditions as stated above.\n請剔選 本人同意上述有關條款及細則。");
					isValid = false;
				}
			}
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
			   clearTimeout(timer);
			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();	 
		   
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_class_selection.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#DayTypeOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
		   
	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){

	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	return confirm('Attention: Information cannot be changed after submission.\n<?=$Lang['Admission']['munsang']['msg']['suresubmit']?>');
}

function check_choose_class(form1) {

	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>");
				return false;
			}
		}
	<?}?>
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>\nPlease select Class.");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	
	//For debugging only
	//return true;
	
	// borower version checking if browser is IE9 or below or not
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	//for email validation
	var re = /\S+@\S+\.\S+/;

	//allow file size checking if the  
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	// Student Info
	if(form1.ApplyClass.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['chooseSession']?>");	
		form1.ApplyClass.focus();
		return false;
	} 
	if($('input:radio[name=OthersIsConsiderAlternative]:checked').val() == null){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['chooseAlternative']?>");	
		form1.OthersIsConsiderAlternative[0].focus();
		return false;
//	} else if(form1.studentssurname_b5.value==''){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterSurname']?>");	
//		form1.studentssurname_b5.focus();
//		return false;
//	} else if(form1.studentsfirstname_b5.value==''){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterFirstName']?>");
//		form1.studentsfirstname_b5.focus();
//		return false;
	} else if(form1.studentssurname_en.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterSurname']?>");	
		form1.studentssurname_en.focus();
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterFirstName']?>");
		form1.studentsfirstname_en.focus();
		return false;
	} else if(!form1.FormStudentInfo_DOB.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.FormStudentInfo_DOB.value!=''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['DOBformat']?>");
		}
		else{
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterDOB']?>");	
		}
		
		form1.FormStudentInfo_DOB.focus();
		return false;
	} else if(dOBRange[0] !='' && form1.FormStudentInfo_DOB.value < dOBRange[0] || dOBRange[1] !='' && form1.FormStudentInfo_DOB.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['invalidbdaydateformat']?>");
		form1.FormStudentInfo_DOB.focus();
		return false;
	} 
	else if($('input:radio[name=FormStudentInfo_StuGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['chooseGender']?>");	
		form1.FormStudentInfo_StuGender[0].focus();
		return false;
	} else if(form1.FormStudentInfo_POB.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterPOB']?>");	
		form1.FormStudentInfo_POB.focus();
		return false;
	} else if(form1.FormStudentInfo_Nationality.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterNation']?>");	
		form1.FormStudentInfo_Nationality.focus();
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterBcertNum']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
	} 
//	else if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['BcertNumFormat']?>");	
//		form1.StudentBirthCertNo.focus();
//		return false;
//	}
	if(checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['BcertNumDuplicate']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(form1.FormStudentInfo_HomeTel.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterHomeNum']?>");	
		form1.FormStudentInfo_HomeTel.focus();
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.FormStudentInfo_HomeTel.value)){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['NumFormat']?>");
		form1.FormStudentInfo_HomeTel.focus();
		return false;
//	} else if(form1.HomeAddrRoom.value==''){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterAddress']?>");	
//		form1.HomeAddrRoom.focus();
//		return false;
//	} else if(form1.HomeAddrFloor.value==''){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterAddress']?>");	
//		form1.HomeAddrFloor.focus();
//		return false;
//	} else if(form1.HomeAddrBlock.value==''){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterAddress']?>");	
//		form1.HomeAddrBlock.focus();
//		return false;
//	} else if(form1.HomeAddrBlding.value==''){
//		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterAddress']?>");	
//		form1.HomeAddrBlding.focus();
//		return false;
	} else if(form1.HomeAddrStreet.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterAddress']?>");	
		form1.HomeAddrStreet.focus();
		return false;
	} else if(form1.HomeAddrDistrict.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterAddress']?>");	
		form1.HomeAddrDistrict.focus();
		return false;
	} else if(form1.PrimaryEmail.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterEmail']?>");	
		form1.PrimaryEmail.focus();
		return false;
	} else if(!re.test(form1.PrimaryEmail.value)){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['EmailFormat']?>");	
		form1.PrimaryEmail.focus();
		return false;
	} else if(form1.G1ChineseName.value=='' && form1.G2ChineseName.value==''){ //for parent info
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterParent']?>");
			form1.G1ChineseName.focus();
			return false;
	}
	if(form1.G1ChineseName.value!=''){
		if(form1.G1Nationality.value==''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterNation']?>");
			form1.G1Nationality.focus();
			return false;
		}
		else if(form1.G1Occupation.value==''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterOccupation']?>");
			form1.G1Occupation.focus();
			return false;
		}
		else if(form1.G1MobileNo.value==''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterMobile']?>");
			form1.G1MobileNo.focus();
			return false;
		}
		else if(!/^[0-9]{8}$/.test(form1.G1MobileNo.value)){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['NumFormat']?>");
			form1.G1MobileNo.focus();
			return false;
		}
	}
	
	if(form1.G2ChineseName.value!=''){
		if(form1.G2Nationality.value==''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterNation']?>");
			form1.G2Nationality.focus();
			return false;
		}
		else if(form1.G2Occupation.value==''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterOccupation']?>");
			form1.G2Occupation.focus();
			return false;
		}
		else if(form1.G2MobileNo.value==''){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['enterMobile']?>");
			form1.G2MobileNo.focus();
			return false;
		}
		else if(!/^[0-9]{8}$/.test(form1.G2MobileNo.value)){
			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['NumFormat']?>");
			form1.G2MobileNo.focus();
			return false;
		}
	}
	
	if(form1.StuMotherTongue.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['chooseNativeLang']?>");
		form1.StuMotherTongue.focus();
		return false;
	}
	else if(form1.IsKinderU.value==''){
		alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['chooseIsKinderU']?>");
		$('input#IsKinderU1').focus();
		return false;
	}
	
//	if(form1.OthersRelativeStudiedName1.value!=''){
//		if(form1.OthersRelativeAge1.value==''){
//			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['']?>");
//			$('input#OthersRelativeAge1').focus();
//			return false;
//		}
//		else if(form1.OthersRelativeGender1.value==''){
//			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['']?>");
//			$('input#OthersRelativeGender1_M').focus();
//			return false;
//		}
//		else if(form1.OthersRelativeClass1.value==''){
//			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['']?>");
//			$('select#OthersRelativeClass1').focus();
//			return false;
//		}
//		else if(form1.OthersRelativeStudiedYear1.value==''){
//			alert("<?=$Lang['Admission']['RMKG']['onlineform']['msg']['']?>");
//			$('select#OthersRelativeStudiedYear1').focus();
//			return false;
//		}
//	}
	
	return true;	
}

function check_docs_upload(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	//allow file size checking if the  
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	
	if(form1.StudentPersonalPhoto.value==''){
		alert("Please upload a personal photo.\n<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("Invalid File Format.\n<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		alert("File size exceeds limit.\n<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	}
	
//	var temp_count = (file_count > 1?1:file_count);
	var temp_count = file_count;

	for(var i=0;i<temp_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			if(i==0){
				alert("Please upload file.\n<?=$Lang['Admission']['msg']['uploadfile']?>");
				file_element.focus();
				return false;
			}
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("Invalid File Format.\n<?=$Lang['Admission']['msg']['invalidfileformat']?>");
			file_element.focus();
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			alert("File size exceeds limit.\n<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
			file_element.focus();
			return false;
		}
	}
//	if(file_count > 1){
//	for(var i=1;i<file_count;i++)
//	{
//		var file_element = file_ary.get(i);
//		var otherFileVal = file_element.value;
//		if(otherFileVal!=''){
//			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//			if(!isOldBrowser){
//				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//			}
//			if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//				alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
//				file_element.focus();
//				return false;
//			} else if(!isOldBrowser && otherFileSize > maxFileSize){
//				alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
//				file_element.focus();
//				return false;
//			}
//		}
//	}
//	}

	return true;
}

function checkBirthCertNo(){
	
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount > 3){
			document.getElementById('btn_addRow1').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow1').style.display = '';
		}
		 
	}
	else{
		if(rowCount > 3){
			document.getElementById('btn_addRow2').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow2').style.display = '';
		}
	}
	
	if(rowCount <= 4){                            // limit the user from creating fields more than your limits
		var row = table.insertRow(rowCount);
		var newcell = row.insertCell(0);
		newcell.className = "field_title";
		newcell.style.textAlign ="right";
		newcell.innerHTML = '('+rowCount+')';
		if(tableID == 'dataTable1'){
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchYear'+rowCount+'" type="text" id="OthersPrevSchYear'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchClass'+rowCount+'" type="text" id="OthersPrevSchClass'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchName'+rowCount+'" type="text" id="OthersPrevSchName'+rowCount+'" class="textboxtext" />';
		}
		else{
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedName'+rowCount+'" type="text" id="OthersRelativeStudiedName'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeRelationship'+rowCount+'" type="text" id="OthersRelativeRelationship'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedYear'+rowCount+'" type="text" id="OthersRelativeStudiedYear'+rowCount+'" class="textboxtext" />';
		}
	}else{
		 //alert("Maximum Passenger per ticket is 5");
			   
	}
}

function disableNotApplicable(id){
	
	var disableCheckBoxId = 'disable'+id;
	
	switch(id){
		
		case 'FormStudentInfo_ContactAddr':
			if($('input#'+disableCheckBoxId).attr('checked')){
				$('input#FormStudentInfo_ContactAddrChi').attr('disabled',true);
				$('input#FormStudentInfo_ContactAddrEng').attr('disabled',true);
			}
			else{
				$('input#FormStudentInfo_ContactAddrChi').removeAttr('disabled');
				$('input#FormStudentInfo_ContactAddrEng').removeAttr('disabled');
			}
		break;
		case 'CurrentSchool':
			if($('input#'+disableCheckBoxId).attr('checked')){
				$('table#CurrentSchoolTable').hide();
				$('table#CurrentSchoolTable').find('input, select, textarea').each(function(){
					
					$(this).attr('disabled',true);
				});
			}
			else{
				$('table#CurrentSchoolTable').show();
				$('table#CurrentSchoolTable').find('input, select, textarea').each(function(){
					$(this).removeAttr('disabled');
				});
			}
		break;
		default:
		if($('input#'+disableCheckBoxId).attr('checked')){
			$('input#'+id).attr('disabled',true);
		}
		else{
			$('input#'+id).removeAttr('disabled');
		}
	}
	
}

function showOtherTextField(id,value){

	switch(id){
		case 'FormStudentInfo_POB':
			if(value == 4){
				$('span#POB_Other').show();	
				$('input#FormStudentInfo_POB_Other').removeAttr('disabled');
			}
			else{
				$('span#POB_Other').hide();
				$('input#FormStudentInfo_POB_Other').attr('disabled',true);
			}
		break;
		case 'FormStudentInfo_Religion':
			if(value == 3){
				$('span#Religion_Other').show();	
				$('input#FormStudentInfo_Religion_Other').removeAttr('disabled');
			}
			else{
				$('span#Religion_Other').hide();
				$('input#FormStudentInfo_Religion_Other').attr('disabled',true);
			}
		break;
		case 'FormStudentInfo_StuIDType':
		if(value == 3){
				$('span#StuID_Other').show();	
				$('input#FormStudentInfo_StuIDType_Other').removeAttr('disabled');
			}
			else{
				$('span#StuID_Other').hide();
				$('input#FormStudentInfo_StuIDType_Other').attr('disabled',true);
			}
		break;
	}
    
}

function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount < 4){
			document.getElementById('btn_deleteRow1').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow1').style.display = '';
		}
	}
	else{
		if(rowCount < 4){
			document.getElementById('btn_deleteRow2').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow2').style.display = '';
		}
	}
	
	if(rowCount <= 2) {               // limit the user from removing all the fields
		//alert("Cannot Remove all the Passenger.");				
	}
	else{
		table.deleteRow(rowCount - 1);	
	}
}


</script>