<style>
.whereToKnowOptionDiv{
    margin-bottom: 5px;
}
</style>
<!--<h1>
    <?=$Lang['Admission']['otherInfo']?>
</h1>-->

<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:10px">
        <col style="width:20%">
        <col style="width:155px">
        <col style="width:155px">
        <col style="">
        <col style="width:155px">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$Lang['Admission']['name']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['gender']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['dateofbirth']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['CurrentSchool']?></td>
		<!--<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['ApplyingPIS']?></td>-->
	</tr>
	
	<?php for($i=0;$i<$lac::SIBLING_COUNT;$i++){ ?>
    	<tr>
    		<?php if($i == 0){ ?>
    			<td class="field_title" rowspan="3">
           			<?=$Lang['Admission']['PICLC']['SiblingInformation'] ?>
        		</td>
    		<?php }?>
    	
    		<td>
       			<?=$i+1?>)
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "Sibling{$i}_Name";
        		if($IsConfirm){
        		    echo $formData[$formName];
        		}else{
        		?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$SiblingInfo[$i]['EnglishName'] ?>"/>
        		<?php 
        		} 
        		?>
    		</td>
    		
    		<td class="form_guardian_field">
        		
            		<?php 
            		if($IsConfirm){ 
            		    if($formData["Sibling{$i}_Gender"] && $formData["Sibling{$i}_Gender"] != ' -- '){
            		        echo $Lang['Admission']['genderType'][$formData["Sibling{$i}_Gender"]];
            		    }else{
            		        echo ' -- ';
            		    }
            		}else{ 
            		    $checkedM = ($SiblingInfo[$i]['Gender'] == 'M')?'checked':'';
            		    $checkedF = ($SiblingInfo[$i]['Gender'] == 'F')?'checked':'';
            		?>
            			<input type="radio" value="M" id="Sibling<?=$i ?>_GenderM" name="Sibling<?=$i ?>_Gender" <?=$checkedM ?> />
            			<label for="Sibling<?=$i ?>_GenderM"><?=$Lang['Admission']['genderType']['M']?></label>
            			<input type="radio" value="F" id="Sibling<?=$i ?>_GenderF" name="Sibling<?=$i ?>_Gender" <?=$checkedF ?> >
            			<label for="Sibling<?=$i ?>_GenderF"><?=$Lang['Admission']['genderType']['F']?></label>
            		<?php 
            		} 
            		?>
        		
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "Sibling{$i}_DOB";
        		
        		if($IsConfirm){
        		    echo $formData[$formName];
        		}else{
        		?>
    				<input 
        				type="text" 
        				class="textboxtext datepicker" 
        				placeholder="YYYY-MM-DD" 
        				maxlength="10" 
        				size="15"
        				style="width: 110px;"
        				 
        				id="<?=$formName ?>" 
        				name="<?=$formName ?>" 
        				value="<?=$SiblingInfo[$i]['DOB'] ?>" 
    				/>
        		<?php 
        		}
        		?>
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "Sibling{$i}_CurrentSchool";
        		if($IsConfirm){
        		    echo $formData[$formName];
        		}else{
        		?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$SiblingInfo[$i]['CurrentSchool'] ?>"/>
        		<?php 
        		} 
        		?>
    		</td>
    		
    		<!--<td class="form_guardian_field">
        		
            		<?php 
            		if($IsConfirm){ 
        		        if($formData["Sibling{$i}_ApplyingSameSchool"] == 'Y'){
        		            echo $Lang['Admission']['yes'];
        		        }else if($formData["Sibling{$i}_ApplyingSameSchool"] == 'N'){
        		            echo $Lang['Admission']['no'];
        		        }else{
        		            echo ' -- ';
        		        }
            		}else{ 
            		    $checkedY = ($SiblingInfo[$i]['ApplyingSameSchool'] == 'Y')?'checked':'';
            		    $checkedN = ($SiblingInfo[$i]['ApplyingSameSchool'] == 'N')?'checked':'';
            		?>
            			<input type="radio" value="Y" id="Sibling<?=$i ?>_ApplyingSameSchoolY" name="Sibling<?=$i ?>_ApplyingSameSchool" <?=$checkedY ?> />
            			<label for="Sibling<?=$i ?>_ApplyingSameSchoolY"><?=$Lang['Admission']['yes'] ?></label>
            			
            			&nbsp;
            			&nbsp;
            			
            			<input type="radio" value="N" id="Sibling<?=$i ?>_ApplyingSameSchoolN" name="Sibling<?=$i ?>_ApplyingSameSchool" <?=$checkedN ?> />
            			<label for="Sibling<?=$i ?>_ApplyingSameSchoolN"><?=$Lang['Admission']['no'] ?></label>
            		<?php 
            		} 
            		?>
        		
    		</td>-->
    	</tr>
	<?php } ?>
</table>

<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="70%">
</colgroup>

<!--<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['SpecialHolidayCelebrate']?> 
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['SpecialHolidayCelebrate']?>
		<?php }else{ ?>
			<input name="SpecialHolidayCelebrate" type="text" id="SpecialHolidayCelebrate" class="textboxtext" value="<?=$allCustInfo['SpecialHolidayCelebrate'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>-->

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['WhyChoosePIS']?> 
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['WhyChoosePIS']?>
		<?php }else{ ?>
			<input name="WhyChoosePIS" type="text" id="WhyChoosePIS" class="textboxtext" value="<?=$allCustInfo['WhyChoosePIS'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		How do you know about Princeton Sky Lake International School?
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		    if(empty($formData['WhereToKnowPIS'])){
		        echo ' -- ';
		    }else{
		        foreach($formData['WhereToKnowPIS'] as $whereToKnowId){
		            $whereToKnow = $admission_cfg['WhereToKnowPis'][$whereToKnowId];
		            $details = $formData["WhereToKnowPIS_{$whereToKnow}Details"];
        ?>
	            	<div>
	            		<span style="width: 200px;display: inline-block;">
	            			<?=$Lang['Admission']['PICLC']['KnowPIS'][$whereToKnow] ?>
	            		</span>
	            		
	            		<?php if($details){ ?>
	            			<span>( <?=$details ?> )</span>
	            		<?php } ?>
	            	</div>
        <?php
		        }
		    }
		}else{ 
	        $whereToKnowStr = $allCustInfo["WhereToKnowPIS"][0]['Value'];
	        $whereToKnowArr = explode(',', $whereToKnowStr);
		    foreach($admission_cfg['WhereToKnowPis'] as $value => $whereToKnow){ 
		        $checked = (in_array($value, $whereToKnowArr))?'checked':'';
	    ?>
    			<div class="whereToKnowOptionDiv">
        			<input type="checkbox" id="WhereToKnowPIS_<?=$whereToKnow ?>" name="WhereToKnowPIS[]" value="<?=$value ?>" <?=$checked ?> />
        			<label for="WhereToKnowPIS_<?=$whereToKnow ?>" style="margin-bottom: 5px;">
            			<?=$Lang['Admission']['PICLC']['KnowPIS'][$whereToKnow] ?>
                    </label>
                    
                    <?php if(in_array($whereToKnow, $admission_cfg['WhereToKnowPisDetails'])){ ?>
                        (
                            <label for="WhereToKnowPIS_<?=$whereToKnow ?>Details">
                            	<?=$Lang['Admission']['PICLC']['PleaseSpecify'] ?>:
                        	</label>
                        	<input id="WhereToKnowPIS_<?=$whereToKnow ?>Details" name="WhereToKnowPIS_<?=$whereToKnow ?>Details" value="<?=$allCustInfo["WhereToKnowPIS_{$whereToKnow}Details"][0]['Value'] ?>" />
                        )
                    <?php } ?>
                </div>
        <?php 
		    }
		} 
		?>
	</td>
</tr>
<tr>	
   	<td class="field_title">
		<?=$Lang['Admission']['otherInfo']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['OtherInformation'];
		}else{
		?>
			<input id="OtherInformation" name="OtherInformation" class="textboxtext" value="<?=$allCustInfo['OtherInformation'][0]['Value']?>" />
		<?php
		}
		?>
	</td>
</tr>
</table>