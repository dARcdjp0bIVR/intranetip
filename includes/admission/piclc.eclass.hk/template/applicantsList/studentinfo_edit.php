<?php

global $libkis_admission;

$star = $mustfillinsymbol;
$StudentInfo = $applicationInfo;


?>
<style>
select:disabled{
    color: #ccc;
}
.col2div{
    display:inline-block;
    width:48%;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%;"/>
		<col style="width: 40%;"/>
		<col style="width: 30%;"/>
	</colgroup>
	<tbody>
	
	
		<!-- ######## Basic Information START ######## -->
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
        		<div class="col2div">
            		<label for="StudentChineseSurname">
                		<?=$kis_lang['Admission']['PICLC']['surname'] ?>
            		</label>
        			<input name="StudentChineseSurname" type="text" id="StudentChineseSurname" class="textboxtext"  value="<?=$StudentInfo['ChineseSurname']?>"/>
        		</div>
        		&nbsp;
        		<div class="col2div">
            		<label for="StudentChineseFirstName">
                		<?=$kis_lang['Admission']['PICLC']['firstName'] ?>
                    </label>
        			<input name="StudentChineseFirstName" type="text" id="StudentChineseFirstName" class="textboxtext"  value="<?=$StudentInfo['ChineseFirstName']?>"/>
        		</div>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id = "btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align:center;">
						<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
        		<div class="col2div">
            		<label for="StudentEnglishSurname">
                		<?=$kis_lang['Admission']['PICLC']['surname'] ?>
            		</label>
        			<input name="StudentEnglishSurname" type="text" id="StudentEnglishSurname" class="textboxtext"  value="<?=$StudentInfo['EnglishSurname']?>"/>
        		</div>
        		&nbsp;
        		<div class="col2div">
            		<label for="StudentEnglishFirstName">
                		<?=$kis_lang['Admission']['PICLC']['firstName'] ?>
                    </label>
        			<input name="StudentEnglishFirstName" type="text" id="StudentEnglishFirstName" class="textboxtext"  value="<?=$StudentInfo['EnglishFirstName']?>"/>
        		</div>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?=$star?><?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
			<input name="StudentDateOfBirth" id="StudentDateOfBirth" value="<?=$StudentInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
    		<td>
    			<?=$libinterface->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', ($StudentInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
    			<?=$libinterface->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', ($StudentInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
    		</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?><?=$kis_lang['Admission']['PICLC']['birthCertNo'] ?>
			</td>
			<td nowrap="" colspan="2">
    			<!--<select id="StudentBirthCertType" name="StudentBirthCertType" style="margin-bottom: 5px;">
    				<?php 
    				foreach($admission_cfg['BirthCertType'] as $index => $type) { 
    				    $selected = ($type == $StudentInfo['BirthCertTypeOther'])? 'selected' : '';
    				?>
    					<option value="<?=$type ?>" <?=$selected ?> ><?=$kis_lang['Admission']['PICLC']['birthCertType'][ $type ] ?></option>
    				<?php 
    				} 
    				?>
    			</select>-->
    			<input name="StudentBirthCertNo" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$StudentInfo['birthcertno'] ?>"/>
			</td>
		</tr>
		<!-- ######## Basic Information END ######## -->
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['PICLC']['PlaceOfIssue'] ?>
			</td>
			<td>
    			<input id="StudentRegistedResidence" name="StudentRegistedResidence" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirth'] ?>" />
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['nationality'] ?>
			</td>
			<td>
    			<input id="StudentCounty" name="StudentCounty" class="textboxtext" value="<?=$StudentInfo['Nationality'] ?>" />
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['PICLC']['nativePlace'] ?>
			</td>
			<td>
    			<input id="StudentNativePlace" name="StudentNativePlace" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirthOther'] ?>" />
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['PICLC']['homeAddress'] ?>
			</td>
			<td>
    			<input id="StudentAddress" name="StudentAddress" class="textboxtext" value="<?=$StudentInfo['Address'] ?>" />
			</td>
		</tr>
		
		<!--<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['PICLC']['registedResidence'] ?>
			</td>
			<td>
    			<input id="StudentRegistedResidence" name="StudentRegistedResidence" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirth'] ?>" />
			</td>
		</tr>-->
		
		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$kis_lang['Admission']['contactEmail'] ?>
			</td>
			<td>
    			<input id="StudentEmail" name="StudentEmail" class="textboxtext" value="<?=$StudentInfo['Email'] ?>" />
			</td>
		</tr>
		
		
		<!-- ######## Document START ######## --> 
    	<?php
    	for($i=0;$i<sizeof($attachmentSettings);$i++) {
    	    
    	    #### Check ClassLevel START ####
    	    if($attachmentSettings[$i]['ClassLevelStr']){
    	        $classLevelArr = explode(',', $attachmentSettings[$i]['ClassLevelStr']);
    	    
    	        if(!in_array($StudentInfo['classLevelID'], $classLevelArr)){
    	            continue;
    	        }
    	    }
    	    #### Check ClassLevel END ####
    	    
    	    
    		$originalAttachmentName = $attachment_name = $attachmentSettings[$i]['AttachmentName'];
    		 
    		$_filePath = $attachmentList[$attachment_name]['link'];
    		if($_filePath){
    			$_spanDisplay = '';
    			$_buttonDisplay = ' style="display:none;"';
    		}else{
    			$_filePath = '';
    			$_spanDisplay = ' style="display:none;"';
    			$_buttonDisplay = '';
    		}
    		
//    		$tempAttachmentName = '';
//    		foreach($admission_cfg['BirthCertType'] as $type){
//    		    if($kis_lang['Admission']['PICLC']['documentUpload'][$type][$i]){
//    		        $tempAttachmentName .= "<span id=\"docUploadRemarks\" class=\"docUpload docUpload_{$type}\" style=\"display:none;\">{$kis_lang['Admission']['PICLC']['documentUpload'][$type][$i]}</span>";
//    		    }
//    		}
//    		$attachment_name = ($tempAttachmentName)?$tempAttachmentName:$attachment_name;
    		
    		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
    		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';
    	?>
    		<tr>
    			<td class="field_title">
    				<?=$attachment_name?>
    			</td>
    			<td id="<?=$originalAttachmentName?>">
    				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
    				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$kis_lang['Upload']?>"  id="uploader-<?=$originalAttachmentName?>" <?=$_buttonDisplay?>/>
    			</td>
    		</tr>	
    	<?php
    	}
    	?>
		<!-- ######## Document END ######## --> 
	</tbody>
</table>

<script>
//// UI Releated START ////
////UI Releated END ////

	$('#StudentBirthCertType').change(function(){
		$('.docUpload').hide();
		$('.docUpload_' + $(this).val()).show();
	}).change();
	
var dOBRange;
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	return check_student_form();
}


function check_student_form(){
	//For debugging only
// 	return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;

	var form1 = applicant_form;
	dOBRange = dOBRange || ['',''];
	/******** Basic init END ********/
	

	/**** Name START ****/
	if(
		!checkNaNull($('#StudentChineseSurname').val()) && 
		!checkIsChineseCharacter($('#StudentChineseSurname').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		$('#StudentChineseSurname').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentChineseFirstName').val()) && 
		!checkIsChineseCharacter($('#StudentChineseFirstName').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		$('#StudentChineseFirstName').focus();
		return false;
	}
	
	
	if($('#StudentEnglishSurname').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishname']?>");	
		$('#StudentEnglishSurname').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishSurname').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishSurname').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		$('#StudentEnglishSurname').focus();
		return false;
	}
	
	
	if($('#StudentEnglishFirstName').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishname']?>");	
		$('#StudentEnglishFirstName').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishFirstName').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishFirstName').val())
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		$('#StudentEnglishFirstName').focus();
		return false;
	}
	/**** Name END ****/

	/**** DOB START ****/
	if( !$('#StudentDateOfBirth').val().match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/) ){
		if($('#StudentDateOfBirth').val()!=''){
			alert(" <?=$kis_lang['Admission']['msg']['invaliddateformat']?>");
		}else{
			alert(" <?=$kis_lang['Admission']['msg']['enterdateofbirth']?>");	
		}
		
		$('#StudentDateOfBirth').focus();
		return false;
	} 
	if(
		(
			dOBRange[0] !='' && 
			$('#StudentDateOfBirth').val() < dOBRange[0] 
		) || (
			dOBRange[1] !='' && 
			$('#StudentDateOfBirth').val() > dOBRange[1]
		)
	){
		alert(" <?=$kis_lang['Admission']['msg']['invalidbdaydateformat']?>");
		$('#StudentDateOfBirth').focus();
		return false;
	} 
	/**** DOB END ****/

	/**** Gender START ****/
//	if($('input[name=StudentGender]:checked').length == 0){
//		alert(" <?=$kis_lang['Admission']['msg']['selectgender']?>");	
//		$('input[name=StudentGender]:first').focus();
//		return false;
//	}
	/**** Gender END ****/
	
	/**** Personal Identification START ****/
	if($('#StudentBirthCertNo').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterPersonalIdentificationNo']?>");	
		$('#StudentBirthCertNo').focus();
		return false;
	}
// 	if(!check_hkid(form1.StudentBirthCertNo.value)){
//    	alert(" <?=$kis_lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>\n Invalid Birth Cert No.");
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }
    
//     if(!isUpdatePeriod && checkBirthCertNo() > 0){
//    	alert(" <?=$kis_lang['Admission']['SHCK']['msg']['duplicateBirthCertNo']?>\n The Birth Cert No. is used for admission! Please enter another Birth Cert No.");	
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }
	/**** Personal Identification END ****/


	/**** Nationality START ****/
//	if($('#StudentCounty').val().trim()==''){
//		alert(" <?=$kis_lang['Admission']['msg']['enternationality']?>");	
//		$('#StudentCounty').focus();
//		return false;
//	}
	/**** Nationality END ****/

	/**** Address START ****/
	if($('#StudentAddress').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['msg']['enterhomeaddress']?>");	
		$('#StudentAddress').focus();
		return false;
	}
	/**** Address END ****/

	/**** Registed Residence START ****/
//	if($('#StudentRegistedResidence').val().trim()==''){
//		alert("<?=$kis_lang['Admission']['PICLC']['msg']['enterRegistedResidence']?>");	
//		$('#StudentRegistedResidence').focus();
//		return false;
//	}
	/**** Registed Residence END ****/

	/**** Native Place START ****/
//	if($('#StudentNativePlace').val().trim()==''){
//		alert("<?=$kis_lang['Admission']['PICLC']['msg']['enterNativePlace']?>");	
//		$('#StudentNativePlace').focus();
//		return false;
//	}
	/**** Native Place END ****/
	

	/**** Email START ****/
	if($('#StudentEmail').val().trim()==''){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	if($('#StudentEmail').val().trim()!='' && !re.test($('#StudentEmail').val().trim())){
		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	/**** Email END ****/
	
	return true;
}
</script>