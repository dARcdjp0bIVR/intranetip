<h1><?= $Lang['Admission']['otherInfo'] ?> Other Information</h1>


<table id="dataTable1" class="form_table" style="font-size: 13px">
    <tr>
        <td rowspan="4" class="field_title">
            <?= $Lang['Admission']['SFAEPS']['currentSiblingInformation'] ?><br/>
            <?= $LangEn['Admission']['SFAEPS']['currentSiblingInformation'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['HKUGAPS']['nameChi'] ?> Name (Chinese)</center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['HKUGAPS']['nameEng'] ?> Name (English)</center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['SFAEPS']['studentNo'] ?> <?= $LangEn['Admission']['SFAEPS']['studentNo'] ?></center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["OthersRelativeStudiedNameChi{$i}"];
                } else { ?>
                    <input name="OthersRelativeStudiedNameChi<?= $i ?>" type="text"
                           id="OthersRelativeStudiedNameChi<?= $i ?>"
                           class="textboxtext" value="<?= $siblingsInfoArr[$i]['name'] ?>"/>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["OthersRelativeStudiedNameEng{$i}"];
                } else { ?>
                    <input name="OthersRelativeStudiedNameEng<?= $i ?>" type="text"
                           id="OthersRelativeStudiedNameEng<?= $i ?>"
                           class="textboxtext" value="<?= $siblingsInfoArr[$i]['englishName'] ?>"/>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["OthersRelativeStudiedStudentId{$i}"];
                } else { ?>
                    <input name="OthersRelativeStudiedStudentId<?= $i ?>" type="text"
                           id="OthersRelativeStudiedStudentId<?= $i ?>"
                           class="textboxtext" value="<?= $siblingsInfoArr[$i]['studentId'] ?>"/>
                <?php } ?>
            </td>
        </tr>
    <?php endfor; ?>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
    <tr>
        <td rowspan="4" class="field_title">
            <?= $Lang['Admission']['SFAEPS']['graduatedSiblingInformation'] ?><br/>
            <?= $LangEn['Admission']['SFAEPS']['graduatedSiblingInformation'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['relationship'] ?> Relationship</center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['HKUGAPS']['nameChi'] ?> Name (Chinese)</center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['HKUGAPS']['nameEng'] ?> Name (English)</center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['HKUGAPS']['graduationYear'] ?> Graduation Year</center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <?php
                if ($IsConfirm) {
                    $relstionship = $formData["OthersRelativeGraduationRelationship{$i}"];
                    switch ($relstionship) {
                        case 'S':
                            echo $Lang['Admission']['Sibling'];
                            break;
                        case 'F':
                        case 'M':
                            echo $Lang['Admission']['PG_Type'][$relstionship];
                            break;
                        default:
                            echo $relstionship;
                    }
                } else {
                    $relstionship = $siblingsInfoArr[$i + 3]['relationship'];
                    ?>
                    <select name="OthersRelativeGraduationRelationship<?= $i ?>"
                            id="OthersRelativeGraduationRelationship<?= $i ?>"
                            class="textboxtext"
                    >
                        <option value="" <?= ($relstionship == '') ? 'selected' : '' ?>><?= $Lang['Admission']['pleaseSelect'] ?> <?= $LangEn['Admission']['pleaseSelect'] ?></option>
                        <option value="F" <?= ($relstionship == 'F') ? 'selected' : '' ?>><?= $Lang['Admission']['PG_Type']['F'] ?> <?= $LangEn['Admission']['PG_Type']['F'] ?></option>
                        <option value="M" <?= ($relstionship == 'M') ? 'selected' : '' ?>><?= $Lang['Admission']['PG_Type']['M'] ?> <?= $LangEn['Admission']['PG_Type']['M'] ?></option>
                        <option value="S" <?= ($relstionship == 'S') ? 'selected' : '' ?>><?= $Lang['Admission']['Sibling'] ?> <?= $LangEn['Admission']['Sibling'] ?></option>
                    </select>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["OthersRelativeGraduationNameChi{$i}"];
                } else { ?>
                    <input name="OthersRelativeGraduationNameChi<?= $i ?>" type="text"
                           id="OthersRelativeGraduationNameChi<?= $i ?>"
                           class="textboxtext" value="<?= $siblingsInfoArr[$i + 3]['name'] ?>"/>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["OthersRelativeGraduationNameEng{$i}"];
                } else { ?>
                    <input name="OthersRelativeGraduationNameEng<?= $i ?>" type="text"
                           id="OthersRelativeGraduationNameEng<?= $i ?>"
                           class="textboxtext" value="<?= $siblingsInfoArr[$i + 3]['englishName'] ?>"/>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php
                if ($IsConfirm) {
                    echo ($formData["OthersRelativeGraduationYear{$i}"]) ? $formData["OthersRelativeGraduationYear{$i}"] : ' -- ';
                } else {
                    ?>
                    <input
                            name="OthersRelativeGraduationYear<?= $i ?>"
                            type="text"
                            id="OthersRelativeGraduationYear<?= $i ?>"
                            class="textboxtext" value="<?= $siblingsInfoArr[$i + 3]['year'] ?>"
                            maxlength="4"
                    />
                    <?php
                }
                ?>
            </td>
        </tr>
    <?php endfor; ?>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
    <tr>
        <td rowspan="4" class="field_title">
            <?= $Lang['Admission']['SFAEPS']['awards'] ?><br/>
            <?= $LangEn['Admission']['SFAEPS']['awards'] ?>
        </td>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['SFAEPS']['item'] ?> <?= $LangEn['Admission']['SFAEPS']['item'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['SFAEPS']['award'] ?> <?= $LangEn['Admission']['SFAEPS']['award'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $Lang['Admission']['SFAEPS']['year'] ?> <?= $LangEn['Admission']['SFAEPS']['year'] ?></center>
        </td>
    </tr>
    <?php for ($i = 1; $i <= 3; $i++): ?>
        <tr>
            <td class="field_title" style="text-align:right;width:50px;">(<?= $i ?>)</td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["awardItem{$i}"];
                } else { ?>
                    <input name="awardItem<?= $i ?>" type="text"
                           id="awardItem<?= $i ?>"
                           class="textboxtext" value="<?= $allCustInfo['AwardItem'][$i-1]['Value'] ?>"/>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["awardAward{$i}"];
                } else { ?>
                    <input name="awardAward<?= $i ?>" type="text"
                           id="awardAward<?= $i ?>"
                           class="textboxtext" value="<?= $allCustInfo['AwardAward'][$i-1]['Value'] ?>"/>
                <?php } ?>
            </td>
            <td class="form_guardian_field">
                <?php if ($IsConfirm) {
                    echo $formData["awardYear{$i}"];
                } else { ?>
                    <input name="awardYear<?= $i ?>" type="text"
                           id="awardYear<?= $i ?>"
                           class="textboxtext" value="<?= $allCustInfo['AwardYear'][$i-1]['Value'] ?>" maxlength="4"/>
                <?php } ?>
            </td>
        </tr>
    <?php endfor; ?>
</table>
