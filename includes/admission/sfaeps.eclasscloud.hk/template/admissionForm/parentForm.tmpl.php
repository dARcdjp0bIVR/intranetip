<h1><?=$Lang['Admission']['PGInfo']?> Parent Information</h1>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
    </colgroup>
    <tr>
    	<td>
    		<span>如沒有資料，例如單親家庭，請輸入 '<font style="color:red;">沒有</font>'</span>
    		<br/>
    		<span>If no information, e.g. single-parent family, fill in '<font style="color:red;">Nil</font>'</span>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['F'] ?> <?=$LangEn['Admission']['PG_Type']['F'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['M'] ?> <?=$LangEn['Admission']['PG_Type']['M'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['G'] ?> <?=$LangEn['Admission']['PG_Type']['G'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['EC'] ?> <?=$LangEn['Admission']['PG_Type']['EC'] ?></center></td>
	
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star.$Lang['Admission']['SFAEPS']['nameChi'] ?> <?=$LangEn['Admission']['SFAEPS']['nameChi']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1ChineseName'];
    		}else{?>
    			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$parentInfoArr['F']['chinesename'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2ChineseName'];
    		}else{?>
    			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$parentInfoArr['M']['chinesename'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3ChineseName'];
    		}else{?>
    			<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" value="<?=$parentInfoArr['G']['chinesename'] ?>"/>
    		<?php } ?>
		</td>
				<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G4ChineseName'];
    		}else{?>
    			<input name="G4ChineseName" type="text" id="G4ChineseName" class="textboxtext" value="<?=$parentInfoArr['E']['chinesename'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star.$Lang['Admission']['SFAEPS']['nameEng'] ?> <?=$LangEn['Admission']['SFAEPS']['nameEng']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1EnglishName'];
    		}else{?>
    			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$parentInfoArr['F']['englishname'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2EnglishName'];
    		}else{?>
    			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$parentInfoArr['M']['englishname'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3EnglishName'];
    		}else{?>
    			<input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext" value="<?=$parentInfoArr['G']['englishname'] ?>"/>
    		<?php } ?>
		</td>
				<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G4EnglishName'];
    		}else{?>
    			<input name="G4EnglishName" type="text" id="G4EnglishName" class="textboxtext" value="<?=$parentInfoArr['E']['englishname'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['HKUGAPS']['contactNum']?> Contact Number
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1MobileNo'];
    		}else{?>
    			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$parentInfoArr['F']['mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2MobileNo'];
    		}else{?>
    			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$parentInfoArr['M']['mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3MobileNo'];
    		}else{?>
    			<input name="G3MobileNo" type="text" id="G3MobileNo" class="textboxtext" value="<?=$parentInfoArr['G']['mobile'] ?>"/>
    		<?php } ?>
		</td>
				<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G4MobileNo'];
    		}else{?>
    			<input name="G4MobileNo" type="text" id="G4MobileNo" class="textboxtext" value="<?=$parentInfoArr['E']['mobile'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<!--<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['email']?> E-mail
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Email'];
    		}else{?>
    			<input name="G1Email" type="text" id="G1Email" class="textboxtext" value="<?=$parentInfoArr['F']['email'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Email'];
    		}else{?>
    			<input name="G2Email" type="text" id="G2Email" class="textboxtext" value="<?=$parentInfoArr['M']['email'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3Email'];
    		}else{?>
    			<input name="G3Email" type="text" id="G3Email" class="textboxtext" value="<?=$parentInfoArr['G']['email'] ?>"/>
    		<?php } ?>
		</td>
				<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G4Email'];
    		}else{?>
    			<input name="G4Email" type="text" id="G4Email" class="textboxtext" value="<?=$parentInfoArr['E']['email'] ?>"/>
    		<?php } ?>
		</td>
	</tr>-->
	<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['occupation']?> Occupation
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Occupation'];
    		}else{?>
    			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$parentInfoArr['F']['occupation'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Occupation'];
    		}else{?>
    			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$parentInfoArr['M']['occupation'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3Occupation'];
    		}else{?>
    			<input name="G3Occupation" type="text" id="G3Occupation" class="textboxtext" value="<?=$parentInfoArr['G']['occupation'] ?>"/>
    		<?php } ?>
		</td>
				<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G4Occupation'];
    		}else{?>
    			<input name="G4Occupation" type="text" id="G4Occupation" class="textboxtext" value="<?=$parentInfoArr['E']['occupation'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
</table>

<!-- 	<tr> -->
<!-- 		<td class="field_title"> -->
<!--    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?> -->
<!--    		<?=$Lang['Admission']['HKUGAPS']['nameInstitution']?> Name of Institution -->
<!-- 		</td> -->
<!-- 		<td class="form_guardian_field"> -->
    		<?php //if($IsConfirm){
//     		    echo $formData['G1Institution'];
//     		}else{?>
<!--    			<input name="G1Institution" type="text" id="G1Institution" class="textboxtext" value="<?=$parentInfoArr['F']['companyname'] ?>"/> -->
    		<?php //} ?>
<!-- 		</td> -->
<!-- 		<td class="form_guardian_field"> -->
    		<?php //if($IsConfirm){
//     		    echo $formData['G2Institution'];
//     		}else{?>
<!--    			<input name="G2Institution" type="text" id="G2Institution" class="textboxtext" value="<?=$parentInfoArr['M']['companyname'] ?>"/> -->
    		<?php //} ?>
<!-- 		</td> -->
<!-- 		<td class="form_guardian_field"> -->
    		<?php //if($IsConfirm){
//     		    echo $formData['G3Institution'];
//     		}else{?>
<!--    			<input name="G3Institution" type="text" id="G3Institution" class="textboxtext" value="<?=$parentInfoArr['G']['companyname'] ?>"/> -->
    		<?php //} ?>
<!-- 		</td> -->
<!-- 	</tr> -->

