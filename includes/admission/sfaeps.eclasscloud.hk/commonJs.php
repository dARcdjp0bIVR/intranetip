<!-- Using: Pun -->
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<style>
    .ui-autocomplete {
        max-height: 200px;
        max-width: 200px;
        overflow-y: auto;
        overflow-x: hidden;
        font-size: 12px;
        font-family: Verdana, "微軟正黑體";
    }

    .ui-autocomplete-category {
        font-style: italic;
    }

    .ui-datepicker {
        font-size: 12px;
        width: 210px;
        font-family: Verdana, "微軟正黑體";
    }

    .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
        width: auto;
    }

    .ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td {
        background-color: #fff7a3
    }
</style>
<script type="text/javascript">


    // autocomplete for inputselect fields
    $('.inputselect').each(function () {
        var this_id = $(this).attr('id');
        if ($(this).length > 0) {
            $(this).autocomplete(
                "ajax_get_suggestions.php",
                {
                    delay: 3,
                    minChars: 1,
                    matchContains: 1,
                    extraParams: {'field': this_id},
                    autoFill: false,
                    overflow_y: 'auto',
                    overflow_x: 'hidden',
                    maxHeight: '200px'
                }
            );
        }
    });

    if($('#religion').val() != '0')
    	$('#religionFile').prop('hidden',true);
        
    $('#religion').change(function(){
		if($('#religion').val() == '0'){
			$('#religionFile').removeAttr('hidden');
			$('.religionFileUpload').attr('data-is-required',1);
		}else{
			if(!$('#religionFile').prop('hidden')){
    			$('#religionFile').prop('hidden',true);
			}
    		$('.religionFileUpload').attr('data-is-required',0);
    		$('.religionFileUpload').val('');
		}
    });


    var isUpdatePeriod = <?=($allowToUpdate) ? 'true' : 'false'; ?>;

    kis.datepicker('#StudentDateOfBirth, #OthersRelativeBirth1, #OthersRelativeBirth2');

    if (isUpdatePeriod) {
        kis.datepicker('#InputStudentDateOfBirth');
    }

    var dOBRange = new Array();

    //--- added to disable the back button [start]
    function preventBack() {
        window.onbeforeunload = '';
        window.history.forward();
        window.onbeforeunload = function (evt) {
            var message = '<?=$Lang['Admission']['msg']['infolost']?>';
            if (typeof evt == 'undefined') {
                evt = window.event;
            }
            if (evt) {
                evt.returnValue = message;
            }
            return message;
        }
    }

    window.onunload = function () {
        null;
    };
    var preventBackTimeout = setTimeout("preventBack()", 0);
    //--- added to disable the back button [end]

    var timer;
    var timeUp = false;

    function autoSubmit(form1) {
        clearTimeout(timer);
        var isValid = true;
        isValid = check_choose_class2(form1);
        if (isValid)
            isValid = check_input_info2(form1);
        if (isValid)
            isValid = check_docs_upload2(form1);
        //alert('You used 3 seconds! The validation of the form: '+isValid);
        if (!isValid) {
            alert('<?=$Lang['Admission']['msg']['timeup']?>\nThe time is up! Please apply again!');
            window.onbeforeunload = '';
            window.location.href = 'submit_time_out.php?sus_status=' + $('input:radio[name=sus_status]:checked').val();
        } else {
            alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>/nThe time is up!\nThe admission form will auto submit after pressing \'OK\'!");
            window.onbeforeunload = '';
            form1.submit();
        }

    }

    function check_choose_class2(form1) {
        if ($('input:radio[name=sus_status]:checked').val() == null) {
            return false;
        } else {
            return true;
        }
    }

    function check_input_info2(form1) {
        var _alert = window.alert;
        window.alert = function (a) { /*console.log(a);*/
        };

        var isValid = check_input_info(form1);

        window.alert = _alert;
        return isValid;
    }

    function check_docs_upload2(form1) {
        var _alert = window.alert;
        window.alert = function (a) { /*console.log(a);*/
        };

        var isValid = check_docs_upload(form1);

        window.alert = _alert;
        return isValid;
    }

    /*
    hkid format:  A123456(7)
    A1234567
    AB123456(7)
    AB1234567
    */
    function check_hkid(hkid) {
// hkid = $.trim(hkid);
// hkid = hkid.replace(/\s/g, '');
// hkid = hkid.toUpperCase();
// $(":input[name='id_no']").val(hkid);

        var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
        var ra = re.exec(hkid);

        if (ra != null) {
            var p1 = ra[1];
            var p2 = ra[2];
            var p3 = ra[4];
            var check_sum = 0;
            if (p1.length == 2) {
                check_sum = (p1.charCodeAt(0) - 55) * 9 + (p1.charCodeAt(1) - 55) * 8;
            } else if (p1.length == 1) {
                check_sum = 324 + (p1.charCodeAt(0) - 55) * 8;
            }

            check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
            var check_digit = 11 - (check_sum % 11);
            if (check_digit == '11') {
                check_digit = 0;
            } else if (check_digit == '10') {
                check_digit = 'A';
            }
            if (check_digit == p3) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function checkIsChineseCharacter(str) {
        return str.match(/^[\u3400-\u9FBF]*$/);
    }

    function checkIsEnglishCharacter(str) {
        return str.match(/^[A-Za-z ,\-]*$/);
    }

    function checkNaNull(str) {
        return (
            ($.trim(str).toLowerCase() == '沒有') ||
            ($.trim(str).toLowerCase() == 'nil') ||
            ($.trim(str).toLowerCase() == 'n.a.')
        );
    }

    function goto(current, page) {
        var isValid = true;
        if (page == 'step_instruction') {
//		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
//	        alert("表格需以 Google Chrome、Firefox 或 Internet Explorer 10 或以上瀏覽器填寫。");
//	        return;
//	    }
            clearTimeout(timer);
            isValid = check_choose_class($("form")[0]);
        } else if (page == 'step_docs_upload') {
            //alert($("form").serialize());
            isValid = check_input_info($("form")[0]);
        } else if (page == 'step_confirm') {
            isValid = check_docs_upload($("form")[0]);
        }

        if (current == 'step_instruction' && page == 'step_input_form') {
            var chk_ary = $('input#Agree');
            var chk_count = chk_ary.length;
            for (var i = 0; i < chk_count; i++) {
                var chk_element = chk_ary.get(i);
                if (chk_element.checked == false) {
                    alert("請剔選 本人同意上述有關條款及細則。\nPlease tick I agree the terms and conditions as stated above.");
                    isValid = false;
                }
            }
        }

        // Henry added [20151013]
        if (current == 'step_index' && page == 'step_update_input_form') {
            if (form1.InputApplicationID.value == '') {
                alert("請輸入申請編號。\nPlease enter Application Number.");
                form1.InputApplicationID.focus();
                return false;
            } else if (form1.InputStudentBirthCertNo.value == '') {
                alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>\nPlease enter Birth Certificate Number.");
                form1.InputStudentBirthCertNo.focus();
                return false;
            }
// 		else if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.InputStudentBirthCertNo.value)){
//			alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
// 			form1.InputStudentBirthCertNo.focus();
// 			return false;
// 		}
            else if (!form1.InputStudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)) {
                if (form1.InputStudentDateOfBirth.value != '') {
                    alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
                } else {
                    alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");
                }

                form1.InputStudentDateOfBirth.focus();
                return false;
            }

            <?if($lac->IsAfterUpdatePeriod()){?>
            clearTimeout(preventBackTimeout);
            window.onbeforeunload = '';
            <?}?>

            /* Clear result div*/
            $("#DayTypeOption").html('');

            /* Get some values from elements on the page: */
            var values = $("#form1").serialize();

            /* Send the data using post and put the results in a div */
            $.ajax({
                url: "ajax_valid_for_update_form.php",
                type: "post",
                data: values,
                async: false,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!");
                    $("#InputApplicationID").val('');
                    $("#InputStudentBirthCertNo").val('');
                    $("#InputStudentDateOfBirth").val('');
                    if (data == 0) {
                        alert("申請編號，出生證明書號碼或出生日期不正確。\nIncorrect Application Number, Birth Certificate Number or Date of Birth.");
                        form1.InputStudentBirthCertNo.focus();
                        isValid = false;
                        return false;
                    }
//		           $("#applicationno").val(data);
//		           $("#divInterviewResult").html(data);
//		           document.getElementById(current).style.display = "none";
//				   document.getElementById(page).style.display = "";
                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });

            /* Send the data using post and put the results in a div */
            $.ajax({
                url: "ajax_get_input_form.php",
                type: "post",
                data: values,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!"+values);
                    $("#step_update_input_form").html(data);
                    kis.datepicker('#StudentDateOfBirth');

                    if($('#religion').val() != 'Catholic')
                    	$('#religionFile').prop('hidden',true);
                        
                    $('#religion').change(function(){
                		if($('#religion').val() == 'Catholic'){
                			$('#religionFile').removeAttr('hidden');
                			$('.religionFileUpload').attr('data-is-required',1);
                		}else{
                			if(!$('#religionFile').prop('hidden')){
                    			$('#religionFile').prop('hidden',true);
                			}
                    		$('.religionFileUpload').attr('data-is-required',0);
                    		$('.religionFileUpload').val('');
                		}
                    });
                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });
        }

        if (current == 'step_update_input_form' && page == 'step_confirm') {
            /* get the birthday range of the form level */
            var values = $("#form1").serialize();
            $.ajax({
                url: "ajax_get_bday_range.php",
                type: "post",
                data: values,
                async: false,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!");
                    dOBRange = data.split(",");
                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });
            isValid1 = true;//check_choose_class($("form")[0]);
            isValid2 = check_input_info($("form")[0]);
            isValid3 = check_docs_upload($("form")[0]);

            if (isValid1 && isValid2 && isValid3) {
                /* Clear result div*/
                $("#step_confirm").html('');

                var studentPersonalPhoto = '&StudentPersonalPhoto=' + $("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
                values += studentPersonalPhoto;

                var file_ary = $('input[type=file][name*=OtherFile]');
                var file_count = file_ary.length;

                for (var i = 0; i < file_count; i++) {
                    var file_element = file_ary.get(i);
                    var otherFile = '&' + file_element.name + '=' + file_element.value.replace(/^.*[\\\/]/, '');
                    values += otherFile;
                }

                /* Send the data using post and put the results in a div */
                $.ajax({
                    url: "ajax_get_update_confirm.php?encoded=1",
                    type: "post",
                    data: values,
                    async: false,
                    success: function (data) {
                        //alert("debugging: The classlevel is updated!"+values);
                        if (data == '') {
                            isValid = false;
                            alert("系統繁忙中，請再次按'下一步'。\nSystem busy. Please click 'Next' button again.");
                        }
                        var decoded = $("#step_confirm").html(data).text();
                        $("#step_confirm").html(decoded);

                        readFileURL($("#StudentPersonalPhoto").get(0));
                        for (var i = 0; i < file_count; i++) {
                            var file_element = file_ary.get(i);
                            readFileURL(file_element);
                        }
                    },
                    error: function () {
                        //alert("failure");
                        $("#result").html('There is error while submit');
                        isValid = false;
                        alert("系統繁忙中，請再次按'下一步'。\nSystem busy. Please click 'Next' button again.");
                    }
                });
            } else {
                return false;
            }
        }

        if (isValid) {
            document.getElementById(current).style.display = "none";
            document.getElementById(page).style.display = "";
        }

        if (current == 'step_index' && page == 'step_instruction') {
            /* Clear result div*/
            $("#DayTypeOption").html('');

            /* Get some values from elements on the page: */
            var values = $("#form1").serialize();

            /* Send the data using post and put the results in a div */
            $.ajax({
                url: "ajax_get_instruction.php",
                type: "post",
                data: values,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!");
                    $("#step_instruction").html(data);
                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });
        }

        if (current == 'step_instruction' && page == 'step_input_form') {
            <?if ($_SESSION["platform"] != "KIS" || !$_SESSION["UserID"]){?>
//			   clearTimeout(timer);
//			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
            <?}?>

            /* Clear result div*/
            $("#DayTypeOption").html('');

            /* Show/Hide .isFirstYear */
            if ($('[name="sus_status"]:checked').data('is-first-year') == '1') {
                $('.isFirstYear').show();
                $('.isNotFirstYear').hide();
                $('.isNotFirstYear').find('input, select, textarea').prop("disabled", true);
            } else {
                $('.isFirstYear').hide();
                $('.isNotFirstYear').show();
                $('.isNotFirstYear').find('input, select, textarea').prop("disabled", false);
            }

            /* Get some values from elements on the page: */
            var values = $("#form1").serialize();

            /* get the birthday range of the form level */
            $.ajax({
                url: "ajax_get_bday_range.php",
                type: "post",
                data: values,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!");
                    dOBRange = data.split(",");
                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });
            window.scrollTo(0, 0);
        }

        if (current != 'step_update_input_form' && page == 'step_confirm' && isValid) {

            /* Clear result div*/
            $("#step_confirm").html('');

            /* Get some values from elements on the page: */
            var values = $("#form1").serialize();
            var studentPersonalPhoto = '&StudentPersonalPhoto=' + $("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
            values += studentPersonalPhoto;

            //		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
            //		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
            //		values+=otherFile;
            //		values+=otherFile1;

            var file_ary = $('input[type=file][name*=OtherFile]');
            var file_count = file_ary.length;

            for (var i = 0; i < file_count; i++) {
                var file_element = file_ary.get(i);
                var otherFile = '&' + file_element.name + '=' + file_element.value.replace(/^.*[\\\/]/, '');
                values += otherFile;
            }

            /*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();

            /* Send the data using post and put the results in a div */
            $.ajax({
                url: "ajax_get_confirm.php?encoded=1",
                type: "post",
                data: values,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!"+values);
                    //$("#step_confirm").html(data);
                    var decoded = $("#step_confirm").html(data).text();
                    $("#step_confirm").html(decoded);
                    readFileURL($("#StudentPersonalPhoto").get(0));
                    for (var i = 0; i < file_count; i++) {
                        var file_element = file_ary.get(i);
                        readFileURL(file_element);
                    }

                    /* Show/Hide .isFirstYear */
                    if ($('[name="sus_status"]:checked').data('is-first-year') == '1') {
                        $('.isFirstYear').show();
                        $('.isNotFirstYear').hide();
                        $('.isNotFirstYear').find('input, select, textarea').prop("disabled", true);
                    } else {
                        $('.isFirstYear').hide();
                        $('.isNotFirstYear').show();
                        $('.isNotFirstYear').find('input, select, textarea').prop("disabled", false);
                    }

                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });

        }
    }

    function submitForm() {
        document.getElementById('form1').target = '';
        if (!isUpdatePeriod)
            document.getElementById('form1').action = 'confirm_update.php';
        window.onbeforeunload = '';
        if (isUpdatePeriod)
            return confirm('<?=$Lang['Admission']['msg']['suresubmit']?>\nAre you sure you want to submit?');
        return confirm('敬請 閣下再次確認所填寫之資料準確無誤。謝謝合作！\nPlease check the information given carefully again.\nThank you for your attention.');
    }

    function check_choose_class(form1) {
        if ($('input:radio[name=sus_status]:checked').val() == null) {
            alert("<?=$Lang['Admission']['msg']['selectclass']?>\nPlease select Class.");
            if (form1.sus_status[0])
                form1.sus_status[0].focus();
            else
                form1.sus_status.focus();
            return false;
        } else {
            return true;
        }
    }

    function check_input_info(form1) {
        //For debugging only
        //return true;

        var isTeacherInput = <?= ($lac->isInternalUse($_GET['token'])) ? 1 : 0 ?>;

        // borower version checking if browser is IE9 or below or not
        var isOldBrowser = 0;
        if (navigator.appName.indexOf("Internet Explorer") != -1 && navigator.appVersion.indexOf("MSIE 1") == -1) {
            isOldBrowser = 1;
        }

        //for email validation
        var re = /\S+@\S+\.\S+/;

        <?if($admission_cfg['maxUploadSize'] > 0){?>
        var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
        <?}else{?>
        var maxFileSize = 1 * 1024 * 1024;
        <?}?>

        /******** Student Info START ********/
        /**** Name START ****/
        if ($.trim(form1.studentsname_b5.value) == '') {
            alert("<?=$Lang['Admission']['msg']['enterchinesename']?>\nPlease enter Name in Chinese.");
            form1.studentsname_b5.focus();
            return false;
        }
        /*if (
            !checkNaNull(form1.studentsname_b5.value) &&
            !checkIsChineseCharacter(form1.studentsname_b5.value)
        ) {
            alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>\nPlease enter Chinese character.");
            form1.studentsname_b5.focus();
            return false;
        }*/

        if ($.trim(form1.studentsname_en.value) == '') {
            alert("<?=$Lang['Admission']['msg']['enterenglishname']?>\nPlease enter Name in English.");
            form1.studentsname_en.focus();
            return false;
        }
        if (
            !checkNaNull(form1.studentsname_en.value) &&
            !checkIsEnglishCharacter(form1.studentsname_en.value)
        ) {
            alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
            form1.studentsname_en.focus();
            return false;
        }
        /**** Name END ****/

        /**** DOB START ****/
        if (!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)) {
            if (form1.StudentDateOfBirth.value != '') {
                alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
            } else {
                alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");
            }

            form1.StudentDateOfBirth.focus();
            return false;
        }
        if (dOBRange[0] != '' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] != '' && form1.StudentDateOfBirth.value > dOBRange[1]) {
            alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>\nInvalid Birthday Range of Student");
            form1.StudentDateOfBirth.focus();
            return false;
        }
        /**** DOB END ****/

        /**** Gender START ****/
        if (!isTeacherInput && $.trim($('input:radio[name=StudentGender]:checked').val()) == '') {
            alert("<?=$Lang['Admission']['msg']['selectgender']?>\nPlease select Gender.");
            form1.StudentGender[0].focus();
            return false;
        }
        /**** Gender END ****/

        /**** Place of Birth START ****/
        if (!isTeacherInput && $.trim(form1.StudentPlaceOfBirth.value) == '') {
            alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>\nPlease enter Place of Birth.");
            form1.StudentPlaceOfBirth.focus();
            return false;
        }
        if ($.trim(form1.StudentPlaceOfBirth.value) == '0' && $.trim(form1.StudentPlaceOfBirthOther.value) == '') {
            alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>\nPlease enter Place of Birth.");
            form1.StudentPlaceOfBirthOther.focus();
            return false;
        }
        /**** Place of Birth END ****/
        
        /**** Religion START ****/
        if ($.trim(form1.religion.value) == '') {
            alert("<?=$Lang['Admission']['SSGC']['msg']['enterReligion']?>\nPlease enter Religious.");
            form1.religion.focus();
            return false;
        }
        /**** Religion END ****/

        /**** Personal Identification START ****/
        if (!isTeacherInput && $.trim(form1.BirthCertType.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertType']?>\nPlease select Personal Identification type.");
            form1.BirthCertType.focus();
            return false;
        }

        if (!isTeacherInput && $.trim(form1.BirthCertType.value) == '1' && $.trim(form1.StudentBirthCertNo.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterBirthCert']?>\nPlease Enter HK Birth Certificate No.");
            form1.StudentBirthCertNo.focus();
            return false;
        }

        if (!isTeacherInput && $.trim(form1.BirthCertType.value) == '2' && $.trim(form1.StudentBirthCertNo.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterHKID']?>\nPlease Enter HKID Card No.");
            form1.StudentBirthCertNo.focus();
            return false;
        }

        if (
            (
                !isTeacherInput &&
                ($.trim(form1.BirthCertType.value) == '1' || $.trim(form1.BirthCertType.value) == '2') &&
                !/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)
            ) || (
                ($.trim(form1.BirthCertType.value) == '1' || $.trim(form1.BirthCertType.value) == '2') &&
                !check_hkid(form1.StudentBirthCertNo.value)
            )
        ) {
            if ($.trim(form1.BirthCertType.value) == '1')
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidBirthCert']?>\nInvalid HK Birth Certificate No.");
            else
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidHKID']?>\nInvalid HKID Card No.");
            form1.StudentBirthCertNo.focus();
            return false;
        }

        if (!isTeacherInput && !isUpdatePeriod && checkBirthCertNo() > 0) {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['duplicateHKID']?>\nThe HKID Card No. is used for admission! Please enter another HKID Card No.");
            form1.StudentBirthCertNo.focus();
            return false;
        }

        if (!isTeacherInput && $.trim(form1.BirthCertType.value) == '3' && $.trim(form1.BirthCertTypeOther.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterIdentification']?>\nPlease Enter Identification.");
            form1.BirthCertTypeOther.focus();
            return false;
        }
        if (!isTeacherInput && $.trim(form1.BirthCertType.value) == '3' && $.trim(form1.StudentBirthCertNo.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterIdentificationNo']?>\nPlease Enter Identification No.");
            form1.StudentBirthCertNo.focus();
            return false;
        }

        /**** Personal Identification END ****/

        /**** Spoken Language For Interview START ****/
//        if (!isTeacherInput && $.trim($('input:radio[name=SpokenLanguageForInterview]:checked').val()) == '') {
//            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSpokenLanguageForInterview']?>\nPlease Enter Spoken language for interview.");
//            form1.SpokenLanguageForInterview[0].focus();
//            return false;
//        }
        /**** Spoken Language For Interview END ****/

        /**** Address START ****/
        if (!isTeacherInput) {
            /*if(form1.HomeAddrRoom.value==''){
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");
    		form1.HomeAddrRoom.focus();
    		return false;
    	} 
    	if(form1.HomeAddrFloor.value==''){
    		alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");	
    		form1.HomeAddrFloor.focus();
    		return false;
    	} 
    	if(form1.HomeAddrBlock.value==''){
    		alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");	
    		form1.HomeAddrBlock.focus();
    		return false;
    	} 
    	if(form1.HomeAddrBlding.value==''){
    		alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");	
    		form1.HomeAddrBlding.focus();
    		return false;
    	} 
    	if(form1.HomeAddrEstate.value==''){
    		alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");	
    		form1.HomeAddrEstate.focus();
    		return false;
    	} 
            if (form1.HomeAddrStreet.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");
                form1.HomeAddrStreet.focus();
                return false;
            }*/
            
            /*
            if (form1.HomeAddrDistrict.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['selectDistrict']?>\nPlease select District.");
                form1.HomeAddrDistrict.focus();
                return false;
            }
            if (form1.HomeAddrArea.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['selectArea']?>\nPlease select Area.");
                form1.HomeAddrArea.focus();
                return false;
            }
            */
            if (form1.HomeAddress.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enteraddress']?>\nPlease enter Home Address.");
                form1.HomeAddress.focus();
                return false;
            }
        }
        /*
        if (form1.HomeAddrArea.value == '<?= (count($admission_cfg['AddressArea']) - 1)?>' && $.trim(form1.HomeAddrAreaOther.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterArea']?>\nPlease enter Area.");
            form1.HomeAddrAreaOther.focus();
            return false;
        }
        */
        
        /**** Address END ****/

        /**** Tel START ****/
        /*if(!isTeacherInput && form1.TelHome.value==''){
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterTel']?>\nPlease enter Tel (Home).");
		form1.TelHome.focus();
		return false;
	}*/
        if (form1.TelHome.value!='' && (!/^[0-9]*$/.test(form1.TelHome.value) || form1.TelHome.value.length < 8)) {
            alert("<?=$Lang['Admission']['SFAEPS']['msg']['invalidContactNoFormat']?>\nContact Number format should be in 8 digits or above.");
            form1.TelHome.focus();
            return false;
        }
        /**** Tel END ****/

        /**** Email START ****/
        if ($.trim(form1.ContactEmail.value) == '') {
            alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>\nPlease enter Parent's Contact E-mail.");
            form1.ContactEmail.focus();
            return false;
        }
        if ($.trim(form1.ContactEmail.value) != '' && !re.test(form1.ContactEmail.value)) {
            alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>\nInvalid E-mail Format");
            form1.ContactEmail.focus();
            return false;
        }
        if(typeof(form1.ContactEmailConfirm) != 'undefined' && $.trim(form1.ContactEmail.value)!=$.trim(form1.ContactEmailConfirm.value)){
	        alert("<?=$Lang['Admission']['HKUGAPS']['msg']['contactEmailMismatch']?>\nParent's Contact E-mail mismatch.");
			form1.ContactEmailConfirm.focus();
			return false;
		}
        /*if ($.trim(form1.ContactEmail2.value) == '') {
            alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>\nPlease enter Parent's Contact E-mail.");
            form1.ContactEmail2.focus();
            return false;
        }*/
        /*
        if ($.trim(form1.ContactEmail2.value) != '' && !re.test(form1.ContactEmail2.value)) {
            alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>\nInvalid E-mail Format");
            form1.ContactEmail2.focus();
            return false;
        }
        */
        /**** Email END ****/

        /****Brother / Sister Start ****/
        /*if($.trim(form1.NoBrotherSister.value)==''){
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterNoOfSiblings']?>\nPlease Enter No. of Brother(s)/Sister(s).");
		form1.NoBrotherSister.focus();
		return false;
	}
	if(form1.NoBrotherSister.value > 0 && $.trim(form1.BrotherSisterRank.value)==''){
		alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterRank']?>\nPlease Enter Rank.");
		form1.BrotherSisterRank.focus();
		return false;
	}*/

        /****Brother / Sister End ****/

        /**** Contact Person START ****/
        if (!isTeacherInput && $('[name="ContactPerson\[\]"]:checked').length == 0) {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['SelectContactPerson']?>\nPlease select Contact Person.");
            form1.ContactPerson1.focus();
            return false;
        }
        /**** Contact Person END ****/

        /**** School Attending START ****/
        if (!isTeacherInput && form1.CurrentAttend1.value == '' && form1.CurrentAttend2.value == '') {
            alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLastSchool']?>\nPlease Enter School Attending.");
            form1.CurrentAttend1.focus();
            return false;
        }
        if (form1.CurrentAttend1.value != '') {
            if (!isTeacherInput && form1.Grades1Start.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterGrade']?>\nPlease enter Grades.");
                form1.Grades1Start.focus();
                return false;
            }
            if (!isTeacherInput && form1.Grades1End.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterGrade']?>\nPlease enter Grades.");
                form1.Grades1End.focus();
                return false;
            }
            if (!isTeacherInput && form1.Year1Start.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterYear']?>\nPlease enter Year.");
                form1.Year1Start.focus();
                return false;
            }
            if (form1.Year1Start.value != '' && !/^[0-9]{4}$/.test(form1.Year1Start.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.Year1Start.focus();
                return false;
            }
            if (!isTeacherInput && form1.Year1End.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterYear']?>\nPlease enter Year.");
                form1.Year1End.focus();
                return false;
            }
            if (form1.Year1End.value != '' && !/^[0-9]{4}$/.test(form1.Year1End.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.Year1End.focus();
                return false;
            }
        }

        if (form1.CurrentAttend2.value != '') {
            if (!isTeacherInput && form1.Grades2Start.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterGrade']?>\nPlease enter Grades.");
                form1.Grades2Start.focus();
                return false;
            }
            if (!isTeacherInput && form1.Grades2End.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterGrade']?>\nPlease enter Grades.");
                form1.Grades2End.focus();
                return false;
            }
            if (!isTeacherInput && form1.Year2Start.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterYear']?>\nPlease enter Year.");
                form1.Year2Start.focus();
                return false;
            }
            if (form1.Year2Start.value != '' && !/^[0-9]{4}$/.test(form1.Year2Start.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.Year2Start.focus();
                return false;
            }
            if (!isTeacherInput && form1.Year2End.value == '') {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterYear']?>\nPlease enter Year.");
                form1.Year2End.focus();
                return false;
            }
            if (form1.Year2End.value != '' && !/^[0-9]{4}$/.test(form1.Year2End.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.Year2End.focus();
                return false;
            }
        }
        /**** School Attending END ****/

        /**** Twins Chekcing START ****/
        if ($('#twinsY').attr('checked') && $.trim(form1.twinsapplicationid.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterTwinsID']?>\nPlease Enter Twins\' Number of Identity Document");
            form1.twinsapplicationid.focus();
            return false;
        }

        if ($('#twinsY').attr('checked') && /\(+|\)+/.test($.trim(form1.twinsapplicationid.value))) {
            alert("雙生兒身份證明文件號碼格式不符。\nInvalid Twins\' Number of Identity Document.");
            form1.twinsapplicationid.focus();
            return false;
        }
        /**** Twins Chekcing END ****/
        /******** Student Info END ********/


        /******** Parent Info START ********/
        <?php for($i = 1;$i <= 4;$i++):?>
        /**** Name START ****/
        if ($.trim(form1.G<?=$i?>ChineseName.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\nPlease Enter Name of Parent.");
            form1.G<?=$i?>ChineseName.focus();
            return false;
        }

        if ($.trim(form1.G<?=$i?>EnglishName.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\nPlease Enter Name of Parent.");
            form1.G<?=$i?>EnglishName.focus();
            return false;
        }
        if (
            !checkNaNull(form1.G<?=$i?>EnglishName.value) &&
            !checkIsEnglishCharacter(form1.G<?=$i?>EnglishName.value)
        ) {
            alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
            form1.G<?=$i?>EnglishName.focus();
            return false;
        }
        /**** Name END ****/

        /**** Contact Number START ****/
        if (!isTeacherInput && $.trim(form1.G<?=$i?>MobileNo.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>\nPlease Enter Contact Number.");
            form1.G<?=$i?>MobileNo.focus();
            return false;
        }
        if (
            !checkNaNull(form1.G<?=$i?>MobileNo.value) &&
            (!/^[0-9]*$/.test(form1.G<?=$i?>MobileNo.value) || form1.G<?=$i?>MobileNo.value.length < 8)
        ) {
            alert("<?=$Lang['Admission']['SFAEPS']['msg']['invalidContactNoFormat']?>\nContact Number format should be in 8 digits or above.");
            form1.G<?=$i?>MobileNo.focus();
            return false;
        }
        /**** Contact Number END ****/

        /**** Email START ****/
        /*if (!isTeacherInput && $.trim(form1.G<?=$i?>Email.value) == '') {
            alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>\nPlease enter Email Address.");
            form1.G<?=$i?>Email.focus();
            return false;
        }
        if (
            (form1.G<?=$i?>Email.value != '') &&
            !checkNaNull(form1.G<?=$i?>Email.value) &&
            (!re.test(form1.G<?=$i?>Email.value))
        ) {
            alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>\nInvalid Email Format");
            form1.G<?=$i?>Email.focus();
            return false;
        }*/
        /**** Email END ****/

        /**** Occupation START ****/
        if (!isTeacherInput && $.trim(form1.G<?=$i?>Occupation.value) == '') {
            alert("<?=$Lang['Admission']['msg']['enteroccupation']?>\nPlease Enter Occupation.");
            form1.G<?=$i?>Occupation.focus();
            return false;
        }
        /**** Occupation END ****/

        /**** Institution START ****/
        /*if (!isTeacherInput && $.trim(form1.G<?=$i?>Institution.value) == '') {
            alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterInstitution']?>\nPlease Enter Institution.");
            form1.G<?=$i?>Institution.focus();
            return false;
        }*/
        /**** Institution END ****/
        <?php endfor; ?>
        /******** Parent Info END ********/

        /******** Other Info START ********/
        <?php for($i = 1;$i <= 3;$i++):?>
        /**** Sibling Studying START ****/
        if (form1.OthersRelativeStudiedNameChi<?=$i?>.value != '') {
            /*if (
                !checkNaNull(form1.OthersRelativeStudiedNameChi<?=$i?>.value) &&
                !checkIsChineseCharacter(form1.OthersRelativeStudiedNameChi<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>\nPlease enter Chinese character.");
                form1.OthersRelativeStudiedNameChi<?=$i?>.focus();
                return false;
            }*/
            if (
                !checkNaNull(form1.OthersRelativeStudiedNameEng<?=$i?>.value) &&
                !checkIsEnglishCharacter(form1.OthersRelativeStudiedNameEng<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
                form1.OthersRelativeStudiedNameEng<?=$i?>.focus();
                return false;
            }

            if (!isTeacherInput) {
                if ($.trim(form1.OthersRelativeStudiedNameEng<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeStudiedNameEng<?=$i?>.focus();
                    return false;
                }
                if ($.trim(form1.OthersRelativeStudiedStudentId<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeStudiedStudentId<?=$i?>.focus();
                    return false;
                }
            }
        }
        /**** Sibling Studying END ****/

        /**** Sibling Graduated START ****/
        /**** Relationship START ****/
        if (form1.OthersRelativeGraduationRelationship<?=$i?>.value == '' && $.trim(form1.OthersRelativeGraduationNameChi<?=$i?>.value) != '') {
            alert("<?=$Lang['Admission']['SFAEPS']['msg']['selectRelationship']?>\n<?=$LangEn['Admission']['SFAEPS']['msg']['selectRelationship']?>");
            form1.OthersRelativeGraduationRelationship<?=$i?>.focus();
            return false;
        }
        /**** Relationship END ****/

        if (form1.OthersRelativeGraduationRelationship<?=$i?>.value != '') {
            /*if (
                !checkNaNull(form1.OthersRelativeGraduationNameChi<?=$i?>.value) &&
                !checkIsChineseCharacter(form1.OthersRelativeGraduationNameChi<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>\nPlease enter Chinese character.");
                form1.OthersRelativeGraduationNameChi<?=$i?>.focus();
                return false;
            }*/
            if (
                !checkNaNull(form1.OthersRelativeGraduationNameEng<?=$i?>.value) &&
                !checkIsEnglishCharacter(form1.OthersRelativeGraduationNameEng<?=$i?>.value)
            ) {
                alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
                form1.OthersRelativeGraduationNameEng<?=$i?>.focus();
                return false;
            }

            if (!isTeacherInput) {
                if ($.trim(form1.OthersRelativeGraduationNameEng<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeGraduationNameEng<?=$i?>.focus();
                    return false;
                }
                if ($.trim(form1.OthersRelativeGraduationYear<?=$i?>.value) == '') {
                    alert("<?=$Lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>\nPlease Enter Sibling Information.");
                    form1.OthersRelativeGraduationYear<?=$i?>.focus();
                    return false;
                }
            }
            if (form1.OthersRelativeGraduationYear<?=$i?>.value != '' && !/^[0-9]{4}$/.test(form1.OthersRelativeGraduationYear<?=$i?>.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.OthersRelativeGraduationYear<?=$i?>.focus();
                return false;
            }
        }
        /**** Sibling Graduated END ****/
        <?php endfor; ?>
        /******** Other Info END ********/

        /******** Awards START ********/
        <?php for($i = 1;$i <= 3;$i++):?>
        if (form1.awardItem<?=$i?>.value != '') {
            if (form1.awardYear<?=$i?>.value != '' && !/^[0-9]{4}$/.test(form1.awardYear<?=$i?>.value)) {
                alert("<?=$Lang['Admission']['HKUGAPS']['msg']['invalidYearFormat']?>\nInvalid Year format.");
                form1.awardYear<?=$i?>.focus();
                return false;
            }
        }
        <?php endfor; ?>
        /******** Awards END ********/

        return true;
    }

    function check_docs_upload(form1) {

        var isTeacherInput = <?= ($lac->isInternalUse($_GET['token'])) ? 1 : 0 ?>;

        var isOldBrowser = 0;
        if (navigator.appName.indexOf("Internet Explorer") != -1 && navigator.appVersion.indexOf("MSIE 1") == -1) {
            isOldBrowser = 1;
        }

        <?if($admission_cfg['maxUploadSize'] > 0){?>
        var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
        <?}else{?>
        var maxFileSize = 1 * 1024 * 1024;
        <?}?>

        var file_ary = $('input[type=file][name*=OtherFile]');
        var file_count = file_ary.length;

        /******** Personal Photo START ********/
        if (!isUpdatePeriod && !isTeacherInput && form1.StudentPersonalPhoto.value == '') {
            alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>\nPlease upload a personal photo.");
            form1.StudentPersonalPhoto.focus();
            return false;
        }
        /******** Personal Photo END ********/

        /******** File format START ********/
        if (form1.StudentPersonalPhoto.value != '') {
            var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
            if (!isOldBrowser) {
                if (form1.StudentPersonalPhoto.files[0]) {
                    studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
                    var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
                }
            }
            if (studentPhotoExt != 'JPG' && studentPhotoExt != 'JPEG' && studentPhotoExt != 'PNG' && studentPhotoExt != 'GIF') {
                alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
                form1.StudentPersonalPhoto.focus();
                return false;
            }
        }
        /******** File format END ********/

        /******** File size START ********/
        if (!isOldBrowser && studentPhotoSize > maxFileSize) {
            alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");
            form1.StudentPersonalPhoto.focus();
            return false;
        }
        /******** File size END ********/

        for (var i = 0; i < file_count; i++) {
            var file_element = file_ary.get(i);

            var otherFileVal = file_element.value;
            var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
            if (!isOldBrowser) {
                otherFileExt = file_element.files.length > 0 ? file_element.files[0].name.split('.').pop().toUpperCase() : '';
                var otherFileSize = file_element.files.length > 0 ? file_element.files[0].size : 0;
            }

            if (otherFileVal == '') {
                if (!isUpdatePeriod && !isTeacherInput && $(file_element).data('isRequired') == '1') {
                    alert("<?=$Lang['Admission']['msg']['uploadfile']?>\nPlease upload file.");
                    file_element.focus();
                    return false;
                }
            } else {
                if (otherFileExt != 'JPG' && otherFileExt != 'JPEG' && otherFileExt != 'PNG' && otherFileExt != 'GIF' && otherFileExt != 'PDF') {
                    alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
                    file_element.focus();
                    return false;
                } else if (!isOldBrowser && otherFileSize > maxFileSize) {
                    alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");
                    file_element.focus();
                    return false;
                }
            }

        }
        return true;
    }

    function checkBirthCertNo() {
        var values = $("#form1").serialize();
        var res = null;
        /* check the birth cert number is applied or not */
        $.ajax({
            url: "ajax_get_birth_cert_no.php",
            type: "post",
            data: values,
            async: false,
            success: function (data) {
                //alert("debugging: The classlevel is updated!");
                res = data;
            },
            error: function () {
                //alert("failure");
                $("#result").html('There is error while submit');
            }
        });
        return res;
    }

    //function checkInterviewQuotaLeft(){
    //	var values = $("#form1").serialize();
    //	var res = null;
    //	/* Check the quota of interview timeslot */
    //   $.ajax({
    //       url: "ajax_check_num_of_interview_quota.php",
    //       type: "post",
    //       data: values,
    //       async: false,
    //       success: function(data){
    //           //alert("debugging: The classlevel is updated!"+values);
    //           //$("#step_confirm").html(data);
    ////           if(data <= 0){
    ////		           	alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
    ////		           	return false;
    //					res = data;
    ////		           }
    //		       },
    //		       error:function(){
    //		           //alert("failure");
    //           //$("#result").html('There is error while submit');
    //       }
    //   });
    //   return res;
    //}

    function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        if (tableID == 'dataTable1') {
            if (rowCount > 4) {
                document.getElementById('btn_addRow1').style.display = 'none';
            } else if (rowCount > 1) {
                document.getElementById('btn_deleteRow1').style.display = '';
            }

        } else {
            if (rowCount > 4) {
                document.getElementById('btn_addRow2').style.display = 'none';
            } else if (rowCount > 1) {
                document.getElementById('btn_deleteRow2').style.display = '';
            }
        }

        if (rowCount <= 5) {                            // limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var newcell = row.insertCell(0);
            newcell.className = "field_title";
            newcell.style.textAlign = "right";
            newcell.innerHTML = '(' + rowCount + ')';
            if (tableID == 'dataTable1') {
                newcell = row.insertCell(1);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersPrevSchYear' + rowCount + '" type="text" id="OthersPrevSchYear' + rowCount + '" class="textboxtext" />';
                newcell = row.insertCell(2);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersPrevSchClass' + rowCount + '" type="text" id="OthersPrevSchClass' + rowCount + '" class="textboxtext" />';
                newcell = row.insertCell(3);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersPrevSchName' + rowCount + '" type="text" id="OthersPrevSchName' + rowCount + '" class="textboxtext" />';
            } else {
                newcell = row.insertCell(1);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersRelativeStudiedYear' + rowCount + '" type="text" id="OthersRelativeStudiedYear' + rowCount + '" class="textboxtext" />';
                newcell = row.insertCell(2);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersRelativeStudiedName' + rowCount + '" type="text" id="OthersRelativeStudiedName' + rowCount + '" class="textboxtext" />';
                newcell = row.insertCell(3);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersRelativeClassPosition' + rowCount + '" type="text" id="OthersRelativeClassPosition' + rowCount + '" class="textboxtext" />';
                newcell = row.insertCell(4);
                newcell.className = "form_guardian_field";
                newcell.innerHTML = '<input name="OthersRelativeRelationship' + rowCount + '" type="text" id="OthersRelativeRelationship' + rowCount + '" class="textboxtext" />';
            }
        } else {
            //alert("Maximum Passenger per ticket is 5");

        }
    }

    function deleteRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        if (tableID == 'dataTable1') {
            if (rowCount < 4) {
                document.getElementById('btn_deleteRow1').style.display = 'none';
            } else if (rowCount < 7) {
                document.getElementById('btn_addRow1').style.display = '';
            }
        } else {
            if (rowCount < 4) {
                document.getElementById('btn_deleteRow2').style.display = 'none';
            } else if (rowCount < 7) {
                document.getElementById('btn_addRow2').style.display = '';
            }
        }

        if (rowCount <= 2) {               // limit the user from removing all the fields
            //alert("Cannot Remove all the Passenger.");
        } else {
            table.deleteRow(rowCount - 1);
        }
    }

    function readFileURL(input) {
        if (navigator.appVersion.indexOf("MSIE 1") == -1 && !(navigator.appName == 'Netscape')) {
            $('#href' + input.name).hide();
            $('#div' + input.name).hide();
            return;
        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var isOldBrowser = 0;
                if (navigator.appName.indexOf("Internet Explorer") != -1 || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null))) {
                    isOldBrowser = 1;
                }

                if (isOldBrowser) {
                    $('#href' + input.name).hide();
                } else {
                    $('#href' + input.name).attr('href', e.target.result);
                }

                if (input.value.split('.').pop().toUpperCase() == "PDF") {
                    $('#img' + input.name).hide();
                    $('#iframe' + input.name).attr('src', e.target.result);
                } else {
                    $('#iframe' + input.name).hide();
                    $('#img' + input.name).attr('src', e.target.result);
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function showOtherTypeTextField(obj, showValue, showTarget) {
        if (obj.value == showValue) {
            $('#' + showTarget).show();
        } else {
            $('#' + showTarget).hide();
        }
    }

    function updateBrotherSisterRankDisabled(obj) {
        if (obj.value > '0') {
            $('#BrotherSisterRank').removeAttr('disabled');
        } else {
            $('#BrotherSisterRank').attr('disabled', 'disabled');
        }
    }

</script>