<?php
/**
 * Change Log:
 * 2019-05-07 Pun
 *  - Added getBilingualArr()
 * 2018-01-24 Pun
 *  - File Created
 */
namespace AdmissionSystem;

include_once ( "{$intranet_root}/includes/admission/libadmission_ui_cust_base.php" );
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");


abstract class AdmissionUiCustBase extends \admission_ui_cust_base
{
    use ActionFilterQueueTrait;

    const FILTER_ADMISSION_FORM_INSTRUCTION_HTML = 'AdmissionForm_InstructionHtml';

    const FILTER_ADMISSION_FORM_LANGUAGE = 'AdmissionForm_Language';

    const FILTER_ADMISSION_FORM_WIZARD_STEPS = 'AdmissionForm_WizardSteps';

    const FILTER_ADMISSION_FORM_WIZARD_STEPS_CURRENT = 'AdmissionForm_WizardStepsCurrent';

    const FILTER_ADMISSION_FORM_STEP_PAGE_HTML = 'AdmissionForm_FormStepPageHtml';

    const FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML = 'AdmissionForm_ApplicantFormHtml';

    const FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML = 'AdmissionForm_ApplicantFormConfirmHtml';

    const FILTER_ADMISSION_FORM_EMAIL_APPLICANT = 'AdmissionForm_CompleteEmailHtml';

    const FILTER_ADMISSION_FORM_FINISH_HTML = 'AdmissionForm_FinishHtml';

    const FILTER_ADMISSION_UPDATE_FORM_WIZARD_STEPS = 'AdmissionUpdateForm_WizardSteps';

    const FILTER_ADMISSION_UPDATE_FORM_WIZARD_STEPS_CURRENT = 'AdmissionUpdateForm_WizardStepsCurrent';

    const FILTER_ADMISSION_UPDATE_FORM_STEP_PAGE_HTML = 'AdmissionUpdateForm_FormStepPageHtml';

    const FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML = 'AdmissionUpdateForm_ApplicantFormHtml';

    const FILTER_ADMISSION_UPDATE_FORM_EMAIL_APPLICANT = 'AdmissionUpdateForm_CompleteEmailHtml';

    const FILTER_ADMISSION_TOP_MENU_TAB = 'Portal_AdmissionTopMenuTab';

    const FILTER_ADMISSION_PDF_BASE_PATH = 'Portal_AdmissionPdfBaseFilePath';

    const FILTER_ADMISSION_PORTAL_EDIT_VALUE = 'Portal_EditValue';

    const FILTER_ADMISSION_PORTAL_DISPLAY_VALUE = 'Portal_DisplayValue';

    const ACTION_ADMISSION_FORM_APPLICANT_FORM_HTML = 'AdmissionForm_ApplicantFormHtml';

    /**
     * Admission form - get language
     */
    public function getAdmissionLang($ApplicationID = '', $forceReload = false)
    {
        global $admission_cfg;

        if ($ApplicationID && ($forceReload || ! $_SESSION['admission_form_language'])) {
            $objDb = new \libdb();
            $sql = "SELECT Lang FROM ADMISSION_OTHERS_INFO WHERE ApplicationID='{$ApplicationID}'";
            $rs = $objDb->returnVector($sql);
            $_SESSION['admission_form_language'] = $admission_cfg['Lang'][$rs[0]];
        }

        if (! $_SESSION['admission_form_language']) {
            $_SESSION['admission_form_language'] = $admission_cfg['DefaultLang'];
        }

        $lang = $this->applyFilter(self::FILTER_ADMISSION_FORM_LANGUAGE, $_SESSION['admission_form_language']);
        $_SESSION['admission_form_language'] = $lang;

        return $lang;
    }

    public function getBilingualArr(){
        global $intranet_root;

        $langArr = array();
        $langNameArr = array('b5', 'en', 'gb');

        foreach($langNameArr as $langName){
            @include("{$intranet_root}/lang/lang.{$langName}.php");
            @include("{$intranet_root}/lang/kis/lang_common_{$langName}.php");
            @include("{$intranet_root}/lang/kis/apps/lang_admission_{$langName}.php");
            @include("{$intranet_root}/lang/admission_lang.{$langName}.php");
            $langArr['Lang'][$langName] = $Lang;
            $langArr['kis_lang'][$langName] = $kis_lang;
            unset($Lang);
            unset($kis_lang);
        }
        return $langArr;
    }

    /**
     * Admission form - step html
     */
    private function getWizardStepsHTML($stepArr, $currentStep, $ApplicationID = '')
    {
        global $Lang, $sys_custom, $lac, $kis_lang;

        $lastStep = count($stepArr);

        /**
         * ** Preview START ***
         */
        $previewStyle = 'display:none;';
        if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && ! $_GET['token'] && $currentStep != $lastStep) {
            $previewStyle = '';
        }
        /**
         * ** Preview END ***
         */

        /**
         * ** Remark START ***
         */
        $remarkStyle = 'display:none;';
        if ($lac->isInternalUse($_REQUEST['token']) && ($currentStep != $lastStep || $lac->getTokenByApplicationNumber($ApplicationID) == '')) {
            $remarkStyle = '';
        }
        /**
         * ** Remark END ***
         */

        /**
         * ** Step START ***
         */
        $stepHTML = '';
        foreach ($stepArr as $index => $stepInfo) {
            $isFirstStep = ($index == 0);
            $isLastStep = ($index + 1 == $lastStep);
            $i = $index + 1;

            $href = '';
            $class = '';
            if ($isFirstStep) {
                $class .= ' first_step';
            } elseif ($isLastStep) {
                $class .= ' last_step';
            }

            if ($index + 1 < $currentStep) {
                $class .= ' completed-step';
                $href = "javascript:goto('{$stepArr[$currentStep - 1]['id']}', '{$stepInfo['id']}');";
            } elseif ($currentStep == $index + 1) {
                $class .= ($isLastStep) ? ' last_step_completed' : ' active-step';
            } elseif ($currentStep < $index + 1) {
                $href = "javascript:void(0);";
            }

            if ($isLastStep) {
                $stepHTML .= <<<HTML
                <div class="{$class}">
                    <span style="width:90px">
                        {$i}
                        {$stepInfo['title']}
                    </span>
                </div>
HTML;
            } else {
                $stepHTML .= <<<HTML
                <div class="{$class}">
                    <a href="{$href}">
                        <span>{$i}</span>
                        {$stepInfo['title']}
                    </a>
                </div>
HTML;
            }
        }
        /**
         * ** Step END ***
         */

        $html = <<<HTML
            <h2 style="font-size:18px;color:red;{$previewStyle}">
                <center>
                    {$Lang['Admission']['msg']['defaultpreviewpagemessage']}
                </center>
            </h2>

            <h2 style="font-size:18px;color:red;{$remarkStyle}">
                <center>
                    {$kis_lang['remarks']}
                </center>
            </h2>

            <div class="wizard-steps">
                {$stepHTML}
            </div>

            <p class="spacer"></p>
HTML;
        return $html;
    }

    /**
     * Admission form - step ui
     */
    protected function getWizardStepsUI($currentStep, $ApplicationID = '')
    {
        global $Lang, $sys_custom, $lac, $kis_lang_b5, $kis_lang_en;

        /**
         * ** Default step START ***
         */
        $defaultStep = array();
        $defaultStep[] = array(
            'id' => 'step_instruction',
            'title' => $Lang['Admission']['instruction']
        );
        $defaultStep[] = array(
            'id' => 'step_input_form',
            'title' => $Lang['Admission']['personalInfo']
        );
        $defaultStep[] = array(
            'id' => 'step_docs_upload',
            'title' => $Lang['Admission']['docsUpload']
        );
        $defaultStep[] = array(
            'id' => 'step_confirm',
            'title' => $Lang['Admission']['confirmation']
        );
        $defaultStep[] = array(
            'id' => 'step_complete',
            'title' => $Lang['Admission']['finish']
        );
        /**
         * ** Default step END ***
         */

        $stepArr = $this->applyFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS, $defaultStep);
        $currentStep = $this->applyFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS_CURRENT, $currentStep);
        return $this->getWizardStepsHTML($stepArr, $currentStep, $ApplicationID);
    }

    /**
     * Admission form - Whole form HTML
     */
    private function getApplicationFormHtml($pageArr, $instruction)
    {
        /**
         * ** Generate page START ***
         */
        $pageHtml = '';
        foreach ($pageArr as $index => $page) {
            $style = ($index == 0) ? 'display:auto;' : 'display:none;';
            $pageHtml .= "<div id='{$page['id']}' style='{$style}'>";

            $pageHtml .= "<div class='admission_board'>";
            if ($page['func']) {
                $pageHtml .= call_user_func_array(array(
                    $this,
                    $page['func']
                ), $page['args']);
            }
            $pageHtml .= "</div>";

            $pageHtml .= "</div>";
        }
        /**
         * ** Generate page END ***
         */

        return $pageHtml;
    }

    /**
     * Admission form - Whole form page for create applicant
     */
    public function getWholeApplicationForm($instruction, $AcademicYearID = '', $YearID = '', $BirthCertNo = '')
    {

        /**
         * ** Default page START ***
         */
        $defaultPage = array();
        $defaultPage[] = array(
            'id' => 'step_instruction',
            'func' => 'getInstructionContent',
            'args' => array(
                $instruction
            )
        );
        $defaultPage[] = array(
            'id' => 'step_input_form',
            'func' => 'getApplicationForm',
            'args' => array(
                $BirthCertNo,
                $YearID,
            )
        );
        $defaultPage[] = array(
            'id' => 'step_docs_upload',
            'func' => 'getDocUploadPage',
            'args' => array(
                $IsConfirm = 0,
                $IsUpdate = 0,
                $AcademicYearID,
                $YearID
            )
        );
        $defaultPage[] = array(
            'id' => 'step_confirm',
            'func' => '',
            'args' => array()
        );
        /**
         * ** Default page END ***
         */

        $pageArr = $this->applyFilter(self::FILTER_ADMISSION_FORM_STEP_PAGE_HTML, $defaultPage);

        return $this->getApplicationFormHTML($pageArr, $instruction);
    }

    /**
     * Admission form - update step ui
     */
    protected function getWizardUpdateStepsUI($currentStep, $ApplicationID = '')
    {
        global $Lang;

        /**
         * ** Default step START ***
         */
        $defaultStep = array();
        $defaultStep[] = array(
            'id' => 'step_update_input_form',
            'title' => $Lang['Admission']['InformationUpdate']
        );
        $defaultStep[] = array(
            'id' => 'step_confirm',
            'title' => $Lang['Admission']['confirmation']
        );
        $defaultStep[] = array(
            'id' => 'step_complete',
            'title' => $Lang['Admission']['finish']
        );
        /**
         * ** Default step END ***
         */

        $stepArr = $this->applyFilter(self::FILTER_ADMISSION_UPDATE_FORM_WIZARD_STEPS, $defaultStep);
        $currentStep = $this->applyFilter(self::FILTER_ADMISSION_UPDATE_FORM_WIZARD_STEPS_CURRENT, $currentStep);
        return $this->getWizardStepsHTML($stepArr, $currentStep, $ApplicationID);
    }

    /**
     * Admission form - Whole form page for update applicant
     */
    public function getWholeApplicationUpdateForm($instruction, $ApplicationID)
    {

        /**
         * ** Default page START ***
         */
        $defaultPage = array();
        $defaultPage[] = array(
            'id' => 'step_update_input_form',
            'func' => 'getInputFormPageContent',
            'args' => array()
        );
        $defaultPage[] = array(
            'id' => 'step_confirm',
            'func' => '',
            'args' => array()
        );
        /**
         * ** Default page END ***
         */

        $pageArr = $this->applyFilter(self::FILTER_ADMISSION_UPDATE_FORM_STEP_PAGE_HTML, $defaultPage);

        return $this->getApplicationFormHTML($pageArr, $instruction);
    }

    /**
     * Admission form - first page create Instruction to Applicants - choose class radio button
     */
    protected function getChooseClassForm($ClassLevel = '')
    {
        global $LangB5, $LangEn, $lac, $sys_custom, $admission_cfg;
        $class_level = $lac->getClassLevel();
        $application_setting = $lac->getApplicationSetting();

        // ### Get choose class input START ####
        $class_level_selection = "";
        // To get the class level which is available
        if ($application_setting) {
            $hasClassLevelApply = 0;

            foreach ($application_setting as $key => $value) {
                if (date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])) {
                    $hasClassLevelApply = 1;
                    $disabled = '';

                    $numOFQuotaLeft = $lac->NumOfQuotaLeft($key, $value['SchoolYearID']);

                    $checked = ($key == $ClassLevel) ? ' checked' : '';

                    $isFullApply = false;
                    if ($numOFQuotaLeft <= 0) {
                        $isFullApply = true;
                    }

                    $fullStyle = 'display:none;';
                    if ($isFullApply) {
                        $disabled = 'disabled';
                        $fullStyle = '';
                    }

                    $class_level_selection .= <<<HTML
                        <input type="radio" name="sus_status" value="{$key}" id="status_{$key}" {$disabled} {$checked} />
                        <label for="status_{$key}">
                            {$value['ClassLevelName']}
                            <span style="color:red;{$fullStyle}">
                                (
                                    {$LangB5['Admission']['IsFull']}
                                    {$LangEn['Admission']['IsFull']}
                                )
                            </span>
                        </label>
                        &nbsp;
HTML;
                }
            }

            if ($hasClassLevelApply == 0) {
                $class_level_selection = <<<HTML
                    <fieldset class="warning_box">
                        <legend>{$LangB5['Admission']['warning']} {$LangEn['Admission']['warning']}</legend>
                        <ul>
                            <li>
                                {$LangB5['Admission']['msg']['noclasslevelapply']}
                                {$LangEn['Admission']['msg']['noclasslevelapply']}
                            </li>
                        </ul>
                    </fieldset>
HTML;
            }
        } else {
            $class_level_selection = <<<HTML
                <fieldset class="warning_box">
                    <legend>{$LangB5['Admission']['warning']} {$LangEn['Admission']['warning']}</legend>
                    <ul>
                        <li>
                            {$LangB5['Admission']['msg']['noclasslevelapply']}
                            {$LangEn['Admission']['msg']['noclasslevelapply']}
                        </li>
                    </ul>
                </fieldset>
HTML;
        }
        // ### Get choose class input END ####

        // ### Get language input START ####
        $languageSelection = '';
        foreach ((array)$admission_cfg['Lang'] as $index => $lang) {
            $checked = ($lang == $admission_cfg['DefaultLang']) ? ' checked' : '';

            $languageSelection .= <<<HTML
                <input type="radio" name="lang" value="{$index}" id="lang_{$index}" {$checked}  />
                <label for="lang_{$index}">
                    {$LangB5['Admission']['LanguagesDisplay'][$lang]}
                    {$LangEn['Admission']['LanguagesDisplay'][$lang]}
                </label>
                &nbsp;
HTML;
        }
        // ### Get language input END ####

        /**
         * ** Step button START ***
         */
        $currentStepId = 'step_index';
        $nextStepId = 'step_instruction';

        $buttonHTML = '';
        if ($hasClassLevelApply) {
            // $nextButton = $this->GET_ACTION_BTN("{$LangB5['Btn']['Next']} {$LangEn['Btn']['Next']}", "button", "goto('{$currentStepId}','{$nextStepId}')", "goToInstruction", "", 0, "formbutton");
            $buttonHTML = $this->getStepButton($currentStepId, $nextStepId, $lastStepId = '');
        }
        /**
         * ** Step button END ***
         */

        $x .= <<<HTML
            <!-- #### Language selection START #### -->
            <fieldset class="instructionLang">
                <legend>
                    {$LangB5['Admission']['Language']}
                    {$LangEn['Admission']['Language']}
                </legend>
                <div class="admission_select_option">
                    {$languageSelection}
                </div>
            </fieldset>
            <!-- #### Language selection END #### -->

            <!-- #### Year selection START #### -->
            <fieldset style="margin-top: 10px;">
                <legend>
                    {$LangB5['Admission']['level']}
                    {$LangEn['Admission']['level']}
                </legend>
                <div class="admission_select_option">
                    {$class_level_selection}
                </div>
            </fieldset>
            <!-- #### Year selection END #### -->

            <!-- #### CUSTOM FIELD #### -->

            {$buttonHTML}

            <br/>
            <div class="instructionSuggestBrowser">
                <span class="instructionSuggestBrowser_Chi">建議使用 <a href="https://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>
                <br/>
                <span class="instructionSuggestBrowser_Eng">Recommended to use <a href="https://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>
            </div>
HTML;

        return $x;
    }

    /**
     * Admission form - first page update Instruction to Applicants - choose class radio button
     */
    protected function getApplicantCheckingForm()
    {
        global $LangB5, $LangEn, $lac, $sys_custom;

        /**
         * ** Step button START ***
         */
        $currentStepId = 'step_index';
        $nextStepId = 'step_update_input_form';

        $buttonHTML = $this->getStepButton($currentStepId, $nextStepId, $lastStepId = '');
        /**
         * ** Step button END ***
         */

        $x = <<<HTML
            <table class="form_table" style="font-size: 13px">
                <tr>
                    <td class="field_title">
                        <font style="color:red;">*</font>輸入申請編號 Application Number
                    </td>
                    <td>
                        <input style="width:200px" name="InputApplicationID" type="text" id="InputApplicationID" class="textboxtext" maxlength="16" size="8"/>
                    </td>
                </tr>
                <tr>
                    <td class="field_title">
                        <font style="color:red;">*</font>輸入出生證明書號碼 Birth Certificate Number
                    </td>
                    <td>
                        <input style="width:200px" name="InputStudentBirthCertNo" type="text" id="InputStudentBirthCertNo" class="textboxtext" maxlength="64" size="8"/>
                        <br />
                                                                        不需要輸入括號，例如：A123456(7)，請輸入 "A1234567"。
                        <br/>
                        No need to enter the brackets. Eg. A123456(7), please enter "A1234567".
                    </td>
                </tr>
                <tr>
                    <td class="field_title">
                        <font style="color:red;">*</font>輸入出生日期 Date of Birth
                    </td>
                    <td>
                        <input style="width:200px" name="InputStudentDateOfBirth" type="text" id="InputStudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>
                        (YYYY-MM-DD)
                    </td>
                </tr>
            </table>

            {$buttonHTML}

            <p class="spacer"></p><br/>

            <div class="suggestBrowser"><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>
            <br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span></div>
HTML;
        return $x;
    }

    /**
     * Admission form - each page next/back/cancel button
     */
    protected function getStepButton($currentStepId, $nextStepId, $lastStepId = '', $nextBtnLang = '', $isUpdate = false)
    {
        global $lac, $sys_custom;
        global $Lang;

        $backButton = '';
        if ($lastStepId) {
            $backButton = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('{$currentStepId}','{$lastStepId}')", "back_{$lastStepId}", "", 0, "formbutton");
        }

        $nextBtnLang = ($nextBtnLang) ? $nextBtnLang : $Lang['Btn']['Next'];

        if ($nextStepId) {
            $nextButton = $this->GET_ACTION_BTN($nextBtnLang, "button", "goto('{$currentStepId}','{$nextStepId}')", "go_{$nextStepId}", "", 0, "formbutton");
        } else {
            $nextButton = $this->GET_ACTION_BTN($nextBtnLang, "submit", "", "go_submit", "", 0, "formbutton");
        }

        if ($lastStepId) {
            if ($isUpdate) {
                $cancelButton = '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index_edit.php\');return document.MM_returnValue" value="' . $Lang['Btn']['Cancel'] . '" />';
            } else {
                if (! $lac->isInternalUse($_REQUEST['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
                    $cancelButton = '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token=' . $_REQUEST['token'] . '\');return document.MM_returnValue" value="' . $Lang['Btn']['Cancel'] . '" />';
                } else {
                    $cancelButton = '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="' . $Lang['Btn']['Cancel'] . '" />';
                }
            }
        }

        $buttonHTML = <<<HTML
            <div class="edit_bottom">
                {$backButton}
                {$nextButton}
                {$cancelButton}
            </div>
            <p class="spacer"></p>
HTML;
        return $buttonHTML;
    }

    /**
     * Admission form - First page Instruction to preview/create/view/update Applicants
     */
    public function getIndexContent($Instruction, $ClassLevel = "", $IsUpdate = false)
    {
        global $LangB5, $LangEn, $kis_lang_b5, $kis_lang_en, $lac, $sys_custom;

        /**
         * ** Instruction START ***
         */
        if (! $Instruction) {
            $Instruction = "{$LangB5['Admission']['msg']['defaultfirstpagemessage']}<br/>{$LangEn['Admission']['msg']['defaultfirstpagemessage']}";
        }
        /**
         * ** Instruction END ***
         */

        /**
         * ** Preview START ***
         */
        $previewStyle = 'display:none;';
        if (! $IsUpdate && ($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && ! $_GET['token']) {
            $previewStyle = '';
        }
        /**
         * ** Preview END ***
         */

        /**
         * ** View/Edit START ***
         */
        if ($IsUpdate) {
            $viewStyle = 'display:none;';
            $editStyle = '';
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && $lac->IsAfterUpdatePeriod()) {
                $viewStyle = '';
                $editStyle = 'display:none;';
            }
        } else {
            $viewStyle = $editStyle = 'display:none;';
        }
        /**
         * ** View/Edit END ***
         */

        /**
         * ** Remark START ***
         */
        $remarkStyle = 'display:none;';
        if ($lac->isInternalUse($_REQUEST['token'])) {
            $remarkStyle = '';
        }
        /**
         * ** Remark END ***
         */

        /**
         * ** Choose class START ***
         */
        $mainHTML = '';
        if ($IsUpdate) {
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && ($lac->IsUpdatePeriod() || $lac->IsAfterUpdatePeriod())) {
                $mainHTML = $this->getApplicantCheckingForm();
            }
        } else {
            if ($lac->schoolYearID) {
                $mainHTML = $this->getChooseClassForm($ClassLevel);
            }
        }
        /**
         * ** Choose class END ***
         */

        $x = <<<HTML
            <div class="notice_paper">
                <h2 style="font-size:18px;color:red;{$previewStyle}">
                    <center>
                        {$LangB5['Admission']['msg']['defaultpreviewpagemessage']}
                        <br/>
                        {$LangEn['Admission']['msg']['defaultpreviewpagemessage']}
                    </center>
                </h2>

                <h2 style="font-size:18px;color:red;{$viewStyle}">
                    <center>
                        {$LangB5['Admission']['msg']['defaultviewpagemessage']}
                        <br/>
                        {$LangEn['Admission']['msg']['defaultviewpagemessage']}
                    </center>
                </h2>
                <h2 style="font-size:18px;color:red;{$editStyle}">
                    <center>
                        {$LangB5['Admission']['msg']['defaultupdatepagemessage']}
                        <br/>
                        {$LangEn['Admission']['msg']['defaultupdatepagemessage']}
                    </center>
                </h2>

                <h2 style="font-size:18px;color:red;{$remarkStyle}">
                    <center>
                        {$kis_lang_b5['remarks']}
                        <br/>
                        {$kis_lang_en['remarks']}
                    </center>
                </h2>

                <div class="notice_paper_top">
                    <div class="notice_paper_top_right">
                        <div class="notice_paper_top_bg">
                            <h1 class="notice_title">{$LangB5['Admission']['onlineApplication']} {$LangEn['Admission']['onlineApplication']}</h1>
                        </div>
                    </div>
                </div>

                <div class="notice_paper_content">
                    <div class="notice_paper_content_right">
                        <div class="notice_paper_content_bg">
                            <div class="notice_content">
                                <div class="admission_content">
                                    {$Instruction}
                                  </div>
                                  <p class="spacer"></p>
                              </div>

                              {$mainHTML}
                          </div>
                      </div>
                  </div>

                  <div class="notice_paper_bottom">
                      <div class="notice_paper_bottom_right">
                          <div class="notice_paper_bottom_bg">
                          </div>
                      </div>
                  </div>
              </div>
HTML;

        $x = $this->applyFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, $html = $x, $Instruction, $ClassLevel, $IsUpdate);

        return $x;
    }

    /**
     * Admission form - second page Instruction for each class level
     */
    public function getInstructionContent($Instruction, $currentStep = 1)
    {
        global $Lang, $lac, $sys_custom;

        $x = $this->getWizardStepsUI($currentStep);

        /**
         * ** Instruction START ***
         */
        if (! $Instruction) {
            $Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'];
        }
        $x .= $Instruction;
        /**
         * ** Instruction END ***
         */

        /**
         * ** Agree button START ***
         */
        $agreeHTML = <<<HTML
            <br /><br />
            <table style="font-size:small;">
                <tr>
                    <td width="5">&nbsp;</td>
                    <td align="center" width="1%" rowspan="2"><input type="checkbox" name="Agree" id="Agree" value="1" style="transform: scale(2);  -webkit-transform: scale(2);"/></td>
                    <td width="5">&nbsp;</td>
                    <td><label for="Agree">{$Lang['Admission']['AgreeTermsCondition']}</label></td>
                </tr>
            </table>
HTML;
        $x .= $agreeHTML;
        /**
         * ** Agree button END ***
         */

        /**
         * ** Step button START ***
         */
        $lastStepId = 'step_index';
        $currentStepId = 'step_instruction';
        $nextStepId = 'step_input_form';

        if ($lac->schoolYearID) {
            $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId, $nextBtnLang = $Lang['Admission']['BeginApplication']);
        }
        /**
         * ** Step button END ***
         */

        return $x;
    }

    /**
     * Admission form - third page application form
     */
    public function getApplicationForm($BirthCertNo = "",$YearID="",$currentStep = 2)
    {
        global $fileData, $formData, $tempFolderPath, $LangB5, $LangEn, $libkis_admission, $sys_custom, $admission_cfg, $lac;

        $x = $this->getWizardStepsUI($currentStep);

        $x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, $html = '', $IsConfirm = false, $BirthCertNo, $IsUpdate = false, $YearID);

        $x .= "<span>{$LangB5['Admission']['munsang']['mandatoryfield']}<br />{$LangEn['Admission']['munsang']['mandatoryfield']}";

        /**
         * ** Step button START ***
         */
        $lastStepId = 'step_instruction';
        $currentStepId = 'step_input_form';
        $nextStepId = 'step_docs_upload';

        $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId);
        /**
         * ** Step button END ***
         */

        return $x;
    }

    /**
     * Admission Form - create/edit form docs upload page
     */
    protected function getDocUploadPage($IsConfirm = 0, $IsUpdate = 0, $AcademicYearID = 0, $YearID = 0)
    {
        $x = $this->getWizardStepsUI(3);

        $x .= $this->getDocUploadForm($IsConfirm, $IsUpdate, $AcademicYearID, $YearID);

        /**
         * ** Step button START ***
         */
        $lastStepId = 'step_input_form';
        $currentStepId = 'step_docs_upload';
        $nextStepId = 'step_confirm';

        $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId);
        /**
         * ** Step button END ***
         */

        return $x;
    }

    protected function getDocUploadForm($IsConfirm = 0, $IsUpdate = 0, $AcademicYearID = 0, $YearID = 0)
    {
        global $Lang;
        global $tempFolderPath, $fileData, $admission_cfg, $lac, $sys_custom, $intranet_root;

        $attachment_settings = $lac->getAttachmentSettings();
        $attachment_settings_count = sizeof($attachment_settings);
        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);

            if (count($application_details) > 0) {
                $applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'], array(
                    'applicationID' => $application_details['ApplicationID']
                ));
                $YearID = $application_details['ApplyLevel'];
            }
            // # Photo START ##
            $viewFilePath = (is_file($intranet_root . "/file/admission/" . $applicationAttachmentInfo[$application_details['ApplicationID']]['personal_photo']['attachment_link'][0]) && ($IsUpdate && ! $IsConfirm || $IsUpdate && $IsConfirm && ! $fileData['StudentPersonalPhoto']) ? ' <a href="download_attachment.php?type=personal_photo'./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]*/'" target="_blank" >' . $Lang['Admission']['viewSubmittedFile'] . '</a>' : '');
            // # Photo END ##
        }

        // # Attachments START ##
        $settings = $lac->getAttachmentSettings();
        $attachment_settings = array();
        foreach ($settings as $index => $setting) {
            if ($sys_custom['KIS_Admission']['DocUploadCustomClassLevel'] && $setting['ClassLevelStr']) {
                $classLevelArr = explode(',', $setting['ClassLevelStr']);

                if (in_array($YearID, $classLevelArr)) {
                    $attachment_settings[$index] = $setting;
                }
            } else {
                $attachment_settings[$index] = $setting;
            }
        }
        // # Attachments END ##

        if (! $lac->isInternalUse($_GET['token'])) {
            $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';
        } else {
            $star = '';
        }

        @ob_start();
        ?>
        <?php if($IsConfirm){ ?>
<h1><?=$Lang['Admission']['docsUpload'] ?></h1>
<?php } ?>

<table class="form_table" style="font-size: 15px">
            <?php if(!$IsConfirm){ ?>
                <tr>
		<td colspan="2"><span class="date_time">
                            <?=$Lang['Admission']['document']?>
                            (
                                <?=$Lang['Admission']['msg']['birthCertFormat']?>
                                <?= ($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1') ?> MB
                            )
                        </span></td>
	</tr>
            <?php } ?>
            <tr>
		<td class="field_title">
                    <?=$star?>
                    <?=$Lang['Admission']['personalPhoto']?>
                </td>
		<td>
                    <?php if($IsConfirm){ ?>
                        <?=stripslashes($fileData['StudentPersonalPhoto'])?>
                    <?php }else{ ?>
                        <input type="file" name="StudentPersonalPhoto"
			id="StudentPersonalPhoto" value="Add" class=""
			accept=".gif, .jpeg, .jpg, .png" data-is-optional="<?=$isOptional ?>"
			data-teacher-is-optional="1" />
                    <?php } ?>
                    <?=$viewFilePath?>
                </td>
	</tr>

            <?php
        foreach($attachment_settings as $i => $setting) {
            $attachment_name = $setting['AttachmentName'];

            $isOptional = $setting['IsOptional'];

            $accept = '.gif, .jpeg, .jpg, .png, .pdf';
            ?>
                <tr>
		<td class="field_title">
                        <?= (!$isOptional)? $star : ''?>
                        <?=$attachment_name?>
                    </td>
		<td>
                        <?php if($IsConfirm){ ?>
                            <?=stripslashes($fileData['OtherFile'.$i])?>
                        <?php }else{ ?>
                            <input type="file" name="OtherFile<?=$i?>"
			id="OtherFile<?=$i?>" value="Add" class="OtherFile"
			accept="<?=$accept ?>" data-is-optional="<?=$isOptional ?>"
			data-teacher-is-optional="1" />
                        <?php } ?>

                        <?php
            if (is_file($intranet_root . "/file/admission/" . $applicationAttachmentInfo[$application_details['ApplicationID']][$setting['AttachmentName']]['attachment_link'][0]) && ($IsUpdate && ! $IsConfirm || $IsUpdate && $IsConfirm && ! $fileData['OtherFile' . $i])) {
                ?>
                            <a
			href="download_attachment.php?type=<?=$setting['AttachmentName'] ?>"
			target="_blank"><?=$Lang['Admission']['viewSubmittedFile'] ?></a>
                        <?php
            }
            ?>

                    </td>
	</tr>
            <?php
        }
        if (! $IsConfirm && $IsUpdate) {
            ?>
                <tr>
		<td colspan="2">
                        <?=$star?>
                        <?=$Lang['Admission']['leaveBlankIfNoUpdate']?>
                    </td>
	</tr>
            <?php
        }
        ?>
        </table>
<?php
        $x .= ob_get_clean();
        return $x;
    }

    /**
     * Admission form - create applicant confirm page
     */
    public function getConfirmPageContent()
    {
        global $Lang, $fileData, $formData, $sys_custom, $lac;

        $formData = $this->stripslashesRecursive($formData);

        $x .= $this->getWizardStepsUI(4);
        $x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, $html = '', $isUpdate = false, $YearID=$formData['sus_status']);

        /**
         * ** Step button START ***
         */
        $lastStepId = 'step_docs_upload';
        $currentStepId = 'step_confirm';
        $nextStepId = '';

        $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId);
        /**
         * ** Step button END ***
         */

        return $x;
    }

    /**
     * Admission form - update applicant confirm page
     */
    public function getUpdateConfirmPageContent()
    {
        global $Lang, $fileData, $formData, $sys_custom, $lac;

        $formData = $this->stripslashesRecursive($formData);

        $x .= $this->getWizardUpdateStepsUI(2);

        $x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, $html = '', $isUpdate = true, $YearID=$lac->schoolYearID);

        /**
         * ** Step button START ***
         */
        $lastStepId = 'step_update_input_form';
        $currentStepId = 'step_confirm';
        $nextStepId = '';

        $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId, '', true);
        /**
         * ** Step button END ***
         */

        return $x;
    }

    /**
     * Admission form - edit page input applicant data form
     */
    public function getInputFormPageContent()
    {
        global $Lang, $fileData, $formData, $sys_custom, $lac;

        // remove the slashes of the special character
        if ($formData) {
            foreach ($formData as $key => $value) {
                $formData[$key] = stripslashes($value);
                if ($formData[$key] == "") {
                    $formData[$key] = " -- ";
                }
            }
        }

        $x = '';

        if ($lac->IsUpdatePeriod()) {
            $x .= '<h2 style="font-size:18px;color:red"><center>' . $Lang['Admission']['msg']['defaultupdatepagemessage'] . '</center></h2>';
            $x .= $this->getWizardUpdateStepsUI(1);

            $x .= $this->applyFilter(
                self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML,
                $html = '',
                $IsConfirm = false,
                $BirthCertNo = $fileData['InputStudentBirthCertNo'],
                $IsUpdate = true,
                $YearID = $lac->schoolYearID
            );

            $x .= "<span>{$Lang['Admission']['munsang']['mandatoryfield']}";

            /**
             * ** Step button START ***
             */
            $lastStepId = 'step_index';
            $currentStepId = 'step_update_input_form';
            $nextStepId = 'step_confirm';

            $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId, '', true);
        /**
         * ** Step button END ***
         */
        } else {
            $applicationSetting = $lac->getApplicationSetting($lac->schoolYearID);
            $applicationOthersInfo = current($lac->getApplicationOthersInfo($lac->schoolYearID, '', $fileData['InputApplicationID']));
            $lastContent = $applicationSetting[$applicationOthersInfo['classLevelID']]['LastPageContent'];
            $x .= $this->getAfterUpdateFinishPageContent($fileData['InputApplicationID'], $lastContent);
        }
        return $x;
    }

    /**
     * Admission form - create applicant finish page
     */
    public function getFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;

        $x = '<div class="admission_board">';
        $x .= $this->getWizardStepsUI(5);

        if ($ApplicationID) {
            $printLink = $lac->getPrintLink($schoolYearID, $ApplicationID, '', $this->getAdmissionLang($ApplicationID));
            $applicantEmail = $lac->getApplicantEmail($ApplicationID);

            $msg = str_replace('<!--ApplicationID-->', $ApplicationID, $Lang['Admission']['finishPage']['applicationSubmitted']);
            $msg2 = str_replace('<!--Email-->', $applicantEmail, $Lang['Admission']['finishPage']['confirmEmail']);
            $x .= <<<HTML
                <div class="admission_complete_msg">
                    <h1>
                        {$msg}
                        <br /><br />
                        <input type="button" value="{$Lang['Admission']['printsubmitform']}" onclick="window.open('{$printLink}','_blank');" />
                    </h1>
                    <br />
                    <h1>
                        <span>
                            {$msg2}
                        </span>
                    </h1>
HTML;
        } else {
            $x .= <<<HTML
                <div class="admission_complete_msg">
                    <h1 style="color:red;">
                        {$Lang['Admission']['finishPage']['failApply']}
                    </h1>
HTML;
        }
        $x .= '<br/>' . $LastContent . '</div>';
        if ((! $lac->isInternalUse($_GET['token']) || $lac->getTokenByApplicationNumber($ApplicationID) != '') && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $x .= '<div class="edit_bottom">
                    <input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'' . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST'] . '\'" value="' . $Lang['Admission']['finish'] . '" />
                </div>
                <p class="spacer"></p></div>';
        } else {
            $x .= '<div class="edit_bottom">';
            $x .= '<input id="finish_page_finish_button" style="" type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="' . $Lang['Admission']['finish'] . '" />';
            $x .= '</div>';
            $x .= '<p class="spacer"></p></div>';
        }
        $x .= '</div>';
        return $x;
    }

    /**
     * Admission form - edit applicant finish page
     */
    public function getUpdateFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;

        $x = <<<HTML
        <div class="admission_board">
            <h2 style="font-size:18px;color:red;{$editStyle}">
                <center>
                    {$Lang['Admission']['msg']['defaultupdatepagemessage']}
                </center>
            </h2>
HTML;
        $x .= $this->getWizardUpdateStepsUI(3);

        if ($ApplicationID) {
            $printLink = $lac->getPrintLink("", $ApplicationID, '', $this->getAdmissionLang($ApplicationID));
            $applicantEmail = $lac->getApplicantEmail($ApplicationID);

            $msg = str_replace('<!--ApplicationID-->', $ApplicationID, $Lang['Admission']['finishPage']['applicationUpdateSubmitted']);
            $msg2 = str_replace('<!--Email-->', $applicantEmail, $Lang['Admission']['finishPage']['confirmEmail']);
            $x .= <<<HTML
                <div class="admission_complete_msg">
                    <h1>
                        {$msg}
                        <br /><br />
                        <input type="button" value="{$Lang['Admission']['printsubmitform']}" onclick="window.open('{$printLink}','_blank');" />
                    </h1>
                    <br />
                    <h1>
                        <span>
                           {$msg2}
                        </span>
                    </h1>
HTML;
        } else {
            $x .= <<<HTML
                <div class="admission_complete_msg">
                    <h1 style="color:red;">
                        {$Lang['Admission']['finishPage']['failUpdate']}
                    </h1>
HTML;
        }
        if (! $LastContent || ! $ApplicationID) {
            $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; // Henry 20131107
        }
        $x .= '<br/>' . $LastContent . '</div>';

        $x .= '<div class="edit_bottom">
                <input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="' . $Lang['Admission']['finish'] . '" />
            </div>
            <p class="spacer"></p></div>';
        $x .= '</div>';
        return $x;
    }

    /**
     * Admission Form - create application send email
     */
    function getFinishPageEmailContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $paymentEmail = '')
    {
        global $PATH_WRT_ROOT, $Lang, $lac, $admission_cfg, $sys_custom;

        if ($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID) == '') {
            if ($ApplicationID) {
                $schoolYearID = $schoolYearID ? $schoolYearID : $lac->schoolYearID;
                $stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID, '', $ApplicationID);

                $x .= '閣下的報名申請表已收到。學童姓名為 ' . $stuedentInfo[0]['student_name_b5'] . '，申請編號為 <u>' . $ApplicationID . '</u>。<br/>';
                $x .= 'Your application form has been received, name of child is ' . $stuedentInfo[0]['student_name_en'] . ' and the application number is <u>' . $ApplicationID . '</u>.';

                $x .= '<br/><br/><br/>學校將透過電郵通知閣下有關面見安排，相關資訊可於學校網頁瀏覽。<br/>';
                $x .= 'You will be informed of arrangements for interview(s) via email. Please visit the school website for related information.';

                $x .= '<br/><br/><br/>謝謝您的申請！<br/>';
                $x .= 'Thanks for your application!';

                if (! $LastContent) {
                    $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage'];
                }
                $x .= '<br/><br/>' . $LastContent;
            }
        } else {
            if ($ApplicationID) {
                $x .= '報名表已遞交，申請編號為 <u>' . $ApplicationID . '</u>。<br/>';
                $x .= 'Your application form has been successfully submitted and the application number is <u>' . $ApplicationID . '</u>.<br/>';

                $x .= '<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
                $x .= 'Click this hyperlink to view your application form.';
                $x .= "<br/><a target='_blank' href='http://" . $_SERVER['HTTP_HOST'] . $lac->getPrintLink("", $ApplicationID) . "'>http://" . $_SERVER['HTTP_HOST'] . $lac->getPrintLink("", $ApplicationID) . "</a>";

                $x .= '<br/><br/><br/>謝謝您使用網上報名服務！<br/>';
                $x .= 'Thanks for lodging your application online!';

                $x .= '<br/><br/>' . $LastContent;
            } else {
                $x .= '未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
                $x .= '<br/><br/>';
                $x .= 'Your application is rejected. Please try to apply again or contact our school.';
            }
        }
        $x = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_APPLICANT, $x, $ApplicationID);

        return $x;
    }

    /**
     * Admission Form - update application send email
     */
    function getUpdateFinishPageEmailContent($ApplicationID = '', $LastContent = '', $schoolYearID = '')
    {
        global $PATH_WRT_ROOT, $Lang, $lac, $admission_cfg, $sys_custom;
        include_once ($PATH_WRT_ROOT . "lang/admission_lang.b5.php");
        if ($ApplicationID) {
            $x .= '報名表資料已更改，申請編號為 <u>' . $ApplicationID . '</u>。<br/>';
            $x .= 'Your application form has been updated and the application number is <u>' . $ApplicationID . '</u>.<br/>';

            $x .= '<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
            $x .= 'Click this hyperlink to view your application form.';
            $x .= "<br/><a target='_blank' href='http://" . $_SERVER['HTTP_HOST'] . $lac->getPrintLink("", $ApplicationID) . "'>http://" . $_SERVER['HTTP_HOST'] . $lac->getPrintLink("", $ApplicationID) . "</a>";

            $x .= '<br/><br/><br/>謝謝您使用網上報名服務！<br/>';
            $x .= 'Thanks for lodging your application online!';
            $x .= '<br/><br/>' . $LastContent;
        } else {
            $x .= '未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
            $x .= '<br/><br/>';
            $x .= 'Your application is rejected. Please try to apply again or contact our school.';
        }
        $x = $this->applyFilter(self::FILTER_ADMISSION_UPDATE_FORM_EMAIL_APPLICANT, $x, $ApplicationID);

        return $x;
    }

    /**
     * Portal - get the top-module-tab
     * Lang file: /lang/kis/apps/lang_admission_*.php
     * Lang: $kis_lang[{$tabID}]
     */
    public function getModuleTab()
    {
        $defaultTab = array(
            'studentinfo',
            'parentinfo',
            'otherinfo',
            'remarks'
        );
        $tab = $this->applyFilter(self::FILTER_ADMISSION_TOP_MENU_TAB, $defaultTab);

        return $tab;
    }

    /**
     * Portal - get the top-module-tab default index
     */
    public function getModuleTabDefaultIndex()
    {
        return count($this->getModuleTab());
    }


	/**
	 * Portal - get the view/edit applicant page
	 */
	public function getDetailsPage($display, $isEdit){
		global $kis_data, $schoolYearID;

		$dafsUi = new \AdmissionSystem\DynamicAdmissionFormSystemUi($schoolYearID);
		echo $dafsUi->generateAdminDetailsPage($display, $isEdit);
	}

    /**
     * Portal - print admission form
     */
    public function getPDFContent($schoolYearID, $applicationIDAry, $type = '')
    {
        global $PATH_WRT_ROOT, $lac, $admission_cfg, $setting_path_ip_rel, $plugin, $kis_lang;

        global $baseFilePath;
        $baseFilePath = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}";
        $baseFilePath = $this->applyFilter(self::FILTER_ADMISSION_PDF_BASE_PATH, $baseFilePath);

        // ####### Init PDF START ########
        $templateHeaderPath = "{$baseFilePath}/pdf/header.php";
        $templatePath = "{$baseFilePath}/pdf/application_form.php";
        require_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

        $mpdf = new \mPDF($mode = '', $format = 'A4', $default_font_size = 0, $default_font = 'msjh', $marginLeft = 10, $marginRight = 10, $marginTop = 5, $marginBottom = 0/*,
            $marginHeader=9,
            $marginFooter=9,
            $orientation='P'*/
        );
        $mpdf->mirrorMargins = 1;
        // ####### Init PDF END ########

        // ####### Load header to PDF START ########
        ob_start();
        include ($templateHeaderPath);
        $pageHeader = ob_get_clean();

        $mpdf->WriteHTML($pageHeader);
        // ####### Load header to PDF END ########

        // ####### Load data to PDF START ########
        global $applicationIndex, $applicationID;
        foreach ((array) $applicationIDAry as $applicationIndex => $applicationID) {

            // ### Load Template START ####
            ob_start();
            include ($templatePath);
            $page1 = ob_get_clean();
            // ### Load Template END ####

            $mpdf->WriteHTML($page1);
        }
        // ####### Load data to PDF END ########

        $mpdf->Output();
    }

    /**
     * Helper function
     */
    protected function stripslashesRecursive($formData)
    {
        if ($formData) {
            foreach ($formData as $key => $value) {
                if (is_array($value)) {
                    $formData[$key] = $this->stripslashesRecursive($value);
                } else {
                    $formData[$key] = stripslashes($value);
                    if ($formData[$key] == "") {
                        $formData[$key] = " -- ";
                    }
                }
            }
        }
        return $formData;
    }
} // End Class
