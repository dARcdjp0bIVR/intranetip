<?php
namespace AdmissionSystem;

trait ActionFilterQueueTrait
{

    protected $actionQueue = array();

    protected $filterQueue = array();

    public function addAction($actionName, $func, $priority = 10)
    {
        $this->actionQueue[$actionName][$priority][] = $func;
        ksort($this->actionQueue[$actionName]);
    }

    public function doAction($actionName)
    {
        if (! isset($this->actionQueue[$actionName])) {
            return;
        }
        
        $args = func_get_args();
        array_shift($args);
        foreach ($this->actionQueue[$actionName] as $priority => $functions) {
            foreach ($functions as $func) {
                call_user_func_array($func, $args);
            }
        }
    }

    public function addFilter($filterName, $func, $priority = 10)
    {
        $this->filterQueue[$filterName][$priority][] = $func;
        ksort($this->filterQueue[$filterName]);
    }

    public function applyFilter($filterName, $value)
    {
        if (! isset($this->filterQueue[$filterName])) {
            return $value;
        }
        
        $args = func_get_args();
        array_shift($args);
        array_shift($args);
        array_unshift($args, $value);
        
        $values = $defaultValue;
        foreach ($this->filterQueue[$filterName] as $priority => $functions) {
            foreach ($functions as $func) {
                $val = call_user_func_array($func, $args);
                $args[0] = $val;
            }
        }
        return $args[0];
    }
}
