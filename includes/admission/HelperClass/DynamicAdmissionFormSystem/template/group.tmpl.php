<?php
global $lauc;

// debug_r($group);
// debug_rt($fields);


$grouppedFields = $this->dafs->grouppedFieldList( $fields );

?>
<section
        class="form displaySection display_pagePersonalInfo display_pageConfirmation display_group_<?= $group['GroupID']; ?>"
        style="display: none;">
    <div class="form-header marginB10">
	    <?=$lauc->getLangStr(array(
		    'b5' => $group['TitleB5'],
		    'en' => $group['TitleEn'],
	    )) ?>
    </div>

	<?php if ( $group['OtherAttributeArr']['label'] ): ?>
        <div style="padding: 0 20px;color: #aaa;">
			<?= $group['OtherAttributeArr']['label'] ?>
        </div>
	<?php endif; ?>

    <div class="sheet">
		<?php
		if ( $custFilePath ) {
			include $custFilePath;
		} else {

			foreach ( $grouppedFields as $grouppedField ):
				$firstField = $grouppedField[0][0];

				$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $firstField['ValidationArr'] ) );
				$hasLabel   = ! ! ( $firstField['LabelB5'] . $firstField['LabelEn'] . $firstField['LabelGb'] );

				#### Groupped START ####
				$itemClass = '';
				if ( $firstField['OtherAttributeArr']['Groupped'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_GROUP_TYPE_TWO_GROUP ) {
					$itemClass = 'item-half';
				} elseif ( $firstField['OtherAttributeArr']['Groupped'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_GROUP_TYPE_THREE_GROUP ) {
					$itemClass = 'item-In3';
				}
				#### Groupped END ####

				$textInstArr = $firstField['ExtraArr']['text-inst'];

				if ( $firstField['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR ) {
					if(strpos($classLevel, '(') !== false && strpos($classLevel, ')') !== false ){
						$firstField['LabelExtra'] = "{$classLevel}";
					}
					else{
						$firstField['LabelExtra'] = "({$classLevel})";
					}

				} elseif ( $firstField['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_ROW ) {
					echo trim( $this->generateCustomRowHtml( $grouppedField ) );
					continue;
				}
				?>
            <div class="item <?= $itemClass ?>">
				<?php if ( $textInstArr ): ?>
                    <div class="text-inst">
	                    <?=$lauc->getLangStr(array(
		                    'b5' => $textInstArr[0],
		                    'en' => $textInstArr[1],
	                    )) ?>
                    </div>
                <?php endif; ?>

				<?php if ( $hasLabel ): ?>
                    <div class="itemLabel <?= ( $isRequired ) ? 'requiredLabel' : '' ?>">
	                    <?=$lauc->getLangStr(array(
		                    'b5' => $firstField['LabelB5'],
		                    'en' => $firstField['LabelEn'],
	                    )) ?>
                        <?= $firstField['LabelExtra'] ?>
                    </div>
                <?php endif; ?>

				<?php
				foreach ( $grouppedField as $rowFields ) {
					foreach ( $rowFields as $field ) {
						echo trim( $this->generateFieldHtml( $field ) );
					}
				}
				?>
                </div><?php
			endforeach;
		}
		?>
    </div>
</section>
