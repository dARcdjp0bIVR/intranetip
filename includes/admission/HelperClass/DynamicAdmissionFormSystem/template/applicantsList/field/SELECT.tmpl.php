<?php
global $dobStart, $dobEnd, $libkis_admission;

$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];
$hasLabel       = ! ! ( $field['LabelB5'] . $field['LabelEn'] . $field['LabelGb'] );
$isRequired     = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );
$isRequired = $isRequired || !$libkis_admission->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get setting START ####
$options = $extraArr['options'];
#### Get setting END ####
?>
<span class="itemInput itemInput-selector">
	<span class="selector" style="display: block">
		<select
                id="field_<?= $field['FieldID'] ?>"
                name="field_<?= $field['FieldID'] ?>"
                data-field="<?= $extraArr['field'] ?>"
                style="width: 100%;"
        >
            <?php foreach ( $options as $index => $option ): ?>
                <option
                        value='<?= $option[0] ?>'
			            <?= ( $option[0] == $value ) ? 'selected' : '' ?>
                ><?= $option[1] ?> <?= $option[2] ?></option>
            <?php endforeach; ?>
		</select>
	</span>

	<?php include( __DIR__ . '/warning.tmpl.php' ); ?>
</span>
