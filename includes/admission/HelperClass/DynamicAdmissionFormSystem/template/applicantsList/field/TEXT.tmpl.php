<?php
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

?>

<span class="itemInput">
	<div class="textbox-floatlabel">
        <input
        	name="field_<?=$field['FieldID'] ?>"
        	type="text"
        	id="field_<?=$field['FieldID'] ?>"
        	class="textboxtext"
        	value="<?=$value ?>"
            data-original-value="<?=$value ?>"
    		data-field="<?=$extraArr['field']?>"
        >
		<?php include(__DIR__.'/warning.tmpl.php'); ?>
	</div>
</span>