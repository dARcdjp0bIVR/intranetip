<?php
global $libkis_admission;
$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$hasLabel   = ! ! ( $field['LabelB5'] . $field['LabelEn'] . $field['LabelGb'] );
$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );
$isRequired = $isRequired || !$libkis_admission->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

$extraArr['optionsY'] = (array) $extraArr['optionsY'];
$extraArr['optionsN'] = (array) $extraArr['optionsN'];

if($value) {
	$_value = explode( ';', $value );
	$value  = array_shift($_value);
	$options = $_value;
}else{
    $value = '';
    $options = [];
}

?>

<span class="itemInput itemInput-choice">
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_Y"
                name="field_<?= $field['FieldID'] ?>"
                value="Y"
                <?=($value === 'Y')?'checked':''?>
        ><label for="field_<?= $field['FieldID'] ?>_Y"><?=$kis_lang['Admission']['yes']?></label>
	</span>
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_N"
                name="field_<?= $field['FieldID'] ?>"
                value="N"
                <?=($value === 'N')?'checked':''?>
        ><label for="field_<?= $field['FieldID'] ?>_N"><?=$kis_lang['Admission']['no']?></label>
	</span>

    <?php
    $countY = count( $extraArr['optionsY'] );
    foreach ( $extraArr['optionsY'] as $index=>$optionsY ):
	    $cssClass = ( $countY === 2 ) ? 'textbox-half' : "textbox-in{$countY}";
	    ?>
        <span class="options_<?= $field['FieldID'] ?>_details Y" style="display: none;">
            <div class="textbox-floatlabel <?= $cssClass ?>">
                <input
                        type="text"
                        id="field_<?= $field['FieldID'] ?>_<?=$index?>"
                        class="field_<?= $field['FieldID'] ?>_option Y"
                        name="field_<?= $field['FieldID'] ?>_option[]"
                        value="<?=$options[$index]?>"
                        placeholder="<?=Get_Lang_Selection($optionsY[0],$optionsY[1])?>"
                />
            </div>
        </span>
    <?php
    endforeach;

    $countN = count( $extraArr['optionsN'] );
    foreach ( $extraArr['optionsN'] as $index=>$optionsN ):
	    $cssClass = ( $countN === 2 ) ? 'textbox-half' : "textbox-in{$countN}";
	    ?>
        <span class="options_<?= $field['FieldID'] ?>_details N" style="display: none;">
            <div class="textbox-floatlabel <?= $cssClass ?>">
                <input
                        type="text"
                        id="field_<?= $field['FieldID'] ?>_<?=$index?>"
                        class="field_<?= $field['FieldID'] ?>_option N"
                        name="field_<?= $field['FieldID'] ?>_option[]"
                        value="<?=$options[$index]?>"
                        placeholder="<?=Get_Lang_Selection($optionsY[0],$optionsY[1])?>"
                />
            </div>
        </span>
    <?php
    endforeach;
    ?>

	<?php include( __DIR__ . '/warning.tmpl.php' ); ?>
</span>
<script>
  $(function () {
    var $field = $('[name="field_<?=$field['FieldID'] ?>"]');
    $field.click(updateValue);
    $('.field_<?=$field['FieldID'] ?>_option').change(updateValue);

    function updateValue() {
      var checkedVal = $field.filter(':checked').val();

      //// Update UI START ////
      $('.field_<?= $field['FieldID'] ?>_option').prop('disabled', true);
      $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).prop('disabled', false);
      $('.options_<?= $field['FieldID'] ?>_details').hide();
      $('.options_<?= $field['FieldID'] ?>_details.'+checkedVal).show().css('display', 'block')
      //// Update UI END ////

      //// Update value START ////
      var $data = $('#data_<?=$field['FieldID'] ?>');
      $data.find('.dataValue, .Y, .N').hide();

      if (checkedVal == '') {
        $data.find('.dataValue-empty').show();
      } else {
        var valueArr = [];
        $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).each(function(){
          valueArr.push($(this).val());
        });
        $data.find('.dataValue-non-empty, .'+checkedVal).show();
        $data.find('.value').html(valueArr.join(', '));
      }
      //// Update value END ////
    }

    updateValue();
  });
</script>
