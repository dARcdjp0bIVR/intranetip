<?php
global $libkis_admission;
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$libkis_admission->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

?>

<span class="itemInput">
	<div class="textbox-floatlabel">
		<textarea
    		type="text"
    		class="textboxtext"
    		rows="5"
    		id="field_<?=$field['FieldID'] ?>"
    		name="field_<?=$field['FieldID'] ?>"
    		data-field="<?=$extraArr['field']?>"
		><?=$value ?></textarea>
		<?php include(__DIR__.'/warning.tmpl.php'); ?>
	</div>
</span>