<?php
global $dobStart, $dobEnd, $lac;

$minDate        = ( $dobStart ) ? $dobStart : '1970-01-01';
$maxDate        = ( $dobEnd ) ? $dobEnd : date( 'Y-m-d' );
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));
$isFuture   = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_DATE_IS_FUTURE, $validation ) );


#### Get setting START ####
if ( $isFuture ) {
	$maxYear = $extraArr['maxYearIncrease'] ? (( (int) date( 'Y' ) ) + (int)$extraArr['maxYearIncrease']) : (( $extraArr['maxYear'] ) ? $extraArr['maxYear'] : ( (int) date( 'Y' ) ) + 10);
	$minYear = $extraArr['minYearDecrease'] ? (( (int) date( 'Y' ) ) - (int)$extraArr['minYearDecrease']) : date( 'Y' );
	$minDate = date( 'Y-m-d' );
	$maxDate = "{$maxYear}-12-31";
} else {
	$maxYear = ( $extraArr['maxYear'] ) ? $extraArr['maxYear'] : (int) date( 'Y' );
	$minYear = ( $extraArr['minYear'] ) ? $extraArr['minYear'] : ( (int) date( 'Y' ) ) - 10;

	if ( $minYear == 'DOB' ) {
		$minYear = substr( $dobStart, 0, 4 );
		$minYear = ( $minYear ) ? $minYear : (int) date( 'Y' ) - 10;
	}

	if ( $maxYear == 'DOB' ) {
		$maxYear = substr( $dobEnd, 0, 4 );
		$maxYear = ( $maxYear ) ? $maxYear : (int) date( 'Y' );
	}
}
#### Get setting END ####

list($selectedYear, $selectedMonth, $selectedDay) = explode('-', $value);
?>
<span class="itemInput itemInput-selector" data-min-date="<?=$minDate?>" data-max-date="<?=$maxDate?>">
	<span class="selector">
		<select
    		id="field_<?=$field['FieldID'] ?>_year"
    		name="field_<?=$field['FieldID'] ?>_year"
    		data-field="<?=$extraArr['field']?>"
		>
			<option value=''>- <?=$kis_lang['Admission']['pleaseSelect'] ?> -</option>
			<?php for($i=$minYear;$i<=$maxYear;$i++): ?>
				<option value="<?=$i ?>" <?=($selectedYear==$i)?'selected':'' ?>><?=$i ?></option>
			<?php endfor; ?>
		</select>
	</span> - <span class="selector">
		<select
    		id="field_<?=$field['FieldID'] ?>_month"
    		name="field_<?=$field['FieldID'] ?>_month"
		>
			<option value=''>- <?=$kis_lang['Admission']['pleaseSelect'] ?> -</option>
			<?php for($i=1;$i<=12;$i++): ?>
				<option value="<?=$i ?>" <?=($selectedMonth==$i)?'selected':'' ?>><?=str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
			<?php endfor; ?>
		</select>
	</span> - <span class="selector">
		<select
    		id="field_<?=$field['FieldID'] ?>_day"
    		name="field_<?=$field['FieldID'] ?>_day"
		>
			<option value=''>- <?=$kis_lang['Admission']['pleaseSelect'] ?> -</option>
			<?php for($i=1;$i<=31;$i++): ?>
				<option value="<?=$i ?>" <?=($selectedDay==$i)?'selected':'' ?>><?=str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
			<?php endfor; ?>
		</select>
	</span>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span>