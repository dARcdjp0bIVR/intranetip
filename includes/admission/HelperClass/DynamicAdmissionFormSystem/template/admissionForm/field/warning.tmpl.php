<?php
global $lauc;
?>
<div style="">
	<div class="remark remark-warn required hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必須填寫',
			'en' => 'Required',
		))?>
	</div>
	<div class="remark remark-warn non_number hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '不可包含數字',
			'en' => 'Must not contains number',
		))?>
	</div>
	<div class="remark remark-warn number hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必須是數字',
			'en' => 'Must be number',
		))?>
	</div>
	<div class="remark remark-warn integer hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必須是整數',
			'en' => 'Must be integer',
		))?>
	</div>
	<div class="remark remark-warn postive_number hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必須是正整數',
			'en' => 'Must be postive integer',
		))?>
	</div>
	<div class="remark remark-warn number_in_range hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '範圍必須由 {min} - {max}',
			'en' => 'Must between {min} - {max}',
		))?>
	</div>
	<div class="remark remark-warn chinese hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必須是中文字',
			'en' => 'Must be Chinese character',
		))?>
	</div>
	<div class="remark remark-warn has_chinese hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必需包含中文字',
			'en' => 'Must contains Chinese character',
		))?>
	</div>
	<div class="remark remark-warn non_chinese hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '不可包含中文字',
			'en' => 'Must not contains Chinese character',
		))?>
	</div>
	<div class="remark remark-warn english hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '必須是英文字',
			'en' => 'Must be English character',
		))?>
	</div>
	<div class="remark remark-warn date hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '日期錯誤',
			'en' => 'Invalid date',
		))?>
	</div>
	<div class="remark remark-warn dob hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '出生日期與申請組別不符',
			'en' => 'Invalid Birthday Range of Student',
		))?>
	</div>
	<div class="remark remark-warn date_range hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '日期範圍不符',
			'en' => 'Invalid Date Range',
		))?>
	</div>
	<div class="remark remark-warn hkid email hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '格式錯誤',
			'en' => 'Invalid format',
		))?>
	</div>
	<div class="remark remark-warn hkid_duplicate hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '出生證明書號碼已被使用！請輸入其他出生證明書號碼。',
			'en' => 'The Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.',
		))?>
	</div>
	<div class="remark remark-warn equal hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '資料不相符',
			'en' => 'Data must be same',
		))?>
	</div>
	<div class="remark remark-warn not_complete hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '資料不完整',
			'en' => 'Data in-completed',
		))?>
	</div>
	<div class="remark remark-warn option_repeat hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '選擇重覆',
			'en' => 'Duplicate selection',
		))?>
	</div>
	<div class="remark remark-warn length hide" style="clear:both;">
		<?=$lauc->getLangStr(array(
			'b5' => '長度必需為 {length}',
			'en' => 'Length must be {length}',
		))?>
	</div>
	<div class="remark remark-warn custom hide" style="clear:both;">
	</div>
</div>
