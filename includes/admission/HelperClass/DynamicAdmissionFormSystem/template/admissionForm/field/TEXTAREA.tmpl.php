<?php
global $lauc, $lac;
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

$itemInputClass = '';
if($width == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_WIDTH_HALF){
    $itemInputClass = 'itemInput-half';
    $itemDataClass = 'itemData-half';
}/*elseif($width == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_WIDTH_ONE_THIRD){
    $itemInputClass = '';
    throw new Exception('Not implemented');
}elseif($width == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_WIDTH_ONE_FOURTH){
    $itemInputClass = '';
    throw new Exception('Not implemented');
}*/
?>

<span class="itemInput <?=$itemInputClass ?>">
	<div class="textbox-floatlabel">
		<textarea
    		type="text"
    		id="field_<?=$field['FieldID'] ?>"
    		name="field_<?=$field['FieldID'] ?>"
    		value=""
    		data-field="<?=$extraArr['field']?>"
    		class="<?=($value)?'notEmpty':'' ?>"
		><?=$value ?></textarea>
		<div class="textboxLabel <?=($isRequired)?'requiredLabel':'' ?>">
            <?=$lauc->getLangStr(array(
	            'b5' => $field['TitleB5'],
	            'en' => $field['TitleEn'],
            ))?>
		</div>

		<?php include(__DIR__.'/warning.tmpl.php'); ?>
	</div>
</span><!--

--><span class="itemData <?=$itemDataClass ?>">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
            'b5' => $field['TitleB5'],
            'en' => $field['TitleEn'],
        ))?>
	</div>
	<div class="dataValue" id="data_<?=$field['FieldID'] ?>"></div>
</span><!--

--><script>
$(function(){
	$('#field_<?=$field['FieldID'] ?>').on('change keyup paste', updateValue);

	function updateValue(){
		if($('#field_<?=$field['FieldID'] ?>').val() == ''){
			$('#data_<?=$field['FieldID'] ?>').addClass('dataValue-empty').html('－－');
		}else{
			$('#data_<?=$field['FieldID'] ?>').removeClass('dataValue-empty').html($('#field_<?=$field['FieldID'] ?>').val().replace(/\r?\n/g,'<br/>'));
		}
	}

	updateValue();
});
</script>
