<?php
global $admission_cfg;
global $lauc, $lac;

$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];
$hasLabel       = ! ! ( $field['LabelB5'] . $field['LabelEn'] . $field['LabelGb'] );
$isRequired     = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));


#### Get setting START ####
$options = $extraArr['options'];
#### Get setting END ####
?>
<?php if ( $hasLabel ): ?>
    <div class="itemLabel <?= ( $isRequired ) ? 'requiredLabel' : '' ?>">
	    <?=$lauc->getLangStr(array(
		    'b5' => $field['LabelB5'],
		    'en' => $field['LabelEn'],
	    ))?>
		<?= $field['LabelExtra'] ?>
    </div>
<?php endif; ?>

<span class="itemInput itemInput-selector">
	<span class="selector" style="display: block">
		<select
                id="field_<?= $field['FieldID'] ?>"
                name="field_<?= $field['FieldID'] ?>"
                data-field="<?= $extraArr['field'] ?>"
        >
            <?php foreach ( $options as $index => $option ): ?>
                <option
                        value='<?= $option[0] ?>'
			            <?= ( $option[0] == $value ) ? 'selected' : '' ?>
                >
                    <?=$lauc->getLangStr(array(
                        'b5' => $option[1],
                        'en' => $option[2],
                    ))?>
                </option>
            <?php endforeach; ?>
		</select>
	</span>

	<?php include( __DIR__ . '/warning.tmpl.php' ); ?>
</span><!--

--><span class="itemData">
	<div class="dataLabel">
		<?= $field['LabelB5'] ?>
		<?= $field['LabelEn'] ?>
	</div>
	<div class="dataValue" id="data_<?= $field['FieldID'] ?>">
        <span data-value="">－－</span>
        <?php foreach ( $options as $index => $option ): ?>
            <span data-value="<?= $option[0] ?>">
                    <?=$lauc->getLangStr(array(
	                    'b5' => $option[1],
	                    'en' => $option[2],
                    ))?>
            </span>
        <?php endforeach; ?>
    </div>
</span><!--

-->
<script>
  $(function () {
    $('[name="field_<?=$field['FieldID'] ?>"]').change(updateValue);

    function updateValue() {
      var $dataContainer = $('#data_<?=$field['FieldID'] ?>');
      $dataContainer.find('span').hide();

      var value = $('#field_<?=$field['FieldID'] ?>').val();
      if (value == '') {
        $dataContainer.addClass('dataValue-empty');
      } else {
        $dataContainer.removeClass('dataValue-empty');
        $dataContainer.find('[data-value="' + value + '"]').show();
      }
    }

    updateValue();
  });
</script>
