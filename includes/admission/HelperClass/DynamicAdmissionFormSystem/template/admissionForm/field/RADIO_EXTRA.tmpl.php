<?php
global $lauc, $lac;
$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get option START ####
$options = $extraArr['options'];
#### Get option END ####

$valueArr = explode(';', $value);

?>

<span class="itemInput itemInput-choice">
    <?php foreach($options as $index=>$option):?>
    	<span>
    		<input
        		type="radio"
        		id="field_<?=$field['FieldID'] ?>_<?=$index ?>"
        		name="field_<?=$field['FieldID'] ?>"
                value="<?=$option[0] ?>"
                data-field="<?=$extraArr['field']?>"
                <?= ($option[0] == $valueArr[0])?'checked':'' ?>
            ><label for="field_<?=$field['FieldID'] ?>_<?=$index ?>">
                <?=$lauc->getLangStr(array(
                    'b5' => $option[1],
                    'en' => $option[2]
                ))?>
            </label>
    	</span>
	<?php endforeach; ?>
	
	<?php 
		$optionArr = array();
		foreach($options as $index=>$option):
			$optionArr[] = $option[0];
		$countOption = count($option);
	    $cssClass = ( ($countOption-3) === 2 ) ? 'textbox-half' : "textbox-in{$countOption-3}";
	    ?>
        <span class="options_<?= $field['FieldID'] ?>_details <?=$option[0]?>" style="display: none;">
            <div class="textbox-floatlabel <?= $cssClass ?>">
                <input
                        type="text"
                        id="field_<?= $field['FieldID'] ?>_<?=$index?>"
                        class="field_<?= $field['FieldID'] ?>_option <?=$option[0]?>"
                        name="field_<?= $field['FieldID'] ?>_option[]"
                        value="<?=$valueArr[$index+1]?>"
                />
                <div class="textboxLabel ">
                    <?=$lauc->getLangStr(array(
                        'b5' => $option[3],
                        'en' => $option[4],
                    )) ?>
                </div>
            </div>
        </span>
    <?php
	endforeach; ?>

	<?php include( __DIR__ . '/warning.tmpl.php' ); ?>
</span><!--

--><span class="itemData <?= $itemDataClass ?>" id="data_<?= $field['FieldID'] ?>">
	<div class="dataLabel">
		<?= $field['TitleB5'] ?>
		<?= $field['TitleEn'] ?>
	</div>
	<div class="dataValue dataValue-empty">－－</div>
	<div class="dataValue dataValue-non-empty">
	<?php foreach($options as $index=>$option):?>
        <span class="<?=$option[0]?>">
            <?=$lauc->getLangStr(array(
                'b5' => $option[1],
                'en' => $option[2],
            )) ?>
        </span>
    <?php endforeach; ?>
        <div class="value"></div>
    </div>
</span><!--

-->
<script>
  $(function () {
    var $field = $('[name="field_<?=$field['FieldID'] ?>"]');
    $field.click(updateValue);
    $('.field_<?=$field['FieldID'] ?>_option').change(updateValue);

    function updateValue() {
      var checkedVal = $field.filter(':checked').val();

      //// Update UI START ////
      $('.field_<?= $field['FieldID'] ?>_option').prop('disabled', true);
      $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).prop('disabled', false);
      $('.options_<?= $field['FieldID'] ?>_details').hide();
      $('.options_<?= $field['FieldID'] ?>_details.'+checkedVal).show().css('display', 'block')
      //// Update UI END ////

      //// Update value START ////
      var $data = $('#data_<?=$field['FieldID'] ?>');
      $data.find('.dataValue, <?php
      echo implode(', ',$optionArr);
      ?>').hide();

      if (checkedVal == '') {
        $data.find('.dataValue-empty').show();
      } else {
        var valueArr = [];
        $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).each(function(){
          valueArr.push($(this).val());
        });
        $data.find('.dataValue-non-empty, .'+checkedVal).show();
        $data.find('.value').html(valueArr.join(', '));
      }
      //// Update value END ////
    }

    updateValue();
  });
</script>
