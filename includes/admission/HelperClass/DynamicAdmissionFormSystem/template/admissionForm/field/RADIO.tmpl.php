<?php
global $lauc, $lac;
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get option START ####
$options = $extraArr['options'];
#### Get option END ####

?>
<span class="itemInput itemInput-choice">
    <?php foreach($options as $index=>$option):?>
    	<span>
    		<input
        		type="radio"
        		id="field_<?=$field['FieldID'] ?>_<?=$index ?>"
        		name="field_<?=$field['FieldID'] ?>"
                value="<?=$option[0] ?>"
                data-field="<?=$extraArr['field']?>"
                <?= ($option[0] == $value)?'checked':'' ?>
            ><label for="field_<?=$field['FieldID'] ?>_<?=$index ?>">
                <?=$lauc->getLangStr(array(
                    'b5' => $option[1],
                    'en' => $option[2]
                ))?>
            </label>
    	</span>
	<?php endforeach; ?>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span><!--

--><span class="itemData">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
            'b5' => $field['LabelB5'],
            'en' => $field['LabelEn'],
        ))?>
	</div>
	<div class="dataValue" id="data_<?=$field['FieldID'] ?>"></div>
</span><!--

--><script>
$(function(){
	$('[name="field_<?=$field['FieldID'] ?>"]').click(updateValue);

	function updateValue(){
		if($('[name="field_<?=$field['FieldID'] ?>"]:checked').val()){
			$('#data_<?=$field['FieldID'] ?>').removeClass('dataValue-empty').html($('[name="field_<?=$field['FieldID'] ?>"]:checked').next().html());
		}else{
			$('#data_<?=$field['FieldID'] ?>').addClass('dataValue-empty').html('－－');
		}
	}

	updateValue();
});
</script>
