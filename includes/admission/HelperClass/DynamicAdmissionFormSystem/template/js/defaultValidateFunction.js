$(function () {
  'use strict';


  function check_hkid(hkid) {
    // hkid = $.trim(hkid);
    // hkid = hkid.replace(/\s/g, '');
    // hkid = hkid.toUpperCase();
    // $(":input[name='id_no']").val(hkid);

    var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
    var ra = re.exec(hkid);

    if (ra != null) {
      var p1 = ra[1];
      var p2 = ra[2];
      var p3 = ra[4];
      var check_sum = 0;
      if (p1.length == 2) {
        check_sum = (p1.charCodeAt(0) - 55) * 9 + (p1.charCodeAt(1) - 55) * 8;
      } else if (p1.length == 1) {
        check_sum = 324 + (p1.charCodeAt(0) - 55) * 8;
      }

      check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
      var check_digit = 11 - (check_sum % 11);
      if (check_digit == '11') {
        check_digit = 0;
      } else if (check_digit == '10') {
        check_digit = 'A';
      }
      if (check_digit == p3) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  var validator = {};

  validator['REQUIRED'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"], [name="' + fieldName + '[]"]');
    if ($field.length == 0) {
      return true;
    }
    var isValid = true;

    if ($field.attr('type') == 'checkbox') {
      if ($field.filter(':checked').length === 0) {
        var $otherField = $('[name="' + fieldName + '_other"]');
        if ($otherField.length === 0) {
          isValid = false;
        } else {
          isValid = ($otherField.val() != '') && isValid
        }
      }
    } else if ($field.attr('type') == 'radio') {
      var checkedVal = $field.filter(':checked').val();
      var name = $field.attr('name');
      if ($field.closest('.itemInput-choice').find('[name="' + name + '"]:checked').length == 0) {
        isValid = false;
      }

      var $fieldOptions = $('[name="' + fieldName + '_option[]"].' + checkedVal);
      if ($fieldOptions.length > 0) {
        $fieldOptions.each(function () {
          isValid = isValid && ($(this).val() != '');
        });
      }

    } else if ($field.val() == '') {
      isValid = false;
    }

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.required').removeClass('hide').show();
    }
    return isValid;
  });

  validator['REQUIRED_WITH_FIELD'] = (function (fieldName1, fieldName2) {
    var $field1 = (fieldName1 instanceof $) ? fieldName1 : $('[name="' + fieldName1 + '"]');
    var $field2 = (fieldName2 instanceof $) ? fieldName2 : $('[name="' + fieldName2 + '"]');
    if ($field1.length == 0 || $field2.length == 0) {
      return true;
    }
    var isEmpty = true;

    if ($field2.attr('type') == 'radio') {
      var name = $field.attr('name');
      if ($field2.closest('.itemInput-choice').find('[name="' + name + '"]:checked').length > 0) {
        isEmpty = false;
      }
    } else if ($field2.val() != '') {
      isEmpty = false;
    }

    if (!isEmpty) {
      return validator['REQUIRED'](fieldName1);
    }
    return true;
  });

  validator['REQUIRED_ONLY_STUDENT'] = (function (fieldName1, isTeacher) {
    if (!isTeacher) {
      return validator['REQUIRED'](fieldName1);
    }
    return true;
  });
  
  validator['NUMBER'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }

    var isValid = !isNaN(parseFloat($field.val())) && isFinite($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.number').removeClass('hide').show();
    }
    return isValid;
  });

  validator['NON_NUMBER'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = !(/\d+/.test($field.val()));

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.non_number').removeClass('hide').show();
    }
    return isValid;
  });

  validator['INTEGER'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[\+\-]?\d+$/.test($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.integer').removeClass('hide').show();
    }
    return isValid;
  });

  validator['POSTIVE_NUMBER'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = false;

    try {
      var num = parseFloat($field.val());
      isValid = num >= 0;
    } catch (e) {
      isValid = false;
    }

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.postive_number').removeClass('hide').show();
    }
    return isValid;
  });

  validator['NUMBER_IN_RANGE'] = (function (fieldName, min, max, inclusivity) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    if (typeof (inclusivity) == 'undefined' || inclusivity == '') {
      inclusivity = '[]';
    }

    var isValid = true;
    try {
      var num = parseFloat($field.val());
      if (inclusivity.indexOf('[') > -1) {
        isValid = isValid && (num >= min);
      } else {
        isValid = isValid && (num > min);
      }

      if (inclusivity.indexOf(']') > -1) {
        isValid = isValid && (num <= max);
      } else {
        isValid = isValid && (num < max);
      }
    } catch (e) {
      isValid = false;
    }

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      var $warning = $field.closest('.itemInput').find('.remark-warn.number_in_range');
      if (_.isEmpty($warning.data('contentTemplate'))) {
        $warning.data('contentTemplate', $warning.html());
      }

      var warning = $warning.data('contentTemplate') || '';
      warning = warning.replace(/{min}/g, min);
      warning = warning.replace(/{max}/g, max);
      $warning.html(warning);

      $warning.removeClass('hide').show();
    }
    return isValid;
  });

  validator['CHINESE'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[\u3400-\u9FBF]*$/.test($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.chinese').removeClass('hide').show();
    }
    return isValid;
  });

  validator['CHINESE_NUMBER'] = (function (fieldName) {
    var $field = $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[\u3400-\u9FBF\d ,\-]*$/.test($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.chinese').removeClass('hide').show();
    }
    return isValid;
  });

  validator['HAS_CHINESE'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = (/[\u3400-\u9FBF]+/.test($field.val()));

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.has_chinese').removeClass('hide').show();
    }
    return isValid;
  });

  validator['NON_CHINESE'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = !((validator['HAS_CHINESE'])(fieldName));
    $field.closest('.itemInput').removeClass('itemInput-warn');
    $field.closest('.itemInput').find('.remark-warn.has_chinese').addClass('hide').hide();

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.non_chinese').removeClass('hide').show();
    }
    return isValid;
  });

  validator['ENGLISH'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[a-z '.,\\/\-]*$/i.test($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.english').removeClass('hide').show();
    }
    return isValid;
  });

  validator['ENGLISH_NUMBER'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[a-z '.,\\/\-\d]*$/i.test($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.english').removeClass('hide').show();
    }
    return isValid;
  });

  validator['HKID'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');

    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[a-zA-Z][0-9]{6}\(?(a|A|[0-9])\)?$/.test($field.val());
    isValid = isValid && check_hkid($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.hkid').removeClass('hide').show();
    }
    return isValid;
  });

  validator['HKID_DUPLICATE'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '' || $field.val() == $field.data('originalValue')) {
      return true;
    }

    var $deferred = $.Deferred();
    $.get('/kis/admission_form/ajax_get_birth_cert_no.php', {
      'StudentBirthCertNo': $field.val(),
      'sus_status': $('[name="sus_status"]').val(),
      'SchoolYearID': $('[name="SchoolYearID"]').val(),
    }, function (res) {
      if (res == '0') {
        $deferred.resolve(true);
      } else {
        $field.closest('.itemInput').addClass('itemInput-warn');
        $field.closest('.itemInput').find('.remark-warn.hkid_duplicate').removeClass('hide').show();
        $deferred.reject(false);
      }
    });

    return $deferred;
  });

  validator['EMAIL'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = /^[_a-z0-9-\+]+(\.[_a-z0-9-\+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z0-9-]{2,4})$/i.test($field.val());

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.email').removeClass('hide').show();
    }
    return isValid;
  });

  validator['EQUAL'] = (function (fieldName1, fieldName2) {
    var $field1 = (fieldName1 instanceof $) ? fieldName1 : $('[name="' + fieldName1 + '"]');
    var $field2 = (fieldName2 instanceof $) ? fieldName2 : $('[name="' + fieldName2 + '"]');
    if ($field1.length == 0 || $field2.length == 0 || $field1.val() == '') {
      return true;
    }
    var isValid = $field1.val() === $field2.val();

    if (!isValid) {
      $field1.closest('.itemInput').addClass('itemInput-warn');
      $field1.closest('.itemInput').find('.remark-warn.equal').removeClass('hide').show();
    }

    return isValid;
  });

  validator['LENGTH'] = (function (fieldName, length) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '"]');
    if ($field.length == 0 || $field.val() == '') {
      return true;
    }
    var isValid = $field.val().length == length;

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      var $warning = $field.closest('.itemInput').find('.remark-warn.length');
      if (_.isEmpty($warning.data('contentTemplate'))) {
        $warning.data('contentTemplate', $warning.html());
      }

      var warning = $warning.data('contentTemplate') || '';
      warning = warning.replace(/{length}/g, length);
      $warning.html(warning);

      $warning.removeClass('hide').show();
    }
    return isValid;
  });

  validator['DATE'] = (function (fieldNameYear, fieldNameMonth, fieldNameDay, isDob) {
    var $fieldYear = (fieldNameYear instanceof $) ? fieldNameYear : $('[name="' + fieldNameYear + '"]');
    var $fieldMonth = (fieldNameMonth instanceof $) ? fieldNameMonth : $('[name="' + fieldNameMonth + '"]');
    var $fieldDay = (fieldNameDay instanceof $) ? fieldNameDay : $('[name="' + fieldNameDay + '"]');
    if ($fieldYear.length == 0 || $fieldMonth.length == 0 || $fieldDay.length == 0) {
      return true;
    }
    if ($fieldYear.val() == '' && $fieldMonth.val() == '' && $fieldDay.val() == '') {
      return true;
    }

    //// Check format START ////
    var year = parseInt($fieldYear.val());
    var month = parseInt($fieldMonth.val());
    var day = parseInt($fieldDay.val());
    var isValid = true;
    var isRangeValid = true;
    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12) {
      isValid = false;
    }

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
      monthLength[1] = 29;

    // Check the range of the day
    isValid = isValid && day > 0 && day <= monthLength[month - 1];
    //// Check format END ////

    //// Check range START ////
    var date = year + '-' + ('0' + month).substr(-2) + '-' + ('0' + day).substr(-2);
    var minDate = $fieldYear.closest('.itemInput').data('minDate');
    var maxDate = $fieldYear.closest('.itemInput').data('maxDate');

    isRangeValid = minDate <= date && date <= maxDate;
    //// Check range END ////

    if (!isValid) {
      $fieldYear.closest('.itemInput').addClass('itemInput-warn');
      $fieldYear.closest('.itemInput').find('.remark-warn.date').removeClass('hide').show();
    } else if (!isRangeValid && isDob) {
      $fieldYear.closest('.itemInput').addClass('itemInput-warn');
      $fieldYear.closest('.itemInput').find('.remark-warn.dob').removeClass('hide').show();
    } else if (!isRangeValid && !isDob) {
      $fieldYear.closest('.itemInput').addClass('itemInput-warn');
      $fieldYear.closest('.itemInput').find('.remark-warn.date_range').removeClass('hide').show();
    }

    return isValid && isRangeValid;
  })

  validator['APPLY_FOR'] = (function (fieldName, isRequired) {
    var $fields = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '[]"]');
    if ($fields.length == 0) {
      return true;
    }
    var isValid = true;
    var values = [];
    $fields.each(function () {
      if ($(this).val()) {
        values.push($(this).val());
      }
    });
    if (values.length == 0) {
      if (isRequired) {
        $fields.closest('.itemInput').addClass('itemInput-warn');
        $fields.closest('.itemInput').find('.remark-warn.required').removeClass('hide').show();
        return false;
      } else {
        return true;
      }
    } else {
      var firstEmptyIndex = $fields.length;
      var lastNonEmptyIndex = 0;
      $fields.each(function (index) {
        if ($(this).val()) {
          lastNonEmptyIndex = index;
        } else {
          firstEmptyIndex = (firstEmptyIndex < index) ? firstEmptyIndex : index;
        }
      });

      if (lastNonEmptyIndex > firstEmptyIndex) {
        $fields.closest('.itemInput').addClass('itemInput-warn');
        $fields.closest('.itemInput').find('.remark-warn.not_complete').removeClass('hide').show();
        isValid = false;
      }
    }

    if (_.uniq(values).length !== values.length) {
      $fields.closest('.itemInput').addClass('itemInput-warn');
      $fields.closest('.itemInput').find('.remark-warn.option_repeat').removeClass('hide').show();
      isValid = false;
    }
    return isValid;
  });


  validator['HKID_WITH_TYPE'] = (function (fieldName, isRequired) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '_value"]');
    if ($field.length == 0) {
      return true;
    }
    var isValid = true;

    if ($field.val() == '') {
      isValid = false;
    }

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.required').removeClass('hide').show();
    }
    return isValid;
  });
  
  validator['HKID_WITH_TYPE_FORMAT'] = (function (fieldName) {
  	var $fieldType = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '_type"]');
    var $fieldValue = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '_value"]');

    if ($fieldType.val() > 1 || $fieldValue.length == 0 || $fieldValue.val() == '') {
      return true;
    }
    var isValid = /^[a-zA-Z][0-9]{6}\(?(a|A|[0-9])\)?$/.test($fieldValue.val());
    isValid = isValid && check_hkid($fieldValue.val());

    if (!isValid) {
      $fieldValue.closest('.itemInput').addClass('itemInput-warn');
      $fieldValue.closest('.itemInput').find('.remark-warn.hkid').removeClass('hide').show();
    }
    return isValid;
  });
  
  validator['HKID_WITH_TYPE_DUPLICATE'] = (function (fieldName) {
    var $field = (fieldName instanceof $) ? fieldName : $('[name="' + fieldName + '_value"]');
    if ($field.length == 0 || $field.val() == '' || $field.val() == $field.data('originalValue')) {
      return true;
    }

    var $deferred = $.Deferred();
    $.get('/kis/admission_form/ajax_get_birth_cert_no.php', {
      'StudentBirthCertNo': $field.val(),
      'sus_status': $('[name="sus_status"]').val(),
      'SchoolYearID': $('[name="SchoolYearID"]').val(),
    }, function (res) {
      if (res == '0') {
        $deferred.resolve(true);
      } else {
        $field.closest('.itemInput').addClass('itemInput-warn');
        $field.closest('.itemInput').find('.remark-warn.hkid_duplicate').removeClass('hide').show();
        $deferred.reject(false);
      }
    });

    return $deferred;
  });

  validator['DATE_IS_FUTURE'] = (function (fieldNameYear, fieldNameMonth, fieldNameDay) {
    var $fieldYear = (fieldNameYear instanceof $) ? fieldNameYear : $('[name="' + fieldNameYear + '"]');
    var $fieldMonth = (fieldNameMonth instanceof $) ? fieldNameMonth : $('[name="' + fieldNameMonth + '"]');
    var $fieldDay = (fieldNameDay instanceof $) ? fieldNameDay : $('[name="' + fieldNameDay + '"]');
    if ($fieldYear.length == 0 || $fieldMonth.length == 0 || $fieldDay.length == 0) {
      return true;
    }
    if ($fieldYear.val() == '' && $fieldMonth.val() == '' && $fieldDay.val() == '') {
      return true;
    }

    var isValid = true;
    var date = $fieldYear.val() + '-' + $fieldMonth.val() + '-' + $fieldDay.val();
    date = new Date(date);

    isValid = (new Date()) < date;

    if (!isValid) {
      $fieldYear.closest('.itemInput').addClass('itemInput-warn');
      $fieldYear.closest('.itemInput').find('.remark-warn.date_range').removeClass('hide').show();
    }
    return isValid;
  });

  validator['CUSTOM'] = validator['CUSTOM'] || {};

  window.validator = window.validator || validator;
});
