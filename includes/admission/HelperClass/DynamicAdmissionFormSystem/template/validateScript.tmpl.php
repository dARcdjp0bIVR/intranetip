<script>
<?php
global $lac, $libkis_admission;
include_once(__DIR__."/js/defaultValidateFunction.js");
include_once("{$intranet_root}/includes/admission/{$kis_admission_school}/template/js/validateScript.tmpl.js");
?>


$(function(){
	'use strict';

	window.validateFunc = window.validateFunc || {};
	window.validateFunc['pagePersonalInfo'] = window.validateFunc['pagePersonalInfo'] || [];
	window.check_input_info = window.check_input_info || (function(pageId){
        var $deferredArr = [];

        $('.remark-warn').each(function(){
			this.style.setProperty('display', 'none', 'important');
		});
		window.focusElement = null;

		_.each(validateFunc, function(funcs, pId){
			if(_.isUndefined(pageId) || (pageId == pId)){
				_.each(funcs, function(func){
                    $deferredArr.push(func(pageId));
				});
			}
		});

        var $deferred = $.Deferred();
        $.when.apply($, $deferredArr).always(function(){
            var isValid = true;

            try {
                _.each(arguments, function (res) {
                    if (typeof (res) == 'boolean') {
                        isValid = isValid && res;
                    } else {
                        isValid = isValid && (res.state() == 'resolved');
                    }
                });
            }catch(e){
                isValid = false;
            }

            if(isValid){
                $deferred.resolve();
            }else{
                $deferred.reject();
            }
        });

        return $deferred;
	});

<?php
foreach($fields as $field):
    $name = "field_{$field['FieldID']}";
    $validators = $field['ValidationArr'];

    if($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DATE){
        $validators[] = \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_DATE;
        $name = "field_{$field['FieldID']}_year";
    }elseif($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR){
        $validators[] = \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_APPLY_FOR;
        $name = "field_{$field['FieldID']}";
    }elseif($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE){
        $validators[] = \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_HKID_WITH_TYPE;
        $name = "field_{$field['FieldID']}";
    }

    foreach($validators as $validator):
        $args = '';
        $functionName = "window.validator['{$validator}']";

        switch($validator){
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_NUMBER_IN_RANGE:
                $args .= ", {$field['ExtraArr']['validateMin']}";
                $args .= ", {$field['ExtraArr']['validateMax']}";
                if($field['ExtraArr']['validateInclusively']){
                    $args .= ", '{$field['ExtraArr']['validateInclusively']}'";
                }
                break;

            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_DATE:
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_DATE_IS_FUTURE:
                $args .= ", 'field_{$field['FieldID']}_month'";
                $args .= ", 'field_{$field['FieldID']}_day'";
                $args .= ", '{$field['ExtraArr']['isDob']}'";
                break;

            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_WITH_FIELD:
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_EQUAL:
                $validateField = $field['ExtraArr']['validateField'];
                $field2Name = '';
                foreach($fields as $field2){
                    if($validateField == $field2['ExtraArr']['field']){
                        $field2Name = "field_{$field2['FieldID']}";
                    }
                }

                $args .= ", '{$field2Name}'";
                break;

            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_LENGTH:
                $args .= ", {$field['ExtraArr']['validateLength']}";
                break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT:
                $args .= ", ".($lac && $lac->isInternalUse($_GET['token']) || $libkis_admission && $libkis_admission->isInternalUse($_GET['token'])?1:0);
                break;
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_HKID_WITH_TYPE:
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_APPLY_FOR:
                if(in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validators)){
                    $args .= ", 1";
                }
                break;

            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_CUSTOM:
                $args .= ", '{$name}', ";
                $args .= json_encode($field['ExtraArr'], JSON_UNESCAPED_UNICODE);
                $functionName = "window.validator['{$validator}']['{$field['ExtraArr']['validateFunc']}']";
                break;

            default:
                $args = '';
        }
?>

    	if(typeof(<?=$functionName?>) == 'function'){

			var isInValidateFuncArr = false;
			$.each(window.validateFunc['pagePersonalInfo'], function(i, fn){
				if(fn.name == '<?="{$name}_{$validator}"?>'){
					isInValidateFuncArr = true;
				}
			});

    		if(!isInValidateFuncArr){
                window.validateFunc['pagePersonalInfo'].push(function <?="{$name}_{$validator}"?>(){
                    var isValid = <?=$functionName?>('<?=$name?>' <?=$args?>);

                	<?php if($plugin['eAdmission_devMode']): ?>
                    	if(!isValid){
                        	console.error('<?="{$name}_{$validator}"?>');

                        	<?php if($args): ?>
                        		console.error(<?=json_encode($args, JSON_UNESCAPED_UNICODE)?>);
                            <?php endif; ?>
                        }
                    <?php endif; ?>

                    return isValid
                });
    		}
    	}else{
        	console.error('missing implement <?=addslashes($functionName)?>()');
    	}

<?php
    endforeach;
endforeach;
?>

});
</script>
