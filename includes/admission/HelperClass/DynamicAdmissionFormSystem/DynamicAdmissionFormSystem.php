<?php
/**
 * Change Log:
 * 2019-06-04 Pun
 *  - File Created
 */

namespace AdmissionSystem;

global $intranet_root;
include_once("{$intranet_root}/includes/admission/libadmission_cust_base.php");

class DynamicAdmissionFormSystem extends \admission_cust_base
{
    const GROUP_TYPE_NORMAL = 'NORMAL';
    const GROUP_TYPE_STU_INFO = 'STU_INFO';
    const GROUP_TYPE_CUSTOM = 'CUSTOM';

    const FIELD_TYPE_TEXT = 'TEXT';
    const FIELD_TYPE_TEXTAREA = 'TEXTAREA';
    const FIELD_TYPE_DATE = 'DATE';
    const FIELD_TYPE_RADIO = 'RADIO';
    const FIELD_TYPE_RADIO_EXTRA = 'RADIO_EXTRA';
    const FIELD_TYPE_CHECKBOX = 'CHECKBOX';
    const FIELD_TYPE_SELECT = 'SELECT';
    const FIELD_TYPE_HKID_TEXT = 'HKID_TEXT';
    const FIELD_TYPE_HKID_WITH_TYPE = 'HKID_WITH_TYPE';
    const FIELD_TYPE_APPLY_FOR = 'APPLY_FOR';
    const FIELD_TYPE_YES_NO_EXTRA = 'YES_NO_EXTRA';
    const FIELD_TYPE_CUSTOM_FIELD = 'CUSTOM_FIELD';
    const FIELD_TYPE_CUSTOM_ROW = 'CUSTOM_ROW';
    const FIELD_TYPE_DUMMY = 'DUMMY';

    const FIELD_WIDTH_FULL = 'FULL';
    const FIELD_WIDTH_HALF = 'HALF';
    const FIELD_WIDTH_ONE_THIRD = 'ONE_THIRD';
    const FIELD_WIDTH_ONE_FOURTH = 'ONE_FOURTH';

    const FIELD_GROUP_TYPE_SINGLE = 'SINGLE';
    const FIELD_GROUP_TYPE_TWO_GROUP = 'TWO_GROUP';
    const FIELD_GROUP_TYPE_THREE_GROUP = 'THREE_GROUP';

    const FIELD_VALIDATION_TYPE_REQUIRED = 'REQUIRED';
    const FIELD_VALIDATION_TYPE_REQUIRED_WITH_FIELD = 'REQUIRED_WITH_FIELD';
    const FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT = 'REQUIRED_ONLY_STUDENT';
    const FIELD_VALIDATION_TYPE_NON_NUMBER = 'NON_NUMBER';
    const FIELD_VALIDATION_TYPE_NUMBER = 'NUMBER';
    const FIELD_VALIDATION_TYPE_INTEGER = 'INTEGER';
    const FIELD_VALIDATION_TYPE_POSTIVE_NUMBER = 'POSTIVE_NUMBER';
    const FIELD_VALIDATION_TYPE_NUMBER_IN_RANGE = 'NUMBER_IN_RANGE';
    const FIELD_VALIDATION_TYPE_HAS_CHINESE = 'HAS_CHINESE';
    const FIELD_VALIDATION_TYPE_NON_CHINESE = 'NON_CHINESE';
    const FIELD_VALIDATION_TYPE_CHINESE = 'CHINESE';
    const FIELD_VALIDATION_TYPE_CHINESE_OR_NUMBER = 'CHINESE_NUMBER';
    const FIELD_VALIDATION_TYPE_ENGLISH = 'ENGLISH';
    const FIELD_VALIDATION_TYPE_ENGLISH_OR_NUMBER = 'ENGLISH_NUMBER';
    const FIELD_VALIDATION_TYPE_DATE = 'DATE';
    const FIELD_VALIDATION_TYPE_HKID = 'HKID';
    const FIELD_VALIDATION_TYPE_HKID_DUPLICATE = 'HKID_DUPLICATE';
    const FIELD_VALIDATION_TYPE_HKID_WITH_TYPE_DUPLICATE = 'HKID_WITH_TYPE_DUPLICATE';
    const FIELD_VALIDATION_TYPE_HKID_WITH_TYPE_FORMAT = 'HKID_WITH_TYPE_FORMAT';
    const FIELD_VALIDATION_TYPE_EMAIL = 'EMAIL';
    const FIELD_VALIDATION_TYPE_EQUAL = 'EQUAL';
    const FIELD_VALIDATION_TYPE_LENGTH = 'LENGTH';
    const FIELD_VALIDATION_TYPE_APPLY_FOR = 'APPLY_FOR';
    const FIELD_VALIDATION_TYPE_HKID_WITH_TYPE = 'HKID_WITH_TYPE';
    const FIELD_VALIDATION_TYPE_DATE_IS_FUTURE = 'DATE_IS_FUTURE';
    const FIELD_VALIDATION_TYPE_CUSTOM = 'CUSTOM';

    public function __construct($schoolYearID)
    {
        parent::__construct();
        $this->libdb();
        $this->schoolYearID = $schoolYearID;
    }

    public static function listGroupTypes()
    {
        return array(
            array(self::GROUP_TYPE_STU_INFO, 'Student Info'),
            array(self::GROUP_TYPE_NORMAL, 'Normal'),
            array(self::GROUP_TYPE_CUSTOM, 'Custom Page {$pageType}'),
        );
    }

    public static function listFieldTypes()
    {
        return array(
            array(self::FIELD_TYPE_TEXT, 'Single-line Text'),
            array(self::FIELD_TYPE_TEXTAREA, 'Multiple-line Text'),
            array(self::FIELD_TYPE_DATE, 'Date'),
            array(self::FIELD_TYPE_RADIO, 'Radio'),
            array(self::FIELD_TYPE_RADIO_EXTRA, 'Radio Extra'),
            array(self::FIELD_TYPE_CHECKBOX, 'Checkbox'),
            array(self::FIELD_TYPE_SELECT, 'Select'),
            array(self::FIELD_TYPE_HKID_TEXT, 'HKID (Text-only)'),
            array(self::FIELD_TYPE_HKID_WITH_TYPE, 'HKID (With type)'),
            array(self::FIELD_TYPE_APPLY_FOR, 'Apply For'),
            array(self::FIELD_TYPE_YES_NO_EXTRA, 'Yes/No Extra'),
            array(self::FIELD_TYPE_CUSTOM_FIELD, 'Custom Field {$fieldType}'),
            array(self::FIELD_TYPE_CUSTOM_ROW, 'Custom Row {$fieldType}'),
            array(self::FIELD_TYPE_DUMMY, 'Dummy'),
        );
    }

    public static function listFieldWidths()
    {
        return array(
            array(self::FIELD_WIDTH_FULL, 'Full width'),
            array(self::FIELD_WIDTH_HALF, 'Half width'),
//             array(self::FIELD_WIDTH_ONE_THIRD, 'One-third width'),
//             array(self::FIELD_WIDTH_ONE_FOURTH, 'One-fourth width'),
        );
    }

    public static function listFieldGroupTypes()
    {
        return array(
            array(self::FIELD_GROUP_TYPE_SINGLE, 'Single'),
            array(self::FIELD_GROUP_TYPE_TWO_GROUP, 'Two-group {$group}'),
            array(self::FIELD_GROUP_TYPE_THREE_GROUP, 'Three-group {$group}'),
        );
    }

    public static function listFieldValidationTypes()
    {
        return array(
            array(self::FIELD_VALIDATION_TYPE_REQUIRED, 'Required'),
            array(self::FIELD_VALIDATION_TYPE_REQUIRED_WITH_FIELD, 'Required if ${validateField} not empty'),
            array(self::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, 'Required only student'),
            array(self::FIELD_VALIDATION_TYPE_INTEGER, 'Integer only'),
            array(self::FIELD_VALIDATION_TYPE_NON_NUMBER, 'Non-Number'),
            array(self::FIELD_VALIDATION_TYPE_NUMBER, 'Number only'),
            array(self::FIELD_VALIDATION_TYPE_POSTIVE_NUMBER, 'Postive number only'),
            array(self::FIELD_VALIDATION_TYPE_NUMBER_IN_RANGE, 'Number between [$validateMin - $validateMax] (with $validateInclusively)'),
            array(self::FIELD_VALIDATION_TYPE_HAS_CHINESE, 'Has-Chinese'),
            array(self::FIELD_VALIDATION_TYPE_NON_CHINESE, 'Non-Chinese'),
            array(self::FIELD_VALIDATION_TYPE_CHINESE, 'Chinese only'),
            array(self::FIELD_VALIDATION_TYPE_CHINESE_OR_NUMBER, 'Chinese or number'),
            array(self::FIELD_VALIDATION_TYPE_ENGLISH, 'English only'),
            array(self::FIELD_VALIDATION_TYPE_ENGLISH_OR_NUMBER, 'English or number'),
            array(self::FIELD_VALIDATION_TYPE_HKID, 'HKID'),
            array(self::FIELD_VALIDATION_TYPE_HKID_DUPLICATE, 'HKID unique'),
            array(self::FIELD_VALIDATION_TYPE_HKID_WITH_TYPE_DUPLICATE, 'HKID with type unique'),
            array(self::FIELD_VALIDATION_TYPE_HKID_WITH_TYPE_FORMAT, 'HKID with type'),
            array(self::FIELD_VALIDATION_TYPE_EMAIL, 'EMAIL'),
            array(self::FIELD_VALIDATION_TYPE_EQUAL, 'Equal to {$validateField}'),
            array(self::FIELD_VALIDATION_TYPE_LENGTH, 'Length equal {$validateLength}'),
            array(self::FIELD_VALIDATION_TYPE_DATE_IS_FUTURE, 'Date is future'),
            array(self::FIELD_VALIDATION_TYPE_CUSTOM, 'Custom {$validateFunc}'),
        );
    }

    public function deleteAllDataByAcademicYear($academicYearId)
    {
        $sql = "DELETE FROM ADMISSION_FORM_GROUP WHERE SchoolYearID='{$academicYearId}'";
        $result = $this->db_db_query($sql);

        $sql = "DELETE FROM ADMISSION_FORM_FIELD WHERE SchoolYearID='{$academicYearId}'";
        $result = $result && $this->db_db_query($sql);

        return $result;
    }

    public function getCurrentAcademicYearGroups()
    {
        $sql = "SELECT * FROM ADMISSION_FORM_GROUP WHERE SchoolYearID='{$this->schoolYearID}' ORDER BY Sequence";
        $rs = $this->returnResultSet($sql);
        return $rs;
    }

    public function getLastActiveAcademicYearGroups()
    {
        $sql = "SELECT DISTINCT
            AFG.SchoolYearID
        FROM
            ADMISSION_FORM_GROUP AFG
        INNER JOIN
            ACADEMIC_YEAR_TERM AYT
        ON
            AFG.SchoolYearID = AYT.AcademicYearID
        WHERE
            AFG.Status = '1'
        ORDER BY
            AYT.TermStart DESC
        LIMIT 1";
        $rs = $this->returnVector($sql);
        $lastActiveSchoolYearId = $rs[0];

        $sql = "SELECT * FROM ADMISSION_FORM_GROUP WHERE SchoolYearID='{$lastActiveSchoolYearId}' AND Status = '1' ORDER BY Sequence";
        $rs = $this->returnResultSet($sql);

        for ($i = 0, $iMax = count($rs); $i < $iMax; $i++) {
            $rs[$i]['OtherAttributeArr'] = $this->parseFieldExtra($rs[$i]['OtherAttribute'], true);
        }
        return $rs;
    }

    public function getGroupByGroupId($groupId, $status = null)
    {
        if ($status !== null) {
            $cond = " AND Status = '{$status}'";
        }
        $sql = "SELECT * FROM ADMISSION_FORM_GROUP WHERE GroupID='{$groupId}' {$cond} ORDER BY Sequence";
        $rs = $this->returnResultSet($sql);

        for ($i = 0, $iMax = count($rs); $i < $iMax; $i++) {
            $rs[$i]['OtherAttributeArr'] = $this->parseFieldExtra($rs[$i]['OtherAttribute'], true);
        }
        return current($rs);
    }

    public function upsertGroup($data)
    {
        $result = true;

        #### INSERT dummy record if not exists START ####
        $sql = "SELECT * FROM ADMISSION_FORM_GROUP WHERE GroupID='{$data['GroupID']}'";
        $rs = $this->returnResultSet($sql);

        if (count($rs) == 0) {
            ## Get sequence START ##
            $sql = "SELECT MAX(Sequence) FROM ADMISSION_FORM_GROUP WHERE SchoolYearID='{$this->schoolYearID}'";
            $rs = $this->returnVector($sql);
            $sequence = ($rs) ? $rs[0] + 1 : 1;
            ## Get sequence END ##

            $sql = "INSERT INTO
                ADMISSION_FORM_GROUP
            (
                SchoolYearID,
                Sequence,
                DateInput,
                InputBy
            ) VALUES (
                '{$this->schoolYearID}',
                '{$sequence}',
                NOW(),
                '{$_SESSION['UserID']}'
            )";
            $result = $this->db_db_query($sql);
            $data['GroupID'] = $this->db_insert_id();
            $data['Sequence'] = $sequence;
        }
        #### INSERT dummy record if not exists END ####

        #### Update data START ####
        $data['OtherAttribute'] = $this->Get_Safe_Sql_Query($data['OtherAttribute']);
        $sql = "UPDATE
            ADMISSION_FORM_GROUP
        SET
            TitleB5 = '{$data['TitleB5']}',
            TitleEn = '{$data['TitleEn']}',
            TitleGb = '{$data['TitleGb']}',
            Type = '{$data['Type']}',
            Sequence = '{$data['Sequence']}',
            Status = '{$data['Status']}',
            OtherAttribute = '{$data['OtherAttribute']}',
            Remark = '{$data['Remark']}',
            DateModified = NOW(),
            ModifiedBy = '{$_SESSION['UserID']}'
        WHERE
            GroupID='{$data['GroupID']}'";
        $result = $result && $this->db_db_query($sql);
        #### Update data END ####

        return $data['GroupID'];
    }

    public function deleteGroup($groupId)
    {
        $sql = "DELETE FROM ADMISSION_FORM_GROUP WHERE GroupID='{$groupId}'";
        $result = $this->db_db_query($sql);
        return $result;
    }


    public function getCurrentAcademicYearFields()
    {
        $sql = "SELECT * FROM ADMISSION_FORM_FIELD WHERE SchoolYearID='{$this->schoolYearID}' ORDER BY Sequence";
        $rs = $this->returnResultSet($sql);
        return $rs;
    }
	
	function getBasicSettings($schoolYearID = '', $SettingNameAry = array())
    {
        $schoolYearID = $schoolYearID ? $schoolYearID : $this->schoolYearID;
        $sql = "
            SELECT
                SettingName,
                SettingValue
            FROM
                ADMISSION_SETTING
            WHERE
                SchoolYearID = '" . $schoolYearID . "'
        ";
        if (sizeof($SettingNameAry) > 0) {
            $sql .= " AND
                        SettingName in ('" . implode("','", $SettingNameAry) . "')";
        }
        $setting = $this->returnArray($sql);
        for ($i = 0; $i < sizeof($setting); $i ++) {
            $Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
        }
        return $Return;
    }
    
    public function getFieldsByGroupId($groupId, $status = null)
    {
        if ($status !== null) {
            $cond = " AND Status = '{$status}'";
        }
        $sql = "SELECT * FROM ADMISSION_FORM_FIELD WHERE GroupID='{$groupId}' {$cond} ORDER BY Sequence";
        $rs = $this->returnResultSet($sql);

        for ($i = 0, $iMax = count($rs); $i < $iMax; $i++) {
        	if(strtolower($rs[$i]['TitleEn']) == 'remarks'){
				$basicSettings = $this->getBasicSettings(99999, array (
					'remarksnamechi',
					'remarksnameeng'
				));
				$rs[$i]['TitleEn'] = $basicSettings['remarksnameeng']?$basicSettings['remarksnameeng']:$rs[$i]['TitleEn'];
				$rs[$i]['TitleB5'] = $basicSettings['remarksnamechi']?$basicSettings['remarksnamechi']:$rs[$i]['TitleB5'];
			}
            $rs[$i]['OtherAttributeArr'] = json_decode(str_replace("\r\n", '\r\n', $rs[$i]['OtherAttribute']), true);
            $rs[$i]['ExtraArr'] = $this->parseFieldExtra($rs[$i]['OtherAttributeArr']['Extra']);
            $rs[$i]['ValidationArr'] = (array)json_decode(str_replace("\r\n", '\r\n', $rs[$i]['Validation']), true);
        }
        return $rs;
    }

    public function getLastActiveAcademicYearFields()
    {
        $sql = "SELECT DISTINCT
            AFG.SchoolYearID
        FROM
            ADMISSION_FORM_GROUP AFG
        INNER JOIN
            ACADEMIC_YEAR_TERM AYT
        ON
            AFG.SchoolYearID = AYT.AcademicYearID
        WHERE
            AFG.Status = '1'
        ORDER BY
            AYT.TermStart DESC
        LIMIT 1";
        $rs = $this->returnVector($sql);
        $lastActiveSchoolYearId = $rs[0];

        $sql = "SELECT
            AFF.*
        FROM
            ADMISSION_FORM_FIELD AFF
        INNER JOIN
            ADMISSION_FORM_GROUP AFG
        USING
            (GroupID)
        WHERE
            AFF.SchoolYearID='{$lastActiveSchoolYearId}'
        AND
            AFF.Status='1'
        ORDER BY
            AFG.Sequence,
            AFF.Sequence
        ";
        $rs = $this->returnResultSet($sql);

        for ($i = 0, $iMax = count($rs); $i < $iMax; $i++) {
            $rs[$i]['OtherAttributeArr'] = json_decode(str_replace("\r\n", '\r\n', $rs[$i]['OtherAttribute']), true);
            $rs[$i]['ExtraArr'] = $this->parseFieldExtra($rs[$i]['OtherAttributeArr']['Extra']);
            $rs[$i]['ValidationArr'] = (array)json_decode(str_replace("\r\n", '\r\n', $rs[$i]['Validation']), true);
        }
        return $rs;
    }

    public function getActiveFieldsByGroupId($groupId)
    {
        return $this->getFieldsByGroupId($groupId, 1);
    }

    public function grouppedFieldList($fields)
    {

        ######## Group rows for width START ########
        $rows = array();
        while (count($fields)) {
            $row = array();
            $field = array_shift($fields);

            if ($field['OtherAttributeArr']['Width'] == self::FIELD_WIDTH_FULL) {
                $row[] = $field;
            } elseif ($field['OtherAttributeArr']['Width'] == self::FIELD_WIDTH_HALF) {
                $row[] = $field;
                $field = array_shift($fields);
                $row[] = $field;
            } elseif ($field['OtherAttributeArr']['Width'] == self::FIELD_WIDTH_ONE_THIRD) {
                $row[] = $field;
                $field = array_shift($fields);
                $row[] = $field;
                $field = array_shift($fields);
                $row[] = $field;
            } elseif ($field['OtherAttributeArr']['Width'] == self::FIELD_WIDTH_ONE_FOURTH) {
                $row[] = $field;
                $field = array_shift($fields);
                $row[] = $field;
                $field = array_shift($fields);
                $row[] = $field;
                $field = array_shift($fields);
                $row[] = $field;
            }

            $rows[] = $row;
        }
        ######## Group rows for width END ########


        ######## Group rows for column START ########
        $fieldList = array();
        foreach ($rows as $row) {
            $firstField = $row[0];
            if ($firstField['OtherAttributeArr']['Groupped'] == self::FIELD_GROUP_TYPE_SINGLE) {
                $name = "field_{$firstField['FieldID']}";
            } else {
                $name = "group_{$firstField['ExtraArr']['group']}";
            }
            $fieldList[$name][] = $row;
        }
        ######## Group rows for column END ########

        return $fieldList;
    }

    public function parseFieldExtra($OtherAttribute)
    {
    	global $admission_cfg;
        if (empty($OtherAttribute)) {
            return array();
        }
        $extraArr = explode("\r\n", $OtherAttribute);

        $extraResult = array();
        foreach ($extraArr as $extraInfo) {
            $extraInfo = str_replace('\\=', '|EQUAL|', stripslashes($extraInfo));

            $extra = explode('=', $extraInfo);
            $name = array_shift($extra);
            $extra = str_replace('|EQUAL|', '=', $extra);

            for($i=0,$iMax=count($extra);$i<$iMax;$i++){
            	if($extra[$i][0] === '$') {
		            eval( '$extra[' . $i . '] = ' . $extra[ $i ] . ';' );
		            if(is_array($extra[$i])){
		            	for($j=0,$jMax=count($extra[$i]);$j<$jMax;$j++) {
				            $extra[ $i ][$j] = explode( '=', $extra[ $i ][$j] );
			            }
		            }
	            }
            }

            if (preg_match('/\[.*\]$/', $name)) {
                $name = str_replace(array('[', ']'), '', $name);
                $extraResult[$name][] = $extra;
            } else {
                if (count($extra) == 1) {
                    $extraResult[$name] = $extra[0];
                } else {
                    $extraResult[$name] = $extra;
                }
            }
        }
        return $extraResult;
    }

    public function upsertField($data)
    {
        $result = true;

        #### INSERT dummy record if not exists START ####
        $sql = "SELECT * FROM ADMISSION_FORM_FIELD WHERE FieldID='{$data['FieldID']}'";
        $rs = $this->returnResultSet($sql);

        if (count($rs) == 0) {
            ## Get sequence START ##
            $sql = "SELECT MAX(Sequence) FROM ADMISSION_FORM_FIELD WHERE SchoolYearID='{$this->schoolYearID}'";
            $rs = $this->returnVector($sql);
            $sequence = ($rs) ? $rs[0] + 1 : 1;
            ## Get sequence END ##

            $sql = "INSERT INTO
                ADMISSION_FORM_FIELD
            (
                SchoolYearID,
                GroupID,
                Sequence,
                DateInput,
                InputBy
            ) VALUES (
                '{$this->schoolYearID}',
                '{$data['GroupID']}',
                '{$sequence}',
                NOW(),
                '{$_SESSION['UserID']}'
            )";
            $result = $this->db_db_query($sql);
            $data['FieldID'] = $this->db_insert_id();
            $data['Sequence'] = $sequence;
        }
        #### INSERT dummy record if not exists END ####

        #### Update data START ####
        $data['OtherAttribute'] = $this->Get_Safe_Sql_Query($data['OtherAttribute']);
        $sql = "UPDATE
            ADMISSION_FORM_FIELD
        SET
            GroupID = '{$data['GroupID']}',
            TitleB5 = '{$data['TitleB5']}',
            TitleEn = '{$data['TitleEn']}',
            TitleGb = '{$data['TitleGb']}',
            LabelB5 = '{$data['LabelB5']}',
            LabelEn = '{$data['LabelEn']}',
            LabelGb = '{$data['LabelGb']}',
            Type = '{$data['Type']}',
            OtherAttribute = '{$data['OtherAttribute']}',
            Validation = '{$data['Validation']}',
            Sequence = '{$data['Sequence']}',
            Status = '{$data['Status']}',
            Remark = '{$data['Remark']}',
            DateModified = NOW(),
            ModifiedBy = '{$_SESSION['UserID']}'
        WHERE
            FieldID='{$data['FieldID']}'";
        $result = $result && $this->db_db_query($sql);
        #### Update data END ####

        return $result;
    }

    public function copyField($fieldId)
    {
        $sql = "SELECT * FROM ADMISSION_FORM_FIELD WHERE FieldID='{$fieldId}'";
        $rs = $this->returnResultSet($sql);

        if (count($rs) == 0) {
            return false;
        }

        $data = $rs[0];
        $data['FieldID'] = 0;
        $result = $this->upsertField($data);

        return $result;
    }

    public function reorderField($fieldIds)
    {
        $fieldIdSql = implode("','", (array)$fieldIds);
        $sql = "SELECT Sequence FROM ADMISSION_FORM_FIELD WHERE FieldID IN ('{$fieldIdSql}') ORDER BY Sequence";
        $sequenceArr = $this->returnVector($sql);

        $result = true;
        foreach ((array)$fieldIds as $index => $fieldId) {
            $sql = "UPDATE ADMISSION_FORM_FIELD SET Sequence='{$sequenceArr[$index]}' WHERE FieldID='{$fieldId}'";
            $result = $result && $this->db_db_query($sql);
        }

        return $result;
    }

    public function deleteField($fieldId)
    {
        $sql = "DELETE FROM ADMISSION_FORM_FIELD WHERE FieldID='{$fieldId}'";
        $result = $this->db_db_query($sql);
        return $result;
    }


    public function getApplicationDataByGroupId($groupId, $ApplicationID)
    {
        if ($groupId == 0 || $ApplicationID == null) {
            return array();
        }

        $fields = $this->getFieldsByGroupId($groupId, 1);


        $relatedDbTables = array();
        foreach ($fields as $field) {
            $relatedDbTables[] = $field['OtherAttributeArr']['DbTableName'];
        }
        $relatedDbTables = array_filter(array_unique($relatedDbTables));


        $applicationData = array();
        foreach ($relatedDbTables as $relatedDbTable) {
            $sql = "SELECT * FROM {$relatedDbTable} WHERE ApplicationID='{$ApplicationID}'";
            $rs = $this->returnResultSet($sql);

            if (count($rs) == 1) {
                $applicationData[$relatedDbTable] = $rs[0];
            } else {
                $applicationData[$relatedDbTable] = $rs;
            }
        }

        return $applicationData;
    }

    public function getDataForDB($field, $Data){
    	$isValid = false;
	    $value = $Data["field_{$field['FieldID']}"];

		$isValid = $isValid || (isset($Data["field_{$field['FieldID']}"]));

	    switch ($field['Type']) {
		    /** @noinspection PhpMissingBreakStatementInspection */
		    case DynamicAdmissionFormSystem::FIELD_TYPE_CHECKBOX:
			    $value = implode(', ', (array)$value);

			    if($field['ExtraArr']['hasOther']){
			        $isValid = $isValid || (isset($Data["field_{$field['FieldID']}_other"]));
				    $value .= ", {$Data["field_{$field['FieldID']}_other"]}";
			    }
			    $value = trim(intranet_htmlspecialchars($value), ', ');
		    case DynamicAdmissionFormSystem::FIELD_TYPE_TEXT:
		    case DynamicAdmissionFormSystem::FIELD_TYPE_TEXTAREA:
		    case DynamicAdmissionFormSystem::FIELD_TYPE_RADIO:
		    case DynamicAdmissionFormSystem::FIELD_TYPE_RADIO_EXTRA:
		    case DynamicAdmissionFormSystem::FIELD_TYPE_SELECT:
		    case DynamicAdmissionFormSystem::FIELD_TYPE_HKID_TEXT:
		    case DynamicAdmissionFormSystem::FIELD_TYPE_YES_NO_EXTRA:
			    $value = intranet_htmlspecialchars($value);
			    break;
		    case DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE:
			    $isValid = $isValid || (isset($Data["field_{$field['FieldID']}_type"]));
			    $isValid = $isValid || (isset($Data["field_{$field['FieldID']}_value"]));
			    $type = intranet_htmlspecialchars($Data["field_{$field['FieldID']}_type"]);
			    $hkid = intranet_htmlspecialchars($Data["field_{$field['FieldID']}_value"]);
			    $value = "{$type};{$hkid}";
			    break;
		    case DynamicAdmissionFormSystem::FIELD_TYPE_DATE:
			    $isValid = $isValid || (isset($Data["field_{$field['FieldID']}_year"]));
			    $isValid = $isValid || (isset($Data["field_{$field['FieldID']}_month"]));
			    $isValid = $isValid || (isset($Data["field_{$field['FieldID']}_day"]));
			    $year = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_year"]), 2, '0', STR_PAD_LEFT);
			    $month = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_month"]), 2, '0', STR_PAD_LEFT);
			    $day = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_day"]), 2, '0', STR_PAD_LEFT);
			    $value = "{$year}-{$month}-{$day}";
			    break;
	    }
	    if(!$isValid){
	    	return null;
	    }
    	return $value;
    }

}// End Class
