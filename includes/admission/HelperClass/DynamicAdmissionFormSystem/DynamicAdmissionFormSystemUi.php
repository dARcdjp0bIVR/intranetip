<?php
/**
 * Change Log:
 * 2019-06-04 Pun
 *  - File Created
 */
namespace AdmissionSystem;

global $intranet_root;
include_once ($intranet_root . "/includes/admission/libadmission_ui_cust_base.php");

class DynamicAdmissionFormSystemUi extends \admission_ui_cust_base
{

    public function __construct($schoolYearID){
        parent::__construct();
        $this->schoolYearID = $schoolYearID;
        $this->dafs = new DynamicAdmissionFormSystem($schoolYearID);
    }

    public function generateApplicationFormPersonalInfo($ApplicationID=0){
        $groups = $this->dafs->getLastActiveAcademicYearGroups();

        $html = '';
        foreach($groups as $group){
            $html .= $this->generateGroupHtml($group, $ApplicationID);
        }

        return $html;
    }

    protected function generateGroupHtml($group, $ApplicationID=null){
        global $applicationData, $isEdit, $classLevel, $intranet_root, $kis_admission_school, $LangB5, $LangEn;

        $fields = $this->dafs->getActiveFieldsByGroupId($group['GroupID']);
        $applicationData = $this->dafs->getApplicationDataByGroupId($group['GroupID'], $ApplicationID);
        $isEdit = $ApplicationID != '';

        #### Get cust group START ####
        $pageType = $group['OtherAttributeArr']['pageType'];
	    $custFilePath = '';
	    if($group['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::GROUP_TYPE_CUSTOM){
		    $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/admissionForm/{$group['Type']}_{$group['OtherAttributeArr']['pageType']}.tmpl.php";

		    if(!file_exists($custFilePath)){
			    $custFilePath = '';
		    }
	    }
        #### Get cust group END ####

        #### Load template START ####
        @ob_start();
        include(__DIR__.'/template/group.tmpl.php');
        $html = ob_get_clean();
        #### Load template END ####

        $html .= $this->generateValidateJs($fields);

        return $html;
    }

    protected function generateCustomRowHtml($grouppedField){
        global $intranet_root, $kis_admission_school, $LangB5, $LangEn;
        $firstField = $grouppedField[0][0];

        #### Get path START ####
        $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/admissionForm/field/{$firstField['Type']}_{$firstField['ExtraArr']['fieldType']}.tmpl.php";
        #### Get path END ####

        #### Load template START ####
        @ob_start();
        if(file_exists($custFilePath)){
            include($custFilePath);
        }

        $html = ob_get_clean();
        #### Load template END ####

        return $html;
    }

    protected function generateFieldHtml($field){
        global $intranet_root, $kis_admission_school, $LangB5, $LangEn, $applicationData, $IsUpdate, $classLevel;

        #### Get data START ####
        $dbTableName = $field['OtherAttributeArr']['DbTableName'];
        $_dbFieldName = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
        $dbFieldName = $_dbFieldName[0];
        $value = '';

	    $data = $applicationData[$dbTableName];
	    if($dbTableName === 'ADMISSION_CUST_INFO'){
		    if(isset($field['ExtraArr']['code'])) {
			    $_valueArr = array();
			    foreach ( (array)$data as $d ) {
				    if ( $d['Code'] === $field['ExtraArr']['code'] ) {
					    $_valueArr[ (int) ( $d['Position'] ) ] = $d['Value'];
				    }
			    }

			    if ( count( $_valueArr ) > 1 ) {
				    $value = $_valueArr;
			    } else {
				    $value = current( $_valueArr );
			    }
		    }elseif(isset($field['ExtraArr']['sqlWhere'])){
			    $sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
			    $sqlWhereField = $sqlWhere[0];
			    $sqlWhereValue = trim($sqlWhere[1], " '");

			    foreach((array)$applicationData[$dbTableName] as $v){
				    if($v[$sqlWhereField] == $sqlWhereValue){
					    $value = $v['Value'];
					    break;
				    }
			    }
		    }
	    }elseif(!isset($data[$dbFieldName]) && isset($field['ExtraArr']['sqlWhere']) && !isset($field['ExtraArr']['code'])){
		    $sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
		    $sqlWhereField = $sqlWhere[0];
		    $sqlWhereValue = trim($sqlWhere[1], " '");

		    foreach((array)$applicationData[$dbTableName] as $v){
			    if($v[$sqlWhereField] == $sqlWhereValue){
				    $value = $v[$dbFieldName];
				    break;
			    }
		    }
	    }else{
            $value = $applicationData[$dbTableName][$dbFieldName];
        }

        switch($field['Type']){
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR:
                $value = array(
                    $applicationData[$dbTableName]['ApplyDayType1'],
                    $applicationData[$dbTableName]['ApplyDayType2'],
                    $applicationData[$dbTableName]['ApplyDayType3'],
                );
                break;
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_HKID_TEXT:
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE:
                global $BirthCertNo;
                $value = ($BirthCertNo)?$BirthCertNo:$value;
                break;
            case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DATE:
                $value = explode('-', $value);
                break;
        }
        #### Get data END ####

        #### Get path START ####
        $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/admissionForm/field/{$field['Type']}.tmpl.php";
        $defaultFilePath = __DIR__."/template/admissionForm/field/{$field['Type']}.tmpl.php";

        if($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_FIELD){
            $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/admissionForm/field/{$field['Type']}_{$field['ExtraArr']['fieldType']}.tmpl.php";
        }
        #### Get path END ####

        #### Load template START ####
        @ob_start();

        if(file_exists($custFilePath)){
            include($custFilePath);
        }elseif(file_exists($defaultFilePath)){
            include($defaultFilePath);
        }

        $html = ob_get_clean();
        #### Load template END ####

        return $html;
    }

    protected function generateValidateJs($fields){
        global $intranet_root, $kis_admission_school, $plugin;

        #### Load template START ####
        @ob_start();
        include(__DIR__."/template/validateScript.tmpl.php");
        $html = ob_get_clean();
        #### Load template END ####
        return $html;
    }

    public function generateAdminDetailsPage($pageName, $isEdit){
        global $intranet_root, $kis_admission_school, $LangB5, $LangEn, $kis_data, $kis_lang, $applicationData;

        #### Init START ####
        $ApplicationID = $kis_data['applicationID'];
        $lauc = $kis_data['lauc'];
        $lac = $kis_data['libadmission'];

        $groupId = IntegerSafe(substr($pageName, 5));
        $group = $this->dafs->getGroupByGroupId($groupId);
        $fields = $this->dafs->getFieldsByGroupId($groupId, 1);
        #### Init END ####

        #### Get data START ####
        $applicationData = $this->dafs->getApplicationDataByGroupId($groupId, $ApplicationID);
        #### Get data END ####

	    #### Get date type START ####
	    global $dobStart, $dobEnd, $dayTypeArr;
	    $sus_status = $applicationData['ADMISSION_OTHERS_INFO']['ApplyLevel'];

	    $application_setting = $lac->getApplicationSetting();
	    $dobStart = $application_setting[$sus_status]['DOBStart'];
	    $dobEnd = $application_setting[$sus_status]['DOBEnd'];
	    $dayTypeArr = explode(',', $application_setting[$sus_status]['DayType']);
	    #### Get date type END ####

        #### Get path START ####
        $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/applicantsList/{$group['Type']}.tmpl.php";
        $filePath = __DIR__."/template/applicantsList/{$group['Type']}.tmpl.php";
        if(!file_exists($filePath)){
            $defaultFilePath = __DIR__."/template/applicantsList/NORMAL.tmpl.php";
        }

        if($group['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::GROUP_TYPE_CUSTOM){
            $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/applicantsList/{$group['Type']}_{$group['OtherAttributeArr']['pageType']}.tmpl.php";
        }

        #### Load template START ####
        @ob_start();
        echo '<script src="/templates/lodash/lodash.js"></script>';
        echo '<script>window.validateFunc = {};</script>';
        if(file_exists($custFilePath)){
            include($custFilePath);
        }elseif(file_exists($filePath)){
            include($filePath);
        }else{
            include($defaultFilePath);
        }
        $html = ob_get_clean();
        #### Load template END ####
        return $html;
    }

    protected function generateAdminDetailsPageFieldHtml($field, $value, $data = ''){
        global $intranet_root, $kis_admission_school, $LangB5, $LangEn, $kis_lang, $applicationData;

		if($data){
	    	$applicationData = $data;
		}
		else if($applicationData==''){
			$applicationData = array();
		}
        #### Get path START ####
        $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/applicantsList/field/{$field['Type']}.tmpl.php";
        $defaultFilePath = __DIR__."/template/applicantsList/field/{$field['Type']}.tmpl.php";

        if($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_FIELD||$field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_ROW){
            $custFilePath = "{$intranet_root}/includes/admission/{$kis_admission_school}/template/applicantsList/field/{$field['Type']}_{$field['ExtraArr']['fieldType']}.tmpl.php";
        }
        #### Get path END ####

        #### Load template START ####
        @ob_start();

        if(file_exists($custFilePath)){
            include($custFilePath);
        }elseif(file_exists($defaultFilePath)){
            include($defaultFilePath);
        }

        $html = ob_get_clean();
        #### Load template END ####

        return $html;
    }

}// End Class
