<?php
// modifying by: 
/**
 * ******************
 * Change Log :
 * Date 2019-10-08 [Tommy]
 * File Created
 *
 * ******************
 */
include_once ("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");

class admission_cust extends \AdmissionSystem\AdmissionCustBase
{

    const STUDENT_ACADEMIC_SCHOOL_COUNT = 1;
    
    const RELATIVES_COUNT = 1;

    public function __construct()
    {
        global $plugin;
        
        if ($plugin['eAdmission_devMode']) {
            error_reporting(E_ALL & ~ E_NOTICE);
            ini_set('display_errors', 1);
            if ($_SERVER['HTTP_HOST'] == '192.168.0.171:31002') {
                $this->AdmissionFormSendEmail = false;
            }
        }
        
        parent::__construct();
        $this->init();
    }

    private function init()
    {
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function ($emailTitle) {
            global $Lang;
            return $Lang['Admission']['FH']['email']['admissionNotification'];
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE END #### */
        
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummySchoolInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */
        
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummyRelativesInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */
        
        /* #### ACTION_APPLICANT_UPDATE_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateStudentInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateParentInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateStudentAcademicBackgroundInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateAppliStatus'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateRelativesInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateOtherInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_INFO END #### */
        
        /* #### ACTION_APPLICANT_UPDATE_*_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INFO, array(
            $this,
            'updateApplicantInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_*_INFO END #### */
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID)
    {
        $result = true;
        
        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i ++) {
            $sql = "INSERT INTO 
                ADMISSION_STU_PREV_SCHOOL_INFO 
            (
                ApplicationID, 
                SchoolOrder,
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";
            
            $result = $result && $this->db_db_query($sql);
        }
        
        if (! $result) {
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

	/**
     * Admission Form - before create applicant
     */
    protected function insertDummyRelativesInfo($ApplicationID)
    {
        $result = true;
        
        for ($i = 0; $i < self::RELATIVES_COUNT; $i ++) {
            $sql = "INSERT INTO 
                ADMISSION_RELATIVES_AT_SCH_INFO 
            (
                ApplicationID, 
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                NOW(),
                '{$this->uid}'
            )";
            
            $result = $result && $this->db_db_query($sql);
        }
        
        if (! $result) {
            throw new \Exception('Cannot insert dummy relatives info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentInfo($Data, $ApplicationID)
    {
        // #### Basic info START ####
 
        if($Data['StudentBirthCertNo'] != ''){
            $birthCertNo = 'StudentBirthCertNo';
        }else if($Data['StudentPassport'] != ''){
            $birthCertNo = 'StudentPassport';
        }
        
        $fieldArr = array(
        	'ChineseName' => 'student_name_b5',
            'EnglishName' => 'student_name_en',
            'Gender' => 'gender',
            'DOB' => 'dateofbirth',
            'Age' => 'StudentAge',
            'PlaceOfBirth' => 'StudentPlaceOfBirth',
            'County' => 'Nationality',
            'BirthCertNo' => $birthCertNo,
            'ReligionOther' => 'StudentReligion',
            'Church' => 'Church',
            'Address' => 'StudentHomeAddress',
            'AddressChi' => 'StudentHomeAddressChi',
            'HomeTelNo' => 'StudentPhone',
            'Email' => 'StudentEmail',
        );
        
        $updateSql = '';
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $updateSql .= "$dbField = '{$Data[$dataField]}',";
            }
        }
        
        // ### Concat name START ####
        if ($Data['StudentDateOfBirthYear'] && $Data['StudentDateOfBirthMonth']  && $Data['StudentDateOfBirthDay']) {
            $updateSql .= "DOB = '{$Data['StudentDateOfBirthYear']}-{$Data['StudentDateOfBirthMonth']}-{$Data['StudentDateOfBirthDay']}',";
        }
        // ### Concat name END ####
        
        $sql = "UPDATE 
            ADMISSION_STU_INFO
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'";
            
        $result = $this->db_db_query($sql);
        // #### Basic info END ####
        
        // #### Cust info START ####
        $fieldArr = array(
            'BD_Ref_Num' => 'BdRefNo',
            'Elementary_Chinese' => 'Elementary',
            'Activity_And_Award' => 'activitiesAndAwards',
        );
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $dbField,
                    'Value' => $Data[$dataField]
                ), $insertIfNotExists = true);
            }
        }
        // #### Cust info END ####
        
        if (! $result) {
            throw new \Exception('Cannot update student info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateParentInfo($Data, $ApplicationID)
    {
        $result = true;
        
        /* #### Update parent basic info START #### */
        $fieldArr = array(
        	'EnglishName' => 'ParentName',
            'Relationship' => 'ParentRelationship',
            'OfficeAddress' => 'ParentAddress',
            'Mobile' => 'ParentContactNo',
            'Company' => 'EmoloyerName',
            'JobTitle' => 'ParentOccupation'
        );
        
        $updateSql = '';
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $updateSql .= "$dbField = '{$Data[$dataField]}',";
            }
        }
        
        $sql = "UPDATE 
            ADMISSION_PG_INFO
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'
		AND
                PG_TYPE = 'G'";
        $result = $result && $this->db_db_query($sql);
        //debug_pr($sql);
        // #### Cust info START ####
        $fieldArr = array(
            'Parent_Religion' => 'ParentReligion'
        );
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $dbField,
                    'Value' => $Data[$dataField]
                ), $insertIfNotExists = true);
            }
        }
        // #### Cust info END ####
        
        if (! $result) {
            throw new \Exception('Cannot update parent info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentAcademicBackgroundInfo($Data, $ApplicationID)
    {
        $result = true;
        
        /* #### Update school basic info START #### */
        $fieldArr = array(
            'NameOfSchool' => 'LastSchool',
            'Class' => 'LastSchoolLevel'
        );
        
        for($i=0;$i<self::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){
	        $updateSql = '';
	        foreach($fieldArr as $dbField => $inputField){
                if(isset($Data["{$inputField}"])){
                    $updateSQL .= "{$dbField} = '{$Data["{$inputField}"]}',";
                }
            }
	        
	        $sql = "UPDATE
	            ADMISSION_STU_PREV_SCHOOL_INFO
	        SET
	            {$updateSQL}
	            DateModified = NOW(),
	            ModifiedBy = '{$this->uid}'
	        WHERE
	            ApplicationID = '{$ApplicationID}'
	        AND
	            SchoolOrder = '{$i}'";
	        $result = $result && $this->db_db_query($sql);
        }
        /* #### Update school basic info END #### */
        
        /* #### Update cust info START #### */
//        $fieldArr = array(
//            'ChildPlacedOutStandardGroup',
//            'ChildPlacedOutStandardGroup_Details',
//            'ChildSpecialClassTalent',
//            'ChildSpecialClassTalent_Details',
//            'ChildEducationalPsychologist',
//            'ChildEducationalPsychologist_Details',
//            'StudentFavouriteSubject',
//            'StudentSuccessfulSubject',
//            'StudentChallengingSubject'
//        );
//        foreach ($fieldArr as $field) {
//            if (isset($Data[$field])) {
//                $result = $result && $this->updateApplicationCustInfo(array(
//                    'filterApplicationId' => $ApplicationID,
//                    'filterCode' => $field,
//                    'Value' => $Data[$field]
//                ), $insertIfNotExists = true);
//            }
//        }
        /* #### Update cust info END #### */
        
        if (! $result) {
            throw new \Exception('Cannot update student academic background info');
        }
        return true;
    }
    
    /**
     * Admission Form - create/update applicant
     */
    protected function updateRelativesInfo($Data, $ApplicationID)
    {
        $result = true;
        
        /* #### Update school basic info START #### */
        $fieldArr = array(
            'Name' => 'RelativeStudent',
            'ClassPosition' => 'RelativeClassAttend',
            'Relationship' => 'RelativeRelationship'
        );
        
        for($i=0;$i<self::RELATIVES_COUNT;$i++){
	        $updateSql = '';
	        foreach($fieldArr as $dbField => $inputField){
                if(isset($Data["{$inputField}"])){
                    $updateSql .= "{$dbField} = '{$Data["{$inputField}"]}',";
                }
            }
        
	        $sql = "UPDATE
	            ADMISSION_RELATIVES_AT_SCH_INFO
	        SET
	            {$updateSql}
	            DateModified = NOW(),
	            ModifiedBy = '{$this->uid}'
	        WHERE
	            ApplicationID = '{$ApplicationID}'";
	        $result = $result && $this->db_db_query($sql);
        }
        /* #### Update school basic info END #### */
        
        /* #### Update cust info START #### */
//        $fieldArr = array(
//            'ChildPlacedOutStandardGroup',
//            'ChildPlacedOutStandardGroup_Details',
//            'ChildSpecialClassTalent',
//            'ChildSpecialClassTalent_Details',
//            'ChildEducationalPsychologist',
//            'ChildEducationalPsychologist_Details',
//            'StudentFavouriteSubject',
//            'StudentSuccessfulSubject',
//            'StudentChallengingSubject'
//        );
//        foreach ($fieldArr as $field) {
//            if (isset($Data[$field])) {
//                $result = $result && $this->updateApplicationCustInfo(array(
//                    'filterApplicationId' => $ApplicationID,
//                    'filterCode' => $field,
//                    'Value' => $Data[$field]
//                ), $insertIfNotExists = true);
//            }
//        }
        /* #### Update cust info END #### */
        
        if (! $result) {
            throw new \Exception('Cannot update student academic background info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateAppliStatus($Data, $ApplicationID)
    {
    	global $admission_cfg;
        // #### Basic info START ####
        $fieldArr = array(
        	'FeeBankName' => 'BankName',
        	'FeeChequeNo' => 'ChequeNo',
        	'Status' => 'Payed'
        );
        
        $updateSql = '';
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
            	if($dataField == 'Payed'){
            		if($Data[$dataField] == 1){
						$updateSql .= "$dbField = '{$admission_cfg['Status']['paymentsettled']}',";
            		}
				}
				else{
                	$updateSql .= "$dbField = '{$Data[$dataField]}',";
				}
            }
        }
        
        $sql = "UPDATE 
            ADMISSION_APPLICATION_STATUS
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'";
        $result = $this->db_db_query($sql);
        // #### Basic info END ####

//         // #### Cust info START ####
//         $fieldArr = array(
//             'Referee_Name' => 'Referee_Name'
//         );
//         foreach ($fieldArr as $dbField => $dataField) {
//             if (isset($Data[$dataField])) {
//                 $result = $result && $this->updateApplicationCustInfo(array(
//                     'filterApplicationId' => $ApplicationID,
//                     'filterCode' => $dbField,
//                     'Value' => $Data[$dataField]
//                 ), $insertIfNotExists = true);
//             }
//         }
//         // #### Cust info END ####
        
        if (! $result) {
            throw new \Exception('Cannot update application status');
        }
        return true;
    }
    
    protected function updateOtherInfo($Data, $ApplicationID)
    {
        $result = true;
        
        /* #### Update school basic info START #### */
        $fieldArr = array(
            'KnowUsByOther' => 'Source'
        );
        
        for($i=0;$i<self::RELATIVES_COUNT;$i++){
            $updateSql = '';
            foreach($fieldArr as $dbField => $inputField){
                if(isset($Data["{$inputField}"])){
                    if($inputField == "Source"){
                        $Data["{$inputField}"] = implode(", ", $Data["{$inputField}"]);
                    }
                    $updateSql .= "{$dbField} = '{$Data["{$inputField}"]}',";
                }
            }
            
            $sql = "UPDATE
	            ADMISSION_OTHERS_INFO
	        SET
	            {$updateSql}
	            DateModified = NOW(),
	            ModifiedBy = '{$this->uid}'
	        WHERE
	            ApplicationID = '{$ApplicationID}'";
	            $result = $result && $this->db_db_query($sql);
        }
        
        if (! $result) {
            throw new \Exception('Cannot update student other infomation');
        }
        return true;
    }

    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID)
    {
        $result = true;
        
        if ($type == 'studentinfo') {
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);
            $result = $result && $this->updateStudentAcademicBackgroundInfo($Data, $ApplicationID);
            
        } elseif ($type == 'parentinfo') {
            $result = $result && $this->updateParentInfo($Data, $ApplicationID);
        }elseif ($type == 'otherinfo') {
            $result = $result && $this->updateOtherInfo($Data, $ApplicationID);
            $result = $result && $this->updateRelativesInfo($Data, $ApplicationID);
            $result = $result && $this->updateAppliStatus($Data, $ApplicationID);
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);
        } elseif ($type == 'remarks') {
            $result = $result && $this->updateApplicationStatus($Data, $ApplicationID);
        } else {
            $result = false;
        }
        
        if (! $result) {
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }

	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
	
		if($paymentStatus==$admission_cfg['PaymentStatus']['OnlinePayment']){
			$paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NOT NULL  ";
		}
		else if($paymentStatus==$admission_cfg['PaymentStatus']['OtherPayment']){
			$paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NULL  ";
		}
		
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			$paymentStatus_cond	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
    	
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				stu.BirthCertNo AS student_hkid,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}
	
	function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1){
		global $admission_cfg;
		
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		
		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sort = $sortby? "$sortby $order":"application_id";
		
//		if(!empty($keyword)){
//			$cond .= "
//				AND ( 
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'					
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'				
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')."
			INNER JOIN 
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				1 	
			".$cond." 
			order by ".$sort."
    	";
    	$sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." ";
     	//debug_r($sql);
    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}
	   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function checkImportDataForImportAdmissionHeader($csv_header, $lang = ''){
		//$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
		$file_format = $this->getExportHeaderEDB();
		$file_format = $file_format[1];
		# check csv header
		$format_wrong = false;
		
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($csv_header[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		
		return $format_wrong;
	}
	
	public function returnPresetCodeAndNameArr($code_type="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$i=0;

		#### Get duplcate application id START ####
		$ids = array();
		foreach($data as $aData){
		    $ids[] = trim($aData[0]);
		}
		$ids = array_filter($ids);
		
		$idSql = implode("','", $ids);
		$sql = "SELECT 
		    ASI.ApplicationID, 
		    ASI.BirthCertNo 
		FROM 
		    ADMISSION_STU_INFO ASI
		INNER JOIN
		    ADMISSION_OTHERS_INFO AOI
	    ON
	        ASI.ApplicationID = AOI.ApplicationID
        AND
            AOI.ApplyYear = '".$this->getNextSchoolYearID()."'
		WHERE 
		    ASI.ApplicationID IN ('{$idSql}')
		";
		$rs = $this->returnResultSet($sql);
		
		$currentApplicationIdArr = Get_Array_By_Key($rs, 'ApplicationID');
		$applicationIdBirthCertMapping = BuildMultiKeyAssoc($rs, array('ApplicationID') , array('BirthCertNo'), $SingleValue=1);
		#### Get duplcate application id END ####
		
		foreach($data as $aData){
		    //valid ApplicationID
		    if($aData[0]){
		        $resultArr[$i]['validApplicationID'] = in_array($aData[0], $currentApplicationIdArr);
		    }else{
		        $resultArr[$i]['validApplicationID'] = true;
		    }
		    
		    //valid E.Name
		    $aData[1] = str_replace(',', '&#44;', $aData[1]);
			$resultArr[$i]['validEName'] = true;
			if($aData[1] == ''){
				$resultArr[$i]['validEName'] = false;
			}
			
		    //valid C.Name
		    $aData[2] = str_replace(',', '&#44;', $aData[2]);
			$resultArr[$i]['validCName'] = true;
			if($aData[2] == ''){
				$resultArr[$i]['validCName'] = false;
			}
		    
			//valid Birth Date
			$aData[3] = getDefaultDateFormat($aData[3]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[3]) ) {
				list($year , $month , $day) = explode('-',$aData[3]);
		       	$resultArr[$i]['validBirthDate'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validBirthDate'] = false;
			}
			
			//valid Gender
			if(strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[4] = 'M';
				$data[$i][4] = 'M';
			}
			else if(strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[4] = 'F';
				$data[$i][4] = 'F';
			}
			if ( strtoupper($aData[4]) == 'M' || strtoupper($aData[4]) == 'F') {
				
		       	$resultArr[$i]['validGender'] = true;
			}
			else{
				$resultArr[$i]['validGender'] = false;
			}
			
			//valid birth cert
			$aData[5] = str_replace(array('(',')'), array('',''), $aData[5]);
			$resultArr[$i]['validBirthCertNoFormat'] = true;
			if (!preg_match('/^[a-zA-Z][0-9A]{7}$/',$aData[5])) {
				$resultArr[$i]['validBirthCertNoFormat'] = false;
			}
			
			$resultArr[$i]['validBirthCertNoDuplicate'] = true;
			if(
			    $aData[5] != $applicationIdBirthCertMapping[$aData[0]] &&
			    $this->hasBirthCertNumber($aData[5], '')
		    ){
    			$resultArr[$i]['validBirthCertNoDuplicate'] = false;
			}
			
		    //valid STRN
			$resultArr[$i]['validSTRN'] = true;
			/*if($aData[6] == ''){
				$resultArr[$i]['validSTRN'] = false;
			}*/
			
		    //valid Current School
			$resultArr[$i]['validPrimarySchool'] = true;
			if($aData[7] == ''){
				$resultArr[$i]['validPrimarySchool'] = false;
			}
			
		    //valid Parent/Guardian
			$resultArr[$i]['validParentGuardian'] = true;
			/*if($aData[8] == ''){
				$resultArr[$i]['validParentGuardian'] = false;
			}*/
			
			//valid email address
			$resultArr[$i]['validEmail'] = true;
			if ($aData[9] && !preg_match('/\S+@\S+\.\S+/',$aData[9])) {
				$resultArr[$i]['validEmail'] = false;
			}
			
			$resultArr[$i]['validData'] = $aData[1];
			$i++;
		}
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Admission']['englishname'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
			if(!$aResult['validApplicationID']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
			}elseif(!$aResult['validEName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['msg']['enterenglishname'];
			}elseif(!$aResult['validCName']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['msg']['enterchinesename'];
			}elseif(!$aResult['validBirthDate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidDateOfBirth'];
			}elseif(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidWordOfGender'];
			}elseif(!$aResult['validBirthCertNoFormat']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertNo'];
			}elseif(!$aResult['validBirthCertNoDuplicate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['UCCKE']['msg']['duplicateHKID'];
			}elseif(!$aResult['validSTRN']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterSTRN'];
			}elseif(!$aResult['validPrimarySchool']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterCurrentSchool'];
			}elseif(!$aResult['validParentGuardian']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterParentName'];
			}elseif(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress'];
			}
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.$i.'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	
	function importDataForImportAdmission($data){
		global $kis_lang, $admission_cfg, $classLevelID;
		$resultArr = array();
		array_shift($data);
		
		$_REQUEST['sus_status'] = $classLevelID;
		foreach($data as $aData){
			global $UserID;
			
			
			$applicationNumber = ($aData[0])?$aData[0]:$this->newApplicationNumber2();
			
			//--- convert the text to key code [start]
		    if(strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[4] = 'M';
			}
			else if(strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[4] = 'F';
			}
			$aData[5] = str_replace(array('(',')'), array('',''), $aData[5]);
			
			$data = array(
                'student_name_en' => addslashes($aData[1]),
                'student_name_b5' => addslashes($aData[2]),
                'dateofbirth' => getDefaultDateFormat($aData[3]),
                'gender' => addslashes($aData[4]),
                'StudentBirthCertNo' => addslashes($aData[5]),
			    'BdRefNo' => addslashes($aData[6]),
			    'LastSchool' => addslashes($aData[7]),
            	'ParentName' => addslashes($aData[8]),
                'ParentEmail' => addslashes($aData[9]),
			);
			
			$result = $this->insertApplicationAllInfo($this, $data, $applicationNumber, !!($aData[0]));
		     
			$resultArr[] = $result;
			//debug_pr($aData);
		}
		return $resultArr;
	}
	
    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '',$classLevelID = '')
    {
        global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$headerArray = array();
		
		if($lang == 'en'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}else if($lang == 'b5'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}
		
		//for student info
		$headerArray[] = $kis_lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['applyLevel'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['chinesename'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['englishname'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['Age'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['icms']['nationality'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['munsang']['birthcertno'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['BdRefNo'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['CurrentStudySchool'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['ClassLastAttended'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['EngAddress'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['ChiAddress'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['email'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['ElementryChinese'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['ExtraCurricular'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['STCC']['religionForm'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['UCCKE']['NameOfChurch'];
		//for parent info
		$headerArray['parentInfo'][] = $kis_lang['Admission']['name'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['UCCKE']['Relationship'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['religion'];
//		$headerArray['parentInfo'][] = $kis_lang['Admission']['email'];
		$headerArray['parentInfo'][] = "{$kis_lang['Admission']['UCCKE']['EngAddress']} ({$kis_lang['Admission']['UCCKE']['DifferentAddress']})";
		$headerArray['parentInfo'][] = $kis_lang['Admission']['UCCKE']['phome'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['UCCKE']['mobile'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['occupation'];
		$headerArray['parentInfo'][] = $kis_lang['Admission']['UCCKE']['NameOfEmployer'];

		//for Relative
		$headerArray['relativeInfo'][] = $kis_lang['Admission']['name'];
		$headerArray['relativeInfo'][] = $kis_lang['Admission']['UCCKE']['Relationship'];
		$headerArray['relativeInfo'][] = $kis_lang['Admission']['UCCKE']['ClassAttending'];
		
		//for other info
		$headerArray['otherInfo'][] = $kis_lang['Admission']['munsang']['IsConsiderAlternative'];
		
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['UCCKE']['Recommended'];
		
		//for official use
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
		
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//student info header
		$exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
		for($i=0; $i < count($headerArray['studentInfo'])-2; $i++){
			$exportColumn[0][] = "";
		}
		
		//parent info header
		$exportColumn[0][] = $kis_lang['Admission']['PGInfo'];
		for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//relative info header
		$exportColumn[0][] = $kis_lang['Admission']['UCCKE']['RelativeAttending'];
		for($i=0; $i < count($headerArray['relativeInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
		
		
		//official use header
		$exportColumn[0][] = $kis_lang['remarks'];
		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//sub header
		$exportColumn[1] = array_merge(
			array($headerArray[0],$headerArray[1]), 
			$headerArray['studentInfo'], 
			$headerArray['parentInfo'],
			$headerArray['relativeInfo'], 
			$headerArray['otherInfo2'],
			$headerArray['officialUse']
		);
		//if($lang)
		//include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
		return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo3 = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
		
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		//$student_name_en = explode(',',$studentInfo['student_name_en']);
		//$student_name_b5 = explode(',',$studentInfo['student_name_b5']);
		
		$birthcertno = $studentInfo['birthcertno'];
		$birthcertno = substr_replace($birthcertno, '(', strlen($birthcertno)-1, 0) . ')'; // Add ( ) for the hkid
		
		$dataArray = array();

		//for student info
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$dataArray['studentInfo'][] = $classLevel[$otherInfo['classLevelID']];
		$dataArray['studentInfo'][] = $studentInfo['student_name_b5'];
		$dataArray['studentInfo'][] = $studentInfo['student_name_en'];
		$dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
		$dataArray['studentInfo'][] = $studentInfo['Age'];
		$dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
		$dataArray['studentInfo'][] = $studentInfo['PlaceOfBirth'];
		$dataArray['studentInfo'][] = $studentInfo['county'];
		$dataArray['studentInfo'][] = $birthcertno;
		$dataArray['studentInfo'][] = $custInfo['BD_Ref_Num'][0]['Value'];
		$dataArray['studentInfo'][] = $otherInfo2[0]['OthersPrevSchName'];
		$dataArray['studentInfo'][] = $otherInfo2[0]['OthersPrevSchClass'];
		$dataArray['studentInfo'][] = $studentInfo['homeaddress'];
		$dataArray['studentInfo'][] = $studentInfo['homeaddresschi'];
		$dataArray['studentInfo'][] = $studentInfo['Email'];
		$dataArray['studentInfo'][] = ($custInfo['Elementary_Chinese'][0]['Value'])? $Lang['Admission']['yes'] : $Lang['Admission']['no'];
		$dataArray['studentInfo'][] = $custInfo['Activity_And_Award'][0]['Value'];
		$dataArray['studentInfo'][] = $studentInfo['ReligionOther'];

// 		## Church Act START ##
// 		$str = '';
// 		foreach((array)$custInfo['Church_Activities'] as $lang){
// 			if($lang['Value'] == 'SundayWorship'){
// 				$str .= $Lang['Admission']['UCCKE']['ChurchActivitesType'][0];
// 			}else if($lang['Value'] == 'SundaySchool'){
// 				$str .= $Lang['Admission']['UCCKE']['ChurchActivitesType'][1];
// 			}else if($lang['Value'] == 'Fellowship'){
// 				$str .= $Lang['Admission']['UCCKE']['ChurchActivitesType'][2];
// 			}else if($lang['Value'] == 'Others'){
// 				$str .= $custInfo['Church_Activities_Other'][0]['Value'];
// 			}
// 			$str .= " ";
// 		}
// 		$dataArray['studentInfo'][] = $str;
// 		## Church Act END ##
		$dataArray['studentInfo'][] = $studentInfo['Church'];
		
		
		//for parent info		
		for($i=0;$i<count($parentInfo);$i++){
			if($parentInfo[$i]['type'] == 'F'){
				continue;
			}
			else if($parentInfo[$i]['type'] == 'M'){
				continue;
			}
			$dataArray['parentInfo'][] = $parentInfo[$i]['parent_name_en'];
			$dataArray['parentInfo'][] = $parentInfo[$i]['relationship'];
			$dataArray['parentInfo'][] = $custInfo['Parent_Religion'][0]['Value'];
//			$dataArray['parentInfo'][] = $parentInfo[$i]['email'];
			$dataArray['parentInfo'][] = $parentInfo[$i]['companyaddress'];
			$dataArray['parentInfo'][] = $parentInfo[$i]['OfficeTelNo'];
			$dataArray['parentInfo'][] = $parentInfo[$i]['Mobile'];
			$dataArray['parentInfo'][] = $parentInfo[$i]['occupation'];
			$dataArray['parentInfo'][] = $parentInfo[$i]['companyname'];
		}
		if(count($dataArray['parentInfo']) == 0){
			$dataArray['parentInfo'] = array('','','','','','','','','','','','');
		}
		
		//for relative info
		$dataArray['relativeInfo'][] = $otherInfo3[0]['OthersRelativeStudiedName'];
		$dataArray['relativeInfo'][] = $otherInfo3[0]['OthersRelativeRelationship'];
		$dataArray['relativeInfo'][] = $otherInfo3[0]['OthersRelativeClassPosition'];
		
		//for other info
		$dataArray['otherInfo'][] = $custInfo['Referee_Name'][0]['Value'];
		
		//for official use
		$dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
		$dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
		$dataArray['officialUse'][] = $status['receiptID'];
		$dataArray['officialUse'][] = $status['receiptdate'];
		$dataArray['officialUse'][] = $status['handler'];
		$dataArray['officialUse'][] = $status['interviewdate'];
		$dataArray['officialUse'][] = $status['interviewlocation'];
		$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
		$dataArray['officialUse'][] = $status['remark'];
		
		$ExportArr = array_merge(
			array($dataArray[0],$dataArray[1]),
			$dataArray['studentInfo'],
			$dataArray['parentInfo'],
			$dataArray['relativeInfo'], 
			$dataArray['otherInfo'], 
			$dataArray['officialUse']
		);
		
		return $ExportArr;
    }
    
    /**
     * Portal - export applicant details header
     */
    public function getExportHeaderEDB($schoolYearID = '',$classLevelID = '')
    {
        global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$exportColumn = array();
		
		$exportColumn[0][] = 'Application No.';
		$exportColumn[0][] = 'E.Name';
		$exportColumn[0][] = 'C.Name';
		$exportColumn[0][] = 'Birth Date';
		$exportColumn[0][] = 'Sex';
		$exportColumn[0][] = 'ID No.';
		$exportColumn[0][] = 'STRN';
		$exportColumn[0][] = 'Primary School';
// 		$exportColumn[0][] = 'Name of Parent/Guardian';
// 		$exportColumn[0][] = 'Email';
		
		return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportDataEDB($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$studentInfo['applicationID'],'');
		$otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
		
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$birthcertno = $studentInfo['birthcertno'];
		$birthcertno = substr_replace($birthcertno, '(', strlen($birthcertno)-1, 0) . ')'; // Add ( ) for the hkid
		
		$ExportArr = array();
		
		$ExportArr[] = $studentInfo['applicationID'];
		$ExportArr[] = $studentInfo['student_name_en'];
		$ExportArr[] = $studentInfo['student_name_b5'];
		$ExportArr[] = $studentInfo['dateofbirth'] > 0?date("j/n/Y", strtotime($studentInfo['dateofbirth'])):'';
		$ExportArr[] = $studentInfo['gender'];
		$ExportArr[] = $studentInfo['birthcertno']?$birthcertno:'';
		$ExportArr[] = $custInfo['BD_Ref_Num'][0]['Value'];
		$ExportArr[] = $otherInfo2[0]['OthersPrevSchName'];
// 		$ExportArr[] = $parentInfo[0]['EnglishName'];
// 		$ExportArr[] = $parentInfo[0]['Email'];
		
		return $ExportArr;
    }
    
    function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		$custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
		
		$dataArray = array();
		if($tabID == 2){
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			$studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = ''; //Mobile
			$dataArray[0][] = ''; //Fax
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = ''; //Remarks
			$dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
			$dataArray[0][] = (is_numeric($studentInfo['homeaddress'])?$Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']]:$studentInfo['homeaddress']); //Address
			if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
			{
				$dataArray[0][] = ''; //CardID
				if($sys_custom['SupplementarySmartCard']){
					$dataArray[0][] = ''; //CardID2
					$dataArray[0][] = ''; //CardID3
				}
			}
			if($special_feature['ava_hkid'])
				$dataArray[0][] = $studentInfo['birthcertno']; //HKID
			if($special_feature['ava_strn'])
				$dataArray[0][] = $custInfo['BD_Ref_Num'][0]['Value']; //STRN
			if($plugin['medical'])
				$dataArray[0][] = ''; //StayOverNight
			$dataArray[0][] = $studentInfo['county']; //Nationality
			$dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
		}
		else if($tabID == 3){
			$hasParent = false;
			$dataCount = array();
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			for($i=0;$i<count($parentInfo);$i++){
			if($parentInfo[$i]['type'] == 'G' && !$hasParent){
					$dataArray[0] = array();
					$dataArray[0][] = ''; //UserLogin
					$dataArray[0][] = ''; //Password
					$dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[0][] = ''; //Gender
					$dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[0][] = $custInfo['Parent_Fax'][0]['Value']; //Fax
					$dataArray[0][] = ''; //Barcode
					$dataArray[0][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[0][] = ''; //HKID
					$dataArray[0][] = ''; //StudentLogin1
					$dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[0][] = ''; //StudentLogin2
					$dataArray[0][] = ''; //StudentEngName2
					$dataArray[0][] = ''; //StudentLogin3
					$dataArray[0][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[0] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
			}
			if($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]){
				$tempDataArray = $dataArray[0];
			}
			else if($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]){
				$tempDataArray = $dataArray[1];
			}
			else if($dataCount[2] > 0){
				$tempDataArray = $dataArray[2];
			}
			$dataArray = array();
			$dataArray[0] = $tempDataArray;
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
    
    function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
// 		$sql = "SELECT
// 					a.Email as Email
// 				FROM ADMISSION_PG_INFO as a
// 				WHERE a.ApplicationID = '".trim($applicationId)."'
// 				AND
// 					a.PG_TYPE = 'G'";

		$sql = "SELECT
					a.Email as Email
				FROM ADMISSION_STU_INFO as a
				WHERE a.ApplicationID = '".trim($applicationId)."'
				";
		
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	/*
     * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();
		
// 		$sql = "SELECT
// 					a.Email as Email
// 				FROM ADMISSION_PG_INFO as a
// 				WHERE a.ApplicationID = '".trim($applicationId)."'
// 				AND a.PG_TYPE='G'";
		
		$sql = "SELECT 
					a.Email as Email
				FROM ADMISSION_STU_INFO as a 
				WHERE a.ApplicationID = '".trim($applicationId)."'
				";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		if($subject == ''){
			$email_subject = "基督教中國佈道會聖道迦南書院入學申請通知 ECF Saint Too Canaan College Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	public function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();

		if($subject == ''){
			$email_subject = "基督教中國佈道會聖道迦南書院入學申請通知 ECF Saint Too Canaan College Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
    
    function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
			//check date
			if ($aData[4] =='' && $aData[5] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
		       list($year , $month , $day) = explode('-',$aData[4]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[4] =='' && $aData[5] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."' AND trim(s.birthCertNo) = '".trim($aData[3])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[4] = getDefaultDateFormat($aData[4]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[4]." ".$aData[5]."',
				InterviewLocation = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1){
		$status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND st.status in ('".implode("','",$selectStatusArr)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		
		$sql='Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" '.$status_cond.' order by y.YearName desc';
				
		$result = $this->returnArray($sql);
		
		$allApplicant = array();
		
		for($i=0; $i<sizeof($result); $i++){
			$allApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
		}
		
//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);
		
		$result = array();
		
		$TwinsAssignedApplicant = array();
		
//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
				$sql = "SELECT RecordID, Quota FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY StartTime, GroupName";
				$interviewRecordIDArr = $this->returnArray($sql);
				
				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
					
					if(!$allApplicant){
						break;
					}
					
					$previousClassLevel = $allApplicant[0]['ClassLevel'];
					$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
					$result[] = $this->db_db_query($sql);
					
					for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
						$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
						$result[] = $this->db_db_query($sql);
						array_shift($allApplicant);
						if($previousClassLevel != $allApplicant[0]['ClassLevel']){
							break;
						}
					}
	
				}
//		}
		
		//handling twins swraping [start]
		$sql='Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID'.($round>1?$round:'').' as InterviewSettingID From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				where o.ApplyYear = "'.$selectSchoolYearID.'" AND s.IsTwinsApplied = "Y" order by y.YearName desc';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
			for($j=0; $j<sizeof($twinsResult); $j++){
				if($twinsResult[$i]['TwinsApplicationID'] == $twinsResult[$j]['BirthCertNo'] && $twinsResult[$i]['InterviewSettingID'] == $twinsResult[$j]['InterviewSettingID'] && !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)){
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" ';
					$originalSession = current($this->returnArray($sql));
					
					$sql = 'Select RecordID From ADMISSION_INTERVIEW_SETTING where ClassLevelID = "'.$originalSession['ClassLevelID'].'" AND Date = "'.$originalSession['Date'].'" '.$round_cond.' AND GroupName <> "'.$originalSession['GroupName'].'" ';
					$newSession = current($this->returnArray($sql));
					
					if($newSession){
						$sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" AND s.IsTwinsApplied <> "Y" ';
						$swapApplicant = current($this->returnArray($sql));
						if($swapApplicant){
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$originalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$j]['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$assignedTwins[] = $twinsResult[$j]['ApplicationID'];
						}
					}
				}
			}
		}
		
		//handling twins swraping [end]
		
		return !in_array(false,$result);
	}
	
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."'";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
    function getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID applicationID,					
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName 
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

    	return $this->returnArray($sql);
	}
	
	function getApplicationRelativesInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year OthersRelativeStudiedYear,
				r.Name OthersRelativeStudiedName,		    					
     			r.ClassPosition OthersRelativeClassPosition,
				r.Relationship OthersRelativeRelationship 
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

    	return $this->returnArray($sql);
	}
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

// 		if($type == "personal_photo"){
// 			return true;
// 		}
			if (!empty($file['image_data'])) {
                list($type, $data) = explode(';', $file['image_data']);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                
                $tmpfname = tempnam("/tmp", "admission_file_");
                $handle = fopen($tmpfname, "w");
                fwrite($handle, $data);
                fclose($handle);
                
                $file['tmp_name'] = $tmpfname;
			}
			if (!empty($file['tmp_name'])) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							if($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
								$image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
							else
								$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}else{
				if($this->isInternalUse($_REQUEST['token'])){
					return true;
				}
			}
			
			if(file_exists($file['tmp_name'])){
			    @unlink($file['tmp_name']);
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	/**
     * Admission Form - Before new applicant create status record
     */
    public function newApplicationNumber2()
    {
        global $sys_custom;

        $nextApplicationId = $this->getNextApplicationNumber($this->schoolYearID);

        $token = $_REQUEST['token'];
        if (! $this->isInternalUse($_REQUEST['token']) && $token == '' && ! $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $token = 1;
        }
        $HTTP_USER_AGENT = addslashes($_SERVER['HTTP_USER_AGENT']);

        $sql = "INSERT INTO
            ADMISSION_OTHERS_INFO
        (
            ApplicationID,
            ApplyYear,
            ApplyLevel,
            ApplyDayType1,
            ApplyDayType2,
            ApplyDayType3,
            Token,
            DateInput,
            HTTP_USER_AGENT
        ) Values (
            '{$nextApplicationId}',
            '{$this->schoolYearID}',
            '{$_REQUEST['sus_status']}',
            '{$_REQUEST['OthersApplyDayType1']}',
            '{$_REQUEST['OthersApplyDayType2']}',
            '{$_REQUEST['OthersApplyDayType3']}',
            '{$token}',
            NOW(),
            '{$HTTP_USER_AGENT}'
        )";
        $result = $this->db_db_query($sql);

        return $nextApplicationId;
    }
	
	private function getNextApplicationNumber($schoolYearID = '')
    {
        $yearStart = date('y', getStartOfAcademicYear('', $this->schoolYearID));

		$classLevel = $this->getClassLevel($_REQUEST['sus_status']);
		$classLevel = $classLevel[$_REQUEST['sus_status']];
		preg_match("/([0-9]+)/", $classLevel, $matches);

        $sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like 'E{$yearStart}{$matches[1]}%' ORDER BY RecordID DESC LIMIT 1";
        $rs = $this->returnVector($sql);
        $lastApplicationId = (count($rs)) ? $rs[0] : "E{$yearStart}{$matches[1]}0000";
        $prefix = substr($lastApplicationId, 0, -4);
		$num = substr($lastApplicationId, -4);
        $nextApplicationId = $prefix . str_pad(($num + 1), 4, '0', STR_PAD_LEFT);

        $nextApplicationId = $this->applyFilter(self::FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER, $nextApplicationId, $this->schoolYearID);

        return $nextApplicationId;
    }
} // End Class
