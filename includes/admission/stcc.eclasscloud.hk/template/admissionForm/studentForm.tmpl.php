<section id="studentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['studentInfo'] ?> <?=$LangEn['Admission']['studentInfo'] ?>
	</div>
	<div class="sheet">

		<div class="item">
			<div class="itemLabel">
				<?=$LangB5['Admission']['childsName'] ?> <?=$LangEn['Admission']['childsName'] ?>
			</div>
			<span class="itemInput itemInput-half">
				<div class="textbox-floatlabel">
					<input type="text" name="student_name_b5" id="student_name_b5" value="<?=$StudentInfo['ChineseName']?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['chinesename'] ?> <?=$LangEn['Admission']['chinesename'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark">(<?=$LangB5['Admission']['STCC']['useEngName']?> <?=$LangEn['Admission']['STCC']['useEngName']?>)</div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['chinesename'] ?> <?=$LangEn['Admission']['chinesename'] ?></div>
				<div class="dataValue <?=$StudentInfo['ChineseName'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['ChineseName'] ? $StudentInfo['ChineseName'] : '－－'?></div>
			</span><span class="itemInput itemInput-half">
				<div class="textbox-floatlabel">
					<input type="text" name="student_name_en" id="student_name_en" value="<?=$StudentInfo['EnglishName']?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['englishname'] ?> <?=$LangEn['Admission']['englishname'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark">(<?=$LangB5['Admission']['UCCKE']['SameAsHKID']?> <?=$LangEn['Admission']['UCCKE']['SameAsHKID']?>)</div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['englishname'] ?> <?=$LangEn['Admission']['englishname'] ?></div>
				<div class="dataValue <?=$StudentInfo['EnglishName'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['EnglishName'] ? $StudentInfo['EnglishName'] : '－－'?></div>
			</span>
		</div>

		<div class="item">
		<span class="itemInput itemInput-half">
			<div class="itemLabel requiredLabel"><?=$LangB5['Admission']['dateofbirth'] ?> <?=$LangEn['Admission']['dateofbirth'] ?></div>
				<span class="selector">
					<select id="StudentDateOfBirthYear" name="StudentDateOfBirthYear" class="dob dateYear">
						<option value="" hidden><?=$LangB5['Admission']['year'] ?> <?=$LangEn['Admission']['year'] ?></option>
						<?php 
 						if($application_setting['DOBStart'] && $application_setting['DOBEnd']){
     						$startYear = substr($application_setting['DOBStart'], 0, 4);
     						$endYear = substr($application_setting['DOBEnd'], 0, 4);
 						}else{
 						    $startYear = date('Y') - 16;
 						    $endYear = date('Y')- 11;
 						}
//						$startYear = date('Y') - 16;
//						$endYear = date('Y')- 11;
						for($i=$endYear;$i>=$startYear;$i--): 
						?>
							<option value="<?=$i ?>" <?=$dobYear == $i ? 'selected' : ''?>><?= $i ?></option>
						<?php 
						endfor; 
						?>
					</select>
				</span><span class="selector">
					<select id="StudentDateOfBirthMonth" name="StudentDateOfBirthMonth" class="dob dateMonth">
						<option value="" hidden><?=$LangB5['Admission']['month'] ?> <?=$LangEn['Admission']['month'] ?></option>
						<?php for($i=1;$i<=12;$i++): ?>
							<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$dobMonth == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
						<?php endfor; ?>
					</select>
				</span><span class="selector">
					<select id="StudentDateOfBirthDay" name="StudentDateOfBirthDay" class="dob dateDay">
						<option value="" hidden><?=$LangB5['Admission']['day'] ?> <?=$LangEn['Admission']['day'] ?></option>
						<?php for($i=1;$i<=31;$i++): ?>
							<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$dobDay == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
						<?php endfor; ?>
					</select>
				</span>
				<span>
					<div class="remark remark-warn hide" id="errDobRequired"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
					<div class="remark remark-warn hide" id="errDobRange"><?=$LangB5['Admission']['FH']['msg']['invalidbdaydateformat'] ?> <?=$LangEn['Admission']['FH']['msg']['invalidbdaydateformat'] ?></div>
				</span>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['dateofbirth'] ?> <?=$LangEn['Admission']['dateofbirth'] ?></div>
				<div class="dataValue <?=$dobYear && $dobMonth && $dobDay ? '' : 'dataValue-empty'?>"><?=$dobYear && $dobMonth && $dobDay? $dobYear.'-'.$dobMonth.'-'.$dobDay : '－－'?></div>
			</span><span class="itemInput itemInput-half">
				<div class="itemLabel requiredLabel"><?=$LangB5['Admission']['RMKG']['age'] ?> <?=$LangEn['Admission']['RMKG']['age'] ?></div>
				<div class="textbox-floatlabel">
					<span class="itemInput itemInput-half">
					<span class="itemInput itemInput-half">
						<input type="text" name="StudentAge" id="StudentAge" value="<?=$StudentInfo['Age']?>" readonly>
						</span>
					</span>
				</div>
			</span><span class="itemData itemData-half" >
				<div class="dataLabel"><?=$LangB5['Admission']['RMKG']['age'] ?> <?=$LangEn['Admission']['RMKG']['age'] ?></div>
				<div class="dataValue <?=$StudentInfo['Age'] ? '' : 'dataValue-empty'?>" id = "ageValue"><?=$StudentInfo['Age'] ? $StudentInfo['Age'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['gender'] ?> <?=$LangEn['Admission']['gender'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="gender_M" name="gender" value="M" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> <?=$StudentInfo['Gender'] == "M"?'checked':'' ?>>
					<label for="gender_M"><?=$LangB5['Admission']['genderType']['M'] ?> <?=$LangEn['Admission']['genderType']['M'] ?></label>
				</span>
				<span>
					<input type="radio" id="gender_F" name="gender" value="F" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> <?=$StudentInfo['Gender'] == "F"?'checked':'' ?>>
					<label for="gender_F"><?=$LangB5['Admission']['genderType']['F'] ?> <?=$LangEn['Admission']['genderType']['F'] ?></label>
    				<div class="remark remark-warn hide"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue <?=$StudentInfo['Gender'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['Gender'] ? $StudentInfo['Gender'] : '－－'?></div>
			</div>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentPlaceOfBirth" id="StudentPlaceOfBirth" value="<?=$StudentInfo['PlaceOfBirth'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['placeofbirth'] ?> <?=$LangEn['Admission']['placeofbirth'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['placeofbirth'] ?> <?=$LangEn['Admission']['placeofbirth'] ?></div>
				<div class="dataValue <?=$StudentInfo['PlaceOfBirth'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['PlaceOfBirth'] ? $StudentInfo['PlaceOfBirth'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="Nationality" id="Nationality" value="<?=$StudentInfo['County']?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['TSUENWANBCKG']['nationality'] ?> <?=$LangEn['Admission']['TSUENWANBCKG']['nationality'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['nationality'] ?> <?=$LangEn['Admission']['nationality'] ?></div>
				<div class="dataValue <?=$StudentInfo['County'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['County'] ? $StudentInfo['County'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
		<span class="itemInput itemInput-half" >
				<?php $hkId = "hkid";
				      $passport = "passport";
				?>
				<span class="selector">
					<select id="hkidOrPassport" name="hkidOrPassport" value="<?=$StudentInfo['hkidOrPassport']?>" required>
						<option value="" hidden><?=$LangB5['Admission']['STCC']['HKID']?> <?=$LangEn['Admission']['STCC']['HKID']?> / <?=$LangEn['Admission']['STCC']['Passport'] ?>*</option>
						<option value="<?=$hkId?>" >HKID</option>
						<option value="<?=$passport?>" >Passport</option>
					</select>
					</span>
					<span>
						<div class="remark remark-warn hide" id="errHkIdPassport"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
					</span>
					<span class="itemData itemData-half">
				<div class="dataLabel"></div>
				<div class="dataValue"></div>
			</span>
			</span>
				
				<div id="selectHkId"  class="hide">
				<span class="itemInput itemInput-half" >
				<div class="textbox-floatlabel">
					<input type="text" name="StudentBirthCertNo" id="StudentBirthCertNo" value="<?=$StudentInfo['BirthCertNo']?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['UCCKE']['HKID'] ?> <?=$LangEn['Admission']['UCCKE']['HKID'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentBirthCertNo"></div>
					<div class="remark"><?=$LangB5['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?> <?=$LangEn['Admission']['HKUGAPS']['msg']['birthcertnohints']?></div>
				</div>
				</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['HKID'] ?> <?=$LangEn['Admission']['UCCKE']['HKID'] ?></div>
				<div class="dataValue <?=$StudentInfo['BirthCertNo'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['BirthCertNo'] ? $StudentInfo['BirthCertNo'] : '－－'?></div>
				</span>
				</div>
				
				<div id="selectPassport" class="hide">
				<span class="itemInput itemInput-half" >
				<div class="textbox-floatlabel">
					<input type="text" name="StudentPassport" id="StudentPassport" value="<?=$StudentInfo['BirthCertNo']?>" required>
					<div class="textboxLabel requiredLabel">Passport</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentPassport"></div>
				</div>
				</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['STCC']['Passport'] ?></div>
				<div class="dataValue <?=$StudentInfo['BirthCertNo'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['BirthCertNo'] ? $StudentInfo['BirthCertNo'] : '－－'?></div>
				</span>
				</div>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="BdRefNo" id="BdRefNo" value="<?=$allCustInfo['BD_Ref_Num'][0]['Value']?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['BdRefNo'] ?> <?=$LangEn['Admission']['UCCKE']['BdRefNo'] ?> (STRN)</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark">(例如：A1234567，請輸入 "A1234567") (eg：A1234567，please enter "A1234567")</div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['BdRefNo'] ?> <?=$LangEn['Admission']['UCCKE']['BdRefNo'] ?> (STRN)</div>
				<div class="dataValue <?=$allCustInfo['BD_Ref_Num'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['BD_Ref_Num'][0]['Value'] ? $allCustInfo['BD_Ref_Num'][0]['Value'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="LastSchool" id="LastSchool" class="inputselect" value="<?=$currentSchoolName?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['CurrentStudySchool'] ?> <?=$LangEn['Admission']['UCCKE']['CurrentStudySchool'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['CurrentStudySchool'] ?> <?=$LangEn['Admission']['UCCKE']['CurrentStudySchool'] ?></div>
				<div class="dataValue <?=$currentSchoolName ? '' : 'dataValue-empty'?>"><?=$currentSchoolName ? $currentSchoolName : '－－'?></div>
				<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentReligion" id="StudentReligion" value="<?=$StudentInfo['ReligionOther']?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['STCC']['religion'] ?> <?=$LangEn['Admission']['STCC']['religion'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['STCC']['religion'] ?> <?=$LangEn['Admission']['STCC']['religion'] ?></div>
				<div class="dataValue <?=$StudentInfo['ReligionOther'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['ReligionOther'] ? $StudentInfo['ReligionOther'] : '－－'?></div>
			</span>
		</div>
	
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="Church" id="Church" value="<?=$StudentInfo['Church']?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['NameOfChurch'] ?> <?=$LangEn['Admission']['UCCKE']['NameOfChurch'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['NameOfChurch'] ?> <?=$LangEn['Admission']['UCCKE']['NameOfChurch'] ?></div>
				<div class="dataValue <?=$StudentInfo['Church'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['Church'] ? $StudentInfo['Church'] : '－－'?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['ElementryChinese'] ?> <?=$LangEn['Admission']['UCCKE']['ElementryChinese'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="Elementary_Y" name="Elementary" value="1" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> <?=$allCustInfo['Elementary_Chinese'][0]['Value']?'checked':'' ?>>
					<label for="Elementary_Y"><?=$LangB5['Admission']['yes'] ?> <?=$LangEn['Admission']['yes'] ?></label>
				</span>
				<span>
					<input type="radio" id="Elementary_N" name="Elementary" value="0" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> <?=!$allCustInfo['Elementary_Chinese'][0]['Value']?'checked':'' ?>>
					<label for="Elementary_N"><?=$LangB5['Admission']['no'] ?> <?=$LangEn['Admission']['no'] ?></label>
    				<div class="remark remark-warn hide"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue"><?=$allCustInfo['Elementary_Chinese'][0]['Value']?$LangB5['Admission']['yes'].' '.$LangEn['Admission']['yes']:$LangB5['Admission']['no'].' '.$LangEn['Admission']['no']?></div>
			</div>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentHomeAddress" id="StudentHomeAddress" value="<?=$StudentInfo['homeaddress']?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['EngAddress'] ?> <?=$LangEn['Admission']['UCCKE']['EngAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['EngAddress'] ?> <?=$LangEn['Admission']['UCCKE']['EngAddress'] ?></div>
				<div class="dataValue <?=$StudentInfo['homeaddress'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['homeaddress'] ? $StudentInfo['homeaddress'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentHomeAddressChi" id="StudentHomeAddressChi" value="<?=$StudentInfo['AddressChi']?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['ChiAddress'] ?> <?=$LangEn['Admission']['UCCKE']['ChiAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['ChiAddress'] ?> <?=$LangEn['Admission']['UCCKE']['ChiAddress'] ?></div>
				<div class="dataValue <?=$StudentInfo['AddressChi'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['AddressChi'] ? $StudentInfo['AddressChi'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentPhone" id="StudentPhone" value="<?=$StudentInfo['HomeTelNo']?>" maxLength="8">
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['phoneno'] ?> <?=$LangEn['Admission']['phoneno'] ?></div>
					
					<div class="remark remark-warn hide" id="errPhoneRequired"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errPhone"><?=$LangB5['Admission']['STCC']['phoneNonvaild'] ?> <?=$LangEn['Admission']['STCC']['phoneNonvaild'] ?></div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['phoneno'] ?> <?=$LangEn['Admission']['phoneno'] ?></div>
				<div class="dataValue <?=$StudentInfo['HomeTelNo'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['HomeTelNo'] ? $StudentInfo['HomeTelNo'] : '－－'?></div>
			</span>
		</div>	
			
		<div class="item">	
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentEmail" id="StudentEmail" value="<?=$StudentInfo['email']?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['email'] ?><?=$Lang['Admission']['STCC']['Notification']?> <?=$LangEn['Admission']['email'] ?> <?=$LangEn['Admission']['STCC']['Notification']?></div>
					<div class="remark remark-warn hide" id="errEmailRequired"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentEmail"><?=$LangB5['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress']?> <?=$LangEn['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress']?></div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$LangB5['Admission']['email'] ?> <?=$LangEn['Admission']['email'] ?></div>
				<div class="dataValue <?=$StudentInfo['email'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['email'] ? $StudentInfo['email'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" id="StudentEmailConfirm" value="<?=$StudentInfo['email']?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['contactEmailConfirm'] ?><?=$LangB5['Admission']['STCC']['Notification']?> <?=$LangEn['Admission']['contactEmailConfirm'] ?><?=$LangEn['Admission']['STCC']['Notification']?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentEmailConfirm"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['contactEmailConfirm'] ?> <?=$LangEn['Admission']['contactEmailConfirm'] ?></div>
				<div class="dataValue <?=$StudentInfo['email'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['email'] ? $StudentInfo['email'] : '－－'?></div>
			</span>
		</div>
		
		<div class="remark">* <?=$LangB5['Admission']['requiredFields'] ?> <?=$LangEn['Admission']['requiredFields'] ?></div>

	</div>
</section>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<script>
$(function(){
	'use strict';
	
	$('#LastSchool').on('blur', function(e){
		setTimeout(function(){
			$('#LastSchool').change();
		}, 1000);
	});

	$('#hkidOrPassport').change(function(){
		var chance = $('#hkidOrPassport').val();
		if(chance == 'hkid'){
			if(!$('#selectPassport').hasClass('hide')){
				$('#selectPassport').addClass('hide');
			}
			$('#selectHkId').removeClass('hide');
			$('#StudentPassport').val('');
			if($('#hkidOrPassport').prop('required')){
				if($('#StudentBirthCertNo').prop('required')){

				}else{
					$('#StudentBirthCertNo').prop('required', true);
				}
			}
			$('#StudentPassport').removeAttr('required');
		}else if(chance == 'passport'){
			if(!$('#selectHkId').hasClass('hide')){
				$('#selectHkId').addClass('hide');
			}
			$('#selectPassport').removeClass('hide');
			$('#StudentBirthCertNo').val('');
			if($('#hkidOrPassport').prop('required')){
				if($('#StudentPassport').prop('required')){

				}else{
					$('#StudentPassport').prop('required', true);
				}
			}
			$('#StudentBirthCertNo').removeAttr('required');
		}
	});

	$('#StudentDateOfBirthYear, #StudentDateOfBirthMonth, #StudentDateOfBirthDay').change(function(){
		
		if(
				($('#StudentDateOfBirthYear').val() != '') &&
				($('#StudentDateOfBirthMonth').val() != '') &&
				($('#StudentDateOfBirthDay').val() != '')
			){
    		var age = getAge();
    		$('#StudentAge').val(age);
    		$('#ageValue').html(age);
    		$('#ageValue').removeClass('dataValue-empty');
		}
	});
		
	$('#studentForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$inputBirthcerttype = $itemInput.find('select.certtype'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($inputText.index($(this)))).html($(this).val());
			$($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
			if(!$($dataValue.get($inputText.index($(this)))).html()){
				$($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
				$($dataValue.get($inputText.index($(this)))).html('－－');
			}
		});
		
		$inputRadio.on('change', function(e){
			$dataValue.html($(this).next('label').html());
			$dataValue.removeClass('dataValue-empty');
			if(!$dataValue.html()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
			
		$inputDateSelect.on('change', function(e){
			$dataValue.html($inputDateYearSelect.val()+'-'+$inputDateMonthSelect.val()+'-'+$inputDateDaySelect.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});

		if($('#hkidOrPassport').prop('required',true)){
		}else{
			$('#hkidOrPassport').removeAttr('required');
		}
		
	});
	
	
	// autocomplete for inputselect fields
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "../admission_form/ajax_get_suggestions.php",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});

	if(window.isUpdatePeriod && ($('#StudentBirthCertNo').val() != '' ||  $('#StudentPassport').val() != '')){
		if(/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test($('#StudentBirthCertNo').val()) || check_hkid($('#StudentBirthCertNo').val())){
			$('#hkidOrPassport').val('hkid');
			$('#selectHkId').removeClass('hide');
		}else{
			$('#hkidOrPassport').val('passport');
			$('#selectPassport').removeClass('hide');
		}
	}
	
	function checkBirthCertNo(){
		var values = $("#form1").serialize();
		var res = null;
		/* check the birth cert number is applied or not */
	   $.ajax({
	       url: "../admission_form/ajax_get_birth_cert_no.php",
	       type: "post",
	       data: values,
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	            res = data;
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   return res;
	}
	
	/*
	hkid format:  A123456(7)
	A1234567
	AB123456(7)
	AB1234567
	*/
	function check_hkid(hkid) {
		var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
		var ra = re.exec(hkid);
		
		if (ra != null) {
			var p1 = ra[1];
			var p2 = ra[2];
			var p3 = ra[4];
			var check_sum = 0;
			if (p1.length == 2) {
				check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
			}
			else if (p1.length == 1){
				check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
			}
		
			check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
			var check_digit = 11 - (check_sum % 11);
			if (check_digit == '11') {
				check_digit = 0;
			}
			else if (check_digit == '10') {
				check_digit = 'A';
			}
			if (check_digit == p3 ) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	function getAge(){
		var dateObj = new Date();
		var month = dateObj.getUTCMonth() + 1; //months from 1-12
		var day = dateObj.getUTCDate();
		var year = dateObj.getUTCFullYear();

		var age = year - $('#StudentDateOfBirthYear').val();
	    if((month < $('#StudentDateOfBirthMonth').val()) || ( (month == $('#StudentDateOfBirthMonth').val()) && day < $('#StudentDateOfBirthDay').val())){
	        age--;
	    }
	    return age;
	}
	
	window.checkStudentForm = (function(lifecycle){
		var isValid = true;
		var $studentForm = $('#studentForm');
		var dateObj = new Date();
		var year = dateObj.getUTCFullYear();
	
		/******** Basic init START ********/
		//for mm/yy validation
		var re = /^(0[123456789]|10|11|12)\/\d{2}$/;
		/******** Basic init END ********/
	
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($studentForm);

		if(
			($('#StudentDateOfBirthYear').val() == '') ||
			($('#StudentDateOfBirthMonth').val() == '') ||
			($('#StudentDateOfBirthDay').val() == '')
		){
			$('#errDobRequired').removeClass('hide');
			$('#StudentDateOfBirthYear').focus();
			isValid = false;
		}

		if($('#hkidOrPassport').val() == '' && $('#hkidOrPassport').prop('required',true)){
			$('#errHkIdPassport').removeClass('hide');
			$('#hkidOrPassport').focus();
			isValid = false;
		}
		
		
// 		if(
// 		(
//			'<?=$application_setting['DOBStart']?>' !='' && 
//			$('#StudentDateOfBirthYear').val()+'-'+$('#StudentDateOfBirthMonth').val()+'-'+$('#StudentDateOfBirthDay').val() < '<?=$application_setting['DOBStart']?>'
// 		) || (
//			'<?=$application_setting['DOBEnd']?>' !='' && 
//			$('#StudentDateOfBirthYear').val()+'-'+$('#StudentDateOfBirthMonth').val()+'-'+$('#StudentDateOfBirthDay').val() > '<?=$application_setting['DOBEnd']?>'
// 		)
// 		){
		if(
		(
			$('#StudentDateOfBirthYear').val()+'-'+$('#StudentDateOfBirthMonth').val()+'-'+$('#StudentDateOfBirthDay').val() < (year - 16)
		) || (
			$('#StudentDateOfBirthYear').val()+'-'+$('#StudentDateOfBirthMonth').val()+'-'+$('#StudentDateOfBirthDay').val() > (year - 11)
		)
		){
			if($('#errDobRequired').hasClass('hide')){
				$('#errDobRange').removeClass('hide');
				focusElement = $('#StudentDateOfBirthYear');
			}
			isValid = false;
		}
		
		if(!window.isUpdatePeriod && $('#StudentBirthCertNo').val().trim()!='' && (!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test($('#StudentBirthCertNo').val()) || !check_hkid($('#StudentBirthCertNo').val()))){
			if($('#StudentBirthCertNo').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentBirthCertNo').html('<?=$LangB5['Admission']['UCCKE']['msg']['invalidHKID']?> <?=$LangEn['Admission']['UCCKE']['msg']['invalidHKID']?>');
				$('#errStudentBirthCertNo').removeClass('hide');
				focusElement = $('#StudentBirthCertNo');
			}	
	    	isValid = false;
	    }
		
		if(!window.isUpdatePeriod && $('#StudentBirthCertNo').val().trim()!='' && checkBirthCertNo() > 0){
			if($('#StudentBirthCertNo').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentBirthCertNo').html('<?=$LangB5['Admission']['UCCKE']['msg']['duplicateHKID']?> <?=$LangEn['Admission']['UCCKE']['msg']['duplicateHKID']?>');
				$('#errStudentBirthCertNo').removeClass('hide');
				focusElement = $('#StudentBirthCertNo');
			}	
	    	isValid = false;
	    }

		if(!window.isUpdatePeriod && $('#StudentPassport').val().trim()!='' && checkBirthCertNo() > 0){
			if($('#StudentPassport').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentPassport').html('<?=$LangB5['Admission']['STCC']['msg']['duplicatePassport']?> <?=$LangEn['Admission']['STCC']['msg']['duplicatePassport']?>');
				$('#errStudentPassport').removeClass('hide');
				focusElement = $('#StudentPassport');
			}	
	    	isValid = false;
	    }

		var age = $('#StudentAge').val();

	    if(age == ''){
			$('#errAgeRequired').removeClass('hide');
			$('#StudentAge').focus();
			isValid = false;
	    }else if(age != getAge()){
			$('#errAge').removeClass('hide');
			$('#StudentAge').focus();
			isValid = false;
	    }

	    var phone = $('#StudentPhone').val();
 		var regex = "^(2|3|5|6|9){1}([0-9]{7})$";
		var checkNum = phone.match(regex);

	    if(phone == ''){
			$('#errPhoneRequired').removeClass('hide');
			$('#StudentPhone').focus();
			isValid = false;
	    }else if(checkNum == null){
			$('#errPhone').removeClass('hide');
			$('#StudentPhone').focus();
			isValid = false;
	    }

		var mail = /\S+@\S+\.\S+/;

		if($('#StudentEmail').val().trim()!='' && !mail.test($('#StudentEmail').val())){
			if($('#StudentEmail').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentEmail').html('<?=$LangB5['Admission']['icms']['msg']['invalidmailaddress']?> <?=$LangEn['Admission']['icms']['msg']['invalidmailaddress']?>');
				$('#errStudentEmail').removeClass('hide');
				focusElement = $('#errStudentEmail');
			}	
	    	isValid = false;
		}

		if($('#StudentEmailConfirm').val().trim()!='' && $('#StudentEmailConfirm').val() != $('#StudentEmail').val()){
			if($('#StudentEmailConfirm').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentEmailConfirm').html('<?=$LangB5['Admission']['msg']['contactEmailMismatch']?> <?=$LangEn['Admission']['msg']['contactEmailMismatch']?>');
				$('#errStudentEmailConfirm').removeClass('hide');
				focusElement = $('#StudentEmailConfirm');
			}	
	    	isValid = false;
		}
		
		/**** Check required END ****/
		
		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkStudentForm);
});
</script>