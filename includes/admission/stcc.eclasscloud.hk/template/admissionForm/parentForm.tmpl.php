<section id="parentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['PGInfo'] ?> <?=$LangEn['Admission']['PGInfo'] ?>
	</div>
	<div class="sheet">
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentName" id="ParentName" value="<?=$ParentInfo['G']['EnglishName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['UCCKE']['NameOfParentGuardian'] ?> <?=$LangEn['Admission']['UCCKE']['NameOfParentGuardian'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['NameOfParentGuardian'] ?> <?=$LangEn['Admission']['UCCKE']['NameOfParentGuardian'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['EnglishName'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['EnglishName'] ? $ParentInfo['G']['EnglishName'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentRelationship" id="ParentRelationship" value="<?=$ParentInfo['G']['Relationship'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['relationship'] ?> <?=$LangEn['Admission']['relationship'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['relationship'] ?> <?=$LangEn['Admission']['relationship'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Relationship'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Relationship'] ? $ParentInfo['G']['Relationship'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentReligion" id="ParentReligion" value="<?=$allCustInfo['Parent_Religion'][0]['Value'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['religion'] ?>(<?=$LangB5['Admission']['ifAny'] ?>) <?=$LangEn['Admission']['religion'] ?>(<?=$LangEn['Admission']['ifAny'] ?>)</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['religion'] ?>(<?=$LangB5['Admission']['ifAny'] ?>) <?=$LangEn['Admission']['religion'] ?>(<?=$LangEn['Admission']['ifAny'] ?>)</div>
				<div class="dataValue <?=$allCustInfo['Parent_Religion'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['Parent_Religion'][0]['Value'] ? $allCustInfo['Parent_Religion'][0]['Value'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentAddress" id="ParentAddress" value="<?=$ParentInfo['G']['OfficeAddress'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['EngAddress'] ?> <?=$LangEn['Admission']['UCCKE']['EngAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark">(<?=$LangB5['Admission']['UCCKE']['DifferentAddress'] ?> <?=$LangEn['Admission']['UCCKE']['DifferentAddress'] ?>)</div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['EngAddress'] ?> <?=$LangEn['Admission']['UCCKE']['EngAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['OfficeAddress'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['OfficeAddress'] ? $ParentInfo['G']['OfficeAddress'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentContactNo" id="ParentContactNo" value="<?=$ParentInfo['G']['Mobile'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> maxlength="8">
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['phoneno'] ?> <?=$LangEn['Admission']['phoneno'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errParentContactNo"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['phoneno'] ?> <?=$LangEn['Admission']['phoneno'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Mobile'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Mobile'] ? $ParentInfo['G']['Mobile'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="EmoloyerName" id="EmoloyerName" value="<?=$ParentInfo['G']['Company'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel requiredLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['STCC']['Employer'] ?> <?=$LangEn['Admission']['STCC']['Employer'] ?></div>
					<div class="remark remark-warn hide" id="errEmployerName"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel "><?=$LangB5['Admission']['STCC']['Employer'] ?> <?=$LangEn['Admission']['STCC']['Employer'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Company'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Company'] ? $ParentInfo['G']['Company'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentOccupation" id="ParentOccupation" value="<?=$ParentInfo['G']['JobTitle'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['occupation'] ?> <?=$LangEn['Admission']['occupation'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['occupation'] ?> <?=$LangEn['Admission']['occupation'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['JobTitle'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['JobTitle'] ? $ParentInfo['G']['JobTitle'] : '－－'?></div>
			</span>
		</div>
		
		<div class="remark">* <?=$LangB5['Admission']['requiredFields'] ?> <?=$LangEn['Admission']['requiredFields'] ?></div>
	</div>
</section>

<script>
$(function(){
	'use strict';
	
	$('#parentForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($inputText.index($(this)))).html($(this).val());
			$($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
			if(!$($dataValue.get($inputText.index($(this)))).html()){
				$($dataValue.get($inputText.index($(this)))).html('－－');
				$($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$dataValue.html($inputRadio.next('label').html());
			$dataValue.removeClass('dataValue-empty');
			if(!$dataValue.html()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
	});
	
	window.checkParentForm = (function(lifecycle){
		var isValid = true;
		var $parentForm = $('#parentForm');

		/******** Basic init START ********/
		//for email validation
		var re = /\S+@\S+\.\S+/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($parentForm);
		
		if($('#ParentContactNo').val().trim()!='' &&  !/^(2|3|5|6|9){1}([0-9]{7})$/.test($('#ParentContactNo').val())){
			if($('#ParentContactNo').parent().find('.remark-warn').hasClass('hide')){
				$('#errParentContactNo').html('<?=$LangB5['Admission']['msg']['invalidmobilephoneformat']?> <?=$LangEn['Admission']['msg']['invalidmobilephoneformat']?>');
				$('#errParentContactNo').removeClass('hide');
				focusElement = $('#ParentContactNo');
			}	
	    	isValid = false;
		}

		if($('#EmoloyerName').val() == ''){
			$('#errEmployerName').removeClass('hide');
			$('#EmoloyerName').focus();
			isValid = false;
		}
		/**** Check required END ****/

		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkParentForm);
});
</script>