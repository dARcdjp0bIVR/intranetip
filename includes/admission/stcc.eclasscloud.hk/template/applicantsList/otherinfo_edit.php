<?php

global $libkis_admission;


#### Get Student Cust Info START ####
$applicationInfo['studentCustInfo'] = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info END ####


#### Get Relatives Info START ####
$applicationInfo['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$knowUsBy = $libkis_admission->getKnowUsBy($applicationInfo['applicationID']);
#### Get Relatives Info END ####


?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<input type="hidden" name="RelativesID" value="<?=$applicationInfo['studentApplicationRelativesInfo'][0]['RecordID']?>" />
<table class="form_table">
	<colgroup>
		<col width="30%">
		<col width="20%">
		<col width="30%">
		<col width="20%">
	</colgroup>
	<tbody>     
<!-------------- Siblings START -------------->
		<tr>	
		
			<td class="field_title">
				<?=$kis_lang['Admission']['STCC']['activitiesAndAwardsForm']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTAREA('activitiesAndAwards', $applicationInfo['studentCustInfo']['Activity_And_Award'][0]['Value'])?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['STCC']['RelativeAttending']?>
			</td>
			<td>
				<?=$libinterface->GET_TEXTBOX('RelativeStudent', 'RelativeStudent', $applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeStudiedName'], $OtherClass='', $OtherPar=array())?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['UCCKE']['ClassAttending']?>
			</td>
			<td>
				<select id="RelativeClassAttend" name="RelativeClassAttend" class="classAttend">
				<option value="" hidden><?=$LangB5['Admission']['STCC']['ClassAttending'] ?> <?=$LangEn['Admission']['STCC']['ClassAttending'] ?></option>
					<?php for($i=1;$i<sizeof($admission_cfg['Class']);$i++): ?>
						<option value="<?=$admission_cfg['Class'][$i]?>" <?=$applicationInfo['studentApplicationRelativesInfo'][0]['OthersRelativeClassPosition'] == $admission_cfg['Class'][$i] ? 'selected' : ''?>><?= $admission_cfg['Class'][$i] ?></option>
					<?php endfor; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['STCC']['Source'] ?>
			</td>
			<td>
				<?php 
    				if(sizeof($knowUsBy) > 0){
    				    for($i=0; $i <= sizeof($knowUsBy); $i++){
    				        $source .= "<p>".$knowUsBy[$i]."</p>";
    				    }
    				}
				?>
				<?=kis_ui::displayTableField($source)?>
			</td>
		</tr>
<!-------------- Siblings END -------------->

	</tbody>
</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var activitiesAndAwards = $('[name=activitiesAndAwards]').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'/'+activitiesAndAwards+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){

	return true;
}
</script>