<?php
# modifying by: Henry

/********************
 * 
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_ui_cust_base
 * 			Modified getDocsUploadForm() to follow attachment settings
 * 			Moved getDocsUploadForm() to libadmission_ui_cust_base.php
 * 			
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step){
		global $Lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">
				<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<!--<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].'</a></div>-->
					<div class="'.$active_step4.' first_step"><a '.$href_step4.'><span>1</span>'.$Lang['Admission']['personalInfo'].'</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>2</span>'.$Lang['Admission']['docsUpload'].'</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>3</span>'.$Lang['Admission']['confirmation'].'</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:40px">'.$Lang['Admission']['finish'].'</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage']; //Henry 20131107
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].'</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm();
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage']; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
         $x .= $Instruction;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->
								<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
								
							</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm(){
		global $libkis_admission, $Lang, $lac;
		$class_level = $lac->getClassLevel();
		//henry modified for ICMS class selection 20140127
		$application_setting = $lac->getApplicationSetting("all_year");
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate']){
					$hasClassLevelApply = 1;
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="" />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].'</label>';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025 comment it for the ICMS
		$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Admission']['newApplication'], "button", "goto('step_index','step_input_form')", "SubmitBtn", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm(){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm();
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span class="text_remark">'.$Lang['General']['RequiredField'].'</span>';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_input_form','step_index')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
			</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg;
		
		//------Get the day type of the class level [START]
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		//henry modified 20140127
		$applicationSetting = $lac->getApplicationSetting("all_year");
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		$classLevel = $lac->getClassLevel($formData['sus_status']);
		$classLevel = $classLevel[$formData['sus_status']];
		if($IsConfirm){
			for($i=1; $i<=3; $i++){
			if($formData['OthersApplyDayType'.$i] == 1)
				$dayTypeOption1 = $Lang['Admission']['Option']." 1: ".$Lang['Admission']['icms'][$classLevel]['TimeSlot'][$i]."<br/>";
			if($formData['OthersApplyDayType'.$i] == 2)
				$dayTypeOption2 = $Lang['Admission']['Option']." 2: ".$Lang['Admission']['icms'][$classLevel]['TimeSlot'][$i]."<br/>";
			if($formData['OthersApplyDayType'.$i] == 3)
				$dayTypeOption3 = $Lang['Admission']['Option']." 3: ".$Lang['Admission']['icms'][$classLevel]['TimeSlot'][$i];
			}
//			for($i=1; $i<=3; $i++){
//				if($formData['OthersApplyDayType'.$i]){
//					$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['icms']['PN']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['icms']['PN']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
//				}
//			}
			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
		else{
			foreach($dayTypeArr as $aDayType){
	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
	//			$dayTypeOption .=" ";
				$dayTypeOption .= $Lang['Admission']['icms'][$classLevel]['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
			}
			 $dayTypeOption .='('.$Lang['Admission']['icms']['msg']['applyDayTypeHints'].')';
		}
		//------Get the day type of the class level [END]
		
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		//Section A of the form for ICMS
		$x .= '<h1><b>'.$Lang['Admission']['sectiona'].'</b> '.$Lang['Admission']['campusnprogram'].'</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['schoolYearOption'].'</td>
				<td '.($IsConfirm?'':'colspan="3"').'>
				<div id="SchoolYearOption">'.getAcademicYearByAcademicYearID($formData['SchoolYearID']).'</div></td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['icms']['selectcampus'].'</td>
				<td '.($IsConfirm?'':'colspan="3"').'>'.$Lang['Admission']['icms']['tinhaucampus'].'</td>
			</tr>
			'.($IsConfirm?'<tr><td class="field_title">'.$Lang['Admission']['selectprogram'].'</td><td>'.$classLevel.'</td></tr>':'').'
			<tr>	
				<td class="field_title">'.$star.$Lang['Admission']['icms']['applyDayType'].'</td>
				<td '.($IsConfirm?'':'colspan="3"').'>
				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
			</tr>
			</table>';
			
		$x .= '<h1><b>'.$Lang['Admission']['sectionb'].'</b> '.$Lang['Admission']['studentInfo'].'</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['studentssurname'].'</td>
				<td>'.($IsConfirm?$formData['studentssurname']:'<input name="studentssurname" type="text" id="studentssurname" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['gender'].'</td>
				<td>';
				if($IsConfirm){
					$x .= $Lang['Admission']['genderType'][$formData['StudentGender']];
				}
				else{
					$x .=$this->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', '0','',$Lang['Admission']['genderType']['M']).'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', '0','',$Lang['Admission']['genderType']['F']);
				}
				
				$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['firstname'].'</td>
				<td>'.($IsConfirm?$formData['studentsfirstname']:'<input name="studentsfirstname" type="text" id="studentsfirstname" class="textboxtext" />').'</td>
				<td class="field_title">'.$Lang['Admission']['icms']['nameinchinese'].'</td>
				<td>'.($IsConfirm?$formData['StudentChiName']:'<input name="StudentChiName" type="text" id="StudentChiName" class="textboxtext" maxlength="10" size="15"/>').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateofbirth'].'</td>
				<td>
				'.($IsConfirm?$formData['StudentDateOfBirth']:'<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)').'</td>
				<td colspan="2">
					<table style="font-size: 13px;padding:0;border:0">
						<tr>
							<td class="field_title">'.$star.$Lang['Admission']['placeofbirth'].'</td>
				<td>'.($IsConfirm?$formData['StudentPlaceOfBirth']:'<input name="StudentPlaceOfBirth" type="text" id="StudentPlaceOfBirth" class="textboxtext" />').'</td>
						</tr>
						<tr><td class="field_title">'.$star.$Lang['Admission']['icms']['nationality'].'</td>
							<td>'.($IsConfirm?$formData['StudentProvince']:'<input name="StudentProvince" type="text" id="StudentProvince" class="textboxtext" />').'</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['homeaddress'].'</td>
				<td colspan="3">'.($IsConfirm?$formData['StudentHomeAddress']:'<input type="text" name="StudentHomeAddress" class="textboxtext" id="StudentHomeAddress" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['homenumber'].'</td>
				<td >'.($IsConfirm?$formData['StudentHomePhoneNo']:'<input name="StudentHomePhoneNo" type="text" id="StudentHomePhoneNo" class="textboxtext" maxlength="8" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['emailaddress'].'</td>
				<td >'.($IsConfirm?$formData['StudentEmail']:'<input name="StudentEmail" type="text" id="StudentEmail" class="textboxtext" />').'</td>
			</tr>	
			<tr>
				<td class="field_title">'.$Lang['Admission']['icms']['CurBSName'].'</td>
				<td>'.($IsConfirm?$formData['CurBSName']:'<input name="CurBSName" type="text" id="CurBSName" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['langspokenathome'].'</td>
				<td>'.($IsConfirm?$formData['langspokenathome']:'<input name="langspokenathome" type="text" id="langspokenathome" class="textboxtext" />').'</td>
			</tr>
			<!--<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].'</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/><br />
					<em>('.$Lang['Admission']['msg']['personalPhotoFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)</em>
					<br />').'
				</td>
			</tr>-->
		</table>';
		
		return $x;
	}
	
	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1><b>'.$Lang['Admission']['sectionc'].'</b> '.$Lang['Admission']['icms']['PGInfo'].'</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title"><b>'.$star.$Lang['Admission']['icms']['fatherssurname'].'</b></td>
				<td>'.($IsConfirm?$formData['fatherssurname']:'<input name="fatherssurname" type="text" id="fatherssurname" class="textboxtext" />').'</td>
				<td class="field_title"><b>'.$star.$Lang['Admission']['icms']['fathersfirstname'].'</b></td>
				<td>'.($IsConfirm?$formData['fathersfirstname']:'<input name="fathersfirstname" type="text" id="fathersfirstname" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['mobile'].'</td>
				<td>'.($IsConfirm?$formData['G1MobileNo']:'<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" maxlength="8" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['nativelanguage'].'</td>
				<td>'.($IsConfirm?$formData['fathersnativelanguage']:'<input name="fathersnativelanguage" type="text" id="fathersnativelanguage" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['occupation'].'</td>
				<td>'.($IsConfirm?$formData['G1Occupation']:'<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['worknumber'].'</td>
				<td>'.($IsConfirm?$formData['G1CompanyNo']:'<input name="G1CompanyNo" type="text" id="G1CompanyNo" class="textboxtext" maxlength="8" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['workaddress'].'</td>
				<td colspan="3">'.($IsConfirm?$formData['G1CompanyAddress']:'<input name="G1CompanyAddress" type="text" id="G1CompanyAddress" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title"><b>'.$star.$Lang['Admission']['icms']['motherssurname'].'</b></td>
				<td>'.($IsConfirm?$formData['motherssurname']:'<input name="motherssurname" type="text" id="motherssurname" class="textboxtext" />').'</td>
				<td class="field_title"><b>'.$star.$Lang['Admission']['icms']['mothersfirstname'].'</b></td>
				<td>'.($IsConfirm?$formData['mothersfirstname']:'<input name="mothersfirstname" type="text" id="mothersfirstname" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['mobile'].'</td>
				<td>'.($IsConfirm?$formData['G2MobileNo']:'<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" maxlength="8" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['nativelanguage'].'</td>
				<td>'.($IsConfirm?$formData['mothersnativelanguage']:'<input name="mothersnativelanguage" type="text" id="mothersnativelanguage" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['occupation'].'</td>
				<td>'.($IsConfirm?$formData['G2Occupation']:'<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['worknumber'].'</td>
				<td>'.($IsConfirm?$formData['G2CompanyNo']:'<input name="G2CompanyNo" type="text" id="G2CompanyNo" class="textboxtext" maxlength="8" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['workaddress'].'</td>
				<td colspan="3">'.($IsConfirm?$formData['G2CompanyAddress']:'<input name="G2CompanyAddress" type="text" id="G2CompanyAddress" class="textboxtext" />').'</td>
			</tr>
		</table>';
		
//		$x .= '<h1>'.$Lang['Admission']['PGInfo'].'</h1>
//				<table class="form_table" style="font-size: 13px">
//				<tr>
//					<td>&nbsp;</td>
//					<td class="form_guardian_head" style="text-align:center">1</td>
//					<td class="form_guardian_head" style="text-align:center">2</td>
//				</tr>
//				<tr>
//					<td class="field_title">'.$Lang['Admission']['name'].'</td>
//					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyname1']:'<input name="accompanyname1" type="text" id="accompanyname1" class="textboxtext" />').'</td>
//					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyname2']:'<input name="accompanyname2" type="text" id="accompanyname2" class="textboxtext" />').'</td>
//				</tr>
//				<tr>
//					<td class="field_title">'.$Lang['Admission']['relationship'].'</td>
//					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyrelationship1']:'<input name="accompanyrelationship1" type="text" id="accompanyrelationship1" class="textboxtext" />').'</td>
//					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyrelationship2']:'<input name="accompanyrelationship2" type="text" id="accompanyrelationship2" class="textboxtext" />').'</td>
//				</tr>
//				<tr>
//					<td class="field_title">'.$Lang['Admission']['icms']['mobilenumber'].'</td>
//					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanymobile1']:'<input name="accompanymobile1" type="text" id="accompanymobile1" class="textboxtext" />').'</td>
//					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanymobile2']:'<input name="accompanymobile2" type="text" id="accompanymobile2" class="textboxtext" />').'</td>
//				</tr>
//				
//			</table>';
			
		return $x;
	}
	
	function getOthersForm($IsConfirm=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		
		/*$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['icms']['PN']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['icms']['PN']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
		else{
			foreach($dayTypeArr as $aDayType){
	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
	//			$dayTypeOption .=" ";
				$dayTypeOption .= $Lang['Admission']['icms']['PN']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
			}
			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
		}*/
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1><b>'.$Lang['Admission']['sectiond'].'</b> '.$Lang['Admission']['bginfo'].'</h1>
			<table class="form_table" style="font-size: 13px">';
//		$x .= '<tr>
//				<td class="field_title">'.$star.$Lang['Admission']['familyStatus'].'</td>
//				<td colspan="3"><!--<input name="OthersFamilyStatus" type="text" id="OthersFamilyStatus" class="textboxtext" />-->
//				<table style="font-size: 13px">
//					<tr>
//						<td>'.$Lang['Admission']['elderBrother'].' ('.$Lang['Admission']['person'].')</td><td>'.$Lang['Admission']['elderSister'].' ('.$Lang['Admission']['person'].')</td><td>'.$Lang['Admission']['youngerBrother'].' ('.$Lang['Admission']['person'].')</td><td>'.$Lang['Admission']['youngerSister'].' ('.$Lang['Admission']['person'].')</td>
//					</tr>
//					<tr>
//						<td>'.($IsConfirm?$formData['OthersFamilyStatus_EB']:$this->Get_Number_Selection('OthersFamilyStatus_EB', '0', '5','0')).'</td>
//						<td>'.($IsConfirm?$formData['OthersFamilyStatus_ES']:$this->Get_Number_Selection('OthersFamilyStatus_ES', '0', '5','0')).'</td>
//						<td>'.($IsConfirm?$formData['OthersFamilyStatus_YB']:$this->Get_Number_Selection('OthersFamilyStatus_YB', '0', '5','0')).'</td>
//						<td>'.($IsConfirm?$formData['OthersFamilyStatus_YS']:$this->Get_Number_Selection('OthersFamilyStatus_YS', '0', '5','0')).'</td>
//					</tr>
//				</table>
//				</td>
//			</tr>
//			<tr>
//				<td class="field_title">'.$star.$Lang['Admission']['dateOfEntry'].'</td>
//				<td><table style="font-size: 13px">
//					<tr><td width="40px">('.$Lang['General']['SchoolYear'].')</td><td>'.($IsConfirm?$admission_year:$admission_year).'</td></tr>
//					<tr><td width="40px">('.$Lang['Admission']['month'].')</td><td>'.($IsConfirm?$formData['OthersApplyMonth']:$this->Get_Number_Selection('OthersApplyMonth', '1', '12')).'</td></tr>
//					</table>
//				<!--'.$this->GET_DATE_PICKER('OthersApplyDate').'--></td>
//				<td class="field_title">'.$star.$Lang['Admission']['applyTerm'].'</td>
//				<td><!--<input name="OthersApplyTerm" type="text" id="OthersApplyTerm" class="textboxtext" />-->';
//				if($IsConfirm){
//					//$x .= "Term ".$formData['OthersApplyTerm'];
//					$x .=$Lang['Admission']['Term'][$formData['OthersApplyTerm']];
//				}
//				else{
//					$x .=$this->Get_Radio_Button('OthersApplyTerm1', 'OthersApplyTerm', '1', '0','',$Lang['Admission']['Term'][1]).' '
//					.$this->Get_Radio_Button('OthersApplyTerm2', 'OthersApplyTerm', '2', '0','',$Lang['Admission']['Term'][2]);
//				}
//			$x .='</td>
//			</tr>';
//			<tr>
//				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>
//				<td '.($IsConfirm?'':'colspan="3"').'>
//				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
//				'.($IsConfirm?'<td class="field_title">'.$Lang['Admission']['applyLevel'].'</td><td>'.$classLevel.'</td>':'').'
//			</tr>
			$x .='<tr>
				<td class="field_title" colspan="4">'.$Lang['Admission']['icms']['listprevschool'].'</td>
			</tr>
			<tr>
				<td colspan="4">'.($IsConfirm?$formData['listprevschool']:'<input name="listprevschool" type="text" id="listprevschool" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title" colspan="4">'.$Lang['Admission']['icms']['childdescription'].'</td>
			</tr>
			<tr>
				<td colspan="4">'.($IsConfirm?$formData['childdescription']:'<input name="childdescription" type="text" id="childdescription" class="textboxtext" />').'</td>
			</tr>
			</table>';
			
			$x .= '<h1><b>'.$Lang['Admission']['sectione'].'</b> '.$Lang['Admission']['healthinfo'].'</h1>';
			$x .= '<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title" colspan="4">'.$star.$Lang['Admission']['icms']['childhealth'].'</td>
			</tr>
			<tr>
				<td colspan="4">'.($IsConfirm?$formData['childhealth']:'<input name="childhealth" type="text" id="childhealth" class="textboxtext" />').'</td>
			</tr>
			</table>';
			
			$x .= '<h1><b>'.$Lang['Admission']['sectionf'].'</b> '.$Lang['Admission']['parentexpectation'].'</h1>';
			$x .= '<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title" colspan="4">'.$Lang['Admission']['icms']['yourwish'].'</td>
			</tr>
			<tr>
				<td colspan="4">'.($IsConfirm?$formData['yourwish']:'<input name="yourwish" type="text" id="yourwish" class="textboxtext" />').'</td>
			</tr>
			</table>';
		
		return $x;
	}
	
//	function getOthersForm($IsConfirm=0){ //should be changed the method of selecting program
//		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
//		
//		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
//
//		$applicationSetting = $lac->getApplicationSetting();
//		
//		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
//		$dayTypeArr = explode(',',$dayType);
//		
//		$dayTypeOption="";
//		
//		if($IsConfirm){
//			$classLevel = $lac->getClassLevel($formData['sus_status']);
//			$classLevel = $classLevel[$formData['sus_status']];
//
//			for($i=1; $i<=3; $i++){
//				if($formData['OthersApplyDayType'.$i]){
//					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
//				}
//			}
//
//		}
//		else{
//			foreach($dayTypeArr as $aDayType){
//				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
//			}
//			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
//		}
//		
//		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
//		
//		$x = '<h1>'.$Lang['Admission']['icms']['programselection'].'</h1>
//			<table class="form_table" style="font-size: 13px">
//			
//			<tr>
//				<td class="field_title">'.$star.$Lang['Admission']['dateOfEntry'].'</td>
//				<td><table style="font-size: 13px">
//					<tr><td width="40px">('.$Lang['General']['SchoolYear'].')</td><td>'.($IsConfirm?$admission_year:$admission_year).'</td></tr>
//					<tr><td width="40px">('.$Lang['Admission']['month'].')</td><td>'.($IsConfirm?$formData['OthersApplyMonth']:$this->Get_Number_Selection('OthersApplyMonth', '1', '12')).'</td></tr>
//					</table>
//				<!--'.$this->GET_DATE_PICKER('OthersApplyDate').'--></td>
//				<td class="field_title">'.$star.$Lang['Admission']['applyTerm'].'</td>
//				<td>';
//				if($IsConfirm){
//					$x .=$Lang['Admission']['Term'][$formData['OthersApplyTerm']];
//				}
//				else{
//					$x .=$this->Get_Radio_Button('OthersApplyTerm1', 'OthersApplyTerm', '1', '0','',$Lang['Admission']['Term'][1]).' '
//					.$this->Get_Radio_Button('OthersApplyTerm2', 'OthersApplyTerm', '2', '0','',$Lang['Admission']['Term'][2]);
//				}
//			$x .='</td>
//			</tr>
//			<!--<tr>
//				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>
//				<td '.($IsConfirm?'':'colspan="3"').'>
//				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
//				'.($IsConfirm?'<td class="field_title">'.$Lang['Admission']['applyLevel'].'</td><td>'.$classLevel.'</td>':'').'
//			</tr>-->';
//			
//			$class_level_selection = "";
//			//To get the class level which is available
//			if($applicationSetting){
//				$hasClassLevelApply = 0;
//				foreach($applicationSetting as $key => $value){
//					//debug_pr($value['StartDate']);
//					if(date('Y-m-d') >= $value['StartDate'] && date('Y-m-d') <= $value['EndDate']){
//						$hasClassLevelApply = 1;
//						$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="" />
//							<label for="status_'.$key.'">'.$value['ClassLevelName'].'</label>';
//					}			
//				}
//				if($hasClassLevelApply == 0){
//					$class_level_selection .='<fieldset class="warning_box">
//												<legend>'.$Lang['Admission']['warning'].'</legend>
//												<ul>
//													<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
//												</ul>
//											</fieldset>';
//				}
//			}
//			else{ //Henry 20131107
//				$class_level_selection .='<fieldset class="warning_box">
//												<legend>'.$Lang['Admission']['warning'].'</legend>
//												<ul>
//													<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
//												</ul>
//											</fieldset>';
//			}
//			
//			//loop the class level here...
//			$dayTypeOption = '';
//			foreach($applicationSetting as $key => $value){
//				$classLevelNameAry[] = array($key,$value['ClassLevelName']);
//			}
//			for($i=0;$i<count($applicationSetting);$i++){
//				$dayTypeOption .=$kis_lang['Admission']['Option'].' '.($i+1).' '.$this->GET_SELECTION_BOX($classLevelNameAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",(count($applicationSetting)==1?'':$kis_lang['Admission']['Nil']));
//				//if(($i+1)%3 == 0)
//					$dayTypeOption .= '<br/>';
//			}
//			$dayTypeOption .='<br/>('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
//			$x .='<tr>
//				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>
//				<td '.($IsConfirm?'':'colspan="3"').'>'.$dayTypeOption.'</td></tr>';
//			//loop the class level here...
//			
//			$x .='</table>';
//		
//		return $x;
//	}

	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1><b>'.$Lang['Admission']['sectiong'].'</b> '.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
		$x .= '<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].'</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/><br />
					<em>('.$Lang['Admission']['msg']['personalPhotoFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)</em>
					<br />').'
				</td>
			</tr>';
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$star.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
				  </tr>';
		}
			
//		$x .='<tr>
//					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
//					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
//			</tr>';
//		$x .='<tr>
//					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
//					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
//			</tr>';
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
				
			</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	/*
	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1>'.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
			
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
				
			</div>
			<p class="spacer"></p>';

		}
		return $x;
	}
	*/
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />

			</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $Lang, $lac, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			$x .='<div class="admission_complete_msg"><h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
			$x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
//	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID=''){
//		global $Lang, $lac;
//		$x = '<div class="admission_board">';
//		$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink($schoolYearID, $ApplicationID).'\',\'_blank\');"></input></span></h1>';        
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
//		$x .= '</div>';
//		$x .= '<div class="edit_bottom">
//				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Admission']['finish'].'" />
//			</div>
//			<p class="spacer"></p></div>';
//		return $x;
//	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.en.php");
		if($ApplicationID){
			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);         
		}
		else{
			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
        }
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent;

		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';           
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		$x .= '<div class="edit_bottom">
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Back'].'" />
			</div>
			<p class="spacer"></p></div>';
		return $x;
	}

	function getPrintPageContent($schoolYearID,$applicationID, $type=""){ //using $type="teacher" if the form is print from teacher
		global $PATH_WRT_ROOT,$Lang,$kis_lang;
		include_once($PATH_WRT_ROOT."lang/admission_lang.en.php");
		$lac = new admission_cust();
		if($applicationID != ""){
		//get student information
		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
		//debug_pr($studentInfo);
		foreach($parentInfo as $aParent){
			if($aParent['type'] == 'F'){
				$fatherInfo = $aParent;
			}
			else if($aParent['type'] == 'M'){
				$motherInfo = $aParent;
			}
			else if($aParent['type'] == 'G'){
				$guardianInfo = $aParent;
			}
		}
		
		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
		if($_SESSION['UserType']==USERTYPE_STAFF){
			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
//			if(!is_date_empty($remarkInfo['interviewdate'])){
//				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
//				list($y,$m,$d) = explode('-',$date);
//				if($hour>12){
//					$period = '下午';
//					$hour -= 12;
//				}elseif($hour<12){
//					$period = '上午';
//				}else{
//					$period = '中午';
//				}
//				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
//				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
//				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
//			}else{
//				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
//			上午／下午____時____分';
//			}
		}
//		else{
//				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
//			上午／下午____時____分';
//		}
		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
		$personalPhotoPath = $attachmentList['personal_photo']['link'];
		$classLevel = $lac->getClassLevel();
		
//		debug_pr($studentInfo);
//		debug_pr($fatherInfo);
//		debug_pr($motherInfo);
//		debug_pr($guardianInfo);
//		debug_pr($othersInfo);
		
		for($i=1; $i<=3; $i++){
				if($othersInfo['ApplyDayType'.$i] != 0){
					//$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
					$dayTypeOption .= "<br/>(option ".$i.") ".$Lang['Admission']['icms'][$classLevel[$othersInfo['classLevelID']]]['TimeSlot'][$othersInfo['ApplyDayType'.$i]];
				}
			}
		
		//Header of the page
		$x = '<div class="input_form" style="width:720px;margin:auto;">';
		$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=160&height=40&format=PNG'></div>";
		//$x .= '<h3 align="center">Island Children\'s Montessori Kindergarten</h3>';
		$x .= '<h2 align="center" style="margin-bottom:5px">Maria Montessori Educational Organisation</h2>';
		
		//$x .= '<h5 align="center">International ◆ Preschool ◆ Education</h5>';
//		$x .= '<h6 align="center" style="margin-top:5px">Island Children\'s Montessori School ▪ Island Children\'s Montessori International Nursery ▪ Island Children\'s Montessori Child Care Centre<br/>
//				Island Children\'s Montessori Kindergarten (Tin Hau) ▪ Island Children\'s Montessori Kindergarten (Lei King Wan) ▪ Island Children\'s Montessori House</h6>';
		$x .= '<h4 align="center" style="margin-top:5px">Island Children\'s Montessori Kindergarten (Tin Hau)</h6>';
		

		//the header
		$x .= '<div style="float:right;position:relative;top:-18px; width:115px; height:135px;"><img src="'.$personalPhotoPath.'" style="width:auto;height:auto;"></div>';
		
		$x .= '<h3 align="center" style="padding-left: 100px;"><u>Application Form</u></h3>';
		
		//the staff only
		$x .= '<table style="width:600px;" class="tg-table-plain">
					<tr>
						<td width="20%" style="padding:2px">Application Number</td>
						<td width="20%" style="padding:2px">Date received</td>
						<td width="20%" style="padding:2px">Student ID</td>
						<td width="20%" style="padding:2px">Start Date</td>
						<td width="20%" style="padding:2px">Notes</td>
					</tr>
					<tr>
						<td style="padding:2px"><b>'.$othersInfo['applicationID'].'</b></td>
						<td style="padding:2px"><b>'.substr($othersInfo['DateInput'], 0, -9).'</b></td>
						<td style="padding:2px"><b>To be confirmed</b></td>
						<td style="padding:2px"><b>To be confirmed</b></td>
						<td style="padding:2px"><b>'.$remarkInfo['remark'].'</b></td>
					</tr>
					<tr>
						<td colspan="5">
							<input type="checkbox" onclick="return false">Photographs</input>
							<input type="checkbox" onclick="return false">Application fee</input>
							<input type="checkbox" onclick="return false">Birth Certificate</input>
							<input type="checkbox" onclick="return false">Immunization</input>
							<input type="checkbox" onclick="return false">Visa</input>
						</td>
					</tr>
				</table>';
		
		//section a
		$x .= '<div class="section_title" style="margin-bottom:0"><b>Section A</b> Campus and Program</div>';
		
		$x .= '<table align="center" class="tg-table-plain tg-table-plain2">
					<tr>
						<td width="30%" style="text-align:right;border:0"><b>School year applied for</b></td>
						<td style="border:0">'.getAcademicYearByAcademicYearID($othersInfo['schoolYearId']).'</b></td>
					</tr>
					<tr>
						<td style="text-align:right;border:0"><b>Select campus</b></td>
						<td  style="border:0">'.$Lang['Admission']['icms']['tinhaucampus'].'</td>
					</tr>
					<tr>
						<td style="text-align:right;border:0"><b>Select program and session</b><!--<br/>(if you are applying for Pre-Nursery program, please indicate 1st and 2nd choice)--></td>
						<td  style="border:0"><u><b>'.$classLevel[$othersInfo['classLevelID']].'</b></u>
							'.$dayTypeOption.'
						</td>
					</tr>
				</table>';
		
		//section b
		$stuNameArr = explode(',',$studentInfo['student_name_en']);
		
		$x .= '<div class="section_title"><b>Section B</b> Student Information</div>';
		
		$x .= '<table align="center" class="tg-table-plain">
				  <tr>
					<td colspan="3"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$stuNameArr[0].'</td></tr><tr><td style="border:0;font-size: 15px;">Surname</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td></tr><tr><td style="border:0">Gender</td></tr></table></td>
				  </tr>
				  <tr class="tg-even">
				    <td colspan="3"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$stuNameArr[1].'</td></tr><tr><td style="border:0">First Name</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.($studentInfo['student_name_b5']?$studentInfo['student_name_b5']:'&nbsp;').'</td></tr><tr><td style="border:0">Name in Chinese (if applicable)</td></tr></table></td>
				  </tr>
				  <tr>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['dateofbirth'].'</td></tr><tr><td style="border:0">Date of birth (YYYY-MM-DD)</td></tr></table></td>
				    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['placeofbirth'].'</td></tr><tr><td style="border:0">Place of Birth</td></tr></table></td>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['province'].'</td></tr><tr><td style="border:0">Nationality</td></tr></table></td>
				  </tr>
				  <tr class="tg-even">
				    <td colspan="4"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['homeaddress'].'</td></tr><tr><td style="border:0">Home Address</td></tr></table></td>
				  </tr>
				  <tr class="tg-even">
				    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['homephoneno'].'</td></tr><tr><td style="border:0">Home Telephone Number</td></tr></table></td>
				    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['email'].'</td></tr><tr><td style="border:0">Email Address</td></tr></table></td>
				  </tr>
				  <tr class="tg-even">
				    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$othersInfo['CurBSName'].'</td></tr><tr><td style="border:0">Name of Sibling Currently Attending ICMS</td></tr></table></td>
				    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['langspokenathome'].'</td></tr><tr><td style="border:0">Language Spoken at Home</td></tr></table></td>
				  </tr>
		
				</table>';
		
		//section c
		$fatherEngName = explode(',',$fatherInfo['parent_name_en']);
		$motherEngName = explode(',',$motherInfo['parent_name_en']);
		
		$x .= '<div class="section_title"><b>Section C</b> Parent Information</div>';
		
		$x .= '<table align="center" class="tg-table-plain" style="page-break-after:always">
				 <tr>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0;font-size:15px">'.$fatherEngName[0].'</td></tr><tr><td style="border:0">Father\'s Surname</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0;font-size:15px">'.$fatherEngName[1].'</td></tr><tr><td style="border:0">Father\'s First Name</td></tr></table></td>
				 </tr>
				 <tr>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['mobile'].'</td></tr><tr><td style="border:0">Mobile Number</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['nativelanguage'].'</td></tr><tr><td style="border:0">Native Language</td></tr></table></td>
				</tr>
				<tr>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['occupation'].'</td></tr><tr><td style="border:0">Occupation</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['companyphone'].'</td></tr><tr><td style="border:0">Work Number</td></tr></table></td>
				</tr>
				<tr>
					<td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['companyaddress'].'</td></tr><tr><td style="border:0">Work Address</td></tr></table></td>
				</tr>
		
				 <tr>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0;font-size:15px">'.$motherEngName[0].'</td></tr><tr><td style="border:0">Mother\'s Surname</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0;font-size:15px">'.$motherEngName[1].'</td></tr><tr><td style="border:0">Mother\'s First Name</td></tr></table></td>
				 </tr>
				 <tr>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['mobile'].'</td></tr><tr><td style="border:0">Mobile Number</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['nativelanguage'].'</td></tr><tr><td style="border:0">Native Language</td></tr></table></td>
				</tr>
				<tr>
					<td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['occupation'].'</td></tr><tr><td style="border:0">Occupation</td></tr></table></td>
				    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['companyphone'].'</td></tr><tr><td style="border:0">Work Number</td></tr></table></td>
				</tr>
				<tr>
					<td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['companyaddress'].'</td></tr><tr><td style="border:0">Work Address</td></tr></table></td>
				</tr>
		
				</table><br/>';
		
		//section d
		$x .= '<div class="section_title"><b>Section D</b> Background Information</div>';
		
		$x .= '<p>Please list the last three (3) schools or playgroups that your child has attended and for how long?</p>';
		$x .= '<table class="tg-table-plain" width="100%">
				<tr><td class="answer" style="padding:5px">'.($studentInfo['lastschool']?$studentInfo['lastschool']:'&nbsp;').'</tr></td>
			</table>';
		$x .= '<p>Please give a brief description of the strengths, work habits, concentration level and independence level of your child.</p>';
		$x .= '<table class="tg-table-plain" width="100%">
				<tr><td class="answer" style="padding:5px">'.($othersInfo['childdescription']?$othersInfo['childdescription']:'&nbsp;').'</tr></td>
			</table>';
			
		//section e
		$x .= '<div class="section_title"><b>Section E</b> Health Information</div>';
		
		$x .= '<p>Please provide information regarding your child\'s health condition, including: food allergies and reactions, special learning requirement or needs, dietary requirements and medication(s) taken on a regular basis.</p>';
		$x .= '<table class="tg-table-plain" width="100%">
				<tr><td class="answer" style="padding:5px">'.($othersInfo['childhealth']?$othersInfo['childhealth']:'&nbsp;').'</tr></td>
			</table>';
		
		//section f
		$x .= '<div class="section_title"><b>Section F</b> Parent Expectation</div>';
		
		$x .= '<p>Please tell us why you wish to enroll your child in a Montessori environment.</p>';
		$x .= '<table class="tg-table-plain" width="100%">
				<tr><td class="answer" style="padding:5px">'.($othersInfo['yourwish']?$othersInfo['yourwish']:'&nbsp;').'</tr></td>
			</table>';
			
		$x .= '</div>';

			return $x;
		}
		else
			return false;
	}
	function getPrintPageCss(){
		return '<style type="text/css">
.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
.tg-table-plain td { border: 1px #555 solid; padding: 0px; vertical-align: top; font-size: 13px;}
.tg-table-plain2 td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
.tg-table-plain .answer {font-size: 15px;font-weight: bold;}
.bottom_text td {vertical-align:bottom;text-align:left;height:50px;}
.center_text td {text-align:center;}
.print_field_title { background: #EFEFEF}
.print_field_title_main { background:#D7D7D7}
.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
.print_field_row1 { width:120px}
.print_field_row2 { width:28px}
.print_field_row3 { width:80px}
.print_field_title_remark { background:#B9B9B9; width:290px;}
.print_field_title_parent{ width:181px;}
div.section_title {background-color: #B9B9B9; width:710px; margin:20px 0px; padding:5px;border:1px #555 solid;margin-bottom:10px;}
@media print
{
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
p, table {font-size: 13px;}

</style>';
	}
	
}
?>