<script type="text/javascript">
var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	var isValid = true;
	//isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	//For debugging only
	//return true;
	
//	form1.hidden_data.value = $("form").serialize();
//	form1.hidden_file.value = form1.StudentPersonalPhoto.files[0];
//	alert(form1.hidden_data.value);
	
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	
	//var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
	if(form1.StudentPersonalPhoto.files[0]){
	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	//var hasPG = 1;
	
	//The new input validation
	if(form1.studentssurname.value==''){
		return false;
	}
	else if(form1.givennames.value==''){
		return false;
	}
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		return false;
	}
	else if(form1.schoolcurrentlyattending.value==''){
		return false;
	}
	else if(form1.homeaddress.value==''){
		return false;
	}
	else if(form1.StudentPersonalPhoto.value==''){
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		return false;
	} else if(studentPhotoSize > maxFileSize){
		return false;
	}
	else if(form1.OthersApplyMonth.value==''){
		return false;
	} else if($('input:radio[name=OthersApplyTerm]:checked').val() == null){
		return false;
	} else if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	//else {
		return true;
	//}
}

/*function check_docs_upload2(form1) {

	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		return false;
	} else if(otherFileSize > maxFileSize){
		return false;
	} else if(form1.OtherFile1.value==''){
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		return false;
	} else if(otherFileSize1 > maxFileSize){
		return false;
	} else  {
		return true;
	}
}*/
function check_docs_upload2(form1) {
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
		var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		
		if(otherFileVal==''){
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(otherFileSize > maxFileSize){
			return false;
		}
	}
	
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
//	if(current == 'step_instruction' && page == 'step_input_form'){
//		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
//			   clearTimeout(timer);
//			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
//		   <?}?>
//		   /* Clear result div*/
//		   $("#DayTypeOption").html('');
//		
//		   /* Get some values from elements on the page: */
//		   var values = $("#form1").serialize();
//		   
//		    /* Send the data using post and put the results in a div */
//		   $.ajax({
//		       url: "ajax_get_class_selection.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!");
//		           $("#DayTypeOption").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });
//		   
//		/* get the birthday range of the form level */
//			   $.ajax({
//			       url: "ajax_get_bday_range.php",
//			       type: "post",
//			       data: values,
//			       success: function(data){
//			           //alert("debugging: The classlevel is updated!");
//			           dOBRange = data.split(",");
//			       },
//			       error:function(){
//			           //alert("failure");
//			           $("#result").html('There is error while submit');
//			       }
//			   });
//		   
//	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){
	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	return confirm('<?=$Lang['Admission']['msg']['suresubmit']?>');
}

function check_choose_class(form1) {
	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>");
				return false;
			}
		}

	<?}?>
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	 else  {
		return true;
	}

}

function check_input_info(form1) {
	//For debugging only
	//return true;
	
//	form1.hidden_data.value = $("form").serialize();
//	form1.hidden_file.value = form1.StudentPersonalPhoto.files[0];
//	alert(form1.hidden_data.value);
	
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	
	//var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
	if(form1.StudentPersonalPhoto.files[0]){
	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	//var hasPG = 1;
	
	//The new input validation
	if(form1.studentssurname.value==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>");
		form1.studentssurname.focus();
		return false;
	}
	else if(form1.givennames.value==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>");
		form1.givennames.focus();
		return false;
	}
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>");	
		form1.StudentGender[0].focus();
		return false;
	}
	else if(form1.schoolcurrentlyattending.value==''){
		alert("<?=$Lang['Admission']['icms']['msg']['enterschoolcurrentlyattending']?>");	
		form1.schoolcurrentlyattending.focus();
		return false;
	}
	else if(form1.homeaddress.value==''){
		alert("<?=$Lang['Admission']['msg']['enterhomeaddress']?>");	
		form1.homeaddress.focus();
		return false;
	}
	else if(form1.StudentPersonalPhoto.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	}
	else if(form1.OthersApplyMonth.value==''){
		alert("<?=$Lang['Admission']['msg']['selectApplyMonth']?>");
		form1.OthersApplyMonth.focus();
		return false;
	} else if($('input:radio[name=OthersApplyTerm]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['enterapplyTerm']?>");
		form1.OthersApplyTerm[0].focus();
		return false;
//	} else if($('input:radio[name=sus_status]:checked').val() == null){
//		alert("<?=$Lang['Admission']['msg']['selectclass']?>");
//		if(form1.sus_status[0])
//			form1.sus_status[0].focus();
//		else
//			form1.sus_status.focus();
//		return false;
	}
	//else {
		return true;
	//}
}

/*function check_docs_upload(form1) {
	//For debugging only
	//return true;
	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadbirthcert']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(form1.OtherFile1.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadimmunisationrecord']?>");
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileSize1 > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else  {
		return true;
	}
}*/
function check_docs_upload(form1) {
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
		var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		
		if(otherFileVal==''){
			alert("<?=$Lang['Admission']['msg']['uploadfile']?>");
			file_element.focus();
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
			file_element.focus();
			return false;
		} else if(otherFileSize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
			file_element.focus();
			return false;
		}
	}
	
	return true;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}
//function startUpload(){
//      document.getElementById('f1_upload_process').style.visibility = 'visible';
//      document.getElementById('f1_upload_form').style.visibility = 'hidden';
//      return true;
//}
//Henry modifying 20131028
//function stopUpload(temp_folder_name){
//		document.getElementById('tempFolderName').value = temp_folder_name;
//		
//		 var values = $("#form1").serialize();
//			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
//			var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
//			var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
//			values+=studentPersonalPhoto;
//			values+=otherFile;
//			values+=otherFile1;
//		$.ajax({
//		       url: "ajax_get_confirm.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!"+values);
//		           $("#step_confirm").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });     
//      return true;  
//}
</script>