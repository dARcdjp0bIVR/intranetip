<?php
# modifying by: Henry

/********************
 * 
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_cust_base
 * 
 * Date		2014-01-02 [Henry]
 * 			created functions getExportHeader() and getExportData()
 * 
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_cust_base.php");

class admission_cust extends admission_cust_base{
	function admission_cust(){
		global $kis_lang, $UserID; //switch $lang for IP/EJ/KIS
		$this->libdb();
		$this->filepath = '/file/admission/';
		$this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
		$this->schoolYearID = $this->getNextSchoolYearID();
		$this->uid = $UserID;
		$this->classLevelAry = $this->getClassLevel();
	}
	
	function encrypt_attachment($file){
		list($filename,$ext) = explode('.',$file);
	    $timestamp = date("YmdHis");
	    return base64_encode($filename.'_'.$timestamp).'.'.$ext;
	}
	function getNextSchoolYearID(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		$SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField=array('AcademicYearID'),1);
		krsort($SchoolYearIDArr);
		
		$SchoolYearIDArr = array_values($SchoolYearIDArr);
		$currentSchoolYear = Get_Current_Academic_Year_ID();
		$key = array_search($currentSchoolYear, $SchoolYearIDArr);
		if($key>0){
    		return $SchoolYearIDArr[$key-1];
    	}else{
    		return false;
    	}
	}
	
	function insertApplicationAllInfo($libkis_admission, $Data, $ApplicationID){ //henry is modifying this function
		extract($Data);
		$Success = array();
		if($ApplicationID != ""){
			
			$Success[] = $this->insertApplicationStatus($libkis_admission, $ApplicationID, $SchoolYearID); //modified
			
			$Success[] = $this->insertApplicationStudentInfo($Data, $ApplicationID);
			
			//Pack father Info
			if(!empty($ApplicationID)){
					$parentInfoAry = array();
					$parentInfoAry['ApplicationID'] = $ApplicationID;
					$parentInfoAry['PG_TYPE'] = 'F';
					//$parentInfoAry['Relationship'] = ${'G'.($_key+1).'Relationship'};
					$parentInfoAry['EnglishName'] = $fatherssurname.','.$fathersfirstname;
					//$parentInfoAry['ChineseName'] = ${'G'.($_key+1).'ChineseName'};
					$parentInfoAry['JobTitle'] = $G1Occupation;
					//$parentInfoAry['Company'] = ${'G'.($_key+1).'CompanyName'};
					//$parentInfoAry['JobPosition'] = ${'G'.($_key+1).'JobPosition'};
					$parentInfoAry['OfficeAddress'] = $G1CompanyAddress;
					$parentInfoAry['OfficeTelNo'] = $G1CompanyNo;
					$parentInfoAry['Mobile'] = $G1MobileNo;
					$parentInfoAry['NativeLanguage'] = $fathersnativelanguage;
					$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
			}
			//Pack mother Info
			if(!empty($ApplicationID)){
					$parentInfoAry = array();
					$parentInfoAry['ApplicationID'] = $ApplicationID;
					$parentInfoAry['PG_TYPE'] = 'M';
					//$parentInfoAry['Relationship'] = ${'G'.($_key+1).'Relationship'};
					$parentInfoAry['EnglishName'] = $motherssurname.','.$mothersfirstname;
					//$parentInfoAry['ChineseName'] = ${'G'.($_key+1).'ChineseName'};
					$parentInfoAry['JobTitle'] = $G2Occupation;
					//$parentInfoAry['Company'] = ${'G'.($_key+1).'CompanyName'};
					//$parentInfoAry['JobPosition'] = ${'G'.($_key+1).'JobPosition'};
					$parentInfoAry['OfficeAddress'] = $G2CompanyAddress;
					$parentInfoAry['OfficeTelNo'] = $G2CompanyNo;
					$parentInfoAry['Mobile'] = $G2MobileNo;
					$parentInfoAry['NativeLanguage'] = $mothersnativelanguage;
					$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
			}
//			//Pack Parent Info 
//			foreach($this->pg_type as $_key => $_pgType){
//				if(!empty($ApplicationID)&&!empty(${'G'.($_key+1).'EnglishName'})){
//					$parentInfoAry = array();
//					$parentInfoAry['ApplicationID'] = $ApplicationID;
//					$parentInfoAry['PG_TYPE'] = $_pgType;
//					$parentInfoAry['Relationship'] = ${'G'.($_key+1).'Relationship'};
//					$parentInfoAry['EnglishName'] = ${'G'.($_key+1).'EnglishName'};
//					$parentInfoAry['ChineseName'] = ${'G'.($_key+1).'ChineseName'};
//					$parentInfoAry['JobTitle'] = ${'G'.($_key+1).'Occupation'};
//					$parentInfoAry['Company'] = ${'G'.($_key+1).'CompanyName'};
//					$parentInfoAry['JobPosition'] = ${'G'.($_key+1).'JobPosition'};
//					$parentInfoAry['OfficeAddress'] = ${'G'.($_key+1).'CompanyAddress'};
//					$parentInfoAry['OfficeTelNo'] = ${'G'.($_key+1).'CompanyNo'};
//					$parentInfoAry['Mobile'] = ${'G'.($_key+1).'MobileNo'};	
//					$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
//				}
//				
// 			}
			/*if($G1EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 1);
				
			if($G2EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 2);
				
			if($G3EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 3);*/
			
			$Success[] = $this->insertApplicationOthersInfo($Data, $ApplicationID);
			
			if(in_array(false,$Success)){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	function insertApplicationStatus($libkis_admission, $ApplicationID, $schoolYearID=""){ //added para $schoolYearID
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
     					SchoolYearID,
     					ApplicationID,
					    Status,
					    DateInput)
					VALUES (
						'".($schoolYearID?$schoolYearID:$this->schoolYearID)."',
						'".$ApplicationID."',
						'1',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	function updateApplicationStatus($data){
		global $UserID;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
				s.ReceiptID = '".$receiptID."',
     			s.ReceiptDate = '".$receiptdate."',
      			s.Handler = '".$handler."', 
      			s.InterviewDate = '".$interviewdate."',
				s.InterviewLocation = '".$interviewlocation."',
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."' 
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}
	function updateApplicationStatus2($data){ //for development only
		global $UserID;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."' 
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}  
	function updateApplicationStatusByIds($applicationIds,$status){
		global $UserID;
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.RecordID IN (".$applicationIds.")
    	";
    	return $this->db_db_query($sql);
	}	  	
   	function insertApplicationStudentInfo($Data, $ApplicationID){ //modified
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_STU_INFO (
						ApplicationID,
		   				 ChineseName,
		  				 EnglishName,   
		   				 Gender,
		  				 DOB,   
		   				 PlaceOfBirth,
		   				 Province,
						HomeTelNo,
						Address,
						Email,
						LastSchool,
						LangSpokenAtHome,
						DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$StudentChiName."',
						'".$studentssurname.','.$studentsfirstname."',
						'".$StudentGender."',
						'".$StudentDateOfBirth."',
						'".$StudentPlaceOfBirth."',
						'".$StudentProvince."',
						'".$StudentHomePhoneNo."',
						'".$StudentHomeAddress."',
						'".$StudentEmail."',
						'".$listprevschool."',
						'".$langspokenathome."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
    }
    
    function insertApplicationParentInfo($Data){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
	
		$sql = "INSERT INTO ADMISSION_PG_INFO (
					".$fieldname."
					DateInput
				)VALUES (
					".$fieldvalue."
					NOW()
				)";
		return $this->db_db_query($sql);
    }
     function updateApplicationParentInfo($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_PG_INFO SET
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		return $this->db_db_query($sql);
    }   
    function insertApplicationOthersInfo($Data, $ApplicationID){ //modified. for parent admission only
   		extract($Data);
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){
			
//   		}
//   		else{
   			//for ICMS only
   			$ApplyDayType1 = '';
   			$ApplyDayType2 = '';
   			$ApplyDayType3 = '';
   			for($i=1;$i<=3;$i++){
   				if(${'OthersApplyDayType'.$i} == 1){
   					$ApplyDayType1 = $i;
   				}
   				else if(${'OthersApplyDayType'.$i} == 2){
   					$ApplyDayType2 = $i;
   				}
   				else if(${'OthersApplyDayType'.$i} == 3){
   					$ApplyDayType3 = $i;
   				}
   			}
   			
			$sql = "UPDATE 
					ADMISSION_OTHERS_INFO 
				SET
				     CurBSName = '".$CurBSName."',
					 ApplyYear = '".$SchoolYearID."',
					 ApplyDayType1 = '".$ApplyDayType1."',
					 ApplyDayType2 = '".$ApplyDayType2."',
					 ApplyDayType3 = '".$ApplyDayType3."',
					 ApplyLevel = '".$sus_status."',
					 ChildDescription = '".$childdescription."',
					 ChildHealth = '".$childhealth."',
					 YourWish = '".$yourwish."',
				     DateInput = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'";
					
			if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
   			
	   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
							ApplicationID,
						     CurBSName,
							 ApplyYear,
							 ApplyDayType1,
						     ApplyDayType2,
						     ApplyDayType3,
							 ApplyLevel,
							 ChildDescription,
							 ChildHealth,
							 YourWish,
						     DateInput
						     )
						VALUES (
							'".$ApplicationID."',
							'".$CurBSName."',
							'".$SchoolYearID."',
							'".$OthersApplyDayType1."',
							'".$OthersApplyDayType2."',
							'".$OthersApplyDayType3."',
							'".$sus_status."',
							'".$childdescription."',
							'".$childhealth."',
							'".$yourwish."',
							now())
				";
				return $this->db_db_query($sql);
			}
			else 
				return true;
   		}
   		return false;
    }
//	function updateApplicationOtherInfo($data){    	
//		global $UserID,$admission_cfg;
//   		extract($data);
// 		if(empty($recordID)||empty($schoolYearId)) return;
//
//		$sql = "UPDATE 
//					ADMISSION_OTHERS_INFO 
//				SET
//	   				 EBrotherNo = '".$OthersFamilyStatus_EB."',
//				     ESisterNo = '".$OthersFamilyStatus_ES."',
//				     YBrotherNo = '".$OthersFamilyStatus_YB."',
//				     YSisterNo = '".$OthersFamilyStatus_YS."',
//				     ApplyMonth = '".$OthersApplyMonth."',
//				     ApplyTerm = '".$OthersApplyTerm."',";
//				
//				$ApplyDayTypeAry = array();
//				for($i=0;$i<3;$i++){
//					if(!empty($ApplyDayType[$i])){
//						$ApplyDayTypeAry[] = $ApplyDayType[$i];
//					}
//				} 
//				for($i=0;$i<3;$i++){
//					$sql .= "ApplyDayType".($i+1)." = '".$ApplyDayTypeAry[$i]."',";
//				}     
//		$sql .= "
//				     ExBSName = '".$OthersExBSName."',
//				     ExBSLevel = '".$OthersExBSLevel."',
//				     CurBSName = '".$OthersCurBSName."',
//				     CurBSLevel = '".$OthersCurBSLevel."',
//				     NeedSchoolBus = '".$OthersNeedSchoolBus."',
//				     SchoolBusPlace = '".($OthersNeedSchoolBus?$OthersSchoolBusPlace:'')."',
//				     KnowUsBy = '".$OthersKnowUsBy."',
//				     KnowUsByOther = '".${'txtOthersKnowUsBy'.$OthersKnowUsBy}."',
//				     DateModified = NOW(),
//				     ModifiedBy = '".$UserID."'
//				WHERE
//					RecordID = '".$recordID."' AND ApplyYear = '".$schoolYearId."'";
//			return $this->db_db_query($sql);
//			
//    }
    function updateApplicationOtherInfo($data){ //for development only
		global $UserID,$admission_cfg;
   		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;

		$sql = "UPDATE 
					ADMISSION_OTHERS_INFO o 
				INNER JOIN 
					ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
				SET
	   				 ";
		if($ApplyDayType){
			$ApplyDayTypeAry = array();
			for($i=0;$i<3;$i++){
				if(!empty($ApplyDayType[$i])){
					$ApplyDayTypeAry[] = $ApplyDayType[$i];
				}
			} 
			for($i=0;$i<3;$i++){
				$sql .= "o.ApplyDayType".($i+1)." = '".$ApplyDayTypeAry[$i]."',";
			}
		}     
		$sql .= $LastSchool?"	 stu.LastSchool = '".$LastSchool."',":"";
		$sql .= $OthersKnowUsBy?"	o.KnowUsBy = '".$OthersKnowUsBy."',":"";
		$sql .= $ChildDescription?"o.ChildDescription = '".$ChildDescription."',":"";
		$sql .= $ChildHealth?"	 o.ChildHealth = '".$ChildHealth."',":"";
		$sql .= $YourWish?" o.YourWish = '".$YourWish."',":"";
		$sql .= "		     o.DateModified = NOW(),
				     o.ModifiedBy = '".$UserID."'
				WHERE
					o.RecordID = '".$recordID."' AND o.ApplyYear = '".$schoolYearId."'";
			return $this->db_db_query($sql);
			
    }    
    function removeApplicationAttachment($data){
    	global $file_path;
    	extract($data);
    	if(empty($recordID)) return;
 		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";
    	$sql = "SELECT 
    				r.RecordID attachment_id,
    				r.AttachmentName attachment_name,
    				r.AttachmentType attachment_type
    			FROM 
    				ADMISSION_ATTACHMENT_RECORD r
    			INNER JOIN 
    				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
    			WHERE 
    				o.RecordID = '".$recordID."' 
    				".$cond."
    			";
    	$applicantAry = $this->returnArray($sql);
    
    	$Success = array();
    	for($i=0;$i<count($applicantAry);$i++){
    		$_attachmentName = $applicantAry[$i]['attachment_name'];
    		$_attachmentType = $applicantAry[$i]['attachment_type']=='personal_photo'? $applicantAry[$i]['attachment_type']:'other_files';
    		$_attachmentId = $applicantAry[$i]['attachment_id'];
    		$image_url = $this->filepath.$recordID.'/'.$_attachmentType.'/'.$_attachmentName;	
    		
    		if(file_exists($file_path.$image_url)){
    			unlink($file_path.$image_url);
    			$sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '".$_attachmentId."'";
    			
    			$Success[] = $this->db_db_query($sql);
    		}
    	}
    	if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }
    function saveApplicationAttachment($data){
    	global $UserID;
    	extract($data);
 		if(empty($recordID)) return;
    	$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '".$recordID."'";
    	$applicationID = current($this->returnVector($sql));
    	
    	if(!empty($applicationID)&&!empty($attachment_name)&&!empty($attachment_type)){
    		$result = $this->removeApplicationAttachment($data);
    		if($result){
	    		$sql = "INSERT INTO 
	    					ADMISSION_ATTACHMENT_RECORD 
	    						(ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy)
	    					VALUES
	    						('".$applicationID."','".$attachment_type."','".$attachment_name."',NOW(),'".$UserID."')
	    				";
	    		return $this->db_db_query($sql);
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
// 	function updateApplicationStudentInfo($data){
// 		extract($data);
// 		if(empty($recordID)||empty($schoolYearId)) return;
// 		
//		$sql = "
//			UPDATE 
//				ADMISSION_STU_INFO stu 
//			INNER JOIN 
//				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
//			SET
//     			stu.ChineseName = '".$student_name_b5."',
//      			stu.EnglishName = '".$student_name_en."', 
//      			stu.Gender = '".$gender."',
//      			stu.DOB = '".$dateofbirth."',	
//      			stu.PlaceOfBirth = '".$placeofbirth."',
//      			stu.Province = '".$province."',
//      			stu.County = '".$county."',
//      			stu.HomeTelNo = '".$homephoneno."',
//				stu.ContactPerson = '".$contactperson."',
//				stu.ContactPersonRelationship = '".$contactpersonrelationship."',
//      			stu.BirthCertNo = '".$birthcertno."' ,
//      			stu.Email = '".$email."',
//      			stu.ReligionCode = '".$religion."' ,
//      			stu.Church = '".$church."',
//      			stu.LastSchoolLevel = '".$lastschoollevel."' ,
//      			stu.LastSchool = '".$lastschool."',
//      			stu.Address = '".$homeaddress."'
//     		WHERE 
//				o.RecordID = '".$recordID."'	
//    	";
//    	return $this->db_db_query($sql);
//	}    
 	function updateApplicationStudentInfo($data){ // for development only
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
 		$result = true;
 		
		$sql = "
			UPDATE 
				ADMISSION_STU_INFO stu 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
     			stu.ChineseName = '".$student_name_b5."',
      			stu.EnglishName = '".$student_surname.','.$student_firstname."',
      			stu.Gender = '".$gender."',
      			stu.DOB = '".$dateofbirth."',	
      			stu.PlaceOfBirth = '".$placeofbirth."',
      			stu.Province = '".$province."',
      			stu.HomeTelNo = '".$homephoneno."',
      			stu.Email = '".$email."',
      			stu.Address = '".$homeaddress."',
				stu.LangSpokenAtHome = '".$LangSpokenAtHome."',
				o.CurBSName = '".$OthersCurBSName."' 
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}
	function getApplicationAttachmentRecord($schoolYearID,$data=array()){
		extract($data);
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";		
		$sql = "
			SELECT
				 o.RecordID folder_id,
     			 r.RecordID,
			     r.ApplicationID applicationID,
			     r.AttachmentType attachment_type,
			     r.AttachmentName attachment_name,
			     r.DateInput dateinput,
			     r.InputBy inputby
			FROM
				ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID						
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			ORDER BY r.AttachmentType, r.RecordID desc
    	";
    	$result = $this->returnArray($sql);
    	$attachmentAry = array();
    	for($i=0;$i<count($result);$i++){
    		$_attachType = $result[$i]['attachment_type'];
     		$_attachName = $result[$i]['attachment_name'];   
     		$_applicationId = $result[$i]['applicationID'];  
     		$_folderId = $result[$i]['folder_id'];
     		$attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;		
     		$attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId.'/'.($_attachType=='personal_photo'?$_attachType:'other_files').'/'.$_attachName;	
     		
    	}
		return $attachmentAry;
	}
//	function saveApplicationParentInfo($data){
//		extract($data);
// 		if(empty($recordID)||empty($schoolYearId)) return;
// 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
// 		$ApplicationID = $applicationStatus['applicationID'];
// 		$parentInfoAry = array();
// 		foreach($this->pg_type as $_key => $_pgType){
//			$parentInfoAry['ApplicationID'] = $ApplicationID;
//			$parentInfoAry['PG_TYPE'] = $_pgType;
//			$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
//			$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
//			$parentInfoAry['ChineseName'] = $parent_name_b5[$_key];
//			$parentInfoAry['JobTitle'] = $occupation[$_key];
//			$parentInfoAry['Company'] = $companyname[$_key];
//			$parentInfoAry['JobPosition'] = $jobposition[$_key];
//			$parentInfoAry['OfficeAddress'] = $companyaddress[$_key];
//			$parentInfoAry['OfficeTelNo'] = $office[$_key];
//			$parentInfoAry['Mobile'] = $mobile[$_key];	
//			if($parent_id[$_key]=='new'){
//				$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
//			}else{
//				$parentInfoAry['RecordID'] = $parent_id[$_key];	
//				$Success[] = $this->updateApplicationParentInfo($parentInfoAry);
//			}
//		}
//		if(in_array(false,$Success)){
//			return false;
//		}
//		else{
//			return true;
//		}
//	}  
	function saveApplicationParentInfo($data){ //for development only
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		$parentInfoAry = array();
 		foreach($this->pg_type as $_key => $_pgType){
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			$parentInfoAry['PG_TYPE'] = $_pgType;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			$parentInfoAry['EnglishName'] = $parent_surname[$_key].','.$parent_firstname[$_key];
			//$parentInfoAry['ChineseName'] = $parent_name_b5[$_key];
			$parentInfoAry['JobTitle'] = $occupation[$_key];
			//$parentInfoAry['Company'] = $companyname[$_key];
			//$parentInfoAry['JobPosition'] = $jobposition[$_key];
			$parentInfoAry['OfficeAddress'] = $companyaddress[$_key];
			$parentInfoAry['OfficeTelNo'] = $office[$_key];
			$parentInfoAry['Mobile'] = $mobile[$_key];
			$parentInfoAry['NativeLanguage'] = $NativeLanguage[$_key];
			if($parent_id[$_key]=='new'){
				$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
			}else{
				$parentInfoAry['RecordID'] = $parent_id[$_key];	
				$Success[] = $this->updateApplicationParentInfo($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	function getApplicationOthersInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){ //modified 20140128
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND RecordID='".$recordID."'":"";

		$sql = "
			SELECT 
				RecordID,
				ApplicationID applicationID,
				EBrotherNo elder_brother,
				ESisterNo elder_sister,
				YBrotherNo younger_brother,
				YSisterNo younger_sister,
				ApplyYear schoolYearId,
				ApplyMonth month,
				ApplyTerm term,
				ApplyDayType1,
				ApplyDayType2,
				ApplyDayType3,
				ApplyLevel classLevelID,
				ExBSName,
				ExBSLevel,
				CurBSName,
				CurBSLevel,
				ChildDescription childdescription,
				ChildHealth childhealth,
				YourWish yourwish,
				CASE WHEN NeedSchoolBus = 1 THEN 'yes' ELSE 'no' END needschoolbus,
				SchoolBusPlace,CASE ";
			foreach($admission_cfg['KnowUsBy'] as $_key => $_knowUsAry){
				$sql .= "WHEN KnowUsBy = ".$_knowUsAry['index']." THEN '".strtolower($_key)."' ";
			}
		$sql .= "
				ELSE KnowUsBy END knowusby,	
				KnowUsByOther,
				DateInput,
				DateModified,
				ModifiedBy
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	return $this->returnArray($sql);
	}		 
	function getApplicationParentInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){ //modified 20140128
		$cond = !empty($applicationID)?" AND pg.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				pg.RecordID,
     			pg.ApplicationID applicationID,
     			pg.ChineseName parent_name_b5,
     			pg.EnglishName parent_name_en,
     			pg.Relationship relationship,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			pg.PG_TYPE type,
     			pg.JobTitle occupation,
     			pg.Company companyname,
     			pg.JobPosition jobposition,
     			pg.OfficeAddress companyaddress,
     			pg.OfficeTelNo companyphone,
     			pg.Mobile mobile,
     			o.ApplyLevel classLevelID,
				pg.NativeLanguage nativelanguage 
			FROM
				ADMISSION_PG_INFO pg
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";

    	return $this->returnArray($sql);
	}	
	function getApplicationStatus($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			s.ApplicationID applicationID,
     			s.ReceiptID receiptID, 
			    IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
			    IF(s.Handler,".getNameFieldByLang2("iu.").",'') AS handler,
			    s.Handler handler_id,
				s.InterviewDate As interviewdate,
				s.InterviewLocation As interviewlocation,
			    s.Remark remark,CASE ";
		foreach($admission_cfg['Status'] as $_key => $_value){
				$sql .= "WHEN s.Status = ".$_value." THEN '".strtolower($_key)."' ";
		}	    
		$sql .= " ELSE s.Status END status,";
		$sql .= "
			    CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
			    s.DateInput dateinput,
			    s.InputBy inputby,
			    s.DateModified datemodified,
			    s.ModifiedBy modifiedby,
			    o.ApplyLevel classLevelID
			FROM
				ADMISSION_APPLICATION_STATUS s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			LEFT JOIN
				INTRANET_USER iu ON s.Handler = iu.UserID
			WHERE
				s.SchoolYearID = '".$schoolYearID."'	
			".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

		//if($type == "personal_photo"){
			if (!empty($file)) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	function moveUploadedAttachment($tempFolderPath, $destFolderPath){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/personal_photo", $destFolderPath);
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/other_files", $destFolderPath);
		$uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);
		
		if(in_array(false, $uploadSuccess))
			return false;
		else
			return true;
	}
	//Siuwan 20131018 Copy from libstudentregistry.php, e.g.$this->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	public function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
		$result = $this->returnArray($sql);
		return getSelectByArray($result, "name=".$selection_name, $code_selected,0,1);
	}
	//Henry 20131018 Copy from libstudentregistry.php
	public function returnPresetCodeName($code_type="", $selected_code="")
	{
		$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
		$result = $this->returnVector($sql);
		return $result[0];
	}
	function displayWarningMsg($warning){
		global $kis_lang;
		$x = '
		 <fieldset class="warning_box">
			<legend>'.$kis_lang['warning'].'</legend>
			<ul>
				<li>'.$kis_lang['msg'][$warning].'</li>
			</ul>
		</fieldset>
		
		';
		return $x;
	}
		
	function insertAttachmentRecord($AttachmentType,$AttachmentName, $ApplicationID){
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
		   				 ApplicationID,
		  				 AttachmentType,   
		   				 AttachmentName,
		  				 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$AttachmentType."',
						'".$AttachmentName."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	//old method
	function newApplicationNumber($schoolYearID=""){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$defaultNo = "PL".$year."-a0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%' order by ApplicationID desc";
		$result = $this->returnArray($sql);
		
		if($result){			
			$num = substr($result[0]['ApplicationID'], -4);
			$num++;
			$num = sprintf("%04s", $num);
		}
		
		$newNo = $prefix.$num;
		
		return $newNo;
	}
	function newApplicationNumber2($schoolYearID="",$classLevelID=""){
		global $Lang;
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$classLevel = $this->getClassLevel();
		$classLevel = $classLevel[$classLevelID];
		$levelKeyWord = $Lang['Admission']['icms'][$classLevel]['keyword'];
		$defaultNo = $levelKeyWord.$yearStart."0001".$yearEnd;
		$prefix_f = substr($defaultNo, 0, -6);
		$prefix_l = substr($defaultNo, -2);
		//$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix_f."%".$prefix_l."'";
		
		if($this->returnArray($sql)){
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					SELECT concat('".$prefix_f."',LPAD(MAX( CONVERT( REPLACE( REPLACE( ApplicationID, '".$prefix_f."', '' ), '".$prefix_l."', '' ) , UNSIGNED ) ) +1, 4, '0'),'".$prefix_l."') 
					FROM ADMISSION_OTHERS_INFO 
					WHERE ApplicationID like '".$prefix_f."%".$prefix_l."'";
			$result = $this->db_db_query($sql);
					
			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
			$newNo = $this->returnArray($sql);
			return $newNo[0]['ApplicationID'];
		}
		else
			return $defaultNo;
	}
//	function newApplicationNumber2($schoolYearID=""){
//		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
//		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
//		if($yearStart == $yearEnd){
//			$year = $yearStart;
//		}
//		else{
//			$year = $yearStart.$yearEnd;
//		}
//		$defaultNo = "PL".$year."-a0001";
//		$prefix = substr($defaultNo, 0, -4);
//		$num = substr($defaultNo, -4);
//
//		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
//		
//		if($this->returnArray($sql)){
//			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
//					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( REPLACE( ApplicationID, '".$prefix."', '' ) , UNSIGNED ) ) +1, 4, '0')) 
//					FROM ADMISSION_OTHERS_INFO 
//					WHERE ApplicationID like '".$prefix."%'";
//			$result = $this->db_db_query($sql);
//					
//			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
//			$newNo = $this->returnArray($sql);
//			return $newNo[0]['ApplicationID'];
//		}
//		else
//			return $defaultNo;
//	}
	function getAttachmentByApplicationID($schoolYearID,$applicationID){
		global $file_path;
		$attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID,array("applicationID"=>$applicationID));
		$attachment = array();
		foreach((array)$attachmentAry[$applicationID] as $_type => $_attachmentAry){
			$_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
			if(!empty($_thisAttachment)){
				$_thisAttachment = $this->filepath.$_thisAttachment; 
				if(file_exists($file_path.$_thisAttachment)){
					$attachment[$_type]['link'] = $_thisAttachment;
				}else{
					$attachment[$_type]['link'] = false;
				}
			}else{
				$attachment[$_type]['link'] = false;
			}
			
		}
		return $attachment;
	}
	public function getPrintLink($schoolYearID="", $applicationID , $type=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?id='.$id;
	}
 	function getClassLevel($ClassLevel=''){
 		global $intranet_root;
    	include_once($intranet_root."/includes/form_class_manage.php");
    	$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
    	$numOfForm = count($FormArr);
    	$classLevelName = array();
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisClassLevelID = $FormArr[$i]['YearID'];
			$thisLevelName = $FormArr[$i]['YearName'];
			$classLevelName[$thisClassLevelID] = $thisLevelName;
		}
		return $classLevelName;
    }	
	function getBasicSettings($schoolYearID='',$SettingNameAry=array()){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
    	$sql = "
			SELECT
			    SettingName,
			    SettingValue
			FROM
				ADMISSION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'	
    	";
    	if (sizeof($SettingNameAry) > 0)  {
			$sql .= " AND 
						SettingName in ('".implode("','",$SettingNameAry)."')";
		}
    	$setting = $this->returnArray($sql);
		for ($i=0; $i< sizeof($setting); $i++) {
			$Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
		}
		return $Return;    	
    }
   	function saveBasicSettings($schoolYearID='',$SettingNameValueAry=array()){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$this->Start_Trans();
		
		$SettingsNameAry = array_keys($SettingNameValueAry);
		
		$result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID,$SettingsNameAry);
		$result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID,$SettingNameValueAry);
		
		if(in_array(false, $result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	 
	function removeBasicSettings($schoolYearID,$SettingsNameAry){
		if(count($SettingsNameAry)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameAry)."'";
		
		$sql = "
			DELETE FROM
				ADMISSION_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
			AND SchoolYearID = '".$schoolYearID."'
		";
		
		return $this->db_db_query($sql);
	}
	
	function insertBasicSettings($schoolYearID,$SettingNameValueAry){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		
		foreach((array)$SettingNameValueAry as $_settingName => $_settingValue)
		{
			$InsertSqlArr[] = "('".$schoolYearID."','".$_settingName."','".$_settingValue."', '".$this->uid."', NOW(), '".$this->uid."', NOW())";
		}
		
		if(count($InsertSqlArr)>0)
		{	
			$InsertSql = implode(',',$InsertSqlArr);	
			
			$sql = "
				INSERT INTO	ADMISSION_SETTING
					(SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)	 
				VALUES
					$InsertSql
			";
			
			return $this->db_db_query($sql);
			
		}
		else
			return false;
		
			
	}
	//henry added 20140127
	function getSchoolIDsByClassLevelID($classLevelID){
		$sql = "
			SELECT
     			SchoolYearID, 
				IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d'),'') As StartDate,
				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d'),'') As EndDate
			FROM
				ADMISSION_APPLICATION_SETTING
			WHERE
				ClassLevelID = '".$classLevelID."'
				AND StartDate <= CURDATE() AND EndDate >= CURDATE()";
		
		return $this->returnArray($sql);
	}
	
	//henry modified for ICMS class selection 20140127
    function getApplicationSetting($schoolYearID=''){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

    	$sql = "
			SELECT
     			ClassLevelID,
			    IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d %H:%i'),'') As StartDate,
				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d %H:%i'),'') As EndDate,
				IF(DOBStart,DATE_FORMAT(DOBStart,'%Y-%m-%d'),'') As DOBStart,
				IF(DOBEnd,DATE_FORMAT(DOBEnd,'%Y-%m-%d'),'') As DOBEnd,
				IF(PreviewStartDate,DATE_FORMAT(PreviewStartDate,'%Y-%m-%d %H:%i'),'') As PreviewStartDate,
				IF(PreviewEndDate,DATE_FORMAT(PreviewEndDate,'%Y-%m-%d %H:%i'),'') As PreviewEndDate,
				Quota,
			    DayType,
			    FirstPageContent,
			    LastPageContent,
				EmailContent,
			    DateInput,
			    InputBy,
			    DateModified,
			    ModifiedBy
			FROM
				ADMISSION_APPLICATION_SETTING";
		if($schoolYearID != "all_year")		
		$sql .= " WHERE
				SchoolYearID = '".$schoolYearID."'";
    	else
    	$sql .= " WHERE
				DATE(EndDate) >= DATE(NOW()) 
				ORDER BY StartDate, EndDate";
					
    	$setting = $this->returnArray($sql);
    	$applicationSettingAry = BuildMultiKeyAssoc($setting, 'ClassLevelID');
    	$applicationPeriodAry = array();
    	foreach($this->classLevelAry as $_classLevelId => $_classLevelName){ 	
    		$_startdate = $applicationSettingAry[$_classLevelId]['StartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['StartDate']:'';	
    		$_enddate = $applicationSettingAry[$_classLevelId]['EndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['EndDate']:'';	
    		
    		$applicationPeriodAry[$_classLevelId] = array(
    													'ClassLevelName'=>$_classLevelName,
      													'StartDate'=>$_startdate,  	
      													'EndDate'=>$_enddate,
      													'PreviewStartDate'=>$_previewstartdate,  	
      													'PreviewEndDate'=>$_previewenddate,
      													'DOBStart'=>$applicationSettingAry[$_classLevelId]['DOBStart'],
      													'DOBEnd'=>$applicationSettingAry[$_classLevelId]['DOBEnd'],
      													'DayType'=>$applicationSettingAry[$_classLevelId]['DayType'],   
      													'FirstPageContent'=>$applicationSettingAry[$_classLevelId]['FirstPageContent'],  
      													'LastPageContent'=>$applicationSettingAry[$_classLevelId]['LastPageContent'],
      													'Quota'=>$applicationSettingAry[$_classLevelId]['Quota'],
      													'EmailContent'=>$applicationSettingAry[$_classLevelId]['EmailContent']      													  																									
    												);
    	}
    	return $applicationPeriodAry;    	
    } 
	public function updateApplicationSetting($data){ //modified for ICMS cust
		global $admission_cfg;
   		extract($data);
   		
   		#get the class name of ICMS
   		$classLevel = $this->getClassLevel();
		$classLevel = trim($classLevel[$classLevelID]);
   		
   		#Check exist setting
   		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
   		$cnt = current($this->returnVector($sql));

   		if($cnt){//update
   			$sql = "UPDATE ADMISSION_APPLICATION_SETTING SET 
		   				 StartDate = '".$startDate."',
		  				 EndDate = '".$endDate."', 
						 PreviewStartDate = '".$previewStartDate."',
		  				 PreviewEndDate = '".$previewEndDate."',
						 Quota = '".$quota."',
						 DOBStart = '".$dOBStart."',
		  				 DOBEnd = '".$dOBEnd."',  
		   				 DayType = '".$admission_cfg['DayType'][$classLevel]."',
		  				 FirstPageContent = '".$firstPageContent."',   
		   				 LastPageContent = '".$lastPageContent."',
						 EmailContent = '".$emailContent."',
		   				 DateModified = NOW(),
		   				 ModifiedBy = '".$this->uid."'
   					WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
   		}else{//insert
   			$sql = "INSERT INTO ADMISSION_APPLICATION_SETTING (
   						SchoolYearID,
   						ClassLevelID,
					    StartDate,
					    EndDate,
						PreviewStartDate,
		  				PreviewEndDate,
						Quota,
					    DayType,
		  				FirstPageContent,   
		   				LastPageContent,
						EmailContent,
					    DateInput,
					    InputBy,
					    DateModified,
					    ModifiedBy) 
					VALUES (
					    '".$schoolYearID."',
					    '".$classLevelID."',
					    '".$startDate."',
					    '".$endDate."',
						'".$previewStartDate."',
						'".$previewEndDate."',
						'".$quota."',	
					    '".$admission_cfg['DayType'][$classLevel]."',
					    '".$firstPageContent."',
					    '".$lastPageContent."',
						'".$emailContent."',	
					    NOW(),
					    '".$this->uid."',
					     NOW(),
					    '".$this->uid."')
			";
   		}
   		return $this->db_db_query($sql);
    }    
    function getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$recordID=''){ //henry modified 20140128
		$cond = !empty($applicationID)?" AND stu.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($status)?" AND s.status='".$Status."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			stu.ApplicationID applicationID,
     			o.ApplyLevel classLevelID,
     			o.ApplyYear schoolYearID,
     			stu.ChineseName student_name_b5,
      			stu.EnglishName student_name_en, 
      			stu.Gender gender,
      			IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,	
      			stu.PlaceOfBirth placeofbirth,
      			stu.Province province,
      			stu.County county,
      			stu.HomeTelNo homephoneno,
      			stu.BirthCertNo birthcertno,
      			stu.Email email,
      			stu.ReligionCode religion,
      			stu.Church church,
      			stu.LastSchoolLevel lastschoollevel,
      			stu.LastSchool lastschool,
      			stu.Address homeaddress,
				stu.LangSpokenAtHome langspokenathome,		
     			".getNameFieldByLang2("stu.")." AS student_name
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID							
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	$studentInfoAry = $this->returnArray($sql);
		return $studentInfoAry;
	}
	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
				o.ApplyDayType1 AS ApplyDayType1,
				o.ApplyDayType2 AS ApplyDayType2,
				o.ApplyDayType3 AS ApplyDayType3,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function getExportHeader(){
		global $kis_lang, $Lang;
		
		$headerArray = array();
		
		//for student info
		$headerArray[] = $Lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray['sectiona'][] = $Lang['Admission']['schoolYearOption'];
		$headerArray['sectiona'][] = $Lang['Admission']['icms']['selectcampus'];
		$headerArray['sectiona'][] = $Lang['Admission']['selectprogram'];
		$headerArray['sectiona'][] = $Lang['Admission']['icms']['applyDayType']."(".$Lang['Admission']['Option']." 1)";
		$headerArray['sectiona'][] = $Lang['Admission']['icms']['applyDayType']."(".$Lang['Admission']['Option']." 2)";
		$headerArray['sectiona'][] = $Lang['Admission']['icms']['applyDayType']."(".$Lang['Admission']['Option']." 3)";
		
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['studentssurname'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['firstname'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['nameinchinese'];
		$headerArray['sectionb'][] = $Lang['Admission']['gender'];
		$headerArray['sectionb'][] = $Lang['Admission']['dateofbirth'];
		$headerArray['sectionb'][] = $Lang['Admission']['placeofbirth'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['nationality'];
		$headerArray['sectionb'][] = $Lang['Admission']['homeaddress'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['homenumber'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['emailaddress'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['CurBSName'];
		$headerArray['sectionb'][] = $Lang['Admission']['icms']['langspokenathome'];
		//$headerArray['studentInfo'][] = $Lang['Admission']['document'];
		
		//for parent info
		$headerArray['parentInfoF'][] = $Lang['Admission']['icms']['fatherssurname'];
		$headerArray['parentInfoF'][] = $Lang['Admission']['icms']['fathersfirstname'];
		$headerArray['parentInfoF'][] = $Lang['Admission']['icms']['mobilenumber'] ;
		$headerArray['parentInfoF'][] = $Lang['Admission']['icms']['nativelanguage'];
		$headerArray['parentInfoF'][] = $Lang['Admission']['occupation'];
		$headerArray['parentInfoF'][] = $Lang['Admission']['icms']['worknumber'];
		$headerArray['parentInfoF'][] = $Lang['Admission']['icms']['workaddress'];
		
		$headerArray['parentInfoM'][] = $Lang['Admission']['icms']['motherssurname'];
		$headerArray['parentInfoM'][] = $Lang['Admission']['icms']['mothersfirstname'];
		$headerArray['parentInfoM'][] = $Lang['Admission']['icms']['mobilenumber'] ;
		$headerArray['parentInfoM'][] = $Lang['Admission']['icms']['nativelanguage'];
		$headerArray['parentInfoM'][] = $Lang['Admission']['occupation'];
		$headerArray['parentInfoM'][] = $Lang['Admission']['icms']['worknumber'];
		$headerArray['parentInfoM'][] = $Lang['Admission']['icms']['workaddress'];
		
		//for other info
		$headerArray['sectiondef'][] = $Lang['Admission']['icms']['listprevschool'];
		$headerArray['sectiondef'][] = $Lang['Admission']['icms']['childdescription'];
		$headerArray['sectiondef'][] = $Lang['Admission']['icms']['childhealth'];
		$headerArray['sectiondef'][] = $Lang['Admission']['icms']['yourwish'];
		
		//for official use
		$headerArray['officialUse'][] = $Lang['Admission']['applicationstatus'];
//		$headerArray['officialUse'][] = $Lang['Admission']['applicationfee']." (".$Lang['Admission']['receiptcode'].")";
//		$headerArray['officialUse'][] = $Lang['Admission']['applicationfee']." (".$Lang['Admission']['date'].")";
//		$headerArray['officialUse'][] = $Lang['Admission']['applicationfee']." (".$Lang['Admission']['handler'].")";
		$headerArray['officialUse'][] = $Lang['Admission']['interviewdate'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
//		$headerArray['officialUse'][] = $Lang['Admission']['isnotified'];
		$headerArray['officialUse'][] = $Lang['Admission']['otherremarks'];
		
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//student info header
		$exportColumn[0][] = $Lang['Admission']['sectiona'];
		for($i=0; $i < count($headerArray['sectiona'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		$exportColumn[0][] = $Lang['Admission']['sectionb'];
		for($i=0; $i < count($headerArray['sectionb'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//parent info header
		$exportColumn[0][] = $Lang['Admission']['sectionc']."(".$Lang['Admission']['PG_Type']['F'].")";
		for($i=0; $i < count($headerArray['parentInfoF'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = $Lang['Admission']['sectionc']."(".$Lang['Admission']['PG_Type']['M'].")";
		for($i=0; $i < count($headerArray['parentInfoM'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $Lang['Admission']['sectiond'];
		$exportColumn[0][] = "";
		$exportColumn[0][] = $Lang['Admission']['sectione'];
		$exportColumn[0][] = $Lang['Admission']['sectionf'];
		
		//official use header
		$exportColumn[0][] = $kis_lang['remarks'];
		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//sub header
		$exportColumn[1] = array_merge(array($headerArray[0],$headerArray[1]), $headerArray['sectiona'], $headerArray['sectionb'], $headerArray['parentInfoF'], $headerArray['parentInfoM'], $headerArray['sectiondef'], $headerArray['officialUse']);
	
		
		return $exportColumn;
	}
	
	function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg, $Lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$dataArray = array();
		
		//for student info
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$tempStuEngName = explode(",",$studentInfo['student_name_en']);
		
		$dataArray['studentInfo'][] = getAcademicYearByAcademicYearID($otherInfo['schoolYearId']);
		$dataArray['studentInfo'][] = $Lang['Admission']['icms']['tinhaucampus'];
		$dataArray['studentInfo'][] = $classLevel[$otherInfo['classLevelID']];
		$dataArray['studentInfo'][] = $Lang['Admission']['icms'][$classLevel[$otherInfo['classLevelID']]]['TimeSlot'][$otherInfo['ApplyDayType1']];
		$dataArray['studentInfo'][] = $Lang['Admission']['icms'][$classLevel[$otherInfo['classLevelID']]]['TimeSlot'][$otherInfo['ApplyDayType2']];
		$dataArray['studentInfo'][] =$Lang['Admission']['icms'][$classLevel[$otherInfo['classLevelID']]]['TimeSlot'][$otherInfo['ApplyDayType3']];
		
		$dataArray['studentInfo'][] = $tempStuEngName[0];
		$dataArray['studentInfo'][] = $tempStuEngName[1];
		$dataArray['studentInfo'][] = $studentInfo['student_name_b5'];
		$dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
		$dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
		$dataArray['studentInfo'][] = $studentInfo['placeofbirth'];
		$dataArray['studentInfo'][] = $studentInfo['province'];
		$dataArray['studentInfo'][] = $studentInfo['homeaddress'];
		$dataArray['studentInfo'][] = $studentInfo['homephoneno'];
		$dataArray['studentInfo'][] = $studentInfo['email'];
		$dataArray['studentInfo'][] = $otherInfo['CurBSName'];
		$dataArray['studentInfo'][] = $studentInfo['langspokenathome'];;
		
		//for parent info		
		
		for($i=0;$i<count($parentInfo);$i++){
			$tempParentEngName = explode(",",$parentInfo[$i]['parent_name_en']);
			if($parentInfo[$i]['type'] == 'F'){
				$dataArray['parentInfoF'][] = $tempParentEngName[0];
				$dataArray['parentInfoF'][] = $tempParentEngName[1];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['nativelanguage'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['companyphone'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['companyaddress'];	
			}
			else if($parentInfo[$i]['type'] == 'M'){
				$dataArray['parentInfoM'][] = $tempParentEngName[0];
				$dataArray['parentInfoM'][] = $tempParentEngName[1];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['nativelanguage'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['companyphone'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['companyaddress'];
			}
		}
		
		if(count($dataArray['parentInfoF']) == 0){
			$dataArray['parentInfoF'] = array('','','','','','','');
		}
		if(count($dataArray['parentInfoM']) == 0){
			$dataArray['parentInfoM'] = array('','','','','','','');
		}

		
		//for other info
		$dataArray['otherInfo'][] = $studentInfo['lastschool'];
		$dataArray['otherInfo'][] = $otherInfo['childdescription'];
		$dataArray['otherInfo'][] = $otherInfo['childhealth'];
		$dataArray['otherInfo'][] = $otherInfo['yourwish'];
		
		
		//for official use
		$dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
//		$dataArray['officialUse'][] = $status['receiptID'];
//		$dataArray['officialUse'][] = $status['receiptdate'];
//		$dataArray['officialUse'][] = $status['handler'];
		$dataArray['officialUse'][] = $status['interviewdate'];
		$dataArray['officialUse'][] = $status['interviewlocation'];
//		$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
		$dataArray['officialUse'][] = $status['remark'];
		
		$ExportArr = array_merge(array($dataArray[0],$dataArray[1]),$dataArray['studentInfo'],$dataArray['parentInfoF'],$dataArray['parentInfoM'], $dataArray['otherInfo'], $dataArray['officialUse']);
		
		return $ExportArr;
	}
	function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature,$sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
		
		$dataArray = array();
		
		if($tabID == 2){
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = ''; //Mobile
			$dataArray[0][] = ''; //Fax
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = ''; //Remarks
			$dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
			$dataArray[0][] = $studentInfo['homeaddress']; //Address
			if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
			{
				$dataArray[0][] = ''; //CardID
				if($sys_custom['SupplementarySmartCard']){
					$dataArray[0][] = ''; //CardID2
					$dataArray[0][] = ''; //CardID3
				}
			}
			
			if($special_feature['ava_hkid'])
				$dataArray[0][] = $studentInfo['birthcertno']; //HKID
			if($special_feature['ava_strn'])
				$dataArray[0][] = ''; //STRN
			if($plugin['medical'])
				$dataArray[0][] = ''; //StayOverNight
			$dataArray[0][] = $studentInfo['province']; //Nationality
			$dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
		}
		else if($tabID == 3){
			$hasParent = false;
			$dataCount = array();
			for($i=0;$i<count($parentInfo);$i++){
				$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
				if($parentInfo[$i]['type'] == 'F' && !$hasParent){
					$dataArray[0] = array();
					$dataArray[0][] = ''; //UserLogin
					$dataArray[0][] = ''; //Password
					$dataArray[0][] = $studentInfo['email']; //UserEmail
					$dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[0][] = ''; //ChineseName
					$dataArray[0][] = 'M'; //Gender
					$dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[0][] = ''; //Fax
					$dataArray[0][] = ''; //Barcode
					$dataArray[0][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[0][] = ''; //HKID
					$dataArray[0][] = ''; //StudentLogin1
					$dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[0][] = ''; //StudentLogin2
					$dataArray[0][] = ''; //StudentEngName2
					$dataArray[0][] = ''; //StudentLogin3
					$dataArray[0][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[0] = ($studentInfo['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'M' && !$hasParent){
					$dataArray[1] = array();
					$dataArray[1][] = ''; //UserLogin
					$dataArray[1][] = ''; //Password
					$dataArray[1][] = $studentInfo['email']; //UserEmail
					$dataArray[1][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[1][] = ''; //ChineseName
					$dataArray[1][] = 'F'; //Gender
					$dataArray[1][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[1][] = ''; //Fax
					$dataArray[1][] = ''; //Barcode
					$dataArray[1][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[1][] = ''; //HKID
					$dataArray[1][] = ''; //StudentLogin1
					$dataArray[1][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[1][] = ''; //StudentLogin2
					$dataArray[1][] = ''; //StudentEngName2
					$dataArray[1][] = ''; //StudentLogin3
					$dataArray[1][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[1] = ($studentInfo['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				
			}
			if($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1]){
				$tempDataArray = $dataArray[0];
			}
			else if($dataCount[1] > 0){
				$tempDataArray = $dataArray[1];
			}
			$dataArray = array();
			$dataArray[0] = $tempDataArray;
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE aoi.ApplyLevel = '{$applyLevel}' AND TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."'";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		if($subject == ''){
			$email_subject = "Island Children's Montessori School & Kindergarten Admission Notification";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path="",$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	
	function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[2] = getDefaultDateFormat($aData[2]);
			//check date
			if ($aData[2] =='' && $aData[3] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[2]) ) {
		       list($year , $month , $day) = explode('-',$aData[2]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[2] =='' && $aData[3] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[3]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[2] = getDefaultDateFormat($aData[2]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[2]." ".$aData[3]."',
				InterviewLocation = '".$aData[4]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
}
?>