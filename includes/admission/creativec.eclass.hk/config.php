<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(帝堡城)';
$admission_cfg['SchoolName']['en'] .= ' (Castello)';
$admission_cfg['SchoolCode'] = 'CAST';
$admission_cfg['SchoolPhone'] = '2882 2005';
$admission_cfg['SchoolAddress']['b5'] = '新界沙田小瀝源路69號L6平台';
$admission_cfg['SchoolAddress']['en'] = 'Podium, L6, 69 Siu Lek Yuen Road, Shatin, New Territories';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckcast.photo@gmail.com';
}
/* for email [end] */


/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = 'L2TEW3Q5ZVHZN';
} else {
    $admission_cfg['paypal_signature'] = 'gjUheOJ1s8XvR-bc-N35u9dQjtW44HvFZGDylaQQKTX7vgDQ-eK-HH2mIlq';
    $admission_cfg['hosted_button_id'] = 'JLM428YAN5HYS';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Castello)';
}
/* for paypal [End] */