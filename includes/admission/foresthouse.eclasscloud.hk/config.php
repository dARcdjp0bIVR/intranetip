<?php
// using: Pun
/*
 * This page is for admission only. For general KIS config : kis/config.php
 */

// ####### Basic config START ########
$admission_cfg['Status'] = array(); // Please also define lang in admission_lang
$admission_cfg['Status']['pending'] = 1;
$admission_cfg['Status']['paymentsettled'] = 2;
$admission_cfg['Status']['confirmed'] = 4;
$admission_cfg['Status']['cancelled'] = 5;

$admission_cfg['PaymentStatus']['OnlinePayment'] = 1;
$admission_cfg['PaymentStatus']['OtherPayment'] = 2;

$admission_cfg['DefaultLang'] = "en";
$admission_cfg['Lang'] = array();
// $admission_cfg['Lang'][1] = 'en';
// $admission_cfg['Lang'][2] = 'b5';

$admission_cfg['interview_arrangment']['interview_group_type'] = 'Room';
// $admission_cfg['interview_arrangment']['interview_group_name'] = array('101', '102', '103', '104', '201', '202', '203', '204', '301', '302', '303', '304');
// ####### Basic config END ########


// ####### Cust config START ########
//$admission_cfg['SchoolName']['b5'] = '樹宏學校';
$admission_cfg['SchoolName']['en'] = 'Forest House Waldorf School';

$admission_cfg['themeStyle'] = 'primary';
// ####### Cust config END ########


// ####### Fixed config START ########
$admission_cfg['MultipleLang'] = count($admission_cfg['Lang']) > 1;
$admission_cfg['PrintByPDF'] = 1;
$admission_cfg['FilePath'] = $PATH_WRT_ROOT . "/file/admission/";
$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
$admission_cfg['maxUploadSize'] = 5; // in MB
$admission_cfg['personal_photo_width'] = 200;
$admission_cfg['personal_photo_height'] = 260;

if($plugin['eAdmission_devMode']){
    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
}else{
    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
}

if($plugin['eAdmission_devMode']){
	/* for paypal testing [start] */
	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	$admission_cfg['paypal_signature'] = 'fdV5u7aH605A0exF-LEiYx6P5l-HcIR9Dm5w0GFjMrafMm65cb1nKudxwam';
	$admission_cfg['hosted_button_id'] = 'BAFCWBYUQV538';
	$admission_cfg['paypal_name'] = "test facilitator's Test Store";
	/* for paypal testing [End] */
}else{
	/* for real paypal use [start] */
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
	$admission_cfg['paypal_signature'] = '-PHq5f-47rJlAtPwaVeMPgCmXHoWn842yXjvTQGlaNZUt5SrmrRIuc6YArC';
	$admission_cfg['hosted_button_id'] = 'YJGGMMXZGW6UQ';
	$admission_cfg['paypal_name'] = 'FOREST HOUSE INTERNATIONAL SCHOOL LTD';
	/* for real paypal use [End] */
}
// ######## Fixed config END ########
