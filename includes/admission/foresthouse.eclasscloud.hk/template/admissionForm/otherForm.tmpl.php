<section id="otherForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$Lang['Admission']['FH']['newStudentQuestionnaire'] ?>
	</div>
	<div class="sheet">

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['howHearSchool'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="HowHearSchool" id="HowHearSchool" value="<?=$allCustInfo['HowHearSchool'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['HowHearSchool'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['HowHearSchool'][0]['Value'] ? $allCustInfo['HowHearSchool'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['haveParticipatedActivity'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="HaveParticipatedActivity" id="HaveParticipatedActivity" value="<?=$allCustInfo['HaveParticipatedActivity'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['HaveParticipatedActivity'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['HaveParticipatedActivity'][0]['Value'] ? $allCustInfo['HaveParticipatedActivity'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['describeExpertise'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="DescribeExpertise" id="DescribeExpertise" value="<?=$allCustInfo['DescribeExpertise'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['DescribeExpertise'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['DescribeExpertise'][0]['Value'] ? $allCustInfo['DescribeExpertise'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['whyChooseSchool'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="WhyChooseSchool" id="WhyChooseSchool" value="<?=$allCustInfo['WhyChooseSchool'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['WhyChooseSchool'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['WhyChooseSchool'][0]['Value'] ? $allCustInfo['WhyChooseSchool'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['hasSibling'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="HasSibling" id="HasSibling" value="<?=$allCustInfo['HasSibling'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['HasSibling'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['HasSibling'][0]['Value'] ? $allCustInfo['HasSibling'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['describeChildPersonality'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="DescribeChildPersonality" id="DescribeChildPersonality" value="<?=$allCustInfo['DescribeChildPersonality'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['DescribeChildPersonality'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['DescribeChildPersonality'][0]['Value'] ? $allCustInfo['DescribeChildPersonality'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['describeChildRoutine'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="DescribeChildRoutine" id="DescribeChildRoutine" value="<?=$allCustInfo['DescribeChildRoutine'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['DescribeChildRoutine'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['DescribeChildRoutine'][0]['Value'] ? $allCustInfo['DescribeChildRoutine'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['describeChildRelationship'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="DescribeChildRelationship" id="DescribeChildRelationship" value="<?=$allCustInfo['DescribeChildRelationship'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['DescribeChildRelationship'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['DescribeChildRelationship'][0]['Value'] ? $allCustInfo['DescribeChildRelationship'][0]['Value'] : '－－' ?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['medicalConcern'] ?></div>
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="MedicalConcern" id="MedicalConcern" value="<?=$allCustInfo['MedicalConcern'][0]['Value'] ?>">
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataValue <?=$allCustInfo['MedicalConcern'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['MedicalConcern'][0]['Value'] ? $allCustInfo['MedicalConcern'][0]['Value'] : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel"><?=$Lang['Admission']['FH']['hasFormalAssessment'] ?></div>
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['speechTherapy'] ?>:</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsSpeechTherapy_Y" name="IsSpeechTherapy" value="Y" required <?=$allCustInfo['IsSpeechTherapy'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsSpeechTherapy_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsSpeechTherapy_N" name="IsSpeechTherapy" value="N" required <?=!$allCustInfo['IsSpeechTherapy'][0]['Value'] || $allCustInfo['IsSpeechTherapy'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsSpeechTherapy_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="SpeechTherapyDate" id="SpeechTherapyDate" value="<?=$allCustInfo['SpeechTherapyDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errSpeechTherapyDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['speechTherapy'] ?>:</div>
						<div class="dataValue"></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['SpeechTherapyDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['SpeechTherapyDate'][0]['Value'] ? $allCustInfo['SpeechTherapyDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['occupationalTherapy'] ?>:</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsOccupationalTherapy_Y" name="IsOccupationalTherapy" value="Y" required <?=$allCustInfo['IsOccupationalTherapy'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsOccupationalTherapy_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsOccupationalTherapy_N" name="IsOccupationalTherapy" value="N" required <?=!$allCustInfo['IsOccupationalTherapy'][0]['Value'] || $allCustInfo['IsOccupationalTherapy'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsOccupationalTherapy_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="OccupationalTherapyDate" id="OccupationalTherapyDate" value="<?=$allCustInfo['OccupationalTherapyDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errOccupationalTherapyDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['occupationalTherapy'] ?>:</div>
						<div class="dataValue"></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['OccupationalTherapyDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['OccupationalTherapyDate'][0]['Value'] ? $allCustInfo['OccupationalTherapyDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['psychoEducationalAssessment'] ?>:</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsPsychoEducationalAssessment_Y" name="IsPsychoEducationalAssessment" value="Y" required <?=$allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsPsychoEducationalAssessment_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsPsychoEducationalAssessment_N" name="IsPsychoEducationalAssessment" value="N" required <?=!$allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] || $allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsPsychoEducationalAssessment_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="PsychoEducationalAssessmentDate" id="PsychoEducationalAssessmentDate" value="<?=$allCustInfo['PsychoEducationalAssessmentDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errPsychoEducationalAssessmentDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['psychoEducationalAssessment'] ?>:</div>
						<div class="dataValue"></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['PsychoEducationalAssessmentDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['PsychoEducationalAssessmentDate'][0]['Value'] ? $allCustInfo['PsychoEducationalAssessmentDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['psychologicalAssessmentCounselling'] ?>:</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsPsychologicalAssessmentCounselling_Y" name="IsPsychologicalAssessmentCounselling" value="Y" required <?=$allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsPsychologicalAssessmentCounselling_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsPsychologicalAssessmentCounselling_N" name="IsPsychologicalAssessmentCounselling" value="N" required <?=!$allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] || $allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsPsychologicalAssessmentCounselling_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="PsychologicalAssessmentCounsellingDate" id="PsychologicalAssessmentCounsellingDate" value="<?=$allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errPsychologicalAssessmentCounsellingDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['psychologicalAssessmentCounselling'] ?>:</div>
						<div class="dataValue"></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value'] ? $allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['remedialInstructionTutoring'] ?>:</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsRemedialInstructionTutoring_Y" name="IsRemedialInstructionTutoring" value="Y" required <?=$allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsRemedialInstructionTutoring_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsRemedialInstructionTutoring_N" name="IsRemedialInstructionTutoring" value="N" required <?=!$allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] || $allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsRemedialInstructionTutoring_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="RemedialInstructionTutoringDate" id="RemedialInstructionTutoringDate" value="<?=$allCustInfo['RemedialInstructionTutoringDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errRemedialInstructionTutoringDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['remedialInstructionTutoring'] ?>:</div>
						<div class="dataValue"></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['RemedialInstructionTutoringDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['RemedialInstructionTutoringDate'][0]['Value'] ? $allCustInfo['RemedialInstructionTutoringDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['individualizedProgramPlan'] ?>:</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsIndividualizedProgramPlan_Y" name="IsIndividualizedProgramPlan" value="Y" required <?=$allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsIndividualizedProgramPlan_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsIndividualizedProgramPlan_N" name="IsIndividualizedProgramPlan" value="N" required <?=!$allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] || $allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsIndividualizedProgramPlan_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="IndividualizedProgramPlanDate" id="IndividualizedProgramPlanDate" value="<?=$allCustInfo['IndividualizedProgramPlanDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errIndividualizedProgramPlanDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="itemLabel"><?=$Lang['Admission']['FH']['individualizedProgramPlan'] ?>:</div>
						<div class="dataValue"></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['IndividualizedProgramPlanDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['IndividualizedProgramPlanDate'][0]['Value'] ? $allCustInfo['IndividualizedProgramPlanDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="OtherFormalAssessmentName" id="OtherFormalAssessmentName" value="<?=$allCustInfo['OtherFormalAssessmentName'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['FH']['other'] ?>:</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsOtherFormalAssessment_Y" name="IsOtherFormalAssessment" value="Y" required <?=$allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsOtherFormalAssessment_Y"><?=$Lang['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsOtherFormalAssessment_N" name="IsOtherFormalAssessment" value="N" required <?=!$allCustInfo['IsOtherFormalAssessment'][0]['Value'] || $allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsOtherFormalAssessment_N"><?=$Lang['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="OtherFormalAssessmentDate" id="OtherFormalAssessmentDate" value="<?=$allCustInfo['OtherFormalAssessmentDate'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errOtherFormalAssessmentDate"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataValue <?=$allCustInfo['OtherFormalAssessmentName'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['OtherFormalAssessmentName'][0]['Value'] ? $allCustInfo['OtherFormalAssessmentName'][0]['Value'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['date'] ?> (mm/yy)</div>
						<div class="dataValue <?=$allCustInfo['OtherFormalAssessmentDate'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['OtherFormalAssessmentDate'][0]['Value'] ? $allCustInfo['OtherFormalAssessmentDate'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
		</div>
	</div>
</section>

<script>
$(function(){
	'use strict';
	
	$('#otherForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html($(this).val());
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html($(this).next('label').html());
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
			
		$inputDateSelect.on('change', function(e){
			$dataValue.html($inputDateYearSelect.val()+'-'+$inputDateMonthSelect.val()+'-'+$inputDateDaySelect.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
		
		
	});
	
	window.checkOtherForm = (function(lifecycle){
		var isValid = true;
		var $otherForm = $('#otherForm');

		/******** Basic init START ********/
		//for mm/yy validation
		var re = /^(0[123456789]|10|11|12)\/\d{2}$/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($otherForm);
		
		if($('#IsSpeechTherapy_Y').prop("checked") && $('#SpeechTherapyDate').val() == ''){
			$('#SpeechTherapyDate').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#SpeechTherapyDate');
			isValid = false;
		}
		
		if($('#SpeechTherapyDate').val().trim()!='' && !re.test($('#SpeechTherapyDate').val().trim())){
			if($('#SpeechTherapyDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errSpeechTherapyDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errSpeechTherapyDate').removeClass('hide');
				focusElement = $('#SpeechTherapyDate');
			}	
	    	isValid = false;
		}
		
		if($('#IsOccupationalTherapy_Y').prop("checked") && $('#IsOccupationalTherapy').val() == ''){
			$('#IsOccupationalTherapy').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#IsOccupationalTherapy');
			isValid = false;
		}
		
		if($('#OccupationalTherapyDate').val().trim()!='' && !re.test($('#OccupationalTherapyDate').val().trim())){
			if($('#OccupationalTherapyDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errOccupationalTherapyDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errOccupationalTherapyDate').removeClass('hide');
				focusElement = $('#OccupationalTherapyDate');
			}	
	    	isValid = false;
		}
		
		if($('#IsPsychoEducationalAssessment_Y').prop("checked") && $('#IsPsychoEducationalAssessment').val() == ''){
			$('#IsPsychoEducationalAssessment').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#IsPsychoEducationalAssessment');
			isValid = false;
		}
		
		if($('#PsychoEducationalAssessmentDate').val().trim()!='' && !re.test($('#PsychoEducationalAssessmentDate').val().trim())){
			if($('#PsychoEducationalAssessmentDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errPsychoEducationalAssessmentDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errPsychoEducationalAssessmentDate').removeClass('hide');
				focusElement = $('#PsychoEducationalAssessmentDate');
			}	
	    	isValid = false;
		}
		
		
		if($('#IsPsychologicalAssessmentCounselling_Y').prop("checked") && $('#IsPsychologicalAssessmentCounselling').val() == ''){
			$('#IsPsychologicalAssessmentCounselling').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#IsPsychologicalAssessmentCounselling');
			isValid = false;
		}
		
		if($('#PsychologicalAssessmentCounsellingDate').val().trim()!='' && !re.test($('#PsychologicalAssessmentCounsellingDate').val().trim())){
			if($('#PsychologicalAssessmentCounsellingDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errPsychologicalAssessmentCounsellingDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errPsychologicalAssessmentCounsellingDate').removeClass('hide');
				focusElement = $('#PsychologicalAssessmentCounsellingDate');
			}	
	    	isValid = false;
		}
		
		if($('#IsRemedialInstructionTutoring_Y').prop("checked") && $('#IsRemedialInstructionTutoring').val() == ''){
			$('#IsRemedialInstructionTutoring').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#IsRemedialInstructionTutoring');
			isValid = false;
		}
		
		if($('#RemedialInstructionTutoringDate').val().trim()!='' && !re.test($('#RemedialInstructionTutoringDate').val().trim())){
			if($('#RemedialInstructionTutoringDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errRemedialInstructionTutoringDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errRemedialInstructionTutoringDate').removeClass('hide');
				focusElement = $('#RemedialInstructionTutoringDate');
			}	
	    	isValid = false;
		}
		
		if($('#IsIndividualizedProgramPlan_Y').prop("checked") && $('#IsIndividualizedProgramPlan').val() == ''){
			$('#IsIndividualizedProgramPlan').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#IsIndividualizedProgramPlan');
			isValid = false;
		}
		
		if($('#IndividualizedProgramPlanDate').val().trim()!='' && !re.test($('#IndividualizedProgramPlanDate').val().trim())){
			if($('#IndividualizedProgramPlanDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errIndividualizedProgramPlanDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errIndividualizedProgramPlanDate').removeClass('hide');
				focusElement = $('#IndividualizedProgramPlanDate');
			}	
	    	isValid = false;
		}
		
		if($('#IsOtherFormalAssessment_Y').prop("checked") && $('#IsOtherFormalAssessment').val() == ''){
			$('#IsOtherFormalAssessment').parent().find('.remark-warn').removeClass('hide');
			focusElement = $('#IsOtherFormalAssessment');
			isValid = false;
		}
		
		if($('#OtherFormalAssessmentDate').val().trim()!='' && !re.test($('#OtherFormalAssessmentDate').val().trim())){
			if($('#OtherFormalAssessmentDate').parent().find('.remark-warn').hasClass('hide')){
				$('#errOtherFormalAssessmentDate').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errOtherFormalAssessmentDate').removeClass('hide');
				focusElement = $('#OtherFormalAssessmentDate');
			}	
	    	isValid = false;
		}
		
		/**** Check required END ****/

		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkOtherForm);
});
</script>