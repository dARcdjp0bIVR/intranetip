<section id="studentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$Lang['Admission']['childDetails'] ?>
	</div>
	<div class="sheet">

		<div class="item">
			<div class="itemLabel">
				<?=$Lang['Admission']['childsName'] ?>
			</div>
			<span class="itemInput itemInput-half">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentEnglishSurname" id="StudentEnglishSurname" value="<?=$StudentInfo['EnglishSurname']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['surname'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$Lang['Admission']['surname'] ?></div>
				<div class="dataValue <?=$StudentInfo['EnglishSurname'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['EnglishSurname'] ? $StudentInfo['EnglishSurname'] : '－－'?></div>
			</span><span class="itemInput itemInput-half">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentEnglishFirstName" id="StudentEnglishFirstName" value="<?=$StudentInfo['EnglishFirstName']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['GivenName'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['givenName'] ?></div>
				<div class="dataValue <?=$StudentInfo['EnglishFirstName'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['EnglishFirstName'] ? $StudentInfo['EnglishFirstName'] : '－－'?></div>
			</span>
		</div>


		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentChineseName" id="StudentChineseName" value="<?=$StudentInfo['ChineseName']?>" required>
					<div class="textboxLabel requiredLabel">
    					<?=$Lang['Admission']['FH']['OtherName'] ?>
    					(<?=$Lang['Admission']['FH']['IncludingChineseName'] ?>)
					</div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel">
					<?=$Lang['Admission']['FH']['OtherName'] ?>
    				(<?=$Lang['Admission']['FH']['IncludingChineseName'] ?>)
				</div>
				<div class="dataValue <?=$StudentInfo['ChineseName'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['ChineseName'] ? $StudentInfo['ChineseName'] : '－－'?></div>
			</span>
		</div>

		<div class="item">
			<div class="itemLabel requiredLabel"><?=$Lang['Admission']['gender'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="StudentGender_M" name="StudentGender" value="M" required <?=$StudentInfo['Gender'] == "M"?'checked':'' ?>>
					<label for="StudentGender_M"><?=$Lang['Admission']['genderType']['M'] ?></label>
				</span>
				<span>
					<input type="radio" id="StudentGender_F" name="StudentGender" value="F" required <?=$StudentInfo['Gender'] == "F"?'checked':'' ?>>
					<label for="StudentGender_F"><?=$Lang['Admission']['genderType']['F'] ?></label>
    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue <?=$StudentInfo['Gender'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['Gender'] ? $StudentInfo['Gender'] : '－－'?></div>
			</div>
		</div>

		<div class="item">
			<div class="itemLabel requiredLabel"><?=$Lang['Admission']['FH']['dateofbirth'] ?></div>
			<span class="itemInput itemInput-selector">
				<span class="selector">
					<select id="StudentDateOfBirthYear" name="StudentDateOfBirthYear" class="dob dateYear">
						<option value="" hidden><?=$Lang['Admission']['year'] ?></option>
						<?php 
						if($application_setting['DOBStart'] && $application_setting['DOBEnd']){
    						$startYear = substr($application_setting['DOBStart'], 0, 4);
    						$endYear = substr($application_setting['DOBEnd'], 0, 4);
						}else{
    						$startYear = date('Y') - 10;
    						$endYear = date('Y');
						}
						for($i=$endYear;$i>=$startYear;$i--): 
						?>
							<option value="<?=$i ?>" <?=$StudentInfo['DateOfBirthYear'] == $i ? 'selected' : ''?>><?= $i ?></option>
						<?php 
						endfor; 
						?>
					</select>
				</span><span class="selector">
					<select id="StudentDateOfBirthMonth" name="StudentDateOfBirthMonth" class="dob dateMonth">
						<option value="" hidden><?=$Lang['Admission']['month'] ?></option>
						<?php for($i=1;$i<=12;$i++): ?>
							<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$StudentInfo['DateOfBirthMonth'] == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
						<?php endfor; ?>
					</select>
				</span><span class="selector">
					<select id="StudentDateOfBirthDay" name="StudentDateOfBirthDay" class="dob dateDay">
						<option value="" hidden><?=$Lang['Admission']['day'] ?></option>
						<?php for($i=1;$i<=31;$i++): ?>
							<option value="<?=str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?=$StudentInfo['DateOfBirthDay'] == $i ? 'selected' : ''?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
						<?php endfor; ?>
					</select>
				</span>
				<span>
					<div class="remark remark-warn hide" id="errDobRequired"><?=$Lang['Admission']['pleaseSelect'] ?></div>
					<div class="remark remark-warn hide" id="errDobRange"><?=$Lang['Admission']['FH']['msg']['invalidbdaydateformat'] ?></div>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue <?=$StudentInfo['DateOfBirthYear'] && $StudentInfo['DateOfBirthMonth'] && $StudentInfo['DateOfBirthDay'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['DateOfBirthYear'] && $StudentInfo['DateOfBirthMonth'] && $StudentInfo['DateOfBirthDay']? $StudentInfo['DateOfBirthYear'].'-'.$StudentInfo['DateOfBirthMonth'].'-'.$StudentInfo['DateOfBirthDay'] : '－－'?></div>
			</div>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentBirthCertNo" id="StudentBirthCertNo" value="<?=$BirthCertNo?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['birthcertno'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentBirthCertNo"></div>
					<div class="remark">(<?=$Lang['Admission']['BirthcertNoHints'] ?>)</div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['birthcertno'] ?></div>
				<div class="dataValue <?=$BirthCertNo ? '' : 'dataValue-empty'?>"><?=$BirthCertNo ? $BirthCertNo : '－－'?></div>
			</span>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentPlaceOfBirth" id="StudentPlaceOfBirth" value="<?=$StudentInfo['PlaceOfBirth'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['placeofbirth'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['placeofbirth'] ?></div>
				<div class="dataValue <?=$StudentInfo['PlaceOfBirth'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['PlaceOfBirth'] ? $StudentInfo['PlaceOfBirth'] : '－－'?></div>
			</span>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentNationality" id="StudentNationality" value="<?=$StudentInfo['Nationality']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['nationality'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['nationality'] ?></div>
				<div class="dataValue <?=$StudentInfo['Nationality'] ? '' : 'dataValue-empty'?>"><?=$StudentInfo['Nationality'] ? $StudentInfo['Nationality'] : '－－'?></div>
			</span>
		</div>


		<div class="item">
			<div class="itemLabel requiredLabel"><?=$Lang['Admission']['FH']['langspokenathome'] ?></div>
			<span class="itemInput">
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentFirstLanguage" id="StudentFirstLanguage" value="<?=$allCustInfo['FirstLanguage'][0]['Value']?>" required>
						<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['first'] ?></div>
    					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSecondLanguage" id="StudentSecondLanguage" value="<?=$allCustInfo['SecondLanguage'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['FH']['second'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentThirdLanguage" id="StudentThirdLanguage" value="<?=$allCustInfo['ThirdLanguage'][0]['Value']?>">
						<div class="textboxLabel"><?=$Lang['Admission']['FH']['third'] ?></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['FH']['first'] ?></div>
						<div class="dataValue <?=$allCustInfo['FirstLanguage'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['StudentFirstLanguage'][0]['Value'] ? $allCustInfo['StudentFirstLanguage'][0]['Value'] : '－－'?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['FH']['second'] ?></div>
						<div class="dataValue <?=$allCustInfo['SecondLanguage'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['SecondLanguage'][0]['Value'] ? $allCustInfo['StudentSecondLanguage'][0]['Value'] : '－－'?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['FH']['third'] ?></div>
						<div class="dataValue <?=$allCustInfo['ThirdLanguage'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ThirdLanguage'][0]['Value'] ? $allCustInfo['StudentThirdLanguage'][0]['Value'] : '－－'?></div>
					</div>
				</div>
			</span>
		</div>

	
		<div class="item">
			<div class="itemLabel requiredLabel"><?=$Lang['Admission']['FH']['previousSchool'] ?></div>
			<span class="itemInput">
				<div class="itemInput-complex-order">1</div>
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool0_Name" id="StudentSchool0_Name" value="<?=$StudentPrevSchoolInfo[0]['NameOfSchool'] ?>" required>
						<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['schoolName'] ?></div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool0_DatesAttendedFrom" id="StudentSchool0_DatesAttendedFrom" value="<?=$StudentPrevSchoolInfo[0]['StartDate'] ?>" required>
						<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['from'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentSchool0_DatesAttendedFrom"></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool0_DatesAttendedTo" id="StudentSchool0_DatesAttendedTo" value="<?=$StudentPrevSchoolInfo[0]['EndDate'] ?>" required>
						<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['to'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentSchool0_DatesAttendedTo"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['FH']['schoolName'] ?></div>
						<div class="dataValue <?=$StudentPrevSchoolInfo[0]['NameOfSchool'] ? '' : 'dataValue-empty'?>"><?=$StudentPrevSchoolInfo[0]['NameOfSchool'] ? $StudentPrevSchoolInfo[0]['NameOfSchool'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['from'] ?> (mm/yy)</div>
						<div class="dataValue <?=$StudentPrevSchoolInfo[0]['StartDate'] ? '' : 'dataValue-empty'?>"><?=$StudentPrevSchoolInfo[0]['StartDate'] ? $StudentPrevSchoolInfo[0]['StartDate'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['to'] ?> (mm/yy)</div>
						<div class="dataValue <?=$StudentPrevSchoolInfo[0]['EndDate'] ? '' : 'dataValue-empty'?>"><?=$StudentPrevSchoolInfo[0]['EndDate'] ? $StudentPrevSchoolInfo[0]['EndDate'] : '－－' ?></div>
					</div>
				</div>
			</span>

			<span class="itemInput">
				<div class="itemInput-complex-order">2</div>
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool1_Name" id="StudentSchool1_Name" value="<?=$StudentPrevSchoolInfo[1]['NameOfSchool'] ?>">
						<div class="textboxLabel"><?=$Lang['Admission']['FH']['schoolName'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text"  name="StudentSchool1_DatesAttendedFrom" id="StudentSchool1_DatesAttendedFrom" value="<?=$StudentPrevSchoolInfo[1]['StartDate'] ?>">
						<div class="textboxLabel"><?=$Lang['Admission']['from'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide" id="errStudentSchool1_DatesAttendedFrom"></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool1_DatesAttendedTo" id="StudentSchool1_DatesAttendedTo" value="<?=$StudentPrevSchoolInfo[1]['EndDate'] ?>">
						<div class="textboxLabel"><?=$Lang['Admission']['to'] ?> (mm/yy)</div>
						<div class="remark remark-warn hide" id="errStudentSchool1_DatesAttendedTo"></div>
					</div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['FH']['schoolName'] ?></div>
						<div class="dataValue <?=$StudentPrevSchoolInfo[1]['NameOfSchool'] ? '' : 'dataValue-empty'?>"><?=$StudentPrevSchoolInfo[1]['NameOfSchool'] ? $StudentPrevSchoolInfo[1]['NameOfSchool'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['from'] ?> (mm/yy)</div>
						<div class="dataValue <?=$StudentPrevSchoolInfo[1]['StartDate'] ? '' : 'dataValue-empty'?>"><?=$StudentPrevSchoolInfo[1]['StartDate'] ? $StudentPrevSchoolInfo[1]['StartDate'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$Lang['Admission']['to'] ?> (mm/yy)</div>
						<div class="dataValue <?=$StudentPrevSchoolInfo[1]['EndDate'] ? '' : 'dataValue-empty'?>"><?=$StudentPrevSchoolInfo[1]['EndDate'] ? $StudentPrevSchoolInfo[1]['EndDate'] : '－－' ?></div>
					</div>
				</div>
			</span>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentEnrolmentToCommence" id="StudentEnrolmentToCommence" value="<?=$allCustInfo['StudentEnrolmentToCommence'][0]['Value']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['enrolmentToCommence'] ?> (mm/yy)</div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentEnrolmentToCommence"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['enrolmentToCommence'] ?></div>
				<div class="dataValue <?=$allCustInfo['StudentEnrolmentToCommence'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['StudentEnrolmentToCommence'][0]['Value'] ? $allCustInfo['StudentEnrolmentToCommence'][0]['Value'] : '－－'?></div>
			</span>
		</div>
		
		<div class="remark">* <?=$Lang['Admission']['requiredFields'] ?></div>

	</div>
</section>

<script>
$(function(){
	'use strict';
	
	$('#studentForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($inputText.index($(this)))).html($(this).val());
			$($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
			if(!$($dataValue.get($inputText.index($(this)))).html()){
				$($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
				$($dataValue.get($inputText.index($(this)))).html('－－');
			}
		});
		
		$inputRadio.on('change', function(e){
			$dataValue.html($(this).next('label').html());
			$dataValue.removeClass('dataValue-empty');
			if(!$dataValue.html()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
			
		$inputDateSelect.on('change', function(e){
			$dataValue.html($inputDateYearSelect.val()+'-'+$inputDateMonthSelect.val()+'-'+$inputDateDaySelect.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
		
		
	});
	
	function checkBirthCertNo(){
		var values = $("#form1").serialize();
		var res = null;
		/* check the birth cert number is applied or not */
	   $.ajax({
	       url: "../admission_form/ajax_get_birth_cert_no.php",
	       type: "post",
	       data: values,
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	            res = data;
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   return res;
	}
	
	window.checkStudentForm = (function(lifecycle){
		var isValid = true;
		var $studentForm = $('#studentForm');
	
		/******** Basic init START ********/
		//for mm/yy validation
		var re = /^(0[123456789]|10|11|12)\/\d{2}$/;
		/******** Basic init END ********/
	
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($studentForm);

		if(
			($('#StudentDateOfBirthYear').val() == '') ||
			($('#StudentDateOfBirthMonth').val() == '') ||
			($('#StudentDateOfBirthDay').val() == '')
		){
			$('#errDobRequired').removeClass('hide');
			$('#StudentDateOfBirthYear').focus();
			isValid = false;
		}
		
		if(
		(
			'<?=$application_setting['DOBStart']?>' !='' && 
			$('#StudentDateOfBirthYear').val()+'-'+$('#StudentDateOfBirthMonth').val()+'-'+$('#StudentDateOfBirthDay').val() < '<?=$application_setting['DOBStart']?>'
		) || (
			'<?=$application_setting['DOBEnd']?>' !='' && 
			$('#StudentDateOfBirthYear').val()+'-'+$('#StudentDateOfBirthMonth').val()+'-'+$('#StudentDateOfBirthDay').val() > '<?=$application_setting['DOBEnd']?>'
		)
		){
			if($('#errDobRequired').hasClass('hide')){
				$('#errDobRange').removeClass('hide');
				focusElement = $('#StudentDateOfBirthYear');
			}
			isValid = false;
		}
		
		if(!isUpdatePeriod && checkBirthCertNo() > 0){
			if($('#StudentBirthCertNo').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentBirthCertNo').html('<?=$Lang['Admission']['FH']['msg']['duplicateBirthCertNo']?>');
				$('#errStudentBirthCertNo').removeClass('hide');
				focusElement = $('#StudentBirthCertNo');
			}	
	    	isValid = false;
	    }
	    
	    if($('#StudentSchool0_DatesAttendedFrom').val().trim()!='' && !re.test($('#StudentSchool0_DatesAttendedFrom').val().trim())){
			if($('#StudentSchool0_DatesAttendedFrom').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentSchool0_DatesAttendedFrom').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errStudentSchool0_DatesAttendedFrom').removeClass('hide');
				focusElement = $('#StudentSchool0_DatesAttendedFrom');
			}	
	    	isValid = false;
		}
		
		if($('#StudentSchool0_DatesAttendedTo').val().trim()!='' && !re.test($('#StudentSchool0_DatesAttendedTo').val().trim())){
			if($('#StudentSchool0_DatesAttendedTo').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentSchool0_DatesAttendedTo').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errStudentSchool0_DatesAttendedTo').removeClass('hide');
				focusElement = $('#StudentSchool0_DatesAttendedTo');
			}	
	    	isValid = false;
		}
		
		if($('#StudentSchool1_DatesAttendedFrom').val().trim()!='' && !re.test($('#StudentSchool1_DatesAttendedFrom').val().trim())){
			if($('#StudentSchool1_DatesAttendedFrom').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentSchool1_DatesAttendedFrom').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errStudentSchool1_DatesAttendedFrom').removeClass('hide');
				focusElement = $('#StudentSchool1_DatesAttendedFrom');
			}	
	    	isValid = false;
		}
		
		if($('#StudentSchool1_DatesAttendedTo').val().trim()!='' && !re.test($('#StudentSchool1_DatesAttendedTo').val().trim())){
			if($('#StudentSchool1_DatesAttendedTo').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentSchool1_DatesAttendedTo').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errStudentSchool1_DatesAttendedTo').removeClass('hide');
				focusElement = $('#StudentSchool1_DatesAttendedTo');
			}	
	    	isValid = false;
		}
		
		if($('#StudentEnrolmentToCommence').val().trim()!='' && !re.test($('#StudentEnrolmentToCommence').val().trim())){
			if($('#StudentEnrolmentToCommence').parent().find('.remark-warn').hasClass('hide')){
				$('#errStudentEnrolmentToCommence').html('<?=$Lang['Admission']['FH']['msg']['dateYearFormatRemark']?>');
				$('#errStudentEnrolmentToCommence').removeClass('hide');
				focusElement = $('#StudentEnrolmentToCommence');
			}	
	    	isValid = false;
		}
		
		/**** Check required END ****/
		
		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkStudentForm);
});
</script>