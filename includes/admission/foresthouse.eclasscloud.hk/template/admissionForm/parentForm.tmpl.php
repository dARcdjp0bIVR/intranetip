
<section id="parentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$Lang['Admission']['parentGuardianDetails'] ?>
	</div>
	<div class="sheet">
		<div class="item item-half">
			<div class="text-inst">
				<?=$Lang['Admission']['FH']['fatherLegalGuardian'] ?>
			</div>
			
			<span class="itemInput">
				<div class="textbox-floatlabel textbox-half">
					<input type="text" name="G1EnglishSurname" id="G1EnglishSurname" value="<?=$ParentInfo['F']['EnglishSurname'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['surname'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
				<div class="textbox-floatlabel textbox-half">
					<input type="text" name="G1EnglishFirstName" id="G1EnglishFirstName" value="<?=$ParentInfo['F']['EnglishFirstName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['GivenName'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex-half">
					<div class="dataLabel"><?=$Lang['Admission']['surname'] ?></div>
					<div class="dataValue <?=$ParentInfo['F']['EnglishSurname'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['EnglishSurname'] ? $ParentInfo['F']['EnglishSurname'] : '－－' ?></div>
				</div><div class="itemData-complex-half">
					<div class="dataLabel"><?=$Lang['Admission']['FH']['GivenName'] ?></div>
					<div class="dataValue <?=$ParentInfo['F']['EnglishFirstName'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['EnglishFirstName'] ? $ParentInfo['F']['EnglishFirstName'] : '－－' ?></div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="FatherFirstLanguage" id="FatherFirstLanguage" value="<?=$allCustInfo['FatherFirstLanguage'][0]['Value']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['firstLanguage'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['firstLanguage'] ?></div>
				<div class="dataValue <?=$allCustInfo['FatherFirstLanguage'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['FatherFirstLanguage'][0]['Value'] ? $allCustInfo['FatherFirstLanguage'][0]['Value'] : '－－'?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="FatherOtherLanguages" id="FatherOtherLanguages" value="<?=$allCustInfo['FatherOtherLanguages'][0]['Value']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['otherLanguage'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['otherLanguage'] ?></div>
				<div class="dataValue <?=$allCustInfo['FatherOtherLanguages'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['FatherOtherLanguages'][0]['Value'] ? $allCustInfo['FatherOtherLanguages'][0]['Value'] : '－－'?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1Email" id="G1Email" value="<?=$ParentInfo['F']['Email'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['emailAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id='errG1Email'></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['emailAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['Email'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['Email'] ? $ParentInfo['F']['Email'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1OfficeAddress" id="G1OfficeAddress" value="<?=$ParentInfo['F']['OfficeAddress'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['homeAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['homeAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['OfficeAddress'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['OfficeAddress'] ? $ParentInfo['F']['OfficeAddress'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1Mobile" id="G1Mobile" value="<?=$ParentInfo['F']['Mobile'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['mobilePhone'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['mobilePhone'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['Mobile'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['Mobile'] ? $ParentInfo['F']['Mobile'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1OfficeTelNo" id="G1OfficeTelNo" value="<?=$ParentInfo['F']['OfficeTelNo'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['workPhone'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['workPhone'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['OfficeTelNo'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['OfficeTelNo'] ? $ParentInfo['F']['OfficeTelNo'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1Nationality" id="G1Nationality" value="<?=$ParentInfo['F']['Nationality'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['nationality'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['nationality'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['Nationality'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['Nationality'] ? $ParentInfo['F']['Nationality'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1JobPosition" id="G1JobPosition" value="<?=$ParentInfo['F']['JobPosition'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['occupationProfession'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['occupationProfession'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['JobPosition'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['JobPosition'] ? $ParentInfo['F']['JobPosition'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1Company" id="G1Company" value="<?=$ParentInfo['F']['Company'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['companyname'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['companyname'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['Company'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['Company'] ? $ParentInfo['F']['Company'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1HobbiesInterests" id="G1HobbiesInterests" value="<?=$ParentInfo['F']['HobbiesInterests'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['hobbiesInterests'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['hobbiesInterests'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['HobbiesInterests'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['HobbiesInterests'] ? $ParentInfo['F']['HobbiesInterests'] : '－－' ?></div>
			</span>
		</div><div class="item item-half">
			<div class="text-inst">
				<?=$Lang['Admission']['FH']['motherLegalGuardian'] ?>
			</div>

			<span class="itemInput">
				<div class="textbox-floatlabel textbox-half">
					<input type="text" name="G2EnglishSurname" id="G2EnglishSurname" value="<?=$ParentInfo['M']['EnglishSurname'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['surname'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
				<div class="textbox-floatlabel textbox-half">
					<input type="text" name="G2EnglishFirstName" id="G2EnglishFirstName" value="<?=$ParentInfo['M']['EnglishFirstName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['GivenName'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="itemData-complex-half">
					<div class="dataLabel"><?=$Lang['Admission']['surname'] ?></div>
					<div class="dataValue <?=$ParentInfo['M']['EnglishSurname'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['EnglishSurname'] ? $ParentInfo['M']['EnglishSurname'] : '－－' ?></div>
				</div><div class="itemData-complex-half">
					<div class="dataLabel"><?=$Lang['Admission']['FH']['GivenName'] ?></div>
					<div class="dataValue <?=$ParentInfo['M']['EnglishFirstName'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['EnglishFirstName'] ? $ParentInfo['M']['EnglishFirstName'] : '－－' ?></div>
				</div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="MotherFirstLanguage" id="MotherFirstLanguage" value="<?=$allCustInfo['MotherFirstLanguage'][0]['Value']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['firstLanguage'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['firstLanguage'] ?></div>
				<div class="dataValue <?=$allCustInfo['MotherFirstLanguage'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['MotherFirstLanguage'][0]['Value'] ? $allCustInfo['MotherFirstLanguage'][0]['Value'] : '－－'?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="MotherOtherLanguages" id="MotherOtherLanguages" value="<?=$allCustInfo['MotherOtherLanguages'][0]['Value']?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['otherLanguage'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['otherLanguage'] ?></div>
				<div class="dataValue <?=$allCustInfo['MotherOtherLanguages'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['MotherOtherLanguages'][0]['Value'] ? $allCustInfo['MotherOtherLanguages'][0]['Value'] : '－－'?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2Email" id="G2Email" value="<?=$ParentInfo['M']['Email'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['emailAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id='errG2Email'></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['emailAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['Email'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['Email'] ? $ParentInfo['M']['Email'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2OfficeAddress" id="G2OfficeAddress" value="<?=$ParentInfo['M']['OfficeAddress'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['homeAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['homeAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['OfficeAddress'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['OfficeAddress'] ? $ParentInfo['M']['OfficeAddress'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2Mobile" id="G2Mobile" value="<?=$ParentInfo['M']['Mobile'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['mobilePhone'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['mobilePhone'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['Mobile'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['Mobile'] ? $ParentInfo['M']['Mobile'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2OfficeTelNo" id="G2OfficeTelNo" value="<?=$ParentInfo['M']['OfficeTelNo'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['workPhone'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['workPhone'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['OfficeTelNo'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['OfficeTelNo'] ? $ParentInfo['M']['OfficeTelNo'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2Nationality" id="G2Nationality" value="<?=$ParentInfo['M']['Nationality'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['nationality'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['nationality'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['Nationality'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['Nationality'] ? $ParentInfo['M']['Nationality'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2JobPosition" id="G2JobPosition" value="<?=$ParentInfo['M']['JobPosition'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['occupationProfession'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['occupationProfession'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['JobPosition'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['JobPosition'] ? $ParentInfo['M']['JobPosition'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2Company" id="G2Company" value="<?=$ParentInfo['M']['Company'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['companyname'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['companyname'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['Company'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['Company'] ? $ParentInfo['M']['Company'] : '－－' ?></div>
			</span>
			
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G2HobbiesInterests" id="G2HobbiesInterests" value="<?=$ParentInfo['M']['HobbiesInterests'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$Lang['Admission']['FH']['hobbiesInterests'] ?></div>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$Lang['Admission']['FH']['hobbiesInterests'] ?></div>
				<div class="dataValue <?=$ParentInfo['M']['HobbiesInterests'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['M']['HobbiesInterests'] ? $ParentInfo['M']['HobbiesInterests'] : '－－' ?></div>
			</span>
		</div>
		<div class="item">
			<div class="itemLabel requiredLabel"><?=$Lang['Admission']['FH']['BothParentResidingHK'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="BothParentResidingHK_Y" name="BothParentResidingHK" value="Y" required <?=$allCustInfo['BothParentResidingHK'][0]['Value'] == "Y"?'checked':'' ?>>
					<label for="BothParentResidingHK_Y"><?=$Lang['Admission']['yes'] ?></label>
				</span>
				<span>
					<input type="radio" id="BothParentResidingHK_N" name="BothParentResidingHK" value="N" required <?=$allCustInfo['BothParentResidingHK'][0]['Value'] == "N"?'checked':'' ?>>
					<label for="BothParentResidingHK_N"><?=$Lang['Admission']['no'] ?></label>
    				<div class="remark remark-warn hide"><?=$Lang['Admission']['pleaseSelect'] ?></div>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue <?=$OtherInfo['BothParentResidingHK'] ? '' : 'dataValue-empty'?>"><?=$OtherInfo['BothParentResidingHK'] == "Y"?$Lang['Admission']['yes']:($OtherInfo['BothParentResidingHK'] == "N" ? $Lang['Admission']['no'] : '－－')?></div>
			</div>
		</div>
		
		<div class="remark">* <?=$Lang['Admission']['requiredFields'] ?></div>
	</div>
</section>

<script>
$(function(){
	'use strict';
	
	$('#parentForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($inputText.index($(this)))).html($(this).val());
			$($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
			if(!$($dataValue.get($inputText.index($(this)))).html()){
				$($dataValue.get($inputText.index($(this)))).html('－－');
				$($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$dataValue.html($inputRadio.next('label').html());
			$dataValue.removeClass('dataValue-empty');
			if(!$dataValue.html()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
	});
	
	window.checkParentForm = (function(lifecycle){
		var isValid = true;
		var $parentForm = $('#parentForm');

		/******** Basic init START ********/
		//for email validation
		var re = /\S+@\S+\.\S+/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($parentForm);
		
		if($('#G1Email').val().trim()!='' && !re.test($('#G1Email').val().trim())){
			if($('#G1Email').parent().find('.remark-warn').hasClass('hide')){
				$('#errG1Email').html('<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>');
				$('#errG1Email').removeClass('hide');
				focusElement = $('#G1Email');
			}	
	    	isValid = false;
		}
		
		if($('#G2Email').val().trim()!='' && !re.test($('#G2Email').val().trim())){
			if($('#G2Email').parent().find('.remark-warn').hasClass('hide')){
				$('#errG2Email').html('<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>');
				$('#errG2Email').removeClass('hide');
				focusElement = $('#G2Email');
			}	
	    	isValid = false;
		}
		/**** Check required END ****/

		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkParentForm);
});
</script>