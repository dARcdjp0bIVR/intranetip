<?php
global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$parentTypeArr = array(
    1 => 'F',
    2 => 'M'
);

?>
<style>
.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%">
		<col style="width: 40%">
		<col style="width: 40%">
	</colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['FH']['fatherLegalGuardian'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['FH']['motherLegalGuardian'] ?></center></td>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['englishname']?>
		</td>
		<?php
foreach ($parentTypeArr as $index => $parentType) {
    if ($ParentInfo[$parentType]['EnglishSurname'] && $ParentInfo[$parentType]['EnglishFirstName']) {
        $name = "{$ParentInfo[$parentType]['EnglishSurname']}, {$ParentInfo[$parentType]['EnglishFirstName']}";
    } else {
        $name = '';
    }
    ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $name )?>
        	</td>
    	<?php
}
?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['firstLanguage'] ?>
		</td>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["FatherFirstLanguage"][0]['Value'] )?>
        	</td>
        	<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["MotherFirstLanguage"][0]['Value'] )?>
        	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['otherLanguage'] ?>
		</td>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["FatherOtherLanguages"][0]['Value'] )?>
        	</td>
        	<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["MotherOtherLanguages"][0]['Value'] )?>
        	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['emailAddress']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Email'] )?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['homeAddress']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['OfficeAddress'] )?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['mobilePhone']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Mobile'] )?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['workPhone']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['OfficeTelNo'] )?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['nationality']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Nationality'] )?>
        	</td>
    	<?php } ?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['occupationProfession']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['JobPosition'] )?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['companyname']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Company'] )?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['hobbiesInterests']?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["G{$index}HobbiesInterests"][0]['Value'] )?>
        	</td>
    	<?php } ?>
	</tr>
</table>
<table class="form_table" style="font-size: 13px">
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['BothParentResidingHK']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['BothParentResidingHK'][0]['Value'] == "Y" ?  $kis_lang['Admission']['yes']: ($allCustInfo['BothParentResidingHK'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : '') )?>
    	</td>
	</tr>
</table>