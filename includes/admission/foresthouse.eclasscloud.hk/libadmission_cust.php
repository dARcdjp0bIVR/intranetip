<?php
// modifying by:
/**
 * ******************
 * Change Log :
 * Date 2018-09-03 [Pun]
 * File Created
 *
 * ******************
 */
include_once ("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");

class admission_cust extends \AdmissionSystem\AdmissionCustBase
{

    const STUDENT_ACADEMIC_SCHOOL_COUNT = 2;

    public function __construct()
    {
        global $plugin;
        
        if ($plugin['eAdmission_devMode']) {
            error_reporting(E_ALL & ~ E_NOTICE);
            ini_set('display_errors', 1);
            if ($_SERVER['HTTP_HOST'] == '192.168.0.171:31002') {
                $this->AdmissionFormSendEmail = false;
            }
        }
        
        parent::__construct();
        $this->init();
    }

    private function init()
    {
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function ($emailTitle) {
            global $Lang;
            return $Lang['Admission']['FH']['email']['admissionNotification'];
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE END #### */
        
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummySchoolInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */
        
        /* #### ACTION_APPLICANT_UPDATE_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateStudentInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateParentInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateStudentAcademicBackgroundInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateFamilyLanguageProfileInfo'
        ));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateOtherInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_INFO END #### */
        
        /* #### ACTION_APPLICANT_UPDATE_*_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INFO, array(
            $this,
            'updateApplicantInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_*_INFO END #### */
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID)
    {
        $result = true;
        
        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i ++) {
            $sql = "INSERT INTO 
                ADMISSION_STU_PREV_SCHOOL_INFO 
            (
                ApplicationID, 
                SchoolOrder,
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";
            
            $result = $result && $this->db_db_query($sql);
        }
        
        if (! $result) {
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentInfo($Data, $ApplicationID)
    {
        // #### Basic info START ####
        $fieldArr = array(
            'ChineseName' => 'StudentChineseName',
//            'ChineseFirstName' => 'StudentChineseFirstName',
            'EnglishSurname' => 'StudentEnglishSurname',
            'EnglishFirstName' => 'StudentEnglishFirstName',
            'DOB' => 'StudentDateOfBirth',
            'Gender' => 'StudentGender',
//            'BirthCertTypeOther' => 'StudentBirthCertType',
            'BirthCertNo' => 'StudentBirthCertNo',
            'Nationality' => 'StudentNationality',
//            'Address' => 'StudentAddress',
            'PlaceOfBirth' => 'StudentPlaceOfBirth',
//            'PlaceOfBirthOther' => 'StudentNativePlace',
//            'Email' => 'StudentEmail',
//            'LangSpokenAtHome' => 'LangSpokenAtHome'
        );
        
        $updateSql = '';
        foreach ($fieldArr as $dbField => $dataField) {
            if (isset($Data[$dataField])) {
                $updateSql .= "$dbField = '{$Data[$dataField]}',";
            }
        }
        
        // ### Concat name START ####
//        if ($Data['StudentChineseSurname'] && $Data['StudentChineseFirstName']) {
//            $updateSql .= "ChineseName = '{$Data['StudentChineseSurname']}{$Data['StudentChineseFirstName']}',";
//        }
        if ($Data['StudentEnglishSurname'] && $Data['StudentEnglishFirstName']) {
            $updateSql .= "EnglishName = '{$Data['StudentEnglishSurname']}, {$Data['StudentEnglishFirstName']}',";
        }
        if ($Data['StudentDateOfBirthYear'] && $Data['StudentDateOfBirthMonth']  && $Data['StudentDateOfBirthDay']) {
            $updateSql .= "DOB = '{$Data['StudentDateOfBirthYear']}-{$Data['StudentDateOfBirthMonth']}-{$Data['StudentDateOfBirthDay']}',";
        }
        // ### Concat name END ####
        
        $sql = "UPDATE 
            ADMISSION_STU_INFO
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'";
        $result = $this->db_db_query($sql);
        // #### Basic info END ####
        
        // #### Cust info START ####
        $fieldArr = array(
            'StudentEnrolmentToCommence'
        );
        foreach ($fieldArr as $field) {
            if (isset($Data[$field])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field]
                ), $insertIfNotExists = true);
            }
        }
        // #### Cust info END ####
        
        if (! $result) {
            throw new \Exception('Cannot update student info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateParentInfo($Data, $ApplicationID)
    {
        $result = true;
        
        $pgType = array(
            1 => 'F',
            2 => 'M',
            3 => 'G'
        );
        
        /* #### Update parent basic info START #### */
        $fieldArr = array(
            'EnglishSurname',
            'EnglishFirstName',
//            'ChineseSurname',
//            'ChineseFirstName',
            'Nationality',
            'Company',
            'JobPosition',
            'OfficeAddress',
            'OfficeTelNo',
            'Mobile',
            'Email'
        );
        
        foreach ($pgType as $id => $type) {
            $updateSQL = '';
            foreach ($fieldArr as $field) {
                if (isset($Data["G{$id}{$field}"])) {
                    $updateSQL .= "{$field} = '{$Data["G{$id}{$field}"]}',";
                }
            }
            
            // #### Concat name START ####
            if ($Data["G{$id}ChineseSurname"] && $Data["G{$id}ChineseFirstName"]) {
                $updateSQL .= "ChineseName = '{$Data["G{$id}ChineseSurname"]}{$Data["G{$id}ChineseFirstName"]}',";
            }
            if ($Data["G{$id}EnglishSurname"] && $Data["G{$id}EnglishFirstName"]) {
                $updateSQL .= "EnglishName = '{$Data["G{$id}EnglishSurname"]}, {$Data["G{$id}EnglishFirstName"]}',";
            }
            // #### Concat name END ####
            
            $sql = "UPDATE 
                ADMISSION_PG_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                PG_TYPE = '{$type}'";
            $result = $result && $this->db_db_query($sql);
        }
        /* #### Update parent basic info END #### */
        
        /* #### Update parent extra info START #### */
        foreach ($pgType as $id => $type) {
            if (isset($Data["G{$id}HobbiesInterests"])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => "G{$id}HobbiesInterests",
                    'Value' => $Data["G{$id}HobbiesInterests"]
                ), $insertIfNotExists = true);
            }
        }
        /* #### Update parent extra info END #### */
        
        /* #### Update Cust info START #### */
//        if (isset($Data['InvoiceTo'])) {
//            $result = $result && $this->updateApplicationCustInfo(array(
//                'filterApplicationId' => $ApplicationID,
//                'filterCode' => 'InvoiceTo',
//                'Value' => $Data['InvoiceTo']
//            ), $insertIfNotExists = true);
//        }
//        
        if (isset($Data['BothParentResidingHK'])) {
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'BothParentResidingHK',
                'Value' => $Data['BothParentResidingHK']
            ), $insertIfNotExists = true);
        }
        /* #### Update Cust info START #### */
        
        if (! $result) {
            throw new \Exception('Cannot update parent info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentAcademicBackgroundInfo($Data, $ApplicationID)
    {
        $result = true;
        
        /* #### Update school basic info START #### */
        $fieldArr = array(
            'NameOfSchool' => 'Name',
            'StartDate' => 'DatesAttendedFrom',
            'EndDate' => 'DatesAttendedTo'
        );
        
        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i ++) {
            $updateSQL = '';
            
            foreach ($fieldArr as $dbField => $inputField) {
                if (isset($Data["StudentSchool{$i}_{$inputField}"])) {
                	if ($inputField == 'DatesAttendedFrom') {
			            $datesAttendedFrom = explode('/', $Data["StudentSchool{$i}_DatesAttendedFrom"]);
			            $Data["StudentSchool{$i}_DatesAttendedFrom"] = '20'.$datesAttendedFrom[1].'-'. $datesAttendedFrom[0].'-01';
		            }
		            else if ($inputField == 'DatesAttendedTo') {
			            $datesAttendedTo = explode('/', $Data["StudentSchool{$i}_DatesAttendedTo"]);
			            $Data["StudentSchool{$i}_DatesAttendedTo"] = '20'.$datesAttendedTo[1].'-'. $datesAttendedTo[0].'-01';
		            }
                    $updateSQL .= "{$dbField} = '{$Data["StudentSchool{$i}_{$inputField}"]}',";
                }
            }
            
            $sql = "UPDATE
                ADMISSION_STU_PREV_SCHOOL_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                SchoolOrder = '{$i}'";
            $result = $result && $this->db_db_query($sql);
        }
        /* #### Update school basic info END #### */
        
        /* #### Update cust info START #### */
        $fieldArr = array(
            'ChildPlacedOutStandardGroup',
            'ChildPlacedOutStandardGroup_Details',
            'ChildSpecialClassTalent',
            'ChildSpecialClassTalent_Details',
            'ChildEducationalPsychologist',
            'ChildEducationalPsychologist_Details',
            'StudentFavouriteSubject',
            'StudentSuccessfulSubject',
            'StudentChallengingSubject'
        );
        foreach ($fieldArr as $field) {
            if (isset($Data[$field])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field]
                ), $insertIfNotExists = true);
            }
        }
        /* #### Update cust info END #### */
        
        if (! $result) {
            throw new \Exception('Cannot update student academic background info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateFamilyLanguageProfileInfo($Data, $ApplicationID)
    {
        $result = true;
        
        $Data['StudentForeignLanguage'] = (array) $Data['StudentForeignLanguage'];
        if (! in_array(6, $Data['StudentForeignLanguage'])) {
            $Data['StudentForeignLanguage_othersDetails'] = '';
        }
        $Data['StudentForeignLanguage'] = implode(',', $Data['StudentForeignLanguage']);
        
        $fieldArr = array(
            'StudentFirstLanguage',
            'StudentSecondLanguage',
            'StudentThirdLanguage',
            'MotherFirstLanguage',
            'MotherOtherLanguages',
            'FatherFirstLanguage',
            'FatherOtherLanguages'
        );
        foreach ($fieldArr as $field) {
            if (isset($Data[$field])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field]
                ), $insertIfNotExists = true);
            }
        }
        
        if (! $result) {
            throw new \Exception('Cannot update family language info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
//    protected function updateSiblingInfo($Data, $ApplicationID)
//    {
//        $result = true;
//        
//        $fieldArr = array(
//            'EnglishName' => 'Name',
//            'Gender' => 'Gender',
//            'DOB' => 'DOB',
//            'CurrentSchool' => 'CurrentSchool',
//            'ApplyingSameSchool' => 'ApplyingSameSchool'
//        );
//        
//        for ($i = 0; $i < self::SIBLING_COUNT; $i ++) {
//            $updateSQL = '';
//            foreach ($fieldArr as $dbField => $field) {
//                if (isset($Data["Sibling{$i}_{$field}"])) {
//                    $updateSQL .= "{$dbField} = '{$Data["Sibling{$i}_{$field}"]}',";
//                }
//            }
//            
//            $sql = "UPDATE
//                ADMISSION_SIBLING
//            SET
//                {$updateSQL}
//                DateModified = NOW(),
//                ModifiedBy = '{$this->uid}'
//            WHERE
//                ApplicationID = '{$ApplicationID}'
//            AND
//                SiblingOrder = '{$i}'";
//            $result = $result && $this->db_db_query($sql);
//        }
//        
//        if (! $result) {
//            throw new \Exception('Cannot update sibling info');
//        }
//        return true;
//    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateOtherInfo($Data, $ApplicationID)
    {
        $result = true;
        
        $fieldArr = array(
            'HowHearSchool',
            'DescribeExpertise',
            'HaveParticipatedActivity',
            'WhyChooseSchool',
            'HasSibling',
            'DescribeChildPersonality',
            'DescribeChildRoutine',
            'DescribeChildRelationship',
            'MedicalConcern',
            'IsSpeechTherapy',
            'SpeechTherapyDate',
            'IsOccupationalTherapy',
            'OccupationalTherapyDate',
            'IsPsychoEducationalAssessment',
            'PsychoEducationalAssessmentDate',
            'IsPsychologicalAssessmentCounselling',
            'PsychologicalAssessmentCounsellingDate',
            'IsRemedialInstructionTutoring',
            'RemedialInstructionTutoringDate',
            'IsIndividualizedProgramPlan',
            'IndividualizedProgramPlanDate',
            'IsOtherFormalAssessment',
            'OtherFormalAssessmentName',
            'OtherFormalAssessmentDate'
        );
        foreach ($fieldArr as $field) {
            if (isset($Data[$field])) {
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field]
                ), $insertIfNotExists = true);
            }
        }
        
        if (isset($Data['lang'])) {
            $sql = "UPDATE 
                ADMISSION_OTHERS_INFO 
            SET 
                Lang='{$Data['lang']}',
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE 
                ApplicationID = '{$ApplicationID}'
            ";
            $result = $result && $this->db_db_query($sql);
        }
        
        if (! $result) {
            throw new \Exception('Cannot update other info');
        }
        return true;
    }

    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID)
    {
        $result = true;
        
        if ($type == 'studentinfo') {
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);
            $result = $result && $this->updateStudentAcademicBackgroundInfo($Data, $ApplicationID);
            $result = $result && $this->updateFamilyLanguageProfileInfo($Data, $ApplicationID);
        } elseif ($type == 'parentinfo') {
            $result = $result && $this->updateParentInfo($Data, $ApplicationID);
            $result = $result && $this->updateFamilyLanguageProfileInfo($Data, $ApplicationID);
        }elseif ($type == 'otherinfo') {
            $result = $result && $this->updateOtherInfo($Data, $ApplicationID);
        } elseif ($type == 'remarks') {
            $result = $result && $this->updateApplicationStatus($Data, $ApplicationID);
        } else {
            $result = false;
        }
        
        if (! $result) {
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }

    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '',$classLevelID = '')
    {
        global $kis_lang;
        
        $headerArray = array();
        
        $headerArray[] = $kis_lang['Admission']['admissiondate'];
        $headerArray[] = $kis_lang['applicationno'];
        $headerArray[] = $kis_lang['Admission']['applyLevel'];
        
        // for student info
        $headerArray['studentInfo'][] = $kis_lang['Admission']['surname'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['FH']['GivenName'];
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['OtherName']} ({$kis_lang['Admission']['FH']['IncludingChineseName']})";
        $headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['munsang']['birthcertno'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['nationality'];
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['langspokenathome']} ({$kis_lang['Admission']['FH']['first']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['langspokenathome']} ({$kis_lang['Admission']['FH']['second']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['langspokenathome']} ({$kis_lang['Admission']['FH']['third']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['previousSchool']}(1) ({$kis_lang['Admission']['FH']['schoolName']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['previousSchool']}(1) ({$kis_lang['Admission']['from']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['previousSchool']}(1) ({$kis_lang['Admission']['to']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['previousSchool']}(2) ({$kis_lang['Admission']['FH']['schoolName']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['previousSchool']}(2) ({$kis_lang['Admission']['from']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['FH']['previousSchool']}(2) ({$kis_lang['Admission']['to']})";
        $headerArray['studentInfo'][] = $kis_lang['Admission']['FH']['enrolmentToCommence'];
        
        // for parent info
        $headerArray['parentInfo'][] = $kis_lang['Admission']['surname'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['GivenName'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['firstLanguage'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['otherLanguage'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['emailAddress'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['homeAddress'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['mobilePhone'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['workPhone'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['nationality'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['occupationProfession'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['companyname'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['FH']['hobbiesInterests'];
        $headerArray['parentInfo2'][] = $kis_lang['Admission']['FH']['BothParentResidingHK'];
        
        // for other info
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['howHearSchool'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['haveParticipatedActivity'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['describeExpertise'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['whyChooseSchool'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['hasSibling'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['describeChildPersonality'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['describeChildRoutine'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['describeChildRelationship'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['FH']['medicalConcern'];
        
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['speechTherapy']})";
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['occupationalTherapy']})";
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['psychoEducationalAssessment']})";
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['psychologicalAssessmentCounselling']})";
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['remedialInstructionTutoring']})";
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['individualizedProgramPlan']})";
        $headerArray['otherInfo'][] = "{$kis_lang['Admission']['FH']['hasFormalAssessment']} ({$kis_lang['Admission']['FH']['other']})";
        
        // for official use
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod']; // Omas
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['receiptcode'] . ")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['date'] . ")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['handler'] . ")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
//        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (1)";
//        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (2)";
//        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (3)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
        
        // ####### Header START ########
        // ### Admission Info START ####
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        // ### Admission Info END ####
        
        // ### Student Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['childDetails'];
        for ($i = 0; $i < count($headerArray['studentInfo']) - 1; $i ++) {
            $exportColumn[0][] = "";
        }
        // ### Student Info END ####
        
        // ### Parent Info START ####
        $exportColumn[0][] = " {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['F']})";
        for ($i = 0; $i < count($headerArray['parentInfo']) - 1; $i ++) {
            $exportColumn[0][] = "";
        }
        
        $exportColumn[0][] = " {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['M']})";
        for ($i = 0; $i < count($headerArray['parentInfo']) - 1; $i ++) {
            $exportColumn[0][] = "";
        }
        
        for ($i = 0; $i < count($headerArray['parentInfo2']); $i ++) {
            $exportColumn[0][] = "";
        }
        // ### Parent Info END ####
        
        // ### Other Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
        for ($i = 0; $i < count($headerArray['otherInfo']) - 1; $i ++) {
            $exportColumn[0][] = "";
        }
        // ### Other Info END ####
        
        // ### Offical Use START ####
        $exportColumn[0][] = $kis_lang['remarks'];
        for ($i = 0; $i < count($headerArray['officialUse']) - 1; $i ++) {
            $exportColumn[0][] = "";
        }
        // ### Offical Use END ####
        
        // sub header
        $exportColumn[1] = array_merge(array(
            $headerArray[0],
            $headerArray[1],
            $headerArray[2]
        ), $headerArray['studentInfo'], $headerArray['parentInfo'], $headerArray['parentInfo'], $headerArray['parentInfo2'], $headerArray['otherInfo'], $headerArray['officialUse']);
        
        return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;
        
        // ######## Get data START ########
        // ### Get student Info START ####
        $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        // ### Get student Info END ####
        
        // ### Get parent Info START ####
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
        $parentInfoArr = array();
        foreach ($parentInfo as $parent) {
            $ParentInfo[$parent['type']] = $parent;
        }
        // ### Get parent Info END ####
        
        // ### Get academic background START ####
        $StudentPrevSchoolInfo = $this->getApplicationPrevSchoolInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID);
        foreach($StudentPrevSchoolInfo as $index => $school){
		    if($school['StartDate'] == '0000-00-00'){
		        $StudentPrevSchoolInfo[$index]['StartDate'] = '';
		    }else{
		        $StudentPrevSchoolInfo[$index]['StartDate'] = date("m/Y", strtotime($school['StartDate']));
		    }
		    if($school['EndDate'] == '0000-00-00'){
		        $StudentPrevSchoolInfo[$index]['EndDate'] = '';
		    }else{
		        $StudentPrevSchoolInfo[$index]['EndDate'] = date("m/Y", strtotime($school['EndDate']));
		    }
		}
        // ### Get academic background END ####
        
        // ### Get other info START ####
        $allCustInfo = $this->getAllApplicationCustInfo($StudentInfo['applicationID']);
        
        $allClassLevel = $this->getClassLevel();
        $classLevel = $this->getClassLevel();
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
        // ### Get other info END ####
        // ######## Get data END ########
        
        $dataArray = array();
        
        // ######## Export data START ########
        // ### Basic Info START ####
        $dataArray[] = substr($otherInfo['DateInput'], 0, - 9);
        $dataArray[] = $StudentInfo['applicationID'];
        $dataArray[] = $classLevel[$otherInfo['classLevelID']];
        // ### Basic Info END ####
        
        // ### Student Info START ####
        $dataArray['studentInfo'][] = $StudentInfo['EnglishSurname'];
        $dataArray['studentInfo'][] = $StudentInfo['EnglishFirstName'];
        $dataArray['studentInfo'][] = $StudentInfo['ChineseName'];
        $dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$StudentInfo['Gender']];
        $dataArray['studentInfo'][] = ($StudentInfo['DOB'] == '0000-00-00') ? '' : $StudentInfo['DOB'];
        $dataArray['studentInfo'][] = $StudentInfo['BirthCertNo'];
        $dataArray['studentInfo'][] = $StudentInfo['PlaceOfBirth'];
        $dataArray['studentInfo'][] = $StudentInfo['Nationality'];
        $dataArray['studentInfo'][] = $allCustInfo['StudentFirstLanguage'][0]['Value'];
        $dataArray['studentInfo'][] = $allCustInfo['StudentSecondLanguage'][0]['Value'];
        $dataArray['studentInfo'][] = $allCustInfo['StudentThirdLanguage'][0]['Value'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[0]['NameOfSchool'];
        $dataArray['studentInfo'][] = ($StudentPrevSchoolInfo[0]['StartDate'] == '0000-00-00') ? '' : $StudentPrevSchoolInfo[0]['StartDate'];
        $dataArray['studentInfo'][] = ($StudentPrevSchoolInfo[0]['EndDate'] == '0000-00-00') ? '' : $StudentPrevSchoolInfo[0]['EndDate'];
        $dataArray['studentInfo'][] = $StudentPrevSchoolInfo[1]['NameOfSchool'];
        $dataArray['studentInfo'][] = ($StudentPrevSchoolInfo[1]['StartDate'] == '0000-00-00') ? '' : $StudentPrevSchoolInfo[1]['StartDate'];
        $dataArray['studentInfo'][] = ($StudentPrevSchoolInfo[1]['EndDate'] == '0000-00-00') ? '' : $StudentPrevSchoolInfo[1]['EndDate'];
        $dataArray['studentInfo'][] = $allCustInfo['StudentEnrolmentToCommence'][0]['Value'];
        // ### Student Info END ####
        
        // ### Parent Info START ####
        $types = array(
            'F',
            'M'
        );
        foreach ($types as $type) {
            $dataArray['parentInfo'][] = $ParentInfo[$type]['EnglishSurname'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['EnglishFirstName'];
            if($type == 'F'){
            	$dataArray['parentInfo'][] = $allCustInfo['FatherFirstLanguage'][0]['Value'];
            	$dataArray['parentInfo'][] = $allCustInfo['FatherOtherLanguages'][0]['Value'];
            }
            else{
            	$dataArray['parentInfo'][] = $allCustInfo['MotherFirstLanguage'][0]['Value'];
            	$dataArray['parentInfo'][] = $allCustInfo['MotherOtherLanguages'][0]['Value'];
            }
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Email'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['OfficeAddress'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Mobile'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['OfficeTelNo'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Nationality'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['JobPosition'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Company'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['HobbiesInterests'];
        }
        $dataArray['parentInfo'][] = $allCustInfo['BothParentResidingHK'][0]['Value'] == "Y"? $Lang['Admission']['yes'] : $Lang['Admission']['no'];
        // ### Parent Info END ####

        // ### Other Info START ####
        $dataArray['otherInfo'][] = $allCustInfo['HowHearSchool'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['HaveParticipatedActivity'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['DescribeExpertise'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['WhyChooseSchool'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['HasSibling'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['DescribeChildPersonality'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['DescribeChildRoutine'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['DescribeChildRelationship'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['MedicalConcern'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['IsSpeechTherapy'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['SpeechTherapyDate'][0]['Value'].')':($allCustInfo['IsSpeechTherapy'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        $dataArray['otherInfo'][] = $allCustInfo['IsOccupationalTherapy'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['OccupationalTherapyDate'][0]['Value'].')':($allCustInfo['IsOccupationalTherapy'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        $dataArray['otherInfo'][] = $allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['PsychoEducationalAssessmentDate'][0]['Value'].')':($allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        $dataArray['otherInfo'][] = $allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value'].')':($allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        $dataArray['otherInfo'][] = $allCustInfo['IsRemedialInstructionTutoring'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['RemedialInstructionTutoringDate'][0]['Value'].')':($allCustInfo['IsRemedialInstructionTutoring'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        $dataArray['otherInfo'][] = $allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['IndividualizedProgramPlanDate'][0]['Value'].')':($allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        $dataArray['otherInfo'][] = $allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "Y"?$Lang['Admission']['yes'].' ('.$allCustInfo['OtherFormalAssessmentName'][0]['Value'].')('.$allCustInfo['OtherFormalAssessmentDate'][0]['Value'].')':($allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "N"?$Lang['Admission']['no']:'');
        // ### Other Info END ####
        
        // ### Official Use START ####
        $dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
        $dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
        $dataArray['officialUse'][] = $status['receiptID'];
        $dataArray['officialUse'][] = $status['receiptdate'];
        $dataArray['officialUse'][] = $status['handler'];
        $dataArray['officialUse'][] = $status['interviewdate'];
		$dataArray['officialUse'][] = $status['interviewlocation'];
//        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
//        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, - 3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, - 3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
//        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
//        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, - 3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, - 3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
//        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
//        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, - 3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, - 3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
        $dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
        $dataArray['officialUse'][] = $status['remark'];
        // ### Official Use END ####
        
        $ExportArr = array_merge(array(
            $dataArray[0],
            $dataArray[1],
            $dataArray[2]
        ), $dataArray['studentInfo'], $dataArray['parentInfo'], $dataArray['otherInfo'], $dataArray['officialUse']);
        // ######## Export data END ########
        
        return $ExportArr;
    }
    
    function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
//		$sql = "SELECT 
//					f.RecordID as UserID,
//					a.ApplicationID as ApplicationNo,
//					a.ChineseName,
//					a.EnglishName,
//					p.Email as Email
//				FROM ADMISSION_OTHERS_INFO as f 
//				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
//				LEFT JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=a.ApplicationID AND p.Email IS NOT NULL AND p.Email<>'' 
//				LEFT JOIN ADMISSION_OTHERS_INFO as d ON d.ApplicationID=a.ApplicationID 
//				LEFT JOIN ADMISSION_APPLICATION_STATUS as b ON b.ApplicationID=a.ApplicationID
//				WHERE f.ApplicationID = '" . trim($applicationId) . "' AND TRIM(p.Email) <> ''
//				ORDER BY a.ApplicationID, p.PG_TYPE";
//		$records = current($this->returnArray($sql));
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_PG_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = $this->returnArray($sql);
		
		$emails = '';
		for($i=0; $i<count($records); $i++){
			$to_email = $records[$i]['Email'];
			if($to_email != '' && intranet_validateEmail($to_email)){
				$emails .= ($emails?'/':'').$to_email;
			}
		}
		
		return $emails;
	}
	
    /**
     * Admission Form - finish send email
     */
    public function sendMailToNewApplicant($applicationId, $subject, $message)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
        include_once ($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new \libwebmail();
        
        $from = $libwebmail->GetWebmasterMailAddress();
        $inputby = $_SESSION['UserID'];
        $result = array();
        
//        $sql = "SELECT 
//					f.RecordID as UserID,
//					a.ApplicationID as ApplicationNo,
//					a.ChineseName,
//					a.EnglishName,
//					p.Email as Email
//				FROM ADMISSION_OTHERS_INFO as f 
//				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
//				LEFT JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=a.ApplicationID AND p.Email IS NOT NULL AND p.Email<>'' 
//				LEFT JOIN ADMISSION_OTHERS_INFO as d ON d.ApplicationID=a.ApplicationID 
//				LEFT JOIN ADMISSION_APPLICATION_STATUS as b ON b.ApplicationID=a.ApplicationID
//				WHERE f.ApplicationID = '" . trim($applicationId) . "' AND TRIM(p.Email) <> ''
//				ORDER BY a.ApplicationID, p.PG_TYPE";
//				
//        $records = current($this->returnArray($sql));
        
        $sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_PG_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = $this->returnArray($sql);
        
        // debug_pr($applicationId);
        $to_email = $records['Email'];
        if ($subject == '') {
            $email_subject = "入學申請通知 Admission Notification";
        } else {
            $email_subject = $subject;
        }
        $email_subject = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, $email_subject);
        
        $email_message = $message;
//        $sent_ok = true;
//        if ($to_email != '' && intranet_validateEmail($to_email)) {
//            $sent_ok = $libwebmail->sendMail($email_subject, $email_message, $from, array(
//                $to_email
//            ), array(), array(), "", $IsImportant = "", $mail_return_path = get_webmaster(), $reply_address = "", $isMulti = null, $nl2br = 0);
//        } else {
//            $sent_ok = false;
//        }
//        
//        return $sent_ok;
		$sent_ok = array();
		for($i=0; $i<count($records); $i++){
			$to_email = $records[$i]['Email'];
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok[] = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}
		}
			
		return !in_array(false, $sent_ok);
    }
} // End Class
