<?php
// modifying by:
/**
 * Change Log:
 * 2018-09-04 Pun
 * - File Created
 */
include_once ("{$intranet_root}/includes/admission/libadmission_ui_cust_base.php");
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once ("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionUiCustBase.class.php");

class admission_ui_cust extends \AdmissionSystem\AdmissionUiCustBase
{

    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    private function init()
    {
    	global $IsUpdate;
        // ### FILTER_ADMISSION_FORM_INSTRUCTION_HTML START ####
        /*$this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml = str_replace('class="instructionSuggestBrowser"', 'class="instructionSuggestBrowser" style="display:none;"', $formHtml);
            return $formHtml;
        }));
        $this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, array(
            $this,
            'instructionPageSchoolSelection'
        ));*/
        // ### FILTER_ADMISSION_FORM_INSTRUCTION_HTML END ####
        
        // ### FILTER_ADMISSION_FORM_WIZARD_STEPS START ####
        if(!$IsUpdate){
	        $this->addFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS, (function ($stepArr) {
	            global $Lang;
	            array_splice($stepArr, 4, 0, array(
	                array(
	                    'id' => 'pagePayment',
	                    'title' => $Lang['Admission']['payment'],
	                ),
	            ));
	            
	            return $stepArr;
	        }));
        }
        // ### FILTER_ADMISSION_FORM_WIZARD_STEPS END ####
        
        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML START ####
        $this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml .= $this->getInstructionForm($BirthCertNo, $IsUpdate);
            $formHtml .= $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
            $formHtml .= $this->getParentForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getOtherForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getDocUploadForm($IsConfirm, $IsUpdate);
//            $formHtml .= $this->getPaymentForm($IsConfirm, $IsUpdate);
            return $formHtml;
        }));
        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML END ####
        
        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML START ####
//        $this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function ($formHtml, $IsUpdate) {
//            $formHtml .= $this->getStudentForm($IsConfirm = 1, '', $IsUpdate);
//            $formHtml .= $this->getParentForm($IsConfirm = 1, $IsUpdate);
//            $formHtml .= $this->getOtherForm($IsConfirm = 1, $IsUpdate);
//            $formHtml .= $this->getDocUploadForm($IsConfirm = 1, $IsUpdate);
//            return $formHtml;
//        }));
        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML END ####
        
        // ### FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML START ####
        $this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml .= $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
            $formHtml .= $this->getParentForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getOtherForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getDocUploadForm($IsConfirm, $IsUpdate);
            
            return $formHtml;
        }));
        // ### FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML END ####
        
        // ### FILTER_ADMISSION_FORM_FINISH_HTML START ####
//        $this->addFilter(self::FILTER_ADMISSION_FORM_FINISH_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
//            $formHtml .= $this->getPaymentForm($IsConfirm, $IsUpdate);
//            $formHtml .= $this->getFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '');
//            
//            return $formHtml;
//        }));
        // ### FILTER_ADMISSION_FORM_FINISH_HTML END ####
        
        // ### FILTER_ADMISSION_TOP_MENU_TAB START ####
        /*$this->addFilter(self::FILTER_ADMISSION_TOP_MENU_TAB, (function ($defaultTab) {
            $newTab = array(
            );
            array_splice($defaultTab, 2, 0, $newTab);
            return $defaultTab;
        }));*/
        // ### FILTER_ADMISSION_TOP_MENU_TAB START ####
    }

    /**
     * Admission Form - create/edit form student part
     */
    protected function getInstructionForm($BirthCertNo = "", $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg, $sus_status;
        
        $sus_status = ($IsConfirm)?$formData['sus_status']:$sus_status;
        
        $allClassLevel = $lac->getClassLevel();
        $settings = $lac->getApplicationSetting();
        $application_setting = $settings[$sus_status];
        
        $Instruction = $application_setting['FirstPageContent'];
        
        if (!$Instruction) {
            $Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'];
        }
        
        @ob_start();
        include (__DIR__ . "/template/admissionForm/instructionForm.tmpl.php");
        $x = ob_get_clean();
        
        return $x;
    }

    /**
     * Admission Form - create/edit form student part
     */
    protected function getStudentForm($IsConfirm = 0, $BirthCertNo = "", $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg, $sus_status;
        
        $pageId = $IsConfirm ? 'pageConfirmation' : 'pagePersonalInfo';
        
        $sus_status = ($IsConfirm)?$formData['sus_status']:$sus_status;
        
        $allClassLevel = $lac->getClassLevel();
        $settings = $lac->getApplicationSetting();
        $application_setting = $settings[$sus_status];
        
        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';
        
        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            
            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
            
            if (count($application_details) > 0) {
                $StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'], '', $application_details['ApplicationID']));
            }
            
            // ## Apply Year START ##
            $classLevelID = $StudentInfo['classLevelID'];
            $classLevel = $allClassLevel[$classLevelID];
            // ## Apply Year END ##
            
            // ## Apply School START ##
            $applySchoolId = $allCustInfo['ApplySchool'][0]['Value'];
            $applySchoolCode = $admission_cfg['SchoolType'][$applySchoolId];
            // ## Apply School END ##
        }
        
        @ob_start();
        include (__DIR__ . "/template/admissionForm/studentForm.tmpl.php");
        $x = ob_get_clean();
        
        return $x;
    }

    /**
     * Admission Form - create/edit form parent part
     */
    protected function getParentForm($IsConfirm = 0, $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg;
        
        $pageId = $IsConfirm? 'pageConfirmation' : 'pagePersonalInfo';
        
        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';
        
        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
            
            $tmpParentInfoArr = $lac->getApplicationParentInfo($application_details['ApplyYear'], '', $_SESSION['KIS_ApplicationID']);
            $parentInfoArr = array();
            foreach ($tmpParentInfoArr as $parent) {
                foreach ($parent as $para => $info) {
                    $ParentInfo[$parent['type']][$para] = $info;
                }
            }
            // ### Parent Info END ####
        }
        $parentTypeArr = array(
            1 => 'F',
            2 => 'M',
            3 => 'G'
        );
        
        @ob_start();
        include ("template/admissionForm/parentForm.tmpl.php");
        $x = ob_get_clean();
        
        return $x;
    }

    /**
     * Admission Form - create/edit form other part
     */
    protected function getOtherForm($IsConfirm = 0, $IsUpdate = 0)
    {
        global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, $lac, $kis_lang, $kis_lang_b5, $kis_lang_en;
        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';
        
        $pageId = $IsConfirm? 'pageConfirmation' : 'pagePersonalInfo';
        
        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
            $SiblingInfo = $lac->getApplicationSibling($schoolYearID = $application_details['ApplyYear'], $classLevelID = '', $applicationID = $application_details['ApplicationID'], $recordID = '');
        }
        
        @ob_start();
        include ("template/admissionForm/otherForm.tmpl.php");
        $x = ob_get_clean();
        
        return $x;
    }

    /**
     * Admission Form - create/edit form other part
     */
    protected function getDocUploadForm($IsConfirm = 0, $IsUpdate = 0, $AcademicYearID = 0, $YearID = 0)
    {
        global $Lang;
        global $tempFolderPath, $fileData, $formData, $admission_cfg, $lac, $sys_custom, $intranet_root, $intranet_session_language;
        
        $pageId = $IsConfirm? 'pageConfirmation' : 'pageDocsUpload';
        
        if ($IsConfirm) {
            $YearID = $formData['sus_status'];
        }
        
        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            
            if (count($application_details) > 0) {
                $applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'], array(
                    'applicationID' => $application_details['ApplicationID']
                ));
                $YearID = $application_details['ApplyLevel'];
            }
            // ## Photo START ##
            $viewFilePath = (is_file($intranet_root . "/file/admission/" . $applicationAttachmentInfo[$application_details['ApplicationID']]['personal_photo']['attachment_link'][0]) && ($IsUpdate && ! $IsConfirm || $IsUpdate && $IsConfirm && ! $fileData['StudentPersonalPhoto']) ? ' <a href="download_attachment.php?type=personal_photo'./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]*/'" target="_blank" >' . $Lang['Admission']['viewSubmittedFile'] . '</a>' : '');
            // ## Photo END ##
        }
        
        // ## Attachments START ##
        $settings = $lac->getAttachmentSettings();
        $attachment_settings = array();
        foreach ($settings as $index => $setting) {
            if ($setting['ClassLevelStr']) {
                $classLevelArr = explode(',', $setting['ClassLevelStr']);
                
                if (in_array($YearID, $classLevelArr)) {
                    $attachment_settings[$index] = $setting;
                }
            } else {
                $attachment_settings[$index] = $setting;
            }
        }
        // ## Attachments END ##
        
        if (! $lac->isInternalUse($_GET['token']) && ! $IsUpdate) {
            $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';
        } else {
            $star = '';
        }
        
        @ob_start();
        include ("template/admissionForm/docUploadForm.tmpl.php");
        $x = ob_get_clean();
        
        return $x;
    }

    /**
     * Admission Form - create/edit form payment part
     */
    protected function getPaymentForm($IsConfirm = 0, $IsUpdate = 0, $AcademicYearID = 0, $YearID = 0)
    {
        global $Lang;
        global $tempFolderPath, $fileData, $formData, $admission_cfg, $lac, $sys_custom, $intranet_root, $intranet_session_language;
        
        
        @ob_start();
        include ("template/admissionForm/paymentForm.tmpl.php");
        $x = ob_get_clean();
        
        return $x;
    }

	/**
     * Admission form - create applicant finish page
     */
    public function getFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;
        
        $hasPaid = 0;
		$cancelled = 0;
		$result = $lac->getPaymentResult('', '', '', '', $ApplicationID);
		if($result){
			foreach($result as $aResult){
				if($aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']){
					$hasPaid = 1;
					//$isSent = $aResult['EmailSent'];
					$paymentId = $aResult['PaymentID'];
				}
				if($aResult['Status'] == $admission_cfg['Status']['cancelled']){
					$cancelled = 1;
				}
			}
		}
		$sql = "SELECT AutoEmailSent FROM ADMISSION_APPLICATION_STATUS Where ApplicationID = '".$ApplicationID."' ";
		$isSent = current($lac->returnArray($sql));
		
		if($hasPaid || $lac->isInternalUse($_REQUEST['token'])){
			if($isSent['AutoEmailSent'] <= 0/* && !($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID)=='')*/){
				$applicationSetting = $lac->getApplicationSetting($schoolYearID);
				$EmailContent = $applicationSetting[$sus_status]['EmailContent'];
				$mail_content = $lauc->getFinishPageEmailContent($ApplicationID, $EmailContent, $schoolYearID);
				if(!$lac->isInternalUse($_REQUEST['token'])){
					$lac->sendMailToNewApplicant($ApplicationID,'',$mail_content);
				}
				$sql = "UPDATE ADMISSION_APPLICATION_STATUS Set  AutoEmailSent = '1' Where  ApplicationID = '".$ApplicationID."' ";
				$lac->db_db_query($sql);
			}
			// finish page	
	        if ($ApplicationID) {
	            $printLink = $lac->getPrintLink($schoolYearID, $ApplicationID, '', $this->getAdmissionLang($ApplicationID));
	            $applicantEmail = $lac->getApplicantEmail($ApplicationID);
	            
	            $msg = str_replace('<!--ApplicationID-->', $ApplicationID, $Lang['Admission']['finishPage']['applicationSubmitted']);
	            $msg2 = str_replace('<!--Email-->', $applicantEmail, $Lang['Admission']['finishPage']['confirmEmail']);
	            
	            $x .= <<<HTML
	            <article id="blkFinish">
					<section id="blkApplicationNo">
						<span class="graphicWithThemeColor">
							<span>
								<img src="/images/kis/eadmission/graphic_finish.png">
							</span>
						</span><span>
							{$msg}
							<br><br>
							<span class="button button-secondary" id="btnPrint" onclick="window.open('{$printLink}','_blank');">{$Lang['Admission']['printsubmitform']}</span>
						</span>
					</section>
					<section class="form sheet" id="blkFinishMsg">
						<div>
							{$msg2}
						</div>
						<div class="remark">
							{$LastContent}
						</div>
					</section>
				</article>
HTML;
//	            $x .= <<<HTML
//	                <div class="admission_complete_msg">
//	                    <h1>
//	                        {$msg}
//	                        <br /><br />
//	                        <input type="button" value="{$Lang['Admission']['printsubmitform']}" onclick="window.open('{$printLink}','_blank');" />
//	                    </h1>
//	                    <br />
//	                    <h1>
//	                        <span>
//	                            {$msg2}
//	                        </span>
//	                    </h1>
//HTML;
	        } else {
	            $x .= <<<HTML
	                <div class="admission_complete_msg">
	                    <h1 style="color:red;">
	                        {$Lang['Admission']['finishPage']['failApply']}
	                    </h1>
HTML;
	        }
//	        $x .= '<br/>' . $LastContent . '</div>';
//	        if ((! $lac->isInternalUse($_GET['token']) || $lac->getTokenByApplicationNumber($ApplicationID) != '') && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
//	            $x .= '<div class="edit_bottom">
//	                    <input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'' . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST'] . '\'" value="' . $Lang['Admission']['finish'] . '" />
//	                </div>
//	                <p class="spacer"></p></div>';
//	        } else {
//	            $x .= '<div class="edit_bottom">';
//	            $x .= '<input id="finish_page_finish_button" style="" type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="' . $Lang['Admission']['finish'] . '" />';
//	            $x .= '</div>';
//	            $x .= '<p class="spacer"></p></div>';
//	        }
		}
        else{
			// payment page
			if($cancelled){
				$x .=' <div class="admission_complete_msg">';
	            $x .='<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
				$x .= '</div></div>';
			}
			else if($ApplicationID){
//				$x .='<div class="admission_complete_msg"><h1>現在請進行報名費付款 ，以完成報名程序。<br/>';            
//				$x .='To finish the online application, please pay now.</h1>';
//				$x .='<h1><span>報名費：港幣 $50
//					  <br/>Application fee:  HK$50</span></h1>';
//				$x .= '</div>';
//				
//				$x .= '<div class="edit_bottom">
//						<input type="button" class="formbutton" value="繳付 HK$50 報名費 Pay application fee HK$50" onclick="$(\'#payment_page\').hide();$(\'#paypal_page\').show();"></input>
//					</div>
//					<p class="spacer"></p></div></div>';
				$x .= <<<HTML
				<div id="payment_page">
					<section class="graphicWithThemeColor">
						<span>
							<img src="/images/kis/eadmission/graphic_submission.png">
						</span>
					</section>
					<section id="lblSubmitInst">
						To finish the online application, please pay now.
					</section>
					<section id="blkPay">
						<div>Application fee</div>
						<div id="lblApplicationFee">HK$2,000</div>
						<span class="button button-primary" id="btnPay" onclick="$('#payment_page').hide();$('#paypal_page').show();">Pay</span>
					</section>
					</div>
HTML;
			
				$x .= '<div id="paypal_page" style="display:none">';
				$hashedApplicationID = MD5($result[0]['ApplicationID']);
				$x .= <<<HTML
					<section class="center">
						<img src="/images/kis/eadmission/paypal.png">
					</section>
					<section id="blkPayment">
						<div id="lblPayment">
							After paying the fee using Paypal, you MUST click
							<div>“Back to {$admission_cfg['paypal_name']}”</div>
							to complete the whole procedure.
						</div>
						<div class="remark">
							Remark: Please allow pop-up to Paypal if blocked by your Chrome browser. If you cannot find the corresponding payment form in Paypal, please click the Payment icon in this step again.
						</div>
					</section>
					<section id="blkPaypal">
						<!-- PayPal Logo -->
						<table border="0" cellpadding="10" cellspacing="0" align="center">
							<tbody><tr>
								<td align="center"><a href="javascript:void(0)" title="PayPal 如何運作" onclick="javascript:window.open('http://{$_SERVER['HTTP_HOST']}/kis/admission_paypal/redirect_to_paypal.php?token={$hashedApplicationID}','payment_page','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/en_AU/i/buttons/btn_paywith_primary_m.png" alt="付款用 PayPal"></a></td>
							</tr>
						</tbody></table>
						<!-- PayPal Logo -->
					</section>
HTML;
//				$x .= $this->getWizardStepsUI(8);
//				$x .='<div class="admission_complete_msg"><h1><span><font color="red">*</font> 現在前往 Paypal 付款，完成後，必須於付款頁面按<br/> "<font color="red">返回'.$admission_cfg['paypal_name'].'</font>" <br/>方能完成整個付款及報名程序。<br/><br/><font color="red">*</font> After paying the fee using Paypal, you MUST click<br/> "<font color="red">Back to '.$admission_cfg['paypal_name'].'</font>" <br/> to complete the whole procedure.<br/><br/>';
//				$x .='<a href="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/redirect_to_paypal.php?token='.MD5($result[0]['ApplicationID']).'" target="payment_page" onclick="$(\'#nextStepLink\').show()">
//						<image src="https://www.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" alt="PayPal － 更安全、更簡單的網上付款方式！">
//						
//						<span id="nextStepLink" style="display:none;color: inherit;">
//							<br />
//							如果已使用 PayPal 成功繳費，但仍停留在此，可按此跳到下一步驟。<br />
//							If you already settle the payment using PayPal but no update to this page, you can click here to proceed to next step.
//						</span>
//						
//						</a></span></h1><br />';         
//						
//				$x .= '</div>';
				$x .= '</div>';
				
			}
			else{
				$x .=' <div class="admission_complete_msg">';
	            $x .='<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
				$x .= '</div></div>';
			}
		}
		
        return $x;
    }

	/**
     * Admission Form - create application send email
     */
    function getFinishPageEmailContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $paymentEmail = '')
    {
        global $PATH_WRT_ROOT, $Lang, $lac, $admission_cfg, $sys_custom;
        
        if ($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID) == '') {
            if ($ApplicationID) {
                $schoolYearID = $schoolYearID ? $schoolYearID : $lac->schoolYearID;
                $stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID, '', $ApplicationID);
                
                $x .= 'Your application form has been received, name of child is ' . $stuedentInfo[0]['student_name_en'] . ' and the application number is <u>' . $ApplicationID . '</u>.';
                
                $x .= '<br/><br/><br/>';
                $x .= 'You will be informed of arrangements for interview(s) via email. Please visit the school website for related information.';
                
                $x .= '<br/><br/><br/';
                $x .= 'Thanks for your application!';
                
                if (! $LastContent) {
                    $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage'];
                }
                $x .= '<br/><br/>' . $LastContent;
            }
        } else {
            if($ApplicationID){
				$sql = "Select Status From ADMISSION_APPLICATION_STATUS Where ApplicationID = '".$ApplicationID."'";
				$result = current($lac->returnArray($sql));
				$paymentSettle = 0;
				if($result['Status'] >= $admission_cfg['Status']['paymentsettled'] && $result['Status'] != $admission_cfg['Status']['cancelled']){
					$paymentSettle = 1;
				}
				if(/*$paymentEmail || */$paymentSettle == 0){
					$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
					$stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
					
					$x .='Your application form has been received, name of child is '.$stuedentInfo[0]['student_name_en'].'. To pay or check payment status, click this hyperlink:';
					$x .="<br/><a target='_blank' href='http://".$_SERVER['HTTP_HOST']."/kis/admission_form2/finish.php?id=".urlencode(getEncryptedText("ApplicationID=".$ApplicationID."&sus_status=".$stuedentInfo[0]['classLevelID']."&SchoolYearID=".$schoolYearID,$admission_cfg['FilePathKey']))."'>http://".$_SERVER['HTTP_HOST']."/kis/admission_form2/finish.php?id=".urlencode(getEncryptedText("ApplicationID=".$ApplicationID."&sus_status=".$stuedentInfo[0]['classLevelID']."&SchoolYearID=".$schoolYearID,$admission_cfg['FilePathKey']))."</a>";
				}
				else{
	
				$x .='Your application form has been successfully submitted and the application number is <u>'.$ApplicationID.'</u>.<br/>';
				
				$x .='<br/><br/>';
				$x .='Click this hyperlink to view your application form.';
				$x .="<br/><a target='_blank' href='http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."'>http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."</a>";
				
//				$x .='<br/><br/><br/>';
//				$x .='Click this hyperlink to amend your application form before the application deadline.';
//				$x .="<br/><a target='_blank' href='http://hkugaps.eclasscloud.hk/kis/admission_form2/index_edit.php'>http://hkugaps.eclasscloud.hk/kis/admission_form2/index_edit.php</a>";
				
				$x .='<br/><br/><br/>';            
				$x .='Thanks for lodging your application online!';
				
				$x .= '<br/><br/>'.$LastContent;
				}
			}
			else{
				$x .='Your application is rejected. Please try to apply again or contact our school.';
	        }
        }
        
        $x = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_APPLICANT, $x, $ApplicationID);
        
        return $x;
    }
	
    function getTimeOutPageContent($ApplicationID = '', $LastContent = '')
    {
        global $Lang, $lac, $admission_cfg, $sys_custom;
        $x = '<div class="admission_board">';
        if ($ApplicationID) {
            
            $x .= ' <div class="admission_complete_msg"><h1>' . $Lang['Admission']['msg']['admissioncomplete'] . '<span>' . $Lang['Admission']['msg']['yourapplicationno'] . ' ' . $ApplicationID . '&nbsp;&nbsp;<input type="button" value="' . $Lang['Admission']['printsubmitform'] . '" onclick="window.open(\'' . $lac->getPrintLink("", $ApplicationID) . '\',\'_blank\');"></input></span></h1>';
        } else {
            $x .= ' <div class="admission_complete_msg"><h1>' . $Lang['Admission']['msg']['admissionnotcomplete'] . '<span>' . $Lang['Admission']['msg']['tryagain'] . '</span></h1>';
            $x .= '<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
        }
        $x .= '</div>';
        
        if (! $lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'' . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST'] . '\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
				</div>
				<p class="spacer"></p></div>';
        } else {
            $x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
				</div>
				<p class="spacer"></p></div>';
        }
        return $x;
    }

    function getQuotaFullPageContent($type = 'Admission', $LastContent = '')
    {
        global $Lang, $lac, $admission_cfg, $sys_custom;
        $x = '<div class="admission_board">';
        if (! $LastContent) {
            if ($type == 'Admission') {
                $LastContent = '<div class="admission_complete_msg"><h1>' . $Lang['Admission']['munsang']['msg']['admissionQuotaFull'] . '</h1>';
                $LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
            } else 
                if ($type == 'Interview') {
                    $LastContent = '<div class="admission_complete_msg"><h1>' . $Lang['Admission']['munsang']['msg']['interviewQuotaFull'] . '</h1>';
                    $LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
                } else {
                    $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; // Henry 20131107
                }
        }
        $x .= $LastContent . '</div>';
        $x .= '</div>';
        
        if (! $lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'' . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST'] . '\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
				</div>
				<p class="spacer"></p></div>';
        } else {
            $x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
				</div>
				<p class="spacer"></p></div>';
        }
        return $x;
    }

    function getPayPalButton($ApplicationID)
    {
        global $admission_cfg, $lac;
        $ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
        return '<form action="' . $admission_cfg['paypal_url'] . '" <!--onsubmit="checkPayment(\'' . $ApplicationID . '\');return false;"--> method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="' . $admission_cfg['hosted_button_id'] . '">
				<input type="hidden" name="return" value="http://' . $_SERVER['HTTP_HOST'] . '/kis/admission_paypal/payment_finish2.php" /> 
				    <input type="hidden" name="cancel_return" value="http://' . $_SERVER['HTTP_HOST'] . '/kis/admission_paypal/payment_finish2.php?cm=' . $ApplicationID . '" />
				    <input type="hidden" name="custom" value="' . $ApplicationID . '" />
					<input type="hidden" name="notify_url" value="http://' . $_SERVER['HTTP_HOST'] . '/kis/admission_paypal/payment_ipn.php" /> 
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();
				        
						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
    }

    function getAfterUpdateFinishPageContent($ApplicationID = '', $LastContent = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;
        $x = '<div class="admission_board">';
        
        $x .= ' <div class="admission_complete_msg"><h1>申請編號為 ' . $ApplicationID . '。&nbsp;&nbsp;<br/>';
        $x .= 'Your application number is ' . $ApplicationID . '.&nbsp;&nbsp;<br><br><input type="button" value="' . $Lang['Admission']['printsubmitform'] . ' Print submitted form" onclick="window.open(\'' . $lac->getPrintLink("", $ApplicationID) . '\',\'_blank\');"></input></h1></div>';
        
        $x .= '<div class="admission_board">';
        $x .= $this->getDocsUploadForm(1, 1);
        
        $applicationStatus = current($lac->getApplicationStatus($lac->schoolYearID, '', $ApplicationID));
        
        if ($applicationStatus['interviewdate'] && $applicationStatus['interviewdate'] != '0000-00-00 00:00:00') {
            $interviewDateTime = explode(" ", $applicationStatus['interviewdate']);
            $x .= '<h1 style="font-size: 15px">面試資料 Interview Information</h1>';
            $x .= '<table class="form_table" style="font-size: 15px">';
            $x .= '<tr>';
            $x .= '<td class="field_title">面試日期 Interview Date</td>';
            $x .= '<td>' . $interviewDateTime[0] . '</td>';
            $x .= '</tr>';
            $x .= '<td class="field_title">面試時間 Interview Time</td>';
            $x .= '<td>' . substr($interviewDateTime[1], 0, - 3) . '</td>';
            $x .= '</tr>';
            $x .= '<td class="field_title">面試地點 Interview Location</td>';
            $x .= '<td>' . $applicationStatus['interviewlocation'] . '</td>';
            $x .= '</tr>';
            $x .= '</table>';
        }
        $x .= '</div>';
        
        if (! $LastContent) {
            $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; // Henry 20131107
        }
        $x .= '<br/>' . $LastContent;
        $x .= '</div>';
        
        $x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
				</div>
				<p class="spacer"></p></div>';
        
        return $x;
    }

    public function getApplicationForm($BirthCertNo = "",$YearID="",$currentStep = 2)
    {
        global $fileData, $formData, $tempFolderPath, $LangB5, $LangEn, $libkis_admission, $sys_custom, $admission_cfg, $lac;
    
        $x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, $html = '', $IsConfirm = false, $BirthCertNo, $IsUpdate = false, $YearID);
//    	$x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, $html = '', $IsUpdate = false, $YearID);
    
        return $x;
    }
    
    public function getApplicationUpdateForm($BirthCertNo = "",$YearID="",$currentStep = 2)
    {
        global $fileData, $formData, $tempFolderPath, $LangB5, $LangEn, $libkis_admission, $sys_custom, $admission_cfg, $lac;
    
        $x .= $this->applyFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, $html = '', $IsConfirm = false, $BirthCertNo, $IsUpdate = true, $YearID);
//    	$x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, $html = '', $IsUpdate = false, $YearID);
    
        return $x;
    }
    
    /**
     * Portal - print admission form
     */
    public function getPDFContent($schoolYearID, $applicationIDAry, $type = '')
    {
        global $PATH_WRT_ROOT, $lac, $admission_cfg, $setting_path_ip_rel, $plugin, $kis_lang, $sys_custom;
        
        global $baseFilePath;
        $baseFilePath = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}";
        $baseFilePath = $this->applyFilter(self::FILTER_ADMISSION_PDF_BASE_PATH, $baseFilePath);
        
        // ####### Init PDF START ########
        $templateHeaderPath = "{$baseFilePath}/pdf/header.php";
        $templatePath = "{$baseFilePath}/pdf/application_form.php";
        require_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");
        
        $mpdf = new \mPDF($mode = '', $format = 'A4', $default_font_size = 0, $default_font = 'msjh', $marginLeft = 0, $marginRight = 0, $marginTop = 0, $marginBottom = 0/*,
            $marginHeader=9,
            $marginFooter=9,
            $orientation='P'*/
        );
        $mpdf->mirrorMargins = 1;
        // ####### Init PDF END ########
        
        // ####### Load header to PDF START ########
        ob_start();
        include ($templateHeaderPath);
        $pageHeader = ob_get_clean();
        
        $mpdf->WriteHTML($pageHeader);
        // ####### Load header to PDF END ########
        
        // ####### Load data to PDF START ########
        global $applicationIndex, $applicationID;
        foreach ((array) $applicationIDAry as $applicationIndex => $applicationID) {
            
            // ### Load Template START ####
            ob_start();
            include ($templatePath);
            $page1 = ob_get_clean();
            // ### Load Template END ####
            
            $mpdf->WriteHTML($page1);
        }
        // ####### Load data to PDF END ########
        
        $mpdf->Output();
    }
} // End Class