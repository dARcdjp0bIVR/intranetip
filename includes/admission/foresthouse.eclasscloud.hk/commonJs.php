<script>
$(function(){
	'use strict';

	window.validateFunc = {};
	
	/**** Step START ****/
	var stepArr = <?=$json->encode((array)$stepArr) ?>;
	var stepHtml = $('#stepHtml').html();

	_.each(stepArr, function(step, index){
		validateFunc[step.id] = [];
	});
	
	var updateStepHtml = (function(stepId){
		$('#stepContainer').empty();
		_.each(stepArr, function(step, index){
			var tmpl = _.template(stepHtml);
			$('#stepContainer').append(tmpl({
				'cssClass': (stepId == step.id)? 'step-current' : '',
				'stepOrderCssClass': ((index + 1) == stepArr.length)? ' fa-flag icon' : '',
				'stepOrder': ((index + 1) < stepArr.length)? (index + 1) : '',
				'title': step.title,
			}));
		});
		
		$('.itemInput').find('input').each(function(){
			if($(this).val()){
				$(this).addClass('notEmpty');
			}
		});
	});
	/**** Step END ****/

	/**** Form START ****/
	var updateFormHtml = (function(stepId){
		
		$('#btnContinue').hide();
		$('#btnSubmit').hide();
		$('#btnBack').hide();
		$('.uploadedFile .fa-trash').show();
		
		switch(stepId) {
			case 'pageInstruction':
		        $('.display_pagePersonalInfo .itemInput').show();
		        $('.display_pagePersonalInfo .itemData').hide();
		        $('#btnContinue').show();
		        $('#btnSubmit').hide();
		        $('#btnBack').hide();
		        break;
		    case 'pagePersonalInfo':
		        $('.display_pagePersonalInfo .itemInput').show();
		        $('.display_pagePersonalInfo .itemData').hide();
		        $('#btnContinue').show();
		        $('#btnSubmit').hide();
		        <?if(!$IsUpdate){?>
		        $('#btnBack').show();
		        <?}?>
		        break;
		    case 'pageDocsUpload':
		        $('.display_pageDocsUpload .itemInput').show();
		        $('.display_pageDocsUpload .itemData').hide();
		        $('#btnContinue').show();
		        $('#btnSubmit').hide();
		        $('#btnBack').show();
		        break;
		    case 'pageConfirmation':
		    	$('.display_pageConfirmation .itemData').show();
		        $('.display_pageConfirmation .itemInput').hide();
		        $('#btnContinue').hide();
		        $('#btnSubmit').show();
		        $('#btnBack').show();
		        $('.uploadedFile .fa-trash').hide();
		        break;
		}
		
		$('.displaySection').hide();
		$('.display_' + stepId).show();
	});
	/**** Form END ****/

	/**** State machine START ****/
	var transitions = [{ name: 'start', from: 'init', to: '<?=$IsUpdate?'pagePersonalInfo':'pageInstruction'?>' }];
	for(var i=0;i<stepArr.length-1;i++){
		transitions.push({
			name: 'nextStep',
			from: stepArr[i].id,
			to: stepArr[i + 1].id
		});
	}
	for(var i=stepArr.length-1;i>0;i--){
		transitions.push({
			name: 'lastStep',
			from: stepArr[i].id,
			to: stepArr[i - 1].id
		});
	}
	transitions.push({ name: 'goto', from: '*', to: function(s) { return s } });

	window.admissionStateMachine = new StateMachine({
        init: 'init',
        transitions: transitions,
        methods: {
            onAfterTransition: function(){
            	updateStepHtml(this.state);
            	updateFormHtml(this.state);
            },
            onBeforeNextStep: function(lifecycle){
                window.scrollTo(0,0);
                if(!check_input_info(lifecycle.from)){
                    return false;
                }
            },
        }
    });
	admissionStateMachine.start();
	if($("#payment_page").length > 0) {
		admissionStateMachine.goto('pagePayment');
	}
	else if($("#blkFinish").length > 0) {
		$("#btnFinish").show();
		admissionStateMachine.goto('pageFinish');
	}
	/**** State machine END ****/

	/**** Button next/back START ****/
//	$('#btnSubmit').click(function(){
//		$('#form1').submit();
//	});
	$('#btnContinue').click(function(){
		admissionStateMachine.nextStep();
	});
	$('#btnBack').click(function(){
		admissionStateMachine.lastStep();
	});
	/**** Button next/back END ****/
	
	$('#btnFinish').click(function(){
		window.location.replace('index.php');
	});
   
	/**** Helper function START ****/
	window.isUpdatePeriod = false;
	
	window.check_input_info = (function(pageId){
		window.focusElement = null;
		var isValid = true;
		_.each(validateFunc, function(funcs, pId){
			if(_.isUndefined(pageId) || (pageId == pId)){
				_.each(funcs, function(func){
    				isValid = func(pageId) && isValid;
				});
			}
		});
		return isValid;
	});
	
	window.checkInputRequired = (function($parent){
		var isValid = true;
		
		$parent.find('.remark-warn').addClass('hide');
		$parent.find('input[required], select, textarea[required]').each(function(){
			var $this = $(this);
			var isEmpty = false;
			
			if($this.val() == ''){
				isEmpty = true;
			}

			if($this.attr('type') == 'radio'){
				var name = $this.attr('name');
				if($parent.find('[name="' + name + '"]:checked').length == 0){
					isEmpty = true;
				}
			}
			
			if(isEmpty){
				$this.parent().find('.remark-warn').removeClass('hide').show();
				if(!focusElement){
					focusElement = $this;
				}
			}

			isValid = isValid && !isEmpty;
		});
		
		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});
	/**** Helper function END ****/
});
</script>