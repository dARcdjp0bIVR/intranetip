<div style="max-width: 800px;font: initial !important;">
    <p>請勿回覆此電郵 Please do not reply to this email</p>

    <p style=""><?= $yearStart ?>/<?= $yearEnd ?> 年度</p>
    <p>新生入學申請 - 確認通知 (申請編號: <?= $ApplicationID ?>)</p>

    <p style="margin-top: 2rem;">
        <?= $stuedentInfo[0]['student_name_b5'] ?> 家長：
    </p>

    <p style="margin-top: 1rem;">
        貴子弟之入學申請表格，經已收妥，現請閣下記錄申請編號 <?= $ApplicationID ?>，以便日後查核有關申請。
    </p>

    <p style="margin-top: 2rem;">
        <?= ($LastContent['b5'])?$LastContent['b5']:'' ?>
    </p>

    <p style="margin-top: 2rem;">
        <?=$admission_cfg['SchoolName']['b5']?>
        <br/>
        <?= $admission_cfg['SchoolAddress']['b5'] ?>
    </p>
</div>

<div style="margin-top: 2rem;max-width: 800px;font: initial !important;">
    <p style=""><?= $yearStart ?>/<?= $yearEnd ?> Academic Year</p>
    <p>New applications – confirmation notice (Application #: <?= $ApplicationID ?>)</p>

    <p style="margin-top: 2rem;">
        Dear Parent of <?= $stuedentInfo[0]['student_name_en'] ?>：
    </p>

    <p style="margin-top: 1rem;">
        We've received your child's application.
        Please note your application number <?= $ApplicationID ?> for future enquiries.
    </p>

    <p style="margin-top: 2rem;">
        <?= ($LastContent['en'])?$LastContent['en']:'' ?>
    </p>

    <p style="margin-top: 2rem;">
        <?=$admission_cfg['SchoolName']['en']?>
        <br/>
        <?= $admission_cfg['SchoolAddress']['en'] ?>
    </p>
</div>