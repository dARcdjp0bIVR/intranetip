<?php
global $libkis_admission, $dayTypeArr;
$OthersInfo = $libkis_admission->getApplicationOthersInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
$OthersInfo = $OthersInfo[0];
$SiblingInfo = $libkis_admission->getApplicationSibling($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);

$application_setting = $libkis_admission->getApplicationSetting();
$dayTypeArr = explode(',', $application_setting[$kis_data['applicationInfo']['classLevelID']]['DayType']);

$remarks = $libkis_admission->getApplicationCustInfo($kis_data['applicationID'], 'Remarks', true);
$remarks = $remarks['Value'];

$siblingTypeArr = array(0,1);
$totalField = (count($fields)-1) / 2;
?>
<?=$this->generateValidateJs($fields); ?>

<style>
.hide{
    display:none;
}
.itemInput .remark {
    margin-top: 5px;
}
.itemInput .remark.remark-warn {
    color: #dd2c00;
}
.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%">
		<?php foreach($siblingTypeArr as $siblingType): ?>
			<col style="">
		<?php endforeach; ?>
	</colgroup>
	<tr>
		<td><?=$kis_lang['Admission']['CREATIVE']['siblingSameSchool'] ?></td>
		<?php foreach($siblingTypeArr as $siblingType): ?>
			<td class="form_guardian_head"><center>(<?=$siblingType+1 ?>)</center></td>
		<?php endforeach; ?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['name']?>
		</td>
		<?php
		foreach($siblingTypeArr as $index => $siblingType):
    		$field = $fields[($totalField * $index) + 1];
    		$value = $SiblingInfo[$siblingType]['ChineseName'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['gender']?>
		</td>
		<?php
		foreach($siblingTypeArr as $index => $siblingType):
    		$field = $fields[($totalField * $index) + 2];
    		$value = $SiblingInfo[$siblingType]['Gender'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
        			<?= kis_ui::displayTableField( $kis_lang['Admission']['genderType'][$value] )?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['CREATIVE']['duration']?>
		</td>
		<?php
		foreach($siblingTypeArr as $index => $siblingType):
    		$field = $fields[($totalField * $index) + 3];
    		$value = $SiblingInfo[$siblingType]['Year'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['class']?>
		</td>
		<?php
		foreach($siblingTypeArr as $index => $siblingType):
    		$field = $fields[($totalField * $index) + 4];
    		$value = $SiblingInfo[$siblingType]['Class'];
		?>
			<td class="form_guardian_field">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
				<?php else: ?>
    				<?=kis_ui::displayTableField($value) ?>
				<?php endif; ?>
        	</td>
    	<?php
    	endforeach;
    	?>
	</tr>
</table>


<table class="form_table">
	<tbody>
		<tr>
			<td class="field_title" style="width: 30%;">
				<?= $kis_lang['Admission']['remarks'] ?>
			</td>
			<td colspan="2">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($fields[count($fields) - 2], $remarks) ?>
				<?php else: ?>
    				<?= kis_ui::displayTableField( nl2br($remarks) )?>
				<?php endif; ?>
        	</td>
		</tr>
		<tr>
			<td class="field_title" style="width: 30%;">
				<?= $kis_lang['Admission']['CREATIVE']['applyFor'] ?>
			</td>
			<td colspan="2">
    			<?php if($isEdit): ?>
    				<?=$this->generateAdminDetailsPageFieldHtml($fields[count($fields) - 1], array($OthersInfo['ApplyDayType1'], $OthersInfo['ApplyDayType2'], $OthersInfo['ApplyDayType3'])) ?>
				<?php else: ?>
    				<?php if(count($dayTypeArr) > 0): ?>
    					<div>
    						<?=$kis_lang['Admission']['PFK']['firstChoice'] ?>:
    						<?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$OthersInfo['ApplyDayType1']]) ?>
    					</div>
					<?php endif; ?>
    				<?php if(count($dayTypeArr) > 1): ?>
    					<div>
    						<?=$kis_lang['Admission']['PFK']['secondChoice'] ?>:
    						<?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$OthersInfo['ApplyDayType2']]) ?>
    					</div>
					<?php endif; ?>
    				<?php if(count($dayTypeArr) > 2): ?>
    					<div>
    						<?=$kis_lang['Admission']['PFK']['thirdChoice'] ?>:
    						<?=kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$OthersInfo['ApplyDayType3']]) ?>
    					</div>
					<?php endif; ?>
				<?php endif; ?>
        	</td>
		</tr>
	</tbody>
</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();

	window.scrollTo(0,0);
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	var data = $(this).serialize()
	check_input_info().then(function(){
        $.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function(success){
            $.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
        });
    });


	return false;
});
</script>
