<script>
    $(function () {
        'use strict';
        <?php if(false && $plugin['eAdmission_devMode'] && !$IsUpdate): ?>
        setTimeout(() => $('#agreeTerms, #btnContinue').click(), 1000);
        <?php endif; ?>

        window.validateFunc = {};

        $(window).on('beforeunload', function(){
            return 'Are you sure you want to leave?';
        });
        $(window).bind("pageshow", function(event) {
		    if (event.originalEvent.persisted) {
		        admissionStateMachine.goto('pageInstruction');
		    }
		});
        $("#form1").submit(function(){
            $(window).off('beforeunload');
        });
        
        /**** Step START ****/
        var stepArr = <?=$json->encode((array)$stepArr) ?>;
        var stepHtml = $('#stepHtml').html();

        _.each(stepArr, function (step, index) {
            validateFunc[step.id] = [];
        });

        var updateStepHtml = (function (stepId) {
            $('#stepContainer').empty();
            _.each(stepArr, function (step, index) {
                var tmpl = _.template(stepHtml);
                $('#stepContainer').append(tmpl({
                    'cssClass': (stepId == step.id) ? 'step-current' : '',
                    'stepOrderCssClass': ((index + 1) == stepArr.length) ? ' fa-flag icon' : '',
                    'stepOrder': ((index + 1) < stepArr.length) ? (index + 1) : '',
                    'title': step.title,
                }));
            });

            $('.itemInput').find('input').each(function () {
                if ($(this).val()) {
                    $(this).addClass('notEmpty');
                }
            });
        });
        /**** Step END ****/

        /**** Form START ****/
        var updateFormHtml = (function (stepId) {

            $('#btnContinue').hide();
            $('#btnSubmit').hide();
            $('#btnBack').hide();
            $('.uploadedFile .fa-trash').show();
            $('#divOtherFormClassLevel').hide();
            if ($('#OtherFile0').files && $('#OtherFile0').files.length > 1) {
                $('.cropImage').show();
            }

            switch (stepId) {
                case 'pageInstruction':
                    $('.display_pagePersonalInfo .itemInput').show();
                    $('.display_pagePersonalInfo .itemData').hide();
                    $('#btnContinue').show();
                    $('#btnSubmit').hide();
                    $('#btnBack').hide();
                    break;
                case 'pagePersonalInfo':
                    $('.display_pagePersonalInfo .itemInput').show();
                    $('.display_pagePersonalInfo .itemData').hide();
                    $('#btnContinue').show();
                    $('#btnSubmit').hide();
                <?if(!$IsUpdate){?>
                    $('#btnBack').show();
                <?}?>
                    break;
                case 'pageDocsUpload':
                    $('.display_pageDocsUpload .itemInput, .display_pageConfirmation .itemLabel').show();
                    $('.display_pageDocsUpload .itemData').hide();
                    $('#btnContinue').show();
                    $('#btnSubmit').hide();
                    $('#btnBack').show();
                    break;
                case 'pageConfirmation':
                    $('.display_pageConfirmation .itemData').show();
                    $('.display_pageConfirmation .itemInput, .display_pageConfirmation .itemLabel').hide();
                    $('#btnContinue').hide();
                    $('#btnSubmit').show();
                    $('#btnBack').show();
                    $('.uploadedFile .fa-trash').hide();
                    $('.cropImage').hide();
                    $('#divOtherFormClassLevel').show();
                    break;
            }

            $('.displaySection').hide();
            $('.display_' + stepId).show();
        });
        /**** Form END ****/

        /**** State machine START ****/
        var transitions = [{
            name: 'start',
            from: 'init',
            to: '<?=$IsUpdate ? 'pagePersonalInfo' : 'pageInstruction'?>'
        }];
        for (var i = 0; i < stepArr.length - 1; i++) {
            transitions.push({
                name: 'nextStep',
                from: stepArr[i].id,
                to: stepArr[i + 1].id
            });
        }
        for (var i = stepArr.length - 1; i > 0; i--) {
            transitions.push({
                name: 'lastStep',
                from: stepArr[i].id,
                to: stepArr[i - 1].id
            });
        }
        transitions.push({
            name: 'goto', from: '*', to: function (s) {
                return s
            }
        });

        window.admissionStateMachine = new StateMachine({
            init: 'init',
            transitions: transitions,
            methods: {
                onAfterTransition: function () {
                    updateStepHtml(this.state);
                    updateFormHtml(this.state);
                },
                onBeforeNextStep: function (lifecycle) {
                    window.scrollTo(0, 0);
                    return check_input_info(lifecycle.from);
                },
            }
        });
        admissionStateMachine.start();
        if ($("#payment_page").length > 0) {
            admissionStateMachine.goto('pagePayment');
        } else if ($("#blkFinish").length > 0) {
            $("#btnFinish").show();
            admissionStateMachine.goto('pageFinish');
        }
        /**** State machine END ****/

        /**** Button next/back START ****/
//	$('#btnSubmit').click(function(){
//		$('#form1').submit();
//	});
        $('#btnContinue').click(function () {
            admissionStateMachine.nextStep();
        });
        $('#btnBack').click(function () {
            admissionStateMachine.lastStep();
        });
        /**** Button next/back END ****/

        $('#btnFinish').click(function () {
            <?php if(!$sys_custom['KIS_Admission']['IntegratedCentralServer'] || $lac->isInternalUse($_REQUEST['token'])): ?>
                window.location.replace('index.php');
            <?php else: ?>
                window.location.replace('<?=$admission_cfg['IntegratedCentralServer']?>?af=<?=$_SERVER['HTTP_HOST']?>');
            <?php endif; ?>
        });

        $('#btnFinishEdit').click(function () {
            window.location.replace('index_edit.php');
        });

        /**** Helper function START ****/
        window.isUpdatePeriod = <?=$IsUpdate ? 'true' : 'false'?>;

        window.check_input_info = (function (pageId) {
            var $deferredArr = [];

            $('input[type!="file"]').each(function(){
                $(this).val($.trim($(this).val()));
            });
            $('.itemInput-warn').removeClass('itemInput-warn');
            $('.remark-warn').each(function () {
                this.style.setProperty('display', 'none', 'important');
            });
            window.focusElement = null;
            _.each(validateFunc, function (funcs, pId) {
                if (_.isUndefined(pageId) || (pageId == pId)) {
                    _.each(funcs, function (func) {
                        $deferredArr.push(func(pageId));
                    });
                }
            });

            var $deferred = $.Deferred();
            $.when.apply($, $deferredArr).always(function(){
                var isValid = true;
                
                try {
                    _.each(arguments, function (res) {
                        if (typeof (res) == 'boolean') {
                            isValid = isValid && res;
                        } else {
                            isValid = isValid && (res.state() == 'resolved');
                        }
                    });
                }catch(e){
                    isValid = false;
                }

                if (pageId == 'pageInstruction' && !isValid) {
                    $('#errAgreeTerms').show();
                }

                if(isValid){
                    $deferred.resolve();
                }else{
                    $deferred.reject();
                }
            });
            return $deferred;
        });

        window.checkInputRequired = (function ($parent) {
            var isValid = true;

            $parent.find('.remark-warn').addClass('hide');
            $parent.find('input[required], select, textarea[required]').each(function () {
                var $this = $(this);
                var isEmpty = false;

                if ($this.val() == '') {
                    isEmpty = true;
                }

                if ($this.attr('type') == 'radio') {
                    var name = $this.attr('name');
                    if ($parent.find('[name="' + name + '"]:checked').length == 0) {
                        isEmpty = true;
                    }
                }

                if (isEmpty) {
                    $this.parent().find('.remark-warn').removeClass('hide').show();
                    if (!focusElement) {
                        focusElement = $this;
                    }
                }

                isValid = isValid && !isEmpty;
            });

            if (focusElement) {
                focusElement.focus();
            }

            return isValid;
        });
        /**** Helper function END ****/
    });
</script>