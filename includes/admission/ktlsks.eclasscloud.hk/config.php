<?php
	//using:
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

	$admission_cfg['PrintByPDF'] = 1;

	//Please also define lang in admission_lang
	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['waitingforinterview']	= 3;
	$admission_cfg['Status']['gotosecondinterview']	= 6;
	$admission_cfg['Status']['gotothirdinterview']	= 7;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;
	$admission_cfg['Status']['KTLMS_admitted']	= 8;
	$admission_cfg['Status']['KTLMS_reserve']	= 9;
	$admission_cfg['Status']['KTLMS_notadmitted']	= 10;

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;
//	$admission_cfg['Status']['reservedstudent']	= 5;

	$admission_cfg['BirthCertType'] = array();
	$admission_cfg['BirthCertType']['hk']	= 1;
	$admission_cfg['BirthCertType']['others']	= 2;

	$admission_cfg['RefereeType'][1] = 'Committee';
	$admission_cfg['RefereeType'][2] = 'Staff';
	$admission_cfg['RefereeType'][3] = 'Other';

	$admission_cfg['LanguageType'][1] = 'Cantonese';
	$admission_cfg['LanguageType'][2] = 'English';
	$admission_cfg['LanguageType'][3] = 'Putonghua';


	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "SDF2672qwer3we523";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 2; //in MB
	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	/* real paypal info [start] */
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
	$admission_cfg['paypal_to_email'] = 'ktlps@eservices.hkedcity.net';
	$admission_cfg['paypal_signature'] = 'OAmLAzer_Rj21eD23JY3fdBSYbFXtuyrcqMEwPmnhfpwfZs7DnydpS3PMD4';
	$admission_cfg['item_name'] = '小一入學報名費';
	$admission_cfg['item_number'] = 'AF2015';
	$admission_cfg['amount'] = '100.00';
	$admission_cfg['currency_code'] = 'HKD';
    $admission_cfg['hosted_button_id'] = 'K2QP5PUSY55FN';
	$admission_cfg['paypal_name'] = 'Kowloon True Light School (Primary Section)';
	/* real paypal info [End] */
	
	/* for paypal testing [start] */
//	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'henrychan-business@broadlearning.com';
//	$admission_cfg['paypal_signature'] = 'L0X0x2QzO53_4Rpirc1eeUt_tD8Ae2YJbn2geu9LacQqJSuZD2qR534yxUG';
//	$admission_cfg['item_name'] = '小一入學報名費';
//	$admission_cfg['item_number'] = 'AF2015';
//	$admission_cfg['amount'] = '100.00';
//	$admission_cfg['currency_code'] = 'HKD';
//	$admission_cfg['hosted_button_id'] = 'UE543GRJL7W9Q';
//$admission_cfg['paypal_name'] = 'Kowloon True Light School (Primary Section)';
	/* for paypal testing [End] */

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}
?>