<h1><?=$Lang['Admission']['PGInfo']?> Parent Information</h1>
<table class="form_table" style="font-size: 13px">

<tr>
	<td>&nbsp;</td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['F']?> Father</center></td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['M']?> Mother</center></td>
	<!--td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['G']?> Guardian</center></td-->
</tr>
<tr>
	<td class="field_title"><?=$star.$Lang['Admission']['englishname']?> English Name</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G1EnglishName']?></center>
		<?php }else{ ?>
			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" />
		<?php } ?>
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G2EnglishName']?></center>
		<?php }else{ ?>
			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" />
		<?php } ?>
	</td>
	<!--td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G3EnglishName']?></center>
		<?php }else{ ?>
			<input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext" />
		<?php } ?>
	</td-->
</tr>

<tr>
	<td class="field_title"><?=$star.$Lang['Admission']['chinesename']?> Chinese Name</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G1ChineseName']?></center>
		<?php }else{ ?>
			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" />
		<?php } ?>
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G2ChineseName']?></center>
		<?php }else{ ?>
			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['munsang']['levelofeducation']?> Education Level
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G1EducationLevel']?></center>
		<?php }else{ ?>
			<select id="G1EducationLevelSelect" style="width: 99%;">
				<option value=''> - <?=$Lang['Admission']['PleaseSelect'] ?> Please Select - </option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][0] ?> Doctoral Degree</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][1] ?> Master's Degree</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][2] ?> Bachelor's Degree</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][3] ?> Post-secondary</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][4] ?> Secondary</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][5] ?> Others (Please specify.)</option>
			</select>
			<input name="G1EducationLevel" type="text" id="G1EducationLevel" class="textboxtext" style="margin-top: 5px" value="<?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][0] ?> Doctoral Degree"/>
		<?php } ?>
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G2EducationLevel']?></center>
		<?php }else{ ?>
			<select id="G2EducationLevelSelect" style="width: 99%;">
				<option value=''> - <?=$Lang['Admission']['PleaseSelect'] ?> Please Select - </option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][0] ?> Doctoral Degree</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][1] ?> Master's Degree</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][2] ?> Bachelor's Degree</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][3] ?> Post-secondary</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][4] ?> Secondary</option>
				<option><?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][5] ?> Others (Please specify.)</option>
			</select>
			<input name="G2EducationLevel" type="text" id="G2EducationLevel" class="textboxtext" style="margin-top: 5px" value="<?=$Lang['Admission']['KTLMSKG']['parentEducationLevelSelect'][0] ?> Doctoral Degree"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['occupation']?> Occupation
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G1Occupation']?></center>
		<?php }else{ ?>
			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />
		<?php } ?>
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G2Occupation']?></center>
		<?php }else{ ?>
			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['icms']['workaddress']?> Company Address
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G1CompanyAddress']?></center>
		<?php }else{ ?>
			<input name="G1CompanyAddress" type="text" id="G1CompanyAddress" class="textboxtext" />
		<?php } ?>
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G2CompanyAddress']?></center>
		<?php }else{ ?>
			<input name="G2CompanyAddress" type="text" id="G2CompanyAddress" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['CHIUCHUNKG']['contactnumber']?> Contact Number
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G1ContactNumber']?></center>
		<?php }else{ ?>
			<input name="G1ContactNumber" type="text" id="G1ContactNumber" class="textboxtext" maxlength="8" />
		<?php } ?>
	</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G2ContactNumber']?></center>
		<?php }else{ ?>
			<input name="G2ContactNumber" type="text" id="G2ContactNumber" class="textboxtext" maxlength="8" />
		<?php } ?>
	</td>
</tr>

</table>


<table class="form_table" style="font-size: 13px">

<tr>
	<td>&nbsp;</td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['G']?>(<?=$Lang['Admission']['ifAny']?>) Guardian (if any)</center></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Admission']['name']?> Name</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G3ChineseName']?></center>
		<?php }else{ ?>
			<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Admission']['CHIUCHUNKG']['contactnumber']?> Contact Number</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G3ContactNumber']?></center>
		<?php }else{ ?>
			<input name="G3ContactNumber" type="text" id="G3ContactNumber" class="textboxtext" maxlength="8" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Admission']['CHIUCHUNKG']['relationship']?> Relationship</td>
	<td class="form_guardian_field">
		<?php if($IsConfirm){ ?>
			<center><?=$formData['G3Relationship']?></center>
		<?php }else{ ?>
			<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

</table>

<script>
var EducationLevelArr = $('#G1EducationLevelSelect option').map(function() {
    return this.value;
}).get();

function updateEducationLevel(){
	var $select = $('#G1EducationLevelSelect, #G2EducationLevelSelect');
	$select.each(function(){
		if($(this)[0].selectedIndex == 0){
			$(this).next().hide().val('');
		}else if($(this)[0].selectedIndex == ($(this).find('option').length - 1)){
			if( $.inArray( $(this).next().val(), EducationLevelArr ) > -1 ){
				$(this).next().show().val('');
			}
		}else{
			$(this).next().hide().val($(this).val());
		}
	});
	/* Old Code
	var $select = $('#G1EducationLevelSelect');
	if($select[0].selectedIndex == 5){
		if( $.inArray( $select.next().val(), EducationLevelArr ) > -1 ){
			$select.next().show().val('');
		}
	}else{
		$select.next().hide().val($select.val());
	}
	
	$select = $('#G2EducationLevelSelect');
	if($select[0].selectedIndex == 5){
		if( $.inArray( $select.next().val(), EducationLevelArr ) > -1 ){
			$select.next().show().val('');
		}
	}else{
		$select.next().hide().val($select.val());
	}
	*/
}

$('#G1EnglishName, #G1ChineseName, #G1EducationLevelSelect, #G1Occupation, #G1CompanyAddress, #G1ContactNumber').change(updateEducationLevel);
$('#G2EnglishName, #G2ChineseName, #G2EducationLevelSelect, #G2Occupation, #G2CompanyAddress, #G2ContactNumber').change(updateEducationLevel);


$('#G1EducationLevelSelect, #G2EducationLevelSelect').change();
$('#G1EducationLevel, #G2EducationLevel').hide();
</script>