<h1><?=$Lang['Admission']['otherInfo']?> Other Information</h1>
<table class="form_table" style="font-size: 13px">

<?php if($IsConfirm){ ?>
	<tr>
		<tr>
			<td class="field_title"><?=$Lang['Admission']['applyLevel']?> Apply Level</td>
			<td><?=$classLevel?></td>
		</tr>
	</tr>
<?php } ?>
<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['KTLMSKG']['MotherTongue']?> Mother Tongue
	</td>
	<td>
		<?php if($IsConfirm){
			if($formData['MotherTongue'] == 1){
				echo $Lang['Admission']['RMKG']['cantonese'];
			}else if($formData['MotherTongue'] == 2){
				echo $Lang['Admission']['RMKG']['eng'];
			}else if($formData['MotherTongue'] == 3){
				echo $Lang['Admission']['RMKG']['pth'];
			}
		}else{ ?>
			<input type="radio" value="1" id="MotherTongue1" name="MotherTongue">			
			<label for="MotherTongue1">
				<?=$Lang['Admission']['RMKG']['cantonese']?>
				Cantonese
			</label><br />
			
			<input type="radio" value="2" id="MotherTongue2" name="MotherTongue">		
			<label for="MotherTongue2">
				<?=$Lang['Admission']['RMKG']['eng']?>
				English
			</label><br />

			<input type="radio" value="3" id="MotherTongue3" name="MotherTongue">		
			<label for="MotherTongue3">
				<?=$Lang['Admission']['RMKG']['pth']?>
				Putonghua
			</label>

		<?php } ?>
	</td>
</tr>
<!-------------- Siblings START -------------->
<tr>
	<td colspan="4">
		<table class="form_table" style="font-size: 13px">
		
		<tr>
			<td rowspan="4" style="width: 300px;">
				<?=$Lang['Admission']['KTLMSKG']['siblings']?><br />
				Name of siblings or relatives in this school &amp; class attending
			</td>
			<td>&nbsp;</td>
			<td class="form_guardian_head"><center><?=$Lang['Admission']['name']?> Name</center></td>
			<td class="form_guardian_head"><center><?=$Lang['Admission']['class']?> Class</center></td>
			<td class="form_guardian_head"><center><?=$Lang['Admission']['relationship']?> Relationship</center></td>
		</tr>
		<?php for($i=1;$i<=3;$i++){ ?>
		<tr>
			<td class="field_title" style="text-align:right;width:50px;">(<?=$i?>)</td>
			<td class="form_guardian_field">
				<?php if($IsConfirm){ ?>
					<center><?=$formData['OthersRelativeStudiedName'.$i]?></center>
				<?php }else{ ?>
					<input name="OthersRelativeStudiedName<?=$i?>" type="text" id="OthersRelativeStudiedName<?=$i?>" class="textboxtext" />
				<?php } ?>
			</td>
			
			<td class="form_guardian_field">
				<?php if($IsConfirm){ ?>
					<center><?=$formData['OthersRelativeClassPosition'.$i]?></center>
				<?php }else{ ?>
					<input name="OthersRelativeClassPosition<?=$i?>" type="text" id="OthersRelativeClassPosition<?=$i?>" class="textboxtext" />
				<?php } ?>
			</td>
			
			<td class="form_guardian_field">
				<?php if($IsConfirm){ ?>
					<center><?=$formData['OthersRelativeRelationship'.$i]?></center>
				<?php }else{ ?>
					<input name="OthersRelativeRelationship<?=$i?>" type="text" id="OthersRelativeRelationship<?=$i?>" class="textboxtext" />
				<?php } ?>
			</td>
		</tr>
		<?php } ?>
		
		</table>
	</td>
</tr>
<!-------------- Siblings END -------------->


<!-------------- Referee START -------------->
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['introducedName']?>(<?=$Lang['Admission']['ifAny']?>) Name of Referee (if any)
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<span><?=$formData['RefereeName']?></span>
		<?php }else{ ?>
			<input name="RefereeName" type="text" id="RefereeName" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['introducedationship']?><br /> Relationship (between referee and the school)
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?php 
				if($formData['RefereeType1'] || $formData['RefereeType2'] || $formData['RefereeType3']){
					if($formData['RefereeType1']){ 
						echo "{$Lang['Admission']['KTLMSKG']['RefereeType']['Committee']} School Management Committee member<br />";
					}
					if($formData['RefereeType2']){ 
						echo "{$Lang['Admission']['KTLMSKG']['RefereeType']['Staff']} Staff member of our Primary or Secondary Section<br />";
					}
					if($formData['RefereeType3']){ 
						echo $formData['RefereeTypeOther'];
					}
				}else{
					echo '--';
				}
			?>
		<?php }else{ ?>
			<input type="checkbox" value="1" id="RefereeType1" name="RefereeType1">
			<label for="RefereeType1"> <?=$Lang['Admission']['KTLMSKG']['RefereeType']['Committee']?> School Management Committee member</label><br />
			
			<input type="checkbox" value="2" id="RefereeType2" name="RefereeType2">
			<label for="RefereeType2"> <?=$Lang['Admission']['KTLMSKG']['RefereeType']['Staff']?> Staff member of our Primary or Secondary Section</label><br />
			
			<input type="checkbox" value="3" id="RefereeType3" name="RefereeType3">
			<label for="RefereeType3">
				<?=$Lang['Admission']['KTLMSKG']['RefereeType']['Other']?> Others:&nbsp;
				<input name="RefereeTypeOther" type="text" id="RefereeTypeOther" class="textboxtext" style="width:75%" onkeypress="$('#RefereeType3').attr('checked', 'checked')" />
			</label>
		<?php } ?>
	</td>
</tr>

</table>
<!-------------- Referee END -------------->


<!-------------- Fee START -------------->
<?if ($lac->isInternalUse($_REQUEST['token'])){?>
<h1><?=$Lang['Admission']['applicationfee']?> Application Fee</h1>
<table class="form_table" style="font-size: 13px">
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['bankName']?> Bank Name
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['BankName']?>
		<?php }else{ ?>
			<input name="BankName" type="text" id="BankName" class="textboxtext" />
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['chequeNum']?> Cheque No.
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['ChequeNo']?>
		<?php }else{ ?>
			<input name="ChequeNo" type="text" id="ChequeNo" class="textboxtext" />
		<?php } ?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['Payed']?> Payment settled
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['Payed'])? "{$Lang['General']['Yes']} Yes" : "<span style=\"color:red\">{$Lang['General']['No']} No</span>" ?>
		<?php }else{ ?>
			<input name="Payed" type="checkbox" id="Payed" value="1" />
			
		<?php } ?>
	</td>
</tr>
</table>
<?}?>
<!-------------- Fee END -------------->


<script>
//$('.fluency').change(function(){
//	debugger;
//	var thisVal = $(this).val();
//	
//	var values = $('.fluency').map(function(idx, elem) {
//		return $(elem).val();
//	}).get();
//	
//	var missingVal;
//	if($.inArray("1", values) == -1){
//		missingVal = "1";
//	}else if($.inArray("2", values) == -1){
//		missingVal = "2";
//	}else if($.inArray("3", values) == -1){
//		missingVal = "3";
//	}
//	
//	$('.fluency[value="' + thisVal + '"]').val(missingVal);
//	$(this).val(thisVal);
//});
</script>