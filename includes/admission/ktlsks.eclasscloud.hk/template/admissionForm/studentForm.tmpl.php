<h1><?=$Lang['Admission']['studentInfo']?> Student Information</h1>
<table class="form_table" style="font-size: 13px">

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['chinesename']?> Chinese Name
	</td>
	<td>
		<table style="font-size: 13px">
		<tr>
			<td width="85px">
				(<?=$Lang['Admission']['csm']['surname_b5']?>)<br/>
				(Surname)
			</td>
			<td>
				<?php if($IsConfirm){ ?>
					<?=$formData['studentssurname_b5']?>
				<?php }else{ ?>
					<input name="studentssurname_b5" type="text" id="studentssurname_b5" class="textboxtext" />
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td width="85px">
				(<?=$Lang['Admission']['csm']['firstname_b5']?>)<br/>
				(First Name)
			</td>
			<td>
				<?php if($IsConfirm){ ?>
					<?=$formData['studentsfirstname_b5']?>
				<?php }else{ ?>
					<input name="studentsfirstname_b5" type="text" id="studentsfirstname_b5" class="textboxtext" />
				<?php } ?>
			</td>
		</tr>
		</table>		
	</td>
	
	<td class="field_title">
		<?=$star.$Lang['Admission']['englishname']?> English Name
	</td>
	<td>
		<table style="font-size: 13px">
			<tr>
				<td width="85px">
					(<?=$Lang['Admission']['csm']['surname_en']?>)<br/>
					(Surname)
				</td>
				<td>
					<?php if($IsConfirm){ ?>
						<?=$formData['studentssurname_en']?>
					<?php }else{ ?>
						<input name="studentssurname_en" type="text" id="studentssurname_en" class="textboxtext" />
					<?php } ?>
				</td>
			</tr>
			
			<tr>
				<td width="85px">
					(<?=$Lang['Admission']['csm']['firstname_en']?>)<br/>
					(First Name)
				</td>
				<td>
					<?php if($IsConfirm){ ?>
						<?=$formData['studentsfirstname_en']?>
					<?php }else{ ?>
						<input name="studentsfirstname_en" type="text" id="studentsfirstname_en" class="textboxtext" />
					<?php } ?>
				</td>
			</tr>
		</table>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['dateofbirth']?> Date of Birth
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['gender']?> Gender
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender">
			<label for="StudentGender1"> <?=$Lang['Admission']['genderType']['M']?> M</label>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender">
			<label for="StudentGender2"> <?=$Lang['Admission']['genderType']['F']?> F</label>
		<?php } ?>
	</td>
</tr>


<tr>	
   	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['placeofbirth']?> Place of Birth
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentPlaceOfBirth']?>
		<?php }else{ ?>
			<input name="StudentPlaceOfBirth" type="text" id="StudentPlaceOfBirth" class="textboxtext" />
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['TSUENWANBCKG']['nationality']?> Nationality
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['Nationality']?>
		<?php }else{ ?>
			<input name="Nationality" type="text" id="Nationality" class="textboxtext" />
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['KTLMSKG']['IdType']?> Type of Identity Document
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?php 
				if($formData['BirthCertType'] == $admission_cfg['BirthCertType']['hk']){ 
					echo "{$Lang['Admission']['BirthCertType']['hk2']} HK Birth Cert.";
				}else{
					echo $formData['BirthCertTypeOther'];
				}
			?>
		<?php }else{ ?>
			<input type="radio" value="<?=$admission_cfg['BirthCertType']['hk']?>" id="BirthCertType1" name="BirthCertType" checked>
			<label for="BirthCertType1"> <?=$Lang['Admission']['BirthCertType']['hk2']?> HK Birth Cert.</label><br />
			
			<div style="white-space: nowrap;">
				<input type="radio" value="<?=$admission_cfg['BirthCertType']['others']?>" id="BirthCertType2" name="BirthCertType">
				<label for="BirthCertType2">
					<?=$Lang['Admission']['BirthCertType']['others']?> Others:&nbsp;
				</label>
				<input name="BirthCertTypeOther" type="text" id="BirthCertTypeOther" style="width: auto; display:none;" class="textboxtext" onkeypress="$('#BirthCertType2').attr('checked', 'checked')" />
			</div>
			(例如：香港特別行政區護照
			<br/>e.g.: HKSAR passport)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['KTLMSKG']['IdNum']?> Number of Identity Document
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentBirthCertNo']?>
		<?php }else{ ?>
			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" value="<?=$BirthCertNo ?>"/>
			<!--<br/>(例如 E.g.: "A123456(7)"，<br/>請輸入 please enter "A1234567")-->
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['KTLMSKG']['address']?> Residential Address
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentHomeAddress']?>
		<?php }else{ ?>
			<input name="StudentHomeAddress" type="text" id="StudentHomeAddress" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['correspondenceAddress']?> (<?=$Lang['Admission']['KTLMSKG']['addressDifference']?>) <br />
		Correspondence Address (if different)
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentContactAddress']?>
		<?php }else{ ?>
			<input name="StudentContactAddress" type="text" id="StudentContactAddress" class="textboxtext" />
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['homephoneno']?> Home Phone
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentHomePhoneNo']?>
		<?php }else{ ?>
			<input name="StudentHomePhoneNo" type="text" id="StudentHomePhoneNo" class="textboxtext" maxlength="8" />
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['religion']?> Religion
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['religion']?>
		<?php }else{ ?>
			<input name="religion" type="text" id="religion" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star.$Lang['Admission']['mgf']['email']?> Email Address
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentEmail']?>
		<?php }else{ ?>
			<input name="StudentEmail" type="text" id="StudentEmail" class="textboxtext" />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['KTLMSKG']['twins']?> Application from Twins
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['twins'])?'Y':'N'?>
		<?php }else{ ?>
			<input type="radio" value="1" id="twinsY" name="twins">
			<label for="twinsY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
			<input type="radio" value="0" id="twinsN" name="twins" checked>
			<label for="twinsN"> <?=$Lang['Admission']['no']?> No</label>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['KTLMSKG']['twinsID']?> Twins' Number of Identity Document
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['twinsapplicationid']?>
		<?php }else{ ?>
			<input name="twinsapplicationid" type="text" id="twinsapplicationid" class="textboxtext" onkeypress="$('#twinsY').attr('checked', 'checked')"/>
		<?php } ?>
	</td>
</tr>


</table>

<script type="text/javascript">
$('#BirthCertType1, #BirthCertType2').change(function (){
	if($('#BirthCertType2').attr('checked') == 'checked'){
		$('#BirthCertTypeOther').show().focus();
	}else{
		$('#BirthCertTypeOther').hide();
	}
});
</script>
