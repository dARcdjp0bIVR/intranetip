<?php

class admission_briefing extends admission_briefing_base{
	
	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = '')
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $Lang;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$result = array();
		
		######## Get Applicant Info START ########
		$applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
		$Email = ($email)?$email:$applicant['Email'];
		$ApplicantID = $applicant['ApplicantID'];
		$DeleteRecordPassKey = $applicant['DeleteRecordPassKey'];
		######## Get Applicant Info END ########

		######## Setup Email Content START ########
		if($subject == ''){
			$email_subject = "簡介會留座通知 Notice of Reservation for Information Talk";
		}
		else{
			$email_subject = $subject;
		}
		
		if($message == ''){
			if($Briefing_ApplicantID == 0){
				return true;
				/* Fail case no email * /
				$email_message = <<<EMAIL
					{$Lang['Admission']['msg']['applicationnotcomplete']} 
					{$Lang['Admission']['msg']['tryagain']}
					<br /><br />
					Application is Not Completed. Please try to apply again!
EMAIL;
				/* */
			}else{
				$deleteRecordLink = $this->getApplicantDeleteLink($Briefing_ApplicantID, $DeleteRecordPassKey);
				$email_message = <<<EMAIL
					<font color="green">
						閣下已成功留座，參考編號為
					</font>
					<font size="5">
						<u>{$ApplicantID}</u>
					</font>。
					
					<br />
					如需取消留座，請按以下連結：
					<br />
					{$deleteRecordLink}
					<br /><br />
					<font color="green">
						Reservation is made. Reference number is 
					</font>
					<font size="5">
						<u>{$ApplicantID}</u>
					</font>.
					<br />
					To cancel your reservation, please click the link below.
					<br />
					{$deleteRecordLink}
EMAIL;
			}
		}else{
			$email_message = $message;
		}
		######## Setup Email Content END ########
		
		$sent_ok = true;
		if($Email != '' && intranet_validateEmail($Email)){
			$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($Email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
		}else{
			$sent_ok = false;
		}
			
		return $sent_ok;
	}
	
	public function getAllBriefingEmailBySchoolYearID($schoolYearID){
		$sql = "SELECT
			ABAI.Email
		FROM
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		INNER JOIN
			ADMISSION_BRIEFING_SETTING ABS
		ON
			ABAI.BriefingID = ABS.BriefingID
		AND
			ABS.SchoolYearID = '{$schoolYearID}'";
		$rs = $this->returnVector($sql);
		
		return $rs;
	}
	
	public function insertBriefingApplicant($schoolYearID, $Data){
   		extract($Data);
   		
   		$emailArr = $this->getAllBriefingEmailBySchoolYearID($schoolYearID);
   		if(in_array($Email, $emailArr)){
   			return 0;
   		}
   		
   		return parent::insertBriefingApplicant($schoolYearID, $Data);
	}
} // End Class