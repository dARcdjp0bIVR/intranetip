<?php
// modifying by:
/**
 * ******************
 * Change Log :
 * Date 2018-09-03 [Pun]
 * File Created
 *
 * ******************
 */
// error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);ini_set('display_errors', 1);
include_once("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/DynamicAdmissionFormSystem/DynamicAdmissionFormSystem.php");

use \AdmissionSystem\DynamicAdmissionFormSystem;

class admission_cust extends \AdmissionSystem\AdmissionCustBase
{

    const STUDENT_ACADEMIC_SCHOOL_COUNT = 1;

    const RELATIVES_COUNT = 2;

    public function __construct()
    {
        global $plugin;

        if ($plugin['eAdmission_devMode']) {
            error_reporting(E_ALL & ~E_NOTICE);
            ini_set('display_errors', 1);
            if ($_SERVER['HTTP_HOST'] == '192.168.0.171:31002') {
//                 $this->AdmissionFormSendEmail = false;
            }
        }

        parent::__construct();
        $this->init();
    }

    private function init()
    {
        /* #### FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER, array(
            $this,
            'genNextApplicationNumberFormat'
        ));
        /* #### FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER END #### */

        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function ($emailTitle, $applicationId) {
            global $admission_cfg;
            $result = $this->getPaymentResult('', '', '', '', $applicationId);

            $hasPaid = 0;
            if ($result) {
                foreach ($result as $aResult) {
                    if ($aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']) {
                        $hasPaid = 1;
                        break;
                    }
                }
            }
            else{
            	$result = $this->getAlipayPaymentResult('', '', '', '', $applicationId);
            	if ($result) {
	                foreach ($result as $aResult) {
	                    if ($aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']) {
	                        $hasPaid = 1;
	                        break;
	                    }
	                }
	            }
            }
            
            $basicSettings = $this->getBasicSettings(99999, array (
						'enablepaypal',
						'enablealipayhk'
					));
            if ($hasPaid || !$basicSettings['enablepaypal'] && !$basicSettings['enablealipayhk']) {
                return "新生入學申請 - 確認通知 (申請編號：{$applicationId}) New applications – confirmation notice (Application #: {$applicationId})";
            }
            return "付款查核 Verifying your payment";
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE END #### */

        /* #### FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC, (function ($emailBcc, $applicationId) {
            global $admission_cfg;
            return array($admission_cfg['EmailBcc']);
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummySchoolInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummyRelativesInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummyCustInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_UPDATE_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateDynamicField'
        ));
        /* #### ACTION_APPLICANT_UPDATE_INFO END #### */

        /* #### ACTION_APPLICANT_UPDATE_*_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INFO, array(
            $this,
            'updateApplicantInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_*_INFO END #### */

        /* #### FILTER_EXPORT_INTERVIEW_LIST START #### */
        $this->addFilter(self::FILTER_EXPORT_INTERVIEW_LIST, (function ($dataArray) {
            usort($dataArray, array($this, 'sortExportInterviewList'));
            return $dataArray;
        }));
        /* #### FILTER_EXPORT_INTERVIEW_LIST END #### */

        /* #### FILTER_EXPORT_APPLICANT_LIST START #### */
        $this->addFilter(self::FILTER_EXPORT_APPLICANT_LIST, array(
            $this,
            'filterExportApplicantList'
        ));
        /* #### FILTER_EXPORT_APPLICANT_LIST END #### */
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID)
    {
        $result = true;

        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i++) {
            $sql = "INSERT INTO
                ADMISSION_STU_PREV_SCHOOL_INFO
            (
                ApplicationID,
                SchoolOrder,
                DateInput,
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";

            $result = $result && $this->db_db_query($sql);
        }

        if (!$result) {
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummyRelativesInfo($ApplicationID)
    {
        $result = true;

        for ($i = 0; $i < self::RELATIVES_COUNT; $i++) {
            $sql = "INSERT INTO
                ADMISSION_SIBLING
            (
                ApplicationID,
                SiblingOrder,
                DateInput,
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";
            $result = $result && $this->db_db_query($sql);
        }

        if (!$result) {
            throw new \Exception('Cannot insert dummy relatives info');
        }
        return true;
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummyCustInfo($ApplicationID)
    {
        global $admission_cfg;
        $result = $this->insertApplicationCustInfo(array('Code' => 'Remarks'), $ApplicationID);
        $result = $result && $this->insertApplicationCustInfo(array(
                'Code' => 'AdmitStatus',
                'Value' => $admission_cfg['AdmitStatus']['NotUpdated']
            ), $ApplicationID);
        $result = $result && $this->insertApplicationCustInfo(array(
                'Code' => 'InterviewStatus',
                'Value' => $admission_cfg['InterviewStatus']['NotUpdated']
            ), $ApplicationID);

        if (!$result) {
            throw new \Exception('Cannot insert dummy cust info');
        }
        return true;
    }

    public function genNextApplicationNumberFormat($nextApplicationId, $schoolYearID, $data)
    {
        global $admission_cfg;
        $yearStart = ($data['isImport']?'H':'').substr(date('Y', getStartOfAcademicYear('', $this->schoolYearID)), -2);
		$basicSettings = $this->getBasicSettings(99999, array (
						'applicationnoformatstartfrom'
					));

        $defaultNo = $yearStart.($basicSettings['applicationnoformatstartfrom']?str_pad($basicSettings['applicationnoformatstartfrom'], 4, '0', STR_PAD_LEFT):"0001");
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);
		
//		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
		
//		if($this->returnArray($sql)){
//			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
//					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( SUBSTRING( ApplicationID, '".(strlen($prefix)+1)."' ) , UNSIGNED ) ) +1, 4, '0')) 
//					FROM ADMISSION_OTHERS_INFO 
//					WHERE ApplicationID like '".$prefix."%'".$cond;
//		}
//		else{
//			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
//					Values ('".$defaultNo."')";
//		}
//			$result = $this->db_db_query($sql);
//					
//			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
//			$newNo = $this->returnArray($sql);
//			return $newNo[0]['ApplicationID'];
		return (substr($nextApplicationId, -4) > 1 ? $nextApplicationId:$defaultNo);
    }

    protected function updateDynamicField($Data, $ApplicationID, $groupId = null)
    {
        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        if ($groupId) {
            $fields = $dafs->getFieldsByGroupId($groupId, 1);
        } else {
            $fields = $dafs->getLastActiveAcademicYearFields();
        }
        $result = true;

        $updateArr = array();
        foreach ($fields as $field) {
            $value = $Data["field_" . $field['FieldID']];
            $table = $field['OtherAttributeArr']['DbTableName'];
            $_field = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
            $fieldName = $_field[0];
            $sqlWhere = $field['ExtraArr']['sqlWhere'];

            if (!$table || !$fieldName || $field['ExtraArr']['skipSaveDb']) {
                continue;
            }

            #### Save sql START ####
            switch ($field['Type']) {
                case DynamicAdmissionFormSystem::FIELD_TYPE_TEXT:
                case DynamicAdmissionFormSystem::FIELD_TYPE_TEXTAREA:
                case DynamicAdmissionFormSystem::FIELD_TYPE_RADIO:
                case DynamicAdmissionFormSystem::FIELD_TYPE_CHECKBOX:
                case DynamicAdmissionFormSystem::FIELD_TYPE_SELECT:
                    $value = intranet_htmlspecialchars($value);
                    break;
                case DynamicAdmissionFormSystem::FIELD_TYPE_DATE:
                    $year = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_year"]), 2, '0', STR_PAD_LEFT);
                    $month = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_month"]), 2, '0', STR_PAD_LEFT);
                    $day = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_day"]), 2, '0', STR_PAD_LEFT);
                    $value = "{$year}-{$month}-{$day}";
                    break;
                case DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR:
                    $value = array_values(array_filter((array)$value));
                    for ($i = 0; $i < 3; $i++) {
                        $v = IntegerSafe($value[$i]);
                        $fieldName = 'ApplyDayType' . ($i + 1);
                        $updateArr[$table][$sqlWhere][] = "{$fieldName}='{$v}'";
                    }
                    continue 2; // Skip default add sql logic
                case DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE:
		        	$value = $Data["field_{$field['FieldID']}_value"];
		        	$valueType = $Data["field_{$field['FieldID']}_type"];
			        $updateArr[$table][$sqlWhere][] = "BirthCertType='{$valueType}'";
		        	break;
                case DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY:
                    continue 2;
                case DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_FIELD:
                    $_fieldName = $field['ExtraArr']['field'];
                    switch ($_fieldName) {
                        case 'brother_sister_1_schoolDuration':
                        case 'brother_sister_2_schoolDuration':
                        case 'previousSchoolYear':
                            $start = IntegerSafe($Data["field_{$field['FieldID']}_start"]);
                            $end = IntegerSafe($Data["field_{$field['FieldID']}_end"]);
                            $value = "{$start} - {$end}";
                            break;
                        case 'previousSchoolClass':
                            $start = intranet_htmlspecialchars($Data["field_{$field['FieldID']}_start"]);
                            $end = intranet_htmlspecialchars($Data["field_{$field['FieldID']}_end"]);
                            $value = "{$start} - {$end}";
                            break;
                    }
                    break;

            }
            #### Save sql END ####

            $updateArr[$table][$sqlWhere][] = "{$fieldName}='{$value}'";
        }

        foreach ($updateArr as $table => $d1) {
            foreach ($d1 as $cond => $updateFields) {
                $updateFieldSql = implode(',', $updateFields);
                $sql = "UPDATE
                    {$table}
                SET
                    {$updateFieldSql}
                WHERE
                   ApplicationID='{$ApplicationID}'
                ";
                if ($cond) {
                    $sql .= " AND {$cond}";
                }
                $result = $result && $this->db_db_query($sql);
            }
        }
        return $result;
    }

    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID)
    {
        $result = true;

        if ($type == 'remarks') {
            $result = $this->updateApplicationStatus($Data, $ApplicationID);
            if (isset($Data['RatingClass'])) {
	            $result = $this->updateApplicationCustInfo(array(
	                'filterApplicationId' => $ApplicationID,
	                'filterCode' => 'RatingClass',
	                'Value' => $Data['RatingClass'],
	            ));
	        }
        } elseif ($type == 'creative_interview_follow_up') {
            $result = $this->updateApplicationInterviewFollowUp($Data, $ApplicationID);
        } else {
            $typeArr = explode('_', $type);
            $result = $this->updateDynamicField($Data, $ApplicationID, $typeArr[1]);
        }

        if (!$result) {
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }

    function updateApplicationStatusByIds($applicationIds, $status)
    {
        $result = true;
        if (isset($_REQUEST['status']) && $_REQUEST['status']) {
            $result = parent::updateApplicationStatusByIds($applicationIds, $status);
        }

        $sql = "SELECT ApplicationID  FROM ADMISSION_OTHERS_INFO WHERE RecordID IN (" . $applicationIds . ")";
        $ApplicationIdArr = $this->returnVector($sql);

        $Data = array();
        if (isset($_REQUEST['applicantInterviewStatus'])) {
            $Data['applicantInterviewStatus'] = $_REQUEST['applicantInterviewStatus'];
        }
        if (isset($_REQUEST['RegisterEmail'])) {
            $Data['RegisterEmail'] = $_REQUEST['RegisterEmail'];
            $Data['RegisterEmailDate_year'] = date('Y');
            $Data['RegisterEmailDate_month'] = date('m');
            $Data['RegisterEmailDate_day'] = date('d');
        }
        if (isset($_REQUEST['AdmitStatus'])) {
            $Data['AdmitStatus'] = $_REQUEST['AdmitStatus'];
        }
        if (isset($_REQUEST['RatingClass'])) {
            $Data['RatingClass'] = $_REQUEST['RatingClass'];
        }
        if (isset($_REQUEST['PaidReservedFee'])) {
            $Data['PaidReservedFee'] = $_REQUEST['PaidReservedFee'];
        }
        if (isset($_REQUEST['PaidRegistrationCertificate'])) {
            $Data['PaidRegistrationCertificate'] = $_REQUEST['PaidRegistrationCertificate'];
        }
        if (isset($_REQUEST['BookFeeEmail'])) {
            $Data['BookFeeEmail'] = $_REQUEST['BookFeeEmail'];
            $Data['BookFeeEmailDate_year'] = date('Y');
            $Data['BookFeeEmailDate_month'] = date('m');
            $Data['BookFeeEmailDate_day'] = date('d');
        }
        if (isset($_REQUEST['PaidBookFee'])) {
            $Data['PaidBookFee'] = $_REQUEST['PaidBookFee'];
        }
        if (isset($_REQUEST['OpeningNoticeEmail'])) {
            $Data['OpeningNoticeEmail'] = $_REQUEST['OpeningNoticeEmail'];
            $Data['OpeningNoticeEmailDate_year'] = date('Y');
            $Data['OpeningNoticeEmailDate_month'] = date('m');
            $Data['OpeningNoticeEmailDate_day'] = date('d');
        }
        if (isset($_REQUEST['PaidTuitionFee'])) {
            $Data['PaidTuitionFee'] = $_REQUEST['PaidTuitionFee'];
        }
        if (isset($_REQUEST['GiveUpOffer'])) {
            $Data['GiveUpOffer'] = $_REQUEST['GiveUpOffer'];
        }
        foreach ((array)$ApplicationIdArr as $applicationId) {
            $result = $result && $this->updateApplicationInterviewFollowUp($Data, $applicationId);
        }

        return $result;
    }

    /**
     * Portal - update interview follow up
     */
    function updateApplicationInterviewFollowUp($Data, $ApplicationID)
    {
        extract($Data);
        $RegisterEmailDate_month = str_pad($RegisterEmailDate_month, 2, '0', STR_PAD_LEFT);
        $RegisterEmailDate_day = str_pad($RegisterEmailDate_day, 2, '0', STR_PAD_LEFT);
        $BookFeeEmailDate_month = str_pad($BookFeeEmailDate_month, 2, '0', STR_PAD_LEFT);
        $BookFeeEmailDate_day = str_pad($BookFeeEmailDate_day, 2, '0', STR_PAD_LEFT);
        $OpeningNoticeEmailDate_month = str_pad($OpeningNoticeEmailDate_month, 2, '0', STR_PAD_LEFT);
        $OpeningNoticeEmailDate_day = str_pad($OpeningNoticeEmailDate_day, 2, '0', STR_PAD_LEFT);

        $result = array();
        $updateArr = array();

        if (isset($Data['applicantInterviewStatus'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'InterviewStatus',
                'Value' => $applicantInterviewStatus,
            );
        }
        if (isset($Data['RegisterEmail'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'RegisterEmail',
                'Value' => $RegisterEmail,
            );

            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'RegisterEmailDate',
                'Value' => "{$RegisterEmailDate_year}-{$RegisterEmailDate_month}-{$RegisterEmailDate_day}",
            );
        }
        if (isset($Data['AdmitStatus'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'AdmitStatus',
                'Value' => $AdmitStatus,
            );
        }
        if (isset($Data['RatingClass'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'RatingClass',
                'Value' => $RatingClass,
            );
        }
        if (isset($Data['PaidReservedFee'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidReservedFee',
                'Value' => $PaidReservedFee,
            );
        }
        if (isset($Data['PaidRegistrationCertificate'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidRegistrationCertificate',
                'Value' => $PaidRegistrationCertificate,
            );
        }
        if (isset($Data['BookFeeEmail'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'BookFeeEmail',
                'Value' => $BookFeeEmail,
            );
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'BookFeeEmailDate',
                'Value' => "{$BookFeeEmailDate_year}-{$BookFeeEmailDate_month}-{$BookFeeEmailDate_day}",
            );
        }
        if (isset($Data['PaidBookFee'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidBookFee',
                'Value' => $PaidBookFee,
            );
        }
        if (isset($Data['OpeningNoticeEmail'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'OpeningNoticeEmail',
                'Value' => $OpeningNoticeEmail,
            );
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'OpeningNoticeEmailDate',
                'Value' => "{$OpeningNoticeEmailDate_year}-{$OpeningNoticeEmailDate_month}-{$OpeningNoticeEmailDate_day}",
            );
        }
        if (isset($Data['PaidTuitionFee'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidTuitionFee',
                'Value' => $PaidTuitionFee,
            );
        }
        if (isset($Data['GiveUpOffer'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'GiveUpOffer',
                'Value' => $GiveUpOffer,
            );
        }

        foreach ($updateArr as $data) {
            $result[] = $this->updateApplicationCustInfo($data);
        }

        return (!in_array(false, $result, true));
    }

    function getApplicationDetails($schoolYearID, $data = array())
    {
        global $admission_cfg;
        extract($data);

        $sort = $sortby ? "$sortby $order" : "application_id";
        $limit = $page ? " LIMIT " . (($page - 1) * $amount) . ", $amount" : "";
        $cond = !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= $status ? " AND s.Status='" . $status . "'" : "";
        if ($status == $admission_cfg['Status']['waitingforinterview']) {
            if ($interviewStatus == 1) {
                $cond .= " AND s.isNotified = 1";
            } else {
                $cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
            }
        }

        if ($paymentStatus == $admission_cfg['PaymentStatus']['OnlinePayment']) {
            $paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
            $cond .= " AND pi.ApplicationID IS NOT NULL  ";
        } else if ($paymentStatus == $admission_cfg['PaymentStatus']['OtherPayment']) {
            $paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
            $cond .= " AND pi.ApplicationID IS NULL  ";
        }

		if($custSelection){
			if($custSelection == 'O'){
				$cond .= " AND o.ApplicationID NOT LIKE 'H%' ";
			}
			else if($custSelection == 'H'){
				$cond .= " AND o.ApplicationID LIKE 'H%' ";
			}
        }
		
        if (!empty($keyword)) {
            $cond .= "
				AND (
                    stu.EnglishName LIKE '%" . $keyword . "%'
                    OR stu.ChineseName LIKE '%" . $keyword . "%'
                    OR stu.ApplicationID LIKE '%" . $keyword . "%'
                    OR pg.EnglishName LIKE '%" . $keyword . "%'
                    OR pg.ChineseName LIKE '%" . $keyword . "%'
                    OR pg.Mobile LIKE '%" . $keyword . "%'
                    OR stu.BirthCertNo LIKE '%" . $keyword . "%'
                    OR stu.HomeTelNo LIKE '%" . $keyword . "%'
				)
			";
        }

        $from_table = "
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			$paymentStatus_cond
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'
			" . $cond . "

    	";
        $sql = "SELECT DISTINCT o.ApplicationID " . $from_table;
        $applicationIDAry = $this->returnVector($sql);
        $applicationIdSql = implode("','", $applicationIDAry);

        //// Filter cust info START ////
        $sql = "SELECT
                ApplicationID,
                Code,
                Value
            FROM
                ADMISSION_CUST_INFO aci
            WHERE
                ApplicationID IN ('{$applicationIdSql}')
            ";
        $rs = $this->returnResultSet($sql);
        $applicationStatusArr = BuildMultiKeyAssoc($rs, array('ApplicationID', 'Code'), array('Value'), true);

        $applicationIDAry = array();
        foreach ($applicationStatusArr as $ApplicationID => $applicationStatus) {
            $isValid = true;

            if (isset($_GET['applicantInterviewStatus']) && $_GET['applicantInterviewStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['InterviewStatus'] == $_GET['applicantInterviewStatus']);
            }
            if (isset($_GET['RegisterEmail']) && $_GET['RegisterEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['RegisterEmail'] == $_GET['RegisterEmail']);
            }
            if (isset($_GET['AdmitStatus']) && $_GET['AdmitStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['AdmitStatus'] == $_GET['AdmitStatus']);
            }
            if (isset($_GET['RatingClass']) && $_GET['RatingClass'] !== '') {
                $isValid = $isValid && ($applicationStatus['RatingClass'] == $_GET['RatingClass']);
            }
            if (isset($_GET['PaidReservedFee']) && $_GET['PaidReservedFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidReservedFee'] == $_GET['PaidReservedFee']);
            }
            if (isset($_GET['PaidRegistrationCertificate']) && $_GET['PaidRegistrationCertificate'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidRegistrationCertificate'] == $_GET['PaidRegistrationCertificate']);
            }
            if (isset($_GET['BookFeeEmail']) && $_GET['BookFeeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['BookFeeEmail'] == $_GET['BookFeeEmail']);
            }
            if (isset($_GET['PaidBookFee']) && $_GET['PaidBookFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidBookFee'] == $_GET['PaidBookFee']);
            }
            if (isset($_GET['OpeningNoticeEmail']) && $_GET['OpeningNoticeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['OpeningNoticeEmail'] == $_GET['OpeningNoticeEmail']);
            }
            if (isset($_GET['PaidTuitionFee']) && $_GET['PaidTuitionFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidTuitionFee'] == $_GET['PaidTuitionFee']);
            }
            if (isset($_GET['GiveUpOffer']) && $_GET['GiveUpOffer'] !== '') {
                $isValid = $isValid && ($applicationStatus['GiveUpOffer'] == $_GET['GiveUpOffer']);
            }
            if ($isValid) {
                $applicationIDAry[] = $ApplicationID;
            }
        }
        $applicationIdSql = implode("','", $applicationIDAry);
        //// Filter cust info END ////


//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
        $sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			" . getNameFieldByLang2("stu.") . " AS student_name,
     			" . getNameFieldByLang2("pg.") . " AS parent_name,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				stu.BirthCertNo AS student_hkid,
				CASE
     	";
        FOREACH ($admission_cfg['Status'] as $_key => $_status) {//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status, s.DateInput	" . $from_table . " AND o.ApplicationID IN ('{$applicationIdSql}') ORDER BY $sort, FIELD(pg.PG_TYPE, 'F', 'M', 'G') ";

        $applicationAry = $this->returnArray($sql);

        //// Cust field START ////
        for ($i = 0, $iMax = count($applicationAry); $i < $iMax; $i++) {
            $application_id = $applicationAry[$i]['application_id'];
            $applicationAry[$i]['Cust_RatingClass'] = $applicationStatusArr[$application_id]['RatingClass'];
            $applicationAry[$i]['Cust_InterviewStatus'] = $applicationStatusArr[$application_id]['InterviewStatus'];
            $applicationAry[$i]['Cust_AdmitStatus'] = $applicationStatusArr[$application_id]['AdmitStatus'];
        }
        //// Cust field END ////

        return array(count($applicationIDAry), $applicationAry);
    }

    function getInterviewListAry($recordID = '', $date = '', $startTime = '', $endTime = '', $keyword = '', $order = '', $sortby = '', $round = 1)
    {
        global $admission_cfg;

        //$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

        if ($recordID != '') {
            $cond = " AND i.RecordID IN ('" . implode("','", (array)($recordID)) . "') ";
        }
        if ($date != '') {
            $cond .= ' AND i.Date >= \'' . $date . '\' ';
        }
        if ($startTime != '') {
            $cond .= ' AND i.StartTime >= \'' . $startTime . '\' ';
        }
        if ($endTime != '') {
            $cond .= ' AND i.EndTime <= \'' . $endTime . '\' ';
        }
        if ($keyword != '') {
            $search_cond = ' AND (i.Date LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.StartTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.EndTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\')';
        }
        $sort = $sortby ? "$sortby $order" : "application_id";

//		if(!empty($keyword)){
//			$cond .= "
//				AND (
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'
//				)
//			";
//		}

        $from_table = "
			FROM
				ADMISSION_INTERVIEW_SETTING AS i
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID" . ($round > 1 ? $round : '') . "
			INNER JOIN
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			WHERE
				1
			" . $cond . "
			order by " . $sort . "
    	";
        $sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			" . getNameFieldByLang2("stu.") . " AS student_name,
     			" . getNameFieldByLang2("pg.") . " AS parent_name,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE
     	";
        FOREACH ($admission_cfg['Status'] as $_key => $_status) {//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status	" . $from_table . " ";
        //debug_r($sql);
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function sortExportInterviewList($a, $b)
    {
        if ($a[1] != $b[1]) {
            return strcmp($a[1], $b[1]);
        } elseif ($a[2] != $b[2]) {
            return strcmp($a[2], $b[2]);
        } elseif ($a[3] != $b[3]) {
            return strcmp($a[3], $b[3]);
        } elseif ($a[4] != $b[4]) {
            return strcmp($a[4], $b[4]);
        }

        $aApplicationIdArr = explode('-', $a[6]);
        $bApplicationIdArr = explode('-', $b[6]);
        $aCode = str_replace(array('S', 'N', 'U'), array('1', '2', '3'), $aApplicationIdArr[2]);
        $bCode = str_replace(array('S', 'N', 'U'), array('1', '2', '3'), $bApplicationIdArr[2]);
        return strcmp($aCode, $bCode);
    }

    function getExportInterviewResultHeaderData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $selectStatus = '', $selectInterviewStatus, $selectInterviewRound = '', $selectFormQuestionID = '', $selectFormAnswerID = '', $interviewDate = '')
    {
        global $kis_lang;
        list($header, $data) = parent::getExportInterviewResultHeaderData($schoolYearID, $classLevelID, $applicationID, $recordID, $selectStatus, $selectInterviewStatus, $selectInterviewRound, $selectFormQuestionID, $selectFormAnswerID, $interviewDate);

        array_unshift($header[0], '');
        array_unshift($header[0], '');
        array_splice($header[1], 1, 0, $kis_lang['interviewdate']);
        array_splice($header[1], 4, 0, $kis_lang['Admission']['CREATIVE']['applyFor']);

        $applicationIdArr = array_map('trim', Get_Array_By_Key($data, 1));

        $applicationIdSql = implode("','", $applicationIdArr);
        $sql = "SELECT
            o.ApplicationID,
            o.ApplyDayType1,
            o.ApplyDayType2,
            o.ApplyDayType3,
			s.Date as Date,
			s.StartTime as StartTime,
			s.EndTime as EndTime,
			s.GroupName as GroupName
        FROM
            ADMISSION_OTHERS_INFO o
		LEFT JOIN 
			ADMISSION_INTERVIEW_SETTING s
		ON 
			o.InterviewSettingID".($selectInterviewRound == 1?'':$selectInterviewRound)." = s.RecordID
        WHERE
            ApplicationID IN ('{$applicationIdSql}')";
        $rs = $this->returnResultSet($sql);

        $applyDayTypeArr = BuildMultiKeyAssoc($rs, array('ApplicationID'));

        for ($i = 0, $iMax = count($data); $i < $iMax; $i++) {
            $applicationId = $data[$i][1] = trim($data[$i][1]);
            $applyDayTypeStr1 = $kis_lang['Admission']['TimeSlot'][$applyDayTypeArr[$applicationId]['ApplyDayType1']];
            $applyDayTypeStr1 = $applyDayTypeStr1 ? $applyDayTypeStr1 : '--';
            $applyDayTypeStr2 = $kis_lang['Admission']['TimeSlot'][$applyDayTypeArr[$applicationId]['ApplyDayType2']];
            $applyDayTypeStr2 = $applyDayTypeStr2 ? $applyDayTypeStr2 : '--';
            $applyDayTypeStr3 = $kis_lang['Admission']['TimeSlot'][$applyDayTypeArr[$applicationId]['ApplyDayType3']];
            $applyDayTypeStr3 = $applyDayTypeStr3 ? $applyDayTypeStr3 : '--';
            $applyDayTypeStr = "1){$applyDayTypeStr1}";
            $applyDayTypeStr .= " 2){$applyDayTypeStr2}";
            $applyDayTypeStr .= " 3){$applyDayTypeStr3}";
            array_splice($data[$i], 3, 0, $applyDayTypeStr);
            
            $interviewDate = $applyDayTypeArr[$applicationId]['Date']?$applyDayTypeArr[$applicationId]['Date'].' ('.substr($applyDayTypeArr[$applicationId]['StartTime'], 0, -3).' ~ '.substr($applyDayTypeArr[$applicationId]['EndTime'], 0, -3).')'.($applyDayTypeArr[$applicationId]['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$applyDayTypeArr[$applicationId]['GroupName']:''):'';
        	array_splice($data[$i], 1, 0, $interviewDate);
        }

        return array($header, $data);
    }

    function hasApplicationSetting()
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '" . $this->schoolYearID . "' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
        return current($this->returnVector($sql));
    }

    function checkImportDataForImportAdmissionHeader($csv_header, $lang = '')
    {
        //$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
        $file_format = $this->getExportHeader();
        $file_format = $file_format[1];
        # check csv header
        $format_wrong = false;
		
        for ($i = 0; $i < sizeof($file_format); $i++) {
            if ($csv_header[$i] != $file_format[$i]) {
                $format_wrong = true;
                break;
            }
        }

        return $format_wrong;
    }

    public function returnPresetCodeAndNameArr($code_type = "")
    {
        $sql = "select Code, " . Get_Lang_Selection("NameChi", "NameEng") . "  from PRESET_CODE_OPTION where CodeType='" . $code_type . "'";
        $result = $this->returnArray($sql);
        return $result;
    }

    public function getImportRequiredFieldIndexArray()
    {
        $exportHeader = $this->getExportHeader('', '', true);
        $exportHeader = $exportHeader[1];
        $requiredFieldIndex = array();
        for($i=0; $i<count($exportHeader); $i++){
        	$requiredFieldIndex[$i]['FieldName'] = substr($exportHeader[$i], 26);
        	$requiredFieldIndex[$i]['Required'] = (strpos($exportHeader[$i], '*') !== false ? true : false);
        }
        return $requiredFieldIndex;
    }
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$allAdmissionNumber = array();

		$admission_year = getAcademicYearByAcademicYearID($this->getNextSchoolYearID());
		$admission_year_start = substr($admission_year, 0, 4);
		
		//// Get all Birth Cert No START ////
		$sql = "SELECT asi.ApplicationID, asi.BirthCertNo FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID JOIN ADMISSION_APPLICATION_STATUS as aas ON asi.ApplicationID = aas.ApplicationID WHERE aoi.ApplyYear = '".$this->getNextSchoolYearID()."' AND aas.Status = 2";
		$rs = $this->returnResultSet($sql);
		$allBirthCertNo = BuildMultiKeyAssoc($rs, array('ApplicationID') , array('BirthCertNo'), $SingleValue=1);
		//// Get all Birth Cert No END ////
		
		$requiredFieldIndex = $this->getImportRequiredFieldIndexArray();
		
		$i=0;
		foreach($data as $aData){
			
			if($requiredFieldIndex[$i]['Required']){
				if($aData[$i] == ''){
			    	$resultArr[$i]['validEmpty'] = false;
				}else{
				    $resultArr[$i]['validEmpty'] = true;
				}
			}
			
			//valid Admission Date
//			if($aData[0]){
//    			$aData[0] = getDefaultDateFormat($aData[0]);
//    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[0]) ) {
//    				list($year , $month , $day) = explode('-',$aData[0]);
//    		       	$resultArr[$i]['validAdmissionDate'] = checkdate($month , $day , $year);
//    			}
//    			else{
//    				$resultArr[$i]['validAdmissionDate'] = false;
//    			}
//			}else{
//				$resultArr[$i]['validAdmissionDate'] = true;
//			}
			
			//valid Admission number
			$resultArr[$i]['validAdmissionNo'] = !in_array($aData[1], $allAdmissionNumber);
			
			//valid English Name
			if($aData[4] == ''){
			    $resultArr[$i]['validEnglishNameEmpty'] = false;
			}else{
			    $resultArr[$i]['validEnglishNameEmpty'] = true;
			}
			
			//valid Gender
			if($aData[5]){
    			if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
    				$aData[5] = 'M';
    				$data[$i][5] = 'M';
    			}
    			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
    				$aData[5] = 'F';
    				$data[$i][5] = 'F';
    			}
    			if ( strtoupper($aData[5]) == 'M' || strtoupper($aData[5]) == 'F') {
    		       	$resultArr[$i]['validGender'] = true;
    			}
    			else{
    				$resultArr[$i]['validGender'] = false;
    			}
			}else{
			    $resultArr[$i]['validGender'] = true;
			}
			
			//valid Date of birth
			if($aData[6] == ''){
			    $resultArr[$i]['validDateOfBirthEmpty'] = false;
			}else{
			    $resultArr[$i]['validDateOfBirthEmpty'] = true;
    			$aData[6] = getDefaultDateFormat($aData[6]);
    			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[6]) ) {
    				list($year , $month , $day) = explode('-',$aData[6]);
    		       	$resultArr[$i]['validDateOfBirth'] = checkdate($month , $day , $year);
    			}
    			else{
    				$resultArr[$i]['validDateOfBirth'] = false;
    			}
			}
			
			//valid Place of birth
//			if($aData[7] == ''){
//				$resultArr[$i]['validPlaceOfBirthEmpty'] = false;
//			}else{
//			    $resultArr[$i]['validPlaceOfBirthEmpty'] = true;
//			}
			
			//valid birth cert
    		$checkBirthCertType = array(
    		    strtoupper($kis_lang['Admission']['BirthCertType']['hk2'])
    		);
    		if(in_array( strtoupper($aData[7]), $checkBirthCertType )){
    			if (
    			    preg_match('/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/',$aData[8]) &&
    			    $this->checkBirthCertNo($aData[8])
			    ) {
    				$resultArr[$i]['validBirthCertNoType'] = true;
    			}else{
    				$resultArr[$i]['validBirthCertNoType'] = false;
    			}
    			
    			if (
    			    $allBirthCertNo[$aData[1]] != $aData[8] && 
    			    $this->hasBirthCertNumber($aData[8], $_REQUEST['classLevelID'], ($_REQUEST['schoolYearID']?$_REQUEST['schoolYearID']:$this->schoolYearID))
			    ) {
    				$resultArr[$i]['validBirthCertNo'] = false;
    			}else{
    				$resultArr[$i]['validBirthCertNo'] = true;
    			}
    		}else{
				$resultArr[$i]['validBirthCertNoType'] = true;
				$resultArr[$i]['validBirthCertNo'] = true;
    		}
			
			//valid contact email
			if($aData[20]){
    			if (preg_match('/\S+@\S+\.\S+/',$aData[20])) {
    				$resultArr[$i]['validEmail'] = true;
    			}
    			else{
    				$resultArr[$i]['validEmail'] = false;
    			}
			}else{
			    $resultArr[$i]['validEmail'] = true;
			}

			$resultArr[$i]['validData'] = $aData[1];
			$i++;
		} // End foreach
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
//			if(!$aResult['validAdmissionDate']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidAdmissionDate'];
//			}
			if(!$aResult['validAdmissionNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidApplicationNo'];
			}
			else if(isset($aResult['validEmpty']) && !$aResult['validEmpty']){
				$hasError = true;
				$errorMag = $requiredFieldIndex[$i]['FieldName'].$kis_lang['Admission']['msg']['importEmpty'];
			}
			else if(!$aResult['validEnglishNameEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyEnglishName'];
			}
			else if(!$aResult['validDateOfBirthEmpty']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importEmptyDateOfBirth'];
			}
			else if(!$aResult['validDateOfBirth']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidDateOfBirth'];
			}
			else if(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidWordOfGender'];
			}
			else if(!$aResult['validBirthCertNoType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidFormatOfBirthCertNo'];
			}
			else if(!$aResult['validBirthCertNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidBirthCertNo'];
			}
			else if(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidContactEmail'] ;
			}
			
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.($i+2).'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	
//    function checkImportDataForImportAdmission($data)
//    {
//        global $kis_lang, $admission_cfg;
//        $resultArr = array();
//        $i = 0;
//
//        #### Get duplcate application id START ####
//        $ids = array();
//        foreach ($data as $aData) {
//            $ids[] = trim($aData[0]);
//        }
//        $ids = array_filter($ids);
//
//        $idSql = implode("','", $ids);
//        $sql = "SELECT
//		    ASI.ApplicationID,
//		    ASI.BirthCertNo
//		FROM
//		    ADMISSION_STU_INFO ASI
//		INNER JOIN
//		    ADMISSION_OTHERS_INFO AOI
//	    ON
//	        ASI.ApplicationID = AOI.ApplicationID
//        AND
//            AOI.ApplyYear = '" . $this->getNextSchoolYearID() . "'
//		WHERE
//		    ASI.ApplicationID IN ('{$idSql}')
//		";
//        $rs = $this->returnResultSet($sql);
//
//        $currentApplicationIdArr = Get_Array_By_Key($rs, 'ApplicationID');
//        $applicationIdBirthCertMapping = BuildMultiKeyAssoc($rs, array('ApplicationID'), array('BirthCertNo'), $SingleValue = 1);
//        #### Get duplcate application id END ####
//
//        foreach ($data as $aData) {
//            //valid ApplicationID
//            if ($aData[0]) {
//                $resultArr[$i]['validApplicationID'] = in_array($aData[0], $currentApplicationIdArr);
//            } else {
//                $resultArr[$i]['validApplicationID'] = true;
//            }
//
//            //valid E.Name
//            $aData[1] = str_replace(',', '&#44;', $aData[1]);
//            $resultArr[$i]['validEName'] = true;
//            if ($aData[1] == '') {
//                $resultArr[$i]['validEName'] = false;
//            }
//
//            //valid C.Name
//            $aData[2] = str_replace(',', '&#44;', $aData[2]);
//            $resultArr[$i]['validCName'] = true;
//            if ($aData[2] == '') {
//                $resultArr[$i]['validCName'] = false;
//            }
//
//            //valid Birth Date
//            $aData[3] = getDefaultDateFormat($aData[3]);
//
//            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[3])) {
//                list($year, $month, $day) = explode('-', $aData[3]);
//                $resultArr[$i]['validBirthDate'] = checkdate($month, $day, $year);
//            } else {
//                $resultArr[$i]['validBirthDate'] = false;
//            }
//
//            //valid Gender
//            if (strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['M'])) {
//                $aData[4] = 'M';
//                $data[$i][4] = 'M';
//            } else if (strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['F'])) {
//                $aData[4] = 'F';
//                $data[$i][4] = 'F';
//            }
//            if (strtoupper($aData[4]) == 'M' || strtoupper($aData[4]) == 'F') {
//
//                $resultArr[$i]['validGender'] = true;
//            } else {
//                $resultArr[$i]['validGender'] = false;
//            }
//
//            //valid birth cert
//            $aData[5] = str_replace(array('(', ')'), array('', ''), $aData[5]);
//            $resultArr[$i]['validBirthCertNoFormat'] = true;
//            if (!preg_match('/^[a-zA-Z][0-9A]{7}$/', $aData[5])) {
//                $resultArr[$i]['validBirthCertNoFormat'] = false;
//            }
//
//            $resultArr[$i]['validBirthCertNoDuplicate'] = true;
//            if (
//                $aData[5] != $applicationIdBirthCertMapping[$aData[0]] &&
//                $this->hasBirthCertNumber($aData[5], '')
//            ) {
//                $resultArr[$i]['validBirthCertNoDuplicate'] = false;
//            }
//
//            //valid STRN
//            $resultArr[$i]['validSTRN'] = true;
//            /*if($aData[6] == ''){
//                $resultArr[$i]['validSTRN'] = false;
//            }*/
//
//            //valid Current School
//            $resultArr[$i]['validPrimarySchool'] = true;
//            if ($aData[7] == '') {
//                $resultArr[$i]['validPrimarySchool'] = false;
//            }
//
//            //valid Parent/Guardian
//            $resultArr[$i]['validParentGuardian'] = true;
//            /*if($aData[8] == ''){
//                $resultArr[$i]['validParentGuardian'] = false;
//            }*/
//
//            //valid email address
//            $resultArr[$i]['validEmail'] = true;
//            if ($aData[9] && !preg_match('/\S+@\S+\.\S+/', $aData[9])) {
//                $resultArr[$i]['validEmail'] = false;
//            }
//
//            $resultArr[$i]['validData'] = $aData[1];
//            $i++;
//        }
//        $result = $resultArr;
//
//        //for printing the error message
//        $errCount = 0;
//
//        $x .= '<table class="common_table_list"><tbody><tr class="step2">
//					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
//					<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['englishname'] . '</th>
//					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
//				</tr>';
//        $i = 1;
//        foreach ($result as $aResult) {
//            //developing
//            $hasError = false;
//            $errorMag = '';
//            if (!$aResult['validApplicationID']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
//            } elseif (!$aResult['validEName']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['msg']['enterenglishname'];
//            } elseif (!$aResult['validCName']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['msg']['enterchinesename'];
//            } elseif (!$aResult['validBirthDate']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidDateOfBirth'];
//            } elseif (!$aResult['validGender']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidWordOfGender'];
//            } elseif (!$aResult['validBirthCertNoFormat']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertNo'];
//            } elseif (!$aResult['validBirthCertNoDuplicate']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['duplicateHKID'];
//            } elseif (!$aResult['validSTRN']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterSTRN'];
//            } elseif (!$aResult['validPrimarySchool']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterCurrentSchool'];
//            } elseif (!$aResult['validParentGuardian']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterParentName'];
//            } elseif (!$aResult['validEmail']) {
//                $hasError = true;
//                $errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress'];
//            }
//
//            //print the error msg to the client
//            if ($hasError) {
//                $errCount++;
//                $x .= '<tr class="step2">
//					<td>' . $i . '</td>
//					<td>' . $aResult['validData'] . '</td>
//					<td><font color="red">';
//                $x .= $errorMag;
//                $x .= '</font></td></tr>';
//            }
//
//            $i++;
//        }
//        $x .= '</tbody></table>';
//        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
//    }

    function importDataForImportAdmission($data)
    {
        global $kis_lang, $admission_cfg, $classLevelID;
        
//        ## Dynamic fields START ##
//        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
//        $groups = $dafs->getLastActiveAcademicYearGroups();
//
//        $fields = array();
//        $data = array();
//        foreach ($groups as $index => $group) {
//            $_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
//            foreach ($_fields as $field) {
//                if ($field['Type'] != DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) {
//                    $fields[$group['GroupID']][] = $field;
//                }
//            }
////            $data[$group['GroupID']] = $dafs->getApplicationDataByGroupId($group['GroupID'], $applicationID);
//        }
//        ## Dynamic fields END ##
        //debug_pr($fields);die();
        //// Get all ApplicationID START ////
		$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO";
		$allApplicationID = $this->returnVector($sql);
		//// Get all ApplicationID END ////

		$allBirthCertType = array(
		    strtoupper($kis_lang['Admission']['BirthCertType']['hk2']) => $admission_cfg['BirthCertType']['hk2'], 
		    strtoupper($kis_lang['Admission']['BirthCertType']['others2']) => $admission_cfg['BirthCertType']['others2']
		);
		
		$resultArr = array();
		array_shift($data);
		array_shift($data);
		foreach($data as $aData){
			global $UserID;
			
			$isUpdate = (in_array(trim($aData[1]), $allApplicationID));
			
			//--- convert the text to key code [start]
		    if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
			}
			
			// Birth cert type for non-other-type
			$_key = strtoupper($aData[7]);
			if(in_array( $_key, array_keys($allBirthCertType) )){
			    $birthCertType = $allBirthCertType[$_key];
			    $birthCertTypeOther = '';
			}else{
			    $birthCertType = $admission_cfg['BirthCertType']['others'];
			    $birthCertTypeOther = $aData[7];
			}
			
			// Twins
			if(strtoupper($aData[23]) == strtoupper($kis_lang['Admission']['yes'])){
			    $aData[23] = 'Y';
			}
			else if(strtoupper($aData[23]) == strtoupper($kis_lang['Admission']['no'])){
			    $aData[23] = 'N';
			}
			
			if(strtoupper($aData[47]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[47] = 'M';
			}
			else if(strtoupper($aData[47]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[47] = 'F';
			}
			
			if(strtoupper($aData[51]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[51] = 'M';
			}
			else if(strtoupper($aData[51]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[51] = 'F';
			}
			
		    $result = array();

		    if($isUpdate){
		        $sql = "DELETE FROM ADMISSION_CUST_INFO WHERE ApplicationID = '{$aData[1]}'";
		        $result[] = $this->db_db_query($sql);
		    }else{
		    	if(!$aData[1]){
		    		$aData[1] = $this->newApplicationNumber2($_REQUEST['schoolYearID'], array('isImport'=>true));
		    	}
		        $result[] = $this->insertApplicationStatus($this, $aData[1],$_REQUEST['schoolYearID']);
		        $sql = "INSERT INTO ADMISSION_STU_INFO (ApplicationID) VALUES ('{$aData[1]}')";
		        $result[] = $this->db_db_query($sql);
		    }
		    
		    $sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				SET
	     			stu.ChineseName = '".$aData[3]."',
	      			stu.EnglishName = '".$aData[4]."', 
	      			stu.DOB = '".getDefaultDateFormat($aData[6])." 00:00:00',	
	      			stu.Gender = '".$aData[5]."',
					stu.BirthCertType = '{$birthCertType}' ,
					stu.BirthCertTypeOther = '{$birthCertTypeOther}' ,
	      			stu.BirthCertNo = '".$aData[8]."' ,
					stu.Nationality = '".$aData[9]."' , 
					stu.PlaceOfBirth = '".$aData[10]."',
					stu.Province = '".$aData[11]."',	
      			    stu.LangSpokenAtHome = '".$aData[12]."',
                    stu.LangSpokenOther = '".$aData[13]."',
                    stu.ReligionOther = '".$aData[14]."',
                    stu.Church = '".$aData[15]."',
                    stu.AddressChi = '".$aData[16]."',
                    stu.Address = '".$aData[17]."',
                    stu.HomeTelNo = '".$aData[18]."',
					stu.Fax = '".$aData[19]."',
                    stu.Email = '".$aData[20]."',
					stu.ContactPerson = '".$aData[21]."',
					stu.ContactPersonRelationship = '".$aData[22]."',
                    stu.IsTwinsApplied = '".$aData[23]."',
                    stu.TwinsApplicationID = '".$aData[24]."',
					stu.DateModified = NOW(),
				    stu.ModifiedBy = '".$UserID."'
				WHERE 
					stu.ApplicationID = '".$aData[1]."'
	    	";
		    
		    $result[] = $this->db_db_query($sql);
		    ######## Student Cust START ########
		    $data = array();
		    
		    $data['NameOfSchool'] = $aData[25];
		    $data['Year'] = $aData[26];
		    $data['Class '] = $aData[27];
		    $data['Remarks'] = $aData[54];
		    
		    $result[] = $this->insertApplicationStudentInfoCust($data, $aData[1], $isUpdate=1);
		    ######## Student Cust END ########
		    
		    
		    ######## Parent START ########
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[28],
		        'EnglishName' => $aData[29],
		        'Company' => $aData[30],
		        'OfficeAddress' => $aData[31],
		        'OfficeTelNo' => $aData[32],
		        'Mobile' => $aData[33],
		        'PG_TYPE' => 'F'
		    ), $isUpdate);
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[34],
		        'EnglishName' => $aData[35],
		        'Company' => $aData[36],
		        'OfficeAddress' => $aData[37],
		        'OfficeTelNo' => $aData[38],
		        'Mobile' => $aData[39],
		        'PG_TYPE' => 'M'
		    ), $isUpdate);
		    $result[] = $this->insertApplicationParentInfo(array(
		        'ApplicationID' => $aData[1],
		        'ChineseName' => $aData[40],
		        'EnglishName' => $aData[41],
		        'Company' => $aData[42],
		        'OfficeAddress' => $aData[43],
		        'OfficeTelNo' => $aData[44],
		        'Mobile' => $aData[45],
		        'PG_TYPE' => 'G'
		    ), $isUpdate);
		    ######## Parent END ########
		    
		    
		    ######## Siblings START ########
		    $data = array();
            $data['OthersRelativeStudiedNameChi1'] = $aData[46];
            $data['OthersRelativeStudiedNameGender1'] = $aData[47];
            $data['OthersRelativeGraduationYear1'] = $aData[48];
            $data['OthersRelativeClass1'] = $aData[49];
            
            $data['OthersRelativeStudiedNameChi2'] = $aData[50];
            $data['OthersRelativeStudiedNameGender2'] = $aData[51];
            $data['OthersRelativeGraduationYear2'] = $aData[52];
            $data['OthersRelativeClass2'] = $aData[53];

            $result[] = $this->insertApplicationRelativesInfoCust($data, $aData[1], $isUpdate);
		    ######## Siblings END ########
            
            ######## Update School Year Record START ########
            $data = array();
            $data['sus_status'] = $_REQUEST['classLevelID']; // ApplyLevel
            $data['schoolYearID'] = $_REQUEST['schoolYearID'];
            $data['token'] = '';
            $result[] = $this->insertApplicationOthersInfo($data, $aData[1]);
            ######## Update School Year Record END ########
		    
		    $sql = "
				UPDATE 
					ADMISSION_APPLICATION_STATUS
		    	SET
	     			FeeBankName = '".$aData[82]."',
	     			FeeChequeNo = '".$aData[83]."',
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE 
					ApplicationID = '".$aData[1]."'
		   	";
		   
		    $result[] = $this->db_db_query($sql);
		     
			$resultArr[] = !in_array(false, $result);
			//debug_pr($aData);
		}
		return $resultArr;
    }
    
    function insertApplicationParentInfo($Data, $isUpdate=0){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
		
		if($isUpdate){
			foreach($Data as $_fieldname => $_fieldvalue){
				if($_fieldname == "ApplicationID"){
					$ApplicationID = $_fieldvalue;
				}
				else if($_fieldname == "PG_TYPE"){
					$PG_TYPE = $_fieldvalue;
				}
				else{
					$updateStr .= $_fieldname." = '".$_fieldvalue."',";
				}
			}
			//Henry
			$sql = "UPDATE 
					ADMISSION_PG_INFO 
				SET
					".$updateStr."
				    DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'
				AND
					PG_TYPE = '".$PG_TYPE."'";
					
			if(($result = $this->db_db_query($sql)) && $this->db_affected_rows() == 0){
				$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
			}
			else{
				return $result;
			}
		}
		else{	
			$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
		}
		return $this->db_db_query($sql);
    }
    
    function insertApplicationOthersInfo($Data, $ApplicationID, $isUpdate=0){ 
    	global $admission_cfg, $sys_custom;
   		extract($Data);
   		$success = array();
   		#Check exist application
   		if($ApplicationID == ""){
   			return false;
   		}
    	
    	if(!$this->isInternalUse($_REQUEST['token']) && $token=='' && !$sys_custom['KIS_Admission']['IntegratedCentralServer']){
    		$token = 1;
    	}
   		
		$sql = "UPDATE 
				ADMISSION_OTHERS_INFO 
			SET
				 ApplyYear = '".($schoolYearID?$schoolYearID:$this->schoolYearID)."',
				 ApplyLevel = '".$sus_status."',
				 Token = '".$token."',
			     DateInput = NOW(),
				 HTTP_USER_AGENT = '".addslashes($_SERVER['HTTP_USER_AGENT'])."'
			WHERE
				ApplicationID = '".$ApplicationID."'";
				
		if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
		
   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
						ApplicationID,
					     ApplyYear,
					     ApplyLevel,
						 Token,
					     DateInput,
   						 HTTP_USER_AGENT
					     )
					VALUES (
						'".$ApplicationID."',
						'".($schoolYearID?$schoolYearID:$this->schoolYearID)."',
						'".$sus_status."',
						'".$token."',
						now(),
						'".addslashes($_SERVER['HTTP_USER_AGENT'])."')
			";
			$success[] = $this->db_db_query($sql);
		}			
			
		return !in_array(false, $success);
    }
    
    function insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			if($isUpdate){
   				//Henry
   				$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE ApplicationID = '".$ApplicationID."'";
   				$success[] = $this->db_db_query($sql);
   			}
			if(
			    $OthersRelativeStudiedNameChi1 != '' || 
			    $OthersRelativeStudiedNameEng1 !='' || 
			    $OthersRelativeClassPosition1 !='' || 
			    $OthersRelativeGraduationYear1 != ''
		    ){
				$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,   
							EnglishName,   
							ClassPosition,
							Year,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeStudiedNameChi1."',
							'".$OthersRelativeStudiedNameEng1."',
							'".$OthersRelativeClassPosition1."',
							'".$OthersRelativeGraduationYear1."',
							'1',
							now())
				";
				$success[] = $this->db_db_query($sql);
   			}
			if(
			    $OthersRelativeStudiedNameChi2 != '' || 
			    $OthersRelativeStudiedNameEng2 !='' || 
			    $OthersRelativeClassPosition2 !='' || 
			    $OthersRelativeGraduationYear2 != ''
		    ){
				$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,   
							EnglishName,   
							ClassPosition,
							Year,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeStudiedNameChi2."',
							'".$OthersRelativeStudiedNameEng2."',
							'".$OthersRelativeClassPosition2."',
							'".$OthersRelativeGraduationYear2."',
							'2',
							now())
				";
				$success[] = $this->db_db_query($sql);
   			}
   			
   			if(
   				$OthersRelativeNameChi1 != ''||
   				$OthersRelativeNameEng1 != ''||
   				$OthersRelativeBirth1 !=''||
   				$OthersRelativeTwin1 !=''||
   				$OthersRelativeSchool1 !=''||
   				$OthersRelativeGrade1 !=''
   			){
   				$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,
							EnglishName,
							DOB,
   							IsTwins,
							SchoolName,
   							ClassPosition,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeNameChi1."',
							'".$OthersRelativeNameEng1."',
							'".$OthersRelativeBirth1."',
							'".$OthersRelativeTwin1."',
							'".$OthersRelativeSchool1."',
							'".$OthersRelativeGrade1."',
							'3',
							now())
				";
   				$success[] = $this->db_db_query($sql);
   			}
   			
   			if(
   					$OthersRelativeNameChi2 != ''||
   					$OthersRelativeNameEng2 != ''||
   					$OthersRelativeBirth2 !=''||
   					$OthersRelativeTwin2 !=''||
   					$OthersRelativeSchool2 !=''||
   					$OthersRelativeGrade2 !=''
   					){
   						$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
							ApplicationID,
							Name,
							EnglishName,
							DOB,
   							IsTwins,
							SchoolName,
   							ClassPosition,
							Type,
							DateInput)
						VALUES (
							'".$ApplicationID."',
							'".$OthersRelativeNameChi2."',
							'".$OthersRelativeNameEng2."',
							'".$OthersRelativeBirth2."',
							'".$OthersRelativeTwin2."',
							'".$OthersRelativeSchool2."',
							'".$OthersRelativeGrade2."',
							'4',
							now())
				";
   				$success[] = $this->db_db_query($sql);
   			}
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function insertApplicationStudentInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			
   			######## Update Old School Record START ########
   			$success = array();
   			if($isUpdate){
   				//Henry
   				$sql = "DELETE FROM ADMISSION_STU_PREV_SCHOOL_INFO WHERE ApplicationID = '".$ApplicationID."'";
   				$success[] = $this->db_db_query($sql);
   				
   				$sql = "DELETE FROM 
   				    ADMISSION_CUST_INFO 
			    WHERE 
   				    ApplicationID = '".$ApplicationID."' 
		        AND 
			        Code in (
   				        'Remarks'
			        )
		        ";
   				$success[] = $this->db_db_query($sql);
   			}
   			
   			if($NameOfSchool){
       			$sql = "INSERT INTO ADMISSION_STU_PREV_SCHOOL_INFO (
    						ApplicationID,
    						NameOfSchool,
    						Class,
       			            Year,
    						DateInput
       					) VALUES (
    						'".$ApplicationID."',
       						'{$NameOfSchool}',
       						'{$Class}',
       						'{$Year}',
       						now()
       					)";
       			$success[] = $this->db_db_query($sql);
   			}
   			######## Update Old School Record END ########

   			######## Student Cust START ########
   			$success[] = $this->insertApplicationCustInfo(array(
   				'Code' => 'Remarks',
   				'Value' => $Remarks
   			), $ApplicationID);
   			######## Student Cust END ########
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }

	/**
     * Portal - import remarks
     */
    public function getImportRemarks()
    {
        $exportHeader = $this->getExportHeader('', '', true);
        return $exportHeader[1];
    }
	
    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '', $classLevelID = '', $forRemark = false)
    {
        global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;

        $headerArray = array();

        #### Get data START ####
        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        $groups = $dafs->getLastActiveAcademicYearGroups();

        $fields = array();
        foreach ($groups as $index => $group) {
            $_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
            $fields[$group['GroupID']] = array();

            foreach ($_fields as $field) {
                if ($field['Type'] != DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) {
                    $fields[$group['GroupID']][] = $field;
                }
            }
        }
        #### Get data END ####

        #### First row START ####
        $column = array();
        $column[] = "";
        $column[] = "";
        $column[] = "";

        foreach ($groups as $group) {
            $column[] = Get_Lang_Selection($group['TitleB5'], $group['TitleEn']);
            for ($i = 0, $iMax = count($fields[$group['GroupID']]) - 1; $i < $iMax; $i++) {
            	if($fields[$group['GroupID']][$i]['Type'] == DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE){
            		$column[] = "";
            	}
                $column[] = "";
            }
        }
//        $column[] = $kis_lang['creative_interview_follow_up'];
//        for ($i = 0; $i < 10; $i++) {
//            $column[] = "";
//        }
        $column[] = $kis_lang['remarks'];
        $headerArray[] = $column;
        #### First row END ####

        #### Second row START ####
        $column = array();
        $column[] = $kis_lang['Admission']['admissiondate'];
        $column[] = $kis_lang['applicationno'];
        $column[] = $kis_lang['Admission']['applyLevel'];

        foreach ($groups as $group) {
            foreach ($fields[$group['GroupID']] as $field) {
            	$star = '';
            	if($forRemark && in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $field['ValidationArr'])){
            		$star ='<font color="red">*</font>';
            	}
            	switch ($field['Type']) {
            		case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE:
                        $column[] = $star.$kis_lang['Admission']['birthcerttype'];
                        break;
            	}
                $column[] = $star.Get_Lang_Selection($field['TitleB5'], $field['TitleEn']);
            }
        }

        ## Interview follow up START ##
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'];
        ## Interview follow up END ##

        ## Remarks START ##
        $column[] = $kis_lang['Admission']['applicationstatus'];
        $column[] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
        $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['receiptcode'] . ")";
        $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['date'] . ")";
        $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['handler'] . ")";
	    $column[] = $kis_lang['Admission']['interviewdate'] . " (1)";
	    $column[] = $kis_lang['Admission']['interviewdate'] . " (2)";
	    $column[] = $kis_lang['Admission']['interviewdate'] . " (3)";
        $column[] = $kis_lang['Admission']['isnotified'];
        $column[] = $kis_lang['Admission']['otherremarks'];
        ## Remarks END ##

        $headerArray[] = $column;
        #### Second row END ####

        return $headerArray;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        #### Get data START ####
        ## ApplicationID START ##
        if (!$applicationID) {
            $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
            $applicationID = $otherInfo['ApplicationID'];
        }
        ## ApplicationID END ##

        $allCustInfo = $this->getAllApplicationCustInfo($applicationID);
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
        $classLevel = $this->getClassLevel();

        ## Dynamic fields START ##
        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        $groups = $dafs->getLastActiveAcademicYearGroups();

        $fields = array();
        $data = array();
        foreach ($groups as $index => $group) {
            $_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
            foreach ($_fields as $field) {
                if ($field['Type'] != DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) {
                    $fields[$group['GroupID']][] = $field;
                }
            }
            $data[$group['GroupID']] = $dafs->getApplicationDataByGroupId($group['GroupID'], $applicationID);
        }
        ## Dynamic fields END ##
        #### Get data END ####

        #### Pack data START ####
        $row = array();

        $row[] = substr($otherInfo['DateInput'], 0, -9);
        $row[] = $otherInfo['applicationID'];
        $row[] = $classLevel[$otherInfo['classLevelID']];

        foreach ($fields as $groupId => $d1) {
            foreach ($d1 as $field) {
                $dbTableName = $field['OtherAttributeArr']['DbTableName'];
                $_dbFieldName = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
                $dbFieldName = $_dbFieldName[0];
                $value = '';
                if (!isset($data[$groupId][$dbTableName][$dbFieldName])) {
                    $sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
                    $sqlWhereField = $sqlWhere[0];
                    $sqlWhereValue = trim($sqlWhere[1], " '");

                    foreach ((array)$data[$groupId][$dbTableName] as $v) {
                        if (isset($v[$sqlWhereField]) && $v[$sqlWhereField] == $sqlWhereValue) {
                            $value = $v[$dbFieldName];
                            break;
                        }
                    }
                } else {
                    $value = $data[$groupId][$dbTableName][$dbFieldName];
                }

                switch ($field['Type']) {
                    case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR:
                        $value = "1){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType1']]}";
                        if ($data[$groupId][$dbTableName]['ApplyDayType2']) {
                            $value .= "\n2){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType2']]}";
                        } else {
                            $value .= "\n2) --";
                        }
                        if ($data[$groupId][$dbTableName]['ApplyDayType3']) {
                            $value .= "\n3){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType3']]}";
                        } else {
                            $value .= "\n3) --";
                        }
                        break;
                    case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_RADIO:
                        foreach ($field['ExtraArr']['options'] as $optionInfo) {
                            if ($value == $optionInfo[0]) {
                                $value = Get_Lang_Selection($optionInfo[1], $optionInfo[2]);
                                break;
                            }
                        }
                        break;
                    case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_HKID_WITH_TYPE:
                        $row[] = ($data[$groupId][$dbTableName]['BirthCertType'] == $admission_cfg['BirthCertType']['hk2']?$kis_lang['Admission']['BirthCertType']['hk2']:$kis_lang['Admission']['BirthCertType']['others2']);
                        break;
                }
                $row[] = $value;
            }
        }

	    // ######## Get interview START ########
	    $interviewInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
	    $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '');
	    $interviewSettings = BuildMultiKeyAssoc($rs, 'RecordID');

	    $interview1 = $interviewSettings[$interviewInfo['InterviewSettingID']];
	    $interview2 = $interviewSettings[$interviewInfo['InterviewSettingID2']];
	    $interview3 = $interviewSettings[$interviewInfo['InterviewSettingID3']];

	    $interviewDateTime1 = $interviewDateTime2 = $interviewDateTime3 = '';
	    if($interview1) {
		    $interviewDateTime1 = $interview1['Date'];
		    $interviewDateTime1 .= ' ' . substr( $interview1['StartTime'], 0, 5 );
		    $interviewDateTime1 .= ' - ' . substr( $interview1['EndTime'], 0, 5 );
	    }
	    if($interview2) {
		    $interviewDateTime2 = $interview2['Date'];
		    $interviewDateTime2 .= ' ' . substr( $interview2['StartTime'], 0, 5 );
		    $interviewDateTime2 .= ' - ' . substr( $interview2['EndTime'], 0, 5 );
	    }
	    if($interview3) {
		    $interviewDateTime3 = $interview3['Date'];
		    $interviewDateTime3 .= ' ' . substr( $interview3['StartTime'], 0, 5 );
		    $interviewDateTime3 .= ' - ' . substr( $interview3['EndTime'], 0, 5 );
	    }
	    // ######## Get interview END ########

        ## Interview follow up START ##
//        $interviewStatusArr = array_flip($admission_cfg['InterviewStatus']);
//        $interviewStatus = $allCustInfo['InterviewStatus'][0]['Value'];
//        $interviewStatus = ($interviewStatus) ? $interviewStatus : $admission_cfg['InterviewStatus']['NotUpdated'];
//        $interviewStatus = $interviewStatusArr[$interviewStatus];
//        $row[] = $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$interviewStatus];

//        if ($allCustInfo['RegisterEmail'][0]['Value']) {
//            $row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['RegisterEmailDate'][0]['Value']})";
//        } else {
//            $row[] = $kis_lang['Admission']['no'];
//        }
//
//        $admitStatusArr = array_flip($admission_cfg['AdmitStatus']);
//        $admitStatus = $allCustInfo['AdmitStatus'][0]['Value'];
//        $admitStatus = ($admitStatus) ? $admitStatus : $admission_cfg['AdmitStatus']['NotUpdated'];
//        $admitStatus = $admitStatusArr[$admitStatus];
//        $row[] = $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$admitStatus];

        $row[] = $kis_lang['Admission']['TimeSlot'][$allCustInfo['RatingClass'][0]['Value']];
//        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidReservedFee'][0]['Value']];
//        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidRegistrationCertificate'][0]['Value']];

//        if ($allCustInfo['BookFeeEmail'][0]['Value']) {
//            $row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['BookFeeEmailDate'][0]['Value']})";
//        } else {
//            $row[] = $kis_lang['Admission']['no'];
//        }
//
//        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidBookFee'][0]['Value']];
//
//        if ($allCustInfo['OpeningNoticeEmail'][0]['Value']) {
//            $row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['OpeningNoticeEmailDate'][0]['Value']})";
//        } else {
//            $row[] = $kis_lang['Admission']['no'];
//        }
//
//        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidTuitionFee'][0]['Value']];
//        $row[] = ($allCustInfo['GiveUpOffer'][0]['Value']) ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
        ## Interview follow up END ##

        ## Remarks START ##
        $row[] = $kis_lang['Admission']['Status'][$status['status']];
        $row[] = $kis_lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
        $row[] = $status['receiptID'];
        $row[] = $status['receiptdate'];
        $row[] = $status['handler'];
//        $row[] = $status['interviewdate'];
//        $row[] = $status['interviewlocation'];
	    $row[] = $interviewDateTime1;
	    $row[] = $interviewDateTime2;
	    $row[] = $interviewDateTime3;
	    $row[] = $kis_lang['Admission'][ $status['isnotified'] ];
	    $row[] = $status['remark'];
        ## Remarks END ##
        #### Pack data END ####

        return $row;
    }

    public function filterExportApplicantList($rows)
    {
        $applicationIdArr = Get_Array_By_Key($rows, 1);
        $applicationIdSql = implode("','", $applicationIdArr);

        //// Filter cust info START ////
        $sql = "SELECT
                ApplicationID,
                Code,
                Value
            FROM
                ADMISSION_CUST_INFO aci
            WHERE
                ApplicationID IN ('{$applicationIdSql}')
            ";
        $rs = $this->returnResultSet($sql);
        $applicationStatusArr = BuildMultiKeyAssoc($rs, array('ApplicationID', 'Code'), array('Value'), true);

        $filterApplicationIDAry = array();
        foreach ($applicationStatusArr as $ApplicationID => $applicationStatus) {
            $isValid = true;

            if (isset($_POST['applicantInterviewStatus']) && $_POST['applicantInterviewStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['InterviewStatus'] == $_POST['applicantInterviewStatus']);
            }
            if (isset($_POST['RegisterEmail']) && $_POST['RegisterEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['RegisterEmail'] == $_POST['RegisterEmail']);
            }
            if (isset($_POST['AdmitStatus']) && $_POST['AdmitStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['AdmitStatus'] == $_POST['AdmitStatus']);
            }
            if (isset($_POST['RatingClass']) && $_POST['RatingClass'] !== '') {
                $isValid = $isValid && ($applicationStatus['RatingClass'] == $_POST['RatingClass']);
            }
            if (isset($_POST['PaidReservedFee']) && $_POST['PaidReservedFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidReservedFee'] == $_POST['PaidReservedFee']);
            }
            if (isset($_POST['PaidRegistrationCertificate']) && $_POST['PaidRegistrationCertificate'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidRegistrationCertificate'] == $_POST['PaidRegistrationCertificate']);
            }
            if (isset($_POST['BookFeeEmail']) && $_POST['BookFeeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['BookFeeEmail'] == $_POST['BookFeeEmail']);
            }
            if (isset($_POST['PaidBookFee']) && $_POST['PaidBookFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidBookFee'] == $_POST['PaidBookFee']);
            }
            if (isset($_POST['OpeningNoticeEmail']) && $_POST['OpeningNoticeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['OpeningNoticeEmail'] == $_POST['OpeningNoticeEmail']);
            }
            if (isset($_POST['PaidTuitionFee']) && $_POST['PaidTuitionFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidTuitionFee'] == $_POST['PaidTuitionFee']);
            }
            if (isset($_POST['GiveUpOffer']) && $_POST['GiveUpOffer'] !== '') {
                $isValid = $isValid && ($applicationStatus['GiveUpOffer'] == $_POST['GiveUpOffer']);
            }
            if ($isValid) {
                $filterApplicationIDAry[] = $ApplicationID;
            }
        }
        //// Filter cust info END ////

        //// Set result START ////
        $resultRow = array();
        foreach ($rows as $row){
            if(in_array($row[1], $filterApplicationIDAry)){
                $resultRow[] = $row;
            }
        }
        //// Set result END ////

        return $resultRow;
    }

    /**
     * Portal - export applicant details header
     */
    public function getExportHeaderEDB($schoolYearID = '', $classLevelID = '')
    {
        global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;

        $exportColumn = array();

        $exportColumn[0][] = 'Application No.';
        $exportColumn[0][] = 'E.Name';
        $exportColumn[0][] = 'C.Name';
        $exportColumn[0][] = 'Birth Date';
        $exportColumn[0][] = 'Sex';
        $exportColumn[0][] = 'ID No.';
        $exportColumn[0][] = 'STRN';
        $exportColumn[0][] = 'Primary School';
// 		$exportColumn[0][] = 'Name of Parent/Guardian';
// 		$exportColumn[0][] = 'Email';

        return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportDataEDB($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        $studentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $studentInfo['applicationID'], '');
        $otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID, $classLevelID, $applicationID, $recordID);
        $custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);

        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));

        $birthcertno = $studentInfo['birthcertno'];
        $birthcertno = substr_replace($birthcertno, '(', strlen($birthcertno) - 1, 0) . ')'; // Add ( ) for the hkid

        $ExportArr = array();

        $ExportArr[] = $studentInfo['applicationID'];
        $ExportArr[] = $studentInfo['student_name_en'];
        $ExportArr[] = $studentInfo['student_name_b5'];
        $ExportArr[] = $studentInfo['dateofbirth'] > 0 ? date("j/n/Y", strtotime($studentInfo['dateofbirth'])) : '';
        $ExportArr[] = $studentInfo['gender'];
        $ExportArr[] = $studentInfo['birthcertno'] ? $birthcertno : '';
        $ExportArr[] = $custInfo['BD_Ref_Num'][0]['Value'];
        $ExportArr[] = $otherInfo2[0]['OthersPrevSchName'];
// 		$ExportArr[] = $parentInfo[0]['EnglishName'];
// 		$ExportArr[] = $parentInfo[0]['Email'];

        return $ExportArr;
    }

//    function getExportDataForImportAccount($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $tabID = '')
//    {
//        global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
//
//        $studentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
//        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
//        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
//        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
//        $custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);
//
//        $dataArray = array();
//        if ($tabID == 2) {
//            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
//            $studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
//            $dataArray[0] = array();
//            $dataArray[0][] = ''; //UserLogin
//            $dataArray[0][] = ''; //Password
//            $dataArray[0][] = ''; //UserEmail
//            $dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
//            $dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
//            $dataArray[0][] = ''; //NickName
//            $dataArray[0][] = $studentInfo['gender']; //Gender
//            $dataArray[0][] = ''; //Mobile
//            $dataArray[0][] = ''; //Fax
//            $dataArray[0][] = ''; //Barcode
//            $dataArray[0][] = ''; //Remarks
//            $dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
//            $dataArray[0][] = (is_numeric($studentInfo['homeaddress']) ? $Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']] : $studentInfo['homeaddress']); //Address
//            if ((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment'])) {
//                $dataArray[0][] = ''; //CardID
//                if ($sys_custom['SupplementarySmartCard']) {
//                    $dataArray[0][] = ''; //CardID2
//                    $dataArray[0][] = ''; //CardID3
//                }
//            }
//            if ($special_feature['ava_hkid'])
//                $dataArray[0][] = $studentInfo['birthcertno']; //HKID
//            if ($special_feature['ava_strn'])
//                $dataArray[0][] = $custInfo['BD_Ref_Num'][0]['Value']; //STRN
//            if ($plugin['medical'])
//                $dataArray[0][] = ''; //StayOverNight
//            $dataArray[0][] = $studentInfo['county']; //Nationality
//            $dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
//            $dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
//        } else if ($tabID == 3) {
//            $hasParent = false;
//            $dataCount = array();
//            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
//            for ($i = 0; $i < count($parentInfo); $i++) {
//                if ($parentInfo[$i]['type'] == 'G' && !$hasParent) {
//                    $dataArray[0] = array();
//                    $dataArray[0][] = ''; //UserLogin
//                    $dataArray[0][] = ''; //Password
//                    $dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
//                    $dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
//                    $dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
//                    $dataArray[0][] = ''; //Gender
//                    $dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
//                    $dataArray[0][] = $custInfo['Parent_Fax'][0]['Value']; //Fax
//                    $dataArray[0][] = ''; //Barcode
//                    $dataArray[0][] = ''; //Remarks
//                    if ($special_feature['ava_hkid'])
//                        $dataArray[0][] = ''; //HKID
//                    $dataArray[0][] = ''; //StudentLogin1
//                    $dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
//                    $dataArray[0][] = ''; //StudentLogin2
//                    $dataArray[0][] = ''; //StudentEngName2
//                    $dataArray[0][] = ''; //StudentLogin3
//                    $dataArray[0][] = ''; //StudentEngName3
//                    //$hasParent = true;
//                    $dataCount[0] = ($parentInfo[$i]['email'] ? 1 : 0) + ($parentInfo[$i]['parent_name_en'] ? 1 : 0) + ($parentInfo[$i]['parent_name_b5'] ? 1 : 0) + ($parentInfo[$i]['mobile'] ? 1 : 0);
//                }
//            }
//            if ($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]) {
//                $tempDataArray = $dataArray[0];
//            } else if ($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]) {
//                $tempDataArray = $dataArray[1];
//            } else if ($dataCount[2] > 0) {
//                $tempDataArray = $dataArray[2];
//            }
//            $dataArray = array();
//            $dataArray[0] = $tempDataArray;
//        }
//        $ExportArr = $dataArray;
//
//        return $ExportArr;
//    }

    function getApplicantEmail($applicationId)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;

        $sql = "SELECT
					a.Email as Email
				FROM ADMISSION_STU_INFO as a
				WHERE a.ApplicationID = '" . trim($applicationId) . "'";
        $records = current($this->returnArray($sql));

        return $records['Email'];
    }

    public function preReplaceEmailVariables($text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1, $interviewDateTime2, $interviewDateTime3, $briefingdate, $briefinginfo, $langSpokenAtHome, $printEmailLink)
    {
        global $kis_lang;
        $sql = "SELECT
					ApplyLevel
				FROM 
				    ADMISSION_OTHERS_INFO
				WHERE 
				    ApplicationID = '{$applicationNo}'";
        $applyLevel = current($this->returnVector($sql));
        $applyLevel = $this->classLevelAry[$applyLevel];
        $rs = $this->getApplicationCustInfo($applicationNo, 'RatingClass', true);
        $ratingClass = $kis_lang['Admission']['TimeSlot'][$rs['Value']];
        $ratingClass_zh = $kis_lang['Admission']['TimeSlot_zh'][$rs['Value']];
        $ratingClass_en = $kis_lang['Admission']['TimeSlot_en'][$rs['Value']];

        $replaced_text = $text;
		$replaced_text = str_replace("[=FirstRoundInterviewDateTimeLocationChi=]",$interviewDateTime1_zh,$replaced_text);
		$replaced_text = str_replace("[=FirstRoundInterviewDateTimeLocationEng=]",$interviewDateTime1_en,$replaced_text);

		$replaced_text = str_replace("[=SecondRoundInterviewDateTimeLocationChi=]",$interviewDateTime2_zh,$replaced_text);
		$replaced_text = str_replace("[=SecondRoundInterviewDateTimeLocationEng=]",$interviewDateTime2_en,$replaced_text);

		$replaced_text = str_replace("[=ThirdRoundInterviewDateTimeLocationChi=]",$interviewDateTime3_zh,$replaced_text);
		$replaced_text = str_replace("[=ThirdRoundInterviewDateTimeLocationEng=]",$interviewDateTime3_en,$replaced_text);
        
        $replaced_text = str_replace("[=AdmissionClass=]", $applyLevel, $replaced_text);
        $replaced_text = str_replace("[=AdmissionSession=]", $ratingClass, $replaced_text);
        $replaced_text = str_replace("[=AdmissionSessionChi=]", $ratingClass_zh, $replaced_text);
        $replaced_text = str_replace("[=AdmissionSessionEng=]", $ratingClass_en, $replaced_text);

        return $replaced_text;
    }

    public function sendMailToNewApplicant($applicationId, $subject, $message, $isResent = false)
    {
        if ($this->isInternalUse($_GET['token']) && !$isResent) {
            return true; // Skip send email for teacher apply
        }
        return parent::sendMailToNewApplicant($applicationId, $subject, $message);
    }

    public function sendMailToNewApplicantWithReceiver($applicationId, $subject, $message, $to_email)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
        include_once($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new libwebmail();

        $from = "no-reply@".$_SERVER['SERVER_NAME'];

        if ($subject == '') {
            $email_subject = "新生入學申請 - 確認通知 (申請編號：{$applicationId})";
        } else {
            $email_subject = $subject;
        }
        $email_message = $message;
        $sent_ok = true;
        if ($to_email != '' && intranet_validateEmail($to_email)) {
            $sent_ok = $libwebmail->sendMail($email_subject, $email_message, $from, array($to_email), array(), array(), "", $IsImportant = "", $mail_return_path = get_webmaster(), $reply_address = "", $isMulti = null, $nl2br = 0);
        } else {
            $sent_ok = false;
        }

        return $sent_ok;
    }


//    function importInterviewInfoByArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds = array())
//    {
//        global $UserID, $admission_cfg;
//
//        // -- insert to arragement log [start]
//        $sql = "INSERT INTO ADMISSION_INTERVIEW_ARRANGEMENT_LOG
//					(SchoolYearID, Round, StatusSelect, DateInput, InputBy, DateModified, ModifiedBy)
//					Values
//					('" . $selectSchoolYearID . "',
//					'" . $round . "',
//					'" . implode(",", $selectStatusArr) . "',
//					NOW(),
//					'" . $UserID . "',
//					NOW(),
//					'" . $UserID . "'
//					)";
//        $insertResult[] = $this->db_db_query($sql);
//        // -- insert to arragement log [end]
//
//        $insertResult[] = $this->updateApplicantArrangement($selectSchoolYearID, $selectStatusArr, $round, $classLevelIds);
//
//        return !in_array(false, $insertResult);
//    }

    function checkImportDataForImportInterview($data)
    {
        $resultArr = array();
        $i = 0;
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);
            //check date
            if ($aData[4] == '' && $aData[5] == '') {
                $validDate = true;
            } else if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4])) {
                list($year, $month, $day) = explode('-', $aData[4]);
                $validDate = checkdate($month, $day, $year);
            } else {
                $validDate = false;
            }

            //check time
            if ($aData[4] == '' && $aData[5] == '') {
                $validTime = true;
            } else if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5])) {
                $validTime = true;
            } else {
                $validTime = false;
            }
            $sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE
					trim(s.applicationID) = '" . trim($aData[0]) . "' AND trim(s.birthCertNo) = '" . trim($aData[3]) . "'
	    	";
            $result = $this->returnVector($sql);
            if ($result[0] == 0) {
                $resultArr[$i]['validData'] = $aData[0];
            } else
                $resultArr[$i]['validData'] = false;
            $resultArr[$i]['validDate'] = $validDate;
            $resultArr[$i]['validTime'] = $validTime;
            if (!$validDate || !$validTime)
                $resultArr[$i]['validData'] = $aData[0];
            $i++;
        }
        return $resultArr;
    }

    function importDataForImportInterview($data)
    {
        $resultArr = array();
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);

            $sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET
		   		InterviewDate = '" . $aData[4] . " " . $aData[5] . "',
				InterviewLocation = '" . $aData[6] . "',
				DateModified = NOW(),
		   		ModifiedBy = '" . $this->uid . "'
   				WHERE ApplicationID = '" . $aData[0] . "'
	    	";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;

        }
        return $resultArr;
    }

    function getExportDataForImportInterview($recordID, $schoolYearID = '', $selectStatus = '', $classLevelID = '')
    {
        global $admission_cfg;
        $cond = !empty($schoolYearID) ? " AND a.SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= !empty($selectStatus) ? " AND a.Status='" . $selectStatus . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				" . $cond . "
    	";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds = array())
    {
        $status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND st.status in ('".implode("','",$selectStatusArr)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		
		$sql='Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" '.$status_cond.' order by y.YearName, o.ApplicationID';
				
		$result = $this->returnArray($sql);
		
		$allApplicant = array();
		
		for($i=0; $i<sizeof($result); $i++){
			$allApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
		}
		
//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);
		
		$result = array();
		
		$TwinsAssignedApplicant = array();
		
//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
				$sql = "SELECT RecordID, Quota FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY StartTime, GroupName";
				$interviewRecordIDArr = $this->returnArray($sql);
				
				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
					
					if(!$allApplicant){
						break;
					}
					
					$previousClassLevel = $allApplicant[0]['ClassLevel'];
					$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
					$result[] = $this->db_db_query($sql);
					
					for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
						$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
						$result[] = $this->db_db_query($sql);
						array_shift($allApplicant);
						if($previousClassLevel != $allApplicant[0]['ClassLevel']){
							break;
						}
					}
	
				}
//		}
		
		//handling twins swraping [start]
		$sql='Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID'.($round>1?$round:'').' as InterviewSettingID From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" AND s.IsTwinsApplied = "Y" order by y.YearName desc';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
			for($j=0; $j<sizeof($twinsResult); $j++){
				if($twinsResult[$i]['TwinsApplicationID'] == $twinsResult[$j]['BirthCertNo'] && $twinsResult[$i]['InterviewSettingID'] == $twinsResult[$j]['InterviewSettingID'] && !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)){
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" ';
					$originalSession = current($this->returnArray($sql));
					
					$sql = 'Select RecordID From ADMISSION_INTERVIEW_SETTING where ClassLevelID = "'.$originalSession['ClassLevelID'].'" AND Date = "'.$originalSession['Date'].'" '.$round_cond.' AND GroupName <> "'.$originalSession['GroupName'].'" ';
					$newSession = current($this->returnArray($sql));
					
					if($newSession){
						$sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" AND s.IsTwinsApplied <> "Y" ';
						$swapApplicant = current($this->returnArray($sql));
						if($swapApplicant){
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$originalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$j]['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$assignedTwins[] = $twinsResult[$j]['ApplicationID'];
						}
					}
				}
			}
		}
		
		//handling twins swraping [end]
		
		return !in_array(false,$result);
    }

    function hasBirthCertNumber($birthCertNo, $applyLevel)
    {
        global $admission_cfg;

        $sql = "SELECT 
               COUNT(*) 
        FROM 
            ADMISSION_STU_INFO AS asi 
        INNER JOIN 
            ADMISSION_OTHERS_INFO AS aoi 
        ON
            asi.ApplicationID = aoi.ApplicationID
        INNER JOIN 
            ADMISSION_APPLICATION_STATUS AS aas 
        ON
            asi.ApplicationID = aas.ApplicationID
        WHERE 
            TRIM(asi.BirthCertNo) = '{$birthCertNo}'
        AND 
            aoi.ApplyYear = '" . $this->getNextSchoolYearID() . "' 
        AND 
            aas.STATUS <> '{$admission_cfg['Status']['CREATIVE_cancelled']}'";

        return current($this->returnVector($sql));
    }

    function hasToken($token)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '" . $token . "' ";
        return current($this->returnVector($sql));
    }

    function getApplicationStudentInfoCust($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = !empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
			SELECT
				s.RecordID,
     			s.ApplicationID applicationID,
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'
			" . $cond . "
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

        return $this->returnArray($sql);
    }

    function getApplicationRelativesInfoCust($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = !empty($applicationID) ? " AND r.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
			SELECT
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year OthersRelativeStudiedYear,
				r.Name OthersRelativeStudiedName,
     			r.ClassPosition OthersRelativeClassPosition,
				r.Relationship OthersRelativeRelationship
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'
			" . $cond . "
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

        return $this->returnArray($sql);
    }

    function uploadAttachment($type, $file, $destination, $randomFileName = "")
    {//The $randomFileName does not contain file extension
        global $admission_cfg, $intranet_root, $libkis;
        include_once($intranet_root . "/includes/libimage.php");
        $uploadSuccess = false;
        $ext = strtolower(getFileExtention($file['name']));

// 		if($type == "personal_photo"){
// 			return true;
// 		}
        if (!empty($file['image_data'])) {
            list($type, $data) = explode(';', $file['image_data']);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);

            $tmpfname = tempnam("/tmp", "admission_file_");
            $handle = fopen($tmpfname, "w");
            fwrite($handle, $data);
            fclose($handle);

            $file['tmp_name'] = $tmpfname;
        }
        if (!empty($file['tmp_name'])) {
            require_once($intranet_root . "/includes/admission/class.upload.php");
            $handle = new Upload($file['tmp_name']);
            if ($handle->uploaded) {
                $handle->Process($destination);
                if ($handle->processed) {
                    $uploadSuccess = true;
                    if ($type == "personal_photo") {
                        $image_obj = new SimpleImage();
                        $image_obj->load($handle->file_dst_pathname);
                        if ($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
                            $image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
                        else
                            $image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
                        //rename the file and then save

                        $image_obj->save($destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext, $image_obj->image_type);
                        unlink($handle->file_dst_pathname);
                    } else {
                        rename($handle->file_dst_pathname, $destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext);
                    }
                    //$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
                } else {
                    // one error occured
                    $uploadSuccess = false;
                }
                // we delete the temporary files
                $handle->Clean();
            }
        } else {
            if ($this->isInternalUse($_REQUEST['token'])) {
                return true;
            }
        }

        if (file_exists($file['tmp_name'])) {
            @unlink($file['tmp_name']);
        }
        return $uploadSuccess;
        //}
        //return true;
    }
    
    function getCustSelection($type='',$name="custSelection",$auto_submit=true,$isAll=true, $isMultiSelect=false){
	    global $admission_cfg,$kis_lang;
    	$x = '<select name="'.$name.'" id="'.$name.'" '.($auto_submit?'class="auto_submit"':'').' '.($isMultiSelect?'multiple':'').'>'; 
    	$x .= ($isAll)?'<option value=""'.($type==''?' selected="selected"':'').'>'.$kis_lang['Admission']['STANDARD']['admissionMethod'] .'</option>':'';
		
		$x .= '<option value="O"'.($type=='O'?' selected="selected"':'').'>';
		$x .= $kis_lang['Admission']['STANDARD']['admissionMethodType']['Online'];
		$x .= '</option>';
		
		$x .= '<option value="H"'.($type=='H'?' selected="selected"':'').'>';
		$x .= $kis_lang['Admission']['STANDARD']['admissionMethodType']['Hand'];
		$x .= '</option>';
		
		$x .= '</select>'; 	
		return $x;
	}
} // End Class
