<?php
# modifying by: Henry

/********************
 *
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_ui_cust_base
 * 			Modified getDocsUploadForm() to follow attachment settings
 * 			Moved getDocsUploadForm() to libadmission_ui_cust_base.php
 *
 * Date		2013-10-09 [Henry]
 * 			File Created
 *
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){

	}

	function getWizardStepsUI($Step){
		global $Lang;

		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";

		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';

		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';

		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';

		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';

		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">
				<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].' Instruction</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].' Personal Info</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].' Docs upload</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>'.$Lang['Admission']['confirmation'].' Confirmation</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:70px">'.$Lang['Admission']['finish'].' Finish</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}

	function getIndexContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].'</br>Welcome to our online application page!'; //Henry 20131107
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';

//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}

					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm();
                    	}
					$x .='</div></div></div>

                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';

    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage']; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
         $x .= $Instruction;
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->
								<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />

							</div>';
					}

					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}

	function getChooseClassForm(){
		global $libkis_admission, $Lang, $lac;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting("all_year");

		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate']){
					$hasClassLevelApply = 1;
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="" />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].'</label>';
				}
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].' </br>Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</br>Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';

		//The new UI 20131025
		$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].' Level</legend><div class="admission_select_option">';
		$x .= $class_level_selection;
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Admission']['newApplication'].' New Application', "button", "goto('step_index','step_instruction')", "SubmitBtn", 'style="font-size:16px;"', 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p>';
			//$x .= '</form></div>';
            return $x;
	}

	function getApplicationForm(){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission;

		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';

		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm();
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span class="text_remark">附有「<span class="tabletextrequire">*</span>」的項目必須填寫</span>';
		$x .='<br/><span class="text_remark">Information marked with “<span class="tabletextrequire">*</span>” must be provided</span>';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />
			</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}

	function getStudentForm($IsConfirm=0){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg;
		
		$religon_option[] = '不適用 N/A';
		$religon_option[] = '天主教 Catholic';
		$religon_option[] = '基督教 Christian';
		$religon_option[] = '佛教 Buddhist';
		$religon_option[] = '道教 Taoist';
		$religon_option[] = '孔教 Confucian';
		$religon_option[] = '伊斯蘭教 Muslim';
		$religon_option[] = '印度教 Hindu';
		$religon_option[] = '其他 Others';
		
		$religion_selection_new = '<select name="StudentReligion">';
		foreach($religon_option as $key => $religon){
			$religion_selection_new .= '<option value="'.$key.'">'.$religon.'</option>';
		}
		$religion_selection_new .= '</select>';
		
		$religion_selected = $religon_option[$formData['StudentReligion']];

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		$x = '<h1>'.$Lang['Admission']['studentInfo'].' Student’s Particulars</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['chinesename'].' Name in Chinese</td>
				<td>'.($IsConfirm?$formData['StudentChiName']:'<input name="StudentChiName" type="text" id="StudentChiName" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['englishname'].' Name in English</td>
				<td>'.($IsConfirm?$formData['StudentEngName']:'<input name="StudentEngName" type="text" id="StudentEngName" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateofbirth'].' Date of Birth</td>
				<td>
				'.($IsConfirm?$formData['StudentDateOfBirth']:'<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['gender'].' Gender</td>
				<td>';
				if($IsConfirm){
					$x .= $Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender'];
				}
				else{
					$x .=$this->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', '0','',$Lang['Admission']['genderType']['M'].' M').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', '0','',$Lang['Admission']['genderType']['F'].' F');
				}

				$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['birthcertno'].' Birth Certificate No.</td>
				<td>'.($IsConfirm?$formData['StudentBirthCertNo']:'<input name="StudentBirthCertNo_f" type="text" id="StudentBirthCertNo_f" class="textboxtext"  maxlength="1" size="1" style="width: 10px;padding: 3px;"/> <input name="StudentBirthCertNo_m" type="text" id="StudentBirthCertNo_m" class="textboxtext"  maxlength="6" size="6" style="width: 50px;padding: 3px;"/> (<input name="StudentBirthCertNo_l" type="text" id="StudentBirthCertNo_l" class="textboxtext"  maxlength="1" size="1" style="width: 10px;padding: 3px;" />) <br/>('.$Lang['Admission']['msg']['birthcertnohints'].')<br/>(e.g.: A123456(3))<input name="StudentBirthCertNo" type="hidden" id="StudentBirthCertNo" value=""/>').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['placeofbirth'].' Place of Birth</td>
				<td>'.($IsConfirm?$formData['StudentPlaceOfBirth']:'<input name="StudentPlaceOfBirth" type="text" id="StudentPlaceOfBirth" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['langspokenathome'].' Spoken language at home</td>
				<td >'.($IsConfirm?$formData['LangSpokenAtHome']:'<input name="LangSpokenAtHome" type="text" id="LangSpokenAtHome" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['homephoneno'].' Contact No.</td>
				<td >'.($IsConfirm?$formData['StudentHomePhoneNo']:'<input name="StudentHomePhoneNo" type="text" id="StudentHomePhoneNo" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['contactperson'].' Contact Person</td>
				<td >'.($IsConfirm?$formData['StudentContactPerson']:'<input name="StudentContactPerson" type="text" id="StudentContactPerson" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['contactpersonrelationship'].' Relationship with Applicant</td>
				<td >'.($IsConfirm?$formData['StudentRelationship']:'<input name="StudentRelationship" type="text" id="StudentRelationship" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['homeaddress'].' Home Address</td>
				<td colspan="3">'.($IsConfirm?$formData['StudentHomeAddress']:'<input type="text" name="StudentHomeAddress" class="textboxtext" id="StudentHomeAddress" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['contactEmail'].' Email Address</td>
				<td colspan="3">'.($IsConfirm?$formData['StudentEmail']:'<input name="StudentEmail" type="text" id="StudentEmail" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['religion'].' Religion</td>
				<td><!--<input name="StudentReligion" type="text" id="StudentReligion" class="textboxtext" />-->
				'.($IsConfirm?$religion_selected:$religion_selection_new).'</td>
				<td class="field_title">'.$Lang['Admission']['church'].' Name of Church</td>
				<td>'.($IsConfirm?$formData['StudentChurch']:'<input name="StudentChurch" id="StudentChurch" type="text" class="textboxtext" />').'
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['lastschool'].' Previous School having attended</td>
				<td>'.($IsConfirm?$formData['StudentLastSchool']:'<input name="StudentLastSchool" type="text" id="StudentLastSchool" class="textboxtext" />').'</td>
				<td class="field_title">'.$Lang['Admission']['lastschoollevel'].' Previous class having attended</td>
				<td>'.($IsConfirm?$formData['StudentLastSchoolLevel']:'<input name="StudentLastSchoolLevel" id="StudentLastSchoolLevel" type="text" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].' Photo</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/><br />
					<em>('.$Lang['Admission']['msg']['personalPhotoFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)</em>
					<br/><em>(Only photo in “.JPG”, “.GIF”, “.PNG” format and under the size of 2 MB is supported)</em>
					<br />').'
				</td>
			</tr>
		</table>';

		return $x;
	}

	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang;

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		$x = '<h1>'.$Lang['Admission']['PGInfo'].' Parents’ Particulars</h1>
				<table class="form_table" style="font-size: 13px">
				<tr>
					<td>&nbsp;</td>
					<td class="form_guardian_head">'.$Lang['Admission']['PG_Type']['F'].' Father</td>
					<td class="form_guardian_head">'.$Lang['Admission']['PG_Type']['M'].' Mother</td>
					<td class="form_guardian_head">'.$Lang['Admission']['PG_Type']['G'].' Guidance</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['chinesename'].' Name in Chinese</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1ChineseName']:'<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2ChineseName']:'<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3ChineseName']:'<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['englishname'].' Name in English</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1EnglishName']:'<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2EnglishName']:'<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3EnglishName']:'<input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['relationship'].' Relationship with Applicant</td>
					<td class="form_guardian_field">
					--
					</td>
					<td class="form_guardian_field">
					--
					</td>
					<td class="form_guardian_field">
					'.($IsConfirm?$formData['G3Relationship']:'<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" />').'
					</td>
				</tr>
				<!--<tr>
					<td class="field_title">Parent Committee</td>
					<td class="form_guardian_field">
					<input type="checkbox" name="G1ParentCommittee" id="G1ParentCommittee" />

					Yes</td>
					<td class="form_guardian_field"><input type="checkbox" name="G2ParentCommittee" id="G2ParentCommittee" />
					Yes</td>
					<td class="form_guardian_field"><input type="checkbox" name="G3ParentCommittee" id="G3ParentCommittee" />
					Yes</td>
				</tr>-->
				<!--<tr>
					<td class="field_title">HKID No.</td>
					<td class="form_guardian_field"><input name="G1HKIdNo" type="text" id="G1HKIdNo" size="15" /></td>
					<td class="form_guardian_field"><input name="G2HKIdNo" type="text" id="G2HKIdNo" size="15" /></td>
					<td class="form_guardian_field"><input name="G3HKIdNo" type="text" id="G3HKIdNo" size="15" /></td>
				</tr>-->
				<tr>
					<td class="field_title">'.$Lang['Admission']['occupation'].' Occupation</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1Occupation']:'<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2Occupation']:'<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3Occupation']:'<input name="G3Occupation" type="text" id="G3Occupation" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['companyname'].' Name of Office</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1CompanyName']:'<input name="G1CompanyName" type="text" id="G1CompanyName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2CompanyName']:'<input name="G2CompanyName" type="text" id="G2CompanyName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3CompanyName']:'<input name="G3CompanyName" type="text" id="G3CompanyName" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['jobposition'].' Job Position</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1JobPosition']:'<input name="G1JobPosition" type="text" id="G1JobPosition" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2JobPosition']:'<input name="G2JobPosition" type="text" id="G2JobPosition" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3JobPosition']:'<input name="G3JobPosition" type="text" id="G3JobPosition" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['companyaddress'].' Office Address</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1CompanyAddress']:'<input name="G1CompanyAddress" type="text" id="G1CompanyAddress" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2CompanyAddress']:'<input name="G2CompanyAddress" type="text" id="G2CompanyAddress" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3CompanyAddress']:'<input name="G3CompanyAddress" type="text" id="G3CompanyAddress" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['phoneno'].' Contact No.</td>
					<td class="form_guardian_field">
					<table style="font-size: 13px;width:auto"><tr><td>('.$Lang['Admission']['office'].' Office)</td><td>'.($IsConfirm?$formData['G1CompanyNo']:'<input name="G1CompanyNo" type="text" id="G1CompanyNo" size="15" />').'</td></tr>
					<tr><td>('.$Lang['Admission']['mobile'].' Mobile)</td><td>'.($IsConfirm?$formData['G1MobileNo']:'<input name="G1MobileNo" type="text" id="G1MobileNo" size="15" />').'</td></tr>
					</table>
					</td>
					<td class="form_guardian_field">
					<table style="font-size: 13px;width:auto"><tr><td>('.$Lang['Admission']['office'].' Office)</td><td>'.($IsConfirm?$formData['G2CompanyNo']:'<input name="G2CompanyNo" type="text" id="G2CompanyNo" size="15" />').'</td></tr>
					<tr><td>('.$Lang['Admission']['mobile'].' Mobile)</td><td>'.($IsConfirm?$formData['G2MobileNo']:'<input name="G2MobileNo" type="text" id="G2MobileNo" size="15" />').'</td></tr>
					</table>
					</td>
					<td class="form_guardian_field">
					<table style="font-size: 13px;width:auto"><tr><td>('.$Lang['Admission']['office'].' Office)</td><td>'.($IsConfirm?$formData['G3CompanyNo']:'<input name="G3CompanyNo" type="text" id="G3CompanyNo" size="15" />').'</td></tr>
					<tr><td>('.$Lang['Admission']['mobile'].' Mobile)</td><td>'.($IsConfirm?$formData['G3MobileNo']:'<input name="G3MobileNo" type="text" id="G3MobileNo" size="15" />').'</td></tr>
					</table>
					</td>
				</tr>
			</table>';

		return $x;
	}

	function getOthersForm($IsConfirm=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;

		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());

		$Lang['Btn']['Select'] = '選擇 Please Choose';
		$Lang['Admission']['mailleaflet'] .= ' Flyers';
		$Lang['Admission']['newspaper'] .= ' Newspaper';
		$Lang['Admission']['introduced'] .= ' Friends';
		$Lang['Admission']['ourwebsite'] .= ' Our website';
		$Lang['Admission']['otherwebsite'] .= ' Other websites (please state)';
		$Lang['Admission']['advertisement'] .= ' Advertisements (please state)';
		$Lang['Admission']['others'] .= ' Other (please state)';
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting("all_year");

		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);

		$dayTypeOption="";

		$termType = $applicationSetting[$_REQUEST['hidden_class']]['TermType'];
		$termTypeArr = explode(',',$termType);

		$termTypeOption="";

		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
		else{
			foreach($dayTypeArr as $aDayType){
	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
	//			$dayTypeOption .=" ";
				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
			}
			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
		}

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		$x = '<h1>'.$Lang['Admission']['otherInfo'].' Other Information</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['familyStatus'].' No. of Sibling(s)</td>
				<td colspan="3"><!--<input name="OthersFamilyStatus" type="text" id="OthersFamilyStatus" class="textboxtext" />-->
				<table style="font-size: 13px">
					<tr>
						<td>'.$Lang['Admission']['elderBrother'].' ('.$Lang['Admission']['person'].') No. of Older Brother(s)</td><td>'.$Lang['Admission']['elderSister'].' ('.$Lang['Admission']['person'].') No. of Older Sister(s)</td><td>'.$Lang['Admission']['youngerBrother'].' ('.$Lang['Admission']['person'].') No. of Younger Brother(s)</td><td>'.$Lang['Admission']['youngerSister'].' ('.$Lang['Admission']['person'].') No. of Younger Sister(s)</td>
					</tr>
					<tr>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_EB']:$this->Get_Number_Selection('OthersFamilyStatus_EB', '0', '5','0')).'</td>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_ES']:$this->Get_Number_Selection('OthersFamilyStatus_ES', '0', '5','0')).'</td>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_YB']:$this->Get_Number_Selection('OthersFamilyStatus_YB', '0', '5','0')).'</td>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_YS']:$this->Get_Number_Selection('OthersFamilyStatus_YS', '0', '5','0')).'</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateOfEntry'].' Semester of Choice</td>
				<td><table style="font-size: 13px">
					<tr><td width="100px">('.$Lang['General']['SchoolYear'].' School Year)</td><td><div id="SchoolYearOption">'.($IsConfirm?getAcademicYearByAcademicYearID($formData['SchoolYearID']):$admission_year).'</div></td></tr>
					<tr><td width="100px">('.$Lang['Admission']['month'].' Month)</td><td>'.($IsConfirm?$formData['OthersApplyMonth']:$this->Get_Number_Selection('OthersApplyMonth', '1', '12')).'</td></tr>
					</table>
				<!--'.$this->GET_DATE_PICKER('OthersApplyDate').'--></td>';
				$x .='<td class="field_title">'.$star.$Lang['Admission']['applyTerm'].' School Semester</td>
				<td><!--<input name="OthersApplyTerm" type="text" id="OthersApplyTerm" class="textboxtext" />--><div id="TermTypeOption">';
				if($IsConfirm){
					//$x .= "Term ".$formData['OthersApplyTerm'];
					$x .=$Lang['Admission']['Term'][$formData['OthersApplyTerm']];
				}
				else{
					foreach($termTypeArr as $aTermType){
						$x .=$this->Get_Radio_Button('OthersApplyTerm'.$aTermType, 'OthersApplyTerm', $aTermType, '0','',$Lang['Admission']['Term'][$aTermType]).' ';
					}
				}
			$x .='</div></td>';
			$x .='</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].' Class of Choice</td>
				<td '.($IsConfirm?'':'colspan="3"').'>
				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
				'.($IsConfirm?'<td class="field_title">'.$Lang['Admission']['applyLevel'].' Level</td><td>'.$classLevel.'</td>':'').'
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['ExBSName'].' Particulars of sibling(s) having attended this kindergarten</td>
				<td>
					<table style="font-size: 13px">
						<tr>
							<td width="40px">('.$Lang['Admission']['name'].' Name)</td>
							<td>
								'.($IsConfirm?$formData['OthersExBSName']:'<input name="OthersExBSName" type="text" id="OthersExBSName" class="textboxtext" />').'
							</td>
						</tr>
						<tr>
							<td width="40px">('.$Lang['Admission']['level'].' Class)</td>
							<td>
								'.($IsConfirm?$formData['OthersExBSLevel']:'<input name="OthersExBSLevel" type="text" id="OthersExBSLevel" class="textboxtext" />').'
							</td>
						</tr>
					</table>
				</td>
				<td class="field_title">'.$Lang['Admission']['CurBSName'].' Particulars of sibling(s) attending this kindergarten</td>
				<td>
					<table style="font-size: 13px">
						<tr>
							<td width="40px">('.$Lang['Admission']['name'].' Name)</td>
							<td>
								'.($IsConfirm?$formData['OthersCurBSName']:'<input name="OthersCurBSName" type="text" id="OthersCurBSName" class="textboxtext" />').'
							</td>
						</tr>
						<tr>
							<td width="40px">('.$Lang['Admission']['level'].' Class)</td>
							<td>
								'.($IsConfirm?$formData['OthersCurBSLevel']:'<input name="OthersCurBSLevel" type="text" id="OthersCurBSLevel" class="textboxtext" />').'
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['needSchoolBus'].' Request of School Bus Service</td>
				<td>';
				if($IsConfirm){
					$x .= $formData['OthersNeedSchoolBus']==1?$Lang['Admission']['need'].' Yes':$Lang['Admission']['noneed'].' No';
				}
				else{
					$x .='
					<input type="radio" name="OthersNeedSchoolBus" id="OthersNeedSchoolBus1" value="1" />
					<label for="OthersNeedSchoolBus1" >'.$Lang['Admission']['need'].' Yes</label>
					<input type="radio" name="OthersNeedSchoolBus" id="OthersNeedSchoolBus2" value="0" />
					<label for="OthersNeedSchoolBus2">'.$Lang['Admission']['noneed'].' No</label>';
				}
				$x .='</td>
				<td class="field_title">'.$star.$Lang['Admission']['placeForTakingSchoolBus'].' ('.$Lang['Admission']['msg']['placeForTakingSchoolBusHints'].') Drop-off Point (if applicable)</td>
				<td>'.($IsConfirm?$formData['OthersSchoolBusPlace']:'<input name="OthersSchoolBusPlace" type="text" id="OthersSchoolBusPlace" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['knowUsBy'].' How do you learn about this kindergarten?</td>
				<td colspan="3"><!--<input name="OthersKnowUsBy" type="text" id="OthersKnowUsBy" class="textboxtext" />-->';
				if($IsConfirm){
					if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['mailleaflet']){
						$x .= $Lang['Admission']['mailLeaflet'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['newspaper']['index']){
						$x .= $Lang['Admission']['newspaper'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['introduced']['index']){
						$x .= $Lang['Admission']['introduced'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['ourwebsite']['index']){
						$x .= $Lang['Admission']['ourwebsite'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['otherwebsite']['index']){
						$x .= $Lang['Admission']['otherwebsite'].' ('.$formData['txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index']].')';
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['advertisement']['index']){
						$x .= $Lang['Admission']['advertisement'].' ('.$formData['txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index']].')';
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['others']['index']){
						$x .= $Lang['Admission']['others'].' ('.$formData['txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index']].')';
					}
				}
				else{
					$x .=$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['mailleaflet']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['mailleaflet']['index'], '0','',$Lang['Admission']['mailleaflet']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['newspaper']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['newspaper']['index'], '0','',$Lang['Admission']['newspaper']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['introduced']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['introduced']['index'], '0','',$Lang['Admission']['introduced']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['ourwebsite']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['ourwebsite']['index'], '0','',$Lang['Admission']['ourwebsite']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['otherwebsite']['index'], '0','',$Lang['Admission']['otherwebsite'].' ('.$Lang['General']['PlsSpecify'].'): ').'
					<input name="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index'].'" type="text" id="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index'].'" class="textboxtext" style="width:100px;"/><br/> '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['advertisement']['index'], '0','',$Lang['Admission']['advertisement'].' ('.$Lang['General']['PlsSpecify'].'): ').'
					<input name="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index'].'" type="text" id="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index'].'" class="textboxtext" style="width:100px;"/> '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['others']['index'], '0','',$Lang['Admission']['others'].' ('.$Lang['General']['PlsSpecify'].'): ').'
					<input name="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index'].'" type="text" id="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index'].'" class="textboxtext" style="width:100px;"/>';
				}
				$x .='</td>
			</tr>
			</table>';

		return $x;
	}

	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac;

		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';

		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1>'.$Lang['Admission']['docsUpload'].' Document</h1>';
		}
		$x .='<table class="form_table">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
			$x .='<tr>
					<td colspan="2">Documents <span class="date_time">(image in JPEG/GIF/PNG/PDF format, file size less than'.($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';	
		}

		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$star.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
				  </tr>';
		}

//		$x .='<tr>
//					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
//					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
//			</tr>';
//		$x .='<tr>
//					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
//					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
//			</tr>';

		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');

		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>,
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />

			</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}

	function getConfirmPageContent(){
		global $Lang, $fileData, $formData;

		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}

		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);

		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Submit'].' Submit', "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />

			</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}

	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $Lang, $lac;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){

			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['TBCK']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink($schoolYearID, $ApplicationID).'&form_lang=chi'.'\',\'_blank\');"></input></span></h1>';
			
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'
//				</p>';
			$x .='<h1><span>並查閱閣下之電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';
			
			$x .='<div class="admission_complete_msg"><h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink($schoolYearID, $ApplicationID).'&form_lang=eng'.'\',\'_blank\');"></input></span></h1>';
				
			$x .='<h1><span>And access to your e-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';			
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
			$x .=' <div class="admission_complete_msg"><h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		$x .= '<div class="edit_bottom">
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Admission']['finish'].' Finish" />
			</div>
			<p class="spacer"></p></div>';
		return $x;
	}

	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom,$intranet_session_language;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		if($ApplicationID){
			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink($schoolYearID, $ApplicationID).'&form_lang=chi';
			$x .='<br/><br/>';
			$x .='Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>Application form preview<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink($schoolYearID, $ApplicationID).'&form_lang=eng';
			if(!$LastContent){
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
			$x .= '<br/>'.$LastContent;
		}
		else{
			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
			$x .='<br/><br/>';
			$x .='Admission is Not Completed. Please try to apply again!';
        }

		return $x;
	}

	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){

			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['TBCK']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'
//				</p>';
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		$x .= '<div class="edit_bottom">
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Back'].'" />
			</div>
			<p class="spacer"></p></div>';
		return $x;
	}

	function getPrintPageContent($schoolYearID,$applicationID, $type="",$form_lang=""){ //using $type="teacher" if the form is print from teacher
		global $PATH_WRT_ROOT,$Lang,$kis_lang, $libkis,$intranet_session_language;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		$lac = new admission_cust();
		if($applicationID != ""){
		//get student information
		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
		foreach($parentInfo as $aParent){
			if($aParent['type'] == 'F'){
				$fatherInfo = $aParent;
			}
			else if($aParent['type'] == 'M'){
				$motherInfo = $aParent;
			}
			else if($aParent['type'] == 'G'){
				$guardianInfo = $aParent;
			}
		}

		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
		if($_SESSION['UserType']==USERTYPE_STAFF){
			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
			if(!is_date_empty($remarkInfo['interviewdate'])){
				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
				list($y,$m,$d) = explode('-',$date);
				if($hour>12){
					$period = '下午';
					$hour -= 12;
				}elseif($hour<12){
					$period = '上午';
				}else{
					$period = '中午';
				}
				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
			}else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
			}
		}
		else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
		}
		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
		$personalPhotoPath = $attachmentList['personal_photo']['link'];
		$classLevel = $lac->getClassLevel();

//		debug_pr($studentInfo);
//		debug_pr($fatherInfo);
//		debug_pr($motherInfo);
//		debug_pr($guardianInfo);
//		debug_pr($othersInfo);

//		$x = '<div id="printOption" style="float: right"><table width="90%" align="center" class="print_hide" border="0">
//			<tr>
//				<td align="right">'.$this->GET_ACTION_BTN($kis_lang['print'], "button", "javascript:window.print();").'</td>
//			</tr>
//		</table></div>';

		$school = $libkis->getUserSchool();
				
				// Header of the page
		if ($form_lang == 'eng' || (!$form_lang && $intranet_session_language == 'en')) {
			$Lang['Admission']['mailleaflet'] = ' Flyers';
			$Lang['Admission']['newspaper'] = ' Newspaper';
			$Lang['Admission']['introduced'] = ' Friends';
			$Lang['Admission']['ourwebsite'] = ' Our website';
			$Lang['Admission']['otherwebsite'] = ' Other websites (please state)';
			$Lang['Admission']['advertisement'] = ' Advertisements (please state)';
			$Lang['Admission']['others'] = ' Other (please state)';
			
			for($i = 1; $i <= 3; $i ++) {
				if ($othersInfo ['ApplyDayType' . $i] != 0) {
					// $dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
					$dayTypeOption .= "(Option " . $i . ") " . ($othersInfo ['ApplyDayType' . $i]==1?'A.M.':($othersInfo ['ApplyDayType' . $i]==2?'P.M.':'Full Day')) . "&nbsp;&nbsp;";
				}
			}
			
			$x = '<div class="input_form" style="width:720px;margin:auto;">';
			$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=" . rawurlencode ( $othersInfo ['applicationID'] ) . "&width=160&height=40&format=PNG'></div>";
			$x .= '<div style="float:left;padding-top:10px; position:absolute"><img style="width: 100px;" src="' . $school ['logo'] . '" /></div>';
			$x .= '<h4 align="center" style="padding-top:45px">真理浸信會何袁惠琼幼稚園<br/>Truth Baptist Church Ho Yuen Wai King Kindergarten</h4>';
			
			$x .= '<h5 align="center">Address：109, 1/F, Yung Shing Shopping Centre, Fai Ming Road, Fanling<br/>
				Tel. : 2606 5599 Fax : 2606 5611<br/>
				Website: http://www.truthbaptist.org.hk</h5>';
			
			$x .= '<h4 align="center">Application Form</h4>';
			$x .= '<span style="font-size: 13px">Online Application no.: ' . $othersInfo ['applicationID'] . '</span>';
			$x .= '<span style="float:right;font-size: 12px">Date of Application: ' . substr ( $othersInfo ['DateInput'], 0, - 9 ) . '</span>';
			$x .= '<table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="5" class="print_field_title_main">Student’s Particulars</td>
			  </tr>
			  <tr>
			    <td class="print_field_title print_field_row1">Name in Chinese</td>
			    <td>' . $studentInfo ['student_name_b5'] . '</td>
			    <td class="print_field_title">Name in English</td>
			    <td>' . $studentInfo ['student_name_en'] . '</td>
			    <td rowspan="6" width="100"><img src="' . $personalPhotoPath . '" width="100px" /><!--相片--></td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">Date of Birth</td>
			    <!--<td colspan="2">' . $studentInfo ['dateofbirth'] . ' (年-月-日)</td>-->
				<td>' . substr ( $studentInfo ['dateofbirth'], 0, 4 ) . ' - ' . substr ( $studentInfo ['dateofbirth'], 5, 2 ) . ' - ' . substr ( $studentInfo ['dateofbirth'], 8, 2 ) . '</td>
			    <td class="print_field_title">Gender</td>
			    <td>' . $Lang ['Admission'] ['genderType'] [$studentInfo ['gender']] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">Birth Certificate No.</td>
			    <td>' . $studentInfo ['birthcertno'] . '</td>
			    <td class="print_field_title">Place of Birth</td>
			    <td>' . $studentInfo ['placeofbirth'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">Spoken language at home</td>
			    <td>' . $studentInfo ['homeLang'] . '</td>
			    <td class="print_field_title">Contact No.</td>
			    <td>' . $studentInfo ['homephoneno'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">Contact Person</td>
			    <td>' . $studentInfo ['contactperson'] . '</td>
				<td class="print_field_title">Relationship with Applicant</td>
			    <td>' . $studentInfo ['contactpersonrelationship'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">Home Address</td>
			    <td colspan="3">' . $studentInfo ['homeaddress'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">Email Address</td>
			    <td colspan="4">' . $studentInfo ['email'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">Religion</td>
			    <td>' . $lac->returnPresetCodeName ( "RELIGION", $studentInfo ['religion'] ) . '</td>
			    <td class="print_field_title">Name of Church</td>
			    <td colspan="2">' . $studentInfo ['church'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">Previous School having attended</td>
			    <td>' . $studentInfo ['lastschool'] . '</td>
			    <td class="print_field_title">Previous class having attended</td>
			    <td colspan="2">' . $studentInfo ['lastschoollevel'] . '</td>
			  </tr>
			</table>
			
			<table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="2" class="print_field_title_main">Parents’ Particulars</td>
			    <td class="print_field_title_main print_field_title_parent">Father</td>
			    <td class="print_field_title_main print_field_title_parent">Mother</td>
			    <td class="print_field_title_main print_field_title_parent">Guidance（Please state the relationship with applicant）</td>
			  </tr>
			  <tr class="tg-even">
			    <td colspan="2" class="print_field_title print_field_row1">Name</td>
			    <td>' . $fatherInfo ['parent_name_b5'] . '</td>
			    <td>' . $motherInfo ['parent_name_b5'] . '</td>
			    <td>' . $guardianInfo ['parent_name_b5'] . ($guardianInfo ['parent_name_b5'] ? ' (' : '') . $guardianInfo ['relationship'] . ($guardianInfo ['parent_name_b5'] ? ')' : '') . '</td>
			  </tr>
			  <tr>
			    <td colspan="2" class="print_field_title">Occupation</td>
			    <td>' . $fatherInfo ['occupation'] . '</td>
			    <td>' . $motherInfo ['occupation'] . '</td>
			    <td>' . $guardianInfo ['occupation'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td rowspan="3" class="print_field_title  print_field_row2">Office</td>
			    <td class="print_field_title  print_field_row3">Title</td>
			    <td>' . $fatherInfo ['companyname'] . '</td>
			    <td>' . $motherInfo ['companyname'] . '</td>
			    <td>' . $guardianInfo ['companyname'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">Position</td>
			    <td>' . $fatherInfo ['jobposition'] . '</td>
			    <td>' . $motherInfo ['jobposition'] . '</td>
			    <td>' . $guardianInfo ['jobposition'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">Address</td>
			    <td>' . $fatherInfo ['companyaddress'] . '</td>
			    <td>' . $motherInfo ['companyaddress'] . '</td>
			    <td>' . $guardianInfo ['companyaddress'] . '</td>
			  </tr>
			
			  <tr class="tg-even">
			    <td class="print_field_title" rowspan="2">Contact<br>No.</td>
				<td class="print_field_title">Office</td>
			    <td>' . $fatherInfo ['companyphone'] . '</td>
			    <td>' . $motherInfo ['companyphone'] . '</td>
			    <td>' . $guardianInfo ['companyphone'] . '</td>
			  </tr>
			  <tr class="tg-even">
				<td class="print_field_title">Mobile</td>
			    <td>' . $fatherInfo ['mobile'] . '</td>
			    <td>' . $motherInfo ['mobile'] . '</td>
			    <td>' . $guardianInfo ['mobile'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td colspan="2" class="print_field_title">No. of Sibling(s)</td>
			    <td colspan="3">Brothers: '.($othersInfo ['elder_brother']+$othersInfo ['younger_brother']).'&nbsp;&nbsp;&nbsp;&nbsp;Sisters: '.( $othersInfo ['elder_sister']+$othersInfo ['younger_sister'] ).'</td>
			  </tr>
			</table>
			<table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="3" class="print_field_title_main">Applying for <span class="input_content">' . date('F', mktime(0, 0, 0, $othersInfo ['month'], 10)) . ' of Year ' .getAcademicYearByAcademicYearID ( $othersInfo ['schoolYearId'] ). ' (' . ($othersInfo ['term'] == 1 ? "1st" : "2nd") . ' Semester)</span>Class：<span class="input_content">' . $classLevel [$othersInfo ['classLevelID']] . '</span><br/>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title  print_field_row3">Class</td>
			    <td colspan="2">' . $dayTypeOption . '</td>
			  </tr>
			  <tr>
				<td class="print_field_title_main">Particulars<br/>of Siblings</td>
			    <td class="print_field_title_main">Having attended</td>
			    <td class="print_field_title_main">Attending</td>
			    <!--<td rowspan="2">面試日期：' . $interviewdate . '</td>-->
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title"><span class="print_field_title  print_field_row3">Name</span></td>
			    <td>' . $othersInfo ['ExBSName'] . '</td>
				<td>' . $othersInfo ['CurBSName'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">Class</td>
			    <td>' . $othersInfo ['ExBSLevel'] . '</td>
			    <td>' . $othersInfo ['CurBSLevel'] . '</td>
			    <!--<td rowspan="3">其他備註：（如入學日期）</br>
					' . $remarkInfo ['remark'] . '
			    </td>-->
			  </tr>
			  <tr>
			    <td colspan="3" class="print_field_title_main">Any school bus service? <span class="input_content">' .$othersInfo ['needschoolbus']. '</span><br/>
			(*For those who need school bus services, please state your drop-off point: <span class="input_content">' . $othersInfo ['SchoolBusPlace'] . '</span>)</td>
			  </tr>
			  <tr class="tg-even">
			    <td colspan="3" class="print_field_title_main">Know this kindergarten via: <span class="input_content">' . $Lang ['Admission'] [$othersInfo ['knowusby']] . ($othersInfo ['KnowUsByOther'] ? '</span>（請註明：<span class="input_content">' . $othersInfo ['KnowUsByOther'] . '</span>)' : '') . '<!--郵寄單張□ 報章□ 介紹□ 本會網站□ 其他網站□（請註明）_________<br/>
			廣告□(請註明)＿＿＿＿＿＿＿＿＿＿＿ 其他＿＿＿＿＿＿＿＿＿＿＿＿--></td>
			  </tr>
			</table>';
			$x .= '</div>';
		}else{
			for($i = 1; $i <= 3; $i ++) {
				if ($othersInfo ['ApplyDayType' . $i] != 0) {
					// $dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
					$dayTypeOption .= "(選擇 " . $i . ") " . ($othersInfo ['ApplyDayType' . $i]==1?'上午班':($othersInfo ['ApplyDayType' . $i]==2?'下午班':'全日制')) . "&nbsp;&nbsp;";
				}
			}
			
		$x = '<div class="input_form" style="width:720px;margin:auto;">';
		$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=" . rawurlencode ( $othersInfo ['applicationID'] ) . "&width=160&height=40&format=PNG'></div>";
		$x .= '<div style="float:left;padding-top:10px; position:absolute"><img style="width: 100px;" src="' . $school ['logo'] . '" /></div>';
		$x .= '<h4 align="center" style="padding-top:45px">真理浸信會何袁惠琼幼稚園<br/>Truth Baptist Church Ho Yuen Wai King Kindergarten</h4>';
		
		$x .= '<h5 align="center">地址：粉嶺暉明路雍盛苑商場109號<br/>
				電話：2606 5599 傳真：2606 5611<br/>
				網址：http://www.truthbaptist.org.hk</h5>';
		
		$x .= '<h4 align="center">新生入學申請表</h4>';
		$x .= '<span style="font-size: 13px">網上申請編號：' . $othersInfo ['applicationID'] . '</span>';
		$x .= '<span style="float:right;font-size: 12px">填表日期：' . substr ( $othersInfo ['DateInput'], 0, - 9 ) . '</span>';
		$x .= '<table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="5" class="print_field_title_main">學生資料</td>
			  </tr>
			  <tr>
			    <td class="print_field_title print_field_row1">中文姓名</td>
			    <td>' . $studentInfo ['student_name_b5'] . '</td>
			    <td class="print_field_title">英文姓名</td>
			    <td>' . $studentInfo ['student_name_en'] . '</td>
			    <td rowspan="6" width="100"><img src="' . $personalPhotoPath . '" width="100px" /><!--相片--></td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">出生日期</td>
			    <!--<td colspan="2">' . $studentInfo ['dateofbirth'] . ' (年-月-日)</td>-->
				<td>' . substr ( $studentInfo ['dateofbirth'], 0, 4 ) . ' 年 ' . substr ( $studentInfo ['dateofbirth'], 5, 2 ) . ' 月 ' . substr ( $studentInfo ['dateofbirth'], 8, 2 ) . ' 日</td>
			    <td class="print_field_title">性別</td>
			    <td>' . $Lang ['Admission'] ['genderType'] [$studentInfo ['gender']] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">出世紙號碼</td>
			    <td>' . $studentInfo ['birthcertno'] . '</td>
			    <td class="print_field_title">出生地點</td>
			    <td>' . $studentInfo ['placeofbirth'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">家中常用語言</td>
			    <td>' . $studentInfo ['homeLang'] . '</td>
			    <td class="print_field_title">聯絡電話</td>
			    <td>' . $studentInfo ['homephoneno'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">聯絡人</td>
			    <td>' . $studentInfo ['contactperson'] . '</td>
				<td class="print_field_title">與聯絡人的關係</td>
			    <td>' . $studentInfo ['contactpersonrelationship'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">地址</td>
			    <td colspan="3">' . $studentInfo ['homeaddress'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">電郵</td>
			    <td colspan="4">' . $studentInfo ['email'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">宗教</td>
			    <td>' . $lac->returnPresetCodeName ( "RELIGION", $studentInfo ['religion'] ) . '</td>
			    <td class="print_field_title">所屬教會</td>
			    <td colspan="2">' . $studentInfo ['church'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">曾就讀學校名稱</td>
			    <td>' . $studentInfo ['lastschool'] . '</td>
			    <td class="print_field_title">曾就讀級別</td>
			    <td colspan="2">' . $studentInfo ['lastschoollevel'] . '</td>
			  </tr>
			</table>
		
			<table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="2" class="print_field_title_main">家長資料</td>
			    <td class="print_field_title_main print_field_title_parent">父親</td>
			    <td class="print_field_title_main print_field_title_parent">母親</td>
			    <td class="print_field_title_main print_field_title_parent">監護人（請註明關係）</td>
			  </tr>
			  <tr class="tg-even">
			    <td colspan="2" class="print_field_title print_field_row1">姓名</td>
			    <td>' . $fatherInfo ['parent_name_b5'] . '</td>
			    <td>' . $motherInfo ['parent_name_b5'] . '</td>
			    <td>' . $guardianInfo ['parent_name_b5'] . ($guardianInfo ['parent_name_b5'] ? ' (' : '') . $guardianInfo ['relationship'] . ($guardianInfo ['parent_name_b5'] ? ')' : '') . '</td>
			  </tr>
			  <tr>
			    <td colspan="2" class="print_field_title">職業</td>
			    <td>' . $fatherInfo ['occupation'] . '</td>
			    <td>' . $motherInfo ['occupation'] . '</td>
			    <td>' . $guardianInfo ['occupation'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td rowspan="3" class="print_field_title  print_field_row2">辦<br/>事<br/>處</td>
			    <td class="print_field_title  print_field_row3">名稱</td>
			    <td>' . $fatherInfo ['companyname'] . '</td>
			    <td>' . $motherInfo ['companyname'] . '</td>
			    <td>' . $guardianInfo ['companyname'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">職位</td>
			    <td>' . $fatherInfo ['jobposition'] . '</td>
			    <td>' . $motherInfo ['jobposition'] . '</td>
			    <td>' . $guardianInfo ['jobposition'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title">地址</td>
			    <td>' . $fatherInfo ['companyaddress'] . '</td>
			    <td>' . $motherInfo ['companyaddress'] . '</td>
			    <td>' . $guardianInfo ['companyaddress'] . '</td>
			  </tr>
		
			  <tr class="tg-even">
			    <td class="print_field_title" rowspan="2">電<br>話</td>
				<td class="print_field_title">辦公室</td>
			    <td>' . $fatherInfo ['companyphone'] . '</td>
			    <td>' . $motherInfo ['companyphone'] . '</td>
			    <td>' . $guardianInfo ['companyphone'] . '</td>
			  </tr>
			  <tr class="tg-even">
				<td class="print_field_title">手提</td>
			    <td>' . $fatherInfo ['mobile'] . '</td>
			    <td>' . $motherInfo ['mobile'] . '</td>
			    <td>' . $guardianInfo ['mobile'] . '</td>
			  </tr>
			  <tr class="tg-even">
			    <td colspan="2" class="print_field_title">家庭狀況</td>
			    <td colspan="3">兄 ' . $othersInfo ['elder_brother'] . ' 人&nbsp;&nbsp;&nbsp;&nbsp;姊 ' . $othersInfo ['elder_sister'] . ' 人&nbsp;&nbsp;&nbsp;&nbsp;弟 ' . $othersInfo ['younger_brother'] . ' 人&nbsp;&nbsp;&nbsp;&nbsp;妹 ' . $othersInfo ['younger_sister'] . ' 人</td>
			  </tr>
			</table>
			<table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="3" class="print_field_title_main">現欲申請 <span class="input_content">' . getAcademicYearByAcademicYearID ( $othersInfo ['schoolYearId'] ) . '年度 ' . $othersInfo ['month'] . ' 月（' . ($othersInfo ['term'] == 1 ? "上" : "下") . '學期）</span>入讀：<span class="input_content">' . $classLevel [$othersInfo ['classLevelID']] . '</span><br/>
			<!--（請在適當位置加；如兩者皆選，請以1、2表示優先次序）--></td>
			    <!--<td class="print_field_title_remark">附 註（由校方填寫）</td>-->
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title  print_field_row3">班級</td>
			    <td colspan="2">' . $dayTypeOption . '</td>
			    <!--<td>報名費收據號碼：' . $remarkInfo ['receiptID'] . '<br/>
			日期：' . (! is_date_empty ( $remarkInfo ['receiptdate'] ) ? $remarkInfo ['receiptdate'] : "__________") . ' 經手人：' . ($remarkInfo ['handler'] ? $remarkInfo ['handler'] : "__________") . '</td>-->
			  </tr>
			  <tr>
				<td class="print_field_title_main">本校<br>就讀兄姊</td>
			    <td class="print_field_title_main">曾在</td>
			    <td class="print_field_title_main">現在</td>
			    <!--<td rowspan="2">面試日期：' . $interviewdate . '</td>-->
			  </tr>
			  <tr class="tg-even">
			    <td class="print_field_title"><span class="print_field_title  print_field_row3">姓名</span></td>
			    <td>' . $othersInfo ['ExBSName'] . '</td>
				<td>' . $othersInfo ['CurBSName'] . '</td>
			  </tr>
			  <tr>
			    <td class="print_field_title">級別</td>
			    <td>' . $othersInfo ['ExBSLevel'] . '</td>
			    <td>' . $othersInfo ['CurBSLevel'] . '</td>
			    <!--<td rowspan="3">其他備註：（如入學日期）</br>
					' . $remarkInfo ['remark'] . '
			    </td>-->
			  </tr>
			  <tr>
			    <td colspan="3" class="print_field_title_main">是否需要乘搭校車？ <span class="input_content">' . ($othersInfo ['needschoolbus'] == 'yes' ? '需要' : '不需要') . '</span><br/>
			(*如需要，請填寫乘搭校車地點：<span class="input_content">' . $othersInfo ['SchoolBusPlace'] . '</span>)</td>
			  </tr>
			  <tr class="tg-even">
			    <td colspan="3" class="print_field_title_main">認識本校途徑：<span class="input_content">' . $Lang ['Admission'] [$othersInfo ['knowusby']] . ($othersInfo ['KnowUsByOther'] ? '</span>（請註明：<span class="input_content">' . $othersInfo ['KnowUsByOther'] . '</span>)' : '') . '<!--郵寄單張□ 報章□ 介紹□ 本會網站□ 其他網站□（請註明）_________<br/>
			廣告□(請註明)＿＿＿＿＿＿＿＿＿＿＿ 其他＿＿＿＿＿＿＿＿＿＿＿＿--></td>
			  </tr>
			</table>';
		$x .= '</div>';
		}
			return $x;
		}
		else
			return false;
	}
	
	function getPrintPageCss(){
		return '<style type="text/css">
.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
.tg-table-plain td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
.print_field_title { background: #EFEFEF}
.print_field_title_main { background:#D7D7D7}
.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
.print_field_row1 { width:120px}
.print_field_row2 { width:28px}
.print_field_row3 { width:80px}
.print_field_title_remark { background:#B9B9B9; width:290px;}
.print_field_title_parent{ width:181px;}
@media print
{
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
</style>';
	}
}
?>
