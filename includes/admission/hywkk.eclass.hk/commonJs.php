<script type="text/javascript">
var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	
	/* get the birthday range of the form level */
	var values = $("#form1").serialize();
   $.ajax({
       url: "ajax_get_bday_range.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
           dOBRange = data.split(",");
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
	
	var english = /^[A-Za-z0-9]*$/;
	var re = /\S+@\S+\.\S+/;
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	var today = '<?=date('Y-m-d')?>';
	
	//Assign the birth cert no parts to hidden field
	form1.StudentBirthCertNo.value = form1.StudentBirthCertNo_f.value.toUpperCase() + form1.StudentBirthCertNo_m.value +'('+form1.StudentBirthCertNo_l.value+')';
	
	if(form1.StudentPersonalPhoto.files[0]){
	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	if(form1.StudentChiName.value==''){
		return false;
	} else if(form1.StudentEngName.value==''){
		return false;
	} else if(!english.test(form1.StudentEngName.value.replace(/\s+/g, ''))){
		return false;
	} else if(!check_date_without_return_msg(form1.StudentDateOfBirth)<?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?> || (dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1])<?}?>){
		return false;
	} else if($('input:radio[name=StudentGender]:checked').val() == null){
		return false;
	} else if(form1.StudentBirthCertNo_f.value==''){
		return false;
	} else if(!/^[a-zA-Z]$/.test(form1.StudentBirthCertNo_f.value)){
		return false;
	} else if(form1.StudentBirthCertNo_m.value==''){
		return false;
	} else if(!/^[0-9]{6}$/.test(form1.StudentBirthCertNo_m.value)){
		return false;
	} else if(form1.StudentBirthCertNo_l.value==''){
		return false;
	} else if(!/^[a-zA-Z0-9]$/.test(form1.StudentBirthCertNo_l.value)){
		return false;
	} else if(form1.StudentBirthCertNo.value=='' || checkBirthCertNo() > 0){
		return false;
	} else if(form1.StudentPlaceOfBirth.value==''){
		return false;
	} else if(form1.StudentHomePhoneNo.value==''){
		return false;
	} else if(form1.LangSpokenAtHome.value==''){
		return false;
	} else if(form1.StudentContactPerson.value==''){
		return false;
	} else if(form1.StudentRelationship.value==''){
		return false;
	} else if(form1.StudentHomeAddress.value==''){
		return false;
	} else if(form1.StudentEmail.value==''){
		return false;
	} else if(!re.test(form1.StudentEmail.value)){
		return false;
	} else if(form1.StudentPersonalPhoto.value==''){
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		return false;
	} else if(studentPhotoSize > maxFileSize){
		return false;
	} else if(form1.G1EnglishName.value=='' && form1.G2EnglishName.value=='' && form1.G3EnglishName.value==''){
			return false;
	} else if(form1.G1EnglishName.value!='' || form1.G1Occupation.value!='' || form1.G1CompanyNo.value!='' && form1.G1MobileNo.value!=''){
		if(form1.G1EnglishName.value==''){
			return false;
		}
		else if(form1.G1Occupation.value==''){
			return false;
		}
		else if(form1.G1CompanyNo.value=='' && form1.G1MobileNo.value==''){
			return false;
		}
	} if(form1.G2EnglishName.value!='' || form1.G2Occupation.value!='' || form1.G2CompanyNo.value!='' && form1.G2MobileNo.value!=''){
		if(form1.G2EnglishName.value==''){
			return false;
		}
		else if(form1.G2Occupation.value==''){
			return false;
		}
		else if(form1.G2CompanyNo.value=='' && form1.G2MobileNo.value==''){
			return false;
		}
	} if(form1.G3Relationship.value!='' || form1.G3EnglishName.value!='' || form1.G3Occupation.value!='' || form1.G3CompanyNo.value!='' && form1.G3MobileNo.value!=''){
		//hasPG = 0;
		if(form1.G3EnglishName.value==''){
			return false;
		}
		else if(form1.G3Relationship.value==''){
			return false;
		}
		else if(form1.G3Occupation.value==''){
			return false;
		}
		else if(form1.G3CompanyNo.value=='' && form1.G3MobileNo.value==''){
			return false;
		}
	}
	
	if($.trim(form1.G1EnglishName.value)!='' && !english.test(form1.G1EnglishName.value.replace(/\s+/g, ''))){
		return false;
	}
	else if($.trim(form1.G2EnglishName.value)!='' && !english.test(form1.G2EnglishName.value.replace(/\s+/g, ''))){
		return false;
	}
	else if($.trim(form1.G3EnglishName.value)!='' && !english.test(form1.G3EnglishName.value.replace(/\s+/g, ''))){
		return false;
	}
		
	 if(form1.OthersFamilyStatus_EB.value==''){
		return false;
	} else if(form1.OthersFamilyStatus_ES.value==''){
		return false;
	} else if(form1.OthersFamilyStatus_YB.value==''){
		return false;
	} else if(form1.OthersFamilyStatus_YS.value==''){
		return false;
	} else if(form1.OthersApplyMonth.value==''){
		return false;
	} else if($('input:radio[name=OthersApplyTerm]').length > 0 && $('input:radio[name=OthersApplyTerm]:checked').val() == null){
		return false;
	} else if(form1.OthersApplyDayType2 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value ==""){
		return false;
	} else if($('input:radio[name=OthersNeedSchoolBus]:checked').val() == null){
		return false;
	} else if($('input:radio[name=OthersNeedSchoolBus]:checked').val() == '1'){
		if(form1.OthersSchoolBusPlace.value==''){
			return false;
		}
	} if($('input:radio[name=OthersKnowUsBy]:checked').val() == null){
		return false;
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '5' && form1.txtOthersKnowUsBy5.value==''){
		return false;			
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '6' && form1.txtOthersKnowUsBy6.value==''){
		return false;			
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '7' && form1.txtOthersKnowUsBy7.value==''){
		return false;			
	} 
		return true;

}
/*
function check_docs_upload2(form1) {

	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		return false;
	} else if(otherFileSize > maxFileSize){
		return false;
	} else if(form1.OtherFile1.value==''){
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		return false;
	} else if(otherFileSize1 > maxFileSize){
		return false;
	} else  {
		return true;
	}
}
*/
function check_docs_upload2(form1) {
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
		var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		
		if(otherFileVal==''){
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(otherFileSize > maxFileSize){
			return false;
		}
	}
	
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
			var chk_ary = $('input[type=checkbox]');
			var chk_count = chk_ary.length;
			for(var i=0;i<chk_count;i++)
			{
				var chk_element = chk_ary.get(i);
				if(chk_element.checked == false){
					alert("請剔選 我已閱讀並理解以上資料，已準備繼續進行網上報名程序。");
					isValid = false;
				}
			}
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		   $("#TermTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_getSchoolIDsByClassLevelID.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#SchoolYearOption").html(data);
		           document.getElementById('SchoolYearID').onchange = function () {var values = $("#form1").serialize();updateDayTypeOption(values);updateTermTypeOption(values)};
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
			   clearTimeout(timer);
			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		   $("#TermTypeOption").html('');
		   		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
		   
		   
		   
		    /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_class_selection.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#DayTypeOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		   $.ajax({
		       url: "ajax_get_term_selection.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#TermTypeOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
		   
	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){
	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	$("input[type=submit]").attr('disabled',true);
	if(confirm('<?=$Lang['Admission']['msg']['suresubmit'].'\nAre you sure you want to submit?'?>')){
		return true;
	}
	else{
		$("input[type=submit]").attr('disabled',false);
		return false;
	}
}

function check_choose_class(form1) {

	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>");
				return false;
			}
		}
	<?}?>
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass'].'\nPlease select Class.'?>");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	//For debugging only
	//return true;
	
//	form1.hidden_data.value = $("form").serialize();
//	form1.hidden_file.value = form1.StudentPersonalPhoto.files[0];
//	alert(form1.hidden_data.value);
	
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	
	/* get the birthday range of the form level */
	var values = $("#form1").serialize();
   $.ajax({
       url: "ajax_get_bday_range.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
           dOBRange = data.split(",");
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
	
	var english = /^[A-Za-z0-9]*$/;
	var re = /\S+@\S+\.\S+/;
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
	if(form1.StudentPersonalPhoto.files[0]){
	var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
	var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	//var hasPG = 1;
	
	//Assign the birth cert no parts to hidden field
	form1.StudentBirthCertNo.value = form1.StudentBirthCertNo_f.value.toUpperCase() + form1.StudentBirthCertNo_m.value +'('+form1.StudentBirthCertNo_l.value+')';
	
	if(form1.StudentChiName.value==''){
		alert("<?=$Lang['Admission']['msg']['enterchinesename'].'\nPlease enter Chinese Name.'?>");	
		form1.StudentChiName.focus();
		return false;
	} else if(form1.StudentEngName.value==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname'].'\nPlease enter English Name.'?>");
		form1.StudentEngName.focus();
		return false;
	} else if(!english.test(form1.StudentEngName.value.replace(/\s+/g, ''))){
		alert("<?=$Lang['Admission']['msg']['ChildEnglishNameFormat'].'\nPlease enter English only in English Name.'?>");	
		form1.StudentEngName.focus();
		return false;
	//} else if(form1.StudentDateOfBirth.value==''  || studentDateOfBirth >= today){
	//} else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
	} else if(!check_date_without_return_msg(form1.StudentDateOfBirth)){
		if(form1.StudentDateOfBirth.value!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat'].'\nInvalid Date Format'?>");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth'].'\nPlease enter Date of Birth.'?>");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>
	else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat'].'\nInvalid Birthday Range of Student'?>");
		form1.StudentDateOfBirth.focus();
		return false;
	} <?}?>
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectgender'].'\nPlease select Gender.'?>");	
		form1.StudentGender[0].focus();
		return false;
	} else if(form1.StudentBirthCertNo_f.value==''){
		alert("<?=$Lang['Admission']['msg']['enterbirthcertificatenumber'].'\nPlease enter Birth Certificate Number.'?>");	
		form1.StudentBirthCertNo_f.focus();
		return false;
	} else if(!/^[a-zA-Z]$/.test(form1.StudentBirthCertNo_f.value)){
		alert("<?=$Lang['Admission']['msg']['invalidbirthcertificatenumber'].'\nInvalid Birth Certificate Number.'?>");	
		form1.StudentBirthCertNo_f.focus();
		return false;
	} else if(form1.StudentBirthCertNo_m.value==''){
		alert("<?=$Lang['Admission']['msg']['enterbirthcertificatenumber'].'\nPlease enter Birth Certificate Number.'?>");	
		form1.StudentBirthCertNo_m.focus();
		return false;
	} else if(!/^[0-9]{6}$/.test(form1.StudentBirthCertNo_m.value)){
		alert("<?=$Lang['Admission']['msg']['invalidbirthcertificatenumber'].'\nInvalid Birth Certificate Number.'?>");	
		form1.StudentBirthCertNo_m.focus();
		return false;
	} else if(form1.StudentBirthCertNo_l.value==''){
		alert("<?=$Lang['Admission']['msg']['enterbirthcertificatenumber'].'\nPlease enter Birth Certificate Number.'?>");	
		form1.StudentBirthCertNo_l.focus();
		return false;
	} else if(!/^[a-zA-Z0-9]$/.test(form1.StudentBirthCertNo_l.value)){
		alert("<?=$Lang['Admission']['msg']['invalidbirthcertificatenumber'].'\nInvalid Birth Certificate Number.'?>");		
		form1.StudentBirthCertNo_l.focus();
		return false;
	/*} else if(form1.StudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['msg']['enterbirthcertificatenumber']?>");	
		form1.StudentBirthCertNo.focus();
		return false;*/
	} else if(checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['msg']['duplicatebirthcertificatenumber'].'\nThe Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.'?>");	
		form1.StudentBirthCertNo_f.focus();
		return false;
	} else if(form1.StudentPlaceOfBirth.value==''){
		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth'].'\nPlease enter Place of Birth.'?>");	
		form1.StudentPlaceOfBirth.focus();
		return false;
	} else if(form1.StudentHomePhoneNo.value==''){
		alert("<?=$Lang['Admission']['msg']['enterstudenthomephoneno'].'\nPlease enter Home Phone No.'?>");	
		form1.StudentHomePhoneNo.focus();
		return false;
	} else if(form1.LangSpokenAtHome.value==''){
		alert("<?=$Lang['Admission']['icms']['msg']['enterlangspokenathome'].'\nPlease Enter Languages Spoken at Home.'?>");	
		form1.LangSpokenAtHome.focus();
		return false;
	} else if(form1.StudentContactPerson.value==''){
		alert("<?=$Lang['Admission']['msg']['enterstudentcontactperson'].'\nPlease enter Content Person.'?>");	
		form1.StudentContactPerson.focus();
		return false;
	} else if(form1.StudentRelationship.value==''){
		alert("<?=$Lang['Admission']['msg']['enterstudentrelationship'].'\nPlease enter Relationship of Student.'?>");	
		form1.StudentRelationship.focus();
		return false;
	} else if(form1.StudentHomeAddress.value==''){
		alert("<?=$Lang['Admission']['msg']['enterhomeaddress'].'\nPlease enter Home Address.'?>");	
		form1.StudentHomeAddress.focus();
		return false;
	} else if(form1.StudentEmail.value==''){
		alert("<?=$Lang['Admission']['msg']['enterstudentemail'].'\nPlease enter Contact Email.'?>");	
		form1.StudentEmail.focus();
		return false;
	} else if(!re.test(form1.StudentEmail.value)){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress'].'\nInvalid Email Format'?>");
		form1.StudentEmail.focus();
		return false;
	} else if(form1.StudentPersonalPhoto.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto'].'\nPlease upload a personal photo.'?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat'].'\nInvalid File Format'?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit'].'\nFile size exceeds limit.'?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(form1.G1EnglishName.value=='' && form1.G2EnglishName.value=='' && form1.G3EnglishName.value==''){
			alert("<?=$Lang['Admission']['msg']['enteratleastoneparent'].'\nPlease enter at least one parent information.'?>");
			form1.G1EnglishName.focus();
			return false;
	} else if(form1.G1EnglishName.value!='' || form1.G1Occupation.value!='' || form1.G1CompanyNo.value!='' && form1.G1MobileNo.value!=''){
		//hasPG = 0;
		if(form1.G1EnglishName.value==''){
			alert("<?=$Lang['Admission']['msg']['enterenglishname'].'\nPlease enter English Name.'?>");
			form1.G1EnglishName.focus();
			return false;
		}
		else if(form1.G1Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation'].'\nPlease enter Occupation.'?>");
			form1.G1Occupation.focus();
			return false;
		}
		else if(form1.G1CompanyNo.value=='' && form1.G1MobileNo.value==''){
			alert("<?=$Lang['Admission']['msg']['enteratleastonephoneno'].'\nPlease enter at least one phone number.'?>");
			form1.G1CompanyNo.focus();
			return false;
		}
	} if(form1.G2EnglishName.value!='' || form1.G2Occupation.value!='' || form1.G2CompanyNo.value!='' && form1.G2MobileNo.value!=''){
		//hasPG = 0;
		if(form1.G2EnglishName.value==''){
			alert("<?=$Lang['Admission']['msg']['enterenglishname'].'\nPlease enter English Name.'?>");
			form1.G2EnglishName.focus();
			return false;
		}
		else if(form1.G2Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation'].'\nPlease enter Occupation.'?>");
			form1.G2Occupation.focus();
			return false;
		}
		else if(form1.G2CompanyNo.value=='' && form1.G2MobileNo.value==''){
			alert("<?=$Lang['Admission']['msg']['enteratleastonephoneno'].'\nPlease enter at least one phone number.'?>");
			form1.G2CompanyNo.focus();
			return false;
		}
	} if(form1.G3Relationship.value!='' || form1.G3EnglishName.value!='' || form1.G3Occupation.value!='' || form1.G3CompanyNo.value!='' && form1.G3MobileNo.value!=''){
		//hasPG = 0;
		if(form1.G3EnglishName.value==''){
			alert("<?=$Lang['Admission']['msg']['enterenglishname'].'\nPlease enter English Name.'?>");
			form1.G3EnglishName.focus();
			return false;
		}
		else if(form1.G3Relationship.value==''){
			alert("<?=$Lang['Admission']['msg']['enterrelationship'].'\nPlease enter Relationship.'?>");
			form1.G3Relationship.focus();
			return false;
		}
		else if(form1.G3Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation'].'\nPlease enter Occupation.'?>");
			form1.G3Occupation.focus();
			return false;
		}
		else if(form1.G3CompanyNo.value=='' && form1.G3MobileNo.value==''){
			alert("<?=$Lang['Admission']['msg']['enteratleastonephoneno'].'\nPlease enter at least one phone number.'?>");
			form1.G3CompanyNo.focus();
			return false;
		}
	}
//	if(hasPG == 0){
//		alert("<?=$Lang['Admission']['msg']['enteratleastoneparent']?>");
//		form1.G1EnglishName.focus();
//		return false;
//	}
	
	if($.trim(form1.G1EnglishName.value)!='' && !english.test(form1.G1EnglishName.value.replace(/\s+/g, ''))){
		alert("<?=$Lang['Admission']['msg']['ChildEnglishNameFormat'].'\nPlease enter English only in English Name.'?>");
		form1.G1EnglishName.focus();
		return false;
	}
	else if($.trim(form1.G2EnglishName.value)!='' && !english.test(form1.G2EnglishName.value.replace(/\s+/g, ''))){
		alert("<?=$Lang['Admission']['msg']['ChildEnglishNameFormat'].'\nPlease enter English only in English Name.'?>");
		form1.G2EnglishName.focus();
		return false;
	}
	else if($.trim(form1.G3EnglishName.value)!='' && !english.test(form1.G3EnglishName.value.replace(/\s+/g, ''))){
		alert("<?=$Lang['Admission']['msg']['ChildEnglishNameFormat'].'\nPlease enter English only in English Name.'?>");	
		form1.G3EnglishName.focus();
		return false;
	}
	
	 if(form1.OthersFamilyStatus_EB.value==''){
		alert("<?=$Lang['Admission']['msg']['enterelderbrother'].'\nPlease enter the no. of elder brother.'?>");
		form1.OthersFamilyStatus_EB.focus();
		return false;
	} else if(form1.OthersFamilyStatus_ES.value==''){
		alert("<?=$Lang['Admission']['msg']['entereldersister'].'\nPlease enter the no. of elder sister.'?>");
		form1.OthersFamilyStatus_ES.focus();
		return false;
	} else if(form1.OthersFamilyStatus_YB.value==''){
		alert("<?=$Lang['Admission']['msg']['enteryoungerbrother'].'\nPlease enter the no. of younger brother.'?>");
		form1.OthersFamilyStatus_YB.focus();
		return false;
	} else if(form1.OthersFamilyStatus_YS.value==''){
		alert("<?=$Lang['Admission']['msg']['enteryoungersister'].'\nPlease enter the no. of younger sister.'?>");
		form1.OthersFamilyStatus_YS.focus();
		return false;
//	} else if(form1.OthersApplyYear.value==''){
//		alert("<?= $i_alert_pleasefillin?> Invalid Year");
//		form1.OthersApplyYear.focus();
//		return false;
	} else if(form1.OthersApplyMonth.value==''){
		alert("<?=$Lang['Admission']['msg']['selectApplyMonth'].'\nPlease select apply month.'?>");
		form1.OthersApplyMonth.focus();
		return false;
	} else if($('input:radio[name=OthersApplyTerm]').length > 0 && $('input:radio[name=OthersApplyTerm]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['enterapplyTerm'].'\nPlease enter Apply Term.'?>");
		if($('input:radio[name=OthersApplyTerm]').length > 1)
			form1.OthersApplyTerm[0].focus();
		else
			form1.OthersApplyTerm.focus();
		return false;
//	} else if(form1.OthersApplyDayType1.value != null && form1.OthersApplyDayType1.value == ""){
//		alert("<?= $i_alert_pleasefillin?> OthersApplyDayType1");
//		form1.OthersApplyDayType1.focus();
//		return false;
//	} else if(form1.OthersApplyDayType2.value != null && form1.OthersApplyDayType2.value == ""){
//		alert("<?= $i_alert_pleasefillin?> OthersApplyDayType2");
//		form1.OthersApplyDayType2.focus();
//		return false;
//	} else if(form1.OthersApplyDayType3.value != null && form1.OthersApplyDayType3.value == ""){
//		alert("<?= $i_alert_pleasefillin?> OthersApplyDayType3");
//		form1.OthersApplyDayType3.focus();
//		return false;
	} else if(form1.OthersApplyDayType2 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value ==""){
		//else if(form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value){
		alert("<?=$Lang['Admission']['msg']['applyDayTypeHints'].'\nPlease choose your priorities(at least two), blank if not consider'?>");
		form1.OthersApplyDayType1.focus();
		return false;
	} else if($('input:radio[name=OthersNeedSchoolBus]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['enterneedschoolbus'].'\nPlease select \'Yes\' or \'No\' for \'Require School Bus Service\' field.' ?>");
		form1.OthersNeedSchoolBus[0].focus();
		return false;
	} else if($('input:radio[name=OthersNeedSchoolBus]:checked').val() == '1'){
		if(form1.OthersSchoolBusPlace.value==''){
			alert("<?=$Lang['Admission']['msg']['enterplacefortakingschoolbus'].'\nPlease enter Place for Taking School Bus.'?>");
			form1.OthersSchoolBusPlace.focus();
			return false;
		}
	} if($('input:radio[name=OthersKnowUsBy]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectknowusby'].'\nPlease select how you learn of our school.'?>");
		form1.OthersKnowUsBy[0].focus();
		return false;
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '5' && form1.txtOthersKnowUsBy5.value==''){
		alert("<?=$Lang['Admission']['msg']['enterknowusby'].'\nPlease enter how you learn of our school.'?>");
		form1.txtOthersKnowUsBy5.focus();
		return false;			
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '6' && form1.txtOthersKnowUsBy6.value==''){
		alert("<?=$Lang['Admission']['msg']['enterknowusby'].'\nPlease enter how you learn of our school.'?>");
		form1.txtOthersKnowUsBy6.focus();
		return false;			
	} else if($('input:radio[name=OthersKnowUsBy]:checked').val() == '7' && form1.txtOthersKnowUsBy7.value==''){
		alert("<?=$Lang['Admission']['msg']['enterknowusby'].'\nPlease enter how you learn of our school.'?>");
		form1.txtOthersKnowUsBy7.focus();
		return false;			
	} else if(!form1.SchoolYearID){
		alert("請選擇報讀學年。\nPlease select School Year.");
		return false;
	} 
	//else {
		return true;
	//}
}
/*
function check_docs_upload(form1) {
	//For debugging only
	//return true;
	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadbirthcert']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(form1.OtherFile1.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadimmunisationrecord']?>");
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileSize1 > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else  {
		return true;
	}
}
*/
function check_docs_upload(form1) {
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
		var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		
		if(otherFileVal==''){
			alert("<?=$Lang['Admission']['msg']['uploadfile'].'\nPlease upload file.'?>");
			file_element.focus();
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat'].'\nInvalid File Format'?>");
			file_element.focus();
			return false;
		} else if(otherFileSize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit'].'\nFile size exceeds limit.'?>");	
			file_element.focus();
			return false;
		}
	}
	
	return true;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}


function updateDayTypeOption(values){
	/* Send the data using post and put the results in a div */
	   $.ajax({
	       url: "ajax_get_class_selection.php",
	       type: "post",
	       data: values,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           $("#DayTypeOption").html(data);
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   return true;
}

function updateTermTypeOption(values){
	/* Send the data using post and put the results in a div */
	   $.ajax({
	       url: "ajax_get_term_selection.php",
	       type: "post",
	       data: values,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           $("#TermTypeOption").html(data);
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   return true;
}

function check_date_without_return_msg(obj){
        var err = 0;
        d_a = obj.value;
        if (d_a.length != 10) err = 1;
        // commented by kenneth chung 2010-04-09, to prevent chinese character pass through to checking
        /*d_b = parseInt(d_a.substring(0, 4),10)        // year
        d_c = d_a.substring(4, 5)                                // '-'
        d_d = parseInt(d_a.substring(5, 7),10)        // month
        d_e = d_a.substring(7, 8)                                // '-'
        d_f = parseInt(d_a.substring(8, 10),10)        // day*/
        d_b = d_a.substring(0, 4);        // year
        d_c = d_a.substring(4, 5);                                // '-'
        d_d = d_a.substring(5, 7);        // month
        d_e = d_a.substring(7, 8);                                // '-'
        d_f = d_a.substring(8, 10);        // day
        //alert(d_f);
        // basic error checking
        if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
        if(d_c != '-') err = 1;
        if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
        if(d_e != '-') err = 1;
        if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
        // advanced error checking
        // months with 30 days
        if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
        // february, leap year
        if(d_d==2){ // feb
                var d_g = parseInt(d_b/4)
                if(isNaN(d_g)) err = 1;
                if(d_f>29) err = 1;
                if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
        }
        if(err==1){
                return false;
        }else{
                return true;
        }
}

//function startUpload(){
//      document.getElementById('f1_upload_process').style.visibility = 'visible';
//      document.getElementById('f1_upload_form').style.visibility = 'hidden';
//      return true;
//}
//Henry modifying 20131028
//function stopUpload(temp_folder_name){
//		document.getElementById('tempFolderName').value = temp_folder_name;
//		
//		 var values = $("#form1").serialize();
//			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
//			var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
//			var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
//			values+=studentPersonalPhoto;
//			values+=otherFile;
//			values+=otherFile1;
//		$.ajax({
//		       url: "ajax_get_confirm.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!"+values);
//		           $("#step_confirm").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });     
//      return true;  
//}
alert("請先細閱以下申請指引，以確認申請過程無誤。");
</script>