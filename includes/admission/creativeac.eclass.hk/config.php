<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(愛琴)';
$admission_cfg['SchoolName']['en'] .= ' (Aegean Coast)';
$admission_cfg['SchoolCode'] = 'AC';
$admission_cfg['SchoolPhone'] = '2949 5028';
$admission_cfg['SchoolAddress']['b5'] = '新界屯門青山公路掃管笏段管青路2號愛琴海岸1樓幼稚園';
$admission_cfg['SchoolAddress']['en'] = 'Kindergarten on 1/F., Aegean Coast, 2 Kwun Tsing Road, So Kwun Wat, Tuen Mun, New Territories';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckac.photo@gmail.com';
}
/* for email [end] */

/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = 'QGV5GTHNCL9DY';
} else {
    $admission_cfg['paypal_signature'] = 'Bf3f936kya9i36dyW_4lZVzQ3rpkrPXidVRrHH9uomaykFjk2BRwEZjDw_0';
    $admission_cfg['hosted_button_id'] = '5PKG6YCF962QS';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Aegean Coast)';
}
/* for paypal [End] */