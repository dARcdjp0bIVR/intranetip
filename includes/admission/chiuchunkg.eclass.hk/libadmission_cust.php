<?php
# modifying by: 

/********************
 * 
 * Log :
 * Date		2015-11-10 [Omas]
 * 			modified getExportHeader(), getExportData() add column
 * Date		2015-10-09 [Henry]
 * 			modified newApplicationNumber2()
 * Date		2015-10-05 [Henry]
 * 			modified getApplicationDetails()
 * Date		2015-07-15 [Omas]
 * 			added updateFamilyInfo()
 * Date		2015-06-30 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_cust_base.php");

class admission_cust extends admission_cust_base{
	function admission_cust(){
		global $kis_lang, $UserID; //switch $lang for IP/EJ/KIS
		
		$this->libdb();
		$this->filepath = '/file/admission/';
		$this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
		$this->schoolYearID = $this->getNextSchoolYearID();
		$this->uid = $UserID;
		$this->classLevelAry = $this->getClassLevel();
	}
	
	function testing(){
		return "test";
	}
	function encrypt_attachment($file){
		list($filename,$ext) = explode('.',$file);
	    $timestamp = date("YmdHis");
	    return base64_encode($filename.'_'.$timestamp).'.'.$ext;
	}
	function getNextSchoolYearID(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		$SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField=array('AcademicYearID'),1);
		krsort($SchoolYearIDArr);
		
		$SchoolYearIDArr = array_values($SchoolYearIDArr);
		$currentSchoolYear = Get_Current_Academic_Year_ID();
		$key = array_search($currentSchoolYear, $SchoolYearIDArr);
		if($key>0){
    		return $SchoolYearIDArr[$key-1];
    	}else{
    		return false;
    	}
	}
	function insertApplicationAllInfo($libkis_admission, $Data, $ApplicationID){
		extract($Data);
		$Success = array();
		if($ApplicationID != ""){
			
			$Success[] = $this->insertApplicationStatus($libkis_admission, $ApplicationID);
			
			$Success[] = $this->insertApplicationStudentInfo($Data, $ApplicationID);
			
			
			//Pack Parent Info 
			foreach($this->pg_type as $_key => $_pgType){
				if(!empty($ApplicationID)/*&&!empty(${'G'.($_key+1).'EnglishName'})*/){
					$parentInfoAry = array();
					$parentInfoAry['ApplicationID'] = $ApplicationID;
					$parentInfoAry['PG_TYPE'] = $_pgType;
					//$parentInfoAry['lsSingleParents'] = $IsSingleParent;
					//$parentInfoAry['HasFullTimeJob'] = $IsFullTime;
					//$parentInfoAry['IsFamilySpecialCase'] = $IsFamilySpecialCase;
					//$parentInfoAry['IsApplyFullDayCare'] = $IsApplyFullDayCare;
					//$parentInfoAry['IsLiveWithChild'] = ${'G'.($_key+1).'LiveWithChild'};
					//$parentInfoAry['Relationship'] = ${'G'.($_key+1).'Relationship'};
					//$parentInfoAry['EnglishName'] = ${'G'.($_key+1).'EnglishName'};
					$parentInfoAry['ChineseName'] = ${'G'.($_key+1).'ChineseName'};
					//$parentInfoAry['HKID'] = ${'G'.($_key+1).'HKID'};
					$parentInfoAry['JobTitle'] = ${'G'.($_key+1).'Occupation'};
					//$parentInfoAry['Company'] = ${'G'.($_key+1).'CompanyName'};
					//$parentInfoAry['JobPosition'] = ${'G'.($_key+1).'JobPosition'};
					//$parentInfoAry['OfficeAddress'] = ${'G'.($_key+1).'CompanyAddress'};
					$parentInfoAry['OfficeTelNo'] = ${'G'.($_key+1).'WorkNo'};
					$parentInfoAry['Email'] = ${'G'.($_key+1).'Email'};
					$parentInfoAry['Relationship'] = ($_pgType=='G'?$G3Relationship:$StudentRelationship);	
					$parentInfoAry['Gender'] = ($_pgType=='G'?$G3Gender:'');
					$parentInfoAry['Mobile'] = ${'G'.($_key+1).'MobileNo'};	
					//$parentInfoAry['LevelOfEducation'] = ${'G'.($_key+1).'LevelOfEducation'};	
					//$parentInfoAry['LastSchool'] = ${'G'.($_key+1).'LastSchool'};	
					$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
				}
				
 			}
			/*if($G1EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 1);
				
			if($G2EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 2);
				
			if($G3EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 3);*/
			
			$Success[] = $this->insertApplicationStudentInfoCust($Data, $ApplicationID);
			
			$Success[] = $this->insertApplicationRelativesInfoCust($Data, $ApplicationID);
			
			$Success[] = $this->insertApplicationOthersInfo($Data, $ApplicationID);
			
			if(in_array(false,$Success)){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	function insertApplicationStatus($libkis_admission, $ApplicationID){
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){

//   		}
//   		else{
   			//Henry: not yet finish
   			$sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
     					SchoolYearID,
     					ApplicationID,
					    Status,
					    DateInput)
					VALUES (
						'".$this->schoolYearID."',
						'".$ApplicationID."',
						'1',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	function updateApplicationStatus($data){
		global $UserID;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
				s.ReceiptID = '".$receiptID."',
     			s.ReceiptDate = '".$receiptdate."',
      			s.Handler = '".$handler."', 
      			s.InterviewDate = '".$interviewdate."',
				s.InterviewLocation = '".$interviewlocation."',
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."' 
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}  
	function updateApplicationStatusByIds($applicationIds,$status){
		global $UserID;
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.RecordID IN (".$applicationIds.")
    	";
    	return $this->db_db_query($sql);
	}	  	
   	function insertApplicationStudentInfo($Data, $ApplicationID){
   		extract($Data);
   		global $admission_cfg;
   		
		if($FormStudentInfo_DOB != ''){
			$yearApply = date('Y-m',getStartOfAcademicYear('',$this->getNextSchoolYearID())); 
			$timestamp_DOB = strtotime($FormStudentInfo_DOB);
			$timestamp_School = strtotime($yearApply.'-01');
			$timeDiff = $timestamp_School - $timestamp_DOB;
			$age = $timeDiff/(24*60*60)/365;
			$ageArr = explode('.',$age);
			$month = '0.'.$ageArr[1];
			$monthArr = explode('.', 12 * $month);
			$Handled_Age = $ageArr[0].'|||'.$monthArr[0];
		}
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_STU_INFO (
						ApplicationID,
		   				 ChineseName,
		  				 EnglishName,   
		   				 Gender,
		  				 DOB,
						 Age,
						 BirthCertType,
						 BirthCertTypeOther,
		   				 BirthCertNo,
		   				 PlaceOfBirth,
						 PlaceOfBirthOther,
						 Province,
						 County,
						 HomeTelNo,
						 Address,
						 AddressChi,
						 ContactAddress,
						 ContactAddressChi,
						 ReligionCode,
						 ReligionOther,
						 Church,
						 IsTwins,
						 TwinsApplicationID,
						 AddressDistrict,
						 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$studentssurname_b5.','.$studentsfirstname_b5."',
						'".$studentssurname_en.','.$studentsfirstname_en."',
						'".$FormStudentInfo_StuGender."',
						'".$FormStudentInfo_DOB."',
						'".$Handled_Age."',
						'".$FormStudentInfo_StuIDType."',
						'".$FormStudentInfo_StuIDType_Other."',
						'".$StudentBirthCertNo."',
						'".$FormStudentInfo_POB."',
						'".$FormStudentInfo_POB_Other."',
						'".$FormStudentInfo_Province."',
						'".$FormStudentInfo_County."',
						'".$FormStudentInfo_HomeTel.'|||'.$FormStudentInfo_ContactTel."',
						'".$FormStudentInfo_HomeAddrEng."',
						'".$FormStudentInfo_HomeAddrChi."',
						'".$FormStudentInfo_ContactAddrEng."',
						'".$FormStudentInfo_ContactAddrChi."',
						'".$FormStudentInfo_Religion."',
						'".$FormStudentInfo_Religion_Other."',
						'".$FormStudentInfo_Church."',
						'".$IsTwins."',
						'".$TwinsIDNo."',
						'".$HomeAddressDistrict.'|||'.$ContactAddressDistrict."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
    }
    
    function insertApplicationParentInfo($Data){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
	
		$sql = "INSERT INTO ADMISSION_PG_INFO (
					".$fieldname."
					DateInput
				)VALUES (
					".$fieldvalue."
					NOW()
				)";
		return $this->db_db_query($sql);
    }
     function updateApplicationParentInfo($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_PG_INFO SET
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		return $this->db_db_query($sql);
    }
    
     function updateApplicationStudentInfoCust($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_STU_PREV_SCHOOL_INFO
				SET 
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		$success[] = $this->db_db_query($sql);
				
		if($Data['Year'] == '' && $Data['Class'] =='' && $Data['NameOfSchool'] ==''){
			$sql = "DELETE FROM ADMISSION_STU_PREV_SCHOOL_INFO WHERE RecordID = '".$Data['RecordID']."'";
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
    }
    
    function updateApplicationRelativesInfoCust($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_RELATIVES_AT_SCH_INFO
				SET 
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		$success[] = $this->db_db_query($sql);
				
		if($Data['Year'] == '' && $Data['Name'] =='' && $Data['ClassPosition'] =='' && $Data['Relationship'] ==''){
			$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE RecordID = '".$Data['RecordID']."'";
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
    }
       
    function insertApplicationOthersInfo($Data, $ApplicationID){    	
   		extract($Data);
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){
			
//   		}
//   		else{
   			//Henry: not yet finish
//   			$ApplyDayType1 = '';
//   			$ApplyDayType2 = '';
//   			$ApplyDayType3 = '';
//   			for($i=1;$i<=3;$i++){
//   				if(${'OthersApplyDayType'.$i} == 1){
//   					$ApplyDayType1 = $i;
//   				}
//   				else if(${'OthersApplyDayType'.$i} == 2){
//   					$ApplyDayType2 = $i;
//   				}
//   				else if(${'OthersApplyDayType'.$i} == 3){
//   					$ApplyDayType3 = $i;
//   				}
//   			}
			$sql = "UPDATE 
					ADMISSION_OTHERS_INFO 
				SET
					 ApplyYear = '".$this->schoolYearID."',
					 ApplyDayType1 = '".$ApplyClass."',
					 ApplyLevel = '".$sus_status."',
					 Token = '".$token."',
					 EBrotherNo = '".$NumElderBro."',
					 ESisterNo = '".$NumElderSis."',
					 YBrotherNo = '".$NumYoungBro."',
					 YSisterNo = '".$NumYoungSis."',
				     DateInput = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'";
					
			if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
   			
	   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
							ApplicationID,
						     ApplyYear,	   
						     ApplyDayType1,
						     ApplyDayType2,
						     ApplyDayType3,
						     ApplyLevel,
							 Token,
							 IsConsiderAlternative,
							 SiblingAppliedName,
							 ExBSName,
							 ExBSName2,
							 ExBSName3,
							 ExBSGradYear,
							 ExBSGradYear2,
							 ExBSGradYear3,
							 ExBSRelation,
							 ExBSRelation2,
							 ExBSRelation3,
					  		 EBrotherNo,
							 ESisterNo,
							 YBrotherNo,
							 YSisterNo,
						     DateInput
						     )
						VALUES (
							'".$ApplicationID."',
							'".$this->schoolYearID."',
							'".$OthersApplyDayType1."',
							'".$OthersApplyDayType2."',
							'".$OthersApplyDayType3."',
							'".$sus_status."',
							'".$token."',
							'".$OthersIsConsiderAlternative."',
							'".$OthersSiblingAppliedName."',
							'".$OthersExBSName."',
							'".$OthersExBSName2."',
							'".$OthersExBSName3."',
							'".$OthersExBSGradYear."',
							'".$OthersExBSGradYear2."',
							'".$OthersExBSGradYear3."',
							'".$OthersRelativeRelationship1."',
							'".$OthersRelativeRelationship2."',
							'".$OthersRelativeRelationship3."',
							'".$NumElderBro."',
							'".$NumElderSis."',
							'".$NumYoungBro."',
							'".$NumYoungSis."',
							now())
				";
				return $this->db_db_query($sql);
			}
			else 
				return true;
   		}
   		return false;
    }
    
    function insertApplicationStudentInfoCust($Data, $ApplicationID){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			$sql = "INSERT INTO ADMISSION_STU_PREV_SCHOOL_INFO (
								ApplicationID,
				  				Class,
								ClassType,    
				   				NameOfSchool,
								SchoolAddress,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".$CurrentSchoolClassLv."',
								'".$CurrentSchoolDayType."',
								'".$CurrentSchoolName."',
								'".$CurrentSchoolAddr."',
								now())
					";
			$success[] = $this->db_db_query($sql);
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID applicationID,
     			s.Year OthersPrevSchYear,		    					
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName,
				s.SchoolAddress ,
				s.ClassType
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

    	return $this->returnArray($sql);
	}	
    
    function insertApplicationRelativesInfoCust($Data, $ApplicationID){
   		extract($Data);
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			for($i=1; $i <= 5; $i++){
   				if(${'OthersRelativeStudiedYear'.$i} != '' || ${'OthersRelativeStudiedName'.$i} !='' || ${'OthersRelativeClassPosition'.$i} !='' || ${'OthersRelativeRelationship'.$i} !=''){
		   			$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				   				Year,
				  				Name,   
								Relationship,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".${'OthersRelativeStudiedYear'.$i}."',
								'".${'OthersRelativeStudiedName'.$i}."',
								'".${'OthersRelativeRelationship'.$i}."',
								now())
					";
					
					$success[] = $this->db_db_query($sql);
   				}
   			}
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function getApplicationRelativesInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year OthersRelativeStudiedYear,
				r.Name OthersRelativeStudiedName,
				r.Relationship OthersRelativeRelationship 
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

    	return $this->returnArray($sql);
	}	
    
	function updateApplicationOtherInfo($data){    	
		global $UserID,$admission_cfg,$sys_custom;
   		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;

		$sql = "UPDATE 
					ADMISSION_OTHERS_INFO 
				SET ";
				
				$ApplyDayTypeAry = array();
				for($i=0;$i<3;$i++){
					if(!empty($ApplyDayType[$i])){
						$ApplyDayTypeAry[] = $ApplyDayType[$i];
					}
				} 
				for($i=0;$i<3;$i++){
					$sql .= "ApplyDayType".($i+1)." = '".$ApplyDayTypeAry[$i]."',";
				}     
		    $sql .= "IsConsiderAlternative = '".$OthersIsConsiderAlternative."',
				     DateModified = NOW(),
				     ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$recordID."' AND ApplyYear = '".$schoolYearId."'";
			$success[] = $this->db_db_query($sql);
		
		$success[] = $this->saveApplicationStudentInfoCust($data);
		
		$success[] = $this->saveApplicationRelativesInfoCust($data);
		
		return !in_array(false,$success);	
    }    
    function removeApplicationAttachment($data){
    	global $file_path;
    	extract($data);
    	if(empty($recordID)) return;
 		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";
    	$sql = "SELECT 
    				r.RecordID attachment_id,
    				r.AttachmentName attachment_name,
    				r.AttachmentType attachment_type
    			FROM 
    				ADMISSION_ATTACHMENT_RECORD r
    			INNER JOIN 
    				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
    			WHERE 
    				o.RecordID = '".$recordID."' 
    				".$cond."
    			";
    	$applicantAry = $this->returnArray($sql);
    
    	$Success = array();
    	for($i=0;$i<count($applicantAry);$i++){
    		$_attachmentName = $applicantAry[$i]['attachment_name'];
    		$_attachmentType = $applicantAry[$i]['attachment_type']=='personal_photo'? $applicantAry[$i]['attachment_type']:'other_files';
    		$_attachmentId = $applicantAry[$i]['attachment_id'];
    		$image_url = $this->filepath.$recordID.'/'.$_attachmentType.'/'.$_attachmentName;	
    		
    		if(file_exists($file_path.$image_url)){
    			unlink($file_path.$image_url);
    			$sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '".$_attachmentId."'";
    			
    			$Success[] = $this->db_db_query($sql);
    		}
    	}
    	if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }
    function saveApplicationAttachment($data){
    	global $UserID;
    	extract($data);
 		if(empty($recordID)) return;
    	$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '".$recordID."'";
    	$applicationID = current($this->returnVector($sql));
    	
    	if(!empty($applicationID)&&!empty($attachment_name)&&!empty($attachment_type)){
    		$result = $this->removeApplicationAttachment($data);
    		if($result){
	    		$sql = "INSERT INTO 
	    					ADMISSION_ATTACHMENT_RECORD 
	    						(ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy)
	    					VALUES
	    						('".$applicationID."','".$attachment_type."','".$attachment_name."',NOW(),'".$UserID."')
	    				";
	    		return $this->db_db_query($sql);
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
 	function updateApplicationStudentInfo($data, $isAdminUpdate = false){
 		global $kis_lang, $Lang,$admission_cfg;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
 		if($isAdminUpdate){
// 			$sql = "
//				UPDATE 
//					ADMISSION_STU_INFO stu 
//				INNER JOIN 
//					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
//				SET
//					stu.LastSchool = '".$lastschool."'
//				WHERE 
//					o.RecordID = '".$recordID."'	
//	    	";
 		}
 		else{
			$sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
				SET
	     			stu.ChineseName = '".$student_surname_b5.','.$student_firstname_b5."',
	      			stu.EnglishName = '".$student_surname_en.','.$student_firstname_en."',
	      			stu.Gender = '".$gender."',
	      			stu.DOB = '".$dateofbirth."',
					stu.BirthCertType = '".$FormStudentInfo_StuIDType."',	
	      			stu.BirthCertNo = '".$birthcertno."' ,
	      			stu.PlaceOfBirth = '".$placeofbirth."',
					stu.PlaceOfBirthOther = '".$FormStudentInfo_POB_Other."',
	      			stu.Province = '".$province."',
					stu.County = '".$county."',
	      			stu.HomeTelNo = '".$homenumber.'|||'.$contactnumber."',
	      			stu.ReligionCode = '".$religion."' ,
	      			stu.Church = '".$church."',
	      			stu.AddressChi = '".$homeaddresschi."',
					stu.Address = '".$homeaddress."',
					stu.ContactAddressChi = '".$ContactAddressChi."',
					stu.ContactAddress = '".$ContactAddress."',
					stu.BirthCertTypeOther = '".$FormStudentInfo_StuIDType_Other."',
					stu.ReligionOther = '".$FormStudentInfo_Religion_Other."',
					stu.AddressDistrict  = '".$HomeAddressDistrict.'|||'.$ContactAddressDistrict."'
				WHERE 
					o.RecordID = '".$recordID."'	
	    	";
 		}

    	return $this->db_db_query($sql);
	}
	
	function updateFamilyInfo($data){
		extract($data);
		
		$sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
				SET
					stu.IsTwins = '".$IsTwins."',
					stu.TwinsApplicationID = '".$TwinsIDNo."',
					o.EBrotherNo = '".$NumElderBro."',
					o.ESisterNo = '".$NumElderSis."',
					o.YBrotherNo = '".$NumYoungBro."',
					o.YSisterNo = '".$NumYoungSis."'
				WHERE 
					o.RecordID = '".$recordID."'	
	    	";
    	return $this->db_db_query($sql);
	}    
 	
	function getApplicationAttachmentRecord($schoolYearID,$data=array()){
		extract($data);
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";		
		$sql = "
			SELECT
				 o.RecordID folder_id,
     			 r.RecordID,
			     r.ApplicationID applicationID,
			     r.AttachmentType attachment_type,
			     r.AttachmentName attachment_name,
			     r.DateInput dateinput,
			     r.InputBy inputby
			FROM
				ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID						
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			ORDER BY r.AttachmentType, r.RecordID desc
    	";
    	$result = $this->returnArray($sql);
    	$attachmentAry = array();
    	for($i=0;$i<count($result);$i++){
    		$_attachType = $result[$i]['attachment_type'];
     		$_attachName = $result[$i]['attachment_name'];   
     		$_applicationId = $result[$i]['applicationID'];  
     		$_folderId = $result[$i]['folder_id'];
     		$attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;		
     		$attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId.'/'.($_attachType=='personal_photo'?$_attachType:'other_files').'/'.$_attachName;	
     		
    	}
		return $attachmentAry;
	}
	function saveApplicationParentInfo($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
 		foreach($this->pg_type as $_key => $_pgType){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			$parentInfoAry['PG_TYPE'] = $_pgType;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			$parentInfoAry['ChineseName'] = $parent_name_b5[$_key];
			$parentInfoAry['JobTitle'] = $occupation[$_key];
//			$parentInfoAry['Company'] = $companyname[$_key];
			//$parentInfoAry['JobPosition'] = $jobposition[$_key];
//			$parentInfoAry['OfficeAddress'] = $companyaddress[$_key];
			$parentInfoAry['OfficeTelNo'] = $office[$_key];
			$parentInfoAry['Mobile'] = $mobile[$_key];	
			$parentInfoAry['Email'] = $email[$_key];
//			$parentInfoAry['LevelOfEducation'] = $levelofeducation[$_key];
//			$parentInfoAry['LastSchool'] = $lastschool[$_key];
			//$parentInfoAry['HKID'] = $hkid[$_key];
			if($_pgType == 'G'){
				$parentInfoAry['Gender'] = $G3Gender;
				$parentInfoAry['Relationship'] = $G3Relationship;
			}
			else{
				$parentInfoAry['Gender'] = '';
				$parentInfoAry['Relationship'] = '';
			}
			
			if($parent_id[$_key]=='new'){
				$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
			}else{
				$parentInfoAry['RecordID'] = $parent_id[$_key];	
				$Success[] = $this->updateApplicationParentInfo($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationStudentInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
// 		for($i=0; $i<1; $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
//			if($student_cust_id[$i]=='new'){
//				$parentInfoAry['OthersPrevSchClass'.($i+1)] = $OthersPrevSchClass[$i];
//				$parentInfoAry['OthersPrevSchName'.($i+1)] = $OthersPrevSchName[$i];
//				$parentInfoAry['SchoolAddress'] = $SchoolAddress[$i];
//				$parentInfoAry['ClassType'] =  $ClassType;
//				$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry);
//			}else{
				if($student_cust_id[0] == 'new'){
					$parentInfoAry['CurrentSchoolClassLv'] = $OthersPrevSchClass[0];
					$parentInfoAry['CurrentSchoolName'] = $OthersPrevSchName[0];
					$parentInfoAry['CurrentSchoolAddr'] = $SchoolAddress[0];
					$parentInfoAry['CurrentSchoolDayType'] =  $ClassType;
					$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry,$parentInfoAry['ApplicationID']);
				}
				else{
					$parentInfoAry['Class'] = $OthersPrevSchClass[0];
					$parentInfoAry['NameOfSchool'] = $OthersPrevSchName[0];
					$parentInfoAry['SchoolAddress'] = $SchoolAddress[0];
					$parentInfoAry['ClassType'] =  $ClassType;
					$parentInfoAry['RecordID'] = $student_cust_id[0];	
					$Success[] = $this->updateApplicationStudentInfoCust($parentInfoAry);
				}
				
//			}
//		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationRelativesInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
 		for($i=0; $i<count($OthersRelativeStudiedName); $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($relatives_info_id[$i]=='new'&& $OthersRelativeStudiedName[$i]!=''){
				$parentInfoAry['OthersRelativeStudiedYear'.($i+1)] = $OthersRelativeStudiedYear[$i];
				$parentInfoAry['OthersRelativeStudiedName'.($i+1)] = $OthersRelativeStudiedName[$i];
				$parentInfoAry['OthersRelativeRelationship'.($i+1)] = $OthersRelativeRelationship[$i];
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry,$parentInfoAry['ApplicationID']);
				
			}else{
				$parentInfoAry['Year'] = $OthersRelativeStudiedYear[$i];
				$parentInfoAry['Name'] = $OthersRelativeStudiedName[$i];
				$parentInfoAry['Relationship'] = $OthersRelativeRelationship[$i];
				$parentInfoAry['RecordID'] = $relatives_info_id[$i];	
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	   
	function getApplicationOthersInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND RecordID='".$recordID."'":"";

		$sql = "
			SELECT 
				RecordID,
				ApplicationID applicationID,
				ApplyYear schoolYearId,
				ApplyDayType1,
				ApplyDayType2,
				ApplyDayType3,
				ApplyLevel classLevelID,
				DateInput,
				DateModified,
				ModifiedBy,
				InterviewSettingID,
				SiblingAppliedName,
			 	ExBSName,
			 	ExBSName2,
				ExBSName3,
			 	ExBSGradYear,
			 	ExBSGradYear2,
			 	ExBSGradYear3,
				EBrotherNo,
				ESisterNo,
				YBrotherNo,
				YSisterNo,
				Remarks,
				IsConsiderAlternative OthersIsConsiderAlternative
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	return $this->returnArray($sql);
	}		 
	function getApplicationParentInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND pg.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				pg.RecordID,
     			pg.ApplicationID applicationID,
     			pg.ChineseName parent_name_b5,
     			pg.PG_TYPE type,
				pg.Company companyname,
				pg.OfficeAddress companyaddress,
				pg.OfficeTelNo OfficeTelNo,
     			pg.JobTitle occupation,
     			pg.Mobile mobile,
				pg.Email email,
				pg.LevelOfEducation levelofeducation,
				pg.LastSchool lastschool,
				pg.Gender Gender,
				pg.Relationship Relationship,
     			o.ApplyLevel classLevelID
			FROM
				ADMISSION_PG_INFO pg
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";

    	return $this->returnArray($sql);
	}	
	function getApplicationStatus($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			s.ApplicationID applicationID,
     			s.ReceiptID receiptID, 
			    IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
			    IF(s.Handler,".getNameFieldByLang2("iu.").",'') AS handler,
			    s.Handler handler_id,
				s.InterviewDate As interviewdate,
				s.InterviewLocation As interviewlocation,
			    s.Remark remark,CASE ";
		foreach($admission_cfg['Status'] as $_key => $_value){
				$sql .= "WHEN s.Status = ".$_value." THEN '".$_key."' ";
		}	    
		$sql .= " ELSE s.Status END status,";
		$sql .= "
			    CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
			    s.DateInput dateinput,
			    s.InputBy inputby,
			    s.DateModified datemodified,
			    s.ModifiedBy modifiedby,
			    o.ApplyLevel classLevelID
			FROM
				ADMISSION_APPLICATION_STATUS s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			LEFT JOIN
				INTRANET_USER iu ON s.Handler = iu.UserID
			WHERE
				s.SchoolYearID = '".$schoolYearID."'	
			".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

		//if($type == "personal_photo"){
			if (!empty($file['name'])) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							if($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
								$image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
							else
								$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}else if($this->isInternalUse($_GET['token'])){
				$uploadSuccess = true;
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	function moveUploadedAttachment($tempFolderPath, $destFolderPath){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/personal_photo", $destFolderPath);
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/other_files", $destFolderPath);
		$uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);
		
		if(in_array(false, $uploadSuccess))
			return false;
		else
			return true;
	}
	//Siuwan 20131018 Copy from libstudentregistry.php, e.g.$this->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	public function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
		$result = $this->returnArray($sql);
		return getSelectByArray($result, "name=".$selection_name, $code_selected,0,1);
	}
	//Henry 20131018 Copy from libstudentregistry.php
	public function returnPresetCodeName($code_type="", $selected_code="")
	{
		$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
		$result = $this->returnVector($sql);
		return $result[0];
	}
	function displayWarningMsg($warning){
		global $kis_lang;
		$x = '
		 <fieldset class="warning_box">
			<legend>'.$kis_lang['warning'].'</legend>
			<ul>
				<li>'.$kis_lang['msg'][$warning].'</li>
			</ul>
		</fieldset>
		
		';
		return $x;
	}
		
	function insertAttachmentRecord($AttachmentType,$AttachmentName, $ApplicationID){
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
		   				 ApplicationID,
		  				 AttachmentType,   
		   				 AttachmentName,
		  				 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$AttachmentType."',
						'".$AttachmentName."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	//old method
	function newApplicationNumber($schoolYearID=""){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$defaultNo = "PL".$year."-a0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%' order by ApplicationID desc";
		$result = $this->returnArray($sql);
		
		if($result){			
			$num = substr($result[0]['ApplicationID'], -4);
			$num++;
			$num = sprintf("%04s", $num);
		}
		
		$newNo = $prefix.$num;
		
		return $newNo;
	}
	
	function newApplicationNumber2($schoolYearID="",$classLevelID=""){
		global $Lang;
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
//		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
//		if($yearStart == $yearEnd){
//			$year = $yearStart;
//		}
//		else{
//			$year = $yearStart;
//		}

		//eg. csm1600001
		$defaultNo = $yearStart."0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
		
		if($this->returnArray($sql)){
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( SUBSTRING( ApplicationID, '".(strlen($prefix)+1)."' ) , UNSIGNED ) ) +1, 4, '0')) 
					FROM ADMISSION_OTHERS_INFO 
					WHERE ApplicationID like '".$prefix."%'";
		}
		else{
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					Values ('".$defaultNo."')";
		}
			$result = $this->db_db_query($sql);
					
			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
			$newNo = $this->returnArray($sql);
			return $newNo[0]['ApplicationID'];
	}
	function getAttachmentByApplicationID($schoolYearID,$applicationID){
		global $file_path;
		$attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID,array("applicationID"=>$applicationID));
		$attachment = array();
		foreach((array)$attachmentAry[$applicationID] as $_type => $_attachmentAry){
			$_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
			if(!empty($_thisAttachment)){
				$_thisAttachment = $this->filepath.$_thisAttachment; 
				if(file_exists($file_path.$_thisAttachment)){
					$attachment[$_type]['link'] = $_thisAttachment;
				}else{
					$attachment[$_type]['link'] = false;
				}
			}else{
				$attachment[$_type]['link'] = false;
			}
			
		}
		return $attachment;
	}
	
	public function getApplicationNumber($RecordIDAry){
		$sql = 'SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID IN ("'.implode('","',$RecordIDAry).'")';
		$result = $this->returnVector($sql);
		return $result;
	}
	
	public function getPrintLink($schoolYearID="", $applicationID , $type=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?id='.$id;
	}
 	function getClassLevel($ClassLevel=''){
 		global $intranet_root;
    	include_once($intranet_root."/includes/form_class_manage.php");
    	$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
    	$numOfForm = count($FormArr);
    	$classLevelName = array();
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisClassLevelID = $FormArr[$i]['YearID'];
			$thisLevelName = $FormArr[$i]['YearName'];
			$classLevelName[$thisClassLevelID] = $thisLevelName;
		}
		return $classLevelName;
    }	
	function getBasicSettings($schoolYearID='',$SettingNameAry=array()){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
    	$sql = "
			SELECT
			    SettingName,
			    SettingValue
			FROM
				ADMISSION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'	
    	";
    	if (sizeof($SettingNameAry) > 0)  {
			$sql .= " AND 
						SettingName in ('".implode("','",$SettingNameAry)."')";
		}
    	$setting = $this->returnArray($sql);
		for ($i=0; $i< sizeof($setting); $i++) {
			$Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
		}
		return $Return;    	
    }
   	function saveBasicSettings($schoolYearID='',$SettingNameValueAry=array()){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$this->Start_Trans();
		
		$SettingsNameAry = array_keys($SettingNameValueAry);
		
		$result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID,$SettingsNameAry);
		$result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID,$SettingNameValueAry);
		
		if(in_array(false, $result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	 
	function removeBasicSettings($schoolYearID,$SettingsNameAry){
		if(count($SettingsNameAry)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameAry)."'";
		
		$sql = "
			DELETE FROM
				ADMISSION_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
			AND SchoolYearID = '".$schoolYearID."'
		";
		
		return $this->db_db_query($sql);
	}
	
	function insertBasicSettings($schoolYearID,$SettingNameValueAry){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		
		foreach((array)$SettingNameValueAry as $_settingName => $_settingValue)
		{
			$InsertSqlArr[] = "('".$schoolYearID."','".$_settingName."','".$_settingValue."', '".$this->uid."', NOW(), '".$this->uid."', NOW())";
		}
		
		if(count($InsertSqlArr)>0)
		{	
			$InsertSql = implode(',',$InsertSqlArr);	
			
			$sql = "
				INSERT INTO	ADMISSION_SETTING
					(SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)	 
				VALUES
					$InsertSql
			";
			
			return $this->db_db_query($sql);
			
		}
		else
			return false;
		
			
	}
//    function getApplicationSetting($schoolYearID=''){
//    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
//
//    	$sql = "
//			SELECT
//     			ClassLevelID,
//			    IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d %H:%i'),'') As StartDate,
//				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d %H:%i'),'') As EndDate,
//				IF(DOBStart,DATE_FORMAT(DOBStart,'%Y-%m-%d'),'') As DOBStart,
//				IF(DOBEnd,DATE_FORMAT(DOBEnd,'%Y-%m-%d'),'') As DOBEnd,
//			    DayType,
//			    FirstPageContent,
//			    LastPageContent,
//			    DateInput,
//			    InputBy,
//			    DateModified,
//			    ModifiedBy
//			FROM
//				ADMISSION_APPLICATION_SETTING
//			WHERE
//				SchoolYearID = '".$schoolYearID."'
//    	";
//    	$setting = $this->returnArray($sql);
//    	$applicationSettingAry = BuildMultiKeyAssoc($setting, 'ClassLevelID');
//    	$applicationPeriodAry = array();
//    	foreach($this->classLevelAry as $_classLevelId => $_classLevelName){ 	
//    		$_startdate = $applicationSettingAry[$_classLevelId]['StartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['StartDate']:'';	
//    		$_enddate = $applicationSettingAry[$_classLevelId]['EndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['EndDate']:'';	
//    		
//    		$applicationPeriodAry[$_classLevelId] = array(
//    													'ClassLevelName'=>$_classLevelName,
//      													'StartDate'=>$_startdate,  	
//      													'EndDate'=>$_enddate,
//      													'DOBStart'=>$applicationSettingAry[$_classLevelId]['DOBStart'],
//      													'DOBEnd'=>$applicationSettingAry[$_classLevelId]['DOBEnd'],
//      													'DayType'=>$applicationSettingAry[$_classLevelId]['DayType'],   
//      													'FirstPageContent'=>$applicationSettingAry[$_classLevelId]['FirstPageContent'],  
//      													'LastPageContent'=>$applicationSettingAry[$_classLevelId]['LastPageContent']      													  																									
//    												);
//    	}
//    	return $applicationPeriodAry;    	
//    } 
//	public function updateApplicationSetting($data){
//   		extract($data);
//   		#Check exist setting
//   		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
//   		$cnt = current($this->returnVector($sql));
//
//   		if($cnt){//update
//   			$sql = "UPDATE ADMISSION_APPLICATION_SETTING SET 
//		   				 StartDate = '".$startDate."',
//		  				 EndDate = '".$endDate."', 
//						 DOBStart = '".$dOBStart."',
//		  				 DOBEnd = '".$dOBEnd."',  
//		   				 DayType = '".$dayType."',
//		  				 FirstPageContent = '".$firstPageContent."',   
//		   				 LastPageContent = '".$lastPageContent."',
//		   				 DateModified = NOW(),
//		   				 ModifiedBy = '".$this->uid."'
//   					WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
//   		}else{//insert
//   			$sql = "INSERT INTO ADMISSION_APPLICATION_SETTING (
//   						SchoolYearID,
//   						ClassLevelID,
//					    StartDate,
//					    EndDate,
//					    DayType,
//		  				FirstPageContent,   
//		   				LastPageContent,
//					    DateInput,
//					    InputBy,
//					    DateModified,
//					    ModifiedBy) 
//					VALUES (
//					    '".$schoolYearID."',
//					    '".$classLevelID."',
//					    '".$startDate."',
//					    '".$endDate."',	
//					    '".$dayType."',
//					    '".$firstPageContent."',
//					    '".$lastPageContent."',	
//					    NOW(),
//					    '".$this->uid."',
//					     NOW(),
//					    '".$this->uid."')
//			";
//   		}
//   		return $this->db_db_query($sql);
//    }    
    function getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$recordID=''){
		$cond = !empty($applicationID)?" AND stu.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($status)?" AND s.status='".$status."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		
		$sql = "
			SELECT
     			stu.ApplicationID applicationID,
     			o.ApplyLevel classLevelID,
     			o.ApplyYear schoolYearID,
     			stu.ChineseName student_name_b5,
      			stu.EnglishName student_name_en, 
      			stu.Gender gender,
				stu.BirthCertType birthcerttype,
				stu.BirthCertTypeOther birthcerttypeother,
      			IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,	
      			stu.PlaceOfBirth placeofbirth,
				stu.PlaceOfBirthOther PlaceOfBirthOther,
      			stu.Province province,
      			stu.County county,
      			stu.HomeTelNo homephoneno,
      			stu.BirthCertNo birthcertno,
      			stu.ReligionCode religion1,
				stu.ReligionOther ReligionOther,
      			stu.Church Church,
      			stu.LastSchool lastschool,
      			stu.Address homeaddress,
				stu.AddressChi homeaddresschi,
				stu.ContactAddress ContactAddress,
				stu.ContactAddressChi ContactAddressChi,
				stu.Age age,
				stu.IsTwins,
				stu.TwinsApplicationID as TwinsIDNo,
				stu.AddressDistrict,
     			".getNameFieldByLang2("stu.")." AS student_name,
				IF(stu.DateInput,DATE_FORMAT(stu.DateInput,'%Y-%m-%d'),'') DateInput
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID							
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	$studentInfoAry = $this->returnArray($sql);
		return $studentInfoAry;
	}
	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['CHIUCHUNKG_waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'
					OR pg.Mobile LIKE '%".$keyword."%'
					OR stu.BirthCertNo LIKE '%".$keyword."%'
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}
	
	function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword=''){
		global $admission_cfg;
		
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		
		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}

//		if(!empty($keyword)){
//			$cond .= "
//				AND ( 
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'					
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'				
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID
			INNER JOIN 
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				1 	
			".$cond." 
			
    	";
    	$sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." ";
     	//debug_r($sql);
    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}
	   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function checkImportDataForImportAdmissionHeader($csv_header, $lang = ''){
		//$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
		$file_format = $this->getExportHeader();
		$file_format = $file_format[1];
		# check csv header
		$format_wrong = false;
		
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($csv_header[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		
		return $format_wrong;
	}
	
	public function returnPresetCodeAndNameArr($code_type="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			
			//valid Admission Date
			$aData[0] = getDefaultDateFormat($aData[0]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[0]) ) {
				list($year , $month , $day) = explode('-',$aData[0]);
		       	$resultArr[$i]['validAdmissionDate'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validAdmissionDate'] = false;
			}
			
			//valid Admission number
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[1])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validAdmissionNo'] = false;
			}
			else
				$resultArr[$i]['validAdmissionNo'] = true;
			
			//valid Gender
			if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
				$data[$i][5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
				$data[$i][5] = 'F';
			}
			if ( strtoupper($aData[5]) == 'M' || strtoupper($aData[5]) == 'F') {
				
		       	$resultArr[$i]['validGender'] = true;
			}
			else{
				$resultArr[$i]['validGender'] = false;
			}
			
			//valid Date of birth
			$aData[6] = getDefaultDateFormat($aData[6]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[6]) ) {
				list($year , $month , $day) = explode('-',$aData[6]);
		       	$resultArr[$i]['validDateOfBirth'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validDateOfBirth'] = false;
			}
			
			//valid age
			if (is_numeric($aData[7])) {
				$resultArr[$i]['validAge'] = true;
			}
			else{
				$resultArr[$i]['validAge'] = false;
			}
			
			//valid birth cert type
			$resultArr[$i]['validBirthCertType'] = false;
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$resultArr[$i]['validBirthCertType'] = true;
					break;
				}
			}
			
			//valid birth cert
//			if (preg_match('/^[a-zA-Z][0-9]{7}$/',$aData[8])) {
//				$resultArr[$i]['validBirthCertNo'] = true;
//			}
//			else{
//				$resultArr[$i]['validBirthCertNo'] = false;
//			}
			
			//valid email address
			if (preg_match('/\S+@\S+\.\S+/',$aData[14])) {
				$resultArr[$i]['validEmail'] = true;
			}
			else{
				$resultArr[$i]['validEmail'] = false;
			}
			//valid Religion
			$religionArr = $this->returnPresetCodeAndNameArr("RELIGION");
			foreach($religionArr as $aReligion){
				if (strtoupper($aReligion[1]) == strtoupper($aData[15])){
					$aData[15] = $aReligion[0];
					$data[$i][15] = $aReligion[0];
					$resultArr[$i]['validReligion'] = true;
					break;
				}
				else{
					$resultArr[$i]['validReligion'] = false;
				}
			}
			
			//valid Apply day type
			$resultArr[$i]['validApplyDayType'] = false;
			if($aData[26] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[26]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[26] = $j;
						$data[$i][26] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			if($aData[27] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[27] = $j;
						$data[$i][27] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			if($aData[28] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[28]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[28] = $j;
						$data[$i][28] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			//valid SiblingApplied
			$resultArr[$i]['validSiblingApplied'] = true;
			if(strtoupper($aData[29]) == strtoupper($kis_lang['Admission']['yes'])){
				if($aData[30] == ''){
					$resultArr[$i]['validSiblingApplied'] = false;
				}
			}
			
			//valid SiblingIsGrad
			$resultArr[$i]['validSiblingIsGrad'] = true;
			$resultArr[$i]['validSiblingIsGradInfo'] = true;
			if(strtoupper($aData[31]) == strtoupper($kis_lang['Admission']['yes'])){
				if(!is_numeric($aData[32])){
					$resultArr[$i]['validSiblingIsGrad'] = false;
				}
				if($aData[32] == 1 || $aData[32] == 2 || $aData[32] == 3){
					if($aData[33] =='' || $aData[34] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
				if($aData[32] == 2 || $aData[32] == 3){
					if($aData[35] =='' || $aData[36] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
				if($aData[32] == 3){
					if($aData[37] =='' || $aData[38] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
			}
			
			//--- Henry Added 20140827
//			//valid record status
//			foreach($admission_cfg['Status'] as $_key => $_status){
//				if (strtoupper($kis_lang['Admission']['Status'][$_key]) == strtoupper($aData[41])){
//					
//					$aData[41] = $_status;
//					$data[$i][41] = $_status;
//					$resultArr[$i]['validRecordStatus'] = true;
//					break;
//				}
//				else{
//					$resultArr[$i]['validRecordStatus'] = false;
//				}
//			}
//			
//			//valid Receipt Date
//			$aData[43] = getDefaultDateFormat($aData[43]);
//
//			if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[43]) ) {
//				list($year , $month , $day) = explode('-',$aData[43]);
//		       	$resultArr[$i]['validReceiptDate'] = checkdate($month , $day , $year);
//			}
//			else{
//				if($aData[43] != '')
//					$resultArr[$i]['validReceiptDate'] = false;
//				else
//					$resultArr[$i]['validReceiptDate'] = true;
//			}
//			
//			//valid is notified
//			$resultArr[$i]['validIsNotified'] = false;
//			if(strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['no'])){
//				$resultArr[$i]['validIsNotified'] = true;
//			}
			
			
			//------------------------------------------------------------------------------------------
//			$aData[4] = getDefaultDateFormat($aData[4]);
//			//check date
//			if ($aData[4] =='' && $aData[5] ==''){
//				$validDate = true;
//			}
//			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
//		       list($year , $month , $day) = explode('-',$aData[4]);
//		       $validDate = checkdate($month , $day , $year);
//		    } else {
//		       $validDate =  false;
//		    }
//
//		    //check time
//		    if ($aData[4] =='' && $aData[5] ==''){
//				$validTime = true;
//			}
//			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
//		       $validTime = true;
//		    } else {
//		       $validTime =  false;
//		    }
//			$sql = "
//				SELECT
//					COUNT(*)
//				FROM
//					ADMISSION_STU_INFO s
//				WHERE 
//					trim(s.applicationID) = '".trim($aData[1])."' AND trim(s.birthCertNo) = '".trim($aData[8])."'
//	    	";
//			$result = $this->returnVector($sql);
//			if($result[0] == 0){
//				$resultArr[$i]['validData'] = $aData[1];
//			}
//			else
//				$resultArr[$i]['validData'] = false;
//			$resultArr[$i]['validDate'] = $validDate;
//			$resultArr[$i]['validTime'] = $validTime;
//			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[1];
			$i++;
		}
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
			if(!$aResult['validAdmissionDate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidAdmissionDate'];
			}
			else if(!$aResult['validAdmissionNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
			}
			else if(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidWordOfGender'] ;
			}
			else if(!$aResult['validDateOfBirth']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidDateOfBirth'];
			}
			else if(!$aResult['validAge']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidNumberOfAge'] ;
			}
			else if(!$aResult['validBirthCertType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertType'];
			}
//			else if(!$aResult['validBirthCertNo']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertNo'];
//			}
			else if(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress'];
			}
			else if(!$aResult['validReligion']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidKeywordOfReligion'] ;
			}
			else if(!$aResult['validApplyDayType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidKeywordOfApplyDayType'] ;
			}
			else if(!$aResult['validSiblingApplied']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNameOfSiblingApplied'];
			}
			else if(!$aResult['validSiblingIsGrad']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNoOfGraduatedSibling'];
			}
			else if(!$aResult['validSiblingIsGradInfo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNameOfGraduatedSibling'];
			}
//			else if(!$aResult['validRecordStatus']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importEnterRecordStatus'];
//			}
//			else if(!$aResult['validReceiptDate']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importInvalidReceiptDate'];
//			}
//			else if(!$aResult['validIsNotified']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importEnterIsNotified'];
//			}
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.$i.'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			//-----------------------------------------------------------------------------------
//			if($aResult['validData']!=false){
//				$errCount++;
//			$x .= '<tr class="step2">
//					<td>'.$i.'</td>
//					<td>'.$aResult['validData'].'</td>
//					<td><font color="red">';
//			if(!$aResult['validDate']){
//				$x .= $kis_lang['invalidinterviewdateformat'];
//			}
//			else if(!$aResult['validTime']){
//				$x .= $kis_lang['invalidinterviewtimeformat'];
//			}
//			else
//				$x .= $kis_lang['invalidapplicationbirthno'];
//			$x .= '</font></td>
//				</tr>';
//			}
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	function importDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		array_shift($data);
		array_shift($data);
		foreach($data as $aData){
			global $UserID;
			//--- convert the text to key code [start]
		    if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
			}
			
			$religionArr = $this->returnPresetCodeAndNameArr("RELIGION");
			foreach($religionArr as $aReligion){
				if (strtoupper($aReligion[1]) == strtoupper($aData[15])){
					$aData[15] = $aReligion[0];
					break;
				}
			}
			
			//valid Apply day type
			if($aData[26] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[26]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[26] = $j;
						break;
					}
				}
			}
			
			if($aData[27] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[27] = $j;
						break;
					}
				}
			}
			
			if($aData[28] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[28]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[28] = $j;
						break;
					}
				}
			}
			
			//valid birth cert type
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$aData[8] =$_type;
					break;
				}
			}
			
			//--- Henry Added 20140827
			//valid record status
//			foreach($admission_cfg['Status'] as $_key => $_status){
//				if (strtoupper($kis_lang['Admission']['Status'][$_key]) == strtoupper($aData[41])){
//					$aData[41] = $_status;
//					break;
//				}
//			}
//			
//			//valid Receipt Date
//			$aData[43] = getDefaultDateFormat($aData[43]);
//			
//			//valid is notified
//
//			if(strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['no'])){
//				$aData[46] = (strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']));
//			}
			//--- convert the text to key code [end]
		    
		    $result = array();
		    
		    $sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
				SET
	     			stu.ChineseName = '".$aData[3]."',
	      			stu.EnglishName = '".$aData[4]."', 
	      			stu.Gender = '".$aData[5]."',
	      			stu.DOB = '".getDefaultDateFormat($aData[6])." 00:00:00',	
	      			stu.PlaceOfBirth = '".$aData[10]."',
	      			stu.Province = '".$aData[11]."',
	      			stu.HomeTelNo = '".$aData[12]."',
					stu.BirthCertType = '".$aData[8]."' ,
	      			stu.BirthCertNo = '".$aData[9]."' ,
	      			stu.Email = '".$aData[14]."',
	      			stu.ReligionCode = '".$aData[15]."' ,
	      			stu.Church = '".$aData[16]."',
	      			stu.LastSchool = '".$aData[39]."',
	      			stu.Address = '".$aData[13]."',
					stu.Age = '".$aData[7]."',
					stu.DateModified = NOW(),
				    stu.ModifiedBy = '".$UserID."',
					o.ApplyDayType1 = '".$aData[26]."',
					o.ApplyDayType2 = '".$aData[27]."',
					o.ApplyDayType3 = '".$aData[28]."',
					o.SiblingAppliedName = '".$aData[30]."',
					o.ExBSName = '".$aData[33]."',
					o.ExBSName2 = '".$aData[35]."',
					o.ExBSName3 = '".$aData[37]."',
					o.ExBSGradYear = '".$aData[34]."',
					o.ExBSGradYear2 = '".$aData[36]."',
					o.ExBSGradYear3 = '".$aData[38]."',
					o.Remarks =  '".$aData[40]."', 
				    o.DateModified = NOW(),
				    o.ModifiedBy = '".$UserID."'
				WHERE 
					o.ApplicationID = '".$aData[1]."'
	    	";
		    
		    $result[] = $this->db_db_query($sql);
		    
		    $sql = "
				UPDATE 
					ADMISSION_PG_INFO pg
		    	SET
	     			pg.ChineseName = '".$aData[17]."',
	     			pg.JobTitle = '".$aData[19]."',
	     			pg.HKID = '".$aData[18]."',
	     			pg.Mobile = '".$aData[20]."',
					pg.DateModified = NOW(),
					pg.ModifiedBy = '".$UserID."'
				WHERE 
					pg.ApplicationID = '".$aData[1]."' AND pg.PG_TYPE = 'F'
		   	";
		   	
		   	$result[] = $this->db_db_query($sql);
		   	
		   	$sql = "
				UPDATE 
					ADMISSION_PG_INFO pg
		    	SET
	     			pg.ChineseName = '".$aData[21]."',
	     			pg.JobTitle = '".$aData[22]."',
	     			pg.HKID = '".$aData[23]."',
	     			pg.Mobile = '".$aData[24]."',
					pg.DateModified = NOW(),
					pg.ModifiedBy = '".$UserID."'
				WHERE 
					pg.ApplicationID = '".$aData[1]."' AND pg.PG_TYPE = 'M'
		   	";
		   
		    $result[] = $this->db_db_query($sql);
		    
//		    $sql = "
//				UPDATE 
//					ADMISSION_APPLICATION_STATUS
//		    	SET
//	     			Status = '".$aData[41]."',
//					ReceiptID = '".$aData[42]."',
//					ReceiptDate = '".$aData[43]." 00:00:00',
//					Handler = '".$aData[44]."',
//					InterviewDate = '".$aData[45]."',
//					isNotified = '".$aData[46]."',
//					DateModified = NOW(),
//					ModifiedBy = '".$UserID."'
//				WHERE 
//					ApplicationID = '".$aData[1]."'
//		   	";
//		   
//		    $result[] = $this->db_db_query($sql);
		     
			$resultArr[] = !in_array(false, $result);
			//debug_pr($aData);
		}
		return $resultArr;
	}
	
	function getExportHeader($lang = ''){
		global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$headerArray = array();
		
		if($lang == 'en'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}else if($lang == 'b5'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}
		
		//for student info
		$headerArray[] = $kis_lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray[] = $kis_lang['Admission']['applyLevel'];
		$headerArray[] = $kis_lang['Admission']['CHIUCHUNKG']['Wish'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['chinesename'].'('.$kis_lang['Admission']['csm']['surname_b5'].')';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['chinesename'].'('.$kis_lang['Admission']['csm']['firstname_b5'].')';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['englishname'].'('.$kis_lang['Admission']['csm']['surname_en'].')';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['englishname'].'('.$kis_lang['Admission']['csm']['firstname_en'].')';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['nativeplace'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['religion'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['church'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['IDtype'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['IDNo'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['homenumber'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['contactnumber'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].$Lang['Admission']['CHIUCHUNKG']['district'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'(Chi)';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['homeaddress'].'(Eng)';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].$Lang['Admission']['CHIUCHUNKG']['district'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].'(Chi)';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['contactaddress'].'(Eng)';
		
		//for parent info
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['name'];
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['mgf']['occupation'];
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['CHIUCHUNKG']['worknumber'];
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['csm']['mobile'];
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['csm']['email'];

		$headerArray['parentInfoM'][] = $kis_lang['Admission']['name'];
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['mgf']['occupation'];
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['CHIUCHUNKG']['worknumber'];
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['csm']['mobile'];
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['csm']['email'];
		
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['CHIUCHUNKG']['relationship'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['CHIUCHUNKG']['gender'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['name'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['mgf']['occupation'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['CHIUCHUNKG']['worknumber'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['csm']['mobile'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['csm']['email'];
		
		//for other info
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['B'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['S'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['YB'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['YS'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['RankInFamily'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['Twins'];
		$headerArray['otherInfo'][] = $kis_lang['Admission']['CHIUCHUNKG']['TwinsID'];
		
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['CHIUCHUNKG']['CurrentSchoolSchoolName'];
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['homeaddress'];
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['level'];
		
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['name'].'(1)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['RelationshipWithApplicant'].'(1)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['Year'].'(1)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['name'].'(2)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['RelationshipWithApplicant'].'(2)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['Year'].'(2)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['name'].'(3)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['RelationshipWithApplicant'].'(3)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['Year'].'(3)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['name'].'(4)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['RelationshipWithApplicant'].'(4)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['munsang']['Year'].'(4)';
		
		//for official use
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
		
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//student info header
		$exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
		for($i=0; $i < count($headerArray['studentInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//parent info header
		$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['F'].")";
		for($i=0; $i < count($headerArray['parentInfoF'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['M'].")";
		for($i=0; $i < count($headerArray['parentInfoM'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['G'].")";
		for($i=0; $i < count($headerArray['parentInfoG'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['familyStatus'];
		for($i=0; $i < count($headerArray['otherInfo'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other2 info header
		$exportColumn[0][] = $kis_lang['Admission']['CHIUCHUNKG']['CurrentSchoolInfo'];
		for($i=0; $i < count($headerArray['otherInfo2'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other3 info header
		$exportColumn[0][] = $kis_lang['Admission']['CHIUCHUNKG']['RelativeStudiedAtSchool'];
		for($i=0; $i < count($headerArray['otherInfo3'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//official use header
		$exportColumn[0][] = $kis_lang['remarks'];
		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
			$exportColumn[0][] = "";
		}
	
		//sub header
		$exportColumn[1] = array_merge(array($headerArray[0],$headerArray[1],$headerArray[2],$headerArray[3]), $headerArray['studentInfo'], $headerArray['parentInfoF'], $headerArray['parentInfoM'],$headerArray['parentInfoG'], $headerArray['otherInfo'], $headerArray['otherInfo2'], $headerArray['otherInfo3'], $headerArray['officialUse']);
		//if($lang)
		//include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
		return $exportColumn;
	}
	
	function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg, $Lang, $kis_lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo3 = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$student_name_en = explode(',',$studentInfo['student_name_en']);
		$student_name_b5 = explode(',',$studentInfo['student_name_b5']);
		
		$dataArray = array();
		
		//for student info
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$dataArray['studentInfo'][] = $classLevel[$otherInfo['classLevelID']];
		$dataArray['studentInfo'][] = $Lang['Admission']['TimeSlot'][$otherInfo['ApplyDayType1']];
		$dataArray['studentInfo'][] = $student_name_b5[0];
		$dataArray['studentInfo'][] = $student_name_b5[1];
		$dataArray['studentInfo'][] = $student_name_en[0];
		$dataArray['studentInfo'][] = $student_name_en[1];
		$dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
		$dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
		$dataArray['studentInfo'][] = $this->getLangByValue($studentInfo['placeofbirth'],$admission_cfg['placeofbirth'],$admission_cfg['placeofbirth']['other'],$studentInfo['PlaceOfBirthOther']);
		$dataArray['studentInfo'][] = $studentInfo['province'].' '.$studentInfo['county'];
		$dataArray['studentInfo'][] = $this->getLangByValue($studentInfo['religion1'],$admission_cfg['religion'],$admission_cfg['religion']['other'],$studentInfo['ReligionOther']);
		$dataArray['studentInfo'][] = $studentInfo['Church'];
		$dataArray['studentInfo'][] = $this->getLangByValue($studentInfo['birthcerttype'],$admission_cfg['BirthCertType'],$admission_cfg['BirthCertType']['other'],$studentInfo['birthcerttypeother']);
		$dataArray['studentInfo'][] = $studentInfo['birthcertno'];
		$PhoneNumArr = explode('|||',$studentInfo['homephoneno']);
		$dataArray['studentInfo'][] = $PhoneNumArr[0]; // home num
		$dataArray['studentInfo'][] = $PhoneNumArr[1]; // contact num 
		$AddressDistrictArr = explode('|||',$studentInfo['AddressDistrict']);
		$dataArray['studentInfo'][] = $this->getLangByValue($AddressDistrictArr[0],$admission_cfg['addressdistrict']);
		$dataArray['studentInfo'][] = $studentInfo['homeaddresschi'];
		$dataArray['studentInfo'][] = $studentInfo['homeaddress'];
		$dataArray['studentInfo'][] = $this->getLangByValue($AddressDistrictArr[1],$admission_cfg['addressdistrict']);
		$dataArray['studentInfo'][] = $studentInfo['ContactAddressChi'];
		$dataArray['studentInfo'][] = $studentInfo['ContactAddress'];
		
		//for parent info		
		for($i=0;$i<count($parentInfo);$i++){
			if($parentInfo[$i]['type'] == 'F'){
				$dataArray['parentInfoF'][] = $parentInfo[$i]['parent_name_b5'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['OfficeTelNo'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['email'];
				
			}
			else if($parentInfo[$i]['type'] == 'M'){
				$dataArray['parentInfoM'][] = $parentInfo[$i]['parent_name_b5'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['OfficeTelNo'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['email'];
				
			}
			else if($parentInfo[$i]['type'] == 'G'){
				$dataArray['parentInfoM'][] = $parentInfo[$i]['Relationship'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['Gender'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['parent_name_b5'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['OfficeTelNo'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['email'];
			}
			
		}
		
		if(count($dataArray['parentInfoF']) == 0){
			$dataArray['parentInfoF'] = array('','','','','','','','');
		}
		if(count($dataArray['parentInfoM']) == 0){
			$dataArray['parentInfoM'] = array('','','','','','','','');
		}
		
		
		//for other info
		$dataArray['otherInfo'][] = $otherInfo['EBrotherNo'];
		$dataArray['otherInfo'][] = $otherInfo['ESisterNo'];
		$dataArray['otherInfo'][] = $otherInfo['YBrotherNo'];
		$dataArray['otherInfo'][] = $otherInfo['YSisterNo'];
		$dataArray['otherInfo'][] = (1 + $otherInfo['EBrotherNo'] + $otherInfo['ESisterNo']);
		$dataArray['otherInfo'][] = ($studentInfo['IsTwins'] == 1? $kis_lang['Admission']['yes']:$kis_lang['Admission']['no']);
		$dataArray['otherInfo'][] = $studentInfo['TwinsIDNo'];
		
		$dataArray['otherInfo2'][] = $otherInfo2[0]['OthersPrevSchName'];
		$dataArray['otherInfo2'][] = $otherInfo2[0]['SchoolAddress'];
		$dataArray['otherInfo2'][] = $otherInfo2[0]['OthersPrevSchClass'].($this->getLangByValue($otherInfo2[0]['ClassType'],$admission_cfg['classtype2']));
		
		for($i=0;$i<4;$i++){
			$dataArray['otherInfo3'][] = $otherInfo3[$i]['OthersRelativeStudiedName'];
			$dataArray['otherInfo3'][] = ($this->getLangByValue($otherInfo3[$i]['OthersRelativeRelationship'],$admission_cfg['relation']));
			$dataArray['otherInfo3'][] = $otherInfo3[$i]['OthersRelativeStudiedYear'];
		}
		//for official use
		$dataArray['officialUse'][] = $admission_cfg['StatusCode'][$status['status']]?$admission_cfg['StatusCode'][$status['status']]:$Lang['Admission']['Status'][$status['status']];
		$dataArray['officialUse'][] = $status['receiptID'];
		$dataArray['officialUse'][] = $status['receiptdate'];
		$dataArray['officialUse'][] = $status['handler'];
		$dataArray['officialUse'][] = $status['interviewdate'];
		$dataArray['officialUse'][] = $status['interviewlocation'];
		$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
		$dataArray['officialUse'][] = $status['remark'];
		
		$ExportArr = array_merge(array($dataArray[0],$dataArray[1]),$dataArray['studentInfo'],$dataArray['parentInfoF'],$dataArray['parentInfoM'], $dataArray['otherInfo'], $dataArray['otherInfo2'], $dataArray['otherInfo3'], $dataArray['officialUse']);
		
		return $ExportArr;
	}
	function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$dataArray = array();
		
		if($tabID == 2){
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			$studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = ''; //Mobile
			$dataArray[0][] = ''; //Fax
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = ''; //Remarks
			$dataArray[0][] = $studentInfo['dateofbirth']; //DOB
			$dataArray[0][] = (is_numeric($studentInfo['homeaddress'])?$Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']]:$studentInfo['homeaddress']); //Address
			if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
			{
				$dataArray[0][] = ''; //CardID
				if($sys_custom['SupplementarySmartCard']){
					$dataArray[0][] = ''; //CardID2
					$dataArray[0][] = ''; //CardID3
				}
			}
			if($special_feature['ava_hkid'])
				$dataArray[0][] = $studentInfo['birthcertno']; //HKID
			if($special_feature['ava_strn'])
				$dataArray[0][] = ''; //STRN
			if($plugin['medical'])
				$dataArray[0][] = ''; //StayOverNight
			$dataArray[0][] = ''; //Nationality
			$dataArray[0][] = $this->getLangByValue($studentInfo['placeofbirth'],$admission_cfg['placeofbirth'],$admission_cfg['placeofbirth']['other'],$studentInfo['PlaceOfBirthOther']); //PlaceOfBirth
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
		}
		else if($tabID == 3){
			$hasParent = false;
			$dataCount = array();
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			for($i=0;$i<count($parentInfo);$i++){
				if($parentInfo[$i]['type'] == 'F' && !$hasParent){
					$dataArray[0] = array();
					$dataArray[0][] = ''; //UserLogin
					$dataArray[0][] = ''; //Password
					$dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[0][] = 'M'; //Gender
					$dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[0][] = ''; //Fax
					$dataArray[0][] = ''; //Barcode
					$dataArray[0][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[0][] = ''; //HKID
					$dataArray[0][] = ''; //StudentLogin1
					$dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[0][] = ''; //StudentLogin2
					$dataArray[0][] = ''; //StudentEngName2
					$dataArray[0][] = ''; //StudentLogin3
					$dataArray[0][] = ''; //StudentEngName3
					//$hasParent = true;
					
					$dataCount[0] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'M' && !$hasParent){
					$dataArray[1] = array();
					$dataArray[1][] = ''; //UserLogin
					$dataArray[1][] = ''; //Password
					$dataArray[1][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[1][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[1][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[1][] = 'F'; //Gender
					$dataArray[1][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[1][] = ''; //Fax
					$dataArray[1][] = ''; //Barcode
					$dataArray[1][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[1][] = ''; //HKID
					$dataArray[1][] = ''; //StudentLogin1
					$dataArray[1][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[1][] = ''; //StudentLogin2
					$dataArray[1][] = ''; //StudentEngName2
					$dataArray[1][] = ''; //StudentLogin3
					$dataArray[1][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[1] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'G' && !$hasParent){
					$dataArray[2] = array();
					$dataArray[2][] = ''; //UserLogin
					$dataArray[2][] = ''; //Password
					$dataArray[2][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[2][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[2][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[2][] = ''; //Gender
					$dataArray[2][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[2][] = ''; //Fax
					$dataArray[2][] = ''; //Barcode
					$dataArray[2][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[2][] = ''; //HKID
					$dataArray[2][] = ''; //StudentLogin1
					$dataArray[2][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[2][] = ''; //StudentLogin2
					$dataArray[2][] = ''; //StudentEngName2
					$dataArray[2][] = ''; //StudentLogin3
					$dataArray[2][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[2] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
			}
			if($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]){
				$tempDataArray = $dataArray[0];
			}
			else if($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]){
				$tempDataArray = $dataArray[1];
			}
			else if($dataCount[2] > 0){
				$tempDataArray = $dataArray[2];
			}
			$dataArray = array();
			$dataArray[0] = $tempDataArray;
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."'";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					p.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				INNER JOIN ADMISSION_PG_INFO  as p ON p.ApplicationID=f.ApplicationID and p.PG_TYPE = 'G'
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		if($subject == ''){
			$email_subject = "聖馬提亞堂肖珍幼稚園入學申請通知";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	public function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();

		if($subject == ''){
			$email_subject = "聖馬提亞堂肖珍幼稚園入學申請通知";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					p.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				INNER JOIN ADMISSION_PG_INFO  as p ON p.ApplicationID=f.ApplicationID and p.PG_TYPE = 'G'
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	
	function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
			//check date
			if ($aData[4] =='' && $aData[5] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
		       list($year , $month , $day) = explode('-',$aData[4]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[4] =='' && $aData[5] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."' AND trim(s.birthCertNo) = '".trim($aData[3])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[4] = getDefaultDateFormat($aData[4]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[4]." ".$aData[5]."',
				InterviewLocation = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function getLangByValue($selectedValue,$ConfigArr,$OtherValue="",$OtherLang=""){
		global $Lang,$kis_lang;
				
		foreach($ConfigArr as $_key => $_val){
			if($selectedValue == $_val){
				
				if(!empty($OtherValue) && $selectedValue == $OtherValue){
					$returnValue = $OtherLang;
				}
				else{
					$returnValue = $Lang['Admission']['CHIUCHUNKG'][$_key];
					if($returnValue == ''){
						$returnValue = $kis_lang['Admission']['CHIUCHUNKG'][$_key];
					}
					break;
				}
			}
			
		}
		return $returnValue;
	}
}
?>