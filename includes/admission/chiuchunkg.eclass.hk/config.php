<?php
	//using:
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */
//	 include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");

	//Please also define lang in admission_lang
	$admission_cfg['PrintByPDF'] = 1;

	$admission_cfg['Status'] = array();
	//$admission_cfg['Status']['pending']	= 1;
	//$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['CHIUCHUNKG_waitingforinterview'] = 1;
	$admission_cfg['Status']['CHIUCHUNKG_reservedstudent']		= 2;
	$admission_cfg['Status']['CHIUCHUNKG_interviewnotattended']		= 3;
	$admission_cfg['Status']['CHIUCHUNKG_notadmitted']		= 4;
	$admission_cfg['Status']['CHIUCHUNKG_amconfirmed']		= 5;
	$admission_cfg['Status']['CHIUCHUNKG_pmconfirmed']		= 6;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1ama']		= 7;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1amb']		= 8;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1pma']		= 9;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1pmb']		= 10;
	$admission_cfg['Status']['CHIUCHUNKG_registrationnotattended']		= 11;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a1ma']		= 12;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a1mb']		= 13;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a2ma']		= 14;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a2mb']		= 15;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a3ma']	= 16;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a3mb']		= 17;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a4ma']		= 18;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1a4mb']		= 19;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p1ma']		= 20;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p1mb']		= 21;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p2ma']		= 22;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p2mb']		= 23;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p3ma']		= 24;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p3mb']		= 25;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p4ma']		= 26;
	$admission_cfg['Status']['CHIUCHUNKG_groupk1p4mb']		= 27;
	$admission_cfg['Status']['CHIUCHUNKG_giveup']		= 28;
//	$admission_cfg['Status']['waitingforinterview']	= 3;
//	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 29;

	$admission_cfg['StatusCode']['CHIUCHUNKG_waitingforinterview']	= "K1IN";
	$admission_cfg['StatusCode']['CHIUCHUNKG_reservedstudent']		= "K1IN(X)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_interviewnotattended'] = "K1IN(ABS)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_notadmitted']			= "K1IN(REJ)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_amconfirmed']			= "K1A(X)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_pmconfirmed']			= "K1P(X)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1ama']			= "K1A(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1amb']			= "K1A(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1pma']			= "K1P(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1pmb']			= "K1P(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_registrationnotattended']	= "K1RG(ABS)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a1ma']			= "K1A1(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a1mb']			= "K1A1(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a2ma']			= "K1A2(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a2mb']			= "K1A2(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a3ma']			= "K1A3(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a3mb']			= "K1A3(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a4ma']			= "K1A4(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1a4mb']			= "K1A4(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p1ma']			= "K1P1(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p1mb']			= "K1P1(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p2ma']			= "K1P2(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p2mb']			= "K1P2(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p3ma']			= "K1P3(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p3mb']			= "K1P3(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p4ma']			= "K1P4(A)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_groupk1p4mb']			= "K1P4(B)";
	$admission_cfg['StatusCode']['CHIUCHUNKG_giveup']				= "K1RG(REN)";

	$admission_cfg['StatusDisplayOnTable'][1] = 'CHIUCHUNKG_waitingforinterview';
	$admission_cfg['StatusDisplayOnTable'][2] = 'CHIUCHUNKG_reservedstudent';
	$admission_cfg['StatusDisplayOnTable'][3] = 'cancelled';

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;
//	$admission_cfg['Status']['reservedstudent']	= 5;

	$admission_cfg['BirthCertType'] = array();
	$admission_cfg['BirthCertType']['hkBirthCert']	= 1;
	$admission_cfg['BirthCertType']['hkIdCard']	= 2;
	$admission_cfg['BirthCertType']['other']	= 3;

	$admission_cfg['placeofbirth'] = array();
	$admission_cfg['placeofbirth']['hk']	= 1;
	$admission_cfg['placeofbirth']['macau']	= 2;
	$admission_cfg['placeofbirth']['china']	= 3;
	$admission_cfg['placeofbirth']['other']	= 4;

	$admission_cfg['addressdistrict'] = array();
	$admission_cfg['addressdistrict']['hk']	= 1;
	$admission_cfg['addressdistrict']['kln']	= 2;
	$admission_cfg['addressdistrict']['nt']	= 3;

	$admission_cfg['religion'] = array();
	$admission_cfg['religion']['Christian']	= 1 ;
	$admission_cfg['religion']['Catholic']	= 2;
	$admission_cfg['religion']['other']	= 3;
	$admission_cfg['religion']['notapplicable']	= 4;

	$admission_cfg['classtype']['AM'] = 1;
	$admission_cfg['classtype']['PM'] = 2 ;
	$admission_cfg['classtype']['EitherOne'] = 3;

	$admission_cfg['classtype2']['AM'] = 1 ;
	$admission_cfg['classtype2']['PM'] = 2 ;
	$admission_cfg['classtype2']['WholeDay'] = 3 ;

	$admission_cfg['relation']['F'] = 1;
	$admission_cfg['relation']['M'] = 2;
	$admission_cfg['relation']['B'] = 3;
	$admission_cfg['relation']['S'] = 4;

	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "SDF2672qwer3we523";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 2; //in MB
	//$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	$admission_cfg['StaticApplicationFormPath'] = $PATH_WRT_ROOT."/file/customization/chiuchunkg.eclass.hk/NS2016-ApplicationForm.pdf";

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}
?>