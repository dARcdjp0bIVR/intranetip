<?php
# modifying by: Pun

/********************
 * Log :
 * Date		2017-09-12 [Pun]
 * 			Modified getUpdateIndexContent(), getInputFormPageContent(), getAfterUpdateFinishPageContent()
 * 
 * Date		2017-08-31 [Pun]
 * 			Modified getApplicationForm(), getOthersForm(), getFinishPageContent(), getPrintPageContent(), 
 *          Added getUpdateIndexContent(), getInputFormPageContent(), getAfterUpdateFinishPageContent()
 * 
 * Date		2016-08-18 [Ronald]
 * 			add english translation for online applciation form and email
 * 
 * Date		2015-12-16 [Henry]
 * 			modified getFinishPageContent()
 * 			modified getFinishPageEmailContent()
 * Date		2015-09-14 [Henry]
 * 			modified getOthersForm() [Case#V80840]
 * 
 * Date		2014-07-25 [Henry]
 * 			add function getFinishPageEmailContent()
 * 
 * Date		2014-07-21 [YatWoon] 
 * 			failed to select Form option with IE8, checked that due to class "admission_select_class", temp remove for this class first
 * 
 * Date		2014-07-18 [YatWoon] [Case#F64448]
 * 			updated wordings / ui 
 * 
 * Date		2014-07-17 [YatWoon]
 * 			comment out the form selection "disabled" 
 * 
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_ui_cust_base
 * 			Modified getDocsUploadForm() to follow attachment settings
 * 			Moved getDocsUploadForm() to libadmission_ui_cust_base.php
 * 			
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step){
		global $Lang, $sys_custom,$lac, $validForAdmission;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token'] && $Step!=7){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .' This is preview form. Any information will not be submitted.</center></h2>';
		}
				$attachment_settings = $lac->getAttachmentSettings();
				$attachment_settings_count  = sizeof($attachment_settings);
				
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].' Instruction</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].' Personal Info</a></div>';
					if($attachment_settings_count > 0)
						$x .='<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].' Docs upload</a></div>';
					$x .='<div class="'.$active_step6.'"><a '.$href_step6.'><span>'.($attachment_settings_count > 0?'4':'3').'</span>'.$Lang['Admission']['confirmation'].' Confirmation</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:70px">'.$Lang['Admission']['finish'].' Finish</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].' Welcome to our online application page!'; //Henry 20131107
		}
		
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token']){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'</center></h2>';
			$previewnote .='<h2 style="font-size:18px;color:red"><center>This is preview form. Any information will not be submitted.</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm($ClassLevel);
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac, $sys_custom;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'].' Please click \'Next\' to continue'; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
         $x .= $Instruction;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->';								
							if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
							}
							else{
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
							}
						$x .= '</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm($ClassLevel = ''){
		global $libkis_admission, $Lang, $lac, $sys_custom;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		/*
		//disable to choose class when using central server
		$disable = '';
		if($ClassLevel)
			$disable = 'return false';
		*/
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])){
					$hasClassLevelApply = 1;
					$selected = '';
					/*
					if($key == $ClassLevel)
						$selected = "checked='checked'";
						*/
					
					//Henry added [20140808]
					$numOFQuotaLeft = $lac->NumOfQuotaLeft($key,$value['SchoolYearID']);
					$isFullApply = false;
					if($numOFQuotaLeft <= 0){
						$isFullApply = true;
					}
					$disable = '';
					if($isFullApply){
						$selected = 'disabled';
					}
					
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="'.$disable.'" '.$selected.'  />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].($isFullApply?' <span style="color:red">('.$Lang['Admission']['IsFull']. ' Full)</span>':'').'<!--(Quota Left:'.$numOFQuotaLeft.')-->'.'</label> ';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].' Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].' Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025
		//$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .='<fieldset ><legend>'.$Lang['Admission']['level'].' From</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Admission']['newApplication'].' New Application', "button", "goto('step_index','step_instruction')", "SubmitBtn", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
		$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm($BirthCertNo = ""){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission, $sys_custom, $admission_cfg;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm(0,$BirthCertNo);
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span>'.$Lang['Admission']['mandatoryfield'].'</span>';
		$x .='<br/><span><span class="tabletextrequire">*</span> Mandatory field(s)</span>';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn_to_upload", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->';
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0, $BirthCertNo = ""){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg;
		
		$religion_selection_new = '<select name="StudentReligion">';
		$religion_selection_new .= '<option value="0">不適用 Not Applicable</option>';
		$religion_selection_new .= '<option value="1">天主教 Catholicism</option>';
		$religion_selection_new .= '<option value="2">基督教 Christian</option>';
		$religion_selection_new .= '<option value="3">佛教 Buddhism</option>';
		$religion_selection_new .= '<option value="4">道教 Taoism</option>';
		$religion_selection_new .= '<option value="5">孔教 Confucianism</option>';
		$religion_selection_new .= '<option value="6">伊斯蘭教 Islam</option>';
		$religion_selection_new .= '<option value="7">印度教 Hinduism</option>';
		$religion_selection_new .= '<option value="9">其他 Others</option>';
		$religion_selection_new .= '</select>';
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		//handle the Birth Cert Type
		$birth_cert_type_selection = '';
 		$Lang['Admission']['BirthCertType']['hk']	= "香港出世紙 Hong Kong Birth Certificate";
 		$Lang['Admission']['BirthCertType']['hk2']	= "香港出生證明書 Hong Kong Birth Certificate";
 		$Lang['Admission']['BirthCertType']['hkid']	= "香港身份證 Hong Kong ID Card";
 		$Lang['Admission']['BirthCertType']['oversea']	= "外國出世紙 Overseas Birth Certificate";
 		$Lang['Admission']['BirthCertType']['mainland']	= "國內出世紙 Mainland Birth Certificate";
		$Lang['Admission']['BirthCertType']['others']	= "其他 Others";
		if($IsConfirm){
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($formData['StudentBirthCertType'] == $_type){
					$birth_cert_type_selection .= ' (';
					$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
					$birth_cert_type_selection .= ') ';
					break;
				}
			}
		}
		else{
			$birth_cert_type_selection .= '<select name="StudentBirthCertType" id="StudentBirthCertType">'; 
	    	foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				$birth_cert_type_selection .= '<option value="'.$_type.'">';
				$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
				$birth_cert_type_selection .= '</option>';
			}
			$birth_cert_type_selection .= '</select>';
		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['studentInfo'].' Student Infomation</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['chinesename'].' Chinese Name</td>
				<td>'.($IsConfirm?$formData['StudentChiName']:'<input name="StudentChiName" type="text" id="StudentChiName" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['gender'].' Sex</td>
				<td>';
				if($IsConfirm){
					if($formData['StudentGender'] == 'M'){
						$x .= $Lang['Admission']['genderType'][$formData['StudentGender']].' Male';
					}else if ($formData['StudentGender'] == 'F'){
						$x .= $Lang['Admission']['genderType'][$formData['StudentGender']].' Female';
					}
				}
				else{
					$x .=$this->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', '0','',$Lang['Admission']['genderType']['M'].' Male').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', '0','',$Lang['Admission']['genderType']['F'].' Female');
				}
				
				$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['englishname'].' English Name</td>
				<td>'.($IsConfirm?$formData['StudentEngName']:'<input name="StudentEngName" type="text" id="StudentEngName" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['nativeplace'].' Nationality</td>
				<td>
				<!--<table style="font-size: 13px">
				<tr><td width="20px">
				('.$Lang['Admission']['province'].' Province)</td><td>'.($IsConfirm?$formData['StudentProvince']:'<input name="StudentProvince" type="text" id="StudentProvince" class="textboxtext" />').'</td></tr>
				<tr><td width="20px">('.$Lang['Admission']['county'].')</td><td>'.($IsConfirm?$formData['StudentCounty']:'<input name="StudentCounty" type="text" id="StudentCounty" class="textboxtext" />').'</td></tr>
				</table>-->
				'.($IsConfirm?$formData['StudentProvince']:'<input name="StudentProvince" type="text" id="StudentProvince" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateofbirth'].' Date of Birth</td>
				<td>
				'.($IsConfirm?$formData['StudentDateOfBirth']:'<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['Age'].' Age</td>
				<td>
				'.($IsConfirm?$formData['StudentAge']:'<input name="StudentAge" type="text" id="StudentAge" class="textboxtext" maxlength="2" size="15"/>').'</td>
			</tr>
			<tr>	
                <td class="field_title">'.$star.$Lang['Admission']['mgf']['birthcertno'].' Birth Certification Number</td>
				<td nowrap colspan="3">'.$birth_cert_type_selection.' '.($BirthCertNo?'<input name="StudentBirthCertNo" value="'.$BirthCertNo.'" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" /><br/>'.$Lang['Admission']['mgf']['msg']['birthcertnohints']:($IsConfirm?$formData['StudentBirthCertNo']:'<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" /><br/>'.$Lang['Admission']['mgf']['msg']['birthcertnohints'].'<br/>(eg：A123456(7)，please enter "A1234567")')).'</td>						
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['placeofbirth'].' Place of Birth</td>
				<td >'.($IsConfirm?$formData['StudentPlaceOfBirth']:'<input name="StudentPlaceOfBirth" type="text" id="StudentPlaceOfBirth" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['mgf']['homeaddress'].' Address</td>
				<td colspan="3">'.($IsConfirm?$formData['StudentHomeAddress']:'<input type="text" name="StudentHomeAddress" class="textboxtext" id="StudentHomeAddress" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['mgf']['email'].' Email Address</td>
				<td>'.($IsConfirm?$formData['StudentEmail']:'<input name="StudentEmail" type="text" id="StudentEmail" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['homephoneno'].' Home Telephone</td>
				<td>'.($IsConfirm?$formData['StudentHomePhoneNo']:'<input name="StudentHomePhoneNo" type="text" id="StudentHomePhoneNo" class="textboxtext" />').'</td>	
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['mgf']['religion'].' Religion</td>
				<td><!--<input name="StudentReligion" type="text" id="StudentReligion" class="textboxtext" />-->
				'.($IsConfirm?$religion_selected:$religion_selection_new).'</td>
				<td class="field_title">'.$Lang['Admission']['church'].' Name of Church</td>
				<td>'.($IsConfirm?$formData['StudentChurch']:'<input name="StudentChurch" id="StudentChurch" type="text" class="textboxtext" />').'
				</td>
			</tr>
			<!--<tr>
				<td class="field_title">'.$star.$Lang['Admission']['mgf']['phoneno'].' </td>
				<td>'.($IsConfirm?$formData['StudentHomePhoneNo']:'<input name="StudentHomePhoneNo" type="text" id="StudentHomePhoneNo" class="textboxtext" />').'</td>	
				<td class="field_title">'.$Lang['Admission']['lastschool'].'</td>
				<td>'.($IsConfirm?$formData['StudentLastSchool']:'<input name="StudentLastSchool" type="text" id="StudentLastSchool" class="textboxtext" />').'</td>
			</tr>-->
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].' Personal Photo</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/><br />
					('.$Lang['Admission']['msg']['personalPhotoFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)<br />
					('.'Personal Photo must be in JPG/GIF/PNG format, file size less than :'.($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)
					<br />').'
				</td>
			</tr>
		</table>';
		
		return $x;
	}
	
	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['PGInfo'].' Parent Infomation</h1>
				<table class="form_table" style="font-size: 13px">
				<tr>
					<td>&nbsp;</td>
					<td class="form_guardian_head">'.$Lang['Admission']['PG_Type']['F'].' Father</td>
					<td class="form_guardian_head">'.$Lang['Admission']['PG_Type']['M'].' Mother</td>
				</tr>
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['chinesename'].' Chinese Name</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1ChineseName']:'<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2ChineseName']:'<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" />').'</td>
				</tr>
				
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['HKID'].' Hong Kong Resident Identity Card</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1HKID']:'<input name="G1HKID" type="text" id="G1HKID" class="textboxtext" maxlength="4" size="4" />'.'<br/>'.$Lang['Admission']['mgf']['msg']['hkidhints'].'<br/>'.'Enter letter & first 3 numbers.<br/>(e.g.: A123456(7)，please enter "A123")').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2HKID']:'<input name="G2HKID" type="text" id="G2HKID" class="textboxtext" maxlength="4" size="4" />'.'<br/>'.$Lang['Admission']['mgf']['msg']['hkidhints'].'<br/>'.'Enter letter & first 3 numbers.<br/>(e.g.: A123456(7)，please enter "A123")').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['mobile'].' Mobile No.</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1MobileNo']:'<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2MobileNo']:'<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['occupation'].' Occupation</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1Occupation']:'<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2Occupation']:'<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />').'</td>
				</tr>
			</table>';
			
		return $x;
	}
	function getOthersForm($IsConfirm=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			$kis_lang['Admission']['Option'] .=  ' Option';
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.")".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
//		else{
//			foreach($dayTypeArr as $aDayType){
//	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
//	//			$dayTypeOption .=" ";
//				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
//			}
//			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
//		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['otherInfo'].' Other Information</h1>
			<table class="form_table" style="font-size: 13px">';
			if($IsConfirm){
				$x .='<tr>
					<td class="field_title">'.$Lang['Admission']['applyLevel'].' Apply Level</td><td>'.$classLevel.'</td>
				</tr>';
			}
			
			$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].' Class Applied</td>
					<td>
					<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
				</tr>';
			
			$x .='<tr>
				<td class="field_title">'.$Lang['Admission']['mgf']['curbsname'].' <br/>Siblings who study in our school</td>
				<td colspan="3">
				'.$Lang['Admission']['name'].' Name '.($IsConfirm?$formData['OthersCurBSName']:'<input style="width:200px" name="OthersCurBSName" type="text" id="OthersCurBSName" class="textboxtext" />').' '.$Lang['Admission']['class'].' Class：'.($IsConfirm?$formData['OthersCurBSLevel']:'<input style="width:200px" name="OthersCurBSLevel" type="text" id="OthersCurBSLevel" class="textboxtext" />').'
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['mgf']['hassiblingapplied'].' <br/>Twins who apply for our school</td>
				<td colspan="3">';
				if($IsConfirm){
					$x .= ($formData['OthersSiblingApplied'] == 'yes'?$Lang['Admission']['yes'].' Yes':$Lang['Admission']['no'].' No');
					$x .= '<br />'.($formData['OthersSiblingAppliedName'] != ''?$Lang['Admission']['name'].' Name: '.$formData['OthersSiblingAppliedName'] . ' (同時報讀的雙生兒需填寫新生入學申請表 Twins should sumit two application instead of come by in one)':'');
					$x .= '<br />'.$Lang['Admission']['HKUGAPS']['twinsID'].' Twins\' Number of Identity Document: '.$formData['twinsapplicationid'];
				}
				else{
					$x .=$this->Get_Radio_Button('OthersSiblingApplied1', 'OthersSiblingApplied', 'yes', '0','',$Lang['Admission']['yes'].' Yes','$(\'#div_OthersSiblingAppliedName\').css(\'display\', \'\');').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('OthersSiblingApplied2', 'OthersSiblingApplied', 'no', '1','',$Lang['Admission']['no'].' No','$(\'#div_OthersSiblingAppliedName\').css(\'display\', \'none\');$(\'#OthersSiblingAppliedName\').val(\'\')');
					$x.= '<div id="div_OthersSiblingAppliedName" style="display:none"><br/>'.$star.$Lang['Admission']['name'].' Name <input style="width:200px" name="OthersSiblingAppliedName" type="text" id="OthersSiblingAppliedName" class="textboxtext" />' . ' (同時報讀的雙生兒需填寫新生入學申請表 Twins should sumit two application instead of come by in one)';
					$x .='<br/>'.$star.$Lang['Admission']['HKUGAPS']['twinsID'].' Twins\' Number of Identity Document <input style="width:200px" name="twinsapplicationid" type="text" id="twinsapplicationid" class="textboxtext"/>';
					$x .='<br/>'.$Lang['Admission']['mgf']['msg']['birthcertnohints'].'<br/>(eg：A123456(7)，please enter "A1234567")';
				}
			$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['mgf']['issiblinggradhere'].' <br/>Siblings who graduate from our school</td>
				<td colspan="3">';
				if($IsConfirm){
					$x .= ($formData['OthersSiblingStudied'] == 'yes'?$Lang['Admission']['yes'].' Yes':$Lang['Admission']['no'].' No');
					$x.= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$formData['NumSiblingStudied'].$Lang['Admission']['NumOfPerson'].' Person(s)<br/>
						'.$Lang['Admission']['name'].' Name: '.$formData['OthersExBSName'].' ('.$Lang['Admission']['GradYear'].' Graduation Year：'.$formData['OthersExBSGradYear'].')<br/>
						'.$Lang['Admission']['name'].' Name: '.$formData['OthersExBSName2'].' ('.$Lang['Admission']['GradYear'].' Graduation Year：'.$formData['OthersExBSGradYear2'].')<br/>
						'.$Lang['Admission']['name'].' Name: '.$formData['OthersExBSName3'].' ('.$Lang['Admission']['GradYear'].' Graduation Year：'.$formData['OthersExBSGradYear3'].')';
				}
				else{
					$x .=$this->Get_Radio_Button('OthersSiblingStudied1', 'OthersSiblingStudied', 'yes', '0','',$Lang['Admission']['yes'].' Yes', '$(\'#span_OthersSiblingStudied\').css(\'display\', \'\');').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('OthersSiblingStudied2', 'OthersSiblingStudied', 'no', '1','',$Lang['Admission']['no'].' No', '$(\'#div_OthersSiblingStudied\').css(\'display\', \'none\');$(\'#OthersExBSName\').val(\'\');$(\'#OthersExBSName2\').val(\'\');$(\'#OthersExBSName3\').val(\'\');$(\'#OthersExBSGradYear\').val(\'\');$(\'#OthersExBSGradYear2\').val(\'\');$(\'#OthersExBSGradYear3\').val(\'\');$(\'#span_OthersExBSName\').css(\'display\', \'none\');$(\'#span_OthersExBSName2\').css(\'display\', \'none\');$(\'#span_OthersExBSName3\').css(\'display\', \'none\');$(\'#span_OthersSiblingStudied\').css(\'display\', \'none\');$(\'input:radio[name=NumSiblingStudied]\').removeAttr(\'checked\');');
					$x.= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="span_OthersSiblingStudied" style="display:none">'.$this->Get_Radio_Button('NumSiblingStudied1', 'NumSiblingStudied', '1', '0','','1 '.$Lang['Admission']['NumOfPerson'].' 1 Person','$(\'#div_OthersSiblingStudied\').css(\'display\', \'\');$(\'#span_OthersExBSName\').css(\'display\', \'\');$(\'#OthersExBSName2\').val(\'\');$(\'#OthersExBSName3\').val(\'\');$(\'#OthersExBSGradYear2\').val(\'\');$(\'#OthersExBSGradYear3\').val(\'\');$(\'#span_OthersExBSName2\').css(\'display\', \'none\');$(\'#span_OthersExBSName3\').css(\'display\', \'none\');$(\'#OthersSiblingStudied1\').attr(\'checked\', \'checked\');').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('NumSiblingStudied2', 'NumSiblingStudied', '2', '0','','2 '.$Lang['Admission']['NumOfPerson'].' 2 Persons','$(\'#div_OthersSiblingStudied\').css(\'display\', \'\');$(\'#span_OthersExBSName\').css(\'display\', \'\');$(\'#span_OthersExBSName2\').css(\'display\', \'\');$(\'#OthersExBSName3\').val(\'\');$(\'#OthersExBSGradYear3\').val(\'\');$(\'#span_OthersExBSName3\').css(\'display\', \'none\');$(\'#OthersSiblingStudied1\').attr(\'checked\', \'checked\');').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('NumSiblingStudied3', 'NumSiblingStudied', '3', '0','','3 '.$Lang['Admission']['NumOfPerson'].' 3 Persons','$(\'#div_OthersSiblingStudied\').css(\'display\', \'\');$(\'#span_OthersExBSName\').css(\'display\', \'\');$(\'#span_OthersExBSName2\').css(\'display\', \'\');$(\'#span_OthersExBSName3\').css(\'display\', \'\');$(\'#OthersSiblingStudied1\').attr(\'checked\', \'checked\');').'</span><br/>
						<div id="div_OthersSiblingStudied" style="display:none"><span id="span_OthersExBSName">'.$Lang['Admission']['name'].' Name <input style="width:200px" name="OthersExBSName" type="text" id="OthersExBSName" class="textboxtext" /> ('.$Lang['Admission']['GradYear'].'  Graduation Year：<input style="width:200px" name="OthersExBSGradYear" type="text" id="OthersExBSGradYear" class="textboxtext" />)</span><br/>
						<span id="span_OthersExBSName2">'.$Lang['Admission']['name'].' Name <input style="width:200px" name="OthersExBSName2" type="text" id="OthersExBSName2" class="textboxtext" /> ('.$Lang['Admission']['GradYear'].' Graduation Year：<input style="width:200px" name="OthersExBSGradYear2" type="text" id="OthersExBSGradYear2" class="textboxtext" />)</span><br/>
						<span id="span_OthersExBSName3">'.$Lang['Admission']['name'].' Name <input style="width:200px" name="OthersExBSName3" type="text" id="OthersExBSName3" class="textboxtext" /> ('.$Lang['Admission']['GradYear'].' Graduation Year：<input style="width:200px" name="OthersExBSGradYear3" type="text" id="OthersExBSGradYear3" class="textboxtext" />)</span></div>';
						
				}
			$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['mgf']['teacherOrManagerChildren'].' Teacher/Manager\'s children</td>
				<td colspan="3">';
				if($IsConfirm){
					$x .= ($formData['teacherOrManagerChildren'] == 'yes'?$Lang['Admission']['yes'].' Yes':$Lang['Admission']['no'].' No');
				}else{
					$x .=$this->Get_Radio_Button('teacherOrManagerChildren1', 'teacherOrManagerChildren', 'yes', '0','',$Lang['Admission']['yes'].' Yes').'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('teacherOrManagerChildren2', 'teacherOrManagerChildren', 'no', '1','',$Lang['Admission']['no'].' No');
				}
			$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['mgf']['lastschool'].' Name of Previous School</td>
				<td colspan="3">'.($IsConfirm?$formData['StudentLastSchool']:'<input name="StudentLastSchool" type="text" id="StudentLastSchool" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['remarks'].' Remarks</td>
				<td colspan="3">
				'.($IsConfirm?$formData['OthersRemarks']:'<input name="OthersRemarks" type="text" id="OthersRemarks" class="textboxtext" />').'
				</td>
			</tr>
		</table>';
		
		return $x;
	}
	
	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac, $formData;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		
		if($attachment_settings_count == 0 && $IsConfirm)
			return '';
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1 style="font-size: 15px">'.$Lang['Admission']['docsUpload'].' Docs upload</h1>';
		}
		$x .='<table class="form_table" style="font-size: 15px">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr><br/>';
			$x .='<tr>
					<td colspan="2">Document <span class="date_time">(image in JPEG/GIF/PNG/PDF format, file size less than : '.($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
		
		for($i=0;$i<$attachment_settings_count;$i++) {
//			if(($formData['StudentBirthCertType'] == 1 || $formData['StudentBirthCertType'] == 4) && $i == 1)
//				break;
//			if($formData['StudentBirthCertType'] == 2 && ($i == 2 || $i == 3))
//				break;
//			if($formData['StudentBirthCertType'] == 3 && $i == 1)
//				continue;
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr id="div_OtherFile'.$i.'">
					<td class="field_title">'.$star.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
				  </tr>';
		}
			
		/*$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';*/
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn_to_confirm", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />
				
			</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Submit'].' Submit', "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />

			</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
//		$LastContent .='<br/> Content on Last Page';
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			//Henry added [20140901] for handle interview timeslot is full
			$isFullTimeslot = false;
			$isFullAllTimeslot = false;
			/* Removed Interview settings
			if($sys_custom['KIS_Admission']['InterviewSettings']){
				//debug_pr($schoolYearID);
				$otherInfoOld = current($lac->getApplicationOthersInfo($schoolYearID?$schoolYearID:$lac->schoolYearID,'',$ApplicationID,''));
				if($_REQUEST['InterviewSettingID'] > 0 && $otherInfoOld['InterviewSettingID'] == 0){
					$lac->updateOtherInterviewSettingID($_REQUEST['InterviewSettingID'], $ApplicationID);
					$lac->updateApplicationStatusByIds($otherInfoOld['RecordID'],1);
					$otherInfo = current($lac->getApplicationOthersInfo($schoolYearID?$schoolYearID:$lac->schoolYearID,'',$ApplicationID,''));
				}
				else{
					$otherInfo = $otherInfoOld;
				}
				//debug_pr($otherInfo);
				if($otherInfo['InterviewSettingID'] == 0){
					$isFullTimeslot = true;
					$id = urlencode(getEncryptedText("ApplicationID=".$ApplicationID."&sus_status=".$sus_status."&SchoolYearID=".$schoolYearID,$admission_cfg['FilePathKey']));
					$x .= '<form id="form1" name="form1" action="finish.php?id='.$id.'" method="post">';
					$x .= '<div class="admission_complete_msg"><h1>';
					
					if($lac->NumOfInterviewQuotaLeftByClassLevel($schoolYearID?$schoolYearID:$lac->schoolYearID, $sus_status) > 0){
						$x .= $Lang['Admission']['msg']['selectanotherinterviewtimeslot'].':<br/>';
						$x .= 'The interview timeslot you have selected is full. Please select another timeslot (If you received the apply success email, please ignore this mail): <br/>'.$lac->getInterviewSettingSelection('',$name='InterviewSettingID',true,'', $schoolYearID?$schoolYearID:$lac->schoolYearID, $sus_status);
					}
					else{
						$isFullAllTimeslot = true;
						$x .= "由於閣下之申請已超出面試人數，所以申請只作後備，待有學額，校方再個別通知面試時間和安排。<br/>";
						$x .= 'As your application has exceeded the quota of interviews, you would be placed on the waiting list, and we may arrange another interview when available.';	
					}
					$x .= '</h1></div>';
				}
			}
			*/
			if(!$isFullTimeslot){
				if($_REQUEST['InterviewSettingID'] > 0 && $otherInfoOld['InterviewSettingID'] == 0){
					//auto send the email to applicant
					if($sys_custom['KIS_Admission']['MGF']['Settings'])
						$mail_content = $lauc->getFinishPageEmailContent($ApplicationID, $LastContent, $schoolYearID, $sus_status);
					else
						$mail_content = $lauc->getFinishPageEmailContent($ApplicationID, $LastContent, $schoolYearID);
					$lac->sendMailToNewApplicant($ApplicationID,'',$mail_content);
					
				}
				//$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><!--<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input>--> <input type="button" value="'.$Lang['Admission']['printReplyNote'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"reply_note").'\',\'_blank\');"></input></span></h1>';
				//$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"reply_note","b5").'\',\'_blank\');"></input></span></h1>';
				$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"","b5").'\',\'_blank\');"></input></span></h1>';
				$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';
				
				//$x .=' <div class="admission_complete_msg"><h1>Admission is Completed. <span> Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"reply_note","en").'\',\'_blank\');"></input></span></h1>';
				$x .=' <div class="admission_complete_msg"><h1>Admission is Completed. <span> Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"","en").'\',\'_blank\');"></input></span></h1>';
				
	//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
	//                           </p>';
	//            $x .='<p>
	//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
	//				</p>';
				$x .='<h1><span>An confirmation email will be sent to you after your submission. Please check your application email: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			}
		}
		else{
			if($sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod()){
				$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['defaultPreviewlastpagemessage'].'</h1>';
				$x .=' <br/><h1>This is the end of preview form. Any information will not be submitted. Thank you for using our services!</h1>';
			}else
				$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
				$x .=' <br/><h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		
		if(!$isFullTimeslot){
			if(!$LastContent){
				if($sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod())
					$LastContent = $Lang['Admission']['msg']['defaultPreviewlastpagemessage'].'<br/>This is the end of preview form. Any information will not be submitted. Thank you for using our services!';
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage'].'<br/>Thank you for using our services!'; //Henry 20131107
			}
			$x .= '<br/>'.$LastContent.'</div>';
			$x .= '</div>';
			if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer'] && (!$sys_custom['KIS_Admission']['PreviewFormMode'] || !$lac->IsPreviewPeriod())){
				$x .= '<div class="edit_bottom">
						<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
					</div>
					<p class="spacer"></p></div>';
			}
			else{	
				$x .= '<div class="edit_bottom">
						<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
					</div>
					<p class="spacer"></p></div>';
			}
		}
		else{
			if(!$isFullAllTimeslot){
			$x .= '<div class="edit_bottom">
						<input type="submit" class="formsubbutton" onclick="return confirm(\''.$Lang['Admission']['msg']['suresubmit'].'\'\n\'Are you sure you want to submit?\')" />
					</div>
					<p class="spacer"></p></div></form>';
			}
			else{
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer'] && (!$sys_custom['KIS_Admission']['PreviewFormMode'] || !$lac->IsPreviewPeriod())){
					$x .= '<div class="edit_bottom">
							<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
						</div>
						<p class="spacer"></p></div>';
				}
				else{	
					$x .= '<div class="edit_bottom">
							<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
						</div>
						<p class="spacer"></p></div>';
				}
			}
		}
		return $x;
	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		if($ApplicationID){
			/*if($sys_custom['KIS_Admission']['InterviewSettings']){
				$otherInfo = current($lac->getApplicationOthersInfo($schoolYearID?$schoolYearID:$lac->schoolYearID,'',$ApplicationID,''));
				//debug_pr($otherInfo);
				if($otherInfo['InterviewSettingID'] == 0){
					$isFullTimeslot = true;
					if($lac->NumOfInterviewQuotaLeftByClassLevel($schoolYearID?$schoolYearID:$lac->schoolYearID, $sus_status) > 0){
						$id = urlencode(getEncryptedText("ApplicationID=".$ApplicationID."&sus_status=".$sus_status."&SchoolYearID=".$schoolYearID,$admission_cfg['FilePathKey']));
						$x .= $Lang['Admission']['msg']['selectanotherinterviewtimeslot'] .'<br/>';
						$x .= 'The interview timeslot you have selected is full. Please select another timeslot (If you received the apply success email, please ignore this mail)<br/>';	
						$x .= 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_form/finish.php?id='.$id.'<br/><br/>';
					}
					else{
						$x .= '由於閣下之申請已超出面試人數，所以申請只作後備，待有學額，校方再個別通知面試時間和安排。<br/>';
						$x .= 'As your application has exceeded the quota of interviews, you would be placed on the waiting list, and we may arrange another interview when available.';	
					}
				}
			}*/
			//$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID).'<br><br/>接見通知書<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID,"reply_note");
			//if($otherInfo['InterviewSettingID'] != 0){
				//$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br/>面試通知書<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID,"reply_note","b5");
				$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br/>已遞交的申請表<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID,"","b5");
		
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
				$x .= '<br/><br/>'.$LastContent.'<br/><br/>';
				
				
				//$x .= 'Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br/>Interview notes<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID,"reply_note","en");
				$x .= 'Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br/>Submitted form<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID,"","en");
				
				$LastContent = 'Thank you for using our services!'; //Henry 20131107
				
				$x .= '<br/><br/>'.$LastContent;
			
			//}
		}
		else{
			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
			$x .='Admission is Not Completed. Please try to apply again!';
        }
		

		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';            
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['mgf']['msg']['admissionQuotaFull'].'</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['mgf']['msg']['interviewQuotaFull'].'</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}

	function getPrintPageContent($schoolYearID,$applicationID, $type="",$form_lang=""){ //using $type="teacher" if the form is print from teacher
		global $PATH_WRT_ROOT,$Lang,$kis_lang, $admission_cfg,$intranet_session_language;
		$form_lang = ($form_lang)? $form_lang : $intranet_session_language;
		
//		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		$lac = new admission_cust();
		if($applicationID != ""){
		//get student information
		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
		foreach($parentInfo as $aParent){
			if($aParent['type'] == 'F'){
				$fatherInfo = $aParent;
			}
			else if($aParent['type'] == 'M'){
				$motherInfo = $aParent;
			}
			else if($aParent['type'] == 'G'){
				$guardianInfo = $aParent;
			}
		}
		
		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
		if($_SESSION['UserType']==USERTYPE_STAFF){
			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
			if(!is_date_empty($remarkInfo['interviewdate'])){
				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
				list($y,$m,$d) = explode('-',$date);
				if($hour>12){
					$period = '下午';
					$hour -= 12;
				}elseif($hour<12){
					$period = '上午';
				}else{
					$period = '中午';
				}
				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
			}else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
			}
		}
		else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
		}
		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
		$personalPhotoPath = $attachmentList['personal_photo']['link'];
		$classLevel = $lac->getClassLevel();
		
//		debug_pr($studentInfo);
//		debug_pr($fatherInfo);
//		debug_pr($motherInfo);
//		debug_pr($guardianInfo);
//		debug_pr($othersInfo);
		
		for($i=1; $i<=3; $i++){
				if($othersInfo['ApplyDayType'.$i] != 0){
					$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
//					$dayTypeOption .= "(選擇 ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
				}
			}
		
		foreach($admission_cfg['BirthCertType'] as $_key => $_type){
			if($studentInfo['birthcerttype'] == $_type){
				$birth_cert_type_selection .= ' (';
				$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
				$birth_cert_type_selection .= ') ';
				break;
			}
		}
		
		if($type != 'reply_note'){
		//Header of the page
		if($form_lang == 'b5'){
			$x = '<div class="input_form" style="width:720px;margin:auto;">';
			$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=180&height=40&format=PNG'></div>";
			$x .= '<h4 align="center" style="padding-top:45px">崇真會美善幼稚園 (馬鞍山)</h4>';
					
			$x .= '<h4 align="center">'.($type != 'reply_note'?'新生入學申請表':'新生入學面試通知').'</h4>';
			
			$interviewList = current($lac->getInterviewListAry($othersInfo['InterviewSettingID']));
			$interviewDate .= $interviewList['Date'];
			if($interviewList['StartTime'] && $interviewList['EndTime']){
			    $interviewDate .= ' ('.substr($interviewList['StartTime'], 0, -3).' ~ '.substr($interviewList['EndTime'], 0, -3).')'; 	
			}else{
			    $interviewDate .= '--';
			}
			
			$x .='<table width="100%">
					<tr>
						<td><span style="font-size: 13px">申請編號：'.$othersInfo['applicationID'].'</span></td>
						<td><span style="font-size: 13px">面試日期：'.$interviewDate.'</span></td>
						<td><span style="float:right;font-size: 12px">填表日期：'.substr($othersInfo['DateInput'], 0, -9).'</span></td>
					</tr>
				</table>';
	
			$x .= '<table align="center" class="tg-table-plain">
				  <tr>
				    <td style="width:130px" class="print_field_title print_field_row1">中文姓名</td>
				    <td>'.$studentInfo['student_name_b5'].'</td>
				    <td class="print_field_title">性別</td>
				    <td>'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td>
				    <td rowspan="5" colspan="2" width="100" align="center"><img src="'.$personalPhotoPath.'" width="100px" /><!--相片--></td>
				  </tr>
				  <tr class="tg-even">
					<td class="print_field_title">英文姓名</td>
				    <td>'.$studentInfo['student_name_en'].'</td>
					<td class="print_field_title">籍貫</td>
				    <td>'.$studentInfo['province'].'</td>
				  </tr>
				  <tr>
					<td class="print_field_title">出生日期</td>
					<td>'.substr($studentInfo['dateofbirth'], 0, 4).' 年 '.substr($studentInfo['dateofbirth'], 5, 2).' 月 '.substr($studentInfo['dateofbirth'], 8, 2).' 日</td>
				    <td class="print_field_title">年齡</td>
				    <td>'.$studentInfo['age'].'</td>
				  </tr>
				  <tr class="tg-even">
				    <td class="print_field_title">出世證編號</td>
				    <td>'.$birth_cert_type_selection.$studentInfo['birthcertno'].'</td>
					<td class="print_field_title">出生地點</td>
				    <td>'.$studentInfo['placeofbirth'].'</td>
				  </tr>
				  <tr class="tg-even">
				    <td class="print_field_title">電郵</td>
				    <td colspan="3">'.$studentInfo['email'].'</td>
				  </tr>
				  <tr>
				    <td class="print_field_title">地址</td>
				    <td colspan="5">'.$studentInfo['homeaddress'].'</td>
				  </tr>
				  <tr>
					<td class="print_field_title">聯絡電話</td>
				    <td>'.$studentInfo['homephoneno'].'</td>
					<td class="print_field_title">宗教</td>
				    <td>'.$lac->returnPresetCodeName("RELIGION", $studentInfo['religion']).'</td>
					<td class="print_field_title">所屬教會</td>
				    <td>'.$studentInfo['church'].'</td>
				  </tr>
				  <tr>
					<td rowspan="5" class="print_field_title">家長資料</td>
				  </tr>
				  <tr>
				    <td colspan="3">父親姓名：'.$fatherInfo['parent_name_b5'].'</td>
				    <td class="print_field_title">手提電話</td>
					<td>'.$fatherInfo['mobile'].'</td>
				  </tr>
				  <tr>
					<td colspan="3">身份證：'.$fatherInfo['hkid'].'</td>
				  	<td class="print_field_title">職業</td>
					<td>'.$fatherInfo['occupation'].'</td>
				  </tr>
				  <tr>
				    <td colspan="3">母親姓名：'.$motherInfo['parent_name_b5'].'</td>
				    <td class="print_field_title">手提電話</td>
					<td>'.$motherInfo['mobile'].'</td>
				  </tr>
				  <tr>
					<td colspan="3">身份證：'.$motherInfo['hkid'].'</td>
				  	<td class="print_field_title">職業</td>
					<td>'.$motherInfo['occupation'].'</td>
				  </tr>
				  
				  <tr>
					<td class="print_field_title">擬報讀班級</td>
					<td colspan="5"><span class="input_content">'.$classLevel[$othersInfo['classLevelID']].'</span></td>
				  </tr>
				  <tr>
				    <td class="print_field_title">班級</td>
				    <td colspan="5">'.$dayTypeOption.'</td>
				  </tr>';
				  if($type != 'reply_note'){
				  $x.='
				  <tr>
					<td class="print_field_title">現於本園就讀之兄/姊</td>
					<td colspan="5">姓名：<span class="input_content">'.$othersInfo['CurBSName'].'</span> 班別：<span class="input_content">'.$othersInfo['CurBSLevel'].'</span></td>
				  </tr>
				  <tr>
					<td class="print_field_title">同時報讀的雙生兒</td>
					<td colspan="5"><span class="input_content"><!--input type="checkbox" onclick="return false" '.($studentInfo['IsTwinsApplied']?'checked':'').' />雙生兒申請</span-->
					    姓名：<span class="input_content">'.($studentInfo['IsTwinsApplied']?$othersInfo['SiblingAppliedName']:'').'</span> 
					 (雙生兒身份證明文件號碼：<span class="input_content">'.($studentInfo['IsTwinsApplied']?$studentInfo['SiblingAppliedID']:'').'</span>)
					 (需填寫另一張申請表)
					</td>
				  </tr>
				  <tr>
					<td class="print_field_title">&nbsp;</td>
					<td colspan="5">
						<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />兄姐是本園畢業生</span>&nbsp;&nbsp;&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />1位</span>&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName2']!=""?'checked':'').' />2位</span>&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName3']!=""?'checked':'').' />3位</span><br/>
						<span class="input_content">姓名：<span>'.$othersInfo['ExBSName'].'</span> (畢業年份：<span>'.$othersInfo['ExBSGradYear'].'</span>)&nbsp;&nbsp;姓名：<span>'.$othersInfo['ExBSName2'].'</span> (畢業年份：<span>'.$othersInfo['ExBSGradYear2'].'</span>)&nbsp;&nbsp;姓名：<span>'.$othersInfo['ExBSName3'].'</span> (畢業年份：<span>'.$othersInfo['ExBSGradYear3'].'</span>)</span>
					</td>
				  </tr>
				  <tr>
					<td class="print_field_title">&nbsp;</td>
					<td colspan="5"><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['TeacherManagerChildren']=="yes"?'checked':'').' />教職員、校董子女</span></td>
				  </tr>
				  <tr>
					<td class="print_field_title">由何校轉來</td>
					<td colspan="5">
						<span class="input_content">'.$studentInfo['lastschool'].'</span>
					</td>
				  </tr>
				  <tr>
					<td class="print_field_title">備註</td>
					<td colspan="5">
						<span class="input_content">'.$othersInfo['Remarks'].'</span>
					</td>
				  </tr>
				</table>
				<br/>
				<table align="center" class="tg-table-plain">
				  <tr><td colspan="5" class="print_field_title"><center>學 校 專 用</center></td></tr>
				  <tr>
					<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ApplyFee']!=""?'checked':'').' />報名費</span></td>
					<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['RegisterFee']!=""?'checked':'').' />註冊費</span></td>
				 	<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['BookFee']!=""?'checked':'').' />書簿雜費</span></td>
				    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['UniformsFee']!=""?'checked':'').' />校服/書包</span></td>
				    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['BookFee']!=""?'checked':'').' />註冊證</span></td>
				    <!--<td>入學日期<br/><span class="input_content">___________</span></td>-->
				 </tr>
				';
				  }
				$x .= '</table></div>';
			}else if($form_lang == 'en'){
				$Lang['Admission']['BirthCertType']['hk']	= "Hong Kong Birth Certificate";
				$Lang['Admission']['BirthCertType']['hk2']	= "Hong Kong Birth Certificate";
				$Lang['Admission']['BirthCertType']['hkid']	= " Hong Kong ID Card";
				$Lang['Admission']['BirthCertType']['oversea']	= "Overseas Birth Certificate";
				$Lang['Admission']['BirthCertType']['mainland']	= "Mainland Birth Certificate";
				$Lang['Admission']['BirthCertType']['others']	= "Others";
				
				$Lang['Admission']['genderType']['M'] = "Male";
				$Lang['Admission']['genderType']['F'] = "Female";
				
				$x = '<div class="input_form" style="width:720px;margin:auto;">';
				$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=180&height=40&format=PNG'></div>";
				$x .= '<h4 align="center" style="padding-top:45px">Tsung Tsin Mission Graceful Kindergarten & Nursery (Ma On Shan)</h4>';
					
				$x .= '<h4 align="center">'.($type != 'reply_note'?'New student application form':'New student interview notice').'</h4>';
					
				$interviewList = current($lac->getInterviewListAry($othersInfo['InterviewSettingID']));

				$interviewDate .= $interviewList['Date'];
				if($interviewList['StartTime'] && $interviewList['EndTime']){
				    $interviewDate .= ' ('.substr($interviewList['StartTime'], 0, -3).' ~ '.substr($interviewList['EndTime'], 0, -3).')';
				}else{
				    $interviewDate .= '--';
				}
				
				$x .='<table width="100%">
					<tr>
						<td><span style="font-size: 13px">Apply No：'.$othersInfo['applicationID'].'</span></td>
						<td><span style="font-size: 13px">Date of Interview：'.$interviewDate.'</span></td>
						<td><span style="float:right;font-size: 12px">Date Received：'.substr($othersInfo['DateInput'], 0, -9).'</span></td>
					</tr>
				</table>';
			
				$x .= '<table align="center" class="tg-table-plain">
				  <tr>
				    <td class="print_field_title print_field_row1">Chinese Name</td>
				    <td>'.$studentInfo['student_name_b5'].'</td>
				    <td class="print_field_title">Gender</td>
				    <td>'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td>
				    <td rowspan="5" colspan="2" width="100" align="center"><img src="'.$personalPhotoPath.'" width="100px" /><!--相片--></td>
				  </tr>
				  <tr class="tg-even">
					<td class="print_field_title">English Name</td>
				    <td>'.$studentInfo['student_name_en'].'</td>
					<td class="print_field_title">Native Place</td>
				    <td>'.$studentInfo['province'].'</td>
				  </tr>
				  <tr>
					<td class="print_field_title">Date of Birth</td>
					<td>'.substr($studentInfo['dateofbirth'], 8, 2).'/'.substr($studentInfo['dateofbirth'], 5, 2).'/'.substr($studentInfo['dateofbirth'], 0, 4).'</td>
				    <td class="print_field_title">Age</td>
				    <td>'.$studentInfo['age'].'</td>
				  </tr>
				  <tr class="tg-even">
				    <td class="print_field_title">Birth Certification Number</td>
				    <td>'.$birth_cert_type_selection.$studentInfo['birthcertno'].'</td>
					<td class="print_field_title">Place of Birth</td>
				    <td>'.$studentInfo['placeofbirth'].'</td>
				  </tr>
				  <tr class="tg-even">
				    <td class="print_field_title">Email Address</td>
				    <td colspan="3">'.$studentInfo['email'].'</td>
				  </tr>
				  <tr>
				    <td class="print_field_title">Address</td>
				    <td colspan="5">'.$studentInfo['homeaddress'].'</td>
				  </tr>
				  <tr>
					<td class="print_field_title">Contact No</td>
				    <td>'.$studentInfo['homephoneno'].'</td>
					<td class="print_field_title">Religion</td>
				    <td>'.$lac->returnPresetCodeName("RELIGION", $studentInfo['religion']).'</td>
					<td class="print_field_title">Name of Church</td>
				    <td>'.$studentInfo['church'].'</td>
				  </tr>
				  <tr>
					<td rowspan="5" class="print_field_title">Information of Parents</td>
				  </tr>
				  <tr>
				    <td colspan="3">Name of Father：'.$fatherInfo['parent_name_b5'].'</td>
				    <td class="print_field_title">Mobile No</td>
					<td>'.$fatherInfo['mobile'].'</td>
				  </tr>
				  <tr>
					<td colspan="3">ID No：'.$fatherInfo['hkid'].'</td>
				  	<td class="print_field_title">Occupation</td>
					<td>'.$fatherInfo['occupation'].'</td>
				  </tr>
				  <tr>
				    <td colspan="3">Name of Mother：'.$motherInfo['parent_name_b5'].'</td>
				    <td class="print_field_title">Mobile No</td>
					<td>'.$motherInfo['mobile'].'</td>
				  </tr>
				  <tr>
					<td colspan="3">ID No：'.$motherInfo['hkid'].'</td>
				  	<td class="print_field_title">Occupation</td>
					<td>'.$motherInfo['occupation'].'</td>
				  </tr>
			
				  <tr>
					<td class="print_field_title">Apply Level</td>
					<td colspan="5"><span class="input_content">'.$classLevel[$othersInfo['classLevelID']].'</span></td>
				  </tr>
				  <tr>
				    <td class="print_field_title">Class</td>
				    <td colspan="5">'.$dayTypeOption.'</td>
				  </tr>';
				if($type != 'reply_note'){
					$x.='
				  <tr>
					<td class="print_field_title">Siblings who study in our school</td>
					<td colspan="5">Name：<span class="input_content">'.$othersInfo['CurBSName'].'</span> Class：<span class="input_content">'.$othersInfo['CurBSLevel'].'</span></td>
				  </tr>
				  <tr>
					<td class="print_field_title">Twins application</td>
					<td colspan="5"><!--span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['SiblingAppliedName']!=""?'checked':'').' /> Twins who apply for our school</span-->
					    Name：<span class="input_content">'.($studentInfo['IsTwinsApplied']?$othersInfo['SiblingAppliedName']:'').'</span>
					    (Twins\' Number of Identity Document:<span class="input_content">'.($studentInfo['IsTwinsApplied']?$studentInfo['SiblingAppliedID']:'').'</span>)<br />
					    (sibling should sumit two application instead of come by in one)
					</td>
				  </tr>
				  <tr>
					<td class="print_field_title">&nbsp;</td>
					<td colspan="5">
						<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />Siblings who graduate from our school</span>&nbsp;&nbsp;&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />1 Person</span>&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName2']!=""?'checked':'').' />2 Persons</span>&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName3']!=""?'checked':'').' />3 Persons</span><br/>
						<span class="input_content">Name：<span>'.$othersInfo['ExBSName'].'</span> (Graduation Year：<span>'.$othersInfo['ExBSGradYear'].'</span>)&nbsp;&nbsp;Name：<span>'.$othersInfo['ExBSName2'].'</span> (Graduation Year：<span>'.$othersInfo['ExBSGradYear2'].'</span>)&nbsp;&nbsp;Name：<span>'.$othersInfo['ExBSName3'].'</span> (Graduation Year：<span>'.$othersInfo['ExBSGradYear3'].'</span>)</span>
					</td>
				  </tr>
				  <tr>
					<td class="print_field_title">&nbsp;</td>
					<td colspan="5"><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['TeacherManagerChildren']=="yes"?'checked':'').' />Teacher/Manager\'s children</span></td>
				  </tr>
				  <tr>
					<td class="print_field_title">Name of Previous School</td>
					<td colspan="5">
						<span class="input_content">'.$studentInfo['lastschool'].'</span>
					</td>
				  </tr>
				  <tr>
					<td class="print_field_title">Remarks</td>
					<td colspan="5">
						<span class="input_content">'.$othersInfo['Remarks'].'</span>
					</td>
				  </tr>
				</table>
				<br/>
				<table align="center" class="tg-table-plain">
				  <tr><td colspan="5" class="print_field_title"><center>School Office Use Only</center></td></tr>
				  <tr>
					<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ApplyFee']!=""?'checked':'').' />Application Fee</span></td>
					<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['RegisterFee']!=""?'checked':'').' />Registration Fee</span></td>
				 	<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['BookFee']!=""?'checked':'').' />Student Book Fee</span></td>
				    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['UniformsFee']!=""?'checked':'').' />Uniform / School Bag</span></td>
				    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['BookFee']!=""?'checked':'').' />Registration certificate</span></td>
				    <!--<td>Date of Admission<br/><span class="input_content">___________</span></td>-->
				 </tr>
				';
				}
				$x .= '</table></div>';
			}
		}
		else{
			//Header of the page
			if($form_lang == 'en'){
				$Lang['Admission']['genderType']['M'] = "Male";
				$Lang['Admission']['genderType']['F'] = "Female";
				
				$x = '<div class="input_form" style="width:600px;margin:auto;">';
				$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=180&height=40&format=PNG'></div>";
				$x .= '<h4 align="center" style="padding-top:45px">Tsung Tsin Mission Graceful Kindergarten & Nursery (Ma On Shan)</h4>';
				
				$x .= '<h4 align="center">'.($type != 'reply_note'?'New student application form':'New student interview notice').'</h4>';
				
				$interviewList = current($lac->getInterviewListAry($othersInfo['InterviewSettingID']));
				
				$x .= '<table align="center" class="tg-table-plain" style="width:600px;">
					  <tr>
						<td class="print_field_title print_field_row1">Application No.</td>
						<td>'.$othersInfo['applicationID'].'</td>
						<td class="print_field_title">Apply Level</td>
						<td>'.$classLevel[$othersInfo['classLevelID']].'</td>
						<td rowspan="5" colspan="2" width="100" align="center"><img src="'.$personalPhotoPath.'" width="100px" /><!--相片--></td>
					  </tr>
					  <tr>
					    <td class="print_field_title">Chinese Name</td>
					    <td>'.$studentInfo['student_name_b5'].'</td>
					    <td class="print_field_title">Gender</td>
					    <td>'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td>
					  </tr>
					  <tr>
						<td class="print_field_title">Date of Birth</td>
						<td colspan="3">'.substr($studentInfo['dateofbirth'], 8, 2).'/'.substr($studentInfo['dateofbirth'], 5, 2).'/'.substr($studentInfo['dateofbirth'], 0, 4).'</td>
					  </tr>
					  <tr>
						<td class="print_field_title">Date of Interview</td>
						<td colspan="3">'.
						    (($interviewList['Date'])? substr($interviewList['Date'], 8, 2).'/'.substr($interviewList['Date'], 5, 2).'/'.substr($interviewList['Date'], 0, 4) : '').
						'</td>
					  </tr>
					  <tr>
						<td class="print_field_title">Time of Interview</td>
						<td colspan="3">'.(($interviewList['StartTime'])?date(" g:i A", strtotime($interviewList['StartTime'])):'').'</td>
					  </tr>';
				$x .= '</table></div>';
			}else{
				$Lang['Admission']['genderType']['M'] = "男";
				$Lang['Admission']['genderType']['F'] = "女";
				
				$x = '<div class="input_form" style="width:600px;margin:auto;">';
				$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=180&height=40&format=PNG'></div>";
				$x .= '<h4 align="center" style="padding-top:45px">崇真會美善幼稚園 (馬鞍山)</h4>';
						
				$x .= '<h4 align="center">'.($type != 'reply_note'?'新生入學申請表':'新生入學面試通知').'</h4>';
				
				$interviewList = current($lac->getInterviewListAry($othersInfo['InterviewSettingID']));
				
				$x .= '<table align="center" class="tg-table-plain" style="width:600px;">
					  <tr>
						<td class="print_field_title print_field_row1">申請編號</td>
						<td>'.$othersInfo['applicationID'].'</td>
						<td class="print_field_title">報讀級別</td>
						<td>'.$classLevel[$othersInfo['classLevelID']].'</td>
						<td rowspan="5" colspan="2" width="100" align="center"><img src="'.$personalPhotoPath.'" width="100px" /><!--相片--></td>
					  </tr>
					  <tr>
					    <td class="print_field_title">中文姓名</td>
					    <td>'.$studentInfo['student_name_b5'].'</td>
					    <td class="print_field_title">性別</td>
					    <td>'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td>
					  </tr>
					  <tr>
						<td class="print_field_title">出生日期</td>
						<td colspan="3">'.substr($studentInfo['dateofbirth'], 0, 4).' 年 '.substr($studentInfo['dateofbirth'], 5, 2).' 月 '.substr($studentInfo['dateofbirth'], 8, 2).' 日</td>
					  </tr>
					  <tr>
						<td class="print_field_title">接見日期</td>
						<td colspan="3">'.
						    (($interviewList['Date'])? substr($interviewList['Date'], 0, 4).' 年 '.substr($interviewList['Date'], 5, 2).' 月 '.substr($interviewList['Date'], 8, 2).' 日' : '').
						'</td>
					  </tr>
					  <tr>
						<td class="print_field_title">接見時間</td>
						<td colspan="3">'.
						    (($interviewList['StartTime'])? str_replace('PM','下午',str_replace('AM','上午',date("A g 時 i 分", strtotime($interviewList['StartTime'])))) : '') .
						'</td>
					  </tr>';
					$x .= '</table></div>';
			}
		}
			return $x;
		}
		else
			return false;
	}
	
	function getPrintPageCss(){
		return '<style type="text/css">
.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
.tg-table-plain td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
.print_field_title { background: #EFEFEF}
.print_field_title_main { background:#D7D7D7}
.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
.print_field_row1 { width:120px}
.print_field_row2 { width:28px}
.print_field_row3 { width:80px}
.print_field_title_remark { background:#B9B9B9; width:290px;}
.print_field_title_parent{ width:181px;}
@media print
{    
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
</style>';
	}
	

	function getUpdateIndexContent($Instruction, $ClassLevel = ""){
	    global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
	    if(!$Instruction){
	        $Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].'<br/>Welcome to our online application page!';
	    }
	    if(!$lac->IsUpdatePeriod()/* && !$lac->IsAfterUpdatePeriod()*/){
	        $Instruction = '<fieldset class="warning_box">
				<legend>'.$Lang['Admission']['warning'].'</legend>
				<ul>
					<li>'.$Lang['Admission']['mgf']['interviewPageNotOpen'].'</li>
				</ul>
			</fieldset>';
	    }
	
        $previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['mgf']['defaultviewpagemessage'] .'<br/>This page is for view interview information.</center></h2>';
	    $x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
	    $x .='<p class="spacer"></p>
                    	</div>';
    	if($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && ($lac->IsUpdatePeriod() /*|| $lac->IsAfterUpdatePeriod()*/)){
            $star = '<font style="color:red;">*</font>';
            	
            $x .= '<table class="form_table" style="font-size: 13px">';
            $x .= '<tr>';
            $x .= '<td class="field_title">'.$star.'輸入申請編號 Application Number</td>';
            $x .= '<td><input style="width:200px" name="InputApplicationID" type="text" id="InputApplicationID" class="textboxtext" maxlength="16" size="8"/></td>';
            $x .= '</tr>';
            $x .= '<tr>';
            $x .= '<td class="field_title">'.$star.'輸入出生證明書號碼 Birth Certificate Number</td>';
            $x .= '<td><input style="width:200px" name="InputStudentBirthCertNo" type="text" id="InputStudentBirthCertNo" class="textboxtext" maxlength="64" size="8"/><br />不需要輸入括號，例如：A123456(7)，請輸入 "A1234567"。<br/>No need to enter the brackets. Eg. A123456(7), please enter "A1234567".</td>';
            $x .= '</tr>';
            $x .= '<tr>';
            $x .= '<td class="field_title">'.$star.'輸入出生日期 Date of Birth</td>';
            $x .= '<td><input style="width:200px" name="InputStudentDateOfBirth" type="text" id="InputStudentDateOfBirth" class="textboxtext hasDatepicker" maxlength="10" size="15"/>(YYYY-MM-DD)</td>';
            $x .= '</tr>';
            $x .= '</table>';
            //$x .='</form>';
            	
            $x .= '<div class="edit_bottom">
    								'.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_index','step_print_interview')", "goToForm", "", 0, "formbutton").'
    							</div>
    							<p class="spacer"></p>';
    
            $x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
            $x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
    	}
	    $x .='</div></div></div>
	
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
	     
	    return $x;
	}
	function getInputFormPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		$applicationSetting = $lac->getApplicationSetting($lac->schoolYearID);
		$applicationOthersInfo = current($lac->getApplicationOthersInfo($lac->schoolYearID,'',$fileData['InputApplicationID']));
		$lastContent = $applicationSetting[$applicationOthersInfo['classLevelID']]['LastPageContent'];
		$updateStartDate = $applicationSetting[$applicationOthersInfo['classLevelID']]['UpdateStartDate'];
		$updateEndDate = $applicationSetting[$applicationOthersInfo['classLevelID']]['UpdateEndDate'];
		
		if(date('Y-m-d H:i') < $updateStartDate || date('Y-m-d H:i') > $updateEndDate){
		    $fileData['InputApplicationID'] = '';
		}
		$x .= $this->getAfterUpdateFinishPageContent($fileData['InputApplicationID'], $lastContent);
		return $x;
	}
	
	function getAfterUpdateFinishPageContent($ApplicationID='', $LastContent=''){
	    global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
	    $x = '<div class="admission_board">';
	
	    if($ApplicationID){
    	    $x .=' <div class="admission_complete_msg" style="padding-bottom: 0;"><h1>申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/><br><input type="button" value="'.$Lang['Admission']['printReplyNote'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"reply_note","b5").'\',\'_blank\');"></input></h1></div>';
    	    $x .=' <div class="admission_complete_msg"><h1>Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="Print interview notes" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID,"reply_note","en").'\',\'_blank\');"></input></h1></div>';
    	    
    	    
    	    if(!$LastContent){
    	        $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
    	    }
    	    $x .= '<br/>'.$LastContent;
	    }else{
	        $x .= '<fieldset class="warning_box">
						<legend>'.$Lang['Admission']['warning'].'</legend>
						<ul>
							<li>'.$Lang['Admission']['mgf']['interviewPageNotOpen'].'</li>
						</ul>
					</fieldset>';
	    }
	    $x .= '</div>';
	
	    $x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
	
	
	    return $x;
	}
	
} // End Class
?>