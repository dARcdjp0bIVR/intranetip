<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">

kis.datepicker('#StudentDateOfBirth');	

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?> Your input data would be lost.';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
var preventBackTimeout = setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>\nThe time is up! Please apply again!');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>The time is up!\nThe admission form will auto submit after pressing \'OK\'!");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	//For debugging only
	//return true;
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
//	form1.hidden_data.value = $("form").serialize();
//	form1.hidden_file.value = form1.StudentPersonalPhoto.files[0];
//	alert(form1.hidden_data.value);
	
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	//var re = /\S+@\S+\.\S+/;
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
	
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	//var hasPG = 1;
	
	//Assign the birth cert no parts to hidden field
	//form1.StudentBirthCertNo.value = form1.StudentBirthCertNo_f.value.toUpperCase() + form1.StudentBirthCertNo_m.value +'('+form1.StudentBirthCertNo_l.value+')';
	
	if(form1.StudentChiName.value==''){
		return false;
	} else if(form1.StudentEngName.value==''){
		return false;
	//} else if(form1.StudentDateOfBirth.value==''  || studentDateOfBirth >= today){
	} else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		return false;
	} <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>
	else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		return false;
	} <?}?>
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		return false;
    } else if(form1.StudentProvince.value==''){
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		return false;
	} else if(form1.StudentBirthCertType.value == 1 && !/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
		return false;
	} else if(checkBirthCertNo() > 0){
		return false;
	} else if(form1.StudentAge.value==''){
		return false;
	} else if(isNaN(form1.StudentAge.value)){
		return false;	
	} else if(form1.StudentPlaceOfBirth.value==''){
		return false;
	} else if(form1.StudentHomePhoneNo.value==''){
		return false;
	}else if(!/^[0-9]{8}$/.test(form1.StudentHomePhoneNo.value)){
		return false;	
//	} else if(form1.StudentChurch.value=='' && form1.StudentReligion.value!=0){
//		return false;
	} else if(form1.StudentHomeAddress.value==''){
		return false;
	} else if(form1.StudentEmail.value==''){
		return false;
	} else if(!re.test(form1.StudentEmail.value)){
		return false;
	} else if(form1.StudentPersonalPhoto.value==''){
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		return false;
	} else if(form1.G1ChineseName.value=='' && form1.G2ChineseName.value==''){
			return false;
	}
	if(form1.G1ChineseName.value!=''){
		
		if(form1.G1HKID.value==''){
			return false;
		} else if(!/^[a-zA-Z][0-9]{3}$/.test(form1.G1HKID.value)){
			return false;
		}
		else if(form1.G1MobileNo.value==''){
			return false;
		}else if(!/^[0-9]{8}$/.test(form1.G1MobileNo.value)){
			return false;
		}
		else if(form1.G1Occupation.value==''){
			return false;
		}
	}
	if(form1.G2ChineseName.value!=''){
		
		if(form1.G2HKID.value==''){
			return false;
		} else if(!/^[a-zA-Z][0-9]{3}$/.test(form1.G2HKID.value)){
			return false;
		}
		else if(form1.G2MobileNo.value==''){
			return false;
		}else if(!/^[0-9]{8}$/.test(form1.G2MobileNo.value)){
			return false;
		}
		else if(form1.G2Occupation.value==''){
			return false;
		}
	} 
	if(form1.InterviewSettingID && form1.InterviewSettingID.value=='0'){
		return false;
	} else if(form1.OthersSiblingApplied.value=='yes' && form1.OthersSiblingAppliedName.value == ''){
		return false;
	} else if(form1.OthersSiblingStudied.value=='yes' && form1.NumSiblingStudied.value == ''){
		return false;
	} else if(form1.NumSiblingStudied.value=='1'){
		if(form1.OthersExBSName.value==''){
			return false;
		} else if(form1.OthersExBSGradYear.value==''){
			return false;
		}
	}
	if(form1.NumSiblingStudied.value=='2'){
		if(form1.OthersExBSName.value==''){
			return false;
		} else if(form1.OthersExBSGradYear.value==''){
			return false;
		} else if(form1.OthersExBSName2.value==''){
			return false;
		} else if(form1.OthersExBSGradYear2.value==''){
			return false;
		}
	}
	if(form1.NumSiblingStudied.value=='3'){
		if(form1.OthersExBSName.value==''){
			return false;
		} else if(form1.OthersExBSGradYear.value==''){
			return false;
		} else if(form1.OthersExBSName2.value==''){
			return false;
		} else if(form1.OthersExBSGradYear2.value==''){
			return false;
		} else if(form1.OthersExBSName3.value==''){
			return false;
		} else if(form1.OthersExBSGradYear3.value==''){
			return false;
		}
	}
	
	if(checkInterviewQuotaLeft() <= 0 && checkInterviewQuotaLeftByClass() > 0){
		return false;
	}
	//else {
		return true;
	//}
}
/*
function check_docs_upload2(form1) {

	if(form1.OtherFile.value!=''){
		var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize = form1.OtherFile.files[0].size;
	}
	if(form1.OtherFile1.value!=''){
		var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
		var otherFileSize1 = form1.OtherFile1.files[0].size;
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		return false;
	} else if(otherFileSize > maxFileSize){
		return false;
	} else if(form1.OtherFile1.value==''){
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		return false;
	} else if(otherFileSize1 > maxFileSize){
		return false;
	} else  {
		return true;
	}
}
*/
function check_docs_upload2(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		if(!isOldBrowser){
			var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			return false;
		} else if(!isOldBrowser && otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			return false;
		}
	}
	
//	for(var i=0;i<1;i++)
//	{
//		var file_element = file_ary.get(i);
//		
//		var otherFileVal = file_element.value;
//		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//		if(!isOldBrowser){
//			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//		}
//		if(otherFileVal==''){
//			return false;
//		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//			return false;
//		} else if(!isOldBrowser && otherFileSize > maxFileSize){
//			return false;
//		}
//	}
//	
//	if(form1.StudentBirthCertType.value == 2 && file_count > 1){
//		for(var i=1;i<2;i++)
//		{
//			var file_element = file_ary.get(i);
//			
//			var otherFileVal = file_element.value;
//			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//			if(!isOldBrowser){
//				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//			}
//			if(otherFileVal==''){
//				return false;
//			} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//				return false;
//			} else if(!isOldBrowser && otherFileSize > maxFileSize){
//				return false;
//			}
//		}
//	}
//	if(form1.StudentBirthCertType.value == 3 && file_count > 2){
//		for(var i=1;i<3;i++)
//		{
//			var file_element = file_ary.get(i);
//			
//			var otherFileVal = file_element.value;
//			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//			if(!isOldBrowser){
//				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//			}
//			if(otherFileVal==''){
//				return false;
//			} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//				return false;
//			} else if(!isOldBrowser && otherFileSize > maxFileSize){
//				return false;
//			}
//		}
//	}
	
	if(checkInterviewQuotaLeft() <= 0  && checkInterviewQuotaLeftByClass() > 0){
		return false;
	}
	
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
//		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
//	        alert("表格需以 Google Chrome、Firefox 或 Internet Explorer 10 或以上瀏覽器填寫。");
//	        return;
//	    }
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		//alert($("form").serialize());
		isValid = check_input_info($("form")[0]);
		if(isValid && $('input[type=file][name*=OtherFile]').length == 0){
			document.getElementById(current).style.display = "none";
			if(current == 'step_input_form'){
				goto('step_docs_upload','step_confirm');
			}
			else if(current == 'step_confirm'){
				goto('step_docs_upload','step_input_form');
			}
			return;
		}
//		if(form1.StudentBirthCertType.value == 1 || form1.StudentBirthCertType.value == 4){
//			document.getElementById('div_OtherFile1').style.display = "none";
//			document.getElementById('div_OtherFile2').style.display = "none";
//			document.getElementById('OtherFile1').value = "";
//			document.getElementById('OtherFile2').value = "";
//		}
//		else if(form1.StudentBirthCertType.value == 2){
//			document.getElementById('div_OtherFile1').style.display = "";
//			document.getElementById('div_OtherFile2').style.display = "none";
//			document.getElementById('OtherFile2').value = "";
//		}
//		else if(form1.StudentBirthCertType.value == 3){
//			document.getElementById('div_OtherFile1').style.display = "";
//			document.getElementById('div_OtherFile2').style.display = "";
//		}
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	else if(page == 'step_print_interview'){
		isValid = check_interview($("form")[0]);
		page = 'step_confirm';
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
			   clearTimeout(timer);
			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
		   
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_class_selection.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#DayTypeOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		    /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_interview_timeslot.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           if(data == '--'){
		           		data = '<font color="red">(申請已超出面試人數，所以申請只作後備，待有學額，校方再個別通知面試時間和安排。)</font>';
		           }
		           $("#InterviewSettingOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
		   
	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){
	if(checkInterviewQuotaLeft() <= 0  && checkInterviewQuotaLeftByClass() > 0){
		alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>\nInterview timeslot is full. Please choose another timeslot.");
		goto('step_confirm','step_input_form');
		form1.InterviewSettingID.focus();
		return false;
	}
	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	return confirm('<?=$Lang['Admission']['csm']['suresubmit']?>\nCaution: Application once submitted cannot be undone.');
}

function check_choose_class(form1) {

	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>\nPlease read and initial all boxes as acknowledgement and acceptance of these terms.");
				return false;
			}
		}
	<?}?>
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>\nPlease select Class.");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	//For debugging only
	//return true;
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
//	form1.hidden_data.value = $("form").serialize();
//	form1.hidden_file.value = form1.StudentPersonalPhoto.files[0];
//	alert(form1.hidden_data.value);
	
	//alert('File name is' + form1.StudentPersonalPhoto.files[0].name + '\nFile size is' + form1.StudentPersonalPhoto.files[0].size);
	//var re = /\S+@\S+\.\S+/;
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	var studentDateOfBirth = form1.StudentDateOfBirth.value;
	//var othersApplyDate = form1.OthersApplyDate.value;
	var today = '<?=date('Y-m-d')?>';
	
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	//var hasPG = 1;
	
	//Assign the birth cert no parts to hidden field
	//form1.StudentBirthCertNo.value = form1.StudentBirthCertNo_f.value.toUpperCase() + form1.StudentBirthCertNo_m.value +'('+form1.StudentBirthCertNo_l.value+')';
	
	if(form1.StudentChiName.value==''){
		alert("<?=$Lang['Admission']['msg']['enterchinesename']?>\nPlease enter Chinese Name.");	
		form1.StudentChiName.focus();
		return false;
	} else if(form1.StudentEngName.value==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>\nPlease enter English Name.");
		form1.StudentEngName.focus();
		return false;
	//} else if(form1.StudentDateOfBirth.value==''  || studentDateOfBirth >= today){
	} else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.StudentDateOfBirth.value!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings']){?>
	else if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>\nInvalid Birthday Range of Student.");
		form1.StudentDateOfBirth.focus();
		return false;
	} <?}?>
	else if($('input:radio[name=StudentGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>\nPlease select Gender.");	
		form1.StudentGender[0].focus();
		return false;
	} else if(form1.StudentProvince.value==''){
		alert("<?=$Lang['Admission']['msg']['enternativeplace']?>\nPlease enter Native Place.");	
		form1.StudentProvince.focus();
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['mgf']['msg']['enterbirthcertificatenumber']?>\nPlease enter Birth Certificate Number.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(form1.StudentBirthCertType.value == 1 && !/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
		alert('<?=$Lang['Admission']['mgf']['msg']['invalidbirthcertificatenumber']?>\n<?=$Lang['Admission']['mgf']['msg']['birthcertnohints']?>\nInvalid Birth Certificate Number.\n(eg：A123456(7)，please enter "A1234567")');	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['mgf']['msg']['duplicatebirthcertificatenumber']?>\nThe Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(form1.StudentAge.value==''){
		alert("<?=$Lang['Admission']['msg']['enterage']?>\nPlease enter Age.");	
		form1.StudentAge.focus();
		return false;
	} else if(isNaN(form1.StudentAge.value)){
		alert("<?=$Lang['Admission']['msg']['enternumber']?>\nPlease enter number.");
		form1.StudentAge.focus();
		return false;	
	} else if(form1.StudentPlaceOfBirth.value==''){
		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>\nPlease enter Place of Birth.");	
		form1.StudentPlaceOfBirth.focus();
		return false;
	} else if(form1.StudentHomePhoneNo.value==''){
		alert("<?=$Lang['Admission']['mgf']['msg']['enterstudenthomephoneno']?>\nPlease enter Home Phone No.");	
		form1.StudentHomePhoneNo.focus();
		return false;
	}else if(!/^[0-9]{8}$/.test(form1.StudentHomePhoneNo.value)){
			alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
			form1.StudentHomePhoneNo.focus();
			return false;
//	} else if(form1.StudentChurch.value=='' && form1.StudentReligion.value!=0){
//		alert("<?=$Lang['Admission']['msg']['enterchurch']?>");
//		form1.StudentChurch.focus();
//		return false;
	} else if(form1.StudentHomeAddress.value==''){
		alert("<?=$Lang['Admission']['mgf']['msg']['enterhomeaddress']?>\nPlease enter Home Address.");	
		form1.StudentHomeAddress.focus();
		return false;
	} else if(form1.StudentEmail.value==''){
		alert("<?=$Lang['Admission']['mgf']['msg']['email']?>\nPlease enter Email Address.");	
		form1.StudentEmail.focus();
		return false;
	} else if(!re.test(form1.StudentEmail.value)){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>\nInvalid Email Format");
		form1.StudentEmail.focus();
		return false;
	} else if(form1.StudentPersonalPhoto.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>\nPlease upload a personal photo.");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(form1.G1ChineseName.value=='' && form1.G2ChineseName.value==''){
			alert("<?=$Lang['Admission']['msg']['enteratleastoneparent']?>\nPlease enter at least one parent information.");
			form1.G1ChineseName.focus();
			return false;
	}
	if(form1.G1ChineseName.value!=''){
		
		if(form1.G1HKID.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['G1HKID']?>\nPlease enter Hong Kong Resident Identity Card.");
			form1.G1HKID.focus();
			return false;
		} else if(!/^[a-zA-Z][0-9]{3}$/.test(form1.G1HKID.value)){
			alert('<?=$Lang['Admission']['mgf']['msg']['invalididentitycardnumber']?>\n<?=str_replace('<br/>', '\n', $Lang['Admission']['mgf']['msg']['hkidhints'])?>\nInvalid Resident Identity Card Number.\nEnter letter & first 3 numbers.\n(e.g.: A123456(7)，please enter "A123")');	
			form1.G1HKID.focus();
			return false;
		}
		else if(form1.G1MobileNo.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile'] ?>\nPlease enter Cell Phone No.");
			form1.G1MobileNo.focus();
			return false;
		}else if(!/^[0-9]{8}$/.test(form1.G1MobileNo.value)){
			alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
			form1.G1MobileNo.focus();
			return false;
		}
		else if(form1.G1Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation']?>\nPlease enter Occupation.");
			form1.G1Occupation.focus();
			return false;
		}
	}
	if(form1.G2ChineseName.value!=''){
		
		if(form1.G2HKID.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['G1HKID']?>\nPlease enter Home Phone No.");
			form1.G2HKID.focus();
			return false;
		} else if(!/^[a-zA-Z][0-9]{3}$/.test(form1.G2HKID.value)){
			alert('<?=$Lang['Admission']['mgf']['msg']['invalididentitycardnumber']?>\n<?=str_replace('<br/>', '\n', $Lang['Admission']['mgf']['msg']['hkidhints'])?>');	
			form1.G2HKID.focus();
			return false;
		}
		else if(form1.G2MobileNo.value==''){
			alert("<?=$Lang['Admission']['csm']['msg']['G1Mobile'] ?>\nPlease enter Cell Phone No.");
			form1.G2MobileNo.focus();
			return false;
		}else if(!/^[0-9]{8}$/.test(form1.G2MobileNo.value)){
			alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>\nPlease enter number with 8 digits.");
			form1.G2MobileNo.focus();
			return false;
		}
		else if(form1.G2Occupation.value==''){
			alert("<?=$Lang['Admission']['msg']['enteroccupation']?>\nPlease enter Occupation.");
			form1.G2Occupation.focus();
			return false;
		}
	}
	
	if(form1.OthersApplyDayType2 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType3 && form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value && form1.OthersApplyDayType1.value !="" || form1.OthersApplyDayType1 && form1.OthersApplyDayType1.value ==""){
		//else if(form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value !="" || form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType3.value  && form1.OthersApplyDayType3.value !="" || form1.OthersApplyDayType1.value == form1.OthersApplyDayType2.value && form1.OthersApplyDayType2.value == form1.OthersApplyDayType3.value){
		alert("<?=$Lang['Admission']['msg']['applyDayTypeHints']?>\nPlease choose your priorities(at least one), blank if not consider");
		form1.OthersApplyDayType1.focus();
		return false; 
	} else if(form1.InterviewSettingID && form1.InterviewSettingID.value=='0'){
		alert("<?=$Lang['Admission']['msg']['selectinterviewtimeslot']?>\nPlease select an interview timeslot.");
		form1.InterviewSettingID.focus();
		return false;
	} else if($('input:radio[name=OthersSiblingApplied]:checked').val() == 'yes'){
		if(form1.OthersSiblingAppliedName.value == ''){
    		alert("<?=$Lang['Admission']['mgf']['msg']['enterhassiblingappliedname']?>\nPlease enter the name of twins who apply for our school");
    		form1.OthersSiblingAppliedName.focus();
    		return false;
		}

		if(form1.twinsapplicationid.value == ''){
    		alert("<?=$Lang['Admission']['mgf']['msg']['enterhassiblingappliedid']?>\nPlease enter Twins\' Number of Identity Document");
    		form1.twinsapplicationid.focus();
    		return false;
		}
	} else if($('input:radio[name=OthersSiblingStudied]:checked').val() == 'yes' && !$('input:radio[name=NumSiblingStudied]:checked').val()){
		alert("<?=$Lang['Admission']['mgf']['msg']['issiblinggradhere']?>\nPlease enter the number of siblings who graduate from our school");
		form1.NumSiblingStudied[0].focus();
		return false;
	} else if($('input:radio[name=NumSiblingStudied]:checked').val()=='1'){
		if(form1.OthersExBSName.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['entersiblinggradherename']?>\nPlease enter the name of siblings who graduate from our school");
			form1.OthersExBSName.focus();
			return false;
		} else if(form1.OthersExBSGradYear.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['enterGradYear']?>\nPlease enter Graduation Year.");
			form1.OthersExBSGradYear.focus();
			return false;
		}
	}
	if($('input:radio[name=NumSiblingStudied]:checked').val()=='2'){
		if(form1.OthersExBSName.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['entersiblinggradherename']?>\nPlease enter the name of siblings who graduate from our school");
			form1.OthersExBSName.focus();
			return false;
		} else if(form1.OthersExBSGradYear.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['enterGradYear']?>\nPlease enter Graduation Year.");
			form1.OthersExBSGradYear.focus();
			return false;
		} else if(form1.OthersExBSName2.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['entersiblinggradherename']?>\nPlease enter the name of siblings who graduate from our school");
			form1.OthersExBSName2.focus();
			return false;
		} else if(form1.OthersExBSGradYear2.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['enterGradYear']?>\nPlease enter Graduation Year.");
			form1.OthersExBSGradYear2.focus();
			return false;
		}
	}
	if($('input:radio[name=NumSiblingStudied]:checked').val()=='3'){
		if(form1.OthersExBSName.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['entersiblinggradherename']?>\nPlease enter the name of siblings who graduate from our school");
			form1.OthersExBSName.focus();
			return false;
		} else if(form1.OthersExBSGradYear.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['enterGradYear']?>\nPlease enter Graduation Year.");
			form1.OthersExBSGradYear.focus();
			return false;
		} else if(form1.OthersExBSName2.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['entersiblinggradherename']?>\nPlease enter the name of siblings who graduate from our school");
			form1.OthersExBSName2.focus();
			return false;
		} else if(form1.OthersExBSGradYear2.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['enterGradYear']?>\nPlease enter Graduation Year.");
			form1.OthersExBSGradYear2.focus();
			return false;
		} else if(form1.OthersExBSName3.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['entersiblinggradherename']?>\nPlease enter the name of siblings who graduate from our school");
			form1.OthersExBSName3.focus();
			return false;
		} else if(form1.OthersExBSGradYear3.value==''){
			alert("<?=$Lang['Admission']['mgf']['msg']['enterGradYear']?>\nPlease enter Graduation Year.");
			form1.OthersExBSGradYear3.focus();
			return false;
		}
	}
	
	if(checkInterviewQuotaLeft() <= 0  && checkInterviewQuotaLeftByClass() > 0){
		alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>\nInterview timeslot is full. Please choose another timeslot.");
		form1.InterviewSettingID.focus();
		return false;
	}
	//else {
		return true;
	//}
}
/*
function check_docs_upload(form1) {
	//For debugging only
	//return true;
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	if(!isOldBrowser){
		if(form1.OtherFile.value!=''){
			var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
			var otherFileSize = form1.OtherFile.files[0].size;
		}
		if(form1.OtherFile1.value!=''){
			var otherFileExt1 = form1.OtherFile1.files[0].name.split('.').pop().toUpperCase();
			var otherFileSize1 = form1.OtherFile1.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	if(form1.OtherFile.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadbirthcert']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else if(form1.OtherFile1.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadimmunisationrecord']?>");
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileExt1 !='JPG' && otherFileExt1 !='JPEG' && otherFileExt1 !='PNG' && otherFileExt1 !='GIF' && otherFileExt1 !='PDF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.OtherFile1.focus();
		return false;
	} else if(otherFileSize1 > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.OtherFile.focus();
		return false;
	} else  {
		return true;
	}
}
*/
function check_docs_upload(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		if(!isOldBrowser){
			var otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			alert("<?=$Lang['Admission']['msg']['uploadfile']?>\nPlease upload file.");
			file_element.focus();
			return false;
		} else if(!isOldBrowser && otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
			file_element.focus();
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
			file_element.focus();
			return false;
		}
	}
	
//	for(var i=0;i<1;i++)
//	{
//		var file_element = file_ary.get(i);
//		
//		var otherFileVal = file_element.value;
//		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//		if(!isOldBrowser){
//			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//		}
//		if(otherFileVal==''){
//			alert("<?=$Lang['Admission']['msg']['uploadfile']?>");
//			file_element.focus();
//			return false;
//		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
//			file_element.focus();
//			return false;
//		} else if(!isOldBrowser && otherFileSize > maxFileSize){
//			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
//			file_element.focus();
//			return false;
//		}
//	}
//	
//	if(form1.StudentBirthCertType.value == 2 && file_count > 1){
//		for(var i=1;i<2;i++)
//		{
//			var file_element = file_ary.get(i);
//			
//			var otherFileVal = file_element.value;
//			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//			if(!isOldBrowser){
//				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//			}
//			if(otherFileVal==''){
//				alert("<?=$Lang['Admission']['msg']['uploadfile']?>");
//				file_element.focus();
//				return false;
//			} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//				alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
//				file_element.focus();
//				return false;
//			} else if(!isOldBrowser && otherFileSize > maxFileSize){
//				alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
//				file_element.focus();
//				return false;
//			}
//		}
//	}
//	if(form1.StudentBirthCertType.value == 3 && file_count > 2){
//		for(var i=1;i<3;i++)
//		{
//			var file_element = file_ary.get(i);
//			
//			var otherFileVal = file_element.value;
//			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
//			if(!isOldBrowser){
//				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
//				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
//			}
//			if(otherFileVal==''){
//				alert("<?=$Lang['Admission']['msg']['uploadfile']?>");
//				file_element.focus();
//				return false;
//			} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
//				alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
//				file_element.focus();
//				return false;
//			} else if(!isOldBrowser && otherFileSize > maxFileSize){
//				alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
//				file_element.focus();
//				return false;
//			}
//		}
//	}
	
	if(checkInterviewQuotaLeft() <= 0  && checkInterviewQuotaLeftByClass() > 0){
		alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>\nInterview timeslot is full. Please choose another timeslot.");
		goto('step_docs_upload','step_input_form');
		form1.InterviewSettingID.focus();
		return false;
	}
	return true;
}

function check_interview(){
	if(form1.InputApplicationID.value==''){
		alert("請輸入申請編號。\nPlease enter Application Number.");	
		form1.InputApplicationID.focus();
		return false;
	}
	else if(form1.InputStudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>\nPlease enter Birth Certificate Number.");	
		form1.InputStudentBirthCertNo.focus();
		return false;
	}
	else if(!form1.InputStudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.InputStudentDateOfBirth.value!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
		}
		
		form1.InputStudentDateOfBirth.focus();
		return false;
	}
	
	clearTimeout(preventBackTimeout);
	window.onbeforeunload = '';
	
	/* Clear result div*/
	   $("#DayTypeOption").html('');
	
	   /* Get some values from elements on the page: */
	   var values = $("#form1").serialize();
		
	   /* Send the data using post and put the results in a div */
	   $.ajax({
	       url: "ajax_valid_for_update_form.php",
	       type: "post",
	       data: values,
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           $("#InputApplicationID").val('');
	           $("#InputStudentBirthCertNo").val('');
	           $("#InputStudentDateOfBirth").val('');
	           if(data==0){
	           		alert("申請編號，出生證明書號碼或出生日期不正確。\nIncorrect Application Number, Birth Certificate Number or Date of Birth.");	
					form1.InputStudentBirthCertNo.focus();
					isValid = false;
					return false;
	           }
//	           $("#applicationno").val(data);
//	           $("#divInterviewResult").html(data);
//	           document.getElementById(current).style.display = "none";
//			   document.getElementById(page).style.display = "";
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   
	   /* Send the data using post and put the results in a div */
	   $.ajax({
	       url: "ajax_get_input_form.php",
	       type: "post",
	       data: values,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!"+values);
	           $("#step_confirm").html(data);
	           kis.datepicker('#StudentDateOfBirth');
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
	   return true;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

function checkInterviewQuotaLeft(){
	return 1; // remove quota check
	
	var values = $("#form1").serialize();
	var res = null;
	/* Check the quota of interview timeslot */
   $.ajax({
       url: "ajax_check_num_of_interview_quota.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!"+values);
           //$("#step_confirm").html(data);
//           if(data <= 0){
//		           	alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
//		           	return false;
					res = data;
//		           }
		       },
		       error:function(){
		           //alert("failure");
           //$("#result").html('There is error while submit');
       }
   });
   return res;	
}

function checkInterviewQuotaLeftByClass(){
	var values = $("#form1").serialize();
	var res = null;
	/* Check the quota of interview timeslot */
   $.ajax({
       url: "ajax_check_num_of_interview_quota_by_class.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!"+values);
           //$("#step_confirm").html(data);
//           if(data <= 0){
//		           	alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
//		           	return false;
					res = data;
//		           }
		       },
		       error:function(){
		           //alert("failure");
           //$("#result").html('There is error while submit');
       }
   });
   return res;	
}

//function startUpload(){
//      document.getElementById('f1_upload_process').style.visibility = 'visible';
//      document.getElementById('f1_upload_form').style.visibility = 'hidden';
//      return true;
//}
//Henry modifying 20131028
//function stopUpload(temp_folder_name){
//		document.getElementById('tempFolderName').value = temp_folder_name;
//		
//		 var values = $("#form1").serialize();
//			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
//			var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
//			var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
//			values+=studentPersonalPhoto;
//			values+=otherFile;
//			values+=otherFile1;
//		$.ajax({
//		       url: "ajax_get_confirm.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!"+values);
//		           $("#step_confirm").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });     
//      return true;  
//}
</script>