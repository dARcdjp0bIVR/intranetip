<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>
<style>
.page-break	{display:block; page-break-before:always;}
.page-wrapper {height:29.7cm; width:21cm; margin:0 auto; border:1px solid #fff;}
.page-content {margin:1cm 2cm 0.5cm 2cm; height:28.2cm; position:relative;}

body {background-color: #fff; color:#000; font-family: "微軟正黑體", serif, "msjh"; font-size: 16px; line-height: 1.2em; margin:0;  padding:0;}

#content {background-color: #fff; margin: 0 auto; width: 210mm;}
div.page_wrapper {padding: 10mm;}

p {line-height: 1.3em; margin: 0; padding: 0;}
tr.control td {height: 0; line-height: 0; padding: 0;}
table {border-collapse: separate; border-spacing: 0; padding: 0; table-layout: fixed;} /*for IE*/
th:last-child {border-right: 1px solid #000;}
.checkbox {height: 3mm; margin-bottom: 0.5mm; vertical-align: middle; width: 3mm;}

/* Header start */
.header {width: 190mm;}
.header_ctrl {height: 0; padding: 0; margin: 0;}
.schLogo {vertical-align: top;}
.schLogo img {height: 15mm; width: auto;}
.barcode {height: 15mm; text-align: right; vertical-align: top;}
.barcode img {height: 10mm; width: auto;}
p.appNo {font-size: 0.75em; line-height: 0.75em; text-align: right;}
td.title {font-size: 1.125em; font-weight: bold; line-height: 1.125em; padding-bottom: 3mm; padding-top: 8mm; text-align: center;}
p.info {font-size: 0.75em; line-height: 1.5em;}
/* Header end */

.subtitle {font-size: 1em; font-weight: bold; line-height: 1.6em;}

/* Level and class start */
.tbl_lvClass {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; width: 190mm;}
.tbl_lvClass p {font-weight: bold; margin: 0;}
.tbl_lvClass td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 0.5mm 1mm;}
.tbl_lvClass td:last-child {border-right: 1px solid #000;}
.lvClass_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
.lvClass_check td {border-top: 0;}
.lvClass_check td:last-child {border-left: 0;}
.lvClass_check .checkbox {margin-top: 1mm;}
/* Level and class end */

/* Student info start */
.tbl_std {border: 1px solid #000; border-bottom: 0; font-size: 0.75em; line-height: 1.3em; margin-top: 1mm; width: 190mm;}
.tbl_std p {font-weight: bold; margin: 0;}
.tbl_std td {padding: 1mm;}
.std_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_std .std_fill {border-bottom: 1px solid #000; min-height: 8mm;}
.tbl_std .std_remark {height: 5mm;}
.tbl_std .photo {padding: 1.5mm 1mm; text-align: center; vertical-align: top;}
.photo img {height: auto; max-height: 49mm; max-width: 40mm; width: auto;}

.tbl_std2 {border: 1px solid #000; border-top: 0; font-size: 0.75em; line-height: 1.3em; margin-bottom: 5mm; padding: 0 0 2mm; width: 190mm;}
.tbl_std2 p {font-weight: bold; margin: 0;}
.tbl_std2 td {padding: 1mm;}
.std2_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_std2 .std2_fill {border-bottom: 1px solid #000; min-height: 8mm;}
/* Student info end */

/* Address start */
.tbl_address {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 3mm; margin-top: 1mm; width: 190mm;}
.tbl_address p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_address td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm;}
.tbl_address td:last-child {border-right: 1px solid #000;}
.address_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}

.tbl_mailAddress {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; width: 190mm;}
.tbl_mailAddress p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_mailAddress td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm;}
.tbl_mailAddress td:last-child {border-right: 1px solid #000;}
.mailAddress_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
/* Address end */

/* Parent start */
.tbl_parent {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; width: 190mm;}
.tbl_parent p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_parent td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm;}
.tbl_parent td:last-child {border-right: 1px solid #000;}
.parent_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
/* Parent end */

/* Other personal information start */
.tbl_other {font-size: 0.75em; line-height: 1.5em; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_other p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_other td {padding: 1mm; vertical-align: top;}
.other_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_other .other_fill {border-bottom: 1px solid #000; line-height: 1.3em; min-height: 8mm; vertical-align: middle;}
.other_space {height: 2mm;}

.tbl_preSch {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.3em; margin: 0 0 3mm 5mm; width: 185mm;}
.tbl_preSch p {font-weight: bold; margin: 0;}
.tbl_preSch td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm; text-align: center;}
.tbl_preSch td:last-child {border-right: 1px solid #000;}
.preSch_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}

.tbl_other2 {font-size: 0.75em; line-height: 1.5em; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_other2 p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_other2 td {padding: 1mm; vertical-align: top;}
.other2_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_other2 .other2_fill {border-bottom: 1px solid #000; line-height: 1.3em; min-height: 8mm; text-align: center; vertical-align: middle;}

.tbl_sibling {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.3em; margin: 2mm 0 5mm 5mm; width: 185mm;}
.tbl_sibling p {font-weight: bold; margin: 0;}
.tbl_sibling td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm; text-align: center;}
.tbl_sibling td:last-child {border-right: 1px solid #000;}
.sibling_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
.tbl_sibling .label {text-align: left;}
/* Other personal information end */

/* Health condition start */
.tbl_health {font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_health p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_health td {padding: 0.5mm 1mm;}
.tbl_health .health_no {vertical-align: top;}
.health_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_health .health_ans {line-height: 1.3em; height: 5mm; vertical-align: bottom;}
.tbl_health .health_fill {border-bottom: 1px solid #000; line-height: 1.3em; height: 5mm;}
.health_space {height: 2mm;}
/* Health condition end */

/* Parental expectations start */
.tbl_expect {font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_expect p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_expect td {padding: 0.5mm 1mm; vertical-align: top;}
.expect_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_expect .expect_no {vertical-align: top;}
.tbl_expect .expect_fill {border-bottom: 1px solid #000; line-height: 1.2em; height: 6mm; vertical-align: middle;}
.expect_space {height: 2mm;}
/* Parental expectations end */

/* Questionnaire start */
.tbl_q {font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_q p {font-weight: bold; line-height: 1.3em; margin: 0; padding-bottom: 1mm;}
.tbl_q td {padding: 0.5mm 1mm;}
.q_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_q .q_no {vertical-align: top;}
.tbl_q .q_checkbox {height: 5mm;}
.tbl_q .checkbox {margin-top: 0.5mm;}
.tbl_q .q_fill {border-bottom: 1px solid #000; line-height: 1.2em; height: 5mm; vertical-align: middle;}
.q_space {height: 4mm;}
/* Questionnaire end */

/* Emergency contact start */
.tbl_contact {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.2em; margin-top: 1mm; width: 190mm;}
.tbl_contact p {font-weight: bold; margin: 0;}
.tbl_contact td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm; text-align: center;}
.tbl_contact td:last-child {border-right: 1px solid #000;}
.contact_ctrl td {border: 0; height: 0; padding: 0; margin: 0;}
.tbl_contact .contact_label {text-align: left;}
/* Emergency contact end */
</style>
