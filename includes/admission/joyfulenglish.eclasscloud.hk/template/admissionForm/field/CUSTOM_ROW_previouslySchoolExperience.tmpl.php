<?php
global $lauc, $applicationData;

$preSchoolExperience = '';
foreach((array)$applicationData['ADMISSION_CUST_INFO'] as $custInfo){
    if($custInfo['Code'] === 'PreSchoolExperience'){
	    $preSchoolExperience = $custInfo['Value'];
	    break;
    }
}

$preSchoolInfoArr = BuildMultiKeyAssoc((array)$applicationData['ADMISSION_STU_PREV_SCHOOL_INFO'], 'SchoolOrder');
foreach((array)$preSchoolInfoArr as $order => $preSchoolInfo){
	$preSchoolInfoArr[$order]['StartMonth'] = substr($preSchoolInfo['StartDate'], 5,2);
	$preSchoolInfoArr[$order]['StartYear'] = substr($preSchoolInfo['StartDate'], 0,4);
	$preSchoolInfoArr[$order]['EndMonth'] = substr($preSchoolInfo['EndDate'], 5,2);
	$preSchoolInfoArr[$order]['EndYear'] = substr($preSchoolInfo['EndDate'], 0,4);
}
?>
<div class="item">
    <div class="itemLabel">
	    <?=$lauc->getLangStr(array(
		    'b5' => '幼兒是否曾修讀任何親子班或幼稚園課程?',
		    'en' => 'Has the applicant previously attended any other playgroup or kindergarten?',
	    ),'<br/>')?>
    </div>
    <span class="itemInput itemInput-choice">
		<span>
			<input
                    type="radio"
                    id="preSchoolExperience_Y"
                    name="preSchoolExperience"
                    value="Y"
                    <?=($preSchoolExperience === 'Y')?'checked':''?>
            ><label for="preSchoolExperience_Y">
	            <?=$lauc->getLangStr(array(
		            'b5' => $LangB5['General']['Yes'],
		            'en' => $LangEn['General']['Yes'],
	            )) ?>
			</label>
		</span>
		<span>
			<input
                    type="radio"
                    id="preSchoolExperience_N"
                    name="preSchoolExperience"
                    value="N"
                    <?=($preSchoolExperience === 'N')?'checked':''?>
            ><label for="preSchoolExperience_N">
	            <?=$lauc->getLangStr(array(
		            'b5' => $LangB5['General']['No'],
		            'en' => $LangEn['General']['No'],
	            )) ?>
			</label>
		</span>
		<div style="">
			<div class="remark remark-warn required hide" style="clear:both;">
				<?=$lauc->getLangStr(array(
					'b5' => '必須填寫',
					'en' => 'Required',
				))?>
			</div>
		</div>
	</span>
</div>
<div id="preSchoolExperience_container" class="item preSchoolExperience_details Y">
    <div class="itemLabel"></div>
	<?php for ( $i = 0; $i < 3; $i ++ ): ?>
        <span class="itemInput">
			<div class="itemInput-complex-order"><?= $i + 1 ?></div>
			<div class="button-deleteComplex icon icon-button"></div>
			<div class="itemInput-complex">
				<div class="textbox-floatlabel textbox-In5 itemInput ">
					<input
                            type="text"
                            id="preSchoolExperience_nameOfSchool_<?= $i ?>"
                            name="preSchoolExperience_nameOfSchool_<?= $i ?>"
                            value="<?=$preSchoolInfoArr[$i]['NameOfSchool']?>"
                    />
					<div class="textboxLabel ">
			            <?=$lauc->getLangStr(array(
				            'b5' => '學校名稱',
				            'en' => 'Name of School',
			            )) ?>
					</div>
					<div class="remark remark-warn required hide" style="clear:both;">
						<?=$lauc->getLangStr(array(
							'b5' => '必須填寫',
							'en' => 'Required',
						))?>
					</div>
				</div>
				<div class="textbox-floatlabel textbox-In5 itemInput">
					<input
                            type="text"
                            id="preSchoolExperience_locationOfSchool_<?= $i ?>"
                            name="preSchoolExperience_locationOfSchool_<?= $i ?>"
                            value="<?=$preSchoolInfoArr[$i]['SchoolAddress']?>"
                    />
					<div class="textboxLabel ">
			            <?=$lauc->getLangStr(array(
				            'b5' => '學校地區',
				            'en' => 'Location of School',
			            )) ?>
					</div>
					<div class="remark remark-warn required hide" style="clear:both;">
						<?=$lauc->getLangStr(array(
							'b5' => '必須填寫',
							'en' => 'Required',
						))?>
					</div>
				</div>
				<div class="textbox-floatlabel textbox-In5 itemInput">
					<input
                            type="text"
                            id="preSchoolExperience_level_<?= $i ?>"
                            name="preSchoolExperience_level_<?= $i ?>"
                            value="<?=$preSchoolInfoArr[$i]['Year']?>"
                    />
					<div class="textboxLabel ">
			            <?=$lauc->getLangStr(array(
				            'b5' => '班級',
				            'en' => 'Level / Grade',
			            )) ?>
					</div>
					<div class="remark remark-warn required hide" style="clear:both;">
						<?=$lauc->getLangStr(array(
							'b5' => '必須填寫',
							'en' => 'Required',
						))?>
					</div>
				</div>
				<div class="textbox-floatlabel textbox-In5 itemInput" style="text-align: center;">
					<span class="selector">
						<select id="preSchoolExperience_start_month_<?=$i?>" name="preSchoolExperience_start_month_<?=$i?>">
							<option value="" hidden>MM</option>
							<?php for($j=1;$j<=12;$j++):?>
								<option
                                        value="<?=$j?>"
                                        <?=($j == (int)($preSchoolInfoArr[$i]['StartMonth']))?'selected':'' ?>
                                >
                                    <?=substr("0$j", -2)?>
                                </option>
							<?php endfor; ?>
						</select>
			        </span>
					/
					<span class="selector">
						<select id="preSchoolExperience_start_year_<?=$i?>" name="preSchoolExperience_start_year_<?=$i?>">
							<option value="" hidden>YYYY</option>
							<?php for($j=(date('Y') - 5);$j<(date('Y'));$j++):?>
								<option
                                        value="<?=$j?>"
                                        <?=($j == $preSchoolInfoArr[$i]['StartYear'])?'selected':'' ?>
                                >
                                    <?=$j?>
                                </option>
							<?php endfor; ?>
						</select>
			        </span>

					<input class="notEmpty" type="text" style="display: none;">
					<div class="textboxLabel ">
			            <?=$lauc->getLangStr(array(
				            'b5' => '就讀時期 (開始)',
				            'en' => 'Period of Stay (Start)',
			            )) ?>
					</div>
					<div class="remark remark-warn required hide" style="clear:both;">
						<?=$lauc->getLangStr(array(
							'b5' => '必須填寫',
							'en' => 'Required',
						))?>
					</div>
				</div>
				<div class="textbox-floatlabel textbox-In5 itemInput" style="text-align: center;">
					<span class="selector">
						<select id="preSchoolExperience_end_month_<?=$i?>" name="preSchoolExperience_end_month_<?=$i?>">
							<option value="" hidden>MM</option>
							<?php for($j=1;$j<=12;$j++):?>
								<option
                                        value="<?=$j?>"
                                        <?=($j == (int)($preSchoolInfoArr[$i]['EndMonth']))?'selected':'' ?>
                                >
                                    <?=substr("0$j", -2)?>
                                </option>
							<?php endfor; ?>
						</select>
			        </span>
					/
					<span class="selector">
						<select id="preSchoolExperience_end_year_<?=$i?>" name="preSchoolExperience_end_year_<?=$i?>">
							<option value="" hidden>YYYY</option>
							<?php for($j=(date('Y') - 5), $jMax=(date('Y')+2);$j<$jMax;$j++):?>
								<option
                                        value="<?=$j?>"
                                        <?=($j == $preSchoolInfoArr[$i]['EndYear'])?'selected':'' ?>
                                >
                                    <?=$j?>
                                </option>
							<?php endfor; ?>
						</select>
			        </span>

					<input class="notEmpty" type="text" style="display: none;">
					<div class="textboxLabel ">
			            <?=$lauc->getLangStr(array(
				            'b5' => '就讀時期 (結束)',
				            'en' => 'Period of Stay (End)',
			            )) ?>
					</div>
					<div class="remark remark-warn required hide" style="clear:both;">
						<?=$lauc->getLangStr(array(
							'b5' => '必須填寫',
							'en' => 'Required',
						))?>
					</div>
				</div>
			</div>
		</span>
	<?php endfor; ?>
</div><!--

--><span class="itemData <?= $itemDataClass ?>" id="data_preSchoolExperience">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
            'b5' => $firstField['TitleB5'],
            'en' => $firstField['TitleEn'],
        )) ?>
	</div>
	<div class="dataValue dataValue-empty">－－</div>
	<div class="dataValue dataValue-non-empty">
        <span class="Y">
	        <?=$lauc->getLangStr(array(
		        'b5' => $LangB5['General']['Yes'],
		        'en' => $LangEn['General']['Yes'],
	        )) ?>
        </span>
        <span class="N">
	        <?=$lauc->getLangStr(array(
		        'b5' => $LangB5['General']['No'],
		        'en' => $LangEn['General']['No'],
	        )) ?>
        </span>
        <div class="value" style="margin-top: 10px;">
			<?php for ( $i = 0; $i < 3; $i ++ ): ?>
                <span style="width: 100%">
					<div class="itemInput-complex-order"><?= $i + 1 ?></div>
					<div class="button-deleteComplex icon icon-button"></div>
					<div class="itemInput-complex">
						<div class="textbox-floatlabel textbox-In5">
							<input
                                    type="text"
                                    class="notEmpty dataValue"
                                    id="data_preSchoolExperience_nameOfSchool_<?= $i ?>"
                                    readonly
                            />
							<div class="textboxLabel ">
						        <?=$lauc->getLangStr(array(
							        'b5' => '學校名稱',
							        'en' => 'Name of School',
						        )) ?>
							</div>
						</div>
						<div class="textbox-floatlabel textbox-In5">
							<input
                                    type="text"
                                    class="notEmpty dataValue"
                                    id="data_preSchoolExperience_locationOfSchool_<?= $i ?>"
                                    readonly
                            />
							<div class="textboxLabel ">
						        <?=$lauc->getLangStr(array(
							        'b5' => '學校地區',
							        'en' => 'Location of School',
						        )) ?>
							</div>
						</div>
						<div class="textbox-floatlabel textbox-In5">
							<input
                                    type="text"
                                    class="notEmpty dataValue"
                                    id="data_preSchoolExperience_level_<?= $i ?>"
                                    readonly
                            />
							<div class="textboxLabel ">
						        <?=$lauc->getLangStr(array(
							        'b5' => '班級',
							        'en' => 'Level / Grade',
						        )) ?>
							</div>
						</div>
						<div class="textbox-floatlabel textbox-In5">
							<input
                                    type="text"
                                    class="notEmpty dataValue"
                                    id="data_preSchoolExperience_start_<?= $i ?>"
                                    readonly
                            />
							<div class="textboxLabel ">
						        <?=$lauc->getLangStr(array(
							        'b5' => '就讀時期 (開始)',
							        'en' => 'Period of Stay (Start)',
						        )) ?>
							</div>
						</div>
						<div class="textbox-floatlabel textbox-In5">
							<input
                                    type="text"
                                    class="notEmpty dataValue"
                                    id="data_preSchoolExperience_end_<?= $i ?>"
                                    readonly
                            />
							<div class="textboxLabel ">
						        <?=$lauc->getLangStr(array(
							        'b5' => '就讀時期 (結束)',
							        'en' => 'Period of Stay (End)',
						        )) ?>
							</div>
						</div>
					</div>
				</span>
			<?php endfor; ?>
        </div>
    </div>
</span><!--

-->
<script>
  $(function () {
    var $field = $('[name="preSchoolExperience"]');
    $field.click(updateValue);
    $('[id^="preSchoolExperience_"]').change(updateValue);

    function updateValue() {
      var checkedVal = $field.filter(':checked').val();
      $('[name^="preSchoolExperience_"]').each(function () {
        var id = $(this).attr('id');
        var value = $(this).val();

        $('#data_' + id).val(value ? value : '－－');
      });

      //// Update UI START ////
      $('.preSchoolExperience_details').hide();
      $('.preSchoolExperience_details.' + checkedVal).show().css('display', 'block')
      //// Update UI END ////

      //// Update value START ////
      var $data = $('#data_preSchoolExperience');
      $data.find('.dataValue, .Y, .N, .value').hide();

      if (checkedVal == '') {
        $data.find('.dataValue-empty').show();
      } else {
        $data.find('.dataValue-non-empty, .' + checkedVal).show();
        if (checkedVal === 'Y') {
          $data.find('.value, .value *').show();
        }
      }

      for(var i=0;i<3;i++) {
        var startStr = '';
        startStr += ('0'+$('#preSchoolExperience_start_month_'+i).val()).slice(-2);
        startStr += ' / ';
        startStr += $('#preSchoolExperience_start_year_'+i).val();
        $('#data_preSchoolExperience_start_' + i).val(startStr);

        var endStr = '';
        endStr += ('0'+$('#preSchoolExperience_end_month_'+i).val()).slice(-2);
        endStr += ' / ';
        endStr += $('#preSchoolExperience_end_year_'+i).val();

        $('#data_preSchoolExperience_end_' + i).val(endStr);
      }
      //// Update value END ////
    }

    updateValue();
  });
</script>
