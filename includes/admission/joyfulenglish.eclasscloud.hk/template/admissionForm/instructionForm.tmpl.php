<?php
global $lauc;
?>
<section id="instructionForm" class="form displaySection display_pageInstruction">
	<div class="form-header marginB10">
		<?=$lauc->getLangStr(array(
			'b5' => $LangB5['Admission']['instruction'],
			'en' => $LangEn['Admission']['instruction'],
		))?>
	</div>
	<div class="sheet">
		<?=$application_setting['FirstPageContent'] ?>

		<div style="margin-top: 10px;">
    		<input type="checkbox" id="agreeTerms"/>
    		<label for="agreeTerms">
			    <?=$lauc->getLangStr(array(
				    'b5' => $LangB5['Admission']['AgreeTermsCondition'],
				    'en' => $LangEn['Admission']['AgreeTermsCondition'],
			    ))?>
		    </label>
    		<span class="itemInput">
				<div id="errAgreeTerms" class="remark remark-warn hide" style="float: left;">
				    <?=$lauc->getLangStr(array(
					    'b5' => $LangB5['Admission']['required'],
					    'en' => $LangEn['Admission']['required'],
				    ))?>
				</div>
			</span>
		</div>
	</div>
</section>

<script>
$(function(){
	'use strict';
	window.checkInstructionForm = (function(lifecycle){
		var isValid = true;

		/**** Check required START ****/
		$('#errAgreeTerms').addClass('hide');
		if($('#agreeTerms:checked').length == 0){
			$('#errAgreeTerms').removeClass('hide');
			$('#agreeTerms').focus();
			isValid = false;
		}
		/**** Check required END ****/

		return isValid;
	});

	window.validateFunc['pageInstruction'].push(checkInstructionForm);
});
</script>
