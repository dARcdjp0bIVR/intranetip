$(function () {
  window.validator['CUSTOM']['permanentResident'] = (function (fieldName, isRequired, args) {
    var isValid = true;
    var $field = $('[name="' + fieldName + '"]:checked');
    var value = $field.val();
    
    if($field.length === 0){
      return true;
    }

    if (!isValid) {
      var $field = $('[name="' + fieldName + '"]');
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.required1').removeClass('hide').show();
    }

    if(value === 'Y') {
      var $hkidField = $('#'+fieldName+'_0');
      if ($hkidField.val() === '') {
        $field.closest('.itemInput').addClass('itemInput-warn');
        $field.closest('.itemInput').find('.remark-warn.required2').removeClass('hide').show();
        isValid = false;
      }
      isValid = isValid && window.validator['HKID']($hkidField);
      isValid = isValid && window.validator['HKID_DUPLICATE']($hkidField);

    }else if(value === 'N') {
      var $hkidField = $('#'+fieldName+'_1');
      if ($hkidField.val() === '') {
        $field.closest('.itemInput').addClass('itemInput-warn');
        $field.closest('.itemInput').find('.remark-warn.required3').removeClass('hide').show();
        isValid = false;
      }
      isValid = isValid && window.validator['HKID_DUPLICATE']($hkidField);

      //////// Expiry START ////////
      var $fieldYear = $('#'+fieldName+'_year');
      var $fieldMonth = $('#'+fieldName+'_month');
      var $fieldDay = $('#'+fieldName+'_day');
      if ($fieldYear.val() === '' || $fieldMonth.val() === '' || $fieldDay.val() === '') {
        $fieldYear.closest('.itemInput').addClass('itemInput-warn');
        $fieldYear.closest('.itemInput').find('.remark-warn.required4').removeClass('hide').show();
        isValid = false;
      }

      //// Check format START ////
      var year = parseInt($fieldYear.val());
      var month = parseInt($fieldMonth.val());
      var day = parseInt($fieldDay.val());
      var dateIsValid = true;
      // Check the ranges of month and year
      if (year < 1000 || year > 3000 || month == 0 || month > 12) {
        dateIsValid = false;
      }

      var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

      // Adjust for leap years
      if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

      // Check the range of the day
      dateIsValid = dateIsValid && day > 0 && day <= monthLength[month - 1];
      //// Check format END ////

      if (!dateIsValid) {
        $fieldYear.closest('.itemInput').addClass('itemInput-warn');
        $fieldYear.closest('.itemInput').find('.remark-warn.date').removeClass('hide').show();
        isValid = false;
      }
      //////// Expiry END ////////


    }

    return isValid;
  });

  window.validator['CUSTOM']['address'] = (function (fieldName, isRequired, args) {
    var isValid = true;

    switch (args['field']) {
      case 'flat':
      case 'floor':
      case 'block':
        var flat = $('[data-field="flat"]').val();
        //var floor = $('[data-field="floor"]').val();
        //var block = $('[data-field="block"]').val();

        isValid = !!(flat);
        break;
      case 'building':
      case 'street':
        var building = $('[data-field="building"]').val();
        var street = $('[data-field="street"]').val();

        isValid = !!(building + street);
        break;
      case 'm_flat':
      case 'm_floor':
      case 'm_block':
        var flat = $('[data-field="m_flat"]').val();
        //var floor = $('[data-field="m_floor"]').val();
        //var block = $('[data-field="m_block"]').val();

        isValid = !!(flat);
        break;
      case 'm_building':
      case 'm_street':
        var building = $('[data-field="m_building"]').val();
        var street = $('[data-field="m_street"]').val();

        isValid = !!(building + street);
        break;
    }

    if (!isValid) {
      var $fields = $('[name="' + fieldName + '"]');
      $fields.closest('.itemInput').addClass('itemInput-warn');
      $fields.closest('.itemInput').find('.remark-warn.required').removeClass('hide').show();
    }

    return isValid;
  });

  window.validator['CUSTOM']['previouslySchool'] = (function (fieldName, isRequired, args) {
    var isValid = true;

    if ($('[name="preSchoolExperience"]:checked').length == 0) {
      isValid = false;
      var $fields = $('[name="preSchoolExperience"]');
      $fields.closest('.itemInput').addClass('itemInput-warn');
      $fields.closest('.itemInput').find('.remark-warn.required').removeClass('hide').show();
    }

    if ($('#preSchoolExperience_Y').prop('checked')) {
      isValid = window.validator['REQUIRED']('preSchoolExperience_nameOfSchool_0') && isValid;
      isValid = window.validator['REQUIRED']('preSchoolExperience_locationOfSchool_0') && isValid;
      isValid = window.validator['REQUIRED']('preSchoolExperience_level_0') && isValid;
      isValid = window.validator['REQUIRED']('preSchoolExperience_start_month_0') && isValid;
      isValid = window.validator['REQUIRED']('preSchoolExperience_start_year_0') && isValid;
      isValid = window.validator['REQUIRED']('preSchoolExperience_end_month_0') && isValid;
      isValid = window.validator['REQUIRED']('preSchoolExperience_end_year_0') && isValid;

      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_locationOfSchool_1', 'preSchoolExperience_nameOfSchool_1') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_level_1', 'preSchoolExperience_nameOfSchool_1') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_start_month_1', 'preSchoolExperience_nameOfSchool_1') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_start_year_1', 'preSchoolExperience_nameOfSchool_1') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_end_month_1', 'preSchoolExperience_nameOfSchool_1') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_end_year_1', 'preSchoolExperience_nameOfSchool_1') && isValid;

      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_locationOfSchool_2', 'preSchoolExperience_nameOfSchool_2') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_level_2', 'preSchoolExperience_nameOfSchool_2') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_start_month_2', 'preSchoolExperience_nameOfSchool_2') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_start_year_2', 'preSchoolExperience_nameOfSchool_2') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_end_month_2', 'preSchoolExperience_nameOfSchool_2') && isValid;
      isValid = window.validator['REQUIRED_WITH_FIELD']('preSchoolExperience_end_year_2', 'preSchoolExperience_nameOfSchool_2') && isValid;
    }

    return isValid;
  });

  window.validator['CUSTOM']['sibling'] = (function (fieldName, isRequired, args) {
    var isValid = true;
    var numOfSibling = $('#numOfSibling').val();

    for (var i = 0; i < numOfSibling - 1; i++) {
      isValid = window.validator['REQUIRED']('siblingName_' + i) && isValid;
      isValid = window.validator['ENGLISH']('siblingName_' + i) && isValid;
      isValid = window.validator['REQUIRED']('siblingAge_' + i) && isValid;
      //isValid = window.validator['POSTIVE_NUMBER']('siblingAge_' + i) && isValid;
      isValid = window.validator['REQUIRED']('siblingBirthCert_' + i) && isValid;
      //isValid = window.validator['REQUIRED']('siblingCurrentSchool_' + i) && isValid;
      //isValid = window.validator['REQUIRED']('siblingCurrentGrade_' + i) && isValid;
      isValid = window.validator['REQUIRED']('siblingSameSchoolGraduated_' + i) && isValid;
      if ($('#siblingSameSchoolGraduated_' + i + '_Y:checked').val() === 'Y') {
        isValid = window.validator['REQUIRED']('siblingSameSchoolGraduatedYear_' + i) && isValid;
      }

      isValid = window.validator['REQUIRED']('siblingSameSchoolApply_' + i) && isValid;
    }

    return isValid;
  });

  window.validator['CUSTOM']['campus'] = (function (fieldName, isRequired) {
    var $fields = $('[name="' + fieldName + '[]"]');
    if ($fields.length == 0) {
      return true;
    }
    var isValid = true;
    var values = [];
    $fields.each(function () {
      if ($(this).val()) {
        values.push($(this).val());
      }
    });
    if (values.length == 0) {
      if (isRequired) {
        $fields.closest('.itemInput').addClass('itemInput-warn');
        $fields.closest('.itemInput').find('.remark-warn.required').removeClass('hide').show();
        return false;
      } else {
        return true;
      }
    } else {
      var firstEmptyIndex = $fields.length;
      var lastNonEmptyIndex = 0;
      $fields.each(function (index) {
        if ($(this).val()) {
          lastNonEmptyIndex = index;
        } else {
          firstEmptyIndex = (firstEmptyIndex < index) ? firstEmptyIndex : index;
        }
      });

      if (lastNonEmptyIndex > firstEmptyIndex) {
        $fields.closest('.itemInput').addClass('itemInput-warn');
        $fields.closest('.itemInput').find('.remark-warn.not_complete').removeClass('hide').show();
        isValid = false;
      }
    }

    if (_.uniq(values).length !== values.length) {
      $fields.closest('.itemInput').addClass('itemInput-warn');
      $fields.closest('.itemInput').find('.remark-warn.option_repeat').removeClass('hide').show();
      isValid = false;
    }
    return isValid;
  });

  window.validator['CUSTOM']['atLeastOneParent'] = function (fieldName, fieldId, args) {
    var $field = $('[name="' + fieldName + '"]');
    if ($field.length == 0) {
      return true;
    }

    var fatherName = $('[data-field="father"]').val();
    var motherName = $('[data-field="mother"]').val();

    var isValid = true;
    isValid = (fatherName + motherName).length > 0;

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.custom').html('At least input one parent information 最少輸入一名家長資料').removeClass('hide').show();
    }
    return isValid;
  }
  
  window.validator['CUSTOM']['contactPriority'] = function (fieldName, fieldId, args) {
    var $field = $('[name="' + fieldName + '"]');
    if ($field.val() == 0) {
      return true;
    }

    var fatherContactPriority = $('[data-field="fContactPriority"]').val();
    var motherContactPriority = $('[data-field="mContactPriority"]').val();

    var isValid = true;
    isValid = fatherContactPriority != motherContactPriority;

    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.custom').html('Duplicate selection 選擇重覆').removeClass('hide').show();
    }
    
    isValid = !(fatherContactPriority > 1 && motherContactPriority < 1 || fatherContactPriority < 1 && motherContactPriority > 1);
    
    if (!isValid) {
      $field.closest('.itemInput').addClass('itemInput-warn');
      $field.closest('.itemInput').find('.remark-warn.custom').html('Must be 1 if only input one parent information 如只輸入一名家長資料，必須輸入1').removeClass('hide').show();
    }
    
    return isValid;
  }
});
