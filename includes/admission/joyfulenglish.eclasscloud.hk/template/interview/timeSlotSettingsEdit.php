<table id="FormTable" class="form_table">
    <tbody>
    <tr>
        <td class="field_title" style="width:30%;"><?=$kis_lang['date']?><?=$mustfillinsymbol?></td>
        <td style="width:70%;">
            <input type="text" name="Date" id="Date" value="<?=$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Date']?>">&nbsp;<span class="error_msg" id="warning_Date"></span>
        </td>
    </tr>
    <tr>
        <td class="field_title" style="width:30%;"><?=$kis_lang['timeslot']?><?=$mustfillinsymbol?></td>
        <td style="width:70%;">
            <table class="form_table" id="timeslot_table">

				<?for($i=0; $i<sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']); $i++){?>
                    <tr>
                        <td style="width:230px"><?=$libadmission->Get_Time_Selection($linterface,'StartTime_'.$i,$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime'][$i],'',false)?>&nbsp;<span class="error_msg" id="warning_StartTime_<?=$i?>"></span> ~
							<?=$libadmission->Get_Time_Selection($linterface,'EndTime_'.$i,$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['EndTime'][$i],'',false)?>&nbsp;<span class="error_msg" id="warning_EndTime_<?=$i?>"></span>
							<?=$linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)?>
                        </td>
                        <td>
							<?if($i>0){?>
                            <div class="table_row_tool"><a title="<?=$kis_lang['Delete']?>" id="DeleteTimeslot" onClick="$(this).parent().parent().parent().remove();" class="delete_dim" href="javascript:void(0);"></a>
								<?}?>
                        </td>
                    </tr>
				<?}?>
				<?if(sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']) <=0){?>
                    <tr>
                        <td style="width:230px"><?=$libadmission->Get_Time_Selection($linterface,'StartTime_0','','',false)?>&nbsp;<span class="error_msg" id="warning_StartTime_0"></span> ~
							<?=$libadmission->Get_Time_Selection($linterface,'EndTime_0','','',false)?>&nbsp;<span class="error_msg" id="warning_EndTime_0"></span>
							<?=$linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)?>
                        </td>
                        <td>
                        </td>
                    </tr>
				<?}?>
                <tr id="timeslot_table_last">
                    <td colspan="2">
                        <div class="table_row_tool"><a title="<?=$kis_lang['add']?>" id="AddTimeslot" class="add_dim" href="javascript:void(0);"></a></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="field_title" style="width:30%;"><?=$admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['roomsetting']:$kis_lang['groupsetting']?><?=$mustfillinsymbol?></td>
        <td style="width:70%;">
			<?php
			if($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Group']){
				$numberChecked = '';
				$customChecked = 'checked';
				$groupCheckedArr = explode(',', ($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Group']));
			}else{
				$numberChecked = 'checked';
				$customChecked = '';
				$groupCheckedArr = array();
			}

			if($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'){
				$langNumOf = $kis_lang['numofroom'];
				$langCustom = $kis_lang['customroom'];
			}else{
				$langNumOf = $kis_lang['numofgroup'];
				$langCustom = $kis_lang['customgroup'];
			}

			if($admission_cfg['interview_arrangment']['interview_group_name']){
				$groups = $admission_cfg['interview_arrangment']['interview_group_name'];
			}else{
				$groups = range('A', 'Z');
			}
			?>

            <div style="margin-bottom: 5px;">
                <input type="radio" id="groupTypeSetting_num" name="groupType" value="number" <?=$numberChecked ?> />
                <label for="groupTypeSetting_num"><?=$langNumOf?>: </label>
				<?=getSelectByValue(range(1, count($groups)), ' id="NumOfGroup" name="NumOfGroup"', $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['NumOfGroup'], 1, 0, $kis_lang['Admission']['pleaseSelect']) ?>
            </div>

            <div style="margin-bottom: 5px;">
                <input type="radio" id="groupTypeSetting_custom" name="groupType" value="custom" <?=$customChecked ?> />
                <label for="groupTypeSetting_custom"><?=$langCustom?>: </label>
                <div id="groupType_customField" style="margin-left: 20px;margin-top: 5px;">
                    <label>
                        <input id="customGroup_all" type="checkbox" value="all"/>
						<?=$Lang['Btn']['SelectAll']?>
                    </label>
                    <br />

					<?php
					foreach($groups as $index => $group){
						$checked = (in_array($group, $groupCheckedArr))? 'checked':'';
						?>
                        <label style="min-width: 80px;display:inline-block;">
                            <input type="checkbox" class="customGroup" name="customGroup[]" value="<?=$group ?>" <?=$checked ?>/>
							<?=$group ?>
                        </label>

						<?php if($index % 5 == 4){ ?>
                            <br />
						<?php } ?>
						<?php
					}
					?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="field_title" style="width:30%;"><?=$kis_lang['qouta']?><?=$mustfillinsymbol?></td>
        <td style="width:70%;">
			<?=$linterface->GET_TEXTBOX("Quota", "Quota", $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Quota'], $OtherClass='', $OtherPar=array()).'<br />'?>
        </td>
    </tr>
    <tr>
        <td class="field_title" style="width:30%;"><?=$kis_lang['Admission']['JOYFUL']['Campas']?><?=$mustfillinsymbol?></td>
        <td style="width:70%;">
			<select name="Extra">
				<?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                    <option
                            value="<?= $type ?>"
							<?= ( $type == $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Extra'] ) ? 'selected' : '' ?>
                    >
						<?= $kis_lang['Admission']['JOYFUL']['SchoolType'][ $type ] ?>
                    </option>
				<?php endforeach; ?>
            </select>
        </td>
    </tr>
    </tbody>
</table>
