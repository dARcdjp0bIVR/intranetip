<?php
// modifying by:
/**
 * Change Log:
 * 2020-03-30 Sam
 * - File Created
 */
    global $libkis_admission;
    $schoolYearID = $kis_data['schoolYearID'];
    $applicationID = $kis_data['applicationID'];
    
    $SiblingInfo = $libkis_admission->getApplicationSibling($schoolYearID, '', $applicationID);
    $SiblingPosition = $libkis_admission->getApplicationCustInfo($applicationID, 'SiblingPosition', true)['Value'];
    $NumOfSibling = $libkis_admission->getApplicationCustInfo($applicationID, 'NumOfSibling', true)['Value'];
?>


<style>
.hide{
    display:none;
}
.itemInput .remark {
    margin-top: 5px;
}
.itemInput .remark.remark-warn {
    color: #dd2c00;
}
.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>

<?php if($isEdit):?>

<script>

function enableRemark($el, className, replace=null) {
	$remark = $el.parents('.itemInput').find(className);
	$remark.removeClass('hide');
	if(replace) {
		var msg = $remark.data('message');
		Object.keys(replace).forEach(function(key){
			var re = new RegExp('{'+key+'}',"g");
			msg = msg.replace(re, replace[key]); 
		});
		$remark.html(msg);
	}
}

function trimVal($el) {
	$el.val($el.val().trim());
}

function checkSiblingForm($form) {
	var pass = true;
	$('.remark').addClass('hide');
	$siblingPosition = $form.find('input[name=siblingPosition]').first();
	$numOfSibling = $form.find('input[name=numOfSibling]').first();
	if(!$siblingPosition.val() || !$numOfSibling.val()) {
		pass = false;
		enableRemark($siblingPosition, '.required');
	}
		
	if($siblingPosition.val() > $numOfSibling.val()) {
		pass = false;
		enableRemark($siblingPosition, '.number_in_range', {min: 1, max: $numOfSibling.val()});
	}
	
	$form.find('input[name^=siblingName_]').each(function() {
		var $this = $(this);
		if($this.is(":hidden")) return;
		trimVal($this);
		if(!$this.val()) {
			pass = false;
			enableRemark($this, '.required');
		}
	});
	
	$form.find('input[name^=siblingAge_]').each(function() {
		var $this = $(this);
		if($this.is(":hidden")) return;
		if(!$this.val()) {
			pass = false;
			enableRemark($this, '.required');
		}
	});
	
	$form.find('input[name^=siblingBirthCert_]').each(function() {
		var $this = $(this);
		if($this.is(":hidden")) return;
		trimVal($this);
		if(!$this.val()) {
			pass = false;
			enableRemark($this, '.required');
		}
	});
	
	$form.find('input[name^=siblingCurrentSchool_]').each(function() {
		var $this = $(this);
		if($this.is(":hidden")) return;
		trimVal($this);
//		if(!$this.val()) {
//			pass = false;
//			enableRemark($this, '.required');
//		}
	});
	
	$form.find('input[name^=siblingCurrentGrade_]').each(function() {
		var $this = $(this);
		if($this.is(":hidden")) return;
		trimVal($this);
//		if(!$this.val()) {
//			pass = false;
//			enableRemark($this, '.required');
//		}
	});
	
	$form.find('input[name^=siblingSameSchoolGraduated_]').each(function(){
		var $this = $(this);
		if($this.is(":hidden")) return;
		var name = $this.attr('name');
		var idx = name.substring(name.indexOf('_')+1); 
		var val = $this.val();
		var $yearField = $form.find('input[name=siblingSameSchoolGraduatedYear_'+idx+']');
		trimVal($yearField);
		if($this.is(':checked') && val == 'Y' &&  !$yearField.val()) {
			pass = false;
			enableRemark($this, '.required');
		}
	});
	return pass;
};

function resetColumn($form, i) {
	$form.find('input[name=siblingName_'+i+']').val('');
	$form.find('input[name=siblingAge_'+i+']').val('');
	$form.find('input[name=siblingBirthCert_'+i+']').val('');
	$form.find('input[name=siblingCurrentSchool_'+i+']').val('');
	$form.find('input[name=siblingCurrentGrade_'+i+']').val('');
	$form.find('input[name=siblingSameSchoolGraduated_'+i+'][value=N]').prop('checked', true);
	$form.find('input[name=siblingSameSchoolGraduatedYear_'+i+']').val('').hide();
	$form.find('input[name=siblingSameSchoolApply_'+i+'][value=N]').prop('checked', true);
}

function showSiblingInput($form) {
	var $numOfSibling = $form.find('input[name=numOfSibling]').first();
	var colIdx;
	var val = $numOfSibling.val();
	if(!val || val == 1){
		colIdx = 0;
	} else {
		colIdx = val-1;
	}
	if(colIdx == 0)
		$form.find('.column_title').hide();
	else
		$form.find('.column_title').show(); 
	for(var i=0; i<5; i++) {
		if(i >= colIdx) {
			resetColumn($form, i);
			$('.column_'+i).hide();
		} else {
			$('.column_'+i).show();
		}
	}
}

function initSiblingForm($form) {
	showSiblingInput($form);

	$form.find('input[name^=siblingSameSchoolGraduatedYear_]').each(function(){
		var $this = $(this);
		var val = $this.val();
		if(!val || val == 'N') {
			$this.hide();
		} else {
			$this.show();
		}
	});
}
	
$(function(){
    var $form = $('#applicant_form');

	initSiblingForm($form);
	
	$form.find('input[name=numOfSibling]').bind('change', function(){
		showSiblingInput($form);
	});

	$form.find('input[name^=siblingSameSchoolGraduated_]').bind('change', function(){
		var $this = $(this);
		var name = $this.attr('name');
		var idx = name.substring(name.indexOf('_')+1); 
		var val = $this.val();
		var $yearField = $form.find('input[name=siblingSameSchoolGraduatedYear_'+idx+']');
		if(val == 'Y') {
			$yearField.show();
		} else {
			$yearField.val('');
			$yearField.hide();
		}
	});

    $form.unbind('submit').submit(function(e) {
    	e.preventDefault();
    	if(!checkSiblingForm($form)) {
    		window.scrollTo(0,0);
    		return false;
    	}
    	var schoolYearId = $('#schoolYearId').val();
    	var recordID = $('#recordID').val();
    	var display = $('#display').val();
    	var data = $(this).serialize();
    	$.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function(success){
    		$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
        });
    });
});
</script>

<table class="form_table">
	<colgroup>
		<col style="width: 20%">
		<col style="">
	</colgroup>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['JOYFUL']['PositionInFamily']?></td>
		<td>
			<span class="itemInput">
				<div class="textbox-floatlabel">
            		<input name="siblingPosition" type="number" min="1" max="6" value="<?=$SiblingPosition?>">
            		/ 
            		<input name="numOfSibling" type="number" min="1" max="6" value="<?=$NumOfSibling?>">
        		</div>
        		<div>
    				<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
    				<div class="remark remark-warn number_in_range hide" style="clear:both;" data-message="範圍必須由 {min} - {max} Must between {min} - {max}"></div>
				</div>
			</span>
		</td>
	</tr>
</table>

<table class="form_table">
	<colgroup>
    	<col style="width: 20%">
    	<?php for($i=0; $i<5; $i++): ?>
    	<col style="">
    	<?php endfor; ?>
	</colgroup>
	<tr>
		<td class="column_title">&nbsp;</td>
		<?php for($i=0; $i<5; $i++): ?>
		<td class="form_guardian_head column_<?=$i?>">
			<?=$kis_lang['Admission']['JOYFUL']['Sibling'].' '.($i+1)?>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['englishname']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput">
				<div class="textbox-floatlabel">
        		<input name="siblingName_<?=$i?>" type="text" class="textboxtext" value="<?=($i<$size)?$SiblingInfo[$i]['EnglishName']:''?>"/>
        		<div>
    				<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
				</div>
			</span>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['Age']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput">
				<div class="textbox-floatlabel">
        		<input name="siblingAge_<?=$i?>" type="text" class="textboxtext" value="<?=($i<$size)?$SiblingInfo[$i]['Age']:''?>"/>
        		<div>
    				<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
				</div>
			</span>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['BirthCertOrPassport']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput">
				<div class="textbox-floatlabel">
        		<input name="siblingBirthCert_<?=$i?>" type="text" class="textboxtext" value="<?=($i<$size)?$SiblingInfo[$i]['BirthCertNo']:''?>"/>
        		<div>
    				<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
				</div>
			</span>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['CurrentSchool']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput">
				<div class="textbox-floatlabel">
        		<input name="siblingCurrentSchool_<?=$i?>" type="text" class="textboxtext" value="<?=($i<$size)?$SiblingInfo[$i]['CurrentSchool']:''?>"/>
			</span>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['CurrentGradeAttending']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput">
				<div class="textbox-floatlabel">
        		<input name="siblingCurrentGrade_<?=$i?>" type="text" class="textboxtext" value="<?=($i<$size)?$SiblingInfo[$i]['Year']:''?>"/>
			</span>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['GraduatedHere']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput itemInput-choice">
            	<span>
            		<input type="radio" id="siblingSameSchoolGraduatedYes_<?=$i?>" name="siblingSameSchoolGraduated_<?=$i?>" value="Y" 
            			<?=$SiblingInfo[$i]['GraduateSameSchool'] != '' ? 'checked' : ''?>/>
            		<label for="siblingSameSchoolGraduatedYes_<?=$i?>">是 Yes</label>
            	</span>
            	<span>
            		<input type="radio" id="siblingSameSchoolGraduatedNo_<?=$i?>" name="siblingSameSchoolGraduated_<?=$i?>" value="N"
            			<?=$SiblingInfo[$i]['GraduateSameSchool'] == '' ? 'checked' : ''?>/>
            		<label for="siblingSameSchoolGraduatedNo_<?=$i?>">否 No</label>
            	</span>
                <span>
                    <div class="textbox-floatlabel textbox-in1">
                        <input type="text" class="textboxtext" id="siblingSameSchoolGraduatedYear_<?=$i?>" name="siblingSameSchoolGraduatedYear_<?=$i?>" value="<?=$SiblingInfo[$i]['GraduateSameSchool']?>" placeholder="<?=$kis_lang['Admission']['JOYFUL']['GraduatedYear']?>"/>
                    </div>
            	</span>
            	<div>
    				<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
				</div>
    		</span>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['CurrentApply']?></td>
		<?php for($i=0,$size=count($SiblingInfo); $i<5; $i++): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<span class="itemInput itemInput-choice">
            	<span>
            		<input type="radio" id="siblingSameSchoolApplyYes_<?=$i?>" name="siblingSameSchoolApply_<?=$i?>" value="Y" 
            			<?=$SiblingInfo[$i]['ApplyingSameSchool'] == 'Y' ? 'checked' : ''?>/>
            		<label for="siblingSameSchoolApplyYes_<?=$i?>">是 Yes</label>
            	</span>
            	<span>
            		<input type="radio" id="siblingSameSchoolApplyNo_<?=$i?>" name="siblingSameSchoolApply_<?=$i?>" value="N"
            		<?=$SiblingInfo[$i]['ApplyingSameSchool'] == 'N' || $SiblingInfo[$i]['ApplyingSameSchool'] == '' ? 'checked' : ''?>/>
            		<label for="siblingSameSchoolApplyNo_<?=$i?>">否 No</label>
            	</span>
    		</span>
		</td>
		<?php endfor; ?>
	</tr>
</table>

<?php else:?>

<script>

function showSibling($form) {
	var colIdx;
	var val = <?=$NumOfSibling?$NumOfSibling:1?>;
	if(!val || val == 1){
		colIdx = 0;
	} else {
		colIdx = val-1;
	}
	if(colIdx == 0)
		$form.find('.column_title').hide();
	else
		$form.find('.column_title').show(); 
	for(var i=0; i<5; i++) {
		if(i >= colIdx)
			$('.column_'+i).hide();
		else
			$('.column_'+i).show();
	}
}

$(function(){
    var $form = $('.form_table');
	showSibling($form);
});

</script>

<table class="form_table">
	<colgroup>
		<col style="width: 20%">
		<col style="">
	</colgroup>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['JOYFUL']['PositionInFamily']?></td>
		<td><?=$SiblingPosition?> / <?=$NumOfSibling?></td>
	</tr>
</table>

<table class="form_table">
	<colgroup>
    	<col style="width: 20%">
    	<?php for($i=0; $i<5; $i++): ?>
    	<col style="">
    	<?php endfor; ?>
	</colgroup>
	<tr>
		<td class="column_title">&nbsp;</td>
		<?php for($i=0; $i<5; $i++): ?>
		<td class="form_guardian_head column_<?=$i?>">
			<?=$kis_lang['Admission']['JOYFUL']['Sibling'].' '.($i+1)?>
		</td>
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['englishname']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>"><?=$SiblingInfo[$i]['EnglishName']?></td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['Age']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>"><?=$SiblingInfo[$i]['Age']?></td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['BirthCertOrPassport']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>"><?=$SiblingInfo[$i]['BirthCertNo']?></td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['CurrentSchool']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>"><?=$SiblingInfo[$i]['CurrentSchool']?></td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['CurrentGradeAttending']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>"><?=$SiblingInfo[$i]['Year']?></td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['GraduatedHere']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<?=$SiblingInfo[$i]['GraduateSameSchool']=='' ? '--' : "{$SiblingInfo[$i]['GraduateSameSchool']}"?>
		</td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
	<tr>
		<td class="field_title column_title"><?=$kis_lang['Admission']['JOYFUL']['CurrentApply']?></td>
		<?php for($i=0; $i<5; $i++): ?>
		<?php if($i < count($SiblingInfo)): ?>
		<td class="form_guardian_field column_<?=$i?>">
			<?=$SiblingInfo[$i]['ApplyingSameSchool']=='Y' ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no']?>
		</td>
		<?php else: ?>
		<td  class="form_guardian_field column_<?=$i?>">--</td>	
		<?php endif; ?>	
		<?php endfor; ?>
	</tr>
</table>

<?php endif;?>

