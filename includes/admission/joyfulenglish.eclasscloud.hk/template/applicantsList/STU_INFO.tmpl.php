<?php
global $admission_cfg;

$blankphoto = $kis_data['blankphoto'];
$attachmentList = $kis_data['attachmentList'];
$attachmentSettings = $lac->getAttachmentSettings();

global $dobStart, $dobEnd;
$application_setting = $lac->getApplicationSetting();
$dobStart = $application_setting[$kis_data['applicationInfo']['classLevelID']]['DOBStart'];
$dobEnd = $application_setting[$kis_data['applicationInfo']['classLevelID']]['DOBEnd'];

$allCustInfo = $lac->getAllApplicationCustInfo($applicationData['ADMISSION_STU_INFO']['ApplicationID']);

// debug_r($fields);
// debug_r($applicationData);
?>
<?= $this->generateValidateJs($fields); ?>

<style>
    .hide {
        display: none;
    }

    .itemInput .remark {
        margin-top: 5px;
    }

    .itemInput .remark.remark-warn {
        color: #dd2c00;
    }
</style>
<table class="form_table">
    <tbody>
    <?php
    foreach ($fields as $index => $field):
        if ($field['ExtraArr']['skipDisplayInAdmin']) {
            continue;
        }

        $title = Get_Lang_Selection($field['TitleB5'], $field['TitleEn']);
        $dbTable = $field['OtherAttributeArr']['DbTableName'];
        $_dbField = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
        $dbField = $_dbField[0];

        $data = $applicationData[$dbTable];
        $value = $data[$dbField];

        if($field['ExtraArr']['field']==='CustBirthCertNo') {
	        $value = $allCustInfo['BirthCertNo'][0]['Value'];
        }elseif($field['ExtraArr']['field']==='permanentResident') {
            if($isEdit) {
                $value = '';
	            if ( $applicationData['ADMISSION_STU_INFO']['BirthCertType'] == $admission_cfg['BirthCertType']['hkid'] ) {
	                $value .= 'Y;';
		            $value .= $applicationData['ADMISSION_STU_INFO']['BirthCertNo'];
	            }else{
		            $value .= 'N;';
		            $value .= $applicationData['ADMISSION_STU_INFO']['BirthCertNo'].';';
		            $value .= $allCustInfo['Visa'][0]['Value'];
	            }
            }else{
	            $value = '';
	            if ( $applicationData['ADMISSION_STU_INFO']['BirthCertType'] == $admission_cfg['BirthCertType']['hkid'] ) {
		            $value .= $kis_lang['Admission']['yes'] . '<br />';
		            $value .= Get_Lang_Selection( '香港身份證號碼', 'Hong Kong ID No.' ) . ':';
	                $value .= $applicationData['ADMISSION_STU_INFO']['BirthCertNo'];
	            } else {
		            $value .= $kis_lang['Admission']['no'] . '<br />';
		            $value .= Get_Lang_Selection( '護照號碼', 'Passport No.' ) . ':';
	                $value .= $applicationData['ADMISSION_STU_INFO']['BirthCertNo'].'<br />';
		            $value .= Get_Lang_Selection( '受養人簽證到期日', 'Dependent Visa Expiry Date' ) . ':';
		            $value .= $allCustInfo['Visa'][0]['Value'];
	            }
            }
        }
        ?>

        <?php if ($index == 0): ?>
        <tr>
            <td width="30%" class="field_title">
                <?= $title ?>
            </td>
            <td width="40%">
                <?php if ($isEdit): ?>
                    <?= $this->generateAdminDetailsPageFieldHtml($field, $value) ?>
                <?php else: ?>
                    <?= kis_ui::displayTableField($value) ?>
                <?php endif; ?>
            </td>
            <td width="30%" rowspan="7" width="145">
                <?php if ($isEdit): ?>
                    <div id="studentphoto" class="student_info" style="margin: 0px;cursor: pointer;">
                        <img
                                src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
                        <div class="mail_icon_form"
                             style="position: absolute;top: -5px;right: -40px; <?= $attachmentList['personal_photo']['link'] ? '' : 'display:none;' ?>">
                            <a id="btn_remove" href="#" class="btn_remove"></a>
                        </div>
                        <div class="text_remark" style="text-align: center;">
                            <?= $kis_lang['Admission']['msg']['clicktouploadphoto'] ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div id="studentphoto" class="student_info" style="margin:0px;">
                        <img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
                    </div>
                <?php endif; ?>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td class="field_title">
                <?= $title ?>
            </td>
            <td>
                <?php if ($isEdit): ?>
                    <?= $this->generateAdminDetailsPageFieldHtml($field, $value) ?>
                <?php else: ?>
                    <?= kis_ui::displayTableField($value) ?>
                <?php endif; ?>
            </td>
        </tr>
    <?php endif; ?>
    <?php
    endforeach;
    ?>


    <?php if ($isEdit): ?>
        <!-- ######## Document START ######## -->
        <?php
        for ($i = 0; $i < sizeof($attachmentSettings); $i++) {

            // #### Check ClassLevel START ####
            if ($attachmentSettings[$i]['ClassLevelStr']) {
                $classLevelArr = explode(',', $attachmentSettings[$i]['ClassLevelStr']);

                if (!in_array($StudentInfo['classLevelID'], $classLevelArr)) {
                    continue;
                }
            }
            // #### Check ClassLevel END ####

            // #### Get attachment name START #### //
            $attachment_name = $attachmentSettings[$i]['AttachmentName'];
            $attachmentNameDisplay = $attachment_name;

            // ## Multi-lang START ## //
            if ($admission_cfg['MultipleLang'] && !$sys_custom['KIS_Admission']['CREATIVE']['Settings']) {
                foreach ($admission_cfg['Lang'] as $index => $lang) {
                    if ($lang == $intranet_session_language) {
                        $attachmentNameDisplay = $attachmentSettings[$i]["AttachmentName{$index}"];
                    }
                }
            }
            // ## Multi-lang END ## //

            // ## Cust-lang START ## //
            $custAttachmentName = '';
            $attachmentNameDisplay = ($custAttachmentName) ? $custAttachmentName : $attachmentNameDisplay;
            // ## Cust-lang END ## //
            // #### Get attachment name END #### //

            $_filePath = $attachmentList[$attachment_name]['link'];
            if ($_filePath) {
                $_spanDisplay = '';
                $_buttonDisplay = ' style="display:none;"';
                $_filePath = '/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$lac->schoolYearID.'&id='.$ApplicationID.'&type='.urlencode($attachment_name);
            } else {
                $_filePath = '';
                $_spanDisplay = ' style="display:none;"';
                $_buttonDisplay = '';
            }

            $_attachment = "<a href ='" . $_filePath . "' class='file_attachment' target='_blank'>" . $kis_lang['view'] . "</a>";
            $_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="' . $kis_lang['delete'] . '"></a></div>';
            ?>

            <tr>
                <td class="field_title">
                    <?= $attachmentNameDisplay ?>
                </td>
                <td id="<?= $attachment_name ?>"><span class="view_attachment"
        				<?= $_spanDisplay ?>><?= $_attachment ?></span> <input type="button"
                                                                               class="attachment_upload_btn formsmallbutton"
                                                                               value="<?= $kis_lang['Upload'] ?>"
                                                                               id="uploader-<?= $attachment_name ?>"
                        <?= $_buttonDisplay ?> /></td>
            </tr>
            <?php
        }
        ?>
    <?php else: ?>
        <tr>
            <td class="field_title">
                <?= $kis_lang['Admission']['document'] ?>
            </td>
            <td colspan="2">
                <?php
                $attachmentAry = array();
                for ($i = 0; $i < sizeof($attachmentSettings); $i++) {
                    $attachment_name = $attachmentSettings[$i]['AttachmentName'];
                    $_filePath = $attachmentList[$attachment_name]['link'];
                    if ($_filePath) {
                        //$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
                        $attachmentAry[] = '<a href="/kis/apps/admission/templates/applicantslist/download_attachment.php?year='.$lac->schoolYearID.'&id='.$ApplicationID.'&type='.urlencode($attachment_name).'" target="_blank">'.$attachment_name.'</a>';

                    }
                }
                ?>
                <?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
            </td>
        </tr>
    <?php endif; ?>
    <!-- ######## Document END ######## -->
    </tbody>
</table>

<input type="hidden" name="isEdit" value="0" />
<input type="hidden" name="isTeacher" value="1" />
<script>
    kis.uploader({
        browse_button: 'studentphoto',
        url: './apps/admission/ajax.php?action=updatephoto&id=<?=$kis_data['recordID'] ?>',
        multi_selection: false,
        auto_start: true,
        onFilesAdded: function (up, files) {
            $('#studentphoto img').css({opacity: 0.5});
        },
        onUploadProgress: function (up, file) {

        },
        onFileUploaded: function (up, file, info) {

            var res = $.parseJSON(info.response);
            if (res.error) {
                alert(res.error);
            } else if (res.personal_photo) {
                $('#studentphoto img').attr('src', res.personal_photo).show();
                $('#studentphoto .mail_icon_form').show();
            }
            $('#studentphoto img').css({opacity: 1});

        }
    });
    $('#studentphoto .btn_remove').click(function () {
        if (confirm(lang.deletecurrentpersonalphoto)) {
            kis.showLoading();
            $(this).parent().hide();
            $.post('apps/admission/ajax.php?action=removeAttachment', {
                id:<?=$kis_data['recordID'] ?>,
                type: 'personal_photo'
            }, function (data) {
                $('#studentphoto img').attr('src', KIS_BLANK_PHOTO).show();
                kis.hideLoading();
            });
        }
        return false;
    })
    $('#applicant_form').unbind('submit').submit(function (e) {
        e.preventDefault();

        window.scrollTo(0, 0);
        var schoolYearId = $('#schoolYearId').val();
        var recordID = $('#recordID').val();
        var display = $('#display').val();
        var timeSlot = lang.timeslot.split(',');
        var data = $('#applicant_form').serialize();
        var result = check_input_info();

        if(typeof(result) === 'boolean'){
          $.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function (success) {
            $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
          });
        }else {
          result.done(function () {
            $.post('apps/admission/ajax.php?action=updateApplicationInfo', $('#applicant_form').serialize(), function (success) {
              $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
            });
          });
        }

        return false;
    });
</script>
