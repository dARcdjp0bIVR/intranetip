<?php
global $applicationData;

$preSchoolExperience = '';
foreach($applicationData['ADMISSION_CUST_INFO'] as $custInfo){
    if($custInfo['Code'] === 'PreSchoolExperience'){
	    $preSchoolExperience = $custInfo['Value'];
	    break;
    }
}

$preSchoolInfoArr = BuildMultiKeyAssoc($applicationData['ADMISSION_STU_PREV_SCHOOL_INFO'], 'SchoolOrder');
foreach($preSchoolInfoArr as $order => $preSchoolInfo){
	$preSchoolInfoArr[$order]['StartMonth'] = substr($preSchoolInfo['StartDate'], 5,2);
	$preSchoolInfoArr[$order]['StartYear'] = substr($preSchoolInfo['StartDate'], 0,4);
	$preSchoolInfoArr[$order]['EndMonth'] = substr($preSchoolInfo['EndDate'], 5,2);
	$preSchoolInfoArr[$order]['EndYear'] = substr($preSchoolInfo['EndDate'], 0,4);
}

?>
<div class="item">
    <div class="itemLabel">
	    <?=$kis_lang['Admission']['JOYFUL']['PreSchoolExperience']?>
    </div>
    <span class="itemInput itemInput-choice">
		<span>
			<input
                    type="radio"
                    id="preSchoolExperience_Y"
                    name="preSchoolExperience"
                    value="Y"
                    <?=($preSchoolExperience === 'Y')?'checked':''?>
            ><label for="preSchoolExperience_Y">
            	<?=$kis_lang['Admission']['yes']?>
			</label>
		</span>
		<span>
			<input
                    type="radio"
                    id="preSchoolExperience_N"
                    name="preSchoolExperience"
                    value="N"
                    <?=($preSchoolExperience === 'N')?'checked':''?>
            ><label for="preSchoolExperience_N">
	            <?=$kis_lang['Admission']['no']?>
			</label>
		</span>
		<div style="">
			<div class="remark remark-warn required hide" style="clear:both;">
				<?=$this->getLangStr(array(
					'b5' => '必須填寫',
					'en' => 'Required',
				))?>
			</div>
		</div>
	</span>
</div>
<div id="preSchoolExperience_container" class="item preSchoolExperience_details Y">
    <div class="itemLabel"></div>
	<?php for ( $i = 0; $i < 3; $i ++ ): ?>
        <span class="itemInput">
			<table>
				<colgroup>
        			<col style="width: 20%">
        	    	<col style="width: 50%">
        	    	<col style="">
    	    	</colgroup>
				<tr>
					<td colspan="3">
						<div class="itemInput-complex-order"><?= $i + 1 ?></div>
					</td>
				</tr>
				<tr>
					<td>
        				<div class="textbox-floatlabel textbox-In5 itemInput ">
        					<input
                                    type="text"
                                    id="preSchoolExperience_nameOfSchool_<?= $i ?>"
                                    name="preSchoolExperience_nameOfSchool_<?= $i ?>"
                                    value="<?=$preSchoolInfoArr[$i]['NameOfSchool']?>"
                            />
        					<div class="textboxLabel ">
        			            <?=$kis_lang['Admission']['JOYFUL']['NameOfSchool']?>
        					</div>
        					<div class="remark remark-warn required hide" style="clear:both;">
        						<?=$this->getLangStr(array(
        							'b5' => '必須填寫',
        							'en' => 'Required',
        						))?>
        					</div>
        				</div>
					</td>
					<td>
						<div class="textbox-floatlabel textbox-In5 itemInput" style="text-align: center;">
        					<span class="selector">
        						<select id="preSchoolExperience_start_month_<?=$i?>" name="preSchoolExperience_start_month_<?=$i?>">
        							<option value="" hidden>MM</option>
        							<?php for($j=1;$j<=12;$j++):?>
        								<option
                                                value="<?=$j?>"
                                                <?=($j == (int)($preSchoolInfoArr[$i]['StartMonth']))?'selected':'' ?>
                                        >
                                            <?=substr("0$j", -2)?>
                                        </option>
        							<?php endfor; ?>
        						</select>
        			        </span>
        					/
        					<span class="selector">
        						<select id="preSchoolExperience_start_year_<?=$i?>" name="preSchoolExperience_start_year_<?=$i?>">
        							<option value="" hidden>YYYY</option>
        							<?php for($j=(date('Y') - 5);$j<(date('Y')+2);$j++):?>
        								<option
                                                value="<?=$j?>"
                                                <?=($j == $preSchoolInfoArr[$i]['StartYear'])?'selected':'' ?>
                                        >
                                            <?=$j?>
                                        </option>
        							<?php endfor; ?>
        						</select>
        			        </span>

        					<input class="notEmpty" type="text" style="display: none;">
        					<div class="textboxLabel ">
        			            <?="{$kis_lang['Admission']['JOYFUL']['PeriodOfStay']} ({$kis_lang['Admission']['JOYFUL']['Start']})"?>
        					</div>
        					<div class="remark remark-warn required hide" style="clear:both;">
        						<?=$this->getLangStr(array(
        							'b5' => '必須填寫',
        							'en' => 'Required',
        						))?>
        					</div>
    					</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<div class="textbox-floatlabel textbox-In5 itemInput">
        					<input
                                    type="text"
                                    id="preSchoolExperience_locationOfSchool_<?= $i ?>"
                                    name="preSchoolExperience_locationOfSchool_<?= $i ?>"
                                    value="<?=$preSchoolInfoArr[$i]['SchoolAddress']?>"
                            />
        					<div class="textboxLabel ">
        			            <?=$kis_lang['Admission']['JOYFUL']['LocationOfSchool']?>
        					</div>
        					<div class="remark remark-warn required hide" style="clear:both;">
        						<?=$this->getLangStr(array(
        							'b5' => '必須填寫',
        							'en' => 'Required',
        						))?>
        					</div>
    					</div>
					</td>
					<td>
						<div class="textbox-floatlabel textbox-In5 itemInput" style="text-align: center;">
        					<span class="selector">
        						<select id="preSchoolExperience_end_month_<?=$i?>" name="preSchoolExperience_end_month_<?=$i?>">
        							<option value="" hidden>MM</option>
        							<?php for($j=1;$j<=12;$j++):?>
        								<option
                                                value="<?=$j?>"
                                                <?=($j == (int)($preSchoolInfoArr[$i]['EndMonth']))?'selected':'' ?>
                                        >
                                            <?=substr("0$j", -2)?>
                                        </option>
        							<?php endfor; ?>
        						</select>
        			        </span>
        					/
        					<span class="selector">
        						<select id="preSchoolExperience_end_year_<?=$i?>" name="preSchoolExperience_end_year_<?=$i?>">
        							<option value="" hidden>YYYY</option>
        							<?php for($j=(date('Y') - 5), $jMax=(date('Y'));$j<$jMax;$j++):?>
        								<option
                                                value="<?=$j?>"
                                                <?=($j == $preSchoolInfoArr[$i]['EndYear'])?'selected':'' ?>
                                        >
                                            <?=$j?>
                                        </option>
        							<?php endfor; ?>
        						</select>
        			        </span>

        					<input class="notEmpty" type="text" style="display: none;">
        					<div class="textboxLabel ">
        			            <?="{$kis_lang['Admission']['JOYFUL']['PeriodOfStay']} ({$kis_lang['Admission']['JOYFUL']['End']})"?>
        					</div>
        					<div class="remark remark-warn required hide" style="clear:both;">
        						<?=$this->getLangStr(array(
        							'b5' => '必須填寫',
        							'en' => 'Required',
        						))?>
        					</div>
        				</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<div class="textbox-floatlabel textbox-In5 itemInput">
        					<input
                                    type="text"
                                    id="preSchoolExperience_level_<?= $i ?>"
                                    name="preSchoolExperience_level_<?= $i ?>"
                                    value="<?=$preSchoolInfoArr[$i]['Year']?>"
                            />
        					<div class="textboxLabel ">
        			            <?=$kis_lang['Admission']['JOYFUL']['LevelOrGrade']?>
        					</div>
        					<div class="remark remark-warn required hide" style="clear:both;">
        						<?=$this->getLangStr(array(
        							'b5' => '必須填寫',
        							'en' => 'Required',
        						))?>
        					</div>
        				</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</span>
	<?php endfor; ?>
</div>

<script>
  $(function () {
    var $field = $('[name="preSchoolExperience"]');
    $field.click(updateValue);
    $('[id^="preSchoolExperience_"]').change(updateValue);

    function updateValue() {
      var checkedVal = $field.filter(':checked').val();
      $('[name^="preSchoolExperience_"]').each(function () {
        var id = $(this).attr('id');
        var value = $(this).val();

        $('#data_' + id).val(value ? value : '－－');
      });

      //// Update UI START ////
      $('.preSchoolExperience_details').hide();
      $('.preSchoolExperience_details.' + checkedVal).show().css('display', 'block')
      //// Update UI END ////

      //// Reset Data START ////
      if(checkedVal == 'N') {
        $('#preSchoolExperience_container').find('input[type=text], select').val('')
      }
      //// Reset Data END ////

      //// Update value START ////
      var $data = $('#data_preSchoolExperience');
      $data.find('.dataValue, .Y, .N, .value').hide();

      if (checkedVal == '') {
        $data.find('.dataValue-empty').show();
      } else {
        $data.find('.dataValue-non-empty, .' + checkedVal).show();
        if (checkedVal === 'Y') {
          $data.find('.value, .value *').show();
        }
      }

      for(var i=0;i<3;i++) {
        var startStr = '';
        startStr += ('0'+$('#preSchoolExperience_start_month_'+i).val()).slice(-2);
        startStr += ' / ';
        startStr += $('#preSchoolExperience_start_year_'+i).val();
        $('#data_preSchoolExperience_start_' + i).val(startStr);

        var endStr = '';
        endStr += ('0'+$('#preSchoolExperience_end_month_'+i).val()).slice(-2);
        endStr += ' / ';
        endStr += $('#preSchoolExperience_end_year_'+i).val();

        $('#data_preSchoolExperience_end_' + i).val(endStr);
      }
      //// Update value END ////
    }

    updateValue();
  });
</script>
