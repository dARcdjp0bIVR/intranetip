<?php
global $admission_cfg, $intranet_root;

$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));

#### Get setting START ####
$birthCertTypeArr = array();
foreach($admission_cfg['BirthCertType'] as $key => $v){
    $birthCertTypeArr[$v] = $kis_lang['Admission']['BirthCertType'][$key];
}
#### Get setting END ####
?>

<?php if($IsUpdate): ?>
    <span class="">
        <div class="dataLabel">
            <?=$this->getLangStr(array(
	            'b5' => $field['TitleB5'],
	            'en' => $field['TitleEn']
            ))?>
        </div>
        <div class="dataValue" id="data_<?=$field['FieldID'] ?>">
            <span class="<?=$v?>"><?=$birthCertTypeArr[$data['BirthCertType']]?></span>:
            <span class="value"><?=$value?></span>

            <input
                    type="hidden"
                    id="field_<?=$field['FieldID'] ?>_value"
                    name="field_<?=$field['FieldID'] ?>_value"
                    value="<?=$value ?>"
            />
            <input
                    type="hidden"
                    id="field_<?=$field['FieldID'] ?>_type"
                    name="field_<?=$field['FieldID'] ?>_type"
                    value="<?=$data['BirthCertType'] ?>"
            />
        </div>
    </span>
<?php else: ?>
    <div class="itemInput itemInput-selector"><span class="selector" style="
    padding-top: 12px;
    margin-right: 10px;
    ">
            <select id="field_<?=$field['FieldID'] ?>_type" name="field_<?=$field['FieldID'] ?>_type" data-field="">
    <!--                <option value="" hidden="">-- 選擇 Option --</option>-->
                <?php foreach($birthCertTypeArr as $v => $lang): ?>
                    <option value="<?=$v?>" <?= ($v == $data['BirthCertType'])?'selected':'' ?>><?=$lang?></option>
                <?php endforeach; ?>
            </select>
        </span><div class="textbox-floatlabel" style="
            width: 320px;
            display: inline-block;
            max-width: 100%;
        ">
            <input
                    type="text"
                    id="field_<?=$field['FieldID'] ?>_value"
                    name="field_<?=$field['FieldID'] ?>_value"
                    value="<?=$value?>"
                    data-field=""
                    class="empty"
            />
            <div style="">
                <?php include("$intranet_root/includes/admission/HelperClass/DynamicAdmissionFormSystem/template/applicantsList/field/warning.tmpl.php"); ?>
                
            </div>
        </div>
    </div>
    <script>
    $(function(){
        $('#field_<?=$field['FieldID'] ?>_type, #field_<?=$field['FieldID'] ?>_value').change(updateValue);

        function updateValue(){
          var $dataContainer = $('#data_<?=$field['FieldID'] ?>');
          var $valueContainer = $dataContainer.find('.value');

          $dataContainer.find('span').hide();
          $dataContainer.find('.'+$('#field_<?=$field['FieldID'] ?>_type').val()).show();
          $valueContainer.show();

            if($('#field_<?=$field['FieldID'] ?>_value').val() == ''){
              $dataContainer.addClass('dataValue-empty')
              $valueContainer.html('－－');
            }else{
              $dataContainer.removeClass('dataValue-empty');
              $valueContainer.html($('#field_<?=$field['FieldID'] ?>_value').val());
            }
        }

        updateValue();
    });
    </script>
<?php endif; ?>
