<?php
global $libkis_admission;
$recordAry = $libkis_admission->getApplicationParentInfo($kis_data['schoolYearID'], $classLevelID = '', $applicationID = '', $kis_data['recordID']);
$recordCount = count($recordAry);
$kis_data['applicationInfo'] = array ();
for ($i = 0; $i < $recordCount; $i++) {
    $_type = $recordAry[$i]['type'];
    $kis_data['applicationInfo']['applicationID'] = $recordAry[$i]['applicationID'];
    $kis_data['applicationInfo']['classLevelID'] = $kis_data['applicationInfo']['classLevelID'] ? $kis_data['applicationInfo']['classLevelID'] : $recordAry[$i]['classLevelID'];
    $kis_data['applicationInfo'][$_type] = $recordAry[$i];
}

$ParentInfo = $kis_data['applicationInfo'];
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$parentTypeArr = array('M','F',);
$totalField = count($fields) / 2;


?>
<?=$this->generateValidateJs($fields); ?>

<style>
.hide{
    display:none;
}
.itemInput .remark {
    margin-top: 5px;
}
.itemInput .remark.remark-warn {
    color: #dd2c00;
}
.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%">
		<?php foreach($parentTypeArr as $parentType): ?>
			<col style="">
		<?php endforeach; ?>
	</colgroup>
	<tr>
		<td>&nbsp;</td>
		<?php foreach($parentTypeArr as $index => $parentType): ?>
			<td class="form_guardian_head">
				<center>
					<?php
					$field = $fields[($totalField * $index) + 1];
					$value = $ParentInfo[$parentType]['relationship'];
					if($isEdit):
						?>
						<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
					<?php
					else:
						?>
						<?=$kis_lang['Admission']['PG_Type'][$value] ?>
					<?php
					endif;
					?>
				</center>
			</td>
		<?php endforeach; ?>
	</tr>

    <?php
    for($i=2;$i<=17;$i++):
	    ?>
	    <tr>
	        <td class="field_title">
		        <?=Get_Lang_Selection($fields[$i]['TitleB5'], $fields[$i]['TitleEn'])?>
	        </td>
			<?php
			foreach($parentTypeArr as $index => $parentType):
				$field = $fields[($totalField * $index) + $i];

				$dbTableName = $field['OtherAttributeArr']['DbTableName'];
				$_dbFieldName = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
				$dbFieldName = $_dbFieldName[0];
				$value = '';

				if(!isset($applicationData[$dbTableName][$dbFieldName])){
					$sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
					$sqlWhereField = $sqlWhere[0];
					$sqlWhereValue = trim($sqlWhere[1], " '");

					foreach((array)$applicationData[$dbTableName] as $v){
						if($v[$sqlWhereField] == $sqlWhereValue){
							$value = $v[$dbFieldName];
							break;
						}
					}
				}else{
					$value = $applicationData[$dbTableName][$dbFieldName];
				}
				
				//Marital Status display mapping
				if(!$isEdit && strpos($dbFieldName, 'MaritalStatus') !== false) {
				    $value = $kis_lang['Admission']['JOYFUL']['MaritalStatus'][$value];
				}

				?>
	            <td class="form_guardian_field">
					<?php if($isEdit): ?>
						<?=$this->generateAdminDetailsPageFieldHtml($field, $value) ?>
					<?php else: ?>
						<?=kis_ui::displayTableField($value) ?>
					<?php endif; ?>
	            </td>
			<?php
			endforeach;
			?>
	    </tr>
	<?php
    endfor;
    ?>

</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();

	window.scrollTo(0,0);
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
  var data = $(this).serialize()
  check_input_info().then(function(){
            $.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function(success){
                $.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
            });
      });

	return false;
});
</script>
