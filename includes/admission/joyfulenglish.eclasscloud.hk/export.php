<?php
// modifying by:
/**
 * Change Log:
 * 2020-03-26 Sam
 *  - Added customization fields
 *  
 * 2020-03-26 Pun
 *  - File created
 */
global $intranet_root, $admission_cfg, $Lang, $kis_lang;

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"]) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}

if($action == 'header') {
    global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
    
    $headerArray = array();
    
    #### Get data START ####
    $dafs = new \AdmissionSystem\DynamicAdmissionFormSystem($this->schoolYearID);
    $groups = $dafs->getLastActiveAcademicYearGroups();
    
    $fields = array();
    foreach ($groups as $index => $group) {
        $_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
        $fields[$group['GroupID']] = array();
        
        foreach ($_fields as $field) {
            if (($field['Type'] != \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) ||
                ($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && $field['ExtraArr']['field'] == 'sibling')) {
                $fields[$group['GroupID']][] = $field;
            }
        }
    }
    #### Get data END ####
    
    #### First row START ####
    $column = array();
    $column[] = "";
    $column[] = "";
    $column[] = "";
    
    foreach ($groups as $group) {
        $column[] = Get_Lang_Selection($group['TitleB5'], $group['TitleEn']);
        for ($i = 0, $iMax = count($fields[$group['GroupID']]) - 1; $i < $iMax; $i++) {
            $column[] = "";
        }
        
        //preserve columns for other info
        if($group['OtherAttributeArr']['pageType'] == 'other_info') {
            $i=14;
            while($i-- > 0) $column[] = "";
        }
        
        //preserve columns for sibling matrix
        if($group['OtherAttributeArr']['pageType'] == 'siblingPage') {
            $i=40;
            while($i-- > 0) $column[] = "";
        }
    }
    $column[] = $kis_lang['creative_interview_follow_up'];
    for ($i = 0; $i < 10; $i++) {
        $column[] = "";
    }
    $column[] = $kis_lang['remarks'];
    $headerArray[] = $column;
    #### First row END ####
    
    #### Second row START ####
    $column = array();
    $column[] = $kis_lang['Admission']['admissiondate'];
    $column[] = $kis_lang['applicationno'];
    $column[] = $kis_lang['Admission']['applyLevel'];
    
    foreach ($groups as $group) {
        //preserve columns for other info
        if($group['OtherAttributeArr']['pageType'] == 'other_info') {
            $column[] = $kis_lang['Admission']['JOYFUL']['PreviouslyAttened'];
            $column[] = $kis_lang['Admission']['JOYFUL']['ClassLevel'];
            $column[] = $kis_lang['Admission']['year'];
            $column[] = $kis_lang['Admission']['JOYFUL']['PreSchoolExperience'];
            $i=0;
            while($i++ < 3) {
                $column[] = "$i.1 {$kis_lang['Admission']['JOYFUL']['NameOfSchool']}";
                $column[] = "$i.2 {$kis_lang['Admission']['JOYFUL']['LocationOfSchool']}";
                $column[] = "$i.3 {$kis_lang['Admission']['JOYFUL']['LevelOrGrade']}";
                $column[] = "$i.4 {$kis_lang['Admission']['JOYFUL']['PeriodOfStay']}";
            }
            continue;
        }
        //sibling matrix
        if($group['OtherAttributeArr']['pageType'] == 'siblingPage') {
            $column[] = $kis_lang['Admission']['JOYFUL']['PositionInFamily'];
            $i=0;
            while($i++ < 5) {
                $column[] = "$i.1 {$kis_lang['Admission']['englishname']}";
                $column[] = "$i.2 {$kis_lang['Admission']['Age']}";
                $column[] = "$i.3 {$kis_lang['Admission']['JOYFUL']['BirthCertOrPassport']}";
                $column[] = "$i.4 {$kis_lang['Admission']['JOYFUL']['CurrentSchool']}";
                $column[] = "$i.5 {$kis_lang['Admission']['JOYFUL']['CurrentGradeAttending']}";
                $column[] = "$i.6 {$kis_lang['Admission']['JOYFUL']['GraduatedHere']}";
                $column[] = "$i.7 {$kis_lang['Admission']['JOYFUL']['GraduatedYear']}";
                $column[] = "$i.8 {$kis_lang['Admission']['JOYFUL']['CurrentApply']}";
            }
            continue;
        }
        
        foreach ($fields[$group['GroupID']] as $field) {
            $column[] = Get_Lang_Selection($field['TitleB5'], $field['TitleEn']);
        }
    }
    
    ## Interview follow up START ##
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'];
    $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'];
    ## Interview follow up END ##
    
    ## Remarks START ##
    $column[] = $kis_lang['Admission']['applicationstatus'];
    $column[] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
    $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['receiptcode'] . ")";
    $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['date'] . ")";
    $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['handler'] . ")";
    $column[] = $kis_lang['Admission']['interviewdate'] . " (1)";
    $column[] = $kis_lang['Admission']['interviewdate'] . " (2)";
    $column[] = $kis_lang['Admission']['interviewdate'] . " (3)";
    $column[] = $kis_lang['Admission']['isnotified'];
    $column[] = $kis_lang['Admission']['otherremarks'];
    ## Remarks END ##
    
    $headerArray[] = $column;
    #### Second row END ####
    
    return $headerArray;
}


#### Get data START ####
## ApplicationID START ##
if (!$applicationID) {
	$otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
	$applicationID = $otherInfo['ApplicationID'];
}
## ApplicationID END ##
$StudentInfo = current($this->getApplicationStudentInfo($schoolYearID, '', $applicationID));
$allCustInfo = $this->getAllApplicationCustInfo($applicationID);
$preSchoolInfo = $this->getApplicationPrevSchoolInfo($this->schoolYearID, $classLevelID, $applicationID);
$siblingInfo =  $this->getApplicationSibling($this->schoolYearID, $classLevelID, $applicationID);
$status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
$classLevel = $this->getClassLevel();

## Dynamic fields START ##
$dafs = new \AdmissionSystem\DynamicAdmissionFormSystem($this->schoolYearID);
$groups = $dafs->getLastActiveAcademicYearGroups();

$fields = array();
$data = array();
foreach ($groups as $index => $group) {
	$_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
	foreach ($_fields as $field) {
	    if (($field['Type'] != \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) ||
	        ($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && $field['ExtraArr']['field'] == 'sibling')) {
			$fields[$group['GroupID']][] = $field;
		}
	}
	$data[$group['GroupID']] = $dafs->getApplicationDataByGroupId($group['GroupID'], $applicationID);
}
## Dynamic fields END ##
#### Get data END ####

#### Pack data START ####
$row = array();

$row[] = substr($otherInfo['DateInput'], 0, -9);
$row[] = $otherInfo['applicationID'];
$row[] = $classLevel[$otherInfo['classLevelID']];

foreach ($fields as $groupId => $d1) {
	foreach ($d1 as $field) {
	    
		$dbTableName = $field['OtherAttributeArr']['DbTableName'];
		$_dbFieldName = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
		$dbFieldName = $_dbFieldName[0];
		$value = '';
		if (!isset($data[$groupId][$dbTableName][$dbFieldName])) {
			$sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
			$sqlWhereField = $sqlWhere[0];
			$sqlWhereValue = trim($sqlWhere[1], " '");

			foreach ((array)$data[$groupId][$dbTableName] as $v) {
				if (isset($v[$sqlWhereField]) && $v[$sqlWhereField] == $sqlWhereValue) {
					$value = $v[$dbFieldName];
					break;
				}
			}
		} else {
			$value = $data[$groupId][$dbTableName][$dbFieldName];
		}

		switch ($field['Type']) {
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR:
				$value = "1){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType1']]}";
				if ($data[$groupId][$dbTableName]['ApplyDayType2']) {
					$value .= "\n2){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType2']]}";
				} else {
					$value .= "\n2) --";
				}
				if ($data[$groupId][$dbTableName]['ApplyDayType3']) {
					$value .= "\n3){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType3']]}";
				} else {
					$value .= "\n3) --";
				}
				break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_RADIO:
			    foreach ($field['ExtraArr']['options'] as $optionInfo) {
			        if ($value == $optionInfo[0]) {
			            $value = Get_Lang_Selection($optionInfo[1], $optionInfo[2]);
			            break;
			        }
			    }
			    break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_SELECT:
			    if($field['ExtraArr']['group'] == 'address') {
			        $dbFieldName = $field['OtherAttributeArr']['DbFieldName'];
			        if(strpos($dbFieldName, 'AddressDistrict') !== false) {
			            $value = $kis_lang['Admission']['JOYFUL']['District'][$StudentInfo['AddressDistrict']];
			        }
			        if(strpos($dbFieldName, 'Address ') !== false) {
			            $value = $kis_lang['Admission']['JOYFUL']['Region'][$StudentInfo['Address']];
			        }
			    }
			    if($field['ExtraArr']['group'] == 'm_address') {
			        $sqlWhere = $field['ExtraArr']['sqlWhere'];
			        if(strpos($sqlWhere, 'MailAddr_District') !== false) {
			            $value = current($allCustInfo['MailAddr_District'])['Value'];
			            $value = $kis_lang['Admission']['JOYFUL']['District'][$value];
			        }
			        if(strpos($sqlWhere, 'MailAddr_Region') !== false) {
			            $value = current($allCustInfo['MailAddr_Region'])['Value'];
			            $value = $kis_lang['Admission']['JOYFUL']['Region'][$value];
			        }
			    }
			    if(strpos($field['OtherAttributeArr']['DbFieldName'], 'MaritalStatus') !== false) {
			        $value = $kis_lang['Admission']['JOYFUL']['MaritalStatus'][$value];
			    }
			    break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_YES_NO_EXTRA:
			    if($field['ExtraArr']['field'] == 'previouslyAttended') {
			        $prevAttend = current($allCustInfo['PreviouslyAttended'])['Value'];
			        $isAttend = substr($prevAttend, 0, 2) == 'Y;';
			        $prevAttendSplits = explode(';', $prevAttend);
			        $row[] = $isAttend ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
			        if($isAttend) {
			            for($i=1; $i<3; $i++)
			                $row[] = $i < count($prevAttendSplits) ? $prevAttendSplits[$i] : '';
			        } else {
			            $j=2;
			            while($j-- > 0) $row[] = "";
			        }
			    }
			    if($field['ExtraArr']['field'] == 'physicalConcern') {
			        $physicalConcern = current($allCustInfo['PhysicalConcern'])['Value'];
			        $isConcern = substr($physicalConcern, 0, 2) == 'Y;';
			        $value = $isConcern ? substr($physicalConcern, 2) : $kis_lang['Admission']['no'];
			    }
			    if($field['ExtraArr']['field'] == 'anyAllergies') {
			        $anyAllergies = current($allCustInfo['AnyAllergies'])['Value'];
			        $isAllergies = substr($anyAllergies, 0, 2) == 'Y;';
			        $value = $isAllergies ? substr($anyAllergies, 2) : $kis_lang['Admission']['no'];
			    }
			    if($field['ExtraArr']['field'] == 'takeMedication') {
			        $takeMedication = current($allCustInfo['TakeMedication'])['Value'];
			        $isMedication = substr($takeMedication, 0, 2) == 'Y;';
			        $value = $isMedication ? substr($takeMedication, 2) : $kis_lang['Admission']['no'];
			    }
			    if($field['ExtraArr']['field'] == 'specialNeeds') {
			        $specialNeeds = current($allCustInfo['SpecialNeeds'])['Value'];
			        $isNeeds = substr($specialNeeds, 0, 2) == 'Y;';
			        $value = $isNeeds ? substr($specialNeeds, 2) : $kis_lang['Admission']['no'];
			    }
			    if($field['ExtraArr']['field'] == 'schoolBus') {
			        $schoolBus = current($allCustInfo['SchoolBus'])['Value'];
			        $isBus = substr($specialNeeds, 0, 2) == 'Y;';
			        $value = $isBus ? substr($schoolBus, 2) : $kis_lang['Admission']['no'];
			    }
			    break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DATE:
			    if($field['ExtraArr']['sqlWhere'] == "Code='Visa'") {
			        if($StudentInfo['BirthCertType'] != $admission_cfg['BirthCertType']['passport'])
			            $value = '';
			    }
			    break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_ROW:
			    if($field['ExtraArr']['fieldType'] == 'previouslySchoolExperience') {
			        $row[] = count($preSchoolInfo) > 0 ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
			        for($i=0; $i<3; $i++) {
			            if($i < count($preSchoolInfo)) {
			                $preSchool = $preSchoolInfo[$i];
			                $row[] = $preSchool['NameOfSchool'];
			                $row[] = $preSchool['SchoolAddress'];
			                $row[] = $preSchool['Year'];
			                $sYear = substr($preSchool['StartDate'], 0,4);
			                $sMonth = substr($preSchool['StartDate'], 5,2);
			                $eYear = substr($preSchool['EndDate'], 0,4);
			                $eMonth = substr($preSchool['EndDate'], 5,2);
			                $row[] = "$sMonth/$sYear - $eMonth/$eYear";
			            } else {
			                $j=4;
			                while($j-- > 0) $row[] = "";
			            }
			        }
			    }
			    break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_FIELD:
			    if($field['ExtraArr']['field'] == 'campus') {
			        $campus = $allCustInfo['Campus'];
			        for($i=0,$j=1; $i<3; $i++,$j++) {
			            $value .= ($i < count($campus) && $campus[$i]['Value']) ? "$j) {$kis_lang['Admission']['JOYFUL']['SchoolType'][$campus[$i]['Value']]}\n" : "$j) --\n";
			        }
			        
			    }
			    break;
			case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY:
			    if($field['ExtraArr']['field'] == 'sibling') {
			        $siblingPos = current($allCustInfo['SiblingPosition'])['Value'];
			        $siblingNum = current($allCustInfo['NumOfSibling'])['Value'];
			        $replaces = ['{total}' => $siblingNum, '{pos}' => $siblingPos];
			        $row[] = str_replace(array_keys($replaces), $replaces, $kis_lang['Admission']['JOYFUL']['Of']);
			        for($i=0; $i<5; $i++) {
			            if($i < count($siblingInfo)) {
			                $s = $siblingInfo[$i];
			                $row[] = $s['EnglishName'];
			                $row[] = $s['Age'];
			                $row[] = $s['BirthCertNo'];
			                $row[] = $s['CurrentSchool'];
			                $row[] = $s['Year'];
			                $row[] = $s['GraduateSameSchool'] != '' ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
			                $row[] = $s['GraduateSameSchool'];
			                $row[] = $s['ApplyingSameSchool'] == 'Y' ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
			            }
			        }
			    }
			    break;
		}
		if($field['ExtraArr']['field'] == 'previouslyAttended') continue;
		if($field['ExtraArr']['fieldType'] == 'previouslySchoolExperience') continue;
		if($field['ExtraArr']['field'] == 'sibling') continue;
		$row[] = $value;
	}
}

// ######## Get interview START ########
$interviewInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
$rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '');
$interviewSettings = BuildMultiKeyAssoc($rs, 'RecordID');

$interview1 = $interviewSettings[$interviewInfo['InterviewSettingID']];
$interview2 = $interviewSettings[$interviewInfo['InterviewSettingID2']];
$interview3 = $interviewSettings[$interviewInfo['InterviewSettingID3']];

$interviewDateTime1 = $interviewDateTime2 = $interviewDateTime3 = '';
if($interview1) {
	$interviewDateTime1 = $interview1['Date'];
	$interviewDateTime1 .= ' ' . substr( $interview1['StartTime'], 0, 5 );
	$interviewDateTime1 .= ' - ' . substr( $interview1['EndTime'], 0, 5 );
}
if($interview2) {
	$interviewDateTime2 = $interview2['Date'];
	$interviewDateTime2 .= ' ' . substr( $interview2['StartTime'], 0, 5 );
	$interviewDateTime2 .= ' - ' . substr( $interview2['EndTime'], 0, 5 );
}
if($interview3) {
	$interviewDateTime3 = $interview3['Date'];
	$interviewDateTime3 .= ' ' . substr( $interview3['StartTime'], 0, 5 );
	$interviewDateTime3 .= ' - ' . substr( $interview3['EndTime'], 0, 5 );
}
// ######## Get interview END ########

## Interview follow up START ##
$interviewStatusArr = array_flip($admission_cfg['InterviewStatus']);
$interviewStatus = $allCustInfo['InterviewStatus'][0]['Value'];
$interviewStatus = ($interviewStatus) ? $interviewStatus : $admission_cfg['InterviewStatus']['NotUpdated'];
$interviewStatus = $interviewStatusArr[$interviewStatus];
$row[] = $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$interviewStatus];

if ($allCustInfo['RegisterEmail'][0]['Value']) {
	$row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['RegisterEmailDate'][0]['Value']})";
} else {
	$row[] = $kis_lang['Admission']['no'];
}

$admitStatusArr = array_flip($admission_cfg['AdmitStatus']);
$admitStatus = $allCustInfo['AdmitStatus'][0]['Value'];
$admitStatus = ($admitStatus) ? $admitStatus : $admission_cfg['AdmitStatus']['NotUpdated'];
$admitStatus = $admitStatusArr[$admitStatus];
$row[] = $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$admitStatus];

$row[] = $kis_lang['Admission']['TimeSlot'][$allCustInfo['RatingClass'][0]['Value']];
$row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidReservedFee'][0]['Value']];
$row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidRegistrationCertificate'][0]['Value']];

if ($allCustInfo['BookFeeEmail'][0]['Value']) {
	$row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['BookFeeEmailDate'][0]['Value']})";
} else {
	$row[] = $kis_lang['Admission']['no'];
}

$row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidBookFee'][0]['Value']];

if ($allCustInfo['OpeningNoticeEmail'][0]['Value']) {
	$row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['OpeningNoticeEmailDate'][0]['Value']})";
} else {
	$row[] = $kis_lang['Admission']['no'];
}

$row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidTuitionFee'][0]['Value']];
$row[] = ($allCustInfo['GiveUpOffer'][0]['Value']) ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
## Interview follow up END ##

## Remarks START ##
$row[] = $kis_lang['Admission']['Status'][$status['status']];
$row[] = $kis_lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
$row[] = $status['receiptID'];
$row[] = $status['receiptdate'];
$row[] = $status['handler'];
//        $row[] = $status['interviewdate'];
//        $row[] = $status['interviewlocation'];
$row[] = $interviewDateTime1;
$row[] = $interviewDateTime2;
$row[] = $interviewDateTime3;
$row[] = $kis_lang['Admission'][ $status['isnotified'] ];
$row[] = $status['remark'];
## Remarks END ##
#### Pack data END ####

return $row;
