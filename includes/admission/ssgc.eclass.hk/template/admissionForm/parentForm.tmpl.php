<h1>
<?=$Lang['Admission']['PGInfo']?>
Parent Information
</h1>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
    <tr>
    	<td>
    		<span>If no information, fill in '<font style="color:red;">N.A.</font>'</span>
    		<br/>
    		<span>如沒有資料，請輸入 '<font style="color:red;">N.A.</font>'</span>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head">
		<center>
   			<?=$star ?>
   			
    		<?php if($IsConfirm){ ?>
    			<?php
    			if($formData['parentType'] == 'F'){
    			    echo "{$Lang['Admission']['HKUGAPS']['father']} Father";
    			}else{
    			    echo "{$Lang['Admission']['SSGC']['guardian']} Guardian";
    			}
    			?>
    		<?php }else{ ?>
        		<select name="parentType">
        			<option value="F" <?= ($parentType == 'F')? 'selected' : '' ?>><?=$Lang['Admission']['HKUGAPS']['father'] ?> Father</option>
        			<option value="G" <?= ($parentType == 'G')? 'selected' : '' ?>><?=$Lang['Admission']['SSGC']['guardian'] ?> Guardian</option>
        		</select>
    		<?php } ?>
		</center>
		</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['mother'] ?> Mother</center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['chinesename']?> 
    		Name in Chinese
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1ChineseName'];
    		}else{?>
    			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$parentInfoArr[$parentType]['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2ChineseName'];
    		}else{?>
    			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$parentInfoArr['M']['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['englishname']?>
    		Name in English
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1EnglishName'];
    		}else{?>
    			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$parentInfoArr[$parentType]['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2EnglishName'];
    		}else{?>
    			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$parentInfoArr['M']['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['occupation']?>
			Occupation
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Occupation'];
    		}else{?>
    			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$parentInfoArr[$parentType]['JobTitle'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Occupation'];
    		}else{?>
    			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$parentInfoArr['M']['JobTitle'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['SSGC']['organization']?>
			Organization
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo nl2br($formData['G1Company']);
    		}else{?>
    			<input name="G1Company" type="text" id="G1Company" class="textboxtext" value="<?=$parentInfoArr[$parentType]['Company'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo nl2br($formData['G2Company']);
    		}else{?>
    			<input name="G2Company" type="text" id="G2Company" class="textboxtext" value="<?=$parentInfoArr['M']['Company'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['SHCK']['ContactNo']?>
			Contact No.
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1MobileNo'];
    		}else{?>
    			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$parentInfoArr[$parentType]['Mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2MobileNo'];
    		}else{?>
    			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$parentInfoArr['M']['Mobile'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['email']?>
    		Email
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Email'];
    		}else{?>
    			<input name="G1Email" type="text" id="G1Email" class="textboxtext" value="<?=$parentInfoArr[$parentType]['Email'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Email'];
    		}else{?>
    			<input name="G2Email" type="text" id="G2Email" class="textboxtext" value="<?=$parentInfoArr['M']['Email'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
</table>
