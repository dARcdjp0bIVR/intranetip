<?php
function getPointHTML($point){
    global $Lang;
    return <<<HTML
        <label>({$point} {$Lang['Admission']['SSGC']['points']})</label>
        <br />
        <label>({$point} points)</label>
HTML;
}
?>
<style>
.otherQuestionRow label.disabled{
     color: #aaaaaa;
}
.partBDetails{
    margin-top: 5px;
}
.partBDetails label{
    margin-right: 5px;
    display: inline-block;
}
.partBDetails input[type="text"]{
     width: 300px;
}
</style>
<h1>
<?=$Lang['Admission']['otherInfo']?>
Other Information
</h1>

<table id="dataTable1" class="form_table" style="font-size: 13px">
<tr>
	<td rowspan="4" style="width: 30%;"><?=$Lang['Admission']['SSGC']['sibilingInfo'] ?><br/>Sibiling's Data</td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['name'] ?> Name </center></td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['Age'] ?> Age</center></td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['gender'] ?> Gender</center></td>
	<td class="form_guardian_head"><center><?=$Lang['Admission']['SSGC']['schoolClass'] ?> School & class</center></td>
</tr>

<?php for($i=0;$i<3;$i++){ ?>
    <tr>
    	<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['SibilingName'.($i+1)];
    		}else{?>
    			<input name="SibilingName<?=$i+1 ?>" type="text" id="SibilingName<?=$i+1 ?>" class="textboxtext" value="<?=$allCustInfo['SibilingName'][$i]['Value'] ?>"/>
    		<?php } ?>
    	</td>
    	<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['SibilingAge'.($i+1)];
    		}else{?>
    			<input name="SibilingAge<?=$i+1 ?>" type="text" id="SibilingAge<?=$i+1 ?>" class="textboxtext" value="<?=$allCustInfo['SibilingAge'][$i]['Value'] ?>"/>
    		<?php } ?>
    	</td>
    	<td class="form_guardian_field">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['SibilingGender'.($i+1)])? ($Lang['Admission']['genderType'][$formData['SibilingGender'.($i+1)]].' '.$formData['SibilingGender'.($i+1)]) : ' -- ' ?>
    		<?php }else{ ?>
        		<div style="text-align: center;min-width: 130px;">
        			<input type="radio" value="M" id="SibilingGenderM<?=$i+1 ?>" name="SibilingGender<?=$i+1 ?>" <?=$allCustInfo['SibilingGender'][$i]['Value'] == "M"?'checked':'' ?>>
        			<label for="SibilingGenderM<?=$i+1 ?>"><?=$Lang['Admission']['genderType']['M']?> M</label>
        			<input type="radio" value="F" id="SibilingGenderF<?=$i+1 ?>" name="SibilingGender<?=$i+1 ?>" <?=$allCustInfo['SibilingGender'][$i]['Value'] == "F"?'checked':'' ?>>
        			<label for="SibilingGenderF<?=$i+1 ?>"><?=$Lang['Admission']['genderType']['F']?> F</label>
    			</div>
    		<?php } ?>
    	</td>
    	<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['SibilingSchoolClass'.($i+1)];
    		}else{?>
    			<input name="SibilingSchoolClass<?=$i+1 ?>" type="text" id="SibilingSchoolClass<?=$i+1 ?>" class="textboxtext" value="<?=$allCustInfo['SibilingSchoolClass'][$i]['Value'] ?>"/>
    		<?php } ?>
    	</td>
    </tr>
<?php } ?>

    <tr>
    	<td>
    		<?=$Lang['Admission']['HasTwinsApply'] ?> 
			Has twins apply
    	</td>
    	<td colspan="3">
    		<?php 
    		if($IsConfirm){
		        if($formData['HasTwinsApply'] == '0'){
		            echo "{$Lang['Admission']['no']} No";
		        }else{
		            echo "{$Lang['Admission']['yes']} Yes <br />( {$Lang['Admission']['name']} Name: {$formData['TwinName']} )";
		        }
    		}else{ 
    		?>
    			<input type="radio" value="0" id="HasTwinsApplyN" name="HasTwinsApply" <?=($applicationStudentInfo['IsTwinsApplied'] == 0)?'checked':'' ?> />
    			<label for="HasTwinsApplyN"><?=$Lang['Admission']['no']?> No</label>
    			<br />
    			<input type="radio" value="1" id="HasTwinsApplyY" name="HasTwinsApply" <?=($applicationStudentInfo['IsTwinsApplied'] == 1)?'checked':'' ?> />
    			<label for="HasTwinsApplyY"><?=$Lang['Admission']['yes']?> Yes</label>
    			(
    				<?=$Lang['Admission']['name'] ?> Name:
    				<input name="TwinName" type="text" id="TwinName" class="textboxtext" value="<?=$allCustInfo['TwinName'][0]['Value'] ?>" style="width: 200px;" />
    			)
    		<?php 
    		}
    		?>
    	</td>
    </tr>
</table>



<table class="form_table otherInformation" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:100px">
        <col style="">
    </colgroup>

<tbody>
    <tr>
    	<td>
    		<?=$Lang['Admission']['SSGC']['partB'] ?> Part B
    	</td>
    	<td colspan="3">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['PartBNotApplicable'])? "{$Lang['Admission']['SSGC']['notApplicable']} Not applicable":""?>
    		<?php }else{ ?>
    			<input type="checkbox" value="1" id="partBNotApplicable" name="PartBNotApplicable" <?=(isset($allCustInfo['PartBNotApplicable'][0]['Value']) && ($allCustInfo['PartBNotApplicable'][0]['Value'] == 1))?'checked':'' ?> />
    			<label for="partBNotApplicable"><?=$Lang['Admission']['SSGC']['notApplicable']?> Not applicable</label>
    		<?php } ?>
    	</td>
    </tr>
<?php if(!$IsConfirm || !$formData['PartBNotApplicable']): ?>
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['parentIsStaff']?>
    		<br />
    		Parent(s) teaching or working full time in our school
    	</td>
    	<td class="field_title" style="width: auto;">
			<?=getPointHTML(20) ?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['ParentIsStaff'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    			<?php if(($formData['ParentIsStaff'])){
    			    echo "<br />{$Lang['Admission']['SSGC']['parentName']} Name of parent: {$formData['ParentIsStaff_Name']}";
    			}?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="ParentIsStaffY" name="ParentIsStaff" <?=($allCustInfo['ParentIsStaff'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="ParentIsStaffY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="ParentIsStaffN" name="ParentIsStaff" <?=($allCustInfo['ParentIsStaff'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="ParentIsStaffN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
    				<label for="ParentIsStaff_Name">
       					<?=$star?>
    					<?=$Lang['Admission']['SSGC']['parentName'] ?>
    					Name of parent
    				</label>
    				
        			<input name="ParentIsStaff_Name" type="text" id="ParentIsStaff_Name" value="<?=$allCustInfo['ParentIsStaff_Name'][0]['Value'] ?>" />
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['sisterIsSameSchool']?>
    		<br />
    		Sister(s) studying in secondary/primary school
    	</td>
    	<td class="field_title" style="width: auto;">
			<?=getPointHTML(20) ?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['SisterIsSameSchool'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    			<?php if(($formData['SisterIsSameSchool'])){
    			    echo "<br />{$Lang['Admission']['SSGC']['classAttending']} Class attending: {$formData['SisterIsSameSchool_NameClass']}";
    			}?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="SisterIsSameSchoolY" name="SisterIsSameSchool" <?=($allCustInfo['SisterIsSameSchool'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="SisterIsSameSchoolY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="SisterIsSameSchoolN" name="SisterIsSameSchool" <?=($allCustInfo['SisterIsSameSchool'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="SisterIsSameSchoolN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
    				<label for="SisterIsSameSchool_NameClass">
       					<?=$star?>
    					<?=$Lang['Admission']['SSGC']['classAttending'] ?>
    					Class attending
    				</label>
    				
        			<input name="SisterIsSameSchool_NameClass" type="text" id="SisterIsSameSchool_NameClass" value="<?=$allCustInfo['SisterIsSameSchool_NameClass'][0]['Value'] ?>" />
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['parentIsManager']?>
    		<br />
    		Parent(s) being a registered school manager of our school
    	</td>
    	<td class="field_title" style="width: auto;">
			<?=getPointHTML(20) ?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['ParentIsManager'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    			<?php if(($formData['ParentIsManager'])){
    			    echo "<br />{$Lang['Admission']['SSGC']['parentName']} Name of parent: {$formData['ParentIsManager_Name']}";
    			}?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="ParentIsManagerY" name="ParentIsManager" <?=($allCustInfo['ParentIsManager'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="ParentIsManagerY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="ParentIsManagerN" name="ParentIsManager" <?=($allCustInfo['ParentIsManager'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="ParentIsManagerN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
    				<label for="ParentIsManager_Name">
       					<?=$star?>
    					<?=$Lang['Admission']['SSGC']['parentName'] ?>
    					Name of parent
    				</label>
    				
        			<input name="ParentIsManager_Name" type="text" id="ParentIsManager_Name" value="<?=$allCustInfo['ParentIsManager_Name'][0]['Value'] ?>" />
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['motherSisterPrimaryGraduate']?>
    		<br />
    		Mother or sister(s) being a graduate of our primary school
    	</td>
    	<td class="field_title" style="width: auto;">
			<?=getPointHTML(10) ?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['MotherSisterPrimaryGraduate'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    			<?php if(($formData['MotherSisterPrimaryGraduate'])){
    			    echo "<br />{$Lang['Admission']['SSGC']['nameGraduation']} Name and year of graduation: {$formData['MotherSisterPrimaryGraduate_NameYearOfGraduation']}";
    			}?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="MotherSisterPrimaryGraduateY" name="MotherSisterPrimaryGraduate" <?=($allCustInfo['MotherSisterPrimaryGraduate'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="MotherSisterPrimaryGraduateY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="MotherSisterPrimaryGraduateN" name="MotherSisterPrimaryGraduate" <?=($allCustInfo['MotherSisterPrimaryGraduate'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="MotherSisterPrimaryGraduateN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
    				<label for="MotherSisterPrimaryGraduate_NameYearOfGraduation">
       					<?=$star?>
    					<?=$Lang['Admission']['SSGC']['nameGraduation'] ?>
    					Name and year of graduation
    				</label>
    				
        			<input name="MotherSisterPrimaryGraduate_NameYearOfGraduation" type="text" id="MotherSisterPrimaryGraduate_NameYearOfGraduation" value="<?=$allCustInfo['MotherSisterPrimaryGraduate_NameYearOfGraduation'][0]['Value'] ?>" />
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['sameReligious']?>
    		<br />
    		Same religious affiliation as the sponsoring body of our school
    	</td>
    	<td class="field_title" style="width: auto;">
			<?=getPointHTML(5) ?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['SameReligious'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="SameReligiousY" name="SameReligious" <?=($allCustInfo['SameReligious'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="SameReligiousY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="SameReligiousN" name="SameReligious" <?=($allCustInfo['SameReligious'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="SameReligiousN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
    				<label>
       					<?=$star?>
    					<?=$Lang['Admission']['SSGC']['enclosingBaptisingDocument'] ?>
    					Enclosing copy of baptising document
    				</label>
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['firstBorn']?>
    		<br />
    		First-born child
    	</td>
    	<td class="field_title" style="width: auto;">
			<?=getPointHTML(5) ?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['FirstBorn'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
        		<div class="">
        			<input type="radio" value="1" id="FirstBornY" name="FirstBorn" <?=($allCustInfo['FirstBorn'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="FirstBornY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="FirstBornN" name="FirstBorn" <?=($allCustInfo['FirstBorn'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="FirstBornN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['motherSecondGraduate']?>
    		<br />
    		Mother being a graduate of our secondary school
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['MotherSecondGraduate'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    			<?php if(($formData['MotherSecondGraduate'])){
    			    echo "<br />{$Lang['Admission']['SSGC']['nameGraduation']} Name and year of graduation: {$formData['MotherSecondGraduate_NameYearOfGraduation']}";
    			}?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="MotherSecondGraduateY" name="MotherSecondGraduate" <?=($allCustInfo['MotherSecondGraduate'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="MotherSecondGraduateY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="MotherSecondGraduateN" name="MotherSecondGraduate" <?=($allCustInfo['MotherSecondGraduate'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="MotherSecondGraduateN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
    				<label for="MotherSecondGraduate_NameYearOfGraduation">
       					<?=$star?>
    					<?=$Lang['Admission']['SSGC']['nameGraduation'] ?>
    					Name and year of graduation
    				</label>
    				
        			<input name="MotherSecondGraduate_NameYearOfGraduation" type="text" id="MotherSecondGraduate_NameYearOfGraduation" value="<?=$allCustInfo['MotherSecondGraduate_NameYearOfGraduation'][0]['Value'] ?>" />
    			</div>
    		<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$Lang['Admission']['SSGC']['siblingSameSchoolGraduate']?>
    		<br />
    		Sibling(s) being a studying in / graduate of our kindergarten
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['SiblingSameSchoolGraduate'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    			
    			<?php 
    			if(($formData['SiblingSameSchoolGraduate'])){
    			    echo "<br />";
    			    if($formData['SiblingSameSchoolGraduate_Type'] == '0'){
    			        echo "{$Lang['Admission']['SSGC']['classAttending']} Class attending: {$formData['SiblingSameSchoolGraduate_NameClass']}";
    			    }else{
    			        echo "{$Lang['Admission']['SSGC']['nameGraduation']} Name and year of graduation: {$formData['SiblingSameSchoolGraduate_NameYearOfGraduation']}";
    			    }
    			}
    			?>
    		<?php }else{ ?>
        		<div class="partBQuestion">
        			<input type="radio" value="1" id="SiblingSameSchoolGraduateY" name="SiblingSameSchoolGraduate" <?=($allCustInfo['SiblingSameSchoolGraduate'][0]['Value'] == 1)?'checked':'' ?> />
        			<label for="SiblingSameSchoolGraduateY"> <?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
        			<input type="radio" value="0" id="SiblingSameSchoolGraduateN" name="SiblingSameSchoolGraduate" <?=($allCustInfo['SiblingSameSchoolGraduate'][0]['Value'] == 0)?'checked':'' ?> />
        			<label for="SiblingSameSchoolGraduateN"> <?=$Lang['Admission']['no']?> No</label>
    			</div>
    			<div class="partBDetails">
        			<div style="margin-top: 5px;">
        				<div style="display:inline-block">
                			<input type="radio" value="0" id="SiblingSameSchoolGraduate_TypeStuding" name="SiblingSameSchoolGraduate_Type" <?=(!$allCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value'])?'checked':'' ?> />
                			<label for="SiblingSameSchoolGraduate_TypeStuding"><?=$Lang['Admission']['SSGC']['currentSchoolStudying'] ?> Studying</label>
            			</div>
            			
            			<div id="SiblingSameSchoolGraduate_NameClassDiv" style="display:inline-block">
            				<label for="SiblingSameSchoolGraduate_NameClass" style="">
       							<?=$star?>
            					<?=$Lang['Admission']['SSGC']['classAttending'] ?>
            					Class attending
            				</label>
                			<input name="SiblingSameSchoolGraduate_NameClass" type="text" id="SiblingSameSchoolGraduate_NameClass" value="<?=$allCustInfo['SiblingSameSchoolGraduate_NameClass'][0]['Value'] ?>" />
                		</div>
					</div>
					<div style="margin-top: 5px;">
    					<div style="display:inline-block">
                			<input type="radio" value="1" id="SiblingSameSchoolGraduate_TypeGraduate" name="SiblingSameSchoolGraduate_Type" <?=($allCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value'])?'checked':'' ?> />
                			<label for="SiblingSameSchoolGraduate_TypeGraduate"><?=$Lang['Admission']['SSGC']['currentSchoolGraduate'] ?> Graduate</label>
            			</div>
            			<div id="SiblingSameSchoolGraduate_NameYearOfGraduationDiv" style="display:inline-block">
            				<label for="SiblingSameSchoolGraduate_NameYearOfGraduation" style="">
       							<?=$star?>
            					<?=$Lang['Admission']['SSGC']['nameGraduation'] ?>
            					Name and year of graduation
            				</label>
                			<input name="SiblingSameSchoolGraduate_NameYearOfGraduation" type="text" id="SiblingSameSchoolGraduate_NameYearOfGraduation" value="<?=$allCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value'] ?>" />
                		</div>
            		</div>
    			</div>
    		<?php } ?>
    	</td>
    </tr>
<?php endif; ?>
    
    
    
</tbody>

</table>

<script>
$(function(){
	'use strict';
	
	function updateUI(){
		var isNotApplicable = !!$('#partBNotApplicable:checked').length;
		$('.otherQuestionRow input, .otherQuestionRow select').prop('disabled', isNotApplicable);
		if(isNotApplicable){
			$('.otherQuestionRow label').addClass('disabled');
		}else{
			$('.otherQuestionRow label').removeClass('disabled');
		}

		$('.partBQuestion input:checked').each(function(index, element){
			var value = $(this).val();
			var $td = $(this).closest('td');
			var $detailsTd = $td.find('.partBDetails');
			if(value == '1'){
				$detailsTd.show();
				$detailsTd.find('input,select').prop('disabled', isNotApplicable);
			}else{
				$detailsTd.hide();
				$detailsTd.find('input,select').prop('disabled', true);
			}
		});

		if($('[name="SiblingSameSchoolGraduate_Type"]:checked').val() == '1'){
			$('#SiblingSameSchoolGraduate_NameClassDiv').hide().find('input,select').prop('disabled', true);
			$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').show().find('input,select').prop('disabled', isNotApplicable);
		}else{
			$('#SiblingSameSchoolGraduate_NameClassDiv').show().find('input,select').prop('disabled', isNotApplicable);
			$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').hide().find('input,select').prop('disabled', true);
		}
	}
	$('#partBNotApplicable, .partBQuestion input').click(updateUI);
	$('[name="SiblingSameSchoolGraduate_Type"]').change(updateUI);
	updateUI();

	$('#form1').submit(function(){
		if($('#HasTwinsApplyN').prop('checked')){
			$('#TwinName').val('');
		}
	});
});
</script>