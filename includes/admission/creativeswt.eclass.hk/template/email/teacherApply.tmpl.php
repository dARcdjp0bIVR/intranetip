<div style="max-width: 800px;">
    <p>請物回覆此電郵 Please do not reply to this email</p>

    <p style="text-align: center;"><?= $yearStart ?>/<?= $yearEnd ?> 年度</p>
    <p>新生入學申請 - 確認通知 (申請編號: <?= $ApplicationID ?>)</p>

    <p style="margin-top: 2rem;">
        <?= $stuedentInfo[0]['student_name_b5'] ?>家長：
    </p>

    <p style="margin-top: 1rem;">
        貴子第之入學申請表格，經已收妥，現請閣下記錄申請編號 <?= $ApplicationID ?>，以便日後查核有關申請。
        由於　貴子弟之入學申請表格於截止日期後才收到，表格會列為後備申請表，
        後備學位將會待有學位空缺時，再另行安排會見。
        如有查詢，請電本園電話：<?= $admission_cfg['SchoolPhone'] ?>。
    </p>

    <p style="margin-top: 2rem;">
        <?= $LastContent ?>
    </p>

    <p style="margin-top: 2rem;">
        啟思幼稚園幼兒園<br/>
        <?= $admission_cfg['SchoolAddress'] ?>
    </p>
</div>