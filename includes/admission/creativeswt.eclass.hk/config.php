<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(深灣軒)';
$admission_cfg['SchoolName']['en'] .= ' (Sham Wan Towers)';
$admission_cfg['SchoolCode'] = 'SWT';
$admission_cfg['SchoolPhone'] = '2873 2128';
$admission_cfg['SchoolAddress']['b5'] = '香港鴨脷洲鴨脷洲徑3號地下';
$admission_cfg['SchoolAddress']['en'] = 'G/F, Sham Wan Towers, 3 Ap Lei Chau Drive, Hong Kong';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckswt.photo@gmail.com';
}
/* for email [end] */


/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = 'X84ZAQ7T5LASQ';
} else {
    $admission_cfg['paypal_signature'] = 'WLdVyinuu9HpscjTdM_0ZITLKnUUYe24WmBlp7R4MCdO4L5xaUmv10_WKWG';
    $admission_cfg['hosted_button_id'] = 'RZPN4ACBSWPDQ';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Sham Wan Towers)';
}
/* for paypal [End] */