<?php
# modifying by: 

/********************
 * Log :
 * Date		2015-06-30 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step, $ApplicationID=''){
		global $Lang, $sys_custom,$lac, $validForAdmission, $kis_lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$active_step8 ="";
		$active_step9 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		$href_step8 ="";
		$href_step9 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="active-step";
		        break;
		    case 8:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="completed-step";
		        $active_step8 ="active-step";
		        break;
		    case 9:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="completed-step";
		        $active_step8 ="completed-step";
		        $active_step9 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token'] && $Step!=7){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'</center></h2>';
		}
		
		if($lac->isInternalUse($_REQUEST['token']) && ($Step!=7 || $lac->getTokenByApplicationNumber($ApplicationID)=='')){
			$x .='<h2 style="font-size:18px;color:red"><center>Internal Use<br/>'.$kis_lang['remarks'] .'</center></h2>';
		}
				
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>Instruction '.$Lang['Admission']['instruction'].'</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>Personal Info '.$Lang['Admission']['personalInfo'].'</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>Docs Upload '.$Lang['Admission']['docsUpload'].'</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>Confirmation '.$Lang['Admission']['confirmation'].'</a></div>
					<div class="'.$active_step7.'"><a><span>5</span>Submission '.$Lang['Admission']['submission'].'</a></div>
					<div class="'.$active_step8.'"><a><span>6</span>Payment '.$Lang['Admission']['payment'].'</a></div>
					<div class="'.$active_step9.' last_step"><span style="width:90px">7 Finish '.$Lang['Admission']['finish'].'</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = 'Thank you for using the online application system!<br />'.$Lang['Admission']['msg']['defaultfirstpagemessage']; //Henry 20131107
		}
		
		if($lac->isInternalUse($_REQUEST['token'])){
			$internalUseNote ='<h2 style="font-size:18px;color:red"><center>Internal Use<br/>'.$kis_lang['remarks'] .'</center></h2>';
		}
		
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token']){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$internalUseNote.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">Online Application '.$Lang['Admission']['onlineApplication'].'</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm($ClassLevel);
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac, $sys_custom;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage']; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';

		$RMKG_cust = '<br /><table style="font-size:small;">
						<tr>
							<td width="5">&nbsp;</td>
							<td align="center" width="1%" rowspan="2"><input type="checkbox" name="Agree" id="Agree" value="1" style="transform: scale(2);  -webkit-transform: scale(2);"/></td>
							<td width="5">&nbsp;</td>
							<td><label for="Agree">I agree the terms and conditions as stated above.</label></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><label for="Agree">本人同意上述有關條款及細則。</label></td>
						</tr>
					</table>';

         $x .= $Instruction.$RMKG_cust;        
              		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN('Back '.$Lang['Btn']['Back'], "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN('Begin Application 開始填寫表格', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->';								
							if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.'Cancel '.$Lang['Btn']['Cancel'].'" />';
							}
							else{
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.'Cancel '.$Lang['Btn']['Cancel'].'" />';
							}
						$x .= '</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm($ClassLevel = ''){
		global $libkis_admission, $Lang, $lac, $sys_custom;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		/*
		//disable to choose class when using central server
		$disable = '';
		if($ClassLevel)
			$disable = 'return false';
		*/
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if($lac->isInternalUse($_GET['token']) && $value['AllowInternalUse'] && $value['SchoolYearID'] == $lac->schoolYearID || date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])){
					$hasClassLevelApply = 1;
					$selected = '';
					/*
					if($key == $ClassLevel)
						$selected = "checked='checked'";
						*/
					
					//Henry added [20140808]
					$numOFQuotaLeft = $lac->NumOfQuotaLeft($key,$value['SchoolYearID']);
					$isFullApply = false;
					if($numOFQuotaLeft <= 0){
						$isFullApply = true;
					}
					$disable = '';
					if($isFullApply){
						$selected = 'disabled';
					}
					
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="'.$disable.'" '.$selected.'  />
						<label for="status_'.$key.'">'.
    					$value['ClassLevelName'];
					if($value['DOBStart'] && $value['DOBEnd']){
    					$class_level_selection .= ' ('.$Lang['Admission']['studentbirthdayrange'].': '.$value['DOBStart'].' ~ '.$value['DOBEnd'].')';
					}
					$class_level_selection .= ($isFullApply?' <span style="color:red">('.$Lang['Admission']['IsFull'].' Full)</span>':'').
    					'<!--(Quota Left:'.$numOFQuotaLeft.')-->'.
					'</label> ';
					
					$class_level_selection .= '<br />';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025
		//$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .='<fieldset ><legend>Level '.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN('Next '.$Lang['Btn']['Next'], "button", "goto('step_index','step_instruction')", "SubmitBtn", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p><br/><span>Recommend to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a>.<br />建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
		//$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm($BirthCertNo = ""){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission, $sys_custom, $admission_cfg;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm(0,$BirthCertNo);
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='「<span class="tabletextrequire">*</span>」are mandatory but if not applicable please fill in N.A.<br/>';
		$x .='<span>'.$Lang['Admission']['munsang']['mandatoryfield'].'</span>';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN('Back '.$Lang['Btn']['Back'], "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN('Next '.$Lang['Btn']['Next'], "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->';
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.'Cancel '.$Lang['Btn']['Cancel'].'" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.'Cancel '.$Lang['Btn']['Cancel'].'" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0, $BirthCertNo = ""){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg,$kis_lang;
		$this->IsConfirm = $IsConfirm;
		
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		
		$applicationSetting = $lac->getApplicationSetting();
		$birth_cert_type_selection = '';
		$Lang['Admission']['BirthCertType']['hkBirthCert']	= "香港出生證 Hong Kong Birth Certificate";
		$Lang['Admission']['BirthCertType']['passport']	= "護照編號 Passport Number";
		
		if($IsConfirm){
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($formData['StudentBirthCertType'] == $_type){
					$birth_cert_type_selection .= ' (';
					$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
					$birth_cert_type_selection .= ') ';
					break;
				}
			}
		}
		else{
			$birth_cert_type_selection .= '<select name="StudentBirthCertType" id="StudentBirthCertType">';
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				$birth_cert_type_selection .= '<option value="'.$_type.'">';
				$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
				$birth_cert_type_selection .= '</option>';
			}
			$birth_cert_type_selection .= '</select>';
		}
		
		$program_selection1 = '<select name="StudentProgram1">';
		$program_selection1 .= '<option value="">Not Applicable 不適用</option>';
		$program_selection1 .= '<option value="1">International Stream PN-K3 國際課程 PN-K3</option>';
		$program_selection1 .= '<option value="2">Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3</option>';
		$program_selection1 .= '</select>';
		
		$program_selection2 = '<select name="StudentProgram2">';
		$program_selection2 .= '<option value="">Not Applicable 不適用</option>';
		$program_selection2 .= '<option value="1">International Stream PN-K3 國際課程 PN-K3</option>';
		$program_selection2 .= '<option value="2">Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3</option>';
		$program_selection2 .= '</select>';
		
		$program_selected1 .= $formData['StudentProgram1'] == 1?' International Stream PN-K3 國際課程 PN-K3':'';
		$program_selected1 .= $formData['StudentProgram1'] == 2?' Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':'';
		
		$program_selected2 = $formData['StudentProgram2'] == 0?' Not Applicable 不適用':'';
		$program_selected2 .= $formData['StudentProgram2'] == 1?' International Stream PN-K3 國際課程 PN-K3':'';
		$program_selected2 .= $formData['StudentProgram2'] == 2? ' Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':'';
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayType = $applicationSetting[$formData['sus_status']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		$dayTypeOption ='';
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." Option ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
		}else{
			// from /kis/admission_form/ajax_get_class_selection.php
		}
		
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
				$x = '';
// 		$x .= '<h1>'.$Lang['Admission']['CHIUCHUNKG']['Wish'].'</h1>';
// 		$x .= '<table class="form_table" style="font-size: 13px">';
// 		if($IsConfirm){
// 				$x .='<tr>
// 					<td class="field_title">'.$Lang['Admission']['RMKG']['onlineform']['LevelApplied'].'</td><td>'.$classLevel.'</td>
// 				</tr>';
// 			}
// 		$x .= '<tr>';
// 		$x .= '<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['SessionApplied'].'</td>';
// 		$x .= '<td><div id="DayTypeOption">'.$dayTypeOption.'</div></td>';
// 		$x .= '</tr>';
// 		$x .='<tr>
// 					<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['IsConsiderAlternative'].'</td>
// 					<td>';
// 					if($IsConfirm){
// 						$x .= $formData['OthersIsConsiderAlternative'] == 'Y'?$Lang['Admission']['yes'].' Yes':$Lang['Admission']['no'].' No';
// 					}
// 				$x .= $this->getFormElement("OthersIsConsiderAlternative");
// 				$x .='</td></tr>';
// 		$x .= '</table>';
		
		$x .= '<h1>'.$Lang['Admission']['RMKG']['onlineform']['StuInfo'].'</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['chinesename'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_StuChiName").'</td>
				<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['engname'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_StuEngName").'</td>
			</tr>
			<tr>	
               	<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['pob'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_POB").'</td>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['nationality'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_Nationality").'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['dob'].'</td>
				<td>
					'.$this->getFormElement("FormStudentInfo_DOB").'</td>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['sex'].'</td>
				<td>';
				$x .= $this->getFormElement("FormStudentInfo_StuGender");
				$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['BirthCertNo'].'</td>
				<td colspan="3">'.$birth_cert_type_selection.($BirthCertNo?'<input name="StudentBirthCertNo" value="'.$BirthCertNo.'" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none"/>':$this->getFormElement("StudentBirthCertNo")).'<br/>'.$Lang['Admission']['mgf']['msg']['birthcertnohints'].'<br/>(eg：A123456(7)，please enter "A1234567")</td>						
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['tel'].'</td>
				<td colspan="3">'.$this->getFormElement("FormStudentInfo_HomeTel").'</td>	
				<!--td class="field_title">Fax 傳真</td>
				<td>'.$this->getFormElement("FormStudentInfo_Fax").'</td-->		
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['address'].'</td>
				<td colspan="3">';
			if($IsConfirm){
        		$_address = $formData['HomeAddrRoom'] . $formData['HomeAddrFloor'] . $formData['HomeAddrBlock'] . $formData['HomeAddrBlding'] . $formData['HomeAddrStreet'] . $formData['HomeAddrDistrict'];
        	    if(preg_match("/[\x{4e00}-\x{9fa5}]+/u", $_address)){
			        if($formData['HomeAddrRegion'] != ' -- '){
    			        switch ($formData['HomeAddrRegion']){
    			            case 1:
    			                $x .= '香港';
    			                break;
    			            case 2:
    			                $x .= '九龍';
    			                break;
    			            case 3:
    			                $x .= '新界';
    			                break;
    			        }
			        }
			        if($formData['HomeAddrDistrict'] != ' -- '){
			            $x .= "{$formData['HomeAddrDistrict']}";
			        }
			        if($formData['HomeAddrStreet'] != ' -- '){
			            $x .= "{$formData['HomeAddrStreet']}";
			        }
			        if($formData['HomeAddrBlding'] != ' -- '){
			            $x .= "{$formData['HomeAddrBlding']}";
			        }
			        if($formData['HomeAddrBlock'] != ' -- '){
			            $x .= "{$formData['HomeAddrBlock']}座";
			        }
			        if($formData['HomeAddrFloor'] != ' -- '){
			            $x .= "{$formData['HomeAddrFloor']}樓";
			        }
			        if($formData['HomeAddrRoom'] != ' -- '){
			            $x .= "{$formData['HomeAddrRoom']}室";
			        }
			    }else{
			        if($formData['HomeAddrRoom'] != ' -- '){
			            $x .= "Room {$formData['HomeAddrRoom']}, ";
			        }
			        if($formData['HomeAddrFloor'] != ' -- '){
			            $x .= "Floor {$formData['HomeAddrFloor']}, ";
			        }
			        if($formData['HomeAddrBlock'] != ' -- '){
			            $x .= "Block {$formData['HomeAddrBlock']}, ";
			        }
			        if($formData['HomeAddrBlding'] != ' -- '){
			            $x .= "{$formData['HomeAddrBlding']}, ";
			        }
			        if($formData['HomeAddrStreet'] != ' -- '){
			            $x .= "{$formData['HomeAddrStreet']}, ";
			        }
			        if($formData['HomeAddrDistrict'] != ' -- '){
			            $x .= "{$formData['HomeAddrDistrict']}, ";
			        }
			        if($formData['HomeAddrRegion'] != ' -- '){
    			        switch ($formData['HomeAddrRegion']){
    			            case 1:
    			                $x .= 'Hong Kong';
    			                break;
    			            case 2:
    			                $x .= 'Kowloon';
    			                break;
    			            case 3:
    			                $x .= 'New Territories';
    			                break;
    			        }
			        }
			    }
			}else{
				$x .='<table>
				        <colgroup>
				            <col style="width: 150px;" />
				            <col style="width: 150px;" />
				            <col style="width: 100px;" />
				            <col style="width: 150px;" />
				            <col style="width: 100px;" />
				            <col style="width: 150px;" />
				            <col />
				        </colgroup>
				        <tr>
				            <td>Room '.$Lang['Admission']['MINGWAI']['room'].'</td>
				            <td>'.$this->getFormElement("HomeAddrRoom").'</td>
				            <td style="text-align: right;">Floor '.$Lang['Admission']['MINGWAI']['floor'].'</td>
				            <td>'.$this->getFormElement("HomeAddrFloor").'</td>
				            <td style="text-align: right;">Block '.$Lang['Admission']['MINGWAI']['block'].'</td>
				            <td>'.$this->getFormElement("HomeAddrBlock").'</td>
			                <td>&nbsp;</td>
				        </tr>
				        <tr>
				            <td>Building '.$Lang['Admission']['MINGWAI']['building'].'</td>
				            <td colspan="6">'.$this->getFormElement("HomeAddrBlding").'</td>
				        </tr>
				        <tr>
				            <td>Street '.$Lang['Admission']['MINGWAI']['street'].'</td>
				            <td colspan="6">'.$this->getFormElement("HomeAddrStreet").'</td>
				        </tr>
				        <tr>
				            <td>District '.$Lang['Admission']['MINGWAI']['district'].'</td>
				            <td colspan="3">'.$this->getFormElement("HomeAddrDistrict").'</td>
				            <td style="text-align: right;">Region '.$Lang['Admission']['MINGWAI']['region'].'</td>
				            <td>'.$this->getFormElement("HomeAddrRegion").'</td>
			                <td>&nbsp;</td>
				        </tr>
				    </table>';
	        }
			    $x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.'Primary email address '.$Lang['Admission']['MINGWAI']['email'].'</td>
				<td colspan="3">'.$this->getFormElement("PrimaryEmailRole").' '.$this->getFormElement("PrimaryEmail").'<br/>(Only one e-mail address is required)<br/>(申請人只需提供一個電郵地址)</td>
		    </tr>
		    <tr>
				<td class="field_title">Secondary email address '.$Lang['Admission']['MINGWAI']['email2'].'</td>
				<td colspan="3">'.($IsConfirm && $this->getFormElement("SecondaryEmail") == ' -- ' ? ' -- ' : $this->getFormElement("SecondaryEmailRole").' '.$this->getFormElement("SecondaryEmail")).'<br/>(Only one e-mail address is required)<br/>(申請人只需提供一個電郵地址)</td>
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).'Language '.$Lang['Admission']['HKUGAPS']['SpokenLanguage'].'</td>
				<td colspan="3">'.$this->getFormElement("SpokenLanguage").'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.'Application from Twins '.$Lang['Admission']['KTLMSKG']['twins'].'</td>
				<td>'.$this->getFormElement("Twins").'</td>
				    
				<td class="field_title">Twins\' Number of Identity Document '.$Lang['Admission']['KTLMSKG']['twinsID'].'</td>
				<td>'.$this->getFormElement("TwinsID").'</td>
			</tr>
		</table>';
				
		$x .= '<h1>'.$Lang['Admission']['YLSYK']['ApplyClass'].' Apply Class</h1>
			<table class="form_table" style="font-size: 13px">
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).'Date of Enrollment 申請入讀日期</td>
					<td>'.$this->getFormElement("FormStudentInfo_Enroll_Date").'</td>
				</tr>
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).'Program 課程</td>
					<td>Option 1 選擇 1'.($IsConfirm?$program_selected1:$program_selection1).' Option 2 選擇 2'.($IsConfirm?$program_selected2:$program_selection2).'</td>
				</tr>	
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).'Session 時段</td>
					<td '.($IsConfirm?'':'colspan="3"').'>
					<div id="DayTypeOption">'.$dayTypeOption.'</div>
					<br/>Note: Our A.M./P.M. classrooms are limited in size by student teacher ratio. We will try to accommodate your request if at all possible.
					<br/>備註:*由於學位有限，幼兒至高班之上午/下午班制選項只作統計之用，幼兒就讀之班制於取錄後以抽籤方式決定。</td>
				</tr>
				<tr>		
					'.(($IsConfirm || $IsUpdate)?'<td class="field_title">'.$Lang['Admission']['applyLevel'].'</td><td>'.$classLevel.'</td>':'').'
				</tr>		
			</table>';

		return $x;
	}
	

	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang, $lac;
		$this->IsConfirm = $IsConfirm;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['RMKG']['onlineform']['ParentInfo'].'</h1>
				<table class="form_table" style="font-size: 13px">
				<tr>
					<td class="field_title"></td>
					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['father'].'</center></td>
					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['mother'].'</center></td>
					<td class="form_guardian_head"><center>Guardian 監護人</center></td>
				</tr>
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['name'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1ChineseName","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2ChineseName","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3ChineseName","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['occupation'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1Occupation","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2Occupation","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3Occupation","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['RMKG']['onlineform']['companyname'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1CompanyName","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2CompanyName","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3CompanyName","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['RMKG']['onlineform']['mobile'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1MobileNo","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2MobileNo","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3MobileNo","Parent").'</td>
				</tr>
			</table>';
			
		return $x;
	}
	function getOthersForm($IsConfirm=0){
		
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang, $PATH_WRT_ROOT, $setting_path_ip_rel;
		$this->IsConfirm = $IsConfirm;
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}

		}
		
		######## Breafing START ########
		include_once ("{$PATH_WRT_ROOT}includes/admission/libadmission_briefing_base.php");
		include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/admission_briefing.php");
		$objBreafing = new admission_briefing();
		$breafingSessions = $objBreafing->getAllBriefingSession($lac->getNextSchoolYearID());
		$breafingDate = substr($breafingSessions[0]['BriefingStartDate'], 0, 10);
		$breafingQuotaPerApplicant = $breafingSessions[0]['QuotaPerApplicant'];
		
		if($IsConfirm){
		    $breafingQuotaSelectHTML = $formData['BreafingReserveSeats'];
		}else{
    		$breafingQuotaOption = array();
    		for($i=1;$i<=$breafingQuotaPerApplicant;$i++){
    		    $breafingQuotaOption[] = array($i,$i);
    		}
    		$breafingQuotaSelectHTML = $this->GET_SELECTION_BOX($breafingQuotaOption, ' id="BreafingReserveSeats" name="BreafingReserveSeats"', '');
		}
		######## Breafing END ########
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>Other Information '.$Lang['Admission']['otherInfo'].'</h1>';

		
		$x .= '<table id="dataTable1" class="form_table" style="font-size: 13px">';
		
		$x .= '<tr>
					<td class="field_title">Name of previous school attended<br/>曾就讀學校名稱</td>
					<td>'.($IsConfirm?$formData['LastSchool']:'<input type="text" name="LastSchool" class="textboxtext" id="LastSchool" value="'.$applicationOthersInfo['LastSchool'].'" />').'</td>
					<td>Class<br/>班別</td>
					<td>'.($IsConfirm?$formData['LastSchoolLevel']:'<input type="text" name="LastSchoolLevel" class="textboxtext" id="LastSchoolLevel" value="'.$applicationOthersInfo['LastSchoolLevel'].'" />').'</td>
				</tr>';
		
		$x .= '<tr>
					<td class="field_title">Names of parents or siblings who are alumni of Ming Wai<br/>曾在本校就讀父母/兄弟姊妹姓名</td>
					<td>'.($IsConfirm?$formData['OthersExRelativeStudiedName']:'<input type="text" name="OthersExRelativeStudiedName" class="textboxtext" id="OthersExRelativeStudiedName" value="'.$applicationOthersInfo['OthersExRelativeStudiedName'].'" />').'</td>
					<td>Year of graduation 畢業年份</td>
					<td>'.($IsConfirm?$formData['OthersExRelativeStudiedYear']:'<input type="text" name="OthersExRelativeStudiedYear" class="textboxtext" id="OthersExRelativeStudiedYear" value="'.$applicationOthersInfo['OthersExRelativeStudiedYear'].'" />').'</td>
				</tr>';
		
		$x .= '<tr>
					<td class="field_title">Name of siblings who are currently enrolled at Ming Wai<br/>現就讀本校之兄弟姊妹姓名</td>
		            <td>'.($IsConfirm?$formData['OthersRefRelativeStudiedName']:'<input type="text" name="OthersRefRelativeStudiedName" class="textboxtext" id="OthersRefRelativeStudiedName" value="'.$applicationOthersInfo['OthersRefRelativeStudiedName'].'" />').'</td>
					<td>Class 班別</td>
		            <td>'.($IsConfirm?$formData['OthersRefRelativeStudiedClass']:'<input type="text" name="OthersRefRelativeStudiedClass" class="textboxtext" id="OthersRefRelativeStudiedClass" value="'.$applicationOthersInfo['OthersRefRelativeStudiedClass'].'" />').'</td>
				</tr>';
		
		if($IsConfirm){
		    if($formData['isJoinBriefing'] == '1'){
    		    $x .= '<tr>
        					<td class="field_title">Briefing Session<br />簡介會</td>
        					<td colspan="3">
                              <div>
        		                  Date 日期： '.$breafingDate.'
                              </div>
                              <div style="margin-top: 5px;">
        	                      Reserve seats 申請預留座位：'.$breafingQuotaSelectHTML.'
                              </div>
        		            </td>
        				</tr>';
		    }else{
    		    $x .= '<tr>
					<td class="field_title">Briefing Session<br />簡介會</td>
					<td colspan="3">
	                  Not join 不參加
	                </td>
		        </tr>';
		    }
		}else{
    		$x .= '<tr>
    					<td class="field_title">'.$star.'Briefing Session<br />簡介會</td>
    					<td colspan="3">
    		              <div>
    		                  <input type="radio" id="notJoinBriefing" name="isJoinBriefing" value="0" />
    	                      <label for="notJoinBriefing">Not join 不參加</label>
    		                  <br />
    		                  <input type="radio" id="joinBriefing" name="isJoinBriefing" value="1" checked />
    		                  <label for="joinBriefing">Join 參加</label>
    		              </div>
    		              <div id="briefingDetails" style="margin:10px;">
                              <div>
        		                  Date 日期： '.$breafingDate.'
                              </div>
                              <div style="margin-top: 5px;">
        	                      Reserve seats 申請預留座位：'.$breafingQuotaSelectHTML.'
                              </div>
                          </div>
    		            </td>
    				</tr>';
		}
		
		if($lac->isInternalUse($_GET['token'])){
		    #### Admission Method START ####
		    $x .= '<tr>
					<td class="field_title">Admission Method 報名方法</td>
					<td colspan="3">';
		    if($IsConfirm){
		        if($formData['AdmissionMethod'] == 'Hand'){
		            $x .= 'Hardcopy (in person) 紙本報名 (親身遞交)';
		        }else{
		            $x .= "Hardcopy (by mail) 紙本報名 (郵寄)";
		            if($formData['MailCode']){
		                $x .= "<br />(Mail Code 郵寄編號: {$formData['MailCode']})";
		            }
		        }
		    }else{
		        $x .= '<select id="AdmissionMethod" name="AdmissionMethod">
		                  <option value="Hand">Hardcopy (in person) 紙本報名 (親身遞交)</option>
		                  <option value="Mail">Hardcopy (by mail) 紙本報名 (郵寄)</option>
		              </select>';
		        $x .= '
	            <span id="MailCodeDiv">
		            (
		                Mail Code 郵寄編號: 
		                <input type="text" name="MailCode" class="textboxtext" id="MailCode" value="" style="width: 200px;" />
		            )
	            </span>';
		    }
		    $x .=   '</td>
				</tr>';
		    #### Admission Method END ####

		    #### Fee START ####
		    $x .= '<tr>
					<td class="field_title">Fee 費用</td>
					<td colspan="3">';
		    if($IsConfirm){
		        $feeHTML = '';
		        foreach($admission_cfg['fee'] as $feeType){
		            if(in_array($feeType, (array)$_POST['Fee'])){
		                $feeHTML .= "{$Lang['Admission']['MINGWAI']['feeType'][$feeType]}, ";
		            }
		        }
		        $feeHTML = trim($feeHTML, ', ');
		        $x .= $feeHTML;
		    }else{
		        foreach($admission_cfg['fee'] as $feeType){
    		        $x .= '
    		            <input type="checkbox" name="Fee[]" id="Fee_'.$feeType.'" value="'.$feeType.'" />
           		    	<label for="Fee_'.$feeType.'">'.$Lang['Admission']['MINGWAI']['feeType'][$feeType].'</label>
    	            ';
		        }
		    }
		    $x .=   '</td>
				</tr>';
		    #### Fee END ####
		}
		
// 		$x .= '<tr>
// 					<td></td>
// 					<td class="form_guardian_head"><center>'.$star.$Lang['Admission']['RMKG']['onlineform']['1stlang'].'</center></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['2ndlang'].'</center></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['3rdlang'].'</center></td>
// 				</tr>
// 				<tr>
// 					<td class="field_title">'.$Lang['Admission']['RMKG']['onlineform']['SpokenLang'].'</td>
// 					<td class="form_guardian_field"><center>'.$this->getFormElement("StuMotherTongue").'</center></td>
// 					<td class="form_guardian_field"><center>'.$this->getFormElement("StuSecondTongue").'</center></td>
// 					<td class="form_guardian_field"><center>'.$this->getFormElement("StuThirdTongue").'</center></td>
// 				</tr>';
// 		$x .= '<tr>
// 					<td class="field_title">'.$star.$Lang['Admission']['RMKG']['onlineform']['KinderU'].'</td>
// 					<td>'.$this->getFormElement("IsKinderU").'</td>
// 				</tr>';
// 		$x .='</table>';

// 		$x .= '<table id="dataTable2" class="form_table" style="font-size: 13px">
// 				<tr>
// 					<td colspan="6">'.$Lang['Admission']['RMKG']['onlineform']['RelativeStudiedAtSchool'].'</td>
// 				</tr>
// 				<tr>
// 					<td></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['name'].'</center></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['age'].'</center></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['sex'].'</center></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['class'].'</center></td>
// 					<td class="form_guardian_head"><center>'.$Lang['Admission']['RMKG']['onlineform']['schoolyear'].'</center></td>
// 				</tr>';
// 		for($i=1; $i<5; $i++){
// 			$x  .=	'<tr>
// 						<td class="field_title" style="text-align:right">('.$i.')</td>
// 						<td class="form_guardian_field"><center>'.$this->getFormElement("OthersRelativeStudiedName".$i).'</center></td>
// 						<td class="form_guardian_field"><center>'.$this->getFormElement("OthersRelativeAge".$i).'</center></td>
// 						<td class="form_guardian_field"><center>'.$this->getFormElement("OthersRelativeGender".$i).'</center></td>
// 						<td class="form_guardian_field"><center>'.$this->getFormElement("OthersRelativeClass".$i).'</center></td>
// 						<td class="form_guardian_field"><center>'.$this->getFormElement("OthersRelativeStudiedYear".$i).'</center></td>
// 					</tr>';
// 		}			
//			for($i=2; $i<=5; $i++){
//				if($IsConfirm && ($formData['OthersRelativeStudiedYear'.$i] !="" || $formData['OthersRelativeStudiedName'.$i] !="" || $formData['OthersRelativeClassPosition'.$i] !="" || $formData['OthersRelativeRelationship'.$i] !=""))
//				$x .= '<tr>
//						<td class="field_title" style="text-align:right">('.$i.')</td>
//						<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersRelativeStudiedName'.$i].'</center>':'<input name="OthersRelativeStudiedName'.$i.'" type="text" id="OthersRelativeStudiedName'.$i.'" class="textboxtext" />').'</td>
//						<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersRelativeRelationship'.$i].'</center>':'<input name="OthersRelativeRelationship'.$i.'" type="text" id="OthersRelativeRelationship'.$i.'" class="textboxtext" />').'</td>
//						<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersRelativeStudiedYear'.$i].'</center>':'<input name="OthersRelativeStudiedYear'.$i.'" type="text" id="OthersRelativeStudiedYear'.$i.'" class="textboxtext" />').'</td>
//					</tr>';
//			}

			$x .= '</table>';

		
		return $x;
	}
	
	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		
		if(!$lac->isInternalUse($_GET['token'])){
			$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		}else{
			$star = '';
		}
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1 style="font-size: 15px">Docs Upload '.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table" style="font-size: 15px">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)<br/>Document (image in JPEG/GIF/PNG/PDF format, file size less than : '.($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
		
		$x .='<tr>
				<td class="field_title">'.$star.'Personal Photo '.$Lang['Admission']['personalPhoto'].'</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/>').'
				</td>
			</tr>';
		
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$star.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
				  </tr>';
		}
			
		/*$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';*/
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN('Back '.$Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN('Next '.$Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.'Cancel '.$Lang['Btn']['Cancel'].'" />
				
			</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = @stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN('Back '.$Lang['Btn']['Back'], "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN('Submit '.$Lang['Btn']['Submit'], "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.'Cancel '.$Lang['Btn']['Cancel'].'" />

			</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		
		// modifing 2015-10-02
		$hasPaid = 0;
		$cancelled = 0;
		$result = $lac->getPaymentResult('', '', '', '', $ApplicationID);
		if($result){
			foreach($result as $aResult){
				if($aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']){
					$hasPaid = 1;
					//$isSent = $aResult['EmailSent'];
					$paymentId = $aResult['PaymentID'];
				}
				if($aResult['Status'] == $admission_cfg['Status']['cancelled']){
					$cancelled = 1;
				}
			}
		}
		$sql = "SELECT AutoEmailSent FROM ADMISSION_APPLICATION_STATUS Where ApplicationID = '".$ApplicationID."' ";
		$isSent = current($lac->returnArray($sql));
		
		if($hasPaid){
		    $lac->autoApplyBriefing($ApplicationID);
		    
			if(!$lac->isInternalUse($_REQUEST['token']) && $isSent['AutoEmailSent'] <= 0){
				$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
				$applicationSetting = $lac->getApplicationSetting($schoolYearID);
				$mail_content = $lauc->getFinishPageEmailContent($ApplicationID, $applicationSetting[$sus_status]['EmailContent'], $schoolYearID);

				$lac->sendMailToNewApplicant($ApplicationID,'',$mail_content);
				$sql = "UPDATE ADMISSION_APPLICATION_STATUS Set  AutoEmailSent = '1' Where  ApplicationID = '".$ApplicationID."' ";
				$lac->db_db_query($sql);
			}
    		// finish page	
    		$x .= $this->getWizardStepsUI(9);
    		
    		//$x .= $this->getWizardStepsUI(7);
    		if($ApplicationID){
    
    		    ######## Briefing START ########
    		    $objBreafing = new admission_briefing();
    		    
    		    $briefing = $objBreafing->getBriefingApplicantByApplicantID($ApplicationID);
    		    if($briefing){
    		        $session = $objBreafing->getBriefingSession($briefing['BriefingID']);
    		        $briefingDate = substr($session['BriefingStartDate'], 0, 10);
    		        $briefingStartTime = substr($session['BriefingStartDate'], 11, 5);
    		        $briefingEndTime = substr($session['BriefingEndDate'], 11, 5);
    		    }else{
    		        $BreafingReserveSeats = $lac->getApplicationCustInfo($ApplicationID, 'BreafingReserveSeats', true);
    		        $BreafingReserveSeats = $BreafingReserveSeats['Value'];
    		    }
    		    ######## Briefing END ########
    		    
    			$x .=' <div class="admission_complete_msg">';
    			//add english version here...
    			$x .='<h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
    //            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
    //                           </p>';
    //            $x .='<p>
    //				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
    //				</p>';
    
    			######## Briefing START ########
        		if($briefing){
        			$x .= "<h1>
        			Briefing Details:
        			<span>
        			    Date: {$briefingDate}<br />
        			    Time: {$briefingStartTime} ~ {$briefingEndTime}
        		    </span></h1>";
        		}else if($BreafingReserveSeats > 0){
        		    $x .= '<h1 style="color: red;">Briefing session full</h1>';
        		}
    			######## Briefing END ########
    			
    			if(!$lac->isInternalUse($_REQUEST['token'])){
    			    $x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';
    			}
    
    			$x .=' <h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
    //            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
    //                           </p>';
    //            $x .='<p>
    //				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
    //				</p>';
    
    			######## Briefing START ########
        		if($briefing){
        			$x .= "<h1>
        			簡介會詳情:
        			<span>
        			    日期: {$briefingDate}<br />
        			    時間: {$briefingStartTime} ~ {$briefingEndTime}
        		    </span></h1>";
        		}else if($BreafingReserveSeats > 0){
        		    $x .= '<h1 style="color: red;">簡介會名額已滿</h1>';
        		}
    			######## Briefing END ########
    			
    			if(!$lac->isInternalUse($_REQUEST['token'])){
    				$x .='<h1><span>申請通知電郵已發送，請檢查 閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
    			}
    			
    			$x .= '</div>';        
    			
    		
    		}
    		else{
    			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
                //$x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
                //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
    		}
    		if(!$LastContent || !$ApplicationID){
    			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
    		}
    		$x .= '<br/>'.$LastContent;
    		$x .= $this->getBriefingEmailContent($ApplicationID, $schoolYearID);
    		$x .= '</div>';
    		$x .= '</div>';

    		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
    			$x .= '<div class="edit_bottom">
    					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.'Finish '.$Lang['Admission']['finish'].'" />
    				</div>
    				<p class="spacer"></p></div>';
    		}
    		else{	
    			$x .= '<div class="edit_bottom">
    					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.'Finish '.$Lang['Admission']['finish'].'" />
    				</div>
    				<p class="spacer"></p></div>';
    		}
    		
		}
		else{
		// payment page
		$x .= '<div id="payment_page">';
		$x .= $this->getWizardStepsUI(7);
		if($cancelled){
			$x .=' <div class="admission_complete_msg"><h1>未能成功遞交申請。<span>請重新嘗試申請或致電與本校聯絡。</span></h1>';
            $x .='<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
			$x .= '</div></div>';
		}
		else if($ApplicationID){
			$x .='<div class="admission_complete_msg"><h1>To finish the online application, please pay now.<br/>';            
			$x .='現在請進行處理費付款 ，以完成報名程序。</h1>';
			$x .= '</div>';
			
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" value="Pay admission fee HK$60 繳付 HK$60 報名費" onclick="$(\'#payment_page\').hide();$(\'#paypal_page\').show();"></input>
				</div>
				<p class="spacer"></p></div></div>';
		
			$x .= '<div id="paypal_page" style="display:none">';
			$x .= $this->getWizardStepsUI(8);
			$x .='<div class="admission_complete_msg"><h1><span><font color="red">*</font> After paying the fee using Paypal, you MUST click<br/> "<font color="red">Back to '.$admission_cfg['paypal_name'].'</font>" <br/> to complete the whole procedure.<br/><br/><u>Remark: Please allow pop-up to PayPal if blocked by your Chrome browser. If you cannot find the corresponding payment form in PayPal, please click the Payment icon in step 6 again.</u><br><br><font color="red">*</font> 現在前往 Paypal 付款，完成後，必須於付款頁面按<br/> "<font color="red">返回'.$admission_cfg['paypal_name'].'</font>" <br/>方能完成整個付款及報名程序。<br/><br/><u>備註：請允許 Chrome 顯示彈出式視窗，以進入 PayPal 系統付款。若允許後顯示不到相關付款介面，請再按第六個步驟內的「立即付款」按鈕來嘗試。</u><br/><br/>'.$lauc->getPayPalButton(MD5($result[0]['ApplicationID'])).'</span></h1>';            
//			$x .='To finish the online application, please pay now.</h1>';
			$x .= '</div>';
			$x .= '</div></div>';
//			$x .= '<div class="edit_bottom">
//					<input type="button" class="formsubbutton" value="繳付 HK$60 處理費 Pay application fee HK$60" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'Payment Status\');"></input>
//				</div>
//				<p class="spacer"></p></div>';
				
			
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>未能成功遞交申請。<span>請重新嘗試申請或致電與本校聯絡。</span></h1>';
            $x .='<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
			$x .= '</div></div>';
		}
//		if(!$LastContent || !$ApplicationID){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent;
		
//		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
//			$x .= '<div class="edit_bottom">
//					<input id="finish_page_payment_button" type="button" class="formsubbutton" value="繼續 Continue" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'Payment Status\');"></input>
//					<input id="finish_page_finish_button" style="display:none" type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
//				</div>
//				<p class="spacer"></p></div>';
//		}
//		else{	
//			$x .= '<div class="edit_bottom">
//					<input id="finish_page_payment_button" style="display:none" type="button" class="formsubbutton" value="繼續 Continue" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'_blank\');"></input>
//					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
//				</div>
//				<p class="spacer"></p></div>';
//		}
		
		}
		return $x;
	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID='', $paymentEmail=''){
		global $PATH_WRT_ROOT,$Lang, $kis_lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		$protocol = (checkHttpsWebProtocol())? 'https':'http';
		if($ApplicationID){
			$sql = "Select Status From ADMISSION_APPLICATION_STATUS Where ApplicationID = '".$ApplicationID."'";
			$result = current($lac->returnArray($sql));
			$paymentSettle = 0;
			if($result['Status'] >= $admission_cfg['Status']['paymentsettled'] && $result['Status'] != $admission_cfg['Status']['cancelled']){
				$paymentSettle = 1;
			}
			if($paymentEmail || $paymentSettle == 0){
				$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
				$stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
				
				$x .='Your application form has been received, name of child is '.$stuedentInfo[0]['student_name_en'].'. To pay or check payment status, click this hyperlink:<br/>';
				$x .='已收到 閣下的申請資料，學童姓名為 '.$stuedentInfo[0]['student_name_b5'].'，待繳付處理費後會作實申請。如未繳付，可按以下連結進行。';         
				$x .="<br/><a target='_blank' href='".$protocol."://".$_SERVER['HTTP_HOST']."/kis/admission_form/finish.php?id=".urlencode(getEncryptedText("ApplicationID=".$ApplicationID."&sus_status=".$stuedentInfo[0]['classLevelID']."&SchoolYearID=".$schoolYearID,$admission_cfg['FilePathKey']))."'>".$protocol."://".$_SERVER['HTTP_HOST']."/kis/admission_form/finish.php?id=".urlencode(getEncryptedText("ApplicationID=".$ApplicationID."&sus_status=".$stuedentInfo[0]['classLevelID']."&SchoolYearID=".$schoolYearID,$admission_cfg['FilePathKey']))."</a>";
			}
			else{
				$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
				$stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
				
				$x .='<p>Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>Application form preview<br/><br/>'.$protocol.'://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);
				$x .='<br/><br/>';
				$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>'.$protocol.'://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink($schoolYearID, $ApplicationID);         
				
				$encrypted_data = getEncryptedText('applicationid='.$ApplicationID);
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_email_acknowledgement.php?q='.$encrypted_data;
				$acknowledge_link = '<br /><div><a style="background-color: #FFFF00" href="'.$link.'" target="_blank">[ '.($acknowledgeMessage==''?$kis_lang['acknowledgementlinktext']:$acknowledgeMessage).' ]</a></div>';
				$x .= $acknowledge_link;
				
				if(!$LastContent){
					$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
				}
				$x .= '<br/>'.$LastContent.'</p>';

				######## Briefing START ########
				$x .= $this->getBriefingEmailContent($ApplicationID, $schoolYearID);
				######## Briefing END ########
			}
		}
		else{
			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
			//$x .='<br/><br/>';
			//$x .='Admission is Not Completed. Please try to apply again!';
        }
		
		return $x;
	}
	
	function getBriefingEmailContent($ApplicationID, $schoolYearID=''){
	    global $Lang, $lac;
	    $protocol = (checkHttpsWebProtocol())? 'https':'http';
	    $x = '';
	    $objBreafing = new admission_briefing();
	    
	    $briefing = $objBreafing->getBriefingApplicantByApplicantID($ApplicationID);
	    if($briefing){
	    	$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
			$stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
				
	        $session = $objBreafing->getBriefingSession($briefing['BriefingID']);
	        $briefingDate = substr($session['BriefingStartDate'], 0, 10);
	        $briefingDateZh = date('Y年m月d日', strtotime($briefingDate));
	        $briefingDateEn = date('d F Y', strtotime($briefingDate));
	        $briefingWeekday = date('w', strtotime($briefingDate));
	    
	        $briefingWeekdayZh = $Lang['Admission']['WeekDay'][$briefingWeekday];
	        $briefingWeekdayEn = date('l', strtotime($briefingDate));
	        $briefingStartTime = substr($session['BriefingStartDate'], 11, 5);
	        $briefingEndTime = substr($session['BriefingEndDate'], 11, 5);
	    
	    
	        $x .= '<br /><br /><br /><p>
                                                                敬啟者 :<br /><br />
                    <span style="margin-left: 20px;">承蒙　閣下對本校的信任及支持, 謹通知 閣下有關下列事項, 敬請留意。</span></p>
	    
                    <p style="text-align: center;">新生家長簡介會</p>
    		        <p style="margin-left: 20px;">為使家長能深入暸解本校課程及理念, 現誠邀 閣下出席「新生家長簡介會」。有關簡介會之安排, 臚列如下:</p>
	    
				    <table style="margin-left: 20px;">
    				    <tr>
    				        <td style="width: 100px">學生姓名:</td>
    				        <td>'.$stuedentInfo[0]['student_name_b5'].'</td>
    				    </tr>
    				    <tr>
    				        <td>參考編號:</td>
    				        <td>'.$ApplicationID.'</td>
    				    </tr>
    				    <tr>
    				        <td>日 期:</td>
    				        <td>'.$briefingDateZh.' ('.$briefingWeekdayZh.')</td>
    				    </tr>
    				    <tr>
    				        <td>時 間:</td>
    				        <td>'.$briefingStartTime.'~'.$briefingEndTime.'</td>
    				    </tr>
    				    <tr>
    				        <td>出席人數:</td>
    				        <td>'.$briefing['SeatRequest'].'</td>
    				    </tr>
    				    <tr>
    				        <td>地 點:</td>
    				        <td>明慧國際幼稚園(北角分校) - 香港北角七姊妹道5-13號地下一號舖及一樓全層</td>
    				    </tr>
    				    <tr>
    				        <td style="vertical-align: top;">備 註:</td>
    				        <td>
    				            1. 請 貴家長憑此確認信依時出席<br />
    				            2. 請勿攜帶幼兒及嬰兒車出席是次活動
				            </td>
    				    </tr>
				    </table>
	    
                    <br />
                    <p style="margin-left: 20px;">此致</p>
                    <p>貴家長</p>
                    <p style="text-align: right;">
    				            陳 杏 校長 謹啟<br />
    				            明慧國際幼稚園·明慧國際幼兒園
		            </p>
                    ';
	    
	        $x .= '<br /><br /><p>
    				    Dear Parents / Guardians,<br /><br />
    				    Thank you for your support and entrusting our school.  Please note the following event:</p>
	    
                    <p style="text-align: center;text-decoration: underline;">Briefing Session for Parents</p>
    		        <p>For a better understanding of our school curriculum and aims, you are cordially invited to attend the Briefing Session for Parents. The arrangement details are as follows:</p>
	    
				    <table style="margin-left: 20px;">
    				    <tr>
    				        <td style="width: 120px">Student Name:</td>
    				        <td>'.$stuedentInfo[0]['student_name_en'].'</td>
    				    </tr>
    				    <tr>
    				        <td>Reference No.:</td>
    				        <td>'.$ApplicationID.'</td>
    				    </tr>
    				    <tr>
    				        <td>Date:</td>
    				        <td>'.$briefingDateEn.' ('.$briefingWeekdayEn.')</td>
    				    </tr>
    				    <tr>
    				        <td>Time:</td>
    				        <td>'.$briefingStartTime.'~'.$briefingEndTime.'</td>
    				    </tr>
    				    <tr>
    				        <td>No. of Attendance:</td>
    				        <td>'.$briefing['SeatRequest'].'</td>
    				    </tr>
    				    <tr>
    				        <td style="vertical-align: top;">Venue:</td>
    				        <td>
    				            Ming Wai Int’l Kindergarten (North Point Branch)<br />
    				            Shop 1, G/F & 1/F., 5 - 13 Tsat Tsz Mui Road, North Point, Hong Kong
				            </td>
    				    </tr>
    				    <tr>
    				        <td style="vertical-align: top;">Remarks:</td>
    				        <td>
    				            1. Please show this confirmation letter on Briefing Session and attend the event on time.<br />
    				            2. Please do not bring any strollers and children on the day.
				            </td>
    				    </tr>
				    </table>
	    
                    <br />
    				<p style="text-align: right;">
			            Chan Hang<br />
			            The Principal<br />
			            Ming Wai Int\'l Kindergarten & Ming Wai Int\'l Preschool
		            </p>';
	    
	        $x .= '<p><img src="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_form/qrcode.php?encode='.$ApplicationID.'" style="width: 30mm;" /></p>';
	    }
	    
	    return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';            
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['admissionQuotaFull'].'</h1>';
				//$LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['interviewQuotaFull'].'</h1>';
				//$LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	
	function getPDF_CSS(){
		$css = '<style>
				@media all {
				.page-break {display: none;}
				}
				@media print {
				.page-break {display: block; page-break-before: always;}
				}
				body { padding:0; margin:0; }
				body{	font-family: "aa";color: #000000; /*font-size:16px;*/}
				#content{ width:18.6cm; height:28.7cm; background-color: #fff;  margin:0 auto; padding-top:0.5cm; }
				.top{ padding:0;  width:21cm;}
				.logo{width:120px; margin:0 auto 0 auto;}
				/*--top--*/
				.heading{margin:0 auto 0 auto; text-align:center; line-height:30px;font-size:0.9em;  width:20.5cm; font-weight:bold; }
				.heading a{ color:#0191da; text-decoration:none;}
				.heading a:hover{color:#0191da;text-decoration:underline;}
				.heading2{margin:5px auto 0 auto; text-align:center; line-height:30px;font-size:1.3em;  width:20.5cm; }
				
				
				/* -----form_table_information----*/
				table.application_form_table_information{ border-collapse:collapse; border:1px solid #000; font-size:0.9em;  margin:5px auto 0px auto; width:20.5cm; }
				.application_form_table_information th {font-weight: normal;color: #000;text-align:left; line-height:20px; border:1px solid #666; padding:3px 5px 2px 5px;vertical-align:top; font-weight:bold; }
				.application_form_table_information td {font-weight: normal;color: #000;text-align:left; line-height:20px; border:1px solid #666; padding:3px 5px 2px 5px;vertical-align:top;}
				.application_form_table_information td.title {font-weight: normal; color: #000;text-align:center;}
				.application_form_table_information td.text_center{text-align:center;}
				.application_form_table_information td.text_left{text-align:left;}
				.application_form_table_information td.text_right{ text-align:right;}
				
				.application_form_table_information th.text_center{text-align:center;}
				.application_form_table_information th.text_left{text-align:left;}
				.application_form_table_information th.text_right{ text-align:right;}
				
				/*underline*/
				.underline {border-bottom: 1px solid #666; color: #000; display: inline-block; padding: 0px 5px 2px 5px; height: 1em; text-align:left;}
				.underline .w85{ width:85px;}
				.underline .w30{ width:30px;}
				
				
				
				.photo{width:4cm; height:4.5cm; display:block; margin:auto; text-align:center;}
				.photo_txt{margin:60px auto; display: inline-block; text-align:center; line-height:22px; font-size:1.2em;}
				
				
				/* -----form_table_information2----*/
				table.application_form_table_information2{ border-collapse:collapse; font-size:0.9em;  margin:5px auto 0px auto; width:20.5cm; border:none; font-weight:bold;}
				.application_form_table_information2 td.title {font-weight: normal; color: #000;text-align:center;}
				.application_form_table_information2 ul{ margin:0; padding-left:25px;}
				
				
				
				/* -----form_table_information3----*/
				table.application_form_table_information3{ border-collapse:collapse; font-size:0.9em;  margin:10px auto 0px auto; width:20.5cm; border:none;}
				table.application_form_table_information3 td{ vertical-align:top;}
				.application_form_table_information3 td.title {font-weight: normal; color: #000;text-align:center;}
				.agree {text-decoration: line-through;}
				.application_form_table_information3 ul{ margin:0; padding-left:25px;}
				
				/*------checkbox-----*/
				/*.checkbox{ width:13px; height:13px; display:inline-block; vertical-align:middle; margin-right:5px; }*/

				
				/*---border----*/
				td.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
				td.border_t_r_none{border-right:none;}
				td.border_t_l_none{border-left:none;}
				th.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
				th.border_t_r_none{border-right:none;}
				th.border_t_l_none{border-left:none;}
				th.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
				td.border_t_r_none{border-top:none;border-right:none;}
				th.border_t_r_none{border-top:none;border-right:none;}
				td.border_t_r_b_none{border-top:none;border-right:none;border-bottom:none;}
				td.border_t_l_b_none{border-top:none;border-left:none;border-bottom:none;}
				th.border_t_r_b_none{border-top:none;border-right:none;border-bottom:none;}
				th.border_t_l_b_none{border-top:none;border-left:none;border-bottom:none;}
				td.border_t_b_none{border-top:none; border-bottom:none;}
				
				
				td.border_none{border:none;}
				</style>';
		return $css;
	}
	
	
	function getPDF_Page2(){
		$page2 = '';
		return $page2;
	}
	
	function getPDFContent($schoolYearID,$applicationIDAry,$type='',$form_lang=''){
		global $PATH_WRT_ROOT,$lac,$Lang,$admission_cfg,$intranet_session_language;
	
		$Lang['General']['EmptySymbol'] = '---';
		# pdf images
		$logo_URL = $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/images/logo.jpg';
		$checkboxOn_URL = $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/images/checkbox_on.png';
		$checkboxOff_URL = $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/images/checkbox_off.png';
		$tick_URL =  $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/images/tick.png';
		$photo_URL =  $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/images/photo.png';
		if($type == 'interview_form'){
			$templatePath = $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/pdf/interview_form.php';
			
			$sql = "SELECT RecordID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = ".$schoolYearID;
			$result = $lac->returnVector($sql);
			
			$interviewGroupArr = $lac->getInterviewGroupSetting($result);
		}
		else{
			$templatePath = $PATH_WRT_ROOT.'file/customization/mingwai.eclass.hk/pdf/application_form.php';
		}
		
// 		if($form_lang){
// 			if($form_lang =='en'){
// 				$templatePath = $PATH_WRT_ROOT.'file/customization/mingwainp.eclass.hk/pdf/application_form.php';
// 			}else{
// 				$templatePath = $PATH_WRT_ROOT.'file/customization/mingwainp.eclass.hk/pdf/application_form.php';
// 			}
// 		}else{
// 			if($intranet_session_language =='en'){
// 				$templatePath = $PATH_WRT_ROOT.'file/customization/mingwainp.eclass.hk/pdf/application_form.php';
// 			}else{
// 				$templatePath = $PATH_WRT_ROOT.'file/customization/mingwainp.eclass.hk/pdf/application_form.php';
// 			}
// 		}
	
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		$margin_top = '7';
		$mpdf = new mPDF('','A4',0,'',0,0,5,0);
		$mpdf->mirrorMargins = 1;
		//		$mpdf->backupSubsFont = array('mingliu');
		//		$mpdf->useSubstitutions = true;
		$yearStart = date('Y',getStartOfAcademicYear('',$schoolYearID));
		$yearEnd = date('Y',getEndOfAcademicYear('',$schoolYearID));
		$settings = $lac->getApplicationSetting($schoolYearID);
	
		foreach((array)$applicationIDAry as $applicationID){
			# Student Info
			//$StuInfoArr = $lac->getApplicationStudentInfo($schoolYearID,'','','',$applicationID);
			$StuInfoArr = $lac->getApplicationStudentInfo($schoolYearID,'',$applicationID);
			$StuInfo = $StuInfoArr[0];
			$StuInfo['student_name_b5'] = explode(",", $StuInfo['student_name_b5']);
			$StuInfo['student_name_en'] = explode(",", $StuInfo['student_name_en']);
			$dobYear = substr($StuInfo['dateofbirth'], 0, 4);
			$dobMonth = substr($StuInfo['dateofbirth'], 5, 2);
			$dobDay = substr($StuInfo['dateofbirth'], 8, 2);
			$inputYear = substr($StuInfo['DateInput'], 0, 4);
			$inputMonth = substr($StuInfo['DateInput'], 5, 2);
			$inputDay = substr($StuInfo['DateInput'], 8, 2);
			$StuInfo['DateInput'] = $inputDay.'-'.$inputMonth.'-'.$inputYear;
			$isTwins = $StuInfo["IsTwinsApplied"];
			
			$remarks = $settings[$StuInfo['classLevelID']]['FirstPageContent'];
			$remarks = preg_replace('/font-family:[^;]*;/', '', $remarks);
			$remarks = preg_replace('/face="[^"]*"/', '', $remarks);
				
			$classLevel = $lac->getClassLevel($StuInfo['classLevelID']);
			$classLevel = $classLevel[$StuInfo['classLevelID']];
			
			$interviewGroupID = $lac->getInterviewGroupID($applicationID, $interviewGroupArr);
			//$classLevel = 'Pre-Nursery Class';
			// It's work! but inside ol have ul will fail~
			//			//debug_pr($remarks);
			//			$remarks = str_replace('</ul>','</ul><!!ul!!><!---------1234567890--->',$remarks);
			//			$remarks = str_replace('</ol>','</ol><!!ol!!><!---------1234567890--->',$remarks);
			//			$remarksArr = explode('<!---------1234567890--->',$remarks);
			//			//debug_pr($remarks);
			//			$html_remarks = '';
			//			foreach($remarksArr as $_html){
			//				$detector = substr($_html,-8);
			//				if($detector == '<!!ul!!>'){
			//					$a = str_replace('<!!ul!!>','',$_html);
			//					$a = str_replace('<li>','<li class="ulli">',$a);
			//				}
			//				else if($detector == '<!!ol!!>'){
			//					$a = str_replace('<!!ol!!>','',$_html);
			//					$a = str_replace('<li>','<li class="olli">',$a);
			//				}
			//				else{
			//					$a = $_html;
			//				}
			//				$html_remarks .= $a;
			//			}
			//			$remarks = 	$html_remarks;
			//					$css = '<style>body{ font-family:msjh;}
			//							.olli { list-style-type : 1 ;}
			//							.ulli { list-style-type : disc ;}
			//							</style>';
			//					$mpdf->WriteHTML($css);
			//					$mpdf->WriteHTML($remarks);
				
			# Parent Info
			$GuardianInfoArr = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
			$GuardianInfo = array();
			foreach((array)$GuardianInfoArr as $_guradianInfoArr ){
				if($_guradianInfoArr['type'] == 'F'){
					$Gnum = 1;
				}else if($_guradianInfoArr['type'] == 'M'){
					$Gnum = 2;
				}else{
					$Gnum = 3;
				}
				$GuardianInfo['G'.$Gnum.'ChineseName'] = $_guradianInfoArr['ChineseName'];
				$GuardianInfo['G'.$Gnum.'JobTitle'] = $_guradianInfoArr['JobTitle'];
				$GuardianInfo['G'.$Gnum.'Mobile'] = $_guradianInfoArr['Mobile'];
				$GuardianInfo['G'.$Gnum.'Company'] = $_guradianInfoArr['Company'];
			}
				
			# OtherInfo
			$relativesInfoCustRef = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID,'','REF');
			$relativesInfoCustEx = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID,'','EX');
			$relativesInfoCustCur = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID,'','CUR');
			$relativesInfoCustApply = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID,'','APPLY');
			$tempClassLevel = $lac->getClassLevel($relativesInfoCustApply[0]['OthersRelativeClassPosition']);
			$relativesInfoCustApply[0]['OthersRelativeClassPosition'] = $tempClassLevel[$relativesInfoCustApply[0]['OthersRelativeClassPosition']];
			$tempClassLevel = $lac->getClassLevel($relativesInfoCustApply[1]['OthersRelativeClassPosition']);
			$relativesInfoCustApply[1]['OthersRelativeClassPosition'] = $tempClassLevel[$relativesInfoCustApply[1]['OthersRelativeClassPosition']];
				
			$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
			$othersInfo['ApplyProgram1'] = $othersInfo['ApplyProgram1']=='1'?'International Stream PN-K3 國際課程 PN-K3':($othersInfo['ApplyProgram1']=='2'?'Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':'');
			$othersInfo['ApplyProgram2'] = $othersInfo['ApplyProgram2']=='1'?'International Stream PN-K3 國際課程 PN-K3':($othersInfo['ApplyProgram2']=='2'?'Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':'');
			$othersInfo['ApplyDayType1'] = $othersInfo['ApplyDayType1']=='1'?'Morning 上午':($othersInfo['ApplyDayType1']=='2'?'Afternoon 下午':($othersInfo['ApplyDayType1']=='3'?'Full Day 全日':''));
			$othersInfo['ApplyDayType2'] = $othersInfo['ApplyDayType2']=='1'?'Morning 上午':($othersInfo['ApplyDayType2']=='2'?'Afternoon 下午':($othersInfo['ApplyDayType2']=='3'?'Full Day 全日':''));
			$othersInfo['ApplyDayType3'] = $othersInfo['ApplyDayType3']=='1'?'Morning 上午':($othersInfo['ApplyDayType3']=='2'?'Afternoon 下午':($othersInfo['ApplyDayType3']=='3'?'Full Day 全日':''));
			# Photo
			$attachmentArr = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
			$photoLink = $attachmentArr['personal_photo']['link']?$attachmentArr['personal_photo']['link']:$photo_URL;
				
	
			############################################################### Page 1 Build Here ###########################################
			ob_start();
			include($templatePath);
			$page1 = ob_get_clean();
			############################################################### Page 1 Build End ###########################################
	
			$mpdf->WriteHTML($page1);
	
		}
	
		//			$mpdf->WriteHTML('<ul><li>abc</li><li>def</li></ul>');
		//		echo $page1;
		$mpdf->Output();
	}
	
	function getPrintPageContent($schoolYearID,$applicationID, $type=""){ //using $type="teacher" if the form is print from teacher
	
//		global $PATH_WRT_ROOT,$Lang,$kis_lang, $admission_cfg;
//		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
//		$lac = new admission_cust();
//		if($applicationID != ""){
//		//get student information
//		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
//		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
//		foreach($parentInfo as $aParent){
//			if($aParent['type'] == 'F'){
//				$fatherInfo = $aParent;
//			}
//			else if($aParent['type'] == 'M'){
//				$motherInfo = $aParent;
//			}
//			else if($aParent['type'] == 'G'){
//				$guardianInfo = $aParent;
//			}
//		}
//		
//		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
//		
//		//for the 2 new table
//		$studentInfoCust = $lac->getApplicationStudentInfoCust($schoolYearID,'',$applicationID);
//		$relativesInfoCust = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID);
//		
//		if($_SESSION['UserType']==USERTYPE_STAFF){
//			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
//			if(!is_date_empty($remarkInfo['interviewdate'])){
//				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
//				list($y,$m,$d) = explode('-',$date);
//				if($hour>12){
//					$period = '下午';
//					$hour -= 12;
//				}elseif($hour<12){
//					$period = '上午';
//				}else{
//					$period = '中午';
//				}
//				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
//				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
//				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
//			}else{
//				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
//			上午／下午____時____分';
//			}
//		}
//		else{
//				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
//			上午／下午____時____分';
//		}
//		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
//		$personalPhotoPath = $attachmentList['personal_photo']['link'];
//		$classLevel = $lac->getClassLevel();
//		
////		debug_pr($studentInfo);
////		debug_pr($fatherInfo);
////		debug_pr($motherInfo);
////		debug_pr($guardianInfo);
////		debug_pr($othersInfo);
//		
//		for($i=1; $i<=3; $i++){
//				if($othersInfo['ApplyDayType'.$i] != 0){
//					//$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
//					$dayTypeOption .= "(選擇 ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
//				}
//			}
//		
//		foreach($admission_cfg['BirthCertType'] as $_key => $_type){
//			if($studentInfo['birthcerttype'] == $_type){
//				$birth_cert_type_selection .= ' (';
//				$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
//				$birth_cert_type_selection .= ') ';
//				break;
//			}
//		}
//		
//		$stuNameArr_en = explode(',',$studentInfo['student_name_en']);
//		$stuNameArr_b5 = explode(',',$studentInfo['student_name_b5']);
//		
//		//Header of the page		
//		$x ='<div id="content">
//			<div class="top">
//			<div class="logo"><img src="/includes/admission/eclassk.munsang.edu.hk/images/logo01.png" width="115px"/></div>  
//			<span class="heading">
//			<em>
//			民生書院幼稚園<br />
//			Munsang College Kindergarten<br />
//			'.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'-'.
//					date('Y',getEndOfAcademicYear('',$lac->getNextSchoolYearID())).' 年度<br />
//			</em>
//			報名表<br />
//			Application Form
//			</span>
//			<span class="logo02"><img src="/includes/admission/eclassk.munsang.edu.hk/images/logo02.png" width="70px"/></span>
//			<div class="application_form_no_block">
//			<div class="chinese">報名表編號:</div>
//			<div class="eng">Application Form No.</div>
//			<div class="eng"><span class="underline" style="width:50px;">'.$othersInfo['applicationID'].'</span></div>
//			<div class="chinese">出生証明文件:</div>
//			<div class="eng">Birth Cert:</div>
//			<div class="eng"><span class="underline" style="width:100px;"></span></div>
//			<div class="chinese">備註:</div>
//			<div class="eng">Remark:</div>
//			<div class="eng"><span class="underline" style="width:110px;"></span></div>
//			<div class="school">(只供校方填寫)<br />School use only</div>
//			</div>
//			</div> <!--end_top-->
//			
//			<div style="clear:both"></div>
//			<div class="application_form_block">
//			<table class="application_form_table_class">
//			  <tr>
//			    <td width="65" rowspan="3" style="border:none; line-height:16px;">申請班級&nbsp;*<br/> Class<br/>Applied For</td>
//			    <td width="119" rowspan="3" class="title">幼兒班<br />Nursery Class</td>
//			    <td width="242"> 
//			    <span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Cantonese A.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
//				&nbsp;&nbsp;上午英粵班&nbsp;&nbsp; Eng / Cantonese A.M.    </td>
//			    <td width="329" rowspan="3"  style="border:none; border-left:1px solid #000000;">如申請的學習時段額滿，是&nbsp; 
//			    
//			    <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> / &nbsp;否&nbsp;  <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span><br />願意由學校重新編配。<br />If the session I have applied for is full,<br /> 
//			    I will &nbsp;
//			       <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>  / &nbsp;will not &nbsp; <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
//			      consider the alternative session.</td>
//			    </tr>
//			  <tr>
//			    <td >
//				<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Cantonese P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
//			         &nbsp;&nbsp;下午英粵班&nbsp;&nbsp; Eng / Cantonese P.M.    </td>
//			    </tr>
//			  <tr>
//			    <td >
//				<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Mandarin P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>       
//			        &nbsp;&nbsp;下午英普班&nbsp;&nbsp; Eng / Mandarin P.M.    </td>
//			    </tr>
//			</table>
//			
//			
//			
//			<table class="application_form_table_information"> 
//			  <tr>
//			    <td width="84" rowspan="2" class="border_t_l_r_none" style="text-align:center;">姓名&nbsp;*<br />
//			      Name</td>
//			    <td width="58" class="border_t_r_none">中文<br />
//			      Chinese</td>
//			    <td colspan="3" class="border_t_l_r_none">'.$stuNameArr_b5[0].' '.$stuNameArr_b5[1].'</td>
//			    <td width="180" rowspan="6" class="border_t_r_b_none" ><div class="photo"><img src="'.$personalPhotoPath.'" style="height:100%;max-width: 4cm;max-height: 4.5cm" /><!--<span class="photo_txt">近照<br />Photo</span>--></div></td>
//			  </tr>
//			  <tr>
//			    <td class="border_t_r_none">英文<br />
//			      English</td>
//			    <td colspan="3" class="border_t_l_r_none">'.$stuNameArr_en[0].' '.$stuNameArr_en[1].'</td>
//			    </tr>
//			  <tr>
//			    <td colspan="2" class="border_t_l_r_none">出生日期&nbsp;*<br />Date of Birth</td>
//			    <td class="border_t_l_r_none">'.substr($studentInfo['dateofbirth'], 0, 4).' 年 '.substr($studentInfo['dateofbirth'], 5, 2).' 月 '.substr($studentInfo['dateofbirth'], 8, 2).' 日</td>
//			    <td width="68" colspan="-2"  class="border_t_r_none">性別&nbsp;*<br />
//			      Sex</td>
//			    <td width="140"  class="border_t_l_r_none">'.$Lang['Admission']['genderType'][$studentInfo['gender']].'<br/>'.$studentInfo['gender'].'</td>
//			  </tr>
//			  <tr>
//			    <td colspan="2" class="border_t_l_r_none">出生地點&nbsp;*<br />Place of Birth</td>
//			    <td class="border_t_l_r_none">'.$studentInfo['placeofbirth'].'</td>
//			    <td colspan="-2"  class="border_t_r_none">宗教<br />Religion</td>
//			    <td  class="border_t_l_r_none">'.$studentInfo['religion'].'</td>
//			  </tr>
//			  <tr>
//			    <td colspan="2" class="border_t_l_r_none">出生證明書號碼&nbsp;*<br />Birth Certificate Number</td>
//			    <td width="217" class="border_t_l_r_none">'.$studentInfo['birthcertno'].'</td>
//			    <td colspan="-2"  class="border_t_r_none">電話&nbsp;*<br />Telephone</td>
//			    <td  class="border_t_l_r_none">'.$studentInfo['homephoneno'].'</td>
//			  </tr>
//			  <tr>
//			    <td class="border_none">地址&nbsp;*<br />Address</td>
//			    <td colspan="4" class="border_none">'.$studentInfo['homeaddress'].'</td>
//			    </tr>
//			</table>
//			
//			
//			<table class="application_form_table_parents">
//			  <tr>
//			    <td width="145"  class="border_t_l_r_none">&nbsp;</td>
//			    <th width="272"  class="border_t_r_none">父親 Father</th>
//			    <th width="276"  class="border_t_r_none">母親 Mother</th>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">姓名&nbsp;*<br />Name
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['parent_name_b5'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['parent_name_b5'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">公司<br />Company
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['companyname'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['companyname'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">地址<br />Address
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['companyaddress'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['companyaddress'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">職業<br />Occupation
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['occupation'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['occupation'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">手提電話&nbsp;*<br />Mobile Phone No.
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['mobile'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['mobile'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">電郵地址<br />E-mail Address
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['email'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['email'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">教育程度<br />Level of Education
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['levelofeducation'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['levelofeducation'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_none">學校名稱<br />Name of School
//			</td>
//			    <td class="border_t_r_b_none"><center>'.$fatherInfo['lastschool'].'&nbsp;</center></td>
//			    <td  class="border_t_r_b_none"><center>'.$motherInfo['lastschool'].'&nbsp;</center></td>
//			  </tr>
//			</table>
//			
//			<table  class="application_form_table_record">
//			  <tr>
//			    <td colspan="3" class="title border_t_l_r_none">幼兒過去入學記錄 Record of Previous Schooling of the Child*</td>
//			    </tr>
//			  <tr>
//			    <th width="159"  class="border_t_l_r_none">年份 Year</th>
//			    <th width="160" class="border_t_r_none">級別 Class</th>
//			    <th width="440"  class="border_t_r_none">學校名稱 Name of School</th>
//			    </tr>';
//			    
//			  	foreach($studentInfoCust as $aStudentInfoCust){
//					if(trim($aStudentInfoCust['OthersPrevSchYear']) != '' || trim($aStudentInfoCust['OthersPrevSchClass']) != '' || trim($aStudentInfoCust['OthersPrevSchName']) != '')
//					$x .= '<tr>
//							<td class="border_t_l_r_none"><center>'.$aStudentInfoCust['OthersPrevSchYear'].'&nbsp;</center></td>
//							<td class="border_t_r_none"><center>'.$aStudentInfoCust['OthersPrevSchClass'].'&nbsp;</center></td>
//							<td class="border_t_r_none"><center>'.$aStudentInfoCust['OthersPrevSchName'].'&nbsp;</center></td>
//						</tr>';
//				}
//				
//			$x.='</table>
//			
//			<table  class="application_form_table_record">
//			  <tr>
//			    <td colspan="4" class="title border_t_l_r_none" style="text-align:left;">請列出曾經或現正在本校就讀/工作的親屬資料 (如有)<br />
//			Please list all the relatives who have studying / working or having studied /worked at our College (if applicable)
//			</td>
//			    </tr>
//			  <tr>
//			    <th width="131" class="border_t_l_r_none">年份 Year</th>
//			    <th width="133"  class="border_t_r_none">姓名 Name</th>
//			    <th width="191"  class="border_t_r_none">就讀班別 / 職位<br />
//			      Class / Position
//			</th>
//			    <th width="300"  class="border_t_r_none">與申請人的關係<br />
//			      Relationship with the Applicant
//			</th>
//			  </tr>';
//			  
//				$hasRelativesInfoCust = 0;
//				foreach($relativesInfoCust as $aRelativesInfoCust){
//					if(trim($aRelativesInfoCust['OthersRelativeStudiedYear']) != '' || trim($aRelativesInfoCust['OthersRelativeStudiedName']) != '' || trim($aRelativesInfoCust['OthersRelativeClassPosition']) != '' || trim($aRelativesInfoCust['OthersRelativeRelationship']) != ''){
//						$x .= '<tr>
//							<td class="border_t_l_r_none"><center>'.$aRelativesInfoCust['OthersRelativeStudiedYear'].'</center></td>
//							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeStudiedName'].'</center></td>
//							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeClassPosition'].'</center></td>
//							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeRelationship'].'</center></td>
//						</tr>';
//						$hasRelativesInfoCust++;
//					}
//				}
//				if($hasRelativesInfoCust < 2){
//					if($hasRelativesInfoCust == 0){
//						$x .= '<tr>
//							    <td  class="border_t_l_r_none">&nbsp;</td>
//							    <td  class="border_t_r_none">&nbsp;</td>
//							    <td  class="border_t_r_none">&nbsp;</td>
//							    <td  class="border_t_r_none">&nbsp;</td>
//							  </tr>';
//					}
//					$x .= '<tr>
//						    <td  class="border_none">&nbsp;</td>
//						    <td  class="border_t_r_b_none">&nbsp;</td>
//						    <td  class="border_t_r_b_none">&nbsp;</td>
//						    <td  class="border_t_r_b_none">&nbsp;</td>
//						  </tr>';
//				}
//				 
//			$x.='</table>
//			
//			<div class="footer">
//			<span>
//			根據個人資料(私穩)條例，以上個人資料只用於報讀幼稚園，完成報名程序後，所有資料將會註銷。<br />
//			According to the Personal Data (Privacy) Ordinance, the above Personal data will be used for Kindergarten application only. All data will be written off after application procedure completed.</span>
//			
//			<span>
//			備註Note:
//			<ul>
//			<li>必須提供標示*的資料，否則申請將不能進行，並視作放棄論。<br />
//			   The * marked information are compulsory and must be provided in order for this application to be processed. Failure to provide such data will be treated as withdrawn.
//			</li>
//			<li>沒有標示*的資料為非必要資料，家長/申請人可自行決定提供與否。<br />
//			   Information not marked with * are optional. Parent/Applicant can decide whether or not to provide the information.
//			</li>
//			</ul>
//			</span>
//			</div>
//			</div><!--end_application_form_block-->
//			</div>';	
//			return $x;
//		}
//		else
//			return false;
	}
	
	function getPrintPageCss(){
//		return '<style type="text/css">
//			body { padding:0; margin:0; }
//			body{	font-family: "Times New Roman", Times, serif,"新細明體" "細明體", "微軟正黑體";color: #000000;}
//			#content{ width:21cm; height:29.7cm; background-color:#fff;  margin:0 auto;}
//			.top{ padding:0; float:left; width:21cm;}
//			.logo{width:115px; height:120px; float:left; margin-right:1.8cm; }
//			.logo02{width:80px; height:95px; float:left; }
//			/*--top--*/
//			.top .heading{float:left; margin-right:1.2cm; text-align:center; line-height:23px;font-size:1.2em;}
//			.top .heading em{font-size:1.1em; line-height:28px; font-style:normal}
//			/*--application_form_no_block--*/
//			.application_form_no_block{ float:right; padding:5px 10px 3px 10px; border:2px dotted #000000;}
//			.application_form_no_block .chinese{font-size:0.75em; clear:both; display:block;}
//			.application_form_no_block .eng{font-size:0.7em; float:left; margin-bottom:3px; line-height:15px;}
//			.application_form_no_block .school{font-size:0.65em; clear:both; display:block; text-align:center;}
//			.underline { border-bottom:1px solid #666;color:#000; float:left; line-height:15px; font-size:1.3em; padding:0px 5px 2px 5px; display:block; height:12px;}
//			.application_form_block{clear:both; border:1px solid #000; width:100%; margin-top:5px;}
//			/* -----form_table_class----*/
//			table.application_form_table_class{  border-collapse:collapse; border:1px solid #666; border-bottom:none; font-size:0.8em; margin:10px auto 0px auto; width:20.5cm; height:20px;  }
//			.application_form_table_class td {font-weight: normal;color: #000;text-align:left; line-height:15px;padding:2px 5px 3px 5px; height:15px;}
//			.application_form_table_class td.title {font-weight:bold; color: #000;text-align:center;}
//			.photo{border:1px dotted #000; width:4cm; height:4.5cm; display:block; margin:auto; text-align:center;}
//			.photo_txt{margin:60px auto; display: inline-block; width:50px; text-align:center; line-height:22px; font-size:1.2em;}
//			
//			/* -----form_table_information----*/
//			table.application_form_table_information{ border-collapse:collapse; border:1px solid #666; font-size:0.8em;  margin:0px auto 0px auto; width:20.5cm;}
//			.application_form_table_information th {background-color: #f5c567; font-weight: bold; color: #000;text-align:left;  padding:10px; border:1px solid #666; }
//			.application_form_table_information td {font-weight: normal;color: #000;text-align:left; line-height:15px; border:1px solid #666; padding:3px 5px 2px 5px;}
//			.application_form_table_information td.title {font-weight: normal; color: #000;text-align:center;}
//			
//			/*-----form_table_parents----*/
//			table.application_form_table_parents{ border-collapse:collapse; border:1px solid #666; font-size:0.8em;  margin:10px auto 0px auto; width:20.5cm;}
//			.application_form_table_parents th {background-color: #f5c567; font-weight: normal; color: #000;text-align:center;  padding:10px 5px 10px 5px; border:1px solid #666; background-color:#e2e2e2;}
//			.application_form_table_parents td {font-weight: normal;color: #000;text-align:left; line-height:15px; border:1px solid #666;  padding:3px 5px 2px 5px;}
//			.application_form_table_parents td.title {text-align:center; padding:3px 5px 2px 5px;}
//			
//			/*-----form_table_record----*/
//			table.application_form_table_record{ border-collapse:collapse; border:1px solid #666; font-size:0.9em;  margin:10px auto 5px auto; width:20.5cm;}
//			.application_form_table_record th {background-color: #e2e2e2; font-weight:normal; color: #000;text-align:center;  padding:2px 5px 2px 5px; border:1px solid #666;  line-height:15px; font-size:0.9em;}
//			.application_form_table_record td {font-weight: normal;color: #000;text-align:left; line-height:13px; border:1px solid #666;  padding:3px 5px 3px 5px; font-size:0.9em; height:20px;}
//			.application_form_table_record td.title {text-align:center; padding:3px 5px 2px 5px;  line-height:13px;}
//			
//			/*------check_box-----*/
//			.check_box{ width:11px; height:11px; border:1px solid #1f1f1f; display:inline-block; vertical-align:middle; }			
//
//			/*----footer---*/
//			.footer{font-weight: normal;color: #000;text-align:left; line-height:12px; width:20.5cm; font-size:0.7em;  margin:7px auto 7px auto;}
//			.footer span{ margin-bottom:7px; display:block; line-height:15px;}
//			.footer ul{ margin:0; padding-left:15px;}
//			.footer li{list-style:decimal; }
//			/*---border----*/
//			td.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
//			th.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
//			td.border_t_r_none{border-top:none;border-right:none;}
//			th.border_t_r_none{border-top:none;border-right:none;}
//			td.border_t_r_b_none{border-top:none;border-right:none;border-bottom:none;}
//			td.border_none{border:none;}
//			@media print
//			{    
//			    .print_hide, .print_hide *
//			    {
//			        display: none !important;
//			    }
//			}
//			</style>';
	}
	
	function getDisableCheckBox($targetElementID, $displayString){
		
		return '<input type="checkbox" id="disable'.$targetElementID.'" name="disable'.$targetElementID.'" value="1" onclick="disableNotApplicable(\''.$targetElementID.'\')"><label for="disable'.$targetElementID.'">'.$displayString.'</label>'; 
	}
	
	
	function getSelectByConfig($config_array,$select_name="",$selected_value="",$withChiAndEng=1){
		global $Lang;
		if(empty($Lang['Admission']['RMKG'])){
			global $kis_lang;
			$Lang['Admission']['RMKG'] = $kis_lang['Admission']['RMKG'];
		}
		if(empty($Lang['Admission']['MINGWAI'])){
			global $kis_lang;
			$Lang['Admission']['MINGWAI'] = $kis_lang['Admission']['MINGWAI'];
		}
		
		foreach((array)$config_array as $_key => $value){
			$result_array[$value] = $Lang['Admission']['MINGWAI']['onlineform'][$_key];
			if($result_array[$value] == ''){
				$result_array[$value] = $kis_lang['Admission']['MINGWAI']['onlineform'][$_key];
			}
			
		    if(!$result_array[$value]){
    			$result_array[$value] = $Lang['Admission']['RMKG']['onlineform'][$_key];
    			if($result_array[$value] == ''){
    				$result_array[$value] = $kis_lang['Admission']['RMKG']['onlineform'][$_key];
    			}
		    }
		}
		$PlsSelectLang = $Lang['Admission']['RMKG']['onlineform']['PleaseSelect'];
		//$tags, $selected="", $all=0, $noFirst=0, $FirstTitle="",
		return getSelectByAssoArray($result_array,'name="'.$select_name.'" id="'.str_replace('[]','',$select_name).'" ',$selected_value,0,0,$PlsSelectLang);
	}
	
	function getPrevClassSelection($id,$selected_value=''){
		global $Lang,$admission_cfg;
		
		foreach((array)$admission_cfg['classSelect'] as $_classlevel => $_numClass){
			$langClassLevel = $Lang['Admission']['RMKG']['onlineform']['classtypeEn'][$_classlevel].' '.$Lang['Admission']['RMKG']['onlineform']['classtype'][$_classlevel];
			
			for($i=0;$i<$_numClass;$i++){
				if($_classlevel == 4 && $i == 1){
					$selectAssoArr[$langClassLevel][$_classlevel.'|||'.$i] = $Lang['Admission']['RMKG']['onlineform']['classnameprefixEn'][4]
																			.' '
																			.$Lang['Admission']['RMKG']['onlineform']['classnamesuffixEn'][2]
																			.' '
																			.$Lang['Admission']['RMKG']['onlineform']['classnameprefix'][4]
																			.$Lang['Admission']['RMKG']['onlineform']['classnamesuffix'][2];
				}
				else{
					$langClassNamePre= $Lang['Admission']['RMKG']['onlineform']['classnameprefix'][$_classlevel];
					$langClassNameSuf = $Lang['Admission']['RMKG']['onlineform']['classnamesuffix'][$i];
					$langClassNamePreEn = $Lang['Admission']['RMKG']['onlineform']['classnameprefixEn'][$_classlevel];
					$langClassNameSufEn = $Lang['Admission']['RMKG']['onlineform']['classnamesuffixEn'][$i];
					
					$selectAssoArr[$langClassLevel][$_classlevel.'|||'.$i] = $langClassNamePreEn.' '.$langClassNameSufEn.' '.$langClassNamePre.$langClassNameSuf;
				}
			}
		}
		$langGrad = $Lang['Admission']['RMKG']['onlineform']['classtypeEn'][5].' '.$Lang['Admission']['RMKG']['onlineform']['classtype'][5];
		$langOther = $Lang['Admission']['RMKG']['onlineform']['others'];
		$selectAssoArr[$langOther]['5|||0'] = $langGrad;
		$selectAssoArr[$langOther]['6|||0'] = 'KinderU';
		$PlsSelectLang = $Lang['Admission']['RMKG']['onlineform']['PleaseSelect'];
		
		return getSelectByAssoArray($selectAssoArr,'name="'.$id.'" id="'.str_replace('[]','',$id).'"',$selected_value,0,0,$PlsSelectLang);
	}
	
	function getPrevClassValueLang($string){
		if($string!=''){
			global $Lang;
			if(empty($Lang['Admission']['RMKG'])){
				global $kis_lang;
				$Lang['Admission']['RMKG'] = $kis_lang['Admission']['RMKG'];
			}
			
			if($string == '4|||1'){
				$returnValue = $Lang['Admission']['RMKG']['onlineform']['classtypeEn'][4].' '.$Lang['Admission']['RMKG']['onlineform']['classtype'][4]
								.' - '
								.$Lang['Admission']['RMKG']['onlineform']['classnameprefixEn'][4]
								.' '
								.$Lang['Admission']['RMKG']['onlineform']['classnamesuffixEn'][2]
								.' '
								.$Lang['Admission']['RMKG']['onlineform']['classnameprefix'][4]
								.$Lang['Admission']['RMKG']['onlineform']['classnamesuffix'][2];
				return $returnValue;
			}
			
			$classArr = explode('|||',$string);
			$classlevelValue = $classArr[0];
			$classnameValue = $classArr[1];
			
			if($classlevelValue == 5){
				$returnValue = $Lang['Admission']['RMKG']['onlineform']['classtypeEn'][5].' '.$Lang['Admission']['RMKG']['onlineform']['classtype'][5];
			}
			else if($classlevelValue == 6){
				$returnValue = 'KinderU';
			}
			else{
				$returnValue = 	$Lang['Admission']['RMKG']['onlineform']['classtypeEn'][$classlevelValue].' '.$Lang['Admission']['RMKG']['onlineform']['classtype'][$classlevelValue]
								.' - '
								.$Lang['Admission']['RMKG']['onlineform']['classnameprefixEn'][$classlevelValue]
								.' '
								.$Lang['Admission']['RMKG']['onlineform']['classnamesuffixEn'][$classnameValue]
								.' '
								.$Lang['Admission']['RMKG']['onlineform']['classnameprefix'][$classlevelValue]
								.$Lang['Admission']['RMKG']['onlineform']['classnamesuffix'][$classnameValue];
			}
		}
		return $returnValue;
	}
	
	function getSchoolYearSelect($id,$selected_value=''){
		global $Lang;
		
		$firstYear = '1995';
		//$academicYearEndDate = date('Y',getEndOfAcademicYear());
		$today = date('Y');
		
		for($year = $firstYear; $year<$today ; $year++){
			$schoolYearArr[$year.'-'.($year+1)] = $year.'-'.($year+1);
		}
		$PlsSelectLang = $Lang['Admission']['RMKG']['onlineform']['PleaseSelect'];
		return getSelectByAssoArray($schoolYearArr, 'name="'.$id.'" id="'.str_replace('[]','',$id).'"',$selected_value,0,0,$PlsSelectLang);
	}
	
	var $IsConfirm;
	function getFormElement($ElementName,$Section=''){
		$IsConfirm = $this->IsConfirm;
		global $admission_cfg,$intranet_root;
		global $Lang;
		
		$formElementArray = array(
			"ApplyDayType" => array( $this->getSelectByConfig($admission_cfg['classtype'],'ApplyDayType') ),
			"OthersIsConsiderAlternative" => array(
				$this->Get_Radio_Button('OthersIsConsiderAlternative1', 'OthersIsConsiderAlternative', 'Y', '0','','Yes '.$Lang['Admission']['yes']).'&nbsp;&nbsp;'
				.$this->Get_Radio_Button('OthersIsConsiderAlternative2', 'OthersIsConsiderAlternative', 'N', '0','','No '.$Lang['Admission']['no'])
			),
			"FormStudentInfo_StuChiName" => array(
			    '<input name="studentsname_b5" type="text" id="studentsname_b5" class="textboxtext" />'
			),
			"FormStudentInfo_StuEngName"  => array(
			    '<input name="studentsname_en" type="text" id="studentsname_en" class="textboxtext" />'
			),
			"FormStudentInfo_StuGender" => array($this->Get_Radio_Button('FormStudentInfo_StuGender1', 'FormStudentInfo_StuGender', 'M', '0','', 'M '.$Lang['Admission']['genderType']['M']).'&nbsp;&nbsp;'
												.$this->Get_Radio_Button('FormStudentInfo_StuGender2', 'FormStudentInfo_StuGender', 'F', '0','', 'F '.$Lang['Admission']['genderType']['F'])),
			"StudentBirthCertNo" => array( '<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px">' ),
			"FormStudentInfo_DOB"  => array( '<input name="FormStudentInfo_DOB" type="text" id="FormStudentInfo_DOB" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)'),
			"FormStudentInfo_Enroll_Date"  => array( '<input name="FormStudentInfo_Enroll_Date" type="text" id="FormStudentInfo_Enroll_Date" class="textboxtext" maxlength="10" size="15" style="width:200px;"/>(YYYY-MM-DD)'),
			"FormStudentInfo_Nationality"  => array( '<input name="FormStudentInfo_Nationality" type="text" id="FormStudentInfo_Nationality" class="textboxtext"/>'),
			"FormStudentInfo_POB" => array( '<input name="FormStudentInfo_POB" type="text" id="FormStudentInfo_POB" class="textboxtext"/>'),
    		"HomeAddrRoom" => array( '<input type="text" name="HomeAddrRoom" class="textboxtext" id="HomeAddrRoom">' ),
    		"HomeAddrFloor" => array( '<input type="text" name="HomeAddrFloor" class="textboxtext" id="HomeAddrFloor" style="width:139px;">' ),
    		"HomeAddrBlock" => array( '<input type="text" name="HomeAddrBlock" class="textboxtext" id="HomeAddrBlock" style="width:139px;">' ),
    		"HomeAddrBlding" => array( '<input type="text" name="HomeAddrBlding" class="textboxtext" id="HomeAddrBlding">' ),
    		"HomeAddrStreet" => array( '<input type="text" name="HomeAddrStreet" class="textboxtext" id="HomeAddrStreet">' ),
    		"HomeAddrDistrict" => array ( '<input type="text" name="HomeAddrDistrict" class="textboxtext" id="HomeAddrDistrict">' ),
    		"HomeAddrRegion" => array ( $this->getSelectByConfig($admission_cfg['addressdistrict'],'HomeAddrRegion') ),
// 			"StudentHomeAddress" => array ('<input type="text" name="StudentHomeAddress" class="textboxtext" id="StudentHomeAddress">'),
			"FormStudentInfo_HomeTel" => array( '(852) <input name="FormStudentInfo_HomeTel" type="text" id="FormStudentInfo_HomeTel" class="textboxtext" maxlength="8" style="width: calc(99% - 50px);">' ),
			"FormStudentInfo_Fax" => array( '<input name="FormStudentInfo_Fax" type="text" id="FormStudentInfo_Fax" class="textboxtext" maxlength="8" style="width: calc(99% - 50px);">' ),
			"PrimaryEmail" => array( '<input name="PrimaryEmail" type="text" id="PrimaryEmail" class="textboxtext" style="width: calc(99% - 141px);" >' ),
			"SecondaryEmail" => array( '<input name="SecondaryEmail" type="text" id="SecondaryEmail" class="textboxtext" style="width: calc(99% - 141px);" >' ),
    		"PrimaryEmailRole" => array( '
    		    <select id="PrimaryEmailRole" name="PrimaryEmailRole">
    		        <option value="F">Father '.$Lang['Admission']['PG_Type']['F'].'</option>
    		        <option value="M">Mother '.$Lang['Admission']['PG_Type']['M'].'</option>
    		        <option value="G">Guardian '.$Lang['Admission']['PG_Type']['G'].'</option>
    		    </select>
    		' ),
    		"SecondaryEmailRole" => array( '
    		    <select id="SecondaryEmailRole" name="SecondaryEmailRole">
    		        <option value="F">Father '.$Lang['Admission']['PG_Type']['F'].'</option>
    		        <option value="M">Mother '.$Lang['Admission']['PG_Type']['M'].'</option>
    		        <option value="G">Guardian '.$Lang['Admission']['PG_Type']['G'].'</option>
    		    </select>
    		' ),
			"SpokenLanguage" => array( 
			    $this->getSelectByConfig($admission_cfg['lang'],'SpokenLanguage').'
			    <span id="SpokenLanguageOtherDiv" style="display:none;">
			    (
			    '.$Lang['Admission']['PleaseSpecify'].': 
			    <input name="SpokenLanguageOther" type="text" id="SpokenLanguageOther" class="textboxtext" style="width:150px;" >
			    )
			    </span>'
			),
			"Twins" => array( 
			    '<input type="radio" value="1" id="twinsY" name="twins">
    			<label for="twinsY"> '.$Lang['Admission']['yes'].' Yes</label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="twinsN" name="twins" checked>
    			<label for="twinsN"> '.$Lang['Admission']['no'].' No</label>'
			),
			"TwinsID" => array( 
			    '<input name="twinsapplicationid" type="text" id="twinsapplicationid" class="textboxtext" onkeypress="$(\'#twinsY\').attr(\'checked\', \'checked\')"/>'
			),
			"G1ChineseName"  => array( '<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" />'),
			"G1Nationality" => array( '<input name="G1Nationality" type="text" id="G1Nationality" class="textboxtext" />' ),
			"G1Occupation" => array( '<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />' ),
			"G1CompanyName" => array( '<input name="G1CompanyName" type="text" id="G1CompanyName" class="textboxtext" />' ),
			//"G1WorkNo"  => array( '<input name="G1WorkNo" type="text" id="G1WorkNo" class="textboxtext" maxlength="8" />'),
			"G1MobileNo" => array( '(852) <input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" maxlength="8" style="width: calc(99% - 50px);" />' ),
			"G2ChineseName"  => array( '<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" />'),
			"G2Nationality" => array( '<input name="G2Nationality" type="text" id="G2Nationality" class="textboxtext" />' ),
			"G2Occupation" => array( '<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />' ),
			"G2CompanyName" => array( '<input name="G2CompanyName" type="text" id="G2CompanyName" class="textboxtext" />' ),
			//"G2WorkNo"  => array( '<input name="G2WorkNo" type="text" id="G2WorkNo" class="textboxtext" maxlength="8" />'),
			"G2MobileNo" => array( '(852) <input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" maxlength="8" style="width: calc(99% - 50px);" />' ),
			"G3ChineseName"  => array( '<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" />'),
			"G3Nationality" => array( '<input name="G3Nationality" type="text" id="G3Nationality" class="textboxtext" />' ),
			"G3Occupation" => array( '<input name="G3Occupation" type="text" id="G3Occupation" class="textboxtext" />' ),
			"G3CompanyName" => array( '<input name="G3CompanyName" type="text" id="G3CompanyName" class="textboxtext" />' ),
			//"G3WorkNo"  => array( '<input name="G2WorkNo" type="text" id="G2WorkNo" class="textboxtext" maxlength="8" />'),
			"G3MobileNo" => array( '(852) <input name="G3MobileNo" type="text" id="G3MobileNo" class="textboxtext" maxlength="8" style="width: calc(99% - 50px);" />' ),
			"StuMotherTongue" => array($this->getSelectByConfig($admission_cfg['lang'],'StuMotherTongue')),
			"StuSecondTongue" => array($this->getSelectByConfig($admission_cfg['lang'],'StuSecondTongue')),
			"StuThirdTongue" => array($this->getSelectByConfig($admission_cfg['lang'],'StuThirdTongue')),			
//			"StuMotherTongue" => array('<input name="StuMotherTongue" type="text" id="StuMotherTongue" class="textboxtext" />'),
//			"StuSecondTongue" => array('<input name="StuSecondTongue" type="text" id="StuSecondTongue" class="textboxtext" />'),
//			"StuThirdTongue" => array('<input name="StuThirdTongue" type="text" id="StuThirdTongue" class="textboxtext" />'),
			"IsKinderU" => array($this->Get_Radio_Button('IsKinderU1', 'IsKinderU', '1', '0','',$Lang['Admission']['RMKG']['onlineform']['yes']).'&nbsp;&nbsp;'
							  	.$this->Get_Radio_Button('IsKinderU2', 'IsKinderU', '0', '0','',$Lang['Admission']['RMKG']['onlineform']['no'])),
			"OthersRelativeStudiedName1" => array('<input name="OthersRelativeStudiedName1" type="text" id="OthersRelativeStudiedName1" class="textboxtext" />'),
			"OthersRelativeStudiedName2" => array('<input name="OthersRelativeStudiedName2" type="text" id="OthersRelativeStudiedName2" class="textboxtext" />'),
			"OthersRelativeStudiedName3" => array('<input name="OthersRelativeStudiedName3" type="text" id="OthersRelativeStudiedName3" class="textboxtext" />'),
			"OthersRelativeStudiedName4" => array('<input name="OthersRelativeStudiedName4" type="text" id="OthersRelativeStudiedName3" class="textboxtext" />'),
			"OthersRelativeGender1" => array($this->Get_Radio_Button('OthersRelativeGender1_M', 'OthersRelativeGender1', 'M', '0','','M '.$Lang['Admission']['genderType']['M']).'&nbsp;'
										.$this->Get_Radio_Button('OthersRelativeGender1_F', 'OthersRelativeGender1', 'F', '0','','F '.$Lang['Admission']['genderType']['F'])),
			"OthersRelativeGender2" => array($this->Get_Radio_Button('OthersRelativeGender2_M', 'OthersRelativeGender2', 'M', '0','','M '.$Lang['Admission']['genderType']['M']).'&nbsp;'
										.$this->Get_Radio_Button('OthersRelativeGender2_F', 'OthersRelativeGender2', 'F', '0','','F '.$Lang['Admission']['genderType']['F'])),
			"OthersRelativeGender3" => array($this->Get_Radio_Button('OthersRelativeGender3_M', 'OthersRelativeGender3', 'M', '0','','M '.$Lang['Admission']['genderType']['M']).'&nbsp;'
										.$this->Get_Radio_Button('OthersRelativeGender3_F', 'OthersRelativeGender3', 'F', '0','','F '.$Lang['Admission']['genderType']['F'])),
			"OthersRelativeGender4" => array($this->Get_Radio_Button('OthersRelativeGender4_M', 'OthersRelativeGender4', 'M', '0','','M '.$Lang['Admission']['genderType']['M']).'&nbsp;'
										.$this->Get_Radio_Button('OthersRelativeGender4_F', 'OthersRelativeGender4', 'F', '0','','F '.$Lang['Admission']['genderType']['F'])),
			"OthersRelativeAge1" => array('<input name="OthersRelativeAge1" type="text" id="OthersRelativeAge1" class="textboxtext" />'),
			"OthersRelativeAge2" => array('<input name="OthersRelativeAge2" type="text" id="OthersRelativeAge2" class="textboxtext" />'),
			"OthersRelativeAge3" => array('<input name="OthersRelativeAge3" type="text" id="OthersRelativeAge3" class="textboxtext" />'),
			"OthersRelativeAge4" => array('<input name="OthersRelativeAge4" type="text" id="OthersRelativeAge4" class="textboxtext" />'),
			"OthersRelativeClass1" => array($this->getPrevClassSelection('OthersRelativeClass1')),
			"OthersRelativeClass2" => array($this->getPrevClassSelection('OthersRelativeClass2')),
			"OthersRelativeClass3" => array($this->getPrevClassSelection('OthersRelativeClass3')),
			"OthersRelativeClass4" => array($this->getPrevClassSelection('OthersRelativeClass4')),
//			"OthersRelativeClass1" => array('<input name="OthersRelativeClass1" type="text" id="OthersRelativeClass1" class="textboxtext" />'),
//			"OthersRelativeClass2" => array('<input name="OthersRelativeClass2" type="text" id="OthersRelativeClass2" class="textboxtext" />'),
//			"OthersRelativeClass3" => array('<input name="OthersRelativeClass3" type="text" id="OthersRelativeClass3" class="textboxtext" />'),
//			"OthersRelativeClass4" => array('<input name="OthersRelativeClass4" type="text" id="OthersRelativeClass4" class="textboxtext" />'),
			"OthersRelativeStudiedYear1" => array($this->getSchoolYearSelect('OthersRelativeStudiedYear1')),
			"OthersRelativeStudiedYear2" => array($this->getSchoolYearSelect('OthersRelativeStudiedYear2')),
			"OthersRelativeStudiedYear3" => array($this->getSchoolYearSelect('OthersRelativeStudiedYear3')),
			"OthersRelativeStudiedYear4" => array($this->getSchoolYearSelect('OthersRelativeStudiedYear4'))
//			"OthersRelativeStudiedYear1" => array('<input name="OthersRelativeStudiedYear1" type="text" id="OthersRelativeStudiedYear1" class="textboxtext" />'),
//			"OthersRelativeStudiedYear2" => array('<input name="OthersRelativeStudiedYear2" type="text" id="OthersRelativeStudiedYear2" class="textboxtext" />'),
//			"OthersRelativeStudiedYear3" => array('<input name="OthersRelativeStudiedYear3" type="text" id="OthersRelativeStudiedYear3" class="textboxtext" />'),
//			"OthersRelativeStudiedYear4" => array('<input name="OthersRelativeStudiedYear4" type="text" id="OthersRelativeStudiedYear4" class="textboxtext" />')
		);
		$returnValue = '';
		if($IsConfirm){
			global $formData;
			//$returnValue = '(!--'.$ElementName.'--!)';
			
			if($Section == 'Parent'){
				
				switch($ElementName){
					
					default:
						$returnValue = '<center>'.$formData[$ElementName].'</center>';
				}
			}
			else{
				switch($ElementName){
				case 'OthersIsConsiderAlternative':
					$returnValue = ($formData[$ElementName]=='Y'? 'Yes '.$Lang['Admission']['yes']:'No '.$Lang['Admission']['no']);
				break;
				//HomeAddrDistrict
				case 'PrimaryEmailRole':
				case 'SecondaryEmailRole':
					$returnValue = "({$Lang['Admission']['PG_Type'][$formData[$ElementName]]}) ";
				break;
				case 'HomeAddrRegion':
					$returnValue = $this->getSelectedValueLang($formData[$ElementName],$admission_cfg['addressdistrict']);
				break;
				case 'SpokenLanguage':
					$returnValue = $this->getSelectedValueLang($formData[$ElementName],$admission_cfg['lang']);
					if($formData[$ElementName] == $admission_cfg['lang']['others']){
					    $returnValue = $formData['SpokenLanguageOther'];
					}
				break;
				//Name
				case 'FormStudentInfo_StuChiName':
					$returnValue = $formData['studentsname_b5'];
				break;
				case 'FormStudentInfo_StuEngName':
					$returnValue = $formData['studentsname_en'];
			    break;
				case 'Twins':
					$returnValue = ($formData['twins']==1? "Yes {$Lang['Admission']['yes']}":"No {$Lang['Admission']['no']}");
				break;
				case 'TwinsID':
					$returnValue = ($formData['twins']==1? $formData['twinsapplicationid'] : '');
				break;
				break;
				default:
					$returnValue = $formData[$ElementName];
				}

			}
			
		}else{
			
			$returnValue = $formElementArray[$ElementName][0];				
			
		}
		return $returnValue ;
	}
	
	function getSelectedValueLang($selectedValue,$ConfigArr){
		global $Lang,$kis_lang;
				
		foreach((array)$ConfigArr as $_key => $_val){
			if($selectedValue == $_val){
				
				$returnValue = $Lang['Admission']['RMKG']['onlineform'][$_key];
				if($returnValue == ''){
					$returnValue = $kis_lang['Admission']['RMKG']['onlineform'][$_key];
				}
				break;
			}
		}
		return $returnValue;
	}
	
	function getStudentEmailChecked($applicationid){
	    global $kis_lang, $libkis_admission;
	    $sql = "SELECT EmailCheckedDate FROM ADMISSION_OTHERS_INFO WHERE ApplicationID = '".$applicationid."'";
	    $result = $libkis_admission->returnVector($sql);
	    
	    if($result[0] != ''){
	        $dateChecked = $kis_lang['Admission']['yes']." (".$result[0].")";
	    }else{
	        $dateChecked = '--';
	    }
	    
	    return $dateChecked;
	}
	
	function getPayPalButton($ApplicationID){
		global $admission_cfg, $lac;
		$protocol = (checkHttpsWebProtocol())? 'https':'http';
		$ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
		return '<form action="'.$admission_cfg['paypal_url'].'" onsubmit="checkPayment(\''.$ApplicationID.'\');return false;" method="post" target="paypal_payment">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="'.$admission_cfg['hosted_button_id'].'">
				<input type="hidden" name="return" value="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php" /> 
				    <input type="hidden" name="cancel_return" value="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php?cm='.$ApplicationID.'" />
				    <input type="hidden" name="custom" value="'.$ApplicationID.'" />
					<input type="hidden" name="notify_url" value="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_ipn.php" /> 
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();
				        
						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
	}
}
?>