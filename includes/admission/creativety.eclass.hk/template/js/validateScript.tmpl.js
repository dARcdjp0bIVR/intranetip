$(function () {
    window.validator['CUSTOM']['schoolDuration'] = function (fieldName, fieldId, args) {
        var startFieldName = fieldId + '_start';
        var endFieldName = fieldId + '_end';
        var nameField = $('[data-field="' + args.validateField + '"]').attr('id');

        var isValid = true;
        isValid = validator['POSTIVE_NUMBER'](startFieldName) && isValid;
        isValid = validator['POSTIVE_NUMBER'](endFieldName) && isValid;
        isValid = validator['LENGTH'](startFieldName, 4) && isValid;
        isValid = validator['LENGTH'](endFieldName, 4) && isValid;
        isValid = validator['REQUIRED_WITH_FIELD'](startFieldName, nameField) && isValid;
        isValid = validator['REQUIRED_WITH_FIELD'](endFieldName, nameField) && isValid;

        if (isValid) {
            $startField = $('[name="' + startFieldName + '"]');
            $endField = $('[name="' + endFieldName + '"]');
            if(
                parseInt($startField.val()) < 1900 ||
                parseInt($endField.val()) < 1900 ||
                //parseInt($endField.val()) > new Date().getFullYear() ||
                $startField.val() > $endField.val()
            ){
                $startField.closest('.itemInput').addClass('itemInput-warn');
                $startField.closest('.itemInput').find('.remark-warn.date').removeClass('hide').show();
                isValid = false;
            }
        }
        return isValid;
    }
    window.validator['CUSTOM']['schoolClass'] = function (fieldName, fieldId, args) {
        var startFieldName = fieldId + '_start';
        var endFieldName = fieldId + '_end';
        var nameField = $('[data-field="' + args.validateField + '"]').attr('id');

        var isValid = true;
        isValid = validator['REQUIRED_WITH_FIELD'](startFieldName, nameField) && isValid;
        isValid = validator['REQUIRED_WITH_FIELD'](endFieldName, nameField) && isValid;
        return isValid;
    }
    window.validator['CUSTOM']['atLeastOneParent'] = function (fieldName, fieldId, args) {
        var $field = $('[name="' + fieldName + '"]');
        if ($field.length == 0) {
            return true;
        }

        var fatherName = $('[data-field="father"]').val();
        var motherName = $('[data-field="mother"]').val();
        var guardianName = $('[data-field="guardian"]').val();

        var isValid = true;
        isValid = (fatherName + motherName + guardianName).length > 0;

        if (!isValid) {
            $field.closest('.itemInput').addClass('itemInput-warn');
            $field.closest('.itemInput').find('.remark-warn.custom').html('最少輸入一名家長資料 At least input one parent information').removeClass('hide').show();
        }
        return isValid;
    }
});
