<?php
global $libkis_admission, $admission_cfg;

$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
//debug_r($applicationCustInfo);

//// InterviewStatus START ////
$interviewStatusArr = array_flip($admission_cfg['InterviewStatus']);
$interviewStatus = $applicationCustInfo['InterviewStatus'][0]['Value'];
$interviewStatus = ($interviewStatus) ? $interviewStatus : $admission_cfg['InterviewStatus']['NotUpdated'];
$interviewStatus = $interviewStatusArr[$interviewStatus];
//// InterviewStatus END ////

//// AdmitStatus START ////
$admitStatusArr = array_flip($admission_cfg['AdmitStatus']);
$admitStatus = $applicationCustInfo['AdmitStatus'][0]['Value'];
$admitStatus = ($admitStatus) ? $admitStatus : $admission_cfg['AdmitStatus']['NotUpdated'];
$admitStatus = $admitStatusArr[$admitStatus];
//// AdmitStatus END ////

//// RatingLevel START ////
$ratingLevel = $applicationCustInfo['RatingLevel'][0]['Value'];
$ratingLevelStr = $libkis_admission->classLevelAry[$ratingLevel];
//// RatingLevel END ////
?>

<table class="form_table">
    <tbody>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['CREATIVE']['InterviewStatus'][$interviewStatus]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'] ?>
        </td>
        <td>
            <?php
            if ($applicationCustInfo['RegisterEmail'][0]['Value']) {
                echo $kis_lang['Admission']['yes'];
                echo " ({$kis_lang['Admission']['date']}: {$applicationCustInfo['RegisterEmailDate'][0]['Value']})";
            } else {
                echo $kis_lang['Admission']['no'];
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['CREATIVE']['AdmitStatus'][$admitStatus]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['TimeSlot'][$applicationCustInfo['RatingClass'][0]['Value']]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['CREATIVE']['Paid'][$applicationCustInfo['PaidReservedFee'][0]['Value']]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['CREATIVE']['Paid'][$applicationCustInfo['PaidRegistrationCertificate'][0]['Value']]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'] ?>
        </td>
        <td>
            <?php
            if ($applicationCustInfo['BookFeeEmail'][0]['Value']) {
                echo $kis_lang['Admission']['yes'];
                echo " ({$kis_lang['Admission']['date']}: {$applicationCustInfo['BookFeeEmailDate'][0]['Value']})";
            } else {
                echo $kis_lang['Admission']['no'];
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['CREATIVE']['Paid'][$applicationCustInfo['PaidBookFee'][0]['Value']]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'] ?>
        </td>
        <td>
            <?php
            if ($applicationCustInfo['OpeningNoticeEmail'][0]['Value']) {
                echo $kis_lang['Admission']['yes'];
                echo " ({$kis_lang['Admission']['date']}: {$applicationCustInfo['OpeningNoticeEmailDate'][0]['Value']})";
            } else {
                echo $kis_lang['Admission']['no'];
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'] ?>
        </td>
        <td>
            <?= kis_ui::displayTableField($kis_lang['Admission']['CREATIVE']['Paid'][$applicationCustInfo['PaidTuitionFee'][0]['Value']]) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title" width="30%">
            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'] ?>
        </td>
        <td>
            <?php
            if ($applicationCustInfo['GiveUpOffer'][0]['Value']) {
                echo $kis_lang['Admission']['yes'];
            } else {
                echo $kis_lang['Admission']['no'];
            }
            ?>
        </td>
    </tr>
    </tbody>
</table>
