<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(青衣)';
$admission_cfg['SchoolName']['en'] .= ' (Tsing Yi)';
$admission_cfg['SchoolCode'] = 'TY';
$admission_cfg['SchoolPhone'] = '2942 8818';
$admission_cfg['SchoolAddress']['b5'] = '新界青衣寮肚路3號曉峰園第五座平台';
$admission_cfg['SchoolAddress']['en'] = 'Podium, Block 5, Mount Haven, 3 Liu To Road, Tsing Yi, New Territories';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckty.photo@gmail.com';
}
/* for email [end] */


/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = 'RSLNAVWT6KL8C';
} else {
    $admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
    $admission_cfg['paypal_signature'] = 'qUMtjgkdSwXkdXAV2BsscqtmgMN7IWjtqR17NeC0xaEf2fmR3_o21GmfQIC';
    $admission_cfg['hosted_button_id'] = '6TH7STYLUNZSN';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Tsing Yi)';
}
/* for paypal [End] */
