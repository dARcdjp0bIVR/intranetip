<?php 
	//using: 
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */
	 
	//Please also define lang in admission_lang
	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['waitingforinterview']	= 3;
	$admission_cfg['Status']['TBCK_admitted'] = 4;
	$admission_cfg['Status']['TBCK_reserve'] = 6;
	$admission_cfg['Status']['cancelled']	= 5;
	
	$admission_cfg['StatusDisplayOnTable'][1] = 'pending';
	$admission_cfg['StatusDisplayOnTable'][2] = 'paymentsettled';
	$admission_cfg['StatusDisplayOnTable'][3] = 'TBCK_admitted';
	
	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);		
		
	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "SdzDdfStXe3wZmv9b";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 2; //in MB
		
?>