<?php
/*
 *  Using: 
 *  
 *  2020-10-21 Cameron
 *      - modify getStocktakeDoneByLocationUI(), add stocktake instruction for iOS [case #B199460]
 *
 *  2020-04-10 Cameron
 *      - change wording for expected and stocktake qty in getStocktakeDoneByLocationUI() to avoid overlap problem
 *      - add line break for expected qty, stock take qty and variance in getStocktakeDoneByLocationUI() for small device
 *      
 *  2020-04-09 Cameron
 *      - don't show building name in getStocktakeProgressByAdminGroupUI(), getStocktakeProgressByLocationUI() for ej
 *      
 *  2020-03-17 Cameron
 *      - add constructor function libinventory_app_ui()
 *       
 *  2020-03-12 Cameron
 *      - add parameter showNoRecord to getItemListUI()
 *      
 *  2020-03-09 Cameron
 *      - fix character encoding problem (use ascii colon instead of Chinese) in getStocktakeProgressByAdminGroupUI(), getStocktakeProgressByLocationUI(),
 *      getStocktakeProgressOfOneLocationUI(), getStocktakeDoneByLocationUI()
 *      
 *  2020-02-18 Cameron
 *      - always show funding source for bulk item
 *      
 *  2019-12-16 Cameron
 *      - create this file
 */

include_once("libinventory.php");
class libinventory_app_ui extends libinventory
{
    function libinventory_app_ui($init_setting = false)
    {
        parent::__construct($init_setting);
    }
    
    function getJsAjaxError()
    {
        global $Lang;

        $x = "
            function show_ajax_error()
            {
                alert('".$Lang['General']['AjaxError']."');
            }";
        return $x;
    }

    function getStocktakeFilterByUI($selected='')
    {
        global $i_InventorySystem, $Lang;

        $filterAry = array(
            array(
                3,
                $i_InventorySystem['Caretaker']
            ),
            array(
                1,
                $i_InventorySystem['Location']
            )
        );
        $filterSelection = getSelectByArray($filterAry, 'name="filterBy" id="filterBy" class="selectpicker" title="'.$Lang['General']['PleaseSelect'].'"', $selected, $all=0, $noFirst=1);

        return $filterSelection;
    }

    function getLocationSelectionUI($targetLocation='')
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
        include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");
        $llocation_ui = new liblocation_ui();
        
        if ($this->IS_ADMIN_USER()) {
            $locationSelection = $llocation_ui->getBuildingFloorRoomSelectionWithClass($targetLocation, "TargetLocation", "", $ParNoFirst=1, "", "", "", "", "");
        }
        else {
            $groupInChargeLocationAry = $this->getGroupInChargeLocation();
            $locationSelection = $llocation_ui->getBuildingFloorRoomSelectionWithClass($targetLocation, "TargetLocation", "", $ParNoFirst=1, "", "", $groupInChargeLocationAry['BuildingID'], $groupInChargeLocationAry['LevelID'], $groupInChargeLocationAry['LocationID'], $CanSelectWholeFloor=0, $multiple=0);
        }
        return $locationSelection;
    }

    function getBuildingFloorSelectionUI($targetFloor='')
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
        include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");
        $llocation_ui = new liblocation_ui();

        if ($this->IS_ADMIN_USER()) {
            $buildingFloorSelection = $llocation_ui->getBuildingFloorSelectionWithClass($targetFloor, 'TargetFloor', $ParOnchange = '', $ParNoFirst = 1, $ShowNoRoomFloor = 0, $ParTitle = '', '', '');
        }
        else {
            $groupInChargeLocationAry = $this->getGroupInChargeLocation();
            $buildingFloorSelection = $llocation_ui->getBuildingFloorSelectionWithClass($targetFloor, 'TargetFloor', $ParOnchange = '', $ParNoFirst = 1, $ShowNoRoomFloor = 0, $ParTitle = '', $groupInChargeLocationAry['BuildingID'], $groupInChargeLocationAry['LevelID']);
        }
        return $buildingFloorSelection;
    }
    
    function getRoomSelectionUI($targetFloor='', $targetRoom='')
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
        include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");
        $llocation_ui = new liblocation_ui();
        
        if ($this->IS_ADMIN_USER()) {
            $roomSelection = $llocation_ui->getRoomSelectionWithClass($targetFloor, $targetRoom, 'TargetLocation', $ParOnchange = '', $ParNoFirst = 1, $ParTitle = '', $ParIsMultiple = '', '');
        }
        else {
            $groupInChargeLocationAry = $this->getGroupInChargeLocation();
            $roomSelection = $llocation_ui->getRoomSelectionWithClass($targetFloor, $targetRoom, 'TargetLocation', $ParOnchange = '', $ParNoFirst = 1, $ParTitle = '', $ParIsMultiple = '', $groupInChargeLocationAry['LocationID']);
        }
        return $roomSelection;
    }
    
    function getStocktakeProgressByAdminGroupUI($adminGroupID)
    {
        global $Lang, $junior_mck;

        $stocktakeProgressAssoc = $this->getStocktakeProgressByAdminGroup($adminGroupID);
        
        $buidingAssoc = $stocktakeProgressAssoc['Building'];
        $floorAssoc = $stocktakeProgressAssoc['Floor'];
        $roomAssoc = $stocktakeProgressAssoc['Room'];

        $adminGroupAry = $this->returnAdminGroup($adminGroupID);
        $adminGroupName = (count($adminGroupAry) == 1) ? $adminGroupAry[0]['AdminGroupName'] : '';

        ob_start();
?>
            <div class="row">
                <div class="subTitle subTitleFix manGp col-xs-12">
                    <div class="title"><?php echo $adminGroupName;?></div>
                </div>
            </div>
            <ul class="stocktakeList contentFixHeight">

                <?php
                foreach((array)$buidingAssoc as $buildingID => $buildingName):
                    foreach((array)$floorAssoc[$buildingID] as $floorID => $_floorAssoc):
                        $buildingFloorID = 'BF_'.$buildingID.'_'.$floorID;
                        $buildingFloorName = '';
                        if (!$junior_mck) {
                            $buildingFloorName .= $buildingName.' > ';
                        }
                        $buildingFloorName .= $_floorAssoc['FloorName'];
                        if ($_floorAssoc['NumberIncomplete']) {
                            $ariaExpanded = 'true';
                            $class = 'alert';
                        }
                        else {
                            $ariaExpanded = 'false';
                            $class = 'collapsed';
                        }
                        ?>
                        <li class="loacation">
                            <a data-toggle="collapse" href="#<?php echo $buildingFloorID;?>" role="button" aria-expanded="<?php echo $ariaExpanded;?>" aria-controls="<?php echo $buildingFloorID;?>" class="<?php echo $class;?>"><?php echo $buildingFloorName;?></a>
                            <div class="panel-collapse collapse in" id="<?php echo $buildingFloorID;?>">
                                <ul class="process locationList">

                                    <?php foreach((array)$roomAssoc[$buildingID][$floorID] as $_locationID=>$_roomAssoc):?>
                                        <?php if ($_roomAssoc['NumberIncomplete'] > 0):?>
                                            <a class="incomplete" href="#" data-LocationID="<?php echo $_locationID;?>" data-AdminGroupID="<?php echo $adminGroupID;?>">
                                                <li>
                                                    <div class="locationName"><span><?php echo $_roomAssoc['LocationName'];?></span></div>
                                                    <div class="processInfo">
                                                        <div class="row">
                                                            <div class="info col-xs-6">
                                                                <span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTakeStatus']['Complete'];?> : </span> <?php echo $_roomAssoc['NumberComplete'];?>
                                                            </div>
                                                            <div class="info alert col-xs-6">
                                                                <span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTakeStatus']['Incomplete'];?> : </span> <?php echo $_roomAssoc['NumberIncomplete'];?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </a>
                                        <?php else:?>
                                            <a class="complete" href="#" data-LocationID="<?php echo $_locationID;?>" data-AdminGroupID="<?php echo $adminGroupID;?>">
                                                <li>
                                                    <div class="locationName"><span><?php echo $_roomAssoc['LocationName'];?></span></div>
                                                    <div class="processInfo">
                                                        <span class="lastUpdate"><?php echo $Lang['eInventory']['eClassApp']['LastStockTake'].' :'.$_roomAssoc['MaxDateInput'].' '.$_roomAssoc['PersonInCharge'];?></span>
                                                    </div>
                                                </li>
                                            </a>

                                        <?php endif;?>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                        </li>
                    <?php
                    endforeach;
                endforeach;
                ?>
            </ul>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getStocktakeProgressByLocationUI($locationID)
    {
        global $PATH_WRT_ROOT, $Lang, $junior_mck;
        include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
        $location = new liblocation();
        
        $locationIDAry = is_array($locationID) ? $locationID : array($locationID);   
        $locationAry = $location->getBuildingFloorRoomNameAry(array($locationID));
        if (count($locationAry)) {
            $locationAry = $locationAry[0];
            $displayLocation = '';
            if (!$junior_mck) {
                $displayLocation .= $locationAry['BuildingName'] . ' > ';
            }
            $displayLocation .= $locationAry['FloorName'] . ' > ' . $locationAry['LocationName'];
        }
        else {
            $displayLocation = '';   
        }
        
        $stocktakeProgressAssoc = $this->getStocktakeProgressByLocation($locationID);
        
        ob_start();
        ?>
            <div class="row">
                <div class="subTitle map subTitleFix col-xs-12">
                    <div class="title"><?php echo $displayLocation;?></div>
                </div>
            </div>
            <ul class="stocktakeList contentFixHeight">

        <?php
            foreach((array)$stocktakeProgressAssoc as $_adminGroupID => $_stocktakeProgressAssoc):
        ?>
                <li class="loacation">
                    <ul class="process groupList">
                    <?php if ($_stocktakeProgressAssoc['NumberIncomplete'] > 0):?>
                        <a class="incomplete" href="#" data-LocationID="<?php echo $locationID;?>" data-AdminGroupID="<?php echo $_adminGroupID;?>">
                            <li>
                                <div class="locationName"><span><?php echo $_stocktakeProgressAssoc['AdminGroupName'];?></span></div>
                                <div class="processInfo">
                                    <div class="row">
                                        <div class="info col-xs-4">
                                            <span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTakeStatus']['Complete'];?> : </span> <?php echo $_stocktakeProgressAssoc['NumberComplete'];?>
                                        </div>
                                        <div class="info alert col-xs-4">
                                            <span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTakeStatus']['Incomplete'];?> : </span> <?php echo $_stocktakeProgressAssoc['NumberIncomplete'];?>
                                        </div>
                                    </div>
                                    <?php if ($_stocktakeProgressAssoc['NumberComplete'] > 0): ?>
                                    	<span class="lastUpdate"><?php echo $Lang['eInventory']['eClassApp']['LastStockTake'].' :'.$_stocktakeProgressAssoc['MaxDateInput'].' '.$_stocktakeProgressAssoc['PersonInCharge'];?></span>
                                    <?php endif;?>
                                </div>
                            </li>
                        </a>
                    <?php else:?>
                        <a class="complete" href="#" data-LocationID="<?php echo $locationID;?>" data-AdminGroupID="<?php echo $_adminGroupID;?>">
                            <li>
                                <div class="locationName"><span><?php echo $_stocktakeProgressAssoc['AdminGroupName'];?></span></div>
                                <div class="processInfo">
                                    <span class="lastUpdate"><?php echo $Lang['eInventory']['eClassApp']['LastStockTake'].' :'.$_stocktakeProgressAssoc['MaxDateInput'].' '.$_stocktakeProgressAssoc['PersonInCharge'];?></span>
                                </div>
                            </li>
                        </a>
                    <?php endif;?>
                    </ul>
                </li>
		<?php
            endforeach;
        ?>
            </ul>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }
    
    function getStocktakeProgressOfOneLocationUI($locationAssoc)
    {
        global $Lang;

        $locationFullName = $this->returnLocationStr($locationAssoc['LocationID']);
        
        ob_start();
?>
        <div class="locationName"><span><?php echo $locationFullName;?></span></div>
        <div class="processInfo">
            <div class="row">
                <div class="info col-xs-6">
                    <span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTakeStatus']['Complete'];?> : </span> <span id="complete"><?php echo $locationAssoc['NumberComplete'];?></span>
                </div>
                <div class="info alert col-xs-6">
                    <span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTakeStatus']['Incomplete'];?> : </span> <span id="incomplete"><?php echo $locationAssoc['NumberIncomplete'];?></span>
                </div>
            </div>
            <span class="lastUpdate" style="display:<?php echo ($locationAssoc['NumberComplete']) ? '' : 'none';?>"><?php echo $Lang['eInventory']['eClassApp']['LastStockTake'].' :'.$locationAssoc['MaxDateInput'].' '.$locationAssoc['PersonInCharge'];?></span>
        </div>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getStocktakeDoneByLocationUI($locationID, $groupInCharge='', $fundingSource='', $itemID='', $itemType='')
    {
        global $Lang, $i_InventorySystem_Stocktake_ExpectedQty, $i_InventorySystem_Stocktake_StocktakeQty, $i_InventorySystem_Stocktake_VarianceQty, $i_InventorySystem_Item_Funding;
        
        $stocktakeDoneByLocationAry = $this->getStocktakeDoneByLocation($locationID, $groupInCharge, $fundingSource, $itemID, $itemType);
        $stocktakeDoneByLocationAssoc = BuildMultiKeyAssoc($stocktakeDoneByLocationAry, array('ItemID','GroupInCharge','FundingSource'));
//debug_pr($stocktakeDoneByLocationAry);
//debug_pr($stocktakeDoneByLocationAssoc);
        $isValidVersion = $this->checkiOSVersion();
        $instruction = $isValidVersion ?  $Lang['eInventory']['eClassApp']['StockTakeInstruction'] : $Lang['eInventory']['eClassApp']['StockTakeInstructionForiOS'];
        ob_start();
?>
<?php if (count($stocktakeDoneByLocationAry) == 0):?>
            <div class="alertMsg" id="StocktakeInstruction">
                <div class="alert bg-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <span><?php echo $instruction;?></span>
                </div>
            </div>
            <span class="info" id="BarcodeList"><?php echo $Lang['eInventory']['eClassApp']['NoStockTakeItem'];?></span>

<?php else:?>        
    <?php foreach((array)$stocktakeDoneByLocationAry as $_stocktakeAry) :?>
    	<?php if ($_stocktakeAry['ItemType'] == ITEM_TYPE_SINGLE) :?>
    			<a href="#" data-ItemID="<?php echo $_stocktakeAry['ItemID'];?>" data-ItemType="<?php echo $_stocktakeAry['ItemType'];?>" data-LocationID="<?php echo $locationID;?>">
        	<?php if ($_stocktakeAry['Action']==ITEM_ACTION_STOCKTAKE_NOT_FOUND) {
                    $liClass = ' notFound';
                    $stocktakeStatus = '<span class="tag notFound">'.$Lang['eInventory']['eClassApp']['StockTakeNotFoundBtn'].'</span>';
        		  }
        		  else {
        		    $liClass = '';
        		    $stocktakeStatus = '';
        		  }
        	?>
    				<li class="item<?php echo $liClass;?>">
    					<div class="itemTitle"><?php echo $_stocktakeAry['ItemName'];?><?php echo $stocktakeStatus;?></div>
    					<?php if ($_stocktakeAry['ShowRecordStatus']):?>
							<div class="processInfo">
								<span class="label"><?php echo $Lang['General']['Status2'];?>:</span><?php echo $Lang['eInventory']['eClassApp']['OnLoan'];?>
							</div>    					
    					<?php endif;?>
    				</li>
    			</a>
    	<?php else :?>
<?php 
$_itemID = $_stocktakeAry['ItemID'];
$_groupInChargeID = $_stocktakeAry['GroupInCharge'];
$displayItemName = $_stocktakeAry['ItemName'];
// if (count($stocktakeDoneByLocationAssoc[$_itemID][$_groupInChargeID]) > 1) {
//     $showFundingSource = 1;
// }
// else {    
//     $showFundingSource = 0;
// }
$displayVariance = $_stocktakeAry['Variance'] > 0 ? '+'.$_stocktakeAry['Variance'] : $_stocktakeAry['Variance'];
?>    				
    			<a href="#" data-ItemID="<?php echo $_itemID;?>" data-ItemType="<?php echo $_stocktakeAry['ItemType'];?>" data-AdminGroupID="<?php echo $_groupInChargeID;?>" data-LocationID="<?php echo $locationID;?>" data-FundingSourceID="<?php echo $_stocktakeAry['FundingSource'];?>" data-ShowFundingSource="<?php //echo $showFundingSource;?>">
    				<li class="item">
    					<div class="itemTitle"><?php echo $displayItemName;?></div>
    					<div class="processInfo">
                    <?php //if ($showFundingSource):?>
							<div class="row">
    							<div class="info col-xs-12">
    								<span class="label"><?php echo $i_InventorySystem_Item_Funding;?> : </span> <?php echo $_stocktakeAry['FundingSourceName'];?>
    							</div>
							</div>                    	
                    <?php //endif;?>    					
    						<div class="row">
    							<div class="info col-xs-4">
    								<span class="label"><?php echo $Lang['eInventory']['eClassApp']['Expected'];?> : </span> <br class="line-break line-break-sm"><?php echo $_stocktakeAry['ExpectedQty'];?>
    							</div>
    							<div class="info col-xs-4">
    								<span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTake'];?> : </span> <br class="line-break line-break-sm"><?php echo $_stocktakeAry['StocktakeQty'];?>
    							</div>
    							<div class="info alert col-xs-4">
    								<span class="label"><?php echo $i_InventorySystem_Stocktake_VarianceQty;?> : </span> <br class="line-break line-break-sm"><?php echo $displayVariance;?>
    							</div>
    						</div>
    					</div>
    				</li>
    			</a>
    	<?php endif;?>
    	
    <?php endforeach;?>
    
<?php endif;?>    
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }
    
    function getItemListUI($offset=0, $numberOfRecord=20, $showNoRecord= true, $filterAry=array())
    {
        global $Lang;
        
        $itemListAry = $this->getItemList($offset, $numberOfRecord, $filterAry);
        $nrItem = count($itemListAry);
        
        ob_start();
?>
<?php if ($nrItem == 0):?>
	<?php if ($showNoRecord):?>			
            <span class="info" id="NoItem"><?php echo $Lang['General']['NoRecordFound'];?></span>
	<?php else:?>
			~~~NoRecord~~~
	<?php endif;?>            
<?php else:?>        
	<?php foreach((array)$itemListAry as $thisItem):?>        
        <?php 
            $_itemID = $thisItem['ItemID'];
            $_itemType = $thisItem['ItemType'];
            $_itemName = $thisItem['ItemName'];
            $_location = $thisItem['Location'];
            $_adminGroup = $thisItem['AdminGroup'];
            $_fundingSourceID = $thisItem['FundingSourceID'];
            $_adminGroupID = $thisItem['AdminGroupID'];
            $_locationID = $thisItem['LocationID'];
            $_qty = $thisItem['Qty'];
        ?>        
            <a href="?task=teacherApp.item.view_item&ItemID=<?php echo $_itemID;?>" data-ItemID="<?php echo $_itemID;?>" data-ItemType="<?php echo $_itemType;?>" data-LocationID="<?php echo $_locationID;?>" data-AdminGroupID="<?php echo $_adminGroupID;?>" data-FundingSourceID="<?php echo $_fundingSrouceID;?>">
                <li class="item">
                    <div class="itemTitle"><?php echo $_itemName;?></div>
                    <div class="info itemLocation"><span class="icon"></span><?php echo $_location;?></div>
                    <div class="info itemGroup"><span class="icon"></span><?php echo $_adminGroup;?></div>
<?php if ($_itemType == ITEM_TYPE_BULK):?>                    
                    <div class="info itemStock"><span class="icon"></span><?php echo $_qty;?></div>
<?php endif;?>                    
                </li>
            </a>
     <?php endforeach;?>
<?php endif;?>
<?php     
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }
    
}   // end class