<?php


# being modified by 

/*
 *  Change Log
 *  03-08-2020 [Philips]
 *  - modified returnAssessmentSemester(), added order by TermAssessment Name
 *  14-10-2019 [Anna] #155645
 *  - added returnStudentsClassLevel()
 *  
 *  15-10-2018 [Anna] #X147195
 *  - modified returnAssessmentYear(), break into two sqls, remove ACADEMIC_YEAR in sql
 *  13-12-2017 [Pun]
 *  	modified Get_Management_Advancement_DBTable_Sql() - fix show Z-score while no score attempt
 *  13-10-2017 [Omas]
 *  	modified returnAllSubjectWithComponents() - fix show deleted subject problem
 *  26-06-2017 [Villa]
 *  	Modified Get_Management_Advancement_DBTable_Sql(), modified targetType related - add monitoring group
 *  02-06-2017 [Villa] #F117604 
 *  	Modified returnAssessmentYear, return YearNameEN in AcademicYear in stead of Year in ASSESSMENT
 *  10-05-2017 [Villa] #E116765 
 *  	Modified Get_Management_Advancement_DBTable_Sql - Fix cannot Show From Term Z-score for Assessment Term
 *  06-04-2017 [Villa]
 *  	Modified Get_Management_Advancement_DBTable_Sql() - Support TermAssessment
 *  	Modified Get_Student_Assessment_Result_Info() - Support TermAssessment
 *  07-12-2016 [Villa]
 *  	Modified Get_Management_Advancement_DBTable_Sql() - add input para subject group id to get other student id from subject group
 *  24-11-2016 [Villa] 
 *  	Modified Get_Management_Advancement_DBTable_Sql() - add student ID in returning sql 
 *  24-11-2016 [Omas]
 *  	Modified returnAllSubjectWithComponents() - improving performance
 *  30-08-2016 [Pun]
 *  	Modified returnAllSubjectWithComponents(), added $filterEmptyScoreSubject param  
 *  01-06-2016 [Pun] [94386]
 *  	Modified getPortfolioClass(), fixed return different classname in different language 
 *  29-03-2016 [Pun] [94386]
 *  	Modified returnAssessmentSemester(), added order by 
 *  11-03-2016 [Pun]
 *  	Modified Get_Management_Advancement_DBTable_Sql(), added order by 
 *  10-03-2016 [Pun]
 *  	Modified returnAllSubjectWithComponents(), hide subject that deleted
 *  27-02-2016 [Pun]
 *  	Modified returnAllSubjectWithComponents(), added return field SubjectComponentID 
 *  18-02-2016 [Pun]
 *  	Modified Get_Management_Advancement_DBTable_Sql(), added standard score field 
 *  26-01-2016 [Pun]
 *  	Added returnAllSubjectWithComponents() will return RecordID 
 *  	Modified returnAssessmentSemester(), fixed duplicate "overall" option 
 *  24-08-2015 [Pun]
 *  	added $includeCodeIdArr, $extrudeCodeIdArr for returnAllSubjectWithComponents()
 *  
 *  ??-??-????
 * 		support assessments like T1A1
 *
 *  */
 
 
include_once 'libportfolio.php';
include_once 'libportfolio2007a.php';

/**
 * Contain functions specific to Assessment Statistic Report.
 */
class libpf_asr extends libportfolio2007
{
	function returnAssessmentYear($SubjectCode="", $SubjectComponentCode="", $TableSelect=0)
	{
		global $eclass_db;

    switch($TableSelect)
    {
      case 0:
        $table = "{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD";
        if($SubjectCode!="")
        {
        	$conds = " AND SubjectCode = '$SubjectCode' ";
        	$conds .= ($SubjectComponentCode=="") ? " AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '')" : " AND SubjectComponentCode = '$SubjectComponentCode'";
        }
        break;
      case 1:
        $table = "{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD";
        break;
    }
    
    $sql = "SELECT AcademicYearID,YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID IS NOT NULL ";
    $AcademicYearAry = $this->returnArray($sql);    
    $AcademicYearAssocAry= BuildMultiKeyAssoc($AcademicYearAry,array('AcademicYearID'));
    
    $sql =  "
                SELECT DISTINCT
                    a.AcademicYearID
                    
                FROM {$table} a
                
                WHERE
                    a.AcademicYearID IS NOT NULL
                    $conds
                ORDER BY Year DESC
    ";
    $AcademicYearIDAry = $this->returnArray($sql);      
   
    $row = array();
    for($i=0;$i<count($AcademicYearIDAry);$i++){
        $thisAcademicYearID = $AcademicYearIDAry[$i]['AcademicYearID'];
        
        $thisYearName = $AcademicYearAssocAry[$thisAcademicYearID]['YearNameEN'];
        $row[$i]['0']=$thisAcademicYearID;
        $row[$i]['AcademicYearID']=$thisAcademicYearID;
        $row[$i]['1']=$thisYearName;
        $row[$i]['YearNameEN']=$thisYearName;    
    }              
  
//     $sql =  "
//             SELECT DISTINCT
//                 a.AcademicYearID,
//                 ay.YearNameEN
//             FROM {$table} a
//             INNER JOIN
//                 ACADEMIC_YEAR ay ON a.AcademicYearID = ay.AcademicYearID
//             WHERE
//                 a.AcademicYearID IS NOT NULL
//                 $conds
//             ORDER BY Year DESC
//             ";

// 		$row = $this->returnArray($sql);
		return $row;
	}
	
	function returnAssessmentSemester($SubjectCode="", $SubjectComponentCode="", $ParAYID)
	{
		global $eclass_db, $intranet_db, $range_all, $ec_iPortfolio;

		if($SubjectCode!="")
		{
			$conds .= " AND a.SubjectCode = '$SubjectCode' ";
			$conds .= ($SubjectComponentCode=="") ? " AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '')" : " AND a.SubjectComponentCode = '$SubjectComponentCode'";
		}

		$semesterSQL = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');
		$sql =  "SELECT DISTINCT
				if(TermAssessment is null, assr.YearTermID, CONCAT(assr.YearTermID, '_', assr.TermAssessment)) as YearTermID,
				if(TermAssessment is not null, assr.TermAssessment, {$semesterSQL}) as Semester
			FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS  assr
			LEFT JOIN
				{$intranet_db}.ACADEMIC_YEAR_TERM ayt
			ON
				assr.YearTermID = ayt.YearTermID
			WHERE
				assr.AcademicYearID = '{$ParAYID}'
				$conds
			ORDER BY
				ayt.YearTermID is null, ayt.TermStart, assr.TermAssessment is null, assr.TermAssessment
            ";
		$TempSemesterArr = $this->returnArray($sql);
		
		/*
		for($i=0, $i_max=sizeof($TempSemesterArr); $i<$i_max; $i++)
		{
		  list($_ytID, $_sem) = $TempSemesterArr[$i];
		  
		  # rename to T1, T1A1 format
		  if (strstr($_ytID, "_"))
		  {
		  	$tmpArr = explode("_", $_ytID);
		  	$t1t2t3 = (substr($tmpArr[1], 0, 2));
		  	if (strtoupper($t1t2t3)=="T1" || strtoupper($t1t2t3)=="T2" || strtoupper($t1t2t3)=="T3" || strtoupper($t1t2t3)=="T4")
		  	{
		  		$termRenamed[$tmpArr[0]] = $t1t2t3;
		  	}
		  	
		  	$TempSemesterArr[$i][1] = $tmpArr[1];
		  }
		  
		}
		*/
				
		# re-order the semester
		//$pre_YTID = "";
		$ReturnSemesterArray = array();
		for($i=0, $i_max=sizeof($TempSemesterArr); $i<$i_max; $i++)
		{
		  list($_ytID, $_sem) = $TempSemesterArr[$i];
		  
		  
		  if ($_sem == "")
		  {
			    $_ytID = "";
	       		 $_sem = trim($ec_iPortfolio['overall_result']);
	      } else
	      {
	      	/*
	      	if (!strstr($_ytID, "_") && $termRenamed[$_ytID]!="")
		 	 {
		 	 	$_sem =  $termRenamed[$_ytID];
		 	 }
		 	 */
	      }

	      if(in_array(array($_ytID, $_sem), $ReturnSemesterArray)){
	      	continue;
	      }
		  $ReturnSemesterArray[] = array($_ytID, $_sem);
		  
		  
		  
		}
//debug_r($ReturnSemesterArray);
		return $ReturnSemesterArray;
	}

	function returnAllAssessmentSemester()
	{
		global $eclass_db, $intranet_db, $range_all, $ec_iPortfolio;

		$sql =  "
			SELECT DISTINCT
				Year,
				AcademicYearID,
				Semester,
				YearTermID
			FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS  assr
			WHERE
				AcademicYearID IS NOT NULL
			AND
				AcademicYearID <> 0
			AND
				YearTermID IS NOT NULL
			AND
				YearTermID <> 0
			AND
				(TermAssessment IS NULL OR TermAssessment ='')
			ORDER BY
				Year,
				Semester
		";

		$SemesterArr = $this->returnArray($sql);
				
		return $SemesterArr;
	}

	function returnAssessmentClass($SubjectCode="", $SubjectComponentCode="", $ParAYID="")
	{
		global $eclass_db, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$conds = "";
		if($SubjectCode!="")
		{
      $conds .= " AND SubjectCode = '$SubjectCode'";
      $conds .= ($SubjectComponentCode != "") ? " AND SubjectComponentCode = '$SubjectComponentCode'" : " AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '')";
    }
    if($Year!="")
    {
      $conds .= " AND a.AcademicYearID = '$ParAYID'";
    }
		
		$sql =  "
              SELECT DISTINCT
                yc.YearClassID,
                $CLASSNAME_FIELD as ClassName
              FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
              INNER JOIN {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
              INNER JOIN {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
              INNER JOIN {$intranet_db}.YEAR as y on yc.YearID = y.YearID
              INNER JOIN {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID AND a.Year = ay.YearNameEN
              WHERE
                1
                $conds
              ORDER BY
                y.Sequence, yc.Sequence
            ";

		$row = $this->returnArray($sql);

		return $row;
	}

	
	# get activated level
	function getActivatedForm($ParAYID="", $OnlyActivated=true)
  {
	    global $eclass_db, $intranet_db;
	    
	    $ay_conds = ($ParAYID == "") ? "" : "assr.AcademicYearID = {$ParAYID}";
		
		if ($OnlyActivated)
		{
		    $sql =  "
		              SELECT DISTINCT
		                y.YearID,
		                y.YearName
		              FROM
		                {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
		              INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps
		                ON assr.UserID = ps.UserID
		              INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
		                ON ycu.UserID = ps.UserID
		              INNER JOIN {$intranet_db}.YEAR_CLASS yc
		                ON  ycu.YearClassID = yc.YearClassID AND
		                    (assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
		              INNER JOIN {$intranet_db}.YEAR y
		                ON yc.YearID = y.YearID
		              ORDER BY
		                y.Sequence
		            ";
		} else
		{
		    $sql =  "
		              SELECT DISTINCT
		                y.YearID,
		                y.YearName
		              FROM
		                 {$intranet_db}.YEAR y
		              ORDER BY
		                y.Sequence
		            ";
			
		}
		$row = $this->returnArray($sql);

		return $row;
	}
	
	# Display Portfolio Actived Class in Pull-down Selection
	function getPortfolioClass($ParAYID="", $ParYID="", $OnlyActivated=true)
  {
	    global $intranet_db, $eclass_db, $intranet_session_language;
	    
	    $conds = ($ParYID == "") ? "" : " AND yc.YearID = {$ParYID}";
	    
	    $ClassNameDB = 'ClassTitleEN';//($intranet_session_language=="EN") ? "ClassTitleEN" : "ClassTitleB5"; 
	    
	    if ($OnlyActivated)
		{
		    $sql =  "
		              SELECT DISTINCT
		                yc.{$ClassNameDB}
		              FROM
		                {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
		              INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps
		                ON assr.UserID = ps.UserID
		              INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
		                ON ycu.UserID = ps.UserID
		              INNER JOIN {$intranet_db}.YEAR_CLASS yc
		                ON  ycu.YearClassID = yc.YearClassID AND
		                    (assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
						INNER JOIN {$intranet_db}.YEAR as y ON (yc.YearID = y.YearID)
		              WHERE
		                yc.AcademicYearID = {$ParAYID}
		                {$conds}
		              ORDER BY
		                y.Sequence, yc.Sequence
		            ";
		} else
		{
			
		    $sql =  "
		              SELECT DISTINCT
		                yc.{$ClassNameDB}
		              FROM {$intranet_db}.YEAR_CLASS yc
						LEFT JOIN {$intranet_db}.YEAR as y ON (yc.YearID = y.YearID)
		              WHERE
		                yc.AcademicYearID = {$ParAYID}
		                {$conds}
		              ORDER BY
		                y.Sequence, yc.Sequence
		            ";
		}
	    $row = $this->returnVector($sql);
//debug($sql);
		return $row;
	}
	function getPortfolioClass2($ParAYID="", $ParYID="", $OnlyActivated=true)
  {
	    global $intranet_db, $eclass_db, $intranet_session_language;
	    
	    $conds = ($ParYID == "") ? "" : " AND yc.YearID = {$ParYID}";
	    
	    $ClassNameDB = 'ClassTitleEN';//($intranet_session_language=="EN") ? "ClassTitleEN" : "ClassTitleB5"; 
	    
	    if ($OnlyActivated)
		{
		    $sql =  "
		              SELECT DISTINCT
		                yc.ClassTitleEN,
		                yc.ClassTitleB5
		              FROM
		                {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
		              INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps
		                ON assr.UserID = ps.UserID
		              INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
		                ON ycu.UserID = ps.UserID
		              INNER JOIN {$intranet_db}.YEAR_CLASS yc
		                ON  ycu.YearClassID = yc.YearClassID AND
		                    (assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
						INNER JOIN {$intranet_db}.YEAR as y ON (yc.YearID = y.YearID)
		              WHERE
		                yc.AcademicYearID = {$ParAYID}
		                {$conds}
		              ORDER BY
		                y.Sequence, yc.Sequence
		            ";
		} else
		{
			
		    $sql =  "
		              SELECT DISTINCT
		                yc.ClassTitleEN,
		                yc.ClassTitleB5
		              FROM {$intranet_db}.YEAR_CLASS yc
						LEFT JOIN {$intranet_db}.YEAR as y ON (yc.YearID = y.YearID)
		              WHERE
		                yc.AcademicYearID = {$ParAYID}
		                {$conds}
		              ORDER BY
		                y.Sequence, yc.Sequence
		            ";
		}
	    $row = $this->returnResultSet($sql);
//debug($sql);
		return $row;
	}
	
	function returnAllSubjectWithComponents($ParAYID="", $ParYID="", $includeCodeIdArr = array(), $extrudeCodeIdArr = array(), $filterEmptyScoreSubject = false)
	{
		global $eclass_db, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		//StartTimer();
		$subj_namefield = Get_Lang_Selection('SUBJ.CH_ABBR', 'SUBJ.EN_ABBR');
		$cmp_subj_namefield = Get_Lang_Selection('CMP.CH_DES', 'CMP.EN_DES');

		#### Extrude CODEID START ####
		$extrudeSQL = '';
		if($extrudeCodeIdArr){
			$extrudeSQL = implode("','", $extrudeCodeIdArr);
			$extrudeSQL = " AND m.CODEID NOT IN ('{$extrudeSQL}')";
		}
		#### Extrude CODEID END ####
		
    $conds = "";
//     $conds .= ($ParAYID!="") ? " AND assr.AcademicYearID = {$ParAYID}" : "";
// 	$conds .= ($ParYID!="") ? " AND yc.YearID = {$ParYID}" : "";
	$assr_conds = ($ParAYID!="") ? " AND assr.AcademicYearID = {$ParAYID}" : "";
	$yc_conds_ay = ($ParAYID!="") ? " AND yc.AcademicYearID = {$ParAYID}" : "";
	$yc_conds_y = ($ParYID!="") ? " AND yc.YearID = {$ParYID}" : "";
	if($filterEmptyScoreSubject){
// 	    $conds .= " AND assr.Score <> -1";
	    $emptyScore_conds = " AND assr.Score <> -1";
	}

		/* Rewrite SQL * /
		$subjectNameField = Get_Lang_Selection("m.CH_DES", "m.EN_DES");
		$subjectComponentNameField = Get_Lang_Selection("c.CH_DES", "c.EN_DES");
		$sql = "SELECT DISTINCT
			m.EN_SNAME AS SubjectCode, 
			IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.en_sname) AS SubjectComponentCode, 
			{$subjectNameField} AS SubjectName, 
			{$subjectComponentNameField} AS SubjectComponentName
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS assr 
		INNER JOIN 
			{$intranet_db}.YEAR_CLASS_USER as ycu 
		on 
			assr.UserID = ycu.UserID 
		INNER JOIN 
			{$intranet_db}.ASSESSMENT_SUBJECT AS m 
		ON 
			assr.SubjectCode = m.EN_SNAME 
			{$extrudeSQL}
		INNER JOIN 
			{$intranet_db}.YEAR_CLASS as yc 
		on 
			ycu.YearClassID = yc.YearClassID 
		LEFT JOIN 
			{$intranet_db}.ASSESSMENT_SUBJECT AS c 
		ON 
			m.codeid = c.codeid 
		AND 
			assr.SubjectComponentCode = c.EN_SNAME 
		WHERE 
			(m.CMP_CODEID IS NULL or TRIM(m.CMP_CODEID) = '') 
			{$conds}
		ORDER BY 
			m.DisplayOrder, 
			IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', 0, c.DisplayOrder)
		";
		/* */
		
	/* Old Code */
// comment by Omas
// 		$sql = "SELECT DISTINCT m.EN_SNAME AS SubjectCode, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.en_sname) AS SubjectComponentCode, ";
// 		$sql .= Get_Lang_Selection("m.CH_DES", "m.EN_DES")." AS SubjectName, ";
//     $sql .= Get_Lang_Selection("c.CH_DES", "c.EN_DES")." AS SubjectComponentName, ";
//     $sql .= " m.RecordID AS SubjectID, ";
//     $sql .= " c.RecordID AS SubjectComponentID ";
// 		$sql .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS assr ";
// 		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER as ycu on assr.UserID = ycu.UserID ";
//     $sql .= "INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS m ON assr.SubjectCode = m.EN_SNAME AND m.RecordStatus=1 {$extrudeSQL}";
//     $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID ";
//     $sql .= "LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS c ON m.codeid = c.codeid AND assr.SubjectComponentCode = c.EN_SNAME ";
//     $sql .= "WHERE (m.CMP_CODEID IS NULL or TRIM(m.CMP_CODEID) = '') {$conds} ";
//     $sql .= "ORDER BY m.DisplayOrder, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', 0, c.DisplayOrder)";
    /* */
    $sql = "SELECT 
			distinct
			m.EN_SNAME AS SubjectCode, 
			IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.en_sname) AS SubjectComponentCode, 
			".Get_Lang_Selection("m.CH_DES", "m.EN_DES")." AS SubjectName, 
			".Get_Lang_Selection("c.CH_DES", "c.EN_DES")." AS SubjectComponentName,  
			m.RecordID AS SubjectID, 
			 c.RecordID AS SubjectComponentID ,
			  c.DisplayOrder
			from 
			{$intranet_db}.YEAR_CLASS as yc 
			inner join {$intranet_db}.YEAR_CLASS_USER as ycu on yc.YearClassID = ycu.YearClassID 
			inner join {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD  as assr on ycu.UserID = assr.UserID $emptyScore_conds  $assr_conds
			INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS m ON assr.SubjectID = m.RecordID AND m.RecordStatus=1  AND (m.CMP_CODEID IS NULL or TRIM(m.CMP_CODEID) = '') 
			LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS c ON m.codeid = c.codeid AND assr.SubjectComponentID = c.RecordID AND c.RecordStatus = 1
			where 1 $yc_conds_ay $yc_conds_y
			 ORDER BY m.DisplayOrder, c.DisplayOrder";
			
    $row = $this->returnArray($sql);
    
		######## Add Subject START ########
    
		if($includeCodeIdArr){
			$intrudeSQL = implode("','", $includeCodeIdArr);
			$intrudeSQL = " AND m.CODEID IN ('{$intrudeSQL}')";
			
			$subjectNameField = Get_Lang_Selection("m.CH_DES", "m.EN_DES");
			$subjectComponentNameField = Get_Lang_Selection("c.CH_DES", "c.EN_DES");
			$sql = "SELECT DISTINCT
				m.EN_SNAME AS SubjectCode, 
				'' AS SubjectComponentCode,
				{$subjectNameField} AS SubjectName,
				NULL AS SubjectComponentName,
				m.RecordID AS SubjectID
			FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS assr
			INNER JOIN 
				{$intranet_db}.ASSESSMENT_SUBJECT AS m 
			ON 
				assr.SubjectCode = m.EN_SNAME
				{$intrudeSQL}
			";
				
			$rs = $this->returnArray($sql);

			foreach ($rs as $r){
				if(!in_array($r, $row)){
					$row[] = $r;
				}
			}
		}
		######## Add Subject END ########

		for($i=0; $i<sizeof($row); $i++)
		{
			$row[$i]["have_score"] = $this->checkSubjectScoreExist($row[$i]["SubjectCode"], $row[$i]["SubjectComponentCode"]);
		}
		
		return $row;
	}
	
	
	
	
	
	
	
	
	
	
	
	function returnSqlFieldLang($my_field, $my_prefix){
//		global $ck_default_lang;
		global $intranet_session_language;

//		if ($ck_default_lang=="chib5")
		if($intranet_session_language=="b5")
		{
			$rx = "{$my_prefix}.CH_{$my_field}";
		} else
		{
			$rx = "{$my_prefix}.EN_{$my_field}";
		}

		return $rx;
	}
	
	# Display Portfolio Actived Class in Pull-down Selection
	function getPortfolioLevelSelection(&$ClassLevelID){
		$active_form_arr  = $this->getActivatedForm();
		if ($ClassLevelID=="")
		{
			$ClassLevelID = $active_form_arr[0][0];
		}

		return getSelectByArray($active_form_arr, "name='ClassLevelID' onChange='this.form.submit()'", $ClassLevelID,0,1);
	}

	# get subject selection (for statistic)
	function getSubjectSelection($SubjectArr, $Tab, $SubjectCode, $SubjectComponentCode, $ShowAll=0, $ShowNoScoreSubject=0)
	{
		global $range_all;

		$ReturnSelection = "<SELECT ".$Tab.">";
		if($ShowAll)
		{
			$ReturnSelection .= "<OPTION value=''>".$range_all."</OPTION>";
		}

		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($s_code, $cmp_code, $s_name, $cmp_name) = $SubjectArr[$i];
			if($SubjectArr[$i]["have_score"] || $ShowNoScoreSubject==1)
			{
				if($cmp_code=="")
				{
					if($s_code==$SubjectCode)
					{
						$selected = "SELECTED";
						$ReturnSubjectName = $s_name;
					}
					else
						$selected = "";

					$ReturnSelection .= "<OPTION value='$s_code' $selected>$s_name</OPTION>";

				}
				else
				{
					if($cmp_code==$SubjectComponentCode && $s_code==$SubjectCode)
					{
						$selected = "SELECTED";
						$ReturnSubjectName = $s_name." ".$cmp_name;
					}
					else
						$selected = "";

					$ReturnSelection .= "<OPTION value='".$s_code."###".$cmp_code."' $selected>&nbsp;&nbsp;&nbsp;$cmp_name</OPTION>";
				}
			}
		}
		$ReturnSelection .= "</SELECT>";

		return array($ReturnSelection, $ReturnSubjectName);
	}

	function generateSchoolSummary($SubjectCode, $SubjectComponentCode="",$ParPageInfoArr, $SchoolYear="",$ClassName="",$form)
	{
		global $eclass_db, $ec_iPortfolio, $ec_iPortfolio_Report, $ec_words, $no_record_msg, $sr_image_path;
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$data = $this->returnSchoolSummaryData($SubjectCode, $SubjectComponentCode,$SchoolYear,$ClassName,0,"");

	if($form==1) //IF Choose FORM
	{
		if(sizeof($data)!=0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				list($year, $cname, $score) = $data[$i];
				list($ClassLevelID, $LevelName) = $this->getClassLevel($cname);
				$dataArr[$ClassLevelID][$year][] = $score;
			}

			foreach($dataArr as $level => $levelArr)
			{
				foreach($levelArr as $year => $valueArr)
				{
					$min = min($valueArr);
					$max = max($valueArr);
					$sd = round(standard_deviation($valueArr), 2);
					$sum = array_sum($valueArr);
					$total = sizeof($valueArr);
					$mean = ($total==0) ? 0 : round($sum/$total, 2);
					

						
					$newArr[$level][$year]["mean"] = $mean;
					$newArr[$level][$year]["sd"] = $sd;
					$newArr[$level][$year]["max"] = $max;
					$newArr[$level][$year]["min"] = $min;
					

				}
			}
			
		}
		
		$rx = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr>
				<td align='center'>
				<table width='100%' border='0' bgcolor='#cccccc' cellpadding='4' cellspacing='0'>
				<tr class='tabletop'> 
					<td class='tabletopnolink'>".$ec_iPortfolio_Report['form']."</td>
					<td class='tabletopnolink' align='center'>".$ec_iPortfolio['year']."</td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2009a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['mean']."</a></td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2009a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['standard_deviation']."</a></td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2009a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['highest_score']."</a></td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2009a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['lowest_score']."</a></td>
				</tr>";	
			
		$count = 0;
		$isFirst = 1;

		if(sizeof($newArr)!=0)
		{
			$i=0;

			foreach($newArr as $level => $levelArr)
			{
					$ClassLevel = $this->getClassLevelName($level);
					
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$rspan = sizeof($levelArr);
				if(strcmp($level,$currlevel)!=0)
				{
					$isNew = 1;
					$currlevel = $level;
				}
				
				$bclass = ($count%2!=0) ? "class='tablerow2'" : "class='tablerow1'";

				foreach($levelArr as $year => $valueArr)
				{

					if($isNew==1) //Table Content
					{
						$rx .= "<tr $bclass><td class=tabletext' rowspan='$rspan'>".$ClassLevel."</td>";
						$isNew = 0;
						$isFirst = 0;
					}
					else
					{
						$rx .= "<tr $bclass>";
					}

					$rx .= "<td align='center' class='tabletext tablerow_underline'><a href='javascript:jTO_STAT_CLASS(\"".$year."\", \"".$level."\")'  class='tablelink'>".$year."</a></td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["mean"]."</td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["sd"]."</td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["max"]."</td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["min"]."</td>";
					$rx .= "</tr>";

				}
				$count++;
			}
			$i++;
			}
		}
		else
		{
			$rx .= "<tr $bclass><td class=tabletext' colspan=6 align='center'>".$no_record_msg."</td></tr>";
		}

		
	}else{ //END IF Choose FORM
		
		if(sizeof($data)!=0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				list($year, $cname, $score) = $data[$i];
				list($ClassLevelID, $LevelName) = $this->getClassLevel($cname);
				$dataArr[$year][$ClassLevelID][] = $score;
			}


			foreach($dataArr as $year => $levelArr)
			{
				foreach($levelArr as $level => $valueArr)
				{
					$min = min($valueArr);
					$max = max($valueArr);
					$sd = round(standard_deviation($valueArr), 2);
					$sum = array_sum($valueArr);
					$total = sizeof($valueArr);
					$mean = ($total==0) ? 0 : round($sum/$total, 2);
					

						
					$newArr[$year][$level]["mean"] = $mean;
					$newArr[$year][$level]["sd"] = $sd;
					$newArr[$year][$level]["max"] = $max;
					$newArr[$year][$level]["min"] = $min;
					

				}
			}

		}
		
		$rx = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr>
				<td align='center'>
				<table width='100%' border='0' bgcolor='#cccccc' cellpadding='4' cellspacing='0'>
				<tr class='tabletop'>
					<td class='tabletopnolink'>".$ec_iPortfolio['year']."</td>
					<td class='tabletopnolink' align='center'>".$ec_iPortfolio_Report['form']."</td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2007a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['mean']."</a></td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2007a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['standard_deviation']."</a></td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2007a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['highest_score']."</a></td>
					<td align='center'><a href='#' class='tabletoplink' onmouseover=MM_swapImage('sort012','','images/2007a/icon_sort_a_on.gif',1) onmouseout=MM_swapImgRestore()>".$ec_iPortfolio['lowest_score']."</a></td>
				</tr>";

		$count = 0;
		$isFirst = 1;

		if(sizeof($newArr)!=0)
		{
			$i=0;
			foreach($newArr as $year => $levelArr)
			{
				
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$rspan = sizeof($levelArr);
				if(strcmp($year,$currYear)!=0)
				{
					$isNew = 1;
					$currYear = $year;
				}

				
				$bclass = ($count%2!=0) ? "class='tablerow2'" : "class='tablerow1'";


				foreach($levelArr as $level => $valueArr)
				{
					$ClassLevel = $this->getClassLevelName($level);
	
					if($isNew==1)
					{
						$rx .= "<tr $bclass><td class=tabletext' rowspan='$rspan'>".$year."</td>";
						$isNew = 0;
						$isFirst = 0;
					}
					else
					{
						$rx .= "<tr $bclass>";
					}

					$rx .= "<td align='center' class='tabletext tablerow_underline'><a href='javascript:jTO_STAT_CLASS(\"".$year."\", \"".$level."\")'  class='tablelink'>".$ClassLevel."</a></td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["mean"]."</td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["sd"]."</td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["max"]."</td>";
					$rx .= "<td align='center' class='tabletext tablerow_underline'>".$valueArr["min"]."</td>";
					$rx .= "</tr>";

				}
				$count++;
			}
			$i++;
			}
		}
		else
		{
			$rx .= "<tr $bclass><td class=tabletext' colspan=6 align='center'>".$no_record_msg."</td></tr>";
		}
		
	} //END IF Choose SchoolYear

		$rx .= "</table>
				</td>
				</tr>
				</table>";

		return $rx;
	}
	
	function returnSchoolSummaryData($SubjectCode, $SubjectComponentCode="", $SchoolYear,$ClassName,$navigation="",$form="")
	{
		global $eclass_db, $ck_course_id, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$course_db = $this->course_db;
		$ClassName = substr($ClassName,1,1);
		$conds = ($SubjectCode=="") ? "" : "AND a.SubjectCode = '$SubjectCode'";
		$conds .= ($SubjectComponentCode=="") ? " AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '')" : " AND a.SubjectComponentCode = '$SubjectComponentCode'";
		$conds .= ($SchoolYear=="") ? "" : "AND a.Year = '$SchoolYear'";
		$conds .= ($ClassName=="")? "" : "AND $CLASSNAME_FIELD like '$ClassName%'";

		if($navigation==1 && $form==1)
		{

		$sql = "SELECT Distinct
					SUBSTRING($CLASSNAME_FIELD,1,1) as ClassName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.IsAnnual = 1
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					a.Year DESC,
					$CLASSNAME_FIELD ASC,
					a.Score ASC
				";


		}else	if($navigation==1 && $form == 0)
		{

			$sql = "SELECT Distinct
					a.Year
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.IsAnnual = 1
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					a.Year DESC,
					$CLASSNAME_FIELD ASC,
					a.Score ASC
				";

		}else{

			$sql = "SELECT
					a.Year,
					$CLASSNAME_FIELD as ClassName,
					a.Score
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					(a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					a.Year DESC,
					$CLASSNAME_FIELD ASC,
					a.Score ASC
				";
		}



		$row = $this->returnArray($sql);

		return $row;
	}

/*
	# Generate first row number and last row number
	function GEN_PAGE_ROW_NUMBER($ParRowCount, $ParPageDivision, $ParCurrentPage)
	{
		# If "show all in a page",
		# first row is 1, last row is number of all rows
		if($ParPageDivision == "")
		{
			$FirstPage = min(1, $ParRowCount);
			$LastPage = $ParRowCount;
		}
		# calculate according to page division and current page
		else
		{
			$FirstPage = ($ParCurrentPage-1) * $ParPageDivision + 1;
			$LastPage = min($ParRowCount,($ParCurrentPage*$ParPageDivision));
		}

		$ReturnStr = $FirstPage." - ".$LastPage;
		return $ReturnStr;
	}
*/	
	function GEN_YEAR_SELECTION($SubjectCode, $SubjectComponentCode,$Year)
	{
		global $i_ReportCard_All_Year;

		$YearArr = $this->returnYearSelectionData($SubjectCode, $SubjectComponentCode);
	
		$YearSelect = getSelectByArrayTitle($YearArr, "name='Year' onChange='jCHANGE_FIELD(\"year\")'",$i_ReportCard_All_Year,$Year, false, 2);
	
		return $YearSelect;

	}
	
	function returnYearSelectionData($SubjectCode, $SubjectComponentCode)
	{
	
		$data = $this->returnSchoolSummarySelectionData($SubjectCode, $SubjectComponentCode);

		if(sizeof($data)!=0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				list($year[$i], $cname[$i], $score[$i]) = $data[$i];
			}
			$year = array_unique($year);
			sort($year);

			//debug_r($year);

			if(is_array($year))
			{
				for($i=0; $i<count($year); $i++)
				{
						//$LevelIDActivated[] = $ClassArr[$i]['Year'];
						$YearArr[$i] = array($year[$i], $year[$i]);
				}
			}
		
			return $YearArr;
	
		}
	}

	function returnSchoolSummarySelectionData($SubjectCode, $SubjectComponentCode="")
	{
		global $eclass_db, $ck_course_id, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$course_db = $this->course_db;
		$ClassName = substr($ClassName,1,1);
		$conds = ($SubjectCode=="") ? "" : "AND a.SubjectCode = '$SubjectCode'";
		$conds .= ($SubjectComponentCode=="") ? " AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '')" : " AND a.SubjectComponentCode = '$SubjectComponentCode'";


				$sql = "SELECT
					a.Year,
					$CLASSNAME_FIELD as ClassName,
					a.Score
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.IsAnnual = 1
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					a.Year DESC,
					$CLASSNAME_FIELD ASC,
					a.Score ASC
				";

		$row = $this->returnArray($sql);
		//echo $sql;
		return $row;
	}
	
	function GEN_CLASSNAME_SELECTION($SubjectCode, $SubjectComponentCode,$SchoolYear,$ClassName)
	{
		global $i_general_WholeSchool;

		$data = $this->returnSchoolSummaryData($SubjectCode, $SubjectComponentCode,$SchoolYear,"",0,"");

		if(sizeof($data)!=0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				list($year[$i], $cname[$i], $score[$i]) = $data[$i];
			//	list($ClassLevelID, $LevelName) = $this->getClassLevel($cname);

			}
			//$Classname = array_unique($cname);
			//debug_r($Classname);

			for($i=0; $i<sizeof($cname); $i++)
			{

					list($ClassLevelID[$i], $LevelName[$i]) = $this->getClassLevel($cname[$i]);

			}
			//debug_r($LevelName);
			$LevelNameUnique = array_unique($LevelName);
			sort($LevelNameUnique);
			//debug_r($LevelNameUnique);

			if(is_array($LevelNameUnique))
			{
				for($i=0; $i<sizeof($LevelNameUnique); $i++)
				{
						//$LevelIDActivated[] = $ClassArr[$i]['Year'];
						$LevelNameUnique[$i] = array($LevelNameUnique[$i], $LevelNameUnique[$i]);
				}
			}

			$ClassSelect = getSelectByArrayTitle($LevelNameUnique, "name='Form' onChange='jCHANGE_FIELD(\"form\")'",$i_general_WholeSchool,$ClassName, false, 2);

			return $ClassSelect;

		}

	}
	
	function returnAssessmentClassGroupByForm($SubjectCode, $SubjectComponentCode, $Year)
	{
		global $eclass_db, $intranet_db;
		$CLASSLEVEL_FIELD = 'y.YearName';
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$conds = "";
		if($SubjectCode!="" && $Year!="")
		{
			$conds = ($SubjectComponentCode!="") ? "WHERE a.SubjectCode = '$SubjectCode' AND a.SubjectComponentCode = '$SubjectComponentCode' AND a.Year = '$Year'" : "WHERE a.SubjectCode = '$SubjectCode' AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '') AND a.Year = '$Year'";
		}
		else if($SubjectCode!="" && $Year=="")
		{
			$conds = ($SubjectComponentCode!="") ? "WHERE a.SubjectCode = '$SubjectCode' AND a.SubjectComponentCode = '$SubjectComponentCode'" : "WHERE a.SubjectCode = '$SubjectCode' AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '')";
		}
		else if($SubjectCode=="" && $Year!="")
		{
			$conds = "WHERE a.Year = '$Year'";
		}

		$sql = "SELECT DISTINCT
					$CLASSNAME_FIELD as ClassName,
					$CLASSLEVEL_FIELD as LevelName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				$conds
				ORDER BY
					$CLASSLEVEL_FIELD,
					$CLASSNAME_FIELD
				";
		$row = $this->returnArray($sql);

		$curr_form = "";
		for($j=0; $j<sizeof($row); $j++)
		{
			list($ClassName, $ClassLevel) = $row[$j];

			if($ClassLevel!=$CurrentLevel)
			{
				// returnSelection does not escape strings, escape here.
				$class_arr[] = array(null, htmlspecialchars($ClassLevel));
				$CurrentLevel = $ClassLevel;
			}
			// returnSelection does not escape strings, escape here.
			$class_arr[] = array(htmlspecialchars($ClassName), htmlspecialchars($ClassName));
		}

		return $class_arr;
	}
	
/*	
		# Generate "page" selection list
	function GEN_PAGE_SELECTION($ParRowCount, $ParPageDivision, $ParCurrentPage, $ParTag)
	{
		# If "show all in a page", one page only
		if($ParPageDivision != "")
		{
			for($i=1; $i<=ceil($ParRowCount/$ParPageDivision); $i++)
			{
				$PageArr[] = array($i, $i);
			}
		}
		else
			$PageArr[] = array(1, 1);

		$ReturnStr = getSelectByArray($PageArr, $ParTag, $ParCurrentPage, 0, 1);

		return $ReturnStr;
	}

	# Generate "records per page" selection list
	function GEN_PAGE_DIVISION_SELECTION($ParPageDivision, $ParTag)
	{
		global $i_general_all;

		# increment of 10 per step
		# Todo: 1 per step for debug -> 10 per step for actual use
		for($i=1; $i<=10; $i++)
		{
			$DivisionArr[] = array($i, $i);
		}
		
		$ReturnStr = getSelectByArrayTitle($DivisionArr, $ParTag, $i_general_all, $ParPageDivision);
		
		return $ReturnStr;
	}
*/	
	function returnAssessmentStudentByClass($Year="", $ClassName, $SubjectCode="", $SubjectComponentCode="")
	{
		global $eclass_db, $intranet_db, $ck_course_id;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		if($SubjectCode!="")
		{
			$conds = ($SubjectComponentCode!="") ? " AND a.SubjectCode = '$SubjectCode' AND a.SubjectComponentCode = '$SubjectComponentCode'" : " AND a.SubjectCode = '$SubjectCode' AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '')";
		}
		$conds .= ($Year!="") ? " AND a.Year = '$Year'" : "";
		$course_db = $this->course_db;
		$sql = "SELECT DISTINCT
					a.UserID,
					CONCAT('(', c.class_number, ') ', CONCAT(c.firstname, '&nbsp;', c.lastname)) as DisplayName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as b ON a.UserID = b.UserID
					LEFT JOIN {$course_db}.usermaster as c ON b.CourseUserID = c.user_id
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					$CLASSNAME_FIELD = '$ClassName'
					AND c.user_id IS NOT NULL
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					c.class_number
				";

		$row = $this->returnArray($sql);

		return $row;
	}
	
	# Change Student List Array Record to previous record or next record
	function CHANGE_STUDENT_LIST_RECORD($StudentArr,$StudentID,$direction)
	{
		for($i=0;$i<sizeof($StudentArr);$i++)
		{
			if($StudentArr[$i][0]==$StudentID)
			{
				if($direction==1)
				{
					$StudentIDNext = $StudentArr[$i+1][0];
				}else{
					$StudentIDNext = $StudentArr[$i-1][0];
				}
			}
		}
		if($StudentIDNext==""){$StudentIDNext =$StudentID;}

		return $StudentIDNext;

	}
	
	# function to retrieve subject that has no score
	function checkSubjectScoreExist($SubjectCode, $SubjectComponentCode)
	{
		global $eclass_db;

		$conds = ($SubjectComponentCode=="") ? " AND (SubjectComponentCode = '' OR SubjectComponentCode IS NULL)" : " AND SubjectComponentCode = '$SubjectComponentCode'";

		$sql = "SELECT
					SUM(Score)
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
					(Score>0 OR (Score = 0 AND Grade <> '' AND Grade IS NOT NULL && OrderMeritForm > 0))
					AND SubjectCode = '".addslashes($SubjectCode)."'
					$conds
				";
		$row = $this->returnVector($sql);

		return ($row[0]>0) ? 1 : 0;
	}
	
	function getAssessmentChartData($data, $SubjectCode, $SubjectComponentCode, $StudentID, $filter, $displayBy, $SubjectName)
	{
		global $ec_iPortfolio;

		$showValue = ($displayBy=="Rank") ? "OrderMeritForm" : $displayBy;

		if($displayBy=="Score")
		{
			$valueTitle = $ec_iPortfolio["score"];
			$x_text = $ec_iPortfolio['score'];
		}
		else if($displayBy=="Rank")
		{
			$valueTitle = $ec_iPortfolio["rank"];
			$x_text = $ec_iPortfolio['rank'];
		}
		else if($displayBy=="StandardScore")
		{
			$valueTitle = $ec_iPortfolio["stand_score"];
			$x_text = $ec_iPortfolio['stand_score'];
		}
		$y_text = ($filter==0) ? $ec_iPortfolio['year']."(".$ec_iPortfolio['semester'].")" : $ec_iPortfolio['year'];

		$maxTotal = 100;
		$minTotal = 0;
		$yearStr = "&title=;";
		$dataStr = "&data1=".$SubjectName.";";
		# data for plotting graph
		if(sizeof($data)>0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				if(!($displayBy=="StandardScore" && $data[$i][$showValue]=="--") && !($displayBy=="Rank" && $data[$i][$showValue]<=0))
				{
					$yearStr .= $data[$i]["YearSem"].";";
					$dataStr .= $data[$i][$showValue].";";

					$StandScoreArr[] = $data[$i]["StandardScore"];
				}
			}

			if($displayBy=="Score")
			{
				// get max score
				$maxScore = $this->getAssessmentMaxScore($SubjectCode, $filter, $SubjectComponentCode);
				$maxTotal = ($maxScore<100) ? 100 : ($maxScore-($maxScore%50)+50);
				$minTotal = 0;
			}
			else if($displayBy=="Rank")
			{
				$rankTotal = $this->getAssessmentMaxTotalRank($SubjectCode, $filter, $SubjectComponentCode);
				$rankTotal = ($rankTotal!=0) ? $rankTotal-($rankTotal%100)+100 : 0;
				$maxTotal = $rankTotal + 1;
				$minTotal = 1;
			}
			else if($displayBy=="StandardScore" && is_array($StandScoreArr))
			{
				sort($StandScoreArr);
				$minStandScore = $StandScoreArr[0];
				$maxStandScore = $StandScoreArr[sizeof($StandScoreArr)-1];
				$minTotal = floor($minStandScore);
				$maxTotal = ceil($maxStandScore);

				if($minTotal==$maxTotal)
					$minTotal = $minTotal-1;
			}
		}
		$and_code = "%26";
		$style_url = ($displayBy=="Rank") ? "ec_max=$minTotal".$and_code."ec_min=$maxTotal" : "ec_max=$maxTotal".$and_code."ec_min=$minTotal";

		$xy_text = $x_text."_".$y_text;
		$axis_url = "xy=$xy_text";
		$chart_data = $yearStr.$dataStr;

		return array($style_url, $axis_url, $chart_data);
	}

	# Retrieve all Asssessment Subject Record By SubjectCode
	function returnAsssessmentSubjectRecordBySubjectCode($StudentID, $SubjectCode, $filter, $SubjectCmpCode="", $Year="")
	{
		global $eclass_db, $ec_iPortfolio;

		$conds = ($filter==0) ? " AND IsAnnual = 0 " : " AND IsAnnual = 1 ";
		$conds .= ($SubjectCmpCode=="") ? "AND SubjectCode = '".$SubjectCode."' AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '') " : "AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode = '".$SubjectCmpCode."' ";
		$conds .= ($Year=="") ? "" : "AND Year = '$Year' ";

		# Retrieve all RAW Records
		$sql = "	SELECT
						Semester,
						Year,
						Score,
						Grade,
						OrderMeritForm,
						OrderMeritFormTotal
					FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
					WHERE
							UserId='$StudentID'
							$conds
					ORDER BY
							Year ASC
				";

				//echo $sql;
		$row = $this->returnArray($sql);

		// prepare a year semester array for sorting
		if($filter==0)
		{
			if($year=="")
			{
				# Retrieve distinct year
				$sql = "SELECT
							DISTINCT Year
						FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
						WHERE
							UserId='$StudentID'
							$conds
						ORDER BY
							Year ASC
						";
				$yearArr = $this->returnVector($sql);
			}
			else
				$yearArr = array($Year);

			list($full_semester_arr, $semester_arr) = $this->getSemesterNameArrFromIP();

			for($k=0; $k<sizeof($yearArr); $k++)
			{
				$yr = $yearArr[$k];
				for($m=0; $m<sizeof($semester_arr); $m++)
				{
					$sem = $semester_arr[$m];
					$YearSemArr[] = array($yr, $sem);
				}
			}
		}

		for($i=0; $i<sizeof($row); $i++)
		{
			$this_year = trim($row[$i]["Year"]);
			$this_sem = trim($row[$i]["Semester"]);
			$row[$i]["StandardScore"] = $this->returnStandardScore($StudentID, $row[$i]["Score"], $this_year, $this_sem, $SubjectCode, $SubjectComponentCode);

			if($filter==0)
			{
				$sem_name = trim(preg_replace($this->semester_patterns, "", $this_sem));
				$row[$i]["YearSem"] = $this_year."(".$sem_name.")";
				for($j=0; $j<sizeof($YearSemArr); $j++)
				{
					list($t_year, $t_sem) = $YearSemArr[$j];
					if($this_year==$t_year && $this_sem==$t_sem)
					{
						$newArr[$j] = $row[$i];
						break;
					}
				}
			}
			else
			{
				$row[$i]["YearSem"] = $row[$i]["Year"];
			}
		}

		// array sorting
		if($filter==0 && is_array($newArr))
		{
			ksort($newArr);
			$row = array_values($newArr);
		}

		return $row;
	}
	
	# Retrieve Student Standard Score
	function returnStandardScore($StudentID, $score, $year, $sem, $subject_code, $cmp_subject_code)
	{
		global $eclass_db, $intranet_db, $ec_iPortfolio;

		$z = "--";
		if(!empty($score))
		{
			# get class level of this record
			$levelID = $this->returnStudentAssessmentClassLevel($StudentID, $year);

			$conds = " AND y.YearID = '".$levelID."' ";
			$conds .= ($cmp_subject_code=="") ? " AND (ASSR.SubjectComponentCode IS NULL OR ASSR.SubjectComponentCode = '')" : " AND ASSR.SubjectComponentCode = '$cmp_subject_code'";
			$conds .= ($sem==$ec_iPortfolio['overall_result']) ? " AND ASSR.IsAnnual = 1 " : " AND ASSR.Semester = '$sem' ";

			# Retrieve ranks for calculating standard deviation
			$sql = "SELECT
						ASSR.Score
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS ASSR
						LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ ON SUBJ.EN_SNAME=ASSR.SubjectCode
						inner join {$intranet_db}.YEAR_CLASS_USER as ycu on ASSR.UserID = ycu.UserID
						inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
						inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
						inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
							and ASSR.Year = ay.YearNameEN
					WHERE
						ASSR.Year = '$year'
						AND ASSR.SubjectCode = '$subject_code'
						AND (ASSR.Score > 0 OR (ASSR.Score=0 AND ASSR.Grade<>'' AND ASSR.OrderMeritForm>0))
						$conds
					";
			$ScoreArr = $this->returnVector($sql);
			$sd = round(standard_deviation($ScoreArr), 2);

			if($sd>0)
			{
				# Calculate the standard score
				$mean = array_sum($ScoreArr)/sizeof($ScoreArr);
				$z = round((($score-$mean)/$sd), 2);
			}
		}
		return $z;
	}

	function returnStudentAssessmentClassLevel($UserID, $Year)
	{
		global $intranet_db, $eclass_db;

		$sql = "SELECT DISTINCT
					y.YearID
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.UserID = '".$UserID."'
					AND a.Year = '".$Year."'";
		$row = $this->returnVector($sql);

		return $row[0];
	}
	
	function returnStudentsClassLevel($UserIDAry)
	{
	    global $intranet_db, $eclass_db;
	    
	    $sql = "SELECT DISTINCT
            	    y.YearID
            	FROM
            	    {$intranet_db}.YEAR_CLASS_USER as ycu
            	    inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
            	    inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
            	    inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
            	WHERE
            	    ycu.UserID IN ('".implode('\',\'',$UserIDAry)."')";
	    
	    $row = $this->returnVector($sql);
	    
	    return $row;
	}
	

	function getAssessmentMaxScore($SubjectCode, $isAnnual, $SubjectCmpCode="")
	{
		global $eclass_db;

		$conds .= ($isAnnual) ? " WHERE IsAnnual = 1" : " WHERE IsAnnual = 0";
		if($SubjectCode!="")
		{
			$conds .= ($SubjectCmpCode!="") ? " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode = '".$SubjectCmpCode."'" : " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode IS NULL";
		}
		$sql = "SELECT
					MAX(Score)
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				$conds
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	# Retrieve the max score of the subject
	function getAssessmentMaxTotalRank($SubjectCode, $isAnnual, $SubjectCmpCode="")
	{
		global $eclass_db;

		$conds .= ($isAnnual) ? " WHERE IsAnnual = 1" : " WHERE IsAnnual = 0";
		if($SubjectCode!="")
		{
			$conds .= ($SubjectCmpCode!="") ? " AND SubjectCode = '".$SubjectCode."' AND SubjectComponentCode = '".$SubjectCmpCode."'" : " AND SubjectCode = '".$SubjectCode."' AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '')";
		}

		$sql = "SELECT
					MAX(OrderMeritForm)
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				$conds
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	## Function to gerenate overall statistic data
	function getOverallDataBasedOnSubject($TargetSubjectArr, $SubjectNameArray, $TargetClassArr, $Year, $Semester)
	{
		global $eclass_db, $ck_course_id, $ec_iPortfolio, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$course_db = $this->course_db;

		$d = 0;
		$MaxValue = 0;
		for($i=0; $i<sizeof($TargetSubjectArr); $i++)
		{
			$SubjectArr = explode("###", $TargetSubjectArr[$i]);
			$s_code = trim($SubjectArr[0]);
			$s_cmp_code = trim($SubjectArr[1]);

			$cond1 = ($s_cmp_code!="") ? " AND a.SubjectComponentCode = '".$s_cmp_code."'" : " AND (a.SubjectComponentCode = '' OR a.SubjectComponentCode IS NULL)";
			$cond2 = ($Semester==$ec_iPortfolio['overall_result']) ? " AND a.IsAnnual = '1'" : " AND a.Semester = '".$Semester."'";
			$TargetClassList = "'".implode("','", $TargetClassArr)."'";

			$sql = "SELECT Distinct
						$CLASSNAME_FIELD as ClassName,
						AVG(a.Score) as Average
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
						inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
						inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
						inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
							and a.Year = ay.YearNameEN
					WHERE
						a.SubjectCode = '".$s_code."'
						AND a.Year = '".$Year."'
						AND $CLASSNAME_FIELD in ({$TargetClassList})
						AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
						$cond1
						$cond2
					GROUP BY
						$CLASSNAME_FIELD
					ORDER BY
						$CLASSNAME_FIELD
					";
			$ClassResultArray = $this->returnArray($sql);

			$FinalResultArray[$TargetSubjectArr[$i]] = $ClassResultArray;
		}

		if(is_array($FinalResultArray))
		{
			$chart_title = "&title=;";
			foreach($FinalResultArray as $TargetSubject => $ClassDataArr)
			{
				//$TargetSubject = str_replace("###", " ", $TargetSubject);
				//$chart_title .= urlencode($TargetSubject).";";

				$SubjectName = trim($SubjectNameArray[$TargetSubject]);
				$SubjectName = str_replace("&nbsp;", "_", $SubjectName);
				$SubjectName = str_replace("&", "%26", $SubjectName);
				$SubjectName = str_replace("_", "&nbsp;", $SubjectName);

				$chart_title .= $SubjectName.";";
				$TmpSubjectArr[] = $TargetSubject;
				for($j=0; $j<sizeof($ClassDataArr); $j++)
				{
					list($cname, $average) = $ClassDataArr[$j];
					$cname = trim($cname);
					$TmpResultArr[$cname][$TargetSubject] = round($average,1);
				}
			}

			if(is_array($TmpResultArr))
			{
				foreach($TmpResultArr as $TargetClassName => $DataArr)
				{
					$d++;
					$chart_data .= "&data".$d."=".$TargetClassName.";";
					for($j=0; $j<sizeof($TmpSubjectArr); $j++)
					{
						$subj = trim($TmpSubjectArr[$j]);
						$value = ($DataArr[$subj]=="") ? 0 : $DataArr[$subj];
						$chart_data .= $value.";";

						if($value>$MaxValue)
							$MaxValue = $value;
					}
				}
			}
		}
		return array($chart_title, $chart_data, $MaxValue);
	}
	
	function getOverallDataBasedOnClass($TargetSubjectArr, $SubjectNameArray, $TargetClassArr, $Year, $Semester)
	{
		global $eclass_db, $ck_course_id, $ec_iPortfolio, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$course_db = $this->course_db;

		$d = 0;
		$MaxValue = 0;
		$SubjCodeArr = array();
		$SubjCmpCodeArr = array();

		for($j=0; $j<sizeof($TargetSubjectArr); $j++)
		{
			$SubjectArr = explode("###", $TargetSubjectArr[$j]);
			$s_code = trim($SubjectArr[0]);
			$s_cmp_code = trim($SubjectArr[1]);
			if($s_code!="" && !in_array($s_code, $SubjCodeArr))
				$SubjCodeArr[] = $s_code;
			if($s_cmp_code!="" && !in_array($s_cmp_code, $SubjCmpCodeArr))
				$SubjCmpCodeArr[] = $s_cmp_code;
		}
		$SubjectList = "'".implode("', '", $SubjCodeArr)."'";
		$CmpSubjectList = (!empty($SubjCmpCodeArr)) ? "'".implode("', '", $SubjCmpCodeArr)."'" : "";

		for($i=0; $i<sizeof($TargetClassArr); $i++)
		{
			$cname = $TargetClassArr[$i];
			$cond1 = ($Semester==$ec_iPortfolio['overall_result']) ? " AND a.IsAnnual = '1'" : " AND a.Semester = '".$Semester."'";
			$cond2 = ($CmpSubjectList!="") ? " AND a.SubjectCode IN ({$SubjectList}) AND ((a.SubjectComponentCode = '' OR a.SubjectComponentCode IS NULL) OR (a.SubjectComponentCode IN ({$CmpSubjectList})))" : " AND a.SubjectCode IN ({$SubjectList}) AND (a.SubjectComponentCode = '' OR a.SubjectComponentCode IS NULL)";

			$sql = "SELECT Distinct
						a.SubjectCode,
						a.SubjectComponentCode,
						AVG(a.Score) as Average
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
						inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
						inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
						inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
							and a.Year = ay.YearNameEN
					WHERE
						a.Year = '".$Year."'
						AND $CLASSNAME_FIELD = '{$cname}'
						AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
						$cond1
						$cond2
					GROUP BY
						a.SubjectCode,
						a.SubjectComponentCode
					ORDER BY
						a.SubjectCode,
						a.SubjectComponentCode
					";
			$SubjectResultArray = $this->returnArray($sql);

			$FinalResultArray[$cname] = $SubjectResultArray;
		}

		if(is_array($FinalResultArray))
		{
			$chart_title = "&title=;";
			foreach($FinalResultArray as $TargetClass => $SubjectDataArr)
			{
				$chart_title .= $TargetClass.";";
				for($j=0; $j<sizeof($SubjectDataArr); $j++)
				{
					list($s_code, $s_cmp_code, $average) = $SubjectDataArr[$j];
					$sname = ($s_cmp_code!="") ? $s_code."###".$s_cmp_code : $s_code;
					$sname = trim($sname);
					$TmpResultArr[$sname][$TargetClass] = round($average,1);
				}
			}

			if(is_array($TmpResultArr))
			{
				foreach($TmpResultArr as $TargetSubject => $DataArr)
				{
					if(!in_array($TargetSubject, $TargetSubjectArr))
						continue;

					$d++;
					//$TargetSubject = str_replace("###", " ", $TargetSubject);
					$SubjectName = trim($SubjectNameArray[$TargetSubject]);
					$SubjectName = str_replace("&nbsp;", "_", $SubjectName);
					$SubjectName = str_replace("&", "%26", $SubjectName);
					$SubjectName = str_replace("_", "&nbsp;", $SubjectName);

					$chart_data .= "&data".$d."=".$SubjectName.";";
					for($j=0; $j<sizeof($TargetClassArr); $j++)
					{
						$class = trim($TargetClassArr[$j]);
						$value = ($DataArr[$class]=="") ? 0 : $DataArr[$class];
						$chart_data .= $value.";";

						if($value>$MaxValue)
							$MaxValue = $value;
					}
				}
			}
		}

		return array($chart_title, $chart_data, $MaxValue);
	}
	
	function generateLevelPerformanceTable($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester, $type)
	{
		global $ec_iPortfolio;

		$data = $this->returnLevelPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester);

		if($Semester=="")
		{
			# retrieve semester
			list($semester_arr, $short_semester_arr) = $this->getSemesterNameArrFromIP();
			$semester_arr[] = $ec_iPortfolio["overall_result"];
		}
		else
		{
			$semester_arr[] = $Semester;
		}

		$ValidSemArray = array();
		if(sizeof($data)>0)
		{
			for($i=0; $i<sizeof($data); $i++)
			{
				list($class, $sem, $score) = $data[$i];
				$sem = ($sem=="") ? $ec_iPortfolio["overall_result"] : $sem;
				$dataArr[$class][$sem]["scores"][] = $score;
				$dataArr[$class][$sem]["sum"] = $dataArr[$class][$sem]["sum"] + $score;

				if(!in_array($sem, $ValidSemArray))
				{
					$ValidSemArray[] = $sem;
				}
			}

			# sorting the semester array
			for($i=0; $i<sizeof($semester_arr); $i++)
			{
				if(in_array($semester_arr[$i], $ValidSemArray))
				{
					$SortedSemArray[] = $semester_arr[$i];
				}
			}
			$chart_title = "&title=;".implode(";", $SortedSemArray).";";

			#calculate the mean, sd, max and min
			foreach($dataArr as $class => $SemDataArr)
			{
				foreach($SemDataArr as $sem => $scoreArr)
				{
					$dataArr[$class][$sem]["mean"] = round($scoreArr["sum"]/sizeof($scoreArr["scores"]), 2);
					$dataArr[$class][$sem]["sd"] = round(standard_deviation($scoreArr["scores"]), 2);
					$dataArr[$class][$sem]["max"] = max($scoreArr["scores"]);
					$dataArr[$class][$sem]["min"] = min($scoreArr["scores"]);
				}
			}

			$d = 0;
			$MaxValue = 0;
			foreach($dataArr as $class => $SemDataArr)
			{
				$d++;
				$chart_data .= "&data".$d."=".$class.";";
				for($i=0; $i<sizeof($SortedSemArray); $i++)
				{
					$sem = $SortedSemArray[$i];
					$value = ($dataArr[$class][$sem][$type]=="") ? 0 : $dataArr[$class][$sem][$type];
					$chart_data .= $value.";";
					$MaxValue = ($value>$MaxValue) ? $value : $MaxValue;
				}
			}
		}

		$chart_param = $chart_title.$chart_data;
		if($type=="sd")
			$MaxValue = $MaxValue-($MaxValue%5)+5;
		else
			$MaxValue = ($MaxValue<100) ? 100 : ($MaxValue-($MaxValue%50)+50);

		return array($chart_param, $MaxValue);
	}

	function generateClassPerformanceTable($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester, $LastSemester,$ParPageInfoArr,$ParStyle="class")
	{
		global $ec_iPortfolio,$image_path;
		

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$data = $this->returnClassPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester,0);
		//debug_r($Semester);
		# retrieve semester
		if($Semester=="")
		{
			list($semester_arr, $short_semester_arr) = $this->getSemesterNameArrFromIP();
			$semester_arr[] = trim($ec_iPortfolio['overall_result']);
		}
		else
			$semester_arr[] = trim($Semester);

		if(sizeof($data)>0)
		{
			$total_student_num = 0;
			$total_score = 0;
			for($i=0; $i<sizeof($data); $i++)
			{
				list($cname, $sem, $score) = $data[$i];
				$sem = ($sem=="") ? trim($ec_iPortfolio['overall_result']) : trim($sem);
				$dataArr[$cname][$sem]["scores"][] = $score;
				$dataArr[$cname][$sem]["sum"] = $dataArr[$cname][$sem]["sum"] + $score;

				$formDataArr[$sem]["scores"][] = $score;
				$formDataArr[$sem]["student_num"] = $formDataArr[$sem]["student_num"] + 1;
				$formDataArr[$sem]["total_score"] = $formDataArr[$sem]["total_score"] + $score;
			}

			// calculate statistic
			// this part is for individual classes
			if(sizeof($dataArr)>0)
			{
				foreach($dataArr as $class => $semArr)
				{
					if(sizeof($semArr)>0)
					{
						foreach($semArr as $semester => $scoreArr)
						{
							$student_num = sizeof($scoreArr["scores"]);
							$dataArr[$class][$semester]["mean"] = ($student_num!=0) ? round($scoreArr["sum"]/$student_num, 2) : 0;
							$dataArr[$class][$semester]["sd"] = round(standard_deviation($scoreArr["scores"]), 2);
							$dataArr[$class][$semester]["max"] = ($student_num!=0) ? max($scoreArr["scores"]) : 0;
							$dataArr[$class][$semester]["min"] = ($student_num!=0) ? min($scoreArr["scores"]) : 0;
						}
					}
				}
			}

			// this part is for the whole form
			if(sizeof($formDataArr)>0)
			{
				foreach($formDataArr as $f_sem => $f_values)
				{
					$formDataArr[$f_sem]["mean"] = round($f_values["total_score"]/$f_values["student_num"], 2);
					$formDataArr[$f_sem]["sd"] = round(standard_deviation($f_values["scores"]), 2);
					$formDataArr[$f_sem]["max"] = max($f_values["scores"]);
					$formDataArr[$f_sem]["min"] = min($f_values["scores"]);
				}
			}
			//table start
			$rx =	"
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='center'>
										<table width='90%' border='0' cellpadding='5' cellspacing='0'>
						";
			
			if($ParStyle=="term")
			{
				$rx .=	"
									<tr class='tabletop'>
										<td class='tabletopnolink' align='left'>".$ec_iPortfolio['term']."</td>
										<td class='tabletopnolink'>".$ec_iPortfolio['form']."</td>
										<td class='tabletopnolink' align='left' width='20'>&nbsp;</td>
								";
			}
			else
			{
				$rx .=	"
									<tr class='tabletop'>
										<td class='tabletopnolink'>".$ec_iPortfolio['form']."</td>
										<td class='tabletopnolink' align='left'>".$ec_iPortfolio['term']."</td>
										<td class='tabletopnolink' align='left' width='20'>&nbsp;</td>
								";
			}

			$level_params = "SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Year=$Year&Semester=$Semester&Form=$Form";

			$rx .= 	"<td align='center'><a href=\"javascript:newWindow('stat_level_barchart.php?$level_params&type=mean',22)\"  title='".$ec_iPortfolio['level_stat_graph']."' class='tabletoplink'>".$ec_iPortfolio['mean']."&nbsp;<img src='$image_path/2009a/icon_files/icon_resultview.gif' width='20' height='20' align='absmiddle' border='0'></a></td>";
			$rx .= 	"<td align='center'><a href=\"javascript:newWindow('stat_level_barchart.php?$level_params&type=sd',22)\"  title='".$ec_iPortfolio['level_stat_graph']."' class='tabletoplink'>".$ec_iPortfolio['standard_deviation']."&nbsp;<img src='$image_path/2009a/icon_files/icon_resultview.gif' width='20' height='20' align='absmiddle' border='0'></a></td>";
			$rx .= 	"<td align='center'><a href=\"javascript:newWindow('stat_level_barchart.php?$level_params&type=max',22)\"  title='".$ec_iPortfolio['level_stat_graph']."' class='tabletoplink'>".$ec_iPortfolio['highest_score']."&nbsp;<img src='$image_path/2009a/icon_files/icon_resultview.gif' width='20' height='20' align='absmiddle' border='0'></a></td>";
			$rx .= 	"<td align='center'><a href=\"javascript:newWindow('stat_level_barchart.php?$level_params&type=min',22)\"  title='".$ec_iPortfolio['level_stat_graph']."' class='tabletoplink'>".$ec_iPortfolio['lowest_score']."&nbsp;<img src='$image_path/2009a/icon_files/icon_resultview.gif' width='20' height='20' align='absmiddle' border='0'></a></td>";
			$rx .= "</tr>";

			$first=1;
			if($ParStyle=="term") // Group by "term" start
			{
				$form_display = "[".$this->getClassLevelName($Form)."]"; //e.g. [S5]
				for($j=0; $j<sizeof($semester_arr); $j++)
				{
					$sem = trim($semester_arr[$j]);
					if(sizeof($formDataArr[$sem])!=0)
					{
					/*
					if($first==1)
					{
						$rowCount = count($dataArr)+1;
						$rx .= "<tr class='tablegreenrow2'><td class='tabletext' rowspan=$rowCount>".$sem."</td>";

						$first = 0;
					}
					else
						$rx .= "<tr class='tablegreenrow2'><td class='tabletext'>&nbsp;</td>";
					*/
						$bclass = ($d%2==0) ? "class='tablerow1'" : "class='tablerow2'";
						if($sem==trim($ec_iPortfolio['overall_result'])){$bclass ="class=tablegreenrow2";}
						$rowCount = count($dataArr)+1;
					//$rx .= "<tr $bclass><td class='tabletext' rowspan=$rowCount>".$sem."</td>";
					
						$trend_up = "<font color='#9988FF'>&#8593;</font>";
						$trend_down = "<font color='#FF8899'>&#8595;</font>";
						$max_mean = 0;
						
						foreach($dataArr as $class => $ClassArr)
						{
							$class_mean = $dataArr[$class][$sem]["mean"];
							$class_sd = $dataArr[$class][$sem]["sd"];
							$class_max = $dataArr[$class][$sem]["max"];
							$class_min = $dataArr[$class][$sem]["min"];
							
							if($class_mean!=$formDataArr[$sem]["mean"])
							{
								$mean_trend = ($class_mean>$formDataArr[$sem]["mean"]) ? $trend_up : $trend_down;
								$class_mean .= $mean_trend;
							}
							
							$class_params = "SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Year=$Year&Semester=$sem&ClassName=$class";

							$class_max = "<a href=\"javascript:newWindow('stat_score_details.php?$class_params&type=1&Score=".$dataArr[$class][$sem]["max"]."',22)\" class='link_a' title='".$ec_iPortfolio['class_highest_score_student']."'>".$class_max."</a>";
							$class_min = "<a href=\"javascript:newWindow('stat_score_details.php?$class_params&type=0&Score=".$dataArr[$class][$sem]["min"]."',22)\" class='link_a' title='".$ec_iPortfolio['class_lowest_score_student']."'>".$class_min."</a>";
							//$class_max = "<a href=\"stat_score_details.php?$class_params&type=1&Score=".$dataArr[$class][$sem]["max"]."\" class='link_a' title='".$ec_iPortfolio['class_highest_score_student']."'>".$class_max."</a>";
							//$class_min = "<a href=\"stat_score_details.php?$class_params&type=0&Score=".$dataArr[$class][$sem]["min"]."\" class='link_a' title='".$ec_iPortfolio['class_lowest_score_student']."'>".$class_min."</a>";

							if($first==1)
							{
								$rx .= "<tr $bclass><td class='tabletext' rowspan=$rowCount>".$sem."</td>";
								$first = 0;
							}
							else
							{
								$rx .= "<tr $bclass>";
							}

							$rx .= "<td class='tabletext tablerow_underline' align='left'><a href=\"javascript:newWindow('stat_student_list.php?$class_params',22)\" title='".$ec_iPortfolio['student_result_list'] ."' class='link_a'><b>".$class."</b></a></td>";
							$rx .= "<td class='tabletext tablerow_underline' align='left'><strong><a href=\"javascript:newWindow('stat_class_barchart.php?$class_params',22)\"  title='".$ec_iPortfolio['class_dist_graph']."'><img src='$image_path/2009a/icon_files/icon_resultview.gif' width='20' height='20' align='absmiddle' border='0'></a></strong></td>";
							$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_mean."</td>";
							$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_sd."</td>";
							$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_max."</td>";
							$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_min."</td>";
							$rx .= "</tr>";
						}
						// Whole Class Data Display
						//debug_r($dataArr);
						//$style = ($sem==$LastSemester || $sem==$Semester) ? "" : "style='border-bottom: 1px dashed #DDDDDD;'";
						$form_max = "<a href=\"javascript:newWindow('stat_score_details.php?type=1&SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Form=$Form&Semester=$sem&Year=$Year&Score=".$formDataArr[$sem]["max"]."',22)\" class='link_a' title='".$ec_iPortfolio['level_highest_score_student']."'>".$formDataArr[$sem]["max"]."</a>";
						$form_min = "<a href=\"javascript:newWindow('stat_score_details.php?type=0&SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Form=$Form&Semester=$sem&Year=$Year&Score=".$formDataArr[$sem]["min"]."',22)\" class='link_a' title='".$ec_iPortfolio['level_highest_score_student']."'>".$formDataArr[$sem]["min"]."</a>";

						$rx .= "<tr $bclass>";
						$rx .= "<td class='tabletext tablerow_underline' align='left'>".$form_display."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='left'>&nbsp;</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$formDataArr[$sem]["mean"]."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$formDataArr[$sem]["sd"]."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$form_max."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$form_min."</td>";
						$rx .= "</tr>";
					}
				}
			}
			else
			{ // Group by "Class" start
				$form_display = "[".$this->getClassLevelName($Form)."]"; //e.g. [S5]
				for($j=0; $j<sizeof($semester_arr); $j++) //Semester Loop
				{
					$sem = trim($semester_arr[$j]);
					if(sizeof($formDataArr[$sem])!=0)
					{
						if($first==1)
						{
							$rx .= "<tr class='tablegreenrow2'><td class='tabletext'>".$form_display."</td>";
							$first = 0;
						}
						else
							$rx .= "<tr class='tablegreenrow2'><td class='tabletext'>&nbsp;</td>";

						//$style = ($sem==$LastSemester || $sem==$Semester) ? "" : "style='border-bottom: 1px dashed #DDDDDD;'";
						$form_max = "<a href=\"javascript:newWindow('stat_score_details.php?type=1&SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Form=$Form&Semester=$sem&Year=$Year&Score=".$formDataArr[$sem]["max"]."',22)\" class='link_a' title='".$ec_iPortfolio['level_highest_score_student']."'>".$formDataArr[$sem]["max"]."</a>";
						$form_min = "<a href=\"javascript:newWindow('stat_score_details.php?type=0&SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Form=$Form&Semester=$sem&Year=$Year&Score=".$formDataArr[$sem]["min"]."',22)\" class='link_a' title='".$ec_iPortfolio['level_highest_score_student']."'>".$formDataArr[$sem]["min"]."</a>";

						$rx .= "<td class='tabletext tablerow_underline' align='left'>".$sem."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='left'>&nbsp;</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$formDataArr[$sem]["mean"]."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$formDataArr[$sem]["sd"]."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$form_max."</td>";
						$rx .= "<td class='tabletext tablerow_underline' align='center'>".$form_min."</td>";
						$rx .= "</tr>";
					}
				}

				$trend_up = "<font color='#9988FF'>&#8593;</font>";
				$trend_down = "<font color='#FF8899'>&#8595;</font>";
				$max_mean = 0;
				$d = 0;
			
				if(sizeof($dataArr)!=0) //start
				{
					$i = 0;
					
					foreach($dataArr as $class => $semArr) //Class Loop
					{
						if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
						{
							$first = 1;
							for($j=0; $j<sizeof($semester_arr); $j++)
							{
								$sem = trim($semester_arr[$j]);
								if(sizeof($semArr[$sem])!=0)
								{
									if($first==1)
									{
										$d++;
										//$bgcolor = ($d%2==0) ? "#FFFFFF" : "#EEEEEE";
										$bclass = ($d%2==0) ? "class='tablerow1'" : "class='tablerow2'";
										$rx .=	"
															<tr ".$bclass.">
																<td class='tabletext'>".$class."</td>
														";
										$first = 0;
									}
									else
									{
										$rx .= "<tr ".$bclass."><td class='tabletext'>&nbsp;</td>";
									}
									
									//$stylecolor = ($d%2==1) ? "#FFFFFF" : "#DDDDDD";
									//$style = ($sem==$LastSemester || $sem==$Semester) ? "" : "style='border-bottom: 1px dashed $stylecolor;'";
									
									$class_mean = $dataArr[$class][$sem]["mean"];
									$class_sd = $dataArr[$class][$sem]["sd"];
									$class_max = $dataArr[$class][$sem]["max"];
									$class_min = $dataArr[$class][$sem]["min"];
									
									if($class_mean!=$formDataArr[$sem]["mean"])
									{
										$mean_trend = ($class_mean>$formDataArr[$sem]["mean"]) ? $trend_up : $trend_down;
										$class_mean .= $mean_trend;
									}
									
									$class_params = "SubjectCode=".urlencode($SubjectCode)."&SubjectComponentCode=".urlencode($SubjectComponentCode)."&Year=$Year&Semester=$sem&Form=$Form&ClassName=$class&Page=".$ParPageInfoArr[1]."&PageDivision=".$ParPageInfoArr[0];
									$class_max = "<a href=\"javascript:newWindow('stat_score_details.php?$class_params&type=1&Score=".$dataArr[$class][$sem]["max"]."',22)\" class='link_a' title='".$ec_iPortfolio['class_highest_score_student']."'>".$class_max."</a>";
									$class_min = "<a href=\"javascript:newWindow('stat_score_details.php?$class_params&type=0&Score=".$dataArr[$class][$sem]["min"]."',22)\" class='link_a' title='".$ec_iPortfolio['class_lowest_score_student']."'>".$class_min."</a>";
									
//									$rx .= "<td  class='tabletext tablerow_underline' align='left'><a href=\"stat_student_list.php?$class_params\" title='".$ec_iPortfolio['student_result_list'] ."' class='link_a'><b>".$sem."</b></a></td>";
									$rx .= "<td class='tabletext tablerow_underline' align='left'><a href=\"javascript:newWindow('stat_student_list.php?$class_params',22)\" title='".$ec_iPortfolio['student_result_list'] ."' class='link_a'><b>".$sem."</b></a></td>";
									$rx .= "<td class='tabletext tablerow_underline' align='left'><strong><a href=\"javascript:newWindow('stat_class_barchart.php?$class_params',22)\"  title='".$ec_iPortfolio['class_dist_graph']."'><img src='$image_path/2009a/icon_files/icon_resultview.gif' width='20' height='20' align='absmiddle' border='0'></a></strong></td>";
									$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_mean."</td>";
									$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_sd."</td>";
									$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_max."</td>";
									$rx .= "<td class='tabletext tablerow_underline' align='center'>".$class_min."</td>";
									$rx .= "</tr>";
								}
							}
						}
						
						$i++;
					} // END For Loop
				}
			} //END Group by Class
			$rx .=	"
											</table>
										</td>
									</tr>
								</table>
							";
		}
		$chart_param = $chart_title.$chart_data;
		$max_mean = ($max_mean<100) ? 100 : ($max_mean-($max_mean%50)+50);

		return array($rx, $chart_param, $max_mean);
	}
	
	function returnAssessmentForm($SubjectCode, $SubjectComponentCode, $Year)
	{
		$classArr = $this->returnAssessmentClass($SubjectCode, $SubjectComponentCode, $Year);

		for($i=0; $i<sizeof($classArr); $i++)
		{
			list($ClassLevelID, $LevelName) = $this->getClassLevel($classArr[$i]);
			$exist = 0;
			for($j=0; $j<sizeof($formArr); $j++)
			{
				if($ClassLevelID==$formArr[$j][0])
					$exist = 1;
			}
			if($exist==0)
				$formArr[] = array($ClassLevelID, $LevelName);
		}

		return $formArr;
	}
	
	function returnClassPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester="",$navigation="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$conds = ($SubjectComponentCode=="") ? " AND (a.SubjectComponentCode IS NULL OR a.SubjectComponentCode = '')" : " AND a.SubjectComponentCode = '$SubjectComponentCode'";

		if($Semester!="")
		{
			$conds .= ($Semester==$ec_iPortfolio['overall_result'] || $Semester==$ec_iPortfolio['overall']) ? " AND a.IsAnnual = 1" : " AND a.IsAnnual = 0 AND a.Semester = '$Semester'";
		}
		if($navigation==2)
		{
			$sql = "SELECT DISTINCT
					a.Semester
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.Year = '$Year'
					AND y.YearID = '$Form'
					AND a.SubjectCode = '$SubjectCode'
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					$CLASSNAME_FIELD ASC
				";

				//echo "SQL: ".$sql;

		}else	if($navigation==1)
		{

		$sql = "SELECT DISTINCT
					$CLASSNAME_FIELD as ClassName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.Year = '$Year'
					AND y.YearID = '$Form'
					AND a.SubjectCode = '$SubjectCode'
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					$CLASSNAME_FIELD ASC
				";
						//echo "SQL: ".$sql;

		}else{

		$sql = "SELECT
					$CLASSNAME_FIELD as ClassName,
					a.Semester,
					a.Score
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as a
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on a.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and a.Year = ay.YearNameEN
				WHERE
					a.Year = '$Year'
					AND y.YearID = '$Form'
					AND a.SubjectCode = '$SubjectCode'
					AND (a.Score>0 OR (a.Score=0 AND a.Grade<>'' AND a.Grade IS NOT NULL AND a.OrderMeritForm>0))
					$conds
				ORDER BY
					$CLASSNAME_FIELD ASC,
					a.Score ASC
				";
		}
		$row = $this->returnArray($sql);


		return $row;
	}

	function getClassLevelName($LevelID)
	{
		if ($this->level_name_by_id==null)
		{
			$this->preloadClassLevelNameByID();
		}

		return $this->level_name_by_id[$LevelID];
	}

	function returnLevelPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_db;
		$CLASSNAME_FIELD = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

		$conds = ($SubjectComponentCode=="") ? " AND (ASSR.SubjectComponentCode IS NULL OR ASSR.SubjectComponentCode = '')" : " AND ASSR.SubjectComponentCode = '$SubjectComponentCode'";
		if($Semester!="")
		{
			$conds .= ($Semester==$ec_iPortfolio['overall_result']) ? " AND ASSR.IsAnnual = 1" : " AND ASSR.IsAnnual = 0 AND ASSR.Semester = '$Semester'";
		}

		$sql = "SELECT
					$CLASSNAME_FIELD as ClassName,
					ASSR.Semester,
					ASSR.Score
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as ASSR
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on ASSR.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and ASSR.Year = ay.YearNameEN
				WHERE
					ASSR.Year = '$Year'
					AND ASSR.SubjectCode = '$SubjectCode'
					AND (ASSR.Score>0 OR (ASSR.Score=0 AND ASSR.Grade<>'' AND ASSR.Grade IS NOT NULL AND ASSR.OrderMeritForm>0))
					AND y.YearID = '$Form'
					$conds
				ORDER BY
					$CLASSNAME_FIELD, ASSR.Score ASC
				";
		$row = $this->returnArray($sql);

		return $row;
	}

	function preloadClassLevelNameByID()
	{
		global $intranet_db;

		$sql = "SELECT YearName as LevelName, YearID as ClassLevelID FROM {$intranet_db}.YEAR ";
		$row = $this->returnArray($sql);

		$Levels = array();
		for ($i=0; $i<sizeof($row); $i++)
		{
			$Levels[$row[$i]['ClassLevelID']] = $row[$i]['LevelName'];
		}

		$this->level_name_by_id = $Levels;
	}
	
	function returnIndividualClassPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $ClassName, $Semester="")
	{
		global $eclass_db, $ec_iPortfolio;

		$conds = ($SubjectComponentCode=="") ? " AND (SubjectComponentCode IS NULL OR SubjectComponentCode = '')" : " AND SubjectComponentCode = '$SubjectComponentCode'";
		if($Semester!="")
		{
			$conds .= ($Semester==$ec_iPortfolio['overall_result']) ? " AND IsAnnual = 1" : " AND IsAnnual = 0 AND Semester = '$Semester'";
		}

		$sql = "SELECT
					Semester,
					Score
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
					Year = '$Year'
					AND ClassName = '$ClassName'
					AND SubjectCode = '$SubjectCode'
					AND (Score>0 OR (Score=0 AND Grade<>'' AND Grade IS NOT NULL AND OrderMeritForm>0))
					$conds
				ORDER BY
					Score ASC
				";
		$row = $this->returnArray($sql);

		return $row;
	}
	
	function Get_Management_Advancement_DBTable_Sql($YearID='', $YearClassID='', $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID, $TargetType='')
	{
		global $PATH_WRT_ROOT, $Lang, $eclass_db;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		### Prepare Temp Table Data Before Generating the DBTable Sql
		if($TargetType!=''){ 
			if($TargetType['Target'] == 'SubjectClass'){//subject group case
				$subjectGroupID = $TargetType['SubjectGroup'];
				$name = Get_Lang_Selection('ChineseName', 'EnglishName');
				$sql = "SELECT 
							stcu.UserID, IFNULL(iu.{$name},iu.EnglishName) as StudentName, iu.ClassName, iu.ClassNumber
						FROM
							SUBJECT_TERM_CLASS_USER stcu
						INNER JOIN 
							INTRANET_USER iu
								ON stcu.UserID = iu.UserID
						WHERE
							stcu.SubjectGroupID= '$subjectGroupID'
						";
			}elseif($TargetType['Target'] == 'MonitoringGroup'){//monitoring Group
				$monitoringGroupID = $TargetType['MonitoringGroup'];
				$name = Get_Lang_Selection('ChineseName', 'EnglishName');
				$sql = "SELECT
							mgm.UserID, IFNULL(iu.{$name},iu.EnglishName) as StudentName, iu.ClassName, iu.ClassNumber
						FROM
							MONITORING_GROUP_MEMBER  mgm
						INNER JOIN
							INTRANET_USER iu
								ON mgm.UserID = iu.UserID
						WHERE
							mgm.GroupID= '$monitoringGroupID'
						AND
							mgm.RecordType = '2'
						AND 
							mgm.IsDeleted = '0'
					";
			}
			$StudentInfoArr = $this->returnResultSet($sql);
		}else{
			if ($YearClassID == '')
			{
				### Get all Students from the Form
				$ObjYear = new Year($YearID);
				$StudentInfoArr = $ObjYear->Get_All_Student();
			}
			else
			{
				### Get Students from the Class
				$ObjYearClass = new year_class($YearClassID, $GetYearDetail=false, $GetClassTeacherList=false, $GetClassStudentList=true, $GetLockedSGArr=false);
				$StudentInfoArr = $ObjYearClass->ClassStudentList;
			}
		}

		$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
		$numOfStudent = count($StudentIDArr);
		
		$FromSubjectID = ($FromSubjectID=='')? 0 : $FromSubjectID;
		$ToSubjectID = ($ToSubjectID=='')? 0 : $ToSubjectID;
		
		$FromAssessmentInfoArr = $this->Get_Student_Assessment_Result_Info($StudentIDArr, $FromSubjectID, $FromAcademicYearID, $FromYearTermID);
		$FromAssessmentInfoAssoArr = BuildMultiKeyAssoc($FromAssessmentInfoArr, array('UserID'));
		$ToAssessmentInfoArr = $this->Get_Student_Assessment_Result_Info($StudentIDArr, $ToSubjectID, $ToAcademicYearID, $ToYearTermID);
		$ToAssessmentInfoAssoArr = BuildMultiKeyAssoc($ToAssessmentInfoArr, array('UserID'));
	
		$FromYearTermID = ($FromYearTermID=='')? 0 : $FromYearTermID;
		$ToYearTermID = ($ToYearTermID=='')? 0 : $ToYearTermID;
		#J114859 
		$FromYearTermID_temp= explode('_',$FromYearTermID);
		$ToYearTermID_temp= explode('_',$ToYearTermID);
		$TermAssessment_FromCond = "TermAssessment = '0'";
		$TermAssessment_ToCond= "TermAssessment = '0'";
		if(sizeof($FromYearTermID_temp)>1){
			$FromYearTermID = $FromYearTermID_temp[0];
			$TermAssessment_FromCond = "TermAssessment = '".$FromYearTermID_temp[1]."'";
		}
		if(sizeof($ToYearTermID_temp)>1){
			$ToYearTermID= $ToYearTermID_temp[0];
			$TermAssessment_ToCond = "TermAssessment = '".$ToYearTermID_temp[1]."'";
		}
		#### Get SD / Mean START ####
		## From START ##
		$sql = "SELECT 
			* 
		FROM 
			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
		WHERE 
			AcademicYearID = '{$FromAcademicYearID}'
		AND
			YearTermID = '{$FromYearTermID}'
		AND
			SubjectID = '{$FromSubjectID}'
		AND
			SubjectComponentID = 0
		AND
			$TermAssessment_FromCond
		AND
			YearClassID = 0
		";
		$rs = $this->returnResultSet($sql);
		$fromSdMeanArr = BuildMultiKeyAssoc($rs, array('ClassLevelID'));
		## From END ##
		
		## To START ##
		$sql = "SELECT 
			* 
		FROM 
			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
		WHERE 
			AcademicYearID = '{$ToAcademicYearID}'
		AND
			YearTermID = '{$ToYearTermID}'
		AND
			SubjectID = '{$ToSubjectID}'
		AND
			SubjectComponentID = 0
		AND
			$TermAssessment_ToCond
		AND
			YearClassID = 0
		";
		$rs = $this->returnResultSet($sql);
		$toSdMeanArr = BuildMultiKeyAssoc($rs, array('ClassLevelID'));
		## To END ##
		#### Get SD / Mean END ####
		
		#### Get Year ID Mapping START ####
		$ObjYearClass = new year_class();
		$rs = $ObjYearClass->Get_All_Class_List($FromAcademicYearID);
		$fromYearClassIdArr = BuildMultiKeyAssoc($rs, array('YearClassID'), array('YearID'), true);
		
		$rs = $ObjYearClass->Get_All_Class_List($ToAcademicYearID);
		$toYearClassIdArr = BuildMultiKeyAssoc($rs, array('YearClassID'), array('YearID'), true);
		#### Get Year ID Mapping END ####
		
		$InsertArr = array();
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentInfoArr[$i]['UserID'];
			$thisStudentName = $StudentInfoArr[$i]['StudentName'];
			$thisClassName = $StudentInfoArr[$i]['ClassName'];
			$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
			
			$thisFromMark = $FromAssessmentInfoAssoArr[$thisStudentID]['Score'];
			$thisFromGrade = $FromAssessmentInfoAssoArr[$thisStudentID]['Grade'];
			$thisFromScore = $this->Get_Student_Assessment_Display_Score($thisFromMark, $thisFromGrade);
						
			$thisToMark = $ToAssessmentInfoAssoArr[$thisStudentID]['Score'];
			$thisToGrade = $ToAssessmentInfoAssoArr[$thisStudentID]['Grade'];
			$thisToScore = $this->Get_Student_Assessment_Display_Score($thisToMark, $thisToGrade);
			
			if (is_numeric($thisFromScore) && is_numeric($thisToScore)) {
				### Both numeric => Calculate the Score Difference
				$thisScoreDiff = $thisToScore - $thisFromScore;
				
				if ($thisFromScore > 0)
					$thisScoreDiffPercentage = my_round(($thisScoreDiff / $thisFromScore) * 100, 2);
				else
					$thisScoreDiffPercentage = "null";
			}
			else
			{
				### Either one is not numeric => Show "---" as the Difference
				$thisScoreDiff = "null";
				$thisScoreDiffPercentage = "null";
			}
			
			######## Calculate Z-score START ########
			#### From Z-score START ####
			$thisYearClassID = $FromAssessmentInfoAssoArr[$thisStudentID]['YearClassID'];
			$yearID = $fromYearClassIdArr[$thisYearClassID];
			$sdMean = $fromSdMeanArr[$yearID];
			$sd = $sdMean['SD'];
			$mean = $sdMean['MEAN'];
			
			if($sd > 0 && is_numeric($thisFromScore)){
				$thisFromZScore = round( ($thisFromScore - $mean) / $sd , 2);
			}else{
				$thisFromZScore = 'null';
			}
			#### From Z-score END ####
		
			#### To Z-score START ####
			$thisYearClassID = $ToAssessmentInfoAssoArr[$thisStudentID]['YearClassID'];
			$yearID = $toYearClassIdArr[$thisYearClassID];
			$sdMean = $toSdMeanArr[$yearID];
			$sd = $sdMean['SD'];
			$mean = $sdMean['MEAN'];
			
			if($sd > 0 && is_numeric($thisToScore)){
				$thisToZScore = round( ($thisToScore - $mean) / $sd , 2);
			}else{
				$thisToZScore = 'null';
			}
			#### To Z-score END ####
			
			#### Calculate Diff START ####
			if (is_numeric($thisFromZScore) && is_numeric($thisToZScore)) {
				### Both numeric => Calculate the Score Difference
				$thisZScoreDiff = $thisToZScore - $thisFromZScore;
				
				if ($thisFromZScore != 0)
					$thisZScoreDiffPercentage = my_round(($thisZScoreDiff / abs($thisFromZScore)) * 100, 2);
				else
					$thisZScoreDiffPercentage = "null";
			}
			else
			{
				### Either one is not numeric => Show "---" as the Difference
				$thisZScoreDiff = "null";
				$thisZScoreDiffPercentage = "null";
			}
			#### Calculate Diff END ####
			######## Calculate Z-score END ########
			
			$thisStudentName = $this->Get_Safe_Sql_Query($thisStudentName);
			$thisClassName = $this->Get_Safe_Sql_Query($thisClassName);
			$thisClassNumber = $this->Get_Safe_Sql_Query($thisClassNumber);
			$thisFromScore = $this->Get_Safe_Sql_Query($thisFromScore);
			$thisToScore = $this->Get_Safe_Sql_Query($thisToScore);
			$thisFromZScore = $this->Get_Safe_Sql_Query($thisFromZScore);
			$thisToZScore = $this->Get_Safe_Sql_Query($thisToZScore);
			$InsertArr[] = " ('$thisStudentID', '$thisStudentName', '$thisClassName', '$thisClassNumber', '$thisFromScore', $thisFromZScore, '$thisToScore', $thisToZScore, $thisScoreDiff, $thisScoreDiffPercentage, $thisZScoreDiff, $thisZScoreDiffPercentage) ";
		}
		
		### Create Temp Table
		$sql = "Create Temporary Table If Not Exists TMP_IPF_MGMT_ADVANCEMENT (
					RecordID Int(8) NOT NULL AUTO_INCREMENT,
					StudentID Int(11) Default Null,
					StudentName varchar(255) Default Null,
					ClassName varchar(255) Default Null,
					ClassNumber Int(8) Default Null,
					FromScore varchar(255) Default Null,
					FromZScore varchar(255) Default Null,
					ToScore varchar(255) Default Null,
					ToZScore varchar(255) Default Null,
					ScoreDiff float Default Null,
					ScoreDiffPercentage float Default Null,
					ZScoreDiff float Default Null,
					ZScoreDiffPercentage float Default Null,
					PRIMARY KEY (RecordID)
				) ENGINE=InnoDB Charset=utf8";
		$SuccessArr['CreateTempTable'] = $this->db_db_query($sql);
		
		### Insert Temp Records
		if (count($InsertArr) > 0)
		{
			$sql = "Insert Into TMP_IPF_MGMT_ADVANCEMENT
						(StudentID, StudentName, ClassName, ClassNumber, FromScore, FromZScore, ToScore, ToZScore, ScoreDiff, ScoreDiffPercentage, ZScoreDiff, ZScoreDiffPercentage)
					Values
						".implode(',', (array)$InsertArr)."
					";
			$SuccessArr['InsertTempData'] = $this->db_db_query($sql);
		}
		
		$DBTable_SQL = "Select 
								StudentID,
								ClassName,
								ClassNumber,
								StudentName,
								FromScore,
								ToScore,
								If (ScoreDiff Is Null, '".$Lang['General']['EmptySymbol']."', ScoreDiff) as ScoreDiffDisplay,
								If (ScoreDiffPercentage Is Null, '".$Lang['General']['EmptySymbol']."', ScoreDiffPercentage) as ScoreDiffPercentageDisplay,
								If (FromZScore Is Null, '".$Lang['General']['EmptySymbol']."', FromZScore) as FromZScore,
								If (ToZScore Is Null, '".$Lang['General']['EmptySymbol']."', ToZScore) as ToZScore,
								If (ZScoreDiff Is Null, '".$Lang['General']['EmptySymbol']."', ZScoreDiff) as ZScoreDiffDisplay,
								If (ZScoreDiffPercentage Is Null, '".$Lang['General']['EmptySymbol']."', ZScoreDiffPercentage) as ZScoreDiffPercentageDisplay
						From 
								TMP_IPF_MGMT_ADVANCEMENT	
						ORDER BY 
								ClassName, ClassNumber
						";
// 		$rs = $this->returnResultSet($DBTable_SQL);
// 		debug_rt($rs);
		return $DBTable_SQL;
	}
	
	function Get_Student_Assessment_Result_Info($StudentIDArr, $SubjectIDArr, $AcademicYearIDArr, $YearTermID, $TermAssessment='', $SubjectComponentID='')
	{
		global $eclass_db;
		#J114859 
		$YearTerm_Temp = explode('_',$YearTermID);
		if(sizeof($YearTerm_Temp)>1){
			$TermAssessment = $YearTerm_Temp[1];
			$YearTermID = $YearTerm_Temp[0];
		}
		
		if ($StudentIDArr != '') {
			$conds_StudentID = " And UserID In (".implode(',', (array)$StudentIDArr).") ";
		}	
			
		if ($SubjectIDArr === 0) 
		{
			$ASSESSMENT_TABLE = 'ASSESSMENT_STUDENT_MAIN_RECORD';
			$SubjectIDField = '0 as SubjectID';
			$conds_SubjectID = '';
		}
		else 
		{
			$ASSESSMENT_TABLE = 'ASSESSMENT_STUDENT_SUBJECT_RECORD';
			$SubjectIDField = 'SubjectID';
			if ($SubjectIDArr !== '') {
				$conds_SubjectID = " And SubjectID In (".implode(',', (array)$SubjectIDArr).") ";
			}

			if($SubjectComponentID == ''){
				$conds_SubjectComponentID = " AND SubjectComponentID IS NULL ";
			}else{
				$conds_SubjectComponentID = " AND SubjectComponentID = '{$SubjectComponentID}' ";
			}
		}
		
		if ($AcademicYearIDArr != '') {
			$conds_AcademicYearID = " And AcademicYearID In (".implode(',', (array)$AcademicYearIDArr).") ";
		}
		
		if ($YearTermID == '') {
			$conds_YearTermID = " And IsAnnual = 1 ";
		}
		else {
			$conds_YearTermID = " And YearTermID = '".$YearTermID."' ";
		}
		
		if($TermAssessment == ''){
			$conds_TermAssessment = " AND TermAssessment IS NULL ";
		}else{
			$conds_TermAssessment = " AND TermAssessment = '{$TermAssessment}' ";
		}
		
		$sql = "Select
						UserID,
						$SubjectIDField,
						Score,
						Grade,
						YearClassID
				From
						{$eclass_db}.{$ASSESSMENT_TABLE}
				Where
						1
						$conds_StudentID
						$conds_SubjectID
						$conds_AcademicYearID
						$conds_YearTermID
						$conds_SubjectComponentID
						$conds_TermAssessment
				";
		return $this->returnArray($sql);
	}
	
	function Get_Student_Assessment_Display_Score($Mark, $Grade)
	{
		global $Lang;
		
		if ($Mark == '-1') {
			$Score = $Grade;
		}	
		else {
			$Score = $Mark;
		}
		$Score = ($Score=='')? $Lang['General']['EmptySymbol'] : $Score;
		
		return $Score;
	}
}

?>