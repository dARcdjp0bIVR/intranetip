<?php
# modify by : 

### Modification Log ###
#
#   Date    :   2019-12-02 (Cameron)
#               replace split with explode in getArrayFromURL() [case #W176396]
#
#	Date	:	2011-11-14 (Yuen)
#				use curl instead of fopen()
#
###### End of Log ######

class libslslib {

      var $api_path;
      function libslslib()
      {
               global $library_info_path;
               $this->api_path = $library_info_path;
      }

      function getArrayFromURL($target_path)
      {
      		/* disabled on 20111114
               $fp = @fopen($target_path,"r");
               if ($fp)
               {
                   while (!feof($fp))
                   {
                           $line = trim(fgets($fp,1024*2));
                           if ($line == "") continue;
                           $entry = split("\t",$line);
                           $content[] = $entry;
                   }
               } else
               
               {
                   $content = array();
               }
             */
               
               $curl_handle=curl_init();
                 curl_setopt($curl_handle, CURLOPT_URL,$target_path);
                 curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                 curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                 curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
                 $contents = curl_exec($curl_handle);
                 curl_close($curl_handle);
                 
                 $content_arr = explode("\n", $contents);
                 
                 for ($i=0; $i<sizeof($content_arr); $i++)
                 {
                 	$line = trim($content_arr[$i]);
                 	if ($line!="")
                 	{
                 		$entry = explode("\t",$line);
                 		if (sizeof($entry)>1)
                 			$content[] = $entry;
                 	}
                 }

               return $content;
      }

      function getNewAdditions()
      {
               $target_path = $this->api_path ."?function=GetNewAdditions";
               return $this->getArrayFromURL($target_path);
      }

      function getTopPatrons($start, $end, $num=10)
      {
               $start_sls = str_replace("-","",$start);
               $end_sls = str_replace("-","",$end);
               $target_path = $this->api_path."?function=GetPatronTopList&start=$start_sls&end=$end_sls&count=$num";
               return $this->getArrayFromURL($target_path);
      }

      function getTopItems($start, $end, $num=10)
      {
               $start_sls = str_replace("-","",$start);
               $end_sls = str_replace("-","",$end);
               $target_path = $this->api_path."?function=GetItemTopList&lang=&start=$start_sls&end=$end_sls&count=$num";
               return $this->getArrayFromURL($target_path);
      }

      function getCheckoutList ($UserLogin)
      {
               $target_path = $this->api_path."?function=GetPatronCheckOutList&oid=$UserLogin";
               return $this->getArrayFromURL($target_path);
      }
      function getHoldList ($UserLogin)
      {
               $target_path = $this->api_path."?function=GetPatronHoldList&oid=$UserLogin";
               return $this->getArrayFromURL($target_path);
      }
      function getOutstandingFine ($UserLogin)
      {
               $target_path = $this->api_path."?function=GetPatronOutstandingFine&oid=$UserLogin";
               
      		/* disabled on 20111114
               $fp = @fopen($target_path,"r");
               if ($fp)
               {
                   $line = trim(fgets($fp,1024));
               }
               $fine = $line + 0;
            */   
               
                $curl_handle=curl_init();
                 curl_setopt($curl_handle, CURLOPT_URL,$target_path);
                 curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                 curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                 curl_setopt($curl_handle, CURLOPT_USERAGENT, 'php');
                 $contents = curl_exec($curl_handle);
                 curl_close($curl_handle);
                 
                $fine = trim($contents) + 0;
                
               return $fine;
      }
}
?>