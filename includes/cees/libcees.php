<?php
// Using:
/*
 * 2020-07-06 (Bill): modified sendRequestToCentralServer(), to handle UTF-8 characters - (KIS) PHP 5.4     [2020-0702-1532-16073]
 * 2018-09-19 (Anna): added getAllSchoolInfoFromCentralServer()
 * 2017-11-27 (Anna): added getMonthlyReportStaffID(),getMonthlyReportReportID(),getMonthlyReportStaffInfo()
 * 2017-11-23 (Omas): added syncMonthlyReportToCentral(), syncStaffInfoToCentral()  
 * 2017-07-04 (Villa): Modified syncStudentInfo() - add sync log
 * 2017-07-03 (Villa): Added function syncStudentInfo()
 * 2017-04-18 (Omas): Added function getJUPASfromCentral()
 * 2017-03-01 (Omas): Add Log for group level error
 * 2016-04-11 (Carlos): Modified getScoreSubmissionPeriodFromCentralServer() filter submission period records with self school code. 
 */
include_once($intranet_root.'/includes/cees/ceesConfig.inc.php');

class libcees extends libdb {
	private $SettingsObj;
	
	public function __construct() {
		$this->libdb();
		$this->SettingsObj = null;
	}
	
	public function getSettingsObj() {
		global $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/cees/libcees_settings.php');
		if ($this->SettingsObj == null) {
			$this->SettingsObj = new libcees_settings();
			$this->SettingsObj->loadSettings();
		}
		
		return $this->SettingsObj;
	}
	
	public function getApiKey() {
		global $intranet_root;
		
		include_once($intranet_root.'/includes/libeclassapiauth.php');
		$lapiAuth = new libeclassapiauth();
		
		$basicApiKey = $lapiAuth->GetAPIKeyByProject('CEES');
		
		$curTimestamp = time();
		$curApiKey = md5($basicApiKey.'_'.date('YmdH', $curTimestamp));
		
		return $curApiKey;
	}
	
	private function getRequestId() {
		return 'req_'.date('Ymd').'_'.time();
	}
	
	private function getRsaKeyPair() {
		global $intranet_root;
		
		include_once($intranet_root.'/includes/libfilesystem.php');
		$lfs = new libfilesystem();
		
		$folderPath = $intranet_root.'/file/temp/cees';
		if (!is_dir($folderPath)) {
			$lfs->folder_new($folderPath);
		}
		
		$privateKeyPath = $folderPath.'/privateKey.pem';
		$publicKeyPath = $folderPath.'/publicKey.pem';

		if (file_exists($privateKeyPath)) {
			$lfs->file_remove($privateKeyPath);
		}
		if (file_exists($publicKeyPath)) {
			$lfs->file_remove($publicKeyPath);
		}
		
		$privateKeyPath = OsCommandSafe($privateKeyPath);
		$publicKeyPath = OsCommandSafe($publicKeyPath);
		exec('openssl genrsa -out '.$privateKeyPath.' 4096');
		exec('openssl rsa -in '.$privateKeyPath.' -pubout -out '.$publicKeyPath);
		
		$returnAry = array();
		$returnAry['privateKey'] = $lfs->file_read($privateKeyPath);
		$returnAry['publicKey'] = $lfs->file_read($publicKeyPath);
		
		if (file_exists($privateKeyPath)) {
			$lfs->file_remove($privateKeyPath);
		}
		if (file_exists($publicKeyPath)) {
			$lfs->file_remove($publicKeyPath);
		}

		return $returnAry;
	}
	
	public function getCentralAesKey() {
//		$funcParamAry = get_defined_vars();
//		$cacheResultAry = $this->getCacheResult('getCentralAesKey', $funcParamAry);
//    	if ($cacheResultAry !== null) {
//    		return $cacheResultAry;
//    	}
    	
		global $intranet_root;
		include_once($intranet_root.'/includes/cees/libcees_aes.php');
		$lceesAes = new libcees_aes();
		
		$settingsName = 'centralAesKey';
		$settingsObj = $this->getSettingsObj();
		$aesLastModified = $settingsObj->getSettingsLastModifiedDate($settingsName);
		
		$nowDate = date('Y-m-d');
		$licenseDate = date('Y-m-d', strtotime($aesLastModified));
		if ($aesLastModified=='' || $nowDate != $licenseDate) {
			$centralAesKey = $this->getAESKeyFromCentralServer();
			
			$tmpSettingsAssoAry = array();
			$tmpSettingsAssoAry[$settingsName] = $lceesAes->encrypt($centralAesKey);
			$settingsObj->saveSettings($tmpSettingsAssoAry);
			$settingsObj->loadSettings($forceReload=true);
		}
		
		$centralAesKey = $lceesAes->decrypt($settingsObj->getSettingsValue($settingsName));
		
//		$this->setCacheResult('getCentralAesKey', $funcParamAry, $centralAesKey);
		return $centralAesKey;
	}
	
	private function getAESKeyFromCentralServer() {
		// requires >= PHP5.2
//		$config = array(
//		    "digest_alg" => "sha512",
//		    "private_key_bits" => 4096,
//		    "private_key_type" => OPENSSL_KEYTYPE_RSA,
//		);
//		$res = openssl_pkey_new($config);
//		
//		// Extract the private key from $res to $privKey
//		openssl_pkey_export($res, $privateKey);
//		
//		// Extract the public key from $res to $pubKey
//		$publicKey = openssl_pkey_get_details($res);
//		$publicKey = $publicKey["key"];
		
		$rsaAry = $this->getRsaKeyPair();
		$privateKey = $rsaAry['privateKey'];
		$publicKey = $rsaAry['publicKey'];
		
		$apiAction = 'getKey';
		$dataAry = array();
		$dataAry['pk'] = base64_encode($publicKey);
		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry, false);
		
		$encryptedAesKey = $resultAry['rda']['ek'];
		$decryptedAesKey = '';
		openssl_private_decrypt ( base64_decode($encryptedAesKey) , $decryptedAesKey , $privateKey, OPENSSL_PKCS1_PADDING);
		
		return $decryptedAesKey;
	}
	
	private function sendRequestToCentralServer($parApiAction, $parDataAry, $parEncrypt=true, $parTimeout=60) {
		global $intranet_root, $ceesConfig, $config_school_code;
		global $plugin;
		
		include_once($intranet_root.'/includes/json.php');
		$jsonObj = new JSON_obj();
		$postParamAry = array();
		$postParamAry['ceesRequest']['rid'] = $this->getRequestId();	// requestId
		$postParamAry['ceesRequest']['ak'] = $this->getApiKey();		// apiKey
		$postParamAry['ceesRequest']['sc'] = $config_school_code;		// schoolCode
		$postParamAry['ceesRequest']['aa'] = $parApiAction;				// apiAction
		$postParamAry['ceesRequest']['da'] = $parDataAry;				// dataAry

        // [2020-0702-1532-16073]
        // (KIS) PHP 5.4 : Handle UTF-8 characters > not convert to "\uxxxx" after json_encode()
        // $escapeUnicode = true > json_encode($val, JSON_UNESCAPED_UNICODE)
        if ($plugin['SDAS_module']['KISMode'] && (intranet_phpversion_compare('5.4')=='SAME' || intranet_phpversion_compare('5.4')=='LATER')) {
            $postJsonString = $jsonObj->encode($postParamAry, true);
        } else {
		    $postJsonString = $jsonObj->encode($postParamAry);
        }

		if ($parEncrypt)
		{
			include_once($intranet_root.'/includes/cees/libcees_aes.php');
			$lceesAes = new libcees_aes();

			unset($postParamAry);
			$postParamAry['ceesRequest_e'] = $lceesAes->encrypt($postJsonString, $this->getCentralAesKey());
			$postJsonString = $jsonObj->encode($postParamAry);
		}

		$headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
		$apiPath = $ceesConfig['apiPath'];

		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL, $apiPath);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$responseJson = curl_exec($ch);
		curl_close($ch);

 		//debug_pr(standardizeFormPostValue($responseJson));
		$responseJsonAry = $jsonObj->decode(standardizeFormPostValue($responseJson));
		if ($parEncrypt) {
		    $responseJsonAry = $jsonObj->decode($this->ceesUni2Html($lceesAes->decrypt($responseJsonAry['ceesRequest_e'], $this->getCentralAesKey())));
		}

		// 2017 03 01 Add Log for group level error
		if(empty($responseJsonAry)){
			$logDetails = 'JSON response : '.$responseJson.' ';
			$this->takeCeesSyncLog('0','0','-1', $logDetails, $parApiAction);
		}

		return $responseJsonAry;
	}
	
	public function getMonthlyReportReportID($reportYearMonth){
	
		$year = substr($reportYearMonth, 0, 4);
		$month = substr($reportYearMonth, 4);
		$sql = "SELECT ReportID FROM CEES_SCHOOL_MONTHLY_REPORT
				WHERE Year = '$year' and Month = '$month'";
		$ReportIDAry = $this->returnVector($sql);
		
		return $ReportIDAry[0];
	}
	public function getMonthlyReportStaffID($ReportID){
	
		$sql = "SELECT StaffID FROM CEES_SCHOOL_MONTHLY_REPORT_APPOINTMENT_INFO WHERE ReportID = '$ReportID'";
		$AppointmentStaffIDAry = $this->returnVector($sql);
		
		$sql = "SELECT SubstitutedStaffID FROM CEES_SCHOOL_MONTHLY_REPORT_SUBSTITUTE_INFO WHERE ReportID = '$ReportID'";
		$SubstituteStaffIDAry = $this->returnVector($sql);
		
		$sql = "SELECT StaffID FROM CEES_SCHOOL_MONTHLY_REPORT_STAFF_SICK_LEAVE WHERE ReportID = '$ReportID'";
		$SickLeaveStaffIDAr = $this->returnVector($sql);
		
		$sql = "SELECT StaffIDs FROM CEES_SCHOOL_MONTHLY_REPORT_STAFF_COURSE_ATTEND WHERE ReportID = '$ReportID'";
		$StaffCourseStaffIDAry = $this->returnVector($sql);
		$StaffIDAry = array_unique(array_merge($AppointmentStaffIDAry,$SubstituteStaffIDAry,$SickLeaveStaffIDAr,$StaffCourseStaffIDAry));
		$StaffList = implode(",",$StaffIDAry);
		
		return $StaffList;
	}
	function getMonthlyReportStaffInfo($StaffList){
		
		$sql = "SELECT UserID,EnglishName,ChineseName,Teaching
				FROM INTRANET_USER
				WHERE UserID IN ($StaffList)";
		$StaffInfo = $this->returnResultSet($sql);
		
		return $StaffInfo;
	}
	
	public function syncStaffInfoToCentral($reportYearMonth){
		
		global $config_school_code, $intranet_root;
		$apiAction = 'syncStaffInfo';
		$dataAry = array();
		$dataAry['School'] = $config_school_code;
		
		$ReportID= $this->getMonthlyReportReportID($reportYearMonth);
		
		$StaffList = $this->getMonthlyReportStaffID($ReportID);
		
		$StaffInfo = $this->getMonthlyReportStaffInfo($StaffList);
		$dataAry['Staffs'] = $StaffInfo;

		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
		include_once($intranet_root.'/includes/json.php');
		$json = new JSON_obj();
		$resultDetails = $json->encode($resultAry['rda']['result']);
		$this->takeCeesSyncLog(0,0,$resultAry['rda']['success'],$resultDetails,$recordType='STAFFINFO');
	}
	
	public function syncMonthlyReportToCentral($reportYearMonth, $reportDataArr){
		
		global $config_school_code;
		$apiAction = 'syncMonthlyReport';
		$dataAry = array();
		$dataAry['School'] = $config_school_code;
		$dataAry['SchType'] = 'IP';
		$dataAry['YearMonth'] = $reportYearMonth;
		$dataAry['ReportData'] = $reportDataArr;

		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
		$this->takeCeesSyncLog($reportYearMonth,0,$resultAry['rda']['success'],$resultAry['rda']['refId'],$recordType='MONTHLYREPORT');
		$this->syncStaffInfoToCentral($reportYearMonth);
		return $resultAry; 
	}
	
	public function getJUPASfromCentral($fromAcademicYearId, $toAcademicYearId){
		global $intranet_root;
		include_once($intranet_root.'/includes/json.php');
		$json = new JSON_obj();
		
		$apiAction = 'getJupusData';
		$dataAry = array();
		$dataAry['fromAcademicYear'] = $this->getAcademicYearString($fromAcademicYearId);
		$dataAry['toAcademicYear'] = $this->getAcademicYearString($toAcademicYearId);
		$dataAry['school'] = $config_school_code;
		
// 		$settingsName = 'centralJupas';
// 		$settingsObj = $this->getSettingsObj();
// 		$lastModified = $settingsObj->getSettingsLastModifiedDate($settingsName);
// 		$nowDate = date('Y-m-d');
// 		$licenseDate = date('Y-m-d', strtotime($lastModified));
		
		//if ($lastModified=='' || $nowDate != $licenseDate) {
		$tmpSettingsAssoAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
// 			debug_pr($tmpSettingsAssoAry);
			// save report data
// 			$settingsObj->saveSettings(array( $settingsName => $settingsObj->Get_Safe_Sql_Query($json->encode($tmpSettingsAssoAry['rda']['data']))));
// 			$settingsObj->loadSettings($forceReload=true);
			
		// save program data
		include_once($intranet_root.'/includes/libpf-exam.php');
		global $JUPAS_CONFIG;
		$libpf_exam = new libpf_exam();			
		$existJupasCodeArr = $libpf_exam->getAllJupasProgramme();
		foreach((array)$tmpSettingsAssoAry['rda']['program'] as $_jupasCode => $_infoArr){
			if(!in_array($_jupasCode, (array)$existJupasCodeArr)){
				$_institution_db =  $_infoArr[0];
				$_programmeName = $_infoArr[1];
				$_programmeType = $libpf_exam->getJupasProgTypeByName($_programmeName);
				
				$insertProgramArr[] = " ( '$_jupasCode', '".$libpf_exam->Get_Safe_Sql_Query($_programmeName)."', '$_institution_db', '$_programmeType' ) ";
			}
		}
		if( count($insertProgramArr) > 0 ){
			$insert_prog = $libpf_exam->insertJupasProgrammeData($insertProgramArr);
		}
		
		$jupasResultArr = $tmpSettingsAssoAry['rda']['data'];
		//$jupasResultArr = $json->decode($settingsObj->getSettingsValue($settingsName));
		return $jupasResultArr;
	}
	
	public function getVerifyStatusFromCentralServer($parAcademicYearId, $parType){
	
		global $config_school_code;
	
		$apiAction = 'getVerifyStatus';
		$dataAry = array();
		$dataAry['academicYear'] = $this->getAcademicYearString($parAcademicYearId);
		$dataAry['type'] = $parType;
		$dataAry['school'] = $config_school_code;
		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
	
		return $resultAry['rda'];
	}

	public function sendDseEndorsementToCentral($parAcademicYearID, $parType){
		global $config_school_code;
	
		$apiAction = 'examDataEndorsement';
		$dataAry = array();
		$dataAry['academicYear'] = $this->getAcademicYearString($parAcademicYearID);
		$dataAry['type'] = $parType;
		$dataAry['school'] = $config_school_code;
		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
		if($resultAry['rda']['success'] == 2){
			$logDetails = 'confirmed before';
		}
		$this->takeCeesSyncLog($parType,$parAcademicYearID,$resultAry['rda']['success'],$logDetails,$recordType='CONFIRM');

		return $resultAry;
	}
	
	public function cancelDseEndorsementOrSyncedLog($parAcademicYearID, $parExamType, $parType){
		$sql = "UPDATE CEES_SYNC_LOG SET RecordStatus = -1, Details = Concat(Details,'; reset ',now()) where RecordStatus > 0 AND AcademicYearID = '$parAcademicYearID' and ExamType = '$parExamType' and RecordType = '$parType'";
		$this->db_db_query($sql);
	}
	
	public function syncScoreToCentral($parAcademicYearId, $parType,$scoreAry ) {
		global $config_school_code;
		
		$academicYear = $this->getAcademicYearString($parAcademicYearId);
		$apiAction = 'syncStudentScore';
		$dataAry = array();
		$dataAry['academicYear'] = $academicYear;
		$dataAry['type'] = $parType;
		$dataAry['school'] = $config_school_code;
		$dataAry['scoreAry'] = $scoreAry;
		$dataAry['schoolType'] = 'S';
		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
 
		if($resultAry['rda']['success'] == 1){
			$status = 1; // success
		}else{
			$status = 0;// failed
		}
		
		$additionLogContent = '';
		if(!empty($resultAry['rda']['error'])){
			$additionLogContent .= ', error:'.$resultAry['rda']['error'];
		}
		if($resultAry['rda']['SNF'] != ''){
			$additionLogContent .= ', SNF: '.$resultAry['rda']['SNF'];
		}
		$log = 'success:'.$resultAry['rda']['success'].$additionLogContent;
		
		$this->takeCeesSyncLog($parType,$parAcademicYearId,$status,$log);
		return $resultAry['rda']['success'];
	}
	
	public function syncReportToCentral($parAcademicYearId, $parType, $reportInfo, $recordAry ) {
		global $config_school_code;
		
		$academicYear = $this->getAcademicYearString($parAcademicYearId);
		$apiAction = 'syncReport';
		$dataAry = array();
		$dataAry['academicYear'] = $academicYear;
		$dataAry['type'] = $parType;
		$dataAry['school'] = $config_school_code;
		$dataAry['recordAry'] = array('reportInfo'=> $reportInfo, 'recordAry' => $recordAry);
		$dataAry['schoolType'] = 'K';
		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
		
		if($resultAry['rda']['success'] == 1){
			$status = 1; // success
		}else{
			$status = 0;// failed
		}
		
		$additionLogContent = '';
		if(!empty($resultAry['rda']['error'])){
			$additionLogContent .= ', error:'.$resultAry['rda']['error'];
		}
		if($resultAry['rda']['SNF'] != ''){
			$additionLogContent .= ', SNF: '.$resultAry['rda']['SNF'];
		}
		$log = 'success:'.$resultAry['rda']['success'].$additionLogContent;
		
		$this->takeCeesSyncLog('10'.$parType,$parAcademicYearId,$status,$log, $recordType='syncReport');
		return $resultAry['rda']['success'];
	}
	
	public function syncStudentInfo($parAcademicYearID, $parType,$StudentInfoArr) {
		global $config_school_code;
		
		##Data Getting should be copied to the script
		
// 		$StudentInfoArr
		$parAcademicYear = $this->getAcademicYearString($parAcademicYearID);
		$apiAction = 'syncStudentInfo';
		$dataAry = array();
		$dataAry['academicYear'] = $parAcademicYear;
		$dataAry['type'] = $parType;
		$dataAry['school'] = $config_school_code;
		$dataAry['StudentInfoArr'] = $StudentInfoArr;
		$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
		
		$log = 'success:'.$resultAry['rda']['success'].', error:'.$resultAry['rda']['error'].', '.$additionLogContent;
		if($dataHash == '' || $resultAry['rda']['checksum'] == ''){
			$status = failed;
		}
		//mark the log
		$this->takeCeesSyncLog('0',$parAcademicYearID,$status,$log,'syncStudentInfo');
		
		// 		return $resultAry['rda']['arrayHash'];
		return $resultAry;
	}
	
	public function getAcademicYearString($parAcademicYearID){
		$aYearInfoArr = getPeriodOfAcademicYear($parAcademicYearID);
		$startYear = substr($aYearInfoArr['StartDate'],0,4);
		$endYear = $startYear+1;
		return $startYear.'-'.$endYear;
	}
	
	public function getScoreSubmissionPeriodFromCentralServer($parAcademicYearId) {
		global $config_school_code;
		
		function filter_callback($v){
			global $config_school_code;
			return $v['SchoolCode']==$config_school_code;
		}
		
		if(is_array($parAcademicYearId)){
			$result = array();
			foreach($parAcademicYearId as $_Id){
				$academicYearString = $this->getAcademicYearString($_Id);
					
				$apiAction = 'getScoreSubmissionPeriod';
				$dataAry = array();
				$dataAry['academicYear'] = $academicYearString;
				$dataAry['schoolType'] = 'S';
				$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);

                $resultAry['rda']['settingsAry'] = (array)$resultAry['rda']['settingsAry'];
				$records = array_filter($resultAry['rda']['settingsAry'], filter_callback);
				if(!empty($records)){
					$result[$_Id] = BuildMultiKeyAssoc($records, 'ExamTypeCode', $IncludedDBField=array('SubmitStartDate', 'SubmitEndDate'));
				}
			}
			
		}
		else{
			$academicYearString = $this->getAcademicYearString($parAcademicYearId);
			
			$apiAction = 'getScoreSubmissionPeriod';
			$dataAry = array();
			$dataAry['academicYear'] = $academicYearString;
			$dataAry['schoolType'] = 'S';
			$resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);

// 			function filter_callback($v){
// 				global $config_school_code;
// 				return $v['SchoolCode']==$config_school_code;
// 			}

            $resultAry['rda']['settingsAry'] = (array)$resultAry['rda']['settingsAry'];
			$records = array_filter($resultAry['rda']['settingsAry'], filter_callback);
			//return BuildMultiKeyAssoc($resultAry['rda']['settingsAry'], 'ExamTypeCode', $IncludedDBField=array('SubmitStartDate', 'SubmitEndDate'));
// 			return BuildMultiKeyAssoc($records, 'ExamTypeCode', $IncludedDBField=array('SubmitStartDate', 'SubmitEndDate'));
			$result = BuildMultiKeyAssoc($records, 'ExamTypeCode', $IncludedDBField=array('SubmitStartDate', 'SubmitEndDate'));
		}
		return $result;
	}
	public function takeCeesSyncLog($exam,$academicYearID,$status,$logDetails='',$recordType='1'){
		global $UserID;
		$sql="INSERT INTO CEES_SYNC_LOG (UserID,ExamType,AcademicYearID,RecordType,RecordStatus,Details,DateInput) 
				Values ('$UserID','$exam','$academicYearID', '$recordType','$status','$logDetails',now())";
		$this->db_db_query($sql);
	}
	public function getCeesSyncLog($Cond='',$OrderBy='',$LogID='',$ExamType = '',$UserID='',$ayID='',$RecordStatus='', $RecordType='1'){
		if($LogID!=''){
			$Cond.= " AND LogID = '".$LogID."' ";
		}
		if($UserID!=''){
			$Cond.= " AND UserID = '".$UserID."' ";
		}
		if($ayID!=''){
			$Cond.= " AND AcademicYearID = '".$ayID."' ";
		}
		if($RecordStatus!=''){
			$Cond.= " AND RecordStatus = '".$RecordStatus."' ";
		}
		if($ExamType!=''){
			$Cond.= " AND ExamType = '".$ExamType."' ";
		}
		if($RecordType!=''){
			$Cond .= " AND RecordType = '".$RecordType."' " ;
		}
		if($OrderBy!=''){
			$OrderBy = " Order By ". $OrderBy." ";
		}
		
		$sql = "SELECT LogID, UserID, ExamType, AcademicYearID, RecordStatus, DateInput FROM CEES_SYNC_LOG WHERE 1 ".$Cond .' '. $OrderBy;
		return $this->returnArray($sql);
	}
	
	public function checkIfEndorsed($parAcademicYearID, $parExamId){
		
		// find local log for endorse
		$sql = "SELECT RecordStatus, count(*) as Count FROM CEES_SYNC_LOG WHERE AcademicYearID = '{$parAcademicYearID}' AND ExamType = '{$parExamId}' AND RecordType = 'CONFIRM' group by RecordStatus";
		$result = BuildMultiKeyAssoc( $this->returnResultSet($sql), 'RecordStatus', array('Count'),1 );
		if(count($result) > 0){
			$verifyAry = $this->getVerifyStatusFromCentralServer($parAcademicYearID, $parExamId);
			$endorseAlready = $verifyAry['endorse'] == 1 && $verifyAry['resubmit'] == 0;
		}
		return $endorseAlready;
	}
	
	public function getAllSchoolInfoFromCentralServer($parType){
	    
	    global $config_school_code,$intranet_root;
	    
	    $apiAction = 'getAllSchoolInfo';
	    
	    $dataAry = array();
// 	    $dataAry['academicYear'] = $this->getAcademicYearString($parAcademicYearId);
	    $dataAry['type'] = $parType;
// 	    $dataAry['school'] = $config_school_code;
	    $resultAry = $this->sendRequestToCentralServer($apiAction, $dataAry);
	    
	    return $resultAry['rda'];
	}
	
	private function ceesUni2Html($string){
	    if (intranet_phpversion_compare('5.4')=='SAME' || intranet_phpversion_compare('5.4')=='LATER') {
// 	        debug_pr('171');
	        return $string;
	    } else {
// 	        debug_pr('146');
    	    $string = explode('\\', $string);
    	    $string = implode('%', $string);
    	    $string = preg_replace('/%u([0-9A-Za-z]+)/', '&#x$1;', $string);
    	    return html_entity_decode($string, ENT_COMPAT, 'UTF-8');
	    }
	}
}
?>