<?php
// editing by 

class libcees_settings extends libdb {
	var $settingsAssoAry;
	
	function libcees_settings() {
		$this->libdb();
	}
	
	function saveSettings($parSettingsAssoAry) {
		if (count($parSettingsAssoAry)==0 || !is_array($parSettingsAssoAry)) return false;
		
		$this->Start_Trans();
		$successAry = array();
		
		$sql = "Select SettingsID, SettingName, SettingValue From CEES_SETTINGS";
		$curSettingsAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'SettingName');
		
		$insertAry = array();
		foreach ((array)$parSettingsAssoAry as $_settingsName => $_settingsValue) {
			if (isset($curSettingsAssoAry[$_settingsName])) {
				// update
				$sql = "Update CEES_SETTINGS 
						Set SettingValue = '".$this->Get_Safe_Sql_Query($_settingsValue)."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' 
						Where SettingsID = '".$curSettingsAssoAry[$_settingsName]['SettingsID']."'";
				$successAry[$_settingsName] = $this->db_db_query($sql);
			}
			else {
				// insert
				$insertAry[] = " ('".$this->Get_Safe_Sql_Query($_settingsName)."', '".$this->Get_Safe_Sql_Query($_settingsValue)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
			}
		}
		
		if (count($insertAry) > 0) {
			$sql = "INSERT INTO CEES_SETTINGS (SettingName, SettingValue, DateInput, InputBy, DateModified, ModifiedBy)
					Values ".implode(', ', (array)$insertAry);
			$successAry['insert'] = $this->db_db_query($sql);
		}
		
		if(in_array(false, $successAry)) {
			$this->RollBack_Trans();
			return false;
		}
		else {
			$this->Commit_Trans();
			return true;
		}
	}
 
	function loadSettings($forceReload=false)
	{
		if ($forceReload || $this->settingsAssoAry == null) {
			$sql = "SELECT SettingName, SettingValue, DateModified FROM CEES_SETTINGS";
			$settingsAry = $this->returnResultSet($sql);
			$this->settingsAssoAry = BuildMultiKeyAssoc($settingsAry, "SettingName");
			$this->setDefaultSettings();
		}
	}
	
	function setDefaultSettings(){
		
	}
	
	function getSettingsValue($settingsName) {
		$this->loadSettings();
		return $this->settingsAssoAry[$settingsName]['SettingValue'];
	}
	
	function getSettingsLastModifiedDate($settingsName) {
		$this->loadSettings();
		return $this->settingsAssoAry[$settingsName]['DateModified'];
	}
}
?>