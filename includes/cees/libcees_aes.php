<?php
include_once($intranet_root.'/includes/cees/ceesConfig.inc.php');
class libcees_aes {
	
	public function __construct() {
		
	}
	
	public function encrypt($parPlainText, $parKey='') {
		global $ceesConfig, $intranet_root;
		
		if ($parKey == '') {
			$parKey = $ceesConfig['aesLocalKey'];
		}
		
		$length = strlen($parPlainText);
		if ($length > 1024) {
			include_once($intranet_root.'/includes/libfilesystem.php');
			$lfs = new libfilesystem();
			
			$tmpFilePath = $ceesConfig['filePath'].'tmp/en/';
			if (!file_exists($tmpFilePath)) {
				$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
			}
			
			$tmpFilePath .= 'd_'.date('Ymd_His').'_'.$_SESSION['UserID'].'_'.parseFileName(substr($parPlainText, 0, 80)).'.tmp';
			$lfs->file_write($parPlainText, $tmpFilePath);
			
			$command = "echo ".'$'."(< ".$tmpFilePath.") | openssl enc -aes-256-cbc -salt -a -A -k " . bin2hex ($parKey);
			$result_e = trim(shell_exec($command));
			
			$lfs->file_remove($tmpFilePath);
		}
		else {
			$command = "echo ".'$'."'".str_replace("'",'\\\'',$parPlainText)."' | openssl enc -aes-256-cbc -salt -a -A -k " . bin2hex ($parKey);
			$result_e = trim(shell_exec($command));
		}
		
		//debug_pr($command);
		return $result_e;
	}
	
	public function decrypt($parEncryptedText, $parKey='') {
		global $ceesConfig, $intranet_root;
		
		if ($parKey == '') {
			$parKey = $ceesConfig['aesLocalKey'];
		}
		
		$length = strlen($parEncryptedText);
		if ($length > 1024) {
			include_once($intranet_root.'/includes/libfilesystem.php');
			$lfs = new libfilesystem();
			
			$tmpFilePath = $ceesConfig['filePath'].'tmp/de/';
			if (!file_exists($tmpFilePath)) {
				$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
			}
			
			$tmpFilePath .= 'd_'.date('Ymd_His').'_'.$_SESSION['UserID'].'_'.parseFileName(substr($parEncryptedText, 0, 80)).'.tmp';
			$lfs->file_write($parEncryptedText, $tmpFilePath);
			
			$command = "echo ".'$'."(< ".$tmpFilePath.") | openssl enc -d -aes-256-cbc -salt -a -A -k " . bin2hex ($parKey);
			$result_d = trim(shell_exec($command));
			
			$lfs->file_remove($tmpFilePath);
		}
		else {
			$command = "echo ".'$'."'".str_replace("'",'\\\'',$parEncryptedText)."' | openssl enc -d -aes-256-cbc -salt -a -A -k " . bin2hex ($parKey);
			$result_d = trim(shell_exec($command));
		}
		
		
		//debug_pr($command);
		return $result_d;
	}
}
?>