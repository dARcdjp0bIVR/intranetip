<?php
// Editing by 
/* 
 * 2020-09-16 (Philips) - modified GetStaffSickLeaveMonthlyData(), fix query join condition with attendance profile
 * 						- fix to 1 dp of DutyCount
 * 2020-07-24 (Philips) - modified GetStudentAttendanceMonthlyData(), fix attendanceRate formula
 * 2020-05-27 (Philips): modified GetStaffSickLeaveMonthlyData(), getStaffOtherLeaveMonthlyData(), use SUM(d.DutyCount) replace COUNT(DISTINCT d.DayNumber)
 * 2020-05-19 (Bill): modified GetStaffSickLeaveMonthlyData(), GetStaffOtherLeaveMonthlyData(), to get data from attendance reason - 'Absent' and 'On Leave'  [2020-0518-1447-06073]
 */
//include_once($intranet_root.'/includes/cees/ceesConfig.inc.php');

/*
 * public function is callable api methods
 */
class libcees_api
{
	private $libdb = null;
	private $is_ej = false;
	
	public function __construct() {
		global $intranet_version, $eclass_version;
		$this->libdb = new libdb();
		$this->is_ej = $intranet_version == "2.0" || $eclass_version==3.0; // assume EJ would not change version number in the near future...
	}
	
	/*
	 * $params['TargetDate'] 
	 * 
	 */
	public function GetStudentAdmissionWithdrawalMonthlyData($params){
		
		if(!isset($params['Year']) || !isset($params['Month'])){
			return array('Error'=>'Invalid parameters.');
		}
		
		$this_month_ts = strtotime($params['Year'].'-'.$params['Month'].'-01');
		$start_ts = $this_month_ts;
		$start_month = date("Y-m-01",$this_month_ts); // start of target Month
		$end_month = date("Y-m-t", $this_month_ts); // end of target Month
		$thisLang = $params['DisplayLang'];
		if($thisLang=='en'){
			$nameFieldClause = "IF(u.EnglishName <> '', u.EnglishName, u.ChineseName)";
		} else {
			$nameFieldClause = "IF(u.ChineseName <> '', u.ChineseName, u.EnglishName)";
		}
		
// 		$modifyCond = " AND p.DateModify >= '$start_month' AND p.DateModify <= '$end_month' ";
		$sql = "SELECT
					u.UserLogin,
					$nameFieldClause AS StudentName,
					u.ClassName,
					IF(p.PeriodStart='1970-01-01','',p.PeriodStart) as PeriodStart,
					IF( p.PeriodStart >= '$start_month' AND p.PeriodStart <= '$end_month', '1', '0' ) as isPeriodStart, 
					IF(p.PeriodEnd='2099-12-31' OR p.PeriodEnd='2037-12-31','',p.PeriodEnd) as PeriodEnd,
					IF( p.PeriodEnd >= '$start_month' AND p.PeriodEnd <= '$end_month', '1', '0' ) as isPeriodEnd
				FROM INTRANET_USER as u
				INNER JOIN CARD_STUDENT_ENTRY_LEAVE_PERIOD as p ON p.UserID=u.UserID
				WHERE u.RecordStatus=1 AND u.RecordType=2 $modifyCond
				ORDER BY u.UserLogin, p.PeriodStart";
		$result = $this->libdb->returnArray($sql);
// 		return $sql;
		$recordAry = array();
		foreach($result as $row){
			if($row['PeriodStart']!='1970-01-01'&&$row['PeriodStart']!=''&&$row['isPeriodStart']=='1'){
				$tempAry = array();
				$tempAry['Name'] = $row['StudentName'];
				$tempAry['Date'] = $row['PeriodStart'];
				$tempAry['Type'] = 'Admission';
				$tempAry['ClassName'] = $row['ClassName'];
				$recordAry[] = $tempAry;
				unset($tempAry);
			}
			if($row['PeriodEnd']!='2099-12-31'&&$row['PeriodEnd']!=''&&$row['isPeriodEnd']=='1'){
				$tempAry = array();
				$tempAry['Name'] = $row['StudentName'];
				$tempAry['Date'] = $row['PeriodEnd'];
				$tempAry['Type'] = 'Withdrawal';
				$tempAry['ClassName'] = $row['ClassName'];
				$recordAry[] = $tempAry;
				unset($tempAry);
			}
		}
		return $recordAry;
	}
	
	public function GetStudentAttendanceMonthlyData($params)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $Lang, $intranet_session_language;
		
		if(!$plugin['attendancestudent']){
			return array('Error'=>'No Student Attendance module.');
		}
		
		if(!isset($params['TargetDate'])){ // need to count attendance rate til the target date
			return array('Error'=>'Invalid parameters.');
		}
		
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		
		$lc = new libcardstudentattend2();
		$attendance_mode = $lc->attendance_mode;
		$is_half_day = in_array($attendance_mode,array('0','1'));
		
		$academic_info = getAcademicYearAndYearTermByDate($params['TargetDate']);
		$academic_year_id = $academic_info['AcademicYearID'];
		
		$this_month_ts = strtotime($params['TargetDate']);
		// find the nearest previous september
		$start_ts = $this_month_ts;
		$start_month = date("m",$this_month_ts);
		while($start_month != '09'){
			$start_ts = strtotime("-1 month",$start_ts);
			$start_month = date("m",$start_ts);
		}
		
		// get the form levels
		if($this->is_ej){
			$sql = "SELECT 
						y.ClassLevelID as ClassLevelID,
						y.LevelName as ClassLevelName,
			 			y.WebSAMSLevel as Level,
						COUNT(DISTINCT yc.ClassID) as ClassesInOperation,
						0 as EnrolledInSeptember,
						0 as EnrolledThisMonth,
						0 as AttendanceRate   
			 		FROM INTRANET_CLASSLEVEL as y 
			 		INNER JOIN INTRANET_CLASS as yc ON y.ClassLevelID = yc.ClassLevelID  
					GROUP BY y.ClassLevelID 
					ORDER BY y.LevelName ";
		}else{
			$sql = "SELECT 
						y.YearID as ClassLevelID,
						y.YearName as ClassLevelName,
			 			y.WEBSAMSCode as Level,
						COUNT(DISTINCT yc.YearClassID) as ClassesInOperation,
						0 as EnrolledInSeptember,
						0 as EnrolledThisMonth,
						0 as AttendanceRate   
			 		FROM YEAR as y 
			 		INNER JOIN YEAR_CLASS as yc ON y.YearID = yc.YearID AND yc.AcademicYearID = '".$academic_year_id."' 
					GROUP BY y.YearID 
					ORDER BY y.YearName ";
		}
		$class_level_records = $this->libdb->returnResultSet($sql);
		//debug_pr($sql);
		
		// count number of students for each class 
		if($this->is_ej){
			$sql = "SELECT 
						yc.ClassID as ClassID,
						yc.ClassName,
						yc.ClassLevelID as ClassLevelID,
						COUNT(DISTINCT su.UserID) as StudentCount 
					FROM INTRANET_CLASS as yc  
					LEFT JOIN INTRANET_USER as su ON su.ClassName=yc.ClassName AND su.RecordStatus=1 AND su.RecordType=2
					WHERE 1 
					GROUP BY yc.ClassID ";
		}else{
			$sql = "SELECT 
						yc.YearClassID as ClassID,
						yc.ClassTitleEN as ClassName,
						yc.YearID as ClassLevelID,
						COUNT(DISTINCT ycu.UserID) as StudentCount 
					FROM YEAR_CLASS as yc 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.YearClassID=yc.YearClassID 
					LEFT JOIN INTRANET_USER as su ON su.UserID=ycu.UserID AND su.RecordStatus=1 AND su.RecordType=2
					WHERE yc.AcademicYearID='".$academic_year_id."' 
					GROUP BY yc.YearClassID ";
		}
		$class_student_records = $this->libdb->returnResultSet($sql);
		
		//debug_pr($class_student_records);
		
		//$yearIdToClass = array();
		$yearIdToStudentCount = array();
		for($i=0;$i<count($class_student_records);$i++){
			$year_id = $class_student_records[$i]['ClassLevelID'];
			$year_class_id = $class_student_records[$i]['ClassID'];
			
			//if(!isset($yearIdToClass[$year_id])){
			//	$yearIdToClass[$year_id] = array();
			//}
			//$yearIdToClass[$year_id][$year_class_id] = $class_student_records[$i];
			if(!isset($yearIdToStudentCount)){
				$yearIdToStudentCount[$year_id] = 0;
			}
			$yearIdToStudentCount[$year_id] += $class_student_records[$i]['StudentCount'];
		}
		
		// count attendance rate for the month
		$this_year = Date("Y",$this_month_ts);
		$this_month = Date("m",$this_month_ts);
		
		$daily_log_table = "CARD_STUDENT_DAILY_LOG_".$this_year."_".$this_month;
		if($this->is_ej){
			$sql = "SELECT 
						d.RecordID,
						d.UserID,
						DATE_FORMAT(CONCAT('$this_year-$this_month-',d.DayNumber),'%Y-%m-%d') as RecordDate,
						d.AMStatus,
						d.PMStatus,
						yc.ClassLevelID as ClassLevelID,
						yc.ClassID as ClassID 
					FROM $daily_log_table as d 
					INNER JOIN INTRANET_USER as ycu ON ycu.UserID=d.UserID 
					INNER JOIN INTRANET_CLASS as yc ON yc.ClassName=ycu.ClassName  
					WHERE 1 ";
		}else{
			$sql = "SELECT 
						d.RecordID,
						d.UserID,
						DATE_FORMAT(CONCAT('$this_year-$this_month-',d.DayNumber),'%Y-%m-%d') as RecordDate,
						d.AMStatus,
						d.PMStatus,
						yc.YearID as ClassLevelID,
						yc.YearClassID as ClassID 
					FROM $daily_log_table as d 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=d.UserID 
					INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID 
					WHERE yc.AcademicYearID='$academic_year_id' ";
		}
		$this_month_attendance = $this->libdb->returnResultSet($sql);
		$this_month_attendance_size = count($this_month_attendance);
		$yearIdToAttendance = array();
		for($i=0;$i<$this_month_attendance_size;$i++){
			$year_id = $this_month_attendance[$i]['ClassLevelID'];
			if(!isset($yearIdToAttendance[$year_id])){
				$yearIdToAttendance[$year_id] = array('AttendCount'=>0,'AbsentCount'=>0);
			}
			$am_status = $this_month_attendance[$i]['AMStatus'];
			$pm_status = $this_month_attendance[$i]['PMStatus'];
			
			if($is_half_day){
				if($am_status == CARD_STATUS_ABSENT || $pm_status == CARD_STATUS_ABSENT){
					$yearIdToAttendance[$year_id]['AbsentCount'] += 1;
				}else{
					$yearIdToAttendance[$year_id]['AttendCount'] += 1;
				}
			}else{
				if($am_status == CARD_STATUS_ABSENT){
					$yearIdToAttendance[$year_id]['AbsentCount'] += 0.5;
				}else{
					$yearIdToAttendance[$year_id]['AttendCount'] += 0.5;
				}
				if($pm_status == CARD_STATUS_ABSENT){
					$yearIdToAttendance[$year_id]['AbsentCount'] += 0.5;
				}else{
					$yearIdToAttendance[$year_id]['AttendCount'] += 0.5;
				}
			}
		}
		
		//debug_pr($yearIdToAttendance);
		
		// count number of students in September by couting how many distinct students in attendance records of September
		$september_year = date("Y", $start_ts);
		$september_month = date("m", $start_ts);
		$daily_log_table = "CARD_STUDENT_DAILY_LOG_".$september_year."_".$september_month;
		if($this->is_ej){
			$sql = "SELECT 
						yc.ClassLevelID as ClassLevelID,
						COUNT(DISTINCT d.UserID) as StudentCount  
					FROM $daily_log_table as d 
					LEFT JOIN INTRANET_USER as ycu ON ycu.UserID=d.UserID 
					LEFT JOIN INTRANET_CLASS as yc ON yc.ClassName=ycu.ClassName  
					WHERE 1 GROUP BY yc.ClassLevelID ";
		}else{
			$sql = "SELECT 
						yc.YearID as ClassLevelID,
						COUNT(DISTINCT d.UserID) as StudentCount  
					FROM $daily_log_table as d 
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=d.UserID 
					LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID 
					WHERE 1 GROUP BY yc.YearID ";
		}
		$september_student_summary = $this->libdb->returnResultSet($sql);
		//debug_pr($september_student_summary);
		$yearIdToSeptemberStudentCount = array();
		for($i=0;$i<count($september_student_summary);$i++){
			$year_id = $september_student_summary[$i]['ClassLevelID'];
			$yearIdToSeptemberStudentCount[$year_id] = $september_student_summary[$i]['StudentCount'];
		}
		
		// consolidate the counting
		for($i=0;$i<count($class_level_records);$i++){
			$year_id = $class_level_records[$i]['ClassLevelID'];
			
			$class_level_records[$i]['EnrolledInSeptember'] = intval($yearIdToSeptemberStudentCount[$year_id]);
			$class_level_records[$i]['EnrolledThisMonth'] = intval($yearIdToStudentCount[$year_id]);
			
			if(isset($yearIdToAttendance[$year_id]) && $yearIdToAttendance[$year_id]['AttendCount']>0){
				// 2020-07-24 (Philips) - fix attendanceRate formula
// 				$class_level_records[$i]['AttendanceRate'] = round(($yearIdToAttendance[$year_id]['AttendCount']-$yearIdToAttendance[$year_id]['AbsentCount']) / $yearIdToAttendance[$year_id]['AttendCount'] * 100,2);
				$class_level_records[$i]['AttendanceRate'] = round($yearIdToAttendance[$year_id]['AttendCount'] / ($yearIdToAttendance[$year_id]['AttendCount'] + $yearIdToAttendance[$year_id]['AbsentCount'])  * 100,2);
			}
		}
		
		return $class_level_records;
	}
	
	/*
	 * $params['Year']
	 * $params['Month']
	 * $params['DisplayLang'] (optional)
	 */
	public function GetStaffSickLeaveMonthlyData($params)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $module_version, $Lang, $intranet_session_language;
		
		if(!($plugin['attendancestaff'] && $module_version['StaffAttendance'] >= 3.0)){
			return array('Error'=>'No Staff Attendance module.');
		}
		
		if(!isset($params['Year']) || !isset($params['Month'])){
			return array('Error'=>'Invalid parameters.');
		}
		
		
		include_once($intranet_root."/includes/libstaffattend3.php");
		
		$year = sprintf("%4d",$params['Year']);
		$month = sprintf("%02d",$params['Month']);
		
		$display_lang = $intranet_session_language;
		if(isset($params['DisplayLang']) && in_array($params['DisplayLang'],array('en','b5','gb'))){
			$display_lang = $params['DisplayLang'];
		}
		
		$name_field = getNameFieldByLang2("u.",$display_lang);
		
		$this_month_ts = strtotime("$year-$month-01");
		$current_ts = $this_month_ts;
		// find the nearest previous september
		$start_ts = $this_month_ts;
		$start_month = date("m",$this_month_ts);
		while($start_month != '09'){
			$start_ts = strtotime("-1 month",$start_ts);
			$start_month = date("m",$start_ts);
		}
		
		$this_year = date("Y",$current_ts);
		$this_month = date("m",$current_ts);
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$this_year."_".$this_month;
		
		// get this month's absent staff users and its absent day count
		$sql = "SELECT 
					u.UserID as TeacherID,
					FORMAT(SUM(d.DutyCount),1) as AbsentCount, ";
		if($this->is_ej){
			$sql .= " 0 as CertificateCount ";	
		}else{
			$sql .= " COUNT(DISTINCT f.DailyLogID) as CertificateCount ";
		}
		$sql .= " FROM INTRANET_USER as u 
				    INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
                    INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$this_year-$this_month-', d.DayNumber) AND d.InAttendanceRecordID = i.RecordID
                    INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID ";
		if(!$this->is_ej){
			$sql.=" LEFT JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as f ON f.DailyLogID=d.RecordID AND f.Year = '".$this_year."' AND f.Month = '".$this_month."' ";
		}
		
		# 2020-10-14 (Philips) - Added CARD_STATUS_LATE, CARD_STATUS_EARLYLEAVE
		$sql .= " WHERE u.RecordType = 1 AND (d.InWaived IS NULL OR d.InWaived = '0') AND
						d.InSchoolStatus = '".CARD_STATUS_ABSENT."' AND (j.ReasonText LIKE '%Sick%' OR j.ReasonText LIKE '%病%')
				    GROUP BY u.UserID 
				    HAVING SUM(d.DutyCount) > 0 ";
		$records = $this->libdb->returnResultSet($sql);
		$recordsAry = BuildMultiKeyAssoc($records, 'TeacherID');
		$sql = "SELECT
					u.UserID as TeacherID,
					Count(d.DayNumber) * 0.5 as AbsentCount, ";
		if($this->is_ej){
			$sql .= " 0 as CertificateCount ";
		}else{
			$sql .= " COUNT(DISTINCT f.DailyLogID) as CertificateCount ";
		}
		$sql .= " FROM INTRANET_USER as u
					INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
					INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$this_year-$this_month-', d.DayNumber) AND d.InAttendanceRecordID = i.RecordID
					INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID ";
		if(!$this->is_ej){
			$sql.=" LEFT JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as f ON f.DailyLogID=d.RecordID AND f.Year = '".$this_year."' AND f.Month = '".$this_month."' ";
		}
		$sql .= " WHERE u.RecordType = 1 AND (d.InWaived IS NULL OR d.InWaived = '0') AND
					d.InSchoolStatus = '".CARD_STATUS_LATE."' AND (j.ReasonText LIKE '%Sick%' OR j.ReasonText LIKE '%病%')
				    GROUP BY u.UserID
				    HAVING Count(d.DayNumber) > 0 ";
		$records2 = $this->libdb->returnResultSet($sql);
		$records2Ary = BuildMultiKeyAssoc($records2, 'TeacherID');
		$sql = "SELECT
					u.UserID as TeacherID,
					Count(d.DayNumber) * 0.5 as AbsentCount, ";
		if($this->is_ej){
			$sql .= " 0 as CertificateCount ";
		}else{
			$sql .= " COUNT(DISTINCT f.DailyLogID) as CertificateCount ";
		}
		$sql .= " FROM INTRANET_USER as u
					INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
					INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$this_year-$this_month-', d.DayNumber) AND d.OutAttendanceRecordID = i.RecordID
					INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID ";
		if(!$this->is_ej){
			$sql.=" LEFT JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as f ON f.DailyLogID=d.RecordID AND f.Year = '".$this_year."' AND f.Month = '".$this_month."' ";
		}
		$sql .= " WHERE u.RecordType = 1 AND (d.OutWaived IS NULL OR d.OutWaived = '0') AND
					d.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' AND d.InSchoolStatus IS NULL AND (j.ReasonText LIKE '%Sick%' OR j.ReasonText LIKE '%病%')
				    GROUP BY u.UserID
				    HAVING Count(d.DayNumber) > 0 ";
		$records3 = $this->libdb->returnResultSet($sql);
		$records3Ary = BuildMultiKeyAssoc($records3, 'TeacherID');
// 		$testResult = array();
// 		$testResult[] = $records;
// 		$testResult[] = $records2;
// 		$testResult[] = $records3;
// 		return $records;
		// get total absent day count since last september
		if(count($records) > 0 || count($records2) > 0 || count($records3) > 0)
		{
			$staff_ids = Get_Array_By_Key($records,'TeacherID');
			$staff_ids2 = Get_Array_By_Key($records2,'TeacherID');
			$staff_ids3 = Get_Array_By_Key($records3,'TeacherID');
			$staff_ids = array_merge($staff_ids,$staff_ids2);
			$staff_ids = array_merge($staff_ids,$staff_ids3);
			$staff_ids = array_unique($staff_ids);
			
			$newRecordAry = array();
			foreach($staff_ids as $sid){
				if($recordsAry[$sid] || $records2Ary[$sid] || $records3Ary[$sid]){
					$newRecordAry[] = array(
							'TeacherID' => $sid,
							'AbsentCount' => $recordsAry[$sid]['AbsentCount'] + $records2Ary[$sid]['AbsentCount'] + $records3Ary[$sid]['AbsentCount'],
							'CertificateCount' => $recordsAry[$sid]['CertificateCount'] + $records2Ary[$sid]['CertificateCount'] + $records3Ary[$sid]['CertificateCount']
					);
				}
			}
			$records = $newRecordAry;
			
			$staff_id_csv = implode(",",$staff_ids);
			$staffIdToAbsentCount = array();
			$staffIdToAbsentDate = array();
			for($temp_ts=$start_ts; $temp_ts<=$this_month_ts; $temp_ts=strtotime("+1 month",$temp_ts))
			{
				$_this_year = date("Y",$temp_ts);
				$_this_month = date("m",$temp_ts);
				$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$_this_year."_".$_this_month;
				$sql = "SELECT 
							u.UserID,
							GROUP_CONCAT(i.RecordDate) AS TargetDate,
							FORMAT(SUM(d.DutyCount),1) as AbsentCount 
						FROM INTRANET_USER as u 
                            INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
                            INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$_this_year-$_this_month-', d.DayNumber) AND d.InAttendanceRecordID = i.RecordID
                            INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID 
						WHERE u.UserID IN ($staff_id_csv) AND u.RecordType = 1 AND (d.InWaived IS NULL OR d.InWaived = '0') AND
							    d.InSchoolStatus = '".CARD_STATUS_ABSENT."' AND (j.ReasonText LIKE '%Sick%' OR j.ReasonText LIKE '%病%')
						GROUP BY u.UserID 
						HAVING SUM(d.DutyCount) > 0 ";
				$temp_records = $this->libdb->returnResultSet($sql);
				for($i=0; $i<count($temp_records); $i++){
					
					if(!isset($staffIdToAbsentCount[$temp_records[$i]['UserID']])){
						$staffIdToAbsentCount[$temp_records[$i]['UserID']] = 0;
					}
					if(!isset($staffIdToAbsentDate[$temp_records[$i]['UserID']])){
						$staffIdToAbsentDate[$temp_records[$i]['UserID']] = '';
					}
					$staffIdToAbsentCount[$temp_records[$i]['UserID']] += $temp_records[$i]['AbsentCount'];
					if($_this_month == $this_month) $staffIdToAbsentDate[$temp_records[$i]['UserID']] .= $temp_records[$i]['TargetDate'] . ',';
				}
				# 2020-10-14 (Philips) - Added CARD_STATUS_LATE, CARD_STATUS_EARLYLEAVE
				$sql = "SELECT
							u.UserID,
							GROUP_CONCAT(i.RecordDate) AS TargetDate,
							COUNT(d.DayNumber) * 0.5 as AbsentCount
							FROM INTRANET_USER as u
							INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
							INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$_this_year-$_this_month-', d.DayNumber) AND d.InAttendanceRecordID = i.RecordID
							INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID
							WHERE u.UserID IN ($staff_id_csv) AND u.RecordType = 1 AND (d.InWaived IS NULL OR d.InWaived = '0') AND
							d.InSchoolStatus = '".CARD_STATUS_LATE."' AND (j.ReasonText LIKE '%Sick%' OR j.ReasonText LIKE '%病%')
						GROUP BY u.UserID
						HAVING COUNT(d.DayNumber) > 0 ";
				$temp_records2 = $this->libdb->returnResultSet($sql);
				for($i=0; $i<count($temp_records2); $i++){
					
					if(!isset($staffIdToAbsentCount[$temp_records2[$i]['UserID']])){
						$staffIdToAbsentCount[$temp_records2[$i]['UserID']] = 0;
					}
					if(!isset($staffIdToAbsentDate[$temp_records2[$i]['UserID']])){
						$staffIdToAbsentDate[$temp_records2[$i]['UserID']] = '';
					}
					$staffIdToAbsentCount[$temp_records2[$i]['UserID']] += $temp_records2[$i]['AbsentCount'];
					if($_this_month == $this_month) $staffIdToAbsentDate[$temp_records2[$i]['UserID']] .= $temp_records2[$i]['TargetDate'] . ',';
				}
				$sql = "SELECT
							u.UserID,
							GROUP_CONCAT(i.RecordDate) AS TargetDate,
							COUNT(d.DayNumber) * 0.5 as AbsentCount
							FROM INTRANET_USER as u
							INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
							INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$_this_year-$_this_month-', d.DayNumber) AND d.OutAttendanceRecordID = i.RecordID
							INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID
							WHERE u.UserID IN ($staff_id_csv) AND u.RecordType = 1 AND (d.OutWaived IS NULL OR d.OutWaived = '0') AND
							d.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' AND d.InSchoolStatus IS NULL AND (j.ReasonText LIKE '%Sick%' OR j.ReasonText LIKE '%病%')
						GROUP BY u.UserID
						HAVING COUNT(d.DayNumber) > 0 ";
				$temp_records3 = $this->libdb->returnResultSet($sql);
				for($i=0; $i<count($temp_records3); $i++){
					
					if(!isset($staffIdToAbsentCount[$temp_records3[$i]['UserID']])){
						$staffIdToAbsentCount[$temp_records3[$i]['UserID']] = 0;
					}
					if(!isset($staffIdToAbsentDate[$temp_records3[$i]['UserID']])){
						$staffIdToAbsentDate[$temp_records3[$i]['UserID']] = '';
					}
					$staffIdToAbsentCount[$temp_records3[$i]['UserID']] += $temp_records3[$i]['AbsentCount'];
					if($_this_month == $this_month) $staffIdToAbsentDate[$temp_records3[$i]['UserID']] .= $temp_records3[$i]['TargetDate'] . ',';
				}
			}
			$table2 = "INTRANET_USER_ADDITIONAL_INFO";
			for($i=0; $i<count($records); $i++){
				$conds = "UserID = '{$records[$i]['TeacherID']}' AND InfoField = 'Position'";
				$sql = "SELECT UserID, InfoField, InfoValue
				FROM $table2
				WHERE $conds";
				$result = $this->libdb->returnArray($sql);
				$records[$i]['PositionId'] = $result[0]['InfoValue'];
				$records[$i]['AbsentCountSinceSeptember'] = $staffIdToAbsentCount[$records[$i]['TeacherID']];
				$tempDatesAry = $staffIdToAbsentDate[$records[$i]['TeacherID']] ? explode(',',substr($staffIdToAbsentDate[$records[$i]['TeacherID']], 0, -1)) : array();
				$tempDatesAry = array_unique($tempDatesAry);
				sort($tempDatesAry);
				$records[$i]['AbsentDates'] = implode(',',$tempDatesAry);
// 				$records[$i]['AbsentDates'] = $staffIdToAbsentDate[$records[$i]['TeacherID']] ? substr($staffIdToAbsentDate[$records[$i]['TeacherID']], 0, -1) : '';
			}
		}
		
		return $records;
	}
	
	public function GetStaffOtherLeaveMonthlyData($params)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $module_version, $Lang, $intranet_session_language;
		
		if(!($plugin['attendancestaff'] && $module_version['StaffAttendance'] >= 3.0)){
			return array('Error'=>'No Staff Attendance module.');
		}
		
		if(!isset($params['Year']) || !isset($params['Month'])){
			return array('Error'=>'Invalid parameters.');
		}
		
		include_once($intranet_root."/includes/libstaffattend3.php");
		
		$lc = new libstaffattend3();
		$attendance_mode = $lc->attendance_mode;
		$is_half_day = in_array($attendance_mode,array('0','1'));
		
		$year = sprintf("%4d",$params['Year']);
		$month = sprintf("%02d",$params['Month']);
		
		$display_lang = $intranet_session_language;
		if(isset($params['DisplayLang']) && in_array($params['DisplayLang'],array('en','b5','gb'))){
			$display_lang = $params['DisplayLang'];
		}
		
		$name_field = getNameFieldByLang2("u.",$display_lang);
		
		$this_month_ts = strtotime("$year-$month-01");
		$current_ts = $this_month_ts;
		// find the nearest previous september
		$start_ts = $this_month_ts;
		$start_month = date("m",$this_month_ts);
		while($start_month != '09'){
			$start_ts = strtotime("-1 month",$start_ts);
			$start_month = date("m",$start_ts);
		}
		
		$this_year = date("Y",$current_ts);
		$this_month = date("m",$current_ts);
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$this_year."_".$this_month;
		
		// get this month's absent staff users and its absent day count
		$sql = "SELECT
		            u.UserID as TeacherID,
					SUM(d.DutyCount) as AbsentCount, ";
		if($this->is_ej){
			$sql .= " 0 as CertificateCount ";
		}else{
			$sql .= " COUNT(DISTINCT f.DailyLogID) as CertificateCount ";
		}
		$sql .= " FROM INTRANET_USER as u
		            INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
                    INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$this_year-$this_month-', d.DayNumber)
                    INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID ";
		if(!$this->is_ej){
			$sql .= " LEFT JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as f ON f.DailyLogID = d.RecordID AND f.Year = '".$this_year."' AND f.Month = '".$this_month."' ";
		}
		$sql .= " WHERE u.RecordType = 1 AND (d.InWaived IS NULL OR d.InWaived = '0') AND 
                            (d.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' OR 
                            (d.InSchoolStatus = '".CARD_STATUS_ABSENT."' AND j.ReasonText NOT LIKE '%Sick%' AND j.ReasonText NOT LIKE '%病%'))
				    GROUP BY u.UserID
				    HAVING SUM(d.DutyCount) > 0 ";
		//debug_pr($sql);
		$records = $this->libdb->returnResultSet($sql);
		
		// get total absent day count since last september
		if(count($records) > 0)
		{
			$staff_ids = Get_Array_By_Key($records,'TeacherID');
			$staff_id_csv = implode(",",$staff_ids);
			$staffIdToAbsentCount = array();
			for($temp_ts=$start_ts; $temp_ts<=$this_month_ts; $temp_ts=strtotime("+1 month",$temp_ts))
			{
				$this_year = date("Y",$temp_ts);
				$this_month = date("m",$temp_ts);
				$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$this_year."_".$this_month;
				$sql = "SELECT
                            u.UserID,
                            SUM(d.DutyCount) as AbsentCount,
                            GROUP_CONCAT(i.RecordDate) AS TargetDate
                        FROM INTRANET_USER as u
                            INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID
                            INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = u.UserID and i.RecordDate = CONCAT('$this_year-$this_month-', d.DayNumber)
                            INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID
                        WHERE u.UserID IN ($staff_id_csv) AND u.RecordType = 1 AND (d.InWaived IS NULL OR d.InWaived = '0') AND 
                                (d.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' OR 
                                (d.InSchoolStatus = '".CARD_STATUS_ABSENT."' AND j.ReasonText NOT LIKE '%Sick%' AND j.ReasonText NOT LIKE '%病%'))
                        GROUP BY u.UserID
                        HAVING SUM(d.DutyCount) > 0 ";
				$temp_records = $this->libdb->returnResultSet($sql);
				for($i=0; $i<count($temp_records); $i++) {
					if(!isset($staffIdToAbsentCount[$temp_records[$i]['UserID']])){
						$staffIdToAbsentCount[$temp_records[$i]['UserID']] = 0;
					}
					if(!isset($staffIdToAbsentDate[$temp_records[$i]['UserID']])){
						$staffIdToAbsentDate[$temp_records[$i]['UserID']] = '';
					}
					$staffIdToAbsentCount[$temp_records[$i]['UserID']] += $temp_records[$i]['AbsentCount'];
					$staffIdToAbsentDate[$temp_records[$i]['UserID']] .= $temp_records[$i]['TargetDate'] . ',';
				}
			}
			$table2 = "INTRANET_USER_ADDITIONAL_INFO";
			for($i=0; $i<count($records); $i++){
				$conds = "UserID = '{$records[$i]['TeacherID']}' AND InfoField = 'Position'";
				$sql = "SELECT UserID, InfoField, InfoValue
						FROM $table2
						WHERE $conds";
				$result = $this->libdb->returnArray($sql);
				$records[$i]['PositionId'] = $result[0]['InfoValue'];
				$records[$i]['AbsentCountSinceSeptember'] = $staffIdToAbsentCount[$records[$i]['TeacherID']];
				$records[$i]['AbsentDates'] = $staffIdToAbsentDate[$temp_records[$i]['UserID']] ? substr($staffIdToAbsentDate[$temp_records[$i]['UserID']], 0, -1) : '';
			}
		}
		
		return $records;
	}
	
	public function GetStudentECAData($params){
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $Lang, $intranet_session_language;
		if(!$plugin['eEnrollment']){
			return array('Error'=>'No Student Enrollment module.');
		}
		
		if(!isset($params['AcademicYearID'])){ // need to count attendance rate til the target date
			return array('Error'=>'Invalid parameters.');
		}
		$AcademicYearID = $params['AcademicYearID'];
		$Month = $params['month'];
		$Year = $params['year'];
		$isSchoolActivity = $params['IsSchoolActivity'];
		
		$thisMonth = strtotime($Year.'-'.$Month);
		
		// get this AcademicYear All Activity Info 
		include_once($intranet_root."/includes/libclubsenrol.php");
		$libenroll = new libclubsenrol($AcademicYearID);
		
		$InternalExternalConds = $isSchoolActivity == '1'? "AND SchoolActivity = '1'" : "AND SchoolActivity IN ('0','2')";
		
		$ActivityInfo = $libenroll->getActivityInfoByConditions($AcademicYearID,$InternalExternalConds);
		$ReturnData = array();
		$j=0;
		for($i=0;$i<sizeof($ActivityInfo);$i++){
			$EventID = $ActivityInfo[$i]['EnrolEventID'];
		
			// get event first date
			$MettingDateAry = $libenroll->GET_ENROL_EVENT_DATE($EventID);
			$__thisMeetingDateAry = array();
			for($k=0;$k<count($MettingDateAry);$k++){
			    $_thisMeetingDate= substr($MettingDateAry[$k]['ActivityDateStart'],0,7);
			    $_thisEventDate = strtotime($_thisMeetingDate);
			 
			    if($_thisEventDate == $thisMonth){
			       
			        $__thisMeetingDateAry[]  = substr($MettingDateAry[$k]['ActivityDateStart'],0,10);
			        
			        
			    }
			}
						
			if(!empty($__thisMeetingDateAry)){
			    
			    $EventTitle= $ActivityInfo[$i]['EventTitle'];
			    $eventMember = $libenroll->Get_Activity_Student_Enrollment_Info($EventID);
			    $StudentNo = sizeof($eventMember[$EventID]['StatusStudentArr']['2']);
			    
			    $OLESettingAry = $libenroll->Get_Enrolment_Default_OLE_Setting('activity',(array)$EventID);
			    
			    
			    $ReturnData[$j]['activityName'] = $EventTitle;
			    $ReturnData[$j]['date'] = implode(',',$__thisMeetingDateAry);
			    $ReturnData[$j]['categoryID']  = $OLESettingAry[0]['OLE_Component'];			    
			    $ReturnData[$j]['participantsNo'] = $StudentNo;
			    
			    $j++;
			}
			
// 			$FirstMeetingDate= substr($MettingDateAry[0]['ActivityDateStart'],0,7);		
// 			$thisEventDate = strtotime($FirstMeetingDate);
						
// 			if($thisEventDate == $thisMonth){
// 				$EventTitle= $ActivityInfo[$i]['EventTitle'];
// 				$MeetingDate  = substr($MettingDateAry[0]['ActivityDateStart'],0,10);
				
// 				$eventMember = $libenroll->Get_Activity_Student_Enrollment_Info($EventID);
// 				$StudentNo = sizeof($eventMember[$EventID]['StatusStudentArr']['2']);
// // 				debug_pr($MeetingDate);
// 				$ReturnData[$j]['activityName'] = $EventTitle;
// 				$ReturnData[$j]['date'] = $MeetingDate;
// 				$ReturnData[$j]['participantsNo'] = $StudentNo;
// 				$j++;
// 			}
				
		}
	
	//	Get_Activity_Meeting_Date
		//GET_ENROL_EVENT_DATE 
		return $ReturnData;
	}
	
	public function GetStaffAppointmentTerminationData($params){
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $Lang, $intranet_session_language;
		$Month = $params['month'];
		$Year = $params['year'];
		$Type = $params['type'];
		if($Month < 10) $Month = '0'.$Month;
		$startDate = date($Year.'-'.$Month.'-01');
		$endDate = date($Year.'-'.$Month.'-t');
		if($Type=='Appointment'){
			$conds = "a.InfoField = 'ContractStartDate'";
			$dateField = 'ContractStartDate';
		}else {
			$conds = "a.InfoField = 'ContractEndDate'";
			$dateField = 'ContractEndDate';
		}
		$conds .= " AND a.InfoValue >= '$startDate' AND a.InfoValue <= '$endDate' ";
		$table = "INTRANET_USER_ADDITIONAL_INFO";
		$sql = "SELECT b.UserID, b.InfoField, b.InfoValue 
				FROM $table a 
				INNER JOIN $table b
				ON a.UserID = b.UserID
				WHERE $conds";
		$result = $this->libdb->returnArray($sql);
// 		return $sql;die();
		$data = array();
		foreach($result as $info){
			$uid = $info['UserID'];
			$data[$uid][$info['InfoField']] = $info['InfoValue'];
		}
		$ReturnData = array();
		foreach($data as $uid => $info){
			$ReturnData[] = array(
				'userId' => $uid,
				'positionId' => $info['Position'],
				'typeId' => $info['ContractType'],
				'date' => $info[$dateField]
			);
		}
		
		return $ReturnData;
	}
	
	public function GetStudentEnrolQuotaData($params){
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $Lang, $intranet_session_language;
		$conds = "iu.RecordType = '2' AND iu.RecordStatus ='1' AND (iups.KISClassType IS NOT NULL AND iups.KISClassType <> '9')";
		$table = "INTRANET_USER";
		$table2 = "INTRANET_USER_PERSONAL_SETTINGS";
		$sql = "SELECT iups.KISClassType AS KISClassType, COUNT(*) AS StudentTotal
					FROM $table iu
					INNER JOIN $table2 iups
					ON iu.UserID = iups.UserID
					WHERE $conds
					GROUP BY iups.KISClassType
					ORDER BY iups.KISClassType
				";
		$result = $this->libdb->returnArray($sql);
		// 		return $sql;die();
		$data = BuildMultiKeyAssoc($result, 'KISClassType');
		$classTypeAry = array('Enrol_Mixed_Half','Enrol_Mixed_Whole','Enrol_AM_Half','Enrol_AM_Whole','Enrol_PM_Half');
		$ReturnData = array();
		$count = 0;
		foreach($classTypeAry as $k => $classType){
			$ReturnData[$classType] = $data[$k+1]['StudentTotal'] ? $data[$k+1]['StudentTotal'] : '0';
			$count += $data[$k+1]['StudentTotal'];
		}
		$ReturnData['StudentCount'] = $count;
		
		return $ReturnData;
	}
	
}

?>