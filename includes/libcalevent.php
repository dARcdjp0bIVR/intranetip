<?php
## Using :

################ Change Log [Start] #####################
#
#   Date:   2019-07-18 [Bill]
#               modified Get_Preload_Result(), Set_Preload_Result(), Check_PreloadInfoArr_Exist(), to remove ';' to prevent eval() security problem
#
#   Date:   2017-09-05 [Anna]
#               modified returnEventByDate(),returnEventByType(), display titleEng according to UI
#
#	Date:	2015-11-19 [Ivan] [M89359] [ip.2.5.7.1.1.0]
#				modified returnAllEvents() to change the group event recordtype from 3 to 2 (EJ is 3 whereas IP is 2)
#			
#	Date:	2014-05-13 [Carlos]
#				added returnAllEventRecordByTimestampRange(), initAllEventRecordByTimestampRange()
#
#	Date:	2013-10-04 [Carlos]
#				[KIS] modified initAllEventRecordByTimestamp(), returnAllEventRecordByTimestamp(), add parameter $ClassGroupID
#
#	Date:	2010-09-22 [Ronald]
#				update displayEventByDateRange(), update all the CSS 
# 
#	Date:	2010-07-15 [YatWoon]
# 				update libcalevent() - don't define $this->EventArray and $this->returnAllEventRecord every new class
#				update displayCalendarEvent(), displayCalendarEvent_portlet() - define $this->EventArray and $this->returnAllEventRecord
#
#	Date:	2010-05-05	[YatWoon]
#			Add "School holiday" in small calendar
#
################ Change Log [End] #####################

class libcalevent extends libcal {

     var $UserID;
     var $Booking;
     var $BookingID;
     var $ResourceID;
     var $DateStart;
     var $DateEnd;
     var $Remark;
     var $EventArray;
     var $AllEventArray;
     var $IsWhite;
     var $IsResource;

     ###############################################################################

     function libcalevent($ts="", $v=""){
          global $UserID;
          $this->UserID = $UserID;
          $this->libcal($ts,$v);
          /*
          $this->EventArray = $this->returnEventRecord();
          $this->AllEventArray = $this->returnAllEventRecord();
          */
          
		  /* tracing
		  debug($UserID);
		  global $counter;
		  $counter ++;
		  debug($counter);
		  */
    }

     ###############################################################################

     function returnEventRecord()
     {
          $sql = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1'";
          $row1 = $this->returnArray($sql,4);
          $sql = "SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType, Title FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2'";
          $row2 = $this->returnArray($sql,4);
          $sql = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = '1'";
          $row3 = $this->returnArray($sql,4);
          $result = array();
          if (sizeof($row1) != 0)
          {
              $result = $row1;
          }
          if (sizeof($row2) != 0)
          {
              $result = array_merge($result, $row2);
          }
          if (sizeof($row3) != 0)
          {
              $result = array_merge($result, $row3);
          }
          return $result;

     }
     
     function returnAllEventRecord()
     {
     	  ## Get All Group Event ##
          $sql = "SELECT DISTINCT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b WHERE b.EventID = a.EventID AND a.RecordStatus = '1'";
          $row1 = $this->returnArray($sql,4);
          ## Get All Event except Group Event ##
          $sql = "SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType, Title FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2'";
          $row2 = $this->returnArray($sql,4);
          ## Get Any Missing Group Event (Only Have Record In INTRANET_EVENT) ##
          $sql = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = '1'";
          $row3 = $this->returnArray($sql,4);
          $result = array();
          if (sizeof($row1) != 0)
          {
              $result = $row1;
          }
          if (sizeof($row2) != 0)
          {
              $result = array_merge($result, $row2);
          }
          if (sizeof($row3) != 0)
          {
              $result = array_merge($result, $row3);
          }
          return $result;
     }
     
     ###
     function returnEventRecordByTimestamp($ts)
     {
          $sql1 = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' AND UNIX_TIMESTAMP(a.EventDate) = '$ts'";
          
          $sql2 = "SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType, Title FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2' AND UNIX_TIMESTAMP(EventDate) = '$ts'";
          
          $sql3 = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = '1' AND UNIX_TIMESTAMP(a.EventDate) = '$ts'";
          
          $sql = "(".$sql1.") UNION (".$sql2.") UNION (".$sql3.")";
          $row = $this->returnArray($sql,4);
          $result = $row;
          return $result;
     }
     
     function returnAllEventRecordByTimestamp($ts, $ClassGroupID='')
     {
     	global $sys_custom;
     	$PreloadArrKey = 'returnAllEventRecordByTimestamp';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
	    if($isKIS && $ClassGroupID != '') {
	    	if(is_array($ClassGroupID)) {
	    		$ClassGroupIdCsv = implode(",",$ClassGroupID);
	    	}else{
	    		$ClassGroupIdCsv = $ClassGroupID;
	    	}
	    	//if($_SESSION['UserType'] != USERTYPE_STAFF){
	    		$classGroupIdCond = " AND (a.ClassGroupID IN ($ClassGroupIdCsv) OR a.ClassGroupID IS NULL OR a.ClassGroupID='') ";
	    	//}else{
	    	//	$classGroupIdCond = " AND a.ClassGroupID IN ($ClassGroupIdCsv) ";
	    	//}
	    }	
	    if($_SESSION["platform"]=="KIS"){
	    	$canGetAllEvents = ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $_SESSION['UserType'] == USERTYPE_STAFF);    
	    }else{
	    	$canGetAllEvents = true;
	    }
	    
	    if($canGetAllEvents) {

          ## Get All Group Event ##
          $sql1 = "SELECT DISTINCT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b WHERE b.EventID = a.EventID AND a.RecordStatus = '1' AND UNIX_TIMESTAMP(a.EventDate) = '$ts' $classGroupIdCond";
         
          ## Get All Event except Group Event ##
          $sql2 = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT as a WHERE a.RecordStatus = '1' AND a.RecordType <>'2' AND UNIX_TIMESTAMP(a.EventDate) = '$ts' $classGroupIdCond";
          
          ## Get Any Missing Group Event (Only Have Record In INTRANET_EVENT) ##
          $sql3 = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = '1' AND UNIX_TIMESTAMP(a.EventDate) = '$ts' $classGroupIdCond";
          
          $sql = "(".$sql1.") UNION (".$sql2.") UNION (".$sql3.")";
	    }else{
	      ## Get Related Group Event ##
          $sql = "SELECT DISTINCT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a INNER JOIN INTRANET_GROUPEVENT AS b ON a.EventID=b.EventID INNER JOIN INTRANET_USERGROUP AS c ON c.GroupID=b.GroupID WHERE a.RecordStatus = '1' AND c.UserID = ".$_SESSION['UserID']." AND UNIX_TIMESTAMP(a.EventDate) = '$ts' $classGroupIdCond";
          
          ## Get All Event except Group Event ##
          $sql .= "UNION (SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT as a WHERE a.RecordStatus = '1' AND a.RecordType <>'2' AND UNIX_TIMESTAMP(a.EventDate) = '$ts' $classGroupIdCond)";
	    }
          $ReturnArr = $this->returnArray($sql,4);
          
          $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
		  return $ReturnArr;
     }
     
     function initEventRecordByTimestamp($ts)
     {
     	$this->EventArray = $this->returnEventRecordByTimestamp($ts);
     }
     
     function initAllEventRecordByTimestamp($ts, $ClassGroupID='')
     {
     	$this->AllEventArray = $this->returnAllEventRecordByTimestamp($ts, $ClassGroupID);
     }
     ###

     function returnEventByDate($ts)
     {
          $sql = "SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType, 
                        If (TitleEng IS NULL Or TitleEng = '',
                        Title, 
                        ".Get_Lang_Selection('Title','TitleEng').") 
                        as Title
                        FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2' AND UNIX_TIMESTAMP(EventDate) = $ts ORDER BY RecordType";
          $row2 = $this->returnArray($sql,4);
          $sql = "SELECT DISTINCT(a.EventID) FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' AND UNIX_TIMESTAMP(a.EventDate) = $ts";
          $id1 = $this->returnVector($sql);
          $sql = "SELECT DISTINCT(a.EventID) FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT AS b ON a.EventID = b.EventID WHERE a.RecordType = '2' AND a.RecordStatus = '1' AND b.EventID IS NULL AND UNIX_TIMESTAMP(EventDate) = $ts";
          $id2 = $this->returnVector($sql);
          if (sizeof($id1)!=0 || sizeof($id2)!=0)
          {
              $list = implode(",",array_merge($id1,$id2));
              $sql = "SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType, Title FROM INTRANET_EVENT WHERE EventID IN ($list)";
              $row3 = $this->returnArray($sql,4);
              $result = array_merge($row2, $row3);
          }
          else
          {
              $result = $row2;
          }
          return $result;
     }

     function returnNonGroupEventByDateRange($start, $end)
     {
              $sql = "SELECT EventID, DATE_FORMAT(EventDate,'%Y-%m-%d'), RecordType, Title
                      FROM INTRANET_EVENT
                      WHERE UNIX_TIMESTAMP(EventDate) BETWEEN $start AND $end
                      ORDER BY EventDate";
              return $this->returnArray($sql,4);
     }

     function returnEventByType($tp,$order=0, $AcademicYearID="")
     {
              $ord_str = $order==0?"ASC":"DESC";
              
              $acyear = "";
              /*
              if($StartYear!="" and $EndYear!="")
              {
              	$startdate	= $StartYear . "-09-01";
                $enddate	= $EndYear ."-08-31";
                
                $acyear = " and EventDate >= '$startdate' and EventDate <= '$enddate'";
              }
              */
              if($AcademicYearID)
              {
              	$startdate = substr(getStartDateOfAcademicYear($AcademicYearID),0,10);
              	$enddate = substr(getEndDateOfAcademicYear($AcademicYearID),0,10);
              	$acyear = " and EventDate >= '$startdate' and EventDate <= '$enddate'";
          		}
          $titleSQL = ", If (TitleEng IS NULL Or TitleEng = '',
                       Title, 
                       ".Get_Lang_Selection('Title','TitleEng').") 
                       as Title";		
              
          switch($tp){
               case 0: $sql="SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType $titleSQL FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType = '0' $acyear ORDER BY EventDate $ord_str"; break;
               case 1: $sql="SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType $titleSQL FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType = '1' $acyear ORDER BY EventDate $ord_str"; break;
               case 2:
                    $sql="SELECT a.EventID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' ORDER BY a.EventDate $ord_str";
                    $id1 = $this->returnVector($sql);
                    $sql = "SELECT a.EventID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT AS b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = 1";
                    $id2 = $this->returnVector($sql);

                    if (sizeof($id1)!=0 || sizeof($id2)!=0)
                    {
                        $list = implode(",", array_merge($id1,$id2));
                        $sql = "SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType $titleSQL FROM INTRANET_EVENT WHERE EventID IN ($list) $acyear ORDER BY EventDate $ord_str";
                        break;
                    }
                    else
                    {
                        return array();
                    }
				case 3: $sql="SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType $titleSQL FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType = '3' $acyear ORDER BY EventDate $ord_str"; break;
				case 4: $sql="SELECT EventID, UNIX_TIMESTAMP(EventDate), RecordType $titleSQL FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType = '4' $acyear ORDER BY EventDate $ord_str"; break;

          }
          return $this->returnArray($sql,4);
     }

     function IsEvent($ts,$AllEvent=''){
     	  if($AllEvent == 1) {
     	  	//$row = $this->returnAllEventRecord();
     	  	$row = $this->AllEventArray;
     	  	//$row = $this->returnAllEventRecordByTimestamp($ts);
     	  } else {
     	  	//$row = $this->returnEventRecord();
          	$row = $this->EventArray;
          	//$row = $this->returnEventRecordByTimestamp($ts);
     	  }
          	
          for($i=0; $i<sizeof($row); $i++)
          	if($row[$i][1]==$ts) return 1;
          return 0;	         
     }

     function IsSchoolEvent($ts,$AllEvent=''){
         if($AllEvent == 1) {
     	 	//$row = $this->returnAllEventRecord();
     	  	$row = $this->AllEventArray;
     	  	//$row = $this->returnAllEventRecordByTimestamp($ts);
     	 } else {
     	 	//$row = $this->returnEventRecord();
          	$row = $this->EventArray;
          	//$row = $this->returnEventRecordByTimestamp($ts);
     	 }
          	
         for($i=0; $i<sizeof($row); $i++)
         	if($row[$i][1]==$ts && $row[$i][2]==0) return 1;
         return 0;
     }

     function IsAcademicEvent($ts,$AllEvent=''){
         if($AllEvent == 1) {
     	  	//$row = $this->returnAllEventRecord();
     	  	$row = $this->AllEventArray;
     	  	//$row = $this->returnAllEventRecordByTimestamp($ts);
     	 } else {
     	  	//$row = $this->returnEventRecord();
          	$row = $this->EventArray;
          	//$row = $this->returnEventRecordByTimestamp($ts);
     	 }
         
         for($i=0; $i<sizeof($row); $i++)
         	if($row[$i][1]==$ts && $row[$i][2]==1) return 1;
         return 0;
     }
     
     function IsGroupEvent($ts,$AllEvent=''){
         if($AllEvent == 1) {
     	  	//$row = $this->returnAllEventRecord();
     	  	$row = $this->AllEventArray;
     	  	//$row = $this->returnAllEventRecordByTimestamp($ts);
     	 } else {
     	  	//$row = $this->returnEventRecord();
          	$row = $this->EventArray;
          	//$row = $this->returnEventRecordByTimestamp($ts);
     	 }
         
         for($i=0; $i<sizeof($row); $i++)
         	if($row[$i][1]==$ts && $row[$i][2]==2) return 1;
         return 0;
     }
     
     function IsHolidayEvent($ts,$AllEvent=''){
          if($AllEvent == 1) {
     	  	//$row = $this->returnAllEventRecord();
     	  	$row = $this->AllEventArray;
     	  	//$row = $this->returnAllEventRecordByTimestamp($ts);
     	  } else {
     	  	//$row = $this->returnEventRecord();
          	$row = $this->EventArray;
          	//$row = $this->returnEventRecordByTimestamp($ts);
     	  }
     	  	
          for($i=0; $i<sizeof($row); $i++)
          	if($row[$i][1]==$ts && $row[$i][2]==3) return 1;
          return 0;
     }

     function IsSchoolHolidayEvent($ts,$AllEvent=''){
          if($AllEvent == 1) {
     	  	//$row = $this->returnAllEventRecord();
     	  	$row = $this->AllEventArray;
     	  	//$row = $this->returnAllEventRecordByTimestamp($ts);
     	  } else {
     	  	//$row = $this->returnEventRecord();
          	$row = $this->EventArray;
          	//$row = $this->returnEventRecordByTimestamp($ts);
     	  }
          	
          for($i=0; $i<sizeof($row); $i++)
          	if($row[$i][1]==$ts && $row[$i][2]==4) return 1;
          return 0;
     }
     ###############################################################################

     function displayCalendarArray($month, $year,$order=""){
          global $i_frontpage_day,$i_EventList,$image_path, $intranet_session_language;
          $row = $this->returnCalendarArray($month, $year);
          $a = new libcycleperiods();
          $cycles_array = $a->getCycleInfoByYearMonth($year,$month);

          if ($order == "")
          {
              $top_image = "top.gif";
              $x .=  "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
              $x .= "<tr><td><img src=/images/index/$top_image></td></tr></table>\n";
          }
          $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
          $x .= "<tr>";
          $td_style = "\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-weight: bold; text-align: center; vertical-align: bottom; background-image: url(/images/index/tab.gif)\"";
          if ($order == "")
          {
              $x .= "<td width=110 STYLE=$td_style>";  # 83
              // # 84
              $x .= "$year/$month</td><td width=84 align=right><a href=javascript:showEventList()>$i_EventList</a></td>
              <td width=9><img src=$image_path/index/green_lh.gif></td></tr></table>\n";   # 20
          }
          else
          {
              $x .= "<td width=110 STYLE=$td_style>";  # 83
              // # 84
              $x .= "$year/$month</td><td width=84 align=right bgcolor=#8CCB21>&nbsp;</td>
              <td width=9><img src=$image_path/index/green_lh.gif></td></tr></table>\n";   # 20
              /*
              $x .= "<td width=183 class=h1>";
              $x .= "$year/$month</td></tr></table>\n";
              */
          }
          #$x .= "<table background=/images/index/cal_tbbg.gif width=203 border=0 cellspacing=0 cellpadding=0>\n";
          $x .= "<table background=/images/index/cellbg_white.gif width=203 border=0 cellspacing=0 cellpadding=0>\n";
#          $x .= "<tr><td align=center><table width=175 border=0 cellspacing=0 cellpadding=2 class=body>\n";
          $x .= "<tr><td align=center><table width=182 border=1 bordercolorlight=#B3B692 bordercolordark=#F8FBD6 cellspacing=0 cellpadding=0 class=body>\n";
          $x .= "<tr align=center valign=middle>\n";
          $x .= "<td  align=center>".$i_frontpage_day[0]."</td>\n";
          $x .= "<td align=center>".$i_frontpage_day[1]."</td>\n";
          $x .= "<td  align=center>".$i_frontpage_day[2]."</td>\n";
          $x .= "<td  align=center>".$i_frontpage_day[3]."</td>\n";
          $x .= "<td  align=center>".$i_frontpage_day[4]."</td>\n";
          $x .= "<td align=center>".$i_frontpage_day[5]."</td>\n";
          $x .= "<td  align=center>".$i_frontpage_day[6]."</td>\n";
          for($i=0; $i<sizeof($row); $i++){
               $x .= ($i%7==0) ? "</tr>\n<tr>\n" : "";
               $date_string = date("Y-m-d",$row[$i]);
               $cycle = $cycles_array[$date_string];
               $display = $cycle[2];
               $x .= $this->displayCalendarEvent($row[$i],$display);
          }
          $x .= "</tr></table></td></tr></table>\n";
          $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
          #$x .= "<tr><td><img src=/images/index/cal_bottom.gif></td></tr></table>\n";
          $x .= "<tr><td><img src=/images/index/bottom.gif></td></tr></table>\n";

          return $x;
     }

     function displayCalendarEvent($ts, $cycle){
	     
		if(!isset($this->EventArray))
			$this->EventArray = $this->returnEventRecord();
		if(!isset($this->AllEventArray))
			$this->AllEventArray = $this->returnAllEventRecord();
          
          if($ts==""){
               $x = "<td align=center><br></td>";
          }else{
               if($this->IsSchoolEvent($ts)) $ClassType = "class=cal_bg_school";
               if($this->IsAcademicEvent($ts)) $ClassType = "class=cal_bg_academic";
               if($this->IsAcademicEvent($ts) && $this->IsSchoolEvent($ts)) $ClassType = "class=cal_bg_both";
               $Event  = ($this->IsHolidayEvent($ts)) ? "<font color=red>".date("j",$ts)."</font>" : date("j",$ts);
               $Event .= ($this->IsGroupEvent($ts)) ? "*" : "";
               if ($cycle=="") $cycle = "&nbsp;";
               $x  = "<td $ClassType align=center>";
               if($this->IsResource){
                    $x .= "<a href=?ts=$ts>".$Event."</a>";
               }else{
                    $Event = ($this->IsEvent($ts)) ? "<a href=javascript:fe_view_event_by_date($ts)>".$Event."</a>" : $Event;

                    $x .= "<table width=24 border=0 cellpadding=0 cellspacing=0 class=body>
                    <tr>
                      <td><I><font color=#0046A0>$cycle</font></I></td>
                    </tr>
                    <tr>
                      <td align=center>$Event</td>
                    </tr>
                  </table>";

               }
               $x .= "</td>\n";
          }
          return $x;
     }

     ###############################################################################

     function displayCalendarResource(){
          $x .= $this->displayCalendarArray($this->m, $this->y);
          return $x;
     }

     function returnAllEvents($lower_bound="",$upper_bound="")
     {
              if ($lower_bound == "" || $upper_bound == "")
              {
              }
              else
              {
                  $conds = " AND UNIX_TIMESTAMP(EventDate) BETWEEN $lower_bound AND $upper_bound";
              }
              //$sql = "SELECT a.EventID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' AND a.RecordType = '3' $conds";
              $sql = "SELECT a.EventID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' AND a.RecordType = '2' $conds";
              $row1 = $this->returnVector($sql);
              //$sql = "SELECT EventID FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'3' $conds";
              $sql = "SELECT EventID FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2' $conds";
              $row2 = $this->returnVector($sql);
              //$sql = "SELECT a.EventID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '3' AND a.RecordStatus = 1 $conds";
              $sql = "SELECT a.EventID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = 1 $conds";
              $row3 = $this->returnVector($sql);
              $all = array_merge($row1, $row2);
              $all = array_merge($all, $row3);
              if (sizeof($all) == 0) return array();
              $sql = "SELECT EventID, DATE_FORMAT(EventDate,'%Y-%m-%d'), Title, RecordType
                       FROM INTRANET_EVENT
                       WHERE EventID IN (".implode(",",$all).") ORDER BY EventDate";
              return $this->returnArray($sql,4);

     }
     function displayCalendar(){
               global $image_path,$intranet_httppath,$i_EventShowAll,$i_EventCloseImage;
               $rb_image_path = "$image_path/resourcesbooking";
               $lower_bound = mktime(0,0,0,$this->m,1,$this->y);
               $upper_bound = mktime(0,0,0,$this->m+2,1,$this->y) - 1;
               /*
               $sql = "SELECT EventID, DATE_FORMAT(EventDate,'%Y-%m-%d'), Title, RecordType
                       FROM INTRANET_EVENT
                       WHERE RecordStatus = 1
                       AND UNIX_TIMESTAMP(EventDate) BETWEEN $lower_bound AND $upper_bound
                       AND RecordType IN (0,1,2) ORDER BY EventDate";
               $event_list = $this->returnArray($sql,4);
               */
               $event_list = $this->returnAllEvents($lower_bound,$upper_bound);
               $x .= "<script language=javascript>\n";
               # $x .= "var table_head = '<table width=448 border=0 cellspacing=0 cellpadding=0><tr><td><img src=$rb_image_path/itemtopbar.gif height=15></td></tr></table>';\n";
               # $x .= "table_head += '<table width=448 border=0 cellspacing=0 cellpadding=0 background=$rb_image_path/itembg.gif><tr><td style=\"vertical-align:bottom\" width=429></td></tr></table>';\n";
               $x .= "var table_head = '<table width=260 border=1 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td align=center><table width=250 border=0 cellpadding=3 cellspacing=0 class=body>';";
               $x .= "table_head += '<tr><td>&nbsp;</td><td align=right><a href=javascript:hideLayer(\"ToolMenu\")>$i_EventCloseImage</a></td></tr>';";
               $x .= "table_head += '<tr><td colspan=2 align=center><img src=$image_path/index/el_2lines.gif width=223 height=8></td></tr>';";
               # $x .= "var table_tail = '<table width=448 border=0 cellspacing=0 cellpadding=0 background=$rb_image_path/itembg.gif><tr><td width=429 align=right><a href=javascript:showAllEventList()>$i_EventShowAll </a></td><td></td></tr><tr><td style=\"vertical-align:bottom\" width=429><img src=$rb_image_path/itembottom.gif></td><td width=19><a href=javascript:hideMenu(\"ToolMenu\")><img border=0 src=$rb_image_path/itemclose.gif></a></td></tr></table>';\n";
               $x .= "var table_tail = '<tr align=center><td colspan=2 valign=top><img src=$image_path/index/el_1lines.gif width=223 height=8></td></tr><tr><td>&nbsp;</td>';";
               $x .= "table_tail += '<td align=right><a href=javascript:showAllEventList()>$i_EventShowAll</a></td></tr>';";
               $x .= "table_tail += '<tr align=center><td colspan=2 valign=top><img src=$image_path/index/el_1lines.gif width=223 height=8></td></tr><tr><td>&nbsp;</td><td align=right><a href=javascript:hideLayer(\"ToolMenu\")>$i_EventCloseImage</a></td></tr></table>';";
               $x .= "var event_list_text = '';\n";
               for ($i=0; $i<sizeof($event_list); $i++)
               {
                    list($eventID, $eventDate,$eventTitle,$eventType) = $event_list[$i];
                    $eventTitle = addslashes($eventTitle);
                    $x .= "event_list_text += '<tr><td width=90>$eventDate</td><td width=160><a href=javascript:fe_view_event($eventID)>$eventTitle</a></td></tr>';\n";
               }
               $x .= "var event_layer_text = table_head+event_list_text+table_tail;\r\n";
               $x .= "function showEventList()
                      {
                               writeToLayer('ToolMenu',event_layer_text)
                               showLayer('ToolMenu');
                      }
                      function showAllEventList()
                      {
                               newWindow('$intranet_httppath/home/allevent.php');
                      }
               \n";
               $x .= "</script>\n";
          $x .= $this->displayCalendarArray($this->m, $this->y);
          $x .= $this->displayCalendarArray(date("m",mktime(0,0,0,$this->m+1,1,$this->y)), date("Y",mktime(0,0,0,$this->m+1,1,$this->y)),2);
          return $x;
     }

     function displayPrevMonth(){
          global $intranet_session_language;
          $x = "<a href=?v=".$this->view."&ts=".$this->go(-1,"m")."><img src=".$this->image_path."/index/cal_btnpre_$intranet_session_language.gif border=0></a>\n";
          return $x;
     }

     function displayNextMonth(){
          global $intranet_session_language;
          $x = "<a href=?v=".$this->view."&ts=".$this->go(+1,"m")."><img src=".$this->image_path."/index/cal_btnnext_$intranet_session_language.gif border=0></a>\n";
          return $x;
     }

     ###############################################################################

     function displayEventByDate($ts){
          global $i_EventTypeSchool, $i_EventTypeAcademic, $i_EventTypeHoliday, $i_EventTypeGroup, $Lang;
          $row = $this->returnEventByDate($ts);
          
	$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>";
        $x .= "<tr>";
        $x .= "		<td>";
        $x .= "			<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $x .= "			<tr>";
        $x .= "				<td class='indexpopsubtitle indexpopsubtitlet'>". date("Y-m-d",$ts) ."</td>";
        $x .= "			</tr>";
        $x .= "			</table>";
        $x .= "		</td>";
        $x .= "</tr>";
        
        $x .= "<tr>";
        $x .= "		<td>";
        $x .= "			<table width='100%' border='0' cellspacing='5' cellpadding='0'>";
        
        if(sizeof($row)==0)
        {
               global $i_no_record_exists_msg;

		$x .= "<tr>";
		$x .= "<td bgcolor='#FFFFFF'>";
		$x .= "		<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
		$x .= "		<tr>";
		$x .= "			<td nowrap class='indexeventlink4' align='center'><br />$i_no_record_exists_msg<br /><br /></td>";
		$x .= "		</tr>";
		$x .= "		</table>";
                $x .= "</td>";
        	$x .= "</tr>";
	}
        else
        {
        	for($i=0; $i<sizeof($row); $i++) 
                {
                    $EventID = $row[$i][0];
                    $EventDate = $row[$i][1];
                    $EventType = $row[$i][2];
                    $EventTitle = intranet_wordwrap($row[$i][3],30,"\n",1);
                    switch($EventType){
                         case 0: $i_EventType = $Lang['EventType']['School']; 	$css = 2; break;	# school event  - green
                         case 1: $i_EventType = $Lang['EventType']['Academic']; 	$css = 3; break;	# academic event -  blue 
                         case 2: $i_EventType = $Lang['EventType']['Group']; 	$css = 1; break;		# group event - orange
                         case 3: $i_EventType = $Lang['EventType']['PublicHoliday']; 	$css = 4; break;	# public holiday - red 
                         case 4: $i_EventType = $Lang['EventType']['SchoolHoliday']; 	$css = 5; break;	# school holiday- purple 
                    }

                    $x .= "<tr>";
		$x .= "<td bgcolor='#FFFFFF'>";
		$x .= "		<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
		$x .= "		<tr>";
		$x .= "			<td width='25%' nowrap class='indexeventlink$css' valign='top'>$i_EventType</td>";
                $x .= "			<td><a href='/home/view_event.php?EventID=$EventID' class='indexpoplist'>$EventTitle</a></td>";
		$x .= "		</tr>";
		$x .= "		</table>";
                $x .= "</td>";
        	$x .= "</tr>";
               }
        }
        
        $x .= "			</table>";
        $x .= "		</td>";
        $x .= "</tr>";
        
        $x .= "</table>";

          return $x;
     }

     function displayEventByType($tp,$order=0, $AcademicYearID="")
     {
          global $i_EventType, $tp, $another, $i_EventDate, $i_adminmenu_event, $select_academicYear, $i_EventAcademicYear, $Lang;
          
          switch ($tp)
          {
				case 0: $i_EventType = $Lang['EventType']['School']; 			$css = 2; break;	# school event  - green
				case 1: $i_EventType = $Lang['EventType']['Academic']; 			$css = 3; break;	# academic event -  blue 
				case 2: $i_EventType = $Lang['EventType']['Group']; 			$css = 1; break;	# group event - orange
				case 3: $i_EventType = $Lang['EventType']['PublicHoliday']; 	$css = 4; break;	# public holiday - red 
				case 4: $i_EventType = $Lang['EventType']['SchoolHoliday']; 	$css = 5; break;	# school holiday- purple 
          } 
          
          $row = $this->returnEventByType($tp,$order,$AcademicYearID);
          
        
        $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $x .= "<tr><td class='tabletext'>&nbsp;$i_EventAcademicYear : $select_academicYear</td></tr>";
        $x .= "<tr>";
        $x .= "<td class='indexpopsubtitle indexeventlink$css'><b>$i_EventType</b></td>";
        $x .= "</tr>";
        $x .= "</table></td></tr>";
        $x .= "<tr>";
        $x .= "<td><table width='100%' border='0' cellspacing='4' cellpadding='3'>";
        $x .= "<tr>";
        $x .= "<td  valign='top' nowrap class='tabletop'><a href='javascript:changeOrder();' class='tabletoplink'>$i_EventDate</a></td>";
        $x .= "<td class='tabletop tabletopnolink'>$i_adminmenu_event</td>";
        $x .= "</tr>";
        
	if(sizeof($row) > 0)
        {
        	for($i=0; $i<sizeof($row); $i++) 
                {
                    $EventID = $row[$i][0];
                    $EventDate = $row[$i][1];
                    $EventType = $row[$i][2];
                    $EventTitle = intranet_wordwrap($row[$i][3],30,"\n",1);
                    
                    $x .= "<tr>";
                        $x .= "<td width='30%'  valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>".date("Y-m-d",$EventDate)."</td>";
                        $x .= "<td bgcolor='#FFFFFF' class='tabletext'><a href='view_event.php?EventID=$EventID' class='indexpoplist indexeventlink$css'>$EventTitle</a></td>";
                        $x .= "</tr>";
               }
        } 
        else 
        {
        	global $i_no_record_exists_msg;
        	$x .= "<tr>";
		$x .= "<td align='center' colspan='2' valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>".$i_no_record_exists_msg."</td>";
                $x .= "</tr>";
        }
        
        $x .= "</table>";

          return $x;
     }

     function displayEventByDateRange($start, $end) {
          global $i_Events, $i_From, $startdate, $i_To, $enddate, $linterface, $button_view, $i_EventDate, $i_adminmenu_event;
	
        $btn = $linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");
        $scal = $linterface->GET_CALENDAR("form1", "startdate");
        $ecal = $linterface->GET_CALENDAR("form1", "enddate");
		$row = $this->returnAllEvents($start,$end);
          
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>";
        $x .= "<form name='form1' action='' method='get'>";
        $x .= "<tr>";
        $x .= "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $x .= "<tr>";
        $x .= "<td class='indexpopsubtitle tabletext'>$i_From <input type='text' name='startdate' value='$startdate' class='textboxnum'> $scal $i_To <input type='text' name='enddate' value='$enddate' class='textboxnum'> $ecal $btn</td>";
        $x .= "</tr>";
        $x .= "</table></td></tr>";
        $x .= "<tr>";
        $x .= "<td><table width='100%' border='0' cellspacing='4' cellpadding='3'>";
        $x .= "<tr>";
        
        $x .= "<td  valign='top' nowrap class='tabletop tabletopnolink'>$i_EventDate</a></td>";
        $x .= "<td class='tabletop tabletopnolink'>$i_adminmenu_event</td>";
        $x .= "</tr>";
        
        if(sizeof($row)>0)
        {
        	for($i=0; $i<sizeof($row); $i++) 
                {
                    $EventID = $row[$i][0];
                    $EventDate = $row[$i][1];
                    $EventType = $row[$i][3];
                    
                    $EventTitle = intranet_wordwrap($row[$i][2],30,"\n",1);
                    switch($EventType)
                    {
                    	/* old CSS style */
                        //case 0: $i_EventType = $i_EventTypeSchool; 	$title_css = "indexpopsubtitleschevent";	break;
                        //case 1: $i_EventType = $i_EventTypeAcademic; 	$title_css = "indexpopsubtitleacevent";		break;
                        //case 2: $i_EventType = $i_EventTypeHoliday; 	$title_css = "indexpopsubtitlehday";		break;
                        //case 3: $i_EventType = $i_EventTypeGroup; 	$title_css = "indexpopsubtitlegroupevent";		break;
                        //case 4: $i_EventType = $i_EventTypeGroup; 	$title_css = "indexpopsubtitleschoolholiday";	break;
                    	case 0: $i_EventType = $Lang['EventType']['School']; 			$css = 2; break;	# school event  - green
						case 1: $i_EventType = $Lang['EventType']['Academic']; 			$css = 3; break;	# academic event -  blue 
						case 2: $i_EventType = $Lang['EventType']['Group']; 			$css = 1; break;	# group event - orange
						case 3: $i_EventType = $Lang['EventType']['PublicHoliday']; 	$css = 4; break;	# public holiday - red 
						case 4: $i_EventType = $Lang['EventType']['SchoolHoliday']; 	$css = 5; break;	# school holiday- purple
                    }
                    
                    $x .= "<tr>";
                	$x .= "<td width='30%' valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>$EventDate</td>";
                	$x .= "<td bgcolor='#FFFFFF' class='tabletext'><a href='view_event.php?EventID=$EventID' class='indexpoplist indexeventlink$css'>$EventTitle</a></td>";
                	$x .= "</tr>";
               }
	}
        else 
        {
        	global $i_no_record_exists_msg;
        	$x .= "<tr>";
                $x .= "<td algin='center' colspan='2' valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>$i_no_record_exists_msg</td>";
                $x .= "</tr>";
        }
               
        $x .= "</table></td>";
        $x .= "</tr>";
        $x .= "</form>";
        $x .= "</table>";
          return $x;
     }
     
     // function for eLib advance search form "from -start date- to -end date-
     function displayDateRangeInput($start, $end) {
          global $i_Events, $i_From, $startdate, $i_To, $enddate, $linterface, $button_view, $i_EventDate, $i_adminmenu_event;
	
        $scal = $linterface->GET_CALENDAR("form1", "startdate");
        $ecal = $linterface->GET_CALENDAR("form1", "enddate");
          
	    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $x .= "<tr>";
        $x .= "<td class='tabletext'>$i_From <input type='text' name='startdate' value='$startdate' class='textboxnum'> $scal $i_To <input type='text' name='enddate' value='$enddate' class='textboxnum'> $ecal</td>";
        $x .= "</tr>";
        $x .= "</table>";
        return $x;
     } // end function display Date Range Input

     ###############################################################################
     function getStartOfAcademicYear($ts)
     {
              if (date('m',$ts) >= 9)
              {
                  return mktime(0,0,0,9,1,date('Y',$ts));
              }
              else
              {
                  return mktime(0,0,0,9,1,date('Y',$ts)-1);
              }
     }

     function getEndOfAcademicYear($ts)
     {
              if (date('m',$ts) >= 9)
              {
                  return mktime(0,0,0,8,31,date('Y',$ts)+1);
              }
              else
              {
                  return mktime(0,0,0,8,31,date('Y',$ts));
              }
     }

     ############################################################################
     function displayCalendar_portal()
     {
              global $image_path,$intranet_httppath,$i_general_MonthShortForm;
              $lower_bound = mktime(0,0,0,$this->m,1,$this->y);
              $upper_bound = mktime(0,0,0,$this->m+1,1,$this->y) - 1;
              #$event_list = $this->returnAllEvents($lower_bound,$upper_bound);
              $x = "<SCRIPT language=Javascript>
function new_fe_view_event_by_date(ts){
        newWindow('/home/view_event_by_date.php?".session_name()."=".session_id()."&ts='+ts,1);
}
</SCRIPT>

              ";
              $x .= "<table width=95% border=0 cellspacing=0 cellpadding=0>\n";
              $x .= "<tr>
          <td width=5><img src=\"$image_path/portal/index/cal_topl.gif\" width=5 height=21></td>
          <td align=center class=portal_calendar_cell_top><img src=\"$image_path/portal/index/cal_top.gif\" width=183 height=21></td>
          <td width=7><img src=\"$image_path/portal/index/cal_topr.gif\" width=7 height=21></td>
        </tr>";
              $x .= "<tr>\n<td class=portal_calendar_cell_left><img src=\"$image_path/spacer.gif\" width=1 height=165></td>";
              $x .= "<td class=portal_calendar_month_name>
                <table width=100% border=0 cellpadding=2 cellspacing=0>
              <tr align=right>
                <td colspan=7 class=24blu>".$i_general_MonthShortForm[intval($this->m)-1]."</td>
              </tr>\n";
              $x .= $this->displayCalendarArray_portal($this->m, $this->y);
              $x .= "</table>";
              $x .= "</td>
          <td class=portal_calendar_cell_right><img src=\"$image_path/spacer.gif\" width=1 height=165></td>
        </tr>
        <tr>
          <td><img src=\"$image_path/portal/index/cal_btml.gif\" width=5 height=11></td>
          <td class=portal_calendar_cell_bottom><img src=\"$image_path/spacer.gif\" width=1 height=11></td>
          <td><img src=\"$image_path/portal/index/cal_btmr.gif\" width=7 height=11></td>
        </tr>
      </table>";

              #$x .= $this->displayCalendarArray(date("m",mktime(0,0,0,$this->m+1,1,$this->y)), date("Y",mktime(0,0,0,$this->m+1,1,$this->y)),2);
              return $x;
     }

     function displayEventList_portlet($ts="")
     {
              global $image_path;
              if ($ts == "")
              {
                  $ts = time();
              }
              $day_ts = mktime(0,0,0,date('m',$ts),date('d',$ts),date('Y',$ts));
              $events = $this->returnAllEvents($day_ts,$day_ts);
              $x = "<table width=100% border=0 cellspacing=0 cellpadding=2>\n";

              for ($i=0; $i<sizeof($events); $i++)
              {
                   list($id,$date,$title,$recordtype) = $events[$i];
                   $link = "<a href=javascript:new_fe_view_event($id)>$title</a>";
                   $x .= "<tr>\n";
                   $x .= "<td width=11><img src=\"$image_path/portal/index/bullet_cross.gif\" width=7 height=11 align=absmiddle></td>\n";
                   $x .= "<td>$link</td>\n";
                   $x .= "</tr>\n";
              }
              $x .= "</table>\n";
              return $x;

     }
     # Not yet make new deployment
     function displayCalendarArray_portal($month, $year,$order=""){
          global $i_frontpage_day,$i_EventList,$image_path;
          $row = $this->returnCalendarArray($month, $year);
          $a = new libcycle();
          if ($a->CycleID != 0)
          {
              for ($i=0; $i<sizeof($row); $i++)
              {
                   if ($row[$i] != "")
                       $timestamps[] = $row[$i];
                       //$dates[] = date('Y-m-d',$row[$i]);
              }
              $cycles = $a->getCycleDaysFromTimestamps($timestamps);
              for ($i=0; $i<sizeof($timestamps); $i++)
              {
                   $cycles_array[$timestamps[$i]] = $cycles[$i];
              }
              if ($a->CycleID == 1 || $a->CycleID == 2 || $a->CycleID == 3 || $a->CycleID == 10)
                  $cycle_type = 1;
              else if ($a->CycleID == 4 || $a->CycleID == 5 || $a->CycleID == 6 || $a->CycleID == 11)
                   $cycle_type = 2;
              else if ($a->CycleID == 7 || $a->CycleID == 8 || $a->CycleID == 9 || $a->CycleID == 12)
                   $cycle_type = 3;
              else $cycle_type = 0;
              global ${"i_SimpleDayType".$cycle_type};
              $DayName = ${"i_SimpleDayType".$cycle_type};
          }
          else
          {
              $cycles = array();
          }

          #$top_image = ($order==""? "cal_top.gif":"cal_top2.gif");
          /*
          if ($order == "")
          {
          $top_image = "top.gif";
          $x .=  "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
          $x .= "<tr><td><img src=/images/index/$top_image></td></tr></table>\n";
          }
          */
          #$x .= "<table class=cal_head width=203 border=0 cellspacing=0 cellpadding=0 background=/images/index/cal_tab.gif height=17>\n";
          #$x .= "<tr><td width=20>&nbsp;</td>";
          /*
          $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
          $x .= "<tr>";
          $td_style = "\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-weight: bold; text-align: center; vertical-align: bottom; background-image: url(/images/index/tab.gif)\"";
          if ($order == "")
          {
              $x .= "<td width=110 STYLE=$td_style>";  # 83
              // # 84
              $x .= "$year/$month</td><td width=84 align=right><a href=javascript:showEventList()>$i_EventList</a></td>
              <td width=9><img src=$image_path/index/green_lh.gif></td></tr></table>\n";   # 20
          }
          else
          {
              $x .= "<td width=110 STYLE=$td_style>";  # 83
              // # 84
              $x .= "$year/$month</td><td width=84 align=right bgcolor=#8CCB21>&nbsp;</td>
              <td width=9><img src=$image_path/index/green_lh.gif></td></tr></table>\n";   # 20

              #$x .= "<td width=183 class=h1>";
              #$x .= "$year/$month</td></tr></table>\n";

          }
          */
          #$x .= "<table background=/images/index/cal_tbbg.gif width=203 border=0 cellspacing=0 cellpadding=0>\n";
/*
          $x .= "<table background=/images/index/cellbg_white.gif width=100% border=0 cellspacing=0 cellpadding=0>\n";
#          $x .= "<tr><td align=center><table width=175 border=0 cellspacing=0 cellpadding=2 class=body>\n";
          $x .= "<tr><td align=center><table width=90% border=1 bordercolorlight=#B3B692 bordercolordark=#F8FBD6 cellspacing=0 cellpadding=0 class=body>\n";
          $x .= "<tr align=center valign=middle>\n";
          */
          $x .= "<tr align=center valign=middle class=12darkblu>";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[0]."</td>\n";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[1]."</td>\n";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[2]."</td>\n";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[3]."</td>\n";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[4]."</td>\n";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[5]."</td>\n";
          $x .= "<td class=12darkblu align=center>".$i_frontpage_day[6]."</td>\n";
          $x .= "</tr>\n";

          for($i=0; $i<sizeof($row); $i++){
               $x .= ($i%7==0) ? "</tr><tr><td colspan=7><img src=\"$image_path/spacer.gif\" width=1 height=1></td></tr>
               <tr align=center valign=middle class=cal11blu>\n" : "";
               $cycle = $cycles_array[$row[$i]];
               $display = ($cycle === -1 || $cycle === "")? "":$DayName[$cycle];
               $x .= $this->displayCalendarEvent_portlet($row[$i],$display);
          }
          $x .= "</tr>";
          $x .= "<tr align=center valign=middle>
                <td colspan=7 class=portal_calendar_year_month>";
          $x .= $this->displayPrevMonth_portal();
          $x .= $this->y ."-".$this->m;
          $x .= $this->displayNextMonth_portal();
          $x .= "</tr>";

          #"</table></td></tr></table>\n";
          #$x .= "<div align=center>";
          #$x .= "</div>\n";
          /*
          $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
          #$x .= "<tr><td><img src=/images/index/cal_bottom.gif></td></tr></table>\n";
          $x .= "<tr><td><img src=/images/index/bottom.gif></td></tr></table>\n";
*/
/*

          $x .= "<tr><td align=right colspan=7><b>".date("Y/m", mktime(0,0,0,$month,1,$year))."</b></td></tr>\n";
          $x .= "</table>\n";
          $x .= "</td></tr>\n";
          $x .= "<tr><td><img src=/images/frontpage/".(($this->IsWhite) ? "cal_white_bg_b.jpg" : "cal_bg_b.jpg")." border=0 width=181 height=11></td></tr>\n";
          $x .= "</table>\n";
*/
          return $x;
     }
     # Not yet make new deployment
     function displayCalendarEvent_portlet($ts, $cycle){
	     
		if(!isset($this->EventArray))
			$this->EventArray = $this->returnEventRecord();
		if(!isset($this->AllEventArray))
			$this->AllEventArray = $this->returnAllEventRecord();
				     
          if($ts==""){
               $x = "<td align=center><br></td>";
          }else{
          /*
               if($this->IsSchoolEvent($ts)) $ClassType = "class=cal_bg_school";
               if($this->IsAcademicEvent($ts)) $ClassType = "class=cal_bg_academic";
               if($this->IsAcademicEvent($ts) && $this->IsSchoolEvent($ts)) $ClassType = "class=cal_bg_both";
               */
               $Event  = ($this->IsHolidayEvent($ts)) ? "<font color=red>".date("j",$ts)."</font>" : date("j",$ts);
               $Event .= (false && $this->IsGroupEvent($ts)) ? "*" : "";

               #$Event .= ($cycle==""? "":"<!---|$cycle--->");
//             $Event = ($this->IsGroupEvent($ts)) ? "<i><b>$Event</b></i>":$Event;
               if ($cycle=="") $cycle = "&nbsp;";
               $x  = "<td $ClassType align=center>";
               if($this->IsResource){
                    $x .= "<a href=?ts=$ts>".$Event."</a>";
               }else{
                    $Event = ($this->IsEvent($ts)) ? "<a STYLE=\"TEXT-DECORATION: underline\" href=javascript:new_fe_view_event_by_date($ts)>".$Event."</a>" : $Event;

                    $x .= "<table width=100% border=0 cellpadding=0 cellspacing=0 class=body>
                    <tr>
                      <td class=cal11blu align=center>$Event</td>
                    </tr>\n";
                    if ($cycle != "&nbsp;")
                    {
                         $x .= "<tr><td><I><font color=#0046A0>$cycle</font></I></td></tr>\n";
                    }
                    $x .= "</table>";

               }
               $x .= "</td>\n";
          }
          return $x;
     }
     function displayPrevMonth_portal(){
          global $intranet_session_language;
          $x = "<a href=?v=".$this->view."&ts=".$this->go(-1,"m")."><img src=".$this->image_path."/portal/index/btn_pre.gif border=0 align=absmiddle></a>\n";
          return $x;
     }

     function displayNextMonth_portal(){
          global $intranet_session_language;
          $x = "<a href=?v=".$this->view."&ts=".$this->go(+1,"m")."><img src=".$this->image_path."/portal/index/btn_next.gif border=0 align=absmiddle></a>\n";
          return $x;
     }
     
     private function Get_Preload_Result($ArrayKey, $FunctionArgumentArr) {
		$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
    	$PreloadInfoArrExist = $this->Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr);
    	
    	$returnAry = array();
    	if ($PreloadInfoArrExist)
    	{
            $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
    		eval('$returnAry = '.$PreloadInfoArrVariableStr.';');
    		return $returnAry;
    	}
    	else {
    		return false;
    	}
	}
	
	private function Set_Preload_Result($ArrayKey, $FunctionArgumentArr, $ResultArr) {
		$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
        $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
		eval($PreloadInfoArrVariableStr.' = $ResultArr;');
	}
	
	private function Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr) {
		$PreloadInfoArrVariableStr = '$this->PreloadInfoArr[\''.$ArrayKey.'\']';
    	foreach( (array)$FunctionArgumentArr as  $arg_key => $arg_val)
    	{
    		if (is_array($arg_val)) {
    			$key_name = implode(',', $arg_val);
    		}
    		else {
    			$key_name = trim($arg_val);
    		}
    		$PreloadInfoArrVariableStr .= "['".addslashes($key_name)."']";
    	}
    	
    	return $PreloadInfoArrVariableStr;
	}
	
	private function Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr) {
		$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
        $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
		
		$PreloadInfoArrExist = false;
    	eval('$PreloadInfoArrExist = isset('.$PreloadInfoArrVariableStr.');');
    	
    	return $PreloadInfoArrExist;
	}
	
	function returnAllEventRecordByTimestampRange($start_ts,$end_ts,$ClassGroupID='')
     {
     	global $sys_custom;
     	$PreloadArrKey = 'returnAllEventRecordByTimestampRange';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
	    if($isKIS && $ClassGroupID != '') {
	    	if(is_array($ClassGroupID)) {
	    		$ClassGroupIdCsv = implode(",",$ClassGroupID);
	    	}else{
	    		$ClassGroupIdCsv = $ClassGroupID;
	    	}
	    	//if($_SESSION['UserType'] != USERTYPE_STAFF){
	    		$classGroupIdCond = " AND (a.ClassGroupID IN ($ClassGroupIdCsv) OR a.ClassGroupID IS NULL OR a.ClassGroupID='') ";
	    	//}else{
	    	//	$classGroupIdCond = " AND a.ClassGroupID IN ($ClassGroupIdCsv) ";
	    	//}
	    }	
	    if($_SESSION["platform"]=="KIS"){
	    	$canGetAllEvents = ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $_SESSION['UserType'] == USERTYPE_STAFF);    
	    }else{
	    	$canGetAllEvents = true;
	    }
	    
	    if($canGetAllEvents) {

          ## Get All Group Event ##
          $sql1 = "SELECT DISTINCT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b WHERE b.EventID = a.EventID AND a.RecordStatus = '1' AND (UNIX_TIMESTAMP(a.EventDate) BETWEEN '$start_ts' AND '$end_ts') $classGroupIdCond";
         
          ## Get All Event except Group Event ##
          $sql2 = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT as a WHERE a.RecordStatus = '1' AND a.RecordType <>'2' AND (UNIX_TIMESTAMP(a.EventDate) BETWEEN '$start_ts' AND '$end_ts') $classGroupIdCond";
          
          ## Get Any Missing Group Event (Only Have Record In INTRANET_EVENT) ##
          $sql3 = "SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = '1' AND (UNIX_TIMESTAMP(a.EventDate) BETWEEN '$start_ts' AND '$end_ts') $classGroupIdCond";
          
          $sql = "(".$sql1.") UNION (".$sql2.") UNION (".$sql3.")";
	    }else{
	      ## Get Related Group Event ##
          $sql = "SELECT DISTINCT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT AS a INNER JOIN INTRANET_GROUPEVENT AS b ON a.EventID=b.EventID INNER JOIN INTRANET_USERGROUP AS c ON c.GroupID=b.GroupID WHERE a.RecordStatus = '1' AND c.UserID = ".$_SESSION['UserID']." AND (UNIX_TIMESTAMP(a.EventDate) BETWEEN '$start_ts' AND '$end_ts') $classGroupIdCond";
          
          ## Get All Event except Group Event ##
          $sql .= "UNION (SELECT a.EventID, UNIX_TIMESTAMP(a.EventDate), a.RecordType, a.Title FROM INTRANET_EVENT as a WHERE a.RecordStatus = '1' AND a.RecordType <>'2' AND (UNIX_TIMESTAMP(a.EventDate) BETWEEN '$start_ts' AND '$end_ts') $classGroupIdCond)";
	    }
          $ReturnArr = $this->returnArray($sql,4);
          
          $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
		  return $ReturnArr;
     }
     
     function initAllEventRecordByTimestampRange($start_ts, $end_ts, $ClassGroupID='')
     {
     	$this->AllEventArray = $this->returnAllEventRecordByTimestampRange($start_ts, $end_ts, $ClassGroupID);
     }
}
?>