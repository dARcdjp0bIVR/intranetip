<?php
// Modifing by 

//
/****************************************************
 * Modification Log
 * 2020-06-09 (Ray)
 *  - modify MENU_TAGS() - add $ip20TopMenu['eAttendanceLesson']
 * 
 * 2020-03-26 (Cameron)
 *  - add function getCustomizedModuleMenu() - consolidate and move it from home_header.php
 *  
 * 2019-11-01 (Cameron)
 *  - fix: add space (&nbsp;) after "Powered by" for case $sys_custom["showWithBL"] in interface_html()
 * 
 * 2019-05-02 (Carlos)
 *  - apply intranet_htmlspecialchars() to checkbox, radio button, date picker's value attribute.
 * 
 * 2019-02-25
 *  - modify getReferenceThickBox() - css style
 * 
 * 2019-01-08 (Isaac)
 *  - modify MENU_TAGS(), add $Lang['Header']['Menu']['eBooking'] to show menu in PowerClass
 * 
 * 2018-07-06 (Paul)
 *  - modify Get_Radio_Button() & Get_Checkbox(), add option for handling multiline label
 * 
 * 2017-11-28 (Cameron)
 * 	- modify MENU_TAGS(), add $i_InventorySystem['eInventory'] to show menu in PowerClass
 * 
 * 2017-11-13 (Carlos)
 *  - Added more icon types to Get_DBTable_Action_Button_IP25().
 * 
 * 2017-08-01 (Cameron)
 * 	- modify MENU_TAGS(), add $Lang['libms']['library'] to show menu in PowerClass
 * 
 * 2017-06-16 (Paul)
 * 	- Modified TITLE_TAGS(), Added MENU_TAGS() for PowerClass display
 * 2017-05-26 (Anna)
 * 	-Modified getReferenceThickBox() - added overflow div
 * 
 * 2017-04-05 (Villa)
 *  - Add Include_TableExportadd tableExport
 *
 *  2016-12-15 (Villa)
 *  - modified function Get_Checkbox_Table set width to 100%
 *  
 * 	2016-09-06 (Villa)
 * 	- added Include_HighChart_JS()
 * 	- modified function Get_Content_Tool_By_Array_v30()-adding id
 * 
 *	2016-07-22 (Kenneth)
 * 	- modified Get_Time_Selection() - add param for adding name as array
 *  2016-07-04 (Omas)
 *  - modified Include_CopyPaste_JS_CSS() - add version param apply new copypaste plugin done by evan
 * 
 *  2016-05-09 (Kenneth) , (Omas)
 *  - modified Get_Time_Selection(), add param to hide seconds' selector , add param $intervalArr to set interval
 * 
 *  2016-05-06 (Kenneth)
 *  - modified GET_WARNING_TABLE(), add param to edit warning heading wordings
 * 
 * 	2016-03-11 (Kennth)
 * 	- modified getReferenceThickBox(), enable table more than 2 column
 * 
 * 	2016-03-11 (Kennth)
 * 	- add getReferenceThickBox(), return table of remarks thickbox
 * 
 *  2015-10-29 (Omas)
 * 	- Added GET_SINGLETEXTBOX_GROUP()
 * 
 *  2015-10-07 Omas
 * 	- modified Get_Import_Page_Column_Display() add 3, 4
 * 
 * 	2015-07-21 Cameron
 * 	- Add parameter $CheckType to Modify GET_SELECTION_BOX()
 * 
 * 	2015-07-15	Pun
 * 	- Modified LAYOUT_START() -> changed call template "FUNC_XXXXX" can override by subclass
 * 
 * 	2014-11-04	Omas
 * 	- Modified Get_Import_Page_Column_Display() -> field will now naming by letter instead of number  
 * 
 * 	2014-07-24 Pun
 * 	- Added generateUserSelection() for generate selection box
 * 
 * 	2014-06-13 YatWoon	 - ip.2.5.5.8.1
 * 	- modified GET_BTN(), add disable parameter
 * 
 * 	2013-04-08 Carlos
 *  - added Include_Plupload_JS_CSS()
 * 
 *	2013-03-01 YatWoon
 *  - modified TITLE_TAGS(), set left td with witdh 100% if right td is exists [Case#2013-0301-0929-05156]
 *
 *  2013-01-30 Carlos
 *  - added Include_TableReplySlip_JS_CSS()
 * 
 * 	2012-10-25 Rita
 * 	- copy Get_Show_Option_Link(), Get_Hide_Option_Link() from EJ
 * 
 * 	2012-10-18 Ivan
 * 	- added Get_File_Type_Icon()
 * 
 *	2012-08-10 Ivan
 *	- modified Get_Content_Tool_v30(), support adding "id" in the option group array for each sub-button
 * 
 *  2012-07-03 Fai
 *  - modifiy Get_Display_From_TextArea() , generate the display that input from a Text Area , support indention , new line, convert HTML tab 
 *
 *  2012-06-25 Henry Chow
 *  - modifiy GET_DATE_PICKER(), add parameter $cssClass
 * 
 *  2012-03-26 Marcus
 *  - Added Get_Time_Selection
 * 
 *  2012-01-27 Marcus
 *  - Added GET_TEXTBOX_NAME
 * 
 *  2011-12-14 Ivan
 *  - modified Get_DBTable_Action_Button_IP25(), added OtherTags in the array to control the display of the button
 *	2011-11-23 Marcus
 *	- added Include_OverlayTextarea_JS_CSS() 
 *
 *	2011-11-23 Marcus
 *	- modified GET_TEXTAREA(), add CommentMaxLength 
 *
 *	2011-10-27 (Henry Chow)
 *	- revised GET_LNK_GENERATE(), using IP25 standard
 * 
 *	2011-09-15 Marcus
 *	- added Get_Attachment_Icon() 
 *  
 *	2011-09-07 Marcus
 *	- modified Get_MultipleSelection_And_SelectAll_Div(), add SpanID 
 *
 *	2011-09-07 YatWoon
 *	- modified MODULE_MENU(), implement eEnrolment Lite, disabled link and just display title  
 *
 *  2011-09-05 Ivan
 * 	- modified function Get_Content_Tool_v30(), added class "generate" in the function
 * 
 * 	2011-09-01 Ivan
 * 	- added function Get_Approved_Image(), Get_Rejected_Image() and Get_Pending_Image()
 * 
 *	2011-08-29 Henry Chow
 *	- modified GET_SELECTION_BOX() & GET_SELECTION_BOX_WITH_OPTGROUP(), check "ParSelected" with array value
 *
 *	2011-08-08 YatWoon
 *	- modified GET_DATE_PICKER(), support empty field
 *
 *  2011-07-29 Marcus
 * 	- modified Get_Content_Tool_v30, support option layer for all type of btn
 * 
 *  2011-07-26 Marcus
 * 	- added Get_Upload_Attachment_UI, get standardize attachment upload ui 
 * 
 *  2011-07-14 Ivan
 * 	- updated LAYOUT_START() to add $html_ies_survey_body_class for IES survey body class
 * 
 *  2011-06-29	Ivan
 * 	- updated Get_Warning_Message_Box() to support content as an array to display in point form format
 * 
 *	2011-06-04	YatWoon
 *	- update InactiveStudentField(), GroupAdminField(), add "withoutClass" parameter for print version
 *
 *	2011-05-20	Yuen
 *	- modified GEN_TOP_MENU_li() to state object ID and do not select any item if cache mode is in used
 *
 *	2011-05-19	Marcus
 *	- add RequiredSymbol(), add asterisk for required field
 *
 *	2011-04-11	Henry Chow
 *	- update GET_SYS_MSG(), add lang $i_con_msg_session_not_available2
 *
 *	2011-04-01	YatWoon
 *	- update GET_STEPS_IP25(), the css "....3s" should according to the number of steps
 *
 * 	2011-03-31 Carlos
 * 	- modified GET_LNK_UPLOAD() to use IP25 standard
 * 
 * 	2011-03-15 Ivan
 * 	- added function Get_CSV_Sample_Download_Link() to generate the csv download link (esp. for import sample download)
 * 
 *	2011-02-24 Ivan
 *	- added function Get_DBTable_Action_Button_IP25() to generate the top-right buttons above the DB Table
 *
 *	2011-02-17 Marcus
 *	- modified GET_NAVIGATION2_IP25(), add param customClass 
 *	- modified MODULE_TITLE(), MODULE_MENU(), add special handling of MODULE_OBJ['CustomLogo']
 *
 *	2011-01-24 Marcus
 *	- GET_RECORD_INDICATION_IP25(), 
 *
 *	2011-01-21 Marcus
 *  - added Include_SuperTable_JS_CSS()
 * 
 *	2011-01-19 Marcus
 *	- added MultiSelectionRemark()
 *
 *	2011-01-17 Marcus
 *	- update Get_Date_Picker(), add param $SkipIncludeJS to skip including js library and css. 
 *	- added Include_DatePicker_JS_CSS()
 *	- update Get_Radio_Button add param $isDisabled
 *
 *	2011-01-04 YatWoon
 *	- update GET_LNK_EXPORT_IP25(), add button text parameter
 *
 *	2010-12-30 (YatWoon)
 *	- update Get_Multiple_Selection_Box(), add $selected_ary variable
 *
 *	2010-12-29 (YatWoon)
 *	- update Get_Multiple_Selection_Box(), add $fix_width variable that allow display multiple with dynamic width
 *
 *	2010-12-20 (Marcus)
 *		- added Include_AutoComplete_JS_CSS(), load jQuery AutoComplete Lib
 *
 *	2010-12-16 YatWoon
 *		- Added GET_LNK_PRINT_IP25(), GET_LNK_EXPORT_IP25()
 *
 *	2010-12-13 (Marcus)
 *		- Added Get_Form_Warning_Msg(), get div of warning msg for form validation
 *		- modified Get_Thickbox_Warning_Msg_Div, add class tag.
 *
 *	2010-12-10 (Marcus)
 *		- Added GET_NAVIGATION2_IP25()
 *
 *	2010-11-24 (Marcus)
 *		- Update GET_NAVIGATION_IP25(), add spacer after Navigation to avoid ui problem
 *
 *	2010-11-18 (Fai)
 *		- Update function TITLE_TAGS() , replace the for loop with foreach to loop the $TAGS_OBJ
 *
 *	2010-11-01 (Henry Chow)
 *		- added Include_JS_CSS(), load Ajax files
 *
 *	2010-10-08 (Ivan)
 *		- modified GET_WARNING_TABLE(), added para $DeleteItemName and $DeleteButtonName for standardizing the generation of the warning message
 *
 *	2010-10-07 (Ronald)
 *		- modified GET_WARNING_TABLE(), remove "align='center'" for the warning message.
 *
 *	2010-10-06 (Ronald)
 *		- modified GET_ACTION_BTN(), add one more Param - $ParClass
 *		- changed the flow of deciding which class should be used
 * 
 *	2010-09-27 (Ronald)
 *		- modified GET_LNK_PRINT(), now using IP25 display standard
 *
 *  2010-09-16 (Kenneth Chung)
 *		- update GET_LNK_EXPORT(),GET_LNK_ADD(),GET_LNK_NEW() 
 *			add the class/ onclick and other member parameter to the link on IP25 standard
 *
 *  2010-09-03 (Ivan)
 *		- update GET_LNK_IMPORT(), GET_LNK_EXPORT() and GET_LNK_EMAIL() to change to IP25 standard
 *
 *  2010-08-31 (Marcus)
 * 		- modified GET_LNK_UPLOAD(), add Param $ParOnClick 
 * 
 *  2010-08-30 (Ivan)
 * 		- modified Get_Content_Tool_v30() to add import & new options layer logic
 * 
 * 	2010-08-27 (Ivan)
 * 		- added GET_TEXTBOX_NUMBER() for IP25 Standard
 * 
 * 	2010-08-20 (Marcus)
 * 		- added Spacer()
 * 
 *	2010-08-20 (YatWoon)
 *		- update GET_LNK_ADD(), GET_LNK_EDIT(), GET_LNK_REMOVE(),GET_LNK_NEW(), change to IP25 standard
 *		- add GET_SUBTAGS()
 *
 * 2010-08-18 (Ivan)
 *		- for IP25 layout standard
 *			- added InactiveStudentField() and GroupAdminField()
 *
 * 2010-08-18 (Marcus)
 *		- Modified GET_TEXTAREA
 *			- add param $class to add extra class
 *
 *  * 2010-08-16 (Marcus)
 *		- added Get_Content_Tool_v30($type, $href="#", $text="", $options=array(), $other="")
 *			- $type = new/edit/import/export....
 *			- options = arrays of sub option for export, arrays of array($href,$option_text)
 *		- added GET_IMPORT_STEPS($CurrStep)
 *			- $CurrStep start with 1,  1 for Select CSV file,2 for Confirmation, 3 for Imported Result
 * 
 * 2010-08-12 (Ivan)
 *		- for IP25 layout standard
 *			- added ReferenceField()
 *
 * 2010-08-10 (Henry Chow)
 * 		- for IP25 layout standard
 * 			- GET_STEPS_IP25()
 * 
 * 2010-07-29 (YatWoon)
 *		- for IP25 layout standard
 *			- add GET_NAVIGATION_IP25()
 *			- update GET_ACTION_BTN()
 *			- add MandatoryField()
  * 
 * 2010-07-29 (Ivan)
 * 		- added function Get_Apply_All_Icon() to get the "apply to all" icon
 * 
 * 2010-07-23 (Thomas):
 * 		- added function GET_CONTENT_TOP_BTN to Generate Button in Content Top Tool
 * 
 * 2010-07-08 (Kenneth chung):
 * 		- modified function Get_Date_Picker() 
 			-	Add Parameter $ID=""
 					If ID=="" then ID = Name
 			- Allow user to define multipule date field with same name but different ID
 *
 * 
 * 2010-06-26 (fai):
 *		- new function TITLE_TAGS_2() to display title
 * 2010-06-24 (Marcus):
 * 		- modified function Get_Search_Box_Div, add option $Others
 * 
 * 2010-03-19 (Ivan):
 * 		- added function Get_Ajax_Loading_Image() to get the ajax loading icon
 *
 * 2010-04-21 (Kenneth chung):
 * 		- modified function Get_Date_Picker() 
 			-	Add Parameter $OnDatePickSelectedFunction 
 					$OnDatePickSelectedFunction - js function to be called while a date is picked from date picker
 *
 *
 * 2010-04-08 (Kenneth chung):
 * 		- modified function Get_Date_Picker() 
 			-	Add default checking on date format for onkeyup event 
 			- add parameter $ExtWarningLayerID="",$ExtWarningLayerContainer=""
 					$ExtWarningLayerID - specify the warning layer to show the invalid date format warning message
 					$ExtWarningLayerContainer - specify the container of warning layer (dummy tr that only contain the warning layer), to trigger the display property
 			- If none of both added parameter is provided, a default warning layer will be placed at right hand side of the date picker field
 *
 * 2010-03-19 (Ivan):
 * 		- modified function Get_Thickbox_Warning_Msg_Div(), added $ParMsg to set the default warning msg of the div
 * 
 * 2010-02-25 (Marcus):
 *		- Added Get_IFrame for gamma mail (Copy from ESF Userinterface.php)
 *
 * 2010-01-13 (Kenneth chung):
 *		- At function Get_Date_Picker, add parameter $MaskFunction to let user define functions to mask the date cell
 *
 * 2010-01-13 (Marcus):
 *		- add function GET_SELECTION_BOX_WITH_OPTGROUP($ParData, $ParTags, $ParDefault, $ParSelected="")
 *		- $ParData[$ctr] = array($ID, $Name, $OptGroupLabel)
 *
 * 2010-01-13 (Henry):
 *    - function GET_SYS_MSG (added error message $Lang['eDiscipline']['PIC_Not_Avaliable'])
 *
 * 20091230 (Henry):
 *    - function GET_SYS_MSG (added error message $i_con_msg_duplicate_item)
 *
 * 20091211 (Ivan):
 *    - function Get_Checkbox (added parameter $Disabled)
 * ***************************************************/


$contentLeftPos = 150;

class interface_html{

	var $TemplateContent;
	var $TemplateFile;
	var $IsHeaderCaching;


	/*
	* Initialize
	*/
	function interface_html($ParTemplateFile = "default.html"){
		global $intranet_root, $LAYOUT_SKIN, $MODULE_OBJ, $sys_custom;
		$this->TemplateFile = $ParTemplateFile;
		$TemplateFile = $ParTemplateFile;
		$this->TemplateContent = file_get_contents($intranet_root."/templates/". $LAYOUT_SKIN ."/layout/".$TemplateFile);
		if ($sys_custom["showWithBL"]) {
		    if ($sys_custom["FooterWithoutLink"]) {
		        $linkHtml = 'BroadLearning';
		    }
		    else {
		        $linkHtml = '<a href="http://www.eclass.com.hk" class="BLlink" target="_blank">BroadLearning</a>';
		    }
		    $poweredby = '<tr><td class="footer"><div class="BLlink"><span>'.$linkHtml.'</span></div></td></tr>';
		} else {
		    $poweredby = '<tr><td class="footer"><span><a href="http://www.eclass.com.hk" target="_blank"><img src="/images/logo_eclass_footer.png" width="59" height="21" border="0" align="absmiddle"></a></span></td></tr>';
		}
		$this->TemplateContent = str_replace("{{HTML_FOOTER_POWEREDBY}}", $poweredby, $this->TemplateContent);
	}

	function LAYOUT_START($ReturnMessage="") {
		global $PATH_WRT_ROOT, $intranet_root, $LAYOUT_SKIN, $intranet_session_language;
		global $FullMenuArr, $CurrentPage, $header_onload_js, $body_tags, $UserID, $CurrentPageArr, $home_header_no_EmulateIE7;
		global $setTdContentHeight100;
		global $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
		global $IP20_NotUseTopBlueBar, $IP20_NotUseTopGreyBar;
		global $Lang, $plugin, $sys_custom, $ip20_loading, $i_eu_current_ver,$lslp_httppath, $html_ies_survey_body_class;
		# For customarization
		global $i_ChuenYuen_Award_Scheme;		# Chuen Yuen Award Scheme
		$TemplateContent = $this->TemplateContent;
		
		$PosBody = strpos($TemplateContent,"{{{HTML_BODY}}}");
		$StartPos = 0;
		$EndPos = $PosBody;
		$Delimitor1 = "{{{";
		$Delimitor2 = "}}}";

		$PrevPos = 0;
		$PrevMatchStartPos = 0;
		$PrevMatchEndPos = 0;

		while (true)
		{
	   		$MatchStartPos = strpos($TemplateContent,$Delimitor1,$PrevPos);
	   		$MatchEndPos = strpos($TemplateContent,$Delimitor2,$MatchStartPos);
	   		
	   		if (($MatchStartPos===false) || ($MatchEndPos===false))
	   		{
	   			break;
	   		}
	   		if ($MatchStartPos >= $EndPos)
	   		{
	   			break;
	   		}
	   		
	   		$PrevMatchStartPos = $MatchStartPos;
	   		$PrevMatchEndPos = $MatchEndPos;

	   		$TextContent = substr($TemplateContent,$PrevPos,$MatchStartPos-$PrevPos);
	   		echo str_replace("web_root_path/", $PATH_WRT_ROOT, $TextContent);

	   		$TemplateName = substr($TemplateContent,$MatchStartPos+3,$MatchEndPos-$MatchStartPos-3);
	   		$MiddlePos = strpos($TemplateName,"_");
	   		$Prefix = substr($TemplateName,0,$MiddlePos);
	   		$Suffix = substr($TemplateName,$MiddlePos+1);


	   		if ($Prefix == "FUNC")
	   		{
	   			call_user_func(array(get_class($this), $Suffix));
	   		}
	   		else if ($Prefix == "TEMPLATE")
	   		{
	   			include($intranet_root."/templates/". $LAYOUT_SKIN ."/layout/".strtolower($Suffix).".php");
	   		}
	   		$PrevPos = $MatchEndPos+3;
		}

   		if ($PrevPos<$EndPos)
   		{
	   		$TextContent = substr($TemplateContent,$PrevPos,$EndPos-$PrevPos);
	   		echo str_replace("web_root_path/", $PATH_WRT_ROOT, $TextContent);
   		}
   		
   		if (trim($ReturnMessage) != "") {
			echo '<script>';
			echo 'Get_Return_Message("'.$ReturnMessage.'");';
			echo '</script>';
		}
		return;
	}

	function LAYOUT_STOP() {
		global $PATH_WRT_ROOT, $intranet_root, $LAYOUT_SKIN, $intranet_session_language, $image_path;
		global $MODULE_OBJ;

		$TemplateContent = $this->TemplateContent;

		$PosBody = strpos($TemplateContent,"{{{HTML_BODY}}}");
		$StartPos = $PosBody+strlen('{{{HTML_BODY}}}');
		$EndPos = strlen($TemplateContent);
		$Delimitor1 = "{{{";
		$Delimitor2 = "}}}";

		$PrevPos = $StartPos;
		$PrevMatchStartPos = $StartPos;
		$PrevMatchEndPos = $StartPos;

		while (true)
		{
	   		$MatchStartPos = strpos($TemplateContent,$Delimitor1,$PrevPos);
	   		$MatchEndPos = strpos($TemplateContent,$Delimitor2,$MatchStartPos);

	   		if (($MatchStartPos===false) || ($MatchEndPos===false))
	   		{
		   		break;
	   		}
	   		if ($MatchStartPos >= $EndPos)
	   		{
		   		break;
	   		}

	   		$PrevMatchStartPos = $MatchStartPos;
	   		$PrevMatchEndPos = $MatchEndPos;

	   		$TextContent = substr($TemplateContent,$PrevPos,$MatchStartPos-$PrevPos);
	   		echo str_replace("web_root_path/", $PATH_WRT_ROOT, $TextContent);

	   		$TemplateName = substr($TemplateContent,$MatchStartPos+3,$MatchEndPos-$MatchStartPos-3);
	   		$MiddlePos = strpos($TemplateName,"_");
	   		$Prefix = substr($TemplateName,0,$MiddlePos);
	   		$Suffix = substr($TemplateName,$MiddlePos+1);
			
	   		if ($Prefix == "FUNC")
	   		{
	   			call_user_func(array("interface_html", $Suffix));
	   			
	   		} else if ($Prefix == "TEMPLATE")
	   		{
		   		include($intranet_root."/templates/". $LAYOUT_SKIN ."/layout/".strtolower($Suffix).".php");
	   		}
			
	   		$PrevPos = $MatchEndPos+3;
		}

   		if ($PrevPos<$EndPos)
   		{
	   		$TextContent = substr($TemplateContent,$PrevPos,$EndPos-$PrevPos);
	   		echo str_replace("web_root_path/", $PATH_WRT_ROOT, $TextContent);
   		}

   		// reposition the content when there's no menu
		if (sizeof($MODULE_OBJ) == 1) {
			print "<script type=\"text/javascript\">";
			print "if (document.getElementById('titlebar') != undefined){ ";
			print "document.getElementById('titlebar').style.paddingLeft = 30 + \"px\";";
			print "}";
			print "if (document.getElementById('content') != undefined){ ";
			print "document.getElementById('content').style.paddingLeft = 10 + \"px\";";
			print "}";
			print "</script>";
		}

		return;
	}


	/*
	* build module menu
	*/
	### Modified By Ronald (20080402)- Add new variable: $MODULE_OBJ['TitleBar_Style'] ###
	### Modified By key (20080818)- Enlarge the width 200 -> 220
	function MODULE_TITLE(){
		global $PATH_WRT_ROOT, $intranet_root, $LAYOUT_SKIN, $MODULE_OBJ;
		
		# 20090713 yat - add $MODULE_OBJ['title_css'] to set the style of the title (opened/closed)
		$title_css = $MODULE_OBJ['title_css'] ? $MODULE_OBJ['title_css'] : "menu_closed";
		
		$x = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
			       <tr><td>
						<div id="header">
							<div class="header_title">
								'.$MODULE_OBJ['CustomLogo'].'
								<span id="module_title" class="'. $title_css.' ">'.$MODULE_OBJ['title'].'</span>
							</div>
						</div>
					</td></tr>
				</table>';
		/*$rx = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
		$rx .= "<tr>\n";
		if($MODULE_OBJ['TitleBar_Style'] == 2)
		{
			$rx .= "<td width='300' id='titlebar' style='padding-left: 170px; ' background='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_01.gif' class='title' />".$MODULE_OBJ['title']."</td>\n";
		}
		else
		{
			$rx .= "<td width='220' id='titlebar' style='padding-left: 170px; ' background='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_01.gif' class='title' />".$MODULE_OBJ['title']."</td>\n";
		}
		$rx .= "<td width='10'><img src='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_02.gif' width='10' height='41' /></td>\n";
		$rx .= "<td background='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_03.gif' />&nbsp;</td>\n";
		$rx .= "<td width='22'><img src='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_04.gif' width='22' height='41' /></td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table>\n";*/

		echo $x;
	}

	/*
	* build popup title
	*/
	function POPUP_TITLE(){

		global $PATH_WRT_ROOT, $intranet_root, $LAYOUT_SKIN, $MODULE_OBJ;

		$rx = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $rx .= "<tr>\n";
		$rx .= "<td background=\"/images/".$LAYOUT_SKIN."/popup/top_01.gif\" class=\"popuptitle\">".$MODULE_OBJ['title'];
		$rx .= "</td>\n";
		$rx .= "<td width=\"14\"><img src=\"/images/".$LAYOUT_SKIN."/popup/top_02b.gif\" width=\"14\" height=\"31\"></td>\n";
        $rx .= "</tr>\n";
		$rx .= "</table>\n";

		/*
		$rx = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
		$rx .= "<tr>\n";
		$rx .= "<td width='160' id='titlebar' style='padding-left: 170px' background='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_01.gif' class='title' />".$MODULE_OBJ['title']."</td>\n";
		$rx .= "<td width='10'><img src='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_02.gif' width='10' height='41' /></td>\n";
		$rx .= "<td background='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_03.gif' />&nbsp;</td>\n";
		$rx .= "<td width='22'><img src='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/titlebar_04.gif' width='22' height='41' /></td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table>\n";
		*/

		echo $rx;
	}

	function POPUP_TITLE2(){

		global $MODULE_OBJ;

		$rx = $MODULE_OBJ['title'];

		echo $rx;
	}

    function BACK(){

		global $button_back;

		$rx = $button_back;

		echo $rx;
	}

	/*
	* build module menu
	*/
	function MODULE_MENU(){
		global $MODULE_OBJ, $contentLeftPos, $UserID, $PATH_WRT_ROOT, $i_general_expand_all, $i_general_collapse_all, $sys_custom;
		global $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
		global $customLeftMenu, $CurrentPageArr;
		global $LAYOUT_SKIN;

		if (sizeof($MODULE_OBJ) == 1) {
			return;
		}

		// customize left menu
		if ((isset($customLeftMenu) && $customLeftMenu != "")||$MODULE_OBJ['CustomLogo']!= "") {
			echo $customLeftMenu;
			return;
		}

		// determine module id by logo image path
		$ModuleID = md5(str_replace("../", "", $MODULE_OBJ["logo"]))."_".$UserID;

		// get the key array
		if (sizeof($MODULE_OBJ['menu']) > 0)
		foreach ($MODULE_OBJ['menu'] as $key => $MenuSection)
		{
			if (!is_array($KeyFlag) || !$KeyFlag[$key])
			{
				$KeyArrayJS .= "MenuKeyArray[MenuKeyArray.length] = \"{$key}\";\n";
				if(!empty($MenuSection['Child']))
					$ParentArrayJS .= "ParentMenuKeyArray[ParentMenuKeyArray.length] = \"{$key}\";\n";
				$KeyFlag[$key] = true;
			}
		}

		# generate
		$rx = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$rx .= "<tr>\n";
		$rx .= "<td width=\"148\" valign=\"top\" background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/bg.gif\">\n";
		$rx .= "<table width=\"148\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$rx .= "<tr>\n";
		$rx .= "<td bgcolor=\"#FFFFFF\">\n";
		$rx .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/top_border.gif\" style=\"background-repeat:no-repeat;background-position:right;\" >
		        <tr>
					<td height='10'><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width='10' height='10'><a href='javascript:void(0)' onClick=\"jDISPLAY_ALL_MENU_ITEM(ParentMenuKeyArray)\" class='hidemenu'></a></td>
					<td width='14'><a href='javascript:void(0)' onClick=\"jDISPLAY_ALL_MENU_ITEM(ParentMenuKeyArray)\" title='".$i_general_expand_all." / ".$i_general_collapse_all."'><img src='{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/icon_hidebtn_more_off.gif' name='allshow' width='14' height='9' border='0' id='allshow' onMouseOver=\"MM_swapImage('allshow','','{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/icon_hidebtn_more_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>
				</tr>
				</table>\n";

		if (sizeof($MODULE_OBJ['menu']) > 0)
		foreach ($MODULE_OBJ['menu'] as $key => $MenuSection)
		{
			$TempArr = $MenuSection['Child'];

			$HasChild = (!empty($TempArr)) ? true : false;

			$Selected = $MenuSection[2];

			if ($Selected)
			{
				$tbClass = "menubgselected";
				$menuClass = "menuon";
			} else
			{
				$tbClass = "menubg";
				$menuClass = "menu";
			}

			####### section ##############################################################

			$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
			$rx .= "<tr>\n";
			$rx .= "<td>";

			for ($i = 0; $i < 2; $i ++)
			{

				if ($i != 0)
				{
					// expand
					if ($Selected)
					{
						$normalImg = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_selected.gif";
					} else
					{
						$normalImg = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";
					}
					$mouseOverImg = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_on.gif";
					$DisplayStyle = " style=\"display: block\" ";
					$TableIDExt = "Expand";
				} else
				{
					// close
					if ($Selected)
					{
						$normalImg = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_selected.gif";
					} else
					{
						$normalImg = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off.gif";
					}
					$mouseOverImg = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_on.gif";
					$DisplayStyle = " style=\"display: none\" ";
					$TableIDExt = "Close";
				}

				if ($MenuSection[3] != "") {
					$frontImg = $MenuSection[3];
					$paddingstyle = "style=\"margin-left: 5px; margin-right: 5px\"";
				} else {
					$frontImg = "$PATH_WRT_ROOT/images/".$LAYOUT_SKIN."/10x10.gif";
					$paddingstyle = "";
				}

				/*
				if (!is_array($KeyFlag) || !$KeyFlag[$key])
				{
					$KeyArrayJS .= "MenuKeyArray[MenuKeyArray.length] = \"{$key}\";\n";
					$KeyFlag[$key] = true;
				}
				*/

				$rx .= "<table width=\"100%\" id=\"Table{$key}{$TableIDExt}\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"$tbClass\" onMouseOver=\"this.className='menubgon'\" onMouseOut=\"this.className='$tbClass'\" $DisplayStyle>\n";
				$rx .= "<tr>\n";
				$rx .= "<td width=\"10\" height=\"30\"> <img src=\"$frontImg\" align=\"absmiddle\" $paddingstyle></td>\n";


				if ($CurrentPageArr['iMail'] == 1)
				{
				//For iMail Checking Mail Menu
					if ($HasChild)
					{
						if ($MenuSection[1] != "")
						{
							$rx .= "<td><a href=\"".$MenuSection[1]."\" class=\"$menuClass\"";
						}
						else
						{
							$rx .= "<td><a href=\"javascript:void(0)\" class=\"$menuClass\"";
						}
					}
					else
					{
						$rx .= "<td><a href=\"".$MenuSection[1]."\" class=\"$menuClass\"";
					}
					if (($HasChild) && ($MenuSection[1] == ""))
					{
						### onClick action ###
						$rx .= " onClick=\"jDISPLAY_MENU_ITEM('{$key}')\"";
						### onClick action ###
					}
				}
				else
				{
					$rx .= ($HasChild) ? "<td><a href=\"javascript:void(0)\" class=\"$menuClass\"" : "<td><a href=\"".$MenuSection[1]."\" class=\"$menuClass\"";
					if ($HasChild)
					{
						### onClick action ###
						$rx .= " onClick=\"jDISPLAY_MENU_ITEM('{$key}')\"";
						### onClick action ###
					}
				}

				$rx .= ">\n";
				$rx .= $MenuSection[0];
				$rx .= "</a></td>\n";


				$rx .= "<td width=\"15\">";

				if($HasChild)
				{
					### OnClick Menu Section ####
					$rx .= "<a href=\"javascript:void(0)\"><img src=\"{$normalImg}\" name=\"lb{$key}\" width=\"15\" height=\"30\" border=\"0\" id=\"lb{$key}{$TableIDExt}\" onMouseOver=\"MM_swapImage('lb{$key}{$TableIDExt}','','{$mouseOverImg}',1)\" onMouseOut=\"MM_swapImgRestore()\"";
					$rx .= " onClick=\"jDISPLAY_MENU_ITEM('{$key}')\" /></a>";
					### OnClick Menu Section ####
				}

				$rx .= "</td>\n";
				$rx .= "</tr></table>";
			}


			$rx .= "</td>\n";
			$rx .= "</tr>\n";

			if($HasChild)
			{
				$rx .= "<tr id=\"ItemList{$key}\" class=\"menubg-ItemList\" $DisplayStyle>\n";
				$rx .= "<td>\n";

				foreach ($TempArr as $MenuItem) {
					//hdebug_r($MenuItem);

					if ($MenuItem[2]) {
						// Selected Style
						if ($sys_custom["DHL"]) {
							$icon = "{$PATH_WRT_ROOT}templates/" . $sys_custom['Project_Label'] . "/images/icon_sub_on.gif";
						} else {
							$icon = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/icon_sub_on.gif";
						}
						$height = "15";
						$tdclass = "menuon";
					} else {
						// Normal Style
						$icon = "{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif";
						$height = "20";
						$tdclass = "submenu";
					}

					if ($MenuItem[3] != "") $icon = $MenuItem[3];

					####### Expand content ##############################################################
					$rx .= "<table>\n";
					$rx .= "<tr>\n";
					$rx .= "<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";

					
					if($MenuItem[1]=="lite")
					{
						# for eEnrolment Lite version 2011-09-07 YatWoon (maybe can use in other module lite version in future)
						$item_link = "<font color='#cccccc'><i>".$MenuItem[0]."</i></font>";
					}
					else
					{
						$target = '';
						if(isset($MenuItem['target'])) {
							$target = 'target="'.$MenuItem['target'].'"';
						}
						$item_link = "<a href=\"".$MenuItem[1]."\" class=\"$tdclass\" $target>".$MenuItem[0]."</a>";
						
					}
					#### Item Line ####
					$rx .= "<tr>\n";
					$rx .= "<td>";
					$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
                    $rx .= "<tr>\n";
					$rx .= "<td width=\"6\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"6\" height=\"20\"></td>\n";
					$rx .= "<td width=\"10\" align=\"center\" valign=\"top\">\n";
					$rx .= "<img src=\"$icon\" ></td>\n";
					$rx .= "<td width=\"124\">". $item_link ."</td>\n";
					$rx .= "</tr>\n";


					$rx .= "</table>";
					$rx .= "</td>\n";
					$rx .= "</tr>\n";

					/*
						if have child, expand here
					*/
					if (sizeof($MenuItem["child"]) > 0)
					{
						$rx .= "<tr><td >";
						foreach ($MenuItem["child"] as $ThirdChildItem)
						{

							if ($ThirdChildItem[3] != "") $childicon = $ThirdChildItem[3];


							if ($ThirdChildItem[2]) {
								// Selected Style
								$childtdclass = "menuon";
							} else {
								// Normal Style
								$childtdclass = "submenu";
							}

							### Third Level Child ###
							$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
                            $rx .= "<tr>\n";
							$rx .= "<td width=\"6\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"30\" height=\"15\"></td>\n";
							$rx .= "<td width=\"10\" align=\"center\" valign=\"top\">\n";
							$rx .= "<img src=\"$childicon\" ></td>\n";
							$rx .= "<td width=\"200\"><a href=\"".$ThirdChildItem[1]."\" class=\"$childtdclass\">\n";
							$rx .= $ThirdChildItem[0];
							$rx .= "</a></td>\n";
							$rx .= "</tr>\n";
							$rx .= "</table>";
							### Third Level Child ###

						}
						$rx .= "</td></tr>";
					}

					#### Item Line ####
					if ($MenuItem[4]) {
						//$SubMenuArr['Settings']['Calculation']['name']
						/*
						for ($SubMenuCount = 0; $SubMenuCount < sizeof(); $SubMenuCount++) {
							$rx .= "<tr>\n";
							$rx .= "<td>";
							$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
							$rx .= "<tr>\n";
							$rx .= "<td width=\"6\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"6\" height=\"20\"></td>\n";
							$rx .= "<td width=\"10\" align=\"center\" valign=\"top\">\n";
							$rx .= "<img src=\"$icon\" width=\"10\" height=\"$height\"></td>\n";
							$rx .= "<td width=\"124\"><a href=\"".$MenuItem[1]."\" class=\"$tdclass\">\n";
							$rx .= $MenuItem[0];
							$rx .= "</a></td>\n";
							$rx .= "</tr>\n";
							$rx .= "</table>";
							$rx .= "</td>\n";
							$rx .= "</tr>\n";
						}
						*/
					}


					$rx .= "</table></td>\n";
					$rx .= "</tr>\n";
					$rx .= "</table>\n";
					####### Expand content ##############################################################
				}

				$rx .= "</td>\n";
				$rx .= "</tr>\n";
			}

			$rx .= "</table>\n";
			####### section ##############################################################
		}


		$rx .= "</td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td bgcolor=\"#FFFFFF\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"30\"></td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table>\n";
		$rx .= "</td>\n";
		$rx .= "<td width=\"14\" valign=\"top\" background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/hide_bg.gif\">\n";
		$rx .= "<table width=\"14\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
		$rx .= "<tr>\n";
		$rx .= "<td width=\"14\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"14\" height=\"47\"></td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td width=\"14\" height=\"104\" align=\"center\" id=\"showhideLinkText\">";

		$rx .= "<a href=\"javascript:void(0)\" class=\"hidemenu\" onMouseOver=\"MM_swapImage('arrow_hide','','{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/icon_hide_arrow_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ";
		$rx .= " onClick=\"(isShow) ? HideLeftMenu(0, -{$contentLeftPos}):ShowLeftMenu(-{$contentLeftPos} ,0); \"";
		$rx .= "><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/icon_hide_arrow_off.gif\" name=\"arrow_hide\" width=\"14\" height=\"11\" border=\"0\" id=\"arrow_hide\"><br>\n";
		$rx .= "&nbsp;<br />\n";
		$rx .= "&nbsp;<br />\n";
		$rx .= "</a>";

		$rx .= "</td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table></td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/bottom.gif\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/bottom.gif\" width=\"148\" height=\"13\"></td>\n";
		$rx .= "<td><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/leftmenu/hide_bottom.gif\" width=\"14\" height=\"13\"></td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table>\n";

		if ($KeyArrayJS!="")
		{
			$rx .= "<script language='javascript'>\n var MenuKeyArray = Array();\n" . $KeyArrayJS . "var MenuID = \"{$ModuleID}\";\n jINITIAL_MENU_OPEN();\n</script>\n";
		}
		$rx .= "<script language='javascript'>\n var ParentMenuKeyArray = Array();\n " . $ParentArrayJS . "\n</script>\n";

		echo $rx;
		
	}

	/*
	*  GET the Current Page Title
	*/
	function CURRENT_PAGE_TITLE(){

		global $CurrentPageName;

		$rx = $CurrentPageName;

		echo $rx;
	}

	/*
	* build module menu for iport student mode
	*/
	function MODULE_MENU_IPORTFOLIO(){

		global $MODULE_OBJ, $contentLeftPos, $UserID, $PATH_WRT_ROOT, $i_general_expand_all, $i_general_collapse_all;
		global $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
		global $customLeftMenu, $CurrentPageArr, $CurrentPage;
		global $image_path, $LAYOUT_SKIN;

		# wordings
		global $iPort;

		$StudentName = $MODULE_OBJ["user_name"];
		$StudentPhotoPath = $MODULE_OBJ["user_photo"];

		if (sizeof($MODULE_OBJ) == 1) {
			return;
		}

		// customize left menu
		if (isset($customLeftMenu) && $customLeftMenu != "") {
			echo $customLeftMenu;
			return;
		}

		$rx .= "<table width=\"192\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$rx .= "<tr>";
		$rx .= "<td><img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_01.gif\" width=\"192\" height=\"21\"></td>";
		$rx .= "</tr>";
		$rx .= "<tr>";
		$rx .= "<td align=\"center\" background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_02.gif\">";
		$rx .= "<div style=\"display:block; background-image:url('".$PATH_WRT_ROOT."/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_name_bg.gif'); background-repeat:repeat-y; padding:5px; padding-right:20px;margin-top:20px; color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; text-align:left\">".$StudentName."</div>";

		$rx .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-right:30px;margin-top:15px;margin-bottom:15px\">";
		$rx .= "<tr>";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/photo_frame_01.gif\" style=\"padding-left:10px; padding-top:10px;\"><img src=\"".$StudentPhotoPath."\" width=\"100\" height=\"130\"></td>";
		$rx .= "<td height=\"10\" background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/photo_frame_02.gif\"><img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" alt=\"\" width=\"10\" height=\"10\"></td>";
		$rx .= "</tr>";
		$rx .= "<tr>";
		$rx .= "<td height=\"10\" background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/photo_frame_03.gif\"><img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"></td>";
		$rx .= "<td height=\"10\"><img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/photo_frame_04.gif\" width=\"10\" height=\"10\"></td>";
		$rx .= "</tr>";
		$rx .= "</table>";

		$rx .= "</td>";
		$rx .= "</tr>";

		// Data Menu
		$rx .= "<tr>";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_03.gif\">";
		$rx .= "<div id=\"stu_left_function_item\">";
		$rx .= "<ul>";

		foreach ($MODULE_OBJ['menu']['Data'] as $Menu)
		{
			$Title = $Menu[0];
			$Link = $Menu[1];
			$Checked = $Menu[2];

			if($Checked == "1")
			$ClassName = "icon_stu_acc_on";
			else
			$ClassName = "icon_stu_acc";

			$rx .= "<li><a href=\"".$Link."\" class=\"".$ClassName."\"><span>".$Title."</span></a></li>";
		}

		$rx .= "</ul>";
		$rx .= "</div>";
		$rx .= "<img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_04_b.gif\" width=\"192\" height=\"12\" style=\"clear:both\"></td>";
		$rx .= "</tr>";

		// Personal Menu - portfolio
		$rx .= "<tr>";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_05.gif\" width=\"192\">";
		$rx .= "<div id=\"stu_left_function_item\">";
		$rx .= "<ul>";

		if(sizeof($MODULE_OBJ['menu']['Personal']) > 0)
		foreach ($MODULE_OBJ['menu']['Personal'] as $Menu)
		{
			$Title = $Menu[0];
			$Link = $Menu[1];
			$Checked = $Menu[2];

			if($Checked == "1")
			$ClassName = "icon_LP_on";
			else
			$ClassName = "icon_LP";

			$rx .= "<li><a href=\"".$Link."\" class=\"".$ClassName."\"><span>".$Title."</span></a></li>";
		}

		$rx .= "</ul>";
		$rx .= "</div>";
		$rx .= "<img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_06b.gif\" width=\"192\" height=\"15\" style=\"clear:both\"></td>";
		$rx .= "</tr>";

		// School Menu - scheme
		$rx .= "<tr>";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_07.gif\" style=\"background-position:bottom\">";
		$rx .= "<div id=\"stu_left_function_item\">";
		$rx .= "<ul>";

		if(is_array($MODULE_OBJ['menu']['School']))
		{
			foreach ($MODULE_OBJ['menu']['School'] as $Menu)
			{
				$Title = $Menu[0];
				$Link = $Menu[1];
				$Checked = $Menu[2];

				if($Checked == "1")
				$ClassName = "icon_scheme_on";
				else
				$ClassName = "icon_scheme";

				$rx .= "<li><a href=\"".$Link."\" class=\"".$ClassName."\"><span>".$Title."</span></a></li>";
			}
		}

		$rx .= "</ul>";
		$rx .= "</div>";
		$rx .= "<img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"192\" height=\"15\" style=\"clear:both\"></td>";
		$rx .= "</tr>";

		// other module
		$rx .= "<tr>";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_08a.gif\" style=\"background-position:bottom\">";
		$rx .= "<div id=\"stu_left_function_item\">";
		$rx .= "<ul>";

		if(is_array($MODULE_OBJ['menu']['Addon']))
		{
			foreach ($MODULE_OBJ['menu']['Addon'] as $Menu)
			{
				$Title = $Menu[0];
				$Link = $Menu[1];
				$Checked = $Menu[2];

				if($Checked == "1")
				$ClassName = "icon_addon_on";
				else
				$ClassName = "icon_addon";

				$rx .= "<li><a href=\"".$Link."\" class=\"".$ClassName."\"><span>".$Title."</span></a></li>";
			}
		}

		$rx .= "</ul>";
		$rx .= "</div>";
		$rx .= "<img src=\"{$PATH_WRT_ROOT}/{$image_path}/{$LAYOUT_SKIN}/iPortfolio/leftmenu_08.gif\" width=\"192\" height=\"14\" style=\"clear:both\"></td>";
		$rx .= "</tr>";

		$rx .= "</table>";

		echo $rx;
	}


	/*
	* Focus to given field when onLoad
	*/
	function FOCUS_ON_LOAD($par_name){

		$ReturnStr = "<script language='JavaScript' type='text/javascript'>\n";
		$ReturnStr .= "if (typeof(document.{$par_name})!=\"undefined\")\n";
		$ReturnStr .= "{document.{$par_name}.focus();}\n";
		$ReturnStr .= "</script>\n";

		return $ReturnStr;

	}



	/*
	* Get corresponding message
	*/
	function GET_SYS_MESSAGE($ParCode, $Supplementary=NULL){

		global $LangWord;

		$Message = $LangWord['sysmsg'][$ParCode];

		# import succeed and display number of updates
		if (is_array($Supplementary) && $ParCode==5)
		{
			if ($Supplementary['TotalNew']>0 && $Supplementary['TotalUpdate']>0)
			{
				$Message = str_replace("<!--TotalNew-->", $Supplementary['TotalNew'], $LangWord['import']['sysmsg3']);
				$Message = str_replace("<!--TotalUpdate-->", $Supplementary['TotalUpdate'], $Message);
			} elseif ($Supplementary['TotalNew']>0)
			{
				$Message = str_replace("<!--TotalNew-->", $Supplementary['TotalNew'], $LangWord['import']['sysmsg1']);
			} elseif ($Supplementary['TotalUpdate']>0)
			{
				$Message = str_replace("<!--TotalUpdate-->", $Supplementary['TotalUpdate'], $LangWord['import']['sysmsg2']);
			}
		}

		if ($Message!="")
		{
			$ReturnStr = "<span class='TEXT_sys_msg'><font color=white>|</font> &nbsp; ".$Message." &nbsp; <font color=white>|</font></span>";
		}

	 	return $ReturnStr;
	}


	/**
	 * Function to generate page navigation title
	 * @param array $pages_arr 2D Array, For 2nd dimension [0=>page title, 1=>page url]
	 * @return html Breadcrumb
	 */
	function GET_NAVIGATION($pages_arr) {

		global $image_path, $LAYOUT_SKIN;

		for ($i=0; $i<sizeof($pages_arr); $i++)
		{
			list($page_title, $page_href) = $pages_arr[$i];
			$page_title = trim($page_title);
			$page_href = trim($page_href);
			if ($page_title!="")
			{
				$rx .= " &nbsp; <img src='{$image_path}/{$LAYOUT_SKIN}/nav_arrow.gif' width='15' height='15' align='absmiddle' />";
				$rx .= ($page_href!="") ? "<a href=\"$page_href\" class='navigation'>" . $page_title . "</a>" : "<span class='navigation'>" . $page_title . "</span>";
			}
		}

		return $rx;
	}

	function GET_NAVIGATION_IP25($pages_arr, $customClass='') {

		$rx = "<div class='navigation_v30 $customClass'>";
		
		for ($i=0; $i<sizeof($pages_arr); $i++)
		{
			list($page_title, $page_href) = $pages_arr[$i];
			$page_title = trim($page_title);
			$page_href = trim($page_href);
			if ($page_title!="")
			{
				$rx .= ($page_href!="") ? "<a href=\"$page_href\">" . $page_title . "</a>" : "<span>" . $page_title . "</span>";
			}
		}
		$rx .= "</div>";
		$rx .= $this->Spacer();
		
		return $rx;
	}

        function GET_NAVIGATION2($title) {

		global $image_path, $LAYOUT_SKIN;

		$rx .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" />";
		$rx .= "<span class=\"sectiontitle\">" . $title . "</span>";

		//$rx = "<span class=\"sectiontitle\"> ". $title ."</span><br />";
		return $rx;
	}

    function GET_NAVIGATION2_IP25($title) {

		$rx .= "<span class=\"sectiontitle_v30\">" . $title . "</span>";

		//$rx = "<span class=\"sectiontitle\"> ". $title ."</span><br />";
		return $rx;
	}
	
	## eCommunity
	function GET_NAVIGATION3($ary)
	{
		global $image_path, $LAYOUT_SKIN;

		$rx .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_section.gif\" width=\"15\" height=\"15\" align=\"absmiddle\" />";

		for($i=0;$i<sizeof($ary);$i++)
		{
			$thisTitle = $ary[$i];
			$thisLink = $ary[++$i];

			if($thisLink)
				$rx .= "<span class=\"navigation\"> <a href=\"". $thisLink ."\">". $thisTitle ."</a> </span>";
			else
				$rx .= "<span class=\"tabletext\"> ". $thisTitle ."</span>";

			if($i+1 < sizeof($ary) and $ary[$i+1] <>"")
				$rx .= "<span class=\"navigation\">&gt; </span>";
		}


		//&gt;

		return $rx;
	}

	// eDiscipline v1.2
	function GET_NAVIGATION4($pages_arr) {

		global $image_path, $LAYOUT_SKIN;

		for ($i=0; $i<sizeof($pages_arr); $i++)
		{
			list($page_title, $page_href) = $pages_arr[$i];
			$page_title = trim($page_title);
			$page_href = trim($page_href);
			if ($page_title!="")
			{
				$rx .= "<img src='{$image_path}/{$LAYOUT_SKIN}/nav_arrow.gif' width='15' height='15' align='absmiddle' />";
				$rx .= ($page_href!="") ? "<a href=\"$page_href\">" . $page_title . "</a>" : $page_title;
			}
		}

		return $rx;
	}
	// eDiscipline v1.2

	function GET_LNK_GENERATE($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $i_Discipline_System_Discipline_Conduct_Mark_Generate_Conduct_Grade;

		$text = ($button_text=="") ? $i_Discipline_System_Discipline_Conduct_Mark_Generate_Conduct_Grade : $button_text;
		
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			
			$rx .= "<a href=\"$ParHref\" class=\"generate $ParClass\" $onclick $ParOthers>";
			$rx .= $text;
			$rx .= "</a>";
		}
		else
		{
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="generate '.$ParClass.'" '.$text.' '.$ParOthers.'> '. $text .'</a>';
			$rx .= '</div>';
		}
		

		return $rx;
	}

	/*
	* Get pull-down selection
	*/
	# generate selection box
	function GET_SELECTION_BOX($ParData, $ParTags, $ParDefault, $ParSelected="", $CheckType=false){

		$ReturnStr = "<select $ParTags>\n";
		if ($ParDefault!="")
		{
			$ReturnStr .= "<option value=\"\">{$ParDefault}</option>\n";
		}
		if ($CheckType) {
			for ($i=0; $i<sizeof($ParData); $i++)
			{
				list($ID, $Name) = $ParData[$i];
				$SelectedStr = ($ParSelected === $ID || (is_array($ParSelected) && in_array($ID, $ParSelected,true)))? "SELECTED" : "";
				$ReturnStr .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
			}
		}
		else {
			for ($i=0; $i<sizeof($ParData); $i++)
			{
				list($ID, $Name) = $ParData[$i];
				$SelectedStr = ($ParSelected == $ID || (is_array($ParSelected) && in_array($ID, $ParSelected)))? "SELECTED" : "";
				$ReturnStr .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
			}
		}
		$ReturnStr .= "</select>\n";

		return $ReturnStr;
	}

	# Param $ParData[$ctr] = array($ID, $Name, $OptGroupLabel)
	function GET_SELECTION_BOX_WITH_OPTGROUP($ParData, $ParTags, $ParDefault, $ParSelected=""){

		for ($i=0; $i<sizeof($ParData); $i++)
		{
			list($ID, $Name, $OptGroupLabel) = $ParData[$i];
			if(!$OptGroupLabel)
			{
				$NonGroupItems[] = array($ID,$Name);
				$OptionOrder[] = $ID;
			}
			else
			{
				$ary[$OptGroupLabel][] = array($ID,$Name);
				$OptionOrder[] = $OptGroupLabel;
			}	
			
		}
		
		$ReturnStr = "<select $ParTags>\n";
		if ($ParDefault!="")
		{
			$ReturnStr .= "<option value=\"\">{$ParDefault}</option>\n";
		}
		
		## non grouped options
		for ($i=0; $i<sizeof($NonGroupItems); $i++)
		{
			list($ID, $Name) = $NonGroupItems[$i];
			$SelectedStr = (($ParSelected == $ID || (is_array($ParSelected) && in_array($ID, $ParSelected)))? "SELECTED":"");
			$OptionStr[$ID] .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
		}
		
		## options with optgroup
		foreach((array)$ary as $OptGroupLabel => $options)
		{
			$OptionStr[$OptGroupLabel] .= "<optgroup label='$OptGroupLabel'>";
			for ($i=0; $i<sizeof($options); $i++)
			{
				list($ID, $Name) = $options[$i];
				$SelectedStr = (($ParSelected == $ID || (is_array($ParSelected) && in_array($ID, $ParSelected)))? "SELECTED":"");
				$OptionStr[$OptGroupLabel] .= "<option value=\"{$ID}\" {$SelectedStr}>{$Name}</option>\n";
			}
			$OptionStr[$OptGroupLabel] .= "</optgroup>";
		}
		
		$OptionOrder = array_unique((array)$OptionOrder);
		foreach($OptionOrder as $idx)
			$ReturnStr .= $OptionStr[$idx];
		
		$ReturnStr .= "</select>\n";

		return $ReturnStr;
	}


	function GET_STEPS($StepsObj) {
		//global $i_general_steps;
		global $Lang,$LAYOUT_SKIN;

		$ReturnStr = "<table align='center' width='90%' border='0' cellpadding='3' cellspacing='0' class='steptable'>\n";
		$ReturnStr .= "<tr><td>\n";
		$ReturnStr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
		$ReturnStr .= "<tr>\n";
		$ReturnStr .= "<td class='steptitlebg steptitletext'>".$Lang['General']['Steps'].":</td>\n";
		$ReturnStr .= "</tr>\n";
		$ReturnStr .= "<tr>\n";
		$ReturnStr .= "<td>\n";

		$ReturnStr .= "<table width='100%' border='0' cellspacing='0' cellpadding='5'><tr>\n";

		$width = 100 / sizeof($StepsObj);
		for ($i=0; $i<sizeof($StepsObj); $i++)
		{
			if ($StepsObj[$i][1])
			{
				$ReturnStr .= "<td valign='top' width='". $width."%'><table width='100%' border='0' cellpadding='3' cellspacing='0'><tr><td width='23' valign='top'>\n";
				$ReturnStr .= "<table width='23' height='23' border='0' cellpadding='0' cellspacing='0'><tr><td width='23' align='center' valign='middle' background='/images/".$LAYOUT_SKIN."/stepnum_on.gif' class='stepon'>".($i+1)."</td>\n";
				$ReturnStr .= "</tr></table></td><td class='steptitletext'>".$StepsObj[$i][0]."</td></tr></table></td>\n";
			} else
			{
				$ReturnStr .= "<td valign='top' width='". $width."%'><table width='100%' border='0' cellpadding='3' cellspacing='0'><tr><td width='19' valign='top'>\n";
				$ReturnStr .= "<table width='19' height='19' border='0' cellpadding='0' cellspacing='0'><tr><td align='center' valign='middle' background='/images/".$LAYOUT_SKIN."/stepnum_off.gif' class='stepoff'>".($i+1)."</td>\n";
				$ReturnStr .= "</tr></table></td><td class='stepofftext'>".$StepsObj[$i][0]."</td></tr></table></td>\n";
			}
		}
		$ReturnStr .= "</tr></table>\n";


		$ReturnStr .= "</td>\n";
		$ReturnStr .= "</tr>\n";
		$ReturnStr .= "</table></td>\n";
		$ReturnStr .= "</tr></table>\n";

		return $ReturnStr;
	}

	function GET_STEPS_IP25($StepsObj) {
		//global $i_general_steps;
		global $Lang,$LAYOUT_SKIN;

		$ReturnStr = "<div class='stepboard stepboard_". sizeof($StepsObj) ."s'>";
		$ReturnStr .= "<h1>".$Lang['General']['Steps'].":</h1>";
		$ReturnStr .= "<ul>";
		for ($i=0; $i<sizeof($StepsObj); $i++) {
			if ($StepsObj[$i][1]) {
				$ReturnStr .= "<li class='stepon'><div><em>".($i+1)."</em><span>".$StepsObj[$i][0]."</span></div></li>";
			} else {
				$ReturnStr .= "<li><div><em>".($i+1)."</em><span>".$StepsObj[$i][0]."</span></div></li>";
			}
		}
		$ReturnStr .= "</ul><br style='clear:both' /></div>";

		return $ReturnStr;
	}
	
	function GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $contentMsg='',$warningHeading='') {

		global $eComm, $Lang;
		
		if ($contentMsg == '') {
			if ($DeleteItemName != '')
			{
				if ($DeleteButtonName == '')
					$DeleteButtonName = $Lang['Btn']['DeleteAll'];
				
				$msg = $Lang['General']['DeleteAllWarning'];
				$msg = str_replace('<!--ItemName-->', $DeleteItemName, $msg);
				$msg = str_replace('<!--ButtonName-->', $DeleteButtonName, $msg);
			}
			else
			{
				$msg = ($eComm[$err_msg] ? $eComm[$err_msg] : $err_msg);
			}
		}
		else {
			$msg = $contentMsg;
		}
		//Change heading 20160506 Kenneth
		if($warningHeading===''){
			$warningHeading = $eComm['Warning'];
		}
		$ReturnStr = "<table align='center' width='100%' border='0' cellpadding='3' cellspacing='0' class='Warningtable'>\n";
		$ReturnStr .= "<tr><td>\n";
		$ReturnStr .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
		$ReturnStr .= "<tr>\n";
		$ReturnStr .= "<td class='Warningtitlebg Warningtitletext'>".$warningHeading.":</td>\n";
		$ReturnStr .= "</tr>\n";
		$ReturnStr .= "<tr>\n";
		$ReturnStr .= "<td>\n";
		$ReturnStr .= "<table width='100%' border='0' cellspacing='0' cellpadding='5'><tr>\n";
		$ReturnStr .= "<td class=\"Warningtitletext\">". $msg ."</td>";
		$ReturnStr .= "</tr></table>\n";
		$ReturnStr .= "</td>\n";
		$ReturnStr .= "</tr>\n";
		$ReturnStr .= "</table></td>\n";
		$ReturnStr .= "</tr></table>\n";

		return $ReturnStr;
	}
	
	/*
	* Get language options: eng, zh-tw, zh-cn
	*/
	function GET_LANGUAGE_SWITCH(){

		global $LangWord;

		if ($_SESSION['SSV_LANGUAGE']!="en")
		{
			$LanguageArr[] = "<a href=\"javascript:jCHANGE_LANG('en')\" class=\"LINK_footer\"> {$LangWord['sys']['en']} </a>";
		}

		if ($_SESSION['SSV_LANGUAGE']!="zh-tw")
		{
			$LanguageArr[] = "<a href=\"javascript:jCHANGE_LANG('zh-tw')\" class=\"LINK_footer\"> {$LangWord['sys']['zh-tw']} </a>";
		}

		if ($_SESSION['SSV_LANGUAGE']!="zh-cn")
		{
			$LanguageArr[] = "<a href=\"javascript:jCHANGE_LANG('zh-cn')\" class=\"LINK_footer\"> {$LangWord['sys']['zh-cn']} </a>";
		}

		$ReturnStr = implode("<span class=\"LINK_footer\">|</span>\n", $LanguageArr);

		return $ReturnStr;
	}



	/*
	* Get title tag
	*/
	function TITLE_TAGS_OLD(){

		global $TAGS_OBJ, $MODULE_OBJ, $PATH_WRT_ROOT;

		if (sizeof($TAGS_OBJ) < 2) {
			$ReturnStr = "<span class=\"contenttitle\" style=\"vertical-align: bottom; \">".$TAGS_OBJ[0][0]."</span>";
		} else {
			$ReturnStr = "<table border=\"0\" bordercolor=\"red\" cellpadding=\"0\" cellspacing=\"0\"><tr>\n";

			$isSelected = 0;

			for ($i = 0; $i < sizeof($TAGS_OBJ); $i++) {
				$ReturnStr .= "<td>";
				if ($TAGS_OBJ[$i][2]) {
					$ReturnStr .= "
								  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                                <tr>
	                                  <td width=\"7\"><img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/tab_on_01.gif\" width=\"7\" height=\"25\"></td>
	                                  <td nowrap background=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/tab_on_02.gif\">
	                                    <a href=\"".$TAGS_OBJ[$i][1]."\" class=\"menuon\">".$TAGS_OBJ[$i][0]."</a></td>
	                                  <td width=\"7\"><img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/tab_on_03.gif\" width=\"7\" height=\"25\"></td>
	                                </tr>
	                              </table>

	                              ";
					$isSelected = 1;
				} else {
					$ReturnStr .= "
									<td nowrap style=\"vertical-align: bottom\">
										<a href=\"".$TAGS_OBJ[$i][1]."\" class=\"submenu\">".$TAGS_OBJ[$i][0]."</a>
									</td>
								  ";
					$isSelected = 0;
				}
				$ReturnStr .= "</td>";

				if ($isSelected) {
					$ReturnStr .= "<td width=\"10\">&nbsp;</td>";
				} else {
					if ($TAGS_OBJ[$i+1][2]) {
						$ReturnStr .= "<td width=\"10\">&nbsp;</td>";
					} else {
						if (($i+1) != sizeof($TAGS_OBJ)) {
							$ReturnStr .= "<td width=\"15\" style=\"padding-top: 6px\"><img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/tab_st.gif\" width=\"15\" height=\"15\"></td>";
						}
					}
				}
			}

			$ReturnStr .= "</tr></table>";
		}

		echo $ReturnStr;
	}


	/*
	* Get title tag
	*/
	function TITLE_TAGS(){
		global $TAGS_OBJ, $MODULE_OBJ, $PATH_WRT_ROOT, $TAGS_OBJ_RIGHT, $CurrentPageArr, $sys_custom, $Lang;
		if($sys_custom['PowerClass']){
			$menu_obj = $this->MENU_TAGS();
		}

		if ($CurrentPageArr['SchoolCalendar'] && $sys_custom['PowerClass']) {
			foreach ($TAGS_OBJ as $key => $value) {
				if ($value[2] == 1) {
					$ReturnStr = "<span class=\"contenttitle\" style=\"vertical-align: bottom; \">".$value[0]."</span>";
				}
			}
		} else {		
			if (sizeof($TAGS_OBJ) < 2) {
				$ReturnStr = "<span class=\"contenttitle\" style=\"vertical-align: bottom; \">".$TAGS_OBJ[0][0]."</span>";
			} else {
				$ReturnStr = '<div id="Content_tab">';
				$ReturnStr .= '<ul>';
	
				/*
				for ($i = 0; $i < sizeof($TAGS_OBJ); $i++) {
					$current = ($TAGS_OBJ[$i][2] == 1) ? 'id="current"':'';
					$ReturnStr .= '<li '.$current.'><a href="'.$TAGS_OBJ[$i][1].'"><span>'.$TAGS_OBJ[$i][0].'</span></a></li>';
				}*/
	
				foreach ($TAGS_OBJ as $key => $value) {
					$current = ($value[2] == 1) ? 'id="current"':'';
					$ReturnStr .= '<li '.$current.'><a href="'.$value[1].'"><span>'.$value[0].'</span></a></li>';
				}
				$ReturnStr .= '</ul>';
				$ReturnStr .= '</div>';
			}
	
			if(sizeof($TAGS_OBJ_RIGHT))
			{
				$r = "
					<table width='100%' height=\"28\"  border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
					<tr>
						<td align=\"left\" valign=\"bottom\" width=\"100%\">". $ReturnStr ."</td>
						<td align=\"right\" valign=\"bottom\">".$TAGS_OBJ_RIGHT[0][0]."</td>
					</tr>
					</table>
				";
	
				$ReturnStr = $r;
			}
		}
		if($sys_custom['PowerClass']){
			echo $menu_obj.$ReturnStr;
		}else{
			echo $ReturnStr;
		}
	}

	
	function MENU_TAGS(){
		global $TAGS_OBJ, $MODULE_OBJ, $PATH_WRT_ROOT, $TAGS_OBJ_RIGHT, $CurrentPageArr, $sys_custom, $Lang, $ip20TopMenu, $i_InventorySystem;
		
		$returnStr = "";
		if(sizeof($MODULE_OBJ["menu"]) > 0 && in_array($MODULE_OBJ["title"], array($ip20TopMenu['ePayment'], $ip20TopMenu['eAttendance'], $ip20TopMenu['eAttendanceLesson'], $ip20TopMenu['iPortfolio'], $Lang['libms']['library'], $Lang['Header']['Menu']['StaffAccount'], $i_InventorySystem['eInventory'], $Lang['Header']['Menu']['eBooking']))){
			$returnStr .= '<span class="selectorWrapper modulePageSwitcher">';
			$returnStr .= '		<select name="status" onChange="if (this.value) window.location.href=this.value">';
			foreach($MODULE_OBJ["menu"] as $menu){ 
				if(isset($menu['Child'])){
					$returnStr .= '<optgroup label="'.$menu[0].'">';
					foreach($menu['Child'] as $m){
						$returnStr .= '<option value="'.($m[1]).'" '.(($m[2]==1)?"selected":"").'>'.$m[0].'</option>';
					}
					$returnStr .= '</optgroup>';
				}else{
					$returnStr .= '<option value="'.($menu[1]).'" '.(($menu[2]==1)?"selected":"").'>'.$menu[0].'</option>';
				}
			}
			$returnStr .= '		</select><br><br>';
			$returnStr .= '	</span>';
		}
		
		return $returnStr;
	}
	
	function TITLE_TAGS_2(){

		global $TAGS_OBJ, $MODULE_OBJ, $PATH_WRT_ROOT, $TAGS_OBJ_RIGHT;

		$ReturnStr = "";

		if (sizeof($TAGS_OBJ) < 2) {
			$ReturnStr = $TAGS_OBJ[0][0];
		}

		echo $ReturnStr;
	}
	
	function TITLE_TAGS_WRITING2(){
		global $w2_h_tabMenu;

		echo $w2_h_tabMenu;
	}




	/*
	* Get button
	*/
	function GET_BTN($ParTitle, $ParType, $ParOnClick="", $ParName="", $ParOtherAttribute="", $ParDisabled=0){
		$btnClick = ($ParOnClick!="") ? "onClick=\"$ParOnClick\"" : "";
		$ParName = ($ParName!="") ? "name=\"$ParName\" id=\"$ParName\"" : "";
		
		$DisabledTag = '';
		if($ParDisabled!=0){
			$Class = "formbutton_disable print_hide";
			$DisabledTag = "disabled";
		} else {
		    $Class = "formsubbutton";
		}
		
		$rx = "<input type=\"$ParType\" class=\"$Class\" $btnClick $ParName value=\"$ParTitle\" ";
		$rx .= " $ParOtherAttribute ";
		$rx .= "onMouseOver=\"this.className='$Class'\" $DisabledTag onMouseOut=\"this.className='$Class'\"/>";

		return $rx;
	}


	/*
	* Get action button
	*/
	function GET_ACTION_BTN($ParTitle, $ParType, $ParOnClick="", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass=""){
		$btnClick = ($ParOnClick!="") ? "onClick=\"$ParOnClick\"" : "";
		$ParName = ($ParName!="") ? "name=\"$ParName\" id=\"$ParName\"" : "";
		// Disabled By Ronald @ 2010-10-06
		//$ClassDisabled = ($ParDisabled!=0) ? "formbutton_disable" : "formbutton";
		
		$DisabledTag = '';
		if($ParDisabled!=0){
			$Class = "formbutton_disable print_hide $ParExtraClass";
			$DisabledTag = "disabled";
		} else {
			if($ParClass == "")
				$Class = "formbutton_v30 print_hide $ParExtraClass";
			else
				$Class = $ParClass;
		}
		/*
		$rx = "<input type=\"$ParType\" class=\"$ClassDisabled print_hide\" $btnClick $ParName value=\"$ParTitle\" ";
		$rx .= " $ParOtherAttribute ";
		$rx .= "onMouseOver=\"this.className='$ClassDisabled'\" onMouseOut=\"this.className='$ClassDisabled'\"/>";
		*/
		// Disabled By Ronald @ 2010-10-06
		//$rx ="<input type=\"$ParType\" $ParName $btnClick class=\"$ClassDisabled print_hide\" value=\"$ParTitle\" $ParOtherAttribute />";
		
		$rx ="<input type=\"$ParType\" $ParName $btnClick class=\"$Class\" value=\"$ParTitle\" $DisabledTag $ParOtherAttribute />";
		return $rx;
	}
	
	/*
	* Get small button (IP25)
	*/
	function GET_SMALL_BTN($ParValue, $ParType, $ParOnClick="", $ParName="", $ParOtherAttribute="", $OtherClass="", $ParTitle=''){
		$btnClick = ($ParOnClick!="") ? "onClick=\"$ParOnClick\"" : "";
		$ParName = ($ParName!="") ? "name=\"$ParName\" id=\"$ParName\"" : "";
		$titleParam = ($ParTitle!='')? ' title="'.$ParTitle.'" ' : '';
		
		$rx = "<input type=\"$ParType\" class=\"formsmallbutton $OtherClass\" $btnClick $ParName value=\"$ParValue\" ";
//        $rx = "<input type=\"$ParType\" class=\"formbutton_v30 $OtherClass\" $btnClick $ParName value=\"$ParValue\" ";
		$rx .= " $ParOtherAttribute ";
		$rx .= "onMouseOver=\"this.className='formsmallbuttonon $OtherClass'\" onMouseOut=\"this.className='formsmallbutton $OtherClass'\" $titleParam />";
//        $rx .= "$titleParam />";

		return $rx;
	}


	/*
	* Get exoprt button
	*/
	function GET_EXPORT_BTN($ParTitle, $ParType, $ParOnClick="", $ParName="", $ParOtherAttribute=""){
		$btnClick = ($ParOnClick!="") ? "onClick=\"$ParOnClick\"" : "";
		$ParName = ($ParName!="") ? "name=\"$ParName\" id=\"$ParName\"" : "";

		$rx .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td>";
		$rx .= "</td><td>";
		$rx .= "<input type='radio' id='export_b5' name='export_coding' value='b5'><span class='tabletext'><label for='export_b5'>Export in Big-5</label></span>";
		$rx .= "</td><td width=\"5\">&nbsp;";
		$rx .= "</td><td>";
		$rx .= "<input type='radio' id='export_utf' name='export_coding' value='utf' checked><span class='tabletext'><label for='export_utf'>Export in UTF</label></span>";
		$rx .= "</td></tr><tr><td colspan=\"4\" align=\"center\" height=\"10\">";
		$rx .= "</td></tr><tr><td colspan=\"4\" align=\"center\">";
		$rx .= "<input type=\"$ParType\" class=\"formbutton print_hide\" $btnClick $ParName value=\"$ParTitle\" ";
		$rx .= " $ParOtherAttribute ";
		$rx .= "onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"/>";
		$rx .= "</td></table>";

		return $rx;
	}


	function GET_LNK_PRINT_IP25($ParHref, $button_text="")
	{
		return $this->GET_LNK_PRINT($ParHref, $button_text, '', '', '', 0);
	}
	
	/*
	* Get action button (updated on 21 Aug 08 - added $button_text enabling customized button text)
	*/
	function GET_LNK_PRINT($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $i_PrinterFriendlyPage, $Lang;
		
		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
		
		$rx = "";
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{	
			
			
			$rx .= "<a href=\"$ParHref\" class=\"print $ParClass\" $onclick $ParOthers>";
				$rx .= ($button_text=="")? $Lang['Btn']['Print'] : $button_text;
			$rx .= "</a>";
		}
		else {
			/*
			$rx = "<a class=\"contenttool\" href=\"".$ParHref."\" $onclick $ParOthers>";
			$rx .= "<img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_print.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
			if ($button_text!="")
			{
				$rx .= $button_text;
			}
			else
			{
				$rx .= $i_PrinterFriendlyPage;
			}
			$rx .= "</a>";
			*/
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="print '.$ParClass.'" '.$onclick.' '.$ParOthers.'> '. ($button_text ? $button_text : $Lang['Btn']['Print']) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}
	
	/*
	 * Get email link
	 */
	function GET_LNK_EMAIL($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1)
	{
		global $PATH_WRT_ROOT, $button_new, $LAYOUT_SKIN;
		
		$rx = "";
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			
			$onclick = "";
			if ($ParOnClick != "")
				$onclick = "onclick=\"".$ParOnClick."\"";
			
			$rx .= "<a href=\"$ParHref\" class=\"new $ParClass\" $onclick $ParOthers>";
				$rx .= ($button_text=="")? $Lang['Btn']['New'] : $button_text;
			$rx .= "</a>";
		}
		else
		{
//			$rx .= "<a class=\"contenttool\" href=\"".$ParHref."\">";
//			$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/iMail/icon_compose.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
//			$rx .= ($button_text=="")? $button_new : $button_text;
//			$rx .= "</a>";
			
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="email"> '. ($button_text ? $button_text : $button_new) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}

	/*
	* Get new link
	*/
	function GET_LNK_NEW($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $button_new, $LAYOUT_SKIN;
		
		$rx = "";
		
		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
				
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			
			$rx .= "<a href=\"$ParHref\" class=\"new $ParClass\" $onclick $ParOthers>";
				$rx .= ($button_text=="")? $Lang['Btn']['New'] : $button_text;
			$rx .= "</a>";
		}
		else
		{
// 			$rx .= "<a class=\"contenttool ".$ParClass."\" href=\"".$ParHref."\" $onclick $ParOthers>";
// 			$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
// 			$rx .= ($button_text=="")? $button_new : $button_text;
// 			$rx .= "</a>";
			
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="new '.$ParClass.'" '.$onclick.' '.$ParOthers.'> '. ($button_text ? $button_text : $button_new) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}
	

	/*
	* Get add link
	*/
	function GET_LNK_ADD($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $button_add, $LAYOUT_SKIN;

		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
			
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			
			$rx .= "<a href=\"$ParHref\" class=\"new $ParClass\" $onclick $ParOthers>";
				$rx .= ($button_text=="")? $Lang['Btn']['Add'] : $button_text;
			$rx .= "</a>";
		}
		else
		{
// 			$rx = "<a class=\"contenttool $ParClass\" href=\"".$ParHref."\" $onclick $ParOthers>";
// 			$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_add.gif\" border=\"0\" align=\"absmiddle\"> ";
// 			$rx .= $button_add;
// 			$rx .= "</a>";
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="new '.$ParClass.'" '.$onclick.' '.$ParOthers.'> '. ($button_text ? $button_text : $button_add) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}

	/*
	* Get edit link
	*/
	function GET_LNK_EDIT($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $button_edit, $LAYOUT_SKIN;
		
// 		$rx = "<a class=\"tabletool\" href=\"".$ParHref."\">";
// 		$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_edit.gif\" border=\"0\" align=\"absmiddle\"> ";
// 		$rx .= $button_edit;
// 		$rx .= "</a>";
		
		# IP25 Standard
// 		$rx = '<div class="common_table_tool">';
		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
		$rx .= '<a href="'. $ParHref .'" class="tool_edit" '.$onclick.'>'. ($button_text ? $button_text : $button_edit) .'</a>';
// 		$rx .= '</div>';
		return $rx;
	}

	function GET_LNK_EXPORT_IP25($ParHref, $button_text="")
	{
		return $this->GET_LNK_EXPORT($ParHref, $button_text, '', '', '', 0);
	}

	/*
	* Get export link
	*/
	function GET_LNK_EXPORT($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $button_export, $LAYOUT_SKIN;
		
		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
		
		$rx = "";
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			
			$rx .= "<a href=\"$ParHref\" class=\"export\" $onclick $ParOthers> ";
				$rx .= ($button_text=="")? $Lang['Btn']['Export'] : $button_text;
			$rx .= "</a>";
		}
		else
		{
//			$rx = "<a class=\"contenttool\" href=\"".$ParHref."\" $onclick $ParOthers>";
//			$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
//			$rx .= $button_export;
//			$rx .= "</a>";
			
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
				$rx .= '<a href="'. $ParHref .'" class="export" '.$onclick.' '.$ParOthers.'> '. ($button_text ? $button_text : $button_export) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}
	
	/*
	* Get import link (updated on 5 Aug 08 - added $button_text enabling customized button text)
	*/
	function GET_LNK_IMPORT($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $button_import, $LAYOUT_SKIN;
		
		$rx = "";
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			
			$rx .= "<a href=\"$ParHref\" class=\"import\"> ";
				$rx .= ($button_text=="")? $Lang['Btn']['Import'] : $button_text;
			$rx .= "</a>";
		}
		else
		{
//			$rx .= "<a class=\"contenttool\" href=\"".$ParHref."\">";
//			$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
//			$rx .= ($button_text=="")? $button_import : $button_text;
//			$rx .= "</a>";

			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="import"> '. ($button_text ? $button_text : $button_import) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}

	/*
	* Get upload link
	*/
	function GET_LNK_UPLOAD($ParHref,$ParOnClick='',$button_text='', $ParClass='', $ParOthers='', $useThickBox=0){
		global $PATH_WRT_ROOT, $button_upload, $LAYOUT_SKIN;
		
		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
		
		if ($LAYOUT_SKIN=='2009a' && $useThickBox==1)
		{
			$rx .= "<a href=\"$ParHref\" class=\"upload\" $onclick $ParOthers> ";
				$rx .= ($button_text=="")? $button_upload : $button_text;
			$rx .= "</a>";
		}else
		{
		//	$rx = "<a class=\"contenttool\" href=\"".$ParHref."\" onclick=\"".$ParOnClick."\">";
		//	$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_upload.gif\" border=\"0\" align=\"absmiddle\"> ";
		//	$rx .= $button_upload;
		//	$rx .= "</a>";
			
			# IP25 Standard
			$rx = "<div class=\"Conntent_tool\">";
			$rx .= "<a href=\"". $ParHref ."\" class=\"upload\" $onclick $ParOthers> ".($button_text ? $button_text : $button_upload)."</a>";
			$rx .= "</div>";
		}
		return $rx;
	}

	/*
	* Get remove 20090714 yat
	*/
	function GET_LNK_REMOVE($ParHref="#")
	{
		global $PATH_WRT_ROOT, $button_delete, $LAYOUT_SKIN;

// 		$rx = "<a class=\"tabletool\" href=\"".$ParHref."\">";
// 		$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_delete.gif\" border=\"0\" align=\"absmiddle\"> ";
// 		$rx .= $button_delete;
// 		$rx .= "</a>";

		# IP25 Standard
// 		$rx = '<div class="common_table_tool">';
		$rx .= '<a href="'. $ParHref .'" class="tool_delete">'. $button_delete .'</a>';
// 		$rx .= '</div>';

		return $rx;
	}
	
	
	/*
	* Get remove all link
	*/
	function GET_LNK_REMOVEALL($ParHref){
		global $PATH_WRT_ROOT, $button_remove_all, $LAYOUT_SKIN;

		$rx = "<a class=\"contenttool\" href=\"".$ParHref."\">";
		$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_delete.gif\" border=\"0\" align=\"absmiddle\"> ";
		$rx .= $button_remove_all;
		$rx .= "</a>";

		return $rx;
	}
	
	/*
	* Get link with custom icon css(IP25)
	*/
	function GET_ACTION_LNK($ParHref, $ParTitle="", $ParOnClick="", $ParClass=""){
		global $PATH_WRT_ROOT, $Lang, $LAYOUT_SKIN;
		
		if ($ParTitle!="")
			$title = "title=\"$ParTitle\"";
			
		if ($ParOnClick!="")
			$onclick = "onclick=\"$ParOnClick\"";
		
		$rx = "";
		$rx .= '<span class="table_row_tool row_content_tool">';
			$rx .= '<a href="'.$ParHref.'" class="'.$ParClass.'" '.$title.' '.$onclick.'></a>';
		$rx .= '</span>';
		
		return $rx;

	}
	
	/*
	* Get delete link (IP25)
	*/
	function GET_LNK_DELETE($ParHref, $ParTitle="", $ParOnClick="", $ParClass="", $WithSpan=1){
		global $PATH_WRT_ROOT, $Lang, $LAYOUT_SKIN;
		
		if ($ParTitle!="")
			$title = "title=\"$ParTitle\"";
			
		if ($ParOnClick!="")
			$onclick = "onclick=\"$ParOnClick\"";
			
		if ($ParClass=='')
			$ParClass = 'delete_dim';
		
		$rx = "";
		$rx .= '<a href="'.$ParHref.'" class="'.$ParClass.'" '.$title.' '.$onclick.'></a>';
		
		if ($WithSpan == 1)
			$rx = '<span class="table_row_tool row_content_tool">'.$rx.'</span>';
		
		return $rx;

	}
	/*
	* Get move link (IP25)
	*/
	function GET_LNK_MOVE($ParHref, $ParTitle="", $WithSpan=1){
		global $PATH_WRT_ROOT, $Lang, $LAYOUT_SKIN;
		
		if ($ParTitle!="")
			$title = "title=\"$ParTitle\"";
			
		$rx = "";
		$rx .= '<a href="'.$ParHref.'" class="move_order_dim" '.$title.'></a>';
		
		if ($WithSpan == 1)
			$rx = '<span class="table_row_tool">'.$rx.'</span>';
		
		return $rx;
	}
	
	function GET_LNK_RESTORE($ParHref, $ParTitle="", $ParOnClick="", $ParClass="", $WithSpan=1){
		global $PATH_WRT_ROOT, $Lang, $LAYOUT_SKIN;
		
		if ($ParTitle!="")
			$title = "title=\"$ParTitle\"";
			
		if ($ParOnClick!="")
			$onclick = "onclick=\"$ParOnClick\"";
			
		if ($ParClass=='')
			$ParClass = 'restore_form_dim';
		
		$rx = "";
		$rx .= '<a href="'.$ParHref.'" class="'.$ParClass.'" '.$title.' '.$onclick.'></a>';
		
		if ($WithSpan == 1)
			$rx = '<span class="table_row_tool row_content_tool">'.$rx.'</span>';
		
		return $rx;

	}
	
	function GET_LNK_COPY($ParHref, $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1){
		global $PATH_WRT_ROOT, $button_new, $LAYOUT_SKIN;
		
		$rx = "";
		
		$onclick = "";
		if ($ParOnClick != "")
			$onclick = "onclick=\"".$ParOnClick."\"";
				
		if ($LAYOUT_SKIN=='2009a' && $useThickBox)
		{
			global $Lang;
			$rx .= "<a href=\"$ParHref\" class=\"copy $ParClass\" $onclick $ParOthers>";
				$rx .= ($button_text=="")? $Lang['Btn']['Copy'] : $button_text;
			$rx .= "</a>";
		}
		else
		{	
			# IP25 Standard
			$rx = '<div class="Conntent_tool">';
			$rx .= '<a href="'. $ParHref .'" class="copy '.$ParClass.'" '.$onclick.' '.$ParOthers.'> '. ($button_text ? $button_text : $Lang['Btn']['Copy']) .'</a>';
			$rx .= '</div>';
		}

		return $rx;
	}
	
	function Get_Thickbox_Link($Height, $Width, $ExtraClass, $Title, $OnClick, $InlineID="FakeLayer", $Content="", $LinkID='')
	{
		$rx = "";
		$rx .= '<a href="#TB_inline?height='.$Height.'&width='.$Width.'&inlineId='.$InlineID.'"
					class="'.$ExtraClass.' thickbox" title="'.$Title.'" onclick="'.$OnClick.'" id="'.$LinkID.'">'.$Content.'</a>';
		return $rx;
	}
	
	function Get_Thickbox_Div($Height, $Width, $ExtraClass, $Title, $OnClick, $InlineID="FakeLayer", $Content="", $DivID='', $LinkID='', $ExtraTags='')
	{
		if ($DivID != '')
			$id_tag = ' id="'.$DivID.'" ';
			
		$rx = "";
		$rx .= '<div class="table_row_tool" '.$id_tag.' '.$ExtraTags.'>'."\n";
			$rx .= $this->Get_Thickbox_Link($Height, $Width, $ExtraClass, $Title, $OnClick, $InlineID, $Content, $LinkID);
		$rx .= '</div>';
		
		return $rx;
	}
	
	function GET_LNK_ACTIVE($ParHref, $IsActive=1, $ParTitle="", $WithSpan=1, $ParID=''){
		global $PATH_WRT_ROOT, $Lang, $LAYOUT_SKIN;
		
		if ($ParTitle!="")
			$title = " title=\"$ParTitle\" ";
			
		if ($ParID!="")
			$id_tag = " id=\"$ParID\" name=\"$ParID\" ";
			
		if ($IsActive==1)
			$class = 'active_item';
		else
			$class = 'inactive_item';
			
		$rx = "";
		$rx .= '<a href="'.$ParHref.'" class="'.$class.'" '.$title.' '.$id_tag.'></a>';
		
		if ($WithSpan==1)
			$rx = '<span class="table_row_tool">'.$rx.'</span>';
		
		return $rx;
	}
	
	
	/*
	* Get import coding checkbox
	*/
	function GET_IMPORT_CODING_CHKBOX(){
		// comment by kenneth chung on 20090824, as this option is considered useless
		/*global $i_import_utf, $i_import_big5, $i_import_gb, $g_encoding_unicode, $intranet_default_lang;

		if ($g_encoding_unicode) {
			$original = ($intranet_default_lang == "gb") ? $i_import_gb : $i_import_big5;
			$ori_checked = ($g_encoding_unicode) ? "" : "checked";
			$utf_checked = (!$g_encoding_unicode) ? "" : "checked";

			$rx .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td>";
			$rx .= "</td><td>";
			$rx .= "<input type='radio' id='import_ori' name='import_coding' value='$intranet_default_lang' $ori_checked><span class='tabletext'><label for='import_ori'>$original</label></span>";
			$rx .= "</td><td width=\"5\">&nbsp;";
			$rx .= "</td><td>";
			$rx .= "<input type='radio' id='import_utf' name='import_coding' value='utf' $utf_checked><span class='tabletext'><label for='import_utf'>$i_import_utf</label></span>";
			$rx .= "</td></table>";
		}

		return $rx;*/
	}


	/*
	* Get system message
	*/
	function GET_SYS_MSG($ParMsg, $ParSpecialMsg = "", $width="") {

		global $i_con_msg_add, $i_con_msg_update, $i_con_msg_delete, $i_con_msg_email, $i_con_msg_duplicate_item;
		global $i_con_msg_password1, $i_con_msg_password2, $i_con_msg_password3;
		global $i_con_msg_email1, $i_con_msg_email2, $i_con_msg_archive;
		global $i_con_msg_import_success, $i_con_msg_date_wrong, $i_con_msg_admin_modified;
		global $i_con_msg_suspend, $i_con_msg_cannot_edit_poll, $i_con_msg_photo_delete;
		global $i_con_msg_import_failed, $i_con_msg_import_failed2, $i_con_msg_import_invalid_login, $i_con_msg_user_add_failed;
		global $i_con_msg_date_startend_wrong_alert, $i_con_msg_date_startend_wrong;
		global $i_con_msg_add_form_failed, $i_con_msg_delete_part, $i_con_msg_enrollment_next;
		global $i_con_msg_enrollment_confirm, $i_con_msg_enrollment_lottery_completed, $i_con_msg_subject_leader_updated, $i_con_msg_quota_exceeded;
		global $i_con_msg_add_failed, $i_con_msg_update_failed, $i_con_msg_delete_failed, $i_con_msg_sports_house_group_existed;
		global $i_con_msg_notify, $i_con_msg_notify_failed, $i_con_msg_hide_notify, $i_con_msg_hide_notify_failed, $i_con_msg_report_generate, $i_con_msg_report_generate_failed, $i_con_msg_data_transit, $i_con_msg_data_transit_failed;
		global $PATH_WRT_ROOT;
		global $i_con_msg_voted, $i_con_msg_upload, $i_con_msg_upload_failed, $i_con_msg_wrong_csv_header;
		global $i_con_msg_delete_following_failed, $i_con_msg_copy, $i_con_msg_copy_failed_dup_name, $i_con_msg_copy_failed, $ec_iPortfolio, $i_con_msg_activate;
		global $i_con_msg_add_overlapped, $i_con_msg_update_overlapped, $i_con_msg_add_past, $i_con_msg_update_past, $i_con_msg_add_past_overlapped, $i_con_msg_update_incompatible_form, $i_con_msg_update_incompatible_vacancy, $i_con_msg_session_not_available, $i_con_msg_session_past, $i_con_msg_session_full, $i_con_msg_session_assigned_before, $i_con_msg_update_inapplicable_form, $i_con_msg_update_already_in_session, $i_con_msg_update_not_enough_session, $i_con_msg_cancel, $i_con_msg_cancel_failed, $i_con_msg_cancel_past, $i_con_msg_session_not_available2;
		global $i_con_msg_approve, $i_con_msg_reject, $i_con_msg_release, $i_con_msg_unrelease, $i_con_msg_waive, $i_con_msg_unwaive, $i_con_msg_no_delete_acc_record, $i_con_msg_no_student_select, $i_con_msg_duplicate_students, $i_con_msg_not_all_approve, $i_con_msg_not_all_reject, $i_con_msg_not_all_release, $i_con_msg_no_record_release, $i_con_msg_not_all_approve, $i_con_msg_no_record_approve, $i_con_msg_not_all_unrelease, $i_con_msg_no_record_unrelease, $i_con_msg_no_record_reject;
		global $i_con_msg_redeem_success, $i_con_msg_redeem_failed, $i_con_msg_cancel_redeem_success, $i_con_msg_cancel_redeem_failed,$i_con_msg_import_header_failed,$i_con_msg_grade_generated,$i_con_msg_wrong_header, $i_con_msg_sports_ranking_generated;
		global $i_con_msg_import_no_record, $i_con_msg_report_archive, $i_con_msg_report_archive_failed;
		global $i_con_msg_survey_add, $i_con_msg_survey_update, $i_con_msg_survey_delete, $i_con_msg_homework_clear;
		global $i_con_msg_New_Record_Delete;
		global $i_con_msg_promotion_generate, $i_con_msg_promotion_generate_failed, $i_con_msg_GroupLogo_PhotoWarning;
		global $Lang;

        $widthTag = $width ? "width=$width" : "";

        if (($ParMsg != "")||($ParSpecialMsg != ""))
		{
			$ReturnStr .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class=\"systemmsg\" ". $widthTag .">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td nowrap='nowrap'>&nbsp; ";
			if ($ParMsg != "")
			{
				$ReturnStr .= ${"i_con_msg_".$ParMsg};
			} else
			{
				$ReturnStr .= $ParSpecialMsg;
			}
			$ReturnStr .= " &nbsp;</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table>\n";
		}

		return $ReturnStr;
	}


	/*
	* Get text area
	*/
	function GET_TEXTAREA($taName, $taContents, $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='', $class='', $taID='', $CommentMaxLength=''){
		$initialRows = 2;
		$initialRows = (trim($taContents)=="") ? $initialRows : $taRows;
		
		$taID = ($taID=='')? $taName : $taID;
		
		if ($readonly == 1)
			$readonly = " readonly ";
		else {
			$readonly = " ";
		}
		
		if (!empty($CommentMaxLength)) {
			$limitText = 'onkeyup="limitText(this, '.$CommentMaxLength.');"';
		}	
			
		$rx = "<textarea class=\"tabletext $class\" name=\"{$taName}\" ID=\"{$taID}\" cols=\"{$taCols}\" rows=\"{$initialRows}\" wrap=\"virtual\" $other $limitText onFocus=\"this.rows={$taRows}; $OnFocus\" $readonly >{$taContents}</textarea>";

		return $rx;
	}
	
	# JQuery Datepicker generator
	function GET_DATE_PICKER($Name,$DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum") {
		global $Defined,$PATH_WRT_ROOT,$image_path,$LAYOUT_SKIN,$Lang;
		
		$DateFormat = ($DateFormat == "")? "yy-mm-dd":$DateFormat;
		if(!$CanEmptyField)
			$DefaultValue = ($DefaultValue == "")? date('Y-m-d'):$DefaultValue;
		$ID = ($ID == "")? $Name:$ID;
		$WarningLayerID = ($ExtWarningLayerID == "")? 'DPWL-'.$ID:$ExtWarningLayerID;
		$Disabled = ($Disable)? 'disabled' : '';
		
		$x = "";
		if(!$CanEmptyField)
			$x .= '<input type=text name="'.$Name.'" id="'.$ID.'" value="'.intranet_htmlspecialchars($DefaultValue).'" size=10 maxlength=10 class="'.$cssClass.'" '.$OtherMember.' onkeyup="Date_Picker_Check_Date_Format(this,\''.$WarningLayerID.'\',\''.$ExtWarningLayerContainer.'\');" '.$Disabled.'>';
		else
			$x .= '<input type=text name="'.$Name.'" id="'.$ID.'" value="'.intranet_htmlspecialchars($DefaultValue).'" size=10 maxlength=10 class="'.$cssClass.'" '.$OtherMember.' onkeyup="Date_Picker_Check_Date_Format_CanEmpty(this,\''.$WarningLayerID.'\',\''.$ExtWarningLayerContainer.'\');" '.$Disabled.'>';
		if ($ExtWarningLayerID == "")
			$x .= '<span style="color:red;" id="'.$WarningLayerID.'"></span>';
		$x .= "\n";
		
		if ($Defined<1) {
			if($SkipIncludeJS!=1)
			{
				$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.js"></script>
						<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.css" type="text/css" />';
			}			

					$x .= '<script>
							$.datepick.setDefaults({
								showOn: \'both\', 
								buttonImageOnly: true, 
								buttonImage: \''.$image_path.'/'.$LAYOUT_SKIN.'/icon_calendar_off.gif\', 
								buttonText: \'Calendar\'});
						
						function Date_Picker_Check_Date_Format(DateObj,WarningLayer,WarningLayerContainer) { ';
						
						$x .= 'if (!check_date_without_return_msg(DateObj)) {';
						
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\''.$Lang['General']['InvalidDateFormat'].'\'); 
							}
							else {';
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'none\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\'\');
							}
						}
						
						function Date_Picker_Check_Date_Format_CanEmpty(DateObj,WarningLayer,WarningLayerContainer) { 
							';
						
						$x .= 'if (!check_date_allow_null_30(DateObj)) {';
						
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\''.$Lang['General']['InvalidDateFormat'].'\'); 
							}
							else {';
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'none\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\'\');
							}
						}
						
						</script>
						';
		}
		$x .= "<script>
						$('input#".$ID."').ready(function(){
							$('input#".$ID."').datepick({
								".(($MaskFunction != "")? 'beforeShowDay: '.$MaskFunction.',':'')."
								dateFormat: '".$DateFormat."',
								dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
								changeFirstDay: false,
								firstDay: 0,
								onSelect: function(dateText, inst) {
				";
							if($CanEmptyField)
									$x .= "Date_Picker_Check_Date_Format_CanEmpty(document.getElementById('".$ID."'),'".$WarningLayerID."','".$ExtWarningLayerContainer."');";
							else		
									$x .= "Date_Picker_Check_Date_Format(document.getElementById('".$ID."'),'".$WarningLayerID."','".$ExtWarningLayerContainer."');";
							
									$x .= $OnDatePickSelectedFunction."
									}
								});
							});
					</script>
					";
		
		$Defined++;
		//debug_pr($ID);
		return $x;
	}
	
	function Get_Time_Selection($ID_Name, $DefaultTime='', $others_tab='',$hideSecondSeletion='', $intervalArr=array(1,1,1), $parObjId=false)
	{
		list($hr, $min, $sec) = explode(":", $DefaultTime);
		if(!is_array($others_tab))
		{
			$tmp = $others_tab;
			$others_tab = array();
			for($i=0; $i<3; $i++) $others_tab[$i] = $tmp;			
		}
		
		if($hideSecondSeletion){
			$others_tab[2] .= 'style="display:none"';
		}
		
		if ($parObjId === false) {
			$hourSelId = $ID_Name."_hour";
			$minSelId = $ID_Name."_min";
			$secSelId = $ID_Name."_sec";
		}
		else {
			$hourSelId = $ID_Name."_hour[".$parObjId."]";
			$minSelId = $ID_Name."_min[".$parObjId."]";
			$secSelId = $ID_Name."_sec[".$parObjId."]";
		}
		
		$time_sel[] = $this->Get_Time_Selection_Box($hourSelId, "hour", $hr, $others_tab[0], $intervalArr[0]);
		$time_sel[] = $this->Get_Time_Selection_Box($minSelId, "min", $min, $others_tab[1], $intervalArr[1]);
		$time_sel[] = $this->Get_Time_Selection_Box($secSelId, "sec", $sec, $others_tab[2], $intervalArr[2]);
		
		if($hideSecondSeletion){
			$return = $time_sel[0].':'.$time_sel[1].''.$time_sel[2];
		}else{
			$return = $time_sel[0].':'.$time_sel[1].':'.$time_sel[2];
		}

		return $return;
	}
	
	# DHTML Calendar for Date Selection
	function GET_CALENDAR($formField, $dateField, $moreHandler="", $hideLayer=true){
		global $number_calendar_used_before, $image_path, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$img_name = "calendar_img".$number_calendar_used_before;

		$rx = "<a href='javascript:calObj.show()'><img id='{$img_name}' name='{$img_name}' onMouseOver=\"calObj.setDate({$formField}.{$dateField}.value);setReceiver({$formField}.{$dateField});MM_swapImage('{$img_name}','','{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" src='{$image_path}/{$LAYOUT_SKIN}/icon_calendar_off.gif' border='0' align='absmiddle' hspace='1' /></a>\n";

		if ($number_calendar_used_before<1)
		{
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/browser_sniffer.js'></script>\n";
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/dyn_calendar.js'></script>\n";
			$rx .= "<script language='JavaScript' type='text/javascript'>\n";
			$rx .= "function selDate(date, month, year, myType)\n";
			$rx .= "{\n";
			$rx .= "if (typeof(document.$formField)!='undefined')\n";
			$rx .= "{\n";
			$rx .= "var selMonth = (parseInt(month)<10) ? '0'+parseInt(month) : parseInt(month);\n";
			$rx .= "var selDate = (parseInt(date)<10) ? '0'+parseInt(date) : parseInt(date);\n";
			$rx .= "calReceiver.value = parseInt(year)+'-'+selMonth+'-'+selDate;\n";
			if ($hideLayer) {
				$rx .= "calObj._hideLayer();\n";
			}
			$rx .= $moreHandler."\n";
			$rx .= "}\n";
			$rx .= "}\n";
			$rx .= "var calReceiver = null;\n";
			$rx .= "function setReceiver(myObj)\n";
			$rx .= "{\n";
			$rx .= "calReceiver = myObj;\n";
			$rx .= "}\n";
			$rx .= "var is_big5 = ".(($ck_default_lang=="chib5") ? "1" : "0") .";\n";
			$rx .= "calObj = new dynCalendar('calObj', 'selDate', '{$image_path}/{$LAYOUT_SKIN}/');\n";
			$rx .= "calObj.setMonthCombo(0);\n";
			$rx .= "calObj.setYearCombo(0);\n";
			$rx .= "calObj.setOffsetX(-8);\n";
			$rx .= "calObj.setOffsetY(5);\n";
			$rx .= "</script>\n";
			$rx .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
		}

		$number_calendar_used_before += 1;

		return $rx;
	}

	# DHTML List for Item Selection
	function GET_LIST($formField, $listField, $inputField, $moreHandler=""){
		global $number_prelist_used_before, $image_path, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$img_name = "list_img".$number_prelist_used_before;

		//$rx = "<a href='javascript:ListObj.show()'><img id='{$img_name}' name='{$img_name}' onMouseOver=\"MM_swapImage('{$img_name}','','{$image_path}/{$LAYOUT_SKIN}/icon_pre-set_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" src='{$image_path}/{$LAYOUT_SKIN}/icon_pre-set_off.gif' border='0' align='absmiddle' hspace='1' /></a>\n";
		$rx = "<a href='javascript:ListObj.show()'><img id='{$img_name}' name='{$img_name}' onMouseOver=\"MM_swapImage('{$img_name}','','{$image_path}/{$LAYOUT_SKIN}/icon_pre-set_on.gif',1);ListObj.setTargetObj('{$inputField}');ListObj.setListArray('{$listField}');\" onMouseOut=\"MM_swapImgRestore()\" src='{$image_path}/{$LAYOUT_SKIN}/icon_pre-set_off.gif' border='0' align='absmiddle' hspace='1' /></a>\n";

		if ($number_prelist_used_before<1)
		{
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/browser_sniffer.js'></script>\n";
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/dyn_list.js'></script>\n";
			$rx .= "<script language='JavaScript' type='text/javascript'>\n";
			$rx .= "ListObj = new dynList('ListObj','', '{$image_path}/{$LAYOUT_SKIN}/');\n";
			$rx .= "ListObj.setListArray('{$listField}');\n";
			$rx .= "ListObj.setTargetObj('{$inputField}');\n";
			$rx .= "ListObj.setOffsetX(-155);\n";
			$rx .= "ListObj.setOffsetY(-10);\n";
			$rx .= "</script>\n";
			$rx .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
		}

		$number_prelist_used_before += 1;

		return $rx;
	}
	
	function Get_Search_Box_Div($Par_ID_Name, $ParValue='', $Others=''){
		$x = '';
		$x .= '<div class="Conntent_search">';
			$x .= '<input id="'.$Par_ID_Name.'" name="'.$Par_ID_Name.'" type="text" value="'.intranet_htmlspecialchars($ParValue).'" '.$Others.'/>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Warning_Message_Box($title, $content, $others="")
	{
		global $Lang;
		
		if ($title=='') {
			$title = '<font color="red">'.$Lang['General']['Warning'].'</font>';
		}
		
		if (is_array($content)) {
			$WarningArr = $content;
			$numOfWarning = count($WarningArr);
			
			$content = '';
			$content .= '<table style="width:100%;">';
			for ($i=0; $i<$numOfWarning; $i++) {
				$thisWarningMsg = $WarningArr[$i];
				$thisIndex = $i + 1;
				
				$content .= '<tr>'."\n";
					$content .= '<td style="width:20px; vertical-align:top;">'.$thisIndex.'. </td>'."\n";
					$content .= '<td>'.$thisWarningMsg.'</td>'."\n";
				$content .= '</tr>';
			}
			$content .= '</table>';
		} 
			
		$showmsg =  "<fieldset class='instruction_box_v30'>";
		$showmsg .=  "<legend>";
		$showmsg .=  $title;
		$showmsg .=  "</legend>";
		$showmsg .=  "<div $others>";
		$showmsg .=  $content;
		$showmsg .=  "</div>";
		$showmsg .=  "</fieldset>";

		return $showmsg;
	}
	
	function Get_Thickbox_Edit_Div($ParID, $ParName, $ParClass, $ParContent)
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		$x = '';
		$x .= '<div id="'.$ParID.'" name="'.$ParName.'" onmouseover="Show_jEditable_Edit_Background(this, \''.$PATH_WRT_ROOT.'\', \''.$LAYOUT_SKIN.'\');" 
					onmouseout="Hide_jEditable_Edit_Background(this, \''.$PATH_WRT_ROOT.'\', \''.$LAYOUT_SKIN.'\');" class="'.$ParClass.'">';
			$x .= $ParContent;
		$x .= '</div>';

		return $x;
	}
	
	function Get_Thickbox_Warning_Msg_Div($ParID, $ParMsg='', $Class='')
	{
		$x .= '<div id="'.$ParID.'" class="'.$Class.'" style="display:none;color:red;">';
			$x .= $ParMsg;
		$x .= '</div>';

		return $x;
	}
	
	function Get_Checkbox($ID, $Name, $Value, $isChecked=0, $Class='', $Display='', $Onclick='', $Disabled='',$isIndented=0,$specialLabel="")
	{
		$onclick_par = '';
		if ($Onclick != "")
			$onclick_par = 'onclick="'.$Onclick.'"';
			
		$class_par = '';
		if ($Class != "")
			$class_par = 'class="'.$Class.'"';
			
		$checked = '';
		if ($isChecked)
			$checked = 'checked';
			
		$disabled = '';
		if ($Disabled)
			$disabled = 'disabled';
		
		$chkbox = '';
		if($isIndented){
			$chkbox .= '<table>';
			$chkbox .= '<tr>';
			$chkbox .= '<td style="vertical-align: top;"><input type="checkbox" value="'.$Value.'" id="'.$ID.'" name="'.$Name.'" '.$class_par.' '.$onclick_par.' '.$checked.' '.$disabled.'/></td>';
			if($specialLabel != ""){
				$chkbox .= '<td style="vertical-align: top;"><label for="'.$ID.'"><b>'.$specialLabel.'</b></label></td>';
			}
			$chkbox .= '<td><label for="'.$ID.'"> '.$Display.'</label></td>';
			$chkbox .= '</tr>';
			$chkbox .= '</table>';			
		}else{
			$chkbox .= '<input type="checkbox" value="'.intranet_htmlspecialchars($Value).'" id="'.$ID.'" name="'.$Name.'" '.$class_par.' '.$onclick_par.' '.$checked.' '.$disabled.'/>';
			
			if ($Display != "")
				$chkbox .= '<label for="'.$ID.'"> '.$Display.'</label>';
		}
		return $chkbox;
	}
	
	function Get_Checkbox_Table($checkboxClass, $checkboxAry, $itemPerRow=5, $defaultCheck=true) {
		global $Lang;
		
		$numOfCheckbox = count((array)$checkboxAry);
		$columnWidth = floor(100 / $numOfCheckbox);
		
		$checkAllId = $checkboxClass.'_global';
		$checkAllName = $checkboxClass.'_global';
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr><td colspan="'.($itemPerRow + 1).'" style="border:0px;">'."\n";
				$x .= $thisCheckbox = $this->Get_Checkbox($checkAllId, $checkAllName, $value='', $defaultCheck, $thisClass='', $Lang['Btn']['SelectAll'], "Check_All_Options_By_Class('".$checkboxClass."', this.checked);");
			$x .= '</td></tr>'."\n";
				
				for ($i=0; $i<$numOfCheckbox; $i++) {
					$_id = $checkboxAry[$i][0];
					$_name = $checkboxAry[$i][1];
					$_val = $checkboxAry[$i][2];
					$_checked = $checkboxAry[$i][3];
					$_class = $checkboxAry[$i][4];
					$_displayLang = $checkboxAry[$i][5];
					$_onclick = $checkboxAry[$i][6];
					
					$_onclickAttr = "Uncheck_SelectAll('".$checkAllId."', this.checked);";
					if ($_onclick != '') {
						$_onclickAttr .= ' '.$_onclick;
					}
					
					$_paddingLeft = '';
					if ($i % $itemPerRow == 0) {
						$_paddingLeft = 'padding-left:20px;';
						$x .= '<tr>'."\n";
					}
						
							$x .= '<td style="width:'.$columnWidth.'%; border:0px; padding-top:0px; padding-bottom:0px; '.$_paddingLeft.'">'."\n";
								$x .= $this->Get_Checkbox($_id, $_name, $_val, $_checked, $_class.' '.$checkboxClass, $_displayLang, $_onclickAttr);
							$x .= '</td>'."\n";
					
					if ($i % $itemPerRow == ($itemPerRow - 1) || ($i == ($numOfCheckbox - 1))) {
						$x .= '</tr>'."\n";
					}
				}
				
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Radio_Button($ID, $Name, $Value, $isChecked=0, $Class="", $Display="", $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="")
	{
		$onclick_par = '';
		if ($Onclick != "")
			$onclick_par = 'onclick="'.$Onclick.'"';
			
		$class_par = '';
		if ($Class != "")
			$class_par = 'class="'.$Class.'"';
			
		$checked = '';
		if ($isChecked)
			$checked = 'checked="checked"';
		
		if($isDisabled)
			$disabled = 'disabled="disabled"';
		
		$chkbox = '';
		if($isIndented){
			$chkbox .= '<table>';
			$chkbox .= '<tr>';
			$chkbox .= '<td style="vertical-align: top;"><input type="radio" value="'.intranet_htmlspecialchars($Value).'" id="'.$ID.'" name="'.$Name.'" '.$class_par.' '.$onclick_par.' '.$checked.' '.$disabled.'/></td>';
			if($specialLabel != ""){
				$chkbox .= '<td style="vertical-align: top;"><label for="'.$ID.'"><b>'.$specialLabel.'</b></label></td>';
			}
			$chkbox .= '<td><label for="'.$ID.'"> '.$Display.'</label></td>';
			$chkbox .= '</tr>';
			$chkbox .= '</table>';			
		}else{
			$chkbox .= '<input type="radio" value="'.intranet_htmlspecialchars($Value).'" id="'.$ID.'" name="'.$Name.'" '.$class_par.' '.$onclick_par.' '.$checked.' '.$disabled.'/>';
			
			if ($Display != "")
				$chkbox .= '<label for="'.$ID.'"> '.$Display.'</label>';
		}
				
		return $chkbox;
	}
	
	function Get_Multiple_Selection_Box($InfoArr, $Id, $Name, $Size='20', $fix_width=1, $selected_ary=array())
	{
		$width_style = $fix_width ? ' style="width:99%" ' : '';
		
		$x = '';
		$x .= '<select name="'.$Name.'" id="'.$Id.'" size="'.$Size.'" '. $width_style .' multiple="true">';
		foreach ($InfoArr as $key => $value)
		{
			$key = intranet_htmlspecialchars($key);
			$selected = in_array($key, $selected_ary) ? " selected " : "";
			$x .= '<option value="'.$key.'" '. $selected .'>'.$value.'</option>';
		}
		$x .= '</select>';
		
		return $x;
	}
	
	function Get_Time_Selection_Box($name, $type, $selvalue, $others_tab='', $interval=1, $id='')
	{
		switch ($type){
			case 'hour':
				$start = 0;
				$end = 24;
				break;
			case 'min':
			case 'sec':
				$start = 0;
				$end = 60;
				break;
			default:
				return '';
		}
		
		if ($id == '') {
			$id = $name;
		}
		$str = '<select id="'.$id.'" name="'.$name.'" '.$others_tab.'>';
		for($i=$start ; $i<$end ; $i++){
			if ($i % $interval != 0) {
				continue;
			}
			$display_val = str_pad($i, 2, 0, STR_PAD_LEFT);
			$str .= '<option value="'.$i.'" '.(($i == $selvalue) ? 'selected' : '').'>'.$display_val.'</option>';
		}
		$str .= '</select>';
		return $str;
	}
	
	function Get_Number_Selection($ID_Name, $MinValue, $MaxValue, $SelectedValue='', $Onchange='', $noFirst=0, $isAll=0, $FirstTitle='', $Disabled=0)
	{
		global $Lang;
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = ' onchange="'.$Onchange.'" ';
			
		$disabled = '';
		if ($Disabled == 1)
			$disabled = ' disabled ';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$disabled;
		
		for($i=$MinValue; $i<=$MaxValue; $i++)
			$selectArr[$i] = $i;																																																																					
		
		if ($FirstTitle=='')
			$FirstTitle = ($isAll)? $Lang['Btn']['All'] : $Lang['Btn']['Select'];
			
		$FirstTitle = Get_Selection_First_Title($FirstTitle);
		$NumberSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedValue, $isAll, $noFirst, $FirstTitle);
		
		return $NumberSelection;
	}
	
	function Get_View_Icon($ParHref, $ParOnClick='', $ButtonText='')
	{
		if ($ParOnClick!="")
			$onclick = "onclick=\"$ParOnClick\"";
			
		$x = '';
		$x .= "<a href=\"$ParHref\" class=\"tablelink\" $onclick>\n";
			$x .= $this->Get_View_Image();
			$x .= $ButtonText;
		$x .= "</a>\n";
		
		return $x;
	}
	
	function Get_View_Image()
	{
		global $image_path, $LAYOUT_SKIN;
		
		$x = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">\n";
		return $x;
	}
	
	function Get_Photo_Icon($ParHref, $ParOnClick='', $ButtonText='')
	{
		global $image_path, $LAYOUT_SKIN;
		
		if ($ParOnClick!="")
			$onclick = "onclick=\"$ParOnClick\"";
			
		$x = '';
		$x .= "<a href=\"$ParHref\" class=\"tablelink\" $onclick>\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">\n";
			$x .= $ButtonText;
		$x .= "</a>\n";
		
		return $x;
	}
	
	function Get_Apply_All_Icon($Href, $Title='', $OtherTag='')
	{
		if ($Title != '')
			$title_tag = ' title="'.intranet_htmlspecialchars($Title).'" ';
			
		return '<div class="table_row_tool" '.$OtherTag.'><a class="icon_batch_assign" href="'.$Href.'" '.$title_tag.'></a></div>';
	}
	
	function Get_Show_Option_Link($Href, $Style='', $IconText='', $ID='')
	{
		global $image_path, $LAYOUT_SKIN, $Lang;
		
		$style_tag = '';
		if ($Style != '')
		{
			$style_tag = ' style="'.$Style.'" ';
		}
		
		$IconText = ($IconText=='')? $Lang['Btn']['ShowOption'] : $IconText;
		$x = '';
		$x .= '<a id="'.$ID.'" href="'.$Href.'" class="contenttool" '.$style_tag.'>'."\n";
			$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle" />'."\n";
			$x .= $IconText."\n";
		$x .= '</a>'."\n";
		return $x;
	}
	
	function Get_Hide_Option_Link($Href, $Style='', $IconText='', $ID='')
	{
		global $image_path, $LAYOUT_SKIN, $Lang;
		
		$style_tag = '';
		if ($Style != '')
		{
			$style_tag = ' style="'.$Style.'" ';
		}
		
		$IconText = ($IconText=='')? $Lang['Btn']['HideOption'] : $IconText;
		$x = '';
		$x .= '<a id="'.$ID.'" href="'.$Href.'" class="contenttool" '.$style_tag.'>'."\n";
			$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle" />'."\n";
			$x .= $IconText."\n";
		$x .= '</a>'."\n";
		return $x;
	}
	
	/*
		 * $ColumnTitleArr		 	Array of Title
		 * $ColumnPropertyArr 		Stores the property of each title of the export column
		 * 							1: Required, 2: Reference, 3: Optional
		 */
	function Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr='')
	{
		global $Lang;
		
		$ColumnDisplayArr = array();
		$numOfColumn = count($ColumnTitleArr);
		for ($i=0; $i<$numOfColumn; $i++) {
			$thisTitle = $ColumnTitleArr[$i];
			$thisProperty = $ColumnPropertyArr[$i];
			$thisRemarks = $RemarksArr[$i];
			
			if ($thisProperty == 1) {
				$thisTitle = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Required'].'</span>'.$thisTitle;
			}
			else if ($thisProperty == 2) {
				$thisTitle = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Reference'].'</span>'.$thisTitle;
			}
			else if ($thisProperty == 3) {
				//$thisTitle = $thisTitle;
			}
			else if ($thisProperty == 4) {
				$thisTitle = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Other'].'</span>'.$thisTitle;
			}
			$fieldletter = numberToLetter(($i+1), true);
			$ColumnDisplayArr[] = $Lang['General']['ImportArr']['Column'].' '.$fieldletter.' : '.$thisTitle.' '.$thisRemarks;
			//$ColumnDisplayArr[] = $Lang['General']['ImportArr']['Column'].' '.($i+1).' : '.$thisTitle.' '.$thisRemarks;
		}
		
		return implode('<br />', $ColumnDisplayArr);
	}
	
	function GET_CALENDAR_INVENTORY($formField, $dateField, $moreHandler="", $masterField){
		global $number_calendar_used_before, $image_path, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$img_name = "calendar_img".$number_calendar_used_before;
		$setdate .="calObjAll.setDate({$formField}.".$masterField.".value); setReceiver({$formField}.".$masterField.");";
		$value = "document.{$formField}.".$masterField.".value = parseInt(year)+'-'+selMonth+'-'+selDate;\n";
		for($a=0;$a<sizeof($dateField);$a++)
		{
			$setdate .="calObjAll.setDate({$formField}.item_warranty_expiry_date_{$dateField[$a]}.value); setReceiver({$formField}.item_warranty_expiry_date_{$dateField[$a]});";
			$value .= "document.{$formField}.item_warranty_expiry_date_{$dateField[$a]}.value = parseInt(year)+'-'+selMonth+'-'+selDate;\n";
		}
		$rx = "<a href='javascript:calObjAll.show()'><img id='{$img_name}' name='{$img_name}' onMouseOver=\"".$setdate."MM_swapImage('{$img_name}','','{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" src='{$image_path}/{$LAYOUT_SKIN}/icon_calendar_off.gif' border='0' align='absmiddle' hspace='1' /></a>\n";
		if ($number_calendar_used_before<1)
		{
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/browser_sniffer.js'></script>\n";
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/dyn_calendar.js'></script>\n";
			$rx .= "<script language='JavaScript' type='text/javascript'>\n";
			$rx .= "function selDateAll(date, month, year, myType)\n";
			$rx .= "{\n";
			$rx .= "if (typeof(document.$formField)!='undefined')\n";
			$rx .= "{\n";
			$rx .= "var selMonth = (parseInt(month)<10) ? '0'+parseInt(month) : parseInt(month);\n";
			$rx .= "var selDate = (parseInt(date)<10) ? '0'+parseInt(date) : parseInt(date);\n";
			$rx .=$value;
			$rx .= "calObjAll._hideLayer();\n";
			$rx .= $moreHandler."\n";
			$rx .= "}\n";
			$rx .= "}\n";
			$rx .= "function selDate(date, month, year, myType)\n";
			$rx .= "{\n";
			$rx .= "if (typeof(document.$formField)!='undefined')\n";
			$rx .= "{\n";
			$rx .= "var selMonth = (parseInt(month)<10) ? '0'+parseInt(month) : parseInt(month);\n";
			$rx .= "var selDate = (parseInt(date)<10) ? '0'+parseInt(date) : parseInt(date);\n";
			$rx .= "calReceiver.value = parseInt(year)+'-'+selMonth+'-'+selDate;\n";
			$rx .= "calObj._hideLayer();\n";
			$rx .= $moreHandler."\n";
			$rx .= "}\n";
			$rx .= "}\n";
			$rx .= "var calReceiver = null;\n";
			$rx .= "function setReceiver(myObj)\n";
			$rx .= "{\n";
			$rx .= "calReceiver = myObj;\n";
			$rx .= "}\n";
			$rx .= "var is_big5 = ".(($ck_default_lang=="chib5") ? "1" : "0") .";\n";
			$rx .= "calObjAll = new dynCalendar('calObjAll', 'selDateAll', '{$image_path}/{$LAYOUT_SKIN}/');\n";
			$rx .= "calObjAll.setMonthCombo(0);\n";
			$rx .= "calObjAll.setYearCombo(0);\n";
			$rx .= "calObjAll.setOffsetX(-8);\n";
			$rx .= "calObjAll.setOffsetY(5);\n";
			$rx .= "calObj = new dynCalendar('calObj', 'selDate', '{$image_path}/{$LAYOUT_SKIN}/');\n";
			$rx .= "calObj.setMonthCombo(0);\n";
			$rx .= "calObj.setYearCombo(0);\n";
			$rx .= "calObj.setOffsetX(-8);\n";
			$rx .= "calObj.setOffsetY(5);\n";
			$rx .= "</script>\n";
			$rx .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
		}

		$number_calendar_used_before += 1;

		return $rx;
	}

	function GET_DATE_FIELD($ParID, $ParFormObj, $ParFieldName, $ParFieldValue = 0, $ParUseCalendar = 1, $moreHandler = "") {

		if ($ParFieldValue == 0) $ParFieldValue = "yyyy-mm-dd"; //date("Y-m-d");

		$calendar = ($ParUseCalendar) ? $this->GET_CALENDAR($ParFormObj, $ParFieldName, $moreHandler) : "";

		$ReturnStr = "
						<div id=\"{$ParID}_div\">
							<input type=\"text\" class=\"textboxnum\" name=\"$ParFieldName\" value=\"$ParFieldValue\" />
							".$calendar."
						</div>
						<script type=\"text/javascript\">
						<!--
							var $ParID = new Spry.Widget.ValidationTextField(\"$ParID\", \"date\", {useCharacterMasking:true, format:\"yyyy-mm-dd\", hint:\"yyyy-mm-dd\", validateOn:[\"change\"]});
						//-->
						</script>
					";
		return $ReturnStr;
	}
	function GET_DATE_FIELD_INVENTORY($ParID, $ParFormObj, $ParFieldName, $ParFieldValue = 0, $ParUseCalendar = 1, $moreHandler = "" ,$ParMasterField) {

		if ($ParFieldValue == 0) $ParFieldValue = "yyyy-mm-dd"; //date("Y-m-d");

		$calendar = ($ParUseCalendar) ? $this->GET_CALENDAR_INVENTORY($ParFormObj, $ParFieldName, $moreHandler, $ParMasterField) : "";

		$ReturnStr = "
						<div id=\"$ParMasterField\">
							<input type=\"text\" class=\"textboxnum\" name=\"$ParMasterField\" id=\"$ParMasterField\" value=\"$ParFieldValue\" onChange=\"setAllExpiryDate(this.value)\"/>
							".$calendar."
						</div>
						<script type=\"text/javascript\">
						<!--
							var $ParMasterField = new Spry.Widget.ValidationTextField(\"$ParMasterField\", \"date\", {useCharacterMasking:true, format:\"yyyy-mm-dd\", hint:\"yyyy-mm-dd\", validateOn:[\"change\"]});
						//-->
						</script>
					";
		return $ReturnStr;
	}

	# DHTML Calendar for Date Selection
	function GET_CALENDAR2($formField, $dateField, $moreHandler=""){
		global $number_calendar_used_before, $image_path, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$img_name = "calendar_img".$number_calendar_used_before;

		$rx = "<a href='javascript:calObj.show()'><img id='{$img_name}' name='{$img_name}' onMouseOver=\"calObj.setDate({$dateField}.value);setReceiver({$dateField});MM_swapImage('{$img_name}','','{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" src='{$image_path}/{$LAYOUT_SKIN}/icon_calendar_off.gif' border='0' align='absmiddle' hspace='1' /></a>\n";

		if ($number_calendar_used_before<1)
		{
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/browser_sniffer.js'></script>\n";
			$rx .= "<script language='javascript' type='text/javascript' src='{$PATH_WRT_ROOT}templates/{$LAYOUT_SKIN}/js/dyn_calendar.js'></script>\n";
			$rx .= "<script language='JavaScript' type='text/javascript'>\n";
			$rx .= "function selDate(date, month, year, myType)\n";
			$rx .= "{\n";
			$rx .= "if (typeof(document.$formField)!='undefined')\n";
			$rx .= "{\n";
			$rx .= "var selMonth = (parseInt(month)<10) ? '0'+parseInt(month) : parseInt(month);\n";
			$rx .= "var selDate = (parseInt(date)<10) ? '0'+parseInt(date) : parseInt(date);\n";
			$rx .= "calReceiver.value = parseInt(year)+'-'+selMonth+'-'+selDate;\n";
			$rx .= "calObj._hideLayer();\n";
			$rx .= $moreHandler."\n";
			$rx .= "}\n";
			$rx .= "}\n";
			$rx .= "var calReceiver = null;\n";
			$rx .= "function setReceiver(myObj)\n";
			$rx .= "{\n";
			$rx .= "calReceiver = myObj;\n";
			$rx .= "}\n";
			$rx .= "var is_big5 = ".(($ck_default_lang=="chib5") ? "1" : "0") .";\n";
			$rx .= "calObj = new dynCalendar('calObj', 'selDate', '{$image_path}/{$LAYOUT_SKIN}/');\n";
			$rx .= "calObj.setMonthCombo(0);\n";
			$rx .= "calObj.setYearCombo(0);\n";
			$rx .= "calObj.setOffsetX(-8);\n";
			$rx .= "calObj.setOffsetY(5);\n";
			$rx .= "</script>\n";
			$rx .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
		}

		$number_calendar_used_before += 1;

		return $rx;
	}
	
	/*	# Additional Handlers can be add in the function
	function GET_TIME_FIELD2($ParID, $ParFormObj, $ParFieldName, $ParFieldValue = 0, $ParUseCalendar = 1, $ParFieldID = "", $moreHandlers="") {

		if ($ParFieldValue == 0)$ParFieldValue = "HH:mm:ss"; //date("Y-m-d");

		if ($ParFieldID != "")
			$calendar = ($ParUseCalendar) ? $this->GET_CALENDAR2($ParFormObj, "getElementById('$ParFieldID')") : "";
		else
			$calendar = ($ParUseCalendar) ? $this->GET_CALENDAR2($ParFormObj, $ParFieldName) : "";

		$ReturnStr = "
						<span id=\"$ParID\">
							<input type=\"text\" class=\"tabletextremark\" id=\"$ParFieldID\" name=\"$ParFieldName\" value=\"$ParFieldValue\" $moreHandlers />
							".$calendar."						</span>
						<script type=\"text/javascript\">
						
							var $ParID = new Spry.Widget.ValidationTextField(\"$ParID\", \"time\", {useCharacterMasking:true, format:\"HH:mm:ss\", hint:\"HH:mm:ss\", validateOn:[\"change\"]});
					
						</script>
					";
		return $ReturnStr;
	}*/
	

	# Additional Handlers can be add in the function
	function GET_DATE_FIELD2($ParID, $ParFormObj, $ParFieldName, $ParFieldValue = 0, $ParUseCalendar = 1, $ParFieldID = "", $moreHandlers="") {

		if ($ParFieldValue == 0) $ParFieldValue = "yyyy-mm-dd"; //date("Y-m-d");

		if ($ParFieldID != "")
			$calendar = ($ParUseCalendar) ? $this->GET_CALENDAR2($ParFormObj, "getElementById('$ParFieldID')") : "";
		else
			$calendar = ($ParUseCalendar) ? $this->GET_CALENDAR2($ParFormObj, $ParFieldName) : "";


		$ReturnStr = "
						<span id=\"$ParID\">
							<input type=\"text\" class=\"tabletextremark\" id=\"$ParFieldID\" name=\"$ParFieldName\" value=\"$ParFieldValue\" $moreHandlers />
							".$calendar."
						</span>
						<script type=\"text/javascript\">
						
							var $ParID = new Spry.Widget.ValidationTextField(\"$ParID\", \"date\", {useCharacterMasking:true, format:\"yyyy-mm-dd\", hint:\"yyyy-mm-dd\", validateOn:[\"change\"]});
					
						</script>
					";
		return $ReturnStr;
	}



	function n_sizeof($arrData){

		$count = 0;
		if (is_array($arrData))
		{
			while (list($key, $value) = each($arrData))
			{
				if (is_int($key))
				{
					$count++;
				}
			}
		} else
		{
			$count = sizeof($arrData);
		}
		return $count;
	}


	// convert PHP array to JS array (2D)
	function CONVERT_TO_JS_ARRAY($arrData, $jArrName, $fromDB = 0, $quot = 0){

		$ReturnStr = "<SCRIPT LANGUAGE=\"JavaScript\">\n";
		$ReturnStr .= "var ".$jArrName." = new Array(".sizeof($arrData).");\n";

		for ($i = 0; $i < sizeof($arrData); $i++)
		{
			//$ReturnStr .= $jArrNa�e."[".$i."] = new Array(".$this->n_sizeof($arrData[$i]).");\n";
			$ReturnStr .= $jArrName."[".$i."] = new Array(".$this->n_sizeof($arrData[$i]).");\n";

			for ($j = 0; $j < $this->n_sizeof($arrData[$i]); $j++)
			{
				if (sizeof($arrData[$i]) > 1)
				{
					if ($quot)
					{
						if ($fromDB)
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("\"", "&quot;", str_replace("'", "&#039;", $arrData[$i][$j]))."\";\n";
						} else
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("\"", "&quot;", str_replace("'", "&#039;", htmlspecialchars($arrData[$i][$j])))."\";\n";
						}
					} else
					{
						if ($fromDB)
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#039;", $arrData[$i][$j])."\";\n";
						} else
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#039;", htmlspecialchars($arrData[$i][$j]))."\";\n";
						}
					}
				} else
				{
					if ($quot)
					{
						if ($fromDB)
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("\"", "&quot;", str_replace("'", "&#039;", $arrData[$i]))."\";\n";
						} else
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("\"", "&quot;", str_replace("'", "&#039;", htmlspecialchars($arrData[$i])))."\";\n";
						}
					} else
					{
						if ($fromDB)
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#039;", $arrData[$i])."\";\n";
						} else
						{
							$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#039;", htmlspecialchars($arrData[$i]))."\";\n";
						}
					}
				}
			}
		}

		$ReturnStr .= "</SCRIPT>\n";

		return $ReturnStr;
	}


	// generate an icon button which calls the preset text for select
	function GET_PRESET_LIST($jArrName, $btnID, $targetName){

		global $image_path, $LAYOUT_SKIN, $flag_is_loaded_before, $number_calendar_used_before, $ec_words;

		$ReturnStr .=  "<a id=\"poslink{$btnID}\" href=\"javascript:jSelectPresetText(".$jArrName.", '$targetName', 'listContent', '".$btnID."');\">";
		$ReturnStr .=  "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pre-set_off.gif\" id=\"posimg{$btnID}\" name=\"posimg{$btnID}\" border=\"0\" onmouseover=\"MM_swapImage('posimg{$btnID}','','{$image_path}/{$LAYOUT_SKIN}/icon_pre-set_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" align='absmiddle' /></a>&nbsp;";
		$ReturnStr .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";

		if (!$flag_is_loaded_before)
		{
			$ReturnStr .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";
			// iframe to cover pull-down menus
			if ($number_calendar_used_before<=0)
			{
				$ReturnStr .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
			}
			$flag_is_loaded_before = true;
		}

		return $ReturnStr;
	}


	// generate an hyperlink which calls the preset text
	function GET_PRESET_LINK($jDataName, $linkID, $linkText, $linkClass, $rowTotal){

		global $image_path, $LAYOUT_SKIN, $flag_is_loaded_before, $number_calendar_used_before, $ec_words;

		$ReturnStr .=  "<a id=\"poslink{$linkID}\" href=\"javascript:jSelectPresetTextForLink(".$jDataName.", 'listContent', ".$rowTotal.", ".$linkID.");\" class=\"".$linkClass."\">".$linkText."</a>";
		$ReturnStr .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";

		if (!$flag_is_loaded_before)
		{
			$ReturnStr .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";
			// iframe to cover pull-down menus
			if ($number_calendar_used_before<=0)
			{
				$ReturnStr .= "<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>\n";
			}
			$flag_is_loaded_before = true;
		}

		return $ReturnStr;
	}


	function GEN_CHART($ParConfArr, $ParDataArr) {

		global $PATH_WRT_ROOT, $button_export, $i_no_record_exists_msg, $LAYOUT_SKIN;

		if (!is_array($ParDataArr) || sizeof($ParDataArr)<=0)
		{
			return "<span class=\"tabletext\">".$i_no_record_exists_msg."</span><br/>";
		}

		$max = 0;
		for ($i = 0; $i < sizeof($ParDataArr); $i++)
		{
			if ($ParDataArr[$i][1] > $max) $max = $ParDataArr[$i][1];
		}

		(isset($ParConfArr['MaxWidth'])) ? $MaxWidth = $ParConfArr['MaxWidth'] : $MaxWidth = 580;
		(isset($ParConfArr['MaxHeight'])) ? $MaxHeight = $ParConfArr['MaxHeight'] : $MaxHeight = 200;

		if ($ParConfArr['IsHorizontal'])
		{

			$ReturnStr .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td align=\"center\">\n";
			//$ReturnStr .= "<br>\n";
			$ReturnStr .= "<table width=\"88%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td align=\"center\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
			$ReturnStr .= "<tr align=\"center\" valign=\"middle\"> \n";
			$ReturnStr .= "<td align=\"left\" nowrap=\"nowrap\" class=\"barxytexth\">\n".$ParConfArr['ChartX'];
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "<td>&nbsp;</td>\n";
			$ReturnStr .= "<td ><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"30\"></td>\n";
			$ReturnStr .= "<td width=\"90%\" align=\"left\" class=\"barxytexth\">".$ParConfArr['ChartY'];
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";

			for ($i = 0; $i < sizeof($ParDataArr); $i++)
			{
				$ReturnStr .= "<tr> \n";
				$ReturnStr .= "<td align=\"left\" valign=\"middle\" nowrap class=\"barnameh\">\n";
				$ReturnStr .= $ParDataArr[$i][0]."</td>\n";
				$ReturnStr .= "<td>&nbsp;</td>\n";
				$ReturnStr .= "<td background=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"1\"></td>\n";
				$ReturnStr .= "<td align=\"left\" valign=\"bottom\"> \n";
				$ReturnStr .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$ReturnStr .= "<tr> \n";
				$no = ($i % 11) + 1;
				if ($no < 10) $no = "0".$no;
				if ($ParDataArr[$i][1] > 0) {
					$width = ($max * $MaxWidth>0) ? intval($ParDataArr[$i][1] / $max * $MaxWidth) : 0;
				} else {
					$width = 1;
				}
				if ($width == 0) $width = 1;
				$ReturnStr .= "<td width=\"".$width."\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/bar_h_".$no.".gif\" width=\"".$width."\" height=\"19\"></td>\n";
				$ReturnStr .= "<td align=\"left\" class=\"barnum\">".$ParDataArr[$i][1]."</td>\n";
				$ReturnStr .= "</tr>\n";
				$ReturnStr .= "</table></td>\n";
				$ReturnStr .= "</tr>\n";
			}

			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td height=\"5\" align=\"center\" valign=\"middle\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td>\n";
			$ReturnStr .= "<td height=\"5\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"5\" height=\"5\"></td>\n";
			$ReturnStr .= "<td width=\"1\" height=\"5\" background=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"5\"></td>\n";
			$ReturnStr .= "<td height=\"5\" align=\"left\" valign=\"bottom\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr height=\"1\" background=\"/images/\"> \n";
			$ReturnStr .= "<td align=\"center\" valign=\"middle\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"5\" height=\"1\"></td>\n";
			$ReturnStr .= "<td width=\"5\" height=\"1\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"5\" height=\"1\"></td>\n";
			$ReturnStr .= "<td width=\"1\" height=\"1\" background=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"1\"></td>\n";
			$ReturnStr .= "<td align=\"center\" valign=\"middle\" background=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"1\"></td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td height=\"5\" align=\"center\" valign=\"middle\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" height=\"5\"></td>\n";
			$ReturnStr .= "<td height=\"5\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" height=\"5\"></td>\n";
			$ReturnStr .= "<td width=\"1\" height=\"5\"><img src=\"/images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"5\"></td>\n";
			$ReturnStr .= "<td height=\"5\" align=\"center\" valign=\"middle\"><img src=\"/images/".$LAYOUT_SKIN."/10x10.gif\" height=\"5\"></td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table></td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td align=\"right\">\n";
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table> \n";

			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table>\n";

		} else
		{

			$ReturnStr .= "<br/><table width=\"88%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td align=\"center\"> \n";
			$ReturnStr .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td class=\"barxytext\">".$ParConfArr['ChartY']."</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td>\n";
			$ReturnStr .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td>&nbsp;</td>\n";
			$ReturnStr .= "<td width=\"1\" background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"230\"></td>\n";
			$ReturnStr .= "<td width=\"10\" align=\"center\" valign=\"middle\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>\n";

			for ($i = 0; $i < sizeof($ParDataArr); $i++)
			{
				$ReturnStr .= "<td align=\"center\" valign=\"bottom\"> \n";
				$ReturnStr .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
				$ReturnStr .= "<tr> \n";
				$ReturnStr .= "<td align=\"center\" class=\"barnum\">".$ParDataArr[$i][1]."</td>\n";
				$ReturnStr .= "</tr>\n";
				$ReturnStr .= "<tr> \n";
				$no = $i % 11 + 1;
				if ($no < 10) $no = "0".$no;
				$height = ($max * $MaxHeight>0) ? intval($ParDataArr[$i][1] / $max * $MaxHeight) : 0;
				$ReturnStr .= "<td align=\"center\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/bar_v_".$no.".gif\" width=\"19\" height=\"".$height."\"></td>\n";
				$ReturnStr .= "</tr>\n";
				$ReturnStr .= "</table>\n";
				$ReturnStr .= "</td>\n";
			}

			$ReturnStr .= "</tr>\n";

			$ReturnStr .= "<tr height=\"1\"> \n";
			$ReturnStr .= "<td height=\"1\" width=\"5\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"5\" height=\"1\"></td>\n";
			$ReturnStr .= "<td width=\"1\" height=\"1\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"1\"></td>\n";
			$ReturnStr .= "<td width=\"10\" align=\"center\" valign=\"middle\" background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"1\"></td>\n";

			for ($i = 0; $i < sizeof($ParDataArr); $i++)
			{
				$ReturnStr .= "<td align=\"center\" valign=\"middle\" background=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"1\"></td>\n";
			}

			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td height=\"5\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" height=\"5\"></td>\n";
			$ReturnStr .= "<td width=\"1\" height=\"5\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/barchart/line.gif\" width=\"1\" height=\"5\"></td>\n";
			$ReturnStr .= "<td width=\"10\" align=\"center\" valign=\"middle\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" height=\"5\"></td>\n";

			for ($i = 0; $i < sizeof($ParDataArr); $i++)
			{
				$ReturnStr .= "<td align=\"center\" valign=\"middle\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" height=\"5\"></td>\n";
			}

			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td>&nbsp;</td>\n";
			$ReturnStr .= "<td width=\"1\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"10\"></td>\n";
			$ReturnStr .= "<td width=\"10\" align=\"center\" valign=\"middle\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"> \n";
			$ReturnStr .= "</td>\n";

			for ($i = 0; $i < sizeof($ParDataArr); $i++)
			{
				$ReturnStr .= "<td align=\"center\" valign=\"middle\" class=\"barname\">\n".chr($i + 65);
				$ReturnStr .= "</td>\n";
			}

			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table></td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td align=\"center\" class=\"barxytext\">\n".$ParConfArr['ChartX'];
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table>\n";
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "<td align=\"center\" valign=\"bottom\"> \n";
			$ReturnStr .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
			$ReturnStr .= "<tr>\n";
			$ReturnStr .= "<td align=\"center\">\n";
			$ReturnStr .= "<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"barremarkbox\">\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td class=\"barxytext\">\n".$ParConfArr['ChartX'];
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr> \n";
			$ReturnStr .= "<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n";

			for ($i = 0; $i < sizeof($ParDataArr); $i++)
			{
				$ReturnStr .= "<tr> \n";
				$ReturnStr .= "<td class=\"barremarktext\">".chr($i + 65)." - ".$ParDataArr[$i][0];
				$ReturnStr .= "</td>\n";
				$ReturnStr .= "</tr>\n";
			}

			$ReturnStr .= "</table></td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table>\n";
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "<tr>\n";
			$ReturnStr .= "<td align=\"right\"><br>\n";
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table>\n";
			$ReturnStr .= "<br>\n";
			$ReturnStr .= "</td>\n";
			$ReturnStr .= "</tr>\n";
			$ReturnStr .= "</table>\n";
		}

		return $ReturnStr;
	}


	##############################
	/*
	function GEN_TOP_MENU() {
		global $image_path, $i_frontpage_menu_home;
		global $FullMenuArr;

		$MenuArr = $FullMenuArr['topMenu'];
		$SubMenuArr = $FullMenuArr['subMenu'];

		$ReturnStr .= "<script language=\"javascript\">\n";
		while (list($key, $value) = each($SubMenuArr))
		{
			if (is_array($SubMenuArr[$key]))
			{
				$SubMenuStr[$key] = $this->GEN_SECOND_MENU($SubMenuArr[$key]);
				$ReturnStr .= " var SubMenuHTML".$key." = '".$SubMenuStr[$key]."';\n";
			}
		}
		$ReturnStr .= "</script>\n";

		$ReturnStr .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                          <tr>
	                            <td width=\"905\" valign=\"bottom\" background=\"$image_path/".$LAYOUT_SKIN."/topbar/menubar_bg.gif\">
	                              <table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
	                                <tr>
						";

		for ($i = 0; $i < sizeof($MenuArr); $i++) {
			if (is_array($MenuArr[$i])) {
				if ($i == 0) {
					if ($MenuArr[$i][3]) {
						$tdClass = "topmenuon";
						$curImg = "icon_home_on";
					} else {
						$tdClass = "topmenuoff";
						$curImg = "icon_home_off";
					}

					if ($MenuArr[$i][1] != "#") {
						$ReturnStr .= "
										<td nowrap><a href=\"".$MenuArr[$i][1]."\" class=\"$tdClass\" onMouseOver=\"MM_swapImage('ihome','','$image_path/".$LAYOUT_SKIN."/topbar/icon_home_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\"  name=\"ihome\" width=\"16\" height=\"14\" border=\"0\" align=\"absmiddle\" id=\"ihome\">
										".$MenuArr[$i][2]."</a></td>
									  ";
					} else {
						$ReturnStr .= "
										<td nowrap><a href=\"javascript:void(0)\" onClick=\"javascript: (typeof(SubMenuHTML".$MenuArr[$i][0].") != 'undefined') ? document.getElementById('SubMenuInnerHTML').innerHTML=SubMenuHTML".$MenuArr[$i][0]." : document.getElementById('SubMenuInnerHTML').innerHTML='';\" class=\"$tdClass\" onMouseOver=\"MM_swapImage('ihome','','$image_path/".$LAYOUT_SKIN."/topbar/icon_home_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\"  name=\"ihome\" width=\"16\" height=\"14\" border=\"0\" align=\"absmiddle\" id=\"ihome\">
										".$MenuArr[$i][2]."</a></td>
									  ";
					}
				} else {
					if ($MenuArr[$i][3]) {
						$tdClass = "topmenuon";
						$curImg = "icon_cube_on";
					} else {
						$tdClass = "topmenuoff";
						$curImg = "icon_cube_off";
					}
					$ReturnStr .= "
									<td nowrap><a href=\"javascript:void(0)\" onClick=\"javascript: (typeof(SubMenuHTML".$MenuArr[$i][0].") != 'undefined') ? document.getElementById('SubMenuInnerHTML').innerHTML=SubMenuHTML".$MenuArr[$i][0].": document.getElementById('SubMenuInnerHTML').innerHTML='';\" class=\"$tdClass\" onMouseOver=\"MM_swapImage('cube$i','','$image_path/".$LAYOUT_SKIN."/topbar/icon_cube_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\" name=\"cube$i\" width=\"16\" height=\"14\" border=\"0\" align=\"absmiddle\" id=\"cube$i\">
									".$MenuArr[$i][2]."</a></td>
									";
				}
			}
		}

		$ReturnStr .= "
	                                </tr>
	                              </table></td>
	                            <td width=\"34\"><img src=\"$image_path/".$LAYOUT_SKIN."/topbar/menubar_corner.gif\" width=\"15\" height=\"36\"></td>
	                          </tr>
	                        </table>
						";
		return $ReturnStr;
	}
	*/
	/*
	function GEN_TOP_MENU() {
		global $image_path, $i_frontpage_menu_home;
		global $FullMenuArr;

		$MenuArr = $FullMenuArr['topMenu'];
		$SubMenuArr = $FullMenuArr['subMenu'];

// 		 === $MenuArr : array fields description====
// 			1 - menu id
// 			2 - path / #  (if #, sub-menu will be displayed)
// 			3 - section name
// 			4 - section focus flag
// 			5 - special icon for the section (xxxx e.g. xxxx_on, xxxx_off)
		

		$ReturnStr .= "<script language=\"javascript\">\n";
		while (list($key, $value) = each($SubMenuArr))
		{
			if (is_array($SubMenuArr[$key]))
			{
				$SubMenuStr[$key] = $this->GEN_SECOND_MENU($SubMenuArr[$key], 0);
				$ReturnStr .= " var SubMenuHTML".$key." = '".$SubMenuStr[$key]."';\n";
			}
		}
		$ReturnStr .= "</script>\n";

		for ($i = 0; $i < sizeof($MenuArr); $i++)
		{
			if (is_array($MenuArr[$i]))
			{
				$icon_name = $MenuArr[$i][4] ? $MenuArr[$i][4] : "icon_cube";
				if ($MenuArr[$i][3])
				{
					$tdClass = "topmenuon";
					$curImg = $icon_name."_on";
				} else {
					$tdClass = "topmenuoff";
					$curImg = $icon_name."_off";
				}

				if ($MenuArr[$i][1] != "#") {
						$ReturnStr .= "
									<td nowrap><a href=\"".$MenuArr[$i][1]."\" class=\"$tdClass\" onMouseOver=\"MM_swapImage('ihome','','$image_path/".$LAYOUT_SKIN."/topbar/icon_home_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\"  name=\"ihome\" width=\"16\" height=\"14\" border=\"0\" align=\"absmiddle\" id=\"ihome\">
									".$MenuArr[$i][2]."</a></td>
								  ";

				} else {
					$ReturnStr .= "
									<td nowrap><a href=\"javascript:void(0)\" onClick=\"javascript: (typeof(SubMenuHTML".$MenuArr[$i][0].") != 'undefined') ? document.getElementById('SubMenuInnerHTML').innerHTML=SubMenuHTML".$MenuArr[$i][0]." : document.getElementById('SubMenuInnerHTML').innerHTML='';\" class=\"$tdClass\" onMouseOver=\"MM_swapImage('ihome','','$image_path/".$LAYOUT_SKIN."/topbar/icon_home_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\"  name=\"ihome\" width=\"16\" height=\"14\" border=\"0\" align=\"absmiddle\" id=\"ihome\">
									".$MenuArr[$i][2]."</a></td>
								  ";
				}
			}
		}

		return $ReturnStr;
	}
	*/

	/*
	function GEN_SECOND_MENU($ParMenuArr, $IsHTML = 0) {
		global $image_path;

		$DisplayMenuArr = $ParMenuArr;

		$ReturnStr = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
		$ReturnStr .= "<tr>";
		$ReturnStr .= "<td width=\"10\" nowrap><img src=\"$image_path/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>";

		for ($i = 0; $i < sizeof($DisplayMenuArr); $i++) {
			if ($DisplayMenuArr[$i][2]) {
				$tdClass = "topmenuon";
				$curImg = "icon_sub_arrow_on";
			} else {
				$tdClass = "topsubmenuoff";
				$curImg = "icon_sub_arrow_off";
			}
			$ReturnStr .= "<td nowrap><a href=\"".$DisplayMenuArr[$i][0]."\" class=\"$tdClass\" ";
			if ($IsHTML) {
				$ReturnStr .= "onMouseOver=\"MM_swapImage('sub$i','','$image_path/".$LAYOUT_SKIN."/topbar/icon_sub_arrow_on.gif',1)\" ";
			} else {
				$ReturnStr .= "onMouseOver=\"MM_swapImage(\'sub$i\',\'\',\'$image_path/".$LAYOUT_SKIN."/topbar/icon_sub_arrow_on.gif\',1)\" ";
			}
			$ReturnStr .= "onMouseOut=\"MM_swapImgRestore()\">";
			$ReturnStr .= "<img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\" name=\"sub$i\" width=\"10\" height=\"11\" border=\"0\" align=\"absmiddle\" id=\"sub$i\">".$DisplayMenuArr[$i][1]."</a> </td>";
		}

		$ReturnStr .= "</tr></table>";
		return $ReturnStr;
	}
	*/
	function GEN_SECOND_MENU($ParMenuArr, $IsHTML = 0) {
		global $image_path;

		$DisplayMenuArr = $ParMenuArr;

		$ReturnStr = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
		$ReturnStr .= "<td width=\"10\" nowrap><img src=\"$image_path/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td>";

		for ($i = 0; $i < sizeof($DisplayMenuArr); $i++) {
			if ($DisplayMenuArr[$i][2]) {
				$tdClass = "topmenuon";
				$curImg = "icon_sub_arrow_on";
			} else {
				$tdClass = "topsubmenuoff";
				$curImg = "icon_sub_arrow_off";
			}

			if ($IsHTML) {
				$ReturnStr .= "<td nowrap><a href=\"".str_replace("\\'", "'", $DisplayMenuArr[$i][0])."\" class=\"$tdClass\" ";
				$ReturnStr .= "onMouseOver=\"MM_swapImage('sub$i','','$image_path/".$LAYOUT_SKIN."/topbar/icon_sub_arrow_on.gif',1)\" ";

			} else {
				$ReturnStr .= "<td nowrap><a href=\"".$DisplayMenuArr[$i][0]."\" class=\"$tdClass\" ";
				$ReturnStr .= "onMouseOver=\"MM_swapImage(\'sub$i\',\'\',\'$image_path/".$LAYOUT_SKIN."/topbar/icon_sub_arrow_on.gif\',1)\" ";

			}
			$ReturnStr .= "onMouseOut=\"MM_swapImgRestore()\">";
			$ReturnStr .= "<img src=\"$image_path/".$LAYOUT_SKIN."/topbar/$curImg.gif\" name=\"sub$i\" width=\"10\" height=\"11\" border=\"0\" align=\"absmiddle\" id=\"sub$i\"> ".$DisplayMenuArr[$i][1]."</a> </td>";
		}

		$ReturnStr .= "</tr></table>";

		return $ReturnStr;
	}

	# TextArea Tools
	function getTextArea($taName, $taContents, $taCols=60, $taRows=5){
	$initialRows = 2;
	$initialRows = (trim($taContents)=="") ? $initialRows : $taRows;

	if($TextAreaClassChangeEnabled=true)
	{
		//Class textboxtext
		$rx = "<textarea  class=\"textboxtext\" name=\"{$taName}\" cols=\"{$taCols}\" rows=\"{$initialRows}\" wrap=\"virtual\" onFocus=\"this.rows={$taRows}\">{$taContents}</textarea>";
	}else{
		//default settings
		$rx = "<textarea class=\"inputfield\" name=\"{$taName}\" cols=\"{$taCols}\" rows=\"{$initialRows}\" wrap=\"virtual\" onFocus=\"this.rows={$taRows}\">{$taContents}</textarea>";
	}

	return $rx;
}

# Display Help Icon
# show the tips on mouse over
function getTipsHelp($myText){
	global $image_path;

	# DHTML tooltips
	$rx = "<img src=\"$image_path/icon/help.gif\" border='0' align='absmiddle' onMouseMove='overhere();' onMouseOver=\"showTipsHelp('". str_replace('"', '&quot;', addslashes($myText)). "')\" onMouseOut='hideTip()' />";
	//$rx = "<img src=\"$image_path/icon/help.gif\" border='0' title=\"". str_replace('"', '&quot;', $myText). "\" >";
	return $rx;
}

	##############################

function Get_Thickbox_Return_Message_Layer($DivID='') {
	
	if ($DivID=='')
		$DivID = 'system_message_box';
		
	$x = '<div id="'.$DivID.'" class="SystemReturnMessage" style="position: static; margin:0 0 0 0; display:block; visibility:hidden;"> 
					<div class="msg_board_left">
				  	<div class="msg_board_right" id="message_body" style="text-align:center;"> 
				  	<a href="#" onclick="$(\'div.SystemReturnMessage\').css(\'visibility\',\'hidden\'); return false;">[Clear]</a>
				  	</div>
					</div>
				</div>';
	
	return $x;
}

	############### IP25 Top Menu #############################################
	function GEN_TOP_MENU_IP25($Arr = array())
	{
		//global $CurrentPageArr;
		//debug_r($CurrentPageArr);
		//hdebug_r($Arr);

		$x = "";
		if(sizeof($Arr))
		{
			$x .= "<ul>\n";
			for($i=0;$i<sizeof($Arr);$i++)
			{
				if(is_array($Arr[$i][0]))
				{
					$x .= $this->GEN_TOP_MENU_li($Arr[$i][0], 0);
					$x .= $this->GEN_TOP_MENU_IP25($Arr[$i][1]);
					$x .= "</li>\n";
				}
				else
				{
					$x .= $this->GEN_TOP_MENU_li($Arr[$i]);
				}
			}
			$x .= "</ul>\n";
		}
		return $x;
	}
	
	function GEN_TOP_MENU_li($Arr = array(), $End_li = 1)
	{
		$x = "";
		list($menuid, $link, $name, $focus, $css, $menuKeyArr) = $Arr;
		$this_css = $css ? $css : "";
		$focus_css = ($focus && !$this->IsHeaderCaching) ? ($this_css ? $this_css."_current" : "current_module") : $this_css;
		$menuIDused = (is_array($menuKeyArr)) ? $menuKeyArr[sizeof($menuKeyArr)-1] : $menuKeyArr;
		$x .= "<li style='z-index: 9999;'><a id=\"".$menuIDused."\" href='$link' class='$focus_css'>$name</a>";
		if($End_li)	$x .= "</li>\n";
		
		return $x;
	}

	#################### Copy from ESF Userinterface.php (for gamma mail)###################
	function Get_IFrame($FrameBorder="0",$Name="",$ID="",$Scroll="no",$Width="100%",$Height="200",$Src="",$MarginWidth="0",$MarginHeight="0",$OtherMember="") {
		$x = '<iframe 
						frameborder="'.$FrameBorder.'" 
						name="'.$Name.'" 
						id="'.$ID.'"
						scrolling="'.$Scroll.'" 
						width="'.$Width.'" 
						height="'.$Height.'" 
						src="'.$Src.'" 
						marginwidth"'.$MarginWidth.'" 
						marginheight="'.$MarginHeight.'" 
						'.$OtherMember.' />mnb</iframe>';
						
		return $x;
	}
	
	
	function Get_Table_Action_Button($ParArr)
	{
		$x = "";
		global $image_path, $LAYOUT_SKIN;
		
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$x .= "<tbody>";
		$x .= "<tr>";
//		$x .= "<td width=\"21\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/table_tool_01.gif\" height=\"23\" width=\"21\"></td>";
//		$x .= "<td background=\"".$image_path."/".$LAYOUT_SKIN."/management/table_tool_02.gif\">";
        $x .= "<td>";
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">";
		$x .= "<tbody>";
		$x .= "<tr>";
		
		for($i = 0; $i < count($ParArr); $i++)
		{
			$Href = $ParArr[$i][0];
			$Img = $ParArr[$i][1];
			$ImgName = $ParArr[$i][2];
			$Title = $ParArr[$i][3];
			$OnClick = $ParArr[$i][4];
			$x .= "<td nowrap=\"nowrap\">";
			$x .= "<a href=\"".$Href."\" class=\"tabletool\" onclick=\"".$OnClick."\">";
			$x .= "<img src=\"".$Img."\" name=\"".$ImgName."\" align=\"absmiddle\" border=\"0\" height=\"12\" width=\"12\">";
			$x .= $Title;
			$x .= "</a></td>";
			
			if($i < count($ParArr) - 1)
				$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/10x10.gif\" width=\"5\"></td>";
		}
		
		$x .= "</tr>";
		$x .= "</tbody>";
		$x .= "</table>";
		$x .= "</td>";
//		$x .= "<td width=\"6\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/table_tool_03.gif\" height=\"23\" width=\"6\"></td>";
		$x .= "</tr>";
		$x .= "</tbody>";
		$x .= "</table>";
		
		return $x;
	}
	
	function Get_DBTable_Action_Button_IP25($ParBtnArr)
	{
		global $Lang;
		
		$x = '';
		$numOfBtn = count((array)$ParBtnArr);
		
		if ($numOfBtn > 0)
		{
			$x .= '<div class="common_table_tool">'."\n";
				for ($i=0; $i<$numOfBtn; $i++)
				{
					$thisBtnType = $ParBtnArr[$i][0];
					$thisHref = $ParBtnArr[$i][1];
					$thisBtnName = $ParBtnArr[$i][2];
					$thisLinkID = $ParBtnArr[$i][3];
					$thisOtherTags = $ParBtnArr[$i][4];
					
					switch (strtolower($thisBtnType))
					{
						case "edit":
							$thisDefaultText = $Lang['Btn']['Edit'];
							$thisClass = 'tool_edit';
						break;	
						case "delete":
							$thisDefaultText = $Lang['Btn']['Delete'];
							$thisClass = 'tool_delete';
						break;
						case "copy":
							$thisDefaultText = $Lang['Btn']['Copy'];
							$thisClass = 'tool_copy';
						break;
						case "move":
							$thisDefaultText = $Lang['Btn']['Move'];
							$thisClass = 'tool_move';
						break;
						case "public":
							$thisDefaultText = $Lang['Btn']['Public'];
							$thisClass = 'tool_public';
						break;
						case "private":
							$thisDefaultText = $Lang['Btn']['Private'];
							$thisClass = 'tool_private';
						break;
						case "email":
							$thisDefaultText = $thisBtnName;
							$thisClass = 'tool_email';
						break;
						case "approve":
							$thisDefaultText = $Lang['Btn']['Approve'];
							$thisClass = 'tool_approve';
						break;
						case "reject":
							$thisDefaultText = $Lang['Btn']['Reject'];
							$thisClass = 'tool_reject';
						break;
						case "assign_to_student":
							$thisDefaultText = $Lang['Btn']['AssignToStudent'];
							$thisClass = 'tool_assign_to_student';
						break;
						case "unassign_to_student":
							$thisDefaultText = $Lang['Btn']['AssignToStudent'];
							$thisClass = 'tool_unassign_to_student';
						break;
						case "totop":
							$thisDefaultText = $Lang['Btn']['ToTop'];
							$thisClass = 'tool_totop';
						break;
						case "set":
							$thisDefaultText = $Lang['Btn']['Set'];
							$thisClass = 'tool_set';
						break;
						case "undo":
							$thisDefaultText = $Lang['Btn']['Undo'];
							$thisClass = 'tool_undo';
						break;
						case "other":
						default:
							$thisDefaultText = $thisBtnName;
							$thisClass = 'tool_other';
					}
					
					$thisBtnName = ($thisBtnName=='')? $thisDefaultText : $thisBtnName;
					$thisIDTag = ($thisLinkID=='')? '' : ' id="'.$thisLinkID.'" ';
					$x .= '<a class="'.$thisClass.'" href="'.$thisHref.'" '.$thisIDTag.' '.$thisOtherTags.'>'.$thisBtnName.'</a>'."\n";
				}
			$x .= '</div>'."\n";
		}
		return $x;
	}
	
	function Get_Ajax_Loading_Image($noLang=0, $custLang='')
	{
		global $LAYOUT_SKIN, $Lang;
		$x .= '<img src="/images/'.$LAYOUT_SKIN.'/indicator.gif">';
		if(!$noLang) {
			$Display = ($custLang=='')? $Lang['General']['Loading'] : $custLang;
			$x .= '<span class="tabletextremark">'.$Display.'</span>';
		}
			
		
		return $x;
	}
	
	##################################################################
	#Function to Generate Button in Content Top Tool
	##################################################################
	#page_title  = text display on the button
	#page_href   = the page that will redirect to after clicking
	#img_src	 = image before the page_title
	#Is_Selected = to indicate the button is selected or not
	##################################################################
	
	function GET_CONTENT_TOP_BTN($pages_arr) {

		//global $image_path, $LAYOUT_SKIN;
		$rx = "<div class=\"thumb_list_tab\">";

		for ($i=0; $i<sizeof($pages_arr); $i++)
		{
			list($page_title, $page_href, $img_src, $Is_Selected) = $pages_arr[$i];
			$page_title = trim($page_title);
			$page_href = trim($page_href);
			$img_src = trim($img_src);
			
			if ($page_title!="" && $page_href!="")
			{
				$rx .= "<a href=\"$page_href\"".($Is_Selected? " class=\"thumb_list_tab_on\"" : "")."><span>";
				$rx .= ($img_src!="")? "<img src=\"$img_src\" border=\"0\" align=\"absmiddle\" />" : "";
				$rx .= $page_title."</span></a>";
				$rx .= ($i == sizeof($pages_arr) - 1)? "" : "<em>|</em>";
			}
		}
		$rx .= "</div>";

		return $rx;
	}
	
	function MandatoryField()
	{
		global $Lang;
		$x = "
			<span class=\"tabletextremark\">
				".$Lang['General']['RequiredField']."
			</span>
			<p class=\"spacer\"></p>
		";
		
		return $x;
	}
	
	function ReferenceField()
	{
		global $Lang;
		$x = "
			<span class=\"tabletextremark\">
				".$Lang['General']['ReferenceField']."
			</span>
			<p class=\"spacer\"></p>
		";
		
		return $x;
	}
	
	function InactiveStudentField($withoutClass=0)
	{
		global $Lang;
		if($withoutClass)
		{
			$x = $Lang['General']['InactiveStudentField'];
		}
		else
		{
			$x = "
				<span class=\"tabletextremark\">
					".$Lang['General']['InactiveStudentField']."
				</span>
				<p class=\"spacer\"></p>
			";
		}
		
		return $x;
	}
	
	function ArchiveStudentField()
	{
		global $Lang;
		
		$x = "
			<span class=\"tabletextremark\">
				".$Lang['General']['ArchiveStudentField']."
			</span>
			<p class=\"spacer\"></p>
		";
		
		return $x;
	}
	
	function GroupAdminField($withoutClass=0)
	{
		global $Lang;
		if($withoutClass)
		{
			$x = $Lang['General']['GroupAdminField'];
		}
		else
		{
			$x = "
				<span class=\"tabletextremark\">
					".$Lang['General']['GroupAdminField']."
				</span>
				<p class=\"spacer\"></p>
			";
		}
		
		return $x;
	}
	
	function Get_Content_Tool_By_Array_v30($buttonAry) {
		$numOfButton = count((array)$buttonAry);
		
		$x = '';
		$x .= '<div class="Conntent_tool">'."\r\n";
			for ($i=0; $i<$numOfButton; $i++) {
				$_btnType = $buttonAry[$i][0];
				$_href = $buttonAry[$i][1];
				$_displayText = $buttonAry[$i][2];
				$_optionAry = $buttonAry[$i][3];
				$_other = $buttonAry[$i][4];
				$x .= $this->Get_Content_Tool_v30($_btnType, $_href, $_displayText, $_optionAry, $_other);
			}
		$x .= '</div>'."\r\n";
		
		return $x;
	}
	
	function Get_Content_Tool_v30($type, $href="#", $text="", $options=array(), $other="", $divID='', $extra_class='', $hideDivAfterClicked=true, $extra_onclick='')
	{
		global $Lang;
		
		if ($divID != '')
			$DivId_tag = ' id="'.$divID.'" ';
		$type = strtolower($type);
		switch($type)
		{
			case "import":
				$default_text = $Lang['Btn']['Import'];
				$class = 'import';
				
//				if(!empty($options))
//				{
//					$a .= '<div class="btn_option" '.$DivId_tag.' >'."\n";
//                    	$a .= '<a onclick="js_Clicked_Option_Layer_Button(\'import_option\', \'btn_import\');" id="btn_import" class="import option_layer" href="'.$href.'"> '.$default_text.'</a>'."\n";
//                    	$a .= '<br style="clear: both;">'."\n";
//                     	$a .= '<div onclick="js_Clicked_Option_Layer_Button(\'import_option\', \'btn_import\');" id="import_option" class="btn_option_layer" style="visibility: hidden;">'."\n";
//							foreach($options as $sub_btn)
//							{
//								list($option_href,$option_text) = $sub_btn;
//								$a .= '<a class="sub_btn" href="'.$option_href.'"> '.$option_text.'</a>'."\n";
//							}
//						$a .= '</div>'."\n";
//					$a .= '</div>'."\n";
//				}
				break;
					
			case "export":
				$default_text = $Lang['Btn']['Export'];
				$class = 'export';
				
//				if(!empty($options))
//				{
//					$a .= '<div class="btn_option" '.$DivId_tag.' >'."\n";
//                    	$a .= '<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="'.$href.'"> '.$default_text.'</a>'."\n";
//                    	$a .= '<br style="clear: both;">'."\n";
//                     	$a .= '<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">'."\n";
//							foreach($options as $sub_btn)
//							{
//								list($option_href,$option_text) = $sub_btn;
//								$a .= '<a class="sub_btn" href="'.$option_href.'"> '.$option_text.'</a>'."\n";
//							}
//						$a .= '</div>'."\n";
//					$a .= '</div>'."\n";
//				}
				break;
				
			case "print":
				$default_text = $Lang['Btn']['Print'];
				$class = 'print';
				break;
					
			case "new":
				$default_text = $Lang['Btn']['New'];
				$class = 'new';
				
//				if(!empty($options))
//				{
//					$a .= '<div class="btn_option" '.$DivId_tag.' >'."\n";
//                    	$a .= '<a onclick="js_Clicked_Option_Layer(\'new_option\', \'btn_new\');" id="btn_new" class="new option_layer" href="'.$href.'"> '.$btn_text.'</a>'."\n";
//                    	$a .= '<br style="clear: both;">'."\n";
//                     	$a .= '<div onclick="js_Clicked_Option_Layer_Button(\'new_option\', \'btn_new\');" id="new_option" class="btn_option_layer" style="visibility: hidden;">'."\n";
//							foreach($options as $sub_btn)
//							{
//								list($option_href,$option_text) = $sub_btn;
//								$a .= '<a class="sub_btn" href="'.$option_href.'"> '.$option_text.'</a>'."\n";
//							}
//						$a .= '</div>'."\n";
//					$a .= '</div>'."\n";
//				}
				break;
					
			case "edit":
			case "edit_content":
				$default_text = $Lang['Btn']['Edit'];
				$class = 'edit_content';
				break;
					
			case "setting":
			case "setting_content":
				$default_text = $Lang['Btn']['Setting'];
				$class = 'setting_content';
				break;
			
			case "copy":
				$default_text = $Lang['Btn']['Copy'];
				$class = 'copy';
				break;
			
			case "generate":
			case "generate_btn":
				$default_text = $Lang['Btn']['Generate'];
				$class = 'generate_btn';
				break;
			
			case "upload":
				$default_text = $Lang['Btn']['Upload'];
				$class = 'upload';
				break;
		}
		
		$btn_text = trim($text)!=''?$text:$default_text;

		if(!empty($options))
		{
			if ($hideDivAfterClicked) {
				$subBtnOnclick = 'js_Clicked_Option_Layer_Button(\''.$type.'_option\', \'btn_'.$type.'\');';
			}
			$a .= '<div class="btn_option" '.$DivId_tag.' >'."\n";
            	$a .= '<a onclick="js_Clicked_Option_Layer(\''.$type.'_option\', \'btn_'.$type.'\'); '.$extra_onclick.'" id="btn_'.$type.'" class="'.$class.' option_layer" href="'.$href.'"> '.$btn_text.'</a>'."\n";
            	$a .= '<br style="clear: both;">'."\n";
             	$a .= '<div onclick="'.$subBtnOnclick.'" id="'.$type.'_option" class="btn_option_layer" style="visibility: hidden;">'."\n";
					foreach($options as $sub_btn)
					{
						list($option_href, $option_text, $option_id) = $sub_btn;
						$a .= '<a id="'.$option_id.'" class="sub_btn" href="'.$option_href.'"> '.$option_text.'</a>'."\n";
					}
				$a .= '</div>'."\n";
			$a .= '</div>'."\n";
		}
		
		if(!$a)
			$a = '<a class="'.$class.' '.$extra_class.'" href="'.$href.'" '.$other.'>'.$btn_text.'</a>';
		
		return $a;	
	}
	
	function GET_HIDDEN_INPUT($ID, $Name, $Value)
	{
		return '<input type="hidden" id="'.$ID.'" name="'.$Name.'" value="'.intranet_htmlspecialchars($Value).'" />';
	}
	
	function GET_IMPORT_STEPS($CurrStep, $CustStepArr='')
	{
		global $Lang;
		
		$StepArr = ($CustStepArr=='')? array_values($Lang['General']['ImportArr']['ImportStepArr']) : $CustStepArr;
		foreach($StepArr as $key => $Step)
			$STEP_OBJ[] = array($Step,($key+1==$CurrStep));
		
		return $this->GET_STEPS_IP25($STEP_OBJ); 
	}
	
	function GET_SUBTAGS($SUBTAGS_OBJ)
	{
		$subtags_table = '<div class="shadetabs"><ul>';
		for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
			if($SUBTAGS_OBJ[$i][2] == 1){
				$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
			}else{
				$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
			}
		}
		$subtags_table .= '</ul></div><p class="spacer"></p>';
		
		return $subtags_table;
	}
	
	function Spacer()
	{
		return '<p class="spacer"></p>';
	}
	
	function GET_TEXTBOX_NUMBER($Id, $Name, $Value, $OtherClass='', $OtherPar=array())
	{
		$OtherParValue = '';
		if(count($OtherPar)>0) {
			foreach($OtherPar as $parKey => $parVal) {
				$OtherParValue .= ' '.$parKey.'="'.$parVal.'" ';
			}
	    }
	    
		return '<input id="'.$Id.'" name="'.$Name.'" type="text" class="textboxnum '.$OtherClass.'" '.$OtherParValue.' value="'.intranet_htmlspecialchars($Value).'" />';
	}
	
	function GET_TEXTBOX($Id, $Name, $Value, $OtherClass='', $OtherPar=array())
	{
		$OtherParValue = '';
		if(count($OtherPar)>0) {
			foreach($OtherPar as $parKey => $parVal) {
				$OtherParValue .= ' '.$parKey.'="'.$parVal.'" ';
			}
	    }
	    
		return '<input id="'.$Id.'" name="'.$Name.'" type="text" class="textboxtext '.$OtherClass.'" '.$OtherParValue.' value="'.intranet_htmlspecialchars($Value).'" />';
	}
	
	function GET_TEXTBOX_NAME($Id, $Name, $Value, $OtherClass='', $OtherPar=array())
	{
		$OtherParValue = '';
		if(count($OtherPar)>0) {
			foreach($OtherPar as $parKey => $parVal) {
				$OtherParValue .= ' '.$parKey.'="'.$parVal.'" ';
			}
	    }
	    
		return '<input id="'.$Id.'" name="'.$Name.'" type="text" class="textbox_name '.$OtherClass.'" '.$OtherParValue.' value="'.intranet_htmlspecialchars($Value).'" />';
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.cookies.2.2.0.js"></script>
			
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
			';
			
		return $x;
	}
	
	function Include_AutoComplete_JS_CSS()
	{
		global $PATH_WRT_ROOT;
		
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
		';
		
		return $x;
	}

	function Include_DatePicker_JS_CSS()
	{
		global $PATH_WRT_ROOT;
		
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.js"></script>
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.css" type="text/css" />
		';
		
		return $x;
	}
	
	function Include_ReplySlip_JS_CSS()
	{
		global $PATH_WRT_ROOT, $replySlipConfig, $intranet_session_language;
		
		ob_start();
			include_once($PATH_WRT_ROOT.'home/common_reply_slip/js_object.php');
			$x = ob_get_contents();
		ob_end_clean();
		
		return $x;
	}
		
	function Get_Form_Warning_Msg($DivID='', $Msg='', $Class='', $display=false)
	{
		$displayAttr = ($display)? 'block' : 'none';
		$x .= '<div id="'.$DivID.'" class="'.$Class.'" style="display:'.$displayAttr.';">'."\n";
			$x .= '<span class="tabletextrequire">* '.$Msg.'</span>'."\n";
		$x .= '</div>'."\n";
		
		return $x; 
	}	
	
	function Get_Form_Sub_Title_v30($Title)
	{
		$x .= '<div class="form_sub_title_v30">'."\n"; 
	        $x .= '<em>'."\n"; 
	      	  $x .= '- <span class="field_title">'.$Title.' </span>-'."\n"; 
	        $x .= '</em>'."\n"; 
	        $x .= '<p class="spacer"></p>'."\n";
	    $x .= '</div>'."\n";
	    
	    return $x;
	}
	
	function MultiSelectionRemark()
	{
		global $Lang;
		$x = "
			<span class=\"tabletextremark\">
				".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."
			</span>
		";
		
		return $x;
	}
	
	function Include_Thickbox_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>'."\r\n";
		$x .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />'."\r\n";
		
		return $x;
	}
	
	function Include_SuperTable_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<link href="'.$PATH_WRT_ROOT.'/templates/'.$LAYOUT_SKIN.'/css/superTables.css" rel="stylesheet" type="text/css">'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.superTable.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/superTables.js"></script>'."\n";
		
		return $x;
	}
	
	function Include_Jquery_UI_JS()
	{
		global $PATH_WRT_ROOT;
		
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>'."\n";
		
		return $x;
	}
	
	function Include_CopyPaste_JS_CSS($version='')
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		if($version == '2016'){
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/copypaste_2016.js"></script>'."\n";
		}
		else{
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/copypaste.js"></script>'."\n";
		}
		
		return $x;
	}
	
	function Include_Excel_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.excel.js"></script>'."\n"; 
		
		return $x;
	}
	
	function Include_Cookies_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.cookies.2.2.0.js"></script>'."\n";
		
		return $x;
	}
	function Include_HighChart_JS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/highchart/highcharts.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/highchart/highcharts-more.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/highchart/modules/exporting.js"></script>'."\n";
				
		return $x;
	}
	
	function Include_TableExport($XLXS=false, $ver5=false){
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		$x = '';
        if($ver5){
            $x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-3.3.1.min.js"></script>'."\n";
            $x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/tableExport5/FileSaver.min.js"></script>'."\n";
            $x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/tableExport5/tableexport.js"></script>'."\n";
        }else{	
    		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/tableExport/FileSaver.min.js"></script>'."\n";
    		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/tableExport/tableexport.js"></script>'."\n";
	    }
		if($XLXS){
			$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/tableExport/xlsx.core.min.js"></script>';
		}
		return $x;
	}
	
	function Include_OverlayTextarea_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.overlayTextarea.js"></script>'."\n";
		
		return $x;
	}
	
	
	
	# Param $css = normal/available/suspend/waiting/drafted 
	function GET_RECORD_INDICATION_IP25($IndicationArr)
	{
		$x.= '<div class="record_indication">'."\n";
			$x.= '<ul>'."\n";
				foreach((array)$IndicationArr as $Indication)
				{
					list($css, $display) =  (array)$Indication;
					$x.= '<li class="title_row_'.$css.'"><em>&nbsp;</em><span>'.$display.'</span></li>'."\n";
				}
			$x.= '</ul>'."\n";
			$x.= '<p class="spacer"></p>'."\n";
		$x.= '</div>'."\n";
		
		return $x;
	}
	
	function Get_MultipleSelection_And_SelectAll_Div($Selection, $SelectAllBtn, $SpanID='')
	{
		global $Lang;
		
		$x = '';
		$x .= '<div>'."\n";
			$x .= '<span id="'.$SpanID.'">'.$Selection.'</span>'.$SelectAllBtn."\n";
			$x .= '<br />'."\n";
			$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Arrow_More_Icon($ParTitle='', $ParOnClick='')
	{
		global $image_path, $LAYOUT_SKIN;
		
		if ($ParOnClick != '') {
			$onclick = 'onClick="'.$ParOnClick.'"';
		}
		
		$x = '';
		$x .= '<a href="javascript:void(0);" class="tablelink" '.$onclick.'>'."\n";
			$x .= $ParTitle;
			$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle">'."\n";
		$x .= '</a>'."\n";
		
		return $x;
	}
	
	function Get_Approved_Image($Title='')
	{
		global $Lang, $LAYOUT_SKIN;
		$Title = ($Title=='')? $Lang['General']['Approved'] : $Title;
		return '<img title="'.$Title.'" src="/images/'.$LAYOUT_SKIN.'/icon_approve_b.gif" border="0" align="absmiddle" />';
	}
	
	function Get_Rejected_Image($Title='')
	{
		global $Lang, $LAYOUT_SKIN;
		$Title = ($Title=='')? $Lang['General']['Rejected'] : $Title;
		return '<img title="'.$Title.'" src="/images/'.$LAYOUT_SKIN.'/icon_reject_l.gif" border="0" align="absmiddle" />';
	}
	
	function Get_Pending_Image($Title='')
	{
		global $Lang, $LAYOUT_SKIN;
		$Title = ($Title=='')? $Lang['General']['Pending'] : $Title;
		return '<img title="'.$Title.'" src="/images/'.$LAYOUT_SKIN.'/icon_waiting.gif" border="0" align="absmiddle" />';
	}
	
	function Get_Tick_Image()
	{
        global $LAYOUT_SKIN;
		return '<img src="/images/'.$LAYOUT_SKIN.'/icon_present.gif" border="0" align="absmiddle" />';
	}
	
	function Get_Excel_Image()
	{
	    global $LAYOUT_SKIN;
		return '<img src="/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" />';
	}
	
	function Get_CSV_Sample_Download_Link($ParHref, $ParText='', $OtherAttributes='')
	{
		global $Lang;
		
		$Text = ($ParText=='')? $Lang['General']['ClickHereToDownloadSample'] : $ParText;
		
		$x = '';
		$x .= '<a class="tablelink" '.$OtherAttributes.' href="'.$ParHref.'">'."\n";
			$x .= $this->Get_Excel_Image()." ".$Text."\n";
		$x .= '</a>'."\n";
		
		return $x;
	}
	
	function Get_Import_Progress_Msg_Span($CurrentNum, $TotalNum, $Msg='')
	{
		global $Lang;
		
		$Msg = ($Msg=='')? $Lang['General']['ImportArr']['RecordsProcessed'] : $Msg;
		
		$x = '';
		$x .= '<span id="BlockUI_Progress_Span">';
			$x .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">'.$CurrentNum.'</span> / '.$TotalNum, $Msg);
		$x .= '</span>';
		
		return $x;
	}
	
	function Get_Form_Separate_Title($Title)
	{
		return '<em class="form_sep_title">- '.$Title.' -</em>';
	}
	
	function GET_TAB_MENU($ParArr="")
	{
		$x = "";
		$x .= "<table border=\"0\" cellpadding=\"7\" cellspacing=\"0\" width=\"100%\">";
        $x .= "<tbody>";
        $x .= "<tr>";
        $x .= "<td class=\"tab_underline\" nowrap>";
        $x .= "<div class=\"shadetabs\">";
        $x .= "<ul>";

        foreach($ParArr as $key => $element) {
	        $Link = $element[0];
	        $Title = $element[1];
	        $Selected = $element[2];
	        $ID = $element[3];
	        
	        if($Selected == 1)
	        $ClassStyle = "selected";
	        else
	    	$ClassStyle = "";
	        
       		$x .= "<li id=\"".$ID."\" class=\"tab_menu ".$ClassStyle."\"><a href=\"".$Link."\"><strong>".$Title."</strong></a></li>";
        }
            	
        $x .= "</ul>";
        $x .= "</div>";
        $x .= "</td>";
        $x .= "</tr>";
        $x .= "</tbody>";
        $x .= "</table>";

		return $x;
	}
	

	/**
	 * This function is to generate a selection-pair. IMPORTANT: You MUST implment the add/remove javascript yourself.
	 * Reference: http://192.168.0.146:31002/home/eAdmin/StudentMgmt/medical/?t=settings.groupFilterSetting
	 * 
	 * Available setting,
	 * 	deselectedHeader: The header of deselected selection box(Default: '')
	 * 	deselectedSelectionName: The HTML name/id of deselected selection box(Default: 'deselected'), The HTML name will automatically add '[]' to the end.
	 * 	deselectedList: The select option of deselected selection box(Default: array() ), Array of array( 'name'=>'', 'value'=>'' )
	 * 	selectedHeader: The header of selected selection box(Default: '')
	 * 	selectedSelectionName: The HTML name/id of selected selection box(Default: 'selected'), The HTML name will automatically add '[]' to the end.
	 * 	selectedList: The select option of selected selection box(Default: array() ), Array of array( 'name'=>'', 'value'=>'' )
	 * 	btnAddAllID: The HTML id of the add all button(Default: 'AddAll')
	 * 	btnAddID: The HTML id of the add button(Default: 'Add')
	 * 	btnRemoveID: The HTML id of the remove button(Default: 'Remove')
	 * 	btnRemoveAllID: The HTML id of the remove all button(Default: 'RemoveAll')
	 */
	function generateUserSelection($settingList=array()){
		global $Lang;
		
		$deselectedHeader = (empty($settingList['deselectedHeader']))? '': $settingList['deselectedHeader'];
		$deselectedSelectionName = (empty($settingList['deselectedSelectionName']))? 'deselected' : $settingList['deselectedSelectionName'];
		$deselectedList = (empty($settingList['deselectedList']))? array() : $settingList['deselectedList'];
		$selectedHeader = (empty($settingList['selectedHeader']))? '' : $settingList['selectedHeader'];
		$selectedSelectionName = (empty($settingList['selectedSelectionName']))? 'selected' : $settingList['selectedSelectionName'];
		$selectedList = (empty($settingList['selectedList']))? array() : $settingList['selectedList'];
		$btnAddAllID = (empty($settingList['btnAddAllID']))? 'AddAll' : $settingList['btnAddAllID'];
		$btnAddID = (empty($settingList['btnAddID']))? 'Add' : $settingList['btnAddID'];
		$btnRemoveID = (empty($settingList['btnRemoveID']))? 'Remove' : $settingList['btnRemoveID'];
		$btnRemoveAllID = (empty($settingList['btnRemoveAllID']))? 'RemoveAll' : $settingList['btnRemoveAllID'];
		
		$html = <<<HTML
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%" bgcolor="#EEEEEE" class="steptitletext">
					{$deselectedHeader}
				</td>
				<td width="40"></td>
				<td width="50%" bgcolor="#EFFEE2" class="steptitletext">
					{$selectedHeader}
				</td>
			</tr>
			<tr>
				<td bgcolor="#EEEEEE" align="center">
					<div id="{$deselectedSelectionName}List">
						<select name="{$deselectedSelectionName}[]" id="{$deselectedSelectionName}" size="10" style="width:99%" multiple="true">
HTML;
	
		foreach ($deselectedList as $item) {
			$html .= '<option value="'.$item['value'].'">'.$item['name'].'</option>';
		}
		
		$html .= <<<HTML
						</select>
					</div>
					<span class="tabletextremark">{$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']}</span>
				</td>
				<td>
					<input id="{$btnAddAllID}" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="{$Lang['Btn']['AddAll']}"><br />
					<input id="{$btnAddID}" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="{$Lang['Btn']['AddSelected']}"><br /><br />
					<input id="{$btnRemoveID}" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="{$Lang['Btn']['RemoveSelected']}"><br />
					<input id="{$btnRemoveAllID}" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="{$Lang['Btn']['RemoveAll']}">
				</td>
				<td bgcolor="#EFFEE2">
					<select name="{$selectedSelectionName}[]" id="{$selectedSelectionName}" size="10" style="width:99%" multiple="true">
HTML;
	
		foreach ($selectedList as $item) {
			$html .= '<option value="'.$item['value'].'">'.$item['name'].'</option>';
		}
		
		$html .= <<<HTML
					</select>
				</td>
			</tr>
		</table>
HTML;
		return $html;
	}
	
	function RequiredSymbol() {
		$x .= '<span class="tabletextrequire">*</span>';
		return $x;
	} 
	
	function OptionalSymbol() {
		return '<span class="tabletextrequire">#</span>';
	}
	
	function Get_Empty_Image($Height)
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		$emptyImagePath = $PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/10x10.gif";
		
		return "<img src='".$emptyImagePath."' height='".$Height."'>";
	}
	
	function Get_Empty_Row($Height, $NumOfRow=1)
	{
		global $PATH_WRT_ROOT;
		
		$x = '';
		for ($i=0; $i<$NumOfRow; $i++)
			$x .= "<tr><td>".$this->Get_Empty_Image($Height)."</td></tr>";
		
		return $x;
	}
	
	function Get_Empty_Image_Div($Height)
	{
		return "<div>".$this->Get_Empty_Image($Height)."</div>";
	}
	
	# please call functions checkOption(SelectionObj);	checkOptionAll(SelectionObj); before submit the form
	# call GetCommonAttachmentFolderPath(in lib.php) to get the folder path in the update page. 
	# e.g. $FolderPath = "/file/ebooking/attachment/$foldername";   
	function Get_Upload_Attachment_UI($FormName, $ID_Name, $FolderPath, $size='')
	{
		if(empty($FormName) || empty($ID_Name) || empty($FolderPath) || trim(str_replace("/","",$FolderPath))=='') return false;
		
		global $Lang, $PATH_WRT_ROOT, $file_path;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/libfiletable.php");
		
		$li = new libfilesystem();
		$path = $file_path.$FolderPath;
		
		if(!empty($FolderPath) && dirname($path) != dirname($file_path)) // avoid remove_recursive from root path
		{
			$tmp_path = $path."tmp";
			$li->folder_remove_recursive($tmp_path);
			$li->folder_new($tmp_path);
			$li->folder_content_copy($path, $tmp_path);
		}
		
		$lo = new libfiletable("", $tmp_path, 0, 0, "");
		$files = $lo->files;
		
		if(!is_array($ID_Name) || sizeof($ID_Name)==1)
		{
			$ID_Name = (array)$ID_Name;
			$tagInfo = ' id="'.$ID_Name[0].'" name="'.$ID_Name[0].'" ';
			$ID = $ID_Name[0];
		}
		else if(sizeof($ID_Name)==2)
		{
			$tagInfo = ' id="'.$ID_Name[0].'" name="'.$ID_Name[1].'" ';
			$ID = $ID_Name[0];
		}
		
		if(trim($size)=='')
			$size = 4;
			
		$select .= '<select '.$tagInfo.' multiple size='.$size.' >'."\n";
			$select .= '<option>'.str_repeat("&nbsp;",20).'</option>';
		$select .= '</select>'."\n";
		
		$x .= '<table cellspacing="1" cellpadding="1" border="0">'."\n";
			$x .= '<tbody>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $select."\n";
					$x .= '</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_BTN($Lang['Btn']['AttachFiles'], "button", "newWindow('".$PATH_WRT_ROOT."home/common_attachment/attach.php?folder=".urlencode($FolderPath)."&SelectionID=".urlencode($ID)."&FormName=$FormName',2)", $ParName="", $ParOtherAttribute="")."\n";
						$x .= '<br>'."\n";
						$x .= $this->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.".$FormName.".elements['".$ID."']); ", $ParName="", $ParOtherAttribute="")."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= $this->MultiSelectionRemark()."\n";
		$x .= '<input type="hidden" name="FolderPath" value="'.$FolderPath.'">'."\n";
		$x .= '<script>'."\n";
		$x .= 'var obj = document.'.$FormName.'.elements["'.$ID.'"];'."\n";
		$x .= 'checkOptionClear(obj);'."\n";
		while (list($key, $value) = each($files))
			 $x .= 'checkOptionAdd(obj, \''.$files[$key][0].' ('.convert_size($files[$key][1]).')\', \''.$files[$key][0].'\');'."\n";;
		$x .= 'checkOptionAdd(obj, "'.str_repeat(" ",40).'", "");'."\n";
		$x .= '</script>'."\n";
		
		return $x;
	}
	
	function Get_Attachment_Icon()
	{
		global $image_path, $LAYOUT_SKIN;
		$x = '<img src="'."{$image_path}/{$LAYOUT_SKIN}".'/icon_attachment.gif" align="absmiddle" border="0">';
		return $x;
	}

	//GENERATE THE DISPLAY THAT INPUT FROM A TEXT AREA , SUPPORT INDENTION , NEW LINE, CONVERT HTML TAB 
	function Get_Display_From_TextArea($displayVal, $convertEntities=true){
		
		if($convertEntities) {
			//convert all the HTML tag
			$displayVal = htmlspecialchars($displayVal);
		}

		//convert the indention
		$displayVal = preg_replace('/ /','&nbsp;',$displayVal);

		//convert to new line
		$displayVal = nl2br($displayVal);


		$displayVal = stripslashes($displayVal);
		return $displayVal;
	}
	
	// $ext = '.xls', '.doc' (with the "." at the beginning)
	function Get_File_Type_Icon($ext, $fileName='') {
		global $LAYOUT_SKIN;
		
		if ($ext == '') {
			$ext = get_file_ext($fileName);
		}
		$ext = strtolower($ext);
		
		$fileExtensionAry = array(	
									".avi"=>"avi",
									".bmp"=>"jpg",
									".csv"=>"xls",
									".doc"=>"doc",
									".docx"=>"doc",
									".eml"=>"eml",
									".fla"=>"fla",
									".flv"=>"avi",
									".gif"=>"jpg",
									".htm"=>"htm",
									".html"=>"htm",
									".jpg"=>"jpg",
									".jpeg"=>"jpg",
									".mov"=>"avi",
									".mp3"=>"mp3",
									".mp4"=>"avi",
									".pdf"=>"pdf",
									".png"=>"jpg",
									".ppt"=>"ppt",
									".pptx"=>"ppt",
									".rar"=>"zip",
									".rmvb"=>"avi",
									".swf"=>"swf",
									".txt"=>"txt",
									".wav"=>"mp3",
									".wma"=>"mp3",
									".wmv"=>"avi",
									".xls"=>"xls",
									".xlsx"=>"xls",
									".xml"=>"text",
									".zip"=>"zip"
								);
		$iconType = (isset($fileExtensionAry[$ext]))? $fileExtensionAry[$ext] : 'file';
		
		return '<img align="absmiddle" src="/images/'.$LAYOUT_SKIN.'/icon_files/'.$iconType.'.gif">';
	}	
	
	function Include_TableReplySlip_JS_CSS()
	{
		global $PATH_WRT_ROOT, $tableReplySlipConfig, $intranet_session_language;
		
		ob_start();
			include_once($PATH_WRT_ROOT.'home/common_table_reply_slip/js_object.php');
			$x = ob_get_contents();
		ob_end_clean();
		
		return $x;
	}
	
	function Include_Plupload_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x  = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.gears.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.silverlight.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.flash.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.browserplus.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.html4.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/plupload.html5.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/plupload/jquery.plupload.queue/jquery.plupload.queue.php"></script>'."\n";
		$x .= '<link rel=stylesheet href="'.$PATH_WRT_ROOT.'templates/jquery/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css">'."\n";
		
		return $x;
	}
	
	function GET_SINGLETEXTBOX_GROUP($numOfBox,$inputName,$defaultValue=array(),$OtherClass='',$disabledArr=array(),$additionHTMLArr=array()){
		$html = '';
		
		for($i=0; $i<$numOfBox; $i++){
			$_Id = $inputName.$i;
			$_nextID = $inputName.($i+1);
			$_name = $inputName.'[]';
			if($i == ($numOfBox-1)){
				$_js = "";
			}
			else{
				# js function focusNext() please find in script.js
				$_js = " setTimeout(  'focusNext( document.getElementById(\'$_Id\'), document.getElementById(\'$_nextID\'),1)' ,100)";		
			}
			
			# other Parm
			$otherParArr = array();
			$otherParArr["tabIndex"] = ($i+1);
			$otherParArr["maxlength"] = 1;
			$otherParArr["onkeypress"] = $_js;
			$otherParArr["onfocus"] = "this.select();";
			$otherParArr["style"] = "width:20px;";
			
			# Disable specific box
			if($disabledArr[$i]==1){
				$otherParArr["disabled"] = "disabled";
			}
			if(!empty($additionHTMLArr[$i])){
				if($additionHTMLArr[$i]['before']){
					$beforehtml = $additionHTMLArr[$i]['before'];
				}
				else{
					$beforehtml = '';
				}
				if($additionHTMLArr[$i]['after']){
					$afterhtml = $additionHTMLArr[$i]['after'];
				}
				else{
					$afterhtml = '';
				}
			}
			else{
				$beforehtml = '';
				$afterhtml = '';
			}
			
			$html .= $beforehtml;
			$html .= $this->GET_TEXTBOX($_Id,$_name,$defaultValue[$i],$OtherClass, $otherParArr);
			$html .= $afterhtml;
			$html .= '&nbsp;';
		}
		return $html;
	}
	
	function getReferenceThickBox($titleArray, $RemarkDetailsArray, $header='', $label=''){
		global $PATH_WRT_ROOT,$image_path,$LAYOUT_SKIN;
		$title = '<tr>';
		foreach($titleArray as $titleText){
			$title .= '<th>'.$titleText.'</th>';
		}
		$title .= '</tr>';
		
		foreach($RemarkDetailsArray as $RemarkDetails){
			$row = '';
			foreach($RemarkDetails as $details){
				$row .= '<td class="tablerow1">'.$details.'</td>';
			}
			$RemarkDetailsArr[] = $row;
		}
		$RemarksLayer .= '';
		$thisRemarksType = 'type';
				//$RemarksLayer .= '<div id="remarkDiv_type class="selectbox_layer" style="width:400px;">'."\r\n";
        		$RemarksLayer .='<div class = "stickyHeader" id= "stickyRemarks" style = "background-color: #f3f3f3;position: sticky; top: 0; margin-bottom: 1px;">';
        		if($label != "Site"){
        		    $RemarksLayer .= '<div style="display: inline;">';
        		    $RemarksLayer .= ($header!='')?$this->GET_NAVIGATION2($header):'&nbsp';
        		    $RemarksLayer .= '</div>';
        		    $RemarksLayer .= '<div style="display: inline; float: right; padding-right: 2px;">';
        		    $RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
        		    $RemarksLayer .= '</div>';
        		}
        		else{
        		    $RemarksLayer .= '<div style="display: inline;">';
        		    $RemarksLayer .= '&nbsp';
        		    $RemarksLayer .= '</div>';
        		    $RemarksLayer .= '<div style="display: inline; float: right; padding-right: 2px;">';
        		    $RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
        		    $RemarksLayer .= '</div>';
        		}        		
        		$RemarksLayer .= '</div>';
        		
				$RemarksLayer .='<div id="tableDivID" style="max-height:220px; overflow-y:auto;">'."\r\n";
    			    $RemarksLayer .= '<div class="content" id="contentDiv">';
    			    $RemarksLayer .= '<table id="container'. $label .'" cellspacing="0" cellpadding="3" border="0" align="center" max-width="550px" width=100%>'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";						
// 							$RemarksLayer .= '<tr>'."\r\n";
// 								$RemarksLayer .= '<td>'."\r\n";
// 									$RemarksLayer .= ($header!='')?$this->GET_NAVIGATION2($header):'';
// 								$RemarksLayer .= '</td>'."\r\n";
// 								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
// 									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
// 								$RemarksLayer .= '</td>'."\r\n";
// 							$RemarksLayer .= '</tr>'."\r\n";							
// 							$RemarksLayer .= '<tr>'/."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;" colspan="2">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30" >'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= $title."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ($RemarkDetailsArr as $RemarkDetail){
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= $RemarkDetail."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
					$RemarksLayer .= '</div>';
				$RemarksLayer .='</div>'."\r\n";
		return $RemarksLayer;
	}
	
	function getCustomizedModuleMenu($MENUS, $zindex=9998)
	{
	    $nrMenus = count($MENUS);
	    if ($nrMenus<1)
	    {
	        return "&nbsp;";
	    }
	    
	    $ReturnRX = "";
	    for ($i=0; $i<$nrMenus; $i++)
	    {
	        list($MenuTitle, $MenuPath, $MenuIsOn) = $MENUS[$i];
	        $MenuID = MD5($MenuTitle."_".$MenuPath);
	        $MenuClass = ($MenuIsOn) ? "common_current" : "common";
	        $ReturnRX .= "<li style=\"z-index: ".$zindex.";\"><a id=\"{$MenuID}\" href=\"{$MenuPath}\" class=\"{$MenuClass}\">{$MenuTitle}</a></li>\n";
	    }
	    
	    return $ReturnRX;
	}
	
	
}
?>