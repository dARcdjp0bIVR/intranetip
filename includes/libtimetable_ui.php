<?php

// Using:  

######################################
#
#   Date : 2020-05-26 Henry
#       - modified Get_View_Secondary_Settings_Table() js_PATH_WRT_ROOT parameter has been changed to '/' [Case#J185891]
#
#   Date : 2019-05-21 Cameron
#       - add function getEverydayTimeTableCalendar(), displayCalandarEverydayPreview(), getTimetableByTimestamp()
#
#   Date : 2019-05-20 Cameron   
#       - add everyday timetable tag in Get_Tag_Array()
#       - copy function Get_Mode_Toolbar from eBooking
#       - add function getEverydayTimetableFilterBar()
#
#   Date : 2019-02-25 Vito
#       - Modifcation(css style) in Get_Remarks_Layer()
#
#	Date : 2017-11-21 Frankie
#		- Modify Get_Timetable_Table for SLRS
#
#	Date : 2017-05-10 Frankie
#		- Modify Get_Timetable_Table for SLRS
#
#	Date : 2017-04-27 Frankie
#		- Modify Get_Class_Timetable for SLRS
#		- Modify Get_Timetable_Table for SLRS
#
#	Date : 2017-02-08 Frankie
#		- Modify Get_Personal_Timetable for SLRS
#		- Modify Get_Timetable_Table for SLRS
#
#	Date: 2015-12-20 Kenneth
#		- Get_Import_Lesson_Step1_UI(), Add Remark Layer for Add remarks layer for subject group, building, location, and sub-location
#		- Add Get_Remarks_Layer(), return Remark Layer UI
######################################

include_once('libtimetable.php');

if (!defined("LIBTIMETABLE_UI_DEFINED"))         // Preprocessor directives
{
	define("LIBTIMETABLE_UI_DEFINED",true);
	
	class libtimetable_ui extends Timetable {
		var $thickBoxWidth;
		var $thickBoxHeight;
		
		function libtimetable_ui() {
			parent:: Timetable();
			
			$this->thickBoxWidth = 750;
			$this->thickBoxHeight = 450;
		}
		
		function Include_JS_CSS()
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN;
			
			$x = '
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.cookies.2.2.0.js"></script>
				
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
				';
				
			return $x;
		}
		
		function Get_Allocation_Filter_Table()
		{
			global $Lang, $PATH_WRT_ROOT;
			
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			# Acadermic Year Selection
			//$acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID", 'onchange="js_Reload_Term_Selection(this.value, 0)"',0);
			$acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID", 'onchange="js_Changed_AcademicYear_Selection(this.value, 0);"',0);
			
			
			# Term Selection (load by Ajax)
			//$subject_class_mapping_ui = new subject_class_mapping_ui();
			//$term_jsOnchange = "js_Reload_Timetable_Selection('', 0);";
			//$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID", '', '', $term_jsOnchange, 0);
			
			# Timetable Selection (load by Ajax)
			//$timetable_jsOnchange = 'js_Reload_Timetable_Edit_Delete_Link(this.value);js_Reload_Timetable(this.value);';
			//$timetable_jsOnchange = 'js_Reload_Timetable_Edit_Delete_Link(this.value); Generate_Timetable(0);';
			//$timetable_jsOnchange = 'js_Changed_Timetable_Selection(this.value);';
			//$timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID", $AcademicYearID, $YearTermID, $TimetableID, $timetable_jsOnchange);
			
			# Edit Timetable icon
			$editTimetableIcon = $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit", 
										$Lang['SysMgr']['Timetable']['Edit']['Timetable'], "js_Show_Timetable_Add_Edit_Layer(); return false;", 'FakeLayer','');
				
			# Delete Timetable Icon
			$deleteTimetableIcon = $libinterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Timetable']['Delete']['Timetable'], "js_Delete_Timetable(); return false;", "delete");
			
			# Add Timetable icon
			$addTimetableIcon = $libinterface->Get_Thickbox_Div($this->thickBoxHeight, $this->thickBoxWidth, "add", 
									$Lang['SysMgr']['Timetable']['Add']['Timetable'], "js_Show_Timetable_Add_Edit_Layer(1); return false;", 'FakeLayer', '', "addTimetableDiv");
			
			# Clone Button
			//$cloneBtn = $libinterface->GET_SMALL_BTN($Lang['Btn']['Clone'], "button", "js_Clone_Timetable()", "CloneBtn");
			
			# Display Option Button
			$displayOption_jsOnchange = "js_Show_Hide_Display_Option_Div(); return false;";
			$displayOptionBtn = '<a href="#" class="tablelink" onclick="'.$displayOption_jsOnchange.'"> '.$Lang['SysMgr']['Timetable']['DisplayOption'].'</a>'."\n";
			
			# Display Option Layer Table
			$displayOptionTable = $this->Get_Display_Option_Div_Table();
			
			# Filter Button
			$filter_jsOnchange = "js_Show_Hide_Filter_Div(); return false;";
			$filterBtn = '<a href="#" class="tablelink" onclick="'.$filter_jsOnchange.'"> '.$Lang['SysMgr']['Timetable']['DataFiltering'].'</a>'."\n";
			
			# Filter Layer Table
			$filterTable = $this->Get_View_Selection_Div_Table();
			
			
			
			### Main Display
			$table = '';
			//$table .= "<a href='void(0);' onclick='javascript:alert($.cookies.get(arrCookies[5]));return false;'>debug</a>";
			$table .= '<div id="SelectionTableDiv" style="float:left">'."\n";
				$table .= '<table id="SelectionTable">'."\n";
					$table .= '<tr>'."\n";
						# Acadermic Year
						$table .= '<td style="vertical-align:top">'.$acadermicYearSelection.'</td>'."\n";
						# Term
						$table .= '<td style="vertical-align:top">'."\n";
							$table .= '<div id="TermSelectionDiv">'."\n";
								$table .= $termSelection;
							$table .= '</div>'."\n";
						$table .= '</td>'."\n";
						# Timetable
						$table .= '<td style="vertical-align:top">'."\n";
							$table .= '<div id="TimetableSelectionDiv">'."\n";
								$table .= $timetableSelection;
							$table .= '</div>'."\n";
						$table .= '</td>'."\n";
						# Edit Button
						$table .= '<td style="vertical-align:top">'."\n";
							$table .= '<div id="editTimetableDiv" class="table_row_tool" style="display:none;">'."\n";
								$table .= $editTimetableIcon;
							$table .= '</div>'."\n";
						$table .= '</td>'."\n";
						# Delete Button
						$table .= '<td style="vertical-align:top">'."\n";
							$table .= '<div id="deleteTimetableDiv" style="display:none;">'."\n";
								$table .= $deleteTimetableIcon;
							$table .= '</div>'."\n";
						$table .= '</td>'."\n";
						# Add Button
						$table .= '<td style="vertical-align:top">'."\n";
							$table .= $addTimetableIcon;
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				$table .= '</table>'."\n";
			$table .= '</div>'."\n";
			
			# Mode Filter
			$table .= '<div id="FilterButtonDiv" style="float:right;display:none;">'."\n";
				$table .= '<table id="FilterButtonTable">'."\n";
					$table .= '<tr>'."\n";
						# Filter button
						$table .= '<td>'."\n";
								$table .= $displayOptionBtn;
								$table .= ' | ';
								$table .= $filterBtn;
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				$table .= '</table>'."\n";
			$table .= '</div>'."\n";
			$table .= '<div style="clear:both">'."\n";
			
			$table .= '<div style="float:right;position:relative;left:-200px;z-index:99;">'."\n";
				$table .= '<table>'."\n";
					$table .= '<tr>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="DisplayOptionDiv" class="selectbox_layer" style="width:200px;">'.$displayOptionTable.'</div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				$table .= '</table>'."\n";
			$table .= '</div>'."\n";
			
			$table .= '<div style="float:right;position:relative;left:-500px;z-index:99;">'."\n";
				$table .= '<table>'."\n";
					$table .= '<tr>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="FilterSelectionDiv" class="selectbox_layer" style="width:500px;overflow:auto;">'.$filterTable.'</div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				$table .= '</table>'."\n";
			$table .= '</div>'."\n";
			
			$table .= '<div class="FakeLayer"></div>'."\n";
				
			return $table;
		}
		
		function Get_Timetable_Selection($ID_Name, $AcademicYearID='', $YearTermID='', $SelectedTimetableID='', $OnChange='', $noFirst=0, $ParFirstTitle='', $ShowAllocatedOnly=0, $ExcludeTimetableID='', $IncludeTimetableIDArr='', $ReleaseStatus='')
		{
			global $Lang;

			# Use current year and term if not specified
			if ($AcademicYearID == '')
			{
				$curYearInfoArr = getCurrentAcademicYearAndYearTerm();
				$AcademicYearID = $curYearInfoArr['AcademicYearID'];
				$YearTermID = $curYearInfoArr['YearTermID'];
			}
			
			$timetableArr = $this->Get_All_TimeTable($AcademicYearID, $YearTermID, $ShowAllocatedOnly, $ReleaseStatus);
			$numOfTimetable = count($timetableArr);
			
			$selectArr = array();
			if ($YearTermID != '')
			{
				for ($i=0; $i<$numOfTimetable; $i++)
				{
					$thisTimetableID = $timetableArr[$i]['TimetableID'];
					$thisTimetableName = $timetableArr[$i]['TimetableName'];
					
					if ($thisTimetableID == $ExcludeTimetableID) {
						continue;
					}
						
					if (is_array($IncludeTimetableIDArr) && !in_array($thisTimetableID, $IncludeTimetableIDArr)) {
						continue;
					}
					
					$selectArr[$thisTimetableID] = $thisTimetableName;
				}
			}
			
			$onchange = '';
			if ($OnChange != "")
				$onchange = 'onchange="'.$OnChange.'"';
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			$firstTitle = ($ParFirstTitle=='')? $Lang['SysMgr']['Timetable']['Select']['Timetable'] : $ParFirstTitle;
			$firstTitle = Get_Selection_First_Title($firstTitle);
			
			$timetableSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedTimetableID, $all=0, $noFirst, $firstTitle);
			
			return $timetableSelection;
		}
		
		function Get_Timetable_Add_Edit_Layer($AcademicYearID, $YearTermID, $TimetableID='')
		{
			global $Lang, $PATH_WRT_ROOT, $intranet_session_language;
			
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			# Add / Edit
			$isEdit = ($TimetableID=='')? false : true;
			
			# Title
			$thisTitle = ($isEdit)? $Lang['SysMgr']['Timetable']['Edit']['Timetable'] : $Lang['SysMgr']['Timetable']['Add']['Timetable'];
			
			if ($isEdit)
			{
				# Button Submit
				$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
									$onclick="js_Insert_Edit_Timetable('$AcademicYearID', '$YearTermID', '$TimetableID')", $id="Btn_Edit");
			}
			else
			{
				# Button Add
				$btn_Add = $libinterface->GET_ACTION_BTN($Lang['Btn']['Add'], "button", 
									$onclick="js_Insert_Edit_Timetable('$AcademicYearID', '$YearTermID', '')", $id="Btn_Add");
			}
			
			# Cancel
			$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
								$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
								
			# Get Academic Year info
			include_once("form_class_manage.php");
			$academicYearObj = new academic_year($AcademicYearID);
			$academicYearName = $academicYearObj->Get_Academic_Year_Name();
			
			# Academic Year Selection for copying
			//$acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID_ForCopyTimetable", 'onchange="js_Reload_Term_Selection(this.value, 1)"', 1, 0, $AcademicYearID);
			$acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID_ForCopyTimetable", 'onchange="js_Changed_AcademicYear_Selection(this.value, 1);"', 1, 0, $AcademicYearID);
			
			# Get Term info
			$yearTermObj = new academic_year_term($YearTermID);
			$yearTermName = $yearTermObj->Get_Year_Term_Name();
			
			# Term Selection for copying
			include_once("subject_class_mapping_ui.php");
			$subject_class_mapping_ui = new subject_class_mapping_ui();
			$term_jsOnchange = "js_Reload_Timetable_Selection('', 1);";
			$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID_ForCopyTimetable", $AcademicYearID, $YearTermID, $term_jsOnchange, 1);
			
			# Timetable Selection for copy
			$timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID_ForCopyTimetable", $AcademicYearID, $YearTermID, '', '', 1, '', 0, $TimetableID);
			
			# Show Timetable info if it is edit mode
			if ($isEdit) {
				$thisTimetableObj = new Timetable($TimetableID);
				$thisTimetableName = intranet_htmlspecialchars($thisTimetableObj->TimetableName);
				
				$thisReleaseStatus = $thisTimetableObj->ReleaseStatus;
				if ($thisReleaseStatus) {
					$releaseStatusPublicChecked = true;
					$releaseStatusPrivateChecked = false;
				}
				else {
					$releaseStatusPublicChecked = false;
					$releaseStatusPrivateChecked = true;
				}
			}
			else {
				$thisTimetableName = intranet_htmlspecialchars($academicYearName." ".$yearTermName);
				
				$releaseStatusPublicChecked = true;
				$releaseStatusPrivateChecked = false;
			}
			
			# Title Input box
			$onkeyup = "js_Vaildate_Timetable_Title('$YearTermID', '$TimetableID')";
			$titleInput = '<input id="TimetableTitle" name="TimetableTitle" type="text" class="textbox" maxlength="255" value="'.$thisTimetableName.'" onkeyup="'.$onkeyup.'" />'."\n";
			
			$x = '';
			$x .= '<div id="debugArea"></div>'."\n";
			$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
				$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
				//$x .= '<h1> '.$Lang['SysMgr']['Timetable']['ModuleTitle'].' &gt; <span>'.$thisTitle.'</span></h1>'."\n";
				$x .= '<div class="edit_pop_board_write">'."\n";
					$x .= '<form id="LocationForm" name="LocationForm">'."\n";
						$x .= '<table class="form_table">'."\n";
							$x .= '<col class="field_title" />'."\n";
							$x .= '<col class="field_c" />'."\n";
							
							# Academic Year Display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['AcademicYear'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'.$academicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Year Term Display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $yearTermName;
								$x .= '</td>'."\n"; # drop down list
							$x .= '</tr>'."\n";
							
							# Title input
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Title'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $titleInput;
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Status
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['General']['Status'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $libinterface->Get_Radio_Button('releaseStatus_public', 'releaseStatus', $value=1, $releaseStatusPublicChecked, "", $Lang['General']['Public']);
									$x .= '&nbsp;&nbsp;';
									$x .= $libinterface->Get_Radio_Button('releaseStatus_private', 'releaseStatus', $value=0, $releaseStatusPrivateChecked, "", $Lang['General']['Private']);
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Copy From
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Copy'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $libinterface->Get_Radio_Button('isCopy_Yes', 'isCopy', $value=1, $checked=0, "", $Lang['General']['Yes'], "js_Show_Hide_Copy_Option_Row()");
									$x .= '&nbsp;&nbsp;';
									$x .= $libinterface->Get_Radio_Button('isCopy_No', 'isCopy', $value=0, $checked=1, "", $Lang['General']['No'], "js_Show_Hide_Copy_Option_Row()");
									$x .= '<br />';
									# Copy From Options
									$x .= '<table id="copySelectionTable" class="form_table" style="display:none; border:1px solid #CCCCCC;">'."\n";
										$x .= '<col class="field_title" />'."\n";
										$x .= '<col class="field_c" />'."\n";
										$x .= '<tr id="AcademicYearRow">'."\n";
											$x .= '<td>'.$Lang['SysMgr']['Timetable']['AcademicYear'].'</td>'."\n";
											$x .= '<td>:</td>'."\n";
											$x .= '<td>'."\n";
												$x .= $acadermicYearSelection;
											$x .= '</td>'."\n";
										$x .= '</tr>'."\n";
										
										$x .= '<tr id="TermRow">'."\n";
											$x .= '<td>'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
											$x .= '<td>:</td>'."\n";
											$x .= '<td>'."\n";
												$x .= '<div id="TermSelectionDiv_ForCopyTimetable">'."\n";
													$x .= $termSelection;
												$x .= '</div>'."\n";
												$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TermWarningDiv_ForCopyTimetable");
											$x .= '</td>'."\n";
										$x .= '</tr>'."\n";
										
										$x .= '<tr id="TimetableRow">'."\n";
											$x .= '<td>'.$Lang['SysMgr']['Timetable']['Timetable'].'</td>'."\n";
											$x .= '<td>:</td>'."\n";
											$x .= '<td>'."\n";
												$x .= '<div id="TimetableSelectionDiv_ForCopyTimetable">'."\n";
													$x .= $timetableSelection;
												$x .= '</div>'."\n";
											$x .= '</td>'."\n";
										$x .= '</tr>'."\n";
									$x .= '</table>';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							
							
						$x .= '</table>'."\n";
					$x .= '</form>'."\n";
				$x .= '</div>'."\n";
				
				$x .= '<div class="edit_bottom">'."\n";
					$x .= '<span></span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					if ($isEdit) {
						$x .= $btn_Edit."\n";
					}
					else {
						$x .= $btn_Add."\n";
					}
					$x .= $btn_Cancel."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		function Get_Timetable_Table($TimetableID, $isViewMode=0, $SubjectGroupID_DisplayFilterArr=array(), $LocationID_DisplayFilterArr=array(),
										 $OthersLocationFilter='', $Day='', $TimeSlotID='', $isSmallFont=0, $MapTimeZoneDay=0, $IndicateCurDay=0, $targetUserID = "")
		{
			global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $plugin;
			
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			include_once($PATH_WRT_ROOT."includes/liblocation.php");
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

			if ($TimetableID == '')
				return '';
				
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			# Get Timetable Info
			$objTimetable = new Timetable($TimetableID);
			$libcycleperiods = new libcycleperiods();
			
			# Double check the term of the timetable
			# If past term => view only
			$thisAcademicYearID = $objTimetable->AcademicYearID;
			$thisYearTermID = $objTimetable->YearTermID;
			
			include_once("form_class_manage.php");
			$lib_form_class_manage = new form_class_manage();
			if ($lib_form_class_manage->Has_Term_Past($thisYearTermID))
				$isViewMode = 1;
			
			$currDate = date("Y-m-d");
			/********************************************************/
			$cyclePeriodInfo = $libcycleperiods->getCurrentCyclePeriod();
			$thisTermFirstPeriodDays = $cyclePeriodInfo[0]["PeriodStart"];
			$thisTermLastPeriodDays = $cyclePeriodInfo[0]["PeriodEnd"];
			if (strtotime($currDate) < strtotime($thisTermFirstPeriodDays) || strtotime($currDate) > strtotime($thisTermFirstPeriodDays)) {
				$thisTermLastPeriodDays = "";
			}
			/********************************************************/
			
			# Get Cycle Info
			if ($isViewMode) {
				// $cyclePeriodInfo = $libcycleperiods->getCurrentCyclePeriod();
				$numOfCycleDays = $cyclePeriodInfo[0]["PeriodDays"];
			} else {
				$numOfCycleDays = $objTimetable->CycleDays;
			}
			
			include_once("libtimetable.php");
			$libtimetable = new Timetable();
			
			# Get Timeslot Info
			$timeSlotArr = $objTimetable->Get_Timeslot_List();
			$numOfTimeSlot = count($timeSlotArr);
			$TimeSlotIDArr = Get_Array_By_Key($timeSlotArr, 'TimeSlotID');
			
			# Get the Day Type using in the current timezone
			$TimezoneDayArr = array();
			if ($MapTimeZoneDay == 1) {
				$TimeZoneInfoArr = GetCurrentTimeZoneInfo();
				$PeriodType = $TimeZoneInfoArr[0]['PeriodType'];
				$CycleType = $TimeZoneInfoArr[0]['CycleType'];
				
				if ($PeriodType == 0) {
					// remove Sunday in the array
					$DayArr = $Lang['General']['DayType4'];
					array_shift($DayArr);
					$TimezoneDayArr = array_values((array)$DayArr);
				}
				else {
					include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
					$lcycleperiods = new libcycleperiods();
			
					if ($CycleType == 1)
						$TimezoneDayArr = $lcycleperiods->array_alphabet;
					else if ($CycleType == 2)
						$TimezoneDayArr = $lcycleperiods->array_roman;
					else 
						$TimezoneDayArr = $lcycleperiods->array_numeric;
				}
			}
			
			# Get Current Day
			$CurrentCycleDay = '';
			if ($IndicateCurDay == 1) {
				$CurrentCycleDay = $this->Get_Current_Timetable_Cycle_Day();
			}
			
			# Get Room Allocation Info (add parameters for filtering later)
			$roomAllocationArr = $objTimetable->Get_Room_Allocation_List($TimeSlotID, $Day, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr);
			
			# Get Subject Group Info
			$libSCM = new subject_class_mapping();
			// $SubjectGroupInfoArr[$SubjectGroupID] = Array Of Info
			$SubjectGroupInfoArr = $libSCM->Get_Subject_Group_Info($thisAcademicYearID, $thisYearTermID, $TimetableID);
			
			# Get Location Info
			include_once("liblocation.php");
			$liblocation = new liblocation();
			# $LocationInfoArr[$LocationID] = LocationInfoArr
			$LocationInfoArr = $liblocation->Get_Building_Floor_Room_Name_Arr($TimetableID);
			
			if ($isSmallFont) {
				$smallFontStyle = "style='font-size:9px;width:20%;'";
			}
			
			$table = '';
			$table .= '<table class="common_table_list" id="ContentTable">'."\n";
				$tempTimeSlotWidth = 10;
				$tempAddCycleDayWidth = ($isViewMode)? 0 : 2;
				if ($numOfCycleDays != 0)
					$colWidth = floor((100 - $tempTimeSlotWidth - $tempAddCycleDayWidth) / $numOfCycleDays)."%";	// 2 is the estimated percentage of the "add cycle day" column
				else
					$colWidth = "100%";
				$timeSlotWidth = 100 - ($colWidth * $numOfCycleDays) - $tempAddCycleDayWidth;	// re-calculate the time slot column width
					
				## Standardize the display of IE and Firefox
				$table .= '<col align="left" '.$smallFontStyle.' width="'.$timeSlotWidth.'%"></col>'."\n";
				if ($Day == '') {
					for ($i=0; $i<$numOfCycleDays; $i++) {
						$thisDay = $i + 1;
						# skip day if specified
						if ($Day != '' && $thisDay != $Day)
							continue;
						
						$table .= '<col align="left" width="'.$colWidth.'"></col>'."\n";
					}
				}
				else {
					$table .= '<col align="left"></col>'."\n";
				}
				if (!$isViewMode) {
					$table .= '<col></col>'."\n";
				}
				

				## Header
				$table .= '<thead>'."\n";
					# Cycle Title Display
					$table .= '<tr>'."\n";
						$table .= '<th>&nbsp;</th>'."\n";
						
						# Cycle Day Display
						for ($i=0; $i<$numOfCycleDays; $i++) {
							// Display 1,2,3...
							$thisDay = $i + 1;

							$accDate = $libtimetable->Get_NextDate_byCycleDay($thisDay, $currDate, $thisTermLastPeriodDays);
							
							# skip day if specified
							if ($Day != '' && $thisDay != $Day)
								continue;
							
							$thisActionTag = '';
							$deleteCycleDayIcon = '';
							$addCycleDayIcon = '';
							if (!$isViewMode) {
								# Show Hide delete icon event
								$thisDeleteBtnSpanID = 'DeleteCycleDays_'.$thisDay;
								$thisActionTag = ' onmouseover="js_Show_Hide_Btn_Div(\''.$thisDeleteBtnSpanID.'\', 1);" ';
								$thisActionTag .= ' onmouseout="js_Show_Hide_Btn_Div(\''.$thisDeleteBtnSpanID.'\', 0);"';
													
								# Delete Cycle Day Icon (Display only if there is more than one cycle day)
								if ($numOfCycleDays > 1) {
									$deleteCycleDayIcon = '<span id="'.$thisDeleteBtnSpanID.'" class="table_row_tool row_content_tool" style="float:right; display:none;">'."\n";
										//$deleteCycleDayIcon .= $libinterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Timetable']['Delete']['CycleDay'], "js_Delete_CycleDays('$thisDay'); return false;", "delete");
										$deleteCycleDayIcon .= $libinterface->GET_LNK_DELETE("javascript:void(0);", $Lang['SysMgr']['Timetable']['Delete']['CycleDay'], "js_Show_Delete_Option_Layer('CycleDay', '$thisDeleteBtnSpanID'); return false;", "delete");
									$deleteCycleDayIcon .= '</span>'."\n";
								}
							}
							
							if ($MapTimeZoneDay == 1)
								$thisDayDisplay = $TimezoneDayArr[$thisDay-1];
							else
								$thisDayDisplay = $Lang['SysMgr']['Timetable']['Day'].' '.$thisDay;
							
							if ($IndicateCurDay == 1 && $thisDay == $CurrentCycleDay && $accDate == $currDate) {
								$thisDayDisplay = '<span class="tabletextrequire">*</span>'.$thisDayDisplay;
							}
							if ($isViewMode && $plugin['SLRS'] && !empty($accDate)) {
								$thisDayDisplay .= " [" . $accDate . "]";
							}

							$table .= '<th style="vertical-align:top" '.$thisActionTag.'>'."\n";
								# Day Display
								$table .= '<span style="float:left">'.$thisDayDisplay.'</span>'."\n";
								# Delete Cycle Day Icon
								$table .= $deleteCycleDayIcon;
							$table .= '</th>'."\n";
						}
						
						# Add Cycle Day Icon
						if (!$isViewMode)
						{
							$table .= '<th width="20px;">'."\n";
								$table .= '<div class="table_row_tool row_content_tool">'."\n";
									// Do not use library function call as this is not calling a thickbox
									$table .= '<a href="#" class="add" title="'.$Lang['SysMgr']['Timetable']['Add']['CycleDay'].'" ';
									$table .= ' onclick="js_Add_CycleDays(); return false;" />'."\n";
								$table .= '</div>'."\n";
							$table .= '</th>'."\n";
						}
					$table .= '</tr>'."\n";
				$table .= '</thead>'."\n";
				
				# Content - Timeslot settings and Room Allocation
				$table .= '<tbody>'."\n";
					# Loop Time Slot
					// For time vaildation in the thickbox
					$lastTimeSlotID = "''";
					for ($i=0; $i<$numOfTimeSlot; $i++) {
						$thisTimeSlotID = $timeSlotArr[$i]['TimeSlotID'];
						
						// For time vaildation in the thickbox
						$nextTimeSlotID = ($i == ($numOfTimeSlot-1) )? "''" : $timeSlotArr[$i+1]['TimeSlotID'];
						
						# Display Timeslot Info
						$thisTimeSlotName = $timeSlotArr[$i]['TimeSlotName'];
						// Hour and Minute only
						$thisStartTime = substr($timeSlotArr[$i]['StartTime'], 0, 5);
						$thisEndTime = substr($timeSlotArr[$i]['EndTime'], 0, 5);
						$thisTimeDisplay = $thisStartTime."-".$thisEndTime;
						
						$thisAddBtnSpanID = 'AddTimeSlotBtnSpan_'.$thisTimeSlotID;
						$thisEditBtnSpanID = 'EditTimeSlotBtnSpan_'.$thisTimeSlotID;
						$thisDeleteBtnSpanID = 'DeleteTimeSlotBtnSpan_'.$thisTimeSlotID;
						
						$thisTimeSlotSessionType = $timeSlotArr[$i]['SessionType'];
						if ($thisTimeSlotSessionType == 'am') {
							$thisTimeSlotSessionTypeDisplay = $Lang['General']['AM'];
						}
						else {
							$thisTimeSlotSessionTypeDisplay = $Lang['General']['PM'];
						}
						
						$thisActionTag = '';
						$addTimeSlotIcon = '';
						$editTimeSlotIcon = '';
						$deleteTimeSlotIconDiv = '';
						if (!$isViewMode) {
							$thisActionTag = 'onmouseover="js_Show_Hide_Btn_Div(\''.$thisDeleteBtnSpanID.'\', 1); js_Show_Hide_Btn_Div(\''.$thisEditBtnSpanID.'\', 1); js_Show_Hide_Btn_Div(\''.$thisAddBtnSpanID.'\', 1);" ';
							$thisActionTag .= ' onmouseout="js_Show_Hide_Btn_Div(\''.$thisDeleteBtnSpanID.'\', 0); js_Show_Hide_Btn_Div(\''.$thisEditBtnSpanID.'\', 0); js_Show_Hide_Btn_Div(\''.$thisAddBtnSpanID.'\', 0);"';
												
							# Add TimeSlot Icon
							$addTimeSlotIcon = '<span id="'.$thisAddBtnSpanID.'" class="table_row_tool row_content_tool" style="float:right;display:none;">'."\n";
								$addTimeSlotIcon .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
											$Lang['SysMgr']['Timetable']['Add']['TimeSlot'], "js_Show_TimeSlot_Add_Edit_Layer('$TimetableID', '', $lastTimeSlotID, '$thisTimeSlotID'); return false;");
							$addTimeSlotIcon .= '</span>'."\n";
							
							# Edit TimeSlot Icon
							$editTimeSlotIcon = '<span id="'.$thisEditBtnSpanID.'" class="table_row_tool row_content_tool" style="float:right;display:none;">'."\n";
								$editTimeSlotIcon .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
											$Lang['SysMgr']['Timetable']['Edit']['TimeSlot'], "js_Show_TimeSlot_Add_Edit_Layer('$TimetableID', '$thisTimeSlotID', $lastTimeSlotID, $nextTimeSlotID); return false;");
							$editTimeSlotIcon .= '</span>'."\n";
							
							# Delete TimeSlot Icon Div
							$deleteTimeSlotIconDiv .= '<div style="clear:both">'."\n";
								$deleteTimeSlotIconDiv .= '<span id="'.$thisDeleteBtnSpanID.'" style="float:right;display:none;">'."\n";
									//$deleteTimeSlotIconDiv .= $libinterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Timetable']['Delete']['TimeSlot'], 
									//							"js_Delete_TimeSlot('$thisTimeSlotID'); return false;", "delete_dim");
									$deleteTimeSlotIconDiv .= $libinterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Timetable']['Delete']['TimeSlot'], 
																"js_Show_Delete_Option_Layer('TimeSlot', '$thisDeleteBtnSpanID'); return false;", "delete_dim");
								$deleteTimeSlotIconDiv .= '</span>'."\n";
							$deleteTimeSlotIconDiv .= '</div>'."\n";
						}
						$rowHeight = ($isViewMode)? '' : 'height:75px;';
						$table .= '<tr style="'.$rowHeight.'">'."\n";
							$table .= '<td '.$thisActionTag.' '.$smallFontStyle.' nowrap>'."\n";
								$table .= '<div>'."\n";
									# Time slot display
									$table .= '<span style="float:left">'.$thisTimeDisplay.'</span>'."\n";
									$table .= $addTimeSlotIcon;
								$table .= '</div>'."\n";
								$table .= '<div style="clear:both">'."\n";
									# Time slot name display
									$table .= '<span style="float:left">'.$thisTimeSlotName.'</span>'."\n";
									$table .= $editTimeSlotIcon;
								$table .= '</div>'."\n";
								$table .= '<div style="clear:both">'."\n";
									# Time slot name display
									$table .= '<span style="float:left">'.$thisTimeSlotSessionTypeDisplay.'</span>'."\n";
									$table .= '<span style="float:right">'.$deleteTimeSlotIconDiv.'</span>';
								$table .= '</div>'."\n";
							$table .= '</td>'."\n";
						
							# Loop Days
							for ($j=0; $j<$numOfCycleDays; $j++) {
								# Display Room Allocation Info
								$thisDay = $j + 1;
								
								$accDate = $libtimetable->Get_NextDate_byCycleDay($thisDay, $currDate);
								
								if ($Day != '' && $thisDay != $Day)
									continue;
								
								$thisRoomAllocationArr = $roomAllocationArr[$thisTimeSlotID][$thisDay];
								
								// set $thisRoomAllocationArr to an array if there is no room allocation record yet
								// so that system will not reload the room allocation info when generating the detailed table
								if ($thisRoomAllocationArr == '')
									$thisRoomAllocationArr = array();
								
								$thisAddBtnDivID = 'AddRoomAllocationBtnDiv_'.$thisTimeSlotID.'_'.$thisDay;
								$thisSeeAllSpanID = 'SeeAllSpan_'.$thisTimeSlotID.'_'.$thisDay;
								$thisDetailTableDivID = 'DetailTableDiv_'.$thisTimeSlotID.'_'.$thisDay;
								$thisAddBtnIconID = 'AddLessonIcon_'.$thisTimeSlotID.'_'.$thisDay;
								
								$thisActionTag = '';
								$thisAddBtnDiv = '';
								
									if (!$isViewMode)
										$thisActionTag = 'onmouseover="js_Show_Hide_Btn_Div(\''.$thisAddBtnDivID.'\', 1)" onmouseout="js_Show_Hide_Btn_Div(\''.$thisAddBtnDivID.'\', 0)"';
										//$thisActionTag = 'onclick="$(\'a#'.$thisAddBtnIconID.'\').click();" onmouseover="js_Show_Hide_Btn_Div(\''.$thisAddBtnDivID.'\', 1)" onmouseout="js_Show_Hide_Btn_Div(\''.$thisAddBtnDivID.'\', 0)"';
									else
										$thisActionTag = '';
									
									# Add Room Allocation Icon
									if (!$isViewMode) {
										$thisAddBtnDiv .= '<div id="'.$thisAddBtnDivID.'" style="display:none; float:right;">'."\n";
										
										
											//Get_Thickbox_Link($Height, $Width, $ExtraClass, $Title, $OnClick, $InlineID="FakeLayer", $Content="", $LinkID='')
											$thisAddBtnDiv .= '<span align="right" class="table_row_tool row_content_tool" style="float:left">';
											$thisAddBtnDiv .= $libinterface->Get_Thickbox_Link(	$this->thickBoxHeight, 
																								$this->thickBoxWidth, 
																								"add_dim", 
																								$Lang['SysMgr']['Timetable']['Add']['RoomAllocation'], 
																								"js_Show_Add_Edit_Room_Allocation_Layer('$thisTimeSlotID', '$thisDay', '');",
																								'FakeLayer',
																								'',
																								$thisAddBtnIconID
																								)."\n";
											$thisAddBtnDiv .= $libinterface->GET_LNK_DELETE("javascript:void(0);", $Lang['SysMgr']['Timetable']['Delete']['RoomAllocationOfThisTimeSlotAndDay'], $ParOnClick="js_Delete_Room_Allocation('', '$thisTimeSlotID', '$thisDay');return false;", $ParClass="", $WithSpan=0);
											$thisAddBtnDiv .= '</span>';
										
										$thisAddBtnDiv .= '</div>'."\n";
										$thisAddBtnDiv .= '<br style="clear:both" />'."\n";
									}

								$table .= '<td '.$thisActionTag.'>'."\n";
									$table .= $thisAddBtnDiv."\n";
									
									$table .= '<div id="'.$thisDetailTableDivID.'">'. "\n";
										$TimeSlotDetail = $this->Get_Room_Allocation_Detail_Table($isViewMode, $thisTimeSlotID, $thisDay, $thisRoomAllocationArr, 
																							$SubjectGroupInfoArr, $LocationInfoArr,
																							$SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter, $isSmallFont);
										if ($plugin['SLRS']) {
											$allowSLRSInViewMode = true;
										} else {
											$allowSLRSInViewMode = false;
										}
										// if ($isViewMode && !empty($targetUserID) && $allowSLRSInViewMode) {
										if ($isViewMode && $allowSLRSInViewMode) {
											$hasSLRS = false;
											include("slrs/slrsConfig.inc.php");
											include_once(dirname(dirname(__FILE__)) . "/lang/slrs_lang." . Get_Lang_Selection("b5", "en") . ".php");
											include_once("slrs/libslrs.php");
											include_once("slrs/libslrs_ui.php");
											$libslrs = new libslrs();
											$libslrs_ui = new libslrs_ui();
											$SLRSTable = "";
											/* for Debug * /
											$table .= '<br>Get SLRS Info<br>';
											$table .= "Target User ID: " . $targetUserID . "<br>";
											$table .= 'TimeSlotID: '. $thisTimeSlotID . "<br>";
											$table .= 'Cycle Day: ' . $thisDay . '<br>';
											$table .= 'SubjectGroup: ' . implode(", ", $SubjectGroupID_DisplayFilterArr) . '<br>';
											// $table .= 'UserTable: ' . $slrsConfig["INTRANET_USER"]. '<br>';
											$table .= $accDate . " " . "<br><br>\n";
											/* */
											
											$SLRS_ExchangeArrangeInfo = $libslrs->getExchangeInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr, "A");
											if (count($SLRS_ExchangeArrangeInfo) > 0) {
												$hasSLRS = true;
												$SLRSTable .= "";
											}
											/****************************************************************/
											$SLRS_ExchangeArrangeInfo = $libslrs->getExchangeInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr, "B");
											$SLRS_ArrangeInfo = $libslrs->getArrangementInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr);
											if (count($SLRS_ExchangeArrangeInfo) > 0) {
												if (count($SLRS_ArrangeInfo) > 0)
												{
													$SLRSTable .= "<div style='color:#707070'>" . $libslrs_ui->getSLRSExchangeInfoForTimeTable($SLRS_ExchangeArrangeInfo, $SubjectGroupInfoArr, $LocationInfoArr, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter, $ignoreColor = true) . "</div>";
												}
												else
												{
													$SLRSTable .= $libslrs_ui->getSLRSExchangeInfoForTimeTable($SLRS_ExchangeArrangeInfo, $SubjectGroupInfoArr, $LocationInfoArr, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter);
												}
												$hasSLRS = true;
											} else {
												if (count($SLRS_ArrangeInfo) > 0) {
													$hasSLRS = true;
													if ($TimeSlotDetail == "&nbsp;") {
														$SLRSTable .= $libslrs_ui->getSLRSInfoForTimeTable($SLRS_ArrangeInfo, $SubjectGroupInfoArr, $LocationInfoArr, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter);
													}
												}
											}
												
											/****************************************************************/
											
											if ($TimeSlotDetail != "&nbsp;") {
												$SLRS_ArrangeInfo = $libslrs->getArrangementInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr);
												if (count($SLRS_ArrangeInfo) > 0) {
													$SLRSTable .= $libslrs_ui->getSLRSInfoForTimeTable($SLRS_ArrangeInfo, $SubjectGroupInfoArr, $LocationInfoArr, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter);
													$hasSLRS = true;
												} else {
												    if (!empty($accDate))
												    {
												        $SLRS_CancelLessonInfo = $libslrs->getCancelLessonInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr);
												        if (count($SLRS_CancelLessonInfo) > 0)
												        {
												            $hasSLRS = true;
												            $SLRSTable .= $libslrs_ui->getSLRSCancelInfoForTimeTable($SLRS_CancelLessonInfo);
												        }
												    }
												}
											} else {
												$SLRS_ArrangeInfo = $libslrs->getArrangementInfoForTimetable($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr, true);
												if (count($SLRS_ArrangeInfo) > 0) {
													$SLRSTable .= $libslrs_ui->getSLRSInfoForTimeTable($SLRS_ArrangeInfo, $SubjectGroupInfoArr, $LocationInfoArr, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter);
													$hasSLRS = true;
												}
											}
											
											if ($TimeSlotDetail != "&nbsp;") {
												$removeDetail = false;
												if ($libslrs->isLeaveTimeSlot($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr)) {
													$removeDetail = true;
												}
												if ($libslrs->isExchangeTimeSlot($slrsConfig, $targetUserID, $thisTimeSlotID, $accDate, $SubjectGroupID_DisplayFilterArr)) {
													$removeDetail = true;
												}
												if ($hasSLRS) {
													if ($removeDetail) {
														$TimeSlotDetail = "&nbsp;";
													} else {
														if ($libslrs->isEJ())
														{
															$TimeSlotDetail = "<div style='padding:2px; background-color:#efefef; border:1px solid #CCC; color:#707070;'>*** " . convert2Big5($Lang['SLRS']['SubstitutionArrangementDes']['InformationUpdated']) . " ***<br>". $TimeSlotDetail . "</div>";
														}
														else
														{
															$TimeSlotDetail = "<div style='padding:2px; background-color:#efefef; border:1px solid #CCC; color:#707070;'>*** " . $Lang['SLRS']['SubstitutionArrangementDes']['InformationUpdated']. " ***<br>". $TimeSlotDetail . "</div>";
														}
													}
												}
											}
											
											if ($hasSLRS) {
												if ($TimeSlotDetail != "&nbsp;") {
													$table .= $TimeSlotDetail;
												}
												$table .= $SLRSTable;
												$TimeSlotDetail = "";
												$SLRSTable = "";
											} else {
												$table .= $TimeSlotDetail;
												$TimeSlotDetail = "";
												$SLRSTable = "";
											}
										} else {
											$table .= $TimeSlotDetail;
											$SLRSTable = "";
											$TimeSlotDetail = "";
										}
									$table .= '</div>'."\n";
								$table .= '</td>'."\n";
							}
							
							if (!$isViewMode) {
								# Empty cell for add cycle day button
								$table .= '<td>&nbsp;</td>'."\n";
							}
						
						$table .= '</tr>'."\n";
						
						$lastTimeSlotID = "'$thisTimeSlotID'";
					}
					if (!$isViewMode) {
						# Add Timeslot
						$table .= '<tr>'."\n";
							$table .= '<td>'."\n";
								$table .= '<div class="table_row_tool row_content_tool">'."\n";
									$table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
												$Lang['SysMgr']['Timetable']['Add']['TimeSlot'], "js_Show_TimeSlot_Add_Edit_Layer('$TimetableID', '', $lastTimeSlotID, ''); return false;");
								$table .= '</div>'."\n";
							$table .= '</td>'."\n";
							
							$table .= '<td colspan="'.($numOfCycleDays + 1).'">&nbsp;</td>'."\n";
						$table .= '</tr>'."\n";
					}
				
				$table .= '</tbody>'."\n";
				
			$table .= '</table>'."\n";
			
			# For thickbox use
			$table .= '<div class="FakeLayer"></div>'."\n";
			
			# For delete option layer use
			// Delete Cycle Day
			$table .= '<div id="DeleteCycleDayLayerDiv" class="Conntent_tool" style="position:absolute; width:200px;">'."\n";
				$table .= '<div class="btn_option">'."\n";
					$table .= '<div id="DeleteCycleDayButtonDiv" class="btn_option_layer">'."\n";
						$table .= '<a href="javascript:void(0);" onclick="js_Delete_CycleDays();" class="sub_btn"> '.$Lang['SysMgr']['Timetable']['Delete']['CycleDay'].'</a>'."\n";
						$table .= '<a href="javascript:void(0);" onclick="js_Delete_Room_Allocation(\'\', \'\', \'\', \'\', 1);" class="sub_btn"> '.$Lang['SysMgr']['Timetable']['Delete']['RoomAllocation'].'</a>'."\n";
					$table .= '</div>'."\n";
				$table .= '</div>'."\n";
			$table .= '</div>'."\n";
			
			// Delete TimeSlot
			$table .= '<div id="DeleteTimeSlotLayerDiv" class="Conntent_tool" style="position:absolute; width:200px;">'."\n";
				$table .= '<div class="btn_option">'."\n";
					$table .= '<div id="DeleteTimeSlotButtonDiv" class="btn_option_layer">'."\n";
						$table .= '<a href="javascript:void(0);" onclick="js_Delete_TimeSlot();" class="sub_btn"> '.$Lang['SysMgr']['Timetable']['Delete']['TimeSlot'].'</a>'."\n";
						$table .= '<a href="javascript:void(0);" onclick="js_Delete_Room_Allocation(\'\', \'\', \'\', 1, \'\');" class="sub_btn"> '.$Lang['SysMgr']['Timetable']['Delete']['RoomAllocation'].'</a>'."\n";
					$table .= '</div>'."\n";
				$table .= '</div>'."\n";
			$table .= '</div>'."\n";
			
			# hidden fields
			$table .= '<input type="hidden" id="NumOfCycleDays" name="NumOfCycleDays" value="'.$numOfCycleDays.'" />'."\n";
			$table .= '<input type="hidden" id="TimeSlotIDList" name="TimeSlotIDList" value="'.implode(',', $TimeSlotIDArr).'" />'."\n";
			
			if ($IndicateCurDay == 1)
			{
				# Show Remarks for Current Day
				$table .= '<p class=\"spacer\"></p>'."\n";
				$table .= '<span class="tabletextremark\">'.$Lang['iAccount']['Timetable']['TodayDayRemarks'].'</span>'."\n";
				$table .= '<p class=\"spacer\"></p>'."\n";
			}
			
			# Show Remarks for Current Day
			$table .= '<p class=\"spacer\"></p>'."\n";
			$table .= '<span class="tabletextremark\">'.$Lang['iAccount']['Timetable']['TimeZoomRemarks'].'</span>'."\n";
			$table .= '<p class=\"spacer\"></p>'."\n";

			return $table;
		}
		
		function Get_Room_Allocation_Detail_Table($isViewMode, $TimeSlotID, $Day, 
					$RoomAllocationArr='', $SubjectGroupInfoArr='', $LocationInfoArr='',
					$SubjectGroupID_DisplayFilterArr=array(), $LocationID_DisplayFilterArr=array(), $OthersLocationFilter='', $isSmallFont=0)
		{
			global $Lang, $PATH_WRT_ROOT, $plugin;
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$libinterface = new interface_html();
			
			## If reload Table partially, load Room Allocation, Location, Subject, Subject Group info here
			//if (count($RoomAllocationArr) == 0)
			
			if ($RoomAllocationArr == '')
			{
				# Get TimetableID from the TimeSlot info
				$objTimeSlot = new TimeSlot($TimeSlotID);
				$TimetableID = $objTimeSlot->TimetableID;
				
				$objTimetable = new Timetable($TimetableID);
				$AllRoomAllocationArr = $objTimetable->Get_Room_Allocation_List($TimeSlotID, $Day, $SubjectGroupIDArr=array(), $LocationID='');
				$RoomAllocationArr = $AllRoomAllocationArr[$TimeSlotID][$Day];
			}
			
			$numOfRoomAllocation = count($RoomAllocationArr);
			if ($numOfRoomAllocation == 0)
				return "&nbsp;";
			
			
			//if (count($SubjectGroupArr) == 0)
			if ($SubjectGroupInfoArr == '')
			{
				# Get Timetable Info
				$thisAcademicYearID = $objTimetable->AcademicYearID;
				$thisYearTermID = $objTimetable->YearTermID;
				
				include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
				$lib_form_class_manage = new form_class_manage();
				if ($lib_form_class_manage->Has_Term_Past($thisYearTermID))
					$isViewMode = 1;
			
				# Subject Group Info
				include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
				$libSCM = new subject_class_mapping();
				$SubjectGroupInfoArr = $libSCM->Get_Subject_Group_Info($thisAcademicYearID, $thisYearTermID, $TimetableID);
			}
			
			//if (count($LocationInfoArr) == 0)
			if ($LocationInfoArr == '')
			{
				# Location Info
				include_once("liblocation.php");
				$liblocation = new liblocation();
				# $LocationInfoArr[$LocationID] = LocationInfoArr
				$LocationInfoArr = $liblocation->Get_Building_Floor_Room_Name_Arr($TimetableID);
			}
			
			# Lesson padding style
			$lessonPadding = ($isViewMode)? 'padding: 3px;' : '';
			
			
			# Filtering Checking
			$numSubjectGroupIDFilter = count($SubjectGroupID_DisplayFilterArr);
			$numLocationIDFilter = count($LocationID_DisplayFilterArr);
			$ContentTableID = 'ContentTable_'.$TimeSlotID.'_'.$Day;
			
			if ($isSmallFont)
				$smallFontStyle = "style='font-size:9px'";
			
			$numOfLessonPerColumn = ($isViewMode)? 2 : 1;
			$deleteIconWidth = ($isViewMode)? 0 : 15;
			$lessonTdWidth = floor((100-$deleteIconWidth)/$numOfLessonPerColumn);
			$roomAllocationCounter = 0;
			$table = '';
			$table .= '<table id="'.$ContentTableID.'" class="common_table_list">'."\n";
				# Loop each Room Allocation
				for ($i=0; $i<$numOfRoomAllocation; $i++)
				{
					$thisRoomAllocationID = $RoomAllocationArr[$i]['RoomAllocationID'];
					$thisSubjectGroupID = $RoomAllocationArr[$i]['SubjectGroupID'];
					$thisLocationID = $RoomAllocationArr[$i]['LocationID'];
					$thisOthersLocation = $RoomAllocationArr[$i]['OthersLocation'];
					$thisTimeSlotID = $RoomAllocationArr[$i]['TimeSlotID'];
					$thisDay = $RoomAllocationArr[$i]['Day'];
					$thisRelatedRoomAllocationID = $RoomAllocationArr[$i]['RelatedRoomAllocationID'];
					$isContinuousLesson = ($thisRelatedRoomAllocationID=='')? 0 : 1;
					
					# Subject Group filtering
					if (($numSubjectGroupIDFilter > 0) && (!in_array($thisSubjectGroupID, $SubjectGroupID_DisplayFilterArr)))
						continue;
						
					# Location filtering
					if (($numLocationIDFilter > 0) && (!in_array($thisLocationID, $LocationID_DisplayFilterArr)))
						continue;
						
					# Others Location filtering
					if ($OthersLocationFilter!='' && $thisOthersLocation!=$OthersLocationFilter)
						continue;
					
					# Subject Info
					$thisSubjectNameEN = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectDescEN'];
					$thisSubjectNameB5 = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectDescB5'];
					$thisSubjectName = Get_Lang_Selection($thisSubjectNameB5, $thisSubjectNameEN);
					
					# Subject Group Info
					$thisSubjectGroupNameEN = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleEN'];
					$thisSubjectGroupNameB5 = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleB5'];
					$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameB5, $thisSubjectGroupNameEN);
					$thisNumOfStudents = ($SubjectGroupInfoArr[$thisSubjectGroupID]['Total']=='')? 0 : $SubjectGroupInfoArr[$thisSubjectGroupID]['Total'];
					
					//$thisSubjectGroupDisplay = $thisSubjectName." - ".$thisSubjectGroupName;
					$thisSubjectGroupDisplay = '';
					$thisSubjectGroupDisplay .= '<span class="SubjectNameDisplaySpan">';
						$thisSubjectGroupDisplay .= $thisSubjectName." - ";
					$thisSubjectGroupDisplay .= '</span>';
					$thisSubjectGroupDisplay .= '<span class="SubjectGroupNameDisplaySpan">';
						$thisSubjectGroupDisplay .= $thisSubjectGroupName;
					$thisSubjectGroupDisplay .= '</span>';
					$thisSubjectGroupDisplay .= '<span class="SubjectGroupNumOfStudentDisplaySpan">';
						$thisSubjectGroupDisplay .= " ($thisNumOfStudents)";
					$thisSubjectGroupDisplay .= '</span>';
					
					# Teachers Info
					$thisStaffNameArr = $SubjectGroupInfoArr[$thisSubjectGroupID]['StaffName'];
					
					if (is_array($thisStaffNameArr))
						$thisStaffNameDisplay = implode("<br />", $thisStaffNameArr);
					else
						$thisStaffNameDisplay = '';
					
					$thisStaffUserLoginArr = $SubjectGroupInfoArr[$thisSubjectGroupID]['StaffUserLogin'];
					if (is_array($thisStaffUserLoginArr))
						$thisStaffUserLoginDisplay = implode("<br />", $thisStaffUserLoginArr);
					else
						$thisStaffUserLoginDisplay = '';
						
					# Location Info
					
					$thisLocationDisplay = '';
					if ($thisLocationID != 0)
					{
						$thisLocationInfoArr = $LocationInfoArr[$thisLocationID];
						$thisBuildingName = Get_Lang_Selection($thisLocationInfoArr['BuildingNameChi'], $thisLocationInfoArr['BuildingNameEng']);
						$thisFloorName = Get_Lang_Selection($thisLocationInfoArr['FloorNameChi'], $thisLocationInfoArr['FloorNameEng']);
						$thisRoomName = Get_Lang_Selection($thisLocationInfoArr['RoomNameChi'], $thisLocationInfoArr['RoomNameEng']);
						//$thisLocationDisplay = $thisBuildingName." > ".$thisFloorName." > ".$thisRoomName;
						$thisLocationDisplay .= '<span class="BuildingNameDisplaySpan">';
							$thisLocationDisplay .= $thisBuildingName." > ";
						$thisLocationDisplay .= '</span>';
						$thisLocationDisplay .= '<span class="FloorNameDisplaySpan">';
							$thisLocationDisplay .= $thisFloorName." > ";
						$thisLocationDisplay .= '</span>';
						$thisLocationDisplay .= '<span class="RoomNameDisplaySpan">';
							$thisLocationDisplay .= $thisRoomName;
						$thisLocationDisplay .= '</span>';
					}
					else
					{
						$thisLocationDisplay .= '<span class="RoomNameDisplaySpan">';
							$thisLocationDisplay .= $thisOthersLocation;
						$thisLocationDisplay .= '</span>';
					}
					
					if ($isViewMode)
					{
						# no delete Icon
						$thisDeleteBtnDivID = '';
						
						# no edit link
						$thisRoomAllocationDisplay = $thisSubjectGroupDisplay;
						
						# no mouseover event
						$thisActionTag = '';
					}
					else
					{
						# Delete Icon
						$thisDeleteBtnDivID = 'DeleteBtnDiv_'.$thisRoomAllocationID;
					
						# edit room allocation link
						$thisRoomAllocationDisplay = $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "", $Lang['SysMgr']['Timetable']['Edit']['RoomAllocation'], 
														"js_Show_Add_Edit_Room_Allocation_Layer('$thisTimeSlotID', '$thisDay', '$thisRoomAllocationID'); return false;", 
														"FakeLayer", "$thisSubjectGroupDisplay");
														
						# mouseover event (show hide delete icon)
						$thisActionTag = "onmouseover=\"js_Show_Hide_Btn_Div('$thisDeleteBtnDivID', 1)\" onmouseout=\"js_Show_Hide_Btn_Div('$thisDeleteBtnDivID', 0)\"";
					}
					
//					$thisStartNewRow = false;
//					if (!$isViewMode) {
//						$thisStartNewRow = true;
//					}
//					else if ($isViewMode && $roomAllocationCounter % 2 == 0) {
//						$thisStartNewRow = true;
//					}
//					if ($thisStartNewRow) {
//						$table .= '<tr '.$thisActionTag.' '.$smallFontStyle.'>'."\n";
//					}

					if ($roomAllocationCounter % $numOfLessonPerColumn == 0) {
						$table .= '<tr '.$thisActionTag.' '.$smallFontStyle.'>'."\n";
					}
					
						$table .= '<td style="width:'.$lessonTdWidth.'%; '.$lessonPadding.'">'."\n";
							# Room Allocation Content
							$table .= '<div>'."\n";
								$table .= '<div class="SubjectGroupNameDisplayDiv">'.$thisRoomAllocationDisplay.'</div>'."\n";
								$table .= '<div class="LocationDisplayDiv">'.$thisLocationDisplay.'</div>'."\n";
								$table .= '<div class="StaffNameDisplayDiv">'.$thisStaffNameDisplay.'</div>'."\n";
								$table .= '<div class="StaffUserLoginDisplayDiv" style="display:none;">'.$thisStaffUserLoginDisplay.'</div>'."\n";
							$table .= '</div>'."\n";
						$table .= '</td>'."\n";
						
						# Show delete room allocation icon if not in view mode
						if (!$isViewMode) {
							$table .= '<td width="20px">'."\n";
								$table .= '<div id="'.$thisDeleteBtnDivID.'" class="table_row_tool row_content_tool" style="display:none">'."\n";
									$table .= '<a href="#" class="delete_dim" onclick="js_Delete_Room_Allocation(\''.$thisRoomAllocationID.'\', \''.$TimeSlotID.'\', \''.$Day.'\', 0, 0, '.$isContinuousLesson.'); return false;" ';
									$table .= ' title="'.$Lang['SysMgr']['Timetable']['Delete']['RoomAllocation'].'"></a>'."\n";
								$table .= '</div>'."\n";
								$table .= '<br style="clear:both" />'."\n";
							$table .= '</td>'."\n";
						}
						
						// for the last lesson, and if more than 2 row, and if the row is not fully filled => fill in the empty td with &nbsp;
						if (($i==$numOfRoomAllocation-1) && ($numOfRoomAllocation > $numOfLessonPerColumn) && ($roomAllocationCounter%$numOfLessonPerColumn != $numOfLessonPerColumn-1)) {
							$thisNumOfEmptyTd = $numOfLessonPerColumn - $roomAllocationCounter%$numOfLessonPerColumn - 1;
							for ($j=0; $j<$thisNumOfEmptyTd; $j++) {
								$table .= '<td>&nbsp;</td>'."\n";
							}
						}
						
//						if ($isViewMode && $i == $numOfRoomAllocation-1 && $roomAllocationCounter % 2 != 1) {
//							$table .= '<td>&nbsp;</td>'."\n";
//						}
//						
//					$thisEndRow = false;
//					if (!$isViewMode) {
//						$thisEndRow = true;
//					}
//					else if ($isViewMode && $roomAllocationCounter % 2 == 1) {
//						$thisEndRow = true;
//					}
//					if ($thisEndRow) {
//						$table .= '</tr>'."\n";
//					}

					if (($roomAllocationCounter % $numOfLessonPerColumn) == ($numOfLessonPerColumn - 1)) {
						$table .= '</tr>'."\n";
					}
					
					$roomAllocationCounter++;
					
				}	// End Loop Room Allocation
			$table .= '</table>'."\n";
			
			if ($numOfRoomAllocation == 0 || $roomAllocationCounter == 0) {
				return "&nbsp;";
			}
			else {
				return $table;
			}
		}
		
		function Get_Personal_Timetable($TargetUserID, $TimetableID='', $YearTermID='', $IsViewMode=0, $Day='', $isSmallFont=0, $FilterSubjectGroupIDArr='')
		{
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			
			if ($TimetableID == '')
			{
				$TimetableID = $this->Get_Current_Timetable();
				
				if ($TimetableID == '')
					return '';
			}
				
			if ($YearTermID == '')
				$YearTermID = getCurrentSemesterID();
			
			$ViewMode = "Personal";
			
			$libuser = new libuser($TargetUserID);
			if ($libuser->RecordType == 2)
				$Identity = "Student";
			else if ($libuser->RecordType == 1)
				$Identity = "Teacher";
			
			$scm = new subject_class_mapping();
			$this_SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID, $ViewMode, '', $TargetUserID, '', $Identity, '', $FilterSubjectGroupIDArr);
			if (count($this_SubjectGroupID_DisplayFilterArr) <= 0)
				$this_SubjectGroupID_DisplayFilterArr = array(0);
			
			return $this->Get_Timetable_Table($TimetableID, $IsViewMode, $this_SubjectGroupID_DisplayFilterArr, array(), '', $Day, '', $isSmallFont, $MapTimeZoneDay=1, $IndicateCurDay=1, $TargetUserID);
		}
		
		function Get_Class_Timetable($YearClassID, $TimetableID='', $YearTermID='', $IsViewMode=0, $Day='', $isSmallFont=0)
		{
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");			
			$scm = new subject_class_mapping();
			
			if ($TimetableID == '')
			{
				$TimetableID = $this->Get_Current_Timetable();
				
				if ($TimetableID == '')
					return '';
			}
				
			if ($YearTermID == '')
				$YearTermID = getCurrentSemesterID();
			
			$ViewMode = "Class";
			$this_SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID, $ViewMode, $YearClassID);
			if(sizeof($this_SubjectGroupID_DisplayFilterArr) <= 0)
				$this_SubjectGroupID_DisplayFilterArr = array(0);
				
			return $this->Get_Timetable_Table($TimetableID, $IsViewMode, $this_SubjectGroupID_DisplayFilterArr, array(), '', $Day, '', $isSmallFont, $MapTimeZoneDay=1, $IndicateCurDay=1);
		}
		
		function Get_TimeSlot_Add_Edit_Layer($TimetableID, $TimeSlotID='', $LastTimeSlotID='', $NextTimeSlotID='')
		{
			global $Lang, $PATH_WRT_ROOT, $intranet_session_language;
			
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			# Add / Edit
			$isEdit = ($TimeSlotID=='')? false : true;
			
			# Title
			$thisTitle = ($isEdit)? $Lang['SysMgr']['Timetable']['Edit']['TimeSlot'] : $Lang['SysMgr']['Timetable']['Add']['TimeSlot'];
			
			## Buttons
			if ($isEdit)
			{
				# Button Submit
				$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
									$onclick="js_Insert_Edit_TimeSlot('$TimeSlotID', '', '')", $id="Btn_Edit");
			}
			else
			{
				# Button Sumbit
				$btn_Submit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
									$onclick="js_Insert_Edit_TimeSlot('', '$LastTimeSlotID', '$NextTimeSlotID')", $id="Btn_Add");
			}
			
			# Reset
			//$btn_Reset = $libinterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "button", 
			//					$onclick="js_Show_TimeSlot_Add_Edit_Layer('$TimetableID', '$TimeSlotID', '$LastTimeSlotID', '$NextTimeSlotID')", $id="Btn_Reset");
			
			# Cancel
			$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
								$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
			
			## Data Display
			# Get Timetable info
			$timetableObj = new Timetable($TimetableID);
			$timetableName = intranet_htmlspecialchars($timetableObj->TimetableName);
			
			# Get Academic Year info
			include_once("form_class_manage.php");
			$AcademicYearID = $timetableObj->AcademicYearID;
			$academicYearObj = new academic_year($AcademicYearID);
			$academicYearName = $academicYearObj->Get_Academic_Year_Name();
			
			# Get Term info
			$YearTermID = $timetableObj->YearTermID;
			$yearTermObj = new academic_year_term($YearTermID);
			$yearTermName = $yearTermObj->Get_Year_Term_Name();
			
			# Show TimeSlot info if it is edit mode
			if ($isEdit)
			{
				$thisTimeSlotObj = new TimeSlot($TimeSlotID);
				$thisTimeSlotName = intranet_htmlspecialchars($thisTimeSlotObj->TimeSlotName);
				$thisStartTime = $thisTimeSlotObj->StartTime;
				$thisEndTime = $thisTimeSlotObj->EndTime;
				$thisFirstPmSession = $thisTimeSlotObj->Is_First_PM_Session();
				
				$thisStartTimeHour = substr($thisStartTime, 0, 2);
				$thisStartTimeMin = substr($thisStartTime, 3, 2);
				$thisEndTimeHour = substr($thisEndTime, 0, 2);
				$thisEndTimeMin = substr($thisEndTime, 3, 2);
			}
			
			# Get Last TimeSlotID for TimeSlot checking
			if ($LastTimeSlotID != '')
			{
				$lastTimeSlotObj = new TimeSlot($LastTimeSlotID);
				$lastTimeSlotName = intranet_htmlspecialchars($lastTimeSlotObj->TimeSlotName);
				$lastStartTime = $lastTimeSlotObj->StartTime;
				$lastEndTime = $lastTimeSlotObj->EndTime;
				
				$lastStartTimeHour = substr($lastStartTime, 0, 2);
				$lastStartTimeMin = substr($lastStartTime, 3, 2);
				$lastEndTimeHour = substr($lastEndTime, 0, 2);
				$lastEndTimeMin = substr($lastEndTime, 3, 2);
			}
			
			# Get Next TimeSlotID for TimeSlot checking
			if ($NextTimeSlotID != '')
			{
				$nextTimeSlotObj = new TimeSlot($NextTimeSlotID);
				$nextTimeSlotName = intranet_htmlspecialchars($nextTimeSlotObj->TimeSlotName);
				$nextStartTime = $nextTimeSlotObj->StartTime;
				$nextEndTime = $nextTimeSlotObj->EndTime;
				
				$nextStartTimeHour = substr($nextStartTime, 0, 2);
				$nextStartTimeMin = substr($nextStartTime, 3, 2);
				$nextEndTimeHour = substr($nextEndTime, 0, 2);
				$nextEndTimeMin = substr($nextEndTime, 3, 2);
			}
			
			## Time Checking Limit (the time slot should be after the end time of last time slot and before the start of the next time slot)
			# case 1: Add timeslot at the top 
			#			- Use the start time of the 1st time slot for both the start time and the end time
			# case 2: Add timeslot in between two time slots 
			#			- Use the end time of the last time slot as the start time
			#			- Use the start time of the next time slot as the end time
			# case 3: Add timeslot at the bottom
			#			- Use the end time of the last time slot for both the start time and the end time
			$startTimeLimit = ($LastTimeSlotID != '')? $lastEndTime : $nextStartTime;
			$startTimeHourLimit = ($LastTimeSlotID != '')? $lastEndTimeHour : $nextStartTimeHour;
			$startTimeMinLimit = ($LastTimeSlotID != '')? $lastEndTimeMin : $nextStartTimeMin;
			$endStartTime = ($NextTimeSlotID != '')? $nextStartTime : $lastEndTime;
			$endTimeHourLimit = ($NextTimeSlotID != '')? $nextStartTimeHour : $lastEndTimeHour;
			$endTimeMinLimit = ($NextTimeSlotID != '')? $nextStartTimeMin : $lastEndTimeMin;
			
			## Data Input
			# Title Input box
			$titleInput = '<input id="TimeSlotTitle" name="TimeSlotTitle" type="text" class="textbox" maxlength="255" value="'.$thisTimeSlotName.'" />'."\n";
			
			# Time Slot Selection
			$startOnchange = "onchange=\"js_Validate_TimeSlot('$TimeSlotID')\"";
			$presetStartHour = ($isEdit)? $thisStartTimeHour : $startTimeHourLimit;
			$startTimeHourSel = $libinterface->Get_Time_Selection_Box('startTimeHour', 'hour', $presetStartHour, $startOnchange);
			$presetStartMin = ($isEdit)? $thisStartTimeMin : $startTimeMinLimit;
			$startTimeMinSel = $libinterface->Get_Time_Selection_Box('startTimeMin', 'min', $presetStartMin, $startOnchange);
			$startTimeSelDisplay = $startTimeHourSel." : ".$startTimeMinSel;
			
			$endOnchange = "onchange=\"js_Validate_TimeSlot('$TimeSlotID')\"";
			$presetEndHour = ($isEdit)? $thisEndTimeHour : $endTimeHourLimit;
			$endTimeHourSel = $libinterface->Get_Time_Selection_Box('endTimeHour', 'hour', $presetEndHour, $endOnchange);
			$presetEndMin = ($isEdit)? $thisEndTimeMin : $endTimeMinLimit;
			$endTimeMinSel = $libinterface->Get_Time_Selection_Box('endTimeMin', 'min', $presetEndMin, $endOnchange);
			$endTimeSelDisplay = $endTimeHourSel." : ".$endTimeMinSel;
			
			$timeSlotSelection = $startTimeSelDisplay." ".$Lang['General']['To']." ".$endTimeSelDisplay;
			
			$x = '';
			$x .= '<div id="debugArea"></div>'."\n";
			$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
				$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
				//$x .= '<h1> '.$Lang['SysMgr']['Timetable']['ModuleTitle'].' &gt; <span>'.$thisTitle.'</span></h1>'."\n";
				$x .= '<div class="edit_pop_board_write">'."\n";
					$x .= '<form id="LocationForm" name="LocationForm">'."\n";
						$x .= '<table class="form_table">'."\n";
							$x .= '<col class="field_title" />'."\n";
							$x .= '<col class="field_c" />'."\n";
							
							# Academic Year Display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['AcademicYear'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'.$academicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Year Term Display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $yearTermName;
								$x .= '</td>'."\n"; # drop down list
							$x .= '</tr>'."\n";
							
							# Timetable Name Display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Timetable'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $timetableName;
								$x .= '</td>'."\n"; # drop down list
							$x .= '</tr>'."\n";
							
							# Title input
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Name'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $titleInput;
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Start Time Selections
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Time'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $timeSlotSelection;
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TimeWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# First PM lesson
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['FirstPmLesson'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $libinterface->Get_Radio_Button('firstPmLessonRadio_yes', 'firstPmLesson', $Value=1, $thisFirstPmSession, $Class="", $Lang['General']['Yes']);
									$x .= '&nbsp;';
									$x .= $libinterface->Get_Radio_Button('firstPmLessonRadio_no', 'firstPmLesson', $Value=0, !$thisFirstPmSession, $Class="", $Lang['General']['No']);
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</form>'."\n";
				$x .= '</div>'."\n";
				
				$x .= '<div class="edit_bottom">'."\n";
					$x .= '<span></span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					if ($isEdit)
					{
						$x .= $btn_Edit."\n";
					}
					else
					{
						$x .= $btn_Submit."\n";
					}
					$x .= $btn_Reset."\n";
					$x .= $btn_Cancel."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		function Get_Room_Allocation_Add_Edit_Layer($TimeSlotID, $Day, $RoomAllocationID='')
		{
			global $Lang, $PATH_WRT_ROOT, $intranet_session_language;
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$libinterface = new interface_html();
			
			# Add / Edit
			$isEdit = ($RoomAllocationID=='')? false : true;
			
			# Title
			$thisTitle = ($isEdit)? $Lang['SysMgr']['Timetable']['Edit']['RoomAllocation'] : $Lang['SysMgr']['Timetable']['Add']['RoomAllocation'];
			
			# Reset
			//$btn_Reset = $libinterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "button", 
			//					$onclick="js_Show_Add_Edit_Room_Allocation_Layer('$TimeSlotID', '$Day', '$RoomAllocationID')", $id="Btn_Reset");
			
			# Cancel
			$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
								$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
			
			if ($isEdit)
			{
				# Button Submit
				$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
									$onclick="js_Insert_Edit_Room_Allocation('$TimeSlotID', '$Day', '$RoomAllocationID', 0)", $id="Btn_Edit");
			}
			else
			{
				# Button Add & Finish
				$btn_Add_Finish = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['Location']['Button']['Add&Finish'], "button", 
									$onclick="js_Insert_Edit_Room_Allocation('$TimeSlotID', '$Day', '', 0)", $id="Btn_AddFinish");
				# Button Add & Add More Location
				$btn_Add_AddMore = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['Timetable']['Button']['Add&AddMoreRoomAllocation'], "button", 
									$onclick="js_Insert_Edit_Room_Allocation('$TimeSlotID', '$Day', '', 1)", $id="Btn_AddFinish");
			}

			# Preset Filters for edit mode
			if ($isEdit)
			{
				$objRoomAllocation = new Room_Allocation($RoomAllocationID);
				$thisSubjectGroupID = $objRoomAllocation->SubjectGroupID;
				
				include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
				$objSubjectGroup = new subject_term_class($thisSubjectGroupID);
				$thisSubjectID = $objSubjectGroup->SubjectID;
				
				$thisLocationID = $objRoomAllocation->LocationID;
				
				if ($thisLocationID == 0)
				{
					# Others Location
					$thisOthersLocation = intranet_htmlspecialchars($objRoomAllocation->OthersLocation);
					
					$thisBuildingID = "-1";
				}
				else
				{
					include_once($PATH_WRT_ROOT."includes/liblocation.php");
					$objRoom = new Room($thisLocationID);
					$thisLocationLevelID = $objRoom->LocationLevelID;
					
					$objFloor = new Floor($thisLocationLevelID);
					$thisBuildingID = $objFloor->BuildingID;
				}
				
				$js_RoomAllocationID = $RoomAllocationID;
				
				$relatedRoomAllocationInfoAry = $objRoomAllocation->Get_Related_Room_Allocation_Info();
				$numOfRelatedRoomAllocation = count($relatedRoomAllocationInfoAry);
			}
			else
			{
				$thisSubjectID = '';
				$thisLocationID = '';
				$thisLocationLevelID = '';
				$thisBuildingID = '';
				$js_RoomAllocationID = 'null';
				$numOfRelatedRoomAllocation = 0;
			}
			
			$warningMsgDiv = '';
			if ($numOfRelatedRoomAllocation > 1) {
				$warningMsgDiv .= $libinterface->Get_Warning_Message_Box('', $Lang['SysMgr']['Timetable']['Warning']['ChangesWillApplyToAllRelatedLesson']);
			}
			
			# Get Time slot info
			$timeSlotObj = new TimeSlot($TimeSlotID);
			$timeSlotName = intranet_htmlspecialchars($timeSlotObj->TimeSlotName);
			
			$startTime = $timeSlotObj->StartTime;
			$endTime = $timeSlotObj->EndTime;
			
			$startTimeHour = substr($startTime, 0, 2);
			$startTimeMin = substr($startTime, 3, 2);
			$startTime = $startTimeHour.":".$startTimeMin;
			
			$endTimeHour = substr($endTime, 0, 2);
			$endTimeMin = substr($endTime, 3, 2);
			$endTime = $endTimeHour.":".$endTimeMin;
			
			$timeSlotDisplay = $startTime."-".$endTime." (".$timeSlotName.")";
			
			# Get Timetable and YearTerm Info
			$timetableObj = new Timetable($timeSlotObj->TimetableID);
			$YearTermID = $timetableObj->YearTermID;
			
			# Subject Filter
			include_once("subject_class_mapping_ui.php");
			if ($SelectedSubjectID == '') {
				$libSubject = new subject();
				$SubjectArr = $libSubject->Get_Subject_List();
				$SelectedSubjectID = $SubjectArr[0]['SubjectID'];
			}
			$libsubject_ui = new subject_class_mapping_ui();
			$subjectSelection = $libsubject_ui->Get_Subject_Selection("SelectedSubjectID_InAddEditLayer", $thisSubjectID, 
										"js_Reload_Subject_Group_Selection_InAddEditLayer(this.value, '$TimeSlotID', '$Day', '$RoomAllocationID', '$YearTermID')", 
										0, $Lang['SysMgr']['SubjectClassMapping']['Select']['Subject'], $YearTermID);
			
			# Subject Group Filter
			if ($RoomAllocationID != '')
			{
				$subjectGroupSelDisplay = '';
				$subjectGroupFilter = $libsubject_ui->Get_Subject_Group_Selection($thisSubjectID, 
																					"SelectedSubjectGroupID_InAddEditLayer", 
																					$thisSubjectGroupID,
																					"js_Validate_Subject_Group_Overlap('$TimeSlotID', '$Day', '$RoomAllocationID')",
																					$YearTermID
																					);
			}
			else
			{
				$subjectGroupSelDisplay = ' style="display:none" ';
			}
			
			# Building Filter
			include_once("liblocation_ui.php");
			$liblocation_ui = new liblocation_ui();
			$buildingSelection = $liblocation_ui->Get_Building_Selection(
									$thisBuildingID, 
									'SelectedBuildingID_InAddEditLayer', 
									"js_Change_Floor_Selection(	'$PATH_WRT_ROOT',
																'SelectedBuildingID_InAddEditLayer', 
																'OthersLocationTb_InAddEditLayer',
																'FloorSelectionTr_InAddEditLayer',
																'FloorSelectionDiv_InAddEditLayer',
																'SelectedFloorID_InAddEditLayer',
																'',
																'RoomSelectionTr_InAddEditLayer',
																'RoomSelectionDiv_InAddEditLayer',
																'SelectedRoomID_InAddEditLayer',
																'',
																'js_Validate_Room_Vacancy($TimeSlotID, $Day, $js_RoomAllocationID)',
																'',
																1
																 );
									",
									$noFirst = 0,
									$withOthersOption = 1
								);
			
			if ($isEdit && $thisLocationID != 0)
			{
				# Floor Filter
				$floorSelDisplay = '';
				$floorFilter = $liblocation_ui->Get_Floor_Selection($thisBuildingID, $thisLocationLevelID, 'SelectedFloorID_InAddEditLayer', 
																	"js_Change_Room_Selection('$PATH_WRT_ROOT', 'SelectedFloorID_InAddEditLayer', 
																								'RoomSelectionTr_InAddEditLayer', 'RoomSelectionDiv_InAddEditLayer', 'SelectedRoomID_InAddEditLayer',
																									'', 'js_Validate_Room_Vacancy($TimeSlotID, $Day, $js_RoomAllocationID)')", 
																	$noFirst = 1,
																	$showNoRoomFloor = 0);
				# Room Filter
				$roomSelDisplay = '';
				$roomFilter = $liblocation_ui->Get_Room_Selection($thisLocationLevelID, $thisLocationID, "SelectedRoomID_InAddEditLayer", 
																	"js_Validate_Room_Vacancy('$TimeSlotID', '$Day', '$RoomAllocationID')", 1);
			}
			else
			{
				$floorSelDisplay = ' style="display:none" ';
				$roomSelDisplay = ' style="display:none" ';
			}
			
			# Others location textbox
			if ($RoomAllocationID != '' && $thisLocationID == 0) {
				$othersLocationTb = '<input id="OthersLocationTb_InAddEditLayer" name="OthersLocation" type="text" size="20" value="'.$thisOthersLocation.'" />'."\n";
			}
			else {
				$othersLocationTb = '<input id="OthersLocationTb_InAddEditLayer" name="OthersLocation" type="text" size="20" style="display:none" />'."\n";
			}
			
			# number of continuous time slot selection
			$timeSlotInfoAry = $timetableObj->Get_TimeSlot_List($ReturnAsso=1);
			$numOfAvailableTimeSlot = 0;
			$startCounting = false;
			foreach ((array)$timeSlotInfoAry as $_timeSlotId => $timeSlot) {
				if ($_timeSlotId == $TimeSlotID) {
					$startCounting = true;
				}
				
				if ($startCounting) {
					$numOfAvailableTimeSlot++;
				}
			}
			$numOfTimeSlotSelection = $libinterface->Get_Number_Selection('ContinuousTimeSlot', 1, $numOfAvailableTimeSlot, $SelectedValue='', $Onchange='', $noFirst=1);
			
			$x = '';
			$x .= '<div id="debugArea"></div>'."\n";
			$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
				$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
				//$x .= '<h1> '.$Lang['SysMgr']['Timetable']['ModuleTitle'].' &gt; <span>'.$thisTitle.'</span></h1>'."\n";
				$x .= '<div class="edit_pop_board_write">'."\n";
					$x .= '<form id="RoomAllocationForm" name="RoomAllocationForm">'."\n";
						$x .= $warningMsgDiv."\n";
						$x .= '<table class="form_table">'."\n";
							$x .= '<col class="field_title" />'."\n";
							$x .= '<col class="field_c" />'."\n";
							
							# Cycle Name display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['Day'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'.$Day.'</td>'."\n"; 
							$x .= '</tr>'."\n";
							# Time Slot display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['TimeSlot'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'.$timeSlotDisplay.'</td>'."\n"; 
							$x .= '</tr>'."\n";
							# No. of continues lesson
							if ($RoomAllocationID == '') {
								$x .= '<tr>'."\n";
									$x .= '<td>'.$Lang['SysMgr']['Timetable']['NumOfContinuousTimeSlot'].'</td>'."\n";
									$x .= '<td>:</td>'."\n";
									$x .= '<td>'.$numOfTimeSlotSelection.'</td>'."\n"; 
								$x .= '</tr>'."\n";
							}
							# Subject Selection
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $subjectSelection;
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Subject Group Selection
							$x .= '<tr id="SubjectGroupSelectionTr_InAddEditLayer" '.$subjectGroupSelDisplay.'>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="SubjectGroupSelectionDiv_InAddEditLayer">'."\n";
										$x .= $subjectGroupFilter;
									$x .= '</div>'."\n";
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("SubjectGroupWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Building Selection
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Location']['Building'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span>'.$buildingSelection.'</span>&nbsp;&nbsp;';
									$x .= '<span>'.$othersLocationTb.'</span>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Floor Selection
							$x .= '<tr id="FloorSelectionTr_InAddEditLayer" '.$floorSelDisplay.'>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Location']['Floor'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="FloorSelectionDiv_InAddEditLayer">'."\n";
										$x .= $floorFilter;
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Location Selection
							$x .= '<tr id="RoomSelectionTr_InAddEditLayer" '.$roomSelDisplay.'>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Location']['Room'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="RoomSelectionDiv_InAddEditLayer">'."\n";
										$x .= $roomFilter;
									$x .= '</div>'."\n";
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("LocationWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</form>'."\n";
				$x .= '</div>'."\n";
				$x .= '<br />'."\n";
				
				$x .= '<div class="edit_bottom">'."\n";
					$x .= '<span></span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					if ($isEdit)
					{
						$x .= $btn_Edit."\n";
					}
					else
					{
						$x .= $btn_Add_Finish."\n";
						$x .= $btn_Add_AddMore."\n";
					}
					$x .= $btn_Cancel."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		
		############## Start View Page ###################
		function Get_View_Settings_Table($ViewMode="", $From_eService=0)
		{
			global $Lang;
			
			include_once("libinterface.php");
			$linterface = new interface_html();
			
			$academicYearInfoArr = getCurrentAcademicYearAndYearTerm();
			$curAcademicYearID = $academicYearInfoArr['AcademicYearID'];
			$curYearTermID = $academicYearInfoArr['YearTermID'];
			
			
			# Acadermic Year Filter
//			if ($From_eService) {
//				// Current Year Timetable only
//				$CurAcademicYearName = getAcademicYearByAcademicYearID($curAcademicYearID, '');
//				
//				$acadermicYearDisplay = '';
//				$acadermicYearDisplay .= $CurAcademicYearName;
//				$acadermicYearDisplay .= '<input type="hidden" id="SelectedAcademicYearID" name="SelectedAcademicYearID" value="'.$curAcademicYearID.'" />';
//			}
//			else {
				$thisOnchange = 'onchange="js_Reload_Term_Selection(this.value, 0)"';
				$acadermicYearDisplay = getSelectAcademicYear("SelectedAcademicYearID", $thisOnchange, 1);
//			}
			
			
			# Term Filter
			include_once("subject_class_mapping_ui.php");
			$subject_class_mapping_ui = new subject_class_mapping_ui();
			$term_jsOnchange = "js_Reload_Timetable_Selection('', 0); MM_showHideLayers('SubjectGroupSelectionDiv','','hide');";
			$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID", '', '', $term_jsOnchange, 1);
			
			# Timetable Filter
			//$timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID", $curAcademicYearID, $curYearTermID, '', '', 0, '', 1);
			$ReleaseStatus = ($From_eService)? 1 : '';	// show public timetable only if from eService, show both public and private for eAdmin
			$timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID", $curAcademicYearID, $curYearTermID, $SelectedTimetableID='', $OnChange='', $noFirst=0, $ParFirstTitle='', $ShowAllocatedOnly=0, $ExcludeTimetableID='', $IncludeTimetableIDArr='', $ReleaseStatus);
			
			# View Mode Filter
			$viewModeSelection = $this->Get_View_Mode_Selection("SelectedViewMode", "SelectedViewMode", $ViewMode, "js_Reload_Secondary_Selection()");
			
			$table = '';
			$table .= '<table id="FilterTable" class="form_table">'."\n";
				$table .= '<col class="field_title" style="vertical-align:top"/>'."\n";
				$table .= '<col class="field_c" style="vertical-align:top"/>'."\n";
				
				# Print in one timetable
				$table .= '<tr>'."\n";
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['PrintDataInOneTimetable'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'."\n";
						$table .= $linterface->Get_Radio_Button("PrintDataInOneTimetable_1", "PrintDataInOneTimetable", 1, $isChecked=0, $Class="", $Lang['General']['Yes']);
						$table .= $linterface->Get_Radio_Button("PrintDataInOneTimetable_0", "PrintDataInOneTimetable", 0, $isChecked=2, $Class="", $Lang['General']['No']);
					$table .= '</td>'."\n";
				$table .= '</tr>'."\n";
				
				# Acadermic Year
				$table .= '<tr>'."\n";
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['AcademicYear'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'.$acadermicYearDisplay.'</td>'."\n";
				$table .= '</tr>'."\n";
				# Term
				$table .= '<tr>'."\n";
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'."\n";
						$table .= '<div id="TermSelectionDiv">'."\n";
							$table .= $termSelection;
						$table .= '</div>'."\n";
					$table .= '</td>'."\n";
				$table .= '</tr>'."\n";
				# Timetable
				$table .= '<tr>'."\n";
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['Timetable'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'."\n";
						$table .= '<div id="TimetableSelectionDiv">'."\n";
							$table .= $timetableSelection;
						$table .= '</div>'."\n";
					$table .= '</td>'."\n";
				$table .= '</tr>'."\n";
				# View Mode
				$table .= '<tr>'."\n";
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['ViewMode'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'.$viewModeSelection.'</td>'."\n";
				$table .= '</tr>'."\n";
			$table .= '</table>'."\n";
			
			return $table;
		}
		
		function Get_View_Secondary_Settings_Table($ViewMode, $InLayer=0, $YearTermID='')
		{
			global $Lang, $PATH_WRT_ROOT;
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			$table = '';
			$table .= '<table id="SecondaryFilterTable" class="form_table">'."\n";
				$table .= '<col class="field_title" style="vertical-align:top"/>'."\n";
				$table .= '<col class="field_c" style="vertical-align:top"/>'."\n";
				
				if ($ViewMode == "Class")
				{
					include_once("form_class_manage_ui.php");
					$form_class_manage_ui = new form_class_manage_ui();
					$formSelection = $form_class_manage_ui->Get_Form_Selection('SelectedYearID', "", "js_Reload_Class_Selection(1)", 0);
	
					# Form Selection
					$table .= '<tr id="FormSelectionTr" >'."\n";
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="FormSelectionDiv">'.$formSelection.'</div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Class Multiple Selection
					$table .= '<tr id="ClassSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="ClassSelectionDiv"></div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($ViewMode == "SubjectGroup")
				{
					include_once("subject_class_mapping_ui.php");
					$libsubject_ui = new subject_class_mapping_ui();
					$subjectSelection = $libsubject_ui->Get_Subject_Selection('SelectedSubjectID', 
																				'', 
																				"js_Reload_Subject_Group_Selection()", 
																				0, 
																				$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject'],
																				$YearTermID);
					
					# Subject Selection
					$table .= '<tr id="SubjectSelectionTr">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Subject'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="SubjectSelectionDiv">'.$subjectSelection.'</div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Subject Group Multiple Selection
					$table .= '<tr id="SubjectGroupSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="SubjectGroupSelectionDiv"></div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($ViewMode == "Personal")
				{
					$identitySelection = $this->Get_Identity_Selection('SelectedIdentity', 'SelectedIdentity', "", "js_Show_Hide_Form_Class_Selection()");
					
					include_once("form_class_manage_ui.php");
					$form_class_manage_ui = new form_class_manage_ui();
					$formSelection = $form_class_manage_ui->Get_Form_Selection('SelectedYearID', "", "js_Reload_Class_Selection(0)", 0);
					
					# Identity Selection
					$table .= '<tr id="IdentitySelectionTr">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['Timetable']['Identity'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="IdentitySelectionDiv">'.$identitySelection.'</div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Form Selection
					$table .= '<tr id="FormSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="FormSelectionDiv">'.$formSelection.'</div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Class Selection
					$table .= '<tr id="ClassSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="ClassSelectionDiv"></div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Target Multiple Selection
					/*
					if ($Identity == "TeachingStaff")
						$thisTitle = $Lang['SysMgr']['RoleManagement']['TeachingStaff'];
					else if ($Identity == "NonTeachingStaff")
						$thisTitle = $Lang['SysMgr']['RoleManagement']['SupportStaff'];
					else if ($Identity == "Student")
						$thisTitle = $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'];
					*/
					
					$table .= '<tr id="PersonSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['Timetable']['User'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="PersonSelectionDiv"></div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($ViewMode == "Room")
				{
					# Building Filter
					include_once("liblocation_ui.php");
					$liblocation_ui = new liblocation_ui();
					$buildingSelection = $liblocation_ui->Get_Building_Selection(
																$SelectedBuildingID, 
																'SelectedBuildingID', 
																"js_Change_Floor_Selection(	'/',
																							'SelectedBuildingID', 
																							'OthersLocationTb',
																							'FloorSelectionTr',
																							'FloorSelectionDiv',
																							'SelectedLocationLevelID',
																							'',
																							'RoomSelectionTr',
																							'RoomSelectionDiv',
																							'SelectedLocationIDArr[]',
																							'',
																							'',
																							1
																							 );
																",
																$NoFirst = 0,
																$WithOthersLocation = 1
															);
					$othersLocationTb = '<input id="OthersLocationTb" name="OthersLocation" type="text" size="20" style="display:none" />'."\n";
		
					# Building Selection
					$table .= '<tr id="BuildingSelectionTr">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['Location']['Building'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<span id="BuildingSelectionDiv">'.$buildingSelection.'</span>&nbsp;&nbsp;'."\n";
							$table .= '<span>'.$othersLocationTb.'</span>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Floor Selection
					$table .= '<tr id="FloorSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['Location']['Floor'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="FloorSelectionDiv"></div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Room Multiple Selection
					$table .= '<tr id="RoomSelectionTr" style="display:none">'."\n";
						$table .= '<td>'.$Lang['SysMgr']['Location']['Room'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>'."\n";
							$table .= '<div id="RoomSelectionDiv"></div>'."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				
				# Empty Row
				$table .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
			$table .= '</table>'."\n";
			
			# Button Submit
			if ($InLayer)
				$btn_Submit = $libinterface->GET_BTN($Lang['Btn']['Submit'], "button", "Generate_Timetable()", "GenerateBtn");
			else
				$btn_Submit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "Generate_Timetable()", "GenerateBtn");
				
			$table .= '<div id="SubmitBtnDiv" class="edit_bottom" style="display:none">'."\n";
				$table .= '<span></span>'."\n";
				$table .= '<p class="spacer"></p>'."\n";
					$table .= $btn_Submit."\n";
				$table .= '<p class="spacer"></p>'."\n";
			$table .= '</div>'."\n";
			
			return $table;
		}
		
		function Get_View_Mode_Selection($ID, $Name, $SelectedMode="", $OnChange="")
		{
			global $Lang;
			
			$selectionArr['Class'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Class'];
			$selectionArr['SubjectGroup'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['SubjectGroup'];
			$selectionArr['Personal'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Personal'];
			$selectionArr['Room'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
				
			$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['ViewMode']);
			
			$selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedMode, $all=0, $noFirst=0, $firstTitle);
			
			return $selection;
		}
		
		function Get_Identity_Selection($ID, $Name, $SelectedIdentity="", $OnChange="")
		{
			global $Lang;
			
			$selectionArr['TeachingStaff'] = $Lang['Identity']['TeachingStaff'];
			//$selectionArr['NonTeachingStaff'] = $Lang['Identity']['NonTeachingStaff'];
			$selectionArr['Student'] = $Lang['Identity']['Student'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
			
			$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['Identity']);
			
			$selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedMode, $all=0, $noFirst=0, $firstTitle);
			
			return $selection;
		}
		############## End View Page ###################
		
		function Get_Display_Option_Div_Table()
		{
			global $Lang, $image_path, $LAYOUT_SKIN;
			
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			//$subSelectionPrefix = "&nbsp;&nbsp;";
			$subSelectionPrefix = "";
			
			# Close Filter Table Button
			$closeDisplayOption_jsOnchange = "js_Show_Hide_Display_Option_Div(); return false;";
			$closeDisplayOptionBtn = '<a href="#" class="tablelink" onclick="'.$closeDisplayOption_jsOnchange.'"> '.$Lang['Btn']['Close'].'</a>'."\n";
			
			# Display Option Layer Table
			$displayOptionTable = '';
			$displayOptionTable .= '<table id="FilterSelectionTable" width="100%" cellpadding="2" cellspacing="0">'."\n";
				# Close Btn
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td style="text-align:left">'."\n";
						$displayOptionTable .= $Lang['SysMgr']['Timetable']['DisplayOption'];
					$displayOptionTable .= '</td>'."\n";
					$displayOptionTable .= '<td style="text-align:right">'."\n";
						$displayOptionTable .= $closeDisplayOptionBtn;
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				# Select All Button
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left;vertical-align:top">'."\n";
						$displayOptionTable .= $libinterface->Get_Checkbox("SelectAllChk", "SelectAllChk", 
														"1", $isChecked=1, "DisplayOptionChkBox", $Lang['Btn']['SelectAll'], 
														"js_Update_Select_All_Display_Option('SelectAllChk'); js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				# Separator
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" align="left" valign="top" class="dotline" height="2"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" height="2"></td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				# Subject Group Title
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left;vertical-align:top">'."\n";
						$displayOptionTable .= $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				# Subject Title
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowSubjectTitleChk", "ShowSubjectTitleChk", 
																		"1", $isChecked=1, "DisplayOptionChkBox", $Lang['SysMgr']['Timetable']['SubjectTitle'], "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				# Number of Student
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowNumOfStudentChk", "ShowNumOfStudentChk", 
																		"1", $isChecked=1, "DisplayOptionChkBox", $Lang['SysMgr']['Timetable']['NumOfStudent'], "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				# Subject Teacher Name
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowSubjectTeacherNameChk", "ShowSubjectTeacherNameChk", 
																		"1", $isChecked=1, "DisplayOptionChkBox", $Lang['SysMgr']['Timetable']['TeacherName'], "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				# Subject Teacher UserLogin
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowSubjectTeacherUserLoginChk", "ShowSubjectTeacherUserLoginChk", 
																		"1", $isChecked=0, "DisplayOptionChkBox", $Lang['SysMgr']['Timetable']['TeacherLoginID'], "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				
				# Separator
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" align="left" valign="top" class="dotline" height="2"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" height="2"></td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				# Location Title
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left;vertical-align:top">'."\n";
						$displayOptionTable .= $Lang['SysMgr']['Location']['Location'];
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				# Building
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowBuildingChk", "ShowBuildingChk", 
																		"1", $isChecked=1, "DisplayOptionChkBox", $Lang['SysMgr']['Location']['Building'], "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				# Floor
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowFloorChk", "ShowFloorChk", 
																		"1", $isChecked=1, "DisplayOptionChkBox", $Lang['SysMgr']['Location']['Floor'], "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				# Room
				$thisTitle = $Lang['SysMgr']['Location']['Room&OthersLocation'];
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td colspan="2" style="text-align:left">'."\n";
						$displayOptionTable .= $subSelectionPrefix.$libinterface->Get_Checkbox("ShowRoomChk", "ShowRoomChk", 
																		"1", $isChecked=1, "DisplayOptionChkBox", $thisTitle, "js_Update_Timetable_Display();");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				
				
				
				/*
				# Submit
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td align="left" valign="top" class="dotline" height="2"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" height="2"></td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				$displayOptionTable .= '<tr>'."\n";
					$displayOptionTable .= '<td class="tabletext" style="text-align:center">'."\n";
						$displayOptionTable .= $libinterface->GET_BTN($Lang['Btn']['Submit'], "button", "javascript: onClick=MM_showHideLayers('SubjectGroupSelectionDiv','','hide');");
					$displayOptionTable .= '</td>'."\n";
				$displayOptionTable .= '</tr>'."\n";
				*/
			$displayOptionTable .= '</table>'."\n";
			
			return $displayOptionTable;
		}
		
		function Get_View_Selection_Div_Table()
		{
			global $Lang;
			
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			# View Mode Filter
			$viewModeSelection = $this->Get_View_Mode_Selection("SelectedViewMode", "SelectedViewMode", $ViewMode, "js_Reload_Secondary_Selection(this.value)");
			
			# Close Filter Table Button
			$close_jsOnchange = "js_Show_Hide_Filter_Div(); return false;";
			$closeFilterBtn = '<a href="#" class="tablelink" onclick="'.$close_jsOnchange.'"> '.$Lang['Btn']['Close'].'</a>'."\n";
			
			# Reset Filter Table Button
			$reset_jsOnchange = "js_Reset_Filter(); return false;";
			$resetFilterBtn = '<a href="#" class="tablelink" onclick="'.$reset_jsOnchange.'"> '.$Lang['SysMgr']['Timetable']['Button']['ClearFiltering'].'</a>'."\n";
			
			# Filter Layer Table
			$filterTable = '';
			$filterTable .= '<table id="FilterSelectionTable" class="form_table">'."\n";
				$filterTable .= '<col class="field_title" style="vertical-align:top"/>'."\n";
				$filterTable .= '<col class="field_c" style="vertical-align:top"/>'."\n";
				# Close Btn
				$filterTable .= '<tr>'."\n";
					$filterTable .= '<td colspan="3" style="text-align:right">'."\n";
						$filterTable .= $resetFilterBtn;
						$filterTable .= " | ";
						$filterTable .= $closeFilterBtn;
					$filterTable .= '</td>'."\n";
				$filterTable .= '</tr>'."\n";
				# View Mode
				$filterTable .= '<tr>'."\n";
					$filterTable .= '<td>'.$Lang['SysMgr']['Timetable']['ViewMode'].'</td>'."\n";
					$filterTable .= '<td>:</td>'."\n";
					$filterTable .= '<td>'.$viewModeSelection.'</td>'."\n";
				$filterTable .= '</tr>'."\n";
			$filterTable .= '</table>'."\n";
			# Secondary Filter
			$filterTable .= '<div id="SecondaryFilterDiv">'."\n";
			$filterTable .= '</div>'."\n";
			
			return $filterTable;
		}
		
		
		### Teacher have Subject Group filtering only
		function Get_View_Selection_Div_Table_For_Non_Admin_User($ParUserID)
		{
			global $Lang, $PATH_WRT_ROOT, $linterface;
			
			if (!isset($linterface))
			{
				include_once($PATH_WRT_ROOT.'includes/libinterface.php');
				$linterface = new interface_html();
			}
			
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
			$libSCM_ui = new subject_class_mapping_ui();
			
			# Close Filter Table Button
			$close_jsOnchange = "js_Show_Hide_Filter_Div(); return false;";
			$closeFilterBtn = '<a href="#" class="tablelink" onclick="'.$close_jsOnchange.'"> '.$Lang['Btn']['Close'].'</a>'."\n";
			
			# Filter Layer Table
			$filterTable = '';
			$filterTable .= '<table id="FilterSelectionTable" class="form_table">'."\n";
				$filterTable .= '<col class="field_title" style="vertical-align:top"/>'."\n";
				$filterTable .= '<col class="field_c" style="vertical-align:top"/>'."\n";
				# Close Btn
				$filterTable .= '<tr>'."\n";
					$filterTable .= '<td colspan="3" style="text-align:right">'."\n";
						$filterTable .= $closeFilterBtn;
					$filterTable .= '</td>'."\n";
				$filterTable .= '</tr>'."\n";
				# View Mode
				$filterTable .= '<tr>'."\n";
					$filterTable .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
					$filterTable .= '<td>:</td>'."\n";
					$filterTable .= '<td>'."\n";
						$filterTable .= $libSCM_ui->Get_User_Accessible_Subject_Group_Selection('SubjectGroupIDArr[]', 'SubjectGroupIDArr[]', $ParUserID, Get_Current_Academic_Year_ID(), '', '', $IsMultiple=1);
						$filterTable .= $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_With_OptGroup('SubjectGroupIDArr[]', 1);");
						$filterTable .= '</td>'."\n";
				$filterTable .= '</tr>'."\n";
			$filterTable .= '</table>'."\n";
			
			$filterTable .= '<div class="edit_bottom">'."\n";
				$filterTable .= '<span></span>'."\n";
				$filterTable .= '<p class="spacer"></p>'."\n";
					$filterTable .= $linterface->GET_BTN($Lang['Btn']['Submit'], "button", "js_Reload_Timetable()", "GenerateBtn")."\n";
				$filterTable .= '<p class="spacer"></p>'."\n";
			$filterTable .= '</div>'."\n";
			
			return $filterTable;
		}
		
		function Get_Import_Lesson_Step1_UI($TimetableID)
		{
			global $PATH_WRT_ROOT, $Lang, $linterface, $image_path, $LAYOUT_SKIN, $intranet_session_language;
			
			### Get Timetable's Academic Year and Term Info
			$ObjTimetable = new Timetable($TimetableID);
			$TimetableName = $ObjTimetable->Get_Timetable_Name();
			$CycleDays = $ObjTimetable->CycleDays;
			$TimeSlotArr = $ObjTimetable->Get_TimeSlot_List();
			$numOfTimeSlot = count($TimeSlotArr);
			
			include_once($PATH_WRT_ROOT.'includes/liblocation.php');
			$liblocationBuilding = new Building();
			$liblocationFloor = new Floor();
			$liblocationRoom = new Room();
			
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			$AcademicYearID = $ObjTimetable->AcademicYearID;
			$ObjAcademicYear = new academic_year($AcademicYearID);
			$AcademicYearName = $ObjAcademicYear->Get_Academic_Year_Name();
			
			$YearTermID = $ObjTimetable->YearTermID;
			$ObjYearTerm = new academic_year_term($YearTermID);
			$YearTermName = $ObjYearTerm->Get_Year_Term_Name();
			
			# navaigtion
			$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Timetable']['MangeTimetable'], "javascript:js_Go_Back();");
			$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Timetable']['Button']['ImportLesson'], "");
			
			# csv sample
			$SampleCSVFile = "<a class='tablelink' href='javascript:js_Go_Download_CSV_Sample();'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
			
			# column property display
			$ColumnTitleArr = $ObjTimetable->Get_Import_Lesson_Column_Title();
			$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();
			
			$RemarksArr = array();
			$RemarksArr[0] = '<a id="remarkBtn_day" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'day\');">['.$Lang['General']['Remark'].']</a>';
			$RemarksArr[1] = '<a id="remarkBtn_timeSlot" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'timeSlot\');">['.$Lang['General']['Remark'].']</a>';
			$RemarksArr[4] = '<a id="remarkBtn_subjectGroup" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'subjectGroup\');">['.$Lang['General']['Remark'].']</a>';
			$RemarksArr[6] = '<a id="remarkBtn_building" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'building\');">['.$Lang['General']['Remark'].']</a>';
			$RemarksArr[8] = '<a id="remarkBtn_location" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'location\');">['.$Lang['General']['Remark'].']</a>';
			$RemarksArr[10] = '<a id="remarkBtn_sublocation" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'sublocation\');">['.$Lang['General']['Remark'].']</a>';
			
			
			$ColumnDisplay = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
			
			# buttons
			$ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
			$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");
			$x = '';
			$x .= '<br />'."\n";
			$x .= '<form id="form1" name="form1" method="post" action="import_class_student_confirm.php" enctype="multipart/form-data">'."\n";
				$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
				$x .= '<br />'."\n";
				$x .= $linterface->GET_IMPORT_STEPS($CurrStep=1);
				$x .= '<div class="table_board">'."\n";
					$x .= '<table id="html_body_frame" width="100%">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>'."\n";
								$x .= '<table class="form_table_v30">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['SysMgr']['FormClassMapping']['SchoolYear'].'</td>'."\n";
										$x .= '<td>'.$AcademicYearName.'</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
										$x .= '<td>'.$YearTermName.'</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Timetable'].'</td>'."\n";
										$x .= '<td>'.$TimetableName.'</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
										$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
										$x .= '<td>'.$SampleCSVFile.'</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="field_title">'."\n";
											$x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']."\n";
										$x .= '</td>'."\n";
										$x .= '<td>'."\n";
											$x .= $ColumnDisplay."\n";
											$x .= "<br /><br />\n";
											$x .= '<span class="tabletextremark">('.$Lang['SysMgr']['Timetable']['ImportLesson']['LocationRemarks'].')</span>'."\n";
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td class="tabletextremark" colspan="2">'."\n";
											$x .= $linterface->MandatoryField();
											$x .= $linterface->ReferenceField();
										$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
								$x .= '</table>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</div>'."\n";
				
				# Buttons
				$x .= '<div class="edit_bottom_v30">'."\n";
					$x .= $ContinueBtn;
					$x .= "&nbsp;";
					$x .= $CancelBtn;
				$x .= '</div>'."\n";
				
				# Hidden Field
				$x .= '<input type="hidden" id="TimetableID" name="TimetableID" value="'.$TimetableID.'">'."\n";
				$x .= '<input type="hidden" id="ForImportSample" name="ForImportSample" value="1">'."\n";
				
				# Day Remarks Layer
				$thisRemarksType = 'day';
				$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$x .= '<tbody>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$x .= '<thead>'."\n";
											$x .= '<tr>'."\n";
												$x .= '<th>'.$Lang['General']['InputCode'].'</th>'."\n";
												$x .= '<th>'.$ColumnTitleArr[0].'</th>'."\n";
											$x .= '</tr>'."\n";
										$x .= '</thead>'."\n";
										$x .= '<tbody>'."\n";
											if ($CycleDays == 0) {
												$x .= '<tr><td colspan="2" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
											}
											else {
												for ($i=0; $i<$CycleDays; $i++) {
													$_day = $i + 1;
													
													$x .= '<tr>'."\n";
														$x .= '<td>'.$_day.'</td>'."\n";
														$x .= '<td>'.$Lang['SysMgr']['Timetable']['Day'].' '.$_day.'</td>'."\n";
													$x .= '</tr>'."\n";
												}
											}
										$x .= '</tbody>'."\n";
									$x .= '</table>'."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
						$x .= '</tbody>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</div>'."\r\n";
				
				
				
				# TimeSlot Remarks Layer
				$thisRemarksType = 'timeSlot';
				$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$x .= '<tbody>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$x .= '<thead>'."\n";
											$x .= '<tr>'."\n";
												$x .= '<th>'.$Lang['General']['InputCode'].'</th>'."\n";
												$x .= '<th>'.$ColumnTitleArr[2].'</th>'."\n";
												$x .= '<th>'.$ColumnTitleArr[3].'</th>'."\n";
											$x .= '</tr>'."\n";
										$x .= '</thead>'."\n";
										$x .= '<tbody>'."\n";
											if ($numOfTimeSlot == 0) {
												$x .= '<tr><td colspan="3" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
											}
											else {
												for ($i=0; $i<$numOfTimeSlot; $i++) {
													$_orderIndex = $i + 1;
													$_timeSlotName = $TimeSlotArr[$i]['TimeSlotName'];
													$_startTime = $TimeSlotArr[$i]['StartTime'];
													$_endTime = $TimeSlotArr[$i]['EndTime'];
													
													$_startTime = substr($_startTime, 0, 5);	// Hour and Minute only
													$_endTime = substr($_endTime, 0, 5);
													$_timeSlotTimeRange = $_startTime.' - '.$_endTime;
													
													$x .= '<tr>'."\n";
														$x .= '<td>'.$_orderIndex.'</td>'."\n";
														$x .= '<td>'.$_timeSlotName.'</td>'."\n";
														$x .= '<td>'.$_timeSlotTimeRange.'</td>'."\n";
													$x .= '</tr>'."\n";
												}
											}
										$x .= '</tbody>'."\n";
									$x .= '</table>'."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
						$x .= '</tbody>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</div>'."\r\n";
			
			
			
			
			# Subject Group Remarks Layer
			$subjectGroupArr = $this->Get_Subject_Group_Info($YearTermID);
			$x .= $this->Get_Remarks_Layer("subjectGroup", $Lang['General']['InputCode'], $ColumnTitleArr[5], $subjectGroupArr, 'ClassCode', ($intranet_session_language=='en')?'ClassTitleEN':'ClassTitleB5');	
			
			# Building Remarks Layer
			$buildingArr = $liblocationBuilding-> Get_All_Building();
			$x .= $this->Get_Remarks_Layer("building", $Lang['General']['InputCode'], $ColumnTitleArr[7],$buildingArr, 'Code', ($intranet_session_language=='en')?'NameEng':'NameChi');	
			
			# Location Remarks Layer
			$locationArr = $liblocationFloor->Get_All_Floor();
			$x .= $this->Get_Remarks_Layer("location", $Lang['General']['InputCode'], $ColumnTitleArr[9], $locationArr, 'Code', ($intranet_session_language=='en')?'NameEng':'NameChi');	
			
			# Sublocation Remarks Layer
			$sublocationArr = $liblocationRoom->Get_All_Room();
			$x .= $this->Get_Remarks_Layer("sublocation", $Lang['General']['InputCode'], $ColumnTitleArr[9], $sublocationArr, 'Code', ($intranet_session_language=='en')?'NameEng':'NameChi');	
		
			$x .= '</form>'."\n";
			$x .= '<br />'."\n";
				
			return $x;
		}
		
		function Get_Remarks_Layer($thisRemarksType, $heading1, $heading2, $dataArry, $key1, $key2){
				//Remarks Layer
				global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang;
				
					$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
    					$x.= '<div id="tableHeaderID" style="position:sticky; top:0px; margin-bottom: 1px; BACKGROUND-COLOR: #f3f3f3;">';
        					$x.= '<div style="display: inline;">&nbsp;</div>';
        					$x.= '<div style="display: inline; float: right; padding-right: 2px;">';
        					   $x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
        					$x.= '</div>';
    					$x.= '</div>';
						$x.= '<div id="lessonTableID" style="max-height:280px; overflow-y:auto; width: 400px;">';						
						  $x.= '<div>';
							$x .= '<table cellspacing="0" cellpadding="3" align="center" border="0" width="95%">'."\r\n";
								$x .= '<tbody>'."\r\n";
									$x .= '<tr>'."\r\n";
										$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
// 											$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
										$x .= '</td>'."\r\n";
									$x .= '</tr>'."\r\n";
									$x .= '<tr>'."\r\n";
										$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
											$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
												$x .= '<thead>'."\n";
													$x .= '<tr>'."\n";
														$x .= '<th>'.$heading1.'</th>'."\n";
														$x .= '<th>'.$heading2.'</th>'."\n";
														//$x .= '<th>'.$ColumnTitleArr[3].'</th>'."\n";
													$x .= '</tr>'."\n";
												$x .= '</thead>'."\n";
												$x .= '<tbody>'."\n";
													if ($dataArry == 0) {
														$x .= '<tr><td colspan="3" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
													}
													else {
														$numOfRow = count($dataArry);
														for ($i=0; $i<$numOfRow; $i++) {	
															$x .= '<tr>'."\n";
																$x .= '<td>'.$dataArry[$i][$key1].'</td>'."\n";
																$x .= '<td>'.$dataArry[$i][$key2].'</td>'."\n";
															$x .= '</tr>'."\n";
														}
													}
												$x .= '</tbody>'."\n";
											$x .= '</table>'."\r\n";
										$x .= '</td>'."\r\n";
									$x .= '</tr>'."\r\n";
								$x .= '</tbody>'."\r\n";
							$x .= '</table>'."\r\n";
							$x .= '</div>';
						$x .= '</div>'."\r\n";
					$x .= '</div>'."\r\n";
				$x .= '<br />'."\n";	
			
			
			return $x;
		}
		
		
		function Get_Tag_Array($CurrenrTag)
		{
		    global $Lang, $sys_custom;
			
			$TagArr = array();
			$TagArr[] = array($Lang['SysMgr']['Timetable']['Tag']['Management'], "index.php?clearCoo=1", ($CurrenrTag===1));
			$TagArr[] = array($Lang['SysMgr']['Timetable']['Tag']['View'], "view.php?clearCoo=1", ($CurrenrTag===2));
			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    $TagArr[] = array($Lang['SysMgr']['Timetable']['Tag']['EverydayTimetable'], "everyday_timetable.php?clearCoo=1", ($CurrenrTag===3));
			}
			
			return $TagArr;
		}
		
		function Get_Timetable_View_Mode_Tab($CurrentMode) {
			global $Lang, $linterface, $sys_custom;
			
			$pages_arr = array();
			
			if ($sys_custom['Timetable']['ShowEmptyLesson_Room']) {
				$pages_arr[] = array($Lang['SysMgr']['Timetable']['FreeRoom'], "free_lesson_view.php?viewType=Room", $img_src='', $CurrentMode=='FreeLessonRoom');
			}
			
			if ($sys_custom['Timetable']['ShowEmptyLesson_Teacher']) {
				$pages_arr[] = array($Lang['SysMgr']['Timetable']['TeacherFreeLesson'], "free_lesson_view.php?viewType=Teacher", $img_src='', $CurrentMode=='FreeLessonTeacher');
			}
			
			if ($sys_custom['Timetable']['ShowEmptyLesson_Teacher'] || $sys_custom['Timetable']['ShowEmptyLesson_Room']) {
				$pages_arr[] = array($Lang['SysMgr']['Timetable']['Timetable'], "view.php", $img_src='', $CurrentMode=='Timetable');
			}
			
			
			return $linterface->GET_CONTENT_TOP_BTN($pages_arr);
		}
		
		function Get_Timetable_View_Option_Filtering_Table($viewType, $From_eService=false) {
			global $Lang, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$linterface = new interface_html();
			
			$academicYearInfoArr = getCurrentAcademicYearAndYearTerm();
			$curAcademicYearID = $academicYearInfoArr['AcademicYearID'];
			$curYearTermID = $academicYearInfoArr['YearTermID'];
			
			# Acadermic Year Filter
//			if ($From_eService) {
//				// Current Year Timetable only
//				$CurAcademicYearName = getAcademicYearByAcademicYearID($curAcademicYearID, '');
//				
//				$acadermicYearDisplay = '';
//				$acadermicYearDisplay .= $CurAcademicYearName;
//				$acadermicYearDisplay .= '<input type="hidden" id="SelectedAcademicYearID" name="SelectedAcademicYearID" value="'.$curAcademicYearID.'" />';
//			}
//			else {
				$thisOnchange = 'onchange="changedAcademicYearSelection(this.value)"';
				$acadermicYearDisplay = getSelectAcademicYear("SelectedAcademicYearID", $thisOnchange, 1);
//			}
			
			
			# Term Filter
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
			$subject_class_mapping_ui = new subject_class_mapping_ui();
			$term_jsOnchange = "changedTermSelection(this.value);";
			$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID", '', '', $term_jsOnchange, 1);
			
			# Timetable Filter
			//$timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID", $curAcademicYearID, $curYearTermID, '', '', 0, '', 1);
			$ReleaseStatus = ($From_eService)? 1 : '';	// show public timetable only if from eService, show both public and private for eAdmin
			$timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID", $curAcademicYearID, $curYearTermID, $SelectedTimetableID='', $OnChange='changedTimetableSelection(this.value);', $noFirst=0, $ParFirstTitle='', $ShowAllocatedOnly=0, $ExcludeTimetableID='', $IncludeTimetableIDArr='', $ReleaseStatus);
			
			$table = '';
			$table .= '<table id="filterOptionTable" class="form_table_v30">'."\n";
				# Acadermic Year
				$table .= '<tr>'."\n";
					$table .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['AcademicYear'].'</td>'."\n";
					$table .= '<td>'.$acadermicYearDisplay.'</td>'."\n";
				$table .= '</tr>'."\n";
				# Term
				$table .= '<tr>'."\n";
					$table .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
					$table .= '<td>'."\n";
						$table .= '<div id="TermSelectionDiv">'."\n";
							$table .= $termSelection;
						$table .= '</div>'."\n";
					$table .= '</td>'."\n";
				$table .= '</tr>'."\n";
				# Timetable
				$table .= '<tr>'."\n";
					$table .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Timetable'].'</td>'."\n";
					$table .= '<td>'."\n";
						$table .= '<div id="TimetableSelectionDiv">'."\n";
							$table .= $timetableSelection;
						$table .= '</div>'."\n";
						$table .= $linterface->Get_Form_Warning_Msg('warningSelectTimetableDiv', $Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Timetable'], $Class='warningDiv');
					$table .= '</td>'."\n";
				$table .= '</tr>'."\n";
				# Time Slot
				$table .= '<tr>'."\n";
					$table .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['TimeSlot'].'</td>'."\n";
					$table .= '<td>'."\n";
						$table .= '<div id="TimeSlotCheckboxesDiv">'."\n";
							$table .= $Lang['General']['EmptySymbol'];
						$table .= '</div>'."\n";
						$table .= $linterface->Get_Form_Warning_Msg('warningSelectTimeSlotDiv', $Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['TimeSlot'], $Class='warningDiv');
					$table .= '</td>'."\n";
				$table .= '</tr>'."\n";
				
				
				if ($viewType == 'Teacher') {
					include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
					$libfcm = new form_class_manage_ui();
					
					$teacherSel = $libfcm->Get_Staff_Selection('teacherIdAry[]', $isTeachingStaff=1, $SelectedUserID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $ParID='teacherSel');
					$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('teacherSel', 1);", 'selectAllBtn');
					$teacherSelDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($teacherSel, $selectAllBtn);
					
					# Teacher Selection
					$table .= '<tr>'."\n";
						$table .= '<td class="field_title">'.$Lang['General']['Teacher'].'</td>'."\n";
						$table .= '<td>'."\n";
							$table .= $teacherSelDiv."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($viewType == 'Room') {
					include_once($PATH_WRT_ROOT.'includes/liblocation_ui.php');
					$liblocation_ui = new liblocation_ui();
					
					$roomSel = $liblocation_ui->Get_Room_Selection_With_Option_Group('roomIdAry[]', $Selected='', $OnChange='', 'roomSel', $isMultiple=true, $noFirst=true);
					$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('roomSel', 1);", 'selectAllBtn');
					$roomSelDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($roomSel, $selectAllBtn);
					
					# Room Selection
					$table .= '<tr>'."\n";
						$table .= '<td class="field_title">'.	$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'].'</td>'."\n";
						$table .= '<td>'."\n";
							$table .= $roomSelDiv."\n";
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				
			$table .= '</table>'."\n";
			
			return $table;
		}
		
		function Get_Free_Lesson_Teacher_Timetable_Table($TimetableID, $IncludedTeacherIdAry='', $IncludedTimeSlotIdAry='') {
			global $Lang, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$libscm = new subject_class_mapping();
			$luser = new libuser();
			
			$objTimetable = new Timetable($TimetableID);
			
			$academicYearID = $objTimetable->AcademicYearID;
			$yearTermID = $objTimetable->YearTermID;
			$numOfCycleDays = $objTimetable->CycleDays;
			
			### Get Timeslot Info
			$timeSlotArr = $objTimetable->Get_Timeslot_List();
			$numOfTimeSlot = count($timeSlotArr);
			
			### Get Lesson
			$roomAllocationArr = $objTimetable->Get_Room_Allocation_List();
			
			### Get all Teachers (limited by the filtering settings)
			$teacherInfoAry = $luser->returnUsersType2(USERTYPE_STAFF, $teaching=true, $IncludedTeacherIdAry);
			$teacherInfoAssoAry = BuildMultiKeyAssoc($teacherInfoAry, 'UserID');
			$numOfTeacher = count($teacherInfoAry);
			
			### Get Teacher info for all Subject Groups
			$subjectGroupTeacherInfoAry = $libscm->Get_Subject_Group_Teacher_Info('', $yearTermID);
			$subjectGroupTeacherAssoAry = BuildMultiKeyAssoc($subjectGroupTeacherInfoAry, 'SubjectGroupID', $IncludedDBField=array('UserID'), $SingleValue=1, $BuildNumericArray=1);
			unset($subjectGroupTeacherInfoAry);
			
			
			### Calculate column width
			$tempTimeSlotWidth = 10;
			if ($numOfCycleDays > 0) {
				$colWidth = floor((100 - $tempTimeSlotWidth) / $numOfCycleDays);
			}
			else {
				$colWidth = 100;
			}
			$timeSlotWidth = 100 - ($colWidth * $numOfCycleDays);	// re-calculate the time slot column width
					
			
			$x = '';
			$x .= '<table class="common_table_list" id="ContentTable">'."\n";
				$x .= '<col align="left" width="'.$timeSlotWidth.'%"></col>'."\n";
				for ($i=0; $i<$numOfCycleDays; $i++) {
					$x .= '<col align="left" width="'.$colWidth.'"></col>'."\n";
				}
				
				### Table header
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
						for ($i=0; $i<$numOfCycleDays; $i++) {
							// Display 1,2,3...
							$thisDay = $i + 1;
							$thisDayDisplay = $Lang['SysMgr']['Timetable']['Day'].' '.$thisDay;
							
							$x .= '<th style="vertical-align:top">'."\n";
								$x .= '<span style="float:left">'.$thisDayDisplay.'</span>'."\n";
							$x .= '</th>'."\n";
						}
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				### Table content
				$x .= '<tbody>'."\n";
					// Loop time slot
					for ($i=0; $i<$numOfTimeSlot; $i++) {
						$_timeSlotID = $timeSlotArr[$i]['TimeSlotID'];
						$_timeSlotName = $timeSlotArr[$i]['TimeSlotName'];
						$_startTime = substr($timeSlotArr[$i]['StartTime'], 0, 5);
						$_endTime = substr($timeSlotArr[$i]['EndTime'], 0, 5);
						$_timeDisplay = $_startTime."-".$_endTime;
						
						if ($IncludedTimeSlotIdAry != '' && !in_array($_timeSlotID, (array)$IncludedTimeSlotIdAry)) {
							continue;
						}
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.$_timeDisplay.'<br />'.$_timeSlotName.'</td>'."\n";
							
							// Loop Days
							for ($j=0; $j<$numOfCycleDays; $j++) {
								$__day = $j + 1;
								$__roomAllocationArr = $roomAllocationArr[$_timeSlotID][$__day];
								
								// Get all teachers having lesson in this time slot of this day
								$__subjectGroupIdAry = Get_Array_By_Key($__roomAllocationArr, 'SubjectGroupID');
								$__numOfSubjectGroup = count($__subjectGroupIdAry);
								$__subjectGroupTeacherAry = array();
								for ($k=0; $k<$__numOfSubjectGroup; $k++) {
									$___subjectGroupId = $__subjectGroupIdAry[$k];
									$__subjectGroupTeacherAry = array_merge($__subjectGroupTeacherAry, (array)$subjectGroupTeacherAssoAry[$___subjectGroupId]);
								}
								
								// Loop all teachers to check if they have lesson in this time slot of this day
								$__freeTeacherIdAry = array();
								for ($k=0; $k<$numOfTeacher; $k++) {
									$___userId = $teacherInfoAry[$k]['UserID'];
									
									if (in_array($___userId, (array)$__subjectGroupTeacherAry)) {
										// teaching in this time slot of this day => not free teacher
									}
									else {
										$__freeTeacherIdAry[] = $___userId;
									}
								}
								
								// display info of the free teacher
								$__numOfFreeTeacher = count($__freeTeacherIdAry);
								$__displayAry = array();
								for ($k=0; $k<$__numOfFreeTeacher; $k++) {
									$___userId = $__freeTeacherIdAry[$k];
									$___userLogin = $teacherInfoAssoAry[$___userId]['UserLogin'];
									
									$__displayAry[] = $___userLogin;
								}
								
								if (count($__displayAry) == 0) {
									$__display = '&nbsp;';
								}
								else {
									$__display = implode('<br />', $__displayAry);
								}
								$x .= '<td>'.$__display.'</td>'."\n";
							}
							
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
				
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		function Get_Free_Room_Timetable_Table($TimetableID, $includedRoomIdAry='', $includedTimeSlotIdAry='') {
			global $Lang, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblocation.php');
			$liblocation = new liblocation();
			
			$objTimetable = new Timetable($TimetableID);
			
			$academicYearID = $objTimetable->AcademicYearID;
			$yearTermID = $objTimetable->YearTermID;
			$numOfCycleDays = $objTimetable->CycleDays;
			
			### Get Timeslot Info
			$timeSlotArr = $objTimetable->Get_Timeslot_List();
			$numOfTimeSlot = count($timeSlotArr);
			
			### Get Lesson
			$roomAllocationArr = $objTimetable->Get_Room_Allocation_List();
			
			### Get all rooms
			$allRoomInfoAssoAry = $liblocation->Get_Building_Floor_Room_Name_Arr('', $includedRoomIdAry);
						
						
			### Calculate column width
			$tempTimeSlotWidth = 10;
			if ($numOfCycleDays > 0) {
				$colWidth = floor((100 - $tempTimeSlotWidth) / $numOfCycleDays);
			}
			else {
				$colWidth = 100;
			}
			$timeSlotWidth = 100 - ($colWidth * $numOfCycleDays);	// re-calculate the time slot column width
					
			
			$x = '';
			$x .= '<table class="common_table_list" id="ContentTable">'."\n";
				$x .= '<col align="left" width="'.$timeSlotWidth.'%"></col>'."\n";
				for ($i=0; $i<$numOfCycleDays; $i++) {
					$x .= '<col align="left" width="'.$colWidth.'"></col>'."\n";
				}
				
				### Table header
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
						for ($i=0; $i<$numOfCycleDays; $i++) {
							// Display 1,2,3...
							$thisDay = $i + 1;
							$thisDayDisplay = $Lang['SysMgr']['Timetable']['Day'].' '.$thisDay;
							
							$x .= '<th style="vertical-align:top">'."\n";
								$x .= '<span style="float:left">'.$thisDayDisplay.'</span>'."\n";
							$x .= '</th>'."\n";
						}
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				### Table content
				$x .= '<tbody>'."\n";
					// Loop time slot
					for ($i=0; $i<$numOfTimeSlot; $i++) {
						$_timeSlotID = $timeSlotArr[$i]['TimeSlotID'];
						$_timeSlotName = $timeSlotArr[$i]['TimeSlotName'];
						$_startTime = substr($timeSlotArr[$i]['StartTime'], 0, 5);
						$_endTime = substr($timeSlotArr[$i]['EndTime'], 0, 5);
						$_timeDisplay = $_startTime."-".$_endTime;
						
						if ($includedTimeSlotIdAry != '' && !in_array($_timeSlotID, (array)$includedTimeSlotIdAry)) {
							continue;
						}
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.$_timeDisplay.'<br />'.$_timeSlotName.'</td>'."\n";
							
							// Loop Days
							for ($j=0; $j<$numOfCycleDays; $j++) {
								$__day = $j + 1;
								$__roomAllocationArr = $roomAllocationArr[$_timeSlotID][$__day];
								
								// Get all rooms having lesson in this time slot of this day
								$__roomIdAry = Get_Array_By_Key($__roomAllocationArr, 'LocationID');
								
								// Loop all rooms to check if they have lesson in this time slot of this day
								$__freeRoomIdAry = array();
								foreach ((array)$allRoomInfoAssoAry as $___roomId => $___roomInfoAry) {
									if (in_array($___roomId, (array)$__roomIdAry)) {
										// room in-use in this time slot of this day => not free room
									}
									else {
										$__freeRoomIdAry[] = $___roomId;
									}
								}
								
								// display info of the free room
								$__numOfFreeRoom = count($__freeRoomIdAry);
								$__displayAry = array();
								for ($k=0; $k<$__numOfFreeRoom; $k++) {
									$___roomId = $__freeRoomIdAry[$k];
									$___roomNameEn = $allRoomInfoAssoAry[$___roomId]['RoomNameEng'];
									$___roomNameCh = $allRoomInfoAssoAry[$___roomId]['RoomNameChi']; 
									$__displayAry[] = Get_Lang_Selection($___roomNameCh, $___roomNameEn);
								}
								
								if (count($__displayAry) == 0) {
									$__display = '&nbsp;';
								}
								else {
									$__display = implode('<br />', $__displayAry);
								}
								$x .= '<td>'.$__display.'</td>'."\n";
							}
							
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
				
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		function Get_TimeSlot_Checkboxes($timetableId) {
			global $Lang, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$linterface = new interface_html();
			
			$objTimetable = new Timetable($timetableId);
			$timeSlotAry = $objTimetable->Get_Timeslot_List();
			$numOfTimeSlot = count($timeSlotAry);
			
			$SelectAllCheckBox = $linterface->Get_Checkbox($ID="timeSlotSelectAllChk", $Name='', $Value='', $isChecked=1, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('timeSlotIdAry[]', this.checked);");
			
			$x = '';
			$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $SelectAllCheckBox."\n";
						$x .= '<br />'."\n";
						for ($i=0; $i<$numOfTimeSlot; $i++) {
							$_timeSlotId = $timeSlotAry[$i]['TimeSlotID'];
							$_timeSlotName = $timeSlotAry[$i]['TimeSlotName'];
							$_startTime = substr($timeSlotAry[$i]['StartTime'], 0, 5);
							$_endTime = substr($timeSlotAry[$i]['EndTime'], 0, 5);
							$_display = $_timeSlotName.' ('.$_startTime.'-'.$_endTime.')';
							
							$thisID = 'timeSlotChk_'.$_timeSlotId;
							$thisName = 'timeSlotIdAry[]';
							$thisClass = 'timeSlotChk';
							$thisCheckbox = $linterface->Get_Checkbox($thisID, $thisName, $_timeSlotId, $isChecked, $thisClass, $_display, $Onclick="Uncheck_SelectAll('timeSlotSelectAllChk', this.checked);");
							$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCheckbox."<br />\n";
						}
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			return $x;
		}
		
		
		// copied from libebooking_ui.php		
		function Get_Mode_Toolbar($ModeArr)
		{
		    global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
		    
		    $numOfMode = count($ModeArr);
		    $Toolbar = '';
		    
		    $Toolbar .= '<span class="thumb_list">' . "\n";
		    for ($i = 0; $i < $numOfMode; $i ++) {
		        list ($page_name, $icon_file, $page_link, $is_current_mode) = $ModeArr[$i];
		        
		        if ($i > 0)
		            $Toolbar .= " | ";
		            
		            $Toolbar .= "<img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/" . $icon_file . "' align='absmiddle'>";
		            
		            if ($is_current_mode)
		                $Toolbar .= "<span>" . $page_name . "</span>";
		                else
		                    $Toolbar .= "<a href='$page_link'>" . $page_name . "</a>";
		    }
		    $Toolbar .= '</span>' . "\n";
		    
		    return $Toolbar;
		}
		
		
		function getEverydayTimetableFilterBar($mode='view')
		{
		    global $Lang, $PATH_WRT_ROOT;
		    
		    include_once("libinterface.php");
		    include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
		    
		    $libinterface = new interface_html();
		    
		    $selectedAcademicYearID = IntegerSafe($_POST['SelectedAcademicYearID']);
		    
		    $timetableListLabel = $mode == 'view' ? $Lang['SysMgr']['Timetable']['Timetable'] : $Lang['SysMgr']['Timetable']['SetTimetable'];
		    
		    # Acadermic Year Selection
		    $acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID");
		    
		    # Timetable Selection
		    $timetableSelection = $this->Get_Timetable_Selection("SelectedTimetableID", $selectedAcademicYearID, $YearTermID='', $SelectedTimetableID='', $OnChange='', $noFirst=0, $ParFirstTitle=$Lang['General']['PleaseSelect']);

		    ### Main Display
		    $table = '';
		    $table .= '<div id="SelectionTableDiv" style="float:left">'."\n";
		        $table .= '<table id="SelectionTable">'."\n";
		            $table .= '<tr>'."\n";
		            
            		    # Acadermic Year
		                $table .= '<td style="vertical-align:top">'.$Lang['General']['SchoolYear']." : ".$acadermicYearSelection.'</td>'."\n";
		    
            		    # Timetable
            		    $table .= '<td style="vertical-align:top">'."\n";
            		        $table .= '<div id="TimetableSelectionDiv">'.$timetableListLabel."\n";
                		    $table .= $timetableSelection;
                		    $table .= '</div>'."\n";
            		    $table .= '</td>'."\n";
		    
            		$table .= '</tr>'."\n";
                $table .= '</table>'."\n";
            $table .= '</div>'."\n";
            
		    return $table;
		}
		
		
		function getTimetableByTimestamp($ts, $timetableName, $showArchiveRecord, $skipSchoolEventDateAssoc, $isReadonly=true)
		{
		    global $image_path,$LAYOUT_SKIN,$Lang;
		    
		    $today = date('Y-m-d');
		    if ($ts == '') {
		        $thisDateString = '';
		        $weekDay = '';
		    }
		    else {
		        $thisDateString = date("Y-m-d",$ts);
		        $weekDay = date("w",$ts);
		    }
		    
		    $dispTimetableName = ($timetableName)? $timetableName : "&nbsp;";
		    
		    if($ts=="") {
		        $x = "<td class=\"non_bookable\" ><br></td>";
		    }
		    else {
		        if (isset($skipSchoolEventDateAssoc[$ts]) && $skipSchoolEventDateAssoc[$ts]) {
		            $classType = "non_bookable";      // grey
		        }
		        else {
		            $classType = "bookable";          // white
		        }
		        $Event = "<font color='#000000'>".date("j",$ts)."</font>";
		        
	            // Sunday & Saturday is not editable except it has been set default time table
		        //if ($showArchiveRecord || ( $classType == 'non_bookable') || ((($weekDay== 0) || ($weekDay == 6)) && $timetableName == '') || $isReadonly){
		        
		        // Sunday & Saturday is not editable
		        if ($showArchiveRecord || ( $classType == 'non_bookable') || ($weekDay== 0) || ($weekDay == 6) || $isReadonly || ($thisDateString < $today)){
                    $startLink = "";
                    $endLink = "";
                    $editable_class = "";
                    $timetable_class = '';
                }
                else {
//                    $startLink = "<a href=\"\" class=\"thickbox add_dim\" title='".$Lang['SysMgr']['Timetable']['Setting']['AssignEverydayTimetable']."' onClick=\"setEverydayTimetable('$thisDateString'); return false;\">";
                    $startLink = "<a href=\"\" class=\"timetableLink thickbox add_dim\" title='".$Lang['SysMgr']['Timetable']['Setting']['AssignEverydayTimetable']."' data-date=\"".$thisDateString."\">";
                    $endLink = "</a>";
                    $editable_class = "editable";
                    $timetable_class = 'timetableName';
                }
                
                ## no cycle period set
                $x  = "<td class=\"$classType $editable_class\" width=\"14%\" >$startLink";
                $x .= "<table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$classType\">\n";
                    $x .= "<tr>\n";
                        $x .= "<td class='$editable_class' align=\"center\" width=\"50%\"><div class=\"booking_calendar_date\">$Event</div></td>\n";
                        $x .= "<td class='$editable_class' align=\"right\" valign=\"top\"></td>\n";
                    $x .= "</tr>\n";
                    $x .= "<tr>\n";
                        $x .= "<td class=\"indexcalendarremark $editable_class $timetable_class\" align=\"left\" valign=\"top\">$dispTimetableName</td>\n";
                        $x .= "<td class=\"indexcalendarremark $editable_class\" align=\"left\" valign=\"top\" >&nbsp;</td>\n";
                    $x .= "</tr>\n";
                $x .= "</table>\n";
                $x .= $endLink;
                $x .= "</td>";

		    }
		    return $x;
		}
		
		
		function displayCalandarEverydayPreview($academicYearID, $month, $year, $showArchiveRecord, $timetableID='', $mode='view')
		{
		    global $Lang;
		    
		    include_once("libcal.php");
		    include_once("libcalevent.php");
		    include_once("libcalevent2007a.php");
		    
		    $lcal = new libcalevent2007();
		    $row = $lcal->returnCalendarArray($month, $year);     // timestamp(date) array of the month
		    $timetableArr = $this->Get_All_TimeTable($academicYearID);
		    $timetableNameAssoc = BuildMultiKeyAssoc($timetableArr,array('TimetableID'),array('TimetableName'),1);
		    $timetableDateAssoc = $this->getEverydayTimetableByMonth($year, $month);
		    $skipSchoolEventDateAssoc = $this->getSkipSchoolEventDateByYear($academicYearID);   // event of full year's

		    $isReadonly = ($mode == 'view') ? 1 : 0;
		    
		    $x = "";
		    $x .= "<div id=\"CalendarDiv\">\n";
		    # Month and Day Title
		    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#D2D2D2\">\n";
		        $x .= "<tr><td colspan=\"7\" align=\"center\" class=\"tabletop\">".$Lang['General']['month'][$month]." ".$year."</td></tr>\n";
		        $x .= "<tr>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" ><font color='red'>&nbsp;".$Lang['General']['DayType4'][0]."</font></td>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][1]."</td>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][2]."</td>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][3]."</td>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][4]."</td>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][5]."</td>\n";
                    $x .= "<td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][6]."</td>\n";
                $x .= "</tr>\n";
		    
		    # Show Dates
		    for($i=0,$iMax=count($row); $i<$iMax; $i++)
		    {
		        $x .= ($i % 7 == 0) ? "<tr>\n" : "";
		        
		        if(!empty($row[$i])) {
		            $date_string = date("Y-m-d",$row[$i]);
		        }
		            
		        $_timetableID = $timetableDateAssoc[$date_string];
		        $_timetableName = $timetableNameAssoc[$_timetableID];
		        if ($timetableID != '') {     // filter timetable
		            if ($timetableID != $_timetableID) {     // don't show other timetables if filtered timetable
		                $_timetableName = '';
		            }
		        }
		        
		        $x .= $this->getTimetableByTimestamp($row[$i],$_timetableName,$showArchiveRecord,$skipSchoolEventDateAssoc,$isReadonly);
	            $x .= ($i % 7 == 6) ? "</tr>\n" : "";
		    }
		    $x .= "</table>\n";
		    $x .= "</div>\n";
		    
		    return $x;
		    
		}
		
		
		function getEverydayTimeTableCalendar($academicYearID,  $timetableID='', $mode='view')
		{
		    include_once("libdb.php");
		    include_once("form_class_manage.php");
		    
		    global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		    
		    $lfcm = new form_class_manage();
		    
		    if($academicYearID == ""){
		        $academicYearID = Get_Current_Academic_Year_ID();
		    }
		    
		    if($academicYearID != "")
		    {
		        ## Check the selected school year id >= current school year
		        ## step 1: get all the academic year which is current year or year in future
		        $sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE NOW() <= TermEnd";
		        $result = $this->returnVector($sql);
		        ## step 2: check the selected school year id is in the return array
		        if(in_array($academicYearID,$result))
		        {
		            $showArchiveRecord = 0;
		        }else{
		            $showArchiveRecord = 1;
		        }
		        
		        $arrResult = $lfcm->Get_Academic_Year_List();
		        if(sizeof($arrResult)>0){
		            for($i=0; $i<sizeof($arrResult); $i++){
		                list($AcademicYearID, $YearNameEN, $YearNameB5, $Sequence, $CurrentSchoolYear, $StartDate, $EndDate) = $arrResult[$i];
		                
		                if($AcademicYearID==$academicYearID){
		                    $TargetStartDate = $StartDate;
		                    $TargetEndDate = $EndDate;
		                    $StartYear = substr($StartDate,0,4);
		                    $StartMonth = substr($StartDate,5,2);
		                    $EndYear = substr($EndDate,0,4);
		                    $EndMonth = substr($EndDate,5,2);
		                }
		            }
		        }
		        
		        $month = intval($StartMonth);
		        $year = intval($StartYear); // get Current Year
		        $end_month = intval($EndMonth);
		        $end_year = intval($EndYear);
		        
		        // calculate how many months does the school year have
		        $FinalEndDate = date("Y-m-d",mktime(0, 0, 0, ($EndMonth + 1), 0, $EndYear));
		        $FinalStartDate = date("Y-m-d",mktime(0, 0, 0, ($StartMonth), 1, $StartYear));
		        
		        $NumOfMonth = getMonthDifferenceBetweenDates($FinalStartDate, $FinalEndDate) + 1;
		        
		        if($NumOfMonth < 1)
		        {
		            $NumOfMonth = 1;
		        }else{
		            $NumOfMonth = ceil($NumOfMonth);
		        }
		        
		        for($i=1; $i<=$NumOfMonth; $i++)
		        {
		            $arrDate[$i]['Month'] = $month;
		            $arrDate[$i]['Year'] = $year;
		            $month = $month + 1;
		            if($month > 12){
		                $month = $month - 12;
		                $year = $year + 1;
		            }
		        }
		        
		        for($i=1; $i<=$NumOfMonth; $i++){
		            $month = $arrDate[$i]['Month'];
		            $year = $arrDate[$i]['Year'];
		            ${calendarTable.$i} = $this->displayCalandarEverydayPreview($academicYearID, $month, $year, $showArchiveRecord, $timetableID, $mode);
		        }
		        
		        $x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		        for($i=1; $i<=$NumOfMonth; $i++){
		            if($i%4 == 1) {
		                $x .= "<tr valign=top>";
		            }
		            $x .= "<td style='width:25%'>${"calendarTable$i"}</td>";
		                
		            if($i%4 == 0) {
		                $x .= "</tr>";
		            }
		        }
		    
		        $x .= "</table>";
		    }
		
		    return $x;
		}
		
	}

} // End of directives
?>