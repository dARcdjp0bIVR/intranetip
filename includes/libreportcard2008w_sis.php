<?php

if ($intranet_session_language == "b5") {
	include_once($intranet_root."/lang/reportcard2008_lang.en.php");
	$EngReportCard = $eReportCard;
	include_once($intranet_root."/lang/reportcard2008_lang.$intranet_session_language.php");
	$ChiReportCard = $eReportCard;
} else {
	include_once($intranet_root."/lang/reportcard2008_lang.b5.php");
	$ChiReportCard = $eReportCard;
	include_once($intranet_root."/lang/reportcard2008_lang.$intranet_session_language.php");
	$EngReportCard = $eReportCard;
}

include_once($intranet_root."/includes/libfilesystem.php");

class libreportcard2008w extends libreportcard 
{
	function getSelectSemester($tags,$selected="")
	{
	         $semester_data = getSemesters();
	         $x = "<SELECT $tags>\n";
	         for ($i=0; $i<sizeof($semester_data); $i++)
	         {
	              $line = split("::",$semester_data[$i]);
	              list ($name,$current) = $line;
	              $selected_str = (($selected==$name || ($selected==""&&$current==1))? "SELECTED":"");
	              $x .= "<OPTION value='$i' $selected_str>$name</OPTION>\n";
	         }
	         $x .= "</SELECT>\n";
	         return $x;
	}


	/*
	Action of insert ReportCard Basic Information (Step 1)
	ref: /home/admin/reportcard2008/settings/reportcard_templates/new_update.php
	*/
	function InsertReportTemplateBasicInfo($data)
	{	
		//$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE";
 		$id = "";
 		
 		if(!empty($data))
 		{
	 		foreach ($data as $key => $value) {
				$k[] = $key;
				$v[] = "'".$value."'";
			}
			$k[] = "DateInput";
			$v[] = "NOW()";
			
			$sql = "INSERT INTO $table (";
	 		$sql .= implode(",", $k);
			$sql .= ") VALUES (";
			$sql .= implode(",", $v);
			$sql .= ")";
			
			$result = $this->db_db_query($sql);
			$id = $this->db_insert_id();
			
		}
		return $id;
		
	}
	
	function InsertReportTemplateColumn($data)
	{	
		$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
 		
 		$sql = "INSERT INTO $table VALUES ";
 		
 		foreach($data as $key => $ary)
 		{
	 		// Check semester duplicate or not
	 		$dupsql = "SELECT ReportColumnID FROM $table where ReportID=". $ary['ReportID'] ." and SemesterNum=".$ary['SemesterNum'];
	 		$dupresult = $this->returnVector($dupsql);
			if($dupresult[0])	continue; 
			
			$ColumnTitle = $ary['ColumnTitle'] ? "'". $ary['ColumnTitle'] ."'" : "''";
			$SemesterNum = $ary['SemesterNum'] ? "'". $ary['SemesterNum'] ."'" : 0;
			$DefaultWeight = $ary['DefaultWeight'] ? "'". $ary['DefaultWeight'] ."'" : "NULL";
			$IsDetails = $ary['IsDetails'] ? "'". $ary['IsDetails'] ."'" : "NULL";
			
			$insertItem[] = "(
				NULL, 
				'". $ary['ReportID'] ."',
				". $ColumnTitle .",
				". $DefaultWeight .",
				'". $ary['DisplayOrder'] ."',
				'". $ary['ShowPosition'] ."',
				'". $ary['PositionRange'] ."',
				". $SemesterNum .",
				". $IsDetails .",
				NOW(),
				NULL)
			";
 		}
 		
 		$id="";
 		if (isset($insertItem) && sizeof($insertItem) > 0) {
			$sql .= implode(",", $insertItem);
			$result = $this->db_db_query($sql);
			$id = $this->db_insert_id();
		}
 		return $id;
	}
	
	/*
	Action of update ReportCard Basic Information (Step 1)
	ref: /home/admin/reportcard2008/settings/reportcard_templates/new_update.php
	*/
	function UpdateReportTemplateBasicInfo($ReportID, $data)
	{	
		$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE";
 		
 		foreach ($data as $key => $value) {
	 		$temp[] .= $key ." = ". "'".$value."'";
		}

		$sql = "UPDATE $table set ";
 		$sql .= implode(",", $temp);
		$sql .= " WHERE ReportID=$ReportID";
		$result = $this->db_db_query($sql);
		return $result;
		
	}
	
	function RemoveReportTemplate($ReportID)
	{	
		$this->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportID', $ReportID);
		$this->DELETE_REPORT_TEMPLATE_COLUMN('ReportID', $ReportID);
 		$table = $this->DBName.".RC_REPORT_TEMPLATE";
 		$sql = "DELETE FROM $table WHERE ReportID=$ReportID";
		return $this->db_db_query($sql);
	}
	
	# DELETE a report template subject weight specify by column name and its value
	#. e.g. $column = "ReportColumnID", "ReportID"
	function DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT($column, $value, $other='') {
		$success = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
		$sql = "DELETE FROM $table WHERE $column = '$value'";
		$sql .= ($other) ? " AND ".$other : "";
		
		$success = $this->db_db_query($sql);
		return success;
	}
	
	function returnReportTemplateCalculation($ReportID)
	{
		$semAry = $this->returnReportTemplateBasicInfo($ReportID);
		$sem = $semAry['Semester'];
		$t = $this->LOAD_SETTING('Calculation');
		if($sem =="F")				// Whole Year
			return $t[OrderFullYear];	
		else						// Term 
			return $t[OrderTerm];
	}
	
	function returnSubjectIDwOrder($ClassLevelID)
	{
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		
		$SubjectIDAry = array();
		if(!empty($SubjectArray))
		{
			foreach($SubjectArray as $key => $val)
			{
				$SubjectIDAry[] = $key;
				if(sizeof($val)>1)
				{
					foreach($val as $key1 => $val1)
					{
						if($key1)	$SubjectIDAry[] = $key1;
					}
				}
			}
		}
		
		return $SubjectIDAry;
		
	}
	
	function returnReportColoumnTitle($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "SELECT ReportColumnID, ColumnTitle, SemesterNum FROM $table where ReportID = $ReportID ORDER BY DisplayOrder";
		$result = $this->returnArray($sql);
		foreach($result as $key => $val)
		{
			list($ReportColumnID, $ColumnTitle, $SemesterNum) = $val;
			$x[$ReportColumnID] = $ColumnTitle ? $ColumnTitle : $this->returnSemesters($SemesterNum);
		}
		
		return $x;
	}
	
	function returnReportColoumnTitleWithOrder($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "SELECT DisplayOrder, ColumnTitle, SemesterNum FROM $table where ReportID = $ReportID ORDER BY DisplayOrder";
		$result = $this->returnArray($sql);
		
		foreach($result as $key => $val)
		{
			list($DisplayOrder, $ColumnTitle, $SemesterNum) = $val;
			$x[$DisplayOrder] = $ColumnTitle ? $ColumnTitle : $this->returnSemesters($SemesterNum);
		}
		
		return $x;
	}
	
	
	
	function UpdateReportTemplateColumn($data, $where)
	{	
		$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
 		
 		foreach ($data as $key => $value) {
	 		$temp[] .= $key ." = ". "'".$value."'";
		}

		$sql = "UPDATE $table set ";
 		$sql .= implode(",", $temp);
		$sql .= " WHERE $where ";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function InsertReportTemplateSubjectWeight($ReportID, $data)
	{
		$result = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
		
		$sql = "INSERT INTO $table VALUES ";
		
		foreach ($data as $key => $value) 
		{
			$SubjID = $value['SubjectID'] ? "'". $value['SubjectID'] ."'" : "NULL";
			$ReportColumnID = $value['ReportColumnID'] ? "'". $value['ReportColumnID'] ."'" : "NULL";
			$IsDisplay = $value['IsDisplay'] ? "'". $value['IsDisplay'] ."'" : "NULL";
			
			$insertItem[] = "(NULL, '$ReportID', ". $ReportColumnID .", ". $SubjID .", ". $IsDisplay .", '". $value['Weight'] ."' ,NOW(), NULL)";
		}
		
		if (isset($insertItem) && sizeof($insertItem) > 0) {
			$sql .= implode(",", $insertItem);
			$result = $this->db_db_query($sql);
		}
		
		return $result;
	}
	
	function returnReportTemplateColumnData($ReportID, $other="")
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "	
			SELECT 
				ReportColumnID,
				ColumnTitle,
				DefaultWeight,
				SemesterNum,
				IsDetails,
				ShowPosition,
				PositionRange,
				DisplayOrder		
			FROM 
				$table 
			WHERE
				ReportID = $ReportID
		";
		if($other) $sql .= " and $other ";
		$sql .="		
			ORDER BY
				DisplayOrder
		";
		$x = $this->returnArray($sql);
		return $x;
	}
	
	function returnReportTemplateSubjectWeightData($ReportID, $other_condition="")
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
		
		$sql = "	
			SELECT 
				ReportColumnID,
				SubjectID,
				Weight,
				IsDisplay
			FROM 
				$table 
			WHERE
				ReportID = $ReportID
		";
		if($other_condition)	$sql .= " AND " . $other_condition;
		$x = $this->returnArray($sql);
		return $x;
	}
	
	function UpdateReportTemplateColumnOrder($ReportID, $curPos, $newPos)
	{
		/* Insert After */
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		// find the curPos ID
		$sql = "select ReportColumnID from $table where ReportID=$ReportID and DisplayOrder=$curPos";
		$x = $this->returnArray($sql);
		$curColID = $x[0][0];

		$sql = "select ReportColumnID, DisplayOrder from $table where ReportID=$ReportID and ReportColumnID<>$curColID order by DisplayOrder";
		$x = $this->returnArray($sql);
		for($i=0;$i<sizeof($x);$i++)
		{
			if($x[$i]['DisplayOrder']<=$newPos)	continue;
			if($x[$i]['DisplayOrder']>$newPos)	
			{
				$sql1 = "update $table set DisplayOrder = ". ($x[$i]['DisplayOrder']+1) ." where ReportColumnID=".$x[$i]['ReportColumnID'];
				$this->db_db_query($sql1);
			}
		}
		$sql1 = "update $table set DisplayOrder = $newPos+1 where ReportColumnID=".$curColID;
		$this->db_db_query($sql1);
	}
	
	function ChangeReportTemplateColumnOrder($ReportID, $ColID, $Order)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		// find curPos and newPos
		$sql = "select ReportColumnID, DisplayOrder from $table where ReportID=$ReportID order by DisplayOrder";
		$x = $this->returnArray($sql);
		
		for($i=0;$i<sizeof($x);$i++)
		{
			if($ColID==$x[$i]['ReportColumnID'])	
			{	
				$data[0]['ReportColumnID'] = $x[$i]['ReportColumnID'];
				$data[0]['DisplayOrder'] = $x[$i]['DisplayOrder'];

				$data[1]['ReportColumnID'] = $x[($Order==1?$i+1:$i-1)]['ReportColumnID'];
				$data[1]['DisplayOrder'] = $x[($Order==1?$i+1:$i-1)]['DisplayOrder'];
				break;
			}
		}
		$this->SwapReportTemplateColumnOrder($ReportID, $data);
	}
	
	function SwapReportTemplateColumnOrder($ReportID, $data)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "update $table set DisplayOrder=". $data[1]['DisplayOrder'] ." where ReportColumnID=".$data[0]['ReportColumnID'];
		$this->db_db_query($sql);
		
		$sql = "update $table set DisplayOrder=". $data[0]['DisplayOrder'] ." where ReportColumnID=".$data[1]['ReportColumnID'];
		$this->db_db_query($sql);
	}	
	
	
	
	function isReportTemplateComplete($ReportID)
	{
 		$table_TEMPLATE					 = $this->DBName.".RC_REPORT_TEMPLATE";
 		$table_TEMPLATE_COLUMN			 = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
 		$table_TEMPLATE_SUBJECT_WEIGHT	 = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";

		//check LineHeight
		$sql = "select ClassLevelID, LineHeight from $table_TEMPLATE where ReportID=$ReportID";
		$x = $this->returnArray($sql);
		if($x[0]['LineHeight']=="")	return false;
		$ClassLevelID = $x[0]['ClassLevelID'];
		
		//check column exists or not
		$sql = "select ReportColumnID from $table_TEMPLATE_COLUMN where ReportID=$ReportID";
		$x = $this->returnArray($sql);
		if(empty($x))		return false;
			
		$SubjectArray = $this->returnSubjectIDwOrder($ClassLevelID);
		  
		//check weight
		for($i=0;$i<sizeof($x);$i++)
		{
			$ColID = $x[$i][0];
			foreach($SubjectArray as $key => $SubjID)
			{
				$sql = "select count(*) from $table_TEMPLATE_SUBJECT_WEIGHT where ReportColumnID=$ColID and SubjectID=$SubjID";
				$x = $this->returnArray($sql);
				if($x[0][0]==0)	return false;
			}			 
		}	
		
		//check ration 
		$cal = $this->returnReportTemplateCalculation($ReportID);
		if($cal==1)
			$sql = "select count(*) from $table_TEMPLATE_SUBJECT_WEIGHT where ReportID=$ReportID and ReportColumnID is null and SubjectID is not null";
		else
			$sql = "select count(*) from $table_TEMPLATE_SUBJECT_WEIGHT where ReportID=$ReportID and SubjectID is null and ReportColumnID is not null";			
		$x = $this->returnArray($sql);
		if($x[0][0]==0)	return false;
		
		return true;
	}
	
	function ReportTemplateFormCopy($frForm='', $toForm, $toTerm='', $frReportID='')
	{
		$table_TEMPLATE = $this->DBName.".RC_REPORT_TEMPLATE";
		$copyResult	= array();
		
		if($frReportID)		// single report copy
		{
			
			$sql = "select * from $table_TEMPLATE where ReportID = $frReportID";
			$template = $this->returnArray($sql);
			
			$copyResult[$frReportID] = $this->ReportTemplateTermCopy($toForm, $toTerm, $template[0], $frReportID);
			
		}
		else				// all reports copy
		{
			//retrieve RC_REPORT_TEMPLATE (frForm)
			$sql = "select * from $table_TEMPLATE where ClassLevelID = $frForm order by ReportTitle";
			$template = $this->returnArray($sql);
			
			for($i=0;$i<sizeof($template);$i++)
			{
				$frReportID = $template[$i][ReportID];
				$copyResult[$frReportID] = $this->ReportTemplateTermCopy($toForm, $toTerm, $template[$i], '');
			}
		}
		
		return $copyResult;
	}
	
	function ReportTemplateTermCopy($toForm, $toTerm='', $template, $frReportID='')
	{
		$table_TEMPLATE = $this->DBName.".RC_REPORT_TEMPLATE";
		
		if($frReportID)		// single report copy
		{
			$Semester = $template[Semester] == "F"? $template[Semester] : $toTerm;
		}
		else				// all reports copy
		{
			$Semester = $template[Semester];
		}
		
		$frReportID = $template[ReportID];
		$SubjMatched = 1;
		
		// Check template already exists
		if($Semester != "F")
		{
			$dupStr = "select ReportID from $table_TEMPLATE where ClassLevelID = $toForm and Semester = '". $Semester ."'";
			$dup = $this->returnArray($dupStr);
			if($dup)
			{
				return -1;
				continue;
			}
		}
			
			// copy RC_REPORT_TEMPLATE
			$data = array();
			$data[ReportTitle] 					= $template[ReportTitle];
			$data[Semester] 					= $Semester;
			$data[Description] 					= addslashes($template[Description]);
			$data[ClassLevelID]					= $toForm;
			$data[HeaderHeight]					= $template[HeaderHeight];
			$data[Footer] 						= addslashes($template[Footer]);
			$data[LineHeight] 					= $template[LineHeight];
			$data[AllowClassTeacherComment]		= $template[AllowClassTeacherComment];
			$data[CommentLengthClassTeacher] 	= $template[CommentLengthClassTeacher];
			$data[AllowSubjectTeacherComment]	= $template[AllowSubjectTeacherComment];
			$data[CommentLengthSubjectTeacher] 	= $template[CommentLengthSubjectTeacher];
			$data[ShowOverallPositionClass] 	= $template[ShowOverallPositionClass];
			$data[ShowOverallPositionForm] 		= $template[ShowOverallPositionForm];
			$data[OverallPositionRangeClass] 	= $template[OverallPositionRangeClass];
			$data[OverallPositionRangeForm]		= $template[OverallPositionRangeForm];
			$data[HideNotEnrolled] 				= $template[HideNotEnrolled];
			$data[DisplaySettings] 				= $template[DisplaySettings];
			$data[PercentageOnColumnWeight] 	= $template[PercentageOnColumnWeight];
			$ReportID = $this->InsertReportTemplateBasicInfo($data);
						
			//copy RC_REPORT_TEMPLATE_COLUMN
			$template_col_data = $this->returnReportTemplateColumnData($frReportID);
			foreach($template_col_data as $key=>$coldata)
			{
				$data = array();
				$data[0][ReportID] 		= $ReportID;
				$data[0][ColumnTitle] 	= $coldata[ColumnTitle];
				$data[0][DefaultWeight]	= $coldata[DefaultWeight];
				$data[0][DisplayOrder] 	= $coldata[DisplayOrder];
				$data[0][ShowPosition] 	= $coldata[ShowPosition];
				$data[0][PositionRange] = $coldata[PositionRange];
				$data[0][SemesterNum] 	= $coldata[SemesterNum];
				$data[0][IsDetails] 	= $coldata[IsDetails];
				$frReportColumnID		= $coldata[ReportColumnID];	
				$ReportColumnID = $this->InsertReportTemplateColumn($data);
								
				//copy RC_REPORT_TEMPLATE_SUBJECT_WEIGHT
				$SubjectArray = $this->returnSubjectIDwOrder($toForm);
				$SubjMatched = empty($SubjectArray) ? 0 : $SubjMatched;
				$sw = 0;
				$data = array();
				foreach($SubjectArray as $key=>$SubjID)
				{
					$other_condition = "ReportColumnID=$frReportColumnID and SubjectID=$SubjID";
					$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition);
					
					if(empty($weight_data_ary))	
					{
						$SubjMatched = 0;
						continue;	
					}
										
					$weight_data = $weight_data_ary[0];
					
					$data[$sw][ReportID] 		= $ReportID;
					$data[$sw][ReportColumnID] 	= $ReportColumnID;
					$data[$sw][SubjectID] 		= $SubjID;
					$data[$sw][IsDisplay] 		= $weight_data[IsDisplay];
					$data[$sw][Weight] 			= $weight_data[Weight];
					$sw++;
				}
				
				$this->InsertReportTemplateSubjectWeight($ReportID, $data);
				
				// Column data of !SubjID & ColID
				$data = array();
				$other_condition = "ReportColumnID = $frReportColumnID and SubjectID is NULL";
				$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition, 0);
				
				if(!empty($weight_data_ary))	
				{
					$weight_data = $weight_data_ary[0];
					$data = array();
					$data[0][ReportID] 			= $ReportID;
					$data[0][ReportColumnID] 	= $ReportColumnID;
					$data[0][IsDisplay] 		= $weight_data[IsDisplay];
					$data[0][Weight] 			= $weight_data[Weight];
					$this->InsertReportTemplateSubjectWeight($ReportID, $data);
				}
			}
			
			// Column data of SubjID & !ColID
			$data = array();
			$sw = 0;
			$SubjectArray = $this->returnSubjectIDwOrder($toForm);
			foreach($SubjectArray as $key=>$SubjID)
			{
				$other_condition = "ReportColumnID is NULL and SubjectID=$SubjID";
				$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition, 0);
				
				if(empty($weight_data_ary))	
				{
					$SubjMatched = 0;
					continue;	
				}
				
				$weight_data = $weight_data_ary[0];
				
				$data[$sw][ReportID] 		= $ReportID;
				$data[$sw][SubjectID] 		= $SubjID;
				$data[$sw][IsDisplay] 		= $weight_data[IsDisplay];
				$data[$sw][Weight] 			= $weight_data[Weight];
				$sw++;
			}
			$this->InsertReportTemplateSubjectWeight($ReportID, $data);
	
			// !SubjID & !ColID
			$other_condition = "ReportColumnID is NULL and SubjectID is NULL";
			$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition);
			$data = array();
			$data[0][ReportID] 		= $ReportID;
			$data[0][IsDisplay] 	= $weight_data[IsDisplay];
			$data[0][Weight] 		= $weight_data[Weight];
			$this->InsertReportTemplateSubjectWeight($ReportID, $data);	
			
			return $SubjMatched;
	}
	
	
/*	
	function checkCanGenernateReport($ReportID)
	{
		global $eReportCard;
		
		$errAry = array();
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		# Check Submission date and Verification date
		$SubmissionEnd = substr($BasicInfo['MarksheetSubmissionEnd'],0,10);
		if($SubmissionEnd=="" or $SubmissionEnd>=date("Y-m-d"))				$errAry[] = $eReportCard['SubmissionEndDateNotReached'];
		$VerificationEnd = $BasicInfo['MarksheetVerificationEnd'];
		if($VerificationEnd!="" and $VerificationEnd>=date("Y-m-d"))		$errAry[] = $eReportCard['VerificationEndDateNotReached'];
		
		# Other variables
		$ClassLevelID = $BasicInfo['ClassLevelID'];
		$ClassAry 	= $this->GET_CLASSES_BY_FORM($ClassLevelID);
		$SubjectAry = $this->returnSubjectwOrderNoL($ClassLevelID);
		
		foreach($ClassAry as $key => $data)
		{
			$ClassID = $data[ClassID];
			
			//if($AllowClassTeacherComment)
			//{
				# Check is completed (RC_CLASS_COMMENT_PROGRESS)
				$C = $this->isClassCommentComplete($ReportID, $ClassID);
				if(!$C)		$errAry[] = $eReportCard['ClassCommentNotCompleted'];
			//}
						
			# Check is completed (RC_MARKSHEET_SUBMISSION_PROGRESS)
			foreach($SubjectAry as $SubjectID => $SubjectName)
			{
				$C = $this->isMSComplete($ReportID, $ClassID, $SubjectID);
				if(!$C)	$errAry[] = $eReportCard['MSNotCompleted'];
			}
		}		
		
		return array_unique($errAry);
	}
*/
/*	
	function returnReportTemplateBasicInfo($ReportID='', $others='')
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		if($ReportID)	$con[] = "ReportID = $ReportID ";
		if($others)		$con[] = $others;
		$cons = empty($con) ? "" : implode("and", $con);
		$sql = "	
			SELECT 
				ReportID,
				ReportTitle,
				ClassLevelID,
				Semester,
				Description,
				PercentageOnColumnWeight,
				HeaderHeight,
				Footer,
				LineHeight,
				AllowClassTeacherComment,
				AllowSubjectTeacherComment,
				DisplaySettings,
				LastGenerated,
				MarksheetSubmissionEnd,
				MarksheetVerificationEnd,
				ShowSubjectFullMark,
				ShowSubjectOverall
			FROM 
				$table 
			WHERE
				$cons
		";
		
		$x = $this->returnArray($sql);
		return $x[0];
	}
*/
	
	function returnSubjectTeacherClass($UserID, $ClassLevelID)
	{
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";

		$con = $ClassLevelID != "" ? " and b.ClassLevelID=$ClassLevelID" : "";
		$from = $ClassLevelID != "" ? " inner join $table as c on (c.SubjectID=a.SubjectID and c.ClassLevelID=$ClassLevelID)" : "";
		
		$sql ="
			select 
				distinct(a.ClassID),
				b.ClassName,
				b.ClassLevelID
			from 
				RC_SUBJECT_TEACHER as a
				$from
				left join INTRANET_CLASS as b on (a.ClassID=b.ClassID)
			where
				a.UserID=$UserID
				$con
			ORDER BY 
				b.ClassLevelID, b.ClassID
	 	";
		return $this->returnArray($sql);
	}
	
	function returnSunjectTeacherForm($UserID)
	{
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		$sql ="
			select 
				distinct(b.ClassLevelID),
				c.LevelName
			from 
				RC_SUBJECT_TEACHER as a
				left join INTRANET_CLASS as b on (a.ClassID=b.ClassID)
				left join INTRANET_CLASSLEVEL as c on (c.ClassLevelID = b.ClassLevelID)
				inner join $table as d on (d.SubjectID=a.SubjectID and d.ClassLevelID=c.ClassLevelID)
			where
				a.UserID=$UserID
			ORDER BY 
				b.ClassLevelID
	 	";
		return $this->returnArray($sql);
	}
	
	function returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='')
	{
		global $eclass_db;
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		if($TeacherID)
		{
			$TeahcerSubjects = array();
			$TeahcerSubjectAry = $this->returnSunjectTeacherSubject($TeacherID, $ClassLevelID);
			foreach($TeahcerSubjectAry as $key=>$val)
				$TeahcerSubjects[] = $val['SubjectID'];
		}
		
		$SubjectField = ($ParForSelection==1) ? "CONCAT(a.EN_DES, ' (', a.CH_DES, ')')" : "CONCAT(a.EN_DES, '<br />', a.CH_DES)";
		$CmpSubjectField = ($ParForSelection==1) ? "CONCAT(b.EN_DES, ' (', b.CH_DES, ')')" : "CONCAT(b.EN_DES, '<br />', b.CH_DES)";
		$sql = "SELECT DISTINCT
					a.RecordID,
					$SubjectField,
					b.RecordID,
					$CmpSubjectField
				FROM
					{$eclass_db}.ASSESSMENT_SUBJECT AS a
					LEFT JOIN {$eclass_db}.ASSESSMENT_SUBJECT AS b ON a.CODEID = b.CODEID
					LEFT JOIN $table as c on c.SubjectID=b.RecordID 
				WHERE
					a.EN_SNAME IS NOT NULL
					AND a.CMP_CODEID IS NULL
					and c.ClassLevelID=$ClassLevelID 
				ORDER BY
					c.DisplayOrder
			";
		$SubjectArr = $this->returnArray($sql);	
		
		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($SubjectID, $SubjectName, $CmpSubjectID, $CmpSubjectName) = $SubjectArr[$i];
			
			if($TeacherID)
			{
				if(in_array($SubjectID, $TeahcerSubjects)===false)	continue;
			}
			
			if($SubjectID==$CmpSubjectID)
			{
				$CmpSubjectID = 0;
			}
			else
			{
				$SubjectName = $CmpSubjectName;
			}
			$ReturnArr[$SubjectID][$CmpSubjectID] = $SubjectName;
			ksort($ReturnArr[$SubjectID]);
		}
		return $ReturnArr;
	}
	
	function returnSunjectTeacherSubject($UserID, $ClassLevelID)
	{
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		$sql ="
			select 
				d.SubjectID
			from 
				RC_SUBJECT_TEACHER as a
				left join INTRANET_CLASS as b on (a.ClassID=b.ClassID)
				left join INTRANET_CLASSLEVEL as c on (c.ClassLevelID = b.ClassLevelID)
				inner join $table as d on (d.SubjectID=a.SubjectID and d.ClassLevelID=c.ClassLevelID)
			where
				a.UserID=$UserID
				and c.ClassLevelID = $ClassLevelID
			ORDER BY 
				b.ClassLevelID
	 	";
		return $this->returnArray($sql);
	}
	
	
	/************************************************/
	/* For Generate Report Layout - START			*/
	/************************************************/
	/*
	function getReportHeader($ReportID)
	{
		global $eReportCard;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  $ReportSetting['ReportTitle'];
			
			# get school badge
			$SchoolLogo = GET_SCHOOL_BADGE();
				
			# get school name
			$SchoolName = GET_SCHOOL_NAME();	
			
			$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
			if ($HeaderHeight != -1) $TempLogo = "&nbsp;";
	
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle."</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	*/
	
	/*
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		$StudentInfoTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			$LineHeight = $ReportSetting['LineHeight'];
			
			# retrieve required variables
			$defaultVal		= "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] 		= $lu->EnglishName;
				$data['ClassNo'] 	= $lu->ClassNumber;
				$data['Class'] 		= $lu->ClassName;
				$data['StudentNo'] 	= $lu->ClassNumber;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] 	= $lu->Gender;

				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				$data['ClassTeacher'] = implode(", ", $CTeacher);
			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					//if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber")
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						
						$Title = str_replace("<br />", " ", $Title);
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top' height='{$LineHeight}'>".$Title." : ". ($data[$SettingID] ? $data[$SettingID] : $defaultVal )."</td>";
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						}
						$count++;
					}
				}
				$StudentInfoTable .= "</table>";
			}
		}
		
		return $StudentInfoTable;
	}
	*/
	
	/*
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum) = $ColHeaderAry;
		
		//$MarksAry = 
		
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		# Show Subject Full Mark?
		if($ShowSubjectFullMark)
		{
			$FullMarkCol = $this->returnSubjectFullMark($ClassLevelID);
		}
		# Show Subject Overall?
		if($ShowSubjectOverall)
		{
			$SubjectOverall = $this->returnSubjectOverall($ReportID, $StudentID);
		}
		# Allow Subject Teacher Comment?
		if($AllowSubjectTeacherComment)
		{
			$SubjectTeacherCommentCol = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		}
		# Display MS Table Footer?
		if($eRCTemplateSetting['MSTableFooter']['Display'])
		{
			$MSTableFooter = $this->genMSTableFooter($ReportID, $MarksAry);
			
		}
		# retrieve Marks Array
		$MarksDisplayAry = $this->genMSTableMarks($ReportID, $MarksAry);
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			$css_border_top = ($isFirst or $isSub)? "" : "border_top";
			
			$DetailsTable .= "<tr>";
			
			# Subject 
			$DetailsTable .= $SubjectCol[$i];;
			
			# Full Mark
			if($ShowSubjectFullMark)	$DetailsTable .= "<td height='{$LineHeight}' class='tabletext border_left {$css_border_top}' align='center'>". $FullMarkCol[$thisSubjectID] ."</td>";
			
			# Marks
			$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
			
			# Subject Overall 
			if($ShowSubjectOverall)
			{
				$SOverall = $StudentID ? $SubjectOverall[$thisSubjectID] : "xxx";
				$DetailsTable .= "<td height='{$LineHeight}' class='tabletext border_left {$css_border_top}' align='center'>". $SOverall ."</td>";
			}
		
			# Subject Teacher Comment
			if($AllowSubjectTeacherComment)
			{
				if($StudentID)
					$SubjectTeacherComment = $SubjectTeacherCommentCol[$thisSubjectID] ? $SubjectTeacherCommentCol[$thisSubjectID] : "&nbsp;";
				else
					$SubjectTeacherComment = "XXX";
				$DetailsTable .= "<td height='{$LineHeight}' class='tabletext border_left {$css_border_top}' align='center'>". $SubjectTeacherComment ."</td>";
			}
			
			$DetailsTable .= "</tr>";
			$isFirst = 0;
		}
		
		# MS Table Footer
		if($eRCTemplateSetting['MSTableFooter']['Display'])
		{
			$DetailsTable .= $MSTableFooter;
		}
		
		//$this->getOtherInfoConfig($UploadType);
		
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	*/
	
	/*
	function genMSTableFooter($ReportID, $MarksAry=array())
	{
		global $eReportCard, $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		$SemID = $ReportSetting['Semester'];
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
 		$ReportType = $SemID == "F" ? "W" : "T";	
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark']; 		
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment']; 		
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall']; 		
		
		$Fields = $eRCTemplateSetting['MSTableFooter']['Fields'];
		# check Subject Column Number
		$SubjectCol = $eRCTemplateSetting['ColumnHeader']['Subject'];
// 		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
// 		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
// 		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$x = "";
		foreach($Fields as $k => $field)
		{
			$x .= "<tr>";
			
			# Subject Col	
			$x .= "<td class='tabletext border_top' height='". $LineHeight ."'><b>". $eReportCard['Template'][$field] . "</b></td>";
			if(sizeof($SubjectCol)>1)
				$x .= "<td class='tabletext border_top' height='". $LineHeight ."'><b>". $eReportCard['Template'][$field."2"] . "</b></td>";
			
			# if GrandTotal
			if($field=="GrandTotal")
			{
				if($ShowSubjectFullMark)
					$x .= "<td class='tabletext border_left border_top' height='". $LineHeight ."' align='center'>". $this->returnGrandTotal($ClassLevelID) . "</td>";
					
				
			}
			
			# if AverageMark
			if($field=="AverageMark")
			{
				if($ShowSubjectFullMark)
					$x U= "<td class='tabletext border_left border_top' height='". $LineHeight ."' align='center'>&nbsp;</td>";
					
				
			}
			
			$t = "";
			$n = 0;
			//MarksAry	
			if($ReportType=="T")	# Temrs Report Type
			{
				# Retrieve Invloved Assesment
				$Col�mnData = $this->returnReportTemplateColumnData($ReportID);
				
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$border_left = $i==0 ? "border_left" : "";
					
					$t .= "<td class='{$border_left} border_top tabletext' align='center'>xxx</td>";
					$n++;
					# check display position
					if($ColumnData[$i]['ShowPosition']>0)
					{
						$t .= "<td class='tabletext border_top' align='center'>(xxx)</td>";
						$n++;
					}
				}
			}
			else					# Whole Year Report Type
			{
				# Retrieve Invloved Temrs
				$ColumnData = $this->returnReportTemplateColumnData($ReportID);
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
						
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						
						$ColumnID = array();
						$ColumnTitle = array();
						foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
						for($j=0;$j<sizeof($ColumnTitle);$j++)
						{
							$border_left = $j==0 ? "border_left" : "";
							$t .= "<td class='{$border_left} border_top tabletext' align='center'>xxx</td>";
							$n++;
						}
					}
					else					# Retrieve Terms Overall marks
					{
						$t .= "<td class='border_left border_top tabletext' align='center'>xxx</td>";
						$n++;
					}
				}
			}
			
			if($field=="GrandTotal" || $field=="AverageMark")
				$x .= $t;
			else
				$x .= "<td colspan='$n' class='border_left border_top tabletext' align='right'>xxx</td>";
			
			if($ShowSubjectOverall)
			{
				$x .= "<td class='border_left border_top tabletext' align='center'>xxx</td>";
			}
			
			if($AllowSubjectTeacherComment)
			{
				$x .= "<td class='border_left border_top tabletext' align='center'>&nbsp;</td>";
			}
			$x .= "</tr>";
		}
		
		return $x;
		
	}
	*/
	
	/*
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		
		$n = 0;

		#########################################################
		############## Marks START
		$row2 = "";
		if($ReportType=="T")	# Temrs Report Type
		{
			# Retrieve Invloved Assesment
			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
			$ColumnID = array();
			$ColumnTitle = array();
			foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
			$e = 0;
			for($i=0;$i<sizeof($ColumnTitle);$i++)
			{
				$border_left = $i==0 ? "border_left" : "";
				$row2 .= "<td class='report_formfieldtitle {$border_left} reportcard_text' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitle[$i] . "</td>";
				$n++;
				$e++;
				
				# check display position
				$cd = $this->returnReportTemplateColumnData($ReportID, "ReportColumnID=".$ColumnID[$i]);
				if($cd[0]['ShowPosition']>0)
				{
					$row2 .= "<td class='report_formfieldtitle reportcard_text' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>(". $eReportCard['Template']['Ranking'] . ")</td>";
					$n++;
					$e++;
				}
			}
			$row1 = "<td colspan='{$e}' class='report_formfieldtitle border_left small_title' align='center'>". $this->returnSemesters($SemID) ."</td>";
		}
		else					# Whole Year Report Type
		{
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 			
			for($i=0;$i<sizeof($ColumnData);$i++)
			{
				$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
				$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
				if($isDetails==1)
				{
					$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
					$thisReportID = $thisReport['ReportID'];
					$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
					for($j=0;$j<sizeof($ColumnTitle);$j++)
					{
						$border_left = $j==0 ? "border_left" : "";
						$row2 .= "<td class='report_formfieldtitle {$border_left} reportcard_text' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $ColumnTitle[$j] . "</td>";
						$n++;
					}
					$colspan = "colspan='". sizeof($ColumnTitle) ."'";
					$Rowspan = "";
				}
				else
				{
					$colspan = "";
					$Rowspan = "rowspan='2'";
				}
				$row1 .= "<td {$Rowspan} {$colspan} class='report_formfieldtitle border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."%'>". $SemName ."</td>";
			}
		}
		############## Marks END
		#########################################################
		
		$Rowspan = $row2 ? "rowspan='2'" : "";
		$x = "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = $eReportCard['Template']['SubjectEng'];
			$SubjectChn = $eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan} class='report_formfieldtitle small_title' height='". $eRCTemplateSetting['ColumnHeaderHeight'] ."' width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."'>". $SubjectTitle . "</td>";
			$n++;
		}
		
		# Show Subject Full Mark
		if($ShowSubjectFullMark)
		{
			$x .= "<td {$Rowspan} class='report_formfieldtitle border_left small_title' align='center' width='". $eRCTemplateSetting['ColumnWidth']['FullMark'] ."' height='". $eRCTemplateSetting['ColumnHeaderHeight'] ."' >". $eReportCard['SchemesFullMark'] . "</td>";
			$n++;
		}
		
		# Marks
		$x .= $row1;
		
		# Subject Overall 
		if($ShowSubjectOverall)
		{
			$x .= "<td {$Rowspan} width='". $eRCTemplateSetting['ColumnWidth']['Overall'] ."' class='report_formfieldtitle border_left small_title' align='center'>". $eReportCard['Template']['SubjectOverall'] ."</td>";
			$n++;
		}
		
		# Subject Teacher Comment
		if($AllowSubjectTeacherComment)
		{
			$x .= "<td {$Rowspan} width='". $eRCTemplateSetting['ColumnWidth']['SubjectTeacherComment'] ."' class='report_formfieldtitle border_left small_title' align='center'>". $eReportCard['Template']['SubjectTeacherComment'] ."</td>";
			$n++;
		}
		
		$x .= "</tr>";
		if($row2)	$x .= "<tr>". $row2 ."</tr>";
		return array($x, $n);
	}
	*/
	
	/*
	function genMSTableMarks($ReportID, $MarksAry=array())
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";		
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$n = 0;
		$x = array();
		$isFirst = 1;
		if($ReportType=="T")	# Temrs Report Type
		{
			# Retrieve Invloved Assesment
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$border_left = $i==0 ? "border_left" : "";
					$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					
					$t .= "<td class='{$border_left} {$css_border_top} tabletext' align='center'>xxx</td>";
					# check display position
					if($ColumnData[$i]['ShowPosition']>0)
					{
						$t .= "<td class='tabletext {$css_border_top}' align='center'>(xxx)</td>";
					}
					
				}
				$isFirst = 0;
				$x[$SubjectID] = $t;
			}
		}
		else					# Whole Year Report Type
		{
			# Retrieve Invloved Temrs
			$ColumnData = $this->returnReportTemplateColumnData($ReportID);
			
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				$t = "";
				$isSub = 0;
				if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
				
				for($i=0;$i<sizeof($ColumnData);$i++)
				{
					$css_border_top = ($isFirst or $isSub)? "" : "border_top";
					
					$isDetails = $ColumnData[$i]['IsDetails'];	# 1 - Show All Assessments, 2 - Show Term Total only
					if($isDetails==1)		# Retrieve assesments' marks
					{
						$thisReport = $this->returnReportTemplateBasicInfo("","Semester=".$ColumnData[$i]['SemesterNum'] ." and ClassLevelID=".$ClassLevelID);
						$thisReportID = $thisReport['ReportID'];
						$ColumnTitleAry = $this->returnReportColoumnTitle($thisReportID);
						$ColumnID = array();
						$ColumnTitle = array();
						foreach ($ColumnTitleAry as $ColumnID[] => $ColumnTitle[]);
						for($j=0;$j<sizeof($ColumnTitle);$j++)
						{
							$border_left = $j==0 ? "border_left" : "";
							$t .= "<td class='{$border_left} {$css_border_top} tabletext' align='center'>xxx</td>";
						}
					}
					else					# Retrieve Terms Overall marks
					{
						$t .= "<td class='border_left {$css_border_top} tabletext' align='center'>xxx</td>";
					}
				}
				$isFirst = 0;
				$x[$SubjectID] = $t;
			}
		}
		
		return $x;
	}
	*/
	
	function returnSubjectTeacherComment($ReportID, $StudentID='')
	{
		$table 	= $this->DBName.".RC_MARKSHEET_COMMENT";
		
		$x = array();
		if($StudentID)
		{
			$sql = "select SubjectID, Comment from $table where ReportID=$ReportID and StudentID=$StudentID";
			$result = $this->returnArray($sql);
			
			foreach($result as $k=>$data)
				$x[$data['SubjectID']] = $data['Comment'];
		}
		
		return $x;
		
	}
	
	function returnSubjectOverall($ReportID, $StudentID='')
	{
		$table 	= $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		
		$x = array();
		if($StudentID)
		{
			/*
			$sql = "select SubjectID, Comment from $table where ReportID=$ReportID and StudentID=$StudentID";
			$result = $this->returnArray($sql);
			
			foreach($result as $k=>$data)
				$x[$data['SubjectID']] = $data['Comment'];
				*/
		}
		
		return $x;
		
	}
	
	/*
	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$LineHeight = $ReportSetting['LineHeight'];
		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		$x = array(); 
		$isFirst = 1;
 		foreach($SubjectArray as $SubjectID=>$Ary)
 		{
	 		foreach($Ary as $SubSubjectID=>$Subjs)
	 		{
		 		$t = "";
		 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
		 		if($SubSubjectID==0)		# Main Subject
		 		{
			 		$SubSubjectID=$SubjectID;
			 		$Prefix = "";
		 		}
		 		
		 		$css_border_top = ($isFirst or $Prefix)? "" : "border_top";
		 		foreach($SubjectDisplay as $k=>$v)
		 		{
			 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
	 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
	 				
	 				$v = str_replace("SubjectEng", $SubjectEng, $v);
	 				$v = str_replace("SubjectChn", $SubjectChn, $v);
	 				
			 		$t	.= "<td class='tabletext {$css_border_top}' height='{$LineHeight}' valign='middle'>". $Prefix. $v."</td>";
		 		}
				$x[] = $t;
				$isFirst = 0;
			}
	 	}
 		return $x;
	}
	*/
	
	
	/*
	function getSignatureTable()
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];

		$SignatureTable = "";
		
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='report_border'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	*/
	
	/*
	function getFooter($ReportID)
	{
		global $eReportCard;
		$FooterRow = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Footer = $ReportSetting['Footer'];
			$FooterRow = "<tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n";
		}
		
		return $FooterRow;
	}
	*/
	/************************************************/
	/* For Generate Report Layout - END				*/
	/************************************************/
	
	# input: Lang = "EN", "CH"
	function GET_SUBJECT_NAME_LANG($SubjectID, $Lang='') 
	{
		global $eclass_db, $intranet_session_language;
		
		if(!$Lang)
			$Lang = ($intranet_session_language=="en") ? "EN" : "CH";
		
		$select = $Lang."_DES";
		$sql = "SELECT
					{$select}
				FROM
					{$eclass_db}.ASSESSMENT_SUBJECT
				WHERE
					RecordID = '$SubjectID'
				";
		$row = $this->returnVector($sql);
		return $row[0];
	}
	
	function returnSubjectFullMark($ClassLevelID, $withSub=1)
	{
		$table_RC_SUBJECT_FORM_GRADING 	= $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$table_RC_GRADING_SCHEME 		= $this->DBName.".RC_GRADING_SCHEME";
		$table_RC_GRADING_SCHEME_RANGE	= $this->DBName.".RC_GRADING_SCHEME_RANGE";
		
		if($withSub)
			$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		else
			$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		$x = array();

		if((sizeof($SubjectArray) > 0) && is_array($SubjectArray)){
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# retrieve SchemeID & ScaleDisplay
				$sql = "SELECT SchemeID, ScaleDisplay FROM $table_RC_SUBJECT_FORM_GRADING WHERE SubjectID=$SubjectID and ClassLevelID=$ClassLevelID";
				$result = $this->returnArray($sql);
				$SchemeID= $result[0]['SchemeID'];
				$ScaleDisplay = $result[0]['ScaleDisplay'];
				
				# retrieve SchemeType & FullMark
				$sql = "SELECT SchemeType, FullMark FROM $table_RC_GRADING_SCHEME WHERE SchemeID=$SchemeID";
				$result = $this->returnArray($sql);
				$SchemeType = $result[0]['SchemeType'];
				$FullMark = $result[0]['FullMark'];
				
				if($SchemeType=="H")
				{
					if($ScaleDisplay=="M")		
					{
						$x[$SubjectID] = $FullMark;
					}
					else
					{
						$sql = "SELECT Grade FROM $table_RC_GRADING_SCHEME_RANGE WHERE SchemeID=$SchemeID order by GradingSchemeRangeID";
						$result = $this->returnVector($sql);
						$x[$SubjectID] = $result[0];
					}
				}
				else
				{
					$x[$SubjectID] = "--";
				}
			}
		}else{
			$x = "--";
		}
		return $x;
		
	}
	
	function getCusTemplate()
	{
		global $PATH_WRT_ROOT, $intranet_root;
		
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateNo = $eRCTemplateSetting['CusTemplate'];
		//$TemplatePath = $intranet_root."/file/reportcard2008/templates/template_". ($TemplateNo ? $TemplateNo : "default") .".php";
		$TemplatePath = $PATH_WRT_ROOT."includes/libreportcard2008_".$TemplateNo.".php";
		if(file_exists($TemplatePath))
		{
			include_once($TemplatePath);
			$eRCtemplate = new libreportcardSIS();
			return $eRCtemplate;
		}
		else
		{
			return $this;
		}
	}
	
	function getTemplateCSS()
	{
		global $PATH_WRT_ROOT;
		
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateCSS = $eRCTemplateSetting['CSS'];
		$TemplateCSSPath = $PATH_WRT_ROOT."/file/reportcard2008/templates/". $TemplateCSS;
		
		if(file_exists($TemplateCSSPath))
			return $TemplateCSSPath;
		else
			return false;
	}
	
	function returnClassLevel($ClassLevelID)
	{
		$sql = "select LevelName from INTRANET_CLASSLEVEL where ClassLevelID = $ClassLevelID";
		$result = $this->returnVector($sql);
		
		return $result[0];
	}
	

	function GenReportList($Semester='', $ClassLevelID='')
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $button_view, $eReportCard, $button_print, $button_promotion;
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		
		$con = array();
		if($Semester)		$con[] = "Semester = '". $Semester ."'";
//		if($ClassLevelID!=-1)	$con[] = "ClassLevelID = '". $ClassLevelID ."'";
		$conStr = implode(" and ", $con);
		$conStr = $conStr ? " where " . $conStr : "";
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "
			select 
				ReportID,
				ReportTitle,
				Semester,
				ClassLevelID,
				Issued,
				LastPrinted,
				LastAdjusted,
				LastGenerated,
				LastMarksheetInput
		 	from 
		 		$table 
		 		$conStr
	 		order by 
	 			ClassLevelID
 			";
		$x = $this->returnArray($sql);
		
		$xi = 0;
		foreach($x as $key => $data)
		{
			$css = "tablerow" . (($xi%2)+1);
			$ReportID 		= $data[ReportID];
			$ClassLevelID	= $data[ClassLevelID];
			$report_title 	= str_replace(":_:", "<br>", $data[ReportTitle]);
			$lv_name 		= $lclass->getLevelName($data[ClassLevelID]);
			$issue_date 	= $data[Issued] == "" ? "--" : $data[Issued];
			$generate_date 	= ($data[LastGenerated]=="0000-00-00 00:00:00" || $data[LastGenerated] == "") ? "&nbsp;": $eReportCard['LastGenerate']." : ". $data[LastGenerated];
			
			$print_date 	= ($data[LastPrinted]=="0000-00-00 00:00:00" || $data[LastPrinted] == "") ? "&nbsp;": $eReportCard['LastPrint']." : ". $data[LastPrinted];
			$print_btn		= ($data[LastGenerated]!="") ? $linterface->GET_ACTION_BTN($button_print, "button", "clickPrintMenu($ReportID)") : "--";
			$modify_alert 	= ($data[LastMarksheetInput]>$data[LastGenerated] && $data[LastGenerated]!="") ? "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"systemmsg\"><tr><td>". $eReportCard['MarkModifiedOn'] ."<br>". $data[LastMarksheetInput] ."</td></tr></table>" : "";
			
			$ReportSetting	= $this->returnReportTemplateBasicInfo($ReportID);
			$SemID = $ReportSetting['Semester'];
 			$ReportType = $SemID == "F" ? "W" : "T";
 			$ClassLevel 	= $this->returnClassLevel($ClassLevelID);
 			$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
 			
			if((substr($ClassLevel, 0, 1)=="P" && $ReportType=="W") or (substr($ClassLevel, 0, 1)=="S" && sizeof($ColoumnTitle)==4))
				$promote_btn	= ($data[LastGenerated]!="") ? $linterface->GET_ACTION_BTN($button_promotion, "button", "clickPromotion($ReportID, $ClassLevelID)") : "--";
			else
				$promote_btn = "";
				
			
			$CanGen 		= $this->checkCanGenernateReport($ReportID);
			$CanGenStr		= implode("<br>", $CanGen);
			$gen_btn		= empty($CanGen) ? $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "clickGenReport($ReportID)") : $CanGenStr;
			
			//$adjust_date 	= ($data[LastAdjusted]=="0000-00-00 00:00:00" || $data[LastAdjusted] == "") ? "": $eReportCard['LastAdjust']." : ". $data[LastGenerated];
			//$adj_btn		= (empty($CanGen) && $data[LastGenerated] != "") ? $linterface->GET_ACTION_BTN("Adjust", "button", "clickAdjust($ReportID)") : "--";
			//$adj_date 		= ($data[LastAdjusted]=="") ? "&nbsp;": $eReportCard['LastAdjust']." : ". $data[LastAdjusted];
			
			$row .= "<tr class=\"resubtabletop\">";
			$row .= "<td class=\"$css tabletext\"><strong>". $report_title ."</strong></td>";
			$row .= "<td width=\"80\" class=\"$css tabletext\">". $lv_name ."</td>";
			$row .= "<td class=\"$css\"><a href=\"javascript:newWindow('../../settings/reportcard_templates/preview.php?ReportID=$ReportID', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">{$button_view}</a></td>";
            $row .= "<td class=\"$css tabletext\" nowrap>". $issue_date ."</td>";
			$row .= "<td class=\"$css tabletext\">". $gen_btn ."<br></td>";
			//if (!(getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com"))  
			$row .= "<td class=\"$css\">". $promote_btn ."</td>";
		//	$row .= "<td class=\"$css\">". $adj_btn ."</td>";
			$row .= "<td class=\"$css\">". $print_btn ."<br></td>";
			$row .= "</tr>
                  <tr class=\"$css\">
                    <td align=\"right\" class=\"tabletext\"></td>
                    <td class=\"tabletext\">&nbsp;</td>
                    <td class=\"tabletext\">&nbsp;</td>
                    <td class=\"tabletext \">&nbsp;</td>
                    <td class=\"tabletextremark \">". $generate_date ."<br>". $modify_alert ."</td>";
			//if (!(getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com"))  
            //$row .= "<td class=\"tabletext \"><span class=\"tabletextremark\">". $adjust_date. "<br>". $adj_list ."</span></td>";
            $row .= "<td class=\"tabletext\">&nbsp;</td>";
//            $row .= "<td class=\"tabletext\"><span class=\"tabletextremark\">". $adj_date ."</span></td>";
			$row .= "
                    <td class=\"tabletext \"><span class=\"tabletextremark\">". $print_date ."</span></td>
                    </tr>
			";
			
			$xi++;
		}
		
		return $row;
	}	
	

/*	
	function isMSComplete($ReportID, $ClassID, $SubjectID)
	{
		
		$isAllColumnWeightZero = $this->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $SubjectID);
		if($isAllColumnWeightZero)	return -1;
		
		$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
		$sql = "select IsCompleted from $table where ReportID=$ReportID and ClassID=$ClassID and SubjectID=$SubjectID";
		$x = $this->returnVector($sql);
		return $x[0]=="" ? -1 : $x[0];
	}
*/
	
	function GET_STUDENT_BY_CLASSLEVEL($ParReportID, $ParClassLevelID, $ParClassID="", $ParStudentID="", $ParVerify=0)
	{
		//$RoundDP = $this->GET_DECIMAL_POINT($ParReportID);
		if($ParStudentID!="") {
			$cond = " AND a.UserID IN ($ParStudentID)";
		}
		else if($ParClassID!="") {
			$cond = " AND b.ClassID = '$ParClassID'";
		}
		$ClassNumField = getClassNumberField("a.");
		$sql = "SELECT
						a.UserID
					FROM
						INTRANET_USER as a
						LEFT JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName
						LEFT JOIN INTRANET_CLASSLEVEL as c ON b.ClassLevelID = c.ClassLevelID
					WHERE
						b.ClassLevelID IN ($ParClassLevelID)
						AND a.RecordType = 2
						AND a.RecordStatus = 1
						$cond
					ORDER BY
						a.ClassName,
						$ClassNumField
					";
		$row_student = $this->returnArray($sql);
		return $row_student;
	}
	
	function getMarks($ReportID, $StudentID='', $cons='')
	{
		$x = array();
		
		if($StudentID)
		{
			## Score
			$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
			$sql = "select * from $table where ReportID=$ReportID and StudentID=$StudentID $cons";
			$result = $this->returnArray($sql);
			
			foreach($result as $key=>$data)
			{
 				$x[$data[SubjectID]][$data[ReportColumnID]][Mark] = $data[Mark];
 				$x[$data[SubjectID]][$data[ReportColumnID]][Grade] = $data[Grade];
 				$x[$data[SubjectID]][$data[ReportColumnID]][RawMark] = $data[RawMark];
			}

		}

		return $x;
	}
	
	function getAllStudentMarks($ReportID, $StudentID='', $cons='', $ExamSubjectOnly=0)
	{
		$x = array();
		
		if($StudentID)
		{
			$studentCond = " AND StudentID=$StudentID ";
		}
		
		if ($ExamSubjectOnly)
		{
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
		}
		
		## Score
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "select * from $table where ReportID=$ReportID $studentCond $cons";
		$result = $this->returnArray($sql);
		
		foreach($result as $key=>$data)
		{
			$thisStudentID = $data['StudentID'];
			$thisSubjectID = $data['SubjectID'];
			$thisReportColumnID = $data['ReportColumnID'];
			
			if ($ExamSubjectOnly)
			{
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID);
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				
				if ($ScaleDisplay != 'M')
					continue;
			}
			
			$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'] = $data['Mark'];
			$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'] = $data['Grade'];
		}

		return $x;
	}
	
	function checkSpCase($ReportID, $SubjectID, $thisMark, $tempGrade='')
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
 		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
 		$SchemeID = $SubjectFormGradingSettings[SchemeID];
 		$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];

		$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
		$SchemeType = $SchemeInfo['SchemeType'];
		
		if($ScaleDisplay=="M" && !$tempGrade)	return array($thisMark, 1);
		switch($SchemeType)
		{
			case "H":
				$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
				# check is the tempGrade in the SpecialCaseArrH
				if(in_array($tempGrade, $SpecialCaseArrH)===true)
					return array($this->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($tempGrade), 0);
				else
					return array($thisMark, 1);
				break;
			case "PF":
				$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
				# check is the tempGrade in the SpecialCaseArrH
				if(in_array($tempGrade, $SpecialCaseArrPF))
					return array($this->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($tempGrade), 0);
				else
					return array($thisMark, 1);
				break;
		}
	}
	
	function returnMarkNature($ClassLevelID, $SubjectID, $thisMark='')
	{
		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
		$SchemeID = $SubjectFormGradingSettings[SchemeID];	
		$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
		
		$NatrueType = array("P"=>"Pass", "F"=>"Fail", "D"=>"Distinction");
		
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		switch($ScaleDisplay)
		{
			case "G":
				$sql = "select Nature from $table where SchemeID=$SchemeID and Grade='$thisMark'";
				$result = $this->returnVector($sql);
				return $NatrueType[$result[0]];
				break;
				
			case "M":
				$sql = "select Nature, LowerLimit from $table where SchemeID=$SchemeID order by LowerLimit desc";
				$result = $this->returnArray($sql);
				
				foreach($result as $key=>$data)
				{
					list($Nature, $LowerLimit) = $data;
					if($thisMark >= $LowerLimit)	return $NatrueType[$Nature];
				}
				break;	
		}
	}
	
	function returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColID)
	{
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$sql = "select FullMark from $table where ReportID=$ReportID and SubjectID=$SubjectID and StudentID=$StudentID  and ReportColumnID=$ColID";
		return $this->returnVector($sql);
		
	}

	function GET_CLASSES_BY_FORM($ParClassLevelID="", $ClassID="")
	{
		$return = array();
		$sql = "SELECT 
					ClassID, ClassName 
				FROM 
					INTRANET_CLASS 
				WHERE 
					RecordStatus = 1
				".(($ParClassLevelID != "") ? " AND ClassLevelID = ".$ParClassLevelID : "");
		if($ClassID)
			$sql .= " and ClassID=$ClassID";
			
		$return = $this->returnArray($sql, 2);

		return $return;
	}
	
	function retrieveLastAdjustDateTime($ReportID, $StudentID='')
	{
		$x = array();
		
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "select StudentID, DateInput, DateModified from $table where ReportID=$ReportID";
		if($StudentID)	$sql .= " and StudentID=$StudentID";
		$result = $this->returnArray($sql);
		
		foreach($result as $key=>$data)
			$x[$data['StudentID']] = ($data['DateInput'] != $data['DateModified']) ? $data['DateModified'] : $x[$data['StudentID']];
		return $x;
	}
	
	
	function UPDATE_REPORT_RESULT_SCORE($ReportID, $StudentID, $ColID=0, $SubjectID, $Mark='', $Grade='')
	{
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
	
		$sql = "UPDATE $table set Mark='$Mark', Grade='$Grade', DateModified=NOW() WHERE ReportID=$ReportID and StudentID=$StudentID and SubjectID=$SubjectID and ReportColumnID=$ColID";
		$result = $this->db_db_query($sql);
		return $result;

	}
	
	function returnSubjectFullMark_MarkOnly($ClassLevelID, $withSub=1)
	{
		$table_RC_SUBJECT_FORM_GRADING 	= $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$table_RC_GRADING_SCHEME 		= $this->DBName.".RC_GRADING_SCHEME";
		$table_RC_GRADING_SCHEME_RANGE	= $this->DBName.".RC_GRADING_SCHEME_RANGE";
		
		if($withSub)
			$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		else
			$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		$x = array();
		foreach($SubjectArray as $SubjectID => $SubjectName)
		{
			# retrieve SchemeID & ScaleDisplay
			$sql = "SELECT SchemeID, ScaleDisplay FROM $table_RC_SUBJECT_FORM_GRADING WHERE SubjectID=$SubjectID and ClassLevelID=$ClassLevelID";
			$result = $this->returnArray($sql);
			$SchemeID= $result[0]['SchemeID'];
			$ScaleDisplay = $result[0]['ScaleDisplay'];
			
			# retrieve SchemeType & FullMark
			$sql = "SELECT SchemeType, FullMark FROM $table_RC_GRADING_SCHEME WHERE SchemeID=$SchemeID";
			$result = $this->returnArray($sql);
			$SchemeType = $result[0]['SchemeType'];
			$FullMark = $result[0]['FullMark'];
			
			if($SchemeType=="H")
			{
				$x[$SubjectID] = $FullMark;
			}
			else
			{
				$x[$SubjectID] = "--";
			}
		}
		return $x;
		
	}
	
	/**************************************************************
	 * Moved to libreportcard2008.php
	 **************************************************************
	function ReturnTextwithStyle($text, $SettingCategory='Highlight', $style='')
	{
		$setting = $this->LOAD_SETTING($SettingCategory, $style);

		//Format of Setting:  Color|bold|underline|italic|LeftSymbol|RightSymbol
		list($c, $b, $u, $i, $l, $r) = explode("|",$setting[$style]);
		
		return "<span style='color=$c; font-weight=$b; text-decoration=$u; font-style=$i'>".$l.$text.$r."</span>";
	}
	
	function returnSemesters($SemID='-1')
	{
		$sem_ary = getSemesters();
		foreach($sem_ary as $key => $val)
		{
 			list($sem_name, $cur_sem) = split("::", $val);
			$x[$key] = $sem_name;
			if($SemID==$key)	return $sem_name;
		}
		
		return $x;
	}
	
	### Display the ReportCard Teamplates list 
	### ref: /home/admin/reportcard2008/settings/reportcard_templates/index.php
	function ReportTemplateList()
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $button_preview, $button_remove, $button_copy, $eReportCard;
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		
		$lclass = new libclass();
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$sql = "	
			SELECT 
				a.ReportID, 
				b.LevelName,
				a.ReportTitle, 
				a.DateModified,
				a.ClassLevelID
			FROM 
				$table as a
				left join INTRANET_CLASSLEVEL as b on a.ClassLevelID = b.ClassLevelID
			ORDER BY
				b.LevelName, a.ReportTitle
		";
		$result = $this->returnArray($sql);
			
		$data = array();	
		for($i=0;$i<sizeof($result);$i++)
		{
			$data[$result[$i]['ClassLevelID']][$result[$i]['ReportID']]['ReportTitle'] = str_replace(":_:", "<br>",$result[$i]['ReportTitle']);
			$data[$result[$i]['ClassLevelID']][$result[$i]['ReportID']]['DateModified'] = $result[$i]['DateModified'];
		}
		
		// Form Looping
		foreach($data as $ClassLevelID => $value1)
		{
			
			$row .= "
				<tr class=\"resubtabletop\">
					<td width=\"80\" class=\"tabletext\"><strong>". $lclass->getLevelName($ClassLevelID) ."</strong></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td id=\"formTD{$ClassLevelID}\"><a href=\"javascript:copyForm(". $ClassLevelID .");\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/btn_copy.gif\" border=\"0\" alt=\"{$button_copy}\" title=\"{$button_copy}\"></a> </td>
				</tr>
			";
					
			// Report Template Looping
			$rowi = 0;
			foreach($value1 as $ReportID => $value2)
			{
				$ReportTitle = $value2['ReportTitle'];
				$DateModified = $value2['DateModified'] ? $value2['DateModified'] : "---";
				$remark = $this->isReportTemplateComplete($ReportID) ? "": $eReportCard['SettingsNotCompleted'];
				
				$row .= "
					<tr class=\"tablerow". ($rowi%2==1?1:2) ."\"> 
						<td align=\"right\" class=\"tabletext\"><a href=\"javascript:newWindow('preview.php?ReportID=$ReportID', '10');\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" alt=\"{$button_preview}\" title=\"{$button_preview}\"></a></td>
						<td class=\"tabletext\"><a href=\"new.php?ReportID={$ReportID}\" class=\"tablelink\">{$ReportTitle}</a></td>
						<td class=\"tabletext\">{$DateModified}</td>
						<td class=\"tabletext status_alert\">". $remark ."</td>
						<td class=\"tabletext\" id=\"formTermTD{$ReportID}\">
							<a href=\"javascript:copyTerm({$ReportID});\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"{$button_copy}\" title=\"{$button_copy}\"></a> 
							<a href=\"javascript:cfmDelete({$ReportID});\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"{$button_remove}\" title=\"{$button_remove}\"></a>
						</td>
					</tr>
				";
				$rowi++;
			}
		}	
		return $row;
	}
	
	*/
	
	
}

?>