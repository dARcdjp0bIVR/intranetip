<?php

class ESF_ui{
	function Get_Div_Open($ID="", $OtherMember="") {
		if ($ID=="") $x = '<div';
		else $x = '<div id="'.$ID.'" name="'.$ID.'"';
		
		$x .= ' '.$OtherMember.' >';
		
		return $x;
	}
	
	function Get_Div_Close() {
		return '</div>';
	}
	
	function Get_Div($ID="", $OtherMember="") {
		if ($ID=="") $x = '<div';
		else $x = '<div id="'.$ID.'"';
		
		$x .= ' '.$OtherMember.' /></span>';
		
		return $x;
	}
	
	function Get_Span_Open($ID="", $OtherMember="") {
		if ($ID=="") $x = '<span';
		else $x = '<span id="'.$ID.'" name="'.$ID.'"';
		
		$x .= ' '.$OtherMember.' >';
		
		return $x;
	}
	
	function Get_Span_Close() {
		return "</span>";
	}
	
	function Get_Label_Open($OtherMember="") {
		$x = '<label';
		$x .= ' '.$OtherMember.' >';
		
		return $x;
	}
	
	function Get_Label_Close() {
		return "</label>";
	}
	
	function Get_Textarea_Open($Name="", $OtherMember="") {
		$x = '<textarea name="'.$Name.'" id="'.$Name.'" '.$OtherMember.' >';
		
		return $x;
	}
	
	function Get_Textarea_Close() {
		return "</textarea>";
	}
	
	function Get_Input_Button($Name="", $ID="", $Value="", $OtherMember="") {
		$x = '<input 
						type="button" 
						name="'.$Name.'" 
						id="'.$ID.'" 
						value="'.$Value.'" 
						'.$OtherMember.' 
					/>';
		
		return $x;
	}
	
	function Get_Reset_Button($Name="", $ID="", $Value="", $OtherMember="") {
		$x = '<input 
						type="reset" 
						name="'.$Name.'" 
						id="'.$ID.'" 
						value="'.$Value.'" 
						'.$OtherMember.' 
					/>';
		
		return $x;
	}
		
	function Get_Image($Src="", $Width="", $Height="", $OtherMember="") {
		if ($Width != "")
		{
			$OtherMember .= ' width="'.$Width.'" ';
		}
		if ($Height != "")
		{
			$OtherMember .= ' height="'.$Height.'" ';
		}		
		$x = '<img src="'.$Src.'" '.$OtherMember.' />';
		
		return $x;
	}
	
	function Get_Input_Text($Name="", $Value="", $OtherMember="") {
		$x = '<input 
						type="text" 
						name="'.$Name.'" 
						id="'.$Name.'" 
						value="'.$Value.'" 
						'.$OtherMember.' 
					/>';
		
		return $x;
	}
	
	function Get_HyperLink_Open($Href="", $OtherMember="") {
		$x = '<a href="'.$Href.'" '.$OtherMember.' >';
		
		return $x;
	}
	
	function Get_HyperLink_Close() {
		return '</a>';
	}
	
	function Get_List_Open($OtherMember="") {
		return '<ul '.$OtherMember.' >';
	}
	
	function Get_List_Close() {
		return '</ul>';
	}
	
	function Get_List_Member($Content="" ,$OtherMember="") {
		return '<li '.$OtherMember.' >'.$Content.'</li>';
	}
	
	function Get_Form_Open($Name="form1",$Method="POST",$Action="",$OtherMember="") {
		$x = '<form 
						name="'.$Name.'" 
						id="'.$Name.'" 
						method="'.$Method.'" 
						action="'.$Action.'" 
						'.$OtherMember.' >';
		
		return $x;
	}
	
	function Get_Form_Close() {
		return '</form>';
	}
	
	function Get_Input_Hidden($Name="",$ID="",$Value="",$OtherMember="") {
		$x = '<input type="hidden" name="'.$Name.'" id="'.$ID.'" value="'.$Value.'" '.$OtherMember.' />';
		
		return $x;
	}
	
	function Get_Input_Submit($Name="",$ID="",$Value="",$OtherMember="") {
		$x = '<input type="submit" name="'.$Name.'" id="'.$ID.'" value="'.$Value.'" '.$OtherMember.' />';
		
		return $x;
	}
	
	function Get_Input_Select($Name="",$ID="",$KeyValues=array(),$DefaultValue="",$OtherMember="") {
		$x = '<select name="'.$Name.'" id="'.$ID.'" '.$OtherMember.' />';
		for ($i=0; $i< sizeof($KeyValues); $i++) {
			$Selected = (($KeyValues[$i][1] == $DefaultValue) && (strlen($KeyValues[$i][1]) == strlen($DefaultValue)))? 'Selected="Selected"':'';
			$x .= '<option value="'.$KeyValues[$i][1].'" '.$Selected.'/>'.$KeyValues[$i][0].'</option>';
		}
		$x .= '</select>'; 
		return $x;
	}
	
	function Get_Input_Select_With_Label($Name="",$ID="",$KeyValues=array(),$DefaultValue="",$OtherMember="") {
		$x = '<select name="'.$Name.'" id="'.$ID.'" '.$OtherMember.' />';
		for ($i=0; $i< sizeof($KeyValues); $i++) {
			if ($KeyValues[$i][2] == 'LabelGroupStart') {
				$x .= '<optgroup label="'.$KeyValues[$i][0].'">';
				continue;
			}
			
			if ($KeyValues[$i][2] == 'LabelGroupEnd') {
				$x .= '</optgroup>';
				continue;
			}
			
			$Selected = ($KeyValues[$i][1] == $DefaultValue)? 'Selected="Selected"':'';
			
			$x .= '<option value="'.$KeyValues[$i][1].'" '.$Selected.'/>'.$KeyValues[$i][0].'</option>';
		}
		$x .= '</select>';
		return $x;
	}

	// For multiple selection
	function Get_Input_Select_Multi($Name="",$ID="",$KeyValues=array(),$DefaultValue="",$OtherMember="") 
	{
		$x = '<select name="'.$Name.'" id="'.$ID.'" '.$OtherMember.' multiple />';
		for ($i=0; $i< sizeof($KeyValues); $i++) 
		{
			if (is_array($DefaultValue) && count($DefaultValue)>0)
			{
				if (in_array($KeyValues[$i][1],$DefaultValue))
				{
					$Selected = 'Selected="Selected"';
				}
				else
				{
					$Selected = '';
				}
			}
			else
			{
				$Selected = '';
			}
			$x .= '<option value="'.$KeyValues[$i][1].'" '.$Selected.'/>'.$KeyValues[$i][0].'</option>';
		}
		$x .= '</select>';
		return $x;
	}
		
	function Get_Paragraph_Open($Member="") {
		$x = '<p '.$Member.' >';
		
		return $x;
	}
	
	function Get_Paragraph_Close(){
		$x = '</p>';
		
		return $x;
	}
	
	function Get_IFrame($FrameBorder="0",$Name="",$ID="",$Scroll="no",$Width="100%",$Height="200",$Src="",$MarginWidth="0",$MarginHeight="0",$OtherMember="") {
		$x = '<iframe 
						frameborder="'.$FrameBorder.'" 
						name="'.$Name.'" 
						id="'.$ID.'"
						scrolling="'.$Scroll.'" 
						width="'.$Width.'" 
						height="'.$Height.'" 
						src="'.$Src.'" 
						marginwidth"'.$MarginWidth.'" 
						marginheight="'.$MarginHeight.'" 
						'.$OtherMember.' /></iframe>';
						
		return $x;
	}
	
	function Get_Input_CheckBox($Name="",$ID="",$Value="",$Checked=false,$OtherMember="") {
		$Checked = ($Checked)? 'checked="checked"':'';
		
		$x = '<input type="checkbox" name="'.$Name.'" id="'.$ID.'" value="'.$Value.'" '.$Checked.' '.$OtherMember.' />';
						
		return $x;
	}
	
	function Get_Input_RadioButton($Name="",$ID="",$Value="",$Checked=false,$OtherMember="") {
		$Checked = ($Checked)? 'checked="checked"':'';
		
		$x = '<input 
						type="radio" 
						name="'.$Name.'" 
						id="'.$ID.'" 
						value="'.$Value.'" 
						'.$Checked.' 
						'.$OtherMember.' 
					/>';
						
		return $x;
	}
	
	function Get_Input_Textarea($Name, $Contents, $Cols=70, $Rows=5, $OnFocus="", $OtherMember=""){
		$InitialRows = 3;
		$InitialRows = (trim($Contents)=="") ? $InitialRows : $Rows;
		$x = '<textarea 
					name="'.$Name.'" 
					id="'.$Name.'" 
					cols="'.$Cols.'" 
					rows="'.$InitialRows.'" 
					wrap="virtual" 
					onFocus="this.rows='.$Rows.'; '.$OnFocus.'"
					'.$OtherMember.'>';
		$x .= $Contents;
		$x .= '</textarea>';
	
		return $x;
	}
	
	function Get_Input_Password($Name="",$ID="",$Value="",$OtherMember="") {
		$Name = ($Name=="")? "":'name="'.$Name.'"';
		$ID = ($ID=="")? "":'id="'.$ID.'"';
		$Value = ($Value=="")? "":'value="'.$Value.'"';
		
		$x = '<input 
						type="password" 
						'.$Name.' 
						'.$ID.' 
						'.$Value.' 
						'.$OtherMember.' 
					/>
				 ';
				 
		return $x;
	}
	
	function Get_Br($OtherMember="")
	{
		$x = '<br ' . $OtherMember . '/>';
		
		return $x;
	}
	
	
	function Get_Word_Editor($Name="",$ID="",$Value="",$OnLoadFocus=false,$OtherMember="") {
		global $PathRelative;
		
		//convert all types of single quotes
		$Value = str_replace(chr(145), chr(39), $Value);
		$Value = str_replace(chr(146), chr(39), $Value);
		$Value = str_replace("'", "&#39;", $Value);
		
		//convert all types of double quotes
		$Value = str_replace(chr(147), chr(34), $Value);
		$Value = str_replace(chr(148), chr(34), $Value);
	//	$tmpString = str_replace("\"", "\"", $tmpString);
		
		//replace carriage returns & line feeds
		$Value = str_replace(chr(10), " ", $Value);
		$Value = str_replace(chr(13), " ", $Value);
		
		/*// Remove any special charactor which will crashes in javascript
		$Value = str_replace(chr(0),"",$Value); // null
		$Value = str_replace(chr(13),"<br>",$Value); // carriage return
		$Value = str_replace(chr(10),"",$Value); // newline 
		$Value = str_replace(chr(11),"",$Value); // vertical tab 
		$Value = str_replace(chr(27),"",$Value); // escape
		$Value = str_replace(chr(39),"\'",$Value); // single quote*/
		
		/*for ($i=0; $i< 33; $i++) {
			$Value = str_replace(chr($i),"",$Value);
		}*/
		$x .= '<script language="JavaScript" type="text/javascript" src="'.$PathRelative.'src/include/js/word_process/html2xhtml.js"></script>
					<script language="JavaScript" type="text/javascript" src="'.$PathRelative.'src/include/js/word_process/richtext_compressed.js"></script>
					
					<script language="JavaScript" type="text/javascript">
					<!--
					
					//Usage: initRTE(imagesPath, includesPath, cssFile, genXHTML, encHTML)
					initRTE("'.$PathRelative.'src/include/js/word_process/images/", "'.$PathRelative.'src/include/js/word_process/", "", false);
					//-->
					</script>
					
					<script language="JavaScript" type="text/javascript">
					<!--
					//build new richTextEditor
					var rte1 = new richTextEditor(\''.$Name.'\');
					var TmpBody = \''.$Value.'\';
					//TmpBody = TmpBody.replace("<<","&lt;");
					//TmpBody = TmpBody.replace(">>","&gt;");
					rte1.html = TmpBody;
					
					//enable all commands
					rte1.cmdFormatBlock = true;
					rte1.cmdFontName = true;
					rte1.cmdFontSize = true;
					rte1.cmdIncreaseFontSize = true;
					rte1.cmdDecreaseFontSize = true;
					
					rte1.cmdBold = true;
					rte1.cmdItalic = true;
					rte1.cmdUnderline = true;
					rte1.cmdStrikethrough = true;
					rte1.cmdSuperscript = true;
					rte1.cmdSubscript = true;
					
					rte1.cmdJustifyLeft = true;
					rte1.cmdJustifyCenter = true;
					rte1.cmdJustifyRight = true;
					rte1.cmdJustifyFull = true;
					
					rte1.cmdInsertHorizontalRule = false;
					rte1.cmdInsertOrderedList = true;
					rte1.cmdInsertUnorderedList = true;
					
					rte1.cmdOutdent = true;
					rte1.cmdIndent = true;
					rte1.cmdForeColor = true;
					rte1.cmdHiliteColor = true;
					rte1.cmdInsertLink = false;
					rte1.cmdInsertImage = false;
					rte1.cmdInsertSpecialChars = false;
					rte1.cmdInsertTable = false;
					rte1.cmdSpellcheck = false;
					
					rte1.cmdCut = false;
					rte1.cmdCopy = false;
					rte1.cmdPaste = false;
					rte1.cmdUndo = false;
					rte1.cmdRedo = false;
					rte1.cmdRemoveFormat = false;
					rte1.cmdUnlink = false;
					
					rte1.toggleSrc = false;
					rte1.width = "100%";
					
					rte1.build();';
					
		if ($OnLoadFocus) {
			$x .= '
						 var Obj = document.getElementById("'.$Name.'");
						 Obj.contentWindow.focus();
						';
		}
						 
		$x .= '//-->
					</script>';
		
		return $x;
	}
	
	function Get_Table_Open($ID="",$Width="",$Height="",$Border=0,$Cellspacing=0,$Cellpadding=0,$OtherMember="") {
		$ID = ($ID=="")? '':'id="'.$ID.'"';
		$Width = ($Width=="")? '':'width="'.$Width.'"';
		$Height = ($Height=="")? '':'height="'.$Height.'"';
		$Border = ($Border=="")? '':'border="'.$Border.'"';
		$Cellspacing = 'cellspacing="'.$Cellspacing.'"';
		$Cellpadding = 'cellpadding="'.$Cellpadding.'"';
		
		$x = '<table '.$ID.' '.$Width.' '.$Height.' '.$Border.' '.$Cellspacing.' '.$Cellpadding.' '.$OtherMember.' />';
		return $x;
	}
	
	function Get_Table_Close() {
		return '</table>';
	}
	
	function Get_Table_Row_Open($ID="",$OtherMember="") {
		$ID = ($ID=="")? "":'id="'.$ID.'" name="'.$ID.'"';
		
		return '<tr 
							'.$ID.' 
							'.$OtherMember.' 
						/>';
	}
	
	function Get_Table_Row_Close() {
		return '</tr>';
	}
	
	function Get_Table_Cell_Open($ID="",$OtherMember="") {
		$ID = ($ID=="")? "":'id="'.$ID.'"';
		
		return '<td 
							'.$ID.' 
							'.$OtherMember.' 
						/>';
	}
	
	function Get_Table_Cell_Close() {
		return '</td>';
	}
	
	function Get_Input_File($Name="",$ID="",$OtherMember="") {
		$x = '<input type="file" name="'.$Name.'" id="'.$ID.'" '.$OtherMember.' />';
		
		return $x;
	}
	/* ----- End of HTML Tag ----- */
	
/******************************************************************/
/*    Get the Ajax jQuery Date Picker for selecting calender Date */
/******************************************************************/
function Get_DatePicker_Init($Style="", $HasSource=false)
{
	Global $PathRelative, $SYS_CONFIG;
		
	// check to customize css
	if($Style == "")
	$Style = $PathRelative."theme/css/jquery/ui.datepicker.css";
		
	// check to include jquery file or not
	if(!$HasSource)
	$jQueryPath = $PathRelative."src/include/js/jquery/jquery.js";
		
	$jDatePickerPath = $PathRelative."src/include/js/jquery/ui.datepicker.js";
		
	$ReturnStr .= "<script type=\"text/javascript\" src=\"".$jQueryPath."\"></script>";
	$ReturnStr .= "<style type=\"text/css\">@import url(".$Style.");</style>";
	$ReturnStr .= "<script type=\"text/javascript\" src=\"".$jDatePickerPath."\"></script>";
		
	return $ReturnStr;
} // end function get datepicker init
	
function Get_DatePicker($ParID="", $DateFormat="", $DefaultDate="", $OtherMember="", $externalParam="")
{
	Global $SYS_CONFIG;
		
	$IconPath = "/theme/image_en/icon_calendar.gif";
	if($DateFormat == "")
	$DateFormat = "dd-mm-yy";
		
	$ReturnStr .= "
	<script>
  	$(document).ready(function()
  	{
		$.datepicker.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '$IconPath', buttonText: 'Calendar'});
    	$('#$ParID').datepicker({
	    dateFormat: '$DateFormat',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
	    });
  	});
  	</script>";
  		
  	$ReturnStr .= $this->Get_Span_Open("",$externalParam);
	$ReturnStr .= $this->Get_Input_Text($ParID, $DefaultDate, $OtherMember);
	$ReturnStr .= $this->Get_Span_Close();
 
	return $ReturnStr;
} // end function Get DatePicker
	
	
	// get the dot line
	function Get_Dot_Line() {
		$x = $this->Get_Table_Open(null,"95%",null,0,4,0);
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('','class="dotline"');
		$x .= '&nbsp;';
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('');
		$x .= '&nbsp;';
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		
		return $x;
	}
	
	function Get_Sub_Function_Header($Title, $Msg="") {
		Global $PathRelative;
		Global $Lang;
		Global $SYS_CONFIG;
		
		# eclass only
		Global $plugin,$iCalendar_iCalendar,$iCalendar_iCalendarLite;
		$icalendar_name = $plugin["iCalendarFull"]?$iCalendar_iCalendar :$iCalendar_iCalendarLite;
		#################
		switch ($Title) {
			case "iMail":
				if ($SYS_CONFIG['Mail']['CacheEnabled']) 
					$TitleLink = $PathRelative."src/module/email/cache_index.php";
				else
					$TitleLink = $PathRelative."src/module/email/imail_inbox.php";
				break;
			case "Demographics":
				$TitleLink = $PathRelative."src/module/demographic/demographic_main.php";
				break;
			case "iCalendar":
				$TitleLink = "index.php";
				break;
			case $Lang['bulletin']['HeaderTitle']:
				$TitleLink = $PathRelative."src/module/bulletin/index.php";
				break;
			case $Lang['bulletin']['HeaderTitleESFC']:
				$TitleLink = $PathRelative."src/module/bulletin/foundation_news.php";
				break;
			case $Lang['email_gamma']['HeaderTitle']:
				$TitleLink = $PathRelative."src/module/email/gamma/imail_gamma.php";
				break;
			case $Lang['cpd']['HeaderMgt']:
				$TitleLink = $PathRelative."src/module/cpd/mgt_index.php";
				break;
			case $Lang['cpd']['HeaderEnrolment']:
				$TitleLink = $PathRelative."src/module/cpd/enrolment_index.php";
				break;
			case $Lang['cpd']['HeaderTrainer']:
				$TitleLink = $PathRelative."src/module/cpd/trainer_index.php";
				break;
			case $Lang['SubjectClassMapping']['HeaderTitle']:
				if (($SYS_CONFIG['SchoolCode'] == 'ESFC' || $SYS_CONFIG['SchoolCode'] == 'ESF') && !$SYS_CONFIG['DisableModule']['GeneralAdmin']['SubjectClassMapping']['GeneralUsage']){
					$TitleLink = $PathRelative."src/module/general_admin/subject_class_mapping/map_year_setting.php";
				}
				else if (!$SYS_CONFIG['DisableModule']['GeneralAdmin']['SubjectClassMapping']['SubjectMapping']) {
					$TitleLink = $PathRelative."src/module/general_admin/subject_class_mapping/map_subject.php";
				}
				else if (!$SYS_CONFIG['DisableModule']['GeneralAdmin']['SubjectClassMapping']['ClassMapping']) {
					$TitleLink = $PathRelative."src/module/general_admin/subject_class_mapping/map_class.php";
				}
				else {
					$TitleLink = $PathRelative."src/module/general_admin/subject_class_mapping/map_subject_head.php";
				}
				break;
			case $Lang['portal']['Attendance']:
				if ($SYS_CONFIG['DisableModule']['GeneralAdmin']['Attendance']['Record']) {
					$TitleLink = $PathRelative."src/module/attendance/report_index.php";
				} else {
					$TitleLink = $PathRelative."src/module/attendance/attendance_index.php";
				}
				break;	
			case $Lang['Communication']['ResourceShare']['ResourceShareTitle']:
				$TitleLink = $PathRelative."src/module/communication/staff_resource_share/search_resource.php";
				break; 
			default:
				$TitleLink = "#";
				break;
		}
		
		$x = $this->Get_Div_Open("module_content_header");
		//$x .= $this->Get_Div_Open("module_content_header_title").$this->Get_HyperLink_Open($TitleLink).$Title.$this->Get_HyperLink_Close().$this->Get_Div_Close();
		$loc = '';
		if (strstr($_SERVER['SCRIPT_FILENAME'],'new_event.php')){
			$loc = 'new_event';
		}
		else if (strstr($_SERVER['SCRIPT_FILENAME'],'index.php')){
			$loc = 'home';
		}
		if (!empty($loc)){
//			$x.= gen_online_help_btn_and_layer('icalendar',$loc);
		}
		
		$x .= $this->Get_Div_Open("module_content_header_title").$this->Get_HyperLink_Open($TitleLink).$icalendar_name.$this->Get_HyperLink_Close().$this->Get_Div_Close();
		
		//$x .= $this->Get_Div_Open("module_content_relate").$this->Get_HyperLink_Open("#")."related information".$this->Get_HyperLink_Close().$this->Get_Div_Close();
		$x .= $this->Get_Br();
		//$x .= "&nbsp;<br>";
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Return_Msg($Msg);
					
		return $x;
	}
	
	function Get_Return_Msg($Msg) {
		if ($Msg == "" || is_null($Msg)) {
			$x .= $this->Get_Div_Open("return_status",'style="display:none;"');
			$x .= $this->Get_Div_Close();
		}
		else {
			$DisplayMsg = explode('|=|',$Msg);

			if ($DisplayMsg[0] == '1') 
				$Style = 'return_status_success';
			else 
				$Style = 'return_status_warning';
				
			$x .= $this->Get_Div_Open("return_status");
			$x .= $this->Get_Span_Open('','class="'.$Style.'"');
			$x .= '<strong>';
			$x .= $DisplayMsg[1];
			$x .= '</strong> ';
			$x .= $this->Get_HyperLink_Open("#",'onclick="document.getElementById(\'return_status\').style.display = \'none\';"');
			$x .= "[X]";
			$x .= $this->Get_HyperLink_Close();
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Div_Close();	
		}


		$x .= '<script>';
		$x .= 'var lSpacing = (document.body.clientWidth - 974) / 2;';

		$x .= 'if (lSpacing < 0) lSpacing = 0;';
		$x .= 'document.getElementById("return_status").style.left=lSpacing + 560;';
		$x .= 'function Hide_Return_Msg() {
						document.getElementById(\'return_status\').style.display = \'none\';
						setTimeout("Hide_Return_Msg();",1000*5);
					 }
					 
					 setTimeout("Hide_Return_Msg();",1000*5);
					 ';
		$x .= '</script>';


		return $x;
	}
	
	function Show_Parent_ReturnMessage($Msg="") {
		if ($Msg != "") {
			
			$DisplayMsg = explode('|=|',$Msg);
			if ($DisplayMsg[0] == '1') 
				$Style = 'return_status_success';
			else 
				$Style = 'return_status_warning';
				
			$ReMsg .= $this->Get_Span_Open('','class="'.$Style.'"');
			$ReMsg .= '<strong>';
			$ReMsg .= $DisplayMsg[1];
			$ReMsg .= '</strong> ';
			$ReMsg .= $this->Get_HyperLink_Open("#",'onclick="document.getElementById(\'return_status\').style.display = \'none\';"');
			$ReMsg .= "[X]";
			$ReMsg .= $this->Get_HyperLink_Close();
			$ReMsg .= $this->Get_Span_Close();
			
			$x .= '<script>
								window.parent.document.getElementById(\'return_status\').innerHTML = \''.str_replace("'","\'",$ReMsg).'\';
								window.parent.document.getElementById(\'return_status\').style.display = \'block\';
						 </script>
						';
		}
		
		return $x;
	}


}


?>