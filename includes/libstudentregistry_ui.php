<?php
# modifying by :  

############# Change Log [Start]
#
#   Date:   2019-08-15 Cameron
#           - retrieve submittedCount in Get_Class_List()
#
#   Date:   2019-07-08 Cameron
#           - fix retrieving subdistrict name in getPdfStudentDataRecordStudentInfo() and getPdfStudentDataRecordParentInfo()
#
#   Date:   2019-05-14 Cameron
#           - fix potential sql injection problem by enclosing var with apostrophe
#
#   Date:   2019-05-09 (Cameron)
#           - fix last school attended title should be no colspan in getPdfStudentDataRecordStudentInfo()
#           - show age event it's 0 in getSiblingsTable()
#           - add function getPresetCodeList()
#
#   Date:   2019-03-12 (Cameron)
#           - call parent constructor in this constructor
#           - add functions for Kentville
#
#	Date:	2017-06-09 (Cameron)
#			Add RecordType=2 to Get_Class_List() so that it retrieve Student only (exlcude Alumni), hence be consistent with that show in deails in class.php
#
#	Date:	2013-03-25 (YatWoon)
#			Modified Get_Class_List(), remove useless "waiting for approval" column
#
#	Date:	2013-03-20 (YatWoon)
#			Modified Get_Group_Access_Right_Settings_Index(), add checking for $plugin['StudentRegistry_Malaysia'] to stats, settings > eNotice template
#	Date:   2010-10-13 (Henry Chow)
#			Modifed Get_Group_Access_Right_Settings_Index(), display Guardian info for update
#
#	Date:   2010-08-20 (Henry Chow)
#			Modifed Get_Group_Access_Right_Settings_Index(), add checking of "Reports" & "Statistics"
#
#	Date:   2010-08-13 (Henry Chow)
#			Modifed Get_Group_Access_Right_Settings_Index(), add checking of "Settings > eNotice Template" for Malaysia ONLY
#
#	Date:   2010-08-11 (Henry Chow)
#			Modifed Get_Group_Access_Right_Settings_Index(), add checking of "Student Registry Status" for Malaysia ONLY
#
#	Date:   2010-07-29 (Henry Chow)
#			Modifed Get_Group_Access_Right_Settings_Index(), display "Last Modifed By" in access right
#
#	Date:   2010-07-27 Thomas
#			Add wording 'Delete' for the delete button
#			Function affected:
#			- Get_Group_Access_Right_Users_List
#
#			Display the Edit Icon (Pen Icon) immediately after the group name and 
#			description instead of displaying at the end of line
#			Function affected:			
#			- Get_Group_Access_Right_Users_Index
#			- Get_Group_Access_Right_Settings_Index 	
#
#	Date:	2010-07-26	Yatwoon
#			update the layout to IP25 standard, remove [ style="width:96%;"]
#
############# Change Log [End]

include_once('libstudentregistry.php');
include_once('libinterface.php');
include_once('libaccessright.php');

if ($sys_custom['StudentRegistry']['Kentville']) {
    include_once('libstudentregistry_kv.php');
}

class libstudentregistry_ui extends interface_html {
	
	var $Module;
	
	function libstudentregistry_ui() {
		$this->Module = "StudentRegistry";
		parent::__construct();
	}

	function Get_Function_Access_Right_Settings_Index()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		//$x .= $this->Include_JS_CSS();
		$x .= '
			  <div class="content_top_tool" id="SearchInputLayer">
		          <div class="Conntent_search">
		          	<input name="Keyword" id="Keyword" type="text" onkeyup="Check_Go_Search(event);"/>
		          </div>
	          	  <br style="clear:both" />
         	  </div>
         	  <div class="content_top_tool">
			  </div>
	          <div class="table_board" id="FunctionGroupListLayer">';
	    
	    $x .= $this->Get_Function_Access_Right_Settings_List();
	    
	    $x .= '</div>
	    			<br/>
	    			<br/>';
		
		return $x;
	}
	
	function Get_Function_Access_Right_Settings_List($Keyword="")
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $i_no_record_exists_msg;
		
		//$StaffAccessRight = new staff_access_right();
		$GroupList = $this->Get_Group_List($Keyword);
		
		$x = '<div class="table_board">
				<table class="common_table_list">
					<thead>
					<tr>
						<th class="num_check">#</th>
						<th>'.$Lang['AccountMgmt']['Settings']['GroupName'].'</th>
						<th>'.$Lang['AccountMgmt']['Settings']['Description'].'</th>
						<th>'.$Lang['AccountMgmt']['Settings']['NoOfMembers'].'</th>
					    <th><div class="table_row_tool row_content_tool"><a title="'.$Lang['StaffAttendance']['AddGroup'].'" class="add" href="group_access_right_detail.php" /></div></th>
					</tr>
					</thead>
					<tbody>';
		
		for($i=0;$i<sizeof($GroupList);$i++)
		{
		 	$x .= '<tr>
		              <td>'.($i+1).'</td>
					  <td><a href="group_access_right_detail.php?GroupID='.$GroupList[$i]['GroupID'].'">'.htmlspecialchars($GroupList[$i]['GroupTitle'],ENT_QUOTES).'</a></td>
					  <td>'.htmlspecialchars($GroupList[$i]['GroupDescription'],ENT_QUOTES).'</td>
					  <td><a href="group_access_right_users.php?GroupID='.$GroupList[$i]['GroupID'].'">'.$GroupList[$i]['NumberOfMember'].'</a></td>
					  <td><div class="table_row_tool"><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="group_access_right_detail.php?GroupID='.$GroupList[$i]['GroupID'].'" /><a title="'.$Lang['Btn']['Delete'].'" class="delete_dim" href="javascript:void(0);" onclick="Delete_Group('.$GroupList[$i]['GroupID'].')" /></div></td>
				  </tr>';
		}
		if(sizeof($GroupList)==0) {
 		$x .= '<tr>
				  <td colspan=\'5\' height=\'40\' align=\'center\'>'.$i_no_record_exists_msg.'</td>
				</tr>';
		}
		
		$x .= '	
 			   </tbody>
			   </table>						
			   </div>';
					  
		return $x;
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		include_once($PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/common_js_lang.php');
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.core.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.draggable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.droppable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.selectable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
			
			
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
			';
			
		return $x;
	}	
	
	function Get_Group_List($Keyword)
	{
		$ldb = new libdb();
		$lsr = new libstudentregistry();
		
		if (trim($Keyword) != "")
		{
			$sql = 'select distinct 
								g.GroupID 
							From 
								ACCESS_RIGHT_GROUP as g  
							where 
								Module = \''.$this->Module.'\'
							and
								(
								g.GroupTitle like \'%'.$ldb->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								g.GroupDescription like \'%'.$ldb->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								g.GroupType like \'%'.$ldb->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								)';
			
			$GroupIDList = '-1,'.implode(',',$lsr->returnVector($sql));
		}
		
		$sql = 'SELECT UserID FROM INTRANET_USER WHERE RecordType=\'1\' AND RecordStatus=\'1\' ';
		$StudentIDList = '-1,'.implode(',',$lsr->returnVector($sql));
		
		$sql = 'SELECT
				  g.GroupID,
				  g.GroupTitle,
				  IF(g.GroupDescription IS NULL OR TRIM(g.GroupDescription) = \'\',\'--\',g.GroupDescription) as GroupDescription,
				  g.GroupType,
				  CASE 
				  	WHEN count(1) = 1 and ug.GroupID IS NULL THEN 0 
				  	ELSE count(1) 
				  END as NumberOfMember
				FROM 
					ACCESS_RIGHT_GROUP as g
				LEFT JOIN ACCESS_RIGHT_GROUP_MEMBER as ug
					ON g.GroupID = ug.GroupID 
					AND ug.UserID IN ('.$StudentIDList.') 
				WHERE 
					g.Module = \''.$this->Module.'\' ';
		
		if (trim($Keyword)!="") 
			$sql .= 'AND g.GroupID in ('.$GroupIDList.') ';
			
		
			$sql .= 'GROUP BY
						  g.GroupID 
					ORDER BY
						  g.GroupTitle';
		
		return $lsr->returnArray($sql);
	}	
	
	function Get_Group_Access_Right_Settings_Index($GroupID)
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $intranet_session_language, $linterface, $iDiscipline, $plugin;
		
		/********************************************
		* -- MGMT --
		* MGMT_RegistryInformation_View
		* MGMT_RegistryInformation_Manage
		* 
		* -- SETTINGS --
		* SETTINGS_AccessRight_Access
		*********************************************/
		
		$lsr = new libstudentregistry();
		if($GroupID != null && trim($GroupID) != '' && $GroupID > 0 )
		{
			$ResultArr = $lsr->Get_Group_Info($GroupID);
			
			$GroupTitle = $ResultArr[0]['GroupTitle'];
			$GroupDescription = $ResultArr[0]['GroupDescription'];
			$name = $lsr->RETRIEVE_MODIFIED_BY_INFO($ResultArr[0]['LastModifiedBy']);
			//$LastUpdateDate = ($intranet_session_language=="en") ? $ResultArr[0]['DateModified']." ".$iDiscipline['By']." ".$name : $ResultArr[0]['DateModified']." ($name)";
			
			if($ResultArr[0]['LastModifiedBy']!="")
				$LastUpdate = ($intranet_session_language=="en") ? " ".$iDiscipline['By']." ".$name : " ($name)";
			$LastUpdateDate = $ResultArr[0]['DateModified'].$LastUpdate;
		}else
		{
			$GroupTitle = "";
			$GroupDescription = "";
			$LastUpdateDate = "";
		}
		
		$RightList = $lsr->retrieveAccessRight($GroupID);
		for ($i=0; $i< sizeof($RightList); $i++) {
			$Right[$RightList[$i][1]][$RightList[$i][2]][$RightList[$i][3]] = 1;
		}
		
		$x = $this->Include_JS_CSS();
		$x .= '
			  <div class="navigation">
					<img height="15" align="absmiddle" width="15" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
					<a href="index.php">'.$Lang['AccountMgmt']['Settings']['GroupList'].'</a>
					<img height="15" align="absmiddle" width="15" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
					<span id="GroupTitleNavLayer">'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span>
					<br />
			  	<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />
			  </div>
        <div class="detail_title_box">
					<span style="width:100%">
						<span style="width:10%;">'.$Lang['AccountMgmt']['Settings']['GroupName'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupTitle" onmouseover=Show_Edit_Icon("GroupTitleEditIcon") onmouseout=Hide_Edit_Icon("GroupTitleEditIcon") class="jEditInput">'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupTitleEditIcon" class="edit_dim" href="javascript:jsClickEdit();"></a></span>
						</strong>
					</span>
					<div id="GroupTitleWarningLayer" style="display:none; color:red;" ></div>
					<br style="clear: both;" />
					<span style="width:100%">
		                <span style="width:10%">'.$Lang['AccountMgmt']['Settings']['Description'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupDescription" onmouseover=Show_Edit_Icon("GroupDescEditIcon") onmouseout=Hide_Edit_Icon("GroupDescEditIcon") class="jEditInput2">'.htmlspecialchars($GroupDescription,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupDescEditIcon" class="edit_dim" href="javascript:jsClickEdit2();"></a></span>
						</strong>
					</span>
          <br style="clear: both;" />
        </div>
				<br style="clear: both;"/>
        <div class="shadetabs">
          <ul>
            <li class="selected"><a href="#">'.$Lang['StaffAttendance']['AccessRight'].'</a></li>
            <li><a href="group_access_right_users.php?GroupID='.$GroupID.'">'.$Lang['StudentRegistry']['UserList'].'</a></li>
          </ul>
        </div>
        <br style="clear: both;"/>
			  <form name="GroupRightForm" id="GroupRightForm" method="POST" action="group_access_right_update.php" >
			  <input type="hidden" id="GroupTitle" name="GroupTitle" value="'.htmlspecialchars($GroupTitle,ENT_QUOTES).'" />
			  <input type="hidden" id="GroupDescription" name="GroupDescription" value="'.htmlspecialchars($GroupDescription,ENT_QUOTES).'" />
        <div class="table_board">';
        
		// MGMT
		if($plugin['StudentRegistry_Malaysia']) {
			$colspan1 = ' colspan="3"';
			$colspan2 = ' colspan="2"';
		}
		$x .= '
				<table class="common_table_list rights_table_list">
				  <tbody>
					<tr class="seperate_row">
				    <th class="sub_row_top" '.$colspan1.'>
							<input type="checkbox" id="MGMT" name="MGMT" onclick="Check_Group(this)"/>
							<label for="MGMT">'.$Lang['StaffAttendance']['Management'].'</label> 
						</th>
				 	</tr>
			  	<tr class="tabletop">';

		$x .= '
				    <th '.$colspan2.'>'.$Lang['Menu']['AccountMgmt']['StudentRegistryInfo'].'</th>';
		if($plugin['StudentRegistry_Malaysia'])				    
			$x .= '			<th>'.$Lang['StudentRegistry']['StudentRegistryStatus'].'</th>';
		$x .= '	    </tr>
					<tr>';
		if($plugin['StudentRegistry_Macau'])			
			$x .= '
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYINFORMATION']['VIEW']==1)?'rights_selected':'rights_not_select').'">
			     		<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYINFORMATION']['VIEW']==1)?'checked':'').' id="MGMT_RegistryInformation_View" value="MGMT_RegistryInformation_View" name="Right[]" onclick="Check_Single(this);"/>
			     		<label for="MGMT_RegistryInformation_View">'.$Lang['AccountMgmt']['Settings']['View'].'</label>
			   			</td>';
		else
			$x .= '
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYINFORMATION']['VIEWBASIC']==1)?'rights_selected':'rights_not_select').'">
			     		<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYINFORMATION']['VIEWBASIC']==1)?'checked':'').' id="MGMT_RegistryInformation_ViewBasic" value="MGMT_RegistryInformation_ViewBasic" name="Right[]" onclick="Check_Single(this);"/>
			     		<label for="MGMT_RegistryInformation_ViewBasic">'.$Lang['AccountMgmt']['Settings']['View'].' ('.$Lang['StudentRegistry']['BasicInfo'].')</label>
			   			</td>
						';
			
		
		if($plugin['StudentRegistry_Malaysia'])
			$x .= '
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYINFORMATION']['VIEWADV']==1)?'rights_selected':'rights_not_select').'">
			     		<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYINFORMATION']['VIEWADV']==1)?'checked':'').' id="MGMT_RegistryInformation_ViewAdv" value="MGMT_RegistryInformation_ViewAdv" name="Right[]" onclick="Check_Single(this);" />
			     		<label for="MGMT_RegistryInformation_ViewAdv">'.$Lang['AccountMgmt']['Settings']['View'].' ('.$Lang['StudentRegistry']['AdvInfo'].')</label>
			   			</td>
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYSTATUS']['VIEW']==1)?'rights_selected':'rights_not_select').'">
			     		<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYSTATUS']['VIEW']==1)?'checked':'').' id="MGMT_RegistryStatus_View" value="MGMT_RegistryStatus_View" name="Right[]" onclick="Check_Single(this);" />
			     		<label for="MGMT_RegistryStatus_View">'.$Lang['AccountMgmt']['Settings']['View'].'</label>
			   			</td>';
		$x .= '			</tr><tr>';
		if($plugin['StudentRegistry_Macau'])
		$x .= '
				    <td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYINFORMATION']['MANAGE']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYINFORMATION']['MANAGE']==1)?'checked':'').' id="MGMT_RegistryInformation_Manage" value="MGMT_RegistryInformation_Manage" name="Right[]" onclick="Check_Single(this)" />
				      <label for="MGMT_RegistryInformation_Manage">'.$Lang['AccountMgmt']['Settings']['Manage'].'</label></td>';
		else 
			$x .= '	
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYINFORMATION']['MANAGEBASIC']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYINFORMATION']['MANAGEBASIC']==1)?'checked':'').' id="MGMT_RegistryInformation_ManageBasic" value="MGMT_RegistryInformation_ManageBasic" name="Right[]" onclick="Check_Single(this)" />
				      <label for="MGMT_RegistryInformation_ManageBasic">'.$Lang['AccountMgmt']['Settings']['Manage'].' ('.$Lang['StudentRegistry']['BasicInfo'].')</label></td>
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYINFORMATION']['MANAGEADV']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYINFORMATION']['MANAGEADV']==1)?'checked':'').' id="MGMT_RegistryInformation_ManageAdv" value="MGMT_RegistryInformation_ManageAdv" name="Right[]" onclick="Check_Single(this)" />
				      <label for="MGMT_RegistryInformation_ManageAdv">'.$Lang['AccountMgmt']['Settings']['Manage'].' ('.$Lang['StudentRegistry']['AdvInfo'].')</label></td>
						<td class="MGMT-Cell '.(($Right['MGMT']['REGISTRYSTATUS']['MANAGE']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="MGMT-Check" type="checkbox" '.(($Right['MGMT']['REGISTRYSTATUS']['MANAGE']==1)?'checked':'').' id="MGMT_RegistryStatus_Manage" value="MGMT_RegistryStatus_Manage" name="Right[]" onclick="Check_Single(this)" />
				      <label for="MGMT_RegistryStatus_Manage">'.$Lang['AccountMgmt']['Settings']['Manage'].'</label></td>
			';
		$x .= '	    </tr>
			  	<tr>
			    	<td class="rights_not_select" '.$colspan1.'>&nbsp;</td>
			  	</tr>
			  	</tbody>
			  </table>';			  
	
		if($plugin['StudentRegistry_Malaysia'])
		{
		// statistics
		$x .= '
				<table class="common_table_list rights_table_list">
				  <tbody>
					<tr class="seperate_row">
			    	<th class="sub_row_top" colspan="2"><input type="checkbox" id="STATS" name="STATS" onclick="Check_Group(this)"/>
			      <label for="STATS">'.$Lang['Menu']['AccountMgmt']['Statistics'].'</label> </th>
			  	</tr>
			  	<tr class="tabletop">
				    <th>'.$Lang['Menu']['AccountMgmt']['Student'].'</th>
 					<th>'.$Lang['Menu']['AccountMgmt']['Parent'].'</th>
			    </tr>
			  	<tr>
				    <td class="STATS-Cell '.(($Right['STATS']['STUDENT']['VIEW']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="STATS-Check" type="checkbox" '.(($Right['STATS']['STUDENT']['VIEW']==1)?'checked':'').' id="STATS_Student_View" value="STATS_Student_View" name="Right[]" onclick="Check_Single(this)" />
				      <label for="STATS_Student_View">'.$Lang['AccountMgmt']['Settings']['View'].'</label></td>
				    <td class="STATS-Cell '.(($Right['STATS']['PARENT']['VIEW']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="STATS-Check" type="checkbox" '.(($Right['STATS']['PARENT']['VIEW']==1)?'checked':'').' id="STATS_Parent_View" value="STATS_Parent_View" name="Right[]" onclick="Check_Single(this)" />
				      <label for="STATS_Parent_View">'.$Lang['AccountMgmt']['Settings']['View'].'</label></td>
			    </tr>
			  	<tr>
			    	<td class="rights_not_select" colspan="2">&nbsp;</td>
			  	</tr>
					</tbody>
				</table>
		';	
		}
		
		// reports
		$x .= '
				<table class="common_table_list rights_table_list">
				  <tbody>
					<tr class="seperate_row">
			    	<th class="sub_row_top" colspan="2"><input type="checkbox" id="REPORTS" name="REPORTS" onclick="Check_Group(this)"/>
			      <label for="REPORTS">'.$Lang['Menu']['AccountMgmt']['Reports'].'</label> </th>
			  	</tr>
			  	<tr class="tabletop">
				    <th>'.$Lang['Menu']['AccountMgmt']['StudentRegistryInfo'].'</th>';
		if($plugin['StudentRegistry_Malaysia'])
		{		    
 		$x .= '		<th>'.$Lang['StudentRegistry']['StudentRegistryStatus'].'</th>';
		}
 		$x .= ' </tr>
			  	<tr>
				    <td class="REPORTS-Cell '.(($Right['REPORTS']['REGISTRYINFORMATION']['VIEW']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="REPORTS-Check" type="checkbox" '.(($Right['REPORTS']['REGISTRYINFORMATION']['VIEW']==1)?'checked':'').' id="REPORTS_RegistryInformation_View" value="REPORTS_RegistryInformation_View" name="Right[]" onclick="Check_Single(this)" />
				      <label for="REPORTS_RegistryInformation_View">'.$Lang['AccountMgmt']['Settings']['View'].'</label></td>';
		if($plugin['StudentRegistry_Malaysia'])
		{
		$x .= '  	<td class="REPORTS-Cell '.(($Right['REPORTS']['REGISTRYSTATUS']['VIEW']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="REPORTS-Check" type="checkbox" '.(($Right['REPORTS']['REGISTRYSTATUS']['VIEW']==1)?'checked':'').' id="REPORTS_RegistryStatus_View" value="REPORTS_RegistryStatus_View" name="Right[]" onclick="Check_Single(this)" />
				      <label for="REPORTS_RegistryStatus_View">'.$Lang['AccountMgmt']['Settings']['View'].'</label></td>';
		}
		$x .= '</tr>
			  	<tr>
			    	<td class="rights_not_select" colspan="2">&nbsp;</td>
			  	</tr>
					</tbody>
				</table>
		';	
			
		// settings
		$x .= '
				<table class="common_table_list rights_table_list">
				  <tbody>
					<tr class="seperate_row">
			    	<th class="sub_row_top" colspan="2"><input type="checkbox" id="SETTINGS" name="SETTINGS" onclick="Check_Group(this)"/>
			      <label for="SETTINGS">'.$Lang['StaffAttendance']['Settings'].'</label> </th>
			  	</tr>
			  	<tr class="tabletop">
				    <th>'.$Lang['StaffAttendance']['FunctionAccess'].'</th>';
		if($plugin['StudentRegistry_Malaysia'])
		{
			$x .= '		<th>'.$Lang['StudentRegistry']['eNoticeTemplate'].'</th>';
		}
		$x .= ' </tr>
			  	<tr>
				    <td class="SETTINGS-Cell '.(($Right['SETTINGS']['ACCESSRIGHT']['ACCESS']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="SETTINGS-Check" type="checkbox" '.(($Right['SETTINGS']['ACCESSRIGHT']['ACCESS']==1)?'checked':'').' id="SETTINGS_AccessRight_Access" value="SETTINGS_AccessRight_Access" name="Right[]" onclick="Check_Single(this)" />
				      <label for="SETTINGS_AccessRight_Access">'.$Lang['AccountMgmt']['Settings']['Access'].'</label></td>';
		if($plugin['StudentRegistry_Malaysia'])
		{
		$x .= '     <td class="SETTINGS-Cell '.(($Right['SETTINGS']['ENOTICETEMPLATE']['ACCESS']==1)?'rights_selected':'rights_not_select').'">
				    	<input class="SETTINGS-Check" type="checkbox" '.(($Right['SETTINGS']['ENOTICETEMPLATE']['ACCESS']==1)?'checked':'').' id="SETTINGS_eNoticeTemplate_Access" value="SETTINGS_eNoticeTemplate_Access" name="Right[]" onclick="Check_Single(this)" />
				      <label for="SETTINGS_eNoticeTemplate_Access">'.$Lang['AccountMgmt']['Settings']['Access'].'</label></td>';
		}
		$x .= ' </tr>
					</tbody>
				</table>
			<br/>
			<p class="spacer"/>
      </div>
		
      <div class="edit_bottom">';
		if($LastUpdateDate != '') 
			$x .= '<span>'.$Lang['StaffAttendance']['LastUpdateOn'].$LastUpdateDate.'</span>';
		$x .= '<p class="spacer"/>';
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "goSubmit()", "SaveBtn")."&nbsp;";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location='index.php'");
        $x .= '<p class="spacer"/>
      </div>
		</form>';
		
		return $x;
	}	
	
	function Get_Group_Access_Right_Users_Index($GroupID, $xmsg="")
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $linterface, $button_new;
		
		$lsr = new libstudentregistry();
		
		if($GroupID != null && trim($GroupID) != '' && $GroupID > 0 )
		{
			$ResultArr = $lsr->Get_Group_Info($GroupID);
			$GroupTitle = $ResultArr[0]['GroupTitle'];
			$GroupDescription = $ResultArr[0]['GroupDescription'];
			$LastUpdateDate = $ResultArr[0]['DateModified'];
		}else
		{
			$GroupTitle = "";
			$GroupDescription = "";
			$LastUpdateDate = "";
		}
		
		$x .= $this->Include_JS_CSS();
		$x .= "<div align='right'>".$linterface->GET_SYS_MSG($xmsg)."</div>";
		$x .= '
			  <div class="navigation">
					<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
					<a href="index.php">'.$Lang['AccountMgmt']['Settings']['GroupList'].'</a>
					<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
          <span id="GroupTitleNavLayer">'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><br />
			  	<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />
			  </div>
        <div class="detail_title_box">
					<span style="width:100%">
						<span style="width:10%">'.$Lang['AccountMgmt']['Settings']['GroupName'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupTitle" onmouseover=Show_Edit_Icon("GroupTitleEditIcon") onmouseout=Hide_Edit_Icon("GroupTitleEditIcon") class="jEditInput" >'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupTitleEditIcon" class="edit_dim" onClick="javascript:jsClickEdit();"></a></span>
							<input type="hidden" id="GroupTitle" name="GroupTitle" value="'.htmlspecialchars($GroupTitle,ENT_QUOTES).'" />
						</strong>
					</span>
					<div id="GroupTitleWarningLayer" style="display:none; color:red;" ></div>
					<br style="clear: both;" />
					<span style="width:100%">
		                <span style="width:10%">'.$Lang['AccountMgmt']['Settings']['Description'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupDescription" onmouseover=Show_Edit_Icon("GroupDescEditIcon") onmouseout=Hide_Edit_Icon("GroupDescEditIcon") class="jEditInput2" >'.htmlspecialchars($GroupDescription,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupDescEditIcon" class="edit_dim" onClick="javascript:jsClickEdit2();"></a></span>
							<input type="hidden" id="GroupDescription" name="GroupDescription" value="'.htmlspecialchars($GroupDescription,ENT_QUOTES).'" />
						</strong>
					</span>
          <br style="clear: both;"/>
				</div>
              <br style="clear: both;"/>
              <div class="shadetabs">
                <ul>
                  <li><a href="group_access_right_detail.php?GroupID='.$GroupID.'">'.$Lang['StaffAttendance']['AccessRight'].'</a></li>
                  <li class="selected"><a href="#">'.$Lang['StudentRegistry']['UserList'].'</a></li>
                </ul>
              </div>
              <br style="clear: both;"/>
              <div class="content_top_tool">
                ';
					/* <div class="Conntent_tool"><a title="'.$Lang['StaffAttendance']['AddUser'].'" class="thickbox new" href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Add_Member_Form();"> '.$Lang['Btn']['New'].' </a></div>*/
		$x .= '<div class="Conntent_tool"><a title="'.$Lang['StaffAttendance']['AddUser'].'" href="group_access_right_users_add.php?GroupID='.$GroupID.'">'.$button_new.'</a></div>';
		//'.$linterface->GET_LNK_NEW("group_access_right_users_add.php?GroupID=".$GroupID,$btn_new,"","","",0).'
					
		$x .= '		
                <div class="Conntent_search" id="SearchInputLayer">
                    <input type="text" name="Keyword" id="Keyword" onkeyup="Check_Go_Search(event);" />
                </div>
                <br style="clear: both;"/>
              </div>
			  <input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />';
 
		$x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">
				  <tbody>
					<tr>
				    <td valign="bottom"><div class="table_filter"/></td>
				    <td valign="bottom"><div class="common_table_tool"> <a title="'.$Lang['Btn']['Delete'].'" class="tool_delete" href="javascript:void(0);" onclick="Delete_Member();" />'.$Lang['Btn']['Delete'].'</a></div></td>
				  	</tr>
				  </tbody>
				</table>';
       $x .= '      <div class="table_board" id="UserListLayer">';		
		$x .= $this->Get_Group_Access_Right_Users_List($GroupID);
				    
		if($LastUpdateDate != '') {
			$x .= '<br><p class="spacer"/>';
	   	    $x .= '<div class="edit_bottom" style="width:100%;">';
			$x .= '<span>'.$Lang['StaffAttendance']['LastUpdateOn'].$LastUpdateDate.'</span>';
			$x .= '</div>';
		}
			
		$x .= '<p class="spacer"/>
		  	</div>
            <br/>
            <br/>';
		
		return $x;
	}
	
	function Get_Group_Access_Right_Users_List($GroupID, $Keyword="")
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $i_no_record_exists_msg;
		
		$lsr = new libstudentregistry();
		
		$UserList = $lsr->Get_Group_Access_Right_Users($GroupID, $Keyword);
		/*
		$x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">
				  <tbody>
					<tr>
				    <td valign="bottom"><div class="table_filter"/></td>
				    <td valign="bottom"><div class="common_table_tool"> <a title="'.$Lang['Btn']['Delete'].'" class="tool_delete" href="javascript:void(0);" onclick="Delete_Member();" />'.$Lang['Btn']['Delete'].'</a></div></td>
				  	</tr>
				  </tbody>
				</table>';
				*/
		$x .= '
				<table class="common_table_list">	
				  <thead>
				    <tr>
				      <th class="num_check" width="1">#</th>
				      <th width="60%">'.$Lang['StaffAttendance']['StaffName'].'</th>
				      <th width="40%">'.$Lang['StaffAttendance']['UserType'].'</th>
				      <th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'StaffID[]\',this.checked)" /></th>
				      </tr>
				  </thead>
				  <tbody>';
		for($i=0;$i<sizeof($UserList);$i++)
		{
			$x .= '<tr>
				      <td>'.($i+1).'</td>
				      <td>'.$UserList[$i]['UserName'].'</td>
				      <td>'.($UserList[$i]['Teaching']==1?$Lang['Identity']['TeachingStaff']:$Lang['Identity']['NonTeachingStaff']).'</td>
				      <td><input type="checkbox" id="StaffID[]" name="StaffID[]" value="'.$UserList[$i]['UserID'].'" /></td>
				   </tr>';
		}
		if(sizeof($UserList)==0) {
			$x .= '<tr><td colspan="4" height="40" align="center">'.$i_no_record_exists_msg.'</td></tr>';	
		}
		   
		$x .= '</tbody>
			  </table>';
		return $x;
	}
	
	function Get_Group_Access_Right_Add_Member_Form($GroupID)
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$lsr = new libstudentregistry();
		
		$AvaliableUserList = $lsr->Get_Group_Access_Right_Aval_User_List();
		
		$x = '<div class="edit_pop_board" style="height:400px;">
				 <div class="edit_pop_board_write" style="height:350px;">
				    <table class="form_table">
					  <tr>
					    <td >
					    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
			          		<tr>
					          	<td>'.$Lang['StaffAttendance']['StaffName'].'</td>
		    					<td>:</td>
			                	<td bgcolor="#EEEEEE" align="center">
			                		<select name="AddUserID[]" id="AddUserID[]" size="20" style="width:99%" Multiple>';
		for ($i=0; $i< sizeof($AvaliableUserList); $i++)
		{
			$x .= '	         		<option value="'.$AvaliableUserList[$i]['UserID'].'">'.$AvaliableUserList[$i]['StaffName'].'</option>';
		}
				$x .= '             </select>
				                </td>
							</tr>
							<col class="field_title" />
							<col  class="field_c" />
						    </table>
						</td>
					 </tr>
				   </table>
				   <br />
				</div>
				<div class="edit_bottom"> 
					<span> </span>
					<p class="spacer"></p>
					<input name="submit" type="button" class="formbutton" onclick="Add_Member();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Add'].'" />
					<input name="submit3" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
				    <p class="spacer"></p>
				</div>
			</div>';
					
		return $x;
	}
	
	function Get_Class_List($text="", $AcademicYearID="") 
	{
		global $Lang;
		
		$lsr = new libstudentregistry();
		$AcademicYearID = ($AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
		
		$sql = "SELECT 
					yc.YearClassID, 
					".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", 
					COUNT(ycu.UserID) as studentCount, 
					0 as waitApproval,
					y.YearName,
					COUNT(srs.UserID) as submittedCount
				FROM 
					YEAR_CLASS yc LEFT OUTER JOIN 
					YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) INNER JOIN
					YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
					INTRANET_USER USR ON (USR.UserID=ycu.UserID AND USR.RecordType=2) LEFT JOIN
					STUDENT_REGISTRY_SUBMITTED srs ON (srs.UserID=USR.UserID AND srs.AcademicYearID='$AcademicYearID')
				WHERE 
					yc.AcademicYearID='$AcademicYearID' AND USR.RecordStatus IN (0,1,2) AND (yc.ClassTitleEN LIKE '%$text%' OR yc.ClassTitleB5 LIKE '%$text%')
				GROUP BY 
					yc.YearClassID
				ORDER BY 
					y.Sequence, yc.Sequence
				";	
				
		$result = $lsr->returnArray($sql,4);
		
		$temptableContent = "";
		/*
		for($i=0; $i<sizeof($result); $i++) {
			$tableContent .= '	<tr>
									<td>'.($i+1).'</td>
									<td>'.$result[$i][1].'</td>
									<td><a href="class.php?AcademicYearID='.$AcademicYearID.'&targetClass=::'.$result[$i][0].'">'.$result[$i][2].'</a></td>
									<td>0</td>
								</tr>
			';
			$totalStudent += $result[$i][2];
			$totalWaitApproval += 0;
		}
		*/
		$formNameAry[] = $result[0][4];
        $totalStudent = 0;
        $totalSubmitted = 0;

		for($i=0; $i<sizeof($result); $i++) {
			if(!in_array($result[$i][4], $formNameAry))
			{
				$tableContent .= "<tbody>
									<tr>
										<td style=\"background-color:#FFFFFF\" rowspan=\"".($NoOfRecordinForm+2)."\">".$result[$i-1][4]."</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
									</tr>".$temptableContent."</tbody>";
				$NoOfRecordinForm = 0;
				$temptableContent = "";
				$formNameAry[] = $result[$i][4];
			}

            $nonSubmitCount = $result[$i]['studentCount'] - $result[$i]['submittedCount'];
			$temptableContent .= "<tr class=\"sub_row\">
									<td>".$result[$i][1]."</td>
				     			  	<td><a href=\"class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[$i][0]."\">".$result[$i][2]."</a></td>
                                    <td><a href=\"class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[$i][0]."&submit_status=1\">".$result[$i]['submittedCount']."</a></td>
                                    <td><a href=\"class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[$i][0]."&submit_status=-1\">".$nonSubmitCount."</a></td>
								  </tr>";
								  
			$NoOfRecordinForm ++;
			$totalStudent += $result[$i][2];
            $totalSubmitted += $result[$i]['submittedCount'];

			if($i == (sizeof($result) - 1))
			{
				$tableContent .= "<tbody>
									<tr>
										<td style=\"background-color:#FFFFFF\" rowspan=\"".($NoOfRecordinForm+2)."\">".$result[$i][4]."</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
									</tr>".$temptableContent."</tbody>";
			}
		}		
		$returnValue = '		
			<table class="common_table_list">
				<thead>
					<tr>
						<th width="100">#</th>
						<th>'.$Lang['StudentRegistry']['Class'].'</th>
						<th width="100">'.$Lang['StudentRegistry']['NoOfStudents'].'</th>
						<th width="100">'.$Lang['StudentRegistry']['Submitted'].'</th>
						<th width="100">'.$Lang['StudentRegistry']['NonSubmitted'].'</th>
					</tr>
					'.$tableContent.'
					<tr  class="total_row">
						<th>&nbsp;</th>
						<th>'.$Lang['StudentRegistry']['Total'].'</th>
						<td>'.$totalStudent.'</td>
                        <td>'.$totalSubmitted.'</td>
                        <td>'.($totalStudent - $totalSubmitted).'</td>
					</tr>
				</thead>
			</table>		
			';
		return $returnValue;	
	}
	
	function Get_RegistryStatus_Class_List($text="", $AcademicYearID="") 
	{
		global $Lang;
		
		$lsr = new libstudentregistry();
		$AcademicYearID = ($AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
		
		$sql = "SELECT 
					yc.YearClassID, 
					".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", 
					COUNT(ycu.UserID) as studentCount, 
					y.YearName
				FROM 
					YEAR_CLASS yc LEFT OUTER JOIN 
					YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) INNER JOIN
					YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
					INTRANET_USER USR ON (USR.UserID=ycu.UserID)
				WHERE 
					yc.AcademicYearID='$AcademicYearID' AND USR.RecordStatus IN (0,1,2) AND (yc.ClassTitleEN LIKE '%$text%' OR yc.ClassTitleB5 LIKE '%$text%')
				GROUP BY 
					yc.YearClassID
				ORDER BY 
					y.Sequence, yc.Sequence
				";	
				
		$result = $lsr->returnArray($sql,6);
		
		$temptableContent = "";

		$formNameAry[] = $result[0][3];

		$totalNormal = 0;
		$totalLeft = 0;
		$totalSuspend = 0;
		$totalResume = 0;
		$sumTotal = 0;
		
		for($i=0; $i<sizeof($result); $i++) {
			if(!in_array($result[$i][3], $formNameAry))
			{
				$tableContent .= "<tbody>
									<tr>
										<td style=\"background-color:#FFFFFF\" rowspan=\"".($NoOfRecordinForm+2)."\">".$result[$i-1][3]."</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
									</tr>".$temptableContent."</tbody>";
				$NoOfRecordinForm = 0;
				$temptableContent = "";
				$formNameAry[] = $result[$i][3];
			}
		
			$studentAry = $lsr->storeStudent(0, '::'.$result[$i][0]);
			$conds = "";
			if(sizeof($studentAry)>0) {
				$studentIDList = implode(',', $studentAry);
				$conds = " AND UserID IN ($studentIDList)";
			}
			
			
			$sql = "SELECT RecordStatus, COUNT(DISTINCT UserID) FROM STUDENT_REGISTRY_STATUS WHERE IsCurrent=1 $conds GROUP BY RecordStatus";
			$temp = $lsr->returnArray($sql,2);
			$ary = array();
			for($k=0; $k<sizeof($temp); $k++) {
				list($status, $count) = $temp[$k];
				$ary[$status] = $count;
			}
			
			$normal = $result[$i][2]-$ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT]-$ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND]-$ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME];
			$temptableContent .= "<tr class=\"sub_row\">
									<td>".$result[$i][1]."</td>
				     			  	<td><a href=\"class.php?AcademicYearID=$AcademicYearID&recordstatus=0&targetClass=::".$result[$i][0]."\">$normal</a></td>
				      			  	<td><a href=\"class.php?AcademicYearID=$AcademicYearID&recordstatus=".STUDENT_REGISTRY_RECORDSTATUS_LEFT."&targetClass=::".$result[$i][0]."\">".((isset($ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT])) ? $ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT] : 0)."</a></td>
									<td><a href=\"class.php?AcademicYearID=$AcademicYearID&recordstatus=".STUDENT_REGISTRY_RECORDSTATUS_SUSPEND."&targetClass=::".$result[$i][0]."\">".((isset($ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND])) ? $ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND] : 0)."</a></td>
									<td><a href=\"class.php?AcademicYearID=$AcademicYearID&recordstatus=".STUDENT_REGISTRY_RECORDSTATUS_RESUME."&targetClass=::".$result[$i][0]."\">".((isset($ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME])) ? $ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME] : 0)."</a></td>
									<td class=\"total_col\"><a href=\"class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[$i][0]."\">".$result[$i][2]."</a></td>	
								  </tr>";
								  
			$NoOfRecordinForm ++;
			$totalNormal += $normal;
			$totalLeft += $ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT];
			$totalSuspend += $ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND];
			$totalResume += $ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME];
		
			if($i == (sizeof($result) - 1))
			{
				$tableContent .= "<tbody>
									<tr>
										<td style=\"background-color:#FFFFFF\" rowspan=\"".($NoOfRecordinForm+2)."\">".$result[$i][3]."</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
										<td style=\"display:none\">&nbsp;</td>
									</tr>".$temptableContent."</tbody>";
			}
		}		
		$sumTotal = $totalNormal+$totalLeft+$totalSuspend+$totalResume;
		$returnValue = '		
			<table class="common_table_list">
				<thead>
					<tr>
						<th>'.$Lang['AccountMgmt']['Form'].'</th>
						<th>'.$Lang['StudentRegistry']['Class'].'</th>
						<th>'.$Lang['StudentRegistry']['AmountOfApprove'].'</th>
						<th>'.$Lang['StudentRegistry']['AmountOfLeft'].'</th>
						<th>'.$Lang['StudentRegistry']['AmountOfSuspend'].'</th>
						<th>'.$Lang['StudentRegistry']['AmountOfResume'].'</th>
						<th class="total_col">'.$Lang['StudentRegistry']['TotalAmountOfStudents'].'</th>
					</tr>
					'.$tableContent.'
					<tr  class="total_row">
						<th>&nbsp;</th>
						<th>'.$Lang['StudentRegistry']['Total'].'</th>
						<td>'.$totalNormal.'</td>
						<td>'.$totalLeft.'</td>
						<td>'.$totalSuspend.'</td>
						<td>'.$totalResume.'</td>
						<td class="total_col">'.$sumTotal.'</td>
					</tr>
				</thead>
			</table>		
			';
		return $returnValue;	
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////////
	// start functions for Kentville

	function getGuardianGraduateYearOptions($name, $id='', $selected='', $class='')
	{
	    global $Lang;
	    
	    $endYear = date('Y') - 14;
	    $startYear = 1967;
	    
	    $x = '<select name="'.$name.'" id="'.$id.'" class="'.$class.'">';
	    $x .= '<option value=""> -- '.$Lang['Btn']['Select'].' -- </option>';
	    for($i=$endYear; $i>=$startYear; $i--) {
	        $isSelected = ($i == $selected) ? 'selected' : '';
	        $j = $i - 1;
	        $x .= '<option value="'.$i.'" '.$isSelected.'>';
	        $x .= $j . '/' . substr($i,-2);
	        $x .= '</option>';
	    }
	    $x .= '</select>';
	    return $x;
	}
	
	function getSiblingGraduateYearOptions($name, $id='', $selected='', $class='')
	{
	    global $Lang;
	    
	    $endYear = date('Y') + 1;
	    $startYear = $endYear - 20;
	    
	    $x = '<select name="'.$name.'" id="'.$id.'" class="'.$class.'">';
	    $x .= '<option value=""> -- '.$Lang['Btn']['Select'].' -- </option>';
	    for($i=$endYear; $i>$startYear; $i--) {
	        $isSelected = ($i == $selected) ? 'selected' : '';
	        $j = $i - 1;
	        $x .= '<option value="'.$i.'" '.$isSelected.'>';
	        $x .= $j . '/' . substr($i,-2);
	        $x .= '</option>';
	    }
	    $x .= '</select>';
	    return $x;
	}
	
	function getSiblingsTable($studentID)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    $brotherAndSisterAry = $lsr->getBroterAndSister($studentID);
	    $nrBrotherAndSister = count($brotherAndSisterAry);
        $maxSiblings = 3;
	    
	    $editTable = "<table id='BSTable' class='form_table_v30' width='100%' style='display:none'>\n";
// 	        $editTable .= "<tr>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['Relationship']."</td>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['BirthDate']."</td>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['Name']."</td>\n";	
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['IsSchoolStudentOrAlumni']."</td>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['ClassLevel']."</td>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['ExKentvilleYear']."</td>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['School']."</td>\n";
// 	            $editTable .= "<td>".$Lang['StudentRegistry']['Sibling']['Level']."</td>\n";
// 	            $editTable .= "<td>&nbsp;</td>\n";
// 	        $editTable .= "</tr>";
	       
	        for ($i=0;$i<$maxSiblings;$i++) {
	            $j = $i + 1;
	            if ($i<$nrBrotherAndSister) {
	                $_relationship = $brotherAndSisterAry[$i]['Relationship'];
	                $_dateOfBirth = $brotherAndSisterAry[$i]['DateOfBirth']=="0000-00-00" ? '' : $brotherAndSisterAry[$i]['DateOfBirth'];
	                $_studentName = $brotherAndSisterAry[$i]['StudentName'];
	                $_isSchoolStudentOrAlumni = $brotherAndSisterAry[$i]['IsSchoolStudentOrAlumni'];
	                $_graduateYear = $brotherAndSisterAry[$i]['GraduateYear'];
	                $_schoolClass = $brotherAndSisterAry[$i]['SchoolClass'];
	                $_curSchool = $brotherAndSisterAry[$i]['CurSchool'];
	                $_curLevel = $brotherAndSisterAry[$i]['CurLevel'];
	                $_curLevelOthers = $brotherAndSisterAry[$i]['CurLevelOthers'];
	            }
	            else {
	                $_relationship = '';
	                $_dateOfBirth = '';
	                $_studentName = '';
	                $_isSchoolStudentOrAlumni = '';
	                $_graduateYear = '';
	                $_schoolClass = '';
	                $_curSchool =  '';
	                $_curLevel = '';
	                $_curLevelOthers = '';
	            }
	            
	            $relationshipOptions = $lsr->displayPresetCodeSelection("SIBLINGS", "BS_Relationship_".$i, $_relationship, $noFirst=0, "BS_Relationship_".$i);
	            $dateOfBirth = $this->GET_DATE_PICKER("BS_DateOfBirth_".$i, $_dateOfBirth, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1);
	            $_checkIsSchoolStudentOrAlumniYes = $_isSchoolStudentOrAlumni ? 'checked' : '';
	            $_checkIsSchoolStudentOrAlumniNo = $_isSchoolStudentOrAlumni ? '' : 'checked';
	            $isSchoolStudentOrAlumni = "<label><input type='radio' name='BS_IsSchoolStudentOrAlumni_".$i."' id='BS_IsSchoolStudentOrAlumniYes_".$i."' value='1' ".$_checkIsSchoolStudentOrAlumniYes.">".$Lang['General']['Yes']."</label>";
	            $isSchoolStudentOrAlumni .= "<label><input type='radio' name='BS_IsSchoolStudentOrAlumni_".$i."' id='BS_IsSchoolStudentOrAlumniNo_".$i."' value='0' ".$_checkIsSchoolStudentOrAlumniNo.">".$Lang['General']['No']."</label>";
	            $graduateYear = $this->getSiblingGraduateYearOptions("BS_GraduateYear_".$i, "BS_GraduateYear_".$i, $_graduateYear);
	            $curLevelOptions = $lsr->displayPresetCodeSelection("SCHOOLLEVEL", "BS_CurLevel_".$i, $_curLevel, $noFirst=0, "BS_CurLevel_".$i);
	            
//    	        $editTable .= "<tr id='bs_row_".$i."'>";
    	        $editTable .= "<tr>";
    	            $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['Relationship']." (".$j.")</td>\n";
    	            $editTable .= "<td>".$relationshipOptions."</td>\n";
    	            $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['BirthDate']."</td>\n";
    	            $editTable .= "<td>".$dateOfBirth."</td>\n";
    	        $editTable .= "</tr>";
    	        
    	        $editTable .= "<tr>";
        	        $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['Name']."</td>\n";
        	        $editTable .= "<td><input type='text' name='BS_Name_". $i ."' value='". $_studentName."'></td>\n";
        	        $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['IsSchoolStudentOrAlumni']."</td>\n";
        	        $editTable .= "<td>".$isSchoolStudentOrAlumni."</td>\n";
        	    $editTable .= "</tr>";
    	        
    	        $editTable .= "<tr id='trSchoolUserOrAlumni_".$i."'>";
        	        $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['ClassLevel']."</td>\n";
        	        $editTable .= "<td><input type='text' class='textboxnum' name='BS_SchoolClass_". $i ."' id='BS_SchoolClass_". $i ."' value='". $_schoolClass."'></td>\n";
        	        $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['ExKentvilleYear']."</td>\n";
        	        $editTable .= "<td>".$graduateYear."</td>\n";
    	        $editTable .= "</tr>";

    	        $editTable .= "<tr>";
    	            $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['School']."</td>\n";
    	            $editTable .= "<td><input type='text' name='BS_CurSchool_". $i ."' value='". $_curSchool."'></td>\n";
    	            $editTable .= "<td  class='field_title_short'>".$Lang['StudentRegistry']['Sibling']['Level']."</td>\n";
        	        $editTable .= "<td>".$curLevelOptions;
            	        $editTable .= "<div id='spanCurLevelOthers".$i."' style='display:none' class='form_field_sub_content'>\n";
            	            $editTable .= "<table>\n";
            	                $editTable .= "<tr>\n";
            	                    $editTable .= "<td><em>".$Lang['General']['PlsSpecify']."</em></td>\n";
            	                    $editTable .= "<td><input type='text' name='BS_CurLevelOthers_".$i."' id='BS_CurLevelOthers_".$i."' value='".$_curLevelOthers."'></td>\n";
            	                $editTable .= "</tr>\n";
            	            $editTable .= "</table>\n";
            	        $editTable .= "</div>\n";                            	
        	        $editTable .= "</td>\n";
        	        
//        	        $editTable.= "<td><span class='table_row_tool'><a class='delete' title='" . $Lang['StudentRegistry']['Sibling']['RemoveSibling'] . "' onClick='removeBSRow(\"" . $i. "\")'></a></span></td>\n";
    	        $editTable .= "</tr>";
    	        if ($i < $maxSiblings-1) {
    	            $editTable .= "<tr><td colspan='4'>&nbsp;</td></tr>";
    	        }
    	    }
   	    $editTable .= "</table>";
//   	    $editTable .= "<input type='hidden' name='bs_row_count' id='bs_row_count' value='". $nrBrotherAndSister."'>\n";
//   	    $editTable .= "<div><span class='table_row_tool'><a class='newBtn add' onclick='javascript:add_sibling()' title='".$Lang['StudentRegistry']['Sibling']['AddSibling']."'></a><br></span></div>";
   	    
   	    if($nrBrotherAndSister) {
	        $viewTable = "<table id='brothersAndSistersTable' class='common_table_list_v30' width='100%'>\n";
    	        $viewTable .= "<tr>\n";
        	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['RelationshipAge']."</th>\n";
        	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['Name']."</th>\n";
        	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['KentvilleClass']."</th>\n";
        	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['ExKentvilleClassYear']."</th>\n";
        	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['SchoolLevel']."</th>\n";
    	        $viewTable .= "</tr>";
	        
	            foreach($brotherAndSisterAry as $_brotherAndSisterAry){
	                $_relationship = $lsr->returnPresetCodeName("SIBLINGS", $_brotherAndSisterAry['Relationship']);
	                
	                $disRelationAge = $_relationship;
//	                if ($_brotherAndSisterAry['Age']) {
	                    $disRelationAge .= ' ('. $_brotherAndSisterAry['Age'].')';
//	                }
	                $_isSchoolStudentOrAlumni = $_brotherAndSisterAry['IsSchoolStudentOrAlumni'];
	                $_schoolClass = $_brotherAndSisterAry['SchoolClass'];
	                $_graduateYear = $_brotherAndSisterAry['GraduateYear'];
	                if ($_isSchoolStudentOrAlumni && $_graduateYear > 0) {
	                    $disCurClass = '--';
	                    $disExClassAndYear = $_schoolClass . ' ( ' . $_graduateYear.')';
	                }
	                else {
	                    $disCurClass = $_schoolClass ? $_schoolClass : '--';
	                    $disExClassAndYear = '--';
	                }
	                $disCurSchoolAndLevel = $_brotherAndSisterAry['CurSchool'] ? $_brotherAndSisterAry['CurSchool'] : '--';
	                
	                $disCurLevel = $lsr->returnPresetCodeName("SCHOOLLEVEL", $_brotherAndSisterAry['CurLevel']);
	                if ($_brotherAndSisterAry['CurLevel'] == '999') {
	                    $disCurLevel = $_brotherAndSisterAry['CurLevelOthers'];
	                }
	                
	                if ($disCurLevel) {
	                    $disCurSchoolAndLevel .= ' / '. $disCurLevel;
	                }
	                $viewTable .= "<tr>\n";
	                    $viewTable .= "<td>". $disRelationAge."</td>\n";
	                    $viewTable .= "<td>". $_brotherAndSisterAry['StudentName']."</td>\n";
	                    $viewTable .= "<td>". $disCurClass."</td>\n";
	                    $viewTable .= "<td>". $disExClassAndYear."</td>\n";
	                    $viewTable .= "<td>". $disCurSchoolAndLevel."</td>\n";
	                $viewTable .= "</tr>\n";
	            }
	        $viewTable .= "</table>\n";
	    }
	    else {
	        $viewTable = $Lang['General']['NoRecordAtThisMoment'];
	    }
	    
	    return array('editTable'=>$editTable, 'viewTable'=>$viewTable);
	    
	}
	
// 	function addSiblingRow($rowNr)
// 	{
// 	    $row = "<tr id='bs_row_".$rowNr."'>";
// 	        $row .= "<td><input type='text' class='textboxnum' name='BS_Relationship_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='number' min='0' max='99' class='textboxnum' name='BS_Age_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='text' name='BS_Name_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='text' class='textboxnum' name='BS_Class_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='text' class='textboxnum' name='BS_ExClass_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='text' class='textboxnum' name='BS_ExClassYear_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='text' name='BS_CurSchool_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><input type='text' class='textboxnum' name='BS_CurLevel_". $rowNr ."' value=''></td>\n";
//     	    $row .= "<td><span class='table_row_tool'><a class='delete' title='" . $Lang['StudentRegistry']['Sibling']['RemoveSibling'] . "' onClick='removeBSRow(\"" . $rowNr . "\")'></a></span></td>\n";
// 	    $row .= "</tr>";
// 	    return $row;
// 	}

	function getPdfStyleSheet(){
	    global $intranet_session_language;
//	    if ($intranet_session_language == 'b5') {
	        $fonts = "'mingliu', 'Times New Roman'";
// 	    }
// 	    else {
// 	        $fonts = "'Times New Roman', 'mingliu'";
// 	    }
	    
	    $style = "<style>
    	    body {font-family: $fonts; color: #000000; font-size: 10pt;}
    	    table { width:100%;}
    	    td {padding: 3px;}
    	    .contentTable {border-collapse:collapse;}
    	    .contentTable th, .contentTable td {border: 1px solid black;}
        	.schoolName {font-size: 1.2em; font-weight:bold; text-align:center;}
        	.reportTitle {font-weight:bold; text-align:left;}
        	.alignCenter {text-align:center;}
        	.bold {font-weight:bold;}
        	.chiFont {font-family:'mingliu' !important}
            .common_table_list_v30{	width: 100%;	border: 1px solid #CCCCCC;		margin:0 auto;	border-collapse:separate;	border-spacing: 0px; 	*border-collapse: expression('separate', cellSpacing = '0px');	clear:both;}
            .common_table_list_v30 tr th{	margin:0px;	padding:3px;	padding-top:5px;	padding-bottom:5px;	background-color: #A6A6A6;	font-weight: normal;	color: #FFFFFF;	text-align:left;	border-bottom: 1px solid #CCCCCC;		border-right: 1px solid #CCCCCC	;}
            .common_table_list_v30 tr td{margin:0px;padding:5px 3px 5px 3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;	border-right: 1px solid #f1f1f1	;vertical-align:top;}
            .form_table_v30 {width:100%; page-break-inside: avoid;}
            .form_table_v30 tr td.field_title_short{border-bottom:1px solid #FFFFFF ; width:18%; background:#f3f3f3; padding-left:2px; padding-right:2px;}
            .form_table_v30 col.field_title_short{width:18%; }
            .form_table_v30 col.field_content_short{width:25%; }
            .form_table_v30 col.field_photo{width:14%; }
            .form_sub_title_v30{ padding: 1px 3px 1px 3px; margin-top:2px;margin-bottom:2px;border-top:1px solid #CCC; background: #dedede; display:block;}
    	</style>";
	    
	    return $style;
	}
	
	function getPdfStudentDataRecordStudentInfo($studentID)
	{
	    global $Lang, $PATH_WRT_ROOT, $intranet_root;
	    
	    include_once($PATH_WRT_ROOT."includes/libuser.php");
	    
	    $lsr = new libstudentregistry_kv();
	    $result = $lsr->getStudentInfoBasic($studentID);

	    $gender_val = $lsr->getGenderDisplay($result['Gender']);

	    $placeOfBirth = $lsr->returnPresetCodeName("PLACEOFBIRTH", $result['B_PLACE_CODE']);
	    if ($result['B_PLACE_CODE'] == '999') {
	        $placeOfBirth = $result['B_PLACE_OTHERS'];
	    }
	    
	    $nationality = $lsr->returnPresetCodeName("NATIONALITY", $result['NATION_CODE']);
	    if ($result['NATION_CODE'] == '999') {
	        $nationality = $result['NATION_OTHERS'];
	    }
	    
	    $ethnicity = $lsr->returnPresetCodeName("ETHNICITY", $result['ETHNICITY_CODE']);
	    if ($result['ETHNICITY_CODE'] == '999') {
	        $ethnicity = $result['RACE_OTHERS'];
	    }
	    
	    $homeLang = $lsr->returnPresetCodeName("FAMILY_LANG", $result['FAMILY_LANG']);
	    if ($result['FAMILY_LANG'] == '999') {
	        $homeLang = $result['FAMILY_LANG_OTHERS'];
	    }

	    $districtName = $lsr->getPresetCodeEnglishName("SUBDISTRICT", $result['DISTRICT_CODE']);
	    if ($result['DISTRICT_CODE'] == '999') {
	        $districtName = $result['ADDRESS_DISTRICT_EN'];
	    }
	    
	    $englishAddress = '';
	    if ($result['ADDRESS_ROOM_EN']) {
	        $englishAddress .= $Lang['StudentRegistry']['ExportPdf']['EnglishFlat']. ' ' . $result['ADDRESS_ROOM_EN'] . ',';
	    }
	    if ($result['ADDRESS_FLOOR_EN']) {
	        $englishAddress .= $Lang['StudentRegistry']['ExportPdf']['EnglishFloor']. ' ' . $result['ADDRESS_FLOOR_EN'] . ',';
	    }
	    if ($result['ADDRESS_BLK_EN']) {
	        $englishAddress .= $Lang['StudentRegistry']['ExportPdf']['EnglishBlock']. ' ' . $result['ADDRESS_BLK_EN'] . ',';
	    }
	    if ($result['ADDRESS_BLD_EN']) {
	        if ($englishAddress) {
	            $englishAddress .= "<br>";
	        }
	        $englishAddress .= $result['ADDRESS_BLD_EN'];
	    }
	    if ($result['ADDRESS_EST_EN']) {
	        if ($englishAddress) {
	            $englishAddress .= "<br>";
	        }
	        $englishAddress .= $result['ADDRESS_EST_EN'];
	    }
	    if ($result['ADDRESS_STR_EN']) {
	        if ($englishAddress) {
	            $englishAddress .= "<br>";
	        }
	        $englishAddress .= $result['ADDRESS_STR_EN'];
	    }
	    if ($englishAddress && $districtName) {
	        $englishAddress .= "<br>".$districtName;
	    }
	    
	    $addressArea = $Lang['StudentRegistry']['EnglishAreaOptions'][$result['ADDRESS_AREA']-1];
	    if ($englishAddress && $addressArea) {
	        $englishAddress .= "<br>".$addressArea;
	    }
	    $englishAddress = rtrim($englishAddress,',');
	    
	    $li = new libuser($studentID);
	    
	    ########## Official Photo
	    $photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
	    if ($li->PhotoLink !="")
	    {
	        if (is_file($intranet_root.$li->PhotoLink))
	        {
	            $photo_link = $li->PhotoLink;
	        }
	    }
	    $x = '';
	    $x .= '<table class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_photo">
					<tbody>';
	    $x .= '<tr class="form_sub_title_v30">
                    <td colspan="5">
	                   <em>- <span class="field_title">'.$Lang['StudentRegistry']['StudInfo'].'</span> -</em>
                    </td>
              </tr>';
	    
	    // Student basic info
	    $x .= '<tr>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['EnglishName'].'</td>
                    <td colspan="3">
                        '.$result['EnglishName'].'
					</td>
					<td rowspan="5">
                    	<img src="'.$photo_link.'" width="100" height="130">
                	</td>
                </tr>';
	    
	    $x .= '<tr>
                	<td class="field_title_short">'.$Lang['StudentRegistry']['ChineseName'].'</td>
					<td class="chiFont">
	       				'.$result['ChineseName'].'
					</td>
                	<td class="field_title_short">'.$Lang['StudentRegistry']['Class'].'</td>
					<td>'.$result['ClassName'].'</td>
                </tr>';
	    
	    $x .= '<tr>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['BirthDate'].'</td>
                    <td>
                    	'.($result['DOB']=="0000-00-00" ? "" : $result['DOB']).'
                    </td>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['BirthPlace'].'</td>
                    <td class="chiFont">
                    	'.$placeOfBirth.'
                    </td>
                </tr>';
	    
	    $x .= '<tr>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['ID_DocumentNo'].'</td>
                    <td>
                    	'.$result['ID_NO'].'
                    </td>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['Gender'].'</td>
                    <td>
                    	'.$gender_val.'
                    </td>
                </tr>';
	    
	    $x .= '<tr>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['Nationality'].'</td>
                    <td class="chiFont">
                    	'.$nationality.'
                    </td>
                    <td class="field_title_short">'.$Lang['StudentRegistry']['Race'].'</td>
                    <td class="chiFont">
                    	'.$ethnicity.'
                    </td>
                </tr>';
	    
	    $x .= '<tr>
					<td class="field_title_short">'.$Lang['StudentRegistry']['HomeLang'].'</td>
                    <td class="chiFont">
                    	'.$homeLang.'
                    </td>
                    	    
					<td class="field_title_short">'.$Lang['StudentRegistry']['HomePhone'].'</td>
                    <td>
                    	'.$result['HomeTelNo'].'
                    </td>
                </tr>';
	    
	    $x .= '<tr>
					<td class="field_title_short">'.$Lang['StudentRegistry']['Address'].'<br> ('.$Lang['StudentRegistry']['inEnglish'].')</td>
                    <td colspan="4" class="chiFont">
                    	'.$englishAddress.'
                    </td>
                </tr>';
	    
	    $x .= '<tr>
					<td class="field_title_short">'.$Lang['StudentRegistry']['Address'].'<br> ('.$Lang['StudentRegistry']['inChinese'].')</td>
                    <td colspan="4" class="chiFont">
                    	'.$result['ADDRESS_STR_CH'].'
                    </td>
                </tr>';
	    
	    $x .= '<tr>
					<td class="field_title_short">'.$Lang['StudentRegistry']['SchoolBus'].'</td>
                    <td>
                    	'.$result['SCHOOL_BUS'].'
                    </td>
                    	    
					<td class="field_title_short">'.$Lang['StudentRegistry']['NannyBus'].'</td>
                    <td class="chiFont">
                    	'.$result['NANNY_BUS'].'
                    </td>
                    <td>&nbsp;</td>
                </tr>';
	    
	    $x .= '</tbody>';
	    $x .= '</table>';
	    
	    // former school attended
	    $y = '<table class="form_table_v30">
    			    <tbody>';
	    $y .= '<tr class="form_sub_title_v30">
                <td colspan="2">
	               <em>- <span class="field_title">'.$Lang['StudentRegistry']['LastSchool'].'</span> -</em>
                </td>
              </tr>';
	    
	    $y .= '<tr>
                	<td class="field_title_short">'.$Lang['StudentRegistry']['LastSchoolName'].'</td>
					<td class="chiFont">
	       				'.$result['LAST_SCHOOL_EN'].'
					</td>
                </tr>';
	    $y .= '</tbody>';
	    $y .= '</table>';
	    
	    return array('StudentInfo'=>$x, 'FormerSchool'=>$y);
	}

	function getPdfStudentDataRecordParentInfo($studentID)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    
	    $x = '';
	    for($g=1;$g<=2;$g++)
	    {
	        $g_result = $lsr->getGuardianInfo($studentID, $g);        // $g = 1 <- father, g = 2 <- mother
	        
	        $districtName = $lsr->getPresetCodeEnglishName("SUBDISTRICT", $g_result['DISTRICT_CODE']);
	        if ($g_result['DISTRICT_CODE'] == '999') {
	            $districtName = $g_result['ADDRESS_DISTRICT_EN'];
	        }
	        
    	    $englishAddress = '';
    	    if ($g_result['ADDRESS_ROOM_EN']) {
    	        $englishAddress .= $Lang['StudentRegistry']['ExportPdf']['EnglishFlat']. ' ' . $g_result['ADDRESS_ROOM_EN'] . ',';
    	    }
    	    if ($g_result['ADDRESS_FLOOR_EN']) {
    	        $englishAddress .= $Lang['StudentRegistry']['ExportPdf']['EnglishFloor']. ' ' . $g_result['ADDRESS_FLOOR_EN'] . ',';
    	    }
    	    if ($g_result['ADDRESS_BLK_EN']) {
    	        $englishAddress .= $Lang['StudentRegistry']['ExportPdf']['EnglishBlock']. ' ' . $g_result['ADDRESS_BLK_EN'] . ',';
    	    }
    	    if ($g_result['ADDRESS_BLD_EN']) {
    	        if ($englishAddress) {
    	            $englishAddress .= "<br>";
    	        }
    	        $englishAddress .= $g_result['ADDRESS_BLD_EN'];
    	    }
    	    if ($g_result['ADDRESS_EST_EN']) {
    	        if ($englishAddress) {
    	            $englishAddress .= "<br>";
    	        }
    	        $englishAddress .= $g_result['ADDRESS_EST_EN'];
    	    }
    	    if ($g_result['ADDRESS_STR_EN']) {
    	        if ($englishAddress) {
    	            $englishAddress .= "<br>";
    	        }
    	        $englishAddress .= $g_result['ADDRESS_STR_EN'];
    	    }
    	    if ($englishAddress && $districtName) {
    	        $englishAddress .= "<br>".$districtName;
    	    }
    	    
    	    $addressArea = $Lang['StudentRegistry']['EnglishAreaOptions'][$g_result['ADDRESS_AREA']-1];
    	    if ($englishAddress && $addressArea) {
    	        $englishAddress .= "<br>".$addressArea;
    	    }
    	    
    	    $englishAddress = rtrim($englishAddress,',');
	    
    	    $dispAlumni = $g_result['IS_ALUMNI'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
    	    $graduateYear = $g_result['GRADUATE_YEAR'];
    	    if ($graduateYear) {
    	        $dispAlumni .= '/'. $graduateYear;
    	    }
    	    
    	    $x .= '<table class="form_table_v30">
    					<tbody>';
    	    $x .= '<tr class="form_sub_title_v30">
                        <td colspan="6">
    	                   <em>- <span class="field_title">'.(($g==1) ?$Lang['StudentRegistry']['FatherInfo']:$Lang['StudentRegistry']['MotherInfo']).'</span> -</em>
                        </td>
                  </tr>';
    	    
    	    // parent info
    	    $x .= '<tr>
                        <td class="field_title_short">'.$Lang['StudentRegistry']['EnglishName'].'</td>
                        <td colspan="2">
                            '.$g_result['NAME_E'].'
    					</td>
                    	<td class="field_title_short">'.$Lang['StudentRegistry']['ChineseName'].'</td>
    					<td class="chiFont" colspan="2">
    	       				'.$g_result['NAME_C'].'
    					</td>
                    </tr>';
    	    
    	    $x .= '<tr>
                    	<td class="field_title_short">'.$Lang['StudentRegistry']['BusinessName'].'</td>
    					<td colspan="2" class="chiFont">
    	       				'.$g_result['BUSINESS_NAME'].'
    					</td>
                    	<td class="field_title_short">'.$Lang['StudentRegistry']['ParentEMailAddress'].'</td>
    					<td colspan="2">'.$g_result['EMAIL'].'</td>
                    </tr>';
    	    
    	    $x .= '<tr>
                        <td class="field_title_short">'.$Lang['StudentRegistry']['BusinessAddress'].'</td>
                        <td colspan="5" class="chiFont">
                        	'.$englishAddress.'
                        </td>
                    </tr>';
    	    
    	    $x .= '<tr>
                        <td class="field_title_short">'.$Lang['StudentRegistry']['JobTitle'].'</td>
                        <td class="chiFont">
                        	'.$g_result['JOB_TITLE'].'
                        </td>
                        <td class="field_title_short">'.$Lang['StudentRegistry']['Job'].'</td>
                        <td class="chiFont">
                        	'.$g_result['JOB_NATURE'].'
                        </td>
                        <td class="field_title_short">'.$Lang['StudentRegistry']['Alumni'].'</td>
                        <td>
                        	'.$dispAlumni.'
                        </td>
                    </tr>';
    	    
    	    $x .= '<tr>
    					<td class="field_title_short">'.$Lang['StudentRegistry']['GuardianDayPhone'].'</td>
                        <td colspan="2">
                        	'.$g_result['TEL'].'
                        </td>
                        	    
    					<td class="field_title_short">'.$Lang['StudentRegistry']['MobilePhoneNo'].'</td>
                        <td colspan="2">
                        	'.$g_result['MOBILE'].'
                        </td>
                    </tr>';
    	    
    	    $x .= '</tbody>';
    	    $x .= '</table>';
	    }
	    return $x;
	}
	
	// siblings info
	function getPdfStudentDataRecordSiblingsInfo($studentID)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    $brotherAndSisterAry = $lsr->getBroterAndSister($studentID);
	    $nrBrotherAndSister = count($brotherAndSisterAry);
	    
	    if($nrBrotherAndSister) {
	        $viewTable = "<table id='brothersAndSistersTable' class='common_table_list_v30' width='100%'>";
	        $viewTable .= "<tr>";
	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['RelationshipAge']."</th>";
	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['Name']."</th>";
	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['KentvilleClass']."</th>";
	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['ExKentvilleClassYear']."</th>";
	        $viewTable .= "<th>".$Lang['StudentRegistry']['Sibling']['SchoolLevel']."</th>";
	        $viewTable .= "</tr>";
	        
	        foreach($brotherAndSisterAry as $_brotherAndSisterAry){
	            
	            $_relationship = $lsr->returnPresetCodeName("SIBLINGS", $_brotherAndSisterAry['Relationship']);
	            
	            $disRelationAge = $_relationship;
	            if ($_brotherAndSisterAry['Age']) {
	                $disRelationAge .= ' ('. $_brotherAndSisterAry['Age'].')';
	            }
	            $_isSchoolStudentOrAlumni = $_brotherAndSisterAry['IsSchoolStudentOrAlumni'];
	            $_schoolClass = $_brotherAndSisterAry['SchoolClass'];
	            $_graduateYear = $_brotherAndSisterAry['GraduateYear'];
	            if ($_isSchoolStudentOrAlumni && $_graduateYear > 0) {
	                $disCurClass = '--';
	                $disExClassAndYear = $_schoolClass . ' (' . $_graduateYear.')';
	            }
	            else {
	                $disCurClass = $_schoolClass ? $_schoolClass : '--';
	                $disExClassAndYear = '--';
	            }
	            $disCurSchoolAndLevel = $_brotherAndSisterAry['CurSchool'] ? $_brotherAndSisterAry['CurSchool'] : '--';
	            
	            $disCurLevel = $lsr->returnPresetCodeName("SCHOOLLEVEL", $_brotherAndSisterAry['CurLevel']);
	            if ($_brotherAndSisterAry['CurLevel'] == '999') {
	                $disCurLevel = $_brotherAndSisterAry['CurLevelOthers'];
	            }
	            
	            if ($disCurLevel) {
	                $disCurSchoolAndLevel .= ' / '. $disCurLevel;
	            }
	            
	            $viewTable .= "<tr>";
	            $viewTable .= "<td>". $disRelationAge."</td>";
	            $viewTable .= "<td class='chiFont'>". $_brotherAndSisterAry['StudentName']."</td>";
	            $viewTable .= "<td>". $disCurClass."</td>";
	            $viewTable .= "<td>". $disExClassAndYear."</td>";
	            $viewTable .= "<td>". $disCurSchoolAndLevel."</td>";
	            $viewTable .= "</tr>";
	        }
	        $viewTable .= "</table>";
	    }
	    else {
	        $viewTable = $Lang['General']['NoRecordAtThisMoment'];
	    }
	    
	    
	    $x .= '<table class="form_table_v30">
    			    <tbody>';
	    $x .= '<tr class="form_sub_title_v30">
                    <td>
	                   <em>- <span class="field_title">'.$Lang['StudentRegistry']['BrotherSisterInfo'].'</span> -</em>
                    </td>
              </tr>';
	    
	    $x .= '<tr>
					<td>
	       				'.$viewTable.'
					</td>
                </tr>';
	    $x .= '</tbody>';
	    $x .= '</table>';
	    
	    return $x;
	}
	
	function getPdfStudentDataRecordEmergencyContactInfo($studentID)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    $contact_result = $lsr->getEmergencyContactInfo($studentID);
	    $nrContactResult = count($contact_result);
	    
        $x = '';
        $x .= '<table class="form_table_v30">
    			    <tbody>';
        $x .= '<tr class="form_sub_title_v30">
                    <td colspan="4">
	                   <em>- <span class="field_title">'.$Lang['StudentRegistry']['EmergencyContactPersonInfo'].'</span> -</em>
                    </td>
              </tr>';
        
        for($i=0;$i<$nrContactResult;$i++)
        {
            $_contact_result = $contact_result[$i];
            
            $_relationship = $lsr->returnPresetCodeName("RELATIONSHIP",  $_contact_result['Contact3_RelationshipCode']);
            if ($_contact_result['Contact3_RelationshipCode'] == '999') {
                $_relationship = $_contact_result['Contact3_Relationship'];
            }
            
            $x .= '<tr>
                        <td class="field_title_short">'.$Lang['StudentRegistry']['EmergencyContact'].'('.($i+1).')<br>('.$Lang['StudentRegistry']['NonParent'].')</td>
                        <td>('.$Lang['StudentRegistry']['inEnglish'].')
                            '.$_contact_result['Contact3_EnglishName'].'
    					</td>
                    	<td class="field_title_short">'.$Lang['StudentRegistry']['ChineseName'].'</td>
    					<td class="chiFont">
    	       				'.$_contact_result['Contact3_ChineseName'].'
    					</td>
                    </tr>';
            
            $x .= '<tr>
					<td class="field_title_short">'.$Lang['StudentRegistry']['GuardianStudentRelationship'].'</td>
                    <td>
                    	'.$_relationship.'
                    </td>
                    	    
					<td class="field_title_short">'.$Lang['StudentRegistry']['ContactPhone'].'</td>
                    <td>
                    	'.$_contact_result['Contact3_Mobile'].'
                    </td>
                </tr>';
        }
        if ($nrContactResult == 0) {
            $x .= '<tr><td>'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
        }
        $x .= '</tbody>';
        $x .= '</table>';
        
        return $x;
	}
	
	function getPdfStudentDataRecord($studentID)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    $x = '';
	    $studentInfoAry = $this->getPdfStudentDataRecordStudentInfo($studentID);
	    $x .= $studentInfoAry['StudentInfo'];
	    $x .= $this->getPdfStudentDataRecordParentInfo($studentID);
	    $x .= $studentInfoAry['FormerSchool'];
	    $x .= $this->getPdfStudentDataRecordSiblingsInfo($studentID);
	    $x .= $this->getPdfStudentDataRecordEmergencyContactInfo($studentID);
	    
	    return $x;
	}
	
	function getPdfStudentDataRecordHeader($studentID, $reportTitle)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    $studentInfo = $lsr->getStudentKeyInfo($studentID);
	    
	    $x = '	<table width="100%" border="0" class="form_table_v30">
					<tr>
						<td colspan="6" class="schoolName">'.$Lang['StudentRegistry']['ExportPdf']['SchoolName'].'</td>
					</tr>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
            	    <tr>
            	        <td class="field_title_short">'.$Lang['StudentRegistry']['StudentName'].'</td>
            			<td>'.Get_Lang_Selection($studentInfo['ChineseName'],$studentInfo['EnglishName']).'</td>
            			<td class="field_title_short">'.$Lang['StudentRegistry']['StudentIDNo'].'</td>
            			<td>'.$studentInfo['UserLogin'].'</td>
            			<td class="field_title_short">'.$Lang['StudentRegistry']['Date'].'</td>
            			<td>'.date("Y-m-d").'</td>
            		</tr>

					<tr>
						<td colspan="6" class="reportTitle">'.$reportTitle.'</td>
					</tr>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
			  	</table>';
        return $x;    
	}
	
	function getPresetCodeList($codeType)
	{
	    global $Lang;
	    
	    $lsr = new libstudentregistry_kv();
	    $codeListAry = $lsr->getPresetCodeList($codeType);
	    
	    $x = '<table class="common_table_list_v30">';
	       $x .= '<tr>
				       <th>'.$Lang['General']['Code'].'</th>
                       <th>'.$Lang['General']['Name'].'</th>
			      </tr>'."\n";
	    for($i=0,$iMax=count($codeListAry);$i<$iMax;$i++) {
	        $x .= '<tr>
				       <td>'.$codeListAry[$i]['Code'].'</td>
                       <td>'.$codeListAry[$i]['Name'].'</td>
			      </tr>'."\n";
	    }
	       
	    $x .= '</table>';

        return $x;	    
	}
	// end functions for Kentville
	///////////////////////////////////////////////////////////////////////////////////////////////
	
}
?>