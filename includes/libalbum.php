<?php
/*
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 * 
 */
class libalbum extends libdb{
	var $Album;
	var $AlbumID;
	var $ParentID;
	var $isLeafNote;
	var $Name;
	var $Description;
	var $FilePath;
	var $DisplayOrder;
	var $OwnerGroup;
	var $LastChangeByUserID;
	var $AccessType;
	var $UserAllowedList;
	var $GroupAllowedList;
	var $NumberOfItems;
	var $ViewCount;	
	var $LastModified;
	var $ThumbnailID;
	var $DateInput;
	var $db_table = "INTRANET_PHOTO_ALBUM";
	
	var $navbar;
	var $album = array();
	var $photo_order;
	var $photo_ids = array();
	var $unsupport_file = array();
	var $photo_now = array();
	var $photo_limit = array(640, 640);
	var $is_view_only;
	var $year_selected;
	
	function libalbum($AlbumID="") {
		$this->libdb();
        if($AlbumID<>""){
                $this->Album = $this->returnAlbum($AlbumID);
                $this->AlbumID = $this->Album[0][0];
                $this->ParentID = $this->Album[0][1];
                $this->isLeafNote = $this->Album[0][2];
                $this->Name = $this->Album[0][3];
                $this->Description = $this->Album[0][4];
                $this->DisplayOrder = $this->Album[0][5];
                $this->OwnerGroup = $this->Album[0][6];
                $this->LastChangeByUserID = $this->Album[0][7];
                $this->AccessType = $this->Album[0][8];
                $this->UserAllowedList = $this->Album[0][9];
                $this->GroupAllowedList = $this->Album[0][10];
                $this->NumberOfItems = $this->Album[0][11];
                $this->LastModified  = $this->Album[0][12];
                $this->ThumbnailID   = $this->Album[0][13];
                $this->DateInput  = $this->Album[0][14];
                $this->LastModified  = $this->Album[0][15];
                $this->FilePath  = $this->Album[0][16];
        }
	}	
	
	function returnAlbumName(){
		return $this->Name;
	}
	
	function returnDisplayOrder(){
		return $this->DisplayOrder;
	}
	
	function returnDescription(){
		return $this->Description;
	}
			
	function getNumberOfItems() {
		return $this->NumberOfItems;
	}
			
	function returnAlbumList(){
		$album_list = Array();		
		
		if ($this->Name != "" && $this->FilePath != "") {				
#			echo $this->Name;
#			echo $this->FilePath;
			($this->isLeafNote == 0) ? array_unshift($album_list, $this->Name, '/admin/photoalbum/index.php?album_id='.$this->AlbumID) : array_unshift($album_list, $this->Name, '/admin/photoalbum/list_photo.php?album_id='.$this->AlbumID.'&count='.$this->NumberOfItems);		
		}		
		
		$this->getParentNode($this->ParentID, $album_list);	
		
/*		echo $album_list[0];
		echo $album_list[1];
		echo $album_list[2];
		echo $album_list[3];*/
		
		return $album_list;
		
	}
	
	function returnAccessType() {
		return $this->AccessType;
	}
	
	function returnUserAllowedList() {		
		$sql = "SELECT EnglishName, UserID FROM INTRANET_USER WHERE UserID IN (".$this->UserAllowedList.")";				
		$row = $this->returnArray($sql, 2);	
		return $row;
	}
	
	function getParentNode($ParentID, &$list) {		
		$query = "SELECT AlbumName, CONCAT('index.php?album_id=', AlbumID), ParentID FROM $this->db_table WHERE AlbumID='$ParentID'";									
		$temp = $this->returnArray($query, 3);
	
		if (sizeof($temp) > 0) {		
			array_unshift ($list, $temp[0][0], $temp[0][1]);		
		}		
		if ($temp[0][2] != 0) {			
			$this->getParentNode($temp[0][2], $list);
		}
	}
		
	
	# navigation bar for Admin Console, Overwrite displayNavTitle in libdb
	function displayNavTitle() {
	        $x = "";
	        $y = "";
	
	        $numargs = func_num_args();
	   
	        for ($i=0; $i<$numargs; $i++) {		        
		        	$title = func_get_arg($i);	                
	                if (is_array($title)) {
		                break;
	                } 
	                $y .= ($i>1) ? "<img src='/images/arrow.gif' width='7' height='9' hspace='5'>" : "";	                
	                
	                $link = func_get_arg(++$i);
	                $y .= (trim($link)=="") ? "<span class='admin_title'>$title</span>" : "<a href='$link' class='admin_title'>$title</a>";
	        }        
	        
	        $arr = func_get_arg($i);	        
	        
	        for ($j=0; $j<sizeof($arr); $j++) {		        
		            $y .= "<img src='/images/arrow.gif' width='7' height='9' hspace='5'>";
	                $title = $arr[$j];
	                $link = $arr[++$j];	                
	                $y .= (trim($link)=="") ? "<span class='admin_title'>$title</span>" : "<a href='$link' class='admin_title'>$title</a>";
	            }
	        
	            
	            if ($numargs > ++$i) {
		            $title = func_get_arg($i);		      
		            $y .= "<img src='/images/arrow.gif' width='7' height='9' hspace='5'>";				         		            
		            $link = func_get_arg(++$i);
		            $y .= (trim($link)=="") ? "<span class='admin_title'>$title</span>" : "<a href='$link' class='admin_title'>$title</a>";
	            }
		
	        $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>\n";
	        $x .= "<tr><td height='34'><b>\n";
	        $x .= $y;
	        $x .= "</b></td></tr></table>\n";
	
	        return $x;
	}

		
		
/*
	function displayTreeView($albumID=0, $level=0){
		$query = "SELECT AlbumID, ParentID, IsLeafNote, AlbumName, DisplayOrder FROM $this->db_table WHERE ParentID=$albumID ORDER BY DisplayOrder ASC";		
		$temp = $this->returnArray($query, 5);
		
		for ($i=0; $i<sizeof($temp); $i++) {		
			for ($j=0; $j<=$level*3; $j++) {
				echo "&nbsp;";
			}	
			echo "<a class=tableContentLink href='listview.php?AlbumID={$temp[$i][0]}&leafnote={$temp[$i][2]}'>{$temp[$i][3]}</a><br>";			
			// not leave node, display child nodes
			if ($temp[$i][2] == 0) {				
				$this->displayTreeView($temp[$i][0], $level+1);
			}
		}
	}	
*/

	# return album info	
    function returnAlbum($AlbumID){
        $sql = "SELECT AlbumID, ParentID, IsLeafNote, AlbumName, Description, DisplayOrder, OwnerGroupID, LastChangeByUserID, AccessType, UserAllowedList, GroupAllowedList, NumberOfItems, ViewCount, ThumbnailID, DateInput, DateModified, FilePath FROM $this->db_table WHERE AlbumID = '$AlbumID'";
        return $this->returnArray($sql,17);
	}
	
	# Return File Path
	function getAlbumPathFile($album_id){		
	    $path = "/file/album/";  
	    $sql = "SELECT FilePath FROM INTRANET_PHOTO_ALBUM WHERE AlbumID='".$album_id."'";
	    $folderpath = $this->returnVector($sql);    
	    
		return $path.$folderpath[0];		
	}
	
	# Check image format (.jpg, .jpe, .gif, .png, .bmp)	
	function checkImageFormat($filename){
		$fileType = strtoupper(strrchr($filename, "."));

		if ($fileType==".JPG" || $fileType==".JPE" || $fileType==".GIF" ||
			$fileType==".PNG" || $fileType==".BMP") {
			return true;
		} else {
			$this->unsupport_file[] = $filename;
			return false;
		}
	}
	
	# Return Used Storage Quota
	function returnAlbumStorageQuota()
	{
		$sql = "SELECT StorageQuota, UsedQuota FROM INTRANET_PHOTO_ALBUM_GROUP_SETTING WHERE GroupID='$this->OwnerGroup'";
		$row = $this->returnArray($sql, 2);
		return $row[0];		
	}	
	
	function updateStorageQuota($filesize)	
	{		
		$sql = "UPDATE INTRANET_PHOTO_ALBUM_GROUP_SETTING SET UsedQuota=UsedQuota+".($filesize)." WHERE GroupID='".$this->OwnerGroup."'";				
		$this->db_db_query($sql);		
	}	
	
	function loadPhotoOrder($album_id, $photo_id){
		$sql = ($photo_id!="") ? "SELECT DisplayOrder FROM INTRANET_PHOTO_ITEM WHERE AlbumID='$album_id' AND ItemID='$photo_id'" : "SELECT Max(DisplayOrder) FROM INTRANET_PHOTO_ITEM WHERE AlbumID='$album_id'";
		$row = $this->returnArray($sql, 1);
		$this->photo_order = $row[0][0]+1;		
	}	
		  
	function addPhoto2Album($album_id, $photo_id, $filename, $photo_description, $filepath, $filesize){
		if (trim($filename)=="" || !$this->checkImageFormat($filename))
		{
			return;
		}

		if ($this->photo_order=="")
		{
			$this->loadPhotoOrder($album_id, $photo_id);
		}
		
		$file_name = intranet_htmlspecialchars(trim($filename));		
		$file_size = intranet_htmlspecialchars(trim($filesize));
		$file_path = intranet_htmlspecialchars(trim($filepath));
		$photo_description = intranet_htmlspecialchars(trim($photo_description));
		$order = $this->photo_order + sizeof($this->photo_ids);	

		$fieldname = "AlbumID, Description, DisplayOrder, FileName, Path, FileSize, DateInput, DateModified, ViewCount";
		$fieldvalues = "'$album_id', '$photo_description', $order, '$file_name', '$file_path', '$file_size', now(), now(), 0 ";

		$sql  = "INSERT INTO INTRANET_PHOTO_ITEM ($fieldname) values ($fieldvalues)";		
		$this->db_db_query($sql);		
		$new_id = $this->db_insert_id();
		if ($new_id)
		{
			$this->photo_ids[] = $new_id;
			$this->AlbumID = $album_id;
			$this->NumberOfItems++;
			$this->updateAlbumItemCount();
		}

		return;
	}	
	
	function updatePhotoOrder(){
		$increment = sizeof($this->photo_ids);
		if ($increment>0)
		{
			$photo_list = implode(",", $this->photo_ids);
			$sql = "UPDATE INTRANET_PHOTO_ITEM SET DisplayOrder=DisplayOrder+$increment WHERE AlbumID='".$this->AlbumID."' AND ItemID NOT IN (".$photo_list.") AND DisplayOrder>=".$this->photo_order;
			$this->db_db_query($sql);
		}
		return ;
	}	
	
	function updateAlbumDate(){
		$sql = "UPDATE $this->db_table SET DateModified=now() WHERE AlbumID='".$this->AlbumID."'";
		$this->db_db_query($sql);

		return;
	}	
	
	function updateAlbumItemCount() {
		$sql = "UPDATE $this->db_table SET NumberOfItems=NumberOfItems+1 WHERE AlbumID='".$this->AlbumID."'";
		$this->db_db_query($sql);
		return;
	}

	function updatePhotoViewCount($ItemID) {
		$sql = "UPDATE INTRANET_PHOTO_ITEM SET ViewCount=ViewCount+1 WHERE ItemID='".$ItemID."'";		
		$this->db_db_query($sql);
	}
	
	function isLeafNote($album_id) {
		$sql = "SELECT isLeafNote FROM $this->db_table WHERE AlbumID='$album_id'";
		$row = $this->returnVector($sql);
		return $row[0];
	}
	
	function updateFirstPhoto($album_id){
		$sql = "SELECT Min(DisplayOrder), ItemID FROM INTRANET_PHOTO_ITEM WHERE AlbumID='".$album_id."' GROUP BY AlbumID ";
		$row = $this->returnArray($sql,2);
		if ($row[0][0]!=1)
		{
			$sql = "UPDATE INTRANET_PHOTO_ITEM SET DisplayOrder=1 WHERE ItemID='".$row[0][1]."'";
			$this->db_db_query($sql);
		}

		return;
	}

	function removePhotos($photo_ids, $album_id){
		global $lf;
		global $file_path;		
		
		$sql = "SELECT Path, FileName, FileSize FROM INTRANET_PHOTO_ITEM WHERE ItemID IN ($photo_ids) ";
		$row = $this->returnArray($sql, 3);		
		
		// remove physical files
		for ($i=0; $i<sizeof($row); $i++)
		{
			$photo_file = $file_path.$row[$i][0].$row[$i][1];			
			$lf->item_remove($photo_file);
			
			# Update used storage quota for Group Album			
			if (isset($this->OwnerGroup) && $this->OwnerGroup != "")
			{	
				$this->updateStorageQuota(-($row[$i][2]));
			}
		}

		// remove DB
		$sql = "DELETE FROM INTRANET_PHOTO_ITEM WHERE ItemID IN ($photo_ids) ";
		$this->db_db_query($sql);
		
		// update parent
		$size = sizeof (explode(",", $photo_ids));		
		$sql = "UPDATE $this->db_table SET NumberOfItems=NumberOfItems-$size WHERE AlbumID='".$album_id."'";		
		$this->db_db_query($sql);		
		
		// update this->count
		$this->NumberOfItems -= $size;
	}
		
	function removeAlbum($album_id){
		global $lf;
		global $file_path;				
				
		if (!$this->isLeafNote($album_id)) {		
			# Remove childs
			$sql = "SELECT AlbumID FROM $this->db_table WHERE ParentID='$album_id'";
			$row = $this->returnVector($sql);
	
			for ($i=0; $i<sizeof($row); $i++) {		
				$this->removeAlbum($row[$i]);
			}
			
			# Remove physical folder
			$folder = $file_path.$this->getAlbumPathFile($album_id);		
			$lf->folder_remove($folder);			            		
		} else {			
			# Remove all physical files						
			$sql = "SELECT Path, FileName FROM INTRANET_PHOTO_ITEM WHERE AlbumID='$album_id'";
			$row = $this->returnArray($sql, 2);				
	
			for ($i=0; $i<sizeof($row); $i++) {			
				$file = $file_path.$row[$i][0].$row[$i][1];				
			//	echo "remove file : ".$file."<br>";
				$lf->item_remove($file);				
			}				
			$folder = $file_path.$this->getAlbumPathFile($album_id);		
	//		echo "remove folder : ".$folder."<br>";
			$lf->item_remove($folder);
			
			$sql = "SELECT SUM(FileSize), b.OwnerGroupID FROM INTRANET_PHOTO_ITEM AS a
						INNER JOIN $this->db_table AS b ON a.AlbumID=b.AlbumID
						INNER JOIN INTRANET_PHOTO_ALBUM_GROUP_SETTING AS c ON b.OwnerGroupID=c.GroupID						
						WHERE a.AlbumID='$album_id'
						GROUP BY a.AlbumID";						
			$row = $this->returnArray($sql, 2);
			if (sizeof($row) > 0)
			{
				$this->OwnerGroup = $row[0][1];
				$this->updateStorageQuota(-($row[0][0]));			
			}
			
			# Remove photos
			$sql = "DELETE FROM INTRANET_PHOTO_ITEM WHERE AlbumID='$album_id'";
			$this->db_db_query($sql);	
		}		
		
		# Update Datebase of Parent Node
		$this->updateParent($album_id);		
						
		# Update Database
		$sql = "DELETE FROM $this->db_table WHERE AlbumID='$album_id'";
		$this->db_db_query($sql); 		
	}
	
	function updateParent($album_id) {		
		# Get Parent Node
		$sql = "SELECT ParentID FROM $this->db_table WHERE AlbumID='$album_id'";
		$row = $this->returnVector($sql);				
		
		# Have Parent
		if ($row[0] != 0) {
			$sql = "SELECT count(distinct AlbumID) AS cnt FROM $this->db_table WHERE ParentID='".$row[0]."'";
			$count = $this->returnVector($sql);					
			
			$update_query = Array();
			
			if ($count[0] == 1) {
				array_push($update_query, "NumberOfItems=0");
				array_push($update_query, "isLeafNote=1");
			} else {
				array_push($update_query, "NumberOfItems=NumberOfItems-1");
			}
			array_push($update_query, "DateModified=NOW()");
			$query = "UPDATE $this->db_table SET ";
			$query .= implode (",", $update_query);
			$query .= " WHERE AlbumID='".$row[0]."'";			
			
			$this->db_db_query($query);			
		}	
	}

	function loadPhoto($photo_id){
		$sql = "SELECT AlbumID, FileName, Description, DisplayOrder, DateModified, Path FROM INTRANET_PHOTO_ITEM WHERE ItemID='$photo_id'";
		$row2 = $this->returnArray($sql, 6);
		$row = $row2[0];
		$this->photo_now['id'] = $photo_id;
		$this->photo_now['album_id'] = $row[0];
		$this->photo_now['file_name'] = $row[1];
		$this->photo_now['description'] = $row[2];
		$this->photo_now['order'] = $row[3];
		$this->photo_now['datemodified'] = $row[4];
		$this->photo_now['path'] = $row[5];

		return;
	}		
	
	function returnPhotoImg(){
		global $file_path;
		list($photo_file, $img_size, $is_popup) = $this->getPhotoParam($this->photo_now['file_name'], $this->photo_now['album_id']);			
		
		#$rx = "<img src='".$photo_file."' $img_size border='0'>";		
		$rx = "<img src='/includes/imagethumbnail.php?image=$photo_file' border=0>";
		
		if ($is_popup)
		{		
			$rx = "<a href=\"javascript:viewPhoto(".$this->photo_now['id'].")\">".$rx."<br>".$this->photo_now['file_name']."</a>";
		}
		
		return $rx;
	}	

	function getPhotoParam($image, $album_id){
		global $file_path;
		$photo_path = $file_path.$this->getAlbumPathFile($album_id).$image;		
	
		if (file_exists($photo_path))
		{
			$photo_size = GetImageSize($photo_path);
			$is_width_larger = ($photo_size[0]>=$photo_size[1]);

			if ($is_width_larger)
			{
				$img_size = "width=";
				$img_size .= ($photo_size[0]>=$this->photo_limit[0]) ? $this->photo_limit[0] : $photo_size[0];
			} else
			{
				$img_size = "height=";
				$img_size .= ($photo_size[1]>=$this->photo_limit[1]) ? $this->photo_limit[1] : $photo_size[1];
			}		
			$is_popup = ($photo_size[0]>$this->photo_limit[0] || $photo_size[1]>$this->photo_limit[1]);			
			$photo_file = $this->getAlbumPathFile($album_id).$image;								
		}
		return array($photo_file, $img_size, $is_popup);
	}
	
	function returnPhotoDescription($is_edit=0){
		$rx = ($is_edit) ? $this->photo_now['description'] : nl2br($this->photo_now['description']);
		return $rx;
	}
				
	function updatePhoto($photo_id, $photo_description){
		$photo_description = htmlspecialchars(trim($photo_description));
		$sql = "UPDATE INTRANET_PHOTO_ITEM SET Description='$photo_description' WHERE ItemID='".$photo_id."' ";
		$this->db_db_query($sql);		
	}	
	
	function swapPhotos($pidA, $pidB){
		$sql = "SELECT ItemID, DisplayOrder FROM INTRANET_PHOTO_ITEM WHERE ItemID='".$pidA."' OR ItemID='".$pidB."' ";
		$row = $this->returnArray($sql, 2);
		if (sizeof($row)!=2)
		{
			return;
		}

		// swap order
		$sql = "UPDATE INTRANET_PHOTO_ITEM SET DisplayOrder='".$row[1][1]."' WHERE ItemID='".$row[0][0]."' ";
		$this->db_db_query($sql);

		$sql = "UPDATE INTRANET_PHOTO_ITEM SET DisplayOrder='".$row[0][1]."' WHERE ItemID='".$row[1][0]."' ";
		$this->db_db_query($sql);			
	}	
	
	function setThumbnail($photo_id) {	
		$sql = "UPDATE $this->db_table SET ThumbnailID=$photo_id WHERE AlbumID='".$this->AlbumID."'";
		$this->db_db_query($sql);		
	}
	
	function updateThumbnail() {
		$sql = "SELECT count(*) FROM $this->db_table AS a INNER JOIN INTRANET_PHOTO_ITEM AS b ON a.ThumbnailID=b.ItemID
					WHERE AlbumID={$this->AlbumID}";
		$row = $this->returnVector($sql);
/*
		$sql = "SELECT ThumbnailID FROM $this->db_table WHERE AlbumID='".$this->AlbumID."'";
		$row = $this->returnVector($sql);
*/		
		# No Thumbnail set , or lost set the first one
		if ($row[0] == 0) {
			$query = "SELECT ItemID FROM INTRANET_PHOTO_ITEM WHERE AlbumID='".$this->AlbumID."' ORDER BY DisplayOrder ASC";
			$result = $this->returnVector($query);
			$query = "UPDATE $this->db_table SET ThumbnailID='".$result[0]."' WHERE AlbumID='".$this->AlbumID."'";
			$this->db_db_query($query);
		}
	}		
	
	function returnParentID ($album_id) 
	{
		$sql = "SELECT ParentID FROM $this->db_table WHERE AlbumID='".$album_id."'";
		$row = $this->returnVector($sql);
		return $row[0];
	}		
	
	#----------------Handle File Copy------------------------------------------------
	# recursively convert old file structure to the new one
	function IO2DB($src, $dest)
	{		
		$ThisDir = array();					//Create array for current directories contents
		$ThisFile = array();
		$ThisLocation = array();
		
		if (!file_exists($src))
		{			
			return;
		}		
		
		global $file_path;	
		$current = $src;
		$handle = opendir($src);		
		while ($file = readdir($handle))
		{
			if ($file != '.' && $file != '..')
			{
				$source = $current."/".$file;
	#			debug($source);
				#$fileNew = $file;
				
				if (is_dir($source))
				{
					array_push($ThisDir, $source);
				}
				else
				{
					if (file_exists($source))
					{						
						copy($source, $file_path.$dest.$file);
						$FileSize = ceil((filesize($source))/1024);
						if ($FileSize < 1)
						{
							$FileSize = 1;
						}
						$this->addPhoto2Album($this->AlbumID, "", addslashes($file), "", $dest, $FileSize);
						$this->updateStorageQuota($FileSize);						
					}
				}
			}
		}
		closedir($handle);
		
		for ($i=0; $i<sizeof($ThisDir); $i++)
		{
			$this->IO2DB($ThisDir[$i], $dest);
		}
	}
	
	#-------------Function for Group Admin-------------------------------------------#
	
	function returnSelectAccessType($tag,$noID=false, $all=0, $selected="")
	{					
		$cats = array ( array("1", "Internal Group only"), array("2", "any intranet users"), array("3", "public"));						
		return getSelectByArray($cats,$tag,$selected,$all);
	}	
	
	function returnStorageQuota($group_id)
	{
		$sql = "SELECT StorageQuota FROM INTRANET_PHOTO_ALBUM_GROUP_SETTING WHERE GroupID='$group_id'";	
		$row = $this->returnVector($sql);
		return $row[0];
	}
	function returnAlowedAccessType($group_id)
	{
		$sql = "SELECT AllowedAccessType FROM INTRANET_PHOTO_ALBUM_GROUP_SETTING WHERE GroupID='$group_id'";	
		$row = $this->returnVector($sql);
		return ($row[0] == "") ? 0 : $row[0];		
	}
}	

?>