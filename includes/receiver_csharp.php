<?php
// using by : 

####################################################################
# Created by : Kenneth Wong
# Creation Date : 20051207
####################################################################
##### Change Log [Start] ##### 						
# Version updates
# 20190507 : Carlos - [ip2.5.10.5.1] Modified Notify_Student_Status() to detect http/https, cater 443 port, and bypass cert checking for https.
# 20190416 : Carlos - [ip2.5.10.5.1] Modified Notify_Student_Status() hardcoded to use http as https has many issues.
# 20190412 : Carlos - [ip2.5.10.5.1] Added flag $sys_custom['cardapi_http_protocol'] to force use http or https in Notify_Student_Status().
# 20190325 : Carlos - [ip2.5.10.4.1] For preset absence/outing, send arrival push message for first tap card in time.
# 20181002 : Carlos - [ip2.5.9.10.1] For student attendance, whole day with lunch mode, if tap card within PM tap card period, and PM status is null or absent, update lunch back time and PM status on time. If student allow to go out for lunch, feedback message is arrived from lunch, otherwise just arrived.
# 20180830 : Carlos - [ip2.5.9.10.1] $sys_custom['StudentAttendance']['CardAPIDebugLog'] log student attendance initial data for checking.
# 20180803 : Carlos - [ip2.5.9.10.1] Replace buildMonthTable($year, $month) with libcardstudentattend2->createTable_Card_Student_Daily_Log($year,$month).
# 20180108 : Carlos - [ip2.5.9.1.1] Modified student tap card, whole day with lunch time mode, tap card within lunch period, check setting [MinsToTreatAsPMIn] to ignore tap card before PM effective tap card period.
# 20171211 : Carlos - [ip2.5.9.1.1] Added function Get_Profile_Office_Remark(), retain absent profile's office remark to late profile's office remark for case from absent status to late status. 
# 20171128 : Carlos - [ip2.5.9.1.1] For whole day mode with or without lunch time, if after school time tap card normal leave would set PM status to present when PM status is absent or null. 
# 20170927 : Carlos - [ip2.5.8.10.1] For WD without lunch time, when current time before school leave time, checked if AM status is null or absent or outing, would update both in time and lunch back time.
# 20161209 : Carlos - [ip2.5.8.1.1] $sys_custom['StudentAttendance']['CustomizedTimeSlotSettings'] - added Handle_Customized_Time_Slots($am_pm).
# 20161110 : Carlos - [ip2.5.8.1.1] Clear PM absent/late profile for scenario: WD with lunch mode and do not allow go out for lunch, PM set absent but tap card early leave.
# 20161012 : Carlos - [ip2.5.7.10.1] $sys_custom['StudentAttendance']['LunchNoPushMessage'] - Student Attendance whole day with lunch mode do not send push message for lunch in/out. 
# 20160913 : Carlos - [ip2.5.7.10.1] Changed Staff Attendance 3.0 non-school day return message.
# 20160830 : Carlos - [ip2.5.7.10.1] For student, added tap card location when recording entry log. 
# 20160712 : Carlos - [ip2.5.7.7.1] For student, added ignore period checking for non-school day to avoid repeatedly sending push message.
# 20160406 : Carlos - [ip2.5.7.4.1] For Staff Attendance v3.0, added setting [HideTimeSlot] for not displaying time slot at return message.
# 20151211 : Carlos - [ip2.5.7.1.1] For student, if on non-school day tap card, depends on eClass App settings to send push notification. 
# 20141223 : Carlos - [ip2.5.6.1.1] [WD with lunch] - Check and display not allow go out for lunch if do not need to record lunch out and student not in allow list  
# 20141211 : Bill - [ip2.5.5.12.1] $sys_custom['SupplementarySmartCard'] added CardID4, get user by either matching CardID or CardID2 or CardID3 or CardID4 for non-RFID card
# 20141105 : Carlos - [ip2.5.5.12.1] added $sys_custom['cardapi_host_ip'] to Notify_Student_Status() for catering some server need to use internal ip instead of domain name
# 20141021 : Carlos - [ip2.5.5.10.1] added local function Notify_Student_Status($TargetUserID,$AccessDateTime,$Status), changed send push message function to be run in a background process.
# 20141007 : Carlos - [ip2.5.5.10.1] added tap card log CARD_STUDENT_RAW_LOG_YYYY_MM
# 20140603 : Carlos - Handle situation: Whole day with lunch
#										not all students allow to go out for lunch, and student not in allow to go out list
#										when AM is present or late, tap card after lunch end, treat it as early leave and PM status as present
# 20131231 : Ivan - Added $plugin['eClassApp'] flag for sending push message
# 20131205 : Carlos - Added parameter $external_date for simulate tap card
# 20131113 : Carlos - $sys_custom['SupplementarySmartCard'] Added CardID2 and CardID3, get user by either matching CardID or CardID2 or CardID3 for non-RFID card
# 20130725 : Carlos - [Student Attendance] record entry log for non school day
# 20120716 : Carlos - add flag $sys_custom['StaffAttendance']['NoDutyAPIReturnMsg'] for Staff Attendance V3 to control no duty return msg
# 20120523 : Carlos - add special case handling 2012-0522-1420-04071, Whole day with Lunch, no need tap card out for lunch, before lunch end, AM absent, record as PM Present
# 20120322 : Carlos - check setting OnlySynceDisLateRecordWhenConfirm to update eDiscipline late record at Set_Profile()
# 20120215 : Carlos
#			 - Modified Whole day with lunch mode PM session not to set AM in time (Requested by 2012-0208-1118-24073)
# 20111216 : Carlos
#			 - Add PMDateModified and PMModifyBy for student attendance.
#			 - Student Attendance : Record in time for preset absent/outing 
# 20110901 : Carlos
#			 Add $lc->Notify_Student_Status() if $plugin['ASLParentApp'] ASL Parent App Web Service is available 
# 20101130 : Kenneth Chung
#			 Apply Clear_Profile/ Set_Profile functions to Studennt Attendance API
# 20100921 : Kenneth Chung 
#			 StaffAttendanceV3 add check and do not display attendance status
# 20100917 : Carlos 
#			 StaffAttendanceV2 Added update CARD_STAFF_ATTENDANCE2_DAILY_LOG_XXXX_XX Early Leave data
# 20100614 : Carlos
#            Add Log for Non-School-Day Tap-Card 
# 20090728 : Kenneth Chung 
#		change to get all setting from libgeneralsettings class
# 20080104 : Fai Khoe
#            Add check group attendance
# 20071029 : Peter Ho
#            Add log of Student Attendance Status ( for St. Mary : Time Slot Mode, WD w/ Lunch )
#            $bug_tracing['smartcard_student_attend_status']
# 20070927 : Kenneth Wong
#            Change Staff output from $attend_lang['InSchool_late'] to $attend_lang['InSchool'] in late situation
# 20070903 : Ronald Yeung
#            Show Student Leave Option When Student Is Normal Leave School Or Early Leave School
# 20070822 : Peter Ho
#                         Add createEntry_Card_Student_Record_Storage() after buildMonthTable()
# 20070720 : Kenneth Wong
#            Modify reminder retrival : Get more than 1 reminder message
# 20070711 : Kenneth Wong
#            Add raw log to staff attendance : buildStaffRawLogMonthTable()
# 2007 : Ronald Yeung
# 20070503: Peter Ho
#           Staff attendance - Add removing the previous records when the new status and old status are not the same
# 20070503: Kenneth Wong
#           Add checking of "0" and "" in InSchoolTime to prevent accidentally 00:00:00 record written
# 20051207: Kenneth Wong
# 20061108: Kenneth Wong
#           Check whether last modified is card read or confirmation
#
###### Change Log [End] ######
####################################################################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Customized
if ($sys_custom['QualiEd_StudentAttendance'])
{
    header("Location: receiver_q.php?CardID=$CardID&sitename=$sitename&dlang=$dlang");
    exit();
}

intranet_opendb();

include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

header("Content-type: text/plain; charset=utf-8");

if ($CardID == "")
{
    # Card not registered
    echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']);
    intranet_closedb();
    exit();
}

### Simulate server busy state and slow reponse situation
if($sys_custom['cardapi_delay_tapcard']){
	$delay_second = rand(isset($sys_custom['cardapi_delay_tapcard_min'])? $sys_custom['cardapi_delay_tapcard_min']:2, isset($sys_custom['cardapi_delay_tapcard_max'])?$sys_custom['cardapi_delay_tapcard_max']:10);
	sleep($delay_second);
}

######################################################
# Param : (From QueryString)
#    CardID
#    sitename 
#		 IsRFID -  1 -> card id is a RFID, 0 -> card is tranditional EM card
#		 dlang
######################################################

# $db_engine (from functions.php)

################################################
$directProfileInput = false; // not applied to student attendance @ 20101130 by kenneth chung
$getremind = true;
$reminder_null_text = "No Reminder";
################################################

$sitename = urldecode($sitename);

# 1. Get UserInfo
$CardField = ($IsRFID == 1)? 'RFID':'CardID';
$namefield = getNameFieldByLang();
$sql = "SELECT 
					UserID, 
					$namefield, 
					RecordType, 
					ClassName, 
					ClassNumber, 
					UserLogin 
				FROM 
					INTRANET_USER 
				WHERE 
					".$CardField." = '$CardID'";
if($sys_custom['SupplementarySmartCard'] && $IsRFID != 1){
	$sql .= "OR CardID2='$CardID' OR CardID3='$CardID' OR CardID4='$CardID'";
}					
$temp = $db_engine->returnArray($sql,6);
list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];

# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']);
    intranet_closedb();
    exit();
}
else if ($targetType == 1)    # Staff, redirection
{
	// get all general setting for student attendance
	$GeneralSetting = new libgeneralsettings();
	$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance');
	
	# Check IP
	if (!isIPAllowed('StaffAttendance'))
	{
	     echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
	     intranet_closedb();
	     exit();
	}
	
    $sitename = intranet_htmlspecialchars($sitename);
    $datatype = intranet_htmlspecialchars($datatype);

    if ($module_version['StaffAttendance'] == 2.0)
    {
        /* User information */
        /* $targetUserID - UserID, $targetName - ChineseName or EnglishName */

        /* Get which group belongs to */
        $sql = "SELECT GroupID FROM CARD_STAFF_ATTENDANCE_USERGROUP WHERE UserID = $targetUserID";
        $temp = $db_engine->returnVector($sql);
        $staffGroupID = $temp[0];
        if (sizeof($temp)==0 || $staffGroupID == 0)
        {
            /* if this staff no needs to take attendance - Not belongs to any group (Print ignoring message) */
            echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###".$attend_lang['NoNeedToTakeAttendance']."###$targetUserLogin");
            intranet_closedb();
            exit();
        }

     $current_time = time();

     if ($external_time != "")
     {
         $current_time = strtotime(date('Y-m-d') . " " . $external_time);
     }

        $month = date('m',$current_time);
        $year = date('Y',$current_time);
        $day = date('d',$current_time);
        $today = date('Y-m-d',$current_time);
        $weekday = date('w', $current_time);
        $time_string = date('H:i:s',$current_time);
        $ts_now = $current_time - strtotime($today);

        /* Add Raw Log to Staff Attendance */
        $tablename = buildStaffRawLogMonthTable($year, $month);
        $sql = "INSERT IGNORE INTO $tablename
                       (UserID, DayNumber, RecordTime, RecordStation, DateInput, DateModified)
                       VALUES
                       ('$targetUserID','$day',NOW(),'$sitename', NOW(),NOW())";
        $db_engine->db_db_query($sql);


        /****************************************/
        /* Get Duty (Exact Day on user > Exact Day on Group > Group Duty)    */
        /*
           $dutyOn -
           $dutyStart -
           $dutyEnd -
           $dutyInWaived -
           $dutyOutWaived -
        */
        $roster_retrieved = false;


        // Get Leave Record
        $sql = "SELECT RecordID, RecordType, OutgoingType, ReasonType
                                FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD
                                WHERE StaffID = '$targetUserID' AND RecordDate = '$today'";
        $temp = $db_engine->returnArray($sql,4);
        list ($t_LeaveRecordID, $t_LeaveRecordType, $t_LeaveOutgoingType, $t_LeaveReasonType) = $temp[0];
        if ($t_LeaveRecordID != "")
        {
            if ($t_LeaveRecordType == STAFF_LEAVE_TYPE_HOLIDAY)
            {
                $dutyOn = false;
                $roster_retrieved = true;
            }
            else if ($t_LeaveRecordType == STAFF_LEAVE_TYPE_OUTGOING)
            {
                 $dutyInWaived = ($t_LeaveOutgoingType == STAFF_LEAVE_BOTH_WAIVED || $t_LeaveOutgoingType == STAFF_LEAVE_IN_WAIVED);
                 $dutyOutWaived = ($t_LeaveOutgoingType == STAFF_LEAVE_BOTH_WAIVED || $t_LeaveOutgoingType == STAFF_LEAVE_OUT_WAIVED);
            }
        }

        if (!$roster_retrieved)
        {
             // Exact Day on User
             $sql = "SELECT RecordID, Duty, TIME_TO_SEC(DutyStart), TIME_TO_SEC(DutyEnd)
                            FROM CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY
                            WHERE UserID = $targetUserID AND DutyDate = '$today'";
             $temp = $db_engine->returnArray($sql,4);
             if (sizeof($temp)!=0)
             {
                 list ($t_id, $t_duty, $t_duty_start, $t_duty_end) = $temp[0];
                 if ($t_id > 0)
                 {
                     $dutyOn = $t_duty;
                     $dutyStart = $t_duty_start;
                     $dutyEnd = $t_duty_end;
                     $roster_retrieved = true;
                 }
             }
        }

        if (!$roster_retrieved)
        {
             // Exact Day on Group
             $sql = "SELECT RecordID, Duty, TIME_TO_SEC(DutyStart), TIME_TO_SEC(DutyEnd)
                            FROM CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY
                            WHERE GroupID = $staffGroupID AND DutyDate = '$today'";
             $temp = $db_engine->returnArray($sql,4);

             if (sizeof($temp)!=0)
             {
                 list ($t_id, $t_duty, $t_duty_start, $t_duty_end) = $temp[0];
                 if ($t_id > 0)
                 {
                     $dutyOn = $t_duty;
                     $dutyStart = $t_duty_start;
                     $dutyEnd = $t_duty_end;
                     $roster_retrieved = true;
                 }

             }
        }

        if (!$roster_retrieved)
        {
             // Group Roster
             switch ($weekday)
             {
                     case 0: $field_ext = "Sun"; break;
                     case 1: $field_ext = "Mon"; break;
                     case 2: $field_ext = "Tue"; break;
                     case 3: $field_ext = "Wed"; break;
                     case 4: $field_ext = "Thur"; break;
                     case 5: $field_ext = "Fri"; break;
                     case 6: $field_ext = "Sat"; break;
                     default: $field_ext = "Mon";
             }
             $field_duty = "Duty".$field_ext;
             $field_duty_start = "TIME_TO_SEC(DutyStart".$field_ext.")";
             $field_duty_end = "TIME_TO_SEC(DutyEnd".$field_ext.")";
             $sql = "SELECT GroupID, $field_duty, $field_duty_start, $field_duty_end
                            FROM CARD_STAFF_ATTENDANCE2_GROUP
                            WHERE GroupID = $staffGroupID";
             $temp = $db_engine->returnArray($sql,4);
             if (sizeof($temp)!=0)
             {
                 list ($t_id, $t_duty, $t_duty_start, $t_duty_end) = $temp[0];
                 if ($t_id > 0)
                 {
                     $dutyOn = $t_duty;
                     $dutyStart = $t_duty_start;
                     $dutyEnd = $t_duty_end;
                     $roster_retrieved = true;
                 }
             }
        }
        if (!$roster_retrieved)
        {
             echo output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']);
             intranet_closedb();
             exit();
        }

        /****************************************/
        // Retrieve Daily Record
        // Get Original Record
        $table_name = buildStaffAttendance2MonthTable($year,$month);

        $sql = "SELECT RecordID, Duty, DutyStart, DutyEnd, StaffPresent,
                       InTime, InSchoolStatus, InWaived, InAttendanceRecordID,
                       OutTime, OutSchoolStatus, OutWaived, OutAttendanceRecordID,
                       MinLate, MinEarlyLeave, RecordType, RecordStatus, DateModified
                       FROM $table_name
                       WHERE StaffID = '$targetUserID' AND DayNumber = '$day'";
        $temp = $db_engine->returnArray($sql, 18);
        list($r_RecordID, $r_Duty, $r_DutyStart, $r_DutyEnd, $r_StaffPresent,
             $r_InTime, $r_InSchoolStatus, $r_InWaived, $r_InAttendID,
             $r_OutTime, $r_OutSchoolStatus, $r_OutWaived, $r_OutAttendID,
             $r_MinLate, $r_MinEarly, $r_RecordType, $r_RecordStatus, $r_DateModified
             ) = $temp[0];
        $noDailyRecord = ($r_RecordID == "");
        if ($noDailyRecord)
        {
            $sql = "INSERT INTO $table_name
                           (StaffID, DayNumber, Duty, DutyStart, DutyEnd, DateInput, DateModified)
                    VALUES
                    ('$targetUserID','$day', '$dutyOn', SEC_TO_TIME('$dutyStart'), SEC_TO_TIME('$dutyEnd') , now(), now())";
            $db_engine->db_db_query($sql);
            $r_RecordID = $db_engine->db_insert_id();
        }
        else
        {
        }

        if ($dutyOn!=1)
        {
            /* if no duty, process OT Record */
            // Check this is the first record or not
            $sql = "SELECT RecordID, TIME_TO_SEC(StartTime)
                                    FROM CARD_STAFF_ATTENDANCE2_OT_RECORD
                                    WHERE StaffID = '$targetUserID' AND RecordDate = '$today'";
            $temp = $db_engine->returnArray($sql,2);
            if (!is_array($temp) || sizeof($temp)==0 || $temp[0][0] == "")
            {
                    // First Record
                    $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD
                                            (StaffID, RecordDate, StartTime, DateInput, DateModified)
                                    VALUES
                                            ('$targetUserID', '$today', '$time_string', NOW(), NOW())";
                    $db_engine->db_db_query($sql);
            }
            else
            {
                    // Finishing
                    $t_OTRecordID = $temp[0][0];
                    $t_Start = $temp[0][1];
                    $time_diff = $ts_now - $t_Start;
                    $OTmins = round($time_diff/60);
                    $sql = "UPDATE CARD_STAFF_ATTENDANCE2_OT_RECORD
                                            SET EndTime = '$time_string', OTmins = '$OTmins',
                                            DateModified = NOW()
                                            WHERE RecordID = $t_OTRecordID";
                    $db_engine->db_db_query($sql);
            }



            /* Response */
            echo output_env_str(CARD_RESP_STAFF_TIME_RECORDED."###".$targetName.$attend_lang['RecordSuccessful'].$time_string."###$targetUserLogin");
            intranet_closedb();
            exit();
        }
        else  /* else Go to Mark Attendance */
        {

            /* Mark Attendance */
            // Classify Time Zone
            if ($ts_now <= $dutyStart)
            {
                // On Time
                $sql = "UPDATE $table_name
                               SET StaffPresent = 1, InTime = '$time_string',
                                   InSchoolStatus = '".CARD_STATUS_PRESENT."',
                                   InSchoolStation = '$sitename',
                                   MinLate = 0, DateModified = now()
                       WHERE RecordID = $r_RecordID
                               ";
                $db_engine->db_db_query($sql);

                // Remove Profile Record (Late / Absent)
                $sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE
                               WHERE StaffID = '$targetUserID' AND RecordDate = '$today'
                                     AND (RecordType = '".PROFILE_TYPE_ABSENT."' OR RecordType='".PROFILE_TYPE_LATE."')
                                     ";
                $db_engine->db_db_query($sql);



                // Response
                echo output_env_str(CARD_RESP_STAFF_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin");
                intranet_closedb();
                exit();

            }
            else if ($ts_now >= $dutyEnd)
            {
                // After Working hour
                // Update Daily Record
                $sql = "UPDATE $table_name
                               SET OutTime = '$time_string',
                                   OutSchoolStatus = '".CARD_LEAVE_NORMAL."',
                                   OutSchoolStation = '$sitename',
                                   MinEarlyLeave = 0, DateModified = now()
                       WHERE RecordID = $r_RecordID
                                   ";
                $db_engine->db_db_query($sql);

                // Remove Profile Record (Early Leave)
                $sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE
                               WHERE StaffID = '$targetUserID' AND RecordDate = '$today'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'
                                     ";
                $db_engine->db_db_query($sql);


                // Process OT Record
                // Check whether record created
                $sql = "SELECT RecordID, TIME_TO_SEC(StartTime)
                                            FROM CARD_STAFF_ATTENDANCE2_OT_RECORD
                                            WHERE StaffID = '$targetUserID' AND RecordDate = '$today'";
                    $temp = $db_engine->returnArray($sql,2);
                    if (!is_array($temp) || sizeof($temp)==0 || $temp[0][0] == "")
                    {
                            // Create Record

                            // Get Threshold
                            #$OT_threshold = 0;
                            $mins_ignored = trim(get_file_content("$intranet_root/file/staffattend_ot_ignore.txt"));
                            $OT_threshold = $mins_ignored+0;


                            // OT Start time
                            $t_OTStart = $dutyEnd + $OT_threshold*60;
                            //$string_OTStart = date('Y-m-d',$t_OTStart);
                            $string_OTStart = date('H:i:s',$t_OTStart+strtotime($today));



                            // OT mins
                            $time_diff = $ts_now - $t_OTStart;
                            $OTmins = round($time_diff/60);

                            if ($OTmins > 0)
                            {
                                $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD
                                              (StaffID, RecordDate, StartTime, EndTime,
                                               OTmins, DateInput, DateModified)
                                            VALUES
                                              ('$targetUserID', '$today', '$string_OTStart', '$time_string',
                                               '$OTmins', NOW(), NOW())";
                                $db_engine->db_db_query($sql);
                            }
                            else  # Nothing to do if not pass threshold
                            {}

                    }
                    else
                    {
                            // Finishing
                            $t_OTRecordID = $temp[0][0];
                            $t_Start = $temp[0][1];
                            $time_diff = $ts_now - $t_Start;
                            $OTmins = round($time_diff/60);
                            $sql = "UPDATE CARD_STAFF_ATTENDANCE2_OT_RECORD
                                                    SET EndTime = '$time_string', OTmins = '$OTmins',
                                                    DateModified = NOW()
                                                    WHERE RecordID = $t_OTRecordID";
                            $db_engine->db_db_query($sql);
                    }


                // Response
                echo output_env_str(CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin");
                intranet_closedb();
                exit();


            }
            else
            {
                // Within

                if ($noDailyRecord || $r_InSchoolStatus == CARD_STATUS_ABSENT)     # First Record or confirmed absent
                {
                    // Late
                    $time_diff = $ts_now - $dutyStart;
                    $minLate = round($time_diff/60);
                    $sql = "UPDATE $table_name
                               SET StaffPresent = 1, InTime = '$time_string',
                                   InSchoolStatus = '".CARD_STATUS_LATE."',
                                   InSchoolStation = '$sitename',
                                   MinLate = '$minLate', DateModified = now()
                             WHERE RecordID = $r_RecordID
                               ";
                    $db_engine->db_db_query($sql);

                     // Remove Profile Record ( Absent)
                        $sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE
                               WHERE StaffID = '$targetUserID' AND RecordDate = '$today'
                                     AND RecordType = '".PROFILE_TYPE_ABSENT."'
                                     ";

                $db_engine->db_db_query($sql);

                    if ($dutyInWaived)
                    {
                        // Mark as waived and response as on time
                        $sql = "UPDATE $table_name
                                       SET InWaived = 1
                                       WHERE RecordID = $r_RecordID
                               ";
                        $db_engine->db_db_query($sql);
                        // Response
                        echo output_env_str(CARD_RESP_STAFF_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin");
                        intranet_closedb();
                        exit();
                    }
                    else
                    {
                        // Insert Profile Record
                        if ($directProfileInput)
                        {

                            $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
                                   (StaffID, RecordDate, RecordType, ReasonType, DateInput, DateModified)
                                   VALUES
                                   ('$targetUserID', '$today', '".PROFILE_TYPE_LATE."',
                                   '$t_LeaveReasonType',
                                   NOW(), NOW()
                                   )";
                            $db_engine->db_db_query($sql);

                        }
                        // Response
                        echo output_env_str(CARD_RESP_STAFF_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin");
                        intranet_closedb();
                        exit();
                    }
                }
                else
                {
                    // Intermediate Records
                    $inter_table_name = buildStaffAttendance2InterLog($year, $month);
                    $sql = "INSERT INTO $inter_table_name
                                   (StaffID, DayNumber, RecordTime, DateInput, DateModified)
                                   VALUES
                                   ('$targetUserID', '$day', '$time_string', now(), now())";
                    $db_engine->db_db_query($sql);
                    
					// Update Early Leave data
					$time_diff = $dutyEnd - $ts_now;
                    $minEarlyleave = round($time_diff/60);
					
					$sql = "UPDATE $table_name 
							SET OutTime = '$time_string',
								OutSchoolStatus = '".PROFILE_TYPE_EARLY."',
								OutSchoolStation = '$sitename',
								MinEarlyLeave = '$minEarlyleave',
								DateModified = now() 
							WHERE RecordID = $r_RecordID ";
					$db_engine->db_db_query($sql);
					
                    if ($directProfileInput)
                    {

                        // Early Leave Record
                       $sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
                                   (StaffID, RecordDate, RecordType, ReasonType, DateInput, DateModified)
                                   VALUES
                                   ('$targetUserID', '$today', '".PROFILE_TYPE_EARLY."',
                                   '$t_LeaveReasonType',
                                   NOW(), NOW()
                                   )";
                           $db_engine->db_db_query($sql);
                    }
                    // Response
                    echo output_env_str(CARD_RESP_STAFF_LEAVE_SCHOOL_EARLY."###$targetName".$attend_lang['StaffEarlyLeave']."$time_string"."###$targetUserLogin");
                    intranet_closedb();
                    exit();

                }


            }

        }


    }
    else if ($module_version['StaffAttendance'] == 3.0)
    {
    	include_once($PATH_WRT_ROOT."includes/libstaffattend3_api.php");
    	
    	$StaffAttend3 = new libstaffattend3_api();
    	
    	$current_time = time();
			if ($external_time != "") {
				$current_time = strtotime(date('Y-m-d') . " " . $external_time);
			}
			if ($external_date != "" && $external_time)
			{
				$current_time = strtotime($external_date. " " . $external_time);
			}
			$time_string = date('H:i:s',$current_time);
			
    	$Result = $StaffAttend3->Record_Tap_Card($targetUserID,$current_time,$sitename);
			
			switch ($Result[1]) {
				case CARD_RESP_NO_NEED_TO_TAKE: 
					if($sys_custom['StaffAttendance']['NoDutyAPIReturnMsg']){
						$ResponseMsg = $attend_lang['EntryLogRecorded'];
					}else{
						//$ResponseMsg = $attend_lang['NonSchoolDay'];
						$ResponseMsg = $attend_lang['HasTappedCard'];
					}
					break;
				case CARD_RESP_STAFF_IN_SCHOOL_ONTIME: 
					$SlotInfo = $Result[0].': ';
					if ($Settings['HideAttendStatus']) 
						$ResponseMsg = $attend_lang['RecordSuccessful'];
					else
						$ResponseMsg = $attend_lang['InSchool'];
					break;
				case CARD_RESP_STAFF_IN_SCHOOL_LATE: 
					$SlotInfo = $Result[0].': ';
					if ($Settings['HideAttendStatus']) 
						$ResponseMsg = $attend_lang['RecordSuccessful'];
					else
						$ResponseMsg = $attend_lang['InSchool_late'];
					break;
				case CARD_RESP_STAFF_LEAVE_SCHOOL_NORMAL: 
					$SlotInfo = $Result[0].': ';
					if ($Settings['HideAttendStatus']) 
						$ResponseMsg = $attend_lang['RecordSuccessful'];
					else
						$ResponseMsg = $attend_lang['LeaveSchool'];
					break;
				case CARD_RESP_STAFF_LEAVE_SCHOOL_EARLY: 
					$SlotInfo = $Result[0].': ';
					if ($Settings['HideAttendStatus']) 
						$ResponseMsg = $attend_lang['RecordSuccessful'];
					else
						$ResponseMsg = $attend_lang['StaffEarlyLeave'];
					break;
				case STAFF_LEAVE_TYPE_HOLIDAY: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['RecordSuccessful'];
					break;
				case STAFF_LEAVE_TYPE_OUTGOING: 
					$SlotInfo = $Result[0].': ';
					$ResponseMsg = $attend_lang['RecordSuccessful'];
					break;
				case CARD_RESP_IGNORE_WITHIN_PERIOD:
					$ResponseMsg = $attend_lang['WithinIgnorePeriod'];
					break;
				default:
					break;
			}

			echo $Result[1]."###".($Settings['HideTimeSlot']? "" : $SlotInfo." ").$targetName.$ResponseMsg.$time_string."###$targetUserLogin";
      intranet_closedb();
      exit();
    }
    else # Old version (1.0)
    {

             $sql = "INSERT INTO CARD_STAFF_ATTENDANCE_LOG (CardID,SiteName,RecordedTime,Type)
                            VALUES ('$CardID','$sitename',now(),'$datatype')";
             $db_engine->db_db_query($sql);
             $record = $db_engine->db_insert_id();

             $sql = "SELECT DATE_FORMAT(RecordedTime,'%H:%i:%s') FROM CARD_STAFF_ATTENDANCE_LOG WHERE LogID = $record";
             $loggedTime = $db_engine->returnVector($sql);

             $user_field = getNameFieldByLang();
             $sql = "SELECT UserID, CardID, $user_field, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
             $user = $db_engine->returnArray($sql,4);
             list($id,$card,$name,$login) = $user[0];
             if ($dlang == "b5")
             {
                 $str = $attend_lang['RecordSuccessful'];
                 $fail = $attend_lang['CardNotRegistered'];
             }
             else
             {
                 $str = "is recorded.\r\n Time Recorded: ";
                 $fail = "Card Error. Please retry or contact System Administrator.";
             }
             if ($id!="")
             {
                 echo output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$name $str ".$loggedTime[0]."###$targetUserLogin");
             }
             else
             {
                 echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###$fail");
             }
    }

}
else if ($targetType == 2)
{
	// get all general setting for student attendance
	$GeneralSetting = new libgeneralsettings();
	$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');
	
	# Check IP
	if (!isIPAllowed('StudentAttendance'))
	{
	     echo output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']);
	     intranet_closedb();
	     exit();
	}
	
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	
	$lc = new libcardstudentattend2();
	$targetName = "$targetName ($targetClass-$targetClassNumber)";
	
	##### Retrive the Time table mode setting #####
	//$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
	$time_table = $Settings['TimeTableMode'];
  $time_table_mode = $time_table;         ### 0 - Time Slot, 1 - Time Session
	
	##### Get Ignore period for card tapping #####
	//$ignore_period = get_file_content("$intranet_root/file/stattend_ignore.txt");
	$ignore_period = $Settings['IgnorePeriod'];
	$ignore_period += 0;
	
	##### Get Attendance Mode Setting #####
	//$content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
	$content_basic = $Settings['AttendanceMode'];
	$attendance_mode = $content_basic;
		
	$null_record_variable = "NULL";
	$upadteeDisLateRecord = $lc->OnlySynceDisLateRecordWhenConfirm != '1';
	# Get ClassID
//	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE ClassTitleEN = '$targetClass' and AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
//	$temp = $db_engine->returnVector($sql);
//	$targetClassID = $temp[0];
	
	# Get Record
	# Create Monthly data table
	$current_time = time();
	if ($external_time != "")
	{
	   $current_time = strtotime(date('Y-m-d') . " " . $external_time);
	}
	if ($external_date != "" && $external_time)
	{
		$current_time = strtotime($external_date. " " . $external_time);
	}
	
	$month = date('m',$current_time);
	$year = date('Y',$current_time);
	$day = date('d',$current_time);
	$today = date('Y-m-d',$current_time);
	$time_string = date('H:i:s',$current_time);
	$ts_now = $current_time - strtotime($today);
	
	# Check need to take attendance or not
	$FinalSetting = $lc->Get_Student_Attend_Time_Setting($today,$targetUserID);
	
	$ts_morningTime = $lc->timeToSec($FinalSetting['MorningTime']);
	$ts_lunchStart = $lc->timeToSec($FinalSetting['LunchStart']);
	$ts_lunchEnd = $lc->timeToSec($FinalSetting['LunchEnd']);
	$ts_leaveSchool = $lc->timeToSec($FinalSetting['LeaveSchoolTime']);
	
	/*echo $today.'<br>';
	echo $FinalSetting['MorningTime'].','.$FinalSetting['LunchStart'].','.$FinalSetting['LunchEnd'].','.$FinalSetting['LeaveSchoolTime'];
	echo "$ts_morningTime,$ts_lunchStart,$ts_lunchStart,$ts_leaveSchool";*/
	
	if($bug_tracing['smartcard_student_attend_status']){
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		if(!is_dir($file_path."/file/log_student_attend_status")){
		        $lf = new libfilesystem();
		        $lf->folder_new($file_path."/file/log_student_attend_status");
		}
		$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($year.$month.$day).".txt";
	}
	
	//buildMonthTable($year, $month);
	//createEntry_Card_Student_Record_Storage($year,$month);
	
	$lc->createTable_Card_Student_Daily_Log($year,$month);
	$lc->createTable_Card_Student_Raw_Log($year,$month);
	$lc->createTable_Card_Student_Entry_Log($year,$month);
	/*
	$raw_log_table = sprintf("CARD_STUDENT_RAW_LOG_%04d_%02d",$year,$month);
	$sql = "SELECT TIME_TO_SEC(MAX(RecordTime)) FROM $raw_log_table WHERE UserID='$targetUserID' AND DayNumber='$day'";
	$last_raw_log_time_record = $lc->returnVector($sql);
	if ($ignore_period > 0 && trim($last_raw_log_time_record[0]) != "")
	{
	   $ts_ignore = $ignore_period * 60;
	   $current_time_timeonly = $current_time - strtotime($today);
	   # Check whether it is w/i last n mins
	   if ($current_time_timeonly - $last_raw_log_time_record[0] < $ts_ignore)
	   {
	       echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###".$attend_lang['WithinIgnorePeriod']."###$targetUserLogin");
	       intranet_closedb();
	       exit();
	   }
	}
	*/
	$lc->addCardStudentRawLogRecord($targetUserID, $year, $month, $day, $time_string,$sitename);
	
	if($sys_custom['StudentAttendance']['RecordBodyTemperature'] && isset($_REQUEST['TemperatureValue']) && is_numeric($_REQUEST['TemperatureValue']) && isset($_REQUEST['TemperatureStatus']))
	{
		$TemperatureStatus = $_REQUEST['TemperatureStatus'];
		if(!in_array($TemperatureStatus,array(0,1))) $TemperatureStatus = 0;
		$TemperatureValue = $_REQUEST['TemperatureValue'];
		$sql = "INSERT INTO CARD_STUDENT_BODY_TEMPERATURE_RECORD (RecordDate,StudentID,TemperatureStatus,TemperatureValue,DateInput) VALUES ('$today','$targetUserID','$TemperatureStatus','$TemperatureValue','$today $time_string')";
		$lc->db_db_query($sql);
	}
	
	/*  Get Reminder Message */
	if ($getremind)
	{
	   $sql = "SELECT ReminderID, Reason
	                  FROM CARD_STUDENT_REMINDER
	                  WHERE StudentID = $targetUserID AND DateOfReminder = '$today'";
	   $temp = $db_engine->returnArray($sql,2);
	
	   if (sizeof($temp)==0 || $temp[0][0] == "")
	   {
	       $t_remind_id = 0;
	       $t_remind_msg = $reminder_null_text;
	   }
	   else
	   {
	       $t_remind_id = 0;
	       $t_remind_msg = "";
	       $delim = "";
	       for($i=0; $i<sizeof($temp); $i++)
	       {
	           list($s_remind_id, $s_remind_msg) = $temp[$i];
	           $t_remind_msg .= $delim.$s_remind_msg;
	           $delim = "\n";
	       }
	       $t_remind_id = $s_remind_id+0;
	   }
	}
	else
	{
	    $t_remind_id = 0;
	    $t_remind_msg = $reminder_null_text;
	}

  ### Created By Ronald On 3 Sep 2007 ###
  ###  Get The Student Leave Option  ###
  if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
  {
		$current_weekday = date("w");
		$sql = "SELECT Weekday".$current_weekday." FROM CARD_STUDENT_LEAVE_OPTION WHERE StudentID = $targetUserID";
		$result = $db_engine->returnArray($sql,1);
		for($i=0; $i<sizeof($result); $i++)
		{
      list($leave_option) = $result[$i];
      $t_leave_option = $leave_option;
		}
  }
  ######################################

	# Get Record From table
	$dailylog_tablename = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	$sql = "SELECT 
					RecordID, 
					TIME_TO_SEC(InSchoolTime),
          IFNULL(AMStatus,'".$null_record_variable."'), 
          TIME_TO_SEC(LunchOutTime), 
          TIME_TO_SEC(LunchBackTime),
          IFNULL(PMStatus,'".$null_record_variable."'), 
          TIME_TO_SEC(LeaveSchoolTime), 
          LeaveStatus, 
          UNIX_TIMESTAMP(DateModified), 
		  UNIX_TIMESTAMP(PMDateModified),
          TIME_TO_SEC(LastTapCardTime) 
	       FROM 
	       	$dailylog_tablename
	       WHERE 
	       	UserID = '".$targetUserID."' 
	       	AND 
	       	DayNumber = '".$day."'";
	$temp = $db_engine->returnArray($sql,9);
	list($DayRecordID, $curr_InSchoolTime, $curr_AMStatus, $curr_LunchOutTime, $curr_LunchBackTime, $curr_PMStatus, $curr_LeaveSchoolTime, $curr_LeaveStatus, $curr_tslastMod, $curr_tspmlastMod, $LastTapCardTime) = $temp[0];

	/*if (sizeof($temp)==0 || $ts_recordID == "")
	{
	   echo output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']);
	   intranet_closedb();
	   exit();
	}*/

	if ($FinalSetting === "NonSchoolDay")
	{
		// according the eClass App settings determine to send push notification or not
		if($plugin['ASLParentApp'] || $plugin['eClassApp'])
		{
			include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
			$leClassApp = new libeClassApp();
			$eClassAppSettingsObj = $leClassApp->getAppSettingsObj();
			$sendNonSchoolDayPushMessage = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']);
			$enableNonSchoolDayPushMessage_arrival = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']);
			$enableNonSchoolDayPushMessage_leave = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']);
			
			if($sendNonSchoolDayPushMessage)
			{
				$card_student_raw_log_table = "CARD_STUDENT_RAW_LOG_".$year."_".$month;
				$sql = "SELECT TIME_TO_SEC(RecordTime) as RecordTime FROM $card_student_raw_log_table WHERE UserID='$targetUserID' AND DayNumber='$day' ORDER BY RecordTime";
				$raw_log_records = $lc->returnResultSet($sql);
				$last_raw_log_time = 0;
				if(count($raw_log_records)>1)
				{
					$last_raw_log_time = $raw_log_records[count($raw_log_records)-2]['RecordTime'];
				}
				$ts_ignore = $ignore_period * 60;
	   			$current_time_timeonly = $current_time - strtotime($today);
	   			if ( ($current_time_timeonly - $last_raw_log_time) > $ts_ignore)
	   			{
					if(count($raw_log_records) > 1) // leave
					{
						if($enableNonSchoolDayPushMessage_leave){
							Notify_Student_Status($targetUserID,$current_time,'L');
						}
					}else{ // arrive
						if($enableNonSchoolDayPushMessage_arrival){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
	   			}
			}
		}
		//if($sys_custom['SmartCardStudentAttend_NonSchoolDay_LogRaw'])
   		//{
   			$lc->Record_Raw_Log($targetUserID,$current_time,$sitename);
   		//}
		echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['NonSchoolDay']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		intranet_closedb();
		exit();
	}
	
	# Check Last Card Read Time for ignore period
	//if ($curr_tslastMod == "")
	if(trim($DayRecordID) == "")
	{
	   # Insert Record
	   $sql = "INSERT INTO $dailylog_tablename (UserID, DayNumber, DateInput,InputBy)
	           VALUES ('$targetUserID','$day',now(),'".$targetUserID."')";
	   $db_engine->db_db_query($sql);
	   $curr_AMStatus = $null_record_variable;
	   $curr_PMStatus = $null_record_variable;
	}
	else
	{
	 if ($ignore_period > 0 && trim($LastTapCardTime) != "")
	 {
	   $ts_ignore = $ignore_period * 60;
	   $current_time_timeonly = $current_time - strtotime($today);
	   # Check whether it is w/i last n mins
	   if ($current_time_timeonly - $LastTapCardTime < $ts_ignore)
	   {
	     /*$tslastMod_timeonly = $curr_tslastMod - strtotime($today);
	     if ($tslastMod_timeonly == $curr_InSchoolTime
	         || $tslastMod_timeonly == $curr_LunchOutTime
	         || $tslastMod_timeonly == $curr_LunchBackTime
	         || $tslastMod_timeonly == $curr_LeaveSchoolTime
	         )
	     {*/
	       echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###".$attend_lang['WithinIgnorePeriod']."###$targetUserLogin");
	       intranet_closedb();
	       exit();
	     /*}*/
	   }
	 }
	}
	
	// Get preset outing for today
	$sql = "select 
					  DayType,
					  OutTime,
					  BackTime,
					  Location,
					  FromWhere,
					  Objective,
					  PIC,
					  Detail 
					from 
						CARD_STUDENT_OUTING 
					where 
						UserID = '".$targetUserID."' 
						and 
						RecordDate = '".$today."'";
	$Temp = $db_engine->returnArray($sql,13);
	for ($i=0; $i< sizeof($Temp); $i++) {
		list($DayType,$OutTime,$BackTime,$Location,$FromWhere,$Objective,$PIC,$Detail) = $Temp[$i];
		$PresetOuting[$DayType]["Message"] = $OutTime.$attend_lang['To'].$BackTime.$attend_lang['On'].$Location;
		$PresetOuting[$DayType]["PIC"] = $PIC;
	}
	if ($curr_AMStatus != $null_record_variable && $curr_AMStatus != CARD_STATUS_OUTING) {
		//debug_r($curr_AMStatus);
		unset($PresetOuting[PROFILE_DAY_TYPE_AM]);
	}
	if ($curr_PMStatus != $null_record_variable && $curr_PMStatus != CARD_STATUS_OUTING) {
		//debug_r($curr_PMStatus);
		unset($PresetOuting[PROFILE_DAY_TYPE_PM]);
	}
	
	// Get preset absence for today
	$sql = "select 
						DayPeriod,
						Reason 
					from 	
						CARD_STUDENT_PRESET_LEAVE 
					where 
						StudentID = '".$targetUserID."' 
						and 
						RecordDate = '".$today."'";
	$Temp = $db_engine->returnArray($sql,2);
	for ($i=0; $i< sizeof($Temp); $i++) {
		list($DayType,$Reason) = $Temp[$i];
		$PresetAbsence[$DayType]['Reason']= $Reason;
	}
	if ($curr_AMStatus != $null_record_variable && $curr_AMStatus != CARD_STATUS_ABSENT) {
		//debug_r($curr_AMStatus);
		unset($PresetAbsence[PROFILE_DAY_TYPE_AM]);
	}
	if ($curr_PMStatus != $null_record_variable && $curr_PMStatus != CARD_STATUS_ABSENT) {
		//debug_r($curr_PMStatus);
		unset($PresetAbsence[PROFILE_DAY_TYPE_PM]);
	}
	
	if($sys_custom['StudentAttendance']['CustomizedTimeSlotSettings']){
		$customizedTimeSlotSettings = $lc->GetCustomizedTimeSlotRecords(array('RecordStatus'=>1,'InBetweenTime'=>1,'Time'=>$time_string));
	}
	
	if($sys_custom['StudentAttendance']['CardAPIDebugLog']){
		$log_data = array("attendance_mode"=>$attendance_mode,"today"=>$today,"time_string"=>$time_string,"ts_now"=>$ts_now,"FinalSetting"=>$FinalSetting,"CurrentSchoolYearID"=>Get_Current_Academic_Year_ID(),"CurAcademicYearID"=>$lc->CurAcademicYearID,
						"ts_morningTime"=>$ts_morningTime,"ts_lunchStart"=>$ts_lunchStart,"ts_lunchEnd"=>$ts_lunchEnd,"ts_leaveSchool"=>$ts_leaveSchool,"curr_InSchoolTime"=>$curr_InSchoolTime,"curr_AMStatus"=>$curr_AMStatus,"curr_PMStatus"=>$curr_PMStatus,"daily_log"=>$temp);
		$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.(isset($_SESSION['UserID'])?$_SESSION['UserID']:0).' '.str_replace(array("\r\n","\n","\r"),' ',serialize($log_data));
		$lc->log($log_row);
	}
	
	if ($attendance_mode == 0) # 0 - AM Only
	{
		if (is_array($PresetOuting[PROFILE_DAY_TYPE_AM])) {
			$lc->Record_Raw_Log($targetUserID,$current_time);
			// Record in time but not touch status
			if(trim($curr_InSchoolTime) == ""){
				$sql = "UPDATE $dailylog_tablename SET 
							InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
							DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
						WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
					Notify_Student_Status($targetUserID,$current_time,'A');
				}
			}
			echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_AM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_AM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			intranet_closedb();
			exit();
		}
		else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_AM])) {
			$lc->Record_Raw_Log($targetUserID,$current_time);
			// Record in time but not touch status
			if(trim($curr_InSchoolTime) == ""){
				$sql = "UPDATE $dailylog_tablename SET 
							InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
							DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
						WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
					Notify_Student_Status($targetUserID,$current_time,'A');
				}
			}
			echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_AM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			intranet_closedb();
			exit();
		}
		else {
			
			if($sys_custom['StudentAttendance']['CustomizedTimeSlotSettings'] && count($customizedTimeSlotSettings)>0){
				if($ts_now <= $ts_leaveSchool){
					if ($curr_InSchoolTime=="" && ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT))
					{
						Handle_Customized_Time_Slots(PROFILE_DAY_TYPE_AM);
					}
				}
			}
			
			if ($ts_now <= $ts_morningTime)
			{
			   if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
			   {
			       # Before school starts
			       # On Time
			       # Mark AM Status, InSchool Time
			       $sql = "UPDATE $dailylog_tablename SET 
			       								InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
			                      InSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', 
			                      LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
			       
			       $lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM);
			       echo output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       send_sms(1);
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		Notify_Student_Status($targetUserID,$current_time,'A');
			       }
			       intranet_closedb();
			       exit();
			   }
			   else
			   {
			       # Before school starts
			       # Already marked on time, no need to handle
			       echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       intranet_closedb();
			       exit();
			   }
			}
			else if ($ts_now >= $ts_leaveSchool)
			{
				# After School
				# Mark Leave Status, Leave Time
				send_sms(2, $dailylog_tablename);
				$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
				              LeaveSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
				              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				# Remove Early Leave Record
				$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
				              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
				                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				$db_engine->db_db_query($sql);
				
				# Remove Reason Record of Early Leave
				$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
				              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
				                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				$db_engine->db_db_query($sql);
				
				if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
				}
				else
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				}
				
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			    	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			    	Notify_Student_Status($targetUserID,$current_time,'L');
			    }
				intranet_closedb();
				exit();
			}
			# Lesson Time
			else if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
			{
				// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
				if (($ts_now - $ts_morningTime) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
					# Response
			    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			    intranet_closedb();
			    exit();
				}
				else {	
			    # Lesson Time and AMStatus is NULL or set Absent
			    # Mark as Late
			    # Mark AM Status, InSchool Time
			    $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
			                   InSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			    $db_engine->db_db_query($sql);
					$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_AM,CARD_STATUS_ABSENT);
					$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
					if($existing_remark != ''){
						$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_AM,CARD_STATUS_LATE,$existing_remark);
					}
					/*
			    if ($directProfileInput)
			    {
			        # Add to student profile
			        $year = getCurrentAcademicYear();
			        $semester = getCurrentSemester();
			        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			        $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
			        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			        $db_engine->db_db_query($sql);
			        $insert_id = $db_engine->db_insert_id();
			        # Update to reason table
			        $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			        $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
			        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                       VALUES ($fieldsvalues)";
			        $db_engine->db_db_query($sql);
			        if ($plugin['Discipline'])
			        {
			            # For Discipline System upgrade
			            include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
			            $ldiscipline = new libdiscipline();
			            $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
			            $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
			        }
			    }
			    */
			
			    # Response
			    echo output_env_str(CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			    if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			    	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			    	Notify_Student_Status($targetUserID,$current_time,'A');
			    }
			    intranet_closedb();
			    exit();
			  }
			}/*
			else if ($curr_AMStatus == CARD_STATUS_OUTING)
			{
			}*/
			else # Early Leave (as already Present, Late or Outing)
			{
		    # Mark Leave Status - AM early leave, Leave Time
		    $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
		                   LeaveSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
		                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		    $db_engine->db_db_query($sql);
				
				$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
				/*
		    if ($directProfileInput)
		    {
		        # Add to student profile
		        $year = getCurrentAcademicYear();
		        $semester = getCurrentSemester();
		        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		        $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
		        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		        $db_engine->db_db_query($sql);
		        $insert_id = $db_engine->db_insert_id();
		        # Update to reason table
		        $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		        $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
		        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                       VALUES ($fieldsvalues)";
		        $db_engine->db_db_query($sql);
		    }
				*/
				
		    if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
		    {
		            echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
		    }
		    else
		    {
		            echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		    }
		    if($plugin['ASLParentApp'] || $plugin['eClassApp']){
				//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
				Notify_Student_Status($targetUserID,$current_time,'L');
			}
		    intranet_closedb();
		    exit();
			}
		}
	}
	else if ($attendance_mode == 1) # 1 - PM Only
	{
		if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
			$lc->Record_Raw_Log($targetUserID,$current_time);
			// Record in time but not touch status
			if(trim($curr_InSchoolTime) == ""){
				$sql = "UPDATE $dailylog_tablename SET 
							InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
							PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
						WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
					Notify_Student_Status($targetUserID,$current_time,'A');
				}
			}
			echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			intranet_closedb();
			exit();
		}
		else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
			$lc->Record_Raw_Log($targetUserID,$current_time);
			// Record in time but not touch status
			if(trim($curr_InSchoolTime) == ""){
				$sql = "UPDATE $dailylog_tablename SET 
							InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
							PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
						WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
					Notify_Student_Status($targetUserID,$current_time,'A');
				}
			}
			echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			intranet_closedb();
			exit();
		}
		else {
			
			if($sys_custom['StudentAttendance']['CustomizedTimeSlotSettings'] && count($customizedTimeSlotSettings)>0){
				if($ts_now <= $ts_leaveSchool){
					if ($curr_InSchoolTime=="" && ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT))
					{
						Handle_Customized_Time_Slots(PROFILE_DAY_TYPE_AM);
					}
				}
			}
			
			if ($ts_now <= $ts_lunchEnd ) # $ts_morningTime )
			{
				if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
				{
				   # On Time
				   # Mark PM Status, InSchool Time
				   $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
				                  InSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
				                  WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				   $db_engine->db_db_query($sql);
				   
				   $lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
				   echo output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				   send_sms(1);
				   if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		Notify_Student_Status($targetUserID,$current_time,'A');
			       }
				   intranet_closedb();
				   exit();
				}
				else
				{
				   echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				   intranet_closedb();
				   exit();
				}
			}
			else if ($ts_now >= $ts_leaveSchool)
			{
				# After School
				# Mark Leave Status, Leave Time
				send_sms(2, $dailylog_tablename);
				$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
				              LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
				              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				# Remove Early Leave Record
				$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
				              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
				                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$db_engine->db_db_query($sql);
				
				# Remove Reason Record of Early Leave
				$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
				              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
				                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$db_engine->db_db_query($sql);
				
				if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
				}
				else
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				}
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			    	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			    	Notify_Student_Status($targetUserID,$current_time,'L');
			    }
				intranet_closedb();
				exit();
			}
			else if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
			{
				// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
				if (($ts_now - $ts_lunchEnd) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
					# Response
			    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			    intranet_closedb();
			    exit();
				}
				else {
					# Lesson Time and PMStatus is NULL or set Absent
					# Mark as Late
					# Mark PMStatus, InSchool Time
					$sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
					               InSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
					               WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_PM,CARD_STATUS_ABSENT);
					$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
					if($existing_remark != ''){
						$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,$existing_remark);
					}
					/*				
					if ($directProfileInput)
					{
					    # Add to student profile
					    $year = getCurrentAcademicYear();
					    $semester = getCurrentSemester();
					    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
					    $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
					    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
					    $db_engine->db_db_query($sql);
					    $insert_id = $db_engine->db_insert_id();
					    # Update to reason table
					    $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
					    $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
					    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
					                   VALUES ($fieldsvalues)";
					    $db_engine->db_db_query($sql);
					
					    if ($plugin['Discipline'])
					    {
					        # For Discipline System upgrade
					        include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
					        $ldiscipline = new libdiscipline();
					        $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
					        $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
					    }
					}
					*/
				
			    # Response
			    echo output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			    if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			    	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			    	Notify_Student_Status($targetUserID,$current_time,'A');
			    }
			    intranet_closedb();
			    exit();
			  }
			}/*
			else if ($curr_PMStatus == CARD_STATUS_OUTING)
			{
			}*/
			else # Early Leave (PM= Present, Late, Outing)
			{
			    # Mark Leave Status -> PM Early, Leave Time
			    $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
			                   LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                   WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			    $db_engine->db_db_query($sql);
					
					$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
					/*
			    if ($directProfileInput)
			    {
			        # Add to PM Early leave student profile
			        $year = getCurrentAcademicYear();
			        $semester = getCurrentSemester();
			        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			        $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
			        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			        $db_engine->db_db_query($sql);
			        $insert_id = $db_engine->db_insert_id();
			        # Update to reason table
			        $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			        $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
			        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                       VALUES ($fieldsvalues)";
			        $db_engine->db_db_query($sql);
			    }
			    */
			
			    if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			    {
			            echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
			    }
			    else
			    {
			            echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			    }
			    if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       //$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       Notify_Student_Status($targetUserID,$current_time,'L');
			    }
			    intranet_closedb();
			    exit();
			}
		}
	}
	else if ($attendance_mode == 2) # 2 - WD w/ Lunch
	{
		if($sys_custom['StudentAttendance']['CustomizedTimeSlotSettings'] && count($customizedTimeSlotSettings)>0){
			if($ts_now < $ts_lunchStart){
				if ($curr_InSchoolTime=="" && ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT))
				{
					Handle_Customized_Time_Slots(PROFILE_DAY_TYPE_AM);
				}
			}else{
				if($curr_LunchBackTime=="" && ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)){
					Handle_Customized_Time_Slots(PROFILE_DAY_TYPE_PM);
				}
			}
		}
		
		if ($ts_now <= $ts_morningTime)
		{
			if (is_array($PresetOuting[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_AM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_AM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_AM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else {
				
		    if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
		    {
		      # On Time
		      # Mark AM Status, InSchool Time
		      $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
		                     InSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
		                     WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		      $db_engine->db_db_query($sql);
					
					$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM);
					
		      ### Bug Tracing
		      if($bug_tracing['smartcard_student_attend_status']){
		        $log_date = date('Y-m-d H:i:s');
		        $log_target_date = date('Y-m-d H:i:s');
		        $log_student_id = $targetUserID;
		        $log_old_status = $curr_AMStatus;
		        $log_new_status = CARD_STATUS_PRESENT;
		        $log_sql = $sql;
		        $log_admin_user = "terminal";
		        $log_time = "id=1,ts_now = $ts_now,ts_morningTime = $ts_morningTime";
		
		        $log_page = 'receiver.php';
		        $log_content = get_file_content($log_filepath);
		        $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		        $log_content .= $log_entry;
		        write_file_content($log_content, $log_filepath);
		      }
		
		      echo output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		      send_sms(1);
		      if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			  	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			  	Notify_Student_Status($targetUserID,$current_time,'A');
			  }
		      intranet_closedb();
		      exit();
		    }
		    else
		    {
		        echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		        intranet_closedb();
		        exit();
		    }
		  }
		}
		else if ($ts_now < $ts_lunchStart) # In AM Lesson Time
		{
			if (is_array($PresetOuting[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_AM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_AM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_AM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else {
				if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
				{
					// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
					if (($ts_now - $ts_morningTime) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
						# Response
				    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				    intranet_closedb();
				    exit();
					}
					else {
						# Late
						# Mark AM Status, InSchool Time
						$sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
						              InSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
						              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_AM,CARD_STATUS_ABSENT);
						$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
						if($existing_remark != ''){
							$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_AM,CARD_STATUS_LATE,$existing_remark);
						}
						### Bug Tracing
						if($bug_tracing['smartcard_student_attend_status']){
							$log_date = date('Y-m-d H:i:s');
						  $log_target_date = date('Y-m-d H:i:s');
						  $log_student_id = $targetUserID;
						  $log_old_status = $curr_AMStatus;
						  $log_new_status = CARD_STATUS_LATE;
						  $log_sql = $sql;
						  $log_admin_user = "terminal";
						  $log_time = "id=2,ts_now = $ts_now,ts_lunchStart = $ts_lunchStart";
						
		          $log_page = 'receiver.php';
		          $log_content = get_file_content($log_filepath);
		          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
		          $log_content .= $log_entry;
		          write_file_content($log_content, $log_filepath);
						}
						
						/*
						if ($directProfileInput)
						{
							# Add late record to student profile
							$year = getCurrentAcademicYear();
							$semester = getCurrentSemester();
							$fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
							$fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
							$sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
							$db_engine->db_db_query($sql);
							$insert_id = $db_engine->db_insert_id();
							# Update to reason table
							$fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
							$fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
							$sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
							              VALUES ($fieldsvalues)";
							$db_engine->db_db_query($sql);
							if ($plugin['Discipline'])
							{
								# For Discipline System upgrade
								include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
								$ldiscipline = new libdiscipline();
								$ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
								$ldiscipline->calculateUpgradeLateToDetention($targetUserID);
							}
						}
						*/
					
						# Response
						echo output_env_str(CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       			//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       			Notify_Student_Status($targetUserID,$current_time,'A');
			       		}
						intranet_closedb();
						exit();
					}
				}/*
				else if ($curr_AMStatus == CARD_STATUS_OUTING)
				{
				}*/
				else     # Present/Late/Outing - AM early leave
				{
				 # AM Early Leave
				 # Mark Leave Status, Leave Time
				 $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
				                LeaveSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
				                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				 $db_engine->db_db_query($sql);
				
				 $lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
				### Bug Tracing
				      if($bug_tracing['smartcard_student_attend_status']){
				              $log_date = date('Y-m-d H:i:s');
				              $log_target_date = date('Y-m-d H:i:s');
				              $log_student_id = $targetUserID;
				              $log_old_status = $curr_LeaveStatus;
				              $log_new_status = CARD_LEAVE_AM;
				              $log_sql = $sql;
				              $log_admin_user = "terminal";
				              $log_time = "id=3,ts_now = $ts_now,ts_lunchStart = $ts_lunchStart";
				
				                           $log_page = 'receiver.php';
				                          $log_content = get_file_content($log_filepath);
				                          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
				                          $log_content .= $log_entry;
				                          write_file_content($log_content, $log_filepath);
				          }
				
				 /*
				 if ($directProfileInput)
				 {
				     # Add Early Leave record to student profile
				     $year = getCurrentAcademicYear();
				     $semester = getCurrentSemester();
				     $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
				     $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
				     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
				     $db_engine->db_db_query($sql);
				     $insert_id = $db_engine->db_insert_id();
				     # Update to reason table
				     $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
				     $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
				     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
				                    VALUES ($fieldsvalues)";
				     $db_engine->db_db_query($sql);
				 }
				 */
				 
				 if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
				 {
				         echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
				 }
				 else
				 {
				         echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				 }
				 if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			     	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			     	Notify_Student_Status($targetUserID,$current_time,'L');
			     }
				 intranet_closedb();
				 exit();
				}
			}
		}
		else if ($ts_now <= $ts_lunchEnd) # In Lunch Time
		{
			if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING || is_array($PresetOuting[PROFILE_DAY_TYPE_AM]) || is_array($PresetAbsence[PROFILE_DAY_TYPE_AM]))  # PM Present (AM Absent/Outing/PresetOuting/PresetAbsent) - Mark Incoming Time
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
								Notify_Student_Status($targetUserID,$current_time,'A');
							}
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
								Notify_Student_Status($targetUserID,$current_time,'A');
							}
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
					$lunch_misc_no_need_record = $Settings['NoRecordLunchOut'];
				    $lunch_misc_once_only = $Settings['LunchOutOnce'];
				    $lunch_misc_all_allowed = $Settings['AllAllowGoOut'];
				    $MinsTreatAsPMIn = $Settings['MinsToTreatAsPMIn'];
					
			   if (($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT) && ($curr_InSchoolTime=="" || $curr_InSchoolTime > $ts_now))
			   {
			   /*    # Mark PM Status, InSchool Time
			       $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
			                      InSchoolStation = '$sitename',
			                      LunchBackTime = '$time_string' , LunchBackStation = '$sitename',
			                      PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'"; */             
			       # Mark PM Status, LunchBackTime
			       $sql = "UPDATE $dailylog_tablename SET PMStatus = '".CARD_STATUS_PRESENT."',
			                      LunchBackTime = '$time_string' , LunchBackStation = '$sitename',
			                      PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
						
						$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
			    ### Bug Tracing
			        if($bug_tracing['smartcard_student_attend_status']){
			                $log_date = date('Y-m-d H:i:s');
			                $log_target_date = date('Y-m-d H:i:s');
			                $log_student_id = $targetUserID;
			                $log_old_status = $curr_PMStatus;
			                $log_new_status = CARD_STATUS_PRESENT;
			                $log_sql = $sql;
			                $log_admin_user = "terminal";
			                $log_time = "id=4,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
			
			                             $log_page = 'receiver.php';
			                            $log_content = get_file_content($log_filepath);
			                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                            $log_content .= $log_entry;
			                            write_file_content($log_content, $log_filepath);
			            }
			
			
			
			       echo output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       			Notify_Student_Status($targetUserID,$current_time,'A');
			       		}
			       }
			       intranet_closedb();
			       exit();
			   }
			   // special case handling 2012-0522-1420-04071 
			   else if($lunch_misc_no_need_record && ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)){
			   	
			   		$sql = "UPDATE $dailylog_tablename SET PMStatus = '".CARD_STATUS_PRESENT."',
			                      LunchBackTime = '$time_string' , LunchBackStation = '$sitename',
			                      PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
						
				   $lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
			    ### Bug Tracing
			        if($bug_tracing['smartcard_student_attend_status']){
			                $log_date = date('Y-m-d H:i:s');
			                $log_target_date = date('Y-m-d H:i:s');
			                $log_student_id = $targetUserID;
			                $log_old_status = $curr_PMStatus;
			                $log_new_status = CARD_STATUS_PRESENT;
			                $log_sql = $sql;
			                $log_admin_user = "terminal";
			                $log_time = "id=4,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
			
			                             $log_page = 'receiver.php';
			                            $log_content = get_file_content($log_filepath);
			                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                            $log_content .= $log_entry;
			                            write_file_content($log_content, $log_filepath);
			            }
					
			       echo output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       			Notify_Student_Status($targetUserID,$current_time,'A');
			       		}
			       }
			       intranet_closedb();
			       exit();
			   }
			   else
			   {
			       # Print already back
			       echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       intranet_closedb();
			       exit();
			   }
			 	}
			}
			else # AM Present/Late, Lunch checking
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
								Notify_Student_Status($targetUserID,$current_time,'A');
							}
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
								Notify_Student_Status($targetUserID,$current_time,'A');
							}
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
			   # Get Lunch Settings
			   /*$content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
			   $lunch_misc_settings = explode("\n",$content_lunch_misc);
			   list($lunch_misc_no_need_record,$lunch_misc_once_only,$lunch_misc_all_allowed) = $lunch_misc_settings;*/
			   $lunch_misc_no_need_record = $Settings['NoRecordLunchOut'];
			   $lunch_misc_once_only = $Settings['LunchOutOnce'];
			   $lunch_misc_all_allowed = $Settings['AllAllowGoOut'];
			   $MinsTreatAsPMIn = $Settings['MinsToTreatAsPMIn'];
			   
			   $is_within_pm_tap_card_period = $MinsTreatAsPMIn != '' && $MinsTreatAsPMIn > 0 && $ts_now >= ($ts_lunchEnd - ($MinsTreatAsPMIn * 60));
			   $allow_to_go_out_for_lunch = $lunch_misc_all_allowed;
			   if(!$lunch_misc_all_allowed){
			   		$sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST WHERE StudentID = '$targetUserID'";
		            $temp = $db_engine->returnVector($sql);
		            $allow_to_go_out_for_lunch = $temp[0] != '';
			   }
			   
			   // if tap card within PM tap card period, and PM status is null or absent, update lunch back time and PM status on time
			   if($is_within_pm_tap_card_period && ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)){
			   		# Lunch Back
			        # Mark PM Status, Lunch Back Time
			       $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
			               LunchBackStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			               WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
							
				   $lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
				   
				   $msg = $allow_to_go_out_for_lunch? $attend_lang['BackFromLunch'] : $attend_lang['InSchool'];
			       echo output_env_str(CARD_RESP_FROM_LUNCH_ONTIME."###$targetName".$msg."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       			Notify_Student_Status($targetUserID,$current_time,'A');
			       		}
			       }
			       intranet_closedb();
			       exit();
			   }
			   
			   if ($lunch_misc_no_need_record)
			   {
			   		// if lunch no need to tap card out, and if not all allow to go out, check from allow go out student list
			   		if(!$lunch_misc_all_allowed)
			   		{
				   		# Check in Database
		                $sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST
		                               WHERE StudentID = '$targetUserID'";
		                $temp = $db_engine->returnVector($sql);
		                if ($temp[0] == "") # Lunch Out NOT allowed
		                {
		                    # Store Bad action : Lunch out failed
		                    $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
		                                   VALUES ('$targetUserID', '$today','$today $time_string' , '".CARD_BADACTION_LUNCH_NOTINLIST."', now(), now())";
		                    $db_engine->db_db_query($sql);
		
		                    echo output_env_str(CARD_RESP_FAILED_OUT_LUNCH_NOTINLIST."###".$attend_lang['NotAllowToOutLunch']." - ".$targetName."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		                    intranet_closedb();
		                    exit();
		                }
			   		}
			   		
			   		if($MinsTreatAsPMIn != '' && $MinsTreatAsPMIn > 0 && $ts_now < ($ts_lunchEnd - ($MinsTreatAsPMIn * 60))){
			   			# Not within PM session tap card period, ignore tap card
			   			echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###".str_replace('<!--TIME-->',date("H:i",strtotime($today) + $ts_lunchEnd - ($MinsTreatAsPMIn * 60)), $attend_lang['IgnoreWithinLunchPeriod'])."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			   			intranet_closedb();
		                exit();
			   		}
			   		
			       # Lunch Back
			       # Mark PM Status, Lunch Back Time
			       $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
			               LunchBackStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			               WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
							
						$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
			                                   ### Bug Tracing
			        if($bug_tracing['smartcard_student_attend_status']){
			                $log_date = date('Y-m-d H:i:s');
			                $log_target_date = date('Y-m-d H:i:s');
			                $log_student_id = $targetUserID;
			                $log_old_status = $curr_PMStatus;
			                $log_new_status = CARD_STATUS_PRESENT;
			                $log_sql = $sql;
			                $log_admin_user = "terminal";
			                $log_time = "id=5,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
			
			                             $log_page = 'receiver.php';
			                            $log_content = get_file_content($log_filepath);
			                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                            $log_content .= $log_entry;
			                            write_file_content($log_content, $log_filepath);
			            }
			
			
			       echo output_env_str(CARD_RESP_FROM_LUNCH_ONTIME."###$targetName".$attend_lang['BackFromLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       			Notify_Student_Status($targetUserID,$current_time,'A');
			       		}
			       }
			       intranet_closedb();
			       exit();
			   }
			   else
			   {
			   	// if tap card within the N mins before the PM starts (in lunch settings > misc > MinsToTreatAsPMIn
			   	// treat all tap card as Lunch back
			   	if ($ts_now >= ($ts_lunchEnd - ($MinsTreatAsPMIn * 60))) {
			   		/*
			   		 # no need to check lunch out allow checking because it is PM back 
			   		# Check Lunch Out allow
		           if (!$lunch_misc_all_allowed)
		           {
		                # Check in Database
		                $sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST
		                               WHERE StudentID = '$targetUserID'";
		                $temp = $db_engine->returnVector($sql);
		                if ($temp[0] == "") # Lunch Out NOT allowed
		                {
		                    # Store Bad action : Lunch out failed
		                    $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
		                                   VALUES ('$targetUserID', '$today','$today $time_string' , '".CARD_BADACTION_LUNCH_NOTINLIST."', now(), now())";
		                    $db_engine->db_db_query($sql);
		
		                    echo output_env_str(CARD_RESP_FAILED_OUT_LUNCH_NOTINLIST."###".$attend_lang['NotAllowToOutLunch']." - ".$targetName."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		                    intranet_closedb();
		                    exit();
		                }
		           }
	           		*/	
	           # Mark Lunch Back time, PM Status
							$sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
							               LunchBackStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							               WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
							$db_engine->db_db_query($sql);
							
							$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
							
							echo output_env_str(CARD_RESP_FROM_LUNCH_ONTIME."###$targetName".$attend_lang['BackFromLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
	            			if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       				//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       				if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       					Notify_Student_Status($targetUserID,$current_time,'A');
			       				}
			       			}
	            			intranet_closedb();
	            			exit();
			   	}
			   	else {
			       if ($curr_LunchOutTime == "")   # Not yet out
			       {
			           # Check Lunch Out allow
			           if (!$lunch_misc_all_allowed)
			           {
			                # Check in Database
			                $sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST
			                               WHERE StudentID = '$targetUserID'";
			                $temp = $db_engine->returnVector($sql);
			                if ($temp[0] == "") # Lunch Out NOT allowed
			                {
			                    # Store Bad action : Lunch out failed
			                    $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
			                                   VALUES ('$targetUserID', '$today','$today $time_string' , '".CARD_BADACTION_LUNCH_NOTINLIST."', now(), now())";
			                    $db_engine->db_db_query($sql);
			
			                    echo output_env_str(CARD_RESP_FAILED_OUT_LUNCH_NOTINLIST."###".$attend_lang['NotAllowToOutLunch']." - ".$targetName."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			                    intranet_closedb();
			                    exit();
			                }
			           }
			
			           # Allow to Go out
			           # Mark Lunch Out Time, Clear Leave Status
			           $sql = "UPDATE $dailylog_tablename SET LunchOutTime = '$time_string', LeaveStatus = NULL,
			                          LunchOutStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                          WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			           $db_engine->db_db_query($sql);
			           
			           # Remove Early Leave Record
									$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
									              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
									                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
									$db_engine->db_db_query($sql);
									
									# Remove Reason Record of Early Leave
									$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
									              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
									                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
									$db_engine->db_db_query($sql);
			           echo output_env_str(CARD_RESP_GO_OUT_LUNCH."###$targetName".$attend_lang['OutLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			           if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       			//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       			if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       				Notify_Student_Status($targetUserID,$current_time,'L');
			       			}
			       		}
			           intranet_closedb();
			           exit();
			
			       }
			       else if ($curr_LunchBackTime == "")    # Not Yet back from Lunch
			       {
			       		if($MinsTreatAsPMIn != '' && $MinsTreatAsPMIn > 0 && $ts_now < ($ts_lunchEnd - ($MinsTreatAsPMIn * 60))){
							# Not within PM session tap card period
							echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###".str_replace('<!--TIME-->',date("H:i",strtotime($today) + $ts_lunchEnd - ($MinsTreatAsPMIn * 60)), $attend_lang['IgnoreWithinLunchPeriod'])."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
							intranet_closedb();
							exit();
						}
			       	
			            # Mark Lunch Back time, PM Status
			            $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
			                           LunchBackStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                           WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			            $db_engine->db_db_query($sql);
									
									$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
			            ### Bug Tracing
			                        if($bug_tracing['smartcard_student_attend_status']){
			                                $log_date = date('Y-m-d H:i:s');
			                                $log_target_date = date('Y-m-d H:i:s');
			                                $log_student_id = $targetUserID;
			                                $log_old_status = $curr_PMStatus;
			                                $log_new_status = CARD_STATUS_PRESENT;
			                                $log_sql = $sql;
			                                $log_admin_user = "terminal";
			                                $log_time = "id=6,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
			
			                                             $log_page = 'receiver.php';
			                                            $log_content = get_file_content($log_filepath);
			                                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                                            $log_content .= $log_entry;
			                                            write_file_content($log_content, $log_filepath);
			                            }
			
			            echo output_env_str(CARD_RESP_FROM_LUNCH_ONTIME."###$targetName".$attend_lang['BackFromLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			            if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       			//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       			if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       				Notify_Student_Status($targetUserID,$current_time,'A');
			       			}
			       		}
			            intranet_closedb();
			            exit();
			       }
			       else # Already back , and try to go out again
			       {
			           if ($lunch_misc_once_only)
			           {
			               # Not allowed to go out 2nd time
			               # Store bad action : Lunch trial 2nd time
			               $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
			                              VALUES ('$targetUserID', '$today', '$today $time_string', '".CARD_BADACTION_LUNCH_BACKALREADY."', now(), now())";
			               $db_engine->db_db_query($sql);
			               echo output_env_str(CARD_RESP_FAILED_OUT_LUNCH_ALREADYBACK."###".$attend_lang['NotAllowToOutLunchAgain']." - ".$targetName."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			               intranet_closedb();
			               exit();
			           }
			           else
			           {
			               # Allow to go out again
			               # Mark Lunch Out Time, Clear Lunch Back Time, Clear PM Status
			               $sql = "UPDATE $dailylog_tablename SET LunchOutTime = '$time_string', LeaveStatus = NULL,
			                              LunchOutStation = '$sitename', LunchBackTime = NULL, LunchBackStation = NULL,
			                              PMStatus = NULL, DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			               $db_engine->db_db_query($sql);
											
											# Remove Early Leave Record
											$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
											              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
											                    AND AttendanceDate = '$today' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
											$db_engine->db_db_query($sql);
											
											# Remove Reason Record of Early Leave
											$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
											              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
											                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
											$db_engine->db_db_query($sql);
			               ### Bug Tracing
			                                if($bug_tracing['smartcard_student_attend_status']){
			                                        $log_date = date('Y-m-d H:i:s');
			                                        $log_target_date = date('Y-m-d H:i:s');
			                                        $log_student_id = $targetUserID;
			                                        $log_old_status = $curr_PMStatus;
			                                        $log_new_status = "NULL";
			                                        $log_sql = $sql;
			                                        $log_admin_user = "terminal";
			                                        $log_time = "id=7,ts_now = $ts_now,ts_lunchEnd = $ts_lunchEnd";
			
			                                                     $log_page = 'receiver.php';
			                                                    $log_content = get_file_content($log_filepath);
			                                                    $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                                                    $log_content .= $log_entry;
			                                                    write_file_content($log_content, $log_filepath);
			                                    }
			
			               echo output_env_str(CARD_RESP_GO_OUT_LUNCH."###$targetName".$attend_lang['OutLunch']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			               if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       				//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       				if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
			       					Notify_Student_Status($targetUserID,$current_time,'L');
			       				}
			       			}
			               intranet_closedb();
			               exit();
			           }
			
			       }
			    }
			   }
			 	}
			}
		}
		else if ($ts_now < $ts_leaveSchool) # PM Lesson Time
		{
			if ($curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING || is_array($PresetOuting[PROFILE_DAY_TYPE_AM]) || is_array($PresetAbsence[PROFILE_DAY_TYPE_AM]))  # PM Present (AM Absent/Outing/PresetOuting/PresetAbsent)
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
					if ($curr_PMStatus==$null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
					{
						// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
						if (($ts_now - $ts_lunchEnd) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
							# Response
					    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					    intranet_closedb();
					    exit();
						}
						else {
						/*	 # Mark InSchool Time, PM status late
							 $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
							                InSchoolStation = '$sitename',
							                LunchBackTime = '$time_string', LunchBackStation = '$sitename',
							                PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							                WHERE DayNumber = '$day' AND UserID = '$targetUserID'"; */
							 
							 # Mark LunchBackTime, PM status late
							 $sql = "UPDATE $dailylog_tablename SET PMStatus = '".CARD_STATUS_LATE."',
							                LunchBackTime = '$time_string', LunchBackStation = '$sitename',
							                PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
							 $db_engine->db_db_query($sql);
							$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_PM,CARD_STATUS_ABSENT);
							$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
							if($existing_remark != ''){
								$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,$existing_remark);
							}
							### Bug Tracing
							  if($bug_tracing['smartcard_student_attend_status']){
							          $log_date = date('Y-m-d H:i:s');
							          $log_target_date = date('Y-m-d H:i:s');
							          $log_student_id = $targetUserID;
							          $log_old_status = $curr_PMStatus;
							          $log_new_status = CARD_STATUS_LATE;
							          $log_sql = $sql;
							          $log_admin_user = "terminal";
							          $log_time = "id=8,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
							
							                       $log_page = 'receiver.php';
							                      $log_content = get_file_content($log_filepath);
							                      $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
							                      $log_content .= $log_entry;
							                      write_file_content($log_content, $log_filepath);
							      }
							      
							 /*
							 if ($directProfileInput)
							 {
							     # Add PM late to student profile
							     $year = getCurrentAcademicYear();
							     $semester = getCurrentSemester();
							     $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
							     $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
							     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
							     $db_engine->db_db_query($sql);
							     $insert_id = $db_engine->db_insert_id();
							     # Update to reason table
							     $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
							     $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
							     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
							                    VALUES ($fieldsvalues)";
							     $db_engine->db_db_query($sql);
							     if ($plugin['Discipline'])
							     {
							         # For Discipline System upgrade
							         include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
							         $ldiscipline = new libdiscipline();
							         $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
							         $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
							     }
							 }
							 */
							
							 # Response
							 echo output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
							 if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       				//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       				Notify_Student_Status($targetUserID,$current_time,'A');
			       			 }
							 intranet_closedb();
							 exit();
						}
					}
					else
					{
					 # Mark Leave time, Leave Status as PM Early leave
					 $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
					                LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
					                WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					 $db_engine->db_db_query($sql);
					 
					 $lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
					### Bug Tracing
					  if($bug_tracing['smartcard_student_attend_status']){
					          $log_date = date('Y-m-d H:i:s');
					          $log_target_date = date('Y-m-d H:i:s');
					          $log_student_id = $targetUserID;
					          $log_old_status = $curr_LeaveStatus;
					          $log_new_status = CARD_LEAVE_PM;
					          $log_sql = $sql;
					          $log_admin_user = "terminal";
					          $log_time = "id=9,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
					
					                       $log_page = 'receiver.php';
					                      $log_content = get_file_content($log_filepath);
					                      $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
					                      $log_content .= $log_entry;
					                      write_file_content($log_content, $log_filepath);
					      }
					 
					 /*
					 if ($directProfileInput)
					 {
					     # Add PM Early Leave to student profile
					     $year = getCurrentAcademicYear();
					     $semester = getCurrentSemester();
					     $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
					     $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
					     $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
					     $db_engine->db_db_query($sql);
					     $insert_id = $db_engine->db_insert_id();
					     # Update to reason table
					     $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
					     $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
					     $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
					                    VALUES ($fieldsvalues)";
					     $db_engine->db_db_query($sql);
					 }
					 */
					
					 if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
					 {
					         echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
					 }
					 else
					 {
					         echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					 }
					 if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       		Notify_Student_Status($targetUserID,$current_time,'L');
			       	 }
					 intranet_closedb();
					 exit();
					
					}
				}
			}
			else # AM Present/Late
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
			   if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
			#                   if ($curr_LunchOutTime != "" && $curr_LunchBackTime == "")      # Gone out lunch and back late
			   {
			   		// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
						if (($ts_now - $ts_lunchEnd) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
							# Response
					    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					    intranet_closedb();
					    exit();
						}
						else {
							
							$lunch_misc_no_need_record = $Settings['NoRecordLunchOut'];
			   				$lunch_misc_once_only = $Settings['LunchOutOnce'];
			  	 			$lunch_misc_all_allowed = $Settings['AllAllowGoOut'];
			   				$MinsTreatAsPMIn = $Settings['MinsToTreatAsPMIn'];
							$allow_lunch_out = true;
							
							if(!$lunch_misc_all_allowed){
								 # Check in Database
				                $sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST
				                               WHERE StudentID = '$targetUserID'";
				                $temp = $db_engine->returnVector($sql);
				                if ($temp[0] == "") # Lunch Out NOT allowed
				                {
				                	$allow_lunch_out = false;
				                }
							}
							
							if(!$lunch_misc_all_allowed && !$allow_lunch_out){
								# not allow to go out for lunch and present/late in AM, treat it as PM early leave, and btw set PM status as present
								
								# Mark Leave time, Leave Status as PM Early leave
						       $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."', LeaveStatus = '".CARD_LEAVE_PM."',
						                      LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
						                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						       $db_engine->db_db_query($sql);
								
								$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,true);
								$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
						    	### Bug Tracing
						        if($bug_tracing['smartcard_student_attend_status']){
						                $log_date = date('Y-m-d H:i:s');
						                $log_target_date = date('Y-m-d H:i:s');
						                $log_student_id = $targetUserID;
						                $log_old_status = $curr_LeaveStatus;
						                $log_new_status = CARD_LEAVE_PM;
						                $log_sql = $sql;
						                $log_admin_user = "terminal";
						                $log_time = "id=11,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
						
			                            $log_page = 'receiver.php';
			                            $log_content = get_file_content($log_filepath);
			                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                            $log_content .= $log_entry;
			                            write_file_content($log_content, $log_filepath);
						        }
									 
								if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
						       	{
						               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
						       	}
						        else
						        {
						               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
						        }
						       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
						       		Notify_Student_Status($targetUserID,$current_time,'L');
						       }
						       intranet_closedb();
						       exit();
								
							}
							
					       # Mark Lunch Back Time, PM status late
					       $sql = "UPDATE $dailylog_tablename SET LunchBackTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
					                      LunchBackStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
					                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					       $db_engine->db_db_query($sql);
							$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_PM,CARD_STATUS_ABSENT);
							$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
					    	if($existing_remark != ''){
								$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,$existing_remark);
							}
					    	### Bug Tracing
					        if($bug_tracing['smartcard_student_attend_status']){
					                $log_date = date('Y-m-d H:i:s');
					                $log_target_date = date('Y-m-d H:i:s');
					                $log_student_id = $targetUserID;
					                $log_old_status = $curr_PMStatus;
					                $log_new_status = CARD_STATUS_LATE;
					                $log_sql = $sql;
					                $log_admin_user = "terminal";
					                $log_time = "id=10,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
					
					                             $log_page = 'receiver.php';
					                            $log_content = get_file_content($log_filepath);
					                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
					                            $log_content .= $log_entry;
					                            write_file_content($log_content, $log_filepath);
					            }
					       
					       /*
					       if ($directProfileInput)
					       {
					           # Add PM Late to student profile
					           $year = getCurrentAcademicYear();
					           $semester = getCurrentSemester();
					           $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
					           $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
					           $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
					           $db_engine->db_db_query($sql);
					           $insert_id = $db_engine->db_insert_id();
					           # Update to reason table
					           $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
					           $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
					           $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
					                          VALUES ($fieldsvalues)";
					           $db_engine->db_db_query($sql);
					       }
					       */
					
					       echo output_env_str(CARD_RESP_BACK_FROM_LUNCH_LATE."###$targetName".$attend_lang['BackFromLunch_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
				       			//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
				       			if(!$sys_custom['StudentAttendance']['LunchNoPushMessage']){
				       				Notify_Student_Status($targetUserID,$current_time,'A');
				       			}
				       		}
					       intranet_closedb();
					       exit();
				    	}
			   }/*
			   else if ($curr_PMStatus == CARD_STATUS_OUTING)
			   {
			   }*/
			   else # AM Present, not go out lunch or already back from lunch => Early leave
			   {
			       # Mark Leave time, Leave Status as PM Early leave
			       $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
			                      LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
			
						$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
			    ### Bug Tracing
			        if($bug_tracing['smartcard_student_attend_status']){
			                $log_date = date('Y-m-d H:i:s');
			                $log_target_date = date('Y-m-d H:i:s');
			                $log_student_id = $targetUserID;
			                $log_old_status = $curr_LeaveStatus;
			                $log_new_status = CARD_LEAVE_PM;
			                $log_sql = $sql;
			                $log_admin_user = "terminal";
			                $log_time = "id=11,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";
			
			                             $log_page = 'receiver.php';
			                            $log_content = get_file_content($log_filepath);
			                            $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
			                            $log_content .= $log_entry;
			                            write_file_content($log_content, $log_filepath);
			            }
						 
						 /*
			       if ($directProfileInput)
			       {
			           # Add PM Early Leave to student profile
			           $year = getCurrentAcademicYear();
			           $semester = getCurrentSemester();
			           $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			           $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
			           $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			           $db_engine->db_db_query($sql);
			           $insert_id = $db_engine->db_insert_id();
			           # Update to reason table
			           $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			           $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
			           $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                          VALUES ($fieldsvalues)";
			           $db_engine->db_db_query($sql);
			       }
			       */
			
			       if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			       {
			               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
			       }
			       else
			       {
			               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       }
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       		Notify_Student_Status($targetUserID,$current_time,'L');
			       }
			       intranet_closedb();
			       exit();
			   }
			 	}
			}
		}
		else
		{
			if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else {
		    # Normal School Leave
		    # Mark Leave Status, Time
		    send_sms(2, $dailylog_tablename);
		    	if($curr_PMStatus == "NULL" || $curr_PMStatus == CARD_STATUS_ABSENT){
		        	// if PM status is absent or not set yet, update it to present and clear absent profile
		        	$more_update_fields = ",PMStatus='".CARD_STATUS_PRESENT."' ";
		        	if($curr_PMStatus == CARD_STATUS_ABSENT){
		        		$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
		        	}
		        }
				$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
	              LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' $more_update_fields
	              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
		
        ### Bug Tracing
        if($bug_tracing['smartcard_student_attend_status']){
          $log_date = date('Y-m-d H:i:s');
          $log_target_date = date('Y-m-d H:i:s');
          $log_student_id = $targetUserID;
          $log_old_status = $curr_LeaveStatus;
          $log_new_status = CARD_LEAVE_NORMAL;
          $log_sql = $sql;
          $log_admin_user = "terminal";
          $log_time = "id=12,ts_now = $ts_now,ts_leaveSchool = $ts_leaveSchool";

          $log_page = 'receiver.php';
          $log_content = get_file_content($log_filepath);
          $log_entry = "\"$log_page,$log_time\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
          $log_content .= $log_entry;
          write_file_content($log_content, $log_filepath);
        }
		
				# Remove Early Leave Record (AM/PM)
				$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
				              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
				                    AND AttendanceDate = '$today' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
				$db_engine->db_db_query($sql);
				
				# Remove Reason Record of Early Leave (AM/PM)
				$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
				              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
				                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
				$db_engine->db_db_query($sql);
		
				if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
				}
				else
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				}
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       	Notify_Student_Status($targetUserID,$current_time,'L');
			    }
				intranet_closedb();
				exit();
		 	}
		}
	}
	else if ($attendance_mode == 3) # 3 - WD w/o Lunch
	{
		if($sys_custom['StudentAttendance']['CustomizedTimeSlotSettings'] && count($customizedTimeSlotSettings)>0){
			if($ts_now < $ts_lunchStart){
				if ($curr_InSchoolTime=="" && ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT))
				{
					Handle_Customized_Time_Slots(PROFILE_DAY_TYPE_AM);
				}
			}else{
				if($curr_LunchBackTime=="" && ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)){
					Handle_Customized_Time_Slots(PROFILE_DAY_TYPE_PM);
				}
			}
		}
		
    if ($ts_now <= $ts_morningTime)
    {
    	if (is_array($PresetOuting[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_AM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_AM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_AM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else {
	      if ($curr_InSchoolTime=="" || $curr_InSchoolTime=="0" || $ts_now <= $curr_InSchoolTime )
	      {
	      	
	        # On Time
	        # Mark AM Status, Time
	        $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_PRESENT."',
	                       InSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
	                       WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	        $db_engine->db_db_query($sql);
	        
	        $lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM);
	        echo output_env_str(CARD_RESP_AM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
	        send_sms(1);
	        if($plugin['ASLParentApp'] || $plugin['eClassApp']){
				//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
				Notify_Student_Status($targetUserID,$current_time,'A');
			}
	        intranet_closedb();
	        exit();
	      }
	      else
	      {
	        echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
	        intranet_closedb();
	        exit();
	      }
	    }
    }
    else if ($ts_now < $ts_lunchStart)  # In AM Lesson Time
    {
    	if (is_array($PresetOuting[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_AM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_AM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_AM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_AM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else {
				if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT)
				{
					// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
					if (($ts_now - $ts_morningTime) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
						# Response
				    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				    intranet_closedb();
				    exit();
					}
					else {
						
					   # Late
					   # Mark AM Status, Time
					   $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', AMStatus = '".CARD_STATUS_LATE."',
					                  InSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
					                  WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					   $db_engine->db_db_query($sql);
						$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_AM,CARD_STATUS_ABSENT);	
						 $lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
						 if($existing_remark != ''){
							$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_AM,CARD_STATUS_LATE,$existing_remark);
						 }
						 /*
					   if ($directProfileInput)
					   {
					       # Add late record to student profile
					       $year = getCurrentAcademicYear();
					       $semester = getCurrentSemester();
					       $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
					       $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
					       $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
					       $db_engine->db_db_query($sql);
					       $insert_id = $db_engine->db_insert_id();
					       # Update to reason table
					       $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
					       $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
					       $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
					                      VALUES ($fieldsvalues)";
					       $db_engine->db_db_query($sql);
					       if ($plugin['Discipline'])
					       {
					           # For Discipline System upgrade
					           include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
					           $ldiscipline = new libdiscipline();
					           $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
					           $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
					       }
					   }
					   */
					
					   # Response
					   echo output_env_str(CARD_RESP_AM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					   if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       			//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       			Notify_Student_Status($targetUserID,$current_time,'A');
			       		}
					   intranet_closedb();
					   exit();
					}
				}
				else
				{
				   # Early Leave
				   # Mark Leave Status, Time
				   $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
				                  LeaveSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
				                  WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				   $db_engine->db_db_query($sql);
					 
					 $lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
					 
					 /*
				   if ($directProfileInput)
				   {
				       # Add Early Leave record to student profile
				       $year = getCurrentAcademicYear();
				       $semester = getCurrentSemester();
				       $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
				       $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
				       $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
				       $db_engine->db_db_query($sql);
				       $insert_id = $db_engine->db_insert_id();
				       # Update to reason table
				       $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
				       $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
				       $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
				                      VALUES ($fieldsvalues)";
				       $db_engine->db_db_query($sql);
				   }
				   */
				
				   if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
				   {
				           echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
				   }
				   else
				   {
				           echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				   }
				   if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       		Notify_Student_Status($targetUserID,$current_time,'L');
			       }
				   intranet_closedb();
				   exit();
				}
			}
    }
    else if ($ts_now <= $ts_lunchEnd) # In Lunch Time
    {
			if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING || is_array($PresetOuting[PROFILE_DAY_TYPE_AM]) || is_array($PresetAbsence[PROFILE_DAY_TYPE_AM]))  # PM Present (AM Absent/Outing/PresetOuting/PresetAbsent)
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
			   if (($curr_PMStatus==$null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT) && ($curr_InSchoolTime=="" || $curr_InSchoolTime > $ts_now))
			   {
			   	
		        # Mark PM Status, InSchool Time
		        $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_PRESENT."',
		                       InSchoolStation = '$sitename',
		                       LunchBackTime = '$time_string', LunchBackStation = '$sitename',
		                       PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
		                       WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		        $db_engine->db_db_query($sql);
		        
		        $lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
		        echo output_env_str(CARD_RESP_PM_IN_SCHOOL_ONTIME."###$targetName".$attend_lang['InSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		        if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			    	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			    	Notify_Student_Status($targetUserID,$current_time,'A');
			    }
		        intranet_closedb();
		        exit();
			   }
			   else
			   {
		        echo output_env_str(CARD_RESP_IGNORE_WITHIN_PERIOD."###$targetName".$attend_lang['AlreadyPresent']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		        intranet_closedb();
		        exit();
			   }
			 	}
			}
			else
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
					# Mark Leave time, Leave Status as PM Early leave
					$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_AM."',
					          LeaveSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
					          WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_AM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
					/*
					if ($directProfileInput)
					{
						# Add PM Early Leave to student profile
						$year = getCurrentAcademicYear();
						$semester = getCurrentSemester();
						$fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
						$fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
						$sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
						$db_engine->db_db_query($sql);
						$insert_id = $db_engine->db_insert_id();
						# Update to reason table
						$fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
						$fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
						$sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
						              VALUES ($fieldsvalues)";
						$db_engine->db_db_query($sql);
					}
					*/
					
					if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
					{
					   echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
					}
					else
					{
					   echo output_env_str(CARD_RESP_AM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					}
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       		Notify_Student_Status($targetUserID,$current_time,'L');
			        }
					intranet_closedb();
					exit();
					
					/*
					# Normal Leave (Let PM absent to handle)
					$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
					              LeaveSchoolStation = '$sitename', DateModified = now(), ModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
					              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
					{
					       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###123213$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
					}
					else
					{
					       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###123213$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					}
					intranet_closedb();
					exit();
					*/
				}
			}
    }
    else if ($ts_now < $ts_leaveSchool) # PM Lesson Time
    {
			if ($curr_AMStatus == $null_record_variable || $curr_AMStatus == CARD_STATUS_ABSENT || $curr_AMStatus==CARD_STATUS_OUTING || is_array($PresetOuting[PROFILE_DAY_TYPE_AM]) || is_array($PresetAbsence[PROFILE_DAY_TYPE_AM]))  # PM Present (AM Absent/Outing/PresetOuting/PresetAbsent)
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
			   if (($curr_PMStatus==$null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT))
			   {
			   	// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
					if (($ts_now - $ts_lunchEnd) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0)  != 0)) { 
						# Response
				    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				    intranet_closedb();
				    exit();
					}
					else {
						
		        # Mark InSchool Time, PM status late
		        $sql = "UPDATE $dailylog_tablename SET InSchoolTime = '$time_string', PMStatus = '".CARD_STATUS_LATE."',
		                       InSchoolStation = '$sitename',
		                       LunchBackTime = '$time_string', LunchBackStation = '$sitename',
		                       PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
		                       WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
		        $db_engine->db_db_query($sql);
						$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_PM,CARD_STATUS_ABSENT);
						$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
						if($existing_remark != ''){
							$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,$existing_remark);
						}
						/*
		        if ($directProfileInput)
		        {
		            # Add PM late to student profile
		            $year = getCurrentAcademicYear();
		            $semester = getCurrentSemester();
		            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
		            $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
		            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
		            $db_engine->db_db_query($sql);
		            $insert_id = $db_engine->db_insert_id();
		            # Update to reason table
		            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		            $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
		            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                           VALUES ($fieldsvalues)";
		            $db_engine->db_db_query($sql);
		
		            if ($plugin['Discipline'])
		            {
		                # For Discipline System upgrade
		                include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
		                $ldiscipline = new libdiscipline();
		                $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
		                $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
		            }
		        }
		        */
		
		        # Response
		        echo output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
		        if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		Notify_Student_Status($targetUserID,$current_time,'A');
			    }
		        intranet_closedb();
		        exit();
			    }
			   }
			   else
			   {
			       # Mark Leave time, Leave Status as PM Early leave
			       $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
			                      LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
						 
						 $lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
						 /*
			       if ($directProfileInput)
			       {
			           # Add PM Early Leave to student profile
			           $year = getCurrentAcademicYear();
			           $semester = getCurrentSemester();
			           $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			           $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
			           $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			           $db_engine->db_db_query($sql);
			           $insert_id = $db_engine->db_insert_id();
			           # Update to reason table
			           $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			           $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
			           $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                          VALUES ($fieldsvalues)";
			           $db_engine->db_db_query($sql);
			       }
			       */
			
			       if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			       {
			               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
			       }
			       else
			       {
			               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       }
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       		Notify_Student_Status($targetUserID,$current_time,'L');
			       }
			       intranet_closedb();
			       exit();
			
			   }
			 	}
			}
			else
			{
				if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
					$lc->Record_Raw_Log($targetUserID,$current_time);
					// Record in time but not touch status
					if(trim($curr_InSchoolTime) == ""){
						$sql = "UPDATE $dailylog_tablename SET 
									InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
									PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
								WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
						$db_engine->db_db_query($sql);
						
						if($plugin['ASLParentApp'] || $plugin['eClassApp']){
							Notify_Student_Status($targetUserID,$current_time,'A');
						}
					}
					echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
					intranet_closedb();
					exit();
				}
				else {
			   if ($curr_PMStatus == $null_record_variable || $curr_PMStatus == CARD_STATUS_ABSENT)
			   {
			   	// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
					if (($ts_now - $ts_lunchEnd) > ($lc->IgnoreLateTapCardMins * 60) && (($lc->IgnoreLateTapCardMins+0) != 0)) { 
						# Response
				    echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['OverLateMinWarning'].$lc->IgnoreLateTapCardMins.$attend_lang['OverLateMinWarning1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				    intranet_closedb();
				    exit();
					}
					else {
						
			        # Mark InSchool Time, PM status late
			        $sql = "UPDATE $dailylog_tablename SET PMStatus = '".CARD_STATUS_LATE."',
			                       LunchBackTime = '$time_string', LunchBackStation = '$sitename',
			                       PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                       WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			        $db_engine->db_db_query($sql);
						 	$existing_remark = Get_Profile_Office_Remark($today,$targetUserID,PROFILE_DAY_TYPE_PM,CARD_STATUS_ABSENT);
						 	$lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
			        		if($existing_remark != ''){
								$lc->updateOfficeRemark($targetUserID,$today,PROFILE_DAY_TYPE_PM,CARD_STATUS_LATE,$existing_remark);
							}
			        /*
			        if ($directProfileInput)
			        {
			            # Add PM late to student profile
			            $year = getCurrentAcademicYear();
			            $semester = getCurrentSemester();
			            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			            $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
			            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			            $db_engine->db_db_query($sql);
			            $insert_id = $db_engine->db_insert_id();
			            # Update to reason table
			            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			            $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
			            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                           VALUES ($fieldsvalues)";
			            $db_engine->db_db_query($sql);
			
			            if ($plugin['Discipline'])
			            {
			                # For Discipline System upgrade
			                include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
			                $ldiscipline = new libdiscipline();
			                $ldiscipline->calculateUpgradeLateToDemerit($targetUserID);
			                $ldiscipline->calculateUpgradeLateToDetention($targetUserID);
			            }
			        }
			        */
			
			        # Response
			        echo output_env_str(CARD_RESP_PM_IN_SCHOOL_LATE."###$targetName".$attend_lang['InSchool_late']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			        if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'A'));
			       		Notify_Student_Status($targetUserID,$current_time,'A');
			       	}
			        intranet_closedb();
			        exit();
			    }
			   }
			   else
			   {
			       # Mark Leave time, Leave Status as PM Early leave
			       $sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_PM."',
			                      LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
			                      WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
			       $db_engine->db_db_query($sql);
						 
						 $lc->Set_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',true,$upadteeDisLateRecord);
						 
						 /*
			       if ($directProfileInput)
			       {
			           # Add PM Early Leave to student profile
			           $year = getCurrentAcademicYear();
			           $semester = getCurrentSemester();
			           $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
			           $fieldvalue = "'$targetUserID','$today','$year','$semester','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
			           $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
			           $db_engine->db_db_query($sql);
			           $insert_id = $db_engine->db_insert_id();
			           # Update to reason table
			           $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			           $fieldsvalues = "'$today', '$targetUserID', '$insert_id', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
			           $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                          VALUES ($fieldsvalues)";
			           $db_engine->db_db_query($sql);
			       }
			       */
			
			       if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
			       {
			               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
			       }
			       else
			       {
			               echo output_env_str(CARD_RESP_PM_LEAVE."###$targetName".$attend_lang['LeaveSchool_early']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
			       }
			       if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			       		//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			       		Notify_Student_Status($targetUserID,$current_time,'L');
			       }
			       intranet_closedb();
			       exit();
			
			
			   }
			 	}
			}
    }
    else
    {
    	if (is_array($PresetOuting[PROFILE_DAY_TYPE_PM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetOut1'].$PresetOuting[PROFILE_DAY_TYPE_PM]["Message"].$attend_lang['PresetOut2'].$PresetOuting[PROFILE_DAY_TYPE_PM]["PIC"].$attend_lang['PresetOut3']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else if (is_array($PresetAbsence[PROFILE_DAY_TYPE_PM])) {
				$lc->Record_Raw_Log($targetUserID,$current_time);
				// Record in time but not touch status
				if(trim($curr_InSchoolTime) == ""){
					$sql = "UPDATE $dailylog_tablename SET 
								InSchoolTime = '$time_string', InSchoolStation = '$sitename', 
								PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' 
							WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
					$db_engine->db_db_query($sql);
					
					if($plugin['ASLParentApp'] || $plugin['eClassApp']){
						Notify_Student_Status($targetUserID,$current_time,'A');
					}
				}
				echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###$targetName".$attend_lang['PresetAbsence'].$PresetAbsence[PROFILE_DAY_TYPE_PM]["Reason"].$attend_lang['PresetAbsence1']."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				intranet_closedb();
				exit();
			}
			else {
		        # Normal School Leave
		        # Mark Leave Status, Time
		        if($curr_PMStatus == "NULL" || $curr_PMStatus == CARD_STATUS_ABSENT){
		        	// if PM status is absent or not set yet, update it to present and clear absent profile
		        	$more_update_fields = ",PMStatus='".CARD_STATUS_PRESENT."' ";
		        	if($curr_PMStatus == CARD_STATUS_ABSENT){
		        		$lc->Clear_Profile($targetUserID,$today,PROFILE_DAY_TYPE_PM);
		        	}
		        }
		        send_sms(2, $dailylog_tablename);
				$sql = "UPDATE $dailylog_tablename SET LeaveSchoolTime = '$time_string', LeaveStatus = '".CARD_LEAVE_NORMAL."',
				              LeaveSchoolStation = '$sitename', PMDateModified = now(), PMModifyBy = '".$targetUserID."', LastTapCardTime = '$time_string' $more_update_fields
				              WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
				$db_engine->db_db_query($sql);
				
				# Remove Early Leave Record (AM/PM)
				$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
				              WHERE RecordType = '".PROFILE_TYPE_EARLY."' AND UserID = '$targetUserID'
				                    AND AttendanceDate = '$today' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
				$db_engine->db_db_query($sql);
				
				# Remove Reason Record of Early Leave (AM/PM)
				$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
				              WHERE RecordDate = '$today' AND StudentID = '$targetUserID'
				                    AND RecordType = '".PROFILE_TYPE_EARLY."' AND (DayType = '".PROFILE_DAY_TYPE_PM."' OR DayType='".PROFILE_DAY_TYPE_AM."')";
				$db_engine->db_db_query($sql);
				
				if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg###$t_leave_option");
				}
				else
				{
				       echo output_env_str(CARD_RESP_NORMAL_LEAVE."###$targetName".$attend_lang['LeaveSchool']."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
				}
				if($plugin['ASLParentApp'] || $plugin['eClassApp']){
			    	//$lc->Notify_Student_Status(array('UserID'=>$targetUserID,'AccessDateTime'=>$current_time,'Status'=>'L'));
			    	Notify_Student_Status($targetUserID,$current_time,'L');
			    }
				intranet_closedb();
				exit();
     	}
    }
	}
	else
	{
	   echo output_env_str(CARD_RESP_SYSTEM_NOT_INIT."###".$attend_lang['SystemNotInit']);
	   intranet_closedb();
	   exit();
	}
}
else     # Card Not Registered
{
  echo output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']);
  intranet_closedb();
  exit();
}


intranet_closedb();

function checkTimeResult($ts_recordID,$ts_morningTime,$ts_lunchStart,$ts_lunchEnd,$ts_leaveSchool,$ts_nonSchoolDay)
{
	global $db_engine;
	
	$sql = "select sec_to_time(".$ts_morningTime."), sec_to_time(".$ts_lunchStart."), sec_to_time(".$ts_lunchEnd."), sec_to_time(".$ts_leaveSchool.")";
	$aaaa = $db_engine->returnArray($sql,4);
	list($bb, $cc, $dd, $ee) = $aaaa[0];
	echo "<br>ts_recordID---><b><font color = \"red\">".$ts_recordID."</font></b>";
	echo "<br>ts_morningTime--->".$ts_morningTime." <b><font color = \"red\">[".$bb."]</font></b>";
	echo "<br>ts_lunchStart--->".$ts_lunchStart."<b><font color = \"red\"> [".$cc."]</font></b>";
	echo "<br>ts_lunchEnd--->".$ts_lunchEnd." <b><font color = \"red\">[".$dd."]</font></b>";
	echo "<br>ts_leaveSchool--->".$ts_leaveSchool." <b><font color = \"red\">[".$ee."]</font></b>";
	echo "<br>ts_nonSchoolDay---><b><font color = \"red\">".$ts_nonSchoolDay."</font></b><br><br><br>";

}

function Notify_Student_Status($TargetUserID,$AccessDateTime,$Status)
{
	global $sys_custom;
	if(isset($sys_custom['cardapi_http_protocol']) && in_array($sys_custom['cardapi_http_protocol'],array('http','https'))){
		$http = $sys_custom['cardapi_http_protocol'];
	}else{
		$http = checkHttpsWebProtocol()? "https" : "http";
		//$http = "http"; // must use http as https requires to bypass cert and cater different IP addresses for cloud clients
	}
	$is_https = $http == "https";
	$server_name = (isset($sys_custom['cardapi_host_ip']) && $sys_custom['cardapi_host_ip']!='')? $sys_custom['cardapi_host_ip'] : $_SERVER['SERVER_NAME'];
	$http_host = $http."://".$server_name.(!in_array($_SERVER["SERVER_PORT"],array(80,443))?":".$_SERVER["SERVER_PORT"]:"");
	$cmd = "/usr/bin/nohup wget".($is_https?" --no-check-certificate":"")." -q '$http_host/cardapi/attendance/notify_student_status.php?TargetUserID=".OsCommandSafe($TargetUserID)."&AccessDateTime=".OsCommandSafe($AccessDateTime)."&Status=".OsCommandSafe($Status)."' > /dev/null &";
	shell_exec($cmd);
}

### $sys_custom['StudentAttendance']['CustomizedTimeSlotSettings']
function Handle_Customized_Time_Slots($am_pm)
{
	global $plugin, $sys_custom, $dailylog_tablename, $attendance_mode, $current_time, $time_string, $sitename, $targetUserID, $targetName, $targetUserLogin, $today, $day, $db_engine, $lc, $attend_lang, $t_remind_id, $t_remind_msg, $upadteeDisLateRecord, $customizedTimeSlotSettings;
	if($am_pm == PROFILE_DAY_TYPE_AM){
		$time_field = 'InSchoolTime';
		$station_field = 'InSchoolStation';
		$status_field = 'AMStatus';
		$date_modified_field = 'DateModified';
		$modify_by_field = 'ModifyBy';
		$response_code = $customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_LATE? CARD_RESP_AM_IN_SCHOOL_LATE : CARD_RESP_AM_IN_SCHOOL_ONTIME;
		$attend_lang_key = $customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_LATE? 'InSchool_late' : 'InSchool';
	}else if($am_pm == PROFILE_DAY_TYPE_PM)
	{
		$time_field = $attendance_mode==1? 'InSchoolTime' : 'LunchBackTime'; // $attendance_mode==1 means PM mode
		$station_field = $attendance_mode==1? 'InSchoolStation' : 'LunchBackStation';
		$status_field = 'PMStatus';
		$date_modified_field = 'PMDateModified';
		$modify_by_field = 'PMModifyBy';
		$response_code = $customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_LATE? CARD_RESP_PM_IN_SCHOOL_LATE : CARD_RESP_PM_IN_SCHOOL_ONTIME;
		$attend_lang_key = $customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_LATE? 'InSchool_late' : ($attendance_mode==1? 'InSchool' : 'BackFromLunch');
	}
	$sql = "UPDATE $dailylog_tablename SET $time_field = '$time_string', $status_field = '".$customizedTimeSlotSettings[0]['Status']."',
             $station_field = '$sitename', $date_modified_field = now(), $modify_by_field = '".$targetUserID."', LastTapCardTime = '$time_string' 
             WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
	$db_engine->db_db_query($sql);
	if($customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_PRESENT){
		$lc->Clear_Profile($targetUserID,$today,$am_pm);
		echo output_env_str($response_code."###$targetName".$attend_lang[$attend_lang_key]."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
      	send_sms(1);
	}else if($customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_ABSENT){
		$lc->Set_Profile($targetUserID,$today,$am_pm,CARD_STATUS_ABSENT,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',false);
		echo output_env_str($response_code."###$targetName".$attend_lang[$attend_lang_key]."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
	}else if($customizedTimeSlotSettings[0]['Status'] == CARD_STATUS_LATE){
		$lc->Set_Profile($targetUserID,$today,$am_pm,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$time_string,false,'|**NULL**|',false,$upadteeDisLateRecord);
		echo output_env_str($response_code."###$targetName".$attend_lang[$attend_lang_key]."$time_string"."###$targetUserLogin"."###$t_remind_id###$t_remind_msg");
	}
	if($plugin['ASLParentApp'] || $plugin['eClassApp']){
		if(!($am_pm == PROFILE_DAY_TYPE_PM && $sys_custom['StudentAttendance']['LunchNoPushMessage'])){
  			Notify_Student_Status($targetUserID,$current_time,'A');
		}
  	}
	intranet_closedb();
	exit();
}

function Get_Profile_Office_Remark($TargetDate,$StudentID,$DayType,$RecordType)
{
	global $lc;
	
	$sql = "SELECT OfficeRemark FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$StudentID' AND DayType='$DayType' AND RecordType='$RecordType'";
	$records = $lc->returnResultSet($sql);
	$remark = count($records)>0? $records[0]['OfficeRemark'] : '';
	return $remark;
}

?>