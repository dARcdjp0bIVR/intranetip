<?php
# modifying by: anna
/*
 * 2020-11-06 Ray: Modified update_request_log_login_eClassApp add SessionID
 * 2019-05-13 Anna: Added '' to avoid sql injection
 * 2019-03-11 Carlos: Modified update_request_log_login_eClassApp($userID) to record every login record. (in new table APP_LOGIN_RAW_LOG)
 */
class libwebservice extends libdb {

     function libwebservice(){
          $this->libdb();
     }

	 function update_request_log_lastrequest($userID, $requestXML, $apikey){
	 	/* Removed the FileByteStream Content to save DB memory */
	 	if(strpos($requestXML, '<RequestMethod>AppFileUpload</RequestMethod>')!==false){
	 		$search_pattern = '/<FileByteStream><!\[CDATA\[.*\]\]><\/FileByteStream>/';
	 		$replacement	= '<FileByteStream><![CDATA[FileByteStream is removed in function update_request_log_lastrequest of libwebservice.php]]</FileByteStream>';
	 		$requestXML 	= preg_replace($search_pattern, $replacement, $requestXML, 1);
	 	}
	 	
	 	$requestXML = mysql_real_escape_string($requestXML); 
	 	$apikey = mysql_real_escape_string($apikey);
	 	
	 	$record = $this->get_request_log($userID);
     	if($record === false){
     		$sql = "INSERT INTO INTRANET_API_REQUEST_LOG (UserID, LastRequestTime, LastRequestXML, apikey) VALUES ($userID, now(), '$requestXML', '$apikey')";     	
     	}
     	else{
     		$sql = "UPDATE INTRANET_API_REQUEST_LOG SET LastRequestTime = now(), LastRequestXML = '$requestXML', apikey = '$apikey' WHERE UserID = $userID";
     	}
	 	
	 	
	 	$result = $this->db_db_query($sql);
     	return $result;
	 }

     function update_request_log_login($userID){     	
     	$record = $this->get_request_log($userID);
     	if($record === false){
     		$sql = "INSERT INTO INTRANET_API_REQUEST_LOG (UserID, LastLoginTime) VALUES ('$userID', now())";     	
     	}
     	else{
     	    $sql = "UPDATE INTRANET_API_REQUEST_LOG SET LastLoginTime = now() WHERE UserID = '".IntegerSafe($userID)."'";
     	}
     	$result = $this->db_db_query($sql);
     	return $result;
     }
     
     function get_request_log($userID){
         $sql = "SELECT * FROM INTRANET_API_REQUEST_LOG WHERE UserID = '".IntegerSafe($userID)."'";
		$result = $this->returnArray($sql);
		if(count($result) > 0) {
			return $result[0];	
		}
		
		return false;
     }
     
     function update_request_log_login_eClassApp($userID, $SessionID=''){
     	$ip_address = getRemoteIpAddress();   	
     	$record = $this->get_request_log_eClassApp($userID);
     	if($record === false){
     		$sql = "INSERT INTO APP_LOGIN_LOG (UserID, LastLoginTime, IPAddress) VALUES ('$userID', now(), '".$ip_address."')";     	
     	}
     	else{
     		$sql = "UPDATE APP_LOGIN_LOG SET IPAddress = '".$ip_address."', LastLoginTime = now() WHERE UserID = '".IntegerSafe($userID)."'";
     	}
     	$this->db_db_query($sql);
     	
     	$sql = "INSERT INTO APP_LOGIN_RAW_LOG (UserID,LoginTime,IPAddress) VALUES ('$userID',NOW(),'$ip_address')";
     	$result = $this->db_db_query($sql);

     	$SessionID = trim($SessionID);
     	if(!empty($SessionID)) {
			$sql = "SELECT * FROM APP_LOGIN_SESSION WHERE SessionID='$SessionID'";
			$rs = $this->returnArray($sql);
			if(count($rs) > 0) {
				$sql = "UPDATE APP_LOGIN_SESSION SET RecordStatus=1 WHERE RecordID='".$rs[0]['RecordID']."'";
				$this->db_db_query($sql);
			} else {
				$sql = "INSERT INTO APP_LOGIN_SESSION (UserID, SessionID, RecordStatus,	DateInput) VALUES ('$userID','$SessionID', 1, now())";
				$this->db_db_query($sql);
			}
		}
     	return $result;
     }
     
     function get_request_log_eClassApp($userID){
     	$sql = "SELECT * FROM APP_LOGIN_LOG WHERE UserID = '".IntegerSafe($userID)."'";
		$result = $this->returnArray($sql);
		if(count($result) > 0) {
			return $result[0];	
		}
		
		return false;
     }
}
?>