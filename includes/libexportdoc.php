<?php
include_once("{$intranet_root}/includes/ies/libies_survey.php");
	class libexportdoc extends libies_survey
	{
		
		var $docFile='';
		var $title='';
		var $style='';
		var $htmlheader='';
		var $htmlHead='';
		var $htmlBody='';
		#-------------------------------------
		
		function libexportdoc()
		{
			$this->title="Untitled Document";
			$this->htmlHead="";
			$this->htmlBody="";
			$this->libdb();
		}
		/**
		 * set html header
		 *
		 * @return /
		 */
		function sethtmlheader($header){
			$this->htmlheader = str_replace("</head>",'',$header);;
		}
		/**
		 * Get html header
		 *
		 * @return String
		 */
		function gethtmlheader(){
			return (empty($this->htmlheader))?"<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>":$this->htmlheader;
		}
		/**
		 * set doc file Name
		 *
		 * @return String
		 */
		function setDocFileName($docfile)
		{
			$this->docFile=$docfile;
			if(!preg_match("/\.doc$/i",$this->docFile))
				$this->docFile.=".doc";
			return;		
		}
		/**
		 * Parse the html and remove <head></head> part if present into html
		 *
		 * @param String $html
		 * @return void
		 * @access Private
		 */
		
		function _parseHtml($html)
		{
			$html=preg_replace("/<!DOCTYPE((.|\n)*?)>/ims","",$html);
			$html=preg_replace("/<script((.|\n)*?)>((.|\n)*?)<\/script>/ims","",$html);
			preg_match("/<head>((.|\n)*?)<\/head>/ims",$html,$matches);
			$head=$matches[1];
			preg_match("/<title>((.|\n)*?)<\/title>/ims",$head,$matches);
			$this->title = $matches[1];
			$html=preg_replace("/<head>((.|\n)*?)<\/head>/ims","",$html);
			$head=preg_replace("/<title>((.|\n)*?)<\/title>/ims","",$head);
			$head=preg_replace("/<\/?head>/ims","",$head);
			$html=preg_replace("/<\/?body((.|\n)*?)>/ims","",$html);
			$this->htmlHead=$head;
			$order   = array("\r\n", "\n", "\r");
			$replace = '<br />';
			$this->htmlBody= str_replace($order,$replace,$html);
			return;
		}
		/**
		 * Return Document footer
		 *
		 * @return String
		 */		 
		function getFotter()
		{
			return "</body></html>";
		}
		/**
		 * Return head close
		 *
		 * @return String
		 */		 
		function get_close_head()
		{
			return "</head>";
		}
		
		/**
		 * set Document style
		 *
		 * @return /
		 */
		function setstyle($style)
		{
			$this->style = "<style type=\"text/css\">" . $style . "</style>";
		}
		/**
		 * Return Document style
		 *
		 * @return String
		 */
		function getstyle()
		{
			return (empty($this->style)) ? "" : $this->style;
		}
		function get_open_body()
		{
			return "<body>";
		}
		
		/**
		 * Create The MS Word Document from given HTML
		 *
		 * @param String $html :: HTML Content or HTML File Name like path/to/html/file.html
		 * @param String $file :: Document File Name
		 * @param Boolean $download :: Wheather to download the file or save the file
		 * @return boolean 
		 */
		function createDoc($html,$file,$download=TRUE)
		{
			
			
			
			$this->_parseHtml($html);
			$this->setDocFileName($file);
			
			$doc .= $this->gethtmlheader();  //<html><head>
			$doc .= $this->getstyle();   //<style></style>
			$doc .= $this->get_close_head();  // </head>
			$doc .= $this->get_open_body(); //<body>
			$doc .=$this->htmlBody;  //content
			$doc .=$this->getFotter();  //</body></html>
			
			if($download){
				
				header('Pragma: public'); 
				header("Content-type: application/msword"); //#-- build header to download the excel file 
				header("Content-Disposition: attachment; filename=$this->docFile"); 
				echo $doc;
				return true;
			}
			
			
			return $doc;
			
			
		}
		/**
		 * Return <ol>
		 *
		 * @return String
		 */
		function get_ol_open(){
			return "<ol>";
		}
		/**
		 * Return </ol>
		 *
		 * @return String
		 */
		function get_ol_close(){
			return "</ol>";
		}
		/**
		 * Return </ol>
		 *
		 * @return String
		 */
		function get_li_open(){
			return "<li>";
		}
		/**
		 * Return </ol>
		 *
		 * @return String
		 */
		function get_li_close(){
			return "</li>";
		}
		/**
		 * @param String $class
		 * @param String $style
		 * Return <p class=$class style=$style>
		 *
		 * @return String
		 */
		function get_p_open($class='', $style=''){
			
			$return = "<p ";
			$return .= "class=\"".$class."\" ";
			$return .= "style=\"".$style."\">";
			return $return;
		}
		/**
		 * Return </p>
		 *
		 * @return String
		 */
		function get_p_close(){
			return "</p>";
		}
		/**
		 * @param String $class
		 * @param String $style
		 * Return <span>
		 *
		 * @return String
		 */
		function get_span_open($class='', $style=''){
			
			$return = "<span ";
			$return .= "class=\"".$class."\" ";
			$return .= "style=\"".$style."\">";
			return $return;
		}
		/**
		 * Return </span>
		 *
		 * @return String
		 */
		function get_span_close(){
			return "</span>";
		}
		/**
		 * Return page break in word .doc
		 *
		 * @return String
		 */
		function get_page_break(){
			return "<br clear=\"all\" style=\"page-break-before:always\" />";
		}
		/**
		 * param int $n which is the number of line break
		 * 
		 * Return line in word .doc
		 *
		 * @return String
		 */
		function get_next_line($n=1){
			$return = '';
			for($i=0;$i<$n;$i++)
				$return .= "<br />";
			return $return;
		}
		/**
		 * Return <center>
		 * 
		 * @return String
		 */
		function get_center_open(){
			return "<center>";
		}
		/**
		 * Return </center>
		 *
		 * @return String
		 */
		function get_center_close(){
			return "</center>";
		}
				/**
		 * Return <ul>
		 * 
		 * @return String
		 */
		function get_ul_open(){
			return "<ul>";
		}
		/**
		 * Return </ul>
		 *
		 * @return String
		 */
		function get_ul_close(){
			return "</ul>";
		}
		/**
		 * @param String $class
		 * @param String $style
		 * Return <table class=? style=?>
		 *
		 * @return String
		 */
		function get_table_open($class ="", $style =""){
			return "<table class='$class' style='$style'>";
		}
		/**
		 * Return </table>
		 *
		 * @return String
		 */
		function get_table_close(){
			return "</table>";
		}
	}
?>