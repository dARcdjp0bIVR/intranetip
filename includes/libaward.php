<?php
# using: 

#################################
#
#   Date:   2020-04-22 (Tommy)
#           modified displayAward(), display student in class record only
#
#	Date:	2011-03-03	YatWoon
#			update displayAward(), display "Whole Year" is semester is empty
#
#################################

if (!defined("LIBAWARD_DEFINED"))         // Preprocessor directives
{

 define("LIBAWARD_DEFINED",true);

 class libaward extends libclass{
       var $StudentAwardID;
       var $UserID;
       var $Year;
       var $Semester;
       var $AwardDate;
       var $AwardName;
       var $Organization;
       var $SubjectArea;
       var $Remark;

       function libaward($StudentAwardID="")
       {
                $this->libclass();
                if ($StudentAwardID != "")
                {
                    $this->StudentAwardID = $StudentAwardID;
                    $this->retrieveRecord($this->StudentAwardID);
                }
       }
       function retrieveRecord($id)
       {
                $fields = "UserID, Year, Semester, AwardDate, AwardName, SubjectArea, Remark, Organization";
                $conds = "StudentAwardID = '$id'";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_AWARD WHERE $conds";
                $result = $this->returnArray($sql,6);
                list(
                $this->UserID, $this->Year,$this->Semester,$this->AwardDate,
                $this->AwardName, $this->SubjectArea, $this->Remark, $this->Organization) = $result[0];
                return $result;
       }
       function returnYears($studentid=null)
       {
       		if (!is_null($studentid)) $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_AWARD WHERE UserID = '$studentid' ORDER BY Year DESC";
       		else $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_AWARD ORDER BY Year DESC";
          
          return $this->returnVector($sql);
       }
        function returnArchiveYears($studentid)
       {
                $sql = "SELECT DISTINCT Year FROM PROFILE_ARCHIVE_AWARD WHERE UserID = '$studentid' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }
       function getAwardCountByStudent($studentid)
       {
                $sql = "SELECT COUNT(StudentAwardID) FROM PROFILE_STUDENT_AWARD
                        WHERE UserID=$studentid";
                $entry = $this->returnVector($sql);
                return $entry[0];
       }
       function getAwardListByClass($classname)
       {
                $counts = array();
                $sql = "SELECT a.UserID, COUNT(a.StudentAwardID)
                        FROM PROFILE_STUDENT_AWARD as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.ClassName = '$classname'
                        GROUP BY a.UserID";
                $result = $this->returnArray($sql,2);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$awardCount) = $result[$i];
                     $counts[$StudentID] = $awardCount;
                }
                $studentList = $this->getStudentNameListByClassName($classname, "1");
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $classNumber) = $studentList[$i];
                     $returnResult[$i][0] = $StudentID;
                     $returnResult[$i][1] = $StudentName;
                     $returnResult[$i][2] = $classNumber;
                     $returnResult[$i][3] = $counts[$StudentID]+0;
                }
                return $returnResult;
                /*
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $temp = $this->getAwardCountByStudent($id);
                     $result[] = array($id,$name,$classnumber,$temp);
                }
                return $result;
                */
       }
       function getAwardByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, Semester, IF (AwardDate IS NULL,'-',AwardDate),AwardName, Remark, Organization
                               FROM PROFILE_STUDENT_AWARD WHERE UserID = '$studentid' $conds
                               ORDER BY Year DESC, Semester, AwardName";
                return $this->returnArray($sql,6);
       }
       function displayAward($studentid, $year="", $student_class=array())
       {
                global $image_path, $intranet_session_language;
                global $i_AwardDate,$i_AwardRemark,$i_AwardName;
                global $i_AwardYear,$i_AwardSemester,$i_AwardDate,$i_AwardRemark,$i_AwardName;
                global $i_AwardNoRecord, $Lang;
                $result = $this->getAwardByStudent($studentid, $year);
                $x = "
                  <table width='100%' border='0' cellpadding='4' cellspacing='0'>
                    <tr>
                      <td width='70' align='left' class='tablebluetop tabletopnolink'>$i_AwardYear</td>
                      <td width='110' align='left' class='tablebluetop tabletopnolink'>$i_AwardSemester</td>
                      <td width='90' align='left' class='tablebluetop tabletopnolink'>$i_AwardDate</td>
                      <td width='174' align='left' class='tablebluetop tabletopnolink'>$i_AwardName</td>
                      <td width='150' align='left' class='tablebluetop tabletopnolink'>$i_AwardRemark</td>
                    </tr>\n";
                
                $class = array();
                for($i = 0; $i < sizeof($student_class); $i++){
                    $class[] = $student_class[$i][0];
                }
                
                $in_class = array();
                for($i = 0; $i < sizeof($result); $i++){
                    if(in_array($result[$i]["Year"], $class))
                        $in_class[] = $result[$i];
                }
                
                for ($i=0; $i<sizeof($in_class); $i++)
                {
                    list($year,$sem,$aDate,$award,$remark) = $in_class[$i];
                     $display_sem = $sem ?$sem : $Lang['General']['WholeYear'];
                     $x .= "
                      <tr class='tablebluerow". ( $i%2 + 1 )."'>
                              <td align='left' class='tabletext'>$year</td>
                              <td align='left' class='tabletext'>$display_sem</td>
                              <td align='left' class='tabletext'>$aDate</td>
                              <td align='left' class='tabletext'>$award</td>
                              <td align='left' class='tabletext'>$remark&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan='5' align='center' class='tablebluerow2 tabletext'>$i_AwardNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       function displayAwardAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_AwardYear,$i_AwardSemester,$i_AwardDate,$i_AwardRemark,$i_AwardName;
                global $i_AwardNoRecord;
                $result = $this->getAwardByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
                      <td width=170 align=center class=tableTitle_new>$i_AwardName</td>
                      <td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($year,$sem,$aDate,$award,$remark) = $result[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$aDate</td>
                              <td align=center>$award</td>
                              <td align=center>$remark&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_AwardNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

        // ++++++ for archive student ++++++ \\

       function displayArchiveAwardAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_AwardYear,$i_AwardSemester,$i_AwardDate,$i_AwardRemark,$i_AwardName;
                global $i_AwardNoRecord;
                $result = $this->getArchiveAwardByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
                      <td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
                      <td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
                      <td width=170 align=center class=tableTitle_new>$i_AwardName</td>
                      <td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($year,$sem,$aDate,$award,$remark) = $result[$i];
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$aDate</td>
                              <td align=center>$award</td>
                              <td align=center>$remark&nbsp;</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_AwardNoRecord</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }

       function getArchiveAwardByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $sql = "SELECT Year, Semester, IF (RecordDate IS NULL,'-',RecordDate),AwardName, Remark
                               FROM PROFILE_ARCHIVE_AWARD WHERE UserID = '$studentid' $conds
                               ORDER BY Year DESC, Semester, AwardName";
                return $this->returnArray($sql,5);
       }

       // +++++ end of archive student +++++ \\

 }


} // End of directives
?>
