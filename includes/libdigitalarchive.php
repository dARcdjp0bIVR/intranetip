<?php
# modifying : 

### Change Log [Start] ###
#   Date    :   2020-09-21 (Tommy) - modified UserInGroupList(), changed sql for sorting like groups
#	Date	:	2020-01-08 (Sam) - Fix SQL query in GetMyGroupList() [2020-0107-1555-29073]
#   Date    :   2019-08-16 (Ray) - added Get_Duplicate_Admin_Document
#   Date    :   2019-06-27 (Ray) - added getGroupIdByCode, Get_AccessRight_Format_Array, Import_AccessRight_Record_To_Temp, Finalize_Import_AccessRight
#
#	Date	:	2018-06-07 (Carlos) - modified Get_Admin_Doc_AdvanceSearchResult_SQL() to left join INTRANET_USER and INTRANET_ARCHIVE_USER.
#	Date	:	2017-07-10 (Anna) - added GetDigitalArchiveGeneralSetting()
#
#	Date	:	2016-06-15 (Carlos) [ip2.5.7.8.1] - modified getDirectoryList(), fixed the query that are bound to document creator issue. 
#
#	Date	:	2016-01-21 (Carlos) [ip2.5.7.3.1] - added getParentFolderHierarchy(), buildParentFolderHierarchy(), getAllowIPInHierarchy(), isDocumentsAllowToDownload() to check whether documents are restricted to be downloaded in certain IP addresses. 
#
#	Date	: 	2015-04-08 (Carlos) [ip2.5.6.5.1] - modified SearchTags() to support multiple keywords and match all/partial keywords
#
#	Date	:	2014-10-03 (Carlos) [ip2.5.5.10.1] - added Authenticate_DigitalArchive() 
#
#	Date	:	2014-05-22 (Carlos)
#				modified GetMyGroupList(), module admin no need to join group member table, can manage all the groups
#				modified getAllAccessRightGroup(), count number of members on INTRANET_USER
#
#   Date    :   2014-05-12 (Tiffany)
#               add AllMatches() to find the documentID matches all tags for advance search.
#   Date    :   2014-02-21 (Tiffany)
#               add Get_Newest_More_SQL(),Get_Favourite_More_SQL(),Get_My_Subject_More_SQL() for handle campus tv.
#
#	Date	:	2013-12-20 (Henry Chan)
#				modified buildFileHierachy() to fix bug on bulid folder with chinese name
#
#	Date	:	2013-08-01 (Henry Chan)
#				added Has_Folder_In_Group() to get whether the folder contains subfolder
#
#	Date	:	2013-07-25 (Carlos)
#				added Get_Folder_Hierarchy_File_Count_Array() to get folder tree with file total count
#				added getFolderHierarchyFileCountArray(), getFolderHierarchySubfoldersFileCount() for Get_Folder_Hierarchy_File_Count_Array()
#
#	Date:	:	2013-07-16 (Rita)
#				amend getDeleteLogSql() to allow advance search 
#
#	Date	:	2013-07-11 (Henry Chan)
#				added Save_File_Format_Setting()
#				added Delete_File_Format_Setting()
#
#	Date	:	2013-07-09 (Henry Chan)
#				added Get_File_Format_Setting()
#
#	Date	:	2013-04-25	(yuen)
#				modified Get_Admin_Doc_AdvanceSearchResult_SQL() to support searching with tag
#
#	Date	:	2013-04-08	(Rita)
#				add Get_Subject_eResources_Record_ID_By_Group_Admin(),
#				modified Get_My_Subject_More_SQL(), Get_Subject_eResources_Group_Sql()
#
#	Date	:	2013-04-05	(Rita)
#				modified Get_Favourite_More_SQL() add checking
#
#	Date	:	2013-04-02 (Rita)
#				add Update_Subject_eResources_Tag_Name_By_Admin()
#
#	Date	:	2013-03-27 (Rita)
#				add Get_Subject_eResources_Group_Sql();
#
#	Date	:	2013-03-05 (Carlos)
#				fix getFolderHierarchyArray() parent child document ID relations 			
#
#	Date	:	2013-02-19 (Carlos)
#				modified MoveAdminRecord(), add checking of duplication with others files
#
#	Date	:	2013-02-07 (Carlos)
#				modified Get_Folder_Hierarchy_Array() to get all subfolders by one query
#
#	Date	:	2013-01-16 (Carlos)
#				add Is_Duplicate_Others_Document() - for checking duplication with others file
#
#	Date	: 	2013-01-07 (Rita)
#				add getDeleteLogSql();
#
#	Date	: 	2013-01-07 (Rita)
#				modifief deleteAdminRecord(), add info - upload user , upload time, group
#
#	Date	:	2012-07-24 (Henry Chow)
#				modified getAllAccessRightGroup(), handle special character on keyword search 
#
#	Date	:	2012-05-31 (Henry Chow)
#				modified getAllAccessRightGroup(), add keyword search feature
#
#	Date	:	2012-03-13 (Henry Chow)
#				modified Get_Latest_Document(), Get_Last_View_Document(), add checking of amount of record before sorting (ksort / krsort) 
#				modified Get_Admin_Doc_AdvanceSearchResult_SQL(), added Get_Viewable_Record_ID_By_GroupID(), return result for searching 
#	Date	:	2012-01-18 (Henry Chow)
#				added getFolderIDByFolderName(), return FolderID according to folder directory
#
#	Date	:	2011-11-03 (Henry Chow)
#				modified Check_Group_Title(), length of group name < 50 and file name < 127
#
#### Change Log [End] ####


###################################################
# this is the library for the "Tag Cloud" [Start] #
###################################################

class wordCloud {
	//var $version = '2.1';
	var $wordsArray = array();
	
	/*
	 * Custom format ourput of words
	 * 		 
	 * @transformation 
	 * upper and lower for change of case
	 * 
	 * @trim
	 * bool, applies trimming to word		 		 		 		 
	 */
	 
	
	var $formatting = array(
		'transformation' => '',
		'trim' => true			
	);
	
	
	/*
	 * PHP 4 Constructor
	 *
	 * @param array $words
	 * 		 
	 * @return void
	 */
	
	function wordCloud($words = false) {
		return $this->__construct($words);
	}
	
	/*
	 * PHP 5 Constructor
	 *
	 * @param array $words
	 * 		 
	 * @return void
	 */
	
	function __construct($words = false) {
		// If we are trying to parse some works, in any format / type
		if ($words !== false) {
			// If we have a string
			if (is_string($words)) {
				$this->addString($words);
			} elseif (count($words)) {
				foreach ($words as $key => $value) {
					$this->addWord($value);
				}
			}
		}
		return;
	}
	
	/*
	 * Convert a string into a cloud
	 *
	 * @param string $string
	 * @return void
	 */
	
	function addString($string, $seperator = ' ') {
		$inputArray = explode($seperator, $string);
		$wordArray = array();
		foreach ($inputArray as $inputWord) {
			$wordArray[]=$this->formatWord($inputWord);
		}
		$this->addWords($wordArray);
	}
	
	/*
	 * Parse word into safe format 
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	
	function formatWord($string) {
		
		if($this->formatting['transformation']){
			switch($this->formatting['transformation']){
				case 'upper':
					$string = mb_strtoupper($string);
					break;
				default:
	      			$string = mb_strtolower($string);
			}
		}
		
		if($this->formatting['trim']){
			$string = trim($string);
		}
		
		# this preg_replace cannot work to chinese word
		//return preg_replace('/[^a-z ]/', '', strip_tags($string));
		return strip_tags($string);
	}
	
	/*
	 * Display user friendly message, for debugging mainly
	 *
	 * @param string $string
	 * @param array $value
	 *
	 * @return bool
	 */
	
	function notify($string, $value)
	{
		echo '<pre>';
		print_r($string);
		print_r($value);
		echo '</pre>';
		return false;
	}
	
	/*
	 * Assign word to array
	 *
	 * @param string $word
	 * 		 
	 * @return string
	 */
	
	function addWord($wordAttributes = array()) {
		if (is_string($wordAttributes)) {
			$wordAttributes = array('word' => $wordAttributes);
		}
		$wordAttributes['word'] = $this->formatWord($wordAttributes['word']);
		
		if (!array_key_exists('size', $wordAttributes)) {
			$wordAttributes = array_merge($wordAttributes, array('size' => 1));
		}
		if (!array_key_exists('word', $wordAttributes)) {
			return $this->notify('no word attribute', print_r($wordAttributes, true));
		}
		$word = $wordAttributes['word'];
		
		if (empty($this->wordsArray[$word])) {
			$this->wordsArray[$word] = array();
		}
		if (!empty($this->wordsArray[$word]['size']) && !empty($wordAttributes['size'])) {
			$wordAttributes['size'] = ($this->wordsArray[$word]['size'] + $wordAttributes['size']);
		} elseif (!empty($this->wordsArray[$word]['size'])) {
			$wordAttributes['size'] = $this->wordsArray[$word]['size'];
		}		
		if (!empty($this->wordsArray[$word]['class']) && !empty($wordAttributes['class'])) {
			$wordAttributes['class'] = ($this->wordsArray[$word]['class'] + $wordAttributes['class']);
		} elseif (!empty($this->wordsArray[$word]['size'])) {
			$wordAttributes['class'] = $this->wordsArray[$word]['class'];
		}		
		$this->wordsArray[$word] = $wordAttributes;
		return $this->wordsArray[$word];
	}
	
	/*
	 * Assign multiple words to array
	 *
	 * @param string $word
	 * 		 
	 * @return string
	 */
	
	function addWords($words = array()) {
		debug_pr($words);
		if (!is_array($words)) {
			$words = func_get_args();
		}
		
		foreach ($words as $wordAttributes) {
			$this->addWord($wordAttributes);
		}					
	}
	
	/*
	 * Shuffle associated names in array
	 *
	 * @return array $this->wordsArray
	 */
	
	function shuffleCloud() {
		$keys = array_keys($this->wordsArray);
		shuffle($keys);
		if (count($keys) && is_array($keys)) {
			$tmpArray = $this->wordsArray;
			$this->wordsArray = array();
			foreach ($keys as $key => $value)
				$this->wordsArray[$value] = $tmpArray[$value];
		}
		return $this->wordsArray;
	}
	
	/*
	 * Get the class range using a percentage
	 *
	 * @returns int $class
	 */
	
	function getClassFromPercent($percent) {
		$percent = floor($percent);
		if ($percent >= 99)
			$class = 9;
		elseif ($percent >= 70)
			$class = 8;
		elseif ($percent >= 60)
			$class = 7;
		elseif ($percent >= 50)
			$class = 6;
		elseif ($percent >= 40)
			$class = 5;
		elseif ($percent >= 30)
			$class = 4;
		elseif ($percent >= 20)
			$class = 3;
		elseif ($percent >= 10)
			$class = 2;     
		elseif ($percent >= 5)
			$class = 1;
		else
			$class = 0;
		return $class;
	}
	
	/*
	 * Sets a limit for the amount of clouds
	 *
	 * @param string $limit		 
	 *		 
	 * @returns string $this->limitAmount
	 */
	
	function setLimit($limit) {
		if (!empty($limit)) {
			$this->limitAmount = $limit;
		}
		return $this->limitAmount;
	}
	
	/*
	 * Gets the limited amount of clouds
	 *
	 * @returns string $wordCloud
	 */
	
	function limitCloud() {
		$i = 0;
		$wordsArray = array();
		foreach ($this->wordsArray as $key => $value) {
			if ($i < $this->limitAmount) {
				$wordsArray[$value['word']] = $value;
			}
			$i++;
		}
		$this->wordsArray = array();
		$this->wordsArray = $wordsArray;
		return $this->wordsArray;
	}
	
	/*
	 * Finds the maximum value of an array
	 *
	 * @param string $word		 
	 *		 
	 * @returns void
	 */
	
	function removeWord($word) {
		$this->removeWords[] = $this->formatWord($word);
	}
	
	/*
	 * Removes tags from the whole array
	 *
	 * @returns array $this->wordsArray
	 */
	
	function removeWords() {
		foreach ($this->wordsArray as $key => $value) {
			if (!in_array($value['word'], $this->removeWords)) {
				$wordsArray[$value['word']] = $value;
			}
		}
		$this->wordsArray = array();
		$this->wordsArray = $wordsArray;
		return $this->wordsArray;
	}
	
	/*
	 * Assign the order field and order direction of the cloud
	 * 
	 * Order by word or size / defaults to random		 		 
	 *
	 * @param array $field
	 * @param string $sortway
	 *     		 
	 * @returns void
	 */
	
	function orderBy($field, $direction = 'ASC') {
        return $this->orderBy = array('field' => $field, 'direction' => $direction);
    }
		
	/*
	 * Orders the cloud by a specific field
	 *
	 * @param array $unsortedArray
	 * @param string $sortField
	 * @param string $sortWay
	 *     		 
	 * @returns array $unsortedArray
	 */
	
	function orderCloud($unsortedArray, $sortField, $sortWay = 'SORT_ASC') {
		$sortedArray = array();
		foreach ($unsortedArray as $uniqid => $row) {
			foreach ($row as $key => $value) {
				$sortedArray[$key][$uniqid] = strtolower($value);
			}
		}
		if ($sortWay) {
			array_multisort($sortedArray[$sortField], constant($sortWay), $unsortedArray);
		}
		return $unsortedArray;
	}
	
	/*
	 * Finds the maximum value of an array
	 *
	 * @returns string $max
	 */
	
	function getMax() {
		$max = 0;
		if (!empty($this->wordsArray)) {
			$p_size = 0;
			foreach ($this->wordsArray as $cKey => $cVal) {
				$c_size = $cVal['size'];
				if ($c_size > $p_size) {
		            $max = $c_size;
					$p_size = $c_size;
		        }
			}
		}
		return $max;
	}
	
	/*
	 * Create the HTML code for each word and apply font size.
	 *
	 * @returns string/array $return
	 */
	
	function showCloud($returnType = 'html') {
		if (!empty($this->removeWords)) {
			$this->removeWords();
		}
		if (empty($this->orderBy)) {
			$this->shuffleCloud();
		} else {
			$orderDirection = strtolower($this->orderBy['direction']) == 'desc' ? 'SORT_DESC' : 'SORT_ASC';
    		$this->wordsArray = $this->orderCloud($this->wordsArray, $this->orderBy['field'], $orderDirection);
		}
		if (!empty($this->limitAmount)) {
			$this->limitCloud();
		}
		$this->max = $this->getMax();
		if (is_array($this->wordsArray)) {
			$return = ($returnType == 'html' ? '' : ($returnType == 'array' ? array() : ''));
			foreach ($this->wordsArray as $word => $arrayInfo) {
				//debug_pr($arrayInfo);
				$sizeRange = $this->getClassFromPercent(($arrayInfo['size'] / $this->max) * 100);
				$arrayInfo['range'] = $sizeRange;
				$class = $arrayInfo['class'];
				if ($returnType == 'array') {
					$return [$word] = $arrayInfo;
				} elseif ($returnType == 'html') {
					if($arrayInfo['url']=="") {
						$return .= "<span class='cloudword size{$sizeRange}'> &nbsp; {$arrayInfo['word']} &nbsp; </span>";
					} else {
						//$return .= "<a href='{$arrayInfo['url']}'><span class='cloudword cloudtextsize{$sizeRange}'> &nbsp; ".$arrayInfo['word']." &nbsp; </span></a>";
						$return .= "<a href='{$arrayInfo['url']}' class='$class'><span> &nbsp; ".$arrayInfo['word']." &nbsp; </span></a>";
					}
				}
			}
			return $return;
		}
		return false;
	}
	
	
}

#################################################
# this is the library for the "Tag Cloud" [End] #
#################################################

include_once("libaccessright.php");

class libdigitalarchive extends libaccessright {
	
	var $Module;
	var $AdminModule;
	var $RenameAs_MaxNo;
	var $IsAdminInGroup;
	var $DefaultRights;
	var $DefaultViewAmount;
	var $MaxLengthOfGroupName;
	var $MaxLengthOfFileName;
	
	function libdigitalarchive($user_id=0)
	{
		$this->Module = "DigitalArchive";
		$this->AdminModule = "DigitalArchiveAdmin";
		parent::libaccessright();
		$this->ModulePath = "/home/eAdmin/ResourcesMgmt/DigitalArchive/";
		$this->ModuleFilePath = "/file/DigitalArchive/";
		$this->RenameAs_MaxNo = 200;
		$this->MaxLengthOfGroupName = 50;
		$this->MaxLengthOfFileName = 127;
		$this->DefaultViewAmount = 5;
		$this->IsAdminInGroup = $this->returnAdmin($user_id);
		$this->DefaultRights = array("ADMIN_OWN_VIEW", "ADMIN_OWN_MANAGE", "ADMIN_OTHERS_VIEW", "ADMIN_OTHERS_MANAGE", "MEMBER_OWN_VIEW", "MEMBER_OWN_MANAGE");
		
	}
	
	
	function returnAdmin($user_id=0)
	{
		global $UserID;
		if($user_id<1) $user_id = $UserID;
		
		$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER WHERE UserID='$user_id' AND IsAdmin=1";
		$result = $this->returnVector($sql);
		
		$IsAdminInGroup = array();
		for($i=0, $i_max=sizeof($result); $i<$i_max; $i++) {
			$groupid = $result[$i];
			$IsAdminInGroup[$groupid] = 1;
		}	
		return $IsAdminInGroup;
	}	
	
	function getLastVisitOfDigitalArchive($GroupID)
	{
		global $UserID, $_SESSION;
		
		$sql = "SELECT StartTime FROM DIGITAL_ARCHIVE_ADMIN_DOC_GROUP_SESSION WHERE GroupID='$GroupID' AND UserID='$UserID'";
		$result = $this->returnVector($sql);
		
		if($result[0]=="") {	# 1st time to login into group
			$sql = "SELECT DateInput FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID'";
			$result = $this->returnVector($sql);
		}
		
		$_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID] = $result[0];
		
		return $result[0];
	}
	
	function updateLastVisitOfDigitalArchive($GroupID, $updateOnly=1)
	{
		global $UserID;
		
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_ADMIN_DOC_GROUP_SESSION WHERE GroupID='$GroupID' AND UserID='$UserID'";
		$count = $this->returnVector($sql);
		
		if($count[0]==0) {	 # insert
			$sql = "INSERT INTO DIGITAL_ARCHIVE_ADMIN_DOC_GROUP_SESSION SET GroupID='$GroupID', UserID='$UserID', StartTime=NOW()";
			$this->db_db_query($sql);
		} else {			
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOC_GROUP_SESSION SET StartTime=NOW() WHERE GroupID='$GroupID' AND UserID='$UserID'";
			$this->db_db_query($sql);
		}
		
		if(!$updateOnly)
			$this->getLastVisitOfDigitalArchive($GroupID);
	}
		
	function insertResourceRecord($dataAry=array()) 
	{
		global $UserID;
		
		if(sizeof($dataAry)>0) {
			$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD SET ";
			foreach($dataAry as $field=>$value) $sql .= " $field='$value', ";
			$sql .= " DateInput=NOW(), InputBy='$UserID', DateModified=NOW(), ModifyBy='$UserID', LikeCount=0";
			
			$this->db_db_query($sql);
			return $this->db_insert_id(); 
		}	
	}
	
	function updateResourceRecord($dataAry=array(), $RecordID) 
	{
		global $UserID;
		
		if(sizeof($dataAry)>0) {
			$sql = "UPDATE DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD SET ";
			foreach($dataAry as $field=>$value) $sql .= " $field='$value', ";
			$sql .= " DateModified=NOW(), ModifyBy='$UserID' WHERE SubjectResourceID='$RecordID'";
			
			$result = $this->db_db_query($sql);
			return $result; 
		}	
	}

	function updateRecordYearRelation($YearAry=array(), $RecordID)
	{
		global $UserID;
		
		$delim = "";
		for($i=0, $i_max=sizeof($YearAry); $i<$i_max; $i++) {
			$values .= $delim."('$RecordID', '".$YearAry[$i]."', NOW(), '$UserID')";
			$delim = ", ";
		}
		//echo $values.'/';
		if($RecordID!="" && $values!="") {
			$this->deleteRecordYearRelation($RecordID);	# since Resource Document should link to at least 1 form
			$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION VALUES $values";
			//echo '<br>'.$sql;
			$result = $this->db_db_query($sql);	
			return $result;
		}
	}
	
	function deleteRecordYearRelation($RecordID)
	{
		$sql = "DELETE FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION WHERE SubjectResourceID='$RecordID'";
		$this->db_db_query($sql);
	}
	
	function Get_View_Detail_By_Resource_ID($RecordID)
	{
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_VIEW_RECORD WHERE SubjectResourceID='$RecordID'";
		$result = current($this->returnVector($sql));
		return $result;	
	}
		
	function Get_Like_Detail_By_Resource_ID($RecordID)
	{
		$sql = "SELECT UserID FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD WHERE SubjectResourceID='$RecordID'";
		$result = $this->returnVector($sql);
		return $result;	
	}
	
	function Update_Like_Status($RecordID, $Status)
	{
		global $UserID;
		
		if($Status==1) {	 # set LIKE
			$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD WHERE SubjectResourceID='$RecordID' AND UserID='$UserID'";
			$count = $this->returnVector($sql);
		
			if($count[0]==0) {
				$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD SET SubjectResourceID='$RecordID', UserID='$UserID', DateInput=NOW()";
				$this->db_db_query($sql);
				
				$sql = "UPDATE DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD SET LikeCount=LikeCount+1 WHERE SubjectResourceID='$RecordID'";
				$this->db_db_query($sql);
			}
			
		} else {			# set UNLIKE
			$sql = "DELETE FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD WHERE SubjectResourceID='$RecordID' AND UserID='$UserID'";
			$this->db_db_query($sql);
			
			$sql = "SELECT LikeCount FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD WHERE SubjectResourceID='$RecordID'";
			$likeCount = $this->returnVector($sql);
			
			if($likeCount[0]>0) {	
				$sql = "UPDATE DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD SET LikeCount=LikeCount-1 WHERE SubjectResourceID='$RecordID'";
				$this->db_db_query($sql);
			}	
		}	
		
	}
	
	function Get_Rating_Detail_By_Resource_ID($RecordID)
	{
		$sql = "SELECT UserID,Rating FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RATING_RECORD WHERE SubjectResourceID='$RecordID'";
		$result = $this->returnArray($sql);
		return $result;	
	}
	
	function Insert_View_Record($RecordID) 
	{
		global $UserID;
		$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_VIEW_RECORD 
				(UserID,SubjectResourceID,DateModified)
				VALUES ('".$UserID."','".$RecordID."',NOW())";
		return $this->db_db_query($sql);
	}	
	function Insert_Rating_Record($RecordID, $Rating)
	{
		global $UserID;
		
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RATING_RECORD WHERE SubjectResourceID='$RecordID' AND UserID='$UserID'";
		$count = $this->returnVector($sql);
		
		if($count[0]==0) {
			$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RATING_RECORD 
					(UserID,SubjectResourceID,Rating,DateInput)
					VALUES ('".$UserID."','".$RecordID."','".$Rating."',NOW())";
			return $this->db_db_query($sql);

		}else{
			return false;
		}

	}
			
	function getGroupInfo($GroupID) 
	{
		$sql = "SELECT GroupTitle, Description, Code FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP WHERE GroupID='$GroupID'";
		$result = $this->returnArray($sql);
		return $result[0];	
	}
	
	
	/*
	function getGroupIDfromFileID($FileID) 
	{
		$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$FileID'";
		$result = $this->returnVector($sql);
		return $result[0];	
	}
	*/
	
	function getDirectoryList($GroupID, $folderID=0, $skipFolder=0, $user_id=0, $group_right_settings=null)
	{
		global $UserID;
		
		if ($user_id==0 || $user_id=="")
		{
			$user_id = $UserID;
		}
		$name_field = getNameFieldByLang("USR.");

		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if($this->IsAdminInGroup[$GroupID]) {	# group admin
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW", $group_right_settings)) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW", $group_right_settings)) {	# can view own
						$conds = " AND rec.InputBy='$user_id'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				}  else {		# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW", $group_right_settings)) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$user_id'";
					}					
				}
				
					
			} else {	# group member
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW", $group_right_settings)) {	# cannot view others

					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW", $group_right_settings)) {	# can view own
						$conds = " AND rec.InputBy='$user_id'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				} else {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW", $group_right_settings)) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$user_id'";
					}	
				}
			}
		}
		
		# commented on 2012-02-23
		#if($skipFolder) { $skipConds = " AND rec.isFolder=0";}
		
		$sql = "SELECT 
					rec.DocumentID, 
					rec.Title,
					rec.FileFolderName, 
					rec.FileHashName, 
					IFNULL(rec.FileExtension,'-') as FileExtension,
					IFNULL(rec.Description,'-') as Description, 
					IF(rec.IsFolder=1,'-',rec.VersionNo) as VersionNo, 
					rec.IsFolder, 
					rec.DateInput,
					$name_field as InputBy,
					SizeInBytes,
					Thread,
					rec.DateModified
				FROM 
					DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec 
					LEFT JOIN INTRANET_USER USR ON USR.UserID=rec.InputBy  
				WHERE 
					rec.GroupID='$GroupID' AND rec.ParentFolderID='$folderID' AND rec.IsLatestFile=1 AND rec.RecordStatus=1 $conds $skipConds
				ORDER BY 
					rec.IsFolder DESC, Title";
		//echo $sql."<p>";
		$result = $this->returnArray($sql,12);

		//echo $sql;
		return $result;	
	}
	
	
	# folder list only and allow to upload!!
	function getDirectoryListToUpload($GroupID, $folderID=0, $skipFolder=0, $user_id=0, $group_right_settings=null)
	{
		global $UserID;
		
		if ($user_id==0 || $user_id=="")
		{
			$user_id = $UserID;
		}
		$name_field = getNameFieldByLang("USR.");

		# tmp yuen - should check module admin from DB 
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if($this->IsAdminInGroup[$GroupID]) {	# group admin
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE", $group_right_settings)) {	# cannot manage others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-MANAGE", $group_right_settings)) {	# can manage own
						$conds = " AND rec.InputBy='$user_id'";
					} else {									# cannot manage own
						$conds = " AND rec.InputBy='0'";
					}
				}  else {		# can manage others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-MANAGE", $group_right_settings)) {	# can manage own
						# nothing filter
					} else {									# cannot manage own
						$conds = " AND rec.InputBy!='$user_id'";
					}					
				}
				
					
			} else {	# group member

				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE", $group_right_settings)) {	# cannot manage others

					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE", $group_right_settings)) {	# can manage own
						$conds = " AND rec.InputBy='$user_id'";
					} else {									# cannot manage own
						$conds = " AND rec.InputBy='0'";
					}
				} else {	# can manage others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE", $group_right_settings)) {	# can manage own
						# nothing filter
					} else {									# cannot manage own
						$conds = " AND rec.InputBy!='$user_id'";
					}	
				}
			}
		}
		
		# commented on 2012-02-23
		#if($skipFolder) { $skipConds = " AND rec.isFolder=0";}
		
		$sql = "SELECT 
					rec.DocumentID, 
					rec.Title,
					rec.FileFolderName, 
					rec.FileHashName, 
					IFNULL(rec.FileExtension,'-') as FileExtension,
					IFNULL(rec.Description,'-') as Description, 
					IF(rec.IsFolder=1,'-',rec.VersionNo) as VersionNo, 
					rec.IsFolder, 
					rec.DateInput,
					$name_field as InputBy,
					SizeInBytes,
					Thread,
					rec.DateModified
				FROM 
					DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec 
					INNER JOIN INTRANET_USER USR ON (USR.UserID=rec.InputBy AND rec.GroupID='$GroupID') 
				WHERE 
					rec.ParentFolderID='$folderID' AND rec.IsLatestFile=1 AND rec.IsFolder=1 AND rec.RecordStatus=1 $conds $skipConds
				ORDER BY 
					rec.IsFolder DESC, Title";
		//echo $sql."<p>";
		$result = $this->returnArray($sql,12);

		//echo $sql;
		return $result;	
	}
	
	function Get_Folder_Info_By_Name($FolderName, $GroupID="", $ParentFolderID="")
	{
		if($ParentFolderID!="") $conds .= " AND ParentFolderID='$ParentFolderID'";
		if($GroupID!="") $conds .= " AND GroupID='$GroupID'";
		
		$sql = "SELECT Title, Description, DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE IsFolder=1 AND RecordStatus=1 AND UPPER(FileFolderName)='".strtoupper(trim($FolderName))."' $conds";
		return $this->returnArray($sql);	
	}
	
	function Create_Admin_Folder($dataAry=array())
	{
		global $UserID, $Lang;
		
		$result = $this->Get_Folder_Info_By_Name($dataAry['FileFolderName'], $dataAry['GroupID'], $dataAry['ParentFolderID']);
		if(sizeof($result)>0) 
			return $Lang['General']['ReturnMessage']['DuplicateFolderName'];
		
		foreach($dataAry as $fields[]=>$values[]);
		
		$sql = "INSERT INTO DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD (";
		foreach($fields as $_f) $sql .= "$_f, ";
		$sql .= "IsFolder, DateInput, InputBy) VALUES (";
		foreach($values as $_v) $sql .= "'$_v', ";
		$sql .= "1, NOW(), '$UserID')";
		$this->db_db_query($sql);
		return $this->db_insert_id();
	}
	
	function insertAdminRecord($dataAry=array())
	{
		global $UserID;
		
		foreach($dataAry as $fields[]=>$values[]);
		
		$sql = "INSERT INTO DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD (";
		foreach($fields as $_f) $sql .= "$_f, ";
		$sql .= "DateInput, DateModified) VALUES (";
		foreach($values as $_v) $sql .= "'$_v', ";
		$sql .= "NOW(), NOW())";
		$this->db_db_query($sql);
		$RecordID = $this->db_insert_id(); 
		
		$sql = "SELECT InputBy, ModifyBy FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$RecordID'";
		$data = $this->returnArray($sql);
		
		list($inputby, $modifyby) = $data[0];
		if($inputby==0) $additional .= ", InputBy='$UserID'";
		if($modifyby==0) $additional .= ", ModifyBy='$UserID'";
		
		$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET Thread='$RecordID' $additional WHERE DocumentID='$RecordID'";
		
		$this->db_db_query($sql); 

		return $RecordID;
	}
	
	function deleteAdminRecord($GroupID, $DocumentIDAry=array(), $skipRearrangeLatest=0, $deleteWholeThread=0)
	{
		include_once("libinterface.php");
		include_once("libdigitalarchive_ui.php");
		include_once("liblog.php");
		include_once("libuser.php");
		$libuser = new libuser();
		
		$ldaUI = new libdigitalarchive_ui();
		$lg = new liblog();
		
		
		for($i=0, $i_max=sizeof($DocumentIDAry); $i<$i_max; $i++) {
			$docID = $DocumentIDAry[$i];
			
			if($docID=="") continue;
			
			$sql = "SELECT IsFolder, Title, FileFolderName, ParentFolderID, VersionNo, InputBy, DateInput, DateModified, ModifyBy FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$docID'";	
			$result = $this->returnArray($sql);
			list($isFolder, $title, $fileFolderName, $parentFolderID, $versionNo, $inputBy, $dateInput, $dateModified, $modifyBy) = $result[0];
			
			if(sizeof($result)>0) {
				if(!$isFolder) {	# file only
					$result[] = $this->Delete_Admin_File_By_DocID($GroupID, $docID, $skipRearrangeLatest, $deleteWholeThread);
				} else {			# is directory
					$result[] = $this->Delete_Admin_Folder_By_DocID($GroupID, $docID);
				}
				
				
				$detailAry = array();
				if ($isFolder)
				{
					$detailAry['Folder name'] = $title;
				} else
				{
					
					if ($title!=$fileFolderName)
					{
						$detailAry['Document title'] = $title;
						$detailAry['Filename'] = $fileFolderName;
					} else
					{
						$detailAry['Filename'] = $title;
					}
				}
				
				# Get Group Info
				$groupInfoArr = $this->Get_Group_Info($GroupID);
				$groupName = $groupInfoArr[0]['GroupTitle'];
				$detailAry['GroupName'] = $groupName;
				
				# Path
				$detailAry['Path'] = $ldaUI->Get_Folder_Navigation($parentFolderID, "/");
				if($detailAry['Path']=="") $detailAry['Path'] = "/";
				$detailAry['VersionNo'] = $versionNo;
							
				# Get Upload User Name
				$userNameInfor = $libuser->returnUser($inputBy);
				$uploadByName =  $userNameInfor[0]['EnglishName'] ;
				
				if($userNameInfor[0]['ChineseName']){
					$uploadByName .=  ' (' . $userNameInfor[0]['ChineseName'] . ')';	
				}		
				$detailAry['UploadBy'] = $uploadByName; 
				
				# Upload Time
				$detailAry['UploadTime'] = $dateInput; 
				
				
//				# Get Modified User Name
//				$modifiedUserNameInfor = $libuser->returnUser($modifyBy);
//				$modifiedByName =  $modifiedUserNameInfor[0]['EnglishName'] ;
//				
//				if($userNameInfor[0]['ChineseName']){
//					$modifiedByName .=  ' (' . $modifiedUserNameInfor[0]['ChineseName'] . ')';	
//				}		
//				$detailAry['ModifiedBy'] = $modifiedByName; 
//				
//				# Modified Time
//				$detailAry['ModifiedTime'] = $dateModified; 
				
				$lg->INSERT_LOG($this->Module, $this->AdminModule, $detailAry, 'DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD', $docID);
			}
		}
		return $result;
	}
	
	function Delete_Admin_File_By_DocID($GroupID, $docID, $skipRearrangeLatest=0, $deleteWholeThread=0) 
	{
		global $UserID, $intranet_root, $PATH_WRT_ROOT;
		
		include_once("libfilesystem.php");
		$lfs = new libfilesystem();
			
		$sql = "SELECT IsLatestFile, Thread, FileHashName FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$docID'";
		$data = $this->returnArray($sql);
		//echo $sql.'<br>';
		# update RecordStatus of record
		if($deleteWholeThread) {
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET RecordStatus=0, IsLatestFile=0, DateModified=NOW(), ModifyBy='$UserID' WHERE Thread='".$data[0]['Thread']."'";
		} else {
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET RecordStatus=0, IsLatestFile=0, DateModified=NOW(), ModifyBy='$UserID' WHERE DocumentID='$docID'";			
		}
		$result = $this->db_db_query($sql);
		//echo $sql.'<br>';
		if($result) {
			# delete the file physically
			$path = $PATH_WRT_ROOT."../intranetdata/digital_archive/admin_doc/";
			
			$file = $path.$data[0]['FileHashName'];
			
			$file = stripslashes($file);
			$lfs->lfs_remove($file);	
					
			# update the latest file flag
			if($data[0]['IsLatestFile'] && !$skipRearrangeLatest) {
				$sql = "SELECT MAX(DocumentID) FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE Thread='".$data[0]['Thread']."' AND RecordStatus=1 AND IsFolder=0 AND GroupID='".$GroupID."'";
				$maxDocID = $this->returnVector($sql);
				
				$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET IsLatestFile=1 WHERE DocumentID='".$maxDocID[0]."'";
				$this->db_db_query($sql);
				
			}
		}
		
		return $result;
			
	}
	
	function Delete_Admin_Folder_By_DocID($GroupID, $DocumentID)
	{
		$sql = "SELECT DocumentID, IsFolder FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE ParentFolderID='$DocumentID'";
		$childDoc = $this->returnVector($sql);
		
		/*  Disabled due to passing accumulated IDs in $IdAry
		$count = sizeof($childDoc);
		
		
		$IdAry = array();
		//if($count==0) {
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET RecordStatus=0 WHERE DocumentID='$DocumentID'";
			
			$result = $this->db_db_query($sql);
		//} else {
			for($i=0; $i<$count; $i++) {
				$IdAry[] = $childDoc[$i];
				$this->deleteAdminRecord($GroupID, $IdAry, $skipRearrangeLatest=1, $deleteWholeThread=0);
			}
			*/
			
			if (sizeof($childDoc)>0)
			{
				$this->deleteAdminRecord($GroupID, $childDoc, $skipRearrangeLatest=1, $deleteWholeThread=0);
			}
			
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET RecordStatus=0 WHERE DocumentID='$DocumentID'";			
			$result = $this->db_db_query($sql);
			
			return $result;
		//}
	}
	
	function GetMyGroupList()
	{
		global $UserID, $_SESSION;
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			$groupAry = $this->UserInGroupList();
			
			for($i=0, $i_max=sizeof($groupAry); $i<$i_max; $i++) {
				$groupid = $groupAry[$i];
				
				if($this->IsAdminInGroup[$groupid]) {
					
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid' AND (rec.InputBy='$UserID' OR rec.InputBy IS NULL))" : " OR (gp.GroupID='$groupid' AND (rec.InputBy='$UserID' OR rec.InputBy IS NULL))";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(rec.InputBy='0' OR rec.InputBy IS NULL)" : " AND (rec.InputBy='0' OR rec.InputBy IS NULL)";
						}
					} else {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid')" : " OR (gp.GroupID='$groupid')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid' AND (rec.InputBy!='$UserID' OR rec.InputBy IS NULL))" : " AND(gp.GroupID='$groupid' AND (rec.InputBy!='$UserID' OR rec.InputBy IS NULL))";
						}
					}
				} else {
					
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OTHERS-VIEW")) {	# cannot view others
					//echo $groupid.'/<br>';
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
						
							$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid' AND (rec.InputBy='$UserID' OR rec.InputBy IS NULL))" : " OR (gp.GroupID='$groupid' AND (rec.InputBy='$UserID' OR rec.InputBy IS NULL))";
							//$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid')" : " OR (gp.GroupID='$groupid')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(rec.InputBy='0' OR rec.InputBy IS NULL)" : " AND (rec.InputBy='0' OR rec.InputBy IS NULL)";
							//$additionConds .= ($additionConds=="") ? "" : "";
						}
					} else {	# can view others
					
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid')" : " OR (gp.GroupID='$groupid')";
						} else {	# cannot view own
							//$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid' AND (rec.InputBy!='$UserID' OR rec.InputBy IS NULL))" : " AND(gp.GroupID='$groupid' AND (rec.InputBy!='$UserID' OR rec.InputBy IS NULL))";
							//[2020-0107-1555-29073] changed the AND operator to OR
							$additionConds .= ($additionConds=="") ? "(gp.GroupID='$groupid')" : " OR (gp.GroupID='$groupid')";
						}
					}
				}
			}	
			if($additionConds != "")
				$conds .= " AND ($additionConds)";
			
			$gmConds = " AND m.UserID='$UserID'";
			
			$sql = "
				SELECT 
					gp.GroupID, 
					gp.GroupTitle, 
					COUNT(DISTINCT rec.DocumentID) as docTotal,
					MAX(rec.DateInput)
				FROM 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP gp  
					LEFT JOIN 
						DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec ON (rec.GroupID=gp.GroupID AND rec.IsFolder=0 AND rec.RecordStatus=1 AND rec.IsLatestFile=1) 
					INNER JOIN 
						DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER m ON (m.GroupID=gp.GroupID $gmConds $conds)
				
				GROUP BY 
					gp.GroupID 
				ORDER BY 
					gp.GroupTitle
				";
		}else{
			// module admin can see all groups
			$sql = "
				SELECT 
					gp.GroupID, 
					gp.GroupTitle, 
					COUNT(DISTINCT rec.DocumentID) as docTotal,
					MAX(rec.DateInput)
				FROM 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP gp  
					LEFT JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec ON (rec.GroupID=gp.GroupID AND rec.IsFolder=0 AND rec.RecordStatus=1 AND rec.IsLatestFile=1) 
				GROUP BY 
					gp.GroupID 
				ORDER BY 
					gp.GroupTitle
				";
		}

		$data = $this->returnArray($sql);
		//echo $sql;
		return $data;
	}
	
	function GetMyGroup($userid="")
	{
		global $UserID;
		
		if($userid=="") $userid = $UserID;
		
		$groupAry = $this->UserInGroupList();
		
		$returnAry = array();
		
		$a = 0;
		
		for($i=0, $i_max=sizeof($groupAry); $i<$i_max; $i++) {
			$groupid = $groupAry[$i];
			$groupinfo = $this->getGroupInfo($groupid);
			
			$this->getLastVisitOfDigitalArchive($groupid);
			
			$totalNoOfRecordInGroup = $this->Get_Total_No_Of_Record_Bt_GroupID($groupid);
			
			# no. of records by current user
			$sql = "
				SELECT
					COUNT(DISTINCT rec.DocumentID) as docTotal,
					MAX(rec.DateInput)
				FROM 
					DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec 
				WHERE
					rec.GroupID='$groupid' AND 
					rec.IsFolder=0 AND 
					rec.RecordStatus=1 AND 
					rec.IsLatestFile=1 AND 
					rec.InputBy='$userid'
				GROUP BY 
					rec.GroupID 
				
				";
				
			$result = $this->returnArray($sql);
			$myNoOfRecord = ($result[0][0]=="") ? 0 : $result[0][0];
			$lastUpdateTime = $result[0][1];
			
			if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
				if($this->IsAdminInGroup[$groupid]) {
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OTHERS-VIEW")) {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$recordNo = $totalNoOfRecordInGroup;
						} else {	# cannot view own
							$recordNo = $totalNoOfRecordInGroup - $myNoOfRecord[0];
						}
					} else {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$recordNo = $myNoOfRecord[0];
						} else {	# cannot view own
							$recordNo = 0;
						}
					}
					
				} else {
					
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OTHERS-VIEW")) {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$recordNo = $totalNoOfRecordInGroup;
						} else {	# cannot view own
							$recordNo = $totalNoOfRecordInGroup - $myNoOfRecord[0];
						}
					} else {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$recordNo = $myNoOfRecord[0];
							
							
						} else {	# cannot view own
							$recordNo = 0;
						}
					}
				}
			} else {
				$recordNo = $totalNoOfRecordInGroup;	
			}	


			$returnAry[$a][] = $groupid;
			$returnAry[$a][] = $groupinfo['GroupTitle'];
			$returnAry[$a][] = ($recordNo=="") ? 0 : $recordNo;
			$returnAry[$a][] = $lastUpdateTime;
			
			$a++;
		}	
		
		return $returnAry;
	}
	
	function Get_Total_No_Of_Record_Bt_GroupID($GroupID, $fromTime="")
	{
		if($fromTime!="") $conds = " AND DateInput>'$fromTime'";
		
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID' AND IsFolder=0 AND RecordStatus=1 AND IsLatestFile=1 $conds";
		$result = $this->returnVector($sql);
		return $result[0];	
	}
	
	function getAccessRightGroupAdmin($GroupID)
	{
		$name_field = getNameFieldByLang("USR.");
		
		$sql = "SELECT $name_field as name FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER m INNER JOIN INTRANET_USER USR ON (USR.UserID=m.UserID AND m.IsAdmin=1) WHERE GroupID='$GroupID'";
		$result = $this->returnVector($sql);
		
		return $result;
	}
	
	function getAllAccessRightGroup($keyword="", $CategoryID=0)
	{
		if($keyword!="") $conds = " AND (gp.GroupTitle LIKE '%".addslashes($keyword)."%' OR gp.Description LIKE '%".addslashes($keyword)."%') ";
		
		if ($CategoryID!="" && $CategoryID>0)
		{
			$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN WHERE CategoryID='$CategoryID' ";
			$CategoryArr = $this->returnVector($sql);
			if (is_array($CategoryArr) && sizeof($CategoryArr)>0)
			{
				$GroupIDs = implode(",", $CategoryArr);
			}
			$conds .= ($GroupIDs!="") ? " AND gp.GroupID IN ($GroupIDs) " : " AND gp.GroupID=-99 ";
		}
		
		$sql = "SELECT 
					gp.GroupID, 
					gp.GroupTitle, 
					gp.Description, 
					COUNT(u.UserID) as totalMember, 
					gp.Code
				FROM
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP gp 
					LEFT JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER m ON (m.GroupID=gp.GroupID) 
					LEFT JOIN INTRANET_USER as u ON u.UserID=m.UserID 
				WHERE
						1
						$conds
				Group By 
					gp.GroupID
				ORDER BY
					gp.Code, gp.GroupTitle
			";
			$data = $this->returnArray($sql);
			
			for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
				$adminName = $this->getAccessRightGroupAdmin($data[$i]['GroupID']);
				$data[$i][]	= implode(', ',$adminName);
				$data[$i]['AdminName']	= implode(', ',$adminName);
			}
			return $data;
	}
	
	function getUsageData()
	{
		$sql = "select GroupID, count(DocumentID) AS NoOfFile, sum(SizeInBytes) As FileSizeByte from DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD where isFolder=0 and recordstatus=1  group by GroupID ORDER BY FileSizeByte DESC";
		$UsageFiles = $this->returnArray($sql);
		
		
		$sql = "select GroupID, count(DocumentID) AS NoOfFolder  from DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD where isFolder=1 and recordstatus=1  group by GroupID";
		$UsageFolders = $this->returnArray($sql);
		
		return array("Files"=>$UsageFiles, "Folders"=>$UsageFolders);
	}
	
	function getAllAccessRightGroupCategory($keyword="")
	{
		if($keyword!="") $conds = " AND (gp.CategoryTitle LIKE '%".addslashes($keyword)."%' OR gp.CategoryCode LIKE '%".addslashes($keyword)."%') ";
		
		$sql = "SELECT 
					gp.CategoryID, 
					gp.CategoryTitle, 
					gp.CategoryCode,
					gp.DisplayOrder,
					COUNT(m.GroupID) as TotalGroups 
				FROM
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY gp 
					LEFT JOIN
						DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN m ON (m.CategoryID=gp.CategoryID)
				WHERE
						1
						$conds
				Group By 
					gp.CategoryID
				ORDER BY
					gp.DisplayOrder, gp.CategoryCode, gp.CategoryTitle
			";
			$data = $this->returnArray($sql);

			return $data;
	}
	
	
	
	function getAllCategoryAndGroup()
	{		
		$sql = "SELECT 
					gp.CategoryID, 
					gp.CategoryTitle, 
					gp.CategoryCode,
					m.GroupID 
				FROM
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY gp 
					LEFT JOIN
						DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN m ON (m.CategoryID=gp.CategoryID)
				ORDER BY
					gp.DisplayOrder, gp.CategoryCode, gp.CategoryTitle
			";
			$data = $this->returnArray($sql);

			return $data;
	}
	
	function UpdateCategoryLinkage($GroupCategoryID, $GroupID)
	{
		global $UserID;
		
		# remove previous
		if ($GroupID!="" && $GroupID>0)
		{
			$sql = "DELETE FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN WHERE GroupID='{$GroupID}'";
			$this->db_db_query($sql);
		
			# add new if any
			if ($GroupCategoryID!="" && $GroupCategoryID>0)
			{
				$sql = "INSERT INTO DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN SET GroupID='$GroupID', CategoryID='$GroupCategoryID', DateInput=NOW(), InputBy='$UserID'";
				$this->db_db_query($sql);
			}
			return true;
		}
		
		return false;
	}
	
	function GetGroupCategoryID($GroupID)
	{
		$sql = "SELECT CategoryID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN WHERE GroupID='{$GroupID}'";
		$results = $this->returnVector($sql);
		return $results[0];
	}
	
	
	
	function Delete_Access_Right_Group_Category($CategoryID)
	{
		$sql = "DELETE FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY WHERE CategoryID='$CategoryID'";
		$result = $this->db_db_query($sql);
		
		return $result;	
	}
	
	function Delete_Access_Right_Group($GroupID)
	{
		$sql = "DELETE FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP WHERE GroupID='$GroupID'";
		$result = $this->db_db_query($sql);
		
		$this->Delete_Access_Right_Group_Member($GroupID);
		$this->Delete_Access_Right_Group_Setting($GroupID);
		
		return $result;	
	}
	
	function Delete_Access_Right_Group_Member($GroupID, $UserIdAry=array()) 
	{
		if(sizeof($UserIdAry)>0) {
			$conds = " AND UserID IN (".implode(',', $UserIdAry).")";	
		}
		
		$sql = "DELETE FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID='$GroupID' $conds";
		
		return $this->db_db_query($sql);
	}

	function Set_Access_Right_Group_Role($GroupID, $UserIdAry=array(), $Role) 
	{
		/*
		$sql = "UPDATE DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER SET IsAdmin=0 WHERE GroupID='$GroupID'";
		$this->db_db_query($sql);
		*/
		if(sizeof($UserIdAry)>0) {
			$sql = "UPDATE DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER SET IsAdmin=$Role WHERE GroupID='$GroupID' AND UserID IN (".implode(',', $UserIdAry).")";
		
			return $this->db_db_query($sql);
		}
	}
	
	function Delete_Access_Right_Group_Setting($GroupID) 
	{
		$sql = "DELETE FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='$GroupID'";
		return $this->db_db_query($sql);
	}
	
	function memberInMgmtGroup($GroupID) 
	{
		$sql = "SELECT UserID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID='$GroupID'";	
		return $this->returnVector($sql);
	}
	
	function getMemberWithMemberType($GroupID)
	{
		$name_field = getNameFieldByLang("USR.");
		$sql = "SELECT IsAdmin, $name_field as name FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER m INNER JOIN INTRANET_USER USR ON (m.UserID=USR.UserID) WHERE m.GroupID='$GroupID' ORDER BY name";
		return $this->returnArray($sql,2);
	}
	
	function Add_Group_Member($GroupID, $UserIdAry=array())
	{
		global $UserID;
		
		if(sizeof($UserIdAry)>0) {
			$sql = "INSERT INTO DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER VALUES ";
			
			$delim = "";
			for($i=0, $i_max=sizeof($UserIdAry); $i<$i_max; $i++) {
				$uid = $UserIdAry[$i];
				$sql .= $delim." ('$GroupID', '$uid', 0, NOW(), '$UserID')";
				$delim = ", ";
			}
			
			return $this->db_db_query($sql);
		}
			
	}
	
	function Get_Group_Info($GroupID)
	{
			$sql = "SELECT
						GroupID,
						GroupTitle,
						Description,
						Code,
						DateModified,
						ModifiedBy
					FROM 
						DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP 
					WHERE 
						GroupID = '$GroupID'
					";
						
			return $this->returnArray($sql);
	}
	
	function retrieveAccessRight($GroupID='')
	{
		$sql = "SELECT UCASE(Role), FileType, UCASE(Action) FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='$GroupID'";
	
		return $this->returnArray($sql);
	}
	
	function Get_Name_By_ID($ID)
	{
		$result = array();
		
		if($ID)
		{
			$name_field = getNameFieldByLang("");
			$sql = "SELECT
						$name_field
					FROM
						INTRANET_USER
					WHERE
						UserID = '$ID'
					";
			$result = $this->returnVector($sql);
		}
		return $result[0];
			
	}
	
	function Check_Group_Title($GroupTitle, $GroupID="")
	{
		if(strlen($GroupTitle) > $this->MaxLengthOfGroupName) return false;
		
		if($GroupID!="") $conds = " AND GroupID != '$GroupID'";
		
		$sql = "SELECT
					GroupID 
				FROM 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP
				WHERE
					GroupTitle = '".$this->Get_Safe_Sql_Query($GroupTitle)."' $conds 
				";
		return (sizeof($this->returnVector($sql))>0 ?false:true);
	}
	
	function Check_Group_Code($GroupCode, $GroupID="")
	{
		if($GroupID!="") $conds = " AND GroupID != '$GroupID'";
		
		$sql = "SELECT
					GroupID 
				FROM 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP
				WHERE
					Code = '".$this->Get_Safe_Sql_Query($GroupCode)."' $conds 
				";
		return (sizeof($this->returnVector($sql))>0) ?false:true;
	}

 	function Rename_Group($GroupTitle,$GroupID)
 	{
 		global $UserID;
		$sql = 'UPDATE 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP 
				SET
					GroupTitle = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\', 
					DateModified = NOW(),  
					ModifiedBy=\''.$UserID.'\'
				WHERE
					GroupID = \''.$GroupID.'\' ';
					
		return $this->db_db_query($sql);
		
	}
	
	function Set_Group_Description($GroupDescription, $GroupID)
	{
		global $UserID;
		$sql = 'UPDATE 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP 
				SET 
					Description = \''.$this->Get_Safe_Sql_Query($GroupDescription).'\',
					DateModified = NOW(),  
					ModifiedBy=\''.$UserID.'\'
				WHERE GroupID = \''.$GroupID.'\'';
				
		return $this->db_db_query($sql);
	}

	function Set_Group_Code($GroupCode, $GroupID)
	{
		global $UserID;
		$sql = 'UPDATE 
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP 
				SET 
					Code = \''.$this->Get_Safe_Sql_Query($GroupCode).'\',
					DateModified = NOW(),  
					ModifiedBy=\''.$UserID.'\'
				WHERE GroupID = \''.$GroupID.'\'';
				
		return $this->db_db_query($sql);
	}
	
	function Insert_Group_Access_Right($GroupID, $GroupTitle, $GroupDescription, $GroupCode="", $Right)
	{
		global $UserID;
		
		$Results = array();
		if ($GroupID == "") {
			$GroupID = $this->GET_ACCESS_RIGHT_GROUP_ID($this->Get_Safe_Sql_Query($GroupTitle));
			
			if($GroupID == "")
			{
				if($GroupCode=="" || $this->Check_Group_Code($GroupCode)) {
					$GroupID = $this->NEW_ACCESS_RIGHT_GROUP($this->Get_Safe_Sql_Query($GroupTitle), $this->Get_Safe_Sql_Query($GroupDescription), $this->Get_Safe_Sql_Query($GroupCode));
					$Result['CreateGroup'] = $GroupID;
				}
			}
			else 
				$Result['UpdateGroup'] = $this->UPDATE_ACCESS_RIGHT_GROUP($GroupID, $this->Get_Safe_Sql_Query($GroupTitle), $this->Get_Safe_Sql_Query($GroupDescription), $this->Get_Safe_Sql_Query($GroupCode));
		}
		//echo $GroupID.'/'; exit;
		$Result['ClearOldRight'] = $this->Delete_Access_Right_Group_Setting($GroupID); 
		
		for ($i=0; $i< sizeof($Right); $i++) {
			$RightDetail = explode('_',$Right[$i]);
			$sql = "INSERT INTO DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_SETTING 
					(GroupID, Role, FileType, Action, DateInput)
					 VALUES('$GroupID', '".$RightDetail[0]."', '".$RightDetail[1]."', '".$RightDetail[2]."', NOW())";
					 
			$Result['InsertNewRight:'.$Right[$i]] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $Result)) 
		{
			return false;
		} else {
			$sql = "UPDATE DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP SET DateModified=now(), ModifiedBy='$UserID' WHERE GroupID='$GroupID'";
			$this->db_db_query($sql);
			return $GroupID;
		}
	}
	
	function GET_ACCESS_RIGHT_GROUP_ID($GroupTitle)
	{
		$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP WHERE GroupTitle='$GroupTitle'";
		$GroupID = $this->returnVector($sql);
		
		return $GroupID[0];
	}
	
	function Get_Category_Info($GroupCategoryID)
	{
			$sql = "SELECT
						CategoryID,
						CategoryCode,
						CategoryTitle,
						DisplayOrder,
						DateModified,
						ModifiedBy
					FROM 
						DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY 
					WHERE 
						CategoryID = '$GroupCategoryID'
					";
						
			$results = $this->returnArray($sql);
			return $results[0];
	}
	
	function NEW_ACCESS_RIGHT_GROUP_CATEGORY($GroupCategoryCode, $GroupCategoryTitle, $GroupCategoryDisplayOrder)
	{
		global $UserID;
		
		$sql = "INSERT INTO DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY SET CategoryCode='$GroupCategoryCode', CategoryTitle='$GroupCategoryTitle', DisplayOrder='$GroupCategoryDisplayOrder', DateInput=NOW(), InputBy='$UserID', DateModified=NOW(), ModifiedBy='$UserID'";
		$Result = $this->db_db_query($sql);

		if ($Result) 
			return $this->db_insert_id();
		else 
			return false;
	}

	function UPDATE_ACCESS_RIGHT_GROUP_CATEGORY($GroupCategoryID, $GroupCategoryCode, $GroupCategoryTitle, $GroupCategoryDisplayOrder)
	{
		$sql = "UPDATE DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY SET CategoryCode='$GroupCategoryCode', CategoryTitle='$GroupCategoryTitle', DisplayOrder='$GroupCategoryDisplayOrder', DateModified=NOW() WHERE CategoryID='$GroupCategoryID'";
		return $this->db_db_query($sql);
	}
	
	function NEW_ACCESS_RIGHT_GROUP($GroupTitle, $GroupDescription, $GroupCode="")
	{
		global $UserID;
		
		$sql = "INSERT INTO DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP SET GroupTitle='$GroupTitle', Description='$GroupDescription', Code='$GroupCode', DateInput=NOW(), InputBy='$UserID', DateModified=NOW(), ModifiedBy='$UserID'";
		$Result = $this->db_db_query($sql);
		//echo $sql;
		if ($Result) 
			return $this->db_insert_id();
		else 
			return false;
	}

	function UPDATE_ACCESS_RIGHT_GROUP($GroupID, $GroupTitle, $GroupDescription, $GroupCode)
	{
		$sql = "UPDATE DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP SET GroupTitle='$GroupTitle', Description='$GroupDescription', Code='$GroupCode', DateModified=NOW() WHERE GroupID='$GroupID'";
		return $this->db_db_query($sql);
	}

	function Get_Group_Access_Right_Users($GroupID, $Keyword="")
	{
		$NameField = getNameFieldByLang("USR.");
		
		$sql = "
			SELECT 
				m.UserID,
				$NameField as UserName,
				m.IsAdmin
			FROM
				DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER m 
				INNER JOIN
					DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP gp ON (gp.GroupID=m.GroupID AND gp.GroupID='$GroupID')
				INNER JOIN
					INTRANET_USER USR ON (USR.UserID=m.UserID)
			WHERE 
				(
				USR.EnglishName LIKE '%$Keyword%' 
				OR 
				USR.ChineseName LIKE '%$Keyword%' 
				OR 
				USR.EnglishName like '%".htmlspecialchars($Keyword,ENT_QUOTES)."%' 
				OR 
				USR.ChineseName like '%".htmlspecialchars($Keyword,ENT_QUOTES)."%'
				)
			ORDER BY
				$NameField
		";
		
		return $this->returnArray($sql);
	}
	
	function Get_Group_Access_Right_Aval_User_List($Module="")
	{
		$Module = ($Module=="") ? $this->Module : $Module; 
		
		$NameField = getNameFieldByLang("u.");
		$sql = "SELECT 
					DISTINCT m.UserID 
				FROM 
					ACCESS_RIGHT_GROUP as g
					INNER JOIN ACCESS_RIGHT_GROUP_MEMBER as m ON m.GroupID = g.GroupID
				WHERE
					g.Module = '$Module' ";
		$StaffList = $this->returnVector($sql);
		
		$sql = "SELECT 
					u.UserID,
					".$NameField." AS StaffName
				FROM 
					INTRANET_USER u  
				WHERE 
					u.RecordType = '1' 
					AND 
					u.RecordStatus = '1' 
					AND u.UserID NOT IN (".(sizeof($StaffList)>0?implode(",",$StaffList):"-1").") 
				ORDER BY 
					u.EnglishName ";
		return $this->returnArray($sql);
	}

	function Add_Member($GroupID,$AddUserID)
	{
		if (sizeof($AddUserID) > 0) 
		{
			$StaffList = implode(',',$AddUserID);
		}
		
		for ($i=0; $i< sizeof($AddUserID); $i++) {
			if($AddUserID[$i]!="") {
				$sql = 'INSERT INTO ACCESS_RIGHT_GROUP_MEMBER 
								(GroupID,UserID, DateInput) 
								VALUES
								(\''.$GroupID.'\',\''.$AddUserID[$i].'\',\'NOW()\')';
								
				$Result['Add-'.$AddUserID[$i]] = $this->db_db_query($sql);
			}
		}
		
		return (!in_array(false,$Result));
	}
	
	function Get_All_Tag_Name($Module="")
	{
		$Module = ($Module=="") ? $this->Module : $Module; 
		
		$sql = "SELECT t.TagName FROM TAG t INNER JOIN TAG_RELATIONSHIP r ON (r.TagID=t.TagID AND Module='$Module') ";
		
		return $this->returnVector($sql);	
	}
	
	function Update_Admin_Doc_Tag_Relation($tagName, $RecordID, $Module="")
	{
		global $UserID;
		
		$Module = ($Module=="") ? $this->Module : $Module; 
		
		$tagNameAry = explode(',',$tagName);
		
		if(sizeof($tagNameAry)>0) {
			$delim = "";
			foreach($tagNameAry as $tag) {
				$tagID = returnModuleTagIDByTagName($tag, $Module);	# will create new tag if null
				
				if($tagID!="" && $tagID!=0) {
					$values .= $delim."('$RecordID', '{$tagID[0]}', NOW(), '$UserID')";
					$delim = ", ";
				}
			}
			if($values!="") {
				$sql = "INSERT INTO DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE (DocumentID, TagID, DateInput, InputBy) VALUES $values";	
				$this->db_db_query($sql);
			}
		}
	}

	function Delete_Resource_Tag_Relation_By_ID($RecordID) 
	{
		$sql = "DELETE FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE WHERE SubjectResourceID='$RecordID'";
		$this->db_db_query($sql);	
	}
	
	function Delete_Admin_Document_Tag_Relation_By_ID($RecordID) 
	{
		$sql = "DELETE FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE WHERE DocumentID='$RecordID'";
		$this->db_db_query($sql);	
	}
	
	function Set_Resource_Record_Not_Use_Tag_By_TagID($TagIdAry=array(), $Module="") 
	{
		switch($Module) {
			case $this->AdminModule: $table = "DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE"; break;
			default: $table = "DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE"; break;	
		}
		
		for($i=0, $i_max=sizeof($TagIdAry); $i<$i_max; $i++) {
			$TagID = $TagIdAry[$i];
			
			$sql = "DELETE FROM $table WHERE TagID='$TagID'";
			$result[] = $this->db_db_query($sql);
			/*
			$sql = "DELETE FROM TAG_RELATIONSHIP WHERE TagID='$TagID' AND Module='".$this->Module."'";
			$this->db_db_query($sql);
			*/
			 
			removeTagModuleRelation($TagID, $Module);
		}
		
		return $result;
	}

	function Update_Resource_Tag_Relation($tagName, $RecordID, $Module="")
	{
		global $UserID;
		
		$Module = ($Module=="") ? $this->Module : $Module; 
		
		$tagNameAry = array_unique(explode(',',$tagName));
		$tagIDAry = array();
		if(sizeof($tagNameAry)>0) {
			$delim = "";
			foreach($tagNameAry as $tag) {
				$tag = Trim($tag);
				$tagID = returnModuleTagIDByTagName($tag, $Module);	# will create new tag if null
				if($tagID!="" && $tagID!=0 && !in_array($tagID[0],$tagIDAry)) {
					$values .= $delim."('$RecordID', '{$tagID[0]}', NOW(), '$UserID')";
					$delim = ", ";
					$tagIDAry[] = $tagID[0];
				}
			}
			if($values!="") {
				$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE (SubjectResourceID, TagID, DateInput, InputBy) VALUES $values";	
				$this->db_db_query($sql);
			}
		}
	}
	
	function Get_Tag_By_Admin_Docuemnt_ID($documentID)
	{
		$sql = "SELECT t.TagID, t.TagName FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE u INNER JOIN TAG t ON (t.TagID=u.TagID AND DocumentID='$documentID')";
		return $this->returnArray($sql);	
	}
	
	function Get_Folder_Hierarchy_Array($GroupID, $FolderID=0) 
	{
		global $_SESSION, $UserID;
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if($this->IsAdminInGroup[$GroupID]) {	# group admin
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$conds = " AND rec.InputBy='$UserID'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				}  else {		# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$UserID'";
					}					
				}
				
					
			} else {	# group member
			
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$conds = " AND rec.InputBy='$UserID'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				} else {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$UserID'";
					}	
				}
			}
		}
		/*
		$sql = "SELECT DocumentID, Title FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec WHERE IsFolder=1 AND RecordStatus=1 AND GroupID='$GroupID' AND ParentFolderID='$FolderID' $conds ORDER BY Title";
		$result = $this->returnArray($sql);
		
		$returnAry = array();
		
		for($i=0, $i_max=sizeof($result); $i<$i_max; $i++) {
			list($id, $title) = $result[$i];
			$returnAry[$id][] = $title;
			
			$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE IsFolder=1 AND GroupID='$GroupID' AND ParentFolderID='$id' AND RecordStatus=1";
			$temp = $this->returnVector($sql);
			if($temp[0]>0) {
				$returnAry[$id][] = $this->Get_Folder_Hierarchy_Array($GroupID, $id);	
			} 
		}
		*/
		
		$returnAry = array();
		$sql = "SELECT DocumentID, Title, ParentFolderID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec WHERE IsFolder=1 AND RecordStatus=1 AND GroupID='$GroupID' $conds ORDER BY Title";
		$result = $this->returnArray($sql);
		$returnAry = $this->getFolderHierarchyArray($result, $FolderID);
		
		return $returnAry;
	}
	
	function getFolderHierarchyArray($dataAry,$curFolderID)
	{
		$returnAry = array();
		$numOfRecord = count($dataAry);
		for($i=0;$i<$numOfRecord;$i++) {
			list($id, $title, $parent_folder_id) = $dataAry[$i];
			
			if($id == $curFolderID) {
				if(!isset($returnAry[$id])){
					$returnAry[$id] = array();
				}
				$returnAry[$id][] = $title;
			}
		}
		for($i=0;$i<$numOfRecord;$i++) {
			list($id, $title, $parent_folder_id) = $dataAry[$i];
			
			if($parent_folder_id == $curFolderID) {
				$returnAry[$curFolderID][] = $this->getFolderHierarchyArray($dataAry, $id);
			}
		}
		return $returnAry;
	}
	
	function Get_Folder_Hierarchy_File_Count_Array($GroupID, $FolderID=0) 
	{
		global $_SESSION, $UserID;
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if($this->IsAdminInGroup[$GroupID]) {	# group admin
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$conds = " AND r1.InputBy='$UserID' ";
						$conds2 = " AND r2.InputBy='$UserID' ";
					} else {									# cannot view own
						$conds = " AND r1.InputBy='0' ";
						$conds2 = " AND r2.InputBy='0' ";
					}
				}  else {		# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND r1.InputBy!='$UserID' ";
						$conds2 = " AND r2.InputBy!='$UserID' ";
					}					
				}
				
					
			} else {	# group member
			
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$conds = " AND r1.InputBy='$UserID' ";
						$conds2 = " AND r2.InputBy='$UserID' ";
					} else {									# cannot view own
						$conds = " AND r1.InputBy='0' ";
						$conds2 = " AND r2.InputBy='0' ";
					}
				} else {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND r1.InputBy!='$UserID' ";
						$conds2 = " AND r2.InputBy!='$UserID' ";
					}	
				}
			}
		}
		
		$returnAry = array();
//		$sql = "SELECT '0' as DocumentID, '/' as Title,'-1' as ParentFolderID, COUNT(r1.DocumentID) as FileCount 
//				FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD as r1 
//				WHERE r1.IsFolder=0 AND r1.RecordStatus=1 AND r1.GroupID='$GroupID' AND r1.ParentFolderID=0 $conds 
//				GROUP BY r1.ParentFolderID";
				
		$sql = "SELECT '0' as DocumentID, '/' as Title,'-1' as ParentFolderID, COUNT(r1.DocumentID) as FileCount
				FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD as r1
				WHERE r1.IsFolder=0 AND r1.RecordStatus=1 AND r1.IsLatestFile=1 AND r1.GroupID='$GroupID' AND r1.ParentFolderID=0 $conds
				GROUP BY r1.ParentFolderID";
		
		$rootResultAry = $this->returnResultSet($sql); 
		
//		$sql = "SELECT r1.DocumentID, r1.Title, r1.ParentFolderID, COUNT(r2.DocumentID) as FileCount  
//				FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r1 
//				LEFT JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r2 ON r2.IsFolder=0 AND r2.RecordStatus=1 AND r2.ParentFolderID=r1.DocumentID AND r2.GroupID='$GroupID' $conds2 
//				WHERE r1.IsFolder=1 AND r1.RecordStatus=1 AND r1.GroupID='$GroupID' $conds 
//				GROUP BY r1.DocumentID";

		$sql = "SELECT r1.DocumentID, r1.Title, r1.ParentFolderID, COUNT(r2.DocumentID) as FileCount
				FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r1
				LEFT JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r2 ON r2.IsFolder=0 AND r2.RecordStatus=1 AND r2.IsLatestFile=1 AND r2.ParentFolderID=r1.DocumentID AND r2.GroupID='$GroupID' $conds2
				WHERE r1.IsFolder=1 AND r1.RecordStatus=1 AND r1.GroupID='$GroupID' $conds
				GROUP BY r1.DocumentID 
				ORDER BY r1.Title";

		$result = $this->returnResultSet($sql);
		if(count($rootResultAry)>0) {
			array_unshift($result,$rootResultAry[0]);
		}
		$returnAry = $this->getFolderHierarchyFileCountArray($result, $FolderID);
		//$this->getFolderHierarchySubfoldersFileCount($returnAry, $FolderID); // $returnAry is pass by reference as it modify FileCount data of the same array
		
		return $returnAry;
	}
	
	function getFolderHierarchyFileCountArray($dataAry,$curFolderID)
	{
		$returnAry = array();
		$numOfRecord = count($dataAry);
		/*
		if($curFolderID == 0) {
			$returnAry[0] = array();
			$returnAry[0]['FolderID'] = 0;
			$returnAry[0]['ParentFolderID'] = -1;
			$returnAry[0]['Title'] = '/';
			$returnAry[0]['FileCount'] = 0;
			$returnAry[0]['Subfolders'] = array();
		}
		*/
		for($i=0;$i<$numOfRecord;$i++) {
			//list($id, $title, $parent_folder_id, $file_count) = $dataAry[$i];
			$id = $dataAry[$i]['DocumentID'];
			$title = $dataAry[$i]['Title'];
			$parent_folder_id = $dataAry[$i]['ParentFolderID'];
			$file_count = $dataAry[$i]['FileCount'];
			
			if($id == $curFolderID) {
				if(!isset($returnAry[$id])){
					$returnAry[$id] = array();
				}
				$returnAry[$id]['FolderID'] = $id;
				$returnAry[$id]['ParentFolderID'] = $parent_folder_id;
				$returnAry[$id]['Title'] = $title;
				$returnAry[$id]['FileCount'] = $file_count;
				$returnAry[$id]['Subfolders'] = array();
			}
		}
		
		for($i=0;$i<$numOfRecord;$i++) {
			//list($id, $title, $parent_folder_id, $file_count) = $dataAry[$i];
			$id = $dataAry[$i]['DocumentID'];
			$title = $dataAry[$i]['Title'];
			$parent_folder_id = $dataAry[$i]['ParentFolderID'];
			$file_count = $dataAry[$i]['FileCount'];
			
			if($parent_folder_id == $curFolderID) {
				$returnAry[$curFolderID]['Subfolders'][] = $this->getFolderHierarchyFileCountArray($dataAry, $id);
			}
		}
		return $returnAry;
	}
	
	/*
	 * @param &$dataAry (pass by reference) : array return from getFolderHierarchyFileCountArray()
	 */
	function getFolderHierarchySubfoldersFileCount(&$dataAry, $curFolderID)
	{
		$subfolder_filecount = 0;
		if(sizeof($dataAry)>0) {
			foreach($dataAry as $folder_id => $folder_ary) {
				if($folder_id == $curFolderID) {
					for($i=0;$i<count($folder_ary['Subfolders']);$i++) {
						foreach($folder_ary['Subfolders'][$i] as $subfolder_id => $subfolder_ary) {
							$subfolder_filecount += $this->getFolderHierarchySubfoldersFileCount($dataAry[$folder_id]['Subfolders'][$i], $subfolder_id);
						}
					}
					$dataAry[$folder_id]['FileCount'] += $subfolder_filecount;
				}
			}
		}
		return $dataAry[$curFolderID]['FileCount'];
	}
	
	function MoveAdminRecord($DocumentIDAry="", $TargetFolderID="", $GroupID="", $consequence=1) 
	{
		$values = "";
		if(sizeof($DocumentIDAry)>0 && $TargetFolderID!="") {
			$tempAry = array($TargetFolderID);
			$NewDocumentAry = (array_diff($DocumentIDAry, $tempAry));
			
			if(sizeof($NewDocumentAry)>0) {
				/*
				$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET ParentFolderID='$TargetFolderID' WHERE DocumentID IN (".implode(',', $NewDocumentAry).")";
				$result = $this->db_db_query($sql);
				*/
				$canManageOthers = 0;
				$adminInGroup = $this->returnAdmin();
				if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
					// is super admin ?
					$canManageOthers = 1;
				} else if($adminInGroup[$GroupID]) {
					// does this admin user has group manage others file access right?
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE")) {
						$canManageOthers = 1;	
					}
				} else if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE")) {
					// does this member user has group manage others file access right?
					$canManageOthers = 1;	
				}
				
				for($i=0, $i_max=sizeof($NewDocumentAry); $i<$i_max; $i++) {
					
					$dataAry = $this->Get_Admin_Doc_Info($NewDocumentAry[$i]);
					
					# check duplication record
					$IsDuplicateFlag = $this->Is_Duplicate_Admin_Document($GroupID, $TargetFolderID, $dataAry['Title'], $dataAry['FileFolderName'], $NewDocumentAry[$i]);
					
					# file is duplicated & consequence action = "Exclude duplicate record"
					if($IsDuplicateFlag && $consequence==3)	 { 
						continue;
					}
					
					# if no manage others file access right, check duplication with others files
					if($canManageOthers == 0){
						$isDuplicateOthers = $this->Is_Duplicate_Others_Document($GroupID, $TargetFolderID, $dataAry['Title'], $dataAry['FileFolderName'], $NewDocumentAry[$i]);
						if($isDuplicateOthers) {
							continue;
						}
					}
					$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET ParentFolderID='$TargetFolderID' WHERE DocumentID=".$NewDocumentAry[$i];
					$result[] = $this->db_db_query($sql);
					
					
					if($IsDuplicateFlag) {	# "Title" / "File Name" is duplicated
					
						if($consequence==1) {	# overwrite
							$this->Update_Duplicate_Record($NewDocumentAry[$i], $GroupID, $TargetFolderID, $dataAry['Title'], $dataAry['FileFolderName']);
						} else if($consequence==2) {	# rename
							$this->Rename_Duplicate_Record($NewDocumentAry[$i], $GroupID, $TargetFolderID, $dataAry['Title'], $dataAry['FileFolderName']);
						}
					}	
				}
			}
			
			return $result;	
		}
	}

	function Get_Duplicate_Admin_Document($GroupID, $FolderID, $Title="", $FileName="", $thisRecordID="")
	{
		if($thisRecordID!="")
			$conds = " AND DocumentID!='$thisRecordID'";

		$sql = "SELECT * FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID' AND IsFolder=0 AND ParentFolderID='$FolderID' AND (Title='$Title') AND RecordStatus=1 AND IsLatestFile=1 $conds";
		$temp = $this->returnArray($sql);

		return $temp;
	}

	function Is_Duplicate_Admin_Document($GroupID, $FolderID, $Title="", $FileName="", $thisRecordID="") 
	{
		if($thisRecordID!="")
			$conds = " AND DocumentID!='$thisRecordID'";
		
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID' AND IsFolder=0 AND ParentFolderID='$FolderID' AND (Title='$Title') AND RecordStatus=1 AND IsLatestFile=1 $conds";
		$temp = $this->returnVector($sql);

		return ($temp[0]>0) ? 1 : 0;
	}
	
	function Is_Duplicate_Others_Document($GroupID, $FolderID, $Title="", $FileName="", $thisRecordID="", $OwnerID="")
	{
		if($thisRecordID!=""){
			$conds = " AND DocumentID!='$thisRecordID'";
		}
		if($OwnerID == "") {
			$OwnerID = $_SESSION['UserID'];
		}
		
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID' AND IsFolder=0 AND ParentFolderID='$FolderID' AND (Title='$Title') AND RecordStatus=1 AND IsLatestFile=1 AND InputBy<>'$OwnerID' $conds";
		$temp = $this->returnVector($sql);
		
		return ($temp[0]>0) ? 1 : 0;
	}
	
	function Get_File_List_With_All_Version($Thread, $VersionNo="")
	{
		global $UserID;
		
		$name_field = getNameFieldByLang("USR.");
		$name_field2 = getNameFieldByLang("USR2.");
		
		if($VersionNo!="") $conds = " AND rec.VersionNo='$VersionNo'";
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$Thread'";
			$result = $this->returnVector($sql);
			$GroupID = $result[0];
			
			if($this->IsAdminInGroup[$GroupID]) {	# group admin
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$conds = " AND rec.InputBy='$UserID'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				}  else {		# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$UserID'";
					}					
				}
				
					
			} else {	# group member
			
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$conds = " AND rec.InputBy='$UserID'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				} else {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$UserID'";
					}	
				}
			}
		}
		
		$sql = "SELECT rec.DocumentID, rec.Title, rec.FileHashName, rec.FileExtension, rec.Description, rec.VersionNo, rec.SizeInBytes, rec.RecordStatus, rec.DateModified, rec.GroupID, $name_field as modifyBy, rec.FileFolderName, $name_field2 as inputBy, rec.DateInput, rec.Thread, rec.InputBy as InputByID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec INNER JOIN INTRANET_USER USR ON (USR.UserID=rec.ModifyBy) INNER JOIN INTRANET_USER USR2 ON (USR2.UserID=rec.InputBy) WHERE rec.Thread='$Thread' $conds ORDER BY rec.VersionNo DESC, rec.DocumentID DESC";
		
		return $this->returnArray($sql);	
	}
	
	function Get_Admin_Doc_Info($DocumentID)
	{	
		$sql = "SELECT * FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$DocumentID'";
		$result = $this->returnArray($sql);
		return $result[0];
	}
	
	function Update_Duplicate_Record($thisRecordID, $GroupID, $FolderID, $title, $fileFolderName, $InHeritTag=0)
	{			
		global $UserID;
		
		$sql = "SELECT Thread FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID' AND ParentFolderID='$FolderID' AND (Title='$title') AND RecordStatus=1 AND IsFolder=0 AND DocumentID!='$thisRecordID' GROUP BY Thread";
		$tempResult = $this->returnVector($sql);
		$Thread = $tempResult[0];
		
		# Inherit the tag of previous record within same thread
		
		if($InHeritTag) {
			$sql = "SELECT DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE Thread='$Thread' AND DocumentID!='$thisRecordID' AND IsLatestFile=1 AND IsFolder=0";
			$result = $this->returnVector($sql);
			$lastDocID = $result[0];
			
			$sql = "SELECT TagID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE WHERE DocumentID='$lastDocID'";
			$lastTagID = $this->returnVector($sql);
			
			$values = "";
			$delim = "";
			for($i=0; $i<sizeof($lastTagID); $i++) {
				$tagid = $lastTagID[$i];
				$values .= $delim." ('$thisRecordID', '$tagid', NOW(), '$UserID')";
				$delim = ", ";
			}
			
			if($values != "") {
				$sql = "INSERT INTO DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE (DocumentID, TagID, DateInput, InputBy) VALUES $values";
				$this->db_db_query($sql);
				
			}
		}
		
		if(sizeof($tempResult)>0) {	# record duplicated
			# update existing record (set IsLatestFile to 0)
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET IsLatestFile=0 WHERE Thread='$Thread' AND (Title='$title') AND IsFolder=0";
			$this->db_db_query($sql);
			
			$sql = "SELECT MAX(VersionNo) FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE Thread='$Thread' AND IsFolder=0";
			$temp = $this->returnVector($sql);
			$maxNo = $temp[0];
			
			# update version no to new record 
			$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET Thread='$Thread', VersionNo='".($maxNo+1)."' WHERE DocumentID='$thisRecordID'";
			$this->db_db_query($sql);
				
		}
	}
	
	function Rename_Duplicate_Record($thisRecordID, $GroupID, $FolderID, $title, $fileFolderName)
	{
		$data = $this->Get_Admin_Doc_Info($thisRecordID);
		
		for($i=1; $i<=$this->RenameAs_MaxNo; $i++) { # rename it as (n), e.g. xxx(1).doc
			if($data['IsFolder']==1) {
				$new_title = $data['Title']."($i)";
				$new_filename = $data['FileFolderName']."($i).".$data['FileExtension'];	
			} else {
				$new_title = $data['Title']."($i)";
				$pos = strrpos($data['FileFolderName'],".");
				$new_filename = substr($data['FileFolderName'],0,$pos)."($i).".$data['FileExtension'];
			}	
			
			$IsDuplicateFlag = $this->Is_Duplicate_Admin_Document($GroupID, $FolderID, $new_title, $new_filename, $thisRecordID);	
			
			if($IsDuplicateFlag) {
				continue;	
			} else {
				$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET Title='$new_title', FileFolderName='$new_filename' WHERE DocumentID='$thisRecordID'";	
				$this->db_db_query($sql);
				break;
			}
		}
		
		
	}
	
	
	function returnTagRankingArray($returnAmount="", $Module="", $returnSQL=0)
	{
		global $UserID, $_SESSION;
		
		$limit = ($returnAmount=="") ? "" : " LIMIT $returnAmount";
		
		if($Module==$this->AdminModule) {
			
			$additionConds = "";
			if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
				$groupAry = $this->UserInGroupList();
				
				for($i=0, $i_max=sizeof($groupAry); $i<$i_max; $i++) {
					$groupid = $groupAry[$i];
					
					if($this->IsAdminInGroup[$groupid]) {
						
						if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OTHERS-VIEW")) {	# cannot view others
							if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
								$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$groupid' AND rec.InputBy='$UserID')";
							} else {	# cannot view own
								$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
							}
						} else {	# can view others
							if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
								$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid')" : " OR (rec.GroupID='$groupid')";
							} else {	# cannot view own
								$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')";
							}
						}
					} else {
						if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OTHERS-VIEW")) {	# cannot view others
							if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
								$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$groupid' AND rec.InputBy='$UserID')";
							} else {	# cannot view own
								$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
							}
						} else {	# can view others
							if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
								$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid')" : " OR (rec.GroupID='$groupid')";
							} else {	# cannot view own
								$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')";
							}
						}
					}
				}	
				if($additionConds != "")
					$conds .= " AND ($additionConds)";
				
				
						
				# Group member should only see the tag of his / her own group
				$gmConds = " AND gm.UserID='$UserID'";
				
			}
		
			if($returnSQL) {
				$sql = "SELECT CONCAT('<a title=\"',LOWER(replace(t.TagName,'\"','&quot;')),'\" href=\"displayTag.php?tagid=',t.TagID,'\">',t.TagName,'</a>') as Tag, tu.DateInput, CONCAT('<input type=\"checkbox\" name=\"TagID[]\" id=\"TagID[]\" value=', t.TagID ,'>') as checkbox FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE u INNER JOIN TAG t INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec ON (rec.DocumentID=u.DocumentID AND rec.IsLatestFile=1 AND rec.IsFolder=0 AND rec.RecordStatus=1) INNER JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER gm ON (gm.GroupID=rec.GroupID $gmConds) INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE tu ON (tu.TagID=t.TagID) WHERE t.TagID=u.TagID $conds GROUP BY u.TagID ";
				return $sql;
			} else {
				$sql = "SELECT u.TagID, t.TagName, COUNT(u.TagUsageID) AS Total FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE u INNER JOIN TAG t INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec ON (rec.DocumentID=u.DocumentID AND rec.IsLatestFile=1 AND rec.IsFolder=0 AND rec.RecordStatus=1) INNER JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER gm ON (gm.GroupID=rec.GroupID $gmConds) WHERE t.TagID=u.TagID $conds GROUP BY u.TagID ORDER BY Total DESC $limit";
			}
		} else {
			
			$sql = "SELECT u.TagID, t.TagName, COUNT(u.TagUsageID) AS Total FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE u INNER JOIN TAG t ON (t.TagID=u.TagID) INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD r ON (r.SubjectResourceID=u.SubjectResourceID AND r.RecordStatus=1) GROUP BY u.TagID ORDER BY Total DESC $limit";
		}

		return $this->returnArray($sql);
		/*
		if($table!="") {
			$limit = ($returnAmount=="") ? "" : " LIMIT $returnAmount";
			
			$sql = "SELECT u.TagID, t.TagName, COUNT(u.TagUsageID) AS Total FROM $table u INNER JOIN TAG t WHERE t.TagID=u.TagID GROUP BY u.TagID ORDER BY Total DESC $limit";
			
			return $this->returnArray($sql);
		}
		*/
	}
	
	function getResourceInfoByTagID($tagid)
	{
		$sql = "SELECT r.SubjectResourceID, r.Title, r.Description, r.DateInput, r.Url, r.FileName, FileHashName, SourceExtension, LikeCount FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD r INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE u On (u.SubjectResourceID=r.SubjectResourceID AND u.TagID='$tagid') WHERE r.RecordStatus=1 ORDER BY r.SubjectResourceID DESC";
		return $this->returnArray($sql);	
	}
	
	function getAdminDocInfoByTagID($tagid)
	{
		global $UserID, $_SESSION;
		
		$additionConds = "";
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
		
			$groupAry = $this->UserInGroupList();
			
			for($i=0, $i_max=sizeof($groupAry); $i<$i_max; $i++) {
				$groupid = $groupAry[$i];
				
				
				if($this->IsAdminInGroup[$groupid]) {
						
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(r.GroupID='$groupid' AND r.InputBy='$UserID')" : " OR (r.GroupID='$groupid' AND r.InputBy='$UserID')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(r.InputBy='0')" : " AND r.InputBy='0'";
						}
					} else {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(r.GroupID='$groupid')" : " OR (r.GroupID='$groupid')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(r.GroupID='$groupid' AND r.InputBy!='$UserID')" : " AND(r.GroupID='$groupid' AND r.InputBy!='$UserID')";
						}
					}
				} else {
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(r.GroupID='$groupid' AND r.InputBy='$UserID')" : " OR (r.GroupID='$groupid' AND r.InputBy='$UserID')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(r.InputBy='0')" : " AND r.InputBy='0'";
						}
					} else {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(r.GroupID='$groupid')" : " OR (r.GroupID='$groupid')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(r.GroupID='$groupid' AND r.InputBy!='$UserID')" : " AND(r.GroupID='$groupid' AND r.InputBy!='$UserID')";
						}
					}
				}
			}	
			if($additionConds != "")
				$conds .= " AND ($additionConds)";
				
			 $gmConds = " AND gm.UserID='$UserID'";
		}
		
		$sql = "SELECT r.DocumentID, r.Title, r.Description, r.DateInput, r.FileFolderName, FileHashName, r.FileExtension, r.GroupID, r.ParentFolderID, r.ModifyBy, r.DateModified FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE u On (u.DocumentID=r.DocumentID AND u.TagID='$tagid') INNER JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER gm ON (gm.GroupID=r.GroupID $gmConds) WHERE r.IsLatestFile=1 AND r.RecordStatus=1 $conds Group By r.Thread ORDER BY r.DocumentID DESC";

		return $this->returnArray($sql);	
	}
		
	function Get_My_Favourite_Resource_Record($limitAmount="")
	{
		global $UserID;
		
		if($limitAmount!="") $limit = " LIMIT $limitAmount";
		
		$sql = "SELECT 
					rec.SubjectResourceID,
					rec.Title,
					rec.Url,
					rec.FileHashName,
					rec.SourceExtension,
					rec.Description
				FROM
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec 
					INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD lk ON (lk.SubjectResourceID=rec.SubjectResourceID AND lk.UserID='$UserID' AND rec.RecordStatus=1)
				GROUP BY
					rec.SubjectResourceID 
				ORDER BY 
					lk.DateInput DESC
				$limit
				";
				
		$data = $this->returnArray($sql);
		
		return $data;	
	}
	
	function Display_Tag_By_Resource_Record_ID($RecordID, $returnFlag='text')
	{
		$sql = "SELECT t.tagID, t.TagName FROM TAG t INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE u ON (u.TagID=t.TagID AND SubjectResourceID='$RecordID')";
		$data = $this->returnArray($sql);
		
		if($returnFlag=='text') {
			for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
				list($tagid, $tagname) = $data[$i];
				$text .= '<a href="displayTag.php?tagid='.$tagid.'">'.$tagname.'</a>';	
			}
			return $text;
		} else if($returnFlag=='textWithoutLink') {
			$delim = "";
			for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
				list($tagid, $tagname) = $data[$i];
				$text .= $delim.$tagname;
				$delim = ", ";	
			}
			return $text;
		} else {
			# return array
			return $data;
		}
	}
	
	function Display_Tag_By_Admin_Document_ID($RecordID, $returnFlag='text')
	{
		$sql = "SELECT t.tagID, t.TagName FROM TAG t INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE u ON (u.TagID=t.TagID AND DocumentID='$RecordID')";
		$data = $this->returnArray($sql);
		
		if($returnFlag=='text') {
			for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
				list($tagid, $tagname) = $data[$i];
				$text .= '<a href="displayTag.php?tagid='.$tagid.'">'.$tagname.'</a>';	
			}
			return $text;
		} else if($returnFlag=='textWithoutLink') {
			$delim = "";
			for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
				list($tagid, $tagname) = $data[$i];
				$text .= $delim.$tagname;
				$delim = ", ";	
			}
			return $text;
		} else {
			# return array
			return $data;
		}
	}
		
	function Display_ClassLevel_By_Resource_Record_ID($RecordID, $returnFlag='text')
	{
		$sql = "SELECT y.YearName FROM YEAR y INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION r ON (r.ClassLevelID=y.YearID AND r.SubjectResourceID='$RecordID')";
		$data = $this->returnVector($sql);
		$delim = "";
		if($returnFlag=='text') {
			for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
				$text .= $delim.$data[$i];
				$delim = ", ";
			}
			return $text;
		} else {
			return $data;	
		}	
	}
	
	function Get_ClassLevel_By_RecordID($RecordID)
	{
		$sql = "SELECT ClassLevelID FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION WHERE SubjectResourceID='$RecordID'";
		return $this->returnVector($sql);
			
	}
	
	function Display_Subject_By_Resource_Record_ID($RecordID, $returnFlag='text')
	{
		$sql = "SELECT ".Get_Lang_Selection("CH_DES","EN_DES")." FROM ASSESSMENT_SUBJECT SUB INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD r ON (r.SubjectID=SUB.RecordID AND r.SubjectResourceID='$RecordID')";
		$data = $this->returnVector($sql);
			
		if($returnFlag=='text') {
			return $data[0];
		} else {
			return $data;	
		}
	}
	
	
	function Cancel_All_Like()
	{
		global $UserID;
		
		$sql = "SELECT SubjectResourceID FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD WHERE UserID='$UserID'";
		$result = $this->returnVector($sql);
		
		for($i=0, $i_max=sizeof($result); $i<$i_max; $i++) {		
			$this->Update_Like_Status($result[$i], -1);
		}
			
	}
	
	function Get_Newest_More_SQL($conds="", $returnSqlOnly=1, $limit="") 
	{
		global $UserID, $_SESSION;
		
		$isModuleAdmin = ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) ? 1 : 0;
		
		if($limit!="") {
			$limitValue = " LIMIT $limit";
			$addition = "ORDER BY SubjectResourceID DESC $limitValue";
			$sql = "SELECT
						rec.SubjectResourceID,
						rec.Title,
						rec.Description,
						rec.DateInput,
						rec.Url,
						rec.FileName,
						rec.FileHashName,
						rec.SourceExtension,
						rec.LikeCount
						
					FROM 
						DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec 
						INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION lvl ON (lvl.SubjectResourceID=rec.SubjectResourceID AND rec.RecordStatus=1) 
						LEFT JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD lk ON (lk.SubjectResourceID=lk.SubjectResourceID)
					$conds
					GROUP BY 
						rec.SubjectResourceID
					$addition
					";		
		} else {
			
			# Record For Group Admin
			$subjectResourceIDArr = $this->Get_Subject_eResources_Record_ID_By_Group_Admin();
			//IF(rec.Url IS NULL,CONCAT('<a href=\"download.php?FileHashName=',rec.FileHashName,'\">',rec.Title,'</a>'),CONCAT('<a href=\"',rec.Url,'\" target=\"_blank\">',rec.Title,'</a>')) as Supplementary,
			$sql = "SELECT
						rec.Title as Title,
						IF(rec.Description IS NULL OR rec.Description='','---',rec.Description) as Description,
						".Get_Lang_Selection("CH_DES", "EN_DES")." as Subject,
						rec.SubjectResourceID as ClassLevel,
						rec.LikeCount,
						rec.DateInput as DateInput,
						IF((rec.InputBy='".$UserID."' OR $isModuleAdmin OR rec.SubjectResourceID IN ('".implode("','", $subjectResourceIDArr)."')),
						CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" id=\"RecordID[]\" value=', rec.SubjectResourceID ,'>'),'-') as checkbox,
						CASE WHEN rec.SourceExtension ='url' THEN
                            CONCAT(
								'<span onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',0,0);\">',
								'<a href=\"',rec.Url,'\" target=\"_blank\">',rec.Title,'</a>',
								'</span>'
							)
							
                             WHEN rec.SourceExtension ='tvurl' THEN
                            CONCAT(
								'<span onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',0,0);\">',
								'<a href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" target=\"_blank\"  class=\"thickbox\" onclick=\"loadCampusTV(\'',rec.SubjectResourceID,'\')\">',rec.Title,'</a>',
								'</span>'
							)
                             ELSE
							CONCAT(
								'',
								'<a href=\"javascript:void(0);\" onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',\'download.php?FileHashName=',rec.FileHashName,'\',0);\" \">',rec.Title,'</a>',
								''
							)
                             END
						as Supplementary,

						rec.SubjectResourceID
                        
					FROM 
						DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec 
						INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION lvl ON (lvl.SubjectResourceID=rec.SubjectResourceID AND rec.RecordStatus=1) 
						LEFT JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD lk ON (lk.SubjectResourceID=lk.SubjectResourceID)
						INNER JOIN ASSESSMENT_SUBJECT SUB ON (SUB.RecordID=rec.SubjectID)
					WHERE
						rec.RecordStatus=1
						$conds
					GROUP BY 
						rec.SubjectResourceID
					
					";	
				
					
		}
		if($returnSqlOnly) 
			return $sql;
		else 
			return $this->returnArray($sql);
		
	}

	function Get_Favourite_More_SQL($conds="", $returnSqlOnly=1)
	{
		global $UserID, $_SESSION;
		
		$isModuleAdmin = ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) ? 1 : 0;
		
		# Get Record for Group Admin
		$subjectResourceIDArr = $this->Get_Subject_eResources_Record_ID_By_Group_Admin();
		//IF(rec.Url IS NULL,CONCAT('<a href=\"download.php?FileHashName=',rec.FileHashName,'\">',rec.Title,'</a>'),CONCAT('<a href=\"',rec.Url,'\" target=\"_blank\">',rec.Title,'</a>')) as Supplementary,
		$sql = "SELECT
					rec.Title as Title,
					IF(rec.Description IS NULL OR rec.Description='','---',rec.Description) as Description,
					".Get_Lang_Selection("CH_DES", "EN_DES")." as Subject,
					rec.SubjectResourceID as ClassLevel,
					rec.LikeCount,
					rec.DateInput as DateInput,
					IF((rec.InputBy='".$UserID."' OR $isModuleAdmin OR rec.SubjectResourceID IN ('".implode("','", $subjectResourceIDArr)."')),
						CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" id=\"RecordID[]\" value=', rec.SubjectResourceID ,'>'),'-') as checkbox,
					CASE WHEN rec.SourceExtension ='url' THEN
                        CONCAT(
						    '<span onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',0,0);\">',
						    '<a href=\"',rec.Url,'\" target=\"_blank\">',rec.Title,'</a>',
					    	'</span>'
					   	)
							
                          WHEN rec.SourceExtension ='tvurl' THEN
                        CONCAT(
							'<span onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',0,0);\">',
							'<a href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" target=\"_blank\"  class=\"thickbox\" onclick=\"loadCampusTV(\'',rec.SubjectResourceID,'\')\">',rec.Title,'</a>',
							'</span>'
						)
                          ELSE
						CONCAT(
						   	'',
					    	'<a href=\"javascript:void(0);\" onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',\'download.php?FileHashName=',rec.FileHashName,'\',0);\" \">',rec.Title,'</a>',
					    	''
					    )
                          END
                    as Supplementary,
					rec.SubjectResourceID
				FROM 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec 
					INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION lvl ON (lvl.SubjectResourceID=rec.SubjectResourceID AND rec.RecordStatus=1) 
					LEFT JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD lk ON (lk.SubjectResourceID=lk.SubjectResourceID)
					INNER JOIN ASSESSMENT_SUBJECT SUB ON (SUB.RecordID=rec.SubjectID)
				WHERE
					rec.RecordStatus=1
					$conds
				GROUP BY 
					rec.SubjectResourceID
				
				";	

		if($returnSqlOnly) 
			return $sql;
		else 
			return $this->returnArray($sql);
	}

	function Get_My_Subject_More_SQL($conds="", $returnSqlOnly=1)
	{
		global $UserID, $_SESSION;
		
		$isModuleAdmin = ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) ? 1 : 0;
		
		# Get Record for Group Admin
		$subjectResourceIDArr = $this->Get_Subject_eResources_Record_ID_By_Group_Admin();
		//IF(rec.Url IS NULL,CONCAT('<a href=\"download.php?FileHashName=',rec.FileHashName,'\">',rec.Title,'</a>'),CONCAT('<a href=\"',rec.Url,'\" target=\"_blank\">',rec.Title,'</a>')) as Supplementary,
		$sql = "SELECT
					rec.Title as Title,
					rec.Description,
					".Get_Lang_Selection("CH_DES", "EN_DES")." as Subject,
					rec.SubjectResourceID as ClassLevel,
					rec.LikeCount,
					rec.DateInput as DateInput,
					IF((rec.InputBy='".$UserID."' OR $isModuleAdmin OR  rec.SubjectResourceID IN ('".implode("','", $subjectResourceIDArr)."')),
						CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" id=\"RecordID[]\" value=', rec.SubjectResourceID ,'>'),'-') as checkbox,
					CASE WHEN rec.SourceExtension ='url' THEN
                        CONCAT(
							'<span onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',0,0);\">',
							'<a href=\"',rec.Url,'\" target=\"_blank\">',rec.Title,'</a>',
							'</span>'
						)
							
                         WHEN rec.SourceExtension ='tvurl' THEN
                        CONCAT(
							'<span onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',0,0);\">',
							'<a href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\"  class=\"thickbox\" onclick=\"loadCampusTV(\'',rec.SubjectResourceID,'\')\">',rec.Title,'</a>',
							'</span>'
						)
                         ELSE
						CONCAT(
							'',
							'<a href=\"javascript:void(0);\" onClick=\"js_Update_HitRate(\'".FUNCTION_MORE."\',',rec.SubjectResourceID,',\'download.php?FileHashName=',rec.FileHashName,'\',0);\" \">',rec.Title,'</a>',
							''
						)
                         END
				    as Supplementary,
					rec.SubjectResourceID
				FROM 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec 
					INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION lvl ON (lvl.SubjectResourceID=rec.SubjectResourceID)
					INNER JOIN ASSESSMENT_SUBJECT SUB ON (SUB.RecordID=rec.SubjectID)
					LEFT OUTER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD l ON (rec.SubjectResourceID=l.SubjectResourceID)
				WHERE
					rec.RecordStatus=1
					$conds
				GROUP BY 
					rec.SubjectResourceID
				
				";	
		if($returnSqlOnly) 
			return $sql;
		else 
			return $this->returnArray($sql);
		
	}
	
	function Get_Tag_More_SQL($conds="", $returnSqlOnly=1, $Module="")
	{
		switch($Module) {
			case $this->AdminModule: $table = "DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE"; break;	
			default: $table = "DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE"; break;	
		}
		
		$sql = "SELECT
					CONCAT('<a title=\"',LOWER(t.TagName),'\" href=\"displayTag.php?tagid=',t.TagID,'\">',t.TagName,'</a>') as Tag,
					tu.DateInput,
					CONCAT('<input type=\"checkbox\" name=\"TagID[]\" id=\"TagID[]\" value=', t.TagID ,'>') as checkbox
				FROM
					TAG t 
					INNER JOIN $table tu ON (tu.TagID=t.TagID)
				GROUP BY 
					t.TagID
				
				";			
	
		if($returnSqlOnly) 
			return $sql;
		else 
			return $this->returnArray($sql);
		
	}	
	
	
	function Delete_Subject_Resource_Record($RecordIdAry=array())
	{
		$result = array();
		
		if(sizeof($RecordIdAry)>0) {
			$sql = "UPDATE DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD SET RecordStatus=0 WHERE SubjectResourceID IN (".implode(',', $RecordIdAry).")";
			$result = $this->db_db_query($sql);
			
			$sql = "SELECT TagID, SubjectResourceID FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE WHERE SubjectResourceID IN (".implode(',', $RecordIdAry).") Group BY TagID, SubjectResourceID";
			$ary = $this->returnArray($sql);
			
			for($a=0; $a<count($ary); $a++) {
				list($tag_id, $rec_id) = $ary[$a];
				$sql = "SELECT COUNT(tu.TagUsageID) FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE tu INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD r ON (r.SubjectResourceID=tu.SubjectResourceID AND r.RecordStatus=1) WHERE tu.TagID='$tag_id' AND tu.SubjectResourceID!='$rec_id'";
				
				$count = $this->returnVector($sql);
				if($count[0]==0) {	# not in use any more
					removeTagModuleRelation($tag_id, $this->Module);
				}	
			}	
		}
		
		
		return $result;
	}
	
	function Unlike_Subject_Resource_Record($RecordIdAry=array())
	{
		global $UserID;
		
		$result = array();
		
		for($i=0, $i_max=sizeof($RecordIdAry); $i<$i_max; $i++) {
			$RecordID = $RecordIdAry[$i];
			
			$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD WHERE SubjectResourceID='$RecordID' AND UserID='$UserID'";
			$count = $this->returnVector($sql);
			
			if($count[0]!=0) { # user "Like" this before
				$sql = "SELECT LikeCount FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD WHERE SubjectResourceID='$RecordID'";
				$likeCount = $this->returnVector($sql);
				
				if($likeCount[0]>0) {
					$sql = "UPDATE DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD SET LikeCount=LikeCount-1 WHERE SubjectResourceID='$RecordID'";
					//echo $sql.'<br>';
					$result[] = $this->db_db_query($sql);
				} 
			} else {
				$result[] = false;	
			}
		}
		
		$sql = "DELETE FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD WHERE SubjectResourceID IN (".implode(',', $RecordIdAry).") AND UserID='$UserID'";
		$this->db_db_query($sql);
		
		
		return $result;
	}
	
	function Get_Subject_Resource_Data_By_ID($RecordID)
	{
		$sql = "SELECT * FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD WHERE SubjectResourceID='$RecordID'";
		$result = $this->returnArray($sql);
		return $result[0];	
	}
	function Get_Resource_View_Data_For_Layer($RecordID)
	{
		global $Lang;

		$name_field = getNameFieldWithClassNumberByLang("USR.");
		$sql = "SELECT $name_field UserName,COUNT(*) Cnt FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_VIEW_RECORD l INNER JOIN INTRANET_USER USR ON (USR.UserID=l.UserID AND USR.RecordStatus=1 $conds) WHERE SubjectResourceID='$RecordID' GROUP BY UserName ORDER BY CNT DESC";
		$data = $this->returnArray($sql);
		
		return $data;

	}	
	function Get_Resource_Like_Data_For_Layer($RecordID, $IncludeCurrentUser=0)
	{
		global $UserID, $Lang;
		
		if($IncludeCurrentUser!=1)
			$conds = " AND l.UserID != '$UserID'";
		
		$name_field = getNameFieldWithClassNumberByLang("USR.");
		$sql = "SELECT $name_field FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD l INNER JOIN INTRANET_USER USR ON (USR.UserID=l.UserID AND USR.RecordStatus=1 $conds) WHERE SubjectResourceID='$RecordID' ORDER BY l.DateInput DESC";
		$data = $this->returnVector($sql);
		return $data;
		/*$name = "";
		$delim = "";
		for($i=0, $i_max=sizeof($data); $i<$i_max; $i++) {
			$name .= $delim.$data[$i];
			$delim = ", ";	
		}		
		if($name!="")
			$name .= $Lang["DigitalArchive"]["ChooseLike"];
		
		return $name;*/	
	}
	
	function Update_Admin_Doc_Data($dataAry=array(), $TagName="", $DocumentID)
	{
		global $UserID;
		
		$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD SET ";
		if(sizeof($dataAry)>0) 
			foreach($dataAry as $_key=>$_value) $sql .= $_key."='$_value', ";
		$sql .= " DateModified=NOW(), ModifyBy='$UserID' WHERE DocumentID='$DocumentID'";
		$result = $this->db_db_query($sql);
		//echo $sql;
		$this->Delete_Admin_Document_Tag_Relation_By_ID($DocumentID);
		$this->Update_Admin_Doc_Tag_Relation($TagName, $DocumentID, $this->AdminModule);
		
		return $result;
	}
	
	function SearchTags($keyword, $matchAll=false)
	{
		$cond_like = '';
		if($matchAll){
			$equal_like = " = ";
			$wildcard = "";
		}else{
			$equal_like = " LIKE ";
			$wildcard = "%";
		}
		if(is_array($keyword) && count($keyword)>0)
		{
			$cond_like = " t.TagName ".$equal_like." '".$wildcard.$keyword[0].$wildcard."' ";
			for($i=1;$i<count($keyword);$i++)
			{
				$cond_like .= " OR t.TagName ".$equal_like." '".$wildcard.$keyword[$i].$wildcard."' ";
			}
		}else{
			$cond_like = " t.TagName ".$equal_like." '".$wildcard.implode("",(array)$keyword).$wildcard."'";
		}
		
		//$sql = "SELECT t.TagID FROM TAG AS t, DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE AS tu WHERE tu.TagID=t.TagID AND t.TagName LIKE '%{$keyword}%'";
		$sql = "SELECT t.TagID FROM TAG AS t INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE AS tu ON tu.TagID=t.TagID WHERE ".$cond_like;
		$rows = $this->returnVector($sql);
		
		if (sizeof($rows)>0)
			return (implode(",", $rows));
		else
			return -1;		
	}
		
	function AllMatches($tag)
	{   $tags = array();
		$tags = explode(",", $tag);
	    for($i=0;$i<count($tags);$i++){
	    	$tagsnew[$i] = "'".$tags[$i]."'";
	    }
		$tag = implode(",",$tagsnew);
		$sql = "SELECT tu.DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE AS tu, TAG AS t WHERE tu.TagID=t.TagID AND t.TagName IN ({$tag})";
		$rows = $this->returnVector($sql);
		$k=0;
		$documentID=array();
		for($j=0;$j<count($rows);$j++){
			$sql="SELECT t.TagName FROM TAG AS t, DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE AS tu WHERE t.TagID=tu.TagID AND tu.DocumentID='{$rows[$j]}'";
			$rows1[$j] = $this->returnVector($sql);
            
            $flag = 1;  
			foreach ($tags as $va) {  
			    if (in_array($va, $rows1[$j])) {  
			        continue;  
			    }else {  
			        $flag = 0;  
			        break;  
			    }  
			}  
                  
			//$new_arr = $tags+$rows1[$j];
			
			if($flag) {
				$documentID[$k]=$rows[$j];
				$k++;
			}

		} 
      return (implode(",", $documentID));
		
	}
		
	function Get_Admin_Doc_AdvanceSearchResult_SQL($conds="", $GroupID, $returnSqlOnly=1, $tagIDs=-1)
	{
		global $UserID, $SESSION_ACCESS_RIGHT, $_SESSION;
		//$groupAry = $this->UserInGroupList();
		
		$additionConds = "";
		
		$groupid = $GroupID;
		
		# search for tags first
		//$sql = "SELECT TagID FROM TAG when TagName like '%%' "
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if(!isset($groupid) || $groupid=='') {		// display record in search result page
				
				$editableDocument = $this->Get_Latest_Document(999999999);
				//debug_pr($editableDocument);
				
				$tempAry = array();
				if(count($editableDocument)>0) {
					for($a=0; $a<count($editableDocument); $a++) {
						$tempAry[] = $editableDocument[$a]['DocumentID'];	
					}
					$additionConds = "rec.DocumentID IN (".implode(',',$tempAry).")";
				}
			} else {			
				if($this->IsAdminInGroup[$groupid]) {
					
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$groupid' AND rec.InputBy='$UserID')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
						}
					} else {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-ADMIN_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid')" : " OR (rec.GroupID='$groupid')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')";
						}
					}
				} else {
					
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$groupid' AND rec.InputBy='$UserID')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
						}
					} else {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupid."-MEMBER_OWN-VIEW")) {	# can view own
							$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid')" : " OR (rec.GroupID='$groupid')";
						} else {	# cannot view own
							$additionConds .= ($additionConds=="") ? "(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$groupid' AND rec.InputBy!='$UserID')";
						}
					}
					
				}
			}
			
			if($additionConds != "")
				$conds .= " AND ($additionConds)";
	
					
			$gmConds = " AND gm.UserID='$UserID'";
			$dbtable .= " INNER JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER gm ON (gm.GroupID=rec.GroupID $gmConds) ";	
		}
	
		$name_field = getNameFieldByLang("USR.");
		$archived_name_field = getNameFieldByLang2("au.");
		/*
		$sql = "SELECT  
					rec.Title,
					IF(rec.IsFolder=0,rec.FileFolderName,'-') as FileFolderName,
					IF(rec.Description!='',rec.Description,'-') as Description, 
					IF(rec.IsFolder=1,'-',rec.VersionNo) as VersionNo, 
					rec.ParentFolderID,
					rec.SizeInBytes,
					rec.DocumentID,
					$name_field as InputBy,
					IF(rec.DateModified='0000-00-00 00:00:00','-',rec.DateModified) as DateModified,
					CONCAT('<input type=\"checkbox\" name=\"DocumentID[]\" id=\"DocumentID[]\" value=', rec.DocumentID ,'>') as checkbox,
					IF(rec.IsFolder=1, 'row_waiting', '') as trCustClass
					
				FROM 
					DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec
					$dbtable
					INNER JOIN INTRANET_USER USR ON (USR.UserID=rec.InputBy) 
					LEFT OUTER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE tu ON (tu.DocumentID=rec.DocumentID)
				WHERE 
					rec.IsLatestFile=1 AND rec.RecordStatus=1 AND rec.GroupID='$groupid'
					$conds 
				GROUP BY rec.DocumentID
				";
		*/
		
		if ($tagIDs!=-1)
		{
			$sql_cond_join = " LEFT OUTER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE tu ON (tu.DocumentID=rec.DocumentID) LEFT JOIN TAG AS t on (t.TagID=tu.TagID)";
		}
		$sql = "SELECT  
					rec.Title,
					IF(rec.IsFolder=0,rec.FileFolderName,'-') as FileFolderName,
					IF(rec.Description!='',rec.Description,'-') as Description, 
					IF(rec.IsFolder=1,'-',rec.VersionNo) as VersionNo, 
					rec.ParentFolderID,
					rec.SizeInBytes,
					rec.DocumentID,
					IF(USR.UserID IS NOT NULL,$name_field,$archived_name_field) as InputBy,
					IF(rec.DateModified='0000-00-00 00:00:00','-',rec.DateModified) as DateModified,
					CONCAT('<input type=\"checkbox\" name=\"DocumentID[]\" id=\"DocumentID[]\" value=', rec.DocumentID ,'>') as checkbox,
					IF(rec.IsFolder=1, 'row_waiting', '') as trCustClass,
					rec.GroupID
					
				FROM 
					DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec
					$dbtable
					LEFT JOIN INTRANET_USER USR ON (USR.UserID=rec.InputBy) 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON (au.UserID=rec.InputBy)
					$sql_cond_join
				WHERE 
					rec.IsLatestFile=1 AND rec.RecordStatus=1 AND rec.IsFolder=0
					$conds 
				GROUP BY rec.DocumentID
				";

		if($returnSqlOnly) 
			return $sql;
		else 
			return $this->returnArray($sql);
	}	
	
	function buildFileHierachy($GroupID, $DocumentID, $dir="", $includeDocID=array())
	{
		global $PATH_WRT_ROOT, $UserID;
		
		include_once("libfilesystem.php");
		include_once("libdigitalarchive_ui.php");
		$lfs = new libfilesystem();
		$ldaUI = new libdigitalarchive_ui();
		
		$docRoot = $PATH_WRT_ROOT."../intranetdata/digital_archive/admin_doc/";
		
		$fileList = $this->getDirectoryList($GroupID, $DocumentID);
		
		for($i=0, $i_max=sizeof($fileList); $i<$i_max; $i++) {
			
			if(sizeof($includeDocID)==0 || in_array($fileList[$i]['DocumentID'], $includeDocID)) {
				
				$data = $this->Get_Admin_Doc_Info($DocumentID);
				
				if($fileList[$i]['IsFolder']==1) {		# folder 
					//Henry Added 20131220
					$lfs->folder_new($dir."/".iconv("utf-8", "big5", $fileList[$i]['FileFolderName']));
					$this->buildFileHierachy($GroupID, $fileList[$i]['DocumentID'], $dir."/".iconv("utf-8", "big5", $fileList[$i]['FileFolderName']));
					
				} else {								# file
					
					//$lfs->file_copy($docRoot.$fileList[$i]['FileHashName'], $dir.'/'.iconv("UTF-8", "BIG5", $fileList[$i]['FileFolderName']));
					//Henry Added 20131220
					if(strtolower(substr($fileList[$i]['Title'],"-".strlen($fileList[$i]['FileExtension'])))==strtolower($fileList[$i]['FileExtension'])) {
						$filename = $fileList[$i]['Title'];
					}
					else{
						$filename = $fileList[$i]['Title'].".".$fileList[$i]['FileExtension'];
					}
					$lfs->file_copy($docRoot.$fileList[$i]['FileHashName'], $dir.'/'.iconv("UTF-8", "BIG5", $filename));
					//echo $docRoot.$fileList[$i]['FileHashName'].'<br>'.$dir.'/'.iconv("UTF-8", "BIG5", $filename).'<br><br>';
					# update view history log
					$this->UpdateViewHistoryLog($fileList[$i]['DocumentID'], $UserID);
					
				}	
			}
		}
		//exit;
		
	}
	
	function Get_Title_Of_Current_Thread($Thread)
	{
		$sql = "SELECT Title FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE Thread='$Thread' AND IsLatestFile=1";
		$data = $this->returnVector($sql);
		
		return $data[0];	
	}
	
	function Get_Tag_Used_In_Group($GroupID)
	{
		$sql = "SELECT t.TagID, t.TagName FROM TAG t INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE tu ON (tu.TagID=t.TagID) INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec ON (rec.DocumentID=tu.DocumentID AND rec.GroupID='$GroupID') GROUP BY t.TagID ORDER BY t.TagName";
		return $this->returnArray($sql,2);	
	}
	
	function UserInGroupList($userid="")
	{
	    global $UserID, $_SESSION, $sys_custom;
		
		if($userid=="") $userid = $UserID;
		
		if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
			# Do NOTHING
		} else {
			$dbtable = " INNER JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER gm ON (gp.GroupID=gm.GroupID)";
			$conds = " WHERE gm.UserID='$userid'";
		}
		$sql = "SELECT gp.GroupID FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP gp $dbtable $conds GROUP BY gp.GroupID ORDER BY gp.GroupTitle";
		if($sys_custom['digital_archive']['sortbyGroup']){
		    $sql = "SELECT gp.GroupID
                    FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP gp
                    $dbtable $conds
                    LEFT JOIN
                        DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN argcj ON (gp.GroupID = argcj.GroupID)
                    LEFT JOIN
                        DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY argc ON (argcj.CategoryID = argc.CategoryID)
                    GROUP BY
                        gp.GroupID
                    ORDER BY
                        CASE WHEN COALESCE(argc.DisplayOrder, '') = '' THEN 1 ELSE 0 END, argc.DisplayOrder , argc.CategoryCode, argc.CategoryTitle,
                        IFNULL(argc.DisplayOrder, LOWER(gp.GroupTitle)), gp.GroupID";
		}

		return $this->returnVector($sql);	
	}
	
	function Get_Editable_Record_ID_By_GroupID($GroupID="", $FolderID="")
	{
		global $UserID;
		$additionConds = "";
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			
			if($this->IsAdminInGroup[$GroupID]) {
						
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-MANAGE")) {	# can view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$GroupID' AND rec.InputBy='$UserID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
					}
				} else {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-MANAGE")) {	# can view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID')" : " OR (rec.GroupID='$GroupID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')";
					}
				}
			} else {
				
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE")) {	# can view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$GroupID' AND rec.InputBy='$UserID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
					}
				} else {	# can view others
				
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE")) {	# can view own
					
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID')" : " OR (rec.GroupID='$GroupID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')";
					}
				}
			}
			
			if($additionConds != "")
				$conds .= " AND ($additionConds)";
		}
		
		if($FolderID!="")
			$conds .= " AND ParentFolderID='$FolderID'";
		
		$sql = "SELECT DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec WHERE IsLatestFile=1 AND RecordStatus=1 AND GroupID='$GroupID' $conds";
		
		return $this->returnVector($sql);
		
	}
	
	function Get_Viewable_Record_ID_By_GroupID($GroupID="", $FolderID="")
	{
		global $UserID;
		$additionConds = "";
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			
			if($this->IsAdminInGroup[$GroupID]) {
						
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$GroupID' AND rec.InputBy='$UserID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
					}
				} else {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID')" : " OR (rec.GroupID='$GroupID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')";
					}
				}
			} else {
				
				if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy='$UserID')" : " OR (rec.GroupID='$GroupID' AND rec.InputBy='$UserID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.InputBy='0')" : " AND rec.InputBy='0'";
					}
				} else {	# can view others
				
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
					
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID')" : " OR (rec.GroupID='$GroupID')";
					} else {	# cannot view own
						$additionConds .= ($additionConds=="") ? "(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')" : " AND(rec.GroupID='$GroupID' AND rec.InputBy!='$UserID')";
					}
				}
			}
			
			if($additionConds != "")
				$conds .= " AND ($additionConds)";
		}
		
		if($FolderID!="")
			$conds .= " AND ParentFolderID='$FolderID'";
		
		$sql = "SELECT DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec WHERE IsLatestFile=1 AND RecordStatus=1 AND GroupID='$GroupID' $conds";
		
		return $this->returnVector($sql);
		
	}
	
	function checkDigitalArchiveRight($GroupID, $Role, $FileType, $Action)
	{
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='$GroupID' AND Role='$Role' AND FileType='$FileType' AND Action='$Action'";
		$result = $this->returnVector($sql);
		
		return ($result[0]==0) ? 0 : 1;
	}
	
	function getNewDocTotalSinceLastLogin($GroupID)
	{
		global $_SESSION, $UserID;
		
		$totalNoOfRecordInGroup = $this->Get_Total_No_Of_Record_Bt_GroupID($GroupID, $_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]);
		
		$this->getLastVisitOfDigitalArchive($GroupID);
		
		# no. of records by current user
		$sql = "
			SELECT
				COUNT(DISTINCT rec.DocumentID) as docTotal
			FROM 
				DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec 
			WHERE
				rec.GroupID='$GroupID' AND 
				rec.IsFolder=0 AND 
				rec.RecordStatus=1 AND 
				rec.IsLatestFile=1 AND 
				rec.InputBy='$UserID' AND
				DateInput>='".$_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]."'
			GROUP BY 
				rec.GroupID 
			
			";
			
		$result = $this->returnArray($sql);
		$myNoOfRecord = ($result[0][0]=="") ? 0 : $result[0][0];
		
		
		if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if($this->IsAdminInGroup[$GroupID]) {
				if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$recordNo = $totalNoOfRecordInGroup;
					} else {	# cannot view own
						$recordNo = $totalNoOfRecordInGroup - $myNoOfRecord[0];
					}
				} else {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$recordNo = $myNoOfRecord[0];
					} else {	# cannot view own
						$recordNo = 0;
					}
				}
				
			} else {
				
				if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# can view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$recordNo = $totalNoOfRecordInGroup;
					} else {	# cannot view own
						$recordNo = $totalNoOfRecordInGroup - $myNoOfRecord[0];
					}
				} else {	# cannot view others
					if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$recordNo = $myNoOfRecord[0];
						
						
					} else {	# cannot view own
						$recordNo = 0;
					}
				}
			}
		} else {
			$recordNo = $totalNoOfRecordInGroup;	
		}	

		
		return $recordNo;
	}
	
	function getIntranetGroupByYearID($AcademicYearID="")
	{
		if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
		
		$sql = "SELECT g.GroupID, g.TitleChinese, g.Title, g.RecordType, c.CategoryName FROM INTRANET_GROUP g INNER JOIN INTRANET_GROUP_CATEGORY c ON (c.GroupCategoryID=g.RecordType) WHERE (g.AcademicYearID='$AcademicYearID' OR g.AcademicYearID is NULL) AND (g.Title IS NOT NULL AND g.TitleChinese IS NOT NULL) ORDER BY c.CategoryName, ".Get_Lang_Selection("g.TitleChinese", "g.Title");
		
		return $sql;	
	}
	
	function ImportAccessGroupFromIntranetGroup($GroupAry, $GroupMember=array())
	{
		global $UserID;
		
		include_once("libgroup.php");
		$lg = new libgroup();
		
		for($i=0, $i_max=sizeof($GroupAry); $i<$i_max; $i++) {
			$intranet_groupid = $GroupAry[$i];	
			$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID='$intranet_groupid'";
			$result = $this->returnVector($sql);
			$title = $result[0];
			$RevisedTitleName = $title;
			$a = 1;
			while(!$this->Check_Group_Title($RevisedTitleName) && $a<=$this->RenameAs_MaxNo) {	# duplicated Title
				$RevisedTitleName = $title."($a)";
				$a++;
			}
			$title = $RevisedTitleName;
			
			# create group
			$GroupID = $this->Insert_Group_Access_Right("", $this->Get_Safe_Sql_Query($title), "", "", $this->DefaultRights);
			$Result[] = ($GroupID) ? 1 : 0;
			
			# update Group Code	
			$this->UPDATE_ACCESS_RIGHT_GROUP($GroupID, $this->Get_Safe_Sql_Query($title), "", $this->Get_Safe_Sql_Query("G".$GroupID));
			
			# copy GROUP member to access right group
			
			if(count($GroupMember) > 0 && in_array($intranet_groupid, $GroupMember)) {
				$memberList = $lg->returnStaffListByGroup($intranet_groupid);
				
				$values = "";
				$delim = "";
				for($j=0, $j_max=sizeof($memberList); $j<$j_max; $j++) {
					$userid = $memberList[$j][0];
					$values .= $delim." ('$GroupID', '$userid', 0, NOW(), '$UserID')";
					$delim = ", ";						
				}
				if($values != "") {
					$sql = "INSERT INTO DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER (GroupID, UserID, IsAdmin, DateInput, InputBy) VALUES $values";
					$this->db_db_query($sql);
					
				}
			}
		}
		
		return $Result;		
	}
	
	function Is_Duplicate_Tag($TagName, $TagID)
	{
		$sql = "SELECT COUNT(r.TagID) FROM TAG t INNER JOIN TAG_RELATIONSHIP r ON (t.TagID=r.TagID AND r.Module='".$this->AdminModule."' AND t.TagName='".$this->Get_Safe_Sql_Query($TagName)."') AND r.TagID!='$TagID'";
		
		$result = $this->returnVector($sql);
		return ($result[0]==0) ? 0 : 1;	
	}
	
	function updateRecordTagRelation($OriginalTagID, $NewTagID)
	{
		$sql = "UPDATE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE SET TagID='$NewTagID' WHERE TagID='$OriginalTagID'";
		return $this->db_db_query($sql);
	}
	
	function UpdateViewHistoryLog($DocumentID, $userId)
	{
		$sql = "SELECT COUNT(*) FROM DIGITAL_ARCHIVE_VIEW_HISTORY WHERE DocumentID='$DocumentID' AND UserID='$userId'";
		$count = $this->returnVector($sql);
		
		if($count[0]==0) {	# insert
			$sql = "INSERT INTO DIGITAL_ARCHIVE_VIEW_HISTORY SET DocumentID='$DocumentID', UserID='$userId', DateInput=NOW()";
		} else {			# update
			$sql = "UPDATE DIGITAL_ARCHIVE_VIEW_HISTORY SET DateInput=NOW() WHERE DocumentID='$DocumentID' AND UserID='$userId'";
		}
		$this->db_db_query($sql);
		
	}
	
	function Get_Last_View_Document($ViewAmount="", $OrderBy="date", $OrderBy2="ASC")
	{
		global $UserID;
		
		if($ViewAmount=="") $ViewAmount = $this->DefaultViewAmount;
		
		$sql = "SELECT r.Thread FROM DIGITAL_ARCHIVE_VIEW_HISTORY h INNER JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r ON (r.DocumentID=h.DocumentID AND h.UserID='$UserID') ORDER BY h.DateInput DESC LIMIT $ViewAmount";	
		$history = $this->returnVector($sql);
		
		$NoOfHistory = count($history);
		
		$returnAry = array();
		
		$sql = "SELECT DocumentID, Title, FileHashName, VersionNo, FileExtension, Description, SizeInBytes, IsFolder, GroupID, ParentFolderID, DateInput, DateModified FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE Thread IN (".implode(',',$history).") AND IsLatestFile=1 AND RecordStatus=1";
		$data = $this->returnArray($sql);	
		//debug_pr($data);
		$NoOfData = count($data);
		for($i=0; $i<$NoOfData; $i++) {
			if($OrderBy=="filename") {
				$returnAry[$data[$i]['Title']][] = $data[$i];
			} else if($OrderBy=="date") {
				$date = ($data[$i]['DateModified']=="0000-00-00 00:00:00") ? $data[$i]['DateInput'] : $data[$i]['DateModified'];
				
				$returnAry[$date][] = $data[$i];
			} else if($OrderBy=="location") {
				include_once("libdigitalarchive_ui.php");
				$ldaUI = new libdigitalarchive_ui();
				$path = $ldaUI->Get_Folder_Navigation($data[$i]['ParentFolderID'], " > ", $isListTable=0, $withGroupName=1, $data[$i]['GroupID']);
				
				$returnAry[$path][] = $data[$i];
			}
		}
		
		if($OrderBy2=="ASC") {
			if(count($returnAry)>0) 
				ksort($returnAry);
		} else {
			if(count($returnAry)>0) 
				krsort($returnAry);
		}

		if(count($returnAry)>0) {
			foreach($returnAry as $valueAry) {
				foreach($valueAry as $_ary)
					$newAry[] = $_ary;	
			}	
		}
		
		return $newAry;
	}
	
	function Get_Latest_Document($ViewAmount="", $OrderBy="date", $OrderBy2="ASC")
	{
		global $UserID;
		
		if($ViewAmount=="") $ViewAmount = $this->DefaultViewAmount;
		
		$allGroups = $this->getAllAccessRightGroup();
		
		$NoOfGroups = count($allGroups);
		$viewableDocument = array();
		for($i=0; $i<$NoOfGroups; $i++) {
			$groupId = $allGroups[$i]['GroupID'];	
			$record = $this->Get_Viewable_Record_ID_By_GroupID($groupId);
			$viewableDocument = array_merge($viewableDocument, $record);
		}
		
		if(count($viewableDocument)>0) {
			$conds = " DocumentID IN (".implode(',',$viewableDocument).") AND ";	
		} else {
			$conds = " DocumentID IN (0)";	
		}
		
		$sql = "SELECT DocumentID, Title, FileHashName, VersionNo, FileExtension, Description, SizeInBytes, IsFolder, GroupID, ParentFolderID, DateInput, DateModified FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE $conds IsFolder=0 AND IsLatestFile=1 AND RecordStatus=1 ORDER BY DateInput DESC LIMIT $ViewAmount";
		
		$data = $this->returnArray($sql);
		
		$NoOfData = count($data);
		
		for($i=0; $i<$NoOfData; $i++) {
			if($OrderBy=="filename") {
				$returnAry[STRTOUPPER($data[$i]['Title'])][] = $data[$i];
			} else if($OrderBy=="date") {
				$date = ($data[$i]['DateModified']=="0000-00-00 00:00:00") ? $data[$i]['DateInput'] : $data[$i]['DateModified'];
				
				$returnAry[$date][] = $data[$i];
			} else if($OrderBy=="location") {
				include_once("libdigitalarchive_ui.php");
				$ldaUI = new libdigitalarchive_ui();
				$path = $ldaUI->Get_Folder_Navigation($data[$i]['ParentFolderID'], " > ", $isListTable=0, $withGroupName=1, $data[$i]['GroupID']);
				
				$returnAry[$path][] = $data[$i];
			}
		}
		
		if($OrderBy2=="ASC") {
			if(count($returnAry) > 0) 
				ksort($returnAry);
		} else {
			if(count($returnAry) > 0)
				krsort($returnAry);
		}

		if(count($returnAry)>0) {
			foreach($returnAry as $valueAry) {
				foreach($valueAry as $_ary)
					$newAry[] = $_ary;	
			}	
		}
		
		return $newAry;
	}
	
	function Get_User_Name($userId, $field) 
	{
		$sql = "SELECT $field FROM INTRANET_USER WHERE UserID='$userId'";
		$result = $this->returnVector($sql);
		return $result[0];	
	}
	
	function getFolderIDByFolderName($GroupID, $FromFolderID, $FolderNameAry) 
	{
		
		$a = 0;
		$thisFolderID = $FromFolderID;
		while($a<count($FolderNameAry)) {
			$thisFolderName = $FolderNameAry[$a];
			$sql = "SELECT DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE GroupID='$GroupID' AND RecordStatus=1 AND ParentFolderID='$thisFolderID' AND Title='$thisFolderName'";
			$tmpResult = $this->returnVector($sql);
			
			if(count($tmpResult)==0) {
				return 0;
			} else {
				$thisFolderID = $tmpResult[0];
			}
			$a++;
		}
		return $thisFolderID;
	}
	
	function getDeleteLogSql($keyword='', $StartDate='', $EndDate='', $fileOrFolderName='', $groupName='', $createdBy='', $deletedBy=''){
		global $Lang;
			
		$name_field = getNameFieldWithClassNumberByLang("b.");
		if($keyword!=''){
			$keyword_conds = " AND (a.RecordDetail LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									OR 
									$name_field LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'						
									)";		
		}		
		
		if($fileOrFolderName!=''){
			$fileOrFolderNameCond = " AND (a.RecordDetail LIKE '%Filename: %" . $this->Get_Safe_Sql_Like_Query($fileOrFolderName) ."%GroupName%'  
									  OR a.RecordDetail LIKE '%Folder name: %" . $this->Get_Safe_Sql_Like_Query($fileOrFolderName) ."%GroupName%'
									  OR a.RecordDetail LIKE '%FileFolderName: %" . $this->Get_Safe_Sql_Like_Query($fileOrFolderName) ."%GroupName%'									 
									  ) ";
		}
		
		if($groupName!=''){
			$groupNameCond = " AND a.RecordDetail LIKE '%GroupName: %" . $this->Get_Safe_Sql_Like_Query($groupName) ."%VersionNo%' ";
		}
		
		if($createdBy!=''){
			$createdByCond = " AND a.RecordDetail LIKE '%UploadBy: %" . $this->Get_Safe_Sql_Like_Query($createdBy) ."%UploadTime%'  ";	
		}		
		
		if($deletedBy!=''){
			$deletedByCond = " AND 	$name_field LIKE '%".$this->Get_Safe_Sql_Like_Query($deletedBy)."%'	";		
		}			
		
		
		$sql = "SELECT 
					a.LogDate,
					$name_field AS logby,				
					a.RecordDetail
				FROM 
					MODULE_RECORD_DELETE_LOG AS a 
					INNER join INTRANET_USER AS b on (b.UserID = a.LogBy)
				WHERE
					LEFT(a.LogDate,10) >= '$StartDate' 
					AND LEFT(a.LogDate,10) <= '$EndDate'
					AND a.Module = '". $this->Module."'
					$keyword_conds
					$fileOrFolderNameCond
					$groupNameCond
					$createdByCond
					$deletedByCond
				";
				
		return $sql;		
	}
	
	
	function Get_Subject_eResources_Group_Sql($groupID='',$groupCode='', $user_id='', $keyword=''){
		
		if($groupID!=''){		
			//$groupIDCond = " AND dasrarg.GroupID = '$groupID' ";		
			$groupIDCond = " AND dasrarg.GroupID IN ('".implode("','", (array)$groupID)."') " ;
		}
		
		if($groupCode!=''){
			$groupCodeCond = " AND dasrarg.Code = '".$this->Get_Safe_Sql_Query($groupCode)."' ";
		}
		
		if($user_id!=''){
			$userIdCond = " AND dasrargm.UserID =  '$user_id'  ";
		}
	
		
		if(trim($keyword)!=''){
		
			$cond_keyword .= " 	AND dasrarg.GroupTitle LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' ";	
		}

	
		$subjectName = Get_Lang_Selection('aso.EN_DES','aso.CH_DES');
//		$classNameField = Get_Lang_Selection('y.YearName', 'y.ClassTitleEN');
		
		$Sql = " SELECT 
				 	dasrarg.Code AS GroupCode, 
				 	dasrarg.GroupTitle AS GroupTitle,
					IF (dasrarg.Description IS NULL OR dasrarg.Description='', '---', dasrarg.Description) AS Description, 					
					$subjectName AS Subject,
					IF(GROUP_CONCAT(DISTINCT y.YearName) IS NULL OR GROUP_CONCAT(DISTINCT y.YearName)='', '---', GROUP_CONCAT(DISTINCT y.YearName)) AS Form,
					CONCAT('<a class=\'tablelink\' href=\'group_member.php?GroupID[]=', dasrarg.GroupID, '\'>', COUNT(DISTINCT dasrargm.UserID) , '</a>') AS NoOfMember,
					CONCAT('<input type=\"checkbox\" name=\"GroupID[]\" value=\"', dasrarg.GroupID ,'\">') As CheckBox,
					dasrarg.GroupID As GroupID,
					dasrarg.SubjectID AS SubjectID,
					GROUP_CONCAT(DISTINCT y.YearID) As FormIDArrSting,
					GROUP_CONCAT(DISTINCT dasrargm.UserID) As MemberIDArrSting
  				 FROM 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP dasrarg
					LEFT JOIN ASSESSMENT_SUBJECT aso ON aso.RecordID = dasrarg.SubjectID
					LEFT JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_MEMBER dasrargm ON (dasrargm.GroupID = dasrarg.GroupID)
					LEFT JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_CLASSLEVEL dasrargc ON (dasrargc.GroupID = dasrarg.GroupID)
					LEFT JOIN YEAR y ON (y.YearID = dasrargc.ClassLevelID)
				 WHERE 1
					$groupIDCond
					$groupCodeCond
					$userIdCond
					$cond_keyword
				 GROUP BY
					
					dasrarg.GroupID
					
					
				";	
			
		return $Sql;
		
	}
	
	
	function Update_Subject_eResources_Group($groupID,$groupCode,$groupName,$description,$SubjectID,$ClassLevelIDArr){
		
		$success = array();
		if ($groupID == ''){
			return false;
		}
		
		$sql = "UPDATE 
						DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP 
					SET 
						Code='".$this->Get_Safe_Sql_Query($groupCode)."',
						GroupTitle='".$this->Get_Safe_Sql_Query($groupName)."',
						Description='".$this->Get_Safe_Sql_Query($description)."',
						SubjectID='$SubjectID'
					WHERE 
						GroupID ='$groupID' ";
						
		$success['updateGroup'] = $this->db_db_query($sql);
			
			
		# Delete Current Form
		$Sql = " DELETE FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_CLASSLEVEL
				 WHERE GroupID = $groupID";
		$result['DeleteForm'] = $this->db_db_query($Sql);
		
				 	
		# Insert Form
			
		$numOfClassLevelIDArr = count($ClassLevelIDArr);
		
		for($i=0;$i<$numOfClassLevelIDArr;$i++){
			$thisClassLevel = $ClassLevelIDArr[$i];
			$Sql = "
						INSERT INTO
									DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_CLASSLEVEL
									(GroupID,ClassLevelID,DateInput,InputBy)
						VALUES
								('".$groupID."',
								 '".$thisClassLevel."',
								 NOW(), 
								 '".$_SESSION['UserID']."'
								)

					";
			$result['InsertForm'][] = $this->db_db_query($Sql);	
		}	
		
		return $result;
		
	}
	
	
	function Add_Subject_eResources_Group($groupCode,$groupName,$description,$SubjectID,$ClassLevelIDArr){
			
			$result = array();
						
			$Sql = " INSERT INTO
								DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP
								(Code, GroupTitle, Description,SubjectID,DateInput,InputBy,DateModified,ModifiedBy)
						VALUES
								('".$this->Get_Safe_Sql_Query($groupCode)."',
								 '".$this->Get_Safe_Sql_Query($groupName)."',
								 '".$this->Get_Safe_Sql_Query($description)."',
								  '".$SubjectID."',
								  NOW(), 
								 '".$_SESSION['UserID']."',
								 NOW(), 
								 '".$_SESSION['UserID']."'
								)
					";

			$result['InsertGroup'] = $this->db_db_query($Sql);
			
			
			$thisGroupID = $this->db_insert_id();
			
			if($thisGroupID){	 
				
				# Insert Form
			
				$numOfClassLevelIDArr = count($ClassLevelIDArr);
				
				for($i=0;$i<$numOfClassLevelIDArr;$i++){
					$thisClassLevel = $ClassLevelIDArr[$i];
					$Sql = "
								INSERT INTO
											DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_CLASSLEVEL
											(GroupID,ClassLevelID,DateInput,InputBy)
								VALUES
										('".$thisGroupID."',
										 '".$thisClassLevel."',
										 NOW(), 
										 '".$_SESSION['UserID']."'
										)
		
							";
					$result['InsertForm'][] = $this->db_db_query($Sql);	
				}
			
			}
			return $result;
	}
	
	function Remove_Subject_eResources_Group($groupID){		
		//		
		if($groupID==''){
			return false;
		}
		
		# Delete Group
		$Sql = " DELETE FROM 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP
				 WHERE GroupID IN ('".implode("','", (array)$groupID)."')" ;

		$result['DeleteGroup'] = $this->db_db_query($Sql);	
			
		# Delete Form
		$Sql = " DELETE FROM 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_CLASSLEVEL
				 WHERE GroupID IN ('".implode("','", (array)$groupID)."')";
			 
		$result['DeleteGroupForm'] = $this->db_db_query($Sql);	
		
		# Delete Member 
		$Sql = " DELETE FROM 
						DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_MEMBER
				 WHERE GroupID IN ('".implode("','", (array)$groupID)."')";
				 
		$result['DeleteGroupMember'] = $this->db_db_query($Sql);
				
		return $result;		
	}
	
	function Get_Subject_eResources_Group_Member_Sql($groupID='', $user_id='', $keyword=''){
		
		$userNameField = getNameFieldByLang("iu.");
		
		if($groupID!=''){
			$groupIDCond = " AND dasrargm.GroupID = '$groupID' " ;
		}
		
		if($user_id!=''){
			$userIDCond = " AND dasrargm.UserID = '$user_id' ";		
		}
		
		if(trim($keyword)!=''){
			$keywordCond = " AND $userNameField LIKE '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' " ;
		}
		
		
		$Sql = " SELECT 
					$userNameField AS UserName,
					CONCAT('<input type=\"checkbox\" name=\"user_id[]\" value=\"', dasrargm.UserID ,'\">') As CheckBox,
					iu.UserID AS UserID,
					dasrargm.GroupID AS GroupID
				 FROM 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_MEMBER dasrargm
				 LEFT JOIN INTRANET_USER iu on iu.UserID = dasrargm.UserID
				 
			     WHERE 1
				 $groupIDCond 
				 $userIDCond
				 $keywordCond
		";
		
		return $Sql;
	}
	
	function Add_Subject_eResources_Group_Member($GroupID, $UserIdAry=array())
	{
		global $UserID;
		
		if(sizeof($UserIdAry)>0) {
			$sql = "INSERT INTO DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_MEMBER VALUES ";
			
			$delim = "";
			for($i=0, $i_max=sizeof($UserIdAry); $i<$i_max; $i++) {
				$uid = $UserIdAry[$i];
				$sql .= $delim." ('$GroupID', '$uid', 0, NOW(), '$UserID')";
				$delim = ", ";
			}
			
			return $this->db_db_query($sql);
		}
			
	}
	
	
	function Remove_Subject_eResources_Group_Member($GroupID, $UserIdAry=array())
	{
	
		if(sizeof($UserIdAry)>0) {
			
			$Sql = "DELETE FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_MEMBER  
				 	WHERE 
						UserID IN ('".implode("','", (array)$UserIdAry)."')
						AND GroupID = '$GroupID'
			";
			
			$result = $this->db_db_query($Sql);
					
			return $result;
		}
			
	}
	

	
	function Update_Subject_eResources_Tag_Name_By_Admin($originalTagID, $RecordID, $newTagID){
		
		if($RecordID){
			$recordID_cond = "AND SubjectResourceID = '$RecordID'";
				
		}
		
		$Sql= "UPDATE 
					DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE 
				Set 
					TagID = '$newTagID' 
				WHERE 1
					$recordID_cond
					 AND TagID = '$originalTagID' ";
	
		$result = $this->db_db_query($Sql);
		return $result;
	}
	
	function Get_Subject_eResources_Record_ID_By_Group_Admin(){
		
		global $UserID;
		
		# Get Group
		$groupSql= $this->Get_Subject_eResources_Group_Sql('','',$UserID);
		$groupInfoArray = $this->returnArray($groupSql);
		$subjectIdArray = array();
		$classIdArray = array();
		for($i=0;$i<count($groupInfoArray);$i++){	
			$thisSubjectId = $groupInfoArray[$i]['SubjectID'];
			$subjectIdArray[]= $groupInfoArray[$i]['SubjectID'];
			$classIdArray[$thisSubjectId][] = $groupInfoArray[$i]['FormIDArrSting'];
		}
		
	
		
		# Check with Current Subject Resources Records
		$CheckingSql= "SELECT * ,
							GROUP_CONCAT(lvl.ClassLevelID) As ClassLevelArrayString
					   FROM 
							DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD rec
					   		INNER JOIN DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION lvl ON (lvl.SubjectResourceID=rec.SubjectResourceID AND rec.RecordStatus=1) 
					   WHERE 
							rec.SubjectID IN ('".implode("','", $subjectIdArray)."')
					   GROUP BY
							rec.SubjectResourceID
						";
		$infoForChecingArray = $this->returnArray($CheckingSql);
	
		$subjectResourceIDArr = array();
		for($i=0;$i<count($infoForChecingArray);$i++){
			
			$thisClassLevelArrayString = $infoForChecingArray[$i]['ClassLevelArrayString'];		
			$thisSubjectID = $infoForChecingArray[$i]['SubjectID'];
			$thisSubjectResourceID = $infoForChecingArray[$i]['SubjectResourceID'];
			
			$thisGroupClassLevelArrayString = $classIdArray[$thisSubjectID][0];
			
			$thisGroupClassLevelArray = explode(',',$thisGroupClassLevelArrayString);
			
			for($j=0;$j<count($thisGroupClassLevelArray);$j++){
				$thisGroupClassLevelId = $thisGroupClassLevelArray[$j];	
				if(in_array($thisGroupClassLevelId, explode(',',$thisClassLevelArrayString))){
					
					$subjectResourceIDArr[]= $thisSubjectResourceID;
				}
			}		
		}
		
		$subjectResourceIDArr = array_unique($subjectResourceIDArr);	
		
		return $subjectResourceIDArr;
	}
	//Henry Added
	function Save_File_Format_Setting($FileFormatList) {
		
		if(!$FileFormatList) return 0;
		
		$returnVal = 1;
//		//change the array to "'value1','value2','value3'"
//		$FileFormatListStr = implode("','", $FileFormatList);
//		$FileFormatListStr = "'".$FileFormatListStr."'";
//		//remove newline characters
//		$FileFormatListStr = preg_replace('~[\r\n ]+~', '', $FileFormatListStr);
//		
//		//debug_pr($FileFormatListStr);
//		$sql = "DELETE FROM DIGITAL_ARCHIVE_FILE_FORMAT WHERE FileFormat NOT IN (".$FileFormatListStr.")";
//		$tempReturnVal = $this->db_db_query($sql);
		
		foreach ($FileFormatList as $FileFormat) {
			
			$FileFormat = strtolower(trim($FileFormat));
			$FileFormat = preg_replace('~[\r\n ]+~', '', $FileFormat);
			
			if(!$FileFormat) continue;
			
			$sql = "SELECT FileFormat
				FROM 
					DIGITAL_ARCHIVE_FILE_FORMAT
					WHERE FileFormat = '".$this->Get_Safe_Sql_Query($FileFormat)."'";
			//debug_pr($this->returnArray($sql));
			if($this->returnArray($sql)){
				continue;
			}
			$sql = 'INSERT INTO DIGITAL_ARCHIVE_FILE_FORMAT (
								FileFormat, 
								InputBy, 
								DateInput)
							VALUES (
								\''.$this->Get_Safe_Sql_Query($FileFormat).'\',
								\''.$_SESSION['UserID'].'\',
								NOW())';

			$Result[] = $this->db_db_query($sql);
			
			$tempReturnVal = !in_array(false,$Result);
			
			if(!$tempReturnVal){
				$returnVal = 0;
			}
		}
		//return !in_array(false,$Result);
		return $returnVal;
	}
	//Henry Added
	function Get_File_Format_Setting() {
		$sql = 'SELECT 
					FileFormat, 
					InputBy, 
					DateInput 
				FROM 
					DIGITAL_ARCHIVE_FILE_FORMAT
				ORDER BY
					FileFormat';

		$Setting = $this->returnArray($sql);
		/*for ($i=0; $i< sizeof($Setting); $i++) {
			$Return[$Setting[$i]['SettingName']] = $Setting[$i]['SettingValue'];
		}
	
		return $Return;*/
		return $Setting;
	}
	//Henry Added
	function Delete_File_Format_Setting($RecordID)
	{	
		$sql = "DELETE FROM DIGITAL_ARCHIVE_FILE_FORMAT WHERE RecordID='$RecordID'";
		return $this->db_db_query($sql);
	}
	function Has_Folder_In_Group($GroupID, $FolderID=0){
		global $_SESSION, $UserID;
			
			if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
				if($this->IsAdminInGroup[$GroupID]) {	# group admin
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
							$conds = " AND r1.InputBy='$UserID' ";
							$conds2 = " AND r2.InputBy='$UserID' ";
						} else {									# cannot view own
							$conds = " AND r1.InputBy='0' ";
							$conds2 = " AND r2.InputBy='0' ";
						}
					}  else {		# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
							# nothing filter
						} else {									# cannot view own
							$conds = " AND r1.InputBy!='$UserID' ";
							$conds2 = " AND r2.InputBy!='$UserID' ";
						}					
					}
					
						
				} else {	# group member
				
					if(!$this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# cannot view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
							$conds = " AND r1.InputBy='$UserID' ";
							$conds2 = " AND r2.InputBy='$UserID' ";
						} else {									# cannot view own
							$conds = " AND r1.InputBy='0' ";
							$conds2 = " AND r2.InputBy='0' ";
						}
					} else {	# can view others
						if($this->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
							# nothing filter
						} else {									# cannot view own
							$conds = " AND r1.InputBy!='$UserID' ";
							$conds2 = " AND r2.InputBy!='$UserID' ";
						}	
					}
				}
			}
			
			$returnAry = array();
			$sql = "SELECT '0' as DocumentID, '/' as Title,'-1' as ParentFolderID, COUNT(r1.DocumentID) as FileCount
					FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD as r1
					WHERE r1.IsFolder=1 AND r1.RecordStatus=1 AND r1.IsLatestFile=1 AND r1.GroupID='$GroupID' AND r1.ParentFolderID=0 $conds
					GROUP BY r1.ParentFolderID";
			$rootResultAry = $this->returnResultSet($sql);
			
			if($rootResultAry != NULL)
				return TRUE;
			else
				return FALSE;
			
	//		$sql = "SELECT r1.DocumentID, r1.Title, r1.ParentFolderID, COUNT(r2.DocumentID) as FileCount  
	//				FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r1 
	//				LEFT JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r2 ON r2.IsFolder=0 AND r2.RecordStatus=1 AND r2.ParentFolderID=r1.DocumentID AND r2.GroupID='$GroupID' $conds2 
	//				WHERE r1.IsFolder=1 AND r1.RecordStatus=1 AND r1.GroupID='$GroupID' $conds 
	//				GROUP BY r1.DocumentID";
	//		$result = $this->returnResultSet($sql);
	//		if(count($rootResultAry)>0) {
	//			array_unshift($result,$rootResultAry[0]);
	//		}
	//		$returnAry = $this->getFolderHierarchyFileCountArray($result, $FolderID);
			//$this->getFolderHierarchySubfoldersFileCount($returnAry, $FolderID); // $returnAry is pass by reference as it modify FileCount data of the same array
			
			//return $returnAry;
	}
	
	function Authenticate_DigitalArchive()
	{
		if($_SESSION['DigitalArchive_session_password']) return; // ok
		
		$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='DigitalArchive' AND SettingName='AccessSetting'";
		$result = $this->returnVector($sql);
		
		if($result[0] == '1' && !$_SESSION['DigitalArchive_session_password']){
			// redirect to login page
			intranet_closedb();
			header('Location: /home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/login.php');
			exit;
		}
	}
	
	function GetDigitalArchiveGeneralSetting($name){
		$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE SettingName='$name' AND Module='DigitalArchive'";
		$temp = $this->returnVector($sql);
		return $temp[0];
	}
	
	/*
	 * return : array([DocumentID] => parent folder tree that walks from current document to top)
	 */
	function getParentFolderHierarchy($GroupID, $DocumentIdAry)
	{
		$sql = "SELECT * FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE RecordStatus=1 AND GroupID='$GroupID' ORDER BY Title";
		$dataAry = $this->returnResultSet($sql);
		
		$document_count = count($DocumentIdAry);
		$documentIdToAry = array();
		for($i=0;$i<$document_count;$i++)
		{
			$returnAry = $this->buildParentFolderHierarchy($dataAry, $DocumentIdAry[$i]);
			$documentIdToAry[$DocumentIdAry[$i]] = $returnAry;
		}
		return $documentIdToAry;
	}
	
	/*
	 * This function is recursively calling itself to build the folder to parent folder tree.
	 * $dataAry : pass by reference to save data copy cost
	 */
	function buildParentFolderHierarchy(&$dataAry, $curID)
	{
		$returnAry = array();
		$numOfRecord = count($dataAry);
		$curParentID = 0;
		for($i=0;$i<$numOfRecord;$i++) {
			$id = $dataAry[$i]['DocumentID'];
			$is_folder = $dataAry[$i]['IsFolder'];
			$parent_folder_id = $dataAry[$i]['ParentFolderID'];
			if($id == $curID){
				$curParentID = $parent_folder_id;
				if(!isset($returnAry[$id])){
					$returnAry[$id] = array();
					$returnAry[$id]['Record'] = $dataAry[$i];
					if($parent_folder_id != 0)
					{
						$returnAry[$id]['Parent'] = array();
					} 
				}
			}
		}
		
		if($curParentID != 0)
		{
			for($i=0;$i<$numOfRecord;$i++) {
				$id = $dataAry[$i]['DocumentID'];
				$is_folder = $dataAry[$i]['IsFolder'];
				if($id == $curParentID) {
					$returnAry[$curID]['Parent'] = $this->buildParentFolderHierarchy($dataAry, $id);
				}
			}
		}
		return $returnAry;
	}
	
	/*
	 * This function is recursively calling itself to gather all parent folders allowed ip addresses.
	 * $ary : result from buildParentFolderHierarchy(&$dataAry, $curID)
	 * $returnAry : pass by reference, result array of restricted IP
	 */
	function getAllowIPInHierarchy($ary, &$returnAry)
	{
		if(count($ary) > 0){
			foreach($ary as $id => $subAry)
			{
				if($subAry['Record']['AllowIP'] != ''){
					$returnAry[] = $subAry['Record']['AllowIP'];
				}
				if(isset($subAry['Parent']))
					$this->getAllowIPInHierarchy($subAry['Parent'], $returnAry);
			}
		}
	}
	
	function isDocumentsAllowToDownload($GroupID, $DocumentIdAry, $ClientIP)
	{
		$is_ip_allowed = true;
		$documentIdToAry = $this->getParentFolderHierarchy($GroupID, $DocumentIdAry);
		foreach($documentIdToAry as $id => $data){
			$ipAry = array();
			$this->getAllowIPInHierarchy($data, $ipAry);
			for($i=0;$i<count($ipAry);$i++){
				$ip_list = explode("\n", $ipAry[$i]);
				$is_either_one_ip_allowed = false;
				for($j=0;$j<count($ip_list);$j++){
					if(testip(trim($ip_list[$j]), $ClientIP)){
						$is_either_one_ip_allowed = true;
					}
				}
				if(!$is_either_one_ip_allowed){
					$is_ip_allowed = false;
				}
			}
		}
		return $is_ip_allowed;
	}
}



?>
