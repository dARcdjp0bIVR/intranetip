<?php
// using by: 
/**
 * Log:
 * 2010-1-20 Sandy : displayRecordListMenu() , new navigation style
 *	
 * 2010-4-13 Adam : duplicate libelibrary.php and modify to fit new eBook 	
 */

include_once($intranet_root."/includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
class elibrary extends libdb
{		
	/* 
		For book conversion
	*/	
	var $breaker;												// Chapter breaker
	var $FileName;											// Input file (.txt)
	var $Content;												// Content of the text file
	var $RowArr;												// Attay of page content						
	var $BookTitle2;											// The title on each page	
	var $IndexPage;											// The table of content page index				
	var $FirstPage;											// The starting page index
	var $ChapterArr;											// Store the chapter information
	var $ChapterPrepageArr;								// Store the introduction information
	var $ChapterPageArr;									// Store the chapter to page index
	var $PageContentArr;									// Store the page information
	var $FileNameXML;										// Output content file (.xml)
	var $FileNameChapterXML;							// Output chapter file (.xml)
	var $BookType;											// Type of input books
	var $BottomLineSkip;									// Number of bottom lines to be skipped
	var $CheckIndent;										// Check the idents
	var $TitleIndenifier;										// Symbols for title
	var $SubTitleIndenifier;									// Symbols for sub title
	var $FootNotesType;									//Foot notes type
	var $FootNotesStr;										//Foot notes symbol
	var $FootNotesIndenifier;								// Symbols for foot notes
	
	var $Default_Copyright_CUP;							// cup copyright template
	var $Default_Copyright_Field_CUP;					// the number of field input in cup copyright template
	
	/*
	*	Constructor
	*/
	function elibrary($InputFile="", $InputType="")
	{
		$this->libdb();
		
		$this->breaker = "";
		if ($InputFile == "")
		{
			$this->FileName = "a64-1.txt";
		}
		else
		{
			$this->FileName = $InputFile;
		}
		if ($InputType == "")
		{
			$this->BookType = "1";
		}
		else
		{
			$this->BookType = $InputType;
		}
		
		$this->IndexPage = 4;
		$this->FirstPage = 1;
		$this->FileNameXML = "output.xml";
		$this->BottomLineSkip = 0;
		$this->CheckIndent = true;

		$this->FootNotesType = "1";
		$this->FootNotesStr = "　　 　";
									
		$this->TitleIndenifier = "***title***";
		$this->SubTitleIndenifier = "***sub_title***";
		$this->FootNotesIndenifier = "***foot_notes***";
		
		$this->ChapterArr = array();
		$this->ChapterPrepageArr = array();
		$this->ChapterPageArr = array();
		$this->PageContentArr = array();												
	}
	
	function GET_COPYRIGHT_TEMPLATE($source="cup")
	{
		if($source == "cup")
		{
			$this->Default_Copyright_Field_CUP = 5;
			
			$this->Default_Copyright_CUP .= "<b>CAMBRIDGE UNIVERSITY PRESS</b><br />";
			$this->Default_Copyright_CUP .= "Cambridge, New York, Melbourne, Madrid, Cape Town, Singapore, <br />S&#227;o Paulo, Delhi<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "Cambridge University Press<br />";
			$this->Default_Copyright_CUP .= "10 Hoe Chiang Road, #08-01/02, Keppel Towers, Singapore 089315<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "www.cambridge.org<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "Information on and purchase of this title: www.cambridge.org/<b>[JS1]</b><b>[/JS1]</b><br />or contact <u>hongkong@cambridge.org</u><br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "First published in print format <b>[JS2]</b><b>[/JS2]</b><br />";
			$this->Default_Copyright_CUP .= "&#169; Cambridge University Press <b>[JS3]</b><b>[/JS3]</b><br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "This publication is in copyright. Subject to statutory exception and to the provisions of relevant collective licensing agreements, no reproduction of any part may take place without the written permission of Cambridge University Press.<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "This electronic edition first published 2009<br />";
			$this->Default_Copyright_CUP .= "&#169; Cambridge University Press and BroadLearning Education 2009<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "ISBN <b>[JS4]</b><b>[/JS4]</b>  (paperback)<br />";
			$this->Default_Copyright_CUP .= "ISBN <b>[JS5]</b><b>[/JS5]</b>  (paperback plus audio CD pack)<br />";
			
			return $this->Default_Copyright_CUP;
		}
		
		return "";
	}

	
	/*
	*	Set indent checking 
	*/	
	function CHECK_IDENT($Option)
	{
		$this->CheckIndent = $Option;
	}	
	
	
	/*
	*	Set the book type 
	*/	
	function SET_BOOK_TYPE($InputType)
	{
		$this->BookType = $InputType;
	}
	
	/*
	*	Set the footnotes type 
	*/	
	function SET_FOOTNOTES_TYPE($InputType="",$InputStr="")
	{
		$this->FootNotesType = $InputType;
		$this->FootNotesStr = $InputStr;
	}		
	
	/*
	*	Set the page title 
	*/	
	function SET_SUB_TITLE($PageTitle)
	{
		$this->BookTitle2 = $PageTitle;
	}
		
	/*
	*	Set the first page index
	*/	
	function SET_FIRST_PAGE($FirstPage)
	{
		$this->FirstPage = $FirstPage;
	}

	
	/*
	*	Set the table of content index
	*/			
	function SET_INDEX_PAGE($IndexPage)
	{
		$this->IndexPage = $IndexPage;
	}
	
	
	/*
	*	Set the file to be input
	*/		
	function SET_INPUT_FILE($InputFile)
	{
		$this->FileName = $InputFile;
	}

	
	/*
	*	Set the file to be output
	*/					
	function SET_OUTPUT_FILE($OutputFile)
	{
		$this->FileNameXML = $OutputFile;
	}

	
	/*
	*	Convert the text file into array
	*/							
	function DO_CONVERSION()
	{
		$this->Content = file_get_contents($this->FileName);
		$this->RowArr = explode($this->breaker,$this->Content);				
				
		for ($i=0;$i<count($this->RowArr);$i++)
		{
			
			$PageArr = explode("\n",$this->RowArr[$i]);
			if ($i==$this->IndexPage)
			{
				if ($this->BookTitle2=="")
				{
					$this->BookTitle2 = $PageArr[1];
				}
				$ChapterCnt = 0;
				
				for ($j=0;$j<count($PageArr);$j++)
				{
					$pos1 = strpos($PageArr[$j],".....");
					if ($pos1 === false)
					{
						continue;
					}
					$pos2 = strrpos($PageArr[$j],".....");				
					if ($pos2 === false)
					{
						continue;
					}
					
					$ChapterName = substr($PageArr[$j],0,$pos1);
					$ChapterName = trim($ChapterName);
					$ChapterPage = substr($PageArr[$j],$pos2+1);
					$ChapterPage = trim($ChapterPage);
	
/*					
					if ($ChapterCnt == 0)
					{
						$this->FirstPage = intval($ChapterPage);
					}
*/					
					if (substr($ChapterName,0,strlen("　　"))=="　　")
					{
						$IsChapter = "0";
					}
					else
					{
						$IsChapter = "1";
					}
					$this->ChapterArr[] = array($ChapterName,$ChapterPage,$this->IndexPage+$this->FirstPage+intval($ChapterPage)-1,$IsChapter);
					$this->ChapterPageArr[] = $this->IndexPage+$this->FirstPage+intval($ChapterPage)-1;
	
					$ChapterCnt++;
				}
			}
			else if (($this->FirstPage != 0) && ($i > $this->IndexPage) && ($i < ($this->FirstPage+$this->IndexPage)))
			{	
				for ($j=0;$j<count($PageArr);$j++)
				{
					$pos1 = strpos($PageArr[$j],".....");
					if ($pos1 === false)
					{
						continue;
					}
					$pos2 = strrpos($PageArr[$j],".....");				
					if ($pos2 === false)
					{
						continue;
					}				
					$ChapterName = substr($PageArr[$j],0,$pos1);
					$ChapterName = trim($ChapterName);
					$ChapterPage = substr($PageArr[$j],$pos2+1);
					$ChapterPage = trim($ChapterPage);
		
					if (substr($ChapterName,0,strlen("　　"))=="　　")
					{
						$IsChapter = "0";
					}
					else
					{
						$IsChapter = "1";
					}					
					$this->ChapterArr[] = array($ChapterName,$ChapterPage,$this->IndexPage+$this->FirstPage+intval($ChapterPage)-1,$IsChapter);
					$this->ChapterPageArr[] = $this->IndexPage+$this->FirstPage+intval($ChapterPage)-1;
	
					$ChapterCnt++;
				}
			}
			else if ($i >= ($this->FirstPage+$this->IndexPage))
			{								
				if (is_array($this->ChapterPageArr) && in_array($i,$this->ChapterPageArr))
				{																				
					$TmpChapterID = $this->CHAPTER_INDEX($i);					
					$TmpChapterIDTitleArr = array();
					$TmpChapterIDSubTitleArr = array();
					
					
					$IsMultiChapter = false;
					if ($TmpChapterID && count($TmpChapterID)>1)
					{
						$IsMultiChapter = true;						
						for ($j=0;$j <count($TmpChapterID);$j++)
						{
							if ($this->ChapterArr[$TmpChapterID[$j]][3] == "1")
								$TmpChapterIDTitleArr[] = trim(str_replace("　　","",$this->ChapterArr[$TmpChapterID[$j]][0]));
							else	
								$TmpChapterIDSubTitleArr[] = trim(str_replace("　　","",$this->ChapterArr[$TmpChapterID[$j]][0]));
						}
					} 
										
										
					$ContentStr = "";
					$FoundContent = false;
					$IsFootprint = false;

					for ($j=0;$j <count($PageArr);$j++)
					{
						if ($IsMultiChapter)
						{
							if ((trim($PageArr[$j]) !="") && count($TmpChapterIDTitleArr)>0 && in_array(trim($PageArr[$j]),$TmpChapterIDTitleArr))
							{
								$FoundContent = true;								
								$ContentStr .= $this->TitleIndenifier.$PageArr[$j]."\n";
								continue;
							}
							else if ((trim($PageArr[$j]) !="") && count($TmpChapterIDSubTitleArr)>0 && in_array(trim($PageArr[$j]),$TmpChapterIDSubTitleArr))
							{
								$FoundContent = true;								
								$ContentStr .= $this->SubTitleIndenifier.$PageArr[$j]."\n";								
								continue;
							}
						}
						else
						{
							if (trim($PageArr[$j]) == trim($this->ChapterArr[$TmpChapterID][0]))
							{
								$FoundContent = true;
								$ContentStr .= $this->TitleIndenifier.$PageArr[$j]."\n";
								continue;
							}
						}

						if ($this->FootNotesType=="1")
						{
							if  ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) ||
								(trim(substr($PageArr[$j],0,strlen($this->FootNotesStr))) == trim($this->FootNotesStr)))
							{
								$IsFootprint = true;
							}
						}
						else if ($this->FootNotesType=="2")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}							
						}
						else if ($this->FootNotesType=="3")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}							
						}
											
						if ($FoundContent) 
						{
							if (($PageArr[$j] == "") && ($ContentStr==""))
							{
								continue;
							} 
							else if (trim($PageArr[$j]) == trim($this->BookTitle2))
							{
								continue;
							}														
							else 
							{
								if ($IsFootprint)		
								{
									$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
								}
								else
									$ContentStr .= $PageArr[$j]."\n";
							}
						}
					} 	


					if (trim($ContentStr) =="")
					{
					$IsFootprint = false;
					for ($j=0;$j <count($PageArr);$j++)
					{
						if ($this->FootNotesType=="1")
						{
							if (substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr)
							{
								$IsFootprint = true;
							}
						}
						else if ($this->FootNotesType=="2")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}							
						}						
						else if ($this->FootNotesType=="3")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}							
						}						

						if (($PageArr[$j] == "") && ($ContentStr==""))
						{
							continue;
						} 
						else if (trim($PageArr[$j]) == trim($this->BookTitle2))
						{
							continue;
						}														
						else 
						{
							//$ContentStr .= $PageArr[$j]."\n";
							if ($IsFootprint)		
							{
								$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
							}
							else
								$ContentStr .= $PageArr[$j]."\n";
						}
					} 						
					}
		
					if (trim($ContentStr) !="")
					{
						if ($IsMultiChapter)
							$this->PageContentArr[] = array($ContentStr,$TmpChapterID,$this->ChapterArr[$TmpChapterID[0]][0]);
						else
							$this->PageContentArr[] = array($ContentStr,$TmpChapterID,$this->ChapterArr[$TmpChapterID][0]);
					}
										
				} 
				else 
				{
					$ContentStr = "";
					$FoundContent = false;		
					
					if (($this->FootNotesType=="1") || ($this->FootNotesType=="2"))
					{										
						$IsFootprint = false;
					}

					for ($j=0;$j <count($PageArr);$j++)
					{
						if (trim($PageArr[$j]) == trim($this->BookTitle2))
						{
							$FoundContent = true;
							continue;
						}
						if ($FoundContent) 
						{
							if (($PageArr[$j] == "") && ($ContentStr==""))
							{
								continue;
							} else {
								if ($this->FootNotesType=="1")
								{
									if (substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr)
									{
										$IsFootprint = true;
									}
								}
								else if ($this->FootNotesType=="2")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}							
								}
								else if ($this->FootNotesType=="3")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}							
								}
								
								if ($IsFootprint)		
								{
									$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
								} else {
									$ContentStr .= $PageArr[$j]."\n";
								}
							}
						}
					} 	
					if (trim($ContentStr) =="")
					{
					$IsFootprint = false;
					for ($j=0;$j <count($PageArr);$j++)
					{
							if (($PageArr[$j] == "") && ($ContentStr==""))
							{
								continue;
							} else {
								if ($this->FootNotesType=="1")
								{
									if (substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr)
									{
										$IsFootprint = true;
									}
								}
								else if ($this->FootNotesType=="2")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}							
								}
								else if ($this->FootNotesType=="3")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}							
								}
								
								if ($IsFootprint)		
								{
									$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
								} else {
									$ContentStr .= $PageArr[$j]."\n";
								}
							}
					} 						
					}
					
					if (trim($ContentStr) !="")
					{				
						$this->PageContentArr[] = array($ContentStr,$TmpChapterID,"");
					}
				}			
			}
			else
			{								
				$ContentStr = trim($this->RowArr[$i]);
				
				$TmpStr = "";
				$PageArr = explode("\n",$this->RowArr[$i]);
				for ($j=0;$j<count($PageArr);$j++)
				{				
					if ((trim($PageArr[$j]) != trim($this->BookTitle2)) && (trim($this->BookTitle2)!=""))
					{
						$TmpStr .= $PageArr[$j]."\n";
					}
				}												
				$this->ChapterPrepageArr[] = $TmpStr;				
			}
		}
	} // end function do convertion
	
	/*
	*	Convert & output the file
	*/	
	function DO_CONVERSION_CUP($ParArr="")
	{
		$xml_contents = $ParArr["xml_contents"];
		$xml_parser = $ParArr["xml_parser"];
		$Location3 = $ParArr["location_orig"];
		$Location4 = $ParArr["location_tmp_convert"];
		
		$xml_contents = str_replace("<emphasis>","&lt;emphasis&gt;",$xml_contents);
		$xml_contents = str_replace('<emphasis role="bold">',"&lt;emphasis&gt;",$xml_contents);
		$xml_contents = str_replace("</emphasis>","&lt;/emphasis&gt;",$xml_contents);
		$xml_contents = str_replace("<glossterm>","&lt;superscript&gt;",$xml_contents);
		$xml_contents = str_replace("</glossterm>","&lt;/superscript&gt;",$xml_contents);
		$xml_contents = str_replace("<superscript>","&lt;superscript&gt;",$xml_contents);
		$xml_contents = str_replace("</superscript>","&lt;/superscript&gt;",$xml_contents);		
		$xml_contents = str_replace("<glosssee>","&lt;glosssee&gt;",$xml_contents);
		$xml_contents = str_replace("</glosssee>","&lt;/glosssee&gt;",$xml_contents);
		
		xml_parse_into_struct($xml_parser, $xml_contents, $temp_arr_vals);
		xml_parser_free($xml_parser);
	
		$Level = 0;
		$OpenArr = array();
		$OutputArr = array();
		$TmpArr = array();	
		$PrevLevel = -1;
		$MaxLevel = -1;
		$CurrentChapter = 0;	

		$OpenTag = "";		
		$IsTagOpen = false;
		$ChapterCnt = 0;	
		$PageCnt = 0;
		$ChapterPageCnt = 0;

		$IsPart = false;
		$InPartTab = false;
		$BufferPartLabel = "";
		$InChapterTab = false;
						
		$AllCdata = "";
			
		foreach($temp_arr_vals as $key=>$value)
		{
			if($value['type'] == "open")
			{
				if ($value['tag'] == "PART")
				{
					$IsPart = true;
					$InPartTab = true;
					//echo "<pre>open part tab, set InPartTab = true</pre>";
					//var_dump($InPartTab);
					$BufferPartLabel = $value['attributes']['LABEL'];
				}		
				else if ($value['tag'] == "CHAPTER")
				{
					$InPartTab = false;
					$InChapterTab = true;
					//echo "<pre>open chapter tab, set InPartTab = false</pre>";
					//var_dump($InPartTab);
					$CurrentChapter = $value['attributes']['LABEL'];
					$ChapterCnt++;
					$PageCnt++;
					$ChapterPageCnt = 1;
				}
				else if ($value['tag'] == "PARA")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"cut");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "MSGMAIN")
				{
					$TmpArr = array("class"=>"text","val"=>"----------------------------------------------------------------------","page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"email");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "MSGTEXT")
				{
					$TmpArr = array("class"=>"text","val"=>"----------------------------------------------------------------------","page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"email");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}										
			}
			//start - handle the content after <cut /> within <para>bah bah bah <cut />bah bah bah</para>
			////////////////////////////////////////////////////////////////////////////////////////////////////			
			else if ($value['type'] == "cdata")
			{
				if ($value['tag'] == "PARA")
				{
					$AllCdata .= $value['value']; //add all the text segments together until close tag	
				}
			}
			else if ($value['type'] == "close")
			{
				if ($value['tag'] == "CHAPTER") 
				{
					if($IsPart == true){
						$OutputArr[$CurrentChapter]["part_title"] = trim($BufferPartLabel);
						$IsPart = false;
						$BufferPartLabel = "";	
					}else{
						$OutputArr[$CurrentChapter]["part_title"] = "null";	
					}
				}
				else if ($value['tag'] == "PARA") 
				{
					$TmpArr = array("class"=>"text","val"=>trim($AllCdata),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"tail");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
					$AllCdata = ""; //reset
				}
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////	
			//end - handle the content after <cut /> within <para>bah bah bah <cut />bah bah bah</para>										
			else if ($value['type'] == "complete")
			{
				//if ($value['tag'] == "PARTTITLE")
				//{
				//	if ($value["level"] == 3)
				//	{
				//		$BufferPartLabel = trim($BufferPartLabel)."  ".trim($value['value']);
				//	}
				//}				
				if ($value['tag'] == "TITLE")
				{
					if($InPartTab == true){
						$BufferPartLabel = trim($BufferPartLabel)."  ".trim($value['value']);
						$InPartTab = false;
						//echo "<pre>complete title tab, set InPartTab = false</pre>";
						//var_dump($InPartTab);
					}else if($InChapterTab == true){
						//$OutputArr[$CurrentChapter]["title"] = "<bold>".trim($CurrentChapter)." </bold>"." ".trim($value['value']);
						$OutputArr[$CurrentChapter]["title"] = trim($CurrentChapter)."   ".trim($value['value']);
						$OutputArr[$CurrentChapter]["chapter_id"] = trim($ChapterCnt);
						$InChapterTab = false;
					}	
				}
				else if ($value['tag'] == "PARA")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "PARAMETER")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"email");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}				
				else if ($value['tag'] == "GLOSSENTRY")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
					$TmpArr = array("class"=>"text","val"=>"","page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;					
				}
				else if ($value['tag'] == "GRAPHIC")
				{
					$TmpArr = array("class"=>"image","val"=>$value['attributes']['FILEREF'],"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "BREAK")
				{
					$ChapterPageCnt++;
					$PageCnt++;
				}
				else if ($value['tag'] == "CUT")
				{
					if($AllCdata != "") { //there is a long paragraph spread 3 pages!
						$TmpArr = array("class"=>"text","val"=>trim($AllCdata),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"middle");
						$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
						$AllCdata = ""; //reset
					}	
					$ChapterPageCnt++;
					$PageCnt++;
				}				
				else
				{}
			}
		} // end for loop
	
		//echo "<pre>";
		//var_dump($temp_arr_vals);
		//var_dump($OutputArr);
		//echo "</pre>";
		//die();

		$imageFolder = array("images/", "Images/");
		$outputFile = "output.xml";
		$chapterFile = "chapter.xml";
		$currentPage = 0;
	
		$x = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; 	
		$y = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$y .= "<chapter>\n";
	
		$x .= "<body>\n";
		if (is_array($OutputArr) && (count($OutputArr)>0))
		{
			foreach ($OutputArr as $Key => $Value)
			{	
				$TmpParaArr = $Value["para"];
				if (count($TmpParaArr)>0)
				{
					for ($i=0;$i<count($TmpParaArr);$i++)
					{
						if ($TmpParaArr[$i]["page_id"] != $currentPage)
						{
							$x .= "<a id=\"page_".trim($TmpParaArr[$i]["page_id"])."\" ></a>\n";
							if ($i==0) //first page of the chapter		
							{	
								if($Value["part_title"] != "null"){
									$x .= "<p class=\"part_title\">". trim($Value["part_title"])."</p>\n";
								}					
								$x .= "<p class=\"chapter_title\">". trim($Value["title"])."</p>\n";
								if ($TmpParaArr[$i]["class"] == "text")
								{
									if($TmpParaArr[$i]["justify"] == "cut") 
									{
										$x .= "<p class=\"noindent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else
									{	
										$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}	
								}
								else if ($TmpParaArr[$i]["class"] == "image")
								{
									
									$x .= "<p class=\"image1\"><img src=\"".str_replace($imageFolder, "image/",trim($TmpParaArr[$i]["val"]))."\" alt=\"Image\"></img></p>\n";					
								}					
					
								if($Value["part_title"] != "null"){
									$modifyCid = -1*$Value["chapter_id"]; 
									$y .= "<title page=\"".$TmpParaArr[$i]["page_id"]."\" cid=\"".trim($modifyCid)."\" sid=\"0\" isChapter=\"2\" >".strip_tags(trim($Value["part_title"]))."</title>\n";
								}	
								$y .= "<title page=\"".$TmpParaArr[$i]["page_id"]."\" cid=\"".trim($Value["chapter_id"])."\" sid=\"0\" isChapter=\"1\" >".strip_tags(trim($Value["title"]))."</title>\n";					
							}	
							else
							{
								if ($TmpParaArr[$i]["class"] == "text")
								{
									if($TmpParaArr[$i]["justify"] == "cut") 
									{
										$x .= "<p class=\"indent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else if($TmpParaArr[$i]["justify"] == "tail") 
									{
										$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";									
									}
									else if($TmpParaArr[$i]["justify"] == "middle") 
									{
										$x .= "<p class=\"noindent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";									
									}
									else if($TmpParaArr[$i]["justify"] == "email") 
									{
										$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";									
									}									
									else
									{
										$x .= "<p class=\"indent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}	
								}
								else if ($TmpParaArr[$i]["class"] == "image")
								{
									$x .= "<p class=\"image1\"><img src=\"".str_replace($imageFolder, "image/",trim($TmpParaArr[$i]["val"]))."\" alt=\"Image\"></img></p>\n";					
								}					
							}
							$currentPage = $TmpParaArr[$i]["page_id"];
					}	// end if currentpage id
					else
					{
						if ($TmpParaArr[$i]["class"] == "text")
						{
							if($TmpParaArr[$i]["justify"] == "cut") 
							{
								$x .= "<p class=\"indent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
							}
							else if($TmpParaArr[$i]["justify"] == "email") 
							{
								$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";									
							}							
							else
							{					
								$x .= "<p class=\"indent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
							}	
						}
						else if ($TmpParaArr[$i]["class"] == "image")
						{
							$x .= "<p class=\"image1\"><img src=\"".str_replace($imageFolder, "image/",trim($TmpParaArr[$i]["val"]))."\" alt=\"Image\"></img></p>\n";					
						}
					}
				} // end for each row
			} // end if each data
		} // end for each OutputArr
		} // end if outputArr > 0
		$x .= "</body>\n";
		$y .= "</chapter>\n";	

		$x = str_replace("<glosssee>","",$x);
		$x = str_replace("</glosssee>","",$x);
		$x = str_replace("<glossterm>","&lt;superscript&gt;",$x);
		$x = str_replace("</glossterm>","&lt;/superscript&gt;",$x);
		//$x = str_replace("<glossterm>","&lt;font size=&apos;20&apos; face=&apos;GG Superscript&apos;&gt;", $x);
		//$x = str_replace("</glossterm>","&lt;/font&gt;",$x);
		$x = str_replace("<superscript>","&lt;superscript&gt;",$x);
		$x = str_replace("</superscript>","&lt;/superscript&gt;",$x);
		$x = str_replace("<bold>","&lt;b&gt;",$x);
		$x = str_replace("</bold>","&lt;/b&gt;",$x);
		$x = str_replace("<emphasis>","&lt;i&gt;",$x);
		$x = str_replace("</emphasis>","&lt;/i&gt;",$x);
		$x = str_replace('<p class="indent">* * *</p>', '<p class="indent-center">* * *</p>', $x);

		$handle = fopen($Location4."/".$outputFile, "w");
		fwrite($handle, $x);
		fclose($handle);

		$handle2 = fopen($Location4."/".$chapterFile, "w");
		fwrite($handle2, $y);
		fclose($handle2);
	} // end functio DO CONVERSION CUP

	/*
	*	Return the chapter id of a page
	*/	
	function CHAPTER_INDEX($page)
	{		
		$TmpArr = array();
		
		for ($i=0;$i<count($this->ChapterPageArr);$i++)
		{
			if (intval($this->ChapterPageArr[$i]) == intval($page))
			{
				
				$TmpArr[] = $i;
				
				//return $i;
				//break;
			}
		}		

		if (count($TmpArr)==0)
		{
			return -1;
		}				
		else if (count($TmpArr)==1)
		{
			return $TmpArr[0];
		}
		else
		{
			return $TmpArr;
		}
		
	}


	/*
	*	Convert the array into XML
	*/	
	function OUTPUT_XML($ParPrePageArr="",$ParPageArr="",$OutputFile="Output.xml",$ImagePath="")
	{
		$ImageCnt = 0;
		$ImageCnt2 = 0;
							
		$ImageArr = $this->GET_IMAGE_ARRAY($ImagePath);
			
		$x = "";
		$x .= "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$x .= "<body>\n";		
		for ($i=0;$i<count($ParPrePageArr);$i++)
		{

			$x .= "<a id=\"prepage_".($i+1)."\"  cid=\"-1\" ></a>\n";									
			$ParPrePageArr[$i] = trim($ParPrePageArr[$i]);
			$CurLineContent = strtolower(trim($ParPrePageArr[$i]));
			
			if (($CurLineContent=="[image here]") || ($CurLineContent=="image") || 
				($CurLineContent=="image here") || ($CurLineContent=="[image here] ")
				 || (strstr($CurLineContent,"image here")) )
			{				
				$ImageCnt++;
				
				$TmpImgName = "pre".$ImageCnt;
				if ($ImageArr[$TmpImgName] == "1")
				{
					$TmpImageName = "image/pre".$ImageCnt.".jpg";
					$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";					
				}
				else
				{
					for ($j=0;$j<$ImageArr[$TmpImgName];$j++)
					{
						$TmpImageName = "image/pre".$ImageCnt."_".($j+1).".jpg";
						$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
					}
				}
			}
			else
			{
				$TmpPageArr = explode("\n",$ParPrePageArr[$i]);
				// hard code to search pre heading
				//$tmpIntro = iconv("big5","utf-8//IGNORE", "");
				$tmpIntro = "";
				
				for ($j=0;$j<count($TmpPageArr);$j++)
				{
					if (trim($TmpPageArr[$j] != ""))
					{						
						//$TmpStr = iconv("big5","utf-8//IGNORE",trim($TmpPageArr[$j]));
						$TmpStr = trim($TmpPageArr[$j]);
						
						if($j == 0 && $TmpStr == $tmpIntro)
						$x .= "<p class=\"into1_title\">".$TmpStr."</p>\n";
						else
						$x .= "<p class=\"into1\">".$TmpStr."</p>\n";
					}
				}
			}
		}		
		for ($i=0;$i<count($ParPageArr);$i++)
		{
			$TmpPageID = $i+1;						
			$TmpCurPage = $this->IndexPage+$this->FirstPage+$i;
			
			$TmpCh1 = $this->CHAPTER_INDEX($TmpCurPage);			 
			if (is_array($TmpCh1))
			{
				if ($TmpCh1 != -1)
				{
					$TmpCurPage2 = $TmpCh1[0];
					$IsStart = 1;
				}
				else
				{
					$IsStart = 0;
				}				
			}
			else
			{
				if ($TmpCh1 != -1)
				{
					$TmpCurPage2 = $TmpCh1;
					$IsStart = 1;
				}
				else
				{
					$IsStart = 0;
				}
			}
			
			$x .= "<a id=\"page_".$TmpPageID."\" cid=\"".$TmpCurPage2."\" IsChapterStart=\"".$IsStart."\" ></a>\n";			
			
			if ((trim($ParPageArr[$i][0])=="[image here]") || (trim($ParPageArr[$i][0])=="image") || (trim($ParPageArr[$i][0])=="image here"))
			{				
				//$ImageCnt++;
				$TmpImageName = "image/p".$TmpPageID.".jpg";
				$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
			}
			else
			{			
				/*	
				if (trim($ParPageArr[$i][2]) != "")
				{
					$TmpStr = iconv("big5","utf-8//IGNORE",trim($ParPageArr[$i][2]));
					
					$x .= "<p class=\"chapter_title\">".$TmpStr."</p>\n";
				}
				*/
				$TmpPageArr = explode("\n", trim($ParPageArr[$i][0]));
				if ((trim($TmpPageArr[0])=="[image here]") || (trim($TmpPageArr[0])=="image") || (trim($TmpPageArr[0])=="image here"))
				{				
					//$ImageCnt++;
					$TmpImageName = "image/p".$TmpPageID.".jpg";
					$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
					continue;
				}
				$StillBlankBefore = true;
				for ($j=0;$j<count($TmpPageArr);$j++)
				{
					
					if ($j == count($TmpPageArr)-1)
					{
						/*
						if ((trim($TmpPageArr[$j]) != "") && (!is_nan(ceil(trim($TmpPageArr[$j])))) )
						{							
							continue;
						}
						*/
						
						if ((trim($TmpPageArr[$j]) != "") && (strlen(trim($TmpPageArr[$j]))<=5) && 
							(trim($TmpPageArr[$j]) >="1")  && (trim($TmpPageArr[$j]) <="9999") )
						{				
										
							continue;
						}
						
					}
//echo trim($TmpPageArr[$j])."<br />";					
					if ((trim($TmpPageArr[$j]) != "") || (!$StillBlankBefore))
					{																														
						if (substr($TmpPageArr[$j],0,strlen($this->TitleIndenifier)) == $this->TitleIndenifier)
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim(substr($TmpPageArr[$j],strlen($this->TitleIndenifier))));
							$TmpStr = trim(substr($TmpPageArr[$j],strlen($this->TitleIndenifier)));
							$x .= "<p class=\"chapter_title\">".$TmpStr."</p>\n";
						}
						else if (substr($TmpPageArr[$j],0,strlen($this->SubTitleIndenifier)) == $this->SubTitleIndenifier)
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim(substr($TmpPageArr[$j],strlen($this->SubTitleIndenifier))));
							$TmpStr = trim(substr($TmpPageArr[$j],strlen($this->SubTitleIndenifier)));
							$x .= "<p class=\"sub_chapter_title\">".$TmpStr."</p>\n";
						}
						else if ((substr($TmpPageArr[$j],0,strlen($this->FootNotesIndenifier)) == $this->FootNotesIndenifier) 
								&& ($this->FootNotesType!=""))	
						{							
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim(substr($TmpPageArr[$j],strlen($this->FootNotesIndenifier))));
							$TmpStr = trim(substr($TmpPageArr[$j],strlen($this->FootNotesIndenifier)));
							$x .= "<p class=\"foot_notes\">".$TmpStr."</p>\n";
						}					
						else
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim($TmpPageArr[$j]));
							$TmpStr = trim($TmpPageArr[$j]);
							if (!$this->CheckIndent)
							{
								$x .= "<p class=\"noindent\">".$TmpStr."</p>\n";
							}
							else
							{
//For checking								
//echo $StillBlankBefore." | ".trim($ParPageArr[$i][2])." | ".trim($TmpPageArr[$j-1])." | ".trim($TmpStr)."<br />";								
								if (($StillBlankBefore && (trim($ParPageArr[$i][2]) != "")) || 
									((!$StillBlankBefore) && (trim($TmpPageArr[$j-1]) == "") && (trim($TmpStr)!="")))							
								{
									$x .= "<p class=\"indent\">".$TmpStr."</p>\n";							
								}
								else	
								{
									$x .= "<p class=\"noindent\">".$TmpStr."</p>\n";
								}
							}
						}
						$StillBlankBefore = false;
					}
				}
			}			
//For checking			
//die();			
		}
		$x .= "</body>\n";
		
		
		$handle = fopen($OutputFile, "w");
		fwrite($handle, $x." \n");	
		fclose($handle);		
		
		return $x;
	}		

	
	/*
	*	Convert the array into XML
	*/	
	function OUTPUT_CHAPTER_XML($ParChapterArr="",$OutputFile="Chapter.xml")
	{
		$SubChapterCnt = 0;
		$CurChapter = 0;
		$CurPage = 0;
		
		$x = "";
				
		$x .= "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$x .= "<chapter>\n";		
		for ($i=0;$i<count($ParChapterArr);$i++)
		{
			
			$TmpPage = $ParChapterArr[$i][1];
			$TmpTitle = $ParChapterArr[$i][0];			
			if (substr($TmpTitle,0,strlen("　　"))=="　　")
			{
				$SubChapterCnt++;
				
				$TmpTitle = substr($TmpTitle,strlen("　　"));
				//$TmpTitle = iconv("big5","utf-8//IGNORE",trim($TmpTitle));						
				$TmpTitle = trim($TmpTitle);
				
				$x .= "<title page=\"".$TmpPage."\" cid=\"".$CurChapter."\" sid=\"".$SubChapterCnt."\" isChapter=\"0\" >{$TmpTitle}</title>"; 								
			}
			else
			{
				$CurChapter++;
				$SubChapterCnt = 0;
								
				//$TmpTitle = iconv("big5","utf-8//IGNORE",trim($TmpTitle));										
				$TmpTitle = trim($TmpTitle);
				
				$x .= "<title page=\"".$TmpPage."\" cid=\"".$CurChapter."\" sid=\"".$SubChapterCnt."\" isChapter=\"1\" >{$TmpTitle}</title>";				
			}
		}				
		$x .= "</chapter>\n";
		
		$handle = fopen($OutputFile, "w");
		fwrite($handle, $x." \n");	
		fclose($handle);		
		
		return $x;		
	}
		
	
	function MY_ECLASS_TABLE($class_icon, $RmTitle) 
	{
		global $image_path, $LAYOUT_SKIN;
                
		$x  = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td width='19'>$class_icon</td>";
		$x .= "<td>$RmTitle</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif' width='10' height='11'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= $whatsnew;
		$x .="</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align='right' class='tabletext'>$lastlogin</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif' width='12' height='12'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board07.gif' width='10' height='10'></td>";
		$x .= "<td height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif' width='10' height='10'></td>";
		$x .= "<td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif' width='12' height='10'></td>";
		$x .= "</tr>";
		$x .= "</table>";
                
		return $x;        
	}
	

	/*
	* Get MODULE_OBJ array
	*/
	function GET_MODULE_OBJ_ARR()
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path;
		### wordings
		global $eLib;
		global $CurrentPageArr;
		global $ip20TopMenu, $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
                
		$CurrentPageArr['eLibrary'] = 1;
                
        switch ($CurrentPage) 
        {
        	case "PageConvertBook":
        		$PageAdmin = 1;
            	$PageConvertBook = 1;
            	break;                            	
        	case "PageImportBook":
        		$PageAdmin = 1;
            	$PageImportBook = 1;
            	break;                            	        
        	case "PageContentManage":
        		$PageAdmin = 1;
            	$PageContentManage = 1;
            	break;                            	

        	case "PageContentView":
        		$PageAdmin = 1;
            	$PageContentView = 1;
            	break;                            	
            	            	    	            	
        }
			
                
		$MenuArr["Admin"] 	= array($eLib['ManageBook']["Title"],"", $PageAdmin);			
		$MenuArr["Admin"]["Child"]["ConvertBook"] = array($eLib['ManageBook']["ConvertBook"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/convert_book.php", $PageConvertBook);
		$MenuArr["Admin"]["Child"]["ImportBook"] = array($eLib['ManageBook']["ImportBook"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/import_book.php", $PageImportBook);
		$MenuArr["Admin"]["Child"]["ContentManage"] = array($eLib['ManageBook']["ContentManage"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/index.php", $PageContentManage);
		$MenuArr["Admin"]["Child"]["ContentView"] = array($eLib['ManageBook']["ContentView"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/view.php", $PageContentView);		
		
		# module information
		$MODULE_OBJ['title'] = $ip20TopMenu['eLibrary'];
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eLibrary.gif";
		$MODULE_OBJ['root_path'] = "#";		
		$MODULE_OBJ['menu'] = $MenuArr;
				
		return $MODULE_OBJ;
	}
	
	function GET_MODULE_OBJ_ARR2()
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path;
		### wordings
		global $eLib;
		global $CurrentPageArr;
		global $ip20TopMenu, $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
                
		$CurrentPageArr['eLibrary'] = 1;
		$CurrentPageArr['eLib'] = 1;
	
		# module information
		$MODULE_OBJ['title'] = $ip20TopMenu['eLibrary'];
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eLibrary.gif";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eclass/index_elib.php";		
		//$MODULE_OBJ['menu'] = $MenuArr;
				
		return $MODULE_OBJ;
	}
	
	function HAS_RIGHT($ParArr="")
	{
		return true;
	}
	
	function IMPORT_BOOK_INFO($InputFile,$ParArr="")
	{
		$Type = $ParArr["Type"];
		if ($Type=="")
		{
			$Type="sys";
		}
		$AuthorArray = array();
		
		if ($InputFile != "")
		{
			$row = 1;
			$handle = fopen($InputFile, "r");
			if ($handle)
			{				
				while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) 
				{
					if ($row !=1)
					{					
					    $TmpTitle = trim($data[0]);
					    $TmpDocument = trim($data[1]);
					    $TmpAuthor = trim($data[2]);
					    $TmpPublisher = trim($data[3]);
					    $TmpPreface = trim($data[4]);
					    $TmpAuthorDesc = trim($data[5]);
					    
					    if (trim($AuthorArray[$TmpAuthor]) == "")
					    {
						    $TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);
						    $AuthorArray[$TmpAuthor] = $TmpAuthorID;
					    }
					    else
					    {
						    $TmpAuthorID = $AuthorArray[$TmpAuthor];
					    }
					    
					    $TmpArr = array();
					    $TmpArr["Title"] =  $TmpTitle;
					    $TmpArr["Author"] =  $TmpAuthor;
					    $TmpArr["AuthorID"] =  $TmpAuthorID;
					    $TmpArr["Publisher"] =  $TmpPublisher;
					    $TmpArr["Preface"] =  $TmpPreface;
					    $TmpArr["Document"] =  $TmpDocument;
					    $TmpArr["InputBy"] =  $Type;
						$TmpArr["Source"]  = $ParArr["Source"];
						$TmpArr["Language"]  = $ParArr["Language"];
						$TmpArr["Category"]  = $ParArr["Category"];
					    		
										
					    if ($TmpDocument != "")
					    {
							$this->UPDATE_BOOK_INFO($TmpArr);	    
						}
					}
				    $row++;
				}
				fclose($handle);
			}
		}
	} // end function
	
	function IMPORT_BOOK_INFO_FROM_CUP($InputFile,$ParArr="")
	{
		//echo "<pre>IMPORT_BOOK_INFO_FROM_CUP is called</pre>";
		
		$Type = $ParArr["Type"];
		if ($Type=="")
		{
			$Type="sys";
		}
		$AuthorArray = array();
		
		//echo "<pre>InputFile is";
		//var_dump($InputFile);
		//echo "</pre>";
		
		if ($InputFile != "")
		{
			$row = 1;
			$handle = fopen($InputFile, "r");
			
			if ($handle)
			{				
				while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) 
				{
					//echo "<pre>data is";
					//var_dump($data);
					//echo "</pre>";

					//echo "<pre>row is";
					//var_dump($row);
					//echo "</pre>";
					
					if ($row != 1)
					{
						$TmpIndexNumber = trim($data[0]);
					    $TmpTitle = trim($data[1]);
					    $TmpDocument = trim($data[2]);
					    $TmpISBN = trim($data[3]);
					    $TmpAuthor = trim($data[4]);
					    $TmpPublisher = trim($data[5]);
					    $TmpSubCategory = trim($data[6]);
					    $TmpLevel = trim($data[7]);
					    $TmpAdultContent = trim($data[8]);
					    $TmpPreface = trim($data[9]);
					    $TmpAuthorDesc = "";
					    
					     //debug_r($data);
					    
					    if (trim($AuthorArray[$TmpAuthor]) == "")
					    {
						    //$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);
						    $AuthorArray[$TmpAuthor] = $TmpAuthorID;
					    }
					    else
					    {
						    $TmpAuthorID = $AuthorArray[$TmpAuthor];
					    }
					    
					    if($ParArr["Category"] == "")
					    $TmpCategory = "Readers";
					    else
					    $TmpCategory = $ParArr["Category"];
					    
					    if($TmpLevel != "")
					    $TmpLevel = $TmpLevel[0];
					    
					    if($TmpSubCategory != "")
					    $TmpSubCategory = ucwords(strtolower($TmpSubCategory));
					    
					   
					    $TmpArr = array();
					    $TmpArr["Title"] =  htmlspecialchars($TmpTitle, ENT_QUOTES);
					    $TmpArr["Author"] =  htmlspecialchars($TmpAuthor, ENT_QUOTES);
					    $TmpArr["AuthorID"] =  $TmpAuthorID;
					    $TmpArr["Publisher"] =  $TmpPublisher;
					    $TmpArr["Preface"] =  htmlspecialchars($TmpPreface, ENT_QUOTES);
					    $TmpArr["Document"] =  htmlspecialchars($TmpDocument, ENT_QUOTES);
					    $TmpArr["InputBy"] =  $Type;
						$TmpArr["Source"]  = $ParArr["Source"];
						$TmpArr["Language"]  = $ParArr["Language"];
						$TmpArr["Category"]  = $TmpCategory;
						$TmpArr["SubCategory"] = $TmpSubCategory;
						$TmpArr["ISBN"] = $TmpISBN;
						$TmpArr["AdultContent"] = $TmpAdultContent;
						$TmpArr["Level"] = $TmpLevel;
						
 						//echo "<pre>TmpArr is";
						//var_dump($TmpArr);
						//echo "</pre>";
						//echo "</pre>TmpDocument is";
						//var_dump($TmpDocument);
						//echo "</pre>";
						//debug_r($TmpArr);
						
					    if ($TmpDocument != "")
					    {
						   // debug_r($TmpArr);
							$this->UPDATE_BOOK_INFO($TmpArr);	    
						}
					}
				    $row++;
				}
				fclose($handle);
			}
		}
		//die();	
	} // end function
	
	function UPDATE_BOOK_PUBLISH($ParArr)
	{
		$BookIDArr = $ParArr["BookID"];
		
		$BookIDField .= "BookID = '".$BookIDArr[0]."'";
		
		for($i = 1; $i < count($BookIDArr); $i++)
		{	
			$BookIDField .= " OR BookID = '".$BookIDArr[$i]."'";
		}
		
		$sql = "
		UPDATE
		INTRANET_ELIB_BOOK
		SET	
		Publish='".$ParArr["Publish"]."'
		WHERE
		$BookIDField
		";

		$this->db_db_query($sql);	
	} // end funciton UPDATE BOOK PUBLISH
	
	function UPDATE_BOOK_INFO($ParArr)
	{
		$Sql1 =  "
						SELECT
							BookID
						FROM
							INTRANET_ELIB_BOOK	 
						WHERE
							Document = '".$ParArr["Document"]."'
							AND InputBy = '".$ParArr["InputBy"]."'
					";
		$ReturnArr = $this->returnVector($Sql1);
		
		if (count($ReturnArr)>0)			
		{
		//Edit currrent record
			$BookID = $ReturnArr[0];
			if (($ParArr["Title"] != "") && ($ParArr["Document"]!=""))
			{
				$Sql1 =  "
								UPDATE
									INTRANET_ELIB_BOOK
								SET	
									Title='".$ParArr["Title"]."',
									Author='".$ParArr["Author"]."',
									AuthorID='".$ParArr["AuthorID"]."',
									Publisher='".$ParArr["Publisher"]."',
									Preface='".$ParArr["Preface"]."',
									DateModified=now(),
									Source='".$ParArr["Source"]."',
									Language='".$ParArr["Language"]."',
									Level='".$ParArr["Level"]."',
									Category='".$ParArr["Category"]."'
								WHERE
									BookID='{$BookID}'
							";
				$this->db_db_query($Sql1);	
			}
		}
		else
		{
		//Insert new record
			if (($ParArr["Title"] != "") && ($ParArr["Document"]!=""))
			{
				$Sql1 =  "
								INSERT INTO
									INTRANET_ELIB_BOOK
									(Title,Author,AuthorID,
									Publisher,Preface,Document,
									InputBy,DateModified,Source,Language,Level,
									Category, SubCategory, ISBN, 
									AdultContent)
								VALUES	
									('".$ParArr["Title"]."','".$ParArr["Author"]."','".$ParArr["AuthorID"]."',
									 '".$ParArr["Publisher"]."','".$ParArr["Preface"]."','".$ParArr["Document"]."',
									 '".$ParArr["InputBy"]."',now(),'".$ParArr["Source"]."','".$ParArr["Language"]."','".$ParArr["Level"]."',
									 '".$ParArr["Category"]."', '".$ParArr["SubCategory"]."', '".$ParArr["ISBN"]."',
									 '".$ParArr["AdultContent"]."')	
							";
				
				$this->db_db_query($Sql1);
				$BookID = $this->db_insert_id();
			}
		}

		return $BookID;
	}
	
	
	/*
	*	edit_book_info
	*/
	function UPDATE_BOOK_INFO2($ParArr)	
	{
		$BookID = $ParArr["BookID"];
		$Type = $ParArr["Type"];
		if ($ParArr["Title"] != "")
		{
			$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($ParArr["Author"],$description,$Type);
			if ($TmpAuthorID != $ParArr["AuthorID"])
			{
				$AuthorSQL = " Author='".$ParArr["Author"]."', AuthorID='".$TmpAuthorID."', ";
			}
			
			$Sql1 =  "
							UPDATE
								INTRANET_ELIB_BOOK
							SET	
								Title='".$ParArr["Title"]."',
								$AuthorSQL
								SeriesEditor='".$ParArr["SeriesEditor"]."',
								Publisher='".$ParArr["Publisher"]."',
								Preface='".$ParArr["Preface"]."',
								DateModified=now(),
								Source='".$ParArr["Source"]."',
								Language='".$ParArr["Language"]."',
								Level='".$ParArr["Level"]."',
								SubCategory='".$ParArr["SubCategory"]."',
								Category='".$ParArr["Category"]."',
								Publish='".$ParArr["Publish"]."',
								Copyright='".$ParArr["Copyright"]."',
								AdultContent='".$ParArr["AdultContent"]."'
							WHERE
								BookID='{$BookID}'
						";
						
			$this->db_db_query($Sql1);	
		}

		return $BookID;
	}
	
	
	
	function UPDATE_BOOK_AUTHOR($Author,$Description="",$Type)
	{
		$Sql1 =  "
						SELECT
							AuthorID
						FROM
							INTRANET_ELIB_BOOK_AUTHOR
						WHERE
							Author = '".trim($Author)."'
							AND InputBy='{$Type}'	
					";
		$ReturnArr = $this->returnVector($Sql1);
		
		if (count($ReturnArr)>0)			
		{
		//Edit currrent record
			$AuthorID = $ReturnArr[0];
			if (trim($Description) != "")
			{
				$Sql1 =  "
								UPDATE
									INTRANET_ELIB_BOOK_AUTHOR
								SET	
									Description='".trim($Description)."',
									DateModified=now()
								WHERE
									AuthorID='{$AuthorID}'
							";
				$this->db_db_query($Sql1);	
			}
		}
		else
		{
		//Insert new record
			$Sql1 =  "
							INSERT INTO
								INTRANET_ELIB_BOOK_AUTHOR
								(Author,Description,InputBy,DateModified)
							VALUES	
								('".trim($Author)."','".trim($Description)."','{$Type}',now())	
						";		
			$this->db_db_query($Sql1);
			$AuthorID = $this->db_insert_id();
		}
		
		return $AuthorID;
	}
		
	
	function UPDATE_BOOK_AUTHORID($BookID,$AuthorID)
	{
		$Sql1 =  "
						UPDATE
							INTRANET_ELIB_BOOK_AUTHOR
						SET	
							AuthorID='{$AuthorID}'
						WHERE
							BookID='{$AuthorID}'
					";
		$this->db_db_query($Sql1);	
		
		return $AuthorID;
	}
	
	
	function GET_BOOK_OVERVIEW_SQL($ParArr="")
	{
		global $eLib;
		global $i_QB_LangSelect,$i_QB_LangSelectChinese,$i_QB_LangSelectEnglish;
		
		// Field SQL
		$SourceField = "CASE Source ";
		if (is_array($eLib['Source']) && count($eLib['Source'])>0)
		{
		foreach($eLib['Source'] as $Key => $Value)	
		{
			
			$SourceField .= " WHEN {$Key} THEN '{$Value} ' ";
		}
		}
		$SourceField .= " ELSE '' END ";

		$SourceField = "IF (Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."')";
		$LanguageField = "IF (Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."')";
	
		//Conditions
		$Cond = " WHERE 1 ";
		if ($ParArr["AuthorID"] != "")	
		{
			$Cond .= " AND AuthorID='".$ParArr["AuthorID"]."' ";
		}
		if ($ParArr["Language"] != "")	
		{
			$Cond .= " AND Language='".$ParArr["Language"]."' ";
		}
		if ($ParArr["Source"] != "")	
		{
			$Cond .= " AND Source='".$ParArr["Source"]."' ";
		}		
		
		//dev
		//$Cond .= " AND IsTLF = 1 ";

		if ($ParArr["Link1"] != "")
		{
			$TitleField = "CONCAT('<a href=\"".$ParArr["Link1"]."?BookID=',BookID,' \" class=\"tablelink\">',Title,'</a>'),";
		}
		else
		{
			$TitleField = "Title,";
		}
		$AuthorField = "CONCAT('<a href=\"edit_book_author.php?BookID=',BookID,'&AuthorID=',AuthorID,' \" class=\"tablelink\">',Author,'</a>'),";
		
		//$PublishField = "IF (Publish='1','".$eLib["html"]["publish"]."', '".$eLib["html"]["unpublish"]."'),";
		$PublishField = "IF (Publish='1','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."'),";
		
		$Sql = "
					SELECT
						SUBSTRING((BookID+100000000), 2), {$TitleField}{$AuthorField}Publisher,{$SourceField},{$LanguageField}, DateModified,{$PublishField} CONCAT('<input type=\"checkbox\" name=\"BookID[]\" value=\"',BookID,'\" />')
					FROM
						INTRANET_ELIB_BOOK
					$Cond	
				  ";
		return  $Sql;
	}
		
	function GET_ALL_AUTHOR_ARRAY()
	{		
		/*
		$Sql = "
					SELECT
						DISTINCT AuthorID, Author
					FROM
						INTRANET_ELIB_BOOK
					WHERE
						IsTLF = 1	
				
				  ";
		*/
		$Sql = "
			SELECT
				DISTINCT AuthorID, Author
			FROM
				INTRANET_ELIB_BOOK
		  ";		  
		$ReturnArr = $this->returnArray($Sql,2);
		
		$ReturnArr2 = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			//$ReturnArr2[] = array($ReturnArr[$i][0],iconv("UTF-8", "big5"."//IGNORE", $ReturnArr[$i][1]));	
			$ReturnArr2[] = array($ReturnArr[$i][0],$ReturnArr[$i][1]);	
		}
		return $ReturnArr2;
	}

	function GET_ALL_SOURCE_ARRAY()
	{		
		global $eLib;
		
		/*
		$Sql = "
					SELECT
						DISTINCT Source
					FROM
						INTRANET_ELIB_BOOK
					WHERE
						IsTLF = 1
				  ";
		*/
		$Sql = "
			SELECT
				DISTINCT Source
			FROM
				INTRANET_ELIB_BOOK
		  ";			  
		$ReturnArr = $this->returnVector($Sql);		  
		
		$ReturnArr2 = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$ReturnArr2[] = array($ReturnArr[$i],$eLib['Source'][$ReturnArr[$i]]);			
		}
		
		return $ReturnArr2;
	}
		
	function GET_BOOK_DETAILS($BookID)
	{
		global $eLib;
		
		/*
		$Sql = "
					SELECT
						a.Title, a.Author, a.AuthorID, a.Publisher, a.Preface, a.Document, a.InputBy, a.DateModified, a.Source, a.Language, a.AdultContent,
						count(DISTINCT b.PageID) AS TotalPage, a.Category, a.Level, a.SeriesEditor, a.SubCategory, a.Publish, a.Copyright
					FROM
						INTRANET_ELIB_BOOK a
							LEFT JOIN
						INTRANET_ELIB_BOOK_PAGE b
							ON a.BookID = b.BookID
					WHERE
						a.BookID='{$BookID}'
						AND a.IsTLF = 1
					GROUP BY	
						 a.BookID			
				  ";
		*/
		$Sql = "
			SELECT
				a.Title, a.Author, a.AuthorID, a.Publisher, a.Preface, a.Document, a.InputBy, a.DateModified, a.Source, a.Language, a.AdultContent,
				count(DISTINCT b.PageID) AS TotalPage, a.Category, a.Level, a.SeriesEditor, a.SubCategory, a.Publish, a.Copyright
			FROM
				INTRANET_ELIB_BOOK a
					LEFT JOIN
				INTRANET_ELIB_BOOK_PAGE b
					ON a.BookID = b.BookID
			WHERE
				a.BookID='{$BookID}'
			GROUP BY	
				 a.BookID			
		  ";			  
		$ReturnArr = $this->returnArray($Sql,17);
		
		return $ReturnArr[0];
	}	
	

	function GET_IMAGE_ARRAY($ImagePath="")
	{
		$TmpArray = array();
		if ($handle = opendir($ImagePath)) 
		{
		    while (false !== ($file = readdir($handle))) 
		    {
				if ($file != "." && $file != "..") 
		        {			        
			        list($filename,$extension) = explode(".",$file);
			        list($pid,$pcnt) = explode("_",$filename);
			        
			        if ($TmpArray[$pid]=="")
			        {
				        $TmpArray[$pid] = 1;
			        }
			        else
			        {
				        $TmpArray[$pid]++;
			        }
			        
		            //echo "$file\n";
		            
		        }
		    }
		    closedir($handle);
		}
		
		return $TmpArray;
	}
	
	function IS_SCHOOL_ADMIN()
	{
		return true;
	}

	function IS_SYSTEM_ADMIN()
	{
		return true;
	}

	function GET_AUTHOR_DETAILS($AuthorID="")
	{		
		if ($AuthorID != "")
		{
			$Cond = " WHERE AuthorID = '{$AuthorID}' ";
		}
		$Sql = "
					SELECT
						AuthorID, Author, Description, InputBy
					FROM
						INTRANET_ELIB_BOOK_AUTHOR
					$Cond	
				  ";		
		$ReturnArr = $this->returnArray($Sql,3);		  
				
		return $ReturnArr;
	}

	
	function GET_BOOK_PAGE_TYPE_ARRAY($BookID)
	{
		$Sql = "
					SELECT
						PageType, count(BookPageID) As PageTotal
					FROM
						INTRANET_ELIB_BOOK_PAGE
					WHERE	
						BookID = '{$BookID}' 	
					GROUP BY
						PageType	
				  ";		
		$ReturnArr = $this->returnArray($Sql,2);		 
		$ReturnArr2 = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$ReturnArr2[$ReturnArr[$i]["PageType"]] = $ReturnArr[$i]["PageTotal"];
		}
		
		return $ReturnArr2;
	}
	
	function REMOVE_BOOK_INFO($BookIDArr="")
	{
				
		if(count($BookIDArr) > 0)
		{
		$con = "";
		
		for($i = 0; $i < count($BookIDArr); $i++)
		{
			if($i < count($BookIDArr) - 1 )
			$con .= "BookID = ".$BookIDArr[$i]." OR ";
			else
			$con .= "BookID = ".$BookIDArr[$i];
		}
	
		$sql = "
				DELETE FROM
				INTRANET_ELIB_BOOK
				WHERE
            	$con
			";

		//debug_r($sql);
		$this->db_db_query($sql);
		}
	} // end function remove book info
	
	function ADD_BOOK_INFO($ParArr="")
	{
		$Type = $ParArr['InputBy'];
		$TmpAuthorDesc = "";
		$TmpAuthor = $ParArr['Author'];
		
		$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);
		
		$sql = "
				INSERT INTO
				INTRANET_ELIB_BOOK
				(Title, Author, AuthorID, SeriesEditor, Category, SubCategory, Level, AdultContent, Publisher, Source, Language, Preface, Copyright, InputBy, Publish, DateModified)
				VALUES
  				(
                  '".$ParArr['Title']."',
                  '".$ParArr['Author']."',
                  '".$TmpAuthorID."',
                  '".$ParArr['SeriesEditor']."',
                  '".$ParArr['Category']."',
                  '".$ParArr['SubCategory']."',
				  '".$ParArr['Level']."',
				  '".$ParArr['AdultContent']."',
                  '".$ParArr['Publisher']."',
                  '".$ParArr['Source']."',
                  '".$ParArr['Language']."',
                  '".$ParArr['Preface']."',
				  '".$ParArr['Copyright']."',
                  '".$ParArr['InputBy']."',
                  '".$ParArr['Publish']."',
                  NOW())
				 ";
	
	$this->db_db_query($sql);
	} // end function add book info
	
	function ADD_BOOK_INFO_TLF($ParArr="")
	{
		$Type = $ParArr['InputBy'];
		$TmpAuthorDesc = "";
		$TmpAuthor = $ParArr['Author'];
		
		$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);
		
		$sql = "
				INSERT INTO
				INTRANET_ELIB_BOOK
				(Title, Author, AuthorID, SeriesEditor, Category, SubCategory, Level, AdultContent, Publisher, Source, Language, Preface, Copyright, InputBy, Publish, IsTLF, DateModified)
				VALUES
  				(
                  '".$ParArr['Title']."',
                  '".$ParArr['Author']."',
                  '".$TmpAuthorID."',
                  '".$ParArr['SeriesEditor']."',
                  '".$ParArr['Category']."',
                  '".$ParArr['SubCategory']."',
				  '".$ParArr['Level']."',
				  '".$ParArr['AdultContent']."',
                  '".$ParArr['Publisher']."',
                  '".$ParArr['Source']."',
                  '".$ParArr['Language']."',
                  '".$ParArr['Preface']."',
				  '".$ParArr['Copyright']."',
                  '".$ParArr['InputBy']."',
                  '".$ParArr['Publish']."',
			      '1',
                  NOW())
				 ";
	
	$this->db_db_query($sql);
	} // end function add book info with TLF content

	function ADD_BOOK_CHAPTER($BookID, $ChapterInfo)
	{
		$Sql =  "
  						INSERT INTO
  							INTRANET_ELIB_BOOK_CHAPTER
  							 (BookID,
                  ChapterID,
                  SubChapterID,
                  Title,
                  OrigPageID,
                  IsChapter,
                  DateModified)
  						VALUES
  						  ('".$BookID."',
                  '".$ChapterInfo['ChapterID']."',
                  '".$ChapterInfo['SubChapterID']."',
                  '".$ChapterInfo['Title']."',
                  '".$ChapterInfo['OrigPageID']."',
                  '".$ChapterInfo['IsChapter']."',
                  NOW())
					   ";
    $this->db_db_query($Sql);
	}
	
	function DELETE_BOOK_CHAPTER($BookID, $BookChapterID = "")
	{
    $Sql = "
						DELETE FROM
							INTRANET_ELIB_BOOK_CHAPTER
            WHERE
              BookID = '".$BookID."'
				   ";
		if($BookChapterID != "")
		  $Sql .= "
                AND
                  BookChapterID = '".$BookChapterID."'
              ";

		$this->db_db_query($Sql);
  }
  
	function IS_CHAPTER_START($BookID, $PageID)
	{
		$Sql = "
					SELECT
						IsChapterStart
					FROM
						INTRANET_ELIB_BOOK_PAGE
					WHERE	
						BookID = '".$BookID."' AND
						PageID = '".$PageID."'
				  ";		
		$ReturnVal = $this->returnVector($Sql);		 
		
		return $ReturnVal[0];
	}
	
	function GET_CHAPTER_INDEXES($BookID)
	{
		$Sql = "
					SELECT DISTINCT
						ChapterID, Title
					FROM
						INTRANET_ELIB_BOOK_CHAPTER
					WHERE	
						BookID = '".$BookID."'
				  ";		
		$ReturnArray = $this->returnArray($Sql);
		
		return $ReturnArray;
	}
	
	function TEST_UPDATE_CAT()
	{
		$Sql = "
					SELECT
						Category
					FROM
						INTRANET_ELIB_BOOK
					WHERE	
						BookID = '2'
				  ";		
		$ReturnArr = $this->returnVector($Sql);		 
		$TmpCat = $ReturnArr[0];
		
		$Sql = "
					UPDATE
						INTRANET_ELIB_BOOK
					SET
						Category = '".$TmpCat."'	
					WHERE	
						(BookID != '2') AND (BookID != '383')
				  ";		
		$this->db_db_query($Sql);		  
	}
	
	function printLeftMenu() {
		global $PATH_WRT_ROOT;
		
		$x  = "<div id=\"eLib_logo\" style=\"position: absolute; z-index: 2; left: 0px; top: -18px;\">";
		$x .= "<table width=\"120\" height=\"110\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
		$x .= "<tbody><tr>";
		
		$x .= "<td valign=\"middle\" background=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/logo_bg_01.gif\" align=\"center\">";
		$x .= "<img width=\"110\" height=\"100\" src=\"".$PATH_WRT_ROOT."images/2009a/leftmenu/icon_eLibrary.gif\"/>";
		$x .= "</td>";
		
		$x .= "<td valign=\"bottom\">";
		$x .= "<img width=\"33\" height=\"110\" src=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/logo_bg_02.gif\"/>";
		$x .= "</td>";
		
		$x .= "</tr>";
		$x .= "</tbody></table>";
		$x .= "</div>";
               		
		return $x;
	}
	
	function GET_MOST_ACTIVE_REVIEWERS($num=10)
	{			  
		/*
		$sql = "SELECT 
			   a.UserID, count(a.UserID) countNum, b.ChineseName, b.FirstName, b.LastName, b.ClassName, b.ClassNumber, b.EnglishName
			   FROM 
			   INTRANET_ELIB_BOOK_REVIEW a, INTRANET_USER b, INTRANET_ELIB_BOOK c
			   WHERE 
			   a.UserID = b.UserID
			   AND a.BookID = c.BookID
			   AND c.Publish = 1
			   AND c.IsTLF = 1
			   GROUP BY 
			   UserID 
			   ORDER BY 
			   countNum desc
			   Limit $num";
			   */
		$sql = "SELECT 
			   a.UserID, count(a.UserID) countNum, b.ChineseName, b.FirstName, b.LastName, b.ClassName, b.ClassNumber, b.EnglishName
			   FROM 
			   INTRANET_ELIB_BOOK_REVIEW a, INTRANET_USER b, INTRANET_ELIB_BOOK c
			   WHERE 
			   a.UserID = b.UserID
			   AND a.BookID = c.BookID
			   AND c.Publish = 1
			   GROUP BY 
			   UserID 
			   ORDER BY 
			   countNum desc
			   Limit $num";
				
		$ReturnArray = $this->returnArray($sql);
		
		return $ReturnArray;
	} // end function get most active reviewers
	
	function printMostActiveReviewers($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;
		
		$lang = $_SESSION["intranet_session_language"];
		
		$x = "<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">";
		
		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			//$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];
			
			if($ClassName != "")
			$ClassName = "(".$ClassName.")";
			
			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;
					
				$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
				$lang1 = $UserName_b5;
			
				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}
			
			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;
			
			if(trim($UserName) == "")
			$UserName = "--";
			
			$rank = $i + 1;
			
			if($rank <= 5)
			$rankBG = "top_no_".$rank."a.gif";
			else
			$rankBG = "top_no_b.gif";
			
		$x .= "<tr>";
		$x .= "<td width=\"35\">";
		$x .= "<table width=\"31\" height=\"30\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/".$rankBG."\" class=\"tabletopnolink\">".$rank."</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "<td>";
		$x .= "<table border=\"0\" cellspacing=\"4\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td valign=\"top\">&nbsp;<a href=\"#\" onClick=\"MM_openBrWindowFull('elib_top_reviewers.php?ReviewerID=$currUserID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\" class=\"eLibrary_top10_link1\">".$UserName." ".$ClassName."</a></td>";
		$x .= "<td valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td width=\"2\" height=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_01.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "<td height=\"2\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_02.gif\" width=\"20\" height=\"2\"></td>";
		$x .= "<td width=\"2\" height=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_03.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"2\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_04.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"eLibrary_top_review_no\">".$Count."</td>";
		$x .= "<td background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_06.gif\" width=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_06.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"2\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_07.gif\" width=\"2\" height=\"5\"></td>";
		$x .= "<td height=\"5\" align=\"left\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_08b.gif\" width=\"7\" height=\"5\"></td>";
		$x .= "<td width=\"2\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_09.gif\" width=\"2\" height=\"5\"></td>";
		$x .= "</tr>";
		$x .= "	</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		}
		$x .= "</table>";
		return $x;
	} // end function print Most active reviewers
	
	function GET_MOST_USEFUL_REVIEWS($num=10)
	{			  
		/*
		$sql = "SELECT 
			   a.BookID, c.Title, a.ReviewID, d.UserID,
			   count(a.ReviewID) countNum, 
			   b.ChineseName, b.EnglishName, b.FirstName, b.LastName, b.ClassName, b.ClassNumber
			   FROM 
				   INTRANET_ELIB_BOOK_REVIEW_HELPFUL a
			   LEFT OUTER JOIN
				   INTRANET_ELIB_BOOK_REVIEW d
			   ON
				   a.ReviewID = d.ReviewID
			   INNER JOIN
				   INTRANET_USER b
			   ON
				   d.UserID = b.UserID
			   INNER JOIN
				   INTRANET_ELIB_BOOK c
			   ON
				   c.BookID = a.BookID
			   WHERE
			   a.Choose = 1
			   AND c.Publish = 1
			   AND c.IsTLF = 1
			   GROUP BY 
			   a.ReviewID
			   ORDER BY 
			   countNum desc
			   Limit $num";
			   */
		$sql = "SELECT 
			   a.BookID, c.Title, a.ReviewID, d.UserID,
			   count(a.ReviewID) countNum, 
			   b.ChineseName, b.EnglishName, b.FirstName, b.LastName, b.ClassName, b.ClassNumber
			   FROM 
				   INTRANET_ELIB_BOOK_REVIEW_HELPFUL a
			   LEFT OUTER JOIN
				   INTRANET_ELIB_BOOK_REVIEW d
			   ON
				   a.ReviewID = d.ReviewID
			   INNER JOIN
				   INTRANET_USER b
			   ON
				   d.UserID = b.UserID
			   INNER JOIN
				   INTRANET_ELIB_BOOK c
			   ON
				   c.BookID = a.BookID
			   WHERE
			   a.Choose = 1
			   AND c.Publish = 1
			   GROUP BY 
			   a.ReviewID
			   ORDER BY 
			   countNum desc
			   Limit $num";

			   //debug_r($sql);
				
		$ReturnArray = $this->returnArray($sql);
	
		return $ReturnArray;
	} // end function get most useful review
	
	function printMostUsefulReviews($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;
		
		//hdebug_r($data);
		
		$lang = $_SESSION["intranet_session_language"];
		
		$x = "<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">";
		
		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$ReviewID = $data[$i]["ReviewID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en2 = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];
			//$BookName = iconv("UTF-8","BIG-5",$data[$i]["Title"]);
			$BookName = $data[$i]["Title"];
			
			if($ClassName != "")
			{
				if($ClassNumber != "")
				$ClassName = "(".$ClassName."-".$ClassNumber.")";
				else
				$ClassName = "(".$ClassName.")";
			}
			
			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;
					
			$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
			$lang1 = $UserName_b5;
			
				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}
			
			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;
			
			if(trim($UserName) == "")
			$UserName = "--";
			
			$rank = $i + 1;
			
			if($rank <= 5)
			$rankBG = "top_no_".$rank."b.gif";
			else
			$rankBG = "top_no_b.gif";

			$x .= "<tr>";
			$x .= "<td width=\"35\">";
			$x .= "<table width=\"31\" height=\"30\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$x .= "<tr>";
			$x .= "<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/".$rankBG."\" class=\"tabletopnolink\">".$rank."</td>";
			$x .= "</tr>";
			$x .= "</table>";
			$x .= "</td>";
			$x .= "<td>&nbsp;<a href=\"#\" onClick=\"MM_openBrWindowFull('elib_top_reviews.php?ReviewID=$ReviewID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\" class=\"eLibrary_top10_link2\">".$BookName."</a></td>";
			$x .= "<td class=\"eLibrary_top10_link2_stu_name\">".$UserName."  ".$ClassName."</td>";
			$x .= "</tr>";
			
		}
		$x .= "</table>";
		return $x;
	} // end function print Most active reviewers
	
	function GET_RECOMMEND_BOOK($numLimit=1)
	{		
		//AND a.RecommendGroup LIKE '%d.ClassLevelID%'
		//AND b.ClassLevel = d.LevelName
		//AND b.UserID = '{$currUserID}'
		//INTRANET_ELIB_BOOK_RECOMMEND a, INTRANET_USER b, INTRANET_ELIB_BOOK c, INTRANET_CLASS d
			  
		$currUserID = $_SESSION["UserID"];
		
		if($this->IS_TEACHER() || $this->IS_ADMIN())
		$con = "";
		else
		$con = "AND b.ClassName = d.ClassTitleEN";
		
		/*
		$sql = "
				SELECT DISTINCT
				a.BookID, c.Title, a.Description, d.ClassTitleEN, a.RecommendGroup, d.YearID
				FROM 
				INTRANET_ELIB_BOOK_RECOMMEND a, INTRANET_ELIB_BOOK c, INTRANET_USER b, YEAR_CLASS d
				WHERE 
				a.BookID = c.BookID
				$con
				AND b.UserID = '{$currUserID}'
				AND 
				(
				a.RecommendGroup LIKE CONCAT('%|',d.YearID,'|%') 
				OR 
				a.RecommendGroup LIKE CONCAT('%|',-1,'|%')
				)
				AND c.Publish = 1
				AND c.IsTLF = 1
				GROUP BY
				a.BookID
				ORDER BY 
				a.RecommendOrder ASC, a.DateModified DESC
				Limit $numLimit
				";
				*/
		$sql = "
				SELECT DISTINCT
				a.BookID, c.Title, a.Description, d.ClassTitleEN, a.RecommendGroup, d.YearID
				FROM 
				INTRANET_ELIB_BOOK_RECOMMEND a, INTRANET_ELIB_BOOK c, INTRANET_USER b, YEAR_CLASS d
				WHERE 
				a.BookID = c.BookID
				$con
				AND b.UserID = '{$currUserID}'
				AND 
				(
				a.RecommendGroup LIKE CONCAT('%|',d.YearID,'|%') 
				OR 
				a.RecommendGroup LIKE CONCAT('%|',-1,'|%')
				)
				AND c.Publish = 1
				GROUP BY
				a.BookID
				ORDER BY 
				a.RecommendOrder ASC, a.DateModified DESC
				Limit $numLimit
				";
		//debug_r($sql);
			   
		$ReturnArray = $this->returnArray($sql);
		
		return $ReturnArray;
	} // end function get recommend book
	
	function GET_BOOK_CATEGORY($ParArr="")
	{
		$Language = $ParArr["Language"];
		
		if($Language == "eng" || $Language == "chi")
		$con = "AND Language='".$Language."'";
		else
		$con = "";
		
		/*
		$sql = "
				SELECT DISTINCT
				Category
				FROM
				INTRANET_ELIB_BOOK
				WHERE 1
				AND Publish = 1
				AND IsTLF = 1
				$con
				";
				*/
		$sql = "
				SELECT DISTINCT
				Category
				FROM
				INTRANET_ELIB_BOOK
				WHERE 1
				AND Publish = 1
				$con
				";
				
		$ReturnArray = $this->returnArray($sql);
		return $ReturnArray;
	} // end function get book category
	
	function getBookLanguage($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		
		$sql = "
		SELECT 
		Language
		FROM
		INTRANET_ELIB_BOOK
		WHERE 
		BookID = {$BookID}
		";
		
		$ReturnArray = $this->returnArray($sql);
		return $ReturnArray;
	} // end function get book language
	
	function ADD_READING_HISTORY_RECORD($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["UserID"];
		
		$sql = "
		INSERT INTO
		INTRANET_ELIB_BOOK_HISTORY
		(UserID,BookID,DateModified)
		VALUES	
			('".$currUserID."','".$BookID."', now())
		";
		
		$this->db_db_query($sql);
	} // end function add reading history record
	
	function ADD_BOOK_HIT_RATE($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		/*
		$sql = "
		SELECT
		HitRate
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		BookID = {$BookID}
		";
		
		$ReturnArray = $this->returnArray($sql);
	
		$HitRate = $ReturnArray[0]["HitRate"];
		
		if($HitRate == "" || $HitRate == NULL)
			$HitRate = 0;
		
		$HitRate += 1;
		
		$sql = "
				UPDATE 
				INTRANET_ELIB_BOOK
				SET
				HitRate = {$HitRate}
				WHERE
				BookID = {$BookID}
			  ";
		*/
		
		$sql = "
				UPDATE 
					INTRANET_ELIB_BOOK
				SET
					HitRate = IF(HitRate IS NULL, 1, HitRate+1)
				WHERE
					BookID = {$BookID}
			  ";
		
	    $this->db_db_query($sql);
	} // end function add book hit rate
	
	function GET_WEEKLY_HIT_BOOK($numLimit=1)
	{		
		$today = date("Y-m-d G:i:s"); 
		$lastweek = date('Y-m-d G:i:s', mktime(date("G"), date("i"), date("s"), date("m"),   date("d")-7,   date("Y"))); // 2009-04-04 12:00:00 

		$start_date = $lastweek;
		$end_date = $today;
		
		/*
		$sql = "SELECT 
			   a.BookID, c.Title, count(a.BookID) countNum
			   FROM 
			   INTRANET_ELIB_BOOK_HISTORY a, INTRANET_ELIB_BOOK c
			   WHERE
			   a.BookID = c.BookID
			   AND c.Publish = 1
			   AND c.IsTLF = 1
			   AND
			   (a.DateModified BETWEEN '$start_date' AND '$end_date')
			   GROUP BY
			   a.BookID
			   ORDER BY
			   countNum desc
			   Limit $numLimit";
			   */
		$sql = "SELECT 
			   a.BookID, c.Title, count(a.BookID) countNum
			   FROM 
			   INTRANET_ELIB_BOOK_HISTORY a, INTRANET_ELIB_BOOK c
			   WHERE
			   a.BookID = c.BookID
			   AND c.Publish = 1
			   AND
			   (a.DateModified BETWEEN '$start_date' AND '$end_date')
			   GROUP BY
			   a.BookID
			   ORDER BY
			   countNum desc
			   Limit $numLimit";
		$ReturnArray = $this->returnArray($sql);
		
		//if(count($ReturnArray) <= 0)
		//$ReturnArray = $this->GET_HIT_BOOK($numLimit=1);
		
		return $ReturnArray;
	} // end function get weekly hit book
	
	function GET_HIT_BOOK($numLimit=1)
	{	
		/*
		$sql = "SELECT 
			   BookID, Title
			   FROM 
			   INTRANET_ELIB_BOOK
			   WHERE
			   Publish = 1
			   AND IsTLF = 1
			   AND HitRate >= 1
			   ORDER BY 
			   HitRate desc
			   Limit $numLimit";
			   */
		$sql = "SELECT 
			   BookID, Title
			   FROM 
			   INTRANET_ELIB_BOOK
			   WHERE
			   Publish = 1
			   AND HitRate >= 1
			   ORDER BY 
			   HitRate desc
			   Limit $numLimit";			   
		$ReturnArray = $this->returnArray($sql);
		
		return $ReturnArray;
	} // end function get hit book
	
	function printBookTable($data="", $image_path="", $LAYOUT_SKIN="", $NumOfBook="", $type="", $eLib=""){
		global $PATH_WRT_ROOT;
		
		//debug_r($data);
		
		if($NumOfBook == 4)
		{
			$NumOfBook = 2;
			$width = 100;
			$height = 135;
		}
		else if($NumOfBook == 9)
		{
			$NumOfBook = 3;
			$width = 45;
			$height = 65;
		}
		else
		{
			$NumOfBook = 1;
			$width = 200;
			$height = 285;
		}
		
		$count = 0;
		
		// check if recommend table
		if(count($data) <= 0 && $type == "recommend")
		{
			$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr><td align=\"center\" class=\"tabletext\">";
			$x .= "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
			$x .= $eLib["html"]["no_recommend_book"];
			$x .= "</td></tr>";
			$x .= "</table>";
			
			return $x;
		}
		
		$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		for($i = 0; $i < $NumOfBook ; $i++)
		{
			$x .= "<tr>";
			for($j = 0; $j < $NumOfBook ; $j++)
			{
				//$BookName = iconv("UTF-8","BIG-5",$data[$count]["Title"]);
				$BookName = $data[$count]["Title"];
				$BookID = $data[$count]["BookID"];
				$Description =  $data[$count]["Description"];
				
				if($BookID != "")
				{
					$BookCover = "/file/elibrary/content/".$BookID."/image/cover.jpg";
				}
				else
				{
					$BookCover = $image_path."/".$LAYOUT_SKIN."/10x10.gif";
				}
				
				$count++;
					
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td>";
				if($type == "recommend")
				{
					if($BookID != "")
					$x .= "<div id=\"book_border\"><a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\" onMouseover=\"MM_showHideLayers('recommend_detail_$count','','show')\" onMouseout=\"MM_showHideLayers('recommend_detail_$count','','hide')\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
					else
					{
						//$x .= "<div id=\"book_border\"><a href=\"#\" onMouseover=\"MM_showHideLayers('recommend_detail_$count','','show')\" onMouseout=\"MM_showHideLayers('recommend_detail_$count','','hide')\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
						$x .= "<div><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></div>";
					}
				}
				else
				{
					if($BookID != "")
					$x .= "<div id=\"book_border\"><a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
					else
					{
						//$x .= "<div id=\"book_border\"><a href=\"#\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
						$x .= "<div><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></div>";
					}
				}
				$x .= "</td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td>";
				if($type == "recommend")
				{
					$x .= "<td align=\"left\" valign=\"top\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"20\"><br>";
					$x .= "<div id=\"recommend_detail_$count\" style=\"position:absolute; width: 120px; z-index:1; visibility: hidden;\"><table  border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"eLibrary_recomm_detail\">";
					$x .= "<tr>";
					$x .= "<td nowrap class=\"eLibrary_title_sub_heading\"> ".$eLib["html"]["recommended_reason"]."</td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td class=\"tabletext\">".$Description."</td>";
					$x .= "</tr></table></div></td>";
				}
				$x .= "</tr><tr><td>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$x .= "<tr>";
				$x .= "<td align=\"center\" class=\"tabletext\"><a href=\"#\" class=\"eLibrary_booktitle_normal\">".$BookName."</a></td>";
				$x .= "</tr></table></td></tr></table></td>";
			} // end for cell
			$x .= "</tr>";
		} // end for row
		$x .= "</table>";
		
		
		return $x;
	} // end function print table
	
	function getSourceFullName($Source="")
	{
		if($Source == "cup")
		$Source = "Cambridge University Press";
		else if($Source == "green")
		$Source = "GREEN APPLE MEDIA LIMITED";
			
		return $Source;
	} // end function get source full name
	
	function GET_USER_BOOK_SETTING($ParArr="")
	{
		// admin acc
		$currUserID = 0;
		//$currUserID = $ParArr["UserID"];
		
		$Sql = "
				SELECT
					DisplayReviewer,DisplayReview,DisplayRecommendBook,DisplayWeeklyHitBook,DisplayHitBook
				FROM
					INTRANET_ELIB_BOOK_SETTINGS
				WHERE
					UserID = '{$currUserID}'
			  ";
			  
		$ReturnArr = $this->returnArray($Sql, 5);
		
		if(count($ReturnArr) <= 0)
		{
			$this->UPDATE_USER_BOOK_SETTING($ParArr);
			
		$Sql = "
				SELECT
					DisplayReviewer,DisplayReview,DisplayRecommendBook,DisplayWeeklyHitBook,DisplayHitBook
				FROM
					INTRANET_ELIB_BOOK_SETTINGS
				WHERE
					UserID = '{$currUserID}'
			  ";
			  
		$ReturnArr = $this->returnArray($Sql, 5);
		}
		
		return $ReturnArr;
	} // end function get user book setting
  	
	function UPDATE_USER_BOOK_SETTING($ParArr="")
	{
		// admin acc setting
		$currUserID = 0;
		//$currUserID = $ParArr["UserID"];
		
		$Sql = "
				SELECT
					UserID
				FROM
					INTRANET_ELIB_BOOK_SETTINGS
				WHERE
					UserID = '{$currUserID}'
			  ";
			  
		$ReturnArr = $this->returnArray($Sql, 1);
		
		if(count($ReturnArr) <= 0)
		{
			$ParArr["UserID"] = 0;
			$ParArr["DisplayReviewer"] = 10;
			$ParArr["DisplayReview"] = 10;
			$ParArr["RecommendBook"] = 4;
			$ParArr["WeeklyHitBook"] = 1;
			$ParArr["HitBook"] = 9;
			
			$Sql1 =  "
					INSERT INTO
							INTRANET_ELIB_BOOK_SETTINGS
							(UserID,DisplayReviewer,DisplayReview,DisplayRecommendBook,DisplayWeeklyHitBook,DisplayHitBook)
					VALUES	
						('".$ParArr["UserID"]."','".$ParArr["DisplayReviewer"]."','".$ParArr["DisplayReview"]."',
						'".$ParArr["RecommendBook"]."','".$ParArr["WeeklyHitBook"]."','".$ParArr["HitBook"]."')	
					";		
				$this->db_db_query($Sql1);
				//$returnID = $this->db_insert_id();
		}
		else
		{
			$Sql1 =  "
					UPDATE
						INTRANET_ELIB_BOOK_SETTINGS
					SET
						DisplayReviewer = '".$ParArr["DisplayReviewer"]."',
						DisplayReview = '".$ParArr["DisplayReview"]."',
						DisplayRecommendBook = '".$ParArr["RecommendBook"]."',
						DisplayWeeklyHitBook = '".$ParArr["WeeklyHitBook"]."',
						DisplayHitBook = '".$ParArr["HitBook"]."'
					WHERE
						UserID = '{$currUserID}'
					";
					
			$this->db_db_query($Sql1);
		}
	} // end function update user book setting
	
	function printSettingBookTable($tableName="", $image_path="", $LAYOUT_SKIN="", $NumOfBook=""){
		global $PATH_WRT_ROOT, $eLib;
		
		$BookName = $eLib["html"]["book_name"];
		$BookCover = $eLib["html"]["book_cover"];
		
		if($NumOfBook == 4)
		{
			$NumOfBook = 2;
			$width = 100;
			$height = 135;
		}
		else if($NumOfBook == 9)
		{
			$NumOfBook = 3;
			$width = 45;
			$height = 65;
			$BookName = $eLib["html"]["book_name"]."<br><br>";
		}
		else
		{
			$NumOfBook = 1;
			$width = 200;
			$height = 285;
		}
		
		$x = "<table name=\"$tableName\" id=\"$tableName\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		for($i = 0; $i < $NumOfBook ; $i++)
		{
			$x .= "<tr>";
			for($j = 0; $j < $NumOfBook ; $j++)
			{
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\">";
				$x .= "<tr>";
				$x .= "<td>";
				$x .= "<td width=\"".$width."\" height=\"".$height."\" align=\"center\" bgcolor=\"#C5E0FC\" class=\"tabletextremark\">".$BookCover."</td>";
				$x .= "</td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "</tr>";
				//$x .= "<tr><td>";
				//$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\">";
				//$x .= "<tr>";
				//$x .= "<td align=\"center\" class=\"tabletext\"><span class=\"eLibrary_booktitle_normal\">".$BookName."</span></td>";
				//$x .= "</tr>";
				//$x .= "</table>";
				//$x .= "</td></tr>";
				$x .= "</table>";
				$x .= "</td>";
			} // end for cell
			$x .= "</tr>";
		} // end for row
		$x .= "</table>";
		
		return $x;
		
	} // end function print setting book table
	
	function n_sizeof($arrData){

		$count = 0;
		if (is_array($arrData))
		{
			while (list($key, $value) = each($arrData))
			{
				if (is_int($key))
				{
					$count++;
				}
			}
		} else
		{
			$count = sizeof($arrData);
		}
		return $count;
	} // end function n_sizeof
	
	// convert PHP array to JS array (2D) -- copy from /home/eclass30/eclass30/src/includes/php/lib.php
	function ConvertToJSArray($arrData, $jArrName, $fromDB = 0){

		$ReturnStr = "<SCRIPT LANGUAGE=\"JavaScript\">\n";
		$ReturnStr .= "var ".$jArrName." = new Array(".sizeof($arrData).");\n";

		for ($i = 0; $i < sizeof($arrData); $i++)
		{
			$ReturnStr .= $jArrName."[".$i."] = new Array(".$this->n_sizeof($arrData[$i]).");\n";

			for ($j = 0; $j < $this->n_sizeof($arrData[$i]); $j++)
			{
				if (sizeof($arrData[$i]) > 1)
				{
					if ($fromDB)
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", $arrData[$i][$j])."\";\n";
					} else
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", htmlspecialchars($arrData[$i][$j]))."\";\n";
					}
				} else
				{
					if ($fromDB)
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", $arrData[$i])."\";\n";
					} else
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", htmlspecialchars($arrData[$i]))."\";\n";
					}
				}
			}
		}

		$ReturnStr .= "</SCRIPT>\n";

		return $ReturnStr;
	} // end function convert to js array
	
	function displayPresetListAuthorInput($title="Author")
	{
		global $linterface;
		/*
		$sql = "SELECT 
					Distinct Author 
				FROM 
					INTRANET_ELIB_BOOK
				WHERE
					IsTLF = 1
				ORDER BY 
					Author";
					*/
		$sql = "SELECT 
					Distinct Author 
				FROM 
					INTRANET_ELIB_BOOK
				ORDER BY 
					Author";
		$returnArr = $this->returnArray($sql,1);
		
		//for($i = 0; $i < count($returnArr); $i++)
		//{
		//	$returnArr[$i][0] = iconv("UTF-8","BIG-5",$returnArr[$i][0]);
		//}
		
		$x .= $this->ConvertToJSArray($returnArr, "arrA", 1);
		$x .= $linterface->GET_LIST("form1", "arrA", $title);
		
		return $x;
	} // end function display preset list input
	
	function displayPresetListSourceInput($title="Source")
	{
		global $linterface;
		/*
		$sql = "SELECT 
					Distinct Source 
				FROM 
					INTRANET_ELIB_BOOK 
				WHERE
					IsTLF = 1
				ORDER BY 
					Source";
				*/
		$sql = "SELECT 
					Distinct Source 
				FROM 
					INTRANET_ELIB_BOOK 
				ORDER BY 
					Source";
		$returnArr = $this->returnArray($sql,1);
		
		//for($i = 0; $i < count($returnArr); $i++)
		//{
		//	$returnArr[$i][0] = iconv("UTF-8","BIG-5",$returnArr[$i][0]);
		//}
		
		$x .= $this->ConvertToJSArray($returnArr, "arrS", 1);
		$x .= $linterface->GET_LIST("form1", "arrS", $title);
		
		return $x;
	} // end function display preset list input

	function displayCategorySelect($sel="selCategory", $eLib="")
	{
		/*
		$sql = "SELECT 
					Distinct Category 
				FROM 
					INTRANET_ELIB_BOOK 
				WHERE
					IsTLF = 1
				ORDER BY 
					Category";
					*/
		$sql = "SELECT 
					Distinct Category 
				FROM 
					INTRANET_ELIB_BOOK 
				ORDER BY 
					Category";
		$category = $this->returnArray($sql,1);
		
		$x = "<select name=".$sel.">";
		$x .= "<option>".$eLib["html"]["all_category"]."</option>";
		
		for($i = 0; $i < count($category); $i++)
		{
			//$x .= "<option>".iconv("UTF-8","BIG-5",$category[$i][0])."</option>";
			$x .= "<option>".$category[$i][0]."</option>";
		}
		
		$x .= "</select>";
		
		return $x;
	} // end function display Category select
	
	function displayLevelSelect($sel="selLevel", $eLib="")
	{
		/*
		$sql = "SELECT 
					Distinct Level 
				FROM 
					INTRANET_ELIB_BOOK 
				WHERE
					IsTLF = 1
				ORDER BY 
					Level
				";
				*/
		$sql = "SELECT 
					Distinct Level 
				FROM 
					INTRANET_ELIB_BOOK 
				ORDER BY 
					Level
				";
		$returnArr = $this->returnArray($sql,1);
		
		$x = "<select name=".$sel.">";
		$x .= "<option>".$eLib["html"]["all_level"]."</option>";
		
		for($i = 0; $i < count($returnArr); $i++)
		{
			if(trim($returnArr[$i][0]) != "")
			//$x .= "<option>".iconv("UTF-8","BIG-5",$returnArr[$i][0])."</option>";
			$x .= "<option>".$returnArr[$i][0]."</option>";
		}
		
		$x .= "</select>";
		
		return $x;
	} // end function display Category select
	
	/* **************************************************************************************************
	/* added by Adam for Book editing page
	/* **************************************************************************************************/
	function getBookDetailAll($ParArr="")
	{
		$BookTitle = $ParArr["BookTitle"];
		$Author = $ParArr["Author"];
		$Source = $ParArr["Source"];
		$Category = $ParArr["Category"];
		$Level = $ParArr["Level"];
		
		$startdate = $ParArr["startdate"];
		$enddate = $ParArr["enddate"];
		$checkWorksheets = $ParArr["checkWorksheets"];
		$sortField = $ParArr["sortField"];
		$sortOrder = $ParArr["sortFieldOrder"];
		
		//$DisplayNumPage = $ParArr["DisplayNumPage"];
		//$limit = "Limit {$DisplayNumPage}";
		
		$con1 = "";
		$con2 = "";
		$con3 = "";
		$con4 = "";
		$con5 = "";
		$con6 = "";
		$con7 = "";
		$con8 = "";
		
		if($BookTitle != "")
		{
			//$BookTitle = iconv("big-5", "utf-8", trim($BookTitle));
			$BookTitle = trim($BookTitle);
			$con1 = "AND Title like '%{$BookTitle}%'";
		}
		
		if($Author != "")
		{
			//$Author = iconv("big-5", "utf-8", trim($Author));
			$Author = trim($Author);
			$con2 = "AND Author like '%{$Author}%'";
		}
		
		if($Source != "")
		{
			//$Source = iconv("big-5", "utf-8", trim($Source));
			$Source = trim($Source);
			$con3 = "AND Source like '%{$Source}%'";
		}
		
		if($Category != $ParArr["all_category"])
		{
			//$Category = iconv("big-5", "utf-8", $Category);
			$con4 = "AND Category = '{$Category}'";
		}
		
		if($Level != $ParArr["all_level"])
		{
			//$Level = iconv("big-5", "utf-8", $Level);
			$con5 = "AND Level = '{$Level}'";
		}
		
		if($checkWorksheets != $ParArr["checkWorksheets"])
		{
			$con6 = "";
		}
		
		if($startdate != "" && $enddate != "")
		{
			$con7 = "AND DateModified BETWEEN '$startdate' AND '$enddate'";
		}
		
		$con8 = "ORDER BY {$sortField} {$sortOrder}";
		/*		
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Publish 
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		AND IsTLF = 1
		$con1
		$con2
		$con3
		$con4
		$con5
		$con6
		$con7
		$con8
		";
		*/
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Publish 
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		$con1
		$con2
		$con3
		$con4
		$con5
		$con6
		$con7
		$con8
		";
		$returnArr = $this->returnArray($sql, 8);
		return $returnArr;
	} // end function get book detail including Unpublished
	/* **************************************************************************************************/

	// advance search
	function getBookDetail($ParArr="")
	{
		$BookTitle = $ParArr["BookTitle"];
		$Author = $ParArr["Author"];
		$Source = $ParArr["Source"];
		$Category = $ParArr["Category"];
		$Level = $ParArr["Level"];
		
		$startdate = $ParArr["startdate"];
		$enddate = $ParArr["enddate"];
		$checkWorksheets = $ParArr["checkWorksheets"];
		$sortField = $ParArr["sortField"];
		$sortOrder = $ParArr["sortFieldOrder"];
		
		//$DisplayNumPage = $ParArr["DisplayNumPage"];
		//$limit = "Limit {$DisplayNumPage}";
		
		$con1 = "";
		$con2 = "";
		$con3 = "";
		$con4 = "";
		$con5 = "";
		$con6 = "";
		$con7 = "";
		$con8 = "";
		
		if($BookTitle != "")
		{
			//$BookTitle = iconv("big-5", "utf-8", trim($BookTitle));
			$BookTitle = trim($BookTitle);
			$con1 = "AND Title like '%{$BookTitle}%'";
		}
		
		if($Author != "")
		{
			//$Author = iconv("big-5", "utf-8", trim($Author));
			$Author = trim($Author);
			$con2 = "AND Author like '%{$Author}%'";
		}
		
		if($Source != "")
		{
			//$Source = iconv("big-5", "utf-8", trim($Source));
			$Source = trim($Source);
			$con3 = "AND Source like '%{$Source}%'";
		}
		
		if($Category != $ParArr["all_category"])
		{
			//$Category = iconv("big-5", "utf-8", $Category);
			$con4 = "AND Category = '{$Category}'";
		}
		
		if($Level != $ParArr["all_level"])
		{
			//$Level = iconv("big-5", "utf-8", $Level);
			$con5 = "AND Level = '{$Level}'";
		}
		
		if($checkWorksheets != $ParArr["checkWorksheets"])
		{
			$con6 = "";
		}
		
		if($startdate != "" && $enddate != "")
		{
			$con7 = "AND DateModified BETWEEN '$startdate' AND '$enddate'";
		}
		
		$con8 = "ORDER BY {$sortField} {$sortOrder}";
		
		/*
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Level
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		AND Publish = 1
		AND IsTLF = 1
		$con1
		$con2
		$con3
		$con4
		$con5
		$con6
		$con7
		$con8
		";
		*/
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Level
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		AND Publish = 1
		$con1
		$con2
		$con3
		$con4
		$con5
		$con6
		$con7
		$con8
		";		
		$returnArr = $this->returnArray($sql, 8);
		return $returnArr;
	} // end function get book detail
	
	function removeMyFavourite($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["UserID"];
		
		$sql = "DELETE FROM INTRANET_ELIB_BOOK_MY_FAVOURITES WHERE BookID = {$BookID} AND UserID = {$currUserID}";
		
		return $this->db_db_query($sql);
	} // end function remove my favourite
	
	function getBookNotesTitle_ByUserID($ParArr="")
	{
		$currUserID = $ParArr["currUserID"];
		
		/*
		$sql = "
		SELECT 
		DISTINCT (a.Title), a.BookID
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		AND a.IsTLF = 1
		";
		*/
		$sql = "
		SELECT 
		DISTINCT (a.Title), a.BookID
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		";		
		$returnArr = $this->returnArray($sql, 2);
		
		return $returnArr;
	} // end function get book notes title by userID
	
	function getBookNotesCategory_ByUserID($ParArr="")
	{
		$currUserID = $ParArr["UserID"];
		
		/*
		$sql = "
		SELECT 
		DISTINCT (b.Category)
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		AND a.IsTLF = 1
		";
		*/
		$sql = "
		SELECT 
		DISTINCT (b.Category)
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		";		
		$returnArr = $this->returnArray($sql, 1);
		
		return $returnArr;
	} // end function get book notes title by userID
	
	function addMyFavourite($ParArr="")
	{
		$id = "";
		
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["currUserID"];
		
		// check exist
		$sql = "
		SELECT 
			FavouriteID
		FROM
			INTRANET_ELIB_BOOK_MY_FAVOURITES
		WHERE
			BookID = {$BookID}
		AND
			UserID = {$currUserID}
		";
		
		$returnArr = $this->returnArray($sql, 1);
		
		$is_fave = $this->IS_MY_FAVOURITE_BOOK($ParArr);
		
		//if(count($returnArr) <= 0)
		if(!$is_fave)
		{
		$sql = "
		INSERT INTO INTRANET_ELIB_BOOK_MY_FAVOURITES
			(BookID, UserID, DateModified)
		VALUES
			('".$BookID."', '".$currUserID."', now())
		";
		
		$this->db_db_query($sql);
		$id = $this->db_insert_id();
		}
		
		return $id;
	} // end funciton add my favourite 
	
	function getCountSubCategory($ParArr="")
	{
		/*
	$sql = " 
		SELECT
		Language,
		SubCategory, 
		Count(SubCategory) as count_subcategory
		FROM 
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		AND IsTLF = 1
		GROUP BY 
		SubCategory
		";
		*/
	$sql = " 
		SELECT
		Language,
		SubCategory, 
		Count(SubCategory) as count_subcategory
		FROM 
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		GROUP BY 
		SubCategory
		";	
		$returnArr = $this->returnArray($sql, 4);
		return $returnArr;
	} // end function get Count SubCategory
	
	function getCountCategory($ParArr="")
	{
		/*
	$sql = " 
		SELECT
		Language,
		Category,
		Count(Category) as count_category
		FROM 
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		AND IsTLF = 1
		GROUP BY 
		Category
		";
		*/
	$sql = " 
		SELECT
		Language,
		Category,
		Count(Category) as count_category
		FROM 
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		GROUP BY 
		Category
		";		
		$returnArr = $this->returnArray($sql, 4);
		return $returnArr;
	} // end function get Count Category
	
	
	function getAllBookCatalogue()
	{
		/*
		$sql = "
		SELECT 
		BookID, Category, SubCategory, Language
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		AND IsTLF = 1
		";
		*/
		$sql = "
		SELECT 
		BookID, Category, SubCategory, Language
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		";
		
		$returnArr = $this->returnArray($sql, 4);
		return $returnArr;
	} // end function get all book catalogue
	
	function getBookInformation($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Level, DateModified, Preface, AdultContent, Publish
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		BookID = '".$BookID."'
		";
		
		//debug_r($sql);
		
		$returnArr = $this->returnArray($sql, 10);
		return $returnArr;
	} // end function get book informaiton by ID
	
	
	function getFieldName($selectSearchType)
	{
		$x = "";
		switch($selectSearchType)
		{
			case 0:
			$x = "Title";
			break;
			
			case 1:
			$x = "Author";
			break;
			
			case 2:
			$x = "Publisher";
			break;
			
			default:
			break;
		}
		return $x;
	} // end function get field name
	
	function getNormalSearch($ParArr="")
	{
    	$selectSearchType = $ParArr["selectSearchType"];
    	
    	$selectSearchType = $this->getFieldName($selectSearchType);
    	
		//$keywords = iconv("big-5","utf-8", $ParArr["keywords"]);
		$keywords = $ParArr["keywords"];

		$con1 = "";
		if($selectSearchType != "")
		{
			$con1 .= "AND $selectSearchType LIKE '%$keywords%'";
		}
		else
		{
			$con1 .= "AND (";
			$con1 .= "Title LIKE '%$keywords%'";
			$con1 .= " OR Author LIKE '%$keywords%'";
			$con1 .= " OR Publisher LIKE '%$keywords%'";
			$con1 .= ")";
		}
		
		if($BookTitle != "")
		{
			//$BookTitle = iconv("big-5", "utf-8", $BookTitle);
			$con1 = "AND Title = '{$BookTitle}'";
		}
		
		/*
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Level
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		AND Publish = 1
		AND IsTLF = 1
		$con1
		";
		*/
		$sql = "
		SELECT 
		BookID, Title, Author, Publisher, Category, SubCategory, Source, Level
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		AND Publish = 1
		$con1
		";		
		//debug_r($sql);
		
		$returnArr = $this->returnArray($sql, 8);
		return $returnArr;
	} // end function get normal search
	
	
	function getBookQueryList($ParArr="", $sql="")
	{
		$sortFieldArray = $ParArr["sortFieldArray"];
		$numField = $ParArr["sortFieldArray"];
		
		$returnArr = $this->returnArray($sql, $numField);
		
		//debug_r($returnArr);
		
		return $returnArr;
	} // end function get book query list
	
	function displayTableHeading($ParArr="", $eLib="")
	{
		
		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$CurrentPageNum = $ParArr["CurrentPageNum"];
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	$totolRecord = $ParArr["totalNum"];
    	
    	if($CurrentPageNum * $DisplayNumPage > $totolRecord)
		{
			$CurrentPageNum = ceil($totolRecord / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}
    	
    	$pageDisplay = $ParArr["pageDisplay"];
    	if($pageDisplay == "none")
    	return "";
    	 	
		//if($totolRecord > 0)
		//$ReportMsg = $eLib["html"]["record"]." 1 - ".$totolRecord.", ".$eLib["html"]["total"]." ".$totolRecord;
		//else
		//$ReportMsg = $eLib["html"]["record"]." 0, ".$eLib["html"]["total"]." 0";

		if($totolRecord > 0){
			$startRec = 1 + ($CurrentPageNum - 1) * $DisplayNumPage;
			//if($startRec > $totolRecord){
			//	$CurrentPageNum = 1;
			//	$startRec = 1;	
			//}
				
			$endRec = $startRec + $DisplayNumPage - 1;
			if($endRec > $totolRecord)
			$endRec = $totolRecord;
	
			//$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".($endRec - $startRec + 1);
			$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".$totolRecord;
		}else{
			$RecordMsg = $eLib["html"]["record"]." 0, ".$eLib["html"]["total"]." 0";
		}
		
		//echo("\n CurrentPageNum: ");
		//var_dump($CurrentPageNum);
		//echo("\n DisplayNumPage: ");
		//var_dump($DisplayNumPage);
		//echo("\n startRec: ");
		//var_dump($startRec);
		//echo("\n endRec: ");
		//var_dump($endRec);
		//echo("\n totolRecord: ");
		//var_dump($totolRecord);
		
		
		$xh1 = "<select name=\"selPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		//$xh2 = "<select name=\"selPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		$xh3 = "<select name=\"selDisplayPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		//$xh4 = "<select name=\"selDisplayPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";

		$totalPage = ceil($totolRecord / $DisplayNumPage);
		
		if($totalPage <= 0)
		$totalPage = 1;

		for($n = 1; $n <= $totalPage; $n++)
		{
			if($n == $CurrentPageNum)
			$xl .= "<option value=\"$n\" selected>$n</option>";
			else
			$xl .= "<option value=\"$n\">$n</option>";
		}
		$xt = "</select>";

		$selectPageTop = $xh1.$xl.$xt;
		//$selectPageBottom = $xh2.$xl.$xt;

		if(5 == $DisplayNumPage){
			$yl .= "<option value=\"5\" selected>5</option>";
		}else{
			$yl .= "<option value=\"5\">5</option>";
		}
		for($n = 10; $n <= 100; $n+=10)
		{
			if($n == $DisplayNumPage)
			$yl .= "<option value=\"$n\" selected>$n</option>";
			else
			$yl .= "<option value=\"$n\">$n</option>";
		}

		$selectDisplayPageTop = $xh3.$yl.$xt;
		//$selectDisplayPageBottom = $xh4.$yl.$xt;
		
		$t  = "";
		$t .= "<tr><td class=\"tablebottom\">";
		$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
		$t .= "<td align=\"left\" class=\"tabletext\">".$RecordMsg."</td>";
		$t .= "<td align=\"right\">";

		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
		
		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$t .= "<tr align=\"center\" valign=\"middle\">";
		$t .= "<td><a href=\"#\" onClick=\"prevPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp','','".$image_path."/".$LAYOUT_SKIN."/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_prev_off.gif\" name=\"prevp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp\"></a> <span class=\"tabletext\">".$eLib["html"]["page"]."</span></td>";
		$t .= "<td class=\"tabletext\">".$selectPageTop."</td>";
		$t .= "<td><a href=\"#\" onClick=\"nextPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp','','".$image_path."/".$LAYOUT_SKIN."/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_next_off.gif\" name=\"nextp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp\"></a></td>";
		$t .= "</tr></table>";
		$t .= "</td>";
		$t .= "<td>&nbsp;<img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\"></td>";
		$t .= "<td>";
		
		$t .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"tabletext\"><tr>";
		$t .= "<td>".$eLib["html"]["display2"]."</td>";
		$t .= "<td>".$selectDisplayPageTop."</td>";
		$t .= "<td>/ ".$eLib["html"]["page"]." </td>";
		$t .= "</tr></table>";
		
		$t .= "</td></tr></table>";

		$t .= "</td></tr></table>";
		$t .= "</td></tr>";
		
		return $t;
	} // end function display Table heading
	
	function displayTableTail($ParArr="", $eLib="")
	{
		
		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
    	$CurrentPageNum = $ParArr["CurrentPageNum"];
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	$totolRecord = $ParArr["totalNum"];
    	
    	$pageDisplay = $ParArr["pageDisplay"];
    	if($pageDisplay == "none")
    	return "";
    	
		//if($totolRecord > 0)
		//$ReportMsg = $eLib["html"]["report"]." 1 - ".$totolRecord.", ".$eLib["html"]["total"]." ".$totolRecord;
		//else
		//$ReportMsg = $eLib["html"]["report"]." 0, ".$eLib["html"]["total"]." 0";

		if($totolRecord > 0){
			$startRec = 1 + ($CurrentPageNum - 1) * $DisplayNumPage;
			//if($startRec > $totolRecord){
			//	$CurrentPageNum = 1;
			//	$startRec = 1;	
			//}
			
			$endRec = $startRec + $DisplayNumPage - 1;
			if($endRec > $totolRecord)
			$endRec = $totolRecord;
	
			//if($endRec == 0)
			//{
			//	$startRec = 0;
			//	$diffRec = 0;
			//}
			//else
			//$diffRec = $endRec - $startRec + 1;
			
			//$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".$diffRec;
			$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".$totolRecord;
		}else{
			$RecordMsg = $eLib["html"]["record"]." 0, ".$eLib["html"]["total"]." 0";
		}		
			
		//$xh1 = "<select name=\"selPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		$xh2 = "<select name=\"selPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		//$xh3 = "<select name=\"selDisplayPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		$xh4 = "<select name=\"selDisplayPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";

		$totalPage = ceil($totolRecord / $DisplayNumPage);
		
		if($totalPage <= 0)
		$totalPage = 1;

		for($n = 1; $n <= $totalPage; $n++)
		{
			if($n == $CurrentPageNum)
			$xl .= "<option value=\"$n\" selected>$n</option>";
			else
			$xl .= "<option value=\"$n\">$n</option>";
		}
		$xt = "</select>";

		//$selectPageTop = $xh1.$xl.$xt;
		$selectPageBottom = $xh2.$xl.$xt;

		if(5 == $DisplayNumPage){
			$yl .= "<option value=\"5\" selected>5</option>";
		}else{
			$yl .= "<option value=\"5\">5</option>";
		}
		for($n = 10; $n <= 100; $n+=10)
		{
			if($n == $DisplayNumPage)
			$yl .= "<option value=\"$n\" selected>$n</option>";
			else
			$yl .= "<option value=\"$n\">$n</option>";
		}

		//$selectDisplayPageTop = $xh3.$yl.$xt;
		$selectDisplayPageBottom = $xh4.$yl.$xt;
		
		$t .= "<tr><td class=\"tablebottom\">";
		$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
		$t .= "<td align=\"left\" class=\"tabletext\">".$RecordMsg."</td>";
		$t .= "<td align=\"right\">";
		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
		$t .= "<td>";
		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$t .= "<tr align=\"center\" valign=\"middle\">";
		$t .= "<td><a href=\"#\" onClick=\"prevPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp1','','".$image_path."/".$LAYOUT_SKIN."/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_prev_off.gif\" name=\"prevp1\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp1\"></a> <span class=\"tabletext\">".$eLib["html"]["page"]." </span></td>";
		$t .= "<td class=\"tabletext\">".$selectPageBottom."</td>";
		$t .= "<td><a href=\"#\" onClick=\"nextPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp1','','".$image_path."/".$LAYOUT_SKIN."/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_next_off.gif\" name=\"nextp1\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp1\"></a></td>";
		$t .= "</tr>";
		$t .= "</table>";
		$t .= "</td>";
		$t .= "<td>&nbsp;<img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\"></td>";
		$t .= "<td>";
		$t .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"tabletext\">";
		$t .= "<tr>";
		$t .= "<td>".$eLib["html"]["display2"]."</td>";
		$t .= "<td>".$selectDisplayPageBottom."</td>";
		$t .= "<td>/ ".$eLib["html"]["page"]." </td>";
		$t .= "</tr>";
		$t .= "</table>";
		$t .= "</td></tr></table>";
		$t .= "</td></tr></table>";
		$t .= "</td></tr>";

		return $t;
	} // end function display table tail
	

	/* **************************************************************************************************
	/* added by Adam for Book editing page
	/* **************************************************************************************************/
	function displayBookAdmin($ParArr="", $eLib="", $sql="")
	{
		global $page_size;
		
		//debug_r($ParArr);
		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$CurrentPageNum = $ParArr["CurrentPageNum"];
		$DisplayNumPage = $ParArr["DisplayNumPage"];
		
		if($DisplayNumPage == "")
		{
			$DisplayNumPage = $page_size;
			$ParArr["DisplayNumPage"] = $DisplayNumPage;
		}
		
		$curUserID = $ParArr["UserID"];
		
		// for sorting
		$sortField = $ParArr["sortField"];
		$sortFieldOrder = $ParArr["sortFieldOrder"];
		//$sortFieldArr = $ParArr["sortFieldArray"];
		
		$x = "";
		
		$returnArr = $this->getBookDetailAll($ParArr);
		//debug_r($returnArr);
		
		$totalNum = count($returnArr);
		
		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}
		
		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;
		$endIndex = $startIndex + $DisplayNumPage; 
		
		if($endIndex > $totalNum)
		$endIndex = $totalNum;
		
		$ParArr["totalNum"] = $totalNum;
			

		////////////////////////////////////////
		$width = 80;
		$height = 100;
		$maxCols = 4;
		
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\">";
		$x .= "<tr>";

		if($totalNum > 0) {
			$tdCount = 0;
			for($i = $startIndex; $i < $endIndex; $i++) {
				$BookID = $returnArr[$i]["BookID"];
				$BookName = $returnArr[$i]["Title"];
				$BookPublish = $returnArr[$i]["Publish"];

				$bookPath = "/file/elibrary/content/".$BookID."/image/cover.jpg";
				# Set Book Cover Image path
				$BookCover = ($BookID != "" && file_exists("../../..".$bookPath))? $bookPath : $image_path."/".$LAYOUT_SKIN."/eLibrary/cover_no_image.jpg";
			
				# HTML BookImage + Name
				$x .= "<td>";
				if($BookID != ""){
					$x .= "<div style='width:100%;'>
					<div style='float:left;'>
					<a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\"><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></a>
					</div>
					<div style='margin-left:3px'>
					<a href=\"elib_admin_edit_book.php?BookID=$BookID\">[ Info ]</a><br/>
					<a href=\"#\" onClick=\"MM_openBrWindowFull('tool/index.php?BookID=$BookID&State=edit','book','scrollbars=yes')\">[ Content ]</a><br/>
					<a href=\"#\" onClick=\"MM_openBrWindowFull('tool/index.php?BookID=$BookID&State=normal','book','scrollbars=yes')\">[ View ]</a><br/>";
					if($BookPublish == "1"){	
						$x .= "<a href=\"admin/unpublish_book_info_update.php?BookID=$BookID\">[ Unpublish ]</a><br/>";
					}else{
						$x .= "<a href=\"admin/publish_book_info_update.php?BookID=$BookID\">[ Publish ]</a><br/>";
					}
					$x .= "<br/>";
					$x .= "<a href=\"admin/remove_update.php?BookID=$BookID\"><span style=\"fontSize:10px;color:#AAAAAA;\">[ delete ]</span></a><br/>";
					$x .= "</div></div>";
					$x .= "<div style='width:100%;clear:both;'><span style='fontSize:15px;fontWeight:900'>".$BookName."</span></div>";
				}
				$x .= "</td>";

				$tdCount++;
				if($tdCount%$maxCols == 0){
					$x .= "</tr><tr>";
				}
			}//end for
			
			$tdLeft = $maxCols - $tdCount%$maxCols;
			if($tdLeft > 0){
				for($j=0; $j<$tdLeft; $j++){
					$x .= "<td></td>";
				}//end for
			}
			
			$x .= "</tr></table>";
		}
		///////////////////////////////////////

		$z = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"><br>";
		$z .= "<table width='98%'>";
		$z .= $this->displayTableTail($ParArr, $eLib);
		$z .= "</table>";
		
		$tmpArr["DisplayNumPage"] = $DisplayNumPage;
		$tmpArr["contentHtml"] = $x.$z;
		$tmpArr["totalRecord"] = count($returnArr);
		return $tmpArr;
	} // end function display Books for Book Editing Page
	/* **************************************************************************************************/


	function displayBookDetail($ParArr="", $eLib="", $sql="")
	{
		global $page_size;
		// display mode 1 - full detail
		// display mode 2 - list mode
		// display mode 3 - review mode
		
		//debug_r($ParArr);
		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$DisplayMode = $ParArr["DisplayMode"];
    	$CurrentPageNum = $ParArr["CurrentPageNum"];
    	
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	
    	if($DisplayNumPage == "")
    	{
    		$DisplayNumPage = $page_size;
    		$ParArr["DisplayNumPage"] = $DisplayNumPage;
		}
    	
    	$curUserID = $ParArr["UserID"];
    	
    	// for sorting
    	$sortField = $ParArr["sortField"];
    	$sortFieldOrder = $ParArr["sortFieldOrder"];
    	$sortFieldArr = $ParArr["sortFieldArray"];
    	
    	// for normal search
    	$selectSearchType = $ParArr["selectSearchType"];
		$keywords = $ParArr["keywords"];
		
		$x = "";
		
		if($sql == "")
		{
			if($selectSearchType == "" && $keywords == "")
			$returnArr = $this->getBookDetail($ParArr);
			else
			$returnArr = $this->getNormalSearch($ParArr);
		}
		else
		{
			$returnArr = $this->getBookQueryList($ParArr, $sql);
		}

		//debug_r($returnArr);
		
		$totalNum = count($returnArr);
		
		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}
		
		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;
		$endIndex = $startIndex + $DisplayNumPage; 
		
		if($endIndex > $totalNum)
		$endIndex = $totalNum;
		
		$ParArr["totalNum"] = $totalNum;
		
		
		if($DisplayMode == 1)
		{
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= $this->displayTableHeading($ParArr, $eLib);
			
			if($totalNum > 0)
			{
				for($i = $startIndex; $i < $endIndex; $i++)
				{
					$BookID = $returnArr[$i]["BookID"];
					//$Title = iconv("utf-8","big-5",$returnArr[$i]["Title"]);
					//$Author = iconv("utf-8","big-5",$returnArr[$i]["Author"]);
					//$Source = iconv("utf-8","big-5",$returnArr[$i]["Source"]);
					//$Category = iconv("utf-8","big-5",$returnArr[$i]["Category"]);
					//$SubCategory = iconv("utf-8","big-5",$returnArr[$i]["SubCategory"]);
					//$Publisher = iconv("utf-8","big-5",$returnArr[$i]["Publisher"]);
					$Title = $returnArr[$i]["Title"];
					$Author = $returnArr[$i]["Author"];
					$Source = $returnArr[$i]["Source"];
					$Category = $returnArr[$i]["Category"];
					$SubCategory = $returnArr[$i]["SubCategory"];
					$Publisher = $returnArr[$i]["Publisher"];
					$Level = $returnArr[$i]["Level"];
			
					$Source = $this->getSourceFullName($Source);
		
					if($Level == "")
					$Level = "--";
		
					$tmpParArr["BookID"] = $BookID;
					$reviewArr = $this->getReview($tmpParArr);
					$reviewNum = count($reviewArr);
			
					$sumRating = 0;
					for($c = 0; $c < $reviewNum ; $c++)
					{
						$sumRating += $reviewArr[$c]["Rating"];
					}
					if($reviewNum != 0)
					$rating = ceil($sumRating / $reviewNum);
					else
					$rating = 0;
			
					$coverFile = "/file/elibrary/content/".$BookID."/image/cover.jpg";
			
					$x .= "<tr><td>";
					$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";
					$x .= "<tr>\n";
					$x .= "<td width=\"110\" align=\"center\" valign=\"top\">\n";
					$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
					$x .= "<tr>\n";
					$x .= "<td><div id=\"book_border\"><a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\"><span><img src=\"".$coverFile."\" width=\"100\" height=\"135\" border=\"0\"></span></a></div></td>\n";
					$x .= "</tr>\n";
					$x .= "</table>\n";
					$x .= "<a href=\"eLibrary_popular_bookdetail.htm\" class=\"contenttool\"></a>";
					$x .= "</td>";
					$x .= "<td align=\"left\" valign=\"top\">";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["title"]."</td>";
					$x .= "<td width=\"10\" align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\"><strong>".$Title."</strong></span></td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["author"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\"><strong>".$Author."</strong></a><a href=\"#\" class=\"eLibrary_commonlink\"></span></td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["category"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\">".$Category;
					$x .= " -> ";
					//$x .= " <img src=\"$image_path/$LAYOUT_SKIN/nav_arrow.gif\" width=\"15\" height=\"15\" align=\"absmiddle\" /> ";
					$x .= $SubCategory."</span></td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["publisher"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\">".$Publisher."</span></td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["source"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\">".$Source."</span></td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["level"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\">".$Level."</td>";
					$x .= "</tr>";
					$x .= "</table>";
					$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";

					//// draw star ////////////////////////////////////////////////////////////////////////////////////////
					$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
					for ($star = 1; $star <= 5; $star++)
					{
						if($star <= $rating)
						{
						$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_on.gif\" width=\"15\" height=\"20\">";
						}
						else
						{
						if($star <= $rating + 0.5)
						$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_half.gif\" width=\"15\" height=\"20\">";
						else
						$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_off.gif\" width=\"15\" height=\"20\">";
						}
					} // end for loop star
					$x .= "</td></tr></table>";
					////////////////////////////////////////////////////////////////////////////////////////////////////////

					$x .= "</td>";
					$x .= "<td>";
					$x .= "<a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\" class=\"contenttool\">(".$reviewNum." <img src=\"$image_path/$LAYOUT_SKIN/eLibrary/icon_comment.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">)</a>";
					$x .= "</td></tr></table>";
					$x .= "</td></tr></table>";
					$x .= "</td></tr>";

					if($i < $endIndex - 1)
					{
					$x .= "<tr>";
					$x .= "<td height=\"5\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"5\"></td>";
					$x .= "</tr>";
					}
				} // end for loop row
			} // end if check total record nun
			else
			{
				$x .= "<tr class=\"tablerow1\">";
				$x .= "<td class=\"tabletext\" align=\"center\">".$eLib["html"]["no_record"]."</td>";
				$x .= "</tr>";	
			}
		} // end if display mode = 1
		else if ($DisplayMode == 2)
		{
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= "<tr><td class=\"tablebottom\">";
			$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$t .= "<tr class=\"tabletop\">";

			for($s = 0; $s < count($sortFieldArr) ; $s++)
			{
				$value = $sortFieldArr[$s]["value"];
				$text = $sortFieldArr[$s]["text"];
	
				if($sortField == $value && $sortFieldOrder == "ASC")
				$order = "a";
				else
				$order = "d";
	
				$t .= "<td><a href=\"#\" class=\"tabletoplink\" onClick=\"sortOrder('".$value."');\"";
				
				if($sortField == $value)
				{
				$t .= " onMouseOver=\"MM_swapImage('sort".$s."','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_on.gif',1)\"";
				$t .= " onMouseOut=\"MM_swapImgRestore()\">".$text."<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_off.gif\"";
				$t .= " name=\"sort".$s."\" width=\"13\" height=\"13\" border=\"0\" align=\"absmiddle\" id=\"sort".$s."\">";
				}
				else
				$t .= ">".$text;
				
				$t .= "</a></td>";
				
			} // end for loop sort
				$t .= "</tr>";
		
			if($totalNum > 0)
			{
				for($i = $startIndex; $i < $endIndex; $i++)
				{
					$BookID = $returnArr[$i]["BookID"];

					$row = "";
					for($k = 0; $k < count($sortFieldArr); $k++)
					{
						$value = $sortFieldArr[$k]["value"];
						//$row[$k] = iconv("utf-8","big-5",$returnArr[$i][$value]);
						$row[$k] = $returnArr[$i][$value];
						
						if($value == "UserName")
						$row[$k] = $returnArr[$i][$value];
						
						if($value == "Source")
						$row[$k] = $this->getSourceFullName($row[$k]);
					}
			
					if($i % 2 == 0)
					$x .= "<tr class=\"tablerow1\">";
					else
					$x .= "<tr class=\"tablerow2\">";
					
					for($k = 0; $k < count($sortFieldArr); $k++)
					{
						$width = $sortFieldArr[$k]["width"];
						$value = $sortFieldArr[$k]["value"];
						
						if($width == "")
						$width = floor(100 / count($sortFieldArr))."%";
						
						if($k == 0 && $value == "Title")
						$x .= "<td width=".$width."><a href=\"#\" class=\"tablelink\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes',true)\">".$row[$k]."</a></td>";
						else
						$x .= "<td width=".$width." class=\"tabletext\">".$row[$k]."</td>";
					}
					
					$x .= "</tr>";
				} // end for loop row
			} // end if check total record num
			else
			{
				$x .= "<tr class=\"tablerow1\">";
				
				for($n = 0; $n < count($sortFieldArr) ; $n++)
				{
					if($n == floor(count($sortFieldArr) / 2))
					$x .= "<td class=\"tabletext\">".$eLib["html"]["no_record"]."</td>";
					else
					$x .= "<td class=\"tabletext\">&nbsp;</td>";
				}
				$x .= "</tr>";
			}
				$x .= "</table>";
				$x .= "</td></tr>";
		} // end display mode = 2
		else if($DisplayMode == 3)
		{
			
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= $this->displayTableHeading($ParArr, $eLib);
			
			$x = "";
			$x = "<tr><td align='center'>";
	
			if($totalNum > 0)
			{
			for($i = $startIndex; $i < $endIndex; $i++)
			{
				$tmpUserID =  $returnArr[$i]["UserID"];
				$BookID = $returnArr[$i]["BookID"];
				$ReviewID = $returnArr[$i]["ReviewID"];
				//$Content = iconv("utf-8","big-5",$returnArr[$i]["Content"]);
				$Content = $returnArr[$i]["Content"];
				
				// remove the tag
				$Content = strip_tags($Content);
				
				$Rating = $returnArr[$i]["Rating"];
				$Date = $returnArr[$i]["Date"];
				//$Title = iconv("utf-8","big-5",$returnArr[$i]["Title"]);
				$Title = $returnArr[$i]["Title"];
				
				$coverFile = "/file/elibrary/content/".$BookID."/image/cover.jpg";
				
				$isAnswer = $this->getHelpfulCheck($ReviewID);
				$helpfulArr = $this->getHelpfulVote($ReviewID);
				$numTotalVote = $helpfulArr["total"];
				$numUsefulVote = $helpfulArr["yesNum"];
			 	$helpfulStr = $this->getHelpfulMessage($eLib, $numTotalVote, $numUsefulVote);
				
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td width=\"100\">";
			$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr><td align=\"center\">";
			$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
			$x .= "<td><div id=\"book_border\"><a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=".$BookID."','bookdetail','scrollbars=yes')\"><span><img src=\"".$coverFile."\" width=\"100\" height=\"135\" border=\"0\"></span></a></div></td>";
			$x .= "</tr></table></td></tr>";
			$x .= "<tr><td>";
			$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
			$x .= "<tr><td align=\"center\" class=\"tabletext\"><a href=\"#\" class=\"eLibrary_commonlink\">".$Title."</a></td></tr>";
			$x .= "</table></td></tr>";
			$x .= "</table></td>";
			$x .= "<td align=\"left\" valign=\"top\">";
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
			$x .= "<td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>";
			$x .= "<td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>";
			$x .= "<td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04.gif\">&nbsp;</td>";
			$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\">";
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr>";
			$x .= "<td align=\"right\" class=\"tabletext forumtablerow\">";
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr><td align=\"left\">";
			$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
			$x .= "<td class=\"tabletextremark\">".$eLib["html"]["rating"].":</td>";

			//// draw star ////////////////////////////////////////////////////////////////////////////////////////
			for ($star = 1; $star <= 5; $star++)
			{
				if($star <= $Rating)
				{
				$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_on.gif\" width=\"15\" height=\"20\"></td>";
				}
				else
				{
				if($star <= $Rating + 0.5)
				$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_half.gif\" width=\"15\" height=\"20\"></td>";
				else
				$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_off.gif\" width=\"15\" height=\"20\"></td>";
				}
			} // end for loop star
			////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			$x .= "</tr></table></td>";
			$x .= "<td align=\"right\" class=\"tabletext\">".$Date."</td>";
			$x .= "</tr></table></td></tr>";
			$x .= "<tr><td class=\"tabletext\">".$Content."</td></tr>";
			$x .= "<tr>";
			$x .= "<td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td height=\"5\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td>";
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td height=\"10\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
			$x .= "<tr>";
			$x .= "<td class=\"tabletext\">".$helpfulStr."</td>";
			
			if($curUserID != $tmpUserID && $isAnswer == false)
			{
				$x .= "<td align=\"right\" class=\"tabletext\">";
				$x .= $eLib["html"]["was_this_review_helpful"]."&nbsp;";
				$x .= "<input name=\"yes_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview('yes','".$ReviewID."','".$BookID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["yes"]."\">&nbsp;";
				$x .= "<input name=\"no_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview('no','".$ReviewID."','".$BookID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["no"]."\">";
				$x .= "</td>";
			}
			else
			$x .= "<td align=\"right\">&nbsp;</td>";
			
			$x .= "</tr></table></td>";
			$x .= "</tr></table></td>";
			
			
			$x .= "<td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_06.gif\">&nbsp;</td>";
				
			$x .= "</tr>";
			$x .= "<tr>";
			$x .= "<td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>";
			$x .= "<td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>";
			$x .= "<td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>";
			$x .= "</tr></table></td></tr></table><br />";
			} // end for loop
		}// if check total num < 0
			else
			{
				$x .= "<tr class=\"tablerow1\">";
				$x .= "<td class=\"tabletext\" align=\"center\">".$eLib["html"]["no_record"]."</td>";
				$x .= "</tr>";	
			}
			$x .= "</td></tr>";
		} // end display mode = 3 for review table
		else if ($DisplayMode == 4)
		{			
		} // End displaymode 4 -> class book summary
		else
		{
			$t = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= $this->displayTableHeading($ParArr, $eLib);	
		} // no display mode select
		

	$z = $this->displayTableTail($ParArr, $eLib);

	$z .= "</table>";
	
	$tmpArr["DisplayNumPage"] = $DisplayNumPage;
	$tmpArr["contentHtml"] = $t.$x.$z;
	$tmpArr["totalRecord"] = count($returnArr);
	return $tmpArr;
} // end function display Book Detail

function displayClassSummaryDetail($ParArr="", $eLib="", $sql="")
	{
		global $page_size;
		
		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$DisplayMode = $ParArr["DisplayMode"];
    	$CurrentPageNum = $ParArr["CurrentPageNum"];
    	
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	
    	if($DisplayNumPage == "")
    	{
    		$DisplayNumPage = $page_size;
    		$ParArr["DisplayNumPage"] = $DisplayNumPage;
		}
    	
    	$curUserID = $ParArr["UserID"];
    	
    	// for sorting
    	$sortField = $ParArr["sortField"];
    	$sortFieldOrder = $ParArr["sortFieldOrder"];
    	$sortFieldArr = $ParArr["sortFieldArray"];
    	
    	// for normal search
    	$selectSearchType = $ParArr["selectSearchType"];
		$keywords = $ParArr["keywords"];
		
		$x = "";
		
		if($sql == "")
		{
			if($selectSearchType == "" && $keywords == "")
			$returnArr = $this->getBookDetail($ParArr);
			else
			$returnArr = $this->getNormalSearch($ParArr);
		}
		else
		{
			$returnArr = $this->getBookQueryList($ParArr, $sql);
		}

//		debug_r($returnArr);
		
		$totalNum = count($returnArr);
		
		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}
		
		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;
		$endIndex = $startIndex + $DisplayNumPage; 
		
		if($endIndex > $totalNum)
		$endIndex = $totalNum;
		
		$ParArr["totalNum"] = $totalNum;
		
		$sortPageBy = ($sortFieldOrder)? 0 : 1;
			
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= "<tr><td class=\"tablebottom\">";
			$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$t .= "<tr class=\"tabletop\">";

			for($s = 0; $s < count($sortFieldArr) ; $s++)
			{
				$value = $sortFieldArr[$s]["value"];
				$text = $sortFieldArr[$s]["text"];
	
				if($sortField == $value && $sortFieldOrder == 0)
					$order = "a";
				else
					$order = "d";
				
				
					$t .= "<td";
				$t .= ($s > 1)? ' colspan= "2" ' : '';
				$t .= ">";
				if( $s <2)
					$t .= "<a href=\"#\" class=\"tabletoplink\" onClick=\"sortPage(".$sortPageBy.", ".$s.", document.form1);\""; 
				
				if($sortField == $value && $s< 2)
				{			    
				$t .= " onMouseOver=\"MM_swapImage('sort".$s."','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_on.gif',1)\"";
				$t .= " onMouseOut=\"MM_swapImgRestore()\">".$text."<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_off.gif\"";
				$t .= " name=\"sort".$s."\" width=\"13\" height=\"13\" border=\"0\" align=\"absmiddle\" id=\"sort".$s."\">";				
				}
				else{						
					$t .= ($s <2)? ">".$text: $text ;
				}
				
				if( $s <2)
					$t .= "</a>";
				
				$t .= "</td>";
				
			} // end for loop sort
				$t .= "</tr>";
				$t .= "<tr class=\"tabletop\">";
			$sortFieldArr2 = $ParArr["sortFieldArray2"];
			
			for($s = 0; $s < count($sortFieldArr2) ; $s++)
				{
					$value = $sortFieldArr2[$s]["value"];
					$text = $sortFieldArr2[$s]["text"];
		
					if($sortField == $value && $sortFieldOrder == 0)
					$order = "a";
					else
					$order = "d";
		
					$t .= ( $s >1)? "<td class='subtitle'>" : "<td>";
					if( $s >1)
						$t .= "<a href=\"#\" class=\"tabletoplink\" onClick=\"sortPage(".$sortPageBy.", ".$s.", document.form1);\"";
					
					if($sortField == $value && $s > 1)
					{
					$t .= " onMouseOver=\"MM_swapImage('sort".$s."','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_on.gif',1)\"";
					$t .= " onMouseOut=\"MM_swapImgRestore()\">".$text."<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_off.gif\"";
					$t .= " name=\"sort".$s."\" width=\"13\" height=\"13\" border=\"0\" align=\"absmiddle\" id=\"sort".$s."\">";
					}
					else
					$t .= ($s > 1)? ">".$text: $text ;
					
					
					
					if( $s >1)
						$t .= "</a>";
						
					$t .= "</td>";
					
				} // end for loop sort
					$t .= "</tr>";
		
			if($totalNum > 0)
			{
				for($i = $startIndex; $i < $endIndex; $i++)
				{
					$BookID = $returnArr[$i]["BookID"];
					$YearClassID = $returnArr[$i]["YearClassID"];

					$row = "";
					for($k = 0; $k < count($sortFieldArr2); $k++)
					{
						$value = $sortFieldArr2[$k]["value"];
						//$row[$k] = iconv("utf-8","big-5",$returnArr[$i][$value]);
						$row[$k] = $returnArr[$i][$value];
						
						if($value == "UserName")
						$row[$k] = $returnArr[$i][$value];
						
						if($value == "Source")
						$row[$k] = $this->getSourceFullName($row[$k]);
					}
			
					if($i % 2 == 0)
					$x .= "<tr class=\"tablerow1\">";
					else
					$x .= "<tr class=\"tablerow2\">";
					
					for($k = 0; $k < count($sortFieldArr2); $k++)
					{
						$width = $sortFieldArr2[$k]["width"];
						$value = $sortFieldArr2[$k]["value"];
						
						if($width == "")
						$width = floor(100 / count($sortFieldArr2))."%";
						
						if($k == 0 && $value == "Title")
							$x .= "<td width=".$width."><a href=\"#\" class=\"tablelink\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\">".$row[$k]."</a></td>";
						else if($value == "ClassName")
							$x .= "<td width=".$width." class=\"tabletext\"><a href=\"elib_public_students_summary.php?YearClassID=".($YearClassID)."&ClassName=".($row[$k])."\">".$row[$k]."</a></td>";
						else
							$x .= "<td width=".$width." class=\"tabletext\">".$row[$k]."</td>";
					}
					
					$x .= "</tr>";
				} // end for loop row
			} // end if check total record num
			else
			{
				$x .= "<tr class=\"tablerow1\">";
				
				for($n = 0; $n < count($sortFieldArr) ; $n++)
				{
					if($n == floor(count($sortFieldArr) / 2))
					$x .= "<td class=\"tabletext\">".$eLib["html"]["no_record"]."</td>";
					else
					$x .= "<td class=\"tabletext\">&nbsp;</td>";
				}
				$x .= "</tr>";
			}
				$x .= "</table>";
				$x .= "</td></tr>";
		$z = $this->displayTableTail($ParArr, $eLib);

	$z .= "</table>";
	
	$tmpArr["DisplayNumPage"] = $DisplayNumPage;
	$tmpArr["contentHtml"] = $t.$x.$z;
	$tmpArr["totalRecord"] = count($returnArr);
	return $tmpArr;
}

	
function printSearchInput($ParArr="", $eLib="")
{
	$selectedArr = Array();
	
	$image_path = $ParArr["image_path"];
	$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
	$isSearch = $_SESSION["elib"]["is_search"];
	$searchWord = $_SESSION["elib"]["search_word"];
	$is_search_select = $_SESSION["elib"]["is_search_select"];
	
	$selectedArr[$is_search_select] = "selected";
	
	if($isSearch == 1)
	{
		$className = "tabletext";
		$checkInput = "";
		$searchTxt = $searchWord;
	}
	else
	{
		$className = "tabletextremark";
		$checkInput = "checkInputText(this);";
		$searchTxt = $eLib["html"]["search_input"];
	}
	
	
	$x = "";
	$x .= "<form name=\"searchForm\" action=\"elib_advanced_search_result.php\" method=\"post\" onSubmit=\"return checkSearchForm(this);\">";
	$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"30\" align=\"left\" >";
	$x .= "<tr>";
	$x .= "<td width=\"25\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\"></td>";
	$x .= "<td align=\"left\" >";
	$x .= "<select name=\"selectSearchType\" >";
	$x .= "<option value=\"0\" ".$selectedArr[0].">".$eLib['html']['book_title']."</option>";
	$x .= "<option value=\"1\" ".$selectedArr[1].">".$eLib["html"]["author"]."</option>";
	$x .= "<option value=\"2\" ".$selectedArr[2].">".$eLib["html"]["publisher"]."</option>";
	$x .= "<option value=\"3\" ".$selectedArr[3].">".$eLib["html"]["keywords"]."</option>";
	$x .= "</select>";
	$x .= "&nbsp;<input onFocus=\"".$checkInput."\" name=\"keywords\" type=\"text\" class=\"".$className."\" value=\"".$searchTxt."\" size=\"45\" /></td>";
	$x .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"><input name=\"submit\" type=\"submit\" class=\"formsubbutton\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["search"]."\">";
	$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\">| <img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"><a href=\"elib_advanced_search_form.php\" class=\"contenttool\">{$eLib['html']['advance_search']}</a></td>";
	$x .= "</tr>";
	$x .= "</table>";
	$x .= "<input type=\"hidden\" name=\"isSearch\" value=\"".$isSearch."\" />";
	$x .= "<input type=\"hidden\" name=\"isSearchSelect\" value=\"".$is_search_select."\" />";
	$x .= "</form>";
	
	return $x;
} // end function print search input

	function getReview($ParArr="")
	{
    	$BookID = $ParArr["BookID"];
    	
		$sql = "
		SELECT 
		a.ReviewID, a.UserID, a.Rating, a.Content, a.DateModified, 
		b.EnglishName, b.ChineseName, b.ClassName, b.ClassNumber
		FROM
		INTRANET_ELIB_BOOK_REVIEW a, INTRANET_USER b
		WHERE
		a.BookID = '{$BookID}'
		AND a.UserID = b.UserID
		ORDER BY a.ReviewID DESC
		";
		
		//debug_r($sql);
		
		$returnArr = $this->returnArray($sql, 5);
		
		//debug_r($returnArr);
		
		return $returnArr;
	} // end function get review
	
	function addBookReview($ParArr="")
	{
		$Rating = $ParArr["Rating"];
		$Content = $ParArr["Content"];
		$BookID = $ParArr["BookID"];
		$currUserID = $_SESSION["UserID"];
		
		//$Content = iconv("BIG-5", "UTF-8", $Content);
		
		$sql = "
		INSERT INTO
		INTRANET_ELIB_BOOK_REVIEW
		(BookID, UserID, Rating, Content, DateModified)
		VALUES
		('".$BookID."', '".$currUserID."', '".$Rating."', '".$Content."', now())
		";
		
		$this->db_db_query($sql);
		$id = $this->db_insert_id();
		
		return $id;
	} // end function add book review
	
	function addBookRecommend($ParArr="")
	{
		$Description = $ParArr["Description"];
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["UserID"];
		//$ClassLevelIDArr = $ParArr["ClassLevelID"];
		$RecommendGroup = $ParArr["ClassLevelID"];
		/*$x = "";
		
		for($i = 0; $i < count($ClassLevelIDArr); $i++)
		{
			$sep = "|";
			$x .= $sep.$ClassLevelIDArr[$i];
		}
		$x .= "|";
		
		$RecommendGroup = $x;*/
		
		$sql = "
		INSERT INTO
		INTRANET_ELIB_BOOK_RECOMMEND
		(BookID, UserID, Description, RecommendGroup, DateModified)
		VALUES
		('".$BookID."', '".$currUserID."', '".$Description."', '".$RecommendGroup."', now())
		";
		
		if($this->db_db_query($sql))
			return $this->db_insert_id();
		else
			return;
	} // end function add book recommend
	
	function getBookRecommend($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		
		$sql = "
		SELECT 
		Description, RecommendGroup
		FROM
		INTRANET_ELIB_BOOK_RECOMMEND
		WHERE
		BookID = '".$BookID."'
		";
		
		$returnArr = $this->returnArray($sql, 2);
		
		return $returnArr;
	} // end function get book recommend
	
	function editBookRecommend($ParArr="")
	{
		$Description = $ParArr["Description"];
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["UserID"];
		//$ClassLevelIDArr = $ParArr["ClassLevelID"];
		$RecommendGroup = $ParArr["ClassLevelID"];
		/*$x = "";
		
		for($i = 0; $i < count($ClassLevelIDArr); $i++)
		{
			$sep = "|";
			$x .= $sep.$ClassLevelIDArr[$i];
		}
		$x .= "|";
		
		$RecommendGroup = $x;*/
		
		$sql = "
		UPDATE
			INTRANET_ELIB_BOOK_RECOMMEND
		SET
			UserID = '".$currUserID."',
			Description = '".$Description."',
			RecommendGroup = '".$RecommendGroup."',
			DateModified = now()
		WHERE
			BookID = '".$BookID."'
			LIMIT 1
		";
		
		return $this->db_db_query($sql);
	} // end function edit book recommend
	
	function addBookReviewHelpful($ParArr="")
	{
		$ReviewID 	= $ParArr["ReviewID"];
		$BookID 	= $ParArr["BookID"];
		$Choose 	= $ParArr["Choose"];
		$currUserID 	= $_SESSION["UserID"];
			
		if($BookID != "" && $ReviewID != "" && $Choose != "" && $currUserID != "")
		{
			
			$sql = "
				INSERT INTO
				INTRANET_ELIB_BOOK_REVIEW_HELPFUL
				(BookID, UserID, Choose, DateModified, ReviewID)
				VALUES
				('".$BookID."', '".$currUserID."', '".$Choose."', now(), '".$ReviewID."')
			";
			
			$this->db_db_query($sql);
			return $this->db_insert_id();
		}
		
		return;
	} // end function add book review
	
	function getHelpfulCheck($ReviewID="")
	{
		$curUserID = $_SESSION["UserID"];
		
		$sql = "
		SELECT 
		ReviewCommentID
		FROM
		INTRANET_ELIB_BOOK_REVIEW_HELPFUL
		WHERE
		ReviewID = '{$ReviewID}'
		AND UserID = '{$curUserID}'
		";
		
		//debug_r($sql);
		
		$returnArr = $this->returnArray($sql, 1);
		
		//debug_r($returnArr);
		if(count($returnArr) > 0)
		return true;
		else
		return false;	
	} // end function get helpful check
	
	function getHelpfulVote($ReviewID="")
	{
		$sql = "
		SELECT 
		ReviewCommentID, Choose, UserID
		FROM
		INTRANET_ELIB_BOOK_REVIEW_HELPFUL
		WHERE
		ReviewID = '{$ReviewID}'
		GROUP BY
		UserID
		";
		
		$returnArr = $this->returnArray($sql, 3);
		
		$tmpArr["total"] = count($returnArr); 
		
		$countYes = 0;
		for($i = 0; $i < count($returnArr); $i++)
		{
			if($returnArr[$i]["Choose"] == "1")
			$countYes++;
		}
		$tmpArr["yesNum"] = $countYes; 
		
		return $tmpArr;
	} // end function get helpful vote
	
	function GET_CLASSNAME()
	{
		//$sql = "SELECT a.ClassID,a.ClassName FROM INTRANET_CLASS AS a WHERE a.RecordStatus=1 ORDER BY a.ClassName";
		
		$yid = Get_Current_Academic_Year_ID();
		$sql = "
			SELECT
				YearClassID, ClassTitleEN
			FROM
				YEAR_CLASS
			WHERE
				AcademicYearID = '{$yid}'
			ORDER BY
				YearID
			ASC
		";
		$returnArr = $this->returnArray($sql,2);
		
		//var_dump($returnArr);
		return $returnArr;
	} // end function get className

	function GET_STUDENT_NAME_BY_CLASS($ParArr="")
	{
		$ClassName = $ParArr["ClassName"];
		
		$sql = "
		SELECT 
		UserID, EnglishName, ChineseName
		FROM
		INTRANET_USER
		WHERE
		ClassName = '{$ClassName}'
		ORDER BY
		EnglishName ASC, ChineseName ASC
		";
		
		$returnArr = $this->returnArray($sql, 3);
		
		return $returnArr;
	} // end function get student name by class
	
	function GET_STUDENT_NAME_BY_ID($ParArr="")
	{
		$StudentID = $ParArr["StudentID"];
		
		$sql = "
		SELECT 
		ClassNumber, EnglishName, ChineseName
		FROM
		INTRANET_USER
		WHERE
		UserID = '{$StudentID}'
		";
		
		$returnArr = $this->returnArray($sql, 3);
		
		return $returnArr;
	} // end function get student name by ID
	
 	function displayReviewTableHeading($ParArr="", $eLib="")
 	{	
    	$returnArr = $this->getReview($ParArr);
 
    	$x = "";
    	
    	if($returnArr != "")
    	{
	    	$ParArr["totalNum"] = count($returnArr);
	 		$x = $this->displayTableHeading($ParArr, $eLib);
 		}
 		
	 	return $x;
 	} // end function print review table
 	
 	function displayReviewTable($ParArr="", $eLib="", $sql="")
 	{
	 	global $intranet_root;
	 	
	 	$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$curUserID = $ParArr["UserID"];
		$CurrentPageNum = $ParArr["CurrentPageNum"];
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	
    	if($sql == "")
	 	$returnArr = $this->getReview($ParArr);
	 	else
	 	$returnArr = $this->getBookQueryList($ParArr, $sql);
	 	
	 	$ParArr["totalNum"] = count($returnArr);
	 	
	 	$totalNum = count($returnArr);
		
		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}
		
		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;
		
		if($startIndex < 0)
		$startIndex = 0;
		
		$endIndex = $startIndex + $DisplayNumPage; 
		
		if($endIndex > $totalNum || $totalNum == 0)
		$endIndex = $totalNum;
		
	 	//debug_r($returnArr);
	 	
	 	$x = "";
	 	
	 	if($returnArr != "")
	 	{
		 	for($i = $startIndex; $i < $endIndex; $i++)
		 	{
			 	$currUserID = $returnArr[$i]["UserID"];
			 	
			 	$li = new libuser($currUserID);
				$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";

				if ($li->PhotoLink !="")
				{
					if (is_file($intranet_root.$li->PhotoLink))
					{
						$photo_link = $li->PhotoLink;
					}
				}
			
				//$headImg = "images/cimg/ecomm/basketball_club/student/Chan Siu Man.jpg";
				$headImg = "$photo_link";
				
				$ReviewID = $returnArr[$i]["ReviewID"];
				$EnglishName = $returnArr[$i]["EnglishName"];
				$ChineseName = $returnArr[$i]["ChineseName"];
				$ClassName = $returnArr[$i]["ClassName"];
				$ClassNumber = $returnArr[$i]["ClassNumber"];
				
				//debug_r($_SESSION);
				if($_SESSION["intranet_session_language"] == "b5")
				{
					$UserName1 = $ChineseName;
					$UserName2 = $EnglishName;
				}
				else
				{
					$UserName1 = $EnglishName;
					$UserName2 = $ChineseName;
				}
				
				if($UserName1 != "")
				$UserName = $UserName1;
				else
				$UserName = $UserName2;
				
				if($ClassName != "")
				$nameStr = $UserName." (".$ClassName."-".$ClassNumber.")";
				else
				$nameStr = $UserName;
				
				$date = $returnArr[$i]["DateModified"];
				$content = $returnArr[$i]["Content"];
				
				//$content = iconv("UTF-8", "BIG-5", $content);
				
				$content = strip_tags($content);
				
				$tmpUserID = $returnArr[$i]["UserID"];
				$rating = $returnArr[$i]["Rating"];
				
				$isAnswer = $this->getHelpfulCheck($ReviewID);
				$voteArr = $this->getHelpfulVote($ReviewID);
				$numTotalVote = $voteArr["total"];
				$numUsefulVote = $voteArr["yesNum"];
				$helpfulStr = $this->getHelpfulMessage($eLib, $numTotalVote, $numUsefulVote);
				
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td width=\"100\">";
				$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
				$x .= "<tr><td width=\"120\" align=\"center\">";
				$x .= "<table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td>";
				$x .= "<div id=\"div\"><span><img src=\"".$headImg."\" width=\"45\" height=\"60\" border=\"0\"></span> </div></td>";
				$x .= "</tr></table>";
				$x .= "</td></tr>";
				$x .= "<tr><td width=\"120\" align=\"center\" class=\"tabletext\">".$nameStr."</td></tr>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "<td align=\"left\" valign=\"top\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>";
				$x .= "<td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>";
				$x .= "<td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04b.gif\" width=\"25\" height=\"40\"></td>";
				$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td align=\"right\" class=\"tabletext forumtablerow\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
				$x .= "<td align=\"left\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td class=\"tabletextremark\" >".$eLib["html"]["rating"].":</td>";
				
				for($j = 1 ; $j <= 5; $j++)
				{
					if($j <= $rating)
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_on.gif\" width=\"15\" height=\"20\"></td>";
					else
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_off.gif\" width=\"15\" height=\"20\"></td>";
				}
				
				$x .= "</tr></table>";
				$x .= "</td>";
				$x .= "<td align=\"right\" class=\"tabletext\">".$date."</td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "</tr>";
				$x .= "<tr><td class=\"tabletext\">".$content."</td></tr>";
				$x .= "<tr><td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td></tr>";
				$x .= "<tr><td height=\"5\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td></tr>";
				$x .= "<tr><td height=\"10\" class=\"tabletext\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";
				$x .= "<td class=\"tabletext\" id='helpful_display_".$ReviewID."'>".$helpfulStr."</td>";
				$x .= "<td align=\"right\" class=\"tabletext\" id='helpful_vote_panel_".$ReviewID."'>";
				
				if($curUserID != $tmpUserID && $isAnswer == false)
				{
				$x .= $eLib["html"]["was_this_review_helpful"]."&nbsp;";
				$x .= "<input name=\"yes_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview(1,'".$ReviewID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["yes"]."\">&nbsp;";
				$x .= "<input name=\"no_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview(0,'".$ReviewID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["no"]."\">";
				}
				
				$x .= "</td></tr></table>";
				$x .= "</td></tr></table>";
				$x .= "</td>";
				
				/*Edited - <img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/forum_bubble_re_06.gif\" width=\"9\" height=\"80\" border='none' />*/				
				$x .= "<td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_06.gif\">&nbsp;</td>";
				
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>";
				$x .= "<td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "<td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "</tr></table>";
				$x .= "</td></tr></table>";
				$x .= "<br />";
	 		} // end for loop 
	 	}
	 	
	 	//$t = $this->displayReviewTableHeading($ParArr, $eLib);
	 	$y = $this->displayTableTail($ParArr, $eLib);
	 	
	 	$tmpArr["content"] = $x.$y;
	 	$tmpArr["totalRecord"] = $totalNum;
	 	
	 	return $tmpArr;
 	} // end function display review table
 	
 	function displayRecordListMenu($ParArr="", $eLib="")
 	{
	 	
		/*2010-01-20 : New navigation style */		
			$image_path = $ParArr["image_path"];
			$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
			$currUserID = $ParArr["UserID"];
			
		 	$x .= "";
			if($this->IS_TEACHER() || $this->IS_ADMIN())
			{
				$x .= "<div id=\"list0\"><p><a href=\"#\">"."<a href=\"elib_admin.php\" class=\"eLibrary_myrecord_list\">Books Editing</a></p></div>";
			}
			$x .= "<div id=\"list1\"><p><a href=\"#\">"."<a href=\"elib_record_history.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reading_history"]."</a></p></div>";
			$x .= "<div id=\"list2\"><p><a href=\"#\">"."<a href=\"elib_record_favourite.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_favourites"]."</a></p></div>";
			$x .= "<div id=\"list3\"><p><a href=\"#\">"."<a href=\"elib_public_students_summary_review.php?myUserID=".$currUserID."\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reviews"]."</a></p></div>";
			$x .= "<div id=\"list4\"><p><a href=\"#\">"."<a href=\"elib_record_mynotes.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_notes"]."</a></p></div>";
			
			$x .= "<div id=\"list5\"><p><a href=\"#\">"."<a href=\"elib_public_all_reviews.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["all_reviews_2"]."</a></p></div>";
			
			if($this->IS_TEACHER() || $this->IS_ADMIN())
			{
			$x .= "<div id=\"list6\"><p><a href=\"#\">"."<a href=\"elib_public_class_summary.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["class_summary"]."</a></p></div>";
			
			}
		
			return $x;
 	} // end function display record list menu
 	
 	
 	function newDisplayRecordListMenu($ParArr="", $eLib="")
	 	{
		 	$image_path = $ParArr["image_path"];
			$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
			$currUserID = $_SESSION["UserID"];
			
		 	$x .= "";
			$x .= "<div id=\"list1\"><p><a href=\"#\">"."<a href=\"elib_record_history.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reading_history"]."</a></p></div>";
			$x .= "<div id=\"list2\"><p><a href=\"#\">"."<a href=\"elib_record_favourite.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_favourites"]."</a></p></div>";
			$x .= "<div id=\"list3\"><p><a href=\"#\">"."<a href=\"elib_public_students_summary_review.php?myUserID=".$currUserID."\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reviews"]."</a></p></div>";
			$x .= "<div id=\"list4\"><p><a href=\"#\">"."<a href=\"elib_record_mynotes.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_notes"]."</a></p></div>";
			
			
			
			//modified by Addam (2008.11.06) - delete the icons and categories
			///////////////////////////////////////////////////////////////////
			//$x .= "</table>";
			//$x .= "</td>";
			//$x .= "<td background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/myrecord_board_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"16\" height=\"110\"></td>";
			//$x .= "</tr>";
			//$x .= "<tr>";
			//$x .= "<td width=\"12\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/myrecord_board_ex_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"12\" height=\"4\"></td>";
			//$x .= "<td valign=\"middle\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/myrecord_board_05.gif\">";
			//$x .= "<span class=\"eLibrary_myrecord_list_title\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_public.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">".$eLib["html"]["public"]."</span>";
			//$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			///////////////////////////////////////////////////////////////////
			
			$x .= "<div id=\"list5\"><p><a href=\"#\">"."<a href=\"elib_public_all_reviews.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["all_reviews_2"]."</a></p></div>";
		
			
			if($this->IS_TEACHER() || $this->IS_ADMIN())
			{
			$x .= "<div id=\"list6\"><p><a href=\"#\">"."<a href=\"elib_record_mynotes.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["student_summary"]."</a></p></div>";
			
			}
		
			return $x;
	 	} // end function display record list menu
 	
 	
 	function getHelpfulMessage($eLib="", $numTotalVote=0, $numUsefulVote=0)
 	{
	 	$x = "<strong>".$numUsefulVote."</strong> ".$eLib["html"]["of"]." ".$numTotalVote." ".$eLib["html"]["people_found_this_review_helpful"];
	 	return $x;
 	} // end function
 	
 	/*
	* Check if the user is guest
	*/
	function IS_GUEST($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_reportcard_usertype'];
		}

		$ReturnVal = ($ParUserType=="GUEST");

		return $ReturnVal;
	}

	/*
	* Check if the user is student
	*/
	function IS_STUDENT($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_elibrary_usertype'];
		}

		$ReturnVal = ($ParUserType=="STUDENT");

		return $ReturnVal;

	}

	/*
	* Check if the user is teacher
	*/
	function IS_TEACHER($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_elibrary_usertype'];
		}

		$ReturnVal = ($ParUserType=="TEACHER");

		return $ReturnVal;

	}

	/*
	* Check if the user is system admin
	*/
	function IS_ADMIN($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_elibrary_usertype'];
		}

		$ReturnVal = ($ParUserType=="ADMIN");

		return $ReturnVal;
	}
	
	function GET_ADMIN_USER()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/elibrary/admin_user.txt"));

		return $AdminUser;
	}
	

	function IS_ADMIN_USER($ParUserID)
	{
		global $intranet_root;

		$AdminUser = $this->GET_ADMIN_USER();
		
		$IsAdmin = 0;
		if(!empty($AdminUser))
		{
			$AdminArray = explode(",", $AdminUser);
			$IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
		}

		return $IsAdmin;
	}
	
	function IS_MY_FAVOURITE_BOOK($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["UserID"];
		
		$sql = "
				SELECT BookID 
				FROM 
				INTRANET_ELIB_BOOK_MY_FAVOURITES 
				WHERE 
				BookID = {$BookID} AND UserID = {$currUserID}
			   ";
		
		$returnArr = $this->returnArray($sql, 1);
		
		if(count($returnArr) > 0)
		return true;
		else
		return false;
		
	} // end function
 	
 	
 	//Modified by Josephine
	function newPrintMostActiveReviewers($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;
		
		$lang = $_SESSION["intranet_session_language"];
		
		//$x = "<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">";
		
		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			//$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];
			
			if($ClassName != "")
			$ClassName = "(".$ClassName.")";
			
			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;
					
				$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
				$lang1 = $UserName_b5;
			
				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}
			
			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;
			
			if(trim($UserName) == "")
			$UserName = "--";
			
			$rank = $i + 1;
			
			if($rank <= 5)
			$rankBG = "top_no_".$rank."a.gif";
			else
			$rankBG = "top_no_b.gif";
			
			if($rank == 1){
				$rank = $rank."st";
			}else if($rank == 2){
				$rank = $rank."nd";
			}else if($rank == 3){
				$rank = $rank."rd";
			}else if($rank == 4 || $rank == 5){
				$rank = $rank."th";
			}
			
			$x .= "<div class=\"left01\">" .
						"<p class=\"text01\">" .
						"<strong>".$rank."&nbsp;"."</strong>" .
						"<a href=\"#\" onClick=\"MM_openBrWindowFull('elib_top_reviewers.php?ReviewerID=$currUserID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\">".$UserName." ".$ClassName."</a>" .
						"</p>" .
					"</div>";
					
			$x .= "<div class=\"review_icon\">" .
						"<label title=\"No.of Review\">".
						"<p>".$Count."</p>".
						"</label>".
					"</div>";
					
			$x .= "<div class=\"line\"></div>";
	
		}
		
		return $x;
	} // end function print Most active reviewers
	
	//Josephine
	
	//Modified by Josephine 2009/10/29
	function newPrintBookTable($data="", $image_path="", $LAYOUT_SKIN="", $NumOfBook="", $type="", $eLib="")
	{
		global $PATH_WRT_ROOT;
		
		# Init variables
		$count = 0;
		$countRowBlock = 0;
		
		
		
		# Calculate the book image dimensions
		switch($NumOfBook){
			case 4:				
				$width 		= ($type=="recommend")? 80 : 120;
				$height 	= ($type=="recommend")? 100: 155;
				$maxRow 	= 2;
				break;
			case 9:			
				$width 		= ($type=="recommend")? 40 : 80;
				$height 	= ($type=="recommend")? 60: 100;
				$maxRow 	= 3;				
				break;
			default:
				$width 		= 200;
				$height 	= 285;
				$maxRow 	= 1;
				break;
		}
		
		# Number of books per row
		 
		$tdWidth = round(100/$maxRow);
		
		# check if recommend table
		if(count($data) <= 0 && $type == "recommend")
		{
			$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr><td align=\"center\" class=\"tabletext\">";
			$x .= "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
			$x .= $eLib["html"]["no_recommend_book"];
			$x .= "</td></tr>";
			$x .= "</table>";
			
			return $x;
		}
		
		# Generate Book display
		$x .= '<table width="100%" border="0"><tr>';
		
		for($j = 0; $j < $NumOfBook ; $j++)
		{		
			$BookName 		= $data[$count]["Title"];
			$BookID 		= $data[$count]["BookID"];
			$Description 	= $data[$count]["Description"];				
			$count++;
			$bookPath 		= "/file/elibrary/content/".$BookID."/image/cover.jpg";
			
			# Set Book Cover Image path
			$BookCover = ($BookID != "" && file_exists("../../..".$bookPath))? $bookPath : $image_path."/".$LAYOUT_SKIN."/eLibrary/cover_no_image.jpg";
			
			# HTML BookImage + Name
			$x .= '<td width = "'.$tdWidth.'%">';
			if($type == "recommend")
			{
				if($BookID != ""){
					
					$x .= "<p>
							<label title='$BookName'>
							<a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\" onMouseover=\"MM_showHideLayers('recommend_detail_$count','','show'); MM_showRecommendedBook($j)\" onMouseout=\"MM_showHideLayers('recommend_detail_$count','','hide') \"><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></a>
							</label>
							</p>";
				}					
			}
			else
			{
				if($BookID != ""){
		
					$x .= "<div style='width:100%;'><a href=\"#\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\"><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></a><br/><span><a href=\"#\">".$BookName."</a></span></div>";
				}
			}
			$x .= '</td>';
			
			  
			$countRowBlock++;
			# Next line 
			if($countRowBlock >= $maxRow){
				$countRowBlock = 0;
				$x .= '</tr><tr>';
			}			
		} // end for cell
 
		
		#Close table row if not Already CLOSED
		if($countRowBlock != 0){		
			$x .= '</tr>';
		}
		$x .= "</table>";
		
		return $x;
	} // end function print table by Josephine
	
	//modified by Josephine
	function newPrintMostUsefulReviews($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;
		
		//hdebug_r($data);
		
		$lang = $_SESSION["intranet_session_language"];
		
		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$ReviewID = $data[$i]["ReviewID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en2 = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];
			//$BookName = iconv("UTF-8","BIG-5",$data[$i]["Title"]);
			$BookName = $data[$i]["Title"];
			
			if($ClassName != "")
			{
				if($ClassNumber != "")
				$ClassName = "(".$ClassName."-".$ClassNumber.")";
				else
				$ClassName = "(".$ClassName.")";
			}
			
			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;
					
			$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
			$lang1 = $UserName_b5;
			
				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}
			
			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;
			
			if(trim($UserName) == "")
			$UserName = "--";
			
			$rank = $i + 1;
			
			if($rank <= 5)
			$rankBG = "top_no_".$rank."b.gif";
			else
			$rankBG = "top_no_b.gif";
			
			if($rank == 1){
				$rank = $rank."st";
			}else if($rank == 2){
				$rank = $rank."nd";
			}else if($rank == 3){
				$rank = $rank."rd";
			}else if($rank == 4 || $rank == 5){
				$rank = $rank."th";
			}
		
			
			$x .= "";
			$x .= "<div class=\"left02\">" .
						"<p class=\"text01\">" .
							"<strong>".$rank."&nbsp;"."</strong><a href=\"#\" onClick=\"MM_openBrWindowFull('elib_top_reviews.php?ReviewID=$ReviewID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\">".$BookName."</a>" .
						"</p>" .
					"</div>".
					
					"<div class=\"right\">" .
						"<span class=\"text02\">".$UserName."  ".$ClassName."</span>" .
					"</div>".
					
					"<div class=\"line\"></div>";
		
		}
		
		return $x;
	} // end function print Most active reviewers
	
	function Get_List_Read_History($BookID){
		global $lang;
		
		$selName1 = "CONCAT('<a href=\"elib_public_students_read_summary.php?UserID=', a.UserID ,'&ClassName=', a.ClassName,'\">' , if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), a.ChineseName , a.EnglishName), '</a>') as UserName";
		$selName2 = "CONCAT('<a href=\"elib_public_students_read_summary.php?UserID=', a.UserID ,'&ClassName=', a.ClassName,'\">' , if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), a.EnglishName, a.ChineseName), '</a>') as UserName";
		$selName = ($lang == "b5")? $selName1 : $selName2;
		
		$sql = "SELECT
					$selName,
					a.UserID
				FROM 
					INTRANET_USER as a
				INNER JOIN 
					INTRANET_ELIB_BOOK_HISTORY as h
				ON
					a.UserID = h.UserID
				WHERE
					h.BookID = ".$BookID;
		echo htmlspecialchars($sql);
		return $this->db_db_query($sql);
	}
	
	/**
	 * Get the number of helpful review count UI
	 * E.g   0 of 0 people found this Review helpful
	 */
	function getBookReviewHelpfulCountUI($ReviewID){	
		global $eLib;	
		
		$voteArr = $this->getHelpfulVote($ReviewID);			
		return $this->getHelpfulMessage($eLib, $voteArr["total"], $voteArr["yesNum"]);
	}
 	
} // end class

?>