<?php
## Using By : 
## Please edit with UTF-8 萬國碼  encoding because it contains special symbols
###################################################################################
## Modification Log
## 2020-10-30 Ray     - modified calculateContinuousAbsentStudent, waive not count as absent
## 2020-10-22 Ray     - modified Get_No_Tapping_Card_Report_Data, delete NonSchoolDay record
## 2020-07-07 Ray     - added createCountSchoolDayFunction
## 2020-06-24 Ray     - added getDateTimeTableID, getDateSessionCount
## 2020-06-17 Ray	  - added Import_Preset_AbsenceTW, Finalize_Import_Preset_AbsenceTW
## 2020-06-04 Ray	  - added checkPresetLeaveOverlap, updateLessonAttendance
## 2020-06-04 Ray     - Build_Subject_Group_Student_Attendance, Get_Lesson_Attendance_Record,Save_Lesson_Attendance_Data added LeaveTypeID
## 2020-06-03 Ray	  - modified Get_Class_Lesson_Attendance_Overview2 add SessionType
## 2020-05-29 Ray	  - added getBodyTemperatureRecords GetYKHQuery, GetYKHStudentQuery
## 2020-05-28 Ray	  - added ViewBodyTemperatureStatus
## 2020-05-27 Ray	  - modified getBodyTemperatureRecords add GetYKHQuery, TemperatureStatus
## 2020-02-20 Ray     - modified Get_No_Tapping_Card_Report_Data check with CARD_STUDENT_LUNCH_ALLOW_LIST
## 2020-02-07 Ray     - added GetStudentAttendanceLeaveType, IsStudentAttendanceTypeExist, UpsertStudentAttendanceType, DeleteStudentAttendanceType
## 2020-01-14 Ray	  - modified GET_MODULE_OBJ_ARR PowerClass eAdmin-LessonAttendance permission check
## 2019-12-16 Ray	  - added getStudentLastHostelAttendanceRecords
## 2019-12-12 Ray     - added settings AllowClassTeacherManagePresetStudentAbsence
## 2019-12-09 YatWoon - modified Remove_Whole_Day_Data(), add Target Class
## 2019-11-27 Ray	  - modified getSummaryCount, getSummaryDetail not check student without classes
## 2019-11-19 Ray     - add $sys_custom['StudentAttendance']['HostelAttendance_Reason'] in getHostelAttendanceRecords, createTableHostelAttendanceDailyLog
## 2019-11-06 Carlos  - fixed Get_No_Tapping_Card_Report_Data() wrong checking on session type and corresponding time field issue.
## 2019-08-09 Ray	  - add InsertSubmittedAbsentLateRecord, session from/to
## 2019-07-26 Ray     - added student attendance rate report, modified Get_Subject_Group_Attendance_List_Without_Timetable add subjectID order
## 2019-06-19 Ray     - add $MenuArr["OtherFeatures"]["Child"]["PresetTeacherRecords"], UpsertTeacherRemarkPresetRecords, GetTeacherRemarkPresetRecords, DeleteTeacherRemarkPresetRecords
## 2019-05-21 Philips - modified function Set_Profile() to add seriousLate column for $sys_custom['StudentAttendance']['SeriousLate']
## 2019-03-01 Cameron - add function getStudentApplyLeaveRemark()
## 2019-02-15 Carlos  - modified Get_Class_Lesson_Attendance_Overview2(), left join INTRANET_USER to get last modified user names.
## 2019-02-13 Cameron - add function checkStudentApplyLeave() to retrieve list of students that have applied leave on specific day section
## 2018-12-19 Carlos  - modified Get_No_Tapping_Card_Report_Data(), do not count offline imported records with checking on bad tap card records.
## 2018-12-13 Isaac   - modified GET_MODULE_OBJ_ARR(), added [Import Smart Card ID] menu item to powerclass 
## 2018-11-23 Carlos  - [ip.2.5.9.11.1#2] modified GET_MODULE_OBJ_ARR(), added checking of $sys_custom['eClassApp']['enableReprintCard'] to display Reprint Card link.
## 2018-10-15 Carlos  - [ip.2.5.9.11.1] modified timeToSec($_time), added empty checking to the time pass in.
## 2018-10-10 Carlos  - [ip.2.5.9.11.1] modified Get_No_Tapping_Card_Report_Data(), for leave time, exclude absent/outing status attendance records. 
## 2018-08-03 Carlos  - [ip.2.5.9.11.1] added setting [iSmartCardDisableModifyStatusAfterConfirmed]. 
##										modified createTable_Card_Student_Daily_Log() added fields [AMConfirmedByAdmin] and [PMConfirmedByAdmin].
## 2018-05-07 Carlos  - modified GET_MODULE_OBJ_ARR() added [Continuous Absence Alert].
## 2018-05-02 Carlos  - modified getHostelAttendanceRecords($year, $month, $map), added filter option UserStatus to filter INTRANET_USER.RecordStatus
## 2018-04-30 Carlos  - added calculateContinuousAbsentStudent() to find out the continuous absent dates for a student.
## 2018-03-12 Carlos  - fixed Get_Expected_Reason() default outing reason condition.
##					  - added setting TeacherCanManageProveDocumentStatus.
## 2018-02-21 Carlos  - modified prepareHKUSPHFluData(), fine tune submission method.
## 2018-02-06 Carlos  - modified prepareHKUSPHFluData(), added $sys_custom['HKUSPH_SubmissionMethodByTimePeriod'] to change submission_method during certain time periods.
## 2018-01-31 Carlos  - fixed retrieveStudentMonthlyRecord() get AM/PM outing reasons.
## 2018-01-03 Isaac   - modified getHostelAttendanceRecords($year, $month, $map) which return also ClassName & ClassNumber.
## 2017-12-19 Carlos  - [ip.2.5.9.1.1] modified prepareHKUSPHFluData(), added $sys_custom['HKUSPH_SubmissionMethodByDateRange'] feature.
## 2017-11-14 Carlos  - [ip.2.5.9.1.1] $sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] - added getStudentLateAbsentCountingRecords($filterMap)
## 2017-10-26 Carlos  - [ip.2.5.9.1.1] added getAttendanceGroupRecords(), getAttendanceGroupAdminUsers(), modified GET_MODULE_OBJ_ARR() added [Group Responsible Users] menu item.
## 2017-10-04 Carlos  - [ip2.5.8.10.1] $sys_custom['StudentAttendance']['HostelAttendance'] - Added getHostelAttendanceAdminUsers()
## 2017-08-11 Carlos  - [ip2.5.8.10.1] $sys_custom['StudentAttendance']['HostelAttendance'] - added createTableHostelAttendanceDailyLog($year="",$month=""), getHostelAttendanceGroupsConfirmedRecords($map), 
##										upsertHostelAttendanceGroupConfirmRecord($map), getHostelAttendanceRecords($year, $month, $map), upsertHostelAttendanceRecord($year,$month,$map).
## 2017-07-11 Carlos  - [ip2.5.8.7.1] added getHKUSPHParentConsentRecords($filterMap) and getHKUSPHParentConsentCsvHeaderMap() for HKU Disease Surveillance.
## 2017-06-22 Carlos  - [ip2.5.8.7.1] added destructor to log down when and whom do update or remove action under Student Attendance module path.
## 2017-05-31 Carlos  - [ip2.5.8.7.1] added getDailyLogRecords($filterMap) for getting one day attendance daily log records.
## 2017-05-26 Carlos  - [ip2.5.8.7.1] $sys_custom['StudentAttendance']['RecordBodyTemperature'] added getBodyTemperatureRecords($filterMap). Modified GET_MODULE_OBJ_ARR() added [Other Features > Body Temperature Records].
## 2017-05-17 Carlos  - [ip2.5.8.7.1] $sys_custom['StudentAttendance']['NoCardPhoto'] added getNoCardPhotoRecords($filterMap).
## 2017-05-04 Carlos  - [ip2.5.8.7.1] modified Get_No_Tapping_Card_Report_Data(), fix leave time checking with non-outing and null AM/PM status issue.
## 2017-04-13 Carlos  - [ip2.5.8.4.1] modified Set_Profile(), fields with value null are skipped to be updated.
## 2017-04-03 Carlos  - [ip2.5.8.4.1] modified sendHKUFluData() and prepareHKUSPHFluData() send half month data logic.
## 2017-03-27 Carlos  - [ip2.5.8.4.1] modified Set_Profile(), clear leave status if absent or outing.
## 2017-03-21 Carlos  - [ip2.5.8.4.1] modified updateTeacherRemark() to remove record if remark is empty value in order to reduce empty record quantity in DB.
## 2017-02-24 Carlos  - [ip2.5.8.3.1] added log($log_row) for logging status change.
## 2017-02-22 Carlos  - [ip2.5.8.3.1] modified getNotSumttedDocumentStudentDataSQL(), excludes waived, outing records by checking ProfileRecordID > 0. 
## 2016-12-19 Carlos  - [ip2.5.8.1.1] $sys_custom['StudentAttendance']['SyncDataToPortfolio'] - added SyncProfileRecordToPortfolio($studentId, $recordDate, $dayType, $recordType), modified Set_Profile(), Clear_Profile().
## 2016-12-14 Carlos  - [ip2.5.8.1.1] $sys_custom['StudentAttendance']['CustomizedTimeSlotSettings']
##									  - Added UpsertCustomizedTimeSlotRecords($map), GetCustomizedTimeSlotRecords($filterMap), DeleteCustomizedTimeSlotRecords($recordId)
##									  - Modified GET_MODULE_OBJ_ARR() to add [Customized Time Slot Settings].
## 2016-12-05 Carlos  - [ip2.5.8.1.1] Modified Get_No_Tapping_Card_Report_Data() to use mysql temporary table, not real table for storing report result set. And cater leave time outing issue.
## 2016-11-22 Carlos  - [ip2.5.8.1.1] Add parameter to sendHKUFluData() to control send one day data or weekly data.
## 2016-11-16 Omas	  - [ip2.5.8.1.1] Modified getNotSumttedDocumentStudentDataSQL() - fixing showing <br> problem
## 2016-10-18 Carlos  - [ip2.5.7.10.1] Added basic setting [DisallowNonTeachingStaffTakeAttendance].
## 2016-09-23 Bill	  - [ip2.5.7.10.1] modified GET_MODULE_OBJ_ARR(), added [Attendance Infomation Report] [2016-0704-1456-16054]
## 2016-08-31 Carlos  - [ip2.5.7.10.1] modified GET_MODULE_OBJ_ARR(), added [HKUSPH Data Submission Settings].
## 2016-08-30 Carlos  - [ip2.5.7.10.1] added sendHKUFluData($TargetDate).
## 2016-08-30 Carlos  - [ip2.5.7.10.1] modified createTable_Card_Student_Entry_Log() and Record_Raw_Log(), added tap card location.
## 2016-08-29 Henry HM- [ip2.5.7.10.1] Added $MenuArr["Report"]["Child"]["HKUSPH"];
## 2016-08-26 Bill	  - [ip2.5.7.10.1] modified getNotSumttedDocumentStudentDataSQL(), display Processed in column Unsubmmited Date(s) if record is processed by school
## 2016-08-16 Carlos  - [ip2.5.7.10.1] $sys_custom['LessonAttendance_LaSalleCollege'] - modified GetLessonAttendanceSettingsList(), UpsertLessonAttendanceLeaveRecords(), GetLessonAttendanceLeaveRecords(), DeleteLessonAttendanceLeaveRecords(), Get_Subject_Group_Session(), Get_Student_List(), GetLessonAttendanceRecords().
## 2016-07-05 Carlos  - [ip2.5.7.7.1] added updateAttendanceInTime() for editing daily log in times.
## 2016-06-30 Carlos  - [ip2.5.7.7.1] modified getNotSumttedDocumentStudentDataSQL(), added parameter record_type to support both absent and early leave record type.
## 2016-06-24 Carlos  - [ip2.5.7.7.1] modified Import_Preset_Outing_Record_To_Temp() and Finalize_Import_Preset_Outing(), add fields [LeavesAt],[Location] and [Remark].
## 2016-06-15 Carlos  - [ip2.5.7.7.1] fixed Get_Search_Report_Data(), missing absent records when there are outing records in the same day. 
## 2016-06-08 Carlos  - [ip2.5.7.7.1] modified IsLessonAttendanceCustomizationForLaSalle(), LessonAttendanceStatusMapping(), Build_Subject_Group_Student_Attendance(), GET_MODULE_OBJ_ARR(), added lesson attendance reason settings and new reports.
## 2016-03-15 Carlos  - [ip2.5.7.4.1] modified Get_Lesson_Non_Confirmed_Report_Records(), changed to left join subject group teacher.
## 2016-03-11 Carlos  - [ip2.5.7.4.1] $sys_custom['StudentAttendance']['PMStatusFollowLunchSetting'] modified Get_PM_Expected_Field(), customized to make students that do not go out for lunch 
##						to set PM status to follow AM status because these students do not tap card in lunch time to trigger the Pm in status. 
## 2016-03-11 Carlos  - fixed Get_Search_Report_Data() to sort class number as number order with usort().
## 2016-02-04 Carlos  - modified Get_Search_Report_Data(), added Outing attendance type. 
## 2016-02-02 Kenneth -	modified $MenuArr["SystemSetting"]["Child"]["AppTemplateSettings"], delete paras 'Section'
## 2016-01-25 Bill	  - [ip2.5.7.3.1] modified Set_Profile(), to fix double late records when manual input late time	[2016-0121-1051-53206] 
## 2016-01-22 Carlos  - [ip2.5.7.3.1] modified Get_No_Tapping_Card_Report_Data(), call createTable_Card_Student_Raw_Log($year,$month) for each month to cater no tap card no table situation.
## 2015-12-14 Carlos  - [ip2.5.7.1.1] modified Notify_Student_Status(), added checking for sedning push message on non-school days.
## 2015-12-11 Carlos  - [ip2.5.7.1.1] modified Delete_eDis_Late_Record(), pre remove late profile record before deleting eDis late misconduct record, and 
##									also remove the eDis late misconduct records that it has StudentAttendanceID but the related PROFILE_STUDENT_ATTENDANCE does not exist anymore. 
## 2015-11-17 Ivan 	  - [ip2.5.7.1.1] [P88366] modified Notify_Student_Status() to add settings to send arrival and/or leave push message
## 2015-10-27 Carlos  - [ip2.5.6.12.1] modified Get_Search_Report_Data(), get LeaveSchoolTime for early leave status.
## 2015-10-19 Carlos  - [ip2.5.6.12.1] modified Get_No_Tapping_Card_Report_Data(), left join CARD_STUDENT_RAW_LOG_YYYY_MM to filter no tap card records. 
## 2015-10-14 Omas	  - [ip2.5.6.10.1] modified Set_Profile(),getNotSumttedDocumentStudentDataSQL() - Add Document status extended logic.. automatic sync AM & PM Document status, add * remarks to report
## 2015-09-30 Carlos  - [ip2.5.6.10.1] modified Get_Attendance_Cycle_Day_Info() added special time table checking. 
##									   added Get_Lesson_Special_Time_Table($TargetDate) for getting special time table id.
##									   modified Get_Class_Lesson_Attendance_Overview2(), Clone_Class_Lesson_Attendance_Overview_And_Students(), 
##									   Get_Lesson_Non_Confirmed_Report_Records() fixed can handle sepcial time table. 
## 2015-09-23 Carlos  - [ip2.5.6.10.1] modified getRawLogRecords(), fetch data RecordStation.
## 2015-09-21 Carlos  - [ip2.5.6.10.1] added Is_Lesson_Plan_Outdated() for checking is lesson attendance records were updated by others after loaded the take attendance page.
## 2015-09-18 Carlos  - [ip2.5.6.10.1] added GetLibGeneralSettings() and Get_General_Settings(). modified Get_Student_List() to follow default status and follow school attendance status.
## 2015-09-07 Carlos  - [ip2.5.6.10.1] $sys_custom['LessonAttendance_SortBySubjectGroupClass']: customize the record order of Get_Class_Lesson_Attendance_Overview2() and Get_Lesson_Non_Confirmed_Report_Records(). 
##						(Requested by S.D.B. Ng Siu Mui Secondary School)
## 2015-07-21 Ivan	  - [ip2.5.6.8.1] modified Notify_Student_Status() to show student english name in english push message and chinese name in chinese push message 
## 2015-07-14 Carlos  - [ip2.5.6.8.1] modified retrieveSettings(), added settings $EditDaysBeforeRecord and $EditDaysBeforeRecordSelect
## 2015-07-09 Carlos  - [ip2.5.6.8.1] modified Get_Search_Report_Data(), added data DocumentStatus.
## 2015-06-19 Shan    - [ip2.5.6.8.1] added Clear_Websams_Reason_Temp(),Get_Websams_Reason_Temp(),Insert_Websams_Reason_Temp(),Add_Websams_Reason();
## 2015-06-18 Evan	  - [ip2.5.6.8.1] modified getImportOutingReasonHeader()
## 2015-06-18 Evan	  - [ip2.5.6.8.1] added Check_OutingReason_Redundancy()
## 2015-06-17 Evan	  - [ip2.5.6.8.1] added getOutingReasonImportTempData(),insertOutingReasonImportTempData(),deleteOutingReasonImportTempData()
## 2015-06-16 Evan	  - [ip2.5.6.8.1] added getImportOutingReasonHeader(), getImportOutingReasonColumnProperty()
## 2015-06-11 Omas	  - [ip2.5.6.8.1] added isStudentAttendance30(), modified getTimeArray(), getGroupTimeArray(), getClassTimeArray() in SA3.0 time format show as hh:mm:ss
## 2015-05-27 Carlos  - [ip2.5.6.8.1] modified Get_Search_Report_Data(), added data field OfficeRemark
## 2015-03-30 Omas    - [ip2.5.6.5.1] modified getNotSumttedDocumentStudentDataSQL() - fix chinese UI show english name
## 2015-03-23 Carlos  - [ip2.5.6.5.1] modified getRawLogRecords() sort records by day,class,class number,record time.
## 2015-03-18 Carlos  - [ip2.5.6.5.1] fix Set_Profile(), auto apply preset absence submit prove document status for new absence profile record
## 2015-02-11 Carlos  - [ip2.5.6.3.1] added updateTeacherRemark(), updateOfficeRemark()
## 2015-02-09 Carlos  - [ip2.5.6.3.1] modified Set_Profile() - fix auto apply preset absence waive option
## 2015-01-20 Carlos  - [ip2.5.6.3.1] modified Remove_Whole_Day_Data(), remove CARD_STUDENT_STATUS_SESSION_COUNT, CARD_STUDENT_ENTRY_LOG_YYYY_MM and CARD_STUDENT_RAW_LOG_YYYY_MM records
## 2015-01-02 Carlos  - [ip2.5.6.1.1] modified Set_Profile() whole day with lunhc mode auto bring AM outing reason to PM
## 2014-12-12 Omas	  -	[ip2.5.5.12.1] added getNotSumttedDocumentStudentDataSQL(), add MenuArr Child - AppTemplateSetting , AbsentProveReport
## 2014-12-11 Carlos  - [ip2.5.5.12.1] modified retrieveStudentMonthlyRecord(), separate early leave reason record from late and absent records to avoid duplicated records
## 2014-12-10 Roy	  - [ip2.5.5.12.1] modified retrieveStudentMonthlyRecord(), add parameter $attendStatusDefaultAbsent
## 2014-12-09 Omas	  - [ip2.5.5.12.1] modified Finalize_Import_Preset_Absence(),Import_Preset_Absence() to have a new field document status for hand-in prove document status
## 2014-11-20 Carlos  - [ip2.5.5.12.1] modified Set_Profile(), use default reason if profile is new and no reason given
## 2014-11-12 Carlos  - [ip2.5.5.12.1] modified Set_Profile(), if already has absent/late record and Waive field is empty or 0, set to 0 to make the profile record confirmed
## 2014-11-06 Roy	  - [ip2.5.5.12.1] modified Notify_Student_Status() to apply checking of sending push message or not 
## 2014-10-22 Roy	  - [ip2.5.5.10.1] modified Notify_Student_Status() for new push message wordings
## 2014-10-21 Carlos  - [ip2.5.5.10.1] modified Set_Profile() added auto update non-confirmed PM absent reason from AM absent reason
## 2014-10-07 Carlos  - [ip2.5.5.10.1] added createTable_Card_Student_Raw_Log($year="",$month=""), addCardStudentRawLogRecord($userId, $year, $month, $day, $time), getRawLogRecords()
##					  - modified GET_MODULE_OBJ_ARR() add menu item [Report > Entry Log]
## 2014-09-23 Carlos  - added Get_Subject_Group_Attendance_List_Without_Timetable($TargetDate,$DayType='',$SubjectGroupId='',$SubjectId='')
##						modified Check_LateAbsentEarlyClassGroup_Confirm_Status($DayType,$RecordType,$TargetDate="") subject group do not follow time table
##						use $sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'] can revert to original design that follow time table.
## 2014-09-03 Carlos  - modified Set_Profile(), bring PM absent reason from AM absent reason if PM profile record is first time generated
## 2014-07-11 Carlos  - modified retrieveSettings(), add setting CannotTakeFutureDateRecord
## 2014-07-07 Carlos  - fix getSummaryDetail(), lunch status counting has retrieve wrong lunch settings
## 2014-07-04 Carlos  - modified Set_Profile(), if first time confirm attendance records as absent and have preset absent reason, auto assign the preset reason to absence profile record
## 2014-06-23 Carlos  - updated getWaiveAbsenceRecords() and updateWaiveAbsenceRecords()
## 2014-03-26 Carlos  - modified Set_Profile(), default set $Waive as '0' if pass in 'NULL' and no profile yet
## 2014-03-18 Carlos  - added getWaiveAbsenceRecords() and updateWaiveAbsenceRecords()
## 2014-03-13 Carlos  - added getDefaultNumOfSessionSettings()
## 2014-02-19 Carlos  - modified retrieveStudentMonthlyRecord(), added AMWaive and PMWaive fields
## 2014-02-14 Ivan	  - modified GET_MODULE_OBJ_ARR() to add Apply Leave settings for eClass App
## 2014-01-28 Carlos  - modified Get_Search_Report_Data(), get Remark data
## 2014-01-21 Carlos  - fix Get_Subject_Group_Session() and Get_Subject_Group_Attendance_List() handle special time table
## 2014-01-09 Carlos  - add debug log flag $sys_custom['hku_flu_log'] to sendMedicalReport()
## 2013-12-31 Ivan    - modified Notify_Student_Status() to add eClassApp push message logic
## 2013-11-18 Carlos  - fix Get_Subject_Group_Attendance_List() get wrong subject groups problem
## 2013-11-14 Carlos  - fix Remove_Whole_Day_Data() fail to remove early leave profile records bug
## 2013-11-06 Carlos  - added retrieveGroupDayData() for [Class Daily Attendance Report] 
## 2013-11-06 Carlos  - modified Get_Student_List_By_Class_Academic_Year(), rewrite query to fetch students
## 2013-10-03 Carlos  - modified Get_Student_By_Classes(), add RecordType=2 to query
## 2013-08-23 Siuwan  - hide some fields in GET_MODULE_OBJ_ARR() if platform = KIS
## 2013-08-19 Henry   - change the path of Daily Lesson Status to dailyoperation/daily_lesson_attendance/index2.php
## 2013-08-16 Henry   - added Get_Time_Slot_List2()
## 2013-08-12 Carlos  - modified GET_MODULE_OBJ_ARR(), added [PageLessonAttendance_ImportLessonAttendanceRecords]
## 2013-08-12 Henry   - added Get_Class_Lesson_Attendance_Overview2 for the new layout of Lesson Attendance Status
## 2013-08-07 Henry   - added functions from the liblessonattendance.php
## 2013-08-06: Carlos - added Clone_Class_Lesson_Attendance_Overview_And_Students(), Get_Lesson_Non_Confirmed_Report_Records()
## 2013-08-05: Henry  - added parameter "StudentID" to Get_Lesson_Summary_Report_Data()
## 2013-08-02: Henry  - Modified Get_Lesson_Summary_Report_Data()
## 2013-05-14: Carlos - Add [View Individual Subject Group List] page 
##						added Get_Subject_Group_Attendance_List(), createTable_Card_Student_Daily_Subject_Group_Confirm()
##						modified createTable_LogAndConfirm(), Check_LateAbsentEarlyClassGroup_Confirm_Status(), GET_MODULE_OBJ_ARR()
## 2013-05-14: Carlos - modified sendMedicalReport(), 1) use English as report language; 2) besides sending email, also post data to HKU 
## 2013-05-06: Carlos - modified sendMedicalReport(), remove "-f envelope-from" because it causes "X-Authentication-Warning" and bounce to sender
## 2013-04-25: Carlos - added getAttendanceStatusDataByAcademicYear() to get attendance status couting by academic year date range
##						modified retrieveStudentMonthlyRecord(), added parameter $get_detail to get AM/PM reason and remark
## 2013-02-20: Yuen   - improve sendMedicalReport() to use appropriate email sender in order to send email out successfully.
## 2013-02-05: Carlos - modified isDataOutdated() removed confirmed user condition, update time need to absolutely later than last tap card time.
## 2013-01-14; Carlos - fix Set_Profile() lost Profile reason and remark problem if user reconfirm status data
## 2013-01-07: Carlos - modified generateMedicalReasonSummary() to base on $i_MedicalReasonName to generate report
## 2012-12-27: Carlos - added Get_Preset_Absent_Student_Records() and Get_School_Holiday_Events() for class monthly attendance report
## 2012-10-04: YatWoon - modified GET_MODULE_OBJ_ARR(), update "Data Export" menu [Case#2012-0927-1629-06132]
## 2012-10-03: Carlos - modified Check_LateAbsentEarlyClassGroup_Confirm_Status() to filter records <= today
## 2012-09-10: Carlos - added Get_Entry_Leave_Export_Records(), modified Delete_Entry_Period($RecordID)
## 2012-07-18: Rita - add retrieveGroupMonthDataByDateRange(), retrieveClassMonthDataByDateRange(), retrieveCustomizedIndividualMonthDataByDateRange()
## 2012-07-10: Carlos - modified all queries that order by ClassNumber by casting to number for sorting
## 2012-05-02: Carlos - changed Get_Subject_Group_Session() now can get both regular time table and special time table
## 2012-04-17: Carlos - modified Get_PM_Expected_Field(), if PM status follow AM status and AM is Outing, then PM will be Outing
## 2012-03-22: Carlos - added setting OnlySynceDisLateRecordWhenConfirm
##						added parameter $eDisUpdate at Set_Profile(), Clear_Profile(), Auto_Set_PM_Status() to control update of eDiscipline late record
## 2012-03-21: Carlos - modified Notify_Student_Status() set SchoolID from $asl_parent_app['SchoolID'] which must use school domain address, IP address does not work
## 2011-12-21: Carlos - modified Get_No_Tapping_Card_Report_Data() status field support array type
## 2011-12-19: Carlos - modified Get_Student_List_By_Class_Academic_Year() append data RecordStatus and IsArchivedUser to the return array
## 2011-12-16: Carlos - modified createTable_Card_Student_Daily_Log() added fields PMDateModified and PMModifyBy
##					  - modified Get_AM_Expected_Field() put checking on InSchoolTime to last evaluation
## 2011-12-15: Carlos - modified retrieveStudentMonthlyRecord() added filter to only display school days record for student and parent
## 2011-12-15: Carlos - modified GET_MODULE_OBJ_ARR() added menu item [Remove past data]
## 2011-12-14: Carlos - added Remove_Whole_Day_Data()
## 2011-11-14: Carlos - modified retrieveTopBadRecordsCountByStudent(), added checking of INTRANET_USER RecordType = 2 to filter students because Staff can have no card bad action
## 2011-11-08: Carlos - modified Check_LateAbsentEarlyClassGroup_Confirm_Status() fix cache of all non-confirm classes
## 2011-11-03: Carlos - fixed getClassListToTakeAttendanceByDate(), pls only use english class name as value, use chinese name for display only
#	Date	:	2011-09-21 Henry Chow
#				modified Update_Template(), Get_Template_Detail(), Get_eNotice_Template_By_Category()
#				retrieve "SendReplySlip" 
##
## 2011-09-20: YatWoon - update getClassListToTakeAttendanceByDate(), getGroupListMode(), retrieve class in eng/chinese
## 2011-09-01: Carlos - Add Notify_Student_Status() for ASL Parent App
## 2011-07-04: Carlos - Add constant BLANKDATA for padding blank cached confirmation data;
##					  - Modified Check_LateAbsentEarlyClassGroup_Confirm_Status() to insert blank cached confirmation data
## 2010-10-05: Kenneth Chung - Changed Check_LateAbsentEarlyClassGroup_Confirm_Status, joined INTRANET_USER for late/absent/early leave confirm color table to ignore profile record for archived student
## 2010-10-05: Kenneth chung - changed isRequiredToTakeAttendanceByDate/ groupIsRequiredToTakeAttendanceByDate to sync the logic with Get_Class_Attend_Time_Setting/ Get_Group_Attend_Time_Setting
## 2010-09-27: Kenneth Chung - change the file encoding to UTF editing for setting $this->DefaultAttendanceSymbol[CARD_STATUS_LATE]
## 2010-09-09: Carlos - Add constant CARD_STUDENT_MAX_SESSION
## 2010-05-06: Carlos - Add LateSession to retrieveClassMonthData() and retrieveGroupMonthData() 
## 2010-04-20: Henry - add updateAttendanceRecordReason(), update table CARD_STUDENT_PROFILE_RECORD_REASON
## 2010-03-25: Henry - add retrievePastDateByDateCount(), countSchoolDayDiff(), counting date diff (only count school day without holiday)
## 2010-03-24: Henry - add menu item 'Daily Operation > Absent Without Parent Letter'
## 2010-03-18: Kenneth Chung - Add menu item 'Setting > iSmartCard Teacher take own class attend only' 
## 2010-03-15: Kenneth Chung - Add menu item 'Setting > Student Entry - Leave Date' 
## 2010-02-12: Carlos - Add menu item 'Daily Absent Analysis Report' 
## 2010-02-04: Max (201001221711)
## - Add functions 
## 		getSelectionTableGroupMode(),
## 		getSelectionTableClassMode(),
## 		generateSelectionTableFormat(),
## 		getGroupAttendanceSelectionBox(),
## 		getClassAttendanceSelectionBox(),
## 		getBundleChangeBox()
###################################################################################
include_once('libclass.php');
include_once('libgeneralsettings.php');

## 20071005 - Kenneth : Add create lunch box option

if (!defined("LIBCARDSTUDENTATTEND2_DEFINED"))                  // Preprocessor directives
{
	define("LIBCARDSTUDENTATTEND2_DEFINED",true);
	
	# Constant definition
	define("CARD_STATUS_PRESENT",0);
	define("CARD_STATUS_ABSENT",1);
	define("CARD_STATUS_LATE",2);
	define("CARD_STATUS_OUTING",3);
	define("CARD_LEAVE_NORMAL",0);
	define("CARD_LEAVE_AM",1);
	define("CARD_LEAVE_PM",2);
	define("CARD_BADACTION_LUNCH_NOTINLIST",1);
	define("CARD_BADACTION_LUNCH_BACKALREADY",2);
	define("CARD_BADACTION_FAKED_CARD_AM",3);
	define("CARD_BADACTION_FAKED_CARD_PM",4);
	define("CARD_BADACTION_NO_CARD_ENTRANCE",5);
	define("PROFILE_DAY_TYPE_WD",1);
	define("PROFILE_DAY_TYPE_AM",2);
	define("PROFILE_DAY_TYPE_PM",3);
	define("PROFILE_TYPE_ABSENT",1);
	define("PROFILE_TYPE_LATE",2);
	define("PROFILE_TYPE_EARLY",3);
	define("PARTIALCONFIRM","P");
	define("ALLCONFIRM","A");
	define("NONECONFIRM","N");
	define("BLANKDATA","B");
	define("CARD_STUDENT_MAX_SESSION",10);
	
	class libcardstudentattend2 extends libclass {
		var $file_array;
		var $word_array;
		var $word_base_dir;
		
		# Settings
		var $Settings;
		var $attendance_mode;
		var $time_table_mode;
		var $disallow_input_in_profile;
		var $DefaultAttendanceSymbol;
		var $DefaultAttendanceStatus;
		var $PMFollowAMStatus;
		var $EnableEntryLeavePeriod;
		var $NoRecordLunchOut;
		var $LunchOutOnce;
		var $AllAllowGoOut;
		var $ClassTeacherTakeOwnClassOnly;
		var $ProfileAttendCount;
		var $DefaultAbsentReason;
		var $DefaultLateReason;
		var $DafaultEarlyReason;
		var $DefaultOutingReason;
		var $TeacheriAccountProfileAllowEdit;
		var $DisableISmartCardPastDate;
		var $IgnoreLateTapCardMins;
		var $MinsToTreatAsPMIn;
		var $iSmartCardOnlyDisplaySchoolDayAttendanceRecord;
		var $iSmartCardDisableModifyStatusAfterConfirmed; // added on 2018-08-03
		
		var $eNoticeTemplateCategorySetting;
		var $OnlySynceDisLateRecordWhenConfirm;
		var $DisallowNonTeachingStaffTakeAttendance;
		var $EditDaysBeforeRecord;
		var $EditDaysBeforeRecordSelect;
		
		var $TeacherCanManageWaiveStatus;
		var $TeacherCanManageReason;
		var $TeacherCanManageTeacherRemark;
		var $TeacherCanManageProveDocumentStatus;
		var $AllowClassTeacherManagePresetStudentAbsence;

		//var $ContinuousAbsenceAlertAbsentDays;
		//var $ContinuousAbsenceAlertJobStatus;
		//var $ContinuousAbsenceAlertJobExecutionTime;
		
		var $Platform = "IP";
		var $Module = "StudentAttendance";
		
		//Henry: For the function from liblessonattendance.php
		var $CurAcademicYearID;
		
		var $LibAccessRight = null;
		var $LibGeneralSettings = null;
		var $is_magic_quotes_active = false;
		
		function libcardstudentattend2()
		{
			$this->libclass();
			
			# Make word templates array
			global $intranet_root, $Lang;
			global $i_StudentAttendance_PresetWord_CardSite,
			       $i_StudentAttendance_PresetWord_OutingFromWhere,
			       $i_StudentAttendance_PresetWord_OutingLocation,
			       $i_StudentAttendance_PresetWord_OutingObjective,
			       $i_StudentAttendance_PresetWord_DetentionLocation,
			       $i_StudentAttendance_PresetWord_DetentionReason;
			$this->file_array = array("site.txt",
			       "outing_from.txt",
			       "outing_loc.txt",
			       "outing_obj.txt",
			       "detent_loc.txt",
			       "detent_reason.txt"
			       );
			$this->word_array = array($i_StudentAttendance_PresetWord_CardSite,
			       $i_StudentAttendance_PresetWord_OutingFromWhere,
			       $i_StudentAttendance_PresetWord_OutingLocation,
			       $i_StudentAttendance_PresetWord_OutingObjective,
			       $i_StudentAttendance_PresetWord_DetentionLocation,
			       $i_StudentAttendance_PresetWord_DetentionReason);
			$this->word_base_dir = "$intranet_root/file/cardword/";
			$this->attendance_mode = "NO";
			$this->DefaultAttendanceSymbol[CARD_STATUS_LATE] = "Ø";
			$this->eNoticeTemplateCategorySetting = array('Absent'=> $Lang['StudentAttendance']['Absent'],
																						'CumulativeProfile'=> $Lang['StudentAttendance']['CumulativeProfile']);
			$this->retrieveSettings();
			
			//Henry: For the function from liblessonattendance.php
			$this->CurAcademicYearID = Get_Current_Academic_Year_ID();
			
			$this->is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
		}
		
		function __destruct()
		{
			global $___STUDENT_ATTENDANCE_LOGGED___; // prevent same page being logged more than one time
			// log down when and who do update or remove action under Student Attendance module path
			if( !$___STUDENT_ATTENDANCE_LOGGED___  && (((strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/attendance/')!==false || strpos($_SERVER['REQUEST_URI'],'/home/smartcard/attendance/')!==false)
				&& (strpos($_SERVER['REQUEST_URI'],'update')!==false || strpos($_SERVER['REQUEST_URI'],'remove')!==false || strpos($_SERVER['REQUEST_URI'],'delete')!==false)) 
				|| strpos($_SERVER['REQUEST_URI'],'/home/smartcard/attendance/app_view/handle_attendance.php')!==false)
			  )
			{
				$___STUDENT_ATTENDANCE_LOGGED___ = 1;
				$log = date("Y-m-d H:i:s").' '.OsCommandSafe($_SERVER['REQUEST_URI']).' '.(isset($_SESSION['UserID'])?$_SESSION['UserID']:0).' '.(count($_POST)>0?base64_encode(serialize($_POST)):'');
				$this->log($log);
			}
		}
		
		// Make the access right library singleton
		function GetLibAccessRight()
		{
			global $intranet_root, $PATH_WRT_ROOT;
			if(!$this->LibAccessRight){
				include_once("libstudentattendance_accessright.php");
				$this->LibAccessRight = new libstudentattendance_accessright();
			}
			return $this->LibAccessRight;
		}
		
		function GET_ADMIN_USER()
		{
			global $intranet_root;

			$lf = new libfilesystem();
			$AdminUser = trim($lf->file_read($intranet_root."/file/student_attendance2/admin_user.txt"));
			
			return $AdminUser;
		}

    function IS_ADMIN_USER($ParUserID)
    {
			global $intranet_root, $plugin;
			
			$AdminUser = $this->GET_ADMIN_USER();
			$IsAdmin = 0;
			if(!empty($AdminUser))
			{
	      $AdminArray = explode(",", $AdminUser);
	      $IsAdmin = ($plugin['attendancestudent_eAdmin'] && in_array($ParUserID, $AdminArray)) ? 1 : 0;
			}
			
			return $IsAdmin;
    }

	/*
	 * @Usage: Check module function access right, do not need to care the case sensitivity of the RightString as it would finally uppercased.
	 * e.g. $lcardstudentattend2->Check_Access_Right("StudentAttendance-MGMT-SchoolAttendance-Manage")
	 */
	 function Check_Access_Right($RightString) 
	 {
	 	global $intranet_root, $PATH_WRT_ROOT, $plugin, $SESSION_ACCESS_RIGHT;
		$libaccessright = $this->GetLibAccessRight();
		switch (sizeof(explode('-',$RightString))) {
			case 2:
				$Return = $libaccessright->CHECK_SECTION_ACCESS($RightString);
				break;
			case 3:
				$Return = $libaccessright->CHECK_FUNCTION_ACCESS($RightString);
				break;
			case 4:
				$Return = $libaccessright->CHECK_ACCESS($RightString);
				break;
			default: 
				$Return = false;
				break;
		}
		
		return $Return;
	}


	  function retrieveTimeTableMode()
	  {
			global $intranet_root;
			
			//$GeneralSettings = new libgeneralsettings();
			$GeneralSettings = $this->GetLibGeneralSettings();
			
			$Settings = $GeneralSettings->Get_General_Setting('StudentAttendance',array("'TimeTableMode'"));
			
			return $Settings['TimeTableMode'];
			/*$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
			$time_table_mode = $time_table;
			return $time_table_mode;*/
	  }

		function retrieveSettings()
		{
			global $sys_custom;
			//$GeneralSettings = new libgeneralsettings();
			$GeneralSettings = $this->GetLibGeneralSettings();
			
			$SettingList = array();
			$SettingList[] = "'NoRecordLunchOut'";
			$SettingList[] = "'LunchOutOnce'";
			$SettingList[] = "'AllAllowGoOut'";
			$SettingList[] = "'AttendanceMode'";
			$SettingList[] = "'TimeTableMode'";
			$SettingList[] = "'DisallowProfileInput'";
			$SettingList[] = "'DefaultAttendanceStatus'";
			$SettingList[] = "'PMStatusNotFollowAMStatus'";
			$SettingList[] = "'EnableEntryLeavePeriod'";
			$SettingList[] = "'ClassTeacherTakeOwnClassOnly'";
			$SettingList[] = "'ProfileAttendCount'";
			$SettingList[] = "'DefaultAbsentReason'";
			$SettingList[] = "'DefaultLateReason'";
			$SettingList[] = "'DefaultEarlyReason'";
			$SettingList[] = "'DefaultOutingReason'";
			$SettingList[] = "'TeacheriAccountProfileAllowEdit'";
			$SettingList[] = "'DisableISmartCardPastDate'";
			$SettingList[] = "'CannotTakeFutureDateRecord'";
			$SettingList[] = "'IgnoreLateTapCardMins'";
			$SettingList[] = "'MinsToTreatAsPMIn'";
			$SettingList[] = "'iSmartCardOnlyDisplaySchoolDayAttendanceRecord'";
			$SettingList[] = "'iSmartCardDisableModifyStatusAfterConfirmed'";
			$SettingList[] = "'OnlySynceDisLateRecordWhenConfirm'";
			$SettingList[] = "'DisallowNonTeachingStaffTakeAttendance'";
			$SettingList[] = "'EditDaysBeforeRecord'";
			$SettingList[] = "'EditDaysBeforeRecordSelect'";
			$SettingList[] = "'TeacherCanManageWaiveStatus'";
			$SettingList[] = "'TeacherCanManageReason'";
			$SettingList[] = "'TeacherCanManageTeacherRemark'";
			$SettingList[] = "'TeacherCanManageProveDocumentStatus'";
			$SettingList[] = "'ContinuousAbsenceAlertAbsentDays'";
			$SettingList[] = "'ContinuousAbsenceAlertJobStatus'";
			$SettingList[] = "'ContinuousAbsenceAlertJobExecutionTime'";
			if($sys_custom['StudentAttendance']['HostelAttendance']){
				$SettingList[] = "'HostelAttendanceGroupCategory'";
				$SettingList[] = "'HostelAttendanceAdmin'";
			}
			if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']){
				$SettingList[] = "'HKUGAC_LateTime'";
				$SettingList[] = "'HKUGAC_JobStatus'";
				$SettingList[] = "'HKUGAC_JobExecutionTime'";
			}
			$SettingList[] = "'ContinuousAbsenceAlertJobSendToAdmin'";
			$SettingList[] = "'ContinuousAbsenceAlertJobSendToAdminAll'";
			$SettingList[] = "'ContinuousAbsenceAlertJobSendToAdminIds'";
			$SettingList[] = "'ContinuousAbsenceAlertJobSendToTeacher'";
			$SettingList[] = "'ContinuousAbsenceAlertJobSendToTeacherAll'";
			$SettingList[] = "'ContinuousAbsenceAlertJobSendToTeacherIds'";
			$SettingList[] = "'ContinuousAbsenceAlertJobDateRangeAcademicYear'";
			$SettingList[] = "'ContinuousAbsenceAlertJobDateRangeStartDate'";
			$SettingList[] = "'ContinuousAbsenceAlertJobDateRangeEndDate'";
			$SettingList[] = "'AllowClassTeacherManagePresetStudentAbsence'";
			if($sys_custom['StudentAttendance']['RecordBodyTemperature']) {
				$SettingList[] = "'StudentBodyTemperatureAbnormalValue'";
			}

			$this->Settings = $GeneralSettings->Get_General_Setting('StudentAttendance',$SettingList);
			$this->attendance_mode = $this->Settings['AttendanceMode'];
			$this->time_table_mode = $this->Settings['TimeTableMode'];
			$this->disallow_input_in_profile = $this->Settings['DisallowProfileInput'];
			$this->DefaultAttendanceStatus = ($this->Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$this->Settings['DefaultAttendanceStatus'];
			$this->PMStatusNotFollowAMStatus = $this->Settings['PMStatusNotFollowAMStatus'];
			$this->EnableEntryLeavePeriod = $this->Settings['EnableEntryLeavePeriod'];
			$this->ClassTeacherTakeOwnClassOnly = $this->Settings['ClassTeacherTakeOwnClassOnly'];
			$this->ProfileAttendCount = $this->Settings['ProfileAttendCount'];
			$this->DefaultAbsentReason = $this->Settings['DefaultAbsentReason'];
			$this->DefaultLateReason = $this->Settings['DefaultLateReason'];
			$this->DefaultEarlyReason = $this->Settings['DefaultEarlyReason'];
			$this->DefaultOutingReason = $this->Settings['DefaultOutingReason'];
			$this->TeacheriAccountProfileAllowEdit = $this->Settings['TeacheriAccountProfileAllowEdit'];
			$this->DisableISmartCardPastDate = $this->Settings['DisableISmartCardPastDate'];
			$this->CannotTakeFutureDateRecord = $this->Settings['CannotTakeFutureDateRecord'];
			$this->IgnoreLateTapCardMins = $this->Settings['IgnoreLateTapCardMins'];
			$this->MinsToTreatAsPMIn = $this->Settings['MinsToTreatAsPMIn'];
			$this->iSmartCardOnlyDisplaySchoolDayAttendanceRecord = $this->Settings['iSmartCardOnlyDisplaySchoolDayAttendanceRecord'];
			$this->iSmartCardDisableModifyStatusAfterConfirmed = $this->Settings['iSmartCardDisableModifyStatusAfterConfirmed'];
			$this->OnlySynceDisLateRecordWhenConfirm = $this->Settings['OnlySynceDisLateRecordWhenConfirm'];
			$this->DisallowNonTeachingStaffTakeAttendance = $this->Settings['DisallowNonTeachingStaffTakeAttendance'];
			$this->EditDaysBeforeRecord = $this->Settings['EditDaysBeforeRecord'];
			$this->EditDaysBeforeRecordSelect = $this->Settings['EditDaysBeforeRecordSelect'];
			$this->TeacherCanManageWaiveStatus = $this->Settings['TeacherCanManageWaiveStatus'];
			$this->TeacherCanManageReason = $this->Settings['TeacherCanManageReason'];
			$this->TeacherCanManageTeacherRemark = $this->Settings['TeacherCanManageTeacherRemark'];
			$this->TeacherCanManageProveDocumentStatus = $this->Settings['TeacherCanManageProveDocumentStatus'];
			$this->AllowClassTeacherManagePresetStudentAbsence = $this->Settings['AllowClassTeacherManagePresetStudentAbsence'];
			//$this->ContinuousAbsenceAlertAbsentDays = $this->Settings['ContinuousAbsenceAlertAbsentDays'];
			//$this->ContinuousAbsenceAlertJobStatus = $this->Settings['ContinuousAbsenceAlertJobStatus'];
			//$this->ContinuousAbsenceAlertJobExecutionTime = $this->Settings['ContinuousAbsenceAlertJobExecutionTime'];
			
			// lunch setting
			$this->NoRecordLunchOut = $this->Settings['NoRecordLunchOut'];
			$this->LunchOutOnce = $this->Settings['LunchOutOnce'];
			$this->AllAllowGoOut = $this->Settings['AllAllowGoOut'];
			
			if($sys_custom['StudentAttendance']['HostelAttendance']){
				$this->HostelAttendanceGroupCategory = $this->Settings['HostelAttendanceGroupCategory'];
				$this->HostelAttendanceAdmin = $this->Settings['HostelAttendanceAdmin']==''? array():explode(",",$this->Settings['HostelAttendanceAdmin']);
			}
			
			if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']){
				$this->HKUGAC_LateTime = $this->Settings['HKUGAC_LateTime'];
				$this->HKUGAC_JobStatus = $this->Settings['HKUGAC_JobStatus'];
				$this->HKUGAC_JobExecutionTime = $this->Settings['HKUGAC_JobExecutionTime'];
			}
			
			//$this->retrieveTimeTableMode();
			//$this->disallowInputInProfile();
		}
	
		function isStudentAttendance30(){
			global $module_version;
			if($module_version['StudentAttendance'] == 3.0 || strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/eAttendance/')!==false){
				return true;
			}
			else{
				return false;
			}
		}
	
                                                                // edited by Peter,14/07/2006 get TimeArray With NonScholDay field
	    function getTimeArray($type,$value)
	    { 		
	    	if($this->isStudentAttendance30()){
	    		$time_format = '%H:%i:%s';
	    	}
	    	else{
	    		$time_format = '%H:%i';
	    	}
				
			if ($type==0)      # Normal
			{
			   $sql = "SELECT TIME_FORMAT(MorningTime,'".$time_format."'), TIME_FORMAT(LunchStart,'".$time_format."'), TIME_FORMAT(LunchEnd,'".$time_format."'), TIME_FORMAT(LeaveSchoolTime,'".$time_format."'),NonSchoolDay
			                  FROM CARD_STUDENT_PERIOD_TIME WHERE DayType = 0 AND DayValue = 0";
			   $temp = $this->returnArray($sql,5);
			   return $temp[0];
			}
			else
			{
			   $sql = "SELECT TIME_FORMAT(MorningTime,'".$time_format."'), TIME_FORMAT(LunchStart,'".$time_format."'), TIME_FORMAT(LunchEnd,'".$time_format."'), TIME_FORMAT(LeaveSchoolTime,'".$time_format."'), NonSchoolDay
			                  FROM CARD_STUDENT_PERIOD_TIME WHERE DayType = '$type' AND DayValue ='$value'";
			   $temp = $this->returnArray($sql,5);
			   return $temp[0];
			}
		}

                                                                // edited by Peter, 14/07/2006 added NonScholDay field
		# Param: $type: 1 - Weekday, 2 - Cycle Day
		function getTimeArrayList($type)
		{
			if ($type != 1 && $type != 2) return;
			$sql = "SELECT DayValue, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
			              FROM CARD_STUDENT_PERIOD_TIME
			              WHERE DayType = '$type' ORDER BY DayValue";
			return $this->returnArray($sql,6);
		}
		
		// Created By Ronald on 30 May 2007
		function getSessionTimeArrayList($type,$class_id)
		{
			if ($type != 1 && $type != 2) return;
			if ($class_id == 0)
			               $cond = " b.ClassID = 0 ";
			else
			               $cond = " b.ClassID = '$class_id' ";
			
			$sql = "SELECT
								a.SessionID, TIME_FORMAT(a.MorningTime,'%H:%i'), TIME_FORMAT(a.LunchStart,'%H:%i'), TIME_FORMAT(a.LunchEnd,'%H:%i'), TIME_FORMAT(a.LeaveSchoolTime,'%H:%i'), a.NonSchoolDay, b.DayValue
							FROM
								CARD_STUDENT_TIME_SESSION AS a INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID)
							WHERE
								b.DayType = '$type' AND $cond ORDER BY b.DayValue";
			return $this->returnArray($sql,7);
		}
		
		// created by PEter, 14/07/2006   CARD_STUDENT_SPECIFIC_DATE_TIME
		function getSpecialTimeArrayList(){
      $sql="SELECT RecordDate,IF(ClassID IS NULL,0,ClassID),TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
              FROM CARD_STUDENT_SPECIFIC_DATE_TIME ORDER BY RecordDate";
		  return $this->returnArray($sql,7);
		}
		
		function getDayValueWithoutSpecific($type=1)
		{
			$sql = "SELECT DayValue FROM CARD_STUDENT_PERIOD_TIME
			              WHERE DayType = '$type' ORDER BY DayValue";
			$specific_dayvalues = $this->returnVector($sql);
			if ($type==2)
			{
			   $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
			   $whole_set = $this->returnVector($sql);
			}
			else
			{
			   $whole_set = array(0,1,2,3,4,5,6);
			}
			for ($i=0; $i<sizeof($whole_set); $i++)
			{
				$temp = $whole_set[$i];
				if (!in_array($temp,$specific_dayvalues))
				{
				     $result[] = $temp;
				}
			}
			return $result;
		}
		function getSessionDayValueWithoutSpecific($type=1)
		{
			$sql = "SELECT DayValue FROM CARD_STUDENT_TIME_SESSION_REGULAR
			              WHERE DayType = '$type' ORDER BY DayValue";
			$specific_dayvalues = $this->returnVector($sql);
			if ($type==2)
			{
			   $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
			   $whole_set = $this->returnVector($sql);
			}
			else
			{
			   $whole_set = array(0,1,2,3,4,5,6);
			}
			for ($i=0; $i<sizeof($whole_set); $i++)
			{
				$temp = $whole_set[$i];
				if (!in_array($temp,$specific_dayvalues))
				{
				     $result[] = $temp;
				}
			}
			return $result;
		}
		function getDayValueWithoutSpecificForClass($ClassID,$type=1)
		{
			$sql = "SELECT DayValue FROM CARD_STUDENT_CLASS_PERIOD_TIME
			              WHERE DayType = '$type' AND ClassID = '$ClassID' ORDER BY DayValue";
			$specific_dayvalues = $this->returnVector($sql);
			if ($type==2)
			{
			   $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
			   $whole_set = $this->returnVector($sql);
			}
			else
			{
			   $whole_set = array(0,1,2,3,4,5,6);
			}
			for ($i=0; $i<sizeof($whole_set); $i++)
			{
		    $temp = $whole_set[$i];
		    if (!in_array($temp,$specific_dayvalues))
		    {
		         $result[] = $temp;
		    }
			}
			return $result;
		}
		function getSessionDayValueWithoutSpecificForClass($ClassID,$type=1)
		{
			$sql = "SELECT DayValue FROM CARD_STUDENT_TIME_SESSION_REGULAR
			              WHERE DayType = '$type' AND ClassID = '$ClassID' ORDER BY DayValue";
			$specific_dayvalues = $this->returnVector($sql);
			if ($type==2)
			{
			   $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
			   $whole_set = $this->returnVector($sql);
			}
			else
			{
			   $whole_set = array(0,1,2,3,4,5,6);
			}
			for ($i=0; $i<sizeof($whole_set); $i++)
			{
			    $temp = $whole_set[$i];
			    if (!in_array($temp,$specific_dayvalues))
			    {
			         $result[] = $temp;
			    }
			}
			return $result;
		}
		function getClassListMode()
		{
			$sql = 'select 
								yc.YearClassID, 
								yc.ClassTitleEN,
								yc.ClassTitleB5,
								cscsm.Mode 
							From 
								YEAR_CLASS as yc 
								inner join 
								YEAR as y 
								on yc.YearID = y.YearID and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\' 
								LEFT JOIN 
								CARD_STUDENT_CLASS_SPECIFIC_MODE as cscsm 
								ON yc.YearClassID = cscsm.ClassID 
							Order by 
								y.sequence, yc.sequence
							';
								
			/*$sql = "SELECT a.ClassID, a.ClassName, b.Mode
			              FROM INTRANET_CLASS as a
			                   LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
			              WHERE a.RecordStatus = 1
			              ORDER BY a.ClassName";*/
			return $this->returnArray($sql,4);
		}
		function getClassAttendanceMode($ClassID)
		{
			$sql = "SELECT Mode FROM CARD_STUDENT_CLASS_SPECIFIC_MODE
			              WHERE ClassID = '$ClassID'";
			$temp = $this->returnVector($sql);
			return $temp[0]+0;
		}
		function getClassListToTakeAttendance()
		{
			$sql = "SELECT a.ClassID, a.ClassName FROM INTRANET_CLASS as a
			              LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
			       WHERE a.RecordStatus = 1 AND (b.ClassID IS NULL OR b.Mode != 2)
			       ORDER BY a.ClassName";
			return $this->returnArray($sql,2);
		}
		
		function getClassListNotTakeAttendance()
		{
			$sql = "SELECT a.ClassID, a.ClassTitleEN 
							FROM 
								YEAR as y 
								inner join 
								YEAR_CLASS as a 
								on y.YearID = a.YearID and a.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
			          LEFT OUTER JOIN 
			          CARD_STUDENT_CLASS_SPECIFIC_MODE as b 
			          ON a.YearClassID = b.ClassID
			       	WHERE b.Mode = 2
			       	ORDER BY y.sequence, a.sequence";
			//echo $sql; die;
			return $this->returnArray($sql,2);
		}
		
		function isRequiredToTakeAttendance($classname)
		{
			$sql = "SELECT b.Mode
			             FROM INTRANET_CLASS as a
			                  LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
			             WHERE a.ClassName = '$classname' AND a.RecordStatus = 1
			             ";
			$temp = $this->returnVector($sql);
			if ($temp[0]==2) return false;
			else return true;
		}



    # updated by Peter 2006/10/06, to support the checking of NonSchoolDay
    # used in:  /home/admin/smartcard/take/takeAM.php
    #           /home/admin/smartcard/take/takePM.php
    #           /home/profile/smartcard/list.php
    #           /admin/student_attendance2/dailyoperation/class/class_status.php
    #           /admin/student_attendance2/dailyoperation/class/view_student_AM.php
    #           /admin/student_attendance2/dailyoperation/class/view_student_AM_q.php
    #           /admin/student_attendance2/dailyoperation/class/view_student_PM.php
    #           /admin/student_attendance2/dailyoperation/class/view_student_PM_q.php
		function isRequiredToTakeAttendanceByDate($classname,$TargetDate="")
		{
			if($TargetDate=="")
				$TargetDate= date('Y-m-d');
				
			$ClassID = $this->getClassID($classname);
			
			$Settings = $this->Get_Class_Attend_Time_Setting($TargetDate,"",$ClassID);
			
			if ($Settings != "NonSchoolDay" && $TimeSetting !== false) {
				return true;
			}
			else {
				return false;
			}
			/*
			$isTimeSessionMode = 1;
			$isTimeSlotMode = 0;
			
			$timeTableMode = $this->retrieveTimeTableMode();
			
			
			// get time zone setting from school calender	
			$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
			
			$sql = "SELECT 
								yc.YearClassID, 
								yc.ClassTitleEN, 
								b.Mode
              FROM 
              	YEAR_CLASS as yc 
                LEFT OUTER JOIN 
                CARD_STUDENT_CLASS_SPECIFIC_MODE as b 
                ON yc.YearClassID = b.ClassID
              WHERE 
              	yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
              	and 
              	yc.ClassTitleEN='".$this->Get_Safe_Sql_Query($classname)."' 
              ORDER BY yc.sequence";
      //echo $sql; die;
			$temp2 =$this->returnArray($sql,3);
			//echo $temp2[0][2] . "<hr>";
			if($temp2[0][2]==2) return false;
			
			$thisClassID = $temp2[0][0];
			
			$ts_record = strtotime($TargetDate);
			$txt_year = date('Y',$ts_record);
			$txt_month = date('m',$ts_record);
			$txt_day = date('j',$ts_record);
			$day_of_week = date('w',$ts_record);
			
			
			# select classes with non school day on TargetDate from CARD_STUDENT_SPECIFIC_DATE_TIME
			$sqlSpecial = "SELECT 
											ClassID,
											IF(NonSchoolDay=1,1,2) 
										FROM 
											CARD_STUDENT_SPECIFIC_DATE_TIME 
										WHERE 
											RecordDate='$TargetDate'";
			
			if($timeTableMode == $isTimeSessionMode)
			{
			//EXTRA HANDLE THE SQL IF IT IS TIME SESSION MODE
			$sqlSpecial = "SELECT
						a.ClassID, 
						if(b.NonSchoolDay = 1,1,2)
					FROM
						CARD_STUDENT_TIME_SESSION_DATE AS a 
						LEFT OUTER JOIN 
						CARD_STUDENT_TIME_SESSION AS b 
						ON (a.SessionID = b.SessionID)
					WHERE
						a.RecordDate = '$TargetDate'
				   ";
			}
			
			$temp = $this->returnArray($sqlSpecial,2);
			for($i=0;$i<sizeof($temp);$i++){
				$resultSpecial[$temp[$i][0]]=$temp[$i][1];
			}
			
			if ($CycleDayInfo[0] == 'Cycle') {
				# select classes with non school day on Target Cycle Day from CARD_STUDENT_CLASS_PERIOD_TIME
				$sqlClassCycle ="select 
													a.ClassID,
													IF(NonSchoolDay=1,1,2) 
												from 
													CARD_STUDENT_CLASS_PERIOD_TIME AS a 
												WHERE 
													a.DayType=2 
													and 
													a.DayValue='".$CycleDayInfo[1]."'";
				
				if($timeTableMode == $isTimeSessionMode)
				{
				//EXTRA HANDLE SQL IF IT IS TIME SESSION MODE
					$sqlClassCycle = "
						SELECT
							b.ClassID, if(a.NonSchoolDay = 1,1,2)
						FROM
				      CARD_STUDENT_TIME_SESSION AS a
							INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b 
							ON 
								(a.SessionID = b.SessionID) 
								and b.DayType = '2' and b.DayValue = '".$CycleDayInfo[1]."'
					  ";
				}
				
				
				$temp=$this->returnArray($sqlClassCycle,2);
				
				for($i=0;$i<sizeof($temp);$i++){
				        $resultClassCycle[$temp[$i][0]]=$temp[$i][1];
				}
				
				# select classes with non school day on Target Cycle Day from CARD_STUDENT_PERIOD_TIME
				$sqlSchoolCycle ="select 
														IF(a.NonSchoolDay=1,1,2) 
													from 
														CARD_STUDENT_PERIOD_TIME AS a 
													WHERE 
														a.DayType=2 and 
														a.DayValue='".$CycleDayInfo[1]."'";
				
				if($timeTableMode == $isTimeSessionMode)
				{
				//EXTRA HANDLE SQL IF IT IS TIME SESSION MODE
				$sqlSchoolCycle = "
						SELECT
							if(a.NonSchoolDay = 1,1,2)
						FROM
				      CARD_STUDENT_TIME_SESSION AS a
							INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b 
							ON 
								b.ClassID = '0' 
								and a.SessionID = b.SessionID 
								and b.DayType = '2' 
								and b.DayValue = '".$CycleDayInfo[1]."'
					   ";
				}
				
				
				$resultSchoolCycle=$this->returnVector($sqlSchoolCycle);
			}
			else {
				# select classes with non school day on Target Week Day from CARD_STUDENT_PERIOD_TIME
				$sqlSchoolWeek ="select 
													IF(NonSchoolDay=1,1,2) 
												from 
													CARD_STUDENT_PERIOD_TIME 
												WHERE DayType=1 and DayValue='".$CycleDayInfo[1]."'";
				if($timeTableMode == $isTimeSessionMode)
				{
					//EXTRA HANDLE SQL IF IT IS TIME SESSION MODE
					$sqlSchoolWeek = "
							SELECT
								if(a.NonSchoolDay = 1,1,2)
							FROM
					      CARD_STUDENT_TIME_SESSION AS a
								INNER JOIN 
								CARD_STUDENT_TIME_SESSION_REGULAR AS b 
								ON (a.SessionID = b.SessionID)
							WHERE
					      b.DayType = '1' and b.DayValue = '".$CycleDayInfo[1]."' and b.ClassID = 0;
						   ";
				}
				
				
				$resultSchoolWeek = $this->returnVector($sqlSchoolWeek);
				
				# select classes with non school day on Target Week Day from CARD_STUDENT_CLASS_PERIOD_TIME
				$sqlClassWeek ="select 
													ClassID,IF(NonSchoolDay=1,1,2) 
												from 
													CARD_STUDENT_CLASS_PERIOD_TIME 
												WHERE DayType=1 and DayValue='".$CycleDayInfo[1]."'";
				
				if($timeTableMode == $isTimeSessionMode)
				{
					//EXTRA HANDLE SQL IF IT IS TIME SESSION MODE
					$sqlClassWeek = "
						SELECT
							b.ClassID, if(a.NonSchoolDay = 1,1,2)
						FROM
					    CARD_STUDENT_TIME_SESSION AS a
							INNER JOIN 
							CARD_STUDENT_TIME_SESSION_REGULAR AS b 
							ON (a.SessionID = b.SessionID)
						WHERE
					    b.DayType = '1' and b.DayValue = '".$CycleDayInfo[1]."'
					   ";
				}
				
				$temp=$this->returnArray($sqlClassWeek,2);
				
				for($i=0;$i<sizeof($temp);$i++){
				  $resultClassWeek[$temp[$i][0]]=$temp[$i][1];
				}
			}
			
			$off=false;
			$done=false;
			$classID = $temp2[0][0];
			$className = $temp2[0][1];
			$classMode = $temp2[0][2];
			
			$specialClassID= ($classMode==0 || is_null($classMode))?0:$classID;
			# check if NonSchoolDay for Speical Date
			if($resultSpecial[$specialClassID]==1){
        //echo"<p>speical date off=$className</p>";
        $off=true;
        $done=true;
			}else if($resultSpecial[$specialClassID]==2){
        //echo"<p>speical date on=$className</p>";
        $off=false;
        $done=true;
			}
			
			if ($CycleDayInfo[0] == 'Cycle') {
				# check if NonSchoolDay for Cycle Day
				if(!$done){
					if($classMode == 1){ # Class Cycle Day
						if($resultClassCycle[$classID]==1){
						  //echo"<p>class cycle date off=$className</p>";
						  $off = true;
						  $done= true;
						}else if($resultClassCycle[$classID]==2){
						  //echo"<p>class cycle date on=$className</p>";
						  $off = false;
						  $done= true;
						}
					}else if($classMode !=2){  # School Cycle Day
						if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
						  //echo"<p>school cycle date off=$className</p>";
						  $off = true;
						  $done = true;
						} else if($resultSchoolCycle[0]==2){
						  //echo"<p>school cycle date on=$className</p>";
						  $off = false;
						  $done = true;
					  }
					}
				}
			}
			else {			
				# check if NonSchoolDay for Week Day
				if(!$done){
				  if($classMode==1){ # Class Week Day
				
						if($resultClassWeek[$classID]==1){
						  //echo"<p>class week date off=$className</p>";
						  $done = true;
						  $off = true;
						}else if($resultClassWeek[$classID]==2){
						  //echo"<p>class week date on=$className</p>";
						  $done = true;
						  $off = false;
						}
				
				  }else if($classMode!=2){ # School Week Day
						if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
						  //echo"<p>school week date off=$className</p>";
						  $off = true;
						  $done=true;
						} else if($resultSchoolWeek[0]==2){
						  //echo"<p>school week date on=$className</p>";
						  $off = false;
						  $done=true;
						}
					}
				}
			}
		
			//debug_r($off);
			if($classMode==2 || $off){
				return false;
			}else{
				return true;
			}
			*/
		}

	function groupIsRequiredToTakeAttendanceByDate($group_id,$TargetDate)
	{
		if($TargetDate=="")
				$TargetDate= date('Y-m-d');
				
		
		$Settings = $this->Get_Group_Attend_Time_Setting($TargetDate,"",false,$group_id);
		
		if ($Settings != "NonSchoolDay" && $TimeSetting !== false) {
			return true;
		}
		else {
			return false;
		}
		/*
	//	echo "groupid [".$group_id."] [".$TargetDate."]<br>";
	//	global $ldb;
		$isTimeSessionMode = 1;
		$isTimeSlotMode = 0;
	
		$timeTableMode = $this->retrieveTimeTableMode();
	
		if($TargetDate=="")
			$TargetDate= date('Y-m-d');
		
		// get time zone setting from school calender	
		$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
	
		/////////// check mode first ///////////////
		$sql = "SELECT
					a.GroupID, a.Title, b.Mode
				FROM
					INTRANET_GROUP a
					inner join 
					CARD_STUDENT_ATTENDANCE_GROUP b
					on 
						a.groupid = b.groupid AND
						a.GroupID='$group_id'
				";
		//debug_r($sql);
		$temp2 = $this->returnArray($sql,3);
		//mode = 2 ==> no need to take attendance
		if($temp2[0][2]==2) return false;
	
		///////////////////////////////////////////////////////
		$ts_record = strtotime($TargetDate);
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('j',$ts_record);
		$day_of_week = date('w',$ts_record);
	
		//////////// check special day///////////////////////////
		$sql = "select
					groupid, 
					IF(NonSchoolDay=1,1,2)
				from
					CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP
				where
					groupid ='$group_id' 
					and 
					RecordDate = '".$TargetDate."'
				";
		if($timeTableMode == $isTimeSessionMode)
		{
			//EXTRA HANDLE THE SQL IF IT IS TIME SESSION MODE
			$sql = "SELECT
						a.GROUPID,
						b.NonSchoolDay
					from
						CARD_STUDENT_TIME_SESSION_DATE_GROUP AS a
						INNER JOIN 
						CARD_STUDENT_TIME_SESSION AS b 
						ON 
							(a.SessionID = b.SessionID) 
							and a.GroupID = '".$group_id."' 
							and a.RecordDate = '".$TargetDate."'
				    ";
		}
		//debug_r($sql);
		$temp2 = $this->returnArray($sql,2);
	
		//NonShcoolDay =1, IT IS NON SCHOOL DAY, NO NEED TO TAKE ATTENDANCE
		if($temp2[0][1] ==1) return false;
	
		if ($CycleDayInfo[0] == "Cycle") {
			//////////////check cycle day////////////////////
			$sql = "select
						a.groupid,IF(NonSchoolDay=1,1,2)
					from
						CARD_STUDENT_GROUP_PERIOD_TIME AS a 
					WHERE 
						a.groupid = '$group_id'
						and 
						a.DayType=2 
						and 
						a.DayValue='".$CycleDayInfo[1]."'
					";
				//echo "sql [".$sql."]<br>";
			if($timeTableMode == $isTimeSessionMode)
			{
				//EXTRA HANDLE THE SQL IF IT IS TIME SESSION MODE
				$sql = "SELECT
									b.groupid, 
									a.NonSchoolDay
		            FROM
									CARD_STUDENT_TIME_SESSION AS a
									INNER JOIN 
									CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b 
									ON 
										(a.SessionID = b.SessionID) 
										and b.DayType = 2 
										and b.groupid = $group_id 
										and a.DayValue='".$CycleDayInfo[1]."'
					    ";
			}
			
			//debug_r($sql);
			$temp2=$this->returnArray($sql,2);
			if($temp2[0][1] ==1) return false;
		}
		else {
			//////////check week day////////////////////////
			$sql = "select
						a.groupid,IF(NonSchoolDay=1,1,2)
					from
						CARD_STUDENT_GROUP_PERIOD_TIME AS a
					WHERE
						a.groupid = '$group_id' 
						and 
						a.DayType=1 and a.DayValue='".$CycleDayInfo[1]."'
					";
				//echo "sql [".$sql."]<br>";
			if($timeTableMode == $isTimeSessionMode)
			{
				//EXTRA HANDLE THE SQL IF IT IS TIME SESSION MODE
				$sql = "SELECT
									b.groupid, a.NonSchoolDay
								FROM
									CARD_STUDENT_TIME_SESSION AS a
									INNER JOIN 
									CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b 
									ON 
										(a.SessionID = b.SessionID) 
										and 
										b.groupid = $group_id 
										and 
										b.DayType = 1 
										and b.DayValue = '".$CycleDayInfo[1]."'
					    ";
			}
			//debug_r($sql);
			$temp2=$this->returnArray($sql,2);
			if($temp2[0][1] ==1) return false;
		}
		/////////check normal day ////////////////////////
	
		$sql = "select
					a.groupid,IF(NonSchoolDay=1,1,2)
				from
					CARD_STUDENT_GROUP_PERIOD_TIME AS a
				WHERE
					a.groupid = '$group_id'
					AND 
					a.DayType = 0 and a.DayValue = 0 
				";
			//echo "sql [".$sql."]<br>";
		if($timeTableMode == $isTimeSessionMode)
		{
			//EXTRA HANDLE THE SQL IF IT IS TIME SESSION MODE
			//SKIP, CANNOT CONFIG GROUP WITH NO SCHOOL DAY WITH NORMAL DAY SETTING
		}
		//debug_r($sql);
		$temp2=$this->returnArray($sql,2);
		if($temp2[0][1] ==1) return false;
	
		// default return true;
		return true;
		*/
	}

				 function getGroupListToTakeAttendanceByDate($TargetDate)
                {
						$TargetDate==""?date('Y-m-d'):$TargetDate;
						$classList = $this->getGroupListMode(3);
                         $need_take_attendance = array();
                         for($i=0;$i<sizeof($classList);$i++){
                                 list($group_id,$group_name,$groupMode) = $classList[$i];
//								 echo "group [".$group_id."] [".$group_name."]<br>";
								 if($this->groupIsRequiredToTakeAttendanceByDate($group_id,$TargetDate))
								{
									 $need_take_attendance[]=array($group_id,$group_name);
								}
                             }
                        return $need_take_attendance;
                }
	# updated by Peter 2006/10/09, to support the checking of NonSchoolDay
	# Used in: /home/admin/smartcard/take/index.php
	function getClassListToTakeAttendanceByDate($TargetDate,$TeacherID="")
	{			
		$TargetDate==""?date('Y-m-d'):$TargetDate;
		//updated by marcus 20090813 migrate from ip20 to 25, INTRANET_CLASS to YEAR_CLASS
		$sql = "SELECT 
							a.YearClassID,
							".Get_Lang_Selection('a.ClassTitleB5', 'a.ClassTitleEN').", 
							y.YearName,
							a.ClassTitleEN 
						FROM 
							YEAR_CLASS AS a 
							INNER JOIN 
							YEAR as y 
							on a.YearID = y.YearID ";
		// condition added by kenneth chung 20100318, allow if settings on ismartcard teacher can only take own class attendance
		if ($this->ClassTeacherTakeOwnClassOnly && $TeacherID != "") {
			$sql .= "
							INNER JOIN 
							YEAR_CLASS_TEACHER yct 
							on 
								a.YearClassID = yct.YearClassID 
								and 
								yct.UserID = '".$TeacherID."' 
							";
		}
		$sql .= "
						Where 
							a.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						ORDER BY 
							y.sequence, a.sequence";
		//debug_r($sql);
		$classList = $this->returnArray($sql,2);
		$need_take_attendance = array();
		for($i=0;$i<sizeof($classList);$i++){
			list($class_id,$class_name,$YearName,$class_name_en) = $classList[$i];
			if($this->isRequiredToTakeAttendanceByDate($class_name_en,$TargetDate))
				$need_take_attendance[]=array($class_id,$class_name,$YearName,$class_name_en);
		}
		return $need_take_attendance;
	}

	# updated by Peter 2006/10/09, to support the checking of NonSchoolDay
	function getClassListNotTakeAttendanceByDate($TargetDate)
	{
		$TargetDate==""?date('Y-m-d'):$TargetDate;
		//updated by marcus 20090813 migrate from ip20 to 25, INTRANET_CLASS to YEAR_CLASS
		$sql = "SELECT 
				a.YearClassID,
				a.ClassTitleEN 
			FROM 
				YEAR_CLASS AS a 
			Where 
				a.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			ORDER BY a.ClassTitleEN";
		$classList = $this->returnArray($sql,2);
		$not_take_attendance = array();
		for($i=0;$i<sizeof($classList);$i++){
			list($class_id,$class_name_en) = $classList[$i];
			$class_name = $class_name_en;
			if(!$this->isRequiredToTakeAttendanceByDate($class_name,$TargetDate))
			 	$not_take_attendance[]=array($class_id,$class_name);
		}
		return $not_take_attendance;
	}

                # edited by PeterHo 14/07/2006 add NonSchoolDay
                function getClassTimeArray($ClassID,$type,$value)
                {
                		 if($this->isStudentAttendance30()){
				    		$time_format = '%H:%i:%s';
				    	 }
				    	 else{
				    		$time_format = '%H:%i';
				    	 }
                		 
                         if ($type==0)      # Normal
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'".$time_format."'), TIME_FORMAT(LunchStart,'".$time_format."'), TIME_FORMAT(LunchEnd,'".$time_format."'), TIME_FORMAT(LeaveSchoolTime,'".$time_format."'),NonSchoolDay
                                            FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType = 0 AND DayValue = 0 AND ClassID = '$ClassID'";
                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                         else
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'".$time_format."'), TIME_FORMAT(LunchStart,'".$time_format."'), TIME_FORMAT(LunchEnd,'".$time_format."'), TIME_FORMAT(LeaveSchoolTime,'".$time_format."'),NonSchoolDay
                                            FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType = '$type' AND DayValue ='$value' AND ClassID = '$ClassID'";
                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                }
                # created by PeterHo 17/07/2006
                function getClassSpecialTimeArray($ClassID,$TargetDate){
                                                                        $sql="SELECT RecordDate,TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_SPECIFIC_DATE_TIME  WHERE ClassID='$ClassID' AND RecordDate='$TargetDate' ORDER BY RecordDate";
                                   $temp=$this->returnArray($sql,6);
                                   return $temp[0];

                }
                # created by PeterHo 14/07/2006
                function getClassSpecialTimeArrayList($ClassID){
                                                                        $sql="SELECT RecordDate,TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_SPECIFIC_DATE_TIME  WHERE ClassID='$ClassID' ORDER BY RecordDate";
                                   return $this->returnArray($sql,6);
                }
                // Edited by PeterHo 14/07/2006 add NonSchoolDay
                # Param: $type: 1 - Weekday, 2 - Cycle Day
                function getClassTimeArrayList($ClassID,$type)
                {
                         if ($type != 1 && $type != 2) return;
                         $sql = "SELECT DayValue, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_CLASS_PERIOD_TIME
                                        WHERE DayType = '$type' AND ClassID = '$ClassID' ORDER BY DayValue";
                         return $this->returnArray($sql,6);
                }

                function removeBadActionRecord($StudentID, $type, $date)
                {
	                global $bug_tracing,$intranet_root,$file_path;
                         if ($date == "")
                         {
                             //$date = date('Y-m-d');
                             return;
                         }

                         $sql =" DELETE FROM CARD_STUDENT_BAD_ACTION WHERE StudentID='$StudentID' AND RecordDate='$date' AND RecordType='$type'";
                         $this->db_db_query($sql);

                       if(isset($bug_tracing['smartcard_student_attend_bad_action']) && $bug_tracing['smartcard_student_attend_bad_action']){
	                         	include_once($intranet_root."/includes/libfilesystem.php");
								if(!is_dir($file_path."/file/log_student_attend_bad_action")){
									$lf = new libfilesystem();
									$lf->folder_new($file_path."/file/log_student_attend_bad_action");
								}
								$log_filepath = "$file_path/file/log_student_attend_bad_action/log_student_attend_bad_action_".date("Ymd").".txt";
								$log_date = date('Y-m-d H:i:s');
		                        $log_target_date = $date;
		                        $log_student_id = $StudentID;
		                        $log_bad_action_type = $type;
		                        $log_sql = $sql;
		                        $log_action="remove bad action";
		                        $log_page = $_SERVER['SCRIPT_NAME'];

					            $log_content = get_file_content($log_filepath);
					            $log_entry = "\"$log_student_id\",\"$log_target_date\",\"$log_action\",\"$log_bad_action_type\",\"$log_page\",\"$log_sql\",\"$log_date\"\n";
					            $log_content .= $log_entry;
					            write_file_content($log_content, $log_filepath);
	                     }

                }
                function removeBadActionOutLunchTrial($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_LUNCH_NOTINLIST, $date);
                }
                function removeBadActionOutLunchAgain($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_LUNCH_BACKALREADY, $date);
                }
                function removeBadActionFakedCardAM($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_AM, $date);
                }
                function removeBadActionFakedCardPM($StudentID, $date)
                {
                  $this->removeBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_PM, $date);
                }
                function removeBadActionFakedCard($StudentID, $date, $DayType) {
                	if ($DayType == PROFILE_DAY_TYPE_AM) 
                		$this->removeBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_AM, $date);
                	else 
                		$this->removeBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_PM, $date);
                }
                function removeBadActionNoCardEntrance($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_NO_CARD_ENTRANCE, $date);
                }
  function addBadActionRecord($StudentID, $type, $date="", $time="")
  {
    global $bug_tracing,$intranet_root,$file_path;
		if ($date == "")
		{
			$date = date('Y-m-d');
		}
		$time = ($time == ""? "now()":"'$date $time'");
		
		$sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
		              VALUES ('$StudentID','$date', $time, '$type', now(), now())";
		$this->db_db_query($sql);
		
		if(isset($bug_tracing['smartcard_student_attend_bad_action']) && $bug_tracing['smartcard_student_attend_bad_action']){
			include_once($intranet_root."/includes/libfilesystem.php");
			if(!is_dir($file_path."/file/log_student_attend_bad_action")){
				$lf = new libfilesystem();
				$lf->folder_new($file_path."/file/log_student_attend_bad_action");
			}
			$log_filepath = "$file_path/file/log_student_attend_bad_action/log_student_attend_bad_action_".date("Ymd").".txt";
			$log_date = date('Y-m-d H:i:s');
      $log_target_date = $date;
      $log_student_id = $StudentID;
      $log_bad_action_type = $type;
      $log_sql = $sql;
      $log_action="add bad action";
      $log_page = $_SERVER['SCRIPT_NAME'];
      $log_content = get_file_content($log_filepath);
      $log_entry = "\"$log_student_id\",\"$log_target_date\",\"$log_action\",\"$log_bad_action_type\",\"$log_page\",\"$log_sql\",\"$log_date\"\n";
      $log_content .= $log_entry;
      write_file_content($log_content, $log_filepath);
		}
  }

                function addBadActionOutLunchTrial($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_LUNCH_NOTINLIST, $date, $time);
                }
                function addBadActionOutLunchAgain($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_LUNCH_BACKALREADY, $date, $time);
                }
                function addBadActionFakedCardAM($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_AM, $date, $time);
                }
                function addBadActionFakedCardPM($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_PM, $date, $time);
                }
                function addBadActionFakedCard($StudentID, $date="", $time="", $DayType="")
                {
                	if ($DayType == PROFILE_DAY_TYPE_AM)
                  	$this->addBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_AM, $date, $time);
                  else 
                  	$this->addBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_PM, $date, $time);
                }
                function addBadActionNoCardEntrance($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_NO_CARD_ENTRANCE, $date, $time);
                }

                function createTable_Card_Student_Daily_Log($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                         $sql = "SHOW TABLES LIKE '$card_log_table_name'";
                         $records = $this->returnResultSet($sql);
				         if(count($records)>0)
				         {
				         	$sql = "ALTER TABLE $card_log_table_name ADD COLUMN AMConfirmedByAdmin varchar(1) DEFAULT '0' AFTER IsConfirmed,ADD COLUMN PMConfirmedByAdmin varchar(1) DEFAULT '0' AFTER PMIsConfirmed";
				         	$this->db_db_query($sql);
				         }else{
	                         $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name(
	                                  RecordID int(11) NOT NULL auto_increment,
	                                  UserID int(11) NOT NULL,
	                                  DayNumber int(11) NOT NULL,
	                                  InSchoolTime time,
	                                  InSchoolStation varchar(255),
	                                  AMStatus int,
	                                  LunchOutTime time,
	                                  LunchOutStation varchar(255),
	                                  LunchBackTime time,
	                                  LunchBackStation varchar(255),
	                                  PMStatus int,
	                                  LeaveSchoolTime time,
	                                  LeaveSchoolStation varchar(255),
	                                  LeaveStatus int,
	                                  ConfirmedUserID int,
	                                  IsConfirmed int,
									  AMConfirmedByAdmin varchar(1) DEFAULT '0',
	                                  PMConfirmedUserID int(11) default NULL,
	                                  PMIsConfirmed int default NULL,
									  PMConfirmedByAdmin varchar(1) DEFAULT '0',
	                                  RecordType int,
	                                  RecordStatus int,
	                                  DateInput datetime,
	                                  InputBy int(11) default NULL,
	                                  DateModified datetime,
	                                  ModifyBy int(11) default NULL,
	                                  LastTapCardTime time default NULL,
									  PMDateModified datetime,
									  PMModifyBy int(11),
	                                  PRIMARY KEY (RecordID),
	                                  UNIQUE UserDay (UserID, DayNumber)
	                                ) ENGINE=InnoDB charset=utf8";
	                         $this->db_db_query($sql);
				         }
                }

                ### Created By Ronald Yeung On 27 Jul 2007 ###
                ### Create CARD_STUDENT_ENTRY_LOG If Not Exists
                function createTable_Card_Student_Entry_Log($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$year."_".$month;
                         $sql = "SHOW TABLES LIKE '$card_log_table_name'";
				         $records = $this->returnResultSet($sql);
				         if(count($records)>0)
				         {
				         	$sql = "ALTER TABLE $card_log_table_name ADD COLUMN RecordStation varchar(100) DEFAULT NULL AFTER RecordTime";
				         	$this->db_db_query($sql);
				         }else{
                         	$sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name
                                    (
                                      RecordID int(11) NOT NULL auto_increment,
                                      UserID int(11) NOT NULL,
                                      DayNumber int(11) NOT NULL,
                                      RecordTime time,
									  RecordStation varchar(100) DEFAULT NULL,
                                      RecordType int(11),
                                      RecordStatus int(11),
                                      DateInput datetime,
                                      DateModified datetime,
                                      PRIMARY KEY (RecordID)
                                    ) ENGINE=InnoDB charset=utf8";
                         	$this->db_db_query($sql);
				         }
                }
				
				function createTable_Card_Student_Raw_Log($year="",$month="")
				{
					$year=($year=="")?date("Y"):$year;
                    $month=($month=="")?date("m"):sprintf("%02d",$month);
                    
                    $card_log_table_name = "CARD_STUDENT_RAW_LOG_".$year."_".$month;
                    $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name
                            (
                            	RecordID int(11) NOT NULL auto_increment,
								UserID int(11) NOT NULL,
								DayNumber int(2) NOT NULL,
								RecordTime time NOT NULL,
								RecordStation varchar(100) default NULL,
								DateInput datetime default NULL,
								PRIMARY KEY (RecordID),
								INDEX CompositeIndex(UserID,DayNumber) 
							) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                     return $this->db_db_query($sql);
				}
				
				function addCardStudentRawLogRecord($userId, $year, $month, $day, $time, $location='')
				{
					$year=($year=="")?date("Y"):$year;
                    $month=($month=="")?date("m"):sprintf("%02d",$month);
                    $day=($day=="")?date("d"):sprintf("%02d",$day);
                    
                    $card_log_table_name = "CARD_STUDENT_RAW_LOG_".$year."_".$month;
                    $sql = "INSERT INTO $card_log_table_name (UserID,DayNumber,RecordTime,RecordStation,DateInput) VALUES ('$userId','$day','$time','".$this->Get_Safe_Sql_Query($location)."',NOW())";
                    return $this->db_db_query($sql);
				}
				
                function createTable_Card_Student_Daily_Class_Confirm($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month;
                         $sql = "CREATE TABLE IF NOT EXISTS $card_student_daily_class_confirm (
                                  RecordID int(11) NOT NULL auto_increment,
                                  ClassID int(11) NOT NULL,
                                  ConfirmedUserID int(11) NOT NULL,
                                  DayNumber int(11) NOT NULL,
                                  DayType int(11) NOT NULL,
                                  RecordType int(11),
                                  RecordStatus int(11),
                                  DateInput datetime,
                                  DateModified datetime,
                                  PRIMARY KEY (RecordID),
                                  UNIQUE (ClassID,DayNumber,DayType)
                                ) ENGINE=InnoDB charset=utf8";
                         $this->db_db_query($sql);
                }
				function createTable_Card_Student_Daily_Group_Confirm($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$year."_".$month;
                         $sql = "CREATE TABLE IF NOT EXISTS $card_student_daily_group_confirm (
                                  RecordID int(11) NOT NULL auto_increment,
                                  GroupID int(11) NOT NULL,
                                  ConfirmedUserID int(11) NOT NULL,
                                  DayNumber int(11) NOT NULL,
                                  DayType int(11) NOT NULL,
                                  RecordType int(11),
                                  RecordStatus int(11),
                                  DateInput datetime,
                                  DateModified datetime,
                                  PRIMARY KEY (RecordID),
                                  UNIQUE (GroupID,DayNumber,DayType)
                                ) ENGINE=InnoDB charset=utf8";
                         $this->db_db_query($sql);
                }
				
				function createTable_Card_Student_Daily_Subject_Group_Confirm($year="",$month="")
				{
					$year=($year=="")?date("Y"):$year;
                    $month=($month=="")?date("m"):$month;
                    
                    $year = sprintf("%02d", $year);
                    $motnh = sprintf("%02d",$month);
                    
                    $card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$year."_".$month;
                    $sql = "CREATE TABLE IF NOT EXISTS $card_student_daily_subject_group_confirm (
                              RecordID int(11) NOT NULL auto_increment,
                              SubjectGroupID int(11) NOT NULL,
                              ConfirmedUserID int(11) NOT NULL,
                              DayNumber int(11) NOT NULL,
                              DayType int(11) NOT NULL,
                              RecordType int(11),
                              RecordStatus int(11),
                              DateInput datetime,
                              DateModified datetime,
                              PRIMARY KEY (RecordID),
                              UNIQUE (SubjectGroupID,DayNumber,DayType)
                            ) ENGINE=InnoDB charset=utf8";
                    $this->db_db_query($sql);
				}
				
                function createEntry_Card_Student_Record_Storage($year="", $month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $sql = "INSERT INTO CARD_STUDENT_RECORD_DATE_STORAGE (Year, Month)
                                        VALUES ($year, $month)";
                         $this->db_db_query($sql);
                }
                function createTable_LogAndConfirm($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $this->createTable_Card_Student_Daily_Log($year, $month);
                         $this->createTable_Card_Student_Daily_Class_Confirm($year, $month);
						 $this->createTable_Card_Student_Daily_Group_Confirm($year, $month);
						 $this->createTable_Card_Student_Daily_Subject_Group_Confirm($year,$month);
                         $this->createEntry_Card_Student_Record_Storage($year, $month);
                }

	# 0 - In School status (morning)
	# 1 - Lunch Status
	# 2 - After School Status
	function getSummaryCount($type)
	{
	     if ($this->attendance_mode=="NO")
	     {
	         $this->retrieveSettings();
	     }
	     /*
	     if ($this->attendance_mode!=2 && $type==2)
	     {
	         $type = 0;
	     }
	     */
	     if ($type != 0 && $type != 1 && $type!=2)
	     {
	         $type = 0;
	     }
	     $year=date("Y");
	     $month=date("m");
	     $day = date('d');
	
	     $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	     $status_field = ($this->attendance_mode==1?"PMStatus":"AMStatus");
	
	     if ($type == 0)  # Back School Time check
	     {
	
	         # Get Already back
	         // $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
	         //               WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR $status_field IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") )";
	         $sql = "SELECT log.UserID 
	         				FROM 
	         					$card_log_table_name log ";
	         if ($this->EnableEntryLeavePeriod) {
	         	$sql .= "INNER JOIN 
	         					CARD_STUDENT_ENTRY_LEAVE_PERIOD selp 
	         					on 
	         						log.DayNumber = '$day' 
	         						AND 
	         						log.UserID = selp.UserID 
	         						AND 
	         						NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
	         						AND 
	         						(log.InSchoolTime IS NOT NULL OR log.AMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") OR log.PMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.")) 
	         					";
	         }
	         else {
	          $sql .= "WHERE 
	          					log.DayNumber = '$day' 
	          					AND 
	          					(log.InSchoolTime IS NOT NULL OR log.AMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") OR log.PMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE."))";
	         }
	         $temp = $this->returnVector($sql);
	         $back = sizeof($temp);
	         if($back>0){
	                 $back_student_list = implode(",",$temp);
	                 $back_conds = " AND a.UserID NOT IN($back_student_list) ";
	             }
	
	         # Get not yet back
	         # Get no need to take attendance classes
	
	         /*
	         $sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
	                        LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
	                        WHERE b.Mode = 2";
	         $temp = $this->returnVector($sql);
	         */
	         $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
	         if (sizeof($temp)==0)
	         {
	             $sql = "SELECT COUNT(a.UserID)
                      FROM 
                      	INTRANET_USER as a ";
               if ($this->EnableEntryLeavePeriod) {
               	$sql .= "
               	        INNER JOIN 
                        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                        on a.UserID = selp.UserID 
                        	AND 
                        	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                        ";
               }
               $sql .= " LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)";
               $sql .= "
               				WHERE 
                      	a.RecordType = 2 
                      	AND 
                      	a.RecordStatus IN (0,1,2) 
                      	$back_conds
                       AND yc.AcademicYearID=".get_Current_Academic_Year_ID();
	         }
	         else
	         {
               for($j=0;$j<sizeof($temp);$j++){
                   $t[] = $temp[$j][1];
               }
	             $class_list = "'".implode("','",$t)."'";
	             $sql = "SELECT COUNT(a.UserID)
                      FROM 
                      	INTRANET_USER as a ";
               if ($this->EnableEntryLeavePeriod) {
               	$sql .= "
               	        INNER JOIN 
                        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                        on a.UserID = selp.UserID 
                        	AND 
                        	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                        ";
               }
               $sql .= " LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)";
               $sql .= "
                      WHERE 
                      	a.RecordType = 2 
                      	AND 
                      	a.RecordStatus IN (0,1,2) 
                      	$back_conds
												AND a.ClassName NOT IN ($class_list) AND yc.AcademicYearID=".get_Current_Academic_Year_ID();

	         }

	         $temp = $this->returnVector($sql);
	         $notyet = $temp[0]+0;
	
	
	        return array($back,$notyet);
	     }
	     else if ($type == 1) # Out for lunch check
	     {
	          # Gone out
	          
	          $sql = "SELECT COUNT(RecordID) 
	          				FROM 
	          					$card_log_table_name a ";
	        	if ($this->EnableEntryLeavePeriod) {
             	$sql .= "
             	        INNER JOIN 
                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                      on a.UserID = selp.UserID 
                      	AND 
                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                      ";
            }
		        $sql .= "
		        				WHERE 
	                  	a.DayNumber = '$day' AND 
	                  	a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.LunchOutTime IS NOT NULL AND a.LunchBackTime IS NULL AND a.LeaveStatus IS NULL AND (a.PMStatus IS NULL OR a.PMStatus IN (1,3))";
	
	          $temp = $this->returnVector($sql);
	          $gone_out = $temp[0]+0;
	          //echo "<BR>".$sql;
	          //echo "<BR>A:".sizeof($temp);
	
	          # back school
	          //$sql = "SELECT COUNT(a.RecordID) FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND a.DayNumber = '$day')
	          //               WHERE (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.LunchOutTime IS NOT NULL AND a.LunchBackTime IS NOT NULL AND LeaveStatus NOT IN (1)) OR (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (a.LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";
						$sql = "SELECT COUNT(a.RecordID) 
										FROM 
											$card_log_table_name AS a 
											INNER JOIN 
											CARD_STUDENT_LUNCH_ALLOW_LIST AS b 
											ON (a.UserID = b.StudentID AND a.DayNumber = '$day') ";
						if ($this->EnableEntryLeavePeriod) {
             	$sql .= "
             	        INNER JOIN 
                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                      on a.UserID = selp.UserID 
                      	AND 
                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                      ";
            }
						$sql .= "
										WHERE 
											(a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.LunchOutTime IS NOT NULL AND a.LunchBackTime IS NOT NULL AND LeaveStatus IS NULL) OR (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (a.LeaveStatus IS NULL OR a.LeaveStatus IN (0,2)))";
						
	          $temp = $this->returnVector($sql);
	          //echo "<BR>".$sql;
	          $back = $temp[0]+0;
	          //echo "<BR>B:".sizeof($temp);
	
	          # Not gone out
	          # Can go out students - $gone_out - $back
	          global $intranet_root;
	          //list($lunch_misc_no_record,$lunch_misc_lunch_once, $lunch_misc_all_allow) = $lunch_misc_settings;
	          $lunch_misc_no_record = $this->NoRecordLunchOut;
			  $lunch_misc_lunch_once = $this->LunchOutOnce;
			  $lunch_misc_all_allow = $this->AllAllowGoOut;
	
	          if ($lunch_misc_all_allow)
	          {
	              # Get no need to take attendance classes
	              //$sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
	              //               LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
	              //               WHERE b.Mode = 2";
	              //$temp = $this->returnVector($sql);
	
	              $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
	              if (sizeof($temp)==0)
	              {
	              /*
	                  $sql = "SELECT COUNT(a.UserID)
	                                 FROM INTRANET_USER as a
	                                      LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
	                                 WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1)
	                                       AND b.RecordID IS NULL";
	                                       */
	                  $sql = "SELECT COUNT(a.UserID)
                           FROM 
                           	INTRANET_USER as a ";
                    if ($this->EnableEntryLeavePeriod) {
				             	$sql .= "
				             	        INNER JOIN 
				                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
				                      on a.UserID = selp.UserID 
				                      	AND 
				                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
				                      ";
				            }
                    $sql .= "       	
                            LEFT OUTER JOIN 
                            $card_log_table_name as b 
                            ON a.UserID = b.UserID AND b.DayNumber = '$day'
                           WHERE 
                           	a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                            AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.")";
	              }
	              else
	              {
	                  $class_list = "'".implode("','",$temp)."'";
	                  $sql = "SELECT COUNT(a.UserID)
                           FROM 
                           	INTRANET_USER as a ";
                    if ($this->EnableEntryLeavePeriod) {
				             	$sql .= "
				             	        INNER JOIN 
				                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
				                      on a.UserID = selp.UserID 
				                      	AND 
				                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
				                      ";
				            }
                    $sql .= "
                            LEFT OUTER JOIN 
                            $card_log_table_name as b 
                            ON 
                            	a.UserID = b.UserID 
                            	AND b.DayNumber = '$day'
                           WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                 AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.")
                                 AND a.ClassName NOT IN ($class_list)";
	                                       # b.RecordID IS NULL
	              }
	          }
	          else
	          {
	              # Get from Lunch outers list
	              $sql = "SELECT COUNT(b.UserID)
                       FROM 
                       	CARD_STUDENT_LUNCH_ALLOW_LIST as a
                        INNER JOIN  
                        INTRANET_USER as b 
                        ON a.StudentID = b.UserID
                        	AND b.RecordType = 2 
                        	AND b.RecordStatus IN (0,1,2) ";
                if ($this->EnableEntryLeavePeriod) {
		             	$sql .= "
		             	        INNER JOIN 
		                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
		                      on b.UserID = selp.UserID 
		                      	AND 
		                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
		                      ";
		            }
		            $sql .= "
                        LEFT OUTER JOIN 
                        $card_log_table_name as c 
                        ON a.StudentID = c.UserID AND c.DayNumber = '$day'
                       WHERE 
                        c.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2))";
	          }
	          //echo "<BR>".$sql;
	          $temp = $this->returnVector($sql);
	          $total = $temp[0]+0;
	          //echo $total;
	          return array($gone_out, $back, $total-$gone_out-$back);
	     }
	     else if ($type == 2)
	     {
	          //$sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
	          //               WHERE DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") )) OR (LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND DayNumber = '$day')";
	          //$sql = "  SELECT
	          //                                        COUNT(RecordID) FROM $card_log_table_name
	          //                        WHERE
	          //                                        DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.")))
	          //                                        OR (LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND DayNumber = '$day')
	          //                                        OR (LunchOutTime IS NULL AND LunchBackTime IS NULL AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND PMStatus IN(".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING."))";
	          $sql = "SELECT COUNT(RecordID) 
	          				FROM $card_log_table_name log ";
	          if ($this->EnableEntryLeavePeriod) {
             	$sql .= "
             	        INNER JOIN 
                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                      on log.UserID = selp.UserID 
                      	AND 
                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                      ";
            }
            $sql .= "
	                  WHERE 
	                  	log.DayNumber = '$day' AND
                      (log.DayNumber = '$day' AND log.LeaveSchoolTime IS NOT NULL OR log.LeaveStatus IS NOT NULL) OR
                      (log.DayNumber = '$day' AND log.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND log.PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING.") AND log.LeaveStatus IS NULL) OR
                      (log.DayNumber = '$day' AND log.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND log.PMStatus IS NULL AND log.LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))";
	          //echo "<BR>".$sql;
	          $temp = $this->returnVector($sql);
	          $left = $temp[0]+0;
	
	          $sql = "SELECT DISTINCT UserID 
	          				FROM $card_log_table_name log ";
	          if ($this->EnableEntryLeavePeriod) {
             	$sql .= "
             	        INNER JOIN 
                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                      on log.UserID = selp.UserID 
                      	AND 
                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                      ";
            }
            $sql .= "
	                  WHERE log.DayNumber = '$day' AND
                      (log.DayNumber = '$day' AND log.LeaveSchoolTime IS NOT NULL OR log.LeaveStatus IS NOT NULL) OR
                      (log.DayNumber = '$day' AND log.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND log.PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING.") AND log.LeaveStatus IS NULL) OR
                      (log.DayNumber = '$day' AND log.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND log.PMStatus IS NULL AND log.LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))";
	          $result = $this->returnVector($sql);
	          if(sizeof($result)>0)
	          {
              $list = implode(",",$result);
              $cond = " AND log.UserID NOT IN ($list)";
	          }
	          else
	          {
	            $cond = " ";
	          }
	          //$sql = "SELECT DISTINCT UserID from $card_log_table_name WHERE DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL OR AMStatus IN (0,2) OR PMStatus IN (0,2)) AND UserID NOT IN ($list)";
	          $sql = "SELECT COUNT(log.RecordID) 
	          				FROM 
	          					$card_log_table_name log";
	         	if ($this->EnableEntryLeavePeriod) {
             	$sql .= "
             	        INNER JOIN 
                      CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
                      on log.UserID = selp.UserID 
                      	AND 
                      	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
                      ";
            }
            $sql .= "
	                  WHERE 
	                  	log.DayNumber = '$day' AND
                     log.LeaveSchoolTime IS NULL AND
                     log.LeaveStatus IS NULL AND
                     (log.DayNumber = '$day' AND log.LeaveSchoolTime IS NULL AND (log.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR log.PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ")))
                     $cond
	                         ";
	          //debug_r($sql);
	          $temp = $this->returnVector($sql);
	          $rest = $temp[0]+0;
	          return array($left,$rest);
	     }
	     else
	     {
	         return;
	     }
	}
	# 0 - In School status (morning)
	# 1 - Lunch Status
	# 2 - After School Status
	function getSummaryDetail($type)
	{
	     if ($this->attendance_mode=="NO")
	     {
	         $this->retrieveSettings();
	     }
	     /*
	     if ($this->attendance_mode!=2 && $type==2)
	     {
	         $type = 0;
	     }
	     */
	     if ($type != 0 && $type != 1 && $type!=2)
	     {
	         $type = 0;
	     }
	     $year=date("Y");
	     $month=date("m");
	     $day = date('d');
	
	     $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	     $status_field = ($this->attendance_mode==1?"PMStatus":"AMStatus");
	
	     if ($type == 0)  # Back School Time check
	     {
	         # Get Already back
	         // $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
	         //               WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR $status_field IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") )";
	         //$sql = "SELECT UserID FROM $card_log_table_name
	         //              WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR $status_field IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") )";
	         $sql = "SELECT UserID FROM $card_log_table_name
	                       WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR AMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE."))";
	         $temp = $this->returnVector($sql);
	         $back = $temp;
	         if(sizeof($back)>0){
	                 $back_student_list = implode(",",$temp);
	                 $back_conds = " AND a.UserID NOT IN($back_student_list) ";
	             }
	
	         # Get not yet back
	         # Get no need to take attendance classes
	
	         /*
	         $sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
	                        LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
	                        WHERE b.Mode = 2";
	         $temp = $this->returnVector($sql);
	         */
	         $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
	         if (sizeof($temp)==0)
	         {
	             $sql = "SELECT a.UserID
	                        FROM INTRANET_USER as a
	                             LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
	                             LEFT OUTER JOIN
								 YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
								 YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
								 YEAR y ON (y.YearID=yc.YearID)
	                        WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $back_conds AND yc.AcademicYearID=".get_Current_Academic_Year_ID();
	         }
	         else
	         {
	                 for($j=0;$j<sizeof($temp);$j++){
	                         $t[] = $temp[$j][1];
	                     }
	             $class_list = "'".implode("','",$t)."'";
	             $sql = "SELECT a.UserID
	                        FROM INTRANET_USER as a
	                             LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
	                             LEFT OUTER JOIN
								 YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
								 YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
								 YEAR y ON (y.YearID=yc.YearID)
	                        WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $back_conds
	                                 AND a.ClassName NOT IN ($class_list) AND yc.AcademicYearID=".get_Current_Academic_Year_ID();
	         }
	         $notyet = $this->returnVector($sql);
	         return array($back,$notyet);
	     }
	     else if ($type == 1) # Out for lunch check
	     {
	          # Gone out
	          $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
	                         WHERE DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND LeaveStatus IS NULL AND (PMStatus IS NULL OR PMStatus IN (1,3))";
	          $gone_out = $this->returnVector($sql);
	
	          # back school
	          //$sql = "SELECT DISTINCT UserID FROM $card_log_table_name
	          //               WHERE DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NOT NULL";
	          //$sql = "SELECT DISTINCT UserID FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND DayNumber = '$day')
	          //               WHERE (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NOT NULL AND LeaveStatus NOT IN (1)) OR (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";
	                                      $sql = "SELECT DISTINCT UserID FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND DayNumber = '$day')
	                         WHERE (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NOT NULL AND LeaveStatus IS NULL) OR (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";
	          $back = $this->returnVector($sql);
	
	          # Not gone out
	          # Can go out students - $gone_out - $back
	          global $intranet_root;
	          //$content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
	          //$lunch_misc_settings = explode("\n",$content_lunch_misc);
	          //list($lunch_misc_no_record,$lunch_misc_lunch_once, $lunch_misc_all_allow) = $lunch_misc_settings;
				
			  $lunch_misc_no_record = $this->NoRecordLunchOut;
			  $lunch_misc_lunch_once = $this->LunchOutOnce;
			  $lunch_misc_all_allow = $this->AllAllowGoOut;
				
	          if ($lunch_misc_all_allow)
	          {
	              # Get no need to take attendance classes
	              //$sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
	              //               LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
	              //               WHERE b.Mode = 2";
	              //$temp = $this->returnVector($sql);
	              $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
	              $minus_cond = "";
	              if (sizeof($back)!=0)
	              {
	                  $minus_cond .= " AND a.UserID NOT IN (".implode(",",$back).")";
	              }
	              if (sizeof($gone_out)!=0)
	              {
	                  $minus_cond .= " AND a.UserID NOT IN (".implode(",",$gone_out).")";
	              }
	              if (sizeof($temp)==0)
	              {
	                  $sql = "SELECT a.UserID
	                                 FROM INTRANET_USER as a
	                                      LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
	                                 WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
	                                       AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LeaveStatus IS NULL $minus_cond";
	
	              }
	              else
	              {
	                  $class_list = "'".implode("','",$temp)."'";
	                  $sql = "SELECT a.UserID
	                                 FROM INTRANET_USER as a
	                                      LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
	                                 WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
	                                       AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.ClassName NOT IN ($class_list) AND LeaveStatus IS NULL $minus_cond";
	              }
	          }
	          else
	          {
	              $minus_cond = "";
	              if (sizeof($back)!=0)
	              {
	                  $minus_cond .= " AND b.UserID NOT IN (".implode(",",$back).")";
	              }
	              if (sizeof($gone_out)!=0)
	              {
	                  $minus_cond .= " AND b.UserID NOT IN (".implode(",",$gone_out).")";
	              }
	              # Get from Lunch outers list
	              $sql = "SELECT b.UserID
	                             FROM CARD_STUDENT_LUNCH_ALLOW_LIST as a
	                                  LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	                                       AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
	                                  LEFT OUTER JOIN $card_log_table_name as c ON a.StudentID = c.UserID AND c.DayNumber = '$day'
	                             WHERE b.UserID IS NOT NULL AND c.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2)) $minus_cond";
	          	
	          }
	          $rest = $this->returnVector($sql);
	          return array($gone_out, $back, $rest);
	     }
	     else if ($type == 2)
	     {
	                     //$sql = "SELECT DISTINCT UserID FROM $card_log_table_name
	          //               WHERE DayNumber = '$day' AND LeaveSchoolTime IS NOT NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") ) OR (LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND Daynumber = '$day')";
	          $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
	                                  WHERE DayNumber = '$day' AND
	                                                            (DayNumber = '$day' AND LeaveSchoolTime IS NOT NULL OR LeaveStatus IS NOT NULL) OR
	                                                            (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING.") AND LeaveStatus IS NULL) OR
	                                                            (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IS NULL AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))";
	          $left = $this->returnVector($sql);
	          if(sizeof($left)>0)
	          {
	                  $list = implode(",",$left);
	                  $cond = " AND UserID NOT IN ($list)";
	          }
	          else
	          {
	                  $cond = " ";
	          }
	          //$sql = "SELECT DISTINCT UserID from $card_log_table_name WHERE DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL OR AMStatus IN (0,2) OR PMStatus IN (0,2)) AND UserID NOT IN ($list)";
	          $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
	                         WHERE DayNumber = '$day' AND
	                         LeaveSchoolTime IS NULL AND
	                         LeaveStatus IS NULL AND
	                         (DayNumber = '$day' AND LeaveSchoolTime IS NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ")))
	                         $cond
	                         ";
	          $rest = $this->returnVector($sql);
	          return array($left,$rest);
	     }
	     else
	     {
	         return;
	     }
	}
                
           function getWordList($type)
           {
                    $content = trim(get_file_content($this->word_base_dir.$this->file_array[$type]));
                    $array = explode("\n",$content);
                    for ($i=0; $i<sizeof($array); $i++)
                    {
                         $array[$i] = trim($array[$i]);
                    }
                    return $array;
           }
           function getRecordYear()
           {
                    $sql = "SELECT DISTINCT Year FROM CARD_STUDENT_RECORD_DATE_STORAGE ORDER BY Year";
                    return $this->returnVector($sql);
           }

           function retrieveMonthAttendanceByClass ($ClassName, $year, $month)
           {
                   $sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$ClassName' AND RecordType = 2 AND RecordStatus IN (0,1)";
                   $temp = $this->returnVector($sql);
                   $list = implode(",", $temp);

                   # Get Daily Records
                   $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                   $sql = "SELECT DayNumber, AMStatus, PMStatus
                                                           FROM $card_log_table_name
                                                           WHERE UserID IN ($list)
                                                           ORDER BY DayNumber";

                                $log_entries = $this->returnArray($sql, 3);
                                for ($i=0; $i<sizeof($log_entries); $i++)
                                {
                                        list($day, $am, $pm) = $log_entries[$i];
                                        $result[$am]++;
                                        $result[$pm]++;
                                }
                                return $result;
           }

	function retrieveClassMonthData($ClassName, $year, $month)
	{
	  $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	
	  $sql = "SELECT 
	            a.UserID, 
	            b.DayNumber, 
	            b.AMStatus, 
	            b.PMStatus, 
	            b.LeaveStatus,
							c.RecordStatus as am_late_waive,
							d.RecordStatus as pm_late_waive,
							e.RecordStatus as am_absent_waive,
							f.RecordStatus as pm_absent_waive,
							g.RecordStatus as am_early_leave_waive,
							h.RecordStatus as pm_early_leave_waive,
							c.Reason as am_late_reason,
							d.Reason as pm_late_reason,
							e.Reason as am_absent_reason,
							f.Reason as pm_absent_reason,
							g.Reason as am_early_leave_reason,
							h.Reason as pm_early_leave_reason,
							i.LateSession as am_late_session,
							i.RequestLeaveSession as am_request_leave_session,
							i.PlayTruantSession as am_play_truant_session,
							i.OfficalLeaveSession as am_offical_leave_session,
							i.AbsentSession as am_absent_session,
							j.LateSession as pm_late_session,
							j.RequestLeaveSession as pm_request_leave_session,
							j.PlayTruantSession as pm_play_truant_session,
							j.OfficalLeaveSession as pm_offical_leave_session,
							j.AbsentSession as pm_absent_session 
						FROM 
							INTRANET_USER as a
							INNER JOIN 
							$card_log_table_name as b 
							ON a.UserID = b.UserID 
								AND a.ClassName = '".$ClassName."' 
								AND a.RecordType = 2
								AND a.RecordStatus IN (0,1,2)  ";
		if ($this->EnableEntryLeavePeriod) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on b.UserID = selp.UserID 
								and 
								DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
							";
		}
		$sql .= "
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
							ON 
								c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND c.StudentID = a.UserID 
								AND c.DayType = '".PROFILE_DAY_TYPE_AM."' AND c.RecordType = '".PROFILE_TYPE_LATE."' 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
							ON 
								d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND d.StudentID = a.UserID 
								AND d.DayType = '".PROFILE_DAY_TYPE_PM."' AND d.RecordType = '".PROFILE_TYPE_LATE."' 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
							ON 
								e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND e.StudentID = a.UserID 
								AND e.DayType = '".PROFILE_DAY_TYPE_AM."' AND e.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.AMStatus='".PROFILE_TYPE_ABSENT."' AND b.AMStatus=e.RecordType 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
							ON 
								f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND f.StudentID = a.UserID 
								AND f.DayType = '".PROFILE_DAY_TYPE_PM."' AND f.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.PMStatus='".PROFILE_TYPE_ABSENT."' AND b.PMStatus=f.RecordType 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
							ON 
								g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND g.StudentID = a.UserID 
								AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
							ON 
								h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND h.StudentID = a.UserID 
								AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
							LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
							ON 
								i.StudentID = a.UserID 
								AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
								AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
								AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
							LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
							ON 
								j.StudentID = a.UserID 
								AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
								AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
								AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
					ORDER BY b.DayNumber";
	  
	  $log_entries = $this->returnArray($sql,21);
	
	  for ($i=0; $i<sizeof($log_entries); $i++)
	  {
			list ($studentid, $day, $am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $log_entries[$i];
			$result[$studentid][$day] = array($am,$pm,$leave,$am_late_waive,$pm_late_waive,$am_absent_waive,$pm_absent_waive,$am_early_waive,$pm_early_waive,$am_late_reason,$pm_late_reason,$am_absent_reason,$pm_absent_reason,$am_early_leave_reason,$pm_early_leave_reason,$am_late_session,$am_request_leave_session,$am_play_truant_session,$am_offical_leave_session,$am_absent_session,$pm_late_session,$pm_request_leave_session,$pm_play_truant_session,$pm_offical_leave_session,$pm_absent_session);
		}
	  return $result;
	}

	   function retrieveGroupMonthData($group_id, $year, $month)
	   {
	            # Get Student List
	            $sql = "SELECT a.UserID FROM INTRANET_USER a, INTRANET_GROUP b, INTRANET_USERGROUP c WHERE a.RecordStatus IN (0,1,2) AND a.RecordType = 2 AND b.groupid = '$group_id' AND b.groupid = c.groupid AND c.userid = a.userid";
	            $temp = $this->returnVector($sql);
	            $list = sizeof($temp)>0?implode(",",$temp):"-1";
	            # Get Daily Records
	            $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	           	
				$sql = "SELECT 
			            b.UserID, 
			            b.DayNumber, 
			            b.AMStatus, 
			            b.PMStatus, 
			            b.LeaveStatus,
									c.RecordStatus as am_late_waive,
									d.RecordStatus as pm_late_waive,
									e.RecordStatus as am_absent_waive,
									f.RecordStatus as pm_absent_waive,
									g.RecordStatus as am_early_leave_waive,
									h.RecordStatus as pm_early_leave_waive,
									c.Reason as am_late_reason,
									d.Reason as pm_late_reason,
									e.Reason as am_absent_reason,
									f.Reason as pm_absent_reason,
									g.Reason as am_early_leave_reason,
									h.Reason as pm_early_leave_reason,
									i.LateSession as am_late_session,
									i.RequestLeaveSession as am_request_leave_session,
									i.PlayTruantSession as am_play_truant_session,
									i.OfficalLeaveSession as am_offical_leave_session,
									i.AbsentSession as am_absent_session,
									j.LateSession as pm_late_session,
									j.RequestLeaveSession as pm_request_leave_session,
									j.PlayTruantSession as pm_play_truant_session,
									j.OfficalLeaveSession as pm_offical_leave_session,
									j.AbsentSession as pm_absent_session 
								FROM 
									INTRANET_USER as a 
									INNER JOIN 
									$card_log_table_name as b 
									on 
										a.UserID IN ($list) 
										AND 
										a.UserID = b.UserID ";
				if ($this->EnableEntryLeavePeriod) {
					$sql .= "
									INNER JOIN 
									CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
									on b.UserID = selp.UserID 
										and 
										DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
									";
				}
				$sql .= "
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
									ON 
										c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND c.StudentID = b.UserID 
										AND c.DayType = '".PROFILE_DAY_TYPE_AM."' AND c.RecordType = '".PROFILE_TYPE_LATE."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
									ON 
										d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND d.StudentID = b.UserID 
										AND d.DayType = '".PROFILE_DAY_TYPE_PM."' AND d.RecordType = '".PROFILE_TYPE_LATE."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
									ON 
										e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND e.StudentID = b.UserID 
										AND e.DayType = '".PROFILE_DAY_TYPE_AM."' AND e.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.AMStatus='".PROFILE_TYPE_ABSENT."' AND b.AMStatus=e.RecordType 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
									ON 
										f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND f.StudentID = b.UserID 
										AND f.DayType = '".PROFILE_DAY_TYPE_PM."' AND f.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.PMStatus='".PROFILE_TYPE_ABSENT."' AND b.PMStatus=f.RecordType 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
									ON 
										g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND g.StudentID = b.UserID 
										AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
									ON 
										h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND h.StudentID = b.UserID 
										AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
									LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
									ON 
										i.StudentID = b.UserID 
										AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
										AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
										AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
									LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
									ON 
										j.StudentID = b.UserID 
										AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
										AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
										AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
								ORDER BY b.DayNumber";
				if($list!="") $log_entries = $this->returnArray($sql,21);
	            for ($i=0; $i<sizeof($log_entries); $i++)
				{
				       list ($studentid, $day, $am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $log_entries[$i];
				       $result[$studentid][$day] = array($am,$pm,$leave,$am_late_waive,$pm_late_waive,$am_absent_waive,$pm_absent_waive,$am_early_waive,$pm_early_waive,$am_late_reason,$pm_late_reason,$am_absent_reason,$pm_absent_reason,$am_early_leave_reason,$pm_early_leave_reason,$am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session);
				}
	            return $result;
	   }
	
	function retrieveCustomizedIndividualMonthData($UserList, $year,$month) {
		$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
		
		$UserList = (sizeof($UserList) > 0)? '\''.implode('\',\'',$UserList).'\'':'-1';
	  $sql = "SELECT 
	            b.UserID, 
	            b.DayNumber, 
	            b.AMStatus, 
	            b.PMStatus, 
	            b.LeaveStatus,
							c.RecordStatus as am_late_waive,
							d.RecordStatus as pm_late_waive,
							e.RecordStatus as am_absent_waive,
							f.RecordStatus as pm_absent_waive,
							g.RecordStatus as am_early_leave_waive,
							h.RecordStatus as pm_early_leave_waive,
							c.Reason as am_late_reason,
							d.Reason as pm_late_reason,
							e.Reason as am_absent_reason,
							f.Reason as pm_absent_reason,
							g.Reason as am_early_leave_reason,
							h.Reason as pm_early_leave_reason,
							i.LateSession as am_late_session,
							i.RequestLeaveSession as am_request_leave_session,
							i.PlayTruantSession as am_play_truant_session,
							i.OfficalLeaveSession as am_offical_leave_session,
							i.AbsentSession as am_absent_session,
							j.LateSession as pm_late_session,
							j.RequestLeaveSession as pm_request_leave_session,
							j.PlayTruantSession as pm_play_truant_session,
							j.OfficalLeaveSession as pm_offical_leave_session,
							j.AbsentSession as pm_absent_session 
						FROM 
							$card_log_table_name as b ";
		if ($this->EnableEntryLeavePeriod) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on b.UserID = selp.UserID 
								and 
								DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
							";
		}
		$sql .= "
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
							ON 
								c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND c.StudentID = b.UserID 
								AND c.DayType = '".PROFILE_DAY_TYPE_AM."' AND c.RecordType = '".PROFILE_TYPE_LATE."' 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
							ON 
								d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND d.StudentID = b.UserID 
								AND d.DayType = '".PROFILE_DAY_TYPE_PM."' AND d.RecordType = '".PROFILE_TYPE_LATE."' 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
							ON 
								e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND e.StudentID = b.UserID 
								AND e.DayType = '".PROFILE_DAY_TYPE_AM."' AND e.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.AMStatus='".PROFILE_TYPE_ABSENT."' AND e.RecordType=b.AMStatus 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
							ON 
								f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND f.StudentID = b.UserID 
								AND f.DayType = '".PROFILE_DAY_TYPE_PM."' AND f.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.PMStatus='".PROFILE_TYPE_ABSENT."' AND f.RecordType=b.PMStatus 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
							ON 
								g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND g.StudentID = b.UserID 
								AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
							LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
							ON 
								h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
								AND h.StudentID = b.UserID 
								AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
							LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
							ON 
								i.StudentID = b.UserID 
								AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
								AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
								AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
							LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
							ON 
								j.StudentID = b.UserID 
								AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
								AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
								AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
					where 
						b.UserID in (".$UserList.") 
					ORDER BY b.DayNumber";
		
	  $log_entries = $this->returnArray($sql,21);
	
	  for ($i=0; $i<sizeof($log_entries); $i++)
	  {
			list ($studentid, $day, $am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $log_entries[$i];
			$result[$studentid][$day] = array($am,$pm,$leave,$am_late_waive,$pm_late_waive,$am_absent_waive,$pm_absent_waive,$am_early_waive,$pm_early_waive,$am_late_reason,$pm_late_reason,$am_absent_reason,$pm_absent_reason,$am_early_leave_reason,$pm_early_leave_reason,$am_late_session,$am_request_leave_session,$am_play_truant_session,$am_offical_leave_session,$am_absent_session,$pm_late_session,$pm_request_leave_session,$pm_play_truant_session,$pm_offical_leave_session,$pm_absent_session);
		}
	  return $result;
	}

	function retrieveClassDayData($ClassName, $year, $month, $day)
	{
		global $sys_custom;
		
    $thisDay = $year ."-". $month ."-" . $day;
    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
    $namefield = getNameFieldByLang("a.");
    //$namefield = "a.ChineseName,a.EnglishName";
    $sql = "SELECT 
						a.UserID, $namefield, a.ClassName, a.ClassNumber,
						b.InSchoolTime, b.InSchoolStation, b.AMStatus,
						b.LunchOutTime, b.LunchOutStation, b.LunchBackTime,
						b.LunchBackStation, b.PMStatus, b.LeaveSchoolTime,
						b.LeaveSchoolStation, b.LeaveStatus,
						c.StudentAttendanceID as am_late_waive,
						d.StudentAttendanceID as pm_late_waive,
						e.RecordStatus as am_absent_waive,
						f.RecordStatus as pm_absent_waive,
						g.RecordStatus as am_early_leave_waive,
						h.RecordStatus as pm_early_leave_waive,
						i.LateSession as am_late_session,
						i.RequestLeaveSession as am_request_leave_session,
						i.PlayTruantSession as am_play_truant_session,
						i.OfficalLeaveSession as am_offical_leave_session,
						j.LateSession as pm_late_session,
						j.RequestLeaveSession as pm_request_leave_session,
						j.PlayTruantSession as pm_play_truant_session,
						j.OfficalLeaveSession as pm_offical_leave_session ";
		//if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin']) {
			$sql .= "
						,a.UserLogin ";
		//}
		$sql .= "
           FROM 
           	INTRANET_USER as a ";
    if ($this->EnableEntryLeavePeriod) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on a.UserID = selp.UserID 
								and 
								'".$thisDay."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
							";
		}
		$sql .= "
            LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day' 
            LEFT OUTER JOIN PROFILE_STUDENT_ATTENDANCE as c ON c.UserID = a.UserID AND c.AttendanceDate = '$thisDay' AND c.DayType = 2 AND c.RecordType = '2'
            LEFT OUTER JOIN PROFILE_STUDENT_ATTENDANCE as d ON d.UserID = a.UserID AND d.AttendanceDate = '$thisDay' AND d.DayType = 3 AND d.RecordType = '2'
            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e ON e.StudentID = a.UserID AND e.RecordDate = '$thisDay' AND e.DayType = 2 AND e.RecordType = '1' AND b.AMStatus='1' AND b.AMStatus=e.RecordType 
            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f ON f.StudentID = a.UserID AND f.RecordDate = '$thisDay' AND f.DayType = 3 AND f.RecordType = '1' AND b.PMStatus='1' AND b.PMStatus=f.RecordType 
            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g ON g.StudentID = a.UserID AND g.RecordDate = '$thisDay' AND g.DayType = 2 AND g.RecordType = '3'
            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h ON h.StudentID = a.UserID AND h.RecordDate = '$thisDay' AND h.DayType = 3 AND h.RecordType = '3'
            LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
						ON 
							i.StudentID = a.UserID 
							AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
							AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
							AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
						LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
						ON 
							j.StudentID = a.UserID 
							AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
							AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
							AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
           WHERE a.ClassName = '$ClassName' AND a.RecordType = 2
                 AND a.RecordStatus IN (0,1,2)
           ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
                   
    return $this->returnArray($sql,30);
	}
	
	function retrieveGroupDayData($GroupID, $year, $month, $day)
	{
		global $sys_custom;
		
    	$thisDay = $year ."-". $month ."-" . $day;
    	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
    	$namefield = getNameFieldByLang("a.");
    	
    	$sql = "SELECT 
						a.UserID, $namefield, a.ClassName, a.ClassNumber,
						b.InSchoolTime, b.InSchoolStation, b.AMStatus,
						b.LunchOutTime, b.LunchOutStation, b.LunchBackTime,
						b.LunchBackStation, b.PMStatus, b.LeaveSchoolTime,
						b.LeaveSchoolStation, b.LeaveStatus,
						c.StudentAttendanceID as am_late_waive,
						d.StudentAttendanceID as pm_late_waive,
						e.RecordStatus as am_absent_waive,
						f.RecordStatus as pm_absent_waive,
						g.RecordStatus as am_early_leave_waive,
						h.RecordStatus as pm_early_leave_waive,
						i.LateSession as am_late_session,
						i.RequestLeaveSession as am_request_leave_session,
						i.PlayTruantSession as am_play_truant_session,
						i.OfficalLeaveSession as am_offical_leave_session,
						j.LateSession as pm_late_session,
						j.RequestLeaveSession as pm_request_leave_session,
						j.PlayTruantSession as pm_play_truant_session,
						j.OfficalLeaveSession as pm_offical_leave_session ";
		//if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin']) {
			$sql .= "
						,a.UserLogin ";
		//}
		$sql .= "FROM 
	           	INTRANET_USER as a 
				INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID";
	    if ($this->EnableEntryLeavePeriod) {
				$sql .= "
								INNER JOIN 
								CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
								on a.UserID = selp.UserID 
									and 
									'".$thisDay."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
								";
			}
			$sql .= "
	            LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day' 
	            LEFT OUTER JOIN PROFILE_STUDENT_ATTENDANCE as c ON c.UserID = a.UserID AND c.AttendanceDate = '$thisDay' AND c.DayType = 2 AND c.RecordType = '2'
	            LEFT OUTER JOIN PROFILE_STUDENT_ATTENDANCE as d ON d.UserID = a.UserID AND d.AttendanceDate = '$thisDay' AND d.DayType = 3 AND d.RecordType = '2'
	            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e ON e.StudentID = a.UserID AND e.RecordDate = '$thisDay' AND e.DayType = 2 AND e.RecordType = '1' AND b.AMStatus='1' AND b.AMStatus=e.RecordType 
	            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f ON f.StudentID = a.UserID AND f.RecordDate = '$thisDay' AND f.DayType = 3 AND f.RecordType = '1' AND b.PMStatus='1' AND b.PMStatus=f.RecordType 
	            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g ON g.StudentID = a.UserID AND g.RecordDate = '$thisDay' AND g.DayType = 2 AND g.RecordType = '3'
	            LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h ON h.StudentID = a.UserID AND h.RecordDate = '$thisDay' AND h.DayType = 3 AND h.RecordType = '3'
	            LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
							ON 
								i.StudentID = a.UserID 
								AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
								AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
								AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
							LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
							ON 
								j.StudentID = a.UserID 
								AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
								AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
								AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
	           WHERE ug.GroupID='$GroupID' AND a.RecordType = 2
	                 AND a.RecordStatus IN (0,1,2)
	           ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
	                   
	    return $this->returnArray($sql,30);
	}
	
	function retrieveBadRecordsCountByClass($StartDate, $EndDate, $BadType)
	{
		# Get Student List By Class
		$sql = "SELECT ClassName, UserID FROM INTRANET_USER
		               WHERE ClassName != '' AND RecordType = 2 AND RecordStatus IN (0,1,2)
		               ORDER BY ClassName";
		$students = $this->returnArray($sql,2);
		
		# Group students into class
		for ($i=0; $i<sizeof($students); $i++)
		{
			list($classname, $uid) = $students[$i];
			$classes[$classname][] = $uid;
		}
		
		# For each class to get
		if (sizeof($classes) > 0)
		foreach ($classes as $classname => $id_array)
		{
			# Get Record Count
			$id_list = implode(",",$id_array);
			$sql = "SELECT 
								COUNT(*) 
							FROM 
								CARD_STUDENT_BAD_ACTION a ";
			if ($this->EnableEntryLeavePeriod) {
				$sql .= "
								INNER JOIN 
								CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
								on 
									a.StudentID IN ($id_list) 
									and 
									a.RecordType = '$BadType' 
									and 
									a.StudentID = selp.UserID 
									and 
									a.RecordDate between '$StartDate' and '$EndDate 23:59:59' 
									and 
									a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
								";
			}
			else {
				$sql .= "					
				        WHERE 
				        	a.StudentID IN ($id_list) 
									AND a.RecordDate between '$StartDate' and '$EndDate 23:59:59' 
									AND a.RecordType = '$BadType'
							";
			}
			$temp = $this->returnVector($sql);
			$record_count = $temp[0]+0;
			$student_count = sizeof($id_array);
			$result[] = array($classname,$student_count,$record_count);
		}
		return $result;
	}
	function retrieveTopBadRecordsCountByStudent($StartDate, $EndDate, $BadType, $TopNumber)
	{
		# Get the count first
		$sql = "SELECT 
							a.StudentID, 
							COUNT(a.RecordID) as num
		        FROM 
		        	CARD_STUDENT_BAD_ACTION a 
					INNER JOIN INTRANET_USER as u ON a.StudentID = u.UserID and u.RecordType = 2 ";
		if ($this->EnableEntryLeavePeriod) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on 
								a.RecordType = '$BadType' 
								and 
								a.StudentID = selp.UserID 
								and 
								a.RecordDate between '$StartDate' and '$EndDate 23:59:59' 
								and 
								a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
							";
		}
		else {
			$sql .= "
		        WHERE 
		        	a.RecordDate between '$StartDate' and '$EndDate 23:59:59' 
							AND a.RecordType = '$BadType' ";
		}
		$sql .= "
						GROUP BY a.StudentID
						ORDER BY num DESC
						LIMIT $TopNumber
		               ";
		$temp = $this->returnArray($sql,2);
		$record_count = $temp[sizeof($temp)-1][1];
		if ($record_count == 0) return;
		
		$namefield = getNameFieldByLang("b.");
		//$namefield = "b.ChineseName,b.EnglishName";
		$sql = "SELECT 
							a.StudentID, 
							$namefield, 
							b.ClassName, 
							b.ClassNumber, 
							COUNT(a.RecordID) as num
						FROM 
							CARD_STUDENT_BAD_ACTION as a ";
		if ($this->EnableEntryLeavePeriod) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on 
								a.StudentID = selp.UserID 
								and 
								a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
							";
		}
		$sql .= "
		          LEFT OUTER JOIN 
		          INTRANET_USER as b 
		          ON a.StudentID = b.UserID
						WHERE b.RecordType = 2 
						AND	a.RecordDate >= '$StartDate'
		        	AND a.RecordDate <= '$EndDate'
		          AND a.RecordType = '$BadType'
		        GROUP BY a.StudentID
		        HAVING num >= $record_count
		        ORDER BY num DESC, b.ClassName, b.ClassNumber+0, b.EnglishName
		        ";
		return $this->returnArray($sql,5);
	}
           function retrieveYearMonthList()
           {
                    $sql = "SELECT Year, Month FROM CARD_STUDENT_RECORD_DATE_STORAGE
                                   ORDER BY Year, Month";
                    return $this->returnArray($sql,2);
           }
           function removeMonthData($year, $month)
           {
              # Drop tables
              $sql = "DROP TABLE CARD_STUDENT_DAILY_LOG_".$year."_".$month;
              $Result['DropDailyLog'] = $this->db_db_query($sql);
              $sql = "DROP TABLE CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month;
              $Result['DropDailyClassConfirm'] = $this->db_db_query($sql);
              $sql = "DELETE FROM CARD_STUDENT_DAILY_DATA_CONFIRM
                             WHERE Year(RecordDate) = '$year' AND Month(RecordDate) = '$month'";
              $Result['DeleteDailyDataConfirm'] = $this->db_db_query($sql);
              $sql = "DELETE FROM CARD_STUDENT_RECORD_DATE_STORAGE WHERE Year = '$year' AND Month = '$month'";
              $Result['DeleteStudentRecordDateStorage'] = $this->db_db_query($sql);
              
              return !in_array(false,$Result);
           }

           function retrieveStudentDailyRecord($student_id, $target_date="")
           {
                    if ($target_date == "")
                    {
                        $target_date = date('Y-m-d');
                        $target_year = date('Y');
                        $target_month = date('m');
                        $target_day = date('d');
                    }
                    else
                    {
                        $ts = strtotime($target_date);
                        $target_year = date('Y', $ts);
                        $target_month = date('m', $ts);
                        $target_day = date('d', $ts);
                    }

                    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$target_year."_".$target_month;
                    $sql = "SELECT
                                  IF(InSchoolTime IS NULL, '-', InSchoolTime),
                                  IF(InSchoolStation IS NULL, '-', InSchoolStation),
                                  IF(AMStatus IS NOT NULL,
                                     AMStatus,
                                     CASE
                                         WHEN InSchoolTime IS NOT NULL THEN '".CARD_STATUS_PRESENT."'
                                         ELSE '".CARD_STATUS_ABSENT."'
                                     END
                                  ),
                                  IF(LunchOutTime IS NULL, '-', LunchOutTime),
                                  IF(LunchOutStation IS NULL, '-', LunchOutStation),
                                  IF(LunchBackTime IS NULL, '-', LunchBackTime),
                                  IF(LunchBackStation IS NULL, '-', LunchBackStation),
                                  IF(PMStatus IS NOT NULL,
                                     PMStatus,
                                     CASE
                                         WHEN (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LunchOutTime IS NULL OR LunchBackTime IS NOT NULL))
                                         THEN '".CARD_STATUS_PRESENT."'
                                         ELSE '".CARD_STATUS_ABSENT."'
                                     END
                                  ),
                                  IF(LeaveSchoolTime IS NULL, '-', LeaveSchoolTime),
                                  LeaveStatus
                                  FROM $card_log_table_name
                                  WHERE UserID = '$student_id' AND DayNumber = '$target_day'
                    ";
                    $temp = $this->returnArray($sql,10);
                    if (sizeof($temp)==0) return false;
                    return $temp[0];
           }
	/*
	 * @param bool $get_detail : true - get AM reason and remark, PM reason and remark; default false
	 */
	function retrieveStudentMonthlyRecord ($student_id, $year, $month, $order=1, $get_detail=false, $attendStatusDefaultAbsent=true)
	{
	  	$this->createTable_Card_Student_Daily_Log($year, $month);
	  	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	  	if ($attendStatusDefaultAbsent) {
	  	$amStatus = "IF(log.AMStatus IS NOT NULL,log.AMStatus,
								CASE
								   WHEN log.InSchoolTime IS NOT NULL THEN '".CARD_STATUS_PRESENT."'
								   ELSE '".CARD_STATUS_ABSENT."'
								END
							)";
		$pmStatus = "IF(log.PMStatus IS NOT NULL,log.PMStatus,
								CASE
								   WHEN (log.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (log.LunchOutTime IS NULL OR log.LunchBackTime IS NOT NULL))
								   THEN '".CARD_STATUS_PRESENT."'
								   ELSE '".CARD_STATUS_ABSENT."'
								END
	            )";
	  } else {
	  	$amStatus = "log.AMStatus";
	  	$pmStatus = "log.PMStatus";
	  }
	  
	    // Check and display school days attendance records for student and parent only
	  	if($this->iSmartCardOnlyDisplaySchoolDayAttendanceRecord==1 && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)){
			$sql = "SELECT DISTINCT u.ClassName, DATE_FORMAT(CONCAT('$year-$month-',c.DayNumber),'%Y-%m-%d') as RecordDate, c.DayNumber FROM INTRANET_USER as u INNER JOIN ".$card_log_table_name." as c ON u.UserID = c.UserID WHERE u.UserID = '$student_id' ORDER BY c.DayNumber";
			$class_days = $this->returnArray($sql);
			$day_numbers = array();
			for($i=0;$i<count($class_days);$i++){
				if($this->isRequiredToTakeAttendanceByDate($class_days[$i]['ClassName'],$class_days[$i]['RecordDate'])){
					$day_numbers[] = $class_days[$i]['DayNumber'];
				}
			}
			$DayNumber_cond = count($day_numbers)>0? " AND log.DayNumber IN (".implode(",",$day_numbers).") " : "";
		}
	  
	  $sql = "SELECT
							log.DayNumber,
							IF(log.InSchoolTime IS NULL, '-', log.InSchoolTime),
							IF(log.InSchoolStation IS NULL, '-', log.InSchoolStation),
							$amStatus,
							IF(log.LunchOutTime IS NULL, '-', log.LunchOutTime),
							IF(log.LunchOutStation IS NULL, '-', log.LunchOutStation),
							IF(log.LunchBackTime IS NULL, '-', log.LunchBackTime),
							IF(log.LunchBackStation IS NULL, '-', log.LunchBackStation),
							$pmStatus,
              IF(log.LeaveSchoolTime IS NULL, '-', log.LeaveSchoolTime),
              log.LeaveStatus ";
        if($get_detail) {
        	$sql .= ",pam.Reason as AMReason,ram.Remark as AMRemark,ppm.Reason as PMReason,rpm.Remark as PMRemark, pam.RecordStatus as AMWaive, ppm.RecordStatus as PMWaive, pe.Reason as EarlyLeaveReason, pe.RecordStatus as EarlyLeaveWaive ";
        }
		$sql.=" FROM 
							$card_log_table_name log ";
		if ($this->EnableEntryLeavePeriod) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on 
								log.UserID = selp.UserID 
								and 
								DATE(CONCAT('".$year."-".$month."-',log.DayNumber)) between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
							";
		}
		if($get_detail) {
			$sql .= "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as pam ON pam.StudentID=log.UserID 
						AND pam.RecordDate=DATE_FORMAT(CONCAT('$year-$month-',log.DayNumber),'%Y-%m-%d') AND pam.DayType='".PROFILE_DAY_TYPE_AM."' 
						AND ((log.AMStatus IN ('".CARD_STATUS_ABSENT."','".CARD_STATUS_OUTING."') AND pam.RecordType='".PROFILE_TYPE_ABSENT."') 
							OR (log.AMStatus='".CARD_STATUS_LATE."' AND pam.RecordType='".PROFILE_TYPE_LATE."'))
					LEFT JOIN CARD_STUDENT_DAILY_REMARK as ram ON ram.StudentID=log.UserID and ram.RecordDate=DATE_FORMAT(CONCAT('$year-$month-',log.DayNumber),'%Y-%m-%d') AND ram.DayType='".PROFILE_DAY_TYPE_AM."' 
					LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as ppm ON ppm.StudentID=log.UserID 
						AND ppm.RecordDate=DATE_FORMAT(CONCAT('$year-$month-',log.DayNumber),'%Y-%m-%d') AND ppm.DayType='".PROFILE_DAY_TYPE_PM."' 
						AND ((log.PMStatus IN ('".CARD_STATUS_ABSENT."','".CARD_STATUS_OUTING."') AND ppm.RecordType='".PROFILE_TYPE_ABSENT."') 
							OR (log.PMStatus='".CARD_STATUS_LATE."' AND ppm.RecordType='".PROFILE_TYPE_LATE."'))
					LEFT JOIN CARD_STUDENT_DAILY_REMARK as rpm ON rpm.StudentID=log.UserID and rpm.RecordDate=DATE_FORMAT(CONCAT('$year-$month-',log.DayNumber),'%Y-%m-%d') AND rpm.DayType='".PROFILE_DAY_TYPE_PM."' 
					LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as pe ON pe.StudentID=log.UserID 
						AND pe.RecordDate=DATE_FORMAT(CONCAT('$year-$month-',log.DayNumber),'%Y-%m-%d') 
						AND pe.RecordType='".PROFILE_TYPE_EARLY."' 
						AND (log.LeaveStatus='".CARD_LEAVE_AM."' OR log.LeaveStatus='".CARD_LEAVE_PM."') ";
					
		}
    $sql .= "        
    				WHERE 
            	log.UserID = '$student_id' $DayNumber_cond
            ORDER BY log.DayNumber
	  ";
	  if (!$order) $sql .= " DESC";
	  //debug_r($sql);
	  return $this->returnArray($sql,15);
	}
    function disallowInputInProfile(){
			//$GeneralSettings = new libgeneralsettings();
			$GeneralSettings = $this->GetLibGeneralSettings();

			$Settings = $GeneralSettings->Get_General_Setting('StudentAttendance',array("'DisallowProfileInput'"));
			$this->disallow_input_in_profile = $Settings['DisallowProfileInput'];
			
			return $this->disallow_input_in_profile;
		}

		function GET_MODULE_OBJ_ARR_TW()
		{
			global $PATH_WRT_ROOT, $ip20TopMenu, $CurrentPage, $LAYOUT_SKIN, $Lang, $module_version, $sys_custom, $plugin;

			global $i_StudentAttendance_Menu_DailyOperation, $i_SmartCard_DailyOperation_ViewClassStatus, $i_SmartCard_DailyOperation_ViewLateStatus, $i_SmartCard_DailyOperation_ViewAbsenceStatus,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus,$i_SmartCard_DailyOperation_Preset_Absence;
			global $i_StudentAttendance_Menu_DailyLessonOperation, $i_StudentAttendance_Menu_DataImport, $i_StudentAttendance_ImportCardID, $i_StudentAttendance_Menu_Report, $i_StudentAttendance_Report_ClassMonth, $button_find;
			global $i_StudentAttendance_Menu_ReportLesson;
			global $i_SmartCard_SystemSettings, $i_general_BasicSettings, $i_SmartCard_TerminalSettings,$i_StudentAttendance_TimeSlotSettings,$i_StudentAttendance_TimeSessionSettings, $i_wordtemplates_attendance;

			$PageDailyOperation= 0;
			$PageDailyOperation_ViewClassStatus = 0;
			$PageDailyOperation_ViewLateStatus = 0;
			$PageDailyOperation_ViewAbsenceStatus = 0;
			$PageDailyOperation_ViewEarlyLeaveStatus = 0;
			$PageDailyOperation_PresetAbsence = 0;
			$PageDailyOperation_ApplyLeaveApp = 0;
			$PageDailyLessonOperation= 0;
			$PageLessonAttendance_DailyOverview = 0;
			$PageLessonAttendance_DailyLateOverview = 0;
			$PageLessonAttendance_DailyAbsenceOverview = 0;
			$PageDataImport = 0;
			$PageDataImport_CardID = 0;
			$PageReport = 0;
			$PageReport_ClassMonth = 0;
			$PageLessonAttendance_LateSkipLessonRecords = 0;
			$PageLessonAttendance_SingleStudentSummaryReport = 0;
			$PageReport_Search = 0;
			$PageReportLesson = 0;
			$PageReport_ClassDetailAttendReport = 0;
			$PageLessonAttendance_LessonSummary = 0;
			$PageReport_LessonAttendSummary = 0;
			$PageReport_LessonNonconfirmedReport = 0;
			$PageSystemSetting = 0;
			$PageSystemSetting_BasicSettings = 0;
			$PageSystemSetting_BasicSettingsLesson = 0;
			$PageSystemSetting_LessonSettings = 0;
			$PageSystemSetting_LessonAttendanceReasons = 0;
			$PageSystemSetting_TerminalSettings = 0;
			$PageSystemSetting_TimeSlotSettings = 0;
			$PageSystemSetting_TimeSessionSettings = 0;
			$PageSystemSetting_ApplyLeaveAppSettings = 0;
			$PageSystemSetting_LeaveType = 0;
			$PageStudentAttendanceSettings_MessageTemplate = 0;
			$PageLessonAttendance_AttendanceWordTemplate = 0;
			switch ($CurrentPage) {
				case "PageDailyOperation_ViewClassStatus":
					$PageDailyOperation = 1;
					$PageDailyOperation_ViewClassStatus = 1;
					break;
				case "PageDailyOperation_ViewLateStatus":
					$PageDailyOperation= 1;
					$PageDailyOperation_ViewLateStatus = 1;
					break;
				case "PageDailyOperation_ViewAbsenceStatus":
					$PageDailyOperation= 1;
					$PageDailyOperation_ViewAbsenceStatus = 1;
					break;
				case "PageDailyOperation_ViewEarlyLeaveStatus":
					$PageDailyOperation= 1;
					$PageDailyOperation_ViewEarlyLeaveStatus = 1;
					break;
				case "PageDailyOperation_PresetAbsence":
					$PageDailyOperation= 1;
					$PageDailyOperation_PresetAbsence = 1;
					break;
				case "PageDailyOperation_ApplyLeaveApp":
					$PageDailyOperation = 1;
					$PageDailyOperation_ApplyLeaveApp = 1;
					break;
				case "PageLessonAttendance_DailyOverview":
					$PageDailyLessonOperation= 1;
					$PageLessonAttendance_DailyOverview = 1;
					break;
				case "PageLessonAttendance_DailyLateOverview":
					$PageDailyLessonOperation= 1;
					$PageLessonAttendance_DailyLateOverview = 1;
					break;
				case "PageLessonAttendance_DailyAbsenceOverview":
					$PageDailyLessonOperation= 1;
					$PageLessonAttendance_DailyAbsenceOverview = 1;
					break;
				case "PageDataImport_CardID":
					$PageDataImport = 1;
					$PageDataImport_CardID = 1;
					break;
				case "PageReport_ClassMonth":
					$PageReport = 1;
					$PageReport_ClassMonth = 1;
					break;
				case "PageLessonAttendance_LateSkipLessonRecords":
					$PageReport = 1;
					$PageLessonAttendance_LateSkipLessonRecords = 1;
					break;
				case "PageLessonAttendance_SingleStudentSummaryReport":
					$PageReport = 1;
					$PageLessonAttendance_SingleStudentSummaryReport = 1;
					break;
				case "PageReport_Search":
					$PageReport = 1;
					$PageReport_Search = 1;
					break;
				case "PageLessonAttendance_ClassDetailAttendReport":
					$PageReportLesson= 1;
					$PageReport_ClassDetailAttendReport = 1;
					break;
				case "PageLessonAttendance_LessonSummary":
					$PageReportLesson = 1;
					$PageLessonAttendance_LessonSummary = 1;
					break;
				case "PageLessonAttendance_LessonAttendSummary":
					$PageReportLesson= 1;
					$PageReport_LessonAttendSummary = 1;
					break;
				case "PageLessonAttendance_LessonNonconfirmedReport":
					$PageReportLesson = 1;
					$PageReport_LessonNonconfirmedReport = 1;
					break;
				case "PageSystemSetting_BasicSettings":
					$PageSystemSetting = 1;
					$PageSystemSetting_BasicSettings = 1;
					break;
				case "PageSystemSetting_LessonSettings":
					$PageSystemSetting = 1;
					$PageSystemSetting_LessonSettings = 1;
					$PageSystemSetting_BasicSettingsLesson = 1;
					break;
				case "PageSystemSetting_LessonAttendanceReasons":
					$PageSystemSetting = 1;
					$PageSystemSetting_LessonAttendanceReasons = 1;
					$PageSystemSetting_BasicSettingsLesson = 1;
					break;
				case "PageSystemSetting_TerminalSettings":
					$PageSystemSetting = 1;
					$PageSystemSetting_TerminalSettings = 1;
					break;
				case "PageSystemSetting_TimeSlotSettings":
					$PageSystemSetting = 1;
					$PageSystemSetting_TimeSlotSettings = 1;
					break;
				case "PageSystemSetting_TimeSessionSettings":
					$PageSystemSetting = 1;
					$PageSystemSetting_TimeSessionSettings = 1;
					break;
				case "PageSystemSetting_ApplyLeaveAppSettings":
					$PageSystemSetting = 1;
					$PageSystemSetting_ApplyLeaveAppSettings = 1;
					break;
				case "PageSystemSetting_LeaveType":
					$PageSystemSetting = 1;
					$PageSystemSetting_LeaveType = 1;
					break;
				case "PageStudentAttendanceSettings_MessageTemplate":
					$PageSystemSetting = 1;
					$PageStudentAttendanceSettings_MessageTemplate = 1;
					break;
				case "PageLessonAttendance_AttendanceWordTemplate":
					$PageSystemSetting= 1;
					$PageLessonAttendance_AttendanceWordTemplate = 1;
					break;
			}

			$StudentAttendancePath = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/";

			$MenuArr["DailyOperation"] = array($i_StudentAttendance_Menu_DailyOperation, "", $PageDailyOperation);

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				$MenuArr["DailyOperation"]["Child"]["ViewClassStatus"] = array($i_SmartCard_DailyOperation_ViewClassStatus, $StudentAttendancePath . "dailyoperation/class/class_status.php", $PageDailyOperation_ViewClassStatus);
				$MenuArr["DailyOperation"]["Child"]["ViewLateStatus"] = array($i_SmartCard_DailyOperation_ViewLateStatus, $StudentAttendancePath."dailyoperation/late/showlate.php", $PageDailyOperation_ViewLateStatus);
				$MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, $StudentAttendancePath."dailyoperation/absence/show.php", $PageDailyOperation_ViewAbsenceStatus);
				$MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"] = array($i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, $StudentAttendancePath."dailyoperation/early/show.php", $PageDailyOperation_ViewEarlyLeaveStatus);
				$MenuArr["DailyOperation"]["Child"]["PresetAbsence"] = array($i_SmartCard_DailyOperation_Preset_Absence, $StudentAttendancePath."dailyoperation/preset_absence/new.php", $PageDailyOperation_PresetAbsence);
				if ($plugin['eClassApp']) {
					$MenuArr["DailyOperation"]["Child"]["ApplyLeave(App)"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath . "dailyoperation/apply_leave/", $PageDailyOperation_ApplyLeaveApp);
				}
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
				$MenuArr["DailyLessonOperation"] = array($i_StudentAttendance_Menu_DailyLessonOperation, "", $PageDailyLessonOperation);
				$MenuArr["DailyLessonOperation"]["Child"]["ViewDailyLessonStatus"] = array($Lang['LessonAttendance']['DailyLessonStatus'], $StudentAttendancePath . "dailyoperation/daily_lesson_attendance/index2.php", $PageLessonAttendance_DailyOverview);
				$MenuArr["DailyLessonOperation"]["Child"]["ViewDailyLateLessonStatus"] = array($Lang['LessonAttendance']['DailyLessonLateOverview'], $StudentAttendancePath . "dailyoperation/daily_lesson_late/index.php", $PageLessonAttendance_DailyLateOverview);
				$MenuArr["DailyLessonOperation"]["Child"]["ViewDailyAbsenceLessonStatus"] = array($Lang['LessonAttendance']['DailyLessonAbsenceOverview'], $StudentAttendancePath . "dailyoperation/daily_lesson_absence/index.php", $PageLessonAttendance_DailyAbsenceOverview);
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				$MenuArr["DataImport"] = array($i_StudentAttendance_Menu_DataImport, "", $PageDataImport);
				$MenuArr["DataImport"]["Child"]["CardID"] = array($i_StudentAttendance_ImportCardID, $StudentAttendancePath."data_import/studentcard.php", $PageDataImport_CardID);
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				$MenuArr["Report"]["Child"]["ClassMonth"] = array($i_StudentAttendance_Report_ClassMonth, $StudentAttendancePath."report/class_month_full.php", $PageReport_ClassMonth);
				$MenuArr["Report"]["Child"]["ClassDaily"] = array($i_StudentAttendance_Report_ClassDaily, $StudentAttendancePath."report/class_day.php", $PageReport_ClassDaily);
			}

			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]){
				$MenuArr["Report"] = array($i_StudentAttendance_Menu_Report, "", $PageReport);
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				$MenuArr["Report"]["Child"]["ClassMonth"] = array($i_StudentAttendance_Report_ClassMonth, $StudentAttendancePath . "report/class_month_full.php", $PageReport_ClassMonth);
				$MenuArr["Report"]["Child"]["LateSkipLessonRecords"] = array($Lang['LessonAttendance']['LateSkipLessonRecords'], $StudentAttendancePath."report/late_skip_lesson_records/index.php", $PageLessonAttendance_LateSkipLessonRecords);
				$MenuArr["Report"]["Child"]["SingleStudentSummaryReport"] = array($Lang['LessonAttendance']['SingleStudentSummaryReport'], $StudentAttendancePath."report/single_student_summary_report/index.php", $PageLessonAttendance_SingleStudentSummaryReport);
				$MenuArr["Report"]["Child"]["Search"] = array($button_find, $StudentAttendancePath."report/search_report.php", $PageReport_Search);
			}

			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]){
				$MenuArr["ReportLesson"] = array($i_StudentAttendance_Menu_ReportLesson, "", $PageReportLesson);
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
				$MenuArr["ReportLesson"]["Child"]["ClassLessonDaily"] = array($Lang['LessonAttendance']['LessonClassDetailReport'], $StudentAttendancePath."report/lesson_class_detail_report.php", $PageReport_ClassDetailAttendReport);
				$MenuArr["ReportLesson"]["Child"]["LessonSummary"] = array($Lang['LessonAttendance']['LessonSummary'], $StudentAttendancePath."report/lesson_summary/index.php", $PageLessonAttendance_LessonSummary);
				$MenuArr["ReportLesson"]["Child"]["LessonSummaryReport"] = array($Lang['LessonAttendance']['LessonSummaryReport'], $StudentAttendancePath."report/lesson_summary_report.php", $PageReport_LessonAttendSummary);
				$MenuArr["ReportLesson"]["Child"]["LessonNonconfirmedReport"] = array($Lang['LessonAttendance']['LessonNonConfirmedReport'], $StudentAttendancePath."report/lesson_non_confirmed_report/index.php", $PageReport_LessonNonconfirmedReport);

			}


			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]){
				$MenuArr["SystemSetting"] = array($i_SmartCard_SystemSettings, "", ($PageSystemSetting || (is_array($CurrentPage) && $CurrentPage['StudentEntryLeaveSetting'] == 1) || (is_array($CurrentPage) && $CurrentPage['eNoticTemplateSetting'] == 1)));
			}
			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				$MenuArr["SystemSetting"]["Child"]["BasicSettings"] = array($i_general_BasicSettings, $StudentAttendancePath."settings/basic/", $PageSystemSetting_BasicSettings);
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
				$MenuArr["SystemSetting"]["Child"]["LessonSetting"] = array($Lang['LessonAttendance']['BasicSettingsLessionAttendance'], $StudentAttendancePath."settings/lesson/", $PageSystemSetting_BasicSettingsLesson);
				//$MenuArr["SystemSetting"]["Child"]["LessonReasonSetting"] = array($Lang['LessonAttendance']['LessonAttendanceReasons'], $StudentAttendancePath."settings/lesson_attendance_reasons/", $PageSystemSetting_LessonAttendanceReasons);
			}

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				$MenuArr["SystemSetting"]["Child"]["TerminalSettings"] = array($i_SmartCard_TerminalSettings, $StudentAttendancePath . "settings/terminal/", $PageSystemSetting_TerminalSettings);

				$time_table_mode = $this->retrieveTimeTableMode();
				if (($time_table_mode == 0) || ($time_table_mode == "")) {
					$MenuArr["SystemSetting"]["Child"]["TimeSlotSettings"] = array($i_StudentAttendance_TimeSlotSettings, $StudentAttendancePath . "settings/slot/school/", $PageSystemSetting_TimeSlotSettings);
				}
				if ($time_table_mode == 1) {
					$MenuArr["SystemSetting"]["Child"]["TimeSessionSettings"] = array($i_StudentAttendance_TimeSessionSettings, $StudentAttendancePath . "settings/slot_session/session_setting/", $PageSystemSetting_TimeSessionSettings);
				}

				if ($plugin['eClassApp']) {
					$MenuArr["SystemSetting"]["Child"]["ApplyLeaveSettings"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath . "settings/apply_leave/", $PageSystemSetting_ApplyLeaveAppSettings);
					//2016-02-01
					$MenuArr["SystemSetting"]["Child"]["AppTemplateSettings"] = array($Lang['eClassApp']['PushMessageTemplate'], $PATH_WRT_ROOT . "home/eClassApp/common/pushMessageTemplate/template_view.php?Module=StudentAttendance&Section=", $PageStudentAttendanceSettings_MessageTemplate);
				}

				if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
					$MenuArr["SystemSetting"]["Child"]["LeaveTypeSetting"] = array($Lang['StudentAttendance']['LeaveType'], $StudentAttendancePath . "settings/leave_type/", $PageSystemSetting_LeaveType);
				}

				/*
				if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
					$MenuArr["SystemSetting"]["Child"]["AttendanceWordTemplate"] = array($i_wordtemplates_attendance, $StudentAttendancePath . "settings/wordtemplates/index.php", $PageLessonAttendance_AttendanceWordTemplate);
				}
				*/
			}


			$MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'];

			if($sys_custom['PowerClass']){
				if(isset($_SESSION['PowerClass_PAGE'])){
					if($_SESSION['PowerClass_PAGE']=="reports"){
						if($PageReportLesson == 1) {
							$MenuArr = array(
								"ReportLesson" => $MenuArr["ReportLesson"],
							);
							$MODULE_OBJ['title'] = $ip20TopMenu['eAttendanceLesson'];
						} else {
							$MenuArr["Report"]["Child"] = array(
								"ClassMonth" => $MenuArr["Report"]["Child"]["ClassMonth"],
								"Search" => $MenuArr["Report"]["Child"]["Search"]
							);
							$MenuArr = array("Report" => $MenuArr["Report"]);
						}
					}elseif($_SESSION['PowerClass_PAGE']=="app"){
						if($PageDailyLessonOperation == 1) {
							$MenuArr = array();
							if(isset($MenuArr["DailyLessonOperation"])) {
								$MenuArr['DailyLessonOperation'] = $MenuArr["DailyLessonOperation"];
							}
							$MODULE_OBJ['title'] = $ip20TopMenu['eAttendanceLesson'];
						} else {
							$MenuArr["DailyOperation"]["Child"] = array(
								"ViewClassStatus" => $MenuArr["DailyOperation"]["Child"]["ViewClassStatus"],
								"ViewLateStatus" => $MenuArr["DailyOperation"]["Child"]["ViewLateStatus"],
								"ViewAbsenceStatus" => $MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"],
								"ViewEarlyLeaveStatus" => $MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"],
								"ApplyLeave(App)" => $MenuArr["DailyOperation"]["Child"]["ApplyLeave(App)"],
								"PresetAbsence" => $MenuArr["DailyOperation"]["Child"]["PresetAbsence"]
							);
							$MenuArr["DataImport"]["Child"] = array("ImportSmartCardID" => $MenuArr["DataImport"]["Child"]["CardID"]);
							$MenuArr = array(
								"DailyOperation" => $MenuArr["DailyOperation"],
								"DataImport" => $MenuArr["DataImport"]
							);
						}
					}
				}
			}

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass eAttendance";'."\n";
            $js.= '</script>'."\n";
            $MODULE_OBJ['title'] .= $js;

			# module information

			$MODULE_OBJ['title_css'] = "menu_opened";
			$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eAttendance.gif";
			$MODULE_OBJ['menu'] = $MenuArr;
			$MODULE_OBJ['root_path'] = $StudentAttendancePath;

			return $MODULE_OBJ;
		}

/*
    * Get MODULE_OBJ array
    */
    function GET_MODULE_OBJ_ARR(){
		if(get_client_region() == 'zh_TW') {
			return $this->GET_MODULE_OBJ_ARR_TW();
		}

	    global $PATH_WRT_ROOT, $ip20TopMenu, $CurrentPage, $LAYOUT_SKIN, $Lang, $module_version, $sys_custom;
	
	    # Menu Item Wordings
	    global $i_SmartCard_SystemSettings, $i_general_BasicSettings, $i_SmartCard_TerminalSettings, $i_StudentAttendance_TimeSessionSettings, $i_StudentAttendance_TimeSlotSettings, $i_StudentAttendance_LunchSettings;
	    global $i_StudentAttendance_Menu_ResponsibleAdmin,$i_SmartCard_Responsible_Admin_Class;
	    global $i_StudentAttendance_Menu_DailyOperation,$i_SmartCard_DailyOperation_ViewClassStatus,$i_SmartCard_DailyOperation_ViewLateStatus,$i_SmartCard_DailyOperation_ViewAbsenceStatus,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus,$i_SmartCard_DailyOperation_ViewLeftStudents, $i_SmartCard_DailyOperation_ImportOfflineRecords, $i_SmartCard_DailyOperation_ViewEntryLog, $i_SmartCard_DailyOperation_Preset_Absence;
	    global $i_StudentAttendance_Menu_DataManagement,$i_StudentAttendance_Menu_DataMgmt_DataClear,$i_StudentAttendance_Menu_DataMgmt_BadLogs;
	    global $i_StudentAttendance_Menu_OtherFeatures,$i_StudentAttendance_Menu_OtherFeatures_Outing,$i_StudentAttendance_Menu_OtherFeatures_Detention, $i_StudentAttendance_Menu_OtherFeatures_Reminder;
	    global $i_StudentAttendance_Menu_DataExport, $i_StudentAttendance_Reminder, $i_StudentAttendance_StudentInformation, $i_StudentAttendance_OR_ExportStudentInfo;
	    global $i_StudentAttendance_Menu_DataImport,$i_StudentAttendance_ImportCardID;
	    global $i_StudentAttendance_Menu_Report, $i_StudentAttendance_Report_ClassMonth, $i_StudentAttendance_Report_ClassDaily, $i_StudentAttendance_Report_ClassBadRecords, $i_StudentAttendance_Report_StudentBadRecords, $button_find;
	    global $i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption, $i_StudentAttendance_Report_NoCardTab;
	    global $i_MedicalReason_Export2,$sys_custom, $i_StudentAttendance_StaffAndStudentInformation;
	    global $i_SmartCard_DailyOperation_Check_Time, $i_SmartCard_DailyOperation_ViewGroupStatus, $plugin, $i_StudentAttendance_Menu_CustomFeatures;
	    
	    //if($module_version['StudentAttendance'] == 3.0 || strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/eAttendance/')!==false){
	    if($this->isStudentAttendance30()){	
	    	return $this->GET_MODULE_OBJ_ARR_30();	
	    }
	    
	    # Current Page Information init
	    $PageDailyOperation = 0;
	    $PageDailyOperation_ViewClassStatus = 0;
	    $PageDailyOperation_ViewGroupStatus = 0;
	    $PageDailyOperation_ViewSubjectGroupStatus = 0;
	    $PageDailyOperation_ViewLateStatus = 0;
	    $PageDailyOperation_ViewAbsenceStatus = 0;
	    $PageDailyOperation_ViewEarlyLeaveStatus = 0;
	    $PageDailyOperation_ViewLeftStudents = 0;
	    $PageDailyOperation_ImportOfflineRecords = 0;
	    $PageDailyOperation_ViewEntryLog = 0;
	    $PageDailyOperation_PresetAbsence = 0;
	    $PageDailyOperation_CheckTime = 0;
	    $PageDailyOperation_WaiveAbsence = 0;
	
	    $PageDataManagement = 0;
	    $PageDataManagement_DataClear = 0;
	    $PageDataManagement_BadLogs = 0;
	    $PageDataManagement_RemovePastData = 0;
	
	    $PageOtherFeatures = 0;
	    $PageOtherFeatures_Outing = 0;
	    $PageOtherFeatures_Detention = 0;
	    $PageOtherFeatures_Reminder = 0;
	    $PageOtherFeatures_PresetTeacherRecords = 0;
	    
	    $PageDataExport = 0;
	    $PageDataExport_Reminder = 0;
	    $PageDataExport_Medical= 0;
	
	    $PageDataImport = 0;
	    $PageDataImport_CardID= 0;
	
	    $PageReport = 0;
	    $PageReport_HKUSPH = 0;
	    $PageReport_ClassMonth = 0;
	    $PageReport_ClassDaily = 0;
	    $PageReport_ClassBadRecords = 0;
	    $PageReport_StudentBadRecords = 0;
	    $PageReport_Search = 0;
		$PageReport_EntryLog = 0;
		
	    $PageSystemSetting = 0;
	    $PageSystemSetting_BasicSettings = 0;
	    $PageSystemSetting_TerminalSettings = 0;
	    # Either one: Slot or Session
	    $PageSystemSetting_TimeSessionSettings = 0;
	    $PageSystemSetting_TimeSlotSettings = 0;
	    $PageSystemSetting_LunchSettings = 0;
		
			# Report Settings - Added 2009/09/18
			$PageSystemSetting_ReportSettings = 0;
		
      $PageAccountResponsibleAdmin = 0;
      $PageAccountResponsibleAdmin_ClassRepresentative = 0;

		$PageCustomFeatures = 0;
		$PageCustomFeatures_HostelGroupStudentList = 0;
		$PageCustomFeatures_StaffAtSchoolList = 0;

		$PageDailyOperation_BodyTemperature = 0;
		$PageSystemSetting_BasicSettingsLesson = 0;
      switch ($CurrentPage) {
      	      case "PageCustomFeatures_HostelGroupStudentList":
					  $PageCustomFeatures = 1;
					  $PageCustomFeatures_HostelGroupStudentList = 1;
      	      	      break;
		      case "PageCustomFeatures_StaffAtSchoolList":
				      $PageCustomFeatures = 1;
				      $PageCustomFeatures_StaffAtSchoolList = 1;
				      break;
              case "PageDailyOperation_ViewClassStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewClassStatus = 1;
                      break;
              case "PageDailyOperation_ViewGroupStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewGroupStatus = 1;
                      break;
              case "PageDailyOperation_ViewSubjectGroupStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewSubjectGroupStatus = 1;
                      break;
              case "PageDailyOperation_ViewHostelGroupStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewHostelGroupStatus = 1;
                      break;
              case "PageDailyOperation_CheckTime":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_CheckTime = 1;
                      break;
              case "PageDailyOperation_ViewLateStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewLateStatus = 1;
                      break;
              case "PageDailyOperation_ViewAbsenceStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewAbsenceStatus = 1;
                      break;
              case "PageDailyOperation_ViewEarlyLeaveStatus":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewEarlyLeaveStatus = 1;
                      break;
              case "PageDailyOperation_ViewLeftStudents":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewLeftStudents = 1;
                      break;
              case "PageDailyOperation_ImportOfflineRecords":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ImportOfflineRecords = 1;
                      break;
              case "PageDailyOperation_ViewEntryLog":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_ViewEntryLog = 1;
                      break;
              case "PageDailyOperation_PresetAbsence":
                      $PageDailyOperation= 1;
                      $PageDailyOperation_PresetAbsence = 1;
                      break;
              case "PageDailyOperation_CheckTime":
                  $PageDailyOperation= 1;
                  $PageDailyOperation_CheckTime = 1;
              		break;
              case "PageDailyOperation_ApplyLeaveApp":
                  $PageDailyOperation = 1;
                  $PageDailyOperation_ApplyLeaveApp = 1;
              break;
              case "PageDailyOperation_WaiveAbsence":
              	$PageDailyOperation = 1;
              	$PageDailyOperation_WaiveAbsence = 1;
              break;
              case "PageDataManagement_DataClear":
                      $PageDataManagement = 1;
                      $PageDataManagement_DataClear = 1;
                      break;
              case "PageDataManagement_BadLogs":
                      $PageDataManagement = 1;
                      $PageDataManagement_BadLogs = 1;
                      break;
              case "PageDataManagement_RemovePastData":
              		  $PageDataManagement = 1;
              		  $PageDataManagement_RemovePastData = 1;
              		  break;
              case "PageOtherFeatures_Outing":
                      $PageOtherFeatures = 1;
                      $PageOtherFeatures_Outing = 1;
                      break;
              case "PageOtherFeatures_Detention":
                      $PageOtherFeatures = 1;
                      $PageOtherFeatures_Detention = 1;
                      break;
              case "PageOtherFeatures_Reminder":
                      $PageOtherFeatures = 1;
                      $PageOtherFeatures_Reminder = 1;
                      break;
              case "PageOtherFeatures_LeaveSchoolOption":
                      $PageOtherFeatures = 1;
                      $PageOtherFeatures_LeaveSchoolOption = 1;
                      break;
              case "PageOtherFeatures_ViewWebSAMSReasonCode":
                      $PageOtherFeatures = 1;
                      $PageOtherFeatures_ViewWebSAMSReasonCode = 1;
                      break;
              case "PageOtherFeatures_BodyTemperatureRecords":
              		$PageOtherFeatures = 1;
                    $PageOtherFeatures_BodyTemperatureRecords = 1;
                    break;
              case "PageOtherFeatures_HKUSPHParentConsentRecords":
              		$PageOtherFeatures = 1;
                    $PageOtherFeatures_HKUSPHParentConsentRecords = 1;
                    break;
              case "PageOtherFeatures_LateAbsentRecords":
              		$PageOtherFeatures = 1;
              		$PageOtherFeatures_LateAbsentRecords = 1;
              		break;
              case "PageOtherFeatures_PresetTeacherRecords":
                    $PageOtherFeatures = 1;
                    $PageOtherFeatures_PresetTeacherRecords = 1;
                    break;
              case "PageDataExport_Reminder":
                      $PageDataExport = 1;
                      $PageDataExport_Reminder = 1;
                      break;
              case "PageDataExport_Medical":
                      $PageDataExport = 1;
                      $PageDataExport_Medical = 1;
                      break;
              case "PageDataImport_CardID":
                      $PageDataImport = 1;
                      $PageDataImport_CardID = 1;
                      break;
              case "PageReport_HKUSPH":
                      $PageReport = 1;
                      $PageReport_HKUSPH = 1;
                      break;
              case "PageReport_ClassMonth":
                      $PageReport = 1;
                      $PageReport_ClassMonth = 1;
                      break;
              case "PageReport_ClassDaily":
                      $PageReport = 1;
                      $PageReport_ClassDaily = 1;
                      break;
              case "PageReport_ClassBadRecords":
                      $PageReport = 1;
                      $PageReport_ClassBadRecords = 1;
                      break;
              case "PageReport_StudentBadRecords":
                      $PageReport = 1;
                      $PageReport_StudentBadRecords = 1;
                      break;
              case "PageReport_Search":
                      $PageReport = 1;
                      $PageReport_Search = 1;
                      break;
              case "PageReport_SearchAbsentSession":
              		$PageReport = 1;
              		$PageReport_SearchAbsentSession = 1;
              		break;
			case "PageReport_NoTappingCard":
					$PageReport = 1;
					$PageReport_NoTappingCard = 1;
					break;
			case "PageReport_DailyAbsentAnalysis":
					$PageReport = 1;
					$PageReport_DailyAbsentAnalysis = 1;
					break;
			case "PageReport_EntryLog":
					$PageReport = 1;
					$PageReport_EntryLog = 1;
			break;
			case "PageReport_ProveDocReport":
					$PageReport = 1;
					$PageReport_ProveDocReport = 1;
					break;
			case "PageReport_StudentAttendanceInfo":
					$PageReport = 1;
					$PageReport_StudentAttendanceInfo = 1;
					break;
			case "PageReport_ContinuousAbsenceReport":
					$PageReport = 1;
					$PageReport_ContinuousAbsenceReport = 1;
					break;
			  case "PageReport_HostelAttendanceStudentList":
					$PageReport = 1;
					$PageReport_HostelAttendanceStudentList = 1;
					break;
			  case "PageReport_HostelAttendanceStatistic":
					$PageReport = 1;
					$PageReport_HostelAttendanceStatistic = 1;
					break;
			  case "PageReport_HostelAttendanceStudentReport":
					$PageReport = 1;
					$PageReport_HostelAttendanceStudentReport = 1;
					break;
              case "PageSystemSetting_BasicSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_BasicSettings = 1;
                      break;
              case "PageSystemSetting_TerminalSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_TerminalSettings = 1;
                      break;
              case "PageSystemSetting_TimeSessionSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_TimeSessionSettings = 1;
                      break;
              case "PageSystemSetting_TimeSlotSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_TimeSlotSettings = 1;
                      break;
              case "PageSystemSetting_CustomizedTimeSlotSettings":
              		  $PageSystemSetting = 1;
                      $PageSystemSetting_CustomizedTimeSlotSettings = 1;
              		break;
              case "PageSystemSetting_LunchSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_LunchSettings = 1;
                      break;
              case "PageSystemSetting_ReportSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_ReportSettings = 1;
                      break;
              case "PageSystemSetting_HKUSPHDataSubmissionSettings":
              		$PageSystemSetting = 1;
                    $PageSystemSetting_HKUSPHDataSubmissionSettings = 1;
              		break;
              case "PageSystemSetting_ContinuousAbsenceAlert":
              		$PageSystemSetting = 1;
              		$PageSystemSetting_ContinuousAbsenceAlert = 1;
              		break;
              case "PageSystemSetting_HostelAttendanceSettings":
              		$PageSystemSetting = 1;
                    $PageSystemSetting_HostelAttendanceSettings = 1;
              		break;
              case "PageSystemSetting_HKUGACCustomSettings":
              		$PageSystemSetting = 1;
                    $PageSystemSetting_HKUGACCustomSettings = 1;
              		break;
              case "PageAccountResponsibleAdmin_ClassRepresentative":
                      $PageAccountResponsibleAdmin = 1;
                      $PageAccountResponsibleAdmin_ClassRepresentative = 1;
                      break;
              case "PageAccountResponsibleAdmin_GroupResponsibleUsers":
              		  $PageAccountResponsibleAdmin = 1;
                      $PageAccountResponsibleAdmin_GroupResponsibleUsers = 1;
              		break;
              // start of lesson attendance menu
              case "PageLessonAttendance_DailyOverview":
                  $PageDailyOperation= 1;
                  $PageLessonAttendance_DailyOverview = 1;
              		break;
              case "PageLessonAttendance_LateSkipLessonRecords":
             		$PageReport = 1;
             		$PageLessonAttendance_LateSkipLessonRecords = 1;
             	break;
              case "PageLessonAttendance_LessonSummary":
              		$PageReport = 1;
              		$PageLessonAttendance_LessonSummary = 1;
              break;
              case "PageLessonAttendance_SingleStudentSummaryReport":
              		$PageReport = 1;
              		$PageLessonAttendance_SingleStudentSummaryReport = 1;
              break;
              case "PageLessonAttendance_HalfDayAbsentSummaryReport":
              		$PageReport = 1;
              		$PageLessonAttendance_HalfDayAbsentSummaryReport = 1;
              break;
              case "PageLessonAttendance_StudentDailyRecords":
              		$PageReport = 1;
              		$PageLessonAttendance_StudentDailyRecords = 1;
              break;
              case "PageLessonAttendance_ClassDetailAttendReport":
                  $PageReport= 1;
                  $PageReport_ClassDetailAttendReport = 1;
              		break;
              case "PageLessonAttendance_LessonAttendSummary":
                  $PageReport= 1;
                  $PageReport_LessonAttendSummary = 1;
              		break;
              case "PageSystemSetting_LessonSettings":
                  $PageSystemSetting = 1;
                  $PageSystemSetting_LessonSettings = 1;
				  $PageSystemSetting_BasicSettingsLesson = 1;
                  break;
              case "PageSystemSetting_LessonAttendanceReasons":
              	  $PageSystemSetting = 1;
                  $PageSystemSetting_LessonAttendanceReasons = 1;
				  $PageSystemSetting_BasicSettingsLesson = 1;
                  break;
              case "PageSystemSetting_LeaveType":
				  $PageSystemSetting = 1;
              	  $PageSystemSetting_LeaveType = 1;
              	  break;
              case "PageSystemSetting_SessionSettings":
                  $PageSystemSetting = 1;
                  $PageSystemSetting_SessionSettings = 1;
                  break;
              case "PageStudentAttendanceSettings_MessageTemplate":
                  $PageSystemSetting = 1;
                  $PageStudentAttendanceSettings_MessageTemplate = 1;
                  break; 	
              case "PageDailyOperation_MissParentLetter":
                  $PageDailyOperation = 1;
                  $PageDailyOperation_MissParentLetter = 1;
                  break;
              case "PageLessonAttendance_LessonNonconfirmedReport":
              	$PageReport = 1;
              	$PageReport_LessonNonconfirmedReport = 1;
              	break;
			  case "PageStudentAttendance_StudentAttendanceRateReport":
				  $PageReport = 1;
				  $PageReport_StudentAttendanceRateReport = 1;
				  break;
              case "PageLessonAttendance_ImportLessonAttendanceRecords":
              	$PageDailyOperation = 1;
              	$PageLessonAttendance_ImportLessonAttendanceRecords = 1;
              	 break;
              case "PageSystemSetting_ApplyLeaveAppSettings":
                  $PageSystemSetting = 1;
                  $PageSystemSetting_ApplyLeaveAppSettings = 1;
              break;
              case "PageDailyOperation_LessonAttendanceLeaveRecords":
              	  $PageDailyOperation = 1;
              	  $PageLessonAttendance_LessonAttendanceLeaveRecords = 1;
              break;
              case "PageDailyOperation_ReprintCard":
              $PageDailyOperation = 1;
              $PageDailyOperation_ReprintCard = 1;
              break;
		  case "PageDailyOperation_BodyTemperature":
		  	$PageDailyOperation = 1;
		  	$PageDailyOperation_BodyTemperature = 1;
		  	break;
              // end of lesson attendance menu
      }

      # Menu information
      $StudentAttendancePath = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/";
      
//    $MenuArr["DailyOperation"] = array($i_StudentAttendance_Menu_DailyOperation, "", $PageDailyOperation || ($CurrentPage['PrintNotice'] + $CurrentPage['CumulativeProfileNotice']) > 0);
	  $MenuArr["DailyOperation"] = array($i_StudentAttendance_Menu_DailyOperation, "", ($PageDailyOperation || (is_array($CurrentPage) && $CurrentPage['CumulativeProfileNotice'] == 1)|| (is_array($CurrentPage) && $CurrentPage['PrintNotice'] == 1)) );
      
     	if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
        $MenuArr["DailyOperation"]["Child"]["ViewClassStatus"] = array($i_SmartCard_DailyOperation_ViewClassStatus, $StudentAttendancePath."dailyoperation/class/class_status.php", $PageDailyOperation_ViewClassStatus);
        $MenuArr["DailyOperation"]["Child"]["ViewGroupStatus"] = array($i_SmartCard_DailyOperation_ViewGroupStatus,$StudentAttendancePath."dailyoperation/group/group_status.php", $PageDailyOperation_ViewGroupStatus);
		if ($_SESSION["platform"]!='KIS')
			$MenuArr["DailyOperation"]["Child"]["ViewSubjectGroupStatus"] = array($Lang['StudentAttendance']['ViewIndividualSubjectGroupList'],$StudentAttendancePath."dailyoperation/subject_group/subject_group_status.php", $PageDailyOperation_ViewSubjectGroupStatus);
      	}
      
      if($sys_custom['StudentAttendance']['HostelAttendance']){
      	$MenuArr["DailyOperation"]["Child"]["ViewHostelGroupStatus"] = array($Lang['StudentAttendance']['ViewHostelGroupStatus'],$StudentAttendancePath."dailyoperation/hostel/", $PageDailyOperation_ViewHostelGroupStatus);
      }
      
      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
      	$MenuArr["DailyOperation"]["Child"]["ViewDailyLessonStatus"] = array($Lang['LessonAttendance']['DailyLessonStatus'], $StudentAttendancePath."dailyoperation/daily_lesson_attendance/index2.php", $PageLessonAttendance_DailyOverview || $PageLessonAttendance_ImportLessonAttendanceRecords);
      	//$MenuArr["DailyOperation"]["Child"]["ImportLessonAttendanceRecords"] = array($Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Title'], $StudentAttendancePath."dailyoperation/import_lesson_attendance/", $PageLessonAttendance_ImportLessonAttendanceRecords);
      }
      
      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
        $MenuArr["DailyOperation"]["Child"]["CheckTime"] = array($i_SmartCard_DailyOperation_Check_Time,$StudentAttendancePath."dailyoperation/checktime/browse_by_student.php", $PageDailyOperation_CheckTime);
        if($sys_custom['StudentAttendance']['RecordBodyTemperature']) {
			$MenuArr["DailyOperation"]["Child"]["ViewBodyTemperatureStatus"] = array($Lang['StudentAttendance']['ViewBodyTemperatureStatus'],$StudentAttendancePath."dailyoperation/body_temperature/index.php", $PageDailyOperation_BodyTemperature);
		}
        $MenuArr["DailyOperation"]["Child"]["ViewLateStatus"] = array($i_SmartCard_DailyOperation_ViewLateStatus, $StudentAttendancePath."dailyoperation/late/showlate.php", $PageDailyOperation_ViewLateStatus);
        $MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, $StudentAttendancePath."dailyoperation/absence/show.php", $PageDailyOperation_ViewAbsenceStatus);
        $MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"] = array($i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, $StudentAttendancePath."dailyoperation/early/show.php", $PageDailyOperation_ViewEarlyLeaveStatus);
        $MenuArr["DailyOperation"]["Child"]["ViewLeftStudents"] = array($i_SmartCard_DailyOperation_ViewLeftStudents, $StudentAttendancePath."dailyoperation/left/summary.php", $PageDailyOperation_ViewLeftStudents);
        $MenuArr["DailyOperation"]["Child"]["ImportOfflineRecords"] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, $StudentAttendancePath."dailyoperation/import/import.php", $PageDailyOperation_ImportOfflineRecords);
        $MenuArr["DailyOperation"]["Child"]["ViewEntryLog"] = array($i_SmartCard_DailyOperation_ViewEntryLog, $StudentAttendancePath."dailyoperation/entry_log/", $PageDailyOperation_ViewEntryLog);
        $MenuArr["DailyOperation"]["Child"]["PresetAbsence"] = array($i_SmartCard_DailyOperation_Preset_Absence, $StudentAttendancePath."dailyoperation/preset_absence/new.php", $PageDailyOperation_PresetAbsence);
        if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
	        $MenuArr["DailyOperation"]["Child"]["MissParentLetter"] = array($Lang['StudentAttendance']['DailyOperation_MissParentLetter'], $StudentAttendancePath."dailyoperation/absent_without_letter/", $PageDailyOperation_MissParentLetter);
	    if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['notice']&&$_SESSION["platform"]!='KIS') {
        	$MenuArr["DailyOperation"]["Child"]["CumulativeProfileNotice"] = array($Lang['StudentAttendance']['CumulativeProfileNotice'], $StudentAttendancePath."dailyoperation/cumulative_profile_notice/",(is_array($CurrentPage) && $CurrentPage['CumulativeProfileNotice'] == 1));
        	$MenuArr["DailyOperation"]["Child"]["PrintNotice"] = array($Lang['StudentAttendance']['eNotice']['PrintNotice'], $StudentAttendancePath."dailyoperation/print_notice/",(is_array($CurrentPage) && $CurrentPage['PrintNotice'] == 1));
        }
        if ($plugin['eClassApp']) {
        	$MenuArr["DailyOperation"]["Child"]["ApplyLeave(App)"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath."dailyoperation/apply_leave/",$PageDailyOperation_ApplyLeaveApp);
            if($sys_custom['eClassApp']['enableReprintCard']){
            	$MenuArr["DailyOperation"]["Child"]["ReprintCard(App)"] = array($Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)'], $StudentAttendancePath."dailyoperation/reprint_card/",$PageDailyOperation_ReprintCard);
            }
        }
        if($sys_custom['StudentAttendance_WaiveAbsent']){
        	$MenuArr["DailyOperation"]["Child"]["WaiveAbsence"] = array($Lang['StudentAttendance']['WaiveAbsence'], $StudentAttendancePath."dailyoperation/waive_absence/index.php", $PageDailyOperation_WaiveAbsence);
        }


        $MenuArr["DataManagement"] = array($i_StudentAttendance_Menu_DataManagement, "", $PageDataManagement);
        $MenuArr["DataManagement"]["Child"]["DataClear"] = array($i_StudentAttendance_Menu_DataMgmt_DataClear, $StudentAttendancePath."datamgmt/dataclear/", $PageDataManagement_DataClear);
		if ($_SESSION["platform"]!='KIS')
			$MenuArr["DataManagement"]["Child"]["BadLogs"] = array($i_StudentAttendance_Menu_DataMgmt_BadLogs, $StudentAttendancePath."datamgmt/badlogs/browse.php?type=1", $PageDataManagement_BadLogs);
		//if($sys_custom['StudentAttendance']['RemovePastData']){
		$MenuArr["DataManagement"]["Child"]["RemovePastData"] = array($Lang['StudentAttendance']['RemovePastData'], $StudentAttendancePath."datamgmt/remove_past_data/index.php", $PageDataManagement_RemovePastData);
		//}
		
        $MenuArr["OtherFeatures"] = array($i_StudentAttendance_Menu_OtherFeatures, "", $PageOtherFeatures);
        $MenuArr["OtherFeatures"]["Child"]["Outing"] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, $StudentAttendancePath."other_features/outing/", $PageOtherFeatures_Outing);
        if ($_SESSION["platform"]!='KIS')
			$MenuArr["OtherFeatures"]["Child"]["Detention"] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, $StudentAttendancePath."other_features/detention/", $PageOtherFeatures_Detention);
        $MenuArr["OtherFeatures"]["Child"]["Reminder"] = array($i_StudentAttendance_Menu_OtherFeatures_Reminder, $StudentAttendancePath."other_features/reminder/", $PageOtherFeatures_Reminder);
        if ($sys_custom['SmartCardStudentAttend_LeaveSchoolOption']) 
        	$MenuArr["OtherFeatures"]["Child"]["LeaveOption"] = array($i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption, $StudentAttendancePath."other_features/leave_option/edit.php", $PageOtherFeatures_LeaveSchoolOption);
        $MenuArr["OtherFeatures"]["Child"]["ViewWebSAMSReasonCode"] = array($Lang['StudentAttendance']['ReasonManagement'], $StudentAttendancePath."other_features/reason_code_view/", $PageOtherFeatures_ViewWebSAMSReasonCode);
		//if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
		//	$MenuArr["OtherFeatures"]["Child"]["BodyTemperatureRecords"] = array($Lang['StudentAttendance']['BodyTemperatureRecords'], $StudentAttendancePath."other_features/body_temperature/", $PageOtherFeatures_BodyTemperatureRecords);
		//}
		if($sys_custom['eClassApp']['HKUSPH']){
			$MenuArr["OtherFeatures"]["Child"]["HKUSPHParentConsentRecords"] = array($Lang['StudentAttendance']['HKUSPHParentConsentRecords'], $StudentAttendancePath."other_features/parent_consent/index.php?clearCoo=1", $PageOtherFeatures_HKUSPHParentConsentRecords);
		}
		if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']){
			$MenuArr["OtherFeatures"]["Child"]["HKUGACLateAbsentRecords"] = array($Lang['StudentAttendance']['LateAbsentRecords'], $StudentAttendancePath."other_features/late_absent_records/", $PageOtherFeatures_LateAbsentRecords);
		}
		
		$MenuArr["OtherFeatures"]["Child"]["PresetTeacherRecords"] = array($Lang['StudentAttendance']['PresetTeacherRecords'], $StudentAttendancePath."other_features/preset_teacher_records/", $PageOtherFeatures_PresetTeacherRecords);
		
        $MenuArr["DataExport"] = array($i_StudentAttendance_Menu_DataExport, "", $PageDataExport);
				if(substr($module_version['StaffAttendance'],0,1) >= 2)
					$MenuArr["DataExport"]["Child"]["StudentInformation"] = array($Lang['StudentAttendance']['ExportForOfflineReaderUse'], $StudentAttendancePath."data_export/staff_and_student.php", $PageDataExport_StudentInformation);
				else
					$MenuArr["DataExport"]["Child"]["StudentInformation"] = array($i_StudentAttendance_StudentInformation, $StudentAttendancePath."data_export/student.php", $PageDataExport_StudentInformation);
        $MenuArr["DataExport"]["Child"]["Reminder"] = array($i_StudentAttendance_Reminder, $StudentAttendancePath."data_export/reminder.php", $PageDataExport_Reminder);
       //$MenuArr["DataExport"]["Child"]["ExportStudent"] = array($Lang['StudentAttendance']['ExportForOfflineReaderUse_950e'], $StudentAttendancePath."data_export/studentInfo_offline_reader_export_950e.php", $PageDataExport_ExportStudent);
        if($sys_custom['hku_medical_research'])
        	$MenuArr["DataExport"]["Child"]["Medical"] = array($i_MedicalReason_Export2, $StudentAttendancePath."data_export/medical.php", $PageDataExport_Medical);

        $MenuArr["DataImport"] = array($i_StudentAttendance_Menu_DataImport, "", $PageDataImport);
        $MenuArr["DataImport"]["Child"]["CardID"] = array($i_StudentAttendance_ImportCardID, $StudentAttendancePath."data_import/studentcard.php", $PageDataImport_CardID);
      }
      
      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] && $_SESSION["platform"]!='KIS') {
         	$MenuArr["DailyOperation"]["Child"]["LessonAttendanceLeaveRecords"] = array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'], $StudentAttendancePath."dailyoperation/lesson_leave_records/index.php", $PageLessonAttendance_LessonAttendanceLeaveRecords);
      }
      if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]){
      	$MenuArr["Report"] = array($i_StudentAttendance_Menu_Report, "", ($PageReport || ((is_array($CurrentPage) && $CurrentPage['NoMissConduct'] == 1) || (is_array($CurrentPage) && $CurrentPage['ClassSexRatioReport'] == 1))));
      }
      if($sys_custom['eClassApp']['HKUSPH'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
      		$MenuArr["Report"]["Child"]["HKUSPH"] = array($Lang['HKUSPH']['HKUSPH Report'],$StudentAttendancePath."report/hkusph/index.php",$PageReport_HKUSPH);
      }
      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
        $MenuArr["Report"]["Child"]["ClassMonth"] = array($i_StudentAttendance_Report_ClassMonth, $StudentAttendancePath."report/class_month_full.php", $PageReport_ClassMonth);
        $MenuArr["Report"]["Child"]["ClassDaily"] = array($i_StudentAttendance_Report_ClassDaily, $StudentAttendancePath."report/class_day.php", $PageReport_ClassDaily);
      }
      
      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
      	$MenuArr["Report"]["Child"]["ClassLessonDaily"] = array($Lang['LessonAttendance']['LessonClassDetailReport'], $StudentAttendancePath."report/lesson_class_detail_report.php", $PageReport_ClassDetailAttendReport);
      	$MenuArr["Report"]["Child"]["LateSkipLessonRecords"] = array($Lang['LessonAttendance']['LateSkipLessonRecords'], $StudentAttendancePath."report/late_skip_lesson_records/index.php", $PageLessonAttendance_LateSkipLessonRecords);
      	$MenuArr["Report"]["Child"]["LessonSummary"] = array($Lang['LessonAttendance']['LessonSummary'], $StudentAttendancePath."report/lesson_summary/index.php", $PageLessonAttendance_LessonSummary);
      	$MenuArr["Report"]["Child"]["SingleStudentSummaryReport"] = array($Lang['LessonAttendance']['SingleStudentSummaryReport'], $StudentAttendancePath."report/single_student_summary_report/index.php", $PageLessonAttendance_SingleStudentSummaryReport);
      	if($sys_custom['LessonAttendance_LaSalleCollege']){
      		$MenuArr["Report"]["Child"]["HalfDayAbsentSummaryReport"] = array($Lang['LessonAttendance']['HalfDayAbsentSummaryReport'], $StudentAttendancePath."report/half_day_absent_summary/index.php", $PageLessonAttendance_HalfDayAbsentSummaryReport);
      		$MenuArr["Report"]["Child"]["StudentDailyRecords"] = array($Lang['LessonAttendance']['StudentDailyRecords'], $StudentAttendancePath."report/student_daily_records/index.php", $PageLessonAttendance_StudentDailyRecords);
      	}
      	$MenuArr["Report"]["Child"]["LessonSummaryReport"] = array($Lang['LessonAttendance']['LessonSummaryReport'], $StudentAttendancePath."report/lesson_summary_report.php", $PageReport_LessonAttendSummary);
      	$MenuArr["Report"]["Child"]["LessonNonconfirmedReport"] = array($Lang['LessonAttendance']['LessonNonConfirmedReport'], $StudentAttendancePath."report/lesson_non_confirmed_report/index.php", $PageReport_LessonNonconfirmedReport);
      }
      
      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        if ($_SESSION["platform"]!='KIS'){
				$MenuArr["Report"]["Child"]["ClassBadRecords"] = array($i_StudentAttendance_Report_ClassBadRecords, $StudentAttendancePath."report/class_bad.php", $PageReport_ClassBadRecords);
				$MenuArr["Report"]["Child"]["StudentBadRecords"] = array($i_StudentAttendance_Report_StudentBadRecords, $StudentAttendancePath."report/top_bad.php", $PageReport_StudentBadRecords);
	        }
			$MenuArr["Report"]["Child"]["Search"] = array($button_find, $StudentAttendancePath."report/search_report.php", $PageReport_Search);
	        
	        if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) 
	        	$MenuArr["Report"]["Child"]["SearchAbsentSession"] = array($button_find." (".$Lang['StudentAttendance']['AbsentSessions'].")", $StudentAttendancePath."report/search_absent_session.php", $PageReport_SearchAbsentSession);
	        $MenuArr["Report"]["Child"]["NoTappingCard"] = array($Lang['StudentAttendance']['NoCardTappingReport'], $StudentAttendancePath."report/nocard.php", $PageReport_NoTappingCard);
	        $this->retrieveSettings();
	        if($this->attendance_mode==2 || $this->attendance_mode==3)//whole day mode
	        {
	        	$MenuArr["Report"]["Child"]["DailyAbsentAnalysis"] = array($Lang['StudentAttendance']['DailyAbsentAnalysisReport'], $StudentAttendancePath."report/daily_absent_analysis.php", $PageReport_DailyAbsentAnalysis);
	        }
	        
	        $MenuArr["Report"]["Child"]["NoMissConduct"] = array($Lang['StudentAttendance']['NoMissConductReport'],$StudentAttendancePath."report/no_miss_conduct/index.php",(is_array($CurrentPage) && $CurrentPage['NoMissConduct'] == 1));
	        $MenuArr["Report"]["Child"]["ClassSexRatioReport"] = array($Lang['StudentAttendance']['ClassSexRatioReport'],$StudentAttendancePath."report/class_sex_ratio_report/index.php",(is_array($CurrentPage) && $CurrentPage['ClassSexRatioReport'] == 1));
			$MenuArr["Report"]["Child"]["EntryLog"] = array($Lang['StudentAttendance']['EntryLog'],$StudentAttendancePath."report/entry_log/index.php",$PageReport_EntryLog);
			$MenuArr["Report"]["Child"]["NotSubmitDocumentReport"] = array($Lang['StudentAttendance']['AbsentProveReport']['AbsentProveReport'],$StudentAttendancePath."report/prove_document_report/index.php",$PageReport_ProveDocReport);
			
			// [2016-0704-1456-16054]
			if ($sys_custom['StudentAttendance']['ReportStudentAttendanceInfo']){ 
				$MenuArr["Report"]["Child"]["StudentAttendanceInfo"] = array($Lang['StudentAttendance']['StudentAttendanceInfo']['Title'], $StudentAttendancePath."report/student_attendance_info/index.php", $PageReport_StudentAttendanceInfo);
			}
			
			$MenuArr["Report"]["Child"]["ContinuousAbsenceReport"] = array($Lang['StudentAttendance']['ContinuousAbsenceReport'], $StudentAttendancePath."report/continuous_absence/index.php", $PageReport_ContinuousAbsenceReport);
			
			if($sys_custom['StudentAttendance']['HostelAttendance']){
				$MenuArr["Report"]["Child"]["HostelAttendanceStudentList"] = array($Lang['StudentAttendance']['HostelStudentList'], $StudentAttendancePath."report/hostel/student_list/index.php", $PageReport_HostelAttendanceStudentList);
				$MenuArr["Report"]["Child"]["HostelAttendanceStatistic"] = array($Lang['StudentAttendance']['HostelStatistic'], $StudentAttendancePath."report/hostel/statistic/index.php", $PageReport_HostelAttendanceStatistic);
				$MenuArr["Report"]["Child"]["HostelAttendanceStudentReport"] = array($Lang['StudentAttendance']['HostelStudentReport'], $StudentAttendancePath."report/hostel/student_report/index.php", $PageReport_HostelAttendanceStudentReport);
			}

		  $MenuArr["Report"]["Child"]["StudentAttendanceRate"] = array($Lang['StudentAttendance']['StudentAttendanceRate'], $StudentAttendancePath."report/student_attendance_rate/index.php", $PageReport_StudentAttendanceRateReport);
		}

		if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {

			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
				$MenuArr["CustomReport"] = array($i_StudentAttendance_Menu_CustomFeatures, "", ($PageCustomFeatures == 1));

				$MenuArr["CustomReport"]["Child"]["HostelGroupStudentList"] =  array($Lang['StudentAttendance']['HostelGroupStudentList'], $StudentAttendancePath."custom_features/student_hostel_list.php", $PageCustomFeatures_HostelGroupStudentList, 'target'=>'_blank');
				$MenuArr["CustomReport"]["Child"]["StaffAtSchoolList"] =  array($Lang['StudentAttendance']['StaffAtSchoolList'], $StudentAttendancePath."custom_features/staff_at_school_list.php", $PageCustomFeatures_StaffAtSchoolList, 'target'=>'_blank');

			}
		}

		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]){				
			$MenuArr["SystemSetting"] = array($i_SmartCard_SystemSettings, "", ($PageSystemSetting || (is_array($CurrentPage) && $CurrentPage['StudentEntryLeaveSetting'] == 1) || (is_array($CurrentPage) && $CurrentPage['eNoticTemplateSetting'] == 1)));
		}
		if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
        	$MenuArr["SystemSetting"]["Child"]["BasicSettings"] = array($i_general_BasicSettings, $StudentAttendancePath."settings/basic/", $PageSystemSetting_BasicSettings);

			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
				$MenuArr["SystemSetting"]["Child"]["LessonSetting"] = array($Lang['LessonAttendance']['BasicSettingsLessionAttendance'], $StudentAttendancePath . "settings/lesson/", $PageSystemSetting_BasicSettingsLesson);
			}

        if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) //Henry Added 20131105
        	$MenuArr["SystemSetting"]["Child"]["SessionSettings"] = array($Lang['StudentAttendance']['SessionSettings'], $StudentAttendancePath."settings/session/", $PageSystemSetting_SessionSettings);	
        # Report Settings - Added 2009/09/18
				$MenuArr["SystemSetting"]["Child"]["ReportSettings"] = array($Lang['SmartCard']['StudentAttendence']['ReportSettings'], $StudentAttendancePath."settings/report_setting/", $PageSystemSetting_ReportSettings);
      }
            
     /* if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
      	$MenuArr["SystemSetting"]["Child"]["LessonSetting"] = array($Lang['LessonAttendance']['LessionAttendanceSetting'], $StudentAttendancePath."settings/lesson/", $PageSystemSetting_LessonSettings);
      	$MenuArr["SystemSetting"]["Child"]["LessonReasonSetting"] = array($Lang['LessonAttendance']['LessonAttendanceReasons'], $StudentAttendancePath."settings/lesson_attendance_reasons/", $PageSystemSetting_LessonAttendanceReasons);
      }*/

      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
        $MenuArr["SystemSetting"]["Child"]["TerminalSettings"] = array($i_SmartCard_TerminalSettings, $StudentAttendancePath."settings/terminal/", $PageSystemSetting_TerminalSettings);

        $time_table_mode = $this->retrieveTimeTableMode();
        if (($time_table_mode == 0)||($time_table_mode == "")) {
                $MenuArr["SystemSetting"]["Child"]["TimeSlotSettings"] = array($i_StudentAttendance_TimeSlotSettings, $StudentAttendancePath."settings/slot/school/", $PageSystemSetting_TimeSlotSettings);
        }
        if ($time_table_mode == 1) {
                $MenuArr["SystemSetting"]["Child"]["TimeSessionSettings"] = array($i_StudentAttendance_TimeSessionSettings, $StudentAttendancePath."settings/slot_session/session_setting/", $PageSystemSetting_TimeSessionSettings);
        }
        if($sys_custom['StudentAttendance']['CustomizedTimeSlotSettings']){
        	$MenuArr["SystemSetting"]["Child"]["CustomizedTimeSlotSettings"] = array($Lang['StudentAttendance']['CustomizedTimeSlotSettings'], $StudentAttendancePath."settings/customized_time_slot/", $PageSystemSetting_CustomizedTimeSlotSettings);
        }
        if ($this->attendance_mode == "2") 
        	$MenuArr["SystemSetting"]["Child"]["LunchSettings"] = array($i_StudentAttendance_LunchSettings, $StudentAttendancePath."settings/lunch/list/", $PageSystemSetting_LunchSettings);
        if ($this->EnableEntryLeavePeriod)
        	$MenuArr["SystemSetting"]["Child"]["EntryLeaveSetting"] = array($Lang['StudentAttendance']['EntryLeaveDate'], $StudentAttendancePath."settings/entry_leave_date/",(is_array($CurrentPage) && $CurrentPage['StudentEntryLeaveSetting'] == 1));
        if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['notice']&&$_SESSION["platform"]!='KIS') 
        	$MenuArr["SystemSetting"]["Child"]["eNoticeTemplateSettings"] = array($Lang['StudentAttendance']['eNoticeTemplate'], $StudentAttendancePath."settings/enotice_template/",(is_array($CurrentPage) && $CurrentPage['eNoticTemplateSetting'] == 1));
        	
        if ($plugin['eClassApp']) {
        	$MenuArr["SystemSetting"]["Child"]["ApplyLeaveSettings"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath."settings/apply_leave/",$PageSystemSetting_ApplyLeaveAppSettings);
        	//2016-02-01
        	$MenuArr["SystemSetting"]["Child"]["AppTemplateSettings"] = array($Lang['eClassApp']['PushMessageTemplate'], $PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/template_view.php?Module=StudentAttendance&Section=",$PageStudentAttendanceSettings_MessageTemplate);
        }

		if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
		  if ($sys_custom['StudentAttendance']['LeaveType']) {
			  $MenuArr["SystemSetting"]["Child"]["LeaveTypeSetting"] = array($Lang['StudentAttendance']['LeaveType'], $StudentAttendancePath . "settings/leave_type/", $PageSystemSetting_LeaveType);
		  }
		}

        if($sys_custom['eClassApp']['HKUSPH']){
        	$MenuArr["SystemSetting"]["Child"]["HKUSPHDataSubmissionSettings"] = array($Lang['StudentAttendance']['HKUSPHDataSubmissionSettings'], $StudentAttendancePath."settings/hkusph/", $PageSystemSetting_HKUSPHDataSubmissionSettings);
        }
        
        $MenuArr["SystemSetting"]["Child"]["ContinuousAbsenceAlert"] = array($Lang['StudentAttendance']['ContinuousAbsenceAlert'], $StudentAttendancePath."settings/continuous_absence_alert/", $PageSystemSetting_ContinuousAbsenceAlert);
        
        if($sys_custom['StudentAttendance']['HostelAttendance']){
        	$MenuArr["SystemSetting"]["Child"]["HostelAttendanceSettings"] = array($Lang['StudentAttendance']['HostelAttendanceSettings'], $StudentAttendancePath."settings/hostel/", $PageSystemSetting_HostelAttendanceSettings);
        }
        
        if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']){
        	$MenuArr["SystemSetting"]["Child"]["HKUGACCustomSettings"] = array($Lang['StudentAttendance']['CustomSettings'], $StudentAttendancePath."settings/hkugac_custom_settings/", $PageSystemSetting_HKUGACCustomSettings);
        }
        	
		if ($_SESSION["platform"]!='KIS'){
			$MenuArr["AccountResponsibleAdmin"] = array($i_StudentAttendance_Menu_ResponsibleAdmin, "", $PageAccountResponsibleAdmin);
			$MenuArr["AccountResponsibleAdmin"]["Child"]["ClassRepresentative"] = array($i_SmartCard_Responsible_Admin_Class, $StudentAttendancePath."admin/class/list/", $PageAccountResponsibleAdmin_ClassRepresentative);
			$MenuArr["AccountResponsibleAdmin"]["Child"]["GroupResponsibleUsers"] = array($Lang['StudentAttendance']['GroupResponsibleUsers'], $StudentAttendancePath."admin/group/", $PageAccountResponsibleAdmin_GroupResponsibleUsers);
		}
	}
		
	if($sys_custom['HanAcademy'] || ($sys_custom['PowerClass'] && $plugin['attendancelesson'])){
	  	if(isset($_SESSION['PowerClass_PAGE'])){
	  		if($_SESSION['PowerClass_PAGE']=="reports"){
	  			$MenuArr["Report"]["Child"] = array(
	  					"ClassMonth"	=>	$MenuArr["Report"]["Child"]["ClassMonth"],
	  					"Search"		=>	$MenuArr["Report"]["Child"]["Search"]);

	  			If($_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
					$MenuArr["Report"]["Child"]["ClassLessonDaily"]	=	array($Lang['LessonAttendance']['LessonClassDetailReport'], $StudentAttendancePath."report/lesson_class_detail_report.php", $PageReport_ClassDetailAttendReport);
					$MenuArr["Report"]["Child"]["LateSkipLessonRecords"] =	array($Lang['LessonAttendance']['LateSkipLessonRecords'], $StudentAttendancePath."report/late_skip_lesson_records/index.php", $PageLessonAttendance_LateSkipLessonRecords);
					$MenuArr["Report"]["Child"]["LessonSummary"] = array($Lang['LessonAttendance']['LessonSummary'], $StudentAttendancePath."report/lesson_summary/index.php", $PageLessonAttendance_LessonSummary);
					$MenuArr["Report"]["Child"]["SingleStudentSummaryReport"] = array($Lang['LessonAttendance']['SingleStudentSummaryReport'], $StudentAttendancePath."report/single_student_summary_report/index.php", $PageLessonAttendance_SingleStudentSummaryReport);
					$MenuArr["Report"]["Child"]["LessonSummaryReport"] = array($Lang['LessonAttendance']['LessonSummaryReport'], $StudentAttendancePath."report/lesson_summary_report.php", $PageReport_LessonAttendSummary);
					$MenuArr["Report"]["Child"]["LessonNonconfirmedReport"] = array($Lang['LessonAttendance']['LessonNonConfirmedReport'], $StudentAttendancePath."report/lesson_non_confirmed_report/index.php", $PageReport_LessonNonconfirmedReport);
				}
	  			$MenuArr = array("Report"=>$MenuArr["Report"]);
	  		}elseif($_SESSION['PowerClass_PAGE']=="app"){
	  			$MenuArr["DailyOperation"]["Child"] = array(
	  					"ViewClassStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewClassStatus"],
	  					"ViewLateStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewLateStatus"],
	  					"ViewAbsenceStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"],
	  					"ViewEarlyLeaveStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"],
	  					"ApplyLeave(App)"	=>	$MenuArr["DailyOperation"]["Child"]["ApplyLeave(App)"],
	  					"PresetAbsence"	=>	$MenuArr["DailyOperation"]["Child"]["PresetAbsence"],
	  					"ViewDailyLessonStatus"	=>	array($Lang['LessonAttendance']['DailyLessonStatus'], $StudentAttendancePath."dailyoperation/daily_lesson_attendance/index2.php", $PageLessonAttendance_DailyOverview || $PageLessonAttendance_ImportLessonAttendanceRecords)
	  			);
	  			$MenuArr["DataImport"]["Child"] = array("ImportSmartCardID" => $MenuArr["DataImport"]["Child"]["CardID"]);
	  			$MenuArr = array(
	  			    "DailyOperation"=>$MenuArr["DailyOperation"],
	  			    "DataImport"    =>$MenuArr["DataImport"]
	  			);
	  		}
	  	}
	  }else if($sys_custom['PowerClass']){
      	if(isset($_SESSION['PowerClass_PAGE'])){
		  	if($_SESSION['PowerClass_PAGE']=="reports"){
		  		$MenuArr["Report"]["Child"] = array(
		  				"ClassMonth"	=>	$MenuArr["Report"]["Child"]["ClassMonth"],
		  				"Search"		=>	$MenuArr["Report"]["Child"]["Search"]
		  		);
		  		$MenuArr = array("Report"=>$MenuArr["Report"]);
		  	}elseif($_SESSION['PowerClass_PAGE']=="app"){
		  		$MenuArr["DailyOperation"]["Child"] = array(
		  				"ViewClassStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewClassStatus"],
		  				"ViewLateStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewLateStatus"],
		  				"ViewAbsenceStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"],
	  					"ViewEarlyLeaveStatus"	=>	$MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"],
		  				"ApplyLeave(App)"	=>	$MenuArr["DailyOperation"]["Child"]["ApplyLeave(App)"],
		  				"PresetAbsence"	=>	$MenuArr["DailyOperation"]["Child"]["PresetAbsence"]
		  		);
		  		$MenuArr["DataImport"]["Child"] = array("ImportSmartCardID" => $MenuArr["DataImport"]["Child"]["CardID"]);
		  		$MenuArr = array(
		  		    "DailyOperation"=>$MenuArr["DailyOperation"],
		  		    "DataImport"    =>$MenuArr["DataImport"]
		  		);
		  	}
      	}
	  }

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eAttendance";'."\n";
        $js.= '</script>'."\n";
	  
      # module information
      $MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'].$js;
      $MODULE_OBJ['title_css'] = "menu_opened";
      $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eAttendance.gif";
      $MODULE_OBJ['menu'] = $MenuArr;
	  $MODULE_OBJ['root_path'] = $StudentAttendancePath;

      return $MODULE_OBJ;
    }

	// V3.0 Menu
	function GET_MODULE_OBJ_ARR_30()
	{
		global $PATH_WRT_ROOT, $ip20TopMenu, $CurrentPage, $LAYOUT_SKIN, $Lang, $module_version, $sys_custom;
	
	    # Menu Item Wordings
	    global $i_SmartCard_SystemSettings, $i_general_BasicSettings, $i_SmartCard_TerminalSettings, $i_StudentAttendance_TimeSessionSettings, $i_StudentAttendance_TimeSlotSettings, $i_StudentAttendance_LunchSettings;
	    global $i_StudentAttendance_Menu_ResponsibleAdmin,$i_SmartCard_Responsible_Admin_Class;
	    global $i_StudentAttendance_Menu_DailyOperation,$i_SmartCard_DailyOperation_ViewClassStatus,$i_SmartCard_DailyOperation_ViewLateStatus,$i_SmartCard_DailyOperation_ViewAbsenceStatus,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus,$i_SmartCard_DailyOperation_ViewLeftStudents, $i_SmartCard_DailyOperation_ImportOfflineRecords, $i_SmartCard_DailyOperation_ViewEntryLog, $i_SmartCard_DailyOperation_Preset_Absence;
	    global $i_StudentAttendance_Menu_DataManagement,$i_StudentAttendance_Menu_DataMgmt_DataClear,$i_StudentAttendance_Menu_DataMgmt_BadLogs;
	    global $i_StudentAttendance_Menu_OtherFeatures,$i_StudentAttendance_Menu_OtherFeatures_Outing,$i_StudentAttendance_Menu_OtherFeatures_Detention, $i_StudentAttendance_Menu_OtherFeatures_Reminder;
	    global $i_StudentAttendance_Menu_DataExport, $i_StudentAttendance_Reminder, $i_StudentAttendance_StudentInformation, $i_StudentAttendance_OR_ExportStudentInfo;
	    global $i_StudentAttendance_Menu_DataImport,$i_StudentAttendance_ImportCardID;
	    global $i_StudentAttendance_Menu_Report, $i_StudentAttendance_Report_ClassMonth, $i_StudentAttendance_Report_ClassDaily, $i_StudentAttendance_Report_ClassBadRecords, $i_StudentAttendance_Report_StudentBadRecords, $button_find;
	    global $i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption, $i_StudentAttendance_Report_NoCardTab;
	    global $i_MedicalReason_Export2,$sys_custom, $i_StudentAttendance_StaffAndStudentInformation;
	    global $i_SmartCard_DailyOperation_Check_Time, $i_SmartCard_DailyOperation_ViewGroupStatus, $plugin;
			
			
		switch ($CurrentPage) {
              case "PageManagement_SchoolAttendance_ClassStatus":
              case "PageManagement_SchoolAttendance_GroupStatus":
              case "PageManagement_SchoolAttendance_SubjectGroupStatus":
                      $PageManagement = 1;
                      $PageManagement_SchoolAttendance = 1;
                      break;
              case "PageManagement_LessonAttendance":
              		$PageManagement = 1;
              		$PageManagement_LessonAttendance = 1;
              		break;
              case "PageManagement_ProfileConfirmation_LateList":
              case "PageManagement_ProfileConfirmation_AbsentList":
              case "PageManagement_ProfileConfirmation_EarlyLeaveList":
              		$PageManagement = 1;
              		$PageManagement_ProfileConfirmation = 1;
              		break;
              case "PageManagement_PresetAbsence":
              		$PageManagement = 1;
              		$PageManagement_PresetAbsence = 1;
              		break;
              case "PageManagement_ApplyLeaveApp":
              		$PageManagement = 1;
              		$PageManagement_ApplyLeaveApp = 1;
              		break;
              case "PageManagement_Outing":
              		$PageManagement = 1;
              		$PageManagement_Outing = 1;
              		break;
              case "PageManagement_Detention":
              		$PageManagement = 1;
              		$PageManagement_Detention = 1;
              		break;
              case "PageManagement_Reminder":
              case "PageManagement_ReminderExport":
              		$PageManagement = 1;
              		$PageManagement_Reminder = 1;
              		break;
              case "PageManagement_OfflineRecords_Import":
              case "PageManagement_OfflineRecords_Export":
              case "PageManagement_OfflineRecords_ImportCardlog":
              		$PageManagement = 1;
              		$PageManagement_OfflineRecords = 1;
              		break;
              case "PageManagement_StudentCardAndDate":
              case "PageManagement_StudentCardAndDate_SmartCardID":
              case "PageManagement_StudentCardAndDate_EntryLeaveDate":
              		$PageManagement = 1;
              		$PageManagement_StudentCardAndDate = 1;
              		break;
              case "PageManagement_CumulativeProfileNotice":
              		$PageManagement = 1;
              		$PageManagement_CumulativeProfileNotice = 1;
              		break;
              case "PageManagement_DataClear":
             		$PageManagement = 1;
             		$PageManagement_DataClear = 1;
             		break;
              
              case "PageReport_StudentArrivalTime":
              		$PageReport = 1;
              		$PageReport_StudentArrivalTime = 1;
              		break;
              case "PageReport_StudentsInSchool":
              		$PageReport = 1;
              		$PageReport_StudentsInSchool = 1;
              		break;
              		
              case "PageReport_DailyAbsentAnalysis":
              		$PageReport = 1;
              		$PageReport_DailyAbsentAnalysis = 1;
              		break;
              case "PageReport_ProveDocReport":
              		$PageReport = 1;
              		$PageReport_ProveDocReport = 1;	
              		break;
              case "PageReport_NoTappingCard":
              		$PageReport = 1;
              		$PageReport_NoTappingCard = 1;
              		break;
              case "PageReport_EntryLog":
              		$PageReport = 1;
              		$PageReport_EntryLog = 1;
              		break;
              case "PageReport_Attendance":
              case "PageReport_ClassSexRatioReport":
              case "PageReport_ClassMonth":
              case "PageReport_ClassDaily":
              		$PageReport = 1;
              		$PageReport_Attendance = 1;
              		break;
              case "PageReport_NoMissConduct":
              		$PageReport = 1;
              		$PageReport_NoMissConduct = 1;
              		break;
              case "PageReport_AdvancedReport":
              		$PageReport = 1;
              		$PageReport_AdvancedReport = 1;
              		break;
              		
             case "PageReport_LessonAttendanceReport":
             case "PageReport_LessonNonconfirmedReport":
             case "PageReport_LessonStudentReport":
             		$PageReport = 1;
              		$PageReport_LessonAttendanceReport = 1;
             		break;
			case "PageReport_StudentAttendanceRateReport":
					$PageReport = 1;
					$PageReport_StudentAttendanceRateReport = 1;
					break;
			case "PageReport_StudentAttendanceInfo":
					$PageReport = 1;
					$PageReport_StudentAttendanceInfo = 1;
					break;
					
             case "PageStatistics_ClassBadRecords":
             		$PageStatistics = 1;
             		$PageStatistics_ClassBadRecords = 1;
             		break;
             case "PageStatistics_StudentBadRecords":
             		$PageStatistics = 1;
             		$PageStatistics_StudentBadRecords = 1;
             		break;
             case "PageStatistics_AbsentSession":
             		$PageStatistics = 1;
             		$PageStatistics_AbsentSession = 1;
             		break;
             case "PageStatistics_AttendancePercentage":
             		$PageStatistics = 1;
             		$PageStatistics_AttendancePercentage = 1;
             		break;
             		
             case "PageSystemSetting_BasicSettings":
             		$PageSystemSetting = 1;
             		$PageSystemSetting_BasicSettings = 1;
             		break;
             case "PageSystemSetting_SessionSettings":
             		$PageSystemSetting = 1;
             		$PageSystemSetting_SessionSettings = 1;
             		break;
             		
             case "PageSystemSetting_ReasonSymbolSettings":
             case "PageSystemSetting_ReasonSymbolSettings_WebSAMSReasons":
             case "PageSystemSetting_ReasonSymbolSettings_OutingReasons":
             case "PageSystemSetting_ReasonSymbolSettings_ReportSymbols":
             		$PageSystemSetting = 1;
             		$PageSystemSetting_ReasonSymbolSettings = 1;
             		break;
             		
              case "PageSystemSetting_TimeSessionSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_TimeSessionSettings = 1;
                      break;
              case "PageSystemSetting_TimeSlotSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_TimeSlotSettings = 1;
                      break;
              case "PageSystemSetting_LessonSettings":
                   $PageSystemSetting = 1;
                   $PageSystemSetting_LessonSettings = 1;
                   break;
              case "PageSystemSetting_TerminalSettings":
                   $PageSystemSetting = 1;
                   $PageSystemSetting_TerminalSettings = 1;
                   break;
              case "PageSystemSetting_LunchSettings":
                      $PageSystemSetting = 1;
                      $PageSystemSetting_LunchSettings = 1;
                      break;
              case "PageSystemSetting_ClassRepresentative":
              		$PageSystemSetting = 1;
              		$PageSystemSetting_ClassRepresentative = 1;
              		break;
			  case "PageStudentAttendanceSettings_MessageTemplate":
	                  $PageSystemSetting = 1;
	                  $PageStudentAttendanceSettings_MessageTemplate = 1;
	                  break;
			  case "PageSystemSetting_ApplyLeaveAppSettings":
	                  $PageSystemSetting = 1;
	                  $PageSystemSetting_ApplyLeaveAppSettings = 1;
	                  break;
	          case "PageSystemSetting_AccessRights":
	          		$PageSystemSetting = 1;
	          		$PageSystemSetting_AccessRights = 1;
	          		break;
	          case "PageSystemSetting_WebSAMSIntegration":
	          		$PageSystemSetting = 1;
	          		$PageSystemSetting_WebSAMSIntegration = 1;
	          		break;
		}	 
		
			
		
			
		# Menu information
	      $StudentAttendancePath = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eAttendance/";
	      
	      $MenuArr["Management"] = array($Lang['StudentAttendance']['Management'], "", $PageManagement);
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	      	$MenuArr["Management"]["Child"]["SchoolAttendance"] = array($Lang['StudentAttendance']['SchoolAttendance'], $StudentAttendancePath."management/school_attendance/class/class_status.php", $PageManagement_SchoolAttendance);
	      }
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	      	$MenuArr["Management"]["Child"]["LessonAttendance"] = array($Lang['StudentAttendance']['LessonAttendance'], $StudentAttendancePath."management/lesson_attendance/index2.php", $PageManagement_LessonAttendance);
	      }
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	      	$MenuArr["Management"]["Child"]["ProfileConfirmation"] = array($Lang['StudentAttendance']['LateAbsentEarlyLeaveConfirmation'], $StudentAttendancePath."management/profile/late/showlate.php", $PageManagement_ProfileConfirmation);
	      	$MenuArr["Management"]["Child"]["PresetAbsence"] = array($i_SmartCard_DailyOperation_Preset_Absence, $StudentAttendancePath."management/preset_absence/new.php", $PageManagement_PresetAbsence);
	      	if ($plugin['eClassApp']) {
	        	$MenuArr["Management"]["Child"]["ApplyLeave(App)"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath."management/apply_leave/",$PageManagement_ApplyLeaveApp);
	        }
	        $MenuArr["Management"]["Child"]["Outing"] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, $StudentAttendancePath."management/outing/", $PageManagement_Outing);
	      	if ($_SESSION["platform"]!='KIS')
				$MenuArr["Management"]["Child"]["Detention"] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, $StudentAttendancePath."management/detention/", $PageManagement_Detention);
	      
	      	$MenuArr["Management"]["Child"]["Reminder"] = array($i_StudentAttendance_Menu_OtherFeatures_Reminder, $StudentAttendancePath."management/reminder/", $PageManagement_Reminder);
	      	$MenuArr["Management"]["Child"]["OfflineRecords"] = array($Lang['StudentAttendance']['OfflineRecords'], $StudentAttendancePath."management/offline_record/import/import.php", $PageManagement_OfflineRecords);
	      	$MenuArr["Management"]["Child"]["StudentCardAndDate"] = array($Lang['StudentAttendance']['StudentCardAndDate'], $StudentAttendancePath."management/student_card_date/studentcard/studentcard.php", $PageManagement_StudentCardAndDate);
	        if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['notice']&&$_SESSION["platform"]!='KIS') {
	        	$MenuArr["Management"]["Child"]["CumulativeProfileNotice"] = array($Lang['StudentAttendance']['CumulativeProfileNotice'], $StudentAttendancePath."management/notice/cumulative_profile_notice/",$PageManagement_CumulativeProfileNotice);
	        }
	      	$MenuArr["Management"]["Child"]["DataClear"] = array($Lang['StudentAttendance']['DataClear'], $StudentAttendancePath."management/dataclear/data_clear/", $PageManagement_DataClear);
	      }
	      
	      $MenuArr["Report"] = array($i_StudentAttendance_Menu_Report, "", $PageReport);
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	      	$MenuArr["Report"]["Child"]["StudentArrivalTime"] = array($Lang['StudentAttendance']['StudentArrivalTime'],$StudentAttendancePath."report/student_arrival_time/browse_by_student.php", $PageReport_StudentArrivalTime);
	      	$MenuArr["Report"]["Child"]["StudentsInSchool"] = array($Lang['StudentAttendance']['StudentsInSchool'], $StudentAttendancePath."report/left/summary.php", $PageReport_StudentsInSchool);
	      
	      	if($this->attendance_mode==2 || $this->attendance_mode==3)//whole day mode
	        {
	        	$MenuArr["Report"]["Child"]["DailyAbsentAnalysis"] = array($Lang['StudentAttendance']['DailyAbsentAnalysisReport'], $StudentAttendancePath."report/daily_absent_analysis/daily_absent_analysis.php", $PageReport_DailyAbsentAnalysis);
	        }
	      	$MenuArr["Report"]["Child"]["NotSubmitDocumentReport"] = array($Lang['StudentAttendance']['AbsentProveReport']['AbsentProveReport'],$StudentAttendancePath."report/prove_document_report/index.php",$PageReport_ProveDocReport);
	      	$MenuArr["Report"]["Child"]["NoTappingCard"] = array($Lang['StudentAttendance']['NoCardTappingReport'], $StudentAttendancePath."report/nocard/nocard.php", $PageReport_NoTappingCard);
	      	$MenuArr["Report"]["Child"]["EntryLog"] = array($Lang['StudentAttendance']['EntryLog'],$StudentAttendancePath."report/entry_log/index.php",$PageReport_EntryLog);
	      	$MenuArr["Report"]["Child"]["AttendanceReport"] = array($Lang['StudentAttendance']['AttendanceReport'],$StudentAttendancePath."report/attendance/class_sex_ratio_report/index.php",$PageReport_Attendance);
	      	$MenuArr["Report"]["Child"]["NoMissConduct"] = array($Lang['StudentAttendance']['NoMissConductReport'],$StudentAttendancePath."report/no_miss_conduct/index.php", $PageReport_NoMissConduct);
	      	$MenuArr["Report"]["Child"]["AdvancedReport"] = array($Lang['StudentAttendance']['AdvancedReport'], $StudentAttendancePath."report/advanced_report/search_report.php", $PageReport_AdvancedReport);
	      	
	      	// [2016-0704-1456-16054]
	      	if ($sys_custom['StudentAttendance']['ReportStudentAttendanceInfo']) {
				$MenuArr["Report"]["Child"]["StudentAttendanceInfo"] = array($Lang['StudentAttendance']['StudentAttendanceInfo']['Title'], $StudentAttendancePath."report/student_attendance_info/index.php", $PageReport_StudentAttendanceInfo);
	      	}

	      	$MenuArr["Report"]["Child"]["StudentAttendanceRate"] = array($Lang['StudentAttendance']['StudentAttendanceRate'], $StudentAttendancePath."report/student_attendance_rate/index.php", $PageReport_StudentAttendanceRateReport);
	      }
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	      	$MenuArr["Report"]["Child"]["LessonAttendanceReport"] = array($Lang['LessonAttendance']['LessonAttendanceReport'], $StudentAttendancePath."report/lesson_attendance/lesson_attendance_report/lesson_class_detail_report.php", $PageReport_LessonAttendanceReport);
	      	//$MenuArr["Report"]["Child"]["LessonNonconfirmedReport"] = array($Lang['LessonAttendance']['LessonNonConfirmedReport'], $StudentAttendancePath."report/lesson_attendance/lesson_non_confirmed_report/index.php", $PageReport_LessonNonconfirmedReport);
	      	//$MenuArr["Report"]["Child"]["LessonStudentReport"] = array($Lang['LessonAttendance']['LessonStudentReport'], $StudentAttendancePath."report/lesson_attendance/lesson_student_report/lesson_summary_report.php", $PageReport_LessonStudentReport);
	      }
	      
	      $MenuArr["Statistics"] = array($Lang['StudentAttendance']['Statistics'], "", $PageStatistics);
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	      	if ($_SESSION["platform"]!='KIS'){
	      		$MenuArr["Statistics"]["Child"]["ClassBadRecords"] = array($i_StudentAttendance_Report_ClassBadRecords, $StudentAttendancePath."statistics/class_bad/class_bad.php", $PageStatistics_ClassBadRecords);
	      		$MenuArr["Statistics"]["Child"]["StudentBadRecords"] = array($i_StudentAttendance_Report_StudentBadRecords, $StudentAttendancePath."statistics/student_bad/top_bad.php", $PageStatistics_StudentBadRecords);
	      	}
	      	if ($sys_custom['SmartCardAttendance_StudentAbsentSession']){
	      		$MenuArr["Statistics"]["Child"]["AbsentSession"] = array($Lang['StudentAttendance']['AbsentSessionTable'], $StudentAttendancePath."statistics/absent_session/search_absent_session.php", $PageStatistics_AbsentSession);
	      	}
	      	$MenuArr["Statistics"]["Child"]["AttendancePercentage"] = array($Lang['StudentAttendance']['AttendancePercentage'], $StudentAttendancePath."statistics/attendance_percentage/index.php", $PageStatistics_AttendancePercentage);
	      }
	      
	      $MenuArr["SystemSetting"] = array($i_SmartCard_SystemSettings, "", ($PageSystemSetting || (is_array($CurrentPage) && $CurrentPage['StudentEntryLeaveSetting'] == 1) || (is_array($CurrentPage) && $CurrentPage['eNoticTemplateSetting'] == 1)));
		  if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        $MenuArr["SystemSetting"]["Child"]["BasicSettings"] = array($i_general_BasicSettings, $StudentAttendancePath."settings/basic/", $PageSystemSetting_BasicSettings);
	        if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) //Henry Added 20131105
	        	$MenuArr["SystemSetting"]["Child"]["SessionSettings"] = array($Lang['StudentAttendance']['SessionSettings'], $StudentAttendancePath."settings/session/", $PageSystemSetting_SessionSettings);	
			$MenuArr["SystemSetting"]["Child"]["ReasonSymbolSettings"] = array($Lang['StudentAttendance']['ReasonSymbolSettings'], $StudentAttendancePath."settings/reason_symbol_settings/report_setting/", $PageSystemSetting_ReasonSymbolSettings);
	      
	      	$time_table_mode = $this->retrieveTimeTableMode();
	        if (($time_table_mode == 0)||($time_table_mode == "")) {
	           $MenuArr["SystemSetting"]["Child"]["TimeSlotSettings"] = array($i_StudentAttendance_TimeSlotSettings, $StudentAttendancePath."settings/slot/school/", $PageSystemSetting_TimeSlotSettings);
	        }
	        if ($time_table_mode == 1) {
	           $MenuArr["SystemSetting"]["Child"]["TimeSessionSettings"] = array($i_StudentAttendance_TimeSessionSettings, $StudentAttendancePath."settings/slot_session/session_setting/", $PageSystemSetting_TimeSessionSettings);
	        }
		  }
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	      		$MenuArr["SystemSetting"]["Child"]["LessonSetting"] = array($Lang['LessonAttendance']['LessionAttendanceSetting'], $StudentAttendancePath."settings/lesson/", $PageSystemSetting_LessonSettings);
	      }
	      if($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
	     	  $MenuArr["SystemSetting"]["Child"]["TerminalSettings"] = array($i_SmartCard_TerminalSettings, $StudentAttendancePath."settings/terminal/", $PageSystemSetting_TerminalSettings);
	      
		      if ($this->attendance_mode == "2") 
		        	$MenuArr["SystemSetting"]["Child"]["LunchSettings"] = array($i_StudentAttendance_LunchSettings, $StudentAttendancePath."settings/lunch/misc/", $PageSystemSetting_LunchSettings);
		      //if ($this->EnableEntryLeavePeriod)
		      //  	$MenuArr["SystemSetting"]["Child"]["EntryLeaveSetting"] = array($Lang['StudentAttendance']['EntryLeaveDate'], $StudentAttendancePath."settings/entry_leave_date/",($CurrentPage['StudentEntryLeaveSetting'] == 1));
		      
		      if ($_SESSION["platform"]!='KIS'){
					$MenuArr["SystemSetting"]["Child"]["ClassRepresentative"] = array($Lang['StudentAttendance']['ClassRepresentativeTakeAttendance'], $StudentAttendancePath."settings/class_representative/class/", $PageSystemSetting_ClassRepresentative);
			  }
		      
		      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['notice']&&$_SESSION["platform"]!='KIS') 
		        	$MenuArr["SystemSetting"]["Child"]["eNoticeTemplateSettings"] = array($Lang['StudentAttendance']['eNoticeTemplate'], $StudentAttendancePath."settings/enotice_template/",(is_array($CurrentPage) && $CurrentPage['eNoticTemplateSetting'] == 1));
		        	
		      if ($plugin['eClassApp']) {
		        	$MenuArr["SystemSetting"]["Child"]["ApplyLeaveSettings"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath."settings/apply_leave/",$PageSystemSetting_ApplyLeaveAppSettings);
		        	//2016-02-01
		        	$MenuArr["SystemSetting"]["Child"]["AppTemplateSettings"] = array($Lang['eClassApp']['PushMessageTemplate'], $PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/template_view.php?Module=StudentAttendance&Section=",$PageStudentAttendanceSettings_MessageTemplate);
		      }
		      
		      $MenuArr["SystemSetting"]["Child"]["AccessRights"] = array($Lang['StudentAttendance']['AccessRights'], $StudentAttendancePath."settings/access_right/", $PageSystemSetting_AccessRights);
		      $MenuArr["SystemSetting"]["Child"]["WebSAMSIntegration"] = array($Lang['StudentAttendance']['WebSAMSIntegration'], $StudentAttendancePath."settings/websams/", $PageSystemSetting_WebSAMSIntegration);
	      }
	      # End of menu
	      
/*	      
	      $MenuArr["DailyOperation"] = array($i_StudentAttendance_Menu_DailyOperation, "", $PageDailyOperation || ($CurrentPage['PrintNotice'] + $CurrentPage['CumulativeProfileNotice']) > 0);
	     	if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        $MenuArr["DailyOperation"]["Child"]["ViewClassStatus"] = array($i_SmartCard_DailyOperation_ViewClassStatus, $StudentAttendancePath."dailyoperation/class/class_status.php", $PageDailyOperation_ViewClassStatus);
	        $MenuArr["DailyOperation"]["Child"]["ViewGroupStatus"] = array($i_SmartCard_DailyOperation_ViewGroupStatus,$StudentAttendancePath."dailyoperation/group/group_status.php", $PageDailyOperation_ViewGroupStatus);
			if ($_SESSION["platform"]!='KIS')
				$MenuArr["DailyOperation"]["Child"]["ViewSubjectGroupStatus"] = array($Lang['StudentAttendance']['ViewIndividualSubjectGroupList'],$StudentAttendancePath."dailyoperation/subject_group/subject_group_status.php", $PageDailyOperation_ViewSubjectGroupStatus);
	      }
	      
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	      	$MenuArr["DailyOperation"]["Child"]["ViewDailyLessonStatus"] = array($Lang['LessonAttendance']['DailyLessonStatus'], $StudentAttendancePath."dailyoperation/daily_lesson_attendance/index2.php", $PageLessonAttendance_DailyOverview || $PageLessonAttendance_ImportLessonAttendanceRecords);
	      	//$MenuArr["DailyOperation"]["Child"]["ImportLessonAttendanceRecords"] = array($Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Title'], $StudentAttendancePath."dailyoperation/import_lesson_attendance/", $PageLessonAttendance_ImportLessonAttendanceRecords);
	      }
	      
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        $MenuArr["DailyOperation"]["Child"]["CheckTime"] = array($i_SmartCard_DailyOperation_Check_Time,$StudentAttendancePath."dailyoperation/checktime/browse_by_student.php", $PageDailyOperation_CheckTime);
	        $MenuArr["DailyOperation"]["Child"]["ViewLateStatus"] = array($i_SmartCard_DailyOperation_ViewLateStatus, $StudentAttendancePath."dailyoperation/late/showlate.php", $PageDailyOperation_ViewLateStatus);
	        $MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, $StudentAttendancePath."dailyoperation/absence/show.php", $PageDailyOperation_ViewAbsenceStatus);
	        $MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"] = array($i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, $StudentAttendancePath."dailyoperation/early/show.php", $PageDailyOperation_ViewEarlyLeaveStatus);
	        $MenuArr["DailyOperation"]["Child"]["ViewLeftStudents"] = array($i_SmartCard_DailyOperation_ViewLeftStudents, $StudentAttendancePath."dailyoperation/left/summary.php", $PageDailyOperation_ViewLeftStudents);
	        $MenuArr["DailyOperation"]["Child"]["ImportOfflineRecords"] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, $StudentAttendancePath."dailyoperation/import/import.php", $PageDailyOperation_ImportOfflineRecords);
	        $MenuArr["DailyOperation"]["Child"]["ViewEntryLog"] = array($i_SmartCard_DailyOperation_ViewEntryLog, $StudentAttendancePath."dailyoperation/entry_log/", $PageDailyOperation_ViewEntryLog);
	        $MenuArr["DailyOperation"]["Child"]["PresetAbsence"] = array($i_SmartCard_DailyOperation_Preset_Absence, $StudentAttendancePath."dailyoperation/preset_absence/new.php", $PageDailyOperation_PresetAbsence);
	        if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
		        $MenuArr["DailyOperation"]["Child"]["MissParentLetter"] = array($Lang['StudentAttendance']['DailyOperation_MissParentLetter'], $StudentAttendancePath."dailyoperation/absent_without_letter/", $PageDailyOperation_MissParentLetter);
		    if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['notice']&&$_SESSION["platform"]!='KIS') {
	        	$MenuArr["DailyOperation"]["Child"]["CumulativeProfileNotice"] = array($Lang['StudentAttendance']['CumulativeProfileNotice'], $StudentAttendancePath."dailyoperation/cumulative_profile_notice/",($CurrentPage['CumulativeProfileNotice'] == 1));
	        	$MenuArr["DailyOperation"]["Child"]["PrintNotice"] = array($Lang['StudentAttendance']['eNotice']['PrintNotice'], $StudentAttendancePath."dailyoperation/print_notice/",($CurrentPage['PrintNotice'] == 1));
	        }
	        if ($plugin['eClassApp']) {
	        	$MenuArr["DailyOperation"]["Child"]["ApplyLeave(App)"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath."dailyoperation/apply_leave/",$PageDailyOperation_ApplyLeaveApp);
	        }
	        if($sys_custom['StudentAttendance_WaiveAbsent']){
	        	$MenuArr["DailyOperation"]["Child"]["WaiveAbsence"] = array($Lang['StudentAttendance']['WaiveAbsence'], $StudentAttendancePath."dailyoperation/waive_absence/index.php", $PageDailyOperation_WaiveAbsence);
	        }
	
	        $MenuArr["DataManagement"] = array($i_StudentAttendance_Menu_DataManagement, "", $PageDataManagement);
	        $MenuArr["DataManagement"]["Child"]["DataClear"] = array($i_StudentAttendance_Menu_DataMgmt_DataClear, $StudentAttendancePath."datamgmt/dataclear/", $PageDataManagement_DataClear);
			if ($_SESSION["platform"]!='KIS')
				$MenuArr["DataManagement"]["Child"]["BadLogs"] = array($i_StudentAttendance_Menu_DataMgmt_BadLogs, $StudentAttendancePath."datamgmt/badlogs/browse.php?type=1", $PageDataManagement_BadLogs);
			//if($sys_custom['StudentAttendance']['RemovePastData']){
			$MenuArr["DataManagement"]["Child"]["RemovePastData"] = array($Lang['StudentAttendance']['RemovePastData'], $StudentAttendancePath."datamgmt/remove_past_data/index.php", $PageDataManagement_RemovePastData);
			//}
			
	        $MenuArr["OtherFeatures"] = array($i_StudentAttendance_Menu_OtherFeatures, "", $PageOtherFeatures);
	        $MenuArr["OtherFeatures"]["Child"]["Outing"] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, $StudentAttendancePath."other_features/outing/", $PageOtherFeatures_Outing);
	        if ($_SESSION["platform"]!='KIS')
				$MenuArr["OtherFeatures"]["Child"]["Detention"] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, $StudentAttendancePath."other_features/detention/", $PageOtherFeatures_Detention);
	        $MenuArr["OtherFeatures"]["Child"]["Reminder"] = array($i_StudentAttendance_Menu_OtherFeatures_Reminder, $StudentAttendancePath."other_features/reminder/", $PageOtherFeatures_Reminder);
	        if ($sys_custom['SmartCardStudentAttend_LeaveSchoolOption']) 
	        	$MenuArr["OtherFeatures"]["Child"]["LeaveOption"] = array($i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption, $StudentAttendancePath."other_features/leave_option/edit.php", $PageOtherFeatures_LeaveSchoolOption);
	        $MenuArr["OtherFeatures"]["Child"]["ViewWebSAMSReasonCode"] = array($Lang['StudentAttendance']['ReasonManagement'], $StudentAttendancePath."other_features/reason_code_view/", $PageOtherFeatures_ViewWebSAMSReasonCode);
	
	        $MenuArr["DataExport"] = array($i_StudentAttendance_Menu_DataExport, "", $PageDataExport);
					if(substr($module_version['StaffAttendance'],0,1) >= 2)
						$MenuArr["DataExport"]["Child"]["StudentInformation"] = array($Lang['StudentAttendance']['ExportForOfflineReaderUse'], $StudentAttendancePath."data_export/staff_and_student.php", $PageDataExport_StudentInformation);
					else
						$MenuArr["DataExport"]["Child"]["StudentInformation"] = array($i_StudentAttendance_StudentInformation, $StudentAttendancePath."data_export/student.php", $PageDataExport_StudentInformation);
	        $MenuArr["DataExport"]["Child"]["Reminder"] = array($i_StudentAttendance_Reminder, $StudentAttendancePath."data_export/reminder.php", $PageDataExport_Reminder);
	       //$MenuArr["DataExport"]["Child"]["ExportStudent"] = array($Lang['StudentAttendance']['ExportForOfflineReaderUse_950e'], $StudentAttendancePath."data_export/studentInfo_offline_reader_export_950e.php", $PageDataExport_ExportStudent);
	        if($sys_custom['hku_medical_research'])
	        	$MenuArr["DataExport"]["Child"]["Medical"] = array($i_MedicalReason_Export2, $StudentAttendancePath."data_export/medical.php", $PageDataExport_Medical);
	
	        $MenuArr["DataImport"] = array($i_StudentAttendance_Menu_DataImport, "", $PageDataImport);
	        $MenuArr["DataImport"]["Child"]["CardID"] = array($i_StudentAttendance_ImportCardID, $StudentAttendancePath."data_import/studentcard.php", $PageDataImport_CardID);
	      }
	      

	      $MenuArr["Report"] = array($i_StudentAttendance_Menu_Report, "", ($PageReport || ($CurrentPage['NoMissConduct'] == 1 || $CurrentPage['ClassSexRatioReport'] == 1)));
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        $MenuArr["Report"]["Child"]["ClassMonth"] = array($i_StudentAttendance_Report_ClassMonth, $StudentAttendancePath."report/class_month_full.php", $PageReport_ClassMonth);
	        $MenuArr["Report"]["Child"]["ClassDaily"] = array($i_StudentAttendance_Report_ClassDaily, $StudentAttendancePath."report/class_day.php", $PageReport_ClassDaily);
	      }
	      
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	      	$MenuArr["Report"]["Child"]["ClassLessonDaily"] = array($Lang['LessonAttendance']['LessonClassDetailReport'], $StudentAttendancePath."report/lesson_class_detail_report.php", $PageReport_ClassDetailAttendReport);
	      	$MenuArr["Report"]["Child"]["LessonSummary"] = array($Lang['LessonAttendance']['LessonSummaryReport'], $StudentAttendancePath."report/lesson_summary_report.php", $PageReport_LessonAttendSummary);
	      	$MenuArr["Report"]["Child"]["LessonNonconfirmedReport"] = array($Lang['LessonAttendance']['LessonNonConfirmedReport'], $StudentAttendancePath."report/lesson_non_confirmed_report/index.php", $PageReport_LessonNonconfirmedReport);
	      }
	      
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        if ($_SESSION["platform"]!='KIS'){
				$MenuArr["Report"]["Child"]["ClassBadRecords"] = array($i_StudentAttendance_Report_ClassBadRecords, $StudentAttendancePath."report/class_bad.php", $PageReport_ClassBadRecords);
				$MenuArr["Report"]["Child"]["StudentBadRecords"] = array($i_StudentAttendance_Report_StudentBadRecords, $StudentAttendancePath."report/top_bad.php", $PageReport_StudentBadRecords);
	        }
			$MenuArr["Report"]["Child"]["Search"] = array($button_find, $StudentAttendancePath."report/search_report.php", $PageReport_Search);
	        
	        if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) 
	        	$MenuArr["Report"]["Child"]["SearchAbsentSession"] = array($button_find." (".$Lang['StudentAttendance']['AbsentSessions'].")", $StudentAttendancePath."report/search_absent_session.php", $PageReport_SearchAbsentSession);
	        $MenuArr["Report"]["Child"]["NoTappingCard"] = array($Lang['StudentAttendance']['NoCardTappingReport'], $StudentAttendancePath."report/nocard.php", $PageReport_NoTappingCard);
	        $this->retrieveSettings();
	        if($this->attendance_mode==2 || $this->attendance_mode==3)//whole day mode
	        {
	        	$MenuArr["Report"]["Child"]["DailyAbsentAnalysis"] = array($Lang['StudentAttendance']['DailyAbsentAnalysisReport'], $StudentAttendancePath."report/daily_absent_analysis.php", $PageReport_DailyAbsentAnalysis);
	        }
	        
	        $MenuArr["Report"]["Child"]["NoMissConduct"] = array($Lang['StudentAttendance']['NoMissConductReport'],$StudentAttendancePath."report/no_miss_conduct/index.php",($CurrentPage['NoMissConduct'] == 1));
	        $MenuArr["Report"]["Child"]["ClassSexRatioReport"] = array($Lang['StudentAttendance']['ClassSexRatioReport'],$StudentAttendancePath."report/class_sex_ratio_report/index.php",($CurrentPage['ClassSexRatioReport'] == 1));
			$MenuArr["Report"]["Child"]["EntryLog"] = array($Lang['StudentAttendance']['EntryLog'],$StudentAttendancePath."report/entry_log/index.php",$PageReport_EntryLog);
			$MenuArr["Report"]["Child"]["NotSubmitDocumentReport"] = array($Lang['StudentAttendance']['AbsentProveReport']['AbsentProveReport'],$StudentAttendancePath."report/prove_document_report/index.php",$PageReport_ProveDocReport);
				}
							
				$MenuArr["SystemSetting"] = array($i_SmartCard_SystemSettings, "", ($PageSystemSetting || $CurrentPage['StudentEntryLeaveSetting'] == 1 || $CurrentPage['eNoticTemplateSetting'] == 1));
				if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        $MenuArr["SystemSetting"]["Child"]["BasicSettings"] = array($i_general_BasicSettings, $StudentAttendancePath."settings/basic/", $PageSystemSetting_BasicSettings);
	        if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) //Henry Added 20131105
	        	$MenuArr["SystemSetting"]["Child"]["SessionSettings"] = array($Lang['StudentAttendance']['SessionSettings'], $StudentAttendancePath."settings/session/", $PageSystemSetting_SessionSettings);	
	        # Report Settings - Added 2009/09/18
					$MenuArr["SystemSetting"]["Child"]["ReportSettings"] = array($Lang['SmartCard']['StudentAttendence']['ReportSettings'], $StudentAttendancePath."settings/report_setting/", $PageSystemSetting_ReportSettings);
	      }
	            
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	      	$MenuArr["SystemSetting"]["Child"]["LessonSetting"] = array($Lang['LessonAttendance']['LessionAttendanceSetting'], $StudentAttendancePath."settings/lesson/", $PageSystemSetting_LessonSettings);
	      }
	      
	      if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	        $MenuArr["SystemSetting"]["Child"]["TerminalSettings"] = array($i_SmartCard_TerminalSettings, $StudentAttendancePath."settings/terminal/", $PageSystemSetting_TerminalSettings);
	
	        $time_table_mode = $this->retrieveTimeTableMode();
	        if (($time_table_mode == 0)||($time_table_mode == "")) {
	                $MenuArr["SystemSetting"]["Child"]["TimeSlotSettings"] = array($i_StudentAttendance_TimeSlotSettings, $StudentAttendancePath."settings/slot/school/", $PageSystemSetting_TimeSlotSettings);
	        }
	        if ($time_table_mode == 1) {
	                $MenuArr["SystemSetting"]["Child"]["TimeSessionSettings"] = array($i_StudentAttendance_TimeSessionSettings, $StudentAttendancePath."settings/slot_session/session_setting/", $PageSystemSetting_TimeSessionSettings);
	        }
	        if ($this->attendance_mode == "2") 
	        	$MenuArr["SystemSetting"]["Child"]["LunchSettings"] = array($i_StudentAttendance_LunchSettings, $StudentAttendancePath."settings/lunch/list/", $PageSystemSetting_LunchSettings);
	        if ($this->EnableEntryLeavePeriod)
	        	$MenuArr["SystemSetting"]["Child"]["EntryLeaveSetting"] = array($Lang['StudentAttendance']['EntryLeaveDate'], $StudentAttendancePath."settings/entry_leave_date/",($CurrentPage['StudentEntryLeaveSetting'] == 1));
	        if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['notice']&&$_SESSION["platform"]!='KIS') 
	        	$MenuArr["SystemSetting"]["Child"]["eNoticeTemplateSettings"] = array($Lang['StudentAttendance']['eNoticeTemplate'], $StudentAttendancePath."settings/enotice_template/",($CurrentPage['eNoticTemplateSetting'] == 1));
	        	
	        if ($plugin['eClassApp']) {
	        	$MenuArr["SystemSetting"]["Child"]["eNoticeTemplateSettings"] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'], $StudentAttendancePath."settings/apply_leave/",$PageSystemSetting_ApplyLeaveAppSettings);
	        	$MenuArr["SystemSetting"]["Child"]["AppTemplateSettings"] = array($Lang['eClassApp']['PushMessageTemplate'], $PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/template_view.php?Module=StudentAttendance&Section=NotSubmitDocument",$PageStudentAttendanceSettings_MessageTemplate);
	        }
	        	
			if ($_SESSION["platform"]!='KIS'){
				$MenuArr["AccountResponsibleAdmin"] = array($i_StudentAttendance_Menu_ResponsibleAdmin, "", $PageAccountResponsibleAdmin);
				$MenuArr["AccountResponsibleAdmin"]["Child"]["ClassRepresentative"] = array($i_SmartCard_Responsible_Admin_Class, $StudentAttendancePath."admin/class/list/", $PageAccountResponsibleAdmin_ClassRepresentative);
			}
		}
	      
	      
*/
        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eAttendance";'."\n";
        $js.= '</script>'."\n";
        $MODULE_OBJ['title'] .= $js;

		# module information
	      $MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'].$js;
	      $MODULE_OBJ['title_css'] = "menu_opened";
	      $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eAttendance.gif";
	      $MODULE_OBJ['menu'] = $MenuArr;
		  $MODULE_OBJ['root_path'] = $StudentAttendancePath;
		
		return $MODULE_OBJ;
	}


        function createTable_Card_Student_Lunch_Box_Option($year="",$month="")
        {
                 $year=($year=="")?date("Y"):$year;
                 $month=($month=="")?date("m"):$month;
                 $tablename = "CARD_STUDENT_ATTENDANCE_LIST_LUNCHBOX_OPTION_".$year."_".$month;

                 $sql = "CREATE TABLE IF NOT EXISTS $tablename (
                                RecordID int(11) NOT NULL auto_increment,
                                StudentID int(11) NOT NULL,
                                DayNumber int(11) NOT NULL,
                                CancelOption int(11) default 0,
                                RecordType int,
                                RecordStatus int,
                                DateInput datetime,
                                DateModified datetime,
                                PRIMARY KEY (RecordID),
                                UNIQUE StudentDay (StudentID, DayNumber),
                                INDEX StudentID (StudentID),
                                INDEX DayNumber (DayNumber)
                        ) ENGINE=InnoDB charset=utf8";
                 $this->db_db_query($sql);
                 return $tablename;

        }


        // return available websams level
        function returnAvailableWebSamsLevel(){
	        global $config_school_type;

	         if ($config_school_type=="P")
	         {
	             $available_level = array("P1","P2","P3","P4","P5","P6");
	         }
	         else if ($config_school_type=="S")
	         {
	              $available_level = array("S1","S2","S3","S4","S5","S6","S7");
	         }
	         else
	         {
	             $available_level = array("P1","P2","P3","P4","P5","P6",
	                                "S1","S2","S3","S4","S5","S6","S7");
	         }
	        return $available_level;
	    }
    ## param: $NoColumnHeader - return with/without Column Header
    ## return -1 if none WebSAMS Level has been set
    function generateMedicalReasonSummary($TargetDate,$NoColumnHeader=false)
    {
    	global $config_school_type, $BroadlearningClientName,$BroadlearningClientID, $i_MedicalReasonName;

		 	$available_level = $this->returnAvailableWebSamsLevel();

			$ts = strtotime($TargetDate);
			$year = date("Y",$ts);
			$month = date("m",$ts);
			$day = date("d",$ts);
			
			// reorder medical reason, put Undefined to the end
			$medical_reason_list = array();
			for($i=1;$i<sizeof($i_MedicalReasonName);$i++) {
				$medical_reason_list[] = $i_MedicalReasonName[$i];
			}
			$medical_reason_list[] = $i_MedicalReasonName[0];
			
			# Retrieve Data
			if ($this->attendance_mode=="NO")
			{
			   $this->retrieveSettings();
			}
			# Retrieve Class Level List
			$sql = "Select 
			 					yc.ClassTitleEN as ClassName, 
			 					y.WEBSAMSCode as WebSAMSLevel 
			 				From 
			 					YEAR as y 
			 					inner join 
			 					YEAR_CLASS as yc 
			 					on y.YearID = yc.YearID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
			       ";
			$classes = $this->returnArray($sql,2);
			
			
			$none_websamslevel = true;
			
			unset($class_data);
			unset($result_count_total);
			unset($result_data);
			
			
			for ($i=0; $i<sizeof($classes); $i++)
			{
			    list($t_classname, $t_webSamsClassLevel) = $classes[$i];
			    if (in_array($t_webSamsClassLevel, $available_level))
			    {
			      $none_websamslevel = false;
			        $class_data[$t_webSamsClassLevel][] = $t_classname;
			    }
			    else
			    {
			    	# auto-map websams level if not set by school
			    	$t_classname_level_int = (int) str_replace("S", "", str_replace("F", "", str_replace("P", "", $t_classname)));
			    	if ($t_classname_level_int>0)
			    	{			    		
			        	$classes[$i][1] = "S".$t_classname_level_int;
			      		$none_websamslevel = false;
			        	$class_data[$classes[$i][1]][] = $t_classname;
			    	}
			        $other_class[] = $t_classname;
			    }
			}
			
			if($none_websamslevel){ // no websams level has been set
			 return -1;
			}

#                 if ($this->attendance_mode==0 || $this->attendance_mode==1) # AM Only or PM Only
#                 {
			foreach ($class_data as $t_webSamsLevel => $t_classArray)
			{
			   $t_class_list = "'".implode("','",$t_classArray)."'";
			   $sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND ClassName IN ($t_class_list)";
			   
			   $temp = $this->returnVector($sql);
			   $result_count_total[$t_webSamsLevel] = $temp[0];
			}
			# Get a list of absence
			$sql = "SELECT 
								user.UserID, 
								level.WEBSAMSCode, 
								day_log.AMStatus, 
								am_reason.MedicalReasonType,
			          day_log.PMStatus, 
			          pm_reason.MedicalReasonType, user.ClassName
			       	FROM 
			       		CARD_STUDENT_DAILY_LOG_".$year."_".$month." as day_log
								LEFT OUTER JOIN 
								INTRANET_USER as user 
								ON day_log.UserID = user.UserID
								LEFT OUTER JOIN 
								SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as am_reason
						    ON day_log.UserID = am_reason.StudentID
						       AND am_reason.RecordDate = '$TargetDate'
						       AND am_reason.DayType = '".PROFILE_DAY_TYPE_AM."'
								LEFT OUTER JOIN 
								SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as pm_reason
						    ON day_log.UserID = pm_reason.StudentID
						       AND pm_reason.RecordDate = '$TargetDate'
						       AND pm_reason.DayType = '".PROFILE_DAY_TYPE_PM."'
								LEFT OUTER JOIN 
								YEAR_CLASS as class 
								ON user.ClassName = class.ClassTitleEN and class.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
								LEFT OUTER JOIN 
								YEAR as level 
								ON level.YearID = class.YearID
							WHERE day_log.DayNumber = '$day'
								AND (day_log.AMStatus='".CARD_STATUS_ABSENT."' OR day_log.PMStatus='".CARD_STATUS_ABSENT."')
			           ";
			//debug_r($sql);
			$abs_data = $this->returnArray($sql,7);
			for ($i=0; $i<sizeof($abs_data); $i++)
			{
			    # auto-map websams level if not set by school
			    list($t_userID, $t_webSamsLevel, $t_amStatus, $t_amReason, $t_pmStatus, $t_pmReason, $t_ClassName) = $abs_data[$i];
			    if ($t_webSamsLevel==null or $t_webSamsLevel=="")
			    {
			    	$t_classname_level_int = (int) str_replace("S", "", str_replace("F", "", str_replace("P", "", $t_ClassName)));
			    	$t_webSamsLevel = "P".$t_classname_level_int;
			    }
			    if ($this->attendance_mode==0)
			    {
			        if ($t_amStatus == CARD_STATUS_ABSENT)
			        {
			        	/*
			            if ($t_amReason == 1)                              # ILI/Resp
			            {
			                $result_data[$t_webSamsLevel][1]++;
			            }
			            else if ($t_amReason == 2)                         # GI illness
			            {
			                $result_data[$t_webSamsLevel][2]++;
			            }
			            else if ($t_amReason == 99)                        # Other illness
			            {
			                $result_data[$t_webSamsLevel][99]++;
			            }
			            else if ($t_amReason == 999)                       # Not illness
			            {
			                $result_data[$t_webSamsLevel][999]++;
			            }
			            else                                               # Undefined
			            {
			                $result_data[$t_webSamsLevel][0]++;
			            }
			            */
			            if($t_amReason > 0) {
			            	$result_data[$t_webSamsLevel][$t_amReason]++;
			            }else{
			            	$result_data[$t_webSamsLevel][0]++; // Undefined
			            }
			        }
			    }
			    else if ($this->attendance_mode==1)
			    {
			        if ($t_pmStatus == CARD_STATUS_ABSENT)
			        {
			        	/*
			            if ($t_pmReason == 1)                              # ILI/Resp
			            {
			                $result_data[$t_webSamsLevel][1]++;
			            }
			            else if ($t_pmReason == 2)                         # GI illness
			            {
			                $result_data[$t_webSamsLevel][2]++;
			            }
			            else if ($t_pmReason == 99)                        # Other illness
			            {
			                $result_data[$t_webSamsLevel][99]++;
			            }
			            else if ($t_pmReason == 999)                       # Not illness
			            {
			                $result_data[$t_webSamsLevel][999]++;
			            }
			            else                                               # Undefined
			            {
			                $result_data[$t_webSamsLevel][0]++;
			            }
			            */
			            if($t_pmReason > 0) {
			            	$result_data[$t_webSamsLevel][$t_pmReason]++;
			            }else{
			            	$result_data[$t_webSamsLevel][0]++; // Undefined
			            }
			        }
			    }
			    else if ($this->attendance_mode==2 || $this->attendance_mode==3)
			    {
			        if ($t_amStatus == CARD_STATUS_ABSENT && ($t_amReason != 0) && ($t_amReason != "") )  # AM Absent and with medical reason
			        {
			          //echo "Part 1";
			          /*
			            if ($t_amReason == 1)                              # ILI/Resp
			            {
			                $result_data[$t_webSamsLevel][1]++;
			            }
			            else if ($t_amReason == 2)                         # GI illness
			            {
			                $result_data[$t_webSamsLevel][2]++;
			            }
			            else if ($t_amReason == 99)                        # Other illness
			            {
			                $result_data[$t_webSamsLevel][99]++;
			            }
			            else if ($t_amReason == 999)                       # Not illness
			            {
			                $result_data[$t_webSamsLevel][999]++;
			            }
			            else                                               # Undefined
			            {
			                $result_data[$t_webSamsLevel][0]++;
			            }
			            */
			            if($t_amReason > 0) {
			            	$result_data[$t_webSamsLevel][$t_amReason]++;
			            }else{
			            	$result_data[$t_webSamsLevel][0]++; // Undefined
			            }
			        }
			        else if ($t_pmStatus==CARD_STATUS_ABSENT)  # PM Absent (As AM has not been set, just use PM medical reason)
			        {
			          //echo "Part 2";
			          /*
			            if ($t_pmReason == 1)                              # ILI/Resp
			            {
			                $result_data[$t_webSamsLevel][1]++;
			            }
			            else if ($t_pmReason == 2)                         # GI illness
			            {
			                $result_data[$t_webSamsLevel][2]++;
			            }
			            else if ($t_pmReason == 99)                        # Other illness
			            {
			                $result_data[$t_webSamsLevel][99]++;
			            }
			            else if ($t_pmReason == 999)                       # Not illness
			            {
			                $result_data[$t_webSamsLevel][999]++;
			            }
			            else                                               # Undefined
			            {
			                $result_data[$t_webSamsLevel][0]++;
			            }
			            */
			            if($t_pmReason > 0) {
			            	$result_data[$t_webSamsLevel][$t_pmReason]++;
			            }else{
			            	$result_data[$t_webSamsLevel][0]++; // Undefined
			            }
			        }
			        else if ($t_amStatus == CARD_STATUS_ABSENT) # AM Absent only, but no medical reason set
			        {
			          //echo "Part 3";
			            $result_data[$t_webSamsLevel][0]++;
			        }
			        /*
			        # Check AM Reason is "Undefined" but PM Reason is Changed #
			        if (($t_amStatus == CARD_STATUS_ABSENT) && ($t_pmStatus == CARD_STATUS_ABSENT) && ($t_amReason == 0) && ($t_pmReason != 0))
			      {
			        if ($t_pmReason == 1)                              # ILI/Resp
			            {
			                $result_data[$t_webSamsLevel][1]++;
			            }
			            else if ($t_pmReason == 2)                         # GI illness
			            {
			                $result_data[$t_webSamsLevel][2]++;
			            }
			            else if ($t_pmReason == 99)                        # Other illness
			            {
			                $result_data[$t_webSamsLevel][99]++;
			            }
			            else if ($t_pmReason == 999)                       # Not illness
			            {
			                $result_data[$t_webSamsLevel][999]++;
			            }
			            else                                               # Undefined
			            {
			                $result_data[$t_webSamsLevel][0]++;
			            }
			            $result_data[$t_webSamsLevel][0]--;				 # del 1 from the total no. of "Undefined"
			      }
			      */
			
			    }
			
			}
			# Combine to csv
			$content = "";
			if(!$NoColumnHeader){
				//$title_row = "\"Date\",\"Name of school\",\"Primary/Secondary\",\"Year/Form\",\"ILI/ resp illness\",\"GI illness\",\"Other illness\",\"Not illness\",\"Undefined\",\"Total no. of registered students\"\r\n";
				$title_row = "\"Date\",\"Name of school\",\"Primary/Secondary\",\"Year/Form\"";
				for($i=0;$i<count($medical_reason_list);$i++) {
					$title_row .= ",\"".$medical_reason_list[$i][1]."\"";
				}
				$title_row .= ",\"Total no. of registered students\"\r\n";
				$content = $title_row;
			}
			for ($i=0; $i<sizeof($available_level); $i++)
			{
			
			 //if($result_data[$available_level[$i]]=="" || sizeof($result_data[$available_level[$i]])<=0)
			 //	continue;
			    $content .= "\"".$TargetDate."\",\"".$BroadlearningClientName."(".$BroadlearningClientID.")"."\"";
			    # Primary / Secondary
			    $sch_level = substr($available_level[$i], 0,1);
			    $form_level = substr($available_level[$i],1,1);
			    $content .= ",\"".$sch_level."\",\"".$form_level."\"";
			    /*
			    $content .= ",\"".$result_data[$available_level[$i]][1]."\"";
			    $content .= ",\"".$result_data[$available_level[$i]][2]."\"";
			    $content .= ",\"".$result_data[$available_level[$i]][99]."\"";
			    $content .= ",\"".$result_data[$available_level[$i]][999]."\"";
			    $content .= ",\"".$result_data[$available_level[$i]][0]."\"";
			    */
			    for($j=0;$j<count($medical_reason_list);$j++) {
			    	$content .= ",\"".$result_data[$available_level[$i]][$medical_reason_list[$j][0]]."\"";
			    }
			    $content .= ",\"".$result_count_total[$available_level[$i]]."\"";
			    $content .= "\r\n";
			}
			//debug_r($content);
			return $content;
    }

    function sendMedicalReport($TargetDate)
    {
			global $sys_custom_config, $BroadlearningClientName,$BroadlearningClientID, $intranet_root, $file_path, $SYS_CONFIG, $Lang, $i_MedicalReasonName, $sys_custom;
			
			$i_MedicalReasonName = array(); // reset $i_MedicalReasonName array and reload in lang.en.ip20.php
      		include_once("$intranet_root/includes/libwebmail.php");
      		include("$intranet_root/lang/lang.en.ip20.php"); // use English as the report language
      		include("$intranet_root/lang/lang.en.php"); // use English as the report language
			$lwebmail = new libwebmail();
      		$webmaster_email = trim($lwebmail->GetWebmasterMailAddress());
			if ($sys_custom_config['hku_medical_research']['email']=="")
			{
			   return false;
			}
			
			
			# Retrieve data content
			$content = $this->generateMedicalReasonSummary($TargetDate);

			if($content == -1)
				return false;
			
			# Prepare send email
			
			# Fixed parameters
			$type="application/octet-stream";
			$priority = 1;
			$charset = "utf-8";
			
			$from = ($webmaster_email!="" && strlen($webmaster_email)>8) ? $webmaster_email : "system@".($SYS_CONFIG['Mail']['UserNameSubfix']!="" ? $SYS_CONFIG['Mail']['UserNameSubfix'] : $_SERVER['HTTP_HOST']);

			$to = $sys_custom_config['hku_medical_research']['email'];
			$reply_address = $from;
			$mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
			$fname = "attendance_report_".$BroadlearningClientID."_".$TargetDate.".csv";
			
			# Subject & Message
			$subject = "School Attendance Report [$BroadlearningClientName($BroadlearningClientID) - $TargetDate]";
			$message = "Attachment";
			
			# Mail Header
			$headers='';
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "X-Priority: $priority\n";
			$headers .= "Content-Type: multipart/mixed; ";
			$headers .= " boundary=\"" . $mime_boundary . "\"\n";
			
			$headers .= "From: $from\n";
			if ($reply_address != "")
			{
			   $headers .= "Reply-To: $reply_address\n";
			}
			$mime = "This is a multi-part message in MIME format.\n";
			$mime .= "\n";
			$mime .= "--" . $mime_boundary . "\n";
			
			# Message Body
			$mime .= "Content-Transfer-Encoding: 7bit\n";
			$mime .= "Content-Type: text/plain; charset=\"$charset\"\n";
			$mime .= "\n";
			
			$mime .= $message;
			$mime .= "\n\n";
			$mime .= "--" . $mime_boundary. "\n";
			
			# Attachment
			$mime .= "Content-Type: $type;\n";
			#$mime .= "\tname=\"".base64_encode($fname)."\"\n";
			$mime .= "\tname=\"".$fname."\"\n";
			$mime .= "Content-Transfer-Encoding: base64\n";
			$mime .= "Content-Disposition: attachment;\n ";
			$mime .= "\tfilename=\"".$fname."\"\n\n";
			#$mime .= "\tfilename=\"".base64_encode($fname)."\"\n\n";
			$mime .= chunk_split(base64_encode($content));
			$mime .= "\n\n";
			$mime .= "--" . $mime_boundary;
			$mime .= "--\n";

			//$result = mail($to, $subject, $mime, $headers, "-f $from");
			$result = mail($to, $subject, $mime, $headers);
			
			//---- Start posting data to HKU
			if(isset($sys_custom_config['hku_medical_research']['post_url'])){	
				$url = $sys_custom_config['hku_medical_research']['post_url'];
			}else {
			 	$url = "https://sa.influenza.hk/upload_broadlearning.php";
			}
			if(isset($sys_custom_config['hku_medical_research']['post_key'])) {
				$key = $sys_custom_config['hku_medical_research']['post_key'];
			}else{
				$key = "df2c78b843dd2c3d5cfb04eaae6f45241678781f3ec4e5c041c2c4b7bfa3e0b2b16e80a85172ff2d28e09a1c05332eea1143d79c60ecb5cc492853dfbbd43bbd";
			}
			$postData = array();
			$postData['key'] = $key;
			$postData['file_name'] = $fname;
			$postData['file_content'] = $content;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			$response = curl_exec($ch);
			curl_close($ch);
			
			if($sys_custom['hku_flu_log']){
				$log_content = $url."\n".$postData['file_content']."\n".$response."\n\n";
				$log_file = $file_path."/file/hku_flu_log.txt";
				$file_handle = fopen($log_file,"a+");
				fwrite($file_handle,$log_content);
				fclose($file_handle);
			}
			//---- End of posting data to HKU
			
			return $result;


    }
		function getGroupListMode($recordTypeID)
        {
			/*
			1) recordtype in INTRANET_GROUP = x (3 ==> class type)
			2) groupid exist in INTRANET_GROUP but not exist in INTRANET_CLASS
			3) INTRANET_GROUP.groupid exist in CARD_STUDENT_ATTENDANCE_GROUP (left outer join)
			*/
			
			global $intranet_session_language;
			$title_field = $intranet_session_language =="en" ? "a.Title":"a.TitleChinese"; 
			
			$sql = "select 
								a.groupid, 
								$title_field,
								c.mode
							from 
								INTRANET_GROUP as a 
								left outer join 
								YEAR_CLASS as b 
								on a.groupid = b.groupid
								left outer join 
								CARD_STUDENT_ATTENDANCE_GROUP c 
								on a.groupid = c.groupid
							where 
								a.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
								and 
								b.groupid is null 
								and 
								a.recordtype='".$recordTypeID."'
							order by a.title";
					return $this->returnArray($sql,3);

		}
	   function getSpecialGroupName($groupId)
       {
                $sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '".$groupId."'";
                $result = $this->returnVector($sql);
                return $result[0];
       }
	   function getSpecialGroupAttendanceMode($GroupId)
       {
                         $sql = "SELECT mode FROM CARD_STUDENT_ATTENDANCE_GROUP
                                        WHERE GroupID = '$GroupId'";
                         $temp = $this->returnVector($sql);
                         return $temp[0]+0;
       }
	   function getGroupSessionTimeArrayList($type,$group_id)
                {
                         if ($type != 1 && $type != 2) return;
                         if ($group_id == 0)
                                         $cond = " b.groupid = 0 ";
                         else
                                         $cond = " b.groupid = $group_id ";

                         $sql = "SELECT
                                                         a.SessionID, TIME_FORMAT(a.MorningTime,'%H:%i'), TIME_FORMAT(a.LunchStart,'%H:%i'), TIME_FORMAT(a.LunchEnd,'%H:%i'), TIME_FORMAT(a.LeaveSchoolTime,'%H:%i'), a.NonSchoolDay, b.DayValue
                                 FROM
                                                 CARD_STUDENT_TIME_SESSION AS a INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b ON (a.SessionID = b.SessionID)
                                 WHERE
                                                 b.DayType = '$type' AND $cond ORDER BY b.DayValue";
                         return $this->returnArray($sql,7);
        }

		function getSessionDayValueWithoutSpecificForGroup($GroupID,$type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_TIME_SESSION_REGULAR_GROUP
                                        WHERE DayType = '$type' AND GroupID = '$GroupID' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }
                function getGroupSessionDayValueWithoutSpecific($type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_TIME_SESSION_REGULAR_GROUP
                                        WHERE DayType = '$type' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }

				function getGroupTimeArray($GroupID,$type,$value)
                {
                		 if($this->isStudentAttendance30()){
				    		$time_format = '%H:%i:%s';
				    	 }
				    	 else{
				    		$time_format = '%H:%i';
				    	 }
				    	 
                         if ($type==0)      # Normal
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'".$time_format."'), TIME_FORMAT(LunchStart,'".$time_format."'), TIME_FORMAT(LunchEnd,'".$time_format."'), TIME_FORMAT(LeaveSchoolTime,'".$time_format."'),NonSchoolDay
                                            FROM CARD_STUDENT_GROUP_PERIOD_TIME WHERE DayType = 0 AND DayValue = 0 AND GroupID = '$GroupID'";
                             $temp = $this->returnArray($sql,5);

                             return $temp[0];
                         }
                         else
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'".$time_format."'), TIME_FORMAT(LunchStart,'".$time_format."'), TIME_FORMAT(LunchEnd,'".$time_format."'), TIME_FORMAT(LeaveSchoolTime,'".$time_format."'),NonSchoolDay
                                            FROM CARD_STUDENT_GROUP_PERIOD_TIME WHERE DayType = '$type' AND DayValue ='$value' AND GroupID = '$GroupID'";

                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                }
								function getGroupTimeArrayList($groupID,$type)
                {
                         if ($type != 1 && $type != 2) return;
                         $sql = "SELECT DayValue, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_GROUP_PERIOD_TIME
                                        WHERE DayType = '$type' AND GROUPID = '$groupID' ORDER BY DayValue";
                         return $this->returnArray($sql,6);
                }

				function getDayValueWithoutSpecificForGroup($GroupID,$type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_GROUP_PERIOD_TIME
                                        WHERE DayType = '$type' AND GROUPID = '$GroupID' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }
				function getGroupSpecialTimeArray($groupID,$TargetDate){
                                                                        $sql="SELECT RecordDate,TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP  WHERE GroupID='$groupID' AND RecordDate='$TargetDate' ORDER BY RecordDate";
                                   $temp=$this->returnArray($sql,6);
                                   return $temp[0];

                }

                # studentid       : student id or array of student id
                # target_date     : attendance date
                # timestamp       : page load time
                # confirmedUserID : user id of login user
                # return  : true - expired ,  false - not expired
                /* function isDataOutdated($student_id,$target_date,$timestamp,$confirmedUserID=null){
	                if($student_id=="") return true;
	                if(!is_array($student_id)){
		                $student_id = array($student_id);
		            }
		            $student_list = implode(",",$student_id);
		            $target_ts = strtotime($target_date);
		            $year = date('Y',$target_ts);
		            $month = date('m',$target_ts);
		            $day = date('j',$target_ts);
		            if ($confirmedUserID!=null)
		            {
			            $user_matching_sql = "AND ConfirmedUserID <> $confirmedUserID";
		            }
		            $sql="SELECT COUNT(*) FROM CARD_STUDENT_DAILY_LOG_".$year."_".$month." WHERE UserID IN ($student_list) AND DayNumber = $day AND (UNIX_TIMESTAMP(DateModified) >=$timestamp $user_matching_sql ) ";
		            $temp = $this->returnVector($sql);
		            return $temp[0]>0;
	            } */

	            function isDataOutdated($student_id,$target_date,$timestamp){
		            if($student_id=="") return true;

	                if(!is_array($student_id)){
		                $student_id = array($student_id);
		            }
		            $student_list = "'".implode("','",$student_id)."'";
		            $target_ts = strtotime($target_date);
		            $year = date('Y',$target_ts);
		            $month = date('m',$target_ts);
		            $day = date('j',$target_ts);
		            //if ($_SESSION['UserID']>0 && isset($_SESSION['UserID']))
		            //{
			        //    $user_matching_sql = "AND ConfirmedUserID <> '".$_SESSION['UserID']."'";
		            //}
		            $sql="SELECT 
		            				COUNT(*) 
		            			FROM 
		            				CARD_STUDENT_DAILY_LOG_".$year."_".$month." 
		            			WHERE 
		            				UserID IN ($student_list) 
		            				AND DayNumber = $day 
		            				AND (UNIX_TIMESTAMP(DateModified) >= '$timestamp' 
		            					OR UNIX_TIMESTAMP(PMDateModified) >= '$timestamp')";
		            //$sql="SELECT COUNT(*) FROM CARD_STUDENT_DAILY_LOG_".$year."_".$month." WHERE UserID IN ($student_list) AND DayNumber = $day AND DateModified >= '$timestamp' $user_matching_sql ";
		            $temp = $this->returnVector($sql);
		            if($temp[0] > 0){
			            return true;
		            }else{
			            return false;
		            }
		            //return $temp[0]>0;
	            }

	            # LOCK TABLES for Admin Console Confirmation Pages
	            # $target_date : Attendance Date in yyyy-mm-dd format (e.g 2007-09-01)
	            function lockTablesAdmin($target_date){
		            /*
		            global $plugin;

					if($target_date=="") return;

					$ts_record = strtotime($TargetDate);
					$txt_year = date('Y',$ts_record);
					$txt_month = date('m',$ts_record);

					$log_table  = $lcardattend->createTable_Card_Student_Daily_Log($txt_year,$txt_month);
					$confirm_table = $lcardattend->createTable_Card_Student_Daily_Class_Confirm($txt_year,$txt_month);

					$table_list = array();
					$table_list[] = "$log_table WRITE";
					$table_list[] = "$confirm_table WRITE";
					$table_list[] = "PROFILE_STUDENT_ATTENDANCE WRITE";
					$table_list[] = "CARD_STUDENT_BAD_ACTION WRITE";
					$table_list[] = "CARD_STUDENT_PROFILE_RECORD_REASON WRITE";

					if(isset($plugin['Discipline']) && $plugin['Discipline']){
						$table_list[] = "DISCIPLINE_ACCU_PERIOD READ";
						$table_list[] = "DISCIPLINE_ACCU_PERIOD AS a READ";
						$table_list[] = "DISCIPLINE_ACCU_CATEGORY_PERIOD AS h READ";
						$table_list[] = "DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b READ";
						$table_list[] = "DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE AS c READ";
					}

					$this->db_db_query($sql);
					*/

		        }
		        function lockTablesTeacher(){
			    }
			    function unlockTables(){
				    /*
				    $sql="UNLOCK TABLES";
				    $this->db_db_query($sql);
				    */
				}
				
				function ReportDisplayStudentNameFormat()
				{
					# implement later
					# 1 - Chinese name only
					# 2 - English name only
					# 3 - Both Chinese and English name
					
					return "3";
				}
  /* 
   // commented on 2012-05-02      
  function Get_Subject_Group_Session($StaffID,$TargetDate="",$SubjectGroupListOnly="") {
  	$TargetDate = ($TargetDate == "")? date("Y-m-d"):$TargetDate;
  	
  	$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
  	if ($CycleDayInfo[0] == 'Cycle') {
  		$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
  	}
		
  	$sql = 'select
						  stc.SubjectGroupID,
						  stc.ClassTitleEN,
						  stc.ClassTitleB5,';
		if ($SubjectGroupListOnly == "") {
			$sql .= '
						  itra.RoomAllocationID,
						  itt.TimeSlotName,
						  itt.StartTime,
						  itt.EndTime,
						  ';
		}
		$sql .= '
							stc.ClassTitleEN, 
						  stc.ClassTitleB5
						From
						  ACADEMIC_YEAR_TERM as ayt
						  inner join
						  SUBJECT_TERM as st
						  on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\'
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on st.SubjectGroupID = stc.SubjectGroupID
						  inner join
						  SUBJECT_TERM_CLASS_TEACHER as stct
						  on stc.SubjectGroupID = stct.SubjectGroupID and UserID = \''.$StaffID.'\'
						  inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on stc.SubjectGroupId = itra.SubjectGroupID
						  inner join
						  INTRANET_SCHOOL_TIMETABLE as ist
						  on itra.TimetableID = ist.TimetableID
						  inner join
						  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
						  on ist.TimeTableID = iptr.TimeTableID
						  inner join
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
						  on
						    iptr.PeriodId = icgp.PeriodID
						    and \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
						    and itra.Day = IF(PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1)
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT as itt
						  on itra.TimeSlotID = itt.TimeSlotID ';
		if ($SubjectGroupListOnly == "") {
			$sql .= '
						  order by
						  	itt.StartTime';
		}
		else {
			$sql .= '
							group by 
								SubjectGroupID 
							order by 
								stc.ClassTitleEN
							';
		}
		//debug_r($sql);
		return $this->returnArray($sql,9);
  }
	*/
	/*function Get_Subject_Group_Session($StaffID,$TargetDate="",$SubjectGroupListOnly="") 
	{
		include_once("libtimetable.php");
		
		$libtimetable = new Timetable();
	  	$TargetDate = ($TargetDate == "")? date("Y-m-d"):$TargetDate;
	  	$timetableID = $libtimetable->Get_Current_Timetable($TargetDate);
	  	
	  	if(trim($timetableID)!='' && $timetableID > 0){
	  		$sql = "select
						  stc.SubjectGroupID,
						  stc.ClassTitleEN,
						  stc.ClassTitleB5,";
		if ($SubjectGroupListOnly == "") {
			$sql .= "	  itra.RoomAllocationID,
						  itt.TimeSlotName,
						  itt.StartTime,
						  itt.EndTime,";
		};
			$sql .= "	  stc.ClassTitleEN, 
						  stc.ClassTitleB5 
					 From 
						  ACADEMIC_YEAR_TERM as ayt
						  inner join
						  SUBJECT_TERM as st
						  on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on st.SubjectGroupID = stc.SubjectGroupID
						  inner join
						  SUBJECT_TERM_CLASS_TEACHER as stct
						  on stc.SubjectGroupID = stct.SubjectGroupID and stct.UserID = '".$StaffID."' 
						  inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$timetableID."'
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT as itt
						  on itra.TimeSlotID = itt.TimeSlotID 
					";
	  		
	  		if ($SubjectGroupListOnly == "") {
				$sql .= "order by
							itt.StartTime";
			}
			else {
				$sql .= "group by 
							SubjectGroupID 
						order by 
							stc.ClassTitleEN ";
			}
	  		//debug_r($sql);
			return $this->returnArray($sql,9);
	  	}
	  	return array();
	}*/
	function Get_Subject_Group_Session($StaffID,$TargetDate="",$SubjectGroupListOnly="",$GetAllLessons=0) 
	{
		global $sys_custom;
		include_once("libtimetable.php");
		
		$libtimetable = new Timetable();
	  	$TargetDate = ($TargetDate == "")? date("Y-m-d"):$TargetDate;
	  	$timetableID = $libtimetable->Get_Current_Timetable($TargetDate);
	  	
	  	if(trim($timetableID)!='' && $timetableID > 0){
	  		$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
			if ($CycleDayInfo[0] == 'Cycle') {
				$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
				$DayCond = " and itra.Day='$DayNumber' ";
			}else{
				$DayCond = " and itra.Day=DAYOFWEEK('".$TargetDate."')-1 ";
			}
	  		
	  		$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	  		if( $AcademicYearID == "") {
	  			$AcademicYearID = Get_Current_Academic_Year_ID();
	  		}
	  		
	  		if($sys_custom['LessonAttendance_LaSalleCollege']){
	  			//if($_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])
	  			//{
	  				// if me is module admin, then can access all subject group lessons
	  				//$GetAllLessons = true;
	  			//}else{
	  				// check if me is form teacher, and get related subject groups
	  				$sql = "SELECT DISTINCT st.SubjectGroupID 
							FROM ASSESSMENT_SUBJECT as s 
							INNER JOIN SUBJECT_TERM as st ON (s.RecordID = st.SubjectID)
							INNER JOIN SUBJECT_TERM_CLASS as stc ON (st.SubjectGroupID = stc.SubjectGroupID)
							INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr ON (st.SubjectGroupID = stcyr.SubjectGroupID) 
							INNER JOIN YEAR as y ON y.YearID=stcyr.YearID 
							INNER JOIN ASSESSMENT_ACCESS_RIGHT as aar ON aar.YearID=stcyr.YearID 
							WHERE aar.UserID='$StaffID' ";
					$subject_id_ary = $this->returnVector($sql);
					//debug_pr($sql);
					
					// check if me if class teacher, and get related subject groups
					/*
					$sql = "SELECT DISTINCT st.SubjectGroupID 
							FROM ASSESSMENT_SUBJECT as s 
							INNER JOIN SUBJECT_TERM as st ON (s.RecordID = st.SubjectID)
							INNER JOIN SUBJECT_TERM_CLASS as stc ON (st.SubjectGroupID = stc.SubjectGroupID)
							INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr ON (st.SubjectGroupID = stcyr.SubjectGroupID) 
							INNER JOIN YEAR as y ON y.YearID=stcyr.YearID 
							INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID AND yc.AcademicYearID='$AcademicYearID' 
							INNER JOIN YEAR_CLASS_TEACHER as yct ON yct.YearClassID=yc.YearClassID AND yct.UserID='$StaffID' 
							WHERE yct.UserID='$StaffID' ";
					*/
					$sql = "SELECT DISTINCT st.SubjectGroupID 
							FROM ASSESSMENT_SUBJECT as s 
							INNER JOIN SUBJECT_TERM as st ON (s.RecordID = st.SubjectID) 
							inner join ACADEMIC_YEAR_TERM as ayt on (st.YearTermID = ayt.YearTermID) AND ayt.AcademicYearID='$AcademicYearID'
							INNER JOIN SUBJECT_TERM_CLASS as stc ON (st.SubjectGroupID = stc.SubjectGroupID)
							INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr ON (st.SubjectGroupID = stcyr.SubjectGroupID) 
							INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct on stc.SubjectGroupID = stct.SubjectGroupID
							WHERE stct.UserID='$StaffID' ";
					$subject_id_ary2 = $this->returnVector($sql);
					$subject_id_ary = array_values(array_unique(array_merge($subject_id_ary,$subject_id_ary2)));
	  			//}
	  		}
	  		
	  		$sql = "select
						  stc.SubjectGroupID,
						  stc.ClassTitleEN,
						  stc.ClassTitleB5,";
		if ($SubjectGroupListOnly == "") {
			$sql .= "	  itra.RoomAllocationID,
						  itt.TimeSlotName,
						  itt.StartTime,
						  itt.EndTime,";
		};
			$sql .= "	  stc.ClassTitleEN, 
						  stc.ClassTitleB5 
					 From 
						  ACADEMIC_YEAR_TERM as ayt
						  inner join
						  SUBJECT_TERM as st
						  on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$AcademicYearID."'
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on st.SubjectGroupID = stc.SubjectGroupID ";
		if(!$GetAllLessons) {
			if($sys_custom['LessonAttendance_LaSalleCollege'] && COUNT($subject_id_ary)>0){
				$cust_cond = " AND stc.SubjectGroupID IN (".implode(",",$subject_id_ary).") ";
			}else{	 
				 $sql .= "inner join
						  SUBJECT_TERM_CLASS_TEACHER as stct
						  on stc.SubjectGroupID = stct.SubjectGroupID and stct.UserID = '".$StaffID."' ";
			}
		}
				 $sql .= "inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$timetableID."' $DayCond 
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT as itt
						  on itra.TimeSlotID = itt.TimeSlotID 
						where 1 $cust_cond ";
	  		
	  		if ($SubjectGroupListOnly == "") {
				$sql .= "order by
							itt.StartTime,stc.ClassTitleEN";
			}
			else {
				$sql .= "group by 
							SubjectGroupID 
						order by 
							stc.ClassTitleEN ";
			}
	  		// debug_r($sql);
			return $this->returnArray($sql,9);
	  	}
	  	return array();
	}
	
	/*
	function Get_Subject_Group_Attendance_List($TargetDate,$DayType='',$SubjectGroupId='')
	{
		global $i_general_sysadmin;
		include_once("libtimetable.php");
		
		$ts_record = strtotime($TargetDate);
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$libtimetable = new Timetable();
		$timetableID = $libtimetable->Get_Current_Timetable($TargetDate);
		### build student table
		$card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;
		$subject_group_name_field = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
		if($SubjectGroupId != '' || (is_array($SubjectGroupId) && count($SubjectGroupId))) {
			$cond = " AND stc.SubjectGroupID IN (".implode(",",(array)$SubjectGroupId).")";
		}
		
		$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
  		if( $AcademicYearID == "") {
  			$AcademicYearID = Get_Current_Academic_Year_ID();
  		}
		
		$sql = "SELECT 
					stc.SubjectGroupID,
					$subject_group_name_field as SubjectGroupName,
					IF(c.ConfirmedUserID IS NULL,
			          '-',
			          IF(c.ConfirmedUserID <> -1, ".getNameFieldByLang("u.").", CONCAT('$i_general_sysadmin'))
		          	) as ConfirmedUser,
		            IF(c.DateModified IS NULL, '-', c.DateModified) as DateModified 
				FROM ACADEMIC_YEAR_TERM as ayt 
				inner join SUBJECT_TERM as st
					on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$AcademicYearID."'
				inner join SUBJECT_TERM_CLASS as stc on st.SubjectGroupID = stc.SubjectGroupID
				inner join SUBJECT_TERM_CLASS_TEACHER as stct
				  on stc.SubjectGroupID = stct.SubjectGroupID 
				inner join INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
				  on stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$timetableID."'
				inner join INTRANET_TIMETABLE_TIMESLOT as itt
				  on itra.TimeSlotID = itt.TimeSlotID 
				LEFT JOIN $card_student_daily_subject_group_confirm as c ON c.SubjectGroupID=stc.SubjectGroupID 
					AND c.DayNumber = '".$txt_day."' ".($DayType != ''? " AND c.DayType = '$DayType' ":"")."
				LEFT JOIN INTRANET_USER as u ON (c.ConfirmedUserID=u.UserID || c.ConfirmedUserID=-1) 
				WHERE ayt.AcademicYearID='".$AcademicYearID."' $cond 
				GROUP BY stc.SubjectGroupID 
				ORDER BY $subject_group_name_field ";
		$result = $this->returnArray($sql);
		
		return $result;
	}
	*/
	function Get_Subject_Group_Attendance_List($TargetDate,$DayType='',$SubjectGroupId='')
	{
		global $i_general_sysadmin;
		include_once("libtimetable.php");
		
		$ts_record = strtotime($TargetDate);
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$result = array();
		$libtimetable = new Timetable();
		$timetableID = $libtimetable->Get_Current_Timetable($TargetDate);
		if(trim($timetableID)!='' && $timetableID > 0){
			### build student table
			$card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;
			$subject_group_name_field = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
			if($SubjectGroupId != '' || (is_array($SubjectGroupId) && count($SubjectGroupId))) {
				$cond = " AND stc.SubjectGroupID IN (".implode(",",	IntegerSafe((array)$SubjectGroupId)).")";
			}
			
			$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
			if ($CycleDayInfo[0] == 'Cycle') {
				$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
				$DayCond = " and itra.Day='".$this->Get_Safe_Sql_Query($DayNumber)."' ";
			}else{
				$DayCond = " and itra.Day=DAYOFWEEK('".$this->Get_Safe_Sql_Query($TargetDate)."')-1 ";
			}
			
			$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	  		if( $AcademicYearID == "") {
	  			$AcademicYearID = Get_Current_Academic_Year_ID();
	  		}
			/*
			$sql = "SELECT 
						stc.SubjectGroupID,
						IF($subject_group_name_field IS NULL OR $subject_group_name_field='',
							IF(stc.ClassTitleB5 IS NULL OR stc.ClassTitleB5='',stc.ClassTitleEN,stc.ClassTitleB5),
							$subject_group_name_field) as SubjectGroupName,
						IF(c.ConfirmedUserID IS NULL,
				          '-',
				          IF(c.ConfirmedUserID <> -1, ".getNameFieldByLang("u.").", CONCAT('$i_general_sysadmin'))
			          	) as ConfirmedUser,
			            IF(c.DateModified IS NULL, '-', c.DateModified) as DateModified 
					FROM ACADEMIC_YEAR_TERM as ayt 
					inner join SUBJECT_TERM as st
						on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$AcademicYearID."'
					inner join SUBJECT_TERM_CLASS as stc on st.SubjectGroupID = stc.SubjectGroupID
					inner join INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$timetableID."' 
					inner join INTRANET_PERIOD_TIMETABLE_RELATION as iptr on iptr.TimeTableID=itra.TimetableID
					inner join INTRANET_CYCLE_GENERATION_PERIOD as icgp
					  on 
					    iptr.PeriodId = icgp.PeriodID
					    and '".$TargetDate."' between icgp.PeriodStart and icgp.PeriodEnd
					    and itra.Day = IF(PeriodType=1,'".$DayNumber."', DAYOFWEEK('".$TargetDate."')-1)
					inner join INTRANET_TIMETABLE_TIMESLOT as itt
					  on itra.TimeSlotID = itt.TimeSlotID 
					LEFT JOIN $card_student_daily_subject_group_confirm as c ON c.SubjectGroupID=stc.SubjectGroupID 
						AND c.DayNumber = '".$txt_day."' ".($DayType != ''? " AND c.DayType = '$DayType' ":"")."
					LEFT JOIN INTRANET_USER as u ON (c.ConfirmedUserID=u.UserID || c.ConfirmedUserID=-1) 
					WHERE ayt.AcademicYearID='".$AcademicYearID."' $cond 
					GROUP BY stc.SubjectGroupID 
					ORDER BY SubjectGroupName ";
			*/		
			$sql = "SELECT 
						stc.SubjectGroupID,
						IF($subject_group_name_field IS NULL OR $subject_group_name_field='',
							IF(stc.ClassTitleB5 IS NULL OR stc.ClassTitleB5='',stc.ClassTitleEN,stc.ClassTitleB5),
							$subject_group_name_field) as SubjectGroupName,
						IF(c.ConfirmedUserID IS NULL,
				          '-',
				          IF(c.ConfirmedUserID <> -1, ".getNameFieldByLang("u.").", CONCAT('$i_general_sysadmin'))
			          	) as ConfirmedUser,
			            IF(c.DateModified IS NULL, '-', c.DateModified) as DateModified 
					FROM ACADEMIC_YEAR_TERM as ayt 
					inner join SUBJECT_TERM as st
						on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$this->Get_Safe_Sql_Query($AcademicYearID)."'
					inner join SUBJECT_TERM_CLASS as stc on st.SubjectGroupID = stc.SubjectGroupID
					inner join INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$this->Get_Safe_Sql_Query($timetableID)."' $DayCond
					inner join INTRANET_TIMETABLE_TIMESLOT as itt
					  on itra.TimeSlotID = itt.TimeSlotID 
					LEFT JOIN $card_student_daily_subject_group_confirm as c ON c.SubjectGroupID=stc.SubjectGroupID 
						AND c.DayNumber = '".$this->Get_Safe_Sql_Query($txt_day)."' ".($DayType != ''? " AND c.DayType = '".$this->Get_Safe_Sql_Query($DayType)."' ":"")."
					LEFT JOIN INTRANET_USER as u ON (c.ConfirmedUserID=u.UserID || c.ConfirmedUserID=-1) 
					WHERE ayt.AcademicYearID='".$this->Get_Safe_Sql_Query($AcademicYearID)."' $cond 
					GROUP BY stc.SubjectGroupID 
					ORDER BY SubjectGroupName ";
			$result = $this->returnArray($sql);
		}
		return $result;
	}
	
	function Get_Subject_Group_Attendance_List_Without_Timetable($TargetDate,$DayType='',$SubjectGroupId='',$SubjectId='')
	{
		global $i_general_sysadmin;
		include_once("libtimetable.php");
		
		$ts_record = strtotime($TargetDate);
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$result = array();
		
		### build student table
		$card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;
		$subject_group_name_field = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
		if($SubjectId != ''){
			if(is_array($SubjectId)){
				$cond .= " AND st.SubjectID IN ('".implode("','",$this->Get_Safe_Sql_Query($SubjectId))."') ";
			}else{
				$cond .= " AND st.SubjectID='$SubjectId' ";
			}
		}
		if($SubjectGroupId != '' || (is_array($SubjectGroupId) && count($SubjectGroupId))) {
			$cond .= " AND stc.SubjectGroupID IN ('".implode("','",$this->Get_Safe_Sql_Query((array)$SubjectGroupId))."') ";
		}
		
		$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
  		if( $AcademicYearID == "") {
  			$AcademicYearID = Get_Current_Academic_Year_ID();
  		}
  		
  		$sql = "SELECT 
					stc.SubjectGroupID,
					IF($subject_group_name_field IS NULL OR $subject_group_name_field='',
						IF(stc.ClassTitleB5 IS NULL OR stc.ClassTitleB5='',stc.ClassTitleEN,stc.ClassTitleB5),
						$subject_group_name_field) as SubjectGroupName,
					IF(c.ConfirmedUserID IS NULL,
			          '-',
			          IF(c.ConfirmedUserID <> -1, ".getNameFieldByLang("u.").", CONCAT('$i_general_sysadmin'))
		          	) as ConfirmedUser,
		            IF(c.DateModified IS NULL, '-', c.DateModified) as DateModified,
		            st.SubjectID
				FROM ACADEMIC_YEAR_TERM as ayt 
				inner join SUBJECT_TERM as st
					on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$this->Get_Safe_Sql_Query($AcademicYearID)."' and st.YearTermID='".$this->Get_Safe_Sql_Query($YearTermID)."' 
				inner join SUBJECT_TERM_CLASS as stc on st.SubjectGroupID = stc.SubjectGroupID
				LEFT JOIN $card_student_daily_subject_group_confirm as c ON c.SubjectGroupID=stc.SubjectGroupID 
					AND c.DayNumber = '".$this->Get_Safe_Sql_Query($txt_day)."' ".($DayType != ''? " AND c.DayType = '".$this->Get_Safe_Sql_Query($DayType)."' ":"")."
				LEFT JOIN INTRANET_USER as u ON (c.ConfirmedUserID=u.UserID || c.ConfirmedUserID=-1) 
				WHERE ayt.AcademicYearID='".$this->Get_Safe_Sql_Query($AcademicYearID)."' $cond 
				GROUP BY stc.SubjectGroupID 
				ORDER BY st.SubjectID,SubjectGroupName ";
		$result = $this->returnArray($sql);
		
		return $result;
	}
			
	function Get_Class_Attend_Time_Setting($TargetDate,$StudentID="",$ClassID="") {
		$time_table_mode = $this->retrieveTimeTableMode();
		
		if ($ClassID=="") {
			list($ClassID,$CycleOrWeek,$CycleDayValue,$WeekDayValue) = $this->Get_General_Info_For_Attend_Setting($TargetDate,$StudentID);
		}
		else {
			list($CycleOrWeek,$CycleDayValue,$WeekDayValue) = $this->Get_General_Info_For_Attend_Setting($TargetDate,"",$ClassID);
		}
		
		# Get Week Day
		$conds = "";
		
		$AttendanceMode = $this->Get_Class_Attendance_Mode($ClassID);
		
		if ($AttendanceMode != 2) {
			# Get Class-specific time boundaries
			if (($time_table_mode == 0)||($time_table_mode == ''))
			{
				# Get Special Day Settings
				$sql = "SELECT 
									RecordID, 
									MorningTime, 
									LunchStart,
		              LunchEnd, 
		              LeaveSchoolTime,
		              NonSchoolDay
		            FROM 
		            	CARD_STUDENT_SPECIFIC_DATE_TIME
		            WHERE 
		            	RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."'
		              AND 
		              ClassID = '".$this->Get_Safe_Sql_Query($ClassID)."'
		              ";
		    //debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}

				// if there is no class special day setting, search for cycle/week day/normal setting
				$sql = "SELECT 
								a.RecordID, 
								a.MorningTime, 
								a.LunchStart,
				        a.LunchEnd, 
				        a.LeaveSchoolTime,
				        a.NonSchoolDay
							FROM 
								CARD_STUDENT_CLASS_PERIOD_TIME as a
								inner join 
								YEAR_CLASS as b 
								ON a.ClassID = b.YearClassID and b.YearClassID = '".$this->Get_Safe_Sql_Query($ClassID)."'
							WHERE 
				        (a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
				        OR ";
				if ($CycleOrWeek == 'Cycle') {
					$sql .= "
						      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
					        OR ";
				}
				$sql .= "
				        (a.DayType = 0 AND a.DayValue = 0)
							ORDER BY a.DayType DESC 
							LIMIT 0,1
							";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}
				
				// if none setting found on class, search for school setting
				# Get Special Day based on School
				$sql = "SELECT 
									RecordID, 
									MorningTime, 
									LunchStart,
				          LunchEnd, 
				          LeaveSchoolTime,
				          NonSchoolDay
				     		FROM 
				     			CARD_STUDENT_SPECIFIC_DATE_TIME
				     		WHERE 
				     			RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."'
				          AND 
				          ClassID = 0
							 ";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
			  if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}
					
				# Get School Settings (Cycle or WeekDay or normal)
				$sql = "SELECT 
									a.SlotID, 
									a.MorningTime, 
									a.LunchStart,
									a.LunchEnd, 
									a.LeaveSchoolTime,
									a.NonSchoolDay 
								FROM 
									CARD_STUDENT_PERIOD_TIME as a
								WHERE 
									(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
				          OR ";
				if ($CycleOrWeek == 'Cycle') {
					$sql .= "
						      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
					        OR ";
				}
				$sql .= "	(a.DayType = 0 AND a.DayValue = 0)
								ORDER BY a.DayType DESC 
								LIMIT 0,1
				       ";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					//echo "<b>Time Slot Whole School CYCLE, WEEKDAY , NORMAL SQL</b> [".$sql."]<br><br>";
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}
			} //close 	if (($time_table_mode == 0)||($time_table_mode == ''))
			else if($time_table_mode == 1)
			{
				# Get Class-specific time boundaries
				# Get Special Day Settings
				$sql = "SELECT
									b.RecordID, 
									a.MorningTime, 
									a.LunchStart,
									a.LunchEnd, 
									a.LeaveSchoolTime,
									a.NonSchoolDay
								FROM
									CARD_STUDENT_TIME_SESSION AS a 
									INNER JOIN
									CARD_STUDENT_TIME_SESSION_DATE AS b 
									ON (a.SessionID = b.SessionID)
								WHERE
									RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."' AND ClassID = '".$this->Get_Safe_Sql_Query($ClassID)."'";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}
				
				// check for class cycle/weekday/ normal setting
				$sql = "SELECT
										a.RecordID, 
										b.MorningTime, 
										b.LunchStart,
										b.LunchEnd, 
										b.LeaveSchoolTime,
										b.NonSchoolDay
									FROM
										CARD_STUDENT_TIME_SESSION_REGULAR AS a 
										LEFT OUTER JOIN
										CARD_STUDENT_TIME_SESSION AS b 
										ON (a.SessionID = b.SessionID)
									WHERE
										a.ClassID = '".$this->Get_Safe_Sql_Query($ClassID)."' AND
										(
										(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
				        		OR ";
				if ($CycleOrWeek == 'Cycle') {
					$sql .= "
							      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
						        OR ";
				}
				$sql .= "
										(a.DayType = 0 AND a.DayValue = 0)
										)
									ORDER BY a.DayType DESC 
									LIMIT 0,1";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}

				# Get Special Day based on School
				$sql = "SELECT
									a.RecordID, 
									b.MorningTime, 
									b.LunchStart,
									b.LunchEnd, 
									b.LeaveSchoolTime,
									b.NonSchoolDay
								FROM
									CARD_STUDENT_TIME_SESSION_DATE AS a 
									INNER JOIN
									CARD_STUDENT_TIME_SESSION AS b 
									ON (a.SessionID = b.SessionID)
								WHERE
									a.RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."' AND a.ClassID = 0";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}
					
				# Get School Settings
				$sql = "SELECT
									a.RecordID, 
									b.MorningTime, 
									b.LunchStart,
									b.LunchEnd, 
									b.LeaveSchoolTime,
									b.NonSchoolDay
								FROM
									CARD_STUDENT_TIME_SESSION_REGULAR AS a 
									INNER JOIN
									CARD_STUDENT_TIME_SESSION as b 
									ON (a.SessionID = b.SessionID AND a.ClassID = 0)
								WHERE
									(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
				        	OR ";
				if ($CycleOrWeek == 'Cycle') {
					$sql .= "
						      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
					        OR ";
				}
				$sql .= "
									(a.DayType = 0 AND a.DayValue = 0)
								ORDER BY
									a.DayType DESC 
								LIMIT 0,1";
				//debug_r($sql);
				$temp = $this->returnArray($sql,6);
				if (sizeof($temp) > 0) {
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
					return ($ts_nonSchoolDay)? "NonSchoolDay":array("MorningTime" => $ts_morningTime, "LunchStart" => $ts_lunchStart, "LunchEnd" => $ts_lunchEnd, "LeaveSchoolTime" => $ts_leaveSchool);
				}
			}
		}
		else {
			return "NonSchoolDay";
		}
	}
	
	function Get_Group_Attend_Time_Setting($TargetDate,$StudentID="",$GetAttendanceSettingList=false,$GroupID="",$GetSchoolSetting=false) {
		$NoAttendance = 0;
		$FollowSchoolSetting = 0;
		$HaveAttendSetting = array();
		$AttendanceSetting = array();
		
		$time_table_mode = $this->retrieveTimeTableMode();
		$AcademicYearInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
		
		if ($GroupID == "") {
			list($ClassID,$CycleOrWeek,$CycleDayValue,$WeekDayValue) = $this->Get_General_Info_For_Attend_Setting($TargetDate,$StudentID);
		}
		else {
			list($CycleOrWeek,$CycleDayValue,$WeekDayValue) = $this->Get_General_Info_For_Attend_Setting($TargetDate,"",$GroupID);
		}
		
		if ($GroupID == "") {
			$sql = "select 
								c.GroupID, 
								c.Title,
								c.Description, 
								e.mode
							from 
								INTRANET_USER a
								inner join 
								INTRANET_USERGROUP b
								on a.userid = b.userid and a.userid = '".$this->Get_Safe_Sql_Query($StudentID)."'
								inner join 
								INTRANET_GROUP c 
								on b.groupid = c.groupid and c.RecordType = '3' and c.AcademicYearID = '".$this->Get_Safe_Sql_Query($AcademicYearInfo[0])."' 
								left outer join 
								YEAR_CLASS d 
								on c.groupid = d.groupid 
								left outer join 
								CARD_STUDENT_ATTENDANCE_GROUP e 
								on e.groupid = c.groupid 
							where
								d.groupid is null
							order by c.Title
							";
		}
		else {
			$sql = "select 
								a.GroupID,
								a.Title,
								a.Description,
								b.mode 
							from 
								INTRANET_GROUP a 
								LEFT JOIN 
								CARD_STUDENT_ATTENDANCE_GROUP b 
								on a.GroupID = b.GroupID 
							where 
								a.GroupID = '".$this->Get_Safe_Sql_Query($GroupID)."'";
		}
		//debug_r($sql);
		$aryGroupIDList = $this->returnArray($sql,4);
		
		if(sizeof($aryGroupIDList) != 0)
		{
			for($i=0; $i< sizeof($aryGroupIDList); $i++)
			{
				list($groupId, $title, $desc,$mode) = $aryGroupIDList[$i];
	
				#MODE = NULL ==> SCHOOL TIME TABLE
				#MDOE = 0    ==> SCHOOL TIME TABLE (SAME AS NULL)
				#MODE = 1    ==> GROUP SPECIFIC TIME TABLE
				#MODE = 2    ==> NO NEED TO TAKE ATTENDANCE
				if ($mode == 2)
				{
					$NoAttendance++;
				}
				else {
					$HaveAttendSetting[] = $aryGroupIDList[$i];
				}
				
				$DisplayGroupSetting[$groupId] = array("","","","","","",$title,$desc,$mode);
			}
		}
		
		if ($time_table_mode == 1) {
			for ($i=0; $i < sizeof($HaveAttendSetting); $i++) {
				list($GroupID,$Title,$Desc,$Mode) = $HaveAttendSetting[$i];
				
				if ($Mode == 1) { // group specific time table
					$sql = "SELECT 
										RecordID, 
										MorningTime, 
										LunchStart,
										LunchEnd, 
										LeaveSchoolTime,
										NonSchoolDay
									FROM
										CARD_STUDENT_TIME_SESSION AS a 
										INNER JOIN
										CARD_STUDENT_TIME_SESSION_DATE_GROUP AS b 
										ON (a.SessionID = b.SessionID)
									WHERE 
										RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."'
										AND GroupId = '".$this->Get_Safe_Sql_Query($GroupID)."' 
									";
					$Temp = $this->returnArray($sql,6);
					
					if (sizeof($Temp) == 0) {
						$sql = "SELECT 
											a.RecordID, 
											b.MorningTime, 
											b.LunchStart,
											b.LunchEnd, 
											b.LeaveSchoolTime, 
											b.NonSchoolDay 
										FROM 
											CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS a 
											LEFT OUTER JOIN
											CARD_STUDENT_TIME_SESSION as b 
											ON (a.SessionID = b.SessionID)					
										WHERE 
											a.GroupId = '".$this->Get_Safe_Sql_Query($GroupID)."' 
											AND
											(
												(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
					        			OR ";
						if ($CycleOrWeek == 'Cycle') {
							$sql .= "
									      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
								        OR ";
						}
						$sql .= "
												(a.DayType = 0 AND a.DayValue = 0)
											)
										ORDER BY 
												a.DayType DESC 
										";
						$Temp = $this->returnArray($sql,6);
					}
					
					if (sizeof($Temp) == 0) {
						if ($GetSchoolSetting) {
							# Get Special Day based on School
							$sql = "SELECT
												a.RecordID, 
												b.MorningTime, 
												b.LunchStart,
												b.LunchEnd, 
												b.LeaveSchoolTime,
												b.NonSchoolDay
											FROM
												CARD_STUDENT_TIME_SESSION_DATE AS a 
												INNER JOIN
												CARD_STUDENT_TIME_SESSION AS b 
												ON (a.SessionID = b.SessionID)
											WHERE
												a.RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."' AND a.ClassID = 0";
							//debug_r($sql);
							$Temp = $this->returnArray($sql,6);
							if (sizeof($Temp) > 0) {
								array_push($Temp[0],$HaveAttendSetting[$i][1]);
								array_push($Temp[0],$HaveAttendSetting[$i][2]);
								array_push($Temp[0],$HaveAttendSetting[$i][3]);
								$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
								$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
							}
							else {
								# Get School Settings
								$sql = "SELECT
													a.RecordID, 
													b.MorningTime, 
													b.LunchStart,
													b.LunchEnd, 
													b.LeaveSchoolTime,
													b.NonSchoolDay
												FROM
													CARD_STUDENT_TIME_SESSION_REGULAR AS a 
													INNER JOIN
													CARD_STUDENT_TIME_SESSION as b 
													ON (a.SessionID = b.SessionID AND a.ClassID = 0)
												WHERE
													(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
								        	OR ";
								if ($CycleOrWeek == 'Cycle') {
									$sql .= "
										      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
									        OR ";
								}
								$sql .= "
													(a.DayType = 0 AND a.DayValue = 0)
												ORDER BY
													a.DayType DESC 
												LIMIT 0,1";
								//debug_r($sql);
								$Temp = $this->returnArray($sql,6);
								if (sizeof($Temp) > 0) {
									array_push($Temp[0],$HaveAttendSetting[$i][1]);
									array_push($Temp[0],$HaveAttendSetting[$i][2]);
									array_push($Temp[0],$HaveAttendSetting[$i][3]);
									$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
									$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
								}
							}
						}
						else {
							$FollowSchoolSetting++;
							$DisplayGroupSetting[$HaveAttendSetting[$i][0]][8] = 0;
						}
					}
					else {
						array_push($Temp[0],$HaveAttendSetting[$i][1]);
						array_push($Temp[0],$HaveAttendSetting[$i][2]);
						array_push($Temp[0],$HaveAttendSetting[$i][3]);
						$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
						$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
					}
				}
				else { // follow school time table
					if ($GetSchoolSetting) {
								# Get Special Day based on School
						$sql = "SELECT
											a.RecordID, 
											b.MorningTime, 
											b.LunchStart,
											b.LunchEnd, 
											b.LeaveSchoolTime,
											b.NonSchoolDay
										FROM
											CARD_STUDENT_TIME_SESSION_DATE AS a 
											INNER JOIN
											CARD_STUDENT_TIME_SESSION AS b 
											ON (a.SessionID = b.SessionID)
										WHERE
											a.RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."' AND a.ClassID = 0";
						//debug_r($sql);
						$Temp = $this->returnArray($sql,6);
						if (sizeof($Temp) > 0) {
							array_push($Temp[0],$HaveAttendSetting[$i][1]);
							array_push($Temp[0],$HaveAttendSetting[$i][2]);
							array_push($Temp[0],$HaveAttendSetting[$i][3]);
							$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
							$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
						}
						else {
							# Get School Settings
							$sql = "SELECT
												a.RecordID, 
												b.MorningTime, 
												b.LunchStart,
												b.LunchEnd, 
												b.LeaveSchoolTime,
												b.NonSchoolDay
											FROM
												CARD_STUDENT_TIME_SESSION_REGULAR AS a 
												INNER JOIN
												CARD_STUDENT_TIME_SESSION as b 
												ON (a.SessionID = b.SessionID AND a.ClassID = 0)
											WHERE
												(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
							        	OR ";
							if ($CycleOrWeek == 'Cycle') {
								$sql .= "
									      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
								        OR ";
							}
							$sql .= "
												(a.DayType = 0 AND a.DayValue = 0)
											ORDER BY
												a.DayType DESC 
											LIMIT 0,1";
							//debug_r($sql);
							$Temp = $this->returnArray($sql,6);
							if (sizeof($Temp) > 0) {
								array_push($Temp[0],$HaveAttendSetting[$i][1]);
								array_push($Temp[0],$HaveAttendSetting[$i][2]);
								array_push($Temp[0],$HaveAttendSetting[$i][3]);
								$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
								$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
							}
						}
					}
				}
			}
		}
		else {
			for ($i=0; $i < sizeof($HaveAttendSetting); $i++) {
				list($GroupID,$Title,$Desc,$Mode) = $HaveAttendSetting[$i];
				
				if ($Mode == 1) {
					$sql =	"SELECT 
										RecordID, 
										MorningTime, 
										LunchStart,
										LunchEnd, 
										LeaveSchoolTime,
										NonSchoolDay
									 FROM 
										CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP
									 WHERE 
										RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."'
										AND GroupId = '".$this->Get_Safe_Sql_Query($GroupID)."' 
									";
					//debug_r($sql);
					$Temp = $this->returnArray($sql,6);
					
					if (sizeof($Temp) == 0) {
						$sql = "SELECT 
										a.RecordID, 
										a.MorningTime, 
										a.LunchStart,
										a.LunchEnd, 
										a.LeaveSchoolTime,
										a.NonSchoolDay 
									 FROM 
										CARD_STUDENT_GROUP_PERIOD_TIME as a
									 WHERE 
											a.GroupId = '".$this->Get_Safe_Sql_Query($GroupID)."' 
											AND 
											(
												(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
					        			OR ";
						if ($CycleOrWeek == 'Cycle') {
							$sql .= "
									      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
								        OR ";
						}
						$sql .= "
												(a.DayType = 0 AND a.DayValue = 0)
											)
									 ORDER BY a.DayType DESC
									";
						//debug_r($sql);
						$Temp = $this->returnArray($sql,6);
					}
					
					if (sizeof($Temp) == 0) {
						if ($GetSchoolSetting) {
							// if none setting found on class, search for school setting
							# Get Special Day based on School
							$sql = "SELECT 
												RecordID, 
												MorningTime, 
												LunchStart,
							          LunchEnd, 
							          LeaveSchoolTime,
							          NonSchoolDay
							     		FROM 
							     			CARD_STUDENT_SPECIFIC_DATE_TIME
							     		WHERE 
							     			RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."'
							          AND 
							          ClassID = 0
										 ";
							//debug_r($sql);
							$Temp = $this->returnArray($sql,6);
						  if (sizeof($Temp) > 0) {
								array_push($Temp[0],$HaveAttendSetting[$i][1]);
								array_push($Temp[0],$HaveAttendSetting[$i][2]);
								array_push($Temp[0],$HaveAttendSetting[$i][3]);
								$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
								$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
							}
							else {
								# Get School Settings (Cycle or WeekDay or normal)
								$sql = "SELECT 
													a.SlotID, 
													a.MorningTime, 
													a.LunchStart,
													a.LunchEnd, 
													a.LeaveSchoolTime,
													a.NonSchoolDay 
												FROM 
													CARD_STUDENT_PERIOD_TIME as a
												WHERE 
													(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
								          OR ";
								if ($CycleOrWeek == 'Cycle') {
									$sql .= "
										      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
									        OR ";
								}
								$sql .= "	(a.DayType = 0 AND a.DayValue = 0)
												ORDER BY a.DayType DESC 
												LIMIT 0,1
								       ";
								//debug_r($sql);
								$Temp = $this->returnArray($sql,6);
								if (sizeof($Temp) > 0) {
									array_push($Temp[0],$HaveAttendSetting[$i][1]);
									array_push($Temp[0],$HaveAttendSetting[$i][2]);
									array_push($Temp[0],$HaveAttendSetting[$i][3]);
									$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
									$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
								}
							}
						}
						else {
							$FollowSchoolSetting++;
							$DisplayGroupSetting[$HaveAttendSetting[$i][0]][8] = 0;
						}
					}
					else {
						array_push($Temp[0],$HaveAttendSetting[$i][1]);
						array_push($Temp[0],$HaveAttendSetting[$i][2]);
						array_push($Temp[0],$HaveAttendSetting[$i][3]);
						$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
						$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
					}
				}
				else {
					if ($GetSchoolSetting) {
						// if none setting found on class, search for school setting
						# Get Special Day based on School
						$sql = "SELECT 
											RecordID, 
											MorningTime, 
											LunchStart,
						          LunchEnd, 
						          LeaveSchoolTime,
						          NonSchoolDay
						     		FROM 
						     			CARD_STUDENT_SPECIFIC_DATE_TIME
						     		WHERE 
						     			RecordDate = '".$this->Get_Safe_Sql_Query($TargetDate)."'
						          AND 
						          ClassID = 0
									 ";
						//debug_r($sql);
						$Temp = $this->returnArray($sql,6);
					  if (sizeof($Temp) > 0) {
							array_push($Temp[0],$HaveAttendSetting[$i][1]);
							array_push($Temp[0],$HaveAttendSetting[$i][2]);
							array_push($Temp[0],$HaveAttendSetting[$i][3]);
							$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
							$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
						}
						else {
							# Get School Settings (Cycle or WeekDay or normal)
							$sql = "SELECT 
												a.SlotID, 
												a.MorningTime, 
												a.LunchStart,
												a.LunchEnd, 
												a.LeaveSchoolTime,
												a.NonSchoolDay 
											FROM 
												CARD_STUDENT_PERIOD_TIME as a
											WHERE 
												(a.DayType = 1 AND a.DayValue = '".$this->Get_Safe_Sql_Query($WeekDayValue)."')
							          OR ";
							if ($CycleOrWeek == 'Cycle') {
								$sql .= "
									      (a.DayType = 2 AND a.DayValue = '".$this->Get_Safe_Sql_Query($CycleDayValue)."')
								        OR ";
							}
							$sql .= "	(a.DayType = 0 AND a.DayValue = 0)
											ORDER BY a.DayType DESC 
											LIMIT 0,1
							       ";
							//debug_r($sql);
							$Temp = $this->returnArray($sql,6);
							if (sizeof($Temp) > 0) {
								array_push($Temp[0],$HaveAttendSetting[$i][1]);
								array_push($Temp[0],$HaveAttendSetting[$i][2]);
								array_push($Temp[0],$HaveAttendSetting[$i][3]);
								$AttendanceSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
								$DisplayGroupSetting[$HaveAttendSetting[$i][0]] = $Temp[0];
							}
						}
					}
				}
			}
		}
		if ($GetAttendanceSettingList) {
			return $DisplayGroupSetting;
		}
		else {
			if (sizeof($AttendanceSetting) > 0) {
				return $this->Merge_Attendance_Setting($AttendanceSetting); // merge time table, may be non school day
			}
			else {
				if ($NoAttendance > 0) {
					return "NonSchoolDay"; // non school day
				}
				else 
					return false; // follow school time setting
			}
		}
	}
	
	function Get_General_Info_For_Attend_Setting($TargetDate,$StudentID="",$ClassID="") {
		if ($ClassID == "") {
			// get student's class id and cycle date or week day value on target date
			$sql = 'select
							  yc.YearClassID,
							  IF(PeriodType=1,\'Cycle\',\'Week\') as CycleOrWeek,
							  icd.textshort as CycleDayValue, 
							  (DAYOFWEEK(\''.$TargetDate.'\')-1) as WeekDayValue 
							from
							  YEAR_CLASS_USER as ycu
							  inner join
							  YEAR_CLASS as yc
							  on ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\' and ycu.UserID = \''.$StudentID.'\'
							  left join
							  INTRANET_CYCLE_GENERATION_PERIOD as icgp
							  on \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd 
							  LEFT JOIN 
							  INTRANET_CYCLE_DAYS as icd 
							  on icd.RecordDate = \''.$TargetDate.'\' 
							  ';
			//debug_r($sql);
			$Temp = $this->returnArray($sql,4);
		}
		else {
			// get student's class id and cycle date or week day value on target date
			$sql = 'select
							  IF(PeriodType=1,\'Cycle\',\'Week\') as CycleOrWeek,
							  icd.textshort as CycleDayValue, 
							  (DAYOFWEEK(\''.$TargetDate.'\')-1) as WeekDayValue 
							from 
							  INTRANET_CYCLE_GENERATION_PERIOD as icgp 
							  LEFT JOIN 
							  INTRANET_CYCLE_DAYS as icd 
							  on icd.RecordDate = \''.$TargetDate.'\' 
							where 
								\''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd 
							  ';
			//debug_r($sql);
			$Temp = $this->returnArray($sql,2);
			
			if (sizeof($Temp) == 0) {
				$Temp[0] = array("Week","",date('w',strtotime($TargetDate)));
			}
		}
		return $Temp[0];
	}
	
	function Merge_Attendance_Setting($AttendanceSetting) {
		$j=0;
		//debug_r($AttendanceSetting);
		foreach ($AttendanceSetting as $Key => $Val) {			
			list($RecordID,$MorningTime,$LunchStart,$LunchEnd,$LeaveSchoolTime,$NonSchoolDay,$Title,$Desc,$Mode) = $Val;
			$MorningTime = $this->timeToSec($MorningTime);
			$LunchStart = $this->timeToSec($LunchStart);
			$LunchEnd = $this->timeToSec($LunchEnd);
			$LeaveSchoolTime = $this->timeToSec($LeaveSchoolTime);
			
			if (!$NonSchoolDay) {
				if ($j==0) {
					$Final['MorningTime'] = $MorningTime;
					$Final['LunchStart'] = $LunchStart;
					$Final['LunchEnd'] = $LunchEnd;
					$Final['LeaveSchoolTime'] = $LeaveSchoolTime;
					$Final['NonSchoolDay'] = $NonSchoolDay;
					$j++;
				}
				else {
					// Morning Time compare 
					if ($Final['MorningTime'] > $MorningTime) 
						$Final['MorningTime'] = $MorningTime;
					
					// Leave school time compare 
					if ($Final['LeaveSchoolTime'] < $LeaveSchoolTime)
						$Final['LeaveSchoolTime'] = $LeaveSchoolTime; 
						
					// Lunch start time compare 
					if ($Final['LunchStart'] < $LunchStart && $Final['LunchEnd'] > $LunchStart) {
						$Final['LunchStart'] = $LunchStart;
					}
					else if ($Final['LunchStart'] < $LunchStart && $Final['LunchEnd'] < $LunchStart) {
						$Final['LunchStart'] = $LunchStart;
						$Final['LunchEnd'] = $LunchEnd;
					}
					/*else if ($Final['LunchStart'] > $LunchStart && $Final['LunchStart'] > $LunchEnd) {
						$Final['LunchStart'] = $LunchStart;
						$Final['LunchEnd'] = $LunchEnd;
					}*/
					
					// lunch
					if ($Final['LunchEnd'] > $LunchEnd && $Final['LunchStart'] < $LunchEnd) {
						$Final['LunchEnd'] = $LunchEnd;
					}
				}
			}
		}
		
		if ($j==0) 
			return "NonSchoolDay";
		else {
			$Final['MorningTime'] = $this->secToTime($Final['MorningTime']);
			$Final['LunchStart'] = $this->secToTime($Final['LunchStart']);
			$Final['LunchEnd'] = $this->secToTime($Final['LunchEnd']);
			$Final['LeaveSchoolTime'] = $this->secToTime($Final['LeaveSchoolTime']);
			return $Final;
		}
	}
	
	function timeToSec($_time)
	{
		if($_time == '' || strpos($_time,':')===false) return 0;
		
		$timeAry = explode(":",$_time);
	
		$returnValue = $timeAry[0] * 60 *60 + $timeAry[1] *60;
		return $returnValue;
	}

	function secToTime($_sec)
	{
		$hours = floor ($_sec / 3600);
		$mins = $_sec % 3600;
		$mins = floor ($mins / 60);
		$hours = ($hours <= 9)? "0".$hours:$hours;
		$mins = ($mins <= 9)? "0".$mins:$mins;
		$returnValue = $hours.":".$mins;
		return $returnValue;	
	}
	
	function Get_Student_Attend_Time_Setting($TargetDate,$StudentID) {
		if ($this->Check_Is_Student_Within_Entry_Period($TargetDate,$StudentID)) {
			$GroupSetting = $this->Get_Group_Attend_Time_Setting($TargetDate,$StudentID);
			
			$ClassSetting = $this->Get_Class_Attend_Time_Setting($TargetDate,$StudentID);
			
			if ($GroupSetting === "NonSchoolDay") {
				return "NonSchoolDay";
			}
			else  {
				if (is_array($GroupSetting)) 
					return $GroupSetting;
				else 
					return $ClassSetting;
			}
		}
		else {
			return "NonSchoolDay";
		}
	}
	
	function Get_Attendance_Cycle_Day_Info($TargetDate="") {
		$TargetDate = ($TargetDate == "")? date("Y-m-d"):$TargetDate;
		$sql = 'select
						  IF(PeriodType=1,\'Cycle\',\'Week\') as CycleOrWeek, 
						  CycleType 
						from
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
						where
						  \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd';
		$Result = $this->returnArray($sql);
		
		// check if using special time table on that date
		$sql = "select 
					t2.RepeatType  
				from INTRANET_CYCLE_GENERATION_PERIOD as t1 
				inner join INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS as t2 ON t2.TimezoneID=t1.PeriodID 
				inner join INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE as t3 ON t3.SpecialTimetableSettingsID=t2.SpecialTimetableSettingsID AND t3.SpecialDate='$TargetDate' 
				where ('$TargetDate' BETWEEN t1.PeriodStart AND t1.PeriodEnd)";
		$records = $this->returnVector($sql);
		
		if (sizeof($Result) == 0 || $Result[0][0] == 'Week' || (sizeof($records)>0) && $records[0]=='weekday') {
			return array('Week',date('w',strtotime($TargetDate)));
		}
		else {
			$sql = 'select 
								TextShort 
							From 
								INTRANET_CYCLE_DAYS 
							where 
								RecordDate = \''.$TargetDate.'\'';
			$DayValue = $this->returnVector($sql);
			return array('Cycle',$DayValue[0],$Result[0][1]);
		}
	}
	
	// find any special time table on that day
	function Get_Lesson_Special_Time_Table($TargetDate)
	{
		$sql = "select 
					t2.TimetableID 
				from INTRANET_CYCLE_GENERATION_PERIOD as t1 
				inner join INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS as t2 ON t2.TimezoneID=t1.PeriodID 
				inner join INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE as t3 ON t3.SpecialTimetableSettingsID=t2.SpecialTimetableSettingsID AND t3.SpecialDate='$TargetDate' 
				where ('$TargetDate' BETWEEN t1.PeriodStart AND t1.PeriodEnd)";
		$records = $this->returnVector($sql);
		return $records;
	}
	
	function Get_Class_Lesson_Attendance_Overview($TargetDate) {
		// build necessary attendance table for attendance taking
		$this->Build_Subject_Group_Attend_Overview();
		$this->Build_Subject_Group_Student_Attendance();
		
		$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
		
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
		
		$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
  	if ($CycleDayInfo[0] == 'Cycle') {
  		$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
  	}
		
		// Get All subject group lesson related to each class on target date
		$sql = 'select
						  yc.YearClassID,
						  stc.SubjectGroupID,
						  stc.ClassTitleEN as SubjectGroupTitleEN,
						  stc.ClassTitleB5 as SubjectGroupTitleB5,
						  itra.RoomAllocationID,
						  itra.TimeSlotID,
						  IFNULL(sgato1.AttendOverviewID,-1) as AttendOverviewID, 
						  s.CH_DES as SubjectNameB5, 
							s.EN_DES as SubjectNameEN
						from
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
						  inner join
						  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
						  on
						    \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
						    and icgp.PeriodID = iptr.PeriodID
						  inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on
						    iptr.TimeTableID = itra.TimeTableID
						    and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1)
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on itra.SubjectGroupID = stc.SubjectGroupID
						  inner join 
						  SUBJECT_TERM as st
						  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\' 
						  inner join 
						  ASSESSMENT_SUBJECT as s 
						  on st.SubjectID = s.RecordID 
						  inner join
						  SUBJECT_TERM_CLASS_USER as stcu
						  on stc.SubjectGroupID = stcu.SubjectGroupID
						  inner join
						  YEAR_CLASS_USER as ycu
						  on stcu.UserID = ycu.UserID
						  inner join
						  YEAR_CLASS as yc
						  on
						    ycu.YearClassID = yc.YearClassID
						    and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\'
						  left join
						  SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as sgato1
						  on
						    stc.SubjectGroupID = sgato1.SubjectGroupID
						    and itra.RoomAllocationID = sgato1.RoomAllocationID
						    and sgato1.LessonDate = \''.$TargetDate.'\'
						Group By
						  yc.YearClassID, stc.SubjectGroupID, itra.TimeSlotID';
		//debug_r($sql);
		//debug_pr($sql);
		return $this->returnArray($sql); 
	}
	
	function Get_Class_Lesson_Attendance_Overview2($TargetDate, $Filter=0, $SubjectGroupID = -1, $groupByStudent=0, $studentIdAry=array()) {
		global $sys_custom;
		// build necessary attendance table for attendance taking
		$this->Build_Subject_Group_Attend_Overview();
		$this->Build_Subject_Group_Student_Attendance();
		
		$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
		
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
		
		$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
		if ($CycleDayInfo[0] == 'Cycle') {
			$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
		}
		$SpecialTimeTableIdRecord = $this->Get_Lesson_Special_Time_Table($TargetDate);
		$is_special_timetable = count($SpecialTimeTableIdRecord)>0;
		$cond = " 1 ";
		if($is_special_timetable){
			$time_table_id = $SpecialTimeTableIdRecord[0];
			$cond .= " and ist.TimetableID='$time_table_id' ";
		}
		if($Filter == 2){
			$cond .= " and sgato1.AttendOverviewID IS NOT NULL ";
		}
		else if($Filter == 1){
			$cond .= " and sgato1.AttendOverviewID IS NULL ";
		}
		
		if(is_array($studentIdAry) && count($studentIdAry)>0){
			$cond .= " and stcu.UserID IN (".implode(",",$studentIdAry).") ";
		}
		
		if($SubjectGroupID != -1){
			$cond1 = " and stc.SubjectGroupID = '".$SubjectGroupID."' ";
		}
		
		// Get All subject group lesson related to each class on target date
		$sql = 'select
					  yc.YearClassID,
					  stc.SubjectGroupID,
					  yc.ClassTitleB5,
					  yc.ClassTitleEN,
					  stc.ClassTitleB5 as SubjectGroupTitleB5,
					  stc.ClassTitleEN as SubjectGroupTitleEN,
					  itt.TimeSlotName,
						il.NameChi as LocationNameB5,
						il.NameEng  as LocationNameEN, 
					  IFNULL(sgato1.AttendOverviewID,-1) as AttendOverviewID, 
					  sgato1.LastModifiedBy,
					  sgato1.LastModifiedDate,
					  itra.RoomAllocationID,
					  itra.TimeSlotID,
					  s.CH_DES as SubjectNameB5, 
					  s.EN_DES as SubjectNameEN,
					  itt.StartTime,
					  itt.EndTime,
					  stcu.UserID,
					  u.EnglishName as LastModifiedUserEnglishName,
					  u.ChineseName as LastModifiedUserChineseName,
					  itt.SessionType,
					  itt.DisplayOrder,
					  st.SubjectID  
					from ';
		if($is_special_timetable){
			$sql .= 'INTRANET_SCHOOL_TIMETABLE as ist 
					  inner join
					  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on 
					    ist.TimeTableID = itra.TimeTableID 
						and itra.Day = '.($CycleDayInfo[0] == 'Cycle'? '\''.$DayNumber.'\'': 'DAYOFWEEK(\''.$TargetDate.'\')-1 ' ).' ';
		}else{
		  $sql .= ' INTRANET_CYCLE_GENERATION_PERIOD as icgp 
					  inner join
					  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
					  on
					    \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
					    and icgp.PeriodID = iptr.PeriodID
					  inner join
					  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on 
					    iptr.TimeTableID = itra.TimeTableID 
						and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1) ';
		}	
			$sql .= 'inner join INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
					 inner join
					  SUBJECT_TERM_CLASS as stc
					  on itra.SubjectGroupID = stc.SubjectGroupID '.$cond1.'
					  inner join 
					  SUBJECT_TERM as st
					  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\' 
					  inner join 
					  ASSESSMENT_SUBJECT as s 
					  on st.SubjectID = s.RecordID 
					  inner join
					  SUBJECT_TERM_CLASS_USER as stcu
					  on stc.SubjectGroupID = stcu.SubjectGroupID
					  inner join
					  YEAR_CLASS_USER as ycu
					  on stcu.UserID = ycu.UserID
					  inner join
					  YEAR_CLASS as yc
					  on
					    ycu.YearClassID = yc.YearClassID
					    and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\' 
					  inner join YEAR as y ON y.YearID=yc.YearID 
					  left join
					  SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as sgato1
					  on
					    stc.SubjectGroupID = sgato1.SubjectGroupID
					    and itra.RoomAllocationID = sgato1.RoomAllocationID
					    and sgato1.LessonDate = \''.$TargetDate.'\'
					left join
						INVENTORY_LOCATION as il
						on itra.LocationID = il.LocationID 
					left join INTRANET_USER as u on u.UserID=sgato1.LastModifiedBy 
					WHERE '.$cond.'			  		
					Group By
					  yc.YearClassID, stc.SubjectGroupID, itra.TimeSlotID ';
		if($groupByStudent){
			$sql .= ',stcu.UserID ';
		}
		if($sys_custom['LessonAttendance_SortBySubjectGroupClass']){
			$sql .=	'ORDER BY itt.DisplayOrder,s.DisplayOrder,y.Sequence,yc.Sequence';
		}else{
			$sql .=	'ORDER BY y.Sequence,yc.Sequence,itt.DisplayOrder,s.DisplayOrder';
		}

		$records = $this->returnArray($sql);
		
		if($groupByStudent){
			$map = array();
			$record_size = count($records);
			for($i=0;$i<$record_size;$i++){
				if(!isset($map[$records[$i]['UserID']])){
					$map[$records[$i]['UserID']] = array();
				}
				$map[$records[$i]['UserID']][] = $records[$i];
			}
			return $map;
		}
			
		// debug_r($sql);
		//debug_pr($sql);
		//debug_pr($this->returnArray($sql));
		return $records;
	}
	
	function Get_Time_Slot_List($TargetDate) {
		$sql = 'select
						  itt.TimeSlotID,
						  itt.TimeSlotName,
						  itt.StartTime,
						  itt.EndTime
						from
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
						  inner join
						  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
						  on
						    \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
						    and icgp.PeriodID = iptr.PeriodID
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr
						  on iptr.TimeTableID = ittr.TimeTableID
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT as itt
						  on ittr.TimeSlotID = itt.TimeSlotID
						order by
						  itt.StartTime';
		
		return $this->returnArray($sql);
	}
	
	function Get_Time_Slot_List2($TargetDate) {
		$sql = 'select
						  itt.TimeSlotID,
						  itt.TimeSlotName,
						  itt.StartTime,
						  itt.EndTime,
						  icgp.PeriodID
						from
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
						  inner join
						  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
						  on
						    \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
						    and icgp.PeriodID = iptr.PeriodID
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr
						  on iptr.TimeTableID = ittr.TimeTableID
						  inner join
						  INTRANET_TIMETABLE_TIMESLOT as itt
						  on ittr.TimeSlotID = itt.TimeSlotID
						order by
						  itt.StartTime';
		
		return $this->returnArray($sql);
	}
	
	function Convert_Cycle_Day_Value_To_Day_Number($DayValue,$CycleType="") {
		include_once('libcycleperiods.php');
  	
		$libcycle = new libcycleperiods();
		
		if ($CycleType == 1) {
			foreach ($libcycle->array_alphabet as $key => $val) {
				if ($val == $DayValue) {
					$DayNumber = $key + 1;
					break;
				}
			}
		}
		else if ($CycleType == 2) {
			foreach ($libcycle->array_roman as $key => $val) {
  			if ($val == $DayValue) {
  				$DayNumber = $key + 1;
  				break;
  			}
  		}
		}
		else if ($CycleType == 0) {
			$DayNumber = $DayValue;
		}
		
		return $DayNumber;
	}
	//Henry Modified (added the $studentID parameter)
	function Get_Class_Lesson_Daily_Late_Absent($TargetDate,$AttendStatus, $StudentID="") 
	{
		// build necessary attendance table for attendance taking
		$this->Build_Subject_Group_Attend_Overview();
		$this->Build_Subject_Group_Student_Attendance();
		
		if (sizeof($AttendStatus) > 0) {
			$NameField = getNameFieldWithClassNumberByLang('u.');
			$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
	//		$NameField1 = getNameFieldWithClassNumberByLang('u1.');
			$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			
			list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
			
			$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
		  	if ($CycleDayInfo[0] == 'Cycle') {
		  		$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
		  	}
		
	//	  	$cond ='';
	//	  	if($StudentID != '')
	//	  		$cond = " AND a1.StudentID = ".$StudentID." ";
		  	/*
		  	$sql = 'select distinct
	                a1.StudentID
	              from
	                SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as o1
	                inner join
	                SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' as a1
	                on
	                o1.LessonDate = \''.$TargetDate.'\'
	                and o1.AttendOverviewID = a1.AttendOverviewID
	                and a1.AttendStatus in ('.implode(',',$AttendStatus).')';
	      if($StudentID == '')
	      $StudentList = $this->returnVector($sql);
	      if($StudentID != '')
	      	$StudentList = array($StudentID);
	      */
	      //debug_pr($sql);
	      $StudentList = (array)$StudentID;
	      
	      if ($StudentID != "" && sizeof($StudentList) > 0) {
	      		$status_cond = " and AttendStatus IN ('".implode("','",$AttendStatus)."') ";
			  	$StudentCondition = '\''.implode('\',\'',$StudentList).'\'';
		 		$class_user_cond = " and stcu.UserID in (".$StudentCondition.") ";
		 		$student_attend_cond = " and sgsa1.StudentID in (".$StudentCondition.")";
		 		$where_cond = " where stcu.UserID IN (".$StudentCondition.") ";
	      }else{
	      	$where_cond = " where AttendStatus IN ('".implode("','",$AttendStatus)."') ";
	      }	  	
			$sql = 'select 
					  itra.TimeSlotID,
					  stcu.UserID,
					  '.$NameField.' as StudentName,
					  stc.ClassTitleEN,
					  stc.ClassTitleB5,
					  IFNULL(sgao1.AttendOverviewID,-1) as AttendOverviewID,
					  IFNULL(sgsa1.AttendStatus,-1) as AttendStatus, 
					  s.CH_DES as SubjectNameB5, 
					  s.EN_DES as SubjectNameEN,
					  icgp.PeriodID,
					  u.EnglishName,
					  u.ChineseName,
					  '.$ClassNameField.' as ClassName,
					  ycu.ClassNumber 
					from 
					  INTRANET_CYCLE_GENERATION_PERIOD as icgp
						inner join
						INTRANET_PERIOD_TIMETABLE_RELATION as iptr
						on
						  \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
							and icgp.PeriodID = iptr.PeriodID
					  inner join 
					  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on
					    itra.TimeTableID = iptr.TimeTableID
					    and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1) 
					  inner join INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
					  inner join
					  SUBJECT_TERM_CLASS as stc
					  on stc.SubjectGroupID = itra.SubjectGroupID
					  inner join 
					  SUBJECT_TERM as st
					  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\' 
					  inner join 
					  ASSESSMENT_SUBJECT as s 
					  on st.SubjectID = s.RecordID 
					  INNER JOIN 
					  SUBJECT_TERM_CLASS_USER as stcu
					  on 
					  	stcu.SubjectGroupID = stc.SubjectGroupID '.$class_user_cond.' 
					  inner join YEAR_CLASS_USER as ycu on stcu.UserID = ycu.UserID
					  inner join YEAR_CLASS as yc
						  on ycu.YearClassID = yc.YearClassID
							and yc.AcademicYearID = \''.$AcademicYearID.'\' 
					  inner join YEAR as y ON y.YearID=yc.YearID 
					  left join 
					  SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as sgao1
					  on
					    sgao1.SubjectGroupID = itra.SubjectGroupID
					    and sgao1.LessonDate = \''.$TargetDate.'\'
					    and sgao1.RoomAllocationID = itra.RoomAllocationID
					  left join
					  SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' as sgsa1
					  on
					    sgsa1.AttendOverviewID = sgao1.AttendOverviewID '.$student_attend_cond.' and sgsa1.StudentID=ycu.UserID
					  LEFT JOIN 
					  INTRANET_USER as u 
					  on u.UserID = stcu.UserID 
					'.$where_cond.'
					order by 
						y.Sequence,yc.Sequence,ycu.ClassNumber+0';
							
			//debug_r($sql);
			return $this->returnArray($sql);
		}else{
			return array();
		}
	}
	
	function Get_Lesson_Summary_Report_Data($FromDate,$ToDate,$ClassID, $AttendStatus = 0, $StudentID = "") {
		// build necessary attendance table for attendance taking
		$this->Build_Subject_Group_Attend_Overview();
		$this->Build_Subject_Group_Student_Attendance();
		
		$NameField = getNameFieldWithClassNumberByLang('u.');
//		$cond = ' and (';
//		if($StudnetID[0] != ''){
//			foreach($StudnetID as $aStudent){
//				$cond .= ' ycu.UserID = \''.$aStudent.'\' OR';
//			}
//			$cond .= ' FALSE) ';
//		}
		$cond = '';
		if( $StudentID[0] !='' && count($StudentID)>0){ 
			$cond .= ' AND ycu.UserID IN (\''.implode('\',\'', $StudentID) . '\') ';
		}
		else
			$cond = ' AND TRUE ';
		
		//Henry: check the attendance status
//		$condAttendStatus = '';
//		if($AttendStatus == 1)
//			$condAttendStatus = '';
		
		// get involved academic year ids
		$sql = 'select distinct
						  AcademicYearID
						from
						  ACADEMIC_YEAR_TERM
						where
						  (\''.$FromDate.'\' between TermStart and TermEnd)
						  or
						  (\''.$ToDate.'\' between TermStart and TermEnd)
						  ';
		$AcademicYearIDs = $this->returnVector($sql);
		
		$Result = array();
		for ($i=0; $i< sizeof($AcademicYearIDs); $i++) {
			$sql = 'select
						  ycu.UserID,
						  '.$NameField.' as StudentName,
						  sgsa.AttendStatus,
						  count(sgsa.AttendStatus) as TotalCount
						From
						  YEAR_CLASS as yc
						  inner join
						  YEAR_CLASS_USER as ycu
						  on yc.YearClassID = \''.$ClassID.'\' and yc.YearClassID = ycu.YearClassID 
						  inner join YEAR as y ON y.YearID=yc.YearID 
						  inner join
						  INTRANET_USER as u
						  on ycu.UserID = u.UserID '.$cond.'
						  inner join
						  SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearIDs[$i].' as sgsa
						  on sgsa.StudentID = ycu.UserID 
						  inner join
						  SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearIDs[$i].' as sgao
						  on
						    sgao.AttendOverviewID = sgsa.AttendOverviewID
						    and sgao.LessonDate between \''.$FromDate.'\' and \''.$ToDate.'\' 
						where yc.AcademicYearID=\''.$AcademicYearIDs[$i].'\' 
						Group by
						  ycu.UserID, sgsa.AttendStatus
						order by
						  y.Sequence,yc.Sequence,ycu.ClassNumber+0';
			//debug_r($sql);
			$Result = array_merge($Result,$this->returnArray($sql));
		}
		
		return $Result;
	}
	
	// Build the lesson attendance overview table in year base
	function Build_Subject_Group_Attend_Overview()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_ATTEND_OVERVIEW_'.Get_Current_Academic_Year_ID().' (
						  AttendOverviewID int(8) NOT NULL auto_increment,
						  SubjectGroupID int(8) NOT NULL,
						  LessonDate date NOT NULL,
						  RoomAllocationID int(8) NOT NULL,
						  StartTime time default NULL,
						  EndTime time default NULL,
						  PlanID varchar(20) default NULL, 
						  PlanName varchar(255),
						  StudentPositionAMF TEXT, 
						  PlanLayoutAMF TEXT,
						  CreateBy int(8) default NULL,
						  CreateDate datetime default NULL,
						  LastModifiedBy int(8) default NULL,
						  LastModifiedDate datetime default NULL,
						  PRIMARY KEY  (AttendOverviewID),
						  UNIQUE KEY SubjectGroupID (SubjectGroupID,LessonDate,RoomAllocationID)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8';
		return $this->db_db_query($sql);
	}
	
	// Build the lesson attendance student data table in year base
	function Build_Subject_Group_Student_Attendance()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_STUDENT_ATTENDANCE_'.Get_Current_Academic_Year_ID().' (
					  AttendOverviewID int(8) NOT NULL,
					  StudentID int(8) NOT NULL,
					  AttendStatus int(2) default NULL,
					  InTime time default NULL,
					  OutTime time default NULL,
					  LateFor int(8) default 0,
					  Reason text default NULL,
					  Remarks text default NULL,
					  CreateBy int(8) default NULL,
					  CreateDate datetime default NULL,
					  LastModifiedBy int(8) default NULL,
					  LastModifiedDate datetime default NULL,
					  LeaveTypeID int(11) default NULL,
					  PRIMARY KEY  (AttendOverviewID,StudentID),
					  KEY StudentID (StudentID,AttendOverviewID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
		return $this->db_db_query($sql);
	}


	/////////////	FUNCTION FOR TIME SESSION SETTINGS IN eATTENDANCE: START  //////////////	
	/**
	 * Generate selection area table for student attendance group mode
	 * @return	String	Html string for the table generated in group mode
	 */
	function getSelectionTableGroupMode($ParAttSelectBoxIndex="1") {
		global $button_change,
		$i_StudentAttendance_Diplay_Mode,
		$i_StudentAttendance_GroupMode_UseSchoolTimetable,
		$i_StudentAttendance_GroupMode_UseGroupTimetable,
		$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance;

		$bundleChangeArray[] = array("-1", "$button_change...");
		$bundleChangeArray[] = array("0", $i_StudentAttendance_GroupMode_UseSchoolTimetable);
		$bundleChangeArray[] = array("1", $i_StudentAttendance_GroupMode_UseGroupTimetable);
		$bundleChangeArray[] = array("2", $i_StudentAttendance_GroupMode_NoNeedToTakeAttendance);

		$attendanceSelectionBox = $this->getGroupAttendanceSelectionBox($ParAttSelectBoxIndex);
		$bundleChangeBox = $this->getBundleChangeBox($bundleChangeArray, "group");
		$returnString = $this->generateSelectionTableFormat($i_StudentAttendance_Diplay_Mode, $attendanceSelectionBox, $bundleChangeBox);
		return $returnString;
	}

	/**
	 * Generate selection area table for student attendance class mode
	 * @return	String	Html string for the table generated in class mode
	 */
	function getSelectionTableClassMode($ParAttSelectBoxIndex="-1") {
	 	global $Lang,
	 	$button_change,
		$i_StudentAttendance_ClassMode_UseSchoolTimetable,
		$i_StudentAttendance_ClassMode_UseClassTimetable,
		$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance;

		$bundleChangeArray[] = array("-1", "$button_change...");
		$bundleChangeArray[] = array("0", $i_StudentAttendance_ClassMode_UseSchoolTimetable);
		$bundleChangeArray[] = array("1", $i_StudentAttendance_ClassMode_UseClassTimetable);
		$bundleChangeArray[] = array("2", $i_StudentAttendance_ClassMode_NoNeedToTakeAttendance);

		$attendanceSelectionBox = $this->getClassAttendanceSelectionBox($ParAttSelectBoxIndex);
		$bundleChangeBox = $this->getBundleChangeBox($bundleChangeArray, "class");
		$returnString = $this->generateSelectionTableFormat($Lang['eAttendance']['ClassAttendanceMode'], $attendanceSelectionBox, $bundleChangeBox);
		return $returnString;
	}
	
	/**
	 * Generate the format for the selection table area
	 * @return	String	Html for the selection table area
	 */
	function generateSelectionTableFormat($ParLabel, $ParAttendanceSelectionBox, $ParBundleChangeBox) {
		$returnString = '
  		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tbody>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $ParLabel .'</td>
					<td class="tabletext" width="70%">'.
						$ParAttendanceSelectionBox .
					'</td>
					<td>' .
						$ParBundleChangeBox .
					'</td>
				</tr>
			</tbody>
		</table>';
		return $returnString;
	}

	/**
	 * Generate the filter selection box for group attendance
	 * @param	String	$ParSelectedIndex	the selected index for the selection box generated
	 * @return	String	Html for the filter generated
	 */	
	function getGroupAttendanceSelectionBox($ParSelectedIndex="1") {
		global $i_StudentAttendance_ShowALlGroup_Options,$i_StudentAttendance_HiddenGroup_Options;
		define("SELECTED", "selected");
		switch ($ParSelectedIndex) {
			case "0":
				$type_0_selected = SELECTED;
				break;
			case "1":
				$type_1_selected = SELECTED;
				break;
			default:
				break;
		}
		$returnString = '<select onchange="submitForm()" name="showAll">
			<option value="1" ' . $type_1_selected . '>' .$i_StudentAttendance_ShowALlGroup_Options . '</option>
			<option value="0" ' . $type_0_selected . '>' .$i_StudentAttendance_HiddenGroup_Options . '</option>
		</select>';
		return $returnString;
	}
	
	/**
	 * Generate the filter selection box for class attendance
	 * @param	String	$ParSelectedIndex	the selected index for the selection box generated
	 * @return	String	Html for the filter generated
	 */	
	function getClassAttendanceSelectionBox($ParSelectedIndex="-1") {
		global $Lang,
		$i_StudentAttendance_ShowAllClass_Options,
		$i_StudentAttendance_ClassMode_UseSchoolTimetable,
		$i_StudentAttendance_ClassMode_UseClassTimetable,
		$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance;
		define("SELECTED", "selected");
		switch ($ParSelectedIndex) {
			case "-1":
				$type_all_selected = SELECTED;
				break;
			case "0":
				$type_0_selected = SELECTED;
				break;
			case "1":
				$type_1_selected = SELECTED;
				break;
			case "2":
				$type_2_selected = SELECTED;
				break;
			default:
				break;
		}

		$returnString = '<select onchange="submitForm()" name="showAll">
			<option value="-1" ' . $type_all_selected . '>' .$Lang['eAttendance']['ShowAllClasses'] . '</option>
			<option value="0" ' . $type_0_selected . '>' .$i_StudentAttendance_ClassMode_UseSchoolTimetable . '</option>
			<option value="1" ' . $type_1_selected . '>' .$i_StudentAttendance_ClassMode_UseClassTimetable . '</option>
			<option value="2" ' . $type_2_selected . '>' .$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance . '</option>
		</select>';
		return $returnString;
	}
	
	/**
	 * Generate the selection box for bundle change options
	 * @param	Array	$ParBundleChangeArray	array storing the options value
	 * @param	String	$ParType	the type for the selection box - "class"/"group"
	 * @return	String	Html for the generated seletion box
	 */
	function getBundleChangeBox($ParBundleChangeArray, $ParType) {
		global $Lang;
		$returnString = getSelectByArray($ParBundleChangeArray, "style=\"float:right;\" name='mode' onChange='updateStatus(\"" . $ParType . "_update.php\",\"" . $Lang['eAttendance']['warn_PleaseChooseAtLeastOneItem'] . "\")'", -1, 0, 1, "", 2);
		return $returnString;
	}
	/////////////	FUNCTION FOR TIME SESSION SETTINGS IN eATTENDANCE: END  //////////////
	
	function Get_AM_Expected_Field($TablePrefix,$OutingTablePrefix,$AbsentPresetPrefix) {
		$this->retrieveSettings();
		$am_expected_field = "IF(".$TablePrefix."AMStatus IS NOT NULL,
								            ".$TablePrefix."AMStatus,
								            CASE 
								              WHEN ".$AbsentPresetPrefix."RecordID IS NOT NULL THEN CONCAT('".CARD_STATUS_ABSENT."')
								              WHEN ".$OutingTablePrefix."OutingID IS NOT NULL THEN CONCAT('".CARD_STATUS_OUTING."')
											  WHEN ".$TablePrefix."InSchoolTime IS NOT NULL THEN CONCAT('".CARD_STATUS_PRESENT."')
								              ELSE CONCAT('".$this->DefaultAttendanceStatus."')
								            END
								          ) ";
		return $am_expected_field;
	}
	
	function Get_PM_Expected_Field($TablePrefix,$OutingTablePrefix,$AbsentPresetPrefix) {
		global $sys_custom, $LUNCH_ALLOW_LIST_TABLE;
		
		$this->retrieveSettings();
		if ($this->attendance_mode==1) {
			$pm_expected_field = "CASE
															WHEN ".$TablePrefix."PMStatus IS NOT NULL THEN ".$TablePrefix."PMStatus 
															WHEN ".$AbsentPresetPrefix."RecordID IS NOT NULL THEN '".CARD_STATUS_ABSENT."'
								              WHEN ".$OutingTablePrefix."OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
		                          ELSE '".$this->DefaultAttendanceStatus."'
			                      END";
		}
		else {
	    $pm_expected_field = "CASE
															WHEN ".$TablePrefix."PMStatus IS NOT NULL THEN ".$TablePrefix."PMStatus 
															WHEN ".$AbsentPresetPrefix."RecordID IS NOT NULL THEN '".CARD_STATUS_ABSENT."' 
								              WHEN ".$OutingTablePrefix."OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."' 
															";
			if($this->PMStatusNotFollowAMStatus == 1) {
				// *** $LUNCH_ALLOW_LIST_TABLE is global variable to control places that should apply the cust $sys_custom['StudentAttendance']['PMStatusFollowLunchSetting'] ***
				if($sys_custom['StudentAttendance']['PMStatusFollowLunchSetting'] && $this->attendance_mode == 2 && $LUNCH_ALLOW_LIST_TABLE != ''){
					// whole day with lunch mode, and if PM status is customized to follow lunch setting, check 
					// if lunch setting not all students allow to go out for lunch and student not in allow go out for lunch list, then PM status follow AM status 
					$pm_expected_field .= " ELSE IF('".$this->AllAllowGoOut."' != '1' AND $LUNCH_ALLOW_LIST_TABLE.StudentID IS NULL,
												CASE WHEN (".$TablePrefix."AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND ".$TablePrefix."LeaveStatus IN('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND ".$TablePrefix."PMStatus IS NULL AND ".$TablePrefix."LeaveSchoolTime IS NOT NULL) THEN '".CARD_STATUS_ABSENT."'
																WHEN (".$TablePrefix."AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND ".$TablePrefix."LunchOutTime IS NULL) OR (".$TablePrefix."LunchBackTime IS NOT NULL) THEN '".CARD_STATUS_PRESENT."'
																WHEN (".$TablePrefix."AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND ".$TablePrefix."LunchOutTime IS NOT NULL) AND (".$TablePrefix."LunchBackTime IS NULL) THEN '".CARD_STATUS_ABSENT."' 
																WHEN (".$TablePrefix."AMStatus = '".CARD_STATUS_OUTING."') THEN '".CARD_STATUS_OUTING."' 
																WHEN (".$TablePrefix."AMStatus IS NULL OR ".$TablePrefix."AMStatus = '".CARD_STATUS_ABSENT."') THEN '".CARD_STATUS_ABSENT."' 
												END,
												'".$this->DefaultAttendanceStatus."') 
											END ";
				}else{
					$pm_expected_field .= "ELSE '".$this->DefaultAttendanceStatus."' 
														END";
				}
			}
			else {
				$pm_expected_field .= "	WHEN (".$TablePrefix."AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND ".$TablePrefix."LeaveStatus IN('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND ".$TablePrefix."PMStatus IS NULL AND ".$TablePrefix."LeaveSchoolTime IS NOT NULL) THEN '".CARD_STATUS_ABSENT."'
																WHEN (".$TablePrefix."AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND ".$TablePrefix."LunchOutTime IS NULL) OR (".$TablePrefix."LunchBackTime IS NOT NULL) THEN '".CARD_STATUS_PRESENT."'
																WHEN (".$TablePrefix."AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND ".$TablePrefix."LunchOutTime IS NOT NULL) AND (".$TablePrefix."LunchBackTime IS NULL) THEN '".CARD_STATUS_ABSENT."' 
																WHEN (".$TablePrefix."AMStatus = '".CARD_STATUS_OUTING."') THEN '".CARD_STATUS_OUTING."' 
																WHEN (".$TablePrefix."AMStatus IS NULL OR ".$TablePrefix."AMStatus = '".CARD_STATUS_ABSENT."') THEN '".CARD_STATUS_ABSENT."' 
															END";
			}
		}
		
		//debug_r($pm_expected_field);
		return $pm_expected_field;
	}
	
	function Get_Class_Attendance_Mode($ClassID) {
		$sql = "SELECT 
							b.Mode
            FROM 
            	YEAR_CLASS as yc 
              LEFT OUTER JOIN 
              CARD_STUDENT_CLASS_SPECIFIC_MODE as b 
              ON yc.YearClassID = b.ClassID
            WHERE 
            	yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
            	and 
            	yc.YearClassID='".$ClassID."' 
            ORDER BY yc.sequence";
    //debug_r($sql); 
		$temp2 =$this->returnVector($sql,3);
		
		return $temp2[0];
	}
	
	function Get_Group_Attendance_Mode($GroupID) {
		$sql = "SELECT
							b.Mode
						FROM
							INTRANET_GROUP a
							inner join 
							CARD_STUDENT_ATTENDANCE_GROUP b
							on 
								a.groupid = b.groupid AND
								a.GroupID='$GroupID'
						";
		//debug_r($sql);
		$temp2 = $this->returnVector($sql,3);
		
		return $temp2[0];
	}
	
	function Get_Entry_Leave_Student_List($ClassID) {
		$NameField = getNameFieldWithClassNumberByLang("u.");
		
		$sql = 'select
							u.UserID,
						  '.$NameField.' as StudentName 
						from
						  YEAR_CLASS yc
						  inner join
						  YEAR_CLASS_USER ycu
						  on yc.YearClassID = \''.$ClassID.'\'
						    and
						    yc.YearClassID = ycu.YearClassID
						  inner join
						  INTRANET_USER u
						  on
						    ycu.UserID = u.UserID 
						    and 
						    u.RecordType = \'2\' 
						    and 
						    u.RecordStatus = \'1\' 
						order by
						  ycu.ClassNumber+0';
							
		return $this->returnArray($sql);
	}
	
	function Get_Entry_Leave_List($StudentID) {
		$sql = 'select 
							RecordID,
							PeriodStart,
							PeriodEnd
						from 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD 
						where 
							UserID = \''.$StudentID.'\' 
						order by 
							PeriodStart asc';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Entry_Leave_Default_Detail($StudentID) {
		$sql = 'select 
							MAX(PeriodEnd) as PeriodEnd 
						From 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD
						where 
							UserID = \''.$StudentID.'\'
							and 
							PeriodEnd != \'2099-12-31\' 
						group by 
							UserID';
		
		$Result = $this->returnVector($sql);
		
		if ($Result[0] != "") {
			$TimeStamp = strtotime($Result[0]);
			$DefaultPeriodStartYear = Date('Y',$TimeStamp);
			$DefaultPeriodStartMonth = Date('n',$TimeStamp);
			$DefaultPeriodStartDay = Date('j',$TimeStamp);
			
			$DefaultPeriodStart = date('Y-m-d',mktime(0,0,0,$DefaultPeriodStartMonth,($DefaultPeriodStartDay+1), $DefaultPeriodStartYear));
		}
		return array(array("",($Result[0]==""? " ":$DefaultPeriodStart)," "));
	}
	
	function Get_Entry_Leave_Detail($RecordID) {
		$sql= 'select 
						RecordID,
						PeriodStart,
						PeriodEnd 
					From 
						CARD_STUDENT_ENTRY_LEAVE_PERIOD 
					where 
						RecordID = \''.$RecordID.'\'';
		return $this->returnArray($sql,3);
	}
	
	function Get_Entry_Leave_Export_Records()
	{
		$sql = "SELECT 
					u.UserLogin,
					IF(p.PeriodStart='1970-01-01','',p.PeriodStart) as PeriodStart,
					IF(p.PeriodEnd='2099-12-31' OR p.PeriodEnd='2037-12-31','',p.PeriodEnd) as PeriodEnd 
				FROM INTRANET_USER as u 
				INNER JOIN CARD_STUDENT_ENTRY_LEAVE_PERIOD as p ON p.UserID=u.UserID 
				WHERE u.RecordStatus=1 AND u.RecordType=2 
				ORDER BY u.UserLogin, p.PeriodStart";
		return $this->returnArray($sql);
	}
	
	function Check_Entry_Leave_Period($StudentID,$RecordID,$PeriodStart,$PeriodEnd) {
		$PeriodStart = trim($PeriodStart) == ""? '1970-01-01':$PeriodStart;
		$PeriodEnd = trim($PeriodEnd) == ""? '2099-12-31':$PeriodEnd;
		$sql = 'select 
							count(1) 
						from 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD 
						where 
							UserID = \''.$StudentID.'\' 
							and ';
		if ($RecordID != "") {
			$sql .= 'RecordID != \''.$RecordID.'\' 
							and ';
		}
		$sql .= '	(
							(\''.$PeriodStart.'\' between PeriodStart and PeriodEnd) 
							OR 
							(\''.$PeriodEnd.'\' between PeriodStart and PeriodEnd)
							OR (\''.$PeriodStart.'\' < PeriodStart AND \''.$PeriodEnd.'\' > PeriodEnd)  
							)';
		$Result = $this->returnVector($sql);
		//debug_r($sql);
		return !($Result[0] > 0);
	}
	
	function Save_Entry_Leave_Period($StudentID,$RecordID,$PeriodStart,$PeriodEnd) {
		$PeriodStart = ($PeriodStart == "")? '1970-01-01':$PeriodStart;
		$PeriodEnd = ($PeriodEnd == "")? '2099-12-31':$PeriodEnd;
		if ($RecordID == "") {
			$sql = 'insert into CARD_STUDENT_ENTRY_LEAVE_PERIOD 
								(
								UserID,
								PeriodStart,
								PeriodEnd,
								DateInput,
								InputBy,
								DateModify,
								ModifyBy
								)
							value 
								(
								\''.$StudentID.'\',
								\''.$PeriodStart.'\',
								\''.$PeriodEnd.'\',
								NOW(),
								\''.$_SESSION['UserID'].'\',
								NOW(),
								\''.$_SESSION['UserID'].'\'
								)';
		}
		else {
			$sql = 'update CARD_STUDENT_ENTRY_LEAVE_PERIOD set 
								PeriodStart = \''.$PeriodStart.'\',
								PeriodEnd = \''.$PeriodEnd.'\',
								DateModify = NOW(),
								ModifyBy = \''.$_SESSION['UserID'].'\' 
							where 
								RecordID = \''.$RecordID.'\'';
		}
		
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Delete_Entry_Period($RecordID) {
		
		$sql = 'delete from CARD_STUDENT_ENTRY_LEAVE_PERIOD ';
		if($RecordID != ''){
			$sql.= ' where RecordID = \''.$RecordID.'\'';
		}
		return $this->db_db_query($sql);
	}
	
	function Check_Is_Student_Within_Entry_Period($TargetDate,$StudentID) {
		if ($this->EnableEntryLeavePeriod) {
			$sql = 'select 
								count(1) 
							from 
								CARD_STUDENT_ENTRY_LEAVE_PERIOD 
							where 
								UserID = \''.$StudentID.'\' 
								and 
								\''.$TargetDate.'\' between PeriodStart and CONCAT(PeriodEnd,\' 23:59:59\') ';
			$Temp = $this->returnVector($sql);
			
			return ($Temp[0] > 0);
		}
		else {
			return true;
		}
	}
	
	function Import_Entry_Period_Info($Data) {
		global $Lang;
		$Error = array();
		$Result = array();
		if(!is_array($Data))
		{
			return array();
		}
		
		$sql = 'DROP TABLE TEMP_IMPORT_STUDENT_ENTRY_PERIOD';
		$this->db_db_query($sql);
		$sql = 'CREATE TABLE TEMP_IMPORT_STUDENT_ENTRY_PERIOD (
							UserID varchar(20) default NULL,
							PeriodStart DATE default NULL,
							PeriodEnd DATE default NULL
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					 ';
		$this->db_db_query($sql);
		
		$this->Start_Trans();
		$NameField = getNameFieldByLang("a.");
		$values_array = array();
		for($i=0;$i<sizeof($Data);$i++)
		{
			list($UserLogin,$PeriodStart,$PeriodEnd) = $Data[$i];
			$UserLogin = trim($UserLogin);
			$PeriodStart = trim($PeriodStart);
			$PeriodEnd = trim($PeriodEnd);
			// must at least enter start or end date
			if ($PeriodStart == "" && $PeriodEnd =="") {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['EntryLeavePeriodNullWarning']);
				continue;
			}
			
			// check date format
			$PeriodStart = trim($PeriodStart) == ""? '1970-01-01':$PeriodStart;
			$PeriodEnd = trim($PeriodEnd) == ""? '2037-12-31':$PeriodEnd;		
			if (!checkDateIsValid($PeriodStart) || !checkDateIsValid($PeriodEnd)) {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['InvalidDateFormat']);
				continue;
			}
			
			// check if end date < start date
			if (strtotime($PeriodStart) > strtotime($PeriodEnd)) {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['EntryLeavePeriodEndDateLesserThenStartDateWarning']);
				continue;
			}
			
			$sql = "SELECT
						a.UserID
					FROM
						INTRANET_USER as a
					WHERE
						a.RecordType = '2'
				  	AND a.RecordStatus = '1'
						AND a.UserLogin = '".$this->Get_Safe_Sql_Query($UserLogin)."' ";
			$UserInfo = $this->returnArray($sql);
			if(sizeof($UserInfo)>0)
			{
				$TargetUserID = $UserInfo[0]['UserID'];
				if ($this->Check_Entry_Leave_Period($TargetUserID,"",$PeriodStart,$PeriodEnd)) {
					$sql = 'select 
										count(1) 
									from 
										 TEMP_IMPORT_STUDENT_ENTRY_PERIOD 
									where 
										UserLogin = \''.$this->Get_Safe_Sql_Query($UserLogin).'\' 
										and 
										(
										\''.$PeriodStart.'\' between PeriodStart and PeriodEnd 
										OR 
										\''.$PeriodEnd.'\' between PeriodStart and PeriodEnd 
										)';
					$Temp = $this->returnVector($sql);
					
					if ($Temp[0] > 0) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['EntryLeavePeriodOverlapWarning']);
					}
					else {
						$sql = 'insert into TEMP_IMPORT_STUDENT_ENTRY_PERIOD (
											UserID, 
											PeriodStart,
											PeriodEnd
											)
										value (
											\''.$this->Get_Safe_Sql_Query($TargetUserID).'\',
											\''.$PeriodStart.'\',
											\''.$PeriodEnd.'\'
											)';
						//debug_r($sql);
						$Result['InsertTempRecord:'.$UserLogin.$PeriodStart.$PeriodEnd] = $this->db_db_query($sql);
					}
				}
				else {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['EntryLeavePeriodOverlapWarning']);
				}
			}else
			{
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning']);
				continue;
				//break;
			}
		}
		
		//debug_r($Result);
		if (in_array(false,$Result)) {
			$this->RollBack_Trans();
		}
		else {
			$this->Commit_Trans();
		}
		
		return $Error;
	}
	
	function Finalize_Entry_Period_Info() {
		$sql = 'CREATE IF NOT EXISTS TABLE TEMP_IMPORT_STUDENT_ENTRY_PERIOD (
							UserID varchar(20) default NULL,
							PeriodStart DATE default NULL,
							PeriodEnd DATE default NULL
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					 ';
		$this->db_db_query($sql);
		
		$sql = 'select 
							UserID, 
							PeriodStart,
							PeriodEnd 
						from 
							TEMP_IMPORT_STUDENT_ENTRY_PERIOD';
		$TempResult = $this->returnArray($sql);
		//debug_r($TempResult);
		for ($i=0; $i< sizeof($TempResult); $i++) {
			list($StudentID,$PeriodStart,$PeriodEnd) = $TempResult[$i];
			$Result['UpdateCardID:'.$StudentID.$PeriodStart.$PeriodEnd] = $this->Save_Entry_Leave_Period($StudentID,"",$PeriodStart,$PeriodEnd);
		}
		
		$sql = 'DROP TABLE TEMP_IMPORT_STUDENT_ENTRY_PERIOD';
		$this->db_db_query($sql);
		
		return sizeof($TempResult);
	}
	
		function retrievePastDateByDateCount($count, $date="")
		{			
			if($date=="") $date = date('Y-m-d');
			
			$sql = "SELECT LEFT(EventDate,10) FROM INTRANET_EVENT WHERE RecordType=3 Or RecordType=4";	# Public Holiday : RecordType=3; School Holiday : RecordType=4
			$holiday = $this->returnVector($sql);

			$cmonth = date($date);
			$recordYear = substr($cmonth, 0, 4);
			$recordMonth = substr($cmonth, 5, 2);
			$recordDay = substr($cmonth, 8, 2);
			$NoOfDays = date("t", mktime(0,0,0,$recordMonth,1,$recordYear));
			$CurrentDay = date("Y-m-d");
		
			$k = $recordDay + 0;
			$recordMonth = $recordMonth + 0;
			$m = 0;
			while($m<($count)) {

				$temp_k = ($k<10) ? "0".$k : $k;
				
				$temp_month = ($recordMonth<10) ? "0".$recordMonth : $recordMonth;
				$tempDate = $recordYear."-".$temp_month."-".$temp_k;
				
				if(!(in_array($tempDate, $holiday))) {	# don't count the holiday
					if ((date("w", mktime(0,0,0,$recordMonth,$k,$recordYear)) != 0) && (date("w", mktime(0,0,0,$recordMonth,$k,$recordYear)) != 6)) {	# don't count Sat & Sun (0: Sun; 6: Sat)
						$m++;
						//echo '....'.$m.'/'.$recordYear.'-'.$temp_month.'-'.$temp_k.'('.date("w", mktime(0,0,0,$recordMonth,$k,$recordYear)).')<br>';
					}
				}
				if($k==1) {
					if($recordMonth-1==0) {
						$recordMonth = 12;
						$recordYear--;
					} else {
						$recordMonth--;
					}
					$NoOfDays = date("t", mktime(0,0,0,$recordMonth,1,$recordYear));
					$k = $NoOfDays;
				} else {
					$k--;
				}
			}
			/*
			$k--;
			if($k==0) {
				if($recordMonth==1) {
					$recordMonth = 12;
					$recordYear--;
				} else {
					$recordMonth--;
				}
				$k = date("t", mktime(0,0,0,$recordMonth,1,$recordYear));
			}
			*/
			$k = ($k<10) ? "0".$k : $k;
			$recordMonth = $recordMonth+0;
			$recordMonth = ($recordMonth<10) ? "0".$recordMonth : $recordMonth;
			$pastSchoolDay = $recordYear."-".$recordMonth."-".$k;

			return $pastSchoolDay;
		}	
		
		function countSchoolDayDiff($start="", $end="") 
		{
			global $lc;
			
			if($start<=$end) {
				$startDate = $start;
				$endDate = $end;	
			} else {
				$startDate = $end;
				$endDate = $start;	
			}
			
			if($startDate!="" && $endDate!="")	 {
				# get holiday
				$sql = "SELECT LEFT(EventDate,10) FROM INTRANET_EVENT WHERE RecordType=3 Or RecordType=4";	# Public Holiday : RecordType=3; School Holiday : RecordType=4
				$holiday = $this->returnVector($sql);
				
				if($startDate==$endDate)		# same day
					return 0;
					
				$cmonth = date($startDate);
				$recordYear = substr($cmonth, 0, 4);
				$recordMonth = substr($cmonth, 5, 2);
				$recordDay = substr($cmonth, 8, 2);
				$NoOfDays = date("t", mktime(0,0,0,$recordMonth,1,$recordYear));
			
				$k = $recordDay + 0;
				$recordMonth = $recordMonth + 0;
				$m = 0;
				$flag = true;
				while($flag) {
					
					$temp_k = ($k<10) ? "0".$k : $k;
					
					$temp_month = ($recordMonth<10) ? "0".$recordMonth : $recordMonth;
					$tempDate = $recordYear."-".$temp_month."-".$temp_k;
					//echo $tempDate.'<br>';
					
					if($tempDate==$endDate) {
						$flag = false;
						break;
					}
					
					if(!(in_array($tempDate, $holiday))) {	# don't count the holiday
						if ((date("w", mktime(0,0,0,$recordMonth,$k,$recordYear)) != 0) && (date("w", mktime(0,0,0,$recordMonth,$k,$recordYear)) != 6)) {	# don't count Sat & Sun (0: Sun; 6: Sat)
							$m++;
						}
					}
					if($k==$NoOfDays) {
						if($recordMonth+1==13) {
							$recordMonth = 1;
							$k = 1;
							$recordYear++;
						} else {
							$recordMonth++;
							$k = 1;
						}
						$NoOfDays = date("t", mktime(0,0,0,$recordMonth,1,$recordYear));
					} else {
						$k++;
					}
				}
				if($start>$end)
					$m = $m * -1;
				
				return $m;
			}
		}
		
		function updateAttendanceRecordReason($RecordID, $dataAry=array())		
		{
			$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET ";
			foreach($dataAry as $field=>$value)
				$sql .= $field . "=\"". $value."\", ";
			$sql .= "DateModified=now() ";
			$sql .= "WHERE RecordID=$RecordID";
			
			$this->db_db_query($sql);
		}
		
		function Set_Profile($StudentID,$TargetDate,$DayType,$ProfileType,$Reason="|**NULL**|",$Remark="|**NULL**|",$LateSession="|**NULL**|",$HandInLetter="|**NULL**|",$InSchoolTime="",$UpdateLateTime=false,$Waive='|**NULL**|',$IsEarlyLeave=false,$eDisUpdate=true, $seriousLate=null) {
			//debug_r("$StudentID,$TargetDate,$DayType,$ProfileType,$Reason,$Remark,$LateSession,$HandInLetter,$InSchoolTime,$UpdateLateTime,$Waive");
			global $plugin, $sys_custom, $PATH_WRT_ROOT, $intranet_db, $eclass_db;
			
			// get academic year info
			list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			$LastUpdateUserID = isset($_SESSION['UserID']) && $_SESSION['UserID']!=''? $_SESSION['UserID'] : 'NULL';
			
			// if waive is not defined, check if there is existing record
			if ($Waive === '|**NULL**|' || $Waive === 'NULL' || $Waive === null) {
				$sql = "select 
							RecordStatus,
							Reason,
							Remark,
							HandinLetter  
						from 
							CARD_STUDENT_PROFILE_RECORD_REASON 
						where 
							RecordDate = '".$TargetDate."' 
							and 
							StudentID = '".$StudentID."' 
							and 
							DayType = '".$DayType."' 
							and 
							RecordType = '".$ProfileType."' 
							";
				$WaviedBefore = $this->returnResultSet($sql);
				if(count($WaviedBefore)>0) {
					$HaveProfileReasonRecord = 1;
					if($Waive === 'NULL'){
						//$Waive = (trim($WaviedBefore[0]['RecordStatus']) == "" || $WaviedBefore[0]['RecordStatus'] == '0')? ($ProfileType==CARD_STATUS_ABSENT || $ProfileType==CARD_STATUS_OUTING? '|**NULL**|':'0'):($WaviedBefore[0]['RecordStatus']+0);
						$Waive = (trim($WaviedBefore[0]['RecordStatus']) == "" || $WaviedBefore[0]['RecordStatus'] == '0')? '0':($WaviedBefore[0]['RecordStatus']+0);
					}
					if($Reason=="|**NULL**|" || $Reason === null){
						$Reason = trim($WaviedBefore[0]['Reason']) == "" ? '|**NULL**|': $WaviedBefore[0]['Reason'];
					}
					if($Remark=="|**NULL**|" || $Remark === null){
						$Remark= trim($WaviedBefore[0]['Remark']) == "" ? '|**NULL**|': $WaviedBefore[0]['Remark'];
					}
					if($HandInLetter == "|**NULL**|" || $HandInLetter === null){
						$HandInLetter = trim($WaviedBefore[0]['HandInLetter']) == "" ? '|**NULL**|': $WaviedBefore[0]['HandInLetter'];
					}
				}else if($Waive === 'NULL'){
					$HaveProfileReasonRecord = 0;
					$Waive = '0';
				}
				//debug_pr($StudentID.'_'.$Waive.'_'.$Reason.'_'.$Remark);debug_pr($WaviedBefore);debug_pr($sql);
				// if first time confirm attendance record as absent and have preset absence reason, auto set the preset reason to absence profile record
				if(!$HaveProfileReasonRecord && $ProfileType == CARD_STATUS_ABSENT){
					$sql = "SELECT Reason, Waive, DocumentStatus FROM CARD_STUDENT_PRESET_LEAVE WHERE RecordDate='".$TargetDate."' AND StudentID='".$StudentID."' AND DayPeriod='".$DayType."'";
					$reason_rs = $this->returnResultSet($sql);
					if(count($reason_rs)>0 && $reason_rs[0]['Reason'] != ""){
						$Reason = $reason_rs[0]['Reason'];
					}
					if(count($reason_rs)>0 && $reason_rs[0]['Waive']==1){
						$Waive = '1';
					}
					if(count($reason_rs)>0 && $reason_rs[0]['DocumentStatus']==1){
						$DocumentStatus = '1';
					}
				}
				if(!$HaveProfileReasonRecord && ($ProfileType == CARD_STATUS_ABSENT || $ProfileType == CARD_STATUS_OUTING)){
					if($DayType==3 && count($reason_rs)==0){
						$sql = "SELECT Reason FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate = '".$TargetDate."' AND StudentID = '".$StudentID."' AND DayType = '2' AND RecordType IN ('".CARD_STATUS_ABSENT."','".CARD_STATUS_OUTING."')";
						$reason_rs = $this->returnVector($sql);
						if(count($reason_rs)>0 && $reason_rs[0] != ""){
							$Reason = $reason_rs[0];
						}
					}
				}
			}else if($ProfileType == CARD_STATUS_ABSENT){
				// if it is new absent profile record, auto apply preset absence prove document waive option
				$sql = "select 
							RecordStatus  
						from 
							CARD_STUDENT_PROFILE_RECORD_REASON 
						where 
							RecordDate = '".$TargetDate."' 
							and 
							StudentID = '".$StudentID."' 
							and 
							DayType = '".$DayType."' 
							and 
							RecordType = '".$ProfileType."' 
							";
				$profileRecordStatusVec = $this->returnVector($sql);
				if(count($profileRecordStatusVec)==0){
					// it is new absent profile record
					$sql = "SELECT DocumentStatus FROM CARD_STUDENT_PRESET_LEAVE WHERE RecordDate='".$TargetDate."' AND StudentID='".$StudentID."' AND DayPeriod='".$DayType."'";
					$documentStatusVec = $this->returnVector($sql);
					if(count($documentStatusVec)>0 && $documentStatusVec[0]==1){
						$DocumentStatus = '1';
					}
				}
				// 2015-10-14 prove document status extended logic.. automatic sync AM & PM
				if($DayType == PROFILE_DAY_TYPE_PM && $DocumentStatus != 1){
					$sql = "SELECT DocumentStatus FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='".$TargetDate."' AND StudentID='".$StudentID."' AND DayType='".PROFILE_DAY_TYPE_AM."'";
					$documentStatusVec = $this->returnVector($sql);
					if(count($documentStatusVec)>0 && $documentStatusVec[0]==1){
						$DocumentStatus = '1';
					}
				}
			}
			
			// remove early leave daily status if is set to Absence or Outing
			if ($ProfileType == CARD_STATUS_ABSENT || (!$IsEarlyLeave && $ProfileType == CARD_STATUS_OUTING)) {
				$TargetTime = strtotime($TargetDate);
				$TargetYear = date('Y',$TargetTime);
				$TargetMonth = date('m',$TargetTime);
				$TargetDay = date('d',$TargetTime)+0;
				if ($DayType == PROFILE_DAY_TYPE_AM) 
					$LeaveStatus = CARD_LEAVE_AM;
				else 
					$LeaveStatus = CARD_LEAVE_PM;
				$sql = "select 
									count(1) 
								from 
									CARD_STUDENT_DAILY_LOG_".$TargetYear."_".$TargetMonth." 
								where 
									UserID = '".$StudentID."' 
									AND 
									DayNumber = '".$TargetDay."' 
									AND 
									LeaveStatus = '".$LeaveStatus."'
							 ";
				$HaveEarlyLeave = $this->returnVector($sql);
				if ($HaveEarlyLeave[0] > 0) {
					$sql = "update CARD_STUDENT_DAILY_LOG_".$TargetYear."_".$TargetMonth." set 
										LeaveStatus = NULL,
										LeaveSchoolTime = NULL,
										LeaveSchoolStation = NULL 
									where 
										UserID = '".$StudentID."' 
										AND 
										DayNumber = '".$TargetDay."' 
										";
					$Result["RemoveEarlyLeave:".$StudentID.":".$TargetDate.":".$DayType] = $this->db_db_query($sql);
				}
			}
			
			// auto set PM daily status if attendance mode is "whole day without lunch" and basic setting "PM Not Follow AM Status" set to false
			if ($DayType == PROFILE_DAY_TYPE_AM && $this->attendance_mode == 3 && $this->PMStatusNotFollowAMStatus != 1) {
				$Result["AutoSetPMStatus:".$StudentID.":".$TargetDate] = $this->Auto_Set_PM_Status($ProfileType,$StudentID,$TargetDate,$eDisUpdate);
			}
			
			// get target user info
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$User = new libuser($StudentID);
			
			switch ($ProfileType) {
				case CARD_STATUS_LATE:
					$RemoveProfileType[] = CARD_STATUS_ABSENT;
					if ($Waive == 1) 
						$RemoveProfileType[] = CARD_STATUS_LATE;
					$RealProfileType = CARD_STATUS_LATE;
					if(!$HaveProfileReasonRecord && $Reason == '|**NULL**|' && $Reason !== null && $this->DefaultLateReason != ''){ // if no profile record and no reason, use default reason if have set
						$Reason = $this->DefaultLateReason;
					}
					break;
				case CARD_STATUS_ABSENT:
					$RemoveProfileType[] = CARD_STATUS_LATE;
					$RemoveProfileType[] = PROFILE_TYPE_EARLY;
					if ($Waive == 1) 
						$RemoveProfileType[] = CARD_STATUS_ABSENT;
					$RealProfileType = CARD_STATUS_ABSENT;
					if(!$HaveProfileReasonRecord && $Reason == '|**NULL**|' && $Reason !== null && $this->DefaultAbsentReason != ''){ // if no profile record and no reason, use default reason if have set
						$Reason = $this->DefaultAbsentReason;
					}
					break;
				case PROFILE_TYPE_EARLY:
				case CARD_STATUS_OUTING:
					if ($IsEarlyLeave) {
						if ($Waive == 1) 
							$RemoveProfileType[] = PROFILE_TYPE_EARLY;
							
						$RealProfileType = PROFILE_TYPE_EARLY;
						if(!$HaveProfileReasonRecord && $Reason == '|**NULL**|' && $Reason !== null && $this->DefaultEarlyReason != ''){ // if no profile record and no reason, use default reason if have set
							$Reason = $this->DefaultEarlyReason;
						}
					}
					else {
						$RemoveProfileType[] = CARD_STATUS_ABSENT;
						$RemoveProfileType[] = CARD_STATUS_LATE;
						$RemoveProfileType[] = PROFILE_TYPE_EARLY;
						$RealProfileType = CARD_STATUS_ABSENT;
						if(!$HaveProfileReasonRecord && $Reason == '|**NULL**|' && $Reason !== null && $this->DefaultOutingReason != ''){ // if no profile record and no reason, use default reason if have set
							$Reason = $this->DefaultOutingReason;
						}
					}
					break;
				default:
					break;
			}
			
			// auto update PM absent Reason from AM 
			if($this->attendance_mode == 3 && $this->PMStatusNotFollowAMStatus != 1 && $DayType == PROFILE_DAY_TYPE_AM && $ProfileType == CARD_STATUS_ABSENT && $Reason != '|**NULL**|' && $Reason !== null){
				$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason='".$this->Get_Safe_Sql_Query(trim($Reason))."' 
							WHERE RecordDate = '".$TargetDate."' AND StudentID = '".$StudentID."' AND DayType = '".PROFILE_DAY_TYPE_PM."' AND RecordType = '".CARD_STATUS_ABSENT."' AND RecordStatus IS NULL ";
				$Result["AutoSetPMAbsentReason_".$StudentID."_".$TargetDate] = $this->db_db_query($sql);
			}
			
			if (is_array($RemoveProfileType)) {
				for ($i=0; $i< sizeof($RemoveProfileType); $i++) {
					if ($RemoveProfileType[$i] != $RealProfileType /* && $ProfileType != CARD_STATUS_OUTING && $RemoveProfileType[$i] != CARD_STATUS_ABSENT */) {
						$sql = 'delete from CARD_STUDENT_PROFILE_RECORD_REASON 
									where 
										StudentID = \''.$StudentID.'\' 
										and 
										RecordDate = \''.$TargetDate.'\' 
										and 
										DayType = \''.$DayType.'\' 
										and 
										RecordType = \''.$RemoveProfileType[$i].'\'';
						$Result[$StudentID.':RemoveOrgCardDProfile:'.$RemoveProfileType[$i]] = $this->db_db_query($sql);
					}

					// if need to remove late profile, remove eDis late record also
					if ($eDisUpdate && $RemoveProfileType[$i] == CARD_STATUS_LATE) {
						$this->Delete_eDis_Late_Record($StudentID,$TargetDate,$DayType);
					}
					
					$sql = 'delete from PROFILE_STUDENT_ATTENDANCE 
									where 
										UserID = \''.$StudentID.'\' 
										and 
										DATE_FORMAT(AttendanceDate,\'%Y-%m-%d\') = \''.$TargetDate.'\' 
										and 
										DayType = \''.$DayType.'\' 
										and 
										RecordType = \''.$RemoveProfileType[$i].'\'';
					$Result[$StudentID.':RemoveOrgProfile'.$RemoveProfileType[$i]] = $this->db_db_query($sql);
					
					if($sys_custom['StudentAttendance']['SyncDataToPortfolio']){
						$this->SyncProfileRecordToPortfolio($StudentID, $TargetDate, $DayType, $RemoveProfileType[$i]);
					}
				}
			}
			
			// [2016-0121-1051-53206] for manual input late time, remove eDis late record
			if ($eDisUpdate && $ProfileType == CARD_STATUS_LATE && $Waive != 1 && $UpdateLateTime) {
				$this->Delete_eDis_Late_Record($StudentID,$TargetDate,$DayType);
			}
			
			if (($ProfileType != CARD_STATUS_OUTING || $IsEarlyLeave) && $Waive != 1) {
				// insert PROFILE_RECORD_REASON 
			    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE (
			    					UserID, 
			    					AttendanceDate,
			    					Year, 
			    					Semester, 
			    					RecordType, 
			    					DayType, 
			    					DateInput,
			    					DateModified,
			    					ClassName,
			    					ClassNumber,
			    					AcademicYearID,
			    					YearTermID ";
			    if ($Reason!="|**NULL**|" && $Reason !== null) {
			    	$sql .= ",Reason";
			    }
			    $sql .= "					
			    				) 
			    				VALUES (
			    					'$StudentID',
			    					'$TargetDate',
			    					'".$this->Get_Safe_Sql_Query($school_year)."',
			    					'".$this->Get_Safe_Sql_Query($semester)."',
			    					'".$RealProfileType."',
			    					'".$DayType."',
			    					now(),
			    					now(),
			    					'".$this->Get_Safe_Sql_Query($User->ClassName)."',
			    					'".$User->ClassNumber."',
			    					'$school_year_id',
			    					'$semester_id' ";
			    if ($Reason!="|**NULL**|" && $Reason !== null) {
			    	$sql .= ",'".$this->Get_Safe_Sql_Query($Reason)."'";
			    }
			    $sql .= "
			    				) 
			    				on duplicate key update 
			    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
			    					DateModified = NOW() ";
			    if ($Reason!="|**NULL**|" && $Reason !== null) {
			    	$sql .= ", Reason = '".$this->Get_Safe_Sql_Query($Reason)."'";
			    }
			    $Result[$StudentID.':InsertTo-PROFILE_STUDENT_ATTENDANCE'] = $this->db_db_query($sql);
		    // get Profile ID
				$sql = 'select LAST_INSERT_ID()';
				$Temp = $this->returnVector($sql);
				$NewProfileID = $Temp[0];
				
				if($sys_custom['StudentAttendance']['SyncDataToPortfolio']){
					$this->SyncProfileRecordToPortfolio($StudentID, $TargetDate, $DayType, $RealProfileType);
				}
		}
	    /*
	    // if PM is early leave, need to remove AM early leave record, because system design structure cannot let both AM & PM early leave exist in the same day
	    if($IsEarlyLeave && $DayType == PROFILE_DAY_TYPE_PM){
	    	$sql = 'delete from CARD_STUDENT_PROFILE_RECORD_REASON 
						where 
							StudentID = \''.$StudentID.'\' 
							and 
							RecordDate = \''.$TargetDate.'\' 
							and 
							DayType = \''.PROFILE_DAY_TYPE_AM.'\' 
							and 
							RecordType = \''.PROFILE_TYPE_EARLY.'\'';
			$Result[$StudentID.':RemoveAMEarlyLeaveProfile'] = $this->db_db_query($sql);
			
			$sql = 'delete from PROFILE_STUDENT_ATTENDANCE 
							where 
								UserID = \''.$StudentID.'\' 
								and 
								DATE_FORMAT(AttendanceDate,\'%Y-%m-%d\') = \''.$TargetDate.'\' 
								and 
								DayType = \''.PROFILE_DAY_TYPE_AM.'\' 
								and 
								RecordType = \''.PROFILE_TYPE_EARLY.'\'';
			$Result[$StudentID.':RemoveAMEarlyLeaveProfile'] = $this->db_db_query($sql);
	    }
	    */
	    // insert CARD_STUDENT_PROFILE_RECORD_REASON
	    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON (
	    					RecordDate, 
	    					StudentID, 
	    					ProfileRecordID, 
	    					RecordType, 
	    					DayType, 
	    					DateInput, 
	    					DateModified,
							InputBy,
							ModifyBy";
	    if ($LateSession!=="|**NULL**|")
	    	$sql .= ",AbsentSession";
	    if ($Reason!=="|**NULL**|" && $Reason !== null) 
	    	$sql .= ",Reason";
	    if ($Remark!=="|**NULL**|" && $Remark !== null) 
	    	$sql .= ",Remark";
	    if ($HandInLetter!=="|**NULL**|")
	    	$sql .= ",HandinLetter";
	    if ($Waive!=="|**NULL**|" && $Waive!==null)
	    	$sql .= ",RecordStatus";
	    if($ProfileType == CARD_STATUS_ABSENT && $DocumentStatus=='1'){
	    	$sql .= ",DocumentStatus";
	    }
	    if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] && $ProfileType == CARD_STATUS_LATE){
	    	$sql .= ",LateTime";
	    }
	    if($sys_custom['StudentAttendance']['SeriousLate'] && $seriousLate !== null){
            $sql .= ",SeriousLate";
	    }
	    $sql .= "
	    				)
	            VALUES (
	            	'$TargetDate', 
	            	'$StudentID', 
	            	'$NewProfileID', 
	            	'".$RealProfileType."',
	    					'".$DayType."',
	            	now(), 
	            	now(),
					$LastUpdateUserID,
					$LastUpdateUserID";
			if ($LateSession!=="|**NULL**|")
	    	$sql .= ",'$LateSession'";
	    if ($Reason!=="|**NULL**|" && $Reason !== null) 
	      $sql .= ",'".$this->Get_Safe_Sql_Query($Reason)."'";
	    if ($Remark!=="|**NULL**|" && $Remark !== null) 
	    	$sql .= ",'".$this->Get_Safe_Sql_Query($Remark)."'";
	    if ($HandInLetter!=="|**NULL**|")
	      $sql .= ",'$HandInLetter'";
	    if ($Waive!=="|**NULL**|" && $Waive!==null)
	    	$sql .= ",'$Waive'";
	    if($ProfileType == CARD_STATUS_ABSENT && $DocumentStatus=='1'){
	    	$sql .= ",'1'";
	    }
	    if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] && $ProfileType == CARD_STATUS_LATE){
	    	$sql .= ",".($InSchoolTime==''?"NULL":"'$InSchoolTime'");
	    }
	    if($sys_custom['StudentAttendance']['SeriousLate'] && $seriousLate !== null){
	        if($ProfileType == CARD_STATUS_LATE){
	            $sql .= ", '" . ($seriousLate ? $seriousLate : '0') ."'";
	        } else {
	            $sql .= ", '0'";
	        }
	    }
	    $sql .= "
	            ) 
							on duplicate key update 
								RecordID = LAST_INSERT_ID(RecordID), 
								DateModified = NOW(), ModifyBy=$LastUpdateUserID, 
								ProfileRecordID = '".$NewProfileID."'";
			if ($Reason!=="|**NULL**|" && $Reason !== null) 
				$sql .= ",Reason = '".$this->Get_Safe_Sql_Query($Reason)."'";
			if ($Remark!=="|**NULL**|" && $Remark !== null) 
				$sql .= ",Remark = '".$this->Get_Safe_Sql_Query($Remark)."'";
			if ($LateSession!=="|**NULL**|")
				$sql .= ",AbsentSession = '".$LateSession."'";
			if ($HandInLetter!=="|**NULL**|")
				$sql .= ",HandinLetter = '".$HandInLetter."'";
			if ($Waive!=="|**NULL**|" && $Waive!==null)
	    		$sql .= ",RecordStatus = '".$Waive."'";
	    	if($sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] && $ProfileType == CARD_STATUS_LATE){
	    		$sql .= ",LateTime = ".($InSchoolTime==''?"NULL":"'$InSchoolTime'");
	    	}
	    	if($sys_custom['StudentAttendance']['SeriousLate'] && $seriousLate !== null){
	    	    if($ProfileType == CARD_STATUS_LATE){
	    	        $sql .= ",SeriousLate = '" . ($seriousLate ? $seriousLate : '0') ."'";
	    	    } else {
	    	        $sql .= ",SeriousLate = '0'";
	    	    }
	    	}
			//debug_r($sql);
			$Result[$StudentID.':InsertTo-CARD_STUDENT_PROFILE_RECORD_REASON'] = $this->db_db_query($sql);
			
			if ($eDisUpdate && $ProfileType == CARD_STATUS_LATE && $Waive != 1) {
				// [2016-0121-1051-53206] commented - as new late record will be linked to deleted attendance record ($NewProfileID)
//				if ($UpdateLateTime) 
//					$this->Delete_eDis_Late_Record($StudentID,$TargetDate,$DayType);
				$this->Insert_eDis_Late_Record($StudentID,$TargetDate,$DayType,$NewProfileID,$InSchoolTime);
			}
			//debug_r($Result);
			return !in_array(false,$Result);
		}
		
		function Clear_Profile($StudentID,$TargetDate,$DayType,$eDisUpdate=true) {
			global $plugin, $sys_custom, $PATH_WRT_ROOT, $intranet_db, $eclass_db;
			
			if($eDisUpdate){
				$this->Delete_eDis_Late_Record($StudentID,$TargetDate,$DayType);
			}
			$sql = 'delete from CARD_STUDENT_PROFILE_RECORD_REASON 
							where 
								StudentID = \''.$StudentID.'\' 
								and 
								RecordDate = \''.$TargetDate.'\' 
								and 
								DayType = \''.$DayType.'\' 
								and 
								RecordType in (\''.CARD_STATUS_LATE.'\',\''.CARD_STATUS_ABSENT.'\')';
			$Result[$StudentID.':RemoveCardProfile'] = $this->db_db_query($sql);
			
			$sql = 'delete from PROFILE_STUDENT_ATTENDANCE 
							where 
								UserID = \''.$StudentID.'\' 
								and 
								AttendanceDate like \''.$TargetDate.'%\' 
								and 
								DayType = \''.$DayType.'\' 
								and 
								RecordType in (\''.CARD_STATUS_LATE.'\',\''.CARD_STATUS_ABSENT.'\')';
			$Result[$StudentID.':RemoveStudProfile'] = $this->db_db_query($sql);
			
			// auto set PM daily status if attendance mode is "whole day without lunch" and basic setting "PM Not Follow AM Status" set to false
			if ($DayType == PROFILE_DAY_TYPE_AM && $this->attendance_mode == 3 && $this->PMStatusNotFollowAMStatus != 1) {
				$Result["AutoSetPMStatus:".$StudentID.":".$TargetDate] = $this->Auto_Set_PM_Status(CARD_STATUS_PRESENT,$StudentID,$TargetDate,$eDisUpdate);
			}
			
			if($sys_custom['StudentAttendance']['SyncDataToPortfolio']){
				$this->SyncProfileRecordToPortfolio($StudentID, $TargetDate, $DayType, CARD_STATUS_LATE);
				$this->SyncProfileRecordToPortfolio($StudentID, $TargetDate, $DayType, CARD_STATUS_ABSENT);
			}
			
			return !in_array(false,$Result);
		}
		
		function Auto_Set_PM_Status($AMStatusType,$StudentID,$TargetDate,$eDisUpdate=true) {
			switch ($AMStatusType) {
				case CARD_STATUS_PRESENT:
				case CARD_STATUS_LATE:
					// check if there is early leave record
					$sql = "select 
										count(1) 
									from 
										CARD_STUDENT_PROFILE_RECORD_REASON 
									where 
										RecordDate = '".$TargetDate."' 
										and 
										StudentID = '".$StudentID."' 
										and 
										DayType = '".PROFILE_DAY_TYPE_AM."' 
										and 
										RecordType = '".PROFILE_TYPE_EARLY."'";
					$Temp = $this->returnVector($sql);
					if ($Temp[0] > 0) { // early leave found
						$PMAutoStatus = CARD_STATUS_ABSENT;
					}
					else {
						$PMAutoStatus = CARD_STATUS_PRESENT;
					}
					break;
				case CARD_STATUS_ABSENT:
				case PROFILE_TYPE_EARLY:
				case CARD_STATUS_OUTING:
					$PMAutoStatus = CARD_STATUS_ABSENT;
					break;
				default:
					// should not be empty
					break;
			}
			
			// check if PM confirmed already
			$Temp = explode('-',$TargetDate);
			list($TargetYear,$TargetMonth,$TargetDay) = $Temp;
			$sql = "select
								count(1) 
							from 
								CARD_STUDENT_DAILY_LOG_".$TargetYear."_".$TargetMonth." 
							where 
								UserID = '".$StudentID."' 
								and 
								DayNumber = '".$TargetDay."' 
								and 
								PMConfirmedUserID IS NOT NULL 
								AND 
								PMIsConfirmed = '1'
						 ";
			$PMConfirmStatus = $this->returnVector($sql);
			
			if (trim($PMAutoStatus) != "" && $PMConfirmStatus[0] == '0') {
				// check if there is preset absence/ outing in PM
				// Get preset outing for today
				$sql = "select 
								  count(1)
								from 
									CARD_STUDENT_OUTING 
								where 
									UserID = '".$StudentID."' 
									and 
									RecordDate = '".$TargetDate."' 
									and 
									DayType = '".PROFILE_DAY_TYPE_PM."'";
				$Temp = $this->returnVector($sql);
				if ($Temp[0] > 0) {
					return true;
				}
				
				// Get preset absence for today 
				$sql = "select 
									count(1) 
								from 	
									CARD_STUDENT_PRESET_LEAVE 
								where 
									StudentID = '".$StudentID."' 
									and 
									RecordDate = '".$TargetDate."' 
									and 
									DayPeriod = '".PROFILE_DAY_TYPE_PM."'";
				$Temp = $this->returnVector($sql);
				if ($Temp[0] > 0) {
					return true;
				}
				
				// no preset absent/ outing found, proceed the auto set
				$TargetTime = strtotime($TargetDate);
				$TargetYear = date('Y',$TargetTime);
				$TargetMonth = date('m',$TargetTime);
				$TargetDay = date('d',$TargetTime) + 0;
				$sql = "update CARD_STUDENT_DAILY_LOG_".$TargetYear."_".$TargetMonth." set 
									PMStatus = '".$PMAutoStatus."' 
								where 
									UserID = '".$StudentID."' 
									and 
									DayNumber = '".$TargetDay."'";
				$Result["UpdateDailyLog"] = $this->db_db_query($sql);
				if($eDisUpdate){
					if ($PMAutoStatus == CARD_STATUS_PRESENT) 
						$Result["ClearProfile"] = $this->Clear_Profile($StudentID,$TargetDate,PROFILE_DAY_TYPE_PM);
					else 
						$Result["SetAbsentProfile"] = $this->Set_Profile($StudentID,$TargetDate,PROFILE_DAY_TYPE_PM,$PMAutoStatus,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',false);
				}
				return !in_array(false,$Result);
			}
			else {
				return true;
			}
		}
		
		function Delete_eDis_Late_Record($StudentID,$TargetDate,$DayType) {
			//debug_r($StudentID.' '.$TargetDate.' '.$DayType);
			global $plugin, $PATH_WRT_ROOT;
			if ($plugin['Disciplinev12']){
				include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
				$ldisciplinev12 = new libdisciplinev12();
				$LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
				
				$sql = 'select 
									StudentAttendanceID 
								from 
									PROFILE_STUDENT_ATTENDANCE 
								where 
									UserID = \''.$StudentID.'\' 
									and 
									AttendanceDate = \''.$TargetDate.'\' 
									and 
									DayType = \''.$DayType.'\' 
									and 
									RecordType = \''.CARD_STATUS_LATE.'\'';
				//debug_r($sql);
				$Temp = $this->returnVector($sql);
				
				// find the eDis late misconduct records that it has StudentAttendanceID but the related PROFILE_STUDENT_ATTENDANCE does not exist anymore. 
				$sql = "SELECT 
							a.StudentAttendanceID 
						FROM DISCIPLINE_ACCU_RECORD as a 
						LEFT JOIN PROFILE_STUDENT_ATTENDANCE as p ON p.StudentAttendanceID=a.StudentAttendanceID 
						WHERE a.StudentID='$StudentID' AND DATE_FORMAT(a.RecordDate, '%Y-%m-%d')='$TargetDate' AND a.StudentAttendanceID IS NOT NULL 
							AND a.StudentAttendanceID > 0 AND p.StudentAttendanceID IS NULL";
				$Temp2 = $this->returnVector($sql);
				
				$Temp = array_values(array_unique(array_merge($Temp, $Temp2)));
				
				# Reset upgrade items
				if ($LateCtgInUsed)
				{
					// should only have one result and loop once
					// but add this loop to cater incase that there is zombie profile late record 
					// created previous [by kenneth chung 2010-06-03]
					for ($i=0; $i< sizeof($Temp); $i++) {
						// pre remove the late profile record before calling $ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD() 
						$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID='".$Temp[$i]."'";
						$this->db_db_query($sql);
						$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($Temp[$i]);
					}
				}
			}
		}
		
		function Insert_eDis_Late_Record($StudentID,$TargetDate,$DayType,$ProfileID,$InSchoolTime) {
			//debug_r("$StudentID,$TargetDate,$DayType,$ProfileID,$InSchoolTime");
			global $plugin, $sys_custom, $PATH_WRT_ROOT;
			
			// check if there is already eDis late record to prevent duplicated records
			$sql = "SELECT COUNT(1) FROM DISCIPLINE_ACCU_RECORD WHERE FORMAT_DATE(RecordDate,'%Y-%m-%d')='$TargetDate' AND StudentID='$StudentID' AND StudentAttendanceID='$ProfileID'";
			$count_result = $this->returnVector($sql);
			if($count_result[0] > 0) return true;
			
			// get academic year info
			list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			
			// get target user info
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$User = new libuser($StudentID);
			if ($plugin['Disciplinev12']){
				include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
		  	$ldisciplinev12 = new libdisciplinev12();
		  	$LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
		  	
		  	## Only For IP25 eDis1.2 Only ##
		  	if($sys_custom['Discipline_SeriousLate']){	
			    $sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType = -1 AND LateMinutes IS NOT NULL";
			    //debug_r($sql);
			    $targetCatID = $ldisciplinev12->returnVector($sql);
			    if(sizeof($targetCatID)>0){
			    	for($i=0; $i<sizeof($targetCatID); $i++){
			    		$result = $ldisciplinev12->CategoryInUsed($targetCatID[$i]);
			    		
			    		if($result){
			    			$cnt++;
			    		}
			    	}
			    }
			    if($cnt > 0){
			    	$SeriousLateUsed = 1;
			    }
				}
			}
			
			if ($plugin['Disciplinev12'] && $LateCtgInUsed) {
				# Calculate upgrade items
				$dataAry = array();
				$dataAry['StudentID'] = $StudentID;
				$dataAry['RecordDate'] = $TargetDate;
				//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
				$dataAry['Year'] = $school_year;
				$dataAry['YearID'] = $school_year_id;
				//$semester = retrieveSemester($TargetDate);
				$dataAry['Semester'] = $semester;
				$dataAry['SemesterID'] = $semester_id;
				$dataAry['StudentAttendanceID'] = $ProfileID;
				$dataAry['Remark'] = $InSchoolTime;
				
				if($SeriousLateUsed == 1) {
					if($InSchoolTime != "")
					{
						$arr_time_table = $this->Get_Student_Attend_Time_Setting($TargetDate,$StudentID);
						if ($DayType == PROFILE_DAY_TYPE_AM) {
							$school_start_time = $arr_time_table['MorningTime'];
						} 
						else if ($DayType == PROFILE_DAY_TYPE_PM) {
							if($this->attendance_mode==1)	# retrieve InSchoolTime
								$school_start_time = $arr_time_table['MorningTime'];
							else # retrieve LunchBackTime
								$school_start_time = $arr_time_table['LunchEnd'];
						}

						
						$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
						$late_minutes = ceil($str_late_minutes/60);
						
						$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
						$min_late_minutes = $this->returnVector($sql);		
						
						if(sizeof($min_late_minutes)>0)
						{
							$dataAry['SeriousLate'] = 1;
							$dataAry['LateMinutes'] = $late_minutes;
						}
						else
						{
							$dataAry['SeriousLate'] = 0;
						}
					}
					else
					{
						$dataAry['SeriousLate'] = 0;
					}
				}
				
				//debug_r($dataAry);
				$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
			}
		}
		
		function Check_LateAbsentEarlyClassGroup_Confirm_Status($DayType,$RecordType,$TargetDate="") {
			global $PATH_WRT_ROOT, $sys_custom;
			
			$today = date("Y-m-d");
			if ($RecordType == "Class" || $RecordType == "Group" || $RecordType == "Subject_Group") {
				$TargetDate = ($TargetDate != "")? $TargetDate:date('Y-m-d');
				$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
				
				if($RecordType == "Subject_Group" && $sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']) {
					include_once($PATH_WRT_ROOT."includes/libtimetable.php");
					$libtimetable = new Timetable();
					$timetableID = $libtimetable->Get_Current_Timetable($TargetDate);
				}
				// for insert cache value
				$InsertValues = array();
									
				if ($RecordType == "Class") {
					// prepare class list
					include_once($PATH_WRT_ROOT.'includes/libclass.php');
					$libclass = new libclass();
					$TempClassList = $libclass->getClassList($CurrentAcademicYearID);
					for ($i=0; $i< sizeof($TempClassList); $i++) {
						$ClassStatus[$TempClassList[$i]['ClassID']] = false;
						$ClassName[$TempClassList[$i]['ClassID']] = $TempClassList[$i]['ClassName'];
					}
					$total_classes=sizeof($TempClassList);
				}
				else if($RecordType =="Group"){
					$TempClassList = $this->getGroupListMode(3);
					$total_classes = 0;
					for ($i=0; $i< sizeof($TempClassList); $i++) {
						list($GroupID,$GroupTitle,$Mode) = $TempClassList[$i];
						if ($Mode != 2) {
							$ClassStatus[$GroupID] = false;
							$ClassName[$GroupID] = $GroupTitle;
							$total_classes++;
						}
					}
				}else if($RecordType == "Subject_Group") {
					if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
						$TempClassList = $this->Get_Subject_Group_Attendance_List($TargetDate,$DayType);
					}else{
						$TempClassList = $this->Get_Subject_Group_Attendance_List_Without_Timetable($TargetDate,$DayType);
					}
					for ($i=0; $i< sizeof($TempClassList); $i++) {
						$ClassStatus[$TempClassList[$i]['SubjectGroupID']] = false;
						$ClassName[$TempClassList[$i]['SubjectGroupID']] = $TempClassList[$i]['SubjectGroupName'];
					}
					$total_classes = sizeof($TempClassList);
				}
				
				// prepare confirm color data
				$records_with_data = array();
				$ts = strtotime($TargetDate);
				$iteration = 0;
				$class_array=array();
				# Count 12 months
				while($iteration < 6)
				{
		      $year = date('Y',$ts);
		      $month = date('m',$ts);
		      $days = date('t',$ts);
		      $ConfirmTable = "CARD_STUDENT_DAILY_".strtoupper($RecordType)."_CONFIRM_".$year."_".$month;
					
					$sql = "select 
										TargetDate,
										ConfirmStatus 
									from 
										CARD_STUDENT_CACHED_CONFIRM_STATUS 
									where 
										DayType = '".$DayType."' 
										and 
										TargetDate like '".$year."-".$month."%' 
										and 
										Type = '".$RecordType."' 
										and TargetDate <= '".$today."'
										";
					$Temp = $this->returnArray($sql);
					for ($i=0; $i< sizeof($Temp); $i++) {
						$CacheStatus[$Temp[$i]['TargetDate']] = $Temp[$i]['ConfirmStatus'];
					}
					
			  	// init all days to be all not confirmed
		      for($d=1;$d<=$days;$d++)
		      {
		      	$day = ($d < 10? "0".$d:"".$d);
		      	$entry_date = $year."-".$month."-".$day;
		      	if($entry_date > $today) continue;
		      	$records_with_data[$entry_date] = $ClassStatus;
		      }
		      
		      // Find days with confirmed data
		      if ($RecordType == "Class") {
			      $sql = "SELECT DISTINCT 
											yc.YearClassID,
											yc.ClassTitleEN,
											cc.DayNumber,
											cc.DayType 
									  FROM 
											YEAR as y 
											INNER JOIN YEAR_CLASS as yc 
											ON y.YearID = yc.YearID AND yc.AcademicYearID = '".$CurrentAcademicYearID."'  
											LEFT JOIN 
											$ConfirmTable as cc 
											ON yc.YearClassID=cc.ClassID 
									  WHERE 
									  	cc.DayNumber IS NOT NULL 
									  	and 
									  	DayType = '".$DayType."'
									  ORDER BY cc.DayNumber,yc.YearClassID ";
					}
					else if($RecordType == "Group"){
						$sql = "SELECT DISTINCT
											g.GroupID,
											g.Title,
											gc.DayNumber,
											gc.DayType 
										FROM 
											INTRANET_GROUP as g 
											LEFT JOIN YEAR_CLASS as yc ON g.GroupID = yc.GroupID 
											LEFT JOIN CARD_STUDENT_ATTENDANCE_GROUP as ag ON ag.GroupID = g.GroupID 
											LEFT JOIN $ConfirmTable as gc ON gc.GroupID = g.GroupID  
										WHERE 
											g.AcademicYearID = '".$CurrentAcademicYearID."' 
											AND gc.DayType = '".$DayType."' 
											AND g.RecordType = 3 
											AND (ag.Mode IS NULL OR ag.Mode <> '2') AND yc.GroupID IS NULL 
										ORDER BY gc.DayNumber,g.GroupID ";
					}else if($RecordType == "Subject_Group") {
						if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
							$sql = "SELECT DISTINCT 
										stc.SubjectGroupID,
										stc.ClassTitleEN,
										c.DayNumber,
										c.DayType 
									FROM ACADEMIC_YEAR_TERM as ayt 
									inner join SUBJECT_TERM as st on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$CurrentAcademicYearID."'
									inner join SUBJECT_TERM_CLASS as stc on st.SubjectGroupID = stc.SubjectGroupID
									inner join SUBJECT_TERM_CLASS_TEACHER as stct on stc.SubjectGroupID = stct.SubjectGroupID 
									inner join INTRANET_TIMETABLE_ROOM_ALLOCATION as itra on stc.SubjectGroupId = itra.SubjectGroupID and itra.TimetableID = '".$timetableID."'
									inner join INTRANET_TIMETABLE_TIMESLOT as itt on itra.TimeSlotID = itt.TimeSlotID 
									LEFT JOIN $ConfirmTable as c ON c.SubjectGroupID=stc.SubjectGroupID AND c.DayType='".$DayType."'
									WHERE ayt.AcademicYearID='$CurrentAcademicYearID' AND c.DayType = '".$DayType."' 
									ORDER BY c.DayNumber,stc.SubjectGroupID";
						}else{
							$sql = "SELECT DISTINCT 
										stc.SubjectGroupID,
										stc.ClassTitleEN,
										c.DayNumber,
										c.DayType 
									FROM ACADEMIC_YEAR_TERM as ayt 
									inner join SUBJECT_TERM as st on ayt.YearTermID = st.YearTermID and ayt.AcademicYearID = '".$CurrentAcademicYearID."'
									inner join SUBJECT_TERM_CLASS as stc on st.SubjectGroupID = stc.SubjectGroupID
									left join SUBJECT_TERM_CLASS_TEACHER as stct on stc.SubjectGroupID = stct.SubjectGroupID 
									LEFT JOIN $ConfirmTable as c ON c.SubjectGroupID=stc.SubjectGroupID AND c.DayType='".$DayType."'
									WHERE ayt.AcademicYearID='$CurrentAcademicYearID' AND c.DayType = '".$DayType."' 
									ORDER BY c.DayNumber,stc.SubjectGroupID";
						}
					}
					//debug_r($sql);
		      $temp = $this->returnArray($sql);
		      //debug_r($temp);
		      for($i=0; $i<sizeof($temp); $i++)
		      {
	          list($YearClassID,$ClassTitle,$DayNumber,$Day_Type) = $temp[$i];
	          $DayNumber = ($DayNumber < 10? "0".$DayNumber : $DayNumber);
	          $entry_date = $year."-".$month."-".$DayNumber;
	          if($entry_date > $today) continue;
          	$records_with_data[$entry_date][$YearClassID]=true;
		      }
			  
		      $ts = mktime(0,0,0,$month-1,1,$year);
		      $iteration++;
				}
				//$today = date("Y-m-d");
				// Period AM :
				foreach($records_with_data as $date_key=>$classes)
				{
				  $date_string = $date_key;
				  if (!$CacheStatus[$date_string]) {
					  $total_not_required = 0;// total of classes that donot require to take attendance on that day
					  $total_confirmed = 0;
					  $total_required = 0;
					  
					  if(sizeof($classes)>0)
					  {
					  	foreach($classes as $year_class_id=> $Confirmed)
					  	{
					  		if(!$Confirmed)
					  		{
					  			if ($RecordType == "Class") {
						  			if(!$this->isRequiredToTakeAttendanceByDate($ClassName[$year_class_id],$date_string))
							      		$total_not_required+=1;
							      	else
							      		$total_required+=1;
							    }
							    else if($RecordType == "Group"){
							    	if(!$this->groupIsRequiredToTakeAttendanceByDate($year_class_id,$date_string))
		          						$total_not_required+=1;
		          					else
		          						$total_required+=1;
							    }else if($RecordType == "Subject_Group") {
							    	$total_required+=1;
							    }
					  		}
					  		else {
					  			$total_confirmed+=1;
					  		}				  		
					  	}
					  }
					  
					  $class_count=$total_confirmed+$total_not_required;
					  if($total_not_required == $total_classes){
					  	$Return[$date_string] = BLANKDATA; // all classes not required to take attendance, just pad cache data
					  }else if($class_count==0 || ($total_required+$total_not_required)==$total_classes && $date_string<=$today){// all classes not confirmed
					  	$Return[$date_string] = NONECONFIRM;
					  }else if($class_count==$total_classes){// all classes are confirmed
					  	if ($total_not_required != $total_classes) 
								$Return[$date_string] = ALLCONFIRM;
					  }else if($total_confirmed > 0){// some classes are confirmed
					  	$Return[$date_string] = PARTIALCONFIRM;
					  }
					  
					  // save the result to cache
					  if ($Return[$date_string] != "") {
						  $InsertValues[] = "(
				  					'".$date_string."',
				  					'".$DayType."',
				  					'".$RecordType."',
				  					'".$Return[$date_string]."'
	  		  					)";
						  $this->db_db_query($sql);
						}
					}
					else {
						$Return[$date_string] = $CacheStatus[$date_string];
					}
				}
				
				if (sizeof($InsertValues) > 0) {
					$sql = "insert into CARD_STUDENT_CACHED_CONFIRM_STATUS (
										TargetDate,
										DayType,
										Type,
										ConfirmStatus) 
									values ".implode(',',$InsertValues);
					//debug_r($sql);
					$this->db_db_query($sql);
				}
			}
			else {
				$sql = "select
								  prr.RecordDate,
								  prr.RecordStatus,
								  count(1) as Total
								from
								  CARD_STUDENT_PROFILE_RECORD_REASON as prr
								  INNER JOIN 
								  INTRANET_USER as u 
								  on 
								  	prr.StudentID = u.UserID 
								  	and 
								  	u.RecordStatus in (0,1,2) 
								where
								  prr.DayType = '".$DayType."'
								  and
								  prr.RecordType = '".$RecordType."' 
								  and prr.RecordDate <= '".$today."' 
								group by
								  prr.RecordDate, prr.RecordStatus";
				$Result = $this->returnArray($sql);
				
				for ($i=0; $i< sizeof($Result); $i++) {
					list($RecordDate,$RecordStatus,$Total) = $Result[$i];
					
					if (trim($RecordStatus) == "") {
						$Temp[$RecordDate]['UnConfirmed'] = true;
					}
					else {
						$Temp[$RecordDate]['Confirmed'] = true;
					}
				}
				
				$Return = array();
				foreach($Temp as $Key => $Var) {
					if ($Var['UnConfirmed'] && $Var['Confirmed']) {
						$Return[$Key] = PARTIALCONFIRM;
					}
					else if ($Var['UnConfirmed']) {
						$Return[$Key] = NONECONFIRM;
					}
					else {
						$Return[$Key] = ALLCONFIRM;
					}
				}
			}

			return $Return;
		}
		
		function Get_Expected_Reason($DayType,$RecordType,$ProfileTablePrefix,$AMProfileTablePrefix="",$RemarkTablePrefix="",$PresetTablePrefix="",$IsOutgoing=false) {
			$ExpectedReason = 'CASE ';
			if ($RecordType == CARD_STATUS_OUTING && $IsOutgoing) {
				$ExpectedReason .= '			WHEN ('.$ProfileTablePrefix.'Reason IS NOT NULL) THEN '.$ProfileTablePrefix.'Reason ';
			}
			else {
				$ExpectedReason .= '			WHEN ('.$ProfileTablePrefix.'RecordStatus IS NOT NULL) THEN '.$ProfileTablePrefix.'Reason 
																	WHEN ('.$ProfileTablePrefix.'Reason IS NOT NULL) THEN '.$ProfileTablePrefix.'Reason ';
			}
			if ($RecordType == PROFILE_TYPE_ABSENT) {
				$ExpectedReason .= '
																	WHEN '.$PresetTablePrefix.'RecordID IS NOT NULL THEN '.$PresetTablePrefix.'Reason ';
				if ($DayType == PROFILE_DAY_TYPE_PM) {
					$ExpectedReason .= '
																	WHEN '.$AMProfileTablePrefix.'RecordID IS NOT NULL and '.$AMProfileTablePrefix.'Reason IS NOT NULL and '.$AMProfileTablePrefix.'Reason != \'\' THEN '.$AMProfileTablePrefix.'Reason 
																';
				}
				$ExpectedReason .= '
																	WHEN '.$RemarkTablePrefix.'Remark IS NOT NULL AND '.$RemarkTablePrefix.'Remark != \'\' THEN '.$RemarkTablePrefix.'Remark 
																	ELSE \''.$this->Get_Safe_Sql_Query($this->DefaultAbsentReason).'\' ';
			}
			else if ($RecordType == CARD_STATUS_OUTING && $IsOutgoing) {
				$ExpectedReason .= '
																	WHEN '.$PresetTablePrefix.'OutingID IS NOT NULL THEN '.$PresetTablePrefix.'Objective ';
				$ExpectedReason .= '
																	WHEN '.$RemarkTablePrefix.'Remark IS NOT NULL AND '.$RemarkTablePrefix.'Remark != \'\' THEN '.$RemarkTablePrefix.'Remark 
																	ELSE IF('.$ProfileTablePrefix.'RecordID IS NULL,\''.$this->Get_Safe_Sql_Query($this->DefaultOutingReason).'\',\'\') ';
			}
			else {
				$DefaultReason = ($RecordType == PROFILE_TYPE_LATE)? $this->DefaultLateReason:$this->DefaultEarlyReason;
					
				$ExpectedReason .= 'ELSE \''.$this->Get_Safe_Sql_Query($DefaultReason).'\' ';
			}
			$ExpectedReason .= '
															END ';
			return $ExpectedReason;
		}
		
		function Get_Preset_Outing_Detail($OutingID) {
			$sql = 'select 
							  OutingID,
							  UserID,
							  RecordDate,
							  DayType,
							  OutTime,
							  BackTime,
							  Location,
							  FromWhere,
							  Objective,
							  PIC,
							  Detail,
							  DateInput,
							  DateModified 
							from 
								CARD_STUDENT_OUTING 
							where 
								OutingID = \''.$OutingID.'\'';
			$Temp = $this->returnArray($sql);
			return $Temp[0];
		}
		
		function Save_Preset_Outing($OutingID,$StudentID,$OutingDate,$DayType,$OutTime,$BackTime,$PIC,$FromWhere,$Location,$Objective,$Detail) {
			$outTimeStr = (trim($OutTime) ==""? "NULL":"'".$OutTime."'");
			$backTimeStr = (trim($BackTime) ==""? "NULL":"'".$BackTime."'");

			if (trim($OutingID) == "") {
				$sql = 'insert into CARD_STUDENT_OUTING (
								  UserID,
								  RecordDate,
								  DayType,
								  OutTime,
								  BackTime,
								  Location,
								  FromWhere,
								  Objective,
								  PIC,
								  Detail,
								  DateInput,
								  DateModified 
								  )
								values';
				for ($j=0; $j< sizeof($StudentID); $j++) {
					for ($i=0; $i< sizeof($DayType); $i++) {
						if ($j > 0 || $i > 0) $sql .= ',';
						$sql .= '
										(
										\''.$StudentID[$j].'\',
										\''.$OutingDate.'\',
										\''.$DayType[$i].'\',
										'.$outTimeStr.',
										'.$backTimeStr.',
										\''.$this->Get_Safe_Sql_Query($Location).'\',
										\''.$this->Get_Safe_Sql_Query($FromWhere).'\',
										\''.$this->Get_Safe_Sql_Query($Objective).'\',
										\''.$this->Get_Safe_Sql_Query($PIC).'\',
										\''.$this->Get_Safe_Sql_Query($Detail).'\',
										NOW(),
										NOW()
										)';
					}
				}
				$sql .= 'on duplicate key update 
									OutTime = '.$outTimeStr.',
								  BackTime = '.$backTimeStr.',
								  Location = \''.$this->Get_Safe_Sql_Query($Location).'\',
								  FromWhere = \''.$this->Get_Safe_Sql_Query($FromWhere).'\',
								  Objective = \''.$this->Get_Safe_Sql_Query($Objective).'\',
								  PIC = \''.$this->Get_Safe_Sql_Query($PIC).'\',
								  Detail = \''.$this->Get_Safe_Sql_Query($Detail).'\',
									DateModified = NOW()
								';
			}
			else {
				$sql = 'UPDATE CARD_STUDENT_OUTING SET
									OutTime = '.$outTimeStr.',
									BackTime = '.$backTimeStr.',
									Location = \''.$this->Get_Safe_Sql_Query($Location).'\',
									Objective = \''.$this->Get_Safe_Sql_Query($Objective).'\',
									FromWhere = \''.$this->Get_Safe_Sql_Query($FromWhere).'\',
									Detail = \''.$this->Get_Safe_Sql_Query($Detail).'\',
									PIC = \''.$this->Get_Safe_Sql_Query($PIC).'\',
									DateModified = now()
								WHERE 
									OutingID = \''.$OutingID.'\'
			         ';
			}
			
			//debug_r($sql);
			return $this->db_db_query($sql);
		}
		
		function Import_Preset_Outing_Record_To_Temp($Data) {
			global $Lang;
			$Error = array();
			$Result = array();
			if(!is_array($Data))
			{
				return array();
			}
			
			$sql = 'DROP TABLE TEMP_IMPORT_STUDENT_OUTING_RECORD';
			$this->db_db_query($sql);
			$sql = 'CREATE TABLE TEMP_IMPORT_STUDENT_OUTING_RECORD (
								UserID int(11) default NULL,
							  RecordDate Date default NULL,
							  DayType int(2) default NULL,
							  PIC varchar(255) default NULL,
							  StartTime Time default NULL,
							  EndTime Time default NULL,
						   	  FromWhere mediumtext,
							  Location mediumtext,
							  Reason mediumtext,
							  Detail mediumtext,
							  UNIQUE KEY (UserID,RecordDate,DayType) 
							) ENGINE=InnoDB DEFAULT CHARSET=utf8;
						 ';
			$this->db_db_query($sql);
			
			$this->Start_Trans();
			$NameField = getNameFieldByLang("a.");
			$values_array = array();
			for($i=0;$i<sizeof($Data);$i++)
			{
				list($ClassName,$ClassNumber,$RecordDate,$DayType,$PIC,$StartTime,$EndTime,$LeavesAt,$Location,$Reason,$Remark) = $Data[$i];
				$ClassName = trim($ClassName);
				$ClassNumber = trim($ClassNumber);
				$RecordDate = (trim($RecordDate) == "")? date('Y-m-d'):trim($RecordDate);
				$Data[$i][2] = $RecordDate;
				$StartTime = (trim($StartTime) == "")? "NULL":"'".$StartTime."'";
				$EndTime = (trim($EndTime) == "")? "NULL":"'".$EndTime."'";
				$LeavesAt = trim($LeavesAt);
				$Location = trim($Location);
				$Reason = trim($Reason);
				$Remark = trim($Remark);
				
				if (trim($DayType) == "" && $ClassName == "" && $ClassNumber == "") { // MUST field missing
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['PresetOutMUSTFileMissed']);
					continue;
				}
				
				if ($DayType != "AM" && $DayType != "PM") { // Invalid Time slot 
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['PresetOutTimeSlotMissMatch']);
					continue;
				}
				else {
					$DayType = ($DayType == "AM")? PROFILE_DAY_TYPE_AM:PROFILE_DAY_TYPE_PM;
				}
				
				if (strtotime($RecordDate) == -1 || strtotime($RecordDate) === FALSE) { // Invalid Target Date
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['General']['InvalidDateFormat']);
					continue;
				}
					
				$sql = "SELECT
							a.UserID
						FROM
							INTRANET_USER as a
						WHERE
							a.RecordType = '2'
					  	AND a.RecordStatus = '1'
							AND a.ClassName = '".$this->Get_Safe_Sql_Query($ClassName)."' 
							AND a.ClassNumber = '".$this->Get_Safe_Sql_Query($ClassNumber)."' ";
				$UserInfo = $this->returnArray($sql);
				if(sizeof($UserInfo) == 0) // no match found
				{
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['StudentNotFound']);
					continue;
				}
				else if (sizeof($UserInfo) > 1) { // more than one match
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['ImportMoreThanOneStudentFound']);
					continue;
				}
				else { // only one match
					$TargetUserID = $UserInfo[0]['UserID'];
				}
				
				if ($PIC != "") { // if entered PIC field
					// check PIC exist
					$PICNameField = getNameFieldByLang();
					$sql = "select ".$PICNameField." from INTRANET_USER where UserLogin = '".$PIC."'";
					$PICInfo = $this->returnVector($sql);
					
					if (sizeof($PICInfo) == 0) {  
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['PresetOutingPICNotFound']);
						continue;
					}
					else {
						$PICName = $PICInfo[0];
					}
				}
							
				// check same user, date session have preset already
				$sql = "select count(1) from CARD_STUDENT_OUTING where UserID = '".$TargetUserID."' and RecordDate = '".$RecordDate."' and DayType = '".$DayType."'";
				//debug_r($sql);
				$Temp = $this->returnVector($sql);							
				if ($Temp[0] > 0) { // preset already
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['PresetOutingSetPreviously']);
					continue;
				}

				// same user, date session have appears more than once in the import file
				$sql = "select count(1) from TEMP_IMPORT_STUDENT_OUTING_RECORD where UserID = '".$TargetUserID."' and RecordDate = '".$RecordDate."' and DayType = '".$DayType."'";
				$Temp = $this->returnVector($sql);
				if ($Temp[0] > 0) { // preset already in same file
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=> $Lang['StudentAttendance']['PresetOutingSetOnSameFile']);
					continue;
				}
				
				// Import Temp
				$sql = 'insert into TEMP_IMPORT_STUDENT_OUTING_RECORD (
								  UserID,
								  RecordDate,
								  DayType,
								  PIC,
								  StartTime,
								  EndTime,
								  FromWhere,
								  Location,
								  Reason,
								  Detail  
									)
								values (
									\''.$TargetUserID.'\',
									\''.$RecordDate.'\',
									\''.$DayType.'\',
									\''.$PICName.'\',
									'.$StartTime.',
									'.$EndTime.', 
									\''.$this->Get_Safe_Sql_Query($LeavesAt).'\',
									\''.$this->Get_Safe_Sql_Query($Location).'\',
									\''.$this->Get_Safe_Sql_Query($Reason).'\',
									\''.$this->Get_Safe_Sql_Query($Remark).'\'
									)';
				//debug_r($sql);
				$Result['InsertTempRecord:'.$TargetUserID] = $this->db_db_query($sql);
			}
			
			//debug_r($Result);
			if (in_array(false,$Result)) {
				$this->RollBack_Trans();
			}
			else {
				$this->Commit_Trans();
			}
			
			return $Error;
		}
		
		function Finalize_Import_Preset_Outing() {
			# Insert to Outing table
      $sql = "INSERT INTO CARD_STUDENT_OUTING (
    					UserID, 
    					PIC, 
    					DayType,
    					RecordDate, 
    					OutTime, 
    					BackTime, 
						Location,
						FromWhere,
    					Objective, 
						Detail,
    					DateInput, 
    					DateModified)
             SELECT
              		a.UserID,
              		a.PIC,
              		a.DayType,
					a.RecordDate,
					a.StartTime,
					a.EndTime,
					a.Location,
					a.FromWhere,
				 	a.Reason,
					a.Detail, 
              		now(), 
              		now()
             FROM 
             	TEMP_IMPORT_STUDENT_OUTING_RECORD as a
						";
    	$Result = $this->db_db_query($sql);
    	
    	if ($Result) {
    		$sql = 'select count(1) from TEMP_IMPORT_STUDENT_OUTING_RECORD';
    		$Temp = $this->returnVector($sql);
    		
    		$sql = 'drop table TEMP_IMPORT_STUDENT_OUTING_RECORD';
    		$this->db_db_query($sql);
    		
    		return $Temp[0];
    	}
    	else {
    		return 0;
    	}
		}
		
		function Record_Raw_Log($StudentID,$current_time="",$location="") {
			# Get Record
			# Create Monthly data table
			$month = date('m',$current_time);
			$year = date('Y',$current_time);
			$day = date('d',$current_time);
			$time_string = date('H:i:s',$current_time);
		   	$this->createTable_Card_Student_Entry_Log($year, $month);
		   	$entrylog_tablename = "CARD_STUDENT_ENTRY_LOG_".$year."_".$month;
		   	if($location != ""){
		   		$this->db_db_query("INSERT INTO CARD_STUDENT_TAP_CARD_LOCATION (Location) VALUES ('".$this->Get_Safe_Sql_Query($location)."')");
		   	}
		   	
		   	$sql = "INSERT INTO $entrylog_tablename (UserID, DayNumber, RecordTime, RecordStation, DateInput ,DateModified)
	      				VALUES ('$StudentID', '$day', '$time_string', '".$this->Get_Safe_Sql_Query($location)."', now(), now())";
	        return $this->db_db_query($sql);
		}
		
		function Get_Class_List_By_Attendance_Mode($Mode, $ClassID="") {
			$sql = "SELECT 
								a.YearClassID as ID, 
								y.YearID,
								y.YearName,
								a.ClassTitleEN as Title
							FROM 
								YEAR as y 
								inner join 
								YEAR_CLASS as a 
								on y.YearID = a.YearID 
									and a.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
			          LEFT OUTER JOIN 
			          CARD_STUDENT_CLASS_SPECIFIC_MODE as b 
			          ON a.YearClassID = b.ClassID
			       	WHERE 
			       		b.Mode = '".$Mode."' 
			       		AND 
			       		a.YearClassID != '".$ClassID."' 
			       	ORDER BY 
			       		y.sequence, a.sequence";
			//debug_r($sql);
			return $this->returnArray($sql,4);
		}
		
		function Get_Group_List_By_Attendance_Mode($Mode, $GroupID="") {
			$sql = "select 
								a.groupid as ID, 
								a.title as Title 
							from 
								INTRANET_GROUP as a 
								left join 
								YEAR_CLASS as b 
								on a.groupid = b.groupid
								left join 
								CARD_STUDENT_ATTENDANCE_GROUP c 
								on a.groupid = c.groupid
							where 
								a.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
								and 
								b.groupid is null 
								and 
								a.recordtype='3' 
								and 
								a.GroupID != '".$GroupID."' 
								and 
								c.mode = '".$Mode."'
							order by a.title";
			return $this->returnArray($sql,3);
		}
		
		function Get_No_Tapping_Card_Report_Data($StartDate,$EndDate,$TargetType,$OrderBy,$CheckField,$Details="",$ClassName=array()) {
			# get months involved
			$start_ts = strtotime($StartDate);
			$end_ts = strtotime($EndDate);
			$first_month = date('m',$start_ts);
			$first_year = date('Y',$start_ts);
			$first_day = date('d',$start_ts);
			$end_month = date('m', $end_ts);
			$end_year = date('Y',$end_ts);
			$end_day = date('d', $end_ts);
			
			for($i=0;$i<sizeof($ClassName);$i++)
				$selected_classes.="'".$this->Get_Safe_Sql_Query($ClassName[$i])."',";
			if($selected_classes!="")
				$selected_classes=substr($selected_classes,0,-1);
				
			# Create temp table
			$sql = "
			CREATE TEMPORARY TABLE TEMP_NO_TAB_TIME (
				UserID int(11),
				ClassName varchar(255) default '',
				RecordDate date,
				RecordType char(5),
				InSchoolTime time default null,
				LunchOutTime time default null,
				LunchBackTime time default null,
				LeaveSchoolTime time default null 
			) ENGINE=InnoDB CHARSET=utf8";
			$this->db_db_query($sql);
			
			//$sql = "DELETE FROM TEMP_NO_TAB_TIME";
			//$this->db_db_query($sql);
			
			if ($first_month == $end_month && $first_year == $end_year) # special, only get 1 month table
			{
			  $this->createTable_Card_Student_Raw_Log($first_year, $first_month);
			  $tablename = "CARD_STUDENT_DAILY_LOG_".$first_year."_".$first_month;
			  $rawlog_tablename = "CARD_STUDENT_RAW_LOG_".$first_year."_".$first_month;
			  for ($i=0; $i<sizeof($CheckField); $i++) {
			  	List($TimeField,$StatusField,$Type) = $CheckField[$i];
			  	$sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType, InSchoolTime, LunchOutTime, LunchBackTime, LeaveSchoolTime)
			            SELECT 
			            	a.UserID, 
			              IF(b.ClassName IS NULL,'',b.ClassName), 
			              DATE_ADD('".$first_year."-".$first_month."-01',INTERVAL (a.DayNumber-1) DAY),
			              '".$Type."',
						  a.InSchoolTime,
						  a.LunchOutTime,
						  a.LunchBackTime,
						  a.LeaveSchoolTime 
			            FROM 
			            	$tablename as a ";
					if ($this->EnableEntryLeavePeriod) {
						$sql .= "
										INNER JOIN 
										CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
										on 
											a.UserID = selp.UserID 
											and 
											DATE(CONCAT('".$first_year."-".$first_month."-',a.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
										";
					}
					$sql .= "
			            	LEFT OUTER JOIN 
			            	INTRANET_USER as b 
			            	ON a.UserID = b.UserID AND b.RecordType = 2 
							LEFT JOIN $rawlog_tablename as r ON r.UserID=a.UserID AND r.DayNumber=a.DayNumber AND r.RecordTime=a.".$TimeField." 
			            WHERE 
			            	(a.DayNumber >= '".$first_day."' AND a.DayNumber <= '".$end_day."') 
			            	 AND r.RecordID IS NULL ";
			    if (is_array($StatusField) && count($StatusField)>0){
			    	$sql .= " AND (";
			    	$tmp_field_arr = array();
			    	foreach($StatusField as $tmp_field){
			    		$tmp_field_arr[] = " a.".$tmp_field." IN (0,2) ";
			    	}
			    	$sql .= implode(" OR ",$tmp_field_arr);
			    	$sql .= ") ";
			    	if($TimeField == 'LeaveSchoolTime'){
			    		foreach($StatusField as $tmp_field){
			    			$sql .= " AND (a.".$tmp_field." NOT IN (1,3) OR a.".$tmp_field." IS NULL) ";// exclude absent and outing
			    		}
			    	}
			    }else if ($StatusField != "") {
			    	$sql .= "
			    					AND 
			              a.".$StatusField." IN (0,2) 
			              ";
			        if($TimeField == 'LeaveSchoolTime'){
			    		$sql .= " AND (a.".$StatusField." NOT IN (1,3) OR a.".$StatusField." IS NULL) ";
			    	}
			    }
			    
			    if ($selected_classes != "") {
						$sql .= " 
										AND
										b.ClassName in (".$selected_classes.") 
		                ";
		      }
				//debug_r($sql);
			    $this->db_db_query($sql);
			  }
			}
			else
			{
			  $this->createTable_Card_Student_Raw_Log($first_year, $first_month);
			  # Grab first month data
			  $tablename = "CARD_STUDENT_DAILY_LOG_".$first_year."_".$first_month;
			  $rawlog_tablename = "CARD_STUDENT_RAW_LOG_".$first_year."_".$first_month;
			  for ($i=0; $i<sizeof($CheckField); $i++) {
			  	List($TimeField,$StatusField,$Type) = $CheckField[$i];
			  	$sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType, InSchoolTime, LunchOutTime, LunchBackTime, LeaveSchoolTime)
			            SELECT 
			            	a.UserID, 
			              IF(b.ClassName IS NULL,'',b.ClassName), 
			              DATE_ADD('".$first_year."-".$first_month."-01',INTERVAL (a.DayNumber-1) DAY),
			              '".$Type."',
						  a.InSchoolTime,
						  a.LunchOutTime,
						  a.LunchBackTime,
						  a.LeaveSchoolTime 
			            FROM 
			            	$tablename as a ";
					if ($this->EnableEntryLeavePeriod) {
						$sql .= "
										INNER JOIN 
										CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
										on 
											a.UserID = selp.UserID 
											and 
											DATE(CONCAT('".$first_year."-".$first_month."-',a.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
										";
					}
					$sql .= "
			            	LEFT OUTER JOIN 
			            	INTRANET_USER as b 
			            	ON a.UserID = b.UserID AND b.RecordType = 2 
							LEFT JOIN $rawlog_tablename as r ON r.UserID=a.UserID AND r.DayNumber=a.DayNumber AND r.RecordTime=a.".$TimeField." 
			            WHERE 
			            	(a.DayNumber >= '".$first_day."')
			            	 AND r.RecordID IS NULL ";
			    if (is_array($StatusField) && count($StatusField)>0){
			    	$sql .= " AND (";
			    	$tmp_field_arr = array();
			    	foreach($StatusField as $tmp_field){
			    		$tmp_field_arr[] = " a.".$tmp_field." IN (0,2) ";
			    	}
			    	$sql .= implode(" OR ",$tmp_field_arr);
			    	$sql .= ") ";
			    	if($TimeField == 'LeaveSchoolTime'){
			    		foreach($StatusField as $tmp_field){
			    			$sql .= " AND (a.".$tmp_field." NOT IN (1,3) OR a.".$tmp_field." IS NULL) ";
			    		}
			    	}
			    }else if ($StatusField != "") {
			    	$sql .= "
			    					AND 
			              a.".$StatusField." IN (0,2) 
			              ";
			        if($TimeField == 'LeaveSchoolTime'){
			    		$sql .= " AND (a.".$StatusField." NOT IN (1,3) OR a.".$StatusField." IS NULL) ";
			    	}
			    }
			    if ($selected_classes != "") {
						$sql .= " 
										AND
										b.ClassName in (".$selected_classes.") 
		                ";
		      }
			
			    $this->db_db_query($sql);
			  }

			  # Grab all required year / month pair
			  if ($first_month != "12")
			  {
			      $curr_year = $first_year;
			      $curr_month = $first_month+1;
			  }
			  else
			  {
			      $curr_year = $first_year+1;
			      $curr_month = 1;
			  }
			  while ($curr_year < $end_year || ($curr_year==$end_year && $curr_month < $end_month))
			  {
					$curr_month_str = ($curr_month<10?"0".$curr_month:$curr_month);
					# Grab data
					$this->createTable_Card_Student_Raw_Log($curr_year, $curr_month_str);
					$tablename = "CARD_STUDENT_DAILY_LOG_".$curr_year."_".$curr_month_str;
					$rawlog_tablename = "CARD_STUDENT_RAW_LOG_".$curr_year."_".$curr_month_str;
					
					for ($i=0; $i<sizeof($CheckField); $i++) {
				  	List($TimeField,$StatusField,$Type) = $CheckField[$i];
				  	$sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType, InSchoolTime, LunchOutTime, LunchBackTime, LeaveSchoolTime)
				            SELECT 
				            	a.UserID, 
				              IF(b.ClassName IS NULL,'',b.ClassName), 
				              DATE_ADD('".$curr_year."-".$curr_month_str."-01',INTERVAL (a.DayNumber-1) DAY),
				              '".$Type."',
							  a.InSchoolTime,
						  	  a.LunchOutTime,
						      a.LunchBackTime,
						      a.LeaveSchoolTime 
				            FROM 
				            	$tablename as a ";
						if ($this->EnableEntryLeavePeriod) {
							$sql .= "
											INNER JOIN 
											CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
											on 
												a.UserID = selp.UserID 
												and 
												DATE(CONCAT('".$curr_year."-".$curr_month_str."-',a.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
											";
						}
						$sql .= "
				            	LEFT OUTER JOIN 
				            	INTRANET_USER as b 
				            	ON a.UserID = b.UserID AND b.RecordType = 2 
								LEFT JOIN $rawlog_tablename as r ON r.UserID=a.UserID AND r.DayNumber=a.DayNumber AND r.RecordTime=a.".$TimeField." 
				            WHERE 
				              r.RecordID IS NULL ";
				    if (is_array($StatusField) && count($StatusField)>0){
				    	$sql .= " AND (";
				    	$tmp_field_arr = array();
				    	foreach($StatusField as $tmp_field){
				    		$tmp_field_arr[] = " a.".$tmp_field." IN (0,2) ";
				    	}
				    	$sql .= implode(" OR ",$tmp_field_arr);
				    	$sql .= ") ";
				    	if($TimeField == 'LeaveSchoolTime'){
				    		foreach($StatusField as $tmp_field){
				    			$sql .= " AND (a.".$tmp_field." NOT IN (1,3) OR a.".$tmp_field." IS NULL) ";
				    		}
				    	}
			    	}else if ($StatusField != "") {
				    	$sql .= " AND 
				              	a.".$StatusField." IN (0,2) 
				              ";
				        if($TimeField == 'LeaveSchoolTime'){
				    		$sql .= " AND (a.".$StatusField." NOT IN (1,3) OR a.".$StatusField." IS NULL) ";
				    	}
				    }
				    if ($selected_classes != "") {
						$sql .= " 
										AND
										b.ClassName in (".$selected_classes.") 
		                ";
		      }
				              
				    $this->db_db_query($sql);
				  }
			
			    if ($curr_month != "12")
			    {
			      $curr_year = $curr_year;
			      $curr_month = $curr_month+1;
			    }
			    else
			    {
			      $curr_year = $curr_year+1;
			      $curr_month = 1;
			    }
			  }
			
			  # Grab last month data
			  $this->createTable_Card_Student_Raw_Log($end_year, $end_month);
			  $tablename = "CARD_STUDENT_DAILY_LOG_".$end_year."_".$end_month;
			  $rawlog_tablename = "CARD_STUDENT_RAW_LOG_".$end_year."_".$end_month;
			  for ($i=0; $i<sizeof($CheckField); $i++) {
			  	List($TimeField,$StatusField,$Type) = $CheckField[$i];
			  	$sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType, InSchoolTime, LunchOutTime, LunchBackTime, LeaveSchoolTime)
			            SELECT 
			            	a.UserID, 
			              IF(b.ClassName IS NULL,'',b.ClassName), 
			              DATE_ADD('".$end_year."-".$end_month."-01',INTERVAL (a.DayNumber-1) DAY),
			              '".$Type."',
						  a.InSchoolTime,
						  a.LunchOutTime,
						  a.LunchBackTime,
						  a.LeaveSchoolTime 
			            FROM 
			            	$tablename as a ";
					if ($this->EnableEntryLeavePeriod) {
						$sql .= "
										INNER JOIN 
										CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
										on 
											a.UserID = selp.UserID 
											and 
											DATE(CONCAT('".$end_year."-".$end_month."-',a.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
										";
					}
					$sql .= "
			            	LEFT OUTER JOIN 
			            	INTRANET_USER as b 
			            	ON a.UserID = b.UserID AND b.RecordType = 2 
							LEFT JOIN $rawlog_tablename as r ON r.UserID=a.UserID AND r.DayNumber=a.DayNumber AND r.RecordTime=a.".$TimeField." 
			            WHERE 
			            	(a.DayNumber <= '$end_day') 
			            	 AND r.RecordID IS NULL ";
			    if (is_array($StatusField) && count($StatusField)>0){
			    	$sql .= " AND (";
			    	$tmp_field_arr = array();
			    	foreach($StatusField as $tmp_field){
			    		$tmp_field_arr[] = " a.".$tmp_field." IN (0,2) ";
			    	}
			    	$sql .= implode(" OR ",$tmp_field_arr);
			    	$sql .= ") ";
			    	if($TimeField == 'LeaveSchoolTime'){
			    		foreach($StatusField as $tmp_field){
			    			$sql .= " AND (a.".$tmp_field." NOT IN (1,3) OR a.".$tmp_field." IS NULL) ";
			    		}
			    	}
			    }else if ($StatusField != "") {
			    	$sql .= " AND 
			              		a.".$StatusField." IN (0,2) 
			              	";
			        if($TimeField == 'LeaveSchoolTime'){
			    		$sql .= " AND (a.".$StatusField." NOT IN (1,3) OR a.".$StatusField." IS NULL) ";
			    	}
			    }
			    if ($selected_classes != "") {
						$sql .= " 
										AND
										b.ClassName in (".$selected_classes.") 
		                ";
		      }
					
			    $this->db_db_query($sql);
			  }
			}
			
			# Find bad tap card record that forgot to bring card and use manual input, these records are really no tap card records
			$sql = "SELECT Student,RecordTime FROM CARD_STUDENT_BAD_ACTION WHERE RecordType IN ('".CARD_BADACTION_NO_CARD_ENTRANCE."','".CARD_BADACTION_FAKED_CARD_AM."','".CARD_BADACTION_FAKED_CARD_PM."') AND RecordDate BETWEEN '$StartDate' AND '$EndDate' ORDER BY StudentID,RecordTime";
			$bad_card_records = $this->returnResultSet($sql);
			$bad_card_record_size = count($bad_card_records);
			$studentIdDatetimeToRecord = array();
			for($i=0;$i<$bad_card_record_size;$i++){
				$tmp_student_id = $bad_card_records[$i]['StudentID'];
				$tmp_record_time = $bad_card_records[$i]['RecordTime'];
				if(!isset($studentIdDatetimeToRecord[$tmp_student_id])){
					$studentIdDatetimeToRecord[$tmp_student_id] = array();
				}
				$studentIdDatetimeToRecord[$tmp_student_id][$tmp_record_time] = $bad_card_records[$i];				
			}
			
			$CheckTimeFields = Get_Array_By_Key($CheckField,0);
			$typeToTimeField = array('AM'=>'InSchoolTime','Lunch'=>'LunchOutTime','PM'=>'LunchBackTime','Out'=>'LeaveSchoolTime');

			$GeneralSetting = new libgeneralsettings();
			$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');
			$lunch_misc_all_allowed = $Settings['AllAllowGoOut'];

			$sql = "SELECT * FROM TEMP_NO_TAB_TIME ORDER BY UserID,RecordDate";
			$temp_records = $this->returnResultSet($sql);
			$temp_record_size = count($temp_records);
			for($i=0;$i<$temp_record_size;$i++){
				$tmp_student_id = $temp_records[$i]['UserID'];

				$FinalSetting = $this->Get_Student_Attend_Time_Setting($temp_records[$i]['RecordDate'],$tmp_student_id);
				if($FinalSetting == 'NonSchoolDay') {
					$sql = "DELETE FROM TEMP_NO_TAB_TIME WHERE UserID='$tmp_student_id' AND RecordDate='".$temp_records[$i]['RecordDate']."'";
					$this->db_db_query($sql);
					continue;
				}

				$should_keep = false;
				/*
				foreach($CheckTimeFields as $CheckTimeField){
					if($temp_records[$i][$CheckTimeField]==''){
						// no tap card time on specific time field, keep it
						$should_keep = true;
					}
				}*/
				$CheckTimeField = $typeToTimeField[$temp_records[$i]['RecordType']];
				if($temp_records[$i][$CheckTimeField]==''){
					// no tap card time on specific time field, keep it
					$should_keep = true;
				}

				$allow_to_go_out_for_lunch = $lunch_misc_all_allowed;
				if(!$lunch_misc_all_allowed){
					$sql = "SELECT StudentID FROM CARD_STUDENT_LUNCH_ALLOW_LIST WHERE StudentID = '$tmp_student_id'";
					$temp = $this->returnVector($sql);
					$allow_to_go_out_for_lunch = $temp[0] != '';
				}

				if(!$allow_to_go_out_for_lunch && $temp_records[$i]['RecordType']=='Lunch') {
					$sql = "DELETE FROM TEMP_NO_TAB_TIME WHERE UserID='$tmp_student_id' AND RecordDate='".$temp_records[$i]['RecordDate']."' AND RecordType='".$temp_records[$i]['RecordType']."'";
					$this->db_db_query($sql);
					continue;
				}

				if($should_keep){
					continue;
				}
				//if($temp_records[$i]['InSchoolTime']=='' && $temp_records[$i]['LunchOutTime']=='' && $temp_records[$i]['LunchBackTime']=='' && $temp_records[$i]['LeaveSchoolTime']==''){
					// really no tap card times, keep it
					//continue;
				//}
				// if has bad tap card record time, the record should be really treated as no tap card record
				if(isset($studentIdDatetimeToRecord[$tmp_student_id]) && 
				  (($temp_records[$i]['InSchoolTime'] != '' && $temp_records[$i]['RecordType']=='AM' && isset($studentIdDatetimeToRecord[$tmp_student_id][$temp_records[$i]['InSchoolTime']])) 
				  || ($temp_records[$i]['LunchOutTime'] != '' && $temp_records[$i]['RecordType']=='Lunch' && isset($studentIdDatetimeToRecord[$tmp_student_id][$temp_records[$i]['LunchOutTime']])) 
				  || ($temp_records[$i]['LunchBackTime'] != '' && $temp_records[$i]['RecordType']=='PM' && isset($studentIdDatetimeToRecord[$tmp_student_id][$temp_records[$i]['LunchBackTime']])) 
				  || ($temp_records[$i]['LeaveSchoolTime'] != '' && $temp_records[$i]['RecordType']=='Out' && isset($studentIdDatetimeToRecord[$tmp_student_id][$temp_records[$i]['LeaveSchoolTime']])))
				){
					// keep the record
				}else{
					// otherwise remove it, the record maybe offline imported, do not count as no tap card
					$sql = "DELETE FROM TEMP_NO_TAB_TIME WHERE UserID='$tmp_student_id' AND RecordDate='".$temp_records[$i]['RecordDate']."' AND RecordType='".$temp_records[$i]['RecordType']."'";
					$this->db_db_query($sql);
				}
			}
			
			# Iterate temp table
			if ($TargetType=="Class")
			{
			  if ($OrderBy=="name")
			  {
			    $order_by_ext = "ClassName ASC";
			  }
			  else           # Count assumed
			  {
			    $order_by_ext = "num DESC";
			  }
			  $sql = "SELECT ClassName, COUNT(*) as num FROM TEMP_NO_TAB_TIME GROUP BY ClassName ORDER BY $order_by_ext";
			  $result = $this->returnArray($sql,2);
			  
			  return $result;
			}
			else if ($TargetType=="Student")
			{
				if ($OrderBy=="name")
				{
				  $order_by_ext = "b.ClassName, b.ClassNumber+0, b.EnglishName ASC";
				}
				else           # Count assumed
				{
				  $order_by_ext = "num DESC";
				}
				$namefield = getNameFieldByLang("b.");
				$sql = "SELECT a.UserID,$namefield,b.ClassName,b.ClassNumber, COUNT(*) as num 
								FROM 
									TEMP_NO_TAB_TIME as a
				          LEFT OUTER JOIN 
				          INTRANET_USER as b 
				          ON a.UserID = b.UserID AND b.RecordType = 2
				        GROUP BY a.UserID ORDER BY $order_by_ext";
				$result = $this->returnArray($sql,5);
				
				if ($Details==1)         # Grab raw date record
				{
					$sql = "SELECT UserID, RecordDate, RecordType FROM TEMP_NO_TAB_TIME";
					$temp = $this->returnArray($sql,3);
					$records = array();
					
					# Build assoc array
					for ($i=0; $i<sizeof($temp); $i++)
					{
						list($t_userID, $t_recordDate, $t_recordType) = $temp[$i];
						$records[$t_userID][$t_recordType][] .= $t_recordDate;
					}
				}

				$count_total = 0;

				return array($result,$records);
			}
		}
		
		function Get_Reason_Symbol($RecordType) {
			$sql = "SELECT 
								Reason, StatusSymbol 
							FROM 
								CARD_STUDENT_ATTENDANCE_REASON_SYMBOL 
							WHERE 
								ReasonType='".$RecordType."' 
							ORDER BY LastModified";
			return $this->returnArray($sql);
		}
		
		function TemplateVariable($CategoryID='')
		{
			global $intranet_root,$intranet_session_language, $Lang;

			# set default value
			$DefaultValue = $i_Discipline_System_Template_Variable['default_value'];
			$data = array();

			# [variable symbol][variable name][default value]
			# Common variables
			$data['StudentName'] = array($Lang['StudentAttendance']['StudentName'], $DefaultValue);
			$data['ClassName'] = array($Lang['StudentAttendance']['ClassName'], $DefaultValue);
			$data['ClassNumber'] = array($Lang['StudentAttendance']['ClassNumber'], $DefaultValue);
			$data['AdditionalInfo'] = array($Lang['StudentAttendance']['AdditionInfo'], $DefaultValue);

			# Category variables
			switch($CategoryID)
			{
				case "Absent":
					$data['RecordDate'] = array($Lang['StudentAttendance']['RecordDate'], date("Y-m-d"));
					$data['Session'] = array($Lang['StudentAttendance']['TimeSlot'], $DefaultValue);
					$data['Reason'] = array($Lang['StudentAttendance']['Reason'], $DefaultValue);
					break;
				case "CumulativeProfile":
					$data['FromDate'] = array($Lang['StudentAttendance']['FromDate'], date("Y-m-d"));
					$data['ToDate'] = array($Lang['StudentAttendance']['ToDate'], date("Y-m-d"));
					$data['TotalCount'] = array($Lang['StudentAttendance']['TotalCount'], $DefaultValue);
					$data['AttendanceType'] = array($Lang['StudentAttendance']['AttendanceType'], $DefaultValue);
					break;
				default:
					break;
			}

			return $data;
		}
		
		function Update_Template($CategoryID,$Title,$Content,$Subject,$RecordStatus,$ReplySlip,$ReplySlipContent,$TemplateID="", $SendReplySlip=0) {
			
			if ($TemplateID != "") {
				$sql = "update INTRANET_NOTICE_MODULE_TEMPLATE set 
									CategoryID = '".$CategoryID."', 
									Title = '".$Title."',
									Content = '".$Content."',
									Subject = '".$Subject."',
									RecordStatus = '".$RecordStatus."',
									ReplySlip = '".$ReplySlip."',
									SendReplySlip = '".$SendReplySlip."',
									DateModified = now(), 
									ReplySlipContent = '".$ReplySlipContent."' 
								where 
									TemplateID = '".$TemplateID."'";
			}
			else {
				$fields = "Module,CategoryID,Title,Content,Subject,RecordStatus,ReplySlip,DateInput,DateModified, ReplySlipContent, SendReplySlip";
				$InsertValue = "'StudentAttendance', 
												'".$CategoryID."',
												'".$Title."',
												'".$Content."',
												'".$Subject."',
												'".$RecordStatus."',
												'".$ReplySlip."',
												now(),now(), 
												'".$ReplySlipContent."',
												'".$SendReplySlip."'";
				$sql = "INSERT INTO INTRANET_NOTICE_MODULE_TEMPLATE (".$fields.") VALUES (".$InsertValue.")";
			}
			
			//debug_r($sql);
			return $this->db_db_query($sql);
		}
		
		function Get_Template_Detail($TemplateID) {
			$sql = "select 
								Module,
								CategoryID,
								Title,
								Subject,
								Content,
								ReplySlip,
								RecordType,
								RecordStatus,
								ReplySlipContent,
								SendReplySlip
							from 
								INTRANET_NOTICE_MODULE_TEMPLATE 
							where 
								TemplateID = '".$TemplateID."'";
			return $this->returnArray($sql);
		}
		
		function Get_eNotice_Template_By_Category($Category="Absent") {
			$sql = "select 
								TemplateID,
								Title 
							from 
								INTRANET_NOTICE_MODULE_TEMPLATE 
							where 
								Module = 'StudentAttendance' AND RecordStatus=1
								AND 
								CategoryID = '".$Category."'";
			return $this->returnArray($sql);
		}
		
		function Get_UserInfo_By_ID($UserList) {
			$NameField = getNameFieldByLang("");
			$sql = "select 
								UserID,
								".$NameField." as Name, 
								ClassName,
								ClassNumber
							from 
								INTRANET_USER 
							where 
								UserID in ('".implode("','",$UserList)."') 
							order by 
								ClassName, ClassNumber+0";
			return $this->returnArray($sql);
		}
		
		function Get_Student_Profile_Info($StudentID,$TargetDate,$DayType,$RecordType) {
			$NameField = getNameFieldByLang("u.");
			$sql = "select 
								RecordID,
								".$NameField." as Name,
								u.ClassName,
								u.ClassNumber, 
								prr.Reason 
							from 
								CARD_STUDENT_PROFILE_RECORD_REASON as prr 
								INNER JOIN 
								INTRANET_USER as u
								on prr.RecordDate = '".$TargetDate."' 
									AND 
									prr.DayType = '".$DayType."' 
									AND 
									prr.RecordType = '".$RecordType."' 
									AND 
									prr.StudentID = u.UserID 
									and 
									u.UserID = '".$StudentID."'";
			//debug_r($sql);
			$Result = $this->returnArray($sql);
			return $Result[0];
		}
		
		function Set_Notice_ID_To_Profile($RecordID,$NoticeID) {
			$sql = "update CARD_STUDENT_PROFILE_RECORD_REASON set 
								eNoticeID = '".$NoticeID."' 
							where 
								RecordID = '".$RecordID."' ";
			
			return $this->db_db_query($sql);
		}
		
		function Get_Student_By_Classes($ClassName=array()) {
			$StudentList = array();
			if (sizeof($ClassName) > 0) {
				$namefield = getNameFieldWithClassNumberByLang();
				$sql = "select 
									UserID,
									".$namefield." as name,
									ClassName 
								from 
									INTRANET_USER 
								where 
									ClassName in ('".implode("','",$this->Get_Safe_Sql_Query($ClassName))."') 
									and RecordType='2' 
									and RecordStatus IN (0,1,2) 
								order by 
									ClassName, ClassNumber+0";
				$StudentList = $this->returnArray($sql);
				//debug_r($sql);
			}
			
			return $StudentList;
		}
		
		function Get_Search_Report_Data($startDate,$endDate,$session,$attendance_type,$match,$reason,$waived,$class_name,$StudentList) {
			global $PATH_WRT_ROOT, $sys_custom;
			
			$selected_classes = implode("','",$this->Get_Safe_Sql_Query($class_name));
			$selected_classes = "'".$selected_classes."'";
			$classes = explode(",",$selected_classes);
			
			if ($StudentList > 0) {
				$selected_students = "'".implode("','",IntegerSafe($StudentList))."'";
				$StudentCondition = "AND a.UserID in ($selected_students)";
			}
					
			$cond_day_type="";
			$username_field = getNameFieldByLang('a.');
			if($session==PROFILE_DAY_TYPE_AM){  # AM
				$cond_day_type = " AND c.DayType='".PROFILE_DAY_TYPE_AM."' ";
				if($attendance_type==CARD_STATUS_LATE){ # Late
					$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."' ";
				}
				else if($attendance_type==CARD_STATUS_ABSENT){ # Absent
					$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."' AND b.AMStatus='".CARD_STATUS_ABSENT."' ";
				}else if($attendance_type==(CARD_STATUS_OUTING+PROFILE_TYPE_ABSENT)) // Outing
				{
					$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."' AND b.AMStatus='".CARD_STATUS_OUTING."' ";
				}
				else if($attendance_type==PROFILE_TYPE_EARLY){ # Early leave
					$cond_day_type.=" AND c.RecordType='".PROFILE_TYPE_EARLY."' ";
				}
				else{ # All ( Late or Absent or Early leave)
					$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."' OR c.RecordType='".PROFILE_TYPE_EARLY."') ";
				}
			}
			else if($session==PROFILE_DAY_TYPE_PM){ # PM
				$cond_day_type = " AND c.DayType='".PROFILE_DAY_TYPE_PM."' ";
				if($attendance_type==CARD_STATUS_LATE){  #Late
					$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."' ";
				}
				else if($attendance_type==CARD_STATUS_ABSENT){  # Absent
					$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."' AND b.PMStatus='".CARD_STATUS_ABSENT."' ";
				}else if($attendance_type==(CARD_STATUS_OUTING+PROFILE_TYPE_ABSENT)){ # Outing
					$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."' AND b.PMStatus='".CARD_STATUS_OUTING."' ";
				}
				else if($attendance_type==PROFILE_TYPE_EARLY){  # Early leave
					$cond_day_type.=" AND c.RecordType='".PROFILE_TYPE_EARLY."' ";
				}
				else{  # ALL(Late or Absent or Early leave)
					$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."' OR c.RecordType='".PROFILE_TYPE_EARLY."') ";
				}
			}
			else{  # ALL (AM or PM)
			
				if($attendance_type==CARD_STATUS_LATE){ # Late
					$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' ) AND c.RecordType='".CARD_STATUS_LATE."'";
				}
				else if($attendance_type==CARD_STATUS_ABSENT){ # Absent
					//$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' ) AND c.RecordType='".CARD_STATUS_ABSENT."' AND (b.AMStatus='".CARD_STATUS_ABSENT."' OR b.PMStatus='".CARD_STATUS_ABSENT."') ";
					$cond_day_type =" AND ((c.DayType='".PROFILE_DAY_TYPE_AM."' AND b.AMStatus='".CARD_STATUS_ABSENT."') OR (c.DayType='".PROFILE_DAY_TYPE_PM."' AND b.PMStatus='".CARD_STATUS_ABSENT."')) AND c.RecordType='".CARD_STATUS_ABSENT."' ";
				}else if($attendance_type==(CARD_STATUS_OUTING+PROFILE_TYPE_ABSENT)){ # Outing
					//$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' ) AND c.RecordType='".CARD_STATUS_ABSENT."' AND (b.AMStatus='".CARD_STATUS_OUTING."' OR b.PMStatus='".CARD_STATUS_OUTING."') ";
					$cond_day_type =" AND ((c.DayType='".PROFILE_DAY_TYPE_AM."' AND b.AMStatus='".CARD_STATUS_OUTING."') OR (c.DayType='".PROFILE_DAY_TYPE_PM."' AND b.PMStatus='".CARD_STATUS_OUTING."')) AND c.RecordType='".CARD_STATUS_ABSENT."' ";
				}
				else if($attendance_type==PROFILE_TYPE_EARLY){ # Early leave
					$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' ) AND c.RecordType='".PROFILE_TYPE_EARLY."'";
				}
				else{ # ALL (LATE or ABSENT or EARLY LEAVE)
					$cond_day_type =" AND ( (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' ) AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."' OR c.RecordType='".PROFILE_TYPE_EARLY."') ) ";
				}
			}
			
			# reason
			$cond_reason ="";
			if($match=="1"){
				$cond_reason=" AND c.Reason='".$this->Get_Safe_Sql_Query($reason)."'";
			}else{
				if($reason!="")
					$cond_reason =" AND c.Reason LIKE '%".$this->Get_Safe_Sql_Query($reason)."%'";
			}
			
			# waived
			$cond_waived="";
			if($waived==1){
				$cond_waived=" AND (c.RecordStatus=1 AND c.RecordStatus IS NOT NULL)";
			}
			else if($waived==2){
				$cond_waived=" AND (c.RecordStatus!=1 OR c.RecordStatus IS NULL) ";
			}
			else{
				# all ( waived or not wavied)
			}
			/*
			$sql="
				SELECT
					a.UserID,
					a.UserLogin,
					$username_field as StudentName,
					a.ClassName,
					a.ClassNumber,
					c.RecordDate,
					c.RecordType AS AttendanceType,
					c.DayType AS Session,
					c.Reason,
					c.RecordStatus,
					r.Remark,
					c.OfficeRemark,
					c.DocumentStatus 
				FROM 
					INTRANET_USER AS a 
					inner join 
					CARD_STUDENT_PROFILE_RECORD_REASON AS c 
					on 
						a.UserID = c.StudentID 
						AND a.RecordType=2 
						AND a.RecordStatus IN(0,1,2) 
						AND a.ClassName IN($selected_classes) 
						".$StudentCondition ." 
						AND c.RecordDate between '$startDate' and '$endDate 23:59:59' ";
			if ($this->EnableEntryLeavePeriod) {
				$sql .= "
					INNER JOIN 
					CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
					on 
						c.StudentID = selp.UserID 
						and 
						c.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
					";
			}
			$sql .=
					 " 
					 LEFT OUTER JOIN 
					 PROFILE_STUDENT_ATTENDANCE AS d 
					 ON c.ProfileRecordID = d.StudentAttendanceID 
					 LEFT JOIN CARD_STUDENT_DAILY_REMARK as r ON r.StudentID=c.StudentID AND r.RecordDate=c.RecordDate AND r.DayType=c.DayType  
				WHERE 
					1 = 1 
					$cond_waived
					$cond_reason
					$cond_day_type
				ORDER BY a.ClassName,a.ClassNumber+0,c.RecordDate
			";
			$final_result = $this->returnArray($sql);
			*/
			$final_result = array();
			################# Build array of Attendance Data from Daily Log tables #################
			# CAL target Year months
			$year_month = array();
			$start_ts = strtotime($startDate);
			$end_ts = strtotime($endDate);
			
			$temp_ts=$start_ts;
			while($temp_ts<=$end_ts){
				$year = date('Y',$temp_ts);
				$month = date('m',$temp_ts);
				$this->createTable_Card_Student_Daily_Log($year,$month);
				$year_month[] = array($year,$month);
				$temp_ts=mktime(0,0,0,date('m',$temp_ts)+1,1,date('Y',$temp_ts));
			}
			
			##
			if($session==""){
				$cond_session2 = "((AMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.")) OR ((AMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM.")) OR (PMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))))";
				//$cond_session2 = " ( b.AMStatus IN ($target_status) OR b.PMStatus IN ($target_status) )";
			}
			else if($session==PROFILE_DAY_TYPE_AM){
				//$cond_session2 = " b.AMStatus IN ($target_status) ";
				$cond_session2 = "((AMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.")) OR ((AMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))))";
			}
			else if($session==PROFILE_DAY_TYPE_PM){
				//$cond_session2 = " b.PMStatus IN ($target_status) ";
				$cond_session2 = "((PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.")) OR ((PMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))))";
			}
			
			
			## get attendance record from DAILY LOG Tables
			$attendance_data = array();
			for($i=0;$i<sizeof($year_month);$i++){
				list($year,$month) = $year_month[$i];
					$table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
					$date_field = "CONCAT('$year','-','$month','-',IF(b.DayNumber<=9,CONCAT('0',b.DayNumber),b.DayNumber))";
					
					$sql="SELECT
							a.UserID,
							a.UserLogin,
							$username_field as StudentName,
							a.ClassName,
							a.ClassNumber,
							c.RecordDate,
							IF(c.RecordType='".PROFILE_TYPE_ABSENT."' AND ((c.DayType='".PROFILE_DAY_TYPE_AM."' AND b.AMStatus='".CARD_STATUS_OUTING."') OR (c.DayType='".PROFILE_DAY_TYPE_PM."' AND b.PMStatus='".CARD_STATUS_OUTING."')),
								'".(CARD_STATUS_OUTING+CARD_STATUS_ABSENT)."',
								c.RecordType 
							) AS AttendanceType,
							c.DayType AS Session,
							c.Reason,
							c.RecordStatus,
							r.Remark,
							c.OfficeRemark,
							c.DocumentStatus,
							IF(b.AMStatus='".CARD_STATUS_OUTING."','".(CARD_STATUS_OUTING+CARD_STATUS_ABSENT)."',b.AMStatus) as AMStatus,
							b.InSchoolTime,
							IF(b.PMStatus='".CARD_STATUS_OUTING."','".(CARD_STATUS_OUTING+CARD_STATUS_ABSENT)."',b.PMStatus) as PMStatus,
							b.LunchBackTime,
							b.LeaveStatus,
							b.LeaveSchoolTime,
							LPAD(a.ClassNumber,8,'0') as ClassNumberStr,
							CONCAT(c.RecordDate,'_',c.DayType) as DateTypeStr 
                            ".($sys_custom['StudentAttendance']['SeriousLate'] ? ",SeriousLate" : "") ."
						FROM 
							INTRANET_USER AS a 
							inner join $table_name as b ON b.UserID=a.UserID 
							inner join CARD_STUDENT_PROFILE_RECORD_REASON AS c 
							on 
								b.UserID = c.StudentID 
								".$StudentCondition ." 
								AND DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d')=c.RecordDate 
								AND c.RecordDate between '$startDate' and '$endDate 23:59:59' 
								AND ((b.AMStatus='".CARD_STATUS_ABSENT."' AND c.RecordType='".CARD_STATUS_ABSENT."' AND c.DayType='".PROFILE_DAY_TYPE_AM."') OR 
									 (b.AMStatus='".CARD_STATUS_LATE."' AND c.RecordType='".CARD_STATUS_LATE."' AND c.DayType='".PROFILE_DAY_TYPE_AM."') OR  
									 (b.LeaveStatus IN ('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND c.RecordType='".PROFILE_TYPE_EARLY."' AND c.DayType='".PROFILE_DAY_TYPE_AM."') OR 
									 (b.AMStatus='".CARD_STATUS_OUTING."' AND c.RecordType='".CARD_STATUS_ABSENT."' AND c.DayType='".PROFILE_DAY_TYPE_AM."') OR 
									 (b.PMStatus='".CARD_STATUS_ABSENT."' AND c.RecordType='".CARD_STATUS_ABSENT."'  AND c.DayType='".PROFILE_DAY_TYPE_PM."') OR 
									 (b.PMStatus='".CARD_STATUS_LATE."' AND c.RecordType='".CARD_STATUS_LATE."' AND c.DayType='".PROFILE_DAY_TYPE_PM."') OR  
									 (b.LeaveStatus IN ('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND c.RecordType='".PROFILE_TYPE_EARLY."' AND c.DayType='".PROFILE_DAY_TYPE_PM."') OR 
									 (b.PMStatus='".CARD_STATUS_OUTING."' AND c.RecordType='".CARD_STATUS_ABSENT."' AND c.DayType='".PROFILE_DAY_TYPE_PM."')
								) ";
					if ($this->EnableEntryLeavePeriod) {
						$sql .= "
							INNER JOIN 
							CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
							on 
								c.StudentID = selp.UserID 
								and 
								c.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
							";
					}
					$sql .=
							 " 
							 LEFT OUTER JOIN 
							 PROFILE_STUDENT_ATTENDANCE AS d 
							 ON c.ProfileRecordID = d.StudentAttendanceID 
							 LEFT JOIN CARD_STUDENT_DAILY_REMARK as r ON r.StudentID=c.StudentID AND r.RecordDate=c.RecordDate AND r.DayType=c.DayType  
						WHERE 
							a.RecordType=2 
							AND a.RecordStatus IN(0,1,2) 
							AND a.ClassName IN($selected_classes) 
							$cond_waived
							$cond_reason
							$cond_day_type
						ORDER BY a.ClassName,a.ClassNumber+0,c.RecordDate 
					";
					
					/*
					$sql="
							SELECT
								b.UserID,
								$date_field as RecordDate,
								b.AMStatus,
								b.InSchoolTime,
								b.PMStatus,
								b.LunchBackTime,
								b.LeaveStatus,
								b.LeaveSchoolTime 
							FROM
								$table_name AS b ,
								INTRANET_USER AS a
							WHERE
								$cond_session2 AND
								$date_field >='$startDate' AND
								$date_field <= '$endDate' AND
								a.ClassName IN ($selected_classes) AND
								a.UserID = b.UserID
							ORDER BY
								b.DayNumber
					";
					$temp = $this->returnArray($sql);
					*/
					$temp = $this->returnResultSet($sql);
					$final_result = array_merge($final_result,$temp);
					//debug_pr($final_result);
					for($j=0;$j<sizeof($temp);$j++){
						//list($user_id,$record_date,$am_status,$InSchoolTime,$pm_status,$LunchBackTime,$leave_status) = $temp[$j];
						$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['AM'] = $temp[$j]['AMStatus'];
						$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['PM'] = $temp[$j]['PMStatus'];
						$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['InSchoolTime'] = $temp[$j]['InSchoolTime'];
						$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['LunchBackTime'] = $temp[$j]['LunchBackTime'];
						$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['LeaveStatus'] = $temp[$j]['LeaveStatus'];
						$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['LeaveSchoolTime'] = $temp[$j]['LeaveSchoolTime'];
						if(($temp[$j]['LeaveStatus'] == CARD_LEAVE_AM) ||($temp[$j]['LeaveStatus'] == CARD_LEAVE_PM))
							$attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['EarlyLeave'] = PROFILE_TYPE_EARLY;
						if($sys_custom['StudentAttendance']['SeriousLate']){
						    $attendance_data[$temp[$j]['UserID']][$temp[$j]['RecordDate']]['SeriousLate'] = $temp[$j]['SeriousLate'];
						}
					}
			}
			##################### end build array ###############
			sortByColumn3($final_result,"ClassName","ClassNumberStr","DateTypeStr");
			//debug_pr($final_result);
			return array($final_result,$attendance_data);
		}

		function Import_Preset_AbsenceTW($Data)
		{
			global $Lang, $TeachingClassListOnly;
			$Error = array();
			$Result = array();
			if(!is_array($Data))
			{
				return array();
			}

			$sql = 'DROP TABLE TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD';
			$this->db_db_query($sql);
			$sql = 'CREATE TABLE TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD (
								RecordID int(11) NOT NULL auto_increment,
								StudentID varchar(20) default NULL,
							  RecordDate Date default NULL,		
							  DayPeriod int(11) default NULL,
							  AttendanceType int(11) default NULL,
							  SessionFrom varchar(20) default NULL,
							  SessionTo varchar(20) default NULL,
							  LeaveTypeCode varchar(65) default NULL,
							  LeaveTypeID int(11) default NULL,
							  Reason mediumtext default NULL,							 
							  Remark mediumtext default NULL,
							  RelatedRecordID int(11) default NULL,	
							  PRIMARY KEY (RecordID)						  
							) ENGINE=InnoDB DEFAULT CHARSET=utf8
						 ';
			$this->db_db_query($sql);
			//debug_r($sql);

			$this->Start_Trans();
			$TodayTimeStamp = strtotime(date("Y-m-d"));
			$NameField = getNameFieldByLang("a.");
			$values_array = array();
			for($i=0;$i<sizeof($Data);$i++)
			{
				list($ClassName,$ClassNumber, $AttendanceType, $StartDate, $EndDate, $SessionFrom, $SessionTo, $LeaveTypeCode, $Reason, $Remark) = $Data[$i];

				$ClassName = trim($ClassName);
				$ClassNumber = trim($ClassNumber);
				$AttendanceType = trim($AttendanceType);
				$StartDate = trim($StartDate);
				$EndDate = trim($EndDate);
				$SessionFrom = trim($SessionFrom);
				$SessionTo = trim($SessionTo);
				$LeaveTypeCode = trim($LeaveTypeCode);
				$Reason = trim($Reason);
				$Remark = trim($Remark);

				// is user found
				$sql = "SELECT
							a.UserID
						FROM
							INTRANET_USER as a
						WHERE
							a.RecordType = '2'
					  	AND a.RecordStatus = '1'
							AND a.ClassName = '".$this->Get_Safe_Sql_Query($ClassName)."' 
							AND a.ClassNumber = '".$this->Get_Safe_Sql_Query($ClassNumber)."'";
				//debug_r($sql);
				$UserInfo = $this->returnVector($sql);
				if(sizeof($UserInfo) == 0)
				{
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['StudentNotFound']);
					continue;
					//break;
				}
				if(is_array($TeachingClassListOnly)) {
					if(in_array($ClassName, $TeachingClassListOnly) == false) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['StudentNotInTeacherClassList']);
						continue;
					}
				}

				$StudentID = $UserInfo[0];

				// is date format valid
				if (!checkDateIsValid($StartDate)) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['General']['InvalidDateFormat']);
					continue;
				}

				// is date is
				/*if (strtotime($StartDate) <= $TodayTimeStamp) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning']);
					continue;
				}*/

				// is leave type correct
				if (in_array($AttendanceType, array(0,1,2,3)) == false) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['InvalidAttendanceType']);
					continue;
				}

				if($AttendanceType == 0) {
					if (!checkDateIsValid($EndDate)) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['General']['InvalidDateFormat']);
						continue;
					}

					if (strtotime($StartDate) > strtotime($EndDate)) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateLaterThanStartDate']);
						continue;
					}
				} else if($AttendanceType == 3) {
					if($SessionFrom == '' || $SessionTo == '' || !is_numeric($SessionFrom) || !is_numeric($SessionTo) || $SessionFrom > $SessionTo) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['InvalidSession']);
						continue;
					}
				}

				$LeaveTypeID = 0;
				$sql = "SELECT TypeID FROM CARD_STUDENT_LEAVE_TYPE WHERE TypeSymbol='".$LeaveTypeCode."'";
				$rs = $this->returnVector($sql);
				if(empty($rs)) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['InvalidLeaveType']);
					continue;
				}
				$LeaveTypeID = $rs[0]['TypeID'];

				$is_overlap = $this->_checkPresetLeaveOverlap('TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD', $StudentID, $AttendanceType, $StartDate, $EndDate, $SessionFrom, $SessionTo);
				//var_dump($is_overlap);
				if($is_overlap[0] == false) {
					$is_overlap = $this->checkPresetLeaveOverlap( $StudentID, $AttendanceType, $StartDate, $EndDate, $SessionFrom, $SessionTo);
					//var_dump($is_overlap);
				}

				if($is_overlap[0] == true) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['LessonAttendance']['StudentAlreadyHasLeaveRecordsOnThatDay']);
					continue;
				}

				if($AttendanceType == 3) {
					if(empty($is_overlap[1])) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['InvalidSession']);
						continue;
					}
				}

				// check if there is duplicate import


				$base_fields = 'StudentID,AttendanceType,SessionFrom,SessionTo,LeaveTypeCode,LeaveTypeID,Reason,Remark,RecordDate,DayPeriod';
				$base_values = "'" . $StudentID . "',
										'" . $AttendanceType . "',
										'" . $SessionFrom . "',
										'" . $SessionTo . "',
										'" . $LeaveTypeCode . "',
										'" . $LeaveTypeID . "',
										'" . $this->Get_Safe_Sql_Query($Reason) . "',				
										'" . $this->Get_Safe_Sql_Query($Remark) . "'";

				$target_date = array();
				if($AttendanceType == 0) {
					$date_diff = strtotime($EndDate) - strtotime($StartDate);
					$date_diff = round($date_diff / (60 * 60 * 24));
					for ($j = 0; $j <= $date_diff; $j++) {
						$current_date = date("Y-m-d", strtotime($StartDate) + ($j * 60 * 60 * 24));
						$target_date[] = $current_date;
					}

					if (count($target_date) == 1) {
						$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields)
									values ($base_values , '$StartDate', '2')";
						$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $StartDate . ':AttendanceType-' . $AttendanceType.'-am'] = $this->db_db_query($sql);
						$record_id = $this->db_insert_id();

						$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields , RelatedRecordID)
									values ($base_values , '$StartDate', '3', $record_id)";
						$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $StartDate . ':AttendanceType-' . $AttendanceType.'-pm'] = $this->db_db_query($sql);
					} else {
						$RelatedRecordID = 'NULL';
						foreach ($target_date as $date_index=>$t_date) {
							$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields , RelatedRecordID)
									values ($base_values , '$t_date', '2', $RelatedRecordID)";

							$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $t_date . ':AttendanceType-' . $AttendanceType.'-am'] = $this->db_db_query($sql);
							if($date_index == 0) {
								$RelatedRecordID = $this->db_insert_id();
							}

							$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields , RelatedRecordID)
									values ($base_values , '$t_date', '3', $RelatedRecordID)";

							$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $t_date . ':AttendanceType-' . $AttendanceType.'-pm'] = $this->db_db_query($sql);
						}
					}
				} else if($AttendanceType == 1 || $AttendanceType == 2) {
					$DayPeriod = ($AttendanceType == 1) ? '2' : '3';
					$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields)
									values ($base_values , '$StartDate', '$DayPeriod')";

					$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $StartDate . ':AttendanceType-' . $AttendanceType.'-'.$DayPeriod] = $this->db_db_query($sql);
				} else if($AttendanceType == 3) {

					$target_date_period = $is_overlap[1];

					$RelatedRecordID = 'NULL';
					if (in_array('am', $target_date_period)) {
						$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields , RelatedRecordID)
									values ($base_values , '$StartDate', '2', $RelatedRecordID)";

						$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $StartDate . ':AttendanceType-' . $AttendanceType.'-am'] = $this->db_db_query($sql);
						$record_id = $this->db_insert_id();
						$RelatedRecordID = $record_id;
					}

					if (in_array('pm', $target_date_period)) {
						$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD ($base_fields , RelatedRecordID)
									values ($base_values , '$StartDate', '3', $RelatedRecordID)";

						$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $StartDate . ':AttendanceType-' . $AttendanceType.'-pm'] = $this->db_db_query($sql);
						$record_id = $this->db_insert_id();
						if ($RelatedRecordID != 'NULL' && $record_id != '') {
							$sql = "UPDATE TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD SET RelatedRecordID=$RelatedRecordID WHERE RecordID='$record_id'";
							$Result['InsertTempLeave:StaffID-' . $StudentID . ':LeaveDate-' . $StartDate . ':AttendanceType-' . $AttendanceType.'-pm-update'] = $this->db_db_query($sql);
						}
					}

				}
			}

			//debug_pr($sql);
			if (in_array(false,$Result)) {
				$this->RollBack_Trans();
			}
			else {
				$this->Commit_Trans();
			}

			return $Error;
		}

		function Finalize_Import_Preset_AbsenceTW()
		{
			$sql = "SELECT * FROM TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD WHERE RelatedRecordID='' OR RelatedRecordID IS NULL";
			$TempResult = $this->returnArray($sql);
			$values_array=array();
			for ($i=0; $i< sizeof($TempResult); $i++)
			{
				$SessionFrom = $TempResult[$i]['SessionFrom'];
				if($SessionFrom == '') {
					$SessionFrom = 'NULL';
				}
				$SessionTo = $TempResult[$i]['SessionTo'];
				if($SessionTo == '') {
					$SessionTo = 'NULL';
				}
				$sql = "INSERT into CARD_STUDENT_PRESET_LEAVE (
									StudentID,
									RecordDate,
									DayPeriod,
									Reason,
									Remark,
									DateInput,
									DateModified,
									Waive,
									AttendanceType, 
									SessionFrom,
									SessionTo,
									LeaveTypeID
									) 
								values (
									'".$TempResult[$i]['StudentID']."',
									'".$TempResult[$i]['RecordDate']."',
									'".$TempResult[$i]['DayPeriod']."',
									'".$this->Get_Safe_Sql_Query($TempResult[$i]['Reason'])."',
									'".$this->Get_Safe_Sql_Query($TempResult[$i]['Remark'])."',
									NOW(),
									NOW(),
									'0',
									'".$TempResult[$i]['AttendanceType']."',
									".$SessionFrom.",
									".$SessionTo.",
									'".$TempResult[$i]['LeaveTypeID']."'
									)";
				$Result['InsertLeaveRecord-StudentID:'.$TempResult[$i]['StudentID'].'-TargetDate:'.$TempResult[$i]['RecordDate'].'-DayType:'.$TempResult[$i]['DayPeriod'].'_'.$temp['RecordID']] = $this->db_db_query($sql);
				$record_id = $this->db_insert_id();
				$sql = "SELECT * FROM TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD WHERE RelatedRecordID='".$TempResult[$i]['RecordID']."'";
				$rs = $this->returnArray($sql);
				foreach($rs as $temp) {
					$SessionFrom = $temp['SessionFrom'];
					if($SessionFrom == '') {
						$SessionFrom = 'NULL';
					}
					$SessionTo = $temp['SessionTo'];
					if($SessionTo == '') {
						$SessionTo = 'NULL';
					}
					$sql = "INSERT into CARD_STUDENT_PRESET_LEAVE (
									StudentID,
									RecordDate,
									DayPeriod,
									Reason,
									Remark,
									DateInput,
									DateModified,
									Waive,
									AttendanceType, 
									SessionFrom,
									SessionTo,
									LeaveTypeID,
									RelatedRecordID
									) 
								values (
									'".$temp['StudentID']."',
									'".$temp['RecordDate']."',
									'".$temp['DayPeriod']."',
									'".$this->Get_Safe_Sql_Query($temp['Reason'])."',
									'".$this->Get_Safe_Sql_Query($temp['Remark'])."',
									NOW(),
									NOW(),
									'0',
									'".$temp['AttendanceType']."',
									".$SessionFrom.",
									".$SessionTo.",
									'".$temp['LeaveTypeID']."',
									'".$record_id."'
									)";

					$Result['InsertLeaveRecord-StudentID:'.$temp['StudentID'].'-TargetDate:'.$temp['RecordDate'].'-DayType:'.$temp['DayPeriod'].'_'.$temp['RecordID']] = $this->db_db_query($sql);
					$this->db_db_query($sql);

				}
			}

			return sizeof($TempResult);
		}

		function Import_Preset_Absence($Data)
		{
			global $Lang, $TeachingClassListOnly;
			$Error = array();
			$Result = array();
			if(!is_array($Data))
			{
				return array();
			}
			
			$sql = 'DROP TABLE TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD';
			$this->db_db_query($sql);
			$sql = 'CREATE TABLE TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD (
								UserID varchar(20) default NULL,
							  TargetDate Date default NULL,
							  DayType int(2) default NULL,
							  Reason mediumtext default NULL,
							  Waive int(1) default NULL,
							  Handin_Prove int(1) default 0,
							  Remark mediumtext default NULL,
							  UNIQUE KEY (UserID,TargetDate,DayType) 
							) ENGINE=InnoDB DEFAULT CHARSET=utf8
						 ';
			$this->db_db_query($sql);
			//debug_r($sql);
			
			$this->Start_Trans();
			$TodayTimeStamp = strtotime(date("Y-m-d"));
			$NameField = getNameFieldByLang("a.");
			$values_array = array();
			for($i=0;$i<sizeof($Data);$i++)
			{
				list($ClassName,$ClassNumber,$LeaveDate,$DayType,$Reason,$Waive,$Handin_Prove,$Remark) = $Data[$i];
				$ClassName = trim($ClassName);
				$ClassNumber = trim($ClassNumber);
				$LeaveDate = trim($LeaveDate);
				$DayType = trim($DayType);
				$Reason = trim($Reason);
				$Waive = trim($Waive);
				$Handin_Prove = trim($Handin_Prove);
				$Remark = trim($Remark);
				
				// is user found
				$sql = "SELECT
							a.UserID
						FROM
							INTRANET_USER as a
						WHERE
							a.RecordType = '2'
					  	AND a.RecordStatus = '1'
							AND a.ClassName = '".$this->Get_Safe_Sql_Query($ClassName)."' 
							AND a.ClassNumber = '".$this->Get_Safe_Sql_Query($ClassNumber)."'";
				//debug_r($sql);
				$UserInfo = $this->returnVector($sql);
				if(sizeof($UserInfo) == 0)
				{
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['StudentNotFound']);
					continue;
					//break;
				}
				if(is_array($TeachingClassListOnly)) {
					if(in_array($ClassName, $TeachingClassListOnly) == false) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['StudentNotInTeacherClassList']);
						continue;
					}
				}

				$StudentID = $UserInfo[0];
				
				// is date format valid
				if (!checkDateIsValid($LeaveDate)) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['General']['InvalidDateFormat']);
					continue;
				}
				
				// is date is future
				/*if (strtotime($LeaveDate) <= $TodayTimeStamp) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning']);
					continue;
				}*/
				
				// is leave type correct
				if ($DayType != 2 && $DayType != 3) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['InvalidTimeSlot']);
					continue;
				}
				
				// is wavie input already?
				if ($Waive == "" || ($Waive != 0 && $Waive != 1)) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['WaiveCodeNotValid']);
					continue;
				}
				// 
				// is Handin_Prove input already?
				if ($Handin_Prove == "" || ($Handin_Prove != 0 && $Handin_Prove != 1)) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['HandinProveCodeNotValid']);
					continue;
				}
				
				// check if there is duplicate import
				$sql = "select 
									count(1) 
								from 
									TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD 
								where 
									UserID = '".$StudentID."' 
									and 
									TargetDate = '".$LeaveDate."' 
									AND 
									DayType = '".$DayType."' 
									AND 
									Reason = '".$this->Get_Safe_Sql_Query($Reason)."' 
									AND 
									Remark = '".$this->Get_Safe_Sql_Query($Remark)."' 
									";
				$ImportedBefore = $this->returnVector($sql);
				if ($ImportedBefore[0] > 0) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StudentAttendance']['DuplicateImportPresetAbsence']);
					continue;
				}
				
				$sql = "insert into TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD (
									UserID,
								  TargetDate,
								  DayType,
								  Reason,
								  Waive,
								  Handin_Prove,
								  Remark)
								values (
									'".$StudentID."',
									'".$LeaveDate."',
									'".$DayType."',
									'".$this->Get_Safe_Sql_Query($Reason)."',
									'".$Waive."',
									'".$Handin_Prove."',
									'".$this->Get_Safe_Sql_Query($Remark)."'
									)";
				$Result['InsertTempLeave:StaffID-'.$StudentID.':LeaveDate-'.$LeaveDate.':DayType-'.$DayType] = $this->db_db_query($sql);
			}
			
			//debug_pr($sql);
			if (in_array(false,$Result)) {
				$this->RollBack_Trans();
			}
			else {
				$this->Commit_Trans();
			}
			
			return $Error;
		}
		
		function Finalize_Import_Preset_Absence()
		{
			$sql = "SELECT 
								UserID,
							  TargetDate,
							  DayType,
							  Reason,
							  Waive,
							  Handin_Prove,
							  Remark
							FROM 
								TEMP_IMPORT_STUDENT_PRESET_ABSENCE_RECORD";
			$TempResult = $this->returnArray($sql);
			$values_array=array();
			for ($i=0; $i< sizeof($TempResult); $i++)
			{
				list($StudentID,$TargetDate,$DayType,$Reason,$Waive,$DocumentStatus,$Remark) = $TempResult[$i];
				
				$sql = "replace into CARD_STUDENT_PRESET_LEAVE (
									StudentID,
									RecordDate,
									DayPeriod,
									Reason,
									Waive,
									DocumentStatus,
									Remark,
									DateInput,
									DateModified 
									) 
								values (
									'".$StudentID."',
									'".$TargetDate."',
									'".$DayType."',
									'".$this->Get_Safe_Sql_Query($Reason)."',
									'".$Waive."',
									'".$DocumentStatus."',
									'".$this->Get_Safe_Sql_Query($Remark)."',
									NOW(),
									NOW() 
									)";
				$Result['InsertLeaveRecord-StudentID:'.$StudentID.'-TargetDate:'.$TargetDate.'-DayType:'.$DayType] = $this->db_db_query($sql);
			}
			
			return sizeof($TempResult);
		}
		
		function Check_Outing_Reason($ReasonText,$RecordID="") {
			if ($ReasonText != "") {
				$sql = "select 
									RecordID 
								from 
									CARD_STUDENT_OUTING_REASON 
								where 
									ReasonText = '".$this->Get_Safe_Sql_Query($ReasonText)."' ";
				if ($RecordID != "") {
					$sql .= "
									AND 
									RecordID != '".$RecordID."' ";
				}
				$Temp = $this->returnVector($sql);
				
				// true - ok to use, false - duplication found			
				return !(sizeof($Temp) > 0);
			}
			else 
				return false;
		}	
		
		function Save_Outing_Reason($ReasonText,$RecordID="") {
			if ($RecordID != "") {
				$sql = "update CARD_STUDENT_OUTING_REASON set 
									ReasonText = '".$this->Get_Safe_Sql_Query($ReasonText)."', 
									DateModify = NOW(),
									ModifyBy = '".$_SESSION['UserID']."'
								where 
									RecordID = '".$RecordID."'";
			}
			else {
				$sql = "insert into CARD_STUDENT_OUTING_REASON (
									ReasonText,
									DateInput,
									InputBy,
									DateModify,
									ModifyBy
									) 
								values (
									'".$this->Get_Safe_Sql_Query($ReasonText)."', 
									NOW(),
									'".$_SESSION['UserID']."',
									NOW(),
									'".$_SESSION['UserID']."'
									)";
			}
			return $this->db_db_query($sql);
		}
		
		function Get_Outing_Reason($RecordID="") {
			$sql = "select 
								ReasonText 
							from 
								CARD_STUDENT_OUTING_REASON ";
			if ($RecordID != "") {
				$sql .= "
							where 
								RecordID = '".$RecordID."'";
			}
			$sql .= "
							Order By 
								ReasonText ";
			//debug_r($sql);
			return $this->returnVector($sql);
		}
		
		function Delete_Outing_Reason($RecordID) {
			$sql = "delete from CARD_STUDENT_OUTING_REASON 
							where 
								RecordID IN ('".implode("','",(array)$RecordID)."')";
			
			return $this->db_db_query($sql);
		}
		
		function Get_Absent_Present_Sex_Distribution_By_Class_Data($TargetDate,$DayType,$ClassNames) {
			$TargetDate = date("Y-m-d", strtotime($TargetDate));
			list($Year,$Month,$DayNumber) = explode('-',$TargetDate);
			$DayNumber = $DayNumber + 0;
			
			if ($DayType == PROFILE_DAY_TYPE_AM) 
				$StatusField = "AMStatus";
			else 
				$StatusField = "PMStatus";
			$sql = "select
								u.ClassName,
								u.Gender,
                count(u.UserID) as TotalStudent,
								count(dailylog.RecordID) as AbsentCount,
                count(dailylog1.RecordID) as PresentCount
							from
								INTRANET_USER u
                INNER JOIN
                YEAR_CLASS yc
                on
                  u.RecordType = '2'
  								and
	  							u.RecordStatus IN(0,1,2)
		  						and
			  					u.ClassName in ('".implode("','",$this->Get_Safe_Sql_Query($ClassNames))."')
                  and
                  yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
                  and
                  u.ClassName = yc.ClassTitleEN
                INNER JOIN
                YEAR y
                on yc.YearID = y.YearID
								LEFT JOIN
								CARD_STUDENT_DAILY_LOG_".$Year."_".$Month." dailylog
								on
									u.UserID = dailylog.UserID
									and
									dailylog.DayNumber = '".$this->Get_Safe_Sql_Query($DayNumber)."'
                  and
                  dailylog.".$StatusField." = '".CARD_STATUS_ABSENT."'
                LEFT JOIN
                CARD_STUDENT_DAILY_LOG_".$Year."_".$Month." dailylog1
                on
									u.UserID = dailylog1.UserID
									and
									dailylog1.DayNumber = '".$this->Get_Safe_Sql_Query($DayNumber)."'
                  and
                  dailylog1.".$StatusField." in ('".CARD_STATUS_PRESENT."','".CARD_STATUS_LATE."')
							group by
								u.ClassName, u.Gender
              Order by
               y.sequence, yc.sequence
							";
			$temp = $this->returnArray($sql);
			for ($i=0; $i< sizeof($temp); $i++) {
				list($ClassName,$Gender,$TotalStudent,$AbsentCount,$PresentCount) = $temp[$i];
				$Result[$ClassName][$Gender] = array("Total"=> ($TotalStudent+0),"Absent"=> ($AbsentCount+0),"Present"=> ($PresentCount+0));
			}
			
			return $Result;
		}
		
		function Get_Academic_Year_List() {
			global $Lang;
			
			if ($this->Platform == "IP") { // IP25
				include_once("form_class_manage.php");
				$lfcm = new form_class_manage();
				$arrResult = $lfcm->Get_Academic_Year_List('', 1, array(), 0, 1, 0);
				$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
				
				for ($i=0; $i< sizeof($arrResult); $i++) {
					$Return[] = array($arrResult[$i]['AcademicYearID'],$arrResult[$i]['YearNameEN'],(($CurrentAcademicYearID == $arrResult[$i]['AcademicYearID'])? 1:0));
				}
			}
			else { // EJ
				include_once("libmanagehistory.php");
				$lmh = new libmanagehistory();
				$arrResult = $lmh->getYearList();
				
				$Return[] = array("",$Lang['SysMgr']['AcademicYear']['FieldTitle']['CurrentSchoolYear'],1);
				for ($i=0; $i< sizeof($arrResult); $i++) {
					$Return[] = array($arrResult[$i],$arrResult[$i],0);
				}
			}
			
			return $Return;
		}
		
		function Get_Class_List_By_Academic_Year($AcademicYear) {
			global $Lang;
			
			if ($this->Platform == "IP") { // IP25
				include_once("form_class_manage.php");
				$lfcm = new form_class_manage();
				$arrResult = $lfcm->Get_Class_List_By_Academic_Year($AcademicYear);
				
				for ($i=0; $i< sizeof($arrResult); $i++) {
					$Return[] = array($arrResult[$i]['YearClassID'],$arrResult[$i]['ClassTitleEN'],"ClassID"=>$arrResult[$i]['YearClassID'],"ClassName"=>$arrResult[$i]['ClassTitleEN']);
				}
			}
			else { // EJ
				include_once("libmanagehistory.php");
				$lmh = new libmanagehistory();
				$arrResult = $lmh->getClassListByYear($AcademicYear);
				
				for ($i=0; $i< sizeof($arrResult); $i++) {
					$Return[] = array($arrResult[0][$i]['ClassId'],$arrResult[0][$i]['ClassName'],"ClassID"=>$arrResult[0][$i]['ClassId'],"ClassName"=>$arrResult[0][$i]['ClassName']);
				}
			}
			
			return $Return;
		}
		
		function Get_Student_List_By_Class_Academic_Year($ClassID,$AcademicYear) {
			global $Lang;
			
			include_once('libuser.php');
			if ($this->Platform == "IP") { // IP25
				/*
				include_once("form_class_manage.php");
				$Class = new year_class($ClassID,false,false,true);
				$StudentList = $Class->ClassStudentList;
				
				for ($i=0; $i < sizeof($StudentList); $i++) {
					list($StudentID,$ClassNumber,$StudentName,$IsArchivedUser,$ClassName) = $StudentList[$i];
					$libuser = new libuser($StudentID);
					
					$Return[] = array("UserID"=>$StudentID,
														"ClassNumber"=>$ClassNumber, 
														"ChineseName"=>$libuser->ChineseName, 
														"EnglishName"=>$libuser->EnglishName, 
														"Gender"=>$libuser->Gender,
														$StudentID,
														$ClassNumber, 
														$libuser->ChineseName, 
														$libuser->EnglishName, 
														$libuser->Gender,
														trim($libuser->Remark),
														"RecordStatus" => $libuser->RecordStatus,
														"IsArchivedUser" => $IsArchivedUser);
				}
				*/
				
				//$NameField = getNameFieldByLang('u.');
				//$ArchiveNameField = getNameFieldByLang2('au.');
				$ClassNameField  = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
				$sql = "SELECT 
							ycu.UserID, 
							ycu.ClassNumber,
							IF(u.UserID IS NULL,au.ChineseName,u.ChineseName) as ChineseName,
							IF(u.UserID IS NULL,au.EnglishName,u.EnglishName) as EnglishName, 
							IF(u.UserID IS NULL,au.Gender,u.Gender) as Gender,
							IF(u.UserID IS NULL,au.Remark,u.Remark) as Remark,
							IF(u.UserID IS NULL,'',u.RecordStatus) as RecordStatus,
							CASE 
								WHEN au.UserID IS NOT NULL then '1' 
								ELSE '0' 
							END as IsArchivedUser,
							$ClassNameField as ClassName,
							u.DateOfBirth as BirthDate,
							us.AdmissionDate as EntryDate							
						FROM 
							YEAR_CLASS_USER as ycu 
							INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID 
							LEFT JOIN INTRANET_USER as u ON (ycu.UserID = u.UserID)
							LEFT JOIN INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID)
							LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as us ON (ycu.UserID = us.UserID) 
						WHERE 
							ycu.YearClassID = '".$this->Get_Safe_Sql_Query($ClassID)."'
						ORDER BY 
							ycu.ClassNumber";
				
				$Return = $this->returnArray($sql);
			}
			else { // EJ
				include_once("libmanagehistory.php");
				$lmh = new libmanagehistory();
				$StudentList = $lmh->getStudentListByClass($AcademicYear, $ClassID);
				
				for ($i=0; $i < sizeof($StudentList); $i++) {
					$StudentID = $StudentList[$i]['id'];
					$ClassNumber = $StudentList[$i]['classnumber'];
					$libuser = new libuser($StudentID);
					$Return[] = array("UserID"=>$StudentID,
														"ClassNumber"=>$ClassNumber, 
														"ChineseName"=>$libuser->ChineseName, 
														"EnglishName"=>$libuser->EnglishName, 
														"Gender"=>$libuser->Gender,
														$StudentID,
														$ClassNumber, 
														$libuser->ChineseName, 
														$libuser->EnglishName, 
														$libuser->Gender);
				}
			}
			
			return $Return;
		}
		
		##############################################
		## ASL Parent App - AWS_NotifyStudentStatus ##
		##############################################
		# ParamArray : 
		# 	UserID - Student ID
		#	AccessDateTime - Tap card time or Confirm time	
		#	Status - Student arrival status - either A or L
		#	ParentID - optional, for filtering valid parents that can access app 
		##############################################
//		function Notify_Student_Status($ParamArray)
//		{
//			global $PATH_WRT_ROOT, $file_path, $asl_parent_app, $sys_custom;
//			include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
//			include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
//			
//			$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
//			
//			$debug_log = $sys_custom['ASLParentAppDebugLog'];
//			$log_content = "";
//			$ReturnArray = array();
//			$StudentID = $ParamArray['UserID']; // student ID
//			$AccessDateTime = date("Y-m-d H:i:s",$ParamArray['AccessDateTime']); // timestamp formatted to YYYY-MM-DD HH:MM:SS 
//			$Status = strtoupper($ParamArray['Status']); // Arrival status - A: Arrrive School, L: Leave School
//			$CheckParentID = isset($ParamArray['ParentID']);
//			$ParentIDArray = $ParamArray['ParentID'];
//			//$SchoolID = $_SERVER['SERVER_NAME'];
//			//$SchoolID = "junior20-b5-demo2.eclass.com.hk";
//			if(isset($asl_parent_app['SchoolID'])){
//				$SchoolID = $asl_parent_app['SchoolID'];
//			}else{
//				$SchoolID = $_SERVER['SERVER_NAME'];
//			}
//			## Get student name, user login, parent info 
//			$NameField = getNameFieldByLang2("u.");
//			$ParentNameField = getNameFieldByLang2("pu.");
//			$sql = "SELECT DISTINCT 
//						u.UserLogin, $NameField as StudentName, pu.UserLogin as ParentUserLogin, $ParentNameField as ParentName  
//					FROM INTRANET_USER as u 
//					INNER JOIN INTRANET_PARENTRELATION as p ON p.StudentID = '$StudentID' 
//					INNER JOIN INTRANET_USER as pu ON p.ParentID = pu.UserID AND pu.RecordStatus = 1 AND pu.RecordType = '".USERTYPE_PARENT."' 
//					INNER JOIN INTRANET_API_REQUEST_LOG as rl ON (rl.UserID = p.ParentID OR rl.UserID = p.StudentID) 
//					WHERE u.UserID = '$StudentID' ";
//			if($CheckParentID){
//				$sql .= " pu.UserID IN ('".implode("','",(array)$ParentIDArray)."') ";
//			}
//			$data = $this->returnArray($sql);
//			## Notify all parents of the student
//			for($i=0;$i<sizeof($data);++$i){
//				$StudentUserLogin = $data[$i]['UserLogin'];
//				$StudentName = $data[$i]['StudentName'];
//				$ParentUserLogin = $data[$i]['ParentUserLogin'];
//				$ParentName = $data[$i]['ParentName'];
//				
//				$ASLParam = array(
//							"SchID" => $SchoolID,
//							"UserName" => $StudentUserLogin,
//							"Status" => $Status,
//							"AccessDateTime" => $AccessDateTime,
//							"StudentName" => $StudentName,
//							"NotifyUser" => $ParentName,
//							"TargetUser" => $ParentUserLogin
//						);
//				$ReturnMessage = "";
//				$Result = $asl->NotifyStudentStatus($ASLParam, $ReturnMessage);
//				$ReturnArray[] = array($Result,$ReturnMessage);
//				if($debug_log){
//					$log_content .= $asl->postData."\n".$Result."\n".$ReturnMessage."\n\n";
//				}
//			}
//			
//			if($debug_log){
//				$log_file = $file_path."/file/asl_app_test.txt";
//				$file_handle = fopen($log_file,"a+");
//				fwrite($file_handle,$log_content);
//				fclose($file_handle);
//			}
//			
//			return $ReturnArray;
//		}

		function Notify_Student_Status($ParamArray)
		{
			global $sys_custom;
			
			if ($sys_custom['eClassApp']['disableTapCardPushMessage']) {
				return true;
			}
			else {
				global $PATH_WRT_ROOT, $file_path, $asl_parent_app, $sys_custom, $intranet_root, $eclassAppConfig, $Lang, $plugin;
				$ReturnArray = array();
				$StudentID = $ParamArray['UserID']; // student ID
				$AccessDateTime = date("Y-m-d H:i:s",$ParamArray['AccessDateTime']); // timestamp formatted to YYYY-MM-DD HH:MM:SS 
				$Status = strtoupper($ParamArray['Status']); // Arrival status - A: Arrrive School, L: Leave School
				$CheckParentID = isset($ParamArray['ParentID']);
				$ParentIDArray = $ParamArray['ParentID'];
				
				## Get student name, user login, parent info 
				$NameField = getNameFieldByLang2("u.");
				$ParentNameField = getNameFieldByLang2("pu.");
				
				if ($plugin['eClassApp']) {
	         		$logTable = 'APP_LOGIN_LOG';
	         	}
	         	else {
	         		$logTable = 'INTRANET_API_REQUEST_LOG';
	         	}
				
				$sql = "SELECT DISTINCT 
							u.UserLogin, $NameField as StudentName, pu.UserLogin as ParentUserLogin, $ParentNameField as ParentName, u.UserID as StudentID, pu.UserID as ParentID, u.ChineseName, u.EnglishName
						FROM INTRANET_USER as u 
						INNER JOIN INTRANET_PARENTRELATION as p ON p.StudentID = '$StudentID' 
						INNER JOIN INTRANET_USER as pu ON p.ParentID = pu.UserID AND pu.RecordStatus = 1 AND pu.RecordType = '".USERTYPE_PARENT."' 
						INNER JOIN $logTable as rl ON (rl.UserID = p.ParentID OR rl.UserID = p.StudentID) 
						WHERE u.UserID = '$StudentID' ";
				if($CheckParentID){
					$sql .= " pu.UserID IN ('".implode("','",(array)$ParentIDArray)."') ";
				}
				$data = $this->returnArray($sql);
				$numOfData = count($data);
				
				if ($plugin['eClassApp']) {
					include_once($PATH_WRT_ROOT."includes/libuser.php");
					$luser = new libuser();
					
					include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
					$leClassApp = new libeClassApp();
					$eClassAppSettingsObj = $leClassApp->getAppSettingsObj();
					
					$enableArrivalMsg = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']);
					$enableLeaveMsg = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']);
					
					$sendNonSchoolDayPushMessage = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']);
					$enableNonSchoolDayPushMessage_arrival = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']);
					$enableNonSchoolDayPushMessage_leave = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']);
					$sendForNonSchoolDayPushMessage = false;
					if($sendNonSchoolDayPushMessage && ($enableNonSchoolDayPushMessage_arrival || $enableNonSchoolDayPushMessage_leave))
					{
						$TimeTableSettings = $this->Get_Student_Attend_Time_Setting(date("Y-m-d",$ParamArray['AccessDateTime']),$StudentID);
						if($TimeTableSettings === "NonSchoolDay" && (($enableNonSchoolDayPushMessage_arrival && $Status=='A') || ($enableNonSchoolDayPushMessage_leave && $Status == 'L')))
						{
							$sendForNonSchoolDayPushMessage = true;
						}
					}
					
					if(!$sendForNonSchoolDayPushMessage) // if it is not non-school day, check normal day settings
					{
						if (!$enableArrivalMsg && $Status=='A') {
							return true;
						}
						if (!$enableLeaveMsg && $Status=='L') {
							return true;
						}
					}

					if($sys_custom['StudentAttendance']['HostelAttendance_Reason'] != true) {
						if($Status == 'HA' || $Status == 'HL') {
							return true;
						}
					}

					$formAry = $luser->Get_User_Studying_Form($StudentID);
					$formId = $formAry[0]['YearID'];
					$settingKey = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_'.$formId;
					$enablePushMessage = $eClassAppSettingsObj->getSettingsValue($settingKey);
					
					if ($enablePushMessage && in_array($StudentID, $luser->getStudentWithParentUsingParentApp())) {
						include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
						$libeClassApp = new libeClassApp();
						
						switch ($Status) {
							case 'A':
								$StatusText = $Lang['StudentAttendance']['StudentTapCardStatusAry']['Arrive'];
								$_messageTitle = $Lang['StudentAttendance']['StudentAttendance']['Arrive'];
								$_messageContent = $Lang['StudentAttendance']['StudentTapCardPushMessage']['Arrive'];
								break;
							case 'L':
								$StatusText = $Lang['StudentAttendance']['StudentTapCardStatusAry']['Leave'];
								$_messageTitle = $Lang['StudentAttendance']['StudentAttendance']['Leave'];
								$_messageContent = $Lang['StudentAttendance']['StudentTapCardPushMessage']['Leave'];
								
								break;
							case 'HA':
								$StatusText = $Lang['StudentAttendance']['StudentTapCardStatusAry']['HostelArrive'];
								$_messageTitle = $Lang['StudentAttendance']['StudentAttendance']['HostelArrive'];
								$_messageContent = $Lang['StudentAttendance']['StudentTapCardPushMessage']['HostelArrive'];
								break;
							case 'HL':
								$StatusText = $Lang['StudentAttendance']['StudentTapCardStatusAry']['HostelLeave'];
								$_messageTitle = $Lang['StudentAttendance']['StudentAttendance']['HostelLeave'];
								$_messageContent = $Lang['StudentAttendance']['StudentTapCardPushMessage']['HostelLeave'];
								break;
							default: 
								$StatusText = "";
								$_messageTitle = "";
								$_messageContent = "";
						}
						
						for ($i=0; $i<$numOfData; $i++) {
							$_parentId = $data[$i]['ParentID'];
							$_studentId = $data[$i]['StudentID'];
							$_studentName = $data[$i]['StudentName'];
							$_chineseName = $data[$i]['ChineseName'];
							$_englishName = $data[$i]['EnglishName'];
							
							
							$_messageContent = str_replace('<!--StudentNameCh-->', $_chineseName, $_messageContent);
							$_messageContent = str_replace('<!--StudentNameEn-->', $_englishName, $_messageContent);
							$_messageContent = str_replace('<!--DateTime-->', $AccessDateTime, $_messageContent);
							
							$_individualMessageInfoAry = array();
							$_individualMessageInfoAry[0]['relatedUserIdAssoAry'][$_parentId] = array($_studentId);
							
							$ReturnArray[$i] = $libeClassApp->sendPushMessage($_individualMessageInfoAry, $_messageTitle, $_messageContent, $isPublic='', $recordStatus=0);
							//$ReturnArray[$i] = $libeClassApp->sendPushMessage($_individualMessageInfoAry, $_messageTitle, $_messageContent, $isPublic='', $recordStatus=0, $eclassAppConfig['appType']['Parent'], $eclassAppConfig['pushMessageSendMode']['scheduled'], $AccessDateTime);
						}
					}
				}
				else {
					// asl
					include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
					include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
				
					$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
				
					$debug_log = $sys_custom['ASLParentAppDebugLog'];
					$log_content = "";
					
					//$SchoolID = $_SERVER['SERVER_NAME'];
					//$SchoolID = "junior20-b5-demo2.eclass.com.hk";
					if(isset($asl_parent_app['SchoolID'])){
						$SchoolID = $asl_parent_app['SchoolID'];
					}else{
						$SchoolID = $_SERVER['SERVER_NAME'];
					}
					
					## Notify all parents of the student
					for($i=0;$i<sizeof($data);++$i){
						$StudentUserLogin = $data[$i]['UserLogin'];
						$StudentName = $data[$i]['StudentName'];
						$ParentUserLogin = $data[$i]['ParentUserLogin'];
						$ParentName = $data[$i]['ParentName'];
						
						$ASLParam = array(
									"SchID" => $SchoolID,
									"UserName" => $StudentUserLogin,
									"Status" => $Status,
									"AccessDateTime" => $AccessDateTime,
									"StudentName" => $StudentName,
									"NotifyUser" => $ParentName,
									"TargetUser" => $ParentUserLogin
								);
						$ReturnMessage = "";
						$Result = $asl->NotifyStudentStatus($ASLParam, $ReturnMessage);
						$ReturnArray[] = array($Result,$ReturnMessage);
						if($debug_log){
							$log_content .= $asl->postData."\n".$Result."\n".$ReturnMessage."\n\n";
						}
					}
					
					if($debug_log){
						$log_file = $file_path."/file/asl_app_test.txt";
						$file_handle = fopen($log_file,"a+");
						fwrite($file_handle,$log_content);
						fclose($file_handle);
					}
				}
				
				
				return $ReturnArray;
			}
		}
		
		######################################################
		## Get all parent ID that can access parent app 
		######################################################
		function Get_API_Request_ParentID()
		{
			global $plugin;
			
			if ($plugin['eClassApp']) {
         		$logTable = 'APP_LOGIN_LOG';
         	}
         	else {
         		$logTable = 'INTRANET_API_REQUEST_LOG';
         	}
         	
			$sql = "SELECT DISTINCT u.UserID 
					FROM INTRANET_USER as u INNER JOIN $logTable as r ON r.UserID = u.UserID 
					WHERE u.RecordStatus = 1 AND u.RecordType = '".USERTYPE_PARENT."' ";
			return $this->returnVector($sql);
		}
		
		function Remove_Whole_Day_Data($TargetDate, $TargetClass='', $TargetDayType='')
		{
			$date_arr = explode("-",$TargetDate);
			$year = $date_arr[0];
			$month = $date_arr[1];
			$day = $date_arr[2];
			$result = array();
			
			##### Selected Class?
			if($TargetClass)
			{
				$sql1 = "Select UserID From YEAR_CLASS_USER where YearClassID='". $TargetClass ."'";
				$sql1_result = $this->returnVector($sql1);
				$studentIdCond1 = " AND StudentID IN ('".implode("','",$sql1_result)."') ";
				$studentIdCond2 = " AND UserID IN ('".implode("','",$sql1_result)."') ";
				$classIdCond = " AND ClassID ='". $TargetClass ."' ";
			}
			
			##### Selected DayType?
			if($TargetDayType)
			{
				$dayTypeCond = " AND DayType=". $TargetDayType ." ";
			}
			
			$sql = "SELECT DISTINCT StudentID, DayType FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate = '".$TargetDate."' ";
			$sql .= $studentIdCond1 . $dayTypeCond;
			$profile_students = $this->returnArray($sql);
			for($i=0;$i<count($profile_students);$i++){
				$result['ClearProfile'.$profile_students[$i]['StudentID']] = $this->Clear_Profile($profile_students[$i]['StudentID'],$TargetDate,$profile_students[$i]['DayType']);
			}
						
			$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' " . $studentIdCond1 . $dayTypeCond;
			$result['remove_profile_reason'] = $this->db_db_query($sql);
			$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate LIKE '$TargetDate%' " . $studentIdCond2 . $dayTypeCond;
			$result['remove_profile'] = $this->db_db_query($sql);
			
			if(empty($TargetDayType))	# Whloe day > delete record
			{
				$sql = "DELETE FROM CARD_STUDENT_DAILY_LOG_".$year."_".$month." WHERE DayNumber = '".$day."' " . $studentIdCond2;
				$result['remove_dailylog'] = $this->db_db_query($sql);
			}
			else{	# Day Type > update record
				if($TargetDayType==2)	#AM
				{
					$fields = "AMStatus = Null, AMConfirmedByAdmin = Null, IsConfirmed = Null, ConfirmedUserID = Null, DateModified = Null, ModifyBy = Null ";
				} 
				if($TargetDayType==3)	#PM
				{
					$fields = "PMStatus = Null, PMConfirmedByAdmin = Null, PMIsConfirmed = Null, PMConfirmedUserID = Null, PMDateModified = Null, PMModifyBy = Null ";
				}
				$sql = "update CARD_STUDENT_DAILY_LOG_".$year."_".$month." set ". $fields . "WHERE DayNumber = '".$day."' " . $studentIdCond2;
				$result['update_dailylog'] = $this->db_db_query($sql);
			}

			$sql = "DELETE FROM CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month." WHERE DayNumber = '".$day."' " . $classIdCond . $dayTypeCond;
			$result['remove_class_confirm'] = $this->db_db_query($sql);
			
			if(empty($TargetClass) && empty($TargetDayType))
			{
				$sql = "DELETE FROM CARD_STUDENT_DAILY_GROUP_CONFIRM_".$year."_".$month." WHERE DayNumber = '".$day."' ";
				$result['remove_group_confirm'] = $this->db_db_query($sql);
				$sql = "DELETE FROM CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$year."_".$month." WHERE DayNumber = '".$day."'";
				$result['remove_subjectgroup_confirm'] = $this->db_db_query($sql);
				$sql = "DELETE FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '".$TargetDate."' ";
				$result['remove_data_confirm'] = $this->db_db_query($sql);
				$sql = "DELETE FROM CARD_STUDENT_CACHED_CONFIRM_STATUS WHERE TargetDate = '".$TargetDate."' ";
				$result['remove_cached_confirm_status'] = $this->db_db_query($sql);
			}
			
			if(empty($TargetDayType))
			{
				$sql = "DELETE FROM CARD_STUDENT_BAD_ACTION WHERE RecordDate = '".$TargetDate."' " . $studentIdCond1;
				$result['remove_bad_action'] = $this->db_db_query($sql);
			}
			$sql = "DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='".$TargetDate."'" . $studentIdCond1 . $dayTypeCond;
			$result['remove_daily_remark'] = $this->db_db_query($sql);
			$sql = "DELETE FROM CARD_STUDENT_STATUS_SESSION_COUNT WHERE RecordDate='".$TargetDate."'" . $studentIdCond1 . $dayTypeCond;
			$result['remove_session'] = $this->db_db_query($sql);
			
			if(empty($TargetDayType))
			{
				$sql = "DELETE FROM CARD_STUDENT_ENTRY_LOG_".$year."_".$month." WHERE DayNumber = '".$day."' " . $studentIdCond2;
				$this->db_db_query($sql);
				//$result['remove_entrylog'] = $this->db_db_query($sql);
				$sql = "DELETE FROM CARD_STUDENT_RAW_LOG_".$year."_".$month." WHERE DayNumber = '".$day."' " . $studentIdCond2;
				$this->db_db_query($sql);
				//$result['remove_rawlog'] = $this->db_db_query($sql);
			}
			
			##### check and remove record if AM and PM both NULL
			if($TargetDayType)
			{
				$sql = "select UserID from CARD_STUDENT_DAILY_LOG_".$year."_".$month." where AMStatus is Null and PMStatus is Null";
				$sql_result = $this->returnVector($sql);
			
				foreach($sql_result as $key => $this_UserID)
				{
					$sql = "DELETE FROM CARD_STUDENT_DAILY_LOG_".$year."_".$month." WHERE DayNumber = '".$day."' and UserID ='". $this_UserID ."' ";
					$result['remove_dailylog'] = $this->db_db_query($sql);
					$sql = "DELETE FROM CARD_STUDENT_BAD_ACTION WHERE RecordDate = '".$TargetDate."' and StudentID ='". $this_UserID ."' ";
					$result['remove_bad_action'] = $this->db_db_query($sql);
					$sql = "DELETE FROM CARD_STUDENT_ENTRY_LOG_".$year."_".$month." WHERE DayNumber = '".$day."' and UserID ='". $this_UserID ."' ";
					$this->db_db_query($sql);
					$sql = "DELETE FROM CARD_STUDENT_RAW_LOG_".$year."_".$month." WHERE DayNumber = '".$day."' and UserID ='". $this_UserID ."' ";
					$this->db_db_query($sql);
				}
			}
			
			return !in_array(false,$result);
		}
		
		
		
		
		  function retrieveGroupMonthDataByDateRange($group_id, $table=array())
	  	  {
	  	  	    # Get Student List
	            $sql = "SELECT a.UserID FROM INTRANET_USER a, INTRANET_GROUP b, INTRANET_USERGROUP c WHERE a.RecordStatus IN (0,1,2) AND a.RecordType = 2 AND b.groupid = '".$this->Get_Safe_Sql_Query($group_id)."' AND b.groupid = c.groupid AND c.userid = a.userid";
	            $temp = $this->returnVector($sql);
	            $list = sizeof($temp)>0?implode(",",$temp):"-1";
	            
        		$log_entries = array();
        
        	for ($i=0; $i <sizeof($table); $i++)
				{
				$parts = explode("_",$table[$i]);
				$year = $parts[4];
				$month = $parts[5];
		  	
	     
	            # Get Daily Records
	            $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	           	
				$sql = "SELECT 
			            b.UserID, 
			            DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d'), 
			            b.AMStatus, 
			            b.PMStatus, 
			            b.LeaveStatus,
									c.RecordStatus as am_late_waive,
									d.RecordStatus as pm_late_waive,
									e.RecordStatus as am_absent_waive,
									f.RecordStatus as pm_absent_waive,
									g.RecordStatus as am_early_leave_waive,
									h.RecordStatus as pm_early_leave_waive,
									c.Reason as am_late_reason,
									d.Reason as pm_late_reason,
									e.Reason as am_absent_reason,
									f.Reason as pm_absent_reason,
									g.Reason as am_early_leave_reason,
									h.Reason as pm_early_leave_reason,
									i.LateSession as am_late_session,
									i.RequestLeaveSession as am_request_leave_session,
									i.PlayTruantSession as am_play_truant_session,
									i.OfficalLeaveSession as am_offical_leave_session,
									i.AbsentSession as am_absent_session,
									j.LateSession as pm_late_session,
									j.RequestLeaveSession as pm_request_leave_session,
									j.PlayTruantSession as pm_play_truant_session,
									j.OfficalLeaveSession as pm_offical_leave_session,
									j.AbsentSession as pm_absent_session 
								FROM 
									INTRANET_USER as a 
									INNER JOIN 
									$card_log_table_name as b 
									on 
										a.UserID IN ($list) 
										AND 
										a.UserID = b.UserID ";
				if ($this->EnableEntryLeavePeriod) {
					$sql .= "
									INNER JOIN 
									CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
									on b.UserID = selp.UserID 
										and 
										DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
									";
				}
				$sql .= "
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
									ON 
										c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND c.StudentID = b.UserID 
										AND c.DayType = '".PROFILE_DAY_TYPE_AM."' AND c.RecordType = '".PROFILE_TYPE_LATE."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
									ON 
										d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND d.StudentID = b.UserID 
										AND d.DayType = '".PROFILE_DAY_TYPE_PM."' AND d.RecordType = '".PROFILE_TYPE_LATE."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
									ON 
										e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND e.StudentID = b.UserID 
										AND e.DayType = '".PROFILE_DAY_TYPE_AM."' AND e.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.AMStatus='".PROFILE_TYPE_ABSENT."' AND b.AMStatus=e.RecordType 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
									ON 
										f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND f.StudentID = b.UserID 
										AND f.DayType = '".PROFILE_DAY_TYPE_PM."' AND f.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.PMStatus='".PROFILE_TYPE_ABSENT."' AND b.PMStatus=f.RecordType 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
									ON 
										g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND g.StudentID = b.UserID 
										AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
									ON 
										h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND h.StudentID = b.UserID 
										AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
									LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
									ON 
										i.StudentID = b.UserID 
										AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
										AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
										AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
									LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
									ON 
										j.StudentID = b.UserID 
										AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
										AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
										AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
								ORDER BY  DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d')";

				if($list!="") $temp_log_entries = $this->returnArray($sql,21);
				
					for ($j= 0; $j <sizeof($temp_log_entries); $j++){
				
					$log_entries[] = $temp_log_entries[$j];
							
				}
   			    }
        	
        	
	            for ($i=0; $i<sizeof($log_entries); $i++)
				{
				       list ($studentid, $day, $am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $log_entries[$i];
				       $result[$studentid][$day] = array($am,$pm,$leave,$am_late_waive,$pm_late_waive,$am_absent_waive,$pm_absent_waive,$am_early_waive,$pm_early_waive,$am_late_reason,$pm_late_reason,$am_absent_reason,$pm_absent_reason,$am_early_leave_reason,$pm_early_leave_reason,$am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session);
				}
				
	      return $result;
	   }
		
		
		
		function retrieveClassMonthDataByDateRange($ClassName, $table=array())
		{
			
		  $log_entries = array();   
		
	  	 for ($i=0; $i <sizeof($table); $i++)
		 {
		  $parts = explode("_",$table[$i]);
		  $year = $parts[4];
		  $month = $parts[5];
		  		
		  $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
		
		  $sql = "SELECT 
		            a.UserID, 
		            DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d'), 
		            b.AMStatus, 
		            b.PMStatus, 
		            b.LeaveStatus,
								c.RecordStatus as am_late_waive,
								d.RecordStatus as pm_late_waive,
								e.RecordStatus as am_absent_waive,
								f.RecordStatus as pm_absent_waive,
								g.RecordStatus as am_early_leave_waive,
								h.RecordStatus as pm_early_leave_waive,
								c.Reason as am_late_reason,
								d.Reason as pm_late_reason,
								e.Reason as am_absent_reason,
								f.Reason as pm_absent_reason,
								g.Reason as am_early_leave_reason,
								h.Reason as pm_early_leave_reason,
								i.LateSession as am_late_session,
								i.RequestLeaveSession as am_request_leave_session,
								i.PlayTruantSession as am_play_truant_session,
								i.OfficalLeaveSession as am_offical_leave_session,
								i.AbsentSession as am_absent_session,
								j.LateSession as pm_late_session,
								j.RequestLeaveSession as pm_request_leave_session,
								j.PlayTruantSession as pm_play_truant_session,
								j.OfficalLeaveSession as pm_offical_leave_session,
								j.AbsentSession as pm_absent_session 
							FROM 
								INTRANET_USER as a
								INNER JOIN 
								$card_log_table_name as b 
								ON a.UserID = b.UserID 
									AND a.ClassName = '".$this->Get_Safe_Sql_Query($ClassName)."' 
									AND a.RecordType = 2
									AND a.RecordStatus IN (0,1,2)  ";
			if ($this->EnableEntryLeavePeriod) {
				$sql .= "
								INNER JOIN 
								CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
								on b.UserID = selp.UserID 
									and 
									DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
								";
			}
			$sql .= "
								LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
								ON 
									c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
									AND c.StudentID = a.UserID 
									AND c.DayType = '".PROFILE_DAY_TYPE_AM."' AND c.RecordType = '".PROFILE_TYPE_LATE."' 
								LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
								ON 
									d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
									AND d.StudentID = a.UserID 
									AND d.DayType = '".PROFILE_DAY_TYPE_PM."' AND d.RecordType = '".PROFILE_TYPE_LATE."' 
								LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
								ON 
									e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
									AND e.StudentID = a.UserID 
									AND e.DayType = '".PROFILE_DAY_TYPE_AM."' AND e.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.AMStatus='".PROFILE_TYPE_ABSENT."' AND b.AMStatus=e.RecordType 
								LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
								ON 
									f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
									AND f.StudentID = a.UserID 
									AND f.DayType = '".PROFILE_DAY_TYPE_PM."' AND f.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.PMStatus='".PROFILE_TYPE_ABSENT."' AND b.PMStatus=f.RecordType 
								LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
								ON 
									g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
									AND g.StudentID = a.UserID 
									AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
								LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
								ON 
									h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
									AND h.StudentID = a.UserID 
									AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
								LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
								ON 
									i.StudentID = a.UserID 
									AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
									AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
									AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
								LEFT OUTER JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
								ON 
									j.StudentID = a.UserID 
									AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
									AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
									AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
						ORDER BY  DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d')";
		
		  //$log_entries = $this->returnArray($sql,21);
		  // $temp_log_entries = $this->returnVector($sql,21);
		   
		   $temp_log_entries = $this->returnArray($sql,21);
		   	  for ($j= 0; $j <sizeof($temp_log_entries); $j++){
				
			 $log_entries[] = $temp_log_entries[$j];
		  }
		 }
		 
		 
	
		  for ($i=0; $i<sizeof($log_entries); $i++)
		  {
				list ($studentid, $day, $am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $log_entries[$i];
				$result[$studentid][$day] = array($am,$pm,$leave,$am_late_waive,$pm_late_waive,$am_absent_waive,$pm_absent_waive,$am_early_waive,$pm_early_waive,$am_late_reason,$pm_late_reason,$am_absent_reason,$pm_absent_reason,$am_early_leave_reason,$pm_early_leave_reason,$am_late_session,$am_request_leave_session,$am_play_truant_session,$am_offical_leave_session,$am_absent_session,$pm_late_session,$pm_request_leave_session,$pm_play_truant_session,$pm_offical_leave_session,$pm_absent_session);
			}
		  return $result;
		}
			
		
		function retrieveCustomizedIndividualMonthDataByDateRange($UserList, $table=array()) {
			
		    $log_entries = array();
		    $UserList = (sizeof($UserList) > 0)? "'".implode("','",$this->Get_Safe_Sql_Query($UserList))."'":'-1';	
		    for ($i=0; $i <sizeof($table); $i++)
			{
			$parts = explode("_",$table[$i]);
			$year = $parts[4];
			$month = $parts[5];
						
			 $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
			
					//DISTINCT DATE_FORMAT(CONCAT('$tmp_year-$tmp_month-',b.DayNumber),'%Y-%m-%d')  
			
			  $sql = "SELECT 
			            b.UserID, 
			            DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d'), 
			            b.AMStatus, 
			            b.PMStatus, 
			            b.LeaveStatus,
									c.RecordStatus as am_late_waive,
									d.RecordStatus as pm_late_waive,
									e.RecordStatus as am_absent_waive,
									f.RecordStatus as pm_absent_waive,
									g.RecordStatus as am_early_leave_waive,
									h.RecordStatus as pm_early_leave_waive,
									c.Reason as am_late_reason,
									d.Reason as pm_late_reason,
									e.Reason as am_absent_reason,
									f.Reason as pm_absent_reason,
									g.Reason as am_early_leave_reason,
									h.Reason as pm_early_leave_reason,
									i.LateSession as am_late_session,
									i.RequestLeaveSession as am_request_leave_session,
									i.PlayTruantSession as am_play_truant_session,
									i.OfficalLeaveSession as am_offical_leave_session,
									i.AbsentSession as am_absent_session,
									j.LateSession as pm_late_session,
									j.RequestLeaveSession as pm_request_leave_session,
									j.PlayTruantSession as pm_play_truant_session,
									j.OfficalLeaveSession as pm_offical_leave_session,
									j.AbsentSession as pm_absent_session 
								FROM 
									$card_log_table_name as b ";
				if ($this->EnableEntryLeavePeriod) {
					$sql .= "
									INNER JOIN 
									CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
									on b.UserID = selp.UserID 
										and 
										DATE(CONCAT('".$year."-".$month."-',b.DayNumber)) between selp.PeriodStart and selp.PeriodEnd 
									";
				}
				$sql .= "
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
									ON 
										c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND c.StudentID = b.UserID 
										AND c.DayType = '".PROFILE_DAY_TYPE_AM."' AND c.RecordType = '".PROFILE_TYPE_LATE."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d 
									ON 
										d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND d.StudentID = b.UserID 
										AND d.DayType = '".PROFILE_DAY_TYPE_PM."' AND d.RecordType = '".PROFILE_TYPE_LATE."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
									ON 
										e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND e.StudentID = b.UserID 
										AND e.DayType = '".PROFILE_DAY_TYPE_AM."' AND e.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.AMStatus='".PROFILE_TYPE_ABSENT."' AND b.AMStatus=e.RecordType 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f 
									ON 
										f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND f.StudentID = b.UserID 
										AND f.DayType = '".PROFILE_DAY_TYPE_PM."' AND f.RecordType = '".PROFILE_TYPE_ABSENT."' AND b.PMStatus='".PROFILE_TYPE_ABSENT."' AND b.PMStatus=f.RecordType 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
									ON 
										g.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND g.StudentID = b.UserID 
										AND g.DayType = '".PROFILE_DAY_TYPE_AM."' AND g.RecordType = '".PROFILE_TYPE_EARLY."' 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as h 
									ON 
										h.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber)) 
										AND h.StudentID = b.UserID 
										AND h.DayType = '".PROFILE_DAY_TYPE_PM."' AND h.RecordType = '".PROFILE_TYPE_EARLY."'  
									LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as i 
									ON 
										i.StudentID = b.UserID 
										AND DATE_FORMAT(i.RecordDate,'%Y%m') = '".$year.$month."' 
										AND DAYOFMONTH(i.RecordDate) = b.DayNumber 
										AND i.DayType = '".PROFILE_DAY_TYPE_AM."' 
									LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as j 
									ON 
										j.StudentID = b.UserID 
										AND DATE_FORMAT(j.RecordDate,'%Y%m') = '".$year.$month."' 
										AND DAYOFMONTH(j.RecordDate) = b.DayNumber 
										AND j.DayType = '".PROFILE_DAY_TYPE_PM."' 
							where 
								b.UserID in (".$UserList.") 
							ORDER BY DATE_FORMAT(CONCAT('$year-$month-',b.DayNumber),'%Y-%m-%d')";

			       $temp_log_entries = $this->returnArray($sql,21);
	
			 		 for ($j= 0; $j <sizeof($temp_log_entries); $j++){
		
					$log_entries[] = $temp_log_entries[$j];		
				
			       }
			  }		
		    
		     for ($i=0; $i<sizeof($log_entries); $i++)
			  {
					list ($studentid, $day, $am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $log_entries[$i];
					$result[$studentid][$day] = array($am,$pm,$leave,$am_late_waive,$pm_late_waive,$am_absent_waive,$pm_absent_waive,$am_early_waive,$pm_early_waive,$am_late_reason,$pm_late_reason,$am_absent_reason,$pm_absent_reason,$am_early_leave_reason,$pm_early_leave_reason,$am_late_session,$am_request_leave_session,$am_play_truant_session,$am_offical_leave_session,$am_absent_session,$pm_late_session,$pm_request_leave_session,$pm_play_truant_session,$pm_offical_leave_session,$pm_absent_session);		
			  }
   
			  
		  return $result;
		}
		
		function Get_Preset_Absent_Student_Records($StudentIdAry,$StartDate,$EndDate)
		{
			$sql = "SELECT 
						StudentID,
						RecordDate,
						DayPeriod,
						Reason,
						Waive 
					FROM CARD_STUDENT_PRESET_LEAVE
					WHERE StudentID IN (".implode(",",IntegerSafe((array)$StudentIdAry)).") 
						AND RecordDate >= '".$this->Get_Safe_Sql_Query($StartDate)."' AND RecordDate <= '".$this->Get_Safe_Sql_Query($EndDate)."' 
					ORDER BY RecordDate,StudentID,DayPeriod";
			$result = $this->returnResultSet($sql);
			
			return $result;
		}
		
		// Get School Holiday Events and Holiday Events
		function Get_School_Holiday_Events($StartDate,$EndDate)
		{
			$sql = "SELECT 
						EventID,
						Title,
						DATE_FORMAT(EventDate,'%Y-%m-%d') as EventDate 
					FROM INTRANET_EVENT 
					WHERE RecordType IN (3,4) 
						AND EventDate >= '".$this->Get_Safe_Sql_Query($StartDate)." 00:00:00' 
						AND EventDate <= '".$this->Get_Safe_Sql_Query($EndDate)." 23:59:59' 
					ORDER BY EventDate";
			$result = $this->returnResultSet($sql);
			
			return $result;
		}
		
		/*
		 * @param array $studentIdAry
		 * @param int $academicYearId : if empty string, will use current academic year ID
		 * @param int or string : set 1 to exclude waived Late/Absent/EarlyLeave records
		 * @return array[StudentUserID][YYYY-MM][StatusString][]=RecordDate where StatusString can be 'Present' | 'Late' | 'Absent' | 'EarlyLeave' | 'Outing'
		 */
		function getAttendanceStatusDataByAcademicYear($studentIdAry, $academicYearId='', $excludeWaivedRecord='') 
		{
			if($academicYearId == '') {
				$academicYearId = Get_Current_Academic_Year_ID();
			}
			$start_ts = getStartOfAcademicYear('', $academicYearId);
			$end_ts = getEndOfAcademicYear('', $academicYearId);
			$start_date = date("Y-m-d",$start_ts);
			$end_date = date("Y-m-d",$end_ts);
			
			$studentIdAry = (array)$studentIdAry;
			$studentIdCond = " AND d.UserID IN ('".implode("','",$studentIdAry)."') ";
			
			$resultAry = array(); // [UserID][YYYY-MM][StatusString][] = RecordDate
			
			$current_ts = $start_ts;
			$current_y = date("Y",$current_ts);
			$current_m = date("m",$current_ts);
			$current_d = date("d",$current_ts);
			
			$waivedRecordAry = array(); // [StudentID][YYYY-MM-DD][DayType][RecordType] = 1;
			if($excludeWaivedRecord != '' && $excludeWaivedRecord) {
				$sql = "SELECT StudentID,RecordDate,DayType,RecordType,RecordStatus 
						FROM CARD_STUDENT_PROFILE_RECORD_REASON 
						WHERE RecordStatus='1' AND StudentID IN ('".implode("','",$studentIdAry)."') 
							AND (RecordDate BETWEEN '$start_date' AND '$end_date')";
				$records = $this->returnResultSet($sql);
				$record_count = count($records);
				
				for($i=0;$i<$record_count;$i++) {
					$student_id = $records[$i]['StudentID'];
					$record_date = $records[$i]['RecordDate'];
					$day_type = $records[$i]['DayType'];
					$record_type = $records[$i]['RecordType'];
					$record_status = $records[$i]['RecordStatus'];
					
					if(!isset($waivedRecordAry[$student_id])) {
						$waivedRecordAry[$student_id] = array();
					}
					if(!isset($waivedRecordAry[$student_id][$record_date])) {
						$waivedRecordAry[$student_id][$record_date] = array();
					}
					if(!isset($waivedRecordAry[$student_id][$record_date][$day_type])) {
						$waivedRecordAry[$student_id][$record_date][$day_type] = array();
					}
					if(!isset($waivedRecordAry[$student_id][$record_date][$day_type][$record_type])) {
						$waivedRecordAry[$student_id][$record_date][$day_type][$record_type] = array();
					}
					$waivedRecordAry[$student_id][$record_date][$day_type][$record_type] = $record_status;
				}
			}
			
			for($current_ts=$start_ts;$current_ts<=$end_ts;$current_ts = mktime(0,0,0,intval($current_m)+1,intval($current_d),intval($current_y)))
			{
				$current_y = date("Y",$current_ts);
				$current_m = date("m",$current_ts);
				$current_d = date("d",$current_ts);
				$year_month = $current_y."-".$current_m;
				
				$dailylog_table = "CARD_STUDENT_DAILY_LOG_".$current_y."_".$current_m;
				$date_range_cond = " AND (DATE_FORMAT(CONCAT('$current_y-$current_m-',d.DayNumber),'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date') ";
				
				$sql = "SELECT 
							d.UserID,
							DATE_FORMAT(CONCAT('$current_y-$current_m-',d.DayNumber),'%Y-%m-%d') as RecordDate,
							d.AMStatus,
							d.PMStatus,
							d.LeaveStatus 
						FROM INTRANET_USER as u  
						INNER JOIN $dailylog_table as d ON d.UserID=u.UserID 
						
						WHERE u.RecordStatus='1' AND u.RecordType='".USERTYPE_STUDENT."'
							$studentIdCond 
							$date_range_cond 
						ORDER BY u.UserID,d.DayNumber";
				
				$records = $this->returnResultSet($sql);
				$record_count = count($records);
				
				for($i=0;$i<$record_count;$i++) {
					$user_id = $records[$i]['UserID'];
					$record_date = $records[$i]['RecordDate'];
					$am_status = trim($records[$i]['AMStatus']);
					$pm_status = trim($records[$i]['PMStatus']);
					$leave_status = trim($records[$i]['LeaveStatus']);
					
					if(!isset($resultAry[$user_id])) {
						$resultAry[$user_id] = array();
					}
					if(!isset($resultAry[$user_id][$year_month])) {
						$resultAry[$user_id][$year_month] = array();
						$resultAry[$user_id][$year_month]['Present'] = array();
						$resultAry[$user_id][$year_month]['Late'] = array();
						$resultAry[$user_id][$year_month]['Absent'] = array();
						$resultAry[$user_id][$year_month]['EarlyLeave'] = array();
						$resultAry[$user_id][$year_month]['Outing'] = array();
					}
					// Early Leave
					if($leave_status == CARD_LEAVE_AM || $leave_status == CARD_LEAVE_PM) {
						if($excludeWaivedRecord != '' && $excludeWaivedRecord){
							if($leave_status == CARD_LEAVE_AM && $waivedRecordAry[$user_id][$record_date][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]!=1) {
								$resultAry[$user_id][$year_month]['EarlyLeave'][] = $record_date;
							}
							if($leave_status == CARD_LEAVE_PM && $waivedRecordAry[$user_id][$record_date][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]!=1) {
								$resultAry[$user_id][$year_month]['EarlyLeave'][] = $record_date;
							}
						}else{
							$resultAry[$user_id][$year_month]['EarlyLeave'][] = $record_date;
						}
					}
					
					if($am_status == CARD_STATUS_PRESENT){
						$resultAry[$user_id][$year_month]['Present'][] = $record_date;
					}
					
					if($pm_status == CARD_STATUS_PRESENT){
						$resultAry[$user_id][$year_month]['Present'][] = $record_date;
					}
					
					if($am_status == CARD_STATUS_LATE){
						if($excludeWaivedRecord != '' && $excludeWaivedRecord){
							if($waivedRecordAry[$user_id][$record_date][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_LATE]!=1) {
								$resultAry[$user_id][$year_month]['Late'][] = $record_date;
							}
						}else{
							$resultAry[$user_id][$year_month]['Late'][] = $record_date;
						}
					}
					
					if($pm_status == CARD_STATUS_LATE){
						if($excludeWaivedRecord != '' && $excludeWaivedRecord){
							if($waivedRecordAry[$user_id][$record_date][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_LATE]!=1) {
								$resultAry[$user_id][$year_month]['Late'][] = $record_date;
							}
						}else{
							$resultAry[$user_id][$year_month]['Late'][] = $record_date;
						}
					}
					
					if($am_status == CARD_STATUS_ABSENT){
						if($excludeWaivedRecord != '' && $excludeWaivedRecord){
							if($waivedRecordAry[$user_id][$record_date][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_ABSENT]!=1) {
								$resultAry[$user_id][$year_month]['Absent'][] = $record_date;
							}
						}else{
							$resultAry[$user_id][$year_month]['Absent'][] = $record_date;
						}
					}
					
					if($pm_status == CARD_STATUS_ABSENT){
						if($excludeWaivedRecord != '' && $excludeWaivedRecord){
							if($waivedRecordAry[$user_id][$record_date][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_ABSENT]!=1) {
								$resultAry[$user_id][$year_month]['Absent'][] = $record_date;
							}
						}else{
							$resultAry[$user_id][$year_month]['Absent'][] = $record_date;
						}
					}
					
					if($am_status == CARD_STATUS_OUTING) {
						$resultAry[$user_id][$year_month]['Outing'][] = $record_date;
					}
					
					if($pm_status == CARD_STATUS_OUTING) {
						$resultAry[$user_id][$year_month]['Outing'][] = $record_date;
					}
				} // end looping records
				
			} // end looping timestamp range
			
			return $resultAry;
		}
		
		function Clone_Class_Lesson_Attendance_Overview_And_Students($TargetDate, $AttendOverviewIdToClone) 
		{
			
			// build necessary attendance table for attendance taking
			$this->Build_Subject_Group_Attend_Overview();
			$this->Build_Subject_Group_Student_Attendance();
			
			$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
			
			list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
			
			$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
			if ($CycleDayInfo[0] == 'Cycle') {
				$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
			}
			
			$SpecialTimeTableIdRecord = $this->Get_Lesson_Special_Time_Table($TargetDate);
			$is_special_timetable = count($SpecialTimeTableIdRecord)>0;
			
			$cond = '';
			if($is_special_timetable){
				$time_table_id = $SpecialTimeTableIdRecord[0];
				$cond = " and ist.TimetableID='$time_table_id' ";
			}
			
			$sql = 'select * from SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' where AttendOverviewID=\''.$AttendOverviewIdToClone.'\' ';
			$AttendOverviewAry = $this->returnResultSet($sql);
			
			$lessonAry = array();
			$resultAry = array();
			if(count($AttendOverviewAry) > 0) {
			// Get All subject group lesson related to each class on target date
				$sql = 'select
							  yc.YearClassID,
							  stc.SubjectGroupID,
							  stc.ClassTitleEN as SubjectGroupTitleEN,
							  stc.ClassTitleB5 as SubjectGroupTitleB5,
							  itra.RoomAllocationID,
							  itra.TimeSlotID,
							  itt.TimeSlotName,
							  itt.StartTime,
							  itt.EndTime,
							  IFNULL(sgato1.AttendOverviewID,-1) as AttendOverviewID, 
							  s.CH_DES as SubjectNameB5, 
								s.EN_DES as SubjectNameEN
							from ';
				if($is_special_timetable){
					$sql .= 'INTRANET_SCHOOL_TIMETABLE as ist 
							  inner join
							  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							  on 
							    ist.TimeTableID = itra.TimeTableID 
								and itra.Day = '.($CycleDayInfo[0] == 'Cycle'? '\''.$DayNumber.'\'': 'DAYOFWEEK(\''.$TargetDate.'\')-1 ' ).' ';
				}else{
					$sql .=	 ' INTRANET_CYCLE_GENERATION_PERIOD as icgp
							  inner join
							  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
							  on
							    \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
							    and icgp.PeriodID = iptr.PeriodID
							  inner join
							  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							  on
							    iptr.TimeTableID = itra.TimeTableID
							    and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1) ';
				}
					$sql .= ' inner join INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
							  inner join
							  SUBJECT_TERM_CLASS as stc
							  on itra.SubjectGroupID = stc.SubjectGroupID
							  inner join 
							  SUBJECT_TERM as st
							  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\' 
							  inner join 
							  ASSESSMENT_SUBJECT as s 
							  on st.SubjectID = s.RecordID 
							  inner join
							  SUBJECT_TERM_CLASS_USER as stcu
							  on stc.SubjectGroupID = stcu.SubjectGroupID
							  inner join
							  YEAR_CLASS_USER as ycu
							  on stcu.UserID = ycu.UserID
							  inner join
							  YEAR_CLASS as yc
							  on 
							    ycu.YearClassID = yc.YearClassID
							    and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\' 
							  left join 
							  SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as sgato1
							  on
							    stc.SubjectGroupID = sgato1.SubjectGroupID
							    and itra.RoomAllocationID = sgato1.RoomAllocationID
							    and sgato1.LessonDate = \''.$TargetDate.'\' 
								and sgato1.AttendOverviewID != \''.$AttendOverviewIdToClone.'\' 
							where 
								stc.SubjectGroupID=\''.$AttendOverviewAry[0]['SubjectGroupID'].'\' 
								and itt.StartTime >= \''.$AttendOverviewAry[0]['EndTime'].'\' '.$cond.'
							Group By 
							  yc.YearClassID, stc.SubjectGroupID, itra.TimeSlotID';
				
				$lessonAry = $this->returnArray($sql);
				$lesson_count = count($lessonAry);
				if($lesson_count>0){
					// Get lesson list of that day
					$TimeSlotList = $this->Get_Time_Slot_List($TargetDate);
					$timeslot_count = count($TimeSlotList);
					$nextStartTimeAry = array();
					for($i=0;$i<$timeslot_count;$i++){
						if(($i+1) < $timeslot_count && $TimeSlotList[$i]['StartTime']>=$AttendOverviewAry[0]['StartTime']) {
							$nextStartTimeAry[$TimeSlotList[$i]['StartTime']] = $TimeSlotList[$i+1]['StartTime'];
						}
					}
					$next_lesson_time = $nextStartTimeAry[$AttendOverviewAry[0]['StartTime']];
					
					for($i=0;$i<$lesson_count;$i++) {
						if($next_lesson_time == $lessonAry[$i]['StartTime'] && $lessonAry[$i]['AttendOverviewID'] == '-1'){
							// Copy the attend overview and student records
							$room_allocation_id = $lessonAry[$i]['RoomAllocationID'];
							$start_time = $lessonAry[$i]['StartTime'];
							$end_time = $lessonAry[$i]['EndTime'];
							// clone attend overview
							$clone_overview_sql = "INSERT INTO SUBJECT_GROUP_ATTEND_OVERVIEW_".$AcademicYearID." 
									(SubjectGroupID,LessonDate,RoomAllocationID,StartTime,EndTime,PlanID,PlanName,StudentPositionAMF,PlanLayoutAMF,CreateBy,CreateDate,LastModifiedBy,LastModifiedDate) 
									SELECT SubjectGroupID,LessonDate,'$room_allocation_id' as RoomAllocationID,'$start_time' as StartTime,'$end_time' as EndTime,
										 PlanID,PlanName,StudentPositionAMF,PlanLayoutAMF,'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW() 
									FROM SUBJECT_GROUP_ATTEND_OVERVIEW_".$AcademicYearID." WHERE AttendOverviewID='$AttendOverviewIdToClone'";
							$clone_overview_success = $this->db_db_query($clone_overview_sql);
							
							$resultAry['CloneOverview_'.$room_allocation_id] = $clone_overview_success;
							if($clone_overview_success){
								$newAttendOverviewID = $this->db_insert_id();
								// clone attend students
								$clone_student_sql = "INSERT INTO SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID 
										(AttendOverviewID,StudentID,AttendStatus,Reason,Remarks,CreateBy,CreateDate,LastModifiedBy,LastModifiedDate) 
										SELECT '$newAttendOverviewID' as AttendOverviewID, StudentID, 
											IF(AttendStatus='".CARD_STATUS_LATE."','".CARD_STATUS_PRESENT."',AttendStatus) as AttendStatus,Reason,Remarks,
											'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW() 
										FROM SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID WHERE AttendOverviewID='$AttendOverviewIdToClone'";
								$clone_student_success = $this->db_db_query($clone_student_sql);
								
								$resultAry['CloneStudent_'.$newAttendOverviewID] = $clone_student_success;
							}
							
							$next_lesson_time = $nextStartTimeAry[$lessonAry[$i]['StartTime']];
							
						}
					}
				}
			}
			
			return !in_array(false,$resultAry);
		}
		
		function Get_Lesson_Non_Confirmed_Report_Records($FromDate,$ToDate)
		{
			global $sys_custom;
			$recordAry = array();
			
			$current_academic_year_id = Get_Current_Academic_Year_ID();
			$class_title_field = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
			$subject_group_field = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
			$subject_name_field = Get_Lang_Selection("s.CH_DES","s.EN_DES");	
			$teacher_name_field = getNameFieldByLang("u.");
			
			for($from_ts=strtotime($FromDate),$to_ts=strtotime($ToDate);$from_ts<=$to_ts;$from_ts+=86400)
			{
				$TargetDate = date("Y-m-d",$from_ts);
				
				$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
				list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
				$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($TargetDate);
				if ($CycleDayInfo[0] == 'Cycle') {
					$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
				}
				
				$SpecialTimeTableIdRecord = $this->Get_Lesson_Special_Time_Table($TargetDate);
				$is_special_timetable = count($SpecialTimeTableIdRecord)>0;
				
				$cond = '';
				if($is_special_timetable){
					$time_table_id = $SpecialTimeTableIdRecord[0];
					$cond = " and ist.TimetableID='$time_table_id' ";
				}
				
				$sql = "SELECT 
						  '$TargetDate' as RecordDate,
						  yc.YearClassID,
						  $class_title_field as ClassTitle,
						  stc.SubjectGroupID,
						  $subject_group_field as SubjectGroupTitle,
						  itra.RoomAllocationID,
						  itra.TimeSlotID,
						  itt.TimeSlotName,
						  itt.StartTime,
						  itt.EndTime,
						  $subject_name_field as SubjectName,
						  u.UserLogin,
						  $teacher_name_field as TeacherName 
						FROM ";
				if($is_special_timetable){
					$sql .= "INTRANET_SCHOOL_TIMETABLE as ist 
							  inner join
							  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							  on 
							    ist.TimeTableID = itra.TimeTableID 
								and itra.Day = ".($CycleDayInfo[0] == 'Cycle'? "'".$DayNumber."'": "DAYOFWEEK('".$TargetDate."')-1 " )." ";
				}else{
					$sql.="INTRANET_CYCLE_GENERATION_PERIOD as icgp
						  inner join
						  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
						  on
						    '".$TargetDate."' between icgp.PeriodStart and icgp.PeriodEnd
						    and icgp.PeriodID = iptr.PeriodID
						  inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on
						    iptr.TimeTableID = itra.TimeTableID
						    and itra.Day = IF(icgp.PeriodType=1,'".$DayNumber."', DAYOFWEEK('".$TargetDate."')-1) ";
				}
				$sql.= " inner join INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on itra.SubjectGroupID = stc.SubjectGroupID
						  inner join 
						  SUBJECT_TERM as st
						  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = '".$YearTermID."' 
						  inner join 
						  ASSESSMENT_SUBJECT as s 
						  on st.SubjectID = s.RecordID 
						  inner join
						  SUBJECT_TERM_CLASS_USER as stcu
						  on stc.SubjectGroupID = stcu.SubjectGroupID
						  inner join
						  YEAR_CLASS_USER as ycu
						  on stcu.UserID = ycu.UserID
						  inner join
						  YEAR_CLASS as yc
						  on ycu.YearClassID = yc.YearClassID
						    and yc.AcademicYearID = '".$current_academic_year_id."' 
						  inner join YEAR as y ON y.YearID=yc.YearID 
						  left join SUBJECT_TERM_CLASS_TEACHER as stct ON stct.SubjectGroupID=stc.SubjectGroupID 
						  left join INTRANET_USER as u ON u.UserID=stct.UserID 
						  left join 
						  SUBJECT_GROUP_ATTEND_OVERVIEW_".$AcademicYearID." as sgato1 
						  on 
						    stc.SubjectGroupID = sgato1.SubjectGroupID 
						    and itra.RoomAllocationID = sgato1.RoomAllocationID
						    and sgato1.LessonDate = '".$TargetDate."' 
						where sgato1.AttendOverviewID IS NULL $cond 
						Group By 
						  yc.YearClassID, stc.SubjectGroupID, itra.TimeSlotID ";
				if($sys_custom['LessonAttendance_SortBySubjectGroupClass']){
					$sql .=	" ORDER BY itt.DisplayOrder,s.DisplayOrder,y.Sequence,yc.Sequence";
				}else{
					$sql .=	" ORDER BY y.Sequence,yc.Sequence,itt.StartTime";
				} 
				$resultAry = $this->returnResultSet($sql);
				$recordAry = array_merge($recordAry,$resultAry);
			}
			return $recordAry;
		}
		
		##########################* Function from liblessonattendance [start] *#########################

	// Get Sitting Plans from ATTEND_DEFAULT_PLAN
	function Get_Default_Plan($RecordID,$RecordType="SubjectGroup")
	{
		$NameField = getNameFieldByLang2("u.");
		
		if ($RecordType=="SubjectGroup") {
			$SubjectGroupFloorPlan = array();
			$YearClassFloorPlan = array();
			
			$sql = "SELECT 
								adp.FlashPlanID as PlanID, 
								adp.PlanName, 
								adp.StudentPositionAMF, 
								adp.PlanLayoutAMF,
								adp.LastModifiedDate, 
								".$NameField." as LastModifiedBy 
							FROM 
								ATTEND_DEFAULT_PLAN as adp 
								LEFT JOIN 
								INTRANET_USER as u 
								on adp.LastModifiedBy = u.UserID 
							WHERE 
								adp.SubjectGroupID = '$RecordID' ";
			$SubjectGroupFloorPlan = $this->returnArray($sql);
								
			// check if subject group is class based
			$sql = "select UserID from SUBJECT_TERM_CLASS_USER where SubjectGroupID = '$RecordID'";
			$SubjectGroupUserList = $this->returnVector($sql);
			
			// get subject group's year(form) relation and get all classID related to those year
			$sql = "select 
								YearClassID 
							From 
								SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr 
								INNER JOIN 
								YEAR_CLASS as yc 
								on 
									stcyr.SubjectGroupID = '".$RecordID."' 
									and stcyr.YearID = yc.YearID 
									and yc.AcademicYearID = '".$this->CurAcademicYearID."'";
			$YearClassList = $this->returnVector($sql);
			
			// get year class student from the YearClassList and compare the student list to subject group student list
			for ($i=0; $i< sizeof($YearClassList); $i++) {
				$sql = "select 
									ycu.UserID 
								From 
									YEAR_CLASS_USER as ycu 
									INNER JOIN 
									INTRANET_USER as u 
									on ycu.UserId = u.UserID 
								where 
									YearClassID = '".$YearClassList[$i]."'";
				$YearClassUserList = $this->returnVector($sql);
				
				if (sizeof($SubjectGroupUserList) == sizeof($YearClassUserList)) {
					$Match = true;
					for ($j=0; $j< sizeof($SubjectGroupUserList); $j++) {
						if (!in_array($SubjectGroupUserList[$j],$YearClassUserList)) {
							$Match = false;
							break;
						}
					}
					
					if ($Match) {
						$MatchedYearClassID[] = $YearClassList[$i];
						break;
					}
				}
			}
			
			if (sizeof($MatchedYearClassID) > 0) {
				$Cond = implode(',',$MatchedYearClassID);
				
				$sql = "Select 
									adp.FlashPlanID as PlanID, 
									adp.PlanName, 
									adp.StudentPositionAMF, 
									adp.PlanLayoutAMF,
									adp.LastModifiedDate, 
									".$NameField." as LastModifiedBy 
								FROM 
									ATTEND_DEFAULT_PLAN as adp 
									LEFT JOIN 
									INTRANET_USER as u 
									on adp.LastModifiedBy = u.UserID 
								WHERE 
									adp.YearClassID in (".$Cond.") 
							 ";
				$YearClassFloorPlan = $this->returnArray($sql);
			}
			
			// combine FloorPlan from Year Class and Subject group		
			$Result = array_merge($SubjectGroupFloorPlan,$YearClassFloorPlan);
		}
		else {
			$sql = "SELECT 
								adp.FlashPlanID as PlanID, 
								adp.PlanName, 
								adp.StudentPositionAMF, 
								adp.PlanLayoutAMF,
								adp.LastModifiedDate, 
								".$NameField." as LastModifiedBy 
							FROM 
								ATTEND_DEFAULT_PLAN as adp 
								LEFT JOIN 
								INTRANET_USER as u 
								on adp.LastModifiedBy = u.UserID 
							WHERE 
								adp.YearClassID = '$RecordID' ";
			
			$Result = $this->returnArray($sql);
		}
		
		return $Result;
	}
	
	// Get Subject Group student list from SUBJECT_TERM_CLASS_USER
	function Get_Student_List($RecordID,$RecordType="SubjectGroup",$LessonDate="")
	{
		global $intranet_root, $sys_custom;
		
		$settings = $this->Get_General_Settings('LessonAttendance', array("'DefaultStatus'","'FollowSchoolStatus'"));
		$default_status = $settings['DefaultStatus'] == ''? 0 : $settings['DefaultStatus'];
		$follow_status = $settings['FollowSchoolStatus'] == 1;
		if($sys_custom['LessonAttendance_LaSalleCollege']) $default_status = "-1";
		//debug_pr($_REQUEST);
		//include("settings.php");
		//added the ORDER BY i.ClassNumber, Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN') and Get_Lang_Selection('i.ChineseName', 'i.EnglishName')
		$TableName = ($RecordType == "SubjectGroup")? "SUBJECT_TERM_CLASS_USER" : "YEAR_CLASS_USER";
		
		$sql = "SELECT 
							i.UserID AS StudentID,
							i.EnglishName, 
							i.ChineseName,
							i.ClassNumber,
							yc.ClassTitleEN,
							yc.ClassTitleB5,
							CONCAT('".($sys_custom['LessonAttendance_LaSalleCollege']?"-1":"0")."') as AttendStatus, 
							CONCAT('') as Remarks, 
							i.UserLogin, 
							CONCAT('0') as LateFor
					 FROM 
					 		".$TableName." a 
					 		INNER JOIN 
					 		INTRANET_USER i 
					 		ON 
					 			a.UserID=i.UserID 
					 			AND 
								i.RecordType = 2 
								AND 
								i.RecordStatus IN (0,1,2) 
					 			AND ";
		if ($RecordType == "SubjectGroup") {
			$sql .= " a.SubjectGroupID='$RecordID' ";
		}
		else {
			$sql .= " a.YearClassID='$RecordID' ";
		}
		$sql .= "	INNER JOIN 
							YEAR_CLASS yc
							on 
								yc.AcademicYearID = '".$this->CurAcademicYearID."' 
								AND 
								yc.ClassTitleEN = i.ClassName
					INNER JOIN YEAR as y ON y.YearID=yc.YearID 
								ORDER BY y.Sequence,yc.Sequence,i.ClassNumber";
		//echo $sql;
		$StudentList = $this->returnArray($sql);
		
		$Month = date('m',strtotime($LessonDate));
		$Year = date('Y',strtotime($LessonDate));
		$Day = date('d',strtotime($LessonDate));
		$this->createTable_Card_Student_Daily_Log($Year,$Month);
		
		
		$statusMap = $this->LessonAttendanceStatusMapping();
		$student_id_ary = Get_Array_By_Key($StudentList,'StudentID');
		$leave_records = $this->GetLessonAttendanceLeaveRecords(array('RecordDate'=>$LessonDate,'StudentID'=>$student_id_ary));
		$leave_records_size = count($leave_records);
		$studentIdToLeaveRecord = array();
		for($i=0;$i<$leave_records_size;$i++){
			$studentIdToLeaveRecord[$leave_records[$i]['StudentID']] = $leave_records[$i];
		}
		if($sys_custom['LessonAttendance_LaSalleCollege']){	
			$lesson_records = $this->GetLessonAttendanceRecords(array('LessonDate'=>$LessonDate,'StudentID'=>$student_id_ary),true);
			$studentIdToLessons = $this->Get_Class_Lesson_Attendance_Overview2($LessonDate, array(), -1, 1,$student_id_ary);
			//debug_pr($lessons);
		}
		//debug_pr($studentIdToLeaveRecord);
		for ($i=0; $i< sizeof($StudentList); $i++) {
			if ($RecordType == "SubjectGroup" && $LessonDate != "") {
				$StudentList[$i]['AttendStatus'] = $default_status;
				if($follow_status){
					$sql = 'Select 
										AMStatus, PMStatus, LeaveStatus  
									From 
										CARD_STUDENT_DAILY_LOG_'.$Year.'_'.$Month .'
									where 
										UserID = \''.$StudentList[$i]['StudentID'].'\'
										and 
										DayNumber = \''.$Day.'\'
								 ';
					$DailyAttendStatus = $this->returnResultSet($sql);
					if(count($DailyAttendStatus)>0){
						$status_value = $DailyAttendStatus[0]['PMStatus']!=''? $DailyAttendStatus[0]['PMStatus']: $DailyAttendStatus[0]['AMStatus'];
						switch ($status_value) {
							case '1': // absent
								$StudentList[$i]['AttendStatus'] = '3';
								break;
							case '2': // late
								$StudentList[$i]['AttendStatus'] = '2';
								break;
							case '0': // normal
								$StudentList[$i]['AttendStatus'] = '0';
								break;
							case '3': // outing
								$StudentList[$i]['AttendStatus'] = '1';
								break;
							default:
								$StudentList[$i]['AttendStatus'] = $default_status;
								break;
						}
					}
				}
			}
			
			if(isset($studentIdToLeaveRecord[$StudentList[$i]['StudentID']])){
				// if has preset leave(absent/outing) on that day, auto set to absent/outing
				if($sys_custom['LessonAttendance_LaSalleCollege']){
					// LaSalle need to check which lesson session applied leave
					$leave_session_ary = explode(',', trim($studentIdToLeaveRecord[$StudentList[$i]['StudentID']]['LeaveSessions']));
					$is_lesson_leave = false;
					if(count($leave_session_ary)>0 && isset($studentIdToLessons[$StudentList[$i]['StudentID']]))
					{
						$lessons_ary = $studentIdToLessons[$StudentList[$i]['StudentID']];
						$lessons_size = count($lessons_ary);
						for($j=0;$j<$lessons_size;$j++){
							if($lessons_ary[$j]['StartTime'] == $_REQUEST['StartTime'] && in_array($j+1,$leave_session_ary)){
								$StudentList[$i]['AttendStatus'] = $studentIdToLeaveRecord[$StudentList[$i]['StudentID']]['LeaveType'];
								$StudentList[$i]['Reason'] = $studentIdToLeaveRecord[$StudentList[$i]['StudentID']]['Reason'];
								$is_lesson_leave = true;
							}
						}
					}
					if(!$is_lesson_leave && isset($lesson_records[$LessonDate]) && isset($lesson_records[$LessonDate][$StudentList[$i]['StudentID']])
						 && count($lesson_records[$LessonDate][$StudentList[$i]['StudentID']])>0 
						  && $lesson_records[$LessonDate][$StudentList[$i]['StudentID']][0]['AttendStatus'] == $statusMap['Absent']['code'])
					{
						$StudentList[$i]['AttendStatus'] = $lesson_records[$LessonDate][$StudentList[$i]['StudentID']][0]['AttendStatus'];
						$StudentList[$i]['Reason'] = $lesson_records[$LessonDate][$StudentList[$i]['StudentID']][0]['Reason'];
					}
				}else{
					// generally apply leave is for whole day
					$StudentList[$i]['AttendStatus'] = $studentIdToLeaveRecord[$StudentList[$i]['StudentID']]['LeaveType'];
					$StudentList[$i]['Reason'] = $studentIdToLeaveRecord[$StudentList[$i]['StudentID']]['Reason'];
				}
			}else if($sys_custom['LessonAttendance_LaSalleCollege'] && isset($lesson_records[$LessonDate]) && isset($lesson_records[$LessonDate][$StudentList[$i]['StudentID']]) && count($lesson_records[$LessonDate][$StudentList[$i]['StudentID']])>0){
				// La Salle want to: if first lesson is absent, following lessons auto follow
				if($lesson_records[$LessonDate][$StudentList[$i]['StudentID']][0]['AttendStatus'] == $statusMap['Absent']['code']){
					$StudentList[$i]['AttendStatus'] = $lesson_records[$LessonDate][$StudentList[$i]['StudentID']][0]['AttendStatus'];
					$StudentList[$i]['Reason'] = $lesson_records[$LessonDate][$StudentList[$i]['StudentID']][0]['Reason'];
				}
			}
			
			$PhotoLink = (is_file($intranet_root."/file/user_photo/".$StudentList[$i]['UserLogin'].".jpg"))? "/file/user_photo/".$StudentList[$i]['UserLogin'].".jpg":"";
			$Return[] = array($StudentList[$i]['StudentID'],
												$StudentList[$i]['EnglishName'],
												$StudentList[$i]['ChineseName'],
												$StudentList[$i]['ClassNumber'],
												$StudentList[$i]['ClassTitleEN'],
												$StudentList[$i]['AttendStatus'],
												$StudentList[$i]['Remarks'],
												$PhotoLink,
												$StudentList[$i]['LateFor'],
												$StudentList[$i]['ClassTitleB5'],
												'',
												'',
												$StudentList[$i]['Reason'],
												'');

		}
		return (sizeof($Return)>0)? $Return:-1;
	}
	
	// Get Sitting Plan from SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}
	function Get_Lesson_Saved_Plan($SubjectGroupID, $LessonDate, $RoomAllocationID)
	{
		$this->Build_Subject_Group_Attend_Overview();
		$NameField = getNameFieldByLang2("u.");
		
		$sql = "SELECT 
							sgao.PlanID, 
							sgao.PlanName, 
							sgao.StudentPositionAMF, 
							sgao.PlanLayoutAMF, 
							".$NameField." as LastModifiedBy,
							sgao.LastModifiedDate
						FROM 
							SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." as sgao 
							LEFT JOIN 
							INTRANET_USER as u 
							on sgao.LastModifiedBy = u.UserID 
						WHERE 
							sgao.SubjectGroupID = '$SubjectGroupID' 
							AND 
							sgao.LessonDate = '$LessonDate' 
							AND 
							sgao.RoomAllocationID = '$RoomAllocationID' ";
		$Result = $this->returnArray($sql,6);
		
		return array($Result[0]['PlanID'],$Result[0]['PlanName'],$Result[0]['StudentPositionAMF'],$Result[0]['PlanLayoutAMF'],$Result[0]['LastModifiedDate'],$Result[0]['LastModifiedBy']);
	}
	
	// Get Attendance data from SUBJECT_GROUP_STUDENT_ATTENDANCE_{Current Academic Year ID}
	function Get_Lesson_Attendance_Record($AttendOverviewID) 
	{
		global $intranet_root;
		//include("settings.php");
		$this->Build_Subject_Group_Student_Attendance();
		
		$sql = "SELECT 
							s.StudentID, 
							u.EnglishName, 
							u.ChineseName, 
							u.ClassNumber, 
							yc.ClassTitleEN, 
							yc.ClassTitleB5, 
							s.AttendStatus,
							s.Reason, 
							s.Remarks, 
							u.UserLogin, 
							s.LateFor,
							s.LastModifiedBy,
							s.LastModifiedDate,
							s.LeaveTypeID 				
						FROM 
							SUBJECT_GROUP_STUDENT_ATTENDANCE_".$this->CurAcademicYearID." s 
							INNER JOIN 
						 	INTRANET_USER u 
						 	ON 
						 		AttendOverviewID = '$AttendOverviewID' 
						 		and 
						 		u.UserID = s.StudentID 
						 	INNER JOIN 
						 	YEAR_CLASS yc 
						 	ON 
						 		yc.AcademicYearID = '".$this->CurAcademicYearID."' 
						 		and 
						 		yc.ClassTitleEN = u.ClassName 
							INNER JOIN YEAR as y ON y.YearID=yc.YearID 
						 		ORDER BY y.Sequence,yc.Sequence,u.ClassNumber";
		/*echo '<pre>';
		var_dump($sql);
		echo '</pre>';*/
		$AttendRecord = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($AttendRecord); $i++) {
			$PhotoLink = (is_file($intranet_root."/file/user_photo/".$AttendRecord[$i]['UserLogin'].".jpg"))? "/file/user_photo/".$AttendRecord[$i]['UserLogin'].".jpg":"";
			$Return[] = array($AttendRecord[$i]['StudentID'],
								$AttendRecord[$i]['EnglishName'],
								$AttendRecord[$i]['ChineseName'],
								$AttendRecord[$i]['ClassNumber'],
								$AttendRecord[$i]['ClassTitleEN'],
								$AttendRecord[$i]['AttendStatus'],
								$AttendRecord[$i]['Remarks'],
								$PhotoLink,
								$AttendRecord[$i]['LateFor'],
								$AttendRecord[$i]['ClassTitleB5'],
								$AttendRecord[$i]['LastModifiedBy'],
								$AttendRecord[$i]['LastModifiedDate'],
								$AttendRecord[$i]['Reason'],
								$AttendRecord[$i]['LeaveTypeID']);
		}
		return (sizeof($Return)>0)? $Return:-1;
	}
	
	// Save Sitting Plans to ATTEND_DEFAULT_PLAN
	function Save_Default_Plan($RecordID,$PlanDetailArray,$RecordType="SubjectGroup")
	{
		$this->Start_Trans();
		$result = array();
		
		$FieldName = ($RecordType=="SubjectGroup")? "SubjectGroupID":"YearClassID";
		$sql = 'delete from ATTEND_DEFAULT_PLAN where '.$FieldName.' = \''.$RecordID.'\'';
		$result['delete'] = $this->db_db_query($sql);
		if(is_array($PlanDetailArray))
		{
			for($i=0;$i<count($PlanDetailArray);$i++)
			{
				//$TestReturn .= $PlanDetailArray[$i][0].'='.$PlanDetailArray[$i][1].'='.$PlanDetailArray[$i][2].'='.$PlanDetailArray[$i][3].'||||||||';
				$PlanDetailPlanID = $PlanDetailArray[$i][0];
				if ($PlanDetailPlanID[0] != "y" || $RecordType == "YearClass") {
					$PlanDetailPlanName = $PlanDetailArray[$i][1];
					$PlanDetailStudentPosition = $PlanDetailArray[$i][2];
					$PlanDetailLayoutPlan = $PlanDetailArray[$i][3];
					
					$sql = "INSERT INTO ATTEND_DEFAULT_PLAN 
										(".$FieldName.", 
										FlashPlanID, 
										PlanName, 
										StudentPositionAMF, 
										PlanLayoutAMF, 
										LocationID, 
										CreateBy, 
										CreateDate, 
										LastModifiedBy, 
										LastModifiedDate
										)
									VALUES 
										('$RecordID',
										'$PlanDetailPlanID',
										'".$this->Get_Safe_Sql_Query($PlanDetailPlanName)."',
										'".$this->Get_Safe_Sql_Query($PlanDetailStudentPosition)."',
										'".$this->Get_Safe_Sql_Query($PlanDetailLayoutPlan)."',
										NULL,
										'".$_SESSION['UserID']."',
										NOW(),
										'".$_SESSION['UserID']."',
										NOW()
										) 
								";
					$result[$i]=$this->db_db_query($sql);
				}
			}
		}
		//return "11111";
		/*echo '<pre>';
		var_dump($result);
		echo '</pre>';*/
		if(in_array(false,$result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else {
			$this->Commit_Trans();
			return true;
		}
	}
	
	// Save Sitting Plan to SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}
	function Save_Lesson_Plan($SubjectGroupID, $LessonDate, $RoomAllocationID, $StartTime, $EndTime, $PlanDetailPlanID, $PlanDetailPlanName, $StudentPositionAMF, $PlanDetailAMF)
	{	
		$Result['1'] = $this->Build_Subject_Group_Attend_Overview();
			
		$AttendOverviewID = -1;
		
		$sqlCheckExist = "SELECT 
												AttendOverviewID 
											FROM 
												SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." 
											WHERE 
												SubjectGroupID='$SubjectGroupID' 
												AND 
												LessonDate='$LessonDate' 
												AND 
												RoomAllocationID='$RoomAllocationID' ";
		$resultCheckExist = $this->returnVector($sqlCheckExist);
		if(count($resultCheckExist)>0)
		{
			$AttendOverviewID = $resultCheckExist[0];
			$sql = "UPDATE SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID;
			$sql.= " SET 
								PlanID='$PlanDetailPlanID', 
								PlanName='".$this->Get_Safe_Sql_Query($PlanDetailPlanName)."', 
								StudentPositionAMF='".$this->Get_Safe_Sql_Query($StudentPositionAMF)."', 
								PlanLayoutAMF='".$this->Get_Safe_Sql_Query($PlanDetailAMF)."', ";
			$sql.= " LastModifiedBy='".$_SESSION['UserID']."', LastModifiedDate=NOW() ";
			$sql.= " WHERE AttendOverviewID='$AttendOverviewID' ";
			$Result['2'] = $this->db_db_query($sql);
		}else
		{
			$sql = "INSERT INTO SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID;
			$sql.= " (SubjectGroupID, 
								LessonDate, 
								RoomAllocationID, 
								StartTime, 
								EndTime, 
								PlanID, 
								PlanName, 
								StudentPositionAMF, 
								PlanLayoutAMF, 
								CreateBy, 
								CreateDate, 
								LastModifiedBy, 
								LastModifiedDate) ";
			$sql.= " VALUES 
							 ('$SubjectGroupID',
							 	'$LessonDate',
							 	'$RoomAllocationID',
							 	'$StartTime',
							 	'$EndTime',
							 	'$PlanDetailPlanID',
							 	'".$this->Get_Safe_Sql_Query($PlanDetailPlanName)."',
							 	'".$this->Get_Safe_Sql_Query($StudentPositionAMF)."',
							 	'".$this->Get_Safe_Sql_Query($PlanDetailAMF)."', 
							 	'".$_SESSION['UserID']."', 
							 	NOW(), 
							 	'".$_SESSION['UserID']."', 
							 	NOW()) ";
			$Result['3'] = $this->db_db_query($sql);
			$AttendOverviewID = $this->db_insert_id();
		}
		
		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>';*/
		
		return $AttendOverviewID;
	}
	
	// Save Attendance data to SUBJECT_GROUP_STUDENT_ATTENDANCE_{Current Academic Year ID}
	function Save_Lesson_Attendance_Data($AttendOverviewID, $StudentAttendArray)
	{
		$this->Build_Subject_Group_Student_Attendance();
		
		$result = false;
		if(is_array($StudentAttendArray))
		{
			$this->Start_Trans();
			for($i=0;$i<count($StudentAttendArray);$i++)
			{
				$extra_fiels = '';
				$extra_value = '';
				$extra_update = '';
				$StudentID = $StudentAttendArray[$i][0];
				$AttendStatus = $StudentAttendArray[$i][1];
				$Remarks = $StudentAttendArray[$i][2];
				$LateFor = $StudentAttendArray[$i][3];
				$Reason = $StudentAttendArray[$i][4];
				if(get_client_region() == 'zh_TW') {
					$leaveTypeId = $StudentAttendArray[$i][5];
					$extra_fiels .= ',LeaveTypeID';
					$extra_value .= ",'".$this->Get_Safe_Sql_Query($leaveTypeId)."'";
					$extra_update .= ",LeaveTypeID='".$this->Get_Safe_Sql_Query($leaveTypeId)."'";
				}
				//$Remarks = HTMLtoDB($Remarks);
				
				$sql = "INSERT INTO SUBJECT_GROUP_STUDENT_ATTENDANCE_".$this->CurAcademicYearID;
				$sql.= " (AttendOverviewID, 
									StudentID, 
									AttendStatus, 
									InTime, 
									OutTime, 
									LateFor,
									Reason, 
									Remarks, 
									CreateBy, 
									CreateDate, 
									LastModifiedBy, 
									LastModifiedDate
									$extra_fiels) ";
				$sql.= " VALUES
									('$AttendOverviewID',
									'$StudentID',
									'$AttendStatus',
									NULL,
									NULL,
									'".$this->Get_Safe_Sql_Query($LateFor)."',
									'".$this->Get_Safe_Sql_Query($Reason)."',
									'".$this->Get_Safe_Sql_Query($Remarks)."',
									'".$_SESSION['UserID']."',
									NOW(),
									'".$_SESSION['UserID']."',
									NOW()
									$extra_value) 
								ON DUPLICATE KEY UPDATE 
									AttendStatus='$AttendStatus', 
									LateFor = '".$this->Get_Safe_Sql_Query($LateFor)."',
									Reason='".$this->Get_Safe_Sql_Query($Reason)."', 
									Remarks='".$this->Get_Safe_Sql_Query($Remarks)."', 
									LastModifiedBy='".$_SESSION['UserID']."', 
									LastModifiedDate=NOW()
									$extra_update";
				
				$result[$i] = $this->db_db_query($sql);
			}
			if(in_array(false,$result))
			{
				$this->RollBack_Trans();
				return false;
			}
			else {
				$this->Commit_Trans();
				return true;
			}
		}
		
		return false;
	}
	
	//Check Existence of Record SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}
	function Check_Record_Exist_Subject_Group_Attend_Overview($SubjectGroupID,$LessonDate,$RoomAllocationID)
	{
		$this->Build_Subject_Group_Attend_Overview();
		
		$sql = "SELECT 
							AttendOverviewID 
						FROM 
							SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." 
						WHERE 
							SubjectGroupID = '$SubjectGroupID'
							and 
							LessonDate = '$LessonDate' 
							and 
							RoomAllocationID = '$RoomAllocationID'";
		
		$result = $this->returnVector($sql);
		if(count($result)>0)
		{
			return $result[0];
		}else
		{
			return -1;
		}
	}
	
	//Check Existence of Record ATTEND_DEFAULT_PLAN
	function Check_Record_Exist_Subject_Group_Attend_Default_Plan($SubjectGroupID)
	{
		$sql = "SELECT COUNT(*) FROM ATTEND_DEFAULT_PLAN ";
		$sql.= " WHERE SubjectGroupID = '$SubjectGroupID'";
		
		$result = $this->returnVector($sql);
		if(count($result)>0)
		{
			if($result[0]>0)
			{
				return true;
			}else
			{
				return false;
			}
		}else
		{
			return false;
		}
	}
	
	// Check if current submittd lesson attendance records were updated by others after loaded the page 
	function Is_Lesson_Plan_Outdated($SubjectGroupID, $LessonDate, $RoomAllocationID, $LastLoadedTimestamp)
	{
		$this->Build_Subject_Group_Attend_Overview();
		
		$sql = "SELECT 
					MAX(b.LastModifiedDate) as LastModifiedDate 
				FROM SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." as a 
				INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$this->CurAcademicYearID." as b ON b.AttendOverviewID=a.AttendOverviewID 
				WHERE a.SubjectGroupID='$SubjectGroupID' 
				AND a.LessonDate='$LessonDate' 
				AND a.RoomAllocationID='$RoomAllocationID'";
		$result = $this->returnVector($sql);
		if(sizeof($result) > 0 && $result[0] != '')
		{
			return strtotime($result[0]) >= $LastLoadedTimestamp;
		}
		return false;
	}
	
	
		##########################* Function from liblessonattendance [end] *#########################
		
		function getDefaultNumOfSessionSettings()
		{
			//$GeneralSetting = new libgeneralsettings();
			$GeneralSetting = $this->GetLibGeneralSettings();
			
			$SettingList[] = "'DefaultNumOfSessionForAbsent'";
			$SettingList[] = "'DefaultNumOfSessionForLate'";
			$SettingList[] = "'DefaultNumOfSessionForLeave'";
			$SettingList[] = "'DefaultNumOfSessionForAbsenteesism'";
			$SettingList[] = "'DefaultNumOfSessionForHoliday'";
			$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
			
			return $Settings;
		}
		
		/*
		 * @param $DayType : '' - All, 2 - AM, 3 - PM
		 * @param $RecordType : '' - All, 1 - Waived, 2 - Not waived
		 */
		function getWaiveAbsenceRecords($options)
		{
			global $sys_custom;
			
			$rankTarget = $options['rankTarget'];
			$selectYear = Get_Current_Academic_Year_ID();
			
			if($rankTarget == 'form'){
				$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'class'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'student'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
			}
			
			$StartDate = $options['StartDate'];
			$EndDate = $options['EndDate'];
			$DayType = $options['DayType'];
			$RecordType = $options['RecordType'];
			$GroupByDay = isset($options['GroupByDay'])? $options['GroupByDay'] : false; // default false
			
			$name_field = getNameFieldByLang("u.");
			$start_date = date("Y-m-d",strtotime($StartDate));
			$end_date = date("Y-m-d",strtotime($EndDate));
			
			if($GroupByDay){
				$teacher_name_field = getNameFieldByLang("u2.");
			}
			
			$sql = "SELECT 
						r.RecordDate,
						u.ClassName,
						u.ClassNumber,
						$name_field as StudentName,
						r.DayType,";
				if($GroupByDay){
					$sql .= "SUM(IF(s.AbsentSession IS NULL OR s.AbsentSession='',0,s.AbsentSession)) as AbsentSession,";
				}else{
					$sql .= "s.AbsentSession,";
				}
				$sql .=	"r.WaiveAbsent,
						r.Reason,
						m.Remark,
						r.Remark as AbsenceRemark,
						r.DateModified,
						u.UserID,
						r.RecordID,
						s.RecordID as SessionRecordID ";
			if($GroupByDay){
				$sql .= ",CONCAT(DATE_FORMAT(r.RecordDate,'%d/%m/%Y'),' (',SUM(IF(s.AbsentSession IS NULL OR s.AbsentSession='',0,s.AbsentSession)),')') as RecordDetail,DATE_FORMAT(NOW(),'%d/%m/%Y') as GeneratedDate ";
				//$sql .= ",ycu.YearClassID";
				//$sql .= ",u2.UserID,$teacher_name_field as ClassTeacherName ";
				//$sql .= ",GROUP_CONCAT(s.RecordID) as RecordCount ";
			}
			$sql.=" FROM INTRANET_USER as u 
					INNER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r ON r.StudentID=u.UserID AND r.RecordType='".PROFILE_TYPE_ABSENT."' 
					LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as s ON s.StudentID=r.StudentID AND s.RecordDate=r.RecordDate AND s.DayType=r.DayType 
					LEFT JOIN CARD_STUDENT_DAILY_REMARK as m ON m.StudentID=r.StudentID AND m.RecordDate=r.RecordDate AND m.DayType=r.DayType 
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=u.UserID 
					LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
					LEFT JOIN YEAR as y ON y.YearID=yc.YearID ";
			//if($GroupByDay){
				
				//$current_acdemic_year_id = Get_Current_Academic_Year_ID();
				/*
				$sql .= "LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=r.StudentID 
						LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID='$current_acdemic_year_id' 
						LEFT JOIN YEAR_CLASS_TEACHER as yct ON yct.YearClassID=ycu.YearClassID 
						LEFT JOIN INTRANET_USER as u2 ON u2.UserID=yct.UserID ";
				*/
				//$sql .= "LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=r.StudentID ";
				
			//}
				$sql.=" WHERE u.RecordStatus='1' AND u.RecordType='".USERTYPE_STUDENT."' AND yc.AcademicYearID = '".$selectYear."' 
						AND r.RecordDate >= '$start_date' AND r.RecordDate <= '$end_date' ";
			if($DayType != '' && in_array($DayType,array(PROFILE_DAY_TYPE_AM,PROFILE_DAY_TYPE_PM))){
				$sql .= " AND r.DayType='$DayType' ";
			}
			if($RecordType != '' && in_array($RecordType,array('1','2'))){
				if($RecordType == '1'){
					$sql .= " AND r.WaiveAbsent='1' ";
				}else{
					$sql .= " AND (r.WaiveAbsent='0' OR r.WaiveAbsent IS NULL) ";
				}
			}
			$sql .= $yearIdCond.$classIdCond.$studentIdCond ;
			if($GroupByDay){
				$sql .= " GROUP BY r.StudentID,r.RecordDate ";
			}
			$sql .= " ORDER BY u.ClassName,u.ClassNumber+0,r.RecordDate,r.DayType";
			//debug_r($sql);
			$records = $this->returnArray($sql);
			return $records;
		}
		
		function updateWaiveAbsenceRecords($recordIdAry, $waiveAbsentAry, $absentSessionAry)
		{
			$record_count = count($recordIdAry);
			
			$sql = "SELECT r.RecordID,c.RecordID as SessionRecordID 
					FROM CARD_STUDENT_PROFILE_RECORD_REASON as r 
					LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as c ON c.StudentID=r.StudentID AND c.RecordDate=r.RecordDate AND c.DayType=r.DayType 
					WHERE r.RecordID IN ('".implode("','",$recordIdAry)."')";
			$records = $this->returnResultSet($sql);
			$assocRecords = array();
			for($i=0;$i<count($records);$i++){
				$assocRecords[$records[$i]['RecordID']] = $records[$i]['SessionRecordID'];
			}
			
			$result = array();
			for($i=0;$i<$record_count;$i++){
				$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET WaiveAbsent='".($waiveAbsentAry[$i]==1?"1":"0")."', DateModified=NOW() WHERE RecordID='".$recordIdAry[$i]."' ";
				$result['UpdateProfileReason_'.$recordIdAry[$i]] = $this->db_db_query($sql);
				
				if($assocRecords[$recordIdAry[$i]]!='' && $assocRecords[$recordIdAry[$i]] > 0){
					$sql = "UPDATE CARD_STUDENT_STATUS_SESSION_COUNT SET AbsentSession='".$absentSessionAry[$i]."',DateModify=NOW(),ModifyBy='".$_SESSION['UserID']."' WHERE RecordID='".$assocRecords[$recordIdAry[$i]]."'";
					$result['UpdateSession_'.$recordIdAry[$i]] = $this->db_db_query($sql);
				}
			}
			
			return !in_array(false, $result);
		}
		
		function getRawLogRecords($StartDate,$EndDate,$UserIdAry,$GetHaveDataOnly=1)
		{
			$start_ts = strtotime($StartDate);
			$end_ts = strtotime($EndDate);
			$join = $GetHaveDataOnly ? "INNER":"LEFT";
			
			$name_field = getNameFieldWithClassNumberByLang("u.");
			$records = array();
			for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,intval(date("m",$cur_ts)+1),1,intval(date("Y",$cur_ts))))
			{
				$cur_year = date("Y",$cur_ts);
				$cur_month = date("m",$cur_ts);
				$cur_day = date("d",$cur_ts);
				
				$raw_log_table = "CARD_STUDENT_RAW_LOG_".$cur_year."_".$cur_month;
				$join_cond = " AND (DATE_FORMAT(CONCAT('$cur_year-$cur_month-',r.DayNumber),'%Y-%m-%d') BETWEEN '".$this->Get_Safe_Sql_Query($StartDate)."' AND '".$this->Get_Safe_Sql_Query($EndDate)."') ";
				
				$sql = "SELECT 
							u.UserID,
							$name_field as StudentName,
							u.ClassName,
							u.ClassNumber,
							DATE_FORMAT(CONCAT('$cur_year-$cur_month-',r.DayNumber),'%Y-%m-%d') as RecordDate,
							r.RecordTime,
							r.RecordStation 
						FROM INTRANET_USER as u 
						$join JOIN $raw_log_table as r ON r.UserID=u.UserID $join_cond
						WHERE u.UserID IN ('".implode("','",IntegerSafe($UserIdAry))."') 
						ORDER BY r.DayNumber,u.ClassName,u.Classnumber+0,r.RecordTime";
				$ary = $this->returnResultSet($sql);
				$records = array_merge($records,$ary);
			}
			return $records;
		}
		
		###############################
		function getNotSumttedDocumentStudentDataSQL($UserIdAry,$DateStart,$DateEnd,$Get_document_status='', $record_type=array(1), $processedDisplay=false, $dateDelimeter=',<br>'){
			global $Lang;
			if($Get_document_status == 0){
				$DocumentStatusSelect = " HAVING countnot > 0 ";
			}
			else{
				$DocumentStatusSelect = "";
			}
			
			$record_type = array_values(array_intersect(IntegerSafe($record_type),array(1,3)));
			
			// AM & PM document status not the same
			$diffDocStatus = "<span class=\'tabletextrequire\'>*</span>";
			
			// Record processed
			if($processedDisplay)
			{
				$csprr_process = " IF(csprr.Processed = 1, ' (".$Lang['StudentAttendance']['Processed'].")', '')";
				$csprr2_process = " IF(csprr.Processed = 1, IF(csprr2.Processed = 1, ' (".$Lang['StudentAttendance']['Processed'].")', CONCAT(' (', IF(csprr.DayType = 2, 'AM', 'PM'), ' - ', '".$Lang['StudentAttendance']['Processed']."', ')')), IF(csprr2.Processed = 1, CONCAT(' (', IF(csprr2.DayType = 2, 'AM', 'PM'), ' - ".$Lang['StudentAttendance']['Processed']."', ')'), ''))";
				$recordDateField = " GROUP_CONCAT( 
							DISTINCT(IF(csprr2.RecordID is null,
								IF(csprr.DocumentStatus = 0, CONCAT(CONVERT(csprr.RecordDate, char), ' (', IF(csprr.DayType=2, 'AM' , 'PM'), ')', ".$csprr_process."), NULL), 	/* AM/PM absence*/								
								IF(csprr.DocumentStatus = 0, IF(csprr2.DocumentStatus = 0, CONCAT(CONVERT(csprr.RecordDate,char), ".$csprr2_process."), CONCAT(CONVERT(csprr.RecordDate,char), ".$csprr2_process.", '".$diffDocStatus."')), IF(csprr2.DocumentStatus = 0, CONCAT(CONVERT(csprr.RecordDate,char), ".$csprr2_process.", '".$diffDocStatus."'), NULL)) 	/* whole day absence*/	
							 ) )
						ORDER BY csprr.RecordDate ASC
						SEPARATOR '$dateDelimeter') as Dates,";
			}
			else
			{
				$recordDateField = "GROUP_CONCAT( 
							DISTINCT (IF (csprr2.RecordID is null,
								IF(csprr.DocumentStatus = 0,CONCAT( csprr.RecordDate, '(', IF(csprr.DayType=2,'AM','PM'), ')' ),NULL), 	/* AM/PM absence*/								
								IF(csprr.DocumentStatus = 0, IF(csprr2.DocumentStatus = 0, csprr.RecordDate, CONCAT(csprr.RecordDate,'$diffDocStatus') ), IF(csprr2.DocumentStatus = 0, CONCAT(csprr.RecordDate,'$diffDocStatus'),NULL) ) 	/* whole day absence*/	
							) )
						ORDER BY csprr.RecordDate ASC
						SEPARATOR '$dateDelimeter') as Dates,";
			}
			
			$sql = "SELECT 
						CASE iu.WebSAMSRegNo
							WHEN '' THEN '---'
							WHEN NULL THEN '---'
							ELSE iu.WebSAMSRegNo
						END	as WebSAMSRegNo,				
						iu.ClassName as ClassName,
						iu.ClassNumber as ClassNumber,
						".Get_Lang_Selection('iu.ChineseName','iu.EnglishName')." as Name,
						IF(csprr.RecordType=1,'".$Lang['StudentAttendance']['Absent']."','".$Lang['StudentAttendance']['EarlyLeave']."') as RecordType,
						count(DISTINCT(csprr.RecordDate)) as countall, 	/*num of days of abesent*/
						count(DISTINCT(IF(csprr.DocumentStatus = 0,csprr.RecordDate,NULL))) as countnot, 	/*num of absent day not subitted document*/
						$recordDateField
						csprr.StudentID as StudentID
					FROM 
						CARD_STUDENT_PROFILE_RECORD_REASON csprr
						INNER JOIN INTRANET_USER iu ON (csprr.StudentID = iu.UserID)
						LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON csprr2 ON (csprr.RecordDate = csprr2.RecordDate AND csprr.StudentID = csprr2.StudentID AND csprr.RecordID != csprr2.RecordID And csprr2.RecordType = csprr.RecordType AND csprr2.ProfileRecordID > 0)
					WHERE 
						csprr.ProfileRecordID > 0 
						AND csprr.StudentID IN ( ". implode(',',IntegerSafe($UserIdAry)) ." )
						AND csprr.RecordType IN (".implode(",",$record_type).")
						AND csprr.RecordDate BETWEEN '".$this->Get_Safe_Sql_Query($DateStart)."' AND '".$this->Get_Safe_Sql_Query($DateEnd)."'
					GROUP BY
						csprr.StudentID,csprr.RecordType 
					$DocumentStatusSelect
					";
			return $sql;
		}
		
		function updateTeacherRemark($StudentID,$TargetDate,$DayType,$Remark)
		{
			//if($Remark == '')
			//{
			//	$sql = "DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE StudentID='$StudentID' AND RecordDate='$TargetDate' AND DayType='$DayType'";
			//	$success = $this->db_db_query($sql);
			//}else{
				$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$Remark',DateModified=NOW(), ModifyBy='".$_SESSION['UserID']."' WHERE StudentID='$StudentID' AND RecordDate='$TargetDate' AND DayType='$DayType'";
				$success = $this->db_db_query($sql);
				if($this->db_affected_rows()==0){
					$sql = "INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark,DateModified,ModifyBy) VALUES ('$StudentID','$TargetDate','$DayType','$Remark',NOW(),'".$_SESSION['UserID']."')";
					$success = $this->db_db_query($sql);
				}
			//}
			return $success;
		}
		
		function insertOutingReasonImportTempData($ValueArr){
			### clear old data
			$this->deleteOutingReasonImportTempData();
			
			### insert new temp data
			$_value = implode(',',$ValueArr);
			$sql = "INSERT INTO 
					TEMP_ATTENDANCE_REASON_IMPORT
					(UserID,RowNumber,ReasonText,DateInput) 
					Values 
					$_value";
			$successAry[] = $this->db_db_query($sql);
			return !in_array(false, $successAry);
		}
		
		function getOutingReasonImportTempData() {
			$sql = "Select * from TEMP_ATTENDANCE_REASON_IMPORT";
			return $this->returnResultSet($sql);
		}
		
		function deleteOutingReasonImportTempData() {
				
			$successAry = array();
			
			$sql = "Delete from TEMP_ATTENDANCE_REASON_IMPORT";
			$successAry['DeleteTempImportCsvData'] = $this->db_db_query($sql);
			
			return !in_array(false, $successAry);
		}
		
		function getImportOutingReasonHeader(){
			$exportHeaderAry['en'] = array("Reason Text");
			$exportHeaderAry['Ch'] = array("原因名稱");
			return $exportHeaderAry;
		}
		
		function getImportOutingReasonColumnProperty(){
			return array(1);
		}
		
		function Check_OutingReason_Redundancy($value){
			$sql="SELECT
					ReasonText
				  FROM
				  	CARD_STUDENT_OUTING_REASON
				  WHERE
				  	ReasonText = \"$value\"
				  	";
			return $this->returnResultSet($sql);
		}
		
		function updateOfficeRemark($StudentID,$TargetDate,$DayType,$RecordType,$Remark)
		{
			$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET OfficeRemark='$Remark',DateModified=NOW() WHERE RecordDate='$TargetDate' AND StudentID='$StudentID' AND DayType='$DayType' AND RecordType='$RecordType'";
			$success = $this->db_db_query($sql);
			return $success;
		}
		
		
		function Clear_Websams_Reason_Temp(){
			
			$successAry = array();
			$table = "TEMP_ATTENDANCE_REASON_IMPORT";
			
			$sql = "Delete from $table Where UserID = '".$_SESSION["UserID"]."' ";
			$successAry['DeleteTempImportCsvData'] = $this->db_db_query($sql);
			return !in_array(false, $successAry);
		
		}

		function Get_Websams_Reason_Temp(){
			$table = "TEMP_ATTENDANCE_REASON_IMPORT";
		 	$sql = "Select * from $table Where UserID = '".$_SESSION["UserID"]."' and ReasonType is not null and CodeID is not null ";
			return $this->returnResultSet($sql);//
		}
		
		function  Insert_Websams_Reason_Temp($ValueArr){
		
				### clear old data 
	     	 $this->Clear_Websams_Reason_Temp();
			
			### insert new temp data
			$table = "TEMP_ATTENDANCE_REASON_IMPORT";
			$sql = "INSERT INTO 
						$table
					(UserID,RowNumber,CodeID,ReasonType,ReasonText,DateInput) 
					Values 		
					".implode(',',(array)$ValueArr)."";
					
			$successAry[] = $this->db_db_query($sql);
			
			
			return !in_array(false, $successAry);
			
		}
	
		function Add_Websams_Reason($code, $text,$type){
				
			$Sql = "
						INSERT INTO
									 INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE
									(CodeID, ReasonType,ReasonText, RecordType, RecordStatus, DateInput,DateModified)
						VALUES
								('".$this->Get_Safe_Sql_Query($code)."',
								 '".$this->Get_Safe_Sql_Query($type)."',
								'".$this->Get_Safe_Sql_Query($text)."',
								 '0',
								 null,
								 NOW(), 
								 NOW()
								)
					"; 
	
			$result = $this->db_db_query($Sql);
			return $result;
		}
		
		// Make libgeneralsettings instance singleton
		function GetLibGeneralSettings()
		{
			if(!$this->LibGeneralSettings){
				$this->LibGeneralSettings = new libgeneralsettings();
			}
			
			return $this->LibGeneralSettings;
		}
		
		function Get_General_Settings($module, $keyAry)
		{
			$GeneralSetting  = $this->GetLibGeneralSettings();
			$key_count = count($keyAry);
			if($key_count == 0){
				return array();
			}
			for($i=0;$i<$key_count;$i++){
				if($keyAry[$i][0] != "'"){
					$keyAry[$i] = "'".$keyAry[$i];
				}
				if($keyAry[$i][strlen($keyAry[$i])-1] != "'"){
					$keyAry[$i] .= "'";
				}
			}
			
			$settingsAry = $GeneralSetting->Get_General_Setting($module,$keyAry);
			return $settingsAry;
		}
		
		function updateAttendanceInTime($StudentID,$TargetDate,$DayType,$NewTime)
		{
			$date_parts = explode("-",$TargetDate);
			$year = $date_parts[0];
			$month = $date_parts[1];
			$day = intval($date_parts[2]);
			
			$daily_log_table = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
			
			$sql = "UPDATE $daily_log_table SET ".($DayType==2?"InSchoolTime='$NewTime'":"LunchBackTime='$NewTime'")." WHERE UserID='$StudentID' AND DayNumber='$day'";
			$success = $this->db_db_query($sql);
			return $success;
		}
		
		###############################
		
		function IsLessonAttendanceCustomizationForLaSalle()
		{
			global $sys_custom;			
			return $sys_custom['LessonAttendance_LaSalleCollege'];
		}
		
		function LessonAttendanceStatusMapping()
		{
			global $Lang, $sys_custom;
			
			$map = array();
			$map['Present'] = array('code'=>0, 'text'=> $Lang['StudentAttendance']['Present'], 'class'=>'LessonAttendance_Present', 'order'=>0);
			$map['Absent'] = array('code'=>3, 'text'=> $Lang['StudentAttendance']['Absent'], 'class'=>'LessonAttendance_Absent', 'order'=>2);
			$map['Late'] = array('code'=>2, 'text'=> $Lang['StudentAttendance']['Late'], 'class'=>'LessonAttendance_Late', 'order'=>1);
			$map['Outing'] = array('code'=>1, 'text'=> $Lang['StudentAttendance']['Outing'], 'class'=>'LessonAttendance_Outing', 'order'=>3);
			
			if($this->IsLessonAttendanceCustomizationForLaSalle())
			{
				$map['LateForSplitClass'] = array('code'=>4, 'text'=> $Lang['LessonAttendance']['LateForSplitClass'], 'class'=>'LessonAttendance_LateForSplitClass', 'order'=>4);
				$map['MedicalLeave'] = array('code'=>5, 'text'=> $Lang['LessonAttendance']['MedicalLeave'], 'class'=>'LessonAttendance_MedicalLeave', 'order'=>5);
				$map['SchoolActivities'] = array('code'=>6, 'text'=> $Lang['LessonAttendance']['SchoolActivities'], 'class'=>'LessonAttendance_SchoolActivities', 'order'=>6);
				$map['OtherApprovedEvents'] = array('code'=>7, 'text'=> $Lang['LessonAttendance']['OtherApprovedEvents'], 'class'=>'LessonAttendance_OtherApprovedEvents', 'order'=>7);
			}
			
			return $map;
		}
		
		### corresponding to /home/eAdmin/StudentMgmt/common/ajax_load_target_selection.php
		function GetTargetSelectionResult($TargetType, $TargetID)
		{
			global $intranet_root, $PATH_WRT_ROOT;
			
			include_once($intranet_root."/includes/libclass.php");
			
			$lclass = new libclass();
			
			$StudentIdAry = array();
			if($TargetType=='student'){
				$StudentIdAry = $TargetID;
				$ClassIdAry = array();
				$FormIdAry = array();
			}else if($TargetType == 'class'){
				$FormIdAry = array();
				$ClassIdAry = $TargetID;
				$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
				$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
			}else if($TargetType == 'form'){
				$FormIdAry = $TargetID;
				$ClassIdAry = array();
				
				for($i=0;$i<sizeof($FormIdAry);$i++)
				{
					$ClassList = $lclass->returnClassListByLevel($FormIdAry[$i]);
					for($j=0;$j<sizeof($ClassList);$j++) {
						$ClassIdAry[] = $ClassList[$j][0];
					}
				}
				$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
				$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
			}
			
			return array($StudentIdAry,$ClassIdAry,$FormIdAry);
		}
		
		function GetLessonAttendanceReasons($reasonId='', $reasonType='')
		{
			global $intranet_root, $PATH_WRT_ROOT;
			
			$sql = "SELECT * FROM SUBJECT_GROUP_ATTENDANCE_REASON WHERE 1 ";
			if($reasonId != '' || is_array($reasonId)){
				$sql .= " AND ReasonID IN (".implode(",",(array)$reasonId).") ";
			}
			if($reasonType != '' && $reasonType>=0){
				$reasonType = IntegerSafe($reasonType);
				$sql .= " AND ReasonType='$reasonType' ";
			}
			$sql .= " ORDER BY ReasonType,ReasonText";
			//debug_pr($sql);
			return $this->returnResultSet($sql);
		}
		
		function UpsertLessonAttendanceReason($map)
		{
			$fields = array('ReasonID','ReasonText','ReasonType','ReasonSymbol');
			$user_id = $_SESSION['UserID'];
			
			if(count($map) == 0) return false;
			
			if(isset($map['ReasonID']) && $map['ReasonID']!='' && $map['ReasonID'] > 0)
			{
				$sql = "UPDATE SUBJECT_GROUP_ATTENDANCE_REASON SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'ReasonID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($val)."'";
					$sep = ",";
				}
				$sql.= " ,ModifiedDate=NOW(),ModifiedBy='$user_id' WHERE ReasonID='".$map['ReasonID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query($val)."'";
					$sep = ",";
				}
				$keys .= ",InputDate,ModifiedDate,InputBy,ModifiedBy";
				$values .= ",NOW(),NOW(),'$user_id','$user_id'";
				
				$sql = "INSERT INTO SUBJECT_GROUP_ATTENDANCE_REASON ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function IsLessonAttendanceReasonExist($reasonType, $checkText, $excludeReasonId='', $checkField='ReasonText')
		{
			$sql = "SELECT COUNT(*) FROM SUBJECT_GROUP_ATTENDANCE_REASON WHERE ReasonType='$reasonType' ";
			if($excludeReasonId != '')
			{
				$sql .= " AND ReasonID!='$excludeReasonId' ";
			}
			$sql .= " AND $checkField='".$this->Get_Safe_Sql_Query(trim($checkText))."' ";
			
			$record = $this->returnVector($sql);
			
			return $record[0] > 0;
		}
		
		function DeleteLessonAttendanceReason($reasonId)
		{
			$reasonId = IntegerSafe($reasonId);
			$sql = "DELETE FROM SUBJECT_GROUP_ATTENDANCE_REASON WHERE ReasonID IN (".implode(",",(array)$reasonId).")";
			return $this->db_db_query($sql);
		}
		
		function GetLessonAttendanceSettingsList()
		{
			global $sys_custom;
			
			$SettingList = array();
			$SettingList[] = "LessonSessionCountAsDay"; // absent
			$SettingList[] = "LessonSessionLateCountAsDay"; // late
			$SettingList[] = "EnablePSLAD";
			$SettingList[] = "DaysBeforeToOpenData";
			$SettingList[] = "TeacherCanTakeAllLessons"; // iSmartCard - teacher can take all lessons
			$SettingList[] = "DefaultStatus";
			$SettingList[] = "FollowSchoolStatus";
			if($sys_custom['LessonAttendance_LaSalleCollege']){
				$SettingList[] = "StartSessionToCountAsAM";
				$SettingList[] = "EndSessionToCountAsAM";
				$SettingList[] = "StartSessionToCountAsPM";
				$SettingList[] = "EndSessionToCountAsPM";
			}
			
			return $SettingList;
		}
		
		function UpsertLessonAttendanceLeaveRecords($map)
		{
			$fields = array('RecordID','RecordDate','StudentID','LeaveType','LeaveSessions','Reason');
			$user_id = $_SESSION['UserID'];
			
			if(count($map) == 0) return false;
			
			if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
			{
				$sql = "UPDATE SUBJECT_GROUP_ATTENDANCE_LEAVE_RECORDS SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'RecordID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$sql.= " ,ModifiedDate=NOW(),ModifiedBy='$user_id' WHERE RecordID='".$map['RecordID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$keys .= ",InputDate,ModifiedDate,InputBy,ModifiedBy";
				$values .= ",NOW(),NOW(),'$user_id','$user_id'";
				
				$sql = "INSERT INTO SUBJECT_GROUP_ATTENDANCE_LEAVE_RECORDS ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function GetLessonAttendanceLeaveRecords($filterMap)
		{
			global $Lang;
			$cond = "";
			if(isset($filterMap['RecordID'])){
				if(is_array($filterMap['RecordID'])){
					$cond .= " AND r.RecordID IN ('".implode("','",$filterMap['RecordID'])."') ";
				}else{
					$cond .= " AND r.RecordID='".$filterMap['RecordID']."' ";
				}
			}
			if(isset($filterMap['RecordDate']) && $filterMap['RecordDate']!=''){
				if(is_array($filterMap['RecordDate'])){
					$cond .= " AND r.RecordDate IN ('".implode("','",$filterMap['RecordDate'])."') ";
				}else{
					$cond .= " AND r.RecordDate='".$filterMap['RecordDate']."' ";
				}
			}
			if(isset($filterMap['StudentID']))
			{
				if(is_array($filterMap['StudentID'])){
					$cond .= " AND r.StudentID IN ('".implode("','",$filterMap['StudentID'])."') ";
				}else{
					$cond .= " AND r.StudentID='".$filterMap['StudentID']."' ";
				}
			}
			if(isset($filterMap['ExcludeRecordID'])){
				if(is_array($filterMap['ExcludeRecordID'])){
					$cond .= " AND r.RecordID NOT IN ('".implode("','",$filterMap['ExcludeRecordID'])."') ";
				}else{
					$cond .= " AND r.RecordID<>'".$filterMap['ExcludeRecordID']."' ";
				}
			}
			if(isset($filterMap['LeaveType']) && $filterMap['LeaveType']!=''){
				$cond .= " AND r.LeaveType='".$filterMap['LeaveType']."' ";
			}
			
			$student_name_field = getNameFieldWithClassNumberByLang("u.");
			$modifier_name_field = getNameFieldByLang2("u2.");
			$archived_student_name_field = getNameFieldWithClassNumberByLang("au.");
			
			$sql = "SELECT r.*, 
						IF(u.UserID IS NULL,$archived_student_name_field,$student_name_field) as StudentName,
						$modifier_name_field as ModifiedName 
					FROM SUBJECT_GROUP_ATTENDANCE_LEAVE_RECORDS as r 
					LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID 
					LEFT JOIN INTRANET_USER as u2 ON u2.UserID=r.ModifiedBy 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID 
					WHERE 1 $cond 
					ORDER BY r.RecordDate,StudentName";
			$records = $this->returnResultSet($sql);
			
			return $records;
		}
		
		function DeleteLessonAttendanceLeaveRecords($recordId)
		{
			$sql = "DELETE FROM SUBJECT_GROUP_ATTENDANCE_LEAVE_RECORDS WHERE RecordID ";
			if(is_array($recordId)){
				$sql .= " IN ('".implode("','",$recordId)."')";
			}else{
				$sql .= " = '$recordId'";
			}
			$success = $this->db_db_query($sql);
			return $success;
		}
		
		function GetLessonAttendanceRecords($filterMap, $isAssoc=false)
		{
			global $intranet_root;
			
			$this->Build_Subject_Group_Student_Attendance();
			
			$cond = "";
			if(isset($filterMap['AttendOverviewID'])){
				if(is_array($filterMap['AttendOverviewID'])){
					$cond .= " AND sgao.AttendOverviewID IN ('".implode("','",$filterMap['AttendOverviewID'])."') ";
				}else{
					$cond .= " AND sgao.AttendOverviewID='".$filterMap['AttendOverviewID']."' ";
				}
			}
			if(isset($filterMap['SubjectGroupID'])){
				if(is_array($filterMap['SubjectGroupID'])){
					$cond .= " AND sgao.SubjectGroupID IN ('".implode("','",$filterMap['SubjectGroupID'])."') ";
				}else{
					$cond .= " AND sgao.SubjectGroupID='".$filterMap['SubjectGroupID']."' ";
				}
			}
			if(isset($filterMap['LessonDate'])){
				if(is_array($filterMap['LessonDate'])){
					$cond .= " AND sgao.LessonDate IN ('".implode("','",$filterMap['LessonDate'])."') ";
				}else{
					$cond .= " AND sgao.LessonDate='".$filterMap['LessonDate']."' ";
				}
			}
			if(isset($filterMap['StudentID'])){
				if(is_array($filterMap['StudentID'])){
					$cond .= " AND sgsa.StudentID IN ('".implode("','",$filterMap['StudentID'])."') ";
				}else{
					$cond .= " AND sgsa.StudentID='".$filterMap['StudentID']."' ";
				}
			}
			
			$sql = "SELECT 
						sgao.AttendOverviewID,
						sgao.SubjectGroupID,
						sgao.LessonDate,
						sgao.RoomAllocationID,
						sgao.StartTime,
						sgao.EndTime,
						sgsa.StudentID,
						sgsa.AttendStatus,
						sgsa.InTime,
						sgsa.OutTime,
						sgsa.Reason,
						sgsa.Remarks 
					FROM SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." as sgao 
					INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$this->CurAcademicYearID." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
					WHERE 1 $cond
					ORDER BY sgao.LessonDate,sgao.StartTime,sgsa.StudentID ";
			$records = $this->returnResultSet($sql);
			if($isAssoc){
				$records_size = count($records);
				$keysToRecords = array();
				for($i=0;$i<$records_size;$i++)
				{
					$tmp_date = $records[$i]['LessonDate'];
					$tmp_student_id = $records[$i]['StudentID'];
					if(!isset($keysToRecords[$tmp_date])){
						$keysToRecords[$tmp_date] = array();
					}
					if(!isset($keysToRecords[$tmp_date][$tmp_student_id])){
						$keysToRecords[$tmp_date][$tmp_student_id] = array();
					}
					$keysToRecords[$tmp_date][$tmp_student_id][] = $records[$i];
				}
				return $keysToRecords;
			}
			return $records;
		}
		
		function prepareHKUSPHFluData($AcademicYearId, $IsSendWeekly, $TargetDate='', $StartDate='', $EndDate='')
		{
			global $intranet_root, $PATH_WRT_ROOT, $file_path, $sys_custom, $eclass_db, $intranet_session_language, $config_school_code, $config_school_type, $BroadlearningClientID, $BroadlearningClientName, $is_debug;
			global $sick_code_map, $symptom_code_map, $symptom_code_size, $json, $fcm;
			
			$form = $config_school_type != '' ? $config_school_type : 'S';
			if($config_school_code == '' && $BroadlearningClientID != '') $config_school_code = $BroadlearningClientID;
			
			$StartDateTS = strtotime($StartDate);
			$EndDateTS = strtotime($EndDate);
			$number_of_days = $IsSendWeekly ? ($EndDateTS - $StartDateTS + 86400) / 86400 : 1;
			
			### [START] School data [Part 1]
			$classIdToClassLevelAry = array();
			$sql = "SELECT 
						yc.YearClassID, 
			 			yc.ClassTitleEN as ClassName, 
			 			y.WEBSAMSCode 
			 		FROM YEAR as y 
			 		INNER JOIN YEAR_CLASS as yc ON y.YearID = yc.YearID AND yc.AcademicYearID = '".$AcademicYearId."'";
			$class_level_records = $this->returnResultSet($sql);
			$accept_forms = array(1,2,3,4,5,6);
			for($i=0;$i<count($class_level_records);$i++){
				$level = intval(str_replace('NA','', str_replace('K', '', str_replace('P','', str_replace('S','', $class_level_records[$i]['WEBSAMSCode'])))));
				if($level == 0){ // handle no websams code case
					$matches = array();
					if(preg_match('/^.*(\d+).*$/i', $class_level_records[$i]['WEBSAMSCode'], $matches)){ // guess form level from class name
						$level = $matches[1];
					}
				}
				if($level == 0 || $level == '' || !in_array($level,$accept_forms)) continue;
				$classIdToClassLevelAry[$class_level_records[$i]['YearClassID']] = $level;
			}
			//debug_pr($classIdToClassLevelAry);
			$sql = "SELECT 
						yc.YearClassID, 
						CONCAT(REPLACE(su.WebSAMSRegNo,'#',''),'_',su.UserID) as student_uid  
					FROM YEAR_CLASS as yc 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.YearClassID=yc.YearClassID 
					LEFT JOIN INTRANET_USER as su On su.UserID=ycu.UserID
					WHERE yc.AcademicYearID='".$AcademicYearId."' 
					ORDER BY yc.Sequence ";
			$class_student_ary = $this->returnResultSet($sql);
			$class_student_size = count($class_student_ary);
			
			//$classIdToStudentAry = array();
			$studentIdToClassMap = array();
			$levelToStudentAry = array();
			
			for($i=0;$i<$class_student_size;$i++){
				$studentIdToClassMap[$class_student_ary[$i]['student_uid']] = $class_student_ary[$i]['YearClassID'];
				$level = $classIdToClassLevelAry[$class_student_ary[$i]['YearClassID']];
				if($level == '') continue; // no matched form/class level, skip it
				if(!isset($levelToStudentAry[$level])){
					$levelToStudentAry[$level] = array();
					$levelToStudentAry[$level]['Students'] = array();
					$levelToStudentAry[$level]['ConsentStudents'] = array();
					$levelToStudentAry[$level]['ConsentAbsentStudents'] = array();
					$levelToStudentAry[$level]['AbsentStudents'] = array();
					
					$levelToStudentAry[$level]['WeeklyStudents'] = array();
					$levelToStudentAry[$level]['WeeklyConsentStudents'] = array();
					$levelToStudentAry[$level]['WeeklyConsentAbsentStudents'] = array();
					$levelToStudentAry[$level]['WeeklyAbsentStudents'] = array();
				}
				//$classIdToStudentAry[$class_student_ary[$i]['YearClassID']]['Students'][] = $class_student_ary[$i]['student_uid'];
				$levelToStudentAry[$level]['Students'][] = $class_student_ary[$i]['student_uid'];
				if($IsSendWeekly){
					$levelToStudentAry[$level]['WeeklyStudents'][$class_student_ary[$i]['student_uid']] = $number_of_days; // total days for each student
				}
			}
			### [End] School data [Part 1]
			
			
			### [START] Parent consent data
			$student_name_field = getNameFieldByLang2("su.");
			$sql = "SELECT 
						DATE_FORMAT(a.SignDate,'%Y-%m-%d') as consent_date,
						REPLACE(su.WebSAMSRegNo,'#','') as student_edb,
						CONCAT(REPLACE(su.WebSAMSRegNo,'#',''),'_',su.UserID) as student_uid,
						$student_name_field as student_name,
						a.ParentName as guardian_name,
						IF(a.IsAgreed='1','A','D') as agreement_status,
						yc.YearClassID as 'grade',
						'$config_school_code' as school_edb 
					FROM HKUSPH_AGREEMENT as a 
					INNER JOIN INTRANET_USER as su ON su.UserID=a.StudentID 
					INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=a.StudentID 
					INNER JOIN INTRANET_USER as pu ON pu.UserID=a.ParentID AND r.ParentID=pu.UserID 
					INNER JOIN YEAR_CLASS_USER as ycu ON su.UserID=ycu.UserID 
					INNER JOIN YEAR_CLASS as yc ON ycu.YearClassID=yc.YearClassID 
					WHERE a.Status='a' AND yc.AcademicYearID='".$AcademicYearId."' ";
			$consent_records = $this->returnResultSet($sql);
			$consent_record_size = count($consent_records);
			
			$sep = ",";
			$wrap = '"';
			$rn = "\r\n";
			$consent_content = '';
			for($i=0;$i<$consent_record_size;$i++){
				$consent_record = $consent_records[$i];
				$consent_students[] = $consent_record['student_uid'];
				
				$year_class_id = $studentIdToClassMap[$consent_record['student_uid']];
				$level = $classIdToClassLevelAry[$year_class_id];
				if($level == '') continue;
				//$classIdToStudentAry[$year_class_id]['ConsentStudents'][] = $consent_record['student_uid'];
				if($consent_record['agreement_status'] == 'A'){
					$levelToStudentAry[$level]['ConsentStudents'][] = $consent_record['student_uid'];
					
					if($IsSendWeekly){
						$number_of_days_since_consent = ($EndDateTS - max($StartDateTS, strtotime($consent_record['consent_date'])) + 86400 ) / 86400;
						$levelToStudentAry[$level]['WeeklyConsentStudents'][$consent_record['student_uid']] = array('ConsentDate'=>$consent_record['consent_date'],'ConsentDays'=>$number_of_days_since_consent); // number of days since consented
					}
				}
				$tmp_sep = "";
				$row = '';
				foreach($consent_record as $key => $val){
					if($key == 'grade'){
						$val = $level;
					}
					$row .= $tmp_sep.$wrap.str_replace("\"","\"\"",$val).$wrap;
					$tmp_sep = $sep;
				}
				$row .= $rn;
				$consent_content .= $row;
			}
			### [END] Parent consent data
			
			### [START] Student data
			$submission_method_field = "IF(s.SubmittedFrom='' OR s.SubmittedFrom IS NULL,'A',s.SubmittedFrom)";
			if(!$IsSendWeekly && $TargetDate !='' && isset($sys_custom['HKUSPH_SubmissionMethodByDateRange'])){
				for($i=0;$i<count($sys_custom['HKUSPH_SubmissionMethodByDateRange']);$i++){
					if($TargetDate >= $sys_custom['HKUSPH_SubmissionMethodByDateRange'][$i]['FromDate'] && $TargetDate <= $sys_custom['HKUSPH_SubmissionMethodByDateRange'][$i]['ToDate']){
						$submission_method_field = "'".$sys_custom['HKUSPH_SubmissionMethodByDateRange'][$i]['SubmissionMethod']."'";
					}
				}
			}
			if(isset($sys_custom['HKUSPH_SubmissionMethodByTimePeriod']) && count($sys_custom['HKUSPH_SubmissionMethodByTimePeriod'])>0){
				$submission_method_field = " CASE  ";
				for($i=0;$i<count($sys_custom['HKUSPH_SubmissionMethodByTimePeriod']);$i++){
					$submission_method_field .= " WHEN s.IsSickLeave='1' AND s.CraetedDate BETWEEN '".$sys_custom['HKUSPH_SubmissionMethodByTimePeriod'][$i]['FromTime']."' AND '".$sys_custom['HKUSPH_SubmissionMethodByTimePeriod'][$i]['ToTime']."' THEN '".$sys_custom['HKUSPH_SubmissionMethodByTimePeriod'][$i]['SubmissionMethod']."' ";
				}
				$submission_method_field.= " ELSE IF(s.SubmittedFrom='' OR s.SubmittedFrom IS NULL,'A',s.SubmittedFrom) ";
				$submission_method_field.= " END ";
			}
			
			$sql = "SELECT 
						DATE_FORMAT(s.CraetedDate,'%Y-%m-%d %H:%i:%s') as submission_datetime,
						REPLACE(su.WebSAMSRegNo,'#','') as student_edb,
						CONCAT(REPLACE(su.WebSAMSRegNo,'#',''),'_',su.UserID) as student_uid,
						'$config_school_code' as school_edb,
						'$form' as school_type,
						yc.YearClassID as class, 
						IF(s.IsSickLeave='1','SL',IF(s.SubmittedFrom='N','NA','CL')) as absence_reason,
						DATE_FORMAT(s.LeaveDate,'%Y-%m-%d') as leave_date,
						DATE_FORMAT(s.SickDate,'%Y-%m-%d') as onset_date,
						s2.SickCode as disease_code,
						s2.Other as disease_other,
						GROUP_CONCAT(DISTINCT s3.SymptomCode SEPARATOR ',') as symptom_json,
						GROUP_CONCAT(DISTINCT s3.Other SEPARATOR ',') as symptom_other,
						IF(COUNT(r.RecordID)>0,'0','1') as present_at_school,
						$submission_method_field as submission_method,
						IF(s.ModifiedDate IS NOT NULL, DATE_FORMAT(s.ModifiedDate,'%Y-%m-%d %H:%i:%s'),DATE_FORMAT(s.CraetedDate,'%Y-%m-%d %H:%i:%s')) as lastmodified_datetime
					FROM HKUSPH_SICK_LEAVE as s 
					INNER JOIN INTRANET_USER as su ON su.UserID=s.StudentID 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=s.StudentID 
					INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID 
					INNER JOIN YEAR as y ON y.YearID=yc.YearID 
					LEFT JOIN HKUSPH_SICK_LEAVE_SICK as s2 ON s2.SickLeaveID=s.SickLeaveID 
					LEFT JOIN HKUSPH_SICK_LEAVE_SYMPTOM as s3 ON s3.SickLeaveID=s.SickLeaveID  
					LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r ON r.RecordDate=DATE_FORMAT(s.LeaveDate,'%Y-%m-%d') AND r.StudentID=s.StudentID AND r.RecordType='".PROFILE_TYPE_ABSENT."' AND (r.RecordStatus='0' OR r.RecordStatus IS NULL)  
					WHERE ".($IsSendWeekly? " (DATE_FORMAT(s.LeaveDate,'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate') " : " DATE_FORMAT(s.LeaveDate,'%Y-%m-%d')='$TargetDate' ")." AND yc.AcademicYearID='".$AcademicYearId."' 
					GROUP BY s.SickLeaveID  
					ORDER BY s.LeaveDate,yc.YearClassID,s.StudentID";
			$student_records1 = $this->returnResultSet($sql);
			
			$sql = "SELECT 
						'' as submission_datetime,
						REPLACE(su.WebSAMSRegNo,'#','') as student_edb,
						CONCAT(REPLACE(su.WebSAMSRegNo,'#',''),'_',su.UserID) as student_uid,
						'$config_school_code' as school_edb,
						'$form' as school_type,
						yc.YearClassID as class, 
						'NA' as absence_reason,
						r.RecordDate as leave_date,
						'' as onset_date,
						'' as disease_code,
						'' as disease_other,
						'' as symptom_json,
						'' as symptom_other,
						'0' as present_at_school,
						'' as submission_method,
						'' as lastmodified_datetime
					FROM CARD_STUDENT_PROFILE_RECORD_REASON as r 
					INNER JOIN PROFILE_STUDENT_ATTENDANCE as p ON p.UserID=r.StudentID AND DATE_FORMAT(p.AttendanceDate,'%Y-%m-%d')=r.RecordDate AND p.DayType=r.DayType AND p.RecordType=r.RecordType 
					INNER JOIN INTRANET_USER as su ON su.UserID=r.StudentID 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=r.StudentID 
					INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID 
					INNER JOIN YEAR as y ON y.YearID=yc.YearID 
					LEFT JOIN HKUSPH_SICK_LEAVE as s ON DATE_FORMAT(s.LeaveDate,'%Y-%m-%d')=r.RecordDate AND s.StudentID=r.StudentID 
					WHERE r.RecordType='".PROFILE_TYPE_ABSENT."' ".($IsSendWeekly? " AND (r.RecordDate BETWEEN '$StartDate' AND '$EndDate') " : " AND r.RecordDate='$TargetDate' ")." AND (r.RecordStatus='0' OR r.RecordStatus IS NULL) AND yc.AcademicYearID='".$AcademicYearId."' AND s.SickLeaveID IS NULL 
					GROUP BY r.RecordDate,r.StudentID  
					ORDER BY r.RecordDate,yc.YearClassID,r.StudentID";
			$student_records2 = $this->returnResultSet($sql);
			$student_records = array_values(array_merge($student_records1, $student_records2));
			$student_record_size = count($student_records);
			
			$student_content = '';
			for($i=0;$i<$student_record_size;$i++){
				$student_record = $student_records[$i];
				$year_class_id = $student_record['class'];
				$level = $classIdToClassLevelAry[$year_class_id];
				if($level == '') continue;
				//$classIdToStudentAry[$year_class_id]['AbsentStudents'][] = $student_record['student_uid'];
				$levelToStudentAry[$level]['AbsentStudents'][] = $student_record['student_uid'];
				if($IsSendWeekly){
					if(!isset($levelToStudentAry[$level]['WeeklyAbsentStudents'][$student_record['student_uid']])) $levelToStudentAry[$level]['WeeklyAbsentStudents'][$student_record['student_uid']] = array();
					$levelToStudentAry[$level]['WeeklyAbsentStudents'][$student_record['student_uid']][$student_record['leave_date']] = 1;
				}
				
				$tmp_sep = "";
				$row = '';
				foreach($student_record as $key => $val){
					$v = $val;
					if($key == 'class'){
						$v = $classIdToClassLevelAry[$year_class_id];
					}
					if($key == 'disease_code') $v = $sick_code_map[$val];
					if($key == 'symptom_json') {
						$symptoms = $val != ''? explode(',',$val) : array();
						$symptom_ary = array();
						for($j=0;$j<$symptom_code_size;$j++){
							$symptom_ary[$j+1] = false;
						}
						for($j=0;$j<count($symptoms);$j++){
							$symptom_ary[$symptom_code_map[$symptoms[$j]]] = true;
						}
						$v = $json->encode($symptom_ary);
					}
					if($key == 'symptom_other'){
						$others = explode(',',$val);
						$v = '';
						for($j=0;$j<count($others);$j++){
							if(trim($others[$j])!=''){
								$v .= ($v != ''?', ':'').$others[$j];
							}
						}
					}
					$row .= $tmp_sep.$wrap.str_replace("\"","\"\"",$v).$wrap;
					$tmp_sep = $sep;
				}
				$row .= $rn;
				$student_content .= $row;
			}
			
			### [END] Student data
			
			
			### [START] School data [Part 2]
			$school_content = '';
			if(count($levelToStudentAry)>0){
				foreach($levelToStudentAry as $level => $ary){
					// one student may have multiple leave records per day
					$levelToStudentAry[$level]['AbsentStudents'] = array_values(array_unique($levelToStudentAry[$level]['AbsentStudents']));
					$levelToStudentAry[$level]['ConsentAbsentStudents'] = array_values(array_unique(array_intersect($levelToStudentAry[$level]['ConsentStudents'],$levelToStudentAry[$level]['AbsentStudents'])));
					
					if($IsSendWeekly){
						$levelToStudentAry[$level]['AbsentStudents'] = array();
						$levelToStudentAry[$level]['Students'] = array();
						$levelToStudentAry[$level]['ConsentAbsentStudents'] = array();
						$levelToStudentAry[$level]['ConsentStudents'] = array();
						
						if(count($levelToStudentAry[$level]['WeeklyStudents'])>0)
						{
							foreach($levelToStudentAry[$level]['WeeklyStudents'] as $student_id => $days)
							{
								for($i=1;$i<=$days;$i++){
									$levelToStudentAry[$level]['Students'][] = array($student_id, $i);
								}
							}
						}
						if(count($levelToStudentAry[$level]['WeeklyAbsentStudents'])>0){
							foreach($levelToStudentAry[$level]['WeeklyAbsentStudents'] as $student_id => $absent_date_ary)
							{
								if(count($absent_date_ary)>0)
								{
									foreach($absent_date_ary as $absent_date => $ary)
									{
										$levelToStudentAry[$level]['AbsentStudents'][] = array($student_id,$absent_date);
										
										if(isset($levelToStudentAry[$level]['WeeklyAbsentStudents'][$student_id]) 
											&& isset($levelToStudentAry[$level]['WeeklyAbsentStudents'][$student_id][$absent_date])){
												if(isset($levelToStudentAry[$level]['WeeklyConsentStudents'][$student_id]) && $absent_date>=$levelToStudentAry[$level]['WeeklyConsentStudents'][$student_id]['ConsentDate'])
												{
													$levelToStudentAry[$level]['ConsentAbsentStudents'][] = array($student_id, $absent_date);
												}
										}
									}
								}
							}
						}
						
						if(count($levelToStudentAry[$level]['WeeklyConsentStudents'])>0){
							foreach($levelToStudentAry[$level]['WeeklyConsentStudents'] as $student_id => $days_ary)
							{
								for($i=1;$i<=$days_ary['ConsentDays'];$i++){
									$levelToStudentAry[$level]['ConsentStudents'][] = array($student_id, $i);
								}
							}
						}
						//if($is_debug){
							//echo 'WeeklyStudents';debug_pr($levelToStudentAry[$level]['WeeklyStudents']);
							//echo 'WeeklyAbsentStudents';debug_pr($levelToStudentAry[$level]['WeeklyAbsentStudents']);
							//echo 'WeeklyConsentStudents';debug_pr($levelToStudentAry[$level]['WeeklyConsentStudents']);
						//}
					}
					
					$row = '';
					$row .= $wrap.$TargetDate.$wrap;
					$row .= $sep.$wrap.$config_school_code.$wrap;
					$row .= $sep.$wrap.str_replace("\"","\"\"",$BroadlearningClientName).$wrap;
					$row .= $sep.$wrap.$form.$wrap;
					$row .= $sep.$wrap.$level.$wrap;
					$row .= $sep.$wrap.count($levelToStudentAry[$level]['AbsentStudents']).$wrap;
					$row .= $sep.$wrap.count($levelToStudentAry[$level]['Students']).$wrap;
					$row .= $sep.$wrap.count($levelToStudentAry[$level]['ConsentAbsentStudents']).$wrap;
					$row .= $sep.$wrap.count($levelToStudentAry[$level]['ConsentStudents']).$wrap;
					$row .= $rn;
					$school_content .= $row;
				}	
			}
			### [END] School data [Part 2]
			
			return array($consent_content, $student_content, $school_content);
		}
		
		function sendHKUFluData($TargetDate,$SendWeeklyData=0)
		{
			global $intranet_root, $PATH_WRT_ROOT, $file_path, $sys_custom, $eclass_db, $intranet_session_language, $config_school_code, $config_school_type, $BroadlearningClientID, $BroadlearningClientName, $is_debug;
			global $sick_code_map, $symptom_code_map, $symptom_code_size, $json, $fcm;
		
			if(!$sys_custom['eClassApp']['HKUSPH']) return;
			
			include_once($intranet_root."/includes/form_class_manage.php");
			include_once($intranet_root."/includes/HKUFlu/HKUFlu.php");
			include_once($intranet_root."/includes/json.php");
			
			//$ts = strtotime($TargetDate);
			//$day = date("j", $ts);
			//$send_all_data = in_array($day,array(1,16));
			$send_weekly_data = $SendWeeklyData;
			//$form = 'S';
			if($send_weekly_data){
				$target_date_ts = strtotime($TargetDate);
				$day = date("j", $target_date_ts);
				if($day == 1){ // last month's 16th to last month's last day 
					$last_month_ts = $target_date_ts - 86400;
					$start_date = date("Y-m-16", $last_month_ts);
					$end_date = date("Y-m-d", $last_month_ts);
				}else if($day == 16){ // this month's 1st to 15th
					$start_date = date("Y-m-01", $target_date_ts);
					$end_date = date("Y-m-15", $target_date_ts);
				}
				//debug_pr($start_date." - ".$end_date);
			}
			
			$HKUFlu =  new HKUFlu();
			$json = new JSON_obj();
			$sick_codes = $HKUFlu->getSickCode();
			$symptom_codes = $HKUFlu->getSymptomCode();
			$symptom_code_size = count($symptom_codes);
			$sick_code_map = array();
			$symptom_code_map = array();
			for($i=0;$i<count($sick_codes);$i++){
				$sick_code_map[$sick_codes[$i]] = $i+1;
			}
			for($i=0;$i<$symptom_code_size;$i++){
				$symptom_code_map[$symptom_codes[$i]] = $i+1;
			}
			
			### Find the earliest date that have data to begin with
			//$sql = "SELECT MIN(SignDate) FROM HKUSPH_AGREEMENT";
			//$min_date_record = $this->returnVector($sql);
			//$min_date = count($min_date_record)>0 && $min_date_record[0]!=''?date("Y-m-d", strtotime($min_date_record[0])) : "2016-11-01";
			
			$fcm = new form_class_manage();
			$currentAcademicYearInfo = getAcademicYearAndYearTermByDate($send_weekly_data? $start_date : $TargetDate);
			$current_academic_year_id = $currentAcademicYearInfo['AcademicYearID'];
			
			$all_consent_content = '';
			$all_student_content = '';
			$all_school_content = '';
			if($send_weekly_data)
			{
				/*
				$academic_year_list = $fcm->Get_Academic_Year_List($__AcademicYearIDArr='', $__OrderBySequence=0, $__excludeYearIDArr=array(), $__NoPastYear=0, $__PastAndCurrentYearOnly=1, $__ExcludeCurrentYear=0, $__ComparePastYearID='', $__SortOrder='ASC');
				$academic_year_list_size = count($academic_year_list);
				
				for($i=0;$i<$academic_year_list_size;$i++){
					if($academic_year_list[$i]['AcademicYearStart']>=$min_date || ($academic_year_list[$i]['AcademicYearStart']<=$min_date && $academic_year_list[$i]['AcademicYearEnd']>=$min_date))
					{
						$this_content = $this->prepareHKUSPHFluData($academic_year_list[$i]['AcademicYearID'], 1, $TargetDate, $min_date);
						$all_consent_content = $this_content[0];
						$all_student_content = $this_content[1];
						$all_school_content = $this_content[2];
					}
				}
				*/
				
				$this_content = $this->prepareHKUSPHFluData($current_academic_year_id, 1, $TargetDate, $start_date, $end_date);
				$all_consent_content = $this_content[0];
				$all_student_content = $this_content[1];
				$all_school_content = $this_content[2];
				
			}else{
				$today_content = $this->prepareHKUSPHFluData($current_academic_year_id, 0, $TargetDate);
				$consent_content = $today_content[0];
				$student_content = $today_content[1];
				$school_content = $today_content[2];
			}
			
			if($is_debug){
				if($sys_custom['HKUSPH_log']){
					if($send_weekly_data)
					{
						$log_content .= $all_consent_content."\n\n";
						$log_content .= $all_student_content."\n\n";
						$log_content .= $all_school_content."\n\n";
					}else{
						$log_content .= $consent_content."\n\n";
						$log_content .= $student_content."\n\n";
						$log_content .= $school_content."\n\n";
					}
					$log_file = $file_path."/file/HKUSPH_log.txt";
					$file_handle = fopen($log_file,"a+");
					fwrite($file_handle,$log_content);
					fclose($file_handle);
				}else{
					if($send_weekly_data)
					{
						debug_pr($all_consent_content);
						debug_pr($all_student_content);
						debug_pr($all_school_content);
					}else{
						debug_pr($consent_content);
						debug_pr($student_content);
						debug_pr($school_content);
					}
				}
				return true;
			}
			
			$host = isset($sys_custom['HKUSPH_host']) && $sys_custom['HKUSPH_host'] != ''? $sys_custom['HKUSPH_host'] : 'sa2.influenza.hk';
			
			if($send_weekly_data)
			{
				$all_consent_salt = '_salt_consent_weekly_sa2+sph-hku*hk';
				$all_consent_data = array();
				$all_consent_data['post1'] = $all_consent_content;
				$all_consent_data['post2'] = $TargetDate;
				$all_consent_data['post3'] = md5($TargetDate.$all_consent_salt);
				$all_consent_url = "https://".$host."/upload_data_consent_weekly_submit.php";
				
				$all_student_salt = '_salt_student_weekly_sa2+sph-hku*hk';
				$all_student_data = array();
				$all_student_data['post1'] = $all_student_content;
				$all_student_data['post2'] = $TargetDate;
				$all_student_data['post3'] = md5($TargetDate.$all_student_salt);
				$all_student_url = "https://".$host."/upload_data_student_weekly_submit.php";
				
				$all_school_salt = '_salt_school_weekly_sa2+sph-hku*hk';
				$all_school_data = array();
				$all_school_data['post1'] = $all_school_content;
				$all_school_data['post2'] = $TargetDate;
				$all_school_data['post3'] = md5($TargetDate.$all_school_salt);
				$all_school_url = "https://".$host."/upload_data_school_weekly_submit.php";
			}else{
				$consent_salt = '_salt_consent_sa2+sph-hku*hk';
				$consent_data = array();
				$consent_data['post1'] = $consent_content;
				$consent_data['post2'] = $TargetDate;
				$consent_data['post3'] = md5($TargetDate.$consent_salt);
				$consent_url = "https://".$host."/upload_data_consent_submit.php";
				
				$student_salt = '_salt_student_sa2+sph-hku*hk';
				$student_data = array();
				$student_data['post1'] = $student_content;
				$student_data['post2'] = $TargetDate;
				$student_data['post3'] = md5($TargetDate.$student_salt);
				$student_url = "https://".$host."/upload_data_student_submit.php";
				
				$school_salt = '_salt_school_sa2+sph-hku*hk';
				$school_data = array();
				$school_data['post1'] = $school_content;
				$school_data['post2'] = $TargetDate;
				$school_data['post3'] = md5($TargetDate.$school_salt);
				$school_url = "https://".$host."/upload_data_school_submit.php";
			}
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			if($send_weekly_data)
			{
				curl_setopt($ch, CURLOPT_URL, $all_consent_url);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $all_consent_data);
				$all_consent_response = curl_exec($ch); // "1" for success
				
				curl_setopt($ch, CURLOPT_URL, $all_student_url);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $all_student_data);
				$all_student_response = curl_exec($ch); // "1" for success
				
				curl_setopt($ch, CURLOPT_URL, $all_school_url);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $all_school_data);
				$all_school_response = curl_exec($ch); // "1" for success
			}else{
				curl_setopt($ch, CURLOPT_URL, $consent_url);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $consent_data);
				$consent_response = curl_exec($ch); // "1" for success
				
				curl_setopt($ch, CURLOPT_URL, $student_url);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $student_data);
				$student_response = curl_exec($ch); // "1" for success
				
				curl_setopt($ch, CURLOPT_URL, $school_url);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $school_data);
				$school_response = curl_exec($ch); // "1" for success
			}
			
			curl_close($ch);
			
			if($sys_custom['HKUSPH_log']){

				if($send_weekly_data)
				{
					$log_content .= $all_consent_content."\n".$all_consent_response."\n\n";
					$log_content .= $all_student_content."\n".$all_student_response."\n\n";
					$log_content .= $all_school_content."\n".$all_school_response."\n\n";
				}else{
					$log_content .= $consent_content."\n".$consent_response."\n\n";
					$log_content .= $student_content."\n".$student_response."\n\n";
					$log_content .= $school_content."\n".$school_response."\n\n";
				}
				$log_file = $file_path."/file/HKUSPH_log.txt";
				$file_handle = fopen($log_file,"a+");
				fwrite($file_handle,$log_content);
				fclose($file_handle);
				
				$log_file_tmp = $file_path."/file/HKUSPH_log_tmp.txt";
				$num_line_to_keep = 200000;
				$line_count = intval(shell_exec("cat '$log_file' | wc -l"));
				if($line_count > $num_line_to_keep){
					shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file' > '$log_file_tmp'");
					shell_exec("rm '$log_file'");
					shell_exec("mv '$log_file_tmp' '$log_file'");
				}
			}
			
			return ($send_weekly_data? ($all_consent_response=="1" && $all_student_response=="1" && $all_school_response=="1") : ($consent_response == "1" && $student_response == "1" && $school_response == "1") );
		}
		
		
		function UpsertCustomizedTimeSlotRecords($map)
		{
			$fields = array('RecordID','StartTime','EndTime','Status','RecordStatus');
			$user_id = $_SESSION['UserID'];
			
			if(count($map) == 0) return false;
			
			if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
			{
				$sql = "UPDATE CARD_STUDENT_CUSTOMIZED_TIME_SLOT_SETTINGS SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'RecordID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$sql.= " ,DateModified=NOW(),ModifiedBy='$user_id' WHERE RecordID='".$map['RecordID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
				$values .= ",NOW(),NOW(),'$user_id','$user_id'";
				
				$sql = "INSERT INTO CARD_STUDENT_CUSTOMIZED_TIME_SLOT_SETTINGS ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function GetCustomizedTimeSlotRecords($filterMap)
		{
			global $Lang;
			$cond = "";
			if(isset($filterMap['RecordID'])){
				if(is_array($filterMap['RecordID'])){
					$cond .= " AND r.RecordID IN ('".implode("','",$filterMap['RecordID'])."') ";
				}else{
					$cond .= " AND r.RecordID='".$filterMap['RecordID']."' ";
				}
			}
			if(isset($filterMap['RecordStatus']) && $filterMap['RecordStatus']!=''){
				if(is_array($filterMap['RecordStatus'])){
					$cond .= " AND r.RecordStatus IN ('".implode("','",$filterMap['RecordStatus'])."') ";
				}else{
					$cond .= " AND r.RecordStatus='".$filterMap['RecordStatus']."' ";
				}
			}
			if(isset($filterMap['Status']))
			{
				if(is_array($filterMap['Status'])){
					$cond .= " AND r.Status IN ('".implode("','",$filterMap['Status'])."') ";
				}else{
					$cond .= " AND r.Status='".$filterMap['Status']."' ";
				}
			}
			if(isset($filterMap['ExcludeRecordID'])){
				if(is_array($filterMap['ExcludeRecordID'])){
					$cond .= " AND r.RecordID NOT IN ('".implode("','",$filterMap['ExcludeRecordID'])."') ";
				}else{
					$cond .= " AND r.RecordID<>'".$filterMap['ExcludeRecordID']."' ";
				}
			}
			if(isset($filterMap['CheckTimeOverlap']) && $filterMap['CheckTimeOverlap'] && $filterMap['StartTime']!='' && $filterMap['EndTime']!=''){
				$cond .= " AND (('".$filterMap['StartTime']."' BETWEEN r.StartTime AND r.EndTime) 
							OR ('".$filterMap['EndTime']."' BETWEEN r.StartTime AND r.EndTime)
							OR ('".$filterMap['StartTime']."' <= r.StartTime AND '".$filterMap['EndTime']."' >= r.EndTime)) ";
			}
			if(isset($filterMap['InBetweenTime']) && $filterMap['Time']!=''){
				$cond .= " AND ('".$filterMap['Time']."' BETWEEN r.StartTime AND r.EndTime) ";
			}
			
			$sql = "SELECT r.* 
					FROM CARD_STUDENT_CUSTOMIZED_TIME_SLOT_SETTINGS as r 
					WHERE 1 $cond 
					ORDER BY r.StartTime";
			$records = $this->returnResultSet($sql);
			//debug_pr($sql);
			return $records;
		}
		
		function DeleteCustomizedTimeSlotRecords($recordId)
		{
			$sql = "DELETE FROM CARD_STUDENT_CUSTOMIZED_TIME_SLOT_SETTINGS WHERE RecordID ";
			if(is_array($recordId)){
				$sql .= " IN ('".implode("','",$recordId)."')";
			}else{
				$sql .= " = '$recordId'";
			}
			$success = $this->db_db_query($sql);
			return $success;
		}
		
		function UpsertTeacherRemarkPresetRecords($map)
		{
		    $fields = array('RecordID','ReasonText');
		    $user_id = $_SESSION['UserID'];
		    
		    if(count($map) == 0) return false;
		    
		    if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
		    {
		        $sql = "UPDATE CARD_STUDENT_PRESET_TEACHER_REMARK SET ";
		        $sep = "";
		        foreach($map as $key => $val){
		            if(!in_array($key, $fields) || $key == 'RecordID') continue;
		            
		            $sql .= $sep."$key='".$this->Get_Safe_Sql_Query(stripslashes(trim($val)))."'";
		            $sep = ",";
		        }
		        $sql.= " ,DateModify=NOW(),ModifyBy='$user_id' WHERE RecordID='".$map['RecordID']."'";
		        $success = $this->db_db_query($sql);
		    }else{
		        $keys = '';
		        $values = '';
		        $sep = "";
		        foreach($map as $key => $val){
		            if(!in_array($key, $fields)) continue;
		            $keys .= $sep."$key";
		            $values .= $sep."'".$this->Get_Safe_Sql_Query(stripslashes(trim($val)))."'";
		            $sep = ",";
		        }
		        $keys .= ",DateInput,DateModify,InputBy,ModifyBy";
		        $values .= ",NOW(),NOW(),'$user_id','$user_id'";
		        
		        $sql = "INSERT INTO CARD_STUDENT_PRESET_TEACHER_REMARK ($keys) VALUES ($values)";
		        $success = $this->db_db_query($sql);
		    }
		    
		    return $success;
		}
		
		function GetTeacherRemarkPresetRecords($filterMap)
		{
		    $cond = "";
		    if(isset($filterMap['RecordID'])){
		        if(is_array($filterMap['RecordID'])){
		            $cond .= " AND r.RecordID IN ('".implode("','",$filterMap['RecordID'])."') ";
		        }else{
		            $cond .= " AND r.RecordID='".$filterMap['RecordID']."' ";
		        }
		    }

		    $sql = "SELECT r.*
					FROM CARD_STUDENT_PRESET_TEACHER_REMARK as r
					WHERE 1 $cond";
		    $records = $this->returnResultSet($sql);
		    //debug_pr($sql);
		    return $records;
		}
		
		function DeleteTeacherRemarkPresetRecords($recordId)
		{
		    $sql = "DELETE FROM CARD_STUDENT_PRESET_TEACHER_REMARK WHERE RecordID ";
		    if(is_array($recordId)){
		        $sql .= " IN ('".implode("','",$recordId)."')";
		    }else{
		        $sql .= " = '$recordId'";
		    }
		    $success = $this->db_db_query($sql);
		    return $success;
		}
		
		function SyncProfileRecordToPortfolio($studentId, $recordDate, $dayType, $recordType)
		{
			global $intranet_db, $eclass_db;
			
			$success = true;
			
			$sql = "SELECT * FROM ".$eclass_db.".ATTENDANCE_STUDENT WHERE UserID='$studentId' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d')='$recordDate' AND DayType='$dayType' AND RecordType='$recordType'";
			$portfolio_records = $this->returnResultSet($sql);
			
			$sql = "SELECT * FROM ".$intranet_db.".PROFILE_STUDENT_ATTENDANCE WHERE UserID='$studentId' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d')='$recordDate' AND DayType='$dayType' AND RecordType='$recordType'";
			$profile_records = $this->returnResultSet($sql);
			
			if(count($profile_records)>0)
			{
				$academic_year_id = $profile_records[0]['AcademicYearID'];
				$year_term_id = $profile_records[0]['YearTermID'];
				$year = $this->Get_Safe_Sql_Query($profile_records[0]['Year']);
				$semester = $profile_records[0]['Semester'];
				$reason = $this->Get_Safe_Sql_Query($profile_records[0]['Reason']);
				$remark = $this->Get_Safe_Sql_Query($profile_records[0]['Remark']);
				$class_name = $this->Get_Safe_Sql_Query($profile_records[0]['ClassName']);
				$class_number = $this->Get_Safe_Sql_Query($profile_records[0]['ClassNumber']);
				if(count($portfolio_records)>0){
					// do update
					$record_id = $portfolio_records[0]['RecordID'];
					
					$sql = "UPDATE ".$eclass_db.".ATTENDANCE_STUDENT SET AcademicYearID='$academic_year_id',Year='$year',YearTermID='$year_term_id',Semester='$semester',ClassName='$class_name',ClassNumber='$class_number',Reason='$reason',Remark='$remark',ModifiedDate=NOW() WHERE RecordID='$record_id'";
					$success = $this->db_db_query($sql);
				}else{
					// do insert
					$sql = "INSERT INTO ".$eclass_db.".ATTENDANCE_STUDENT (UserID,AcademicYearID,Year,YearTermID,Semester,IsAnnual,ClassName,ClassNumber,AttendanceDate,DayType,Reason,RecordType,Remark,InputDate,ModifiedDate) 
							VALUES ('$studentId','$academic_year_id','$year','$year_term_id','$semester','0','$class_name','$class_number','$recordDate 00:00:00','$dayType','$reason','$recordType','$remark',NOW(),NOW())";
					$success = $this->db_db_query($sql);
				}
			}else{
				// do delete
				$sql = "DELETE FROM ".$eclass_db.".ATTENDANCE_STUDENT WHERE UserID='$studentId' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d')='$recordDate' AND DayType='$dayType' AND RecordType='$recordType'";
				$success = $this->db_db_query($sql);
			}
			return $success;
		}
		
		function log($log_row)
		{
			global $intranet_root, $file_path, $sys_custom;
			
			$num_line_to_keep = isset($sys_custom['StudentAttendanceLogMaxLine'])? $sys_custom['StudentAttendanceLogMaxLine'] : 400000;
			$log_file_path = $file_path."/file/student_attendance_log";
			$log_file_path_tmp = $file_path."/file/student_attendance_log_tmp";
			// assume $log_row does not contains single quote that would break the shell command statement
			shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
			$line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
			if($line_count > $num_line_to_keep){
				shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
				shell_exec("rm '$log_file_path'");
				shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
			}
		}
		
		function getNoCardPhotoRecords($filterMap)
		{
			global $Lang, $LAYOUT_SKIN, $file_path;
			
			$name_field = Get_Lang_Selection("u.ChineseName","u.EnglishName");
			$archived_name_field = Get_Lang_Selection("au.ChineseName","au.EnglishName");
			$cond = "";
			if(isset($filterMap['RecordID'])){
				$recordId = IntegerSafe($filterMap['RecordID']);
				if(is_array($recordId)){
					$cond .= " AND r.RecordID IN (".implode(",",$recordId).") ";
				}else{
					$cond .= " AND r.RecordID='$recordId' ";
				}
			}
			if(isset($filterMap['RecordDate']) && $filterMap['RecordDate']!=''){
				$cond .= " AND r.RecordDate = '".$filterMap['RecordDate']."' ";
			}
			if(isset($filterMap['StartDate']) && $filterMap['StartDate']!=''){
				$cond .= " AND r.RecordDate >= '".$filterMap['StartDate']."' ";
			}
			if(isset($filterMap['EndDate']) && $filterMap['EndDate']!=''){
				$cond .= " AND r.RecordDate <= '".$filterMap['EndDate']."' ";
			}
			
			if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
				$keyword = trim( $this->is_magic_quotes_active ? stripslashes($filterMap['Keyword']): $filterMap['Keyword'] );
				$keyword = $this->Get_Safe_Sql_Like_Query($keyword);
				$cond .= " AND ($name_field LIKE '%$keyword%' OR $archived_name_field LIKE '%$keyword%' OR r.SiteName LIKE '%$keyword%') ";
			}
			
			if($filterMap['GetDBQuery']){
				$libdbtable = $filterMap['libdbtable'];
				$field_array = array("r.RecordDate","StudentClassName","StudentClassNumber","StudentName","r.TakePhotoTime","r.SiteName","Photo");
				$sql = "SELECT 
							r.RecordDate,
							IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName) as StudentClassName,
							IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber) as StudentClassNumber,
							IF(u.UserID IS NOT NULL,$name_field,CONCAT('<span class=\"red\">*</span>',$archived_name_field)) as StudentName,
							r.TakePhotoTime,
							r.SiteName,
							CONCAT('<a href=\"/file/student_attendance/no_card_photo/',r.PhotoPath,'\" rel=\"photos\" class=\"tablelink fancybox\" title=\"',r.TakePhotoTime,' ',IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName),'#',IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber),' ',IF(u.UserID IS NOT NULL,$name_field,$archived_name_field),'\"><img src=\"/images/".$LAYOUT_SKIN."/icon_pic.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>') as Photo,
							CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',r.RecordID,'\" />') as Checkbox   
						FROM 
						CARD_STUDENT_NO_CARD_PHOTO as r 
						LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
						LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID AND au.RecordType='".USERTYPE_STUDENT."' 
						WHERE 1 $cond ";
				//debug_pr($sql);		
				// TABLE COLUMN
				$pos = 0;
				$column_list = array();
				$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['General']['RecordDate'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassName'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassNumber'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['StudentName'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['PhotoTakenTime'])."</th>\n";
				$column_list[] = "<th width='13%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['TerminalLocation'])."</th>\n";
				$column_list[] = "<th width='12%' style=\"font-weight:bold\">".$Lang['StudentAttendance']['Photo']."</th>\n";
				//$column_list[] = "<th width='12%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['Photo'])."</th>\n";
				$column_list[] = "<th width='1'>".$libdbtable->check("RecordID[]")."</th>\n";
				
				$column_array = array(22,22,22,0,22,22,0);
				$wrap_array = array_fill(0,count($field_array),0);
				
				return array($field_array,$sql,$column_list,$column_array,$wrap_array);
			}
			
			$sql = "SELECT 
						r.*,
						IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName) as StudentClassName,
						IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber) as StudentClassNumber,
						IF(u.UserID IS NOT NULL,u.EnglishName,au.EnglishName) as StudentEnglishName,
						IF(u.UserID IS NOT NULL,u.ChineseName,au.ChineseName) as StudentChineseName,
						IF(u.UserID IS NOT NULL,u.RecordStatus,-1) as RecordStatus 
					FROM CARD_STUDENT_NO_CARD_PHOTO as r 
					LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
					WHERE 1 $cond 
					ORDER BY r.RecordDate,StudentClassName,StudentClassNumber+0,r.TakePhotoTime ";
			$records = $this->returnResultSet($sql);
			//debug_pr($sql);
			if(isset($filterMap['StudentIDToRecord']) && $filterMap['StudentIDToRecord'])
			{
				$record_size = count($records);
				$studentIdToRecords = array();
				for($i=0;$i<$record_size;$i++)
				{
					if(!isset($studentIdToRecords[$records[$i]['StudentID']]))
					{
						$studentIdToRecords[$records[$i]['StudentID']] = array();
					}
					$studentIdToRecords[$records[$i]['StudentID']][] = $records[$i];
				}
				return $studentIdToRecords;
			}
			
			return $records;
		}

		function getBodyTemperatureRecords($filterMap)
		{
			global $Lang, $LAYOUT_SKIN, $file_path;
			
			$name_field = Get_Lang_Selection("u.ChineseName","u.EnglishName");
			$archived_name_field = Get_Lang_Selection("au.ChineseName","au.EnglishName");
			$cond = "";
			if(isset($filterMap['RecordID'])){
				$recordId = IntegerSafe($filterMap['RecordID']);
				if(is_array($recordId)){
					$cond .= " AND r.RecordID IN (".implode(",",$recordId).") ";
				}else{
					$cond .= " AND r.RecordID='$recordId' ";
				}
			}
			if(isset($filterMap['RecordDate']) && $filterMap['RecordDate']!=''){
				$cond .= " AND r.RecordDate = '".$filterMap['RecordDate']."' ";
			}
			if(isset($filterMap['StartDate']) && $filterMap['StartDate']!=''){
				$cond .= " AND r.RecordDate >= '".$filterMap['StartDate']."' ";
			}
			if(isset($filterMap['EndDate']) && $filterMap['EndDate']!=''){
				$cond .= " AND r.RecordDate <= '".$filterMap['EndDate']."' ";
			}
			
			if(isset($filterMap['StudentID']))
			{
				$studentId = IntegerSafe($filterMap['StudentID']);
				if(is_array($studentId)){
					$cond .= " AND r.StudentID IN (".implode(",",$studentId).") ";
				}else{
					$cond .= " AND r.StudentID='$studentId' ";
				}
			}

			if(isset($filterMap['TemperatureStatus']))
			{
				$TemperatureStatus = IntegerSafe($filterMap['TemperatureStatus']);
				if(is_array($TemperatureStatus)){
					$cond .= " AND r.TemperatureStatus IN (".implode(",",$TemperatureStatus).") ";
				}else{
					$cond .= " AND r.TemperatureStatus='$TemperatureStatus' ";
				}
			}

			if(isset($filterMap['UserID']))
			{
				$userId = IntegerSafe($filterMap['UserID']);
				if(is_array($userId)){
					$cond .= " AND b.UserID IN (".implode(",",$userId).") ";
				}else{
					$cond .= " AND b.UserID='$userId' ";
				}
			}

			if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
				$keyword = trim( $this->is_magic_quotes_active ? stripslashes($filterMap['Keyword']): $filterMap['Keyword'] );
				$keyword = $this->Get_Safe_Sql_Like_Query($keyword);
				$cond .= " AND ($name_field LIKE '%$keyword%' OR $archived_name_field LIKE '%$keyword%') ";
			}
			
			if($filterMap['GetDBQuery']){
				$libdbtable = $filterMap['libdbtable'];
				$field_array = array("r.RecordDate","StudentClassName","StudentClassNumber","StudentName","r.TemperatureValue","r.DateInput");
				$sql = "SELECT 
							r.RecordDate,
							IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName) as StudentClassName,
							IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber) as StudentClassNumber,
							IF(u.UserID IS NOT NULL,$name_field,CONCAT('<span class=\"red\">*</span>',$archived_name_field)) as StudentName,
							CONCAT('<span style=\"color:',IF(r.TemperatureStatus='1','red','green'),'\">',r.TemperatureValue,' &#8451;</span>') as TemperatureValue,
							r.DateInput,
							CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',r.RecordID,'\" />') as Checkbox   
						FROM 
						CARD_STUDENT_BODY_TEMPERATURE_RECORD as r 
						LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
						LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID AND au.RecordType='".USERTYPE_STUDENT."' 
						WHERE 1 $cond ";
				//debug_pr($sql);		
				// TABLE COLUMN
				$pos = 0;
				$column_list = array();
				$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['General']['RecordDate'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassName'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassNumber'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['StudentName'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['BodyTemperature'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['General']['DateInput'])."</th>\n";
				$column_list[] = "<th width='1'>".$libdbtable->check("RecordID[]")."</th>\n";
				
				$column_array = array(22,22,22,22,0,22);
				$wrap_array = array_fill(0,count($field_array),0);
				
				return array($field_array,$sql,$column_list,$column_array,$wrap_array);
			}

			if($filterMap['GetYKHQuery']){
				$libdbtable = $filterMap['libdbtable'];
				$field_array = array("RecordDateTime","StudentClassName","StudentClassNumber","StudentName","r.TemperatureValue");
				$sql = "SELECT 
							CONCAT(r.RecordDate, ' ', DATE_FORMAT(r.ModifiedDate, '%H:%i')) as RecordDateTime,
							IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName) as StudentClassName,
							IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber) as StudentClassNumber,
							IF(u.UserID IS NOT NULL,".getNameFieldByLang("u.").",CONCAT('<span class=\"red\">*</span>',".getNameFieldByLang("au.").")) as StudentName,
							CONCAT('<span style=\"color:',IF(r.TemperatureStatus='1','red','green'),'\">',r.TemperatureValue,' &#8451;</span>') as TemperatureValue				
						FROM 
						CARD_STUDENT_BODY_TEMPERATURE_RECORD as r 
						LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
						LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID AND au.RecordType='".USERTYPE_STUDENT."' 
						WHERE 1 $cond ";
				//debug_pr($sql);
				// TABLE COLUMN
				$pos = 0;
				$column_list = array();
				$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
				$column_list[] = "<th width='18%'>".$libdbtable->column_IP25($pos++, $Lang['General']['Date'].'/'.$Lang['General']['Time'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassName'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassNumber'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['StudentName'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['BodyTemperature'])."</th>\n";

				$column_array = array(22,22,22,22,0);
				$wrap_array = array_fill(0,count($field_array),0);

				return array($field_array,$sql,$column_list,$column_array,$wrap_array);
			}

			if($filterMap['GetYKHStudentQuery']){
				$month = $filterMap['Month'];
				if($month < 10) {
					$month = '0' . intval($month);
				}
				$cond = "";
				if(isset($filterMap['UserID'])) {
					$userId = IntegerSafe($filterMap['UserID']);
					if (is_array($userId)) {
						$cond .= " AND r.StudentID IN (" . implode(",", $userId) . ") ";
					} else {
						$cond .= " AND r.StudentID='$userId' ";
					}
				}

				if(isset($filterMap['Year']) && isset($filterMap['Month'])) {
					$start_day = $filterMap['Year']."-".$filterMap['Month']."-01";
					$end_day = $filterMap['Year']."-".$filterMap['Month']."-".date("t", strtotime($start_day));
					$cond .= " AND r.RecordDate>='$start_day' AND r.RecordDate<='$end_day'";
				}
				
				$ExpectedField = $this->Get_AM_Expected_Field("b.","c.","f.");
				$DayType = PROFILE_DAY_TYPE_AM;
				$AbsentExpectedReasonField = $this->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
				$LateExpectedReasonField = $this->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
				$OutingExpectedReasonField = $this->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);

				$libdbtable = $filterMap['libdbtable'];
				$daily_log_table = "CARD_STUDENT_DAILY_LOG_".$filterMap['Year']."_".$month;
				$field_array = array("RecordDate","TemperatureValue","TemperatureStatus","ModifiedDate","Status_txt");
				$ToDate = "DATE_FORMAT(CONCAT('".$filterMap['Year']."-".$filterMap['Month']."-',b.DayNumber),'%Y-%m-%d')";

				$sql = "SELECT 
 							RecordDate,
 							TemperatureValue,
 							TemperatureStatus,
 							ModifiedDate,
 							CASE Status
 							   WHEN 99 THEN '--'
							   WHEN ".CARD_STATUS_ABSENT." THEN IF(Reason IS NULL OR Reason='', '".$Lang['StudentAttendance']['Absent']."', CONCAT('".$Lang['StudentAttendance']['Absent']."','(',Reason,')'))
							   WHEN ".CARD_STATUS_LATE." THEN IF(Reason IS NULL OR Reason='', '".$Lang['StudentAttendance']['Late']."', CONCAT('".$Lang['StudentAttendance']['Late']."','(',Reason,')'))
							   WHEN ".CARD_STATUS_OUTING." THEN IF(Reason IS NULL OR Reason='', '".$Lang['StudentAttendance']['Outing']."', CONCAT('".$Lang['StudentAttendance']['Outing']."','(',Reason,')'))
							   WHEN ".CARD_STATUS_PRESENT." THEN '".$Lang['StudentAttendance']['Present']."'
							   ELSE ''
							END as Status_txt
 						FROM (
						SELECT 
						r.RecordDate,
						r.TemperatureValue,
						r.TemperatureStatus,
						DATE_FORMAT(r.ModifiedDate, '%H:%i:%s') as ModifiedDate,
						IF(b.PMStatus IS NOT NULL, b.PMStatus, IF(b.AMStatus IS NOT NULL, b.AMStatus, '99')) as Status,
						if (" . $ExpectedField . " = '" . CARD_STATUS_ABSENT . "',
							" . $AbsentExpectedReasonField . ",
							IF(" . $ExpectedField . " = '" . CARD_STATUS_LATE . "',
								" . $LateExpectedReasonField . ",
								IF(" . $ExpectedField . " = '" . CARD_STATUS_OUTING . "',
									" . $OutingExpectedReasonField . ",
									j.Reason))) as Reason
						FROM CARD_STUDENT_BODY_TEMPERATURE_RECORD r
						LEFT JOIN $daily_log_table b ON b.UserID=r.StudentID AND r.RecordDate=$ToDate
						LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (b.UserID=c.UserID AND c.RecordDate = " . $ToDate . " AND c.DayType = " . $DayType . ")								
						LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (b.UserID=d.StudentID AND d.RecordDate=" . $ToDate . " AND d.DayType=$DayType)
						LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (b.UserID=f.StudentID AND f.RecordDate=" . $ToDate . " AND f.DayPeriod=" . $DayType . ")
						LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
						ON j.StudentID = b.UserID 
							AND j.RecordDate = " . $ToDate . " 
							AND j.DayType = IF(b.PMStatus IS NOT NULL, ".PROFILE_DAY_TYPE_PM.", ".PROFILE_DAY_TYPE_AM.")
							AND 
							(
								(j.RecordType = IF(b.PMStatus IS NOT NULL, b.PMStatus, b.AMStatus) AND j.RecordType<>'" . PROFILE_TYPE_EARLY . "')
								OR  
								(j.RecordType = '" . PROFILE_TYPE_ABSENT . "' 
								AND IF(b.PMStatus IS NOT NULL, b.PMStatus, b.AMStatus) = '" . CARD_STATUS_OUTING . "' 
								)
							)
						
						WHERE 1 $cond
						) xx ORDER BY RecordDate ASC";


				//var_dump($sql);

				return $sql;
			}

			$sql = "SELECT 
						r.*,
						IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName) as StudentClassName,
						IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber) as StudentClassNumber,
						IF(u.UserID IS NOT NULL,u.EnglishName,au.EnglishName) as StudentEnglishName,
						IF(u.UserID IS NOT NULL,u.ChineseName,au.ChineseName) as StudentChineseName,
						IF(u.UserID IS NOT NULL,u.RecordStatus,-1) as RecordStatus 
					FROM CARD_STUDENT_BODY_TEMPERATURE_RECORD as r 
					LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
					WHERE 1 $cond 
					ORDER BY r.RecordDate,StudentClassName,StudentClassNumber+0,r.DateInput ";
			$records = $this->returnResultSet($sql);
			//debug_pr($sql);
			if(isset($filterMap['StudentIDToRecord']) && $filterMap['StudentIDToRecord'])
			{
				$record_size = count($records);
				$studentIdToRecords = array();
				for($i=0;$i<$record_size;$i++)
				{
					if(!isset($studentIdToRecords[$records[$i]['StudentID']]))
					{
						$studentIdToRecords[$records[$i]['StudentID']] = array();
					}
					$studentIdToRecords[$records[$i]['StudentID']][] = $records[$i];
				}
				return $studentIdToRecords;
			}
			
			return $records;
		}
		
		function getDailyLogRecords($filterMap)
		{
			$cond = "";
			if(!isset($filterMap['RecordDate']))
			{
				return array();
			}
			$ts = strtotime($filterMap['RecordDate']);
			$year = date("Y",$ts);
			$month = date("m",$ts);
			$day = date("j",$ts);
			$daily_log_table = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
			$sql = "SELECT * FROM $daily_log_table WHERE DayNumber='$day' ";
			if(isset($filterMap['StudentID'])){
				$student_id_ary = IntegerSafe($filterMap['StudentID']);
				if(is_array($student_id_ary))
				{
					$sql .= " AND UserID IN (".implode(",",$student_id_ary).") ";
				}else{
					$sql .= " AND UserID='$student_id_ary' ";
				}
			}
			$records = $this->returnResultSet($sql);
			if(isset($filterMap['StudentIDToRecord']) && $filterMap['StudentIDToRecord'])
			{
				$record_size = count($records);
				$studentIdToRecords = array();
				for($i=0;$i<$record_size;$i++)
				{
					$studentIdToRecords[$records[$i]['UserID']] = $records[$i];
				}
				return $studentIdToRecords;
			}
			
			return $records;
		}
		
		function getHKUSPHParentConsentRecords($filterMap)
		{
			global $Lang;
			
			$parent_namefield = getNameFieldByLang2("p.");
			$student_namefield = getNameFieldByLang2("s.");
			$class_namefield = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
			
			$cond = "";
			if(isset($filterMap['AcademicYearID'])){
				$id = IntegerSafe($filterMap['AcademicYearID']);
				if(is_array($id)){
					$cond .= " AND yc.AcademicYearID IN (".implode(",",$id).") ";
				}else{
					$cond .= " AND yc.AcademicYearID='$id' ";
				}
			}
			
			if(isset($filterMap['YearID'])){
				$id = IntegerSafe($filterMap['YearID']);
				if(is_array($id)){
					$cond .= " AND yr.YearID IN (".implode(",",$id).") ";
				}else{
					$cond .= " AND yr.YearID='$id' ";
				}
			}
			
			if(isset($filterMap['YearClassID'])){
				$id = IntegerSafe($filterMap['YearClassID']);
				if(is_array($id)){
					$cond .= " AND yc.YearClassID IN (".implode(",",$id).") ";
				}else{
					$cond .= " AND yc.YearClassID='$id' ";
				}
			}
			
			if(isset($filterMap['ParentID']))
			{
				$id = IntegerSafe($filterMap['ParentID']);
				if(is_array($id)){
					$cond .= " AND p.UserID IN (".implode(",",$id).") ";
				}else{
					$cond .= " AND p.UserID='$id' ";
				}
			}
			
			if(isset($filterMap['StudentID']))
			{
				$id = IntegerSafe($filterMap['StudentID']);
				if(is_array($id)){
					$cond .= " AND s.UserID IN (".implode(",",$id).") ";
				}else{
					$cond .= " AND s.UserID='$id' ";
				}
			}
			
			if(isset($filterMap['ConsentStatus']) && in_array($filterMap['ConsentStatus'], array('-1','0','1'))){
				if($filterMap['ConsentStatus'] == '-1'){
					$cond .= " AND a.IsAgreed IS NULL ";
				}else{
					$cond .= " AND a.IsAgreed='".$filterMap['ConsentStatus']."' ";
				}
			}
			
			if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
				$keyword = trim( $this->is_magic_quotes_active ? stripslashes($filterMap['Keyword']): $filterMap['Keyword'] );
				$keyword = $this->Get_Safe_Sql_Like_Query($keyword);
				$cond .= " AND ($parent_namefield LIKE '%$keyword%' OR $student_namefield LIKE '%$keyword%') ";
			}
			
			if($filterMap['GetDBQuery']){
				$libdbtable = $filterMap['libdbtable'];
				$field_array = array("ParentUserLogin","ParentName","ClassName","ycu.ClassNumber","StudentUserLogin","StudentName","ConsentStatus","a.SignDate");
				$sql = "SELECT 
							p.UserLogin as ParentUserLogin,
							$parent_namefield as ParentName,
							$class_namefield as ClassName,
							ycu.ClassNumber,
							s.UserLogin as StudentUserLogin,
							$student_namefield as StudentName,
							IF(a.AgreementID IS NULL,'<span style=\"color:blue\">".$Lang['StudentAttendance']['Unsigned']."</span>',IF(a.IsAgreed='1','<span style=\"color:green\">".$Lang['StudentAttendance']['Agreed']."</span>','<span style=\"color:red\">".$Lang['StudentAttendance']['Denied']."</span>')) as ConsentStatus,
							a.SignDate,
							CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',p.UserID,',',s.UserID,'\" />') as Checkbox  
						FROM INTRANET_USER as p 
						INNER JOIN INTRANET_PARENTRELATION as r ON r.ParentID=p.UserID 
						INNER JOIN INTRANET_USER as s ON s.UserID=r.StudentID AND s.RecordType='".USERTYPE_STUDENT."' 
						INNER JOIN YEAR_CLASS_USER as ycu ON s.UserID=ycu.UserID 
						INNER JOIN YEAR_CLASS as yc ON ycu.YearClassID=yc.YearClassID 
						INNER JOIN YEAR as yr ON yr.YearID=yc.YearID 
						LEFT JOIN HKUSPH_AGREEMENT as a ON a.ParentID=p.UserID AND a.StudentID=s.UserID AND a.Status='a' 
						WHERE p.RecordStatus='1' AND p.RecordType='".USERTYPE_PARENT."' $cond ";
				//debug_pr($sql);		
				// TABLE COLUMN
				$pos = 0;
				$column_list = array();
				$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
				$column_list[] = "<th width='11%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ParentLoginID'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ParentName'])."</th>\n";
				$column_list[] = "<th width='12%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassName'])."</th>\n";
				$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassNumber'])."</th>\n";
				$column_list[] = "<th width='11%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['StudentLoginID'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['StudentName'])."</th>\n";
				$column_list[] = "<th width='10%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ConsentStatus'])."</th>\n";
				$column_list[] = "<th width='15%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['SignDate'])."</th>\n";
				$column_list[] = "<th width='1'>".$libdbtable->check("RecordID[]")."</th>\n";
				
				$column_array = array(22,22,22,22,22,22,0,22);
				$wrap_array = array_fill(0,count($field_array),0);
				
				return array($field_array,$sql,$column_list,$column_array,$wrap_array);
			}
			
			$sql = "SELECT 
						p.UserID as ParentID,
						p.UserLogin as ParentUserLogin,
						$parent_namefield as ParentName,
						s.UserID as StudentID,
						s.UserLogin as StudentUserLogin,
						$student_namefield as StudentName,
						yc.AcademicYearID,
						yc.YearClassID,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						$class_namefield as ClassName,
						ycu.ClassNumber,
						a.AgreementID,
						a.IsAgreed,
						a.SignDate 
					FROM INTRANET_USER as p 
					INNER JOIN INTRANET_PARENTRELATION as r ON r.ParentID=p.UserID 
					INNER JOIN INTRANET_USER as s ON s.UserID=r.StudentID AND s.RecordType='".USERTYPE_STUDENT."' 
					INNER JOIN YEAR_CLASS_USER as ycu ON s.UserID=ycu.UserID 
					INNER JOIN YEAR_CLASS as yc ON ycu.YearClassID=yc.YearClassID 
					INNER JOIN YEAR as yr ON yr.YearID=yc.YearID 
					LEFT JOIN HKUSPH_AGREEMENT as a ON a.ParentID=p.UserID AND a.StudentID=s.UserID AND a.Status='a' 
					WHERE p.RecordStatus='1' AND p.RecordType='".USERTYPE_PARENT."' $cond ";
			if(isset($filterMap['CustomSort']) && $filterMap['CustomSort']){
				$field_array = array("ParentUserLogin","ParentName","ClassName","ycu.ClassNumber","StudentUserLogin","StudentName","a.IsAgreed","a.SignDate");
				$order = in_array($filterMap['SortOrder'],array(0,1))? $filterMap['SortOrder'] : 1;
				$field = in_array($filterMap['SortField'],array(0,1,2,3,4,5,6,7))? $filterMap['SortField'] : 2;
				$sql.=" ORDER BY ".$field_array[$field].($order==0?" DESC ":" ASC ");
				if($field == 2){
					$sql .= ",".$field_array[$field+1];
				}
			}else{
				$sql.=" ORDER BY yr.Sequence,yc.Sequence,ycu.ClassNumber ";
			}
			$records = $this->returnResultSet($sql);
			//debug_pr($sql);debug_pr($records);
			return $records;
		}
		
		function getHKUSPHParentConsentCsvHeaderMap($formatType=1)
		{
			if($formatType == 2){
				$header_map = array("Class"=>"ClassName","Class No."=>"ClassNumber","Student"=>"StudentName","Consent"=>"IsAgreed","Parent Name"=>"ParentName","Signer"=>"ParentName","Consent Time"=>"SignDate");
			}else{
				$header_map = array("Parent Login ID"=>"ParentUserLogin","Parent Name"=>"ParentName","Student Login ID"=>"StudentUserLogin","Class Name"=>"ClassName","Class Number"=>"ClassNumber","Student Name"=>"StudentName","Consent"=>"IsAgreed","Consent Time"=>"SignDate");
			}
			return $header_map;
		}
		
		/*** [BEGIN] $sys_custom['StudentAttendance']['HostelAttendance'] Customization for TWGHs Kwan Fong Kai Chi School [BEGIN] ***/
		function createTableHostelAttendanceDailyLog($year="",$month="")
        {
             $year=($year=="")?date("Y"):$year;
             $month=($month=="")?date("m"):$month;

             $table_name = sprintf("CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_%4d_%02d",$year,$month);
             $sql = "CREATE TABLE IF NOT EXISTS $table_name (
						RecordID int(11) NOT NULL PRIMARY KEY auto_increment, 
						UserID int(11) NOT NULL,
						DayNumber tinyint NOT NULL,
						InStatus varchar(2) DEFAULT NULL,
						InTime datetime DEFAULT NULL,
						OutStatus varchar(2) DEFAULT NULL,
						OutTime datetime DEFAULT NULL,
						Remark TEXT DEFAULT '',
						Waived int(11) default NULL,
						Reason mediumtext default NULL,
						DateInput datetime DEFAULT NULL,
						InputBy int(11) DEFAULT NULL,
						DateModified datetime DEFAULT NULL,
						ModifiedBy int(11) DEFAULT NULL,
						UNIQUE KEY UserDay(UserID,DayNumber) 
					)Engine=InnoDB DEFAULT CHARSET=utf8";
             $success = $this->db_db_query($sql);
             //debug_pr($sql);
             return $success;
        }
        
        function getHostelAttendanceGroupsConfirmedRecords($map)
        {
        	global $i_general_sysadmin, $Lang;
        	$conds = "";
        	if(isset($map['GroupID'])){
        		$group_id = IntegerSafe($map['GroupID']);
        		$conds .= " AND g.GroupID".(is_array($group_id)?" IN (".implode(",",$group_id).") ": "='$group_id' ");
        	}
        	if(isset($map['CategoryID'])){
        		$category_id = IntegerSafe($map['CategoryID']);
        		$conds .= " AND g.RecordType".(is_array($category_id)?" IN (".implode(",",$category_id).") ": "='$category_id' ");
        	}
        	if(isset($map['AcademicYearID'])){
        		$academic_year_id = IntegerSafe($map['AcademicYearID']);
        		$conds .= " AND g.AcademicYearID='$academic_year_id' ";
        	}
        	if(isset($map['RecordDate']) && $map['RecordDate']!=''){
        		$date_conds = " AND c.RecordDate='".$map['RecordDate']."' ";
        	}
        	if(isset($map['IsGroupStaff']) && $map['IsGroupStaff']){
        		$join_table = " INNER JOIN INTRANET_USERGROUP as ug ON ug.GroupID=g.GroupID ";
        		if(isset($map['GroupUserID'])){
        			$conds .= " AND ug.UserID ".(is_array($map['GroupUserID'])? " IN (".implode(",",IntegerSafe($map['GroupUserID'])).") " : "='".$map['GroupUserID']."' ");
        		}
        	}
        	$title_field = Get_Lang_Selection("g.TitleChinese","g.Title");
        	$sql = "SELECT 
						g.GroupID,$title_field as Title,g.RecordType as CategoryID,
						IF(c.ConfirmedUserID IS NULL,'-',IF(c.ConfirmedUserID <> -1, ".getNameFieldByLang("u.").", CONCAT('$i_general_sysadmin'))) as ConfirmedUser,
						c.ConfirmedUserID,
						c.ConfirmedDate,
						c.RecordID  
					FROM INTRANET_GROUP as g $join_table 
					LEFT JOIN CARD_STUDENT_HOSTEL_GROUP_CONFIRM as c ON c.GroupID=g.GroupID $date_conds 
					LEFT JOIN INTRANET_USER as u ON u.UserID=c.ConfirmedUserID 
					WHERE 1 $conds 
					ORDER BY $title_field ";
        	$records = $this->returnResultSet($sql);
        	if(isset($map['GroupIDToRecord']) && $map['GroupIDToRecord']){
        		$groupIdToRecords = array();
        		for($i=0;$i<count($records);$i++){
        			$groupIdToRecords[$records[$i]['GroupID']] = $records[$i];
        		}
        		return $groupIdToRecords;
        	}
        	
        	return $records;
        }
        
        function upsertHostelAttendanceGroupConfirmRecord($map)
        {
        	$fields = array('RecordID','GroupID','RecordDate');
			$user_id = $_SESSION['UserID'];
			
			if(count($map) == 0) return false;
			
			if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
			{
				$sql = "UPDATE CARD_STUDENT_HOSTEL_GROUP_CONFIRM SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'RecordID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$sql.= " ,ConfirmedDate=NOW(),ConfirmedUserID='$user_id' WHERE RecordID='".$map['RecordID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$keys .= ",ConfirmedDate,ConfirmedUserID";
				$values .= ",NOW(),'$user_id'";
				
				$sql = "INSERT INTO CARD_STUDENT_HOSTEL_GROUP_CONFIRM ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			
			return $success;	
        }
        
        function getHostelAttendanceRecords($year, $month, $map)
        {
        	global $sys_custom;
        	$conds = "";
        	$day_cond = "";
        	$year = sprintf("%4d",$year);
        	$month = sprintf("%02d",$month);
        	if(isset($map['RecordDate'])){
	        	$record_date = $map['RecordDate'];
	        	if(is_array($record_date)){
	        		$day_ary = array();
	        		for($i=0;$i<count($record_date);$i++){
	        			$day_ary[] = date("j", strtotime($record_date[$i]));
	        		}
	        		$day_cond .= " AND d.DayNumber IN ('".implode("','",$this->Get_Safe_Sql_Query($day_ary))."') ";
	        	}else{
	        		$ts = strtotime($record_date);
	        		$day = date("j",$ts);
	        		$day_cond .= " AND d.DayNumber='".$this->Get_Safe_Sql_Query($day)."' ";
	        	}
        	}
        	if(isset($map['UserID'])){
        		$user_id = IntegerSafe($map['UserID']);
        		$conds .= " AND ug.UserID".(is_array($user_id)?" IN (".implode(",",$user_id).") ": "='$user_id' ");
        	}
        	if(isset($map['UserStatus'])){
        		if(is_array($map['UserStatus'])){
        			$conds .= " AND u.RecordStatus IN (".implode(",",IntegerSafe($map['UserStatus'])).") ";
        		}else{
        			$conds .= " AND u.RecordStatus='".IntegerSafe($map['UserStatus'])."' ";
        		}
        	}
        	if(isset($map['StartDate'])){
        		$conds .= " AND DATE_FORMAT(CONCAT('".$year."-".$month."-',d.DayNumber),'%Y-%m-%d') >= '".$this->Get_Safe_Sql_Query($map['StartDate'])."' ";
        	}
        	if(isset($map['EndDate'])){
        		$conds .= " AND DATE_FORMAT(CONCAT('".$year."-".$month."-',d.DayNumber),'%Y-%m-%d') <= '".$this->Get_Safe_Sql_Query($map['EndDate'])."' ";
        	}
        	if(isset($map['GroupID'])){
        		$group_id = IntegerSafe($map['GroupID']);
        		$conds .= " AND g.GroupID".(is_array($group_id)?" IN (".implode(",",$group_id).") ": "='$group_id' ");
        	}
        	if(isset($map['CategoryID'])){
        		$category_id = IntegerSafe($map['CategoryID']);
        		$conds .= " AND g.RecordType".(is_array($category_id)?" IN (".implode(",",$category_id).") ": "='$category_id' ");
        	}
        	if(isset($map['AcademicYearID'])){
        		$academic_year_id = IntegerSafe($map['AcademicYearID']);
        		$conds .= " AND g.AcademicYearID='$academic_year_id' ";
        	}
        	if(isset($map['InStatus'])){
        		if(is_array($map['InStatus'])){
        			$conds .= " AND d.InStatus IN ('".implode("','",$this->Get_Safe_Sql_Query($map['InStatus']))."') ";
        		}else{
        			$conds .= " AND d.InStatus='".$this->Get_Safe_Sql_Query($map['InStatus'])."' ";
        		}
        	}
        	if(isset($map['OutStatus'])){
        		if(is_array($map['OutStatus'])){
        			$conds .= " AND (d.OutStatus IN ('".implode("','",$this->Get_Safe_Sql_Query($map['OutStatus']))."') ";
        			if(in_array('',$map['OutStatus'])){
        				$conds .= " OR d.OutStatus IS NULL ";
        			}
        			$conds .= ") ";
        		}else{
        			$conds .= " AND d.OutStatus ".($map['OutStatus']==''?" IS NULL ":"='".$this->Get_Safe_Sql_Query($map['OutStatus'])."'")." ";
        		}
        	}
        	if(isset($map['StayType'])){
        		$conds .= " AND d.InStatus='".CARD_STATUS_PRESENT."' ";
        		if($map['StayType'] == '1'){
        			$conds .= " AND (d.OutStatus IS NULL OR d.OutStatus='' OR d.OutStatus='".CARD_STATUS_PRESENT."') ";
        		}else if($map['StayType'] == '0'){
        			$conds .= " AND (d.OutStatus IS NOT NULL AND d.OutStatus='".PROFILE_TYPE_EARLY."') ";
        		}
        	}
        	$daily_log_table = sprintf("CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_%4d_%02d",$year,$month);
        	
        	$more_fields = '';
        	if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
				$more_fields .= ",d.Waived";
				$more_fields .= ",d.Reason";
			}

        	$title_field = Get_Lang_Selection("g.TitleChinese","g.Title");
        	$name_field = getNameFieldWithClassNumberByLang("u.");
        	$confirm_username_field = getNameFieldByLang2("u2.");
        	$sql = "SELECT 
						u.UserID,
						$name_field as StudentName,
                        u.ClassName,u.ClassNumber,
						g.GroupID,
						$title_field as GroupName,
						d.RecordID,
						d.InStatus,
						d.InTime,
						d.OutStatus,
						d.OutTime,
						d.Remark,
						d.DateModified,
						d.ModifiedBy,
						$confirm_username_field as ConfirmedUser,
						DATE_FORMAT(CONCAT('".$year."-".$month."-',d.DayNumber),'%Y-%m-%d') as RecordDate 
						$more_fields
					FROM INTRANET_USERGROUP as ug 
					INNER JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID 
					INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
					LEFT JOIN $daily_log_table as d ON d.UserID=u.UserID $day_cond 
					LEFT JOIN INTRANET_USER as u2 ON u2.UserID=d.ModifiedBy 
					WHERE u.RecordType='".USERTYPE_STUDENT."' $conds 
					ORDER BY RecordDate,g.Title,u.ClassName,u.ClassNumber+0 ";
			$records = $this->returnResultSet($sql);

			if(isset($map['UserIDToRecord']) && $map['UserIDToRecord']){
				$userIdToRecords = array();
				for($i=0;$i<count($records);$i++){
					$userIdToRecords[$records[$i]['UserID']] = $records[$i];
				}
				return $userIdToRecords;
			}
			
			return $records;
        }
        
        function upsertHostelAttendanceRecord($year,$month,$map)
        {
			global $sys_custom;
        	$fields = array('RecordID','UserID','DayNumber','InStatus','InTime','OutStatus','OutTime','Remark');
			if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
				$fields[] = 'Waived';
				$fields[] = 'Reason';
			}
			$user_id = $_SESSION['UserID'];
			$daily_log_table = sprintf("CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_%4d_%02d",$year,$month);
			if(count($map) == 0) return false;
			
			if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
			{
				$sql = "UPDATE $daily_log_table SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'RecordID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$sql.= " ,DateModified=NOW(),ModifiedBy='$user_id' WHERE RecordID='".$map['RecordID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'RecordID') continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
				$values .= ",NOW(),NOW(),'$user_id','$user_id'";
				
				$sql = "INSERT INTO $daily_log_table ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			//debug_pr($sql);
			return $success;	
        }
        
        function getHostelAttendanceAdminUsers($filterMap=array())
        {
        	$name_field = getNameFieldByLang2();
        	if(isset($filterMap['OnlyUserID']) && $filterMap['OnlyUserID']){
        		$only_user_id = true;
        	}
        	$sql = "SELECT UserID ".($only_user_id?"":",$name_field as UserName ")." FROM INTRANET_USER WHERE UserID IN ('".(implode("','",(array)$this->HostelAttendanceAdmin))."')";
        	$users = $only_user_id? $this->returnVector($sql) : $this->returnArray($sql);
        	return $users;
        }
        
        /*** [END] $sys_custom['StudentAttendance']['HostelAttendance'] Customization for TWGHs Kwan Fong Kai Chi School [END] ***/
        
        function getAttendanceGroupRecords($filterMap=array())
        {
        	$conds = "";
        	$academic_year_id = Get_Current_Academic_Year_ID();
        	if(isset($filterMap['AcademicYearID']) && $filterMap['AcademicYearID']!=''){
        		$academic_year_id = IntegerSafe($filterMap['AcademicYearID']);
        	}
        	
        	if(isset($filterMap['GroupID'])){
        		if(is_array($filterMap['GroupID'])){
        			$conds .= " AND g.GroupID IN (".implode(",",IntegerSafe($filterMap['GroupID'])).") ";
        		}else{
        			$conds .= " AND g.GroupID='".IntegerSafe($filterMap['GroupID'])."' ";
        		}
        	}
        	
        	$sql = "SELECT 
						g.* 
					FROM INTRANET_GROUP as g 
					LEFT JOIN YEAR_CLASS as yc ON g.GroupID = yc.GroupID 
					WHERE g.AcademicYearID='$academic_year_id' AND g.RecordType=3 AND yc.GroupID IS NULL $conds 
					ORDER BY g.Title ";
			$records = $this->returnResultSet($sql);
			//debug_pr($sql);
			return $records;
        }
        
        function getAttendanceGroupAdminUsers($filterMap=array())
        {
        	$conds = "";
        	$name_field = getNameFieldByLang2("u.");
        	if(isset($filterMap['GroupID'])){
        		if(is_array($filterMap['GroupID'])){
        			$conds .= " AND ga.GroupID IN (".implode(",",IntegerSafe($filterMap['GroupID'])).") ";
        		}else{
        			$conds .= " AND ga.GroupID='".IntegerSafe($filterMap['GroupID'])."' ";
        		}
        	}
        	if(isset($filterMap['UserID'])){
        		if(is_array($filterMap['UserID'])){
        			$conds .= " AND ga.UserID IN (".implode(",",IntegerSafe($filterMap['UserID'])).") ";
        		}else{
        			$conds .= " AND ga.UserID='".IntegerSafe($filterMap['UserID'])."' ";
        		}
        	}
        	
        	$sql = "SELECT 
						ga.*,
						$name_field as UserName,
						u.RecordType as UserType,
						u.RecordStatus as UserStatus 
					FROM CARD_STUDENT_ATTENDANCE_GROUP_ADMIN as ga 
					INNER JOIN INTRANET_USER as u ON u.UserID=ga.UserID 
					WHERE 1 $conds 
					ORDER BY ga.GroupID,UserName ";
        	$records = $this->returnResultSet($sql);
        	if(isset($filterMap['GroupIDToUsers']) && $filterMap['GroupIDToUsers']){
        		$groupIdToUsers = array();
        		$record_size = count($records);
        		for($i=0;$i<$record_size;$i++){
        			if(!isset($groupIdToUsers[$records[$i]['GroupID']])){
        				$groupIdToUsers[$records[$i]['GroupID']] = array();
        			}
        			$groupIdToUsers[$records[$i]['GroupID']][] = $records[$i];
        		}
        		return $groupIdToUsers;
        	}
        	
        	return $records;
        }
        
        function getStudentLateAbsentCountingRecords($filterMap)
        {
        	global $Lang;
        	
        	$conds = "";
        	if(isset($filterMap['AcademicYearID']) && $filterMap['AcademicYearID']!=''){
        		$academic_year_id = IntegerSafe($filterMap['AcademicYearID']);
        	}else{
        		$academic_year_id = Get_Current_Academic_Year_ID();
        	}
        	
        	if(isset($filterMap['YearTermID']) && $filterMap['YearTermID']!=''){
        		$year_term_id = IntegerSafe( $filterMap['YearTermID'] );
        		$conds .= " AND ayt.YearTermID='$year_term_id' ";
        	}
        	
        	$sql = "SELECT MIN(ayt.TermStart) as StartDate,MAX(ayt.TermEnd) as EndDate 
					FROM ACADEMIC_YEAR as ay 
					INNER JOIN ACADEMIC_YEAR_TERM as ayt ON ayt.AcademicYearID=ay.AcademicYearID 
					WHERE ay.AcademicYearID='$academic_year_id' $conds  
					GROUP BY ay.AcademicYearID ";
        	$date_range = $this->returnResultSet($sql);
        	
        	$start_date = $date_range[0]['StartDate'];
        	$end_date = $date_range[0]['EndDate'];
        	
        	$conds = "";
        	$late_time = $this->HKUGAC_LateTime;
        	if($late_time == ''){
        		$late_time = '00:00:00';
        	}else{
        		$late_time .= ':00';
        	}
        	$late_count = 0;
        	$absent_count = 0;
        	$and_or = 'OR';
        	if(isset($filterMap['LateCount']) && $filterMap['LateCount']>0){
        		$late_count = $filterMap['LateCount'];
        	}
        	if(isset($filterMap['AbsentCount']) && $filterMap['AbsentCount']>0){
        		$absent_count = $filterMap['AbsentCount'];
        	}
        	if(isset($filterMap['AndOr']) && in_array($filterMap['AndOr'],array('OR','AND'))){
        		$and_or = $filterMap['AndOr'];
        	}
        	
        	if(isset($filterMap['YearClassID']) && $filterMap['YearClassID']!=''){
        		$year_class_id = IntegerSafe($filterMap['YearClassID']);
        		$conds .= " AND yc.YearClassID ".(is_array($year_class_id)?" IN (".implode(",",$year_class_id).") " : "='$year_class_id' ");
        	}
        	if(isset($filterMap['StartDate']) && $filterMap['StartDate']!='' && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$filterMap['StartDate']) 
        		&& isset($filterMap['EndDate']) && $filterMap['EndDate']!='' && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$filterMap['EndDate'])){
        		$r1_date_conds = " AND r1.RecordDate BETWEEN '".$filterMap['StartDate']."' AND '".$filterMap['EndDate']."' ";
				$r2_date_conds = " AND r2.RecordDate BETWEEN '".$filterMap['StartDate']."' AND '".$filterMap['EndDate']."' ";
        	}else{
        		$r1_date_conds = " AND (r1.RecordDate BETWEEN '$start_date' AND '$end_date') ";
        		$r2_date_conds = " AND (r2.RecordDate BETWEEN '$start_date' AND '$end_date') ";
        	}
        	if(isset($filterMap['StudentID']) && $filterMap['StudentID']){
        		$student_id = IntegerSafe($filterMap['StudentID']);
        		if(is_array($student_id)){
        			$conds .= " AND u.UserID IN (".implode(",",$student_id).") ";
        		}else{
        			$conds .= " AND u.UserID='$student_id' ";
        		}
        	}
        	
        	if(isset($filterMap['Keyword']) && trim($filterMap['Keyword']) != ''){
				$keyword = trim( $this->is_magic_quotes_active ? stripslashes($filterMap['Keyword']): $filterMap['Keyword'] );
				$keyword = $this->Get_Safe_Sql_Like_Query($keyword);
				$conds .= " AND (u.EnglishName LIKE '%$keyword%' OR u.ChineseName LIKE '%$keyword%') ";
			}
        	
        	$studentname_field = getNameFieldByLang2("u.");
        	$classname_field = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
        	
        	if(isset($filterMap['GetDBQuery']) && $filterMap['GetDBQuery']){
        		$libdbtable = $filterMap['libdbtable'];
				$field_array = array("ClassName","ycu.ClassNumber","StudentName","LateCount","AbsentCount");
				$sql = "SELECT 
							$classname_field as ClassName,
							ycu.ClassNumber,
							$studentname_field as StudentName,
							IF(COUNT(DISTINCT r2.RecordID)>0,CONCAT('<a href=\"javascript:void(0);\" onclick=\"displayInfoLayer(this,\'InfoLayer\',',u.UserID,',2)\">',COUNT(DISTINCT r2.RecordID),'</a>'),COUNT(DISTINCT r2.RecordID)) as LateCount,
							IF(COUNT(DISTINCT r1.RecordID)>0,CONCAT('<a href=\"javascript:void(0);\" onclick=\"displayInfoLayer(this,\'InfoLayer\',',u.UserID,',1)\">',COUNT(DISTINCT r1.RecordID),'</a>'),COUNT(DISTINCT r1.RecordID)) as AbsentCount,
							CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"',u.UserID,'\" />') as Checkbox   
						FROM INTRANET_USER as u 
						INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=u.UserID 
						INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID 
						LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r2 ON r2.StudentID=u.UserID $r2_date_conds AND r2.DayType=2 AND r2.RecordType=2 AND (r2.RecordStatus<>1 OR r2.RecordStatus IS NULL) AND r2.LateTime IS NOT NULL AND r2.LateTime>='$late_time'
						LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r1 ON r1.StudentID=u.UserID $r1_date_conds AND r1.DayType=2 AND r1.RecordType=1 AND (r1.RecordStatus<>1 OR r1.RecordStatus IS NULL) AND r1.ProfileRecordID IS NOT NULL AND r1.ProfileRecordID>0 
						WHERE yc.AcademicYearID='$academic_year_id' $conds 
						GROUP BY u.UserID HAVING COUNT(DISTINCT r2.RecordID) >= $late_count $and_or COUNT(DISTINCT r1.RecordID) >= $absent_count ";
				//debug_pr($sql);		
				// TABLE COLUMN
				$pos = 0;
				$column_list = array();
				$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassName'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['ClassNumber'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['StudentName'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['NumberOfLate'])."</th>\n";
				$column_list[] = "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['StudentAttendance']['NumberOfAbsence'])."</th>\n";
				$column_list[] = "<th width='1'>".$libdbtable->check("StudentID[]")."</th>\n";
				
				$column_array = array(22,22,22,0,0);
				$wrap_array = array_fill(0,count($field_array),0);
				
				return array($field_array,$sql,$column_list,$column_array,$wrap_array);
        	}
        	
        	$get_detail_records = isset($filterMap['GetDetailRecords']) && $filterMap['GetDetailRecords'] && in_array($filterMap['DetailRecordType'],array(1,2));
        	
        	$sql = "SELECT 
						u.UserID,
						$studentname_field as StudentName,
						$classname_field as ClassName,
						ycu.ClassNumber,
						yc.YearClassID,
						yc.AcademicYearID,";
			if($get_detail_records){
				$alias = "r".$filterMap['DetailRecordType'];
				$sql .= " $alias.RecordDate,$alias.DayType,$alias.RecordType,$alias.Reason,$alias.Remark,$alias.OfficeRemark,$alias.LateTime,$alias.DateInput,$alias.DateModified ";
			}else{
				$sql.=" COUNT(DISTINCT r2.RecordID) as LateCount,
						COUNT(DISTINCT r1.RecordID) as AbsentCount ";
			} 
			$sql .=	" FROM INTRANET_USER as u 
					INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=u.UserID 
					INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID ";
			if($get_detail_records){
				if($filterMap['DetailRecordType'] == 2){
					$sql.=" INNER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r2 ON r2.StudentID=u.UserID $r2_date_conds AND r2.DayType=2 AND r2.RecordType=2 AND (r2.RecordStatus<>1 OR r2.RecordStatus IS NULL) AND r2.LateTime IS NOT NULL AND r2.LateTime>='$late_time' ";
				}else{
					$sql.=" INNER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r1 ON r1.StudentID=u.UserID $r1_date_conds AND r1.DayType=2 AND r1.RecordType=1 AND (r1.RecordStatus<>1 OR r1.RecordStatus IS NULL) AND r1.ProfileRecordID IS NOT NULL AND r1.ProfileRecordID>0  ";
				}
			}else{	
			$sql.=" LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r2 ON r2.StudentID=u.UserID $r2_date_conds AND r2.DayType=2 AND r2.RecordType=2 AND (r2.RecordStatus<>1 OR r2.RecordStatus IS NULL) AND r2.LateTime IS NOT NULL AND r2.LateTime>='$late_time'
					LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as r1 ON r1.StudentID=u.UserID $r1_date_conds AND r1.DayType=2 AND r1.RecordType=1 AND (r1.RecordStatus<>1 OR r1.RecordStatus IS NULL) AND r1.ProfileRecordID IS NOT NULL AND r1.ProfileRecordID>0  ";
			}
			$sql.=" WHERE yc.AcademicYearID='$academic_year_id' $conds ";
			if(!$get_detail_records){
				$sql.=" GROUP BY u.UserID HAVING COUNT(DISTINCT r2.RecordID) >= $late_count $and_or COUNT(DISTINCT r1.RecordID) >= $absent_count ";
			} 
			$sql.=" ORDER BY $classname_field,ycu.ClassNumber ";
			if($get_detail_records){
				$alias = "r".$filterMap['DetailRecordType'];
				$sql.=",".$alias.".RecordDate ";
			}
        	$records = $this->returnResultSet($sql);
        	if(isset($filterMap['StudentIDToRecords']) && $filterMap['StudentIDToRecords']){
        		$studentIdToRecords = array();
        		for($i=0;$i<count($records);$i++){
        			if(!isset($studentIdToRecords[$records[$i]['UserID']])){
        				$studentIdToRecords[$records[$i]['UserID']] = array();
        			}
        			$studentIdToRecords[$records[$i]['UserID']][] = $records[$i];
        		}
        		return $studentIdToRecords;
        	}
        	
        	return $records;
        }
        
        
        function calculateContinuousAbsentStudent($student_id, $start_date, $end_date, $absent_day_count)
		{	
			$attendance_mode = $this->attendance_mode;
			$is_whole_day = $attendance_mode == '2' || $attendance_mode == '3';
			$is_half_day_am = $attendance_mode == '0';
			$is_half_day_pm = $attendance_mode == '1';
			
			$absent_dates = array();
			$school_dates = array();
			$school_dates_prev_next = array();
			$absent_dates_stack = array();
			$absent_ranges = array();
			$return_data = array('UserID'=>$student_id,'ClassName'=>'','ClassNumber'=>'','StudentName'=>'','AbsentDates'=>array());
			
			$start_ts = strtotime($start_date);
			$end_ts = strtotime($end_date);
			
			$current_ts = $start_ts;
			$current_y = date("Y",$current_ts);
			$current_m = date("m",$current_ts);
			$current_d = date("d",$current_ts);
			
			$name_field = getNameFieldByLang("u.");
			
			for($current_ts=$start_ts;$current_ts<=$end_ts;$current_ts = mktime(0,0,0,intval($current_m)+1,1,intval($current_y)))
			{
				$current_y = date("Y",$current_ts);
				$current_m = date("m",$current_ts);
				$current_d = date("d",$current_ts);
				$year_month = $current_y."-".$current_m;
				
				$dailylog_table = "CARD_STUDENT_DAILY_LOG_".$current_y."_".$current_m;
				$date_range_cond = " AND (DATE_FORMAT(CONCAT('$current_y-$current_m-',d.DayNumber),'%Y-%m-%d') BETWEEN '".$this->Get_Safe_Sql_Query($start_date)."' AND '".$this->Get_Safe_Sql_Query($end_date)."') ";
				
				$sql = "SELECT 
							u.ClassName,
							u.ClassNumber,
							$name_field as StudentName,
							DATE_FORMAT(CONCAT('$current_y-$current_m-',d.DayNumber),'%Y-%m-%d') as RecordDate,
							d.AMStatus,
							d.PMStatus,
							am_reason.RecordStatus as AM_waive,
							pm_reason.RecordStatus as PM_waive
						FROM INTRANET_USER as u  
						INNER JOIN $dailylog_table as d ON d.UserID=u.UserID 
						LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as am_reason ON am_reason.RecordDate=DATE_FORMAT(CONCAT('$current_y-$current_m-',d.DayNumber),'%Y-%m-%d') AND am_reason.StudentID=d.UserID AND am_reason.DayType=".PROFILE_DAY_TYPE_AM."
						LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as pm_reason ON pm_reason.RecordDate=DATE_FORMAT(CONCAT('$current_y-$current_m-',d.DayNumber),'%Y-%m-%d') AND pm_reason.StudentID=d.UserID AND pm_reason.DayType=".PROFILE_DAY_TYPE_PM."
						WHERE u.UserID='".$this->Get_Safe_Sql_Query($student_id)."' $date_range_cond 
						ORDER BY RecordDate";

				$records = $this->returnResultSet($sql);
				$record_count = count($records);
				
				for($i=0;$i<$record_count;$i++){
					$return_data['ClassName'] = $records[$i]['ClassName'];
					$return_data['ClassNumber'] = $records[$i]['ClassNumber'];
					$return_data['StudentName'] = $records[$i]['StudentName'];
					$record_date = $records[$i]['RecordDate'];
					$am_status = $records[$i]['AMStatus'];
					$pm_status = $records[$i]['PMStatus'];

					if($am_status == CARD_STATUS_ABSENT && $records[$i]['AM_waive'] == 1) {
						$am_status = '';
					}

					if($pm_status == CARD_STATUS_ABSENT && $records[$i]['PM_waive'] == 1) {
						$pm_status = '';
					}

					$school_dates[] = $record_date;
					if(($is_whole_day && $am_status == CARD_STATUS_ABSENT && $pm_status == CARD_STATUS_ABSENT) 
						|| ($is_half_day_am && $am_status == CARD_STATUS_ABSENT) 
						|| ($is_half_day_pm && $pm_status == CARD_STUDENT_ABSENT)
					){
						$absent_dates[] = $record_date;
					}
					
					
				}
				
			}
			
			$school_date_size = count($school_dates);
			for($i=0;$i<$school_date_size;$i++){
				$school_dates_prev_next[$school_dates[$i]] = array('prev'=>'','next'=>'');
				if($i>0){
					$school_dates_prev_next[$school_dates[$i]]['prev'] = $school_dates[$i-1];
				}
				if($i<$school_date_size-1){
					$school_dates_prev_next[$school_dates[$i]]['next'] = $school_dates[$i+1];
				}
			}
			
			$absent_date_size = count($absent_dates);
			for($i=0;$i<$absent_date_size;$i++){
				$absent_date = $absent_dates[$i];
				$prev_date = $school_dates_prev_next[$absent_date]['prev'];
				$next_date = $school_dates_prev_next[$absent_date]['next'];
				$prev_absent_date = '';
				$next_absent_date = '';
				if($i > 0){
					$prev_absent_date = $absent_dates[$i-1];
				}
				if($i<$absent_date_size-1){
					$next_absent_date = $absent_dates[$i+1];
				}
				if($prev_absent_date == $prev_date || count($absent_dates_stack)==0){
					$absent_dates_stack[] = $absent_date;
				}
				
				if(count($absent_dates_stack)>=$absent_day_count && ($next_absent_date != $next_date || $i==$absent_date_size-1)){
					$absent_ranges[] = $absent_dates_stack;
					$absent_dates_stack = array(); // clear the stack and count new range
				}
				if($next_absent_date != $next_date){
					$absent_dates_stack = array(); // clear the stack and count new range
				}
			}
			$return_data['AbsentDates'] = $absent_ranges;
			
			return $return_data;
		}
  
		// return list of students that have applied leave on specific day section
		function checkStudentApplyLeave($studentIDAry,$date,$dayPeriod)
		{
		    $sql = "SELECT
						StudentID
					FROM CARD_STUDENT_PRESET_LEAVE
					WHERE StudentID IN (".implode(",",(array)$studentIDAry).")
						AND RecordDate = '".$date."' AND DayPeriod = '".$dayPeriod."'
					ORDER BY StudentID";
		    $presetLeaveAry = $this->returnResultSet($sql);
		    $presetLeaveStudentIDAry = BuildMultiKeyAssoc($presetLeaveAry, 'StudentID', array('StudentID'), $SingleValue=1);
		    
		    $ret = array();
		    if (count($presetLeaveAry)) {
		        $sql = "SELECT RecordID, StudentID
                      FROM
                            CARD_STUDENT_APPLY_LEAVE_RECORD 
                      WHERE
                            '".$date."'>=StartDate AND '".$date."'<=EndDate
                            AND StudentID IN (".implode(",",(array)$presetLeaveStudentIDAry).")
                            AND IsDeleted=0
                      ORDER BY StudentID, RecordID";
		        $appliedLeaveStudentAry = $this->returnResultSet($sql);
		        
		        $ret = BuildMultiKeyAssoc($appliedLeaveStudentAry, 'StudentID', array('RecordID'), $SingleValue=1);
		    }
		        
		    return $ret;
		}

		function getStudentApplyLeaveRemark($studentIDAry,$date,$dayPeriod)
		{
		    $sql = "SELECT
						StudentID, Remark
					FROM CARD_STUDENT_PRESET_LEAVE
					WHERE StudentID IN (".implode(",",(array)$studentIDAry).")
						AND RecordDate = '".$date."' AND DayPeriod = '".$dayPeriod."'
					ORDER BY StudentID";
		    $presetLeaveAry = $this->returnResultSet($sql);
		    $appliedLeaveRemarkAry = BuildMultiKeyAssoc($presetLeaveAry, 'StudentID');
		    
		    return $appliedLeaveRemarkAry;
		}

		function InsertSubmittedAbsentLateRecord($StudentID, $RecordDate, $DayPeriod, $EnrolGroupID, $EnrolEventID, $TargetStatus, $SessionFrom, $SessionTo)
		{
			if($SessionFrom == '') {
				$SessionFrom = 'NULL';
			} else {
				$SessionFrom = "'$SessionFrom'";
			}

			if($SessionTo == '') {
				$SessionTo = 'NULL';
			} else {
				$SessionTo = "'$SessionTo'";
			}

			$sql = "INSERT INTO STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD (StudentID, RecordDate, DayPeriod, EnrolGroupID, EnrolEventID, TargetStatus, SessionFrom, SessionTo, DateInput)
			  VALUES ('$StudentID','$RecordDate', '$DayPeriod', '$EnrolGroupID', '$EnrolEventID', '$TargetStatus', $SessionFrom,$SessionTo, now())";
			return $this->db_db_query($sql);
		}

		function getStudentLastHostelAttendanceRecords($year, $month, $map)
		{
			global $sys_custom;
			$conds = "";
			$day_cond = "";
			$year = sprintf("%4d",$year);
			$month = sprintf("%02d",$month);
			if(isset($map['RecordDate'])){
				$record_date = $map['RecordDate'];
				if(is_array($record_date)){
					$day_ary = array();
					for($i=0;$i<count($record_date);$i++){
						$day_ary[] = date("j", strtotime($record_date[$i]));
					}
					$day_cond .= " AND d.DayNumber IN ('".implode("','",$this->Get_Safe_Sql_Query($day_ary))."') ";
				}else{
					$ts = strtotime($record_date);
					$day = date("j",$ts);
					$day_cond .= " AND d.DayNumber='".$this->Get_Safe_Sql_Query($day)."' ";
				}
			}
			if(isset($map['UserID'])){
				$user_id = IntegerSafe($map['UserID']);
				$conds .= " AND ug.UserID".(is_array($user_id)?" IN (".implode(",",$user_id).") ": "='$user_id' ");
			}
			if(isset($map['UserStatus'])){
				if(is_array($map['UserStatus'])){
					$conds .= " AND u.RecordStatus IN (".implode(",",IntegerSafe($map['UserStatus'])).") ";
				}else{
					$conds .= " AND u.RecordStatus='".IntegerSafe($map['UserStatus'])."' ";
				}
			}
			if(isset($map['StartDate'])){
				$conds .= " AND (DATE_FORMAT(CONCAT('".$year."-".$month."-',d.DayNumber),'%Y-%m-%d') >= '".$this->Get_Safe_Sql_Query($map['StartDate'])."' OR d.DayNumber IS NULL)";
			}
			if(isset($map['EndDate'])){
				$conds .= " AND (DATE_FORMAT(CONCAT('".$year."-".$month."-',d.DayNumber),'%Y-%m-%d') <= '".$this->Get_Safe_Sql_Query($map['EndDate'])."' OR d.DayNumber IS NULL)";
			}
			if(isset($map['GroupID'])){
				$group_id = IntegerSafe($map['GroupID']);
				$conds .= " AND g.GroupID".(is_array($group_id)?" IN (".implode(",",$group_id).") ": "='$group_id' ");
			}
			if(isset($map['CategoryID'])){
				$category_id = IntegerSafe($map['CategoryID']);
				$conds .= " AND g.RecordType".(is_array($category_id)?" IN (".implode(",",$category_id).") ": "='$category_id' ");
			}
			if(isset($map['AcademicYearID'])){
				$academic_year_id = IntegerSafe($map['AcademicYearID']);
				$conds .= " AND g.AcademicYearID='$academic_year_id' ";
			}
			if(isset($map['InStatus'])){
				if(is_array($map['InStatus'])){
					$conds .= " AND d.InStatus IN ('".implode("','",$this->Get_Safe_Sql_Query($map['InStatus']))."') ";
				}else{
					$conds .= " AND d.InStatus='".$this->Get_Safe_Sql_Query($map['InStatus'])."' ";
				}
			}
			if(isset($map['OutStatus'])){
				if(is_array($map['OutStatus'])){
					$conds .= " AND (d.OutStatus IN ('".implode("','",$this->Get_Safe_Sql_Query($map['OutStatus']))."') ";
					if(in_array('',$map['OutStatus'])){
						$conds .= " OR d.OutStatus IS NULL ";
					}
					$conds .= ") ";
				}else{
					$conds .= " AND d.OutStatus ".($map['OutStatus']==''?" IS NULL ":"='".$this->Get_Safe_Sql_Query($map['OutStatus'])."'")." ";
				}
			}
			if(isset($map['StayType'])){
				$conds .= " AND d.InStatus='".CARD_STATUS_PRESENT."' ";
				if($map['StayType'] == '1'){
					$conds .= " AND (d.OutStatus IS NULL OR d.OutStatus='' OR d.OutStatus='".CARD_STATUS_PRESENT."') ";
				}else if($map['StayType'] == '0'){
					$conds .= " AND (d.OutStatus IS NOT NULL AND d.OutStatus='".PROFILE_TYPE_EARLY."') ";
				}
			}
			$daily_log_table = sprintf("CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_%4d_%02d",$year,$month);

			$more_fields = '';
			if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
				$more_fields .= ",d.Waived";
				$more_fields .= ",d.Reason";
			}

			$title_field = Get_Lang_Selection("g.TitleChinese","g.Title");
			$name_field = getNameFieldWithClassNumberByLang("u.");
			$confirm_username_field = getNameFieldByLang2("u2.");
			$sql = "SELECT 
						u.UserID,
						$name_field as StudentName,
                        u.ClassName,u.ClassNumber,
						g.GroupID,
						$title_field as GroupName,
						d.RecordID,
						d.InStatus,
						d.InTime,
						d.OutStatus,
						d.OutTime,
						d.Remark,
						d.DateModified,
						d.ModifiedBy,
						$confirm_username_field as ConfirmedUser,
						DATE_FORMAT(CONCAT('".$year."-".$month."-',d.DayNumber),'%Y-%m-%d') as RecordDate
						$more_fields
					FROM INTRANET_USERGROUP as ug 
					INNER JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID 
					INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
					LEFT JOIN $daily_log_table as d ON d.UserID=u.UserID $day_cond 
					LEFT JOIN INTRANET_USER as u2 ON u2.UserID=d.ModifiedBy 
					WHERE u.RecordType='".USERTYPE_STUDENT."' $conds 
					ORDER BY d.DayNumber DESC, RecordDate,g.Title,u.ClassName,u.ClassNumber+0 
				";

			$records = $this->returnResultSet($sql);
			$new_records = array();
			$user_ids_chekced = array();
			foreach($records as $temp) {
				if(in_array($temp['UserID'], $user_ids_chekced) == false) {
					$user_ids_chekced[] = $temp['UserID'];
					$new_records[] = $temp;
				}
			}
			$records = $new_records;

			if(isset($map['UserIDToRecord']) && $map['UserIDToRecord']){
				$userIdToRecords = array();
				for($i=0;$i<count($records);$i++){
					$userIdToRecords[$records[$i]['UserID']] = $records[$i];
				}
				return $userIdToRecords;
			}

			return $records;
		}

		function GetStudentAttendanceLeaveType($typeId='')
		{
			global $intranet_root, $PATH_WRT_ROOT;

			$sql = "SELECT * FROM CARD_STUDENT_LEAVE_TYPE WHERE 1 ";
			if($typeId != '' || is_array($typeId)){
				$sql .= " AND TypeID IN (".implode(",",(array)$typeId).") ";
			}

			$sql .= " ORDER BY TypeSymbol,TypeName";
			//debug_pr($sql);
			return $this->returnResultSet($sql);
		}


		function IsStudentAttendanceTypeExist($checkText, $excludeTypeId='', $checkField='TypeName')
		{
			$sql = "SELECT COUNT(*) FROM CARD_STUDENT_LEAVE_TYPE WHERE 1 ";
			if($excludeTypeId != '')
			{
				$sql .= " AND TypeID!='$excludeTypeId' ";
			}
			$sql .= " AND $checkField='".$this->Get_Safe_Sql_Query(trim($checkText))."' ";

			$record = $this->returnVector($sql);

			return $record[0] > 0;
		}

		function UpsertStudentAttendanceType($map)
		{
			$fields = array('TypeID','TypeName','TypeSymbol');
			$user_id = $_SESSION['UserID'];

			if(count($map) == 0) return false;

			if(isset($map['TypeID']) && $map['TypeID']!='' && $map['TypeID'] > 0)
			{
				$sql = "UPDATE CARD_STUDENT_LEAVE_TYPE SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'TypeID') continue;

					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($val)."'";
					$sep = ",";
				}
				$sql.= " ,ModifiedDate=NOW(),ModifiedBy='$user_id' WHERE TypeID='".$map['TypeID']."'";
				$success = $this->db_db_query($sql);
			}else{
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query($val)."'";
					$sep = ",";
				}
				$keys .= ",InputDate,ModifiedDate,InputBy,ModifiedBy";
				$values .= ",NOW(),NOW(),'$user_id','$user_id'";

				$sql = "INSERT INTO CARD_STUDENT_LEAVE_TYPE ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}

			return $success;
		}

		function DeleteStudentAttendanceType($typeId)
		{
			$typeId = IntegerSafe($typeId);
			$sql = "DELETE FROM CARD_STUDENT_LEAVE_TYPE WHERE TypeID IN (".implode(",",(array)$typeId).")";
			return $this->db_db_query($sql);
		}

		function updateLessonAttendance($AcademicYearID, $studentId, $attendovervire_id, $status, $reason='', $remarks='', $leavetype_id='')
		{
			$table_name = 'SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID;
			$sql = "UPDATE $table_name SET LastModifiedDate=NOW(), LastModifiedBy='".$_SESSION['UserID']."',LeaveTypeID='".$this->Get_Safe_Sql_Query($leavetype_id)."', AttendStatus='" . $this->Get_Safe_Sql_Query($status) . "', Reason='" . $this->Get_Safe_Sql_Query($reason) . "', Remarks='" . $this->Get_Safe_Sql_Query($remarks) . "' WHERE StudentID='" . $this->Get_Safe_Sql_Query($studentId) . "' AND AttendOverviewID='" . $this->Get_Safe_Sql_Query($attendovervire_id) . "'";
			$this->db_db_query($sql);
		}

		function checkPresetLeaveOverlap($studentId, $AttendanceType, $startdDate, $endDate = '', $SessionFrom = '', $SessionTo = '')
		{
			return $this->_checkPresetLeaveOverlap('CARD_STUDENT_PRESET_LEAVE', $studentId, $AttendanceType, $startdDate, $endDate, $SessionFrom, $SessionTo);
		}

		function _checkPresetLeaveOverlap($table, $studentId, $AttendanceType, $startdDate, $endDate = '', $SessionFrom = '', $SessionTo = '')
		{
			$target_date_period = array();
			if($AttendanceType == 0) {
				$target_date_period = array('am','pm');
				if($endDate == '' || $startDate == $endDate) {
					$sql = "SELECT count(*) as count FROM $table WHERE StudentID='" . $this->Get_Safe_Sql_Query($studentId) . "' AND RecordDate='" . $this->Get_Safe_Sql_Query($startdDate) . "'";
					$rs = $this->returnArray($sql);
					if ($rs[0]['count'] > 0) {
						return array(true);
					}
				} else {
					$sql = "SELECT count(*) as count FROM $table WHERE StudentID='".$this->Get_Safe_Sql_Query($studentId)."' AND RecordDate>='".$this->Get_Safe_Sql_Query($startdDate)."' AND RecordDate<='".$this->Get_Safe_Sql_Query($endDate)."'";
					$rs = $this->returnArray($sql);
					if($rs[0]['count'] > 0) {
						return array(true);
					}
				}
			} else if($AttendanceType == 1 || $AttendanceType == 2) {
				if($AttendanceType == 1) {
					$DayPeriod = 2;
					$target_date_period = array('am');
				} else {
					$DayPeriod = 3;
					$target_date_period = array('pm');
				}

				$sql = "SELECT count(*) as count FROM $table WHERE StudentID='".$this->Get_Safe_Sql_Query($studentId)."' AND RecordDate='".$this->Get_Safe_Sql_Query($startdDate)."' AND DayPeriod='$DayPeriod'";
				$rs = $this->returnArray($sql);
				if($rs[0]['count'] > 0) {
					return array(true);
				}
			} else if($AttendanceType == 3) {
				$timetableid = $this->getDateTimeTableID($startdDate);
				if($timetableid != '') {

					$sql = "SELECT
                                    itt.TimeSlotID, 
                                    itt.TimeSlotName, 
                                    itt.StartTime, 
                                    itt.EndTime,
                                    itt.DisplayOrder,
                                    itt.SessionType
                            FROM
                                    INTRANET_TIMETABLE_TIMESLOT as itt
                                    INNER JOIN
                                    INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr ON (itt.TimeSlotID = ittr.TimeSlotID)
                            WHERE
                                    ittr.TimetableID = '$timetableid'
                            ORDER BY
                                    itt.StartTime";

					$rs = $this->returnArray($sql);
					$time_slot_period = array();
					foreach($rs as $temp) {
						$slot = $temp['DisplayOrder'];
						$time_slot_period[$slot] = $temp['SessionType'];
					}
					$target_date_period = array();
					for($i=$SessionFrom;$i<=$SessionTo;$i++) {
						if(isset($time_slot_period[$i])) {
							$target_date_period[] = $time_slot_period[$i];
						}
					}
					$target_date_period = array_unique($target_date_period);

					$sql = "SELECT * FROM $table WHERE StudentID='".$this->Get_Safe_Sql_Query($studentId)."' AND RecordDate='".$this->Get_Safe_Sql_Query($startdDate)."'";
					$rs = $this->returnArray($sql);
					$session_to_apply = range($SessionFrom, $SessionTo);
					foreach($rs as $temp) {
						if ($temp['AttendanceType'] == 3) {
							$session_leave = range($temp['SessionFrom'], $temp['SessionTo']);
							foreach ($session_to_apply as $s) {
								if (in_array($s, $session_leave)) {
									return array(true);
								}
							}
							continue;
						}
						if ($temp['DayPeriod'] == '2' && in_array('am', $target_date_period)) {
							return array(true);
						}
						if ($temp['DayPeriod'] == '3' && in_array('pm', $target_date_period)) {
							return array(true);
						}
					}

					return array(false, $target_date_period);
				}

			}

			return array(false, $target_date_period);
		}

		function getDateSessionCount($timeslot)
		{
			if($timeslot != '') {

				$sql = "SELECT
							count(*) as count
					FROM
							INTRANET_TIMETABLE_TIMESLOT as itt
							INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT_RELATION as ittr ON (itt.TimeSlotID = ittr.TimeSlotID)
					WHERE
							ittr.TimetableID = '$timeslot'
					ORDER BY
							itt.StartTime";

				$rs = $this->returnArray($sql);
				return $rs[0]['count'];
			} else {
				return 0;
			}
		}

		function getDateTimeTableID($targetDate)
		{
			global $PATH_WRT_ROOT, $sys_custom;

			$timeslot = '';
			if($sys_custom['eBooking']['EverydayTimetable']) {
				include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
				$libcycleperiods = new libcycleperiods();
				$defaultTimetableAry = $libcycleperiods->getDefaultTimetableInfo($targetDate, $targetDate);
				if(count($defaultTimetableAry) > 0) {
					$timeslot = $defaultTimetableAry[$targetDate]['TimetableID'] ? $defaultTimetableAry[$targetDate]['TimetableID'] : $defaultTimetableAry[$targetDate]['DefaultTimetableID'];
				}
			} else {
				$TermInfo = getAcademicYearInfoAndTermInfoByDate($targetDate);

				list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;

				$CycleDayInfo = $this->Get_Attendance_Cycle_Day_Info($targetDate);

				if ($CycleDayInfo[0] == 'Cycle') {
					$DayNumber = $this->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
				}

				$SpecialTimeTableIdRecord = $this->Get_Lesson_Special_Time_Table($targetDate);
				$is_special_timetable = count($SpecialTimeTableIdRecord)>0;


				if($is_special_timetable){
					$sql = "select itra.TimetableID FROM ";
					$sql .= 'INTRANET_SCHOOL_TIMETABLE as ist 
					  inner join
					  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on 
					    ist.TimeTableID = itra.TimeTableID 
						and itra.Day = '.($CycleDayInfo[0] == 'Cycle'? '\''.$DayNumber.'\'': 'DAYOFWEEK(\''.$targetDate.'\')-1 ' ).' 
						and ist.AcademicYearID=\''.$AcademicYearID.'\' AND ist.YearTermID=\''.$YearTermID.'\'
						';
				}else{
					$sql = "select iptr.TimetableID FROM ";
					$sql .= ' INTRANET_CYCLE_GENERATION_PERIOD as icgp 
					  inner join
					  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
					  on
					    \''.$targetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
					    and icgp.PeriodID = iptr.PeriodID
					  inner join
					  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
					  on 
					    iptr.TimeTableID = itra.TimeTableID 
						and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$targetDate.'\')-1) ';
				}

				$records = $this->returnArray($sql);
				if(count($records) > 0) {
					$timeslot = $records[0]['TimetableID'];
				}
			}

			return $timeslot;
		}

		function createCountSchoolDayFunction()
		{
			$sql = 'DROP function IF EXISTS `count_schooldays`;';
			$this->db_db_query($sql);
			$sql ='CREATE FUNCTION `count_schooldays` (startDate date, endDate date)
					RETURNS INTEGER
					BEGIN
					RETURN 
						(SELECT 
						count(*)
						FROM 
						(   SELECT ADDDATE(startDate, INTERVAL @i:=@i+1 DAY) AS DAY
							FROM (
								SELECT da.a
								FROM (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS da
								CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS db
								CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS dc
							) aa
							JOIN (SELECT @i := -1) r1
							WHERE 
							@i < DATEDIFF(endDate, startDate)
					
						) AS dateTable
						WHERE dateTable.Day Not IN (
						SELECT  LEFT(EventDate,10)  as ev_date FROM INTRANET_EVENT WHERE (RecordType=3 Or RecordType=4)
						and  LEFT(EventDate,10)  >=startDate and  LEFT(EventDate,10)  <=endDate
						) and WEEKDAY(dateTable.Day) NOT IN (5,6));
						END';

			$this->db_db_query($sql);
		}

	} // End of class
} // End of directives
?>