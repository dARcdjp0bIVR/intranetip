<?php
// Using:
/************************* Changes ***********************************
 * 2020-02-03 (Sam): Url escape the password before calling the osapi, modified openAccount(), changePassword(), setUnsuspendUser()
 * 2018-06-11 (Carlos): check flag $sys_custom['osapi_use_https'] to call api with https:// .
 * 2014-07-04 (Carlos): modified openAccount(), changePassword(), setUnsuspendUser(), added parameter pwd for encryted password, removed parameter newPassword
 * 2014-05-05 (Carlos): added setSuspendUser() and setUnsuspendUser()
 * 2014-03-18 (Carlos): added changeAccountName()
 * 2011-03-30 (Carlos): modified getQuotaTable() check failure response to exactly match "0,failed"
 *********************************************************************/
# Abstract class containing functions on managing accounts on remote server /w OSAPI installed
# Write for FTP and EMail module

if (!defined("LIBOSACCOUNT_DEFINED"))                     // Preprocessor directive
{
define("LIBOSACCOUNT_DEFINED", true);

if (!isset($usr_home_dir) || $usr_home_dir=="")
{
     $usr_home_dir = "/home";
}

include_once("$intranet_root/includes/libhttpclient.php");


class libosaccount  {

      var $remote_server;
      var $remote_server_port;
      var $secret;
      var $secret_timestamp;
      var $secret_expiration;
      var $api_url;

      var $actype;
      var $login_type;        # login as email or login name, default login name
      var $login_domain;      # if $login_type == "email", this is the domain name after
	 
	  var $key = "f2rhXjdpJQ7cS75Y";
	  var $iv = "ph6LW7zLFS5KBZgH";
      
        function libosaccount()
        {
                 $this->secret_expiration = 60*60;
                 $this->secret = "";
                 $this->secret_timestamp = 0;
                 $this->api_url['Secret'] = "/osapi/secret.php";
                 $this->api_url['OpenAccount'] = "/osapi/openaccount.php";
                 $this->api_url['RemoveAccount'] = "/osapi/removeaccount.php";
                 $this->api_url['ChangePassword'] = "/osapi/changepassword.php";
                 $this->api_url['CheckAccount'] = "/osapi/checkaccount.php";
                 $this->api_url['GetQuota'] = "/osapi/getquota.php";
                 $this->api_url['GetUsedQuota'] = "/osapi/getusedquota.php";
                 $this->api_url['SetQuota'] = "/osapi/setquota.php";
                 $this->api_url['QuotaList'] = "/osapi/quotalist.php";
                 $this->api_url['SetEmailForward'] = "/osapi/setmailforward.php";
                 $this->api_url['GetEmailForward'] = "/osapi/getmailforward.php";
                 $this->api_url['ChangeAccountName'] = "/osapi/changeAccountName.php";
                 $this->api_url['SetSuspendUser'] = "/osapi/setSuspendUser.php";
                 $this->api_url['SetUnsuspendUser'] = "/osapi/setUnsuspendUser.php";
        }
		
		function isHttps()
		{
			global $web_protocol, $sys_custom;
			/*
			if((isset($web_protocol) && $web_protocol == "https") || ($sys_custom["HTTP_USED"] == "https://") 
				|| ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    	        || ( !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    	        || ( !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
    	        || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)
    	        || (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && $_SERVER['HTTP_X_FORWARDED_PORT'] == 443)
    	        || (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https'))
    		{
    			return true;
    		}
    		*/
    		if(isset($sys_custom) && $sys_custom['osapi_use_https']){
    			return true;
    		}
    		
    		return false;
		}
		
        function setRemoteAPIHost($host, $port="")
        {
                 if ($port == "") $port = 80;
                 $this->remote_server = $host;
                 $this->remote_server_port = $port;
        }

        function getWebPage($path)
        {
        		$protocol = $this->isHttps()? "https://": "http://";
                 $url = $protocol.$this->remote_server .
                                  ($this->remote_server_port!=""&&$this->remote_server_port!=80?
                                   ":".$this->remote_server_port : "")
                                  .$path;
                 $fp = @fopen($url,"r");
                 
                 $content = "";
                 if ($fp)
                 {
                     while (!feof($fp))
                     {
                     	
                             $line = trim(fgets($fp,1024*2));
                             $content .= $line;
                     }
                 }
                 else
                 {
                     $content = "";
                 }
                 return $content;
        }

        function getSecret()
        {
			$temp = trim($this->getWebPage($this->api_url['Secret']));
			if ($temp != "" && $temp != "failed")
			{
				$this->secret = trim($temp);
				$this->secret_timestamp = time();
				return true;
			}
			else return false;
        }

        function isSecretValid()
        {
                 if ($this->secret == "") return false;
                 if (time() - $this->secret_timestamp < $this->secret_expiration) return true;
                 else return false;
        }
        function checkSecret()
        {
                 if ($this->isSecretValid()) 
                 {
                 	return true;
             	 }
                 else
                 {
                     $this->getSecret();
                     if ($this->isSecretValid()) return true;
                     else return false;
                 }
        }

        // Attempt to create an user account.
        // Return TRUE on succeed and FALSE on error.
        function openAccount($newLogin,$newPassword,$module="")
        {
        		/*
                if (!$this->checkSecret())
                {
                    return false;
                }
                */
                $newLogin = strtolower($newLogin);

				if($module == "iMail"){
					if ($this->login_type=="email" && $this->login_domain != ""){
						$newLogin = $newLogin . "@" . $this->login_domain;
					}else{
						$newLogin = $newLogin;
					}
				}elseif($module == "iFolder"){
					$newLogin = $newLogin;
				}
				
                //$path = $this->api_url['OpenAccount']."?loginID=".urlencode($newLogin)."&newPassword=".urlencode($newPassword)."&secret=".urlencode($this->secret)."&module=".$module;
                $path = $this->api_url['OpenAccount']."?loginID=".urlencode($newLogin)."&pwd=".urlencode(base64_encode($this->encrypt($newPassword)))."&secret=".urlencode($this->secret)."&module=".$module;

                if ($this->actype != "")
                {
                    $path .= "&actype=".$this->actype;
                }

                $response = $this->getWebPage($path);
                if ($response[0]="1") return true;
                else return false;                 
        }

        function removeAccount($login,$module="")
        {
				if (!$this->checkSecret())
                {
                    return false;
                }
                $login = strtolower($login);
			
				if($module == "iMail"){
					if ($this->login_type=="email" && $this->login_domain != "")
					{
						$login = $login . "@" . $this->login_domain;
					}else{
						$login = $login;
					}
				}elseif($module == "iFolder" || $module == ""){
					if($module == ""){
						$module = "iFolder";
					}
					$login = $login;
				}

                $path = $this->api_url['RemoveAccount']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret)."&module=".$module;

                if ($this->actype != "")
                {
                    $path .= "&actype=".$this->actype;
                }

                $response = $this->getWebPage($path);
                if ($response[0]="1") return true;
                else return false;
        }

        // Attempt to change the password of the given $login.
        // Returns TRUE on succeed, FALSE on failed.
        function changePassword($login,$newPassword,$module = "")
        {
                if (!$this->checkSecret())
                {
                     return false;
                }
                $login = strtolower($login);

				if($module == "iMail"){
					if ($this->login_type=="email" && $this->login_domain != "")
					{
						$login = $login . "@" . $this->login_domain;
					}else{
						$login = $login;
					}
				}elseif($module == "iFolder" || $module == ""){
					if($module == ""){
						$module = "iFolder";
					}
					$login = $login;
				}
			
                //$path = $this->api_url['ChangePassword']."?loginID=".urlencode($login)."&newPassword=".urlencode($newPassword)."&secret=".urlencode($this->secret)."&module=".$module;
                $path = $this->api_url['ChangePassword']."?loginID=".urlencode($login)."&pwd=".urlencode(base64_encode($this->encrypt($newPassword)))."&secret=".urlencode($this->secret)."&module=".$module;
                if ($this->actype != "")
                {
                    $path .= "&actype=".$this->actype;
                }
                $response = $this->getWebPage($path);
                if ($response[0]="1") return true;
                else return false;
        }

        // Return TRUE if the specified user exists, FALSE otherwise.
        function isAccountExist($login,$module="")
        {
                 if (!$this->checkSecret())
                 {
                     return false;
                 }
                 $login = strtolower($login);

		if($module == "iMail"){
			if ($this->login_type=="email" && $this->login_domain != "")
			{
				$login = $login . "@" . $this->login_domain;
			}else{
				$login = $login;
			}
		}elseif($module == "iFolder" ||  $module == ""){
			if($module == ""){
				$module = "iFolder";
			}
			$login = $login;
		}
                 $path = $this->api_url['CheckAccount']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret)."&module=".$module;

                 if ($this->actype != "")
                 {
                     $path .= "&actype=".$this->actype;
                 }
                 $response = $this->getWebPage($path);
                 if ($response[0]=="1") return true;
                 else return false;
        }

        function getTotalQuota($login,$module="")
        {
                 if (!$this->checkSecret())
                 {
                     return false;
                 }
                 $login = strtolower($login);
			if($module == "iMail"){
				if ($this->login_type=="email" && $this->login_domain != "")
				{
					$login = $login . "@" . $this->login_domain;
				}else{
					$login = $login;
				}
			}elseif($module == "iFolder" ||  $module==""){
				if($module == ""){
					$module = "iFolder";
				}
				$login=$login;
			}
                 $path = $this->api_url['GetQuota']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret)."&module=".$module;
                 if ($this->actype != "")
                 {
                     $path .= "&actype=".$this->actype;
                 }
                 $response = $this->getWebPage($path);

                 if ($response[0]=="1")
                 {
                     $temp = explode(",",$response);
                     return $temp[1];
                 }
                 else return false;
        }

        # Get back format : $login,$used,$soft,$hard
        function getQuotaTable($module)
        {
                 if (!$this->checkSecret())
                 {
                     return false;
                 }
                 $path = $this->api_url['QuotaList']."?secret=".urlencode($this->secret)."&module=".$module;
                 if ($this->actype != "")
                 {
                     $path .= "&actype=".$this->actype;
                 }
                 $response = $this->getWebPage($path);
                 //if ($response[0]=="0") return false; // may have conflict if return result starts with "0..."
                 if ($response=="0,failed") return false;
                 else
                 {
                     $temp = explode("###",$response);
                     for ($i=0; $i<sizeof($temp); $i++)
                     {
                          $temp_array = explode(",",$temp[$i]);
                          $result[] = $temp_array;
                     }
                     return $result;
                 }
        }

        function getUsedQuota($login,$module)
        {
                 if (!$this->checkSecret())
                 {
                     return false;
                 }
                 $login = strtolower($login);

			if($module == "iMail"){
				if ($this->login_type=="email" && $this->login_domain != "")
				{
					$login = $login . "@" . $this->login_domain;
				}else{
					$login = $login;
				}
			}elseif($module == "iFolder"){
				$login = $login;
			}

                 //if ($this->login_type=="email" && $this->login_domain != "")
                 //{
                 //    $login = $login . "@" . $this->login_domain;
                 //}

                 $path = $this->api_url['GetUsedQuota']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret)."&module=".$module;
                 if ($this->actype != "")
                 {
                     $path .= "&actype=".$this->actype;
                 }
                 $response = $this->getWebPage($path);
                 if ($response[0]=="1")
                 {
                     $temp = explode(",",$response);
                     return $temp[1]+0;
                 }
                 else return false;
        }
        function setTotalQuota($login, $quota, $module)
        {
			if (!$this->checkSecret())
			{
				return false;
			}
			$login = strtolower($login);

			if($module == "iMail"){
				if ($this->login_type=="email" && $this->login_domain != ""){
					if (stristr($login, '@')) {
						$login = $login;
					} else {
						$login = $login . "@" . $this->login_domain;
					}
				}else{
					$login = $login;
				}
			}elseif($module == "iFolder"){
				$login = $login;
			}
			
			$path = $this->api_url['SetQuota']."?loginID=".urlencode($login)."&quota=".urlencode($quota)."&secret=".urlencode($this->secret)."&module=".$module;
			if ($this->actype != "")
			{
				$path .= "&actype=".$this->actype;
			}
			
			$response = $this->getWebPage($path);
			
			if ($response[0]=="1")
			{
				return true;
			}
			else return false;
        }
        function setMailForward($login, $targetEmail, $keepCopy)
        {
                 if (!$this->checkSecret())
                 {
                     return false;
                 }
                 # Try to connect to service host
                 $lhttp = new libhttpclient($this->remote_server,$this->remote_server_port);
                 if (!$lhttp->connect())    # Exit if failed to connect
                      return false;
                 $login = strtolower($login);
                 if ($this->login_type=="email" && $this->login_domain != "")
                 {
                     $login = $login . "@" . $this->login_domain;
                 }
                 $targetEmail = stripslashes($targetEmail);
                 $lhttp->set_path($path = $this->api_url['SetEmailForward']);
                 if($keepCopy == ''){
	                 $keepCopy = 0;
                 }
                 $post_data = "loginID=".urlencode($login)."&targetEmail=".urlencode($targetEmail)."&keepCopy=".urlencode($keepCopy)."&secret=".urlencode($this->secret);
                 
                 if ($this->actype != "")
                 {
                     $post_data .= "&actype=".$this->actype;
                 }
                 
                 $lhttp->post_data = $post_data;
                 $response = $lhttp->send_request();

                 $pos = strpos($response,"\r\n\r\n");
                 $response = substr($response,$pos+4);

                 if ($response[0]=="1")
                 {
                     return true;
                 }
                 else return false;
        }
        function removeMailForward($login)
        {
                 if (!$this->checkSecret())
                 {
                     return false;
                 }
                 $login = strtolower($login);
                 if ($this->login_type=="email" && $this->login_domain != "")
                 {
                     $login = $login . "@" . $this->login_domain;
                 }
                 $path = $this->api_url['SetEmailForward']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret);
                 if ($this->actype != "")
                 {
                     $path .= "&actype=".$this->actype;
                 }
                 $response = $this->getWebPage($path);
                 if ($response[0]=="1")
                 {
                     return true;
                 }
                 else return false;
        }
        #### Get .forward content
        #### Return : array ($keepCopy, $forwarded_email)
        function getMailForward($login)
        {
                 $login = strtolower($login);
                 if ($this->login_type=="email" && $this->login_domain != "")
                 {
                     $login = $login . "@" . $this->login_domain;
                 }
                 $path = $this->api_url['GetEmailForward']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret);
                 if ($this->actype != "")
                 {
                     $path .= "&actype=".$this->actype;
                 }
                 $response = $this->getWebPage($path);
                 if ($response == "None")
                 {
                     return array(false, "");
                 }
                 $pos = strpos($response, "\\$login");
                 if ($pos === false)
                 {
                     return array(false,$response);
                 }
                 else
                 {
                     $response = substr($response, $pos+strlen($login)+1);
                     return array(true, $response);
                 }
        }
		
		/*
		 * @param $ParOldAccount : Old account name. No @domainname for iFolder, Should have @domainname for Webmail or iMail plus
		 * @param $ParnewAccount : New account name. 
		 * @param $module : "ifolder" for iFolder, "iMail" for Webmail or iMail plus
		 */
		function changeAccountName($ParOldAccount, $ParNewAccount, $module="")
	    {
	        global $SYS_CONFIG;
			
	        if (!$this->checkSecret())
	        {
	             return false;
	        }
			
			$oldAccount = $ParOldAccount;
			$newAccount = $ParNewAccount;
			
	        $success = true;
			$path = $this->api_url['ChangeAccountName'];
	        $post_data = "oldAccount=".urlencode($oldAccount);
	        $post_data .= "&newAccount=".urlencode($newAccount);
	        if($module != ""){
	        	$post_data .= "&module=".$module;
	        }
	        if ($this->actype != "")
	        {
	            $post_data .= "&actype=".$this->actype;
	        }
			$post_data .= "&secret=".urlencode($this->secret);
	
	        $response = $this->getWebPage($path."?".$post_data);
	        $success = $response=="1";
	           
	        if ($success)
	        {
	            return true;
	        }else{
	        	return false;
	        }
	    }
		
		/*
		 * $loginID : iMail should have @domain, iFolder does not
		 * $module: iMail or iFolder
		 */
		function setSuspendUser($loginID,$module)
		{
			global $SYS_CONFIG;
			
	        if (!$this->checkSecret())
	        {
	             return false;
	        }
			
	        $success = true;
			$path = $this->api_url['SetSuspendUser'];
	        $post_data = "loginID=".urlencode($loginID);
	        
	        if ($this->actype != "")
	        {
	            $post_data .= "&actype=".$this->actype;
	        }
	        $post_data .= "&module=".$module;
			$post_data .= "&secret=".urlencode($this->secret);
	
	        $response = $this->getWebPage($path."?".$post_data);
	        $success = $response[0]=="1";
	        
	        if ($success)
	        {
	            return true;
	        }else{
	        	return false;
	        }
		}
		
		/*
		 * $loginID : iMail should have @domain, iFolder does not
		 * $module: iMail or iFolder
		 * $password: iFolder need to provide original password to resume ftp account
		 */
		function setUnsuspendUser($loginID,$module,$password="")
		{
			global $SYS_CONFIG;
			
	        if (!$this->checkSecret())
	        {
	             return false;
	        }
			
	        $success = true;
			$path = $this->api_url['SetUnsuspendUser'];
	        $post_data = "loginID=".urlencode($loginID);
	        
	        if ($this->actype != "")
	        {
	            $post_data .= "&actype=".$this->actype;
	        }
	        $post_data .= "&module=".$module;
	        //$post_data .= "&password=".rawurlencode($password);
	        $post_data .= "&secret=".urlencode($this->secret);
	        $post_data .= "&pwd=".urlencode(base64_encode($this->encrypt($password)));
	
	        $response = $this->getWebPage($path."?".$post_data);
	        $success = $response[0]=="1";
	        
	        if ($success)
	        {
	            return true;
	        }else{
	        	return false;
	        }
		}
		
		// encrypt with aes-128 in ecb mode
		function encrypt($text)
		{
			if($text == "") return $text;
			
			$data = $this->pad($text,16);
			/*
			 * echo string in this format $'abcd'edf' to escape single quotes and line feed characters
			 */
			$command="echo ".'$'."'".str_replace("'",'\\\'',$data)."' | openssl enc -aes-128-ecb -nosalt -a -A -K " . bin2hex ($this->key) . " -iv " . bin2hex ($this->iv);
			$encrypted_text=trim(shell_exec("$command"));
			
			return $encrypted_text;
		}
		
		// make string size multiple of block size, pad with the char of the pad size
		function pad($text, $block_size)
		{
			$data = $text;
			$pad = $block_size - (strlen($data) % $block_size);
			$data .= str_repeat(chr($pad), $pad);
			
			return $data;
		}
	} # End of class

}        # End of directive
?>