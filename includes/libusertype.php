<?php
### Using By: Ronald

if (!defined("LIBUSERTYPE_DEFINED"))         // Preprocessor directives
{

 define("LIBUSERTYPE_DEFINED",true);

 class libusertype extends libdb
 {
       var $retrievedID;
       var $RecordType;

       function libusertype()
       {
                $this->libdb();
       }
       function returnIdentity($uid)
       {
                if ($uid != $this->retrievedID)      # Retrieve from DB if not the same
                {
                    $sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '$uid'";
                    $result = $this->returnVector($sql);
                    $this->RecordType = $result[0];
                    $this->retrievedID = $uid;
                }
                return $this->RecordType;     # return result
       }
       function isIdentity($uid,$target)
       {
                return ($this->returnIdentity == $target);  # return result
       }
       function isTeacherStaff($uid)
       {
                return $this->isIdentity($uid,1);
       }
       function isStudent($uid)
       {
                return $this->isIdentity($uid,2);
       }
       function isParent($uid)
       {
                return $this->isIdentity($uid,3);
       }
       function returnTeachingStaff()
       {
                $username_field = getNameFieldWithClassNumberByLang("");
                $sql = "SELECT UserID, $username_field FROM INTRANET_USER WHERE RecordType = 1 AND Teaching = 1";
                return $this->returnArray($sql,2);
       }
       function returnNonTeachingStaff()
       {
                $username_field = getNameFieldWithClassNumberByLang("");
                $sql = "SELECT UserID, $username_field FROM INTRANET_USER WHERE RecordType = 1 AND (Teaching = 0 OR Teaching IS NULL)";
                return $this->returnArray($sql,2);
       }
       
       ### The following is added by Ronald (20090918) ###
       function isTeacher($uid)
       {
       			$sql = "SELECT Teaching FROM INTRANET_USER WHERE UserID = '$uid' AND RecordType = 1";
       			$result = $this->returnVector($sql);
       			if($result[0] == 1){
       				return true;
       			}
       			return false;
       }
 }


} // End of directives
?>