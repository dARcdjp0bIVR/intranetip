<?php
// Using: 
/*
 * 2013-10-07 (Carlos): [KIS] modified Get_Class_Group_Selection(), added parameter $firstTitleText
 * 						[KIS] added Get_Class_GroupID_By_UserID() for getting user related class groups
 */

if (!defined("LIBCLASSGROUP_DEFINED"))         // Preprocessor directives
{
	define("LIBCLASSGROUP_DEFINED", true);
	
	class Class_Group extends libdb {
		var $ClassGroupID;
		var $Code;
		var $TitleEn;
		var $TitleCh;
		var $DisplayOrder;
		var $RecordStatus;
		var $DateInput;
		var $DateModified;
		var $LastModifiedBy;
		
		function Class_Group($ClassGroupID=''){
			parent:: libdb();
			$this->TableName = 'YEAR_CLASS_GROUP';
			$this->ID_FieldName = "ClassGroupID";
			
			if ($ClassGroupID != '')
			{
				$this->RecordID = $ClassGroupID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_Self_Info()
		{
			$sql = "";
			$sql .= " SELECT * FROM ".$this->TableName;
			$sql .= " WHERE ".$this->ID_FieldName." = '".$this->RecordID."'";
			
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			if (count($infoArr > 0))
			{
				foreach ($infoArr as $key => $value)
					$this->{$key} = $value;
			}
		}
		
		function Get_Name($Lang='')
		{
			$returnName = '';
			if ($Lang == '')
			{
				$returnName = $this->TitleEn.' ('.$this->TitleCh.')';
			}
			else if ($Lang == 'en')
			{
				$returnName = $this->TitleEn;
			}
			else if ($Lang == 'b5')
			{
				$returnName = $this->TitleCh;
			}
			
			return $returnName;
		}
		
		function Get_All_Class_Group($active='')
		{
			$recordstatus_conds = '';
			if ($active != '')
				$recordstatus_conds = " And RecordStatus = '".$active."' ";
				
			$sql = "Select
							*
					From
							".$this->TableName."
					Where
							1
							$recordstatus_conds
					Order By
							DisplayOrder
					";
			$classGroupInfoArr = $this->returnArray($sql);
			return $classGroupInfoArr;
		}
		
		function Get_Class_List()
		{
			$sql = "Select
							*
					From
							YEAR_CLASS
					Where
							ClassGroupID = '".$this->ClassGroupID."'
					";
			$subjectInfoArr = $this->returnArray($sql);
			return $subjectInfoArr;
		}
		
		function Get_Last_Modified_Info()
		{
			if ($this->ClassGroupID != '')
			{
				# self last modified info
				$sql = '';
				$sql .= ' SELECT * FROM '.$this->TableName;
				$sql .= ' WHERE ClassGroupID = \''.$this->ClassGroupID.'\' ';
			}
			else
			{
				# all class group last modified info
				$sql = '';
				$sql .= ' SELECT * ';
				$sql .= ' FROM '.$this->TableName;
				$sql .= ' WHERE DateModified IN 
								(SELECT MAX(DateModified) FROM '.$this->TableName.') ';
			}
			
			$returnArr = $this->returnArray($sql);
			return $returnArr;
		}
		
		function Insert_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			## insert data
			# DisplayOrder
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			
			# Last Modified By
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DisplayOrder
			$fieldArr[] = 'DisplayOrder';
			$valueArr[] = $thisDisplayOrder;
			# Last Modified By
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $LastModifiedBy;
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			# Set RecordStatus = active
			$fieldArr[] = 'RecordStatus';
			$valueArr[] = 1;
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success = $this->db_db_query($sql);
			if ($success == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
			
			return $success;
		}
		
		function Update_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
			}
			$valueFieldText .= ' DateModified = now(), ';
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
			
			$sql = '';
			$sql .= ' UPDATE '.$this->TableName;
			$sql .= ' SET '.$valueFieldText;
			$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->RecordID.'\' ';
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Delete_Record()
		{
			$sql = 'Delete From '.$this->TableName.' Where '.$this->ID_FieldName.' = \''.$this->RecordID.'\' ';
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Inactivate_Record()
		{
			$DataArr['RecordStatus'] = LOCATION_STATUS_INACTIVE;
			$success = $this->Update_Record($DataArr);
			
			return $success;
		}
		
		function Activate_Record()
		{
			$DataArr['RecordStatus'] = LOCATION_STATUS_ACTIVE;
			
			# assign to the last display for activated records
			$thisDisplayOrder = $this->Get_Max_Display_Order() + 1;
			$DataArr['DisplayOrder'] = $thisDisplayOrder;
			
			$success = $this->Update_Record($DataArr);
			return $success;
		}
		
		function Get_Max_Display_Order()
		{
			$sql = '';
			$sql .= 'SELECT max(DisplayOrder) FROM '.$this->TableName;
			$MaxDisplayOrder = $this->returnVector($sql);
			
			return $MaxDisplayOrder[0];
		}
		
		function Is_Code_Existed($TargetCode, $ExcludeID='')
		{
			$TargetCode = $this->Get_Safe_Sql_Query(trim($TargetCode));
			
			$excludeClassGroupCond = '';
			if ($ExcludeID != '')
				$excludeClassGroupCond = " AND ClassGroupID != '$ExcludeID' ";
				
			# Check Class Group Code
			$sql = "SELECT 
							DISTINCT(ClassGroupID) 
					FROM 
							".$this->TableName." 
					WHERE 
							Code = '".$TargetCode."'
							$excludeClassGroupCond
					";
			$classGroupArr = $this->returnVector($sql);
			$isCodeExisted = (count($classGroupArr)==0)? 0 : 1;
			
			return $isCodeExisted;
		}
		
		function Get_Update_Display_Order_Arr($OrderText, $Separator=",", $Prefix="")
		{
			if ($OrderText == "")
				return false;
				
			$orderArr = explode($Separator, $OrderText);
			$orderArr = array_remove_empty($orderArr);
			$orderArr = Array_Trim($orderArr);
			
			$numOfOrder = count($orderArr);
			# display order starts from 1
			$counter = 1;
			$newOrderArr = array();
			for ($i=0; $i<$numOfOrder; $i++)
			{
				$thisID = str_replace($Prefix, "", $orderArr[$i]);
				if (is_numeric($thisID))
				{
					$newOrderArr[$counter] = $thisID;
					$counter++;
				}
			}
			
			return $newOrderArr;
		}
		
		function Update_DisplayOrder($DisplayOrderArr=array())
		{
			if (count($DisplayOrderArr) == 0)
				return false;
				
			$this->Start_Trans();
			for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
				$thisSubjectClassID = $DisplayOrderArr[$i];
				
				$sql = 'UPDATE '.$this->TableName.' SET 
									DisplayOrder = \''.$i.'\' 
								WHERE 
									'.$this->ID_FieldName.' = \''.$thisSubjectClassID.'\'';
				$Result['ReorderResult'.$i] = $this->db_db_query($sql);
			}
			
			if (in_array(false, $Result)) 
			{
				$this->RollBack_Trans();
				return false;
			}
			else
			{
				$this->Commit_Trans();
				return true;
			}
		}
		
		function Get_Class_Group_Selection($ID_Name, $SelectedClassGroupID='', $Onchange='', $noFirst=0, $firstTitleText='')
		{
			global $Lang;
			
			$ClassGroupArr = $this->Get_All_Class_Group($active=1);
			$numOfClassGroup = count($ClassGroupArr);
			
			$selectArr = array();
			for ($i=0; $i<$numOfClassGroup; $i++)
			{
				$thisClassGroupID = $ClassGroupArr[$i]['ClassGroupID'];
				$thisName = Get_Lang_Selection($ClassGroupArr[$i]['TitleCh'], $ClassGroupArr[$i]['TitleEn']);
				
				$selectArr[$thisClassGroupID] = $thisName;
			}
			
			$onchange = '';
			if ($Onchange != "")
				$onchange = ' onchange="'.$Onchange.'" ';
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			
			if ($noFirst == 0){
				if($firstTitleText != '') {
					$firstTitle = Get_Selection_First_Title($firstTitleText);
				}else{
					$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['ClassGroup']);
				}
			}
			$classGroupSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedClassGroupID, $isAll=0, $noFirst, $firstTitle);
			
			return $classGroupSelection;
		}
		
		function Get_Class_Group_ID_By_Code($ParCode)
		{
			$sql = "
				SELECT 
					ClassGroupID
				FROM 
					YEAR_CLASS_GROUP
				WHERE
					Code = '$ParCode'
			";
			
			$tmp = $this->returnVector($sql);
			return  empty($tmp)?'':$tmp[0];
			
		}
		
		function Get_Class_GroupID_By_UserID($academic_year_id, $user_id='')
		{
			global $intranet_root;
			include_once($intranet_root."/includes/libuser.php");
			
			$target_academic_year_id = $academic_year_id;
			$target_user_id = $user_id == '' ? $_SESSION['UserID'] : $user_id;
			
			$libuser = new libuser($target_user_id);
			
			$result = array();
			if($libuser->isTeacherStaff())
			{
				/*
				$sql = "SELECT DISTINCT ycg.ClassGroupID 
						FROM YEAR_CLASS as yc 
						INNER JOIN YEAR_CLASS_TEACHER as yct ON yct.YearClassID=yc.YearClassID 
						INNER JOIN YEAR_CLASS_GROUP as ycg ON ycg.ClassGroupID=yc.ClassGroupID 
						WHERE yct.UserID='$target_user_id' AND yc.AcademicYearID='".$target_academic_year_id."' ";
				*/
				$sql = "SELECT DISTINCT ycg.ClassGroupID 
						FROM YEAR_CLASS_GROUP as ycg";
				$result = $this->returnVector($sql);
			}else if($libuser->isStudent()){
				$sql = "SELECT 
							DISTINCT ycg.ClassGroupID 
						FROM YEAR_CLASS as yc 
						INNER JOIN YEAR_CLASS_USER as ycu On ycu.YearClassID=yc.YearClassID
						INNER JOIN YEAR_CLASS_GROUP as ycg ON ycg.ClassGroupID=yc.ClassGroupID 
						WHERE ycu.UserID='$target_user_id' AND yc.AcademicYearID='".$target_academic_year_id."' ";
				$result = $this->returnVector($sql);
			}else if($libuser->isParent()){
				$children_id_ary = $libuser->getChildren();
				if(count($children_id_ary)>0) {
					$sql = "SELECT 
								DISTINCT ycg.ClassGroupID 
							FROM YEAR_CLASS as yc 
							INNER JOIN YEAR_CLASS_USER as ycu On ycu.YearClassID=yc.YearClassID
							INNER JOIN YEAR_CLASS_GROUP as ycg ON ycg.ClassGroupID=yc.ClassGroupID 
							WHERE ycu.UserID IN (".implode(",",$children_id_ary).") AND yc.AcademicYearID='".$target_academic_year_id."' ";
					$result = $this->returnVector($sql);
				}
			}
			//else if($libuser->isAlumni()){}
			if(count($result)==0) {
				$result[] = -1;
			}
			
			return $result;
		}
	}
} // End of directives
?>